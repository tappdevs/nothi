-- 15-05-2016 --
-- office db --
ALTER TABLE `nothi_master_permissions` ADD `nothi_office` INT NULL DEFAULT '0' AFTER `nothi_part_no`;
ALTER TABLE `nothi_master_current_users` ADD `nothi_office` INT NOT NULL DEFAULT '0' AFTER `nothi_part_no`;
ALTER TABLE `nothi_master_movements` ADD `nothi_office` INT NOT NULL DEFAULT '0' AFTER `nothi_part_no`;

-- 15-05-2016 --
-- Office DB --
ALTER TABLE `nothi_master_current_users` ADD `is_new` TINYINT(2) NOT NULL DEFAULT '0' AFTER `forward_date`;

-- 17-05-2016 --
-- nothi access db --
ALTER TABLE `nothi_master_permissions` ADD INDEX(`nothi_office`); 
ALTER TABLE `nothi_master_current_users` ADD INDEX(`nothi_office`);

-- 23-05-2016 (Only For Nothi Access DB)
ALTER TABLE `nothi_master_permissions` ADD `designation_level` INT NOT NULL DEFAULT '0' AFTER `office_unit_organograms_id`;

-- (only for all offices db)
ALTER TABLE `potrojari` ADD `potrojari_draft_unit` INT NOT NULL DEFAULT '0' AFTER `nothi_part_no`;
ALTER TABLE `potrojari` ADD `potrojari_draft_office_id` INT NOT NULL DEFAULT '0' AFTER `nothi_part_no`;

-- 24-05-2016 (Only for Projapoti DB)
ALTER TABLE `dak_actions` ADD `office_id` INT NOT NULL DEFAULT '0' AFTER `status`, ADD `unit_id` INT NOT NULL DEFAULT '0' AFTER `office_id`, ADD `organogram_id` INT NOT NULL DEFAULT '0' AFTER `unit_id`;

-- 28-05-2016 (Only For Nothi Access DB)
UPDATE `nothi_master_current_users` SET `nothi_office`=`office_id` WHERE `nothi_office`=0

-- 29-05-2016 (For all office db)
ALTER TABLE `potro_flags` CHANGE `potro_no` `page_no` INT NULL;
ALTER TABLE `potro_flags` ADD `potro_id` BIGINT NOT NULL AFTER `title`;
ALTER TABLE `potro_flags` CHANGE `potro_id` `potro_attachment_id` BIGINT(20) NOT NULL;
ALTER TABLE `potro_flags` DROP `employee_id`;
ALTER TABLE `potro_flags` CHANGE `page_no` `page_no` INT(11) NULL DEFAULT '0';
ALTER TABLE `potro_flags` ADD INDEX( `nothi_master_id`, `office_id`);
ALTER TABLE `nothi_types` CHANGE `type_last_number` `type_last_number` BIGINT(20) NULL DEFAULT '0';

-- (For Projapoti DB)
INSERT INTO `module_menus` (`id`,`module_info_id`, `menu_name`, `menu_name_bng`, `menu_controller`, `menu_action`, `main_menu_id`, `sub_menu_id`, `sequence_no`, `media_file_name`, `media_dir`, `created`, `modified`, `created_by`, `modified_by`) VALUES
(190, 5, 'Settings', 'সেটিং', '', NULL, 0, 0, 5, NULL, NULL, NOW(), NOW(), '', ''),
(191, 5, 'Dak Decissions', 'ডাক সিদ্ধান্ত', 'dakActions', 'index', 190, 0, 1, NULL, NULL, NOW(), NOW(), '', '');

-- 30-05-2016 (For Projapoti DB)
ALTER TABLE `nothi_decisions` ADD `office_id` INT NULL DEFAULT '0' AFTER `status`, ADD `unit_id` INT NULL DEFAULT '0' AFTER `office_id`, ADD `organogram_id` INT NULL DEFAULT '0' AFTER `unit_id`;

-- 01-06-2016 (For Office DB)
update nothi_parts set nothi_created_date = date(modified) where nothi_created_date = '0000-00-00' or nothi_created_date is null