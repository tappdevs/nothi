
--
-- Table structure for table `notification_events`
--

DROP TABLE IF EXISTS `notification_events`;
CREATE TABLE `notification_events` (
  `id` int(11) NOT NULL,
  `event_name_bng` varchar(255) NOT NULL,
  `event_name_eng` varchar(255) NOT NULL,
  `notification_type` varchar(100) DEFAULT NULL,
  `template_id` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `notification_events`
--

INSERT INTO `notification_events` (`id`, `event_name_bng`, `event_name_eng`, `notification_type`, `template_id`) VALUES
(3, 'ডাক আপলোড', 'Dak Upload', 'Dak.upload', 1),
(5, 'ডাক ফরোয়ার্ড ', 'forward Dak', 'Dak.forward', 1),
(6, 'আগত ডাক', 'Dak Inbox', 'Dak.inbox', 1),
(7, 'প্রেরিত ডাক', 'Dak Sent', 'Dak.sent', 1),
(8, 'ডাক নথিতে উপস্থাপিত', 'Dak Nothivukto', 'Dak.nothivukto', 1),
(9, 'পুরানো ডাক ', 'Dak Aging', 'Dak.waiting', 1),
(10, 'আগত নথি', 'Nothi Inbox', 'Nothi.inbox', 2),
(11, 'প্রেরিত নথি', 'Nothi Sent', 'Nothi.sent', 2),
(13, 'পত্রজারি খসড়া তৈরি', 'Create Draft', 'Nothi.draft', 2),
(14, 'পত্রজারি', 'Sent Potrojari', 'Nothi.potrojari', 2),
(15, 'SMTP পোর্ট', 'SMTP PORT', NULL, 3),
(16, 'SMTP হোস্ট', 'SMTP Host', NULL, 3),
(17, 'SMTP ইউজারনেম', 'SMTP Username', NULL, 3),
(18, 'SMTP পাসওয়ার্ড', 'SMTP Password', NULL, 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `notification_events`
--
ALTER TABLE `notification_events`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `notification_events`
--
ALTER TABLE `notification_events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;


-- 23-04-2016
-- for All office

CREATE TABLE IF NOT EXISTS `nothi_note_attachment_refs` (
  `id` bigint(20) NOT NULL,
  `nothi_master_id` bigint(20) NOT NULL,
  `nothi_part_no` bigint(20) NOT NULL DEFAULT '1',
  `attachment_type` varchar(255) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `nothi_note_attachment_refs`
--
ALTER TABLE `nothi_note_attachment_refs`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `nothi_note_attachment_refs`
--
ALTER TABLE `nothi_note_attachment_refs`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

-- 26-04-2016
-- Office DB
ALTER TABLE `nothi_note_attachments` ADD `user_file_name` VARCHAR(255) NULL DEFAULT NULL AFTER `file_name`; 

-- 3-05-2016 (Only For Projapoti DB)
CREATE TABLE IF NOT EXISTS `dak_tracks` ( `sarok_no` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL , `sender_office_id` INT NULL , `sender_officer_designation_id` INT NULL , `sender_name` VARCHAR(255) NULL , `sender_email` VARCHAR(255) NULL , `sender_mobile` VARCHAR(255) NULL , `dak_received_no` VARCHAR(50) NULL , `docketing_no` VARCHAR(255) NULL , `receiving_date` DATETIME NULL , `dak_subject` VARCHAR(255) NULL , `receiving_office_id` INT NULL , `receiving_officer_designation_label` VARCHAR(255) NULL , `receiving_officer_name` VARCHAR(255) NULL , `dak_id` BIGINT(20) NULL , `dak_type` VARCHAR(255) NULL , `id` BIGINT(20) NULL AUTO_INCREMENT , PRIMARY KEY (`id`)) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `dak_tracks` ADD `created` DATETIME NULL AFTER `id`, ADD `modified` DATETIME NULL AFTER `created`;
