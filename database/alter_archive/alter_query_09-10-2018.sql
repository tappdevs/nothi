-- 01-01-2018 (Office Db)
ALTER TABLE `potrojari_receiver` ADD `group_member` INT NOT NULL DEFAULT '1' AFTER `group_name`;
ALTER TABLE `potrojari_onulipi` ADD `group_member` INT NOT NULL DEFAULT '1' AFTER `group_name`;

-- 03-01-2018 (Projapoti Db)
ALTER TABLE `employee_offices` CHANGE `last_office_date` `last_office_date` DATE NULL;

-- 02-01-2018 (Projapoti Db)
ALTER TABLE `employee_offices` CHANGE `status` `status` TINYINT(1) NULL, CHANGE `status_change_date` `status_change_date` DATETIME NULL, CHANGE `created` `created` DATETIME NULL, CHANGE `modified` `modified` DATETIME NULL, CHANGE `created_by` `created_by` BIGINT(20) NULL, CHANGE `modified_by` `modified_by` BIGINT(20) NULL;

-- 10-01-2018 (Projapoti Db)
ALTER TABLE `office_unit_organograms` ADD `is_admin` TINYINT(1) NOT NULL DEFAULT '0' AFTER `status`;

-- 10-01-2018 (Office Db)
ALTER TABLE `nothi_master_current_users` ADD `is_summary` TINYINT(2) NOT NULL DEFAULT '0' AFTER `is_finished`;
-- 02-01-2018 (Nothi Access DB)
ALTER TABLE `guard_file_api_datas` ADD `layer_id` INT NULL AFTER `id`;

-- 15-01-2018 (Office DB)
ALTER TABLE `nothi_potro_attachments` ADD `user_file_name` VARCHAR(255) NULL DEFAULT NULL AFTER `file_name`;
ALTER TABLE `potrojari_attachments` ADD `user_file_name` VARCHAR(255) NULL DEFAULT NULL AFTER `file_name`;
ALTER TABLE `dak_attachments` ADD `user_file_name` VARCHAR(255) NULL DEFAULT NULL AFTER `file_name`;
-- alters after live server update
-- 23-01-2018 (Projapoti DB)
UPDATE `projapoti_db`.`notification_events` SET `event_name_bng` = 'পত্রজারি খসড়া' WHERE `notification_events`.`event_name_eng` = 'Create Draft';

-- 14-02-2018 (Office DB)
ALTER TABLE `dak_movements` ADD `from_potrojari` TINYINT(2) NOT NULL DEFAULT '0' AFTER `dak_actions`;

-- 28-02-2018 (Projapoti DB)
ALTER TABLE `offices` ADD `unit_organogram_edit_option` INT(1) NULL COMMENT '1 for all; 2 for only unit; 3 for only organogram; zero/null for disabled;' AFTER `active_status`, ADD `unit_organogram_edit_option_status_updated_at` TIMESTAMP NULL AFTER `unit_organogram_edit_option`;

-- 06-03-2018 (Projapoti DB)
CREATE TABLE IF NOT EXISTS `nothi_archive_requests` (`id` int(11) NOT NULL,`office_id` int(11) DEFAULT NULL,`nothi_master_id` int(11) DEFAULT NULL,`requested_date` datetime DEFAULT NULL,`requested_organogram_id` int(11) DEFAULT NULL,`status` int(11) DEFAULT NULL COMMENT '1 for no action; 2 for running; 3 for done; 4 for failed;',`responsed_date` datetime DEFAULT NULL,`comments` varchar(1024) DEFAULT NULL,`created_at` timestamp NULL DEFAULT NULL,`updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
ALTER TABLE `nothi_archive_requests` ADD PRIMARY KEY (`id`);
ALTER TABLE `nothi_archive_requests` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

-- 20-03-2018 (Office DB)
ALTER TABLE `potrojari_receiver` ADD `officer_mobile` VARCHAR(20) NULL DEFAULT NULL AFTER `officer_designation_label`, ADD `sms_message` VARCHAR(250) NULL DEFAULT NULL AFTER `officer_mobile`;
ALTER TABLE `potrojari_onulipi` ADD `officer_mobile` VARCHAR(20) NULL DEFAULT NULL AFTER `officer_designation_label`, ADD `sms_message` VARCHAR(250) NULL DEFAULT NULL AFTER `officer_mobile`;

-- 27-03-2018 (Nothi Access DB)
DROP TABLE IF EXISTS `protikolpo_settings`;
CREATE TABLE `protikolpo_settings` (
  `id` int(11) NOT NULL,
  `office_id` int(11) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `designation_id` bigint(20) NOT NULL,
  `protikolpos` text NOT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `active_status` tinyint(4) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `modified_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `protikolpo_settings`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `protikolpo_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `protikolpo_settings` ADD `selected_protikolpo` INT NOT NULL AFTER `protikolpos`;
ALTER TABLE `protikolpo_settings` CHANGE `active_status` `active_status` TINYINT(4) NOT NULL DEFAULT '0';
ALTER TABLE `protikolpo_settings` CHANGE `protikolpos` `protikolpos` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;
ALTER TABLE `protikolpo_settings` ADD `employee_record_id` BIGINT NULL AFTER `designation_id`;

-- 27-03-2018 (Projapodi DB)
ALTER TABLE `employee_offices` ADD `protikolpo_status` TINYINT(2) NOT NULL DEFAULT '0' AFTER `show_unit`;

-- 28-03-2018 (Nothi Access DB)
ALTER TABLE `protikolpo_settings` ADD INDEX(`office_id`);
ALTER TABLE `protikolpo_settings` ADD INDEX(`active_status`);
ALTER TABLE `protikolpo_settings` ADD `employee_name` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `employee_record_id`;

-- 30-03-2018 (Nothi Access DB)
DROP TABLE IF EXISTS `login_page_settings`;
CREATE TABLE `login_page_settings` (
  `id` int(11) NOT NULL,
  `type` tinyint(4) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `attachments` text,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
ALTER TABLE `login_page_settings`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `login_page_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

 -- 01-04-2018 (Office DB)
ALTER TABLE `potrojari_attachments` ADD `options` TEXT NULL DEFAULT NULL AFTER `file_dir`;
-- 05-04-2018 (Office DB)
ALTER TABLE `potrojari_receiver` ADD `options` TEXT NULL DEFAULT NULL AFTER `receiving_office_head`;
ALTER TABLE `potrojari_onulipi` ADD `options` TEXT NULL DEFAULT NULL AFTER `receiving_office_head`;

-- 17-04-2018 (Nothi Access DB))
DROP TABLE IF EXISTS `login_notice_attachments`;
CREATE TABLE `login_notice_attachments` (
  `id` bigint(20) NOT NULL,
  `attachment_type` varchar(255) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `user_file_name` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `login_notice_attachments`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `login_notice_attachments`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

-- 18-04-2018 (Projapoti DB)
DROP TABLE IF EXISTS `unit_update_history`;
CREATE TABLE IF NOT EXISTS `unit_update_history` (
`id` int(11) NOT NULL,
  `office_id` int(11) NOT NULL,
  `office_unit_id` int(11) NOT NULL,
  `office_origin_unit_id` int(11) DEFAULT NULL,
  `parent_unit_id` int(11) DEFAULT NULL,
  `old_unit_eng` varchar(255) DEFAULT NULL,
  `old_unit_bng` varchar(255) NOT NULL,
  `unit_eng` varchar(255) DEFAULT NULL,
  `unit_bng` varchar(255) NOT NULL,
  `employee_office_id` int(11) NOT NULL,
  `employee_unit_id` int(11) NOT NULL,
  `employee_designation_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `unit_update_history`
 ADD PRIMARY KEY (`id`);
 ALTER TABLE `unit_update_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

-- 19-04-2018 (Office DB)
ALTER TABLE `potrojari` ADD `approval_visibleName` VARCHAR(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `approval_officer_name`;
ALTER TABLE `potrojari` ADD `officer_visibleName` VARCHAR(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `officer_name`;
ALTER TABLE `potrojari` ADD `approval_visibleDesignation` VARCHAR(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `approval_visibleName`;
ALTER TABLE `potrojari` ADD `officer_visibleDesignation` VARCHAR(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `officer_name`;

-- 7-5-18 (projapoti db)
ALTER TABLE `potrojari_header_settings` ADD `header_photo` VARCHAR(255) NULL AFTER `write_unit`;

-- 10-05-2018 (office db)
ALTER TABLE `nothi_potros` DROP `dispatch_date`;

-- 13-05-2018 (projapoti DB)
CREATE TABLE `projapoti_db`.`office_custom_layers` ( `id` BIGINT NOT NULL AUTO_INCREMENT , `name` VARCHAR(250) NOT NULL , `created_by` BIGINT NULL , `modified_by` BIGINT NULL , `created` DATETIME NOT NULL , `modified` DATETIME NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

ALTER TABLE `offices` ADD `custom_layer_id` INT NULL AFTER `office_layer_id`;

-- 28-05-2018 (projapoti DB)
ALTER TABLE `office_layers` ADD `custom_layer_id` INT NULL AFTER `layer_name_bng`;

-- 29-05-2018 (office db)

ALTER TABLE `potrojari` CHANGE `meta_data` `meta_data` VARCHAR(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;
ALTER TABLE `potrojari_attachments` CHANGE `meta_data` `meta_data` VARCHAR(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;
ALTER TABLE `dak_attachments` CHANGE `meta_data` `meta_data` VARCHAR(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;
ALTER TABLE `dak_daptoriks` CHANGE `meta_data` `meta_data` VARCHAR(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;
ALTER TABLE `nothi_potros` CHANGE `meta_data` `meta_data` VARCHAR(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;
ALTER TABLE `nothi_potro_attachments` CHANGE `meta_data` `meta_data` VARCHAR(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;

ALTER TABLE `nothi_note_attachments` DROP INDEX `note_no_2`;
ALTER TABLE `nothi_parts` DROP INDEX `nothi_types_id_2`;

-- 28-05-2018 (Nothi Access DB)
ALTER TABLE `potrojari_groups_users` ADD `officer_mobile` VARCHAR(20) NULL AFTER `officer_email`;

-- 06-05-2018 (Office DB)
ALTER TABLE dak_movements DROP INDEX from_officer_designation_id;
ALTER TABLE dak_movements DROP INDEX dak_type;
ALTER TABLE dak_movements DROP INDEX dak_type_2;
ALTER TABLE `dak_movements` ADD INDEX( `dak_type`, `dak_id`);
ALTER TABLE `dak_movements` ADD INDEX(`from_officer_designation_id`);

-- 10-06-2018 (Nothi Access DB)

DROP TABLE IF EXISTS `protikolpo_log`;
CREATE TABLE `protikolpo_log` (
  `id` int(11) NOT NULL,
  `protikolpo_id` int(11) NOT NULL,
  `protikolpo_start_date` datetime NOT NULL,
  `protikolpo_end_date` datetime DEFAULT NULL,
  `protikolpo_ended_by` int(11) DEFAULT NULL,
  `employee_office_id_from_name` varchar(255) DEFAULT NULL,
  `employee_office_id_to_name` varchar(255) DEFAULT NULL,
  `protikolpo_status` tinyint(5) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `protikolpo_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employee_office_id_from` (`employee_office_id_from_name`),
  ADD KEY `employee_office_id_to` (`employee_office_id_to_name`),
  ADD KEY `protikolpo_id` (`protikolpo_id`);

ALTER TABLE `protikolpo_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

-- 27-06-2018 (Nothi Access DB)
ALTER TABLE `protikolpo_settings` ADD `is_show_acting` INT(0) NOT NULL AFTER `end_date`;

-- 01-07-2018 (Projapoti DB)
ALTER TABLE `office_origin_unit_organograms` CHANGE `short_name_eng` `short_name_eng` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL, CHANGE `short_name_bng` `short_name_bng` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL, CHANGE `designation_level` `designation_level` INT(4) NOT NULL DEFAULT '0', CHANGE `designation_sequence` `designation_sequence` INT(4) NOT NULL DEFAULT '0', CHANGE `status` `status` TINYINT(1) NOT NULL DEFAULT '0', CHANGE `created_by` `created_by` INT(11) NULL, CHANGE `modified_by` `modified_by` INT(11) NULL, CHANGE `created` `created` DATETIME NULL, CHANGE `modified` `modified` DATETIME NULL;
ALTER TABLE `office_origin_unit_organograms` CHANGE `superior_unit_id` `superior_unit_id` INT(11) NULL, CHANGE `superior_designation_id` `superior_designation_id` INT(11) NULL;

ALTER TABLE `office_unit_organograms` CHANGE `superior_unit_id` `superior_unit_id` INT(11) NULL, CHANGE `superior_designation_id` `superior_designation_id` INT(11) NULL, CHANGE `ref_sup_origin_unit_desig_id` `ref_sup_origin_unit_desig_id` INT(11) NULL, CHANGE `ref_sup_origin_unit_id` `ref_sup_origin_unit_id` INT(11) NULL, CHANGE `designation_eng` `designation_eng` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL, CHANGE `designation_bng` `designation_bng` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL, CHANGE `short_name_eng` `short_name_eng` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL, CHANGE `short_name_bng` `short_name_bng` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL, CHANGE `designation_level` `designation_level` INT(4) NOT NULL DEFAULT '0', CHANGE `designation_sequence` `designation_sequence` INT(4) NOT NULL DEFAULT '0', CHANGE `status` `status` TINYINT(1) NOT NULL DEFAULT '0', CHANGE `created_by` `created_by` INT(11) NULL, CHANGE `modified_by` `modified_by` INT(11) NULL, CHANGE `created` `created` DATETIME NULL, CHANGE `modified` `modified` DATETIME NULL;

-- 01-07-2018 (Nothi Access DB)
CREATE TABLE IF NOT EXISTS `monthly_reports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `year` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `month` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attachment_path` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `rank` int(11) DEFAULT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `modified` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 02-07-2018 (Office DB)
ALTER TABLE `potrojari_onulipi` CHANGE `officer_mobile` `officer_mobile` VARCHAR(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;
ALTER TABLE `potrojari_receiver` CHANGE `officer_mobile` `officer_mobile` VARCHAR(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;

-- 03-07-2018 (Projapoti Auth DB)
ALTER TABLE  `api_access_token` DROP INDEX `api_token`;
ALTER TABLE `api_access_token` CHANGE `api_token` `api_token` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;

-- 07-07-2018 (Office DB)
ALTER TABLE `nothi_notes` CHANGE `subject` `subject` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;

-- 12-07-2018 (Office DB)
ALTER TABLE dak_movements DROP INDEX dak_type;
ALTER TABLE `dak_movements` ADD INDEX( `dak_type`, `dak_id`);

-- 17-07-2018 (office db)
ALTER TABLE  `dak_users` DROP INDEX `dak_category`;
ALTER TABLE  `dak_daptoriks` DROP INDEX `uploader_designation_id`;
ALTER TABLE  `dak_users` ADD INDEX (  `dak_category` );
ALTER TABLE  `dak_daptoriks` ADD INDEX (  `uploader_designation_id` );


-- 19-07-2018 (Nothi Access db)
ALTER TABLE `protikolpo_settings` CHANGE `is_show_acting` `is_show_acting` INT(11) NULL;

-- 26-07-2018 (Nothi Report db)
CREATE TABLE `nothi_reports`.`domain_reports` ( `id` INT NOT NULL AUTO_INCREMENT , `office_id` INT NOT NULL , `url` VARCHAR(128) NOT NULL , `status` VARCHAR(16) NOT NULL , `total_time` VARCHAR(16) NOT NULL , `connect_time` VARCHAR(16) NOT NULL , `created_at` TIMESTAMP on update CURRENT_TIMESTAMP NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

-- 26-07-2018 (Projapoti DB)
ALTER TABLE `potrojari_header_settings` ADD `potrojari_head_img` LONGTEXT NULL AFTER `potrojari_head_eng`;

-- 02-08-2018 (office db)
ALTER TABLE `potrojari` ADD `cloned_potrojari_id` INT UNSIGNED NULL AFTER `device_id`;

-- 08-08-2018 (office db)
ALTER TABLE `portal_publishes` ADD `publish_type_category` VARCHAR(255) NULL AFTER `publish_type`;

-- 26-08-2018 (Nothi Report DB)
CREATE TABLE `nothi_reports`.`report_marking` ( `id` INT NOT NULL , `dak_inbox` INT NOT NULL DEFAULT '0' , `dak_outbox` INT NOT NULL DEFAULT '0' , `dak_nothijat` INT NOT NULL DEFAULT '0' , `dak_nothivukto` INT NOT NULL DEFAULT '0' , `dak_nisponno` INT NOT NULL DEFAULT '0' , `dak_onisponno` INT NOT NULL DEFAULT '0' , `nothi_self_note` INT NOT NULL DEFAULT '0' , `nothi_dak_note` INT NOT NULL DEFAULT '0' , `nothi_note_niponnon` INT NOT NULL DEFAULT '0' , `nothi_potrojari_niponno` INT NOT NULL DEFAULT '0' , `nothi_onisponno` INT NOT NULL DEFAULT '0' , `nothi_nisponno` INT NOT NULL DEFAULT '0' , `potrojari` INT NOT NULL DEFAULT '0' , `potrojari_nisponno_internal` INT NOT NULL DEFAULT '0' , `potrojari_nisponno_external` INT NOT NULL DEFAULT '0' , `created` TIMESTAMP NULL DEFAULT NULL , `modified` TIMESTAMP on update CURRENT_TIMESTAMP NULL DEFAULT NULL , `device_id` VARCHAR(50) NULL DEFAULT NULL , `device_type` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL , `created_by` INT NOT NULL DEFAULT '0' , PRIMARY KEY (`id`)) ENGINE = InnoDB;

ALTER TABLE `report_marking` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `report_marking` ADD `status` TINYINT(2) NOT NULL DEFAULT '0' AFTER `id`;

ALTER TABLE `report_marking` CHANGE `nothi_note_niponnon` `nothi_note_niponno` INT(11) NOT NULL DEFAULT '0';

CREATE TABLE `nothi_reports`.`report_set` ( `id` INT NOT NULL AUTO_INCREMENT , `name` VARCHAR(250) NOT NULL , `created_by` INT NULL , `modified_by` INT NULL , `created` TIMESTAMP  NULL DEFAULT NULL  , `modified` TIMESTAMP on update CURRENT_TIMESTAMP NOT NULL , `device_id` VARCHAR(50) NULL , `device_type` VARCHAR(100) NULL , `offices` TEXT NULL DEFAULT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

RENAME TABLE `nothi_reports`.`report_set` TO `nothi_reports`.`report_category`;


-- 03-09-2018 (office db)
ALTER TABLE `dak_nagoriks` ADD `application_origin` VARCHAR(50) NULL DEFAULT 'nothi' AFTER `uploader_designation_id`;
ALTER TABLE `dak_attachments` ADD `file_custom_name` VARCHAR(200) NULL AFTER `file_name`;
ALTER TABLE `dak_nagoriks` CHANGE `created_by` `created_by` INT(10) NULL;
ALTER TABLE `nothi_potro_attachments` ADD `application_origin` VARCHAR(50) NULL DEFAULT 'nothi' AFTER `status`;
ALTER TABLE `dak_nagoriks` ADD `application_meta_data` TEXT NULL AFTER `application_origin`;
ALTER TABLE `nothi_potros` ADD `application_origin` VARCHAR(50) NULL DEFAULT 'nothi' AFTER `is_deleted`;
ALTER TABLE `nothi_potros` ADD `application_meta_data` TEXT NULL AFTER `application_origin`;

-- 03-09-2018 (projapoti db)
-- disable all event except only InboxDak & InboxNothi
DELETE FROM `notification_settings` WHERE `employee_id` = 0 AND (event_id !=6 and event_id!=10);
-- for one office add office_id condition

-- 03-09-2018 (office db)
ALTER TABLE `nothi_masters` CHANGE `subject` `subject` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;

-- training done --

-- 18-09-2018 Projapoti DB
ALTER TABLE `users` CHANGE `is_email_verified` `is_email_verified` TINYINT(4) NULL, CHANGE `email_verify_code` `email_verify_code` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL, CHANGE `verification_date` `verification_date` DATE NULL, CHANGE `ssn` `ssn` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL, CHANGE `force_password_change` `force_password_change` TINYINT(1) NULL DEFAULT '1', CHANGE `last_password_change` `last_password_change` DATETIME NULL DEFAULT NULL, CHANGE `created_by` `created_by` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL;

-- 17-09-2018 (Nothi Report db)
ALTER TABLE `report_category` ADD `serial` INT NOT NULL DEFAULT '0' AFTER `offices`;

-- Previous alter goes to live server

-- 19-10-2018 (Nothi Access DB)
CREATE TABLE `nothi_access_db`.`ds_user_info` ( `id` BIGINT NOT NULL AUTO_INCREMENT , `username` VARCHAR(250) NOT NULL , `employee_record_id` BIGINT NOT NULL , `soft_token` VARCHAR(250) NOT NULL , `expire_time` DATETIME NOT NULL , `created` DATETIME NOT NULL , `modified` TIMESTAMP on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP , `device_id` VARCHAR(50) NULL , `device_type` VARCHAR(100) NULL , PRIMARY KEY (`id`), INDEX (`username`), INDEX (`employee_record_id`)) ENGINE = InnoDB;

ALTER TABLE `ds_user_info` CHANGE `expire_time` `expired_at` DATETIME NOT NULL;


 -- 31-10-2018 (Office db)
 ALTER TABLE `nothi_masters` ADD INDEX(`nothi_no`);