UPDATE `module_infos` SET `name_bn` = 'দপ্তর' WHERE `module_infos`.`id` = 2;
ALTER TABLE `nothi_notes` ADD `potrojari` TINYINT NOT NULL DEFAULT '0' AFTER `is_potrojari`, ADD `potrojari_id` BIGINT(20) NOT NULL DEFAULT '0' AFTER `potrojari`, ADD `potrojari_status` VARCHAR(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `potrojari_id`;
CREATE TABLE IF NOT EXISTS `potrojari_versions` (
  `id` bigint(20) NOT NULL,
  `potrojari_id` bigint(20) NOT NULL,
  `nothi_master_id` bigint(20) NOT NULL,
  `updated_content` longtext CHARACTER SET utf8,
  `office_id` bigint(20) NOT NULL,
  `officer_id` bigint(20) NOT NULL,
  `officer_name` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `officer_unit_id` bigint(20) NOT NULL,
  `office_organogram_id` bigint(20) NOT NULL,
  `designation_label` varchar(250) CHARACTER SET utf8 NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `potrojari_versions`
--
ALTER TABLE `potrojari_versions`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `potrojari_versions`
--
ALTER TABLE `potrojari_versions`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;


-- 14-12-2015
ALTER TABLE `dak_nagoriks` ADD `sender_name` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `name_bng`;


-- 17-12-2015
ALTER TABLE `potrojari` ADD `dak_id` BIGINT(20) NOT NULL DEFAULT '0' AFTER `sarok_no`;


-- 23-12-2015
ALTER TABLE `potrojari_onulipi` ADD `dak_id` BIGINT(20) NOT NULL DEFAULT '0' AFTER `potro_type`;


DROP TABLE IF EXISTS `potrojari_receiver`;
CREATE TABLE IF NOT EXISTS `potrojari_receiver` (
  `id` bigint(20) NOT NULL,
  `potrojari_id` bigint(20) NOT NULL,
  `nothi_master_id` bigint(20) NOT NULL,
  `nothi_notes_id` bigint(20) NOT NULL,
  `nothi_potro_id` bigint(20) NOT NULL,
  `potro_type` int(11) NOT NULL,
  `dak_id` bigint(20) NOT NULL DEFAULT '0',
  `sarok_no` varchar(255) NOT NULL,
  `office_id` int(11) NOT NULL,
  `officer_id` int(11) NOT NULL,
  `office_name` varchar(255) NOT NULL,
  `officer_name` varchar(255) DEFAULT NULL,
  `office_unit_id` int(11) NOT NULL,
  `office_unit_name` varchar(255) NOT NULL,
  `officer_designation_id` int(11) NOT NULL,
  `officer_designation_label` varchar(255) DEFAULT NULL,
  `potrojari_date` datetime NOT NULL,
  `potro_subject` varchar(255) NOT NULL,
  `potro_security_level` varchar(64) NOT NULL,
  `potro_priority_level` varchar(64) NOT NULL,
  `potro_description` text,
  `receiving_office_id` int(11) DEFAULT NULL,
  `receiving_office_unit_id` int(11) DEFAULT NULL,
  `receiving_office_unit_name` varchar(255) DEFAULT NULL,
  `receiving_officer_id` int(11) DEFAULT NULL,
  `receiving_officer_designation_id` int(11) DEFAULT NULL,
  `receiving_officer_designation_label` varchar(255) DEFAULT NULL,
  `receiving_officer_name` varchar(255) DEFAULT NULL,
  `potro_status` varchar(64) NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified_by` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `potrojari_receiver`
--
ALTER TABLE `potrojari_receiver`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `potrojari_receiver`
--
ALTER TABLE `potrojari_receiver`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;


ALTER TABLE `nothi_potros` CHANGE `dak_id` `dak_id` INT(11) NULL;

-- 27-12-2015
ALTER TABLE `potrojari_receiver` ADD `receiving_office_name` VARCHAR(200) NULL AFTER `receiving_office_id`;
ALTER TABLE `potrojari_onulipi`  ADD `receiving_office_name` VARCHAR(200) NULL  AFTER `receiving_office_id`;


-- 30-12-2015

--
-- Table structure for table `office_front_desk`
--

DROP TABLE IF EXISTS `office_front_desk`;
CREATE TABLE IF NOT EXISTS `office_front_desk` (
  `id` bigint(20) NOT NULL,
  `office_id` bigint(20) NOT NULL,
  `office_name` varchar(200) CHARACTER SET utf8 NOT NULL,
  `office_address` varchar(200) CHARACTER SET utf8 NOT NULL,
  `office_unit_id` bigint(20) NOT NULL,
  `office_unit_name` varchar(200) CHARACTER SET utf8 NOT NULL,
  `office_unit_organogram_id` bigint(20) NOT NULL,
  `designation_label` varchar(200) CHARACTER SET utf8 NOT NULL,
  `officer_id` bigint(20) NOT NULL,
  `officer_name` varchar(200) CHARACTER SET utf8 NOT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `office_front_desk`
--

INSERT INTO `office_front_desk` (`id`, `office_id`, `office_name`, `office_address`, `office_unit_id`, `office_unit_name`, `office_unit_organogram_id`, `designation_label`, `officer_id`, `officer_name`, `created_by`, `created`, `modified`) VALUES
(1, 11, 'জেলা প্রশাসকের কার্যালয়', 'জেলা প্রশাসকের কার্যালয় জামালপুর, বাংলাদেশ', 321, 'সাধারন শাখা', 779, 'অফিস সহকারী কাম কম্পিউটার অপারেটর', 52, 'কোহিনূর বেগম', NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `office_front_desk`
--
ALTER TABLE `office_front_desk`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `office_front_desk`
--
ALTER TABLE `office_front_desk`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;


ALTER TABLE `dak_movements` CHANGE `from_office_unit_id` `from_office_unit_id` INT(11) NOT NULL DEFAULT '0', CHANGE `from_office_unit_name` `from_office_unit_name` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL, CHANGE `from_officer_designation_id` `from_officer_designation_id` INT(11) NOT NULL DEFAULT '0';

ALTER TABLE `dak_movements` ADD `dak_dagorik_type` TINYINT(2) NOT NULL DEFAULT '1' AFTER `dak_type`;
ALTER TABLE `dak_nagoriks` CHANGE `dak_received_no` `dak_received_no` CHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;


-- 05-01-2016
ALTER TABLE `potrojari_templates` CHANGE `status` `status` TINYINT(5) NOT NULL DEFAULT '1';

-- 07-01-2016
ALTER TABLE `potrojari` ADD `is_summary_nothi` TINYINT(2) NOT NULL DEFAULT '0' AFTER `potro_status`;
ALTER TABLE `potrojari` ADD `potro_cover` LONGTEXT CHARACTER SET utf16 COLLATE utf16_general_ci NULL AFTER `potro_priority_level`;
ALTER TABLE `potrojari` CHANGE `potro_description` `potro_description` LONGTEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;


-- 08-01-2016
ALTER TABLE `employee_offices` ADD `summary_nothi_post_type` TINYINT(9) NOT NULL DEFAULT '0' AFTER `is_default_role`;
ALTER TABLE `employee_offices` CHANGE `summary_nothi_post_type` `summary_nothi_post_type` TINYINT(9) NOT NULL DEFAULT '0' COMMENT '0:normal user, 1: মন্ত্রনালয়ের সচিব, 2: প্রতিমন্ত্রী, 3: মন্ত্রী, 4: প্রধানমন্ত্রী, 5: রাষ্ট্রপতি';


-- 09-01-2016
ALTER TABLE `dak_daptoriks` ADD `is_summary_nothi` TINYINT(2) NOT NULL DEFAULT '0' AFTER `dak_status`;
ALTER TABLE `dak_movements` ADD `is_summary_nothi` TINYINT NOT NULL DEFAULT '0' AFTER `dak_actions`;
ALTER TABLE `dak_users` ADD `is_summary_nothi` TINYINT(2) NOT NULL DEFAULT '0' AFTER `dak_category`;
ALTER TABLE `dak_user_actions` ADD `is_summary_nothi` TINYINT(2) NOT NULL DEFAULT '0' AFTER `dak_action`;
ALTER TABLE `potrojari_versions` ADD `updated_cover` LONGTEXT NULL DEFAULT NULL AFTER `nothi_master_id`;
ALTER TABLE `dak_daptoriks` CHANGE `dak_description` `dak_description` LONGTEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL;
ALTER TABLE `nothi_potro_attachments` CHANGE `content_body` `content_body` LONGTEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;
ALTER TABLE `nothi_potro_attachments` ADD `potro_cover` LONGTEXT NULL AFTER `file_dir`;
ALTER TABLE `dak_daptoriks` ADD `dak_cover` LONGTEXT NULL AFTER `dak_priority_level`;
ALTER TABLE `dak_attachments` ADD `content_cover` LONGTEXT NULL AFTER `file_dir`;
ALTER TABLE `potrojari_attachments` ADD `content_cover` LONGTEXT NULL AFTER `attachment_description`;

-- 10-01-2016
ALTER TABLE `nothi_potro_attachments` ADD `is_summary_nothi` TINYINT(2) NOT NULL DEFAULT '0' AFTER `potrojari_status`;
ALTER TABLE `potrojari_attachments` ADD `is_summary_nothi` TINYINT(2) NULL DEFAULT '0' AFTER `content_body`;
ALTER TABLE `dak_attachments` ADD `is_summary_nothi` TINYINT(2) NOT NULL DEFAULT '0' AFTER `content_body`;
ALTER TABLE `dak_attachments` CHANGE `content_body` `content_body` LONGTEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;
ALTER TABLE `nothi_potro_attachments` ADD `is_approved` TINYINT(2) NOT NULL DEFAULT '0' AFTER `is_summary_nothi`;

-- 17-01-2016

ALTER TABLE `nothi_dak_potro_maps`  ADD `nothi_part_no` BIGINT(20)  NOT NULL DEFAULT '1'  AFTER `nothi_masters_id`;
ALTER TABLE `nothi_masters_dak_map` ADD `nothi_part_no` BIGINT(20)  NOT NULL DEFAULT '1' AFTER `nothi_masters_id`;
ALTER TABLE `nothi_master_current_users` ADD `nothi_part_no` BIGINT(20)  NOT NULL DEFAULT '1' AFTER `nothi_master_id`;
ALTER TABLE `nothi_master_movements` ADD `nothi_part_no` BIGINT(20)  NOT NULL DEFAULT '1' AFTER `nothi_master_id`;
ALTER TABLE `nothi_notes` ADD `nothi_part_no` BIGINT(20) NOT NULL DEFAULT '1' AFTER `nothi_master_id`;
ALTER TABLE `nothi_note_attachments` ADD `nothi_part_no` BIGINT(20) NOT NULL DEFAULT '1' AFTER `nothi_master_id`;
ALTER TABLE `nothi_note_sheets` ADD `nothi_part_no` BIGINT(20) NOT NULL DEFAULT '1' AFTER `nothi_master_id`;
ALTER TABLE `nothi_note_signatures` ADD `nothi_part_no` BIGINT(20) NOT NULL DEFAULT '1' AFTER `nothi_master_id`;
ALTER TABLE `nothi_potros` ADD `nothi_part_no` BIGINT(20) NOT NULL DEFAULT '1' AFTER `nothi_master_id`;
ALTER TABLE `nothi_potro_attachments` ADD `nothi_part_no` BIGINT(20) NOT NULL DEFAULT '1' AFTER `nothi_master_id`;
ALTER TABLE `potrojari` ADD `nothi_part_no` BIGINT(20) NOT NULL DEFAULT '1' AFTER `nothi_master_id`;
ALTER TABLE `potrojari_onulipi` ADD `nothi_part_no` BIGINT(20) NOT NULL DEFAULT '1' AFTER `nothi_master_id`;
ALTER TABLE `potrojari_receiver` ADD `nothi_part_no` BIGINT(20) NOT NULL DEFAULT '1' AFTER `nothi_master_id`;
ALTER TABLE `potrojari_versions` ADD `nothi_part_no` BIGINT(20) NOT NULL DEFAULT '1' AFTER `nothi_master_id`;
ALTER TABLE `potro_flags` ADD `nothi_part_no` BIGINT(20) NOT NULL DEFAULT '1' AFTER `nothi_master_id`;
ALTER TABLE `summary_nothi_users` ADD `nothi_part_no` BIGINT(20) NOT NULL DEFAULT '1' AFTER `nothi_master_id`;



--
-- Table structure for table `nothi_parts`
--

DROP TABLE IF EXISTS `nothi_parts`;
CREATE TABLE `nothi_parts` (
  `id` bigint(20) NOT NULL,
  `nothi_masters_id` bigint(20) NOT NULL,
  `nothi_part_no` bigint(20) NOT NULL DEFAULT '1',
  `nothi_part_no_bn` varchar(20) NOT NULL DEFAULT '১',
  `office_id` bigint(20) NOT NULL,
  `office_units_id` int(11) NOT NULL,
  `office_units_organogram_id` int(11) NOT NULL,
  `nothi_types_id` int(11) NOT NULL,
  `nothi_no` varchar(64) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `nothi_created_date` date NOT NULL,
  `description` text,
  `nothi_class` tinyint(1) NOT NULL DEFAULT '4',
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `nothi_parts`
--
ALTER TABLE `nothi_parts`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `nothi_parts`
--
ALTER TABLE `nothi_parts`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;


-- 21-01-2016
ALTER TABLE `nothi_master_permissions` ADD `visited` TINYINT(2) NOT NULL DEFAULT '0' AFTER `privilige_type`;
ALTER TABLE `nothi_master_permissions` ADD `nothi_part_no` BIGINT(20) NOT NULL AFTER `nothi_masters_id`;


-- 24-01-2016
ALTER TABLE `potrojari_attachments` ADD `is_inline` TINYINT(2) NOT NULL DEFAULT '0' AFTER `is_summary_nothi`;

-- 26-01-2016
ALTER TABLE `dak_daptoriks` ADD INDEX(`sender_office_id`);
ALTER TABLE `dak_daptoriks` ADD INDEX(`receiving_office_id`);
ALTER TABLE `dak_movements` ADD INDEX(`dak_type`);
ALTER TABLE `dak_movements` ADD INDEX(`to_office_id`);
ALTER TABLE `dak_movements` ADD INDEX(`dak_type`);
ALTER TABLE `dak_users` ADD INDEX(`to_office_id`);
ALTER TABLE `nothi_masters` ADD INDEX( `office_id`);
ALTER TABLE `nothi_master_movements` ADD INDEX(`nothi_part_no`);
ALTER TABLE `nothi_master_movements` ADD INDEX(`from_office_id`);
ALTER TABLE `nothi_master_movements` ADD INDEX(`to_office_id`);
ALTER TABLE `nothi_master_permissions` ADD INDEX(`office_id`);
ALTER TABLE `nothi_master_permissions` ADD INDEX(`nothi_part_no`);
ALTER TABLE `nothi_notes` ADD INDEX(`nothi_part_no`);
ALTER TABLE `nothi_parts` ADD INDEX( `office_id`);
ALTER TABLE `nothi_potro_attachments` ADD INDEX(`nothi_part_no`);
ALTER TABLE `notification_messages` ADD INDEX(`to_user_id`);
ALTER TABLE `potrojari` ADD INDEX(`nothi_part_no`);
ALTER TABLE `office_unit_seals` ADD INDEX(`office_id`);

ALTER TABLE `potrojari_receiver` ADD `receiving_officer_email` VARCHAR(150) NULL AFTER `receiving_officer_name`;
ALTER TABLE `potrojari_onulipi` ADD `receiving_officer_email` VARCHAR(150) NULL AFTER `receiving_officer_name`;

-- 28-01-2016
ALTER TABLE `employee_offices` ADD INDEX( `office_id`);
ALTER TABLE `potrojari` ADD INDEX(`officer_id`);

-- 01-02-2016
ALTER TABLE `office_units` ADD INDEX(`office_id`);
ALTER TABLE `office_unit_organograms` ADD INDEX(`office_id`);



-- 04-02-2016
ALTER TABLE `potrojari_receiver` ADD `is_sent` TINYINT(2) NOT NULL DEFAULT '0' AFTER `potro_status`;
ALTER TABLE `potrojari_onulipi` ADD `is_sent` TINYINT(2) NOT NULL DEFAULT '0' AFTER `potro_status`;

-- 06-02-2016
ALTER TABLE `employee_records` CHANGE `is_cadre` `is_cadre` TINYINT(9) NOT NULL;
ALTER TABLE `nothi_potro_attachments` ADD `nothijato` TINYINT(2) NOT NULL DEFAULT '0' AFTER `nothi_potro_id`;
ALTER TABLE `nothi_potros` ADD `nothijato` TINYINT(2) NOT NULL DEFAULT '0' AFTER `dak_type`;
ALTER TABLE `nothi_masters_dak_map` CHANGE `dak_type` `dak_type` VARCHAR(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'Daptorik';

-- 10-02-2016
ALTER TABLE `office_units` ADD `sarok_no_start` INT(4) NOT NULL DEFAULT '0' AFTER `unit_nothi_code`;

-- 14-02-2016
ALTER TABLE `potrojari_attachments` ADD `potro_id` BIGINT(20) NULL DEFAULT '0' AFTER `is_inline`;


-- 16-02-2016
ALTER TABLE `nothi_types` ADD `type_last_number` BIGINT NOT NULL DEFAULT '0' AFTER `type_code`;

-- 24-02-2016
ALTER TABLE `potrojari` ADD `sovapoti_office_id` INT NULL AFTER `officer_designation_label`, 
ADD `sovapoti_officer_id` INT NULL AFTER `sovapoti_office_id`, ADD `sovapoti_officer_designation_id` INT NULL AFTER `sovapoti_officer_id`, ADD `sovapoti_officer_designation_label` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `sovapoti_officer_designation_id`, ADD `sovapoti_officer_name` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `sovapoti_officer_designation_label`;

-- 03-03-2016
ALTER TABLE `nothi_master_current_users` ADD `is_archive` TINYINT(2) NOT NULL DEFAULT '0' AFTER `nothi_part_no`;

-- 04-03-2016
ALTER TABLE `potrojari` ADD `can_potrojari` TINYINT(2) NOT NULL DEFAULT '0' AFTER `dak_id`;



-- 10-03-2016
ALTER TABLE `dak_nagoriks` CHANGE `name_eng` `name_eng` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL, CHANGE `birth_registration_number` `birth_registration_number` VARCHAR(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL, CHANGE `passport` `passport` VARCHAR(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL, CHANGE `father_name` `father_name` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL, CHANGE `mother_name` `mother_name` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL, CHANGE `address` `address` VARCHAR(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL, CHANGE `parmanent_address` `parmanent_address` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL, CHANGE `mobile_no` `mobile_no` VARCHAR(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL;

-- 22-03-2016
ALTER TABLE `dak_movements` CHANGE `dak_priority` `dak_priority` TINYINT(9) NOT NULL;
ALTER TABLE `users` ADD `is_admin` TINYINT(2) NOT NULL DEFAULT '0' AFTER `user_role_id`;


-- 11-04-2016
ALTER TABLE `dak_attachment_comment` CHANGE `fonts` `fonts` VARCHAR(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT 'italic 14px "Nikosh"';
ALTER TABLE `dak_attachment_comment` CHANGE `position_y` `position_y` DOUBLE NOT NULL DEFAULT '20';

-- 12-04-2016
ALTER TABLE `dak_users` ADD `is_archive` TINYINT(2) NOT NULL DEFAULT '0' AFTER `attention_type`;

-- 15-04-2016

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;


DROP TABLE IF EXISTS `error_log`;
CREATE TABLE `error_log` (
  `id` bigint(20) NOT NULL,
  `request_ip` varchar(50) NOT NULL,
  `office_id` bigint(20) DEFAULT NULL,
  `request_url` varchar(255) DEFAULT NULL,
  `error_code` int(11) NOT NULL,
  `error_msg` text,
  `error_title` varchar(255) DEFAULT NULL,
  `error_status` tinyint(2) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `error_log`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `error_log`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;


ALTER TABLE `potrojari_attachments` ADD `potro_id` BIGINT NULL AFTER `is_inline`;