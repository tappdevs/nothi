-- 02-08-2016 Propoti DB
ALTER TABLE `office_origin_units` ADD `unit_level` INT NOT NULL DEFAULT '0' AFTER `parent_unit_id`;
ALTER TABLE `office_units` ADD `unit_level` INT NOT NULL DEFAULT '0' AFTER `unit_nothi_code`;

-- 14-08-2016 (Propoti DB)
DROP TABLE IF EXISTS `notification_settings`;
CREATE TABLE `notification_settings` (
  `id` bigint(20) NOT NULL,
  `event_id` bigint(20) NOT NULL,
  `employee_id` bigint(20) NOT NULL,
  `office_id` bigint(20) NOT NULL,
  `system` tinyint(2) NOT NULL DEFAULT '1',
  `email` tinyint(2) NOT NULL DEFAULT '1',
  `sms` tinyint(2) NOT NULL DEFAULT '0',
  `mobile_app` tinyint(1) NOT NULL DEFAULT '1',
  `is_notified` tinyint(1) NOT NULL DEFAULT '0',
  `last_request` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
ALTER TABLE `notification_settings`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `notification_settings`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

-- Office DB
ALTER TABLE `notification_messages` ADD `mobile_app` TINYINT(1) NOT NULL DEFAULT '1' AFTER `sms`, ADD `is_notified` TINYINT(1) NOT NULL DEFAULT '0' AFTER `mobile_app`;
UPDATE `notification_messages` SET  `is_notified` =1;

-- 21-08-16 (For Office DB)
ALTER TABLE `dak_users` ADD `is_nisponno` TINYINT(2) NOT NULL DEFAULT '0' AFTER `is_summary_nothi`;
UPDATE dak_users set is_nisponno = 1 where dak_category = 'NothiJato';

-- 22-08-16 (For Office DB)
ALTER TABLE `nothi_dak_potro_maps` ADD `is_nisponno` TINYINT(2) NOT NULL DEFAULT '0' AFTER `dak_type`;

-- 23-08-16 (For Nothi Acees DB)
ALTER TABLE `nothi_master_current_users` ADD `created` DATETIME NOT NULL AFTER `is_new`, ADD `modified` DATETIME NOT NULL AFTER `created`;

-- 25-08-16 (Office DB)
update dak_movements set dak_movements.operation_type = 'Forward' WHERE dak_movements.sequence = 2 and dak_movements.operation_type = 'Sent' AND dak_type = 'Daptorik';

-- 28-08-16 (Projapoti DB)
update notification_settings set sms = 0;

-- 28-08-16 (nothi_reports DB)
CREATE TABLE `nothi_reports`.`monitor_offices` ( 
    `id` INT NOT NULL AUTO_INCREMENT ,  `office_id` INT NOT NULL ,  `totaluser` INT NOT NULL ,  `todaylogin` INT NOT NULL , 
    `yesterdayinbox` INT NOT NULL , `yesterdayoutbox` INT NOT NULL ,`yesterdaynothijat` INT NOT NULL , 
    `yesterdaynothivukto` INT NOT NULL,  `totalnispoonodak` INT NOT NULL , `totalonisponnodak` INT NOT NULL ,
    `yesterdayselfnote` INT NOT NULL , `yesterdaydaksohonote` INT NOT NULL ,`yesterdaypotrojari` INT NOT NULL ,
    `totalnisponnonote` INT NOT NULL ,  `totalonisponnonote` INT NOT NULL , `totalnisppnno` INT NOT NULL , 
    `totalonisponno` INT NOT NULL , `created` DATETIME NOT NULL ,  `modified` DATETIME NOT NULL , 
    `updated` DATE NOT NULL , `status` TINYINT NOT NULL,
     PRIMARY KEY (id)
) ENGINE = InnoDB;

ALTER TABLE `monitor_offices` CHANGE `totalnisppnno` `totalnisponno` INT(11) NOT NULL;

ALTER TABLE `monitor_offices` CHANGE `status` `status` TINYINT(1) NOT NULL DEFAULT '0';

-- 30-08-2016 (nothi_reports DB)
ALTER TABLE `monitor_offices`
  DROP `totaluser`,
  DROP `todaylogin`;

ALTER TABLE `monitor_offices` CHANGE `totalnispoonodak` `totalnisponnodak` INT(11) NOT NULL;

-- 30-08-16 (Office DB)
ALTER TABLE `dak_movements` CHANGE `from_office_id` `from_office_id` INT(11) NULL DEFAULT '0', CHANGE `from_office_name` `from_office_name` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL, CHANGE `from_office_unit_id` `from_office_unit_id` INT(11) NULL DEFAULT '0', CHANGE `from_office_unit_name` `from_office_unit_name` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL, CHANGE `from_office_address` `from_office_address` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL, CHANGE `from_officer_id` `from_officer_id` INT(11) NULL DEFAULT '0', CHANGE `from_officer_name` `from_officer_name` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL, CHANGE `from_officer_designation_id` `from_officer_designation_id` INT(11) NULL DEFAULT '0', CHANGE `from_officer_designation_label` `from_officer_designation_label` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;

-- 31-08-16 (Projapoti DB) - This is already in Live server, so don't run again
-- ALTER TABLE `users` ADD UNIQUE(`username`);
-- ALTER TABLE `user_login_history` ADD INDEX(`office_id`);
-- ALTER TABLE `user_login_history` ADD INDEX(`employee_record_id`);
-- ALTER TABLE `office_origin_unit_organograms` ADD INDEX(`office_origin_unit_id`);
-- ALTER TABLE `office_origin_units` ADD INDEX(`office_origin_id`);
-- ALTER TABLE `office_origins` ADD INDEX(`office_ministry_id`);
-- ALTER TABLE `office_layers` ADD INDEX(`office_ministry_id`);
-- ALTER TABLE `office_domains` ADD INDEX(`office_id`);
-- ALTER TABLE `office_front_desk` ADD INDEX(`office_id`);
-- ALTER TABLE `offices` ADD INDEX(`office_ministry_id`);
-- ALTER TABLE `offices` ADD INDEX(`office_origin_id`);
-- ALTER TABLE `notification_settings` ADD INDEX(`employee_id`);
-- ALTER TABLE `employee_offices` ADD INDEX(`employee_record_id`);
-- ALTER TABLE `employee_offices` ADD INDEX(`office_id`);
-- ALTER TABLE `dak_tracks` ADD INDEX( `dak_id`, `dak_type`);

-- 31-08-16 (Nothi_reports)
CREATE TABLE `nothi_reports`.`performance_offices` ( `id` INT NOT NULL AUTO_INCREMENT , `office_id` INT NOT NULL , `ministry_id` INT NOT NULL , `layer_id` INT NOT NULL , `origin_id` INT NOT NULL , `ministry_name` INT NOT NULL , `layer_name` INT NOT NULL , `origin_name` INT NOT NULL , `office_name` INT NOT NULL , `inbox` INT NOT NULL , `outbox` INT NOT NULL , `nothijat` INT NOT NULL , `nothivukto` INT NOT NULL , `nisponnodak` INT NOT NULL , `onisponnodak` INT NOT NULL , `selfnote` INT NOT NULL , `daksohonote` INT NOT NULL , `potrojari` INT NOT NULL , `nisponnonote` INT NOT NULL , `onisponnonote` INT NOT NULL , `nisponno` INT NOT NULL , `onisponno` INT NOT NULL , `status` TINYINT NOT NULL DEFAULT '0' , `created` DATETIME NOT NULL , `modified` DATETIME NOT NULL , `record_date` DATE NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

ALTER TABLE `performance_offices` CHANGE `ministry_id` `ministry_id` INT(11) NULL, CHANGE `layer_id` `layer_id` INT(11) NULL, CHANGE `origin_id` `origin_id` INT(11) NULL, CHANGE `ministry_name` `ministry_name` INT(11) NULL, CHANGE `layer_name` `layer_name` INT(11) NULL, CHANGE `origin_name` `origin_name` INT(11) NULL, CHANGE `office_name` `office_name` INT(11) NULL;

-- 01-Sep-2016 (Nothi Reports)
CREATE TABLE `nothi_reports`.`performance_units` ( `id` INT NOT NULL AUTO_INCREMENT , `unit_id` INT NOT NULL , `office_id` INT NULL DEFAULT NULL , `ministry_id` INT NULL DEFAULT NULL , `layer_id` INT NULL DEFAULT NULL , `origin_id` INT NULL DEFAULT NULL , `ministry_name` INT NULL DEFAULT NULL , `layer_name` INT NULL DEFAULT NULL , `origin_name` INT NULL DEFAULT NULL , `office_name` INT NULL DEFAULT NULL , `unit_name` INT NULL , `inbox` INT NOT NULL , `outbox` INT NOT NULL , `nothijat` INT NOT NULL , `nothivukto` INT NOT NULL , `nisponnodak` INT NOT NULL , `onisponnodak` INT NOT NULL , `selfnote` INT NOT NULL , `daksohonote` INT NOT NULL , `potrojari` INT NOT NULL , `nisponnonote` INT NOT NULL , `onisponnonote` INT NOT NULL , `nisponno` INT NOT NULL , `onisponno` INT NOT NULL , `status` TINYINT(2) NOT NULL DEFAULT '0' , `created` DATETIME NOT NULL , `modified` DATETIME NOT NULL , `record_date` DATE NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

ALTER TABLE `performance_offices` CHANGE `ministry_name` `ministry_name` VARCHAR(200) NULL DEFAULT NULL, CHANGE `layer_name` `layer_name` VARCHAR(200) NULL DEFAULT NULL, CHANGE `origin_name` `origin_name` VARCHAR(200) NULL DEFAULT NULL, CHANGE `office_name` `office_name` VARCHAR(200) NULL DEFAULT NULL;

ALTER TABLE `performance_units` CHANGE `ministry_name` `ministry_name` VARCHAR(200) NULL DEFAULT NULL, CHANGE `layer_name` `layer_name` VARCHAR(200) NULL DEFAULT NULL, CHANGE `origin_name` `origin_name` VARCHAR(200) NULL DEFAULT NULL, CHANGE `office_name` `office_name` VARCHAR(200) NULL DEFAULT NULL, CHANGE `unit_name` `unit_name` VARCHAR(200) NULL DEFAULT NULL;

CREATE TABLE `nothi_reports`.`performance_designations` ( `id` INT NOT NULL AUTO_INCREMENT , `designation_id` INT NOT NULL , `unit_id` INT NULL DEFAULT NULL , `office_id` INT NULL DEFAULT NULL , `ministry_id` INT NULL , `layer_id` INT NULL , `origin_id` INT NULL , `ministry_name` VARCHAR(200) NULL , `layer_name` VARCHAR(200) NULL , `origin_name` VARCHAR(200) NULL , `office_name` VARCHAR(200) NULL , `unit_name` VARCHAR(200) NULL , `designation_name` VARCHAR(200) NULL , `inbox` INT NOT NULL , `outbox` INT NOT NULL , `nothijat` INT NOT NULL , `nothivukto` INT NOT NULL , `nisponnodak` INT NOT NULL , `onisponnodak` INT NOT NULL , `selfnote` INT NOT NULL , `daksohonote` INT NOT NULL , `potrojari` INT NOT NULL , `nisponnonote` INT NOT NULL , `onisponnonote` INT NOT NULL , `nisponno` INT NOT NULL , `onisponno` INT NOT NULL , `status` TINYINT(2) NOT NULL , `created` DATETIME NOT NULL , `modified` DATETIME NOT NULL , `record_date` DATE NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

-- 20-9-16 (office db)
ALTER TABLE `potrojari_onulipi` CHANGE `potro_status` `potro_status` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;
ALTER TABLE `potrojari_receiver` CHANGE `potro_status` `potro_status` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;


--01-10-2016 (projapoti db)
ALTER TABLE `employee_records` CHANGE `nid` `nid` VARCHAR(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL, CHANGE `bcn` `bcn` VARCHAR(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL, CHANGE `ppn` `ppn` VARCHAR(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL, CHANGE `gender` `gender` VARCHAR(8) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL, CHANGE `religion` `religion` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL, CHANGE `blood_group` `blood_group` VARCHAR(4) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL, CHANGE `marital_status` `marital_status` VARCHAR(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;

ALTER TABLE `users` ADD `force_password_change` TINYINT(1) NOT NULL DEFAULT '0' AFTER `ssn`, ADD `last_password_change` DATETIME NOT NULL AFTER `force_password_change`;

update users set last_password_change = modified;

--03-10-2016 (office db)
ALTER TABLE `summary_nothi_users` ADD `designation_id` BIGINT NULL DEFAULT '0' AFTER `potrojari_id`;

-- 05-10-2016 (office db)
ALTER TABLE `nothi_notes` ADD `description_bk` TEXT NULL AFTER `note_description`;
UPDATE nothi_notes SET description_bk = note_description;
ALTER TABLE `nothi_notes` ADD `encoded` TINYINT NOT NULL DEFAULT '0' AFTER `description_bk`;

-- 05-10-2016 (Projapoti db)
ALTER TABLE `users` ADD `user_alias` VARCHAR(50) NOT NULL AFTER `password`;
UPDATE users SET user_alias = username; 

-- 14-10-2016 (Office db)
ALTER TABLE `dak_users` ADD `dak_category_old` VARCHAR(20) NULL AFTER `dak_view_status`;
update dak_users set dak_category_old = dak_category;

-- 17-10-2016 (Office db)
ALTER TABLE `nothi_masters_dak_map` ADD `status` TINYINT(2) NOT NULL DEFAULT '1' AFTER `nothi_part_no`;
ALTER TABLE `nothi_potro_attachments` ADD `status` TINYINT(2) NOT NULL DEFAULT '1' AFTER `is_approved`;
ALTER TABLE `nothi_dak_potro_maps` ADD `status` TINYINT(2) NOT NULL DEFAULT '1' AFTER `is_nisponno`;

-- Projapoti db
ALTER TABLE `office_unit_organograms` ADD `designation_description` TEXT NULL AFTER `designation_sequence`;

--- Projapoti DB (23 OCT 2016)
CREATE TABLE `projapoti_db`.`monitoring_privilage` ( `id` INT NOT NULL AUTO_INCREMENT , `designation_id` BIGINT NOT NULL , `created` DATETIME NOT NULL , `modified` DATETIME NOT NULL , PRIMARY KEY (`id`));

-- Projapoti DB (24-10-2016)
ALTER TABLE `employee_records` ADD `nid_valid` TINYINT(1) NOT NULL DEFAULT '0' AFTER `nid`;

-- Office DB
ALTER TABLE `potrojari`  ADD `approval_office_id` BIGINT NOT NULL DEFAULT '0'  AFTER `officer_designation_label`,  ADD `approval_officer_id` BIGINT NOT NULL DEFAULT '0'  AFTER `approval_office_id`,  ADD `approval_office_unit_id` BIGINT NOT NULL DEFAULT '0'  AFTER `approval_officer_id`,  ADD `approval_officer_designation_id` BIGINT NOT NULL DEFAULT '0'  AFTER `approval_office_unit_id`,  ADD `approval_officer_name` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL  AFTER `approval_officer_designation_id`,  ADD `approval_office_unit_name` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL  AFTER `approval_officer_name`,  ADD `approval_officer_designation_label` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL  AFTER `approval_office_unit_name`;
ALTER TABLE `potrojari` ADD `approval_office_name` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `approval_officer_designation_id`;


-- Projapoti DB (09 Nov 2016)

CREATE TABLE `projapoti_db`.`designation_update_history` ( `id` INT NOT NULL AUTO_INCREMENT , `designation_id` INT NOT NULL , `office_id` INT NOT NULL , `office_unit_id` INT NOT NULL , `superior_unit_id` INT NULL , `superior_designation_id` INT NULL , `ref_origin_unit_org_id` INT NULL , `old_designation_eng` VARCHAR(255) NULL , `old_designation_bng` VARCHAR(255) NOT NULL , `designation_eng` VARCHAR(255) NULL , `designation_bng` VARCHAR(255) NOT NULL , `employee_office_id` INT NOT NULL , `employee_unit_id` INT NOT NULL , `employee_designation_id` INT NOT NULL , `created` DATETIME NOT NULL , `modified` DATETIME NOT NULL , `created_by` INT NOT NULL , `modified_by` INT NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

-- Nothi Acess DB (16 Nov 2016)

CREATE TABLE `nothi_access_db`.`nisponno_records` ( `id` INT NOT NULL AUTO_INCREMENT , `nothi_master_id` INT NOT NULL , `nothi_part_no` INT NOT NULL , `type` VARCHAR(100) NOT NULL , `employee_id` INT NOT NULL , `created` DATETIME NOT NULL , `modified` DATETIME NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

ALTER TABLE `nisponno_records` ADD `operation_date` DATE NOT NULL AFTER `employee_id`;

ALTER TABLE `nisponno_records` ADD `nothi_onucched_id` INT NULL AFTER `type`, ADD `nothi_office_id` INT NOT NULL AFTER `nothi_onucched_id`, ADD `office_id` INT NOT NULL AFTER `nothi_office_id`, ADD `unit_id` INT NOT NULL AFTER `office_id`, ADD `designation_id` INT NOT NULL AFTER `unit_id`;

ALTER TABLE `nisponno_records` CHANGE `operation_date` `operation_date` DATETIME NOT NULL;

-- Nothi Reports (20 Nov 2016)

CREATE TABLE `nothi_reports`.`dashboard_offices` ( `id` INT NOT NULL AUTO_INCREMENT ,`office_id` INT NOT NULL , `operation_date` DATE NOT NULL , `dak_inbox` INT NOT NULL , `dak_outbox` INT NOT NULL , `nothivukto` INT NOT NULL , `nothijat` INT NOT NULL , `dak_nisponno` INT NOT NULL , `dak_onisponno` INT NOT NULL , `self_note` INT NOT NULL , `dak_note` INT NOT NULL , `potrojari_nisponno` INT NOT NULL , `note_nisponno` INT NOT NULL , `onisponno_note` INT NOT NULL , `potrojari` INT NOT NULL , `created` DATETIME NOT NULL , `modified` DATETIME NOT NULL ,  PRIMARY KEY (`id`)) ENGINE = InnoDB;

CREATE TABLE `nothi_reports`.`dashboard_units` ( `id` INT NOT NULL AUTO_INCREMENT ,`unit_id` INT NOT NULL , `operation_date` DATE NOT NULL , `dak_inbox` INT NOT NULL , `dak_outbox` INT NOT NULL , `nothivukto` INT NOT NULL , `nothijat` INT NOT NULL , `dak_nisponno` INT NOT NULL , `dak_onisponno` INT NOT NULL , `self_note` INT NOT NULL , `dak_note` INT NOT NULL , `potrojari_nisponno` INT NOT NULL , `note_nisponno` INT NOT NULL , `onisponno_note` INT NOT NULL , `potrojari` INT NOT NULL , `created` DATETIME NOT NULL , `modified` DATETIME NOT NULL ,  PRIMARY KEY (`id`)) ENGINE = InnoDB;

CREATE TABLE `nothi_reports`.`dashboard_designations` ( `id` INT NOT NULL AUTO_INCREMENT ,`designation_id` INT NOT NULL , `operation_date` DATE NOT NULL , `dak_inbox` INT NOT NULL , `dak_outbox` INT NOT NULL , `nothivukto` INT NOT NULL , `nothijat` INT NOT NULL , `dak_nisponno` INT NOT NULL , `dak_onisponno` INT NOT NULL , `self_note` INT NOT NULL , `dak_note` INT NOT NULL , `potrojari_nisponno` INT NOT NULL , `note_nisponno` INT NOT NULL , `onisponno_note` INT NOT NULL , `potrojari` INT NOT NULL , `created` DATETIME NOT NULL , `modified` DATETIME NOT NULL ,  PRIMARY KEY (`id`)) ENGINE = InnoDB;


-- 23-11-2016 (Projapoti DB)
ALTER TABLE `user_login_history` ADD `is_mobile` TINYINT(2) NOT NULL DEFAULT '0' AFTER `id`;

-- 25-11-2016 (Nothi Reports)
ALTER TABLE `performance_offices` CHANGE `potrojari` `nisponnopotrojari` INT(11) NOT NULL;
ALTER TABLE `performance_offices` ADD `potrojari` INT NOT NULL DEFAULT '0' AFTER `onisponno`;
ALTER TABLE `performance_offices` ADD `updated` TINYINT NOT NULL DEFAULT '0' AFTER `record_date`;

ALTER TABLE `performance_units` CHANGE `potrojari` `nisponnopotrojari` INT(11) NOT NULL;
ALTER TABLE `performance_units` ADD `potrojari` INT NOT NULL DEFAULT '0' AFTER `onisponno`;
ALTER TABLE `performance_units` ADD `updated` TINYINT NOT NULL DEFAULT '0' AFTER `record_date`;

ALTER TABLE `performance_designations` CHANGE `potrojari` `nisponnopotrojari` INT(11) NOT NULL;
ALTER TABLE `performance_designations` ADD `potrojari` INT NOT NULL DEFAULT '0' AFTER `onisponno`;
ALTER TABLE `performance_designations` ADD `updated` TINYINT NOT NULL DEFAULT '0' AFTER `record_date`;

-- 26-Dec-2016 (Nothi Access DB)
CREATE TABLE `nothi_access_db`.`potrojari_groups` ( `id` INT NOT NULL AUTO_INCREMENT , `group_name` TEXT NOT NULL , `privacy_type` VARCHAR(50) NOT NULL , `group_value` LONGTEXT NOT NULL , `creator_employee_id` INT NOT NULL , `creator_office_unit_organogram_id` INT NOT NULL , `creator_unit_id` INT NOT NULL , `creator_office_id` INT NOT NULL , `created` DATETIME NOT NULL , `modified` DATETIME NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

-- 27-Dec-2016 (Nothi Access DB)
ALTER TABLE `potrojari_groups` ADD INDEX( `creator_office_id`);