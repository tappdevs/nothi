-- 06 Apr 2017 (Projapoti DB)
ALTER TABLE `office_units` ADD `email` VARCHAR(100) NULL DEFAULT NULL AFTER `sarok_no_start`, ADD `phone` VARCHAR(100) NULL DEFAULT NULL AFTER `email`, ADD `fax` VARCHAR(100) NULL DEFAULT NULL AFTER `phone`;

-- 11-04-2017 (Office DB)
ALTER TABLE  `potrojari_receiver` CHANGE  `potro_description`  `potro_description` LONGTEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;
ALTER TABLE  `potrojari_onulipi` CHANGE  `potro_description`  `potro_description` LONGTEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;

-- 15-04-2017 (Projapoti DB)
CREATE TABLE `projapoti_db`.`view_reports` ( `id` INT NOT NULL AUTO_INCREMENT , `username` INT NOT NULL , `created_by` INT NULL , `modified_by` INT NULL , `created` DATETIME NOT NULL , `modified` DATETIME NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

ALTER TABLE `view_reports` CHANGE `username` `username` VARCHAR(15) NOT NULL;

-- 24 Apr 2017 (Projapoti DB for API)

ALTER TABLE `user_login_history` ADD `token` TEXT NULL DEFAULT NULL , ADD `device_type` VARCHAR(50) NOT NULL DEFAULT 'web' AFTER `token`, ADD `device_id` VARCHAR(100) NULL DEFAULT NULL AFTER `device_type`;

ALTER TABLE `dak_actions` DROP `token`, DROP `device_type`, DROP `device_id`;

ALTER TABLE `dak_actions` ADD `token` BIGINT NULL DEFAULT NULL , ADD `device_type` VARCHAR(50) NOT NULL DEFAULT 'web' AFTER `token`, ADD `device_id` VARCHAR(100) NULL DEFAULT NULL AFTER `device_type`;

ALTER TABLE `nothi_decisions` DROP `token`, DROP `device_type`, DROP `device_id`;

ALTER TABLE `nothi_decisions` ADD `token` BIGINT NULL DEFAULT NULL , ADD `device_type` VARCHAR(50) NOT NULL DEFAULT 'web' AFTER `token`, ADD `device_id` VARCHAR(100) NULL DEFAULT NULL AFTER `device_type`;


-- 24 Apr 2017 (Office DB for API)

ALTER TABLE `dak_movements` DROP `token`, DROP `device_type`, DROP `device_id`;

ALTER TABLE `dak_movements` ADD `token` BIGINT NULL DEFAULT NULL , ADD `device_type` VARCHAR(50) NOT NULL DEFAULT 'web' AFTER `token`, ADD `device_id` VARCHAR(100) NULL DEFAULT NULL AFTER `device_type`;

ALTER TABLE `dak_users` DROP `token`, DROP `device_type`, DROP `device_id`;

ALTER TABLE `dak_users` ADD `token` BIGINT NULL DEFAULT NULL , ADD `device_type` VARCHAR(50) NOT NULL DEFAULT 'web' AFTER `token`, ADD `device_id` VARCHAR(100) NULL DEFAULT NULL AFTER `device_type`;

ALTER TABLE `nothi_master_permissions` DROP `token`, DROP `device_type`, DROP `device_id`;

ALTER TABLE `nothi_master_permissions` ADD `token` BIGINT NULL DEFAULT NULL , ADD `device_type` VARCHAR(50) NOT NULL DEFAULT 'web' AFTER `token`, ADD `device_id` VARCHAR(100) NULL DEFAULT NULL AFTER `device_type`;

ALTER TABLE `nothi_master_current_users` DROP `token`, DROP `device_type`, DROP `device_id`;

ALTER TABLE `nothi_master_current_users` ADD `token` BIGINT NULL DEFAULT NULL , ADD `device_type` VARCHAR(50) NOT NULL DEFAULT 'web' AFTER `token`, ADD `device_id` VARCHAR(100) NULL DEFAULT NULL AFTER `device_type`;

ALTER TABLE `nothi_master_movements` DROP `token`, DROP `device_type`, DROP `device_id`;

ALTER TABLE `nothi_master_movements` ADD `token` BIGINT NULL DEFAULT NULL , ADD `device_type` VARCHAR(50) NOT NULL DEFAULT 'web' AFTER `token`, ADD `device_id` VARCHAR(100) NULL DEFAULT NULL AFTER `device_type`;

ALTER TABLE `nothi_potro_attachments` DROP `token`, DROP `device_type`, DROP `device_id`;

ALTER TABLE `nothi_potro_attachments` ADD `token` BIGINT NULL DEFAULT NULL , ADD `device_type` VARCHAR(50) NOT NULL DEFAULT 'web' AFTER `token`, ADD `device_id` VARCHAR(100) NULL DEFAULT NULL AFTER `device_type`;

ALTER TABLE `nothi_parts` DROP `token`, DROP `device_type`, DROP `device_id`;

ALTER TABLE `nothi_parts` ADD `token` BIGINT NULL DEFAULT NULL , ADD `device_type` VARCHAR(50) NOT NULL DEFAULT 'web' AFTER `token`, ADD `device_id` VARCHAR(100) NULL DEFAULT NULL AFTER `device_type`;

ALTER TABLE `potrojari` DROP `token`, DROP `device_type`, DROP `device_id`;

ALTER TABLE `potrojari` ADD `token` BIGINT NULL DEFAULT NULL , ADD `device_type` VARCHAR(50) NOT NULL DEFAULT 'web' AFTER `token`, ADD `device_id` VARCHAR(100) NULL DEFAULT NULL AFTER `device_type`;

ALTER TABLE `potrojari_attachments` DROP `token`, DROP `device_type`, DROP `device_id`;

ALTER TABLE `potrojari_attachments` ADD `token` BIGINT NULL DEFAULT NULL , ADD `device_type` VARCHAR(50) NOT NULL DEFAULT 'web' AFTER `token`, ADD `device_id` VARCHAR(100) NULL DEFAULT NULL AFTER `device_type`;

ALTER TABLE `nothi_note_signatures` DROP `token`, DROP `device_type`, DROP `device_id`;

ALTER TABLE `nothi_note_signatures` ADD `token` BIGINT NULL DEFAULT NULL , ADD `device_type` VARCHAR(50) NOT NULL DEFAULT 'web' AFTER `token`, ADD `device_id` VARCHAR(100) NULL DEFAULT NULL AFTER `device_type`;

ALTER TABLE `nothi_notes` DROP `token`, DROP `device_type`, DROP `device_id`;

ALTER TABLE `nothi_notes` ADD `token` BIGINT NULL DEFAULT NULL , ADD `device_type` VARCHAR(50) NOT NULL DEFAULT 'web' AFTER `token`, ADD `device_id` VARCHAR(100) NULL DEFAULT NULL AFTER `device_type`;

ALTER TABLE `potrojari_versions` DROP `token`, DROP `device_type`, DROP `device_id`;

ALTER TABLE `potrojari_versions` ADD `token` BIGINT NULL DEFAULT NULL , ADD `device_type` VARCHAR(50) NOT NULL DEFAULT 'web' AFTER `token`, ADD `device_id` VARCHAR(100) NULL DEFAULT NULL AFTER `device_type`;

ALTER TABLE `nothi_note_attachments` DROP `token`, DROP `device_type`, DROP `device_id`;

ALTER TABLE `nothi_note_attachments` ADD `token` BIGINT NULL DEFAULT NULL , ADD `device_type` VARCHAR(50) NOT NULL DEFAULT 'web' AFTER `token`, ADD `device_id` VARCHAR(100) NULL DEFAULT NULL AFTER `device_type`;

ALTER TABLE `office_designation_seals` DROP `token`, DROP `device_type`, DROP `device_id`;

ALTER TABLE `office_designation_seals` ADD `token` BIGINT NULL DEFAULT NULL , ADD `device_type` VARCHAR(50) NOT NULL DEFAULT 'web' AFTER `token`, ADD `device_id` VARCHAR(100) NULL DEFAULT NULL AFTER `device_type`;

-- 26 April 2017 (Projapoti DB)
CREATE TABLE `projapoti_db`.`api_tokens` ( `id` INT NOT NULL AUTO_INCREMENT , `fcm_token` TEXT NOT NULL ,`username` VARCHAR(20) NOT NULL , `token` BIGINT NULL DEFAULT NULL , `device_id` VARCHAR(100) NULL DEFAULT NULL , `device_type` VARCHAR(50) NULL DEFAULT NULL , `created` DATETIME NOT NULL , `modified` DATETIME NOT NULL ,  PRIMARY KEY (`id`)) ENGINE = InnoDB;

-- 29 April 2017 (Projapoti DB)
ALTER TABLE `api_tokens` ADD `employee_record_id` BIGINT NOT NULL AFTER `username`;

-- 7-05-2017 (Projapoti DB)
ALTER TABLE `potrojari_header_settings` ADD `id` BIGINT NOT NULL AUTO_INCREMENT FIRST, ADD PRIMARY KEY (`id`);

-- 15-05-2017 (Projapoti DB)
DROP TABLE IF EXISTS `summary_nothi_users`;
CREATE TABLE `summary_nothi_users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nothi_office` int(11) DEFAULT '0',
  `current_office_id` int(11) DEFAULT '0',
  `nothi_master_id` bigint(20) DEFAULT '0',
  `nothi_part_no` bigint(20) DEFAULT '0',
  `potrojari_id` bigint(20) DEFAULT '0',
  `dak_id` bigint(20) DEFAULT '0',
  `potro_id` bigint(20) DEFAULT '0',
  `designation_id` bigint(20) DEFAULT '0',
  `employee_record_id` bigint(20) NOT NULL,
  `position_number` varchar(50) NOT NULL,
  `sequence_number` int(11) NOT NULL,
  `can_approve` tinyint(2) DEFAULT '1',
  `is_approve` tinyint(2) DEFAULT '0',
  `is_sent` tinyint(2) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- 17-05-2017 (Nothi Access DB)
ALTER TABLE `nothi_master_current_users` CHANGE `id` `id` BIGINT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `nothi_master_permissions` CHANGE `id` `id` BIGINT(11) NOT NULL AUTO_INCREMENT;

-- 28-05-2017 (Projapoti DB)
CREATE TABLE `projapoti_db`.`sms_request` ( `id` BIGINT NOT NULL AUTO_INCREMENT , `cell_number` VARCHAR(20) NOT NULL , `message` VARCHAR(250) NOT NULL , `status` TINYINT NOT NULL DEFAULT '0' , `try` INT NOT NULL DEFAULT '0' , `created` DATETIME NOT NULL , `modified` DATETIME NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;
-- 30-05-2017 (office db)
ALTER TABLE `dak_daptoriks`  ADD `previous_receipt_no` TEXT NULL  AFTER `modified`,  ADD `previous_docketing_no` VARCHAR(250) NULL  AFTER `previous_receipt_no`;
ALTER TABLE `dak_nagoriks`  ADD `previous_receipt_no` TEXT NULL  AFTER `modified`,  ADD `previous_docketing_no` VARCHAR(250) NULL  AFTER `previous_receipt_no`;
ALTER TABLE `dak_daptoriks`  ADD `is_rollback_to_dak` TINYINT NOT NULL DEFAULT '0'  AFTER `previous_docketing_no`; 
ALTER TABLE `dak_nagoriks`  ADD `is_rollback_to_dak` TINYINT NOT NULL DEFAULT '0'  AFTER `previous_docketing_no`;

-- 30-05-2017 (Office DB)
CREATE TABLE IF NOT EXISTS `other_office_nothi_master_movements` (
  `id` bigint(20) NOT NULL,
  `nothi_master_id` bigint(20) NOT NULL,
  `nothi_part_no` bigint(20) NOT NULL DEFAULT '1',
  `nothi_office` int(11) NOT NULL DEFAULT '0',
  `from_office_id` int(11) DEFAULT NULL,
  `from_office_name` varchar(255) DEFAULT NULL,
  `from_office_unit_id` int(11) NOT NULL,
  `from_office_unit_name` varchar(255) NOT NULL,
  `from_officer_id` int(11) DEFAULT NULL,
  `from_officer_name` varchar(255) DEFAULT NULL,
  `from_officer_designation_id` int(11) NOT NULL,
  `from_officer_designation_label` varchar(255) DEFAULT NULL,
  `to_office_id` int(11) NOT NULL,
  `to_office_name` varchar(255) DEFAULT NULL,
  `to_office_unit_id` int(11) NOT NULL,
  `to_office_unit_name` varchar(255) NOT NULL,
  `to_officer_id` int(11) NOT NULL,
  `to_officer_name` varchar(255) DEFAULT NULL,
  `to_officer_designation_id` int(11) NOT NULL,
  `to_officer_designation_label` varchar(255) DEFAULT NULL,
  `view_status` tinyint(4) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `modified_by` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `token` text,
  `device_type` varchar(50) NOT NULL DEFAULT 'web',
  `device_id` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `other_office_nothi_master_movements`
  ADD PRIMARY KEY (`id`),
  ADD KEY `nothi_part_no` (`nothi_part_no`),
  ADD KEY `from_office_id` (`from_office_id`),
  ADD KEY `to_office_id` (`to_office_id`);

ALTER TABLE `other_office_nothi_master_movements`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

-- 04-06-2017 (Projapoti DB)
DROP TABLE IF EXISTS `office_health_status`;
CREATE TABLE `office_health_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `office_id` int(11) NOT NULL,
  `has_error` tinyint(2) NOT NULL DEFAULT '0',
  `health_report` text,
  `check_date` date NOT NULL,
  `last_check` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),KEY `office_id` (`office_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 17-04-2017 (Projapoti DB)
DROP TABLE IF EXISTS `user_signatures`;
CREATE TABLE IF NOT EXISTS `user_signatures` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(14) NOT NULL,
  `signature_file` varchar(255) NOT NULL,
  `encode_sign` text NOT NULL,
  `previous_signature` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
ALTER TABLE `user_signatures` ADD INDEX(`username`);

-- 05-06-2017 (projapoti DB)
CREATE TABLE `potrojari_request` ( `id` BIGINT NOT NULL AUTO_INCREMENT , `command` VARCHAR(250) NOT NULL UNIQUE , `office_id` INT NOT NULL , `potrojari_id` BIGINT NOT NULL , `try` TINYINT NOT NULL , `status` TINYINT DEFAULT 0 , `created` DATETIME NOT NULL , `modified` DATETIME NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

-- 06-06-2017 (Office Db)
ALTER TABLE `potrojari_receiver` ADD INDEX( `potrojari_id`);
ALTER TABLE `potrojari_onulipi` ADD INDEX( `potrojari_id`);

-- 06-06-2017 (office DB)
ALTER TABLE `other_office_nothi_master_movements` ADD `nothi_no` VARCHAR(64) NULL DEFAULT NULL AFTER `id`, ADD `subject` VARCHAR(255) NULL DEFAULT NULL AFTER `nothi_no`, ADD `nothi_shakha` VARCHAR(255) NULL DEFAULT NULL AFTER `subject`;

-- SSL implementation (caution: already run in live server)
-- 29-06-2017 (Projapoti DB)
update `office_domains` set `domain_url` = REPLACE(`domain_url`,'http://','https://');

-- 13-07-2017 (office db)
ALTER TABLE `nothi_master_movements` ADD `movement_type` TINYINT(2) NOT NULL DEFAULT '1' AFTER `view_status`;

-- 20-07-2017 (office db)
ALTER TABLE `potrojari` ADD `meta_data` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `potro_description`;
ALTER TABLE `dak_attachments` ADD `meta_data` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `content_body`;
ALTER TABLE `dak_daptoriks` ADD `meta_data` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `dak_description`;
ALTER TABLE `nothi_potros` ADD `meta_data` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `potro_content`;
ALTER TABLE `nothi_potros` CHANGE `potro_content` `potro_content` LONGTEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;
ALTER TABLE `nothi_potro_attachments` ADD `meta_data` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `content_body`;
ALTER TABLE `potrojari_attachments` ADD `meta_data` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `content_body`;

ALTER TABLE `potrojari_onulipi` CHANGE `potro_description` `potro_description` LONGTEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;
ALTER TABLE `potrojari_receiver` CHANGE `potro_description` `potro_description` LONGTEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;

-- 30-07-2017 (Projapoti DB)
DROP TABLE IF EXISTS `unit_update_history`;
CREATE TABLE IF NOT EXISTS `unit_update_history` (
  `id` int(11) NOT NULL PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `office_id` int(11) NOT NULL,
  `office_unit_id` int(11) NOT NULL,
  `office_origin_unit_id` int(11) DEFAULT NULL,
  `parent_unit_id` int(11) DEFAULT NULL,
  `old_unit_eng` varchar(255) DEFAULT NULL,
  `old_unit_bng` varchar(255) NOT NULL,
  `unit_eng` varchar(255) DEFAULT NULL,
  `unit_bng` varchar(255) NOT NULL,
  `employee_office_id` int(11) NOT NULL,
  `employee_unit_id` int(11) NOT NULL,
  `employee_designation_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 01-08-2017 (office db)
ALTER TABLE `nothi_note_signatures` CHANGE `sequence` `sequence` INT(11) NOT NULL DEFAULT '0';

-- 02-08-2017 (office db)
DROP TABLE IF EXISTS `portal_publishes`;
CREATE TABLE `portal_publishes` (
  `id` bigint(20) NOT NULL,
  `potrojari_id` bigint(20) NOT NULL,
  `publish_type` varchar(20) NOT NULL,
  `nothi_master_id` bigint(20) NOT NULL,
  `nothi_office` text NOT NULL,
  `nothi_part_id` bigint(20) NOT NULL,
  `subject` text NOT NULL,
  `description` text NOT NULL,
  `domain` varchar(255) NOT NULL,
  `archive_date` date NOT NULL,
  `potrojari_date` date NOT NULL,
  `file_path` text NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `created_by` bigint(20) NOT NULL DEFAULT '0',
  `modified_by` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
ALTER TABLE `portal_publishes`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `portal_publishes`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

-- 03-08-2017 (office db)
ALTER TABLE `nothi_potros` CHANGE `dispatch_date` `dispatch_date` DATETIME NULL;

-- 07-08-2017 (Nothi Access DB)
ALTER TABLE `nisponno_records` ADD `potrojari_id` INT NOT NULL DEFAULT '0' AFTER `nothi_onucched_id`;
ALTER TABLE `nothi_master_current_users` ADD `priority` INT NOT NULL DEFAULT '0' AFTER `is_new`;

-- 07-08-2017 (Nothi Report DB)
ALTER TABLE `performance_offices` ADD `potrojari_nisponno_internal` INT NOT NULL DEFAULT '0' AFTER `potrojari`, ADD `potrojari_nisponno_external` INT NOT NULL DEFAULT '0' AFTER `potrojari_nisponno_internal`;

-- 09-08-2017 (Office DB)
-- ALTER TABLE `potrojari` CHANGE `potrojari_internal` `potrojari_internal` TINYINT(2) NOT NULL DEFAULT '0';
ALTER TABLE `potrojari` ADD `potrojari_internal` TINYINT(2) NOT NULL DEFAULT '0';
-- given in last release note


-- 13-08-2017 (Projapoti DB)
ALTER TABLE `potrojari_templates` ADD `template_id` INT NOT NULL DEFAULT '0' AFTER `status`, ADD `version` VARCHAR(5) NOT NULL DEFAULT 'bn' AFTER `template_id`;
ALTER TABLE `potrojari_header_settings` ADD `potrojari_head_eng` LONGTEXT NULL AFTER `potrojari_head`;
update `potrojari_header_settings` set potrojari_head_eng = `potrojari_head`;
-- 15-08-2017 (Nothi Report)
CREATE TABLE `nothi_reports`.`cron_request` ( `id` INT NOT NULL AUTO_INCREMENT , `command` VARCHAR(250) NOT NULL , `created` DATETIME NOT NULL , `modified` DATETIME NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

-- 15-08-2017 (Office DB)
ALTER TABLE `potrojari` ADD `potrojari_language` VARCHAR(5) NOT NULL DEFAULT 'bn' AFTER `potrojari_internal`;

-- 20-08-2017 (Office DB)
ALTER TABLE `nothi_master_movements` ADD `priority` TINYINT(4) NOT NULL DEFAULT '0' AFTER `movement_type`;
ALTER TABLE `other_office_nothi_master_movements` ADD `priority` TINYINT(4) NOT NULL DEFAULT '0' AFTER `view_status`;

-- 20-08-2017 (Nothi Access DB)
ALTER TABLE `nothi_master_current_users` CHANGE `priority` `priority` TINYINT(4) NOT NULL DEFAULT '0';

-- 21-08-2017 (projapoti db)
ALTER TABLE `summary_nothi_users` ADD `tracking_id` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `id`;

-- 22-08-2017 (projapoti db)
ALTER TABLE `dak_tracks` ADD `forms_track_no` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `docketing_no`;
-- 22-08-2017 (Office DB)
ALTER TABLE `potrojari` ADD `attension_officer_designation_label` VARCHAR(255) NULL AFTER `potrojari_language`, ADD `attension_officer_designation_id` BIGINT NULL AFTER `attension_officer_designation_label`, ADD `attension_office_unit_name` VARCHAR(255) NULL AFTER `attension_officer_designation_id`, ADD `attension_office_unit_id` BIGINT NULL AFTER `attension_office_unit_name`, ADD `attension_office_name` VARCHAR(255) NULL AFTER `attension_office_unit_id`, ADD `attension_office_id` BIGINT NULL AFTER `attension_office_name`, ADD `attension_officer_name` VARCHAR(255) NULL AFTER `attension_office_id`, ADD `attension_officer_id` BIGINT NULL AFTER `attension_officer_name`;

-- 24-08-2017 (Office DB)
ALTER TABLE `potrojari` ADD `attached_potro_id` BIGINT(20) NOT NULL DEFAULT '0' AFTER `nothi_potro_id`;
ALTER TABLE `potrojari` ADD `attached_potro` LONGTEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `attached_potro_id`;

-- 28-08-2017 (Office DB)
ALTER TABLE `potrojari_attachments` ADD `attachment_name` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `attachment_description`;

-- 12-09-2017 (Office DB)
ALTER TABLE `dak_attachments` CHANGE `file_dir` `file_dir` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL;
ALTER TABLE `dak_attachments` CHANGE `created_by` `created_by` INT(11) NULL, CHANGE `modified_by` `modified_by` INT(11) NULL, CHANGE `created` `created` DATETIME NULL, CHANGE `modified` `modified` DATETIME NULL;
ALTER TABLE `dak_movements` CHANGE `sequence` `sequence` INT(4) NULL, CHANGE `created_by` `created_by` INT(11) NULL, CHANGE `modified_by` `modified_by` INT(11) NULL, CHANGE `created` `created` DATETIME NULL, CHANGE `modified` `modified` TIMESTAMP on update CURRENT_TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP, CHANGE `dak_priority` `dak_priority` TINYINT(2) NULL;
ALTER TABLE `dak_users` CHANGE `created_by` `created_by` INT(11) NULL, CHANGE `modified_by` `modified_by` INT(11) NULL, CHANGE `created` `created` DATETIME NULL, CHANGE `modified` `modified` DATETIME NULL;
ALTER TABLE `dak_user_actions` CHANGE `created_by` `created_by` INT(11) NULL, CHANGE `created` `created` DATETIME NULL;

-- 17-09-2017 (nothi_access_db)
DROP TABLE IF EXISTS `potrojari_groups_users`;
CREATE TABLE `potrojari_groups_users` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `group_name` text NOT NULL,
  `office_head` tinyint(2) NOT NULL DEFAULT '0',
  `privacy_type` varchar(50) NOT NULL,
  `office_id` int(11) NOT NULL,
  `office_name_eng` varchar(255) NULL DEFAULT '0',
  `office_name_bng` varchar(255) NULL DEFAULT '0',
  `office_unit_id` int(11) NOT NULL,
  `office_unit_name_eng` varchar(255) NULL DEFAULT '0',
  `office_unit_name_bng` varchar(255) NULL DEFAULT '0',
  `office_unit_organogram_id` int(11) NOT NULL,
  `office_unit_organogram_name_eng` varchar(255) NULL DEFAULT '0',
  `office_unit_organogram_name_bng` varchar(255) NULL DEFAULT '0',
  `employee_id` int(11) NOT NULL,
  `employee_name_eng` varchar(255) NULL DEFAULT '0',
  `employee_name_bng` varchar(255) NULL DEFAULT '0',
  `officer_email` varchar(255) NULL DEFAULT '0',
  `creator_employee_id` int(11) NOT NULL,
  `creator_office_unit_organogram_id` int(11) NOT NULL,
  `creator_unit_id` int(11) NOT NULL,
  `creator_office_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `potrojari_groups_users`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `potrojari_groups_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

-- 19-Sep-2017 ( Nothi access DB)
ALTER TABLE `potrojari_groups` ADD `total_users` INT NOT NULL DEFAULT '0' AFTER `creator_office_id`;

-- (Office DB)
ALTER TABLE `nothi_potro_attachments` CHANGE `file_dir` `file_dir` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL;

-- 25-09-2017 (Projapoti DB)
ALTER TABLE `dak_tracks` CHANGE `sender_email` `sender_email` VARCHAR(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL, CHANGE `sender_mobile` `sender_mobile` VARCHAR(17) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL, CHANGE `docketing_no` `docketing_no` BIGINT NULL DEFAULT NULL, CHANGE `receiving_officer_designation_label` `receiving_officer_designation_label` VARCHAR(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL, CHANGE `receiving_officer_name` `receiving_officer_name` VARCHAR(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL, CHANGE `dak_type` `dak_type` VARCHAR(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;

-- 25-09-2017 (Office DB)
ALTER TABLE `dak_attachments` ADD INDEX( `dak_type`, `dak_id`);

ALTER TABLE `dak_daptoriks` ADD INDEX( `receiving_officer_designation_id`);
ALTER TABLE `dak_daptoriks` ADD INDEX( `sender_officer_designation_id`);

ALTER TABLE `dak_nagoriks` ADD INDEX( `receiving_officer_designation_id`);
ALTER TABLE `dak_nagoriks` ADD INDEX( `receiving_office_id`);

ALTER TABLE dak_movements DROP INDEX dak_type_2;
ALTER TABLE `dak_movements` ADD INDEX( `dak_type`, `dak_id`);
ALTER TABLE `dak_movements` ADD INDEX( `from_officer_designation_id`);
ALTER TABLE `dak_movements` ADD INDEX( `to_officer_designation_id`);
ALTER TABLE dak_movements DROP INDEX to_office_id;

ALTER TABLE dak_users DROP INDEX to_office_id;
ALTER TABLE `dak_users` ADD INDEX( `dak_type`, `dak_id`);
ALTER TABLE `dak_users` ADD INDEX( `to_officer_designation_id`);

ALTER TABLE `dak_user_actions` ADD INDEX( `dak_user_id`);

ALTER TABLE `nothi_masters_dak_map` ADD INDEX( `nothi_part_no`);

ALTER TABLE `nothi_master_current_users` ADD INDEX( `office_unit_organogram_id`);
ALTER TABLE `nothi_master_movements` ADD INDEX( `from_officer_designation_id`);
ALTER TABLE `nothi_master_movements` ADD INDEX( `to_officer_designation_id`);

ALTER TABLE `nothi_master_permissions` ADD INDEX( `office_unit_organograms_id`);

ALTER TABLE `nothi_note_attachments` ADD INDEX( `nothi_part_no`);
ALTER TABLE `nothi_note_attachment_refs` ADD INDEX( `nothi_part_no`);
ALTER TABLE `nothi_note_sheets` ADD INDEX( `nothi_part_no`);

ALTER TABLE `nothi_note_signatures` ADD INDEX( `nothi_part_no`);
ALTER TABLE `nothi_note_signatures` ADD INDEX( `office_organogram_id`);

ALTER TABLE `nothi_parts` ADD INDEX( `nothi_masters_id`);
ALTER TABLE `nothi_parts` ADD INDEX( `office_units_organogram_id`);

ALTER TABLE `nothi_potros` ADD INDEX( `nothi_part_no`);

ALTER TABLE `office_unit_seals` ADD INDEX( `seal_owner_unit_id`);
ALTER TABLE `potrojari` ADD INDEX( `potro_status`);
ALTER TABLE `potrojari_attachments` ADD INDEX( `potrojari_id`);
ALTER TABLE `potrojari_onulipi` ADD INDEX( `potrojari_id`);
ALTER TABLE `potrojari_receiver` ADD INDEX( `potrojari_id`);
ALTER TABLE `potrojari_versions` ADD INDEX( `potrojari_id`);

-- 27-09-17 (projapoti_db)
ALTER TABLE `potrojari_header_settings` ADD `write_unit` TINYINT(2) NOT NULL DEFAULT '1' AFTER `potrojari_head_eng`;

-- 04-10-2017 (projapoti db)
drop table sessions;

-- 04-10-2017 (nothi access db)
CREATE TABLE `sessions` (
  `id` char(40) CHARACTER SET ascii COLLATE ascii_bin NOT NULL,
  `created` datetime DEFAULT CURRENT_TIMESTAMP, -- Optional
  `modified` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, -- Optional
  `data` blob DEFAULT NULL, -- for PostgreSQL use bytea instead of blob
  `expires` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 08-10-2017 (office DB)
CREATE TABLE `note_initialize` ( `id` BIGINT NOT NULL AUTO_INCREMENT , `nothi_masters_id` BIGINT NOT NULL , `nothi_part_no` BIGINT NOT NULL , `office_id` BIGINT NOT NULL , `office_units_id` INT NOT NULL , `office_units_organogram_id` INT NOT NULL , `created_by` INT NOT NULL , `modified_by` INT NOT NULL , `created` DATETIME NOT NULL , `modified` TIMESTAMP NOT NULL , `token` TEXT NULL DEFAULT NULL , `device_type` VARCHAR(50) NOT NULL DEFAULT 'web' , `device_id` VARCHAR(100) NULL DEFAULT NULL , `self_note` TINYINT(2) NOT NULL DEFAULT '1' , `dak_note` TINYINT(2) NOT NULL DEFAULT '0' , PRIMARY KEY (`id`)) ENGINE = InnoDB;

-- 09-10-2017 (office DB)
ALTER TABLE `dak_daptoriks` CHANGE `sending_date` `sending_date` DATETIME NULL;
ALTER TABLE `dak_daptoriks` CHANGE `receiving_date` `receiving_date` VARCHAR(50) NULL DEFAULT NULL;
update dak_daptoriks set receiving_date = created where receiving_date = '0000-00-00 00:00:00';
ALTER TABLE `dak_daptoriks` CHANGE `docketing_no` `docketing_no` BIGINT NOT NULL;
ALTER TABLE `dak_daptoriks` CHANGE `receiving_date` `receiving_date` DATETIME NULL;

ALTER TABLE `dak_nagoriks` CHANGE `receive_date` `receive_date` VARCHAR(50) NULL DEFAULT NULL;
update dak_nagoriks set receive_date = created where receive_date = '0000-00-00 00:00:00';
ALTER TABLE `dak_nagoriks` CHANGE `docketing_no` `docketing_no` BIGINT NOT NULL;
ALTER TABLE `dak_nagoriks` CHANGE `receive_date` `receive_date` DATETIME NULL;

ALTER TABLE `dak_movements` CHANGE `docketing_no` `docketing_no` BIGINT NOT NULL;

-- 15-10-2017 (Nothi Access DB)
DROP TABLE IF EXISTS `guard_file_api_datas`;
CREATE TABLE `guard_file_api_datas` (
  `id` bigint(20) NOT NULL,
  `type` text,
  `subdomain` varchar(255) DEFAULT NULL,
  `name` text,
  `link` text,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `guard_file_api_datas`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `guard_file_api_datas`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;


-- 16-10-2017 (office db)
ALTER TABLE `nothi_parts` ADD INDEX(`nothi_types_id`);

-- 21-10-2017 (Nothi report)
ALTER TABLE `performance_offices` ADD `unassigned_pending_dak` INT NOT NULL DEFAULT '0' AFTER `potrojari_nisponno_external`, ADD `unassigned_pending_note` INT NOT NULL DEFAULT '0' AFTER `unassigned_pending_dak`;\
ALTER TABLE `performance_offices` ADD `unassigned_designation` INT NOT NULL DEFAULT '0' AFTER `unassigned_pending_note`;

-- 23-10-2017 (projapoti db)
ALTER TABLE `employee_records` CHANGE `date_of_birth` `date_of_birth` DATETIME NULL;
ALTER TABLE `users` CHANGE `verification_date` `verification_date` DATE NULL;
ALTER TABLE `users` CHANGE `last_password_change` `last_password_change` DATETIME NULL;
ALTER TABLE `employee_offices` CHANGE `last_office_date` `last_office_date` DATE NULL;
ALTER TABLE `employee_offices` CHANGE `status` `status` TINYINT(1) NOT NULL DEFAULT '1';
ALTER TABLE `employee_offices` CHANGE `status_change_date` `status_change_date` DATETIME NULL;
ALTER TABLE `employee_offices` CHANGE `created` `created` DATETIME NULL;
ALTER TABLE `employee_offices` CHANGE `modified` `modified` DATETIME NULL;
ALTER TABLE `employee_offices` CHANGE `created_by` `created_by` BIGINT(20) NULL;
ALTER TABLE `employee_offices` CHANGE `modified_by` `modified_by` BIGINT(20) NULL;


-- 07-11-2017 ProjapotiDb
ALTER TABLE  `employee_offices` ADD INDEX (  `office_unit_organogram_id` ) ;
ALTER TABLE  `employee_offices` ADD INDEX (  `status` ) ;

-- Nothi access db
ALTER TABLE  `nisponno_records` CHANGE  `id`  `id` BIGINT( 20 ) NOT NULL AUTO_INCREMENT;
ALTER TABLE  `nisponno_records` ADD INDEX (  `office_id` ) ;
ALTER TABLE  `nisponno_records` ADD INDEX (  `unit_id` ) ;
ALTER TABLE  `nisponno_records` ADD INDEX (  `designation_id` ) ;
ALTER TABLE `potrojari_groups_users` ADD INDEX(`group_id`);
ALTER TABLE `potrojari_groups_users` CHANGE  `id`  `id` BIGINT( 20 ) NOT NULL AUTO_INCREMENT


-- 08-11-2017 Nothi Acess DB
DROP TABLE IF EXISTS `dak_actions_mig`;
CREATE TABLE `dak_actions_mig` (
  `id` int(11) NOT NULL,
  `dak_action_name` varchar(255) NOT NULL DEFAULT '',
  `status` tinyint(2) NOT NULL DEFAULT '1',
  `creator` int(11) DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `dak_actions_mig`
  ADD PRIMARY KEY (`id`),
  ADD KEY `creator` (`creator`);

ALTER TABLE `dak_actions_mig`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

-- 08-11-2017 Nothi Acess DB
DROP TABLE IF EXISTS `dak_action_employees`;
CREATE TABLE `dak_action_employees` (
  `id` int(11) NOT NULL,
  `dak_action_id` int(11) NOT NULL,
  `employee_id` int(11) DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `dak_action_employees`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employee_id` (`employee_id`);

ALTER TABLE `dak_action_employees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

-- 08-11-2017 Nothi Acess DB
DROP TABLE IF EXISTS `nothi_decisions_mig`;
CREATE TABLE `nothi_decisions_mig` (
  `id` int(11) NOT NULL,
  `decisions` varchar(255) NOT NULL DEFAULT '',
  `status` tinyint(2) NOT NULL DEFAULT '1',
  `creator` int(11) DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `nothi_decisions_mig`
  ADD PRIMARY KEY (`id`),
  ADD KEY `creator` (`creator`);

ALTER TABLE `nothi_decisions_mig`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

-- 08-11-2017 Nothi Acess DB
CREATE TABLE `nothi_decision_employees` (
  `id` int(11) NOT NULL,
  `nothi_decision_id` int(11) NOT NULL,
  `employee_id` int(11) DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `nothi_decision_employees`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employee_id` (`employee_id`);

ALTER TABLE `nothi_decision_employees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE  `potrojari_groups_users` CHANGE  `id`  `id` BIGINT( 20 ) NOT NULL AUTO_INCREMENT;

-- projapotidb
ALTER TABLE `employee_offices` ADD `show_unit` TINYINT(2) NOT NULL DEFAULT '1' AFTER `status_change_date`;


-- 12-11-2017 office db
ALTER TABLE `nothi_note_attachments` DROP `created`;
ALTER TABLE `nothi_note_attachments` ADD `created` DATETIME NULL DEFAULT NULL AFTER `modified_by`;
update nothi_note_attachments set created = modified;

ALTER TABLE `nothi_potros` DROP `dispatch_date`;
ALTER TABLE `nothi_potros` ADD `dispatch_date` DATETIME NULL DEFAULT NULL AFTER `issue_date`;
update nothi_potros set dispatch_date = null;

ALTER TABLE `potrojari_attachments` DROP `created`;
ALTER TABLE `potrojari_attachments` ADD `created` DATETIME NULL DEFAULT NULL AFTER `modified_by`;
update potrojari_attachments set created = modified;


-- 22-11-2017
ALTER TABLE `nothi_note_attachments` ADD INDEX( `note_no`);
ALTER TABLE `dak_daptoriks` CHANGE `dak_subject` `dak_subject` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;

ALTER TABLE `nothi_potros` DROP `dispatch_date`;
ALTER TABLE `nothi_potros` ADD `dispatch_date` DATETIME NULL DEFAULT NULL;
ALTER TABLE `nothi_potros` CHANGE `subject` `subject` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;

-- 27-11-2017
ALTER TABLE `nothi_note_signatures` ADD `can_delete` TINYINT(2) NOT NULL DEFAULT '1' AFTER `signature_date`;
-- (Caution) after running previous query then run this
ALTER TABLE nothi_note_signatures ALTER can_delete SET DEFAULT 0;

-- 28-11-2017 (Office DB)
ALTER TABLE `portal_publishes` ADD `unique_id` VARCHAR(255) NULL AFTER `file_path`, ADD `portal_returned_id` VARCHAR(255) NULL AFTER `unique_id`;
ALTER TABLE `dak_movements` CHANGE `dak_priority` `dak_priority` TINYINT(3) NULL DEFAULT '0';

-- 29-11-2017 (Office DB)
ALTER TABLE `portal_publishes` CHANGE `publish_type` `publish_type` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;

-- 29-11-2017 (Office DB)
DROP TABLE IF EXISTS `nothi_data_change_history`;
CREATE TABLE `nothi_data_change_history` (
  `id` int(11) NOT NULL,
  `nothi_master_id` int(11) NOT NULL,
  `nothi_data` text,
  `employee_id` int(11) DEFAULT NULL,
  `office_unit_organogram_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `nothi_data_change_history`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `nothi_data_change_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

-- 05-12-2017 (Office DB)
ALTER TABLE `potrojari_receiver` ADD `visibleName` VARCHAR(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `receiving_officer_email`;
ALTER TABLE `potrojari_onulipi` ADD `visibleName` VARCHAR(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `receiving_officer_email`;

-- 07-12-2017 (Office DB)
ALTER TABLE `dak_users` ADD `is_rollback_dak` TINYINT(2) NOT NULL DEFAULT '0' AFTER `is_archive`;

-- 26-12-2017 (office DB) for nothi_archive
ALTER TABLE `nothi_masters` ADD `is_archived` TINYINT(1) NULL AFTER `is_deleted`, ADD `archived_date` DATETIME NULL AFTER `is_archived`, ADD `archived_organogram_id` INT NULL AFTER `archived_date`;
