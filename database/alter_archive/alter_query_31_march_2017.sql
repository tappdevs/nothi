
-- 11 Mar 2017 (Projapoti DB)
ALTER TABLE `nothi_updates` ADD `version` VARCHAR(100) NOT NULL AFTER `is_display`, ADD `release_date` DATE NOT NULL AFTER `version`;

-- 14 Mar 2017 (Projapoti DB)
DROP TABLE IF EXISTS `user_device_registration`;
CREATE TABLE IF NOT EXISTS `user_device_registration` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `device_id` varchar(100) NOT NULL,
  `activate` tinyint(2) NOT NULL DEFAULT '0',
  `activation_code` varchar(80) DEFAULT NULL,
  `activate_request` date DEFAULT NULL,
  `activate_date` date DEFAULT NULL,
  `expire_date` date DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 15 Mar 2017 (Projapoti DB)
CREATE TABLE IF NOT EXISTS `projapoti_db`.`view_office_dashboard` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `office_id` INT NOT NULL ,
  `viewer_designations` LONGTEXT NOT NULL ,
  `created_by` INT NOT NULL ,
  `created` DATETIME NOT NULL ,
  `modified` DATETIME NOT NULL ,
  PRIMARY KEY (`id`)) ENGINE = InnoDB DEFAULT CHARSET=utf8;

-- 15 MAR 2017 (Projapoti DB)
DROP TABLE potrojari_header_settings;
CREATE TABLE IF NOT EXISTS `potrojari_header_settings` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_record_id` bigint(20) NOT NULL,
  `office_id` bigint(20) NOT NULL,
  `office_unit_id` bigint(20) NOT NULL,
  `office_unit_organogram_id` bigint(20) NOT NULL,
  `potrojari_head` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 19 Mar 2017 (Projapoti DB)
ALTER TABLE `potrojari_header_settings` CHANGE `potrojari_head` `potrojari_head` LONGTEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;
ALTER TABLE `user_device_registration` ADD `id` BIGINT NOT NULL AUTO_INCREMENT FIRST, ADD PRIMARY KEY (`id`) ;

-- 30 Mar 2017 (Office DB)
DROP TABLE IF EXISTS `office_designation_seals`;
CREATE TABLE `office_designation_seals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `designation_name_bng` varchar(256) NOT NULL,
  `designation_name_eng` varchar(256) NOT NULL,
  `office_id` int(11) NOT NULL,
  `office_unit_organogram_id` int(11) NOT NULL,
  `office_unit_id` int(11) NOT NULL,
  `unit_name_bng` varchar(255) NOT NULL,
  `unit_name_eng` varchar(255) NOT NULL,
  `seal_owner_unit_id` int(11) NOT NULL,
  `seal_owner_designation_id` BIGINT NOT NULL,
  `designation_seq` int(11) NOT NULL DEFAULT '0',
  `designation_level` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `modified_by` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `office_designation_seals`
  ADD KEY `seal_owner_designation_id` (`seal_owner_designation_id`);

 */

