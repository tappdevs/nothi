-- 02-01-2017 (Office DB)
ALTER TABLE `potrojari_receiver` 
ADD `run_task` TINYINT(2) NOT NULL DEFAULT '0' AFTER `receiving_officer_email`, 
ADD `retry_count` INT NOT NULL DEFAULT '0' AFTER `run_task`, 
ADD `task_reposponse` VARCHAR(255) NULL DEFAULT NULL AFTER `retry_count`;

UPDATE potrojari_receiver set run_task = 1, retry_count = 0;

ALTER TABLE `potrojari_onulipi` 
ADD `run_task` TINYINT(2) NOT NULL DEFAULT '0' AFTER `receiving_officer_email`, 
ADD `retry_count` INT NOT NULL DEFAULT '0' AFTER `run_task`, 
ADD `task_reposponse` VARCHAR(255) NULL DEFAULT NULL AFTER `retry_count`;

UPDATE potrojari_onulipi set run_task = 1, retry_count = 0;

-- 03-01-2017 (Office DB)
ALTER TABLE `potrojari` ADD `receiver_sent` TINYINT(1) NOT NULL DEFAULT '0' AFTER `potro_status`;
ALTER TABLE `potrojari` ADD `onulipi_sent` TINYINT(1) NOT NULL DEFAULT '0' AFTER `receiver_sent`;

-- 04-01-2017 (Projapoti DB)
ALTER TABLE `dak_tracks` ADD `service_name` VARCHAR(100) NOT NULL DEFAULT 'nothi' AFTER `sarok_no`;

-- 05-01-2017 (Nothi Access DB)
ALTER TABLE `potrojari_groups` ADD `created_by` BIGINT NULL AFTER `modified`, ADD `modified_by` BIGINT NULL AFTER `created_by`;
ALTER TABLE `potrojari_groups` CHANGE `created` `created` DATETIME NULL, CHANGE `modified` `modified` DATETIME NULL;

-- 05-01-2017 (Office DB)
ALTER TABLE `potrojari_receiver` ADD `group_id` BIGINT NULL DEFAULT '0' AFTER `potro_description`, ADD `group_name` VARCHAR(250) NULL AFTER `group_id`;
ALTER TABLE `potrojari_onulipi` ADD `group_id` BIGINT NULL DEFAULT '0' AFTER `potro_description`, ADD `group_name` VARCHAR(250) NULL AFTER `group_id`;