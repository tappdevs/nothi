-- previous alters can be found in alter_query_09-10-2018.sql
-- 06-11-2008 (Office db)
 CREATE TABLE `nothi_potrojari_attachment_refs` (
  `id` bigint(20) NOT NULL,
  `office_id` int(11) NOT NULL,
  `nothi_part_no` bigint(20) NOT NULL,
  `attachment_type` varchar(255) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `nothi_potrojari_attachment_refs`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `nothi_potrojari_attachment_refs`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

-- 09-11-2018 (Office DB)
ALTER TABLE `potrojari` ADD `sovapoti_visibleName` VARCHAR(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `sovapoti_officer_name`;
ALTER TABLE `potrojari` ADD `sovapoti_visibleDesignation` VARCHAR(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `sovapoti_visibleName`;

-- 20-11-2018 (Access DB)
CREATE TABLE `efile_related_reports` (
  `id` int(11) NOT NULL,
  `title` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_of_submit` date DEFAULT NULL,
  `attachment_path` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `rank` int(11) DEFAULT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `modified` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
ALTER TABLE `efile_related_reports` ADD PRIMARY KEY (`id`);
ALTER TABLE `efile_related_reports` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

-- 26-11-2018 (Office DB)

ALTER TABLE `nothi_dak_potro_maps` DROP INDEX `nothi_part_no`;
ALTER TABLE `nothi_dak_potro_maps` DROP INDEX `is_nisponno`;
ALTER TABLE `note_initialize` DROP INDEX nothi_part_no;
ALTER TABLE `note_initialize` DROP INDEX office_units_organogram_id;
ALTER TABLE `dak_movements` DROP INDEX operation_type;
ALTER TABLE `dak_movements` DROP INDEX to_office_unit_id;
ALTER TABLE `dak_movements` DROP INDEX attention_type;
ALTER TABLE `dak_movements` DROP INDEX to_officer_designation_id;
ALTER TABLE `dak_attachments` DROP INDEX attachment_type;
ALTER TABLE `nothi_note_attachments` DROP INDEX `nothi_part_no`;
ALTER TABLE `nothi_note_attachments` DROP INDEX `nothi_notesheet_id`;
ALTER TABLE `nothi_note_attachments` DROP INDEX attachment_type;
ALTER TABLE `nothi_potro_attachments` DROP INDEX attachment_type;
ALTER TABLE `potrojari_attachments` DROP INDEX potrojari_id;
ALTER TABLE `potrojari_attachments` DROP INDEX attachment_type;
ALTER TABLE `nothi_potro_attachments` DROP INDEX status;
ALTER TABLE `nothi_master_permissions` DROP INDEX office_unit_organograms_id;
ALTER TABLE `potrojari` DROP INDEX potro_status;
ALTER TABLE `potrojari` DROP INDEX potrojari_draft_office_id;
ALTER TABLE `nothi_master_movements` DROP INDEX from_officer_designation_id;
ALTER TABLE `nothi_master_movements` DROP INDEX nothi_office;
ALTER TABLE `dak_movements` DROP INDEX from_officer_designation_id;
ALTER TABLE `nothi_master_movements` DROP INDEX view_status;
ALTER TABLE `nothi_parts` DROP INDEX nothi_masters_id;
ALTER TABLE `nothi_parts` DROP INDEX nothi_class;
ALTER TABLE `nothi_notes` DROP INDEX note_status;
ALTER TABLE `dak_users` DROP INDEX to_officer_designation_id;
ALTER TABLE `dak_users` DROP INDEX dak_category;
ALTER TABLE `dak_users` DROP INDEX attention_type;
ALTER TABLE `potrojari_receiver` DROP INDEX potrojari_id;
ALTER TABLE `potrojari_receiver` DROP INDEX nothi_part_no;
ALTER TABLE `potrojari_receiver` DROP INDEX nothi_master_id;
ALTER TABLE `potrojari_onulipi` DROP INDEX potrojari_id;
ALTER TABLE `potrojari_onulipi` DROP INDEX nothi_part_no;
ALTER TABLE `potrojari_onulipi` DROP INDEX nothi_master_id;
ALTER TABLE `nothi_dak_potro_maps` ADD INDEX(`nothi_part_no`);
ALTER TABLE `nothi_dak_potro_maps` ADD INDEX(`is_nisponno`);

ALTER TABLE `nothi_dak_potro_maps` DROP INDEX `nothi_part_no_2`;
ALTER TABLE `nothi_dak_potro_maps` DROP INDEX `is_nisponno`;
ALTER TABLE `note_initialize` DROP INDEX nothi_part_no_2;
ALTER TABLE `note_initialize` DROP INDEX office_units_organogram_id_2;
ALTER TABLE `dak_movements` DROP INDEX operation_type_2;
ALTER TABLE `dak_movements` DROP INDEX to_office_unit_id_2;
ALTER TABLE `dak_movements` DROP INDEX attention_type_2;
ALTER TABLE `dak_movements` DROP INDEX to_officer_designation_id_2;
ALTER TABLE `dak_attachments` DROP INDEX attachment_type_2;
ALTER TABLE `nothi_note_attachments` DROP INDEX `nothi_part_no`;
ALTER TABLE `nothi_note_attachments` DROP INDEX `nothi_notesheet_id`;
ALTER TABLE `nothi_note_attachments` DROP INDEX attachment_type_2;
ALTER TABLE `nothi_potro_attachments` DROP INDEX attachment_type_2;
ALTER TABLE `potrojari_attachments` DROP INDEX potrojari_id_2;
ALTER TABLE `potrojari_attachments` DROP INDEX attachment_type_2;
ALTER TABLE `nothi_potro_attachments` DROP INDEX status_2;
ALTER TABLE `nothi_master_permissions` DROP INDEX office_unit_organograms_id_2;
ALTER TABLE `potrojari` DROP INDEX potro_status_2;
ALTER TABLE `potrojari` DROP INDEX potrojari_draft_office_id_2;
ALTER TABLE `nothi_master_movements` DROP INDEX from_officer_designation_id_2;
ALTER TABLE `nothi_master_movements` DROP INDEX nothi_office_2;
ALTER TABLE `dak_movements` DROP INDEX from_officer_designation_id_2;
ALTER TABLE `nothi_master_movements` DROP INDEX view_status_2;
ALTER TABLE `nothi_parts` DROP INDEX nothi_masters_id_2;
ALTER TABLE `nothi_parts` DROP INDEX nothi_class_2;
ALTER TABLE `nothi_notes` DROP INDEX note_status_2;
ALTER TABLE `dak_users` DROP INDEX to_officer_designation_id_2;
ALTER TABLE `dak_users` DROP INDEX dak_category_2;
ALTER TABLE `dak_users` DROP INDEX attention_type_2;
ALTER TABLE `potrojari_receiver` DROP INDEX potrojari_id_2;
ALTER TABLE `potrojari_receiver` DROP INDEX nothi_part_no_2;
ALTER TABLE `potrojari_receiver` DROP INDEX nothi_master_id_2;
ALTER TABLE `potrojari_onulipi` DROP INDEX potrojari_id_2;
ALTER TABLE `potrojari_onulipi` DROP INDEX nothi_part_no_2;
ALTER TABLE `potrojari_onulipi` DROP INDEX nothi_master_id_2;

ALTER TABLE `note_initialize` ADD INDEX(`nothi_part_no`);
ALTER TABLE `note_initialize` ADD INDEX(`office_units_organogram_id`);
ALTER TABLE `dak_movements` ADD INDEX(`operation_type`);
ALTER TABLE `dak_movements` ADD INDEX(`to_office_unit_id`);
ALTER TABLE `dak_movements` ADD INDEX(`attention_type`);
ALTER TABLE `dak_movements` ADD INDEX(`to_officer_designation_id`);
ALTER TABLE `dak_attachments` ADD INDEX(`attachment_type`);
ALTER TABLE `nothi_note_attachments` ADD INDEX( `nothi_part_no`, `nothi_notesheet_id`);
ALTER TABLE `nothi_note_attachments` ADD INDEX(`attachment_type`);
ALTER TABLE `nothi_potro_attachments` ADD INDEX(`attachment_type`);
ALTER TABLE `potrojari_attachments` ADD INDEX(`potrojari_id`);
ALTER TABLE `potrojari_attachments` ADD INDEX(`attachment_type`);
ALTER TABLE `nothi_potro_attachments` ADD INDEX(`status`);
ALTER TABLE `nothi_master_permissions` ADD INDEX(`office_unit_organograms_id`);
ALTER TABLE `potrojari` ADD INDEX(`potro_status`);
ALTER TABLE `potrojari` ADD INDEX(`potrojari_draft_office_id`);
ALTER TABLE `nothi_master_movements` ADD INDEX(`from_officer_designation_id`);
ALTER TABLE `nothi_master_movements` ADD INDEX(`nothi_office`);
ALTER TABLE `dak_movements` ADD INDEX(`from_officer_designation_id`);
ALTER TABLE `nothi_master_movements` ADD INDEX(`view_status`);

ALTER TABLE `nothi_parts` ADD INDEX(`nothi_masters_id`);
ALTER TABLE `nothi_parts` ADD INDEX(`nothi_class`);
ALTER TABLE `nothi_notes` ADD INDEX(`note_status`);
ALTER TABLE `dak_users` ADD INDEX(`to_officer_designation_id`);
ALTER TABLE `dak_users` ADD INDEX(`dak_category`);
ALTER TABLE `dak_users` ADD INDEX(`attention_type`);
ALTER TABLE `potrojari_receiver` ADD INDEX(`potrojari_id`);
ALTER TABLE `potrojari_receiver` ADD INDEX(`nothi_part_no`);
ALTER TABLE `potrojari_receiver` ADD INDEX(`nothi_master_id`);

ALTER TABLE `potrojari_onulipi` ADD INDEX(`potrojari_id`);
ALTER TABLE `potrojari_onulipi` ADD INDEX(`nothi_part_no`);
ALTER TABLE `potrojari_onulipi` ADD INDEX(`nothi_master_id`);

-- 2018-12-20 (access db)
CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `title` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `message_for` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `related_id` int(11) NOT NULL,
  `message_by` int(11) NOT NULL,
  `is_deleted` int(1) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
CREATE TABLE `message_views` (
  `id` int(11) NOT NULL,
  `message_id` int(11) NOT NULL,
  `is_view` int(1) NOT NULL,
  `organogram_id` int(11) NOT NULL,
  `view_count` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
ALTER TABLE `messages` ADD PRIMARY KEY (`id`);
ALTER TABLE `message_views` ADD PRIMARY KEY (`id`);
ALTER TABLE `messages` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
ALTER TABLE `message_views` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

-- Nothi Reports (02-12-2018)
CREATE TABLE `nothi_reports`.`alter_users` ( `id` INT NOT NULL AUTO_INCREMENT , `email` VARCHAR(50) NOT NULL , `password` VARCHAR(250) NOT NULL , `created` DATETIME DEFAULT Null , `modiifed` TIMESTAMP on update CURRENT_TIMESTAMP NULL DEFAULT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

CREATE TABLE `nothi_reports`.`alter_queries` ( `id` BIGINT NOT NULL AUTO_INCREMENT , `query` TEXT NOT NULL , `request_by` INT NOT NULL , `created` DATETIME DEFAULT NULL , `modified` TIMESTAMP on update CURRENT_TIMESTAMP NULL DEFAULT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

CREATE TABLE `nothi_reports`.`alter_request` ( `id` BIGINT NOT NULL AUTO_INCREMENT , `query_id` BIGINT NOT NULL , `db_name` VARCHAR(250) NOT NULL , `status` TINYINT(4) NOT NULL DEFAULT '0' , `created` DATETIME DEFAULT NULL , `retry_count` INT NOT NULL , `response` TEXT NOT NULL , `last_request_time` INT NOT NULL , `modified` TIMESTAMP on update CURRENT_TIMESTAMP NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;
ALTER TABLE `alter_request` CHANGE `retry_count` `retry_count` INT(11) NULL, CHANGE `response` `response` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL, CHANGE `last_request_time` `last_request_time` INT(11) NULL;
ALTER TABLE `alter_request` CHANGE `retry_count` `retry_count` INT(11) NOT NULL DEFAULT '0';

CREATE TABLE `nothi_reports`.`alter_request_logs` ( `id` INT NOT NULL , `query_id` INT NOT NULL , `request_by` INT NOT NULL , `db_name` VARCHAR(250) NOT NULL , `retry_count` INT NOT NULL DEFAULT '0' , `last_request_time` INT NULL , `created` DATETIME DEFAULT NULL ) ENGINE = InnoDB;
ALTER TABLE `alter_request_logs` ADD PRIMARY KEY(`id`);
ALTER TABLE `alter_request_logs` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;


-- Projapoti DB (20-12-2018)
ALTER TABLE `employee_offices` CHANGE `joining_date` `joining_date` DATE NULL;
ALTER TABLE `office_origin_unit_organograms` CHANGE `office_origin_unit_id` `office_origin_unit_id` INT( 11 ) NULL ,
  CHANGE `superior_unit_id` `superior_unit_id` INT( 11 ) NULL ,
  CHANGE `superior_designation_id` `superior_designation_id` INT( 11 ) NULL ,
  CHANGE `short_name_eng` `short_name_eng` VARCHAR( 100 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
  CHANGE `short_name_bng` `short_name_bng` VARCHAR( 100 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
  CHANGE `created_by` `created_by` INT( 11 ) NULL ,
  CHANGE `modified_by` `modified_by` INT( 11 ) NULL ,
  CHANGE `created` `created` DATETIME NULL ,
  CHANGE `modified` `modified` DATETIME NULL;

-- 2018-01-03 (Projapoti DB)
CREATE TABLE `office_segregation_logs` (
  `id` int(11) NOT NULL,
  `prev_office_origin_id` int(11) DEFAULT NULL,
  `prev_unit_origin_id` int(11) DEFAULT NULL,
  `prev_organogram_origin_id` int(11) DEFAULT NULL,
  `prev_office_id` int(11) DEFAULT NULL,
  `prev_unit_id` int(11) DEFAULT NULL,
  `prev_organogram_id` int(11) DEFAULT NULL,
  `office_origin_id` int(11) DEFAULT NULL,
  `unit_origin_id` int(11) DEFAULT NULL,
  `organogram_origin_id` int(11) DEFAULT NULL,
  `office_id` int(11) DEFAULT NULL,
  `unit_id` int(11) DEFAULT NULL,
  `organogram_id` int(11) DEFAULT NULL,
  `prev_dak_inbox_count` int(11) DEFAULT NULL,
  `prev_nothi_inbox_count` int(11) DEFAULT NULL,
  `prev_nothi_sent_count` int(11) DEFAULT NULL,
  `prev_nothi_other_sent_count` int(11) DEFAULT NULL,
  `prev_potro_count` int(11) DEFAULT NULL,
  `dak_inbox_count` int(11) DEFAULT NULL,
  `nothi_inbox_count` int(11) DEFAULT NULL,
  `nothi_sent_count` int(11) DEFAULT NULL,
  `nothi_other_sent_count` int(11) DEFAULT NULL,
  `potro_count` int(11) DEFAULT NULL,
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
ALTER TABLE `office_segregation_logs`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `office_segregation_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;


-- 2019-01-13 (Office DB)
CREATE TABLE `other_organogram_activities_settings` ( `id` INT NOT NULL AUTO_INCREMENT , `organogram_id` INT NOT NULL , `assigned_organogram_id` INT NOT NULL , `assigned_date` DATETIME NULL , `status` INT(1) NOT NULL , `created` DATETIME NULL , `updated` DATETIME NULL , `unassigned_date` DATETIME NULL, `permission_for` INT(1) NOT NULL, PRIMARY KEY (`id`)) ENGINE = InnoDB;
-- ALTER TABLE `other_organogram_activities_settings` ADD `unassigned_date` DATETIME NULL AFTER `assigned_date`;
-- ALTER TABLE `other_organogram_activities_settings` ADD `permission_for` INT(1) NOT NULL COMMENT '0 for all - 1 for dak - 2 for nothi' AFTER `unassigned_date`;
ALTER TABLE `other_organogram_activities_settings` CHANGE `updated` `modified` DATETIME NULL DEFAULT NULL;


-- 2019-01-17 (Office Db)
ALTER TABLE  `dak_attachments` CHANGE  `is_summary_nothi`  `is_summary_nothi` TINYINT( 2 ) NULL DEFAULT  '0';
ALTER TABLE  `dak_daptoriks` CHANGE  `is_summary_nothi`  `is_summary_nothi` TINYINT( 2 ) NULL DEFAULT  '0';
ALTER TABLE  `nothi_potro_attachments` CHANGE  `is_summary_nothi`  `is_summary_nothi` TINYINT( 2 ) NULL DEFAULT  '0';
UPDATE `dak_attachments` SET `is_summary_nothi` = 0 WHERE `is_summary_nothi` IS NULL;
UPDATE `dak_daptoriks` SET `is_summary_nothi` = 0 WHERE `is_summary_nothi` IS NULL;
UPDATE `nothi_potro_attachments` SET `is_summary_nothi` = 0 WHERE `is_summary_nothi` IS NULL;

-- 2019-01-22 (Projapoti Db)
-- #2079 ----------------------
CREATE TABLE `user_manuals` (
  `id` int(11) NOT NULL,
  `title` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `module` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tags` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
ALTER TABLE `user_manuals` ADD PRIMARY KEY (`id`);
ALTER TABLE `user_manuals` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
-- #2079 need to add folder in CONTENT folder:
-- #content/UserManual/audio
-- #content/UserManual/text_image
-- #2079 END -------------------