
-- 03-04-2018 (Office db)
ALTER TABLE `potrojari_onulipi`
  DROP `sarok_no`,
  DROP `office_id`,
  DROP `officer_id`,
  DROP `office_name`,
  DROP `officer_name`,
  DROP `office_unit_id`,
  DROP `office_unit_name`,
  DROP `officer_designation_id`,
  DROP `officer_designation_label`,
  DROP `potrojari_date`,
  DROP `potro_subject`,
  DROP `potro_security_level`,
  DROP `potro_priority_level`,
  DROP `potro_description`;

ALTER TABLE `potrojari_receiver`
  DROP `sarok_no`,
  DROP `office_id`,
  DROP `officer_id`,
  DROP `office_name`,
  DROP `officer_name`,
  DROP `office_unit_id`,
  DROP `office_unit_name`,
  DROP `officer_designation_id`,
  DROP `officer_designation_label`,
  DROP `potrojari_date`,
  DROP `potro_subject`,
  DROP `potro_security_level`,
  DROP `potro_priority_level`,
  DROP `potro_description`;

