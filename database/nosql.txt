$tableNoSQL = TableRegistry::get('NoSql');

$savetoSql = $tableNoSQL->updateSql([
                    'id'=> $potrojari->nosql_id,
                    'user_designation'=> $current_office_selection['office_unit_organogram_id'],
                    'table_type'=> 4,
                    'options'=> [
                        'attached_potro'=>$potrojari->attached_potro
                    ],
                    'content_body' => $potrojari->potro_description,
                    'modifiedBy'=> $current_office_selection['office_unit_organogram_id'],
                    'modifiedAt'=> date("Y-m-d H:i:s")
                ]);

                if($savetoSql && $savetoSql['error']==0){
                    $tablePotroJari->save($potrojari);
                }


$pdfbody = $potrojari->potro_description_nosql['options']['attached_potro'] . "<br/>" . $potrojari->potro_description_nosql['content_body'];

$potrojari->potro_description_nosql