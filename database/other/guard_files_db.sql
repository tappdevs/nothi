-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:8889
-- Generation Time: Jun 09, 2016 at 01:10 AM
-- Server version: 5.5.42
-- PHP Version: 5.6.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nothi_access_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `guard_files`
--

DROP TABLE IF EXISTS `guard_files`;
CREATE TABLE `guard_files` (
  `id` bigint(20) NOT NULL,
  `name_bng` varchar(200) NOT NULL,
  `name_eng` varchar(200) DEFAULT NULL,
  `guard_file_category_id` bigint(20) NOT NULL,
  `file_number` bigint(20) NOT NULL,
  `office_id` int(11) NOT NULL,
  `office_name` varchar(255) DEFAULT NULL,
  `office_unit_id` bigint(20) NOT NULL,
  `office_unit_name` varchar(255) DEFAULT NULL,
  `office_unit_organogram_id` bigint(20) DEFAULT NULL,
  `office_unit_organogram_name` varchar(255) DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `guard_file_attachments`
--

DROP TABLE IF EXISTS `guard_file_attachments`;
CREATE TABLE `guard_file_attachments` (
  `id` int(11) NOT NULL,
  `guard_file_id` bigint(20) NOT NULL,
  `name_bng` varchar(255) DEFAULT NULL,
  `attachment_type` varchar(150) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `guard_file_categories`
--

DROP TABLE IF EXISTS `guard_file_categories`;
CREATE TABLE `guard_file_categories` (
  `id` int(11) NOT NULL,
  `name_bng` varchar(200) NOT NULL,
  `parent_id` bigint(20) NOT NULL DEFAULT '0',
  `office_id` int(11) NOT NULL,
  `office_unit_id` bigint(20) NOT NULL,
  `office_unit_organogram` bigint(20) DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `guard_files`
--
ALTER TABLE `guard_files`
  ADD PRIMARY KEY (`id`),
  ADD KEY `office_id` (`office_id`);

--
-- Indexes for table `guard_file_attachments`
--
ALTER TABLE `guard_file_attachments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `guard_file_categories`
--
ALTER TABLE `guard_file_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `office_id` (`office_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `guard_files`
--
ALTER TABLE `guard_files`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `guard_file_attachments`
--
ALTER TABLE `guard_file_attachments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `guard_file_categories`
--
ALTER TABLE `guard_file_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
