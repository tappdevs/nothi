-- Digital Signature
-- 28-03-2018 (Office DB)
ALTER TABLE `potrojari` ADD `digital_sign` TINYINT(2) NOT NULL DEFAULT '0' AFTER `modified`, ADD `sign_info` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `digital_sign`;
ALTER TABLE `potrojari_attachments` ADD `digital_sign` TINYINT(2) NOT NULL DEFAULT '0' AFTER `file_dir`, ADD `sign_info` TEXT NULL DEFAULT NULL AFTER `digital_sign`;

-- 03-01-2017 (Projapoti DB -- digital signature)
ALTER TABLE `employee_records` ADD `default_sign` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '0: electronic;1: soft,2:hard' AFTER `status`, ADD `hard_signature` TINYINT(1) NOT NULL DEFAULT '0' AFTER `default_sign`, ADD `soft_signature` TINYINT(1) NOT NULL DEFAULT '0' AFTER `hard_signature`;

-- 29-01-2018 (Projapoti DB --digital-signature)
ALTER TABLE `employee_records` ADD `cert_id` VARCHAR(50) NULL DEFAULT NULL AFTER `soft_signature`, ADD `cert_type` VARCHAR(10) NULL DEFAULT NULL AFTER `cert_id`, ADD `cert_provider` VARCHAR(20) NULL DEFAULT NULL AFTER `cert_type`, ADD `cert_serial` VARCHAR(250) NULL DEFAULT NULL AFTER `cert_provider`;

-- 30-01-2018 (Office DB --digital-signature)
ALTER TABLE `nothi_notes` ADD `digital_sign` TINYINT(2) NOT NULL DEFAULT '0' AFTER `note_description`, ADD `sign_info` TEXT NULL DEFAULT NULL AFTER `digital_sign`;
ALTER TABLE `nothi_note_signatures` ADD `digital_sign` TINYINT NULL DEFAULT '0' AFTER `signature_date`;

-- 31-01-2018 (OfficeDB --digital-signature)
ALTER TABLE `nothi_note_attachments` ADD `digital_sign` TINYINT(2) NOT NULL DEFAULT '0' AFTER `file_dir`, ADD `sign_info` TEXT NULL DEFAULT NULL AFTER `digital_sign`;
ALTER TABLE `nothi_note_attachments` CHANGE `sign_info` `sign_info` LONGTEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;

-- 19-04-2018 (projapoti DB)
ALTER TABLE `employee_records` CHANGE `default_sign` `default_sign` TINYINT(3) NOT NULL DEFAULT '0' COMMENT '0: electronic;1: soft,2:hard';