<!--Proggapon-->
<div class="templateWrapper">
    <div class="row customFontSize" style="font-size:13pt">
        <div class="col-md-12 col-xs-12 col-sm-12 text-center customFontSize" style="font-size:13pt">
            <div class="col-md-3 col-xs-3 col-sm-3 text-center"><a href="javascript:;" class="editable editable-click"
                                                                   id="left_slogan" data-type="textarea"
                                                                   data-pk="1">...</a></div>
            <div class="col-md-6 col-xs-6 col-sm-6 text-center customFontSize" style="font-size:13pt"><a
                    href="javascript:" class="editable editable-click" id="dynamic_header" data-type="textarea"
                    data-pk="1">...</a> <br><a href="javascript:;" class="editable editable-click" id="gov_name"
                                               data-type="text" data-pk="1"> <strong>...</strong> </a> <br/> <a
                    href="javascript:;" class="editable editable-click" id="office_ministry" data-type="text"
                    data-pk="1">... </a> <br/> <a href="javascript:;" sclass="editable editable-click" id="offices"
                                                  data-type="text" data-pk="1">...</a> <br/> <a href="javascript:;"
                                                                                                class="editable editable-click"
                                                                                                id="unit_name_editable"
                                                                                                data-type="text"
                                                                                                data-pk="1">...</a>
                <br/><a href="javascript:;" class="editable editable-click" id="web_url_or_office_address"
                        data-type="text" data-pk="1" data-original-title="অফিসের ঠিকানা বা ওয়েবসাইট -এর ঠিকানা লিখুন">...</a>
                <br/><a href="javascript:;" class="editable editable-click" id="office_address" data-type="text"
                        data-pk="1" data-original-title="অফিসের ঠিকানা লিখুন">...</a></div>
            <div class="col-md-3 col-xs-3 col-sm-3 customFontSize" style="font-size:13pt"><a href="javascript:;"
                                                                                             class="editable editable-click"
                                                                                             id="right_slogan"
                                                                                             data-type="textarea"
                                                                                             data-pk="1">...</a><br/> <a
                    href="javascript:;" class="editable editable-click" id="potro_priority" data-type="text" data-pk="1"
                    data-original-title="স্মারক নম্বর লিখুন">...</a> <br/> <a href="javascript:;"
                                                                              class="editable editable-click"
                                                                              id="potro_security" data-type="text"
                                                                              data-pk="1"
                                                                              data-original-title="স্মারক নম্বর লিখুন">...</a>
            </div>
        </div>
    </div>
    <br/>
    <div class="row customFontSize" style="font-size: 13pt!important;">
        <div class="col-md-6 col-xs-6 col-sm-6 text-left"> Number: <a href="javascript:;"
                                                                      class="editable editable-click" id="sharok_no"
                                                                      data-type="text" data-pk="1"
                                                                      data-original-title="Enter sharok number here">...</a>
        </div>
        <div class="col-md-6 col-xs-6 col-sm-6 text-right">
            <div style="text-align: right;"> Date: <a data-original-title="তারিখ বাছাই করুন" data-placement="right"
                                                      data-pk="1" data-viewformat="dd.mm.yyyy" data-type="date"
                                                      id="sending_date" href="#" class="editable editable-click">
                ... </a></div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12 text-center"><strong> <a href="javascript:;"
                                                                           class="editable editable-click"
                                                                           id="subject" data-type="text"
                                                                           data-pk="1"
                                                                           data-original-title="Enter Proggapon Title here">Notification</a>
        </strong></div>
    </div>
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12 text-left"><a id="pencil" href="#"> <i class="fa fa-pencil"></i>[Edit]</a>
            <textarea name="" id="note" style="display: none;"></textarea>
            <div data-original-title="Enter notes" data-toggle="manual" data-type="wysihtml5" data-pk="1" id="noteView"
                 class="editable" tabindex="-1" style="display:inline;"><br/>
                ----------------------------------------------------------------------------<br/>
                ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------।
                <br/> <br/> -----------------------------------------------------------------------------------<br/>
                ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------।
                <br/> <br/></div>
        </div>
    </div>
    <br/>
    <div style="text-align: center;">
        <div class="row customFontSize" style="font-size: 13pt!important;">
            <div class="col-md-5 col-xs-5 col-sm-5 col-md-offset-7 col-xs-offset-7 col-sm-offset-7"><a
                    href="javascript:;" class="editable editable-click" id="i_remain_sir" data-type="text" data-pk="1"
                    data-original-title="Enter regards here"> By order of President, </a></div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-5 col-xs-5 col-sm-5 col-md-offset-7 col-xs-offset-7 col-sm-offset-7 text-center"><span
                    id="sender_signature" data-type="text" data-original-title="Enter signature here">                    &nbsp;                </span>
                <span id="sender_signature_date"
                      style="                display: block;                font-size: 10px;                text-align: center;"></span>
            </div>
        </div>
        <br>
        <div class="row customFontSize" style="font-size: 13pt!important;">
            <div class="col-md-5 col-xs-5 col-sm-5 col-md-offset-7 col-xs-offset-7 col-sm-offset-7"><a
                    href="javascript:;" class="editable editable-click" id="sender_name" data-type="text" data-pk="1"
                    data-original-title="Enter sender name here"> ... </a></div>
        </div>
        <div class="row customFontSize" style="font-size: 13pt!important;">
            <div class="col-md-5 col-xs-5 col-sm-5 col-md-offset-7 col-xs-offset-7 col-sm-offset-7"><a
                    href="javascript:;" class="editable editable-click" id="sender_designation" data-type="text"
                    data-pk="1" data-original-title="Enter sender designation here"> ... </a></div>
        </div>
        <div class="row">
            <div class="col-md-5 col-xs-5 col-sm-5 col-md-offset-7 col-xs-offset-7 col-sm-offset-7"> Phone: <a
                    href="javascript:;" class="editable editable-click" id="sender_phone" data-type="text" data-pk="1"
                    data-original-title="Enter sender phone here"> ... </a></div>
        </div>
        <div class="row">
            <div class="col-md-5 col-xs-5 col-sm-5 col-md-offset-7 col-xs-offset-7 col-sm-offset-7"> Fax: <a
                    href="javascript:;" class="editable editable-click" id="sender_fax" data-type="text" data-pk="1"
                    data-original-title="Enter sender fax here"> ... </a></div>
        </div>
        <div class="row customFontSize" style="font-size: 13pt!important">
            <div class="col-md-5 col-xs-5 col-sm-5 col-md-offset-7 col-xs-offset-7 col-sm-offset-7"> Email: <a
                    href="javascript:;" class="editable editable-click" id="sender_email" data-type="text" data-pk="1"
                    data-original-title="Enter sender email here" style="font-size:  11px!important;"> ... </a></div>
        </div>
    </div>
    <br>
    <div class="row customFontSize" style="font-size: 13pt!important;">
        <div class="col-md-6 col-xs-6 col-sm-6 text-left"> Number: <a href="javascript:;"
                                                                      class="editable editable-click" id="sharok_no2"
                                                                      data-type="text" data-pk="1"
                                                                      data-original-title=" "> ... </a></div>
        <div class="col-md-6 col-xs-6 col-sm-6 ctext-right">
            <div style="text-align: right;"> Date: <span id="sending_date_2"> ... </span></div>
        </div>
    </div>
    <div class="row customFontSize" style="font-size: 13pt!important;">
        <div class="col-md-12 col-xs-12 col-sm-12 text-left"><a href="javascript:;" class="editable editable-click"
                                                                id="getarthe" data-type="text" data-pk="1"
                                                                data-original-title="">Copy for Kind Information and
            Necessary Action,</a> <br>
            <div id='cc_list_div'></div>
        </div>
    </div>
</div>