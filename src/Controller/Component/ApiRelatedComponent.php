<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\Routing\Router;
use Firebase\JWT\JWT;

/**
 * UploadFile component
 */
class ApiRelatedComponent extends Component
{
    protected function getTokenData($token, $secretKey = '', $algorithm = 'HS256')
    {
        try {
            if(empty($secretKey)){
                $secretKey = SECRET_KEY;
            }
            if (defined("TOKEN_CHECK") && TOKEN_CHECK) {
                return json_decode(json_encode(JWT::decode($token, $secretKey, array($algorithm))), true);
            } else {
                return API_KEY;
            }
        } catch (\Exception $ex) {
            return API_KEY;
        }

    }
    public function getMobileApiTokenData($token = ''){
        $options = [];
        try{
            $data = $this->getTokenData($token);
            if(!empty($data['data'])){
                $data = $data['data'];
                $options['user_name'] =getDecryptedData($data['un']);
                $options['device_id'] = $data['di'];
                $options['device_type'] = $data['dt'];
                $options['employee_record_id'] = getDecryptedData($data['er']);
                $options['designations'] = [];
                if(!empty($data['as'])){
                    foreach($data['as'] as $designation){
                        $options['designations'][] =getDecryptedData($designation);
                    }
                }
            }
        }catch (Exception $ex){
        }
        return $options;
    }
}