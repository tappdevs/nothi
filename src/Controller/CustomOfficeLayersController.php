<?php

namespace App\Controller;

use App\Controller\ProjapotiController;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\TableRegistry;

class CustomOfficeLayersController extends ProjapotiController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
//        pr($this->request->param('action'));die;
        $this->loadComponent('Paginator');
        $this->Auth->allow(['apiGetCustomLayerWiseOffices']);
        $allowable_actions = ['apiGetCustomLayerWiseOffices'];
        if(!in_array($this->request->param('action'),$allowable_actions)){
            $user = $this->Auth->user();
            if(empty($user) || $user['user_role_id']> 2){
                return $this->redirect('/login');
            }
        }
    }
    public function index(){
        try {
            $custom_office_layer_data = TableRegistry::get('OfficeCustomLayers')->find();
            $data = $this->paginate($custom_office_layer_data);
            $this->set('data', $data);
        } catch (\Exception $e) {
            $this->redirect('/login');
        }
    }
    public function add()
    {
        $response = [
            'status' => 'error',
            'msg' => 'something went wrong'
        ];
        if ($this->request->is(['post', 'ajax']) && !empty($this->request->data['text'])) {
            try{
                $text     = h($this->request->data['text']);
                $text_eng     = h($this->request->data['text_eng']);
                $layer_level     = h($this->request->data['layer_level']);

                $table                 = TableRegistry::get('OfficeCustomLayers');
                $data                  = $table->newEntity();
                $data->name = $text;
                $data->name_eng = $text_eng;
                $data->layer_level = $layer_level;
                $data->created_by      = $this->Auth->user('id');
                $data->modified_by     = $this->Auth->user('id');
                if ($table->save($data)) {
                    $response = [
                        'status' => 'success',
                        'msg' => 'সংরক্ষিত করা হয়েছে',
                    ];
                    $this->Flash->success('সংরক্ষিত করা হয়েছে');
                }
            }catch(\Exception $e){
                $response['msg'] = 'টেকনিকাল ত্রুটির কারণে সংরক্ষণ সম্ভব নয়।';
                $response['reason'] = makeEncryptedData($e->getMessage());
            }

        }
        $this->response->type('json');
        $this->response->body(json_encode($response));
        return $this->response;
    }

    public function update()
    {
        $response = [
            'status' => 'error',
            'msg' => 'something went wrong'
        ];
        if ($this->request->is(['post', 'ajax']) && isset($this->request->data['text']) && isset($this->request->data['id'])) {
            $employee = $this->getCurrentDakSection();
            $text     = h($this->request->data['text']);
            $text_eng     = h($this->request->data['text_eng']);
            $layer_level     = h($this->request->data['layer_level']);
            $id = getDecryptedData($this->request->data['id']);
            if(empty($id)){
                $response['msg'] = 'কোন তথ্য পাওয়া যায়নি';
                goto rtn;
            }
            try{
                $table                 = TableRegistry::get('OfficeCustomLayers');
                $data                  = $table->get(intval($id));
                if(empty($data) || $data['created_by'] != $this->Auth->user('id')){
                    $response['msg'] = 'কোন তথ্য পাওয়া যায়নি';
                    goto rtn;
                }
                $data->name = $text;
                $data->name_eng = $text_eng;
                $data->layer_level = $layer_level;
                if ($table->save($data)) {
                    $response = [
                        'status' => 'success',
                        'msg' => 'সংশোধন করা হয়েছে',
                    ];
                    $this->Flash->success('সংশোধন করা হয়েছে');
                }
            }catch (\Exception $e){
                $response['msg'] = 'টেকনিকাল ত্রুটির কারণে সংশোধন সম্ভব নয়।';
                $response['reason'] = makeEncryptedData($e->getMessage());
            }

        }
        rtn:
        $this->response->type('json');
        $this->response->body(json_encode($response));
        return $this->response;
    }

    public function delete()
    {
        $response = [
            'status' => 'error',
            'msg' => 'something went wrong'
        ];
        if ($this->request->is(['post', 'ajax']) && isset($this->request->data['id'])) {
            $table    = TableRegistry::get('OfficeCustomLayers');
            $id = getDecryptedData($this->request->data['id']);
            if(empty($id)){
                $response['msg'] = 'কোন তথ্য পাওয়া যায়নি';
                goto rtn;
            }
            try{
                $data     = $table->get(intval($id));
                if(empty($data)){
                    $response['msg'] = 'কোন তথ্য পাওয়া যায়নি';
                    goto rtn;
                }
                if ($data['created_by'] == $this->Auth->user('id')) {
                    // check if this custom layer has any entry in offices table
                    $custom_layer_count = TableRegistry::get('Offices')->getCustomLayerInfo($id)->count();
                    if($custom_layer_count > 0){
                        $response['msg'] =' দুঃখিত কাস্টম লেয়ার দিয়ে '.enTobn($custom_layer_count). 'টি অফিস ম্যাপিং করা হয়েছে।ম্যাপিং থাকাকালে লেয়ার বাতিল করা অনুমোদিত নয়।';
                        goto rtn;
                    }
                    if ($table->delete($data)) {
                        $response = [
                            'status' => 'success',
                            'msg' => 'মুছে ফেলা হয়েছে',
                        ];
                        $this->Flash->success('মুছে ফেলা হয়েছে');
                    }
                    else {
                        $response['msg'] =' দুঃখিত পুনরায় চেষ্টা করুন ';
                    }
                }
                else {
                    $response['msg'] = ' দুঃখিত আপনি অনুমতিপ্রাপ্ত নয়। ';
                }
            }catch (\Exception $e){
                $response['msg'] = 'টেকনিকাল ত্রুটির কারণে মুছে ফেলা সম্ভব নয়।';
                $response['reason'] = makeEncryptedData($e->getMessage());
            }

        }
        rtn:
        $this->response->type('json');
        $this->response->body(json_encode($response));
        return $this->response;
    }
    public function mapInfos(){
        $table                 = TableRegistry::get('OfficeCustomLayers');
        $data = $table->find('list',['keyField'=> 'id','valueField' =>'name'])->toArray();
        $custom_layer_level = !empty($this->request->query['custom_layer_level'])?getDecryptedData(str_replace(' ','+',$this->request->query['custom_layer_level'])):0;
        if(!empty($data)){
            foreach($data as $k => $val){
                $enc_k =$this->makeEncryptedData($k);
                if($custom_layer_level== $k){
                    $this->set('custom_layer_level',$enc_k);
                }
                $data[$enc_k] = $val;
                unset($data[$k]);
            }
        }
        $this->set(compact('data'));
    }
    public function getCustomLayerWiseOffices(){
        $response = [
            'status' => 'error',
            'msg' => 'something went wrong'
        ];
        if ($this->request->is(['post', 'ajax']) && isset($this->request->data['custom_office_layer_type'])) {
            $custom_office_layer_type = $this->getDecryptedData($this->request->data['custom_office_layer_type']);
            $table                 = TableRegistry::get('OfficeCustomLayers');
            $all_Offices = [];
            if(($custom_office_layer_type == 0)){
                $all_Offices =$table->getCustomLayerWiseOffices($custom_office_layer_type)->select(['id','custom_layer_id','office_name_bng'])->toArray();
            }else{
                $all_Offices =$table->getCustomLayerWiseOffices($custom_office_layer_type)->select(['id','custom_layer_id','office_name_bng'])->toArray();
            }
            $data = $table->find('list',['keyField'=> 'id','valueField' =>'name'])->toArray();
            if(!empty($all_Offices)){
                foreach($all_Offices as &$val){
                    $val['custom_layer_name'] = isset($data[$val['custom_layer_id']])?$data[$val['custom_layer_id']]:'';
                    $val['custom_layer_id'] = $this->makeEncryptedData($val['custom_layer_id']);
                }
            }
            $response = [
                'status' => 'success',
                'data' => $all_Offices
            ];
        }
        rtn:
        $this->response->type('json');
        $this->response->body(json_encode($response));
        return $this->response;
    }
    public function apiGetCustomLayerWiseOffices(){
        $response = [
            'status' => 'error',
            'msg' => 'Invalid request'
        ];
        $apikey = isset($this->request->data['api_key']) ? $this->request->data['api_key']
            : '';
//        $this->checkToken($apikey) == FALSE
        if (empty($apikey) || $apikey != API_KEY) {
            goto rtn;
        }
        $custom_office_layer_type = $this->request->data['custom_office_layer_type'];
        $table                 = TableRegistry::get('OfficeCustomLayers');
        $all_Offices = [];
        if(empty($custom_office_layer_type)){
            $response['msg'] = 'required info was missing';
            goto rtn;
        }else{
            $all_Offices =$table->getCustomLayerWiseOffices($custom_office_layer_type)->select(['id','office_name_bng'])->toArray();
        }
        $data = [];
        if(!empty($all_Offices)){
            foreach($all_Offices as $val){
                $data[$val['id']] = $val['office_name_bng'];
            }
        }
        $response = [
            'status' => 'success',
            'data' => $data
        ];
        rtn:
        $this->response->type('json');
        $this->response->body(json_encode($response));
        return $this->response;
    }
    public function ministryWiseMap(){
        if ($this->request->is('get')) {
            $table_instance = TableRegistry::get('OfficeMinistries');
            $data_items = $table_instance->find('list')->toArray();
            $this->set('officeMinistries', $data_items);
            $custom_layers_table = TableRegistry::get('OfficeCustomLayers');
            $all_custom_layers =$custom_layers_table->getCustomLayerList('id','name',[])->toArray();
            $this->set(compact('all_custom_layers'));
        }
        if ($this->request->is('post')) {
            $response = [
                'status' => 'error',
                'msg' => 'Invalid request'
            ];
            try{
                $ministry_id = $this->request->data['ministry_id'];
                if(empty($ministry_id)){
                    goto rtn;
                }
                $office_layers_table = TableRegistry::get('OfficeLayers');
                $all_layers = $office_layers_table->getAll(['OfficeLayers.office_ministry_id'=>$ministry_id],['id','layer_name_bng','custom_layer_id']);
//                if(!empty($all_layers)){

//                }
                $response = [
                    'status' => 'success',
                    'data' => $all_layers
                ];
            }catch (\Exception $ex){
                $response['msg'] = 'Technical Error happen';
                $response['reason'] = $this->makeEncryptedData($ex->getMessage());
            }

            rtn:
            $this->response->type('json');
            $this->response->body(json_encode($response));
            return $this->response;
        }
    }
    function customLayerEdit(){
        $response = [
            'status' => 'error',
            'msg' => 'Invalid request'
        ];
        $layer_id = $this->request->data['layer_id'];
        $custom_layer_id = $this->request->data['custom_layer_id'];
        if(empty($layer_id) || empty($custom_layer_id)){
            $response['msg'] = 'Required info missing';
            goto rtn;
        }
        try{
            $offices_table = TableRegistry::get('Offices');
            $layers_table = TableRegistry::get('OfficeLayers');
            if($layers_table->updateAll(['custom_layer_id' => $custom_layer_id],['id' => $layer_id]) > 0){
                $offices_table->updateAll(['custom_layer_id' => $custom_layer_id],['office_layer_id' => $layer_id]);
            }
            $response = [
                'status' => 'success',
                'msg' => 'তথ্য সংশোধন হয়েছে'
            ];
        }catch (\Exception $ex){
            $response['msg'] = 'Technical Error happen';
            $response['reason'] = $this->makeEncryptedData($ex->getMessage());
        }

        rtn:
        $this->response->type('json');
        $this->response->body(json_encode($response));
        return $this->response;
    }
}