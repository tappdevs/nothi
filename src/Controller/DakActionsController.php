<?php

namespace App\Controller;

use App\Controller\ProjapotiController;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;
use Cake\Network\Email\Email;

class DakActionsController extends ProjapotiController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->loadComponent('Paginator');
        $this->Auth->allow(['apiDecisions', 'postApiDecisions']);
    }

    public function index()
    {
        if($this->request->is('get')){
            $refer_url = $this->referer();
            if(stripos($refer_url,'dakActions/index')){
                $refer_url = Router::url(['controller' => 'Dashboard', 'action' => 'dashboard'], true);
            }
            $this->set('dak_action_refer_url',$refer_url);
        }
        $table    = TableRegistry::get('DakActionsMig');
        $table_employee    = TableRegistry::get('DakActionEmployees');
        $employee = $this->getCurrentDakSection();
        if(empty($employee['officer_id']) && ($this->Auth->user('user_role_id')<= 2)){
            $employee['officer_id']=0;
        }
        $status_data = $table_employee->find('list',
            ['keyField' => 'dak_action_id', 'valueField' => 'dak_action_id'])->where(['employee_id'=>$employee['officer_id']])->toArray();
        $data = $table->GetDakActions($employee['officer_id'])->toArray();

        $this->set('status_data', $status_data);
        $this->set('employee', $employee);
        try {
            $this->set('data', $data);
        } catch (\Exception $e) {
            $this->Flash->error('টেকনিক্যাল ত্রুটি হয়েছে');
            $this->redirect(['action' => 'index']);
        }
    }

    public function updateStatus(){
        if ($this->request->is(['post', 'ajax']) && isset($this->request->data['ids'])) {
            $employee = $this->getCurrentDakSection();
            $table_employee    = TableRegistry::get('DakActionEmployees');
            $admin = false;
            if(empty($employee['officer_id']) && ($this->Auth->user('user_role_id')<= 2)){
                $employee['officer_id']=0;
                $admin = true;
            }
            $table_employee->deleteAll(['employee_id'=>$employee['officer_id']]);
            $datas=[];
            foreach ($this->request->data['ids'] as $id){

                $data['dak_action_id']= $id;
                $data['employee_id']  = $employee['officer_id'];
                $data['created_by']  = $this->Auth->user('id');
                $data['modified_by']  = $this->Auth->user('id');
                $datas[]=$data;

            }
            $all_data = $table_employee->newEntities($datas);
            foreach ($all_data as $all_data){
                $table_employee->save($all_data);
            }
            if($admin == true){
                $table    = TableRegistry::get('DakActionsMig');
                $table->updateAll(['status'=>1],['id IN'=>$this->request->data['ids'],'creator'=>0]);
                $table->updateAll(['status'=>0],['id NOT IN'=>$this->request->data['ids'],'creator'=>0]);
            }
            $this->Flash->success('সংরক্ষিত করা হয়েছে');
        }
        die;
    }

    public function add()
    {
        if ($this->request->is(['post', 'ajax']) && isset($this->request->data['text'])) {
            $employee = $this->getCurrentDakSection();
            $text     = htmlspecialchars($this->request->data['text']);
            if(empty($text)){
                $this->Flash->error('সংরক্ষন করা সম্ভব হচ্ছে না। পুনরায় চেস্টা করুন।');
                die;
            }

            $table_employee        = TableRegistry::get('DakActionEmployees');
            $table                 = TableRegistry::get('DakActionsMig');
            $data                  = $table->newEntity();
            $data->dak_action_name = $text;
            $data->status          = 1;
            $data->created_by      = $this->Auth->user('id');
            $data->modified_by     = $this->Auth->user('id');
            if(empty($employee['officer_id']) && ($this->Auth->user('user_role_id')<= 2)){
                $employee['officer_id']=0;
            }
            $data->creator         = $employee['officer_id'];
            if ($action=$table->save($data)) {

                $dak_action_employee = $table_employee->newEntity();
                $dak_action_employee->dak_action_id= $action->id;
                $dak_action_employee->employee_id  = $employee['officer_id'];
                $dak_action_employee->created_by  = $this->Auth->user('id');
                $dak_action_employee->modified_by  = $this->Auth->user('id');
                $table_employee->save($dak_action_employee);

                if (isset($this->request->data['dak'])) {
                    echo 'সংরক্ষিত করা হয়েছে';
                    die;
                }
                $this->Flash->success('সংরক্ষিত করা হয়েছে');
            }
        }
        die;
    }

    public function update()
    {
        if ($this->request->is(['post', 'ajax']) && isset($this->request->data['text']) && isset($this->request->data['id'])) {
            $employee = $this->getCurrentDakSection();
            $text     = htmlspecialchars($this->request->data['text']);
            if(empty($text)){
                $this->Flash->error('সংরক্ষন করা সম্ভব হচ্ছে না। পুনরায় চেস্টা করুন।');
                die;
            }

            $table                 = TableRegistry::get('DakActionsMig');
            $data                  = $table->get(intval($this->request->data['id']));
            $data->dak_action_name = $text;
            $data->status          = 1;
            if(empty($employee['officer_id']) && ($this->Auth->user('user_role_id')<= 2)){
                $employee['officer_id']=0;
            }
            $data->creator         = $employee['officer_id'];

            if ($table->save($data)) {
                $this->Flash->success('সংশোধন করা হয়েছে');
            }
        }
        die;
    }

    public function delete()
    {
        if ($this->request->is(['post', 'ajax']) && isset($this->request->data['id'])) {
            $employee = $this->getCurrentDakSection();
            $table    = TableRegistry::get('DakActionsMig');
            $table_employee    = TableRegistry::get('DakActionEmployees');
            $data     = $table->get(intval($this->request->data['id']));
            if(empty($employee['officer_id']) && ($this->Auth->user('user_role_id')<= 2)){
                $employee['officer_id']=0;
            }
            if ($data->creator == $employee['officer_id']) {
                if ($table->delete($data) && $table_employee->deleteAll(['dak_action_id'=>intval($this->request->data['id'])])) {
                    $this->Flash->success('মুছে ফেলা হয়েছে');
                }
                else {
                    $this->Flash->error(' দুঃখিত পুনরায় চেষ্টা করুন ');
                }
            }
            else {
                $this->Flash->error(' দুঃখিত আপনি অনুমতিপ্রাপ্ত নয়। ');
            }
        }
        die;
    }

    public function GetDakActions()
    {
        $data                       = '';
        if ($this->request->is(['ajax','post'])) {
            $table_instance_dak_action_employee = TableRegistry::get('DakActionEmployees');
            if(empty($user['employee_record_id']) && ($this->Auth->user('user_role_id')<= 2)){
                $user['employee_record_id']=0;
            }
            $user                       = $this->Auth->user();
            $dak_actions = $table_instance_dak_action_employee->GetDakActionsByEmployee($user['employee_record_id']);


            foreach ($dak_actions as $dak_action) {
                $data .= '<tr>
                            <td >
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="radio" name="Radios" onclick="putthis('.$dak_action['id'].')"  >
                                    </label>
                                </div>
                            </td>
                            <td class="text-left" id="action'.$dak_action['id'].'">'.$dak_action['dak_action_name'].'</td>
                          </tr>';
            }
        }
        $this->response->body(json_encode(['status' => 'success', 'data' => $data]));
        $this->response->type('application/json');
        return $this->response;
    }

    public function apiDecisions()
    {
        $user_designation = $this->request->query['user_designation'];
        $jsonArray        = array(
            "status" => "error",
            "error_code" => 404,
            "message" => "Invalid Request"
        );

        $apikey = isset($this->request->query['api_key']) ? $this->request->query['api_key'] : '';
        if (empty($apikey) || $this->checkToken($apikey) == FALSE) {
            echo json_encode($jsonArray);
            die;
        }

        if (!defined("PROJAPOTI_API") || PROJAPOTI_API == false) {
            echo json_encode($jsonArray);
            die;
        }

        if (empty($user_designation)) {
            echo json_encode($jsonArray);
            die;
        }
        if($apikey != API_KEY){
            //verify api token
            $this->loadComponent('ApiRelated');
            $getApiTokenData = $this->ApiRelated->getMobileApiTokenData($apikey);
            if(!empty($getApiTokenData)){
                if(!in_array($user_designation,$getApiTokenData['designations'])){
                    $jsonArray['message'] = 'Unauthorized request';
                    echo json_encode($jsonArray);die;
                }
            }
            //verify api token
        }
        $EmployeeOfficesTable = TableRegistry::get('EmployeeOffices');
        $employee_office      = $EmployeeOfficesTable->find()->where(['office_unit_organogram_id' => $user_designation,
                'status' => 1])->first();

        $this->switchOffice($employee_office['office_id'], "apiOffice");

        $table = TableRegistry::get('DakActionEmployees');

        $data = $table->GetDakActionsByEmployee($employee_office['employee_record_id']);

        $this->response->body(json_encode(['status' => 'success', 'data' => $data]));
        $this->response->type('application/json');
        return $this->response;
    }

    public function postApiDecisions()
    {
        $user_designation = $this->request->data['user_designation'];
        $jsonArray        = array(
            "status" => "error",
            "error_code" => 404,
            "message" => "Invalid Request"
        );

        $apikey = isset($this->request->data['api_key']) ? $this->request->data['api_key'] : '';
        if ( empty($apikey) || $this->checkToken($apikey) == FALSE) {
            echo json_encode($jsonArray);
            die;
        }

        if (!defined("PROJAPOTI_API") || PROJAPOTI_API == false) {
            echo json_encode($jsonArray);
            die;
        }

        if (empty($user_designation)) {
            echo json_encode($jsonArray);
            die;
        }
        if($apikey != API_KEY){
            //verify api token
            $this->loadComponent('ApiRelated');
            $getApiTokenData = $this->ApiRelated->getMobileApiTokenData($apikey);
            if(!empty($getApiTokenData)){
                if(!in_array($user_designation,$getApiTokenData['designations'])){
                    $jsonArray['message'] = 'Unauthorized request';
                    echo json_encode($jsonArray);die;
                }
            }
            //verify api token
        }
        $EmployeeOfficesTable = TableRegistry::get('EmployeeOffices');
        $employee_office      = $EmployeeOfficesTable->find()->where(['office_unit_organogram_id' => $user_designation,
                'status' => 1])->first();

        $this->switchOffice($employee_office['office_id'], "apiOffice");

        $table = TableRegistry::get('DakActionEmployees');

        $data = $table->GetDakActionsByEmployee($employee_office['employee_record_id']);

        $this->response->body(json_encode(['status' => 'success', 'data' => $data]));
        $this->response->type('application/json');
        return $this->response;
    }


}