<?php

namespace App\Controller;

use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\I18n\I18n;
use Cake\I18n\Time;
use Cake\I18n\Number;
use Cake\Datasource\ConnectionManager;

class DakMovementsController extends ProjapotiController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['APIDakDetail', 'APIDakList', 'APIForwardSelectedDak',
            'APIUploadImage', 'apiViewDak', 'apiDownloadDakAttachment', 'apiArchiveDak',
            'dakMovementHistory', 'apiGetSeal', 'dakMakeArchive', 'postAPIDakList', 'postApiViewDak',
            'postAPIDakDetail', 'postApiGetSeal', 'postApiArchiveDak', 'postAPIForwardSelectedDak',
            'popupPotro', 'postApiSealSave', 'postApiSealDelete']);
    }

    public function dakInbox($type = 'inbox')
    {
        $this->layout = null;

        /* Dak Office Selection */
        $session = $this->request->session();
        $this->set('dak_inbox_group', $type);

        $table_instance_emp_records = TableRegistry::get('EmployeeRecords');
        $user = $this->Auth->user();

        if ($user['employee_record_id'] != null) {
            $employee_info = $table_instance_emp_records->get($user['employee_record_id']);

            $table_instance_emp_offices = TableRegistry::get('EmployeeOffices');
            $employee_office_records = $table_instance_emp_offices->getEmployeeOfficeRecords($employee_info->id);

            $data_item = array();
            $data_item['personal_info'] = $employee_info;
            $data_item['office_records'] = count($employee_office_records) > 0 ? $employee_office_records
                : array();

            $table_instance_offices = TableRegistry::get('Offices');
            $table_instance_ministris = TableRegistry::get('OfficeMinistries');
            $officeInformation = array();
            $ministryInformation = array();
            if (!empty($employee_office_records[0]['office_id'])) {
                $officeInformation = $table_instance_offices->get($employee_office_records[0]['office_id']);

                if (!empty($officeInformation)) {
                    $ministryInformation = $table_instance_ministris->get($officeInformation['office_ministry_id']);
                }
            }

            $data_item['ministry_records'] = count($ministryInformation) > 0 ? $ministryInformation : array();

            if (!$session->check('logged_employee_record')) {
                $session->write('logged_employee_record', $data_item);
            }

            if (!empty($this->request->data['selected_office_unit_id'])) {
                $office_section['office_id'] = $this->request->data['selected_office_id'];
                $office_section['office_unit_id'] = $this->request->data['selected_office_unit_id'];
                $office_section['office_unit_organogram_id'] = $this->request->data['selected_office_unit_organogram_id'];
                $office_section['officer_id'] = $user['employee_record_id'];
                $office_section['incharge_label'] = $employee_office_records[0]['incharge_label'];

                $office_section['is_admin'] = $employee_office_records[0]['is_admin'];

                $table_instance_office = TableRegistry::get('Offices');
                $office = $table_instance_office->get($office_section['office_id']);

                $office_section['office_name'] = $office['office_name_bng'];
                $office_section['office_address'] = $office['office_address'];
                $office_section['designation_label'] = $this->request->data['selected_office_unit_organogram_label'];
                $office_section['officer_name'] = $employee_info['name_bng'];
                $ministryInformation = $table_instance_ministris->get($office['office_ministry_id']);
                $office_section['ministry_records'] = count($ministryInformation) > 0 ? $ministryInformation['name_bng']
                    : '';

                $office_section['office_phone'] = $office['office_phone'];
                $office_section['office_fax'] = $office['office_fax'];
                $office_section['office_email'] = $office['office_email'];
                $office_section['office_web'] = $office['office_web'];

                $table_instance_office_unit = TableRegistry::get('OfficeUnits');
                $office_unit = $table_instance_office_unit->get($office_section['office_unit_id']);
                $office_section['office_unit_name'] = $office_unit['unit_name_bng'];

                $session->write('selected_office_section', $office_section);
            }
        }

        $selected_office_section = $session->read('selected_office_section');
        if (empty($selected_office_section)) {
            return $this->redirect(array('action' => 'dashboard', 'controller' => 'dashboard'));
        }
        $table_instance_dak_action_employee = TableRegistry::get('DakActionEmployees');
        if (empty($user['employee_record_id']) && ($this->Auth->user('user_role_id') <= 2)) {
            $user['employee_record_id'] = 0;
        }

        $dak_actions = $table_instance_dak_action_employee->GetDakActionsByEmployee($user['employee_record_id']);

        $this->set('dak_actions', $dak_actions);
        $this->set('selected_office_section', $session->read('selected_office_section'));
    }

    public function inboxContent($dak_inbox_group)
    {
        $this->layout = null;
        $this->view = 'inbox_content_new';

        $user = $this->Auth->user();

        $EmployeeOfficesTable = TableRegistry::get('EmployeeOffices');
        $employee_office = $EmployeeOfficesTable->find()->where(['employee_record_id' => $user['employee_record_id'],
            'status' => 1])->first();

        $this->set('dak_inbox_group', $dak_inbox_group);
        $table_instance_dak_actions = TableRegistry::get('DakActions');
        $this->set('dak_actions',
            $table_instance_dak_actions->GetDakActions($employee_office['office_unit_organogram_id']));
        $session = $this->request->session();
        $selected_office_section = $session->read('selected_office_section');
        $this->set(compact('selected_office_section'));

        //end
    }

    public function daklist_nothivukto()
    {
        $this->layout = null;
        $table_instance_dm = TableRegistry::get('DakMovements');
        $employee_office = $selected_office_section = $this->getCurrentDakSection();
        $this->set(compact('selected_office_section'));

        $response = $table_instance_dm->getDakList('nothivukto', $employee_office, $this);

        $json_data = array(
            "draw" => isset($this->request->data['draw']) ? intval($this->request->data['draw']) : 1,
            "recordsTotal" => $response['total'],
            "recordsFiltered" => $response['total'],
            "data" => $response['data']
        );
        echo json_encode($json_data);
        die;
    }

    public function daklist_nothijato()
    {
        $this->layout = null;
        $table_instance_dm = TableRegistry::get('DakMovements');
        $employee_office = $selected_office_section = $this->getCurrentDakSection();
        $this->set(compact('selected_office_section'));

        $response = $table_instance_dm->getDakList('nothijato', $employee_office, $this);

        $json_data = array(
            "draw" => isset($this->request->data['draw']) ? intval($this->request->data['draw']) : 1,
            "recordsTotal" => $response['total'],
            "recordsFiltered" => $response['total'],
            "data" => $response['data']
        );

        echo json_encode($json_data);
        die;
    }

    public function daklist_inbox($nothiDecision = 0)
    {
        $this->layout = null;
        $table_instance_dm = TableRegistry::get('DakMovements');
        $employee_office = $selected_office_section = $this->getCurrentDakSection();
        $this->set(compact('selected_office_section'));

        $response = $table_instance_dm->getDakList('inbox', $employee_office, $this, $nothiDecision);

        $json_data = array(
            "draw" => isset($this->request->data['draw']) ? intval($this->request->data['draw']) : 1,
            "recordsTotal" => $response['total'],
            "recordsFiltered" => $response['total'],
            "data" => $response['data']
        );
        echo json_encode($json_data);
        die;
    }

    public function daklist_onulipi($nothiDecision = 0)
    {
        $this->layout = null;
        $table_instance_dm = TableRegistry::get('DakMovements');
        $employee_office = $selected_office_section = $this->getCurrentDakSection();
        $this->set(compact('selected_office_section'));
        if ($nothiDecision == 'archive') {
            $response = $table_instance_dm->getDakList('archive', $employee_office, $this, $nothiDecision);
        } else {
            $response = $table_instance_dm->getDakList('onulipi', $employee_office, $this, $nothiDecision);
        }

        $json_data = array(
            "draw" => isset($this->request->data['draw']) ? intval($this->request->data['draw']) : 1,
            "recordsTotal" => $response['total'],
            "recordsFiltered" => $response['total'],
            "data" => $response['data']
        );

        echo json_encode($json_data);
        die;
    }

    public function daklist_sent()
    {
        $this->layout = null;
        $table_instance_dm = TableRegistry::get('DakMovements');
        $employee_office = $selected_office_section = $this->getCurrentDakSection();
        $this->set(compact('selected_office_section'));

        $response = $table_instance_dm->getDakList('sent', $employee_office, $this);

        $json_data = array(
            "draw" => isset($this->request->data['draw']) ? intval($this->request->data['draw']) : 1,
            "recordsTotal" => $response['total'],
            "recordsFiltered" => $response['total'],
            "data" => $response['data']
        );

        echo json_encode($json_data);
        die;
    }

    public function deleteDak($dakType = "Nagorik")
    {
        $this->layout = 'ajax';

        $user = $this->getCurrentDakSection();
        TableRegistry::remove('DakUsers');
        $dakUsersTable = TableRegistry::get('DakUsers');
        if ($this->request->is('ajax')) {
            if (!empty($this->request->data)) {
                $dak_id = intval($this->request->data['message_id']);

                $getDak = $dakUsersTable->find()->where(['dak_id' => $dak_id, 'dak_type' => $dakType,
                    'to_office_id' => $user['office_id'],
                    'to_officer_designation_id' => $user['office_unit_organogram_id'], 'dak_category' => DAK_CATEGORY_INBOX, 'is_archive' => 0])->first();

                if (!empty($getDak)) {
                    $dakUsersTable->updateAll(['is_archive'=>1, 'modified' => date('Y-m-d H:i:s')],['id'=>$getDak['id']]);

                    echo json_encode(array('status' => 'success', 'msg' => 'ডাকটি আর্কাইভ হয়েছে '));
                } else {
                    echo json_encode(array('status' => 'error', 'msg' => 'তথ্য পাওয়া যায়নি'));
                }
            } else {
                echo json_encode(array('status' => 'error', 'msg' => 'তথ্য পাওয়া যায়নি'));
            }
        }

        die;
    }

    public function viewDakDaptorik($archive = 0)
    {
        $this->layout = null;
//        $this->view = 'view_dak_daptorik_new';

        $id = !empty($this->request->query['message_id']) ? (int)$this->request->query['message_id'] : 0;
        $page = !empty($this->request->query['page']) ? (int)$this->request->query['page'] : 0;
        $si = !empty($this->request->query['si']) ? (int)$this->request->query['si'] : 0;
        $permitted_organogram = !empty($this->request->query['organogramId']) ? (int)$this->request->query['organogramId'] : null;

        $conditions = "";

        if (!empty($this->request->query['conditions'])) {
            $conditionsKey = array_values($this->request->query['conditions']);

            foreach ($conditionsKey as $key => $cond) {
                $condKey = array_keys($cond);
                $condValue = array_values($cond);
                if (empty($condValue[0]) || empty($condKey[0])) continue;
                if (!empty($conditions)) $conditions .= " AND ";

                if ($condKey[0] == 'receiving_office_unit_id') {
                    $conditions .= "{$condKey[0]} = {$condValue[0]}";
                } else if ($condKey[0] == 'from') {
                    $conditions .= "date(modified) >= '%{$condValue[0]}%'";
                } else if ($condKey[0] == 'to') {
                    $conditions .= "date(modified) <= '%{$condValue[0]}%'";
                } else $conditions .= "{$condKey[0]} LIKE '%{$condValue[0]}%'";
            }
        }
        $this->set('dak_id', $id);
        $this->set('dak_type', DAK_DAPTORIK);
        $this->set('archive', $archive);
        $this->set('permitted_organogram', $permitted_organogram);

        $employee_office = $this->getCurrentDakSection();
        $table_instance_dm = TableRegistry::get('DakMovements');


        $table_instance_dak_user = TableRegistry::get('DakUsers');
        $dakIds = array();
        $user_new_daks = $table_instance_dak_user->find()->select(['dak_id'])->where(['dak_category' => DAK_CATEGORY_INBOX,
            'to_officer_designation_id' => $employee_office['office_unit_organogram_id'],
            'dak_type' => DAK_DAPTORIK])->order(['dak_id DESC'])->toArray();

        $index = 0;
        foreach ($user_new_daks as $new_dak) {
            $dakIds[] = $new_dak['dak_id'];
            if (defined("PAGINATE_TYPE") && PAGINATE_TYPE == 2 && $new_dak['dak_id'] == $id) {
                $si = $index + 1;
            }
            $index++;
        }


        $table_instance_dd = TableRegistry::get('DakDaptoriks');
        $totalRec = $table_instance_dd->find()->where(['id IN' => $dakIds])->where([$conditions])->order("DakDaptoriks.id DESC")->count();

        if (defined("PAGINATE_TYPE") && PAGINATE_TYPE == 1) {
            $totalRec = !empty($this->request->query['totalRec']) ? $this->request->query['totalRec']
                : 0;
        }

        $this->set('totalRec', $totalRec);
        if (!empty($id)) {
            $dak_entity = $table_instance_dd->get($id);
        } else {
            if (!empty($conditions)) {
                $dak_entity = $table_instance_dd->find()->where(['id IN' => $dakIds])->where($conditions)->order("DakDaptoriks.id DESC")->page($si,
                    1)->first();
            } else {
                $dak_entity = $table_instance_dd->find()->where(['id IN' => $dakIds])->order("DakDaptoriks.id DESC")->page($si,
                    1)->first();
            }
        }

        $this->set(compact('si'));

        if ($dak_entity['is_summary_nothi']) {
            $summary_nothi_users_table = TableRegistry::get('SummaryNothiUsers');
            $summary_nothi_users = $summary_nothi_users_table->find()->where(['tracking_id' => bnToen($dak_entity['sender_sarok_no'])])->order(['id' => 'desc', 'sequence_number' => 'desc'])->first();

            if ($summary_nothi_users['is_approve'] == 1) {
                $dak_entity['show_utsonothi_button'] = true;
            } else {
                $dak_entity['show_utsonothi_button'] = false;
            }
        }
        $this->set('dak_details', $dak_entity);
        $rollback = -1;
        $nothiJato = 0;
        if ($dak_entity['is_rollback_to_dak'] == 1) {
            $nothiJato = 1;
        }

        $dakUsers = $table_instance_dak_user->find()->where(['dak_id' => $dak_entity['id'], 'dak_type' => DAK_DAPTORIK, 'to_officer_designation_id' => $employee_office['office_unit_organogram_id'], 'dak_category' => 'Inbox'])->toArray();
        if (!$table_instance_dm->hasAccessInDak($id, DAK_DAPTORIK, $employee_office['office_unit_organogram_id'])) {
            if (empty($dakUsers)) {
                die('Invalid request');
            }
        }
        foreach ($dakUsers as $dakUser) {
            $dakUser->dak_view_status = DAK_VIEW_STATUS_VIEW;
            $table_instance_dak_user->save($dakUser);

            if ($nothiJato == 0 && $dakUser['is_rollback_dak'] == 0 && $dakUser['is_archive'] === 1) {
                $rollback = 1;
            }
            if ($dakUser['is_rollback_dak'] === 1 && $dakUser['is_archive'] === 1) {
                $rollback = 0;
            }
        }

        $table_instance_da = TableRegistry::get('DakAttachments');
        $this->set('dak_attachments',
            $table_instance_da->loadAllAttachmentByDakId($dak_entity['id']));

        $table_instance_dak_actions = TableRegistry::get('DakActions');
        $this->set('dak_actions',
            $table_instance_dak_actions->GetDakActions($employee_office['office_unit_organogram_id']));

        /* Show Sender Information */


        $dak_priority_type_list = json_decode(DAK_PRIORITY_TYPE, true);
        $dak_security_level_list = json_decode(DAK_SECRECY_TYPE, true);

        $dak_priority = 1;

        $dak_last_move = $table_instance_dm->getInboxDakLastMoveBy_dakid_attention_type($dak_entity['id']);

        $selected_office_section = $this->getCurrentDakSection();

        if (!empty($dak_last_move)) {
            $sender = h($dak_last_move['from_officer_name']) . ', ' . h($dak_last_move['from_officer_designation_label']);
            $dak_priority = $dak_last_move['dak_priority'] == 0 ? 1 : h($dak_last_move['dak_priority']);
            $move_date = $dak_last_move['modified'];
            $this->set('canEdit',
                ($selected_office_section['office_unit_organogram_id'] == $dak_last_move['to_officer_designation_id']
                    && $dak_last_move['attention_type']));
        }

        $this->set('dak_priority', $dak_priority);
        $this->set('dak_priority_txt',
            isset($dak_priority_type_list[$dak_priority]) ? $dak_priority_type_list[$dak_priority] : '');
        $this->set('dak_security_txt',
            isset($dak_security_level_list[$dak_entity['dak_security_level']]) ? $dak_security_level_list[$dak_entity['dak_security_level']]
                : '');
        $this->set('sender_office', $sender);
        $this->set('move_date', $move_date);
        $this->set('dak_action', $dak_last_move['dak_actions']);
        $this->set('employee_rec', $selected_office_section);
        $this->set('dak_user', $dakUsers);
        $this->set(compact('rollback'));
    }

    public function viewDakNagorik($archive = 0)
    {
        $this->layout = null;
//        $this->view = 'view_dak_nagorik_new';

        $id = !empty($this->request->query['message_id']) ? $this->request->query['message_id']
            : 0;
        $page = !empty($this->request->query['page']) ? $this->request->query['page'] : 0;
        $si = !empty($this->request->query['si']) ? $this->request->query['si'] : 0;
        $this->set('dak_id', $id);
        $this->set('dak_type', DAK_NAGORIK);
        $this->set('archive', $archive);
        $conditions = "";

        $table_instance_dm = TableRegistry::get('DakMovements');
        if (!empty($this->request->query['conditions'])) {
            $conditionsKey = array_values($this->request->query['conditions']);

            foreach ($conditionsKey as $key => $cond) {
                $condKey = array_keys($cond);
                $condValue = array_values($cond);
                if (empty($condValue[0]) || empty($condKey[0])) continue;
                if (!empty($conditions)) $conditions .= " AND ";

                if ($condKey[0] == 'receiving_office_unit_id') {
                    $conditions .= "{$condKey[0]} = {$condValue[0]}";
                } else if ($condKey[0] == 'from') {
                    $conditions .= "date(modified) >= '%{$condValue[0]}%'";
                } else if ($condKey[0] == 'to') {
                    $conditions .= "date(modified) <= '%{$condValue[0]}%'";
                } else $conditions .= "{$condKey[0]} LIKE '%{$condValue[0]}%'";
            }
        }

        $employee_office = $this->getCurrentDakSection();

        $table_instance_dak_user = TableRegistry::get('DakUsers');
        $dakIds = array();
        $user_new_daks = $table_instance_dak_user->find()->select(['dak_id'])->where(['dak_category' => DAK_CATEGORY_INBOX,
            'to_officer_designation_id' => $employee_office['office_unit_organogram_id'],
            'dak_type' => DAK_NAGORIK])->order(['dak_id DESC'])->order(['dak_id DESC'])->toArray();

        $index = 0;
        foreach ($user_new_daks as $new_dak) {
            $dakIds[] = $new_dak['dak_id'];
            if (defined("PAGINATE_TYPE") && PAGINATE_TYPE == 2 && $new_dak['dak_id'] == $id) {
                $si = $index + 1;
            }
            $index++;
        }

        $this->set(compact('si'));

        $table_instance_dd = TableRegistry::get('DakNagoriks');
        $totalRec = $table_instance_dd->find()->where(['id IN' => $dakIds])->where([$conditions])->order("DakNagoriks.id DESC")->count();


        if (defined("PAGINATE_TYPE") && PAGINATE_TYPE == 1) {
            $totalRec = !empty($this->request->query['totalRec']) ? $this->request->query['totalRec']
                : 0;
        }

        $this->set('totalRec', $totalRec);
        if (!empty($id)) {
            $dak_entity = $table_instance_dd->get($id);
        } else {
            if (!empty($conditions)) {
                $dak_entity = $table_instance_dd->find()->where(['id IN' => $dakIds])->where($conditions)->order("DakNagoriks.id DESC")->page($si,
                    1)->first();
            } else {
                $dak_entity = $table_instance_dd->find()->where(['id IN' => $dakIds])->order("DakNagoriks.id DESC")->page($si,
                    1)->first();
            }
        }
        $meta = [];
        if(!empty($dak_entity['application_meta_data'])){
            $meta = jsonA($dak_entity['application_meta_data']);
        }

        $this->set(compact('meta'));

        $rollback = -1;
        $nothiJato = 0;
        if ($dak_entity['is_rollback_to_dak'] == 1) {
            $nothiJato = 1;
        }

        $dakUsers = $table_instance_dak_user->find()->where(['dak_id' => $dak_entity['id'], 'dak_type' => DAK_NAGORIK, 'to_officer_designation_id' => $employee_office['office_unit_organogram_id']])->toArray();
        if (!$table_instance_dm->hasAccessInDak($id, DAK_NAGORIK, $employee_office['office_unit_organogram_id'])) {
            if (empty($dakUsers)) {
                die('Invalid request');
            }
        }
        foreach ($dakUsers as $dakUser) {
            $dakUser->dak_view_status = DAK_VIEW_STATUS_VIEW;
            $table_instance_dak_user->save($dakUser);

            if ($nothiJato == 0 && $dakUser['is_rollback_dak'] == 0 && $dakUser['is_archive'] === 1) {
                $rollback = 1;
            }
            if ($dakUser['is_rollback_dak'] === 1 && $dakUser['is_archive'] === 1) {
                $rollback = 0;
            }
        }

        $this->set('dak_details', $dak_entity);

        $table_instance_da = TableRegistry::get('DakAttachments');
        $attachment = $table_instance_da->loadAllAttachmentByDakId($dak_entity['id'],
            DAK_NAGORIK);

        $this->set('dak_attachments', $attachment);

        $table_instance_dak_actions = TableRegistry::get('DakActions');
        $this->set('dak_actions',
            $table_instance_dak_actions->GetDakActions($employee_office['office_unit_organogram_id']));

        /* Show Sender Information */

        $dak_priority_type_list = json_decode(DAK_PRIORITY_TYPE, true);

        $dak_security_level_list = json_decode(DAK_SECRECY_TYPE, true);

        $dak_priority = 1;

        $sender = $dak_entity['sender_officer_designation_label'] . ',' . $dak_entity['sender_office_name'];
        $dak_last_move = $table_instance_dm->getInboxDakLastMoveBy_dakid_attention_type($dak_entity['id'],
            1, DAK_NAGORIK);

        $move_date = $dak_entity['modified'];
        if (!empty($dak_last_move)) {
            $sender = h($dak_last_move['from_officer_name']) . ', ' . h($dak_last_move['from_officer_designation_label']);
            $dak_priority = $dak_last_move['dak_priority'] == 0 ? 1 : $dak_last_move['dak_priority'];
            $move_date = $dak_last_move['modified'];
            $this->set('canEdit',
                ($employee_office['office_unit_organogram_id'] == $dak_last_move['to_officer_designation_id']
                    && $dak_last_move['attention_type']));
        }

        $this->set(compact('rollback'));
        $this->set('dak_priority', $dak_priority);
        $this->set('dak_priority_txt',
            isset($dak_priority_type_list[$dak_priority]) ? $dak_priority_type_list[$dak_priority] : '');
        $this->set('dak_security_txt',
            isset($dak_security_level_list[$dak_entity['dak_security_level']]) ? $dak_security_level_list[$dak_entity['dak_security_level']]
                : '');
        $this->set('sender_office', $sender);
        $this->set('move_date', $move_date);
        $this->set('dak_action', $dak_last_move['dak_actions']);

        $this->set('selected_office_section', $this->getCurrentDakSection());
    }

    public function viewDakDaptorikSent()
    {
        $this->layout = null;
//        $this->view = 'view_dak_daptorik_sent_new';

        $table_instance = TableRegistry::get('OfficeUnitOrganograms');
        $table_instance_unit = TableRegistry::get('OfficeUnits');
        $table_instance_dm = TableRegistry::get('DakMovements');
        $table_instance_dd = TableRegistry::get('DakDaptoriks');
        $table_instance_da = TableRegistry::get('DakAttachments');


        $id = !empty($this->request->query['message_id']) ? $this->request->query['message_id'] : 0;
        $page = !empty($this->request->query['page']) ? $this->request->query['page'] : 0;
        $si = !empty($this->request->query['si']) ? $this->request->query['si'] : 0;
        $this->set('dak_id', $id);
        $this->set('dak_type', DAK_DAPTORIK);

        $conditions = "";

        if (!empty($this->request->query['conditions'])) {
            $conditionsKey = array_values($this->request->query['conditions']);

            foreach ($conditionsKey as $key => $cond) {
                $condKey = array_keys($cond);
                $condValue = array_values($cond);
                if (empty($condValue[0]) || empty($condKey[0])) continue;
                if (!empty($conditions)) $conditions .= " AND ";

                if ($condKey[0] == 'receiving_office_unit_id') {
                    $conditions .= "{$condKey[0]} = {$condValue[0]}";
                } else if ($condKey[0] == 'from') {
                    $conditions .= "date(modified) >= '%{$condValue[0]}%'";
                } else if ($condKey[0] == 'to') {
                    $conditions .= "date(modified) <= '%{$condValue[0]}%'";
                } else $conditions .= "{$condKey[0]} LIKE '%{$condValue[0]}%'";
            }
        }

        $employee_office = $this->getCurrentDakSection();
        $table_instance_dak_user = TableRegistry::get('DakUsers');

        //chec has access in dak
        if (!$table_instance_dm->hasAccessInDak($id, DAK_DAPTORIK, $employee_office['office_unit_organogram_id'])) {
            die('Invalid Request');
        }
        $dakIds = array();
        $user_new_daks = $table_instance_dak_user->find()->select(['dak_id'])->where(['dak_category' => DAK_CATEGORY_FORWARD,
            'to_officer_designation_id' => $employee_office['office_unit_organogram_id'],
            'dak_type' => DAK_DAPTORIK])->order(['dak_id DESC'])->toArray();

        $index = 0;
        foreach ($user_new_daks as $new_dak) {
            $dakIds[] = $new_dak['dak_id'];
            if (defined("PAGINATE_TYPE") && PAGINATE_TYPE == 2 && $new_dak['dak_id'] == $id) {
                $si = $index + 1;
            }
            $index++;
        }

        $this->set(compact('si'));
        $totalRec = 0;

        $totalRec = $table_instance_dd->find()->where(['id IN' => $dakIds])->where([$conditions])->order("DakDaptoriks.id DESC")->count();


        if (defined("PAGINATE_TYPE") && PAGINATE_TYPE == 1) {
            $totalRec = !empty($this->request->query['totalRec']) ? $this->request->query['totalRec']
                : 0;
        }

        $this->set('totalRec', $totalRec);
        if (!empty($id)) {
            $dak_daptoriks_entity = $table_instance_dd->get($id);
        } else {
            if (!empty($conditions)) {
                $dak_daptoriks_entity = $table_instance_dd->find()->where(['id IN' => $dakIds])->where($conditions)->order("DakDaptoriks.id DESC")->page($si,
                    1)->first();
            } else {
                $dak_daptoriks_entity = $table_instance_dd->find()->where(['id IN' => $dakIds])->order("DakDaptoriks.id DESC")->page($si,
                    1)->first();
            }
        }

        $this->set('dak_daptoriks', $dak_daptoriks_entity);

        $dakUsers = $table_instance_dak_user->find()->where(['dak_id' => $dak_daptoriks_entity['id'],
            'to_officer_designation_id' => $employee_office['office_unit_organogram_id']])->toArray();

        foreach ($dakUsers as $dakUser) {
            $dakUser->dak_view_status = DAK_VIEW_STATUS_VIEW;
            $dakUser = $table_instance_dak_user->save($dakUser);
        }

        $this->set('dak_attachments',
            $table_instance_da->loadAllAttachmentByDakId($dak_daptoriks_entity['id']));

        $dak_priority_type_list = json_decode(DAK_PRIORITY_TYPE, true);

        $dak_security_level_list = json_decode(DAK_SECRECY_TYPE, true);


        $dak_last_move = $table_instance_dm->getInboxDakLastMoveBy_dakid_attention_type($dak_daptoriks_entity['id']);

        $dak_priority = 1;
        $is_revert_enabled = 0;

        $move_date = $dak_daptoriks_entity['modified'];
        if (!empty($dak_last_move)) {
            $sender = (!empty($dak_last_move['to_officer_name']) ? (h($dak_last_move['to_officer_name']) . ", ")
                    : '') . h($dak_last_move['to_officer_designation_label']);
            $dak_priority = $dak_last_move['dak_priority'] == 0 ? 1 : h($dak_last_move['dak_priority']);
            $move_date = $dak_last_move['modified'];
            if ($dak_last_move['from_officer_designation_id'] == $employee_office['office_unit_organogram_id']
                && $dak_last_move['operation_type'] == 'Forward' && $dak_last_move['sequence'] > 2) {
                $is_revert_enabled = 1;
            }
        }

        $this->set('dak_priority', $dak_priority);
        $this->set('dak_priority_txt',
            isset($dak_priority_type_list[$dak_priority]) ? $dak_priority_type_list[$dak_priority] : '');
        $this->set('dak_security_txt',
            isset($dak_security_level_list[$dak_daptoriks_entity['dak_security_level']]) ? $dak_security_level_list[$dak_daptoriks_entity['dak_security_level']]
                : '');
        $this->set('sender_office', $sender);
        $this->set(compact('move_date'));

        $this->set('is_revert_enabled', $is_revert_enabled);

        $this->set('selected_office_section', $this->getCurrentDakSection());
    }

    public function viewDakNagorikSent()
    {
        $this->layout = null;
//        $this->view = 'view_dak_nagorik_sent_new';

        $table_instance = TableRegistry::get('OfficeUnitOrganograms');
        $table_instance_unit = TableRegistry::get('OfficeUnits');
        $table_instance_dm = TableRegistry::get('DakMovements');
        $table_instance_dn = TableRegistry::get('DakNagoriks');
        $table_instance_da = TableRegistry::get('DakAttachments');

        //
        $id = !empty($this->request->query['message_id']) ? $this->request->query['message_id']
            : 0;
        $page = !empty($this->request->query['page']) ? $this->request->query['page'] : 0;
        $si = !empty($this->request->query['si']) ? $this->request->query['si'] : 0;
        $this->set('dak_id', $id);
        $this->set('dak_type', DAK_NAGORIK);
        $conditions = "";

        if (!empty($this->request->query['conditions'])) {
            $conditionsKey = array_values($this->request->query['conditions']);

            foreach ($conditionsKey as $key => $cond) {
                $condKey = array_keys($cond);
                $condValue = array_values($cond);
                if (empty($condValue[0]) || empty($condKey[0])) continue;
                if (!empty($conditions)) $conditions .= " AND ";

                if ($condKey[0] == 'receiving_office_unit_id') {
                    $conditions .= "{$condKey[0]} = {$condValue[0]}";
                } else if ($condKey[0] == 'from') {
                    $conditions .= "date(modified) >= '%{$condValue[0]}%'";
                } else if ($condKey[0] == 'to') {
                    $conditions .= "date(modified) <= '%{$condValue[0]}%'";
                } else $conditions .= "{$condKey[0]} LIKE '%{$condValue[0]}%'";
            }
        }

        $employee_office = $this->getCurrentDakSection();
        $table_instance_dak_user = TableRegistry::get('DakUsers');

        //has access in dak
        if (!$table_instance_dm->hasAccessInDak($id, DAK_NAGORIK, $employee_office['office_unit_organogram_id'])) {
            die('Invalid Request');
        }

        $dakIds = array();
        $user_new_daks = $table_instance_dak_user->find()->select(['dak_id'])->where(['dak_category' => DAK_CATEGORY_FORWARD,
            'to_officer_designation_id' => $employee_office['office_unit_organogram_id'],
            'dak_type' => DAK_NAGORIK])->order(['dak_id DESC'])->toArray();

        $index = 0;
        foreach ($user_new_daks as $new_dak) {
            $dakIds[] = $new_dak['dak_id'];
            if (defined("PAGINATE_TYPE") && PAGINATE_TYPE == 2 && $new_dak['dak_id'] == $id) {
                $si = $index + 1;
            }
            $index++;
        }

        $this->set(compact('si'));

        $totalRec = 0;

        $totalRec = $table_instance_dn->find()->where(['id IN' => $dakIds])->where([$conditions])->order("DakNagoriks.id DESC")->count();


        if (defined("PAGINATE_TYPE") && PAGINATE_TYPE == 1) {
            $totalRec = !empty($this->request->query['totalRec']) ? $this->request->query['totalRec']
                : 0;
        }

        $this->set('totalRec', $totalRec);
        if (!empty($id)) {
            $dak_nagoriks_entity = $table_instance_dn->get($id);
        } else {
            if (!empty($conditions)) {
                $dak_nagoriks_entity = $table_instance_dn->find()->where(['id IN' => $dakIds])->where($conditions)->order("DakNagoriks.id DESC")->page($si,
                    1)->first();
            } else {
                $dak_nagoriks_entity = $table_instance_dn->find()->where(['id IN' => $dakIds])->order("DakNagoriks.id DESC")->page($si,
                    1)->first();
            }
        }

        $this->set('dak_nagoriks', $dak_nagoriks_entity);

        $dakUsers = $table_instance_dak_user->find()->where(['dak_id' => $dak_nagoriks_entity['id'],
            'to_officer_designation_id' => $employee_office['office_unit_organogram_id'],
            'dak_type' => DAK_NAGORIK])->toArray();

        foreach ($dakUsers as $dakUser) {
            $dakUser->dak_view_status = DAK_VIEW_STATUS_VIEW;
            $dakUser = $table_instance_dak_user->save($dakUser);
        }

        $this->set('dak_attachments',
            $table_instance_da->loadAllAttachmentByDakId($dak_nagoriks_entity['id'], DAK_NAGORIK));

        $dak_priority_type_list = json_decode(DAK_PRIORITY_TYPE, true);

        $dak_security_level_list = json_decode(DAK_SECRECY_TYPE, true);

        $dak_last_move = $table_instance_dm->getInboxDakLastMoveBy_dakid_attention_type($dak_nagoriks_entity['id'],
            1, DAK_NAGORIK);

        $dak_priority = 1;
        $move_date = $dak_nagoriks_entity['modified'];
        $is_revert_enabled = 0;

        if (!empty($dak_last_move)) {
            $sender = (!empty($dak_last_move['to_officer_name']) ? (h($dak_last_move['to_officer_name']) . ", ")
                    : '') . h($dak_last_move['to_officer_designation_label']);
            $dak_priority = $dak_last_move['dak_priority'] == 0 ? 1 : h($dak_last_move['dak_priority']);
            $move_date = $dak_last_move['modified'];

            if ($dak_last_move['from_officer_designation_id'] == $employee_office['office_unit_organogram_id']
                && $dak_last_move['operation_type'] == 'Forward' && $dak_last_move['sequence'] > 2) {
                $is_revert_enabled = 1;
            }
        }

        $this->set('dak_priority', $dak_priority);
        $this->set('dak_priority_txt',
            isset($dak_priority_type_list[$dak_priority]) ? $dak_priority_type_list[$dak_priority] : '');
        $this->set('dak_security_txt',
            isset($dak_security_level_list[$dak_nagoriks_entity['dak_security_level']]) ? $dak_security_level_list[$dak_nagoriks_entity['dak_security_level']]
                : '');
        $this->set('sender_office', $sender);
        $this->set(compact('move_date'));

        $this->set('is_revert_enabled', $is_revert_enabled);
        $this->set('selected_office_section', $this->getCurrentDakSection());
    }

    public function viewNothiVukto()
    {
        $this->layout = null;
//        $this->view = 'view_nothi_vukto_new';

        $table_instance = TableRegistry::get('OfficeUnitOrganograms');
        $table_instance_unit = TableRegistry::get('OfficeUnits');
        $table_instance_dm = TableRegistry::get('DakMovements');
        $table_instance_dd = TableRegistry::get('DakDaptoriks');
        $table_instance_da = TableRegistry::get('DakAttachments');

        //
        $id = !empty($this->request->query['message_id']) ? $this->request->query['message_id'] : 0;
        $page = !empty($this->request->query['page']) ? $this->request->query['page'] : 0;
        $si = !empty($this->request->query['si']) ? $this->request->query['si'] : 0;
        $this->set('dak_id', $id);
        $this->set('dak_type', DAK_DAPTORIK);

        $conditions = "";

        if (!empty($this->request->query['conditions'])) {
            $conditionsKey = array_values($this->request->query['conditions']);

            foreach ($conditionsKey as $key => $cond) {
                $condKey = array_keys($cond);
                $condValue = array_values($cond);
                if (empty($condValue[0]) || empty($condKey[0])) continue;
                if (!empty($conditions)) $conditions .= " AND ";

                if ($condKey[0] == 'receiving_office_unit_id') {
                    $conditions .= "{$condKey[0]} = {$condValue[0]}";
                } else if ($condKey[0] == 'from') {
                    $conditions .= "date(modified) >= '%{$condValue[0]}%'";
                } else if ($condKey[0] == 'to') {
                    $conditions .= "date(modified) <= '%{$condValue[0]}%'";
                } else $conditions .= "{$condKey[0]} LIKE '%{$condValue[0]}%'";
            }
        }

        $employee_office = $this->getCurrentDakSection();
        $table_instance_dak_user = TableRegistry::get('DakUsers');

        //check user has access
        if (!$table_instance_dm->hasAccessInDak($id, DAK_DAPTORIK, $employee_office['office_unit_organogram_id'])) {
            die('Invalid Request');
        }

        $dakIds = array();
        $user_new_daks = $table_instance_dak_user->find()->select(['dak_id'])->where(['dak_category' => DAK_CATEGORY_NOTHIVUKTO,
            'to_officer_designation_id' => $employee_office['office_unit_organogram_id'],
            'dak_type' => DAK_DAPTORIK])->order(['dak_id DESC'])->toArray();

        $index = 0;
        foreach ($user_new_daks as $new_dak) {
            $dakIds[] = $new_dak['dak_id'];
            if (defined("PAGINATE_TYPE") && PAGINATE_TYPE == 2 && $new_dak['dak_id'] == $id) {
                $si = $index + 1;
            }
            $index++;
        }

        $this->set(compact('si'));

        $totalRec = 0;
        $totalRec = $table_instance_dd->find()->where(['id IN' => $dakIds])->where([$conditions])->order("DakDaptoriks.id DESC")->count();

        if (defined("PAGINATE_TYPE") && PAGINATE_TYPE == 1) {
            $totalRec = !empty($this->request->query['totalRec']) ? $this->request->query['totalRec']
                : 0;
        }

        $this->set('totalRec', $totalRec);
        if (!empty($id)) {
            $dak_daptoriks_entity = $table_instance_dd->get($id);
        } else {
            if (!empty($conditions)) {
                $dak_daptoriks_entity = $table_instance_dd->find()->where(['id IN' => $dakIds])->where($conditions)->order("DakDaptoriks.id DESC")->page($si,
                    1)->first();
            } else {
                $dak_daptoriks_entity = $table_instance_dd->find()->where(['id IN' => $dakIds])->order("DakDaptoriks.id DESC")->page($si,
                    1)->first();
            }
        }

        $this->set('dak_daptoriks', $dak_daptoriks_entity);

        $this->set('dak_attachments',
            $table_instance_da->loadAllAttachmentByDakId($dak_daptoriks_entity['id']));

        $dak_priority_type_list = json_decode(DAK_PRIORITY_TYPE, true);

        $dak_security_level_list = json_decode(DAK_SECRECY_TYPE, true);

        $dak_last_move = $table_instance_dm->getInboxDakLastMoveBy_dakid_attention_type($dak_daptoriks_entity['id']);

        $dak_priority = 1;
        $is_revert_enabled = $table_instance_dm->canRevert($dak_daptoriks_entity['id'],
            DAK_DAPTORIK, $employee_office['office_unit_organogram_id'],
            $employee_office['office_unit_id'], $employee_office['office_id']);;

        $move_date = $dak_daptoriks_entity['modified'];
        if (!empty($dak_last_move)) {
            $sender = (!empty($dak_last_move['to_officer_name']) ? (h($dak_last_move['to_officer_name']) . ", ")
                    : '') . h($dak_last_move['to_officer_designation_label']);
            $dak_priority = $dak_last_move['dak_priority'] == 0 ? 1 : h($dak_last_move['dak_priority']);
            $move_date = $dak_last_move['modified'];
        }

        $this->set('dak_priority', $dak_priority);
        $this->set('dak_priority_txt',
            isset($dak_priority_type_list[$dak_priority]) ? $dak_priority_type_list[$dak_priority] : '');
        $this->set('dak_security_txt',
            isset($dak_security_level_list[$dak_daptoriks_entity['dak_security_level']]) ? $dak_security_level_list[$dak_daptoriks_entity['dak_security_level']]
                : '');
        $this->set('sender_office', $sender);
        $this->set(compact('move_date'));

        $this->set('is_revert_enabled', $is_revert_enabled);
    }

    public function viewNothiJato()
    {
        $this->layout = null;
//        $this->view = 'view_nothi_jato_new';

        $table_instance = TableRegistry::get('OfficeUnitOrganograms');
        $table_instance_unit = TableRegistry::get('OfficeUnits');
        $table_instance_dm = TableRegistry::get('DakMovements');
        $table_instance_dd = TableRegistry::get('DakDaptoriks');
        $table_instance_da = TableRegistry::get('DakAttachments');

        $id = !empty($this->request->query['message_id']) ? $this->request->query['message_id']
            : 0;
        $page = !empty($this->request->query['page']) ? $this->request->query['page'] : 0;
        $si = !empty($this->request->query['si']) ? $this->request->query['si'] : 0;
        $this->set('dak_id', $id);
        $this->set('dak_type', DAK_NAGORIK);
        $conditions = "";

        if (!empty($this->request->query['conditions'])) {
            $conditionsKey = array_values($this->request->query['conditions']);

            foreach ($conditionsKey as $key => $cond) {
                $condKey = array_keys($cond);
                $condValue = array_values($cond);
                if (empty($condValue[0]) || empty($condKey[0])) continue;
                if (!empty($conditions)) $conditions .= " AND ";

                if ($condKey[0] == 'receiving_office_unit_id') {
                    $conditions .= "{$condKey[0]} = {$condValue[0]}";
                } else if ($condKey[0] == 'from') {
                    $conditions .= "date(modified) >= '%{$condValue[0]}%'";
                } else if ($condKey[0] == 'to') {
                    $conditions .= "date(modified) <= '%{$condValue[0]}%'";
                } else $conditions .= "{$condKey[0]} LIKE '%{$condValue[0]}%'";
            }
        }

        $employee_office = $this->getCurrentDakSection();
        $table_instance_dak_user = TableRegistry::get('DakUsers');
        //has access in dak
        if (!$table_instance_dm->hasAccessInDak($id, DAK_DAPTORIK, $employee_office['office_unit_organogram_id'])) {
            die('Invalid Request');
        }
        $dakIds = array();
        $user_new_daks = $table_instance_dak_user->find()->select(['dak_id'])->where(['dak_category' => DAK_CATEGORY_NOTHIJATO,
            'to_officer_designation_id' => $employee_office['office_unit_organogram_id'],
            'dak_type' => DAK_DAPTORIK])->order(['dak_id DESC'])->toArray();

        $index = 0;
        foreach ($user_new_daks as $new_dak) {
            $dakIds[] = $new_dak['dak_id'];
            if (defined("PAGINATE_TYPE") && PAGINATE_TYPE == 2 && $new_dak['dak_id'] == $id) {
                $si = $index + 1;
            }
            $index++;
        }

        $this->set(compact('si'));

        $totalRec = 0;
        $totalRec = $table_instance_dd->find()->where(['id IN' => $dakIds])->where([$conditions])->order("DakDaptoriks.id DESC")->count();

        if (defined("PAGINATE_TYPE") && PAGINATE_TYPE == 1) {
            $totalRec = !empty($this->request->query['totalRec']) ? $this->request->query['totalRec']
                : 0;
        }

        $this->set('totalRec', $totalRec);
        if (!empty($id)) {
            $dak_daptoriks_entity = $table_instance_dd->get($id);
        } else {
            if (!empty($conditions)) {
                $dak_daptoriks_entity = $table_instance_dd->find()->where(['id IN' => $dakIds])->where($conditions)->order("DakDaptoriks.id DESC")->page($si,
                    1)->first();
            } else {
                $dak_daptoriks_entity = $table_instance_dd->find()->where(['id IN' => $dakIds])->order("DakDaptoriks.id DESC")->page($si,
                    1)->first();
            }
        }

        $this->set('dak_daptoriks', $dak_daptoriks_entity);

        $this->set('dak_attachments',
            $table_instance_da->loadAllAttachmentByDakId($dak_daptoriks_entity['id']));

        $dak_priority_type_list = json_decode(DAK_PRIORITY_TYPE, true);
        $dak_security_level_list = json_decode(DAK_SECRECY_TYPE, true);

        $dak_last_move = $table_instance_dm->getInboxDakLastMoveBy_dakid_attention_type($dak_daptoriks_entity['id']);

        $is_revert_enabled = 0;
        $dak_priority = 1;
        $move_date = $dak_daptoriks_entity['modified'];
        if (!empty($dak_last_move)) {
            $sender = (!empty($dak_last_move['to_officer_name']) ? (h($dak_last_move['to_officer_name']) . ", ")
                    : '') . h($dak_last_move['to_officer_designation_label']);
            $dak_priority = $dak_last_move['dak_priority'] == 0 ? 1 : h($dak_last_move['dak_priority']);
            $move_date = $dak_last_move['modified'];
            if ($dak_last_move['from_officer_designation_id'] == $employee_office['office_unit_organogram_id']
                && $dak_last_move['operation_type'] == DAK_CATEGORY_NOTHIJATO) {
                $is_revert_enabled = 1;
            }
        }

        $this->set('dak_priority', $dak_priority);
        $this->set('dak_priority_txt',
            isset($dak_priority_type_list[$dak_priority]) ? $dak_priority_type_list[$dak_priority] : '');
        $this->set('dak_security_txt',
            isset($dak_security_level_list[$dak_daptoriks_entity['dak_security_level']]) ? $dak_security_level_list[$dak_daptoriks_entity['dak_security_level']]
                : '');
        $this->set('sender_office', $sender);
        $this->set(compact('move_date'));

        $this->set('is_revert_enabled', $is_revert_enabled);

        $this->set('selected_office_section', $this->getCurrentDakSection());
        $this->set('dak_id', $id);
        $this->set('dak_type', DAK_DAPTORIK);
    }

    public function viewDakNagorikNothiVukto()
    {
        $this->layout = null;

        $table_instance = TableRegistry::get('OfficeUnitOrganograms');
        $table_instance_unit = TableRegistry::get('OfficeUnits');
        $table_instance_dm = TableRegistry::get('DakMovements');
        $table_instance_dd = TableRegistry::get('DakNagoriks');
        $table_instance_da = TableRegistry::get('DakAttachments');


        $id = !empty($this->request->query['message_id']) ? $this->request->query['message_id'] : 0;
        $page = !empty($this->request->query['page']) ? $this->request->query['page'] : 0;
        $si = !empty($this->request->query['si']) ? $this->request->query['si'] : 0;

        $this->set('dak_id', $id);
        $this->set('dak_type', DAK_NAGORIK);
        $conditions = "";

        if (!empty($this->request->query['conditions'])) {
            $conditionsKey = array_values($this->request->query['conditions']);

            foreach ($conditionsKey as $key => $cond) {
                $condKey = array_keys($cond);
                $condValue = array_values($cond);
                if (empty($condValue[0]) || empty($condKey[0])) continue;
                if (!empty($conditions)) $conditions .= " AND ";

                if ($condKey[0] == 'receiving_office_unit_id') {
                    $conditions .= "{$condKey[0]} = {$condValue[0]}";
                } else if ($condKey[0] == 'from') {
                    $conditions .= "date(modified) >= '%{$condValue[0]}%'";
                } else if ($condKey[0] == 'to') {
                    $conditions .= "date(modified) <= '%{$condValue[0]}%'";
                } else $conditions .= "{$condKey[0]} LIKE '%{$condValue[0]}%'";
            }
        }

        $employee_office = $this->getCurrentDakSection();
        $table_instance_dak_user = TableRegistry::get('DakUsers');

        //has access in dak
        if (!$table_instance_dm->hasAccessInDak($id, DAK_NAGORIK, $employee_office['office_unit_organogram_id'])) {
            die('Invalid Request');
        }
        $dakIds = array();
        $user_new_daks = $table_instance_dak_user->find()->select(['dak_id'])->where(['dak_category' => DAK_CATEGORY_NOTHIVUKTO,
            'to_officer_designation_id' => $employee_office['office_unit_organogram_id'],
            'dak_type' => DAK_NAGORIK])->order(['dak_id DESC'])->toArray();

        $index = 0;
        foreach ($user_new_daks as $new_dak) {
            $dakIds[] = $new_dak['dak_id'];
            if (defined("PAGINATE_TYPE") && PAGINATE_TYPE == 2 && $new_dak['dak_id'] == $id) {
                $si = $index + 1;
            }
            $index++;
        }

        $this->set(compact('si'));

        $totalRec = 0;

        $totalRec = $table_instance_dd->find()->where(['id IN' => $dakIds])->where([$conditions])->order("DakDaptoriks.id DESC")->count();


        if (defined("PAGINATE_TYPE") && PAGINATE_TYPE == 1) {
            $totalRec = !empty($this->request->query['totalRec']) ? $this->request->query['totalRec']
                : 0;
        }

        $this->set('totalRec', $totalRec);
        if (!empty($id)) {
            $dak_daptoriks_entity = $table_instance_dd->get($id);
        } else {
            if (!empty($conditions)) {
                $dak_daptoriks_entity = $table_instance_dd->find()->where(['id IN' => $dakIds])->where($conditions)->order("DakDaptoriks.id DESC")->page($si,
                    1)->first();
            } else {
                $dak_daptoriks_entity = $table_instance_dd->find()->where(['id IN' => $dakIds])->order("DakDaptoriks.id DESC")->page($si,
                    1)->first();
            }
        }

        $this->set('dak_daptoriks', $dak_daptoriks_entity);

        $this->set('dak_attachments',
            $table_instance_da->loadAllAttachmentByDakId($dak_daptoriks_entity['id'], DAK_NAGORIK));

        $dak_priority_type_list = json_decode(DAK_PRIORITY_TYPE, true);

        $dak_security_level_list = json_decode(DAK_SECRECY_TYPE, true);
        $dak_last_move = $table_instance_dm->getInboxDakLastMoveBy_dakid_attention_type($dak_daptoriks_entity['id'],
            1, DAK_NAGORIK);

        $dak_priority = 1;
        $move_date = $dak_daptoriks_entity['modified'];
        $is_revert_enabled = $table_instance_dm->canRevert($dak_daptoriks_entity['id'], DAK_NAGORIK,
            $employee_office['office_unit_organogram_id'], $employee_office['office_unit_id'],
            $employee_office['office_id']);

        if (!empty($dak_last_move)) {
            $sender = (!empty($dak_last_move['to_officer_name']) ? ($dak_last_move['to_officer_name'] . ", ")
                    : '') . $dak_last_move['to_officer_designation_label'];
            $dak_priority = $dak_last_move['dak_priority'] == 0 ? 1 : $dak_last_move['dak_priority'];
            $move_date = $dak_last_move['modified'];
        }

        $this->set('dak_priority', $dak_priority);
        $this->set('dak_priority_txt',
            isset($dak_priority_type_list[$dak_priority]) ? $dak_priority_type_list[$dak_priority] : '');
        $this->set('dak_security_txt',
            isset($dak_security_level_list[$dak_daptoriks_entity['dak_security_level']]) ? $dak_security_level_list[$dak_daptoriks_entity['dak_security_level']]
                : '');
        $this->set('sender_office', $sender);
        $this->set(compact('move_date'));
        $this->set('is_revert_enabled', $is_revert_enabled);
        $this->set('selected_office_section', $this->getCurrentDakSection());
    }

    public function viewDakNagorikNothiJato()
    {
        $this->layout = null;
//        $this->view = 'view_dak_nagorik_nothi_jato_new';

        $table_instance = TableRegistry::get('OfficeUnitOrganograms');
        $table_instance_unit = TableRegistry::get('OfficeUnits');
        $table_instance_dm = TableRegistry::get('DakMovements');
        $table_instance_dd = TableRegistry::get('DakNagoriks');
        $table_instance_da = TableRegistry::get('DakAttachments');

        $id = !empty($this->request->query['message_id']) ? $this->request->query['message_id'] : 0;
        $page = !empty($this->request->query['page']) ? $this->request->query['page'] : 0;
        $si = !empty($this->request->query['si']) ? $this->request->query['si'] : 0;

        $conditions = "";

        if (!empty($this->request->query['conditions'])) {
            $conditionsKey = array_values($this->request->query['conditions']);

            foreach ($conditionsKey as $key => $cond) {
                $condKey = array_keys($cond);
                $condValue = array_values($cond);
                if (empty($condValue[0]) || empty($condKey[0])) continue;
                if (!empty($conditions)) $conditions .= " AND ";

                if ($condKey[0] == 'receiving_office_unit_id') {
                    $conditions .= "{$condKey[0]} = {$condValue[0]}";
                } else if ($condKey[0] == 'from') {
                    $conditions .= "date(modified) >= '%{$condValue[0]}%'";
                } else if ($condKey[0] == 'to') {
                    $conditions .= "date(modified) <= '%{$condValue[0]}%'";
                } else $conditions .= "{$condKey[0]} LIKE '%{$condValue[0]}%'";
            }
        }

        $employee_office = $this->getCurrentDakSection();
        $table_instance_dak_user = TableRegistry::get('DakUsers');

        //check user has access
        if (!$table_instance_dm->hasAccessInDak($id, DAK_NAGORIK, $employee_office['office_unit_organogram_id'])) {
            die('Invalid Request');
        }
        $dakIds = array();
        $user_new_daks = $table_instance_dak_user->find()->select(['dak_id'])->where(['dak_category' => DAK_CATEGORY_NOTHIJATO,
            'to_officer_designation_id' => $employee_office['office_unit_organogram_id'],
            'dak_type' => DAK_NAGORIK])->order(['dak_id DESC'])->toArray();

        $index = 0;
        foreach ($user_new_daks as $new_dak) {
            $dakIds[] = $new_dak['dak_id'];
            if (defined("PAGINATE_TYPE") && PAGINATE_TYPE == 2 && $new_dak['dak_id'] == $id) {
                $si = $index + 1;
            }
            $index++;
        }

        $this->set(compact('si'));

        $totalRec = 0;

        $totalRec = $table_instance_dd->find()->where(['id IN' => $dakIds])->where([$conditions])->order("DakDaptoriks.id DESC")->count();


        if (defined("PAGINATE_TYPE") && PAGINATE_TYPE == 1) {
            $totalRec = !empty($this->request->query['totalRec']) ? $this->request->query['totalRec']
                : 0;
        }

        $this->set('totalRec', $totalRec);
        if (!empty($id)) {
            $dak_daptoriks_entity = $table_instance_dd->get($id);
        } else {
            if (!empty($conditions)) {
                $dak_daptoriks_entity = $table_instance_dd->find()->where(['id IN' => $dakIds])->where($conditions)->order("DakDaptoriks.id DESC")->page($si,
                    1)->first();
            } else {
                $dak_daptoriks_entity = $table_instance_dd->find()->where(['id IN' => $dakIds])->order("DakDaptoriks.id DESC")->page($si,
                    1)->first();
            }
        }

        $this->set('dak_daptoriks', $dak_daptoriks_entity);


        $this->set('dak_attachments',
            $table_instance_da->loadAllAttachmentByDakId($dak_daptoriks_entity['id'], DAK_NAGORIK));

        $dak_priority_type_list = json_decode(DAK_PRIORITY_TYPE, true);

        $dak_security_level_list = json_decode(DAK_SECRECY_TYPE, true);

        $dak_last_move = $table_instance_dm->getInboxDakLastMoveBy_dakid_attention_type($dak_daptoriks_entity['id'],
            1, DAK_NAGORIK);

        $is_revert_enabled = 0;
        $dak_priority = 1;
        $move_date = $dak_daptoriks_entity['modified'];
        if (!empty($dak_last_move)) {
            $sender = (!empty($dak_last_move['to_officer_name']) ? ($dak_last_move['to_officer_name'] . ", ")
                    : '') . $dak_last_move['to_officer_designation_label'];
            $dak_priority = $dak_last_move['dak_priority'] == 0 ? 1 : $dak_last_move['dak_priority'];
            $move_date = $dak_last_move['modified'];
            if ($dak_last_move['from_officer_designation_id'] == $employee_office['office_unit_organogram_id']
                && $dak_last_move['operation_type'] == DAK_CATEGORY_NOTHIJATO) {
                $is_revert_enabled = 1;
            }
        }

        $this->set('dak_priority', $dak_priority);
        $this->set('dak_priority_txt',
            isset($dak_priority_type_list[$dak_priority]) ? $dak_priority_type_list[$dak_priority] : '');
        $this->set('dak_security_txt',
            isset($dak_security_level_list[$dak_daptoriks_entity['dak_security_level']]) ? $dak_security_level_list[$dak_daptoriks_entity['dak_security_level']]
                : '');
        $this->set('sender_office', $sender);
        $this->set(compact('move_date'));

        $this->set('is_revert_enabled', $is_revert_enabled);

        $this->set('selected_office_section', $this->getCurrentDakSection());
        $this->set('dak_id', $id);
        $this->set('dak_type', DAK_NAGORIK);
    }

    private function getAttentionType($attentionType = 0)
    {
        return ($attentionType == 0 ? ANULIPI : PRAPOK);
    }

    public function dakReply()
    {
        $this->layout = null;
        $id = $this->request->query['message_id'];
        $dak_daptorik_table = TableRegistry::get('DakDaptoriks');
        $dak_daptoriks_entity = $dak_daptorik_table->get($id);
        $this->set('dak_daptoriks', $dak_daptoriks_entity);
        $this->loadModel('DakAttachments');
        $this->set('dak_attachments', $this->DakAttachments->loadAllAttachmentByDakId($id));

        /* Show Sender Information */
        $table_instance = TableRegistry::get('OfficeUnitOrganograms');
        $table_instance_unit = TableRegistry::get('OfficeUnits');
        $table_instance_office = TableRegistry::get('Offices');

        /* Sender Information */
        $organograms = $table_instance->get($dak_daptoriks_entity['sender_officer_designation_id']);
        $unit = $table_instance_unit->get($organograms['office_unit_id']);
        $office = $table_instance_office->get($organograms['office_id']);
        $sender = $dak_daptoriks_entity['sender_officer_designation_label'] . ',' . $unit['unit_name_bng'] . ',' . $office['office_name_bng'];

        /* Receiver Information */
        $receiver_organograms = $table_instance->get($dak_daptoriks_entity['receiving_officer_designation_id']);
        $receiver_unit = $table_instance_unit->get($receiver_organograms['office_unit_id']);
        $receiver_office = $table_instance_office->get($receiver_organograms['office_id']);
        $receiver = $receiver_organograms['designation_bng'] . ',' . $receiver_unit['unit_name_bng'] . ',' . $receiver_office['office_name_bng'];

        /* Set up office information */
        $this->set('sender_office', $sender);
        $this->set('receiver_office', $receiver);
    }

    public function forwardSelectedDaptorikDak()
    {
        $daks = $this->request->data['messageId'];
        $comment = !empty($this->request->data['comment']) ? $this->request->data['comment'] : '';
        $toOfficerId = $this->request->data['toOfficerId'];

        $toOfficerName = $this->request->data['to_office_name'];

        $to_priority_level = $this->request->data['to_priority_level'];
        $to_officer_level = isset($this->request->data['to_officer_level']) ? $this->request->data['to_officer_level']
            : '';

        $dakArray = explode(',', $daks);
        $toOfficerIdArray = explode(',', $toOfficerId);
        $toOfficerNameArray = explode(';', $toOfficerName);

        $toPriorityLevelArray = explode(',', $to_priority_level);
        $toAttentionTypes = $to_officer_level;

        $DakUsersTable = TableRegistry::get('DakUsers');
        $request_data = array();
        $EmployeeOfficesTable = TableRegistry::get('EmployeeOffices');
        $OfficesTable = TableRegistry::get('Offices');
        $table_instance_unit = TableRegistry::get('OfficeUnits');
        $subject = '';
        $current_office_section = $this->getCurrentDakSection();

        $own = false;
        if (!empty($dakArray)) {
            $totalSuccessDak = 0;
            foreach ($dakArray as $key => $dak_id) {
                if (empty($dak_id)) continue;

                $seq_no = $this->getDakMovementSequence($dak_id);

                try {
                    $conn = ConnectionManager::get('default');
                    $conn->begin();

                    $DakUsers_sender = $this->getExistingDakUser($dak_id,
                        $current_office_section['office_unit_id'],
                        $current_office_section['office_unit_organogram_id']);
                    if (empty($DakUsers_sender)) {
                        continue;
                    }
                    $DakUsersTable->updateAll(['dak_category' => DAK_CATEGORY_FORWARD],
                        ['id' => $DakUsers_sender['id']]);
                    if (count($toOfficerIdArray) > 0) {

                        foreach ($toOfficerIdArray as $keyOfc => $ofcVal) {
                            $request_data['to_officer_id'] = $ofcVal;
                            $designation_info = designationInfo($ofcVal);
                            //$employee_office = $EmployeeOfficesTable->find()->where(['EmployeeOffices.id' => $ofcVal, 'status' => 1])->first();

                            $DakUsers_receiver = $this->getExistingDakUser($dak_id,
                                $designation_info['office_unit_id'],
                                $designation_info['office_unit_organogram_id']);

                            if (empty($DakUsers_receiver)) {
                                $DakUsers_receiver = $DakUsersTable->newEntity();
                                $DakUsers_receiver->dak_type = DAK_DAPTORIK;
                                $DakUsers_receiver->dak_id = $dak_id;
                                $DakUsers_receiver->to_office_id = $current_office_section['office_id'];
                                $DakUsers_receiver->to_office_name = $current_office_section['office_name'];
                                $DakUsers_receiver->to_office_address = $current_office_section['office_address'];
                                $DakUsers_receiver->to_officer_id = $designation_info['officer_id'];
                                $DakUsers_receiver->to_officer_name = $toOfficerNameArray[$keyOfc];
                                $DakUsers_receiver->to_officer_designation_id = $designation_info['office_unit_organogram_id'];
                                $DakUsers_receiver->to_officer_designation_label = $designation_info['designation_label'];

                                //$receiver_office_unit = $table_instance_unit->getOfficeUnitBy_designationId($employee_office['office_unit_organogram_id']);
                                //if (!empty($receiver_office_unit)) {
                                    $DakUsers_receiver->to_office_unit_id = $designation_info['office_unit_id'];
                                    $DakUsers_receiver->to_office_unit_name = $designation_info['office_unit_name'];
                                //}

                                $DakUsers_receiver->dak_view_status = DAK_VIEW_STATUS_NEW;
                                $DakUsers_receiver->dak_priority = isset($toPriorityLevelArray[0]) ? $toPriorityLevelArray[0] : 1;
                                $DakUsers_receiver->attention_type = (($ofcVal == $toAttentionTypes) ? 1 : 0);
                                $DakUsers_receiver->is_archive = 0;
                                $DakUsers_receiver->dak_category = DAK_CATEGORY_INBOX;

                                $DakUsers_receiver->created_by = $this->Auth->user('id');
                                $DakUsers_receiver->modified_by = $this->Auth->user('id');
                            } else {
                                /* Update Dak User Status */
                                $DakUsers_receiver = $DakUsersTable->get($DakUsers_receiver['id']);
                                $DakUsers_receiver->dak_view_status = DAK_VIEW_STATUS_NEW;
                                $DakUsers_receiver->dak_category = DAK_CATEGORY_INBOX;
                                $DakUsers_receiver->is_archive = 0;
                                $DakUsers_receiver->attention_type = (($ofcVal == $toAttentionTypes) ? 1 : 0);
                                $DakUsers_receiver->dak_priority = isset($toPriorityLevelArray[0]) ? $toPriorityLevelArray[0] : 1;
                                $DakUsers_receiver->created_by = $this->Auth->user('id');
                                $DakUsers_receiver->modified_by = $this->Auth->user('id');
                            }

                            //$DakUsers_receiver = $DakUsersTable->save($DakUsers_receiver);
                            if (!$DakUsersTable->save($DakUsers_receiver)) {
                                throw new \Exception('DU: ');
                            }

                            $DakDaptoriksTable = TableRegistry::get('DakDaptoriks');
                            $DakDaptoriks = $DakDaptoriksTable->get($dak_id);

                            $DakMovementsTable = TableRegistry::get('DakMovements');

                            //$dakMovementLastComment = $DakMovementsTable->getInboxDakLastMoveBy_dakid_officerId_designationId($dak_id,$current_office_section['officer_id'],$current_office_section['office_unit_organogram_id']);

                            if (empty($comment)) {
//                                $comment = $dakMovementLastComment->dak_actions;
                                $comment = 'বিধি মোতাবেক ব্যবস্থা গ্রহণের জন্য প্রেরণ করা হল';
                            }

                            if ($current_office_section['officer_id'] == $designation_info['officer_id']) {
                                $own = true;
                            }
                            /* Sender to Draft User Movement */
                            $second_move = $DakMovementsTable->newEntity();
                            $second_move->dak_type = DAK_DAPTORIK;
                            $second_move->dak_id = $dak_id;

                            $second_move->from_office_id = $current_office_section['office_id'];
                            $second_move->from_office_name = $current_office_section['office_name'];
                            $second_move->from_office_address = $current_office_section['office_address'];
                            $second_move->from_office_unit_id = $current_office_section['office_unit_id'];
                            $second_move->from_office_unit_name = $current_office_section['office_unit_name'];
                            $second_move->from_officer_id = $current_office_section['officer_id'];
                            $second_move->from_officer_name = $current_office_section['officer_name'];
                            $second_move->from_officer_designation_id = $current_office_section['office_unit_organogram_id'];
                            $second_move->from_officer_designation_label = $current_office_section['designation_label'];

                            $second_move->to_office_id = $current_office_section['office_id'];
                            $second_move->to_office_name = $current_office_section['office_name'];
                            $second_move->to_office_address = $current_office_section['office_address'];
                            $second_move->to_office_unit_id = $DakUsers_receiver['to_office_unit_id'];
                            $second_move->to_office_unit_name = $DakUsers_receiver['to_office_unit_name'];
                            $second_move->to_officer_id = $designation_info['officer_id'];
                            $second_move->to_officer_name = $toOfficerNameArray[$keyOfc];
                            $second_move->to_officer_designation_id = $designation_info['office_unit_organogram_id'];
                            $second_move->to_officer_designation_label = $designation_info['designation_label'];

                            $second_move->attention_type = (($ofcVal == $toAttentionTypes) ? 1 : 0);
                            $second_move->docketing_no = $DakDaptoriks['docketing_no'];
                            $second_move->from_sarok_no = "";
                            $second_move->to_sarok_no = "";
                            $second_move->operation_type = DAK_CATEGORY_FORWARD;
                            $second_move->dak_actions = $comment;
                            $second_move->sequence = $seq_no;
                            $second_move->dak_priority = isset($toPriorityLevelArray[0]) ? $toPriorityLevelArray[0] : 1;

                            $second_move->created_by = $this->Auth->user('id');
                            $second_move->modified_by = $this->Auth->user('id');

                            if (!$DakMovementsTable->save($second_move)) {
                                throw new \Exception('DM: ');
                            }

                            $toOfficers['office_id'] = $second_move->to_office_id;
                            $toOfficers['officer_id'] = $second_move->to_officer_id;

                            $subject = $DakDaptoriks['dak_subject'];
                            if (!$this->NotificationSet('inbox', array(1, "Daptorik Dak"), 1, $current_office_section, $toOfficers, $subject)) {

                            }
                            // Give Receiver Notification
                            $title = 'আপনার ডেস্কে নতুন ডাক এসেছে';
                            $mail_sender = [
                                'sender' => h($current_office_section["officer_name"] . ', ' . $current_office_section["designation_label"] . ', ' . $current_office_section['office_unit_name']),
                                'subject' => h($subject),
                                'dak_type' => __('Daptorik Dak'),
                                'time' => Time::parse(date('Y-m-d H:i:s'))->i18nFormat(null, null,
                                    'bn-BD'),
//                        'time' => Time(date('H:i:s'))->i18nFormat(null,null,'bn-BD'),
                            ];
                            $mail_sender_notify = 'প্রেরকঃ ' . $mail_sender['sender'] . "\n বিষয়ঃ " . h($mail_sender['subject']) . "\n ডাকের ধরনঃ " . $mail_sender['dak_type'] . "\n সময়ঃ " . $mail_sender['time'];
                            $this->notifyAppUser('', $toOfficers['officer_id'],
                                ['title' => $title, 'body' => $mail_sender],
                                ['title' => $title, 'body' => $mail_sender_notify]);
                            // Give Receiver Notification
                        }
                    }

                    $table_instance_dua = TableRegistry::get('DakUserActions');

                    $user_action = $table_instance_dua->newEntity();
                    $user_action->dak_id = $dak_id;
                    $user_action->dak_user_id = $DakUsers_sender['id'];
                    $user_action->dak_type = DAK_DAPTORIK;
                    $user_action->dak_action = DAK_CATEGORY_FORWARD;
                    $user_action->created_by = $this->Auth->user('id');
                    $user_action->modified_by = $this->Auth->user('id');

                    if (!$table_instance_dua->save($user_action)) {
                        throw new \Exception('DA: ');
                    }

                    $DakDaptoriks->modified = date("Y-m-d H:i:s");
                    if (!$DakDaptoriksTable->save($DakDaptoriks)) {
                        throw new \Exception('DD: ');
                    }

                    $totalSuccessDak++;
//                    if (!$this->NotificationSet('forward', array(1, "Daptorik Dak"), 1,
//                        $current_office_section, [], $subject)) {
//
//                    }
                } catch (\Exception $e) {
                    $conn->rollback();
                }
                $conn->commit();
            }

            $this->response->body(json_encode(['id' => ($own == true ? 1 : 0), 'totalSuccessDak' => $totalSuccessDak]));
            $this->response->type('application/json');
            return $this->response;
        } else {
            $this->response->body(json_encode(0));
            $this->response->type('application/json');
            return $this->response;
        }
    }

    public function forwardSelectedNagorikDak()
    {

        $this->layout = null;

        $daks = $this->request->data['messageId'];
        $comment = !empty($this->request->data['comment']) ? $this->request->data['comment'] : '';
        $toOfficerId = $this->request->data['toOfficerId'];
        $toOfficerName = $this->request->data['to_office_name'];
        $canvasInfo = isset($this->request->data['currentShape']) ? $this->request->data['currentShape']
            : array();
        $attachmentid = isset($this->request->data['attachmentid']) ? intval($this->request->data['attachmentid'])
            : -1;

        $to_priority_level = $this->request->data['to_priority_level'];
        $to_officer_level = isset($this->request->data['to_officer_level']) ? $this->request->data['to_officer_level']
            : '';

        $dakArray = explode(',', $daks);
        $toOfficerIdArray = explode(',', $toOfficerId);
        $toOfficerNameArray = explode(';', $toOfficerName);

        $toPriorityLevelArray = explode(',', $to_priority_level);
        $toAttentionTypes = $to_officer_level;

        $DakUsersTable = TableRegistry::get('DakUsers');
        $request_data = array();
        $EmployeeOfficesTable = TableRegistry::get('EmployeeOffices');
        $OfficesTable = TableRegistry::get('Offices');
        $table_instance_unit = TableRegistry::get('OfficeUnits');

        $subject = '';

        $current_office_section = $this->getCurrentDakSection();
        $own = false;
        if (!empty($dakArray)) {
            $totalSuccessDak = 0;
            foreach ($dakArray as $key => $dak_id) {
                $seq_no = $this->getDakMovementSequence($dak_id, DAK_NAGORIK);

                if (empty($dak_id)) continue;

                try {
                    $conn = ConnectionManager::get('default');
                    $conn->begin();

                    $DakUsers_sender = $this->getExistingDakUser($dak_id,
                        $current_office_section['office_unit_id'],
                        $current_office_section['office_unit_organogram_id'], DAK_NAGORIK);
                    if (empty($DakUsers_sender)) {
                        continue;
                    }
                    $DakUsers_sender->dak_category = DAK_CATEGORY_FORWARD;
                    $DakUsers_sender->dak_type = DAK_NAGORIK;
                    $DakUsersTable->save($DakUsers_sender);

                    if (count($toOfficerIdArray) > 0) {
                        foreach ($toOfficerIdArray as $keyOfc => $ofcVal) {
                            $request_data['to_officer_id'] = $ofcVal;
                            $designation_info = designationInfo($ofcVal);
                            //$employee_office = $EmployeeOfficesTable->find()->where(['EmployeeOffices.id' => $ofcVal])->order(['status' => 'desc'])->first();

                            $DakUsers_receiver = $this->getExistingDakUser($dak_id,
                                $designation_info['office_unit_id'],
                                $designation_info['office_unit_organogram_id'], DAK_NAGORIK);

                            if (empty($DakUsers_receiver)) {
                                $DakUsers_receiver = $DakUsersTable->newEntity();
                                $DakUsers_receiver->dak_type = DAK_NAGORIK;
                                $DakUsers_receiver->dak_id = $dak_id;
                                $DakUsers_receiver->to_office_id = $current_office_section['office_id'];
                                $DakUsers_receiver->to_office_name = $current_office_section['office_name'];
                                $DakUsers_receiver->to_office_address = $current_office_section['office_address'];
                                $DakUsers_receiver->to_officer_id = $designation_info['officer_id'];
                                $DakUsers_receiver->to_officer_name = $toOfficerNameArray[$keyOfc];
                                $DakUsers_receiver->to_officer_designation_id = $designation_info['office_unit_organogram_id'];
                                $DakUsers_receiver->to_officer_designation_label = $designation_info['designation_label'];

                                //$receiver_office_unit = $table_instance_unit->getOfficeUnitBy_designationId($designation_info['office_unit_organogram_id']);
                                //if (!empty($receiver_office_unit)) {
                                    $DakUsers_receiver->to_office_unit_id = $designation_info['office_unit_id'];
                                    $DakUsers_receiver->to_office_unit_name = $designation_info['office_unit_name'];
                                //}

                                $DakUsers_receiver->dak_view_status = DAK_VIEW_STATUS_NEW;
                                $DakUsers_receiver->dak_priority = "";
                                $DakUsers_receiver->is_archive = 0;
                                $DakUsers_receiver->attention_type = (($ofcVal == $toAttentionTypes) ? 1 : 0);
                                $DakUsers_receiver->dak_priority = isset($toPriorityLevelArray[0]) ? $toPriorityLevelArray[0] : 1;
                                $DakUsers_receiver->dak_category = DAK_CATEGORY_INBOX;

                                $DakUsers_receiver = $DakUsersTable->save($DakUsers_receiver);
                            } else {
                                /* Update Dak User Status */
                                $DakUsers_receiver = $DakUsersTable->get($DakUsers_receiver['id']);
                                $DakUsers_receiver->dak_view_status = DAK_VIEW_STATUS_NEW;
                                $DakUsers_receiver->dak_category = DAK_CATEGORY_INBOX;
                                $DakUsers_receiver->is_archive = 0;
                                $DakUsers_receiver->dak_priority = isset($toPriorityLevelArray[0]) ? $toPriorityLevelArray[0] : 1;
                                $DakUsers_receiver->attention_type = (($ofcVal == $toAttentionTypes) ? 1 : 0);
                                $DakUsers_receiver = $DakUsersTable->save($DakUsers_receiver);
                            }

                            $DakNagoriksTable = TableRegistry::get('DakNagoriks');
                            $DakNagoriks = $DakNagoriksTable->get($dak_id);

                            $DakMovementsTable = TableRegistry::get('DakMovements');

                            //$dakMovementLastComment = $DakMovementsTable->getInboxDakLastMoveBy_dakid_officerId_designationId($dak_id,$current_office_section['officer_id'],$current_office_section['office_unit_organogram_id'], DAK_NAGORIK);

                            if (empty($comment)) {
                                $comment = 'বিধি মোতাবেক ব্যবস্থা গ্রহণের জন্য প্রেরণ করা হল';
                                //$comment = $dakMovementLastComment->dak_actions;
                            }
                            if ($current_office_section['officer_id'] == $designation_info['officer_id']) {
                                $own = true;
                            }

                            /* Sender to Draft User Movement */
                            $second_move = $DakMovementsTable->newEntity();
                            $second_move->dak_type = DAK_NAGORIK;
                            $second_move->dak_id = $dak_id;

                            $second_move->from_office_id = $current_office_section['office_id'];
                            $second_move->from_office_name = $current_office_section['office_name'];
                            $second_move->from_office_address = $current_office_section['office_address'];
                            $second_move->from_office_unit_id = $current_office_section['office_unit_id'];
                            $second_move->from_office_unit_name = $current_office_section['office_unit_name'];
                            $second_move->from_officer_id = $current_office_section['officer_id'];
                            $second_move->from_officer_name = $current_office_section['officer_name'];
                            $second_move->from_officer_designation_id = $current_office_section['office_unit_organogram_id'];
                            $second_move->from_officer_designation_label = $current_office_section['designation_label'];

                            $second_move->to_office_id = $current_office_section['office_id'];
                            $second_move->to_office_name = $current_office_section['office_name'];
                            $second_move->to_office_address = $current_office_section['office_address'];
                            $second_move->to_office_unit_id = $DakUsers_receiver['to_office_unit_id'];
                            $second_move->to_office_unit_name = $DakUsers_receiver['to_office_unit_name'];
                            $second_move->to_officer_id = $designation_info['officer_id'];
                            $second_move->to_officer_name = $toOfficerNameArray[$keyOfc];
                            $second_move->to_officer_designation_id = $designation_info['office_unit_organogram_id'];
                            $second_move->to_officer_designation_label = $designation_info['designation_label'];

                            $second_move->attention_type = (($ofcVal == $toAttentionTypes) ? 1 : 0);
                            $second_move->docketing_no = $DakNagoriks['docketing_no'];
                            $second_move->from_sarok_no = "";
                            $second_move->to_sarok_no = "";
                            $second_move->operation_type = DAK_CATEGORY_FORWARD;
                            $second_move->dak_actions = $comment;
                            $second_move->sequence = $seq_no;
                            $second_move->dak_priority = isset($toPriorityLevelArray[0]) ? $toPriorityLevelArray[0]
                                : 1;

                            $DakMovementsTable->save($second_move);
                            $toOfficers['office_id'] = $second_move->to_office_id;
                            $toOfficers['officer_id'] = $second_move->to_officer_id;

                            $priority = json_decode(DAK_PRIORITY_TYPE, true);
                            $security = json_decode(DAK_SECRECY_TYPE, true);

                            $status = (isset($priority[$DakNagoriks['dak_priority_level']]) && $DakNagoriks['dak_priority_level']
                                > 0 ? ('"' . $priority[$DakNagoriks['dak_priority_level']] . '" ') : '') . (isset($security[$DakNagoriks['dak_security_level']])
                                && $DakNagoriks['dak_security_level'] > 0 ? ('"' . $security[$DakNagoriks['dak_security_level']] . '" ')
                                    : '');

                            $subject = $DakNagoriks['dak_subject'];

                            $feedbackmeta = !empty($DakNagoriks['application_meta_data']) ? jsonA($DakNagoriks['application_meta_data']) : [];
                            if (!empty($feedbackmeta['feedback_url'])) {
                                $feedbackdata = [
                                    'api_key' => !empty($feedbackmeta['feedback_api_key']) ? $feedbackmeta['feedback_api_key'] : '',
                                    'aid' => $feedbackmeta['tracking_id'],
                                    'action' => 1,
                                    'decision' => 1,
                                    'decision_note' => $comment,
                                    'dak_id' => $dak_id,
                                    'nothi_id' => 0,
                                    'note_id' => 0,
                                    'potro_id' => 0,
                                    'current_desk_id' => $designation_info['office_unit_organogram_id']
                                ];

                                $this->notifyFeedback($feedbackmeta['feedback_url'], $feedbackdata);
                            }
                            if (!$this->NotificationSet('inbox', array(1, ("Nagorik Dak")), 1,
                                $current_office_section, $toOfficers, $subject)) {

                            }
                            // Give Receiver Notification
                            $title = 'আপনার ডেস্কে নতুন ডাক এসেছে';
                            $mail_sender = [
                                'sender' => h($current_office_section["officer_name"] . ', ' . $current_office_section["designation_label"] . ', ' . $current_office_section['office_unit_name']),
                                'subject' => h($subject),
                                'dak_type' => h(__('Nagorik Dak')),
                                'time' => Time::parse(date('Y-m-d H:i:s'))->i18nFormat(null, null,
                                    'bn-BD'),
//                        'time' => Time(date('H:i:s'))->i18nFormat(null,null,'bn-BD'),
                            ];
                            $mail_sender_notify = 'প্রেরকঃ ' . $mail_sender['sender'] . "\n বিষয়ঃ " . h($mail_sender['subject']) . "\n ডাকের ধরনঃ " . $mail_sender['dak_type'] . "\n সময়ঃ " . $mail_sender['time'];
                            $this->notifyAppUser('', $toOfficers['officer_id'],
                                ['title' => $title, 'body' => $mail_sender],
                                ['title' => $title, 'body' => $mail_sender_notify]);
                            // Give Receiver Notification
                        }
                    }

                    $table_instance_dua = TableRegistry::get('DakUserActions');

                    $DakUsers_sender = $this->getExistingDakUser($dak_id,
                        $current_office_section['office_unit_id'],
                        $current_office_section['office_unit_organogram_id'], DAK_NAGORIK);

                    $user_action = $table_instance_dua->newEntity();
                    $user_action->dak_id = $dak_id;
                    $user_action->dak_user_id = $DakUsers_sender['id'];
                    $user_action->dak_action = DAK_CATEGORY_FORWARD;
                    $user_action->dak_type = DAK_NAGORIK;

                    $table_instance_dua->save($user_action);

                    $DakNagoriks->modified = date("Y-m-d H:i:s");
                    $DakNagoriksTable->save($DakNagoriks);

                    $conn->commit();
                    $totalSuccessDak++;

                    if (!$this->NotificationSet('forward', array(1, "Nagorik Dak"), 1,
                        $current_office_section, [], $subject)) {

                    }
                } catch (\Exception $e) {
                    $this->Flash->error($e->getMessage());
                    $conn->rollback();
                }
            }
            $this->response->body(json_encode(['id' => ($own == true ? 1 : 0)]));
            $this->response->type('application/json');
            return $this->response;
        } else {
            $this->response->body(json_encode(0));
            $this->response->type('application/json');
            return $this->response;
        }
    }

    public function movementHistory()
    {

        $this->layout = null;
        $dak_id = isset($this->request->data['dak_id']) ? $this->request->data['dak_id'] : '';
        $dakType = isset($this->request->data['dak_type']) ? $this->request->data['dak_type'] : 'Daptorik';

        $employee_office = $this->getCurrentDakSection();
        $table_instance_dm = TableRegistry::get('DakMovements');

        $all_move_sequence = $table_instance_dm->getDakMovesBydakId($dak_id, $dakType);

        $move_data = [];
        $i = 0;
        foreach ($all_move_sequence as $move_seq) {
            $dak_moves_by_sequence = $table_instance_dm->getDakMovesBy_sequence_dakId($move_seq['sequence'],
                $dak_id, $dakType);
            $receiver_arr = array();

            foreach ($dak_moves_by_sequence as $move) {

                //$receiver = "<b>". $this->getAttentionType($move['attention_type']) . ":</b> " . $move['to_officer_name'] . ", " . $move['to_officer_designation_label'];
                $receiver = $move['to_officer_name'] . ", " . $move['to_officer_designation_label'];

                if ($move['to_office_id'] != $employee_office['office_id']) {
                    $receiver .= ', ' . $move['to_office_name'];
                }

                $receiver_arr[$move['attention_type']][] = $receiver;
            }

            $i++;
            $row_move = isset($dak_moves_by_sequence[0]) ? $dak_moves_by_sequence[0] : array();

            $row = array();
            if (!empty($row_move)) {

                $sender = (!empty($row_move['from_officer_name']) ? ($row_move['from_officer_name'] . ', ')
                        : '') . $row_move['from_officer_designation_label'] . (($move['from_office_id']
                        != $employee_office['office_id']) ? (', ' . $row_move['from_office_name']) : '');


                $users_table = \Cake\ORM\TableRegistry::get('Users');
                $users = $users_table->find()->where(['employee_record_id' => $row_move['from_officer_id']])->first();
                $projapoti_controller = new \App\Controller\ProjapotiController();
                if ($users) {
                    $user_photo = $projapoti_controller->get_user_photo($users->username, $encode = 1);
                } else {
                    $user_photo = $projapoti_controller->get_user_photo(0, $encode = 1);;
                }

                $row['photo'] = $user_photo;
                $row['value'] = $row_move['dak_actions'];
                $row['dak_type'] = $move['dak_type'];
                $row['move_date_time'] = $row_move['created'];
                $row['priority'] = $row_move['dak_priority'];
                $row['receiver'] = $receiver_arr;
                $row['sender'] = $sender;
                $move_data[] = $row;

            }
        }
        $tracking_data = $table_instance_dm->potrojariDakTracking($dak_id, $dakType);

        if ($tracking_data['status'] == 'error') {
            $tracking_data = false;
        } else {
            $tracking_data = enTobn($tracking_data['data'][0]['dak_actions']);
        }

        $this->set(compact('tracking_data'));
        $this->set(compact('move_data'));

    }

    public function getDakMovementSequence($dak_id, $dak_type = DAK_DAPTORIK)
    {
        $table_instance_dak = TableRegistry::get('DakMovements');
        $dak = $table_instance_dak->find()->select(['sequence'])->where(['dak_id' => $dak_id,
            'dak_type' => $dak_type])->order(['created desc', 'id desc'])->first();
        return (intval($dak['sequence']) + 1);
    }

    public function getExistingDakUser($dak_id = 0, $dak_officer_unit_id = 0, $dak_office_organogram_id,
                                       $dakType = DAK_DAPTORIK)
    {
        $DakUsersTable = TableRegistry::get('DakUsers');
        $data = $DakUsersTable->find()->where(['dak_id' => $dak_id,
            'to_officer_designation_id' => $dak_office_organogram_id, 'dak_type' => $dakType, 'dak_category <>' => 'Sent'])->order(['id desc'])->first();
        return $data;
    }

    public function downloadDakAttachment()
    {
        $this->layout = null;

        $dak_id = (int)$this->request->query['dak_id'];
        $attachment_id = (int)$this->request->query['attachment_id'];

        $dak_attachments = array();
        $dak_attachments_table = TableRegistry::get('DakAttachments');

        $attachmentData = $dak_attachments_table->find()->where(['dak_id' => $dak_id,
            'id' => $attachment_id])
            ->first();


        if (!empty($attachmentData['file_name'])) {
            $dak_users_table = TableRegistry::get('DakUsers');
            $employee_office = $this->getCurrentDakSection();
            $permission = $dak_users_table->find()->where(['dak_id'=>$dak_id,'dak_type'=>$attachmentData['dak_type'],'to_officer_designation_id'=>$employee_office['office_unit_organogram_id']])->first();

            if(empty($permission)){
                $this->Flash->error("দুঃখিত! প্রবেশধিকার সংরক্ষিত!");
                return $this->redirect(array('action' => 'dashboard', 'controller' => 'dashboard'));
            }

            $filename = explode("/", $attachmentData['file_name']);

            if (file_exists(FILE_FOLDER_DIR . urldecode($attachmentData['file_name']))) {
                $this->response->file(
                    (FILE_FOLDER_DIR . urldecode($attachmentData['file_name'])),
                    array('download' => true, 'name' => urldecode($filename[count($filename) - 1]))
                );
            } else {
                $this->Flash->error("দুঃখিত! ফাইলের নাম ঠিক ভাবে দেয়া হয়নি। তাই সিস্টেম ফাইল ডাউনলোড করতে পারছে না। দয়া করে সাপোর্ট টিমের সহায়তা নিন। ");
                return $this->redirect(array('action' => 'dashboard', 'controller' => 'dashboard'));
            }
        }

        return $this->response;
    }

    public function apiDownloadDakAttachment()
    {
        $this->layout = null;

        $dak_id = (int)$this->request->query['dak_id'];
        $dak_type = !empty($this->request->query['dak_type']) ? $this->request->query['dak_type']
            : DAK_DAPTORIK;
        $attachment_id = (int)$this->request->query['attachment_id'];
        $user_designation = (int)$this->request->query['user_designation'];

        $EmployeeOfficesTable = TableRegistry::get('EmployeeOffices');
        $employee_office = $EmployeeOfficesTable->getDesignationInfo($user_designation);
        $this->switchOffice($employee_office['office_id'], 'Receiveroffice');

        $dak_attachments = array();
        $dak_attachments_table = TableRegistry::get('DakAttachments');

        $attachmentData = $dak_attachments_table->find()->where(['dak_id' => $dak_id, 'dak_type' => $dak_type,
            'id' => $attachment_id])
            ->first();


        if (!empty($attachmentData['file_name'])) {

            $dak_users_table = TableRegistry::get('DakUsers');
            $permission = $dak_users_table->find()->where(['dak_id'=>$dak_id,'dak_type'=>$dak_type,'to_officer_designation_id'=>$user_designation])->first();

            if(empty($permission)){
                $this->Flash->error("দুঃখিত! প্রবেশধিকার সংরক্ষিত!");
                return $this->redirect(array('action' => 'dashboard', 'controller' => 'dashboard'));
            }

            $filename = explode("/", $attachmentData['file_name']);

            if (file_exists(FILE_FOLDER_DIR . urldecode($attachmentData['file_name']))) {
                $this->response->file(
                    (FILE_FOLDER_DIR . urldecode($attachmentData['file_name'])),
                    array('download' => true, 'name' => urldecode($filename[count($filename) - 1]))
                );
            } else {
                $this->Flash->error("দুঃখিত! ফাইলের নাম ঠিক ভাবে দেয়া হয়নি। তাই সিস্টেম ফাইল ডাউনলোড করতে পারছে না। দয়া করে সাপোর্ট টিম ের সহায়তা নিন। ");
                return $this->redirect(array('action' => 'dashboard', 'controller' => 'dashboard'));
            }
        }

        return $this->response;
    }

    public function popupPotro()
    {
        $this->layout = 'online_dak';
        $this->view = '/NothiMasters/potro_popup';
        $this->set('mobile', true);
        $this->set('backButton',
            \Cake\Routing\Router::url([
                'controller' => 'DakMovements', 'action' => 'apiViewDak', '?' => $this->request->query
            ]));

        $dak_id = $this->request->query['dak_id'];
        $dak_type = !empty($this->request->query['dak_type']) ? $this->request->query['dak_type']
            : DAK_DAPTORIK;
        $attachment_id = $this->request->query['attachment_id'];
        $user_designation = $this->request->query['user_designation'];

        $EmployeeOfficesTable = TableRegistry::get('EmployeeOffices');
        $employee_office = $EmployeeOfficesTable->getDesignationInfo($user_designation);
        $this->switchOffice($employee_office['office_id'], 'Receiveroffice');

        $dak_attachments = array();
        TableRegistry::remove('DakAttachments');
        $dak_attachments_table = TableRegistry::get('DakAttachments');

        $attachmentData = $dak_attachments_table->find()->where(['dak_id' => $dak_id, 'dak_type' => $dak_type,
            'id' => $attachment_id])->first();

        if (!empty($attachmentData['file_name'])) {

//            $filename = explode("/", $attachmentData['file_name']);
            $this->set('row', $attachmentData);
        }

        $userAgent = env('HTTP_USER_AGENT');
        $isMobile = preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $userAgent);

        $this->set('isMobile', $isMobile);
        $this->set('nothi_office', $employee_office['office_id']);
        $this->set('name', 'downloadAttachment');
    }

    public function dakRevert()
    {
        try {
            $dak_id = $this->request->data['message_id'];
            $dak_type = empty($this->request->data['dak_type']) ? DAK_DAPTORIK : $this->request->data['dak_type'];
            $is_revert_enabled = 0;
            $DakMovementsTable = TableRegistry::get('DakMovements');
            $DakUsersTable = TableRegistry::get('DakUsers');
            $table_instance_dua = TableRegistry::get('DakUserActions');
            $dak_last_move = $DakMovementsTable->getDakLastMove($dak_id, $dak_type);
            $employee_office = $this->getCurrentDakSection();
            if ($dak_last_move['from_officer_designation_id'] == $employee_office['office_unit_organogram_id']) {
                //Dak Users Update
                $DakUsers_receiver = $this->getExistingDakUser($dak_id,
                    $dak_last_move['from_office_unit_id'],
                    $dak_last_move['from_officer_designation_id'], $dak_type);

                if (!empty($DakUsers_receiver)) {
                    $DakUsers_receiver->dak_category = 'Inbox';
                    $DakUsers_receiver->dak_view_status = 'New';
                    $DakUsersTable->save($DakUsers_receiver);
                }

                if ($dak_last_move['to_officer_designation_id'] != $employee_office['office_unit_organogram_id']) {
                    $DakUsers_sender = $this->getExistingDakUser($dak_id,
                        $dak_last_move['to_office_unit_id'],
                        $dak_last_move['to_officer_designation_id'], $dak_type);
                    $DakUsers_sender->dak_category = DAK_CATEGORY_FORWARD;
                    $DakUsersTable->save($DakUsers_sender);
                }

                //Dak Movement Update
                $move = $DakMovementsTable->newEntity();
                $move->dak_type = $dak_type;
                $move->dak_id = $dak_id;

                $move->from_office_id = $dak_last_move['to_office_id'];
                $move->from_office_name = $dak_last_move['to_office_name'];
                $move->from_office_address = $dak_last_move['to_office_address'];
                $move->from_officer_id = $dak_last_move['to_officer_id'];
                $move->from_officer_name = $dak_last_move['to_officer_name'];
                $move->from_office_unit_id = $dak_last_move['to_office_unit_id'];
                $move->from_office_unit_name = $dak_last_move['to_office_unit_name'];
                $move->from_officer_designation_id = $dak_last_move['to_officer_designation_id'];
                $move->from_officer_designation_label = $dak_last_move['to_officer_designation_label'];

                $move->to_office_id = $dak_last_move['from_office_id'];
                $move->to_office_name = $dak_last_move['from_office_name'];
                $move->to_office_address = $dak_last_move['from_office_address'];
                $move->to_officer_id = $dak_last_move['from_officer_id'];
                $move->to_officer_name = $dak_last_move['from_officer_name'];
                $move->to_office_unit_id = $dak_last_move['from_office_unit_id'];
                $move->to_office_unit_name = $dak_last_move['from_office_unit_name'];
                $move->to_officer_designation_id = $dak_last_move['from_officer_designation_id'];
                $move->to_officer_designation_label = $dak_last_move['from_officer_designation_label'];

                $move->attention_type = 1;
                $move->docketing_no = $dak_last_move['docketing_no'];
                $move->from_sarok_no = "";
                $move->to_sarok_no = "";
                $move->sequence = $dak_last_move->sequence + 1;
                $move->operation_type = DAK_CATEGORY_FORWARD;
                $move->dak_actions = ACTION_REVERT;
                $move->created_by = $this->Auth->user('id');
                $move->modified_by = $this->Auth->user('id');
                $move->dak_priority = $dak_last_move->dak_priority;

                $DakMovementsTable->save($move);

                $this->response->body(json_encode(1));
                $this->response->type('application/json');
                return $this->response;
            } else {
                $this->response->body(json_encode(0));
                $this->response->type('application/json');
                return $this->response;
            }
        } catch (\Exception $ex) {
            $this->response->body(json_encode(0));
            $this->response->type('application/json');
            return $this->response;
        }
    }

    public function dakRevertNothi()
    {
        try {
            $dak_id = $this->request->data['message_id'];
            $dak_type = empty($this->request->data['dak_type']) ? DAK_DAPTORIK : $this->request->data['dak_type'];

            $DakMovementsTable = TableRegistry::get('DakMovements');
            $DakUsersTable = TableRegistry::get('DakUsers');
            $table_instance_dua = TableRegistry::get('DakUserActions');
            if ($dak_type == DAK_DAPTORIK) {
                $dak_table = TableRegistry::get('DakDaptoriks');
            } else {
                $dak_table = TableRegistry::get('DakNagoriks');
            }
            $nothiDakPotroMapsTable = TableRegistry::get("NothiDakPotroMaps");
            $nothiPotrosTable = TableRegistry::get("NothiPotros");
            $nothiPotroAttachmentsTable = TableRegistry::get("NothiPotroAttachments");

            $employee_office = $this->getCurrentDakSection();

            $is_revert_enabled = $DakMovementsTable->canRevert($dak_id, $dak_type,
                $employee_office['office_unit_organogram_id'], $employee_office['office_unit_id'],
                $employee_office['office_id']);;

            $dak_last_move = $DakMovementsTable->getDakLastMove($dak_id, $dak_type);

            if ($is_revert_enabled && $dak_last_move['from_officer_designation_id'] == $employee_office['office_unit_organogram_id']
                && $dak_last_move['operation_type'] == 'NothiVukto') {

//Dak Users Update

                $DakUsersTable->updateAll(['dak_category' => DAK_CATEGORY_FORWARD],
                    ['dak_id' => $dak_id, 'dak_type' => $dak_type, 'to_officer_designation_id' => $employee_office['office_unit_organogram_id']]);

                $DakUsers_sender = $this->getExistingDakUser($dak_id,
                    $dak_last_move['to_office_unit_id'],
                    $dak_last_move['to_officer_designation_id'], $dak_type);

                $DakUsers_sender->dak_category = DAK_CATEGORY_INBOX;
                $DakUsers_sender->dak_view_status = DAK_VIEW_STATUS_NEW;
                $DakUsersTable->save($DakUsers_sender);

                //Dak Movement Update
                $move = $DakMovementsTable->newEntity();
                $move->dak_type = $dak_type;
                $move->dak_id = $dak_id;

                $move->from_office_id = $dak_last_move['to_office_id'];
                $move->from_office_name = $dak_last_move['to_office_name'];
                $move->from_office_address = $dak_last_move['to_office_address'];
                $move->from_officer_id = $dak_last_move['to_officer_id'];
                $move->from_officer_name = $dak_last_move['to_officer_name'];
                $move->from_office_unit_id = $dak_last_move['to_office_unit_id'];
                $move->from_office_unit_name = $dak_last_move['to_office_unit_name'];
                $move->from_officer_designation_id = $dak_last_move['to_officer_designation_id'];
                $move->from_officer_designation_label = $dak_last_move['to_officer_designation_label'];

                $move->to_office_id = $dak_last_move['from_office_id'];
                $move->to_office_name = $dak_last_move['from_office_name'];
                $move->to_office_address = $dak_last_move['from_office_address'];
                $move->to_officer_id = $dak_last_move['from_officer_id'];
                $move->to_officer_name = $dak_last_move['from_officer_name'];
                $move->to_office_unit_id = $dak_last_move['from_office_unit_id'];
                $move->to_office_unit_name = $dak_last_move['from_office_unit_name'];
                $move->to_officer_designation_id = $dak_last_move['from_officer_designation_id'];
                $move->to_officer_designation_label = $dak_last_move['from_officer_designation_label'];

                $move->attention_type = 1;
                $move->sequence = $dak_last_move['sequence'] + 1;
                $move->docketing_no = $dak_last_move['docketing_no'];
                $move->from_sarok_no = "";
                $move->to_sarok_no = "";
                $move->operation_type = DAK_CATEGORY_FORWARD;
                $move->dak_actions = "নথি থেকে ডাক ফেরত আনা হয়েছে"; //.'। পূর্ববর্তী সিদ্ধান্ত: ' . $dak_last_move['dak_actions'];

                $DakMovementsTable->save($move);

                $user_action = $table_instance_dua->newEntity();
                $user_action->dak_id = $dak_id;
                $user_action->dak_user_id = $DakUsers_sender['id'];
                $user_action->dak_action = DAK_CATEGORY_REVERT;

                $dak_table->updateAll(['dak_status' => 'Sent'], ['id' => $dak_id]);
                $nothiForDak = $nothiDakPotroMapsTable->getNothiInformationForDak($dak_id, $dak_type);

                $nothiDakPotroMapsTable->updateAll(['status' => 0],
                    ['dak_id' => $dak_id, 'dak_type' => $dak_type]);
                $nothiPotrosTable->updateAll(['is_deleted' => 1],
                    ['id' => $nothiForDak['nothi_potro_id']]);
                $nothiPotroAttachmentsTable->updateAll(['status' => 0],
                    ['nothi_potro_id' => $nothiForDak['nothi_potro_id']]);

                $this->response->body(json_encode(1));
                $this->response->type('application/json');
                return $this->response;
            } else {
                $this->response->body(json_encode(0));
                $this->response->type('application/json');
                return $this->response;
            }
        } catch (Exception $ex) {
            $this->response->body(json_encode(0));
            $this->response->type('application/json');
            return $this->response;
        }
    }

    public function getOfficerInfoForSealSelection($selecteddesignation)
    {
        $table_instance_eo = TableRegistry::get('EmployeeOffices');
        $table_instance_org = TableRegistry::get('OfficeUnitOrganograms');
        $table_instance_ou = TableRegistry::get('OfficeUnits');

        $employee_info = array();
        //echo $selecteddesignation;
//        $designation      = $table_instance_org->getUnitOrganogramInfo($selecteddesignation);
        //pr($designation);die;
        $designation_info = $table_instance_eo->getDesignationInfo($selecteddesignation);
//pr($designation_info);die;
        $officeUnit = $table_instance_ou->find()->where(['id' => $designation_info['office_unit_id']])->first();


        $employee_info['label'] = $designation_info['designation_bng'];

        $employee_info['designation_id'] = $selecteddesignation;
        $employee_info['designation_label'] = $designation_info['designation'];


        $employee_info['office_unit_id'] = $officeUnit['id'];
        $employee_info['office_id'] = $designation_info['office_id'];

        $employee_info['office_unit_name'] = $officeUnit['unit_name_bng'];

        $employee_record_id = 0;

        if (!empty($designation_info)) {
            $employee_record_id = $designation_info['employee_record_id'];

            $table_instance_er = TableRegistry::get('EmployeeRecords');

            $employee_info_data = $table_instance_er->get($employee_record_id);

            $employee_info['employee_name'] = $employee_info_data['name_bng'];

            $employee_info['employee_office_id'] = $designation_info['id'];
        }

        $employee_info['employee_record_id'] = $employee_record_id;

        return $employee_info;
    }

    /* API of this Controller */

    public function APIDakListOld()
    {
        $dak_inbox_group = isset($this->request->query['dak_group']) ? $this->request->query['dak_group']
            : "";
        $jsonArray = array(
            "status" => "error",
            "error_code" => 404,
            "message" => "Invalid Request"
        );
        if (!defined("PROJAPOTI_API") || PROJAPOTI_API == false) {
            echo json_encode($jsonArray);
            die;
        }
        if (empty($dak_inbox_group)) {
            echo json_encode($jsonArray);
            die;
        }

        $apikey = isset($this->request->query['api_key']) ? $this->request->query['api_key'] : '';


        if (!defined("API_KEY") || API_KEY == false || empty($apikey) || $apikey != API_KEY) {
            echo json_encode($jsonArray);
            die;
        }

        $start = isset($this->request->data['start']) ? intval($this->request->data['start']) : 0;
        $len = isset($this->request->data['length']) ? intval($this->request->data['length']) : PAGELIMIT
            + 100;
        $page = !empty($this->request->data['page']) ? intval($this->request->data['page']) : ($start
                / $len) + 1;

        $userDesignation = !empty($this->request->query['user_designation']) ? intval($this->request->query['user_designation'])
            : 0;

        $condition = '1 ';
        $subject = isset($this->request->data['dak_subject']) ? trim($this->request->data['dak_subject'])
            : '';
        if (!empty($subject)) {
            if (!empty($condition)) $condition .= ' AND ';
            $condition .= "dak_subject LIKE '%{$subject}%'";
        }

        $officername = isset($this->request->data['receiving_officer_name']) ? trim($this->request->data['receiving_officer_name'])
            : '';
        if (!empty($officername)) {
            if (!empty($condition)) $condition .= ' AND ';
            $condition .= "  receiving_officer_name LIKE '%{$officername}%'";
        }

        $EmployeeOfficesTable = TableRegistry::get('EmployeeOffices');
        $employee_office = $EmployeeOfficesTable->find()->where(['office_unit_organogram_id' => $userDesignation,
            'status' => 1])->first();
        $this->switchOffice($employee_office['office_id'], 'Receiveroffice');
        $dak_daptoriks = array();
        $dak_nagoriks = array();

        $dak_daptorik_table = TableRegistry::get('DakDaptoriks');
        $dak_nagorik_table = TableRegistry::get('DakNagoriks');
        $dak_movement_table = TableRegistry::get('DakMovements');

        $ng_ids = array();
        $dp_ids = array();
        $unreadDaptorik = array();
        $unreadNagorik = array();
        $attentionDaptorik = array();
        $attentionNagorik = array();
        $table_instance_dak_user = TableRegistry::get('DakUsers');

        if ($dak_inbox_group == 'new') {

            $user_new_daks = $table_instance_dak_user->find()->select(['dak_id',
                'dak_view_status', 'dak_type'])->where(['dak_category' => DAK_CATEGORY_INBOX,
                'to_officer_designation_id' => $employee_office['office_unit_organogram_id'],
                'is_archive' => 0, 'dak_view_status' => DAK_VIEW_STATUS_NEW])->page($page, $len)->order(['modified DESC'])->toArray();

            $totalRec = $table_instance_dak_user->find()->select(['dak_id',
                'dak_view_status', 'dak_type'])->where(['dak_category' => DAK_CATEGORY_INBOX,
                'to_officer_designation_id' => $employee_office['office_unit_organogram_id'],
                'is_archive' => 0, 'dak_view_status' => DAK_VIEW_STATUS_NEW])->count();
        } else if ($dak_inbox_group == 'inbox') {

            $user_new_daks = $table_instance_dak_user->find()->select(['dak_id',
                'dak_view_status', 'dak_type', 'attention_type'])->where(['dak_category' => DAK_CATEGORY_INBOX,
                'to_officer_designation_id' => $employee_office['office_unit_organogram_id'],
                'is_archive' => 0])->order(['modified DESC'])->page($page, $len)->toArray();

            $totalRec = $table_instance_dak_user->find()->select(['dak_id',
                'dak_view_status', 'dak_type', 'attention_type'])->where(['dak_category' => DAK_CATEGORY_INBOX,
                'to_officer_designation_id' => $employee_office['office_unit_organogram_id'],
                'is_archive' => 0])->count();
        } else if ($dak_inbox_group == 'archive') {

            $user_new_daks = $table_instance_dak_user->find()->select(['dak_id',
                'dak_view_status', 'dak_type', 'attention_type'])->where(['dak_category' => DAK_CATEGORY_INBOX,
                'to_officer_designation_id' => $employee_office['office_unit_organogram_id'],
                'is_archive' => 1])->order(['modified DESC'])->toArray();

            $totalRec = $table_instance_dak_user->find()->select(['dak_id',
                'dak_view_status', 'dak_type', 'attention_type'])->where(['dak_category' => DAK_CATEGORY_INBOX,
                'to_officer_designation_id' => $employee_office['office_unit_organogram_id'],
                'is_archive' => 1])->order(['modified DESC'])->count();
        } else if ($dak_inbox_group == 'sent') {

            $user_new_daks = $table_instance_dak_user->find()->select(['dak_id',
                'dak_view_status', 'dak_type'])->where(['dak_category' => DAK_CATEGORY_FORWARD,
                'to_officer_designation_id' => $userDesignation])->page($page, $len)->toArray();

            $totalRec = $table_instance_dak_user->find()->select(['dak_id',
                'dak_view_status', 'dak_type'])->where(['dak_category' => DAK_CATEGORY_FORWARD,
                'to_officer_designation_id' => $userDesignation])->count();
        }

        if (!empty($user_new_daks)) {
            foreach ($user_new_daks as $new_dak) {
                if ($new_dak['dak_type'] == DAK_DAPTORIK) {
                    $getLastMove = $dak_movement_table->getDakLastMoveByDesignation($new_dak['dak_id'],
                        $employee_office['office_unit_organogram_id']);

                    $attentionDaptorik[$new_dak['dak_id']] = $getLastMove['attention_type'];
                    if ($new_dak['dak_view_status'] == 'New') {
                        $unreadDaptorik[] = $new_dak['dak_id'];
                    }

                    $dp_ids[] = $new_dak['dak_id'];
                } else if ($new_dak['dak_type'] == DAK_NAGORIK) {

                    $getLastMove = $dak_movement_table->getDakLastMoveByDesignation($new_dak['dak_id'],
                        $employee_office['office_unit_organogram_id'], DAK_NAGORIK);
                    $attentionNagorik[$new_dak['dak_id']] = $getLastMove['attention_type'];
                    $ng_ids[] = $new_dak['dak_id'];
                    if ($new_dak['dak_view_status'] == 'New') {
                        $unreadNagorik[] = $new_dak['dak_id'];
                    }
                }
            }
        }


//        $totalRec = 0;
        if (count($dp_ids) > 0) {
//            $totalRec_daptoriks = $dak_daptorik_table->find()->where(['id IN' => $dp_ids])->where([$condition])->toArray();
            $dak_daptoriks = $dak_daptorik_table->find()->where(['id IN' => $dp_ids])->where([$condition])->order(['modified DESC'])->toArray();
        }
        if (count($ng_ids) > 0) {
//            $totalRec_nagoriks = $dak_nagorik_table->find()->where(['id IN' => $ng_ids])->where([$condition])->toArray();
            $dak_nagoriks = $dak_nagorik_table->find()->where(['id IN' => $ng_ids])->where([$condition])->order(['modified DESC'])->toArray();
        }

        $all_daks = array_merge($dak_daptoriks, $dak_nagoriks);
//        $totalRec = count($totalRec_daptoriks) + count($totalRec_nagoriks);

        usort($all_daks,
            function ($a, $b) {
                $time = new Time($a['modified']);
                $a['modified'] = $time->i18nFormat(null, null, 'en-US');

                $time = new Time($b['modified']);
                $b['modified'] = $time->i18nFormat(null, null, 'en-US');

                $vala = strtotime($a['modified']);
                $valb = strtotime($b['modified']);
                return ($valb - $vala);
            });


        $jsonArray = array(
            "status" => "success",
            "recordsTotal" => $totalRec,
            "data" => array(
                "dak_list" => $all_daks,
                'unreadDaptorik' => $unreadDaptorik,
                'unreadNagorik' => $unreadNagorik,
                'attentionDaptorik' => $attentionDaptorik,
                'attentionNagorik' => $attentionNagorik
            )
        );
        echo json_encode($jsonArray);
        die;
    }

    public function APIDakList()
    {
        $this->request->data = $this->request->query;
        $dak_inbox_group = isset($this->request->data['dak_group']) ? $this->request->data['dak_group']
            : "";
        $jsonArray = array(
            "status" => "error",
            "error_code" => 404,
            "message" => "Invalid Request"
        );
        if (!defined("PROJAPOTI_API") || PROJAPOTI_API == false) {
            echo json_encode($jsonArray);
            die;
        }
        if (empty($dak_inbox_group)) {
            echo json_encode($jsonArray);
            die;
        }

        $apikey = isset($this->request->data['api_key']) ? $this->request->data['api_key'] : '';


        if (empty($apikey) || $this->checkToken($apikey) == FALSE) {
            echo json_encode($jsonArray);
            die;
        }

        $userDesignation = !empty($this->request->data['user_designation']) ? intval($this->request->data['user_designation'])
            : 0;
        if (empty($userDesignation)) {
            echo json_encode($jsonArray);
            die;
        }
        if ($apikey != API_KEY) {
            //verify api token
            $this->loadComponent('ApiRelated');
            $getApiTokenData = $this->ApiRelated->getMobileApiTokenData($apikey);
            if (!empty($getApiTokenData)) {
                if (!in_array($userDesignation, $getApiTokenData['designations'])) {
                    $jsonArray['message'] = 'Unauthorized request';
                    echo json_encode($jsonArray);
                    die;
                }
            }
            //verify api token
        }

        $EmployeeOfficesTable = TableRegistry::get('EmployeeOffices');
        $employee_office = $EmployeeOfficesTable->find()->where(['office_unit_organogram_id' => $userDesignation,
            'status' => 1])->first();
        $this->switchOffice($employee_office['office_id'], 'Receiveroffice');

        $dak_movement_table = TableRegistry::get('DakMovements');

        $unreadDaptorik = array();
        $unreadNagorik = array();
        $attentionDaptorik = array();
        $attentionNagorik = array();

        $this->request->data['length'] = !empty($this->request->data['length']) ? $this->request->data['length'] : 200;
        if ($dak_inbox_group == 'archive') {
            $response = $dak_movement_table->getDakList('onulipi', $employee_office, $this, 0, 1);
        } else {
            $response = $dak_movement_table->getDakList($dak_inbox_group, $employee_office, $this, 0, 1);
        }

        $all_daks = [];
        if (!empty($response['data'])) {
            foreach ($response['data'] as $ke => $new_dak) {

                $move_date = $new_dak['modified'];
                $move_date = new Time($move_date);
                $move_date = $move_date->i18nFormat("dd-MM-YY HH:mm:ss", null, 'bn-BD');

                if ($new_dak['dak_type'] == DAK_DAPTORIK) {
                    unset($new_dak['DakDaptoriks']['dak_description']);
                    unset($new_dak['DakDaptoriks']['dak_cover']);
                    $new_dak['DakDaptoriks']['id'] = intval($new_dak['DakDaptoriks']['id']);
                    $new_dak['DakDaptoriks']['modified'] = $move_date;
                    $new_dak['DakDaptoriks']['sender_officer_designation_label'] = (!empty($new_dak['DakDaptoriks']['sender_officer_designation_label'])
                            ? ($new_dak['DakDaptoriks']['sender_officer_designation_label'] . ", ") : '') . (!empty($new_dak['DakDaptoriks']['sender_office_name'])
                            ? ($new_dak['DakDaptoriks']['sender_office_name']) : '');
                    $all_daks[] = $new_dak['DakDaptoriks'];
                    $getLastMove = $dak_movement_table->getDakLastMoveByDesignation($new_dak['dak_id'],
                        $employee_office['office_unit_organogram_id']);

                    $move_date = $getLastMove['modified'];
                    $attentionDaptorik[$new_dak['dak_id']] = isset($getLastMove['attention_type']) ? $getLastMove['attention_type'] : 0;
                    if (($dak_inbox_group == 'inbox' || $dak_inbox_group == 'archive') && $new_dak['dak_view_status'] == 'New') {
                        $unreadDaptorik[] = $new_dak['dak_id'];
                    }
                } else if ($new_dak['dak_type'] == DAK_NAGORIK) {
                    unset($new_dak['DakNagoriks']['description']);
                    $new_dak['DakNagoriks']['id'] = intval($new_dak['DakNagoriks']['id']);
                    $new_dak['DakNagoriks']['modified'] = $move_date;
                    $new_dak['DakNagoriks']['sender_officer_designation_label'] = $new_dak['DakNagoriks']['name_bng'];
                    $new_dak['DakNagoriks']['sender_office_name'] = '';
                    $all_daks[] = $new_dak['DakNagoriks'];
                    $getLastMove = $dak_movement_table->getDakLastMoveByDesignation($new_dak['dak_id'],
                        $employee_office['office_unit_organogram_id'], DAK_NAGORIK);
                    $attentionNagorik[$new_dak['dak_id']] = isset($getLastMove['attention_type']) ? $getLastMove['attention_type'] : 0;

                    $move_date = $getLastMove['modified'];
                    if (($dak_inbox_group == 'inbox' || $dak_inbox_group == 'archive') && $new_dak['dak_view_status'] == 'New') {
                        $unreadNagorik[] = $new_dak['dak_id'];
                    }
                }
            }
        }


        $jsonArray = array(
            "status" => "success",
            "recordsTotal" => $response['total'],
            "data" => array(
                "dak_list" => $all_daks,
                'unreadDaptorik' => $unreadDaptorik,
                'unreadNagorik' => $unreadNagorik,
                'attentionDaptorik' => $attentionDaptorik,
                'attentionNagorik' => $attentionNagorik
            )
        );
        echo json_encode($jsonArray);
        die;
    }

    public function postAPIDakList()
    {
        $dak_inbox_group = isset($this->request->data['dak_group']) ? $this->request->data['dak_group']
            : "";
        $jsonArray = array(
            "status" => "error",
            "error_code" => 404,
            "message" => "Invalid Request"
        );
        if (!defined("PROJAPOTI_API") || PROJAPOTI_API == false) {
            echo json_encode($jsonArray);
            die;
        }
        if (empty($dak_inbox_group)) {
            echo json_encode($jsonArray);
            die;
        }

        $apikey = isset($this->request->data['api_key']) ? $this->request->data['api_key'] : '';


        if (empty($apikey) || $this->checkToken($apikey) == FALSE) {
            echo json_encode($jsonArray);
            die;
        }

        $userDesignation = !empty($this->request->data['user_designation']) ? intval($this->request->data['user_designation'])
            : 0;
        if (empty($userDesignation)) {
            echo json_encode($jsonArray);
            die;
        }
        if ($apikey != API_KEY) {
            //verify api token
            $this->loadComponent('ApiRelated');
            $getApiTokenData = $this->ApiRelated->getMobileApiTokenData($apikey);
            if (!empty($getApiTokenData)) {
                if (!in_array($userDesignation, $getApiTokenData['designations'])) {
                    $jsonArray['message'] = 'Unauthorized request';
                    echo json_encode($jsonArray);
                    die;
                }
            }
            //verify api token
        }

        $EmployeeOfficesTable = TableRegistry::get('EmployeeOffices');
        $employee_office = $EmployeeOfficesTable->find()->where(['office_unit_organogram_id' => $userDesignation,
            'status' => 1])->first();
        $this->switchOffice($employee_office['office_id'], 'Receiveroffice');

        $dak_movement_table = TableRegistry::get('DakMovements');

        $unreadDaptorik = array();
        $unreadNagorik = array();
        $attentionDaptorik = array();
        $attentionNagorik = array();

        $this->request->data['length'] = !empty($this->request->data['length']) ? $this->request->data['length'] : 200;
        if ($dak_inbox_group == 'archive') {
            $response = $dak_movement_table->getDakList('onulipi', $employee_office, $this, 0, 1);
        } else {
            $response = $dak_movement_table->getDakList($dak_inbox_group, $employee_office, $this, 0, 1);
        }

        $all_daks = [];
        if (!empty($response['data'])) {
            foreach ($response['data'] as $ke => $new_dak) {

                $move_date = $new_dak['modified'];
                $move_date = new Time($move_date);
                $move_date = $move_date->i18nFormat("dd-MM-YY HH:mm:ss", null, 'bn-BD');

                if ($new_dak['dak_type'] == DAK_DAPTORIK) {
                    $new_dak['DakDaptoriks']['id'] = intval($new_dak['DakDaptoriks']['id']);
                    $new_dak['DakDaptoriks']['modified'] = $move_date;
                    $new_dak['DakDaptoriks']['sender_officer_designation_label'] = (!empty($new_dak['DakDaptoriks']['sender_officer_designation_label'])
                            ? ($new_dak['DakDaptoriks']['sender_officer_designation_label'] . ", ") : '') . (!empty($new_dak['DakDaptoriks']['sender_office_name'])
                            ? ($new_dak['DakDaptoriks']['sender_office_name']) : '');
                    $all_daks[] = $new_dak['DakDaptoriks'];
                    $getLastMove = $dak_movement_table->getDakLastMoveByDesignation($new_dak['dak_id'],
                        $employee_office['office_unit_organogram_id']);

                    $attentionDaptorik[$new_dak['dak_id']] = isset($getLastMove['attention_type']) ? $getLastMove['attention_type'] : 0;
                    if (($dak_inbox_group == 'inbox' || $dak_inbox_group == 'archive') && $new_dak['dak_view_status'] == 'New') {
                        $unreadDaptorik[] = $new_dak['dak_id'];
                    }
                } else if ($new_dak['dak_type'] == DAK_NAGORIK) {
                    $new_dak['DakNagoriks']['id'] = intval($new_dak['DakNagoriks']['id']);
                    $new_dak['DakNagoriks']['modified'] = $move_date;
                    $new_dak['DakNagoriks']['sender_officer_designation_label'] = $new_dak['DakNagoriks']['name_bng'];
                    $all_daks[] = $new_dak['DakNagoriks'];
                    $getLastMove = $dak_movement_table->getDakLastMoveByDesignation($new_dak['dak_id'],
                        $employee_office['office_unit_organogram_id'], DAK_NAGORIK);
                    $attentionNagorik[$new_dak['dak_id']] = isset($getLastMove['attention_type']) ? $getLastMove['attention_type'] : 0;

                    if (($dak_inbox_group == 'inbox' || $dak_inbox_group == 'archive') && $new_dak['dak_view_status'] == 'New') {
                        $unreadNagorik[] = $new_dak['dak_id'];
                    }
                }
            }
        }


        $jsonArray = array(
            "status" => "success",
            "recordsTotal" => $response['total'],
            "data" => array(
                "dak_list" => $all_daks,
                'unreadDaptorik' => $unreadDaptorik,
                'unreadNagorik' => $unreadNagorik,
                'attentionDaptorik' => $attentionDaptorik,
                'attentionNagorik' => $attentionNagorik
            )
        );
        echo json_encode($jsonArray);
        die;
    }

    public function APIDakDetail()
    {
        $id = $this->request->query['dak_id'];
        $user_designation = $this->request->query['user_designation'];
        $dak_type = $this->request->query['dak_type'];


        $jsonArray = array(
            "status" => "error",
            "error_code" => 404,
            "message" => "Invalid Request"
        );

        if (!defined("PROJAPOTI_API") || PROJAPOTI_API == false) {
            echo json_encode($jsonArray);
            die;
        }

        if (empty($user_designation)) {
            echo json_encode($jsonArray);
            die;
        }

        if (empty($id) || empty($dak_type)) {
            echo json_encode($jsonArray);
            die;
        }

        $apikey = isset($this->request->query['api_key']) ? $this->request->query['api_key'] : '';


        if (empty($apikey) || $this->checkToken($apikey) == FALSE) {
            echo json_encode($jsonArray);
            die;
        }
        if ($apikey != API_KEY) {
            //verify api token
            $this->loadComponent('ApiRelated');
            $getApiTokenData = $this->ApiRelated->getMobileApiTokenData($apikey);
            if (!empty($getApiTokenData)) {
                if (!in_array($user_designation, $getApiTokenData['designations'])) {
                    $jsonArray['message'] = 'Unauthorized request';
                    echo json_encode($jsonArray);
                    die;
                }
            }
            //verify api token
        }

        $EmployeeOfficesTable = TableRegistry::get('EmployeeOffices');
        $employee_office = $EmployeeOfficesTable->getDesignationInfo($user_designation);
        $this->switchOffice($employee_office['office_id'], 'Receiveroffice');

        $result = array();
        if (!TableRegistry::get('DakMovements')->hasAccessInDak($id, $dak_type, $user_designation)) {
            echo json_encode($jsonArray);
            die;
        }
        $table_instance_dak_user = TableRegistry::get('DakUsers');
        $dakUser = $table_instance_dak_user->find()->where(['dak_id' => $id, 'dak_type' => $dak_type])->first();

        $table_instance_dak_user->updateAll(['dak_view_status' => DAK_VIEW_STATUS_VIEW],
            ['id' => $dakUser['id']]);

        if ($dak_type == DAK_DAPTORIK) {
            $table_instance_dd = TableRegistry::get('DakDaptoriks');
        } else {
            $table_instance_dd = TableRegistry::get('DakNagoriks');
        }

        $dak_daptoriks_entity = $table_instance_dd->get($id);
        $result['dak_daptoriks'] = $dak_daptoriks_entity;


        $table_instance_da = TableRegistry::get('DakAttachments');
        $result['dak_attachments'] = $table_instance_da->loadAllAttachmentByDakId($id, $dak_type);


        $table_instance_dak_actions = TableRegistry::get('DakActions');
        $result['dak_actions'] = $table_instance_dak_actions->GetDakActions($employee_office['office_unit_organogram_id']);

        $sender = ($dak_type == DAK_DAPTORIK) ? ($dak_daptoriks_entity['sender_officer_designation_label'] . (!empty($dak_daptoriks_entity['sender_office_name'])
                ? (',' . $dak_daptoriks_entity['sender_office_name']) : '')) : ($dak_daptoriks_entity['sender_name']);
        $result['sender_office'] = $sender;

        $DakMovementsTable = TableRegistry::get('DakMovements');
        $all_moves = $DakMovementsTable->find()->where(['dak_id' => $id,
            'dak_type' => $dak_type])->order(['id desc'])->toArray();
        $move_data = array();

        if (!empty($all_moves)) {
            foreach ($all_moves as $key => $move) {
                if ($move['from_officer_designation_id'] == $user_designation) {
                    $row = array();
                    $row['name'] = $move['from_officer_name'] . "," . $move['from_officer_designation_label'] . "," . $move['from_office_unit_name'];
                    $row['value'] = $move['dak_actions'];
                    $row['move_date_time'] = $move['created'];
                    $row['id'] = $move['id'];
                    $move_data[] = $row;
                }
            }
        }

        $table_instance_unit_seal = TableRegistry::get('OfficeDesignationSeals');
        $seal_info = $table_instance_unit_seal->getSeal($employee_office);

        $result['sender_office'] = $seal_info;
        $result['dak_moves'] = $move_data;

        $result['attachment_dir'] = $this->request->webroot . 'content/';

        $jsonArray = array(
            "status" => "success",
            "data" => $result
        );
        echo json_encode($jsonArray);
        die;
    }

    public function postAPIDakDetail()
    {
        $id = $this->request->data['dak_id'];
        $user_designation = $this->request->data['user_designation'];
        $dak_type = $this->request->data['dak_type'];


        $jsonArray = array(
            "status" => "error",
            "error_code" => 404,
            "message" => "Invalid Request"
        );

        if (!defined("PROJAPOTI_API") || PROJAPOTI_API == false) {
            echo json_encode($jsonArray);
            die;
        }

        if (empty($user_designation)) {
            echo json_encode($jsonArray);
            die;
        }

        if (empty($id) || empty($dak_type)) {
            echo json_encode($jsonArray);
            die;
        }

        $apikey = isset($this->request->data['api_key']) ? $this->request->data['api_key'] : '';


        if (empty($apikey) || $this->checkToken($apikey) == FALSE) {
            echo json_encode($jsonArray);
            die;
        }
        if ($apikey != API_KEY) {
            //verify api token
            $this->loadComponent('ApiRelated');
            $getApiTokenData = $this->ApiRelated->getMobileApiTokenData($apikey);
            if (!empty($getApiTokenData)) {
                if (!in_array($user_designation, $getApiTokenData['designations'])) {
                    $jsonArray['message'] = 'Unauthorized request';
                    echo json_encode($jsonArray);
                    die;
                }
            }
            //verify api token
        }
        $EmployeeOfficesTable = TableRegistry::get('EmployeeOffices');
        $employee_office = $EmployeeOfficesTable->getDesignationInfo($user_designation);
        $this->switchOffice($employee_office['office_id'], 'Receiveroffice');
        //check access Dak
        if (!TableRegistry::get('DakMovements')->hasAccessInDak($id, $dak_type, $user_designation)) {
            echo json_encode($jsonArray);
            die;
        }
        $result = array();

        $table_instance_dak_user = TableRegistry::get('DakUsers');
        $dakUser = $table_instance_dak_user->find()->where(['dak_id' => $id, 'dak_type' => $dak_type])->first();

        $table_instance_dak_user->updateAll(['dak_view_status' => DAK_VIEW_STATUS_VIEW],
            ['id' => $dakUser['id']]);

        if ($dak_type == DAK_DAPTORIK) {
            $table_instance_dd = TableRegistry::get('DakDaptoriks');
        } else {
            $table_instance_dd = TableRegistry::get('DakNagoriks');
        }

        $dak_daptoriks_entity = $table_instance_dd->get($id);
        $result['dak_daptoriks'] = $dak_daptoriks_entity;


        $table_instance_da = TableRegistry::get('DakAttachments');
        $result['dak_attachments'] = $table_instance_da->loadAllAttachmentByDakId($id, $dak_type);


        $table_instance_dak_actions = TableRegistry::get('DakActions');
        $result['dak_actions'] = $table_instance_dak_actions->GetDakActions($employee_office['office_unit_organogram_id']);

        $sender = ($dak_type == DAK_DAPTORIK) ? ($dak_daptoriks_entity['sender_officer_designation_label'] . (!empty($dak_daptoriks_entity['sender_office_name'])
                ? (',' . $dak_daptoriks_entity['sender_office_name']) : '')) : ($dak_daptoriks_entity['sender_name']);
        $result['sender_office'] = $sender;

        $DakMovementsTable = TableRegistry::get('DakMovements');
        $all_moves = $DakMovementsTable->find()->where(['dak_id' => $id,
            'dak_type' => $dak_type])->order(['id desc'])->toArray();
        $move_data = array();

        if (!empty($all_moves)) {
            foreach ($all_moves as $key => $move) {
                if ($move['from_officer_designation_id'] == $user_designation) {
                    $row = array();
                    $row['name'] = $move['from_officer_name'] . "," . $move['from_officer_designation_label'] . "," . $move['from_office_unit_name'];
                    $row['value'] = $move['dak_actions'];
                    $row['move_date_time'] = $move['created'];
                    $row['id'] = $move['id'];
                    $move_data[] = $row;
                }
            }
        }

        $table_instance_unit_seal = TableRegistry::get('OfficeDesignationSeals');
        $seal_info = $table_instance_unit_seal->getSeal($employee_office);

        $result['sender_office'] = $seal_info;
        $result['dak_moves'] = $move_data;

        $result['attachment_dir'] = $this->request->webroot . 'content/';

        $jsonArray = array(
            "status" => "success",
            "data" => $result
        );
        echo json_encode($jsonArray);
        die;
    }

    public function apiGetSeal()
    {
        $user_designation = $this->request->query['user_designation'];

        $jsonArray = array(
            "status" => "error",
            "error_code" => 404,
            "message" => "Invalid Request"
        );

        if (!defined("PROJAPOTI_API") || PROJAPOTI_API == false) {
            echo json_encode($jsonArray);
            die;
        }

        if (empty($user_designation)) {
            echo json_encode($jsonArray);
            die;
        }

        $apikey = isset($this->request->query['api_key']) ? $this->request->query['api_key'] : '';

        if (empty($apikey) || $this->checkToken($apikey) == FALSE) {
            echo json_encode($jsonArray);
            die;
        }
        if ($apikey != API_KEY) {
            //verify api token
            $this->loadComponent('ApiRelated');
            $getApiTokenData = $this->ApiRelated->getMobileApiTokenData($apikey);
            if (!empty($getApiTokenData)) {
                if (!in_array($user_designation, $getApiTokenData['designations'])) {
                    $jsonArray['message'] = 'Unauthorized request';
                    echo json_encode($jsonArray);
                    die;
                }
            }
            //verify api token
        }
        $EmployeeOfficesTable = TableRegistry::get('EmployeeOffices');
        $employee_office = $EmployeeOfficesTable->getDesignationInfo($user_designation);
        if (!empty($employee_office)) {
            $this->switchOffice($employee_office['office_id'], 'Receiveroffice');
            $table_instance_unit_seal = TableRegistry::get('OfficeDesignationSeals');
            $seal_info = $table_instance_unit_seal->getSeal($employee_office);
        } else {
            $seal_info = [];
        }


        $jsonArray = array(
            "status" => "success",
            "data" => $seal_info
        );

        $this->response->body(json_encode($jsonArray));
        $this->response->type('application/json');
        return $this->response;
    }

    public function postApiGetSeal()
    {
        $user_designation = $this->request->data['user_designation'];

        $jsonArray = array(
            "status" => "error",
            "error_code" => 404,
            "message" => "Invalid Request"
        );

        if (!defined("PROJAPOTI_API") || PROJAPOTI_API == false) {
            echo json_encode($jsonArray);
            die;
        }

        if (empty($user_designation)) {
            echo json_encode($jsonArray);
            die;
        }

        $apikey = isset($this->request->data['api_key']) ? $this->request->data['api_key'] : '';

        if (empty($apikey) || $this->checkToken($apikey) == FALSE) {
            echo json_encode($jsonArray);
            die;
        }
        if ($apikey != API_KEY) {
            //verify api token
            $this->loadComponent('ApiRelated');
            $getApiTokenData = $this->ApiRelated->getMobileApiTokenData($apikey);
            if (!empty($getApiTokenData)) {
                if (!in_array($user_designation, $getApiTokenData['designations'])) {
                    $jsonArray['message'] = 'Unauthorized request';
                    echo json_encode($jsonArray);
                    die;
                }
            }
            //verify api token
        }

        $EmployeeOfficesTable = TableRegistry::get('EmployeeOffices');
        $employee_office = $EmployeeOfficesTable->getDesignationInfo($user_designation);
        $this->switchOffice($employee_office['office_id'], 'Receiveroffice');

        $table_instance_unit_seal = TableRegistry::get('OfficeDesignationSeals');
        $seal_info = $table_instance_unit_seal->getSeal($employee_office);

        $jsonArray = array(
            "status" => "success",
            "data" => $seal_info
        );

        $this->response->body(json_encode($jsonArray));
        $this->response->type('application/json');
        return $this->response;
    }

    public function APIForwardSelectedDak()
    {

        $designationId = $this->request->query['user_designation'];
        $daks = isset($this->request->query['dak_id']) ? $this->request->query['dak_id'] : ''; //comma seperator
        $dak_type = isset($this->request->query['dak_type']) ? $this->request->query['dak_type']
            : ''; //comma seperator
        $cc_designations = isset($this->request->data['cc_designations']) ? $this->request->data['cc_designations'] : ''; //comma seperator

        $jsonArray = array(
            "status" => "error",
            "error_code" => 404,
            "message" => "Invalid Request"
        );

        if (!defined("PROJAPOTI_API") || PROJAPOTI_API == false) {
            echo json_encode($jsonArray);
            die;
        }

        if (empty($designationId)) {
            echo json_encode($jsonArray);
            die;
        }

        if (empty($daks) || empty($dak_type)) {
            echo json_encode($jsonArray);
            die;
        }

        $apikey = isset($this->request->query['api_key']) ? $this->request->query['api_key'] : '';

        if (empty($apikey) || $this->checkToken($apikey) == FALSE) {
            echo json_encode($jsonArray);
            die;
        }
        if ($apikey != API_KEY) {
            //verify api token
            $this->loadComponent('ApiRelated');
            $getApiTokenData = $this->ApiRelated->getMobileApiTokenData($apikey);
            if (!empty($getApiTokenData)) {
                if (!in_array($designationId, $getApiTokenData['designations'])) {
                    $jsonArray['message'] = 'Unauthorized request';
                    echo json_encode($jsonArray);
                    die;
                }
            }
            //verify api token
        }
        $comment = !empty($this->request->data['comment']) ? $this->request->data['comment'] : '';
        $toOfficerorgId = $this->request->data['toOfficerId'];

        $to_priority_level = empty($this->request->data['to_priority_level']) ? 1 : $this->request->data['to_priority_level'];

        $EmployeeOfficesTable = TableRegistry::get('EmployeeOffices');
        $employee_office = $EmployeeOfficesTable->getDesignationInfo($designationId);

        $this->switchOffice($employee_office['office_id'], 'Receiveroffice');

        $DakUsersTable = TableRegistry::get('DakUsers');
        $request_data = array();
        $EmployeeRecordTable = TableRegistry::get('EmployeeRecords');
        $OfficesTable = TableRegistry::get('Offices');
        $table_instance_unit = TableRegistry::get('OfficeUnits');

        $subject = '';
        $current_office_section = $EmployeeOfficesTable->find()->where(['office_unit_organogram_id' => $designationId,
            'status' => 1])->first();
        $current_employee_record = $EmployeeRecordTable->find()->where(['id' => $employee_office['employee_record_id']])->first();
        $current_office_info = $OfficesTable->find()->where(['id' => $current_office_section['office_id']])->first();
        $sender_office_unit = $table_instance_unit->find()->where(['id' => $current_office_section['office_unit_id']])->first();

        $dakArray = explode(',', $daks);
        $toOfficerOrgIdArray = explode(',', $toOfficerorgId);
        $toPriorityLevelArray = explode(',', $to_priority_level);
        $cc_designations_array = (!empty($cc_designations) ? explode(',', $cc_designations) : []);
        $attension_array = [];

        if (!empty($cc_designations_array)) {
            foreach ($cc_designations_array as $c_k => $c_v) {
                $toOfficerOrgIdArray[] = $c_v;
            }
        }
        foreach ($toOfficerOrgIdArray as $to_k => $to_v) {
            if (in_array($to_v, $cc_designations_array)) {
                $attension_array[] = 0;
            } else {
                $attension_array[] = 1;
            }
        }

        if (!empty($dakArray)) {
            foreach ($dakArray as $key => $daks_id) {
                if (empty($daks_id)) continue;
                try {
                    $conn = ConnectionManager::get('default');
                    $conn->begin();
                    $seq_no = $this->getDakMovementSequence($daks_id, $dak_type);

                    $current_office_section['officer_id'] = $current_office_section['employee_record_id'];

                    $DakUsers_sender = $this->getExistingDakUser($daks_id,
                        $current_office_section['office_unit_id'],
                        $current_office_section['office_unit_organogram_id'], $dak_type);
                    if (empty($DakUsers_sender)) {
                        continue;
                    }
                    $DakUsersTable->updateAll(['dak_category' => DAK_CATEGORY_FORWARD],
                        ['id' => $DakUsers_sender['id']]);

                    if (count($toOfficerOrgIdArray) > 0) {
                        foreach ($toOfficerOrgIdArray as $keyOfc => $orgVal) {
                            $sealinfo = $this->getOfficerInfoForSealSelection($orgVal);
                            if (!empty($sealinfo['office_id']) && $sealinfo['office_id'] != $current_office_section['office_id']) {
                                continue;
                            }
                            $request_data['to_officer_id'] = $sealinfo['employee_record_id'];
                            $ofcVal = $sealinfo['employee_record_id'];
                            if (empty($sealinfo['employee_name'])) {
                                $sealinfo['employee_name'] = '';
                            }

                            $receiver_office_unit = $table_instance_unit->getOfficeUnitBy_designationId($sealinfo['designation_id']);

                            $DakUsers_receiver = $this->getExistingDakUser($daks_id,
                                $receiver_office_unit['id'], $sealinfo['designation_id'], $dak_type);

                            if (empty($DakUsers_receiver)) {
                                $DakUsers_receiver = $DakUsersTable->newEntity();
                                $DakUsers_receiver->dak_type = $dak_type;
                                $DakUsers_receiver->dak_id = $daks_id;
                                $DakUsers_receiver->to_office_id = $current_office_info['id'];
                                $DakUsers_receiver->to_office_name = $current_office_info['office_name_bng'];
                                $DakUsers_receiver->to_office_address = $current_office_info['office_address'];
                                $DakUsers_receiver->to_officer_id = $sealinfo['employee_record_id'];
                                $DakUsers_receiver->to_officer_name = $sealinfo['employee_name'];
                                $DakUsers_receiver->to_officer_designation_id = $sealinfo['designation_id'];
                                $DakUsers_receiver->to_officer_designation_label = $sealinfo['designation_label'];

                                if (!empty($receiver_office_unit)) {
                                    $DakUsers_receiver->to_office_unit_id = $receiver_office_unit['id'];
                                    $DakUsers_receiver->to_office_unit_name = $receiver_office_unit['unit_name_bng'];
                                }
                                $DakUsers_receiver->dak_view_status = DAK_VIEW_STATUS_NEW;
                                $DakUsers_receiver->dak_priority = isset($toPriorityLevelArray[0]) ? $toPriorityLevelArray[0] : 1;
                                $DakUsers_receiver->attention_type = (isset($attension_array[$keyOfc])) ? $attension_array[$keyOfc] : 1;//can be main or onulipi
                                $DakUsers_receiver->is_archive = 0;
                                $DakUsers_receiver->dak_category = DAK_CATEGORY_INBOX;

                                $DakUsers_receiver = $DakUsersTable->save($DakUsers_receiver);
                            } else {

                                $DakUsers_receiver = $DakUsersTable->get($DakUsers_receiver['id']);
                                $DakUsers_receiver->dak_view_status = DAK_VIEW_STATUS_NEW;
                                $DakUsers_receiver->dak_category = DAK_CATEGORY_INBOX;
                                $DakUsers_receiver->is_archive = 0;
                                $DakUsers_receiver->dak_priority = isset($toPriorityLevelArray[0]) ? $toPriorityLevelArray[0] : 1;
                                $DakUsers_receiver = $DakUsersTable->save($DakUsers_receiver);
                            }

                            if ($dak_type == DAK_NAGORIK) {
                                $dak_typ_bn = __('Nagorik Dak');
                                $DakDaptoriksTable = TableRegistry::get('DakNagoriks');
                            } else {
                                $dak_typ_bn = __('Daptorik Dak');
                                $DakDaptoriksTable = TableRegistry::get('DakDaptoriks');
                            }

                            $DakDaptoriks = $DakDaptoriksTable->get($daks_id);

                            $DakDaptoriks->modified = date("Y-m-d H:i:s");
                            $DakDaptoriksTable->save($DakDaptoriks);

                            $DakMovementsTable = TableRegistry::get('DakMovements');

//                            if (empty($comment)) {
//                                $comment = 'বিধি মোতাবেক ব্যবস্থা গ্রহণের জন্য প্রেরণ করা হল';
//                                //$comment = $dakMovementLastComment->dak_actions;
//                            }

                            /* Sender to Draft User Movement */
                            $second_move = $DakMovementsTable->newEntity();
                            $second_move->dak_type = $dak_type;
                            $second_move->dak_id = $daks_id;
                            $second_move->from_office_id = $current_office_section['office_id'];
                            $second_move->from_office_name = $current_office_info['office_name_bng'];
                            $second_move->from_office_address = $current_office_info['office_address'];
                            $second_move->from_office_unit_id = $current_office_section['office_unit_id'];
                            $second_move->from_office_unit_name = $sender_office_unit['unit_name_bng'];
                            $second_move->from_officer_id = $current_office_section['employee_record_id'];
                            $second_move->from_officer_name = $current_employee_record['name_bng'];
                            $second_move->from_officer_designation_id = $current_office_section['office_unit_organogram_id'];
                            $second_move->from_officer_designation_label = $current_office_section['designation'];
                            $second_move->to_office_id = $current_office_info['id'];
                            $second_move->to_office_name = $current_office_info['office_name_bng'];
                            $second_move->to_office_address = $current_office_info['office_address'];
                            $second_move->to_office_unit_id = $receiver_office_unit['id'];
                            $second_move->to_office_unit_name = $receiver_office_unit['unit_name_bng'];
                            $second_move->to_officer_id = $sealinfo['employee_record_id'];
                            $second_move->to_officer_name = $sealinfo['employee_name'];
                            $second_move->to_officer_designation_id = $sealinfo['designation_id'];
                            $second_move->to_officer_designation_label = $sealinfo['designation_label'];
                            $second_move->attention_type = (isset($attension_array[$keyOfc])) ? $attension_array[$keyOfc] : 1;//can be main or onulipi
                            $second_move->docketing_no = $DakDaptoriks['docketing_no'];
                            $second_move->from_sarok_no = "";
                            $second_move->to_sarok_no = "";
                            $second_move->operation_type = DAK_CATEGORY_FORWARD;
                            $second_move->dak_actions = $comment;
                            $second_move->sequence = $seq_no;
                            $second_move->dak_priority = isset($toPriorityLevelArray[0])
                                ? $toPriorityLevelArray[0] : 1;

                            $DakMovementsTable->save($second_move);

                            $toOfficers['office_id'] = $second_move->to_office_id;
                            $toOfficers['officer_id'] = $second_move->to_officer_id;
                            $subject = $DakDaptoriks['dak_subject'];

                            $feedbackmeta = !empty($DakDaptoriks['application_meta_data']) ? jsonA($DakDaptoriks['application_meta_data']) : [];
                            if (!empty($feedbackmeta['feedback_url'])) {
                                $feedbackdata = [
                                    'api_key' => !empty($feedbackmeta['feedback_api_key']) ? $feedbackmeta['feedback_api_key'] : '',
                                    'aid' => $feedbackmeta['tracking_id'],
                                    'action' => 1,
                                    'decision' => 1,
                                    'decision_note' => $comment,
                                    'dak_id' => $daks_id,
                                    'nothi_id' => 0,
                                    'note_id' => 0,
                                    'potro_id' => 0,
                                    'current_desk_id' => $employee_office['office_unit_organogram_id']
                                ];

                                $this->notifyFeedback($feedbackmeta['feedback_url'], $feedbackdata);
                            }
                            if( isset($current_employee_record['name_bng'])){
                                $current_office_section['name_bng'] =$current_employee_record['name_bng'];
                            }
                            if( isset($sender_office_unit['unit_name_bng'])){
                                $current_office_section['unit_name_bng'] =$sender_office_unit['unit_name_bng'];
                            }

                            if (!$this->NotificationSet('inbox', array(1, "Daptorik Dak"), 1,
                                $current_office_section, $toOfficers, $subject)) {

                            }
                            // Give Receiver Notification
                            $title = 'আপনার ডেস্কে নতুন ডাক এসেছে';
                            $mail_sender = [
                                'sender' => h($current_employee_record['name_bng'] . ', ' . $current_office_section['designation'] . ', ' . $sender_office_unit['unit_name_bng']),
                                'subject' => h($subject),
                                'dak_type' => h($dak_typ_bn),
                                'time' => Time::parse(date('Y-m-d H:i:s'))->i18nFormat(null, null,
                                    'bn-BD'),
//                        'time' => Time(date('H:i:s'))->i18nFormat(null,null,'bn-BD'),
                            ];
                            $mail_sender_notify = 'প্রেরকঃ ' . $mail_sender['sender'] . "\n বিষয়ঃ " . h($mail_sender['subject']) . "\n ডাকের ধরনঃ " . $mail_sender['dak_type'] . "\n সময়ঃ " . $mail_sender['time'];
                            $this->notifyAppUser('', $toOfficers['officer_id'],
                                ['title' => $title, 'body' => $mail_sender],
                                ['title' => $title, 'body' => $mail_sender_notify]);
                            // Give Receiver Notification
                        }
                    }

                    $table_instance_dua = TableRegistry::get('DakUserActions');

                    $user_action = $table_instance_dua->newEntity();
                    $user_action->dak_id = $daks_id;
                    $user_action->dak_user_id = $DakUsers_sender['id'];
                    $user_action->dak_type = $dak_type;
                    $user_action->dak_action = DAK_CATEGORY_FORWARD;

                    $table_instance_dua->save($user_action);

                    $DakDaptoriks->modified = date("Y-m-d H:i:s");
                    $DakDaptoriksTable->save($DakDaptoriks);

                    $conn->commit();
                    if (!$this->NotificationSet('forward', array(1, "Daptorik Dak"), 1,
                        $current_office_section, [], $subject)) {

                    }

                    $jsonArray = array(
                        "status" => "success",
                        "data" => ""
                    );
                    echo json_encode($jsonArray);
                    die;
                } catch (\Exception $ex) {
                    $conn->rollback();
                    $jsonArray = array(
                        "status" => "error",
                        "data" => $this->makeEncryptedData($ex->getMessage()),
                    );
                    echo json_encode($jsonArray);
                    die;
                }
            }
        }

        $this->response->body(json_encode(
                array(
                    "status" => "error",
                    "data" => 'দুঃখিত কোন তথ্য পাওয়া যায়নি।'
                )
            )
        );
        $this->response->type('application/json');
        return $this->response;
    }

    public function postAPIForwardSelectedDak()
    {
        $designationId = $this->request->data['user_designation'];
        $daks = isset($this->request->data['dak_id']) ? $this->request->data['dak_id'] : ''; //comma seperator
        $dak_types = isset($this->request->data['dak_type']) ? $this->request->data['dak_type'] : ''; //comma seperator

        $cc_designations = isset($this->request->data['cc_designations']) ? $this->request->data['cc_designations'] : ''; //comma seperator

        $jsonArray = array(
            "status" => "error",
            "error_code" => 404,
            "message" => "Invalid Request"
        );

        if (!defined("PROJAPOTI_API") || PROJAPOTI_API == false) {
            echo json_encode($jsonArray);
            die;
        }

        if (empty($designationId)) {
            echo json_encode($jsonArray);
            die;
        }

        if (empty($daks) || empty($dak_types)) {
            echo json_encode($jsonArray);
            die;
        }

        $apikey = isset($this->request->data['api_key']) ? $this->request->data['api_key'] : '';

        if (empty($apikey) || $this->checkToken($apikey) == FALSE) {
            echo json_encode($jsonArray);
            die;
        }
        if ($apikey != API_KEY) {
            //verify api token
            $this->loadComponent('ApiRelated');
            $getApiTokenData = $this->ApiRelated->getMobileApiTokenData($apikey);
            if (!empty($getApiTokenData)) {
                if (!in_array($designationId, $getApiTokenData['designations'])) {
                    $jsonArray['message'] = 'Unauthorized request';
                    echo json_encode($jsonArray);
                    die;
                }
            }
            //verify api token
        }
        $comment = !empty($this->request->data['comment']) ? $this->request->data['comment'] : '';
        $toOfficerorgId = $this->request->data['toOfficerId'];

        $to_priority_level = empty($this->request->data['to_priority_level']) ? 1 : $this->request->data['to_priority_level'];

        $EmployeeOfficesTable = TableRegistry::get('EmployeeOffices');
        $employee_office = $EmployeeOfficesTable->getDesignationInfo($designationId);

        $this->switchOffice($employee_office['office_id'], 'Receiveroffice');

        $DakUsersTable = TableRegistry::get('DakUsers');
        $request_data = array();
        $EmployeeRecordTable = TableRegistry::get('EmployeeRecords');
        $OfficesTable = TableRegistry::get('Offices');
        $table_instance_unit = TableRegistry::get('OfficeUnits');

        $subject = '';
        $current_office_section = $EmployeeOfficesTable->find()->where(['office_unit_organogram_id' => $designationId,
            'status' => 1])->first();
        $current_employee_record = $EmployeeRecordTable->find()->where(['id' => $employee_office['employee_record_id']])->first();
        $current_office_info = $OfficesTable->find()->where(['id' => $current_office_section['office_id']])->first();
        $sender_office_unit = $table_instance_unit->find()->where(['id' => $current_office_section['office_unit_id']])->first();

        $dakArray = explode(',', $daks);
        $dakTypeArray = explode(',', $dak_types);
        $toOfficerOrgIdArray = explode(',', $toOfficerorgId);
        $cc_designations_array = (!empty($cc_designations) ? explode(',', $cc_designations) : []);
        $toPriorityLevelArray = explode(',', $to_priority_level);
        $attension_array = [];

        if (!empty($cc_designations_array)) {
            foreach ($cc_designations_array as $c_k => $c_v) {
                $toOfficerOrgIdArray[] = $c_v;
            }
        }
        foreach ($toOfficerOrgIdArray as $to_k => $to_v) {
            if (in_array($to_v, $cc_designations_array)) {
                $attension_array[] = 0;
            } else {
                $attension_array[] = 1;
            }
        }

        $success = 0;
        if (!empty($dakArray)) {
            foreach ($dakArray as $key => $daks_id) {
                if (empty($daks_id)) continue;
                try {
                    $conn = ConnectionManager::get('default');
                    $conn->begin();
                    $dak_type = $dakTypeArray[$key];
                    $seq_no = $this->getDakMovementSequence($daks_id, $dak_type);

                    $current_office_section['officer_id'] = $current_office_section['employee_record_id'];

                    $DakUsers_sender = $this->getExistingDakUser($daks_id,
                        $current_office_section['office_unit_id'],
                        $current_office_section['office_unit_organogram_id'], $dak_type);
                    if (empty($DakUsers_sender)) {
                        continue;
                    }
                    $DakUsersTable->updateAll(['dak_category' => DAK_CATEGORY_FORWARD],
                        ['id' => $DakUsers_sender['id']]);
                    if (count($toOfficerOrgIdArray) > 0) {
                        foreach ($toOfficerOrgIdArray as $keyOfc => $orgVal) {
                            $sealinfo = $this->getOfficerInfoForSealSelection($orgVal);
                            if (!empty($sealinfo['office_id']) && $sealinfo['office_id'] != $current_office_section['office_id']) {
                                continue;
                            }
                            $request_data['to_officer_id'] = $sealinfo['employee_record_id'];
                            $ofcVal = $sealinfo['employee_record_id'];
                            if (empty($sealinfo['employee_name'])) {
                                $sealinfo['employee_name'] = '';
                            }

                            $receiver_office_unit = $table_instance_unit->getOfficeUnitBy_designationId($sealinfo['designation_id']);

                            $DakUsers_receiver = $this->getExistingDakUser($daks_id,
                                $receiver_office_unit['id'], $sealinfo['designation_id'], $dak_type);

                            if (empty($DakUsers_receiver)) {
                                $DakUsers_receiver = $DakUsersTable->newEntity();
                                $DakUsers_receiver->dak_type = $dak_type;
                                $DakUsers_receiver->dak_id = $daks_id;
                                $DakUsers_receiver->to_office_id = $current_office_info['id'];
                                $DakUsers_receiver->to_office_name = $current_office_info['office_name_bng'];
                                $DakUsers_receiver->to_office_address = $current_office_info['office_address'];
                                $DakUsers_receiver->to_officer_id = $sealinfo['employee_record_id'];
                                $DakUsers_receiver->to_officer_name = $sealinfo['employee_name'];
                                $DakUsers_receiver->to_officer_designation_id = $sealinfo['designation_id'];
                                $DakUsers_receiver->to_officer_designation_label = $sealinfo['designation_label'];

                                if (!empty($receiver_office_unit)) {
                                    $DakUsers_receiver->to_office_unit_id = $receiver_office_unit['id'];
                                    $DakUsers_receiver->to_office_unit_name = $receiver_office_unit['unit_name_bng'];
                                }
                                $DakUsers_receiver->dak_view_status = DAK_VIEW_STATUS_NEW;
                                $DakUsers_receiver->attention_type = (isset($attension_array[$keyOfc])) ? $attension_array[$keyOfc] : 1;//can be main or onulipi
                                $DakUsers_receiver->is_archive = 0;
                                $DakUsers_receiver->dak_category = DAK_CATEGORY_INBOX;
                                $DakUsers_receiver->dak_priority = isset($toPriorityLevelArray[0]) ? $toPriorityLevelArray[0] : 1;

                                $DakUsers_receiver = $DakUsersTable->save($DakUsers_receiver);
                            } else {

                                $DakUsers_receiver = $DakUsersTable->get($DakUsers_receiver['id']);
                                $DakUsers_receiver->dak_view_status = DAK_VIEW_STATUS_NEW;
                                $DakUsers_receiver->dak_category = DAK_CATEGORY_INBOX;
                                $DakUsers_receiver->is_archive = 0;
                                $DakUsers_receiver->dak_priority = isset($toPriorityLevelArray[0]) ? $toPriorityLevelArray[0] : 1;
                                $DakUsers_receiver = $DakUsersTable->save($DakUsers_receiver);
                            }

                            if ($dak_type == DAK_NAGORIK) {
                                $dak_typ_bn = __('Nagorik Dak');
                                $DakDaptoriksTable = TableRegistry::get('DakNagoriks');
                            } else {
                                $dak_typ_bn = __('Daptorik Dak');
                                $DakDaptoriksTable = TableRegistry::get('DakDaptoriks');
                            }

                            $DakDaptoriks = $DakDaptoriksTable->get($daks_id);

                            $DakDaptoriks->modified = date("Y-m-d H:i:s");
                            $DakDaptoriksTable->save($DakDaptoriks);

                            $DakMovementsTable = TableRegistry::get('DakMovements');

                            /* Sender to Draft User Movement */
                            $second_move = $DakMovementsTable->newEntity();
                            $second_move->dak_type = $dak_type;
                            $second_move->dak_id = $daks_id;
                            $second_move->from_office_id = $current_office_section['office_id'];
                            $second_move->from_office_name = $current_office_info['office_name_bng'];
                            $second_move->from_office_address = $current_office_info['office_address'];
                            $second_move->from_office_unit_id = $current_office_section['office_unit_id'];
                            $second_move->from_office_unit_name = $sender_office_unit['unit_name_bng'];
                            $second_move->from_officer_id = $current_office_section['employee_record_id'];
                            $second_move->from_officer_name = $current_employee_record['name_bng'];
                            $second_move->from_officer_designation_id = $current_office_section['office_unit_organogram_id'];
                            $second_move->from_officer_designation_label = $current_office_section['designation'];
                            $second_move->to_office_id = $current_office_info['id'];
                            $second_move->to_office_name = $current_office_info['office_name_bng'];
                            $second_move->to_office_address = $current_office_info['office_address'];
                            $second_move->to_office_unit_id = $receiver_office_unit['id'];
                            $second_move->to_office_unit_name = $receiver_office_unit['unit_name_bng'];
                            $second_move->to_officer_id = $sealinfo['employee_record_id'];
                            $second_move->to_officer_name = $sealinfo['employee_name'];
                            $second_move->to_officer_designation_id = $sealinfo['designation_id'];
                            $second_move->to_officer_designation_label = $sealinfo['designation_label'];
                            $second_move->attention_type = (isset($attension_array[$keyOfc])) ? $attension_array[$keyOfc] : 1;
                            $second_move->docketing_no = $DakDaptoriks['docketing_no'];
                            $second_move->from_sarok_no = "";
                            $second_move->to_sarok_no = "";
                            $second_move->operation_type = DAK_CATEGORY_FORWARD;
                            $second_move->dak_actions = $comment;
                            $second_move->sequence = $seq_no;
                            $second_move->dak_priority = isset($toPriorityLevelArray[0]) ? $toPriorityLevelArray[0] : 1;

                            $DakMovementsTable->save($second_move);

                            $toOfficers['office_id'] = $second_move->to_office_id;
                            $toOfficers['officer_id'] = $second_move->to_officer_id;
                            $subject = $DakDaptoriks['dak_subject'];

                            $feedbackmeta = !empty($DakDaptoriks['application_meta_data']) ? jsonA($DakDaptoriks['application_meta_data']) : [];
                            if (!empty($feedbackmeta['feedback_url'])) {
                                $feedbackdata = [
                                    'api_key' => !empty($feedbackmeta['feedback_api_key']) ? $feedbackmeta['feedback_api_key'] : '',
                                    'aid' => $feedbackmeta['tracking_id'],
                                    'action' => 1,
                                    'decision' => 1,
                                    'decision_note' => $comment,
                                    'dak_id' => $daks_id,
                                    'nothi_id' => 0,
                                    'note_id' => 0,
                                    'potro_id' => 0,
                                    'current_desk_id' => $employee_office['office_unit_organogram_id']
                                ];

                                $this->notifyFeedback($feedbackmeta['feedback_url'], $feedbackdata);
                            }

                            if( isset($current_employee_record['name_bng'])){
                                $current_office_section['name_bng'] =$current_employee_record['name_bng'];
                            }
                            if( isset($sender_office_unit['unit_name_bng'])){
                                $current_office_section['unit_name_bng'] =$sender_office_unit['unit_name_bng'];
                            }
                            if (!$this->NotificationSet('inbox', array(1, "Daptorik Dak"), 1,
                                $current_office_section, $toOfficers, $subject)) {

                            }
                            // Give Receiver Notification
                            $title = 'আপনার ডেস্কে নতুন ডাক এসেছে';
                            $mail_sender = [
                                'sender' => h($current_employee_record['name_bng'] . ', ' . $current_office_section['designation'] . ', ' . $sender_office_unit['unit_name_bng']),
                                'subject' => h($subject),
                                'dak_type' => h($dak_typ_bn),
                                'time' => Time::parse(date('Y-m-d H:i:s'))->i18nFormat(null, null,
                                    'bn-BD'),
//                        'time' => Time(date('H:i:s'))->i18nFormat(null,null,'bn-BD'),
                            ];
                            $mail_sender_notify = 'প্রেরকঃ ' . $mail_sender['sender'] . "\n বিষয়ঃ " . h($mail_sender['subject']) . "\n ডাকের ধরনঃ " . $mail_sender['dak_type'] . "\n সময়ঃ " . $mail_sender['time'];
                            $this->notifyAppUser('', $toOfficers['officer_id'],
                                ['title' => $title, 'body' => $mail_sender],
                                ['title' => $title, 'body' => $mail_sender_notify]);
                            // Give Receiver Notification
                        }
                    }

                    $table_instance_dua = TableRegistry::get('DakUserActions');
                    $DakUsers_sender = $this->getExistingDakUser($daks_id,
                        $current_office_section['office_unit_id'],
                        $current_office_section['office_unit_organogram_id'], $dak_type);

                    $user_action = $table_instance_dua->newEntity();
                    $user_action->dak_id = $daks_id;
                    $user_action->dak_user_id = $DakUsers_sender['id'];
                    $user_action->dak_type = $dak_type;
                    $user_action->dak_action = DAK_CATEGORY_FORWARD;

                    $table_instance_dua->save($user_action);

                    $DakDaptoriks->modified = date("Y-m-d H:i:s");
                    $DakDaptoriksTable->save($DakDaptoriks);

                    $conn->commit();
                    $success++;
                    if (!$this->NotificationSet('forward', array(1, "Daptorik Dak"), 1,
                        $current_office_section, [], $subject)) {

                    }
                } catch (\Exception $ex) {

                }
            }
        }

        if($success == count($dakArray)){
            $jsonArray = array(
                "status" => "success",
                "data" => ""
            );
        }
        else if($success == 0){
            $jsonArray = array(
                "status" => "error",
                "data" => "দুঃখিত! কোনো ডাক প্রেরণ করা সম্ভব হচ্ছে না"
            );
        }else{
            $jsonArray = array(
                "status" => "error",
                "data" => "দুঃখিত! কিছু ডাক প্রেরণ করা সম্ভব হচ্ছে না"
            );
        }

        $this->response->body(json_encode(
            $jsonArray
        ));
        $this->response->type('application/json');
        return $this->response;
    }

    public function APIUploadImage()
    {
        $designationId = $this->request->query['user_designation'];
        $dakId = isset($this->request->query['dak_id']) ? intval($this->request->query['dak_id'])
            : 0;
        $dakType = isset($this->request->query['dak_type']) ? intval($this->request->query['dak_type'])
            : 0;

        $jsonArray = array(
            "status" => "error",
            "error_code" => 404,
            "message" => "Invalid Request"
        );
        if (!defined("PROJAPOTI_API") || PROJAPOTI_API == false) {
            echo json_encode($jsonArray);
            die;
        }
        if (empty($designationId)) {
            echo json_encode($jsonArray);
            die;
        }
        $apikey = isset($this->request->query['api_key']) ? $this->request->query['api_key'] : '';


        if (empty($apikey) || $this->checkToken($apikey) == FALSE) {
            echo json_encode($jsonArray);
            die;
        }
        $OrganogramTable = TableRegistry::get('OfficeUnitOrganograms');
        $organogram = $OrganogramTable->find()->where(['id' => $designationId])->hydrate(false)->first();
        // pr($organogram);die;
        if (!empty($organogram)) {


            $img = $this->request->data['image'];
            $img = str_replace('data:image/png;base64,', '', $img);
            $img = str_replace(' ', '+', $img);
            $data = base64_decode($img);

            $path = 'Dak';

            $EmployeeOfficesTable = TableRegistry::get('EmployeeOffices');
            $employee_office = $EmployeeOfficesTable->find()->where(['office_unit_organogram_id' => $designationId,
                'status' => 1])->first();

            if (!is_dir(FILE_FOLDER_DIR . $path . '/')) {
                mkdir(FILE_FOLDER_DIR . $path . '/');
            }

            //office
            if (!is_dir(FILE_FOLDER_DIR . $path . '/' . $employee_office['office_id'] . '/')) {
                mkdir(FILE_FOLDER_DIR . $path . '/' . $employee_office['office_id'] . '/');
            }

            //year
            if (!is_dir(FILE_FOLDER_DIR . $path . '/' . $employee_office['office_id'] . '/' . date('Y') . '/')) {
                mkdir(FILE_FOLDER_DIR . $path . '/' . $employee_office['office_id'] . '/' . date('Y') . '/');
            }

            //month
            if (!is_dir(FILE_FOLDER_DIR . $path . '/' . $employee_office['office_id'] . '/' . date('Y') . '/' . date('m') . '/')) {
                mkdir(FILE_FOLDER_DIR . $path . '/' . $employee_office['office_id'] . '/' . date('Y') . '/' . date('m') . '/');
            }

            //day
            if (!is_dir(FILE_FOLDER_DIR . $path . '/' . $employee_office['office_id'] . '/' . date('Y') . '/' . date('m') . '/' . date('d') . '/')) {
                mkdir(FILE_FOLDER_DIR . $path . '/' . $employee_office['office_id'] . '/' . date('Y') . '/' . date('m') . '/' . date('d') . '/');
            }

            $file = $path . '/' . $employee_office['office_id'] . '/' . date('Y') . '/' . date('m') . '/' . date('d') . '/' . uniqid() . '.png';
            $success = file_put_contents(FILE_FOLDER_DIR . $file, $data);

            if ($success) {

                $this->switchOffice($employee_office['office_id'], 'Receiveroffice');

                $table_instance_dua = TableRegistry::get('DakAttachments');
                $user_action = $table_instance_dua->get($this->request->data['prev_id']);

                $user_action->file_name = $this->request->webroot . 'content/' . $file;
                $user_action->file_dir = $this->request->webroot . 'content/';
                $user_action->id = $this->request->data['prev_id'];
                try {
                    $table_instance_dua->save($user_action);

                    $jsonArray = array(
                        'status' => 'success',
                    );
                } catch (Exception $ex) {

                    $jsonArray = array(
                        'status' => 'error',
                        'data' => $ex,
                    );
                }
            } else {
                $jsonArray = array(
                    'status' => 'error',
                );
            }
        }

        echo json_encode($jsonArray);
        die;
    }

    public function apiViewDak()
    {
        $this->layout = 'online_dak';
        $this->view = 'api_view_dak_daptorik';

        $designationId = $this->request->query['user_designation'];
        $id = isset($this->request->query['dak_id']) ? $this->request->query['dak_id'] : '';
        $type = isset($this->request->query['dak_type']) ? $this->request->query['dak_type']
            : 'Daptorik';
        $is_new = isset($this->request->query['is_new']) ? $this->request->query['is_new'] : 0;

        if (!defined("PROJAPOTI_API") || PROJAPOTI_API == false) {
            die('Invalid Request');
        }

        if (empty($designationId)) {
            die('Invalid Designation');
        }

        if (empty($id)) {
            die('Invalid ID');
        }

        $apikey = isset($this->request->query['api_key']) ? $this->request->query['api_key'] : '';
        $this->set('query', $this->request->query);
        $this->set('designationId', $designationId);


        if (empty($apikey) || $this->checkToken($apikey) == FALSE) {
            die('Invalid Request');
        }
        if ($apikey != API_KEY) {
            //verify api token
            $this->loadComponent('ApiRelated');
            $getApiTokenData = $this->ApiRelated->getMobileApiTokenData($apikey);
            if (!empty($getApiTokenData)) {
                if (!in_array($designationId, $getApiTokenData['designations'])) {
                    die('Unauthorized request');
                }
            }
            //verify api token
        }
        $EmployeeOfficesTable = TableRegistry::get('EmployeeOffices');
        $employee_office = $EmployeeOfficesTable->find()->where(['office_unit_organogram_id' => $designationId,
            'status' => 1])->first();

        $this->switchOffice($employee_office['office_id'], 'Receiveroffice');


        if ($type == 'Daptorik') {
            $table_instance_dd = TableRegistry::get('DakDaptoriks');
        } else {
            $table_instance_dd = TableRegistry::get('DakNagoriks');
        }

        $table_instance_dak_user = TableRegistry::get('DakUsers');

        if (!empty($id)) {
            $dak_entity = $table_instance_dd->get($id);
        } else {
            die('Invalid Request');
        }

        $this->set('dak_details', $dak_entity);
        $this->set('type', $type);

        $dakUsers = $table_instance_dak_user->find()->where(['dak_id' => $dak_entity['id'],
            'to_officer_designation_id' => $employee_office['office_unit_organogram_id'],
            'dak_type' => $type])->toArray();
        if (!(TableRegistry::get('DakMovements')->hasAccessInDak($id, $type, $designationId))) {
            if (empty($dakUsers)) {
                die('Invalid request');
            }
        }
        foreach ($dakUsers as $dakUser) {
            $dakUser->dak_view_status = DAK_VIEW_STATUS_VIEW;
            $table_instance_dak_user->save($dakUser);
            if (isset($dakUser['attention_type'])) {
                $this->set('attention_type', $dakUser['attention_type']);
            }
        }

        $table_instance_da = TableRegistry::get('DakAttachments');
        $this->set('dak_attachments',
            $table_instance_da->loadAllAttachmentByDakId($dak_entity['id'], $type));


        /* Show Sender Information */
        $table_instance_dm = TableRegistry::get('DakMovements');

        $dak_priority_type_list = json_decode(DAK_PRIORITY_TYPE, true);
        $dak_security_level_list = json_decode(DAK_SECRECY_TYPE, true);

        $dak_priority = 1;

        $sender = $dak_entity['sender_officer_designation_label'] . (!empty($dak_entity['sender_office_name'])
                ? (',' . $dak_entity['sender_office_name']) : '');
        $dak_last_move = $table_instance_dm->getInboxDakLastMoveBy_dakid_attention_type($dak_entity['id'],
            $type);

        $move_date = $dak_entity['modified'];
        $receiver = '';

        $all_move_sequence = $table_instance_dm->getSequenceNumber($dak_entity['id'], $type);

        $i = 0;

        $move_seq = !empty($all_move_sequence) ? $all_move_sequence[0] : '';
        if (!empty($move_seq)) {
            $dak_moves_by_sequence = $table_instance_dm->getDakMovesBy_sequence_dakId($move_seq['sequence'],
                $dak_entity['id'], $type);
        }

        $dak_priority = 0;
        $move_date = $dak_entity['created'];
        $row_move = isset($dak_moves_by_sequence[0]) ? $dak_moves_by_sequence[0] : array();

        if (!empty($row_move)) {

            $sender = (!empty($row_move['from_officer_name']) ? ($row_move['from_officer_name'] . ', ')
                    : '') . $row_move['from_officer_designation_label'];

            if ($row_move['from_office_id'] != $employee_office['office_id']) {
                $sender .= ', ' . $row_move['from_office_name'];
            }
            $receiver = (!empty($row_move['to_officer_name']) ? ($row_move['to_officer_name'] . ", ")
                    : '') . $row_move['to_officer_designation_label'];
            $move_date = $row_move['modified'];
            $dak_priority = $row_move['dak_priority'] == 0 ? 1 : $row_move['dak_priority'];
        }
        $this->set('dak_priority_txt',
            isset($dak_priority_type_list[$dak_priority]) ? $dak_priority_type_list[$dak_priority] : '');
        $this->set('dak_security_txt',
            isset($dak_security_level_list[$dak_entity['dak_security_level']]) ? $dak_security_level_list[$dak_entity['dak_security_level']]
                : '');

        $this->set('dak_action', isset($row_move['dak_actions']) ? $row_move['dak_actions'] : '');


        $this->set('move_date', $move_date);
        $this->set('dak_priority', $dak_priority);
        $this->set('sender_office', $sender);
        $this->set('receiver', $receiver);
        $this->set('is_new', $is_new);
        $EmployeeRecordTable = TableRegistry::get('EmployeeRecords');
        $selected_office_section = $EmployeeRecordTable->get($employee_office['employee_record_id']);
        $this->set('employee_rec', $selected_office_section);
    }

    public function postApiViewDak()
    {
        $this->layout = 'online_dak';
        $this->view = 'api_view_dak_daptorik';

        $designationId = $this->request->data['user_designation'];
        $id = isset($this->request->data['dak_id']) ? $this->request->data['dak_id'] : '';
        $type = isset($this->request->data['dak_type']) ? $this->request->data['dak_type'] : 'Daptorik';
        // For cheking if request comes from old android version, we set a message on view.
        $is_new = 1;
        $this->set(compact('is_new'));

        if (!defined("PROJAPOTI_API") || PROJAPOTI_API == false) {
            die('Invalid Request');
        }

        if (empty($designationId)) {
            die('Invalid Designation');
        }

        if (empty($id)) {
            die('Invalid ID');
        }

        $apikey = isset($this->request->data['api_key']) ? $this->request->data['api_key'] : '';
        $this->set('query', $this->request->data);
        $this->set('designationId', $designationId);
        $this->set('type', $type);


        if (empty($apikey) || $this->checkToken($apikey) == FALSE) {
            die('Invalid Request');
        }
        if ($apikey != API_KEY) {
            //verify api token
            $this->loadComponent('ApiRelated');
            $getApiTokenData = $this->ApiRelated->getMobileApiTokenData($apikey);
            if (!empty($getApiTokenData)) {
                if (!in_array($designationId, $getApiTokenData['designations'])) {
                    die('Unauthorized request');
                }
            }
            //verify api token
        }
        $EmployeeOfficesTable = TableRegistry::get('EmployeeOffices');
        $employee_office = $EmployeeOfficesTable->find()->where(['office_unit_organogram_id' => $designationId,
            'status' => 1])->first();

        $this->switchOffice($employee_office['office_id'], 'Receiveroffice');


        if ($type == 'Daptorik') {
            $table_instance_dd = TableRegistry::get('DakDaptoriks');
        } else {
            $table_instance_dd = TableRegistry::get('DakNagoriks');
        }

        $table_instance_dak_user = TableRegistry::get('DakUsers');

        if (!empty($id)) {
            $dak_entity = $table_instance_dd->get($id);
        } else {
            die('Invalid Request');
        }

        $this->set('dak_details', $dak_entity);


        $dakUsers = $table_instance_dak_user->find()->where(['dak_id' => $dak_entity['id'],
            'to_officer_designation_id' => $employee_office['office_unit_organogram_id'],
            'dak_type' => $type])->toArray();
        if (!TableRegistry::get('DakMovements')->hasAccessInDak($id, $type, $employee_office['office_unit_organogram_id'])) {
            if (empty($dakUsers)) {
                die('Invalid request');
            }
        }
        foreach ($dakUsers as $dakUser) {
            $dakUser->dak_view_status = DAK_VIEW_STATUS_VIEW;
            $table_instance_dak_user->save($dakUser);
            if (isset($dakUser['attention_type'])) {
                $this->set('attention_type', $dakUser['attention_type']);
            }
        }

        $table_instance_da = TableRegistry::get('DakAttachments');
        $this->set('dak_attachments',
            $table_instance_da->loadAllAttachmentByDakId($dak_entity['id'], $type));


        /* Show Sender Information */
        $table_instance_dm = TableRegistry::get('DakMovements');

        $dak_priority_type_list = json_decode(DAK_PRIORITY_TYPE, true);
        $dak_security_level_list = json_decode(DAK_SECRECY_TYPE, true);

        $dak_priority = 1;

        $sender = $dak_entity['sender_officer_designation_label'] . (!empty($dak_entity['sender_office_name'])
                ? (',' . $dak_entity['sender_office_name']) : '');
        $dak_last_move = $table_instance_dm->getInboxDakLastMoveBy_dakid_attention_type($dak_entity['id'],
            $type);

        $move_date = $dak_entity['modified'];
        $receiver = '';

        $all_move_sequence = $table_instance_dm->getSequenceNumber($dak_entity['id'], $type);

        $i = 0;

        $move_seq = !empty($all_move_sequence) ? $all_move_sequence[0] : '';
        if (!empty($move_seq)) {
            $dak_moves_by_sequence = $table_instance_dm->getDakMovesBy_sequence_dakId($move_seq['sequence'],
                $dak_entity['id'], $type);
        }

        $dak_priority = 0;
        $move_date = $dak_entity['created'];
        $row_move = isset($dak_moves_by_sequence[0]) ? $dak_moves_by_sequence[0] : array();

        if (!empty($row_move)) {

            $sender = (!empty($row_move['from_officer_name']) ? ($row_move['from_officer_name'] . ', ')
                    : '') . $row_move['from_officer_designation_label'];

            if ($row_move['from_office_id'] != $employee_office['office_id']) {
                $sender .= ', ' . $row_move['from_office_name'];
            }
            $receiver = (!empty($row_move['to_officer_name']) ? ($row_move['to_officer_name'] . ", ")
                    : '') . $row_move['to_officer_designation_label'];
            $move_date = $row_move['modified'];
            $dak_priority = $row_move['dak_priority'] == 0 ? 1 : $row_move['dak_priority'];
        }
        $this->set('dak_priority_txt',
            isset($dak_priority_type_list[$dak_priority]) ? $dak_priority_type_list[$dak_priority] : '');
        $this->set('dak_security_txt',
            isset($dak_security_level_list[$dak_entity['dak_security_level']]) ? $dak_security_level_list[$dak_entity['dak_security_level']]
                : '');

        $this->set('dak_action', !empty($row_move['dak_actions']) ? $row_move['dak_actions'] : '');


        $this->set('move_date', $move_date);
        $this->set('dak_priority', $dak_priority);
        $this->set('sender_office', $sender);
        $this->set('receiver', $receiver);
        $EmployeeRecordTable = TableRegistry::get('EmployeeRecords');
        $selected_office_section = $EmployeeRecordTable->get($employee_office['employee_record_id']);
        $this->set('employee_rec', $selected_office_section);
    }

    public function postApiArchiveDak()
    {
        $designationId = $this->request->data['user_designation'];
        $dak_id = isset($this->request->data['dak_id']) ? $this->request->data['dak_id'] : '';
        $dakType = isset($this->request->data['dak_type']) ? $this->request->data['dak_type'] : 'Daptorik';

        if (!defined("PROJAPOTI_API") || PROJAPOTI_API == false) {
            die('Invalid Request');
        }

        if (empty($designationId)) {
            die('Invalid Designation');
        }

        if (empty($dak_id)) {
            die('Invalid ID');
        }

        $apikey = isset($this->request->data['api_key']) ? $this->request->data['api_key'] : '';

        if (empty($apikey) || $this->checkToken($apikey) == FALSE) {
            die('Invalid Request');
        }
        if ($apikey != API_KEY) {
            //verify api token
            $this->loadComponent('ApiRelated');
            $getApiTokenData = $this->ApiRelated->getMobileApiTokenData($apikey);
            if (!empty($getApiTokenData)) {
                if (!in_array($designationId, $getApiTokenData['designations'])) {
                    die('Unauthorized request');
                }
            }
            //verify api token
        }

        $EmployeeOfficesTable = TableRegistry::get('EmployeeOffices');
        $user = $EmployeeOfficesTable->find()->where(['office_unit_organogram_id' => $designationId, 'status' => 1])->first();

        $this->switchOffice($user['office_id'], 'Receiveroffice');
        $DakUsers_receiver = $this->getExistingDakUser($dak_id,
            $user['office_unit_id'],
            $user['office_unit_organogram_id'], $dakType);
        if (empty($DakUsers_receiver) || $DakUsers_receiver['dak_category'] != DAK_CATEGORY_INBOX) {
            echo json_encode(array('status' => 'error', 'msg' => 'তথ্য পাওয়া যায়নি'));
            die;
        }
        $dakUsersTable = TableRegistry::get('DakUsers');

        $dakUsersTable->updateAll(['is_archive' => 1], ['dak_id' => $dak_id, 'dak_type' => $dakType, 'to_officer_designation_id' => $designationId]);

        echo json_encode(array('status' => 'success', 'msg' => 'ডাকটি আইকাইভ করা হয়েছে। '));
//        if (!empty($getDak)) {
//            $entity             = $dakUsersTable->get($getDak['id']);
//            $entity->is_archive = 1;
//
//            $dakUsersTable->save($entity);
//
//            echo json_encode(array('status' => 'success', 'msg' => 'ডাকটি আইকাইভ করা হয়েছে। '));
//        }
//        else {
//            echo json_encode(array('status' => 'error', 'msg' => 'তথ্য পাওয়া যায়নি'));
//        }
        die;
    }

    public function apiArchiveDak()
    {
        $designationId = $this->request->query['user_designation'];
        $dak_id = isset($this->request->query['dak_id']) ? $this->request->query['dak_id'] : '';
        $dakType = isset($this->request->query['dak_type']) ? $this->request->query['dak_type']
            : 'Daptorik';

        if (!defined("PROJAPOTI_API") || PROJAPOTI_API == false) {
            die('Invalid Request');
        }

        if (empty($designationId)) {
            die('Invalid Designation');
        }

        if (empty($dak_id)) {
            die('Invalid ID');
        }

        $apikey = isset($this->request->query['api_key']) ? $this->request->query['api_key'] : '';

        if (empty($apikey) || $this->checkToken($apikey) == FALSE) {
            die('Invalid Request');
        }
        if ($apikey != API_KEY) {
            //verify api token
            $this->loadComponent('ApiRelated');
            $getApiTokenData = $this->ApiRelated->getMobileApiTokenData($apikey);
            if (!empty($getApiTokenData)) {
                if (!in_array($designationId, $getApiTokenData['designations'])) {
                    die('Unauthorized request');
                }
            }
            //verify api token
        }

        $EmployeeOfficesTable = TableRegistry::get('EmployeeOffices');
        $user = $EmployeeOfficesTable->find()->where(['office_unit_organogram_id' => $designationId,
            'status' => 1])->first();

        $this->switchOffice($user['office_id'], 'Receiveroffice');
        $DakUsers_receiver = $this->getExistingDakUser($dak_id,
            $user['office_unit_id'],
            $user['office_unit_organogram_id'], $dakType);
        if (empty($DakUsers_receiver) || $DakUsers_receiver['dak_category'] != DAK_CATEGORY_INBOX) {
            echo json_encode(array('status' => 'error', 'msg' => 'তথ্য পাওয়া যায়নি'));
            die;
        }
        $dakUsersTable = TableRegistry::get('DakUsers');

        $dakUsersTable->updateAll(['is_archive' => 1], ['dak_id' => $dak_id, 'dak_type' => $dakType, 'to_officer_designation_id' => $user['office_unit_organogram_id']]);

        echo json_encode(array('status' => 'success', 'msg' => 'ডাকটি আইকাইভ করা হয়েছে। '));

//        if (!empty($getDak)) {
//            $entity             = $dakUsersTable->get($getDak['id']);
//            $entity->is_archive = 1;
//
//            $dakUsersTable->save($entity);
//
//
//        }
//        else {
//            echo json_encode(array('status' => 'error', 'msg' => 'তথ্য পাওয়া যায়নি'));
//        }
        die;
    }

    public function dakMovementHistory()
    {

        $designationId = $this->request->query['user_designation'];
        $dak_id = isset($this->request->query['dak_id']) ? $this->request->query['dak_id'] : '';
        $dakType = isset($this->request->query['dak_type']) ? $this->request->query['dak_type']
            : 'Daptorik';

        if (!defined("PROJAPOTI_API") || PROJAPOTI_API == false) {
            die('Invalid Request');
        }

        if (empty($designationId)) {
            die('Invalid Designation');
        }

        if (empty($dak_id)) {
            die('Invalid ID');
        }

        $apikey = isset($this->request->query['api_key']) ? $this->request->query['api_key'] : '';

        if (!defined("API_KEY") || API_KEY == false || empty($apikey) || $apikey != API_KEY) {
            die('Invalid Request');
        }

        $EmployeeOfficesTable = TableRegistry::get('EmployeeOffices');
        $employee_office = $EmployeeOfficesTable->find()->where(['office_unit_organogram_id' => $designationId,
            'status' => 1])->first();

        $this->switchOffice($employee_office['office_id'], 'Receiveroffice');


        $table_instance_dm = TableRegistry::get('DakMovements');

        $all_move_sequence = $table_instance_dm->getDakMovesBydakId($dak_id, $dakType);

        $this->response->body(json_encode(['status' => 'success', 'data' => $all_move_sequence]));
        $this->response->type('application/json');
        return $this->response;
    }

    public function viewNothiJatoDakSingle()
    {
        $this->layout = null;

        $table_instance_dm = TableRegistry::get('DakMovements');
        $table_instance_da = TableRegistry::get('DakAttachments');

        $id = !empty($this->request->data['dak_id']) ? $this->request->data['dak_id'] : 0;
        $dak_type = !empty($this->request->data['dak_type']) ? $this->request->data['dak_type'] : '';
        $part_no = !empty($this->request->data['part_no']) ? getDecryptedData($this->request->data['part_no']) : '';
        $nothi_office = !empty($this->request->data['nothi_office']) ? $this->request->data['nothi_office'] : '';
        if (empty($id) || empty($dak_type) || empty($nothi_office)) return;
        if (empty($part_no) || intval($part_no) < 1) {
            return;
        }
        if ($dak_type == 'Daptorik') {
            $table_instance_dd = TableRegistry::get('DakDaptoriks');
        } else if ($dak_type == 'Nagorik') {
            $table_instance_dd = TableRegistry::get('DakNagoriks');
        }

        $employee_office = $this->getCurrentDakSection();

        if (!TableRegistry::get('NothiMasterPermissions')->hasAccessPartList($employee_office['office_id'], $employee_office['office_unit_id'], $employee_office['office_unit_organogram_id'], $part_no, $nothi_office)) {
            die('Invalid Request');
        }
        if (!empty($id)) {
            $dak_daptoriks_entity = $table_instance_dd->get($id);
        }
        $this->set('dak_daptoriks', $dak_daptoriks_entity);

        $this->set('dak_attachments',
            $table_instance_da->loadAllAttachmentByDakId($dak_daptoriks_entity['id'], $dak_type));

        $dak_priority_type_list = json_decode(DAK_PRIORITY_TYPE, true);
        $dak_security_level_list = json_decode(DAK_SECRECY_TYPE, true);

        $dak_last_move = $table_instance_dm->getInboxDakLastMoveBy_dakid_attention_type($dak_daptoriks_entity['id'],
            1, $dak_type);

        $is_revert_enabled = 0;
        $dak_priority = 1;
        $move_date = $dak_daptoriks_entity['modified'];
        $_is_allowed_to_sent_back = 0;
        if (!empty($dak_last_move)) {
            $sender = (!empty($dak_last_move['to_officer_name']) ? ($dak_last_move['to_officer_name'] . ", ")
                    : '') . $dak_last_move['to_officer_designation_label'];
            $dak_priority = $dak_last_move['dak_priority'] == 0 ? 1 : $dak_last_move['dak_priority'];
            $move_date = $dak_last_move['modified'];
            if ($dak_last_move['from_officer_designation_id'] == $employee_office['office_unit_organogram_id']
                && $dak_last_move['operation_type'] == DAK_CATEGORY_NOTHIJATO) {
                $is_revert_enabled = 1;
                $_is_allowed_to_sent_back = 1;
            }
        }

        $this->set('dak_priority', $dak_priority);
        $this->set('dak_priority_txt',
            isset($dak_priority_type_list[$dak_priority]) ? $dak_priority_type_list[$dak_priority] : '');
        $this->set('dak_security_txt',
            isset($dak_security_level_list[$dak_daptoriks_entity['dak_security_level']]) ? $dak_security_level_list[$dak_daptoriks_entity['dak_security_level']]
                : '');
        $this->set('sender_office', $sender);
        $this->set(compact('move_date'));

        $this->set('is_revert_enabled', $is_revert_enabled);

        $this->set('selected_office_section', $employee_office);

        $this->set('is_allowed_to_sent_back', $_is_allowed_to_sent_back);
        $this->set('dak_id', $id);
        $this->set('dak_type', $dak_type);
    }

//    public function dakMakeArchive($office_id)
//    {
//        set_time_limit(0);
//        ini_set('memory_limit', -1);
//        $this->switchOffice($office_id, "archiveOffice");
//        $this->layout = null;
//
//
//        TableRegistry::remove('DakDaptoriks');
//        TableRegistry::remove('DakNagoriks');
//        TableRegistry::remove('DakMovements');
//        TableRegistry::remove('DakUsers');
//
//        $dak_daptorik_table      = TableRegistry::get('DakDaptoriks');
//        $dak_nagorik_table       = TableRegistry::get('DakNagoriks');
//        $table_instance_dm       = TableRegistry::get('DakMovements');
//        $table_instance_dak_user = TableRegistry::get('DakUsers');
//
//        $dak_daptoriks = array();
//        $dak_nagoriks  = array();
//
//        $user_new_daks = $table_instance_dak_user->find()->select(['dak_id', 'dak_view_status',
//                'dak_type'])->where(['dak_category' => DAK_CATEGORY_INBOX, 'to_office_id' => $office_id,
//                'is_archive' => 0, 'date(modified) <=' => '2016-10-10'])->toArray();
//
//        $ng_ids = array();
//        $dp_ids = array();
//        foreach ($user_new_daks as $new_dak) {
//            if ($new_dak['dak_type'] == DAK_DAPTORIK) {
//                $dp_ids[] = $new_dak['dak_id'];
//            } else if ($new_dak['dak_type'] == DAK_NAGORIK) {
//                $ng_ids[] = $new_dak['dak_id'];
//            }
//        }
//
//        if (count($dp_ids) > 0) {
//
//            $dak_daptoriks = $dak_daptorik_table->find()->where(['id IN' => $dp_ids])->order(['modified DESC'])->toArray();
//        }
//        if (count($ng_ids) > 0) {
//
//            $dak_nagoriks = $dak_nagorik_table->find()->where(['id IN' => $ng_ids])->order(['modified DESC'])->toArray();
//        }
//
//        $all_daks = array_merge($dak_daptoriks, $dak_nagoriks);
//
////        $co  = 0;
//        if (!empty($all_daks)) {
//            foreach ($all_daks as $dak) {
//                $dak_last_move = $table_instance_dm->getInboxDakLastMoveBy_dakid_attention_type($dak['id'],
//                    1, (!isset($dak['dak_type_id']) ? "Daptorik" : "Nagorik"));
//
//                if ($dak_last_move['attention_type'] == 1) {
////                    $co++;
//                    $table_instance_dak_user->updateAll(['dak_category' => 'Forward'],
//                        ['dak_id' => $dak_last_move['dak_id'], 'dak_type' => (!isset($dak['dak_type_id'])
//                                ? "Daptorik" : "Nagorik"), 'to_officer_designation_id <>' => $dak_last_move['to_officer_designation_id'],
//                        "dak_category" => 'Inbox', 'attention_type' => 1]);
//                } else {
//                    continue;
//                }
//            }
//        }
//        echo json_encode(['status' => 'success']);
//        die;
//    }

    public function dakMakeArchive($id, $start_date = "2016-01-01", $end_date = "2016-10-10")
    {
        set_time_limit(0);
        ini_set('memory_limit', "1024M");
        $this->switchOffice($id, 'OfficeDb');
        TableRegistry::remove('DakMovements');
        TableRegistry::remove('DakUsers');

        $query1 = "SELECT m1.dak_type, m1.dak_id, max(m1.sequence) as seq FROM dak_movements m1 WHERE date(m1.modified) >= '{$start_date}'  and date(m1.modified) <= '{$end_date}' and m1.to_office_id = {$id} and m1.attention_type = 1 and m1.sequence > 1 group by dak_id, dak_type
ORDER BY `seq`  ASC";

        $conn = ConnectionManager::get('default');
        $stmt = $conn->query($query1);
        $stmt->execute();
        $data = $stmt->fetchAll('assoc');

        echo "Total: " . count($data);
        if (!empty($data)) {
            foreach ($data as $key => $value) {

                $query2 = "update dak_users inner join dak_movements on (dak_movements.dak_id = dak_users.dak_id and dak_movements.dak_type = dak_users.dak_type)
                    set dak_category = 'Forward'
where dak_movements.sequence = {$value['seq']} and dak_users.dak_id = {$value['dak_id']} and dak_users.dak_type = '{$value['dak_type']}' and dak_movements.to_officer_designation_id <> dak_users.to_officer_designation_id and dak_category = 'Inbox' and dak_users.attention_type = 1 and dak_users.is_archive = 0";

                $stmt = $conn->query($query2);
                $stmt->execute();
            }
        }
        echo json_encode(['status' => 'success']);
        die;
    }

    public function dakTrack()
    {
        if ($this->request->is('post')) {
            $j = 0;
            $response = array(
                'status' => 'error',
                'msg' => "দুঃখিত! আপনি কোনো তথ্য প্রদান করেন নি। দয়া করে যে কোনো একটি তথ্য প্রদান করুন।",
                'error' => $j
            );
            $employee_details = $this->getCurrentDakSection();

            if (!empty($this->request->data['receive_no']) || !empty($this->request->data['mobile_no']) || !empty($this->request->data['subject']) || (!empty($this->request->data['date_from']) && !empty($this->request->data['date_to']))) {

                if (!empty($employee_details)) {
					if (!empty($this->request->data['receive_no'])) {
						$dak_type = substr($this->request->data('receive_no'), 0, 1);
						if ($dak_type == 1) {
							$dak_table = TableRegistry::get('DakNagoriks');
						} else {
							$dak_table = TableRegistry::get('DakDaptoriks');
						}
						$dak_table_data = $dak_table->find();
						$dak_table_data->where(['dak_received_no' => h($this->request->data['receive_no'])]);
						if (!empty($this->request->data['mobile_no'])) {
							if ($dak_type == 1) {
								$dak_table_data->where(['mobile_no' => h($this->request->data['mobile_no'])]);
							} else {
								$dak_table_data->where(['sender_mobile' => h($this->request->data['mobile_no'])]);
							}
						}
						if (!empty($this->request->data['date_from'])) {
							$dak_table_data->where(['date(created) >=' => date("Y-m-d", strtotime($this->request->data['date_from']))]);
						}
						if (!empty($this->request->data['date_to'])) {
							$dak_table_data->where(['date(created) <=' => date("Y-m-d", strtotime($this->request->data['date_to']))]);
						}
						if (!empty($this->request->data['subject'])) {
							$dak_table_data->where(['dak_subject like ' => '%' . h($this->request->data['subject']) . '%']);
						}
						$dak_track_data = $dak_table_data->group(['dak_received_no'])->toArray();
					} else {
						$dak_table = TableRegistry::get('DakDaptoriks');
						$dak_table_data_daptorik = $dak_table->find();
						if (!empty($this->request->data['mobile_no'])) {
							$dak_table_data_daptorik->where(['sender_mobile' => h($this->request->data['mobile_no'])]);
						}
						if (!empty($this->request->data['date_from'])) {
							$dak_table_data_daptorik->where(['date(created) >=' => date("Y-m-d", strtotime($this->request->data['date_from']))]);
						}
						if (!empty($this->request->data['date_to'])) {
							$dak_table_data_daptorik->where(['date(created) <=' => date("Y-m-d", strtotime($this->request->data['date_to']))]);
						}
						if (!empty($this->request->data['subject'])) {
							$dak_table_data_daptorik->where(['dak_subject like ' => '%' . h($this->request->data['subject']) . '%']);
						}
						$dak_track_data_daptorik = $dak_table_data_daptorik->group(['dak_received_no'])->toArray();

						$dak_table = TableRegistry::get('DakNagoriks');
						$dak_table_data_nagorik = $dak_table->find();
						if (!empty($this->request->data['mobile_no'])) {
							$dak_table_data_nagorik->where(['mobile_no' => h($this->request->data['mobile_no'])]);
						}
						if (!empty($this->request->data['date_from'])) {
							$dak_table_data_nagorik->where(['date(created) >=' => date("Y-m-d", strtotime($this->request->data['date_from']))]);
						}
						if (!empty($this->request->data['date_to'])) {
							$dak_table_data_nagorik->where(['date(created) <=' => date("Y-m-d", strtotime($this->request->data['date_to']))]);
						}
						if (!empty($this->request->data['subject'])) {
							$dak_table_data_nagorik->where(['dak_subject like ' => '%' . h($this->request->data['subject']) . '%']);
						}
						$dak_track_data_nagorik = $dak_table_data_nagorik->group(['dak_received_no'])->toArray();
						$dak_track_data = array_merge(array_values($dak_track_data_daptorik), array_values($dak_track_data_nagorik));
					}
                } else {
					$dakTracksTable = TableRegistry::get('DakTracks');
					$dak_track_data = $dakTracksTable->find();
					if (!empty($this->request->data['receive_no'])) {
						$dak_track_data->where(['dak_received_no' => h($this->request->data['receive_no'])]);
					}

					if (!empty($this->request->data['mobile_no'])) {
						$dak_track_data->where(['sender_mobile' => h($this->request->data['mobile_no'])]);
					}

					if (!empty($this->request->data['date_from'])) {
						$dak_track_data->where(['date(created) >=' => date("Y-m-d", strtotime($this->request->data['date_from']))]);
					}

					if (!empty($this->request->data['date_to'])) {
						$dak_track_data->where(['date(created) <=' => date("Y-m-d", strtotime($this->request->data['date_to']))]);
					}

					if (!empty($this->request->data['subject'])) {
						$dak_track_data->where(['dak_subject like ' => '%' . h($this->request->data['subject']) . '%']);
					}
					$dak_track_data = $dak_track_data->group(['dak_received_no'])->toArray();
				}

				if (!empty($dak_track_data)) {
                    $i = 0;
                    $data = [];
                    foreach ($dak_track_data as $dak_track_data) {
                        $this->switchOffice($dak_track_data['receiving_office_id'], 'OfficeDB');
                        $checkDBConnection = $this->checkCurrentOfficeDBConnection();
						$dak_type_id = substr($dak_track_data->dak_received_no, 0, 1);
						if ($dak_type_id == 1) {
							$dak_track_data['dak_type'] = 'Nagorik';
						} else {
							$dak_track_data['dak_type'] = 'Daptorik';
						}
                        if (empty($checkDBConnection) || (!empty($checkDBConnection['status']) && $checkDBConnection['status'] == 'error')) {
                            ++$j;
                            continue;
                        }

                        if ($dak_track_data['dak_type'] == 'Daptorik') {
                            $conditions = [];
                            TableRegistry::remove('DakMovements');
                            $dakMovements_table = TableRegistry::get("DakMovements");

                            $conditions['DakDaptoriks.receiving_office_id'] = intval($dak_track_data['receiving_office_id']);

                            if (!empty($dak_track_data['dak_received_no'])) {
                                $conditions['DakDaptoriks.dak_received_no'] = $dak_track_data['dak_received_no'];
                            }
                            $tracking = $dakMovements_table->getLastofEachDaptorik($conditions);
                            if (!empty($tracking[0])) {
                                $data[$i] = $tracking[0];
                            }
                        } else {
                            TableRegistry::remove('DakMovements');

                            $dakMovements_table = TableRegistry::get("DakMovements");
                            $conditions = [];

                            if (!empty($dak_track_data['dak_received_no'])) {
                                $conditions['DakNagoriks.dak_received_no'] = $dak_track_data['dak_received_no'];
                            }

                            $tracking = $dakMovements_table->getLastStatusofEachNaogirk($conditions);

                            if (empty($tracking)) $tracking[0] = [];
                            $data[$i] = $tracking[0];

                            if (!empty($data[$i])) {
                                if ($data[$i]['sequence'] > 1) {
                                    $data[$i]['dak_actions'] = "আপনার আবেদনটি প্রক্রিয়াধীন আছে (বর্তমান ডেস্ক: {$data[$i]['to_officer_designation_label']}, {$data[$i]['to_office_unit_name']}, গ্রহণের সময়: " . $data[$i]['movemodified'] . ")";
                                }
                            }
                        }
                        ++$i;
                    }
                    $response = array(
                        'status' => 'success',
                        'data' => $data,
                        'error' => $j
                    );
                    $this->switchOffice($employee_details['office_id'], 'OfficeDB');
                } else {
                    $response = array(
                        'status' => 'error',
                        'msg' => "দুঃখিত! কোনো ডাক পাওয়া যায় নি",
                        'error' => 1
                    );
                }
            } else {
                $response = array(
                    'status' => 'error',
                    'msg' => "দুঃখিত! কোনো ডাক পাওয়া যায় নি",
                    'error' => 1
                );
            }
            $this->response->body(json_encode($response));
            $this->response->type('application/json');
            return $this->response;
        }
    }

    /* API of the Controller  */

    public function rollbackFromArchieveToDak()
    {
        if (empty($this->request->data['dak_id']) || empty($this->request->data['dak_type'])) {
            $this->response->body(json_encode(['status' => 'error', 'msg' => __("ডাক তথ্য পাওয়া যায়নি")]));
            $this->response->type('application/json');
            return $this->response;
        }
        $dak_id = intval($this->request->data['dak_id']);
        $dak_type = !empty($this->request->data['dak_type']) ? $this->request->data['dak_type'] : 'Daptorik';

        $employee_office = $this->getCurrentDakSection();

        TableRegistry::remove('DakMovements');
        TableRegistry::remove('DakAttachments');
        $tableDakMovements = TableRegistry::get('DakMovements');
        $table_instance_da = TableRegistry::get('DakAttachments');

        if ($dak_type == 'Daptorik') {
            TableRegistry::remove('DakDaptoriks');
            $table_instance_dd = TableRegistry::get('DakDaptoriks');
        } else if ($dak_type == 'Nagorik') {
            TableRegistry::remove('DakNagoriks');
            $table_instance_dd = TableRegistry::get('DakNagoriks');
        }
        $dakInformation = $table_instance_dd->get($dak_id);
        $response = $tableDakMovements->backArchiveDak($dakInformation, $dak_type, $employee_office);

        if ($response['status'] == 'success') {
            try {
                $dak_receipt_no = $this->generateDakReceivedNo($employee_office['office_id'], $dak_type);
                $DakDaptoriks = $response['dak'];
                $dak_countdp = TableRegistry::get('DakDaptoriks')->find()->where(['receiving_office_id' => $employee_office['office_id']])->order(['cast(docketing_no as signed)' => 'desc'])->first();
                $dak_countng = TableRegistry::get('DakNagoriks')->find()->where(['receiving_office_id' => $employee_office['office_id']])->order(['cast(docketing_no as signed)' => 'desc'])->first();
                $dak_count = max([(!empty($dak_countdp['docketing_no']) ? $dak_countdp['docketing_no'] : 0), (!empty($dak_countng['docketing_no']) ? $dak_countng['docketing_no'] : 0)]) + 1;

                $DakDaptoriks->docketing_no = $dak_count;
                $DakDaptoriks->dak_received_no = $dak_receipt_no;
                $DakDaptoriks->dak_status = DAK_CATEGORY_INBOX;
                $DakDaptoriks->sending_date = date("Y-m-d H:i:s");
                $DakDaptoriks->created_by = $this->Auth->user('id');
                $DakDaptoriks->modified_by = $this->Auth->user('id');

                $table_instance_dd->save($DakDaptoriks);

                $dak_attachments = $response['attachments'];
                if (!empty($dak_attachments)) {
                    foreach ($dak_attachments as $da) {
                        $new_da['file_name'] = $da['file_name'];
                        $new_da['file_dir'] = empty($da['file_dir']) ? FILE_FOLDER : $da['file_dir'];
                        $new_da['content_cover'] = $da['content_cover'];
                        $new_da['content_body'] = $da['content_body'];
                        $new_da['meta_data'] = $da['meta_data'];
                        $new_da['is_summary_nothi'] = intval($da['is_summary_nothi']);
                        $new_da['attachment_type'] = $da['attachment_type'];
                        $new_da['attachment_description'] = $da['attachment_description'];
                        $new_da['dak_id'] = $DakDaptoriks['id'];
                        $new_da['dak_type'] = $dak_type;
                        $new_da['created_by'] = $this->Auth->user('id');
                        $new_da['modified_by'] = $this->Auth->user('id');
                        $attachments[] = $new_da;
                    }
                    $attachments = $table_instance_da->newEntities($attachments);
                    foreach ($attachments as $da) {

                        $table_instance_da->save($da);
                    }
                }

                $DakUsersTable = TableRegistry::get('DakUsers');

                $drafter = $DakUsersTable->newEntity();
                $drafter->dak_type = $dak_type;
                $drafter->dak_id = $DakDaptoriks['id'];
                $drafter->to_office_id = $employee_office['office_id'];
                $drafter->to_office_name = $employee_office['office_name'];
                $drafter->to_office_unit_id = $employee_office['office_unit_id'];
                $drafter->to_office_unit_name = $employee_office['office_unit_name'];
                $drafter->to_office_address = $employee_office['office_address'];
                $drafter->to_officer_id = $employee_office['officer_id'];
                $drafter->to_officer_name = $employee_office['officer_name'];
                $drafter->to_officer_designation_id = $employee_office['office_unit_organogram_id'];
                $drafter->to_officer_designation_label = $employee_office['designation_label'];
                $drafter->dak_view_status = DAK_VIEW_STATUS_NEW;
                $drafter->dak_priority = 0;
                $drafter->attention_type = 1;
                $drafter->dak_category = DAK_CATEGORY_INBOX;
                $drafter->created_by = $this->Auth->user('id');
                $drafter->modified_by = $this->Auth->user('id');

                $DakUsers_draft = $DakUsersTable->save($drafter);

                $DakMovementsTable = TableRegistry::get('DakMovements');

                $get_last_archieve_movement_data = !empty($response['movement']) ? $response['movement'] : ['dak_actions' => ''];

                $first_move = $DakMovementsTable->newEntity();
                $first_move->dak_type = $dak_type;
                $first_move->dak_id = $DakDaptoriks->id;
                $first_move->from_office_id = $employee_office['office_id'];
                $first_move->from_office_name = $employee_office['office_name'];
                $first_move->from_office_unit_id = $employee_office['office_unit_id'];
                $first_move->from_office_unit_name = $employee_office['office_unit_name'];
                $first_move->from_office_address = $employee_office['office_address'];
                $first_move->from_officer_id = $employee_office['officer_id'];
                $first_move->from_officer_name = $employee_office['officer_name'];
                $first_move->from_officer_designation_id = $employee_office['office_unit_organogram_id'];
                $first_move->from_officer_designation_label = $employee_office['designation_label'];
                $first_move->to_office_id = $employee_office['office_id'];
                $first_move->to_office_name = $employee_office['office_name'];
                $first_move->to_office_unit_id = $employee_office['office_unit_id'];
                $first_move->to_office_unit_name = $employee_office['office_unit_name'];
                $first_move->to_office_address = $employee_office['office_address'];
                $first_move->to_officer_id = $employee_office['officer_id'];
                $first_move->to_officer_name = $employee_office['officer_name'];
                $first_move->to_officer_designation_id = $employee_office['office_unit_organogram_id'];
                $first_move->to_officer_designation_label = $employee_office['designation_label'];
                $first_move->attention_type = 1;
                $first_move->docketing_no = $DakDaptoriks['docketing_no'];
                $first_move->from_sarok_no = "";
                $first_move->to_sarok_no = "";
                $first_move->operation_type = DAK_CATEGORY_SENT;
                $first_move->sequence = 2;
                $first_move->dak_actions = $get_last_archieve_movement_data['dak_actions'] . " (ফেরত আনা হয়েছে)";
                $first_move->dak_priority = $DakDaptoriks['dak_priority_level'];
                $first_move->created_by = $this->Auth->user('id');
                $first_move->modified_by = $this->Auth->user('id');

                $DakMovementsTable->save($first_move);

                $DakUserActionsTable = TableRegistry::get('DakUserActions');
                $DakUserAction = $DakUserActionsTable->newEntity();
                $DakUserAction->dak_user_id = $DakUsers_draft['id'];
                $DakUserAction->dak_id = $DakDaptoriks['id'];
                $DakUserAction->dak_action = "Forward";
                $DakUserActionsTable->save($DakUserAction);

                /*                     * Dak Tracks in Projapoti* */
                $data['sender_office_id'] = $employee_office['office_id'];
                $data['sender_officer_designation_id'] = $employee_office['office_unit_organogram_id'];
                $data['sender_name'] = $employee_office['officer_name'];
                $data['dak_received_no'] = $dak_receipt_no;
                $data['docketing_no'] = $dak_count;
                $data['receiving_date'] = $DakDaptoriks->sending_date;
                $data['receiving_office_id'] = $employee_office['office_id'];
                $data['receiving_officer_designation_label'] = $employee_office['designation_label'];
                $data['receiving_officer_name'] = $employee_office['officer_name'];
                $data['dak_type'] = $dak_type;
                $data['dak_id'] = $DakDaptoriks->id;
                $this->addDakTracks($data);
                $this->NotificationSet('inbox', array(1, $dak_type . " Dak"), 1, $employee_office, $employee_office, $DakDaptoriks['dak_subject']);
                /*                     * Dak Tracks in Projapoti* */
            } catch (\Exception $ex) {
                $response = ['status' => 'error', 'msg' => ($ex->getMessage())];
            }


            echo json_encode($response);
            die;
        } else {
            echo json_encode($response);
            die;
        }
    }

    public function generateDakReceivedNo($office_id, $dak_type = 'Daptorik')
    {

        if ($dak_type == 'Daptorik') {
            TableRegistry::remove('DakDaptoriks');
            $table_instance_dd = TableRegistry::get('DakDaptoriks');
            $daktype = 2;
        } else if ($dak_type == 'Nagorik') {
            TableRegistry::remove('DakNagoriks');
            $table_instance_dd = TableRegistry::get('DakNagoriks');
            $daktype = 1;
        }

        $dak_count = $table_instance_dd->find()->where(['receiving_office_id' => $office_id, 'date(created)' => date('Y-m-d')])->count();

        startagain:
        $dak_count++;
        $year = date('y');
        $month = date('m');
        $day = date('d');
        $offline = 1;
        $received_no = $daktype . $offline . str_pad($office_id, 5, "0", STR_PAD_LEFT) . $year . $month . $day . str_pad($dak_count, 3, "0", STR_PAD_LEFT);

        $exists = $table_instance_dd->find()->where(['dak_received_no' => $received_no])->count();
        if ($exists) {
            goto startagain;
        }
        return $received_no;
    }

    private function addDakTracks($data)
    {
        $employee_office = $this->getCurrentDakSection();
        $table = TableRegistry::get('DakTracks');
        $dt = $table->newEntity();
        if (!empty($data['service_name'])) $dt->service_name = !empty($data['service_name']) ? strtolower($data['service_name']) : 'nothi';
        if (!empty($data['sarok_no'])) $dt->sarok_no = $data['sarok_no'];
        if (!empty($data['sender_office_id']))
            $dt->sender_office_id = $data['sender_office_id'];
        if (!empty($data['sender_officer_designation_id']))
            $dt->sender_officer_designation_id = $data['sender_officer_designation_id'];
        if (!empty($data['sender_name']))
            $dt->sender_name = $data['sender_name'];
        if (!empty($data['email'])) $dt->sender_email = $data['email'];
        if (!empty($data['mobile_no'])) $dt->sender_mobile = $data['mobile_no'];
        if (!empty($data['dak_received_no']))
            $dt->dak_received_no = $data['dak_received_no'];
        if (!empty($data['docketing_no']))
            $dt->docketing_no = $data['docketing_no'];
        if (!empty($data['receive_date']))
            $dt->receiving_date = $data['receive_date'];
        if (!empty($data['dak_subject']))
            $dt->dak_subject = $data['dak_subject'];

        $dt->receiving_office_id = $employee_office['office_id'];

        if (!empty($data['receiving_officer_designation_label']))
            $dt->receiving_officer_designation_label = !empty($data['receiving_officer_designation_label']) ? $data['receiving_officer_designation_label'] : '';
        if (!empty($data['receiving_officer_name']))
            $dt->receiving_officer_name = !empty($data['receiving_officer_name']) ? $data['receiving_officer_name'] : '';
        if (!empty($data['dak_id'])) $dt->dak_id = $data['dak_id'];
        if (!empty($data['dak_type'])) $dt->dak_type = $data['dak_type'];
        $table->save($dt);
    }

    public function postApiSealSave()
    {
        $user_designation = $this->request->data['user_designation'];
        $add_designations_in_seal = $this->request->data['add_designations_in_seal'];

        $jsonArray = array(
            "status" => "error",
            "error_code" => 404,
            "message" => "Invalid Request"
        );

        if (!defined("PROJAPOTI_API") || PROJAPOTI_API == false) {
            goto rtn;
        }

        if (empty($user_designation)) {
            $jsonArray['message'] = 'Seal owner designation is not given';
            goto rtn;
        }
        if (empty($add_designations_in_seal)) {
            $jsonArray['message'] = 'No designation found to add in seal';
            goto rtn;
        }

        $apikey = isset($this->request->data['api_key']) ? $this->request->data['api_key'] : '';

        if (empty($apikey) || $this->checkToken($apikey) == FALSE) {
            goto rtn;
        }
        try {
            if ($apikey != API_KEY) {
                //verify api token
                $this->loadComponent('ApiRelated');
                $getApiTokenData = $this->ApiRelated->getMobileApiTokenData($apikey);
                if (!empty($getApiTokenData)) {
                    if (!in_array($user_designation, $getApiTokenData['designations'])) {
                        $jsonArray['message'] = 'Unauthorized request';
                        goto rtn;
                    }
                }
                //verify api token
            }

            $EmployeeOfficesTable = TableRegistry::get('EmployeeOffices');
            $employee_office = $this->setCurrentDakSection($user_designation);
            $this->switchOffice($employee_office['office_id'], 'ReceiverOffice');

            TableRegistry::remove('OfficeDesignationSeals');
            $table_instance_designation_seal = TableRegistry::get('OfficeDesignationSeals');
            $designations_info = [];
            if (!empty($add_designations_in_seal)) {
                $all_designations_to_add = explode(',', $add_designations_in_seal);
                if (!empty($all_designations_to_add)) {
                    $designation_that_already_exist = $table_instance_designation_seal->getEntry($employee_office['office_unit_organogram_id'], $employee_office['office_id'], 0, $all_designations_to_add, 1)->toArray();
                    if (!empty($designation_that_already_exist)) {
                        foreach ($designation_that_already_exist as $designation_id) {
                            $key_to_delete = array_search($designation_id, $all_designations_to_add);
                            if (isset($key_to_delete)) {
                                unset($all_designations_to_add[$key_to_delete]);
                            }
                        }
                    }
                }
                // as unset occurs, all index can be unset. need to check again.
                if (!empty($all_designations_to_add)) {
                    $designations_info = $EmployeeOfficesTable->getEmployeeOfficeRecordsByOfficeId($employee_office['office_id'], 0, $all_designations_to_add, 1, [
                        'EmployeeOffices.employee_record_id',
                        'EmployeeOffices.office_id',
                        'EmployeeOffices.office_unit_id',
                        'EmployeeOffices.office_unit_organogram_id',
                        'designation_level' => 'EmployeeOffices.designation_level',
                        'designation_seq' => 'EmployeeOffices.designation_sequence',
                        'unit_name_bng' => 'OfficeUnits.unit_name_bng',
                        'unit_name_eng' => 'OfficeUnits.unit_name_eng',
                        'designation_name_bng' => 'OfficeUnitOrganograms.designation_bng',
                        'designation_name_eng' => 'OfficeUnitOrganograms.designation_eng',
                    ]);
                }
            }
            $user_id = $employee_office['user_id'];
            if (!empty($designations_info)) {
                foreach ($designations_info as $key => $desingation) {
                    if (!empty($desingation['office_unit_organogram_id'])) {
                        $desingation['seal_owner_designation_id'] = $employee_office['office_unit_organogram_id'];
                        $desingation['seal_owner_unit_id'] = $employee_office['office_unit_id'];
                        $desingation['created_by'] = $user_id;
                        $desingation['modified_by'] = $user_id;
                        // if eng column empty then bangla name will be set
                        if (empty($desingation['designation_name_eng'])) {
                            $desingation['designation_name_eng'] = $desingation['designation_name_bng'];
                        }
                        if (empty($desingation['unit_name_eng'])) {
                            $desingation['unit_name_eng'] = $desingation['unit_name_bng'];
                        }

                        $entity_instance_designation_seal = $table_instance_designation_seal->patchEntity($table_instance_designation_seal->newEntity(), $desingation->toArray());
                        $errors = $entity_instance_designation_seal->errors();
                        if (empty($errors)) {
                            $table_instance_designation_seal->save($entity_instance_designation_seal);
                        } else {
                            $jsonArray = array(
                                "status" => "error",
                                "message" => __('Technical error happen'),
                                'reason' => $errors,
                            );
                            goto rtn;
                        }
                    }
                }
            }
            $jsonArray = array(
                "status" => "success",
                "message" => __('Data successfully added'),
            );
        } catch (\Exception $ex) {
            $jsonArray = array(
                "status" => "error",
                "message" => __('Technical error happen'),
                'reason' => $ex->getMessage(),
            );
        }

        rtn:
        $this->response->body(json_encode($jsonArray));
        $this->response->type('application/json');
        return $this->response;
    }

    public function postApiSealDelete()
    {
        $user_designation = $this->request->data['user_designation'];
        $delete_designations_from_seal = $this->request->data['delete_designations_from_seal'];

        $jsonArray = array(
            "status" => "error",
            "error_code" => 404,
            "message" => "Invalid Request"
        );

        if (!defined("PROJAPOTI_API") || PROJAPOTI_API == false) {
            goto rtn;
        }

        if (empty($user_designation)) {
            $jsonArray['message'] = 'Seal owner designation is not given';
            goto rtn;
        }
        if (empty($delete_designations_from_seal)) {
            $jsonArray['message'] = 'No designation found to delete from seal';
            goto rtn;
        }

        $apikey = isset($this->request->data['api_key']) ? $this->request->data['api_key'] : '';

        if (empty($apikey) || $this->checkToken($apikey) == FALSE) {
            goto rtn;
        }
        try {
            if ($apikey != API_KEY) {
                //verify api token
                $this->loadComponent('ApiRelated');
                $getApiTokenData = $this->ApiRelated->getMobileApiTokenData($apikey);
                if (!empty($getApiTokenData)) {
                    if (!in_array($user_designation, $getApiTokenData['designations'])) {
                        $jsonArray['message'] = 'Unauthorized request';
                        goto rtn;
                    }
                }
                //verify api token
            }

            $EmployeeOfficesTable = TableRegistry::get('EmployeeOffices');
            $employee_office = $EmployeeOfficesTable->getDesignationInfo($user_designation);
            $this->switchOffice($employee_office['office_id'], 'ReceiverOffice');

            TableRegistry::remove('OfficeDesignationSeals');
            $table_instance_designation_seal = TableRegistry::get('OfficeDesignationSeals');
            if (!empty($delete_designations_from_seal)) {
                $all_designations_to_delete = explode(',', $delete_designations_from_seal);
                if (!empty($all_designations_to_delete)) {
                    $table_instance_designation_seal->deleteAll(['office_unit_organogram_id IN' => $all_designations_to_delete], ['seal_owner_designation_id' => $employee_office['office_unit_organogram_id'], 'seal_owner_unit_id' => $employee_office['office_unit_id']]);
                }
            }

            $jsonArray = array(
                "status" => "success",
                "message" => __('Data successfully deleted'),
            );
        } catch (\Exception $ex) {
            $jsonArray = array(
                "status" => "error",
                "message" => __('Technical error happen'),
                'reason' => $ex->getMessage(),
            );
        }

        rtn:
        $this->response->body(json_encode($jsonArray));
        $this->response->type('application/json');
        return $this->response;
    }

    public function permitted_dak() {
		$current_user = $this->getCurrentDakSection();
		$other_organogram_activities_settings_table = TableRegistry::get('OtherOrganogramActivitiesSettings');
		$other_organogram_activities_settings = $other_organogram_activities_settings_table->find()->where([
			'assigned_organogram_id' => $current_user['office_unit_organogram_id'],
			'status' => 1,
			'permission_for IN' => [0, 1]
		]);
		$other_organogram_activities_setting = [];
		if (isset($this->request->data()['organogram_id']) && $this->request->data('organogram_id') > 0) {
			$other_organogram_activities_setting = $other_organogram_activities_settings_table->find()->where([
				'assigned_organogram_id' => $current_user['office_unit_organogram_id'],
				'status' => 1,
				'permission_for IN' => [0, 1],
				'organogram_id' => $this->request->data('organogram_id')
			])->toArray();
		}

		$other_organogram_activities_settings = $other_organogram_activities_settings->toArray();
		$this->set(compact('current_user', 'other_organogram_activities_settings', 'other_organogram_activities_setting'));

	}
	public function permitted_dak_view($organogram_id = 0, $type = 'inbox') {
		$current_user = $this->getCurrentDakSection();
		$other_organogram_activities_settings_table = TableRegistry::get('OtherOrganogramActivitiesSettings');
		$other_organogram_activities_settings = $other_organogram_activities_settings_table->find()->where([
			'assigned_organogram_id' => $current_user['office_unit_organogram_id'],
			'status' => 1,
			'permission_for IN' => [0, 1]
		]);
		if ($organogram_id > 0) {
			$other_organogram_activities_settings = $other_organogram_activities_settings->where(['organogram_id' => $organogram_id]);
		}

		$other_organogram_activities_settings = $other_organogram_activities_settings->toArray();

		$other_organogram_activities_setting = $other_organogram_activities_settings[0];


		$table_instance_dm = TableRegistry::get('DakMovements');
		$employee_office = designationInfo($other_organogram_activities_setting['organogram_id']);

		$response = $table_instance_dm->getDakList($type, $employee_office, $this, 0, 0, true);

		$json_data = array(
			"draw" => isset($this->request->data['draw']) ? intval($this->request->data['draw']) : 1,
			"recordsTotal" => $response['total'],
			"recordsFiltered" => $response['total'],
			"data" => $response['data']
		);
		echo json_encode($json_data);
		die;
	}

    public function postDakView(){
        $domain = $this->getMainDomainUrl();
        $this->layout = null;
        //$this->view = 'api_view_dak_daptorik';

        $designationId = $this->request->data['user_designation'];
        $id = isset($this->request->data['dak_id']) ? $this->request->data['dak_id'] : '';
        $type = isset($this->request->data['dak_type']) ? $this->request->data['dak_type'] : 'Daptorik';
        // For cheking if request comes from old android version, we set a message on view.
        $is_new = 1;

        if (!defined("PROJAPOTI_API") || PROJAPOTI_API == false) {
            $this->response->body(json_encode(['status' => 'error', 'msg' => 'Invalid Request : (Parameter missing)']));
            $this->response->type('application/json');
            return $this->response;
        }

        if (empty($designationId)) {
            $this->response->body(json_encode(['status' => 'error', 'msg' => 'Invalid Request : (Invalid designation)']));
            $this->response->type('application/json');
            return $this->response;
        }

        if (empty($id)) {
            $this->response->body(json_encode(['status' => 'error', 'msg' => 'Invalid Request : (Dak ID missing)']));
            $this->response->type('application/json');
            return $this->response;
        }

        $apikey = isset($this->request->data['api_key']) ? $this->request->data['api_key'] : '';

        if (empty($apikey) || $this->checkToken($apikey) == FALSE) {
            $this->response->body(json_encode(['status' => 'error', 'msg' => 'Invalid Request : (app_key INVALID)']));
            $this->response->type('application/json');
            return $this->response;
        }
        if ($apikey != API_KEY) {
            //verify api token
            $this->loadComponent('ApiRelated');
            $getApiTokenData = $this->ApiRelated->getMobileApiTokenData($apikey);
            if (!empty($getApiTokenData)) {
                if (!in_array($designationId, $getApiTokenData['designations'])) {
                    $this->response->body(json_encode(['status' => 'error', 'msg' => 'Unauthorized request']));
                    $this->response->type('application/json');
                    return $this->response;
                }
            }
            //verify api token
        }
        $EmployeeOfficesTable = TableRegistry::get('EmployeeOffices');
        $employee_office = $EmployeeOfficesTable->find()->where(['office_unit_organogram_id' => $designationId,
            'status' => 1])->first();

        $this->switchOffice($employee_office['office_id'], 'Receiveroffice');


        if ($type == 'Daptorik') {
            $table_instance_dd = TableRegistry::get('DakDaptoriks');
        } else {
            $table_instance_dd = TableRegistry::get('DakNagoriks');
        }

        $table_instance_dak_user = TableRegistry::get('DakUsers');

        if (!empty($id)) {
            $dak_entity = $table_instance_dd->get($id);
        } else {
            $this->response->body(json_encode(['status' => 'error', 'msg' => 'Invalid Request : (Dak ID missing)']));
            $this->response->type('application/json');
            return $this->response;
        }

        $dakUsers = $table_instance_dak_user->find()->where(['dak_id' => $dak_entity['id'],
            'to_officer_designation_id' => $employee_office['office_unit_organogram_id'],
            'dak_type' => $type])->toArray();
        if (!TableRegistry::get('DakMovements')->hasAccessInDak($id, $type, $employee_office['office_unit_organogram_id'])) {
            if (empty($dakUsers)) {
                $this->response->body(json_encode(['status' => 'error', 'msg' => 'Invalid Request : (Dak user not found)']));
                $this->response->type('application/json');
                return $this->response;
            }
        }
        foreach ($dakUsers as $dakUser) {
            $dakUser->dak_view_status = DAK_VIEW_STATUS_VIEW;
            $table_instance_dak_user->save($dakUser);
            if (isset($dakUser['attention_type'])) {
                $this->set('attention_type', $dakUser['attention_type']);
            }
        }

        $table_instance_da = TableRegistry::get('DakAttachments');

        /* Show Sender Information */
        $table_instance_dm = TableRegistry::get('DakMovements');

        $dak_priority_type_list = json_decode(DAK_PRIORITY_TYPE, true);
        $dak_security_level_list = json_decode(DAK_SECRECY_TYPE, true);

        $dak_priority = 1;

        $sender = $dak_entity['sender_officer_designation_label'] . (!empty($dak_entity['sender_office_name'])
                ? (',' . $dak_entity['sender_office_name']) : '');
        $dak_last_move = $table_instance_dm->getInboxDakLastMoveBy_dakid_attention_type($dak_entity['id'],
            $type);

        $move_date = $dak_entity['modified'];
        $receiver = '';

        $all_move_sequence = $table_instance_dm->getSequenceNumber($dak_entity['id'], $type);

        $i = 0;

        $move_seq = !empty($all_move_sequence) ? $all_move_sequence[0] : '';
        if (!empty($move_seq)) {
            $dak_moves_by_sequence = $table_instance_dm->getDakMovesBy_sequence_dakId($move_seq['sequence'],
                $dak_entity['id'], $type);
        }

        $dak_priority = 0;
        $move_date = $dak_entity['created'];
        $row_move = isset($dak_moves_by_sequence[0]) ? $dak_moves_by_sequence[0] : array();

        if (!empty($row_move)) {

            $sender = (!empty($row_move['from_officer_name']) ? ($row_move['from_officer_name'] . ', ')
                    : '') . $row_move['from_officer_designation_label'];

            if ($row_move['from_office_id'] != $employee_office['office_id']) {
                $sender .= ', ' . $row_move['from_office_name'];
            }
            $receiver = (!empty($row_move['to_officer_name']) ? ($row_move['to_officer_name'] . ", ")
                    : '') . $row_move['to_officer_designation_label'];
            $move_date = $row_move['modified'];
            $dak_priority = $row_move['dak_priority'] == 0 ? 1 : $row_move['dak_priority'];
        }

        $EmployeeRecordTable = TableRegistry::get('EmployeeRecords');
        $selected_office_section = $EmployeeRecordTable->get($employee_office['employee_record_id']);
        $dak_attachments = $table_instance_da->loadAllAttachmentByDakId($dak_entity['id'], $type);

        $dak_origin = (!empty($dak_entity['sender_name']) ? ($dak_entity['sender_name']) : '').(!empty($dak_entity['sender_officer_designation_label']) ? ((!empty($dak_entity['sender_name']) ? ', ' : '').$dak_entity['sender_officer_designation_label'].", ") : ' ').$dak_entity['sender_office_name'];
        $dak_action = !empty($row_move['dak_actions']) ? $row_move['dak_actions'] : '';
        $attachmentGroup      = array();
        $attachmentGroupimage = array();
        if (isset($dak_attachments) && count($dak_attachments) > 0) {
            $i = 1;
            foreach ($dak_attachments as $single_data) {
                $file_name                      = $single_data['file_name'];
                $file_name                      = explode("/", $file_name);
                $file_name                      = $file_name[count($file_name) - 1];
                if ($single_data['attachment_type'] == 'text') continue;
                $single_data['attachment_type'] = get_file_type($single_data['file_name']);
                $f_type                         = explode('/',
                    $single_data['attachment_type']);
                $file_name_arr[0]               = urldecode($file_name);
                $file_name_arr[1]               = $f_type[1];

                $single_data['file_name'] = $single_data['file_name'];

                if (isset($file_name_arr[1])) {
                    if (!empty($single_data['file_name'])) {
                        if (strtolower($file_name_arr[1]) == 'jpg' || strtolower($file_name_arr[1]) == 'jpeg' || strtolower($file_name_arr[1]) == 'png') {
                            $attachmentGroupimage['image'][] = array("id" => $single_data['id'],
                                "file_type" => $single_data['attachment_type'], "file_name" => $single_data['file_name'],
                                "name" => $file_name_arr[0], 'id' => $single_data['id'], 'link' => ($domain . 'getContent?file=' . $single_data['file_name'] . '&token=' . sGenerateToken(['file' => $single_data['file_name']], ['exp' => time() + 60 * 300])));
                        } elseif (strtolower($file_name_arr[1]) == 'mp3' || strtolower($file_name_arr[1]) == 'mpeg') {
                            $attachmentGroup['mp3'][] = array("file_type" => $single_data['attachment_type'],
                                "file_name" => $single_data['file_name'], "name" => $file_name_arr[0],
                                'id' => $single_data['id'], 'link' => ($domain . 'getContent?file=' . $single_data['file_name'] . '&token=' . sGenerateToken(['file' => $single_data['file_name']], ['exp' => time() + 60 * 300])));
                        } elseif (strtolower($file_name_arr[1]) == 'wmv' || strtolower($file_name_arr[1]) == 'mp4' || strtolower($file_name_arr[1]) == 'mkv' || strtolower($file_name_arr[1]) == 'avi' || strtolower($file_name_arr[1]) == '3gp' || strtolower($file_name_arr[1]) == 'mpg') {
                            $attachmentGroup['video'][] = array("file_type" => $single_data['attachment_type'],
                                "file_name" => $single_data['file_name'], "name" => $file_name_arr[0],
                                'id' => $single_data['id'], 'link' => ($domain . 'getContent?file=' . $single_data['file_name'] . '&token=' . sGenerateToken(['file' => $single_data['file_name']], ['exp' => time() + 60 * 300])));
                        } elseif (strtolower($file_name_arr[1]) == 'pdf') {
                            $attachmentGroup['pdf'][] = array("file_type" => $single_data['attachment_type'],
                                "file_name" => $single_data['file_name'], "name" => $file_name_arr[0],
                                'id' => $single_data['id'], 'link' => ($domain . 'getContent?file=' . $single_data['file_name'] . '&token=' . sGenerateToken(['file' => $single_data['file_name']], ['exp' => time() + 60 * 300])));
                        } else {
                            $attachmentGroup["other"][] = array("file_type" => $single_data['attachment_type'],
                                "file_name" => $single_data['file_name'], "name" => $file_name_arr[0],
                                'id' => $single_data['id'], 'link' => ($domain . 'getContent?file=' . $single_data['file_name'] . '&token=' . sGenerateToken(['file' => $single_data['file_name']], ['exp' => time() + 60 * 300])));
                        }
                    }
                } else {
                    if (!empty($single_data['file_name']))
                        $attachmentGroup["other"][] = array("file_type" => $single_data['attachment_type'],
                            "file_name" => $single_data['file_name'], 'id' => $single_data['id'], 'link' => ($domain . 'getContent?file=' . $single_data['file_name'] . '&token=' . sGenerateToken(['file' => $single_data['file_name']], ['exp' => time() + 60 * 300])));
                }
            }
        }
        $attachmentGroup = $attachmentGroupimage + $attachmentGroup;

        $return_data['status'] = 'success';
        $return_data['data'] = [
            'styles' => [
                $domain.'assets/global/plugins/bootstrap/css/bootstrap.min.css'
            ],
            'dak_subject' => $dak_entity['dak_subject'],
            'docketing_no' => $dak_entity['docketing_no'],
            'dak_received_no' => $dak_entity['dak_received_no'],
            'sender_sarok_no' => $dak_entity['sender_sarok_no'],
            'sending_date' => $dak_entity['sending_date']->format('Y-m-d H:i:s'),
            'dak_attachments_count' => count($dak_attachments),
            'dak_attachments' => $attachmentGroup,
            'movement_date' => $move_date->format('Y-m-d H:i:s'),
            'dak_origin' => $dak_origin,
            'dak_sender' => $sender,
            'dak_action' => strip_tags($dak_action),
            'dak_cover' => $dak_entity['dak_cover'],
            'dak_description' => $dak_entity['dak_description'],

            'dak_sending_media' => $dak_entity['dak_sending_media'],
            'receiving_date' => $dak_entity['receiving_date']->format('Y-m-d H:i:s'),
            'meta_data' => $dak_entity['meta_data'],
            'dak_status' => $dak_entity['dak_status'],
            'is_summary_nothi' => $dak_entity['is_summary_nothi'],

            'is_new' => $is_new,
            'dak_type' => $type,
            'dak_priority_level' => $dak_entity['dak_priority_level'],
            'dak_priority_txt' => isset($dak_priority_type_list[$dak_priority]) ? $dak_priority_type_list[$dak_priority] : '',
            'dak_security_level' => $dak_entity['dak_security_level'],
            'dak_security_txt' => isset($dak_security_level_list[$dak_entity['dak_security_level']]) ? $dak_security_level_list[$dak_entity['dak_security_level']] : '',


        ];

        $this->response->body(json_encode($return_data));
        $this->response->type('application/json');
        return $this->response;
    }
}

