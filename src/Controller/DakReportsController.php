<?php

namespace App\Controller;

use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

class DakReportsController extends ProjapotiController {

    public function initialize() {
        parent::initialize();
        $this->loadComponent('Paginator');
    }

    public function pendingDakListExcel($type='pdf',$startDate = "", $endDate = "",$dateRangeBn='',$unit_id = '')
    {
        $result = $this->pendingDakListContent($startDate, $endDate, $type, 20, $unit_id);
        $unitInformation='';
        $DakList = Array();
        $j=0;

        if (!empty($result['data'])) {

            foreach ($result['data'] as $dak) {
                if( $unitInformation ==  $dak['to_office_unit_name']){
                    $DakList[$j]['to_office_unit_name'] ='';
                }
                else {
                    $DakList[$j]['to_office_unit_name'] = $dak['to_office_unit_name'];
                    $unitInformation = $dak['to_office_unit_name'];
                }

                $created = new Time($dak['dakcreted']);
                $dak['dakcreted'] = $created->i18nFormat(null, null, 'bn-BD');

                $dak['movecreated'] = $modified = new Time($dak['movecreated']);
                $dak['movecreated'] = $modified->i18nFormat(null, null, 'bn-BD');


                $pending = new Time($dak['pending_time']);

                $dak['pending_time'] = $pending->i18nFormat(null, null, 'bn-BD');

                $pending = ProjapotiController::dateDiff($created->i18nFormat(null, null, 'En'), $pending->i18nFormat(null, null, 'En'));


                $DakList[$j]['dak_received_no'] = enTobn($dak['dak_received_no']);
                $DakList[$j]['docketing_no'] = enTobn($dak['docketing_no']);
                $DakList[$j]['sender_sarok_no'] = h($dak['sender_sarok_no']);
                $DakList[$j]['dakcreted'] = $dak['dakcreted'];
                $DakList[$j]['dak_subject'] = h($dak['dak_subject']);
                $DakList[$j]['sender_name'] = h($dak['sender_name']) . ', ' . ($dak['sender_officer_designation_label']) . __(!empty($dak['sender_office_unit_name']) ? (", " . $dak['sender_office_unit_name']) : '') . __(!empty($dak['sender_office_name']) ? (", " . $dak['sender_office_name']) : '');
                $DakList[$j]['receiving_officer_name'] = h($dak['receiving_officer_name'] . ', ' . $dak['receiving_officer_designation_label']) . __(!empty($dak['receiving_office_unit_name']) ? (", " . $dak['receiving_office_unit_name']) : '');
                $DakList[$j]['movecreated'] = h($dak['movecreated']);
                $DakList[$j]['dak_security_level'] = h(isset($dak['dak_security_level']) ? $dak['dak_security_level'] : '');
                $DakList[$j]['dak_priority'] = h(isset($dak['dak_priority']) ? $dak['dak_priority'] : '');
                $DakList[$j]['last_status_officer_name'] = h($dak['last_status_officer_name'] . ', ' . $dak['last_status_officer_designation_label']) . __(!empty($dak['last_status_office_unit_name']) ? (", " . $dak['last_status_office_unit_name']) : '');
                $DakList[$j]['pending'] = h($pending);
                    $j++;

            }
        }
        if(empty($DakList)){
            $DakList= Array();
        }
        if($type == 'excel'){
            $this->printExcel($DakList, [
                ['key' => 'to_office_unit_name', 'title' => 'শাখা'],
                ['key' => 'dak_received_no', 'title' => 'গ্রহণ নম্বর'],
                ['key' => 'docketing_no', 'title' => 'ডকেট নং'],
                ['key' => 'sender_sarok_no', 'title' => 'স্মারক নম্বর'],
                ['key' => 'dakcreted', 'title' => 'আবেদনের তারিখ'],
                ['key' => 'dak_subject', 'title' => 'বিষয়'],
                ['key' => 'sender_name', 'title' => 'আবেদনকারী'],
                ['key' => 'receiving_officer_name', 'title' => 'মূল প্রাপক'],
                ['key' => 'movecreated', 'title' => 'প্রেরণের তারিখ'],
                ['key' => 'dak_security_level', 'title' => 'গোপনীয়তা'],
                ['key' => 'dak_priority', 'title' => 'অগ্রাধিকার'],
                ['key' => 'last_status_officer_name', 'title' => 'সর্বশেষ অবস্থা'],
                ['key' => 'pending', 'title' => 'পেন্ডিং সময়'],
            ],[
                'name'=>((isset($result['office_name']))?$result['office_name'].' অফিসের ':'') .'অমীমাংসিত ডাকসমূহ' ,
                'title' => [
                    ((isset($result['office_name']))?$result['office_name'].' অফিসের ':'') .'অমীমাংসিত ডাকসমূহ' ,
                    $dateRangeBn,
                ]
            ]);
        } else {
            $this->printPdf($DakList, [
                ['key' => 'to_office_unit_name', 'title' => 'শাখা'],
                ['key' => 'dak_received_no', 'title' => 'গ্রহণ নম্বর'],
                ['key' => 'docketing_no', 'title' => 'ডকেট নং'],
                ['key' => 'sender_sarok_no', 'title' => 'স্মারক নম্বর'],
                ['key' => 'dakcreted', 'title' => 'আবেদনের তারিখ'],
                ['key' => 'dak_subject', 'title' => 'বিষয়'],
                ['key' => 'sender_name', 'title' => 'আবেদনকারী'],
                ['key' => 'receiving_officer_name', 'title' => 'মূল প্রাপক'],
                ['key' => 'movecreated', 'title' => 'প্রেরণের তারিখ'],
                ['key' => 'dak_security_level', 'title' => 'গোপনীয়তা'],
                ['key' => 'dak_priority', 'title' => 'অগ্রাধিকার'],
                ['key' => 'last_status_officer_name', 'title' => 'সর্বশেষ অবস্থা'],
                ['key' => 'pending', 'title' => 'পেন্ডিং সময়'],
            ],[
                'name'=>((isset($result['office_name']))?$result['office_name'].' অফিসের ':'') .'অমীমাংসিত ডাকসমূহ' ,
                'title' => [
                    ((isset($result['office_name']))?$result['office_name'].' অফিসের ':'') .'অমীমাংসিত ডাকসমূহ' ,
                    $dateRangeBn,
                ]
            ]);
        }

            exit;
    }

    public function pendingDakList() {
        $session = $this->request->session();
        $employee = $session->read('logged_employee_record');
        $this->set("office", $employee);

        $units_list = $this->getOwnUnitList();
        $this->set("units_list", $units_list);
    }

    protected function getOwnUnitList(){
        $employee_office = $this->getCurrentDakSection();

        $officeUnitTable = TableRegistry::get("OfficeUnits");

        $units = $officeUnitTable->getOfficeChildInfo($employee_office['office_unit_id']);
        $unitsOwn = $officeUnitTable->getOwnChildInfo($employee_office['office_unit_id']);

        $units = !empty($units[0]) ? $units[0] : '';
        $unitsOwn = !empty($unitsOwn[0]) ? ($employee_office['office_unit_id'] . ',' . $unitsOwn[0]) : $employee_office['office_unit_id'];

        $units = $units . (!empty($unitsOwn) ? ((!empty($units) ? ',' : '') . $unitsOwn) : '');
        $unit_id_array = explode(",",$units);
        $units_list = $officeUnitTable->find('list',['keyField'=>'id','valueField'=>'unit_name_bng'])->where(['id IN'=>$unit_id_array])->toArray();
        if(count($unit_id_array) > 1){
            $units_list = [''=>'সকল শাখা']+$units_list;
        }

        return $units_list;
    }

    public function pendingDakListContent($startDate = "", $endDate = "", $isExport="",$per_page = 20 ,$unit_id = "")
    {

        $this->layout = null;
        $employee_office = $this->getCurrentDakSection();
        $employee_section_id = $employee_office['office_unit_id'];
        $table_instance_dak_movements = TableRegistry::get("DakMovements");
        $table_instance_dak_daptorik = TableRegistry::get("DakDaptoriks");
        $table_instance_dak_users = TableRegistry::get("DakUsers");

        $officeUnitTable = TableRegistry::get("OfficeUnits");

        $units = $officeUnitTable->getOfficeChildInfo($employee_office['office_unit_id']);
        $unitsOwn = $officeUnitTable->getOwnChildInfo($employee_office['office_unit_id']);

        $units = !empty($units[0]) ? $units[0] : '';
        $unitsOwn = !empty($unitsOwn[0]) ? ($employee_office['office_unit_id'] . ',' . $unitsOwn[0]) : $employee_office['office_unit_id'];

        $units = $units . (!empty($unitsOwn) ? ((!empty($units) ? ',' : '') . $unitsOwn) : '');
        $unit_id_array = explode(",",$units);
        if(!empty($unit_id) && in_array((int)$unit_id,$unit_id_array)){
                $units = (int)$unit_id;
        }
 
        $startDate = !empty($startDate)?h($startDate):date('Y-m-d');
        $endDate = !empty($endDate)?h($endDate):date('Y-m-d');

        if($isExport=='excel' || $isExport=='pdf'){
            $user_last_movements = $table_instance_dak_movements->getLastofEachGroupOfDak($startDate, $endDate, $units);
        } else{
            $user_last_movements = $this->Paginator->paginate($table_instance_dak_movements->getLastofEachGroupOfDak($startDate, $endDate, $units), ['limit' => $per_page]);
        }
        $dp_ids = [];
        $user_dak_list = [];
        foreach ($user_last_movements->toArray() as $user_dak) {
            $dp_ids[] = $user_dak['dak_id'];
//            if($user_dak['dak_type']==DAK_NAGORIK)$dn_ids[] = $user_dak['dak_id'];
            $user_dak_list[$user_dak['dak_id']] = $user_dak;
        }

        if(!empty($dp_ids)){
        $daptorik_daks_info_list = $table_instance_dak_daptorik->find()->select([
            'id',
            'docketing_no',
            'dak_received_no',
            'sender_sarok_no',
            'dak_subject',
            'receiving_officer_name',
            'receiving_officer_designation_label',
            'receiving_office_unit_name',
            'sender_name',
            'sender_officer_designation_label',
            'sender_office_unit_name',
            'sender_office_name',
            'receiving_date',
            'dak_security_level',
            'dak_priority_level'
        ])->where(['id IN' => $dp_ids])->toArray();

        $allDakPendingTimes = $table_instance_dak_users->find()->select([
            'dak_id',
            'dak_category',
            'modified'
        ])->where(['dak_id IN' => $dp_ids])
            ->group('dak_id')
            ->toArray();
        
        foreach ($allDakPendingTimes as $DakPendingTimes) {
            if ($DakPendingTimes['dak_category'] == DAK_CATEGORY_NOTHIVUKTO || $DakPendingTimes['dak_category'] == DAK_CATEGORY_NOTHIJATO) {
                $DakPendingTimes['lastPendingTime'] = $DakPendingTimes['modified'];
            } else {
                $DakPendingTimes['lastPendingTime'] = date('Y-m-d H:i:s');
            }
            $dakPendingTime[$DakPendingTimes['dak_id']] = $DakPendingTimes;
        }

        foreach ($daptorik_daks_info_list as $daptorik_daks_info_list_with_dak_users_data) {
            $daptorik_daks_info_list_with_dak_users_data['to_office_unit_id'] = $user_dak_list[$daptorik_daks_info_list_with_dak_users_data['id']]['to_office_unit_id'];
            $daptorik_daks_info_list_with_dak_users_data['modified'] = $user_dak_list[$daptorik_daks_info_list_with_dak_users_data['id']]['modified'];
            $daptorik_daks_info_list_with_dak_users_data['to_office_unit_name'] = $user_dak_list[$daptorik_daks_info_list_with_dak_users_data['id']]['to_office_unit_name'];
            $daptorik_daks_info_list_with_dak_users_data['dak_category'] = $user_dak_list[$daptorik_daks_info_list_with_dak_users_data['id']]['operation_type'];
            $daptorik_daks_info_list_with_dak_users_data['last_status_officer_name'] = $user_dak_list[$daptorik_daks_info_list_with_dak_users_data['id']]['to_officer_name'];
            $daptorik_daks_info_list_with_dak_users_data['last_status_officer_designation_label'] = $user_dak_list[$daptorik_daks_info_list_with_dak_users_data['id']]['to_officer_designation_label'];
            $daptorik_daks_info_list_with_dak_users_data['last_status_office_unit_name'] = $user_dak_list[$daptorik_daks_info_list_with_dak_users_data['id']]['to_office_unit_name'];
            $daptorik_daks_info_list_with_dak_users_data['pending_time'] = $dakPendingTime[$daptorik_daks_info_list_with_dak_users_data['id']]['lastPendingTime'];
        }

        $priorities = json_decode(DAK_PRIORITY_TYPE, true);
        $priorities[0] = '';
        $security_levels = json_decode(DAK_SECRECY_TYPE, true);
        $security_levels[0] = '';

        $preorder = '';
        $data = [];
        $i = 0;
        if (!empty($daptorik_daks_info_list)) {
            foreach ($daptorik_daks_info_list as $dak) {
                if (empty($preorder)) {
                    $preorder = $dak['to_office_unit_id'];
                }
                $preorder = $preorder . ',' . $dak['to_office_unit_id'];
            }
            if (!empty($preorder)) {
                $order_unit = $officeUnitTable->getOrderwiseUnits($preorder);
            }
            if (!empty($order_unit)) {
                foreach ($order_unit as $key => $val) {
                    foreach ($daptorik_daks_info_list as $dak) {
                        if ($dak['to_office_unit_id'] == $key) {
                            $data[$i]['to_office_unit_name'] = $dak['to_office_unit_name'];
                            $data[$i]['to_office_unit_id'] = $dak['to_office_unit_id'];

                            $data[$i]['dak_category'] = $dak['dak_category'];

                            $data[$i]['movecreated'] = $dak['modified'];

                            $data[$i]['dak_priority'] = $priorities[$dak['dak_priority_level']];

                            $data[$i]['sender_sarok_no'] = $dak['sender_sarok_no'];
                            $data[$i]['sender_office_name'] = $dak['sender_office_name'];
                            $data[$i]['sender_office_unit_name'] = $dak['sender_office_unit_name'];
                            $data[$i]['sender_officer_designation_label'] = $dak['sender_officer_designation_label'];
                            $data[$i]['sender_name'] = $dak['sender_name'];

                            $data[$i]['dakcreted'] = $dak['receiving_date'];
                            $data[$i]['dak_received_no'] = $dak['dak_received_no'];
                            $data[$i]['docketing_no'] = $dak['docketing_no'];
                            $data[$i]['dak_subject'] = $dak['dak_subject'];
                            $data[$i]['dak_security_level'] = $security_levels[$dak['dak_security_level']];

                            $data[$i]['receiving_office_unit_name'] = $dak['receiving_office_unit_name'];
                            $data[$i]['receiving_officer_designation_label'] = $dak['receiving_officer_designation_label'];
                            $data[$i]['receiving_officer_name'] = $dak['receiving_officer_name'];
                            $data[$i]['last_status_officer_name'] = $dak['last_status_officer_name'];
                            $data[$i]['last_status_officer_designation_label'] = $dak['last_status_officer_designation_label'];
                            $data[$i]['last_status_office_unit_name'] = $dak['last_status_office_unit_name'];
                            $data[$i]['pending_time'] = $dak['pending_time'];

                            $i++;
                        }
                    }
                }
            }
        }
    }
    else {
            $data=NULL;
    }
        $office = TableRegistry::get('Offices');
        $office_name = $office->find()->select(['office_name_bng', 'office_address'])->where(['id' => $employee_office['office_id']])->first();
        if($isExport=='excel' || $isExport=='pdf'){
            return $result= Array('data'=>$data,'office_name'=>$office_name['office_name_bng']);
        }
        $this->set('data', $data);
        $this->set('office_name', $office_name);
    }

    public function nothiVuktoDakListExcel($type='pdf',$startDate='',$endDate='',$dateRangeBn='',$unit_id='') {
        $result = $this->nothiVuktoDakListContent($startDate, $endDate, $type, 20, $unit_id);
        $unitInformation='';
        $DakList = Array();
        $j=0;

        if (!empty($result['data'])) {

            foreach ($result['data'] as $dak) {
                if( $unitInformation ==  $dak['receiving_office_unit_name']){
                    $DakList[$j]['receiving_office_unit_name'] ='';
                }
                else {
                    $DakList[$j]['receiving_office_unit_name'] = $dak['receiving_office_unit_name'];
                    $unitInformation = $dak['receiving_office_unit_name'];
                }
                $created = new Time($dak['created']);
                $dak['created'] = $created->i18nFormat(null, null, 'bn-BD');

                $DakList[$j]['docketing_no'] = enTobn($dak['docketing_no']);
                $DakList[$j]['sender_sarok_no'] = h($dak['sender_sarok_no']);
                $DakList[$j]['created'] = $dak['created'];
                $DakList[$j]['dak_subject'] = h($dak['dak_subject']);
                $DakList[$j]['sender_name'] = h($dak['sender_name']) . ', ' . ($dak['sender_officer_designation_label']) . __(!empty($dak['sender_office_unit_name']) ? (", " . $dak['sender_office_unit_name']) : '') . __(!empty($dak['sender_office_name']) ? (", " . $dak['sender_office_name']) : '');
                $DakList[$j]['modified'] =h($dak['modified']);
                $DakList[$j]['dak_security_level'] = h(isset($result['security_level_list'][$dak['dak_security_level']]) ? $result['security_level_list'][$dak['dak_security_level']] : '');
                $DakList[$j]['dak_priority'] = h(isset($result['priority_list'][$dak['dak_priority_level']]) ? $result['priority_list'][$dak['dak_priority_level']] : '');

                $j++;

            }
        }
        if(empty($DakList)){
            $DakList= Array();
        }
        if($type == 'excel'){
            $this->printExcel($DakList, [
                ['key' => 'receiving_office_unit_name', 'title' => 'শাখা'],
                ['key' => 'docketing_no', 'title' => 'গ্রহণ নম্বর'],
                ['key' => 'sender_sarok_no', 'title' => 'স্মারক নম্বর'],
                ['key' => 'created', 'title' => 'আবেদনের তারিখ'],
                ['key' => 'dak_subject', 'title' => 'বিষয়'],
                ['key' => 'sender_name', 'title' => 'আবেদনকারী'],
                ['key' => 'modified', 'title' => 'নথিতে উপস্থাপনের তারিখ'],
                ['key' => 'dak_security_level', 'title' => 'গোপনীয়তা'],
                ['key' => 'dak_priority', 'title' => 'অগ্রাধিকার'],
            ],[
                'name'=>((isset($result['office_name']))?$result['office_name'].' অফিসের ':'') .'নথিতে উপস্থাপিত ডাকসমূহ' ,
                'title' => [
                    ((isset($result['office_name']))?$result['office_name'].' অফিসের ':'') .'নথিতে উপস্থাপিত ডাকসমূহ' ,
                    $dateRangeBn,
                ]
            ]);
        } else {
            $this->printPdf($DakList, [
                ['key' => 'receiving_office_unit_name', 'title' => 'শাখা'],
                ['key' => 'docketing_no', 'title' => 'গ্রহণ নম্বর'],
                ['key' => 'sender_sarok_no', 'title' => 'স্মারক নম্বর'],
                ['key' => 'created', 'title' => 'আবেদনের তারিখ'],
                ['key' => 'dak_subject', 'title' => 'বিষয়'],
                ['key' => 'sender_name', 'title' => 'আবেদনকারী'],
                ['key' => 'modified', 'title' => 'নথিতে উপস্থাপনের তারিখ'],
                ['key' => 'dak_security_level', 'title' => 'গোপনীয়তা'],
                ['key' => 'dak_priority', 'title' => 'অগ্রাধিকার'],
            ],[
                'name'=>((isset($result['office_name']))?$result['office_name'].' অফিসের ':'') .'নথিতে উপস্থাপিত ডাকসমূহ' ,
                'title' => [
                    ((isset($result['office_name']))?$result['office_name'].' অফিসের ':'') .'নথিতে উপস্থাপিত ডাকসমূহ' ,
                    $dateRangeBn,
                ]
            ]);
        }

        exit;
    }

    public function nothiVuktoDakList() {
        $session = $this->request->session();
        $employee = $session->read('logged_employee_record');
        $this->set("office", $employee);

        $units_list = $this->getOwnUnitList();
        $this->set("units_list", $units_list);
    }
    public function getNothiVuktoDakListContent($options = []){

        /*Set Variables for further query*/
        $startDate = isset($options['startDate'])?$options['startDate']:'';
        $endDate = isset($options['endDate'])?$options['endDate']:'';
        $isExport = isset($options['isExport'])?$options['isExport']:'';
        $per_page = isset($options['per_page'])?$options['per_page']:'';
        $unit_id = isset($options['unit_id'])?$options['unit_id']:'';
        $condition = isset($options['condition'])?$options['condition']:[];
        $priorities = isset($options['priority_list'])?$options['priority_list']:[];
        $security_levels = isset($options['security_level_list'])?$options['security_level_list']:[];
        $employee_office = isset($options['selected_office_section'])?$options['selected_office_section']:[];
        $unit_id_array = isset($options['unit_id_array'])?$options['unit_id_array']:[];
        $office_name = isset($options['office_name'])?$options['office_name']:[];
        /*Set Variables for further query*/

        $dak_grohon_table = TableRegistry::get("DakDaptoriks");

        $officeUnitTable = TableRegistry::get("OfficeUnits");

        $query = $dak_grohon_table
            ->find()
            ->select([
                'id',

                'sender_sarok_no',
                'sender_office_id',
                'sender_officer_id',
                'sender_office_name',
                'sender_office_unit_id',
                'sender_office_unit_name',
                'sender_officer_designation_id',
                'sender_officer_designation_label',
                'sending_date',
                'sender_name',

                'dak_sending_media',
                'dak_received_no',
                'docketing_no',
                'dak_subject',
                'dak_security_level',
                'dak_priority_level',

                'receiving_office_id',
                'receiving_office_unit_id',
                'receiving_office_unit_name',
                'receiving_officer_id',
                'receiving_officer_designation_id',
                'receiving_officer_designation_label',
                'receiving_officer_name',

                'dak_status',
                'created_by',
                'modified_by',
                'created',
                'modified'
            ])
            ->where(["dak_status" => 'NothiVukto'])
            ->where(["receiving_office_unit_id IN" => $unit_id_array])
            ->where(function($query) use ($startDate,$endDate){
                                      return $query->between('date(modified)',$startDate,$endDate);
            })
            ->order(['receiving_office_unit_id' => 'DESC','created' => 'DESC']);

        if($isExport=='excel' || $isExport=='pdf'){
            $grohon_daks = $query->toArray();
        }
        else {
            $grohon_daks = $this->Paginator->paginate($query, ['limit' => $per_page]);
        }
        $preorder = '';
        $data = [];
        $i = 0;
        if (!empty($grohon_daks)) {
            foreach ($grohon_daks as $dak) {
                if (empty($preorder)) {
                    $preorder = $dak['receiving_office_unit_id'];
                }
                $preorder = $preorder . ',' . $dak['receiving_office_unit_id'];
            }
            if (!empty($preorder)) {
                $order_unit = $officeUnitTable->getOrderwiseUnits($preorder);
            }
            if (!empty($order_unit)) {
                foreach ($order_unit as $key => $val) {
                    foreach ($grohon_daks as $dak) {
                        if ($dak['receiving_office_unit_id'] == $key) {

                            $data[$i]['id'] = $dak['id'];

                            $data[$i]['sender_sarok_no'] = $dak['sender_sarok_no'];
                            $data[$i]['sender_office_id'] = $dak['sender_office_id'];
                            $data[$i]['sender_officer_id'] = $dak['sender_officer_id'];
                            $data[$i]['sender_office_name'] = $dak['sender_office_name'];
                            $data[$i]['sender_office_unit_id'] = $dak['sender_office_unit_id'];
                            $data[$i]['sender_office_unit_name'] = $dak['sender_office_unit_name'];
                            $data[$i]['sender_officer_designation_id'] = $dak['sender_officer_designation_id'];
                            $data[$i]['sender_officer_designation_label'] = $dak['sender_officer_designation_label'];
                            $data[$i]['sending_date'] = $dak['sending_date'];
                            $data[$i]['sender_name'] = $dak['sender_name'];
                            $data[$i]['sender_address'] = $dak['sender_address'];
                            $data[$i]['sender_email'] = $dak['sender_email'];
                            $data[$i]['sender_phone'] = $dak['sender_phone'];
                            $data[$i]['sender_mobile'] = $dak['sender_mobile'];

                            $data[$i]['dak_sending_media'] = $dak['dak_sending_media'];
                            $data[$i]['dak_received_no'] = $dak['dak_received_no'];
                            $data[$i]['docketing_no'] = $dak['docketing_no'];
                            $data[$i]['receiving_date'] = $dak['receiving_date'];
                            $data[$i]['dak_subject'] = $dak['dak_subject'];
                            $data[$i]['dak_security_level'] = $dak['dak_security_level'];
                            $data[$i]['dak_priority_level'] = $dak['dak_priority_level'];

                            $data[$i]['receiving_office_id'] = $dak['receiving_office_id'];
                            $data[$i]['receiving_office_unit_id'] = $dak['receiving_office_unit_id'];
                            $data[$i]['receiving_office_unit_name'] = $dak['receiving_office_unit_name'];
                            $data[$i]['receiving_officer_id'] = $dak['receiving_officer_id'];
                            $data[$i]['receiving_officer_designation_id'] = $dak['receiving_officer_designation_id'];
                            $data[$i]['receiving_officer_designation_label'] = $dak['receiving_officer_designation_label'];
                            $data[$i]['receiving_officer_name'] = $dak['receiving_officer_name'];

                            $data[$i]['dak_status'] = $dak['dak_status'];
                            $data[$i]['created_by'] = $dak['created_by'];
                            $data[$i]['modified_by'] = $dak['modified_by'];
                            $data[$i]['uploader_designation_id'] = $dak['uploader_designation_id'];
                            $data[$i]['created'] = $dak['created'];
                            $data[$i]['modified'] = $dak['modified'];
                            $i++;
                        }
                    }
                }
            }
        }
        if($isExport=='excel' || $isExport=='pdf'){
            return $result= Array('data'=> $data,'office_name'=> $office_name['office_name_bng'],'priority_list'=> $priorities,'security_level_list'=> $security_levels);
        }

        $this->set('data', $data);
        $this->set('office_name', $office_name);

    }
    public function generateNothiVuktoDakListContentFromDumpTable($options = []){

        /*Set Variables for further query*/
        $startDate = isset($options['startDate'])?$options['startDate']:'';
        $endDate = isset($options['endDate'])?$options['endDate']:'';
        $isExport = isset($options['isExport'])?$options['isExport']:'';
        $per_page = isset($options['per_page'])?$options['per_page']:'';
        $unit_id = isset($options['unit_id'])?$options['unit_id']:'';
        $condition = isset($options['condition'])?$options['condition']:[];
        $priorities = isset($options['priority_list'])?$options['priority_list']:[];
        $security_levels = isset($options['security_level_list'])?$options['security_level_list']:[];
        $employee_office = isset($options['selected_office_section'])?$options['selected_office_section']:[];
        $unit_id_array = isset($options['unit_id_array'])?$options['unit_id_array']:[];
        $office_name = isset($options['office_name'])?$options['office_name']:[];
        /*Set Variables for further query*/

        $table_DakRegister = TableRegistry::get("DakRegister");

        $officeUnitTable = TableRegistry::get("OfficeUnits");

        $query = $table_DakRegister->find()
            ->select([
                'dak_id',
                'dak_type',

                'sender_sarok_no',
                'sender_office_id',
                'sender_officer_id',
                'sender_office_name',
                'sender_office_unit_id',
                'sender_office_unit_name',
                'sender_officer_designation_id',
                'sender_officer_designation_label',
                'sending_date',
                'sender_name',

                'dak_sending_media',
                'dak_received_no',
                'docketing_no',
                'dak_subject',
                'dak_security_level',
                'dak_priority_level',

                'receiving_office_id',
                'receiving_office_unit_id',
                'receiving_office_unit_name',
                'receiving_officer_id',
                'receiving_officer_designation_id',
                'receiving_officer_designation_label',
                'receiving_officer_name',

                'dak_status',
                'created_by',
                'modified_by',
                'created'=> 'dak_created',
                'modified' => 'movement_time'
            ])
              ->where(["operation_type" => 'NothiVukto','dak_type' => DAK_DAPTORIK])
              ->where(["receiving_office_unit_id IN" => $unit_id_array])
              ->where(function($query) use ($startDate,$endDate){
                  return $query->between('date(movement_time)',$startDate,$endDate);
              })
              ->order(['receiving_office_unit_id' => 'DESC','dak_created' => 'DESC']);

        if($isExport=='excel' || $isExport=='pdf'){
            $grohon_daks = $query->toArray();
        }
        else {
            $grohon_daks = $this->Paginator->paginate($query, ['limit' => $per_page]);
        }
        $preorder = '';
        $data = [];
        $i = 0;
        if (!empty($grohon_daks)) {
            foreach ($grohon_daks as $dak) {
                if (empty($preorder)) {
                    $preorder = $dak['receiving_office_unit_id'];
                }
                $preorder = $preorder . ',' . $dak['receiving_office_unit_id'];
            }
            if (!empty($preorder)) {
                $order_unit = $officeUnitTable->getOrderwiseUnits($preorder);
            }
            if (!empty($order_unit)) {
                foreach ($order_unit as $key => $val) {
                    foreach ($grohon_daks as $dak) {
                        if ($dak['receiving_office_unit_id'] == $key) {

                            $data[$i]['id'] = $dak['dak_id'];

                            $data[$i]['sender_sarok_no'] = $dak['sender_sarok_no'];
                            $data[$i]['sender_office_id'] = $dak['sender_office_id'];
                            $data[$i]['sender_officer_id'] = $dak['sender_officer_id'];
                            $data[$i]['sender_office_name'] = $dak['sender_office_name'];
                            $data[$i]['sender_office_unit_id'] = $dak['sender_office_unit_id'];
                            $data[$i]['sender_office_unit_name'] = $dak['sender_office_unit_name'];
                            $data[$i]['sender_officer_designation_id'] = $dak['sender_officer_designation_id'];
                            $data[$i]['sender_officer_designation_label'] = $dak['sender_officer_designation_label'];
                            $data[$i]['sending_date'] = $dak['sending_date'];
                            $data[$i]['sender_name'] = $dak['sender_name'];

                            $data[$i]['dak_sending_media'] = $dak['dak_sending_media'];
                            $data[$i]['dak_received_no'] = $dak['dak_received_no'];
                            $data[$i]['docketing_no'] = $dak['docketing_no'];
                            $data[$i]['dak_subject'] = $dak['dak_subject'];
                            $data[$i]['dak_security_level'] = $dak['dak_security_level'];
                            $data[$i]['dak_priority_level'] = $dak['dak_priority_level'];
                            $data[$i]['dak_cover'] = $dak['dak_cover'];

                            $data[$i]['receiving_date'] = $dak['receiving_date'];
                            $data[$i]['receiving_office_id'] = $dak['receiving_office_id'];
                            $data[$i]['receiving_office_unit_id'] = $dak['receiving_office_unit_id'];
                            $data[$i]['receiving_office_unit_name'] = $dak['receiving_office_unit_name'];
                            $data[$i]['receiving_officer_id'] = $dak['receiving_officer_id'];
                            $data[$i]['receiving_officer_designation_id'] = $dak['receiving_officer_designation_id'];
                            $data[$i]['receiving_officer_designation_label'] = $dak['receiving_officer_designation_label'];
                            $data[$i]['receiving_officer_name'] = $dak['receiving_officer_name'];

                            $data[$i]['dak_status'] = $dak['operation_type'];
                            $data[$i]['created'] = $dak['created'];
                            $data[$i]['modified'] = $dak['modified'];
                            $i++;
                        }
                    }
                }
            }
        }
        if($isExport=='excel' || $isExport=='pdf'){
            return $result= Array('data'=> $data,'office_name'=> $office_name['office_name_bng'],'priority_list'=> $priorities,'security_level_list'=> $security_levels);
        }

        $this->set('data', $data);
        $this->set('office_name', $office_name);

    }

    public function nothiVuktoDakListContent($startDate = "", $endDate = "",$isExport="", $per_page = 20 ,$unit_id = "",$version = 1) {

        $this->layout = null;
        $employee_office = $this->getCurrentDakSection();
        $employee_section_id = $employee_office['office_unit_id'];

        $officeUnitTable = TableRegistry::get("OfficeUnits");

        $units = $officeUnitTable->getOfficeChildInfo($employee_office['office_unit_id']);
        $unitsOwn = $officeUnitTable->getOwnChildInfo($employee_office['office_unit_id']);

        $units = !empty($units[0]) ? $units[0] : '';
        $unitsOwn = !empty($unitsOwn[0]) ? ($employee_office['office_unit_id'] . ',' . $unitsOwn[0]) : $employee_office['office_unit_id'];

        $units = $units . (!empty($unitsOwn) ? ((!empty($units) ? ',' : '') . $unitsOwn) : '');
        $unit_id_array = explode(",",$units);
        if(!empty($unit_id) && in_array((int)$unit_id,$unit_id_array)){
            $unit_id_array = (int)$unit_id;
        }

        $startDate = !empty($startDate)?h($startDate):date('Y-m-d');
        $endDate = !empty($endDate)?h($endDate):date('Y-m-d');

        $priority_list = json_decode(DAK_PRIORITY_TYPE, TRUE);
        $security_level_list = json_decode(DAK_SECRECY_TYPE, TRUE);

        $office = TableRegistry::get('Offices');
        $office_name = $office->find()->select(['office_name_bng', 'office_address'])->where(['id' => $employee_office['office_id']])->first();

        $condition = [];

        /*Set Variables for further query*/
        $function_options = [];
        $function_options['startDate'] = $startDate;
        $function_options['endDate'] = $endDate;
        $function_options['isExport'] = $isExport;
        $function_options['per_page'] = $per_page;
        $function_options['unit_id'] = $unit_id;
        $function_options['condition'] = $condition;
        $function_options['priority_list'] = $priority_list;
        $function_options['security_level_list'] = $security_level_list;
        $function_options['selected_office_section'] = $employee_section_id;
        $function_options['unit_id_array'] = $unit_id_array;
        $function_options['office_name'] = $office_name;
        /*Set Variables for further query*/

        $response = ''; //for export
        if($version == 1){
            $response =  $this->getNothiVuktoDakListContent($function_options);
        }
        else if($version == 2){
            $response = $this->generateNothiVuktoDakListContentFromDumpTable($function_options);
        }
        if(!empty($isExport) && ($isExport=='excel' || $isExport=='pdf')){
            return $response;
        }

        $this->set(compact('security_level_list','priority_list','version'));


    }
//    public function nothiVuktoDakListContetnt($startDate = "", $endDate = "",$isExport='') {
//
//        $this->layout = null;
//        $condition = '1 ';
//        $employee_office = $this->getCurrentDakSection();
//        $employee_section_id = $employee_office['office_unit_id'];
//
//        $dak_grohon_table = TableRegistry::get("DakDaptoriks");
//
//        $officeUnitTable = TableRegistry::get("OfficeUnits");
//
//        $units = $officeUnitTable->getOfficeChildInfo($employee_office['office_unit_id']);
//        $unitsOwn = $officeUnitTable->getOwnChildInfo($employee_office['office_unit_id']);
//
//        $units = !empty($units[0]) ? $units[0] : '';
//        $unitsOwn = !empty($unitsOwn[0]) ? ($employee_office['office_unit_id'] . ',' . $unitsOwn[0]) : $employee_office['office_unit_id'];
//
//        $units = $units . (!empty($unitsOwn) ? ((!empty($units) ? ',' : '') . $unitsOwn) : '');
//        $condition .= " AND receiving_office_unit_id IN ({$units})";
//
//
//        $startDate = !empty($startDate)?h($startDate):'';
//        $endDate = !empty($endDate)?h($endDate):'';
//
//        if (!empty($startDate) And ! empty($endDate)) {
//            if (!empty($condition))
//                $condition .= ' AND ';
//            $condition .= " modified  BETWEEN  '{$startDate} 00:00:00' AND '{$endDate}
//            23:59:59' ";
//        } else {
//            $startDate = date('Y-m-d');
//            $endDate = date('Y-m-d');
//            if (!empty($condition))
//                $condition .= ' AND ';
//            $condition .= " modified  BETWEEN  '{$startDate} 00:00:00' AND '{$endDate}
//            23:59:59' ";
//        }
//        if($isExport=='excel' || $isExport=='pdf'){
//            $grohon_daks = $dak_grohon_table->find()->where(["dak_status" => 'NothiVukto'])->where([$condition])->order(['receiving_office_unit_id' => 'DESC','created' => 'DESC']);
//        }
//        else {
//            $grohon_daks = $this->Paginator->paginate($dak_grohon_table->find()->where(["dak_status" => 'NothiVukto'])->where([$condition])->order(['created' => 'DESC']), ['limit' => 20]);
//        }
//        //pr($grohon_daks);die;
//
//        $priority_list = json_decode(DAK_PRIORITY_TYPE, TRUE);
//        $security_level_list = json_decode(DAK_SECRECY_TYPE, TRUE);
//
//        $office = TableRegistry::get('Offices');
//        $office_name = $office->find()->select(['office_name_bng', 'office_address'])->where(['id' => $employee_office['office_id']])->first();
//
//        $preorder = '';
//        $data = [];
//        $i = 0;
//        if (!empty($grohon_daks)) {
//            foreach ($grohon_daks as $dak) {
//                if (empty($preorder)) {
//                    $preorder = $dak['receiving_office_unit_id'];
//                }
//                $preorder = $preorder . ',' . $dak['receiving_office_unit_id'];
//            }
//            if (!empty($preorder)) {
//                $order_unit = $officeUnitTable->getOrderwiseUnits($preorder);
//            }
//            if (!empty($order_unit)) {
//                foreach ($order_unit as $key => $val) {
//                    foreach ($grohon_daks as $dak) {
//                        if ($dak['receiving_office_unit_id'] == $key) {
//
//                            $data[$i]['id'] = $dak['id'];
//                            $data[$i]['sender_sarok_no'] = $dak['sender_sarok_no'];
//                            $data[$i]['sender_office_id'] = $dak['sender_office_id'];
//                            $data[$i]['sender_officer_id'] = $dak['sender_officer_id'];
//                            $data[$i]['sender_office_name'] = $dak['sender_office_name'];
//                            $data[$i]['sender_office_unit_id'] = $dak['sender_office_unit_id'];
//                            $data[$i]['sender_office_unit_name'] = $dak['sender_office_unit_name'];
//                            $data[$i]['sender_officer_designation_id'] = $dak['sender_officer_designation_id'];
//                            $data[$i]['sender_officer_designation_label'] = $dak['sender_officer_designation_label'];
//                            $data[$i]['sending_date'] = $dak['sending_date'];
//                            $data[$i]['sender_name'] = $dak['sender_name'];
//                            $data[$i]['sender_address'] = $dak['sender_address'];
//                            $data[$i]['sender_email'] = $dak['sender_email'];
//                            $data[$i]['sender_phone'] = $dak['sender_phone'];
//                            $data[$i]['sender_mobile'] = $dak['sender_mobile'];
//                            $data[$i]['dak_sending_media'] = $dak['dak_sending_media'];
//                            $data[$i]['dak_received_no'] = $dak['dak_received_no'];
//                            $data[$i]['docketing_no'] = $dak['docketing_no'];
//                            $data[$i]['receiving_date'] = $dak['receiving_date'];
//                            $data[$i]['dak_subject'] = $dak['dak_subject'];
//                            $data[$i]['dak_security_level'] = $dak['dak_security_level'];
//                            $data[$i]['dak_priority_level'] = $dak['dak_priority_level'];
//                            $data[$i]['dak_cover'] = $dak['dak_cover'];
//                            $data[$i]['dak_description'] = $dak['dak_description'];
//                            $data[$i]['receiving_office_id'] = $dak['receiving_office_id'];
//                            $data[$i]['receiving_office_unit_id'] = $dak['receiving_office_unit_id'];
//                            $data[$i]['receiving_office_unit_name'] = $dak['receiving_office_unit_name'];
//                            $data[$i]['receiving_officer_id'] = $dak['receiving_officer_id'];
//                            $data[$i]['receiving_officer_designation_id'] = $dak['receiving_officer_designation_id'];
//                            $data[$i]['receiving_officer_designation_label'] = $dak['receiving_officer_designation_label'];
//                            $data[$i]['receiving_officer_name'] = $dak['receiving_officer_name'];
//                            $data[$i]['dak_status'] = $dak['dak_status'];
//                            $data[$i]['created_by'] = $dak['created_by'];
//                            $data[$i]['modified_by'] = $dak['modified_by'];
//                            $data[$i]['uploader_designation_id'] = $dak['uploader_designation_id'];
//                            $data[$i]['created'] = $dak['created'];
//                            $data[$i]['modified'] = $dak['modified'];
//                            $i++;
//                        }
//                    }
//                }
//            }
//        }
//
//        if($isExport=='excel' || $isExport=='pdf'){
//            return $result= Array('data'=> $data,'office_name'=> $office_name['office_name_bng'],'priority_list'=> $priority_list,'security_level_list'=> $security_level_list);
//        }
//
//        $this->set('priority_list', $priority_list);
//        $this->set('security_level_list', $security_level_list);
//        $this->set('data', $data);
//        $this->set('office_name', $office_name);
//    }

    public function nothiJaatDakListExcel($type='pdf',$startDate = "", $endDate = "",$dateRangeBn='',$unit_id='') {
        $result = $this->nothiJaatDakListContent($startDate, $endDate, $type, 20, $unit_id);
        $unitInformation='';
        $DakList = Array();
        $j=0;

        if (!empty($result['data'])) {

            foreach ($result['data'] as $dak) {
                if( $unitInformation ==  $dak['receiving_office_unit_name']){
                    $DakList[$j]['receiving_office_unit_name'] ='';
                }
                else {
                    $DakList[$j]['receiving_office_unit_name'] = $dak['receiving_office_unit_name'];
                    $unitInformation = $dak['receiving_office_unit_name'];
                }

                $DakList[$j]['docketing_no'] = enTobn($dak['docketing_no']);
                $DakList[$j]['sender_sarok_no'] = h($dak['sender_sarok_no']);
                $DakList[$j]['created'] = h($dak['created']);
                $DakList[$j]['dak_subject'] = h($dak['dak_subject']);
                $DakList[$j]['sender_name'] = h($dak['sender_name']) . ', ' . ($dak['sender_officer_designation_label']) . __(!empty($dak['sender_office_unit_name']) ? (", " . $dak['sender_office_unit_name']) : '') . __(!empty($dak['sender_office_name']) ? (", " . $dak['sender_office_name']) : '');
                $DakList[$j]['modified'] =h($dak['modified']);
                $DakList[$j]['dak_security_level'] = h(isset($result['security_level_list'][$dak['dak_security_level']]) ? $result['security_level_list'][$dak['dak_security_level']] : '');
                $DakList[$j]['dak_priority'] = h(isset($result['priority_list'][$dak['dak_priority_level']]) ? $result['priority_list'][$dak['dak_priority_level']] : '');

                $j++;

            }
        }
        if(empty($DakList)){
            $DakList= Array();
        }
        if($type == 'excel'){
            $this->printExcel($DakList, [
                ['key' => 'receiving_office_unit_name', 'title' => 'শাখা'],
                ['key' => 'docketing_no', 'title' => 'গ্রহণ নম্বর'],
                ['key' => 'sender_sarok_no', 'title' => 'স্মারক নম্বর'],
                ['key' => 'created', 'title' => 'আবেদনের তারিখ'],
                ['key' => 'dak_subject', 'title' => 'বিষয়'],
                ['key' => 'sender_name', 'title' => 'আবেদনকারী'],
                ['key' => 'modified', 'title' => 'নথিজাতের তারিখ'],
                ['key' => 'dak_security_level', 'title' => 'গোপনীয়তা'],
                ['key' => 'dak_priority', 'title' => 'অগ্রাধিকার'],
            ],[
                'name'=>((isset($result['office_name']))?$result['office_name'].' অফিসের ':'') .'নথিজাত ডাকসমূহ' ,
                'title' => [
                    ((isset($result['office_name']))?$result['office_name'].' অফিসের ':'') .'নথিজাত ডাকসমূহ' ,
                    $dateRangeBn,
                ]
            ]);
        } else {
            $this->printPdf($DakList, [
                ['key' => 'receiving_office_unit_name', 'title' => 'শাখা'],
                ['key' => 'docketing_no', 'title' => 'গ্রহণ নম্বর'],
                ['key' => 'sender_sarok_no', 'title' => 'স্মারক নম্বর'],
                ['key' => 'created', 'title' => 'আবেদনের তারিখ'],
                ['key' => 'dak_subject', 'title' => 'বিষয়'],
                ['key' => 'sender_name', 'title' => 'আবেদনকারী'],
                ['key' => 'modified', 'title' => 'নথিজাতের তারিখ'],
                ['key' => 'dak_security_level', 'title' => 'গোপনীয়তা'],
                ['key' => 'dak_priority', 'title' => 'অগ্রাধিকার'],
            ],[
                'name'=>((isset($result['office_name']))?$result['office_name'].' অফিসের ':'') .'নথিজাত ডাকসমূহ' ,
                'title' => [
                    ((isset($result['office_name']))?$result['office_name'].' অফিসের ':'') .'নথিজাত ডাকসমূহ' ,
                    $dateRangeBn,
                ]
            ]);
        }

        exit;
    }

    public function nothiJaatDakList() {
        $session = $this->request->session();
        $employee = $session->read('logged_employee_record');
        $this->set("office", $employee);

        $units_list = $this->getOwnUnitList();
        $this->set("units_list", $units_list);
    }

    public function nothiJaatDakListContent($startDate = "", $endDate = "",$isExport="",$per_page = 20 ,$unit_id = "",$version = 1) {

        $this->layout = null;
        $employee_office = $this->getCurrentDakSection();
        $employee_section_id = $employee_office['office_unit_id'];

        if($version == 1){
            $dak_grohon_table = TableRegistry::get("DakDaptoriks");
        }
        elseif ($version == 2){
            $dak_grohon_table = TableRegistry::get("DakRegister");
        }

        $officeUnitTable = TableRegistry::get("OfficeUnits");

        $units = $officeUnitTable->getOfficeChildInfo($employee_office['office_unit_id']);
        $unitsOwn = $officeUnitTable->getOwnChildInfo($employee_office['office_unit_id']);

        $units = !empty($units[0]) ? $units[0] : '';
        $unitsOwn = !empty($unitsOwn[0]) ? ($employee_office['office_unit_id'] . ',' . $unitsOwn[0]) : $employee_office['office_unit_id'];

        $units = $units . (!empty($unitsOwn) ? ((!empty($units) ? ',' : '') . $unitsOwn) : '');
        $unit_id_array = explode(",",$units);
        if(!empty($unit_id) && in_array((int)$unit_id,$unit_id_array)){
            $unit_id_array = (int)$unit_id;
        }
        $order_unit = $officeUnitTable->getOrderwiseUnits($units);


        $startDate = !empty($startDate)?h($startDate):date('Y-m-d');
        $endDate = !empty($endDate)?h($endDate):date('Y-m-d');

        if($version == 1){
            $query = $dak_grohon_table->find()

                                      ->where(["receiving_office_unit_id IN " => $unit_id_array])
                                      ->where(function($query) use ($startDate,$endDate){
                                          return $query->between('date(modified)',$startDate,$endDate);
                                      })
                                      ->where(["dak_status" => DAK_CATEGORY_NOTHIJATO])
                                      ->order([ 'receiving_office_unit_id' => 'DESC', 'created' => 'DESC']);
        }
        else{
            $query = $dak_grohon_table->find()
                                      ->where(["receiving_office_unit_id IN " => $unit_id_array])
                                      ->where(function($query) use ($startDate,$endDate){
                                          return $query->between('date(movement_time)',$startDate,$endDate);
                                      })
                                      ->where(["dak_status" => DAK_CATEGORY_NOTHIJATO,'dak_type' => DAK_DAPTORIK])
                                      ->order([ 'receiving_office_unit_id' => 'DESC', 'movement_time' => 'DESC'])->group(['dak_id']);
        }


        if($isExport=='excel' || $isExport=='pdf'){
            $grohon_daks = $query->toArray();
        }
        else {
            $grohon_daks = $this->Paginator->paginate($query,['limit' => $per_page]);
        }



        $security_level_list = json_decode(DAK_SECRECY_TYPE, TRUE);
        $security_level_list[0] = '';
        $priority_list= json_decode(DAK_PRIORITY_TYPE, TRUE);
        $priority_list[0] = '';
        
        $office = TableRegistry::get('Offices');
        $office_name = $office->find()->select(['office_name_bng', 'office_address'])->where(['id' => $employee_office['office_id']])->first();
        $preorder = '';
        $data = [];
        $i = 0;
        if (!empty($grohon_daks)) {
            foreach ($grohon_daks as $dak) {
                if (empty($preorder)) {
                    $preorder = $dak['receiving_office_unit_id'];
                }
                $preorder = $preorder . ',' . $dak['receiving_office_unit_id'];
            }
            if (!empty($preorder)) {
                $order_unit = $officeUnitTable->getOrderwiseUnits($preorder);
            }
            if (!empty($order_unit)) {
                foreach ($order_unit as $key => $val) {
                    foreach ($grohon_daks as $dak) {
                        if ($dak['receiving_office_unit_id'] == $key) {

                            $data[$i]['id'] = $dak['id'];
                            
                            $data[$i]['sender_sarok_no'] = $dak['sender_sarok_no'];
                            $data[$i]['sender_office_id'] = $dak['sender_office_id'];
                            $data[$i]['sender_officer_id'] = $dak['sender_officer_id'];
                            $data[$i]['sender_office_name'] = $dak['sender_office_name'];
                            $data[$i]['sender_office_unit_id'] = $dak['sender_office_unit_id'];
                            $data[$i]['sender_office_unit_name'] = $dak['sender_office_unit_name'];
                            $data[$i]['sender_officer_designation_id'] = $dak['sender_officer_designation_id'];
                            $data[$i]['sender_officer_designation_label'] = $dak['sender_officer_designation_label'];
                            $data[$i]['sending_date'] = $dak['sending_date'];
                            $data[$i]['sender_name'] = $dak['sender_name'];
                            $data[$i]['sender_address'] = $dak['sender_address'];
                            $data[$i]['sender_email'] = $dak['sender_email'];
                            $data[$i]['sender_phone'] = $dak['sender_phone'];
                            $data[$i]['sender_mobile'] = $dak['sender_mobile'];
                            
                            $data[$i]['dak_sending_media'] = $dak['dak_sending_media'];
                            $data[$i]['dak_received_no'] = $dak['dak_received_no'];
                            $data[$i]['docketing_no'] = $dak['docketing_no'];
                            $data[$i]['receiving_date'] = $dak['receiving_date'];
                            $data[$i]['dak_subject'] = $dak['dak_subject'];
                            $data[$i]['dak_security_level'] = $dak['dak_security_level'];
                            $data[$i]['dak_priority_level'] = $dak['dak_priority_level'];

                            
                            $data[$i]['receiving_office_id'] = $dak['receiving_office_id'];
                            $data[$i]['receiving_office_unit_id'] = $dak['receiving_office_unit_id'];
                            $data[$i]['receiving_office_unit_name'] = $dak['receiving_office_unit_name'];
                            $data[$i]['receiving_officer_id'] = $dak['receiving_officer_id'];
                            $data[$i]['receiving_officer_designation_id'] = $dak['receiving_officer_designation_id'];
                            $data[$i]['receiving_officer_designation_label'] = $dak['receiving_officer_designation_label'];
                            $data[$i]['receiving_officer_name'] = $dak['receiving_officer_name'];
                            
                            $data[$i]['dak_status'] = $dak['dak_status'];

                            if($version == 1){
                                $data[$i]['created'] = $dak['dak_created'];
                                $data[$i]['modified'] = $dak['movement_time'];
                            }else{
                                $data[$i]['created'] = $dak['created'];
                                $data[$i]['modified'] = $dak['modified'];
                            }
                            $i++;
                        }
                    }
                }
            }
        }

        if($isExport=='excel' || $isExport=='pdf'){
            return $result= Array('data'=> $data,'office_name'=> $office_name['office_name_bng'],'priority_list'=> $priority_list,'security_level_list'=> $security_level_list);
        }

       $this->set('data',$data);
       $this->set('priority_list',$priority_list);
       $this->set('security_level_list',$security_level_list);
       $this->set('office_name',$office_name);
    }
//    public function nothiJaatDakListContent($startDate = "", $endDate = "",$isExport="") {
//
//        $this->layout = null;
//        $condition = '1 ';
//        $employee_office = $this->getCurrentDakSection();
//        $employee_section_id = $employee_office['office_unit_id'];
//
//        $dak_grohon_table = TableRegistry::get("DakDaptoriks");
//
//        $officeUnitTable = TableRegistry::get("OfficeUnits");
//
//        $units = $officeUnitTable->getOfficeChildInfo($employee_office['office_unit_id']);
//        $unitsOwn = $officeUnitTable->getOwnChildInfo($employee_office['office_unit_id']);
//
//        $units = !empty($units[0]) ? $units[0] : '';
//        $unitsOwn = !empty($unitsOwn[0]) ? ($employee_office['office_unit_id'] . ',' . $unitsOwn[0]) : $employee_office['office_unit_id'];
//
//        $units = $units . (!empty($unitsOwn) ? ((!empty($units) ? ',' : '') . $unitsOwn) : '');
//        $order_unit = $officeUnitTable->getOrderwiseUnits($units);
//        $condition .= " AND receiving_office_unit_id IN ({$units})";
//
//
//        $startDate = !empty($startDate)?h($startDate):'';
//        $endDate = !empty($endDate)?h($endDate):'';
//
//        if (!empty($startDate) And ! empty($endDate)) {
//            if (!empty($condition))
//                $condition .= ' AND ';
//            $condition .= " modified  BETWEEN  '{$startDate} 00:00:00' AND '{$endDate}
//            23:59:59' ";
//        } else {
//            $startDate = date('Y-m-d');
//            $endDate = date('Y-m-d');
//            if (!empty($condition))
//                $condition .= ' AND ';
//            $condition .= " modified  BETWEEN  '{$startDate} 00:00:00' AND '{$endDate}
//            23:59:59' ";
//        }
//        if($isExport=='excel' || $isExport=='pdf'){
//            $grohon_daks = $dak_grohon_table->find()->where(["dak_status" => DAK_CATEGORY_NOTHIJATO])->where([$condition])->order([ 'receiving_office_unit_id' => 'DESC', 'created' => 'DESC']);
//        }
//        else {
//            $grohon_daks = $this->Paginator->paginate($dak_grohon_table->find()->where(["dak_status" => DAK_CATEGORY_NOTHIJATO])->where([$condition])->order([ 'created' => 'DESC']),['limit' => 20]);
//        }
//
//
//
//        $security_level_list = json_decode(DAK_SECRECY_TYPE, TRUE);
//        $priority_list= json_decode(DAK_PRIORITY_TYPE, TRUE);
//
//        $office = TableRegistry::get('Offices');
//        $office_name = $office->find()->select(['office_name_bng', 'office_address'])->where(['id' => $employee_office['office_id']])->first();
//        $preorder = '';
//        $data = [];
//        $i = 0;
//        if (!empty($grohon_daks)) {
//            foreach ($grohon_daks as $dak) {
//                if (empty($preorder)) {
//                    $preorder = $dak['receiving_office_unit_id'];
//                }
//                $preorder = $preorder . ',' . $dak['receiving_office_unit_id'];
//            }
//            if (!empty($preorder)) {
//                $order_unit = $officeUnitTable->getOrderwiseUnits($preorder);
//            }
//            if (!empty($order_unit)) {
//                foreach ($order_unit as $key => $val) {
//                    foreach ($grohon_daks as $dak) {
//                        if ($dak['receiving_office_unit_id'] == $key) {
//
//                            $data[$i]['id'] = $dak['id'];
//
//                            $data[$i]['sender_sarok_no'] = $dak['sender_sarok_no'];
//                            $data[$i]['sender_office_id'] = $dak['sender_office_id'];
//                            $data[$i]['sender_officer_id'] = $dak['sender_officer_id'];
//                            $data[$i]['sender_office_name'] = $dak['sender_office_name'];
//                            $data[$i]['sender_office_unit_id'] = $dak['sender_office_unit_id'];
//                            $data[$i]['sender_office_unit_name'] = $dak['sender_office_unit_name'];
//                            $data[$i]['sender_officer_designation_id'] = $dak['sender_officer_designation_id'];
//                            $data[$i]['sender_officer_designation_label'] = $dak['sender_officer_designation_label'];
//                            $data[$i]['sending_date'] = $dak['sending_date'];
//                            $data[$i]['sender_name'] = $dak['sender_name'];
//                            $data[$i]['sender_address'] = $dak['sender_address'];
//                            $data[$i]['sender_email'] = $dak['sender_email'];
//                            $data[$i]['sender_phone'] = $dak['sender_phone'];
//                            $data[$i]['sender_mobile'] = $dak['sender_mobile'];
//
//                            $data[$i]['dak_sending_media'] = $dak['dak_sending_media'];
//                            $data[$i]['dak_received_no'] = $dak['dak_received_no'];
//                            $data[$i]['docketing_no'] = $dak['docketing_no'];
//                            $data[$i]['receiving_date'] = $dak['receiving_date'];
//                            $data[$i]['dak_subject'] = $dak['dak_subject'];
//                            $data[$i]['dak_security_level'] = $dak['dak_security_level'];
//                            $data[$i]['dak_priority_level'] = $dak['dak_priority_level'];
//                            $data[$i]['dak_cover'] = $dak['dak_cover'];
//                            $data[$i]['dak_description'] = $dak['dak_description'];
//
//                            $data[$i]['receiving_office_id'] = $dak['receiving_office_id'];
//                            $data[$i]['receiving_office_unit_id'] = $dak['receiving_office_unit_id'];
//                            $data[$i]['receiving_office_unit_name'] = $dak['receiving_office_unit_name'];
//                            $data[$i]['receiving_officer_id'] = $dak['receiving_officer_id'];
//                            $data[$i]['receiving_officer_designation_id'] = $dak['receiving_officer_designation_id'];
//                            $data[$i]['receiving_officer_designation_label'] = $dak['receiving_officer_designation_label'];
//                            $data[$i]['receiving_officer_name'] = $dak['receiving_officer_name'];
//
//                            $data[$i]['dak_status'] = $dak['dak_status'];
//                            $data[$i]['created_by'] = $dak['created_by'];
//                            $data[$i]['modified_by'] = $dak['modified_by'];
//                            $data[$i]['uploader_designation_id'] = $dak['uploader_designation_id'];
//                            $data[$i]['created'] = $dak['created'];
//                            $data[$i]['modified'] = $dak['modified'];
//                            $i++;
//                        }
//                    }
//                }
//            }
//        }
//
//        if($isExport=='excel' || $isExport=='pdf'){
//            return $result= Array('data'=> $data,'office_name'=> $office_name['office_name_bng'],'priority_list'=> $priority_list,'security_level_list'=> $security_level_list);
//        }
//
//       $this->set('data',$data);
//       $this->set('priority_list',$priority_list);
//       $this->set('security_level_list',$security_level_list);
//       $this->set('office_name',$office_name);
//    }

    public function resolvedDakListExcel($type='pdf',$startDate = "", $endDate = "",$dateRangeBn='', $unit_id = '') {
        $result = $this->resolvedDakListContent($startDate, $endDate, $type, 20, $unit_id);
        $unitInformation = '';
        $DakList = Array();
        $j=0;

        if (!empty($result['data'])) {

            foreach ($result['data'] as $dak) {
                if( $unitInformation ==  $dak['to_office_unit_name']){
                    $DakList[$j]['to_office_unit_name'] ='';
                }
                else {
                    $DakList[$j]['to_office_unit_name'] = $dak['to_office_unit_name'];
                    $unitInformation = $dak['to_office_unit_name'];
                }

                $created = new Time($dak['dakcreted']);
                $dak['dakcreted'] = $created->i18nFormat(null, null, 'bn-BD');

                $modified = new Time($dak['movecreated']);
                $dak['movecreated'] = $modified->i18nFormat(null, null, 'bn-BD');

                $pending = ProjapotiController::dateDiff(  $created->i18nFormat(null, null, 'En'), $modified->i18nFormat(null, null, 'En'));

                $DakList[$j]['dak_received_no'] = enTobn($dak['dak_received_no']);
                $DakList[$j]['docketing_no'] = enTobn($dak['docketing_no']);
                $DakList[$j]['sender_sarok_no'] = h($dak['sender_sarok_no']);
                $DakList[$j]['dakcreted'] = $dak['dakcreted'];
                $DakList[$j]['dak_subject'] = h($dak['dak_subject']);
                $DakList[$j]['sender_name'] = h($dak['sender_name']) . ', ' . ($dak['sender_officer_designation_label']) . __(!empty($dak['sender_office_unit_name']) ? (", " . $dak['sender_office_unit_name']) : '') . __(!empty($dak['sender_office_name']) ? (", " . $dak['sender_office_name']) : '' );
                $DakList[$j]['receiving_officer_name'] = h($dak['receiving_officer_name'] . ', ' . $dak['receiving_officer_designation_label']) . __(!empty($dak['receiving_office_unit_name']) ? (", " . $dak['receiving_office_unit_name']) : '');
                $DakList[$j]['movecreated'] = $dak['movecreated'];
                $DakList[$j]['dak_security_level'] = h(isset($dak['dak_security_level']) ? $dak['dak_security_level'] : '');
                $DakList[$j]['dak_priority'] = h(isset($dak['dak_priority']) ? $dak['dak_priority'] : '');
                $DakList[$j]['dak_category'] = h(isset($dak['dak_category']) ? $dak['dak_category'] : '');
                $DakList[$j]['pending'] = h($pending);
                $j++;

            }
        }
        if(empty($DakList)){
            $DakList= Array();
        }
        if($type == 'excel'){
            $this->printExcel($DakList, [
                ['key' => 'to_office_unit_name', 'title' => 'শাখা'],
                ['key' => 'dak_received_no', 'title' => 'গ্রহণ নম্বর'],
                ['key' => 'docketing_no', 'title' => 'ডকেট নং'],
                ['key' => 'sender_sarok_no', 'title' => 'স্মারক নম্বর'],
                ['key' => 'dakcreted', 'title' => 'আবেদনের তারিখ'],
                ['key' => 'dak_subject', 'title' => 'বিষয়'],
                ['key' => 'sender_name', 'title' => 'আবেদনকারী'],
                ['key' => 'receiving_officer_name', 'title' => 'মূল প্রাপক'],
                ['key' => 'movecreated', 'title' => 'মীমাংসার তারিখ'],
                ['key' => 'dak_security_level', 'title' => 'গোপনীয়তা'],
                ['key' => 'dak_priority', 'title' => 'অগ্রাধিকার'],
                ['key' => 'dak_category', 'title' => 'সর্বশেষ অবস্থা'],
                ['key' => 'pending', 'title' => 'পেন্ডিং সময়'],
            ],[
                'name'=>((isset($result['office_name']))?$result['office_name'].' অফিসের ':'') .'মীমাংসিত ডাকসমূহ' ,
                'title' => [
                    ((isset($result['office_name']))?$result['office_name'].' অফিসের ':'') .'মীমাংসিত ডাকসমূহ' ,
                    $dateRangeBn,
                ]
            ]);
        } else {
            $this->printPdf($DakList, [
                ['key' => 'to_office_unit_name', 'title' => 'শাখা'],
                ['key' => 'dak_received_no', 'title' => 'গ্রহণ নম্বর'],
                ['key' => 'docketing_no', 'title' => 'ডকেট নং'],
                ['key' => 'sender_sarok_no', 'title' => 'স্মারক নম্বর'],
                ['key' => 'dakcreted', 'title' => 'আবেদনের তারিখ'],
                ['key' => 'dak_subject', 'title' => 'বিষয়'],
                ['key' => 'sender_name', 'title' => 'আবেদনকারী'],
                ['key' => 'receiving_officer_name', 'title' => 'মূল প্রাপক'],
                ['key' => 'movecreated', 'title' => 'মীমাংসার তারিখ'],
                ['key' => 'dak_security_level', 'title' => 'গোপনীয়তা'],
                ['key' => 'dak_priority', 'title' => 'অগ্রাধিকার'],
                ['key' => 'dak_category', 'title' => 'সর্বশেষ অবস্থা'],
                ['key' => 'pending', 'title' => 'পেন্ডিং সময়'],
            ],[
                'name'=>((isset($result['office_name']))?$result['office_name'].' অফিসের ':'') .'মীমাংসিত ডাকসমূহ' ,
                'title' => [
                    ((isset($result['office_name']))?$result['office_name'].' অফিসের ':'') .'মীমাংসিত ডাকসমূহ' ,
                    $dateRangeBn,
                ]
            ]);
        }

            exit;

    }

    public function resolvedDakList() {
        $session = $this->request->session();
        $employee = $session->read('logged_employee_record');
        $this->set("office", $employee);

        $units_list = $this->getOwnUnitList();
        $this->set("units_list", $units_list);
    }
    public function getResolvedDakListContent($options = []){
        /*Set Variables for further query*/
        $startDate = isset($options['startDate'])?$options['startDate']:'';
        $endDate = isset($options['endDate'])?$options['endDate']:'';
        $isExport = isset($options['isExport'])?$options['isExport']:'';
        $per_page = isset($options['per_page'])?$options['per_page']:'';
        $unit_id = isset($options['unit_id'])?$options['unit_id']:'';
        $condition = isset($options['condition'])?$options['condition']:[];
        $priorities = isset($options['priority_list'])?$options['priority_list']+[0=>'']:[];
        $security_levels = isset($options['security_level_list'])?$options['security_level_list']+[0=>'']:[];
        $employee_office = isset($options['selected_office_section'])?$options['selected_office_section']:[];
        $unit_id_array = isset($options['unit_id_array'])?$options['unit_id_array']:[];
        $office_name = isset($options['office_name'])?$options['office_name']:[];
        /*Set Variables for further query*/

        $table_instance_dak_user = TableRegistry::get("DakUsers");
        $table_instance_dak_daptorik = TableRegistry::get("DakDaptoriks");
        $table_instance_DakMovements = TableRegistry::get("DakMovements");
        $officeUnitTable = TableRegistry::get("OfficeUnits");

        //dak user modified field not updated due to updateAll in nothivukto code
        // so use Dak Movements
//        $condition = ["to_office_unit_id IN" => $unit_id_array, "dak_type" => DAK_DAPTORIK,"dak_category" =>DAK_CATEGORY_NOTHIVUKTO];
//        $orCondition = ["to_office_unit_id IN" => $unit_id_array, "dak_type" => DAK_DAPTORIK,"dak_category" =>DAK_CATEGORY_NOTHIJATO];
//        $query = $table_instance_dak_user->find()->select([
//            'dak_id',
//            'dak_type',
//            'dak_category',
//            'to_office_unit_id',
//            'to_office_unit_name'
//        ])
//
//            ->where([$condition])
//            ->orWhere([$orCondition])
//            ->where(function($query) use ($startDate,$endDate){
//                return $query->between('date(modified)',$startDate,$endDate);
//            })
//            ->group(['dak_id','dak_type']);

        $condition = ["to_office_unit_id IN" => $unit_id_array, "dak_type" => DAK_DAPTORIK,"operation_type" =>DAK_CATEGORY_NOTHIVUKTO];
        $orCondition = ["to_office_unit_id IN" => $unit_id_array, "dak_type" => DAK_DAPTORIK,"operation_type" =>DAK_CATEGORY_NOTHIJATO];
        $query = $table_instance_DakMovements
            ->find()
            ->select([
            'dak_id',
            'dak_type',
            'dak_category' => 'operation_type',
            'to_office_unit_id',
            'to_office_unit_name'
        ])
            ->where([$condition])
            ->orWhere([$orCondition])
            ->where(function($query) use ($startDate,$endDate){
                return $query->between('date(modified)',$startDate,$endDate);
            })
            ->group(['dak_id','dak_type']);

        if($isExport=='excel' || $isExport=='pdf'){
            $user_daks = $query->toArray();
        } else {
            $user_daks =$this->Paginator->paginate($query,['limit'=> $per_page]);
        }

        $dp_ids = array();
        foreach ($user_daks as $user_dak) {
            if ($user_dak['dak_type'] == DAK_DAPTORIK) {
                $dp_ids[] = $user_dak['dak_id'];
                $user_dak_list[$user_dak['dak_id']]=$user_dak;
            }
        }

        $daptorik_daks_info_list=$table_instance_dak_daptorik->find()->select([
            'id',
            'docketing_no',
            'dak_received_no',
            'sender_sarok_no',
            'dak_subject',
            'receiving_officer_name',
            'receiving_officer_designation_label',
            'receiving_office_unit_name',
            'sender_name',
            'sender_officer_designation_label',
            'sender_office_unit_name',
            'sender_office_name',
            'receiving_date',
            'dak_security_level',
            'dak_priority_level',
            'modified'
        ])->where(['id IN'=>$dp_ids])->toArray();


        foreach ($daptorik_daks_info_list as $daptorik_daks_info_list_with_dak_users_data){
            $daptorik_daks_info_list_with_dak_users_data['to_office_unit_id']=$user_dak_list[$daptorik_daks_info_list_with_dak_users_data['id']]['to_office_unit_id'];
            $daptorik_daks_info_list_with_dak_users_data['to_office_unit_name']=$user_dak_list[$daptorik_daks_info_list_with_dak_users_data['id']]['to_office_unit_name'];
            $daptorik_daks_info_list_with_dak_users_data['dak_category']=$user_dak_list[$daptorik_daks_info_list_with_dak_users_data['id']]['dak_category'];
        }


        $preorder = '';
        $data = [];
        $i = 0;
        if (!empty($daptorik_daks_info_list)) {
            foreach ($daptorik_daks_info_list as $dak) {
                if (empty($preorder)) {
                    $preorder = $dak['to_office_unit_id'];
                }
                $preorder = $preorder . ',' . $dak['to_office_unit_id'];
            }
            if (!empty($preorder)) {
                $order_unit = $officeUnitTable->getOrderwiseUnits($preorder);
            }
            if (!empty($order_unit)) {
                foreach ($order_unit as $key => $val) {
                    foreach ($daptorik_daks_info_list as $dak) {
                        if ($dak['to_office_unit_id'] == $key) {

                            $data[$i]['to_office_unit_name'] = $dak['to_office_unit_name'];
                            $data[$i]['to_office_unit_id'] = $dak['to_office_unit_id'];
                            $data[$i]['dak_category'] = (($dak['dak_category'] == DAK_CATEGORY_NOTHIJATO)?'নথিজাত করা হয়েছে':'নথিভুক্ত করা হয়েছে');
                            $data[$i]['movecreated'] = $dak['modified'];
                            $data[$i]['dak_priority'] = $priorities[$dak['dak_priority_level']];

                            $data[$i]['sender_sarok_no'] = $dak['sender_sarok_no'];
                            $data[$i]['sender_office_name'] = $dak['sender_office_name'];
                            $data[$i]['sender_office_unit_name'] = $dak['sender_office_unit_name'];
                            $data[$i]['sender_officer_designation_label'] = $dak['sender_officer_designation_label'];
                            $data[$i]['sender_name'] = $dak['sender_name'];

                            $data[$i]['dakcreted'] = $dak['receiving_date'];
                            $data[$i]['dak_received_no'] = $dak['dak_received_no'];
                            $data[$i]['docketing_no'] = $dak['docketing_no'];
                            $data[$i]['dak_subject'] = $dak['dak_subject'];
                            $data[$i]['dak_security_level'] = $security_levels[$dak['dak_security_level']];

                            $data[$i]['receiving_office_unit_name'] = $dak['receiving_office_unit_name'];
                            $data[$i]['receiving_officer_designation_label'] = $dak['receiving_officer_designation_label'];
                            $data[$i]['receiving_officer_name'] = $dak['receiving_officer_name'];



                            $i++;
                        }
                    }
                }
            }
        }


        if($isExport=='excel' || $isExport=='pdf'){
            return $result= Array('data'=>$data,'office_name'=>$office_name['office_name_bng']);
        }

        $this->set('data', $data);

    }
    public function getResolvedDakListContentFromDumpTable($options = []){
        /*Set Variables for further query*/
        $startDate = isset($options['startDate'])?$options['startDate']:'';
        $endDate = isset($options['endDate'])?$options['endDate']:'';
        $isExport = isset($options['isExport'])?$options['isExport']:'';
        $per_page = isset($options['per_page'])?$options['per_page']:'';
        $unit_id = isset($options['unit_id'])?$options['unit_id']:'';
        $condition = isset($options['condition'])?$options['condition']:[];
        $priorities = isset($options['priority_list'])?$options['priority_list']:[];
        $security_levels = isset($options['security_level_list'])?$options['security_level_list']:[];
        $employee_office = isset($options['selected_office_section'])?$options['selected_office_section']:[];
        $unit_id_array = isset($options['unit_id_array'])?$options['unit_id_array']:[];
        $office_name = isset($options['office_name'])?$options['office_name']:[];
        /*Set Variables for further query*/

        $table_instance_DakRegister = TableRegistry::get("DakRegister");
        $officeUnitTable = TableRegistry::get("OfficeUnits");

        $condition = ["from_office_unit_id IN" => $unit_id_array, "dak_type" => DAK_DAPTORIK,"operation_type" =>DAK_CATEGORY_NOTHIVUKTO];
        $orCondition = ["from_office_unit_id IN" => $unit_id_array, "dak_type" => DAK_DAPTORIK,"operation_type" =>DAK_CATEGORY_NOTHIJATO];
        $query = $table_instance_DakRegister
            ->find()
            ->select([
                'dak_id',
                'dak_type',
                'dak_category' => 'operation_type',
                'from_office_unit_id',
                'from_office_unit_name',

                'docketing_no',
                'dak_received_no',
                'sender_sarok_no',
                'dak_subject',

                'receiving_officer_name',
                'receiving_officer_designation_label',
                'receiving_office_unit_name',

                'sender_name',
                'sender_officer_designation_label',
                'sender_office_name',
                'sender_office_name',

                'receiving_date',
                'dak_security_level',
                'dak_priority_level',
                'movement_time',

                'dak_created',
                'receiving_date',
                'sender_sarok_no',

            ])
            ->where([$condition])
            ->orWhere([$orCondition])
            ->where(function($query) use ($startDate,$endDate){
                return $query->between('date(movement_time)',$startDate,$endDate);
            })
            ->group(['dak_id','dak_type']);

        if($isExport=='excel' || $isExport=='pdf'){
            $user_daks = $query->toArray();
        } else {
            $user_daks =$this->Paginator->paginate($query,['limit'=> $per_page]);
        }

        $preorder = '';
        $data = [];
        $i = 0;
        if (!empty($user_daks)) {
            foreach ($user_daks as $dak) {
                if (empty($preorder)) {
                    $preorder = $dak['from_office_unit_id'];
                }
                $preorder = $preorder . ',' . $dak['from_office_unit_id'];
            }
            if (!empty($preorder)) {
                $order_unit = $officeUnitTable->getOrderwiseUnits($preorder);
            }
            if (!empty($order_unit)) {
                foreach ($order_unit as $key => $val) {
                    foreach ($user_daks as $dak) {
                        if ($dak['from_office_unit_id'] == $key) {

                            $data[$i]['to_office_unit_name'] = $dak['from_office_unit_name'];
                            $data[$i]['to_office_unit_id'] = $dak['from_office_unit_id'];
                            $data[$i]['dak_category'] = (($dak['dak_category'] == DAK_CATEGORY_NOTHIJATO)?'নথিজাত করা হয়েছে':'নথিভুক্ত করা হয়েছে');
                            $data[$i]['movecreated'] = $dak['movement_time'];
                            $data[$i]['dak_priority'] = $priorities[$dak['dak_priority_level']];

                            $data[$i]['sender_sarok_no'] = $dak['sender_sarok_no'];
                            $data[$i]['sender_office_name'] = $dak['sender_office_name'];
                            $data[$i]['sender_office_unit_name'] = $dak['sender_office_unit_name'];
                            $data[$i]['sender_officer_designation_label'] = $dak['sender_officer_designation_label'];
                            $data[$i]['sender_name'] = $dak['sender_name'];

                            $data[$i]['dakcreted'] = $dak['receiving_date'];
                            $data[$i]['dak_received_no'] = $dak['dak_received_no'];
                            $data[$i]['docketing_no'] = $dak['docketing_no'];
                            $data[$i]['dak_subject'] = $dak['dak_subject'];
                            $data[$i]['dak_security_level'] = $security_levels[$dak['dak_security_level']];

                            $data[$i]['receiving_office_unit_name'] = $dak['receiving_office_unit_name'];
                            $data[$i]['receiving_officer_designation_label'] = $dak['receiving_officer_designation_label'];
                            $data[$i]['receiving_officer_name'] = $dak['receiving_officer_name'];



                            $i++;
                        }
                    }
                }
            }
        }


        if($isExport=='excel' || $isExport=='pdf'){
            return $result= Array('data'=>$data,'office_name'=>$office_name['office_name_bng']);
        }

        $this->set('data', $data);

    }
    public function resolvedDakListContent($startDate = "", $endDate = "",$isExport="",$per_page = 20 ,$unit_id = "",$version = 1) {
        $this->layout = null;
        $employee_office = $this->getCurrentDakSection();

        $officeUnitTable = TableRegistry::get("OfficeUnits");

        $units = $officeUnitTable->getOfficeChildInfo($employee_office['office_unit_id']);
        $unitsOwn = $officeUnitTable->getOwnChildInfo($employee_office['office_unit_id']);

        $units = !empty($units[0]) ? $units[0] : '';
        $unitsOwn = !empty($unitsOwn[0]) ? ($employee_office['office_unit_id'] . ',' . $unitsOwn[0]) : $employee_office['office_unit_id'];

        $units = $units . (!empty($unitsOwn) ? ((!empty($units) ? ',' : '') . $unitsOwn) : '');
        $unit_id_array = explode(",",$units);
        if(!empty($unit_id) && in_array((int)$unit_id,$unit_id_array)){
            $unit_id_array = (int)$unit_id;
        }


        $startDate = !empty($startDate)?h($startDate):date('Y-m-d');
        $endDate = !empty($endDate)?h($endDate):date('Y-m-d');

        $office = TableRegistry::get('Offices');
        $office_name = $office->find()->select(['office_name_bng', 'office_address'])->where(['id' => $employee_office['office_id']])->first();
        $this->set('office_name', $office_name);

        $condition = [];
        $priorities = jsonA(DAK_PRIORITY_TYPE);

        $security_levels = jsonA(DAK_SECRECY_TYPE);

        /*Set Variables for further query*/
        $function_options = [];
        $function_options['startDate'] = $startDate;
        $function_options['endDate'] = $endDate;
        $function_options['isExport'] = $isExport;
        $function_options['per_page'] = $per_page;
        $function_options['unit_id'] = $unit_id;
        $function_options['condition'] = $condition;
        $function_options['priority_list'] = $priorities;
        $function_options['security_level_list'] = $security_levels;
        $function_options['selected_office_section'] = $employee_office;
        $function_options['unit_id_array'] = $unit_id_array;
        $function_options['office_name'] = $office_name;
        /*Set Variables for further query*/


        $response = ''; //for export
        if($version == 1){
            $response =  $this->getResolvedDakListContent($function_options);
        }
        else if($version == 2){
            $response = $this->getResolvedDakListContentFromDumpTable($function_options);
        }
        if(!empty($isExport) && ($isExport=='excel' || $isExport=='pdf')){
            return $response;
        }

        $this->set(compact('security_level_list'));
        $this->set(compact('priority_list'));
        $this->set(compact('version'));


    }
//    public function resolvedDakListContent($startDate = "", $endDate = "",$isExport='') {
//        $this->layout = null;
//        $condition = ' 1 ';
//        $employee_office = $this->getCurrentDakSection();
//        $table_instance_dak_user = TableRegistry::get("DakUsers");
//        $table_instance_dak_daptorik = TableRegistry::get("DakDaptoriks");
////
//        $officeUnitTable = TableRegistry::get("OfficeUnits");
////
//        $units = $officeUnitTable->getOfficeChildInfo($employee_office['office_unit_id']);
//        $unitsOwn = $officeUnitTable->getOwnChildInfo($employee_office['office_unit_id']);
//
//        $units = !empty($units[0]) ? $units[0] : '';
//        $unitsOwn = !empty($unitsOwn[0]) ? ($employee_office['office_unit_id'] . ',' . $unitsOwn[0]) : $employee_office['office_unit_id'];
//
//        $units = $units . (!empty($unitsOwn) ? ((!empty($units) ? ',' : '') . $unitsOwn) : '');
//
//
//        $startDate = !empty($startDate)?h($startDate):'';
//        $endDate = !empty($endDate)?h($endDate):'';
//
//        if (!empty($startDate) And ! empty($endDate)) {
//            if (!empty($condition))
//                $condition .= ' AND ';
//            $condition .= " DATE(modified)  BETWEEN  '{$startDate}' AND '{$endDate}' ";
//        } else {
//            $startDate = date('Y-m-d');
//            $endDate = date('Y-m-d');
//            if (!empty($condition))
//                $condition .= ' AND ';
//            $condition .= " DATE(modified)  BETWEEN  '{$startDate}' AND '{$endDate}' ";
//        }
//        $condition .= "AND to_office_unit_id IN ({$units}) AND dak_type = '".DAK_DAPTORIK."' AND (dak_category = '".DAK_CATEGORY_NOTHIVUKTO."' OR dak_category = '".DAK_CATEGORY_NOTHIJATO."')";
//
//        if($isExport=='excel' || $isExport=='pdf'){
//            $user_daks =$table_instance_dak_user->find()->select(['dak_id', 'dak_type','dak_category','to_office_unit_id','to_office_unit_name'])->where([$condition])->group(['dak_id','dak_type'])->toArray();
//        } else {
//            pr($table_instance_dak_user->find()->select(['dak_id', 'dak_type','dak_category','to_office_unit_id','to_office_unit_name'])->where([$condition])->group(['dak_id','dak_type'])); die;
//        }
//
//        $dp_ids = array();
//        foreach ($user_daks as $user_dak) {
//            if ($user_dak['dak_type'] == DAK_DAPTORIK) {
//                $dp_ids[] = $user_dak['dak_id'];
//                $user_dak_list[$user_dak['dak_id']]=$user_dak;
//            }
//        }
//
//        $daptorik_daks_info_list=$table_instance_dak_daptorik->find()->select([
//            'id',
//            'docketing_no',
//            'dak_received_no',
//            'sender_sarok_no',
//            'dak_subject',
//            'receiving_officer_name',
//            'receiving_officer_designation_label',
//            'receiving_office_unit_name',
//            'sender_name',
//            'sender_officer_designation_label',
//            'sender_office_unit_name',
//            'sender_office_name',
//            'receiving_date',
//            'dak_security_level',
//            'dak_priority_level',
//            'modified'
//        ])->where(['id IN'=>$dp_ids])->toArray();
//
//
//        foreach ($daptorik_daks_info_list as $daptorik_daks_info_list_with_dak_users_data){
//            $daptorik_daks_info_list_with_dak_users_data['to_office_unit_id']=$user_dak_list[$daptorik_daks_info_list_with_dak_users_data['id']]['to_office_unit_id'];
//            $daptorik_daks_info_list_with_dak_users_data['to_office_unit_name']=$user_dak_list[$daptorik_daks_info_list_with_dak_users_data['id']]['to_office_unit_name'];
//            $daptorik_daks_info_list_with_dak_users_data['dak_category']=$user_dak_list[$daptorik_daks_info_list_with_dak_users_data['id']]['dak_category'];
//        }
//        $priorities = json_decode(DAK_PRIORITY_TYPE,true);
//
//        $security_levels = json_decode(DAK_SECRECY_TYPE,true);
//
//        $preorder = '';
//        $data = [];
//        $i = 0;
//        if (!empty($daptorik_daks_info_list)) {
//            foreach ($daptorik_daks_info_list as $dak) {
//                if (empty($preorder)) {
//                    $preorder = $dak['to_office_unit_id'];
//                }
//                $preorder = $preorder . ',' . $dak['to_office_unit_id'];
//            }
//            if (!empty($preorder)) {
//                $order_unit = $officeUnitTable->getOrderwiseUnits($preorder);
//            }
//            if (!empty($order_unit)) {
//                foreach ($order_unit as $key => $val) {
//                    foreach ($daptorik_daks_info_list as $dak) {
//                        if ($dak['to_office_unit_id'] == $key) {
//
//                            $data[$i]['to_office_unit_name'] = $dak['to_office_unit_name'];
//                            $data[$i]['to_office_unit_id'] = $dak['to_office_unit_id'];
//                            $data[$i]['dak_category'] = (($dak['dak_category'] == DAK_CATEGORY_NOTHIJATO)?'নথিজাত করা হয়েছে':'নথিভুক্ত করা হয়েছে');
//                            $data[$i]['movecreated'] = $dak['modified'];
//                            $data[$i]['dak_priority'] = $priorities[$dak['dak_priority_level']];
//
//                            $data[$i]['sender_sarok_no'] = $dak['sender_sarok_no'];
//                            $data[$i]['sender_office_name'] = $dak['sender_office_name'];
//                            $data[$i]['sender_office_unit_name'] = $dak['sender_office_unit_name'];
//                            $data[$i]['sender_officer_designation_label'] = $dak['sender_officer_designation_label'];
//                            $data[$i]['sender_name'] = $dak['sender_name'];
//
//                            $data[$i]['dakcreted'] = $dak['receiving_date'];
//                            $data[$i]['dak_received_no'] = $dak['dak_received_no'];
//                            $data[$i]['docketing_no'] = $dak['docketing_no'];
//                            $data[$i]['dak_subject'] = $dak['dak_subject'];
//                            $data[$i]['dak_security_level'] = $security_levels[$dak['dak_security_level']];
//
//                            $data[$i]['receiving_office_unit_name'] = $dak['receiving_office_unit_name'];
//                            $data[$i]['receiving_officer_designation_label'] = $dak['receiving_officer_designation_label'];
//                            $data[$i]['receiving_officer_name'] = $dak['receiving_officer_name'];
//
//
//
//                            $i++;
//                        }
//                    }
//                }
//            }
//        }
//
//        $office = TableRegistry::get('Offices');
//        $office_name = $office->find()->select(['office_name_bng', 'office_address'])->where(['id' => $employee_office['office_id']])->first();
//        if($isExport=='excel' || $isExport=='pdf'){
//            return $result= Array('data'=>$data,'office_name'=>$office_name['office_name_bng']);
//        }
//
//        $this->set('data', $data);
//        $this->set('office_name', $office_name);
//    }

    public function potrojariDakListExcel($type='pdf',$startDate = "", $endDate = "",$dateRangeBn='',$unit_id='') {
        $result = $this->potrojariDakListContent($startDate, $endDate, $type,20, $unit_id);
        $unitInformation='';
        $DakList = Array();
        $j=0;

        if (!empty($result['data'])) {

            foreach ($result['data'] as $dak) {
                if( $unitInformation ==  $dak['office_unit_name']){
                    $DakList[$j]['office_unit_name'] ='';
                }
                else {
                    $DakList[$j]['office_unit_name'] = $dak['office_unit_name'];
                    $unitInformation = $dak['office_unit_name'];
                }

                $DakList[$j]['docketing_no'] = enTobn($dak['DakDaptoriks']['docketing_no']);
                $DakList[$j]['sender_sarok_no'] = h($dak['DakDaptoriks']['sender_sarok_no']);
                $DakList[$j]['potrojari_date'] = h($dak['potrojari_date']);
                $DakList[$j]['potro_subject'] = h($dak['potro_subject']);
                $DakList[$j]['officer_name'] = h($dak['officer_name'] . ', ' . $dak['officer_designation_label']) . __(!empty($dak['office_unit_name']) ? (", " . $dak['office_unit_name']) : '') . __(!empty($dak['office_name']) ? (", " . $dak['office_name']) : '' );
                $DakList[$j]['created'] =h($dak['DakDaptoriks']['created']);
                $DakList[$j]['potro_security_level'] = h(isset($result['security_level_list'][$dak['potro_security_level']]) ? $result['security_level_list'][$dak['potro_security_level']] : '');
                $DakList[$j]['potro_priority_level'] = h(isset($result['priority_list'][$dak['potro_priority_level']]) ? $result['priority_list'][$dak['potro_priority_level']] : '');

                $j++;

            }
        }
        if(empty($DakList)){
            $DakList= Array();
        }
        if($type == 'excel'){
            $this->printExcel($DakList, [
                ['key' => 'office_unit_name', 'title' => 'শাখা'],
                ['key' => 'docketing_no', 'title' => 'গ্রহণ নম্বর'],
                ['key' => 'sender_sarok_no', 'title' => 'স্মারক নম্বর'],
                ['key' => 'potrojari_date', 'title' => 'আবেদনের তারিখ'],
                ['key' => 'potro_subject', 'title' => 'বিষয়'],
                ['key' => 'officer_name', 'title' => 'আবেদনকারী'],
                ['key' => 'created', 'title' => 'পত্রজারির তারিখ'],
                ['key' => 'potro_security_level', 'title' => 'গোপনীয়তা'],
                ['key' => 'potro_priority_level', 'title' => 'অগ্রাধিকার'],
            ],[
                'name'=>((isset($result['office_name']))?$result['office_name'].' অফিসের ':'') .'জারিকৃত ডাকসমূহ' ,
                'title' => [
                    ((isset($result['office_name']))?$result['office_name'].' অফিসের ':'') .'জারিকৃত ডাকসমূহ' ,
                    $dateRangeBn,
                ]
            ]);
        } else {
            $this->printPdf($DakList, [
                ['key' => 'office_unit_name', 'title' => 'শাখা'],
                ['key' => 'docketing_no', 'title' => 'গ্রহণ নম্বর'],
                ['key' => 'sender_sarok_no', 'title' => 'স্মারক নম্বর'],
                ['key' => 'potrojari_date', 'title' => 'আবেদনের তারিখ'],
                ['key' => 'potro_subject', 'title' => 'বিষয়'],
                ['key' => 'officer_name', 'title' => 'আবেদনকারী'],
                ['key' => 'created', 'title' => 'পত্রজারির তারিখ'],
                ['key' => 'potro_security_level', 'title' => 'গোপনীয়তা'],
                ['key' => 'potro_priority_level', 'title' => 'অগ্রাধিকার'],
            ],[
                'name'=>((isset($result['office_name']))?$result['office_name'].' অফিসের ':'') .'জারিকৃত ডাকসমূহ' ,
                'title' => [
                    ((isset($result['office_name']))?$result['office_name'].' অফিসের ':'') .'জারিকৃত ডাকসমূহ' ,
                    $dateRangeBn,
                ]
            ]);
        }

        exit;
    }

    public function potrojariDakList() {
        $session = $this->request->session();
        $employee = $session->read('logged_employee_record');
        $this->set("office", $employee);

        $units_list = $this->getOwnUnitList();
        $this->set("units_list", $units_list);
    }
    private function generatePotrojariDakListContent($options = []){
        /*Set Variables for further query*/
        $startDate = isset($options['startDate'])?$options['startDate']:'';
        $endDate = isset($options['endDate'])?$options['endDate']:'';
        $isExport = isset($options['isExport'])?$options['isExport']:'';
        $per_page = isset($options['per_page'])?$options['per_page']:'';
        $unit_id = isset($options['unit_id'])?$options['unit_id']:'';
        $condition = isset($options['condition'])?$options['condition']:[];
        $priority_list = isset($options['priority_list'])?$options['priority_list']:[];
        $security_level_list = isset($options['security_level_list'])?$options['security_level_list']:[];
        $selected_office_section = isset($options['selected_office_section'])?$options['selected_office_section']:[];
        $unit_id_array = isset($options['unit_id_array'])?$options['unit_id_array']:[];
        $office_name = isset($options['office_name'])?$options['office_name']:[];
        /*Set Variables for further query*/

        $potrojari_table = TableRegistry::get("Potrojari");

        $officeUnitTable = TableRegistry::get("OfficeUnits");
        $query =$potrojari_table->find()->select([
            'DakDaptoriks.id',
            'DakDaptoriks.sender_sarok_no',
            'DakDaptoriks.created',
            'officer_name',
            'office_unit_name',
            'office_unit_id',
            'officer_designation_label',
            'office_name',
            'potro_subject',
            'DakDaptoriks.docketing_no',
            'potro_security_level',
            'potro_priority_level',
            'potrojari_date',
            'PotrojariOnulipi.dak_id',
            'PotrojariReceiver.dak_id'
        ])
        ->join([
            'PotrojariReceiver' => [
                'table' => 'potrojari_receiver',
                'type' => 'Left',
                'conditions' => [
                    'PotrojariReceiver.potrojari_id = Potrojari.id'
                ]
            ],
            'PotrojariOnulipi' => [
                'table' => 'potrojari_onulipi',
                'type' => 'Left',
                'conditions' => [
                    'PotrojariOnulipi.potrojari_id = Potrojari.id'
                ]
            ],
            'DakDaptoriks' => [
                'table' => 'dak_daptoriks',
                'type' => 'INNER',
                'conditions' => [
                    'DakDaptoriks.id = PotrojariReceiver.dak_id OR DakDaptoriks.id = PotrojariOnulipi.dak_id '
                ]
            ]
        ])
        ->where(["Potrojari.potro_status" => DAK_CATEGORY_SENT])
        ->where(["Potrojari.office_unit_id IN" => $unit_id_array])
        ->where(function($query) use ($startDate,$endDate){
            return $query->between('date(DakDaptoriks.created)',$startDate,$endDate);
        })
        ->group(['Potrojari.id'])
        ->order(['DakDaptoriks.created' => 'DESC']);

        if($isExport=='excel' || $isExport=='pdf'){
            $grohon_daks = $query->toArray();
        }
        else {
            $grohon_daks = $this->Paginator->paginate($query, ['limit' => $per_page]);
        }

        $preorder = '';
        $data = [];
        $i = 0;
        if (!empty($grohon_daks)) {
            foreach ($grohon_daks as $dak) {
                if (empty($preorder)) {
                    $preorder = $dak['office_unit_id'];
                }
                $preorder = $preorder . ',' . $dak['office_unit_id'];
            }
            if (!empty($preorder)) {
                $order_unit = $officeUnitTable->getOrderwiseUnits($preorder);
            }
            if (!empty($order_unit)) {
                foreach ($order_unit as $key => $val) {
                    foreach ($grohon_daks as $dak) {
                        if ($dak['office_unit_id'] == $key) {

                            $data[$i]['officer_name'] = $dak['officer_name'];
                            $data[$i]['office_unit_name'] = $dak['office_unit_name'];
                            $data[$i]['office_unit_id'] = $dak['office_unit_id'];
                            $data[$i]['officer_designation_label'] = $dak['officer_designation_label'];

                            $data[$i]['sender_officer_id'] = $dak['sender_officer_id'];

                            $data[$i]['office_name'] = $dak['office_name'];

                            $data[$i]['potro_subject'] = $dak['potro_subject'];
                            $data[$i]['potro_security_level'] = $dak['potro_security_level'];
                            $data[$i]['potro_priority_level'] = $dak['potro_priority_level'];

                            $data[$i]['potrojari_date'] = Time::parse($dak['potrojari_date']);

                            $data[$i]['DakDaptoriks']['id'] = $dak['DakDaptoriks']['id'];
                            $data[$i]['DakDaptoriks']['sender_sarok_no'] = $dak['DakDaptoriks']['sender_sarok_no'];
                            $data[$i]['DakDaptoriks']['created'] = Time::parse($dak['DakDaptoriks']['created']);
                            $data[$i]['DakDaptoriks']['docketing_no'] = $dak['DakDaptoriks']['docketing_no'];


                            $data[$i]['PotrojariOnulipi']['dak_id'] = $dak['PotrojariOnulipi']['dak_id'];

                            $data[$i]['PotrojariReceiver']['dak_id'] = $dak['PotrojariReceiver']['dak_id'];

                            $i++;
                        }
                    }
                }
            }
        }
        if($isExport=='excel' || $isExport=='pdf'){
            return $result= Array('data'=> $data,'office_name'=> $office_name['office_name_bng'],'priority_list'=> $priority_list,'security_level_list'=> $security_level_list);
        }
        $this->set('data', $data);
    }

    public function potrojariDakListContent($startDate = "", $endDate = "",$isExport="",$per_page = 20 ,$unit_id = "",$version = 1) {
        set_time_limit(0);
        $this->layout = null;
        $employee_office = $this->getCurrentDakSection();
        $employee_section_id = $employee_office['office_unit_id'];

        $potrojari_table = TableRegistry::get("Potrojari");

        $officeUnitTable = TableRegistry::get("OfficeUnits");

        $units = $officeUnitTable->getOfficeChildInfo($employee_office['office_unit_id']);
        $unitsOwn = $officeUnitTable->getOwnChildInfo($employee_office['office_unit_id']);

        $units = !empty($units[0]) ? $units[0] : '';
        $unitsOwn = !empty($unitsOwn[0]) ? ($employee_office['office_unit_id'] . ',' . $unitsOwn[0]) : $employee_office['office_unit_id'];

        $units = $units . (!empty($unitsOwn) ? ((!empty($units) ? ',' : '') . $unitsOwn) : '');
        $unit_id_array = explode(",",$units);
        if(!empty($unit_id) && in_array((int)$unit_id,$unit_id_array)){
            $unit_id_array = (int)$unit_id;
        }

        $startDate = !empty($startDate)?$startDate:date('Y-m-d');
        $endDate = !empty($endDate)?h($endDate):date('Y-m-d');

        $priority_list = json_decode(DAK_PRIORITY_TYPE,TRUE);
        $security_level_list = json_decode(DAK_SECRECY_TYPE, TRUE);

        $office = TableRegistry::get('Offices');
        $office_name = $office->find()->select(['office_name_bng', 'office_address'])->where(['id' => $employee_office['office_id']])->first();

        $condition = [];

        /*Set Variables for further query*/
        $function_options = [];
        $function_options['startDate'] = $startDate;
        $function_options['endDate'] = $endDate;
        $function_options['isExport'] = $isExport;
        $function_options['per_page'] = $per_page;
        $function_options['unit_id'] = $unit_id;
        $function_options['condition'] = $condition;
        $function_options['priority_list'] = $priority_list;
        $function_options['security_level_list'] = $security_level_list;
        $function_options['selected_office_section'] = $employee_office;
        $function_options['unit_id_array'] = $unit_id_array;
        $function_options['office_name'] = $office_name;
        /*Set Variables for further query*/

        $response = ''; //for export
        if($version == 1){
            $response =  $this->generatePotrojariDakListContent($function_options);
        }
        else if($version == 2){
            $response = $this->generatePotrojariDakListContentFromDumpTable($function_options);
        }
        if(!empty($isExport) && ($isExport=='excel' || $isExport=='pdf')){
            return $response;
        }

        $this->set(compact('security_level_list','priority_list','version','office_name'));
    }
    private function generatePotrojariDakListContentFromDumpTable($options = []){
        /*Set Variables for further query*/
        $startDate = isset($options['startDate'])?$options['startDate']:'';
        $endDate = isset($options['endDate'])?$options['endDate']:'';
        $isExport = isset($options['isExport'])?$options['isExport']:'';
        $per_page = isset($options['per_page'])?$options['per_page']:'';
        $unit_id = isset($options['unit_id'])?$options['unit_id']:'';
        $condition = isset($options['condition'])?$options['condition']:[];
        $priority_list = isset($options['priority_list'])?$options['priority_list']:[];
        $security_level_list = isset($options['security_level_list'])?$options['security_level_list']:[];
        $selected_office_section = isset($options['selected_office_section'])?$options['selected_office_section']:[];
        $unit_id_array = isset($options['unit_id_array'])?$options['unit_id_array']:[];
        $office_name = isset($options['office_name'])?$options['office_name']:[];
        /*Set Variables for further query*/

        $dakRegistersTable = TableRegistry::get('DakRegister');
        $officeUnitTable = TableRegistry::get('OfficeUnits');

        $query = $dakRegistersTable->generatePotrojariDakList($options);

        if($isExport=='excel' || $isExport=='pdf'){
            $grohon_daks = $query->toArray();
        }
        else {
            $grohon_daks = $this->Paginator->paginate($query, ['limit' => $per_page]);
        }

        $preorder = '';
        $data = [];
        $i = 0;
        if (!empty($grohon_daks)) {
            foreach ($grohon_daks as $dak) {
                if (empty($preorder)) {
                    $preorder = $dak['office_unit_id'];
                }
                $preorder = $preorder . ',' . $dak['office_unit_id'];
            }
            if (!empty($preorder)) {
                $order_unit = $officeUnitTable->getOrderwiseUnits($preorder);
            }
            if (!empty($order_unit)) {
                foreach ($order_unit as $key => $val) {
                    foreach ($grohon_daks as $dak) {
                        if ($dak['office_unit_id'] == $key) {

                            $data[$i]['officer_name'] = $dak['officer_name'];
                            $data[$i]['office_unit_name'] = $dak['office_unit_name'];
                            $data[$i]['office_unit_id'] = $dak['office_unit_id'];
                            $data[$i]['officer_designation_label'] = $dak['officer_designation_label'];

                            $data[$i]['sender_officer_id'] = $dak['sender_officer_id'];

                            $data[$i]['office_name'] = $dak['office_name'];

                            $data[$i]['potro_subject'] = $dak['potro_subject'];
                            $data[$i]['potro_security_level'] = $dak['potro_security_level'];
                            $data[$i]['potro_priority_level'] = $dak['potro_priority_level'];

                            $data[$i]['potrojari_date'] = Time::parse($dak['potrojari_date']);

                            $data[$i]['DakDaptoriks']['id'] = $dak['dak_id'];
                            $data[$i]['DakDaptoriks']['sender_sarok_no'] = $dak['sender_sarok_no'];
                            $data[$i]['DakDaptoriks']['created'] = Time::parse($dak['created']);
                            $data[$i]['DakDaptoriks']['docketing_no'] = $dak['docketing_no'];
                            $data[$i]['DakDaptoriks']['dak_received_no'] = $dak['dak_received_no'];

                            $i++;
                        }
                    }
                }
            }
        }
        if($isExport=='excel' || $isExport=='pdf'){
            return $result= Array('data'=> $data,'office_name'=> $office_name['office_name_bng'],'priority_list'=> $priority_list,'security_level_list'=> $security_level_list);
        }
        $this->set('data', $data);
    }
//    public function potrojariDakListContent($startDate = "", $endDate = "",$isExport='') {
//        $this->layout = null;
//        $condition = '1 ';
//        $employee_office = $this->getCurrentDakSection();
//        $employee_section_id = $employee_office['office_unit_id'];
//
//        $potrojari_table = TableRegistry::get("Potrojari");
//
//        $officeUnitTable = TableRegistry::get("OfficeUnits");
//
//        $units = $officeUnitTable->getOfficeChildInfo($employee_office['office_unit_id']);
//        $unitsOwn = $officeUnitTable->getOwnChildInfo($employee_office['office_unit_id']);
//
//        $units = !empty($units[0]) ? $units[0] : '';
//        $unitsOwn = !empty($unitsOwn[0]) ? ($employee_office['office_unit_id'] . ',' . $unitsOwn[0]) : $employee_office['office_unit_id'];
//
//        $units = $units . (!empty($unitsOwn) ? ((!empty($units) ? ',' : '') . $unitsOwn) : '');
//        $condition .= " AND Potrojari.office_unit_id IN ({$units})";
//
//        $startDate = !empty($startDate)?$startDate:'';
//        $endDate = !empty($endDate)?h($endDate):'';
//
//        if (!empty($startDate) And ! empty($endDate)) {
//            if (!empty($condition))
//                $condition .= ' AND ';
//            $condition .= " DakDaptoriks.created  BETWEEN  '{$startDate} 00:00:00' AND '{$endDate}
//            23:59:59' ";
//        } else {
//            $startDate = date('Y-m-d');
//            $endDate = date('Y-m-d');
//            if (!empty($condition))
//                $condition .= ' AND ';
//            $condition .= " DakDaptoriks.created  BETWEEN  '{$startDate} 00:00:00' AND '{$endDate}
//            23:59:59' ";
//        }
//
//
//        $priority_list = json_decode(DAK_PRIORITY_TYPE,TRUE);
//
//        $security_level_list = json_decode(DAK_SECRECY_TYPE, TRUE);
//
//        $query =$potrojari_table->find()->select(['DakDaptoriks.id', 'DakDaptoriks.sender_sarok_no', 'DakDaptoriks.created', 'officer_name', 'office_unit_name', 'office_unit_id', 'officer_designation_label', 'office_name', 'potro_subject', 'DakDaptoriks.docketing_no', 'potro_security_level', 'potro_priority_level', 'potrojari_date', 'PotrojariOnulipi.dak_id', 'PotrojariReceiver.dak_id'
//                        ])
//                        ->join([
//
//                            'PotrojariReceiver' => [
//                                'table' => 'potrojari_receiver',
//                                'type' => 'Left',
//                                'conditions' => [
//                                    'PotrojariReceiver.potrojari_id = Potrojari.id'
//                                ]
//                            ],
//                            'PotrojariOnulipi' => [
//                                'table' => 'potrojari_onulipi',
//                                'type' => 'Left',
//                                'conditions' => [
//                                    'PotrojariOnulipi.potrojari_id = Potrojari.id'
//                                ]
//                            ],
//                            'DakDaptoriks' => [
//                                'table' => 'dak_daptoriks',
//                                'type' => 'INNER',
//                                'conditions' => [
//                                    'DakDaptoriks.id = PotrojariReceiver.dak_id OR DakDaptoriks.id = PotrojariOnulipi.dak_id '
//                                ]
//                            ]
//                        ])
//                        ->where(["Potrojari.potro_status" => DAK_CATEGORY_SENT])
//                        ->group(['Potrojari.id'])
//                        ->where([$condition])
//                        ->order(['DakDaptoriks.created' => 'DESC']);
//
//        if($isExport=='excel' || $isExport=='pdf'){
//            $grohon_daks = $query->toArray();
//        }
//        else {
//            $grohon_daks = $this->Paginator->paginate($query, ['limit' => 20]);
//        }
//
//        $preorder = '';
//        $data = [];
//        $i = 0;
//        if (!empty($grohon_daks)) {
//            foreach ($grohon_daks as $dak) {
//                if (empty($preorder)) {
//                    $preorder = $dak['office_unit_id'];
//                }
//                $preorder = $preorder . ',' . $dak['office_unit_id'];
//            }
//            if (!empty($preorder)) {
//                $order_unit = $officeUnitTable->getOrderwiseUnits($preorder);
//            }
//            if (!empty($order_unit)) {
//                foreach ($order_unit as $key => $val) {
//                    foreach ($grohon_daks as $dak) {
//                        if ($dak['office_unit_id'] == $key) {
//
//                            $data[$i]['officer_name'] = $dak['officer_name'];
//                            $data[$i]['office_unit_name'] = $dak['office_unit_name'];
//                            $data[$i]['office_unit_id'] = $dak['office_unit_id'];
//                            $data[$i]['officer_designation_label'] = $dak['officer_designation_label'];
//
//                            $data[$i]['sender_officer_id'] = $dak['sender_officer_id'];
//
//                            $data[$i]['office_name'] = $dak['office_name'];
//
//                            $data[$i]['potro_subject'] = $dak['potro_subject'];
//                            $data[$i]['potro_security_level'] = $dak['potro_security_level'];
//                            $data[$i]['potro_priority_level'] = $dak['potro_priority_level'];
//
//                            $data[$i]['potrojari_date'] = Time::parse($dak['potrojari_date']);
//
//                            $data[$i]['DakDaptoriks']['id'] = $dak['DakDaptoriks']['id'];
//                            $data[$i]['DakDaptoriks']['sender_sarok_no'] = $dak['DakDaptoriks']['sender_sarok_no'];
//                            $data[$i]['DakDaptoriks']['created'] = Time::parse($dak['DakDaptoriks']['created']);
//                            $data[$i]['DakDaptoriks']['docketing_no'] = $dak['DakDaptoriks']['docketing_no'];
//
//
//                            $data[$i]['PotrojariOnulipi']['dak_id'] = $dak['PotrojariOnulipi']['dak_id'];
//
//                            $data[$i]['PotrojariReceiver']['dak_id'] = $dak['PotrojariReceiver']['dak_id'];
//
//                            $i++;
//                        }
//                    }
//                }
//            }
//        }
//
//        $office = TableRegistry::get('Offices');
//        $office_name = $office->find()->select(['office_name_bng', 'office_address'])->where(['id' => $employee_office['office_id']])->first();
//
//        if($isExport=='excel' || $isExport=='pdf'){
//            return $result= Array('data'=> $data,'office_name'=> $office_name['office_name_bng'],'priority_list'=> $priority_list,'security_level_list'=> $security_level_list);
//        }
//        $this->set('security_level_list', $security_level_list);
//        $this->set('priority_list', $priority_list);
//        $this->set('data', $data);
//        $this->set('office_name', $office_name);
//    }

}