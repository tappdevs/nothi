<?php

namespace App\Controller;

use App\Controller\ProjapotiController;
use Cake\Event\Event;
use Cake\Filesystem\Folder;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\I18n\Number;
use Cake\I18n\Time;
use Cake\Datasource\ConnectionManager;
use Cake\Cache\Cache;
use Exception;
use Cake\View\CellTrait;
use \Firebase\JWT\JWT;

class DashboardController extends ProjapotiController
{

    use CellTrait;

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['ministryDivisionDashboard', 'officeSummary', 'officeDashboard',
            'unitDashboard', 'unitDashboardAjax', 'MonitorLayers', 'loginDetails',
            'ApiDashboardSummary',
            'ApiDetailDashboard',
            'getOfficesId', 'ministryDivisionDashboardAjax', 'getNewNotification',
            'apiUserSummary', 'monitorDashboardReportsOfOffices', 'newMonitor', 'newMonitorAjax',
            'downloadUsermanual', 'agingOwnUnitCell', 'absentDetails', 'downloadApp', 'postApiUserSummary',
            'postApiDetailDashboard', 'monitorCron', 'test', 'officeListCell', 'faq', 'releaseNote', 'postApiUserNotification', 'sslInstruction', 'ownUnitList', 'loadSingleUnit', 'speedTest', 'showUserManualList','checkOISFToken','jwtSSO','privacyPolicyForApp','getOfficeLoginCount', 'userManual', 'playRequest',
            'notices', 'nothi_video_tutorial',
        ]);
    }

    /**
     * LOGGED USER DASHBOARD
     */
    public function dashboard() {
        $session = $this->request->session();
        $selected_module_id = $session->read('module_id');
        $selected_designation = $this->getCurrentDakSection();
		$user = $this->Auth->user();

        // check admin in user table
        $users_table = TableRegistry::get('Users');
        if(!empty($selected_designation['officer_id'])){
             $users = $users_table->find()->where(['employee_record_id' => $user['employee_record_id']])->first();
        }


        if (!empty($users) && $users->is_admin == 1) {
            $employee_offices_table = TableRegistry::get('EmployeeOffices');
            $employee_offices = $employee_offices_table->find()->where(['employee_record_id' => $this->getCurrentDakSection()['officer_id'], 'status' => 1])->group(['office_id']);
            if ($employee_offices->count() > 1) {
                $users->is_admin = 0;
                $users_table->save($users);

                $this->Flash->error('অফিস এডমিন বাছাই করার জন্য সাপোর্ট টিমের সাথে যোগাযোগ করুন');
                return $this->redirect(['action' => 'dashboard']);
            } else {
                $OfficeUnitOrganogramsTable = TableRegistry::get('OfficeUnitOrganograms');
                $owninfo = $OfficeUnitOrganogramsTable->find()->where(['office_id' => $this->getCurrentDakSection()['office_id'], 'is_admin' => 1])->first();
                if (empty($owninfo)) {
                    $this->Flash->error('অনুগ্রহ করে অফিস এডমিন বাছাই করুন।');
                    return $this->redirect(['action' => 'setOfficeAdmin']);
                } else {
                    $users->is_admin = 0;
                    $users_table->save($users);
                }
            }
        }
        //dd('final die');

        if (empty($selected_module_id)) {
            $selected_module_id = 5;
        }else if($selected_module_id == 2 && $selected_designation['is_admin'] == 0){
             $selected_module_id = 5;
        }

        $modules = jsonA(MODULES);

        $session->write('modules', $modules);
        $selected_module_name = $modules[$selected_module_id];
        $table_instance_emp_records = TableRegistry::get('EmployeeRecords');


        if (!empty($user) && $selected_designation['is_admin'] == 1 && !empty($selected_designation['office_id']) && $selected_module_name == DAK) {

            if (TableRegistry::get('EmployeeOffices')->checkOfficeHeadExist($selected_designation['office_id']) == 0) {
                $url = \Cake\Routing\Router::url(['controller' => 'OfficeEmployeeMappings', 'action' => 'officeHeadManager']);
                $this->Flash->default('<a href="' . $url . '">অফিস প্রধান বাছাই করা হয়নি। দয়া করে অফিস প্রধান বাছাই করতে ক্লিক করুন।</a>');
            }
        }
        if (!empty($user)) {
            //actually this is right now protibedon module
            if (!$session->check('can_see_report_module')) {
                $can_see_report_module = TableRegistry::get('ViewReports')->find()->where(['username' => $user['username']])->count();
                $session->write('can_see_report_module', $can_see_report_module);
            }
        }

        $dashboard_data = array();

        if ($user['employee_record_id'] != null) {
            $employee_info = $table_instance_emp_records->get($user['employee_record_id']);

            $table_instance_emp_offices = TableRegistry::get('EmployeeOffices');
            $employee_office_records = $table_instance_emp_offices->getEmployeeOfficeRecords($employee_info->id);
            $employee_office_recordLists = $table_instance_emp_offices->find('list',['keyField'=>'office_unit_organogram_id','valueField'=>'designation'])
                ->where(['employee_record_id'=> $user['employee_record_id'],'status'=>1])->toArray();

            $data_item = array();
            $data_item['personal_info'] = $employee_info;
            $data_item['office_records'] = count($employee_office_records) > 0 ? $employee_office_records
                : array();

            $table_instance_offices = TableRegistry::get('Offices');
            $table_instance_ministris = TableRegistry::get('OfficeMinistries');
            $officeInformation = array();
            $ministryInformation = array();
            if (!empty($employee_office_records[0]['office_id'])) {
                $officeInformation = $table_instance_offices->get($employee_office_records[0]['office_id']);

                if (!empty($officeInformation)) {
                    $ministryInformation = $table_instance_ministris->get($officeInformation['office_ministry_id']);
                }
            }

            $data_item['ministry_records'] = count($ministryInformation) > 0 ? $ministryInformation : array();

            if (!empty($employee_info['date_of_birth'])) {
                $time = new \Cake\I18n\Time($employee_info['date_of_birth']);

                if ($time->day == date('d') && $time->month == date("m")) {
                    $this->Flash->success(__('শুভ জন্মদিন। আপনার দিন আরো সুন্দর কাটুক। শুভেচ্ছান্তে, নথি টিম।'));
                }
            }

            if (defined("Live") && Live == 1) {
                $lastchangetime = new \Cake\I18n\Time($user['last_password_change']);
                $todaytime = new \Cake\I18n\Time("2017-05-05");

                if ($todaytime->gt($lastchangetime)) {
                    $this->Flash->success(__('সিস্টেমের নিরাপত্তার স্বার্থে   কমপক্ষে ৬ ডিজিটের আলফা-নিউমেরিক পাসওয়ার্ড ব্যাবহার করুন। আপনার পাসওয়ার্ড টি কমপক্ষে ৬ ডিজিটের আলফা-নিউমেরিক পাসওয়ার্ড না হলে এখন ই পরিবরতন করুন।'));
                }
            }
            if (empty($employee_info['date_of_birth']) || $this->checkNID($employee_info['date_of_birth']->format("Y-m-d"),
                    $employee_info['nid'])
            ) {
                if (strlen($employee_info['nid']) != 10) {
                    $nidmsg = __('আপনার জন্ম তারিখ অথবা জাতীয় পরিচয় পত্র নম্বর সঠিক নয়। আপনার জন্ম তারিখ এবং জাতীয় পরিচয় পত্র সংশোধন করার জন্য অনুরোধ করা হচ্ছে। ধন্যবাদ');
                    $this->set('niderror', $nidmsg);
                }
            }

            $session = $this->request->session();
            if (!$session->check('logged_employee_record')) {
                $session->write('logged_employee_record', $data_item);
            }

            if (!empty($this->request->data['selected_office_unit_organogram_id'])) {
                $session->delete('canSeeOfficeDB');
                $session->delete('has_monitor');
                $office_section['office_id'] = intval($this->request->data['selected_office_id']);
                $office_section['office_unit_id'] = intval($this->request->data['selected_office_unit_id']);
                $office_section['office_unit_organogram_id'] = intval($this->request->data['selected_office_unit_organogram_id']);


                if(!in_array($office_section['office_unit_organogram_id'],array_keys($employee_office_recordLists))){

                    $this->Flash->error("Unauthorize access");
                    return true;
                }
                $office_section['officer_id'] = $user['employee_record_id'];
                $designation_info = TableRegistry::get('OfficeUnitOrganograms')
                    ->getAll(['id' => $office_section['office_unit_organogram_id']], ['designation_eng'])->first();
                $office_section['designation_eng'] = $designation_info['designation_eng'];

                $OfficeUnitOrganogramsTable = TableRegistry::get('OfficeUnitOrganograms');
                $owninfo = $OfficeUnitOrganogramsTable->get($this->request->data['selected_office_unit_organogram_id']);
                $office_section['is_admin'] = $owninfo['is_admin'];

                if (count($employee_office_records) == 1) {
                    $office_section['incharge_label'] = $employee_office_records[0]['incharge_label'];
                } else {
                    foreach ($employee_office_records as $eor_val) {
                        if ($eor_val['office_unit_organogram_id'] == $this->request->data['selected_office_unit_organogram_id']) {
                            $office_section['incharge_label'] = $eor_val['incharge_label'];
                            $office_section['designation_label'] = $office_section['designation'] = $eor_val['designation'];
                            $office_section['show_unit'] = $eor_val['show_unit'];
                        }
                    }
                }

                $table_instance_office = TableRegistry::get('Offices');
                $office = $table_instance_office->get($office_section['office_id']);
                $office_section['office_name'] = $office['office_name_bng'];
                $office_section['office_name_eng'] = $office['office_name_eng'];
                $office_section['office_address'] = $office['office_address'];

                $office_section['officer_name'] = $employee_info['name_bng'];
                $office_section['officer_name_eng'] = $employee_info['name_eng'];
                $office_section['ministry_id'] = $office['office_ministry_id'];

                $ministryInformation = $table_instance_ministris->get($office['office_ministry_id']);
                $office_section['ministry_records'] = $office_section['ministry_name_bng'] = count($ministryInformation)
                > 0 ? $ministryInformation['name_bng'] : '';
                $office_section['ministry_name_eng'] = count($ministryInformation)
                > 0 ? $ministryInformation['name_eng'] : '';


                $office_section['office_phone'] = $office['office_phone'];
                $office_section['office_fax'] = $office['office_fax'];
                $office_section['office_email'] = $office['office_email'];
                $office_section['office_web'] = $office['office_web'];

                $table_instance_office_unit = TableRegistry::get('OfficeUnits');
                $office_unit = $table_instance_office_unit->get($office_section['office_unit_id']);
                $office_section['office_unit_name'] = $office_unit['unit_name_bng'];
                $office_section['office_unit_name_eng'] = $office_unit['unit_name_eng'];

                //Digital sign
                $office_section['default_sign'] = intval($employee_info['default_sign']);
                $office_section['cert_id'] = $employee_info['cert_id'];
                $office_section['cert_type'] = $employee_info['cert_type'];
                $office_section['cert_provider'] = $employee_info['cert_provider'];
                $office_section['cert_serial'] = $employee_info['cert_serial'];

                $office_section['user_id'] = isset($user['id'])?$user['id']:0;
                $office_section['username'] = isset($user['username'])?$user['username']:'';

                $session->write('selected_office_section', $office_section);

                if (!empty($office_section['office_unit_organogram_id'])) {
                    /*                     * Can User see Office Dashboard and has monitor button privilage* */
                    $this->checkMonitorPrivilage($office_section['office_unit_organogram_id']);
                    $this->checkOfficeMonitorPrivilage($office_section['office_id'], $office_section['office_unit_organogram_id']);
                }
            } else {
                if (empty($selected_designation['office_unit_organogram_id']) && !empty($employee_office_records[0])) {

                    $office_section = array();
                    $selected_office = $employee_office_records[0];
                    $office_section['office_id'] = $selected_office['office_id'];
                    $office_section['office_unit_id'] = $selected_office['office_unit_id'];
                    $office_section['office_unit_organogram_id'] = $selected_office['office_unit_organogram_id'];
                    $office_section['officer_id'] = $employee_office_records[0]['employee_record_id'];
                    $office_section['incharge_label'] = $employee_office_records[0]['incharge_label'];

                    $OfficeUnitOrganogramsTable = TableRegistry::get('OfficeUnitOrganograms');
                    $owninfo = $OfficeUnitOrganogramsTable->get($employee_office_records[0]['office_unit_organogram_id']);
                    $office_section['is_admin'] = $owninfo['is_admin'];
                    //$office_section['is_admin'] = $selected_office['is_admin'];

                    $table_instance_office = TableRegistry::get('Offices');
                    $office = $table_instance_office->get($office_section['office_id']);

                    $office_section['office_name'] = $office['office_name_bng'];
                    $office_section['office_name_eng'] = $office['office_name_eng'];
                    $office_section['office_address'] = $office['office_address'];

                    $designation_info = TableRegistry::get('OfficeUnitOrganograms')->getAll(['id' => $selected_office['office_unit_organogram_id']], ['designation_eng'])->first();
                    $office_section['designation_label'] = $office_section['designation'] = $selected_office['designation'];
                    $office_section['designation_eng'] = $designation_info['designation_eng'];

                    $office_section['officer_name'] = $employee_info['name_bng'];
                    $office_section['ministry_id'] = $office['office_ministry_id'];

                    $ministryInformation = $table_instance_ministris->get($office['office_ministry_id']);
                    $office_section['ministry_records'] = count($ministryInformation) > 0 ? $ministryInformation['name_bng']
                        : '';

                    $office_section['office_phone'] = $office['office_phone'];
                    $office_section['office_fax'] = $office['office_fax'];
                    $office_section['office_email'] = $office['office_email'];
                    $office_section['office_web'] = $office['office_web'];

                    $table_instance_office_unit = TableRegistry::get('OfficeUnits');
                    $office_unit = $table_instance_office_unit->get($office_section['office_unit_id']);
                    $office_section['office_unit_name'] = $office_unit['unit_name_bng'];
                    $office_section['show_unit'] = $selected_office['show_unit'];

                    //Digital sign
                    $office_section['default_sign'] = intval($employee_info['default_sign']);
                    $office_section['cert_id'] = $employee_info['cert_id'];
                    $office_section['cert_type'] = $employee_info['cert_type'];
                    $office_section['cert_provider'] = $employee_info['cert_provider'];
                    $office_section['cert_serial'] = $employee_info['cert_serial'];

                    $office_section['user_id'] = isset($user['id'])?$user['id']:0;
                    $office_section['username'] = isset($user['username'])?$user['username']:'';

                    $session->write('selected_office_section', $office_section);

                    if (!empty($office_section['office_unit_organogram_id'])) {
                        /*                     * Can User see Office Dashboard and has monitor button privilage* */
                        $this->checkMonitorPrivilage($office_section['office_unit_organogram_id']);
                        $this->checkOfficeMonitorPrivilage($office_section['office_id'], $office_section['office_unit_organogram_id']);
                    }
                }
            }

            if (!empty($selected_designation['office_unit_organogram_id'])) {
                /*                     * Can User see Office Dashboard and has monitor button privilage* */
                $this->checkMonitorPrivilage($selected_designation['office_unit_organogram_id']);
                $this->checkOfficeMonitorPrivilage($selected_designation['office_id'], $selected_designation['office_unit_organogram_id']);
            }
            $dashboard_data = $this->_populateDakDashboard($employee_office_records);

            if ($selected_module_name == DAK) {
                $this->view = 'dashboard_dak';
                $dashboard_data = $this->_populateDakDashboard($employee_office_records);
            }

            if ($selected_module_name == NOTHI) {
                $this->view = 'dashboard_nothi';
                $dashboard_data = $this->_populateNothiDashboard($employee_office_records);
                if(!empty($selected_designation['office_id'])) {
                    $NothiTypesTable = TableRegistry::get('NothiTypes');
					$nothiTypes = $NothiTypesTable->find('list', ['keyField' => 'id', 'valueField' => 'type_name'])->toArray();
					$this->set('nothiTypes', $nothiTypes);
                    $OfficeUnitsTable = TableRegistry::get('OfficeUnits');
                    $ownChildInfo = $OfficeUnitsTable->getOfficeUnitsList($selected_designation['office_id']);
                    $this->set('ownChildInfo', $ownChildInfo);
                }

            }

			$selected_designation = $this->getCurrentDakSection();
			if ($user['user_role_id'] > 2 && $selected_designation && isset($selected_designation['office_id'])) {
				$OtherOrganogramActivitiesSettingsTable = TableRegistry::get('OtherOrganogramActivitiesSettings');
				$OtherOrganogramActivitiesSettings = $OtherOrganogramActivitiesSettingsTable->find()->where(['assigned_organogram_id' => $selected_designation['office_unit_organogram_id'], 'status' => 1, 'permission_for IN' => [0, 1]])->count();
				if ($OtherOrganogramActivitiesSettings > 0) {
					$selected_designation['other_organogram_activities_permission_dak'] = 1;
				} else {
					$selected_designation['other_organogram_activities_permission_dak'] = 0;
				}
				$OtherOrganogramActivitiesSettingsTable = TableRegistry::get('OtherOrganogramActivitiesSettings');
				$OtherOrganogramActivitiesSettings = $OtherOrganogramActivitiesSettingsTable->find()->where(['assigned_organogram_id' => $selected_designation['office_unit_organogram_id'], 'status' => 1, 'permission_for IN' => [0, 2]])->count();
				if ($OtherOrganogramActivitiesSettings > 0) {
					$selected_designation['other_organogram_activities_permission_nothi'] = 1;
				} else {
					$selected_designation['other_organogram_activities_permission_nothi'] = 0;
				}
				$session->write('selected_office_section', $selected_designation);
				$selected_designation = $this->getCurrentDakSection();
			}

            if ($selected_module_name == DOPTOR) {
                $dashboard_data = $this->_populateDoptorDashboard($employee_office_records);
            }


            if ($selected_module_name == "ড্যাশবোর্ড") {
                $this->redirect(['controller' => 'Dashboard', 'action' => 'detailDashboard']);
            }

            if ($selected_module_name == USER) {
                $dashboard_data = $this->_populateUserDashboard($employee_office_records);
            }

            if ($selected_module_name == SETTINGS) {
                $dashboard_data = $this->_populateSettingsDashboard($employee_office_records);
            }

        }
        if ($selected_module_id == 8) {
            $this->view = 'dashboard_report';
            $dashboard_data = $this->_populateReportDashboard();
        }
//       pr($this->getCurrentDakSection());die;
        $this->set('user', $user);
        $this->set('dashboard_data', $dashboard_data);

        if ($selected_module_id == 2 && $selected_designation['is_admin'] != true) {
            $session->write('module_id', 5);

            return $this->redirect(['action'=>'dashboard']);
        }
    }

    public function setOfficeAdmin() {
        $OfficeUnitOrganogramsTable = TableRegistry::get('OfficeUnitOrganograms');
        $owninfo = $OfficeUnitOrganogramsTable->find()->where(['office_id' => $this->getCurrentDakSection()['office_id'], 'is_admin' => 1])->first();
        $this->set('office_id', $this->getCurrentDakSection()['office_id']);
        if (!empty($owninfo)) {
            $this->Flash->error('অফিস এডমিন ইতোমধ্যে বাছাই করা হয়েছে।');
            $this->redirect(['controller' => 'dashboard', 'action' => 'dashboard']);
        }
    }
    private function checkMonitorPrivilage($designation_id)
    {
        $session = $this->request->session();
        if (empty($designation_id)) {
            $session->write('has_monitor', 0);
            return;
        }
        if (!$session->check('has_monitor')) {
            $has_monitor = TableRegistry::get('MonitoringPrivilage')->find()->where(['designation_id' => $designation_id])->count();
            $session->write('has_monitor', $has_monitor);
        }
    }

    private function checkOfficeMonitorPrivilage($office_id, $designation_id)
    {
        $session = $this->request->session();
        if (empty($designation_id) || empty($office_id)) {
            $session->write('canSeeOfficeDB', 0);
            return;
        }
        if (!$session->check('canSeeOfficeDB')) {
            /*                     * Can User see Office Dashboard* */
            $tbl_VOD = TableRegistry::get('ViewOfficeDashboard');
            $has_access = $tbl_VOD->find()->select(['viewer_designations'])->where(['office_id' => $office_id])->first();
            $canSee = 0;
            if (!empty($has_access)) {
                $php_array = json_decode($has_access['viewer_designations']);
                if (!empty($php_array)) {
                    foreach ($php_array as $val) {
                        if ($val == $designation_id) {
                            $canSee = 1;
                            break;
                        }
                    }
                }
            }
            $session->write('canSeeOfficeDB', $canSee);
        }
    }

    private function _populateDakDashboard($employee_office_records)
    {
        $dashboard_data = array();
        $user = $this->Auth->user();

        $office_unit_table = TableRegistry::get('OfficeUnits');

        foreach ($employee_office_records as $designation) {

            $dak_designation = array();
            $designation_id = $designation['office_unit_organogram_id'];
            $dak_designation['id'] = $designation_id;
            $dak_designation['name'] = $designation['designation'] . (!empty($designation['incharge_label']) ? (' (' . $designation['incharge_label'] . ')') : '') . ', ' . rtrim($designation['unit_name']);
            $dak_designation['office_unit_id'] = $designation['office_unit_id'];
            $dak_designation['office_id'] = $designation['office_id'];

            $row2 = array();
            $row2['name'] = 'আগত  ডাক';
            $row2['key'] = 'inbox';
            $row2['value'] = 0;
            $dak_designation['child'][] = $row2;


            $row3 = array();
            $row3['name'] = 'প্রেরিত ডাক';
            $row3['key'] = 'sent';
            $row3['value'] = 0;
            $dak_designation['child'][] = $row3;

            $dashboard_data[] = $dak_designation;
        }

        return $dashboard_data;
    }

    private function _populateNothiDashboard($employee_office_records)
    {
        $dashboard_data = array();
        TableRegistry::remove('NothiMasterCurrentUsers');
        $nothiCurrentUser = TableRegistry::get('NothiMasterCurrentUsers');

        foreach ($employee_office_records as $designation) {

            $nothi_inbox = array();
            $designation_id = $designation['office_unit_organogram_id'];
            $nothi_inbox['id'] = $designation_id;
            $nothi_inbox['name'] = $designation['designation'] . (!empty($designation['incharge_label']) ? (' (' . $designation['incharge_label'] . ')') : '') . ', ' . rtrim($designation['unit_name']);
            $nothi_inbox['office_unit_id'] = $designation['office_unit_id'];
            $nothi_inbox['office_id'] = $designation['office_id'];

            $totalInbox = $nothiCurrentUser->find()->where([
                'view_status' => 0,
                'office_unit_organogram_id' => $designation['office_unit_organogram_id'],
                'office_unit_id' => $designation['office_unit_id'],
                'office_id' => $designation['office_id'],
                'nothi_office' => $designation['office_id'],
                'is_new' => 0,
                'is_archive' => 0
            ])->count();
            $totalOtherInbox = $nothiCurrentUser->find()->where([
                'view_status' => 0,
                'office_unit_organogram_id' => $designation['office_unit_organogram_id'],
                'office_unit_id' => $designation['office_unit_id'],
                'office_id' => $designation['office_id'],
                'nothi_office <>' => $designation['office_id'],
                'is_new' => 0,
                'is_archive' => 0
            ])->count();

            $row2 = array();
            $row2['name'] = 'আগত নথি';
            $row2['key'] = 'inbox';
            $row2['value'] = $totalInbox;
            $nothi_inbox['child'][] = $row2;

            $row1 = array();
            $row1['name'] = 'প্রেরিত নথি';
            $row1['key'] = 'sent';
            $row1['value'] = 0;
            $nothi_inbox['child'][] = $row1;

            $row3 = array();
            $row3['name'] = 'অন্যান্য  অফিসের আগত নথি';
            $row3['key'] = 'other';
            $row3['value'] = $totalOtherInbox;
            $nothi_inbox['child'][] = $row3;

            $row3 = array();
            $row3['name'] = 'সকল নথিসমূহ ';
            $row3['key'] = 'all';
            $row3['value'] = 0;
            $nothi_inbox['child'][] = $row3;

            $dashboard_data[] = $nothi_inbox;
        }

        return $dashboard_data;
    }

    private function _populateDoptorDashboard($employee_office_records)
    {
        $dashboard_data = array();
      
        $designation_ids = [];
        if(!empty($employee_office_records)){
            foreach ($employee_office_records as $designation){
                $designation_ids[] = $designation['office_unit_organogram_id'];
            }
        }
        if(!empty($designation_ids)){
          $officeUnitOrganogramsTable = TableRegistry::get('OfficeUnitOrganograms');
          $all_designation = $officeUnitOrganogramsTable->getAllDesignationsByDesignationIds($designation_ids,[],1,['keyField'=>'id','valueField'=>'is_admin'])->toArray();
        }
        foreach ($employee_office_records as $designation) {
          
            $nothi_inbox = array();
            $designation_id = $designation['office_unit_organogram_id'];
            //if(!empty($all_designation) && $all_designation[$designation_id] == 0){
            //    continue;
            //}
            $nothi_inbox['id'] = $designation_id;
            $nothi_inbox['name'] = $designation['designation'] . (!empty($designation['incharge_label']) ? (' (' . $designation['incharge_label'] . ')') : '') . ', ' . rtrim($designation['unit_name']);
            $nothi_inbox['office_unit_id'] = $designation['office_unit_id'];
            $nothi_inbox['office_id'] = $designation['office_id'];
            $nothi_inbox['is_admin'] = $all_designation[$designation_id];

            $dashboard_data[] = $nothi_inbox;
        }

        return $dashboard_data;
    }

    private function _populateSettingsDashboard($employee_office_records)
    {

    }

    private function _populateUserDashboard($employee_office_records)
    {

    }

    public function personalDashboard()
    {

    }

    public function detailDashboard()
    {
        set_time_limit(0);
        $employee_office = $this->getCurrentDakSection();
        $OfficeUnitOrganogramsTable = TableRegistry::get('OfficeUnitOrganograms');
        $owninfo = $OfficeUnitOrganogramsTable->get($employee_office['office_unit_organogram_id']);
        $canSee = 0;

        $officeEmployeeTable = TableRegistry::get('EmployeeOffices');
        $isOfficeHead= $officeEmployeeTable->find()->where(['office_unit_organogram_id'=>$employee_office['office_unit_organogram_id'],'office_head'=>1])->count();
        // office head and office admin can see office dashboard
        if ($employee_office['is_admin'] || $isOfficeHead > 0 ) {
            $canSee = 1;
        }
        // 2583
        if(!empty($employee_office['office_id'])){
            $origins_2_show_mapped_list = [15,16,17];
            $office_info = TableRegistry::get('Offices')->getAll(
                ['id' =>$employee_office['office_id']],['office_origin_id']
                )->first();
            if(!empty($office_info) && !empty($office_info['office_origin_id'])){
                if(in_array($office_info['office_origin_id'],$origins_2_show_mapped_list)) {
                    // need to show division, district, upazila list
                    $this->set('show_geo_div',1);
                }
            }

        }

        $this->set('canSee', $canSee);
        $this->set('owninfo', $owninfo);
    }

    public function ApiDetailDashboard()
    {
        set_time_limit(0);
        $user_designation = $this->request->query['user_designation'];

        $apikey = isset($this->request->query['api_key']) ? $this->request->query['api_key'] : '';
        if (empty($apikey) || $this->checkToken($apikey) == FALSE) {
            die("Invalid Request");
        }

        if (!defined("PROJAPOTI_API") || PROJAPOTI_API == false) {
            die("Invalid Request");
        }
        if (empty($user_designation)) {
            die("Invalid Request");
        }

        $this->view = 'detail_dashboard_api';
        $this->layout = 'dashboard_api';

        $EmployeeOfficesTable = TableRegistry::get('EmployeeOffices');

        $owninfo = $EmployeeOfficesTable->getDesignationInfo($user_designation);
        $canSee = 0;

        if ($owninfo['designation_level'] == 1 || $owninfo['summary_nothi_post_type'] > 0) {
            $canSee = 1;
        }

        $this->set('canSee', $canSee);
        $this->set('owninfo', $owninfo);
        $this->set('api', 1);
    }

    public function postApiDetailDashboard()
    {
        set_time_limit(0);
        $user_designation = $this->request->data['user_designation'];

        $apikey = isset($this->request->data['api_key']) ? $this->request->data['api_key'] : '';
        if (empty($apikey) || $this->checkToken($apikey) == FALSE) {
            die("Invalid Request");
        }

        if (!defined("PROJAPOTI_API") || PROJAPOTI_API == false) {
            die("Invalid Request");
        }
        if (empty($user_designation)) {
            die("Invalid Request");
        }
        if($apikey != API_KEY){
            //verify api token
            $this->loadComponent('ApiRelated');
            $getApiTokenData = $this->ApiRelated->getMobileApiTokenData($apikey);
            if(!empty($getApiTokenData)){
                if(!in_array($user_designation,$getApiTokenData['designations'])){
                    die("Unauthorized request");
                }
            }
            //verify api token
        }
        $this->view = 'detail_dashboard_api';
        $this->layout = 'dashboard_api';

        $EmployeeOfficesTable = TableRegistry::get('EmployeeOffices');

        $owninfo = $EmployeeOfficesTable->getDesignationInfo($user_designation);
        $canSee = 0;

        if ($owninfo['designation_level'] == 1 || $owninfo['summary_nothi_post_type'] > 0) {
            $canSee = 1;
        }

        $this->set('canSee', $canSee);
        $this->set('owninfo', $owninfo);
        $this->set('api', 1);
    }

    public function DashboardGraph()
    {
        $employee_office = $this->getCurrentDakSection();
        $employeeOfficeTable = TableRegistry::get('EmployeeOffices');

        $owninfo = $employeeOfficeTable->getDesignationInfo($employee_office['office_unit_organogram_id']);
        $canSee = 0;
        if ($owninfo['designation_level'] == 1 || $owninfo['summary_nothi_post_type'] > 0) {
            $canSee = 1;
        }

        $this->set('canSee', $canSee);
        $this->set('owninfo', $owninfo);
    }

    public function officeSummary($officeid = 0)
    {
        set_time_limit(0);
        $this->layout = null;


        if ($officeid == 0) {
            $employee_office = $this->getCurrentDakSection();
            if (!empty($employee_office)) {
                $officeid = $employee_office['office_id'];
            } else {
                die;
            }
        } else {
            $this->switchOffice($officeid, 'DashboardOffice');
        }

//        $table_reports = TableRegistry::get('Reports');
        $table_dashboard = TableRegistry::get('Dashboard');

        $officeTable = TableRegistry::get('Offices');

        $officeinfo = $officeTable->get($officeid);
        $result = $table_dashboard->officeSummary($officeid);

        $this->set('result', $result);
        $this->set('officeinfo', $officeinfo);
    }

    private function totalEmployeecount($OfficeIds)
    {
        $sum_all = 0;
        $totalLoginToday = 0;
        $totalLoginYesterday = 0;
        $user_login_history_table = TableRegistry::get('UserLoginHistory');
        $office_domain_table = TableRegistry::get('OfficeDomains');
        $table_eo = TableRegistry::get('EmployeeOffices');
        $today = date('Y-m-d');
        $yesterday = date('Y-m-d', strtotime($today . ' -1 day'));
        foreach ($OfficeIds as $key => $val) {
//            $checkdomain = $office_domain_table->getDomainbyOffice($key);
//            if (empty($checkdomain)) {
//                continue;
//            }
            try {
//                $totalUser = $table_eo->getAllEmployeeRecordID($key);
                $sum_all += $table_eo->getCountOfEmployeeOfOffices($key);

                $totalLoginToday += $user_login_history_table->getLoginCount(0, 0, 0, $key, 0, 0, [$today, $today])->count();
                $totalLoginYesterday += $user_login_history_table->getLoginCount(0, 0, 0, $key, 0, 0, [$yesterday, $yesterday])->count();
            } catch (Exception $ex) {
                continue;
            }
        }
        $sum = [];
        $sum['loginToday'] = $totalLoginToday;
        $sum['loginYesterday'] = $totalLoginYesterday;
        $sum['employee'] = $sum_all;
        return $sum;
    }

    public function ministryDivisionDashboard()
    {

        set_time_limit(0);
        $this->layout = 'dashboard';
        if ($this->request->is('get')) {
            $table_instance = TableRegistry::get('OfficeMinistries');
            $data_items = $table_instance->find('list')->toArray();
            $this->set('officeMinistries', $data_items);
        }
        if ($this->request->is('post')) {
            $this->layout = 'ajax';
            $office_layers_table = TableRegistry::get('OfficeLayers');

            $OfficeIds = $office_layers_table->getLayerwiseOfficeEmployeeCount();

            $totaloffices_all = count($OfficeIds);
            $sum = $this->totalEmployeecount($OfficeIds);
            $sum_all = $sum['employee'];
            $today_login_all = $sum['loginToday'];
            $yesterday_login_all = $sum['loginYesterday'];

            $OfficeIds = $office_layers_table->getLayerwiseOfficeEmployeeCount('Ministry');
            $totaloffices_ministry = count($OfficeIds);
            $sum = $this->totalEmployeecount($OfficeIds);
            $sum_Ministry = $sum['employee'];
            $today_login_ministry = $sum['loginToday'];
            $yesterday_login_ministry = $sum['loginYesterday'];

            $OfficeIds = $office_layers_table->getLayerwiseOfficeEmployeeCount('Directorate');
            $totaloffices_directorate = count($OfficeIds);
            $sum = $this->totalEmployeecount($OfficeIds);
            $sum_Directorate = $sum['employee'];
            $today_login_directorate = $sum['loginToday'];
            $yesterday_login_directorate = $sum['loginYesterday'];

            $OfficeIds = $office_layers_table->getLayerwiseOfficeEmployeeCount('Divisional');
            $totaloffices_divisional = count($OfficeIds);
            $sum = $this->totalEmployeecount($OfficeIds);
            $sum_Divisional = $sum['employee'];
            $today_login_divisional = $sum['loginToday'];
            $yesterday_login_divisional = $sum['loginYesterday'];

            $OfficeIds = $office_layers_table->getLayerwiseOfficeEmployeeCount('District');
            $totaloffices_district = count($OfficeIds);
            $sum = $this->totalEmployeecount($OfficeIds);
            $sum_District = $sum['employee'];
            $today_login_district = $sum['loginToday'];
            $yesterday_login_district = $sum['loginYesterday'];

            $OfficeIds = $office_layers_table->getLayerwiseOfficeEmployeeCount('Upazilla');
            $totaloffices_upazilla = count($OfficeIds);
            $sum = $this->totalEmployeecount($OfficeIds);
            $sum_Upazilla = $sum['employee'];
            $today_login_upazilla = $sum['loginToday'];
            $yesterday_login_upazilla = $sum['loginYesterday'];

            $sum_Other = $sum_all - ($sum_Ministry + $sum_Directorate + $sum_Divisional + $sum_Upazilla
                    + $sum_District);

            $this->set('sum_Ministry', Number::format($sum_Ministry));
            $this->set('sum_Directorate', Number::format($sum_Directorate));
            $this->set('sum_Divisional', Number::format($sum_Divisional));
            $this->set('sum_District', Number::format($sum_District));
            $this->set('sum_Upazilla', Number::format($sum_Upazilla));
            $this->set('sum_Other', Number::format($sum_Other));
            $this->set('sum_All', Number::format($sum_all));


            $totaloffices_other = $totaloffices_all - ($totaloffices_ministry + $totaloffices_directorate
                    + $totaloffices_divisional + $totaloffices_district + $totaloffices_upazilla);
            $this->set('totaloffices_ministry', Number::format($totaloffices_ministry));
            $this->set('totaloffices_directorate', Number::format($totaloffices_directorate));
            $this->set('totaloffices_divisional', Number::format($totaloffices_divisional));
            $this->set('totaloffices_district', Number::format($totaloffices_district));
            $this->set('totaloffices_upazilla', Number::format($totaloffices_upazilla));
            $this->set('totaloffices_other', Number::format($totaloffices_other));
            $this->set('totaloffices_all', Number::format($totaloffices_all));

            $today_login_other = $today_login_all - ($today_login_ministry + $today_login_directorate + $today_login_district
                    + $today_login_divisional + $today_login_upazilla);

            $this->set('today_login_ministry', Number::format($today_login_ministry));
            $this->set('today_login_directorate', Number::format($today_login_directorate));
            $this->set('today_login_divisional', Number::format($today_login_divisional));
            $this->set('today_login_district', Number::format($today_login_district));
            $this->set('today_login_upazilla', Number::format($today_login_upazilla));
            $this->set('today_login_other', Number::format($today_login_other));
            $this->set('today_login_all', Number::format($today_login_all));

            $yesterday_login_other = $yesterday_login_all - ($yesterday_login_ministry + $yesterday_login_directorate
                    + $yesterday_login_district + $yesterday_login_divisional + $yesterday_login_upazilla);

            $this->set('yesterday_login_ministry', Number::format($yesterday_login_ministry));
            $this->set('yesterday_login_directorate', Number::format($yesterday_login_directorate));
            $this->set('yesterday_login_divisional', Number::format($yesterday_login_divisional));
            $this->set('yesterday_login_district', Number::format($yesterday_login_district));
            $this->set('yesterday_login_upazilla', Number::format($yesterday_login_upazilla));
            $this->set('yesterday_login_other', Number::format($yesterday_login_other));
            $this->set('yesterday_login_all', Number::format($yesterday_login_all));
            $this->view = 'summary_view';
        }
    }

    public function ministryDivisionDashboardAjax()
    {
        set_time_limit(0);
        if ($this->request->is('ajax')) {
            $this->layout = 'ajax';
            $alldata = [];
            $MinistryId = 0;
            $LayerId = 0;
            $OriginId = 0;
            $totalLogin = 0;
            if (!empty($this->request->data['office_ministry_id'])) {
                $MinistryId = $this->request->data['office_ministry_id'];
                TableRegistry::remove('OfficeMinistries');
                $ministry_table = TableRegistry::get('OfficeMinistries');
                $ministry_name = $ministry_table->getBanglaName($this->request->data['office_ministry_id']);
            }

            $office_ids = !empty($this->request->data['office_id']) ? $this->request->data['office_id']
                : array();
            $office_ids_name = !empty($this->request->data['name']) ? ($this->request->data['name'])
                : '';
//            {
            $office_layers = !empty($this->request->data['office_layer_id']) ? $this->request->data['office_layer_id']
                : array();

            if (!empty($office_layers)) {

                foreach ($office_layers as $layerKey => $layerValue) {
                    $office_origins = array();
                    TableRegistry::remove('OfficeLayers');
                    $office_layer_table = TableRegistry::get('OfficeLayers');
                    $layer_name = $office_layer_table->getBanglaName($layerKey);
                    $LayerId = $layerKey;
                    TableRegistry::remove('OfficeOrigins');
                    $table_office_origins = TableRegistry::get('OfficeOrigins');
                    $office_origins = $table_office_origins->getOriginsByLayer($layerKey);

                    if (!empty($office_origins)) {
                        foreach ($office_origins as $key => $value) {
                            $OriginId = $key;
                            $origin_name = $table_office_origins->getBanglaName($key);
                            TableRegistry::remove('Offices');
                            $table_offices = TableRegistry::get('Offices');
                            $offices = $table_offices->getOfficesByOrigin($key);

                            foreach ($offices as $office_key => $office_value) {
                                if (!isset($alldata[$MinistryId]['TotalOffices'])) {
                                    $alldata[$MinistryId]['TotalOffices'] = 1;
                                } else {
                                    $alldata[$MinistryId]['TotalOffices']++;
                                }
                                if (!isset($alldata[$MinistryId][$LayerId]['TotalOffices'])) {
                                    $alldata[$MinistryId][$LayerId]['TotalOffices'] = 1;
                                } else {
                                    $alldata[$MinistryId][$LayerId]['TotalOffices']++;
                                }
                                if (!isset($alldata[$MinistryId][$LayerId][$OriginId]['TotalOffices'])) {
                                    $alldata[$MinistryId][$LayerId][$OriginId]['TotalOffices'] = 1;
                                } else {
                                    $alldata[$MinistryId][$LayerId][$OriginId]['TotalOffices']++;
                                }
                                $result = '';
                                $result = $this->MonitorDashboardSummary($office_key);
                                if (!isset($result)) {
                                    $table_eo = TableRegistry::get('employee_offices');
//                                    $employee_records = $table_eo->getAllEmployeeRecordID($office_key);
                                    $employee_records = 0;


//                                    $total_employee           = count($employee_records);
                                    $total_employee = 0;
                                    $user_login_history_table = TableRegistry::get('UserLoginHistory');
//                                    $totalLogin               = $user_login_history_table->countLogin(array_values($employee_records));
                                    $totalLogin = 0;
//

                                    $alldata[$MinistryId][$LayerId][$OriginId][$office_key]['TotalEmployee']
                                        = Number::format($total_employee);
                                    $alldata[$MinistryId][$LayerId][$OriginId][$office_key]['TotalLogin']
                                        = Number::format($totalLogin);

                                    $alldata[$MinistryId][$LayerId][$OriginId][$office_key]['TotalYesterdayInbox']
                                        = Number::format(0);
                                    $alldata[$MinistryId][$LayerId][$OriginId][$office_key]['TotalYesterdayOutbox']
                                        = Number::format(0);
                                    $alldata[$MinistryId][$LayerId][$OriginId][$office_key]['TotalYesterdayNothijato']
                                        = Number::format(0);
                                    $alldata[$MinistryId][$LayerId][$OriginId][$office_key]['TotalYesterdayNothiVukto']
                                        = Number::format(0);
                                    $alldata[$MinistryId][$LayerId][$OriginId][$office_key]['TotalNisponnoDak']
                                        = Number::format(0);
                                    $alldata[$MinistryId][$LayerId][$OriginId][$office_key]['TotalOnisponnoDak']
                                        = Number::format(0);
                                    $alldata[$MinistryId][$LayerId][$OriginId][$office_key]['TotalYesterdaySrijitoNote']
                                        = Number::format(0);
                                    $alldata[$MinistryId][$LayerId][$OriginId][$office_key]['TotalYesterdayDakSohoNote']
                                        = Number::format(0);
                                    $alldata[$MinistryId][$LayerId][$OriginId][$office_key]['TotalYesterdayPotrojari']
                                        = Number::format(0);
                                    $alldata[$MinistryId][$LayerId][$OriginId][$office_key]['TotalNisponnoNote']
                                        = Number::format(0);
                                    $alldata[$MinistryId][$LayerId][$OriginId][$office_key]['TotalOnisponnoNote']
                                        = Number::format(0);

                                    $alldata[$MinistryId][$LayerId][$OriginId][$office_key]['TotalEmployee']
                                        = Number::format(0);
                                    $alldata[$MinistryId][$LayerId][$OriginId][$office_key]['Name'] = $office_value;
                                    $alldata[$MinistryId][$LayerId][$OriginId][$office_key]['Status']
                                        = 0;

                                    /*                                     * ** */
                                    if (!isset($alldata[$MinistryId][$LayerId][$OriginId]['TotalEmployee'])) {
                                        $alldata[$MinistryId][$LayerId][$OriginId]['TotalEmployee'] = $total_employee;
                                    } else {
                                        $alldata[$MinistryId][$LayerId][$OriginId]['TotalEmployee'] += $total_employee;
                                    }
                                    if (!isset($alldata[$MinistryId][$LayerId][$OriginId]['TotalLogin'])) {
                                        $alldata[$MinistryId][$LayerId][$OriginId]['TotalLogin'] = $totalLogin;
                                    } else {
                                        $alldata[$MinistryId][$LayerId][$OriginId]['TotalLogin'] += $totalLogin;
                                    }

                                    if (!isset($alldata[$MinistryId][$LayerId]['TotalEmployee'])) {
                                        $alldata[$MinistryId][$LayerId]['TotalEmployee'] = $total_employee;
                                    } else {
                                        $alldata[$MinistryId][$LayerId]['TotalEmployee'] += $total_employee;
                                    }
                                    if (!isset($alldata[$MinistryId][$LayerId]['TotalLogin'])) {
                                        $alldata[$MinistryId][$LayerId]['TotalLogin'] = $totalLogin;
                                    } else {
                                        $alldata[$MinistryId][$LayerId]['TotalLogin'] += $totalLogin;
                                    }


                                    if (!isset($alldata[$MinistryId]['TotalEmployee'])) {
                                        $alldata[$MinistryId]['TotalEmployee'] = $total_employee;
                                    } else {
                                        $alldata[$MinistryId]['TotalEmployee'] += $total_employee;
                                    }
                                    if (!isset($alldata[$MinistryId]['TotalLogin'])) {
                                        $alldata[$MinistryId]['TotalLogin'] = $totalLogin;
                                    } else {
                                        $alldata[$MinistryId]['TotalLogin'] += $totalLogin;
                                    }

                                    /*                                     * ** */
                                } else {
                                    $table_eo = TableRegistry::get('employee_offices');
//                                    $employee_records = $table_eo->getAllEmployeeRecordID($office_key);
                                    $employee_records = 0;


                                    $total_employee = count($employee_records);
                                    $user_login_history_table = TableRegistry::get('UserLoginHistory');
//                                    $totalLogin               = $user_login_history_table->countLogin(array_values($employee_records));
                                    $totalLogin = 0;
//

                                    $alldata[$MinistryId][$LayerId][$OriginId][$office_key]['TotalEmployee']
                                        = Number::format($total_employee);
                                    $alldata[$MinistryId][$LayerId][$OriginId][$office_key]['Name'] = $office_value;
                                    if (!empty($result['status'])) {
                                        $alldata[$MinistryId][$LayerId][$OriginId][$office_key]['Status']
                                            = $result['status'];
                                    } else {
                                        $alldata[$MinistryId][$LayerId][$OriginId][$office_key]['Status']
                                            = 1;
                                    }

                                    $alldata[$MinistryId][$LayerId][$OriginId][$office_key]['TotalLogin']
                                        = Number::format($totalLogin);

                                    $alldata[$MinistryId][$LayerId][$OriginId][$office_key]['TotalYesterdayInbox']
                                        = Number::format($result['totalyesterdayinbox']);

                                    $alldata[$MinistryId][$LayerId][$OriginId][$office_key]['TotalYesterdayOutbox']
                                        = Number::format($result['totalyesterdayoutbox']);

                                    $alldata[$MinistryId][$LayerId][$OriginId][$office_key]['TotalYesterdayNothijato']
                                        = Number::format($result['totalyesterdaynothijato']);

                                    $alldata[$MinistryId][$LayerId][$OriginId][$office_key]['TotalYesterdayNothiVukto']
                                        = Number::format($result['totalyesterdaynothivukto']);

                                    $alldata[$MinistryId][$LayerId][$OriginId][$office_key]['TotalNisponnoDak']
                                        = Number::format($result['totalnisponnodakall']);

                                    $alldata[$MinistryId][$LayerId][$OriginId][$office_key]['TotalOnisponnoDak']
                                        = Number::format($result['totalOnisponnodakall']);

                                    $alldata[$MinistryId][$LayerId][$OriginId][$office_key]['TotalYesterdaySrijitoNote']
                                        = Number::format($result['totalyesterdaysrijitonote']);

                                    $alldata[$MinistryId][$LayerId][$OriginId][$office_key]['TotalYesterdayDakSohoNote']
                                        = Number::format($result['totalyesterdaydaksohonote']);

                                    $alldata[$MinistryId][$LayerId][$OriginId][$office_key]['TotalYesterdayPotrojari']
                                        = Number::format($result['totalyesterdaypotrojari']);

                                    $alldata[$MinistryId][$LayerId][$OriginId][$office_key]['TotalOnisponnoNote']
                                        = Number::format($result['totalOnisponnonoteall']);

                                    $alldata[$MinistryId][$LayerId][$OriginId][$office_key]['TotalNisponnoNote']
                                        = Number::format($result['totalnisponnonoteall']);

                                    /*                                     * For Ministry* */


                                    if (!isset($alldata[$MinistryId]['Offices'])) {
                                        $alldata[$MinistryId]['Offices'] = 1;
                                    } else {
                                        $alldata[$MinistryId]['Offices']++;
                                    }
                                    if (!isset($alldata[$MinistryId]['TotalEmployee'])) {
                                        $alldata[$MinistryId]['TotalEmployee'] = $total_employee;
                                    } else {
                                        $alldata[$MinistryId]['TotalEmployee'] += $total_employee;
                                    }
                                    if (!isset($alldata[$MinistryId]['TotalLogin'])) {
                                        $alldata[$MinistryId]['TotalLogin'] = $totalLogin;
                                    } else {
                                        $alldata[$MinistryId]['TotalLogin'] += $totalLogin;
                                    }
                                    if (!isset($alldata[$MinistryId]['TotalYesterdayInbox'])) {
                                        $alldata[$MinistryId]['TotalYesterdayInbox'] = $result['totalyesterdayinbox'];
                                    } else {
                                        $alldata[$MinistryId]['TotalYesterdayInbox'] += $result['totalyesterdayinbox'];
                                    }
                                    if (!isset($alldata[$MinistryId]['TotalYesterdayOutbox'])) {
                                        $alldata[$MinistryId]['TotalYesterdayOutbox'] = $result['totalyesterdayoutbox'];
                                    } else {
                                        $alldata[$MinistryId]['TotalYesterdayOutbox'] += $result['totalyesterdayoutbox'];
                                    }
                                    if (!isset($alldata[$MinistryId]['TotalYesterdayNothijato'])) {
                                        $alldata[$MinistryId]['TotalYesterdayNothijato'] = $result['totalyesterdaynothijato'];
                                    } else {
                                        $alldata[$MinistryId]['TotalYesterdayNothijato'] += $result['totalyesterdaynothijato'];
                                    }
                                    if (!isset($alldata[$MinistryId]['TotalYesterdayNothiVukto'])) {
                                        $alldata[$MinistryId]['TotalYesterdayNothiVukto'] = $result['totalyesterdaynothivukto'];
                                    } else {
                                        $alldata[$MinistryId]['TotalYesterdayNothiVukto'] += $result['totalyesterdaynothivukto'];
                                    }
                                    if (!isset($alldata[$MinistryId]['TotalNisponnoDak'])) {
                                        $alldata[$MinistryId]['TotalNisponnoDak'] = $result['totalnisponnodakall'];
                                    } else {
                                        $alldata[$MinistryId]['TotalNisponnoDak'] += $result['totalnisponnodakall'];
                                    }
                                    if (!isset($alldata[$MinistryId]['TotalOnisponnoDak'])) {
                                        $alldata[$MinistryId]['TotalOnisponnoDak'] = $result['totalOnisponnodakall'];
                                    } else {
                                        $alldata[$MinistryId]['TotalOnisponnoDak'] += $result['totalOnisponnodakall'];
                                    }

                                    if (!isset($alldata[$MinistryId]['TotalYesterdaySrijitoNote'])) {
                                        $alldata[$MinistryId]['TotalYesterdaySrijitoNote'] = $result['totalyesterdaysrijitonote'];
                                    } else {
                                        $alldata[$MinistryId]['TotalYesterdaySrijitoNote'] += $result['totalyesterdaysrijitonote'];
                                    }
                                    if (!isset($alldata[$MinistryId]['TotalYesterdayDakSohoNote'])) {
                                        $alldata[$MinistryId]['TotalYesterdayDakSohoNote'] = $result['totalyesterdaydaksohonote'];
                                    } else {
                                        $alldata[$MinistryId]['TotalYesterdayDakSohoNote'] += $result['totalyesterdaydaksohonote'];
                                    }
                                    if (!isset($alldata[$MinistryId]['TotalYesterdayPotrojari'])) {
                                        $alldata[$MinistryId]['TotalYesterdayPotrojari'] = $result['totalyesterdaypotrojari'];
                                    } else {
                                        $alldata[$MinistryId]['TotalYesterdayPotrojari'] += $result['totalyesterdaypotrojari'];
                                    }
                                    if (!isset($alldata[$MinistryId]['TotalNisponnoNote'])) {
                                        $alldata[$MinistryId]['TotalNisponnoNote'] = $result['totalnisponnonoteall'];
                                    } else {
                                        $alldata[$MinistryId]['TotalNisponnoNote'] += $result['totalnisponnonoteall'];
                                    }
                                    if (!isset($alldata[$MinistryId]['TotalOnisponnoNote'])) {
                                        $alldata[$MinistryId]['TotalOnisponnoNote'] = $result['totalOnisponnonoteall'];
                                    } else {
                                        $alldata[$MinistryId]['TotalOnisponnoNote'] += $result['totalOnisponnonoteall'];
                                    }

                                    /*                                     * For Ministry* */

//
                                    /*                                     * For Layer* */


                                    if (!isset($alldata[$MinistryId][$LayerId]['Offices'])) {
                                        $alldata[$MinistryId][$LayerId]['Offices'] = 1;
                                    } else {
                                        $alldata[$MinistryId][$LayerId]['Offices']++;
                                    }
                                    if (!isset($alldata[$MinistryId][$LayerId]['TotalEmployee'])) {
                                        $alldata[$MinistryId][$LayerId]['TotalEmployee'] = $total_employee;
                                    } else {
                                        $alldata[$MinistryId][$LayerId]['TotalEmployee'] += $total_employee;
                                    }
                                    if (!isset($alldata[$MinistryId][$LayerId]['TotalLogin'])) {
                                        $alldata[$MinistryId][$LayerId]['TotalLogin'] = $totalLogin;
                                    } else {
                                        $alldata[$MinistryId][$LayerId]['TotalLogin'] += $totalLogin;
                                    }

                                    if (!isset($alldata[$MinistryId][$LayerId]['TotalYesterdayInbox'])) {
                                        $alldata[$MinistryId][$LayerId]['TotalYesterdayInbox'] = $result['totalyesterdayinbox'];
                                    } else {
                                        $alldata[$MinistryId][$LayerId]['TotalYesterdayInbox'] += $result['totalyesterdayinbox'];
                                    }
                                    if (!isset($alldata[$MinistryId][$LayerId]['TotalYesterdayOutbox'])) {
                                        $alldata[$MinistryId][$LayerId]['TotalYesterdayOutbox'] = $result['totalyesterdayoutbox'];
                                    } else {
                                        $alldata[$MinistryId][$LayerId]['TotalYesterdayOutbox'] += $result['totalyesterdayoutbox'];
                                    }
                                    if (!isset($alldata[$MinistryId][$LayerId]['TotalYesterdayNothijato'])) {
                                        $alldata[$MinistryId][$LayerId]['TotalYesterdayNothijato'] = $result['totalyesterdaynothijato'];
                                    } else {
                                        $alldata[$MinistryId][$LayerId]['TotalYesterdayNothijato'] += $result['totalyesterdaynothijato'];
                                    }
                                    if (!isset($alldata[$MinistryId][$LayerId]['TotalYesterdayNothiVukto'])) {
                                        $alldata[$MinistryId][$LayerId]['TotalYesterdayNothiVukto'] = $result['totalyesterdaynothivukto'];
                                    } else {
                                        $alldata[$MinistryId][$LayerId]['TotalYesterdayNothiVukto'] += $result['totalyesterdaynothivukto'];
                                    }
                                    if (!isset($alldata[$MinistryId][$LayerId]['TotalNisponnoDak'])) {
                                        $alldata[$MinistryId][$LayerId]['TotalNisponnoDak'] = $result['totalnisponnodakall'];
                                    } else {
                                        $alldata[$MinistryId][$LayerId]['TotalNisponnoDak'] += $result['totalnisponnodakall'];
                                    }
                                    if (!isset($alldata[$MinistryId][$LayerId]['TotalOnisponnoDak'])) {
                                        $alldata[$MinistryId][$LayerId]['TotalOnisponnoDak'] = $result['totalOnisponnodakall'];
                                    } else {
                                        $alldata[$MinistryId][$LayerId]['TotalOnisponnoDak'] += $result['totalOnisponnodakall'];
                                    }

                                    if (!isset($alldata[$MinistryId][$LayerId]['TotalYesterdaySrijitoNote'])) {
                                        $alldata[$MinistryId][$LayerId]['TotalYesterdaySrijitoNote']
                                            = $result['totalyesterdaysrijitonote'];
                                    } else {
                                        $alldata[$MinistryId][$LayerId]['TotalYesterdaySrijitoNote'] += $result['totalyesterdaysrijitonote'];
                                    }
                                    if (!isset($alldata[$MinistryId][$LayerId]['TotalYesterdayDakSohoNote'])) {
                                        $alldata[$MinistryId][$LayerId]['TotalYesterdayDakSohoNote']
                                            = $result['totalyesterdaydaksohonote'];
                                    } else {
                                        $alldata[$MinistryId][$LayerId]['TotalYesterdayDakSohoNote'] += $result['totalyesterdaydaksohonote'];
                                    }
                                    if (!isset($alldata[$MinistryId][$LayerId]['TotalYesterdayPotrojari'])) {
                                        $alldata[$MinistryId][$LayerId]['TotalYesterdayPotrojari'] = ($result['totalyesterdaypotrojari']);
                                    } else {
                                        $alldata[$MinistryId][$LayerId]['TotalYesterdayPotrojari'] += $result['totalyesterdaypotrojari'];
                                    }
                                    if (!isset($alldata[$MinistryId][$LayerId]['TotalNisponnoNote'])) {
                                        $alldata[$MinistryId][$LayerId]['TotalNisponnoNote'] = $result['totalnisponnonoteall'];
                                    } else {
                                        $alldata[$MinistryId][$LayerId]['TotalNisponnoNote'] += $result['totalnisponnonoteall'];
                                    }
                                    if (!isset($alldata[$MinistryId][$LayerId]['TotalOnisponnoNote'])) {
                                        $alldata[$MinistryId][$LayerId]['TotalOnisponnoNote'] = $result['totalOnisponnonoteall'];
                                    } else {
                                        $alldata[$MinistryId][$LayerId]['TotalOnisponnoNote'] += $result['totalOnisponnonoteall'];
                                    }


                                    /*                                     * For Layer* */

                                    /*                                     * For Origin* */


                                    if (!isset($alldata[$MinistryId][$LayerId][$OriginId]['Offices'])) {
                                        $alldata[$MinistryId][$LayerId][$OriginId]['Offices'] = 1;
                                    } else {
                                        $alldata[$MinistryId][$LayerId][$OriginId]['Offices']++;
                                    }
                                    if (!isset($alldata[$MinistryId][$LayerId][$OriginId]['TotalEmployee'])) {
                                        $alldata[$MinistryId][$LayerId][$OriginId]['TotalEmployee'] = $total_employee;
                                    } else {
                                        $alldata[$MinistryId][$LayerId][$OriginId]['TotalEmployee'] += $total_employee;
                                    }
                                    if (!isset($alldata[$MinistryId][$LayerId][$OriginId]['TotalLogin'])) {
                                        $alldata[$MinistryId][$LayerId][$OriginId]['TotalLogin'] = $totalLogin;
                                    } else {
                                        $alldata[$MinistryId][$LayerId][$OriginId]['TotalLogin'] += $totalLogin;
                                    }

                                    if (!isset($alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayInbox'])) {
                                        $alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayInbox']
                                            = $result['totalyesterdayinbox'];
                                    } else {
                                        $alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayInbox'] += $result['totalyesterdayinbox'];
                                    }
                                    if (!isset($alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayOutbox'])) {
                                        $alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayOutbox']
                                            = $result['totalyesterdayoutbox'];
                                    } else {
                                        $alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayOutbox'] += $result['totalyesterdayoutbox'];
                                    }
                                    if (!isset($alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayNothijato'])) {
                                        $alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayNothijato']
                                            = $result['totalyesterdaynothijato'];
                                    } else {
                                        $alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayNothijato'] += $result['totalyesterdaynothijato'];
                                    }
                                    if (!isset($alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayNothiVukto'])) {
                                        $alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayNothiVukto']
                                            = $result['totalyesterdaynothivukto'];
                                    } else {
                                        $alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayNothiVukto'] += $result['totalyesterdaynothivukto'];
                                    }
                                    if (!isset($alldata[$MinistryId][$LayerId][$OriginId]['TotalNisponnoDak'])) {
                                        $alldata[$MinistryId][$LayerId][$OriginId]['TotalNisponnoDak']
                                            = $result['totalnisponnodakall'];
                                    } else {
                                        $alldata[$MinistryId][$LayerId][$OriginId]['TotalNisponnoDak'] += $result['totalnisponnodakall'];
                                    }
                                    if (!isset($alldata[$MinistryId][$LayerId][$OriginId]['TotalOnisponnoDak'])) {
                                        $alldata[$MinistryId][$LayerId][$OriginId]['TotalOnisponnoDak']
                                            = $result['totalOnisponnodakall'];
                                    } else {
                                        $alldata[$MinistryId][$LayerId][$OriginId]['TotalOnisponnoDak'] += $result['totalOnisponnodakall'];
                                    }

                                    if (!isset($alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdaySrijitoNote'])) {
                                        $alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdaySrijitoNote']
                                            = $result['totalyesterdaysrijitonote'];
                                    } else {
                                        $alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdaySrijitoNote'] += $result['totalyesterdaysrijitonote'];
                                    }
                                    if (!isset($alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayDakSohoNote'])) {
                                        $alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayDakSohoNote']
                                            = $result['totalyesterdaydaksohonote'];
                                    } else {
                                        $alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayDakSohoNote'] += $result['totalyesterdaydaksohonote'];
                                    }
                                    if (!isset($alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayPotrojari'])) {
                                        $alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayPotrojari']
                                            = $result['totalyesterdaypotrojari'];
                                    } else {
                                        $alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayPotrojari'] += $result['totalyesterdaypotrojari'];
                                    }
                                    if (!isset($alldata[$MinistryId][$LayerId][$OriginId]['TotalNisponnoNote'])) {
                                        $alldata[$MinistryId][$LayerId][$OriginId]['TotalNisponnoNote']
                                            = $result['totalnisponnonoteall'];
                                    } else {
                                        $alldata[$MinistryId][$LayerId][$OriginId]['TotalNisponnoNote'] += $result['totalnisponnonoteall'];
                                    }
                                    if (!isset($alldata[$MinistryId][$LayerId][$OriginId]['TotalOnisponnoNote'])) {
                                        $alldata[$MinistryId][$LayerId][$OriginId]['TotalOnisponnoNote']
                                            = $result['totalOnisponnonoteall'];
                                    } else {
                                        $alldata[$MinistryId][$LayerId][$OriginId]['TotalOnisponnoNote'] += $result['totalOnisponnonoteall'];
                                    }
                                }
                            }
                            /*                             * *Making Bangla Number** */
                            /*                             * *Making Bangla Number** */

                            /*                             * For Origin* */
                            $alldata[$MinistryId][$LayerId][$OriginId]['Name'] = $origin_name['office_name_bng'];
                            if (!isset($alldata[$MinistryId][$LayerId][$OriginId]['TotalOffices'])) {
                                $alldata[$MinistryId][$LayerId][$OriginId]['TotalOffices'] = Number::format(0);
                            } else {
                                $alldata[$MinistryId][$LayerId][$OriginId]['TotalOffices'] = Number::format($alldata[$MinistryId][$LayerId][$OriginId]['TotalOffices']);
                            }
                            if (!isset($alldata[$MinistryId][$LayerId][$OriginId]['TotalLogin'])) {
                                $alldata[$MinistryId][$LayerId][$OriginId]['TotalLogin'] = Number::format(0);
                            } else {
                                $alldata[$MinistryId][$LayerId][$OriginId]['TotalLogin'] = Number::format($alldata[$MinistryId][$LayerId][$OriginId]['TotalLogin']);
                            }
                            if (!isset($alldata[$MinistryId][$LayerId][$OriginId]['Offices'])) {
                                $alldata[$MinistryId][$LayerId][$OriginId]['Offices'] = Number::format(0);
                            } else {
                                $alldata[$MinistryId][$LayerId][$OriginId]['Offices'] = Number::format($alldata[$MinistryId][$LayerId][$OriginId]['Offices']);
                            }
                            if (!isset($alldata[$MinistryId][$LayerId][$OriginId]['TotalEmployee'])) {
                                $alldata[$MinistryId][$LayerId][$OriginId]['TotalEmployee'] = Number::format(0);
                            } else {
                                $alldata[$MinistryId][$LayerId][$OriginId]['TotalEmployee'] = Number::format($alldata[$MinistryId][$LayerId][$OriginId]['TotalEmployee']);
                            }

                            if (!isset($alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayInbox'])) {
                                $alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayInbox'] = Number::format(0);
                            } else {
                                $alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayInbox'] = Number::format($alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayInbox']);
                            }
                            if (!isset($alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayOutbox'])) {
                                $alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayOutbox'] = Number::format(0);
                            } else {
                                $alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayOutbox'] = Number::format($alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayOutbox']);
                            }
                            if (!isset($alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayNothijato'])) {
                                $alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayNothijato']
                                    = Number::format(0);
                            } else {
                                $alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayNothijato']
                                    = Number::format($alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayNothijato']);
                            }
                            if (!isset($alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayNothiVukto'])) {
                                $alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayNothiVukto']
                                    = Number::format(0);
                            } else {
                                $alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayNothiVukto']
                                    = Number::format($alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayNothiVukto']);
                            }
                            if (!isset($alldata[$MinistryId][$LayerId][$OriginId]['TotalNisponnoDak'])) {
                                $alldata[$MinistryId][$LayerId][$OriginId]['TotalNisponnoDak'] = Number::format(0);
                            } else {
                                $alldata[$MinistryId][$LayerId][$OriginId]['TotalNisponnoDak'] = Number::format($alldata[$MinistryId][$LayerId][$OriginId]['TotalNisponnoDak']);
                            }
                            if (!isset($alldata[$MinistryId][$LayerId][$OriginId]['TotalOnisponnoDak'])) {
                                $alldata[$MinistryId][$LayerId][$OriginId]['TotalOnisponnoDak'] = Number::format(0);
                            } else {
                                $alldata[$MinistryId][$LayerId][$OriginId]['TotalOnisponnoDak'] = Number::format($alldata[$MinistryId][$LayerId][$OriginId]['TotalOnisponnoDak']);
                            }

                            if (!isset($alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdaySrijitoNote'])) {
                                $alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdaySrijitoNote']
                                    = Number::format(0);
                            } else {
                                $alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdaySrijitoNote']
                                    = Number::format($alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdaySrijitoNote']);
                            }
                            if (!isset($alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayDakSohoNote'])) {
                                $alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayDakSohoNote']
                                    = Number::format(0);
                            } else {
                                $alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayDakSohoNote']
                                    = Number::format($alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayDakSohoNote']);
                            }
                            if (!isset($alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayPotrojari'])) {
                                $alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayPotrojari']
                                    = Number::format(0);
                            } else {
                                $alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayPotrojari']
                                    = Number::format($alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayPotrojari']);
                            }
                            if (!isset($alldata[$MinistryId][$LayerId][$OriginId]['TotalNisponnoNote'])) {
                                $alldata[$MinistryId][$LayerId][$OriginId]['TotalNisponnoNote'] = Number::format(0);
                            } else {
                                $alldata[$MinistryId][$LayerId][$OriginId]['TotalNisponnoNote'] = Number::format($alldata[$MinistryId][$LayerId][$OriginId]['TotalNisponnoNote']);
                            }
                            if (!isset($alldata[$MinistryId][$LayerId][$OriginId]['TotalOnisponnoNote'])) {
                                $alldata[$MinistryId][$LayerId][$OriginId]['TotalOnisponnoNote'] = Number::format(0);
                            } else {
                                $alldata[$MinistryId][$LayerId][$OriginId]['TotalOnisponnoNote'] = Number::format($alldata[$MinistryId][$LayerId][$OriginId]['TotalOnisponnoNote']);
                            }

                            /*                             * For Origin* */
                        }
                    }

                    /*                     * For Layers Bangla** */
                    $alldata[$MinistryId][$LayerId]['Name'] = $layer_name['layer_name_bng'];
                    if (!isset($alldata[$MinistryId][$LayerId]['TotalOffices'])) {
                        $alldata[$MinistryId][$LayerId]['TotalOffices'] = Number::format(0);
                    } else {
                        $alldata[$MinistryId][$LayerId]['TotalOffices'] = Number::format($alldata[$MinistryId][$LayerId]['TotalOffices']);
                    }
                    if (!isset($alldata[$MinistryId][$LayerId]['TotalLogin'])) {
                        $alldata[$MinistryId][$LayerId]['TotalLogin'] = Number::format(0);
                    } else {
                        $alldata[$MinistryId][$LayerId]['TotalLogin'] = Number::format($alldata[$MinistryId][$LayerId]['TotalLogin']);
                    }
                    if (!isset($alldata[$MinistryId][$LayerId]['Offices'])) {
                        $alldata[$MinistryId][$LayerId]['Offices'] = Number::format(0);
                    } else {
                        $alldata[$MinistryId][$LayerId]['Offices'] = Number::format($alldata[$MinistryId][$LayerId]['Offices']);
                    }
                    if (!isset($alldata[$MinistryId][$LayerId]['TotalEmployee'])) {
                        $alldata[$MinistryId][$LayerId]['TotalEmployee'] = Number::format(0);
                    } else {
                        $alldata[$MinistryId][$LayerId]['TotalEmployee'] = Number::format($alldata[$MinistryId][$LayerId]['TotalEmployee']);
                    }

                    if (!isset($alldata[$MinistryId][$LayerId]['TotalYesterdayInbox'])) {
                        $alldata[$MinistryId][$LayerId]['TotalYesterdayInbox'] = Number::format(0);
                    } else {
                        $alldata[$MinistryId][$LayerId]['TotalYesterdayInbox'] = Number::format($alldata[$MinistryId][$LayerId]['TotalYesterdayInbox']);
                    }
                    if (!isset($alldata[$MinistryId][$LayerId]['TotalYesterdayOutbox'])) {
                        $alldata[$MinistryId][$LayerId]['TotalYesterdayOutbox'] = Number::format(0);
                    } else {
                        $alldata[$MinistryId][$LayerId]['TotalYesterdayOutbox'] = Number::format($alldata[$MinistryId][$LayerId]['TotalYesterdayOutbox']);
                    }
                    if (!isset($alldata[$MinistryId][$LayerId]['TotalYesterdayNothijato'])) {
                        $alldata[$MinistryId][$LayerId]['TotalYesterdayNothijato'] = Number::format(0);
                    } else {
                        $alldata[$MinistryId][$LayerId]['TotalYesterdayNothijato'] = Number::format($alldata[$MinistryId][$LayerId]['TotalYesterdayNothijato']);
                    }
                    if (!isset($alldata[$MinistryId][$LayerId]['TotalYesterdayNothiVukto'])) {
                        $alldata[$MinistryId][$LayerId]['TotalYesterdayNothiVukto'] = Number::format(0);
                    } else {
                        $alldata[$MinistryId][$LayerId]['TotalYesterdayNothiVukto'] = Number::format($alldata[$MinistryId][$LayerId]['TotalYesterdayNothiVukto']);
                    }
                    if (!isset($alldata[$MinistryId][$LayerId]['TotalNisponnoDak'])) {
                        $alldata[$MinistryId][$LayerId]['TotalNisponnoDak'] = Number::format(0);
                    } else {
                        $alldata[$MinistryId][$LayerId]['TotalNisponnoDak'] = Number::format($alldata[$MinistryId][$LayerId]['TotalNisponnoDak']);
                    }
                    if (!isset($alldata[$MinistryId][$LayerId]['TotalOnisponnoDak'])) {
                        $alldata[$MinistryId][$LayerId]['TotalOnisponnoDak'] = Number::format(0);
                    } else {
                        $alldata[$MinistryId][$LayerId]['TotalOnisponnoDak'] = Number::format($alldata[$MinistryId][$LayerId]['TotalOnisponnoDak']);
                    }

                    if (!isset($alldata[$MinistryId][$LayerId]['TotalYesterdaySrijitoNote'])) {
                        $alldata[$MinistryId][$LayerId]['TotalYesterdaySrijitoNote'] = Number::format(0);
                    } else {
                        $alldata[$MinistryId][$LayerId]['TotalYesterdaySrijitoNote'] = Number::format($alldata[$MinistryId][$LayerId]['TotalYesterdaySrijitoNote']);
                    }
                    if (!isset($alldata[$MinistryId][$LayerId]['TotalYesterdayDakSohoNote'])) {
                        $alldata[$MinistryId][$LayerId]['TotalYesterdayDakSohoNote'] = Number::format(0);
                    } else {
                        $alldata[$MinistryId][$LayerId]['TotalYesterdayDakSohoNote'] = Number::format($alldata[$MinistryId][$LayerId]['TotalYesterdayDakSohoNote']);
                    }
                    if (!isset($alldata[$MinistryId][$LayerId]['TotalYesterdayPotrojari'])) {
                        $alldata[$MinistryId][$LayerId]['TotalYesterdayPotrojari'] = Number::format(0);
                    } else {
                        $alldata[$MinistryId][$LayerId]['TotalYesterdayPotrojari'] = Number::format($alldata[$MinistryId][$LayerId]['TotalYesterdayPotrojari']);
                    }
                    if (!isset($alldata[$MinistryId][$LayerId]['TotalNisponnoNote'])) {
                        $alldata[$MinistryId][$LayerId]['TotalNisponnoNote'] = Number::format(0);
                    } else {
                        $alldata[$MinistryId][$LayerId]['TotalNisponnoNote'] = Number::format($alldata[$MinistryId][$LayerId]['TotalNisponnoNote']);
                    }
                    if (!isset($alldata[$MinistryId][$LayerId]['TotalOnisponnoNote'])) {
                        $alldata[$MinistryId][$LayerId]['TotalOnisponnoNote'] = Number::format(0);
                    } else {
                        $alldata[$MinistryId][$LayerId]['TotalOnisponnoNote'] = Number::format($alldata[$MinistryId][$LayerId]['TotalOnisponnoNote']);
                    }

                    /*                     * For Layers Bangla** */
                }
            }
            /*             * For Ministry Bangla** */
            $alldata[$MinistryId]['Name'] = $ministry_name['name_bng'];

            if (!isset($alldata[$MinistryId]['TotalOffices'])) {
                $alldata[$MinistryId]['EnTotalOffices'] = 0;
                $alldata[$MinistryId]['TotalOffices'] = Number::format(0);
            } else {
                $alldata[$MinistryId]['EnTotalOffices'] = $alldata[$MinistryId]['TotalOffices'];
                $alldata[$MinistryId]['TotalOffices'] = Number::format($alldata[$MinistryId]['TotalOffices']);
            }

            if (!isset($alldata[$MinistryId]['TotalLogin'])) {
                $alldata[$MinistryId]['EnTotalLogin'] = 0;
                $alldata[$MinistryId]['TotalLogin'] = Number::format(0);
            } else {
                $alldata[$MinistryId]['EnTotalLogin'] = $alldata[$MinistryId]['TotalLogin'];
                $alldata[$MinistryId]['TotalLogin'] = Number::format($alldata[$MinistryId]['TotalLogin']);
            }
            if (!isset($alldata[$MinistryId]['Offices'])) {
                $alldata[$MinistryId]['EnOffices'] = 0;
                $alldata[$MinistryId]['Offices'] = Number::format(0);
            } else {
                $alldata[$MinistryId]['EnOffices'] = $alldata[$MinistryId]['Offices'];
                $alldata[$MinistryId]['Offices'] = Number::format($alldata[$MinistryId]['Offices']);
            }
            if (!isset($alldata[$MinistryId]['TotalEmployee'])) {
                $alldata[$MinistryId]['EnTotalEmployee'] = 0;
                $alldata[$MinistryId]['TotalEmployee'] = Number::format(0);
            } else {
                $alldata[$MinistryId]['EnTotalEmployee'] = $alldata[$MinistryId]['TotalEmployee'];
                $alldata[$MinistryId]['TotalEmployee'] = Number::format($alldata[$MinistryId]['TotalEmployee']);
            }

            if (!isset($alldata[$MinistryId]['TotalYesterdayOutbox'])) {
                $alldata[$MinistryId]['EnTotalYesterdayOutbox'] = 0;
                $alldata[$MinistryId]['TotalYesterdayOutbox'] = Number::format(0);
            } else {
                $alldata[$MinistryId]['EnTotalYesterdayOutbox'] = $alldata[$MinistryId]['TotalYesterdayOutbox'];
                $alldata[$MinistryId]['TotalYesterdayOutbox'] = Number::format($alldata[$MinistryId]['TotalYesterdayOutbox']);
            }

            if (!isset($alldata[$MinistryId]['TotalYesterdayInbox'])) {
                $alldata[$MinistryId]['EnTotalYesterdayInbox'] = 0;
                $alldata[$MinistryId]['TotalYesterdayInbox'] = Number::format(0);
            } else {
                $alldata[$MinistryId]['EnTotalYesterdayInbox'] = $alldata[$MinistryId]['TotalYesterdayInbox'];
                $alldata[$MinistryId]['TotalYesterdayInbox'] = Number::format($alldata[$MinistryId]['TotalYesterdayInbox']);
            }

            if (!isset($alldata[$MinistryId]['TotalYesterdayNothijato'])) {
                $alldata[$MinistryId]['EnTotalYesterdayNothijato'] = 0;
                $alldata[$MinistryId]['TotalYesterdayNothijato'] = Number::format(0);
            } else {
                $alldata[$MinistryId]['EnTotalYesterdayNothijato'] = $alldata[$MinistryId]['TotalYesterdayNothijato'];
                $alldata[$MinistryId]['TotalYesterdayNothijato'] = Number::format($alldata[$MinistryId]['TotalYesterdayNothijato']);
            }

            if (!isset($alldata[$MinistryId]['TotalYesterdayNothiVukto'])) {
                $alldata[$MinistryId]['EnTotalYesterdayNothiVukto'] = 0;
                $alldata[$MinistryId]['TotalYesterdayNothiVukto'] = Number::format(0);
            } else {
                $alldata[$MinistryId]['EnTotalYesterdayNothiVukto'] = $alldata[$MinistryId]['TotalYesterdayNothiVukto'];
                $alldata[$MinistryId]['TotalYesterdayNothiVukto'] = Number::format($alldata[$MinistryId]['TotalYesterdayNothiVukto']);
            }
            if (!isset($alldata[$MinistryId]['TotalNisponnoDak'])) {
                $alldata[$MinistryId]['EnTotalNisponnoDak'] = 0;
                $alldata[$MinistryId]['TotalNisponnoDak'] = Number::format(0);
            } else {
                $alldata[$MinistryId]['EnTotalNisponnoDak'] = $alldata[$MinistryId]['TotalNisponnoDak'];
                $alldata[$MinistryId]['TotalNisponnoDak'] = Number::format($alldata[$MinistryId]['TotalNisponnoDak']);
            }
            if (!isset($alldata[$MinistryId]['TotalOnisponnoDak'])) {
                $alldata[$MinistryId]['EnTotalOnisponnoDak'] = 0;
                $alldata[$MinistryId]['TotalOnisponnoDak'] = Number::format(0);
            } else {
                $alldata[$MinistryId]['EnTotalOnisponnoDak'] = $alldata[$MinistryId]['TotalOnisponnoDak'];
                $alldata[$MinistryId]['TotalOnisponnoDak'] = Number::format($alldata[$MinistryId]['TotalOnisponnoDak']);
            }

            if (!isset($alldata[$MinistryId]['TotalYesterdaySrijitoNote'])) {
                $alldata[$MinistryId]['EnTotalYesterdaySrijitoNote'] = 0;
                $alldata[$MinistryId]['TotalYesterdaySrijitoNote'] = Number::format(0);
            } else {
                $alldata[$MinistryId]['EnTotalYesterdaySrijitoNote'] = $alldata[$MinistryId]['TotalYesterdaySrijitoNote'];
                $alldata[$MinistryId]['TotalYesterdaySrijitoNote'] = Number::format($alldata[$MinistryId]['TotalYesterdaySrijitoNote']);
            }
            if (!isset($alldata[$MinistryId]['TotalYesterdayDakSohoNote'])) {
                $alldata[$MinistryId]['EnTotalYesterdayDakSohoNote'] = 0;
                $alldata[$MinistryId]['TotalYesterdayDakSohoNote'] = Number::format(0);
            } else {
                $alldata[$MinistryId]['EnTotalYesterdayDakSohoNote'] = $alldata[$MinistryId]['TotalYesterdayDakSohoNote'];
                $alldata[$MinistryId]['TotalYesterdayDakSohoNote'] = Number::format($alldata[$MinistryId]['TotalYesterdayDakSohoNote']);
            }

            if (!isset($alldata[$MinistryId]['TotalYesterdayPotrojari'])) {
                $alldata[$MinistryId]['EnTotalYesterdayPotrojari'] = 0;
                $alldata[$MinistryId]['TotalYesterdayPotrojari'] = Number::format(0);
            } else {
                $alldata[$MinistryId]['EnTotalYesterdayPotrojari'] = $alldata[$MinistryId]['TotalYesterdayPotrojari'];
                $alldata[$MinistryId]['TotalYesterdayPotrojari'] = Number::format($alldata[$MinistryId]['TotalYesterdayPotrojari']);
            }

            if (!isset($alldata[$MinistryId]['TotalNisponnoNote'])) {
                $alldata[$MinistryId]['EnTotalNisponnoNote'] = 0;
                $alldata[$MinistryId]['TotalNisponnoNote'] = Number::format(0);
            } else {
                $alldata[$MinistryId]['EnTotalNisponnoNote'] = $alldata[$MinistryId]['TotalNisponnoNote'];
                $alldata[$MinistryId]['TotalNisponnoNote'] = Number::format($alldata[$MinistryId]['TotalNisponnoNote']);
            }

            if (!isset($alldata[$MinistryId]['TotalOnisponnoNote'])) {
                $alldata[$MinistryId]['EnTotalOnisponnoNote'] = 0;
                $alldata[$MinistryId]['TotalOnisponnoNote'] = Number::format(0);
            } else {
                $alldata[$MinistryId]['EnTotalOnisponnoNote'] = $alldata[$MinistryId]['TotalOnisponnoNote'];
                $alldata[$MinistryId]['TotalOnisponnoNote'] = Number::format($alldata[$MinistryId]['TotalOnisponnoNote']);
            }
            /*             * For Ministry Bangla** */
//            }


            $this->response->type('application/json');
            $this->response->body(json_encode($alldata));
            return $this->response;
        }
    }

    public function checkUndoneTask()
    {

        $auth = $this->Auth->user();
        $result = array();

        $tableofficeemployee = TableRegistry::get('EmployeeOffices');

        if (empty($auth)) {
            return $this->redirect('/login');
        }

        if ($auth['user_role_id'] > 2) {
            $officeexist = $tableofficeemployee->getEmployeeOffices($auth['employee_record_id']);
            if (!empty($officeexist)) {
                $undoneTask = $this->checkUndone();
            }
            $result = array();
            if (empty($undoneTask['DraftPotro']) && empty($undoneTask['DraftSummary']) && empty($undoneTask['PendingDak'])) {
                $result['status'] = 'success';
            } else {
                $result['status'] = 'error';
                $result['data'] = array(
                    'draft' => (($undoneTask['DraftPotro'] + $undoneTask['DraftSummary']) > 0 ? \Cake\I18n\Number::format($undoneTask['DraftPotro']
                        + $undoneTask['DraftSummary']) : 0),
                    'pendingdak' => $undoneTask['PendingDak'] > 0 ? \Cake\I18n\Number::format($undoneTask['PendingDak'])
                        : 0,
                );
            }
        } else {
            $result['status'] = 'success';
        }

        echo json_encode($result);
        die;
    }

    public function unitDashboard($office_id = 0, $unit_id = 0)
    {
        set_time_limit(0);
        $this->switchOffice($office_id, 'DashboardOffice');
        $data = [];
        $auth = 0;
        if ($this->Auth->user())
            $auth = 1;
        $this->set('auth', $auth);
        if ($auth == 0)
            $this->layout = 'dashboard';
        if (empty($office_id)) {
            $employeeinformation = $this->getCurrentDakSection();
            $office_id = $employeeinformation['office_id'];
        }

        $table = TableRegistry::get('Reports');
        $Dashboardtable = TableRegistry::get('Dashboard');
        if (!empty($office_id) && !empty($unit_id)) {
            $unitTable = TableRegistry::get('OfficeUnits');
            $unit_name = $unitTable->get($unit_id);

            $EmployeeOfficeTable = TableRegistry::get('EmployeeOffices');

            $organogramArray = $EmployeeOfficeTable->getEmployeeOfficeRecordsByOfficeId($office_id,
                $unit_id);

            foreach ($organogramArray as $organograms) {
                echo $organograms['office_unit_organogram_id'];
                $result = $Dashboardtable->designationSummary($office_id, $unit_id, $organograms['office_unit_organogram_id']);
//                pr($result);die;
                $data[$organograms['office_unit_organogram_id']]['totaltodayinbox'] = $result['totaltodayinbox'];
                $data[$organograms['office_unit_organogram_id']]['totalyesterdayinbox'] = $result['totalyesterdayinbox'];
                $data[$organograms['office_unit_organogram_id']]['totalinboxall'] = $result['totalinboxall'];

                $data[$organograms['office_unit_organogram_id']]['totaltodayoutbox'] = $result['totaltodayoutbox'];
                $data[$organograms['office_unit_organogram_id']]['totalyesterdayoutbox'] = $result['totalyesterdayoutbox'];
                $data[$organograms['office_unit_organogram_id']]['totaloutboxall'] = $result['totaloutboxall'];

                $data[$organograms['office_unit_organogram_id']]['totaltodaynothivukto'] = $result['totaltodaynothivukto'];
                $data[$organograms['office_unit_organogram_id']]['totalyesterdaynothivukto'] = $result['totalyesterdaynothivukto'];
                $data[$organograms['office_unit_organogram_id']]['totalnothivuktoall'] = $result['totalnothivuktoall'];

                $data[$organograms['office_unit_organogram_id']]['totaltodaynothijato'] = $result['totaltodaynothijato'];
                $data[$organograms['office_unit_organogram_id']]['totalyesterdaynothijato'] = $result['totalyesterdaynothijato'];
                $data[$organograms['office_unit_organogram_id']]['totalnothijatoall'] = $result['totalnothijatoall'];

                $data[$organograms['office_unit_organogram_id']]['totaltodaysrijitonote'] = $result['totaltodaysrijitonote'];
                $data[$organograms['office_unit_organogram_id']]['totalyesterdaysrijitonote'] = $result['totalyesterdaysrijitonote'];
                $data[$organograms['office_unit_organogram_id']]['totalsrijitonoteall'] = !empty($result['totalsrijitonoteall']) ? $result['totalsrijitonoteall'] : 0;

                $data[$organograms['office_unit_organogram_id']]['totaltodaypotrojari'] = $result['totaltodaypotrojari'];
                $data[$organograms['office_unit_organogram_id']]['totalyesterdaypotrojari'] = $result['totalyesterdaypotrojari'];
                $data[$organograms['office_unit_organogram_id']]['totalpotrojariall'] = $result['totalpotrojariall'];

                $data[$organograms['office_unit_organogram_id']]['totaltodaydaksohonote'] = $result['totaltodaydaksohonote'];
                $data[$organograms['office_unit_organogram_id']]['totalyesterdaydaksohonote'] = $result['totalyesterdaydaksohonote'];
                $data[$organograms['office_unit_organogram_id']]['totaldaksohonoteall'] = !empty($result['totaldaksohonoteall']) ? $result['totaldaksohonoteall'] : 0;

                $data[$organograms['office_unit_organogram_id']]['totaltodaynisponnodak'] = $result['totaltodaynisponnodak'];
                $data[$organograms['office_unit_organogram_id']]['totalyesterdaynisponnodak'] = $result['totalyesterdaynisponnodak'];
                $data[$organograms['office_unit_organogram_id']]['totalnisponnodakall'] = !empty($result['totalnisponnodakall']) ? $result['totalnisponnodakall'] : 0;

                $data[$organograms['office_unit_organogram_id']]['totaltodaynisponnonote'] = $result['totaltodaynisponnonote'];
                $data[$organograms['office_unit_organogram_id']]['totalyesterdaynisponnonote'] = $result['totalyesterdaynisponnonote'];
                $data[$organograms['office_unit_organogram_id']]['totalnisponnonoteall'] = !empty($result['totalnisponnonoteall']) ? $result['totalnisponnonoteall'] : 0;

                $data[$organograms['office_unit_organogram_id']]['totaltodaynisponnopotrojari'] = $result['totaltodaynisponnopotrojari'];
                $data[$organograms['office_unit_organogram_id']]['totalyesterdaynisponnopotrojari'] = $result['totalyesterdaynisponnopotrojari'];
                $data[$organograms['office_unit_organogram_id']]['totalnisponnopotrojariall'] = !empty($result['totalnisponnopotrojariall']) ? $result['totalnisponnopotrojariall'] : 0;

                $data[$organograms['office_unit_organogram_id']]['totaltodayOnisponnodak'] = $result['totaltodayOnisponnodak'];
                $data[$organograms['office_unit_organogram_id']]['totalyesterdayOnisponnodak'] = $result['totalyesterdayOnisponnodak'];
                $data[$organograms['office_unit_organogram_id']]['totalOnisponnodakall'] = !empty($result['totalOnisponnodakall']) ? $result['totalOnisponnodakall'] : 0;

                $data[$organograms['office_unit_organogram_id']]['totaltodayOnisponnonote'] = $result['totaltodayOnisponnonote'];
                $data[$organograms['office_unit_organogram_id']]['totalyesterdayOnisponnonote'] = $result['totalyesterdayOnisponnonote'];
                $data[$organograms['office_unit_organogram_id']]['totalOnisponnonoteall'] = !empty($result['totalOnisponnonoteall']) ? $result['totalOnisponnonoteall'] : 0;

//                $data[$organograms['office_unit_organogram_id']]['totaltodayOnisponno']     = $result['totaltodayOnisponno'];
//                $data[$organograms['office_unit_organogram_id']]['totalyesterdayOnisponno'] = $result['totalyesterdayOnisponno'];
//                $data[$organograms['office_unit_organogram_id']]['totalOnisponnoall']       = $result['totalOnisponnonoteall'];

                $data[$organograms['office_unit_organogram_id']]['unit_id'] = $organograms['office_unit_id'];
                $data[$organograms['office_unit_organogram_id']]['office_id'] = $organograms['office_id'];

                $data[$organograms['office_unit_organogram_id']]['ajax_request'] = $result['ajax_request'];


                $data[$organograms['office_unit_organogram_id']]['name'] = $organograms['name_bng'] . ', ' . $organograms['designation'];

                $data3 = $table->dakinbox($office_id,
                    $unit_id, $organograms['office_unit_organogram_id'],
                    'DATEDIFF(NOW(),date(created))>=3 and DATEDIFF(NOW(),date(created))<5');
                $data5 = $table->dakinbox($office_id,
                    $unit_id, $organograms['office_unit_organogram_id'],
                    'DATEDIFF(NOW(),date(created))>=5 and DATEDIFF(NOW(),date(created))<10');
                $data10 = $table->dakinbox($office_id,
                    $unit_id, $organograms['office_unit_organogram_id'],
                    'DATEDIFF(NOW(),date(created))>=10 and DATEDIFF(NOW(),date(created))<15');
                $data15 = $table->dakinbox($office_id,
                    $unit_id, $organograms['office_unit_organogram_id'],
                    'DATEDIFF(NOW(),date(created))>=15 and DATEDIFF(NOW(),date(created))<30');
                $data30 = $table->dakinbox($office_id,
                    $unit_id, $organograms['office_unit_organogram_id'],
                    'DATEDIFF(NOW(),date(created))>=30');
                $data[$organograms['office_unit_organogram_id']]['dak'] = [
                    'd3' => \Cake\I18n\Number::format($data3),
                    'd5' => \Cake\I18n\Number::format($data5),
                    'd10' => \Cake\I18n\Number::format($data10),
                    'd15' => \Cake\I18n\Number::format($data15),
                    'd30' => \Cake\I18n\Number::format($data30)
                ];

                $data3 = $table->nothiinbox($office_id, $unit_id,
                    $organograms['office_unit_organogram_id'],
                    'DATEDIFF(NOW(),date(issue_date))>=3 and DATEDIFF(NOW(),date(issue_date))<5');
                $data5 = $table->nothiinbox($office_id, $unit_id,
                    $organograms['office_unit_organogram_id'],
                    'DATEDIFF(NOW(),date(issue_date))>=5 and DATEDIFF(NOW(),date(issue_date))<10');
                $data10 = $table->nothiinbox($office_id, $unit_id,
                    $organograms['office_unit_organogram_id'],
                    'DATEDIFF(NOW(),date(issue_date))>=10 and DATEDIFF(NOW(),date(issue_date))<15');
                $data15 = $table->nothiinbox($office_id, $unit_id,
                    $organograms['office_unit_organogram_id'],
                    'DATEDIFF(NOW(),date(issue_date))>=15 and DATEDIFF(NOW(),date(issue_date))<30');
                $data30 = $table->nothiinbox($office_id, $unit_id,
                    $organograms['office_unit_organogram_id'],
                    'DATEDIFF(NOW(),date(issue_date))>=30');

                $data[$organograms['office_unit_organogram_id']]['nothi'] = [
                    'd3' => \Cake\I18n\Number::format($data3),
                    'd5' => \Cake\I18n\Number::format($data5),
                    'd10' => \Cake\I18n\Number::format($data10),
                    'd15' => \Cake\I18n\Number::format($data15),
                    'd30' => \Cake\I18n\Number::format($data30)
                ];
            }
            $this->set('data', $data);
            $this->set('office_name_bng', $unit_name);
            $this->set('unit_id', $unit_id);
            $this->set('office_id', $office_id);
        } else {
            $this->redirect(['controller' => 'dashboard', 'action' => 'dashboard']);
        }
    }

    public function unitDashboardAjax($office_id = 0, $unit_id = 0)
    {
        $this->switchOffice($office_id, 'DashboardOfiice');
        $data = [];
        $auth = 0;
        if ($this->Auth->user()) $auth = 1;
        $this->set('auth', $auth);
        if ($auth == 0) $this->layout = 'dashboard';

        $ajax = 1;
        if ($this->request->is('ajax')) {
            $this->layout = 'ajax';
            $this->view = 'ajax_unit_dashboard';
            $ajax = 0;
        }
        $this->set(compact('ajax'));

        if (empty($office_id)) {
            $employeeinformation = $this->getCurrentDakSection();
            $office_id = $employeeinformation['office_id'];
        }

        $table = TableRegistry::get('Reports');
        $Dashboardtable = TableRegistry::get('Dashboard');
        if (!empty($office_id) && !empty($unit_id)) {
            $unitTable = TableRegistry::get('OfficeUnits');
            $unit_name = $unitTable->get($unit_id);

            $EmployeeOfficeTable = TableRegistry::get('EmployeeOffices');

            $organogramArray = $EmployeeOfficeTable->getEmployeeOfficeRecordsByOfficeId($office_id,
                $unit_id);

            foreach ($organogramArray as $organograms) {

                $result = $Dashboardtable->designationSummary($office_id, $unit_id, $organograms['office_unit_organogram_id']);

                $data[$organograms['office_unit_organogram_id']]['totaltodayinbox'] = $result['totaltodayinbox'];
                $data[$organograms['office_unit_organogram_id']]['totalyesterdayinbox'] = $result['totalyesterdayinbox'];
                $data[$organograms['office_unit_organogram_id']]['totalinboxall'] = $result['totalinboxall'];

                $data[$organograms['office_unit_organogram_id']]['totaltodayoutbox'] = $result['totaltodayoutbox'];
                $data[$organograms['office_unit_organogram_id']]['totalyesterdayoutbox'] = $result['totalyesterdayoutbox'];
                $data[$organograms['office_unit_organogram_id']]['totaloutboxall'] = $result['totaloutboxall'];

                $data[$organograms['office_unit_organogram_id']]['totaltodaynothivukto'] = $result['totaltodaynothivukto'];
                $data[$organograms['office_unit_organogram_id']]['totalyesterdaynothivukto'] = $result['totalyesterdaynothivukto'];
                $data[$organograms['office_unit_organogram_id']]['totalnothivuktoall'] = $result['totalnothivuktoall'];

                $data[$organograms['office_unit_organogram_id']]['totaltodaynothijato'] = $result['totaltodaynothijato'];
                $data[$organograms['office_unit_organogram_id']]['totalyesterdaynothijato'] = $result['totalyesterdaynothijato'];
                $data[$organograms['office_unit_organogram_id']]['totalnothijatoall'] = $result['totalnothijatoall'];

                $data[$organograms['office_unit_organogram_id']]['totaltodaysrijitonote'] = $result['totaltodaysrijitonote'];
                $data[$organograms['office_unit_organogram_id']]['totalyesterdaysrijitonote'] = $result['totalyesterdaysrijitonote'];
                $data[$organograms['office_unit_organogram_id']]['totalsrijitonoteall'] = !empty($result['totalsrijitonoteall']) ? $result['totalsrijitonoteall'] : 0;

                $data[$organograms['office_unit_organogram_id']]['totaltodaypotrojari'] = $result['totaltodaypotrojari'];
                $data[$organograms['office_unit_organogram_id']]['totalyesterdaypotrojari'] = $result['totalyesterdaypotrojari'];
                $data[$organograms['office_unit_organogram_id']]['totalpotrojariall'] = $result['totalpotrojariall'];

                $data[$organograms['office_unit_organogram_id']]['totaltodaydaksohonote'] = $result['totaltodaydaksohonote'];
                $data[$organograms['office_unit_organogram_id']]['totalyesterdaydaksohonote'] = $result['totalyesterdaydaksohonote'];
                $data[$organograms['office_unit_organogram_id']]['totaldaksohonoteall'] = !empty($result['totaldaksohonoteall']) ? $result['totaldaksohonoteall'] : 0;

                $data[$organograms['office_unit_organogram_id']]['totaltodaynisponnodak'] = $result['totaltodaynisponnodak'];
                $data[$organograms['office_unit_organogram_id']]['totalyesterdaynisponnodak'] = $result['totalyesterdaynisponnodak'];
                $data[$organograms['office_unit_organogram_id']]['totalnisponnodakall'] = !empty($result['totalnisponnodakall']) ? $result['totalnisponnodakall'] : 0;

                $data[$organograms['office_unit_organogram_id']]['totaltodaynisponnonote'] = $result['totaltodaynisponnonote'];
                $data[$organograms['office_unit_organogram_id']]['totalyesterdaynisponnonote'] = $result['totalyesterdaynisponnonote'];
                $data[$organograms['office_unit_organogram_id']]['totalnisponnonoteall'] = !empty($result['totalnisponnonoteall']) ? $result['totalnisponnonoteall'] : 0;

                $data[$organograms['office_unit_organogram_id']]['totaltodaynisponnopotrojari'] = $result['totaltodaynisponnopotrojari'];
                $data[$organograms['office_unit_organogram_id']]['totalyesterdaynisponnopotrojari'] = $result['totalyesterdaynisponnopotrojari'];
                $data[$organograms['office_unit_organogram_id']]['totalnisponnopotrojariall'] = !empty($result['totalnisponnopotrojariall']) ? $result['totalnisponnopotrojariall'] : 0;

                $data[$organograms['office_unit_organogram_id']]['totaltodayOnisponnodak'] = $result['totaltodayOnisponnodak'];
                $data[$organograms['office_unit_organogram_id']]['totalyesterdayOnisponnodak'] = $result['totalyesterdayOnisponnodak'];
                $data[$organograms['office_unit_organogram_id']]['totalOnisponnodakall'] = !empty($result['totalOnisponnodakall']) ? $result['totalOnisponnodakall'] : 0;
                $data[$organograms['office_unit_organogram_id']]['totaltodayOnisponnonote'] = $result['totaltodayOnisponnonote'];
                $data[$organograms['office_unit_organogram_id']]['totalyesterdayOnisponnonote'] = $result['totalyesterdayOnisponnonote'];
                $data[$organograms['office_unit_organogram_id']]['totalOnisponnonoteall'] = !empty($result['totalOnisponnonoteall']) ? $result['totalOnisponnonoteall'] : 0;

//                $data[$organograms['office_unit_organogram_id']]['totaltodayOnisponno']     = $result['totaltodayOnisponno'];
//                $data[$organograms['office_unit_organogram_id']]['totalyesterdayOnisponno'] = $result['totalyesterdayOnisponno'];
//                $data[$organograms['office_unit_organogram_id']]['totalOnisponnoall']       = $result['totalOnisponnoall'];

                $data[$organograms['office_unit_organogram_id']]['unit_id'] = $organograms['office_unit_id'];
                $data[$organograms['office_unit_organogram_id']]['office_id'] = $organograms['office_id'];

                $data[$organograms['office_unit_organogram_id']]['ajax_request'] = !empty($result['ajax_request']) ? $result['ajax_request'] : false;


                $data[$organograms['office_unit_organogram_id']]['name'] = $organograms['name_bng'] . ', ' . $organograms['designation'];

                $data3 = $table->dakinbox($office_id,
                    $unit_id, $organograms['office_unit_organogram_id'],
                    'DATEDIFF(NOW(),date(created))>=3 and DATEDIFF(NOW(),date(created))<5');
                $data5 = $table->dakinbox($office_id,
                    $unit_id, $organograms['office_unit_organogram_id'],
                    'DATEDIFF(NOW(),date(created))>=5 and DATEDIFF(NOW(),date(created))<10');
                $data10 = $table->dakinbox($office_id,
                    $unit_id, $organograms['office_unit_organogram_id'],
                    'DATEDIFF(NOW(),date(created))>=10 and DATEDIFF(NOW(),date(created))<15');
                $data15 = $table->dakinbox($office_id,
                    $unit_id, $organograms['office_unit_organogram_id'],
                    'DATEDIFF(NOW(),date(created))>=15 and DATEDIFF(NOW(),date(created))<30');
                $data30 = $table->dakinbox($office_id,
                    $unit_id, $organograms['office_unit_organogram_id'],
                    'DATEDIFF(NOW(),date(created))>=30');
                $data[$organograms['office_unit_organogram_id']]['dak'] = [
                    'd3' => \Cake\I18n\Number::format($data3),
                    'd5' => \Cake\I18n\Number::format($data5),
                    'd10' => \Cake\I18n\Number::format($data10),
                    'd15' => \Cake\I18n\Number::format($data15),
                    'd30' => \Cake\I18n\Number::format($data30)
                ];

                $data3 = $table->nothiinbox($office_id, $unit_id,
                    $organograms['office_unit_organogram_id'],
                    'DATEDIFF(NOW(),date(issue_date))>=3 and DATEDIFF(NOW(),date(issue_date))<5');
                $data5 = $table->nothiinbox($office_id, $unit_id,
                    $organograms['office_unit_organogram_id'],
                    'DATEDIFF(NOW(),date(issue_date))>=5 and DATEDIFF(NOW(),date(issue_date))<10');
                $data10 = $table->nothiinbox($office_id, $unit_id,
                    $organograms['office_unit_organogram_id'],
                    'DATEDIFF(NOW(),date(issue_date))>=10 and DATEDIFF(NOW(),date(issue_date))<15');
                $data15 = $table->nothiinbox($office_id, $unit_id,
                    $organograms['office_unit_organogram_id'],
                    'DATEDIFF(NOW(),date(issue_date))>=15 and DATEDIFF(NOW(),date(issue_date))<30');
                $data30 = $table->nothiinbox($office_id, $unit_id,
                    $organograms['office_unit_organogram_id'],
                    'DATEDIFF(NOW(),date(issue_date))>=30');

                $data[$organograms['office_unit_organogram_id']]['nothi'] = [
                    'd3' => \Cake\I18n\Number::format($data3),
                    'd5' => \Cake\I18n\Number::format($data5),
                    'd10' => \Cake\I18n\Number::format($data10),
                    'd15' => \Cake\I18n\Number::format($data15),
                    'd30' => \Cake\I18n\Number::format($data30)
                ];
            }
            $this->set('data', $data);
            $this->set('office_name_bng', $unit_name);
            $this->set('unit_id', $unit_id);
        } else {
            $this->redirect(['controller' => 'dashboard', 'action' => 'dashboard']);
        }
    }

    public function officeDashboard($office_id)
    {
        set_time_limit(0);
        if (!$this->Auth->user()) {
            $this->layout = 'dashboard';
        }

        $this->switchOffice($office_id, 'DashboardOffice');
        $this->set('office_id', $office_id);
    }

    private function MonitorDashboardSummary($officeid)
    {
        set_time_limit(0);
        try {
            $this->switchOffice($officeid, 'DashboardOffice');

            TableRegistry::remove('DakMovements');
            TableRegistry::remove('Potrojari');
            TableRegistry::remove('NothiParts');
            TableRegistry::remove('employee_offices');
            TableRegistry::remove('UserLoginHistory');
            TableRegistry::remove('NothiMasterCurrentUsers');
            TableRegistry::remove('NothiDakPotroMaps');

            $DakMovementsTable = TableRegistry::get('DakMovements');
            $potrojariTable = TableRegistry::get('Potrojari');
            $nothiPartTable = TableRegistry::get('NothiParts');
            $NothiMasterCurrentUsersTable = TableRegistry::get('NothiMasterCurrentUsers');
            $NothiDakPotroMapsTable = TableRegistry::get('NothiDakPotroMaps');
            $today = date('Y-m-d');
            $yesterday = date('Y-m-d', strtotime($today . ' -1 day'));


            /*             * Inbox Total* */

            $totalyesterdayinbox = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0,
                'Inbox', [$yesterday, $yesterday]);

            $totalprevinbox = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0, 'Inbox');
            /*             * Inbox Total* */

            /*             * Outbox Total* */

            $totalyesterdayoutbox = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0,
                'Outbox', [$yesterday, $yesterday]);

            $totalprevoutbox = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0, 'Outbox');
            /*             * Outbox Total* */

            /*             * Nothivukto * */

            $totalyesterdaynothivukto = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0,
                'NothiVukto', [$yesterday, $yesterday]);

            $totalprevnothivukto = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0,
                'NothiVukto');
            /*             * Nothivukto * */

            /*             * Nothijato * */

            $totalyesterdaynothijato = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0,
                'NothiJato', [$yesterday, $yesterday]);
            $totalprevnothijato = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0,
                'NothiJato');
            /*             * Nothijato * */

            /*             * Potrojari* */

            $totalyesterdaypotrojari = $potrojariTable->allPotrojariCount($officeid, 0, 0,
                [$yesterday, $yesterday]);
            /*             * Potrojari* */


            /*             * Self Given Note* */

            $returnSummary['totalyesterdaysrijitonote'] = $nothiPartTable->selfSrijitoNoteCount($officeid,
                0, 0, [$yesterday, $yesterday]);

            /*             * Self Given Note* */

            /*             * Result* */


            $returnSummary['totalyesterdayinbox'] = $totalyesterdayinbox;
            $returnSummary['totalinboxall'] = $totalprevinbox;


            $returnSummary['totalyesterdayoutbox'] = $totalyesterdayoutbox;
            $returnSummary['totaloutboxall'] = $totalprevoutbox;


            $returnSummary['totalyesterdaynothivukto'] = $totalyesterdaynothivukto;
            $returnSummary['totalnothivuktoall'] = $totalprevnothivukto;


            $returnSummary['totalyesterdaynothijato'] = $totalyesterdaynothijato;
            $returnSummary['totalnothijatoall'] = $totalprevnothijato;


            $returnSummary['totalyesterdaypotrojari'] = $totalyesterdaypotrojari;


            $returnSummary['totalyesterdaydaksohonote'] = $nothiPartTable->dakSrijitoNoteCount($officeid,
                0, 0, [$yesterday, $yesterday]);


            $returnSummary['totalnisponnodakall'] = $returnSummary['totalnothijatoall'] + $returnSummary['totalnothivuktoall'] /* + $DakMovementsTable->countOnulipiDak($officeid,
              0, 0) */
            ;

            $returnSummary['totalnisponnonoteall'] = $NothiMasterCurrentUsersTable->nisponnoNoteCount($officeid,
                0, 0);

            $returnSummary['totalOnisponnodakall'] = TableRegistry::get('DakUsers')->getOnisponnoDak($officeid,
                0, 0);
            if ($returnSummary['totalOnisponnodakall'] < 0) {
                $returnSummary['totalOnisponnodakall'] = 0;
            }

            $returnSummary['totalOnisponnonoteall'] = $NothiMasterCurrentUsersTable->getOnisponnoNoteCount($officeid,
                0, 0);

            $returnSummary['totalOnisponnoall'] = $returnSummary['totalOnisponnodakall'] + $returnSummary['totalOnisponnonoteall'];

            $returnSummary['totalnisponnoall'] = $returnSummary['totalnisponnodakall'] + $returnSummary['totalnisponnonoteall'];

            $returnSummary['status'] = 1;

            /* Save in Monitor Offices Table */
            $MonitorOfficesTable = TableRegistry::get('MonitorOffices');
            $monitorOfficesEntity = $MonitorOfficesTable->newEntity();
            $monitorOfficesEntity->office_id = $officeid;
            $monitorOfficesEntity->yesterdayinbox = $returnSummary['totalyesterdayinbox'];
            $monitorOfficesEntity->yesterdayoutbox = $returnSummary['totalyesterdayoutbox'];
            $monitorOfficesEntity->yesterdaynothijat = $returnSummary['totalyesterdaynothijato'];
            $monitorOfficesEntity->yesterdaynothivukto = $returnSummary['totalyesterdaynothivukto'];
            $monitorOfficesEntity->totalnisponnodak = $returnSummary['totalnisponnodakall'];
            $monitorOfficesEntity->totalonisponnodak = $returnSummary['totalOnisponnodakall'];
            $monitorOfficesEntity->yesterdayselfnote = $returnSummary['totalyesterdaysrijitonote'];
            $monitorOfficesEntity->yesterdaydaksohonote = $returnSummary['totalyesterdaydaksohonote'];
            $monitorOfficesEntity->yesterdaypotrojari = $returnSummary['totalyesterdaypotrojari'];
            $monitorOfficesEntity->totalnisponnonote = $returnSummary['totalnisponnonoteall'];
            $monitorOfficesEntity->totalonisponnonote = $returnSummary['totalOnisponnonoteall'];
            $monitorOfficesEntity->totalnisponno = $returnSummary['totalnisponnoall'];
            $monitorOfficesEntity->totalonisponno = $returnSummary['totalOnisponnoall'];
            $monitorOfficesEntity->status = $returnSummary['status'];
            $monitorOfficesEntity->updated = date('Y-m-d');

            if ($MonitorOfficesTable->save($monitorOfficesEntity)) {

            }

            /* Save in Monitor Offices Table */

            return $returnSummary;
        } catch (\Exception $ex) {
            return;
        }
    }

    protected function reportMonitorDashboardSummary($officeid)
    {
        set_time_limit(0);
        try {
            $this->switchOffice($officeid, 'DashboardOffice');

            TableRegistry::remove('DakMovements');
            TableRegistry::remove('Potrojari');
            TableRegistry::remove('NothiParts');
            TableRegistry::remove('employee_offices');
            TableRegistry::remove('UserLoginHistory');
            TableRegistry::remove('NothiMasterCurrentUsers');
            TableRegistry::remove('NothiDakPotroMaps');
            TableRegistry::remove('Dashboard');
            TableRegistry::remove('DakUsers');

            $dashboardTable = TableRegistry::get('Dashboard');
            $DakUsersTable = TableRegistry::get('DakUsers');
            $DakMovementsTable = TableRegistry::get('DakMovements');
            $potrojariTable = TableRegistry::get('Potrojari');
            $today = date('Y-m-d');
            $yesterday = date('Y-m-d', strtotime($today . ' -1 day'));


            /*             * Inbox Total* */

            $totalyesterdayinbox = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0,
                'Inbox', [$yesterday, $yesterday]);

            $totalprevinbox = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0, 'Inbox');
            /*             * Inbox Total* */

            /*             * Outbox Total* */

            $totalyesterdayoutbox = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0,
                'Outbox', [$yesterday, $yesterday]);

            $totalprevoutbox = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0, 'Outbox');
            /*             * Outbox Total* */

            /*             * Nothivukto * */

            $totalyesterdaynothivukto = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0,
                'NothiVukto', [$yesterday, $yesterday]);

            $totalprevnothivukto = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0,
                'NothiVukto');
            /*             * Nothivukto * */

            /*             * Nothijato * */

            $totalyesterdaynothijato = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0,
                'NothiJato', [$yesterday, $yesterday]);
            $totalprevnothijato = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0,
                'NothiJato');
            /*             * Nothijato * */

            /*             * Nothi Part Here * */
            $nothi_data_yesterday = TableRegistry::get('Dashboard')->nothiDashboardCount($officeid, 0, 0, [$yesterday, $yesterday]);

            /*             * Potrojari* */

            $totalyesterdaypotrojari = $potrojariTable->allPotrojariCount($officeid, 0, 0, [$yesterday, $yesterday]);
            /*             * Potrojari* */


            /*             * Self Given Note* */

            $returnSummary['totalyesterdaysrijitonote'] = $nothi_data_yesterday['selfNote'];

            /*             * Self Given Note* */

            /*             * Result* */


            $returnSummary['totalyesterdayinbox'] = $totalyesterdayinbox;
            $returnSummary['totalinboxall'] = $totalprevinbox;


            $returnSummary['totalyesterdayoutbox'] = $totalyesterdayoutbox;
            $returnSummary['totaloutboxall'] = $totalprevoutbox;


            $returnSummary['totalyesterdaynothivukto'] = $totalyesterdaynothivukto;
            $returnSummary['totalnothivuktoall'] = $totalprevnothivukto;


            $returnSummary['totalyesterdaynothijato'] = $totalyesterdaynothijato;
            $returnSummary['totalnothijatoall'] = $totalprevnothijato;


            $returnSummary['totalyesterdaypotrojari'] = $totalyesterdaypotrojari;


            $returnSummary['totalyesterdaydaksohonote'] = $nothi_data_yesterday['dakNote'];


            $returnSummary['totalnisponnodakall'] = $returnSummary['totalnothijatoall'] + $returnSummary['totalnothivuktoall'] /* + $DakMovementsTable->countOnulipiDak($officeid,
              0, 0) */
            ;

            $prevNothiData = $dashboardTable->nothiDashboardCount($officeid, 0, 0);
            $returnSummary['totalnisponnonoteall'] = $prevNothiData['potrojariNisponno'] + $prevNothiData['noteNisponno'];


            $returnSummary['totalOnisponnodakall'] = $DakUsersTable->getOnisponnoDak($officeid, 0, 0);


            $returnSummary['totalOnisponnonoteall'] = $prevNothiData['onisponno'];

            if ($returnSummary['totalOnisponnodakall'] < 0) {
                $returnSummary['totalOnisponnodakall'] = 0;
            }
            if ($returnSummary['totalnisponnonoteall'] < 0) {
                $returnSummary['totalnisponnonoteall'] = 0;
            }

            $returnSummary['totalOnisponnoall'] = $returnSummary['totalOnisponnodakall'] + $returnSummary['totalOnisponnonoteall'];

            $returnSummary['totalnisponnoall'] = $returnSummary['totalnisponnodakall'] + $returnSummary['totalnisponnonoteall'];

            $returnSummary['Status'] = 1;
        } catch (\Exception $ex) {
            $returnSummary['totalyesterdayinbox'] = 0;
            $returnSummary['totalinboxall'] = 0;


            $returnSummary['totalyesterdayoutbox'] = 0;
            $returnSummary['totaloutboxall'] = 0;


            $returnSummary['totalyesterdaynothivukto'] = 0;
            $returnSummary['totalnothivuktoall'] = 0;


            $returnSummary['totalyesterdaynothijato'] = 0;
            $returnSummary['totalnothijatoall'] = 0;

            $returnSummary['totalnisponnodakall'] = 0;


            $returnSummary['totalOnisponnodakall'] = 0;

            $returnSummary['totalyesterdaypotrojari'] = 0;

            $returnSummary['totalyesterdaydaksohonote'] = 0;

            $returnSummary['totalyesterdaysrijitonote'] = 0;

            $returnSummary['totalnisponnonoteall'] = 0;

            $returnSummary['totalOnisponnonoteall'] = 0;

            $returnSummary['totalOnisponnoall'] = 0;

            $returnSummary['totalnisponnoall'] = 0;

            $returnSummary['Status'] = 0;
        }
        return $returnSummary;
    }

    protected function updateMonitorDashboardSummary($officeid)
    {
        set_time_limit(0);
        try {
            $this->switchOffice($officeid, 'DashboardOffice');

            TableRegistry::remove('DakMovements');
            TableRegistry::remove('Potrojari');
            TableRegistry::remove('NothiParts');
            TableRegistry::remove('employee_offices');
            TableRegistry::remove('UserLoginHistory');
            TableRegistry::remove('NothiMasterCurrentUsers');
            TableRegistry::remove('NothiDakPotroMaps');

            $dashboardTable = TableRegistry::get('Dashboard');
            $DakUsersTable = TableRegistry::get('DakUsers');
            $DakMovementsTable = TableRegistry::get('DakMovements');
            $potrojariTable = TableRegistry::get('Potrojari');
            $nothiPartTable = TableRegistry::get('NothiParts');
            $NothiMasterCurrentUsersTable = TableRegistry::get('NothiMasterCurrentUsers');
            $NothiDakPotroMapsTable = TableRegistry::get('NothiDakPotroMaps');
            $today = date('Y-m-d');
            $yesterday = date('Y-m-d', strtotime($today . ' -1 day'));


            /*             * Inbox Total* */

            $totaltodayinbox = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0,
                'Inbox', [$today, $today]);
            $totalyesterdayinbox = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0,
                'Inbox', [$yesterday, $yesterday]);

            /*             * Inbox Total* */

            /*             * Outbox Total* */

            $totaltodayoutbox = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0,
                'Outbox', [$today, $today]);
            $totalyesterdayoutbox = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0,
                'Outbox', [$yesterday, $yesterday]);

            /*             * Outbox Total* */

            /*             * Nothivukto * */

            $totaltodaynothivukto = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0,
                'NothiVukto', [$today, $today]);
            $totalyesterdaynothivukto = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0,
                'NothiVukto', [$yesterday, $yesterday]);

            /*             * Nothivukto * */

            /*             * Nothijato * */

            $totaltodaynothijato = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0,
                'NothiJato', [$today, $today]);
            $totalyesterdaynothijato = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0,
                'NothiJato', [$yesterday, $yesterday]);
            /*             * Nothijato * */

            /*             * Potrojari* */

            $totalyesterdaypotrojari = $potrojariTable->allPotrojariCount($officeid, 0, 0,
                [$yesterday, $yesterday]);
            /*             * Potrojari* */

            /*             * Nothi Part Here * */
            $nothi_data_yesterday = TableRegistry::get('Dashboard')->nothiDashboardCount($officeid, 0, 0, [$yesterday, $yesterday]);

            /*             * Self Given Note* */

            $returnSummary['totalyesterdaysrijitonote'] = $nothi_data_yesterday['selfNote'];

            /*             * Self Given Note* */

            /*             * Result* */


            $returnSummary['totaltodayinbox'] = $totaltodayinbox;
            $returnSummary['totalyesterdayinbox'] = $totalyesterdayinbox;


            $returnSummary['totaltodayoutbox'] = $totaltodayoutbox;
            $returnSummary['totalyesterdayoutbox'] = $totalyesterdayoutbox;


            $returnSummary['totaltodaynothivukto'] = $totaltodaynothivukto;
            $returnSummary['totalyesterdaynothivukto'] = $totalyesterdaynothivukto;


            $returnSummary['totaltodaynothijato'] = $totaltodaynothijato;
            $returnSummary['totalyesterdaynothijato'] = $totalyesterdaynothijato;


            $returnSummary['totalyesterdaypotrojari'] = $totalyesterdaypotrojari;


            $returnSummary['totalyesterdaydaksohonote'] = $nothi_data_yesterday['dakNote'];

            $result = TableRegistry::get('DashboardOffices')->getData($officeid);

            /**Full Nisponno Dak**/
            $returnSummary['totaltodaynisponnodak'] = $returnSummary['totaltodaynothijato'] + $returnSummary['totaltodaynothivukto'] /* + $DakMovementsTable->countOnulipiDak($officeid,
              0, 0, [$today, $today]) */ + $result['totalNisponnoDak'];


            $temp_today = TableRegistry::get('Dashboard')->nothiDashboardCount($officeid, 0, 0, [$today, $today]);

            /**Full Nisponno Note**/
            $returnSummary['totaltodaynisponnonote'] = $temp_today['noteNisponno'] + $result['totalNisponnoNote'];


            /**Full Onisponno Dak**/
            $returnSummary['totaltodayOnisponnodak'] = TableRegistry::get('DakUsers')->getOnisponnoDak($officeid,
                0, 0);

            /**Full Onisponno Note**/
            $returnSummary['totaltodayOnisponnonote'] = $result['totalONisponno'] + $temp_today['selfNote']
                + $temp_today['dakNote'] - $temp_today['noteNisponno'] - $temp_today['potrojariNisponno'];

            if ($returnSummary['totaltodayOnisponnodak'] < 0) {
                $returnSummary['totaltodayOnisponnodak'] = 0;
            }
            if ($returnSummary['totaltodaynisponnonote'] < 0) {
                $returnSummary['totaltodaynisponnonote'] = 0;
            }

            $returnSummary['totaltodayOnisponno'] = $returnSummary['totaltodayOnisponnodak'] + $returnSummary['totaltodayOnisponnonote'];

            $returnSummary['totaltodaynisponno'] = $returnSummary['totaltodaynisponnodak'] + $returnSummary['totaltodaynisponnonote'];

            $returnSummary['Status'] = 1;
        } catch (\Exception $ex) {

            /*             * Result* */


            $returnSummary['totaltodayinbox'] = 0;
            $returnSummary['totalyesterdayinbox'] = 0;


            $returnSummary['totaltodayoutbox'] = 0;
            $returnSummary['totalyesterdayoutbox'] = 0;


            $returnSummary['totaltodaynothivukto'] = 0;
            $returnSummary['totalyesterdaynothivukto'] = 0;


            $returnSummary['totaltodaynothijato'] = 0;
            $returnSummary['totalyesterdaynothijato'] = 0;

            $returnSummary['totalyesterdaysrijitonote'] = 0;

            $returnSummary['totalyesterdaypotrojari'] = 0;


            $returnSummary['totalyesterdaydaksohonote'] = 0;


            $returnSummary['totaltodaynisponnodak'] = 0;


            $returnSummary['totaltodaynisponnonote'] = 0;


            $returnSummary['totaltodayOnisponnodak'] = 0;

            $returnSummary['totaltodayOnisponnonote'] = 0;


            $returnSummary['totaltodayOnisponno'] = 0;
            $returnSummary['totaltodaynisponno'] = 0;

            $returnSummary['Status'] = 0;
        }
        return $returnSummary;
    }

    public function ApiDashboardSummary()
    {
        $user_designation = $this->request->query['user_designation'];

        $jsonArray = array(
            "status" => "error",
            "error_code" => 404,
            "message" => "Invalid Request"
        );

        if (!defined("PROJAPOTI_API") || PROJAPOTI_API == false) {
            echo json_encode($jsonArray);
            die;
        }

        if (empty($user_designation)) {
            echo json_encode($jsonArray);
            die;
        }

        $apikey = isset($this->request->query['api_key']) ? $this->request->query['api_key'] : '';

        if (empty($apikey) || $this->checkToken($apikey) == FALSE) {
            echo json_encode($jsonArray);
            die;
        }

        $EmployeeOfficesTable = TableRegistry::get('EmployeeOffices');
        $employee_office = $EmployeeOfficesTable->getDesignationInfo($user_designation);
        $this->switchOffice($employee_office['office_id'], 'Receiveroffice');

        $result = array();

        $table_instance_dak_user = TableRegistry::get('DakUsers');
        $dak_count_inbox = $table_instance_dak_user->getDakCountBy_employee_designation(DAK_CATEGORY_INBOX,
            $user_designation);
        $dak_count_inboxng = $table_instance_dak_user->getDakCountBy_employee_designation(DAK_CATEGORY_INBOX,
            $user_designation, DAK_NAGORIK);

        $dak_count_new = $table_instance_dak_user->getDakCountBy_status_employee_designation(DAK_VIEW_STATUS_NEW,
            $user_designation);
        $dak_count_newng = $table_instance_dak_user->getDakCountBy_status_employee_designation(DAK_VIEW_STATUS_NEW,
            $user_designation, DAK_NAGORIK);

        TableRegistry::remove('NothiMasterCurrentUsers');
        $nothiCurrentUser = TableRegistry::get('NothiMasterCurrentUsers');
        $total_nothi = $nothiCurrentUser->find()->where(['office_unit_organogram_id' => $user_designation,
            'is_archive' => 0])->count();

        $total_nothi_inbox = $nothiCurrentUser->find()->where([
            'view_status' => 0,
            'office_unit_organogram_id' => $user_designation,
            'office_id' => $employee_office['office_id'],
            'nothi_office' => $employee_office['office_id'],
            'is_new' => 0,
            'is_archive' => 0
        ])->count();

        $result = array(
            'totalDak' => intval($dak_count_inbox) + intval($dak_count_inboxng),
            'totalDakNew' => intval($dak_count_new) + intval($dak_count_newng),
            'totalNothi' => intval($total_nothi),
            'totalNothiNew' => intval($total_nothi_inbox),
        );

        $jsonArray = array(
            "status" => "success",
            "data" => $result
        );

        $this->response->body(json_encode($jsonArray));
        $this->response->type('application/json');
        return $this->response;
    }

    public function apiUserSummary()
    {
        $user_id = $this->request->query['user_id'];

        $jsonArray = array(
            "status" => "error",
            "error_code" => 404,
            "message" => "Invalid Request"
        );

        if (!defined("PROJAPOTI_API") || PROJAPOTI_API == false) {
            echo json_encode($jsonArray);
            die;
        }

        if (empty($user_id)) {
            echo json_encode($jsonArray);
            die;
        }

        $apikey = isset($this->request->query['api_key']) ? $this->request->query['api_key'] : '';

        if (empty($apikey) || $this->checkToken($apikey) == FALSE) {
            echo json_encode($jsonArray);
            die;
        }

        $result = array();
        $usersTable = TableRegistry::get('Users');

        $user_info = $usersTable->get($user_id);
        $EmployeeOfficesTable = TableRegistry::get('EmployeeOffices');

        $user_designations = $EmployeeOfficesTable->getEmployeeOfficeRecords($user_info->employee_record_id);

        if (!empty($user_designations)) {
            foreach ($user_designations as $key => $user_designation) {

                $employee_office = $EmployeeOfficesTable->getDesignationInfo($user_designation['office_unit_organogram_id']);
                $this->switchOffice($employee_office['office_id'], 'Receiveroffice');


                TableRegistry::remove('DakUsers');
                $table_instance_dak_user = TableRegistry::get('DakUsers');
                $dak_count_inbox = $table_instance_dak_user->getDakCountBy_employee_designation(DAK_CATEGORY_INBOX,
                    $user_designation['office_unit_organogram_id']);
                $dak_count_inboxng = $table_instance_dak_user->getDakCountBy_employee_designation(DAK_CATEGORY_INBOX,
                    $user_designation['office_unit_organogram_id'], DAK_NAGORIK);

                $dak_count_new = $dak_count_inbox;
                $dak_count_newng = $dak_count_inboxng;

                TableRegistry::remove('NothiMasterCurrentUsers');
                $nothiCurrentUser = TableRegistry::get('NothiMasterCurrentUsers');

                $onisponno_nothi_count = $nothiCurrentUser->getOnisponnoNothiCount($user_designation['office_unit_organogram_id'],$employee_office['office_id']);
                $onisponno_other_nothi_count = $nothiCurrentUser->getOnisponnoOtherNothiCount($user_designation['office_unit_organogram_id'],$employee_office['office_id']);

                $total_nothi = $onisponno_nothi_count + $onisponno_other_nothi_count;
                $total_nothi_inbox = $total_nothi;


                if (!isset($result['total']['totalDak'])) {
                    $result['total']['totalDak'] = intval($dak_count_inbox) + intval($dak_count_inboxng);
                } else {
                    $result['total']['totalDak'] += intval($dak_count_inbox) + intval($dak_count_inboxng);
                }

                if (!isset($result['total']['totalDakNew'])) {
                    $result['total']['totalDakNew'] = intval($dak_count_new) + intval($dak_count_newng);
                } else {
                    $result['total']['totalDakNew'] += intval($dak_count_new) + intval($dak_count_newng);
                }

                if (!isset($result['total']['totalNothi'])) {
                    $result['total']['totalNothi'] = intval($total_nothi);
                } else {
                    $result['total']['totalNothi'] += intval($total_nothi);
                }

                if (!isset($result['total']['totalNothiNew'])) {
                    $result['total']['totalNothiNew'] = intval($total_nothi_inbox);
                } else {
                    $result['total']['totalNothiNew'] += intval($total_nothi_inbox);
                }

                if (!isset($result['total']['totalOnisponnoNothi'])) {
                    $result['total']['totalOnisponnoNothi'] = intval($onisponno_nothi_count);
                } else {
                    $result['total']['totalOnisponnoNothi'] += intval($onisponno_nothi_count);
                }

                if (!isset($result['total']['totalOnisponnoOtherNothi'])) {
                    $result['total']['totalOnisponnoOtherNothi'] = intval($onisponno_other_nothi_count);
                } else {
                    $result['total']['totalOnisponnoOtherNothi'] += intval($onisponno_other_nothi_count);
                }

                $result['designation'][$user_designation['office_unit_organogram_id']] = array(
                    'totalDak' => intval($dak_count_inbox) + intval($dak_count_inboxng),
                    'totalDakNew' => intval($dak_count_new) + intval($dak_count_newng),
                    'totalNothi' => intval($total_nothi),
                    'totalNothiNew' => intval($total_nothi_inbox),
                    'totalOnisponnoNothi' => intval($onisponno_nothi_count),
                    'totalOnisponnoOtherNothi' => intval($onisponno_other_nothi_count),
                );
            }
        }

        $jsonArray = array(
            "status" => "success",
            "data" => $result
        );

        $this->response->body(json_encode($jsonArray));
        $this->response->type('application/json');
        return $this->response;
    }

    public function postApiUserSummary()
    {
        $jsonArray = array(
            "status" => "error",
            "error_code" => 404,
            "message" => "Invalid Request"
        );
        if ($this->request->is('post')) {
            $user_id = $this->request->data['user_id'];
            $apikey = isset($this->request->data['api_key']) ? $this->request->data['api_key'] : '';

            $jsonArray = array(
                "status" => "error",
                "error_code" => 404,
                "message" => "Invalid Request"
            );

            if (!defined("PROJAPOTI_API") || PROJAPOTI_API == false) {
                echo json_encode($jsonArray);
                die;
            }

            if (empty($user_id)) {
                echo json_encode($jsonArray);
                die;
            }

            if (empty($apikey) || $this->checkToken($apikey) == FALSE) {
                echo json_encode($jsonArray);
                die;
            }

            $result = array();
            $usersTable = TableRegistry::get('Users');

            $user_info = $usersTable->get($user_id);
            $EmployeeOfficesTable = TableRegistry::get('EmployeeOffices');
            if($apikey != API_KEY){
                //verify api token
                $this->loadComponent('ApiRelated');
                $getApiTokenData = $this->ApiRelated->getMobileApiTokenData($apikey);
                //verify api token
                if(!empty($getApiTokenData)){
                    if($user_info->username != $getApiTokenData['user_name'] || $user_info->employee_record_id != $getApiTokenData['employee_record_id']){
                        $jsonArray['message'] = 'Unauthorized request';
                        echo json_encode($jsonArray);die;
                    }
                }
            }

            $user_designations = $EmployeeOfficesTable->getEmployeeOfficeRecords($user_info->employee_record_id);

            if (!empty($user_designations)) {
                foreach ($user_designations as $key => $user_designation) {

                    $employee_office = $EmployeeOfficesTable->getDesignationInfo($user_designation['office_unit_organogram_id']);
                    $this->switchOffice($employee_office['office_id'], 'Receiveroffice');


                    TableRegistry::remove('DakUsers');
                    $table_instance_dak_user = TableRegistry::get('DakUsers');
                    $dak_count_inbox = $table_instance_dak_user->getDakCountBy_employee_designation(DAK_CATEGORY_INBOX,
                        $user_designation['office_unit_organogram_id']);
                    $dak_count_inboxng = $table_instance_dak_user->getDakCountBy_employee_designation(DAK_CATEGORY_INBOX,
                        $user_designation['office_unit_organogram_id'], DAK_NAGORIK);

                    $dak_count_new = $dak_count_inbox;
                    $dak_count_newng = $dak_count_inboxng;

                    TableRegistry::remove('NothiMasterCurrentUsers');
                    $nothiCurrentUser = TableRegistry::get('NothiMasterCurrentUsers');

                    $onisponno_nothi_count = $nothiCurrentUser->getOnisponnoNothiCount($user_designation['office_unit_organogram_id'],$employee_office['office_id']);
                    $onisponno_other_nothi_count = $nothiCurrentUser->getOnisponnoOtherNothiCount($user_designation['office_unit_organogram_id'],$employee_office['office_id']);

                    $total_nothi = $onisponno_nothi_count + $onisponno_other_nothi_count;


                    if (!isset($result['total']['totalDak'])) {
                        $result['total']['totalDak'] = intval($dak_count_inbox) + intval($dak_count_inboxng);
                    } else {
                        $result['total']['totalDak'] += intval($dak_count_inbox) + intval($dak_count_inboxng);
                    }

                    if (!isset($result['total']['totalDakNew'])) {
                        $result['total']['totalDakNew'] = intval($dak_count_new) + intval($dak_count_newng);
                    } else {
                        $result['total']['totalDakNew'] += intval($dak_count_new) + intval($dak_count_newng);
                    }

                    if (!isset($result['total']['totalNothi'])) {
                        $result['total']['totalNothi'] = intval($total_nothi);
                    } else {
                        $result['total']['totalNothi'] += intval($total_nothi);
                    }

                    if (!isset($result['total']['totalNothiNew'])) {
                        $result['total']['totalNothiNew'] = intval($total_nothi);
                    } else {
                        $result['total']['totalNothiNew'] += intval($total_nothi);
                    }
                    if (!isset($result['total']['totalOnisponnoNothi'])) {
                        $result['total']['totalOnisponnoNothi'] = intval($onisponno_nothi_count);
                    } else {
                        $result['total']['totalOnisponnoNothi'] += intval($onisponno_nothi_count);
                    }

                    if (!isset($result['total']['totalOnisponnoOtherNothi'])) {
                        $result['total']['totalOnisponnoOtherNothi'] = intval($onisponno_other_nothi_count);
                    } else {
                        $result['total']['totalOnisponnoOtherNothi'] += intval($onisponno_other_nothi_count);
                    }
                    $result['designation'][$user_designation['office_unit_organogram_id']] = array(
                        'totalDak' => intval($dak_count_inbox) + intval($dak_count_inboxng),
                        'totalDakNew' => intval($dak_count_new) + intval($dak_count_newng),
                        'totalNothi' => intval($total_nothi),
                        'totalNothiNew' => intval($total_nothi),
                        'totalOnisponnoNothi' => intval($onisponno_nothi_count),
                        'totalOnisponnoOtherNothi' => intval($onisponno_other_nothi_count),
                    );
                }
            }

            $jsonArray = array(
                "status" => "success",
                "data" => $result
            );
            $this->response->body(json_encode($jsonArray));
            $this->response->type('application/json');
            return $this->response;
        } else {
            $this->response->body(json_encode($jsonArray));
            $this->response->type('application/json');
            return $this->response;
        }
    }

    public function postApiUserNotification()
    {
        $user_id = $this->request->data['user_id'];

        $jsonArray = array(
            "status" => "error",
            "error_code" => 404,
            "message" => "Invalid Request"
        );

        if (!defined("PROJAPOTI_API") || PROJAPOTI_API == false) {
            echo json_encode($jsonArray);
            die;
        }

        if (empty($user_id)) {
            echo json_encode($jsonArray);
            die;
        }

        $apikey = isset($this->request->data['api_key']) ? $this->request->data['api_key'] : '';

        if (empty($apikey) || $this->checkToken($apikey) == FALSE) {
            echo json_encode($jsonArray);
            die;
        }

        $result = array();
        $usersTable = TableRegistry::get('Users');

        $user_info = $usersTable->get($user_id);
        if($apikey != API_KEY){
            //verify api token
            $this->loadComponent('ApiRelated');
            $getApiTokenData = $this->ApiRelated->getMobileApiTokenData($apikey);
            //verify api token
            if(!empty($getApiTokenData)){
                if($user_info->username != $getApiTokenData['user_name'] || $user_info->employee_record_id != $getApiTokenData['employee_record_id']){
                    $jsonArray['message'] = 'Unauthorized request';
                    echo json_encode($jsonArray);die;
                }
            }
        }
        $EmployeeOfficesTable = TableRegistry::get('EmployeeOffices');

        $user_designations = $EmployeeOfficesTable->getEmployeeOfficeRecords($user_info->employee_record_id);

        if (!empty($user_designations)) {
            foreach ($user_designations as $key => $user_designation) {

                $employee_office = $EmployeeOfficesTable->getDesignationInfo($user_designation['office_unit_organogram_id']);

                $this->switchOffice($employee_office['office_id'], 'Receiveroffice');
                $lastDate = !empty($this->request->data['last_request_time']) ? date("Y-m-d H:i:s", strtotime($this->request->data['last_request_time'])) : '';

                TableRegistry::remove('DakUsers');
                $table_instance_dak_user = TableRegistry::get('DakUsers');
                $dak_count_inbox = $table_instance_dak_user->getDakCountBy_employee_designation(DAK_CATEGORY_INBOX,
                    $user_designation['office_unit_organogram_id']);


                $dak_count_inboxng = $table_instance_dak_user->getDakCountBy_employee_designation(DAK_CATEGORY_INBOX,
                    $user_designation['office_unit_organogram_id'], DAK_NAGORIK);

                $dak_count_new = $table_instance_dak_user->getDakCountBy_employee_designationWithDate(DAK_VIEW_STATUS_NEW,
                    $user_designation['office_unit_organogram_id']);
                if (!empty($dak_count_new)) {
                    $dak_count_new = $dak_count_new->where(['modified >' => $lastDate])->count();
                }

                $dak_count_newng = $table_instance_dak_user->getDakCountBy_employee_designationWithDate(DAK_VIEW_STATUS_NEW,
                    $user_designation['office_unit_organogram_id'], DAK_NAGORIK);

                if (!empty($dak_count_newng)) {
                    $dak_count_newng = $dak_count_newng->where(['modified >' => $lastDate])->count();
                }

                TableRegistry::remove('NothiMasterCurrentUsers');
                $nothiCurrentUser = TableRegistry::get('NothiMasterCurrentUsers');
                $total_nothi = $nothiCurrentUser->find()->where(['office_unit_organogram_id' => $user_designation['office_unit_organogram_id'],
                    'is_archive' => 0])->count();

                $total_nothi_inbox = $nothiCurrentUser->find()->where([
                    'view_status' => 0,
                    'office_unit_organogram_id' => $user_designation['office_unit_organogram_id'],
                    'office_id' => $employee_office['office_id'],
                    'nothi_office' => $employee_office['office_id'],
                    'is_new' => 0,
                    'is_archive' => 0
                ]);

                if (!empty($total_nothi_inbox)) {
                    $total_nothi_inbox = $total_nothi_inbox->where(['issue_date >' => $lastDate])->count();
                }


                if (!isset($result['total']['totalDak'])) {
                    $result['total']['totalDak'] = intval($dak_count_inbox) + intval($dak_count_inboxng);
                } else {
                    $result['total']['totalDak'] += intval($dak_count_inbox) + intval($dak_count_inboxng);
                }

                if (!isset($result['total']['totalDakNew'])) {
                    $result['total']['totalDakNew'] = intval($dak_count_new) + intval($dak_count_newng);
                } else {
                    $result['total']['totalDakNew'] += intval($dak_count_new) + intval($dak_count_newng);
                }

                if (!isset($result['total']['totalNothi'])) {
                    $result['total']['totalNothi'] = intval($total_nothi);
                } else {
                    $result['total']['totalNothi'] += intval($total_nothi);
                }

                if (!isset($result['total']['totalNothiNew'])) {
                    $result['total']['totalNothiNew'] = intval($total_nothi_inbox);
                } else {
                    $result['total']['totalNothiNew'] += intval($total_nothi_inbox);
                }
                $result['last_request_time'] = date("Y-m-d H:i:s");
                $result['designation'][$user_designation['office_unit_organogram_id']]['name'] = $employee_office['designation'];
                $result['designation'][$user_designation['office_unit_organogram_id']]['data'] = array(
                    'totalDak' => intval($dak_count_inbox) + intval($dak_count_inboxng),
                    'totalDakNew' => intval($dak_count_new) + intval($dak_count_newng),
                    'totalNothi' => intval($total_nothi),
                    'totalNothiNew' => intval($total_nothi_inbox),
                );
            }
        }

        $jsonArray = array(
            "status" => "success",
            "data" => $result
        );

        $this->response->body(json_encode($jsonArray));
        $this->response->type('application/json');
        return $this->response;
    }

    public function getOfficesId()
    {
        $offices_table = TableRegistry::get('Offices');
        $offices_data = $offices_table->find()->select(['id'])->toArray();
        $offices_ids = [];
        foreach ($offices_data as $office_data) {
            $offices_ids[] = $office_data['id'];
        }

        $this->response->body(json_encode($offices_ids));
        $this->response->type('application/json');
        return $this->response;
    }

    public function getNewNotification($notificationtype = 'mobile_app')
    {
        $user_id = $this->request->query['user_id'];

        $jsonArray = array(
            "status" => "error",
            "error_code" => 404,
            "message" => "Invalid Request"
        );

        if (!defined("PROJAPOTI_API") || PROJAPOTI_API == false) {
            echo json_encode($jsonArray);
            die;
        }

        if (empty($user_id)) {
            echo json_encode($jsonArray);
            die;
        }

        $apikey = isset($this->request->query['api_key']) ? $this->request->query['api_key'] : '';

        if (!defined("API_KEY") || API_KEY == false || empty($apikey) || $apikey != API_KEY) {
            echo json_encode($jsonArray);
            die;
        }

        $usersTable = TableRegistry::get('Users');
        $user_info = $usersTable->get($user_id);
        $EmployeeOfficesTable = TableRegistry::get('EmployeeOffices');

        $user_designations = $EmployeeOfficesTable->getEmployeeOfficeRecords($user_info->employee_record_id);
        $event = TableRegistry::get('NotificationEvents');
        $totalByCat = [];

        if (!empty($user_designations)) {
            foreach ($user_designations as $key => $value) {
                $this->switchOffice($value['office_id'], 'OwnOffice');
                TableRegistry::remove('NotificationMessages');
                $notificationTable = TableRegistry::get('NotificationMessages');

                $totalNotification = $notificationTable->getNewNotification($user_info->employee_record_id,
                    $notificationtype);

                if (!empty($totalNotification)) {
                    foreach ($totalNotification as $ke => $val) {
                        $eventname = $event->getTypeDetail($val['event_id']);

                        $type = explode('.', $eventname['notification_type']);
                        if (!isset($totalByCat[ucfirst($type[0])][$eventname['event_name_bng']])) {
                            $totalByCat[ucfirst($type[0])][$eventname['event_name_bng']] = 1;
                        } else {
                            $totalByCat[ucfirst($type[0])][$eventname['event_name_bng']]++;
                        }
                    }
                }
            }
        }

        if (!empty($totalByCat)) {
            foreach ($totalByCat as $key => &$value) {
                array_walk_recursive($value,
                    function (&$number) {
                        $number = Number::format($number);
                    });
            }
        }

        $this->response->body(json_encode($totalByCat));
        $this->response->type('application/json');
        return $this->response;
    }

    public function monitorLayers($keyword = 'Other')
    {
        set_time_limit(0);
        $this->layout = 'dashboard';

        $today = date('Y-m-d');
        if (!empty($keyword)) {
            $offceLayerTable = TableRegistry::get('OfficeLayers');

            if (($Offices = Cache::read('layer_wise_Offices_id_and_name_' . $keyword, 'memcached')) === false) {
                //            $result = $table_reports->officeSummary($officeid);
                $Offices = $offceLayerTable->getLayerwiseOfficeEmployeeCount($keyword);
                Cache::write('layer_wise_Offices_id_and_name_' . $keyword, $Offices, 'memcached');
            }

            if ($keyword == 'Directorate') {
                $keyword = 'অধিদপ্তর ও দপ্তর সংস্থা / পরিদপ্তর / সংস্থা / প্রকল্প';
            }
            if ($keyword == 'Upazilla') {
                $keyword = 'উপজেলা';
            }
            if ($keyword == 'Divisional') {
                $keyword = 'বিভাগ';
            }
           $token =  $this->makeEncryptedData(date('Y-m-d H:i:s', strtotime("+60 min")));
            $this->set(compact('keyword','today','token','Offices'));
        }
    }
    public function getOfficeLoginCount(){
        $office_id = isset($this->request->data['office_id'])?$this->request->data['office_id']:0;
        $office_name = isset($this->request->data['office_name'])?$this->request->data['office_name']:'';
        $start_date = isset($this->request->data['start_date'])?$this->request->data['start_date']:0;
        $end_date = isset($this->request->data['end_date'])?$this->request->data['end_date']:'';
        $token = isset($this->request->data['token'])?$this->request->data['token']:'';

        $data = ['status' => 'success', 'message' => 'Something Went Wrong'];

        try{
            if(empty($token)){
                $data['message'] = 'অনুরোধ গ্রহণযোগ্য নয়। দয়া করে পুনরায় চেষ্টা করুন।Code: 1';
                goto rtn;
            }else{
                $token = $this->getDecryptedData($token);
                if(empty($token)){
                    $data['message'] = 'অনুরোধ গ্রহণযোগ্য নয়। দয়া করে পুনরায় চেষ্টা করুন।Code: 2';
                    goto rtn;
                }
                if($token < date('Y-m-d H:i:s')){
                    $data['message'] = 'অনুরোধ গ্রহণযোগ্য নয়। দয়া করে পুনরায় চেষ্টা করুন।Code: 3';
                    goto rtn;
                }
            }

            $UserLoginHistoryTable = TableRegistry::get('UserLoginHistory');
            $UnitWiseUserLoginTable = TableRegistry::get('UnitWiseUserLogin');

            if(!isset($office_name)){
                $office_info = TableRegistry::get('Offices')->getBanglaName($office_id);
                $office_name =isset($office_info['office_name_bng'])?$office_info['office_name_bng']:'';
            }
            if (!empty($office_id)) {

                $data['Name'] = $office_name;
                $data['Id'] = $office_id;
                $totalUser = $UnitWiseUserLoginTable->getOfficeData($office_id, [$start_date, $end_date])->first();

                $data['TotalUser'] = $totalUser['total_user'];
                $data['TodayLogin'] = $UserLoginHistoryTable->getLoginCount(0, 0, 0, $office_id, 0, 0, [$start_date, $end_date])->count();
                $data['YesterdayLogin'] = $totalUser['yesterday_login'];
                $data['status'] = 'success';
            }
        }catch (\Exception $ex){
            $data['message'] = __('Technical error happen');
            $data['reason'] = $this->makeEncryptedData($ex->getMessage());
        }

        rtn:
        $this->response->type('application/json');
        $this->response->body(json_encode($data));
        return $this->response;
    }

    public function loginDetails($office_id = 0, $unit_id = 0, $designtion_id = 0, $time = [],
                                 $order = [])
    {
        $this->layout = 'dashboard';
        $employee_office = $this->getCurrentDakSection();
        if (!empty($employee_office)) {
            $is_session = 1;
        } else {
            $is_session = 0;
        }
        $this->set('is_session', $is_session);
        if ($this->request->is('post')) {
            $this->layout = 'ajax';
            if (!empty($this->request->data['office_id'])) {
                $office_id = intval($this->request->data['office_id']);
            }
            if (!empty($this->request->data['unit_id'])) {
                $unit_id = intval($this->request->data['unit_id']);
            }
            if (!empty($this->request->data['designation_id'])) {
                $designtion_id = intval($this->request->data['designation_id']);
            }
        }
        $url = '';
        $tableHeader = '';
        $tableType = '';
        $office_id = intval($office_id);
        $unit_id = intval($unit_id);
        $designtion_id = intval($designtion_id);
        if (!empty($office_id)) {
            $url .= "/" . $office_id;
        } else {
            $url .= "/0";
        }
        if (!empty($unit_id)) {
            $url .= "/" . $unit_id;
        } else {
            $url .= "/0";
        }
        if (!empty($designtion_id)) {
            $designationTable = TableRegistry::get('OfficeUnitOrganograms');
            $EmployeeRecordsTable = TableRegistry::get('EmployeeRecords');
            $employee_info = $EmployeeRecordsTable->getEmployeeInfoByDesignation($designtion_id);
            $designation_info = $designationTable->get($designtion_id);
            $tableHeader = $employee_info['name_bng'] . ", " . $designation_info['designation_bng'];
            $tableType = 'Designation';
            $url .= "/" . $designtion_id;
        } else {
            if (!empty($unit_id)) {
                $unitTable = TableRegistry::get('OfficeUnits');
                $unit_info = $unitTable->get($unit_id);
                $tableHeader = $unit_info['unit_name_bng'];
                $tableType = 'Unit';
            } else {
                if (!empty($office_id)) {
                    $office_table = TableRegistry::get('Offices');
                    $office_info = $office_table->get($office_id);
                    $tableHeader = $office_info['office_name_bng'];
                    $tableType = 'Office';
                } else {
                    die;
                }
            }
        }
        $today = date('Y-m-d');
        $yesterday = date('Y-m-d', strtotime($today . ' -1 day'));
        $userLoginHistoriesTable = TableRegistry::get('UserLoginHistory');
        $todayLogin = $userLoginHistoriesTable->getLogingHistory($office_id, $unit_id,
            $designtion_id, [$today, $today], ['EmployeeOffices.designation_level ASC'])->toArray();
        $yesterdayLogin = $userLoginHistoriesTable->getLogingHistory($office_id, $unit_id,
            $designtion_id, [$yesterday, $yesterday], ['EmployeeOffices.designation_level ASC'])->toArray();
        $data['today'] = $todayLogin;
        $data['yesterday'] = $yesterdayLogin;

        $this->set(compact('url'));
        $this->set(compact('data'));
        $this->set(compact('tableHeader'));
        $this->set(compact('tableType'));
    }

    public function testOffice()
    {
        echo '<pre>';
        echo 'Curl request initiated: http://localhost:8918/deviceId';
        pr(curlRequest('http://localhost:8918/deviceId'));
        echo 'Curl request initiated: http://192.168.10.145:8918/deviceId';
        pr(curlRequest('http://192.168.10.145:8918/deviceId'));
        pr($this->getCurrentDakSection());
        die;
        $date = date('Y-m-d');
        $this->switchOffice(11, 'OfficeDB');
        echo '<pre>';
        print_r(TableRegistry::get('Reports')->newPerformanceReport(11,
                            0, 0, ['date_start' => $date, 'date_end' => $date]));
        print_r($this->getCurrentDakSection());
        die;
        set_time_limit(0);
        ini_set("Memory", '1024MB');
        $array = [
            'user_login_history', 'dak_actions', 'nothi_decisions', 'dak_movements', 'dak_users', 'nothi_master_permissions', 'nothi_master_current_users',
            'nothi_master_movements', 'nothi_potro_attachments', 'nothi_parts', 'potrojari', 'potrojari_attachments', 'nothi_note_signatures',
            'nothi_notes', 'potrojari_versions', 'nothi_note_attachments', 'office_designation_seals'
        ];
//        $array = [
//            'note_flags','nothi_dak_potro_maps','nothi_masters','nothi_masters_dak_map','nothi_master_movements',
//            'nothi_notes','nothi_note_attachments','nothi_note_attachment_refs','nothi_note_permissions',
//            'nothi_note_signatures','nothi_parts','nothi_potros','nothi_potro_attachments',
//            'potrojari','potrojari_attachments','potrojari_onulipi','potrojari_receiver','potrojari_versions','potro_flags'
//        ];
        if (!empty($array)) {
            foreach ($array as $a_k => $table_name) {
//                  echo "delete from `".$table_name."` where date(`created`) < '24-04-17';<br><br>";
                echo 'ALTER TABLE `' . $table_name . '` DROP `token`, DROP `device_type`, DROP `device_id`;<br><br>';
                echo "ALTER TABLE `" . $table_name . "` ADD `token` BIGINT NULL DEFAULT NULL , ADD `device_type` VARCHAR(50) NOT NULL DEFAULT 'web' AFTER `token`, ADD `device_id` VARCHAR(100) NULL DEFAULT NULL AFTER `device_type`;<br><br>";
            }
        }
        die;
    }

    public function monitorDashboardReportsOfOffices()
    {
        set_time_limit(0);
        ini_set('memory_limit', -1);
        $MonitorOfficesTable = TableRegistry::get('MonitorOffices');
        $officesTable = TableRegistry::get('Offices');
        if ($this->request->is('get')) {
            $this->layout = 'dashboard';
            $all_offices = $officesTable->find('list')->where(['active_status' => 1])->toArray();
            $this->set(compact('all_offices'));
            $this->view = 'monitor_report_offices';
        }
        if ($this->request->is('post')) {
            $today = date('Y-m-d');
            $yesterday = date('Y-m-d', strtotime($today . ' -1 day'));
            $result = [];
            $function = '';
            $data = [
                'status' => 'Error',
                'msg' => 'No Office Id'
            ];
            if (empty($this->request->data['office_id'])) {
                $this->response->type('application/json');
                $this->response->body(json_encode($data));
                return $this->response;
            }
            $office_id = $this->request->data['office_id'];
            if (!empty($office_id)) {
                $office_record_today = $MonitorOfficesTable->checkUpdate($office_id, $today);
                if ($office_record_today > 0) {
                    $record = $MonitorOfficesTable->getLastUpdateTime($office_id);
                    $cur_time = strtotime(date('Y-m-d H:i:s'));
                    $record_time = strtotime($record['modified']->format("Y-m-d H:i:s"));
                    $time_difference = round(abs($record_time - $cur_time) / 60, 2);
                    if ($time_difference > 60) {
                        $query = $MonitorOfficesTable->deleteDataofOffices($office_id,
                            $today);
                        $office_record_today = 0;
                    }
                }
                if ($office_record_today < 1) {
                    $office_record_yesterday = $MonitorOfficesTable->checkUpdate($office_id,
                        $yesterday);
                    if ($office_record_yesterday < 1) {
                        $MonitorOfficesTable->deleteDataofOffices($office_id);
                        $result = $this->reportMonitorDashboardSummary($office_id);
                        $function = 'report';
                    } else {
                        $result = $this->updateMonitorDashboardSummary($office_id);
                        $function = 'update';
                    }
                } else {
                    $data = [
                        'status' => 'Success',
                        'msg' => 'Already Updated'
                    ];
                    $this->response->type('application/json');
                    $this->response->body(json_encode($data));
                    return $this->response;
                }
            }

            if (!empty($result)) {
                if (!empty($function) && $function == 'report') {
                    $monitorOfficesEntity = $MonitorOfficesTable->newEntity();
                    $monitorOfficesEntity->office_id = $office_id;
                    $monitorOfficesEntity->yesterdayinbox = $result['totalyesterdayinbox'];
                    $monitorOfficesEntity->yesterdayoutbox = $result['totalyesterdayoutbox'];
                    $monitorOfficesEntity->yesterdaynothijat = $result['totalyesterdaynothijato'];
                    $monitorOfficesEntity->yesterdaynothivukto = $result['totalyesterdaynothivukto'];
                    $monitorOfficesEntity->totalnisponnodak = $result['totalnisponnodakall'];
                    $monitorOfficesEntity->totalonisponnodak = $result['totalOnisponnodakall'];
                    $monitorOfficesEntity->yesterdayselfnote = $result['totalyesterdaysrijitonote'];
                    $monitorOfficesEntity->yesterdaydaksohonote = $result['totalyesterdaydaksohonote'];
                    $monitorOfficesEntity->yesterdaypotrojari = $result['totalyesterdaypotrojari'];
                    $monitorOfficesEntity->totalnisponnonote = $result['totalnisponnonoteall'];
                    $monitorOfficesEntity->totalonisponnonote = $result['totalOnisponnonoteall'];
                    $monitorOfficesEntity->totalnisponno = $result['totalnisponnoall'];
                    $monitorOfficesEntity->totalonisponno = $result['totalOnisponnoall'];
                    $monitorOfficesEntity->status = $result['Status'];
                    $monitorOfficesEntity->updated = date('Y-m-d');

                    if ($MonitorOfficesTable->save($monitorOfficesEntity)) {
                        $data = [
                            'status' => 'Success',
                            'msg' => 'Full'
                        ];
                    }
                } elseif (!empty($function) && $function == 'update') {
                    $office_record_yesterday = $MonitorOfficesTable->getOfficeData($office_id,
                        [$yesterday, $yesterday])->order(['date(modified) desc'])->first();
                    $monitorOfficesEntity = $MonitorOfficesTable->newEntity();
                    $monitorOfficesEntity->office_id = $office_id;
                    $monitorOfficesEntity->yesterdayinbox = $result['totalyesterdayinbox'];
                    $monitorOfficesEntity->yesterdayoutbox = $result['totalyesterdayoutbox'];
                    $monitorOfficesEntity->yesterdaynothijat = $result['totalyesterdaynothijato'];
                    $monitorOfficesEntity->yesterdaynothivukto = $result['totalyesterdaynothivukto'];
                    $monitorOfficesEntity->totalnisponnodak = $result['totaltodaynisponnodak'];
                    $monitorOfficesEntity->totalonisponnodak = $result['totaltodayOnisponnodak'];

                    $monitorOfficesEntity->yesterdayselfnote = $result['totalyesterdaysrijitonote'];
                    $monitorOfficesEntity->yesterdaydaksohonote = $result['totalyesterdaydaksohonote'];
                    $monitorOfficesEntity->yesterdaypotrojari = $result['totalyesterdaypotrojari'];
                    $monitorOfficesEntity->totalnisponnonote = $result['totaltodaynisponnonote'];
                    $monitorOfficesEntity->totalonisponnonote = $result['totaltodayOnisponnonote'];
                    $monitorOfficesEntity->totalnisponno = $result['totaltodaynisponnodak'] + $result['totaltodaynisponnonote'];
                    $monitorOfficesEntity->totalonisponno = $result['totaltodayOnisponnonote'] + $result['totaltodayOnisponnodak'];
                    $monitorOfficesEntity->status = $result['Status'];
                    $monitorOfficesEntity->updated = date('Y-m-d');
                    if ($MonitorOfficesTable->save($monitorOfficesEntity)) {
                        $data = [
                            'status' => 'Success',
                            'msg' => 'Partial'
                        ];
                    }
                }
            }
            $this->response->type('application/json');
            $this->response->body(json_encode($data));
            return $this->response;
        }
    }

    private function monitorOfficeSummary($office_id = 0)
    {
        if (empty($office_id)) {
            return;
        }
        $monitorOfficeTable = TableRegistry::get('MonitorOffices');
        $today = date('Y-m-d');
        $yesterday = date('Y-m-d', strtotime($today . ' -1 day'));
        $office_data = [];
        $has_data = $monitorOfficeTable->checkUpdate($office_id, $today);
        if ($has_data > 0) {
            $office_data = $monitorOfficeTable->getOfficeData($office_id,
                [$today, $today])->order(['date(modified) desc'])->first();
            $returnSummary['totalyesterdayinbox'] = $office_data['yesterdayinbox'];

            $returnSummary['totalyesterdayoutbox'] = $office_data['yesterdayoutbox'];

            $returnSummary['totalyesterdaynothivukto'] = $office_data['yesterdaynothivukto'];

            $returnSummary['totalyesterdaynothijato'] = $office_data['yesterdaynothijat'];

            $returnSummary['totalnisponnodakall'] = $office_data['totalnisponnodak'];

            $returnSummary['totalOnisponnodakall'] = $office_data['totalonisponnodak'];

            $returnSummary['totalyesterdaypotrojari'] = $office_data['yesterdaypotrojari'];

            $returnSummary['totalyesterdaydaksohonote'] = $office_data['yesterdaydaksohonote'];

            $returnSummary['totalyesterdaysrijitonote'] = $office_data['yesterdayselfnote'];

            $returnSummary['totalnisponnonoteall'] = $office_data['totalnisponnonote'];

            $returnSummary['totalOnisponnonoteall'] = $office_data['totalonisponnonote'];

            $returnSummary['totalOnisponnoall'] = $office_data['totalonisponnonote'];

            $returnSummary['totalnisponnoall'] = $office_data['totalnisponno'];

            $returnSummary['status'] = $office_data['status'];
        } else if ($monitorOfficeTable->checkUpdate($office_id, $yesterday) > 0) {
            $office_data = $monitorOfficeTable->getOfficeData($office_id,
                [$yesterday, $yesterday])->order(['date(modified) desc'])->first();
            $returnSummary['totalyesterdayinbox'] = $office_data['yesterdayinbox'];

            $returnSummary['totalyesterdayoutbox'] = $office_data['yesterdayoutbox'];

            $returnSummary['totalyesterdaynothivukto'] = $office_data['yesterdaynothivukto'];

            $returnSummary['totalyesterdaynothijato'] = $office_data['yesterdaynothijat'];

            $returnSummary['totalnisponnodakall'] = $office_data['totalnisponnodak'];

            $returnSummary['totalOnisponnodakall'] = $office_data['totalonisponnodak'];

            $returnSummary['totalyesterdaypotrojari'] = $office_data['yesterdaypotrojari'];

            $returnSummary['totalyesterdaydaksohonote'] = $office_data['yesterdaydaksohonote'];

            $returnSummary['totalyesterdaysrijitonote'] = $office_data['yesterdayselfnote'];

            $returnSummary['totalnisponnonoteall'] = $office_data['totalnisponnonote'];

            $returnSummary['totalOnisponnonoteall'] = $office_data['totalonisponnonote'];

            $returnSummary['totalOnisponnoall'] = $office_data['totalonisponnonote'];

            $returnSummary['totalnisponnoall'] = $office_data['totalnisponno'];

            $returnSummary['status'] = $office_data['status'];
        } else {
//            $returnSummary = $this->MonitorDashboardSummary($office_id);
            $returnSummary['totalyesterdayinbox'] = 0;

            $returnSummary['totalyesterdayoutbox'] = 0;

            $returnSummary['totalyesterdaynothivukto'] = 0;

            $returnSummary['totalyesterdaynothijato'] = 0;

            $returnSummary['totalnisponnodakall'] = 0;

            $returnSummary['totalOnisponnodakall'] = 0;

            $returnSummary['totalyesterdaypotrojari'] = 0;

            $returnSummary['totalyesterdaydaksohonote'] = 0;

            $returnSummary['totalyesterdaysrijitonote'] = 0;

            $returnSummary['totalnisponnonoteall'] = 0;

            $returnSummary['totalOnisponnonoteall'] = 0;

            $returnSummary['totalOnisponnoall'] = 0;

            $returnSummary['totalnisponnoall'] = 0;

            $returnSummary['status'] = 0;
        }
        return $returnSummary;
    }

    public function newMonitor()
    {

        set_time_limit(0);
        $this->layout = 'dashboard';
        if ($this->request->is('get')) {
            $table_instance = TableRegistry::get('OfficeMinistries');
            $data_items = $table_instance->find('list')->toArray();
            $this->set('officeMinistries', $data_items);
            $monitor_office = TableRegistry::get('MonitorOffices');
            $monitor_time = $monitor_office->getLastUpdateTime();
            $this->set(compact('monitor_time'));
        }


        if ($this->request->is('post')) {
            $this->layout = 'ajax';
            $office_layers_table = TableRegistry::get('OfficeLayers');
            $unit_wise_user_login_table = TableRegistry::get('UnitWiseUserLogin');

            // get layer level
            $layerLevels = (jsonA(OFFICE_LAYER_TYPE));

            $today = date('Y-m-d');

            $data = [];
            if(!empty($layerLevels)){
                $layerLevels[0] = 'all';
                foreach ($layerLevels as $level){
                    if($level == 'all'){
                        if (($OfficeIds = Cache::read('layer_wise_Offices_id_and_name_all', 'memcached')) === false) {
                            $OfficeIds = $office_layers_table->getLayerwiseOfficeEmployeeCount();
                            Cache::write('layer_wise_Offices_id_and_name_all', $OfficeIds, 'memcached');
                        }
                    }else{
                        if (($OfficeIds = Cache::read('layer_wise_Offices_id_and_name_'.$level, 'memcached')) === false) {
                            $OfficeIds = $office_layers_table->getLayerwiseOfficeEmployeeCount($level);
                            Cache::write('layer_wise_Offices_id_and_name_'.$level, $OfficeIds, 'memcached');
                        }
                    }
                    $data['totaloffices_'.$level] = count($OfficeIds);
                    $sum = $unit_wise_user_login_table->getOfficeData(array_keys($OfficeIds), [$today, $today])->first();
                    $data['sum_'.$level] = $sum['total_user'];
                    $data['yesterday_login_'.$level] = $sum['yesterday_login'];
                }
                // now its time to calculate other
                $data['totaloffices_Other'] = $data['totaloffices_all'];
                $data['sum_Other'] = $data['sum_all'];
                $data['yesterday_login_Other'] = $data['yesterday_login_all'];
                foreach($layerLevels as $level){
                    if($level == 'all'){
                        continue;
                    }
                    $data['sum_Other'] -= $data['sum_'.$level];
                    $data['totaloffices_Other'] -= $data['totaloffices_'.$level];
                    $data['yesterday_login_Other'] -= $data['yesterday_login_'.$level];
                }
                $layerLevels[] = 'Other';
            }
//            pr($data);die;
            $this->set(compact('data','layerLevels'));
            $this->view = 'summary_view';
        }
    }

    public function newMonitorAjax()
    {
        set_time_limit(0);
        if ($this->request->is('ajax')) {
            $this->layout = 'ajax';
            $alldata = [];
            $MinistryId = 0;
            $LayerId = 0;
            $OriginId = 0;
            $totalLogin = 0;
            if (!empty($this->request->data['office_ministry_id'])) {
                $MinistryId = $this->request->data['office_ministry_id'];
                TableRegistry::remove('OfficeMinistries');
                $ministry_table = TableRegistry::get('OfficeMinistries');
                $ministry_name = $ministry_table->getBanglaName($this->request->data['office_ministry_id']);
            }
            if (($alldata = Cache::read('monitor_Dashboard' . $MinistryId, 'memcached')) === false) {
                $office_ids = !empty($this->request->data['office_id']) ? $this->request->data['office_id']
                    : array();
                $office_ids_name = !empty($this->request->data['name']) ? ($this->request->data['name'])
                    : '';
//            {
                $office_layers = !empty($this->request->data['office_layer_id']) ? $this->request->data['office_layer_id']
                    : array();

                if (!empty($office_layers)) {

                    foreach ($office_layers as $layerKey => $layerValue) {
                        $office_origins = array();
                        TableRegistry::remove('OfficeLayers');
                        $office_layer_table = TableRegistry::get('OfficeLayers');
                        $layer_name = $office_layer_table->getBanglaName($layerKey);
                        $LayerId = $layerKey;
                        TableRegistry::remove('OfficeOrigins');
                        $table_office_origins = TableRegistry::get('OfficeOrigins');
                        $office_origins = $table_office_origins->getOriginsByLayer($layerKey);

                        if (!empty($office_origins)) {
                            foreach ($office_origins as $key => $value) {
                                $OriginId = $key;
                                $origin_name = $table_office_origins->getBanglaName($key);
                                TableRegistry::remove('Offices');
                                $table_offices = TableRegistry::get('Offices');
                                $offices = $table_offices->getOfficesByOrigin($key);

                                foreach ($offices as $office_key => $office_value) {
                                    if (!isset($alldata[$MinistryId]['TotalOffices'])) {
                                        $alldata[$MinistryId]['TotalOffices'] = 1;
                                    } else {
                                        $alldata[$MinistryId]['TotalOffices']++;
                                    }
                                    if (!isset($alldata[$MinistryId][$LayerId]['TotalOffices'])) {
                                        $alldata[$MinistryId][$LayerId]['TotalOffices'] = 1;
                                    } else {
                                        $alldata[$MinistryId][$LayerId]['TotalOffices']++;
                                    }
                                    if (!isset($alldata[$MinistryId][$LayerId][$OriginId]['TotalOffices'])) {
                                        $alldata[$MinistryId][$LayerId][$OriginId]['TotalOffices'] = 1;
                                    } else {
                                        $alldata[$MinistryId][$LayerId][$OriginId]['TotalOffices']++;
                                    }
                                    $result = '';
                                    $result = $this->monitorOfficeSummary($office_key);
                                    if (!isset($result)) {
                                        $table_eo = TableRegistry::get('employee_offices');
//                                    $employee_records = $table_eo->getAllEmployeeRecordID($office_key);
//
//
//                                    $total_employee           = count($employee_records);
//                                    $user_login_history_table = TableRegistry::get('UserLoginHistory');
//                                    $totalLogin               = $user_login_history_table->countLogin(array_values($employee_records));
                                        $totalLogin = 0;
                                        $total_employee = 0;


                                        $alldata[$MinistryId][$LayerId][$OriginId][$office_key]['TotalEmployee']
                                            = Number::format($total_employee);
                                        $alldata[$MinistryId][$LayerId][$OriginId][$office_key]['TotalLogin']
                                            = Number::format($totalLogin);
                                        $alldata[$MinistryId][$LayerId][$OriginId][$office_key]['TotalYesterdayInbox']
                                            = Number::format(0);
                                        $alldata[$MinistryId][$LayerId][$OriginId][$office_key]['TotalYesterdayOutbox']
                                            = Number::format(0);
                                        $alldata[$MinistryId][$LayerId][$OriginId][$office_key]['TotalYesterdayNothijato']
                                            = Number::format(0);
                                        $alldata[$MinistryId][$LayerId][$OriginId][$office_key]['TotalYesterdayNothiVukto']
                                            = Number::format(0);
                                        $alldata[$MinistryId][$LayerId][$OriginId][$office_key]['TotalNisponnoDak']
                                            = Number::format(0);
                                        $alldata[$MinistryId][$LayerId][$OriginId][$office_key]['TotalOnisponnoDak']
                                            = Number::format(0);
                                        $alldata[$MinistryId][$LayerId][$OriginId][$office_key]['TotalYesterdaySrijitoNote']
                                            = Number::format(0);
                                        $alldata[$MinistryId][$LayerId][$OriginId][$office_key]['TotalYesterdayDakSohoNote']
                                            = Number::format(0);
                                        $alldata[$MinistryId][$LayerId][$OriginId][$office_key]['TotalYesterdayPotrojari']
                                            = Number::format(0);
                                        $alldata[$MinistryId][$LayerId][$OriginId][$office_key]['TotalNisponnoNote']
                                            = Number::format(0);
                                        $alldata[$MinistryId][$LayerId][$OriginId][$office_key]['TotalOnisponnoNote']
                                            = Number::format(0);

                                        $alldata[$MinistryId][$LayerId][$OriginId][$office_key]['Name'] = $office_value;
                                        $alldata[$MinistryId][$LayerId][$OriginId][$office_key]['Status']
                                            = 0;
                                    } else {
                                        $table_eo = TableRegistry::get('employee_offices');
//                                    $employee_records = $table_eo->getAllEmployeeRecordID($office_key);
//
//
//                                    $total_employee           = count($employee_records);
//                                    $user_login_history_table = TableRegistry::get('UserLoginHistory');
//                                    $totalLogin               = $user_login_history_table->countLogin(array_values($employee_records));
                                        $totalLogin = 0;
                                        $total_employee = 0;

                                        $alldata[$MinistryId][$LayerId][$OriginId][$office_key]['TotalEmployee']
                                            = Number::format($total_employee);
                                        $alldata[$MinistryId][$LayerId][$OriginId][$office_key]['Name'] = $office_value;
//                                    if(!isset($result['status'])){
                                        $alldata[$MinistryId][$LayerId][$OriginId][$office_key]['Status']
                                            = $result['status'];
//                                    }else{
//                                         $alldata[$MinistryId][$LayerId][$OriginId][$office_key]['Status'] = 1;
//                                    }

                                        $alldata[$MinistryId][$LayerId][$OriginId][$office_key]['TotalLogin']
                                            = Number::format($totalLogin);

                                        $alldata[$MinistryId][$LayerId][$OriginId][$office_key]['TotalYesterdayInbox']
                                            = Number::format($result['totalyesterdayinbox']);

                                        $alldata[$MinistryId][$LayerId][$OriginId][$office_key]['TotalYesterdayOutbox']
                                            = Number::format($result['totalyesterdayoutbox']);

                                        $alldata[$MinistryId][$LayerId][$OriginId][$office_key]['TotalYesterdayNothijato']
                                            = Number::format($result['totalyesterdaynothijato']);

                                        $alldata[$MinistryId][$LayerId][$OriginId][$office_key]['TotalYesterdayNothiVukto']
                                            = Number::format($result['totalyesterdaynothivukto']);

                                        $alldata[$MinistryId][$LayerId][$OriginId][$office_key]['TotalNisponnoDak']
                                            = Number::format($result['totalnisponnodakall']);

                                        $alldata[$MinistryId][$LayerId][$OriginId][$office_key]['TotalOnisponnoDak']
                                            = Number::format($result['totalOnisponnodakall']);

                                        $alldata[$MinistryId][$LayerId][$OriginId][$office_key]['TotalYesterdaySrijitoNote']
                                            = Number::format($result['totalyesterdaysrijitonote']);

                                        $alldata[$MinistryId][$LayerId][$OriginId][$office_key]['TotalYesterdayDakSohoNote']
                                            = Number::format($result['totalyesterdaydaksohonote']);

                                        $alldata[$MinistryId][$LayerId][$OriginId][$office_key]['TotalYesterdayPotrojari']
                                            = Number::format($result['totalyesterdaypotrojari']);

                                        $alldata[$MinistryId][$LayerId][$OriginId][$office_key]['TotalOnisponnoNote']
                                            = Number::format($result['totalOnisponnonoteall']);

                                        $alldata[$MinistryId][$LayerId][$OriginId][$office_key]['TotalNisponnoNote']
                                            = Number::format($result['totalnisponnonoteall']);

                                        /*                                     * For Ministry* */


                                        if (!isset($alldata[$MinistryId]['Offices'])) {
                                            if ($result['status'] == 1) {
                                                $alldata[$MinistryId]['Offices'] = 1;
                                            }
                                        } else {
                                            if ($result['status'] == 1) {
                                                $alldata[$MinistryId]['Offices']++;
                                            }
                                        }
                                        if (!isset($alldata[$MinistryId]['TotalEmployee'])) {
                                            $alldata[$MinistryId]['TotalEmployee'] = $total_employee;
                                        } else {
                                            $alldata[$MinistryId]['TotalEmployee'] += $total_employee;
                                        }
                                        if (!isset($alldata[$MinistryId]['TotalLogin'])) {
                                            $alldata[$MinistryId]['TotalLogin'] = $totalLogin;
                                        } else {
                                            $alldata[$MinistryId]['TotalLogin'] += $totalLogin;
                                        }
                                        if (!isset($alldata[$MinistryId]['TotalYesterdayInbox'])) {
                                            $alldata[$MinistryId]['TotalYesterdayInbox'] = $result['totalyesterdayinbox'];
                                        } else {
                                            $alldata[$MinistryId]['TotalYesterdayInbox'] += $result['totalyesterdayinbox'];
                                        }
                                        if (!isset($alldata[$MinistryId]['TotalYesterdayOutbox'])) {
                                            $alldata[$MinistryId]['TotalYesterdayOutbox'] = $result['totalyesterdayoutbox'];
                                        } else {
                                            $alldata[$MinistryId]['TotalYesterdayOutbox'] += $result['totalyesterdayoutbox'];
                                        }
                                        if (!isset($alldata[$MinistryId]['TotalYesterdayNothijato'])) {
                                            $alldata[$MinistryId]['TotalYesterdayNothijato'] = $result['totalyesterdaynothijato'];
                                        } else {
                                            $alldata[$MinistryId]['TotalYesterdayNothijato'] += $result['totalyesterdaynothijato'];
                                        }
                                        if (!isset($alldata[$MinistryId]['TotalYesterdayNothiVukto'])) {
                                            $alldata[$MinistryId]['TotalYesterdayNothiVukto'] = $result['totalyesterdaynothivukto'];
                                        } else {
                                            $alldata[$MinistryId]['TotalYesterdayNothiVukto'] += $result['totalyesterdaynothivukto'];
                                        }
                                        if (!isset($alldata[$MinistryId]['TotalNisponnoDak'])) {
                                            $alldata[$MinistryId]['TotalNisponnoDak'] = $result['totalnisponnodakall'];
                                        } else {
                                            $alldata[$MinistryId]['TotalNisponnoDak'] += $result['totalnisponnodakall'];
                                        }
                                        if (!isset($alldata[$MinistryId]['TotalOnisponnoDak'])) {
                                            $alldata[$MinistryId]['TotalOnisponnoDak'] = $result['totalOnisponnodakall'];
                                        } else {
                                            $alldata[$MinistryId]['TotalOnisponnoDak'] += $result['totalOnisponnodakall'];
                                        }

                                        if (!isset($alldata[$MinistryId]['TotalYesterdaySrijitoNote'])) {
                                            $alldata[$MinistryId]['TotalYesterdaySrijitoNote'] = $result['totalyesterdaysrijitonote'];
                                        } else {
                                            $alldata[$MinistryId]['TotalYesterdaySrijitoNote'] += $result['totalyesterdaysrijitonote'];
                                        }
                                        if (!isset($alldata[$MinistryId]['TotalYesterdayDakSohoNote'])) {
                                            $alldata[$MinistryId]['TotalYesterdayDakSohoNote'] = $result['totalyesterdaydaksohonote'];
                                        } else {
                                            $alldata[$MinistryId]['TotalYesterdayDakSohoNote'] += $result['totalyesterdaydaksohonote'];
                                        }
                                        if (!isset($alldata[$MinistryId]['TotalYesterdayPotrojari'])) {
                                            $alldata[$MinistryId]['TotalYesterdayPotrojari'] = $result['totalyesterdaypotrojari'];
                                        } else {
                                            $alldata[$MinistryId]['TotalYesterdayPotrojari'] += $result['totalyesterdaypotrojari'];
                                        }
                                        if (!isset($alldata[$MinistryId]['TotalNisponnoNote'])) {
                                            $alldata[$MinistryId]['TotalNisponnoNote'] = $result['totalnisponnonoteall'];
                                        } else {
                                            $alldata[$MinistryId]['TotalNisponnoNote'] += $result['totalnisponnonoteall'];
                                        }
                                        if (!isset($alldata[$MinistryId]['TotalOnisponnoNote'])) {
                                            $alldata[$MinistryId]['TotalOnisponnoNote'] = $result['totalOnisponnonoteall'];
                                        } else {
                                            $alldata[$MinistryId]['TotalOnisponnoNote'] += $result['totalOnisponnonoteall'];
                                        }

                                        /*                                     * For Ministry* */

//
                                        /*                                     * For Layer* */


                                        if (!isset($alldata[$MinistryId][$LayerId]['Offices'])) {
                                            if ($result['status'] == 1) {
                                                $alldata[$MinistryId][$LayerId]['Offices'] = 1;
                                            }
                                        } else {
                                            if ($result['status'] == 1) {
                                                $alldata[$MinistryId][$LayerId]['Offices']++;
                                            }
                                        }
                                        if (!isset($alldata[$MinistryId][$LayerId]['TotalEmployee'])) {
                                            $alldata[$MinistryId][$LayerId]['TotalEmployee'] = $total_employee;
                                        } else {
                                            $alldata[$MinistryId][$LayerId]['TotalEmployee'] += $total_employee;
                                        }
                                        if (!isset($alldata[$MinistryId][$LayerId]['TotalLogin'])) {
                                            $alldata[$MinistryId][$LayerId]['TotalLogin'] = $totalLogin;
                                        } else {
                                            $alldata[$MinistryId][$LayerId]['TotalLogin'] += $totalLogin;
                                        }

                                        if (!isset($alldata[$MinistryId][$LayerId]['TotalYesterdayInbox'])) {
                                            $alldata[$MinistryId][$LayerId]['TotalYesterdayInbox'] = $result['totalyesterdayinbox'];
                                        } else {
                                            $alldata[$MinistryId][$LayerId]['TotalYesterdayInbox'] += $result['totalyesterdayinbox'];
                                        }
                                        if (!isset($alldata[$MinistryId][$LayerId]['TotalYesterdayOutbox'])) {
                                            $alldata[$MinistryId][$LayerId]['TotalYesterdayOutbox'] = $result['totalyesterdayoutbox'];
                                        } else {
                                            $alldata[$MinistryId][$LayerId]['TotalYesterdayOutbox'] += $result['totalyesterdayoutbox'];
                                        }
                                        if (!isset($alldata[$MinistryId][$LayerId]['TotalYesterdayNothijato'])) {
                                            $alldata[$MinistryId][$LayerId]['TotalYesterdayNothijato'] = $result['totalyesterdaynothijato'];
                                        } else {
                                            $alldata[$MinistryId][$LayerId]['TotalYesterdayNothijato'] += $result['totalyesterdaynothijato'];
                                        }
                                        if (!isset($alldata[$MinistryId][$LayerId]['TotalYesterdayNothiVukto'])) {
                                            $alldata[$MinistryId][$LayerId]['TotalYesterdayNothiVukto'] = $result['totalyesterdaynothivukto'];
                                        } else {
                                            $alldata[$MinistryId][$LayerId]['TotalYesterdayNothiVukto'] += $result['totalyesterdaynothivukto'];
                                        }
                                        if (!isset($alldata[$MinistryId][$LayerId]['TotalNisponnoDak'])) {
                                            $alldata[$MinistryId][$LayerId]['TotalNisponnoDak'] = $result['totalnisponnodakall'];
                                        } else {
                                            $alldata[$MinistryId][$LayerId]['TotalNisponnoDak'] += $result['totalnisponnodakall'];
                                        }
                                        if (!isset($alldata[$MinistryId][$LayerId]['TotalOnisponnoDak'])) {
                                            $alldata[$MinistryId][$LayerId]['TotalOnisponnoDak'] = $result['totalOnisponnodakall'];
                                        } else {
                                            $alldata[$MinistryId][$LayerId]['TotalOnisponnoDak'] += $result['totalOnisponnodakall'];
                                        }

                                        if (!isset($alldata[$MinistryId][$LayerId]['TotalYesterdaySrijitoNote'])) {
                                            $alldata[$MinistryId][$LayerId]['TotalYesterdaySrijitoNote']
                                                = $result['totalyesterdaysrijitonote'];
                                        } else {
                                            $alldata[$MinistryId][$LayerId]['TotalYesterdaySrijitoNote'] += $result['totalyesterdaysrijitonote'];
                                        }
                                        if (!isset($alldata[$MinistryId][$LayerId]['TotalYesterdayDakSohoNote'])) {
                                            $alldata[$MinistryId][$LayerId]['TotalYesterdayDakSohoNote']
                                                = $result['totalyesterdaydaksohonote'];
                                        } else {
                                            $alldata[$MinistryId][$LayerId]['TotalYesterdayDakSohoNote'] += $result['totalyesterdaydaksohonote'];
                                        }
                                        if (!isset($alldata[$MinistryId][$LayerId]['TotalYesterdayPotrojari'])) {
                                            $alldata[$MinistryId][$LayerId]['TotalYesterdayPotrojari'] = ($result['totalyesterdaypotrojari']);
                                        } else {
                                            $alldata[$MinistryId][$LayerId]['TotalYesterdayPotrojari'] += $result['totalyesterdaypotrojari'];
                                        }
                                        if (!isset($alldata[$MinistryId][$LayerId]['TotalNisponnoNote'])) {
                                            $alldata[$MinistryId][$LayerId]['TotalNisponnoNote'] = $result['totalnisponnonoteall'];
                                        } else {
                                            $alldata[$MinistryId][$LayerId]['TotalNisponnoNote'] += $result['totalnisponnonoteall'];
                                        }
                                        if (!isset($alldata[$MinistryId][$LayerId]['TotalOnisponnoNote'])) {
                                            $alldata[$MinistryId][$LayerId]['TotalOnisponnoNote'] = $result['totalOnisponnonoteall'];
                                        } else {
                                            $alldata[$MinistryId][$LayerId]['TotalOnisponnoNote'] += $result['totalOnisponnonoteall'];
                                        }


                                        /*                                     * For Layer* */

                                        /*                                     * For Origin* */


                                        if (!isset($alldata[$MinistryId][$LayerId][$OriginId]['Offices'])) {
                                            if ($result['status'] == 1) {
                                                $alldata[$MinistryId][$LayerId][$OriginId]['Offices'] = 1;
                                            }
                                        } else {
                                            if ($result['status'] == 1) {
                                                $alldata[$MinistryId][$LayerId][$OriginId]['Offices']++;
                                            }
                                        }
                                        if (!isset($alldata[$MinistryId][$LayerId][$OriginId]['TotalEmployee'])) {
                                            $alldata[$MinistryId][$LayerId][$OriginId]['TotalEmployee'] = $total_employee;
                                        } else {
                                            $alldata[$MinistryId][$LayerId][$OriginId]['TotalEmployee'] += $total_employee;
                                        }
                                        if (!isset($alldata[$MinistryId][$LayerId][$OriginId]['TotalLogin'])) {
                                            $alldata[$MinistryId][$LayerId][$OriginId]['TotalLogin'] = $totalLogin;
                                        } else {
                                            $alldata[$MinistryId][$LayerId][$OriginId]['TotalLogin'] += $totalLogin;
                                        }

                                        if (!isset($alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayInbox'])) {
                                            $alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayInbox']
                                                = $result['totalyesterdayinbox'];
                                        } else {
                                            $alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayInbox'] += $result['totalyesterdayinbox'];
                                        }
                                        if (!isset($alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayOutbox'])) {
                                            $alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayOutbox']
                                                = $result['totalyesterdayoutbox'];
                                        } else {
                                            $alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayOutbox'] += $result['totalyesterdayoutbox'];
                                        }
                                        if (!isset($alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayNothijato'])) {
                                            $alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayNothijato']
                                                = $result['totalyesterdaynothijato'];
                                        } else {
                                            $alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayNothijato'] += $result['totalyesterdaynothijato'];
                                        }
                                        if (!isset($alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayNothiVukto'])) {
                                            $alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayNothiVukto']
                                                = $result['totalyesterdaynothivukto'];
                                        } else {
                                            $alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayNothiVukto'] += $result['totalyesterdaynothivukto'];
                                        }
                                        if (!isset($alldata[$MinistryId][$LayerId][$OriginId]['TotalNisponnoDak'])) {
                                            $alldata[$MinistryId][$LayerId][$OriginId]['TotalNisponnoDak']
                                                = $result['totalnisponnodakall'];
                                        } else {
                                            $alldata[$MinistryId][$LayerId][$OriginId]['TotalNisponnoDak'] += $result['totalnisponnodakall'];
                                        }
                                        if (!isset($alldata[$MinistryId][$LayerId][$OriginId]['TotalOnisponnoDak'])) {
                                            $alldata[$MinistryId][$LayerId][$OriginId]['TotalOnisponnoDak']
                                                = $result['totalOnisponnodakall'];
                                        } else {
                                            $alldata[$MinistryId][$LayerId][$OriginId]['TotalOnisponnoDak'] += $result['totalOnisponnodakall'];
                                        }

                                        if (!isset($alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdaySrijitoNote'])) {
                                            $alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdaySrijitoNote']
                                                = $result['totalyesterdaysrijitonote'];
                                        } else {
                                            $alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdaySrijitoNote'] += $result['totalyesterdaysrijitonote'];
                                        }
                                        if (!isset($alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayDakSohoNote'])) {
                                            $alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayDakSohoNote']
                                                = $result['totalyesterdaydaksohonote'];
                                        } else {
                                            $alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayDakSohoNote'] += $result['totalyesterdaydaksohonote'];
                                        }
                                        if (!isset($alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayPotrojari'])) {
                                            $alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayPotrojari']
                                                = $result['totalyesterdaypotrojari'];
                                        } else {
                                            $alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayPotrojari'] += $result['totalyesterdaypotrojari'];
                                        }
                                        if (!isset($alldata[$MinistryId][$LayerId][$OriginId]['TotalNisponnoNote'])) {
                                            $alldata[$MinistryId][$LayerId][$OriginId]['TotalNisponnoNote']
                                                = $result['totalnisponnonoteall'];
                                        } else {
                                            $alldata[$MinistryId][$LayerId][$OriginId]['TotalNisponnoNote'] += $result['totalnisponnonoteall'];
                                        }
                                        if (!isset($alldata[$MinistryId][$LayerId][$OriginId]['TotalOnisponnoNote'])) {
                                            $alldata[$MinistryId][$LayerId][$OriginId]['TotalOnisponnoNote']
                                                = $result['totalOnisponnonoteall'];
                                        } else {
                                            $alldata[$MinistryId][$LayerId][$OriginId]['TotalOnisponnoNote'] += $result['totalOnisponnonoteall'];
                                        }
                                    }
                                }
                                /*                             * *Making Bangla Number** */
                                /*                             * *Making Bangla Number** */

                                /*                             * For Origin* */
                                $alldata[$MinistryId][$LayerId][$OriginId]['Name'] = $origin_name['office_name_bng'];
                                if (!isset($alldata[$MinistryId][$LayerId][$OriginId]['TotalOffices'])) {
                                    $alldata[$MinistryId][$LayerId][$OriginId]['TotalOffices'] = Number::format(0);
                                } else {
                                    $alldata[$MinistryId][$LayerId][$OriginId]['TotalOffices'] = Number::format($alldata[$MinistryId][$LayerId][$OriginId]['TotalOffices']);
                                }
                                if (!isset($alldata[$MinistryId][$LayerId][$OriginId]['TotalLogin'])) {
                                    $alldata[$MinistryId][$LayerId][$OriginId]['TotalLogin'] = Number::format(0);
                                } else {
                                    $alldata[$MinistryId][$LayerId][$OriginId]['TotalLogin'] = Number::format($alldata[$MinistryId][$LayerId][$OriginId]['TotalLogin']);
                                }
                                if (!isset($alldata[$MinistryId][$LayerId][$OriginId]['Offices'])) {
                                    $alldata[$MinistryId][$LayerId][$OriginId]['Offices'] = Number::format(0);
                                } else {
                                    $alldata[$MinistryId][$LayerId][$OriginId]['Offices'] = Number::format($alldata[$MinistryId][$LayerId][$OriginId]['Offices']);
                                }
                                if (!isset($alldata[$MinistryId][$LayerId][$OriginId]['TotalEmployee'])) {
                                    $alldata[$MinistryId][$LayerId][$OriginId]['TotalEmployee'] = Number::format(0);
                                } else {
                                    $alldata[$MinistryId][$LayerId][$OriginId]['TotalEmployee'] = Number::format($alldata[$MinistryId][$LayerId][$OriginId]['TotalEmployee']);
                                }

                                if (!isset($alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayInbox'])) {
                                    $alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayInbox'] = Number::format(0);
                                } else {
                                    $alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayInbox'] = Number::format($alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayInbox']);
                                }
                                if (!isset($alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayOutbox'])) {
                                    $alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayOutbox'] = Number::format(0);
                                } else {
                                    $alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayOutbox'] = Number::format($alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayOutbox']);
                                }
                                if (!isset($alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayNothijato'])) {
                                    $alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayNothijato']
                                        = Number::format(0);
                                } else {
                                    $alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayNothijato']
                                        = Number::format($alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayNothijato']);
                                }
                                if (!isset($alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayNothiVukto'])) {
                                    $alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayNothiVukto']
                                        = Number::format(0);
                                } else {
                                    $alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayNothiVukto']
                                        = Number::format($alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayNothiVukto']);
                                }
                                if (!isset($alldata[$MinistryId][$LayerId][$OriginId]['TotalNisponnoDak'])) {
                                    $alldata[$MinistryId][$LayerId][$OriginId]['TotalNisponnoDak'] = Number::format(0);
                                } else {
                                    $alldata[$MinistryId][$LayerId][$OriginId]['TotalNisponnoDak'] = Number::format($alldata[$MinistryId][$LayerId][$OriginId]['TotalNisponnoDak']);
                                }
                                if (!isset($alldata[$MinistryId][$LayerId][$OriginId]['TotalOnisponnoDak'])) {
                                    $alldata[$MinistryId][$LayerId][$OriginId]['TotalOnisponnoDak'] = Number::format(0);
                                } else {
                                    $alldata[$MinistryId][$LayerId][$OriginId]['TotalOnisponnoDak'] = Number::format($alldata[$MinistryId][$LayerId][$OriginId]['TotalOnisponnoDak']);
                                }

                                if (!isset($alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdaySrijitoNote'])) {
                                    $alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdaySrijitoNote']
                                        = Number::format(0);
                                } else {
                                    $alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdaySrijitoNote']
                                        = Number::format($alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdaySrijitoNote']);
                                }
                                if (!isset($alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayDakSohoNote'])) {
                                    $alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayDakSohoNote']
                                        = Number::format(0);
                                } else {
                                    $alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayDakSohoNote']
                                        = Number::format($alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayDakSohoNote']);
                                }
                                if (!isset($alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayPotrojari'])) {
                                    $alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayPotrojari']
                                        = Number::format(0);
                                } else {
                                    $alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayPotrojari']
                                        = Number::format($alldata[$MinistryId][$LayerId][$OriginId]['TotalYesterdayPotrojari']);
                                }
                                if (!isset($alldata[$MinistryId][$LayerId][$OriginId]['TotalNisponnoNote'])) {
                                    $alldata[$MinistryId][$LayerId][$OriginId]['TotalNisponnoNote'] = Number::format(0);
                                } else {
                                    $alldata[$MinistryId][$LayerId][$OriginId]['TotalNisponnoNote'] = Number::format($alldata[$MinistryId][$LayerId][$OriginId]['TotalNisponnoNote']);
                                }
                                if (!isset($alldata[$MinistryId][$LayerId][$OriginId]['TotalOnisponnoNote'])) {
                                    $alldata[$MinistryId][$LayerId][$OriginId]['TotalOnisponnoNote'] = Number::format(0);
                                } else {
                                    $alldata[$MinistryId][$LayerId][$OriginId]['TotalOnisponnoNote'] = Number::format($alldata[$MinistryId][$LayerId][$OriginId]['TotalOnisponnoNote']);
                                }

                                /*                             * For Origin* */
                            }
                        }

                        /*                     * For Layers Bangla** */
                        $alldata[$MinistryId][$LayerId]['Name'] = $layer_name['layer_name_bng'];
                        if (!isset($alldata[$MinistryId][$LayerId]['TotalOffices'])) {
                            $alldata[$MinistryId][$LayerId]['TotalOffices'] = Number::format(0);
                        } else {
                            $alldata[$MinistryId][$LayerId]['TotalOffices'] = Number::format($alldata[$MinistryId][$LayerId]['TotalOffices']);
                        }
                        if (!isset($alldata[$MinistryId][$LayerId]['TotalLogin'])) {
                            $alldata[$MinistryId][$LayerId]['TotalLogin'] = Number::format(0);
                        } else {
                            $alldata[$MinistryId][$LayerId]['TotalLogin'] = Number::format($alldata[$MinistryId][$LayerId]['TotalLogin']);
                        }
                        if (!isset($alldata[$MinistryId][$LayerId]['Offices'])) {
                            $alldata[$MinistryId][$LayerId]['Offices'] = Number::format(0);
                        } else {
                            $alldata[$MinistryId][$LayerId]['Offices'] = Number::format($alldata[$MinistryId][$LayerId]['Offices']);
                        }
                        if (!isset($alldata[$MinistryId][$LayerId]['TotalEmployee'])) {
                            $alldata[$MinistryId][$LayerId]['TotalEmployee'] = Number::format(0);
                        } else {
                            $alldata[$MinistryId][$LayerId]['TotalEmployee'] = Number::format($alldata[$MinistryId][$LayerId]['TotalEmployee']);
                        }

                        if (!isset($alldata[$MinistryId][$LayerId]['TotalYesterdayInbox'])) {
                            $alldata[$MinistryId][$LayerId]['TotalYesterdayInbox'] = Number::format(0);
                        } else {
                            $alldata[$MinistryId][$LayerId]['TotalYesterdayInbox'] = Number::format($alldata[$MinistryId][$LayerId]['TotalYesterdayInbox']);
                        }
                        if (!isset($alldata[$MinistryId][$LayerId]['TotalYesterdayOutbox'])) {
                            $alldata[$MinistryId][$LayerId]['TotalYesterdayOutbox'] = Number::format(0);
                        } else {
                            $alldata[$MinistryId][$LayerId]['TotalYesterdayOutbox'] = Number::format($alldata[$MinistryId][$LayerId]['TotalYesterdayOutbox']);
                        }
                        if (!isset($alldata[$MinistryId][$LayerId]['TotalYesterdayNothijato'])) {
                            $alldata[$MinistryId][$LayerId]['TotalYesterdayNothijato'] = Number::format(0);
                        } else {
                            $alldata[$MinistryId][$LayerId]['TotalYesterdayNothijato'] = Number::format($alldata[$MinistryId][$LayerId]['TotalYesterdayNothijato']);
                        }
                        if (!isset($alldata[$MinistryId][$LayerId]['TotalYesterdayNothiVukto'])) {
                            $alldata[$MinistryId][$LayerId]['TotalYesterdayNothiVukto'] = Number::format(0);
                        } else {
                            $alldata[$MinistryId][$LayerId]['TotalYesterdayNothiVukto'] = Number::format($alldata[$MinistryId][$LayerId]['TotalYesterdayNothiVukto']);
                        }
                        if (!isset($alldata[$MinistryId][$LayerId]['TotalNisponnoDak'])) {
                            $alldata[$MinistryId][$LayerId]['TotalNisponnoDak'] = Number::format(0);
                        } else {
                            $alldata[$MinistryId][$LayerId]['TotalNisponnoDak'] = Number::format($alldata[$MinistryId][$LayerId]['TotalNisponnoDak']);
                        }
                        if (!isset($alldata[$MinistryId][$LayerId]['TotalOnisponnoDak'])) {
                            $alldata[$MinistryId][$LayerId]['TotalOnisponnoDak'] = Number::format(0);
                        } else {
                            $alldata[$MinistryId][$LayerId]['TotalOnisponnoDak'] = Number::format($alldata[$MinistryId][$LayerId]['TotalOnisponnoDak']);
                        }

                        if (!isset($alldata[$MinistryId][$LayerId]['TotalYesterdaySrijitoNote'])) {
                            $alldata[$MinistryId][$LayerId]['TotalYesterdaySrijitoNote'] = Number::format(0);
                        } else {
                            $alldata[$MinistryId][$LayerId]['TotalYesterdaySrijitoNote'] = Number::format($alldata[$MinistryId][$LayerId]['TotalYesterdaySrijitoNote']);
                        }
                        if (!isset($alldata[$MinistryId][$LayerId]['TotalYesterdayDakSohoNote'])) {
                            $alldata[$MinistryId][$LayerId]['TotalYesterdayDakSohoNote'] = Number::format(0);
                        } else {
                            $alldata[$MinistryId][$LayerId]['TotalYesterdayDakSohoNote'] = Number::format($alldata[$MinistryId][$LayerId]['TotalYesterdayDakSohoNote']);
                        }
                        if (!isset($alldata[$MinistryId][$LayerId]['TotalYesterdayPotrojari'])) {
                            $alldata[$MinistryId][$LayerId]['TotalYesterdayPotrojari'] = Number::format(0);
                        } else {
                            $alldata[$MinistryId][$LayerId]['TotalYesterdayPotrojari'] = Number::format($alldata[$MinistryId][$LayerId]['TotalYesterdayPotrojari']);
                        }
                        if (!isset($alldata[$MinistryId][$LayerId]['TotalNisponnoNote'])) {
                            $alldata[$MinistryId][$LayerId]['TotalNisponnoNote'] = Number::format(0);
                        } else {
                            $alldata[$MinistryId][$LayerId]['TotalNisponnoNote'] = Number::format($alldata[$MinistryId][$LayerId]['TotalNisponnoNote']);
                        }
                        if (!isset($alldata[$MinistryId][$LayerId]['TotalOnisponnoNote'])) {
                            $alldata[$MinistryId][$LayerId]['TotalOnisponnoNote'] = Number::format(0);
                        } else {
                            $alldata[$MinistryId][$LayerId]['TotalOnisponnoNote'] = Number::format($alldata[$MinistryId][$LayerId]['TotalOnisponnoNote']);
                        }

                        /*                     * For Layers Bangla** */
                    }
                }
                /*             * For Ministry Bangla** */
                $alldata[$MinistryId]['Name'] = $ministry_name['name_bng'];

                if (!isset($alldata[$MinistryId]['TotalOffices'])) {
                    $alldata[$MinistryId]['EnTotalOffices'] = 0;
                    $alldata[$MinistryId]['TotalOffices'] = Number::format(0);
                } else {
                    $alldata[$MinistryId]['EnTotalOffices'] = $alldata[$MinistryId]['TotalOffices'];
                    $alldata[$MinistryId]['TotalOffices'] = Number::format($alldata[$MinistryId]['TotalOffices']);
                }

                if (!isset($alldata[$MinistryId]['TotalLogin'])) {
                    $alldata[$MinistryId]['EnTotalLogin'] = 0;
                    $alldata[$MinistryId]['TotalLogin'] = Number::format(0);
                } else {
                    $alldata[$MinistryId]['EnTotalLogin'] = $alldata[$MinistryId]['TotalLogin'];
                    $alldata[$MinistryId]['TotalLogin'] = Number::format($alldata[$MinistryId]['TotalLogin']);
                }
                if (!isset($alldata[$MinistryId]['Offices'])) {
                    $alldata[$MinistryId]['EnOffices'] = 0;
                    $alldata[$MinistryId]['Offices'] = Number::format(0);
                } else {
                    $alldata[$MinistryId]['EnOffices'] = $alldata[$MinistryId]['Offices'];
                    $alldata[$MinistryId]['Offices'] = Number::format($alldata[$MinistryId]['Offices']);
                }
                if (!isset($alldata[$MinistryId]['TotalEmployee'])) {
                    $alldata[$MinistryId]['EnTotalEmployee'] = 0;
                    $alldata[$MinistryId]['TotalEmployee'] = Number::format(0);
                } else {
                    $alldata[$MinistryId]['EnTotalEmployee'] = $alldata[$MinistryId]['TotalEmployee'];
                    $alldata[$MinistryId]['TotalEmployee'] = Number::format($alldata[$MinistryId]['TotalEmployee']);
                }

                if (!isset($alldata[$MinistryId]['TotalYesterdayOutbox'])) {
                    $alldata[$MinistryId]['EnTotalYesterdayOutbox'] = 0;
                    $alldata[$MinistryId]['TotalYesterdayOutbox'] = Number::format(0);
                } else {
                    $alldata[$MinistryId]['EnTotalYesterdayOutbox'] = $alldata[$MinistryId]['TotalYesterdayOutbox'];
                    $alldata[$MinistryId]['TotalYesterdayOutbox'] = Number::format($alldata[$MinistryId]['TotalYesterdayOutbox']);
                }

                if (!isset($alldata[$MinistryId]['TotalYesterdayInbox'])) {
                    $alldata[$MinistryId]['EnTotalYesterdayInbox'] = 0;
                    $alldata[$MinistryId]['TotalYesterdayInbox'] = Number::format(0);
                } else {
                    $alldata[$MinistryId]['EnTotalYesterdayInbox'] = $alldata[$MinistryId]['TotalYesterdayInbox'];
                    $alldata[$MinistryId]['TotalYesterdayInbox'] = Number::format($alldata[$MinistryId]['TotalYesterdayInbox']);
                }

                if (!isset($alldata[$MinistryId]['TotalYesterdayNothijato'])) {
                    $alldata[$MinistryId]['EnTotalYesterdayNothijato'] = 0;
                    $alldata[$MinistryId]['TotalYesterdayNothijato'] = Number::format(0);
                } else {
                    $alldata[$MinistryId]['EnTotalYesterdayNothijato'] = $alldata[$MinistryId]['TotalYesterdayNothijato'];
                    $alldata[$MinistryId]['TotalYesterdayNothijato'] = Number::format($alldata[$MinistryId]['TotalYesterdayNothijato']);
                }

                if (!isset($alldata[$MinistryId]['TotalYesterdayNothiVukto'])) {
                    $alldata[$MinistryId]['EnTotalYesterdayNothiVukto'] = 0;
                    $alldata[$MinistryId]['TotalYesterdayNothiVukto'] = Number::format(0);
                } else {
                    $alldata[$MinistryId]['EnTotalYesterdayNothiVukto'] = $alldata[$MinistryId]['TotalYesterdayNothiVukto'];
                    $alldata[$MinistryId]['TotalYesterdayNothiVukto'] = Number::format($alldata[$MinistryId]['TotalYesterdayNothiVukto']);
                }
                if (!isset($alldata[$MinistryId]['TotalNisponnoDak'])) {
                    $alldata[$MinistryId]['EnTotalNisponnoDak'] = 0;
                    $alldata[$MinistryId]['TotalNisponnoDak'] = Number::format(0);
                } else {
                    $alldata[$MinistryId]['EnTotalNisponnoDak'] = $alldata[$MinistryId]['TotalNisponnoDak'];
                    $alldata[$MinistryId]['TotalNisponnoDak'] = Number::format($alldata[$MinistryId]['TotalNisponnoDak']);
                }
                if (!isset($alldata[$MinistryId]['TotalOnisponnoDak'])) {
                    $alldata[$MinistryId]['EnTotalOnisponnoDak'] = 0;
                    $alldata[$MinistryId]['TotalOnisponnoDak'] = Number::format(0);
                } else {
                    $alldata[$MinistryId]['EnTotalOnisponnoDak'] = $alldata[$MinistryId]['TotalOnisponnoDak'];
                    $alldata[$MinistryId]['TotalOnisponnoDak'] = Number::format($alldata[$MinistryId]['TotalOnisponnoDak']);
                }

                if (!isset($alldata[$MinistryId]['TotalYesterdaySrijitoNote'])) {
                    $alldata[$MinistryId]['EnTotalYesterdaySrijitoNote'] = 0;
                    $alldata[$MinistryId]['TotalYesterdaySrijitoNote'] = Number::format(0);
                } else {
                    $alldata[$MinistryId]['EnTotalYesterdaySrijitoNote'] = $alldata[$MinistryId]['TotalYesterdaySrijitoNote'];
                    $alldata[$MinistryId]['TotalYesterdaySrijitoNote'] = Number::format($alldata[$MinistryId]['TotalYesterdaySrijitoNote']);
                }
                if (!isset($alldata[$MinistryId]['TotalYesterdayDakSohoNote'])) {
                    $alldata[$MinistryId]['EnTotalYesterdayDakSohoNote'] = 0;
                    $alldata[$MinistryId]['TotalYesterdayDakSohoNote'] = Number::format(0);
                } else {
                    $alldata[$MinistryId]['EnTotalYesterdayDakSohoNote'] = $alldata[$MinistryId]['TotalYesterdayDakSohoNote'];
                    $alldata[$MinistryId]['TotalYesterdayDakSohoNote'] = Number::format($alldata[$MinistryId]['TotalYesterdayDakSohoNote']);
                }

                if (!isset($alldata[$MinistryId]['TotalYesterdayPotrojari'])) {
                    $alldata[$MinistryId]['EnTotalYesterdayPotrojari'] = 0;
                    $alldata[$MinistryId]['TotalYesterdayPotrojari'] = Number::format(0);
                } else {
                    $alldata[$MinistryId]['EnTotalYesterdayPotrojari'] = $alldata[$MinistryId]['TotalYesterdayPotrojari'];
                    $alldata[$MinistryId]['TotalYesterdayPotrojari'] = Number::format($alldata[$MinistryId]['TotalYesterdayPotrojari']);
                }

                if (!isset($alldata[$MinistryId]['TotalNisponnoNote'])) {
                    $alldata[$MinistryId]['EnTotalNisponnoNote'] = 0;
                    $alldata[$MinistryId]['TotalNisponnoNote'] = Number::format(0);
                } else {
                    $alldata[$MinistryId]['EnTotalNisponnoNote'] = $alldata[$MinistryId]['TotalNisponnoNote'];
                    $alldata[$MinistryId]['TotalNisponnoNote'] = Number::format($alldata[$MinistryId]['TotalNisponnoNote']);
                }

                if (!isset($alldata[$MinistryId]['TotalOnisponnoNote'])) {
                    $alldata[$MinistryId]['EnTotalOnisponnoNote'] = 0;
                    $alldata[$MinistryId]['TotalOnisponnoNote'] = Number::format(0);
                } else {
                    $alldata[$MinistryId]['EnTotalOnisponnoNote'] = $alldata[$MinistryId]['TotalOnisponnoNote'];
                    $alldata[$MinistryId]['TotalOnisponnoNote'] = Number::format($alldata[$MinistryId]['TotalOnisponnoNote']);
                }
                /*             * For Ministry Bangla** */
//            }
                Cache::write('monitor_Dashboard' . $MinistryId, $alldata, 'memcached');
            }

            $this->response->type('application/json');
            $this->response->body(json_encode($alldata));
            return $this->response;
        }
    }

    public function downloadApp()
    {
        $auth = $this->Auth->user();

        if (empty($auth)) {
//            $this->Flash->error("দুঃখিত! নথি অ্যাপ এর জন্য আপনাকে লগইন করতে হবে। ");
//            $this->Auth->redirectUrl(['action' => 'downloadApp', 'controller' => 'Dashboard']);
            $this->layout = 'dashboard';
        }
    }

    public function downloadUsermanual()
    {

        $path = ROOT . DS . 'webroot' . DS . (!empty($this->request->query['path']) ? $this->request->query['path'] . DS : '');
        $file = !empty($this->request->query['file']) ? $this->request->query['file'] : 'nothi_user_manual_v9';
        $ext = !empty($this->request->query['ext']) ? $this->request->query['ext'] : 'pdf';
        $name = !empty($this->request->query['name']) ? $this->request->query['name'] : 'ই-ফাইল ব্যবহার সহাথিকা.';

        if (!empty($file)) {
            if (file_exists($path . $file . "." . $ext)) {
                $this->response->file($path . $file . "." . $ext,
                    ['name' => $name . $ext, 'download' => true]);
            } else {
                die("দুঃখিত! ফাইল পাওয়া যায়নি!!");
            }
        } else {
            die("দুঃখিত! ফাইল পাওয়া যায়নি!!");
        }

        return $this->response;
    }

    public function agingOwnUnitCell()
    {
        set_time_limit(0);
        if ($this->request->is('post')) {
            if (!empty($this->request->data['office_id'])) {
                $office_id = $this->request->data['office_id'];
                $this->switchOffice($office_id, 'OfficeDB');
                echo $this->Cell('DakDashboard::agingAllUnit', [$office_id])->render('aging_other_desk');
            }
        }
        die;
    }

    public function ownUnitList()
    {
        $office_id = $this->request->data('office_id');
        $response = ['status' => 'success', 'msg' => 'Something Went Wrong'];
        $officeUnitTable = TableRegistry::get('OfficeUnits');
        try {
            $unitarray = $officeUnitTable->find('list',
                ['keyField' => 'id', 'valueField' => 'unit_name_bng'])->where(['office_id' => $office_id])->order(['parent_unit_id asc',
                'id desc'])->toArray();
            if (!empty($unitarray)) {
                $response = ['status' => 'success', 'data' => $unitarray];
            } else {
                $response = ['status' => 'success', 'data' => ' দুঃখিত কোন তথ্য পাওয়া যায়নি। '];
            }
        } catch (\Exception $ex) {
            $response['msg'] = $ex->getMessage();
        }
        $this->response->type('application/json');
        $this->response->body(json_encode($response));
        return $this->response;
    }

    public function loadSingleUnit($office_id = 0, $unit_id = 0, $unit_name = '')
    {
        try {
            $this->switchOffice($office_id, 'OfficeDB');
            echo $this->Cell('DakDashboard::agingSingleUnit', [$office_id, $unit_id, $unit_name])->render('aging_other_desk');
        } catch (\Exception $ex) {
            echo $ex->getMessage();
        }
        die;
    }

    public function officeListCell()
    {
        set_time_limit(0);
        if ($this->request->is('post')) {
            if (!empty($this->request->data['office_id'])) {
                $office_id = $this->request->data['office_id'];
                echo $this->Cell('DakDashboard::OfficeList', [$office_id]);
            }
        }
        die;
    }
    public function officeListForGeo()
    {
        set_time_limit(0);
        if ($this->request->is('post')) {
            $employee_office = $this->getCurrentDakSection();
            $mapped_offices = [];
            $title = '';
            if(!empty($employee_office['office_id'])){
                $office_layers_table = TableRegistry::get('OfficeLayers');
                $origins_2_show_mapped_list = [15,16,17];
                $office_info = TableRegistry::get('Offices')->getAll(
                    ['id' =>$employee_office['office_id']],
                    ['office_origin_id','geo_division_id','geo_district_id','geo_upazila_id']
                )->first();



                if(!empty($office_info) && !empty($office_info['office_origin_id'])){
                    if(in_array($office_info['office_origin_id'],$origins_2_show_mapped_list)){
                        $OfficeIds = $office_layers_table->getLayerwiseOfficeEmployeeCount('Divisional');

                        // need to show division, district, upazila list
                        if($origins_2_show_mapped_list[0] == $office_info['office_origin_id']){
                            // get Offices mapped with that division
                            $mapped_offices = TableRegistry::get('Offices')->getAll(
                                ['geo_division_id' =>$office_info['geo_division_id'],'active_status' => 1,'id IN' => array_keys($OfficeIds)],
                                ['id','office_name_bng']
                            )->toArray();
                            $title = 'বিভাগ স্তরের অফিসসমূহ';
                        }
                        else if($origins_2_show_mapped_list[1] == $office_info['office_origin_id']){
                            $OfficeIds = $office_layers_table->getLayerwiseOfficeEmployeeCount('District');
                            // get Offices mapped with that division
                            $mapped_offices = TableRegistry::get('Offices')->getAll(
                                ['geo_district_id' =>$office_info['geo_district_id'],'active_status' => 1,'id IN' => array_keys($OfficeIds)],
                                ['id','office_name_bng']
                            )->toArray();
                            $title = 'জেলা স্তরের অফিসসমূহ';
                        }
                        else if($origins_2_show_mapped_list[2] == $office_info['office_origin_id']){
                            $OfficeIds = $office_layers_table->getLayerwiseOfficeEmployeeCount('Upazilla');
                            // get Offices mapped with that division
                            $mapped_offices = TableRegistry::get('Offices')->getAll(
                                ['geo_upazila_id' =>$office_info['geo_upazila_id'],'active_status' => 1,'id IN' => array_keys($OfficeIds)],
                                ['id','office_name_bng']
                            )->toArray();
                            $title = 'উপজেলা স্তরের অফিসসমূহ';
                        }
                    }
                }
                echo $this->Cell('DakDashboard::OfficeListForRender', [$mapped_offices,$title])->render('Office_list');
            }
        }
        die;
    }

    public function unitListCell()
    {
        set_time_limit(0);
        if ($this->request->is('post')) {
            if (!empty($this->request->data['office_id'])) {
                $office_id = $this->request->data['office_id'];
                $office_unit_id = $this->request->data['office_unit_id'];
                echo $this->Cell('DakDashboard::unitList', [$office_id, $office_unit_id]);
            }
        }
        die;
    }

    public function absentDetails($office_id = 0, $unit_id = 0, $designtion_id = 0, $time = [],
                                  $order = [])
    {
        set_time_limit(0);
        $this->layout = 'dashboard';
        if ($this->request->is('post')) {
            $this->layout = 'ajax';
            if (!empty($this->request->data['office_id'])) {
                $office_id = $this->request->data['office_id'];
            }
            if (!empty($this->request->data['unit_id'])) {
                $unit_id = $this->request->data['unit_id'];
            }
            if (!empty($this->request->data['designation_id'])) {
                $designtion_id = $this->request->data['designation_id'];
            }
        }
        $tableHeader = '';
        $tableType = '';
        $EmployeeOfficesTable = TableRegistry::get('EmployeeOffices');
        if (!empty($designtion_id)) {
            $designationTable = TableRegistry::get('OfficeUnitOrganograms');
            $EmployeeRecordsTable = TableRegistry::get('EmployeeRecords');
            $employee_info = $EmployeeRecordsTable->getEmployeeInfoByDesignation($designtion_id);
            $designation_info = $designationTable->get($designtion_id);
            $tableHeader = $employee_info['name_bng'] . ", " . $designation_info['designation_bng'];
            $tableType = 'Designation';
        } else {
            if (!empty($unit_id)) {
                $unitTable = TableRegistry::get('OfficeUnits');
                $unit_info = $unitTable->get($unit_id);
                $tableHeader = $unit_info['unit_name_bng'];
                $tableType = 'Unit';
            } else {
                if (!empty($office_id)) {
                    $office_table = TableRegistry::get('Offices');
                    $office_info = $office_table->get($office_id);
                    $tableHeader = $office_info['office_name_bng'];
                    $tableType = 'Office';
                } else {
                    die;
                }
            }
        }
        $today = date('Y-m-d');
        $yesterday = date('Y-m-d', strtotime($today . ' -1 day'));
        $userLoginHistoriesTable = TableRegistry::get('UserLoginHistory');
        $todayLogin = $EmployeeOfficesTable->getAbsentId($office_id, $unit_id,
            $designtion_id, $today);
        $yesterdayLogin = $EmployeeOfficesTable->getAbsentId($office_id, $unit_id,
            $designtion_id, $yesterday);
        $data['today'] = $todayLogin;
        $data['yesterday'] = $yesterdayLogin;

        $this->set(compact('data'));
        $this->set(compact('tableHeader'));
        $this->set(compact('tableType'));
    }

    public function monitorCron()
    {
        set_time_limit(0);
        ini_set('memory_limit', -1);
        $MonitorOfficesTable = TableRegistry::get('MonitorOffices');
        $officesTable = TableRegistry::get('Offices');
        $all_offices = $officesTable->find('list')->where(['active_status' => 1])->toArray();
        $today = date('Y-m-d');
        $yesterday = date('Y-m-d', strtotime($today . ' -1 day'));
        $function = '';
        $data = [
            'partial' => 0,
            'error' => 0,
            'already' => 0,
            'full' => 0,
            'error_ids' => ''
        ];
        foreach ($all_offices as $office_id => $office_val) {
            if (!empty($office_id)) {
                $result = '';
                $function = '';
                $office_record_today = $MonitorOfficesTable->checkUpdate($office_id, $today);
                if ($office_record_today > 0) {
                    $record = $MonitorOfficesTable->getLastUpdateTime($office_id);
                    $cur_time = strtotime(date('Y-m-d H:i:s'));
                    $record_time = strtotime($record['modified']->format("Y-m-d H:i:s"));
                    $time_difference = round(abs($record_time - $cur_time) / 60, 2);
                    if ($time_difference > 60) {
                        $query = $MonitorOfficesTable->deleteDataofOffices($office_id,
                            $today);
                        $office_record_today = 0;
                    } else {
                        $data['already']++;
                    }
                }
                if ($office_record_today < 1) {
                    $office_record_yesterday = $MonitorOfficesTable->checkUpdate($office_id,
                        $yesterday);
                    if ($office_record_yesterday < 1) {
                        $MonitorOfficesTable->deleteDataofOffices($office_id);
                        $result = $this->reportMonitorDashboardSummary($office_id);
                        $function = 'report';
                    } else {
                        $result = $this->updateMonitorDashboardSummary($office_id);
                        $function = 'update';
                    }
                }
                if (!empty($result)) {
                    if (!empty($function) && $function == 'report') {
                        $monitorOfficesEntity = $MonitorOfficesTable->newEntity();
                        $monitorOfficesEntity->office_id = $office_id;
                        $monitorOfficesEntity->yesterdayinbox = $result['totalyesterdayinbox'];
                        $monitorOfficesEntity->yesterdayoutbox = $result['totalyesterdayoutbox'];
                        $monitorOfficesEntity->yesterdaynothijat = $result['totalyesterdaynothijato'];
                        $monitorOfficesEntity->yesterdaynothivukto = $result['totalyesterdaynothivukto'];
                        $monitorOfficesEntity->totalnisponnodak = $result['totalnisponnodakall'];
                        $monitorOfficesEntity->totalonisponnodak = $result['totalOnisponnodakall'];
                        $monitorOfficesEntity->yesterdayselfnote = $result['totalyesterdaysrijitonote'];
                        $monitorOfficesEntity->yesterdaydaksohonote = $result['totalyesterdaydaksohonote'];
                        $monitorOfficesEntity->yesterdaypotrojari = $result['totalyesterdaypotrojari'];
                        $monitorOfficesEntity->totalnisponnonote = $result['totalnisponnonoteall'];
                        $monitorOfficesEntity->totalonisponnonote = $result['totalOnisponnonoteall'];
                        $monitorOfficesEntity->totalnisponno = $result['totalnisponnoall'];
                        $monitorOfficesEntity->totalonisponno = $result['totalOnisponnoall'];
                        $monitorOfficesEntity->status = $result['Status'];
                        $monitorOfficesEntity->updated = date('Y-m-d');

                        if ($MonitorOfficesTable->save($monitorOfficesEntity)) {
                            $data['full']++;
                        }
                    } elseif (!empty($function) && $function == 'update') {
                        $office_record_yesterday = $MonitorOfficesTable->getOfficeData($office_id,
                            [$yesterday, $yesterday])->order(['date(modified) desc'])->first();
                        $monitorOfficesEntity = $MonitorOfficesTable->newEntity();
                        $monitorOfficesEntity->office_id = $office_id;
                        $monitorOfficesEntity->yesterdayinbox = $result['totalyesterdayinbox'];
                        $monitorOfficesEntity->yesterdayoutbox = $result['totalyesterdayoutbox'];
                        $monitorOfficesEntity->yesterdaynothijat = $result['totalyesterdaynothijato'];
                        $monitorOfficesEntity->yesterdaynothivukto = $result['totalyesterdaynothivukto'];
                        $monitorOfficesEntity->totalnisponnodak = $office_record_yesterday['totalnisponnodak']
                            + $result['totaltodaynisponnodak'];
                        $monitorOfficesEntity->totalonisponnodak = $result['totaltodayOnisponnodak'];

                        $monitorOfficesEntity->yesterdayselfnote = $result['totalyesterdaysrijitonote'];
                        $monitorOfficesEntity->yesterdaydaksohonote = $result['totalyesterdaydaksohonote'];
                        $monitorOfficesEntity->yesterdaypotrojari = $result['totalyesterdaypotrojari'];
                        $monitorOfficesEntity->totalnisponnonote = $office_record_yesterday['totalnisponnonote']
                            + $result['totaltodaynisponnonote'];
                        $monitorOfficesEntity->totalonisponnonote = $result['totaltodayOnisponnonote'];
                        $monitorOfficesEntity->totalnisponno = $office_record_yesterday['totalnisponno']
                            + $result['totaltodaynisponno'];
                        $monitorOfficesEntity->totalonisponno = $result['totaltodayOnisponnonote']
                            + $result['totaltodayOnisponnodak'];
                        $monitorOfficesEntity->status = $result['Status'];
                        $monitorOfficesEntity->updated = date('Y-m-d');
                        if ($MonitorOfficesTable->save($monitorOfficesEntity)) {
                            $data['partial']++;
                        }
                    }
                }
            } else {
                $data['error']++;
                $data['error_ids'] .= $office_id;
            }
        }

        pr($data);
        die;
    }

    public function nisponnoCount($office_id = 0, $unit_id = 0, $designation_id = 0, $time = [])
    {
        set_time_limit(0);
        $start_time = strtotime(date('Y-m-d H:i:s'));
        try {
            $this->switchOffice($office_id, 'DashboardOffice');
            $dashboardTable = TableRegistry::get('Dashboard');
            $data = $dashboardTable->nothiDashboardCount($office_id, $unit_id,
                $designation_id, $time);
            $end_time = strtotime(date('Y-m-d H:i:s'));
            print_r($data);
            echo "<br>" . $start_time . " : " . $end_time . " ( " . round(abs($end_time - $start_time), 2) . " )";
            die;

        } catch (\Exception $ex) {

        }
    }

    public function DashboardUpdate($office_id = 0, $unit_id = 0, $designation_id = 0)
    {
        $dashboardTable = TableRegistry::get('Dashboard');
        if (!empty($office_id)) {
            try {
                $this->switchOffice($office_id, 'DashboardOffice');
                if (!empty($office_id) && empty($unit_id) && empty($designation_id)) {
                    $result = $dashboardTable->OfficeDashboardUpdate($office_id);
                } else {
                    $result = $dashboardTable->OtherDashboard($office_id, $unit_id, $designation_id);
                }
                print_r($result);
                die;
            } catch (\Exception $ex) {

            }
        }
    }

    public function nothiUpdate($id = 0, $encode_id = '')
    {

        $user = $this->Auth->user();
        if ($user['user_role_id'] > 2) {
            return $this->redirect(['controller' => 'Users', 'action' => 'login']);
        }
        $edit = 0;
        $table = TableRegistry::get('NothiUpdate');
        if (!empty($id)) {
            if ($id != base64_decode($encode_id)) {
                $this->Flash->error('Not Authorized for this action');
                return $this->redirect(['controller' => 'Dashboard', 'action' => 'login']);
            }
            $edit = 1;
            $data = $table->find()->select(['body', 'version', 'release_date'])->where(['id' => $id])->first();
            $this->set(compact(['data']));
        } else {
            $id = 0;
        }
        $this->set(compact(['id', 'edit']));
        if ($this->request->is('post')) {
            $response = ['status' => 'error', 'msg' => 'something happen'];
            try {
                $id = (empty($this->request->data['id']) ? 0 : $this->request->data['id']);
                $edit = (empty($this->request->data['edit']) ? false : $this->request->data['edit']);
                if (empty($id) && $edit == 0) {
                    $entity = $table->newEntity();
                    $entity->employee_id = isset($user['employee_record_id']) ? $user['employee_record_id'] : 0;
                    $entity->body = $this->request->data['body'];
                    $entity->version = bnToen($this->request->data['version']);
                    $entity->release_date = bnToen($this->request->data['release']);
                    $entity->is_display = 1;
                    if ($table->save($entity)) {
                        $response['status'] = 'success';
                        $response['msg'] = 'Saved Successfully';
                    } else {
                        $response['msg'] = 'can not save';
                    }
                } elseif (!empty ($id) && $edit == true) {
                    $has_data = $table->find()->where(['id' => $id])->first();
                    if (!empty($has_data) && !empty($has_data['id'])) {
                        $has_data->body = $this->request->data['body'];
                        $has_data->version = bnToen($this->request->data['version']);
                        $has_data->release_date = bnToen($this->request->data['release']);
                        if ($table->save($has_data)) {
                            $response['status'] = 'success';
                            $response['msg'] = 'Updated Successfully';
                        } else {
                            $response['msg'] = 'can not update';
                        }
                    }
                }

            } catch (\Exception $ex) {
                $response['msg'] = $ex->getMessage();
            }
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        }
    }

    public function nothiUpdateList()
    {
        $user = $this->Auth->user();
        if ($user['user_role_id'] > 2) {
            return $this->redirect(['controller' => 'Users', 'action' => 'login']);
        }
        $table = TableRegistry::get('NothiUpdate');
        if ($this->request->is('get')) {
            $data = $table->find()->toArray();
            $this->set(compact(['data']));
        } else {
            $data = $table->find()->select(['body', 'version', 'release_date'])->where(['id' => $this->request->data['id']])->first();
            $data['release_date'] = (!empty($data['release_date']) ? Time::parse($data['release_date'])->format('Y-m-d') : '');
            $this->response->type('application/json');
            $this->response->body(json_encode($data));
            return $this->response;
        }

    }

    public function nothiUpdateEdit()
    {
        $user = $this->Auth->user();
        if ($user['user_role_id'] > 2) {
            return $this->redirect(['controller' => 'Users', 'action' => 'login']);
        }
        $table = TableRegistry::get('NothiUpdate');
        if ($this->request->is('GET')) {
            $id = (empty($this->request->query['id']) ? 0 : $this->request->query['id']);
            $encode_id = (empty($this->request->query['en_id']) ? 0 : $this->request->query['en_id']);
            $response = ['status' => 'error', 'msg' => 'something happen'];
            if ($id != base64_decode($encode_id)) {
                $response['msg'] = 'Not Authorized for this action';
            } else {
                $has_data = $table->find()->where(['id' => $id])->first();
                $user['employee_record_id'] = isset($user['employee_record_id']) ? $user['employee_record_id'] : 0;
                if (empty($has_data)) {
                    $response['msg'] = 'No Data record found';
                } else if (!empty($has_data['employee_id']) && $user['employee_record_id'] != $has_data['employee_id']) {
                    $response['msg'] = 'Not Authorized for this Action. You are not owner of this post.';
                } else {
                    $table->deleteAll(['id' => $id]);
                    $response = ['status' => 'success', 'msg' => 'Data Deleted'];
                }
            }

            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        }

        if ($this->request->is('POST')) {
            $id = (empty($this->request->data['id']) ? 0 : $this->request->data['id']);
            $response = ['status' => 'error', 'msg' => 'something happen'];
            $has_data = $table->find()->where(['id' => $id])->first();
            if (empty($has_data)) {
                $response['msg'] = 'No Data record found';
            } else {
//               $table->updateAll(['is_display' => 0],[]);display
                $display = $this->request->data['display'];
                $table->updateAll(['is_display' => $display], ['id' => $id]);
                $response = ['status' => 'success', 'msg' => ' Content will ' . (($display == 0) ? 'not ' : '') . 'display in Release Note Page'];
            }

            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        }
    }

    public function releaseNote()
    {
        $table = TableRegistry::get('NothiUpdate');
        $data = $table->find()->where(['is_display' => 1])->order(['release_date DESC', 'version DESC'])->toArray();
        $this->set(compact(['data']));
        if (empty($this->Auth->User())) {
            $this->layout = 'dashboard';
        }
    }
    public function notices()
    {
        $table_instance_lps = TableRegistry::get('LoginPageSettings');
        $data = $table_instance_lps->find()->where(['type' => 1])->hydrate(true)->order(['id' => 'desc'])->toArray();

        $this->set(compact('data'));
        if (empty($this->Auth->User())) {
            $this->layout = 'dashboard';
        }
    }

    public function faq()
    {
        if (empty($this->Auth->User())) {
            $this->layout = 'dashboard';
        }
        $data = [
            [
                'question' => 'নথি সিস্টেমে সাহায্যের জন্য সরাসরি যোগাযোগ করার কোন উপায় আছে কি?',
                'ans' => 'হ্যাঁ আছে। <a href="https://www.facebook.com/groups/nothi" target="_blank">ফেসবুক গ্রুপ </a> এবং +৮৮ ০১৭১২-৪৮৫৭৪৭,+৮৮ ০২-৯১১৩৭৮১ নম্বরে সরাসরি যোগাযোগ করতে পারেন।',
                'img' => '',
                'type' => 'web'
            ],
            [
                'question' => 'নথি সিস্টেম ব্যবহার সহায়িকা আছে কি?',
                'ans' => 'হ্যাঁ আছে।  <a href="' . $this->request->webroot . '" target="_blank"> লগইন পেইজে ‘ব্যবহার সহায়িকা’ (চিত্রে উল্লেখিত) </a>অপশন থেকে ডাউনলোড করতে পারেন।',
                'img' => '2.png',
                'type' => 'web'
            ],
            [
                'question' => 'নথি সিস্টেম ব্যবহারের জন্য মোবাইল অ্যাপ আছে কি?',
                'ans' => 'হ্যাঁ আছে। অ্যান্ড্রয়েড ফোনের জন্য গুগল প্লে-স্টোর
       (Play Store) থেকে <b>e Nothi System 2.0</b>&nbsp;<a href="https://play.google.com/store/apps/details?id=com.tappware.nothipro" target="_blank">ডাউনলোড করতে পারেন। </a> এবং আইওএস (ios) ফোনের জন্য আপেল-স্টোর (apple store) থেকে &nbsp;<a href="https://itunes.apple.com/us/app/id1187955540" target="_blank">Nothi  নথি মোবাইল অ্যাপস ডাউনলোড করে </a>  ব্যবহার করতে পারেন । ',
                'img' => '',
                'type' => 'web'
            ],
            [
                'question' => 'নথি সিস্টেম ব্যবহারের জন্য কোন নির্দিষ্ট ব্রাউজারের পরামর্শ আছে কি?',
                'ans' => 'হ্যাঁ, নথি সিস্টেমে নির্বিঘ্নে কাজ করার জন্য Google Chrome ব্রাউজার ব্যবহার  করবেন।Google Chrome ইন্সটল না থাকলে <a target="_blank" href="https://www.google.com/chrome/"> এখানে ক্লিক করুন </a>।',
                'img' => '',
                'type' => 'web'
            ],
            [
                'question' => 'ইউজার আইডি ও ইজারনেমের মধ্যে পার্থক্য কি?',
                'ans' => '<br><li>ইউজার আইডি:নথি সিস্টেম থেকে প্রাপ্ত সংখ্যাবাচক ১২ ডিজিটের আইডি হচ্ছে ইউজার আইডি।</li> <li>প্রোফাইল ব্যবস্থাপনা থেকে আপনি আপনার নামের মিল রেখে যে ইউনিক নেম বাছাই করবেন, সেটাই ইউজার নেম।</li>',
                'img' => '5.png',
                'type' => 'web'
            ],
            [
                'question' => 'লগইন করার সময় ইউজার আইডি ও পাসওয়ার্ড সঠিক দেওয়ার পর কোন ইরোর মেসেজ না দিলে করণীয় কি?',
                'ans' => 'এক্ষেত্রে আপনাকে ব্রাউজারের  ক্যাশ ক্লিয়ার করতে হবে। ক্যাশ ক্লিয়ার জন্য ব্রাউজার (গুগল ক্রম) ওপেন করে <code>Ctrl + Shift + Delete</code> চাপুন। এরপর নিচের চিত্রের মত উইন্ডো ওপেন হবে। এখন ধাপ ১, ২, ও ৩ সম্পন্ন করুন।<br>',
                'img' => '6.png',
                'type' => 'web'
            ],
            [
                'question' => 'পাসওয়ার্ড ভূলে গেলে করণীয় কি?',
                'ans' => '<a href="' . $this->request->webroot . '" target="_blank"> লগইন পেইজে </a>  <b>"পাসওয়ার্ড ভুলে গেছেন?"</b> অপশনে ক্লিক করে আপনার ইউজার আইডি ও ই-মেইল (প্রোফাইলে যে ই-মেইল দেয়া) এড্রেস দিয়ে ‘অনুরোধ করুন’ বাটনে ক্লিক করলে আপনার ই-মেইল একটা লিংক পাঠানো হবে, ঐ লিংকে ক্লিক করে আপনি পাসওয়ার্ড পরিবর্তন করতে পারবেন।',
                'img' => '7.png',
                'type' => 'web'
            ],
            [
                'question' => 'নথি ব্যবহারকারীর ছবি ও স্বাক্ষরের ক্ষেত্রে কোন নির্দিষ্ট সাইজ আছে কি?',
                'ans' => 'হ্যাঁ আছে। ছবির সাইজ হবে 300px width×300px height এবং স্বাক্ষরের সাইজ হবে 150px width×50px height.',
                'img' => '',
                'type' => 'web'
            ],
            [
                'question' => 'দাপ্তরিক ডাক ও নাগরিক ডাক কি?',
                'ans' => '<br><li>দাপ্তরিক ডাক: কোন অফিস/দপ্তর থেকে যে পত্র আপনার অফিসে আসে সাধারণত সেগুলো দাপ্তরিক ডাক।</li><li> নাগরিক ডাক: কোন নাগরিক যখন সরাসরি আপনার অফিসে আবেদন/অভিযোগ করে সেগুলো নাগরিক আবেদন।</li>',
                'img' => '',
                'type' => 'web'
            ],
            [
                'question' => 'নথি সিস্টেমে একজন নাগরিক কোন অফিস কিভাবে আবেদন করতে পারবে?',
                'ans' => ' লগইন পেইজে  <a target="_blank" href="' . $this->request->webroot . 'dak_nagoriks/NagorikAbedon">নাগরিক কর্ণার </a> অপশন থেকে একজন নাগরিক নথি সিস্টেমে যে কোন অফিসে আবেদন করতে পারবে।',
                'img' => '10.png',
                'type' => 'web'
            ],
            [
                'question' => 'ডাক আপলোড কখন করতে হবে?',
                'ans' => 'আপনার অফিসে কোন পত্রের হার্ড কপি আসলে সেই পত্রটি স্ক্যান করে আপলোড করতে হবে।',
                'img' => '',
                'type' => 'web'
            ],
            [
                'question' => 'দাপ্তরিক ডাক আপলোডের ক্ষেত্রে প্রেরক কে হবে?',
                'ans' => 'পত্রের প্রেরকই (যার স্বাক্ষরিত) ডাক আপলোডের ক্ষেত্রে প্রেরক হবে।
',
                'img' => '',
                'type' => 'web'
            ],
            [
                'question' => 'সিল কি?',
                'ans' => 'ডাক প্রেরণের জন্য প্রাপকগণের নাম ও পদবিসহ যে লিস্ট তৈরি করা হয়, সেটাই মূলত সিল হিসেবে পরিচিত।',
                'img' => '',
                'type' => 'web'
            ],
            [
                'question' => 'সিল তৈরির ক্ষেত্রে অস্থায়ী সিল কি?',
                'ans' => 'যে সিল তৈরি করে একবার ব্যবহার করার পর তালিকা থেকে স্বয়ংক্রিয়ভাবে মুছে যায় সেটাই অস্থায়ী সিল।',
                'img' => '',
                'type' => 'web'
            ],
            [
                'question' => 'মূল প্রাপক ও প্রাপকগণ কি?',
                'ans' => 'মূল প্রাপক ডাকের সকল কার্যক্রম গ্রহণ করতে পারবেন এবং প্রাপক/প্রাপকগণ শুধু ডাক দেখতে ও আর্কাইভ করতে পারবেন।',
                'img' => '',
                'type' => 'web'
            ],
            [
                'question' => 'ডাক আপলোডের পর প্রেরিত ডাকের তালিকা কোথায় দেখতে পাব?',
                'ans' => 'দাপ্তরিক/নাগরিক ডাক -> খসড়া ডাক তালিকায় ‘প্রেরিত ডাক’ ট্যাবে ক্লিক করলে ডাক আপলোডের পর প্রেরিত ডাকের তালিকা দেখা যাবে।',
                'img' => '',
                'type' => 'web'
            ],
            [
                'question' => 'ডাক প্রেরণের পর ফেরত আনার সুযোগ আছে কি?',
                'ans' => 'হ্যাঁ আছে। ডাক প্রেরণের পর প্রেরিত ডাক থেকে সংশ্লিষ্ট ডাকের বিস্তারিত বাটনে ক্লিক করে ডাক ফেরত বাটনে ক্লিক করলে ডাক ফেরত আসবে। ডাক প্রেরণের পর প্রাপক যদি ডাকে বিস্তারিত ভিউ করে তাহলে ফেরত আনা যাবে না।',
                'img' => '16.png',
                'type' => 'web'
            ],
            [
                'question' => 'ডাক ভূল নথিতে উপস্থাপন করলে ফেরত আনার সুযোগ আছে কি?',
                'ans' => 'হ্যাঁ আছে। ডাক নথিতে উপস্থাপনের পর ফেরত আনার জন্য ডাক মেনু -> ‘অন্যান্য’ ট্যাব থেকে ‘নথিতে উপস্থাপিত ডাক’ লিষ্ট থেকে সংশ্লিষ্ট ডাকের বিস্তারিত বাটনে ক্লিক করে ডাক ফেরত বাটনে ক্লিক করলে ডাক ফেরত আসবে। ডাক উপস্থাপনের পর নোট লিখে প্রেরণ করার আগ পর্যন্ত ফেরত আনা যাবে।',
                'img' => '16.png',
                'type' => 'web'
            ],
            [
                'question' => 'কখন ডাক নথিজাত করা যাবে?',
                'ans' => 'শুধুমাত্র পুর্ববর্তী সিন্ধান্ত ‘নথিজাত করুন’ থাকলেই ডাক নথিজাত করা যাবে।',
                'img' => '',
                'type' => 'web'
            ],
            [
                'question' => 'নথি কিভাবে তৈরি করা যাবে?',
                'ans' => 'নথি মেনু -> নথি ব্যবস্থাপনা -> নতুন নথি তৈরি থেকে নথির ধরন, বিষয়, নথির শ্রেণি এবং নথি অনুমতি (টিক চিহ্ন দিয়ে) সমূহ পুরণ করে সংরক্ষণ করলে নতুন নথি তৈরি হবে।',
                'img' => '',
                'type' => 'web'
            ],
            [
                'question' => 'নথির তৈরি সময় ১৮ ডিজিট কোড স্বয়ংক্রিয়ভাবে না আসলে করণীয়?',
                'ans' => 'অফিস এডমিন থেকে ৮ ডিজিটের অফিস কোড ও ৩ ডিজিটের শাখা কোড সংশোধন করে দিতে হবে।',
                'img' => '',
                'type' => 'web'
            ],
            [
                'question' => 'নতুন করে কাউকে নথিতে অনুমতি দিব কিভাবে?',
                'ans' => 'নথি তালিকা থেকে বিস্তারিত বাটনে ক্লিক করে উপরে ‘অনুমতি সংশোধন’ বাটনে ক্লিক করে সংশ্লিষ্ঠ পদবির পাশে টিক চিহ্ন দিয়ে সংরক্ষণ করলে অনুমতি সংশোধন হবে।',
                'img' => '',
                'type' => 'web'
            ],
            [
                'question' => 'নথিতে অনুমতির ক্ষেত্রে ক্রম কি?',
                'ans' => 'ক্রম সিনিয়রিটি নির্ধারণ করে। এখানে সিনিয়রকে ১ থেকে ক্রমান্বয়ে ২, ৩, ৪, ........ দিয়ে সংরক্ষণ করতে হবে।',
                'img' => '',
                'type' => 'web'
            ],
            [
                'question' => 'নথি নিষ্পন্ন করবো কিভাবে?',
                'ans' => 'নথি দুই ভাবে নিষ্পন্ন হবে।<br> ১। পত্রজারি থাকলে পত্রজারি করলে স্বয়ংক্রিয়ভাবে নথি নিষ্পন্ন হবে। ২। স্ব-উদ্যোগে নথি উপস্থাপনের ক্ষেত্রে যে নথি উপস্থাপন করবে তার নিকট নথি অনুমোদিত হয়ে আসলে ‘নোট নিষ্পন্ন’ বাটন পাবেন। তখন নোট নিষ্পন্ন বাটনে ক্লিক করে নোট নিষ্পন্ন করতে হবে।',
                'img' => '',
                'type' => 'web'
            ],
            [
                'question' => 'নোট নিষ্পন্ন বাটন কখন দেখায়?',
                'ans' => 'যে নথি উপস্থাপন করবে তার নিকট নথি অনুমোদিত হয়ে আসলে ‘নোট নিষ্পন্ন’ বাটন পাবেন।',
                'img' => '',
                'type' => 'web'
            ],
            [
                'question' => 'পত্রজারি হয়েছে কিনা কিভাবে নিশ্চিত হবো?',
                'ans' => 'পত্রের উপর ‘পত্রজারি প্রেরণ অবস্থা’ বাটনে ক্লিক করে নিশ্চিত হওয়া যাবে।',
                'img' => '22.png',
                'type' => 'web'
            ],
            [
                'question' => 'গার্ড ফাইল কি?',
                'ans' => 'যে ফাইলে বরাতসুত্র (রেফারেন্স) হিসাবে বিভিন্ন আইন , সার্কুলার , বিধি, ম্যানুয়াল নথিতে ব্যবহারের জন্য সংরক্ষণ করা হয়।',
                'img' => '',
                'type' => 'web'
            ],
            [
                'question' => 'অফিস এডমিন কিভাবে পরিবর্তন করা যায়?',
                'ans' => 'অফিস এডমিন পরিবর্তন করতে হলে ‘হেল্প ডেস্ক’-এ যোগাযোগের মাধ্যমে পরিবর্তন করতে হবে।',
                'img' => '',
                'type' => 'web'
            ],
            [
                'question' => 'ডাক ও নথির বিষয়ে মোবাইল বা ই-মেইলে নোটিফিকেশন পাওয়ার কোন সুযোগ আছে কি?',
                'ans' => 'হ্যাঁ আছে। প্রোফাইল ব্যবস্থাপনা থেকে সেটিং  অপশন থেকে প্রয়োজনীয় নোটিফিকেশনের জন্য সেটিং পরিবর্তন করে নোটিফিকেশন পেতে পারেন।',
                'img' => '',
                'type' => 'web'
            ],
            [
                'question' => 'মূল প্রাপক হিসেবে একটি ডাক আমার কাছে এসেছে। ডাকটি আর্কাইভ করা যায় না। কিভাবে করা যাবে?',
                'ans' => 'মূল প্রাপক হিসেবে আগত ডাক আর্কাইভ করা যায় না। অনুলিপি হিসেবে আগত ডাক আর্কাইভ করতে পারবেন। ',
                'img' => '',
                'type' => 'web'
            ],
            [
                'question' => 'একটি ডাক নথিতে উপস্থাপন করার সময় নথির ধরন দিতে হয় কিন্তু অপশনে ধরন লিখে দিলে তা থাকেনা এবং সংরক্ষণ করা যায় না।',
                'ans' => 'এখানে ধরন লেখার সুযোগ নেই। নথি ব্যবস্থাপনার নতুন ধরন তৈরি করার অপশন থেকে ধরন তৈরি করার পর এখান থেকে সেই ধরন নির্বাচন করা যাবে। ',
                'img' => '',
                'type' => 'web'
            ],
            [
                'question' => 'নথিজাত করা ডাকটি আমি ফেরত এনে নথিতে উপস্থাপন করতে পারব কিনা?',
                'ans' => 'হ্যাঁ। ডাক মেনু > অন্যান্য ডাক> নথিতজাতকৃত ডাক এ ক্লিক করে যে ডাকটি ফেরত আনতে চান সেই ডাকের বিস্তারিত বাটনে ক্লিক করুন। এরপর উপরে ডান দিকে দেখতে পাবেন ডাক ফেরত আনার বাটন আছে। এবার আগত ডাকে ঐ ডাকটি দেখতে পাবেন। তারপর নথিতে উপস্থাপন করুন। ',
                'img' => '',
                'type' => 'web'
            ],
            [
                'question' => 'নথির নাম (বিষয়) পরিবর্তন করা যাবে কি? যদি করা যায়, তাহলে কিভাবে?',
                'ans' => 'করা যাবে। এডমিন আইডি থেকে দপ্তর নথি সেটিং নথির নাম সংশোধন থেকে নথির নাম (বিষয়) পরিবর্তন করা যাবে।  ',
                'img' => '',
                'type' => 'web'
            ],
            [
                'question' => 'আমার তৈরিকৃত নথির ধরন আমার বিভাগের সবাই বা প্রতিষ্ঠানের সবাই দেখতে পাবে কিনা?',
                'ans' => 'নথির ধরন কেবল মাত্র সংশ্লিষ্ট শাখার সবাই দেখতে পাবে, অন্যান্য শাখার কেউ দেখতে পাবে না। ',
                'img' => '',
                'type' => 'web'
            ],
            [
                'question' => 'আমার তৈরিকৃত পত্রজারি গ্রুপের সকল বিভাগ বা প্রতিষ্ঠানের সবাই দেখতে পারবে কিনা?',
                'ans' => 'পত্রজারি গ্রুপের সকলে পত্র ড্রাফট করার সময় দেখতে পাবে। ',
                'img' => '',
                'type' => 'web'
            ],
            [
                'question' => 'পত্রাংশের হেডিং এ উপজেলা নির্বাহী অফিসারের কার্যালয় দুবার আসছে কেন? ',
                'ans' => 'পত্রজারি হেডিং অফিস এডমিন থেকে যেভাবে সেট করবে সেই ভাবে দেখা যাবে। পরিবর্তন করতে চাইলে এডমিন আইডি থেকে দপ্তর নথি সেটিং পত্রজারি হেডিং সংশোধন অপশন থেকে হেডিং ঠিক করে দেয়া যাবে।  ',
                'img' => '',
                'type' => 'web'
            ],
            [
                'question' => 'প্রাপকের স্থানে প্রাপকের কার্যালয়, যেমনঃ “জেলা প্রশাসকের কার্যালয়” দুবার আসছে কেন?',
                'ans' => 'অফিস প্রধান হিসাবে জেলা প্রশাসককে নির্বাচন করা নেই। ',
                'img' => '',
                'type' => 'web'
            ],
            [
                'question' => 'প্রেরকের স্বাক্ষরের নিচে অফিসের নাম আসছে না কেন?',
                'ans' => 'প্রেরকের স্বাক্ষরের নিচে নাম এবং পদবি আসবে। অফিসের নাম আসবে না। ',
                'img' => '',
                'type' => 'web'
            ],
            [
                'question' => 'পত্রজারি করার পর পত্রে অনুমোদনকারী উপজেলা নির্বাহী অফিসার এর নিচে অবশিষ্ট অংশ যেমন:- মেলান্দহ, জামালপুর, ইমেইল, মোবাইল, ফ্যাক্স নম্বর ইত্যাদি আসে না কেন?',
                'ans' => 'শাখার তথ্য সংশোধন করে ঐ ফিল্ড গুলো পূরণ করলেই তথ্য আসবে।  ',
                'img' => '',
                'type' => 'web'
            ],
            [
                'question' => 'একটি পত্রের অনুমোদনকারী ও প্রেরক একই ব্যক্তি হলে তিনি পত্রের খসড়া অনুমোদন করে ভুলবশত জারি না করে নথি ডাউন করে দিয়েছে কিন্তু দেখা যাচ্ছে যাকে নথি ডাউন করা হয়েছে তিনিও পত্রজারির অপশন পাচ্ছেন? ',
                'ans' => 'পত্রের অনুমোদনকারী ও প্রেরক একই ব্যক্তি হলে তিনি পত্র অনুমোদনের পর পত্রজারি না করে পত্র যাকে প্রেরণ করবেন তিনিও পত্রজারি বাটন পাবেন।  ',
                'img' => '',
                'type' => 'web'
            ],
            [
                'question' => 'নথি লাইভ সার্ভার আইডিতে লগ-ইন করলে শুধু আইডি দেখা যাচ্ছে নাম ও পদবি দেখা যাচ্ছে না কেন? ',
                'ans' => 'এই আইডি কোন পদবিতে এ্যাসাইন করা নেই। এই জন্য নাম পদবি দেখাচ্ছে না।  ',
                'img' => '',
                'type' => 'web'
            ],
            [
                'question' => 'খসড়া পত্র প্রস্তুত করে Edit/সম্পাদন করে সংরক্ষণ করেছিলাম কিন্তু সম্পাদনা অংশে পূর্বের Save করা লেখা গুলো আর দেখা বা পাওয়া যাচ্ছে না।',
                'ans' => 'ইন্টারনেট স্পিড খুবই স্লো থাকলে সাধারণত এই সমস্যা হয়। এই সমস্যার ক্ষেত্রে পত্র লেখার পর, উপরের সংরক্ষণ বাটনে ক্লিক করে কিছুক্ষণ অপেক্ষা করতে। যখন সংরক্ষণ হবে, তখন নিচে সংরক্ষণ বাটন ক্লিক করতে হবে। ',
                'img' => '',
                'type' => 'web'
            ],
            [
                'question' => 'যদিও নথি অনুমতি প্রাপ্ত কর্মকর্তার তালিকায় ক্রম সঠিক আছে কিন্তু নথির নোটাংশে স্বাক্ষরের পদবি ক্রম সঠিক আসে না।',
                'ans' => 'অফিস এডমিন থেকে পদবির স্তর এবং ক্রম ঠিক করা থাকলে নোটাংশে স্বাক্ষরের ক্ষেত্রেও ক্রম ঠিক আসবে। ',
                'img' => '',
                'type' => 'web'
            ],
            [
                'question' => 'পত্রটি ই-ফাইলিং এ করা কিন্তু আমরা ই-ফাইলে পাইনি অথচ হার্ডকপি পেয়েছি। ',
                'ans' => 'প্রাপক নির্বাচনের সময় নতুন অফিসার অপশন থেকে প্রাপকের নাম, অফিসের নাম ইত্যাদি ব্যবহার করা হয়েছে। সিস্টেম থেকে অফিসার নির্বাচন (মন্ত্রণালয়, অধিদপ্তর ......) করা হয়নি। তাই নথি সিস্টেমে পত্র যায়নি।',
                'img' => '',
                'type' => 'web'
            ],
            [
                'question' => 'নতুন নোটে ক্লিক করলে ম্যাসেজ আসছে পূর্বে অনুচ্ছেদ দেয়া হয়নি। উল্লেখ্য পূর্বের অনুচ্ছেদ সংরক্ষণ করা সম্ভব হয়নি। ',
                'ans' => 'সংশ্লিষ্ট নথির সকল নোটে ক্লিক করলে বাম পাশে ঐ নথির সকল নোট দেখতে পাবেন। সেখানে যে নোট লাল রঙের হয়ে আছে সেই নোটে ক্লিক করে কাজ করুন।',
                'img' => '',
                'type' => 'web'
            ],
            [
                'question' => 'অনিষ্পন্ন কার্যক্রম বিবরণীতে কোন নথি অনিষ্পন্ন নেই। অথচ নথি ট্যাবে দেখা যাচ্ছে একটি নথি অনিষ্পন্ন। যেটি খুঁজে পাওয়া যাচ্ছে না।  ',
                'ans' => 'সংশ্লিষ্ট নথির সকল নোটে ক্লিক করলে বাম পাশে ঐ নথির সকল নোট দেখতে পাবেন। সেখানে যে নোট লাল রঙের হয়ে আছে সেই নোটে ক্লিক করে কাজ করুন।',
                'img' => '',
                'type' => 'web'
            ],
            [
                'question' => 'পত্র জারি হয়েছে। পত্র জারির পর নথি শাখায় ফেরত পাঠানো হয়েছে। কিন্তু নথি নিষ্পন্ন বাটন পাওয়া যাচ্ছে না। ফলে নথি নিষ্পন্ন করা যাচ্ছে না। ',
                'ans' => 'যিনি নোট উপস্থাপন করেছেন তিনি নোট নিষ্পন্ন বাটন পাবেন। নথিটি নিষ্পন্নের জন্য তার কাছে প্রেরণ করুন। তিনি পত্রজারি বাটন পাবেন।',
                'img' => '',
                'type' => 'web'
            ],
            [
                'question' => 'পদবি থেকে বদলির কারণে রিলিজ করা হয়েছে। কিন্থু ঐ পদবিতে ডাক/ নথি আছে কিভাবে নিষ্পন্ন করা যাবে ? ',
                'ans' => 'ঐ পদবিতে অন্য ইউজার এ্যাসাইন করলে তার কাছে ডাক/ নথি অনিষ্পন্ন অবস্থায় দেখাবে এবং তিনি সেগুলো নিষ্পন্ন করতে পারবেন। ',
                'img' => '',
                'type' => 'web'
            ],
            [
                'question' => 'ড্যাশবোর্ড থেকে এক সপ্তাহের (অথবা নির্দিষ্ট কয়েক দিনের) কার্যক্রমের পরিসংখ্যান কীভাবে বের করা যায়? ',
                'ans' => 'ড্যাশবোর্ড থেকে আপনি অফিসের কার্যক্রমের পরিসংখ্যান বের করতে পারবেন না। আপনি অফিস এডমিন আইডি থেকে করতে পারবেন।।',
                'img' => '',
                'type' => 'web'
            ],
            [
                'question' => 'নথি প্রেরণ বাটনে ক্লিক করলে নাম পাওয়া যায় না কেন?  ',
                'ans' => 'প্রেরণ বাটনের পাশেই অনুমতি সংশোধন বাটন আছে। অনুমতি সংশোধন বাটনে ক্লিক করে প্রথমে নথিতে সংশ্লিষ্ট কর্মকর্তাকে অনুমতি দিন। এরপর প্রেরণ তালিকা রিফ্রেশ বাটনে ক্লিক করে অনুমতিপ্রাপ্ত নতুন কর্মকর্তাকে প্রেরণ তালিকায় নিয়ে আসা যাবে।।',
                'img' => '',
                'type' => 'web'
            ],
            [
                'question' => 'পত্রজারিতে প্রিন্ট প্রিভিউ আসছে না, পত্র প্রিন্ট দেয়া এবং ডাউনলোড করা যাচ্ছে না।  ',
                'ans' => 'প্রিন্ট প্রিভিউতে ক্লিক করলে আসবে। ',
                'img' => '',
                'type' => 'web'
            ],
            [
                'question' => 'নতুন নথি তৈরি করার সময় নথির শ্রেণিতে ক, খ, গ, ঘ বলতে কী বুঝানো হয়েছে?   ',
                'ans' => 'সচিবালয় নির্দেশমালা ২০১৪ অনুযায়ী কোন ধরনের নথি কতদিন সংরক্ষণ করা হবে তা বোঝানোর জন্য নথির শ্রেণি ব্যবহার করা হয়। ',
                'img' => '',
                'type' => 'web'
            ],
            [
                'question' => 'জারিকৃত পত্রের ক্লোন করা যাচ্ছে, এটা আবার কী? কেউ একটু খোলাসা করবেন?   ',
                'ans' => 'ক্লোন অপশনের মাধ্যমে আপনার অফিসের পূর্বের জারিকৃত পত্র খসড়া পত্র হিসাবে নিতে পারবেন। পরবর্তীতে সেখানে প্রয়োজনীয় সম্পাদনা করে জারি করা যাবে। ',
                'img' => '',
                'type' => 'web'
            ],
            [
                'question' => 'জারিকৃত পত্রের তারিখ ও সাল পূর্বের দেখাচ্ছে কেন?   ',
                'ans' => 'যে কম্পিউটার থেকে কাজ করছেন ঐ কম্পিউটারের উইন্ডোজ তারিখ ও সাল সঠিক নেই। ',
                'img' => '',
                'type' => 'web'
            ],
            [
                'question' => 'প্রেরিত নথি ফেরত আনা যায় কি?   ',
                'ans' => 'হ্যাঁ। কোন নথি প্রেরণ করার পর না দেখা পর্যন্ত ঐ দিন রাত ১২.০০ টা পর্যন্ত ফেরত আনা যাবে। এই ক্ষেত্রে নথি প্রেরণ করার পর প্রেরিত নথিতে গিয়ে “ নথি ফেরত আনুন” বাটনে ক্লিক করলে নথিটি ফেরত আসবে এবং এডিট করা যাবে। ',
                'img' => '',
                'type' => 'web'
            ],
            [
                'question' => 'একটি ফাইল এ্যাটাচমেন্ট করার সময় ভুলে অন্য ফাইল এ্যাটাচ হয়ে গেলে প্রেরণ করার পর তা এডিট করার আর কোন সুযোগ আছে? অথবা প্রেরণ করার পর তা মুছে দেয়ার কি কোন অপশন আছে?   ',
                'ans' => 'নেই। কিন্তু আপনি প্রেরণ করা আগে সংরক্ষণ করে রাখলে পরিবর্তন করতে পারবেন। ',
                'img' => '',
                'type' => 'web'
            ],
            [
                'question' => 'নথি নিষ্পন্ন করার পর তা কোথায় আছে তা জানার পদ্ধতি কি? অর্থাৎ ফাইল নিষ্পন্নকারী ছাড়া অন্য অনুমতিপ্রাপ্ত ব্যক্তি দেখার উপায় কি? ',
                'ans' => 'সকল নথির তালিকায় আপনার অনুমতি প্রাপ্ত সকল নথি পাওয়া যাবে।',
                'img' => '',
                'type' => 'web'
            ],
            [
                'question' => 'একই নোটে একাধিক অনুচ্ছেদের বেলায়/ক্ষেত্রে পত্রের খসড়া ক্লোন করা যায়, তবে নতুন নোট নিয়ে তাতে পত্রের খসড়া ক্লোন করা যায় না, এ বিষয়ে কোন সমাধান আছে কিনা? ',
                'ans' => 'নতুন নোট দেয়ার পর পত্রাংশে সকল পত্র বাটনে ক্লিক করলে ক্লোন করার অপশন পাবেন।',
                'img' => '',
                'type' => 'web'
            ],
            [
                'question' => 'নথি সিস্টেমে ব্যবহারের জন্য কি কোন এ্যাপ আছে?',
                'ans' => 'হাঁ আছে ।  এন্ড্রোয়েড ফোনের জন্য গুগল প্লে- স্টোর (Play Store) থেকে e-nothi system 2 ডাউনলোড করতে পারেন। আইওএস (ios) ফোনের জন্য এ্যাপেল- স্টোর (apple store) থেকে ইংরেজী নাম Nothi লিখে সার্চ করে ডাউনলোড করতে পারেন। ',
                'img' => '',
                'type' => 'mbl'
            ],
            [
                'question' => 'এ্যাপে ট্রেনিং বা লাইভ দুই সার্ভারের আইডি ও পাসওয়ার্ড ব্যবহার করা যাবে কি ?',
                'ans' => 'না, এ্যাপে শুধু মাত্র লাইভ সার্ভারের আইডি ও পাসওয়ার্ড ব্যবহার করা যাবে। ',
                'img' => '',
                'type' => 'mbl'
            ],
            [
                'question' => 'কোন কোন মোবাইল অপারেটিং সিস্টেমের জন্য নথি এ্যাপ রয়েছে? ',
                'ans' => 'এন্ড্রোয়েড ও আইওএস',
                'img' => '',
                'type' => 'mbl'
            ],
            [
                'question' => 'এ্যাপের কি কোন ব্যবহার সহায়িকা আছে ?',
                'ans' => 'আছে । এ্যাপের লগইন পেজে ও নেভিগেশন মেন্যুর ‘হেল্পডেস্ক’ এ পাওয়া যাবে। ',
                'img' => '',
                'type' => 'mbl'
            ],
            [
                'question' => 'এ্যাপের কোন ভিডিও টিউটোরিয়াল আছে কিনা?',
                'ans' => 'আছে। এ্যাপের লগইন পেজে ও নেভিগেশন মেন্যুর ‘হেল্পডেস্ক’ এ পাওয়া যাবে। ',
                'img' => '',
                'type' => 'mbl'
            ],
            [
                'question' => ' মোবাইল এ্যাপ দিয়ে কি এক বা একাধিক ডাক সিলেক্ট করা যায়?',
                'ans' => 'হাঁ । ইউজারের আগত লিস্ট থেকে চেক বক্স টাচ করে সিলেক্ট করতে পারবেন। ',
                'img' => '',
                'type' => 'mbl'
            ],
            [
                'question' => 'এ্যাপ দিয়ে ড্যাশ বোর্ড দেখা যায় কি?',
                'ans' => 'হোম পেজের ডান দিকে ড্যাশ বোর্ড আইকন থেকে দেখা যায়। ',
                'img' => '65.png',
                'type' => 'mbl'
            ],
            [
                'question' => 'এ্যাপ দিয়ে ডাক নথিজাত করা যায় কিনা?',
                'ans' => 'হাঁ । ডাকের বিস্তারিত ভিউ থেকে নথিজাত আইকনে ক্লিক করে নথিজাত করা যায়।',
                'img' => '66.png',
                'type' => 'mbl'
            ],
            [
                'question' => 'এ্যাপ দিয়ে কি ডাক আর্কাইভ করা যায়?',
                'ans' => 'হাঁ। আর্কাইভ বাটনে ক্লিক করলে আর্কাইভ হয়ে যাবে। ',
                'img' => '67.png',
                'type' => 'mbl'
            ],
            [
                'question' => 'এ্যাপ দিয়ে কি ডাকের সিন্ধান্ত দেয়া যায়?',
                'ans' => 'হাঁ। ডাক সিলেক্ট করার পর প্রেরণ বাটনে ক্লিক করে সিদ্ধান্ত নির্বাচন/ লিখে দিতে হবে। ',
                'img' => '',
                'type' => 'mbl'
            ],
            [
                'question' => 'এ্যাপের মাধ্যমে নোট অনুমোদন ও কার্যক্রম করা যায় কি?',
                'ans' => 'হাঁ, অনুমোদন ও কার্যক্রম করা যাবে । ',
                'img' => '',
                'type' => 'mbl'
            ],
            [
                'question' => 'এ্যাপ থেকে পত্রের খসড়া সংশোধন করা যায় কি?',
                'ans' => 'হাঁ, খসড়া পত্রের উপর থেকে “ খসড়া পত্র দেখুন” বাটনে টাচ করে খসড়া সংশোধন করা যাবে।',
                'img' => '70.png',
                'type' => 'mbl'
            ],
            [
                'question' => 'এ্যাপ থেকে পত্রজারি করা যায় কি?',
                'ans' => 'হাঁ, পত্রজারি করা যায়। ',
                'img' => '',
                'type' => 'mbl'
            ],
            [
                'question' => 'এ্যাপ থেকে ডাক নথিতে উপস্থাপন করা যায় কি?',
                'ans' => 'না, নথিতে উপস্থাপন করা যাবে না। ',
                'img' => '',
                'type' => 'mbl'
            ],
            [
                'question' => 'এ্যাপ থেকে ডাক/ নথি ফেরত আনা যায় কি?',
                'ans' => 'না, ডাক বা নথি ফেরত আনা যাবে না।  ',
                'img' => '',
                'type' => 'mbl'
            ],
            [
                'question' => 'এ্যাপ থেকে কি সকল নথির তালিকা দেখা যায় কি?',
                'ans' => 'না, সকল নথি তালিকা দেখা যাবে না।  ',
                'img' => '',
                'type' => 'mbl'
            ],
        ];
        $query = $this->request->query('type');
        $this->set(compact('data', 'query'));
    }

    public function OwnOfficeDB()
    {
        $employee_office = $this->getCurrentDakSection();
        if (empty($employee_office['office_id'])) {
            return $this->redirect(['controller' => 'users', 'action' => 'login']);
        }
        $this->set('office_id', $employee_office['office_id']);
        $this->render('office_dashboard');
    }

    public function sslInstruction()
    {
        $user = $this->Auth->User('id');
        if (empty($user)) {
            $this->layout = 'dashboard';
        }
    }

    public function speedTest()
    {
        $time = 0;
        $ch = curl_init('http://nothi.gov.bd/users/sslLogin');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_exec($ch);
        if (!curl_errno($ch)) {
            $info = curl_getinfo($ch);
            $time = ceil($info['total_time']);
        }

        $this->response->body($time);
        $this->response->type('json');
        return $this->response;
    }

    public function addUserManual() {
		$userManualTable = TableRegistry::get('UserManuals');
    	$list = $userManualTable->find()->toArray();
    	$this->set(compact('list'));
    	if ($this->request->is('post')) {
			$userManualTable = TableRegistry::get('UserManuals');
			$userManual = $userManualTable->newEntity();
			$userManual->title = $this->request->data()['title'];
			$userManual->description = $this->request->data()['description'];
			$userManual->module = $this->request->data()['module'];
			$tags = str_replace(',', '|', $this->request->data()['tags']);
			$userManual->tags = $tags;
			if ($userManualTable->save($userManual)) {
				$this->Flash->success('সফলভাবে সংরক্ষণ করা হয়েছে');
				return $this->redirect(['action' =>'addUserManual']);
			} else {
				$this->Flash->error('সংরক্ষণ করা হচ্ছে না, পুনরায় চেষ্টা করুন');
				return $this->redirect(['action' =>'addUserManual']);
			}
		}
	}
	public function userManualDelete() {
		$userManualTable = TableRegistry::get('UserManuals');
		$userManualTable->deleteAll(['id' => $this->request->data('id')]);
		$this->Flash->success('মুছে ফেলা হয়েছে');
		return $this->redirect(['action' =>'addUserManual']);
	}
	public function text_image_upload() {
    	$file = $this->request->data()['file'];
    	$folder = 'UserManual/text_image/'.date('Y-m-d').'/';
    	if (!file_exists(FILE_FOLDER_DIR.$folder)) {
    		mkdir(FILE_FOLDER_DIR.$folder, 0755);
		}
    	$new_file_name = $folder.time().'.'.pathinfo($file['name'])['extension'];
    	move_uploaded_file($file['tmp_name'], FILE_FOLDER_DIR.$new_file_name);
    	$return = ['link' => FILE_FOLDER.$new_file_name];

		$this->response->type('application/json');
		$this->response->body(json_encode($return));
		return $this->response;
	}
	public function userManual() {
		$this->layout = 'dashboard';
		$this->set('details', false);
		$userManualTable = TableRegistry::get('UserManuals');
		if (!empty($this->request->query('manual'))) {
			$userManual = $userManualTable->find()->where(['id' => $this->request->query('manual')])->toArray();

			if (count($userManual) == 0) {
				$this->Flash->error('তথ্য পাওয়া যাচ্ছে না');
				return $this->redirect('/userManual');
			}
			$tags_only = "'".$userManual[0]['tags']."'";

			$related = $userManualTable->find('list', ['keyField'=>'id', 'valueField'=>'title'], array(
				'conditions' => array('title' => 'REGEXP '.$tags_only)
			))->order(['created' => 'desc'])->limit(5)->toArray();
			$this->set('related', $related);
			$this->set('details', true);
			$this->set('userManual', $userManual);
		} else {
			$userManual = $userManualTable->find();
			if (!empty($this->request->query('tag'))) {
				$userManual = $userManual->where(['tags LIKE' => '%'.$this->request->query('tag').'%']);
			}
			if (!empty($this->request->query('search'))) {
				$this->set('search_text', $this->request->query('search'));
				$userManual = $userManual->where(['title LIKE' => '%'.$this->request->query('search').'%'])->orWhere(['description LIKE' => '%'.$this->request->query('search').'%']);
			}
			if (!empty($this->request->query('module'))) {
				$userManual = $userManual->where(['module LIKE' => '%'.$this->request->query('module').'%']);
			}
			$userManual = $userManual->order(['rand()']); #->limit(10)->toArray();

			$this->paginate = ['limit' => 10];
			$this->set('userManual', $this->paginate($userManual));
		}

		$tag_list = $userManualTable->find()->group(['tags'])->toArray();
		$tags = [];
		foreach ($tag_list as $k => $data_list) {
			$tag_one = explode('|', $data_list['tags']);
			$tags = array_merge($tags, $tag_one);
		}


		$recent = $userManualTable->find('list', ['keyField'=>'id', 'valueField'=>'title'])->order(['created' => 'desc'])->limit(5)->toArray();
		$this->set(compact('userManual', 'tags', 'recent'));
	}
	public function playRequest() {
		$httpClient = new \GuzzleHttp\Client();
		if (isset($this->request->data()['text'])) {
			$data_id = $this->request->data()['id'];
			$files = [];
			$requested_text = $this->request->data()['text'];
			$text_parts = wordwrap($requested_text, 180, '|', false);
			$text_parts = explode('|', $text_parts);

			$audioOutputDir = FILE_FOLDER_DIR.'UserManual/audio/'.$data_id.'/';
			if (file_exists($audioOutputDir)) {
				$dir = new Folder($audioOutputDir);
				$files = $dir->find('.*\.mp3');
				if (count($text_parts) == count($files)) {
					foreach ($files as $key => $file) {
					    $file_name = 'UserManual/audio/'.$data_id.'/'.$file;
                        $token = sGenerateToken(['file' => $file_name], ['exp' => time() + 60 * 300]);
                        $files[$key] = "content/".$file_name.'?token='.$token;
					}
					echo json_encode($files);
					die;
				} else {
					$files = [];
				}
			} else {
				mkdir($audioOutputDir,0755);
			}
			$ttsClient = new \GoogleFree\TextToSpeech\Client($httpClient);
			$ttsClient->setAudioOutputDir($audioOutputDir);

			try {
				foreach ($text_parts as $key => $text) {
					$speechFile = new \GoogleFree\TextToSpeech\SpeechFile($text, 'bn-BD');
					$speechFile->setName($key);

					$didSucceed = $ttsClient->downloadAudio($speechFile);
					$full_path = 'content/UserManual/audio/'.$data_id.'/'.$speechFile->getName().'.mp3';

					if ($didSucceed) {
						$files[] = $full_path;
					} else {
						//echo 'Argh! Something went wrong';
					}
				}
			}
			catch(\Exception $ex) {
				var_dump($ex->getMessage());
			}

			echo json_encode($files);
			die;
		}
	}
    public function showUserManualList()
    {
        $this->layout = 'dashboard';
//        $this->view = 'download_app';
    }
    private function _populateReportDashboard(){
        if (($all_login_page_seetings_data = Cache::read('login_page_settings', 'memcached')) === false) {
            $table_instance_lps = TableRegistry::get('LoginPageSettings');
            $all_data['other'] = $table_instance_lps->find()->where(['type' => 2])->order('id DESC')->limit(2)->hydrate(true)->toArray();
            $all_data['notice'] = $table_instance_lps->find()->where(['type' => 1])->hydrate(true)->toArray();
            $all_data['report'] = $table_instance_lps->find()->where(['type' => 3])->hydrate(true)->toArray();
            Cache::write('login_page_settings', $all_data, 'loginSettingsCache');
            $all_login_page_seetings_data = $all_data;
        }
        return $all_login_page_seetings_data;
    }

    public function test() {
//        dd('Hello, I am test');
//        $employeeOffice_table = TableRegistry::get('EmployeeOffices');
//        pr($employeeOffice_table->getSuggestion('মোঃ হাসানুজ্জামান'));
//        pr($employeeOffice_table->getSuggestion('সিনিয়র সফটওয়্যার ইঞ্জিনিয়ার'));
//        pr($employeeOffice_table->getSuggestion('ই-সার্ভিস'));
//        pr($employeeOffice_table->getSuggestion('এ্যাকসেস টু ইনফরমেশন'));
        die;
    }
    public function privacyPolicyForApp(){
        if (empty($this->Auth->User())) {
            $this->layout = 'dashboard';
        }
    }

    public function nothi_video_tutorial() {
        $video_tutorial_links = [
            ['id' => '_gU_2BGa28w', 'name' => 'নথি ১ - নথি সিস্টেমে লগইন ও পরিচিতি', 'url' => 'https://youtube.com/embed/_gU_2BGa28w'],
            ['id' => 'rxoS55PAQBI', 'name' => 'নথি ২ - প্রোফাইল এডিট করা', 'url' => 'https://youtube.com/embed/rxoS55PAQBI'],
            ['id' => 'EMz_fHYCom4', 'name' => 'নথি ৩ - সিস্টেমে ডাক আপলোড করা', 'url' => 'https://youtube.com/embed/EMz_fHYCom4'],
            ['id' => 'xfC3QkoSsbs', 'name' => 'নথি ৪ - আগত ডাক দেখা ও সিদ্ধান্ত দেয়া', 'url' => 'https://youtube.com/embed/xfC3QkoSsbs'],
            ['id' => 'jMlRuCDsoOY', 'name' => 'নথি ৫ - আগত ডাক নথিতে উপস্থাপন', 'url' => 'https://youtube.com/embed/jMlRuCDsoOY'],
            ['id' => 'IBaNz6HEL-4', 'name' => 'নথি ৬ - আগত নথি দেখা ও সিদ্ধান্ত দেয়া', 'url' => 'https://youtube.com/embed/IBaNz6HEL-4'],
            ['id' => 'edDnoU7EouA', 'name' => 'নথি ৭ - খসড়াপত্র তৈরি ও প্রেরণ', 'url' => 'https://youtube.com/embed/edDnoU7EouA'],
            ['id' => 'CstqbJ_-aGg', 'name' => 'নথি ৮ - খসড়াপত্র অনুমোদন ও পত্রজারির জন্য প্রেরণ', 'url' => 'https://youtube.com/embed/CstqbJ_-aGg'],
            ['id' => 'hZiZDcR6_sQ', 'name' => 'নথি ৯ - নথি সিস্টেমে পত্রজারি ও প্রেরণ', 'url' => 'https://youtube.com/embed/hZiZDcR6_sQ'],
            ['id' => 'CI--x1gfOqw', 'name' => 'নথি ১০ - নোট নিষ্পত্তি', 'url' => 'https://youtube.com/embed/CI--x1gfOqw'],
            ['id' => 's1R-dzq9PHE', 'name' => 'নথি ১১.১ - অফিস এডমিন (কর্মকর্তা-কর্মচারীর তালিকা দেখা, ইউজার আইডি তৈরি, রিলিজ, এসাইন)', 'url' => 'https://youtube.com/embed/s1R-dzq9PHE'],
            ['id' => 'j5druwosldw', 'name' => 'নথি ১১.২ - অফিস এডমিন (ড্যাশবোর্ড দেখার অনুমতি, কর্মকর্তাদের ইংরেজি পদবি সংশোধন, অফিস প্রধান বাছাই, অফিস ফ্রন্ট ডেস্ক নির্বাচন, অফিস তথ্য সংশোধন)', 'url' => 'https://youtube.com/embed/j5druwosldw'],
            ['id' => 'wz0wt2VWk3M', 'name' => 'নথি ১১.৩ - অফিস এডমিন (অফিস কাঠামো ট্রান্সফার, শাখা তথ্য সংশোধন,পদবির স্তর ও ক্রম ির্বাচন, রিপোর্ট সংগ্রহ)', 'url' => 'https://youtube.com/embed/wz0wt2VWk3M'],
            ['id' => 'GIkoC9Qz0Ps', 'name' => 'নথি ১১.৪ - অফিস এডমিন (প্রতিকল্প সেট করা)', 'url' => 'https://youtube.com/embed/GIkoC9Qz0Ps'],
            ['id' => 'Q5fqmlsVfto', 'name' => 'নথি ১১.৫ - অফিস এডমিন (নথির তথ্য ও পত্রজারি হেডিং সংশোধন)', 'url' => 'https://youtube.com/embed/Q5fqmlsVfto'],
        ];
        $this->set(compact('video_tutorial_links'));
        $this->layout = null;
        $this->view = '/Users/nothi_video_tutorial';
    }
}