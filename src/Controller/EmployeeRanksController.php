<?php
namespace App\Controller;

use App\Controller\ProjapotiController;
use App\Model\Table;
use App\Model\Table\EmployeeRanksTable;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\Entity;
use App\Model\Entity\EmployeeRanks;
use Cake\ORM\TableRegistry;

class EmployeeRanksController extends ProjapotiController
{
    public function index()
    {
        /* $this->paginate = [
            'contain' => ['Employee']
        ];
        $this->set('geoDistricts', $this->paginate($this->EmployeeRanks));
        $this->set('_serialize', ['geoDistricts']); */

        $employeeRanks = TableRegistry::get('EmployeeRanks');
        $query = $employeeRanks->find('all');
        $this->set(compact('query'));
    }

    public function add()
    {
        $this->loadModel('OfficeMinistries');
        $office_ministries = $this->OfficeMinistries->find('all');
        $this->set(compact('office_ministries'));

        if ($this->request->is('post')) {

            $d = $this->request->data['office_ministry_id'];

            $this->loadModel('OfficeMinistries');
            $office_ministry = TableRegistry::get('OfficeMinistries');

            $office_ministry = $office_ministry->find()
                ->where(['id =' => $d])
                ->extract('reference_code');
            foreach ($office_ministry as $ministry):
            endforeach;

//            pr($division);
//            die();

            $employee_rank = $this->EmployeeRanks->newEntity();
            $employee_rank->division_bbs_code = $ministry;
            $employee_rank = $this->EmployeeRanks->patchEntity($employee_rank, $this->request->data);
            if ($this->EmployeeRanks->save($employee_rank)) {
                return $this->redirect(['action' => 'index']);
            }
        }
    }

    public function edit($id = null, $is_ajax = null)
    {
        if ($is_ajax == 'ajax') {
            $this->layout = null;
        }

        $this->loadModel('OfficeMinistries');
        $office_ministries = $this->OfficeMinistries->find('all');
        $this->set(compact('office_ministries'));

        $employee_rank = $this->EmployeeRanks->get($id);
        if ($this->request->is(['post', 'put'])) {
            $this->EmployeeRanks->patchEntity($employee_rank, $this->request->data);
            if ($this->EmployeeRanks->save($employee_rank)) {
                return $this->redirect(['action' => 'index']);
            }
        }
        $this->set('employee_rank', $employee_rank);
    }

    public function view($id = null)
    {
        $employee_rank = $this->EmployeeRanks->get($id);
        $this->set(compact('employee_rank'));
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);

        $employee_rank = $this->EmployeeRanks->get($id);
        if ($this->EmployeeRanks->delete($employee_rank)) {
            return $this->redirect(['action' => 'index']);
        }
    }
}
