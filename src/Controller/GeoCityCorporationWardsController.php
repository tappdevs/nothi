<?php

namespace App\Controller;

use App\Controller\ProjapotiController;
use App\Model\Table;
use App\Model\Table\GeoCityCorporationWardsTable;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\Entity;
use App\Model\Entity\GeoCityCorporationWards;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

class GeoCityCorporationWardsController extends ProjapotiController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Paginator');
    }
    public function index()
    {
        $geo_city_corporation_wards = TableRegistry::get('GeoCityCorporationWards');
        $query                      = $geo_city_corporation_wards->find('all')->contain(['GeoDivisions','GeoCityCorporations','GeoDistricts']);
          try {
            $this->set('query', $this->paginate($query));
        } catch (NotFoundException $e) {
            $this->redirect(['action' => 'index']);
        }
    }

    public function add()
    {
        $this->loadModel('GeoCityCorporations');
        $geo_city_corporations = $this->GeoCityCorporations->find('all');
        $this->set(compact('geo_city_corporations'));

        $this->loadModel('GeoDistricts');
        $geo_districts = $this->GeoDistricts->find('all');
        $this->set(compact('geo_districts'));

        $this->loadModel('GeoDivisions');
        $geo_divisions = $this->GeoDivisions->find('all');
        $this->set(compact('geo_divisions'));

        $geo_city_corporation_wards = $this->GeoCityCorporationWards->newEntity();
        if ($this->request->is('post')) {

            $validator = new Validator();
            $validator->notEmpty('ward_name_bng',
                'ওয়ার্ডের  নাম(বাংলা) দেওয়া হয় নি')->notEmpty('ward_name_eng',
                'ওয়ার্ডের  নাম(ইংরেজি) দেওয়া হয় নি')->notEmpty('bbs_code',
                'ওয়ার্ডের কোড দেওয়া হয় নি')->notEmpty('status',
                'অবস্থা নির্বাচন করুন')->notEmpty('geo_district_id',
                'জেলা কোড দেওয়া হয় নি')->notEmpty('geo_division_id',
                'বিভাগ কোড দেওয়া হয় নি')->notEmpty('geo_city_corporation_id',
                ' সিটি কর্‌পোরেশন নির্বাচন করুন');
            $errors    = $validator->errors($this->request->data());

           if (empty($errors)) {
                $geo_city_corporation_wards = $this->GeoCityCorporationWards->patchEntity($geo_city_corporation_wards,
                    $this->request->data);
                $other_info                     = TableRegistry::get('GeoCityCorporations')->find()->select(['bbs_code',
                        'district_bbs_code', 'division_bbs_code'])->where([ 'id' => $geo_city_corporation_wards->geo_city_corporation_id])->first();
                $geo_city_corporation_wards->district_bbs_code         = $other_info['district_bbs_code'];
                $geo_city_corporation_wards->division_bbs_code         = $other_info['division_bbs_code'];
                $geo_city_corporation_wards->city_corporation_bbs_code = $other_info['bbs_code'];
                if ($this->GeoCityCorporationWards->save($geo_city_corporation_wards)) {
                    $this->Flash->success('সংরক্ষিত হয়েছে।');
                    return $this->redirect(['action' => 'index']);
                }
            } else {
                 $this->set(compact('errors'));
            }
        }
        $this->set('geo_city_corporation_wards', $geo_city_corporation_wards);
    }

    public function edit($id = null, $is_ajax = null)
    {
        if ($is_ajax == 'ajax') {
            $this->layout = null;
        }

          $geo_divisions = TableRegistry::get('GeoDivisions')->find('list',
                    ['keyField' => 'id', 'valueField' => 'division_name_bng'])->toArray();
            $this->set(compact('geo_divisions'));

            $geo_districts = TableRegistry::get('GeoDistricts')->find('list',
                    ['keyField' => 'id', 'valueField' => 'district_name_bng'])->toArray();
            $this->set(compact('geo_districts'));

            $GeoCityCorporations       = TableRegistry::get('GeoCityCorporations')->find('list',
                    ['keyField' => 'id', 'valueField' => 'city_corporation_name_bng'])->toArray();
            $this->set(compact('GeoCityCorporations'));

        $geo_city_corporation_wards = $this->GeoCityCorporationWards->get($id);
        if ($this->request->is(['post', 'put'])) {

            $validator = new Validator();
            $validator->notEmpty('ward_name_bng',
                'ওয়ার্ডের  নাম(বাংলা) দেওয়া হয় নি')->notEmpty('ward_name_eng',
                'ওয়ার্ডের  নাম(ইংরেজি) দেওয়া হয় নি')->notEmpty('bbs_code',
                'ওয়ার্ডের কোড দেওয়া হয় নি')->notEmpty('status',
                'অবস্থা নির্বাচন করুন')->notEmpty('geo_district_id',
                'জেলা কোড দেওয়া হয় নি')->notEmpty('geo_division_id',
                'বিভাগ কোড দেওয়া হয় নি')->notEmpty('geo_city_corporation_id',
                ' সিটি কর্‌পোরেশন নির্বাচন করুন');
            $errors    = $validator->errors($this->request->data());

            if (empty($errors)) {
                   $this->GeoCityCorporationWards->patchEntity($geo_city_corporation_wards,
                    $this->request->data);
                if ($this->GeoCityCorporationWards->save($geo_city_corporation_wards)) {
                    $this->Flash->success('সংরক্ষিত হয়েছে।');
                    return $this->redirect(['action' => 'index']);
                }
            } else {
                  $this->set(compact('errors'));
            }
         
        }
        $this->set('geo_city_corporation_ward', $geo_city_corporation_wards);
    }

    public function view($id = null)
    {
        $geo_city_corporation_wards = $this->GeoCityCorporationWards->get($id);
        $this->set(compact('geo_city_corporation_wards'));
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);

        $geo_city_corporation_wards = $this->GeoCityCorporationWards->get($id);
        if ($this->GeoCityCorporationWards->delete($geo_city_corporation_wards)) {
            return $this->redirect(['action' => 'index']);
        }
    }
}