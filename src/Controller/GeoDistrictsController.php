<?php
namespace App\Controller;

use App\Controller\ProjapotiController;
use App\Model\Table;
use App\Model\Table\GeoDistrictsTable;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\Entity;
use App\Model\Entity\GeoDistricts;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

class GeoDistrictsController extends ProjapotiController
{
    public function index()
    {
          $GeoDistricts = TableRegistry::get('GeoDistricts');
        $query                  = $GeoDistricts->find('all')->contain(['GeoDivisions']);
          try{
        $this->set('query',$this->paginate($query));
        }catch (NotFoundException $e) {
            $this->redirect(['action'=>'index']);
        }
    }

    public function add()
    {
        $this->loadModel('GeoDivisions');
        $geo_divisions = $this->GeoDivisions->find('all');
        $this->set(compact('geo_divisions'));
        $geo_district = array();
        if ($this->request->is('post')) {

            $validator = new Validator();
            $validator->notEmpty('geo_division_id', 'বিভাগ নির্বাচন করুন')->notEmpty('district_name_bng',
                'জেলার নাম(বাংলা) দেওয়া হয় নি')->notEmpty('district_name_eng',
                'জেলার নাম(ইংরেজি) দেওয়া হয় নি')->notEmpty('bbs_code',
                'জেলা কোড দেওয়া হয় নি')->notEmpty('status',
                'অবস্থা নির্বাচন করুন');
            $errors    = $validator->errors($this->request->data());
            if (empty($errors)) {
                            $d = $this->request->data['geo_division_id'];

                $geo_division = TableRegistry::get('GeoDivisions');

                $geo_division = $geo_division->find()
                    ->where(['id =' => $d])
                    ->select(['bbs_code']);
                $geo_district                    = $this->GeoDistricts->newEntity();
                $geo_district->division_bbs_code = $geo_division['bbs_code'];
                $geo_district                    = $this->GeoDistricts->patchEntity($geo_district,
                    $this->request->data);
                if ($this->GeoDistricts->save($geo_district)) {
                    $this->Flash->success('সংরক্ষিত হয়েছে।');
                    return $this->redirect(['action' => 'index']);
                }
            } else {
                $this->set('errors', $errors);
            }

        }
        $this->set('user', $geo_district);
    }

    public function edit($id = null, $is_ajax = null)
    {
        if ($is_ajax == 'ajax') {
            $this->layout = null;
        }
          $geo_divisions = TableRegistry::get('GeoDivisions')->find('list',
                    ['keyField' => 'id', 'valueField' => 'division_name_bng'])->toArray();
            $this->set(compact('geo_divisions'));
        $geo_district = $this->GeoDistricts->get($id);
        if ($this->request->is(['post', 'put'])) {

            $validator = new Validator();
            $validator->notEmpty('geo_division_id', 'বিভাগ নির্বাচন করুন')->notEmpty('district_name_bng',
                'জেলার নাম(বাংলা) দেওয়া হয় নি')->notEmpty('district_name_eng',
                'জেলার নাম(ইংরেজি) দেওয়া হয় নি')->notEmpty('bbs_code',
                'জেলা কোড দেওয়া হয় নি')->notEmpty('status',
                'অবস্থা নির্বাচন করুন')->notEmpty('division_bbs_code',
                'বিভাগ কোড দেওয়া হয় নি');
            $errors    = $validator->errors($this->request->data());
            if (empty($errors)) {
                 $this->GeoDistricts->patchEntity($geo_district,
                    $this->request->data);
                if ($this->GeoDistricts->save($geo_district)) {
                    $this->Flash->success('সংরক্ষিত হয়েছে।');
                    return $this->redirect(['action' => 'index']);
                }
            } else {
                $this->set(compact('errors'));
            }

           
        }
        $this->set('geo_district', $geo_district);
    }

    public function view($id = null)
    {
        $geo_district = $this->GeoDistricts->get($id);
        $this->set(compact('geo_district'));
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);

        $geo_district = $this->GeoDistricts->get($id);
        if ($this->GeoDistricts->delete($geo_district)) {
            return $this->redirect(['action' => 'index']);
        }
    }

    public function getDistrictsByDivision()
    {
        $division_id = $this->request->data['geo_division_id'];
        $districtTable = TableRegistry::get('GeoDistricts');
        $data_array = $districtTable->find('list')->where(['geo_division_id' => $division_id])->order(['id'])->toArray();
        $this->response->body(json_encode($data_array));
        $this->response->type('application/json');
        return $this->response;
    }
}
