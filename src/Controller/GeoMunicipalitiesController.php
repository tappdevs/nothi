<?php

namespace App\Controller;

use App\Controller\ProjapotiController;
use App\Model\Table;
use App\Model\Table\GeoMunicipalitiesTable;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\Entity;
use App\Model\Entity\GeoMunicipalities;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

class GeoMunicipalitiesController extends ProjapotiController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Paginator');
    }
    public function index()
    {
        $geo_municipalities = TableRegistry::get('GeoMunicipalities');
        $query              = $geo_municipalities->find('all')->contain(['GeoDivisions','GeoDistricts','GeoUpazilas']);
          try {
            $this->set('query', $this->paginate($query));
        } catch (NotFoundException $e) {
            $this->redirect(['action' => 'index']);
        }
    }

    public function add()
    {
        $this->loadModel('GeoDivisions');
        $geo_divisions = $this->GeoDivisions->find('all');
        $this->set(compact('geo_divisions'));

        $this->loadModel('GeoDistricts');
        $geo_districts = $this->GeoDistricts->find('all');
        $this->set(compact('geo_districts'));

        $this->loadModel('GeoUpazilas');
        $geo_upazilas = $this->GeoUpazilas->find('all');
        $this->set(compact('geo_upazilas'));

        $geo_municipalities = $this->GeoMunicipalities->newEntity();
        if ($this->request->is('post')) {

            $validator = new Validator();
            $validator->notEmpty('municipality_name_bng',
                'পৌরসভার  নাম(বাংলা) দেওয়া হয় নি')->notEmpty('municipality_name_eng',
                'পৌরসভার  নাম(ইংরেজি) দেওয়া হয় নি')->notEmpty('bbs_code',
                'পৌরসভার কোড দেওয়া হয় নি')->notEmpty('status',
                'অবস্থা নির্বাচন করুন')->notEmpty('geo_district_id',
                'জেলা নির্বাচন করুন')->notEmpty('geo_division_id',
                'বিভাগ নির্বাচন করুন')->notEmpty('geo_upazila_id',
                'উপজেলা নির্বাচন করুন');
            $errors    = $validator->errors($this->request->data());

            if (empty($errors)) {
                $geo_municipalities                    = $this->GeoMunicipalities->patchEntity($geo_municipalities,
                    $this->request->data);
                $other_info                            = TableRegistry::get('GeoUpazilas')->find()->select(['bbs_code',
                        'district_bbs_code', 'division_bbs_code'])->where([ 'id' => $geo_municipalities->geo_upazila_id])->first();
                $geo_municipalities->district_bbs_code = $other_info['district_bbs_code'];
                $geo_municipalities->division_bbs_code = $other_info['division_bbs_code'];
                $geo_municipalities->upazila_bbs_code  = $other_info['bbs_code'];
                if ($this->GeoMunicipalities->save($geo_municipalities)) {
                    $this->Flash->success('সংরক্ষিত হয়েছে।');
                    return $this->redirect(['action' => 'index']);
                }
            } else {
                $this->set(compact('errors'));
            }
        }
        $this->set('geo_municipalities', $geo_municipalities);
    }

    public function edit($id = null, $is_ajax = null)
    {
        if ($is_ajax == 'ajax') {
            $this->layout = null;
        }

          $geo_divisions = TableRegistry::get('GeoDivisions')->find('list',
                    ['keyField' => 'id', 'valueField' => 'division_name_bng'])->toArray();
            $this->set(compact('geo_divisions'));

            $geo_districts = TableRegistry::get('GeoDistricts')->find('list',
                    ['keyField' => 'id', 'valueField' => 'district_name_bng'])->toArray();
            $this->set(compact('geo_districts'));

            $geo_upazilas       = TableRegistry::get('GeoUpazilas')->find('list',
                    ['keyField' => 'id', 'valueField' => 'upazila_name_bng'])->toArray();
            $this->set(compact('geo_upazilas'));

        $geo_municipalities = $this->GeoMunicipalities->get($id);
        if ($this->request->is(['post', 'put'])) {

            $validator = new Validator();
            $validator->notEmpty('municipality_name_bng',
                'পৌরসভার  নাম(বাংলা) দেওয়া হয় নি')->notEmpty('municipality_name_eng',
                'পৌরসভার  নাম(ইংরেজি) দেওয়া হয় নি')->notEmpty('bbs_code',
                'পৌরসভার কোড দেওয়া হয় নি')->notEmpty('status',
                'অবস্থা নির্বাচন করুন')->notEmpty('geo_district_id',
                'জেলা নির্বাচন করুন')->notEmpty('geo_upazila_id',
                'উপজেলা নির্বাচন করুন')->notEmpty('upazila_bbs_code',
                'উপজেলা কোড কোড দেওয়া হয় নি')->notEmpty('district_bbs_code',
                'জেলা কোড কোড দেওয়া হয় নি');
            $errors    = $validator->errors($this->request->data());

            if (empty($errors)) {
                $this->GeoMunicipalities->patchEntity($geo_municipalities,
                    $this->request->data);
                if ($this->GeoMunicipalities->save($geo_municipalities)) {
                    $this->Flash->success('সংরক্ষিত হয়েছে।');
                    return $this->redirect(['action' => 'index']);
                }
            } else {
                $this->set(compact('errors'));
            }
        }
        $this->set('geo_municipalities', $geo_municipalities);
    }

    public function view($id = null)
    {
        $geo_municipalities = $this->GeoMunicipalities->get($id);
        $this->set(compact('geo_municipalities'));
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);

        $geo_municipalities = $this->GeoMunicipalities->get($id);
        if ($this->GeoMunicipalities->delete($geo_municipalities)) {
            return $this->redirect(['action' => 'index']);
        }
    }
}