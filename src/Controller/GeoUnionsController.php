<?php
namespace App\Controller;

use App\Controller\ProjapotiController;
use App\Model\Table;
use App\Model\Table\GeoUnionsTable;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\Entity;
use App\Model\Entity\GeoUnions;
use Cake\ORM\TableRegistry;

class GeoUnionsController extends ProjapotiController
{
        public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Paginator');
    }
    public function index()
    {
        $geo_union = TableRegistry::get('GeoUnions');
        $query = $geo_union->find('all');
          try {
            $this->set('query', $this->paginate($query));
        } catch (NotFoundException $e) {
            $this->redirect(['action' => 'index']);
        }
    }

    public function add()
    {
        $this->loadModel('GeoDivisions');
        $geo_divisions = $this->GeoDivisions->find('all');
        $this->set(compact('geo_divisions'));

        $this->loadModel('GeoDistricts');
        $geo_districts = $this->GeoDistricts->find('all');
        $this->set(compact('geo_districts'));

        $this->loadModel('GeoUpazilas');
        $geo_upazilas = $this->GeoUpazilas->find('all');
        $this->set(compact('geo_upazilas'));

        $this->loadModel('GeoThanas');
        $geo_thanas = $this->GeoThanas->find('all');
        $this->set(compact('geo_thanas'));

        $geo_union = $this->GeoUnions->newEntity();
        if ($this->request->is('post')) {
            $geo_union = $this->GeoUnions->patchEntity($geo_union, $this->request->data);
            if ($this->GeoUnions->save($geo_union)) {
                return $this->redirect(['action' => 'index']);
            }
        }
        $this->set('geo_union', $geo_union);
    }

    public function edit($id = null, $is_ajax = null)
    {
        if ($is_ajax == 'ajax') {
            $this->layout = null;
        }
        $geo_union = $this->GeoUnions->get($id);
        if ($this->request->is(['post', 'put'])) {
            $this->GeoUnions->patchEntity($geo_union, $this->request->data);
            if ($this->GeoUnions->save($geo_union)) {
                return $this->redirect(['action' => 'index']);
            }
        }
        $this->set('geo_union', $geo_union);
    }


    public function view($id = null)
    {
        $geo_union = $this->GeoUnions->get($id);
        $this->set(compact('geo_union'));
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);

        $geo_union = $this->GeoUnions->get($id);
        if ($this->GeoUnions->delete($geo_union)) {
            return $this->redirect(['action' => 'index']);
        }
    }

    public function getUnionsByUpazilla()
    {
        $id = $this->request->data['geo_upazila_id'];
        $districtTable = TableRegistry::get('GeoUnions');
        $data_array = $districtTable->find('list')->where(['geo_upazila_id' => $id])->order(['id'])->toArray();
        $this->response->body(json_encode($data_array));
        $this->response->type('application/json');
        return $this->response;
    }
}
