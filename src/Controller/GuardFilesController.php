<?php

namespace App\Controller;

use App\Controller\ProjapotiController;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\TableRegistry;
use Cake\I18n\Number;
use Cake\Datasource\ConnectionManager;
use Cake\I18n\Time;

class GuardFilesController extends ProjapotiController {
    /*
     * Nothi Masters
     */

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
    }

    public function guardFileCategoryAdd() {
        $guardFileCategories_table = TableRegistry::get("GuardFileCategories");
        $guardFileCategories_records = $guardFileCategories_table->newEntity();

        $employee_office = $this->getCurrentDakSection();

        if ($this->request->is('post')) {

            if (!empty($this->request->data)) {
                $this->request->data['office_id'] = $employee_office['office_id'];
                $this->request->data['office_unit_id'] = $employee_office['office_unit_id'];
                $this->request->data['office_unit_organogram'] = $employee_office['office_unit_organogram_id'];
                $guardFileCategories_records = $guardFileCategories_table->patchEntity($guardFileCategories_records, $this->request->data, ['validate' => 'add']);
                $guardFileCategories_records->created = date("Y-m-d");


                $errors = $guardFileCategories_records->errors();
                if (!$errors) {
                    try {
                        $guardFileCategories_table->save($guardFileCategories_records);
                        $this->Flash->success('ধরন সংরক্ষিত হয়েছে।');
                        $this->redirect(['action' => 'guardFileCategories']);
                    } catch (\Exception $ex) {
                        $this->Flash->error('ধরন সংরক্ষিত করা সম্ভব হচ্ছে না।');
                    }
                } else {
                    $this->Flash->error('ধরন সংরক্ষিত করা সম্ভব হচ্ছে না।');
                }
            }
        }

        $this->set(compact('guardFileCategories_records'));
    }

    public function guardFileCategoryEdit($id) {
        $guardFileCategories_table = TableRegistry::get("GuardFileCategories");

        if (empty($id)) {
            $this->Flash->error('দুঃখিত আপনি অনুমতিপ্রাপ্ত নয়।');
            $this->redirect(['action' => 'guardFileCategories']);
        }

        $employee_office = $this->getCurrentDakSection();

        $guardFileCategories_records = $guardFileCategories_table->get($id);
        if(empty($guardFileCategories_records) || $guardFileCategories_records['office_unit_organogram']!= $employee_office['office_unit_organogram_id']){
            $this->Flash->error('দুঃখিত আপনি অনুমতিপ্রাপ্ত নয়।');
            $this->redirect(['action' => 'guardFileCategories']);
        }

        if ($this->request->is('post')) {
            if (!empty($this->request->data)) {
                $this->request->data['office_id'] = $employee_office['office_id'];
                $this->request->data['office_unit_id'] = $employee_office['office_unit_id'];
                $this->request->data['office_unit_organogram'] = $employee_office['office_unit_organogram_id'];
                $guardFileCategories_records = $guardFileCategories_table->patchEntity($guardFileCategories_records, $this->request->data, ['validate' => 'edit']);
                $guardFileCategories_records->created = date("Y-m-d");

                $errors = $guardFileCategories_records->errors();

                if (!$errors) {
                    try {

                        $guardFileCategories_table->save($guardFileCategories_records);
                        $this->Flash->success('ধরন সংরক্ষিত হয়েছে।');
                        $this->redirect(['action' => 'guardFileCategories']);
                    } catch (\Exception $e) {
                        $this->Flash->error('ধরন সংরক্ষিত করা সম্ভব হচ্ছে না।');
                    }
                } else {
                    $this->Flash->error('ধরন সংরক্ষিত করা সম্ভব হচ্ছে না।');
                }
            }
        }
        $this->set('guardFileCategories_records', $guardFileCategories_records);
    }

    public function guardFileCategories() {
        
    }

    public function guardFileCategoriesList() {
        $this->layout = null;
        $guardFileCategories_table = TableRegistry::get("GuardFileCategories");

        $start = isset($this->request->data['start']) ? intval($this->request->data['start']) : 0;
        $len = isset($this->request->data['length']) ? intval($this->request->data['length']) : 500;
        $page = ($start / $len) + 1;


        $employee_office = $this->getCurrentDakSection();

        if ($this->request->is('ajax')) {
            $totalRec = $guardFileCategories_table->getAllTypes(0, 100, $employee_office)->toArray();
            $nothityperecord = $guardFileCategories_table->getAllTypes($page, $len, $employee_office)->toArray();

            $data = [];
            if (!empty($nothityperecord)) {
                $i = 0;
                foreach ($nothityperecord as $key => $record) {
                    $i++;
                    $si = (($page - 1) * $len) + $i;
                    $data[] = array(
                        "<div class='text-center'>" . Number::format($si) . "</div>",
                        "<div class='text-left'>" . h($record['name_bng']) . "</div>",
                        "<div class='text-center'>" . '<a title="সম্পাদনা" href="guardFileCategoryEdit/' . $record['id'] . '" class="btn btn-primary btn-sm"  ><span class="glyphicon glyphicon-pencil"></span></a>' .
                        "</div>"
                    );
                }
            }

            $json_data = array(
                "draw" => intval($this->request->data['draw']),
                "recordsTotal" => count($totalRec),
                "recordsFiltered" => count($totalRec),
                "data" => $data
            );
            $this->response->body(json_encode($json_data));
        } else {
            $this->response->body(json_encode(0));
        }

        $this->response->type('application/json');
        return $this->response;
    }

    public function guardFiles() {
        $employee_office = $this->getCurrentDakSection();
        $guardFileCategories_table = TableRegistry::get("GuardFileCategories");
        $guarfilesubjects = $guardFileCategories_table->getTypes(0, $employee_office);
//        pr($guarfilesubjects); die;
        $this->set(compact('guarfilesubjects','employee_office'));
    }

    public function guardFilesList() {
        $this->layout = null;
        $guardFiles_table = TableRegistry::get("GuardFiles");
        $guardFilesAttachment_table = TableRegistry::get("GuardFileAttachments");

        $start = isset($this->request->data['start']) ? intval($this->request->data['start']) : 0;
        $len = isset($this->request->data['length']) ? intval($this->request->data['length']) : 500;
        $page = ($start / $len) + 1;

        $employee_office = $this->getCurrentDakSection();

        if ($this->request->is('ajax')) {
            $totalRec = $guardFiles_table->getAll(0, 0, $employee_office)->toArray();
            $guardFilesrecord = $guardFiles_table->getAll($page, $len, $employee_office)->toArray();

            $data = [];
            if (!empty($guardFilesrecord)) {
                $i = 0;
                foreach ($guardFilesrecord as $key => $record) {
                    $totalAttachment = $guardFilesAttachment_table->getTotal($record['id']);
                    $i++;
                    $si = (($page - 1) * $len) + $i;
                    $data[] = array(
                        "<div class='text-center'>" . Number::format($si) . "</div>",
                        "<div class='text-left'>" . $record['category_name'] . "</div>",
                        "<div class='text-left'>" . Number::format($record['file_number']) . ') ' . $record['name_bng'] . "</div>",
//                        "<div class='text-left'>" . $record['office_unit_name'] . "</div>",
                        "<div class='text-center'>" . Number::format($totalAttachment) . "</div>",
                        "<div class='text-center'><a title='বিস্তারিত'  data-title='বিস্তারিত' href='" . $this->request->webroot . 'guardFiles/showDetails/' . $record['id'] . "' class='btn btn-success btn-xs showdetails' ><i class=' a2i_gn_details2'></i></a></div>"
                    );
                }
            }

            $json_data = array(
                "draw" => intval($this->request->data['draw']),
                "recordsTotal" => count($totalRec),
                "recordsFiltered" => count($totalRec),
                "data" => $data
            );
            $this->response->body(json_encode($json_data));
        } else {
            $this->response->body(json_encode(0));
        }

        $this->response->type('application/json');
        return $this->response;
    }

    public function guardFileUploadAttachments() {
        $guardFileCategories_table = TableRegistry::get("GuardFileCategories");
        $guardFilesAttachment_table = TableRegistry::get("GuardFileAttachments");
        $guardFiles_table = TableRegistry::get("GuardFiles");

        $employee_office = $this->getCurrentDakSection();

        $guardFileEntity = $guardFiles_table->newEntity();
        $attachments = array();
        if ($this->request->is('post', 'ajax')) {
            if (!empty($this->request->data)) {
                if(!empty($this->request->data['type']) && $this->request->data['type']=='portal'){
                    $file= $this->request->data['uploaded_attachments'];
                    $file_info=pathinfo($file);
                    $file_name='nothi_'.$this->request->data['office_id'].'_'.date('Y_m_d').'_'.time().'.'.$file_info['extension'];
                    $current_path=FILE_FOLDER_DIR.'Portal/'.$file_name;
                    $current_link=$this->request->webroot.'content/Portal/'.$file_name;
                    $arrContextOptions=array(
                        "ssl"=>array(
                            "verify_peer"=>false,
                            "verify_peer_name"=>false,
                        ),
                    );
                    try {
                        $res = file_get_contents($file,false, stream_context_create($arrContextOptions));

                        if($res== false){
                            $this->response->body(json_encode(array('status'=>'error', 'details'=>"দুঃখিত! পোর্টালের সাথে যোগাযোগ করা সম্ভব হচ্ছে না।")));
                            $this->response->type('application/json');
                            return $this->response;
                        }

                        $result = file_put_contents($current_path, $res);
                        if($result== false || $result== 0){
                            $this->response->body(json_encode(array('status'=>'error', 'details'=>"দুঃখিত! পোর্টালের সাথে যোগাযোগ করা সম্ভব হচ্ছে না।")));
                            $this->response->type('application/json');
                            return $this->response;
                        }
                    } catch (Exception $e) {
                        $this->response->body(json_encode(array('status'=>'error', 'details'=>"দুঃখিত! পোর্টালের সাথে যোগাযোগ করা সম্ভব হচ্ছে না।".$e)));
                        $this->response->type('application/json');
                        return $this->response;
                    }

                    $this->request->data['uploaded_attachments']= $current_link;

                };

                try {
                    $con = ConnectionManager::get('NothiAccessDb');

                    $con->begin();
                    $fileNumber = $guardFiles_table->getFileNumber($employee_office['office_id'], $employee_office['office_unit_id'],$this->request->data['guard_file_category_id']);

                    $attachments = explode(',', $this->request->data['uploaded_attachments']);

                    unset($this->request->data['uploaded_attachments']);

                    $guardFiles_table->patchEntity($guardFileEntity, $this->request->data, ['validate' => 'add']);

                    $guardFileEntity->office_id = $employee_office['office_id'];
                    $guardFileEntity->office_name = $employee_office['office_name'];
                    $guardFileEntity->office_unit_id = $employee_office['office_unit_id'];
                    $guardFileEntity->office_unit_name = $employee_office['office_unit_name'];
                    $guardFileEntity->office_unit_organogram_id = $employee_office['office_unit_organogram_id'];
                    $guardFileEntity->office_unit_organogram_name = $employee_office['designation_label'];

                    if (!empty($fileNumber)) {
                        $guardFileEntity->file_number = $fileNumber['file_number'] + 1;
                    } else {
                        $guardFileEntity->file_number = 1;
                    }

                    $guardFiles_table->save($guardFileEntity);

                    if (!empty($attachments)) {
                        foreach ($attachments as $k => $file) {

                            $attachment_data = array();

                            $attachment_data['guard_file_id'] = $guardFileEntity->id;
                            $attachment_data['attachment_type'] = $this->getMimeType($file);


                            if(!empty($this->request->data['type']) && $this->request->data['type']=='portal'){
                                $filepath = explode($this->request->webroot . 'content/Portal', $file);
                                $attachment_data['file_name'] = 'Portal/'.urldecode($filepath[1]);
                            } else{

                                $filepath = explode(FILE_FOLDER , $file);

                                $attachment_data['file_name'] = urldecode($filepath[1]);
                            }
                            $filename = explode('/', $filepath[1]);

                            $attachment_data['name_bng'] = urldecode($filename[count($filename)-1]);

                            $guard_attachment = $guardFilesAttachment_table->newEntity();
                            $guardFilesAttachment_table->patchEntity($guard_attachment, $attachment_data);
                            $guardFilesAttachment_table->save($guard_attachment);
                        }
                    }

                    $con->commit();
                    if(!empty($this->request->data['type']) && $this->request->data['type']=='portal'){
                        $this->response->body(json_encode(array('status'=>'success')));
                        $this->response->type('application/json');
                        return $this->response;
                    }

                    $this->Flash->success("গার্ড ফাইল সংরক্ষণ করা হয়েছে");
                    $this->redirect(['action' => 'guardFiles']);
                } catch (\Exception $ex) {
                    $con->rollback();
                    $this->Flash->error($ex->getMessage());
                }
            }
        }

        $categories = $guardFileCategories_table->getTypes(0, $employee_office);
        $this->set('guardFileEntity', $guardFileEntity);
        $this->set('selected_office_section', $employee_office);
        $this->set('categories', $categories);
        $this->set('attachments', $attachments);
    }
    
    public function showDetails($id){
        
        $this->layout = 'ajax';
        $guardFiles_table = TableRegistry::get("GuardFiles");
        $guardFilesAttachment_table = TableRegistry::get("GuardFileAttachments");
        
        $employee_office = $this->getCurrentDakSection();
        $data = array();
        $attachments = array();
        if(!empty($id)){
            $data = $guardFiles_table->getByUnit($id,$employee_office);
            
            if(!empty($data)){
                $attachments = $guardFilesAttachment_table->getAttachments($id);
            }
        }
        
        $this->set(compact('data','attachments'));
    }
    
    
     public function officeGuardFile($guard_file_page=false) {
        $this->layout = null;
        $guardFiles_table = TableRegistry::get("GuardFiles");
        $guardFilesAttachment_table = TableRegistry::get("GuardFileAttachments");


         $start = isset($this->request->data['start']) ? intval($this->request->data['start']) : 0;
         $len = isset($this->request->data['length']) ? intval($this->request->data['length']) : 500;
         $page = ($start / $len) + 1;

         $employee_office = $this->getCurrentDakSection();

         $data = [];
        if ($this->request->is('ajax')) {
            $conditions = [];
            if(!empty($this->request->data['guard_file_category_id'])){
                $conditions['guard_file_category_id'] = $this->request->data['guard_file_category_id'];
            }

            $totalRec = $guardFiles_table->getAll(0, 0, $employee_office)->where($conditions)->toArray();
            $guardFilesrecord = $guardFiles_table->getAll($page, $len, $employee_office)->where($conditions)->toArray();

            $showattachments = isset($this->request->query['attachment']) && $this->request->query('attachment') == 0?false:true;
            $isGuardFilePage = isset($this->request->query['guardFilePage']) && $this->request->query('guardFilePage') == 1?true:false;
            if (!empty($guardFilesrecord)) {
                $i = 0;
                foreach ($guardFilesrecord as $key => $record) {
                    $totalAttachment = $guardFilesAttachment_table->getAttachments($record['id']);

                    $attachents = '';

                    if(!empty($totalAttachment)){
                        foreach($totalAttachment as $k=>$at){
                            $attachents.= '<a class="showDetailAttach btn btn-primary btn-sm" title="দেখুন" data-title="'. h($record['name_bng']) .'" style="cursor:pointer" href="'.$at['id'] . '/guard-attachment/' . $record['id'] .'"><i class="fa fa-eye"></i></a>'.(($isGuardFilePage==true && $record['office_unit_organogram_id'] == $employee_office['office_unit_organogram_id']) ?'&nbsp;<button title="মুছুন" class="btn btn-primary btn-sm red" onclick =deleteGuardFile("'.$record['id'].'")  ><span class="glyphicon glyphicon-remove"></span></button>': '' ).($showattachments?('<button title="গার্ড ফাইল রেফারেন্স" class="btn btn-success btn-sm tagfile"><i class="fa fa-paperclip"></i></button>'):'');
                        }
                    }
                    $i++;

                    $data[]=  [
                        "<div class='text-center'>" . Number::format($i) . "</div>",
                        "<div class='text-left'>" . h($record['category_name']) . "</div>",
                        "<div class='text-left'>" . h($record['name_bng']) . "</div>",
                        "<div class='text-center'>" . $attachents . "</div>"
                    ];
                }
                $json_data = array(
                    "draw" => intval($this->request->data['draw']),
                    "recordsTotal" => count($totalRec),
                    "recordsFiltered" => count($totalRec),
                    "data" => $data
                );
                $this->response->body(json_encode($json_data));
            }else{
                $json_data = array(
                    "draw" => intval($this->request->data['draw']),
                    "recordsTotal" => 0,
                    "recordsFiltered" => 0,
                    "data" => []
                );
                $this->response->body(json_encode($json_data));
            }

        }
        $this->response->type('application/json');
        return $this->response;
    }
    public function deleteGuardFile($id)
    {
            $employee_office = $this->getCurrentDakSection();
            $guardFiles_table = TableRegistry::get("GuardFiles");
            $guardFilerecord = $guardFiles_table->get($id);
            if (!empty($id) || !empty($guardFilerecord)) {
                if (!empty($guardFilerecord['office_unit_organogram_id']) && $guardFilerecord['office_unit_organogram_id'] == $employee_office['office_unit_organogram_id']) {


                    $guardFilesAttachment_table = TableRegistry::get("GuardFileAttachments");

                    $guardFileAttachmentrecord = $guardFilesAttachment_table->find()->where(['guard_file_id' => $id])->first();

                    if (file_exists(FILE_FOLDER_DIR . $guardFileAttachmentrecord['file_name'])) {
                        unlink(FILE_FOLDER_DIR . $guardFileAttachmentrecord['file_name']);
                    }
                    $guardFilesAttachment_table->delete($guardFileAttachmentrecord);
                    $guardFiles_table->delete($guardFilerecord);

                    $this->Flash->success('গার্ড ফাইল সফলভাবে মুছে দেওয়া হয়েছে।');
                    $this->redirect(['action' => 'guardFiles']);

                } else {
                    $this->Flash->error('দুঃখিত! আপনি অনুমতিপ্রাপ্ত নন।');
                    $this->redirect(['action' => 'guardFiles']);
                }
            } else {
                $this->Flash->error('দুঃখিত! গার্ড ফাইল পাওয়া যায়নি। ');
                $this->redirect(['action' => 'guardFiles']);
            }
    }
    public function guardFileApi($type=''){
        $employee_office = $this->getCurrentDakSection();

        $OfficesTable = TableRegistry::get('Offices');
        $GuardFileApiDatasTable = TableRegistry::get('GuardFileApiDatas');
        if(!empty($type)){
            $data = $GuardFileApiDatasTable->find()->where(['type' => $type])->toArray();
            if(!empty($data)){
                $result = json_encode(['status' => 'success', 'data' => $data]);

                $this->response->body($result);
                $this->response->type('application/json');
                return $this->response;
            } else {
                $result = json_encode(['status' => 'error']);
                $this->response->body($result);
                $this->response->type('application/json');
                return $this->response;
            }
        }
        else {
            $OfficeDomainTable = TableRegistry::get('OfficeDomains');

            $office_domain = $OfficeDomainTable->getApiPortalDomainbyOffice($employee_office['office_id']);
            $data = $GuardFileApiDatasTable->find()->where(['subdomain' => $office_domain])->toArray();

            if (empty($data)) {
                $office_info = $OfficesTable->get($employee_office['office_id']);
                if (!empty($office_info['parent_office_id'])) {
                    $ids[] = $office_info['parent_office_id'];
                    $office_domain = $OfficeDomainTable->getApiPortalDomainbyOffice($office_info['parent_office_id']);
                    $subd[] = $office_domain;
                    $data = $GuardFileApiDatasTable->find()->where(['subdomain' => $office_domain])->toArray();
                } else {
                    $result = json_encode(['status' => 'error']);
                    $this->response->body($result);
                    $this->response->type('application/json');
                    return $this->response;
                }
            }

            while (empty($data)) {
                $office_info = $OfficesTable->get($office_info['parent_office_id']);
                if (!empty($office_info['parent_office_id'])) {
                    $ids[] = $office_info['parent_office_id'];
                    $office_domain = $OfficeDomainTable->getApiPortalDomainbyOffice($office_info['parent_office_id']);
                    $subd[] = $office_domain;
                    $data = $GuardFileApiDatasTable->find()->where(['subdomain' => $office_domain])->toArray();
                } else {
                    $result = json_encode(['status' => 'error']);
                    $this->response->body($result);
                    $this->response->type('application/json');
                    return $this->response;
                }
            }


            $result = json_encode(['status' => 'success', 'data' => $data]);

            $this->response->body($result);
            $this->response->type('application/json');
            return $this->response;
        }
    }
    public function getPortalTypesByLayer($id){
        $guard_file_api_data_table = TableRegistry::get('GuardFileApiDatas');
        if(!empty($id)){
            $result = $guard_file_api_data_table->find('list', [
                'keyField' => 'type',
                'valueField' => 'type'
            ])->where(['layer_id'=>$id])
            ->toArray();
        }
        $this->response->body(json_encode($result));
        $this->response->type('application/json');
        return $this->response;
    }
}