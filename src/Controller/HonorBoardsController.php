<?php
/**
 * Created by PhpStorm.
 * User: sayeed
 * Date: 3/20/19
 * Time: 5:33 PM
 */

namespace App\Controller;

use Cake\Event\Event;
use Cake\ORM\TableRegistry;

class HonorBoardsController extends ProjapotiController
{
    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
    }

    public function index () {
        $get_dak_current_section = $this->getCurrentDakSection();
        $honor_boards_table = TableRegistry::get('HonorBoards');
        $honor_boards = $honor_boards_table->find()->where(['unit_id' => $get_dak_current_section['office_unit_id']])->order(['join_date' => 'ASC'])->toArray();
        $this->set(compact('honor_boards', 'get_dak_current_section'));
    }

    public function add () {
        $OfficeInchargeTypesTable = TableRegistry::get('OfficeInchargeTypes');
        $officeInchargeTypes = $OfficeInchargeTypesTable->find('list',['keyField' => 'name_bng','valueField' => 'name_bng'])->toArray();

        if ($this->request->is('post')) {
            $honorBoardsTable = TableRegistry::get('HonorBoards');
            $this->request->data['join_date'] = date('Y-m-d', strtotime($this->request->data['join_date']));
            if (empty($this->request->data['release_date'])) {
                $this->request->data['release_date'] = null;
            } else {
                $this->request->data['release_date'] = date('Y-m-d', strtotime($this->request->data['release_date']));
            }

            if ($this->request->data['release_date'] == null) {
                $honorBoardsCheck = $honorBoardsTable->find()->where(['unit_id' => $this->request->data['unit_id'], 'join_date <' => date('Y-m-d'), 'release_date >' => $this->request->data['join_date']])->count();
                if ($honorBoardsCheck == 0) {
                    $honorBoardsCheck = $honorBoardsTable->find()->where(['unit_id' => $this->request->data['unit_id'], 'join_date <' => date('Y-m-d'), 'release_date is null'])->count();
                }
            } else {
                $honorBoardsCheck = $honorBoardsTable->find()->where(['unit_id' => $this->request->data['unit_id'], 'join_date <' => $this->request->data['release_date']])->where(['OR' => ['release_date >' => $this->request->data['join_date'], 'release_date IS NULL']])->count();
            }
            if ($honorBoardsCheck == 0) {
                $honorBoardsEntities = $honorBoardsTable->newEntity($this->request->data);

                if ($honorBoardsTable->save($honorBoardsEntities)) {
                    $this->response->type('application/json');
                    $this->response->body(json_encode(['status' => 'success']));
                    return $this->response;
                } else {
                    $this->response->type('application/json');
                    $this->response->body(json_encode(['status' => 'error', 'msg' => 'দুঃখিত! কিছুক্ষণ পর পুনরায় চেষ্টা করুন']));
                    return $this->response;
                }
            } else {
                $this->response->type('application/json');
                $this->response->body(json_encode(['status' => 'error', 'msg' => 'তারিখের সীমা পূর্বে ব্যবহার করা হয়েছে।']));
                return $this->response;
            }
        }
        $get_dak_current_section = $this->getCurrentDakSection();
        if ($get_dak_current_section['is_admin'] == 1) {
            $office_unit_table = TableRegistry::get('OfficeUnits');
            $office_units = $office_unit_table->find('list', ['keyField' => 'id', 'valueField' => 'unit_name_bng'])->where(['office_id' => $get_dak_current_section['office_id'], 'active_status' => 1])->toArray();

            $selected_unit_id = '';
            if (isset($this->request->query['unit_id'])) {
                $selected_unit_id = $this->request->query['unit_id'];

                $employee_offices_table = TableRegistry::get('EmployeeOffices');
                $employee_offices = $employee_offices_table->find()->select(['EmployeeOffices.employee_record_id', 'EmployeeRecords.name_bng', 'EmployeeOffices.designation'])->join([
                    "EmployeeRecords" => [
                        'table' => 'employee_records',
                        'type' => 'left',
                        'conditions' => ['EmployeeOffices.employee_record_id = EmployeeRecords.id']
                    ]
                ])->where(['EmployeeOffices.office_unit_id' => $selected_unit_id])->toArray();
            }
            $this->set(compact('office_units', 'selected_unit_id', 'employee_offices', 'officeInchargeTypes'));
        }
    }

    public function name_search() {
        if (isset($this->request->data['unit_id'])) {
            $selected_unit_id = $this->request->data['unit_id'];
            $name_key = $this->request->data['name_key'];

            $employee_offices_table = TableRegistry::get('EmployeeOffices');
            $employee_offices = $employee_offices_table->find()->select(['EmployeeOffices.employee_record_id', 'EmployeeRecords.name_bng', 'EmployeeOffices.designation'])->join([
                "EmployeeRecords" => [
                    'table' => 'employee_records',
                    'type' => 'left',
                    'conditions' => ['EmployeeOffices.employee_record_id = EmployeeRecords.id']
                ]
            ])->where(['EmployeeOffices.office_unit_id' => $selected_unit_id, 'EmployeeRecords.name_bng LIKE' => '%'.$name_key.'%'])->group(['EmployeeOffices.employee_record_id'])->order(['EmployeeOffices.id' => 'DESC'])->toArray();

            $this->response->type('application/json');
            $this->response->body(json_encode($employee_offices));
            return $this->response;
        }
    }

    public function organogram_search() {
        if (isset($this->request->data['unit_id'])) {
            $selected_unit_id = $this->request->data['unit_id'];
            $organogram_key = $this->request->data['organogram_key'];

            $office_unit_organograms_table = TableRegistry::get('OfficeUnitOrganograms');
            $office_unit_organograms = $office_unit_organograms_table->find()->select(['id', 'designation_bng'])->where(['office_unit_id' => $selected_unit_id, 'designation_bng LIKE' => '%'.$organogram_key.'%'])->toArray();

            $this->response->type('application/json');
            $this->response->body(json_encode($office_unit_organograms));
            return $this->response;
        }
    }

    public function loadHonorBoard() {
        $selected_unit_id = $this->request->data['unit_id'];
        $honor_boards_table = TableRegistry::get('HonorBoards');
        $honor_boards = $honor_boards_table->find()->where(['unit_id' => $selected_unit_id])->order(['join_date' => 'ASC'])->toArray();


        $this->response->type('application/json');
        $this->response->body(json_encode($honor_boards));
        return $this->response;
    }

    public function delete() {
        if (isset($this->request->data['unit_id']) && isset($this->request->data['data_id'])) {
            $honorBoardsTable = TableRegistry::get('HonorBoards');
            $honorBoards = $honorBoardsTable->deleteAll([
                'unit_id' => $this->request->data['unit_id'],
                'id' => $this->request->data['data_id']
            ]);

            if ($honorBoards) {
                $this->response->type('application/json');
                $this->response->body(json_encode(['status' => 'success']));
                return $this->response;
            } else {
                $this->response->type('application/json');
                $this->response->body(json_encode(['status' => 'error']));
                return $this->response;
            }
        }
    }


}