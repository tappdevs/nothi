<?php

namespace App\Controller;

use App\Controller\ProjapotiController;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\TableRegistry;
use Cake\Cache\Cache;
use Cake\I18n\Number;
use Cake\Datasource\ConnectionManager;
use Cake\I18n\Time;

class LoginPageSettingsController extends ProjapotiController {
    /*
     * Nothi Masters
     */

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $this->Auth->allow(['NoticePopup']);
    }
    public function addLoginMenuNotice(){
        $user = $this->Auth->user();
        if(empty($user['user_role_id']) || $user['user_role_id'] > 2){
            $this->redirect(['controller' => 'users', 'action' => 'login']);
        }
        $table_instance_lps = TableRegistry::get('LoginPageSettings');
        $list = $table_instance_lps->find('all')->toArray();


        if($this->request->is('post')){
            if(!empty($this->request->data['description'])) {
                if(!empty($this->request->data['attachments'])){
                    $this->request->data['attachments'] =json_encode(jsonA($this->request->data['attachments']));

                }
                $notice = $table_instance_lps->newEntity($this->request->data);
                $table_instance_lps->save($notice);
                $all_data['other'] = $table_instance_lps->find()->where(['type' => 2])->order('id DESC')->limit(2)->hydrate(true)->toArray();
                $all_data['notice'] = $table_instance_lps->find()->where(['type' => 1])->hydrate(true)->toArray();
                $all_data['report'] = $table_instance_lps->find()->where(['type' => 3])->hydrate(true)->toArray();
                Cache::delete('login_page_settings','loginSettingsCache');
                Cache::write('login_page_settings', $all_data, 'loginSettingsCache');
                $this->Flash->success("লগইন পেইজ সেটিংস সংরক্ষণ করা হয়েছে।");
                $this->redirect('/addLoginMenuNotice');
            }
            $this->Flash->success("লগইন পেইজ সেটিংস সংরক্ষণ করা সম্ভব হয়নি।");
            $this->redirect('/addLoginMenuNotice');
        }
        $this->set('list',$list);

    }

    public function editLoginMenuNotice($id){
        $user = $this->Auth->user();
        if(empty($user['user_role_id']) || $user['user_role_id'] >= 2 || empty($id)){
            $this->redirect(['controller' => 'users', 'action' => 'login']);
        }
        $table_instance_lps = TableRegistry::get('LoginPageSettings');
        $pageSetting = $table_instance_lps->get($id);
        if($this->request->is('post')){
//            pr(count(json_decode($this->request->data['attachments'],true))); die;
            $requested_attachments =jsonA($this->request->data['attachments']);

            if((count($requested_attachments)>0)){
                $prev = json_decode($pageSetting['attachments'],true);
                foreach ($requested_attachments as $item){
                    array_push($prev,$item);
                }
                $this->request->data['attachments'] = json_encode($prev);

            }
            $pageSetting = $table_instance_lps->patchEntity($pageSetting, $this->request->data);
            $table_instance_lps->save($pageSetting);

            $all_data['other'] = $table_instance_lps->find()->where(['type' => 2])->order('id DESC')->limit(2)->hydrate(true)->toArray();
            $all_data['notice'] = $table_instance_lps->find()->where(['type' => 1])->hydrate(true)->toArray();
            Cache::delete('login_page_settings','loginSettingsCache');
            Cache::write('login_page_settings', $all_data, 'loginSettingsCache');
            $this->Flash->success("লগইন পেইজ সেটিংস হালনাগাদ করা হয়েছে।");
            $this->redirect('/addLoginMenuNotice');

        }

        $this->set('pageSetting',$pageSetting);

    }
    public function delete(){
        $user = $this->Auth->user();
        if(empty($user['user_role_id']) || $user['user_role_id'] >= 2 || empty($id)){
            $this->redirect(['controller' => 'users', 'action' => 'login']);
        }
        if ($this->request->is(['post', 'ajax']) && isset($this->request->data['id'])) {
            $table_instance_lps = TableRegistry::get('LoginPageSettings');
            $pageSetting = $table_instance_lps->get($this->request->data['id']);
            $attachment = json_decode($pageSetting['attachments'],true);
            if(count($attachment) > 0){
                foreach ($attachment as $item){
                    $file_name = explode('content/',$item['href']);
                    unlink(FILE_FOLDER_DIR.$file_name[1]);
                }

            }
           $table_instance_lps->deleteAll(['id'=>$this->request->data['id']]);
            $all_data['other'] = $table_instance_lps->find()->where(['type'=>2])->order('id DESC')->limit(2)->hydrate(true)->toArray();
            $all_data['notice'] = $table_instance_lps->find()->where(['type'=>1])->hydrate(true)->toArray();
            Cache::delete('login_page_settings','loginSettingsCache');
            Cache::write('login_page_settings', $all_data, 'loginSettingsCache');
            $this->Flash->success('মুছে ফেলা হয়েছে');
        }
        die;
    }
    public function save_login_notice_attachment_refs()
    {
        $user = $this->Auth->user();
        if(empty($user['user_role_id']) || $user['user_role_id'] >= 2 || empty($id)){
            $this->redirect(['controller' => 'users', 'action' => 'login']);
        }

        if ($this->request->is('post')) {
            $data = [
                'status' => 'error',
            ];
            $table = TableRegistry::get('LoginNoticeAttachments');
            $login_notice_attachment_refs = $table->newEntity($this->request->data);
            $filePath = explode('content/',
                htmlspecialchars($this->request->data['file_name']), 2);
            empty($filePath[1])?($filePath[1]=$filePath[0]):($filePath[1]=$filePath[1]);

            $file = explode('?token=',$filePath[1]);
            if(!empty($file[0])){
                $file = $file[0];
            } else {
                $file = $this->request->data['file_name'];
            }
            $login_notice_attachment_refs->file_name = $file;
            $login_notice_attachment_refs->attachment_type = $this->request->data['type'];
            $login_notice_attachment_refs->user_file_name = $this->request->data['user_file_name'];
            if ($table->save($login_notice_attachment_refs)) {
                $id = $login_notice_attachment_refs->id;
                $data = [
                    'status' => 'success',
                    'id' => base64_encode($id),
                ];
            }
            $this->response->body(json_encode($data));
            $this->response->type('application/json');
            return $this->response;
        }
        echo 'Invalid Request';
        die;
    }
    public function NoticePopup($id, $type='notice-ref')
    {

        $this->view = 'file_popup';
        $this->layout = null;
        $table = TableRegistry::get('LoginNoticeAttachments');
        $data = $table->get(base64_decode($id));
        $this->set('data', $data);
    }

    public function monthlyReport(){
        $user = $this->Auth->user();
        if(empty($user['user_role_id']) || $user['user_role_id'] > 2){
            $this->redirect(['controller' => 'users', 'action' => 'login']);
        }
        $table_instance_lps = TableRegistry::get('MonthlyReports');
        $list = $table_instance_lps->find('all')->order(['rank asc'])->toArray();

        $year = [];
        for ($i=2016; $i <= date('Y'); $i++) {
            $year[$i] = enTobn($i);
        }
        $month = ['01' => 'জানুয়ারী', '02' => 'ফেব্রুয়ারী', '03' => 'মার্চ', '04' => 'এপ্রিল', '05' => 'মে', '06' => 'জুন', '07' => 'জুলাই', '08' => 'আগস্ট', '09' => 'সেপ্টেম্বর', '10' => 'অক্টোবর', '11' => 'নভেম্বর', '12' => 'ডিসেম্বর'];
        if($this->request->is('post')){
            if(!empty($this->request->data['title'])) {
                if ($this->request->data['year'] > date('Y')) {
                    $this->Flash->error("দুঃখিত! ভবিষ্যৎ কোনো তারিখে ফাইল আপলোড করা যাবে না।");
                    return $this->redirect('/monthlyReport');
                }
                if ($this->request->data['year'] == date('Y') && $this->request->data['month'] > date('m')) {
                    $this->Flash->error("দুঃখিত! ভবিষ্যৎ কোনো তারিখে ফাইল আপলোড করা যাবে না।");
                    return $this->redirect('/monthlyReport');
                }
                $check_duplicate = $table_instance_lps->find()->where([
                    'title' => $this->request->data['title'],
                    'year' => $this->request->data['year'],
                    'month' => $this->request->data['month'],
                ])->first();
                if ($check_duplicate) {
                    $this->Flash->error("দুঃখিত! একই নামে একই সময়ে একাধিক ফাইল যুক্ত করা যাবে না।");
                    return $this->redirect('/monthlyReport');
                } else {
                    if (!empty($this->request->data['attachments'])) {
                        $this->request->data['attachment_path'] = $this->request->data['attachments'];
                        $notice = $table_instance_lps->newEntity($this->request->data);
                        $table_instance_lps->save($notice);
                        $this->Flash->success("সংরক্ষণ করা হয়েছে।");
                        return $this->redirect('/monthlyReport');
                    } else {
                        $this->Flash->error("সংযুক্তি দেয়া হয়নি।");
                        return $this->redirect('/monthlyReport');
                    }
                }
            }
            $this->Flash->error("সংরক্ষণ করা সম্ভব হয়নি।");
            return $this->redirect('/monthlyReport');
        }
        $this->set(compact('list', 'year', 'month'));
    }
    public function efileRelatedReport(){
        $user = $this->Auth->user();
        if(empty($user['user_role_id']) || $user['user_role_id'] > 2){
            $this->redirect(['controller' => 'users', 'action' => 'login']);
        }
        $table_instance_lps = TableRegistry::get('efileRelatedReports');
        $list = $table_instance_lps->find()->order(['rank asc'])->toArray();

        if($this->request->is('post')){
            if(!empty($this->request->data['title'])) {
                $check_duplicate = $table_instance_lps->find()->where([
                    'title' => $this->request->data['title'],
                    'date_of_submit' => date('Y-m-d', strtotime($this->request->data['date_of_submit'])),
	                ])->first();
                if ($check_duplicate) {
                    $this->Flash->error("দুঃখিত! একই নামে একই সময়ে একাধিক ফাইল যুক্ত করা যাবে না।");
                    return $this->redirect('/efileRelatedReport');
                } else {
                    if (!empty($this->request->data['attachments'])) {
                        $this->request->data['attachment_path'] = $this->request->data['attachments'];
                        $this->request->data['date_of_submit'] = date('Y-m-d', strtotime($this->request->data['date_of_submit']));
                        $notice = $table_instance_lps->newEntity($this->request->data);
                        $table_instance_lps->save($notice);
                        $this->Flash->success("সংরক্ষণ করা হয়েছে।");
                        return $this->redirect('/efileRelatedReport');
                    } else {
                        $this->Flash->error("সংযুক্তি দেয়া হয়নি।");
                        return $this->redirect('/efileRelatedReport');
                    }
                }
            }
            $this->Flash->error("সংরক্ষণ করা সম্ভব হয়নি।");
            return $this->redirect('/efileRelatedReport');
        }
        $this->set(compact('list'));
    }

    public function downloadContentFile() {
        //$path = trim($this->request->query['file'], 'content/');
        $file_name = $this->request->query['name'];
		$path = str_replace('content/', '', $this->request->query['file']);
        return downloadContentFile($path, $file_name);
    }

    public function monthlyReportReOrder() {
        $table_instance = TableRegistry::get('MonthlyReports');
        foreach($this->request->data['data'] as $key => $value) {
            $currentitem = $table_instance->get($value);
            $currentitem->rank = $key+1;
            $table_instance->save($currentitem);
        }
        $this->response->body(json_encode(['status'=>'success']));
        $this->response->type('application/json');
        return $this->response;
    }
    public function eFileRelatedReportReOrder() {
        $table_instance = TableRegistry::get('efileRelatedReports');
        foreach($this->request->data['data'] as $key => $value) {
            $currentitem = $table_instance->get($value);
            $currentitem->rank = $key+1;
            $table_instance->save($currentitem);
        }
        $this->response->body(json_encode(['status'=>'success']));
        $this->response->type('application/json');
        return $this->response;
    }

    public function monthlyReportUser() {
        $table_instance_lps = TableRegistry::get('MonthlyReports');
        $yearListRaw = $table_instance_lps->find('list', ['keyField'=>'year', 'valueField'=>'year'])->distinct(['year']);
        $yearList = [];
        foreach ($yearListRaw as $key => $value) {
            $yearList[$key] = enTobn($value);
        }
        if($this->request->is('post')){
            $year = $this->request->data['year'];
            $month = $this->request->data['month'];
        } else {
            $year = date('Y');
            $month = date('m');
        }
        $list = $table_instance_lps->find()->where(['year'=>$year, 'month'=>$month])->order(['rank asc'])->toArray();
        $this->set(compact('list', 'year', 'month', 'yearList'));
    }
    public function efileRelatedReportUser() {
        $table_instance_lps = TableRegistry::get('efileRelatedReports');
        $list = $table_instance_lps->find()->order(['rank asc'])->toArray();
        $this->set(compact('list'));
    }

    public function monthlyReportDelete(){
        $user = $this->Auth->user();
        if(empty($user['user_role_id']) || $user['user_role_id'] >= 2 || empty($id)){
            $this->redirect(['controller' => 'users', 'action' => 'login']);
        }
        if ($this->request->is(['post', 'ajax']) && isset($this->request->data['id'])) {
            $table_instance_lps = TableRegistry::get('MonthlyReports');
            $pageSetting = $table_instance_lps->get($this->request->data['id']);
            $attachment = $pageSetting['attachment_path'];
            $file_name = explode('content/', $attachment );
            unlink(FILE_FOLDER_DIR.$file_name[1]);

            $table_instance_lps->deleteAll(['id'=>$this->request->data['id']]);
            $this->Flash->success('মুছে ফেলা হয়েছে');
        }
        die;
    }
    public function efileRelatedReportDelete(){
        $user = $this->Auth->user();
        if(empty($user['user_role_id']) || $user['user_role_id'] >= 2 || empty($id)){
            $this->redirect(['controller' => 'users', 'action' => 'login']);
        }
        if ($this->request->is(['post', 'ajax']) && isset($this->request->data['id'])) {
            $table_instance_lps = TableRegistry::get('efileRelatedReports');
            $pageSetting = $table_instance_lps->get($this->request->data['id']);
            $attachment = $pageSetting['attachment_path'];
            $file_name = explode('content/', $attachment );
            unlink(FILE_FOLDER_DIR.$file_name[1]);

            $table_instance_lps->deleteAll(['id'=>$this->request->data['id']]);
            $this->Flash->success('মুছে ফেলা হয়েছে');
        }
        die;
    }
}