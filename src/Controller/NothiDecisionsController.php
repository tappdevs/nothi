<?php
namespace App\Controller;

use App\Controller\ProjapotiController;
use App\Model\Table;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\View\CellTrait;
use Cake\I18n\I18n;
use Cake\I18n\Time;
use Cake\I18n\Number;
use Cake\Datasource\ConnectionManager;

class NothiDecisionsController extends ProjapotiController
{
	
   public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $this->loadComponent('Paginator');
       
    }
    public function index() {
        $table=  TableRegistry::get('NothiDecisionsMig');
        $table_employee=  TableRegistry::get('NothiDecisionEmployees');
        $employee=$this->getCurrentDakSection();
        
        if(empty($employee['officer_id']) && ($this->Auth->user('user_role_id')<= 2)){
            $employee['officer_id']=0;
        }
        $status_data = $table_employee->find('list',
            ['keyField' => 'nothi_decision_id', 'valueField' => 'nothi_decision_id'])->where(['employee_id'=>$employee['officer_id']])->toArray();
        $data = $table->GetNothiDecisions($employee['officer_id'])->toArray();

        $this->set('status_data', $status_data);
        
        $this->set('employee',$employee);
        try{
        $this->set('data',$data);
        }catch (NotFoundException $e) {
            $this->redirect(['action'=>'index']);
        }
    }

    public function updateStatus(){
        if ($this->request->is(['post', 'ajax']) && isset($this->request->data['ids'])) {
            $employee = $this->getCurrentDakSection();
            $table_employee    = TableRegistry::get('NothiDecisionEmployees');
            $admin = false;
            if(empty($employee['officer_id']) && ($this->Auth->user('user_role_id')<= 2)){
                $employee['officer_id']=0;
                $admin= true;
            }
            $table_employee->deleteAll(['employee_id'=>$employee['officer_id']]);
            $datas=[];
            foreach ($this->request->data['ids'] as $id){

                $data['nothi_decision_id']= $id;
                $data['employee_id']  = $employee['officer_id'];
                $data['created_by']  = $this->Auth->user('id');
                $data['modified_by']  = $this->Auth->user('id');
                $datas[]=$data;

            }
            $all_data = $table_employee->newEntities($datas);
            foreach ($all_data as $all_data){
                $table_employee->save($all_data);
            }
            if($admin == true){
                $table    = TableRegistry::get('NothiDecisionsMig');
                $table->updateAll(['status'=>1],['id IN'=>$this->request->data['ids'],'creator'=>0]);
                $table->updateAll(['status'=>0],['id NOT IN'=>$this->request->data['ids'],'creator'=>0]);
            }
            $this->Flash->success('সংরক্ষিত করা হয়েছে');
        }
        die;
    }

    public function add()
    {
        if($this->request->is(['post','ajax']) && isset($this->request->data['text'])){
            $employee=$this->getCurrentDakSection();
            $text= htmlspecialchars($this->request->data['text']);

            $table_employee    = TableRegistry::get('NothiDecisionEmployees');
            $table=  TableRegistry::get('NothiDecisionsMig');
            $data=$table->newEntity();
            $data->decisions=$text;
            $data->status=1;
            $data->created_by      = $this->Auth->user('id');
            $data->modified_by     = $this->Auth->user('id');
            if(empty($employee['officer_id']) && ($this->Auth->user('user_role_id')<= 2)){
                $employee['officer_id']=0;
            }
            $data->creator         = $employee['officer_id'];
            if($decision = $table->save($data)){
                $dak_action_employee = $table_employee->newEntity();
                $dak_action_employee->nothi_decision_id= $decision->id;
                $dak_action_employee->employee_id  = $employee['officer_id'];
                $dak_action_employee->created_by  = $this->Auth->user('id');
                $dak_action_employee->modified_by  = $this->Auth->user('id');
                $table_employee->save($dak_action_employee);

            $this->Flash->success('সংরক্ষিত করা হয়েছে');
            }
        }
       die; 
    }
    
    public function update()
    {
        if($this->request->is(['post','ajax']) && isset($this->request->data['text']) && isset($this->request->data['id'])){
            $employee=$this->getCurrentDakSection();
            $text= htmlspecialchars($this->request->data['text']);
           
            $table=  TableRegistry::get('NothiDecisionsMig');
            $data=$table->get(intval($this->request->data['id']));
            $data->decisions=$text;
            $data->status=1;
            if(empty($employee['officer_id']) && ($this->Auth->user('user_role_id')<= 2)){
                $employee['officer_id']=0;
            }
            $data->creator         = $employee['officer_id'];
            if($table->save($data)){
                $this->Flash->success('সংশোধন করা হয়েছে');
            }
        }
       die; 
    }
      public function delete()
    {
        if($this->request->is(['post','ajax']) && isset($this->request->data['id'])){
            $employee=$this->getCurrentDakSection();
            $table=  TableRegistry::get('NothiDecisionsMig');
            $table_employee    = TableRegistry::get('NothiDecisionEmployees');


            $data=$table->get(intval($this->request->data['id']));
            if($data->creator == $employee['officer_id']){
                    if($table->delete($data) && $table_employee->deleteAll(['nothi_decision_id'=>$this->request->data['id']])){
                    $this->Flash->success('মুছে ফেলা হয়েছে');
                }else{
                     $this->Flash->error(' দুঃখিত পুনরায় চেষ্টা করুন ');
                }
            }else{
                     $this->Flash->error(' দুঃখিত আপনি অনুমতিপ্রাপ্ত নয়। ');
                }
        }
       die; 
    }

  }