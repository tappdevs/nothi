<?php

namespace App\Controller;

use App\Controller\ProjapotiController;
use App\Model\Table;
use Aura\Intl\Exception;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\View\CellTrait;
use Cake\Datasource\ConnectionManager;
use Cake\I18n\Time;
use Cake\Cache\Cache;

class NothiMasterMovementsController extends ProjapotiController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);

        $this->Auth->allow(['ApiForwardNothiMaster', 'postApiForwardNothiMaster', 'finishNothiMaster']);
    }

    public function forwardNothiMaster()
    {
        $this->digitalSigantureUrlChecker();

        $session = $this->request->session();
        $employee = $session->read('logged_employee_record');
        $employee_office = $this->getCurrentDakSection();

        $username = $this->Auth->user('username');
        $options['token'] = sGenerateToken(['file' => $username], ['exp' => time() + 60 * 300]);
        $data = $this->getSignature($username, 1, 0, true, null, $options);
        $user_id = $this->Auth->user('id');

        $request_success = 0;
        $response = [
            'status' => 'error',
            'msg' => 'Something went wrong.'
        ];

        if ($data == false) {
            $response['msg'] = 'দুঃখিত! আপনার স্বাক্ষর পাওয়া যায়নি। অনুগ্রহ করে সিস্টেমে স্বাক্ষর আপলোড করুন। ধন্যবাদ।';
            goto rtn;
        }
        if ($this->request->is('ajax') && !empty($this->request->data['nothi_master_id'])) {
            if (!empty($this->request->data['to_office_id']) && !empty($this->request->data['to_officer_designation_id']) && !empty($this->request->data['to_office_unit_id'])) {
            } else {
                $response['msg'] = 'দুঃখিত! প্রেরকের তথ্য পাওয়া যায়নি';
                goto rtn;
            }
            // need to check if receiver office DB connection is valid or not

            $nothi_office = intval($this->request->data['nothi_office']);
            $otherNothi = false;
            if ($nothi_office != 0 && $nothi_office != $employee_office['office_id']) {
                $otherNothi = true;

                $this->switchOffice($nothi_office, 'MainNothiOffice');
                $checkDBConnection = $this->checkCurrentOfficeDBConnection();
                if (empty($checkDBConnection) || (!empty($checkDBConnection['status']) && $checkDBConnection['status'] == 'error')) {
                    $response['msg'] = 'দুঃখিত! ডাটাবেস (আইডিঃ ' . $nothi_office . ') সংযোগ সম্ভব হয়নি।';
                    goto rtn;
                }
            } else {
                $nothi_office = $employee_office['office_id'];
            }

            $this->set('nothi_office', $nothi_office);
            $this->set(compact('otherNothi'));

            if ($employee_office['default_sign'] == 1) {
                if (empty($this->request->data['soft_token'])) {
                    $response['msg'] = 'দুঃখিত! টোকেনটি সঠিক নয়।';
                    goto rtn;
                }
            }

            //It is needed as new entry will be given in nothi master user (officewise). we have part and nothi office but not nothi master id
            $real_nothi_master_id = 0;
            try {
                $conn = ConnectionManager::get('default');
                $conn->begin();

                $nothi_part_id = intval($this->request->data['nothi_master_id']);
                $nothiMasterCurrentUserTable = TableRegistry::get('NothiMasterCurrentUsers');

                $nothiMastersCurrentUser = $nothiMasterCurrentUserTable->find()->where(['nothi_part_no' => $nothi_part_id,
                    'office_unit_organogram_id' => $employee_office['office_unit_organogram_id'],
                    'nothi_office' => $nothi_office])->first();

                if (empty($nothiMastersCurrentUser)) {
                    $response['msg'] = 'দুঃখিত! নথি পরবর্তী কার্যক্রমের জন্য প্রেরণ করা সম্ভব হচ্ছে না';
                    $response['reason'] = 'No data found';
                    goto rtn;

                } else {
                    $real_nothi_master_id = $nothiMastersCurrentUser['nothi_master_id'];
                    $is_finished = (isset($nothiMastersCurrentUser['is_finished']) ? $nothiMastersCurrentUser['is_finished'] : 0);
                }


                if ($nothiMastersCurrentUser['nothi_office'] != 0 && $nothiMastersCurrentUser['nothi_office']
                    != $employee_office['office_id'] && $nothi_office != $nothiMastersCurrentUser['nothi_office']
                ) {
                    $this->switchOffice($nothiMastersCurrentUser['nothi_office'], 'NothiOffice');
                }

                TableRegistry::remove('NothiParts');
                TableRegistry::remove('NothiMasterMovements');
                TableRegistry::remove('NothiNoteSignatures');
                TableRegistry::remove('NothiNotes');
                TableRegistry::remove('Potrojari');
                TableRegistry::remove('PotrojariVersions');
                TableRegistry::remove('NothiParts');
                TableRegistry::remove('NothiMasterPermissions');
                TableRegistry::remove('NothiPotroAttachments');

                $nothiPartsTable = TableRegistry::get('NothiParts');
                $nothiMovementsTable = TableRegistry::get('NothiMasterMovements');
                $nothiMasterPermissionTable = TableRegistry::get('NothiMasterPermissions');
                $nothiNoteSignaturesTable = TableRegistry::get('NothiNoteSignatures');
                $nothiNotesTable = TableRegistry::get('NothiNotes');
                $officeTable = TableRegistry::get('Offices');
                $potrojariTable = TableRegistry::get('Potrojari');
                $potrojariVersionTable = TableRegistry::get('PotrojariVersions');
                $nothiPotroAttachmentsTable = TableRegistry::get('NothiPotroAttachments');

                $nothiPartsInformation = $nothiPartsTable->get(intval($this->request->data['nothi_master_id']));
                $nothi_master_id = $nothiPartsInformation['nothi_masters_id'];
                $nothi_no = $nothiPartsInformation['nothi_no'];
                $subject = $nothiPartsInformation['subject'];
                $creator_unit_id = $nothiPartsInformation['office_units_id'];

                $hasAccess = $nothiMasterPermissionTable->hasAccessList($nothi_office,
                    $this->request->data['to_office_id'],
                    $this->request->data['to_office_unit_id'],
                    $this->request->data['to_officer_designation_id'],
                    $nothi_master_id, $nothi_part_id);

                if (empty($hasAccess)) {
                    $conn->rollback();
                    $response['msg'] = 'দুঃখিত! নথি পরবর্তী কার্যক্রমের জন্য প্রেরণ করা সম্ভব হচ্ছে না';
                    $response['reason'] = 'Don\'t have access';
                    goto rtn;
                }
                /*
                * Check If there is notes or not
                */
                $hasOnucched = $nothiNotesTable->checkEmptyNote($nothi_master_id, $nothi_part_id);
                if ($hasOnucched == 0) {
                    $conn->rollback();
                    $response['msg'] = 'কোন অনুচ্ছেদ না দিয়ে প্রেরণ করা সম্ভব নয়। দয়া করে নোটে অনুচ্ছেদ দিন';
                    goto rtn;
                }

                $nothiPartsInformation->modified = date("Y-m-d H:i:s");
                $nothiPartsTable->save($nothiPartsInformation);

                /*
                 * Time to check digital signature
                 * if soft token = - 1 skip
                */
                if ($employee_office['default_sign'] > 0 && isset($this->request->data['soft_token']) && $this->request->data['soft_token'] != -1) {
                    $allNotesToSign = $nothiNotesTable->find('list', ['keyField' => 'id', 'valueField' => 'note_description'])->where(['note_status' => 'DRAFT', 'nothi_part_no' => $this->request->data['nothi_master_id'], 'office_organogram_id' => $employee_office['office_unit_organogram_id']])->toArray();
                    $this->loadComponent('DigitalSignatureRelated');
                    $res = $this->DigitalSignatureRelated->signOnucched($allNotesToSign, $this->request->data['soft_token'], $this->request->data['nothi_master_id'], $employee_office['default_sign'], $employee_office, $nothi_office);
                    if ($res['status'] == 'error') {
                        $conn->rollback();
                        $response['msg'] = 'দুঃখিত! ডিজিটাল সিগনেচার করা সম্ভব হয়নি। কারণঃ ' . $res['msg'];
                        goto rtn;
                    } else {
                        $note_ids = $res['note_ids'];
                    }
                }
                //check for draft potro

                //"nothi_part_no = " . $nothi_part_id . " AND (potro_status = 'Draft' OR  potro_status = 'SummaryDraft')"
                $searchForDraft = $potrojariTable->getInfo([
                    'nothi_part_no' => $nothi_part_id,
                    'potro_status IN' => ['Draft', 'SummaryDraft']
                ])->toArray();

                $hasSummary = 0;
                if (!empty($searchForDraft)) {
                    foreach ($searchForDraft as $key => $drafts) {
                        $draftVersion = $potrojariVersionTable->newEntity();
                        $draftVersion->potrojari_id = $drafts->id;
                        $draftVersion->nothi_master_id = $drafts->nothi_master_id;
                        $draftVersion->nothi_part_no = $drafts->nothi_part_no;
                        $draftVersion->updated_content = $drafts->potro_description;
                        $draftVersion->office_id = $employee_office['office_id'];
                        $draftVersion->officer_id = $employee_office['officer_id'];
                        $draftVersion->officer_name = $employee_office['officer_name'];
                        $draftVersion->officer_unit_id = $employee_office['office_unit_id'];
                        $draftVersion->office_organogram_id = $employee_office['office_unit_organogram_id'];
                        $draftVersion->designation_label = $employee_office['designation_label'];

                        if ($drafts['potro_status'] == 'SummaryDraft') {
                            $hasSummary = 1;
                        }
                        if(!$potrojariVersionTable->save($draftVersion)){
                            $conn->rollback();
                            $response['msg'] = __('Technical error happen');
                            $response['reason'] = 'Error Code: Nothi FORWARD - 0';
                            goto rtn;
                        }
                    }
                }

                //check for summary
                if (!$hasSummary) {
                    $hasSummary = $nothiPotroAttachmentsTable->find()->where(['is_summary_nothi' => 1, 'nothi_part_no' => $nothi_part_id])->count();
                }
                //movements
                $toOfficeName = $officeTable->get($this->request->data['to_office_id']);

                $first_move = $nothiMovementsTable->newEntity();
                $first_move->nothi_master_id = $nothi_master_id;
                $first_move->nothi_part_no = $nothi_part_id;
                $first_move->nothi_office = $nothi_office;
                $first_move->from_office_id = $employee_office['office_id'];
                $first_move->from_officer_id = $employee['personal_info']['id'];
                $first_move->from_office_unit_id = $employee_office['office_unit_id'];
                $first_move->from_officer_designation_id = $employee_office['office_unit_organogram_id'];
                $first_move->from_office_name = $employee_office['office_name'];
                $first_move->from_office_unit_name = $employee_office['office_unit_name'];
                $first_move->from_officer_name = $employee['personal_info']['name_bng'];
                $first_move->from_officer_designation_label = $employee_office['designation_label'] . (!empty($employee_office['incharge_label']) ? (" (" . $employee_office['incharge_label'] . ')') : '');

                $first_move->to_office_id = $this->request->data['to_office_id'];
                $first_move->to_office_name = (!empty($toOfficeName)
                    ? $toOfficeName['office_name_bng'] : '');
                $first_move->to_office_unit_id = $this->request->data['to_office_unit_id'];
                $first_move->to_office_unit_name = $this->request->data['to_office_unit_name'];
                $first_move->to_officer_id = $this->request->data['to_officer_id'];
                $first_move->to_officer_name = $this->request->data['to_officer_name'];
                $first_move->to_officer_designation_id = $this->request->data['to_officer_designation_id'];
                $first_move->to_officer_designation_label = $this->request->data['to_officer_designation_label'] . (!empty($this->request->data['to_officer_incharge_label']) ? (" (" . $this->request->data['to_officer_incharge_label'] . ')') : '');


                $first_move->priority = isset($this->request->data['priority']) ? $this->request->data['priority'] : 0;
                $first_move->view_status = 0;
                $first_move->created_by = $user_id;
                $first_move->modified_by = $user_id;
                $first_move->created = date('Y-m-d H:i:s');
                $first_move->modified = date('Y-m-d H:i:s');

                if(!$nothiMovementsTable->save($first_move)){
                    $conn->rollback();
                    $response['msg'] = __('Technical error happen');
                    $response['reason'] = 'Error Code: Nothi FORWARD - 1';
                    goto rtn;
                }
                //end
                //change current user -- Extra -- previous_code
//                $nothiMastersCurrentUser->office_id = $this->request->data['to_office_id'];
//                $nothiMastersCurrentUser->office_unit_id = $this->request->data['to_office_unit_id'];
//                $nothiMastersCurrentUser->office_unit_organogram_id = $this->request->data['to_officer_designation_id'];
//                $nothiMastersCurrentUser->employee_id = $this->request->data['to_officer_id'];
//                $nothiMastersCurrentUser->forward_date = $nothiMastersCurrentUser->issue_date;
//                $nothiMastersCurrentUser->issue_date = date("Y-m-d H:i:s");
//                $nothiMastersCurrentUser->is_archive = 0;
//                $nothiMastersCurrentUser->is_finished = 0;
//                $nothiMastersCurrentUser->view_status = 0;
//                $nothiMastersCurrentUser->is_new = 0;
//
//                $nothiMasterCurrentUserTable->save($nothiMastersCurrentUser);
                //end -- Extra -- previous_code
                $hasAccess->visited = 1;

                if(!$nothiMasterPermissionTable->save($hasAccess)){
                    $conn->rollback();
                    $response['msg'] = __('Technical error happen');
                    $response['reason'] = 'Error Code: Nothi FORWARD - 2';
                    goto rtn;
                }


                //keep signature on note
                $lastNote = $nothiNotesTable->find()->select(['id'])->where(['nothi_part_no' => $nothi_part_id, 'note_no >=' => 0])->order(['id DESC'])->first();

                //sometimes error occurred. Receiver Signature already Exist as last signature.Throw Exception
                $lastSignature_2_check_receiver_sign= $nothiNoteSignaturesTable->getLastSignature(0,
                    $nothi_part_id, $lastNote['id'])->order(['id desc'])->first();
                if(!empty($lastSignature_2_check_receiver_sign)){
                    if($lastSignature_2_check_receiver_sign['office_organogram_id'] == $this->request->data['to_officer_designation_id']){
                        //something wrong . Delete this signature
                        $conn->rollback();
                        $nothiNoteSignaturesTable->deleteAll(['id' =>$lastSignature_2_check_receiver_sign['id']]);
                        $response['msg'] = __('Technical error happen');
                        $response['reason'] = 'Error Code: Nothi FORWARD - 10';
                        goto rtn;
                    }
                }
                //sometimes error occurred. Receiver Signature already Exist as last signature.Throw Exception

                //save own signature
                $lastSignature = $nothiNoteSignaturesTable->getByEmployeeId($employee_office['office_unit_organogram_id'],
                    $nothi_part_id)->first();

                $lastCrossSignature = ($lastSignature['nothi_note_id'] == $lastNote['id']) ? 0 : $lastSignature['cross_signature'];

                if (empty($lastNote)) {
                    $lastNote['id'] = 0;
                }
                //get last signature of that note for is_signature = 1and cross_signature = 0
                $lastSignatureFor_is_Signature_1 = $nothiNoteSignaturesTable->getLastSignature($employee_office['office_unit_organogram_id'],
                    $nothi_part_id, $lastNote['id'])->where(['is_signature' => 1, 'cross_signature' => 0])->order(['id desc'])->first();
                $f = true;
                if (!empty($lastSignatureFor_is_Signature_1)) {
                    /**
                     * *Note nisponno put signature on that note of this user.
                     * Checking if user sending note after nisponno that note as it will give another signature in that note.
                     */
                    if ($lastSignatureFor_is_Signature_1['nothi_note_id'] == $lastNote['id']
                        && $lastSignatureFor_is_Signature_1['office_organogram_id']
                        == $employee_office['office_unit_organogram_id']
                    ) {
                        $f = false;
                    }
                }

                if (!empty($lastSignature)) {
                    $nothiNoteSignaturesTable->updateAll([
                        'is_signature' => 1,
                        'cross_signature' => $lastCrossSignature,
                        'office_id' => $employee_office['office_id'],
                        'office_unit_id' => $employee_office['office_unit_id'],
                        'employee_id' => $employee_office['officer_id'],
                        'signature_date' => date("Y-m-d H:i:s"),
                        'employee_designation' => $employee_office['designation_label'] . (!empty($employee_office['incharge_label']) ? (" (" . $employee_office['incharge_label'] . ')') : '') . (!empty($employee_office['office_unit_name']) ? ("<br style='font-size: 8pt !important;'>" . $employee_office['office_unit_name']) : ''),
                    ], ['id' => $lastSignature['id']]);
                } else {
                    $lastSignature_singed = $nothiNoteSignaturesTable->getByEmployeeId($employee_office['office_unit_organogram_id'], $nothi_part_id, 1)->first();
                    if (!empty($lastSignature_singed)) {
                        $nothiNoteSignaturesTable->updateAll([
                            'signature_date' => date("Y-m-d H:i:s")
                        ], ['id' => $lastSignature_singed['id']]);
                    }
                }

                if ($f == true) {
                    if (empty($lastSignature)) {
                        $nothiNoteSignatures = $nothiNoteSignaturesTable->newEntity();
                        $nothiNoteSignatures->nothi_master_id = $nothi_master_id;
                        $nothiNoteSignatures->nothi_part_no = $nothi_part_id;
                        $nothiNoteSignatures->office_id = $employee_office['office_id'];
                        $nothiNoteSignatures->employee_id = $employee['personal_info']['id'];
                        $nothiNoteSignatures->office_unit_id = $employee_office['office_unit_id'];
                        $nothiNoteSignatures->office_organogram_id = $employee_office['office_unit_organogram_id'];
                        $nothiNoteSignatures->employee_designation = $employee_office['designation_label'] . (!empty($employee_office['incharge_label']) ? (" (" . $employee_office['incharge_label'] . ')') : '') . (!empty($employee_office['office_unit_name']) ? ("<br style='font-size: 8pt !important;'>" . $employee_office['office_unit_name']) : '');

                        $nothiNoteSignatures->nothi_note_id = !empty($lastNote['id'])
                            ? intval($lastNote['id']) : 0;
                        $nothiNoteSignatures->note_decision = '';
                        $nothiNoteSignatures->cross_signature = 0;
                        $nothiNoteSignatures->is_signature = 1;
                        $nothiNoteSignatures->signature_date = date("Y-m-d H:i:s");
                        $nothiNoteSignatures->created = date("Y-m-d H:i:s");
                        $nothiNoteSignatures->created_by = $this->Auth->user('id');
                        $nothiNoteSignatures->modified_by = $this->Auth->user('id');

                        if(!$nothiNoteSignaturesTable->save($nothiNoteSignatures)){
                            $conn->rollback();
                            $response['msg'] = __('Technical error happen');
                            $response['reason'] = 'Error Code: Nothi FORWARD - 3';
                            goto rtn;
                        }
                    } else {

                        if ($lastSignature->nothi_note_id == $lastNote['id']) {
                            if ($lastSignature->office_organogram_id == $employee_office['office_unit_organogram_id']) {
                                $lastSignature->office_id = $employee_office['office_id'];
                                $lastSignature->office_unit_id = $employee_office['office_unit_id'];
                                $lastSignature->employee_id = $employee_office['officer_id'];
                                $lastSignature->employee_designation = $employee_office['designation_label'] . (!empty($employee_office['incharge_label']) ? (" (" . $employee_office['incharge_label'] . ')') : '') . (!empty($employee_office['office_unit_name']) ? ("<br style='font-size: 8pt !important;'>" . $employee_office['office_unit_name']) : '');
                                $lastSignature->is_signature = 1;
                                $lastSignature->signature_date = date("Y-m-d H:i:s");
                                $lastSignature->created = date("Y-m-d H:i:s");
                                $lastSignature->created_by = $this->Auth->user('id');
                                $lastSignature->modified_by = $this->Auth->user('id');

                                $nothiNoteSignaturesTable->save($lastSignature);
                            } else {

                                $nothiNoteSignatures = $nothiNoteSignaturesTable->newEntity();
                                $nothiNoteSignatures->nothi_master_id = $nothi_master_id;
                                $nothiNoteSignatures->nothi_part_no = $nothi_part_id;
                                $nothiNoteSignatures->office_id = $employee_office['office_id'];
                                $nothiNoteSignatures->employee_id = $employee_office['officer_id'];
                                $nothiNoteSignatures->office_unit_id = $employee_office['office_unit_id'];
                                $nothiNoteSignatures->office_organogram_id = $employee_office['office_unit_organogram_id'];
                                $nothiNoteSignatures->employee_designation = $employee_office['designation_label'] . (!empty($employee_office['incharge_label']) ? (" (" . $employee_office['incharge_label'] . ')') : '') . (!empty($employee_office['office_unit_name']) ? ("<br style='font-size: 8pt !important;'>" . $employee_office['office_unit_name']) : '');

                                $nothiNoteSignatures->nothi_note_id = !empty($lastNote['id'])
                                    ? intval($lastNote['id']) : 0;
                                $nothiNoteSignatures->note_decision = '';
                                $nothiNoteSignatures->cross_signature = 0;
                                $nothiNoteSignatures->is_signature = 1;
                                $nothiNoteSignatures->signature_date = date("Y-m-d H:i:s");
                                $nothiNoteSignatures->created = date("Y-m-d H:i:s");
                                $nothiNoteSignatures->created_by = $this->Auth->user('id');
                                $nothiNoteSignatures->modified_by = $this->Auth->user('id');

                                if(!$nothiNoteSignaturesTable->save($nothiNoteSignatures)){
                                    $conn->rollback();
                                    $response['msg'] = __('Technical error happen');
                                    $response['reason'] = 'Error Code: Nothi FORWARD - 4';
                                    goto rtn;
                                }
                            }
                        } else {
                            if ($lastSignature->office_organogram_id == $employee_office['office_unit_organogram_id']) {
                                $lastSignature->office_id = $employee_office['office_id'];
                                $lastSignature->office_unit_id = $employee_office['office_unit_id'];
                                $lastSignature->employee_id = $employee_office['officer_id'];
                                $lastSignature->employee_designation = $employee_office['designation_label'] . (!empty($employee_office['incharge_label']) ? (" (" . $employee_office['incharge_label'] . ')') : '') . (!empty($employee_office['office_unit_name']) ? ("<br style='font-size: 8pt !important;'>" . $employee_office['office_unit_name']) : '');
                                $lastSignature->is_signature = 1;
                                $lastSignature->cross_signature = 1;
                                $lastSignature->signature_date = date("Y-m-d H:i:s");
                                $lastSignature->created = date("Y-m-d H:i:s");
                                $lastSignature->created_by = $this->Auth->user('id');
                                $lastSignature->modified_by = $this->Auth->user('id');

                                if(!$nothiNoteSignaturesTable->save($lastSignature)){
                                    $conn->rollback();
                                    $response['msg'] = __('Technical error happen');
                                    $response['reason'] = 'Error Code: Nothi FORWARD - 5';
                                    goto rtn;
                                }
                            }
                            $nothiNoteSignatures = $nothiNoteSignaturesTable->newEntity();
                            $nothiNoteSignatures->nothi_master_id = $nothi_master_id;
                            $nothiNoteSignatures->nothi_part_no = $nothi_part_id;
                            $nothiNoteSignatures->office_id = $employee_office['office_id'];
                            $nothiNoteSignatures->employee_id = $employee_office['officer_id'];
                            $nothiNoteSignatures->office_unit_id = $employee_office['office_unit_id'];
                            $nothiNoteSignatures->office_organogram_id = $employee_office['office_unit_organogram_id'];
                            $nothiNoteSignatures->employee_designation = $employee_office['designation_label'] . (!empty($employee_office['incharge_label']) ? (" (" . $employee_office['incharge_label'] . ')') : '') . (!empty($employee_office['office_unit_name']) ? ("<br style='font-size: 8pt !important;'>" . $employee_office['office_unit_name']) : '');

                            $nothiNoteSignatures->nothi_note_id = !empty($lastNote['id'])
                                ? intval($lastNote['id']) : 0;
                            $nothiNoteSignatures->note_decision = '';
                            $nothiNoteSignatures->is_signature = 1;
                            $nothiNoteSignatures->signature_date = date("Y-m-d H:i:s");
                            $nothiNoteSignatures->created = date("Y-m-d H:i:s");
                            $nothiNoteSignatures->created_by = $user_id;
                            $nothiNoteSignatures->modified_by = $user_id;

                            if(!$nothiNoteSignaturesTable->save($nothiNoteSignatures)){
                                $conn->rollback();
                                $response['msg'] = __('Technical error happen');
                                $response['reason'] = 'Error Code: Nothi FORWARD - 5';
                                goto rtn;
                            }
                        }
                    }
                }
                //end

                $nothiNoteSignatures = $nothiNoteSignaturesTable->newEntity();
                $nothiNoteSignatures->nothi_master_id = $nothi_master_id;
                $nothiNoteSignatures->nothi_part_no = $nothi_part_id;
                $nothiNoteSignatures->office_id = $this->request->data['to_office_id'];
                $nothiNoteSignatures->employee_id = $this->request->data['to_officer_id'];
                $nothiNoteSignatures->office_unit_id = $this->request->data['to_office_unit_id'];
                $nothiNoteSignatures->office_organogram_id = $this->request->data['to_officer_designation_id'];
                $nothiNoteSignatures->employee_designation = $this->request->data['to_officer_designation_label'] . (!empty($this->request->data['to_officer_incharge_label']) ? (" (" . $this->request->data['to_officer_incharge_label'] . ')') : '') . (!empty($this->request->data['to_office_unit_name']) ? ("<br style='font-size: 8pt !important;'>" . $this->request->data['to_office_unit_name']) : '');

                $nothiNoteSignatures->nothi_note_id = !empty($lastNote['id']) ? intval($lastNote['id'])
                    : 0;
                $nothiNoteSignatures->note_decision = '';
                $nothiNoteSignatures->cross_signature = 0;
                $nothiNoteSignatures->is_signature = ($this->request->data['to_officer_designation_id'] == $employee_office['office_unit_organogram_id']) ? 1 : 0;
                $nothiNoteSignatures->signature_date = date("Y-m-d H:i:s");
                $nothiNoteSignatures->created = date("Y-m-d H:i:s");
                $nothiNoteSignatures->created_by = $user_id;
                $nothiNoteSignatures->modified_by = $user_id;

                if(!$nothiNoteSignaturesTable->save($nothiNoteSignatures)){
                    $conn->rollback();
                    $response['msg'] = __('Technical error happen');
                    $response['reason'] = 'Error Code: Nothi FORWARD - 6';
                    goto rtn;
                }
                //end
                //change nothi notes status
                $nothiNotesTable->updateAll(['note_status' => 'INBOX'], ['nothi_part_no' => $nothi_part_id]);

                //change nothi movements view status
                $nothiMovementsTable->updateAll(['view_status' => 1],
                    ['nothi_part_no' => $nothi_part_id, 'to_officer_designation_id' => $employee_office['office_unit_organogram_id']]);


                //feedback call
                $nothiPotrosTable = TableRegistry::get('NothiPotros');
                $potro = $nothiPotrosTable->find()
                    ->where([
                        'nothi_part_no' => $nothi_part_id,
                        'nothi_master_id'=>$nothi_master_id
                    ])->order(['id'=>'desc'])->first();

                if(!empty($potro['application_meta_data'])) {
                    $potro_meta = jsonA($potro['application_meta_data']);
                    if (!empty($potro_meta['feedback_url'])) {
                        if(!empty($potro_meta['token_url'])){
                           if(!empty($potro_meta['api_client']) && ($potro_meta['api_client'] == 'BTRC')){
                               $auth_token_response = $this->NotifiyFeedbackAuthTokenGeneration($potro_meta['token_url'],['user' => 'A2i','password' => '123456']);
                               if(!empty($auth_token_response['status']) && $auth_token_response['status'] == 'success'){
                                   if(!empty($auth_token_response['token'])){
                                        $potro_meta['token'] = $auth_token_response['token'];
                                   }
                               }
                           }
                        }
                        $feedbackdata = $potro_meta + [
                                'api_key' => !empty($potro_meta['feedback_api_key']) ? $potro_meta['feedback_api_key'] : '',
                                'aid' => $potro_meta['tracking_id'],
                                'current_desk_id' => $this->request->data['to_officer_designation_id'],
                                'data'=>$potro['potro_content'],
                                'nothi_id' => $potro['nothi_master_id'],
                                'note_id' => $potro['nothi_part_no'],
                                'potro_id' => $potro['id'],
                            ];
                        $notification_response =  $this->notifyFeedback($potro_meta['feedback_url'], $feedbackdata);
                        $notification_response['url'] = $potro_meta['feedback_url'];
                        $notification_response['data_sent'] = $feedbackdata;
                    }
                }

                //feedback call end


                //checking if new note is opening or not
                if ($is_finished == 1) {
                    TableRegistry::get('NoteInitialize')->saveData(['nothi_masters_id' => $nothi_master_id, 'nothi_part_no' => $nothi_part_id, 'office_id' => $employee_office['office_id'], 'office_units_id' => $employee_office['office_unit_id'], 'office_units_organogram_id' => $employee_office['office_unit_organogram_id'], 'user_id' => $this->Auth->user('id')]);
                }
                // only for digital signature -- as signature set in last step so signature should be updated after process end
                if (!empty($note_ids)) {
                    TableRegistry::remove('NothiNoteSignatures');
                    $nothiNoteSignaturesTable = TableRegistry::get('NothiNoteSignatures');
                    $nothiNoteSignaturesTable->updateAll(['digital_sign' => 1], ['digital_sign' => 0, 'nothi_note_id IN' => $note_ids, 'office_organogram_id' => $employee_office['office_unit_organogram_id'], 'cross_signature' => 0, 'nothi_part_no' => $nothi_part_id]);
                }
                // only for digital signature

                $current_office_section = $employee_office;
                $current_office_section['office_id'] = $employee_office['office_id'];
                $current_office_section['officer_id'] = $employee_office['officer_id'];

                $sentUser['office_id'] = $this->request->data['to_office_id'];
                $sentUser['officer_id'] = $this->request->data['to_officer_id'];

                if (!$this->NotificationSet('sent', array(1, "Nothi"), 2,
                    $current_office_section, $current_office_section,
                    $nothiPartsInformation['subject'])
                ) {

                }
                if (!$this->NotificationSet('inbox', array(1, "Nothi"), 2,
                    $current_office_section, $sentUser,
                    $nothiPartsInformation['subject'])
                ) {

                }
                // Give Receiver Notification
                $title = 'আপনার ডেস্কে নতুন নথি এসেছে';
                $mail_sender = [
                    'sender' => h($employee['personal_info']['name_bng'] . ', ' . $employee_office['designation_label'] . ', ' . $employee_office['office_unit_name']),
                    'subject' => h($nothiPartsInformation['subject']),
                    'time' => Time::parse(date('Y-m-d H:i:s'))->i18nFormat(null, null, 'bn-BD'),
                ];
                $mail_sender_notify = 'প্রেরকঃ ' . $mail_sender['sender'] . "\n বিষয়ঃ " . h($mail_sender['subject']) . "\n সময়ঃ " . $mail_sender['time'];
                $this->notifyAppUser('', $sentUser['officer_id'], ['title' => $title, 'body' => $mail_sender], ['title' => $title, 'body' => $mail_sender_notify]);
                // Give Receiver Notification

                $conn->commit();

            } catch (\Exception $ex) {
                $conn->rollback();

                $response['msg'] = 'দুঃখিত! নথি পরবর্তী কার্যক্রমের জন্য প্রেরণ করা সম্ভব হচ্ছে না';
                $response['reason'] = $this->makeEncryptedData($ex->getMessage());
                goto rtn;
            }
                // If nothi from other office we need to keep track of that nothi in current employee office DB - Start
                if ($otherNothi == true) {
                    $this->saveOtherOfficeNothiMasterTransaction($nothi_master_id, $nothi_part_id, $nothi_office, $employee_office, $employee, $toOfficeName, $this->request->data, $nothi_no, $subject, $creator_unit_id);
                }
                // If nothi from other office we need to keep track of that nothi in current employee office DB - End

                // Multiple Office have to keep track of current user -Start
                TableRegistry::remove('NothiMasterPermissions');
                $nothiPriviligeTable = TableRegistry::get('NothiMasterPermissions');
                $allPermittedOfficesList = $nothiPriviligeTable->permittedOfficesId($nothi_part_id, $nothi_office);
                if (!in_array($nothi_office, $allPermittedOfficesList)) {
                    $allPermittedOfficesList[$nothi_office] = $nothi_office;
                }
                $multiple_offices_permission = false;
                if (!empty($allPermittedOfficesList)) {
                    if (count($allPermittedOfficesList) == 1 && current($allPermittedOfficesList) == $employee_office['office_id']) {
                        $multiple_offices_permission = true;
                    }
                    $priviliges = $nothiPriviligeTable->find()->select([
                        'perm_value' => 'privilige_type',
                        'ofc_id' => 'office_id',
                        'unit_id' => 'office_unit_id',
                        'org_id' => 'office_unit_organograms_id',
                        'designation_level' => 'designation_level',
                        'visited' => 'visited'
                    ])->where(['nothi_masters_id' => $real_nothi_master_id, 'nothi_part_no' => $nothi_part_id, 'nothi_office' => $nothi_office])->toArray();
                    foreach ($allPermittedOfficesList as $key => $value) {
                        try {
                            if ($multiple_offices_permission == false) {
                                $is_connected = $this->switchOfficeWithStatus($key, 'MainNothiOffice', -1, true);
                                if (!$is_connected || (!empty($is_connected['status']) && $is_connected['status'] == 'error')) {
                                    if ($key != $employee_office['office_id']) {
                                        $officeDomainsTable = TableRegistry::get('OfficeDomains');
                                        $otherOffice = $officeDomainsTable->find()->where(['office_id' => $key])->first();
                                        $url = $otherOffice->domain_url . "/postUpdateOtherOfficePermissions";

                                        $encrypted_data = $this->makeEncryptedData(serialize([
                                            'nothi_office' => $nothi_office,
                                            'nothi_part_no' => $nothi_part_id,
                                            'nothi_master_id' => $real_nothi_master_id
                                        ]));
                                        $data = [
                                            'token' => base64_encode($encrypted_data),
                                            'nothi_office' => $nothi_office,
                                            'nothi_part_no' => $nothi_part_id,
                                            'nothi_master_id' => $real_nothi_master_id,
                                            'current_users' => json_encode(['office_id' => $this->request->data['to_office_id'], 'office_unit_id' => $this->request->data['to_office_unit_id'], 'office_unit_organogram_id' => $this->request->data['to_officer_designation_id']]),
                                            'permissions' => json_encode($priviliges),
                                            'target_office_id' => $key
                                        ];

                                        $auth_token_response = curlRequest($url, $data);
                                        if (!empty($auth_token_response)) {
                                            $auth_token_response = jsonA($auth_token_response);
                                            if (!empty($auth_token_response['status']) && ($auth_token_response['status']) == 'success') {
                                                continue;
                                            }
                                        }

                                        $office_info = TableRegistry::get('Offices')->getBanglaName($key);
                                        if (!empty($office_info['office_name_bng'])) {
                                            throw new Exception('অফিসের  (' . $office_info['office_name_bng'] . ') নির্বাচিত কর্মকর্তাদের অনুমতি দেওয়া সম্ভব হয়নি। কারণঃ ডাটাবেজ পাওয়া যায় নি!');
                                        } else {
                                            throw new Exception('অফিসের (আইডিঃ ' . $key . ') নির্বাচিত কর্মকর্তাদের অনুমতি দেওয়া সম্ভব হয়নি। কারণঃ ডাটাবেজ পাওয়া যায় নি!');
                                        }
                                        continue;
                                    }
                                }

                                /*if (!$is_connected || (!empty($is_connected['status']) && $is_connected['status'] == 'error')) {
//                                    continue;
                                    throw new Exception('অফিস ডাটাবেস সংযোগ সম্ভব হচ্ছেনাঃ '. $key.(isset($is_connected['reason'])?(' '.$is_connected['reason']):''));
                                }
                                $checkDBConnection = $this->checkCurrentOfficeDBConnection();
                                if (empty($checkDBConnection) || (!empty($checkDBConnection['status']) && $checkDBConnection['status'] == 'error')) {
                                    throw new Exception('অফিস ডাটাবেস সংযোগ সম্ভব হচ্ছেনাঃ '. $key.(isset($is_connected['reason'])?(' '.$is_connected['reason']):''));
//                                    continue;
                                }*/
                            }


                            TableRegistry::remove('NothiMasterCurrentUsers');
                            $nothiMasterCurrentUsersTable = TableRegistry::get("NothiMasterCurrentUsers");
                            //first check if current note has entry
                            $hasDatainCurrentUser = $nothiMasterCurrentUsersTable->find()->where(
                                [
                                    'nothi_part_no' => $nothi_part_id,
                                    'nothi_office' => $nothi_office,
                                    'nothi_master_id'=>$real_nothi_master_id
                                ]
                            )->count();
                            //change current user
                            if(!empty($hasDatainCurrentUser)){
                                $nothiMasterCurrentUsersTable->updateAll(
                                    [
                                        'is_archive' => 0,
                                        'is_finished' => 0,
                                        'is_summary' => !empty($hasSummary) ? $hasSummary : 0,
                                        'office_id' => $this->request->data['to_office_id'],
                                        'office_unit_id' => $this->request->data['to_office_unit_id'],
                                        'office_unit_organogram_id' => $this->request->data['to_officer_designation_id'],
                                        'employee_id' => $this->request->data['to_officer_id'],
                                        'view_status' => 0,
                                        'issue_date' => date("Y-m-d H:i:s"),
                                        'forward_date' => date("Y-m-d H:i:s"),
                                        'is_new' => 0,
                                        'priority' => isset($this->request->data['priority']) ? $this->request->data['priority'] : 0,
                                    ],
                                    [
                                        'nothi_part_no' => $nothi_part_id,
                                        'nothi_office' => $nothi_office,
                                        'nothi_master_id' => $real_nothi_master_id,
                                    ]);
                            }else{
                                // need to create a new row
                                $nothiMasterCurrentUsersEntity = $nothiMasterCurrentUsersTable->newEntity();
                                $nothiMasterCurrentUsersEntity->nothi_master_id = $real_nothi_master_id;
                                $nothiMasterCurrentUsersEntity->nothi_part_no = $nothi_part_id;
                                $nothiMasterCurrentUsersEntity->office_id = $this->request->data['to_office_id'];
                                $nothiMasterCurrentUsersEntity->office_unit_id = $this->request->data['to_office_unit_id'];
                                $nothiMasterCurrentUsersEntity->office_unit_organogram_id = $this->request->data['to_officer_designation_id'];
                                $nothiMasterCurrentUsersEntity->employee_id = $this->request->data['to_officer_id'];
                                $nothiMasterCurrentUsersEntity->issue_date = date("Y-m-d H:i:s");
                                $nothiMasterCurrentUsersEntity->forward_date = date("Y-m-d H:i:s");
                                $nothiMasterCurrentUsersEntity->is_new = 0;
                                $nothiMasterCurrentUsersEntity->nothi_office = $nothi_office;
                                $nothiMasterCurrentUsersEntity->is_archive =0;
                                $nothiMasterCurrentUsersEntity->is_finished =0;
                                $nothiMasterCurrentUsersEntity->priority =isset($this->request->data['priority']) ? $this->request->data['priority'] : 0;
                                $nothiMasterCurrentUsersEntity->is_summary =!empty($hasSummary) ? $hasSummary : 0;
                                $nothiMasterCurrentUsersEntity->view_status =0;

                                $nothiMasterCurrentUsersRecord = $nothiMasterCurrentUsersTable->save($nothiMasterCurrentUsersEntity);
                            }

                            //end
                        } catch (\Exception $ex) {
                            // rollback -- delete previous nothi transactions - signature and movements
                            $this->switchOffice($employee_office['office_id'], 'MainNothiOffice');
                            TableRegistry::remove('NothiMasterMovements');
                            TableRegistry::remove('NothiMasterPermissions');
                            TableRegistry::remove('NothiNoteSignatures');
                            $nothiMovementsTable = TableRegistry::get('NothiMasterMovements');
                            $nothiNoteSignaturesTable = TableRegistry::get('NothiNoteSignatures');
                            TableRegistry::remove('NothiMasterCurrentUsers');
                            $nothiMasterCurrentUsersTable = TableRegistry::get("NothiMasterCurrentUsers");
                            $nothiMovementsTable->deleteLastMovementForError($nothi_part_id,$employee_office['office_unit_organogram_id'],$this->request->data['to_officer_designation_id']);
                            $nothiNoteSignaturesTable->deleteLastSignatureForError($nothi_part_id,isset($lastNote['id'] )?$lastNote['id'] :0,$this->request->data['to_officer_designation_id']);
                            $nothiNoteSignaturesTable->unsetLastSignatureForError($nothi_part_id,isset($lastNote['id'] )?$lastNote['id'] :0,$employee_office['office_unit_organogram_id']);
                            $nothiMasterCurrentUsersTable->updateAll(
                                [
                                    'office_id' => $employee_office['office_id'],
                                    'office_unit_id' => $employee_office['office_unit_id'],
                                    'office_unit_organogram_id' => $employee_office['office_unit_organogram_id'],
                                    'employee_id' => $employee_office['officer_id'],
                                    'view_status' => 1,
                                ],
                                [
                                    'nothi_part_no' => $nothi_part_id,
                                    'nothi_office' => $nothi_office,
                                ]);
                            // rollback -- delete previous nothi transactions - signature and movements
                            $response['msg'] ='দুঃখিত! নথি পরবর্তী কার্যক্রমের জন্য প্রেরণ করা সম্ভব হচ্ছে না';
                            $response['reason'] =$this->makeEncryptedData($ex->getMessage());
                            goto rtn;
//                            $this->Flash->error($ex->getMessage());
                        }
                    }
                }
//                if ($multiple_offices_permission == false) {
//                    $this->switchOffice($employee_office['office_id'], 'MainNothiOffice');
//                }
                // Multiple Office have to keep track of current user -End
            $request_success = 1;
        }
        if(!empty($request_success)){
            $response['msg'] = 'নথি পরবর্তী কার্যক্রমের জন্য প্রেরণ করা হয়েছে';
            $response['status'] = 'success';
        }
            rtn:
            $this->response->body(json_encode($response));
            $this->response->type('application/json');
            return $this->response;
    }

    private function saveOtherOfficeNothiMasterTransaction($nothi_master_id, $nothi_part_id, $nothi_office, $employee_office, $employee, $toOfficeName, $post_data, $nothi_no = '', $subject = '', $creator_unit_id = '')
    {
        try {
            $this->switchOffice($employee_office['office_id'], 'NothiOffice');

            TableRegistry::remove('OtherOfficeNothiMasterMovements');
            $otherOfficeNothiMovementsTable = TableRegistry::get('OtherOfficeNothiMasterMovements');

            // first delete previous trasactions
            $otherOfficeNothiMovementsTable->deletePreviousTransactions($nothi_master_id, $nothi_part_id, $nothi_office, $employee_office);
            // first delete previous trasactions

            $first_move = $otherOfficeNothiMovementsTable->newEntity();
            $first_move->nothi_master_id = $nothi_master_id;
            $first_move->nothi_part_no = $nothi_part_id;
            $first_move->nothi_office = $nothi_office;
            $first_move->from_office_id = $employee_office['office_id'];
            $first_move->from_officer_id = $employee['personal_info']['id'];
            $first_move->from_office_unit_id = $employee_office['office_unit_id'];
            $first_move->from_officer_designation_id = $employee_office['office_unit_organogram_id'];
            $first_move->from_office_name = $employee_office['office_name'];
            $first_move->from_office_unit_name = $employee_office['office_unit_name'];
            $first_move->from_officer_name = $employee['personal_info']['name_bng'];
            $first_move->from_officer_designation_label = $employee_office['designation_label'];

            $first_move->to_office_id = $post_data['to_office_id'];
            $first_move->to_office_name = (!empty($toOfficeName) ? $toOfficeName['office_name_bng']
                : '');
            $first_move->to_office_unit_id = $post_data['to_office_unit_id'];
            $first_move->to_office_unit_name = $post_data['to_office_unit_name'];
            $first_move->to_officer_id = $post_data['to_officer_id'];
            $first_move->to_officer_name = $post_data['to_officer_name'];
            $first_move->to_officer_designation_id = $post_data['to_officer_designation_id'];
            $first_move->to_officer_designation_label = $post_data['to_officer_designation_label'];

            $first_move->priority = isset($post_data['priority']) ? $post_data['priority'] : 0;
            $first_move->nothi_no = $nothi_no;
            $first_move->subject = $subject;
            if (!empty($creator_unit_id)) {
                $shakha = TableRegistry::get('OfficeUnits')->getNameWithOffice($creator_unit_id);
                $first_move->nothi_shakha = $shakha['unit_name_bng'] . ',' . $shakha['office_name_bng'];
            }
            $user_id = !empty($this->Auth->user('id')) ? $this->Auth->user('id') : 0;
            $first_move->view_status = 0;
            $first_move->created_by = $user_id;
            $first_move->modified_by = $user_id;
            $first_move->created = date('Y-m-d H:i:s');
            $first_move->modified = date('Y-m-d H:i:s');
            if(!$otherOfficeNothiMovementsTable->save($first_move)){
                return false;
            }
        } catch (\Exception $exp) {
            return false;
        }
        return true;
    }

    public function archiveNothiMaster()
    {
        $username = $this->Auth->user('username');
        $options['token'] = sGenerateToken(['file' => $username], ['exp' => time() + 60 * 300]);
        $data = $this->getSignature($username, 1, 0, true, null, $options);

        if ($data == false) {
            $this->response->body(json_encode(array('status' => 'error',
                'msg' => 'দুঃখিত! আপনার স্বাক্ষর পাওয়া যায়নি। অনুগ্রহ করে সিস্টেমে স্বাক্ষর আপলোড করুন। ধন্যবাদ।')));
            $this->response->type('application/json');
            return $this->response;
        }
        $return = array(
            'status' => 'error',
            'msg' => ''
        );
        $user = $this->getCurrentDakSection();

        if ($this->request->is('ajax')) {
            if (!empty($this->request->data)) {
                $nothimasterarray = $this->request->data['selectednothi'];
                $total = 0;
                try {
                    if (!empty($nothimasterarray)) {
                        $nothiMasterCurrentUserTable = TableRegistry::get('NothiMasterCurrentUsers');
                        $total = $nothiMasterCurrentUserTable->find()->where(['nothi_master_id IN' => array_values($nothimasterarray), 'office_unit_organogram_id' => $user['office_unit_organogram_id'],
                            'nothi_office' => $user['office_id'], 'is_finished' => 1])->count();
                        foreach ($nothimasterarray as $key => $value) {
                            TableRegistry::remove('NothiMasterPermissions');
                            $nothiPriviligeTable = TableRegistry::get('NothiMasterPermissions');
                            $allPermittedOfficesList = $nothiPriviligeTable->permittedOfficesIdByNothiMasterID($value, $user['office_id']);
                            $multiple_offices_permission = false;
                            if (!empty($allPermittedOfficesList)) {
                                if (count($allPermittedOfficesList) == 1 && current($allPermittedOfficesList) == $user['office_id']) {
                                    $multiple_offices_permission = true;
                                }
                                foreach ($allPermittedOfficesList as $p_key => $p_value) {
                                    if ($multiple_offices_permission == false) {
                                        $is_connected = $this->switchOfficeWithStatus($p_key, 'MainNothiOffice', -1, true);
                                        if (!$is_connected || (!empty($is_connected['status']) && $is_connected['status'] == 'error')) {
                                            continue;
                                        }
                                        $checkDBConnection = $this->checkCurrentOfficeDBConnection();
                                        if (empty($checkDBConnection) || (!empty($checkDBConnection['status']) && $checkDBConnection['status'] == 'error')) {
                                            continue;
                                        }
                                    }
                                    TableRegistry::remove('NothiMasterCurrentUsers');
                                    TableRegistry::get('NothiMasterCurrentUsers')->updateAll(['is_archive' => 1,
                                        'is_new' => 0],
                                        ['nothi_master_id' => $value, 'office_unit_organogram_id' => $user['office_unit_organogram_id'],
                                            'nothi_office' => $user['office_id'], 'is_finished' => 1]);
                                }
                                if ($multiple_offices_permission == false) {
                                    $this->switchOffice($user['office_id'], 'MainNothiOffice');
                                }
                            }

                        }
                    }

                    if ($total == 0) {
                        $return = array('status' => 'error', 'msg' => 'দুঃখিত! নথিতে আর্কাইভ করার মত নোট পাওয়া যায়নি');
                    } else {
                        $return = array('status' => 'success', 'msg' => 'নথির নিষ্পন্ন নোটসমূহ আর্কাইভ করা হয়েছে');
                    }
                } catch (\Exception $ex) {
                    $return = array('status' => 'error', 'msg' => 'দুঃখিত! নথি আর্কাইভ করা সম্ভব হচ্ছে না', 'reason' => '');
                }
            }
        }

        echo json_encode($return);

        die;
    }

    public function finishNothiMaster()
    {
        $this->digitalSigantureUrlChecker();
        if (isset($this->request->data['api_key'])) {
            $apikey = $this->request->data['api_key'];
            if ($apikey != API_KEY) {
                //verify api token
                $this->loadComponent('ApiRelated');
                $getApiTokenData = $this->ApiRelated->getMobileApiTokenData($apikey);
                if (!empty($getApiTokenData)) {
                    if (!in_array($this->request->data['user_designation'], $getApiTokenData['designations'])) {
                        die('Unauthorized request');
                    }
                }
            }

            $username = $getApiTokenData['user_name'];
            $user = $this->setCurrentDakSection($this->request->data['user_designation']);
            $employee['personal_info']['id'] = $user['officer_id'];
        } else {
            $username = $this->Auth->user('username');
            $user = $this->getCurrentDakSection();
            $employee = $this->request->session()->read('logged_employee_record');
        }
        $this->switchOffice($user['office_id'], 'OfficeDB');

        $options['token'] = sGenerateToken(['file' => $username], ['exp' => time() + 60 * 300]);
        $data = $this->getSignature($username, 1, 0, true, null, $options);

        if ($data == false) {
            $this->response->body(json_encode(array('status' => 'error',
                'msg' => 'দুঃখিত! আপনার স্বাক্ষর পাওয়া যায়নি। অনুগ্রহ করে সিস্টেমে স্বাক্ষর আপলোড করুন। ধন্যবাদ।')));
            $this->response->type('application/json');
            return $this->response;
        }
        $return = array(
            'status' => 'error',
            'msg' => ''
        );
        $nothiMasterCurrentUserTable = TableRegistry::get('NothiMasterCurrentUsers');
        if ($this->request->is('ajax')) {

            if (!empty($this->request->data)) {
                $nothimaster = $this->request->data['selectednothi'];

                try {
                    if (!empty($nothimaster)) {
                        $check_is_finished = $nothiMasterCurrentUserTable->find()->select(['is_finished'])->where(['office_unit_organogram_id' => $user['office_unit_organogram_id'], 'office_unit_id' => $user['office_unit_id'], 'office_id' => $user['office_id'], 'nothi_office' => $user['office_id'], 'nothi_part_no' => $nothimaster])->first();
                        if (!empty($check_is_finished) && $check_is_finished['is_finished'] == 0) {
                            /*
                              * Time to check digital signature
                             *  skip if soft token -1
                              */
                            if ($user['default_sign'] > 0 && isset($this->request->data['soft_token']) && $this->request->data['soft_token'] != -1) {
                                $allNotesToSign = TableRegistry::get('NothiNotes')->find('list', ['keyField' => 'id', 'valueField' => 'note_description'])->where(['note_status' => 'DRAFT', 'nothi_part_no' => $nothimaster, 'office_organogram_id' => $user['office_unit_organogram_id']])->toArray();
                                $this->loadComponent('DigitalSignatureRelated');
                                $res = $this->DigitalSignatureRelated->signOnucched($allNotesToSign, $this->request->data['soft_token'], $nothimaster, $user['default_sign'], $user, $user['office_id']);
                                if ($res['status'] == 'error') {
                                    $this->response->body(json_encode(array('status' => 'error',
                                        'msg' => 'দুঃখিত! ডিজিটাল সিগনেচার করা সম্ভব হয়নি। কারণঃ ' . $res['msg'])));
                                    $this->response->type('application/json');
                                    return $this->response;
                                } else {
                                    $note_ids = $res['note_ids'];
                                }

                            }
                            /**
                             * Create an entry in Nisponno Records
                             */
                            $NisponnoRecordsTable = TableRegistry::get('NisponnoRecords');
                            try {
                                TableRegistry::remove('NothiMasterPermissions');
                                $nothiPriviligeTable = TableRegistry::get('NothiMasterPermissions');
                                $allPermittedOfficesList = $nothiPriviligeTable->permittedOfficesId($nothimaster, $user['office_id']);
                                $multiple_offices_permission = false;
                                if (!empty($allPermittedOfficesList)) {
                                    if (count($allPermittedOfficesList) == 1 && current($allPermittedOfficesList) == $user['office_id']) {
                                        $multiple_offices_permission = true;
                                    }
                                    foreach ($allPermittedOfficesList as $key => $value) {
                                        if ($multiple_offices_permission == false) {
                                            $is_connected = $this->switchOfficeWithStatus($key, 'MainNothiOffice', -1, true);
                                            if (!$is_connected || (!empty($is_connected['status']) && $is_connected['status'] == 'error')) {
                                                continue;
                                            }
                                            $checkDBConnection = $this->checkCurrentOfficeDBConnection();
                                            if (empty($checkDBConnection) || (!empty($checkDBConnection['status']) && $checkDBConnection['status'] == 'error')) {
                                                continue;
                                            }
                                        }
                                        TableRegistry::remove('NothiMasterCurrentUsers');
                                        TableRegistry::get('NothiMasterCurrentUsers')->updateAll(['is_finished' => 1, 'is_new' => 0, 'is_archive' => 1], ['office_unit_organogram_id' => $user['office_unit_organogram_id'], 'office_unit_id' => $user['office_unit_id'], 'office_id' => $user['office_id'], 'nothi_office' => $user['office_id'], 'nothi_part_no' => $nothimaster]);
                                    }
                                }

                                $this->switchOffice($user['office_id'], 'OfficeDB');
                                TableRegistry::remove('NothiNotes');
                                TableRegistry::remove('nothiNoteSignatures');
                                TableRegistry::remove('NothiDakPotroMaps');
                                $NothiNotesTable = TableRegistry::get('NothiNotes');
                                $last_onucched_info = $NothiNotesTable->find()->where(['nothi_part_no' => $nothimaster, 'note_no >=' => 0])->order(['id DESC'])->first();

                                $nisponnoEntity = $NisponnoRecordsTable->newEntity();
                                $nisponnoEntity->nothi_master_id = $last_onucched_info['nothi_master_id'];
                                $nisponnoEntity->nothi_part_no = $nothimaster;
                                $nisponnoEntity->type = 'note';
                                $nisponnoEntity->nothi_onucched_id = ($last_onucched_info['note_no'] == -1) ? 0 : $last_onucched_info['note_no'];
                                $nisponnoEntity->nothi_office_id = $user['office_id'];
                                $nisponnoEntity->office_id = $user['office_id'];
                                $nisponnoEntity->unit_id = $user['office_unit_id'];
                                $nisponnoEntity->designation_id = $user['office_unit_organogram_id'];
                                $nisponnoEntity->employee_id = $last_onucched_info['employee_id'];
                                $nisponnoEntity->operation_date = date('Y-m-d H:i:s', strtotime("+2 min")); //Important don't remove +2 min
                                if ($NisponnoRecordsTable->save($nisponnoEntity)) {

                                }

                                /**
                                 * Put signature in a note
                                 *
                                 */
                                $nothiNoteSignaturesTable = TableRegistry::get('nothiNoteSignatures');
                                //keep signature on last Onucched
                                $lastNote = $last_onucched_info;

                                //get last signature of that note for is_signature = 1and cross_signature = 0
//                                $lastSignatureFor_is_Signature_1 = $nothiNoteSignaturesTable->getLastSignature($user['office_unit_organogram_id'],
//                                        $nothimaster, $lastNote['id'])->where(['is_signature' => 1,'cross_signature' =>0])->order(['id desc'])->first();
                                $lastSignatureFor_is_Signature_1 = $nothiNoteSignaturesTable->getLastSignature(0,
                                    $nothimaster, $lastNote['id'])->where(['is_signature' => 1, 'cross_signature' => 0])->order(['id desc'])->first();
                                // signature already exist no need to add another one.
                                $f = true;
                                if (!empty($lastSignatureFor_is_Signature_1) && $lastSignatureFor_is_Signature_1['office_organogram_id'] == $user['office_unit_organogram_id']) {
                                    $nothiNoteSignaturesTable->updateAll(['signature_date' => date("Y-m-d H:i:s")], ['id' => $lastSignatureFor_is_Signature_1['id']]);
                                    $f = false;
                                }
                                //get last signature for this user where 'is_signature'=>0
                                $lastSignature = $nothiNoteSignaturesTable->getByEmployeeId($user['office_unit_organogram_id'],
                                    $nothimaster)->first();

                                //save own signature
                                if ($f == true) {
                                    if (empty($lastSignature)) {
                                        // No signature = 0 For this onucched. Create a new one.
                                        $nothiNoteSignatures = $nothiNoteSignaturesTable->newEntity();
                                        $nothiNoteSignatures->nothi_master_id = $last_onucched_info['nothi_master_id'];
                                        $nothiNoteSignatures->nothi_part_no = $nothimaster;
                                        $nothiNoteSignatures->office_id = $user['office_id'];
                                        $nothiNoteSignatures->employee_id = $employee['personal_info']['id'];
                                        $nothiNoteSignatures->office_unit_id = $user['office_unit_id'];
                                        $nothiNoteSignatures->office_organogram_id = $user['office_unit_organogram_id'];
                                        $nothiNoteSignatures->employee_designation = $user['designation_label'] . (!empty($user['incharge_label']) ? (" (" . $user['incharge_label'] . ')') : '') . (!empty($user['office_unit_name']) ? ("<br style='font-size: 8pt !important;'>" . $user['office_unit_name']) : '');

                                        $nothiNoteSignatures->nothi_note_id = !empty($lastNote['id'])
                                            ? intval($lastNote['id']) : 0;
                                        $nothiNoteSignatures->note_decision = '';
                                        $nothiNoteSignatures->cross_signature = 0;
                                        $nothiNoteSignatures->is_signature = 1;
                                        $nothiNoteSignatures->can_delete = 1;
                                        $nothiNoteSignatures->signature_date = date("Y-m-d H:i:s");
                                        $nothiNoteSignatures->created = date("Y-m-d H:i:s");
                                        $nothiNoteSignatures->created_by = $this->Auth->user('id');
                                        $nothiNoteSignatures->modified_by = $this->Auth->user('id');

                                        $nothiNoteSignaturesTable->save($nothiNoteSignatures);
                                    } else {
                                        // Has a signature =0
                                        if ($lastSignature['nothi_note_id'] == $lastNote['id']) {
                                            // Last signature should be from current user
                                            if ($lastSignature->office_organogram_id == $user['office_unit_organogram_id']) {
                                                $lastSignature->office_id = $user['office_id'];
                                                $lastSignature->employee_id = $employee['personal_info']['id'];
                                                $lastSignature->office_unit_id = $user['office_unit_id'];
                                                $lastSignature->office_organogram_id = $user['office_unit_organogram_id'];
                                                $lastSignature->employee_designation = $user['designation_label'] . (!empty($user['incharge_label']) ? (" (" . $user['incharge_label'] . ')') : '') . (!empty($user['office_unit_name']) ? ("<br style='font-size: 8pt !important;'>" . $user['office_unit_name']) : '');
                                                $lastSignature->is_signature = 1;
                                                $lastSignature->can_delete = 1;
                                                $lastSignature->signature_date = date("Y-m-d H:i:s");
                                                $lastSignature->created = date("Y-m-d H:i:s");
                                                $lastSignature->created_by = $this->Auth->user('id');
                                                $lastSignature->modified_by = $this->Auth->user('id');

                                                $nothiNoteSignaturesTable->save($lastSignature);
                                            }
                                        } else {
                                            //User has signature = 0 but not in last onucched
                                            if ($lastSignature->office_organogram_id == $user['office_unit_organogram_id']) {
                                                $lastSignature->office_id = $user['office_id'];
                                                $lastSignature->employee_id = $employee['personal_info']['id'];
                                                $lastSignature->office_unit_id = $user['office_unit_id'];
                                                $lastSignature->office_organogram_id = $user['office_unit_organogram_id'];
                                                $lastSignature->employee_designation = $user['designation_label'] . (!empty($user['incharge_label']) ? (" (" . $user['incharge_label'] . ')') : '') . (!empty($user['office_unit_name']) ? ("<br style='font-size: 8pt !important;'>" . $user['office_unit_name']) : '');
                                                $lastSignature->is_signature = 1;
                                                $lastSignature->can_delete = 1;
                                                $lastSignature->cross_signature = 1;
                                                $lastSignature->signature_date = date("Y-m-d H:i:s");
                                                $lastSignature->created = date("Y-m-d H:i:s");
                                                $lastSignature->created_by = $this->Auth->user('id');
                                                $lastSignature->modified_by = $this->Auth->user('id');

                                                $nothiNoteSignaturesTable->save($lastSignature);
                                                $nothiNoteSignatures = $nothiNoteSignaturesTable->newEntity();
                                                $nothiNoteSignatures->nothi_master_id = $last_onucched_info['nothi_master_id'];
                                                $nothiNoteSignatures->nothi_part_no = $nothimaster;
                                                $nothiNoteSignatures->office_id = $user['office_id'];
                                                $nothiNoteSignatures->employee_id = $employee['personal_info']['id'];
                                                $nothiNoteSignatures->office_unit_id = $user['office_unit_id'];
                                                $nothiNoteSignatures->office_organogram_id
                                                    = $user['office_unit_organogram_id'];
                                                $nothiNoteSignatures->employee_designation
                                                    = $user['designation_label'] . (!empty($user['incharge_label']) ? (" (" . $user['incharge_label'] . ')') : '') . (!empty($user['office_unit_name']) ? ("<br style='font-size: 8pt !important;'>" . $user['office_unit_name']) : '');

                                                $nothiNoteSignatures->nothi_note_id = !empty($lastNote['id'])
                                                    ? intval($lastNote['id']) : 0;
                                                $nothiNoteSignatures->note_decision = '';
                                                $nothiNoteSignatures->cross_signature = 0;
                                                $nothiNoteSignatures->is_signature = 1;
                                                $nothiNoteSignatures->can_delete = 1;
                                                $nothiNoteSignatures->signature_date = date("Y-m-d H:i:s");
                                                $nothiNoteSignatures->created = date("Y-m-d H:i:s");
                                                $nothiNoteSignatures->created_by = $this->Auth->user('id');
                                                $nothiNoteSignatures->modified_by = $this->Auth->user('id');

                                                $nothiNoteSignaturesTable->save($nothiNoteSignatures);
                                            }
                                        }
                                    }
                                }
                                //                            }
                                //end
                                $NothiNotesTable->updateAll(['note_status' => 'INBOX'], ['nothi_part_no' => $nothimaster]);
                                /**
                                 * Update Nisponno Field for dak in this nothi
                                 */
                                $tableNothiDakPotro = TableRegistry::get('NothiDakPotroMaps');
                                $tableNothiDakPotro->UpdateNisponnoField($nothimaster);
                                /**
                                 * Update Notes status Field
                                 */
                                // only for digital signature -- as signature set in last step so update signature will be done in the end process
                                if (!empty($note_ids)) {
                                    $nothiNoteSignaturesTable->updateAll(['digital_sign' => 1], ['digital_sign' => 0, 'nothi_note_id IN' => $note_ids, 'office_organogram_id' => $user['office_unit_organogram_id'], 'cross_signature' => 0, 'nothi_part_no' => $nothimaster]);
                                }
                                // only for digital signature

                                $this->notifyFeedbackOnNisponno($nothimaster, '');

                                $return = array('status' => 'success', 'msg' => 'নোট নিষ্পন্ন করা হয়েছে');
                            } catch (\Exception $ex) {
                                $return = array('status' => 'success', 'msg' => 'নোট নিষ্পন্ন করা সম্ভব হচ্ছে না।');
                            }
                        } else {
                            $return = array('status' => 'error', 'msg' => 'দুঃখিত! নোট পূর্বে থেকেই নিষ্পন্ন।');
                        }

                    }


                } catch (\Exception $ex) {
                    $return = array('status' => 'error', 'msg' => 'দুঃখিত! নোট নিষ্পন্ন করা সম্ভব হচ্ছে না। ');
                }
            }
        }

        echo json_encode($return);

        die;
    }

    public function nextPendingNote()
    {


        $employee_office = $this->getCurrentDakSection();

        $nothiCurrentUserTable = TableRegistry::get('NothiMasterCurrentUsers');

        $currentNote = $nothiCurrentUserTable->getFirstPendingNote($employee_office['office_unit_organogram_id']);

        if (!empty($currentNote)) {
            $this->Flash->success('পরবর্তী  পেন্ডিং নথিতে নিয়ে আসা হয়েছে');
            $this->response->body(json_encode(array(
                'status' => 'success',
                'note' => $currentNote
            )));
        } else {
            $this->response->body(json_encode(array(
                'status' => 'error',
                'msg' => "কোনো পেন্ডিং নথি পাওয়া যায়নি"
            )));
        }

        $this->response->type('application/json');
        return $this->response;
    }

    public function nextPendingNoteSequentially()
    {
        $employee_office = $this->getCurrentDakSection();

        $nothiMovementsTable = TableRegistry::get('NothiMasterMovements');
        $lastNote = $nothiMovementsTable->find()->where(['from_officer_designation_id' => $employee_office['office_unit_organogram_id']])->order(['created desc'])->first();

        $nisponnoRecordsTable = TableRegistry::get('NisponnoRecords');
        $lastNisponno = $nisponnoRecordsTable->find()->where(['designation_id' => $employee_office['office_unit_organogram_id']])->order(['created desc'])->first();
        if ($lastNisponno && $lastNote) {
            $lastNisponno = $lastNisponno->toArray();
            $lastNote = $lastNote->toArray();

            if ($lastNote['created'] > $lastNisponno['created']) {
                $last_note_info = [
                    'office_id' => $lastNote['from_office_id'],
                    'nothi_master_id' => $lastNote['nothi_master_id'],
                    'nothi_part_no' => $lastNote['nothi_part_no'],
                ];
            } else {
                $last_note_info = [
                    'office_id' => $lastNisponno['office_id'],
                    'nothi_master_id' => $lastNisponno['nothi_master_id'],
                    'nothi_part_no' => $lastNisponno['nothi_part_no'],
                ];
            }
        } else {
            if ($lastNote) {
                $lastNote = $lastNote->toArray();
                $last_note_info = [
                    'office_id' => $lastNote['from_office_id'],
                    'nothi_master_id' => $lastNote['nothi_master_id'],
                    'nothi_part_no' => $lastNote['nothi_part_no'],
                ];
            } elseif ($lastNisponno) {
                $lastNisponno = $lastNisponno->toArray();
                $last_note_info = [
                    'office_id' => $lastNisponno['office_id'],
                    'nothi_master_id' => $lastNisponno['nothi_master_id'],
                    'nothi_part_no' => $lastNisponno['nothi_part_no'],
                ];
            } else {
                $nothiCurrentUserTable = TableRegistry::get('NothiMasterCurrentUsers');
                $currentNote = $nothiCurrentUserTable->getFirstPendingNote($employee_office['office_unit_organogram_id']);
                if (!empty($currentNote)) {
                    $this->Flash->success('পরবর্তী  পেন্ডিং নথিতে নিয়ে আসা হয়েছে');
                    $this->response->body(json_encode(array(
                        'status' => 'success',
                        'note' => $currentNote
                    )));
                } else {
                    $this->response->body(json_encode(array(
                        'status' => 'error',
                        'msg' => "কোনো পেন্ডিং নথি পাওয়া যায়নি"
                    )));
                }
                $this->response->type('application/json');
                return $this->response;
            }
        }

        $nothiCurrentUserTable = TableRegistry::get('NothiMasterCurrentUsers');
        $currentNote = $nothiCurrentUserTable->find()->select([
            'nothi_master_id',
            'nothi_part_no',
            'nothi_office',
            'view_status',
            'is_new',
        ])->join([
            "NothiNotes" => [
                'table' => 'nothi_notes', 'type' => 'inner',
                'conditions' => ['NothiNotes.nothi_part_no =NothiMasterCurrentUsers.nothi_part_no']
            ]
        ])->where(['office_unit_organogram_id' => $employee_office['office_unit_organogram_id'], 'is_archive' => 0, 'is_finished' => 0])->order(['NothiMasterCurrentUsers.priority desc', 'NothiMasterCurrentUsers.id asc']);
        if (count($currentNote) == 0) {
            $currentNote = $currentNote->where(['nothi_office' => $last_note_info['office_id'], 'NothiMasterCurrentUsers.nothi_master_id' => $last_note_info['nothi_master_id']]);
        }

        $first_note = '';
        if (count($currentNote->toArray()) != 0) {
            foreach ($currentNote->toArray() as $notes) {
                if ($first_note == '') {
                    $first_note = $notes;
                }
                if ($notes['nothi_part_no'] >= $last_note_info['nothi_part_no']) {
                    $first_note = $notes;
                    break;
                }
            }
        } else {
            $currentNote = $nothiCurrentUserTable->find()->select([
                'nothi_master_id',
                'nothi_part_no',
                'nothi_office',
                'view_status',
                'is_new',
            ])->join([
                "NothiNotes" => [
                    'table' => 'nothi_notes', 'type' => 'inner',
                    'conditions' => ['NothiNotes.nothi_part_no =NothiMasterCurrentUsers.nothi_part_no']
                ]
            ])->where(['office_unit_organogram_id' => $employee_office['office_unit_organogram_id'], 'is_archive' => 0, 'is_finished' => 0])->order(['NothiMasterCurrentUsers.id asc'])
                ->where(['nothi_office' => $last_note_info['office_id']]);
            if (count($currentNote->toArray()) != 0) {
                foreach ($currentNote->toArray() as $notes) {
                    if ($first_note == '') {
                        $first_note = $notes;
                    }
                    if ($notes['nothi_master_id'] >= $last_note_info['nothi_master_id']) {
                        $first_note = $notes;
                        break;
                    }
                }
            }
        }
        if ($first_note != '') {
            $this->Flash->success('পরবর্তী  পেন্ডিং নথিতে নিয়ে আসা হয়েছে');
            $this->response->body(json_encode(array(
                'status' => 'success',
                'note' => $first_note
            )));
        } else {
            $this->response->body(json_encode(array(
                'status' => 'error',
                'msg' => "কোনো পেন্ডিং নথি পাওয়া যায়নি"
            )));
        }

        $this->response->type('application/json');
        return $this->response;
    }

    public function ApiForwardNothiMaster($nothi_part_id, $nothi_office)
    {
        $this->digitalSigantureUrlChecker();

        $user_designation = $this->request->query['user_designation'];
        $response = array(
            "status" => "error",
            "error_code" => 404,
            "msg" => "Invalid Request"
        );

        $apikey = isset($this->request->query['api_key']) ? $this->request->query['api_key']
            : '';
        if (empty($apikey) || $this->checkToken($apikey) == FALSE) {
            goto rtn;
        }

        if (!defined("PROJAPOTI_API") || PROJAPOTI_API == false) {
            goto rtn;
        }
        if (empty($user_designation)) {
            goto rtn;
        }
        if (empty($nothi_part_id)) {
            goto rtn;
        }
        if ($apikey != API_KEY) {
            //verify api token
            $this->loadComponent('ApiRelated');
            $getApiTokenData = $this->ApiRelated->getMobileApiTokenData($apikey);
            if (!empty($getApiTokenData)) {
                if (!in_array($user_designation, $getApiTokenData['designations'])) {
                    $response['msg'] = 'Unauthorized request';
                    goto rtn;
                }
            }
            //verify api token
        }
//        $EmployeeOfficesTable = TableRegistry::get('EmployeeOffices');
//        $employee_office = $EmployeeOfficesTable->find()->where(['office_unit_organogram_id' => $user_designation,
//            'status' => 1])->first();
        $employee_office = $this->setCurrentDakSection($user_designation);
        $request_success = 0;
        //need to check receiver DB connection
        if (!empty($this->request->data['to_office_id']) && !empty($this->request->data['to_officer_designation_id']) && !empty($this->request->data['to_office_unit_id'])) {
            $this->switchOffice($this->request->data['to_office_id'], 'MainNothiOffice');
            $checkDBConnection = $this->checkCurrentOfficeDBConnection();
            if (empty($checkDBConnection) || (!empty($checkDBConnection['status']) && $checkDBConnection['status'] == 'error')) {
                $response['msg'] = 'দুঃখিত! প্রেরকের ডাটাবেস (আইডিঃ ' . $this->request->data['to_office_id'] . ') সংযোগ সম্ভব হয়নি।';
                goto rtn;
            }
        } else {
            $response['msg'] = 'দুঃখিত! প্রেরকের তথ্য পাওয়া যায়নি';
            goto rtn;
        }

        $this->switchOffice($nothi_office, 'MainNothiOffice');
        $checkDBConnection = $this->checkCurrentOfficeDBConnection();
        if (empty($checkDBConnection) || (!empty($checkDBConnection['status']) && $checkDBConnection['status'] == 'error')) {
            $response['msg'] = 'দুঃখিত! ডাটাবেস (আইডিঃ ' . $nothi_office . ') সংযোগ সম্ভব হয়নি।';
            goto rtn;
        }
        $otherNothi = false;
        if ($nothi_office != 0 && $nothi_office != $employee_office['office_id']) {
            $otherNothi = true;
        }

        TableRegistry::remove('NothiParts');
        $nothiPartsTable = TableRegistry::get('NothiParts');
        $employee_record_table = TableRegistry::get('EmployeeRecords');

        $employee = $employee_record_table->getEmployeeInfoByRecordId($employee_office['employee_record_id']);

        $usersTable = TableRegistry::get('Users');
        $users = $usersTable->find()->where(['employee_record_id' => $employee_office['employee_record_id']])->first();
        $username = $users['username'];
        $options['token'] = sGenerateToken(['file' => $username], ['exp' => time() + 60 * 300]);
        $data = $this->getSignature($username, 1, 0, true, null, $options);


        if ($data == false) {
            $response['msg'] = 'দুঃখিত! আপনার স্বাক্ষর পাওয়া যায়নি। অনুগ্রহ করে সিস্টেমে স্বাক্ষর আপলোড করুন। ধন্যবাদ।';
            goto rtn;
        }

        $api_nothi_part_flag_table = TableRegistry::get('ApiNothiPartFlag');
        $nothi_part_flag_count = $api_nothi_part_flag_table->find()->where(['part_id'=>$nothi_part_id,'nothi_office_id' => $nothi_office,'office_unit_organogram_id'=>$user_designation])->first();
        if(!empty($nothi_part_flag_count)){
            if($nothi_part_flag_count['type']== 1){
                $response['msg'] = 'দুঃখিত! খসড়াপত্র সংরক্ষন না করে নথি প্রেরন করা যাবে না। অনুগ্রহপুর্বক খসড়াপত্রটি সংরক্ষন করুন।';
            } else {
                $response['msg'] = 'দুঃখিত! অনুচ্ছেদ সংরক্ষন না করে নথি প্রেরন করা যাবে না। অনুগ্রহপুর্বক অনুচ্ছেদটি সংরক্ষন করুন।';
            }
            goto rtn;
        }
        $real_nothi_master_id = 0;
        try {
            $conn = ConnectionManager::get('default');
            $conn->begin();
            TableRegistry::remove('NothiMasterCurrentUsers');
            $nothiMasterCurrentUserTable = TableRegistry::get('NothiMasterCurrentUsers');

            $nothiMastersCurrentUser = $nothiMasterCurrentUserTable->find()->where(['nothi_part_no' => $nothi_part_id,
                'office_unit_organogram_id' => $employee_office['office_unit_organogram_id'],
                'nothi_office' => $nothi_office])->first();

            if (empty($nothiMastersCurrentUser)) {
                $conn->rollback();
                $response['msg'] = 'দুঃখিত! নথি পরবর্তী কার্যক্রমের জন্য প্রেরণ করা সম্ভব হচ্ছে না';
                $response['reason'] = 'No data found';
               goto rtn;
            } else {
                $real_nothi_master_id = $nothiMastersCurrentUser['nothi_master_id'];
                $is_finished = (isset($nothiMastersCurrentUser['is_finished']) ? $nothiMastersCurrentUser['is_finished'] : 0);
            }


            if ($nothiMastersCurrentUser['nothi_office'] != 0 && $nothiMastersCurrentUser['nothi_office']
                != $employee_office['office_id']
            ) {
                $this->switchOffice($nothiMastersCurrentUser['nothi_office'],
                    'NothiOffice');
            }

            TableRegistry::remove('NothiMasterMovements');
            TableRegistry::remove('NothiNoteSignatures');
            TableRegistry::remove('NothiNotes');
            TableRegistry::remove('Potrojari');
            TableRegistry::remove('PotrojariVersions');
            TableRegistry::remove('NothiPotroAttachments');
            $nothiMovementsTable = TableRegistry::get('NothiMasterMovements');
            TableRegistry::remove('NothiMasterPermissions');
            TableRegistry::remove('NothiPotroAttachments');
            $nothiMasterPermissionTable = TableRegistry::get('NothiMasterPermissions');
            $nothiNoteSignaturesTable = TableRegistry::get('NothiNoteSignatures');
            $nothiNotesTable = TableRegistry::get('NothiNotes');
            $officeTable = TableRegistry::get('Offices');
            $potrojariTable = TableRegistry::get('Potrojari');
            $officeUnitTable = TableRegistry::get('OfficeUnits');
            $potrojariVersionTable = TableRegistry::get('PotrojariVersions');
            $nothiPotroAttachmentsTable = TableRegistry::get('NothiPotroAttachments');

            $nothiPartsInformation = $nothiPartsTable->get($nothi_part_id);
            $nothi_master_id = $nothiPartsInformation['nothi_masters_id'];
            $nothi_no = $nothiPartsInformation['nothi_no'];
            $subject = $nothiPartsInformation['subject'];
            $creator_unit_id = $nothiPartsInformation['office_units_id'];

            $hasAccess = $nothiMasterPermissionTable->hasAccessList($nothi_office,
                $this->request->data['to_office_id'],
                $this->request->data['to_office_unit_id'],
                $this->request->data['to_officer_designation_id'],
                $nothi_master_id, $nothi_part_id);

            if (empty($hasAccess)) {
                $conn->rollback();
                $response['msg'] = 'দুঃখিত! নথি পরবর্তী কার্যক্রমের জন্য প্রেরণ করা সম্ভব হচ্ছে না';
                $response['reason'] = 'No access';
            }
            /*
             * Check If there is notes or not
             */
            $hasOnucched = $nothiNotesTable->checkEmptyNote($nothi_master_id, $nothi_part_id);
            if ($hasOnucched == 0) {
                $conn->rollback();
                $response['msg'] = 'কোন অনুচ্ছেদ না দিয়ে প্রেরণ করা সম্ভব নয়। দয়া করে নোটে অনুচ্ছেদ দিন';
                goto rtn;
            }

            $nothiPartsInformation->modified = date("Y-m-d H:i:s");
            $nothiPartsTable->save($nothiPartsInformation);
            /*
                 * Time to check digital signature
             * skip if soft token - 1
                 */
            if ($employee['default_sign'] > 0 && isset($this->request->data['soft_token']) && $this->request->data['soft_token'] != -1) {
                if ($employee['default_sign'] == 1 && empty($this->request->data['soft_token'])) {
                    $conn->rollback();
                    $response['msg'] = 'দুঃখিত! সফট টোকেন দেওয়া হয়নি';
                    goto rtn;
                }
                $allNotesToSign = $nothiNotesTable->find('list', ['keyField' => 'id', 'valueField' => 'note_description'])->where(['note_status' => 'DRAFT', 'nothi_part_no' => $nothi_part_id, 'office_organogram_id' => $employee_office['office_unit_organogram_id']])->toArray();
                $this->loadComponent('DigitalSignatureRelated');
                $res = $this->DigitalSignatureRelated->signOnucched($allNotesToSign, $this->request->data['soft_token'], $nothi_part_id, $employee_office['default_sign'], $employee_office, $nothi_office);
                if ($res['status'] == 'error') {
                    $conn->rollback();
                    $response['msg'] = 'দুঃখিত! ডিজিটাল সিগনেচার করা সম্ভব হয়নি। কারণঃ ' . $res['msg'];
                    goto rtn;
                } else {
                    $note_ids = $res['note_ids'];
                }

            }

            //check for draft potro

            $searchForDraft = $potrojariTable->getInfo([
                'nothi_part_no' => $nothi_part_id,
                'potro_status IN' => ['Draft', 'SummaryDraft']
            ])->toArray();

            if (!empty($searchForDraft)) {
                foreach ($searchForDraft as $key => $drafts) {
                    $draftVersion = $potrojariVersionTable->newEntity();
                    $draftVersion->potrojari_id = $drafts->id;
                    $draftVersion->nothi_master_id = $drafts->nothi_master_id;
                    $draftVersion->nothi_part_no = $drafts->nothi_part_no;
                    $draftVersion->updated_content = $drafts->potro_description;
                    $draftVersion->office_id = $employee_office['office_id'];
                    $draftVersion->officer_id = $employee_office['employee_record_id'];
                    $draftVersion->officer_name = $employee['name_bng'];
                    $draftVersion->officer_unit_id = $employee_office['office_unit_id'];
                    $draftVersion->office_organogram_id = $employee_office['office_unit_organogram_id'];
                    $draftVersion->designation_label = $employee_office['designation'];

                    if ($drafts['potro_status'] == 'SummaryDraft') {
                        $hasSummary = 1;
                    }

                    if(!$potrojariVersionTable->save($draftVersion)){
                        $conn->rollback();
                        $response['msg'] = __('Technical error happen');
                        $response['reason'] = 'Error Code: API Nothi FORWARD - 1';
                        goto rtn;
                    }
                }
            }

            //check for summary
            if (!empty($hasSummary)) {
                $hasSummary = $nothiPotroAttachmentsTable->find()->where(['is_summary_nothi' => 1, 'nothi_part_no' => $nothi_part_id])->count();
            }

            //movements
            $unitinfomation = $officeUnitTable->get($employee_office['office_unit_id']);
            $toOfficeName = $officeTable->get($this->request->data['to_office_id']);

            $first_move = $nothiMovementsTable->newEntity();
            $first_move->nothi_master_id = $nothi_master_id;
            $first_move->nothi_part_no = $nothi_part_id;
            $first_move->nothi_office = $nothi_office;
            $first_move->from_office_id = $employee_office['office_id'];
            $first_move->from_officer_id = $employee['id'];
            $first_move->from_office_unit_id = $employee_office['office_unit_id'];
            $first_move->from_officer_designation_id = $employee_office['office_unit_organogram_id'];
            $first_move->from_office_name = $employee_office['office_name'];
            $first_move->from_office_unit_name = $unitinfomation['unit_name_bng'];
            $first_move->from_officer_name = $employee['name_bng'];
            $first_move->from_officer_designation_label = $employee_office['designation'] . (!empty($employee_office['incharge_label']) ? (" (" . $employee_office['incharge_label'] . ')') : '');

            $first_move->to_office_id = $this->request->data['to_office_id'];
            $first_move->to_office_name = (!empty($toOfficeName) ? $toOfficeName['office_name_bng']
                : '');
            $first_move->to_office_unit_id = $this->request->data['to_office_unit_id'];
            $first_move->to_office_unit_name = $this->request->data['to_office_unit_name'];
            $first_move->to_officer_id = $this->request->data['to_officer_id'];
            $first_move->to_officer_name = $this->request->data['to_officer_name'];
            $first_move->to_officer_designation_id = $this->request->data['to_officer_designation_id'];
            $first_move->to_officer_designation_label = $this->request->data['to_officer_designation_label'] . (!empty($this->request->data['to_officer_incharge_label']) ? (" (" . $this->request->data['to_officer_incharge_label'] . ')') : '');

            $first_move->view_status = 0;
            $first_move->created_by = $employee['id'];
            $first_move->modified_by = $employee['id'];
            $first_move->created = date('Y-m-d H:i:s');
            $first_move->modified = date('Y-m-d H:i:s');

            if(!$nothiMovementsTable->save($first_move)){
                $conn->rollback();
                $response['msg'] = __('Technical error happen');
                $response['reason'] = 'Error Code: API Nothi FORWARD - 2';
                goto rtn;
            }
            //end
            //change current user --previous_code
//            $nothiMastersCurrentUser->office_id = $this->request->data['to_office_id'];
//            $nothiMastersCurrentUser->office_unit_id = $this->request->data['to_office_unit_id'];
//            $nothiMastersCurrentUser->office_unit_organogram_id = $this->request->data['to_officer_designation_id'];
//            $nothiMastersCurrentUser->employee_id = $this->request->data['to_officer_id'];
//            $nothiMastersCurrentUser->forward_date = $nothiMastersCurrentUser->issue_date;
//            $nothiMastersCurrentUser->issue_date = date("Y-m-d H:i:s");
//            $nothiMastersCurrentUser->is_archive = 0;
//            $nothiMastersCurrentUser->is_finished = 0;
//            $nothiMastersCurrentUser->view_status = 0;
//            $nothiMastersCurrentUser->is_new = 0;
//
//            $nothiMasterCurrentUserTable->save($nothiMastersCurrentUser);
            //end -- previous_code
            $hasAccess->visited = 1;

            if(!$nothiMasterPermissionTable->save($hasAccess)){
                $conn->rollback();
                $response['msg'] = __('Technical error happen');
                $response['reason'] = 'Error Code: API Nothi FORWARD - 3';
                goto rtn;
            }
            //keep signature on note
            $lastNote = $nothiNotesTable->find()->select(['id'])->where(['nothi_part_no' => $nothi_part_id, 'note_no >=' => 0])->order(['id DESC'])->first();

            //sometimes error occurred. Receiver Signature already Exist as last signature.Throw Exception
            $lastSignature_2_check_receiver_sign= $nothiNoteSignaturesTable->getLastSignature(0,
                $nothi_part_id, $lastNote['id'])->order(['id desc'])->first();
            if(!empty($lastSignature_2_check_receiver_sign)){
                if($lastSignature_2_check_receiver_sign['office_organogram_id'] == $this->request->data['to_officer_designation_id']){
                    //something wrong . Delete this signature
                    $conn->rollback();
                    $nothiNoteSignaturesTable->deleteAll(['id' =>$lastSignature_2_check_receiver_sign['id']]);
                    $response['msg'] = __('Technical error happen');
                    $response['reason'] = 'Error Code: Nothi FORWARD - 10';
                    goto rtn;
                }
            }
            //sometimes error occurred. Receiver Signature already Exist as last signature.Throw Exception

            //save own signature
            $lastSignature = $nothiNoteSignaturesTable->getByEmployeeId($employee_office['office_unit_organogram_id'],
                $nothi_part_id)->first();
            $lastCrossSignature = ($lastSignature['nothi_note_id'] == $lastNote['id']) ? 0 : $lastSignature['cross_signature'];

            if (empty($lastNote)) {
                $lastNote['id'] = 0;
            }
            //get last signature of that note for is_signature = 1and cross_signature = 0
            $lastSignatureFor_is_Signature_1 = $nothiNoteSignaturesTable->getLastSignature($employee_office['office_unit_organogram_id'],
                $nothi_part_id, $lastNote['id'])->where(['is_signature' => 1, 'cross_signature' => 0])->order(['id desc'])->first();
            $f = true;
            if (!empty($lastSignatureFor_is_Signature_1)) {
                /**
                 * *Note nisponno put signature on that note of this user.
                 * Checking if user sending note after nisponno that note as it will give another signature in that note.
                 */
                if ($lastSignatureFor_is_Signature_1['nothi_note_id'] == $lastNote['id']
                    && $lastSignatureFor_is_Signature_1['office_organogram_id']
                    == $employee_office['office_unit_organogram_id']
                ) {
                    $f = false;
                }
            }
            if (!empty($lastSignature)) {
                $nothiNoteSignaturesTable->updateAll([
                    'is_signature' => 1,
                    'cross_signature' => $lastCrossSignature,
                    'office_id' => $employee_office['office_id'],
                    'office_unit_id' => $employee_office['office_unit_id'],
                    'employee_id' => $employee['id'],
                    'signature_date' => date("Y-m-d H:i:s"),
                    'employee_designation' => $employee_office['designation_label'] . (!empty($employee_office['incharge_label']) ? (" (" . $employee_office['incharge_label'] . ')') : '') . (!empty($employee_office['office_unit_name']) ? ("<br style='font-size: 8pt !important;'>" . $employee_office['office_unit_name']) : ''),
                ], ['id' => $lastSignature['id']]);
            } else {
                $lastSignature_singed = $nothiNoteSignaturesTable->getByEmployeeId($employee_office['office_unit_organogram_id'], $nothi_part_id, 1)->first();
                if (!empty($lastSignature_singed)) {
                    $nothiNoteSignaturesTable->updateAll([
                        'signature_date' => date("Y-m-d H:i:s")
                    ], ['id' => $lastSignature_singed['id']]);
                }
            }
            if ($f == true) {
                if (empty($lastSignature)) {
                    $nothiNoteSignatures = $nothiNoteSignaturesTable->newEntity();
                    $nothiNoteSignatures->nothi_master_id = $nothi_master_id;
                    $nothiNoteSignatures->nothi_part_no = $nothi_part_id;
                    $nothiNoteSignatures->office_id = $employee_office['office_id'];
                    $nothiNoteSignatures->employee_id = $employee['id'];
                    $nothiNoteSignatures->office_unit_id = $employee_office['office_unit_id'];
                    $nothiNoteSignatures->office_organogram_id = $employee_office['office_unit_organogram_id'];
                    $nothiNoteSignatures->employee_designation = $employee_office['designation'] . (!empty($employee_office['incharge_label']) ? (" (" . $employee_office['incharge_label'] . ')') : '') . (!empty($employee_office['office_unit_name']) ? ("<br style='font-size: 8pt !important;'>" . $employee_office['office_unit_name']) : '');

                    $nothiNoteSignatures->nothi_note_id = !empty($lastNote['id']) ? intval($lastNote['id'])
                        : 0;
                    $nothiNoteSignatures->note_decision = '';
                    $nothiNoteSignatures->cross_signature = 0;
                    $nothiNoteSignatures->is_signature = 1;
                    $nothiNoteSignatures->signature_date = date("Y-m-d H:i:s");
                    $nothiNoteSignatures->created = date("Y-m-d H:i:s");
                    $nothiNoteSignatures->created_by = $employee['id'];
                    $nothiNoteSignatures->modified_by = $employee['id'];

                    if(!$nothiNoteSignaturesTable->save($nothiNoteSignatures)){
                        $conn->rollback();
                        $response['msg'] = __('Technical error happen');
                        $response['reason'] = 'Error Code: API Nothi FORWARD - 3';
                        goto rtn;
                    }
                } else {

                    if ($lastSignature->nothi_note_id == $lastNote['id']) {
                        if ($lastSignature->office_organogram_id == $employee_office['office_unit_organogram_id']) {
                            $lastSignature->office_id = $employee_office['office_id'];
                            $lastSignature->employee_id = $employee['id'];
                            $lastSignature->office_unit_id = $employee_office['office_unit_id'];
                            $lastSignature->office_organogram_id = $employee_office['office_unit_organogram_id'];
                            $lastSignature->employee_designation = $employee_office['designation'] . (!empty($employee_office['incharge_label']) ? (" (" . $employee_office['incharge_label'] . ')') : '') . (!empty($employee_office['office_unit_name']) ? ("<br style='font-size: 8pt !important;'>" . $employee_office['office_unit_name']) : '');
                            $lastSignature->is_signature = 1;
                            $lastSignature->signature_date = date("Y-m-d H:i:s");
                            $lastSignature->created = date("Y-m-d H:i:s");
                            $lastSignature->created_by = $employee['id'];
                            $lastSignature->modified_by = $employee['id'];

                            if(!$nothiNoteSignaturesTable->save($lastSignature)){
                                $conn->rollback();
                            $response['msg'] = __('Technical error happen');
                            $response['reason'] = 'Error Code: API Nothi FORWARD - 4';
                            goto rtn;
                            }
                        } else {

                            $nothiNoteSignatures = $nothiNoteSignaturesTable->newEntity();
                            $nothiNoteSignatures->nothi_master_id = $nothi_master_id;
                            $nothiNoteSignatures->nothi_part_no = $nothi_part_id;
                            $nothiNoteSignatures->office_id = $employee_office['office_id'];
                            $nothiNoteSignatures->employee_id = $employee['id'];
                            $nothiNoteSignatures->office_unit_id = $employee_office['office_unit_id'];
                            $nothiNoteSignatures->office_organogram_id = $employee_office['office_unit_organogram_id'];
                            $nothiNoteSignatures->employee_designation = $employee_office['designation'] . (!empty($employee_office['incharge_label']) ? (" (" . $employee_office['incharge_label'] . ')') : '') . (!empty($employee_office['office_unit_name']) ? ("<br style='font-size: 8pt !important;'>" . $employee_office['office_unit_name']) : '');

                            $nothiNoteSignatures->nothi_note_id = !empty($lastNote['id'])
                                ? intval($lastNote['id']) : 0;
                            $nothiNoteSignatures->note_decision = '';
                            $nothiNoteSignatures->cross_signature = 0;
                            $nothiNoteSignatures->is_signature = 1;
                            $nothiNoteSignatures->signature_date = date("Y-m-d H:i:s");
                            $nothiNoteSignatures->created = date("Y-m-d H:i:s");
                            $nothiNoteSignatures->created_by = $employee['id'];
                            $nothiNoteSignatures->modified_by = $employee['id'];

                           if(!$nothiNoteSignaturesTable->save($nothiNoteSignatures)){
                               $conn->rollback();
                               $response['msg'] = __('Technical error happen');
                               $response['reason'] = 'Error Code: API Nothi FORWARD - 5';
                               goto rtn;
                           }
                        }
                    } else {
                        if ($lastSignature->office_organogram_id == $employee_office['office_unit_organogram_id']) {
                            $lastSignature->office_id = $employee_office['office_id'];
                            $lastSignature->employee_id = $employee['id'];
                            $lastSignature->office_unit_id = $employee_office['office_unit_id'];
                            $lastSignature->office_organogram_id = $employee_office['office_unit_organogram_id'];
                            $lastSignature->employee_designation = $employee_office['designation'] . (!empty($employee_office['incharge_label']) ? (" (" . $employee_office['incharge_label'] . ')') : '') . (!empty($employee_office['office_unit_name']) ? ("<br style='font-size: 8pt !important;'>" . $employee_office['office_unit_name']) : '');
                            $lastSignature->is_signature = 1;
                            $lastSignature->cross_signature = 1;
                            $lastSignature->signature_date = date("Y-m-d H:i:s");
                            $lastSignature->created = date("Y-m-d H:i:s");
                            $lastSignature->created_by = $employee['id'];
                            $lastSignature->modified_by = $employee['id'];

                            if(!$nothiNoteSignaturesTable->save($lastSignature)){
                                $conn->rollback();
                                $response['msg'] = __('Technical error happen');
                                $response['reason'] = 'Error Code: API Nothi FORWARD - 6';
                                goto rtn;
                            }
                        }
                        $nothiNoteSignatures = $nothiNoteSignaturesTable->newEntity();
                        $nothiNoteSignatures->nothi_master_id = $nothi_master_id;
                        $nothiNoteSignatures->nothi_part_no = $nothi_part_id;
                        $nothiNoteSignatures->office_id = $employee_office['office_id'];
                        $nothiNoteSignatures->employee_id = $employee['id'];
                        $nothiNoteSignatures->office_unit_id = $employee_office['office_unit_id'];
                        $nothiNoteSignatures->office_organogram_id = $employee_office['office_unit_organogram_id'];
                        $nothiNoteSignatures->employee_designation = $employee_office['designation'] . (!empty($employee_office['incharge_label']) ? (" (" . $employee_office['incharge_label'] . ')') : '') . (!empty($employee_office['office_unit_name']) ? ("<br style='font-size: 8pt !important;'>" . $employee_office['office_unit_name']) : '');

                        $nothiNoteSignatures->nothi_note_id = !empty($lastNote['id'])
                            ? intval($lastNote['id']) : 0;
                        $nothiNoteSignatures->note_decision = '';
                        $nothiNoteSignatures->is_signature = 1;
                        $nothiNoteSignatures->signature_date = date("Y-m-d H:i:s");
                        $nothiNoteSignatures->created = date("Y-m-d H:i:s");
                        $nothiNoteSignatures->created_by = $employee['id'];
                        $nothiNoteSignatures->modified_by = $employee['id'];

                        if(!$nothiNoteSignaturesTable->save($nothiNoteSignatures)){
                            $conn->rollback();
                        $response['msg'] = __('Technical error happen');
                        $response['reason'] = 'Error Code: API Nothi FORWARD - 7';
                        goto rtn;
                        }
                    }
                }
            }
            //end

            $nothiNoteSignatures = $nothiNoteSignaturesTable->newEntity();
            $nothiNoteSignatures->nothi_master_id = $nothi_master_id;
            $nothiNoteSignatures->nothi_part_no = $nothi_part_id;
            $nothiNoteSignatures->office_id = $this->request->data['to_office_id'];
            $nothiNoteSignatures->employee_id = $this->request->data['to_officer_id'];
            $nothiNoteSignatures->office_unit_id = $this->request->data['to_office_unit_id'];
            $nothiNoteSignatures->office_organogram_id = $this->request->data['to_officer_designation_id'];
            $nothiNoteSignatures->employee_designation = $this->request->data['to_officer_designation_label'] . (!empty($this->request->data['to_officer_incharge_label']) ? (" (" . $this->request->data['to_officer_incharge_label'] . ')') : '') . (!empty($this->request->data['to_office_unit_name']) ? ("<br style='font-size: 8pt !important;'>" . $this->request->data['to_office_unit_name']) : '');

            $nothiNoteSignatures->nothi_note_id = !empty($lastNote['id']) ? intval($lastNote['id'])
                : 0;
            $nothiNoteSignatures->note_decision = '';
            $nothiNoteSignatures->cross_signature = 0;
            $nothiNoteSignatures->is_signature = 0;
            $nothiNoteSignatures->signature_date = date("Y-m-d H:i:s");
            $nothiNoteSignatures->created = date("Y-m-d H:i:s");
            $nothiNoteSignatures->created_by = $employee['id'];
            $nothiNoteSignatures->modified_by = $employee['id'];

            if(!$nothiNoteSignaturesTable->save($nothiNoteSignatures)){
                $conn->rollback();
                $response['msg'] = __('Technical error happen');
                $response['reason'] = 'Error Code: API Nothi FORWARD - 7';
                goto rtn;
            }
            //end
            //change nothi notes status
            $nothiNotesTable->updateAll(['note_status' => 'INBOX'], ['nothi_part_no' => $nothi_part_id]);

            //change nothi movements view status
            $nothiMovementsTable->updateAll(['view_status' => 1],
                ['nothi_part_no' => $nothi_part_id, 'to_officer_designation_id' => $employee_office['office_unit_organogram_id']]);



            //checking if new note is opening or not
            if ($is_finished == 1) {
                TableRegistry::get('NoteInitialize')->saveData(['nothi_masters_id' => $nothi_master_id, 'nothi_part_no' => $nothi_part_id, 'office_id' => $employee_office['office_id'], 'office_units_id' => $employee_office['office_unit_id'], 'office_units_organogram_id' => $employee_office['office_unit_organogram_id'], 'user_id' => $employee['id']]);
            }
            //checking if new note is opening or not
            // only for digital signature

            $current_office_section = $employee_office;
            $current_office_section['office_id'] = $employee_office['office_id'];
            $current_office_section['officer_id'] = $employee['id'];

            $sentUser['office_id'] = $this->request->data['to_office_id'];
            $sentUser['officer_id'] = $this->request->data['to_officer_id'];

            // as signature put in final step, update signature should be done after that.
            if (!empty($note_ids)) {
                TableRegistry::remove('NothiNoteSignatures');
                $nothiNoteSignaturesTable = TableRegistry::get('NothiNoteSignatures');
                $nothiNoteSignaturesTable->updateAll(['digital_sign' => 1], ['digital_sign' => 0, 'nothi_note_id IN' => $note_ids, 'office_organogram_id' => $employee_office['office_unit_organogram_id'], 'cross_signature' => 0, 'nothi_part_no' => $nothi_part_id]);
            }
            // only for digital signature

            if (!$this->NotificationSet('forward', array(1, "Nothi"), 2,
                $current_office_section, $current_office_section,
                $nothiPartsInformation['subject'])
            ) {

            }
            if (!$this->NotificationSet('inbox', array(1, "Nothi"), 2,
                $current_office_section, $sentUser,
                $nothiPartsInformation['subject'])
            ) {

            }
            // Give Receiver Notification
            $title = 'আপনার ডেস্কে নতুন নথি এসেছে';
            $mail_sender = [
                'sender' => h($employee['personal_info']['name_bng'] . ', ' . $employee_office['designation_label'] . ', ' . $employee_office['office_unit_name']),
                'subject' => h($nothiPartsInformation['subject']),
                'time' => Time::parse(date('Y-m-d H:i:s'))->i18nFormat(null, null, 'bn-BD'),
            ];
            $mail_sender_notify = 'প্রেরকঃ ' . $mail_sender['sender'] . "\n বিষয়ঃ " . h($mail_sender['subject']) . "\n সময়ঃ " . $mail_sender['time'];
            $this->notifyAppUser('', $sentUser['officer_id'], ['title' => $title, 'body' => $mail_sender], ['title' => $title, 'body' => $mail_sender_notify]);
            // Give Receiver Notification

                $conn->commit();
            }
        catch (\Exception $ex) {
                $conn->rollback();

            $response['msg'] = 'দুঃখিত! নথি পরবর্তী কার্যক্রমের জন্য প্রেরণ করা সম্ভব হচ্ছে না.';
            $response['reason'] = $this->makeEncryptedData($ex->getMessage());
            goto rtn;

            }

        // If nothi from other office we need to keep track of that nothi in current employee office DB - Start
        if ($otherNothi == true) {
            $this->saveOtherOfficeNothiMasterTransaction($nothi_master_id, $nothi_part_id, $nothi_office, $employee_office, $employee, $toOfficeName, $this->request->data, $nothi_no, $subject, $creator_unit_id);
        }
        // If nothi from other office we need to keep track of that nothi in current employee office DB - End

            TableRegistry::remove('NothiMasterPermissions');
            $nothiPriviligeTable = TableRegistry::get('NothiMasterPermissions');
            $allPermittedOfficesList = $nothiPriviligeTable->permittedOfficesId($nothi_part_id, $nothi_office);
            if (!in_array($nothi_office, $allPermittedOfficesList)) {
                $allPermittedOfficesList[$nothi_office] = $nothi_office;
            }
            $multiple_offices_permission = false;
            if (!empty($allPermittedOfficesList)) {
                if (count($allPermittedOfficesList) == 1 && current($allPermittedOfficesList) == $employee_office['office_id']) {
                    $multiple_offices_permission = true;
                }
                foreach ($allPermittedOfficesList as $key => $value) {
                    try {
                        if ($multiple_offices_permission == false) {
                            $is_connected = $this->switchOfficeWithStatus($key, 'MainNothiOffice', -1, true);
                            if (!$is_connected || (!empty($is_connected['status']) && $is_connected['status'] == 'error')) {
                                continue;
                            }
                            $checkDBConnection = $this->checkCurrentOfficeDBConnection();
                            if (empty($checkDBConnection) || (!empty($checkDBConnection['status']) && $checkDBConnection['status'] == 'error')) {
                                continue;
                            }
                        }
                        TableRegistry::remove('NothiMasterCurrentUsers');
                        $nothiMasterCurrentUsersTable = TableRegistry::get("NothiMasterCurrentUsers");
                        //first check if current note has entry
                        $hasDatainCurrentUser = $nothiMasterCurrentUsersTable->find()->where(
                            [
                                'nothi_part_no' => $nothi_part_id,
                                'nothi_office' => $nothi_office,
                                'nothi_master_id'=>$real_nothi_master_id
                            ]
                        )->count();
                        //change current user
                        if(!empty($hasDatainCurrentUser)){
                            $nothiMasterCurrentUsersTable->updateAll(
                                [
                                    'is_archive' => 0,
                                    'is_finished' => 0,
                                    'is_summary' => !empty($hasSummary) ? $hasSummary : 0,
                                    'office_id' => $this->request->data['to_office_id'],
                                    'office_unit_id' => $this->request->data['to_office_unit_id'],
                                    'office_unit_organogram_id' => $this->request->data['to_officer_designation_id'],
                                    'employee_id' => $this->request->data['to_officer_id'],
                                    'view_status' => 0,
                                    'issue_date' => date("Y-m-d H:i:s"),
                                    'forward_date' => date("Y-m-d H:i:s"),
                                    'is_new' => 0,
                                    'priority' => isset($this->request->data['priority']) ? $this->request->data['priority'] : 0,
                                ],
                                [
                                    'nothi_part_no' => $nothi_part_id,
                                    'nothi_office' => $nothi_office,
                                    'nothi_master_id' => $real_nothi_master_id,
                                ]);
                        }else{
                            // need to create a new row
                            $nothiMasterCurrentUsersEntity = $nothiMasterCurrentUsersTable->newEntity();
                            $nothiMasterCurrentUsersEntity->nothi_master_id = $real_nothi_master_id;
                            $nothiMasterCurrentUsersEntity->nothi_part_no = $nothi_part_id;
                            $nothiMasterCurrentUsersEntity->office_id = $this->request->data['to_office_id'];
                            $nothiMasterCurrentUsersEntity->office_unit_id = $this->request->data['to_office_unit_id'];
                            $nothiMasterCurrentUsersEntity->office_unit_organogram_id = $this->request->data['to_officer_designation_id'];
                            $nothiMasterCurrentUsersEntity->employee_id = $this->request->data['to_officer_id'];
                            $nothiMasterCurrentUsersEntity->issue_date = date("Y-m-d H:i:s");
                            $nothiMasterCurrentUsersEntity->forward_date = date("Y-m-d H:i:s");
                            $nothiMasterCurrentUsersEntity->is_new = 0;
                            $nothiMasterCurrentUsersEntity->nothi_office = $nothi_office;
                            $nothiMasterCurrentUsersEntity->is_archive =0;
                            $nothiMasterCurrentUsersEntity->is_finished =0;
                            $nothiMasterCurrentUsersEntity->priority =isset($this->request->data['priority']) ? $this->request->data['priority'] : 0;
                            $nothiMasterCurrentUsersEntity->is_summary =!empty($hasSummary) ? $hasSummary : 0;
                            $nothiMasterCurrentUsersEntity->view_status =0;

                            $nothiMasterCurrentUsersRecord = $nothiMasterCurrentUsersTable->save($nothiMasterCurrentUsersEntity);
                        }

                        //end

                    } catch (\Exception $ex) {
                        // rollback -- delete previous nothi transactions - signature and movements
                        $this->switchOffice($employee_office['office_id'], 'MainNothiOffice');
                        TableRegistry::remove('NothiMasterMovements');
                        TableRegistry::remove('NothiMasterPermissions');
                        TableRegistry::remove('NothiNoteSignatures');
                        $nothiMovementsTable = TableRegistry::get('NothiMasterMovements');
                        $nothiNoteSignaturesTable = TableRegistry::get('NothiNoteSignatures');
                        TableRegistry::remove('NothiMasterCurrentUsers');
                        $nothiMasterCurrentUsersTable = TableRegistry::get("NothiMasterCurrentUsers");
                        $nothiMovementsTable->deleteLastMovementForError($nothi_part_id,$employee_office['office_unit_organogram_id'],$this->request->data['to_officer_designation_id']);
                        $nothiNoteSignaturesTable->deleteLastSignatureForError($nothi_part_id,isset($lastNote['id'] )?$lastNote['id'] :0,$this->request->data['to_officer_designation_id']);
                        $nothiNoteSignaturesTable->unsetLastSignatureForError($nothi_part_id,isset($lastNote['id'] )?$lastNote['id'] :0,$employee_office['office_unit_organogram_id']);
                        $nothiMasterCurrentUsersTable->updateAll(
                            [
                                'office_id' => $employee_office['office_id'],
                                'office_unit_id' => $employee_office['office_unit_id'],
                                'office_unit_organogram_id' => $employee_office['office_unit_organogram_id'],
                                'employee_id' => $employee_office['officer_id'],
                                'view_status' => 1,
                            ],
                            [
                                'nothi_part_no' => $nothi_part_id,
                                'nothi_office' => $nothi_office,
                            ]);
                        // rollback -- delete previous nothi transactions - signature and movements
                        $response['msg'] = __('Technical error happen');
                        $response['reason'] = $this->makeEncryptedData($ex->getMessage());
                        goto rtn;
                    }
                }
                $request_success = 1;
            }
//            if ($multiple_offices_permission == false) {
//                $this->switchOffice($employee_office['office_id'], 'MainNothiOffice');
//            }

        if(!empty($request_success)){
            $this->response->body(json_encode(array('status' => 'success', 'msg' => 'নথি পরবর্তী কার্যক্রমের জন্য প্রেরণ করা হয়েছে','message' => 'নথি পরবর্তী কার্যক্রমের জন্য প্রেরণ করা হয়েছে')));
            $this->response->type('application/json');
            return $this->response;
        }
        rtn:
            if(isset($response['msg'])){
                $response['message'] = $response['msg'];
            }
            if(isset($response['message'])){
                $response['msg'] = $response['message'];
            }
            $this->response->body(json_encode($response));
            $this->response->type('application/json');
            return $this->response;

    }

    public function postApiForwardNothiMaster($nothi_part_id, $nothi_office)
    {
        $this->digitalSigantureUrlChecker();

        $user_designation = $this->request->data['user_designation'];
        $response = array(
            "status" => "error",
            "error_code" => 404,
            "message" => "Invalid Request"
        );

        $apikey = isset($this->request->data['api_key']) ? $this->request->data['api_key']
            : '';
        if (empty($apikey) || $this->checkToken($apikey) == FALSE) {
            goto rtn;
        }

        if (!defined("PROJAPOTI_API") || PROJAPOTI_API == false) {
            goto rtn;
        }

        if (empty($user_designation)) {
            goto rtn;
        }
        if (empty($nothi_part_id)) {
            goto rtn;
        }
        if ($apikey != API_KEY) {
            //verify api token
            $this->loadComponent('ApiRelated');
            $getApiTokenData = $this->ApiRelated->getMobileApiTokenData($apikey);
            if (!empty($getApiTokenData)) {
                if (!in_array($user_designation, $getApiTokenData['designations'])) {
                    $response['message'] = 'Unauthorized request';
                    goto rtn;
                }
            }
            //verify api token
        }
//        $EmployeeOfficesTable = TableRegistry::get('EmployeeOffices');
//        $employee_office = $EmployeeOfficesTable->find()->where(['office_unit_organogram_id' => $user_designation,
//            'status' => 1])->first();
        //need to check receiver DB connection
        if (!empty($this->request->data['to_office_id']) && !empty($this->request->data['to_officer_designation_id']) && !empty($this->request->data['to_office_unit_id'])) {
            $this->switchOffice($this->request->data['to_office_id'], 'MainNothiOffice');
            $checkDBConnection = $this->checkCurrentOfficeDBConnection();
            if (empty($checkDBConnection) || (!empty($checkDBConnection['status']) && $checkDBConnection['status'] == 'error')) {
                $response['msg'] = 'দুঃখিত! প্রেরকের ডাটাবেস (আইডিঃ ' . $this->request->data['to_office_id'] . ') সংযোগ সম্ভব হয়নি।';
                goto rtn;
            }
        } else {
            $response['msg'] = 'দুঃখিত! প্রেরকের তথ্য পাওয়া যায়নি';
            goto rtn;
        }

        $employee_office = $this->setCurrentDakSection($user_designation);
        $request_success = 0;
        $otherNothi = false;
        if ($nothi_office != 0 && $nothi_office != $employee_office['office_id']) {
            $otherNothi = true;
        }

        $this->switchOffice($nothi_office, 'MainNothiOffice');
        $checkDBConnection = $this->checkCurrentOfficeDBConnection();
        if (empty($checkDBConnection) || (!empty($checkDBConnection['status']) && $checkDBConnection['status'] == 'error')) {
            $response['msg'] = 'দুঃখিত! ডাটাবেস (আইডিঃ ' . $nothi_office . ') সংযোগ সম্ভব হয়নি।';
            goto rtn;
        }

        TableRegistry::remove('NothiParts');
        $nothiPartsTable = TableRegistry::get('NothiParts');
        $employee_record_table = TableRegistry::get('EmployeeRecords');

        $employee = $employee_record_table->getEmployeeInfoByRecordId($employee_office['employee_record_id']);

        $usersTable = TableRegistry::get('Users');
        $users = $usersTable->find()->where(['employee_record_id' => $employee_office['employee_record_id']])->first();
        $username = $users['username'];
        $options['token'] = sGenerateToken(['file' => $username], ['exp' => time() + 60 * 300]);
        $data = $this->getSignature($username, 1, 0, true, null, $options);

        if ($data == false) {
            $response['msg'] = 'দুঃখিত! আপনার স্বাক্ষর পাওয়া যায়নি। অনুগ্রহ করে সিস্টেমে স্বাক্ষর আপলোড করুন। ধন্যবাদ।';
            goto rtn;
        }
        $api_nothi_part_flag_table = TableRegistry::get('ApiNothiPartFlag');
        $nothi_part_flag_count = $api_nothi_part_flag_table->find()->where(['part_id'=>$nothi_part_id,'nothi_office_id' => $nothi_office,'office_unit_organogram_id'=>$user_designation])->first();
        if(!empty($nothi_part_flag_count)){
            if($nothi_part_flag_count['type']== 1){
                $response['msg'] = 'দুঃখিত! খসড়াপত্র সংরক্ষন না করে নথি প্রেরন করা যাবে না। অনুগ্রহপুর্বক খসড়াপত্রটি সংরক্ষন করুন।';
            } else {
                $response['msg'] = 'দুঃখিত! অনুচ্ছেদ সংরক্ষন না করে নথি প্রেরন করা যাবে না। অনুগ্রহপুর্বক অনুচ্ছেদটি সংরক্ষন করুন।';
            }
            goto rtn;
        }
        $real_nothi_master_id = 0;
        try {
            $conn = ConnectionManager::get('default');
            $conn->begin();
            TableRegistry::remove('NothiMasterCurrentUsers');
            $nothiMasterCurrentUserTable = TableRegistry::get('NothiMasterCurrentUsers');

            $nothiMastersCurrentUser = $nothiMasterCurrentUserTable->find()->where(['nothi_part_no' => $nothi_part_id,
                'office_unit_organogram_id' => $employee_office['office_unit_organogram_id'],
                'nothi_office' => $nothi_office])->first();

            if (empty($nothiMastersCurrentUser)) {
                $conn->rollback();
                $response['msg'] = 'দুঃখিত! নথি পরবর্তী কার্যক্রমের জন্য প্রেরণ করা সম্ভব হচ্ছে না';
                $response['reason'] = 'No data found';
                goto rtn;
            } else {
                $real_nothi_master_id = $nothiMastersCurrentUser['nothi_master_id'];
                $is_finished = (isset($nothiMastersCurrentUser['is_finished']) ? $nothiMastersCurrentUser['is_finished'] : 0);
            }


            if ($nothiMastersCurrentUser['nothi_office'] != 0 && $nothiMastersCurrentUser['nothi_office']
                != $employee_office['office_id']
            ) {
                $this->switchOffice($nothiMastersCurrentUser['nothi_office'],
                    'NothiOffice');
            }

            TableRegistry::remove('NothiMasterMovements');
            TableRegistry::remove('NothiNoteSignatures');
            TableRegistry::remove('NothiNotes');
            TableRegistry::remove('Potrojari');
            TableRegistry::remove('PotrojariVersions');
            TableRegistry::remove('NothiPotroAttachments');
            $nothiMovementsTable = TableRegistry::get('NothiMasterMovements');
            TableRegistry::remove('NothiMasterPermissions');
            TableRegistry::remove('NothiPotroAttachments');
            $nothiMasterPermissionTable = TableRegistry::get('NothiMasterPermissions');
            $nothiNoteSignaturesTable = TableRegistry::get('NothiNoteSignatures');
            $nothiNotesTable = TableRegistry::get('NothiNotes');
            $officeTable = TableRegistry::get('Offices');
            $potrojariTable = TableRegistry::get('Potrojari');
            $officeUnitTable = TableRegistry::get('OfficeUnits');
            $potrojariVersionTable = TableRegistry::get('PotrojariVersions');
            $nothiPotroAttachmentsTable = TableRegistry::get('NothiPotroAttachments');

            $nothiPartsInformation = $nothiPartsTable->get($nothi_part_id);
            $nothi_master_id = $nothiPartsInformation['nothi_masters_id'];
            $nothi_no = $nothiPartsInformation['nothi_no'];
            $subject = $nothiPartsInformation['subject'];
            $creator_unit_id = $nothiPartsInformation['office_units_id'];

            $hasAccess = $nothiMasterPermissionTable->hasAccessList($nothi_office,
                $this->request->data['to_office_id'],
                $this->request->data['to_office_unit_id'],
                $this->request->data['to_officer_designation_id'],
                $nothi_master_id, $nothi_part_id);

            if (empty($hasAccess)) {
                $conn->rollback();
                $response['msg'] = 'দুঃখিত! নথি পরবর্তী কার্যক্রমের জন্য প্রেরণ করা সম্ভব হচ্ছে না';
                $response['reason'] = 'No access';
            }
            /*
             * Check If there is notes or not
             */
            $hasOnucched = $nothiNotesTable->checkEmptyNote($nothi_master_id, $nothi_part_id);
            if ($hasOnucched == 0) {
                $conn->rollback();
                $response['msg'] = 'কোন অনুচ্ছেদ না দিয়ে প্রেরণ করা সম্ভব নয়। দয়া করে নোটে অনুচ্ছেদ দিন';
                goto rtn;
            }
            $nothiPartsInformation->modified = date("Y-m-d H:i:s");
            $nothiPartsTable->save($nothiPartsInformation);
            /*
                * Time to check digital signature
             * skip if soft token -1
                */
            if ($employee_office['default_sign'] > 0 && isset($this->request->data['soft_token']) && $this->request->data['soft_token'] != -1) {
                if ($employee_office['default_sign'] == 1 && empty($this->request->data['soft_token'])) {
                    $conn->rollback();
                    $response['msg'] = 'দুঃখিত! সফট টোকেন দেওয়া হয়নি';
                    goto rtn;
                }
                $allNotesToSign = $nothiNotesTable->find('list', ['keyField' => 'id', 'valueField' => 'note_description'])->where(['note_status' => 'DRAFT', 'nothi_part_no' => $nothi_part_id, 'office_organogram_id' => $employee_office['office_unit_organogram_id']])->toArray();
                $this->loadComponent('DigitalSignatureRelated');
                $res = $this->DigitalSignatureRelated->signOnucched($allNotesToSign, $this->request->data['soft_token'], $nothi_part_id, $employee_office['default_sign'], $employee_office, $nothi_office);
                if ($res['status'] == 'error') {
                    $conn->rollback();
                    $response['msg'] = 'দুঃখিত! ডিজিটাল সিগনেচার করা সম্ভব হয়নি। কারণঃ ' . $res['msg'];
                    goto rtn;
                } else {
                    $note_ids = $res['note_ids'];
                }

            }
            //check for draft potro

            $searchForDraft = $potrojariTable->getInfo([
                'nothi_part_no' => $nothi_part_id,
                'potro_status IN' => ['Draft', 'SummaryDraft']
            ])->toArray();

            if (!empty($searchForDraft)) {
                foreach ($searchForDraft as $key => $drafts) {
                    $draftVersion = $potrojariVersionTable->newEntity();
                    $draftVersion->potrojari_id = $drafts->id;
                    $draftVersion->nothi_master_id = $drafts->nothi_master_id;
                    $draftVersion->nothi_part_no = $drafts->nothi_part_no;
                    $draftVersion->updated_content = $drafts->potro_description;
                    $draftVersion->office_id = $employee_office['office_id'];
                    $draftVersion->officer_id = $employee_office['employee_record_id'];
                    $draftVersion->officer_name = $employee['name_bng'];
                    $draftVersion->officer_unit_id = $employee_office['office_unit_id'];
                    $draftVersion->office_organogram_id = $employee_office['office_unit_organogram_id'];
                    $draftVersion->designation_label = $employee_office['designation'];
                    if ($drafts['potro_status'] == 'SummaryDraft') {
                        $hasSummary = 1;
                    }

                    if(!$potrojariVersionTable->save($draftVersion)){
                        $conn->rollback();
                        $response['msg'] = __('Technical error happen');
                        $response['reason'] = 'Error Code:Post API Nothi FORWARD - 1';
                        goto rtn;
                    }
                }
            }
            //check for summary
            if (!empty($hasSummary)) {
                $hasSummary = $nothiPotroAttachmentsTable->find()->where(['is_summary_nothi' => 1, 'nothi_part_no' => $nothi_part_id])->count();
            }
            //movements
            $unitinfomation = $officeUnitTable->get($employee_office['office_unit_id']);
            $toOfficeName = $officeTable->get($this->request->data['to_office_id']);

            $first_move = $nothiMovementsTable->newEntity();
            $first_move->nothi_master_id = $nothi_master_id;
            $first_move->nothi_part_no = $nothi_part_id;
            $first_move->nothi_office = $nothi_office;
            $first_move->from_office_id = $employee_office['office_id'];
            $first_move->from_officer_id = $employee['id'];
            $first_move->from_office_unit_id = $employee_office['office_unit_id'];
            $first_move->from_officer_designation_id = $employee_office['office_unit_organogram_id'];
            $first_move->from_office_name = $employee_office['office_name'];
            $first_move->from_office_unit_name = $unitinfomation['unit_name_bng'];
            $first_move->from_officer_name = $employee['name_bng'];
            $first_move->from_officer_designation_label = $employee_office['designation'] . (!empty($employee_office['incharge_label']) ? (" (" . $employee_office['incharge_label'] . ')') : '');

            $first_move->to_office_id = $this->request->data['to_office_id'];
            $first_move->to_office_name = (!empty($toOfficeName) ? $toOfficeName['office_name_bng']
                : '');
            $first_move->to_office_unit_id = $this->request->data['to_office_unit_id'];
            $first_move->to_office_unit_name = $this->request->data['to_office_unit_name'];
            $first_move->to_officer_id = $this->request->data['to_officer_id'];
            $first_move->to_officer_name = $this->request->data['to_officer_name'];
            $first_move->to_officer_designation_id = $this->request->data['to_officer_designation_id'];
            $first_move->to_officer_designation_label = $this->request->data['to_officer_designation_label'] . (!empty($this->request->data['to_officer_incharge_label']) ? (" (" . $this->request->data['to_officer_incharge_label'] . ')') : '');

            $first_move->view_status = 0;
            $first_move->created_by = $employee['id'];
            $first_move->modified_by = $employee['id'];
            $first_move->created = date('Y-m-d H:i:s');
            $first_move->modified = date('Y-m-d H:i:s');

            if(!$nothiMovementsTable->save($first_move)){
                $conn->rollback();
                $response['msg'] = __('Technical error happen');
                $response['reason'] = 'Error Code:POST API Nothi FORWARD - 2';
                goto rtn;
            }
            //end
            //change current user -- previous_code
//            $nothiMastersCurrentUser->office_id = $this->request->data['to_office_id'];
//            $nothiMastersCurrentUser->office_unit_id = $this->request->data['to_office_unit_id'];
//            $nothiMastersCurrentUser->office_unit_organogram_id = $this->request->data['to_officer_designation_id'];
//            $nothiMastersCurrentUser->employee_id = $this->request->data['to_officer_id'];
//            $nothiMastersCurrentUser->forward_date = $nothiMastersCurrentUser->issue_date;
//            $nothiMastersCurrentUser->issue_date = date("Y-m-d H:i:s");
//            $nothiMastersCurrentUser->is_archive = 0;
//            $nothiMastersCurrentUser->is_finished = 0;
//            $nothiMastersCurrentUser->view_status = 0;
//            $nothiMastersCurrentUser->is_new = 0;
//
//            $nothiMasterCurrentUserTable->save($nothiMastersCurrentUser);
            //end -- previous_code
            $hasAccess->visited = 1;

            if(!$nothiMasterPermissionTable->save($hasAccess)){
                $conn->rollback();
                $response['msg'] = __('Technical error happen');
                $response['reason'] = 'Error Code:POST API Nothi FORWARD - 3';
                goto rtn;
            }
            //keep signature on note
            $lastNote = $nothiNotesTable->find()->select(['id'])->where(['nothi_part_no' => $nothi_part_id, 'note_no >=' => 0])->order(['id DESC'])->first();
            //sometimes error occurred. Receiver Signature already Exist as last signature.Throw Exception
            $lastSignature_2_check_receiver_sign= $nothiNoteSignaturesTable->getLastSignature(0,
                $nothi_part_id, $lastNote['id'])->order(['id desc'])->first();
            if(!empty($lastSignature_2_check_receiver_sign)){
                if($lastSignature_2_check_receiver_sign['office_organogram_id'] == $this->request->data['to_officer_designation_id']){
                    //something wrong . Delete this signature
                    $conn->rollback();
                    $nothiNoteSignaturesTable->deleteAll(['id' =>$lastSignature_2_check_receiver_sign['id']]);
                    $response['msg'] = __('Technical error happen');
                    $response['reason'] = 'Error Code: Nothi FORWARD - 10';
                    goto rtn;
                }
            }
            //sometimes error occurred. Receiver Signature already Exist as last signature.Throw Exception
            //save own signature
            $lastSignature = $nothiNoteSignaturesTable->getByEmployeeId($employee_office['office_unit_organogram_id'],
                $nothi_part_id)->first();
            $lastCrossSignature = ($lastSignature['nothi_note_id'] == $lastNote['id']) ? 0 : $lastSignature['cross_signature'];

            if (empty($lastNote)) {
                $lastNote['id'] = 0;
            }
            //get last signature of that note for is_signature = 1and cross_signature = 0
            $lastSignatureFor_is_Signature_1 = $nothiNoteSignaturesTable->getLastSignature($employee_office['office_unit_organogram_id'],
                $nothi_part_id, $lastNote['id'])->where(['is_signature' => 1, 'cross_signature' => 0])->order(['id desc'])->first();
            $f = true;
            if (!empty($lastSignatureFor_is_Signature_1)) {
                /**
                 * *Note nisponno put signature on that note of this user.
                 * Checking if user sending note after nisponno that note as it will give another signature in that note.
                 */
                if ($lastSignatureFor_is_Signature_1['nothi_note_id'] == $lastNote['id']
                    && $lastSignatureFor_is_Signature_1['office_organogram_id']
                    == $employee_office['office_unit_organogram_id']
                ) {
                    $f = false;
                }
            }
            if (!empty($lastSignature)) {
                $nothiNoteSignaturesTable->updateAll([
                    'is_signature' => 1,
                    'cross_signature' => $lastCrossSignature,
                    'office_id' => $employee_office['office_id'],
                    'office_unit_id' => $employee_office['office_unit_id'],
                    'employee_id' => $employee['id'],
                    'signature_date' => date("Y-m-d H:i:s"),
                    'employee_designation' => $employee_office['designation_label'] . (!empty($employee_office['incharge_label']) ? (" (" . $employee_office['incharge_label'] . ')') : '') . (!empty($employee_office['office_unit_name']) ? ("<br style='font-size: 8pt !important;'>" . $employee_office['office_unit_name']) : ''),
                ], ['id' => $lastSignature['id']]);
            } else {
                $lastSignature_singed = $nothiNoteSignaturesTable->getByEmployeeId($employee_office['office_unit_organogram_id'], $nothi_part_id, 1)->first();
                if (!empty($lastSignature_singed)) {
                    $nothiNoteSignaturesTable->updateAll([
                        'signature_date' => date("Y-m-d H:i:s")
                    ], ['id' => $lastSignature_singed['id']]);
                }
            }
            if ($f == true) {
                if (empty($lastSignature)) {
                    $nothiNoteSignatures = $nothiNoteSignaturesTable->newEntity();
                    $nothiNoteSignatures->nothi_master_id = $nothi_master_id;
                    $nothiNoteSignatures->nothi_part_no = $nothi_part_id;
                    $nothiNoteSignatures->office_id = $employee_office['office_id'];
                    $nothiNoteSignatures->employee_id = $employee['id'];
                    $nothiNoteSignatures->office_unit_id = $employee_office['office_unit_id'];
                    $nothiNoteSignatures->office_organogram_id = $employee_office['office_unit_organogram_id'];
                    $nothiNoteSignatures->employee_designation = $employee_office['designation'] . (!empty($employee_office['incharge_label']) ? (" (" . $employee_office['incharge_label'] . ')') : '') . (!empty($employee_office['office_unit_name']) ? ("<br style='font-size: 8pt !important;'>" . $employee_office['office_unit_name']) : '');

                    $nothiNoteSignatures->nothi_note_id = !empty($lastNote['id']) ? intval($lastNote['id'])
                        : 0;
                    $nothiNoteSignatures->note_decision = '';
                    $nothiNoteSignatures->cross_signature = 0;
                    $nothiNoteSignatures->is_signature = 1;
                    $nothiNoteSignatures->signature_date = date("Y-m-d H:i:s");
                    $nothiNoteSignatures->created = date("Y-m-d H:i:s");
                    $nothiNoteSignatures->created_by = $employee['id'];
                    $nothiNoteSignatures->modified_by = $employee['id'];

                    if(!$nothiNoteSignaturesTable->save($nothiNoteSignatures)){
                        $conn->rollback();
                        $response['msg'] = __('Technical error happen');
                        $response['reason'] = 'Error Code:POST API Nothi FORWARD - 3';
                        goto rtn;
                    }
                } else {

                    if ($lastSignature->nothi_note_id == $lastNote['id']) {
                        if ($lastSignature->office_organogram_id == $employee_office['office_unit_organogram_id']) {
                            $lastSignature->office_id = $employee_office['office_id'];
                            $lastSignature->employee_id = $employee['id'];
                            $lastSignature->office_unit_id = $employee_office['office_unit_id'];
                            $lastSignature->office_organogram_id = $employee_office['office_unit_organogram_id'];
                            $lastSignature->employee_designation = $employee_office['designation'] . (!empty($employee_office['incharge_label']) ? (" (" . $employee_office['incharge_label'] . ')') : '') . (!empty($employee_office['office_unit_name']) ? ("<br style='font-size: 8pt !important;'>" . $employee_office['office_unit_name']) : '');
                            $lastSignature->is_signature = 1;
                            $lastSignature->signature_date = date("Y-m-d H:i:s");
                            $lastSignature->created = date("Y-m-d H:i:s");
                            $lastSignature->created_by = $employee['id'];
                            $lastSignature->modified_by = $employee['id'];

                            if(!$nothiNoteSignaturesTable->save($lastSignature)){
                                $conn->rollback();
                                $response['msg'] = __('Technical error happen');
                                $response['reason'] = 'Error Code:POST API Nothi FORWARD - 4';
                                goto rtn;
                            }
                        } else {

                            $nothiNoteSignatures = $nothiNoteSignaturesTable->newEntity();
                            $nothiNoteSignatures->nothi_master_id = $nothi_master_id;
                            $nothiNoteSignatures->nothi_part_no = $nothi_part_id;
                            $nothiNoteSignatures->office_id = $employee_office['office_id'];
                            $nothiNoteSignatures->employee_id = $employee['id'];
                            $nothiNoteSignatures->office_unit_id = $employee_office['office_unit_id'];
                            $nothiNoteSignatures->office_organogram_id = $employee_office['office_unit_organogram_id'];
                            $nothiNoteSignatures->employee_designation = $employee_office['designation'] . (!empty($employee_office['incharge_label']) ? (" (" . $employee_office['incharge_label'] . ')') : '') . (!empty($employee_office['office_unit_name']) ? ("<br style='font-size: 8pt !important;'>" . $employee_office['office_unit_name']) : '');

                            $nothiNoteSignatures->nothi_note_id = !empty($lastNote['id'])
                                ? intval($lastNote['id']) : 0;
                            $nothiNoteSignatures->note_decision = '';
                            $nothiNoteSignatures->cross_signature = 0;
                            $nothiNoteSignatures->is_signature = 1;
                            $nothiNoteSignatures->signature_date = date("Y-m-d H:i:s");
                            $nothiNoteSignatures->created = date("Y-m-d H:i:s");
                            $nothiNoteSignatures->created_by = $employee['id'];
                            $nothiNoteSignatures->modified_by = $employee['id'];

                            if(!$nothiNoteSignaturesTable->save($nothiNoteSignatures)){
                                $conn->rollback();
                                $response['msg'] = __('Technical error happen');
                                $response['reason'] = 'Error Code:POST API Nothi FORWARD - 5';
                                goto rtn;
                            }
                        }
                    } else {
                        if ($lastSignature->office_organogram_id == $employee_office['office_unit_organogram_id']) {
                            $lastSignature->office_id = $employee_office['office_id'];
                            $lastSignature->employee_id = $employee['id'];
                            $lastSignature->office_unit_id = $employee_office['office_unit_id'];
                            $lastSignature->office_organogram_id = $employee_office['office_unit_organogram_id'];
                            $lastSignature->employee_designation = $employee_office['designation'] . (!empty($employee_office['incharge_label']) ? (" (" . $employee_office['incharge_label'] . ')') : '') . (!empty($employee_office['office_unit_name']) ? ("<br style='font-size: 8pt !important;'>" . $employee_office['office_unit_name']) : '');
                            $lastSignature->is_signature = 1;
                            $lastSignature->cross_signature = 1;
                            $lastSignature->signature_date = date("Y-m-d H:i:s");
                            $lastSignature->created = date("Y-m-d H:i:s");
                            $lastSignature->created_by = $employee['id'];
                            $lastSignature->modified_by = $employee['id'];

                            if(!$nothiNoteSignaturesTable->save($lastSignature)){
                                $conn->rollback();
                                $response['msg'] = __('Technical error happen');
                                $response['reason'] = 'Error Code:POST API Nothi FORWARD - 6';
                                goto rtn;
                            }
                        }
                        $nothiNoteSignatures = $nothiNoteSignaturesTable->newEntity();
                        $nothiNoteSignatures->nothi_master_id = $nothi_master_id;
                        $nothiNoteSignatures->nothi_part_no = $nothi_part_id;
                        $nothiNoteSignatures->office_id = $employee_office['office_id'];
                        $nothiNoteSignatures->employee_id = $employee['id'];
                        $nothiNoteSignatures->office_unit_id = $employee_office['office_unit_id'];
                        $nothiNoteSignatures->office_organogram_id = $employee_office['office_unit_organogram_id'];
                        $nothiNoteSignatures->employee_designation = $employee_office['designation'] . (!empty($employee_office['incharge_label']) ? (" (" . $employee_office['incharge_label'] . ')') : '') . (!empty($employee_office['office_unit_name']) ? ("<br style='font-size: 8pt !important;'>" . $employee_office['office_unit_name']) : '');

                        $nothiNoteSignatures->nothi_note_id = !empty($lastNote['id'])
                            ? intval($lastNote['id']) : 0;
                        $nothiNoteSignatures->note_decision = '';
                        $nothiNoteSignatures->is_signature = 1;
                        $nothiNoteSignatures->signature_date = date("Y-m-d H:i:s");
                        $nothiNoteSignatures->created = date("Y-m-d H:i:s");
                        $nothiNoteSignatures->created_by = $employee['id'];
                        $nothiNoteSignatures->modified_by = $employee['id'];

                        if(!$nothiNoteSignaturesTable->save($nothiNoteSignatures)){
                            $conn->rollback();
                            $response['msg'] = __('Technical error happen');
                            $response['reason'] = 'Error Code:POST API Nothi FORWARD - 7';
                            goto rtn;
                        }
                    }
                }
            }
            //end

            $nothiNoteSignatures = $nothiNoteSignaturesTable->newEntity();
            $nothiNoteSignatures->nothi_master_id = $nothi_master_id;
            $nothiNoteSignatures->nothi_part_no = $nothi_part_id;
            $nothiNoteSignatures->office_id = $this->request->data['to_office_id'];
            $nothiNoteSignatures->employee_id = $this->request->data['to_officer_id'];
            $nothiNoteSignatures->office_unit_id = $this->request->data['to_office_unit_id'];
            $nothiNoteSignatures->office_organogram_id = $this->request->data['to_officer_designation_id'];
            $nothiNoteSignatures->employee_designation = $this->request->data['to_officer_designation_label'] . (!empty($this->request->data['to_officer_incharge_label']) ? (" (" . $this->request->data['to_officer_incharge_label'] . ')') : '') . (!empty($this->request->data['to_office_unit_name']) ? ("<br style='font-size: 8pt !important;'>" . $this->request->data['to_office_unit_name']) : '');

            $nothiNoteSignatures->nothi_note_id = !empty($lastNote['id']) ? intval($lastNote['id']) : 0;
            $nothiNoteSignatures->note_decision = '';
            $nothiNoteSignatures->cross_signature = 0;
            $nothiNoteSignatures->is_signature = 0;
            $nothiNoteSignatures->signature_date = date("Y-m-d H:i:s");
            $nothiNoteSignatures->created = date("Y-m-d H:i:s");
            $nothiNoteSignatures->created_by = $employee['id'];
            $nothiNoteSignatures->modified_by = $employee['id'];

            if(!$nothiNoteSignaturesTable->save($nothiNoteSignatures)){
                $conn->rollback();
                $response['msg'] = __('Technical error happen');
                $response['reason'] = 'Error Code:POST API Nothi FORWARD - 7';
                goto rtn;
            }
            //end
            //change nothi notes status
            $nothiNotesTable->updateAll(['note_status' => 'INBOX'], ['nothi_part_no' => $nothi_part_id]);

            //change nothi movements view status
            $nothiMovementsTable->updateAll(['view_status' => 1],
                ['nothi_part_no' => $nothi_part_id, 'to_officer_designation_id' => $employee_office['office_unit_organogram_id']]);





            //checking if new note is opening or not
            if ($is_finished == 1) {
                TableRegistry::get('NoteInitialize')->saveData(['nothi_masters_id' => $nothi_master_id, 'nothi_part_no' => $nothi_part_id, 'office_id' => $employee_office['office_id'], 'office_units_id' => $employee_office['office_unit_id'], 'office_units_organogram_id' => $employee_office['office_unit_organogram_id'], 'user_id' => $employee['id']]);
            }
            //checking if new note is opening or not

            $current_office_section = $employee_office;
            $current_office_section['office_id'] = $employee_office['office_id'];
            $current_office_section['officer_id'] = $employee['id'];

            $sentUser['office_id'] = $this->request->data['to_office_id'];
            $sentUser['officer_id'] = $this->request->data['to_officer_id'];

            // only for digital signature -- as signature done in final step, so signature should be done after that
            if (!empty($note_ids)) {
                TableRegistry::remove('NothiNoteSignatures');
                $nothiNoteSignaturesTable = TableRegistry::get('NothiNoteSignatures');
                $nothiNoteSignaturesTable->updateAll(['digital_sign' => 1], ['digital_sign' => 0, 'nothi_note_id IN' => $note_ids, 'office_organogram_id' => $employee_office['office_unit_organogram_id'], 'cross_signature' => 0, 'nothi_part_no' => $nothi_part_id]);
            }
            // only for digital signature

            if (!$this->NotificationSet('forward', array(1, "Nothi"), 2,
                $current_office_section, $current_office_section,
                $nothiPartsInformation['subject'])
            ) {

            }
            if (!$this->NotificationSet('inbox', array(1, "Nothi"), 2,
                $current_office_section, $sentUser,
                $nothiPartsInformation['subject'])
            ) {

            }
            // Give Receiver Notification
            $title = 'আপনার ডেস্কে নতুন নথি এসেছে';
            $mail_sender = [
                'sender' => h($employee['name_bng'] . ', ' . $employee_office['designation'] . ', ' . $unitinfomation['unit_name_bng']),
                'subject' => h($nothiPartsInformation['subject']),
                'time' => Time::parse(date('Y-m-d H:i:s'))->i18nFormat(null, null, 'bn-BD'),
            ];
            $mail_sender_notify = 'প্রেরকঃ ' . $mail_sender['sender'] . "\n বিষয়ঃ " . h($mail_sender['subject']) . "\n সময়ঃ " . $mail_sender['time'];
            $this->notifyAppUser('', $sentUser['officer_id'], ['title' => $title, 'body' => $mail_sender], ['title' => $title, 'body' => $mail_sender_notify]);
            // Give Receiver Notification

            $conn->commit();

        } catch (\Exception $ex) {
            $conn->rollback();

            $response['msg'] = 'দুঃখিত! নথি পরবর্তী কার্যক্রমের জন্য প্রেরণ করা সম্ভব হচ্ছে না.';
            $response['reason'] = $this->makeEncryptedData($ex->getMessage());
            goto rtn;

        }

        // If nothi from other office we need to keep track of that nothi in current employee office DB - Start
        if ($otherNothi == true) {
            $this->saveOtherOfficeNothiMasterTransaction($nothi_master_id, $nothi_part_id, $nothi_office, $employee_office, $employee, $toOfficeName, $this->request->data, $nothi_no, $subject, $creator_unit_id);
        }
        // If nothi from other office we need to keep track of that nothi in current employee office DB - End

            TableRegistry::remove('NothiMasterPermissions');
            $nothiPriviligeTable = TableRegistry::get('NothiMasterPermissions');
            $allPermittedOfficesList = $nothiPriviligeTable->permittedOfficesId($nothi_part_id, $nothi_office);
            if (!in_array($nothi_office, $allPermittedOfficesList)) {
                $allPermittedOfficesList[$nothi_office] = $nothi_office;
            }
            $multiple_offices_permission = false;
            if (!empty($allPermittedOfficesList)) {
                if (count($allPermittedOfficesList) == 1 && current($allPermittedOfficesList) == $employee_office['office_id']) {
                    $multiple_offices_permission = true;
                }
                foreach ($allPermittedOfficesList as $key => $value) {
                    try {
                        if ($multiple_offices_permission == false) {
                            $is_connected = $this->switchOfficeWithStatus($key, 'MainNothiOffice', -1, true);
                            if (!$is_connected || (!empty($is_connected['status']) && $is_connected['status'] == 'error')) {
                                continue;
                            }
                            $checkDBConnection = $this->checkCurrentOfficeDBConnection();
                            if (empty($checkDBConnection) || (!empty($checkDBConnection['status']) && $checkDBConnection['status'] == 'error')) {
                                continue;
                            }
                        }
                        TableRegistry::remove('NothiMasterCurrentUsers');
                        $nothiMasterCurrentUsersTable = TableRegistry::get("NothiMasterCurrentUsers");
                        //first check if current note has entry
                        $hasDatainCurrentUser = $nothiMasterCurrentUsersTable->find()->where(
                            [
                                'nothi_part_no' => $nothi_part_id,
                                'nothi_office' => $nothi_office,
                                'nothi_master_id'=>$real_nothi_master_id
                            ]
                        )->count();
                        //change current user
                        if(!empty($hasDatainCurrentUser)){
                            $nothiMasterCurrentUsersTable->updateAll(
                                [
                                    'is_archive' => 0,
                                    'is_finished' => 0,
                                    'is_summary' => !empty($hasSummary) ? $hasSummary : 0,
                                    'office_id' => $this->request->data['to_office_id'],
                                    'office_unit_id' => $this->request->data['to_office_unit_id'],
                                    'office_unit_organogram_id' => $this->request->data['to_officer_designation_id'],
                                    'employee_id' => $this->request->data['to_officer_id'],
                                    'view_status' => 0,
                                    'issue_date' => date("Y-m-d H:i:s"),
                                    'forward_date' => date("Y-m-d H:i:s"),
                                    'is_new' => 0,
                                    'priority' => isset($this->request->data['priority']) ? $this->request->data['priority'] : 0,
                                ],
                                [
                                    'nothi_part_no' => $nothi_part_id,
                                    'nothi_office' => $nothi_office,
                                    'nothi_master_id' => $real_nothi_master_id,
                                ]);
                        }else{
                            // need to create a new row
                            $nothiMasterCurrentUsersEntity = $nothiMasterCurrentUsersTable->newEntity();
                            $nothiMasterCurrentUsersEntity->nothi_master_id = $real_nothi_master_id;
                            $nothiMasterCurrentUsersEntity->nothi_part_no = $nothi_part_id;
                            $nothiMasterCurrentUsersEntity->office_id = $this->request->data['to_office_id'];
                            $nothiMasterCurrentUsersEntity->office_unit_id = $this->request->data['to_office_unit_id'];
                            $nothiMasterCurrentUsersEntity->office_unit_organogram_id = $this->request->data['to_officer_designation_id'];
                            $nothiMasterCurrentUsersEntity->employee_id = $this->request->data['to_officer_id'];
                            $nothiMasterCurrentUsersEntity->issue_date = date("Y-m-d H:i:s");
                            $nothiMasterCurrentUsersEntity->forward_date = date("Y-m-d H:i:s");
                            $nothiMasterCurrentUsersEntity->is_new = 0;
                            $nothiMasterCurrentUsersEntity->nothi_office = $nothi_office;
                            $nothiMasterCurrentUsersEntity->is_archive =0;
                            $nothiMasterCurrentUsersEntity->is_finished =0;
                            $nothiMasterCurrentUsersEntity->priority =isset($this->request->data['priority']) ? $this->request->data['priority'] : 0;
                            $nothiMasterCurrentUsersEntity->is_summary =!empty($hasSummary) ? $hasSummary : 0;
                            $nothiMasterCurrentUsersEntity->view_status =0;

                            $nothiMasterCurrentUsersRecord = $nothiMasterCurrentUsersTable->save($nothiMasterCurrentUsersEntity);
                        }

                        //end
                        $request_success = 1;

                    } catch (\Exception $ex) {
                        // rollback -- delete previous nothi transactions - signature and movements
                        $this->switchOffice($employee_office['office_id'], 'MainNothiOffice');
                        TableRegistry::remove('NothiMasterMovements');
                        TableRegistry::remove('NothiMasterPermissions');
                        TableRegistry::remove('NothiNoteSignatures');
                        $nothiMovementsTable = TableRegistry::get('NothiMasterMovements');
                        $nothiNoteSignaturesTable = TableRegistry::get('NothiNoteSignatures');
                        TableRegistry::remove('NothiMasterCurrentUsers');
                        $nothiMasterCurrentUsersTable = TableRegistry::get("NothiMasterCurrentUsers");
                        $nothiMovementsTable->deleteLastMovementForError($nothi_part_id,$employee_office['office_unit_organogram_id'],$this->request->data['to_officer_designation_id']);
                        $nothiNoteSignaturesTable->deleteLastSignatureForError($nothi_part_id,isset($lastNote['id'] )?$lastNote['id'] :0,$this->request->data['to_officer_designation_id']);
                        $nothiNoteSignaturesTable->unsetLastSignatureForError($nothi_part_id,isset($lastNote['id'] )?$lastNote['id'] :0,$employee_office['office_unit_organogram_id']);
                        $nothiMasterCurrentUsersTable->updateAll(
                            [
                                'office_id' => $employee_office['office_id'],
                                'office_unit_id' => $employee_office['office_unit_id'],
                                'office_unit_organogram_id' => $employee_office['office_unit_organogram_id'],
                                'employee_id' => $employee_office['officer_id'],
                                'view_status' => 1,
                            ],
                            [
                                'nothi_part_no' => $nothi_part_id,
                                'nothi_office' => $nothi_office,
                            ]);
                        // rollback -- delete previous nothi transactions - signature and movements
                        $response['msg'] = __('Technical error happen');
                        $response['reason'] = $this->makeEncryptedData($ex->getMessage());
                        goto rtn;
                    }
                }
            }
//            if ($multiple_offices_permission == false) {
//                $this->switchOffice($employee_office['office_id'], 'MainNothiOffice');
//            }

        if(!empty($request_success)){
            $this->response->body(json_encode(array('status' => 'success', 'msg' => 'নথি পরবর্তী কার্যক্রমের জন্য প্রেরণ করা হয়েছে','message' => 'নথি পরবর্তী কার্যক্রমের জন্য প্রেরণ করা হয়েছে')));
            $this->response->type('application/json');
            return $this->response;
        }
        rtn:
            if(isset($response['msg'])){
                $response['message'] = $response['msg'];
            }
            if(isset($response['message'])){
                $response['msg'] = $response['message'];
            }
            $this->response->body(json_encode($response));
            $this->response->type('application/json');
            return $this->response;

    }

    public function backwardNothi()
    {
        set_time_limit(0);
        $employee_office = $this->getCurrentDakSection();
        $request_success = 0;
        $response =array('status' => 'error', 'msg' => 'দুঃখিত! নথি ফেরত আনা সম্ভব নয়');

        if (!empty($this->request->data['nothi_part_id'])) {
            $nothi_office = intval($this->request->data['nothi_office']);
            $otherNothi = false;
            if ($nothi_office != 0 && $nothi_office != $employee_office['office_id']) {
                $otherNothi = true;
                $this->switchOffice($nothi_office, 'MainNothiOffice');
            } else {
                $nothi_office = $employee_office['office_id'];
            }

            if ($otherNothi) {
                $response['reason'] = 'OtherNothi';
                goto rtn;
            }

            $id = $this->request->data['nothi_part_id'];

            try {
                $conn = ConnectionManager::get('default');
                $conn->begin();
                TableRegistry::remove('NothiParts');
                TableRegistry::remove('NothiMasterMovements');
                TableRegistry::remove('NothiNoteSignatures');
                TableRegistry::remove('NothiNotes');
                TableRegistry::remove('NothiParts');
                TableRegistry::remove('NothiMasterCurrentUsers');

                $nothiMovementsTable = TableRegistry::get('NothiMasterMovements');
                $nothiNoteSignaturesTable = TableRegistry::get('NothiNoteSignatures');
                $nothiNotesTable = TableRegistry::get('NothiNotes');
                $nothiMasterCurrentUserTable = TableRegistry::get('NothiMasterCurrentUsers');

                $getLastMove = $nothiMovementsTable->getLastMove($id);
                $nothiMastersCurrentUser = $nothiMasterCurrentUserTable->find()->where(['nothi_office' => $nothi_office, 'nothi_part_no' => $id, 'office_unit_organogram_id' => $getLastMove['to_officer_designation_id']])->first();

                $real_nothi_master_id = 0;
                $issue_date = date('Y-m-d H:i:s');
                if (!empty($nothiMastersCurrentUser['nothi_master_id'])) {
                    $real_nothi_master_id = $nothiMastersCurrentUser['nothi_master_id'];
                }
                if (!empty($nothiMastersCurrentUser['issue_date'])) {
                    $issue_date = $nothiMastersCurrentUser['issue_date'];
                }

                $timeDif = new Time($getLastMove['modified']);

                if (!empty($nothiMastersCurrentUser) &&
                    $getLastMove['from_officer_designation_id'] == $employee_office['office_unit_organogram_id'] &&
                    $getLastMove['view_status'] == 0 &&
                    $timeDif->diffInDays() == 0
                ) {
                    //add new move
                    $first_move = $nothiMovementsTable->newEntity();
                    $first_move->nothi_master_id = $getLastMove['nothi_master_id'];
                    $first_move->nothi_part_no = $getLastMove['nothi_part_no'];
                    $first_move->nothi_office = $nothi_office;
                    $first_move->from_office_id = $getLastMove['to_office_id'];
                    $first_move->from_officer_id = $getLastMove['to_officer_id'];
                    $first_move->from_office_unit_id = $getLastMove['to_office_unit_id'];
                    $first_move->from_officer_designation_id = $getLastMove['to_officer_designation_id'];
                    $first_move->from_office_name = $getLastMove['to_office_name'];
                    $first_move->from_office_unit_name = $getLastMove['to_office_unit_name'];
                    $first_move->from_officer_name = $getLastMove['to_officer_name'];
                    $first_move->from_officer_designation_label = $getLastMove['to_officer_designation_label'];
                    $first_move->to_office_id = $getLastMove['from_office_id'];
                    $first_move->to_office_name = $getLastMove['from_office_name'];
                    $first_move->to_office_unit_id = $getLastMove['from_office_unit_id'];
                    $first_move->to_office_unit_name = $getLastMove['from_office_unit_name'];
                    $first_move->to_officer_id = $getLastMove['from_officer_id'];
                    $first_move->to_officer_name = $getLastMove['from_officer_name'];
                    $first_move->to_officer_designation_id = $getLastMove['from_officer_designation_id'];
                    $first_move->to_officer_designation_label = $getLastMove['from_officer_designation_label'];
                    $first_move->view_status = 1;
                    $first_move->movement_type = 2;
                    $first_move->created_by = $this->Auth->user('id');
                    $first_move->modified_by = $this->Auth->user('id');
                    $first_move->created = date('Y-m-d H:i:s');
                    $first_move->modified = date('Y-m-d H:i:s');
                    if(!$nothiMovementsTable->save($first_move)){
                        $conn->rollback();
                        $response['reason'] ='Error Code: Back Nothi - 1';
                        goto rtn;
                    }
                    //delete signatures
//                    $lastSignatures = $nothiNoteSignaturesTable->getLastSignature($employee_office['office_unit_organogram_id'], $id)->limit(4)->order(['id desc'])->toArray();
                    $lastSignatures = $nothiNoteSignaturesTable->getLastSignature(0, $id)->limit(3)->order(['id desc'])->toArray();

                    $lastNotes = [];
                    if (!empty($lastSignatures)) {
                        $hasCross = false;
                        foreach ($lastSignatures as $key => $value) {
                            $lastNotes[$value['nothi_note_id']] = $value['office_organogram_id'];
                            // signature which has cross in it.User puts onucched after receive that note.
                            if ($key == 2 && $employee_office['office_unit_organogram_id'] == $value['office_organogram_id'] && $value['cross_signature'] == 1 && $value['can_delete'] == 0) {
                                $nothiNoteSignaturesTable->updateAll(['cross_signature' => 0, 'is_signature' => 0], ['id' => $value['id']]);
                                $nothiNoteSignaturesTable->deleteAll(['id' => $lastSignatures[1]['id']]);
                            }
                            // signature of sender
                            if ($key == 1 && $value['can_delete'] == 0) {
                                $nothiNoteSignaturesTable->updateAll(['cross_signature' => 0, 'is_signature' => 0], ['id' => $value['id']]);
                            }
                            // signature of receiver which will not exist
                            if ($key == 0) {
                                $nothiNoteSignaturesTable->deleteAll(['id' => $value['id']]);
                            }
                        }
                    }
                    //update onucched status
                    $minNote = min(array_keys($lastNotes));
                    $maxNote = max(array_keys($lastNotes));
                    if (count($lastSignatures) < 3) {
                        $nothiNoteSignaturesTable->deleteAll(['nothi_part_no' => $id, 'can_delete' => 0]);
                        $nothiNotesTable->updateAll(['note_status' => 'DRAFT'], ['nothi_part_no' => $id, 'office_id' => $nothi_office, 'note_no <>' => -1, 'office_organogram_id' => $employee_office['office_unit_organogram_id']]);
                    } else if ($minNote != $maxNote) {
                        $nothiNotesTable->updateAll(['note_status' => 'DRAFT'], ['nothi_part_no' => $id, 'id >' => $minNote, 'office_id' => $nothi_office, 'note_no <>' => -1, 'office_organogram_id' => $employee_office['office_unit_organogram_id']]);
                    }

                    $conn->commit();


                }
            } catch (\Exception $ex) {
                $conn->rollback();
                $response['reason'] =$this->makeEncryptedData($ex->getMessage());
                goto rtn;
            }
            TableRegistry::remove('NothiMasterPermissions');
            $nothiPriviligeTable = TableRegistry::get('NothiMasterPermissions');
            $allPermittedOfficesList = $nothiPriviligeTable->permittedOfficesId($id, $nothi_office);
            $multiple_offices_permission = false;
            if (!empty($allPermittedOfficesList)) {
                if (count($allPermittedOfficesList) == 1 && current($allPermittedOfficesList) == $employee_office['office_id']) {
                    $multiple_offices_permission = true;
                }
                foreach ($allPermittedOfficesList as $p_key => $p_value) {
                    try {
                        if ($multiple_offices_permission == false) {
                            $is_connected = $this->switchOfficeWithStatus($p_key, 'MainNothiOffice', -1, true);
                            if (!$is_connected || (!empty($is_connected['status']) && $is_connected['status'] == 'error')) {
                                continue;
                            }
                            $checkDBConnection = $this->checkCurrentOfficeDBConnection();
                            if (empty($checkDBConnection) || (!empty($checkDBConnection['status']) && $checkDBConnection['status'] == 'error')) {
                                continue;
                            }
                        }
                        TableRegistry::remove('NothiMasterCurrentUsers');
                        $nothiMasterCurrentUsersTable = TableRegistry::get("NothiMasterCurrentUsers");
                        $nothiMasterCurrentUsersTable->deleteAll(['nothi_part_no' => $id,
                                                                  'nothi_office' => $nothi_office]);
                        //update current user
                        $nothiMastersCurrentUserEntity = $nothiMasterCurrentUsersTable->newEntity();
                        $nothiMastersCurrentUserEntity->nothi_master_id = $real_nothi_master_id;
                        $nothiMastersCurrentUserEntity->nothi_part_no = $id;
                        $nothiMastersCurrentUserEntity->nothi_office = $nothi_office;
                        $nothiMastersCurrentUserEntity->is_archive = 0;
                        $nothiMastersCurrentUserEntity->is_finished = 0;
                        $nothiMastersCurrentUserEntity->office_id = $getLastMove['from_office_id'];
                        $nothiMastersCurrentUserEntity->office_unit_id = $getLastMove['from_office_unit_id'];
                        $nothiMastersCurrentUserEntity->office_unit_organogram_id = $getLastMove['from_officer_designation_id'];
                        $nothiMastersCurrentUserEntity->employee_id = $getLastMove['from_officer_id'];
                        $nothiMastersCurrentUserEntity->view_status = 0;
                        $nothiMastersCurrentUserEntity->issue_date = date("Y-m-d H:i:s");
                        $nothiMastersCurrentUserEntity->forward_date = $issue_date;
                        $nothiMastersCurrentUserEntity->is_new = 0;
                        if(!$nothiMasterCurrentUsersTable->save($nothiMastersCurrentUserEntity)){
                            $response['reason'] ='Error Code: Back Nothi - 2';
                            goto rtn;
                        }
                        //end

                    } catch (\Exception $ex) {
                        $response['reason'] =$this->makeEncryptedData($ex->getMessage());
                        goto rtn;
                    }
                }
            }
            $request_success = 1;
        }
        if(!empty($request_success)){
            $this->response->body(json_encode(array('status' => 'success', 'msg' => 'নথি ফেরত আনা হয়েছে')));
            $this->response->type('application/json');
            return $this->response;
        }
        rtn:
        $this->response->body(json_encode($response));
        $this->response->type('application/json');
        return $this->response;
    }
}