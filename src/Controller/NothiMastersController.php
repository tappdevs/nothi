<?php

namespace App\Controller;

use App\Controller\ProjapotiController;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\TableRegistry;
use Cake\I18n\Number;
use Cake\Datasource\ConnectionManager;
use Cake\I18n\Time;
use Cake\Cache\Cache;
use Cake\Routing\Router;

class NothiMastersController extends ProjapotiController
{
    /*
     * Nothi Masters
     */
    public $load_part_at_a_time = 10;

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['APInothiMastersList', 'APInothiDetails',
            'ApiNothiParts', 'ApiShowForwaredUsers', 'ApiNothiPotroLists', 'ApiNothiPotros',
            'onuccedList', 'apiGetNothiMasterParts', 'apiNothiVuktoKoron', 'apiPotroPage',
            'apiNextPotroPage', 'showPopUp', 'postAPInothiMastersList', 'postApiGetNothiMasterParts',
            'postApiNothiPotroLists', 'postApiNothiPotros', 'postApiNothiParts',
            'postApiPotroPage', 'other_user',
            'postApiShowForwaredUsers', 'postApiGetPermittedNothiMasterList', 'postApiNothiPartPermissionEditList',
            'getOtherOfficePermissionList', 'nothiMasterPermissionUpdate', 'apiGetSummaryPartList', 'showSonglap', 'postApiNothiInfo', 'UpdateOtherOfficePermissions']);
    }

    public function nothiVuktoKoron()
    {
        $nothiPotrosTable = TableRegistry::get("NothiPotros");
        $dakUsersTable = TableRegistry::get("DakUsers");
        $dakMovementsTable = TableRegistry::get("DakMovements");
        $dakAttachmentsTable = TableRegistry::get("DakAttachments");
        $nothiMastersTable = TableRegistry::get("NothiMasters");
        $nothiDakMovesTable = TableRegistry::get("NothiMastersDakMap");
        $nothiDakPotroMapsTable = TableRegistry::get("NothiDakPotroMaps");
        $nothiPotroAttachmentsTable = TableRegistry::get("NothiPotroAttachments");
        $nothiPartsTable = TableRegistry::get("NothiParts");

        $employee_offices = $this->getCurrentDakSection();

        if ($this->request->is('ajax', 'post')) {
            if (!empty($this->request->data)) {
                $requestedData = $this->request->data;

                if (empty($requestedData['nothi_part_no'])) {
                    echo json_encode(['status' => 'error', 'msg' => 'দুঃখিত! নথি বাছাই করা হয়নি']);
                    die;
                }

                $nothiPartId = $requestedData['nothi_part_no'];
                $successnothi = 0;

                $nothiPartInformation = array();
                if (!empty($nothiPartId)) {
                    $nothiPartInformation = $nothiPartsTable->get($nothiPartId);
                    $requestedData['nothi_master_id'] = $nothiPartInformation['nothi_masters_id'];
                }

                $dakId = explode(",", $requestedData['selected_id']);
                $daktypeArr = explode(",", $requestedData['dak_type']);

                if (empty($dakId)) {
                    echo json_encode(['status' => 'error', 'msg' => 'দুঃখিত! ডাক পুনরায় বাছাই করুন']);
                    die;
                }

                $nothi_potro_page = $nothiPotroAttachmentsTable->find()->select(['nothi_potro_page'])->where(['nothi_master_id' => $requestedData['nothi_master_id']])->order(['nothi_potro_page DESC'])->first();

                if (!empty($dakId)) {
                    $requestedData['nothijato'] = !empty($requestedData['nothijato'])
                        ? $requestedData['nothijato'] : 0;
                    $errorCount = 0;
                    $totalDak = count($dakId);
                    $potroPage = !empty($nothi_potro_page->nothi_potro_page)
                        ? ($nothi_potro_page->nothi_potro_page) : 0;

                    foreach ($dakId as $key => $value) {

                        if (empty($value)) continue;

                        if (isset($daktypeArr[$key])) {
                            $dak_type = $daktypeArr[$key];
                        } else {
                            $dak_type = "Daptorik";
                        }

                        if ($dak_type == "Daptorik") {
                            $dakDaptoriksTable = TableRegistry::get("DakDaptoriks");
                        } else {
                            $dakDaptoriksTable = TableRegistry::get("DakNagoriks");
                        }

                        $dakRecord = $dakDaptoriksTable->get($value);
                        if (empty($dakRecord)) continue;


                        $dakLastMoves = $dakMovementsTable->getDak_for_nothiVukto($value,
                            $dak_type);

                        if ($dakLastMoves['to_officer_designation_id'] != $employee_offices['office_unit_organogram_id']) {
                            echo json_encode(['status' => 'error', 'msg' => 'দুঃখিত! ' . $dakRecord['subject'] . ' বিষয়ের ডাকটির অনুরোধ অগ্রহণযোগ্য']);
                            die;
                        }

                        $dakLastMoveId = $dakLastMoves['id'];
                        $nothiDakMovesRecord = $nothiDakMovesTable->newEntity();

                        $attachments = $dakAttachmentsTable->find()->where(['dak_id' => $value,
                            'dak_type' => $dak_type])->order(['id ASC'])->toArray();

                        $nothiDakPotroMapsRecord = $nothiDakPotroMapsTable->newEntity();
                        $nothiPotroRecord = $nothiPotrosTable->newEntity();

                        if ($requestedData['nothijato'] != 1) {
                            if (count($attachments) == 0 || empty($attachments)) {
                                $errorCount++;
                                continue;
                            }
                        }


                        if (($dakLastMoves['dak_actions'] == 'নথিতে উপস্থাপন করুন') && ($requestedData['nothijato'] == 1)
                        ) {
                            echo json_encode(['status' => 'error', 'msg' => 'দুঃখিত! নথিজাত করা সম্ভব হচ্ছে না। নথিতে উপস্থাপন করুন']);
                            die;
                        }

                        if (empty($requestedData['nothi_master_id'])
                        ) {
                            echo json_encode(['status' => 'error', 'msg' => 'দুঃখিত! নথি বাছাই করা হয়নি']);
                            die;
                        }
                        // for BTRC template - need to put sarok number on body
                        $btrc_sarok_number = '';
                        if(!empty($dakRecord['application_origin']) && strtolower($dakRecord['application_origin']) == 'btrc'){
                            //BTRC template consist of Sarok number, need to check duplicate sarok
                            $offices_to_check_unique_sarok_in_nothi_potros = $this->getOffices2CheckUniqueSarokFromNothiPotros();
                            if(in_array($employee_offices['office_id'],$offices_to_check_unique_sarok_in_nothi_potros)){
                                $check_sarok_in_nothi_potros = 1;
                            }else{
                                $check_sarok_in_nothi_potros = 0;
                            }
                            $btrc_sarok_number = TableRegistry::get('Potrojari')->generatePotrojariSarokNumber($requestedData['nothi_master_id'],$employee_offices['office_unit_id'],$employee_offices['office_id']);
                            // sometimes duplicate created need to check duplicacy
                            $btrc_sarok_number = TableRegistry::get('Potrojari')->getUniqueSarok(entobn($btrc_sarok_number),0,1);
                            $dakRecord['description'] = str_replace('{{sharok_no}}', entobn($btrc_sarok_number),$dakRecord['description']);
                            $dakRecord['sender_sarok_no'] = $btrc_sarok_number;
                            if(!empty($dakRecord['application_meta_data'])){
                                $meta_data_2change =  jsonA($dakRecord['application_meta_data']);
                                $dakRecord['application_meta_data'] = json_encode($meta_data_2change + ['memorandum_no' => $btrc_sarok_number]);
                            }
                        }

                        $nothiPotroRecord->nothi_master_id = $requestedData['nothi_master_id'];
                        $nothiPotroRecord->nothi_part_no = $requestedData['nothi_part_no'];
                        $nothiPotroRecord->dak_id = $value;
                        $nothiPotroRecord->dak_type = $dak_type;
                        $nothiPotroRecord->nothijato = ($dakLastMoves['dak_actions'] == 'নথিতে উপস্থাপন করুন') ? 0 : ($requestedData['nothijato'] == 1 ? $requestedData['nothijato'] : 0);
                        $nothiPotroRecord->potro_media = (!isset($dakRecord['dak_sending_media'])
                            || empty($dakRecord['dak_sending_media'])) ? 1 : $dakRecord['dak_sending_media'];
                        $nothiPotroRecord->potro_pages = count($attachments);
                        $nothiPotroRecord->subject = $dakRecord['dak_subject'];
                        $nothiPotroRecord->sarok_no = !empty($dakRecord['sender_sarok_no'])
                            ? $dakRecord['sender_sarok_no'] : '';
                        $nothiPotroRecord->application_origin = !empty($dakRecord['application_origin'])?$dakRecord['application_origin']:'nothi';
                        $nothiPotroRecord->application_meta_data = !empty($dakRecord['application_meta_data'])?$dakRecord['application_meta_data']:'';
                        $nothiPotroRecord->issue_date = date("Y-m-d H:i:s");
                        $nothiPotroRecord->due_date = date("Y-m-d H:i:s");
                        $nothiPotroRecord->created = date("Y-m-d H:i:s");
                        $nothiPotroRecord->created_by = $this->Auth->user('id');
                        $nothiPotroRecord->modified_by = $this->Auth->user('id');
                        $nothiPotroRecord = $nothiPotrosTable->save($nothiPotroRecord);

                        $nothiDakPotroMapsRecord->dak_id = $nothiPotroRecord->dak_id;
                        $nothiDakPotroMapsRecord->dak_type = $dak_type;
                        $nothiDakPotroMapsRecord->nothi_masters_id = $nothiPotroRecord->nothi_master_id;
                        $nothiDakPotroMapsRecord->nothi_part_no = $requestedData['nothi_part_no'];
                        $nothiDakPotroMapsRecord->nothi_potro_id = $nothiPotroRecord->id;
                        $nothiDakPotroMapsRecord->created_by = $this->Auth->user('id');
                        $nothiDakPotroMapsRecord->modified_by = $this->Auth->user('id');
                        $nothiDakPotroMapsTable->save($nothiDakPotroMapsRecord);

                        if (!empty($attachments)) {
                            foreach ($attachments as $attachmentKey => $attachmentValue) {
                                $potroPage++;
                                $nextPotroPagebn = Number::format($potroPage);
                                $nothiPotroAttachmentsRecord = $nothiPotroAttachmentsTable->newEntity();

                                $nothiPotroAttachmentsRecord->nothi_master_id = $requestedData['nothi_master_id'];
                                $nothiPotroAttachmentsRecord->nothi_part_no = $requestedData['nothi_part_no'];

                                $nothiPotroAttachmentsRecord->nothi_potro_id = $nothiPotroRecord->id;
                                $nothiPotroAttachmentsRecord->nothijato = $nothiPotroRecord->nothijato;
                                $nothiPotroAttachmentsRecord->is_main = $attachmentValue['is_main'];

                                // for BTRC template - need to put sarok number on body
                                if($attachmentValue['is_main'] && !empty($dakRecord['application_origin']) && strtolower($dakRecord['application_origin']) == 'btrc'){
                                    $attachmentValue['content_body'] = str_replace('{{sharok_no}}', entobn($btrc_sarok_number),$dakRecord['description']);
                                }


                                $nothiPotroAttachmentsRecord->sarok_no = $nothiPotroRecord->sarok_no;
                                $nothiPotroAttachmentsRecord->created_by = $this->Auth->user('id');
                                $nothiPotroAttachmentsRecord->modified_by = $this->Auth->user('id');
                                $nothiPotroAttachmentsRecord->created = date("Y-m-d H:i:s");
                                $nothiPotroAttachmentsRecord->modified = date("Y-m-d H:i:s");
                                $nothiPotroAttachmentsRecord->application_origin = !empty($dakRecord['application_origin'])?$dakRecord['application_origin']:'nothi';
                                $nothiPotroAttachmentsRecord->application_meta_data = !empty($dakRecord['application_meta_data'])?$dakRecord['application_meta_data']:'';
                                $nothiPotroAttachmentsRecord->nothi_potro_page = $potroPage;
                                $nothiPotroAttachmentsRecord->nothi_potro_page_bn
                                    = $nextPotroPagebn;
                                $nothiPotroAttachmentsRecord->attachment_type = $attachmentValue['attachment_type'];

                                $nothiPotroAttachmentsRecord->potro_cover = !empty($attachmentValue['content_cover'])
                                    ? $attachmentValue['content_cover'] : '';
                                $nothiPotroAttachmentsRecord->content_body = $attachmentValue['content_body'];
                                $nothiPotroAttachmentsRecord->meta_data = $attachmentValue['meta_data'];
                                $nothiPotroAttachmentsRecord->is_summary_nothi = $attachmentValue['is_summary_nothi'];
                                $nothiPotroAttachmentsRecord->potrojari_status = $attachmentValue['is_summary_nothi']
                                == 1 ? 'SummaryNothi' : '';

                                $nothiPotroAttachmentsRecord->attachment_description
                                    = $attachmentValue['attachment_description'];
                                $nothiPotroAttachmentsRecord->file_name = $attachmentValue['file_name'];
                                $nothiPotroAttachmentsRecord->file_dir = !empty($attachmentValue['file_dir']) ? $attachmentValue['file_dir'] : FILE_FOLDER;
                                $nothiPotroAttachmentsRecord->created_by = $this->Auth->user('id');
                                $nothiPotroAttachmentsRecord->modified_by = $this->Auth->user('id');
                                $nothiPotroAttachmentsTable->save($nothiPotroAttachmentsRecord);
                            }
                        }

                        $nothiDakMovesRecord->dak_movements_id = $dakLastMoveId;
                        $nothiDakMovesRecord->nothi_masters_id = $nothiPotroRecord->nothi_master_id;
                        $nothiDakMovesRecord->nothi_part_no = $nothiPotroRecord->nothi_part_no;
                        $nothiDakMovesRecord->created_by = $this->Auth->user('id');
                        $nothiDakMovesRecord->modified_by = $this->Auth->user('id');
                        $nothiDakMovesTable->save($nothiDakMovesRecord);

                        $nothiMastersRecord = $nothiMastersTable->get($nothiDakPotroMapsRecord->nothi_masters_id);

                        $seq = $this->getDakMovementSequence($value,
                            $dak_type);
                        /* Sender to Draft User Movement */
                        $dak_move = $dakMovementsTable->newEntity();
                        $dak_move->dak_type = $dak_type;
                        $dak_move->dak_id = $value;
                        $dak_move->from_office_id = $dakLastMoves['to_office_id'];
                        $dak_move->from_office_name = $dakLastMoves['to_office_name'];
                        $dak_move->from_office_address = $dakLastMoves['to_office_address'];
                        $dak_move->from_officer_id = $dakLastMoves['to_officer_id'];
                        $dak_move->from_officer_name = $dakLastMoves['to_officer_name'];
                        $dak_move->from_officer_designation_id = $dakLastMoves['to_officer_designation_id'];
                        $dak_move->from_officer_designation_label = $dakLastMoves['to_officer_designation_label'];
                        $dak_move->from_office_unit_id = $dakLastMoves['to_office_unit_id'];
                        $dak_move->from_office_unit_name = $dakLastMoves['to_office_unit_name'];
                        $dak_move->to_office_id = $dakLastMoves['to_office_id'];
                        $dak_move->to_office_name = $dakLastMoves['to_office_name'];
                        $dak_move->to_office_address = $dakLastMoves['to_office_address'];
                        $dak_move->to_officer_id = $dakLastMoves['to_officer_id'];
                        $dak_move->to_office_unit_id = $dakLastMoves['to_office_unit_id'];
                        $dak_move->to_office_unit_name = $dakLastMoves['to_office_unit_name'];
                        $dak_move->to_officer_name = $dakLastMoves['to_officer_name'];
                        $dak_move->to_officer_designation_id = $dakLastMoves['to_officer_designation_id'];
                        $dak_move->to_officer_designation_label = $dakLastMoves['to_officer_designation_label'];
                        $dak_move->attention_type = $dakLastMoves['attention_type'];
                        $dak_move->docketing_no = $dakLastMoves['docketing_no'];
                        $dak_move->from_sarok_no = $dakLastMoves['from_sarok_no'];
                        $dak_move->to_sarok_no = $dakLastMoves['to_sarok_no'];
                        $dak_move->dak_priority = $dakLastMoves['dak_priority'];
                        $dak_move->operation_type = ($nothiPotroRecord->nothijato
                            != 1) ? DAK_CATEGORY_NOTHIVUKTO : DAK_CATEGORY_NOTHIJATO;
                        $dak_move->sequence = $seq;
                        $dak_move->dak_actions = ($nothiPotroRecord->nothijato
                        != 1 ? ("নথিতে উপস্থাপন করা হয়েছে। " . "<a href='" . $this->request->webroot . "noteDetail/" . $nothiPotroRecord->nothi_part_no . "'  >নথি নম্বর " . $nothiMastersRecord['nothi_no'] . ", বিষয়: " . $nothiMastersRecord['subject'] . "</a>")
                            : "নথিজাত করা হয়েছে। ");
                        $dak_move->created_by = $this->Auth->user('id');
                        $dak_move->modified_by = $this->Auth->user('id');
                        $dakMovementsTable->save($dak_move);

                        $dakRecord->dak_status = ($nothiPotroRecord->nothijato != 1
                            ? DAK_CATEGORY_NOTHIVUKTO : DAK_CATEGORY_NOTHIJATO);
                        $dakDaptoriksTable->save($dakRecord);

                        $dakUsersTable->updateAll(['dak_category' => ($nothiPotroRecord->nothijato
                        != 1 ? DAK_CATEGORY_NOTHIVUKTO : DAK_CATEGORY_NOTHIJATO),
                            'dak_view_status' => DAK_VIEW_STATUS_VIEW],
                            ['dak_id' => $value, 'dak_type' => $dak_type, 'to_officer_designation_id' => $employee_offices['office_unit_organogram_id']]);

                        $feedbackmeta = !empty( $dakRecord['application_meta_data'])?jsonA( $dakRecord['application_meta_data']):[];

                        if(!empty($feedbackmeta['note_id']) && $nothiPotroRecord->application_origin != 'nothi'){
                            $allBsapPotros = $nothiPotrosTable->find()->where(['application_origin'=>$nothiPotroRecord->application_origin,'id <>'=>$nothiPotroRecord->id,'application_meta_data <>' => ''])->toArray();
                            if(!empty($allBsapPotros)){
                                foreach($allBsapPotros as $bsapKey=>$bsapValue){
                                    $fmeta = !empty( $bsapValue['application_meta_data'])?jsonA( $bsapValue['application_meta_data']):[];
                                    if(empty($fmeta)){
                                        continue;
                                    }

                                    if($feedbackmeta['tracking_id'] == $fmeta['tracking_id']){
                                        $fmeta['locked'] = 1;
                                    }

                                    if(strtolower($bsapValue['application_origin']) == 'btrc'){
                                        $fmeta['memorandum_no'] = isset($btrc_sarok_number)?$btrc_sarok_number:'';
                                    }

                                    $bsapValue['application_meta_data'] = json_encode($fmeta);

                                    $nothiPotrosTable->save($bsapValue);
                                }
                            }
                        }
                        if(!empty($feedbackmeta['feedback_url'])){
                            $potro_meta = jsonA($nothiPotroRecord->application_meta_data) ;
                            if(!empty($potro_meta['token_url'])){
                                if(!empty($potro_meta['api_client']) && ($potro_meta['api_client'] == 'BTRC')){
                                    $auth_token_response = $this->NotifiyFeedbackAuthTokenGeneration($potro_meta['token_url'],['user' => 'A2i','password' => '123456']);
                                    if(!empty($auth_token_response['status']) && $auth_token_response['status'] == 'success'){
                                        if(!empty($auth_token_response['token'])){
                                            $potro_meta['token'] = $auth_token_response['token'];
                                        }
                                    }
                                }
                            }
                            $feedbackdata = $potro_meta + [
                                'api_key'=>!empty($feedbackmeta['feedback_api_key'])?$feedbackmeta['feedback_api_key']:'',
                                'aid'=>$feedbackmeta['tracking_id'],
                                'action'=>1,
                                'decision'=> 1,
                                'dak_id' => $value,
                                'nothi_id' => $nothiPotroRecord->nothi_master_id,
                                'note_id' => $nothiPotroRecord->nothi_part_no,
                                'potro_id' => 0,
                                'decision_note'=> ($nothiPotroRecord->nothijato != 1 ? "নথিতে উপস্থাপন করা হয়েছে।" : "নথিজাত করা হয়েছে। "),
                                'nothi_action'=> ($nothiPotroRecord->nothijato != 1 ? 2 : 1),
                                'current_desk_id'=>$dakLastMoves['to_officer_designation_id'],

                            ];

                            $this->notifyFeedback($feedbackmeta['feedback_url'],$feedbackdata);
                        }

                        $successnothi++;

                        if ($dakRecord->is_summary_nothi == 1) {
                            $summaryNothiTable = TableRegistry::get('SummaryNothiUsers');

                            $summarynothi = $summaryNothiTable->getData(
                                [
                                    'nothi_office' => $dakRecord->sender_office_id,
                                    'current_office_id' => $employee_offices['office_id'],
                                    'dak_id' => $value
                                ])->first();

                            if (!empty($summarynothi)) {
                                $summaryNothiTable->updateAll(
                                    [
                                        'nothi_master_id' => $nothiPotroRecord->nothi_master_id,
                                        'nothi_part_no' => $nothiPotroRecord->nothi_part_no,
                                        'potro_id' => $nothiPotroRecord->id,
                                    ], ['id' => $summarynothi['id']]);
                            }
                        }
                    }
                    /*
                     * Note is from dak track
                     */
                    TableRegistry::get('NoteInitialize')->updateDakNoteStatus($requestedData['nothi_part_no'], date('Y-m-d'));
                    /*                     * Archive Record Off For Nothijat* */
//                    if ($successnothi > 0 && $requestedData['nothijato'] == 1) {
//                        $nothiMasterCurrentUsersTable->updateAll(['is_archive' => 1],
//                            ['nothi_part_no' => $nothiPartId, 'nothi_office' => $employee_offices['office_id']]);
//                    }
                } else {
                    echo json_encode(['status' => 'error', 'msg' => 'কোনো ডাক বাছাই করা হয়নি']);
                    die;
                }

                if ($successnothi == 0) {
                    echo json_encode(['status' => 'error', 'msg' => (($requestedData['nothijato']
                        == 0) ? 'নথিতে উপস্থাপন করা সম্ভব হচ্ছে না' : 'নথিজাত করা সম্ভব হচ্ছে না')]);
                    die;
                }
                $current_office_section = $employee_offices;
                $current_office_section['office_id'] = $dakLastMoves['to_office_id'];
                $current_office_section['officer_id'] = $dakLastMoves['to_officer_id'];

                if (!$this->NotificationSet($requestedData['nothijato'] != 0 ? "nothijat"
                    : 'nothivukto', array($successnothi, ""), 1,
                    $current_office_section, [], $dakRecord->dak_subject)
                ) {

                }

                if ($successnothi == $totalDak)
                    echo json_encode(['status' => 'success', 'msg' => ($requestedData['nothijato']
                    == 0 ? 'বাছাইকৃত ডাক নথিতে উপস্থাপন করা হয়েছে' : 'বাছাইকৃত ডাক নথিজাত করা হয়েছে')]);
                else
                    echo json_encode(['status' => 'success', 'msg' => ($requestedData['nothijato']
                    == 1 ? (Number::format($successnothi) . 'টি বাছাইকৃত ডাক নথিজাত করা হয়েছে')
                        : (Number::format($successnothi) . 'টি বাছাইকৃত ডাক নথিতে উপস্থাপন করা হয়েছে'))]);
            }
        }
        die;
    }

    public function index($listtype = 'inbox')
    {

        if ($this->request->is('ajax') == false) {
            return $this->redirect(['controller' => 'dashboard', 'action' => 'dashboard']);
        }
        $this->layout = null;
        $this->set('listtype', $listtype);

        $session = $this->request->session();

        $table_instance_emp_records = TableRegistry::get('EmployeeRecords');
        $user = $this->Auth->user();

        if ($user['employee_record_id'] != null) {
            $employee_info = $table_instance_emp_records->get($user['employee_record_id']);

            $table_instance_emp_offices = TableRegistry::get('EmployeeOffices');
            $employee_office_records = $table_instance_emp_offices->getEmployeeOfficeRecords($employee_info->id);
            $employee_office_recordLists = $table_instance_emp_offices->find('list', ['keyField' => 'office_unit_organogram_id', 'valueField' => 'designation'])
                ->where(['employee_record_id' => $user['employee_record_id'], 'status' => 1])->toArray();

            $data_item = array();
            $data_item['personal_info'] = $employee_info;
            $data_item['office_records'] = count($employee_office_records) > 0 ? $employee_office_records
                : array();

            $table_instance_offices = TableRegistry::get('Offices');
            $table_instance_ministris = TableRegistry::get('OfficeMinistries');
            $officeInformation = array();
            $ministryInformation = array();
            if (!empty($employee_office_records[0]['office_id'])) {
                $officeInformation = $table_instance_offices->get($employee_office_records[0]['office_id']);

                if (!empty($officeInformation)) {
                    $ministryInformation = $table_instance_ministris->get($officeInformation['office_ministry_id']);
                }
            }

            $data_item['ministry_records'] = count($ministryInformation) > 0 ? $ministryInformation
                : array();

            $session = $this->request->session();
            if (!$session->check('logged_employee_record')) {
                $session->write('logged_employee_record', $data_item);
            }

            if (!empty($this->request->data['selected_office_unit_id'])) {
                $office_section['office_id'] = intval($this->request->data['selected_office_id']);
                $office_section['office_unit_id'] = intval($this->request->data['selected_office_unit_id']);
                $office_section['office_unit_organogram_id'] = intval($this->request->data['selected_office_unit_organogram_id']);
                if (!in_array($office_section['office_unit_organogram_id'], array_keys($employee_office_recordLists))) {

                    $this->Flash->error("Unauthorize access");
                    return true;
                }
                $office_section['officer_id'] = $user['employee_record_id'];
                $office_section['incharge_label'] = $employee_office_records[0]['incharge_label'];

                $table_instance_office = TableRegistry::get('Offices');
                $office = $table_instance_office->get($office_section['office_id']);

                $office_section['office_name'] = $office['office_name_bng'];
                $office_section['office_address'] = $office['office_address'];
                $office_section['designation_label'] = $this->request->data['selected_office_unit_organogram_label'];
                $office_section['officer_name'] = $employee_info['name_bng'];
                $ministryInformation = $table_instance_ministris->get($office['office_ministry_id']);
                $office_section['ministry_records'] = count($ministryInformation)
                > 0 ? $ministryInformation['name_bng'] : '';


                $office_section['office_phone'] = $office['office_phone'];
                $office_section['office_fax'] = $office['office_fax'];
                $office_section['office_email'] = $office['office_email'];
                $office_section['office_web'] = $office['office_web'];

                $table_instance_office_unit = TableRegistry::get('OfficeUnits');
                $office_unit = $table_instance_office_unit->get($office_section['office_unit_id']);
                $office_section['office_unit_name'] = $office_unit['unit_name_bng'];

                $session->write('selected_office_section', $office_section);
            }
        }
        $selected_office_section = $session->read('selected_office_section');

        if (empty($selected_office_section)) {
            $this->Flash->error("দুঃখিত! কোনো শাখা বাছাই করা হয়নি");
            return $this->redirect(array('action' => 'dashboard', 'controller' => 'dashboard'));
        }

        $this->set('selected_office_section',
            $session->read('selected_office_section'));
    }

    public function inboxContent($listype = 'all')
    {
        $this->layout = null;
        $session = $this->request->session();
        $logged_user = $this->getCurrentDakSection();
        $session->write('refer_url', $listype);
        $this->view = 'inbox_content_new';
        $nothiMasterCurrentUserTable = TableRegistry::get('NothiMasterCurrentUsers');
        $totalNoteCount = $nothiMasterCurrentUserTable->find()->where([
            'nothi_office' => $logged_user['office_id'],
            'office_unit_organogram_id' => $logged_user['office_unit_organogram_id'],
            'is_archive' => 0,
            'is_finished' => 0,
            'is_new' => 0
        ])->count();

        $totalOtherNoteCount = $nothiMasterCurrentUserTable->find()->where([
            'nothi_office !=' => $logged_user['office_id'],
            'office_unit_organogram_id' => $logged_user['office_unit_organogram_id'],
            'office_unit_id' => $logged_user['office_unit_id'],
            'is_archive' => 0,
            'is_finished' => 0,
            'is_new' => 0
        ])->count();
        $this->set(compact(['listtype' => $listype, 'totalNoteCount', 'totalOtherNoteCount']));
    }

    public function archivedNothiInbox() {
        $this->layout = null;
        $session = $this->request->session();
        $session->write('refer_url', 'archiveNothi');
        $this->set('listtype', 'archiveNothi');
    }

    public function folderContent($listType = 'all')
    {
        $this->layout = null;
        $this->set('listtype', $listType);
    }

//    public function folderView($listType = 'all')
//    {
//
//        $this->layout = null;
//        $nothiMastersTable = TableRegistry::get("NothiMasters");
//        $nothiPartsTable = TableRegistry::get("NothiParts");
//
//        $session = $this->request->session();
//        $employee = $session->read('logged_employee_record');
//        $employee_office = $this->getCurrentDakSection();
//        $nothiMasterPermissionsTable = TableRegistry::get("NothiMasterPermissions");
//        $nothiMasterCurrentUserTable = TableRegistry::get("NothiMasterCurrentUsers");
//        $nothiMasterMovementTable = TableRegistry::get("NothiMasterMovements");
//
//        $nothiMastersId = '';
//        $nothiMastersCurrentUser = array();
//
//        $start = isset($this->request->data['start']) ? intval($this->request->data['start'])
//            : 0;
//        $len = isset($this->request->data['length']) ? intval($this->request->data['length'])
//            : 500;
//        $page = ($start / $len) + 1;
//
//        $condition = '';
//
//        $subject = isset($this->request->data['subject']) ? trim($this->request->data['subject'])
//            : '';
//        if (!empty($subject)) {
//            if (!empty($condition)) $condition .= ' AND ';
//            $condition .= "subject LIKE '%{$subject}%'";
//        }
//
//        $type_name = isset($this->request->data['type_name']) ? trim($this->request->data['type_name'])
//            : '';
//        if (!empty($type_name)) {
//            if (!empty($condition)) $condition .= ' AND ';
//            $condition .= "NothiTypes.type_name LIKE '%{$type_name}%'";
//        }
//
////        $unit_name_bng = isset($this->request->data['unit_name_bng']) ? trim($this->request->data['unit_name_bng']) : '';
////        if (!empty($unit_name_bng)) {
////            if (!empty($condition))
////                $condition .= ' AND ';
////            $condition .= "OfficeUnits.unit_name_bng LIKE '%{$unit_name_bng}%'";
////        }
//
//
//        $nothi_no = isset($this->request->data['nothi_no']) ? trim($this->request->data['nothi_no'])
//            : '';
//        if (!empty($nothi_no)) {
//            if (!empty($condition)) $condition .= ' AND ';
//            $condition .= "nothi_no LIKE '%{$nothi_no}%'";
//        }
//
//        if (empty($employee['office_records'])) {
//            $json_data = array(
//                "draw" => 0,
//                "recordsTotal" => 0,
//                "recordsFiltered" => 0,
//                "data" => []
//            );
//            $this->response->body(json_encode($json_data));
//            $this->response->type('application/json');
//            return $this->response;
//        }
//
//        if ($this->request->is('ajax')) {
//            $data = [];
//            $currentNothiMasterArray = array();
//            $currentNothiMasterViewStatusArray = array();
//
//            $nothiMastersCurrentUser = $nothiMasterCurrentUserTable->find()->select(['nothi_part_no',
//                'nothi_master_id', 'view_status'])->where(['office_unit_organogram_id' => $employee_office['office_unit_organogram_id'],
//                'office_unit_id' => $employee_office['office_unit_id'], 'nothi_office' => $employee_office['office_id']])->toArray();
//
//            $currentNothiMaster = '';
//            if (!empty($nothiMastersCurrentUser)) {
//                foreach ($nothiMastersCurrentUser as $key => $currentUser) {
//                    $currentNothiMasterArray[] = $currentUser['nothi_part_no'];
//                    $currentNothiMasterViewStatusArray[$currentUser['nothi_master_id']]
//                        = ($currentUser['view_status'] == 0 ? (!isset($currentNothiMasterViewStatusArray[$currentUser['nothi_master_id']])
//                        ? 1 : (++$currentNothiMasterViewStatusArray[$currentUser['nothi_master_id']]))
//                        : (!isset($currentNothiMasterViewStatusArray[$currentUser['nothi_master_id']])
//                            ? 0 : $currentNothiMasterViewStatusArray[$currentUser['nothi_master_id']]));
//                    $currentNothiMaster .= $currentUser['nothi_part_no'] . ',';
//                }
//            }
//
//            if ($listType == 'inbox') {
//
//                if (!empty($currentNothiMaster)) {
//                    if (!empty($condition)) {
//                        $condition .= " AND ";
//                    }
//                    $condition .= ' NothiParts.id IN (' . substr($currentNothiMaster,
//                            0, -1) . ')';
//                } else {
//                    $condition = ' 0 ';
//                }
//            } else if ($listType == 'sent') {
//
//                $sentNothi = $nothiMasterMovementTable->find()->select(["nothimasterid" => "DISTINCT(nothi_part_no)"])->where(['from_office_unit_id' => $employee_office['office_unit_id'],
//                    'from_officer_designation_id' => $employee_office['office_unit_organogram_id']])->toArray();
//
//                if (!empty($sentNothi)) {
//                    $sentNothiMaster = '';
//                    if (!empty($condition)) $condition .= " AND ";
//
//                    foreach ($sentNothi as $ke => $sentNothiId) {
//                        $sentNothiMaster .= $sentNothiId['nothimasterid'] . ',';
//                    }
//
//                    $condition .= ' NothiParts.id IN (' . substr($sentNothiMaster,
//                            0, -1) . ')';
//
//                    if (!empty($currentNothiMaster)) {
//                        $condition .= 'AND NothiParts.id NOT IN (' . substr($currentNothiMaster,
//                                0, -1) . ')';
//                    }
//                } else {
//                    $condition = " 0 ";
//                }
//            } else if ($listType == 'office') {
//                if (!empty($condition)) $condition .= " AND ";
//
//                $condition .= ' NothiParts.office_id = ' . $employee_office['office_id'];
//            } else {
//                $nothiMastersPermitted = $nothiMasterPermissionsTable->getMasterNothiList($employee_office['office_id'],
//                    $employee_office['office_id'],
//                    $employee_office['office_unit_id'],
//                    $employee_office['office_unit_organogram_id']);
//                $nothiMastersId = "";
//                if (!empty($nothiMastersPermitted)) {
//                    foreach ($nothiMastersPermitted as $nothiKey => $nothiId) {
//                        $nothiMastersId .= $nothiId['nothi_masters_id'] . ',';
//                    }
//                    if (!empty($condition)) $condition .= " AND ";
//                    $condition .= ' NothiParts.nothi_masters_id IN (' . substr($nothiMastersId,
//                            0, -1) . ') ';
//                } else {
//                    $condition = " 0 ";
//                }
//            }
//
//            $totalRec = $nothiPartsTable->getAll($condition)->group(['NothiParts.nothi_masters_id'])->toArray();
//            $nothiMasterRecord = $nothiPartsTable->getAll($condition)->group(['NothiParts.nothi_masters_id'])->toArray();
//
//            $html = '';
//
//            $office_unit_table = TableRegistry::get('OfficeUnits');
//
//            if (!empty($nothiMasterRecord)) {
//                $i = 0;
//                $html = "<div class='row' style='margin-bottom: 8px;'>";
//                foreach ($nothiMasterRecord as $key => $record) {
//                    $officeUnit = $office_unit_table->get($record['office_units_id']);
//
//                    $employeeInfo = $nothiMasterCurrentUserTable->getCurrentUserDetails($record['nothi_masters_id'],
//                        $record['id'], $record['office_id']);
//                    $i++;
//                    $html .= "<div class='col-lg-3 col-md-4 col-sm-6 col-xs-12'>";
//
//                    $html .= '<div class="dashboard-stat blue-madison"><div class="visual"><i class="fa fa-book fa-icon-medium"></i></div><div class="details"><div class="number">' . $record['nothi_no'] . (isset($currentNothiMasterViewStatusArray[$record['nothi_masters_id']])
//                        && $currentNothiMasterViewStatusArray[$record['nothi_masters_id']]
//                        > 1 ? (' <label class="badge badge-success">' . Number::format($currentNothiMasterViewStatusArray[$record['nothi_masters_id']]) . '</label> ')
//                            : '') . '</div><div class="desc">' . "শাখা: " . $officeUnit['unit_name_bng'] . '<br/>নথির ধরন: ' . $record['NothiTypes']['type_name'] . '<br/>বিষয়: ' . $record['subject'] . '<br/>তারিখ: ' . __("{$record['modified']}") . "<br/><div class='text-right' style='vertical-align: bottom;padding-top: 5px;'>" .
//                        '<a title="বিস্তারিত"  data-title="বিস্তারিত" href="' . $this->request->webroot . 'nothiDetail/' . $record['id'] . '" class="btn btn-success btn-xs" ><i class=" a2i_gn_details2"></i></a>' . "</div><br/></div>" . '</div><a class="more" href="javascript:;">দায়িত্বে: ' . (isset($employeeInfo)
//                            ? $employeeInfo : '') . ' </a></div>';
//
//                    $html .= '</div>';
//                }
//                $html .= "</div>";
//            }
//
//            $json_data = array(
//                "recordsTotal" => count($totalRec),
//                "data" => $html
//            );
//            $this->response->body(json_encode($json_data));
//            $this->response->type('application/json');
//            return $this->response;
//        } else {
//            $this->response->body(json_encode(0));
//            $this->response->type('application/json');
//            return $this->response;
//        }
//    }

    public function nothiMastersListBk($listType = 'all')
    {
        $this->layout = null;

        $nothiMasterPermissionsTable = TableRegistry::get("NothiMasterPermissions");
        $nothiMasterCurrentUserTable = TableRegistry::get("NothiMasterCurrentUsers");
        $session = $this->request->session();
        $employee = $session->read('logged_employee_record');
        $employee_office = $this->getCurrentDakSection();

        $nothiPartsTable = TableRegistry::get("NothiParts");
        $PotrojariReceiverTable = TableRegistry::get("PotrojariReceiver");
        $nothiMasterMovementTable = TableRegistry::get("NothiMasterMovements");
        $potroAttachmentTable = TableRegistry::get('NothiPotroAttachments');
        $potrojariTable = TableRegistry::get('Potrojari');

        $nothiMastersId = '';
        $nothiMastersCurrentUser = array();

        $start = isset($this->request->data['start']) ? intval($this->request->data['start']) : 0;
        $len = isset($this->request->data['length']) ? intval($this->request->data['length']) : 200;
        $page = !empty($this->request->data['page']) ? intval($this->request->data['page']) : (($start / $len) + 1);

        $condition = '';
        $condition2 = '';

        $subject = isset($this->request->data['subject']) ? h(trim($this->request->data['subject']))
            : '';
        if (!empty($subject)) {
            if (!empty($condition)) $condition .= ' AND ';
            $condition .= "NothiParts.subject LIKE '%{$subject}%'";
            $condition2 .= "NothiParts2.subject LIKE '%{$subject}%'";
        }

        $unit = isset($this->request->data['unit']) ? h(trim($this->request->data['unit']))
            : '';
        if (!empty($unit)) {
            if (!empty($condition)) $condition .= ' AND ';
            $condition .= "NothiParts.office_units_id LIKE '%{$unit}%'";
            $condition2 .= "NothiParts2.office_units_id LIKE '%{$unit}%'";
        }

        $type_name = isset($this->request->data['type_name']) ? h(trim($this->request->data['type_name']))
            : '';
        if (!empty($type_name)) {
            if (!empty($condition)) $condition .= ' AND ';
            $condition .= "NothiTypes.type_name LIKE '%{$type_name}%'";
        }

        $nothi_no = isset($this->request->data['nothi_no']) ? h(trim($this->request->data['nothi_no']))
            : '';
        if (!empty($nothi_no)) {
            if (!empty($condition)) $condition .= ' AND ';
            $condition .= "NothiParts.nothi_no LIKE '%{$nothi_no}%'";
            $condition2 .= "NothiParts2.nothi_no LIKE '%{$nothi_no}%'";
        }

        $from_date = isset($this->request->data['from']) ? h(trim($this->request->data['from']))
            : '';
        if (!empty($from_date)) {
            if (!empty($condition)) $condition .= ' AND ';
            $condition .= "  date(NothiParts.modified) >= '{$from_date}'";
            $condition2 .= "  date(NothiParts2.modified) >= '{$from_date}'";
        }

        $end_date = isset($this->request->data['to']) ? h(trim($this->request->data['to']))
            : '';
        if (!empty($end_date)) {
            if (!empty($condition)) $condition .= ' AND ';
            $condition .= "  date(NothiParts.modified) <= '{$end_date}'";
            $condition2 .= "  date(NothiParts2.modified) <= '{$end_date}'";
        }
        $show_only_important = isset($this->request->data['show_only_important']) ? $this->request->data['show_only_important'] : 0;

        if (empty($employee['office_records'])) {
            $json_data = array(
                "draw" => 0,
                "recordsTotal" => 0,
                "recordsFiltered" => 0,
                "data" => []
            );
            $this->response->body(json_encode($json_data));
            $this->response->type('application/json');
            return $this->response;
        }
        $noteTable = TableRegistry::get('NothiNotes');

        if ($this->request->is('ajax')) {
            if ($listType == 'other_sent') {
                $json_data = $this->nothiMasterListForOtherOfficesSent($nothiMasterCurrentUserTable, $this->request->data, $page, $len, $employee_office);
                $this->response->body(json_encode($json_data));
                $this->response->type('application/json');
                return $this->response;
            }
            if ($listType == 'other') {
                $json_data = $this->nothiMasterListForOtherOffices($nothiMasterCurrentUserTable, $employee_office, $nothiMasterPermissionsTable, $condition, [], $page, $len, $this->request->data);
                $this->response->body(json_encode($json_data));
                $this->response->type('application/json');
                return $this->response;
            }

            $data = [];
            $currentNothiMasterArray = array();
            $currentNothiMasterViewStatusArray = array();
            $currentNothiMasterTotalArray = array();

            $totalRec = array();
            $nothiMasterRecord = array();

            $show_only_important = isset($this->request->data['show_only_important']) ? $this->request->data['show_only_important'] : 0;

            $nothi_master_conditions = [];
            $nothi_master_conditions['office_unit_organogram_id'] = $employee_office['office_unit_organogram_id'];
            $nothi_master_conditions['office_unit_id'] = $employee_office['office_unit_id'];
            $nothi_master_conditions['nothi_office'] = $employee_office['office_id'];
            $nothi_master_conditions['is_archive'] = 0;
            //only inbox and other_inbox search option consider joruri params
            if ($listType == 'inbox') {
                if (!empty($show_only_important)) {
                    $nothi_master_conditions['priority'] = $show_only_important;
                }
            }
            $nothiMastersCurrentUser = $nothiMasterCurrentUserTable->find()->select(['nothi_master_id',
                'nothi_office', 'nothi_part_no', 'view_status', 'issue_date', 'priority'])->where($nothi_master_conditions)->order('issue_date desc')->toArray();

            //part 1
            $currentNothiMaster = '';
            $currentNothiIssueDate = array();
            if (!empty($nothiMastersCurrentUser)) {
                $minarray = array();
                $priority = [];
                foreach ($nothiMastersCurrentUser as $key => $currentUser) {
                    $currentNothiMasterArray[] = $currentUser['nothi_part_no'];
                    $minarray[$currentUser['nothi_master_id']][] = $currentUser['view_status'];

                    if (!isset($priority[$currentUser['nothi_master_id']])) {
                        $priority[$currentUser['nothi_master_id']] = 0;
                    } else if ($currentUser['priority'] == 1) {
                        $priority[$currentUser['nothi_master_id']] = $currentUser['priority'];
                    }
                    $currentNothiMasterViewStatusArray[$currentUser['nothi_master_id']]
                        = ($currentUser['view_status'] == 0 ? (!isset($currentNothiMasterViewStatusArray[$currentUser['nothi_master_id']])
                        ? 1 : (++$currentNothiMasterViewStatusArray[$currentUser['nothi_master_id']]))
                        : (!isset($currentNothiMasterViewStatusArray[$currentUser['nothi_master_id']])
                            ? 0 : $currentNothiMasterViewStatusArray[$currentUser['nothi_master_id']]));


                    $firstsubject = $noteTable->getFirstNote($currentUser['nothi_part_no']);
                    $currentNothiMasterTotalArray[$currentUser['nothi_master_id']][$currentUser['nothi_part_no']]
                        = $firstsubject['subject'];

                    $prevmax = $currentUser['view_status'];
                    $currentNothiMaster .= $currentUser['nothi_part_no'] . ',';
                    $currentNothiIssueDate[$currentUser['nothi_master_id']] = (!isset($currentNothiIssueDate[$currentUser['nothi_master_id']])
                    || $currentNothiIssueDate[$currentUser['nothi_master_id']]
                    < $currentUser['issue_date'] ? $currentUser['issue_date']
                        : $currentNothiIssueDate[$currentUser['nothi_master_id']]);
                }
            }

            //part 2
            $nothiCurrentUserCondition = '';

            if ($listType == 'inbox') {

                if (!empty($currentNothiMaster)) {
                    if (!empty($condition)) {
                        $condition .= " AND ";
                    }
                    $condition .= ' NothiParts.id IN (' . substr($currentNothiMaster,
                            0, -1) . ')';
                } else {
                    $condition = ' 0 ';
                }
            } else if ($listType == 'sent') {
                $sent_date_array = [];
                $sent_priority = [];
                $sentNothi = $nothiMasterMovementTable->find()->select(["nothimasterid" => "DISTINCT(nothi_part_no)", 'sent_date' => 'created', 'real_nothi_master_id' => 'nothi_master_id', 'priority'])->where(['from_office_unit_id' => $employee_office['office_unit_id'],
                    'from_officer_designation_id' => $employee_office['office_unit_organogram_id']])->order(['modified desc'])->toArray();

                if (!empty($sentNothi)) {
                    $sentNothiMaster = '';
                    if (!empty($condition)) $condition .= " AND ";
                    if (!empty($condition2)) $condition2 .= " AND ";

                    foreach ($sentNothi as $ke => $sentNothiId) {
                        $sentNothiMaster .= $sentNothiId['nothimasterid'] . ',';
                        if (!empty($sent_date_array[$sentNothiId['real_nothi_master_id']]) && $sent_date_array[$sentNothiId['real_nothi_master_id']] < $sentNothiId['sent_date']) {
                            $sent_date_array[$sentNothiId['real_nothi_master_id']] = $sentNothiId['sent_date'];
                        } else if (empty($sent_date_array[$sentNothiId['real_nothi_master_id']])) {
                            $sent_date_array[$sentNothiId['real_nothi_master_id']] = $sentNothiId['sent_date'];
                        }
                        if (!empty($sentNothiId['priority']) && empty($sent_priority[$sentNothiId['real_nothi_master_id']])) {
                            $sent_priority[$sentNothiId['real_nothi_master_id']] = $sentNothiId['priority'];
                        } elseif (empty($sent_priority[$sentNothiId['real_nothi_master_id']])) {
                            $sent_priority[$sentNothiId['real_nothi_master_id']] = 0;
                        }
                    }

                    $condition .= ' NothiParts.id IN (' . substr($sentNothiMaster, 0, -1) . ')';
                    $condition2 .= ' NothiParts2.id IN (' . substr($sentNothiMaster, 0, -1) . ')';
                    // No need to check if sent nothi current user is me.
//                    if (!empty($currentNothiMaster)) {
//                        $condition .= 'AND NothiParts.id NOT IN (' . substr($currentNothiMaster,
//                                0, -1) . ')';
//                    }
                } else {
                    $condition = " 0 ";
                    $condition2 = " 0 ";
                }
            } else if ($listType == 'office') {
                if (!empty($condition)) $condition .= " AND ";
                if (!empty($condition2)) $condition2 .= " AND ";

                $condition .= ' NothiParts.office_id = ' . $employee_office['office_id'];
                $condition2 .= ' NothiParts2.office_id = ' . $employee_office['office_id'];
            } else if ($listType == 'other' || $listType == 'other_sent') {

            } else {
                $nothiMastersPermitted = $nothiMasterPermissionsTable->getMasterNothiList($employee_office['office_id'],
                    $employee_office['office_id'],
                    $employee_office['office_unit_id'],
                    $employee_office['office_unit_organogram_id'],
                    $employee_office['office_id']);
                $nothiMastersId = "";
                if (!empty($nothiMastersPermitted)) {
                    foreach ($nothiMastersPermitted as $nothiKey => $nothiId) {
                        $nothiMastersId .= $nothiId['nothi_masters_id'] . ',';
                    }
                    if (!empty($condition)) $condition .= " AND ";
                    if (!empty($condition2)) $condition2 .= " AND ";
                    $condition .= ' NothiParts.nothi_masters_id IN (' . substr($nothiMastersId,
                            0, -1) . ') ';
                    $condition2 .= ' NothiParts2.nothi_masters_id IN (' . substr($nothiMastersId,
                            0, -1) . ') ';
                } else {
                    $condition = " 0 ";
                    $condition2 = " 0 ";
                }
            }

            if ($listType == 'inbox') {
                $totalRec = $nothiPartsTable->getAll($condition, $page,
                    $len)->join([
                    "NothiNotes" => [
                        'table' => 'nothi_notes', 'type' => 'inner',
                        'conditions' => ['NothiParts.id =NothiNotes.nothi_part_no']
                    ]
                ])->group(['NothiParts.nothi_masters_id'])->order(['NothiParts.modified desc'])->count();


                $nothiMasterRecord = $nothiPartsTable->getAll($condition, $page,
                    $len)->join([
                    "NothiNotes" => [
                        'table' => 'nothi_notes', 'type' => 'inner',
                        'conditions' => ['NothiParts.id =NothiNotes.nothi_part_no']
                    ]
                ])
                    ->group(['NothiParts.nothi_masters_id'])->order(['NothiParts.modified desc'])->toArray();
            } else if ($listType == 'other') {
                $response = $this->nothiMasterListForOtherOffices($nothiMasterCurrentUserTable, $employee_office, $nothiMasterPermissionsTable, $condition, $nothiMasterRecord, $page, $len, $this->request->data);
                if (!empty($response)) {
                    foreach ($response as $res_k => $res_v) {
                        $$res_k = $res_v;
                    }
                }
            } else {

                $nothiMasterRecord = $nothiPartsTable->getAll($condition, $page,
                    $len)->join([
                    "NothiParts2" => [
                        'className' => 'NothiParts',
                        'table' => 'nothi_parts', 'type' => 'left',
                        'conditions' => ['NothiParts.nothi_masters_id =NothiParts2.nothi_masters_id AND NothiParts2.modified > NothiParts.modified AND ' . $condition2]
                    ]
                ])->where(['NothiParts2.id is null'])->group(['NothiParts.nothi_masters_id'])->toArray();

                $totalRec = $nothiPartsTable->getAll($condition)->join([
                    "NothiParts2" => [
                        'className' => 'NothiParts',
                        'table' => 'nothi_parts', 'type' => 'left',
                        'conditions' => ['NothiParts.nothi_masters_id =NothiParts2.nothi_masters_id AND NothiParts2.modified > NothiParts.modified AND ' . $condition2]
                    ]
                ])->where(['NothiParts2.id is null'])->group(['NothiParts.nothi_masters_id'])->count();
            }

            $table_office_unit = TableRegistry::get('OfficeUnits');

            if (!empty($nothiMasterRecord)) {
                $i = 0;
                $nothiClass = json_decode(NOTHI_CLASS, true);
                $last_office_id = 0;
                $key = -1;
                foreach ($nothiMasterRecord as $keyRecord => $record) {

                    if ($show_only_important == 1) {
//                        if ($listType == 'sent' && isset($sent_priority[$record['nothi_masters_id']]) && $sent_priority[$record['nothi_masters_id']] != 1) {
//                            unset($sent_priority[$record['nothi_masters_id']]);
//                            $totalRec--;
//                            continue;
//                        }
                        if ($listType == 'inbox' && isset($priority[$record['nothi_masters_id']]) && $priority[$record['nothi_masters_id']] != 1) {
                            unset($priority[$record['nothi_masters_id']]);
                            $totalRec--;
                            continue;
                        }
                    }
                    $key++;
                    if (empty($last_office_id) || ($record['office_id'] != $employee_office['office_id'] &&
                            $last_office_id != $record['office_id'])) {
                        $last_office_id = $record['office_id'];
                        try {
                            $this->switchOffice($record['office_id'], 'otherOffices');
                            TableRegistry::remove('NothiParts');
                            $nothiPartsTable = TableRegistry::get('NothiParts');
                        } catch (\Exception $ex) {

                        }
                    }
                    $lastNote = $nothiPartsTable->lastNote($record['nothi_masters_id']);
                    // add cache so that no extra query required
                    if (($unit = Cache::read('getNameWithOffice_' . $record['office_units_id'], 'memcached')) === false) {
                        $unit = $table_office_unit->getNameWithOffice($record['office_units_id']);
                        Cache::write('getNameWithOffice_' . $record['office_units_id'], $unit, 'memcached');
                    }
                    // add cache so that no extra query required

                    $getDraftDetails = 0;
                    $draftSummary = 0;
                    $summaryAttachmentRecord = 0;
                    $hasSummary = 0;
                    if ($listType != 'other') {
                        $getDraftDetails = $PotrojariReceiverTable->find()->where(['receiving_office_id' => $employee_office['office_id'],
                            'potro_status' => 'SummaryDraft', 'nothi_master_id' => $record['nothi_masters_id']])->count();

                        $draftSummary = $potrojariTable->find()->where(['potro_status' => 'SummaryDraft',
                            'nothi_master_id' => $record['nothi_masters_id'],
                            'is_summary_nothi' => 1])->count();

                        $summaryAttachmentRecord = $potroAttachmentTable->find()->where(['nothi_master_id' => $record['nothi_masters_id'],
                            'is_summary_nothi' => 1])->count();

                        $hasSummary = ($getDraftDetails >= 1 ? 1 : ($draftSummary
                        >= 1 ? 1 : ($summaryAttachmentRecord >= 1 ? 1 : 0)));
                    }
//end

                    $employeeInfo = $nothiMasterCurrentUserTable->getCurrentUserDetails($record['nothi_masters_id'],
                        $record['id'], $record['office_id']);
                    $i++;
                    $si = (($page - 1) * $len) + $i;

                    if ($listType == 'inbox') {
                        $time = isset($currentNothiIssueDate[$record['nothi_masters_id']])
                            ? (new Time($currentNothiIssueDate[$record['nothi_masters_id']]))
                            : (new Time());
                        $dattime = $currentNothiIssueDate[$record['nothi_masters_id']]
                            = $time->format('Y-m-d H:i:s');

                        $datedif = $this->dateDiff($dattime);
                    } else if ($listType == 'other') {
                        if (!empty($othercurrentNothiIssueDate[$record['office_id']][$record['nothi_masters_id']])) {
                            $time = new Time($othercurrentNothiIssueDate[$record['office_id']][$record['nothi_masters_id']]);
                            $dattime = $othercurrentNothiIssueDate[$record['office_id']][$record['nothi_masters_id']] = $time->format('Y-m-d H:i:s');
                            $datedif = $this->dateDiff($dattime);
                        } else if (!empty($lastNote['created'])) {
                            $time = new Time($lastNote['created']);
                            $datedif = $this->dateDiff($time->format('Y-m-d H:i:s'));
                        } else {
                            $time = new Time(date('Y-m-d H:i:s'));
                            $datedif = $this->dateDiff($time->format('Y-m-d H:i:s'));
                        }
                    }
                    $crTime = ($lastNote['nothi_created_date'] != '0000-00-00' || $record['nothi_created_date']
                        != '') ? New Time($lastNote['nothi_created_date']) : New Time($lastNote['modified']);
                    // it's time to make data array
//                    if($listType == 'sent'){
                    $data[] = [];
                    array_push($data[$key], "<div class='text-center'>" . Number::format($si) . "</div>");

                    array_push($data[$key], ("<div class='text-left'>" . h($record['nothi_no']) .
                        (isset($currentNothiMasterTotalArray[$record['nothi_masters_id']])
                        && count($currentNothiMasterTotalArray[$record['nothi_masters_id']])
                        > 1 ? (' <label class="badge badge-primary" data-title="' . (implode('&#10;',
                                array_values($currentNothiMasterTotalArray[$record['nothi_masters_id']]))) . '">' .
                                Number::format(count($currentNothiMasterTotalArray[$record['nothi_masters_id']])) . '</label> ')
                            : '') . "</div>"));

                    array_push($data[$key], "<div class='text-left'>" . h($record['subject']) . "</div>");

                    if ($listType == 'sent') {
                        if (empty($sent_date_array[$record['nothi_masters_id']])) {
                            $sent_date_array[$record['nothi_masters_id']] = new Time(date('Y-m-d H:i:s'));
                        } else {
                            $sent_date_array[$record['nothi_masters_id']] = new Time($sent_date_array[$record['nothi_masters_id']]);
                        }
                        array_push($data[$key], "<div class='text-center'>" .
                            $sent_date_array[$record['nothi_masters_id']]->i18nFormat(null, null, 'bn-BD') . "</div>");
                    }

                    array_push($data[$key], ($listType == 'inbox' || $listType == 'other' ?
                        ("<div class='text-center'>" . $crTime->i18nFormat("dd-MM-YYYY") . '
                                <p class="text-danger">(' . $datedif . ')</p>' . "</div>")
                        : "<div class='text-left'>" . (isset($employeeInfo)
                            ? $employeeInfo : '') . "</div>"));

                    if ($listType == 'sent') {
                        array_push($data[$key], "<div class='text-center'>" .h($unit['unit_name_bng'])).'</div>';
                    } else {
                        array_push($data[$key], "<div class='text-center'>" .h($unit['unit_name_bng']) . ', ' .
                            h($unit['office_name_bng']).'</div>');
                    }

                    array_push($data[$key], "<div class='text-center'>" .
                        '<a title="বিস্তারিত"  data-title="বিস্তারিত" href="' . (($listType
                            != 'other') ? (isset($currentNothiMasterViewStatusArray[$record['nothi_masters_id']])
                            ? ($this->request->webroot . 'noteDetail/' . $record['id'])
                            : ($this->request->webroot . ($listType != 'sent' ? 'nothiDetail/' : 'noteDetail/') . $record['id']))
                            : (isset($othercurrentNothiMasterViewStatusArray[$record['office_id']][$record['nothi_masters_id']])
                                ? ($this->request->webroot . 'noteDetail/' . $record['id'] . '/' . $record['office_id'])
                                : ($this->request->webroot . 'nothiDetail/' . $record['id'] . '/' . $record['office_id']))) . '" class="btn btn-success btn-xs dbl_click" ><i class=" a2i_gn_details2"></i></a>' . "</div>");
                    if ($listType == 'inbox') {
                        array_push($data[$key],
                            "<div class='text-center'><input type='checkbox' class='checkarchive' data-nothimasterid='{$record['nothi_masters_id']}' /></div>");
                    }
                    if ($listType == 'sent') {
                        $data[$key]['DT_RowClass'] = ($sent_priority[$record['nothi_masters_id']] == 1) ? 'nothiImportant' : '';
                    } else {
                        if ($listType == 'inbox' && isset($priority[$record['nothi_masters_id']]) && $priority[$record['nothi_masters_id']] == 1) {
                            $data[$key]['DT_RowClass'] = 'nothiImportant';
                        } else {
                            $data[$key]['DT_RowClass'] = (((in_array($record['id'],
                                    $currentNothiMasterArray) && isset($currentNothiMasterViewStatusArray[$record['nothi_masters_id']])
                                && $currentNothiMasterViewStatusArray[$record['nothi_masters_id']]
                                > 0) ? ($hasSummary ? 'summaryList' : 'new') : ($hasSummary
                                ? 'summaryList' : '')));
                        }

                    }
                }
            }

            $json_data = array(
                "draw" => intval($this->request->data['draw']),
                "recordsTotal" => $totalRec,
                "recordsFiltered" => $totalRec,
                "data" => $data
            );
            $this->response->body(json_encode($json_data));
            $this->response->type('application/json');
            return $this->response;
        } else {
            $this->response->body(json_encode(0));
            $this->response->type('application/json');
            return $this->response;
        }
    }

    public function nothiMastersList($listType = 'all', $organogram_id = false)
    {
        $this->layout = null;

        $nothiMasterPermissionsTable = TableRegistry::get("NothiMasterPermissions");
        $nothiMasterCurrentUserTable = TableRegistry::get("NothiMasterCurrentUsers");
        $session = $this->request->session();
        $employee = $session->read('logged_employee_record');
        $employee_office = $this->getCurrentDakSection();

        $nothiPartsTable = TableRegistry::get("NothiParts");
        $PotrojariReceiverTable = TableRegistry::get("PotrojariReceiver");
        $nothiMasterMovementTable = TableRegistry::get("NothiMasterMovements");
        $potroAttachmentTable = TableRegistry::get('NothiPotroAttachments');
        $potrojariTable = TableRegistry::get('Potrojari');

        if ($organogram_id) {
        	$designation_info = designationInfo($organogram_id);
        	$employee_office = $designation_info;
		}

        $nothiMastersId = [];
        $nothiMastersCurrentUser = array();

        $start = isset($this->request->data['start']) ? intval($this->request->data['start']) : 0;
        $len = isset($this->request->data['length']) ? intval($this->request->data['length']) : 200;
        $page = !empty($this->request->data['page']) ? intval($this->request->data['page']) : (($start / $len) + 1);

        $condition = [];
        $condition2 = [];

        $subject = isset($this->request->data['subject']) ? h(trim($this->request->data['subject']))
            : '';
        if (!empty($subject)) {
            array_push($condition,['NothiParts.subject LIKE'=>"%".$subject."%"]);
            array_push($condition2,['NothiParts2.subject LIKE'=>"%".$subject."%"]);

        }

        $unit = isset($this->request->data['unit']) ? h(trim($this->request->data['unit']))
            : '';
        if (!empty($unit)) {
            array_push($condition,['NothiParts.office_units_id'=>$unit]);
            array_push($condition2,['NothiParts2.office_units_id'=>$unit]);
        }

        $type_name = isset($this->request->data['type_name']) ? h(trim($this->request->data['type_name']))
            : '';
        if (!empty($type_name)) {
            array_push($condition,['NothiTypes.type_name LIKE'=>"%".$type_name."%"]);
        }

        $nothi_no = isset($this->request->data['nothi_no']) ? h(trim($this->request->data['nothi_no']))
            : '';
        if (!empty($nothi_no)) {
            array_push($condition,['NothiParts.nothi_no LIKE'=>"%".$nothi_no."%"]);
            array_push($condition2,['NothiParts2.nothi_no LIKE'=>"%".$nothi_no."%"]);
        }

        $nothi_type_id = isset($this->request->data['nothi_type_id']) ? h(trim($this->request->data['nothi_type_id']))
            : '';
        if (!empty($nothi_type_id)) {
            array_push($condition,['NothiParts.nothi_types_id' => $nothi_type_id]);
            array_push($condition2,['NothiParts2.nothi_types_id' => $nothi_type_id]);
        }
        //pr($condition);die;

        $from_date = isset($this->request->data['from']) ? h(trim($this->request->data['from']))
            : '';
        if (!empty($from_date)) {
            array_push($condition,['date(NothiParts.modified) >='=>$from_date]);
            array_push($condition2,['date(NothiParts2.modified) >='=>$from_date]);

        }

        $end_date = isset($this->request->data['to']) ? h(trim($this->request->data['to']))
            : '';
        if (!empty($end_date)) {
            array_push($condition,['date(NothiParts.modified) <='=>$end_date]);
            array_push($condition2,['date(NothiParts2.modified) <='=>$end_date]);
        }
        $show_only_important = isset($this->request->data['show_only_important']) ? $this->request->data['show_only_important'] : 0;

        if (empty($employee['office_records'])) {
            $json_data = array(
                "draw" => 0,
                "recordsTotal" => 0,
                "recordsFiltered" => 0,
                "data" => []
            );
            $this->response->body(json_encode($json_data));
            $this->response->type('application/json');
            return $this->response;
        }
        $noteTable = TableRegistry::get('NothiNotes');

        if ($this->request->is('ajax')) {
            if ($listType == 'other_sent') {
                $json_data = $this->nothiMasterListForOtherOfficesSent($nothiMasterCurrentUserTable, $this->request->data, $page, $len, $employee_office);
                $this->response->body(json_encode($json_data));
                $this->response->type('application/json');
                return $this->response;
            }
            if ($listType == 'other') {
                $json_data = $this->nothiMasterListForOtherOffices($nothiMasterCurrentUserTable, $employee_office, $nothiMasterPermissionsTable, $condition, [], $page, $len, $this->request->data);
                $this->response->body(json_encode($json_data));
                $this->response->type('application/json');
                return $this->response;
            }

            $data = [];
            $currentNothiMasterArray = array();
            $currentNothiMasterViewStatusArray = array();
            $currentNothiMasterTotalArray = array();

            $totalRec = array();
            $nothiMasterRecord = array();

            $show_only_important = isset($this->request->data['show_only_important']) ? $this->request->data['show_only_important'] : 0;

            $nothi_master_conditions = [];
            $nothi_master_conditions['office_unit_organogram_id'] = $employee_office['office_unit_organogram_id'];
            $nothi_master_conditions['office_unit_id'] = $employee_office['office_unit_id'];
            $nothi_master_conditions['nothi_office'] = $employee_office['office_id'];
            $nothi_master_conditions['is_archive'] = 0;
            //only inbox and other_inbox search option consider joruri params
            if ($listType == 'inbox') {
                if (!empty($show_only_important)) {
                    $nothi_master_conditions['priority'] = $show_only_important;
                }
            }
            $nothiMastersCurrentUser = $nothiMasterCurrentUserTable->find()->select(['nothi_master_id',
                'nothi_office', 'nothi_part_no', 'view_status', 'issue_date', 'priority'])->where($nothi_master_conditions)->order('issue_date desc')->toArray();

            //part 1
            $currentNothiMaster = [];
            $currentNothiIssueDate = array();
            if (!empty($nothiMastersCurrentUser)) {
                $minarray = array();
                $priority = [];
                foreach ($nothiMastersCurrentUser as $key => $currentUser) {
                    $currentNothiMasterArray[] = $currentUser['nothi_part_no'];
                    $minarray[$currentUser['nothi_master_id']][] = $currentUser['view_status'];

                    if (!isset($priority[$currentUser['nothi_master_id']])) {
                        $priority[$currentUser['nothi_master_id']] = 0;
                    } else if ($currentUser['priority'] == 1) {
                        $priority[$currentUser['nothi_master_id']] = $currentUser['priority'];
                    }
                    $currentNothiMasterViewStatusArray[$currentUser['nothi_master_id']]
                        = ($currentUser['view_status'] == 0 ? (!isset($currentNothiMasterViewStatusArray[$currentUser['nothi_master_id']])
                        ? 1 : (++$currentNothiMasterViewStatusArray[$currentUser['nothi_master_id']]))
                        : (!isset($currentNothiMasterViewStatusArray[$currentUser['nothi_master_id']])
                            ? 0 : $currentNothiMasterViewStatusArray[$currentUser['nothi_master_id']]));


                    $firstsubject = $noteTable->getFirstNote($currentUser['nothi_part_no']);
                    $currentNothiMasterTotalArray[$currentUser['nothi_master_id']][$currentUser['nothi_part_no']]
                        = $firstsubject['subject'];

                    $prevmax = $currentUser['view_status'];
                    $currentNothiMaster[]= $currentUser['nothi_part_no'];
                    $currentNothiIssueDate[$currentUser['nothi_master_id']] = (!isset($currentNothiIssueDate[$currentUser['nothi_master_id']])
                    || $currentNothiIssueDate[$currentUser['nothi_master_id']]
                    < $currentUser['issue_date'] ? $currentUser['issue_date']
                        : $currentNothiIssueDate[$currentUser['nothi_master_id']]);
                }
            }

            //part 2
            $nothiCurrentUserCondition = '';

            if ($listType == 'inbox') {
                if (!empty($currentNothiMaster)) {
                    array_push($condition,['NothiParts.id IN'=>$currentNothiMaster]);
                }else{
                    goto rtn;
                }
            } else if ($listType == 'sent') {
                $sent_date_array = [];
                $sent_priority = [];
                $sentNothi = $nothiMasterMovementTable->find()->select(["nothimasterid" => "DISTINCT(nothi_part_no)", 'sent_date' => 'created', 'real_nothi_master_id' => 'nothi_master_id', 'priority'])->where(['from_office_unit_id' => $employee_office['office_unit_id'],
                    'from_officer_designation_id' => $employee_office['office_unit_organogram_id']])->order(['modified desc'])->limit(500)->toArray();
                if (!empty($sentNothi)) {
                    $sentNothiMaster =[];
                    $real_nothi_master_id_list = [];
                    foreach ($sentNothi as $ke => $sentNothiId) {
                        $sentNothiMaster[]= $sentNothiId['nothimasterid'];
                        if(!in_array($sentNothiId['real_nothi_master_id'],$real_nothi_master_id_list)){
                            $real_nothi_master_id_list[]=$sentNothiId['real_nothi_master_id'];
                        }
                        if (!empty($sent_date_array[$sentNothiId['real_nothi_master_id']]) && $sent_date_array[$sentNothiId['real_nothi_master_id']] < $sentNothiId['sent_date']) {
                            $sent_date_array[$sentNothiId['real_nothi_master_id']] = $sentNothiId['sent_date'];
                        } else if (empty($sent_date_array[$sentNothiId['real_nothi_master_id']])) {
                            $sent_date_array[$sentNothiId['real_nothi_master_id']] = $sentNothiId['sent_date'];
                        }
                        if (!empty($sentNothiId['priority']) && empty($sent_priority[$sentNothiId['real_nothi_master_id']])) {
                            $sent_priority[$sentNothiId['real_nothi_master_id']] = $sentNothiId['priority'];
                        } elseif (empty($sent_priority[$sentNothiId['real_nothi_master_id']])) {
                            $sent_priority[$sentNothiId['real_nothi_master_id']] = 0;
                        }
                    }
                    array_push($condition,['NothiParts.id IN'=>$sentNothiMaster]);
                    array_push($condition2,['NothiParts2.id IN'=>$sentNothiMaster]);

                    // No need to check if sent nothi current user is me.
//                    if (!empty($currentNothiMaster)) {
//                        $condition .= 'AND NothiParts.id NOT IN (' . substr($currentNothiMaster,
//                                0, -1) . ')';
//                    }
                }
                else{
                    goto rtn;
                }
            } else if ($listType == 'office') {
                array_push($condition,['NothiParts.office_id'=>$employee_office['office_id']]);
                array_push($condition2,['NothiParts2.office_id'=>$employee_office['office_id']]);

            } else if ($listType == 'other' || $listType == 'other_sent') {

            } else {
                $nothiMastersPermitted = $nothiMasterPermissionsTable->getMasterNothiList($employee_office['office_id'],
                    $employee_office['office_id'],
                    $employee_office['office_unit_id'],
                    $employee_office['office_unit_organogram_id'],
                    $employee_office['office_id']);
                $nothiMastersId = [];
                if (!empty($nothiMastersPermitted)) {
                    foreach ($nothiMastersPermitted as $nothiKey => $nothiId) {
                        $nothiMastersId[]= $nothiId['nothi_masters_id'];
                    }
                    array_push($condition,['NothiParts.nothi_masters_id IN'=>$nothiMastersId]);
                    array_push($condition2,['NothiParts2.nothi_masters_id IN'=>$nothiMastersId]);
                }
            }

            if ($listType == 'inbox') {
                $totalRec = $nothiPartsTable->getAll($condition, $page,
                    $len)->join([
                    "NothiNotes" => [
                        'table' => 'nothi_notes', 'type' => 'inner',
                        'conditions' => ['NothiParts.id =NothiNotes.nothi_part_no']
                    ]
                ])->group(['NothiParts.nothi_masters_id'])->order(['NothiParts.modified desc'])->count();


                $nothiMasterRecord = $nothiPartsTable->getAll($condition, $page,
                    $len)->join([
                    "NothiNotes" => [
                        'table' => 'nothi_notes', 'type' => 'inner',
                        'conditions' => ['NothiParts.id =NothiNotes.nothi_part_no']
                    ]
                ])
                    ->group(['NothiParts.nothi_masters_id'])->order(['NothiParts.modified desc'])->toArray();
            } else if ($listType == 'other') {
                $response = $this->nothiMasterListForOtherOffices($nothiMasterCurrentUserTable, $employee_office, $nothiMasterPermissionsTable, $condition, $nothiMasterRecord, $page, $len, $this->request->data);
                if (!empty($response)) {
                    foreach ($response as $res_k => $res_v) {
                        $$res_k = $res_v;
                    }
                }
            } else {
                $need_order = 1;
                if($listType == 'sent'){
                    $need_order = 0;
                }
                $nothiMasterRecordQuery = $nothiPartsTable->getAll($condition, $page,
                    $len,$need_order)->join([
                    "NothiParts2" => [
                        'className' => 'NothiParts',
                        'table' => 'nothi_parts', 'type' => 'left',
                        'conditions' => ['NothiParts.nothi_masters_id'=>'NothiParts2.nothi_masters_id', 'NothiParts2.modified >'=> 'NothiParts.modified'] + $condition2
                    ]
                ])->where(['NothiParts2.id is null'])->group(['NothiParts.nothi_masters_id']);

                $totalRec = $nothiPartsTable->getAll($condition)->join([
                    "NothiParts2" => [
                        'className' => 'NothiParts',
                        'table' => 'nothi_parts', 'type' => 'left',
                        'conditions' => ['NothiParts.nothi_masters_id'=>'NothiParts2.nothi_masters_id', 'NothiParts2.modified >' => 'NothiParts.modified'] + $condition2
                    ]
                ])->where(['NothiParts2.id is null'])->group(['NothiParts.nothi_masters_id'])->count();
                // for sent list need to filter by sent time
                if($listType == 'sent' && !empty($real_nothi_master_id_list)){
                    $sorted_sentNothiMaster_string = implode(',',$real_nothi_master_id_list);
                    $nothiMasterRecord = $nothiMasterRecordQuery->order('IF(FIELD(NothiParts.nothi_masters_id,'.$sorted_sentNothiMaster_string.')=0,1,0),FIELD(NothiParts.nothi_masters_id,'.$sorted_sentNothiMaster_string.')')->toArray();

                }else{
                    $nothiMasterRecord = $nothiMasterRecordQuery->toArray();
                }
            }

            $table_office_unit = TableRegistry::get('OfficeUnits');

            if (!empty($nothiMasterRecord)) {
                $i = 0;
                $nothiClass = json_decode(NOTHI_CLASS, true);
                $last_office_id = 0;
                $key = -1;
//                pr($nothiMasterRecord);
                foreach ($nothiMasterRecord as $keyRecord => $record) {

                    if ($show_only_important == 1) {
//                        if ($listType == 'sent' && isset($sent_priority[$record['nothi_masters_id']]) && $sent_priority[$record['nothi_masters_id']] != 1) {
//                            unset($sent_priority[$record['nothi_masters_id']]);
//                            $totalRec--;
//                            continue;
//                        }
                        if ($listType == 'inbox' && isset($priority[$record['nothi_masters_id']]) && $priority[$record['nothi_masters_id']] != 1) {
                            unset($priority[$record['nothi_masters_id']]);
                            $totalRec--;
                            continue;
                        }
                    }
                    $key++;
                    if (empty($last_office_id) || ($record['office_id'] != $employee_office['office_id'] && $last_office_id != $record['office_id'])) {
                        $last_office_id = $record['office_id'];
                        try {
                            $this->switchOffice($record['office_id'], 'otherOffices');
                            TableRegistry::remove('NothiParts');
                            $nothiPartsTable = TableRegistry::get('NothiParts');
                        } catch (\Exception $ex) {

                        }
                    }
                    if($listType == 'sent'){
                        $lastNote = $nothiMasterMovementTable->getLastNoteByMovement($record['nothi_masters_id'],$employee_office['office_unit_organogram_id']);
                    }else{
                        $lastNote = $nothiPartsTable->lastNote($record['nothi_masters_id']);
                    }

//                    pr($lastNote);die;
                    // add cache so that no extra query required
                    if (($unit = Cache::read('getNameWithOffice_' . $record['office_units_id'], 'memcached')) === false) {
                        $unit = $table_office_unit->getNameWithOffice($record['office_units_id']);
                        Cache::write('getNameWithOffice_' . $record['office_units_id'], $unit, 'memcached');
                    }
                    // add cache so that no extra query required

                    $getDraftDetails = 0;
                    $draftSummary = 0;
                    $summaryAttachmentRecord = 0;
                    $hasSummary = 0;
                    if ($listType != 'other') {
                        $getDraftDetails = $PotrojariReceiverTable->find()->where(['receiving_office_id' => $employee_office['office_id'],
                            'potro_status' => 'SummaryDraft', 'nothi_master_id' => $record['nothi_masters_id']])->count();

                        $draftSummary = $potrojariTable->find()->where(['potro_status' => 'SummaryDraft',
                            'nothi_master_id' => $record['nothi_masters_id'],
                            'is_summary_nothi' => 1])->count();

                        $summaryAttachmentRecord = $potroAttachmentTable->find()->where(['nothi_master_id' => $record['nothi_masters_id'],
                            'is_summary_nothi' => 1])->count();

                        $hasSummary = ($getDraftDetails >= 1 ? 1 : ($draftSummary
                        >= 1 ? 1 : ($summaryAttachmentRecord >= 1 ? 1 : 0)));
                    }
//end
                    if($listType == "sent" && !empty($lastNote)){
                        $employeeInfo = $nothiMasterCurrentUserTable->getCurrentUserDetails($lastNote['nothi_masters_id'],
                            $lastNote['id'], $lastNote['office_id']);
                    }else{
                        $employeeInfo = $nothiMasterCurrentUserTable->getCurrentUserDetails($record['nothi_masters_id'],
                            $record['id'], $record['office_id']);
                    }

                    $i++;
                    $si = (($page - 1) * $len) + $i;

                    if ($listType == 'inbox') {
                        $time = isset($currentNothiIssueDate[$record['nothi_masters_id']])
                            ? (new Time($currentNothiIssueDate[$record['nothi_masters_id']]))
                            : (new Time());
                        $dattime = $currentNothiIssueDate[$record['nothi_masters_id']]
                            = $time->format('Y-m-d H:i:s');

                        $datedif = $this->dateDiff($dattime);
                    } else if ($listType == 'other') {
                        if (!empty($othercurrentNothiIssueDate[$record['office_id']][$record['nothi_masters_id']])) {
                            $time = new Time($othercurrentNothiIssueDate[$record['office_id']][$record['nothi_masters_id']]);
                            $dattime = $othercurrentNothiIssueDate[$record['office_id']][$record['nothi_masters_id']] = $time->format('Y-m-d H:i:s');
                            $datedif = $this->dateDiff($dattime);
                        } else if (!empty($lastNote['created'])) {
                            $time = new Time($lastNote['created']);
                            $datedif = $this->dateDiff($time->format('Y-m-d H:i:s'));
                        } else {
                            $time = new Time(date('Y-m-d H:i:s'));
                            $datedif = $this->dateDiff($time->format('Y-m-d H:i:s'));
                        }
                    }
                    $crTime = ($lastNote['nothi_created_date'] != '0000-00-00' || $record['nothi_created_date']
                        != '') ? New Time($lastNote['nothi_created_date']) : New Time($lastNote['modified']);
                    // it's time to make data array
//                    if($listType == 'sent'){
                    $data[] = [];
                    array_push($data[$key], "<div class='text-center'>" . Number::format($si) . "</div>");

                    array_push($data[$key], ("<div class='text-left'>" . h($record['nothi_no']) .
                        (isset($currentNothiMasterTotalArray[$record['nothi_masters_id']])
                        && count($currentNothiMasterTotalArray[$record['nothi_masters_id']])
                        > 1 ? (' <label class="badge badge-primary" data-title="' . (implode('&#10;',
                                array_values($currentNothiMasterTotalArray[$record['nothi_masters_id']]))) . '">' . Number::format(count($currentNothiMasterTotalArray[$record['nothi_masters_id']])) . '</label> ')
                            : '') . "</div>"));

                    array_push($data[$key], "<div class='text-left'>" . h($record['subject']) . "</div>");

                    if ($listType == 'sent') {
                        if (empty($sent_date_array[$record['nothi_masters_id']])) {
                            $sent_date_array[$record['nothi_masters_id']] = new Time(date('Y-m-d H:i:s'));
                        } else {
                            $sent_date_array[$record['nothi_masters_id']] = new Time($sent_date_array[$record['nothi_masters_id']]);
                        }
                        array_push($data[$key], "<div class='text-center'>" .
                            $sent_date_array[$record['nothi_masters_id']]->i18nFormat(null, null, 'bn-BD') . "</div>");
                    }

                    array_push($data[$key], ($listType == 'inbox' || $listType == 'other' ?
                        ("<div class='text-center'>" . $crTime->i18nFormat("dd-MM-YYYY") .
                            '<p class="text-danger">(' . $datedif . ')</p>' . "</div>")
                        : "<div class='text-left'>" . (isset($employeeInfo)
                            ? $employeeInfo : '') . "</div>"));

                    $unit_name = '';
                    if ($listType == 'sent') {
                        $unit_name = '<div class="text-center">'.h($unit['unit_name_bng']).'</div>';
                    } else {

                        $unit_name= '<div class="text-center">'.h($unit['unit_name_bng']) . ', ' . h($unit['office_name_bng']).'</div>';

                    }

                    array_push($data[$key], $unit_name."<div class='text-center hidden'>" .
                        '<a title="বিস্তারিত"  data-title="বিস্তারিত" href="' . (($listType
                            != 'other') ? (isset($currentNothiMasterViewStatusArray[$record['nothi_masters_id']])
                            ? ($this->request->webroot . 'noteDetail/' . ((isset($lastNote) && $listType == 'sent')?
                                    $lastNote['id']:$record['id']))
                            : ($this->request->webroot . ($listType != 'sent' ? 'nothiDetail/' : 'noteDetail/') . ((isset($lastNote) && $listType == 'sent')?$lastNote['id']:$record['id']) ))
                            : (isset($othercurrentNothiMasterViewStatusArray[$record['office_id']][$record['nothi_masters_id']])
                                ? ($this->request->webroot . 'noteDetail/' . $record['id'] . '/' . $record['office_id'])
                                : ($this->request->webroot . 'nothiDetail/' . $record['id'] . '/' . $record['office_id']))) .
                        '" class="btn btn-success btn-xs dbl_click" ><i class=" a2i_gn_details2"></i></a>' . "</div>");
                    if ($listType == 'inbox') {
                        array_push($data[$key],
                            "<div class='text-center'><input type='checkbox' class='checkarchive' data-nothimasterid='{$record['nothi_masters_id']}' /></div>");
                    }
                    if ($listType == 'sent') {
                        $data[$key]['DT_RowClass'] = (isset($sent_priority[$record['nothi_masters_id']]) &&
                            $sent_priority[$record['nothi_masters_id']] == 1) ? 'nothiImportant' : '';
                    } else {
                        if ($listType == 'inbox' && isset($priority[$record['nothi_masters_id']]) &&
                            $priority[$record['nothi_masters_id']] == 1) {
                            $data[$key]['DT_RowClass'] = 'nothiImportant';
                        } else {
                            $data[$key]['DT_RowClass'] = (((in_array($record['id'],
                                    $currentNothiMasterArray) && isset($currentNothiMasterViewStatusArray[$record['nothi_masters_id']])
                                && $currentNothiMasterViewStatusArray[$record['nothi_masters_id']]
                                > 0) ? ($hasSummary ? 'summaryList' : 'new') : ($hasSummary
                                ? 'summaryList' : '')));
                        }

                    }
                }
            }

            $json_data = array(
                "draw" => intval($this->request->data['draw']),
                "recordsTotal" => $totalRec,
                "recordsFiltered" => $totalRec,
                "data" => $data
            );
            $this->response->body(json_encode($json_data));
            $this->response->type('application/json');
            return $this->response;
        }
        rtn:
            $json_data = array(
                "draw" => intval($this->request->data['draw']),
                "recordsTotal" => 0,
                "recordsFiltered" => 0,
                "data" => []
            );
            $this->response->body(json_encode($json_data));
            $this->response->type('application/json');
            return $this->response;
    }

    private function nothiMasterListForOtherOfficesSentBk($nothiMasterCurrentUserTable, $post_data, $page, $len, $employee_office = [], $api = 0)
    {
        $condition = [];
        $data = [];
        $subject = isset($post_data['subject']) ? trim($post_data['subject'])
            : '';
        if (!empty($subject)) {
            $condition[] = "subject LIKE '%{$subject}%'";
        }
        $nothi_no = isset($post_data['nothi_no']) ? trim($post_data['nothi_no'])
            : '';
        if (!empty($nothi_no)) {
            $condition[] = "nothi_no LIKE '%{$nothi_no}%'";
        }
        $from_date = isset($post_data['from']) ? trim($post_data['from'])
            : '';
        if (!empty($from_date)) {
            $condition[] = "date(created) >= '{$from_date}'";
        }
        $end_date = isset($post_data['to']) ? trim($post_data['to'])
            : '';
        if (!empty($end_date)) {
            $condition[] = "date(created) <= '{$end_date}'";
        }
        if (empty($employee_office)) {
            $employee_office = $this->getCurrentDakSection();
        }
        if (!empty($employee_office)) {
            $condition['from_officer_designation_id'] = $employee_office['office_unit_organogram_id'];
        }
        $otherOfficeNothiMasterMovementsTable = TableRegistry::get('OtherOfficeNothiMasterMovements');
        $allSentOtherOfficeNothi = $otherOfficeNothiMasterMovementsTable->find()->where($condition)->order(['id desc'])->page($page, $len);
        $totalRec = $otherOfficeNothiMasterMovementsTable->find()->where($condition)->count();
        if (!empty($allSentOtherOfficeNothi)) {
            $i = 0;
            foreach ($allSentOtherOfficeNothi as $val) {
                $i++;
                $si = (($page - 1) * $len) + $i;
                $employeeInfo = $nothiMasterCurrentUserTable->getCurrentUserDetails($val['nothi_master_id'],
                    $val['nothi_part_no'], $val['nothi_office'], $api);
                if ($api == 1) {
                    $data[] = array(
                        Number::format($i),//sl no.
                        h($val['nothi_no']),//nothi no
                        h($val['subject']),// nothi subject
                        $val['created']->i18nFormat(null, null, 'bn-BD'),// sent date
                        $employeeInfo,// current user
                        h($val['nothi_shakha']),//nothi originate unit name
                        $val['nothi_part_no'] . '/' . $val['nothi_office'],//link to note
                    );
                } else {
                    $data[] = array(
                        //1st column
                        "<div class='text-center'>" . Number::format($si) . "</div>",
                        //2nd column
                        "<div class='text-left'>" . h($val['nothi_no']) . "</div>",
                        //3rd column
                        "<div class='text-left'>" . h($val['subject']) . "</div>",
                        //4th column
                        "<div class='text-left'>" . $val['created']->i18nFormat(null, null, 'bn-BD') . "</div>",
                        //5th column
                        $employeeInfo,
                        // 6th column
                        "<div class='text-center'>" .
                        h($val['nothi_shakha']).'</div>',
                        // 7th column
                        "<div class='text-center'>" .
                        '<a title="বিস্তারিত"  data-title="বিস্তারিত" href="' . $this->request->webroot . 'nothiDetail/' . $val['nothi_part_no'] . '/' . $val['nothi_office'] . '" class="btn btn-success btn-xs dbl_click" ><i class=" a2i_gn_details2"></i></a>' . "</div>",
                        "DT_RowClass" => ''
                    );
                }
            }
        }
        $json_data = array(
            "draw" => isset($this->request->data['draw']) ? intval($this->request->data['draw'])
                : 1,
            "recordsTotal" => $totalRec,
            "recordsFiltered" => $totalRec,
            "data" => $data
        );
        return $json_data;

    }

    private function nothiMasterListForOtherOfficesSent($nothiMasterCurrentUserTable, $post_data, $page, $len, $employee_office = [], $api = 0)
    {
        $condition = [];
        $data = [];
        $subject = isset($post_data['subject']) ? trim($post_data['subject'])
            : '';
        if (!empty($subject)) {
            array_push($condition,['subject LIKE'=>"%".$subject."%"]);
        }
        $nothi_no = isset($post_data['nothi_no']) ? trim($post_data['nothi_no'])
            : '';
        if (!empty($nothi_no)) {
            array_push($condition,['nothi_no LIKE'=>"%".$nothi_no."%"]);
        }
        $from_date = isset($post_data['from']) ? trim($post_data['from'])
            : '';
        if (!empty($from_date)) {
            array_push($condition,['date(created) >= '=>$from_date]);
        }
        $end_date = isset($post_data['to']) ? trim($post_data['to'])
            : '';
        if (!empty($end_date)) {
            array_push($condition,['date(created) <= '=>$end_date]);
        }
        if (empty($employee_office)) {
            $employee_office = $this->getCurrentDakSection();
        }
        if (!empty($employee_office)) {
            array_push($condition,['from_officer_designation_id'=>$employee_office['office_unit_organogram_id']]);
        }
        $otherOfficeNothiMasterMovementsTable = TableRegistry::get('OtherOfficeNothiMasterMovements');
        $allSentOtherOfficeNothi = $otherOfficeNothiMasterMovementsTable->find()->where($condition)->order(['id desc'])->page($page, $len);
        $totalRec = $otherOfficeNothiMasterMovementsTable->find()->where($condition)->count();
        if (!empty($allSentOtherOfficeNothi)) {
            $i = 0;
            foreach ($allSentOtherOfficeNothi as $val) {
                $i++;
                $si = (($page - 1) * $len) + $i;
                $employeeInfo = $nothiMasterCurrentUserTable->getCurrentUserDetails($val['nothi_master_id'],
                    $val['nothi_part_no'], $val['nothi_office'], $api);
                if ($api == 1) {
                    $data[] = array(
                        Number::format($i),//sl no.
                        h($val['nothi_no']),//nothi no
                        h($val['subject']),// nothi subject
                        $val['created']->i18nFormat(null, null, 'bn-BD'),// sent date
                        $employeeInfo,// current user
                        h($val['nothi_shakha']),//nothi originate unit name
                        $val['nothi_part_no'] . '/' . $val['nothi_office'],//link to note
                    );
                } else {
                    $data[] = array(
                        //1st column
                        "<div class='text-center'>" . Number::format($si) . "</div>",
                        //2nd column
                        "<div class='text-left'>" . h($val['nothi_no']) . "</div>",
                        //3rd column
                        "<div class='text-left'>" . h($val['subject']) . "</div>",
                        //4th column
                        "<div class='text-left'>" . $val['created']->i18nFormat(null, null, 'bn-BD') . "</div>",
                        //5th column
                        $employeeInfo,
                        // 6th column

                        "<div class='text-center'>".
                        h($val['nothi_shakha'])."</div>" .

                        // 7th column
                        "<div class='text-center hidden'>" .
                        '<a title="বিস্তারিত"  data-title="বিস্তারিত" href="' . $this->request->webroot . 'nothiDetail/' . $val['nothi_part_no'] . '/' . $val['nothi_office'] . '" class="btn btn-success btn-xs dbl_click" ><i class=" a2i_gn_details2"></i></a>' . "</div>",
                        "DT_RowClass" => ''
                    );
                }
            }
        }
        $json_data = array(
            "status" => "success",
            "draw" => isset($this->request->data['draw']) ? intval($this->request->data['draw'])
                : 1,
            "recordsTotal" => $totalRec,
            "recordsFiltered" => $totalRec,
            "data" => $data
        );
        return $json_data;

    }

    private function nothiMasterListForOtherOfficesBk($nothiMasterCurrentUserTable, $employee_office, $nothiMasterPermissionsTable, $condition, $nothiMasterRecord, $page, $len, $post_data, $api = 0)
    {
        //part 1
        $show_only_important = (isset($this->request->data['show_only_important']) ? $this->request->data['show_only_important'] : (isset($this->request->query['show_only_important']) ? $this->request->query['show_only_important'] : 0));
        $nothi_master_conditions['office_unit_organogram_id'] = $employee_office['office_unit_organogram_id'];
        $nothi_master_conditions['office_unit_id'] = $employee_office['office_unit_id'];
        $nothi_master_conditions['nothi_office <>'] = $employee_office['office_id'];
        $nothi_master_conditions['is_archive'] = 0;
        if (!empty($show_only_important)) {
            $nothi_master_conditions['priority'] = $show_only_important;
        }

        $othernothiMastersCurrentUser = $nothiMasterCurrentUserTable->find()->select(['nothi_master_id',
            'nothi_office', 'nothi_part_no', 'view_status', 'issue_date', 'priority'])->where($nothi_master_conditions)->order('issue_date desc')->page($page, $len);
        //part 1
        //part 2
        $othercurrentNothiMasterTotalArray = array();
        $othercurrentNothiMasterViewStatusArray = array();
        $othercurrentNothiMaster = $otherprevmax = '';
        $othercurrentNothiIssueDate = $othercurrentNothiMasterArray = array();
        if (!empty($othernothiMastersCurrentUser)) {
            $otherminarray = array();
            $priority = [];
            foreach ($othernothiMastersCurrentUser as $key => $othercurrentUser) {
                $crnt_nothi_office = $othercurrentUser['nothi_office'];
                $crnt_noth_master_id = $othercurrentUser['nothi_master_id'];

                if (empty($priority[$crnt_nothi_office][$crnt_noth_master_id]) || ($priority[$crnt_nothi_office][$crnt_noth_master_id] == 0)) {
                    $priority[$crnt_nothi_office][$crnt_noth_master_id] = $othercurrentUser['priority'];
                }

                $othercurrentNothiMasterArray[$crnt_nothi_office][] = $othercurrentUser['nothi_part_no'];
                $otherminarray[$crnt_nothi_office][$crnt_noth_master_id][] = $othercurrentUser['view_status'];
                $othercurrentNothiMasterViewStatusArray[$crnt_nothi_office][$crnt_noth_master_id]
                    = ($othercurrentUser['view_status'] == 0 ? (!isset($othercurrentNothiMasterViewStatusArray[$crnt_nothi_office][$crnt_noth_master_id])
                    ? 1 : (++$othercurrentNothiMasterViewStatusArray[$crnt_nothi_office][$crnt_noth_master_id]))
                    : (!isset($othercurrentNothiMasterViewStatusArray[$othercurrentUser[$crnt_nothi_office]['nothi_master_id']])
                        ? 0 : $othercurrentNothiMasterViewStatusArray[$crnt_nothi_office][$crnt_noth_master_id]));
                $othercurrentNothiMasterTotalArray[$crnt_nothi_office][$crnt_noth_master_id]
                    = (!isset($othercurrentNothiMasterTotalArray[$crnt_nothi_office][$crnt_noth_master_id])
                    ? 1 : (++$othercurrentNothiMasterTotalArray[$crnt_nothi_office][$crnt_noth_master_id]));

                $otherprevmax = $othercurrentUser['view_status'];
                $othercurrentNothiMaster[$crnt_nothi_office][] = $othercurrentUser['nothi_part_no'];
                $othercurrentNothiIssueDate[$crnt_nothi_office][$crnt_noth_master_id]
                    = (!isset($othercurrentNothiIssueDate[$crnt_nothi_office][$crnt_noth_master_id])
                || $othercurrentNothiIssueDate[$crnt_nothi_office][$crnt_noth_master_id]
                < $othercurrentUser['issue_date'] ? $othercurrentUser['issue_date'] : $othercurrentNothiIssueDate[$crnt_nothi_office][$crnt_noth_master_id]);
            }
        }
        //part 2
        if (!empty($condition)) {
            $condition .= " AND ";
        }
        if (!empty($othercurrentNothiMaster)) {
            $last_office_id = 0;
            foreach ($othercurrentNothiMaster as $key => $otherOfficeNothi) {

                $conditions = '';
                if (empty($last_office_id) || $last_office_id != $key) {
                    $last_office_id = $key;
                    $this->switchOffice($key, 'OtherOffice');
                }
                TableRegistry::remove('NothiParts');
                $nothiPartsTable = TableRegistry::get('NothiParts');

                $conditions = $condition . ' NothiParts.id IN (' . implode(',', $otherOfficeNothi) . ') ';

                $nothiMasterRecord = array_merge($nothiMasterRecord,
                    $nothiPartsTable->getAll($conditions)->group(['NothiParts.nothi_masters_id'])->order(['NothiParts.modified desc'])->toArray());
            }
        }
        $totalRec = $nothiMasterCurrentUserTable->find()->where($nothi_master_conditions)->count();
        $data = [];
        if (!empty($nothiMasterRecord)) {
            $i = $hasSummary = 0;
            $last_office_id = $employee_office['office_id'];
            foreach ($nothiMasterRecord as $record) {
                if (empty($last_office_id) || $last_office_id != $record['office_id']) {
                    try {
                        $last_office_id = $record['office_id'];
                        $this->switchOffice($record['office_id'], 'otherOffices');
                        TableRegistry::remove('NothiParts');
                        $nothiPartsTable = TableRegistry::get('NothiParts');
                    } catch (\Exception $ex) {
                        continue;
                    }
                }

                $lastNote = $nothiPartsTable->lastNote($record['nothi_masters_id']);
                $unit = TableRegistry::get('OfficeUnits')->getNameWithOffice($record['office_units_id']);
                $crTime = ($lastNote['nothi_created_date'] != '0000-00-00' || $record['nothi_created_date']
                    != '') ? New Time($lastNote['nothi_created_date']) : New Time($lastNote['modified']);

                if (!empty($othercurrentNothiIssueDate[$record['office_id']][$record['nothi_masters_id']])) {
                    $time = new Time($othercurrentNothiIssueDate[$record['office_id']][$record['nothi_masters_id']]);
                    $dattime = $othercurrentNothiIssueDate[$record['office_id']][$record['nothi_masters_id']] = $time->format('Y-m-d H:i:s');
                    $datedif = $this->dateDiff($dattime);
                } else if (!empty($lastNote['created'])) {
                    $time = new Time($lastNote['created']);
                    $datedif = $this->dateDiff($time->format('Y-m-d H:i:s'));
                } else {
                    $time = new Time(date('Y-m-d H:i:s'));
                    $datedif = $this->dateDiff($time->format('Y-m-d H:i:s'));
                }

                $i++;
                $si = (($page - 1) * $len) + $i;
                if (empty($priority[$record['office_id']][$record['nothi_masters_id']])) {
                    $priority[$record['office_id']][$record['nothi_masters_id']] = 0;
                }
                if ($api == 1) {
                    $data[] = array(
                        Number::format($i),//sl no.
                        h($record['nothi_no']),//nothi no
                        (isset($othercurrentNothiMasterTotalArray[$record['office_id']][$record['nothi_masters_id']])
                        && $othercurrentNothiMasterTotalArray[$record['office_id']][$record['nothi_masters_id']]
                        > 1 ? (Number::format($othercurrentNothiMasterTotalArray[$record['office_id']][$record['nothi_masters_id']]))
                            : 0),// how many notes are in this note
                        h($record['subject']),// nothi subject
                        $crTime->i18nFormat("dd-MM-YYYY"),// inbox date
                        h($unit['unit_name_bng']) . ', ' . h($unit['office_name_bng']),// note origin unit and office info
                        $record['id'] . '/' . $record['office_id'],//link to note
                        ((in_array($record['id'],
                                $othercurrentNothiMasterArray[$record['office_id']]) && isset($othercurrentNothiMasterViewStatusArray[$record['office_id']][$record['nothi_masters_id']])
                            && $othercurrentNothiMasterViewStatusArray[$record['office_id']][$record['nothi_masters_id']]
                            > 0) ? 1 : 0)//new or old
                    );
                } else {
                    $data[] = array(
                        //1st column
                        "<div class='text-center'>" . Number::format($si) . "</div>",
                        //2nd column
                        ("<div class='text-left'>" . h($record['nothi_no']) . (isset($othercurrentNothiMasterTotalArray[$record['office_id']][$record['nothi_masters_id']])
                            && $othercurrentNothiMasterTotalArray[$record['office_id']][$record['nothi_masters_id']]
                            > 1 ? (' <label class="badge badge-primary">' . Number::format($othercurrentNothiMasterTotalArray[$record['office_id']][$record['nothi_masters_id']]) . '</label> ')
                                : '') . "</div>"),
                        //3rd column
                        //                        $record['NothiTypes']['type_name'],
                        "<div class='text-left'>" . h($record['subject']) . "</div>",
                        //4th column
                        ("<div class='text-center'>" . $crTime->i18nFormat("dd-MM-YYYY") .
                            '<p class="text-danger">(' . $datedif . ')</p>' . "</div>"),
                        //5th column
                        //
                        // 6th column
                        "<div class='text-center'>" .
                        h($unit['unit_name_bng']) . ', ' . h($unit['office_name_bng']).'</div>',
                        "<div class='text-center'>" .
                        '<a title="বিস্তারিত"  data-title="বিস্তারিত" href="' . ((isset($othercurrentNothiMasterViewStatusArray[$record['office_id']][$record['nothi_masters_id']])
                            ? ($this->request->webroot . 'noteDetail/' . $record['id'] . '/' . $record['office_id'])
                            : ($this->request->webroot . 'nothiDetail/' . $record['id'] . '/' . $record['office_id']))) . '" class="btn btn-success btn-xs dbl_click" ><i class=" a2i_gn_details2"></i></a>' . "</div>",
                        "DT_RowClass" => ((in_array($record['id'],
                                $othercurrentNothiMasterArray[$record['office_id']]) && isset($othercurrentNothiMasterViewStatusArray[$record['office_id']][$record['nothi_masters_id']])
                            && $othercurrentNothiMasterViewStatusArray[$record['office_id']][$record['nothi_masters_id']]
                            > 0) ? ($hasSummary ? 'summaryList' : 'new') : ($hasSummary ? 'summaryList' : ($priority[$record['office_id']][$record['nothi_masters_id']] == 1) ? 'nothiImportant' : ''))
                    );
                }
            }
        }
        $json_data = array(
            "draw" => isset($this->request->data['draw']) ? intval($this->request->data['draw'])
                : 1,
            "recordsTotal" => $totalRec,
            "recordsFiltered" => $totalRec,
            "data" => $data
        );
        return $json_data;
    }

    private function nothiMasterListForOtherOffices($nothiMasterCurrentUserTable, $employee_office, $nothiMasterPermissionsTable, $condition, $nothiMasterRecord, $page, $len, $post_data, $api = 0)
    {
        //part 1
        $show_only_important = (isset($this->request->data['show_only_important']) ? $this->request->data['show_only_important'] : (isset($this->request->query['show_only_important']) ? $this->request->query['show_only_important'] : 0));

        $nothi_master_conditions['office_unit_organogram_id'] = $employee_office['office_unit_organogram_id'];
        $nothi_master_conditions['office_unit_id'] = $employee_office['office_unit_id'];
        $nothi_master_conditions['nothi_office <>'] = $employee_office['office_id'];
        $nothi_master_conditions['is_archive'] = 0;
        if (!empty($show_only_important)) {
            $nothi_master_conditions['priority'] = $show_only_important;
        }

        $othernothiMastersCurrentUser = $nothiMasterCurrentUserTable->find()->select(['nothi_master_id',
            'nothi_office', 'nothi_part_no', 'view_status', 'issue_date', 'priority'])->where($nothi_master_conditions)->order('issue_date desc')->page($page, $len);
        //part 1
        //part 2
        $othercurrentNothiMasterTotalArray = array();
        $othercurrentNothiMasterViewStatusArray = array();
        $othercurrentNothiMaster = $otherprevmax = '';
        $othercurrentNothiIssueDate = $othercurrentNothiMasterArray = array();
        if (!empty($othernothiMastersCurrentUser)) {
            $otherminarray = array();
            $priority = [];
            foreach ($othernothiMastersCurrentUser as $key => $othercurrentUser) {
                $crnt_nothi_office = $othercurrentUser['nothi_office'];
                $crnt_noth_master_id = $othercurrentUser['nothi_master_id'];

                if (empty($priority[$crnt_nothi_office][$crnt_noth_master_id]) || ($priority[$crnt_nothi_office][$crnt_noth_master_id] == 0)) {
                    $priority[$crnt_nothi_office][$crnt_noth_master_id] = $othercurrentUser['priority'];
                }

                $othercurrentNothiMasterArray[$crnt_nothi_office][] = $othercurrentUser['nothi_part_no'];
                $otherminarray[$crnt_nothi_office][$crnt_noth_master_id][] = $othercurrentUser['view_status'];
                $othercurrentNothiMasterViewStatusArray[$crnt_nothi_office][$crnt_noth_master_id]
                    = ($othercurrentUser['view_status'] == 0 ? (!isset($othercurrentNothiMasterViewStatusArray[$crnt_nothi_office][$crnt_noth_master_id])
                    ? 1 : (++$othercurrentNothiMasterViewStatusArray[$crnt_nothi_office][$crnt_noth_master_id]))
                    : (!isset($othercurrentNothiMasterViewStatusArray[$othercurrentUser[$crnt_nothi_office]['nothi_master_id']])
                        ? 0 : $othercurrentNothiMasterViewStatusArray[$crnt_nothi_office][$crnt_noth_master_id]));
                $othercurrentNothiMasterTotalArray[$crnt_nothi_office][$crnt_noth_master_id]
                    = (!isset($othercurrentNothiMasterTotalArray[$crnt_nothi_office][$crnt_noth_master_id])
                    ? 1 : (++$othercurrentNothiMasterTotalArray[$crnt_nothi_office][$crnt_noth_master_id]));

                $otherprevmax = $othercurrentUser['view_status'];
                $othercurrentNothiMaster[$crnt_nothi_office][] = $othercurrentUser['nothi_part_no'];
                $othercurrentNothiIssueDate[$crnt_nothi_office][$crnt_noth_master_id]
                    = (!isset($othercurrentNothiIssueDate[$crnt_nothi_office][$crnt_noth_master_id])
                || $othercurrentNothiIssueDate[$crnt_nothi_office][$crnt_noth_master_id]
                < $othercurrentUser['issue_date'] ? $othercurrentUser['issue_date'] : $othercurrentNothiIssueDate[$crnt_nothi_office][$crnt_noth_master_id]);
            }
        }
        //part 2

        if (!empty($othercurrentNothiMaster)) {
            $last_office_id = 0;
            foreach ($othercurrentNothiMaster as $key => $otherOfficeNothi) {

                $conditions = [];
                if (empty($last_office_id) || $last_office_id != $key) {
                    $last_office_id = $key;
                    $this->switchOffice($key, 'OtherOffice');
                }
                TableRegistry::remove('NothiParts');
                $nothiPartsTable = TableRegistry::get('NothiParts');

                array_push($conditions,$condition);
                array_push($conditions,['NothiParts.id IN'=>$otherOfficeNothi]);

                $nothiMasterRecord = array_merge($nothiMasterRecord,
                    $nothiPartsTable->getAll($conditions)->group(['NothiParts.nothi_masters_id'])->order(['NothiParts.modified desc'])->toArray());
            }
        }
        $totalRec = $nothiMasterCurrentUserTable->find()->where($nothi_master_conditions)->count();
        $data = [];
        if (!empty($nothiMasterRecord)) {
            $i = $hasSummary = 0;
            $last_office_id = $employee_office['office_id'];
            foreach ($nothiMasterRecord as $record) {
                if (empty($last_office_id) || $last_office_id != $record['office_id']) {
                    try {
                        $last_office_id = $record['office_id'];
                        $this->switchOffice($record['office_id'], 'otherOffices');
                        TableRegistry::remove('NothiParts');
                        $nothiPartsTable = TableRegistry::get('NothiParts');
                    } catch (\Exception $ex) {
                        continue;
                    }
                }

                $lastNote = $nothiPartsTable->lastNote($record['nothi_masters_id']);
                $unit = TableRegistry::get('OfficeUnits')->getNameWithOffice($record['office_units_id']);
                $crTime = ($lastNote['nothi_created_date'] != '0000-00-00' || $record['nothi_created_date']
                    != '') ? New Time($lastNote['nothi_created_date']) : New Time($lastNote['modified']);

                if (!empty($othercurrentNothiIssueDate[$record['office_id']][$record['nothi_masters_id']])) {
                    $time = new Time($othercurrentNothiIssueDate[$record['office_id']][$record['nothi_masters_id']]);
                    $dattime = $othercurrentNothiIssueDate[$record['office_id']][$record['nothi_masters_id']] = $time->format('Y-m-d H:i:s');
                    $datedif = $this->dateDiff($dattime);
                } else if (!empty($lastNote['created'])) {
                    $time = new Time($lastNote['created']);
                    $datedif = $this->dateDiff($time->format('Y-m-d H:i:s'));
                } else {
                    $time = new Time(date('Y-m-d H:i:s'));
                    $datedif = $this->dateDiff($time->format('Y-m-d H:i:s'));
                }

                $i++;
                $si = (($page - 1) * $len) + $i;
                if (empty($priority[$record['office_id']][$record['nothi_masters_id']])) {
                    $priority[$record['office_id']][$record['nothi_masters_id']] = 0;
                }
                if ($api == 1) {
                    $data[] = array(
                        Number::format($i),//sl no.
                        h($record['nothi_no']),//nothi no
                        (isset($othercurrentNothiMasterTotalArray[$record['office_id']][$record['nothi_masters_id']])
                        && $othercurrentNothiMasterTotalArray[$record['office_id']][$record['nothi_masters_id']]
                        > 1 ? (Number::format($othercurrentNothiMasterTotalArray[$record['office_id']][$record['nothi_masters_id']]))
                            : 0),// how many notes are in this note
                        h($record['subject']),// nothi subject
                        $crTime->i18nFormat("dd-MM-YYYY"),// inbox date
                        h($unit['unit_name_bng']) . ', ' . h($unit['office_name_bng']),// note origin unit and office info
                        $record['id'] . '/' . $record['office_id'],//link to note
                        ((in_array($record['id'],
                                $othercurrentNothiMasterArray[$record['office_id']]) && isset($othercurrentNothiMasterViewStatusArray[$record['office_id']][$record['nothi_masters_id']])
                            && $othercurrentNothiMasterViewStatusArray[$record['office_id']][$record['nothi_masters_id']]
                            > 0) ? 1 : 0)//new or old
                    );
                } else {
                    $data[] = array(
                        //1st column
                        "<div class='text-center'>" . Number::format($si) . "</div>",
                        //2nd column
                        ("<div class='text-left'>" . h($record['nothi_no']) . (isset($othercurrentNothiMasterTotalArray[$record['office_id']][$record['nothi_masters_id']])
                            && $othercurrentNothiMasterTotalArray[$record['office_id']][$record['nothi_masters_id']]
                            > 1 ? (' <label class="badge badge-primary">' . Number::format($othercurrentNothiMasterTotalArray[$record['office_id']][$record['nothi_masters_id']]) . '</label> ')
                                : '') . "</div>"),
                        //3rd column
                        //                        $record['NothiTypes']['type_name'],
                        "<div class='text-left'>" . h($record['subject']) . "</div>",
                        //4th column
                        ("<div class='text-center'>" . $crTime->i18nFormat("dd-MM-YYYY") . '<p class="text-danger">(' . $datedif . ')</p>' . "</div>"),
                        //5th column
                        //
                        // 6th column

                        "<div class='text-center'>" .
                        h($unit['unit_name_bng']) . ', ' . h($unit['office_name_bng']).'</div>' .
                        "<div class='text-center hidden'>" .

                        '<a title="বিস্তারিত"  data-title="বিস্তারিত" href="' . ((isset($othercurrentNothiMasterViewStatusArray[$record['office_id']][$record['nothi_masters_id']])
                            ? ($this->request->webroot . 'noteDetail/' . $record['id'] . '/' . $record['office_id'])
                            : ($this->request->webroot . 'nothiDetail/' . $record['id'] . '/' . $record['office_id']))) . '" class="btn btn-success btn-xs dbl_click" ><i class=" a2i_gn_details2"></i></a>' . "</div>",
                        "DT_RowClass" => ((in_array($record['id'],
                                $othercurrentNothiMasterArray[$record['office_id']]) && isset($othercurrentNothiMasterViewStatusArray[$record['office_id']][$record['nothi_masters_id']])
                            && $othercurrentNothiMasterViewStatusArray[$record['office_id']][$record['nothi_masters_id']]
                            > 0) ? ($hasSummary ? 'summaryList' : 'new') : ($hasSummary ? 'summaryList' : ($priority[$record['office_id']][$record['nothi_masters_id']] == 1) ? 'nothiImportant' : ''))
                    );
                }
            }
        }
        $json_data = array(
            "status" => "success",
            "draw" => isset($this->request->data['draw']) ? intval($this->request->data['draw'])
                : 1,
            "recordsTotal" => $totalRec,
            "recordsFiltered" => $totalRec,
            "data" => $data
        );
        return $json_data;
    }

    public function UpdateOtherOfficePermissions() {
        $requested_data = $this->request->data;
        $response = [
            'status' => 'success',
            'msg' => 'সংরক্ষিত করা হচ্ছে',
        ];
        if (!empty($requested_data)) {
            if (!empty($requested_data['token'])) {
                $dycrepted_data = unserialize($this->getDecryptedData(base64_decode($requested_data['token'])));
                if ($dycrepted_data['nothi_office'] == $requested_data['nothi_office'] && $dycrepted_data['nothi_part_no'] == $requested_data['nothi_part_no']) {

                    $permissions = json_decode($requested_data['permissions'], true);
                    $current_users = json_decode($requested_data['current_users'], true);

                    $curl_office = $this->switchOfficeWithStatus($requested_data['target_office_id'], 'curl_office', -1, true);
                    if ($curl_office['status'] == 'success') {
                        if (!empty($current_users)) {
                            $designation_info = designationInfo($current_users['office_unit_organogram_id']);
                            /// current user update
                            TableRegistry::remove('NothiMasterCurrentUsers');
                            $nothiMasterCurrentUsersTable = TableRegistry::get('NothiMasterCurrentUsers');
                            $hasDataInCurrentUser = $nothiMasterCurrentUsersTable->find()->where([
                                    'nothi_part_no' => $requested_data['nothi_part_no'],
                                    'nothi_office' => $requested_data['nothi_office'],
                                    'nothi_master_id'=>$requested_data['nothi_master_id']
                                ])->count();
                            if (!isset($requested_data['issue_date'])) {
                                $requested_data['issue_date'] = date('Y-m-d H:i:s');
                            }
                            if (!isset($requested_data['forward_date'])) {
                                $requested_data['forward_date'] = date('Y-m-d H:i:s');
                            }
                            if (!isset($requested_data['is_archive'])) {
                                $requested_data['is_archive'] = 0;
                            }
                            if (!isset($requested_data['priority'])) {
                                $requested_data['priority'] = 0;
                            }
                            if (!isset($requested_data['is_summary'])) {
                                $requested_data['is_summary'] = 0;
                            }
                            if ($hasDataInCurrentUser > 0) {
                                $nothiMasterCurrentUsersTable->updateAll(
                                    [
                                        'is_archive' => 0,
                                        'is_finished' => 0,
                                        'office_id' => $designation_info['office_id'],
                                        'office_unit_id' => $designation_info['office_unit_id'],
                                        'office_unit_organogram_id' => $designation_info['office_unit_organogram_id'],
                                        'employee_id' => $designation_info['officer_id'],
                                        'view_status' => 0,
                                        'issue_date' => $requested_data['issue_date'],
                                        'forward_date' => $requested_data['forward_date'],
                                        'is_new' => 0,
                                        'priority' => $requested_data['priority'],
                                    ],
                                    [
                                        'nothi_part_no' => $requested_data['nothi_part_no'],
                                        'nothi_office' => $requested_data['nothi_office'],
                                        'nothi_master_id'=>$requested_data['nothi_master_id']
                                    ]);
                            } else {
                                $nothiMasterCurrentUsersEntity = $nothiMasterCurrentUsersTable->newEntity();
                                $nothiMasterCurrentUsersEntity->nothi_master_id = $requested_data['nothi_master_id'];
                                $nothiMasterCurrentUsersEntity->nothi_part_no = $requested_data['nothi_part_no'];
                                $nothiMasterCurrentUsersEntity->office_id = $current_users['office_id'];
                                $nothiMasterCurrentUsersEntity->office_unit_id = $current_users['office_unit_id'];
                                $nothiMasterCurrentUsersEntity->office_unit_organogram_id = $current_users['office_unit_organogram_id'];
                                $nothiMasterCurrentUsersEntity->employee_id = $designation_info['officer_id'];
                                $nothiMasterCurrentUsersEntity->issue_date = date("Y-m-d H:i:s");
                                $nothiMasterCurrentUsersEntity->forward_date = $requested_data['forward_date'];
                                $nothiMasterCurrentUsersEntity->is_new = 1;
                                $nothiMasterCurrentUsersEntity->nothi_office = $requested_data['nothi_office'];
                                $nothiMasterCurrentUsersEntity->is_archive = $requested_data['is_archive'];
                                $nothiMasterCurrentUsersEntity->is_finished = 0;
                                $nothiMasterCurrentUsersEntity->priority = $requested_data['priority'];
                                $nothiMasterCurrentUsersEntity->is_summary = $requested_data['is_summary'];
                                $nothiMasterCurrentUsersEntity->view_status =0;
                                $nothiMasterCurrentUsersRecord = $nothiMasterCurrentUsersTable->save($nothiMasterCurrentUsersEntity);
                            }
                        }

                        /// permission user update
                        TableRegistry::remove('NothiMasterPermissions');
                        $nothiPriviligeTable = TableRegistry::get('NothiMasterPermissions');
                        $nothiPriviligeTable->deleteAll(['nothi_masters_id' => $requested_data['nothi_master_id'], 'nothi_part_no' => $requested_data['nothi_part_no'], 'nothi_office' => $requested_data['nothi_office']]);
                        foreach ($permissions as $key => $permission) {
                            if ($permission['perm_value'] == 0) continue;
                            if (!isset($permission['ofc_id'])) continue;
                            $optionsRadios = array();

                            $nothiPriviligeRecord = $nothiPriviligeTable->newEntity();

                            $optionsRadios['nothi_masters_id'] = $requested_data['nothi_master_id'];
                            $optionsRadios['nothi_part_no'] = $requested_data['nothi_part_no'];
                            $optionsRadios['office_id'] = $permission['ofc_id'];
                            $optionsRadios['office_unit_id'] = $permission['unit_id'];
                            $optionsRadios['office_unit_organograms_id'] = $permission['org_id'];
                            $optionsRadios['designation_level'] = $this->BngToEng($permission['designation_level']);
                            $optionsRadios['privilige_type'] = intval($permission['perm_value']);

                            if (isset($permission['visited'])) {
                                $optionsRadios['visited'] = $permission['visited'];
                            }
                            /*if ($current_users['office_unit_organogram_id'] == $permission['org_id']
                                && $current_users['office_unit_id'] == $permission['unit_id']
                                && $current_users['office_id'] == $permission['ofc_id']
                            ) {
                                $optionsRadios['visited'] = 1;
                            }*/

                            $optionsRadios['nothi_office'] = $requested_data['nothi_office'];
                            $optionsRadios['created'] = date("Y-m-d H:i:s");
                            $optionsRadios['created_by'] = ($designation_info['officer_id']) ? $designation_info['officer_id'] : 1;

                            $otherOfficePriv[] = $optionsRadios;

                            $nothiPriviligeRecord = $nothiPriviligeTable->patchEntity($nothiPriviligeRecord,
                                $optionsRadios, ['validate' => 'add']);

                            $priviligeError = $nothiPriviligeRecord->errors();

                            if (!$priviligeError) {
                                $nothiPriviligeTable->save($nothiPriviligeRecord);
                            } else {
                                $response = [
                                    'status' => 'error',
                                    'msg' => 'সংরক্ষিত করা সম্ভব হচ্ছে না। কোডঃ ১',
                                    'reason' => makeEncryptedData(json_encode($priviligeError))
                                ];
                            }
                        }
                    } else {
                        $response = $curl_office;
                    }
                } else {
                    $response = [
                        'status' => 'error',
                        'msg' => 'Invalid request : Token missmatch',
                        'reason' => ''
                    ];
                }
            } else {
                $response = [
                    'status' => 'error',
                    'msg' => 'Token missing',
                    'reason' => ''
                ];
            }
        } else {
            $response = [
                'status' => 'error',
                'msg' => 'No request data',
                'reason' => ''
            ];
        }

        $this->response->body(json_encode($response));
        $this->response->type('application/json');
        return $this->response;
    }

    public function add($id = 0, $title = "add", $subject = '', $nothijato = 0)
    {
        set_time_limit('0');
        $type_id = $this->request->query('type_id');
        if(!empty($type_id))
        {
            $this->set('type_id',$type_id);
        }
        $this->set('nothijato', $nothijato);
        $this->set('dak_subject', $subject);
        if ($this->request->is('ajax')) {
            $this->layout = null;

            $this->set('ajaxcall', true);
        }
        // sometimed permitted user office not able to connect
        $saved_but_has_some_prroblems = '';

        // IF this set to true, user can not edit 1st part.
        $this->set('nothi_no_1st_part_edit', true);
        $office_id = $this->getCurrentDakSection()['office_id'];
        $offices_table = TableRegistry::get('Offices');
        $office = $offices_table->get($office_id);
        $office_layers_table = TableRegistry::get('OfficeLayers');
        $office_layer = $office_layers_table->get($office->office_layer_id);
        //if ($office_layer->layer_level > 0 && $office_layer->layer_level < 5) {
        if ($office_id == 2102) {
            $this->set('nothi_no_1st_part_edit', false);
        }
        $this->set(compact('title'));
        $msgTitle = "সংরক্ষিত";
        $nothiTypesTable = TableRegistry::get("NothiTypes");

        $employee_office = $this->getCurrentDakSection();

        $nothiTypes = $nothiTypesTable->getTypes(0, $employee_office);

        $this->set(compact('nothiTypes', 'employee_office'));

        $officeId = 0;

        if (!empty($employee_office['office_id'])) {
            $officeId = $employee_office['office_id'];
            $officeUnitId = $employee_office['office_unit_id'];
            $officeUnitOrganogramId = $employee_office['office_unit_organogram_id'];
        }

        $officeUnitsTable = TableRegistry::get('OfficeUnits');

        $unitList = $officeUnitsTable->getOfficeUnitsList($officeId);
        $unitInfo = $officeUnitsTable->getUnitsInfoByOfficeId($officeId);
        $unitInfo = $officeUnitsTable->getBanglaName($officeUnitId)->unit_name_bng;

        $this->set(compact('unitList', 'officeId','unitInfo'));

        $nothiMastersTable = TableRegistry::get('NothiMasters');
        TableRegistry::remove('NothiMasterCurrentUsers');
        $nothiMasterCurrentUsersTable = TableRegistry::get('NothiMasterCurrentUsers');
        $employeeOfficeTable = TableRegistry::get('EmployeeOffices');
        TableRegistry::remove('NothiMasterPermissions');
        $nothiPriviligeTable = TableRegistry::get('NothiMasterPermissions');
        $nothiPartsTable = TableRegistry::get('NothiParts');
        $nothiNotesTable = TableRegistry::get('NothiNotes');

        $nothiMastersRecords = $nothiMastersTable->newEntity();

        $nothiMastersRecords->office_units_id = $officeUnitId;

        if ($this->request->is('ajax')) {
            if (!empty($this->request->data)) {

                $formData = array();
                $formPartNothiData = $nothiPartsTable->newEntity();
                if (!empty($this->request->data['formData'])) {
                    foreach ($this->request->data['formData'] as $ke => $data) {
                        if (isset($data['name'])) {
                            $formData[$data['name']] = $data['value'];
                            if ($data['name'] != 'id') {
                                $formPartNothiData[$data['name']] = $data['value'];
                            }
                        }
                    }
                } elseif ($this->request->data['formPart']) {

                    foreach ($this->request->data['formPart'] as $ke => $data) {
                        if (isset($ke)) {
                            $formData[$ke] = $data;
                            if ($ke != 'id') {
                                $formPartNothiData[$ke] = $data;
                            }
                        }
                    }
                }

                $nothiMastersRecords = $nothiMastersTable->newEntity();

                if (!empty($formData['nothi_master_id'])) {
                    $nothiMastersRecords = $nothiMastersTable->get($formData['nothi_master_id']);

                    $lastPart = array();

                    if (!empty($nothiMastersRecords)) {
                        $formData['nothi_types_id'] = $nothiMastersRecords['nothi_types_id'];
                        $formData['nothi_no'] = $nothiMastersRecords['nothi_no'];
                        $formData['nothi_class'] = $nothiMastersRecords['nothi_class'];
                        $formData['subject'] = $nothiMastersRecords['subject'];
                        $formData['id'] = $nothiMastersRecords['id'];
                        $formData['nothi_created_date'] = $nothiMastersRecords['nothi_created_date'];
                    }
                } else {
                    $formData['nothi_no'] = bnToen($formData['nothi_no']);
                    $formData['nothi_no'] = str_replace('*', '', $formData['nothi_no']);
                    if (mb_strlen($formData['nothi_no']) != 24) {
                        echo json_encode(['status' => 'error', 'msg' => 'দুঃখিত! নথি নম্বর সঠিক নয়। নথি নম্বর ১৮ ডিজিটের হতে হবে ']);
                        die;
                    }

                    $formData['nothi_no'] = enTobn($formData['nothi_no']);
                }
                $user_id = $this->Auth->user('id');
                $formData['office_id'] = $officeId;
                $formData['office_units_organogram_id'] = $officeUnitOrganogramId;
                $formData['office_units_id'] = $officeUnitId;

                $formPartNothiData['office_id'] = $officeId;
                $formPartNothiData['office_units_organogram_id'] = $officeUnitOrganogramId;
                $formPartNothiData['office_units_id'] = $officeUnitId;

                $nothiMastersRecords = $nothiMastersTable->patchEntity($nothiMastersRecords,
                    $formData, ['validate' => 'add']);

                $nothiMastersRecords->created = date("Y-m-d H:i:s");
                $nothiMastersRecords->created_by = $user_id;
                $nothiMastersRecords->modified_by = $user_id;
                $errors = $nothiMastersRecords->errors();

                $getCurrentParts = $nothiMasterCurrentUsersTable->getAllPartByDesignation($nothiMastersRecords->id,
                    $officeUnitOrganogramId, $officeUnitId, $officeId);


                if ($nothiNotesTable->checkEmptyNote($nothiMastersRecords->id,
                        array_keys($getCurrentParts)) != count($getCurrentParts)
                ) {
//                    $empty = $nothiPartsTable->getAllNothi(['nothi_masters_id' => $nothiMastersRecords->id, 'id IN' => array_keys($getCurrentParts)])->order(['id desc'])->first();
                    $empty = TableRegistry::get('NothiNotes')->getEmptyNoteInfo(array_keys($getCurrentParts));
                    // check if note is hidden from user. if hidden make it visible
                    if(!empty($empty['id'])){
//                        $url = \Cake\View\Helper\UrlHelper::build(['controller' => 'NothiMasters', 'action' => 'nothiDetailsNew',$empty['id']]);
//                        echo json_encode(['status' => 'error', 'msg' => '<a href="' . $url . '"> দুঃখিত! পূর্বের নোট এ অনুচ্ছেদ দেয়া হয়নি। দয়া করে পূর্বের নোট এ যাওয়ার জন্য ক্লিক করুন।</a>', 'id' => ($empty['id'])]);
                        echo json_encode(['status' => 'success', 'msg' => 'পূর্বের নোট এ নিয়ে যাওয়া হয়েছে।', 'id' => ($empty['id'])]);
                    }else{
                        echo json_encode(['status' => 'error', 'msg' => 'দুঃখিত! পূর্বের নোট এ অনুচ্ছেদ দেয়া হয়নি। ']);
                    }

                    die;
                }

                if (!$errors) {
//                    $conn = ConnectionManager::get('default');
                    try {
//                        $conn->begin();

                        if (empty($formData['nothi_master_id'])) {
                            $nothiMastersRecords = $nothiMastersTable->save($nothiMastersRecords);

                            $formPartNothiData['nothi_types_id'] = $nothiMastersRecords['nothi_types_id'];
                            $formPartNothiData['nothi_no'] = $nothiMastersRecords['nothi_no'];
                            $formPartNothiData['nothi_class'] = $nothiMastersRecords['nothi_class'];
                            $formPartNothiData['subject'] = $nothiMastersRecords['subject'];
                        } else {
                            $formPartNothiData['nothi_types_id'] = $nothiMastersRecords['nothi_types_id'];
                            $formPartNothiData['nothi_no'] = $nothiMastersRecords['nothi_no'];
                            $formPartNothiData['nothi_class'] = $nothiMastersRecords['nothi_class'];
                            $formPartNothiData['subject'] = $nothiMastersRecords['subject'];
                            $formPartNothiData['nothi_created_date'] = date("Y-m-d");
                        }


                        $formPartNothiData['nothi_masters_id'] = !empty($formData['nothi_master_id'])
                            ? $formData['nothi_master_id'] : $nothiMastersRecords->id;

                        $lastPart = $nothiPartsTable->find()->select(['nothi_part_no'])->where(['nothi_masters_id' => $formPartNothiData['nothi_masters_id']])->order(['nothi_part_no desc'])->first();
                        if (!empty($lastPart)) {
                            $formPartNothiData['nothi_part_no'] = $lastPart['nothi_part_no']
                                + 1;

                            $formPartNothiData['nothi_part_no_bn'] = Number::format(($lastPart['nothi_part_no']
                                + 1));
                        } else {
                            $formPartNothiData['nothi_part_no'] = 1;

                            $formPartNothiData['nothi_part_no_bn'] = Number::format(1);
                        }

                        $formPartNothiData->modified_by = $this->Auth->user('id');
                        $formPartNothiData = $nothiPartsTable->save($formPartNothiData);

                        $employeeOfficeRecord = $employeeOfficeTable->getDesignationInfo($nothiMastersRecords->office_units_organogram_id);

                        $permission_offices = [];
                        $multiple_office_has_permission = false;
                        // First note of a nothi with Permission related info
                        if (!empty($this->request->data['priviliges'])) {

                            foreach ($this->request->data['priviliges'] as $k => $v) {
                                if ($v['perm_value'] == 0) continue;
                                if (!isset($v['ofc_id'])) continue;
                                // Setting Office wise permission so that less switch DB needed.
                                if (!in_array($v['ofc_id'], $permission_offices)) {
                                    $permission_offices[] = $v['ofc_id'];
                                }
                            }

                            // previous_code
//                            $nothiPriviligeTable->deleteAll(['nothi_masters_id' => $nothiMastersRecords->id,
//                                'nothi_part_no' => $formPartNothiData->id, 'nothi_office' => $employee_office['office_id']]);

                            $otherOfficePriv = array();
                            $otherofficehas = 0;
                            if (!empty($permission_offices)) {
                                /* If only one permission exist in permission office array and that office is own office */
                                if (count($permission_offices) == 1 && $permission_offices[0] == $employee_office['office_id']) {
                                    $multiple_office_has_permission = true;
                                }
                                foreach ($permission_offices as $k => $v) {
                                    if ($multiple_office_has_permission == false) {
                                        $is_connected = $this->switchOfficeWithStatus($v, 'MainNothiOffice', -1, true);
                                        if (!$is_connected || (!empty($is_connected['status']) && $is_connected['status'] == 'error')) {
                                            if ($v != $formPartNothiData['office_id']) {
                                                $officeDomainsTable = TableRegistry::get('OfficeDomains');
                                                $otherOffice = $officeDomainsTable->find()->where(['office_id' => $v])->first();
                                                $url = $otherOffice->domain_url . "/postUpdateOtherOfficePermissions";

                                                $encrypted_data = $this->makeEncryptedData(serialize([
                                                    'nothi_office' => $formPartNothiData['office_id'],
                                                    'nothi_part_no' => $formPartNothiData['id'],
                                                    'nothi_master_id' => $formPartNothiData['nothi_masters_id']
                                                ]));
                                                $priviliges = $this->request->data['priviliges'];
                                                $data = [
                                                    'token' => base64_encode($encrypted_data),
                                                    'nothi_office' => $formPartNothiData['office_id'],
                                                    'nothi_part_no' => $formPartNothiData['id'],
                                                    'nothi_master_id' => $formPartNothiData['nothi_masters_id'],
                                                    'current_users' => json_encode(['office_id' => $officeId, 'office_unit_id' => $officeUnitId, 'office_unit_organogram_id' => $officeUnitOrganogramId]),
                                                    'permissions' => json_encode($priviliges),
                                                    'target_office_id' => $v
                                                ];

                                                $auth_token_response = curlRequest($url, $data);
                                                if (!empty($auth_token_response)) {
                                                    $auth_token_response = jsonA($auth_token_response);
                                                    if (!empty($auth_token_response['status']) && ($auth_token_response['status']) == 'success') {
                                                        continue;
                                                    }
                                                }

                                                $office_info = TableRegistry::get('Offices')->getBanglaName($v);
                                                if (!empty($office_info['office_name_bng'])) {
                                                    $saved_but_has_some_prroblems .= '<br/> অফিসের  (' . $office_info['office_name_bng'] . ') নির্বাচিত কর্মকর্তাদের অনুমতি দেওয়া সম্ভব হয়নি। কারণঃ ডাটাবেজ পাওয়া যায় নি!';
                                                } else {
                                                    $saved_but_has_some_prroblems .= '<br/> অফিসের (আইডিঃ ' . $v . ') নির্বাচিত কর্মকর্তাদের অনুমতি দেওয়া সম্ভব হয়নি। কারণঃ ডাটাবেজ পাওয়া যায় নি!';
                                                }
                                                continue;
                                            }
                                        }
                                        /*$checkDBConnection = $this->checkCurrentOfficeDBConnection();
                                        if(empty($checkDBConnection) || (!empty($checkDBConnection['status']) && $checkDBConnection['status'] == 'error')){
                                            $office_info = TableRegistry::get('Offices')->getBanglaName($v);
                                            if(!empty($office_info['office_name_bng'])){
                                                $saved_but_has_some_prroblems .= '<br/> অফিসের  ('.$office_info['office_name_bng'].') নির্বাচিত কর্মকর্তাদের অনুমতি দেওয়া সম্ভব হয়নি। কারণঃ ডাটাবেজ পাওয়া যায় নি!';
                                            }else{
                                                $saved_but_has_some_prroblems .= '<br/> অফিসের (আইডিঃ '.$v.') নির্বাচিত কর্মকর্তাদের অনুমতি দেওয়া সম্ভব হয়নি। কারণঃ ডাটাবেজ পাওয়া যায় নি!';
                                            }
                                            continue;
                                        }*/
                                    }

                                    TableRegistry::remove('NothiMasterCurrentUsers');
                                    $nothiMasterCurrentUsersTable = TableRegistry::get('NothiMasterCurrentUsers');
                                    $nothiMasterCurrentUsersEntity = $nothiMasterCurrentUsersTable->newEntity();
                                    $nothiMasterCurrentUsersEntity->nothi_master_id = $formPartNothiData['nothi_masters_id'];
                                    $nothiMasterCurrentUsersEntity->nothi_part_no = $formPartNothiData->id;
                                    $nothiMasterCurrentUsersEntity->office_id = $employeeOfficeRecord->office_id;
                                    $nothiMasterCurrentUsersEntity->office_unit_id = $employeeOfficeRecord->office_unit_id;
                                    $nothiMasterCurrentUsersEntity->office_unit_organogram_id = $employeeOfficeRecord->office_unit_organogram_id;
                                    $nothiMasterCurrentUsersEntity->employee_id = $employeeOfficeRecord->employee_record_id;
                                    $nothiMasterCurrentUsersEntity->issue_date = date("Y-m-d H:i:s");
                                    $nothiMasterCurrentUsersEntity->is_new = 1;
                                    $nothiMasterCurrentUsersEntity->nothi_office = $employee_office['office_id'];

                                    $nothiMasterCurrentUsersRecord = $nothiMasterCurrentUsersTable->save($nothiMasterCurrentUsersEntity);

                                    TableRegistry::remove('NothiMasterPermissions');
                                    $nothiPriviligeTable = TableRegistry::get('NothiMasterPermissions');
                                    $nothiPriviligeTable->deleteAll(['nothi_masters_id' => $nothiMastersRecords->id,
                                        'nothi_part_no' => $formPartNothiData->id, 'nothi_office' => $employee_office['office_id']]);
                                    foreach ($this->request->data['priviliges'] as $key => $value) {
                                        if ($value['perm_value'] == 0) continue;
                                        if (!isset($value['ofc_id'])) continue;
                                        $optionsRadios = array();

                                        $nothiPriviligeRecord = $nothiPriviligeTable->newEntity();

                                        $optionsRadios['nothi_masters_id'] = $nothiMastersRecords->id;
                                        $optionsRadios['nothi_part_no'] = $formPartNothiData->id;
                                        $optionsRadios['office_id'] = $value['ofc_id'];
                                        $optionsRadios['office_unit_id'] = $value['unit_id'];
                                        $optionsRadios['office_unit_organograms_id'] = $value['org_id'];
                                        $optionsRadios['designation_level'] = $this->BngToEng($value['designation_level']);
                                        $optionsRadios['privilige_type'] = intval($value['perm_value']);

                                        /*if ($employeeOfficeRecord->office_unit_organogram_id == $value['org_id']
                                            && $employeeOfficeRecord->office_unit_id == $value['unit_id']
                                            && $employeeOfficeRecord->office_id == $value['ofc_id']
                                        ) {
                                            $optionsRadios['visited'] = 1;
                                        }*/

                                        $optionsRadios['nothi_office'] = $employee_office['office_id'];
                                        $optionsRadios['created'] = date("Y-m-d H:i:s");
                                        $optionsRadios['created_by'] = $user_id;

                                        $otherOfficePriv[] = $optionsRadios;

                                        $nothiPriviligeRecord = $nothiPriviligeTable->patchEntity($nothiPriviligeRecord,
                                            $optionsRadios, ['validate' => 'add']);

                                        $priviligeError = $nothiPriviligeRecord->errors();

                                        if (!$priviligeError) {
                                            $nothiPriviligeTable->save($nothiPriviligeRecord);
                                        } else {
//                                               $conn->rollback();

                                            echo json_encode(['status' => 'error', 'msg' => 'নথি ' . $msgTitle . ' করা সম্ভব হচ্ছে না। কোডঃ ১', 'reason' => makeEncryptedData(json_encode($priviligeError))]);
                                            die;
                                        }
                                    }
                                }
                            }
//                            previous_code
//                            foreach ($this->request->data['priviliges'] as $key => $value) {
//                                if ($value['perm_value'] == 0) continue;
//                                if (!isset($value['ofc_id'])) continue;
//                                $optionsRadios = array();
//
//                                $nothiPriviligeRecord = $nothiPriviligeTable->newEntity();
//
//                                $optionsRadios['nothi_masters_id'] = $nothiMastersRecords->id;
//                                $optionsRadios['nothi_part_no'] = $formPartNothiData->id;
//                                $optionsRadios['office_id'] = $value['ofc_id'];
//                                $optionsRadios['office_unit_id'] = $value['unit_id'];
//                                $optionsRadios['office_unit_organograms_id'] = $value['org_id'];
//                                $optionsRadios['designation_level'] = $this->BngToEng($value['designation_level']);
//                                $optionsRadios['privilige_type'] = intval($value['perm_value']);
//
//                                if ($employeeOfficeRecord->office_unit_organogram_id
//                                    == $value['org_id'] && $employeeOfficeRecord->office_unit_id
//                                    == $value['unit_id'] && $employeeOfficeRecord->office_id
//                                    == $value['ofc_id']
//                                ) {
//                                    $optionsRadios['visited'] = 1;
//                                }
//
//                                $optionsRadios['nothi_office'] = $employee_office['office_id'];
//                                $optionsRadios['created'] = date("Y-m-d H:i:s");
//                                $optionsRadios['created_by'] = $this->Auth->user('id');
//
//                                $otherOfficePriv[] = $optionsRadios;
//
//                                $nothiPriviligeRecord = $nothiPriviligeTable->patchEntity($nothiPriviligeRecord,
//                                    $optionsRadios, ['validate' => 'add']);
//
//                                $priviligeError = $nothiPriviligeRecord->errors();
//
//                                if (!$priviligeError) {
//                                    $nothiPriviligeTable->save($nothiPriviligeRecord);
//                                } else {
//                                    $conn->rollback();
//
//                                    echo json_encode(['status' => 'error', 'msg' => 'নথি ' . $msgTitle . ' করা সম্ভব হচ্ছে না']);
//                                    die;
//                                }
//                            }
                        } /**
                         * New Note Creation does not take any permission data rather use previous notes permission. Ex: New part creation of a nothi.
                         */
                        else {
                            $nothiPriviligeTable->deleteAll(['nothi_masters_id' => $nothiMastersRecords->id,
                                'nothi_part_no' => $formPartNothiData->id, 'nothi_office' => $employee_office['office_id']]);

//                            $nothiParts = $nothiPartsTable->find()->where(['nothi_masters_id' => $nothiMastersRecords->id,
//                                'nothi_part_no' => '1'])->first();

                            /**
                             * In previous we copy permission of part 1 privilege in new note.
                             * But from now we will add all permistted user from all previous notes of that Nothi
                             */
                            $existingPriviliges = $nothiPriviligeTable->find()->where(['nothi_masters_id' => $nothiMastersRecords->id,
                                'nothi_office' => $employee_office['office_id']])->distinct(['office_unit_organograms_id'])->toArray();

                            // Need to check which Office has Permission -Start
                            if (!empty($existingPriviliges)) {
                                foreach ($existingPriviliges as $key => $value) {
                                    if (!in_array($value['office_id'], $permission_offices)) {
                                        $permission_offices[] = $value['office_id'];
                                    }
                                }
                            }
                            // Need to check which Office has Permission - End
                            // Now Office wise Permission entry
                            if (!empty($permission_offices)) {
                                if (count($permission_offices) == 1 && $permission_offices[0] == $employee_office['office_id']) {
                                    $multiple_office_has_permission = true;
                                }
                                foreach ($permission_offices as $k => $v) {
                                    if ($multiple_office_has_permission == false) {
                                        $is_connected =  $this->switchOfficeWithStatus($v, 'MainNothiOffice',-1,true);
                                        if(!$is_connected || (!empty($is_connected['status']) && $is_connected['status'] == 'error')){
                                            $office_info = TableRegistry::get('Offices')->getBanglaName($v);
                                            if(!empty($office_info['office_name_bng'])){
                                                $saved_but_has_some_prroblems .= '<br/> অফিসের  ('.$office_info['office_name_bng'].') নির্বাচিত কর্মকর্তাদের অনুমতি দেওয়া সম্ভব হয়নি। কারণঃ ডাটাবেজ পাওয়া যায় নি!';
                                            }else{
                                                $saved_but_has_some_prroblems .= '<br/> অফিসের (আইডিঃ '.$v.') নির্বাচিত কর্মকর্তাদের অনুমতি দেওয়া সম্ভব হয়নি। কারণঃ ডাটাবেজ পাওয়া যায় নি!';
                                            }
                                            continue;
                                        }
                                        $checkDBConnection = $this->checkCurrentOfficeDBConnection();
                                        if(empty($checkDBConnection) || (!empty($checkDBConnection['status']) && $checkDBConnection['status'] == 'error')){
                                            $office_info = TableRegistry::get('Offices')->getBanglaName($v);
                                            if(!empty($office_info['office_name_bng'])){
                                                $saved_but_has_some_prroblems .= '<br/> অফিসের  ('.$office_info['office_name_bng'].') নির্বাচিত কর্মকর্তাদের অনুমতি দেওয়া সম্ভব হয়নি। কারণঃ ডাটাবেজ পাওয়া যায় নি!';
                                            }else{
                                                $saved_but_has_some_prroblems .= '<br/> অফিসের (আইডিঃ '.$v.') নির্বাচিত কর্মকর্তাদের অনুমতি দেওয়া সম্ভব হয়নি। কারণঃ ডাটাবেজ পাওয়া যায় নি!';
                                            }
                                            continue;
                                        }
                                    }

                                    /*
                             * Current Nothi User, who is set as দায়িত্বপ্রাপ্ত কর্মকর্তা
                             */

                                    TableRegistry::remove('NothiMasterCurrentUsers');
                                    $nothiMasterCurrentUsersTable = TableRegistry::get('NothiMasterCurrentUsers');
                                    $nothiMasterCurrentUsersEntity = $nothiMasterCurrentUsersTable->newEntity();
                                    $nothiMasterCurrentUsersEntity->nothi_master_id = $formPartNothiData['nothi_masters_id'];
                                    $nothiMasterCurrentUsersEntity->nothi_part_no = $formPartNothiData->id;
                                    $nothiMasterCurrentUsersEntity->office_id = $employeeOfficeRecord->office_id;
                                    $nothiMasterCurrentUsersEntity->office_unit_id = $employeeOfficeRecord->office_unit_id;
                                    $nothiMasterCurrentUsersEntity->office_unit_organogram_id = $employeeOfficeRecord->office_unit_organogram_id;
                                    $nothiMasterCurrentUsersEntity->employee_id = $employeeOfficeRecord->employee_record_id;
                                    $nothiMasterCurrentUsersEntity->issue_date = date("Y-m-d H:i:s");
                                    $nothiMasterCurrentUsersEntity->is_new = 1;
                                    $nothiMasterCurrentUsersEntity->nothi_office = $employee_office['office_id'];

                                    $nothiMasterCurrentUsersRecord = $nothiMasterCurrentUsersTable->save($nothiMasterCurrentUsersEntity);

                                    TableRegistry::remove('NothiMasterPermissions');
                                    $nothiPriviligeTable = TableRegistry::get('NothiMasterPermissions');
                                    $otherOfficePriv = [];
                                    $otherofficehas = 0;
                                    $is_current_user_has_acces = 0;
                                    foreach ($existingPriviliges as $key => $value) {

                                        $optionsRadios = array();

                                        $nothiPriviligeRecord = $nothiPriviligeTable->newEntity();

                                        $optionsRadios['nothi_masters_id'] = $nothiMastersRecords->id;
                                        $optionsRadios['nothi_part_no'] = $formPartNothiData->id;
                                        $optionsRadios['office_id'] = $value['office_id'];
                                        $optionsRadios['office_unit_id'] = $value['office_unit_id'];
                                        $optionsRadios['office_unit_organograms_id'] = $value['office_unit_organograms_id'];
                                        $optionsRadios['designation_level'] = $value['designation_level'];
                                        $optionsRadios['privilige_type'] = intval($value['privilige_type']);

                                        if ($employeeOfficeRecord->office_unit_organogram_id == $value['office_unit_organograms_id']
                                            && $employeeOfficeRecord->office_unit_id == $value['office_unit_id']
                                            && $employeeOfficeRecord->office_id == $value['office_id']
                                        ) {
                                            $optionsRadios['visited'] = 1;
                                            $is_current_user_has_acces = 1;
                                        }

                                        $optionsRadios['nothi_office'] = $employee_office['office_id'];

                                        $optionsRadios['created'] = date("Y-m-d H:i:s");
                                        $optionsRadios['created_by'] = $user_id;


                                        $nothiPriviligeRecord = $nothiPriviligeTable->patchEntity($nothiPriviligeRecord,
                                            $optionsRadios,
                                            [
                                                'validate' => 'add'
                                            ]);

                                        $priviligeError = $nothiPriviligeRecord->errors();

                                        if (!$priviligeError) {
                                            $nothiPriviligeTable->save($nothiPriviligeRecord);
                                        } else {
//                                            $conn->rollback();
                                            echo json_encode(['status' => 'error', 'msg' => 'নথি ' . $msgTitle . ' করা সম্ভব হচ্ছে না। কোডঃ ২', 'reason' => makeEncryptedData(json_encode($priviligeError))]);
                                            die;
                                        }
                                    }
                                }
                            }
                            // previous_code
//                            $otherOfficePriv = [];
//                            $otherofficehas = 0;
//                            $is_current_user_has_acces = 0;
//                            foreach ($existingPriviliges as $key => $value) {
//
//                                $optionsRadios = array();
//
//                                $nothiPriviligeRecord = $nothiPriviligeTable->newEntity();
//
//                                $optionsRadios['nothi_masters_id'] = $nothiMastersRecords->id;
//                                $optionsRadios['nothi_part_no'] = $formPartNothiData->id;
//                                $optionsRadios['office_id'] = $value['office_id'];
//                                $optionsRadios['office_unit_id'] = $value['office_unit_id'];
//                                $optionsRadios['office_unit_organograms_id'] = $value['office_unit_organograms_id'];
//                                $optionsRadios['designation_level'] = $value['designation_level'];
//                                $optionsRadios['privilige_type'] = intval($value['privilige_type']);
//
//                                if ($employeeOfficeRecord->office_unit_organogram_id
//                                    == $value['office_unit_organograms_id'] && $employeeOfficeRecord->office_unit_id
//                                    == $value['office_unit_id'] && $employeeOfficeRecord->office_id
//                                    == $value['office_id']
//                                ) {
//                                    $optionsRadios['visited'] = 1;
//                                    $is_current_user_has_acces = 1;
//                                }
//
//                                $optionsRadios['nothi_office'] = $employee_office['office_id'];
//
//                                $optionsRadios['created'] = date("Y-m-d H:i:s");
//                                $optionsRadios['created_by'] = $this->Auth->user('id');
//
//
//                                $nothiPriviligeRecord = $nothiPriviligeTable->patchEntity($nothiPriviligeRecord,
//                                    $optionsRadios,
//                                    [
//                                        'validate' => 'add'
//                                    ]);
//
//                                $priviligeError = $nothiPriviligeRecord->errors();
//
//                                if (!$priviligeError) {
//                                    $nothiPriviligeTable->save($nothiPriviligeRecord);
//                                } else {
//                                    $conn->rollback();
//
//                                    echo json_encode(['status' => 'error', 'msg' => 'নথি ' . $msgTitle . ' করা সম্ভব হচ্ছে না']);
//                                    die;
//                                }
//                            }
                        }
                        //back to own Office
                        if ($multiple_office_has_permission == false) {
                            $this->switchOffice($employee_office['office_id'], 'MainNothiOffice');
                        }
                        /* If current User does not have default permission */
                        if (isset($is_current_user_has_acces) && $is_current_user_has_acces
                            == 0
                        ) {
                            $nothiPriviligeRecord = $nothiPriviligeTable->newEntity();
                            $optionsRadios = array();
                            $optionsRadios['nothi_masters_id'] = $formPartNothiData['nothi_masters_id'];
                            $optionsRadios['nothi_part_no'] = $formPartNothiData->id;
                            $optionsRadios['office_id'] = $employeeOfficeRecord->office_id;
                            $optionsRadios['office_unit_id'] = $employeeOfficeRecord->office_unit_id;
                            $optionsRadios['office_unit_organograms_id'] = $employeeOfficeRecord->office_unit_organogram_id;
                            $optionsRadios['designation_level'] = $this->BngToEng(1);
                            $optionsRadios['privilige_type'] = intval(1);

                            $optionsRadios['visited'] = 1;
                            $is_current_user_has_acces = 1;

                            $nothiPriviligeRecord = $nothiPriviligeTable->patchEntity($nothiPriviligeRecord,
                                $optionsRadios, ['validate' => 'add']);
                            $priviligeError = $nothiPriviligeRecord->errors();

                            if (!$priviligeError) {
                                $nothiPriviligeTable->save($nothiPriviligeRecord);
                            } else {
//                                $conn->rollback();

                                echo json_encode(['status' => 'error', 'msg' => 'নথি ' . $msgTitle . ' করা সম্ভব হচ্ছে না। কোডঃ ৩','res' => 'Current user does not have access']);
                                die;
                            }
                        }


                        /*
                         * Current Nothi User, who is set as দায়িত্বপ্রাপ্ত কর্মকর্তা
                         */

                        //previous_code
//                        $nothiMasterCurrentUsersEntity = $nothiMasterCurrentUsersTable->newEntity();
//                        $nothiMasterCurrentUsersEntity->nothi_master_id = $formPartNothiData['nothi_masters_id'];
//                        $nothiMasterCurrentUsersEntity->nothi_part_no = $formPartNothiData->id;
//                        $nothiMasterCurrentUsersEntity->office_id = $employeeOfficeRecord->office_id;
//                        $nothiMasterCurrentUsersEntity->office_unit_id = $employeeOfficeRecord->office_unit_id;
//                        $nothiMasterCurrentUsersEntity->office_unit_organogram_id
//                            = $employeeOfficeRecord->office_unit_organogram_id;
//                        $nothiMasterCurrentUsersEntity->employee_id = $employeeOfficeRecord->employee_record_id;
//                        $nothiMasterCurrentUsersEntity->issue_date = date("Y-m-d H:i:s");
//                        $nothiMasterCurrentUsersEntity->is_new = 1;
//                        $nothiMasterCurrentUsersEntity->nothi_office = $employee_office['office_id'];
//
//                        $nothiMasterCurrentUsersRecord = $nothiMasterCurrentUsersTable->save($nothiMasterCurrentUsersEntity);


                        /*
                         * End
                         */
//                        $conn->commit();
                        $session = $this->request->session();
                        $session->write('refer_url', 'all');
                        TableRegistry::get('NoteInitialize')->saveData(['nothi_masters_id' => $formPartNothiData['nothi_masters_id'], 'nothi_part_no' => $formPartNothiData['id'], 'office_id' => $employee_office['office_id'], 'office_units_id' => $employee_office['office_unit_id'], 'office_units_organogram_id' => $employee_office['office_unit_organogram_id'], 'user_id' => $this->Auth->user('id')]);

                        // check if this request is for new nothi or new note(nothi_part) create.
                        if(!empty($this->request->data['formPart']['nothi_master_id'])){
                            $res_msg = 'নোট ' . $msgTitle . ' হয়েছে। ';
                        }else{
                            $res_msg = 'নথি ' . $msgTitle . ' হয়েছে। সকল নথি তালিকায় নথিটি অবস্থান করছে। ';
                        }
                        if(!empty($saved_but_has_some_prroblems)){
                            echo json_encode(['status' => 'success', 'msg' => $res_msg,'extra_message'=> $res_msg . 'কার্যক্রমটি করার সময় কিছু সমস্যা হয়েছে যা নিম্নে বর্ণিত হলঃ <br>'.$saved_but_has_some_prroblems,
                                              'id' => $formPartNothiData->id]);
                        }else{
                            echo json_encode(['status' => 'success', 'msg' => $res_msg,
                                              'id' => $formPartNothiData->id]);
                        }

                        die;
                    } catch (\Exception $er) {
//                        $conn->rollback();
                        echo json_encode(['status' => 'error', 'msg' => 'নথি ' . $msgTitle . ' করা সম্ভব হচ্ছে নির্বাচিত। কোডঃ ৪','res' => makeEncryptedData($er->getMessage())]);
                        die;
                    }
                } else {
                    echo json_encode(['status' => 'error', 'msg' => 'নথি ' . $msgTitle . ' করা সম্ভব হচ্ছে না। কোডঃ ৫', 'data' => $errors,'res' => makeEncryptedData(json_encode($errors))]);
                    die;
                }
            }
        }
        $this->set(compact('nothiMastersRecords'));
    }

    public function nothiMasterPermissionList($id = 0)
    {
        $employee_office = $this->getCurrentDakSection();
        $this->layout = 'ajax';
        $this->set('id', $id);
        $this->set('nothi_office', $employee_office['office_id']);
        $this->set('nothi_unit', $employee_office['office_unit_id']);
    }

    public function nothiMasterPermissionEditList($id = 0, $nothi_office = 0)
    {
        /*
         *  used for permission edit list generation.
         * 1. NothiOwnUnitPermissionList::updatePermission
         * 2. NothiOtherOfficePermissionList::display
         */

        $this->layout = null;
        $this->set('id', $id);
        $this->set('nothi_office', $nothi_office);
    }

    public function nothiMasterPermissionUpdate($id, $nothi_office = 0)
    {

        if (isset($this->request->data['api'])) {
            $employee_office = $this->request->data['employee_office'];
            $this->switchOffice($nothi_office, 'MainNothiOffice');
        } else {
            $employee_office = $this->getCurrentDakSection();
        }
        if (empty($employee_office)) {
            throw new \Exception('Invalid request');
        }

        $otherNothi = false;
        if ($nothi_office != 0 && $nothi_office != $employee_office['office_id']) {
            $otherNothi = true;

            $this->switchOffice($nothi_office, 'MainNothiOffice');
        } else {
            $nothi_office = $employee_office['office_id'];
        }

        $this->set('nothi_office', $nothi_office);
        $this->set(compact('otherNothi'));

        TableRegistry::remove('NothiParts');
        TableRegistry::remove('NothiMasterPermissions');
        $nothiMastersTable = TableRegistry::get('NothiParts');
        $nothiPriviligeTable = TableRegistry::get('NothiMasterPermissions');

        $saved_but_has_some_prroblems = '';
        if (!empty($id)) {

            try {

//                $conn = ConnectionManager::get('default');
//                $conn->begin();
                $nothiPartInformation = $nothiMastersTable->get($id);
                if (isset($this->request->data['api'])) {
                    $user_id = $employee_office['user_id'];
                } else {
                    $user_id = $this->Auth->user('id');
                }
                if (!empty($this->request->data['priviliges'])) {
                    $permission_offices = [$nothi_office];
                    //get current office ID from DB
                    $currentPermittedOffices = $nothiPriviligeTable->permittedOfficesId($id, $nothi_office);
                    if (!empty($currentPermittedOffices)) {
                        foreach ($currentPermittedOffices as $cur_per_office_id) {
                            if (!in_array($cur_per_office_id, $permission_offices)) {
                                $permission_offices[] = $cur_per_office_id;
                            }
                        }
                    }
                    foreach ($this->request->data['priviliges'] as $k => $v) {
                        if ($v['perm_value'] == 0) continue;
                        if (!isset($v['ofc_id'])) continue;
                        // Setting Office wise permission so that less switch DB needed.
                        if (!in_array($v['ofc_id'], $permission_offices)) {
                            $permission_offices[] = $v['ofc_id'];
                        }
                    }
                    $multiple_offices_permission = false;
                    if (!empty($permission_offices)) {
                        if (count($permission_offices) == 1 && current($permission_offices) == $employee_office['office_id']) {
                            $multiple_offices_permission = true;
                        }
                        TableRegistry::remove('NothiMasterCurrentUsers');
                        $nothiMasterCurrentUsersTableofOtherOffice = TableRegistry::get('NothiMasterCurrentUsers');
                        $nothiMasterCurrentUser = $nothiMasterCurrentUsersTableofOtherOffice->find()->where(
                            [
                                'nothi_part_no' => $nothiPartInformation['id'],
                                'nothi_office' => $nothi_office,
                            ]
                        )->first();
                        foreach ($permission_offices as $k => $v) {
                            if ($multiple_offices_permission == false) {
                                $is_connected =  $this->switchOfficeWithStatus($v, 'MainNothiOffice',-1,true);
                                if (!$is_connected || (!empty($is_connected['status']) && $is_connected['status'] == 'error')) {
                                    if ($v != $employee_office['office_id']) {
                                        $officeDomainsTable = TableRegistry::get('OfficeDomains');
                                        $otherOffice = $officeDomainsTable->find()->where(['office_id' => $v])->first();
                                        $url = $otherOffice->domain_url . "/postUpdateOtherOfficePermissions";

                                        $encrypted_data = $this->makeEncryptedData(serialize([
                                            'nothi_office' => $nothi_office,
                                            'nothi_part_no' => $nothiPartInformation['id'],
                                            'nothi_master_id' => $nothiPartInformation['nothi_masters_id']
                                        ]));
                                        $priviliges = $this->request->data['priviliges'];
                                        $data = [
                                            'token' => base64_encode($encrypted_data),
                                            'nothi_office' => $nothi_office,
                                            'nothi_part_no' => $nothiPartInformation['id'],
                                            'nothi_master_id' => $nothiPartInformation['nothi_masters_id'],
                                            'current_users' => json_encode(['office_id' => $nothiMasterCurrentUser['office_id'], 'office_unit_id' => $nothiMasterCurrentUser['office_unit_id'], 'office_unit_organogram_id' => $nothiMasterCurrentUser['office_unit_organogram_id']]),
                                            'permissions' => json_encode($priviliges),
                                            'target_office_id' => $v
                                        ];

                                        $auth_token_response = curlRequest($url, $data);
                                        if (!empty($auth_token_response)) {
                                            $auth_token_response = jsonA($auth_token_response);
                                            if (!empty($auth_token_response['status']) && ($auth_token_response['status']) == 'success') {
                                                continue;
                                            }
                                        }

                                        $office_info = TableRegistry::get('Offices')->getBanglaName($v);
                                        if (!empty($office_info['office_name_bng'])) {
                                            $saved_but_has_some_prroblems .= '<br/> অফিসের  (' . $office_info['office_name_bng'] . ') নির্বাচিত কর্মকর্তাদের অনুমতি দেওয়া সম্ভব হয়নি। কারণঃ ডাটাবেজ পাওয়া যায় নি!';
                                        } else {
                                            $saved_but_has_some_prroblems .= '<br/> অফিসের (আইডিঃ ' . $v . ') নির্বাচিত কর্মকর্তাদের অনুমতি দেওয়া সম্ভব হয়নি। কারণঃ ডাটাবেজ পাওয়া যায় নি!';
                                        }
                                        continue;
                                    }
                                }
                                /*$checkDBConnection = $this->checkCurrentOfficeDBConnection();
                                if(empty($checkDBConnection) || (!empty($checkDBConnection['status']) && $checkDBConnection['status'] == 'error')){
                                    $office_info = TableRegistry::get('Offices')->getBanglaName($v);
                                    if(!empty($office_info['office_name_bng'])){
                                        $saved_but_has_some_prroblems .= '<br/> অফিসের  ('.$office_info['office_name_bng'].') নির্বাচিত কর্মকর্তাদের অনুমতি দেওয়া সম্ভব হয়নি। কারণঃ ডাটাবেজ পাওয়া যায় নি!';
                                    }else{
                                        $saved_but_has_some_prroblems .= '<br/> অফিসের (আইডিঃ '.$v.') নির্বাচিত কর্মকর্তাদের অনুমতি দেওয়া সম্ভব হয়নি। কারণঃ ডাটাবেজ পাওয়া যায় নি!';
                                    }
                                    continue;
                                }*/
                            }

                            TableRegistry::remove('NothiMasterPermissions');
                            $nothiPriviligeTable = TableRegistry::get('NothiMasterPermissions');
                            $nothiPriviligeTable->deleteAll(['nothi_masters_id' => $nothiPartInformation['nothi_masters_id'],
                                'nothi_part_no' => $nothiPartInformation['id'], 'nothi_office' => $nothi_office]);
                            foreach ($this->request->data['priviliges'] as $key => $value) {
                                if ($value['perm_value'] == 0) continue;
                                if (!isset($value['ofc_id'])) continue;

                                $optionsRadios = array();

                                $nothiPriviligeRecord = $nothiPriviligeTable->newEntity();

                                $optionsRadios['nothi_masters_id'] = $nothiPartInformation['nothi_masters_id'];
                                $optionsRadios['nothi_part_no'] = $id;
                                $optionsRadios['office_id'] = $value['ofc_id'];
                                $optionsRadios['office_unit_id'] = $value['unit_id'];
                                $optionsRadios['office_unit_organograms_id'] = $value['org_id'];
                                $optionsRadios['designation_level'] = $this->BngToEng($value['designation_level']);
                                $optionsRadios['privilige_type'] = intval($value['perm_value']);
                                $optionsRadios['nothi_office'] = $nothi_office;

                                if ($employee_office['office_unit_organogram_id'] == $value['org_id']
                                    && $employee_office['office_unit_id'] == $value['unit_id'] && $employee_office['office_id']
                                    == $value['ofc_id']
                                ) {
                                    $optionsRadios['visited'] = 1;
                                }

                                $optionsRadios['created'] = date("Y-m-d H:i:s");
                                $optionsRadios['created_by'] = $user_id;

                                $nothiPriviligeRecord = $nothiPriviligeTable->patchEntity($nothiPriviligeRecord,
                                    $optionsRadios,
                                    [
                                        'validate' => 'add'
                                    ]);

                                $nothiPriviligeTable->save($nothiPriviligeRecord);
                            }
                        }
                    }
                }
//                $conn->commit();

                if(!empty($saved_but_has_some_prroblems)){
                    echo json_encode(['status' => 'success', 'msg' => 'নথি অনুমতি সংশোধন করা হয়েছে','extra_message'=> 'কার্যক্রমটি করার সময় কিছু সমস্যা হয়েছে যা নিম্নে বর্ণিত হলঃ <br>'.$saved_but_has_some_prroblems]);
                }else{
                    echo json_encode(['status' => 'success', 'msg' => 'নথি অনুমতি সংশোধন করা হয়েছে']);
                }

                die;
            } catch (\Exception $ex) {
//                $conn->rollback();
                dd($ex);

                echo json_encode(['status' => 'error', 'msg' => 'নথি  অনুমতি সংশোধন করা সম্ভব হচ্ছে না', 'reason' => $this->makeEncryptedData($ex->getMessage())]);
                die;
            }
        } else {
            echo json_encode(['status' => 'error', 'msg' => 'নথি অনুমতি সংশোধন করা সম্ভব হচ্ছে না']);
            die;
        }

        die;
    }

    public function userNothiList()
    {
        $this->layout = null;
        $this->set('dak_subject', '');
        if (!empty($this->request->data)) {

            $selectedDak = !empty($this->request->data['dak_id']) ? explode(',', $this->request->data['dak_id']) : array();
            $selectedDakType = !empty($this->request->data['dak_type']) ? explode(',', $this->request->data['dak_type']) : array();
            $selectedMeta = !empty($this->request->data['meta_data']) ? explode(',', $this->request->data['meta_data']) : array();

            $nothijato = !empty($this->request->data['nothijato']) ? $this->request->data['nothijato']
                : 0;

            if($nothijato == 0 && !empty($selectedMeta) && count($selectedMeta) == 1){
                $employee = $this->getCurrentDakSection();
                $nothiCurrentUsersTable = TableRegistry::get('NothiMasterCurrentUsers');
                $nothiMastersPermitted = $nothiCurrentUsersTable->find()->select([
                    'nothi_master_id','nothi_part_no'
                ])->where([
                    'nothi_part_no'=>$selectedMeta[0],'office_unit_organogram_id'=>$employee['office_unit_organogram_id'],'nothi_office'=>$employee['office_id']
                ])->first();

                $return = ['status'=>'error'];
                if(!empty($nothiMastersPermitted)){
                    $return = ['status'=>'success','data'=>$nothiMastersPermitted];
                }else{
                    $nothiMastersPermitted = $nothiCurrentUsersTable->find()->select([
                        'nothi_master_id','nothi_part_no','office_unit_organogram_id'
                    ])->where([
                        'nothi_part_no'=>$selectedMeta[0],'nothi_office'=>$employee['office_id']
                    ])->first();

                    $employeeOfficeTable = TableRegistry::get('EmployeeOffices');
                    $details = $employeeOfficeTable->getDesignationDetailsInfo($nothiMastersPermitted['office_unit_organogram_id']);

                    $return = ['status'=>'error','details'=>$details];
                }
                $this->response->type('json');
                $this->response->body(json_encode($return));
                return $this->response;
            }else {
                if (!empty($selectedDak)) {
                    $dakFirst = $selectedDak[0];
                    $dakType = $selectedDakType[0];

                    $dakTable = TableRegistry::get(($dakType == DAK_DAPTORIK) ? 'DakDaptoriks'
                        : 'DakNagoriks');

                    $dakInfo = $dakTable->get($dakFirst);

                    $this->set('dak_subject', $dakInfo['dak_subject']);
                }

                $this->set('nothijato', $nothijato);
                $this->set('selectedMeta', $selectedMeta);
            }
        }
    }

    public function userNothiListSelectedForSummeryNote()
    {
        $this->layout = null;
        $this->set('dak_subject', '');
        if (!empty($this->request->data)) {
            $selectedDak = !empty($this->request->data['dak_id']) ? explode(',', $this->request->data['dak_id']) : array();
            $selectedDakType = !empty($this->request->data['dak_type']) ? explode(',', $this->request->data['dak_type']) : array();
            $nothijato = !empty($this->request->data['nothijato']) ? $this->request->data['nothijato'] : 0;
            $sarokNo = !empty($this->request->data['sarok_no']) ? $this->request->data['sarok_no'] : 0;

            $summery_nothi_users_table = TableRegistry::get('SummaryNothiUsers');
            $summery_nothi_users = $summery_nothi_users_table->find()->where(['tracking_id' => $sarokNo, 'sequence_number' => 1])->order(['id desc'])->first();
            if ($summery_nothi_users) {
                $nothi_master_id = $summery_nothi_users->nothi_master_id;
                $nothi_part_no = $summery_nothi_users->nothi_part_no;

                $dakFirst = $selectedDak;
                $dakType = $selectedDakType;

                $dakTable = TableRegistry::get(($dakType == DAK_DAPTORIK) ? 'DakDaptoriks' : 'DakNagoriks');
                $dakInfo = $dakTable->get($dakFirst);

                $return['selected_id'] = $selectedDak[0];
                $return['nothijato'] = $nothijato;
                $return['nothi_master_id'] = $nothi_master_id;
                $return['nothi_part_no'] = $nothi_part_no;
                $return['dak_type'] = $dakType[0];
                $return['due_date'] = '';
                $return['nothisubject'] = $dakInfo['subject'];

                $nothi_master_current_users_table = TableRegistry::get('NothiMasterCurrentUsers');
                $nothi_master_current_users = $nothi_master_current_users_table->find()->where(['nothi_master_id' => $nothi_master_id, 'nothi_part_no' => $nothi_part_no])->first();
                $getCurrentDakSection = $this->getCurrentDakSection();
                if ($getCurrentDakSection['office_unit_organogram_id'] == $nothi_master_current_users->office_unit_organogram_id) {
                    $this->response->body(json_encode(array('status' => 'success', 'msg' => $return)));
                } else {
                    $this->response->body(json_encode(array('status' => 'error', 'msg' => 'নথিটি এই মুহূর্তে আপনার ডেস্কে নাই।')));
                }
            } else {
                $this->response->body(json_encode(array('status' => 'error', 'msg' => 'এই মুহূর্তে নথিটি পাওয়া যায়নি।')));
            }
        } else {
            $this->response->body(json_encode(array('status' => 'error', 'msg' => 'Request method not allowed')));
        }
        $this->response->type('application/json');
        return $this->response;
    }

    public function nothiDetails($id, $nothi_office_id = 0)
    {
        /*         * Module Set to Nothi* */
        $session = $this->request->session();
        if ($session->read('module_id') != 4) {
            $module_id = 4;
            $session->write('module_id', $module_id);

//            $menus = TableRegistry::get('ModuleMenus');
//            $menus = $menus->setupModuleMenu($module_id);
//            $session = $this->request->session();
//            $session->write('menu_items', $menus);
        }

        //Check User comes from Sent List
        $refer = $session->read('refer_url');
        if (!empty($refer)) {
            $listtype = $session->read('refer_url');
        } else {
            $listtype = 'inbox';
        }
        /*         * * */

        $this->set('id', $id);

        $employee_office = $this->getCurrentDakSection();

        $this->set(compact('employee_office'));

        $OfficeDomainTable = TableRegistry::get('OfficeDomains');
        $office_domain = $OfficeDomainTable->getPortalDomainbyOffice($employee_office['office_id']);

        $this->set('office_domain', $office_domain);


        $nothiMasters = array();
        $nothiMastersCurrentUser = array();

        $otherNothi = false;
        if ($nothi_office_id != 0 && $nothi_office_id != $employee_office['office_id']) {
            $otherNothi = true;

            $this->switchOffice($nothi_office_id, 'MainNothiOffice');
        } else {
            $nothi_office_id = $employee_office['office_id'];
        }

        $this->set('nothi_office', $nothi_office_id);
        $this->set(compact('otherNothi'));

        TableRegistry::remove("NothiMasterPermissions");
        TableRegistry::remove("NothiMasterCurrentUsers");
        TableRegistry::remove("NothiMasterMovements");
        TableRegistry::remove("NothiParts");
        TableRegistry::remove("NothiPotros");
        $nothiMasterPermissionsTable = TableRegistry::get("NothiMasterPermissions");
        $nothiMasterCurrentUsersTable = TableRegistry::get("NothiMasterCurrentUsers");
        //$nothiMasterMovementsTable    = TableRegistry::get("NothiMasterMovements");

        $nothiPartsTable = TableRegistry::get("NothiParts");
        //$nothiPotroTable       = TableRegistry::get("NothiPotros");
        $nothiPartsInformation = $nothiPartsTable->find()->where(['id' => $id, 'office_id' => $nothi_office_id])->first();
        $nothi_master_current_user = $nothiMasterCurrentUsersTable->find()->where(['nothi_master_id' => $nothiPartsInformation['nothi_masters_id'], 'nothi_part_no' => $nothiPartsInformation['id']])->first();
        $this->set('nothiInformation', $nothiPartsInformation);

        //dd($nothiPartsInformation['id']);
        //dd($nothi_master_current_user['is_finished']);
        $is_nisponno_this_note = $nothi_master_current_user['is_finished'];
        //$totalhas = 0;
        if (!empty($employee_office)) {
            $id = $nothiPartsInformation['id'];
            try {
                $nothiMasters = $nothiMasterPermissionsTable->hasAccess($nothi_office_id,
                    $employee_office['office_id'],
                    $employee_office['office_unit_id'],
                    $employee_office['office_unit_organogram_id'],
                    $nothiPartsInformation['nothi_masters_id'],
                    $nothiPartsInformation['id']);
                // User does not have permission
                if (empty($nothiMasters)) {
                    $nothiMastersCurrentUser = $nothiMasterCurrentUsersTable->getCurrentUserofNote($nothiPartsInformation['nothi_masters_id'],
                        $nothiPartsInformation['id'], $nothi_office_id)->first();
                    if (empty($nothiMastersCurrentUser) || $nothiMastersCurrentUser['office_unit_organogram_id']
                        != $employee_office['office_unit_organogram_id']
                    ) {
                        $this->Flash->error("দুঃখিত! বর্তমানে এই নোটে আপনার অনুমতি নাই। প্রয়োজনে আপনার অফিস এ্যাডমিনের সাথে যোগাযোগ করুন। (Err: NMCU02)");
                        return $this->redirect(['controller' => 'Dashboard', 'action' => 'dashboard']);
                    } // User does not have permission but user is current user of this note
                    else {
                        $nothiPriviligeRecord = $nothiMasterPermissionsTable->newEntity();
                        $optionsRadios = array();
                        $optionsRadios['nothi_masters_id'] = $nothiPartsInformation['nothi_masters_id'];
                        $optionsRadios['nothi_part_no'] = $nothiPartsInformation['id'];
                        $optionsRadios['office_id'] = $employee_office['office_id'];
                        $optionsRadios['office_unit_id'] = $employee_office['office_unit_id'];
                        $optionsRadios['office_unit_organograms_id'] = $employee_office['office_unit_organogram_id'];
                        $optionsRadios['designation_level'] = $this->BngToEng(1);
                        $optionsRadios['privilige_type'] = 1;
                        $optionsRadios['visited'] = 1;
                        $optionsRadios['nothi_office'] = $nothi_office_id;
                        $optionsRadios['created_by'] = $this->Auth->user('id');

                        $nothiPriviligeRecord = $nothiMasterPermissionsTable->patchEntity($nothiPriviligeRecord,
                            $optionsRadios, ['validate' => 'add']);
                        $priviligeError = $nothiPriviligeRecord->errors();

                        if (!$priviligeError) {
                            $nothiMasterPermissionsTable->save($nothiPriviligeRecord);
                        } else {

                        }
                    }
                } else if ($nothiMasters['privilige_type'] == 0) {
                    $this->Flash->error(" দুঃখিত আপনি অনুমতি প্রাপ্ত নয় ");
                    $this->redirect(['controller' => 'Dashboard', 'action' => 'dashboard']);
                }
//                --previous_code
//                $nothiMastersCurrentUser = $nothiMasterCurrentUsersTable->find()->where(['nothi_part_no' => $id,
//                        'office_id' => $employee_office['office_id'], 'office_unit_id' => $employee_office['office_unit_id'],
//                        'office_unit_organogram_id' => $employee_office['office_unit_organogram_id'],
//                        'nothi_office' => $nothi_office_id])->first();
//
//                if (!empty($nothiMastersCurrentUser) && $nothiMastersCurrentUser['view_status']
//                    == 0) {
//                    $nothiMastersCurrentUser->view_status = 1;
//                    $nothiMasterCurrentUsersTable->save($nothiMastersCurrentUser);
//                }
//                -- previous_code
                $nothiMasterMovementsTable = TableRegistry::get('NothiMasterMovements');
                $getLastMove = $nothiMasterMovementsTable->getLastMove($id);

                if ($getLastMove['to_officer_designation_id'] == $employee_office['office_unit_organogram_id'] &&
                    $getLastMove['view_status'] == 0) {
                    $nothiMasterMovementsTable->updateAll(['view_status' => 1], ['id' => $getLastMove['id']]);
                }

                $timeDif = new Time($getLastMove['modified']);

                if ($getLastMove['from_officer_designation_id'] == $employee_office['office_unit_organogram_id'] &&
                    $getLastMove['view_status'] == 0 &&
                    $timeDif->diffInDays() == 0) {
                    $this->set('nothi_back', 1);
                }
            } catch (RecordNotFoundException $ex) {
                $this->Flash->error($ex->getMessage() . "দুঃখিত! কোনো তথ্য পাওয়া যায়নি");
                $this->redirect(['controller' => 'Dashboard', 'action' => 'dashboard']);
            }
        } else {
            $this->Flash->error("দুঃখিত! কোনো তথ্য পাওয়া যায়নি");
            $this->redirect(['controller' => 'Dashboard', 'action' => 'dashboard']);
        }

        if ($otherNothi == true) {
            $canDelete = 0;
        } else {
            $canDelete = $nothiPartsTable->canDelete($id);
        }
        $this->set(compact('canDelete'));

        $potrojariTable = TableRegistry::get('Potrojari');
        $allNothiParts = array();

        $officeInfo = 0;
        $office_id = $this->request->session()->read('selected_office_section')['office_id'];
        $layer_id = TableRegistry::get('offices')->get($office_id);
        $layer_level = TableRegistry::get('office_layers')->find()->where(['id' => $layer_id['office_layer_id']])->first();
        if ($layer_level['layer_level'] == 1) {
            $officeInfo = 1;
        }

        $this->set('canSummary', 0);
        if (!empty($officeInfo)) {
            $this->set('canSummary', 1);
        }

        try {

            $draftSummary = array();
            $draftSummary = $potrojariTable->find()->where(['potro_status' => 'SummaryDraft',
                'nothi_master_id' => $nothiPartsInformation['nothi_masters_id'],
                'nothi_part_no' => $nothiPartsInformation['id'], 'is_summary_nothi' => 1])->count();
            $this->set(compact('draftSummary'));

            $nothiRecord = $nothiPartsTable->getAll(['NothiParts.id'=> $id])->toArray();

            $officeUnitTable = TableRegistry::get('OfficeUnits');
            $officeunit = $officeUnitTable->get($nothiRecord[0]['office_units_id']);

            $nothiUnit = $officeunit->unit_name_bng;

            $this->set('nothiUnit', $nothiUnit);

            $current_nothi_list = $nothiMasterCurrentUsersTable->getAllPartByDesignation($nothiPartsInformation['nothi_masters_id'],
                $employee_office['office_unit_organogram_id'],
                $employee_office['office_unit_id'], $nothi_office_id);

            if ($listtype == 'sent') {
                $permitted_nothi_list = TableRegistry::get('NothiMasterMovements')->find('list', ['keyField' => 'nothi_part_no', 'valueField' => 'nothi_master_id'])->where(['nothi_master_id' => $nothiPartsInformation['nothi_masters_id'],
                    'from_officer_designation_id' => $employee_office['office_unit_organogram_id'], 'from_office_unit_id' => $employee_office['office_unit_id'],
                    'from_office_id' => $employee_office['office_id']])->toArray();
            } else if ($listtype == 'all') {
                $permitted_nothi_list = $nothiMasterPermissionsTable->find('list',
                    ['keyField' => 'nothi_part_no', 'valueField' => 'nothi_masters_id'])->where(['office_unit_organograms_id' => $employee_office['office_unit_organogram_id'],
                    'nothi_office' => $nothi_office_id, 'office_id' => $employee_office['office_id'],
                    'office_unit_id' => $employee_office['office_unit_id'],
                    'nothi_masters_id' => $nothiPartsInformation['nothi_masters_id']])->toArray();
            } else {
                $permitted_nothi_list = $current_nothi_list;
            }

            $this->set('current_nothi_list', $current_nothi_list);

            $allNothiParts = $nothiPartsTable->find()->select(['id', 'nothi_part_no',
                'nothi_part_no_bn', 'NothiNotes.subject'])
                ->join([
                    'NothiNotes' => [
                        'table' => 'nothi_notes',
                        'type' => 'left',
                        'conditions' => "NothiNotes.nothi_part_no = NothiParts.id AND NothiNotes.note_no=0"
                    ]
                ])
                ->where(['NothiParts.id IN' => array_keys($permitted_nothi_list)])->order(['NothiParts.id  ASC'])->toArray();

            $nothiMasterPriviligeType = array();

            if (!empty($allNothiParts)) {
                foreach ($allNothiParts as $key => $nothiMasters) {

                    $nothiMastersCurrentUser = $nothiMasterCurrentUsersTable->find()->where(['nothi_part_no' => $nothiMasters['id'],
                        'office_id' => $employee_office['office_id'], 'office_unit_id' => $employee_office['office_unit_id'],
                        'office_unit_organogram_id' => $employee_office['office_unit_organogram_id'],
                        'nothi_office' => $nothi_office_id])->first();
                    if (!empty($nothiMastersCurrentUser)) {
                        $nothiMasterPriviligeType[$nothiMasters['id']] = 1;
                    }
                    if (!empty($nothiMastersCurrentUser)) {
                        $nothiMasters['is_finished'] = $nothiMastersCurrentUser['is_finished'];
                    } else {
                        $nothiMasters['is_finished'] = 0;
                    }
                    if (($id == $nothiMastersCurrentUser['nothi_part_no']) && !empty($nothiMastersCurrentUser['priority']) && $nothiMastersCurrentUser['priority'] == 1) {
                        $this->set('note_priority', $nothiMastersCurrentUser['priority']);
                        $this->set('note_priority_text', 'নোটের অগ্রাধিকারঃ ' . (($nothiMastersCurrentUser['priority'] == 1) ? 'জরুরি' : 'সাধারণ'));
                    }
//                    if (!empty($nothiMastersCurrentUser) && $nothiMastersCurrentUser['view_status']
//                        == 0
//                    ) {
//                        $nothiMastersCurrentUser->view_status = 1;
//                        $nothiMasterCurrentUsersTable->save($nothiMastersCurrentUser);
//                    }
                }
            }
        } catch (\PDOException $ex) {
            $this->Flash->error("দুঃখিত! কোনো তথ্য পাওয়া যায়নি");
        }

        $this->set(compact('nothiRecord', 'allNothiParts',
            'nothiMasterPriviligeType', 'nothi_master'));

        $nothiInformationForPotro = $nothiPartsTable->get($id);
        $permitted_nothi_list_for_potro = $nothiMasterPermissionsTable->allNothiPermissionList($nothi_office_id, $employee_office['office_id'], $employee_office['office_unit_id'], $employee_office['office_unit_organogram_id']);
        $potroAttachmentTable = TableRegistry::get('NothiPotroAttachments');

        $potroAttachmentRecord = $potroAttachmentTable->find('list', ['keyField' => 'id', 'valueField' => 'nothi_potro_page_bn'])->where(['NothiPotroAttachments.nothi_master_id' => $nothiInformationForPotro['nothi_masters_id']])
            ->where(['NothiPotroAttachments.nothijato' => 0, 'NothiPotroAttachments.status' => 1])
            ->where(['NothiPotroAttachments.nothi_part_no IN' => $permitted_nothi_list_for_potro])
            ->order(['NothiPotroAttachments.id desc'])->toArray();

        $potroAttachmentRecordBn = [];
        if (!empty($potroAttachmentRecord)) {
            foreach ($potroAttachmentRecord as $key => $value) {
                $potroAttachmentRecordBn[enTobn($key)] = $value;
            }
        }

        $this->set('potroAttachmentRecord', $potroAttachmentRecordBn);
        $noteDecisionTable = TableRegistry::get('NothiDecisionEmployees');
        $creator_employee_id = (empty($employee_office['officer_id']) && $this->Auth->user('user_role_id') <= 2) ? 0 : $employee_office['officer_id'];
        $noteDecision = $noteDecisionTable->GetNothiDecisionsByEmployee($creator_employee_id);
        if (empty($noteDecision)) {
            $noteDecision = $noteDecisionTable->GetNothiDecisionsByEmployee(0);
        }
        $this->set(compact('noteDecision'));

        $guardFileCategories_table = TableRegistry::get("GuardFileCategories");
        $guarfilesubjects = $guardFileCategories_table->getTypes(0, $employee_office);
        $this->set(compact('guarfilesubjects'));

        // Setting Current user in each office DB
        $priv_type = $this->setNothiCurrentUser($id, $nothi_office_id, $employee_office);
        // Setting Current user in each office DB

        if ($priv_type['prev'] == 1) {
            $this->set('privilige_type', 1);
        } else {
            $this->set('privilige_type', 0);
        }

        #### nothi archive age detection
        //dd($nothiRecord[0]['nothi_masters_id']);
        $archive = getNothiArchiveStatus($nothiRecord[0]['nothi_masters_id'], $nothiRecord[0]['office_id']);
        $this->set('archive', $archive);
        #### nothi archive age detection
        if ($archive['level'] == 2) {
            if ($is_nisponno_this_note == 1) {
                $this->set('privilige_type', 0);
            }
        }
        if ($priv_type['multiple_offices'] == false) {
            $this->switchOffice($employee_office['office_id'], 'MainNothiOffice');
            TableRegistry::remove('NothiMasterCurrentUsers');
            TableRegistry::remove('NothiMasterPermissions');
        }
    }

    public function nothiDetailsNew($id, $nothi_office_id = 0)
    {
        /* * Module Set to Nothi * */
        $session = $this->request->session();
        if ($session->read('module_id') != 4) {
            $module_id = 4;
            $session->write('module_id', $module_id);
        }

        //Check User comes from Sent List
        $refer = $session->read('refer_url');
        if (!empty($refer)) {
            $listtype = $session->read('refer_url');
        } else {
            $listtype = 'inbox';
        }
        /*         * * */

        $this->set('id', $id);
        $previ = 0;

        $employee_office = $this->getCurrentDakSection();
        if (!empty($this->request->query('permitted_by'))) {
            $organogram_id = $this->request->query('permitted_by');
            $OtherOrganogramActivitiesSettingsTable = TableRegistry::get('OtherOrganogramActivitiesSettings');
            $OtherOrganogramActivitiesSettings = $OtherOrganogramActivitiesSettingsTable->find()->where(['organogram_id' => $organogram_id, 'assigned_organogram_id' => $employee_office['office_unit_organogram_id'], 'status' => 1, 'permission_for IN' => [0,2]])->count();
            if ($OtherOrganogramActivitiesSettings > 0) {
                $employeeOfficeTable = TableRegistry::get('EmployeeOffices');
                $emp_office = designationInfo($organogram_id);
                $employee_office = $employeeOfficeTable->find()->where(['status' => 1, 'office_unit_organogram_id' => $organogram_id])->first();
                $employee_office = array_merge($employee_office->toArray(), $emp_office);
            }
        }
        $this->set(compact('employee_office'));

        $nothiMasters = array();
        $nothiMastersCurrentUser = array();

        $otherNothi = false;
        if ($nothi_office_id != 0 && $nothi_office_id != $employee_office['office_id']) {
            $otherNothi = true;
            $this->switchOffice($nothi_office_id, 'MainNothiOffice');
        } else {
            $nothi_office_id = $employee_office['office_id'];
        }

        $this->set('nothi_office', $nothi_office_id);
        $this->set(compact('otherNothi'));

        TableRegistry::remove("NothiMasterPermissions");
        TableRegistry::remove("NothiMasterCurrentUsers");
        TableRegistry::remove("NothiMasterMovements");
        TableRegistry::remove("NothiParts");
        TableRegistry::remove("NothiPotros");
        $nothiMasterPermissionsTable = TableRegistry::get("NothiMasterPermissions");
        $nothiMasterCurrentUsersTable = TableRegistry::get("NothiMasterCurrentUsers");
        $nothiMasterMovementsTable = TableRegistry::get("NothiMasterMovements");

        $nothiPartsTable = TableRegistry::get("NothiParts");
        $nothiPartsInformation = $nothiRecord = $nothiPartsTable->getAll(['NothiParts.id' => $id])->first();
        $nothi_master_current_user = $nothiMasterCurrentUsersTable->find()->where(['nothi_master_id' => $nothiPartsInformation['nothi_masters_id'], 'nothi_part_no' => $nothiPartsInformation['id']])->first();
        if(!empty($nothi_master_current_user) && !empty($nothi_master_current_user['office_unit_organogram_id'])){
           //check if this note is in other office desk and that user is current
            if($nothi_master_current_user['office_id'] != $employee_office['office_id']){
                try{
                    $priviliges = $nothiMasterPermissionsTable->find()->select([
                        'perm_value' => 'privilige_type',
                        'ofc_id' => 'office_id',
                        'unit_id' => 'office_unit_id',
                        'org_id' => 'office_unit_organograms_id',
                        'designation_level' => 'designation_level',
                        'visited' => 'visited'
                    ])->where(['nothi_masters_id' => $nothi_master_current_user['nothi_master_id'], 'nothi_part_no' => $nothi_master_current_user['nothi_part_no'], 'nothi_office' => $nothi_master_current_user['nothi_office']])->toArray();
                    $is_connected = $this->switchOfficeWithStatus($nothi_master_current_user['office_id'], 'OtherOffice', -1, true);
                    if (!$is_connected || (!empty($is_connected['status']) && $is_connected['status'] == 'error')) {
                        if ($nothi_master_current_user['office_id'] != $employee_office['office_id']) {
                            $officeDomainsTable = TableRegistry::get('OfficeDomains');
                            $otherOffice = $officeDomainsTable->find()->where(['office_id' => $nothi_master_current_user['office_id']])->first();
                            $url = $otherOffice->domain_url . "/postUpdateOtherOfficePermissions";

                            $encrypted_data = $this->makeEncryptedData(serialize([
                                'nothi_office' => $nothi_master_current_user['nothi_office'],
                                'nothi_part_no' => $nothi_master_current_user['nothi_part_no'],
                                'nothi_master_id' => $nothi_master_current_user['nothi_master_id']
                            ]));
                            $data = [
                                'token' => base64_encode($encrypted_data),
                                'nothi_office' => $nothi_master_current_user['nothi_office'],
                                'nothi_part_no' => $nothi_master_current_user['nothi_part_no'],
                                'nothi_master_id' => $nothi_master_current_user['nothi_master_id'],
                                'current_users' => json_encode(['office_id' => $nothi_master_current_user['office_id'], 'office_unit_id' => $nothi_master_current_user['office_unit_id'], 'office_unit_organogram_id' => $nothi_master_current_user['office_unit_organogram_id']]),
                                'permissions' => json_encode($priviliges),
                                'target_office_id' => $nothi_master_current_user['office_id']
                            ];

                            $auth_token_response = curlRequest($url, $data);
                            if (!empty($auth_token_response)) {
                                $auth_token_response = jsonA($auth_token_response);
                                if (!empty($auth_token_response['status']) && ($auth_token_response['status']) == 'success') {
                                    //continue;
                                }
                            } else {
                                $office_info = TableRegistry::get('Offices')->getBanglaName($nothi_master_current_user['office_id']);
                                if (!empty($office_info['office_name_bng'])) {
                                    throw new Exception('অফিসের  (' . $office_info['office_name_bng'] . ') নির্বাচিত কর্মকর্তাদের অনুমতি দেওয়া সম্ভব হয়নি। কারণঃ ডাটাবেজ পাওয়া যায় নি!');
                                } else {
                                    throw new Exception('অফিসের (আইডিঃ ' . $nothi_master_current_user['office_id'] . ') নির্বাচিত কর্মকর্তাদের অনুমতি দেওয়া সম্ভব হয়নি। কারণঃ ডাটাবেজ পাওয়া যায় নি!');
                                }
                            }
                        }
                    } else {
                        TableRegistry::remove("NothiMasterCurrentUsers");
                        $nothiMasterCurrentUsersTableofOtherOffice = TableRegistry::get("NothiMasterCurrentUsers");
                        //first check if current note has entry
                        $hasDatainCurrentUser = $nothiMasterCurrentUsersTableofOtherOffice->find()->where(
                            [
                                'nothi_part_no' => $id,
                                'nothi_office' => $nothi_office_id,
                            ]
                        )->count();
                        if (empty($hasDatainCurrentUser)) {
                            // other office current user table is missing . need to add
                            // need to create a new row
                            $nothiMasterCurrentUsersEntity = $nothiMasterCurrentUsersTableofOtherOffice->newEntity();
                            $nothiMasterCurrentUsersEntity->nothi_master_id = $nothi_master_current_user['nothi_master_id'];
                            $nothiMasterCurrentUsersEntity->nothi_part_no = $nothi_master_current_user['nothi_part_no'];
                            $nothiMasterCurrentUsersEntity->office_id = $nothi_master_current_user['office_id'];
                            $nothiMasterCurrentUsersEntity->office_unit_id = $nothi_master_current_user['office_unit_id'];
                            $nothiMasterCurrentUsersEntity->office_unit_organogram_id = $nothi_master_current_user['office_unit_organogram_id'];
                            $nothiMasterCurrentUsersEntity->employee_id = $nothi_master_current_user['employee_id'];
                            $nothiMasterCurrentUsersEntity->issue_date = $nothi_master_current_user['issue_date'];
                            $nothiMasterCurrentUsersEntity->forward_date = $nothi_master_current_user['forward_date'];
                            $nothiMasterCurrentUsersEntity->is_new = 0;
                            $nothiMasterCurrentUsersEntity->nothi_office = $nothi_office_id;
                            $nothiMasterCurrentUsersEntity->is_archive = $nothi_master_current_user['is_archive'];
                            $nothiMasterCurrentUsersEntity->is_finished = 0;
                            $nothiMasterCurrentUsersEntity->priority = $nothi_master_current_user['priority'];
                            $nothiMasterCurrentUsersEntity->is_summary = $nothi_master_current_user['is_summary'];
                            $nothiMasterCurrentUsersEntity->view_status = 0;
                            $nothiMasterCurrentUsersTableofOtherOffice->save($nothiMasterCurrentUsersEntity);
                        } else {
                            $nothiMasterCurrentUsersTableofOtherOffice->updateAll([
                                'office_id' => $nothi_master_current_user['office_id'],
                                'office_unit_id' => $nothi_master_current_user['office_unit_id'],
                                'office_unit_organogram_id' => $nothi_master_current_user['office_unit_organogram_id'],
                                'employee_id' => $nothi_master_current_user['employee_id'],
                            ], [
                                'nothi_part_no' => $id,
                                'nothi_office' => $nothi_office_id,
                            ]);
                        }
                    }
                }catch(\Exception $ex){
                    $this->Flash->error("দুঃখিত! যান্ত্রিক ত্রুটি হয়েছে ERR:ND001");
                    return $this->redirect(['controller' => 'Dashboard', 'action' => 'dashboard']);
                }
                $this->switchOffice($nothi_office_id, 'MainNothiOffice');
            }
        }
        else{
            // current user is not found. Some problem occurred. need to entry a new row for current user
            if(!empty($nothiPartsInformation)){
                $nothiMasterCurrentUsersEntity = $nothiMasterCurrentUsersTable->newEntity();
                $nothiMasterCurrentUsersEntity->nothi_master_id = $nothiPartsInformation['nothi_masters_id'];
                $nothiMasterCurrentUsersEntity->nothi_part_no = $nothiPartsInformation['id'];
                $nothiMasterCurrentUsersEntity->office_id = $nothiPartsInformation['office_id'];
                $nothiMasterCurrentUsersEntity->office_unit_id = $nothiPartsInformation['office_units_id'];
                $nothiMasterCurrentUsersEntity->office_unit_organogram_id = $nothiPartsInformation['office_units_organogram_id'];
                $nothiMasterCurrentUsersEntity->employee_id = !empty($nothiPartsInformation['created_by'])?$nothiPartsInformation['created_by']:0;
                $nothiMasterCurrentUsersEntity->issue_date = $nothiPartsInformation['created'];
                $nothiMasterCurrentUsersEntity->forward_date = $nothiPartsInformation['created'];
                $nothiMasterCurrentUsersEntity->is_new = 0;
                $nothiMasterCurrentUsersEntity->nothi_office = $nothi_office_id;
                $nothiMasterCurrentUsersEntity->is_archive =!empty($nothiPartsInformation['is_archive'])?$nothiPartsInformation['is_archive']:0;
                $nothiMasterCurrentUsersEntity->is_finished =0;
                $nothiMasterCurrentUsersEntity->is_summary =!empty($nothiPartsInformation['is_summary'])?$nothiPartsInformation['is_summary']:0;
                $nothiMasterCurrentUsersEntity->view_status =0;
                $isDataSave = $nothiMasterCurrentUsersTable->save($nothiMasterCurrentUsersEntity);
                if(!empty($isDataSave)){
                    $nothi_master_current_user = $isDataSave->toArray();
                }
            }
        }

        $total_note_of_this_part = TableRegistry::get('NothiNotes')->find()->where(['nothi_part_no' => $id])->count();
        $this->set('total_note_of_this_part', $total_note_of_this_part);

        $this->set('nothiInformation', $nothiPartsInformation);

        $this->set('noteNos', (new NothiNoteSheetsController())->showNoteList($nothiPartsInformation['nothi_masters_id'], $nothi_office_id, $employee_office));

        //dd($nothi_master_current_user['is_finished']);
        $is_nisponno_this_note = $nothi_master_current_user['is_finished'];
        //$totalhas = 0;
        if (!empty($employee_office)) {
            $id = $nothiPartsInformation['id'];
            try {
                $nothiMasters = $nothiMasterPermissionsTable->hasAccess($nothi_office_id,
                    $employee_office['office_id'],
                    $employee_office['office_unit_id'],
                    $employee_office['office_unit_organogram_id'],
                    $nothiPartsInformation['nothi_masters_id'],
                    $nothiPartsInformation['id']);
                // User does not have permission
                if (empty($nothiMasters)) {
                    $nothiMastersCurrentUser = $nothiMasterCurrentUsersTable->getCurrentUserofNote($nothiPartsInformation['nothi_masters_id'],
                        $nothiPartsInformation['id'], $nothi_office_id)->first();
                    if (empty($nothiMastersCurrentUser) || $nothiMastersCurrentUser['office_unit_organogram_id']
                        != $employee_office['office_unit_organogram_id']
                    ) {
                        $this->Flash->error("দুঃখিত! বর্তমানে এই নোটে আপনার অনুমতি নাই। প্রয়োজনে আপনার অফিস এ্যাডমিনের সাথে যোগাযোগ করুন। (Err: NMCU03)");
                        return $this->redirect(['controller' => 'Dashboard', 'action' => 'dashboard']);
                    } // User does not have permission but user is current user of this note
                    else {
                        $nothiPriviligeRecord = $nothiMasterPermissionsTable->newEntity();
                        $optionsRadios = array();
                        $optionsRadios['nothi_masters_id'] = $nothiPartsInformation['nothi_masters_id'];
                        $optionsRadios['nothi_part_no'] = $nothiPartsInformation['id'];
                        $optionsRadios['office_id'] = $employee_office['office_id'];
                        $optionsRadios['office_unit_id'] = $employee_office['office_unit_id'];
                        $optionsRadios['office_unit_organograms_id'] = $employee_office['office_unit_organogram_id'];
                        $optionsRadios['designation_level'] = $this->BngToEng(1);
                        $optionsRadios['privilige_type'] = 1;
                        $optionsRadios['visited'] = 1;
                        $optionsRadios['nothi_office'] = $nothi_office_id;
                        $optionsRadios['created_by'] = $this->Auth->user('id');

                        $nothiPriviligeRecord = $nothiMasterPermissionsTable->patchEntity($nothiPriviligeRecord,
                            $optionsRadios, ['validate' => 'add']);
                        $priviligeError = $nothiPriviligeRecord->errors();

                        if (!$priviligeError) {
                            $nothiMasterPermissionsTable->save($nothiPriviligeRecord);
                        } else {

                        }
                    }
                } else if ($nothiMasters['privilige_type'] == 0) {
                    $this->Flash->error(" দুঃখিত আপনি অনুমতি প্রাপ্ত নয় ");
                    return $this->redirect(['controller' => 'Dashboard', 'action' => 'dashboard']);
                }

                $nothiMastersCurrentUser = $nothiMasterCurrentUsersTable->find()->where(['nothi_part_no' => $id, 'nothi_master_id' => $nothiPartsInformation['nothi_masters_id'], 'nothi_office' => $nothi_office_id])->first();

                if (!empty($nothiMastersCurrentUser)) {
                    $this->set('nisponno', $nothiMastersCurrentUser['is_finished']);
                    if ($nothiMastersCurrentUser['office_unit_organogram_id'] == $employee_office['office_unit_organogram_id']) {
                        $this->set('privilige_type', $nothiMasters['privilige_type']);
                        $this->set('current_status', 1);
                        $previ = $nothiMasters['privilige_type'];
                    } else {
                        $this->set('privilige_type', 2);
                        $this->set('current_status', $nothiMastersCurrentUser['view_status']);
                    }
                } else {
                    $this->set('nisponno', 0);
                    $this->set('privilige_type', 2);
                    $this->set('current_status', 0);
                }
                $nothimovement_count = $nothiMasterMovementsTable->getTotalMove($id, $employee_office);
                $this->set('nothimovement', $nothimovement_count);

                $nothiMasterMovementsTable = TableRegistry::get('NothiMasterMovements');
                $getLastMove = $nothiMasterMovementsTable->getLastMove($id);

                if ($getLastMove['to_officer_designation_id'] == $employee_office['office_unit_organogram_id'] &&
                    $getLastMove['view_status'] == 0) {
                    $nothiMasterMovementsTable->updateAll(['view_status' => 1], ['id' => $getLastMove['id']]);
                }

                $timeDif = new Time($getLastMove['modified']);

                if ($getLastMove['from_officer_designation_id'] == $employee_office['office_unit_organogram_id'] &&
                    $getLastMove['view_status'] == 0 &&
                    $timeDif->diffInDays() == 0) {
                    $this->set('nothi_back', 1);
                }
            } catch (RecordNotFoundException $ex) {
                $this->Flash->error($ex->getMessage() . "দুঃখিত! কোনো তথ্য পাওয়া যায়নি");
                return $this->redirect(['controller' => 'Dashboard', 'action' => 'dashboard']);
            }
        } else {
            $this->Flash->error("দুঃখিত! কোনো তথ্য পাওয়া যায়নি");
            return $this->redirect(['controller' => 'Dashboard', 'action' => 'dashboard']);
        }

        if ($otherNothi == true) {
            $canDelete = 0;
        } else {
            $canDelete = $nothiPartsTable->canDelete($id);
        }
        $this->set(compact('canDelete'));

        $potrojariTable = TableRegistry::get('Potrojari');
        $allNothiParts = array();

        $officeInfo = 0;
        $office_id = $this->request->session()->read('selected_office_section')['office_id'];
        $layer_id = TableRegistry::get('offices')->get($office_id);
        $layer_level = TableRegistry::get('office_layers')->find()->where(['id' => $layer_id['office_layer_id']])->first();
        if ($layer_level['layer_level'] == 1) {
            $officeInfo = 1;
        }

        $this->set('canSummary', 0);
        if (!empty($officeInfo)) {
            $this->set('canSummary', 1);
        }

        try {

            $draftSummary = array();
            $draftSummary = $potrojariTable->find()->where(['potro_status' => 'SummaryDraft',
                'nothi_master_id' => $nothiPartsInformation['nothi_masters_id'],
                'nothi_part_no' => $nothiPartsInformation['id'], 'is_summary_nothi' => 1])->count();
            $this->set(compact('draftSummary'));


            $officeUnitTable = TableRegistry::get('OfficeUnits');
            $officeunit = $officeUnitTable->get($nothiRecord['office_units_id']);

            $nothiUnit = $officeunit->unit_name_bng;

            $this->set('nothiUnit', $nothiUnit);

            $current_nothi_list = $nothiMasterCurrentUsersTable->getAllPartByDesignation($nothiPartsInformation['nothi_masters_id'],
                $employee_office['office_unit_organogram_id'],
                $employee_office['office_unit_id'], $nothi_office_id);

            if ($listtype == 'sent') {
                $permitted_nothi_list = TableRegistry::get('NothiMasterMovements')->find('list', ['keyField' => 'nothi_part_no', 'valueField' => 'nothi_master_id'])->where(['nothi_master_id' => $nothiPartsInformation['nothi_masters_id'],
                    'from_officer_designation_id' => $employee_office['office_unit_organogram_id'], 'from_office_unit_id' => $employee_office['office_unit_id'],
                    'from_office_id' => $employee_office['office_id']])->toArray();
            } else if ($listtype == 'all') {
                $permitted_nothi_list = $nothiMasterPermissionsTable->find('list',
                    ['keyField' => 'nothi_part_no', 'valueField' => 'nothi_masters_id'])->where(['office_unit_organograms_id' => $employee_office['office_unit_organogram_id'],
                    'nothi_office' => $nothi_office_id, 'office_id' => $employee_office['office_id'],
                    'office_unit_id' => $employee_office['office_unit_id'],
                    'nothi_masters_id' => $nothiPartsInformation['nothi_masters_id']])->toArray();
            } else {
                $permitted_nothi_list = $current_nothi_list;
            }

            $this->set('current_nothi_list', $current_nothi_list);

            $allNothiParts = $nothiPartsTable->find()->select(['id', 'nothi_part_no',
                'nothi_part_no_bn', 'NothiNotes.subject'])
                ->join([
                    'NothiNotes' => [
                        'table' => 'nothi_notes',
                        'type' => 'left',
                        'conditions' => "NothiNotes.nothi_part_no = NothiParts.id AND NothiNotes.note_no=0"
                    ]
                ])
                ->where(['NothiParts.id IN' => array_keys($permitted_nothi_list)])->order(['NothiParts.nothi_part_no  ASC']);
            $total_part = $allNothiParts->count();
            $allNothiParts = $allNothiParts->limit($this->load_part_at_a_time)->toArray();

            $pending_note = $nothiMasterCurrentUsersTable->find('list', ['keyField' => 'nothi_part_no', 'valueField' => 'nothi_part_no'])->where(['is_finished !=' => 1, 'nothi_part_no IN' => array_keys($permitted_nothi_list)])->toArray();
            $this->set('pending_note', $pending_note);

            $this->set('permitted_nothi_list', $permitted_nothi_list);
            $this->set('total_part', $total_part);

            $nothiMasterPriviligeType = array();

            if (!empty($allNothiParts)) {
                foreach ($allNothiParts as $key => $nothiMasters) {

                    $nothiMastersCurrentUser = $nothiMasterCurrentUsersTable->find()->where(['nothi_part_no' => $nothiMasters['id'],
                        'office_id' => $employee_office['office_id'], 'office_unit_id' => $employee_office['office_unit_id'],
                        'office_unit_organogram_id' => $employee_office['office_unit_organogram_id'],
                        'nothi_office' => $nothi_office_id])->first();
                    if (!empty($nothiMastersCurrentUser)) {
                        $nothiMasterPriviligeType[$nothiMasters['id']] = 1;
                    }
                    if (!empty($nothiMastersCurrentUser)) {
                        $nothiMasters['is_finished'] = $nothiMastersCurrentUser['is_finished'];
                    } else {
                        $nothiMasters['is_finished'] = 0;
                    }
                    if (($id == $nothiMastersCurrentUser['nothi_part_no']) && !empty($nothiMastersCurrentUser['priority']) && $nothiMastersCurrentUser['priority'] == 1) {
                        $this->set('note_priority', $nothiMastersCurrentUser['priority']);
                        $this->set('note_priority_text', 'নোটের অগ্রাধিকারঃ ' . (($nothiMastersCurrentUser['priority'] == 1) ? 'জরুরি' : 'সাধারণ'));
                    }
                }
            }
        } catch (\PDOException $ex) {
            $this->Flash->error("দুঃখিত! কোনো তথ্য পাওয়া যায়নি");
        }

        $this->set(compact('nothiRecord', 'allNothiParts',
            'nothiMasterPriviligeType', 'nothi_master'));

        $nothiInformationForPotro = $nothiPartsTable->get($id);
        $permitted_nothi__part_list_for_potro = $nothiMasterPermissionsTable->NothiPermissionListForSingleNothi($nothi_office_id, $employee_office['office_id'], $employee_office['office_unit_id'], $employee_office['office_unit_organogram_id'], $nothiInformationForPotro['nothi_masters_id']);
        $potroAttachmentTable = TableRegistry::get('NothiPotroAttachments');

        $potroAttachmentRecord = $potroAttachmentTable->getBibecchoPotroWithSubject($permitted_nothi__part_list_for_potro);

        $this->set('potroAttachmentRecord', $potroAttachmentRecord);
        $noteDecisionTable = TableRegistry::get('NothiDecisionEmployees');
        $creator_employee_id = (empty($employee_office['officer_id']) && $this->Auth->user('user_role_id') <= 2) ? 0 : $employee_office['officer_id'];
        $noteDecision = $noteDecisionTable->GetNothiDecisionsByEmployee($creator_employee_id);
        if (empty($noteDecision)) {
            $noteDecision = $noteDecisionTable->GetNothiDecisionsByEmployee(0);
        }
        $this->set(compact('noteDecision'));

        $guardFileCategories_table = TableRegistry::get("GuardFileCategories");
        $guarfilesubjects = $guardFileCategories_table->getTypes(0, $employee_office);
        $this->set(compact('guarfilesubjects'));

        // Setting Current user in each office DB
        $priv_type = $this->setNothiCurrentUser($id, $nothi_office_id, $employee_office);
        // Setting Current user in each office DB

        if ($priv_type['prev'] == 1) {
            $previ =1;
            $this->set('privilige_type', 1);
        } else {
            $previ =0;
            $this->set('privilige_type', 0);
        }

        $nothi_note_subject = TableRegistry::get('NothiNotes')->find()->where([
        	'nothi_part_no' => $id,
			'note_no' => 0
		])->first();
		$this->set('nothi_note_subject', $nothi_note_subject);

        #### nothi archive age detection
        //dd($nothiRecord[0]['nothi_masters_id']);
        $archive = getNothiArchiveStatus($nothiRecord['nothi_masters_id'], $nothiRecord['office_id']);
        $this->set('archive', $archive);
        #### nothi archive age detection
        if ($archive['level'] == 2) {
            if ($is_nisponno_this_note == 1) {
                $this->set('privilige_type', 0);
            }
        }
        if ($priv_type['multiple_offices'] == false) {
            $this->switchOffice($employee_office['office_id'], 'MainNothiOffice');
            TableRegistry::remove('NothiMasterCurrentUsers');
            TableRegistry::remove('NothiMasterPermissions');
        }
        $potrojari_templates = TableRegistry::get('PotrojariTemplates');
        $template_list = $potrojari_templates->find('list', ['keyField' => 'template_id', 'valueField' => 'template_name'])->where(['status' => 1])->toArray();

        $i = 0;
        $templete_options[$i]['text'] = 'পত্রের ধরন বাছাই করুন';
        $templete_options[$i]['value'] = '';
        // define offices2ShowTemplateModals
        $offices2ShowTemplateModals = $this->offices2ShowTemplateModals();
        $this->set(compact('offices2ShowTemplateModals'));
        foreach ($template_list as $key => $value) {
            if ($key == 21 && !in_array($nothi_office_id, $offices2ShowTemplateModals)) {
                continue;
            }
            $i++;
            $templete_options[$i]['text'] = $value;
            $templete_options[$i]['value'] = $key;
        }
        $this->set('templete_options', $templete_options);

        if($previ==1 && $refer == 'sent'){
            $this->Flash->set("নোটটি বর্তমানে আপনার ডেস্ক এ রয়েছে");
        }
    }

    private function setNothiCurrentUser($id, $nothi_office_id, $employee_office)
    {
        // set view Stauts one in every Office nothi master current user table which have permission on this nothi
        $response = ['prev' => 0, 'multiple_offices' => FALSE];
        $nothiMasterPermissionsTable = TableRegistry::get("NothiMasterPermissions");
        $allPermittedOfficesList = $nothiMasterPermissionsTable->permittedOfficesId($id, $nothi_office_id);
        $multiple_offices_has_permission = false;
        if (!empty($allPermittedOfficesList)) {
            if (count($allPermittedOfficesList) == 0 && current($allPermittedOfficesList) == $employee_office['office_id']) {
                $multiple_offices_has_permission = true;
                $response['multiple_offices'] = true;
            }
            foreach ($allPermittedOfficesList as $key => $value) {
                try {
                    if ($multiple_offices_has_permission == false) {
                        $is_connected =  $this->switchOfficeWithStatus($key, 'MainNothiOffice',-1,true);
                        if(!$is_connected || (!empty($is_connected['status']) && $is_connected['status'] == 'error')){
                            continue;
                        }
                        $checkDBConnection = $this->checkCurrentOfficeDBConnection();
                        if(empty($checkDBConnection) || (!empty($checkDBConnection['status']) && $checkDBConnection['status'] == 'error')){
                            continue;
                        }
                    }

                    TableRegistry::remove('NothiMasterCurrentUsers');
                    $nothiMasterCurrentUsersTable = TableRegistry::get("NothiMasterCurrentUsers");
                    $nothiMastersCurrentUser = $nothiMasterCurrentUsersTable->find()->where(['nothi_part_no' => $id,
                        'office_id' => $employee_office['office_id'], 'office_unit_id' => $employee_office['office_unit_id'],
                        'office_unit_organogram_id' => $employee_office['office_unit_organogram_id'],
                        'nothi_office' => $nothi_office_id])->first();
                    if (!empty($nothiMastersCurrentUser) && $nothiMastersCurrentUser['view_status'] == 0) {
                        $nothiMastersCurrentUser->view_status = 1;
                        $nothiMasterCurrentUsersTable->save($nothiMastersCurrentUser);
                    }
                    if (!empty($nothiMastersCurrentUser)) {
                        $response['prev'] = 1;
                    }
                } catch (\Exception $ex) {
                    $response['prev'] = 0;
                    return $response;
                }
            }
        }
        return $response;
    }

    public function nothiDelete($id, $nothi_office = 0)
    {
        $employee_office = $this->getCurrentDakSection();

        $nothi_dak_potro_mapsTable = TableRegistry::get("NothiDakPotroMaps");
        $nothi_masters_dak_mapTable = TableRegistry::get("NothiMastersDakMap");
        $nothi_master_permissionsTable = TableRegistry::get("NothiMasterPermissions");
        $nothi_notesTable = TableRegistry::get("NothiNotes");
        $nothi_note_attachmentsTable = TableRegistry::get("NothiNoteAttachments");
        $nothi_note_attachment_refsTable = TableRegistry::get("NothiNoteAttachmentRefs");
        $nothi_note_permissionsTable = TableRegistry::get("NothiNotePermissions");
        $nothi_note_sheetsTable = TableRegistry::get("NothiNoteSheets");
        $nothi_potrosTable = TableRegistry::get("NothiPotros");
        $nothi_potro_attachmentsTable = TableRegistry::get("NothiPotroAttachments");
        $nothiMasterCurrentUsersTable = TableRegistry::get("NothiMasterCurrentUsers");
        $nothiPartsTable = TableRegistry::get("NothiParts");
        $nothiMasterMovementsTable = TableRegistry::get("NothiMasterMovements");
        $potroFlagsTable = TableRegistry::get("PotroFlags");

        $nothiPartsInformation = $nothiPartsTable->get($id);

        $response = array(
            'status' => 'error',
            'msg' => 'নোট মুছে ফেলা সম্ভব হচ্ছে না। '
        );
        if (!empty($employee_office)) {

            $con = ConnectionManager::get('default');
            try {
                $con->begin();

                $nothiMastersCurrentUser = $nothiMasterCurrentUsersTable->find()->where(['nothi_part_no' => $id,
                    'office_id' => $employee_office['office_id'], 'office_unit_id' => $employee_office['office_unit_id'],
                    'office_unit_organogram_id' => $employee_office['office_unit_organogram_id'],
                    'nothi_office' => $employee_office['office_id']])->first();

                $canDelete = $nothiPartsTable->canDelete($id);

                if (!empty($nothiMastersCurrentUser) && $canDelete == true) {

                    $nothi_dak_potro_mapsTable->deleteAll(['nothi_part_no' => $id]);
                    $nothi_masters_dak_mapTable->deleteAll(['nothi_part_no' => $id]);
                    $nothi_master_permissionsTable->deleteAll(['nothi_part_no' => $id,
                        'nothi_office' => $employee_office['office_id']]);
                    $nothi_notesTable->deleteAll(['nothi_part_no' => $id]);
                    $nothi_note_attachmentsTable->deleteAll(['nothi_part_no' => $id]);
                    $nothi_note_attachment_refsTable->deleteAll(['nothi_part_no' => $id]);

                    $nothi_note_sheetsTable->deleteAll(['nothi_part_no' => $id]);
                    $nothi_potrosTable->deleteAll(['nothi_part_no' => $id]);
                    $nothi_potro_attachmentsTable->deleteAll(['nothi_part_no' => $id]);
                    $nothiMasterMovementsTable->deleteAll(['nothi_part_no' => $id]);
                    $potroFlagsTable->deleteAll(['nothi_part_no' => $id]);


                    $nothiMasterCurrentUsersTable->deleteAll(['nothi_part_no' => $id,
                        'nothi_office' => $employee_office['office_id']]);
                    $nothiPartsTable->delete($nothiPartsInformation);
                    $response = array(
                        'status' => 'success',
                        'msg' => 'নোট মুছে ফেলা হয়েছে।'
                    );
                }

                $con->commit();
            } catch (\Exception $ex) {
                $response = array(
                    'status' => 'error',
                    'msg' => $ex->getMessage()
                );

                $con->rollback();
            }
        }

        echo json_encode($response);
        die;
    }

    public function nothiDetailsAll($part_id, $nothi_office = 0)
    {

//        $this->set('refer_url', $this->referer());
        $employee_office = $this->getCurrentDakSection();
        $otherNothi = false;
        if ($nothi_office != 0 && $nothi_office != $employee_office['office_id']) {
            $otherNothi = true;

            $this->switchOffice($nothi_office, 'MainNothiOffice');
        } else {
            $nothi_office = $employee_office['office_id'];
        }

        $this->set('nothi_office', $nothi_office);
        $this->set(compact('otherNothi','part_id'));

        $nothiMasterPermissionsTable = TableRegistry::get("NothiMasterPermissions");
        $nothiMasterCurrentUsersTable = TableRegistry::get("NothiMasterCurrentUsers");

        TableRegistry::remove('NothiParts');
        $nothiPartsTable = TableRegistry::get("NothiParts");


        $nothiMastersCurrentUser = array();

        $nothiPartsInformation = $nothiPartsTable->getAll(['NothiParts.id' => $part_id])->first();
        /**
         * Has access in this nothi part
         * if not visited redirect to dashboard
         */
        $has_access = $nothiMasterPermissionsTable->hasAccessPartList($employee_office['office_id'],
            $employee_office['office_unit_id'],
            $employee_office['office_unit_organogram_id'], $part_id,
            $nothi_office);
        if (empty($has_access)) {
            /**
             * Last Nothi part does not have permission but one of the Nothi part of this nothi has permission
             * let's find that nothi
             */
            if (!empty($nothiPartsInformation['nothi_masters_id'])) {
                $hasAccessinNothiMaster = $nothiMasterPermissionsTable->hasAccess($nothi_office, $employee_office['office_id'],
                    $employee_office['office_unit_id'],
                    $employee_office['office_unit_organogram_id'], $nothiPartsInformation['nothi_masters_id']);
                if (empty($hasAccessinNothiMaster)) {
                    $this->Flash->error("দুঃখিত! বর্তমানে এই নোটে আপনার অনুমতি নাই। প্রয়োজনে আপনার অফিস এ্যাডমিনের সাথে যোগাযোগ করুন। (Err: NMCU04)");
                    return $this->redirect(['controller' => 'Dashboard', 'action' => 'dashboard']);
                } else {
                    $this->redirect(['_name' => 'nothiDetail', $hasAccessinNothiMaster['nothi_part_no'], $hasAccessinNothiMaster['nothi_office']]);
                }
            }

        }
//        else if($has_access['visited'] <1){
//              $this->Flash->error("দুঃখিত! কোন তথ্য পাওয়া যায় নি। ");
//                    $this->redirect(['controller' => 'Dashboard', 'action' => 'dashboard']);
//        }
        $officeUnitTable = TableRegistry::get('OfficeUnits');
        $officeunit = $officeUnitTable->get($nothiPartsInformation['office_units_id']);

        $nothiUnit = $officeunit->unit_name_bng;

        $this->set('nothiUnit', $nothiUnit);

        $allNothiMasterList = $nothiMasterPermissionsTable->getMasterNothiPartList($nothi_office,
            $employee_office['office_id'], $employee_office['office_unit_id'],
            $employee_office['office_unit_organogram_id'],
            $nothiPartsInformation['nothi_masters_id']);

        /**
         * Only visited nothi will be viewed to user
         */
        /* $permitted_nothi_list = $nothiMasterPermissionsTable->allNothiPermissionList($nothi_office,
             $employee_office['office_id'], $employee_office['office_unit_id'],
             $employee_office['office_unit_organogram_id'], 1);*/

        $allNothiParts = $a = array();

        $allNothiParts = $nothiPartsTable->find()->select(['id', 'nothi_part_no_bn',
            'NothiNotes.subject'])
            ->join([
                'NothiNotes' => [
                    'table' => 'nothi_notes',
                    'type' => 'left',
                    'conditions' => "NothiNotes.nothi_part_no = NothiParts.id AND NothiNotes.note_no=0"
                ]
            ])
            ->where(['NothiParts.nothi_masters_id' => $nothiPartsInformation['nothi_masters_id'],
                'NothiParts.id IN' => array_map(function ($val) {
                    return $val['nothi_part_no'];
                }, $allNothiMasterList)])->order(['NothiParts.id  ASC'])->toArray();


        $nothiMasterPriviligeType = array();

        if (!empty($allNothiParts)) {
            foreach ($allNothiParts as $key => $nothiMasters) {

                $nothiMastersCurrentUser = $nothiMasterCurrentUsersTable->find()->where(['nothi_part_no' => $nothiMasters['id'],
                    'office_id' => $employee_office['office_id'], 'office_unit_id' => $employee_office['office_unit_id'],
                    'office_unit_organogram_id' => $employee_office['office_unit_organogram_id'],
                    'nothi_office' => $nothi_office])->first();
                if (!empty($nothiMastersCurrentUser)) {
                    $nothiMasterPriviligeType[$nothiMasters['id']] = 1;
                }
                if (!empty($nothiMastersCurrentUser)) {
                    $nothiMasters['is_finished'] = $nothiMastersCurrentUser['is_finished'];
                } else {
                    $nothiMasters['is_finished'] = 0;
                }
                if ($part_id == $nothiMastersCurrentUser['nothi_part_no'] && !empty($nothiMastersCurrentUser['priority']) && $nothiMastersCurrentUser['priority'] == 1) {

                    $this->set('note_priority', $nothiMastersCurrentUser['priority']);
                    $this->set('note_priority_text', 'নোটের অগ্রাধিকারঃ ' . (($nothiMastersCurrentUser['priority'] == 1) ? 'জরুরি' : 'সাধারণ'));
                }
                if (!empty($nothiMastersCurrentUser) && $nothiMastersCurrentUser['view_status']
                    == 0
                ) {
                    $nothiMastersCurrentUser->view_status = 1;
                    $nothiMasterCurrentUsersTable->save($nothiMastersCurrentUser);
                }
            }
        }


        #### nothi archive age detection
        //dd($nothiRecord[0]['nothi_masters_id']);
        $archive = getNothiArchiveStatus($nothiPartsInformation['nothi_masters_id'], $nothiPartsInformation['office_id']);
        $this->set('archive', $archive);
        #### nothi archive age detection

        $this->set(compact('allNothiParts'));
        $this->set(compact('nothiMasterPriviligeType'));
        $this->set('nothiRecord', $nothiPartsInformation);
        $this->set('nothi_office', $nothi_office);
    }

    public function nothiDetailsAllNew($part_id, $nothi_office = 0)
    {
        //$this->view = 'nothi_details_new_all';
        $employee_office = $this->getCurrentDakSection();
        $otherNothi = false;
        if ($nothi_office != 0 && $nothi_office != $employee_office['office_id']) {
            $otherNothi = true;

            $this->switchOffice($nothi_office, 'MainNothiOffice');
        } else {
            $nothi_office = $employee_office['office_id'];
        }

        $this->set('nothi_office', $nothi_office);
        $this->set(compact('otherNothi','part_id'));

        $nothiMasterPermissionsTable = TableRegistry::get("NothiMasterPermissions");
        $nothiMasterCurrentUsersTable = TableRegistry::get("NothiMasterCurrentUsers");

        TableRegistry::remove('NothiParts');
        $nothiPartsTable = TableRegistry::get("NothiParts");


        $nothiMastersCurrentUser = array();

        $nothiPartsInformation = $nothiPartsTable->getAll(['NothiParts.id' => $part_id])->first();
        /**
         * Has access in this nothi part
         * if not visited redirect to dashboard
         */
        $has_access = $nothiMasterPermissionsTable->hasAccessPartList($employee_office['office_id'],
            $employee_office['office_unit_id'],
            $employee_office['office_unit_organogram_id'], $part_id,
            $nothi_office);
        if (empty($has_access)) {
            /**
             * Last Nothi part does not have permission but one of the Nothi part of this nothi has permission
             * let's find that nothi
             */
            if (!empty($nothiPartsInformation['nothi_masters_id'])) {
                $hasAccessinNothiMaster = $nothiMasterPermissionsTable->hasAccess($nothi_office, $employee_office['office_id'],
                    $employee_office['office_unit_id'],
                    $employee_office['office_unit_organogram_id'], $nothiPartsInformation['nothi_masters_id']);
                if (empty($hasAccessinNothiMaster)) {
                    $this->Flash->error("দুঃখিত! বর্তমানে এই নোটে আপনার অনুমতি নাই। প্রয়োজনে আপনার অফিস এ্যাডমিনের সাথে যোগাযোগ করুন। (Err: NMCU05)");
                    return $this->redirect(['controller' => 'Dashboard', 'action' => 'dashboard']);
                } else {
                    $this->redirect(['_name' => 'nothiDetail', $hasAccessinNothiMaster['nothi_part_no'], $hasAccessinNothiMaster['nothi_office']]);
                }
            }

        }
//        else if($has_access['visited'] <1){
//              $this->Flash->error("দুঃখিত! কোন তথ্য পাওয়া যায় নি। ");
//                    $this->redirect(['controller' => 'Dashboard', 'action' => 'dashboard']);
//        }
        $officeUnitTable = TableRegistry::get('OfficeUnits');
        $officeunit = $officeUnitTable->get($nothiPartsInformation['office_units_id']);

        $nothiUnit = $officeunit->unit_name_bng;

        $this->set('nothiUnit', $nothiUnit);

        $allNothiMasterList = $nothiMasterPermissionsTable->getMasterNothiPartList($nothi_office,
            $employee_office['office_id'], $employee_office['office_unit_id'],
            $employee_office['office_unit_organogram_id'],
            $nothiPartsInformation['nothi_masters_id']);

        /**
         * Only visited nothi will be viewed to user
         */
        /* $permitted_nothi_list = $nothiMasterPermissionsTable->allNothiPermissionList($nothi_office,
             $employee_office['office_id'], $employee_office['office_unit_id'],
             $employee_office['office_unit_organogram_id'], 1);*/

        $allNothiParts = $a = array();

        $allNothiParts = $nothiPartsTable->find()->select(['id', 'nothi_part_no_bn', 'NothiParts.office_id',
            'NothiNotes.subject'])
            ->join([
                'NothiNotes' => [
                    'table' => 'nothi_notes',
                    'type' => 'left',
                    'conditions' => "NothiNotes.nothi_part_no = NothiParts.id AND NothiNotes.note_no=0"
                ]
            ])
            ->where(['NothiParts.nothi_masters_id' => $nothiPartsInformation['nothi_masters_id'],
                'NothiParts.id IN' => array_map(function ($val) {
                    return $val['nothi_part_no'];
                }, $allNothiMasterList)])->order(['NothiParts.nothi_part_no  ASC']);

        $total_part = $allNothiParts->count();
        $allNothiParts = $allNothiParts->limit($this->load_part_at_a_time)->toArray();

        $permitted_nothi_part_no = array_keys(groupByField($allNothiMasterList, 'nothi_part_no'));

        $pending_note = $nothiMasterCurrentUsersTable->find('list', ['keyField'=>'nothi_part_no','valueField'=>'nothi_part_no'])->where(['is_finished !=' => 1, 'nothi_master_id' => $nothiPartsInformation['nothi_masters_id'], 'nothi_part_no IN' => $permitted_nothi_part_no, 'office_unit_organogram_id' => $employee_office['office_unit_organogram_id']])->toArray();
        $this->set('pending_note', $pending_note);

        $this->set('permitted_nothi_list', $permitted_nothi_part_no);
        $this->set('total_part', $total_part);


        $nothiMasterPriviligeType = array();

        if (!empty($allNothiParts)) {
            foreach ($allNothiParts as $key => $nothiMasters) {

                $nothiMastersCurrentUser = $nothiMasterCurrentUsersTable->find()->where(['nothi_part_no' => $nothiMasters['id'],
                    'office_id' => $employee_office['office_id'], 'office_unit_id' => $employee_office['office_unit_id'],
                    'office_unit_organogram_id' => $employee_office['office_unit_organogram_id'],
                    'nothi_office' => $nothi_office])->first();
                if (!empty($nothiMastersCurrentUser)) {
                    $nothiMasterPriviligeType[$nothiMasters['id']] = 1;
                }
                if (!empty($nothiMastersCurrentUser)) {
                    $nothiMasters['is_finished'] = $nothiMastersCurrentUser['is_finished'];
                } else {
                    $nothiMasters['is_finished'] = 0;
                }
                if ($part_id == $nothiMastersCurrentUser['nothi_part_no'] && !empty($nothiMastersCurrentUser['priority']) && $nothiMastersCurrentUser['priority'] == 1) {

                    $this->set('note_priority', $nothiMastersCurrentUser['priority']);
                    $this->set('note_priority_text', 'নোটের অগ্রাধিকারঃ ' . (($nothiMastersCurrentUser['priority'] == 1) ? 'জরুরি' : 'সাধারণ'));
                }
                if (!empty($nothiMastersCurrentUser) && $nothiMastersCurrentUser['view_status']
                    == 0
                ) {
                    $nothiMastersCurrentUser->view_status = 1;
                    $nothiMasterCurrentUsersTable->save($nothiMastersCurrentUser);
                }
            }
        }


        #### nothi archive age detection
        //dd($nothiRecord[0]['nothi_masters_id']);
        $archive = getNothiArchiveStatus($nothiPartsInformation['nothi_masters_id'], $nothiPartsInformation['office_id']);
        $this->set('archive', $archive);
        #### nothi archive age detection

        $this->set(compact('allNothiParts'));
        $this->set(compact('nothiMasterPriviligeType'));
        $this->set('nothiRecord', $nothiPartsInformation);
        $this->set('nothi_office', $nothi_office);
        $this->set('noteNos', (new NothiNoteSheetsController())->showNoteList($nothiPartsInformation['nothi_masters_id'], $nothi_office, $employee_office));

    }

    public function onuccedList($nothiMasterId, $nothi_office = 0, $user_designation = 0)
    {
        $this->layout = null;
        $otherNothi = false;

        if (empty($user_designation)) {
            $employee_office = $this->getCurrentDakSection();
        } else {
            $EmployeeOfficesTable = TableRegistry::get('EmployeeOffices');
            $employee_office = $EmployeeOfficesTable->find()->where(['office_unit_organogram_id' => $user_designation,
                'status' => 1])->first();
        }

        if ($nothi_office != 0 && $nothi_office != $employee_office['office_id']) {
            $otherNothi = true;
        } else {
            $nothi_office = $employee_office['office_id'];
        }
        $this->switchOffice($nothi_office, 'MainNothiOffice');

        $this->set('nothi_office', $nothi_office);
        $this->set(compact('otherNothi'));

        TableRegistry::remove('NothiNotes');
        TableRegistry::remove('NothiParts');
        $nothiNoteTable = TableRegistry::get('NothiNotes');

        $nothiPartTable = TableRegistry::get('NothiParts');

        $nothiInformation = $nothiPartTable->get($nothiMasterId);


        $nothiMastersPermissionTable = TableRegistry::get('NothiMasterPermissions');
        $permitted_nothi_list = $nothiMastersPermissionTable->allNothiPermissionList($nothi_office,
            $employee_office['office_id'], $employee_office['office_unit_id'],
            $employee_office['office_unit_organogram_id']);

        $noteList = $nothiNoteTable->find()
            ->select(['NothiNotes.id', 'NothiNotes.nothi_master_id', 'NothiNotes.nothi_part_no',
                'nothi_part_no_en' => 'NothiParts.nothi_part_no', 'nothi_part_no_bn' => 'NothiParts.nothi_part_no_bn',
                'NothiNotes.nothi_notesheet_id', 'NothiNotes.note_no'])
            ->where(['NothiNotes.nothi_master_id' => $nothiInformation['nothi_masters_id'],
                'note_no >= ' => 0])
            ->join([
                'NothiParts' => [
                    'table' => 'nothi_parts',
                    "conditions" => "NothiParts.id= NothiNotes.nothi_part_no",
                    "type" => "INNER"
                ]
            ])
            ->where(['NothiNotes.nothi_part_no IN' => $permitted_nothi_list])
            ->order(['nothi_part_no_en asc,cast(NothiNotes.note_no as signed) asc'])->toArray();


        $noteListArray = array();

        if (!empty($noteList)) {
            foreach ($noteList as $key => $value) {
                $noteListArray[] = array(
                    'id' => $value->id,
                    'nothi_notesheet_id' => $value->nothi_notesheet_id,
                    'nothi_part_no' => $value->nothi_part_no,
                    'nothi_part_no_bn' => $value->nothi_part_no_bn,
                    'note_no' => $value->nothi_part_no_bn . '.' . Number::format($value->note_no)
                );
            }
        }

        echo json_encode($noteListArray);
        die;
    }

    public function potakaList($nothiMasterId, $nothi_office = 0)
    {
        $this->layout = null;
        $employee_office = $this->getCurrentDakSection();

        if ($nothi_office != 0 && $nothi_office != $employee_office['office_id']) {

            $this->switchOffice($nothi_office, 'MainNothiOffice');
        } else {
            $nothi_office = $employee_office['office_id'];
        }

        TableRegistry::remove('NothiParts');
        TableRegistry::remove('PotroFlags');
        $potroFlagTable = TableRegistry::get('PotroFlags');
        $nothiPartTable = TableRegistry::get('NothiParts');
        /*
                $nothiInformation = $nothiPartTable->get($nothiMasterId);

                $nothiMasterPermissionsTable = TableRegistry::get("NothiMasterPermissions");
                $permitted_nothi_list = array();
                $permitted_nothi_list = $nothiMasterPermissionsTable->allNothiPermissionList($nothi_office,
                    $employee_office['office_id'], $employee_office['office_unit_id'],
                    $employee_office['office_unit_organogram_id'], 1);*/


        $potakaList = $potroFlagTable->find()
            ->select([
                'PotroFlags.id',
                'PotroFlags.nothi_master_id',
                'PotroFlags.nothi_part_no',
                'PotroFlags.color',
                'PotroFlags.title',
                'PotroFlags.page_no',
                'PotroFlags.potro_attachment_id',
                "nothi_potro_page_bn" => 'NothiPotroAttachments.nothi_potro_page_bn',
                "nothi_potro_page" => 'NothiPotroAttachments.nothi_potro_page'
            ])
            //->where(['PotroFlags.nothi_master_id' => $nothiInformation['nothi_masters_id']])
            ->where(['PotroFlags.nothi_part_no' => $nothiMasterId])
            //->where(["PotroFlags.nothi_part_no IN" => $permitted_nothi_list])
            ->join([
                'NothiPotroAttachments' => [
                    'table' => 'nothi_potro_attachments',
                    'type' => 'inner',
                    'conditions' => "NothiPotroAttachments.id = PotroFlags.potro_attachment_id"
                ]
            ])
            ->order(['PotroFlags.nothi_part_no asc'])
            ->toArray();


        $potroListArray = array();

        if (!empty($potakaList)) {
            foreach ($potakaList as $key => $value) {
                $potroListArray[] = array(
                    'id' => $value->id,
                    'nothi_master_id' => $value->nothi_master_id,
                    'nothi_part_no' => $value->nothi_part_no,
                    'title' => $value->title,
                    'attachment_id' => $value->potro_attachment_id,
                    'page_no' => $value->page_no,
                );
            }
        }

        echo json_encode($potroListArray);
        die;
    }

    public function potroPage($nothiMasterId, $nothi_office = 0)
    {
        $this->layout = null;
        $employee_office = $this->getCurrentDakSection();
        $this->set(compact('employee_office'));
        $otherNothi = false;
        if ($nothi_office != 0 && $nothi_office != $employee_office['office_id']) {
            $otherNothi = true;
            $this->switchOffice($nothi_office, 'MainNothiOffice');
        } else {
            $nothi_office = $employee_office['office_id'];
        }

        $this->set(compact('employee_office'));
        $this->set(compact('nothi_office'));
        $this->set(compact('otherNothi'));
        $this->set('id', $nothiMasterId);
        $nothiMasterPermissionsTable = TableRegistry::get("NothiMasterPermissions");
        $nothiMasterCurrentUsersTable = TableRegistry::get("NothiMasterCurrentUsers");
        TableRegistry::remove('NothiPotroAttachments');
        TableRegistry::remove('Potrojari');
        TableRegistry::remove('NothiParts');
        TableRegistry::remove('SummaryNothiUsers');
        $potroAttachmentTable = TableRegistry::get('NothiPotroAttachments');
        $potrojariTable = TableRegistry::get('Potrojari');
        $nothiPartsTable = TableRegistry::get('NothiParts');
        $nothiPartsInformation = $nothiPartsTable->get($nothiMasterId);

        $archive_level = getNothiArchiveStatus($nothiPartsInformation['nothi_masters_id'], $nothiPartsInformation['office_id']);
        $this->set('archive', $archive_level);

        $total = $potroAttachmentTable->find()->where(['nothi_part_no' => $nothiMasterId,
            '(is_summary_nothi IS NULL OR is_summary_nothi = 0)', 'nothijato' => 0, 'status' => 1])->count();
        $permitted_nothi_list = $nothiMasterPermissionsTable->allNothiPermissionList($nothi_office,
            $employee_office['office_id'], $employee_office['office_unit_id'],
            $employee_office['office_unit_organogram_id']);

        $potroAttachmentRecord = $potroAttachmentTable->find()
            ->where(['NothiPotroAttachments.nothi_part_no' => $nothiMasterId,
                '(NothiPotroAttachments.is_summary_nothi IS NULL OR NothiPotroAttachments.is_summary_nothi = 0)', 'NothiPotroAttachments.nothijato' => 0,
                'NothiPotroAttachments.status' => 1])
            ->contain(['PotrojariData', 'NothiPotros'])
            ->order(['NothiPotroAttachments.nothi_potro_id desc', 'NothiPotroAttachments.is_main desc'])->page(1,
                5)->toArray();

        $potroAttachments = array();
        if (!empty($potroAttachmentRecord)) {
            foreach ($potroAttachmentRecord as $ke => $val) {
                if(empty($val['potrojari_id']))continue;
                if(!in_array($val['potrojari_id'],$potroAttachments)) {
                    $array = $potroAttachmentTable->find()->where(['potrojari_id' => $val['potrojari_id']])->hydrate(false)->toArray();
                    if(!empty($array)){
                        $potroAttachments[$val['potrojari_id']] = $array;
                    }
                }
            }
        }

        $this->set(compact('potroAttachments'));
        $potroAllAttachmentRecord = $potroAttachmentTable->find()
            ->join([
                'NothiPotros' => [
                    'table' => 'nothi_potros',
                    'type' => 'INNER',
                    'conditions' => 'NothiPotros.id = NothiPotroAttachments.nothi_potro_id'
                ]
            ])
            ->where(['NothiPotroAttachments.nothi_master_id' => $nothiPartsInformation['nothi_masters_id']])
            ->where(['NothiPotroAttachments.nothijato' => 0, 'NothiPotroAttachments.status' => 1])
            ->where(['NothiPotroAttachments.nothi_part_no IN' => $permitted_nothi_list])->count();
        //real nothi_master_id of this part
        $nothimastersid = $nothiPartsInformation['nothi_masters_id'];
        $this->set(compact('potroAllAttachmentRecord'));
        $this->set(compact('potroAttachmentRecord'));
        $this->set(compact('total'));
        $this->set(compact('nothimastersid'));
        $potroNothijatoAttachmentRecord = $potroAttachmentTable->find()->where(['nothi_part_no' => $nothiMasterId,
            '(is_summary_nothi IS NULL OR is_summary_nothi = 0)', 'nothijato' => 1, 'status' => 1])->count();
        $this->set(compact('potroNothijatoAttachmentRecord'));
		$draftPotroCount = $potrojariTable->find()->where(['potro_status' => 'Draft',
			'nothi_part_no' => $nothiMasterId])->count();
        $draftPotro = $potrojariTable->find()->where(['potro_status' => 'Draft',
            'nothi_part_no' => $nothiMasterId])->order(['id desc'])->page(1, 5)->toArray();
        $potrojariAttachmentsTable = TableRegistry::get('PotrojariAttachments');
        $potrojariAttachments = $potrojariAttachmentsTable->find()->select(['attachment_type', 'user_file_name',
            'file_name', 'potrojari_id', 'id', 'sarok_no' => 'Potrojari.sarok_no', 'potro_id'])->join(['Potrojari' => ['table' => 'potrojari',
            'type' => 'inner', 'conditions' => 'Potrojari.id = PotrojariAttachments.potrojari_id']])->where(['Potrojari.potro_status' => 'Draft',
            'Potrojari.nothi_part_no' => $nothiMasterId])->order(['Potrojari.id desc'])->toArray();
        // Now text / html attachment will be also added
        // 'PotrojariAttachments.attachment_type <> ' => 'text' --omitted
        $draftPotroAttachments = array();
        if (!empty($potrojariAttachments)) {
            foreach ($potrojariAttachments as $ke => $val) {
                $draftPotroAttachments[$val['potrojari_id']][] = $val;
            }
        }

        $potro_receiver_list = array();
        foreach($draftPotro as $potro) {
            TableRegistry::remove('PotrojariReceiver');
            TableRegistry::remove('PotrojariOnulipi');
            $PotrojariReceiverTable = TableRegistry::get('PotrojariReceiver');
            $potro_receiver_list[$potro['id']]['receiver']['System'] = $PotrojariReceiverTable->find()->where(['potrojari_id' => $potro['id'], 'receiving_officer_designation_id !=' => 0])->toArray();
            $potro_receiver_list[$potro['id']]['receiver']['Other'] = $PotrojariReceiverTable->find()->where(['potrojari_id' => $potro['id'], 'receiving_officer_designation_id' => 0])->toArray();

            $PotrojariOnulipiTable = TableRegistry::get('PotrojariOnulipi');
            $potro_receiver_list[$potro['id']]['onulipi']['System'] = $PotrojariOnulipiTable->find()->where(['potrojari_id' => $potro['id'], 'receiving_officer_designation_id !=' => 0])->toArray();
            $potro_receiver_list[$potro['id']]['onulipi']['Other'] = $PotrojariOnulipiTable->find()->where(['potrojari_id' => $potro['id'], 'receiving_officer_designation_id' => 0])->toArray();
        }

        $this->set(compact('draftPotro', 'draftPotroAttachments', 'draftPotroCount', 'potro_receiver_list'));
        $draftSummary = $potrojariTable->find()->where(['potro_status' => 'SummaryDraft',
            'nothi_part_no' => $nothiMasterId, 'is_summary_nothi' => 1])->order(['id desc'])->first();
        $is_approve = 0;
        $summaryNothiTable = TableRegistry::get('SummaryNothiUsers');
        if (!empty($draftSummary)) {
            $totalapp = 0;
            $getSummaryUserInfo = $summaryNothiTable->find()->where(['potrojari_id' => $draftSummary['id'], 'current_office_id' => $employee_office['office_id']])->toArray();
            if (!empty($getSummaryUserInfo)) {
                foreach ($getSummaryUserInfo as $k => $v) {
                    if ($v['is_approve']) {
                        $totalapp++;
                    }
                }
            }

            if ($totalapp > 0 && $totalapp == count($getSummaryUserInfo)) {
                $is_approve = 1;
            }
        }
        $this->set('is_approve', $is_approve);
        $summaryAttachmentRecord = $potroAttachmentTable->find()->where(['nothi_part_no' => $nothiMasterId, 'is_summary_nothi' => 1])->order(['id desc'])->toArray();
        $this->set(compact('draftSummary'));
        $this->set(compact('summaryAttachmentRecord'));


        //get summary nothi relation

        $potrosummarynothi = $summaryNothiTable->getData(
            [
                'current_office_id' => $employee_office['office_id'],
                'nothi_part_no' => $nothiMasterId
            ])->toArray();

        $potrosummarystatus = [];
        $potrosummarydraftstatus = [];
        $summaryApprove = false;
        if (!empty($potrosummarynothi)) {
            foreach ($potrosummarynothi as $key => $pair) {

                $potrosummarystatus[$pair['potro_id']][] = [
                    'is_approve' => $pair['is_approve'],
                    'designation_id' => $pair['designation_id'],
                    'is_sent' => $pair['is_sent'],
                    'can_approve' => $pair['can_approve'],
                ];
                if ($pair['is_approve']) {
                    $summaryApprove = true;
                }
                if ($employee_office['office_unit_organogram_id'] == $pair['designation_id'] && $pair['is_sent'] == 0 && $pair['can_approve'] == 1) {
                    $potrosummarydraftstatus[$pair['potrojari_id']] = [
                        'is_approve' => $pair['is_approve'],
                        'designation_id' => $pair['designation_id'],
                        'is_sent' => $pair['is_sent'],
                        'can_approve' => $pair['can_approve'],
                    ];
                }
            }
        }

        $this->set('potroFlags', $this->showPotroFlag($nothiMasterId));
        $this->set('summaryApprove', $summaryApprove);
        $this->set('potrosummarystatus', $potrosummarystatus);
        $this->set('potrosummarydraftstatus', $potrosummarydraftstatus);
        $nothiMasters = array();
        $nothiMastersCurrentUser = array();
        $this->set('privilige_type', 0);
        if (!empty($employee_office)) {
            $nothiMasters = $nothiMasterPermissionsTable->hasAccess($nothi_office,
                $employee_office['office_id'],
                $employee_office['office_unit_id'],
                $employee_office['office_unit_organogram_id'],
                $nothiPartsInformation['nothi_masters_id'],
                $nothiPartsInformation['id']);
            $nothiMastersCurrentUser = $nothiMasterCurrentUsersTable->find()->where(['nothi_part_no' => $nothiMasterId,
                'office_id' => $employee_office['office_id'], 'office_unit_id' => $employee_office['office_unit_id'],
                'office_unit_organogram_id' => $employee_office['office_unit_organogram_id'],
                'nothi_office' => $nothi_office])->first();
            if (!empty($nothiMastersCurrentUser)) {
                $this->set('privilige_type', $nothiMasters['privilige_type']);
            } else {
                $this->set('privilige_type', 0);
            }
        }
        if ($nothiMastersCurrentUser['is_finished'] == 1 && $archive_level['level'] == 2) {
            $this->set('privilige_type', 0);
        }
    }
    public function loadNextPotroPage($nothiMasterId, $nothi_office = 0)
    {
		$this->layout = '';
		$page = $this->request->query('page');
        $employee_office = $this->getCurrentDakSection();
        $this->set(compact('employee_office'));
        $otherNothi = false;
        if ($nothi_office != 0 && $nothi_office != $employee_office['office_id']) {
            $otherNothi = true;
            $this->switchOffice($nothi_office, 'MainNothiOffice');
        } else {
            $nothi_office = $employee_office['office_id'];
        }

        $this->set(compact('employee_office'));
        $this->set(compact('nothi_office'));
        $this->set(compact('otherNothi'));
        $this->set('id', $nothiMasterId);
        $nothiMasterPermissionsTable = TableRegistry::get("NothiMasterPermissions");
        $nothiMasterCurrentUsersTable = TableRegistry::get("NothiMasterCurrentUsers");
        TableRegistry::remove('NothiPotroAttachments');
        TableRegistry::remove('Potrojari');
        TableRegistry::remove('NothiParts');
        TableRegistry::remove('SummaryNothiUsers');
        $potroAttachmentTable = TableRegistry::get('NothiPotroAttachments');
        $potrojariTable = TableRegistry::get('Potrojari');
        $nothiPartsTable = TableRegistry::get('NothiParts');
        $nothiPartsInformation = $nothiPartsTable->get($nothiMasterId);

        $archive_level = getNothiArchiveStatus($nothiPartsInformation['nothi_masters_id'], $nothiPartsInformation['office_id']);
        $this->set('archive', $archive_level);

        $total = $potroAttachmentTable->find()->where(['nothi_part_no' => $nothiMasterId,
            '(is_summary_nothi IS NULL OR is_summary_nothi = 0)', 'nothijato' => 0, 'status' => 1])->count();
        $permitted_nothi_list = $nothiMasterPermissionsTable->allNothiPermissionList($nothi_office,
            $employee_office['office_id'], $employee_office['office_unit_id'],
            $employee_office['office_unit_organogram_id']);

        $potroAttachmentRecord = $potroAttachmentTable->find()
            ->where(['NothiPotroAttachments.nothi_part_no' => $nothiMasterId,
                '(NothiPotroAttachments.is_summary_nothi IS NULL OR NothiPotroAttachments.is_summary_nothi = 0)', 'NothiPotroAttachments.nothijato' => 0,
                'NothiPotroAttachments.status' => 1])
            ->contain(['PotrojariData', 'NothiPotros'])
            ->order(['NothiPotroAttachments.id desc'])->page(1,
                5)->toArray();

        $potroAttachments = array();
        if (!empty($potroAttachmentRecord)) {
            foreach ($potroAttachmentRecord as $ke => $val) {
                if(empty($val['potrojari_id']))continue;
                if(!in_array($val['potrojari_id'],$potroAttachments)) {
                    $array = $potroAttachmentTable->find()->where(['potrojari_id' => $val['potrojari_id']])->hydrate(false)->toArray();
                    if(!empty($array)){
                        $potroAttachments[$val['potrojari_id']] = $array;
                    }
                }
            }
        }

        $this->set(compact('potroAttachments'));
        $potroAllAttachmentRecord = $potroAttachmentTable->find()
            ->join([
                'NothiPotros' => [
                    'table' => 'nothi_potros',
                    'type' => 'INNER',
                    'conditions' => 'NothiPotros.id = NothiPotroAttachments.nothi_potro_id'
                ]
            ])
            ->where(['NothiPotroAttachments.nothi_master_id' => $nothiPartsInformation['nothi_masters_id']])
            ->where(['NothiPotroAttachments.nothijato' => 0, 'NothiPotroAttachments.status' => 1])
            ->where(['NothiPotroAttachments.nothi_part_no IN' => $permitted_nothi_list])->count();
        //real nothi_master_id of this part
        $nothimastersid = $nothiPartsInformation['nothi_masters_id'];
        $this->set(compact('potroAllAttachmentRecord'));
        $this->set(compact('potroAttachmentRecord'));
        $this->set(compact('total'));
        $this->set(compact('nothimastersid'));
        $potroNothijatoAttachmentRecord = $potroAttachmentTable->find()->where(['nothi_part_no' => $nothiMasterId,
            '(is_summary_nothi IS NULL OR is_summary_nothi = 0)', 'nothijato' => 1, 'status' => 1])->count();
        $this->set(compact('potroNothijatoAttachmentRecord'));
        $draftPotro = $potrojariTable->find()->where(['potro_status' => 'Draft',
            'nothi_part_no' => $nothiMasterId])->order(['id desc'])->page($page, 5)->toArray();
        $potrojariAttachmentsTable = TableRegistry::get('PotrojariAttachments');
        $potrojariAttachments = $potrojariAttachmentsTable->find()->select(['attachment_type', 'user_file_name',
            'file_name', 'potrojari_id', 'id', 'sarok_no' => 'Potrojari.sarok_no', 'potro_id'])->join(['Potrojari' => ['table' => 'potrojari',
            'type' => 'inner', 'conditions' => 'Potrojari.id = PotrojariAttachments.potrojari_id']])->where(['Potrojari.potro_status' => 'Draft',
            'Potrojari.nothi_part_no' => $nothiMasterId])->order(['Potrojari.id desc'])->toArray();
        // Now text / html attachment will be also added
        // 'PotrojariAttachments.attachment_type <> ' => 'text' --omitted
        $draftPotroAttachments = array();
        if (!empty($potrojariAttachments)) {
            foreach ($potrojariAttachments as $ke => $val) {
                $draftPotroAttachments[$val['potrojari_id']][] = $val;
            }
        }
        $this->set(compact('draftPotro', 'draftPotroAttachments'));
        $draftSummary = $potrojariTable->find()->where(['potro_status' => 'SummaryDraft',
            'nothi_part_no' => $nothiMasterId, 'is_summary_nothi' => 1])->order(['id desc'])->first();
        $is_approve = 0;
        $summaryNothiTable = TableRegistry::get('SummaryNothiUsers');
        if (!empty($draftSummary)) {
            $totalapp = 0;
            $getSummaryUserInfo = $summaryNothiTable->find()->where(['potrojari_id' => $draftSummary['id'], 'current_office_id' => $employee_office['office_id']])->toArray();
            if (!empty($getSummaryUserInfo)) {
                foreach ($getSummaryUserInfo as $k => $v) {
                    if ($v['is_approve']) {
                        $totalapp++;
                    }
                }
            }

            if ($totalapp > 0 && $totalapp == count($getSummaryUserInfo)) {
                $is_approve = 1;
            }
        }
        $this->set('is_approve', $is_approve);
        $summaryAttachmentRecord = $potroAttachmentTable->find()->where(['nothi_part_no' => $nothiMasterId, 'is_summary_nothi' => 1])->order(['id desc'])->toArray();
        $this->set(compact('draftSummary'));
        $this->set(compact('summaryAttachmentRecord'));


        //get summary nothi relation

        $potrosummarynothi = $summaryNothiTable->getData(
            [
                'current_office_id' => $employee_office['office_id'],
                'nothi_part_no' => $nothiMasterId
            ])->toArray();

        $potrosummarystatus = [];
        $potrosummarydraftstatus = [];
        $summaryApprove = false;
        if (!empty($potrosummarynothi)) {
            foreach ($potrosummarynothi as $key => $pair) {

                $potrosummarystatus[$pair['potro_id']][] = [
                    'is_approve' => $pair['is_approve'],
                    'designation_id' => $pair['designation_id'],
                    'is_sent' => $pair['is_sent'],
                    'can_approve' => $pair['can_approve'],
                ];
                if ($pair['is_approve']) {
                    $summaryApprove = true;
                }
                if ($employee_office['office_unit_organogram_id'] == $pair['designation_id'] && $pair['is_sent'] == 0 && $pair['can_approve'] == 1) {
                    $potrosummarydraftstatus[$pair['potrojari_id']] = [
                        'is_approve' => $pair['is_approve'],
                        'designation_id' => $pair['designation_id'],
                        'is_sent' => $pair['is_sent'],
                        'can_approve' => $pair['can_approve'],
                    ];
                }
            }
        }

        $this->set('potroFlags', $this->showPotroFlag($nothiMasterId));
        $this->set('summaryApprove', $summaryApprove);
        $this->set('potrosummarystatus', $potrosummarystatus);
        $this->set('potrosummarydraftstatus', $potrosummarydraftstatus);
        $nothiMasters = array();
        $nothiMastersCurrentUser = array();
        $this->set('privilige_type', 0);
        if (!empty($employee_office)) {
            $nothiMasters = $nothiMasterPermissionsTable->hasAccess($nothi_office,
                $employee_office['office_id'],
                $employee_office['office_unit_id'],
                $employee_office['office_unit_organogram_id'],
                $nothiPartsInformation['nothi_masters_id'],
                $nothiPartsInformation['id']);
            $nothiMastersCurrentUser = $nothiMasterCurrentUsersTable->find()->where(['nothi_part_no' => $nothiMasterId,
                'office_id' => $employee_office['office_id'], 'office_unit_id' => $employee_office['office_unit_id'],
                'office_unit_organogram_id' => $employee_office['office_unit_organogram_id'],
                'nothi_office' => $nothi_office])->first();
            if (!empty($nothiMastersCurrentUser)) {
                $this->set('privilige_type', $nothiMasters['privilige_type']);
            } else {
                $this->set('privilige_type', 0);
            }
        }
        if ($nothiMastersCurrentUser['is_finished'] == 1 && $archive_level['level'] == 2) {
            $this->set('privilige_type', 0);
        }
    }

    public function potroPageAll($part_id, $nothi_office = 0)
    {

        $this->layout = null;
        $employee_office = $this->getCurrentDakSection();
        $otherNothi = false;
        if ($nothi_office != 0 && $nothi_office != $employee_office['office_id']) {
            $otherNothi = true;

            $this->switchOffice($nothi_office, 'MainNothiOffice');
        } else {
            $nothi_office = $employee_office['office_id'];
        }

        $this->set('otherNothi', $otherNothi);
        $this->set('nothi_office', $nothi_office);

        $nothiMasterPermissionsTable = TableRegistry::get("NothiMasterPermissions");
        $nothiMasterMovementsTable = TableRegistry::get("NothiMasterMovements");

        TableRegistry::remove("NothiParts");
        TableRegistry::remove("NothiPotros");
        $nothiPartsTable = TableRegistry::get("NothiParts");
        $nothiPotroTable = TableRegistry::get("NothiPotros");

        $nothiMastersCurrentUser = array();
        $potroAttachmentRecord = array();

        $nothiPartsInformation = $nothiPartsTable->getAll(['NothiParts.id' => $part_id])->first();
        //real nothi_master_id of this part
        $nothimastersid = $nothiPartsInformation['nothi_masters_id'];
        $this->set(compact('nothimastersid'));
        $this->set('part_id', $part_id);

        $allNothiMasterList = $nothiMasterPermissionsTable->getMasterNothiPartList($nothi_office,
            $employee_office['office_id'], $employee_office['office_unit_id'],
            $employee_office['office_unit_organogram_id'],
            $nothiPartsInformation['nothi_masters_id']);

        $nothiPartId = $draftSummary = $summaryAttachmentRecord
            = array();

        $this->set(compact('draftSummary'));
        $this->set(compact('summaryAttachmentRecord'));

        if (!empty($allNothiMasterList)) {
            foreach ($allNothiMasterList as $key => $value) {
                $nothiPartId[] = $value['nothi_part_no'];
            }
        }

        TableRegistry::remove('NothiPotroAttachments');
        TableRegistry::remove('Potrojari');
        $potroAttachmentTable = TableRegistry::get('NothiPotroAttachments');
        $potrojariTable = TableRegistry::get('Potrojari');


        $total = $potroAttachmentTable->find()->where(['nothi_part_no IN' => $nothiPartId,
            'nothijato' => 0, 'status' => 1])->count();

        $potroAttachmentRecord = $potroAttachmentTable->find()->where(['NothiPotroAttachments.nothi_part_no IN' => $nothiPartId,
            'NothiPotroAttachments.nothijato' => 0, 'NothiPotroAttachments.status' => 1])->contain(['PotrojariData', 'NothiPotros'])->order(['NothiPotroAttachments.id desc'])->page(1,
            10)->toArray();

        $this->set(compact('potroAttachmentRecord'));
        $this->set(compact('total'));

        $potroNothijatoAttachmentRecord = $potroAttachmentTable->find()->where(['nothi_part_no IN' => $nothiPartId,
            '(is_summary_nothi IS NULL OR is_summary_nothi = 0)', 'nothijato' => 1, 'status' => 1])->order(['id desc'])->toArray();

        $this->set(compact('potroNothijatoAttachmentRecord'));

        $draftPotro = $potrojariTable->find()->where(['potro_status' => 'Draft',
            'nothi_part_no IN' => $nothiPartId])->order(['id desc'])->toArray();

        TableRegistry::remove('PotrojariAttachments');
        $potrojariAttachmentsTable = TableRegistry::get('PotrojariAttachments');
        $potrojariAttachments = $potrojariAttachmentsTable->find()->select(['attachment_type',
            'file_name', 'potrojari_id', 'id'])->join(['Potrojari' => ['table' => 'potrojari',
            'type' => 'inner', 'conditions' => 'Potrojari.id = PotrojariAttachments.potrojari_id']])->where(['Potrojari.potro_status' => 'Draft',
            'Potrojari.nothi_part_no IN' => $nothiPartId, 'PotrojariAttachments.attachment_type <> ' => 'text'])->order(['Potrojari.id desc'])->toArray();


        $draftPotroAttachments = array();
        if (!empty($potrojariAttachments)) {
            foreach ($potrojariAttachments as $ke => $val) {
                $draftPotroAttachments[$val['potrojari_id']][] = $val;
            }
        }

        $this->set(compact('draftPotro', 'draftPotroAttachments'));

        $this->set('potroFlags', $this->showPotroFlagAll($nothiPartId));
        $this->set('nothi_office', $nothi_office);
    }

    public function masterFile()
    {

        $this->layout = null;
        $employee_office = $this->getCurrentDakSection();

        $OfficesTable = TableRegistry::get('Offices');
        $office_info = $OfficesTable->get($employee_office['office_id']);

        $OfficeLayersTable = TableRegistry::get('OfficeLayers');
        $office_layer_info = $OfficeLayersTable->get($office_info['office_layer_id']);
        $this->set('layer_level', $office_layer_info['layer_level']);


        $potrojariId = [];
        $OfficeDomainTable = TableRegistry::get('OfficeDomains');
        if ($office_layer_info['layer_level'] == 1 || $office_layer_info['layer_level'] == 2 || $office_layer_info['layer_level'] == 3) {
            $office_domain = $OfficeDomainTable->getNothiReplacedPortalDomainbyOffice($employee_office['office_id']);
        } else {
            $office_domain = $OfficeDomainTable->getPortalDomainByFieldOffice($employee_office['office_id']);
        }
        $this->set('office_domain', $office_domain);
        $potrojariTable = TableRegistry::get('Potrojari');
        $unitTable = TableRegistry::get("OfficeUnits");
        $nothi_office = $employee_office['office_id'];

        $this->set('nothi_office', $nothi_office);

        $allPotrojari = $potrojariTable->find()
            ->select([
                'Potrojari.id',
                'Potrojari.nothi_master_id',
                'Potrojari.nothi_part_no',
                'Potrojari.potro_subject',
                'Potrojari.potrojari_date',
                'Potrojari.sarok_no',
                'Potrojari.potro_type',
                'Potrojari.potro_description',
                'Potrojari.potro_cover',
                'Potrojari.attached_potro_id',
                'Potrojari.attached_potro',
                'Potrojari.is_summary_nothi',
                'Potrojari.meta_data',

                'NothiParts.subject',
                'NothiParts.nothi_no',
                'NothiParts.office_units_id',

                'NothiTypes.type_name',
            ])
            ->where([
                'Potrojari.potro_status' => 'Sent',
                'Potrojari.potrojari_draft_office_id' => $employee_office['office_id'],
                'NothiMasterPermissions.office_unit_organograms_id' => $employee_office['office_unit_organogram_id'],
                'NothiMasterPermissions.nothi_office' => $employee_office['office_id'],
            ])->join([
                'NothiParts' => [
                    'table' => 'nothi_parts',
                    'type' => 'INNER',
                    'conditions' => 'NothiParts.id = Potrojari.nothi_part_no'
                ],
                'NothiTypes' => [
                    'table' => 'nothi_types',
                    'type' => 'inner',
                    'conditions' => 'NothiTypes.id = NothiParts.nothi_types_id'
                ],
                'NothiMasterPermissions' => [
                    'table' => 'nothi_master_permissions',
                    'type' => 'INNER',
                    'conditions' => 'NothiParts.office_id = NothiMasterPermissions.nothi_office AND 
                    NothiParts.nothi_masters_id = NothiMasterPermissions.nothi_masters_id AND 
                    NothiParts.id = NothiMasterPermissions.nothi_part_no and 
                    NothiMasterPermissions.office_unit_organograms_id =' . $employee_office['office_unit_organogram_id'] . ' 
                    AND NothiMasterPermissions.nothi_office = ' . $employee_office['office_id']
                ]
            ])->order(['Potrojari.potrojari_date' => 'asc']);

         
            
        if (!empty($this->request->query['potrojari_template'])) {
            $potrojari_template = $this->request->query['potrojari_template'];
                $allPotrojari = $allPotrojari->where(["OR" => ['Potrojari.potro_type like' => $potrojari_template 
                ]]);
            }

         
        if (!empty($this->request->query['start_date'])) {
            $start_date = $this->request->query['start_date'];
            $end_date = $this->request->query['end_date'];
                $allPotrojari = $allPotrojari->where(["AND" => [' date(Potrojari.potrojari_date) >= ' => $start_date, ' date(Potrojari.potrojari_date) <= '=>$end_date 
                ]]);
               
            }

        if (!empty($this->request->query['search'])) {
            $search = $this->request->query['search'];
            $this->set(compact('search'));
             $allPotrojari = $allPotrojari->where(["OR" => ['NothiParts.nothi_no like' => '%' . $search . '%',
                'NothiParts.nothi_no like' => '%' . $search . '%',
                'NothiParts.subject like' => '%' . $search . '%',
                'Potrojari.sarok_no LIKE' => '%' . $search . '%',
                'Potrojari.potro_subject LIKE' => '%' . $search . '%'
            ]]);
        }
        $potrojariarray = $allPotrojari->count();

        $allUnits = $unitTable->getOfficeUnitsList($employee_office['office_id']);
        $this->set(compact('allUnits'));
        $paginate = array('limit' => 1, 'page' => $potrojariarray);

        $this->paginate = $paginate;

        try {
            $potro = $this->paginate($allPotrojari);
        } catch (NotFoundException $e) {
            return $this->redirect(['action' => 'masterFile']);
        }


        $currentPotro = $potro->toArray();
        $potroid = !empty($currentPotro) ? $currentPotro[0]->id : 0;

        $potrojariAttachmentsTable = TableRegistry::get('PotrojariAttachments');
        $potrojariAttachments = $potrojariAttachmentsTable->find()->select(['attachment_type',
            'file_name', 'potrojari_id', 'id', 'user_file_name'])
            ->join([
                'Potrojari' =>
                    ['table' => 'potrojari',
                        'type' => 'inner', 'conditions' => 'Potrojari.id = PotrojariAttachments.potrojari_id']
            ])->where(['Potrojari.potro_status' => 'Sent',
                'Potrojari.id' => $potroid, 'PotrojariAttachments.attachment_type <> ' => 'text'])->order(['Potrojari.id desc'])->toArray();


            $potrojari_templates = TableRegistry::get('PotrojariTemplates');
            $template_list = $potrojari_templates->find('list', ['keyField' => 'template_id', 'valueField' => 'template_name'])->where(['status' => 1])->toArray();
    
            $i = 0;
            $templete_options[$i]['text'] = 'পত্রের ধরন বাছাই করুন';
            $templete_options[$i]['value'] = '';
            //define offices2ShowTemplateModals
            $offices2ShowTemplateModals = $this->offices2ShowTemplateModals();
            
            $this->set(compact('offices2ShowTemplateModals'));
            foreach ($template_list as $key => $value) {
                if (($key == 21 || $key == 27) && !in_array($employee_office['office_id'], $offices2ShowTemplateModals)) {
                   continue;
                }
                $i++;
                $templete_options[$i]['text'] = $value;
                $templete_options[$i]['value'] = $key;
            }
              
        $this->set('templete_options', $templete_options);
        $this->set('potro', $potro);
        $this->set('potrojariAttachments', $potrojariAttachments);
        $this->set('nothiDetails', !empty($potrojariarray) ? $potrojariarray[0] : []);
        if (isset($this->request->query['nothi_master'])) {
            $this->set('selected_nothi_master_id', $this->request->query['nothi_master']);
        }
        if (isset($this->request->query['nothi_part'])) {
            $nothi_part_info = TableRegistry::get('NothiNotes')->find()->where(["nothi_part_no" => $this->request->query['nothi_part']])->first();
            $this->set('selected_nothi_part_id', $this->request->query['nothi_part']);
            $this->set('nothi_subject', strip_tags($nothi_part_info['subject']));
        }
        $this->set('is_summary_nothi', false);
    }

    public function summaryMasterFile()
    {

        $this->layout = null;
        $this->view = 'master_file';
        $this->set('is_summary_nothi', true);
        $employee_office = $this->getCurrentDakSection();

        $OfficesTable = TableRegistry::get('Offices');
        $office_info = $OfficesTable->get($employee_office['office_id']);

        $OfficeLayersTable = TableRegistry::get('OfficeLayers');
        $office_layer_info = $OfficeLayersTable->get($office_info['office_layer_id']);
        $this->set('layer_level', $office_layer_info['layer_level']);


        $potrojariId = [];
        $OfficeDomainTable = TableRegistry::get('OfficeDomains');
        if ($office_layer_info['layer_level'] == 1 || $office_layer_info['layer_level'] == 2 || $office_layer_info['layer_level'] == 3) {
            $office_domain = $OfficeDomainTable->getNothiReplacedPortalDomainbyOffice($employee_office['office_id']);
        } else {
            $office_domain = $OfficeDomainTable->getPortalDomainByFieldOffice($employee_office['office_id']);
        }
        $this->set('office_domain', $office_domain);
        $potrojariTable = TableRegistry::get('Potrojari');
        $unitTable = TableRegistry::get("OfficeUnits");
        $nothi_office = $employee_office['office_id'];

        $this->set('nothi_office', $nothi_office);

        $allPotrojari = $potrojariTable->find()
            ->select([
                'Potrojari.id',
                'Potrojari.nothi_master_id',
                'Potrojari.nothi_part_no',
                'Potrojari.potro_subject',
                'Potrojari.potrojari_date',
                'Potrojari.sarok_no',
                'Potrojari.potro_type',
                'Potrojari.potro_description',
                'Potrojari.potro_cover',
                'Potrojari.attached_potro_id',
                'Potrojari.attached_potro',
                'Potrojari.is_summary_nothi',
                'Potrojari.meta_data',

                'NothiParts.subject',
                'NothiParts.nothi_no',
                'NothiParts.office_units_id',

                'NothiTypes.type_name',
            ])
            ->where([
                'Potrojari.potro_status' => 'Sent',
                'Potrojari.office_id' => $employee_office['office_id'],
                'NothiMasterPermissions.office_unit_organograms_id' => $employee_office['office_unit_organogram_id'],
                'NothiMasterPermissions.nothi_office' => $employee_office['office_id'],
            ])->join([
                'NothiParts' => [
                    'table' => 'nothi_parts',
                    'type' => 'INNER',
                    'conditions' => 'NothiParts.id = Potrojari.nothi_part_no'
                ],
                'NothiTypes' => [
                    'table' => 'nothi_types',
                    'type' => 'inner',
                    'conditions' => 'NothiTypes.id = NothiParts.nothi_types_id'
                ],
                'NothiMasterPermissions' => [
                    'table' => 'nothi_master_permissions',
                    'type' => 'INNER',
                    'conditions' => 'NothiParts.office_id = NothiMasterPermissions.nothi_office AND 
                    NothiParts.nothi_masters_id = NothiMasterPermissions.nothi_masters_id AND 
                    NothiParts.id = NothiMasterPermissions.nothi_part_no and 
                    NothiMasterPermissions.office_unit_organograms_id =' . $employee_office['office_unit_organogram_id'] . ' 
                    AND NothiMasterPermissions.nothi_office = ' . $employee_office['office_id']
                ]
            ])->order(['Potrojari.potrojari_date' => 'asc']);


        if (!empty($this->request->query['potrojari_template'])) {
            $potrojari_template = $this->request->query['potrojari_template'];
            $allPotrojari = $allPotrojari->where(["OR" => ['Potrojari.potro_type like' => $potrojari_template
            ]]);
        }


        if (!empty($this->request->query['start_date'])) {
            $start_date = $this->request->query['start_date'];
            $end_date = $this->request->query['end_date'];
            $allPotrojari = $allPotrojari->where(["AND" => [' date(Potrojari.potrojari_date) >= ' => $start_date, ' date(Potrojari.potrojari_date) <= '=>$end_date
            ]]);

        }

        if (!empty($this->request->query['search'])) {
            $search = $this->request->query['search'];
            $this->set(compact('search'));
            $allPotrojari = $allPotrojari->where(["OR" => [
                'NothiParts.nothi_no like' => '%' . $search . '%',
                'NothiParts.subject like' => '%' . $search . '%',
                'Potrojari.sarok_no LIKE' => '%' . $search . '%',
                'Potrojari.potro_subject LIKE' => '%' . $search . '%'
            ]]);
        }
        $potrojariarray = $allPotrojari->count();

        $allUnits = $unitTable->getOfficeUnitsList($employee_office['office_id']);
        $this->set(compact('allUnits'));
        $paginate = array('limit' => 1, 'page' => $potrojariarray);

        $this->paginate = $paginate;

        try {
            $potro = $this->paginate($allPotrojari);
        } catch (NotFoundException $e) {
            return $this->redirect(['action' => 'masterFile']);
        }


        $currentPotro = $potro->toArray();
        $potroid = !empty($currentPotro) ? $currentPotro[0]->id : 0;

        $potrojariAttachmentsTable = TableRegistry::get('PotrojariAttachments');
        $potrojariAttachments = $potrojariAttachmentsTable->find()->select(['attachment_type',
            'file_name', 'potrojari_id', 'id', 'user_file_name'])
            ->join([
                'Potrojari' =>
                    ['table' => 'potrojari',
                        'type' => 'inner', 'conditions' => 'Potrojari.id = PotrojariAttachments.potrojari_id']
            ])->where(['Potrojari.potro_status' => 'Sent',
                'Potrojari.id' => $potroid, 'PotrojariAttachments.attachment_type <> ' => 'text'])->order(['Potrojari.id desc'])->toArray();


        $potrojari_templates = TableRegistry::get('PotrojariTemplates');
        $template_list = $potrojari_templates->find('list', ['keyField' => 'template_id', 'valueField' => 'template_name'])->where(['status' => 2])->toArray();

        $i = 0;
        $templete_options[$i]['text'] = 'পত্রের ধরন বাছাই করুন';
        $templete_options[$i]['value'] = '';
        //define offices2ShowTemplateModals
        $offices2ShowTemplateModals = $this->offices2ShowTemplateModals();

        $this->set(compact('offices2ShowTemplateModals'));
        foreach ($template_list as $key => $value) {
            if (($key == 21 || $key == 27) && !in_array($employee_office['office_id'], $offices2ShowTemplateModals)) {
                continue;
            }
            $i++;
            $templete_options[$i]['text'] = $value;
            $templete_options[$i]['value'] = $key;
        }

        $this->set('templete_options', $templete_options);
        $this->set('potro', $potro);
        $this->set('potrojariAttachments', $potrojariAttachments);
        $this->set('nothiDetails', !empty($potrojariarray) ? $potrojariarray[0] : []);
        if (isset($this->request->query['nothi_master'])) {
            $this->set('selected_nothi_master_id', $this->request->query['nothi_master']);
        }
        if (isset($this->request->query['nothi_part'])) {
            $nothi_part_info = TableRegistry::get('NothiNotes')->find()->where(["nothi_part_no" => $this->request->query['nothi_part']])->first();
            $this->set('selected_nothi_part_id', $this->request->query['nothi_part']);
            $this->set('nothi_subject', strip_tags($nothi_part_info['subject']));
        }
    }

    public function nextPotroPage($nothiMasterId, $page = 1, $potroId = 0)
    {
        $this->layout = null;
        if ($this->request->is('ajax')) {
            $nothi_office = intval($this->request->data['nothi_office']);
            $employee_office = $this->getCurrentDakSection();
            $otherNothi = false;
            if ($nothi_office != 0 && $nothi_office != $employee_office['office_id']) {
                $otherNothi = true;

                $this->switchOffice($nothi_office, 'MainNothiOffice');
            } else {
                $nothi_office = $employee_office['office_id'];
            }

            $this->set('nothi_office', $nothi_office);
            $this->set(compact('otherNothi'));

            $nothiMasterCurrentUsersTable = TableRegistry::get("NothiMasterCurrentUsers");

            $nothiMastersCurrentUser = $nothiMasterCurrentUsersTable->find()->where(['nothi_part_no' => $nothiMasterId,
                'office_id' => $employee_office['office_id'], 'office_unit_id' => $employee_office['office_unit_id'],
                'office_unit_organogram_id' => $employee_office['office_unit_organogram_id'],
                'nothi_office' => $nothi_office])->first();

            $privilige_type = 0;
            if (!empty($nothiMastersCurrentUser)) {
                $privilige_type = 1;
            }
            $this->set('id', $nothiMasterId);
            TableRegistry::remove('NothiPotroAttachments');
            $potroAttachmentTable = TableRegistry::get('NothiPotroAttachments');

            $conditions = '';
            if (!empty($potroId)) {
                $conditions = '(nothi_potro_page="' . $potroId . '" OR nothi_potro_page_bn = "' . $potroId . '")';
            }

            $potroAttachmentRecord = $potroAttachmentTable->find()->contain(['PotrojariData', 'NothiPotros'])->where(['NothiPotroAttachments.nothi_part_no' => $nothiMasterId,
                'NothiPotroAttachments.status' => 1])->where([$conditions])->order(['NothiPotroAttachments.id DESC '])->page($page,
                5)->toArray();

            $html = '';
            $potroNo = '';
            if (!empty($potroAttachmentRecord)) {
                $i = 0;
                foreach ($potroAttachmentRecord as $row) {
                    if ($i == 0) {
                        $potroNo = $row['nothi_potro_page_bn'];
                    }

                    $html .= '<div id_ar="' . $row['id'] . '" id_en="' . $row['nothi_potro_page'] . '" id_bn="' . $row['nothi_potro_page_bn'] . '" data-content-type="' . $row['attachment_type'] . '" class="attachmentsRecord ' . ($i
                        == 0 ? 'active ' : ($i == (count($potroAttachmentRecord)
                            - 1) ? 'last' : '')) . '">';

                    $html .= '<div  style="background-color: #a94442; padding: 5px; color: #fff;"> 
                        <div class="pull-left" style="padding-top: 0px;">' .
                         mb_substr($row['nothi_potro']['subject'], 0, 50) .
                        (mb_strlen($row['nothi_potro']['subject']) > 50 ? '...' : '') .
                        '</div><div class="pull-right text-right"> ';

                    if ($row['attachment_type'] == 'text') {
                        if ($privilige_type == 1 && !empty($row['nothi_potro']['application_origin']) && $row['nothi_potro']['application_origin'] != 'nothi') {
                            $decision_meta = jsonA($row['nothi_potro']['application_meta_data']);
                            $decision = !empty($decision_meta['decision']) ? ($decision_meta['decision']) : '{}';
                            $signdesk = !empty($decision_meta['sdesk']) ? ($decision_meta['sdesk']) : 0;
                            $action_decisions = !empty($decision_meta['action_decisions']) ? $decision_meta['action_decisions'] : [];
                            $decision_answer = isset($decision_meta['decision_answer']) ? $decision_meta['decision_answer'] : -1;
                            $presentation_date = isset($decision_meta['presentation_date']) ? $decision_meta['presentation_date'] :'';
                            $decision_note = isset($decision_meta['decision_note']) ? $decision_meta['decision_note'] : '';
                            $decision_id = isset($decision_meta['decision_id']) ? $decision_meta['decision_id'] : -1;
                            $same_desk = !empty($decision_meta['decision_desk']) && $employee_office['office_unit_organogram_id'] == $decision_meta['decision_desk'] ? 1 : 0;
                            $is_locked = !empty($decision_meta['locked']) ? 1: 0;
                            $decision_locked = $is_locked ? 1 : $same_desk;

                            $empty_obj = '{}';
                            if(empty($decision_meta['other_party_certificate']) && $row['nothi_potro']['application_origin'] != 'ARGS'):
                              $html .=  '<button type="button" class="btn btn-info btn-xs showDecisionModal" data-presentation_date="'.$presentation_date.'" data-id="'.$row['id'].'" data-potro-id="'. $row['nothi_potro']['id'] .'" data-decision="'.h(json_encode($decision)).'" data-action-decisions="'.(!empty($action_decisions)?h(json_encode($action_decisions)):$empty_obj).'" data-decision-id="'. $decision_id .'" data-comment="'. h($decision_note) .'" data-decision-answer="'. $decision_answer .'" data-locked="'.$decision_locked. '" data-nothi-office="'.$nothi_office.'" data-part-id="'.$row['nothi_part_no'].'" data-potro-subject="'. h($row['nothi_potro']['subject']).'">সিদ্ধান্ত</button>';
                            elseif($row['nothi_potro']['application_origin'] != 'ARGS'):
                                $file_name = explode("content/",$decision_meta['other_party_certificate']);
                            if(!empty($file_name[1])){
                                $href = $decision_meta['other_party_certificate'].'?token=' . sGenerateToken(['file' => $file_name[1]], ['exp' => time() + 60 * 300]);
                            }else{
                                $href =  $decision_meta['other_party_certificate'];
                            }
                                $html .= '<a target="__blank" class="btn btn-info btn-xs" href="'.$href.'">সার্টিফিকেট</a>';
                            endif;
                            if(($privilige_type == 1 && !$is_locked && !empty($action_decisions['editable']) && in_array($decision_answer,$action_decisions['editable'])) || $row['nothi_potro']['application_origin'] == 'ARGS'){
                                $html .=  '<a title="সম্পাদনা করুন" href="'. Router::url(['_name' => 'editPotro', $nothi_office,$row['id']]).'" class="btn blue btn-xs"><i class="fs1 fa fa-pencil"></i></a>';
                            }
                        }
                    }

                    $html .= ($row['attachment_type'] == 'text' ? '<a data-id="' . $row['id'] . '" title="প্রিন্ট করুন" href="javascript:void(0)" class="btn    green btn-sm btn-print"><i class="fs1 a2i_gn_print2"></i></a> ' : '') . (!empty($row['potrojari_data'])
                            ? '<a  href="' . $this->request->webroot . 'potrojari/pendingList/' . $nothi_office . '/' . $row['potrojari_id'] . '" target="_tab" class="btn   btn-xs btn-primary" title="পত্রজারি প্রেরণ অবস্থা দেখুন" data-title-orginal="পত্রজারি প্রেরণ অবস্থা দেখুন"> <i class="fs1 a2i_gn_refresh1"></i> </a> ' : '') .
                        ($row['attachment_type'] == 'text' && empty($row['potrojari_data']['attached_potro_id'])
                            ? '<a class="btn   btn-xs btn-danger btn-endorsment" url="' . Router::url(['controller' => 'potrojari', 'action' => 'makeDraft', $row['nothi_part_no'], $row['id'], 'potro', $row['id']]) . '" title="পৃষ্ঠাঙ্কন করুন" data-title-orginal="পৃষ্ঠাঙ্কন করুন"> <span class="fa fa-certificate"></span></a> '
                            : '') .
                        (!empty($row['potrojari_id']) && empty($row['potrojari_data']['attached_potro_id'])
                            ? ($otherNothi == false) ? '<a class="btn   btn-xs btn-warning" onclick="clonePotrojari(\'' . Router::url(['controller' => 'potrojari', 'action' => 'potrojariClone', $row['potrojari_id']]) . '\')" title="পত্রজারি ক্লোন করুন 2" data-title-orginal="পত্রজারি ক্লোন করুন"><span class="glyphicon glyphicon-copy"></span></a> ' : ''
                            : '') .
                        (($privilige_type == 1 && $row['potrojari_status'] != 'Draft')
                            ? ('<a  url="' . $this->request->webroot . 'potrojari/makeDraft/' . $row['nothi_part_no'] . '/' . $row['id'] . '" href="javascript: void(0);" class="btn btn-xs   green potroCreate"> <i class="fs1 a2i_nt_dakdraft4"></i> </a> ')
                            : '') . '</div><div  style="clear: both;" ></div>
</div>';//edit draft is not needed


                    if ($row['attachment_type'] == 'text') {
                        if ($row['is_summary_nothi'] == 1) {
                            $html .= '<div style="background-color: #fff; max-width:950px; min-height:815px;  margin:0 auto; page-break-inside: auto;">' . html_entity_decode($row['potro_cover']) . "<br/>" . html_entity_decode($row['content_body']) . "</div>";
                        } else {
                            $header = jsonA($row['meta_data']);
                            $html .= (!empty($header['potro_header_banner']) ? ('<div style="text-align: ' . ($header['banner_position']) . '!important;"><img src="' . $this->request->webroot . 'getContent?file=' . base64_decode($header['potro_header_banner']) . '&token=' . sGenerateToken(['file' => base64_decode($header['potro_header_banner'])], ['exp' => time() + 60 * 300]) . '" style="width:' . ($header['banner_width']) . ';height: 60px;max-width: 100%;"/></div>') : '') . $row['content_body'];
                        }
                    } else if (substr($row['attachment_type'], 0, 5) != 'image') {
                        if (substr($row['attachment_type'], 0, strlen('application/vnd')) == 'application/vnd' || substr($row['attachment_type'], 0, strlen('application/ms')) == 'application/ms') {
                            $url = urlencode($this->request->webroot . 'getContent?file=' . $row['file_name'] . '&token=' . sGenerateToken(['file' => $row['file_name']], ['exp' => time() + 60 * 300]));
                            $html .= '<iframe style=" width:100%; height: 700px;" src="https://docs.google.com/gview?url=' . $url . '&embedded=true" style="width:600px; height:500px;" frameborder="0"></iframe>';
                        } else {
                            $html .= '<embed src="' . $this->request->webroot . 'getContent?file=' . $row['file_name'] . '&token=' . sGenerateToken(['file' => $row['file_name']], ['exp' => time() + 60 * 300]) . '" style=" width:100%; height: 700px;" type="' . $row['attachment_type'] . '"></embed>';
                        }
                    } else {
                        $html .= '<div class="text-center"><a href="' . $this->request->webroot . 'content/' . $row['file_name'] . '?token=' . sGenerateToken(['file' => $row['file_name']], ['exp' => time() + 60 * 300]) . '" onclick="return false;" data-title="পত্র নম্বর:  ' . $row['nothi_potro_page_bn'] . '" data-footer="" ><img class="zoomimg img-responsive"  src="' . $this->request->webroot . 'content/' . $row['file_name'] . '?token=' . sGenerateToken(['file' => $row['file_name']], ['exp' => time() + 60 * 300]) . '" alt="ছবি পাওয়া যায়নি"></a></div>';
                    }

                    $html .= "</div>";
                    $i++;
                }
            }

            echo json_encode(array('html' => $html, 'potrono' => $potroNo));
            die;
        }
    }

    public function nextAllPotroPage($nothiMasterId = 0, $page = 1, $search = 0)
    {
        $this->layout = null;
        if ($this->request->is('ajax')) {
            $nothi_office = intval($this->request->data['nothi_office']);
            $employee_office = $this->getCurrentDakSection();
            $otherNothi = false;
            if ($nothi_office != 0 && $nothi_office != $employee_office['office_id']) {
                $otherNothi = true;

                $this->switchOffice($nothi_office, 'MainNothiOffice');
            } else {
                $nothi_office = $employee_office['office_id'];
            }

            TableRegistry::remove("NothiParts");
            $nothiPartsTable = TableRegistry::get("NothiParts");
            $nothiPartsInformation = $nothiPartsTable->getAll(['NothiParts.id' => $nothiMasterId])->first();

            $nothiMasterPermissionsTable = TableRegistry::get("NothiMasterPermissions");

            $allNothiMasterList = $nothiMasterPermissionsTable->getMasterNothiPartList($nothi_office,
                $employee_office['office_id'], $employee_office['office_unit_id'],
                $employee_office['office_unit_organogram_id'],
                $nothiPartsInformation['nothi_masters_id']);

            $nothiPartId = $draftSummary = $summaryAttachmentRecord
                = array();

            if (!empty($allNothiMasterList)) {
                foreach ($allNothiMasterList as $key => $value) {
                    $nothiPartId[] = $value['nothi_part_no'];
                }
            }

            TableRegistry::remove('NothiPotroAttachments');
            $potroAttachmentTable = TableRegistry::get('NothiPotroAttachments');

            $potroAttachmentRecord = $potroAttachmentTable->find()->where(['NothiPotroAttachments.nothi_part_no IN' => $nothiPartId,
                '(NothiPotroAttachments.is_summary_nothi IS NULL OR NothiPotroAttachments.is_summary_nothi = 0)', 'NothiPotroAttachments.nothijato' => 0, 'NothiPotroAttachments.status' => 1])
                ->contain(['PotrojariData', 'NothiPotros']);

            $limit = 10;
            if ($search) {
                $page = 1;

                $searchData = !empty($this->request->data['q']) ? h($this->request->data['q']) : '';
                if (!empty($searchData)) {
                    $potroAttachmentRecord->join([
                        'NothiPotros' => [
                            'table' => 'nothi_potros',
                            'type' => 'INNER',
                            'conditions' => 'NothiPotros.id = NothiPotroAttachments.nothi_potro_id'
                        ]
                    ]);
                    $potroAttachmentRecord->where(["OR" => [
                        'NothiPotros.sarok_no' => $searchData,
                        'NothiPotros.subject LIKE' => "%{$searchData}%"
                    ]
                    ]);
                }

                if (!empty($searchData)) {
                    $limit = 100;
                }
            }

            $potroAttachmentRecord = $potroAttachmentRecord->order(['NothiPotroAttachments.id desc'])->page($page, $limit)->toArray();

            $html = '';
            $potroNo = '';
            if (!empty($potroAttachmentRecord)) {
                $i = 0;
                foreach ($potroAttachmentRecord as $row) {
                    if ($i == 0) {
                        $potroNo = $row['nothi_potro_page_bn'];
                    }
                    $html .= '<div id_en="' . $row['nothi_potro_page'] . '" id_bn="' . $row['nothi_potro_page_bn'] . '" class="attachmentsRecord ' . ($i
                        == 0 ? 'active ' : ($i == (count($potroAttachmentRecord)
                            - 1) ? 'last' : '')) . '">';

                    $html .= '<div  style="background-color: #a94442; padding: 5px; color: #fff;"> <div class="pull-left" style="padding-top: 0px;">' .
                        mb_substr($row['nothi_potro']['subject'], 0, 50) . (mb_strlen($row['nothi_potro']['subject']) > 50 ? '...' : '') . '</div>
    <div class="pull-right text-right"> ' .
                        (($row['is_summary_nothi'] == 0 && $row['attachment_type'] == 'text' && !empty($row['potrojari_id']) && empty($row['potrojari_data']['attached_potro_id'])) ? (
                            '<button type="button" class="btn   btn-sm btn-warning" onclick="selectNote(' . $row['potrojari_id'] . ')" data-toggle="tooltip" title="পত্রজারি ক্লোন করুন" >
                            <span class="glyphicon glyphicon-copy"></span></button> ') : '')
                        . ($row['attachment_type'] == 'text' || substr($row['attachment_type'], 0, 5) == 'image' ? '<a data-id="' . $row['id'] . '" title="প্রিন্ট করুন" href="javascript:void(0)" class="btn    green btn-sm btn-print"><i class="fs1 a2i_gn_print2"></i></a> ' : '')
                        . ($row['attachment_type'] != 'text' && substr($row['attachment_type'], 0, 5) != 'image' && $row['attachment_type'] != 'application/pdf' ? '<a onclick="'.Router::url(['_name'=>'downloadPotro',$nothi_office,$row['id']]).'" href="javascript:void(0)" class="btn green btn-sm btn-download" title="ডাউনলোড করুন"><i class="glyphicon glyphicon-download-alt"></i></a>' : '')
                        . '</div><div  style="clear: both;" ></div>
</div>';

                    if ($row['attachment_type'] == 'text') {
                        if ($row['is_summary_nothi'] == 1) {
                            $html .= '<div style="background-color: #fff; max-width:950px; min-height:815px;  margin:0 auto; page-break-inside: auto;">' . html_entity_decode($row['potro_cover']) . "<br/>" . html_entity_decode($row['content_body']) . "</div>";
                        } else {
                            $header = jsonA($row['meta_data']);
                            $html .= (!empty($header['potro_header_banner']) ? ('<div style="text-align: ' . ($header['banner_position']) . '!important;"><img src="' . $this->request->webroot . 'getContent?file=' . base64_decode($header['potro_header_banner']) . '&token=' . sGenerateToken(['file' => base64_decode($header['potro_header_banner'])], ['exp' => time() + 60 * 300]) . '" style="width:' . ($header['banner_width']) . ';height: 60px;max-width: 100%;"/></div>') : '') . $row['content_body'];
                        }
                    } else if (substr($row['attachment_type'], 0, 5) != 'image') {
                        if (substr($row['attachment_type'], 0, strlen('application/vnd')) == 'application/vnd' || substr($row['attachment_type'], 0, strlen('application/ms')) == 'application/ms') {
                            $url = urlencode($this->request->webroot . 'getContent?file=' . $row['file_name'] . '&token=' . sGenerateToken(['file' => $row['file_name']], ['exp' => time() + 60 * 300]));
                            $html .= '<iframe style=" width:100%; height: 700px;" src="https://docs.google.com/gview?url=' . $url . '&embedded=true" style="width:600px; height:500px;" frameborder="0"></iframe>';
                        } else {
                            $html .= '<embed src="' . $this->request->webroot . 'getContent?file=' . $row['file_name'] . '&token=' . sGenerateToken(['file' => $row['file_name']], ['exp' => time() + 60 * 300]) . '" style=" width:100%; height: 700px;" type="' . $row['attachment_type'] . '"></embed>';
                        }
                    } else {
                        $html .= '<div class="text-center"><a href="' . $this->request->webroot . 'content/' . $row['file_name'] . '?token=' . sGenerateToken(['file' => $row['file_name']], ['exp' => time() + 60 * 300]) . '" onclick="return false;" data-title="পত্র নম্বর:  ' . $row['nothi_potro_page_bn'] . '" data-footer="" ><img class="zoomimg img-responsive"  src="' . $this->request->webroot . 'content/' . $row['file_name'] . '?token=' . sGenerateToken(['file' => $row['file_name']], ['exp' => time() + 60 * 300]) . '" alt="ছবি পাওয়া যায়নি"></a></div>';
                    }

                    $html .= "</div>";
                    $i++;
                }
            }

            echo json_encode(array('html' => $html, 'potrono' => $potroNo));
            die;
        }
    }

    public function potroList($nothiMasterId, $nothi_office = 0)
    {

        $this->layout = null;
        $potroTableRecord = array();
        $html = '';
        if ($this->request->is('ajax')) {
            $employee_office = $this->getCurrentDakSection();
            $otherNothi = false;
            if ($nothi_office != 0 && $nothi_office != $employee_office['office_id']) {
                $otherNothi = true;

                $this->switchOffice($nothi_office, 'MainNothiOffice');
            } else {
                $nothi_office = $employee_office['office_id'];
            }

            $this->set('nothi_office', $nothi_office);
            $this->set(compact('otherNothi'));

            TableRegistry::remove('NothiPotros');
            TableRegistry::remove('NothiParts');
            $potroTable = TableRegistry::get('NothiPotros');
            $nothiPartsTable = TableRegistry::get('NothiParts');
            $nothiPartsInformation = $nothiPartsTable->get($nothiMasterId);

            $potroTableRecord = $potroTable->find()->where(['nothi_part_no' => $nothiMasterId])->where(['nothijato' => 0,
                'is_deleted' => 0])->order(['id desc'])->toArray();

            if (!empty($potroTableRecord)) {
                foreach ($potroTableRecord as $key => $value) {
                    $html .= '<div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a id="' . $value['id'] . '" src="collapse_' . $value['nothi_part_no'] . '_' . $value['id'] . '"  class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" href="#collapse_' . $value['nothi_part_no'] . '_' . $value['id'] . '">' . $value['subject'] . '</a>
                            </h4>
                        </div>
                        <div id="collapse_' . $value['nothi_part_no'] . '_' . $value['id'] . '" class="panel-collapse collapse">
                            <div class="panel-body " style="max-height: 842px; overflow-y:auto;">

                            </div>
                        </div>
                    </div>';
                }
            }
        }

        echo json_encode(array('html' => $html));

        die;
    }

    public function potroListAll($nothiMasterId, $nothi_office)
    {

        $employee_office = $this->getCurrentDakSection();
        $otherNothi = false;
        if ($nothi_office != 0 && $nothi_office != $employee_office['office_id']) {
            $otherNothi = true;

            $this->switchOffice($nothi_office, 'MainNothiOffice');
        } else {
            $nothi_office = $employee_office['office_id'];
        }
        $this->layout = null;
        $potroTableRecord = array();
        $html = '';
        if ($this->request->is('ajax')) {

            TableRegistry::remove('NothiPotros');
            TableRegistry::remove('NothiParts');
            $potroTable = TableRegistry::get('NothiPotros');
            $nothiPartsTable = TableRegistry::get('NothiParts');


            $nothiMasterPermissionsTable = TableRegistry::get("NothiMasterPermissions");

            $nothiPartsInformation = $nothiPartsTable->getAll(['NothiParts.id' => $nothiMasterId])->first();

            $officeUnitTable = TableRegistry::get('OfficeUnits');

            $allNothiMasterList = $nothiMasterPermissionsTable->getMasterNothiPartList($nothi_office,
                $employee_office['office_id'],
                $employee_office['office_unit_id'],
                $employee_office['office_unit_organogram_id'],
                $nothiPartsInformation['nothi_masters_id']);

            $nothiPartId = array();

            if (!empty($allNothiMasterList)) {
                foreach ($allNothiMasterList as $key => $value) {
                    $nothiPartId[] = $value['nothi_part_no'];
                }
            }

            $potroTableRecord = $potroTable->find()->where(['nothi_part_no IN' => $nothiPartId,
                'nothijato' => 0, 'is_deleted' => 0])->order(['id desc'])->toArray();

            if (!empty($potroTableRecord)) {
                foreach ($potroTableRecord as $key => $value) {
                    $html .= '<div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a id="' . $value['id'] . '" src="collapse_' . $value['nothi_part_no'] . '_' . $value['id'] . '"  class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" href="#collapse_' . $value['nothi_part_no'] . '_' . $value['id'] . '">' . $value['subject'] . '</a>
                            </h4>
                        </div>
                        <div id="collapse_' . $value['nothi_part_no'] . '_' . $value['id'] . '" class="panel-collapse collapse">
                            <div class="panel-body " style="max-height: 842px; overflow-y:auto;">

                            </div>
                        </div>
                    </div>';
                }
            }
        }

        echo json_encode(array('html' => $html));

        die;
    }

    public function getPotro($nothiMasterId, $potroId)
    {

        $this->layout = null;
        if ($this->request->is('ajax')) {
            $this->set('id', $nothiMasterId);
            TableRegistry::remove('NothiPotroAttachments');
            $potroAttachmentTable = TableRegistry::get('NothiPotroAttachments');

            $potroAttachmentRecord = $potroAttachmentTable->find()->where(['nothi_part_no' => $nothiMasterId,
                'nothi_potro_page_bn' => $potroId, 'status' => 1])->first();

            $html = '';

            if (!empty($potroAttachmentRecord)) {
                $i = 0;
                $html .= '
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title"> পত্র: ' . $potroAttachmentRecord['nothi_potro_page_bn'] . '</h3>
                        </div>
                    
<div class="panel-body">';

                $html .= '<div > ';

                $potroAttachmentRecord['attachment_type'] = str_replace("; charset=binary",
                    "", $potroAttachmentRecord['attachment_type']);
                if ($potroAttachmentRecord['attachment_type'] == 'text') {
                    $html .= '<div style="background-color: #fff; max-width:950px; height:815px; margin:0 auto; page-break-inside: auto; ">' . html_entity_decode($potroAttachmentRecord['content_body']) . "</div>";
                } elseif (substr($potroAttachmentRecord['attachment_type'], 0, 5)
                    != 'image'
                ) {
                    if (substr($potroAttachmentRecord['attachment_type'], 0,
                            strlen('application/vnd')) == 'application/vnd' || substr($potroAttachmentRecord['attachment_type'],
                            0, strlen('application/ms')) == 'application/ms'
                    ) {
                        $html .= '<iframe style=" width:100%; height: 700px;" src="https://docs.google.com/gview?url=' . FILE_FOLDER . $potroAttachmentRecord['file_name'] . '&embedded=true" style="width:600px; height:500px;" frameborder="0"></iframe>';
                    } else {
                        $html .= '<embed src="' . $this->request->webroot . 'content/' . $potroAttachmentRecord['file_name'] . '?token=' . $this->generateToken(['file' => $potroAttachmentRecord['attachment_type']], ['exp' => time() + 60 * 300]) . '" style=" width:100%; height: 700px;" type="' . $potroAttachmentRecord['attachment_type'] . '"></embed>';
                    }
                } else {

                    $html .= '<div class="text-center img"><img class="img-responsive"  src="' . $this->request->webroot . 'content/' . $potroAttachmentRecord['file_name'] . '?token=' . $this->generateToken(['file' => $potroAttachmentRecord['file_name']], ['exp' => time() + 60 * 300]) . '" alt="ছবি পাওয়া যায়নি"></div>';
                }
                $html .= "</div>";
                $html .= "</div></div>";
            } else {
                $html .= "<p class='text-center text-danger'>দুঃখিত! পত্র পাওয়া যায়নি।</p>";
            }

            echo json_encode(array('html' => $html));
            die;
        }
    }

    public function attachmentsForPotro($nothi_part_id = 0, $potroId,
                                        $nothi_office = 0, $nothiMasterId = 0)
    {

        $this->layout = null;

        if ($this->request->is('ajax')) {
            $employee_office = $this->getCurrentDakSection();
            $otherNothi = false;
            if ($nothi_office != 0 && $nothi_office != $employee_office['office_id']) {
                $otherNothi = true;

                $this->switchOffice($nothi_office, 'MainNothiOffice');
            } else {
                $nothi_office = $employee_office['office_id'];
            }

            $this->set('nothi_office', $nothi_office);
            $this->set(compact('otherNothi'));

            $this->set('id', $nothi_part_id);
            TableRegistry::remove('NothiPotroAttachments');
            $potroAttachmentTable = TableRegistry::get('NothiPotroAttachments');

            $potroAttachmentRecord = $potroAttachmentTable->find()->where([
                'nothi_potro_id' => $potroId, 'status' => 1]);
            if (!empty($nothi_part_id)) {
                $potroAttachmentRecord = $potroAttachmentRecord->where(['nothi_part_no' => $nothi_part_id]);
            }
            if (!empty($nothiMasterId)) {
                $potroAttachmentRecord = $potroAttachmentRecord->where(['nothi_master_id' => $nothiMasterId]);
            }
            $potroAttachmentRecord = $potroAttachmentRecord->order(['id desc'])->toArray();
//            pr($potroAttachmentRecord);die;

            $html = '';

            if (!empty($potroAttachmentRecord)) {
                $i = 0;
                $html .= '
                    <div class="portlet box blue">
                    <div class="portlet-title">
<div class="caption">
 মোট: ' . Number::format(count($potroAttachmentRecord)) . 'টি
</div>
<div class="actions">
<ul class="pager potroAccordianPaginator" id="' . $potroId . '">
<li><button class="btn btn-sm btn-danger prevImageAccordian" ><</button> </li>
<li><button class="btn btn-sm btn-danger nextImageAccordian" >></button> </li>
</ul>
</div>
</div><div class="portlet-body">';
                foreach ($potroAttachmentRecord as $row) {

                    $html .= '<div  class="potroAccordianPaginationBody potro_details_accordian_div_' . $potroId . ($i
                        == 0 ? ' active' : '') . '"> &nbsp;';
                    if (!empty($nothi_part_id)) {
                        $html .= '<input class="potro_choose" type="checkbox" potro_page_no="' . $row['nothi_potro_page_bn'] . '" potro_page_id="' . $row['id'] . '"/>' . $row['nothi_potro_page_bn'] . '';
                    }
                    $row['attachment_type'] = str_replace("; charset=binary",
                        "", $row['attachment_type']);
                    if ($row['attachment_type'] == 'text') {
                        $html .= '<div  data-type="text" style="background-color: #fff; max-width:950px; min-height:815px; margin:0 auto; page-break-inside: auto; ">' . html_entity_decode($row['content_body']) . "</div>";
                    } elseif (substr($row['attachment_type'], 0, 5) != 'image') {
                        if (substr($row['attachment_type'], 0,
                                strlen('application/vnd')) == 'application/vnd' || substr($row['attachment_type'],
                                0, strlen('application/ms')) == 'application/ms'
                        ) {
                            $html .= '<iframe style=" width:100%; height: 700px;" src="https://docs.google.com/gview?url=' . FILE_FOLDER . $row['file_name'] . '&embedded=true" style="width:600px; height:500px;" frameborder="0"></iframe>';
                        } else {
                            $html .= '<embed src="' . $this->request->webroot . 'content/' . $row['file_name'] . '?token=' . $this->generateToken(['file' => $row['file_name']], ['exp' => time() + 60 * 300]) . '" style=" width:100%; height: 700px;" type="' . $row['attachment_type'] . '"></embed>';
                        }
                    } else {

                        $html .= '<div class="text-center img"><a href="' . $this->request->webroot . 'content/' . $row['file_name'] . '?token=' . $this->generateToken(['file' => $row['file_name']], ['exp' => time() + 60 * 300]) . '" onclick="return false;" data-title="পত্র নম্বর:  ' . $row['nothi_potro_page_bn'] . '" data-footer="" ><img class="zoomimg img-responsive"  src="' . $this->request->webroot . 'content/' . $row['file_name'] . '?token=' . $this->generateToken(['file' => $row['file_name']], ['exp' => time() + 60 * 300]) . '" alt="ছবি পাওয়া যায়নি"></a></div>';
                    }
                    $html .= "</div>";
                    $i++;
                }
                $html .= "</div></div>";
            } else {
                $html .= "<p class='text-center text-danger'>দুঃখিত! পত্র পাওয়া যায়নি।</p>";
            }
            echo json_encode(array('html' => $html));
            die;
        }
    }

    public function attachmentsForPotroAll($nothiMasterId, $potroId,
                                           $nothi_office = 0)
    {

        $employee_office = $this->getCurrentDakSection();
        $otherNothi = false;
        if ($nothi_office != 0 && $nothi_office != $employee_office['office_id']) {
            $otherNothi = true;

            $this->switchOffice($nothi_office, 'MainNothiOffice');
        } else {
            $nothi_office = $employee_office['office_id'];
        }
        $otherNothi = false;
        if ($nothi_office != 0 && $nothi_office != $employee_office['office_id']) {
            $otherNothi = true;

            $this->switchOffice($nothi_office, 'MainNothiOffice');
        } else {
            $nothi_office = $employee_office['office_id'];
        }
        $this->layout = null;

        if ($this->request->is('ajax')) {

            TableRegistry::remove('NothiParts');
            TableRegistry::remove('NothiPotroAttachments');
            $nothiPartsTable = TableRegistry::get("NothiParts");
            $nothiMasterPermissionsTable = TableRegistry::get("NothiMasterPermissions");

            $nothiPartsInformation = $nothiPartsTable->getAll(['NothiParts.id' => $nothiMasterId])->first();

            $allNothiMasterList = array();
            $allNothiMasterList = $nothiMasterPermissionsTable->getMasterNothiPartList($nothi_office,
                $employee_office['office_id'],
                $employee_office['office_unit_id'],
                $employee_office['office_unit_organogram_id'],
                $nothiPartsInformation['nothi_masters_id']);

            $nothiPartId = array();

            if (!empty($allNothiMasterList)) {
                foreach ($allNothiMasterList as $key => $value) {
                    $nothiPartId[] = $value['nothi_part_no'];
                }
            }

            $potroAttachmentTable = TableRegistry::get('NothiPotroAttachments');

            $potroAttachmentRecord = $potroAttachmentTable->find()->where(['nothi_part_no IN' => $nothiPartId,
                'nothi_potro_id' => $potroId, 'status' => 1])->order(['id desc'])->toArray();

            $html = '';

            if (!empty($potroAttachmentRecord)) {
                $i = 0;
                $html .= '
                    <div class="portlet box blue">
                    <div class="portlet-title">
<div class="caption">
 মোট: ' . Number::format(count($potroAttachmentRecord)) . 'টি
</div>
<div class="actions">
<ul class="pager potroAccordianPaginator" id="' . $potroId . '">
<li><button class="btn btn-sm btn-danger prevImageAccordian" ><</button> </li>
<li><button class="btn btn-sm btn-danger nextImageAccordian" >></button> </li>
</ul>
</div>
</div><div class="portlet-body">';
                foreach ($potroAttachmentRecord as $row) {

                    $html .= '<div  class="potroAccordianPaginationBody potro_details_accordian_div_' . $potroId . ($i
                        == 0 ? ' active' : '') . '"> <input class="potro_choose" type="checkbox" potro_page_no="' . $row['nothi_potro_page_bn'] . '" potro_page_id="' . $row['id'] . '"/> &nbsp;' . $row['nothi_potro_page_bn'] . '';

                    $row['attachment_type'] = str_replace("; charset=binary",
                        "", $row['attachment_type']);
                    if ($row['attachment_type'] == 'text') {
                        $html .= '<div style="background-color: #fff; max-width:950px; min-height:815px; margin:0 auto; page-break-inside: auto;">' . html_entity_decode($row['content_body']) . "</div>";
                    } elseif (substr($row['attachment_type'], 0, 5) != 'image') {
                        if (substr($row['attachment_type'], 0,
                                strlen('application/vnd')) == 'application/vnd' || substr($row['attachment_type'],
                                0, strlen('application/ms')) == 'application/ms'
                        ) {
                            $html .= '<iframe style=" width:100%; height: 700px;" src="https://docs.google.com/gview?url=' . FILE_FOLDER . $row['file_name'] . '&embedded=true" style="width:600px; height:500px;" frameborder="0"></iframe>';
                        } else {
                            $html .= '<embed src="' . $this->request->webroot . 'content/' . $row['file_name'] . '?token=' . $this->generateToken(['file' => $row['file_name']], ['exp' => time() + 60 * 300]) . '" style=" width:100%; height: 700px;" type="' . $row['attachment_type'] . '"></embed>';
                        }
                    } else {
                        $html .= '<div class="text-center img"><a href="' . $this->request->webroot . 'content/' . $row['file_name'] . '?token=' . $this->generateToken(['file' => $row['file_name']], ['exp' => time() + 60 * 300]) . '" onclick="return false;" data-title="পত্র নম্বর:  ' . $row['nothi_potro_page_bn'] . '" data-footer="" ><img class="zoomimg img-responsive"  src="' . $this->request->webroot . 'content/' . $row['file_name'] . '?token=' . $this->generateToken(['file' => $row['file_name']], ['exp' => time() + 60 * 300]) . '" alt="ছবি পাওয়া যায়নি"></a></div>';
                    }
                    $html .= "</div>";
                    $i++;
                }
                $html .= "</div></div>";
            } else {
                $html .= "<p class='text-center text-danger'>দুঃখিত! পত্র পাওয়া যায়নি।</p>";
            }

            echo json_encode(array('html' => $html));
            die;
        }
    }

    /*
     * End Nothi Masters
     */

    public function generateNothiNumber($isAdmin = 0)
    {
            $role_id = $this->Auth->user('user_role_id');
            if(!empty($this->request->data['officeId'])){
                $this->request->data['office_id'] = $this->request->data['officeId'];
            }
            if (($role_id == 1 || $role_id == 2) && !empty($this->request->data['office_id'])) {
                $this->switchOffice($this->request->data['office_id'], 'OfficeDB');
                $employee_office['office_id'] = (int)$this->request->data['office_id'];
                $employee_office['office_unit_id'] = (int)$this->request->data['office_unit_id'];
            } else {
                $employee_office = $this->getCurrentDakSection();
            }


        $id = (int)$this->request->data('id');

        $office_unit_id = !empty($this->request->data['office_unit_id']) ? (int)$this->request->data['office_unit_id'] : $employee_office['office_unit_id'];

        $officeTable = TableRegistry::get('Offices');
        $officeUnitTable = TableRegistry::get('OfficeUnits');
        TableRegistry::remove('NothiTypes');
        $nothiTypesTable = TableRegistry::get('NothiTypes');
        TableRegistry::remove('NothiParts');
        $nothiPartsTable = TableRegistry::get('NothiParts');
        TableRegistry::remove('NothiMasters');
        $nothiMastersTable = TableRegistry::get('NothiMasters');

//        $ministrycode = $employee_info['ministry_records']['reference_code'];
        $office_nothi_code = $officeTable->find()->select(['digital_nothi_code'])->where(['id' => $employee_office['office_id']])->first();

//        $nothi_code = $office_nothi_code['digital_nothi_code'];

        $query = $officeUnitTable->find()->select(['unit_nothi_code'])->where(['id' => $office_unit_id])->first();

        $unit_code = $query['unit_nothi_code'];

        $query = $nothiTypesTable->find()->select(['type_code', 'type_last_number'])->where(['id' => $id])->first();

        $thisyear = date("Y");

        $query3 = $nothiMastersTable->find()->select(['nothi_no'])->where(['nothi_types_id' => $id,
            'office_id' => $employee_office['office_id'], 'office_units_id' => $office_unit_id,
            'nothi_created_date >= ' => "{$thisyear}-01-01", 'nothi_created_date <= ' => "{$thisyear}-12-31"])->order(['id' => 'desc'])->first();

        $query3Data = [];
        if ($query3['nothi_no']) {
            $query3Data = explode('.', $query3['nothi_no']);
        }

        $nothiSerial = 0;

        if (!empty($query3Data) && count($query3Data) >= 7 && !empty($query3Data[5])) {
            $nothiSerial = (bnToen($query3Data[5]) + 1);
            $nothiSerial = str_pad(($nothiSerial), 3, "0", STR_PAD_LEFT);
        } else {
            $query4 = $nothiMastersTable->find()->select(['counttotal' => 'count(nothi_types_id)'])->where(['nothi_types_id' => $id,
                'office_id' => $employee_office['office_id'], 'office_units_id' => $office_unit_id,
                'nothi_created_date >=' => "{$thisyear}-01-01", 'nothi_created_date <=' => "{$thisyear}-12-31"])->first();

            $bn_digits = array('০', '১', '২', '৩', '৪', '৫', '৬', '৭', '৮', '৯');
            $nothiSerial = str_replace(range(0, 9), $bn_digits,
                (str_pad(($query4['counttotal'] + $query['type_last_number'] + 1),
                    3, "0", STR_PAD_LEFT)));
        }

        if($nothiSerial > 999){
            $nothiSerial = str_pad(1,
                3, "0", STR_PAD_LEFT);
        }

        $nothi_code = $office_nothi_code['digital_nothi_code']."." . $unit_code."." . str_pad(bnToen($query['type_code']), 2, "0", STR_PAD_LEFT) . "." . $nothiSerial . "." . Number::format(substr($thisyear,
                -2));

        while($nothiMastersTable->find()->where(['nothi_no' => enTobn($nothi_code)])->count() > 0){
            if($nothiSerial > 999){
                break;
            }
            $nothiSerial = str_pad(($nothiSerial + 1),
                3, "0", STR_PAD_LEFT);

            $nothi_code = $office_nothi_code['digital_nothi_code']."." . $unit_code."." . str_pad(bnToen($query['type_code']), 2, "0", STR_PAD_LEFT) . "." . $nothiSerial . "." . Number::format(substr($thisyear,
                    -2));
        }


        $this->response->body(json_encode(enTobn($nothi_code)));
        $this->response->type('application/json');
        return $this->response;
    }

    private function showPotroFlag($nothimasterid = [])
    {
        TableRegistry::remove('PotroFlags');
        $bookmarkedTable = TableRegistry::get('PotroFlags');

        $bookmarkedTablequery = $bookmarkedTable->find()
            ->select([
                'PotroFlags.nothi_master_id',
                'PotroFlags.nothi_part_no',
                'PotroFlags.color',
                'PotroFlags.title',
                'PotroFlags.page_no',
                'PotroFlags.potro_attachment_id',
                "nothi_potro_page_bn" => 'NothiPotroAttachments.nothi_potro_page_bn',
                "nothi_potro_page" => 'NothiPotroAttachments.nothi_potro_page'
            ])
            ->where(["PotroFlags.nothi_part_no IN" => $nothimasterid])
            ->join([
                'NothiPotroAttachments' => [
                    'table' => 'nothi_potro_attachments',
                    'type' => 'inner',
                    'conditions' => "NothiPotroAttachments.id = PotroFlags.potro_attachment_id"
                ]
            ])
            ->toArray();

        $returnData = array();

        if (!empty($bookmarkedTablequery)) {
            foreach ($bookmarkedTablequery as $key => $row) {
                $returnData[] = array(
                    'class' => $row['color'],
                    'attachment_id' => $row['potro_attachment_id'],
                    'potro_no' => $row['nothi_potro_page'],
                    'potro_no_bn' => $row['nothi_potro_page_bn'],
                    'nothi_master_id' => $row['nothi_master_id'],
                    'nothi_part_id' => $row['nothi_part_no'],
                    'title' => $row['title'],
                    'page' => $row['page_no']
                );
            }
        }

        return $returnData;
    }

    private function showPotroFlagAll($nothimasterid)
    {

        $this->set('id', $nothimasterid);
        TableRegistry::remove('PotroFlags');
        $bookmarkedTable = TableRegistry::get('PotroFlags');

        $bookmarkedTablequery = $bookmarkedTable->find()
            ->select([
                'PotroFlags.nothi_master_id',
                'PotroFlags.nothi_part_no',
                'PotroFlags.color',
                'PotroFlags.title',
                'PotroFlags.page_no',
                'PotroFlags.potro_attachment_id',
                "nothi_potro_page_bn" => 'NothiPotroAttachments.nothi_potro_page_bn',
                "nothi_potro_page" => 'NothiPotroAttachments.nothi_potro_page'
            ])
            ->where(["PotroFlags.nothi_part_no IN" => $nothimasterid])
            ->join([
                'NothiPotroAttachments' => [
                    'table' => 'nothi_potro_attachments',
                    'type' => 'inner',
                    'conditions' => "NothiPotroAttachments.id = PotroFlags.potro_attachment_id"
                ]
            ])
            ->toArray();

        $returnData = array();

        if (!empty($bookmarkedTablequery)) {
            foreach ($bookmarkedTablequery as $key => $row) {
                $returnData[] = array(
                    'class' => $row['color'],
                    'attachment_id' => $row['potro_attachment_id'],
                    'potro_no' => $row['nothi_potro_page'],
                    'potro_no_bn' => $row['nothi_potro_page_bn'],
                    'nothi_master_id' => $row['nothi_master_id'],
                    'nothi_part_id' => $row['nothi_part_no'],
                    'title' => $row['title'],
                    'page' => $row['page_no']
                );
            }
        }

        return $returnData;
    }

    public function getOtherOfficePermissionList()
    {
        /*
         * 1.
         */
        if ($this->request->is('ajax')) {

            $officeId = intval($this->request->data['office_id']);
            $id = intval($this->request->data['nothi_id']);
            $nothi_office = intval($this->request->data['nothi_office']);
            $officeUnitId = isset($this->request->data['office_unit_id']) ? intval($this->request->data['office_unit_id'])
                : 0;

            if (!empty($this->request->data['api'])) {
                $this->switchOffice($nothi_office, 'OfficeDB');
            }
            $officesTable = TableRegistry::get('Offices');
            $office_name = $officesTable->getOfficeName($officeId);

            $officeUnitsTable = TableRegistry::get('OfficeUnits');
            $nothiPriviligeTable = TableRegistry::get('NothiMasterPermissions');

            $employeeOfficeTable = TableRegistry::get('EmployeeOffices');


            if ($officeUnitId != 0)
                $unitLists = $officeUnitsTable->getOfficeUnitsinfo($officeId,
                    $officeUnitId);
            else $unitLists = $officeUnitsTable->getOfficeUnitsinfo($officeId);

            $data = '';

            if (!empty($unitLists)) {
                foreach ($unitLists as $unitList) {

                    $data .= '<div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a class="accordion-toggle accordion-toggle-styled " data-toggle="collapse" 
                                       data-parent="#accordion3" href="#other_collapse_' . $unitList['id'] . '" >' . h($unitList['unit_name_bng']) . '</a>
                                </h4>
                            </div>
                            <div id="other_collapse_' . $unitList['id'] . '" class="panel-collapse ">
                                <div class="panel-body">';

                    $employee = $employeeOfficeTable->getEmployeeOfficeRecordsByOfficeId($officeId,
                        $unitList['id']);

                    if (!empty($employee)) {
                        foreach ($employee as $ekey => $eVal) {

                            if (!empty($id)) {
                                $hasThisEmployeeAccess = $nothiPriviligeTable->hasAccessPartList($officeId,
                                    $eVal['office_unit_id'],
                                    $eVal['office_unit_organogram_id'], $id,
                                    $nothi_office);
                                if (intval($hasThisEmployeeAccess['privilige_type'])
                                    > 0
                                ) $eVal['check'] = 1;
                                else {
                                    $eVal['check'] = 0;
                                }
                            }
                            $checked = false;
//                            if(trim($eVal['designation']) == 'উপজেলা নির্বাহী অফিসার ' || trim($eVal['designation']) == 'উপজেলা নির্বাহী অফিসার'){
//                                $checked = true;
//                            }

                            if ($eVal['check'] == 1) {

                                $data .= '<input title="বাছাই করুন" type="checkbox" onclick=" list_all(' . $eVal['office_unit_organogram_id'] . ');" class="org_checkbox checkthis ' . ($checked ? 'checkclick' : '') . ' optionsothers" data-employee-incharge-label="' . $eVal['incharge_label'] . '" data-office-unit-id="' . $unitList['id'] . '"  data-office-unit-organogram-id="' . $eVal['office_unit_organogram_id'] . '"  data-unit-name="' . h($unitList['unit_name_bng']) . '"  data-unit-name-eng="' . h($unitList['unit_name_eng']) . '" data-office-name="' . h($office_name['office_name_bng']) . '" data-office-name-eng="' . h($office_name['office_name_eng']) . '" data-designation-name="' . h($eVal['designation']) . '" data-designation-name-eng="' . h($eVal['designation_eng']) . '" data-employee-name="' .
                                    h($eVal['name_bng']) . '" data-employee-name-eng="' . h($eVal['name_eng']) . '" data-employee-personal-email="' . h($eVal['personal_email']) . '" data-employee-personal-mobile="' . h($eVal['personal_mobile']) . '" data-employee-id="' . $eVal['employee_record_id'] . '" value="1" data-employee-office-id="' . $eVal['office_id'] . '" checked="checked" data-designation-level="' . h($eVal['designation_level']) . '" data-office-head="' . $eVal['office_head'] . '" > &nbsp; &nbsp;' . h($eVal['name_bng']) . ', ' . h($eVal['designation']) . ' <br>';
                            } else {

                                $data .= '<input title="বাছাই করুন" type="checkbox" onclick=" list_all(' . $eVal['office_unit_organogram_id'] . ');" class="org_checkbox checkthis optionsothers ' . ($checked ? 'checkclick' : '') . '"  data-employee-incharge-label="' . $eVal['incharge_label'] . '" data-office-unit-id="' . $unitList['id'] . '"
                                                   data-office-unit-organogram-id="' . $eVal['office_unit_organogram_id'] . '"  data-unit-name="' . h($unitList['unit_name_bng']) . '"  data-unit-name-eng="' . h($unitList['unit_name_eng']) . '" data-office-name="' . h($office_name['office_name_bng']) . '" data-office-name-eng="' . h($office_name['office_name_eng']) . '" data-designation-name="' . h($eVal['designation']) . '" data-designation-name-eng="' . h($eVal['designation_eng']) . '" data-employee-name="' . h($eVal['name_bng']) . '" data-employee-name-eng="' . h($eVal['name_eng']) . '" data-employee-personal-email="' . h($eVal['personal_email']) . '" data-employee-personal-mobile="' . h($eVal['personal_mobile']) . '" data-employee-id="' . $eVal['employee_record_id'] . '" 
                                                   value="1" data-employee-office-id="' . $eVal['office_id'] . '" data-designation-level="' . h($eVal['designation_level']) . '"  data-office-head="' . $eVal['office_head'] . '"> &nbsp;&nbsp;' . h($eVal['name_bng']) . ', ' . h($eVal['designation']) . ' <br>';
                            }
                        }
                    }

                    $data .= '</div> </div> </div>';
                }
            }

            echo $data;
        }
        die;
    }

    private function getSignatures($masterId, $nothiNoteId = 0)
    {

        TableRegistry::remove('NothiNoteSignatures');
        $noteSignatureTable = TableRegistry::get('NothiNoteSignatures');
        $employeeRecordsTable = TableRegistry::get('EmployeeRecords');
        $employeeOfficesTable = TableRegistry::get('EmployeeOffices');
        $userTable = TableRegistry::get('Users');

        $allSignatures = $noteSignatureTable->find()->where(['nothi_part_no' => $masterId]);

        if (!empty($nothiNoteId)) {
            $allSignatures = $allSignatures->where(['nothi_note_id' => $nothiNoteId]);
        }

        $allSignatures = $allSignatures->toArray();

        $returnData = array();
        if (!empty($allSignatures)) {
            foreach ($allSignatures as $key => $value) {
                $employeeRecords = $employeeRecordsTable->get($value['employee_id']);
                $employeeinfo = $employeeOfficesTable->getOfficeDesignation($value['employee_id'], $value['office_organogram_id']);
                $userInfo = $userTable->find()->where(['employee_record_id' => $value['employee_id']])->first();

                $englishdate = new Time($value['signature_date']);
                $options['token'] = sGenerateToken(['file' => $userInfo['username']], ['exp' => time() + 60 * 300]);
                $data = $this->getSignature($userInfo['username'], 1, 1, false, $englishdate->i18nFormat("Y-M-d H:m:s", null, 'en-US'), $options);

                $returnData[$value['nothi_note_id']][] = array(
                    'employee_designation' => $employeeinfo['designation'] . (!empty($employeeinfo['incharge_label']) ? (" (" . $employeeinfo['incharge_label'] . ')') : ''),
                    'office_id' => $value['office_id'],
                    'office_unit_id' => $value['office_unit_id'],
                    'office_organogram_id' => $value['office_organogram_id'],
                    'employee_id' => $value['employee_id'],
                    'note_decision' => $value['note_decision'],
                    'cross_signature' => $value['cross_signature'],
                    'signature_date' => $value['signature_date'],
                    'is_signature' => $value['is_signature'],
                    'cross_signature' => $value['cross_signature'],
                    'digital_sign' => $value['digital_sign'],
                    'signature' => $employeeRecords['d_sign'],
                    'name' => $employeeRecords['name_bng'],
                    'userInfo' => $userInfo['username'],
                    'signature_b64' => $data,
                );
            }
        }

        return $returnData;
    }

    public function getDakMovementSequence($dak_id, $dak_type = DAK_DAPTORIK)
    {
        TableRegistry::remove('DakMovements');
        $table_instance_dak = TableRegistry::get('DakMovements');
        $dak = $table_instance_dak->find()->select(['sequence'])->where(['dak_id' => $dak_id,
            'dak_type' => $dak_type])->order(['created desc', 'id desc'])->first();
        return (intval($dak['sequence']) + 1);
    }

    public function showPopUp($id, $type = 'note', $masterid = 0, $page = 0, $attachment_office_id = 0)
    {
        $token = $this->request->query['token'];
        $nothi_part = (int)$this->request->query['nothi_part'];

        if(empty($token)){
            echo 'Invalid Request. Err: 1';
            die;
        }
        $tokenDecode = $this->getTokenDetail($token);
        if(!empty($tokenDecode['data'])){

            $decrypt_data = $this->getDecryptedData($tokenDecode['data']['file_access_key']);

            if($nothi_part != $decrypt_data){
                echo 'Invalid Request. Err: 2';
                die;
            }
        } else {
            echo 'Invalid Request. Err: 3';
            die;
        }
        $nothi_office = 0;
        if (isset($this->request->data['nothi_office'])) {
            $nothi_office = intval($this->request->data['nothi_office']);
        }

        if($type != 'guard-attachment' && $type != 'potrojariAttachmentRef') {
            $employee_office = $this->getCurrentDakSection();
            if(empty($employee_office)){
                if(empty($tokenDecode['data']['office_id']) || empty($tokenDecode['data']['office_unit_id']) || empty($tokenDecode['data']['office_unit_organogram_id'])){
                    echo 'Invalid Request. Err: 4';
                    die;
                }
                $employee_office['office_id'] = $tokenDecode['data']['office_id'];
                $employee_office['office_unit_id'] = $tokenDecode['data']['office_unit_id'];
                $employee_office['office_unit_organogram_id'] = $tokenDecode['data']['office_unit_organogram_id'];
            }
            if(empty($nothi_office)){
                $nothi_office= $employee_office['office_id'];
            }
            TableRegistry::remove('NothiMasterPermissions');
            $this->switchOffice($nothi_office,'officeDb');
            $nothiMasterPermissionsTable = TableRegistry::get("NothiMasterPermissions");

            $has_access = $nothiMasterPermissionsTable->hasAccessPartList($employee_office['office_id'],
                $employee_office['office_unit_id'],
                $employee_office['office_unit_organogram_id'],
                $nothi_part,
                $nothi_office);

            if (empty($has_access)) {
                echo 'Invalid Request. Err: 5';
                die;
            }
        }


        $this->layout = null;


        $data_ref = '';
        if (isset($this->request->query['data_ref'])) {
            $data_ref = $this->request->query['data_ref'];
        } else if (isset($this->request->data['data_ref'])) {
            $data_ref = $this->request->data['data_ref'];
        }

        $userAgent = env('HTTP_USER_AGENT');
        $isMobile = preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $userAgent);

        if ($type == 'note') {
            $this->OnuccedPage($id, $nothi_office, $data_ref);
        } elseif ($type == 'potaka') {
            $this->potro_popup($id, $masterid, $nothi_office, $page, $data_ref, $isMobile);
        } elseif ($type == 'potro') {
            $this->potro_popup($id, $masterid, $nothi_office, 0, $data_ref, $isMobile);
        } elseif ($type == 'showAttachment') {
            $this->showAttachment($id, $masterid, $nothi_office, $data_ref, $isMobile);
        } elseif ($type == 'showPotroAttachment') {
            $this->showPotroAttachment($id, $masterid, $nothi_office, $data_ref, $isMobile);
        } elseif ($type == 'guard-attachment') {
            $this->showGuardAttachment($id, $masterid, $nothi_office, $page, $isMobile);
        } elseif($type == 'potrojariAttachmentRef'){
            $this->showPotrojariAttachmentRef($id, $masterid, $nothi_office, $data_ref, $isMobile, $attachment_office_id);
        } else {
            $this->file_popup($id, $masterid, $nothi_office, $data_ref, $isMobile);
        }
    }

    public function showAttachment($id, $masterid, $nothi_office = 0, $data_ref = '',$isMobile= false)
    {

        $employee_office = $this->getCurrentDakSection();
        if (empty($employee_office['office_id'])) {
            if (!empty($this->request->query['user_designation'])) {
                $this->request->query['user_designation'] = intval($this->request->query['user_designation']);
                $employee_office = $this->setCurrentDakSection($this->request->query['user_designation']);
            } elseif (!empty($this->request->data['user_designation'])) {
                $this->request->data['user_designation'] = intval($this->request->data['user_designation']);
                $employee_office = $this->setCurrentDakSection($this->request->data['user_designation']);
            } else {
                //die("Invalid request");
            }
        }
        $otherNothi = false;
        if (($nothi_office != 0 && $nothi_office != $employee_office['office_id']) || ($nothi_office != 0 && $data_ref == 'api')) {
            $otherNothi = true;

            $this->switchOffice($nothi_office, 'MainNothiOffice');
        } else {
            $nothi_office = $employee_office['office_id'];
        }
        $this->view = 'file_popup';
        $this->layout = null;
        $this->set('nothi_office',$nothi_office);
        TableRegistry::remove('NothiNoteAttachments');
        $table = TableRegistry::get('NothiNoteAttachments');

        $data = $table->get($id);
        if (!empty($data['file_name'])) {
            $token = $this->generateToken([
                'office_unit_organogram_id' => $employee_office['office_unit_organogram_id'],
                'file' => $data['file_name'],
            ], ['exp' => time() + 60 * 300]);
            $this->set('token', $token);
        }


        if ($masterid != $data->nothi_part_no) {
            $data = [];
        }

        $this->set('data', $data);
        $this->set('isMobile', $isMobile);
        $this->set('name', 'downloadNote');
    }

    public function showPotroAttachment($id, $masterid = 0, $nothi_office = 0, $data_ref = '', $isMobile= false)
    {
        $employee_office = $this->getCurrentDakSection();

        if (empty($employee_office['office_id'])) {
            if (!empty($this->request->query['user_designation'])) {
                $this->request->query['user_designation'] = intval($this->request->query['user_designation']);
                $employee_office = $this->setCurrentDakSection($this->request->query['user_designation']);
            } elseif (!empty($this->request->data['user_designation'])) {
                $this->request->data['user_designation'] = intval($this->request->data['user_designation']);
                $employee_office = $this->setCurrentDakSection($this->request->data['user_designation']);
            } else {
                //die("Invalid request");
            }
        }
        $otherNothi = false;
        if (($nothi_office != 0 && $nothi_office != $employee_office['office_id']) || ($nothi_office != 0 && $data_ref == 'api')) {
            $otherNothi = true;

            $this->switchOffice($nothi_office, 'MainNothiOffice');
        } else {
            $nothi_office = $employee_office['office_id'];
        }

        $this->view = 'file_popup';
        $this->layout = null;
        TableRegistry::remove('PotrojariAttachments');
        $table = TableRegistry::get('PotrojariAttachments');
        $data = $table->find()->select(['attachment_type', 'file_name','id'])->where(['id' => $id])->first();

        if (!empty($data['file_name'])) {
            $token = $this->generateToken([
                'office_unit_organogram_id' => $employee_office['office_unit_organogram_id'],
                'file' => $data['file_name'],
            ], ['exp' => time() + 60 * 300]);
            $this->set('token', $token);
        }

        $this->set('data', $data);
        $this->set('isMobile', $isMobile);
        $this->set('nothi_office',$nothi_office);
        $this->set('name', 'downloadPotrojari');
    }

    public function showPotrojariAttachmentRef($id, $masterid = 0, $nothi_office = 0, $data_ref = '', $isMobile= false, $attachment_office_id = 0)
    {
        $employee_office = $this->getCurrentDakSection();

        if (empty($employee_office['office_id'])) {
            if (!empty($this->request->query['user_designation'])) {
                $this->request->query['user_designation'] = intval($this->request->query['user_designation']);
                $employee_office = $this->setCurrentDakSection($this->request->query['user_designation']);
            } elseif (!empty($this->request->data['user_designation'])) {
                $this->request->data['user_designation'] = intval($this->request->data['user_designation']);
                $employee_office = $this->setCurrentDakSection($this->request->data['user_designation']);
            } else {
                //die("Invalid request");
            }
        }
        $otherNothi = false;
        if (($attachment_office_id != 0 && $attachment_office_id != $employee_office['office_id']) || ($attachment_office_id != 0 && $data_ref == 'api')) {
            $otherNothi = true;

            $this->switchOffice($attachment_office_id, 'MainNothiOffice');
        } else {
            $nothi_office = $employee_office['office_id'];
        }

        $this->view = 'file_popup';
        $this->layout = null;
        TableRegistry::remove('nothiPotrojariAttachmentRefs');
        $table = TableRegistry::get('nothiPotrojariAttachmentRefs');
        $data = $table->find()->select(['attachment_type', 'file_name','id'])->where(['id' => $id])->first();

        if (!empty($data['file_name'])) {
            $token = $this->generateToken([
                'office_unit_organogram_id' => $employee_office['office_unit_organogram_id'],
                'file' => $data['file_name'],
            ], ['exp' => time() + 60 * 300]);
            $this->set('token', $token);
        }

        $this->set('data', $data);
        $this->set('isMobile', $isMobile);
        $this->set('nothi_office',$nothi_office);
        $this->set('name', 'downloadPotrojariAttachmentRef');
    }

    public function file_popup($id, $masterid, $nothi_office = 0, $data_ref = '', $isMobile= false)
    {
        $employee_office = $this->getCurrentDakSection();
        if (empty($employee_office['office_id'])) {
            if (!empty($this->request->query['user_designation'])) {
                $this->request->query['user_designation'] = intval($this->request->query['user_designation']);
                $employee_office = $this->setCurrentDakSection($this->request->query['user_designation']);
            } elseif (!empty($this->request->data['user_designation'])) {
                $this->request->data['user_designation'] = intval($this->request->data['user_designation']);
                $employee_office = $this->setCurrentDakSection($this->request->data['user_designation']);
            } else {
                //die("Invalid request");
            }
        }
        $otherNothi = false;
        if (($nothi_office != 0 && $nothi_office != $employee_office['office_id']) || ($nothi_office != 0 && $data_ref == 'api')) {
            $otherNothi = true;

            $this->switchOffice($nothi_office, 'MainNothiOffice');
        } else {
            $nothi_office = $employee_office['office_id'];
        }
        $this->view = 'file_popup';
        $this->layout = null;
        TableRegistry::remove('NothiNoteAttachmentRefs');
        $table = TableRegistry::get('NothiNoteAttachmentRefs');
        $data = $table->find()->select(['attachment_type', 'file_name','id'])->where(['id' => $id,
            'nothi_part_no' => $masterid])->first();
        if (!empty($data['file_name'])) {
            $token = $this->generateToken([
                'office_unit_organogram_id' => $employee_office['office_unit_organogram_id'],
                'file' => $data['file_name'],
            ], ['exp' => time() + 60 * 300]);
            $this->set('token', $token);
        }
        $this->set('isMobile', $isMobile);
        $this->set('data', $data);
        $this->set('nothi_office', $nothi_office);
        $this->set('name', 'downloadRefs');

    }

    public function OnuccedPage($nothi_note_no, $nothi_office = 0, $data_ref = '')
    {

        $employee_office = $this->getCurrentDakSection();
        if (empty($employee_office['office_id'])) {
            if (!empty($this->request->query['user_designation'])) {
                $this->request->query['user_designation'] = intval($this->request->query['user_designation']);
                $employee_office = $this->setCurrentDakSection($this->request->query['user_designation']);
            } elseif (!empty($this->request->data['user_designation'])) {
                $this->request->data['user_designation'] = intval($this->request->data['user_designation']);
                $employee_office = $this->setCurrentDakSection($this->request->data['user_designation']);
            } else {
                //die("Invalid request");
            }
        }
        $otherNothi = false;
        if (($nothi_office != 0 && $nothi_office != $employee_office['office_id']) || ($nothi_office != 0 && $data_ref == 'api')) {
            $otherNothi = true;

            $this->switchOffice($nothi_office, 'MainNothiOffice');
        } else {
            $nothi_office = $employee_office['office_id'];
        }


        $this->set('nothi_office', $nothi_office);
        $this->set(compact('otherNothi'));

        $this->view = 'onucced_page';
        $this->layout = null;
        TableRegistry::remove('NothiNotes');
        TableRegistry::remove('NothiParts');
        $noteTable = TableRegistry::get('NothiNotes');

        $notesquery = $noteTable->find()
            ->select(['NothiNotes.nothi_master_id', 'NothiNotes.nothi_part_no',
                'nothi_part_no_en' => 'NothiParts.nothi_part_no', 'nothi_part_no_bn' => 'NothiParts.nothi_part_no_bn',
                'NothiNotes.nothi_notesheet_id', 'NothiNotes.note_no', 'NothiNotes.id',
                'NothiNotes.nothi_notesheet_id', 'NothiNotes.subject', 'NothiNotes.note_description',
                'NothiNotes.note_status', 'NothiNotes.is_potrojari', 'NothiNotes.potrojari_status', 'NothiNotes.digital_sign', 'NothiNotes.sign_info',
                'NothiNotes.created', 'NothiNotes.modified'])
            ->where(['NothiNotes.id' => $nothi_note_no])->join([
                'NothiParts' => [
                    'table' => 'nothi_parts',
                    "conditions" => "NothiParts.id= NothiNotes.nothi_part_no",
                    "type" => "INNER"
                ]
            ])->first();

        TableRegistry::remove('NothiNoteAttachments');
        $noteAttachments = TableRegistry::get('NothiNoteAttachments');

        $noteAttachmentsquery = $noteAttachments->find()->where(['nothi_part_no' => $notesquery['nothi_part_no'],
            'note_no' => $nothi_note_no])->toArray();

        $noteAttachmentsmap = array();
        if (!empty($noteAttachmentsquery)) {
            foreach ($noteAttachmentsquery as $key => $value) {
                $noteAttachmentsmap[$value['note_no']][] = array(
                    'user_file_name' => $value['user_file_name'],
                    'attachment_type' => $value['attachment_type'],
                    'file_name' => $value['file_name'],
                    'id' => $value['id'],
                    'digital_sign' => $value['digital_sign'],
                    'sign_info' => $value['sign_info'],
                );
            }
        }

        $nothi_note_sheets_id = $notesquery['nothi_notesheet_id'];
        $this->set('id', $notesquery['id']);
        $this->set(compact('notesquery', 'nothi_note_sheets_id',
            'noteAttachmentsmap'));

        // This Query[token] conflicts with getSignatures Query[token]
        $this->request->query['token'] = '';
        $this->set('signatures',
            $this->getSignatures($notesquery['nothi_part_no'], $nothi_note_no));

        $nothiMasterPermissionsTable = TableRegistry::get("NothiMasterPermissions");

        $employeeOfficeDesignation = array();
        $allPermittedUser = $nothiMasterPermissionsTable->getMasterNothiListbyMasterId($notesquery['nothi_master_id'],
            $nothi_office);

        if (!empty($allPermittedUser)) {
            foreach ($allPermittedUser as $key => $value) {
                $employeeOfficeDesignation[$value['office_unit_organograms_id']]
                    = array(
                    $value['designation_level'], 1
                );
            }
        }

        $this->set('employeeOfficeDesignation', $employeeOfficeDesignation);
    }

    public function potro_popup($id, $masterid = 0, $nothi_office = 0, $page = 0, $data_ref = '', $isMobile= false)
    {
        $employee_office = $this->getCurrentDakSection();
        if (empty($employee_office['office_id'])) {
            if (!empty($this->request->query['user_designation'])) {
                $this->request->query['user_designation'] = intval($this->request->query['user_designation']);
                $employee_office = $this->setCurrentDakSection($this->request->query['user_designation']);
            } elseif (!empty($this->request->data['user_designation'])) {
                $this->request->data['user_designation'] = intval($this->request->data['user_designation']);
                $employee_office = $this->setCurrentDakSection($this->request->data['user_designation']);
            } else {
                //die("Invalid request");
            }
        }
        $otherNothi = false;
        if (($nothi_office != 0 && $nothi_office != $employee_office['office_id']) || ($nothi_office != 0 && $data_ref == 'api')) {
            $otherNothi = true;

            $this->switchOffice($nothi_office, 'MainNothiOffice');
        } else {
            $nothi_office = $employee_office['office_id'];
        }

        $this->set('nothi_office', $nothi_office);
        $this->set(compact('otherNothi'));
        $this->view = 'potro_popup';

        TableRegistry::remove('NothiPotroAttachments');
        $tableNothiPotroAttachment = TableRegistry::get('NothiPotroAttachments');
        if (!empty($masterid)) {
            $file = $tableNothiPotroAttachment->find()->where(['nothi_master_id' => $masterid,
                'id' => $id,
                'status' => 1])->first();
        } else {
            $file = $tableNothiPotroAttachment->find()->where(['id' => $id, 'status' => 1])->first();
        }

        $this->set('row', $file);
        $this->set('page', $page);
        $this->set('isMobile', $isMobile);
    }

    public function showGuardAttachment($id = 0, $masterid = 0,
                                        $nothi_office = 0, $page = 0, $isMobile = false)
    {
        //$employee_office = $this->getCurrentDakSection();
        if (empty($employee_office['office_id'])) {
            if (!empty($this->request->query['user_designation'])) {
                $this->request->query['user_designation'] = intval($this->request->query['user_designation']);
                //   $employee_office = $this->setCurrentDakSection($this->request->query['user_designation']);
            } elseif (!empty($this->request->data['user_designation'])) {
                $this->request->data['user_designation'] = intval($this->request->data['user_designation']);
//                $employee_office = $this->setCurrentDakSection($this->request->data['user_designation']);
            } else {
                //die("Invalid request");
            }
        }

        $page = bnToen($page);
        $this->view = 'potro_popup';

        $tableGuardFileAttachments = TableRegistry::get('GuardFileAttachments');

        $file = array();

        if (!empty($id)) {
            $file = $tableGuardFileAttachments->get($id);
        }

        $this->set('row', $file);
        $this->set('page', $page);
        $this->set('isMobile', $isMobile);

        $this->set('nothi_office',$nothi_office);
        $this->set('name','downloadGuardFile');
    }

    public function getPart($id = null, $nothijato = 0)
    {
        if (empty($id)) {
            echo '<tr><td></td></tr>';
            die;
        }

        $employee_office = $this->getCurrentDakSection();
        $nothiPartsTable = TableRegistry::get('NothiParts');
        $nothiCurrentTable = TableRegistry::get('NothiMasterCurrentUsers');

        $currentNothi = $nothiCurrentTable->find('list',
            ['keyField' => 'id', 'valueField' => 'nothi_part_no'])->where(['NothiMasterCurrentUsers.office_id' => $employee_office['office_id'],
            'NothiMasterCurrentUsers.office_unit_id' => $employee_office['office_unit_id'],
            'NothiMasterCurrentUsers.office_unit_organogram_id' => $employee_office['office_unit_organogram_id'],
            'nothi_master_id' => $id, 'nothi_office' => $employee_office['office_id']])->toArray();

        $allNothiParts = $nothiPartsTable->find()->select(['id', 'nothi_part_no_bn',
            'NothiNotes.subject'])
            ->join([
                'NothiNotes' => [
                    'table' => 'nothi_notes',
                    'type' => 'left',
                    'conditions' => "NothiNotes.nothi_part_no = NothiParts.id AND NothiNotes.note_no=0"
                ]
            ])
            ->where(['NothiParts.nothi_masters_id' => $id, 'NothiParts.id IN' => $currentNothi])->toArray();

        $selectPart = -1;

        if ($nothijato == 0) {
            $html = '<tr  class="selectPartNo active"><td class="newPartCreate" nothi_parts_id=0 nothi_masters_id=' . $id . '><i class="fs0 a2i_gn_add2"></i> নতুন নোট </td></tr>';
        } else {
            $selectPart = 0;
            if (!empty($allNothiParts)) {
                foreach ($allNothiParts as $key => $value) {
                    if (empty($value['NothiNotes']['subject'])) {
                        $selectPart = $key;
                        break;
                    }
                }
            }
            $html = '';
        }

        if (!empty($allNothiParts)) {
            foreach ($allNothiParts as $key => $value) {
                if ($key == $selectPart) {
                    $html .= '<tr class="selectPartNo active"><td nothi_masters_id=' . $id . ' nothi_parts_id=' . $value['id'] . '><i class="fs0 a2i_gn_note1"></i> ' . ($value['NothiNotes']['subject'] . ' (' . $value['nothi_part_no_bn']) . ')' . ' </td></tr>';
                } else {
                    $html .= '<tr class="selectPartNo "><td nothi_masters_id=' . $id . ' nothi_parts_id=' . $value['id'] . '><i class="fs0 a2i_gn_note1"></i> ' . ($value['NothiNotes']['subject'] . ' (' . $value['nothi_part_no_bn']) . ')' . ' </td></tr>';
                }
            }
        }

        echo $html;

        die;
    }

    public function getFirstPart($id = 0, $nothijato = 0)
    {
        if (empty($id) && empty($nothijato)) {
            $getFirstPart['id'] = 0;
            goto rtn;
        }
        $nothiPartsTable = TableRegistry::get('NothiParts');
        $getFirstPart = $nothiPartsTable->firstNote($id);
        rtn:
        $this->response->type('application/json');
        $this->response->body(json_encode($getFirstPart['id']));
        return $this->response;
    }

    public function showForwaredUsers($nothiMasterId = 0, $nothi_office = 0, $from_save_and_send = 0)
    {

        if (empty($nothiMasterId)) {
            die;
        }

        $employee_office = $this->getCurrentDakSection();
        $otherNothi = false;
        if ($nothi_office != 0 && $nothi_office != $employee_office['office_id']) {
            $otherNothi = true;

            $this->switchOffice($nothi_office, 'MainNothiOffice');
        } else {
            $nothi_office = $employee_office['office_id'];
        }

        $this->set('nothi_office', $nothi_office);
        $this->set('otherNothi', $otherNothi);

        TableRegistry::remove('NothiParts');
        $nothiMasterPermissionsTable = TableRegistry::get("NothiMasterPermissions");
        $nothiPartsTable = TableRegistry::get('NothiParts');
        $nothiInformation = $nothiPartsTable->get($nothiMasterId);

        $nothiMasterPermissionsRecord = $nothiMasterPermissionsTable->find()->where(['nothi_masters_id' => $nothiInformation['nothi_masters_id'],
            'nothi_part_no' => $nothiInformation['id'], 'nothi_office' => $nothi_office])->order(['designation_level asc'])->toArray();

        $html = "";

        if (!empty($nothiMasterPermissionsRecord)) {

            $nothiMasterTable = TableRegistry::get('NothiMasters');
            $employeeOfficeTable = TableRegistry::get('EmployeeOffices');
            $employeeRecordTable = TableRegistry::get('EmployeeRecords');

            $officeTable = TableRegistry::get('Offices');

            $nothiRecord = $nothiPartsTable->getAll(['NothiParts.id' => $nothiMasterId])->first();

            $officeUnitTable = TableRegistry::get('OfficeUnits');
            $unitInformation = $officeUnitTable->get($nothiRecord['office_units_id']);

            $html .= "<div class='portlet box blue'>
<div class='portlet-title'>
        <div class='caption'>
                     শাখা:  {$unitInformation['unit_name_bng']}; নথি নম্বর:  {$nothiRecord['nothi_no']}; বিষয়: {$nothiRecord['subject']}
                  
         </div>
        <div class='actions form-inline'>
            <select class='form-control' id='priority'>
                <option value='0'>--নোট প্রেরণের অগ্রাধিকার বাছাই --</option>
               <option value='1'> জরুরি</option>
              </select>
            <a  title=' অনুমতি সংশোধন' nothi_master_id='  " . $nothiMasterId . " ' class='btn btn-sm green round-corner-5 btn-edit-permission' >
                <i class='fs1 a2i_gn_secrecy3'></i> অনুমতি সংশোধন
            </a>
            <a  title='রিফ্রেশ করুন'  nothi_master_id='" . $nothiMasterId . "'  nothi_office='" . $nothi_office . "' class=' btn   hidden btn-sm purple" . (($from_save_and_send == 1) ? ' btn-save-forward-nothi' : ' btn-forward-list') . "' >
                            <i class='fa fa-refresh'></i> প্রেরণ তালিকা  রিফ্রেশ 
            </a>
     </div>
</div>
<div class='portlet-body'>
<div id='user_tree'><ul><li> <ul>
";
            $i = 0;
            foreach ($nothiMasterPermissionsRecord as $key => $nothiUserInfo) {
                $employeeOfficeRecord = $employeeOfficeTable->find()->where(['office_id' => $nothiUserInfo['office_id'],
                    'office_unit_id' => $nothiUserInfo['office_unit_id'], 'office_unit_organogram_id' => $nothiUserInfo['office_unit_organograms_id'],
                    'office_unit_organogram_id <> ' => $employee_office['office_unit_organogram_id'],
                    'status' => 1])->first();

                if (!empty($employeeOfficeRecord)) {
                    $employeeRecord = $employeeRecordTable->get($employeeOfficeRecord['employee_record_id']);

                    $officeUnitRecord = $officeUnitTable->get($nothiUserInfo['office_unit_id']);

                    $officeInfo = array();
                    if ($nothiUserInfo['office_id'] != $employee_office['office_id']) {
                        $officeInfo = $officeTable->get($nothiUserInfo['office_id']);
                    }

                    $html .= "<li data-jstree='{" . ($i == 0 ? '"selected":true' : "") . "}'> <a type='radio' to_office_id={$nothiUserInfo['office_id']} to_unit_id={$nothiUserInfo['office_unit_id']} to_unit_name='{$officeUnitRecord['unit_name_bng']}' to_officer_name='{$employeeRecord['name_bng']}'
to_org_id={$nothiUserInfo['office_unit_organograms_id']}
 to_org_name='{$employeeOfficeRecord->designation}' incharge-label = '{$employeeOfficeRecord->incharge_label}'
to_officer_id={$employeeRecord['id']} nothi_office={$nothi_office} > " . $employeeRecord['name_bng'] . ', ' . $employeeOfficeRecord->designation . (!empty($employeeOfficeRecord->incharge_label) ? (' (' . $employeeOfficeRecord->incharge_label . ')') : '') . ', ' . $officeUnitRecord['unit_name_bng'] . (!empty($officeInfo) ? (", " . $officeInfo['office_name_bng']) : '') . "</a></li>";

                    $i++;
                }
            }
        }
        echo $html .= "
</ul></li></ul></div></div>
</div>";

        die;
    }

//summary nothi functions

    public function showSonglap($id, $nothi_office = 0)
    {
        $employee_office = $this->getCurrentDakSection();
        $otherNothi = false;
        if ($nothi_office != 0 && $nothi_office != $employee_office['office_id']) {
            $otherNothi = true;

            $this->switchOffice($nothi_office, 'MainNothiOffice');
        } else {
            $nothi_office = $employee_office['office_id'];
        }
        if ($this->request->is('ajax')) {
            TableRegistry::remove('NothiPotroAttachments');
            $tableNothiPotroAttachment = TableRegistry::get('NothiPotroAttachments');

            $file = $tableNothiPotroAttachment->get($id);
            $file->file_name = $file->file_name . '?token=' . $this->generateToken(['file' => $file->file_name], ['exp' => time() + 60 * 300]);

            echo json_encode($file);
        }

        die;
    }

    /** For Saving Data in Nothi_attachment_refs table* */
    public function save_nothi_attachment_refs()
    {

        if ($this->request->is('post')) {
            $data = [
                'status' => 'error',
            ];
            $this->switchOffice($this->request->data['nothi_office'],
                'MainNothiOffice');
            $table = TableRegistry::get('NothiNoteAttachmentRefs');
            $nothi_attachment_refs = $table->newEntity();
            $nothi_attachment_refs->nothi_master_id = $this->request->data['id'];
            $nothi_attachment_refs->nothi_part_no = $this->request->data['part_no'];
            $nothi_attachment_refs->attachment_type = $this->request->data['type'];
            $file = trim(str_replace(FILE_FOLDER,'',$this->request->data['file_name']));
            $file = explode('?token=', $file);
            if (!empty($file[0])) {
                $file = $file[0];
            } else {
                $file = trim(str_replace(FILE_FOLDER,'',$this->request->data['file_name']));
            }
            $nothi_attachment_refs->file_name = $file;
            if ($table->save($nothi_attachment_refs)) {
                $id = $nothi_attachment_refs->id;
                $data = [
                    'status' => 'success',
                    'id' => $id,
                ];
            }
            $this->response->body(json_encode($data));
            $this->response->type('application/json');
            return $this->response;
        }
        echo 'Invalid Request';
        die;
    }

    public function saveNothiPotrojariAttachmentRef()
    {
        if ($this->request->is('post')) {
            $data = [
                'status' => 'error',
            ];
            $this->switchOffice($this->request->data['nothi_office'],
                'MainNothiOffice');
            $table = TableRegistry::get('nothiPotrojariAttachmentRefs');
            $nothi_attachment_refs = $table->newEntity();
            $nothi_attachment_refs->office_id = (int)$this->request->data['nothi_office'];
            $nothi_attachment_refs->nothi_part_no = (int)$this->request->data['part_no'];
            $nothi_attachment_refs->attachment_type = $this->request->data['type'];
            $file = explode('?token=', $this->request->data['file_name']);
            if (!empty($file[0])) {
                $file = $file[0];
            } else {
                $file = $this->request->data['file_name'];
            }
            $nothi_attachment_refs->file_name = $file;
            if ($table->save($nothi_attachment_refs)) {
                $id = $nothi_attachment_refs->id;
                $data = [
                    'status' => 'success',
                    'id' => $id,
                ];
            }
            $this->response->body(json_encode($data));
            $this->response->type('application/json');
            return $this->response;
        }
        echo 'Invalid Request';
        die;
    }

    private function potrojari_template_list($status = 1)
    {

        $potrojari_templates = TableRegistry::get('PotrojariTemplates');
        $query = $potrojari_templates->find('list',
            ['keyField' => 'id', 'valueField' => 'template_name'])->select(["id",
            "template_name"])->where(['status' => $status])->toArray();

        return $query;
    }

    public function other_user()
    {
        $master_id = $this->request->data['master_id'];
        $nothi_office = $this->request->data['nothi_office'];
        $api = isset($this->request->data['api']) ? $this->request->data['api'] : false;

        try {
            if ($api) {
                $this->switchOffice($nothi_office, 'OfficeDB');
                $selectedOffice = isset($this->request->data['employee_office']) ? ($this->request->data['employee_office']) : ['office_id' => $nothi_office];
            } else {
                $selectedOffice = $this->getCurrentDakSection();
            }

            $nothi_master_permissions = TableRegistry::get('NothiMasterPermissions');
            $data = $nothi_master_permissions->get_other_offices($selectedOffice['office_id'],
                $master_id, $nothi_office);

            $html = '';
            $employee = TableRegistry::get('EmployeeRecords');
            $unit = TableRegistry::get('OfficeUnits');
            $employeeOffices = TableRegistry::get('employeeOffices');

//        $all_units_of_this_office= $unit->getOfficeUnitsList($of)
            if (!empty($data)) {
                foreach ($data as $key => $val) {

                    $employee_record = $employeeOffices->find()->select(['employee_record_id',
                        'office_id', 'office_unit_id', 'office_unit_organogram_id',
                        'designation', 'designation_level'])
                        ->where(['status' => 1, 'office_id' => $val['office_id'],
                            'office_unit_id' => $val['office_unit_id'], 'office_unit_organogram_id' => $val['office_unit_organograms_id']])->first();


                    $unit_name = $unit->find()->select(['unit_name_bng'])->where(['id' => $val['office_unit_id']])->first();
                    $name = $employee->find()->where(['id' => $employee_record['employee_record_id']])
                        ->select(['name_bng'])->first();
                    $html .= '<tr><td style="width: 30px;text-align: center;">&nbsp;&nbsp;';
                    $html .= '<input type="hidden" class="other_office_permission" name="office-employee" data-office-id="' . $val['office_id'] . '"' .
                        'data-office-employee-name="' . $name['name_bng'] . '" data-office-employee-id="' . $employee_record['employee_record_id'] . '"' .
                        ' data-office-unit-id="' . $val['office_unit_id'] . '" data-office-unit-organogram-id="' . $employee_record['office_unit_organogram_id'] . '" ' .
                        'data-designation-name="' . $employee_record['designation'] . '" data-unit-name="' . $unit_name['unit_name_bng'] . '"data-designation-level ="' . $employee_record['designation_level'] . '"></td><td>&nbsp;&nbsp;' . $name['name_bng'] . ', ' . $employee_record['designation'] . ', ' . $unit_name['unit_name_bng'] . '</td><td><input id="designation_level" type="text" value="' . $val['designation_level'] . '" style="width:40px"></td></tr>';
                }
            }
            echo $html;
            die;
        } catch (\Exception $ex) {
            print_r($ex->getMessage());
            die;
        }
    }

    public function loadSokolPotro($nothiMasterId, $nothi_office = 0)
    {
        $this->layout = null;
        $employee_office = $this->getCurrentDakSection();
        $otherNothi = false;
        if ($nothi_office != 0 && $nothi_office != $employee_office['office_id']) {
            $otherNothi = true;
            $this->switchOffice($nothi_office, 'MainNothiOffice');
        } else {
            $nothi_office = $employee_office['office_id'];
        }

		if (!empty($this->request->query('permitted_by'))) {
			$organogram_id = $this->request->query('permitted_by');
			$OtherOrganogramActivitiesSettingsTable = TableRegistry::get('OtherOrganogramActivitiesSettings');
			$OtherOrganogramActivitiesSettings = $OtherOrganogramActivitiesSettingsTable->find()->where(['organogram_id' => $organogram_id, 'assigned_organogram_id' => $employee_office['office_unit_organogram_id'], 'status' => 1, 'permission_for IN' => [0,2]])->count();
			if ($OtherOrganogramActivitiesSettings > 0) {
				$employeeOfficeTable = TableRegistry::get('EmployeeOffices');
				$emp_office = designationInfo($organogram_id);
				$employee_office = $employeeOfficeTable->find()->where(['status' => 1, 'office_unit_organogram_id' => $organogram_id])->first();
				$employee_office = array_merge($employee_office->toArray(), $emp_office);
			}
		}

        $nothiMasterPermissionsTable = TableRegistry::get("NothiMasterPermissions");
        $potroAttachmentTable = TableRegistry::get('NothiPotroAttachments');
        $permitted_nothi_list = $nothiMasterPermissionsTable->allNothiPermissionList($nothi_office,
            $employee_office['office_id'], $employee_office['office_unit_id'],
            $employee_office['office_unit_organogram_id']);

        $search = !empty($this->request->data['q']) ? h($this->request->data['q']) : '';

        $potroAllAttachmentRecord = $potroAttachmentTable->find()
            ->contain(['PotrojariData', 'NothiPotros'])
            ->where(['NothiPotroAttachments.nothi_master_id' => $nothiMasterId])
            ->where(['NothiPotroAttachments.nothijato' => 0, 'NothiPotroAttachments.status' => 1])
            ->where(['NothiPotroAttachments.nothi_part_no IN' => $permitted_nothi_list]);

        if (!empty($search)) {
            $potroAllAttachmentRecord->where(["OR" => [
                'NothiPotros.sarok_no' => $search,
                'NothiPotros.subject LIKE' => "%{$search}%"
            ]
            ]);
        }
        $potroAllAttachmentRecord = $potroAllAttachmentRecord->order(['NothiPotroAttachments.id desc'])->toArray();

        $this->set(compact('potroAllAttachmentRecord'));
        $this->set(compact('employee_office'));
        $this->set(compact('nothi_office'));
    }

    public function loadNothijatoPotro($nothiMasterId, $nothi_office = 0)
    {
        $this->layout = null;
        $employee_office = $this->getCurrentDakSection();
        $this->set(compact('employee_office'));
        $this->set('nothi_part_no', $nothiMasterId);
        $this->set('nothi_part_no_encrypted', $this->makeEncryptedData($nothiMasterId));
        $otherNothi = false;
        if ($nothi_office != 0 && $nothi_office != $employee_office['office_id']) {
            $otherNothi = true;
            $this->switchOffice($nothi_office, 'MainNothiOffice');
        } else {
            $nothi_office = $employee_office['office_id'];
        }
        $this->set(compact('nothi_office'));
        TableRegistry::remove('NothiPotroAttachments');
        $potroAttachmentTable = TableRegistry::get('NothiPotroAttachments');
        $potroNothijatoAttachmentRecord = $potroAttachmentTable->find()->select(['NothiPotroAttachments.id', 'NothiPotroAttachments.nothi_master_id', 'NothiPotroAttachments.nothi_part_no', 'NothiPotroAttachments.sarok_no', 'subject' => 'NothiPotros.subject', 'NothiPotroAttachments.nothi_potro_id'])
            ->join([
                "NothiPotros" => [
                    'table' => 'nothi_potros', 'type' => 'inner',
                    'conditions' => ['NothiPotros.id =NothiPotroAttachments.nothi_potro_id']
                ]
            ])
            ->where(['NothiPotroAttachments.nothi_master_id' => $nothiMasterId,
                '(NothiPotroAttachments.is_summary_nothi IS NULL OR NothiPotroAttachments.is_summary_nothi = 0)', 'NothiPotroAttachments.nothijato' => 1, 'NothiPotroAttachments.status' => 1])->order(['NothiPotroAttachments.id desc'])->toArray();

        $html = '';
        if (!empty($potroNothijatoAttachmentRecord)) {
            foreach ($potroNothijatoAttachmentRecord as $key => $value) {
                $html .= '<div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a id="' . $value['id'] . '" src="collapse_' . $value['nothi_potro_id'] . '_' . $value['id'] . '"  class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" href="#collapse_' . $value['nothi_potro_id'] . '_' . $value['id'] . '">' . $value['subject'] . '</a>
                            </h4>
                        </div>
                        <div id="collapse_' . $value['nothi_potro_id'] . '_' . $value['id'] . '" class="panel-collapse collapse">
                            <div class="panel-body " style="max-height: 842px; overflow-y:auto;">

                            </div>
                        </div>
                    </div>';
            }
        }

        echo $html;
        die;
    }

    public function loadNothijatoPotrowithPagination($nothiMasterId, $nothi_office = 0, $part_no = 0)
    {
        $this->view = 'load_nothijato_potro_view_with_pagination';
        $this->layout = null;
        $employee_office = $this->getCurrentDakSection();
        $this->set(compact('employee_office'));
        $this->set(compact('nothi_office'));
        $this->set('nothi_part_no', $part_no);
        $this->set('nothi_part_no_encrypted', makeEncryptedData($part_no));

        if ($nothi_office != 0 && $nothi_office != $employee_office['office_id']) {
            $this->switchOffice($nothi_office, 'MainNothiOffice');
        }
        TableRegistry::remove('NothiPotroAttachments');
        $NothiPotroAttachmentsData = $this->Paginator->paginate(TableRegistry::get('NothiPotroAttachments')->getPotro(['nothi_master_id' => $nothiMasterId,'nothijato'=>1], 'id desc', false), ['limit' => 1]);

        $this->set(compact('NothiPotroAttachmentsData'));
    }

    public function loadAllNothijatoPotro($part_id, $nothi_office = 0)
    {
        $this->layout = null;
        $employee_office = $this->getCurrentDakSection();
        $nothiMasterPermissionsTable = TableRegistry::get("NothiMasterPermissions");
        TableRegistry::remove("NothiParts");
        TableRegistry::remove("NothiPotros");
        $nothiPartsTable = TableRegistry::get("NothiParts");

        $nothiPartsInformation = $nothiPartsTable->getAll(['NothiParts.id' => $part_id])->first();
        $this->set('part_id', $part_id);
        $allNothiMasterList = $nothiMasterPermissionsTable->getMasterNothiPartList($nothi_office,
            $employee_office['office_id'], $employee_office['office_unit_id'],
            $employee_office['office_unit_organogram_id'],
            $nothiPartsInformation['nothi_masters_id']);

        $nothiPartId = $draftSummary = $summaryAttachmentRecord
            = array();

        if (!empty($allNothiMasterList)) {
            foreach ($allNothiMasterList as $key => $value) {
                $nothiPartId[] = $value['nothi_part_no'];
            }
        }

        TableRegistry::remove('NothiPotroAttachments');
        TableRegistry::remove('Potrojari');
        $potroAttachmentTable = TableRegistry::get('NothiPotroAttachments');

        $potroNothijatoAttachmentRecord = $potroAttachmentTable->find()->where(['nothi_part_no IN' => $nothiPartId,
            '(is_summary_nothi IS NULL OR is_summary_nothi = 0)', 'nothijato' => 1, 'status' => 1])->order(['id desc'])->toArray();

        $this->set(compact('potroNothijatoAttachmentRecord'));
    }
    /*     * */

    /* API of this Controller */

    public function APInothiMastersListBk()
    {
        $listType = $this->request->query['list_type'];
        $user_designation = $this->request->query['user_designation'];
        $jsonArray = array(
            "status" => "error",
            "error_code" => 404,
            "message" => "Invalid Request"
        );
        if (!defined("PROJAPOTI_API") || PROJAPOTI_API == false) {
            echo json_encode($jsonArray);
            die;
        }

        $apikey = isset($this->request->query['api_key']) ? $this->request->query['api_key']
            : '';
        if (empty($apikey) || $this->checkToken($apikey) == FALSE) {
            echo json_encode($jsonArray);
            die;
        }

        if (empty($user_designation)) {
            echo json_encode($jsonArray);
            die;
        }

        if (empty($listType)) {
            echo json_encode($jsonArray);
            die;
        }
        if ($apikey != API_KEY) {
            //verify api token
            $this->loadComponent('ApiRelated');
            $getApiTokenData = $this->ApiRelated->getMobileApiTokenData($apikey);
            if (!empty($getApiTokenData)) {
                if (!in_array($user_designation, $getApiTokenData['designations'])) {
                    $jsonArray['message'] = 'Unauthorized request';
                    echo json_encode($jsonArray);
                    die;
                }
            }
            //verify api token
        }


        $EmployeeOfficesTable = TableRegistry::get('EmployeeOffices');
        $employee_office = $EmployeeOfficesTable->find()->where(['office_unit_organogram_id' => $user_designation,
            'status' => 1])->first();

        $this->switchOffice($employee_office['office_id'], "apiOffice");

        $nothiMasterPermissionsTable = TableRegistry::get("NothiMasterPermissions");
        $nothiMasterCurrentUserTable = TableRegistry::get("NothiMasterCurrentUsers");

        $nothiPartsTable = TableRegistry::get("NothiParts");
        $nothiMasterMovementTable = TableRegistry::get("NothiMasterMovements");

        $start = isset($this->request->query['start']) ? intval($this->request->query['start']) : 0;
        $len = isset($this->request->query['length']) ? intval($this->request->query['length']) : 200;
        $page = !empty($this->request->query['page']) ? intval($this->request->query['page']) : (($start / $len) + 1);

        $condition = '';
        $condition2 = '';

        $subject = isset($this->request->query['subject']) ? h(trim($this->request->query['subject']))
            : '';
        if (!empty($subject)) {
            if (!empty($condition)) $condition .= ' AND ';
            $condition .= "NothiParts.subject LIKE '%{$subject}%'";
            $condition2 .= "NothiParts2.subject LIKE '%{$subject}%'";
        }

        $unit = isset($this->request->query['unit']) ? h(trim($this->request->query['unit']))
            : '';
        if (!empty($unit)) {
            if (!empty($condition)) $condition .= ' AND ';
            $condition .= "NothiParts.office_units_id LIKE '%{$unit}%'";
            $condition2 .= "NothiParts2.office_units_id LIKE '%{$unit}%'";
        }

        $type_name = isset($this->request->query['type_name']) ? h(trim($this->request->query['type_name']))
            : '';
        if (!empty($type_name)) {
            if (!empty($condition)) $condition .= ' AND ';
            $condition .= "NothiTypes.type_name LIKE '%{$type_name}%'";
        }

        $nothi_no = isset($this->request->query['nothi_no']) ? h(trim($this->request->query['nothi_no']))
            : '';
        if (!empty($nothi_no)) {
            if (!empty($condition)) $condition .= ' AND ';
            $condition .= "NothiParts.nothi_no LIKE '%{$nothi_no}%'";
            $condition2 .= "NothiParts2.nothi_no LIKE '%{$nothi_no}%'";
        }

        $from_date = isset($this->request->query['from']) ? h(trim($this->request->query['from']))
            : '';
        if (!empty($from_date)) {
            if (!empty($condition)) $condition .= ' AND ';
            $condition .= "  date(NothiParts.modified) >= '{$from_date}'";
            $condition2 .= "  date(NothiParts2.modified) >= '{$from_date}'";
        }

        $end_date = isset($this->request->query['to']) ? h(trim($this->request->query['to']))
            : '';
        if (!empty($end_date)) {
            if (!empty($condition)) $condition .= ' AND ';
            $condition .= "  date(NothiParts.modified) <= '{$end_date}'";
            $condition2 .= "  date(NothiParts2.modified) <= '{$end_date}'";
        }

        $noteTable = TableRegistry::get('NothiNotes');

        if ($listType == 'other_sent') {
            $json_data = $this->nothiMasterListForOtherOfficesSent($nothiMasterCurrentUserTable, $this->request->query, $page, $len, $employee_office, 1);
            $this->response->body(json_encode($json_data));
            $this->response->type('application/json');
            return $this->response;
        }
        if ($listType == 'other') {
            $json_data = $this->nothiMasterListForOtherOffices($nothiMasterCurrentUserTable, $employee_office, $nothiMasterPermissionsTable, $condition, [], $page, $len, $this->request->query, 1);
            $this->response->body(json_encode($json_data));
            $this->response->type('application/json');
            return $this->response;
        }

        $data = [];
        $currentNothiMasterArray = array();
        $currentNothiMasterViewStatusArray = array();
        $currentNothiMasterTotalArray = array();

        $totalRec = array();
        $nothiMasterRecord = array();

        $show_only_important = isset($this->request->query['show_only_important']) ? $this->request->query['show_only_important'] : 0;

        $nothi_master_conditions = [];
        $nothi_master_conditions['office_unit_organogram_id'] = $employee_office['office_unit_organogram_id'];
        $nothi_master_conditions['office_unit_id'] = $employee_office['office_unit_id'];
        $nothi_master_conditions['nothi_office'] = $employee_office['office_id'];
        $nothi_master_conditions['is_archive'] = 0;
        //only inbox and other_inbox search option consider joruri params
        if ($listType == 'inbox') {
            $nothi_master_conditions['is_finished'] = 0;
            $nothi_master_conditions['is_new'] = 0;
            if (!empty($show_only_important)) {
                $nothi_master_conditions['priority'] = $show_only_important;
            }
        }
        $nothiMastersCurrentUser = $nothiMasterCurrentUserTable->find()->select(['nothi_master_id',
            'nothi_office', 'nothi_part_no', 'view_status', 'issue_date', 'priority'])
            ->where($nothi_master_conditions)->order('issue_date desc')->toArray();

        //part 1
        $currentNothiMaster = '';
        $currentNothiIssueDate = array();
        if (!empty($nothiMastersCurrentUser)) {
            $minarray = array();
            $priority = [];
            foreach ($nothiMastersCurrentUser as $key => $currentUser) {
                $currentNothiMasterArray[] = $currentUser['nothi_part_no'];
                $minarray[$currentUser['nothi_master_id']][] = $currentUser['view_status'];

                if (!isset($priority[$currentUser['nothi_master_id']])) {
                    $priority[$currentUser['nothi_master_id']] = 0;
                } else if ($currentUser['priority'] == 1) {
                    $priority[$currentUser['nothi_master_id']] = $currentUser['priority'];
                }
                $currentNothiMasterViewStatusArray[$currentUser['nothi_master_id']]
                    = ($currentUser['view_status'] == 0 ? (!isset($currentNothiMasterViewStatusArray[$currentUser['nothi_master_id']])
                    ? 1 : (++$currentNothiMasterViewStatusArray[$currentUser['nothi_master_id']]))
                    : (!isset($currentNothiMasterViewStatusArray[$currentUser['nothi_master_id']])
                        ? 0 : $currentNothiMasterViewStatusArray[$currentUser['nothi_master_id']]));


                $firstsubject = $noteTable->getFirstNote($currentUser['nothi_part_no']);
                $currentNothiMasterTotalArray[$currentUser['nothi_master_id']][$currentUser['nothi_part_no']]
                    = $firstsubject['subject'];

                $prevmax = $currentUser['view_status'];
                $currentNothiMaster .= $currentUser['nothi_part_no'] . ',';
                $currentNothiIssueDate[$currentUser['nothi_master_id']] = (!isset($currentNothiIssueDate[$currentUser['nothi_master_id']])
                || $currentNothiIssueDate[$currentUser['nothi_master_id']]
                < $currentUser['issue_date'] ? $currentUser['issue_date']
                    : $currentNothiIssueDate[$currentUser['nothi_master_id']]);
            }
        }

        //part 2
        $nothiCurrentUserCondition = '';

        if ($listType == 'inbox') {

            if (!empty($currentNothiMaster)) {
                if (!empty($condition)) {
                    $condition .= " AND ";
                }
                $condition .= ' NothiParts.id IN (' . substr($currentNothiMaster,
                        0, -1) . ')';
            } else {
                $condition = ' 0 ';
            }
        } else if ($listType == 'sent') {
            $sent_date_array = [];
            $sent_priority = [];
            $sentNothi = $nothiMasterMovementTable->find()->select(["nothimasterid" => "DISTINCT(nothi_part_no)", 'sent_date' => 'created', 'real_nothi_master_id' => 'nothi_master_id', 'priority'])->where(['from_office_unit_id' => $employee_office['office_unit_id'],
                'from_officer_designation_id' => $employee_office['office_unit_organogram_id']])->order(['modified desc'])->toArray();

            if (!empty($sentNothi)) {
                $sentNothiMaster = '';
                if (!empty($condition)) $condition .= " AND ";
                if (!empty($condition2)) $condition2 .= " AND ";

                foreach ($sentNothi as $ke => $sentNothiId) {
                    $sentNothiMaster .= $sentNothiId['nothimasterid'] . ',';
                    if (!empty($sent_date_array[$sentNothiId['real_nothi_master_id']]) && $sent_date_array[$sentNothiId['real_nothi_master_id']] < $sentNothiId['sent_date']) {
                        $sent_date_array[$sentNothiId['real_nothi_master_id']] = $sentNothiId['sent_date'];
                    } else if (empty($sent_date_array[$sentNothiId['real_nothi_master_id']])) {
                        $sent_date_array[$sentNothiId['real_nothi_master_id']] = $sentNothiId['sent_date'];
                    }
                    if (!empty($sentNothiId['priority']) && empty($sent_priority[$sentNothiId['real_nothi_master_id']])) {
                        $sent_priority[$sentNothiId['real_nothi_master_id']] = $sentNothiId['priority'];
                    } elseif (empty($sent_priority[$sentNothiId['real_nothi_master_id']])) {
                        $sent_priority[$sentNothiId['real_nothi_master_id']] = 0;
                    }
                }

                $condition .= ' NothiParts.id IN (' . substr($sentNothiMaster, 0, -1) . ')';
                $condition2 .= ' NothiParts2.id IN (' . substr($sentNothiMaster, 0, -1) . ')';
                // No need to check if sent nothi current user is me.
//                    if (!empty($currentNothiMaster)) {
//                        $condition .= 'AND NothiParts.id NOT IN (' . substr($currentNothiMaster,
//                                0, -1) . ')';
//                    }
            } else {
                $condition = " 0 ";
                $condition2 = " 0 ";
            }
        } else if ($listType == 'office') {
            if (!empty($condition)) $condition .= " AND ";
            if (!empty($condition2)) $condition2 .= " AND ";

            $condition .= ' NothiParts.office_id = ' . $employee_office['office_id'];
            $condition2 .= ' NothiParts2.office_id = ' . $employee_office['office_id'];
        } else if ($listType == 'other' || $listType == 'other_sent') {

        } else {
            $nothiMastersPermitted = $nothiMasterPermissionsTable->getMasterNothiList($employee_office['office_id'],
                $employee_office['office_id'],
                $employee_office['office_unit_id'],
                $employee_office['office_unit_organogram_id'],
                $employee_office['office_id']);
            $nothiMastersId = "";
            if (!empty($nothiMastersPermitted)) {
                foreach ($nothiMastersPermitted as $nothiKey => $nothiId) {
                    $nothiMastersId .= $nothiId['nothi_masters_id'] . ',';
                }
                if (!empty($condition)) $condition .= " AND ";
                if (!empty($condition2)) $condition2 .= " AND ";
                $condition .= ' NothiParts.nothi_masters_id IN (' . substr($nothiMastersId,
                        0, -1) . ') ';
                $condition2 .= ' NothiParts2.nothi_masters_id IN (' . substr($nothiMastersId,
                        0, -1) . ') ';
            } else {
                $condition = " 0 ";
                $condition2 = " 0 ";
            }
        }

        if ($listType == 'inbox') {
            $totalRec = $nothiPartsTable->getAll($condition, $page,
                $len)->join([
                "NothiNotes" => [
                    'table' => 'nothi_notes', 'type' => 'inner',
                    'conditions' => ['NothiParts.id =NothiNotes.nothi_part_no']
                ]
            ])->group(['NothiParts.nothi_masters_id'])->order(['NothiParts.modified desc'])->count();


            $nothiMasterRecord = $nothiPartsTable->getAll($condition, $page,
                $len)->join([
                "NothiNotes" => [
                    'table' => 'nothi_notes', 'type' => 'inner',
                    'conditions' => ['NothiParts.id =NothiNotes.nothi_part_no']
                ]
            ])
                ->group(['NothiParts.nothi_masters_id'])->order(['NothiParts.modified desc'])->toArray();
        } else if ($listType == 'other') {
            $response = $this->nothiMasterListForOtherOffices($nothiMasterCurrentUserTable, $employee_office, $nothiMasterPermissionsTable, $condition, $nothiMasterRecord, $page, $len, $this->request->data);
            if (!empty($response)) {
                foreach ($response as $res_k => $res_v) {
                    $$res_k = $res_v;
                }
            }
        } else {

            $nothiMasterRecord = $nothiPartsTable->getAll($condition, $page,
                $len)->join([
                "NothiParts2" => [
                    'className' => 'NothiParts',
                    'table' => 'nothi_parts', 'type' => 'left',
                    'conditions' => ['NothiParts.nothi_masters_id =NothiParts2.nothi_masters_id AND NothiParts2.modified > NothiParts.modified AND ' . $condition2]
                ]
            ])->where(['NothiParts2.id is null'])->group(['NothiParts.nothi_masters_id'])->toArray();

            $totalRec = $nothiPartsTable->getAll($condition)->join([
                "NothiParts2" => [
                    'className' => 'NothiParts',
                    'table' => 'nothi_parts', 'type' => 'left',
                    'conditions' => ['NothiParts.nothi_masters_id =NothiParts2.nothi_masters_id AND NothiParts2.modified > NothiParts.modified AND ' . $condition2]
                ]
            ])->where(['NothiParts2.id is null'])->group(['NothiParts.nothi_masters_id'])->count();
        }

        $table_office_unit = TableRegistry::get('OfficeUnits');

        if (!empty($nothiMasterRecord)) {
            $i = 0;

            foreach ($nothiMasterRecord as $key => $record) {
                $show_only_important = isset($this->request->query['show_only_important']) ? $this->request->query['show_only_important'] : 0;
                if ($show_only_important == 1) {
//                    if ($listType == 'sent' && isset($sent_priority[$record['nothi_masters_id']]) && $sent_priority[$record['nothi_masters_id']] != 1) {
//                        unset($sent_priority[$record['nothi_masters_id']]);
//                        $totalRec--;
//                        continue;
//                    }
                    if ($listType == 'inbox' && isset($priority[$record['nothi_masters_id']]) && $priority[$record['nothi_masters_id']] != 1) {
                        unset($priority[$record['nothi_masters_id']]);
                        $totalRec--;
                        continue;
                    }
                }
                $lastNote = $nothiPartsTable->lastNote($record['nothi_masters_id']);
                // add cache so that no extra query required
                if (($unit = Cache::read('getNameWithOffice_' . $record['office_units_id'], 'memcached')) === false) {
                    $unit = $table_office_unit->getNameWithOffice($record['office_units_id']);
                    Cache::write('getNameWithOffice_' . $record['office_units_id'], $unit, 'memcached');
                }
                // add cache so that no extra query required

                $i++;


                $crTime = ($lastNote['nothi_created_date'] != '0000-00-00' || $record['nothi_created_date']
                    != '') ? New Time($lastNote['nothi_created_date']) : New Time($lastNote['modified']);

                $data[] = array(
                    Number::format($i),
                    $record['nothi_no'],
                    ((isset($currentNothiMasterTotalArray[$record['nothi_masters_id']])
                    && count($currentNothiMasterTotalArray[$record['nothi_masters_id']])
                    > 1 ? (Number::format(count($currentNothiMasterTotalArray[$record['nothi_masters_id']])))
                        : 0)),
                    $record['NothiTypes']['type_name'],
                    $record['subject'],
                    $unit['unit_name_bng'] . ', ' . $unit['office_name_bng'],
                    $record['id'] . '/' . $record['office_id'],
                    $crTime->i18nFormat("dd-MM-YYYY"),
                    ((in_array($record['id'],
                            $currentNothiMasterArray) && isset($currentNothiMasterViewStatusArray[$record['nothi_masters_id']])
                        && $currentNothiMasterViewStatusArray[$record['nothi_masters_id']]
                        > 0) ? 1 : 0)
                );
            }

            $json_data = array(
                "draw" => isset($this->request->query['draw']) ? intval($this->request->query['draw'])
                    : 1,
                "recordsTotal" => count($data),
                "recordsFiltered" => count($data),
                "data" => $data
            );
            $this->response->body(json_encode($json_data));
            $this->response->type('application/json');
            return $this->response;
        } else {
            $this->response->body(json_encode(array(
                "draw" => 1,
                "recordsTotal" => 0,
                "recordsFiltered" => 0,
                "data" => array()
            )));
            $this->response->type('application/json');
            return $this->response;
        }
    }

    public function APInothiMastersList()
    {
        $listType = $this->request->query['list_type'];
        $user_designation = $this->request->query['user_designation'];
        $jsonArray = array(
            "status" => "error",
            "error_code" => 404,
            "message" => "Invalid Request"
        );
        if (!defined("PROJAPOTI_API") || PROJAPOTI_API == false) {
            echo json_encode($jsonArray);
            die;
        }

        $apikey = isset($this->request->query['api_key']) ? $this->request->query['api_key']
            : '';
        if (empty($apikey) || $this->checkToken($apikey) == FALSE) {
            echo json_encode($jsonArray);
            die;
        }

        if (empty($user_designation)) {
            echo json_encode($jsonArray);
            die;
        }

        if (empty($listType)) {
            echo json_encode($jsonArray);
            die;
        }
        if ($apikey != API_KEY) {
            //verify api token
            $this->loadComponent('ApiRelated');
            $getApiTokenData = $this->ApiRelated->getMobileApiTokenData($apikey);
            if (!empty($getApiTokenData)) {
                if (!in_array($user_designation, $getApiTokenData['designations'])) {
                    $jsonArray['message'] = 'Unauthorized request';
                    echo json_encode($jsonArray);
                    die;
                }
            }
            //verify api token
        }


        $EmployeeOfficesTable = TableRegistry::get('EmployeeOffices');
        $employee_office = $EmployeeOfficesTable->find()->where(['office_unit_organogram_id' => $user_designation,
            'status' => 1])->first();

        $this->switchOffice($employee_office['office_id'], "apiOffice");

        $nothiMasterPermissionsTable = TableRegistry::get("NothiMasterPermissions");
        $nothiMasterCurrentUserTable = TableRegistry::get("NothiMasterCurrentUsers");

        $nothiPartsTable = TableRegistry::get("NothiParts");
        $nothiMasterMovementTable = TableRegistry::get("NothiMasterMovements");

        $start = isset($this->request->query['start']) ? intval($this->request->query['start']) : 0;
        $len = isset($this->request->query['length']) ? intval($this->request->query['length']) : 200;
        $page = !empty($this->request->query['page']) ? intval($this->request->query['page']) : (($start / $len) + 1);

        $condition = [];
        $condition2 = [];


        $subject = isset($this->request->query['subject']) ? h(trim($this->request->query['subject']))
            : '';
        if (!empty($subject)) {
            array_push($condition,['NothiParts.subject LIKE'=>"%".$subject."%"]);
            array_push($condition2,['NothiParts2.subject LIKE'=>"%".$subject."%"]);
        }

        $unit = isset($this->request->query['unit']) ? h(trim($this->request->query['unit']))
            : '';
        if (!empty($unit)) {
            array_push($condition,['NothiParts.office_units_id'=>$unit]);
            array_push($condition2,['NothiParts2.office_units_id'=>$unit]);
        }

        $type_name = isset($this->request->query['type_name']) ? h(trim($this->request->query['type_name']))
            : '';
        if (!empty($type_name)) {
            array_push($condition,['NothiTypes.type_name LIKE'=>"%".$type_name."%"]);
        }

        $nothi_no = isset($this->request->query['nothi_no']) ? h(trim($this->request->query['nothi_no']))
            : '';
        if (!empty($nothi_no)) {
            array_push($condition,['NothiParts.nothi_no LIKE'=>"%".$nothi_no."%"]);
            array_push($condition2,['NothiParts2.nothi_no LIKE'=>"%".$nothi_no."%"]);
        }

        $from_date = isset($this->request->query['from']) ? h(trim($this->request->query['from']))
            : '';
        if (!empty($from_date)) {
            array_push($condition,['date(NothiParts.modified) >='=>$from_date]);
            array_push($condition2,['date(NothiParts2.modified) >='=>$from_date]);
        }

        $end_date = isset($this->request->query['to']) ? h(trim($this->request->query['to']))
            : '';
        if (!empty($end_date)) {
            array_push($condition,['date(NothiParts.modified) <='=>$end_date]);
            array_push($condition2,['date(NothiParts2.modified) <='=>$end_date]);
        }

        $noteTable = TableRegistry::get('NothiNotes');

        if ($listType == 'other_sent') {
            $json_data = $this->nothiMasterListForOtherOfficesSent($nothiMasterCurrentUserTable, $this->request->query, $page, $len, $employee_office, 1);
            $this->response->body(json_encode($json_data));
            $this->response->type('application/json');
            return $this->response;
        }
        if ($listType == 'other') {
            $json_data = $this->nothiMasterListForOtherOffices($nothiMasterCurrentUserTable, $employee_office, $nothiMasterPermissionsTable, $condition, [], $page, $len, $this->request->query, 1);
            $this->response->body(json_encode($json_data));
            $this->response->type('application/json');
            return $this->response;
        }

        $data = [];
        $currentNothiMasterArray = array();
        $currentNothiMasterViewStatusArray = array();
        $currentNothiMasterTotalArray = array();

        $totalRec = array();
        $nothiMasterRecord = array();

        $show_only_important = isset($this->request->query['show_only_important']) ? $this->request->query['show_only_important'] : 0;

        $nothi_master_conditions = [];
        $nothi_master_conditions['office_unit_organogram_id'] = $employee_office['office_unit_organogram_id'];
        $nothi_master_conditions['office_unit_id'] = $employee_office['office_unit_id'];
        $nothi_master_conditions['nothi_office'] = $employee_office['office_id'];
        $nothi_master_conditions['is_archive'] = 0;
        //only inbox and other_inbox search option consider joruri params
        if ($listType == 'inbox') {
            $nothi_master_conditions['is_finished'] = 0;
            $nothi_master_conditions['is_new'] = 0;
            if (!empty($show_only_important)) {
                $nothi_master_conditions['priority'] = $show_only_important;
            }
        }
        $nothiMastersCurrentUser = $nothiMasterCurrentUserTable->find()->select(['nothi_master_id',
            'nothi_office', 'nothi_part_no', 'view_status', 'issue_date', 'priority'])
            ->where($nothi_master_conditions)->order('issue_date desc')->toArray();

        //part 1
        $currentNothiMaster = [];
        $currentNothiIssueDate = array();
        if (!empty($nothiMastersCurrentUser)) {
            $minarray = array();
            $priority = [];
            foreach ($nothiMastersCurrentUser as $key => $currentUser) {
                $currentNothiMasterArray[] = $currentUser['nothi_part_no'];
                $minarray[$currentUser['nothi_master_id']][] = $currentUser['view_status'];

                if (!isset($priority[$currentUser['nothi_master_id']])) {
                    $priority[$currentUser['nothi_master_id']] = 0;
                } else if ($currentUser['priority'] == 1) {
                    $priority[$currentUser['nothi_master_id']] = $currentUser['priority'];
                }
                $currentNothiMasterViewStatusArray[$currentUser['nothi_master_id']]
                    = ($currentUser['view_status'] == 0 ? (!isset($currentNothiMasterViewStatusArray[$currentUser['nothi_master_id']])
                    ? 1 : (++$currentNothiMasterViewStatusArray[$currentUser['nothi_master_id']]))
                    : (!isset($currentNothiMasterViewStatusArray[$currentUser['nothi_master_id']])
                        ? 0 : $currentNothiMasterViewStatusArray[$currentUser['nothi_master_id']]));


                $firstsubject = $noteTable->getFirstNote($currentUser['nothi_part_no']);
                $currentNothiMasterTotalArray[$currentUser['nothi_master_id']][$currentUser['nothi_part_no']]
                    = $firstsubject['subject'];

                $prevmax = $currentUser['view_status'];
                $currentNothiMaster[] = $currentUser['nothi_part_no'];
                $currentNothiIssueDate[$currentUser['nothi_master_id']] = (!isset($currentNothiIssueDate[$currentUser['nothi_master_id']])
                || $currentNothiIssueDate[$currentUser['nothi_master_id']]
                < $currentUser['issue_date'] ? $currentUser['issue_date']
                    : $currentNothiIssueDate[$currentUser['nothi_master_id']]);
            }
        }



        //part 2
        $nothiCurrentUserCondition = [];

        if ($listType == 'inbox') {

            if (!empty($currentNothiMaster)) {
                array_push($condition,['NothiParts.id IN'=>$currentNothiMaster]);
            }else{
                goto rtn;
            }
        } else if ($listType == 'sent') {
            $sent_date_array = [];
            $sent_priority = [];
            $sentNothi = $nothiMasterMovementTable->find()->select(["nothimasterid" => "DISTINCT(nothi_part_no)", 'sent_date' => 'created', 'real_nothi_master_id' => 'nothi_master_id', 'priority'])->where(['from_office_unit_id' => $employee_office['office_unit_id'],
                'from_officer_designation_id' => $employee_office['office_unit_organogram_id']])->order(['modified desc'])->toArray();

            if (!empty($sentNothi)) {
                $sentNothiMaster = [];
                $real_nothi_master_id_list = [];
                foreach ($sentNothi as $ke => $sentNothiId) {
                    $sentNothiMaster[] = $sentNothiId['nothimasterid'];
                    if(!in_array($sentNothiId['real_nothi_master_id'],$real_nothi_master_id_list)){
                        $real_nothi_master_id_list[]=$sentNothiId['real_nothi_master_id'];
                    }
                    if (!empty($sent_date_array[$sentNothiId['real_nothi_master_id']]) && $sent_date_array[$sentNothiId['real_nothi_master_id']] < $sentNothiId['sent_date']) {
                        $sent_date_array[$sentNothiId['real_nothi_master_id']] = $sentNothiId['sent_date'];
                    } else if (empty($sent_date_array[$sentNothiId['real_nothi_master_id']])) {
                        $sent_date_array[$sentNothiId['real_nothi_master_id']] = $sentNothiId['sent_date'];
                    }
                    if (!empty($sentNothiId['priority']) && empty($sent_priority[$sentNothiId['real_nothi_master_id']])) {
                        $sent_priority[$sentNothiId['real_nothi_master_id']] = $sentNothiId['priority'];
                    } elseif (empty($sent_priority[$sentNothiId['real_nothi_master_id']])) {
                        $sent_priority[$sentNothiId['real_nothi_master_id']] = 0;
                    }
                }

                array_push($condition,['NothiParts.id IN'=>$sentNothiMaster]);
                array_push($condition2,['NothiParts2.id IN'=>$sentNothiMaster]);
                // No need to check if sent nothi current user is me.
//                    if (!empty($currentNothiMaster)) {
//                        $condition .= 'AND NothiParts.id NOT IN (' . substr($currentNothiMaster,
//                                0, -1) . ')';
//                    }
            }else{
                goto rtn;
            }
        } else if ($listType == 'office') {
            array_push($condition,['NothiParts.office_id'=>$employee_office['office_id']]);
            array_push($condition2,['NothiParts2.office_id'=>$employee_office['office_id']]);
        } else if ($listType == 'other' || $listType == 'other_sent') {

        } else {
            $nothiMastersPermitted = $nothiMasterPermissionsTable->getMasterNothiList($employee_office['office_id'],
                $employee_office['office_id'],
                $employee_office['office_unit_id'],
                $employee_office['office_unit_organogram_id'],
                $employee_office['office_id']);
            $nothiMastersId = [];
            if (!empty($nothiMastersPermitted)) {
                foreach ($nothiMastersPermitted as $nothiKey => $nothiId) {
                    $nothiMastersId[] = $nothiId['nothi_masters_id'];
                }
                array_push($condition,['NothiParts.nothi_masters_id IN'=>$nothiMastersId]);
                array_push($condition2,['NothiParts2.nothi_masters_id IN'=>$nothiMastersId]);
            }
        }

        if ($listType == 'inbox') {
            $totalRec = $nothiPartsTable->getAll($condition, $page,
                $len)->join([
                "NothiNotes" => [
                    'table' => 'nothi_notes', 'type' => 'inner',
                    'conditions' => ['NothiParts.id =NothiNotes.nothi_part_no']
                ]
            ])->group(['NothiParts.nothi_masters_id'])->order(['NothiParts.modified desc'])->count();


            $nothiMasterRecord = $nothiPartsTable->getAll($condition, $page,
                $len)->join([
                "NothiNotes" => [
                    'table' => 'nothi_notes', 'type' => 'inner',
                    'conditions' => ['NothiParts.id =NothiNotes.nothi_part_no']
                ]
            ])
                ->group(['NothiParts.nothi_masters_id'])->order(['NothiParts.modified desc'])->toArray();
        } else if ($listType == 'other') {
            $response = $this->nothiMasterListForOtherOffices($nothiMasterCurrentUserTable, $employee_office, $nothiMasterPermissionsTable, $condition, $nothiMasterRecord, $page, $len, $this->request->data);
            if (!empty($response)) {
                foreach ($response as $res_k => $res_v) {
                    $$res_k = $res_v;
                }
            }
        } else {
            $need_order = 1;
            if($listType == 'sent'){
                $need_order = 0;
            }
            $nothiMasterRecordQuery = $nothiPartsTable->getAll($condition, $page,
                $len,$need_order)->join([
                "NothiParts2" => [
                    'className' => 'NothiParts',
                    'table' => 'nothi_parts', 'type' => 'left',
                    'conditions' => ['NothiParts.nothi_masters_id'=>'NothiParts2.nothi_masters_id', 'NothiParts2.modified >'=> 'NothiParts.modified'] + $condition2
                ]
            ])->where(['NothiParts2.id is null'])->group(['NothiParts.nothi_masters_id']);

            $totalRec = $nothiPartsTable->getAll($condition)->join([
                "NothiParts2" => [
                    'className' => 'NothiParts',
                    'table' => 'nothi_parts', 'type' => 'left',
                    'conditions' => ['NothiParts.nothi_masters_id' => 'NothiParts2.nothi_masters_id', 'NothiParts2.modified >' => 'NothiParts.modified'] + $condition2
                ]
            ])->where(['NothiParts2.id is null'])->group(['NothiParts.nothi_masters_id'])->count();
            // for sent list need to filter by sent time
            if($listType == 'sent' && !empty($real_nothi_master_id_list)){
                $sorted_sentNothiMaster_string = implode(',',$real_nothi_master_id_list);
                $nothiMasterRecord = $nothiMasterRecordQuery->order('IF(FIELD(NothiParts.nothi_masters_id,'.$sorted_sentNothiMaster_string.')=0,1,0),FIELD(NothiParts.nothi_masters_id,'.$sorted_sentNothiMaster_string.')')->toArray();

            }else{
                $nothiMasterRecord = $nothiMasterRecordQuery->toArray();
            }
        }

        $table_office_unit = TableRegistry::get('OfficeUnits');

        if (!empty($nothiMasterRecord)) {
            $i = 0;

            foreach ($nothiMasterRecord as $key => $record) {
                $show_only_important = isset($this->request->query['show_only_important']) ? $this->request->query['show_only_important'] : 0;
                if ($show_only_important == 1) {
//                    if ($listType == 'sent' && isset($sent_priority[$record['nothi_masters_id']]) && $sent_priority[$record['nothi_masters_id']] != 1) {
//                        unset($sent_priority[$record['nothi_masters_id']]);
//                        $totalRec--;
//                        continue;
//                    }
                    if ($listType == 'inbox' && isset($priority[$record['nothi_masters_id']]) && $priority[$record['nothi_masters_id']] != 1) {
                        unset($priority[$record['nothi_masters_id']]);
                        $totalRec--;
                        continue;
                    }
                }
                if($listType == 'sent'){
                    $lastNote = $nothiMasterMovementTable->getLastNoteByMovement($record['nothi_masters_id'],$employee_office['office_unit_organogram_id']);
                }else{
                    $lastNote = $nothiPartsTable->lastNote($record['nothi_masters_id']);
                }
                // add cache so that no extra query required
                if (($unit = Cache::read('getNameWithOffice_' . $record['office_units_id'], 'memcached')) === false) {
                    $unit = $table_office_unit->getNameWithOffice($record['office_units_id']);
                    Cache::write('getNameWithOffice_' . $record['office_units_id'], $unit, 'memcached');
                }
                // add cache so that no extra query required

                $i++;


                $crTime = ($lastNote['nothi_created_date'] != '0000-00-00' || $record['nothi_created_date']
                    != '') ? New Time($lastNote['nothi_created_date']) : New Time($lastNote['modified']);

                $data[] = array(
                    Number::format($i),
                    $record['nothi_no'],
                    ((isset($currentNothiMasterTotalArray[$record['nothi_masters_id']])
                    && count($currentNothiMasterTotalArray[$record['nothi_masters_id']])
                    > 1 ? (Number::format(count($currentNothiMasterTotalArray[$record['nothi_masters_id']])))
                        : 0)),
                    $record['NothiTypes']['type_name'],
                    $record['subject'],
                    $unit['unit_name_bng'] . ', ' . $unit['office_name_bng'],
                    $record['id'] . '/' . $record['office_id'],
                    $crTime->i18nFormat("dd-MM-YYYY"),
                    ((in_array($record['id'],
                            $currentNothiMasterArray) && isset($currentNothiMasterViewStatusArray[$record['nothi_masters_id']])
                        && $currentNothiMasterViewStatusArray[$record['nothi_masters_id']]
                        > 0) ? 1 : 0)
                );
            }

            $json_data = array(
                "status" => "success",
                "draw" => isset($this->request->query['draw']) ? intval($this->request->query['draw'])
                    : 1,
                "recordsTotal" => count($data),
                "recordsFiltered" => count($data),
                "data" => $data
            );
            $this->response->body(json_encode($json_data));
            $this->response->type('application/json');
            return $this->response;
        } else {
            rtn:
            $this->response->body(json_encode(array(
                "status" => "success",
                "draw" => 1,
                "recordsTotal" => 0,
                "recordsFiltered" => 0,
                "data" => array()
            )));
            $this->response->type('application/json');
            return $this->response;
        }
    }

    public function postAPInothiMastersListBk()
    {
        $listType = $this->request->data['list_type'];
        $user_designation = $this->request->data['user_designation'];
        $jsonArray = array(
            "status" => "error",
            "error_code" => 404,
            "message" => "Invalid Request"
        );
        if (!defined("PROJAPOTI_API") || PROJAPOTI_API == false) {
            echo json_encode($jsonArray);
            die;
        }

        $apikey = isset($this->request->data['api_key']) ? $this->request->data['api_key']
            : '';
        if (empty($apikey) || $this->checkToken($apikey) == FALSE
        ) {
            echo json_encode($jsonArray);
            die;
        }

        if (empty($user_designation)) {
            echo json_encode($jsonArray);
            die;
        }

        if (empty($listType)) {
            echo json_encode($jsonArray);
            die;
        }
        if ($apikey != API_KEY) {
            //verify api token
            $this->loadComponent('ApiRelated');
            $getApiTokenData = $this->ApiRelated->getMobileApiTokenData($apikey);
            if (!empty($getApiTokenData)) {
                if (!in_array($user_designation, $getApiTokenData['designations'])) {
                    $jsonArray['message'] = 'Unauthorized request';
                    echo json_encode($jsonArray);
                    die;
                }
            }
            //verify api token
        }

        $EmployeeOfficesTable = TableRegistry::get('EmployeeOffices');
        $employee_office = $EmployeeOfficesTable->find()->where(['office_unit_organogram_id' => $user_designation,
            'status' => 1])->first();

        $this->switchOffice($employee_office['office_id'], "apiOffice");

        $nothiMasterPermissionsTable = TableRegistry::get("NothiMasterPermissions");
        $nothiMasterCurrentUserTable = TableRegistry::get("NothiMasterCurrentUsers");

        $nothiPartsTable = TableRegistry::get("NothiParts");
        $nothiMasterMovementTable = TableRegistry::get("NothiMasterMovements");

        $start = isset($this->request->data['start']) ? intval($this->request->data['start']) : 0;
        $len = isset($this->request->data['length']) ? intval($this->request->data['length']) : 200;
        $page = !empty($this->request->data['page']) ? intval($this->request->data['page']) : (($start / $len) + 1);

        $condition = '';
        $condition2 = '';

        $subject = isset($this->request->data['subject']) ? h(trim($this->request->data['subject']))
            : '';
        if (!empty($subject)) {
            if (!empty($condition)) $condition .= ' AND ';
            $condition .= "NothiParts.subject LIKE '%{$subject}%'";
            $condition2 .= "NothiParts2.subject LIKE '%{$subject}%'";
        }

        $unit = isset($this->request->data['unit']) ? h(trim($this->request->data['unit']))
            : '';
        if (!empty($unit)) {
            if (!empty($condition)) $condition .= ' AND ';
            $condition .= "NothiParts.office_units_id LIKE '%{$unit}%'";
            $condition2 .= "NothiParts2.office_units_id LIKE '%{$unit}%'";
        }

        $type_name = isset($this->request->data['type_name']) ? h(trim($this->request->data['type_name']))
            : '';
        if (!empty($type_name)) {
            if (!empty($condition)) $condition .= ' AND ';
            $condition .= "NothiTypes.type_name LIKE '%{$type_name}%'";
        }

        $nothi_no = isset($this->request->data['nothi_no']) ? h(trim($this->request->data['nothi_no']))
            : '';
        if (!empty($nothi_no)) {
            if (!empty($condition)) $condition .= ' AND ';
            $condition .= "NothiParts.nothi_no LIKE '%{$nothi_no}%'";
            $condition2 .= "NothiParts2.nothi_no LIKE '%{$nothi_no}%'";
        }

        $from_date = isset($this->request->data['from']) ? h(trim($this->request->data['from']))
            : '';
        if (!empty($from_date)) {
            if (!empty($condition)) $condition .= ' AND ';
            $condition .= "  date(NothiParts.modified) >= '{$from_date}'";
            $condition2 .= "  date(NothiParts2.modified) >= '{$from_date}'";
        }

        $end_date = isset($this->request->data['to']) ? h(trim($this->request->data['to']))
            : '';
        if (!empty($end_date)) {
            if (!empty($condition)) $condition .= ' AND ';
            $condition .= "  date(NothiParts.modified) <= '{$end_date}'";
            $condition2 .= "  date(NothiParts2.modified) <= '{$end_date}'";
        }

        $noteTable = TableRegistry::get('NothiNotes');

        if ($listType == 'other_sent') {
            $json_data = $this->nothiMasterListForOtherOfficesSent($nothiMasterCurrentUserTable, $this->request->data, $page, $len, $employee_office, 1);
            $this->response->body(json_encode($json_data));
            $this->response->type('application/json');
            return $this->response;
        }
        if ($listType == 'other') {
            $json_data = $this->nothiMasterListForOtherOffices($nothiMasterCurrentUserTable, $employee_office, $nothiMasterPermissionsTable, $condition, [], $page, $len, $this->request->data, 1);
            $this->response->body(json_encode($json_data));
            $this->response->type('application/json');
            return $this->response;
        }

        $data = [];
        $currentNothiMasterArray = array();
        $currentNothiMasterViewStatusArray = array();
        $currentNothiMasterTotalArray = array();

        $totalRec = array();
        $nothiMasterRecord = array();

        $show_only_important = isset($this->request->data['show_only_important']) ? $this->request->data['show_only_important'] : 0;

        $nothi_master_conditions = [];
        $nothi_master_conditions['office_unit_organogram_id'] = $employee_office['office_unit_organogram_id'];
        $nothi_master_conditions['office_unit_id'] = $employee_office['office_unit_id'];
        $nothi_master_conditions['nothi_office'] = $employee_office['office_id'];
        $nothi_master_conditions['is_archive'] = 0;
        //only inbox and other_inbox search option consider joruri params
        if ($listType == 'inbox') {
            $nothi_master_conditions['is_finished'] = 0;
            $nothi_master_conditions['is_new'] = 0;
            if (!empty($show_only_important)) {
                $nothi_master_conditions['priority'] = $show_only_important;
            }
        }
        $nothiMastersCurrentUser = $nothiMasterCurrentUserTable->find()->select(['nothi_master_id',
            'nothi_office', 'nothi_part_no', 'view_status', 'issue_date', 'priority'])->where($nothi_master_conditions)->order('issue_date desc')->toArray();

        //part 1
        $currentNothiMaster = '';
        $currentNothiIssueDate = array();
        if (!empty($nothiMastersCurrentUser)) {
            $minarray = array();
            $priority = [];
            foreach ($nothiMastersCurrentUser as $key => $currentUser) {
                $currentNothiMasterArray[] = $currentUser['nothi_part_no'];
                $minarray[$currentUser['nothi_master_id']][] = $currentUser['view_status'];

                if (!isset($priority[$currentUser['nothi_master_id']])) {
                    $priority[$currentUser['nothi_master_id']] = 0;
                } else if ($currentUser['priority'] == 1) {
                    $priority[$currentUser['nothi_master_id']] = $currentUser['priority'];
                }
                $currentNothiMasterViewStatusArray[$currentUser['nothi_master_id']]
                    = ($currentUser['view_status'] == 0 ? (!isset($currentNothiMasterViewStatusArray[$currentUser['nothi_master_id']])
                    ? 1 : (++$currentNothiMasterViewStatusArray[$currentUser['nothi_master_id']]))
                    : (!isset($currentNothiMasterViewStatusArray[$currentUser['nothi_master_id']])
                        ? 0 : $currentNothiMasterViewStatusArray[$currentUser['nothi_master_id']]));


                $firstsubject = $noteTable->getFirstNote($currentUser['nothi_part_no']);
                $currentNothiMasterTotalArray[$currentUser['nothi_master_id']][$currentUser['nothi_part_no']]
                    = $firstsubject['subject'];

                $prevmax = $currentUser['view_status'];
                $currentNothiMaster .= $currentUser['nothi_part_no'] . ',';
                $currentNothiIssueDate[$currentUser['nothi_master_id']] = (!isset($currentNothiIssueDate[$currentUser['nothi_master_id']])
                || $currentNothiIssueDate[$currentUser['nothi_master_id']]
                < $currentUser['issue_date'] ? $currentUser['issue_date']
                    : $currentNothiIssueDate[$currentUser['nothi_master_id']]);
            }
        }

        //part 2
        $nothiCurrentUserCondition = '';

        if ($listType == 'inbox') {

            if (!empty($currentNothiMaster)) {
                if (!empty($condition)) {
                    $condition .= " AND ";
                }
                $condition .= ' NothiParts.id IN (' . substr($currentNothiMaster,
                        0, -1) . ')';
            } else {
                $condition = ' 0 ';
            }
        } else if ($listType == 'sent') {
            $sent_date_array = [];
            $sent_priority = [];
            $sentNothi = $nothiMasterMovementTable->find()->select(["nothimasterid" => "DISTINCT(nothi_part_no)", 'sent_date' => 'created', 'real_nothi_master_id' => 'nothi_master_id', 'priority'])->where(['from_office_unit_id' => $employee_office['office_unit_id'],
                'from_officer_designation_id' => $employee_office['office_unit_organogram_id']])->order(['modified desc'])->toArray();

            if (!empty($sentNothi)) {
                $sentNothiMaster = '';
                if (!empty($condition)) $condition .= " AND ";
                if (!empty($condition2)) $condition2 .= " AND ";

                foreach ($sentNothi as $ke => $sentNothiId) {
                    $sentNothiMaster .= $sentNothiId['nothimasterid'] . ',';
                    if (!empty($sent_date_array[$sentNothiId['real_nothi_master_id']]) && $sent_date_array[$sentNothiId['real_nothi_master_id']] < $sentNothiId['sent_date']) {
                        $sent_date_array[$sentNothiId['real_nothi_master_id']] = $sentNothiId['sent_date'];
                    } else if (empty($sent_date_array[$sentNothiId['real_nothi_master_id']])) {
                        $sent_date_array[$sentNothiId['real_nothi_master_id']] = $sentNothiId['sent_date'];
                    }
                    if (!empty($sentNothiId['priority']) && empty($sent_priority[$sentNothiId['real_nothi_master_id']])) {
                        $sent_priority[$sentNothiId['real_nothi_master_id']] = $sentNothiId['priority'];
                    } elseif (empty($sent_priority[$sentNothiId['real_nothi_master_id']])) {
                        $sent_priority[$sentNothiId['real_nothi_master_id']] = 0;
                    }
                }

                $condition .= ' NothiParts.id IN (' . substr($sentNothiMaster, 0, -1) . ')';
                $condition2 .= ' NothiParts2.id IN (' . substr($sentNothiMaster, 0, -1) . ')';
                // No need to check if sent nothi current user is me.
//                    if (!empty($currentNothiMaster)) {
//                        $condition .= 'AND NothiParts.id NOT IN (' . substr($currentNothiMaster,
//                                0, -1) . ')';
//                    }
            } else {
                $condition = " 0 ";
                $condition2 = " 0 ";
            }
        } else if ($listType == 'office') {
            if (!empty($condition)) $condition .= " AND ";
            if (!empty($condition2)) $condition2 .= " AND ";

            $condition .= ' NothiParts.office_id = ' . $employee_office['office_id'];
            $condition2 .= ' NothiParts2.office_id = ' . $employee_office['office_id'];
        } else if ($listType == 'other' || $listType == 'other_sent') {

        } else {
            $nothiMastersPermitted = $nothiMasterPermissionsTable->getMasterNothiList($employee_office['office_id'],
                $employee_office['office_id'],
                $employee_office['office_unit_id'],
                $employee_office['office_unit_organogram_id'],
                $employee_office['office_id']);
            $nothiMastersId = "";
            if (!empty($nothiMastersPermitted)) {
                foreach ($nothiMastersPermitted as $nothiKey => $nothiId) {
                    $nothiMastersId .= $nothiId['nothi_masters_id'] . ',';
                }
                if (!empty($condition)) $condition .= " AND ";
                if (!empty($condition2)) $condition2 .= " AND ";
                $condition .= ' NothiParts.nothi_masters_id IN (' . substr($nothiMastersId,
                        0, -1) . ') ';
                $condition2 .= ' NothiParts2.nothi_masters_id IN (' . substr($nothiMastersId,
                        0, -1) . ') ';
            } else {
                $condition = " 0 ";
                $condition2 = " 0 ";
            }
        }

        if ($listType == 'inbox') {
            $totalRec = $nothiPartsTable->getAll($condition, $page,
                $len)->join([
                "NothiNotes" => [
                    'table' => 'nothi_notes', 'type' => 'inner',
                    'conditions' => ['NothiParts.id =NothiNotes.nothi_part_no']
                ]
            ])->group(['NothiParts.nothi_masters_id'])->order(['NothiParts.modified desc'])->count();


            $nothiMasterRecord = $nothiPartsTable->getAll($condition, $page,
                $len)->join([
                "NothiNotes" => [
                    'table' => 'nothi_notes', 'type' => 'inner',
                    'conditions' => ['NothiParts.id =NothiNotes.nothi_part_no']
                ]
            ])
                ->group(['NothiParts.nothi_masters_id'])->order(['NothiParts.modified desc'])->toArray();
        } else if ($listType == 'other') {
            $response = $this->nothiMasterListForOtherOffices($nothiMasterCurrentUserTable, $employee_office, $nothiMasterPermissionsTable, $condition, $nothiMasterRecord, $page, $len, $this->request->data);
            if (!empty($response)) {
                foreach ($response as $res_k => $res_v) {
                    $$res_k = $res_v;
                }
            }
        } else {

            $nothiMasterRecord = $nothiPartsTable->getAll($condition, $page,
                $len)->join([
                "NothiParts2" => [
                    'className' => 'NothiParts',
                    'table' => 'nothi_parts', 'type' => 'left',
                    'conditions' => ['NothiParts.nothi_masters_id =NothiParts2.nothi_masters_id AND NothiParts2.modified > NothiParts.modified AND ' . $condition2]
                ]
            ])->where(['NothiParts2.id is null'])->group(['NothiParts.nothi_masters_id'])->toArray();

            $totalRec = $nothiPartsTable->getAll($condition)->join([
                "NothiParts2" => [
                    'className' => 'NothiParts',
                    'table' => 'nothi_parts', 'type' => 'left',
                    'conditions' => ['NothiParts.nothi_masters_id =NothiParts2.nothi_masters_id AND NothiParts2.modified > NothiParts.modified AND ' . $condition2]
                ]
            ])->where(['NothiParts2.id is null'])->group(['NothiParts.nothi_masters_id'])->count();
        }

        $table_office_unit = TableRegistry::get('OfficeUnits');

        if (!empty($nothiMasterRecord)) {
            $i = 0;

            foreach ($nothiMasterRecord as $key => $record) {
                $show_only_important = isset($this->request->data['show_only_important']) ? $this->request->data['show_only_important'] : 0;
                if ($show_only_important == 1) {
//                    if ($listType == 'sent' && isset($sent_priority[$record['nothi_masters_id']]) && $sent_priority[$record['nothi_masters_id']] != 1) {
//                        unset($sent_priority[$record['nothi_masters_id']]);
//                        $totalRec--;
//                        continue;
//                    }
                    if ($listType == 'inbox' && isset($priority[$record['nothi_masters_id']]) && $priority[$record['nothi_masters_id']] != 1) {
                        unset($priority[$record['nothi_masters_id']]);
                        $totalRec--;
                        continue;
                    }
                }
                $lastNote = $nothiPartsTable->lastNote($record['nothi_masters_id']);
                // add cache so that no extra query required
                if (($unit = Cache::read('getNameWithOffice_' . $record['office_units_id'], 'memcached')) === false) {
                    $unit = $table_office_unit->getNameWithOffice($record['office_units_id']);
                    Cache::write('getNameWithOffice_' . $record['office_units_id'], $unit, 'memcached');
                }
                // add cache so that no extra query required

                $i++;


                $crTime = ($lastNote['nothi_created_date'] != '0000-00-00' || $record['nothi_created_date']
                    != '') ? New Time($lastNote['nothi_created_date']) : New Time($lastNote['modified']);

                $data[] = array(
                    Number::format($i),
                    $record['nothi_no'],
                    ((isset($currentNothiMasterTotalArray[$record['nothi_masters_id']])
                    && count($currentNothiMasterTotalArray[$record['nothi_masters_id']])
                    > 1 ? (Number::format(count($currentNothiMasterTotalArray[$record['nothi_masters_id']])))
                        : 0)),
                    $record['NothiTypes']['type_name'],
                    $record['subject'],
                    $unit['unit_name_bng'] . ', ' . $unit['office_name_bng'],
                    $record['id'] . '/' . $record['office_id'],
                    $crTime->i18nFormat("dd-MM-YYYY"),
                    ((in_array($record['id'],
                            $currentNothiMasterArray) && isset($currentNothiMasterViewStatusArray[$record['nothi_masters_id']])
                        && $currentNothiMasterViewStatusArray[$record['nothi_masters_id']]
                        > 0) ? 1 : 0)
                );
            }

            $json_data = array(
                "draw" => isset($this->request->data['draw']) ? intval($this->request->data['draw'])
                    : 1,
                "recordsTotal" => count($data),
                "recordsFiltered" => count($data),
                "data" => $data
            );
            $this->response->body(json_encode($json_data));
            $this->response->type('application/json');
            return $this->response;
        } else {
            $this->response->body(json_encode(array(
                "draw" => 1,
                "recordsTotal" => 0,
                "recordsFiltered" => 0,
                "data" => array()
            )));
            $this->response->type('application/json');
            return $this->response;
        }
    }

    public function postAPInothiMastersList()
    {
        $listType = $this->request->data['list_type'];
        $user_designation = $this->request->data['user_designation'];
        $jsonArray = array(
            "status" => "error",
            "error_code" => 404,
            "message" => "Invalid Request"
        );
        if (!defined("PROJAPOTI_API") || PROJAPOTI_API == false) {
            echo json_encode($jsonArray);
            die;
        }

        $apikey = isset($this->request->data['api_key']) ? $this->request->data['api_key']
            : '';
        if (empty($apikey) || $this->checkToken($apikey) == FALSE
        ) {
            echo json_encode($jsonArray);
            die;
        }

        if (empty($user_designation)) {
            echo json_encode($jsonArray);
            die;
        }

        if (empty($listType)) {
            echo json_encode($jsonArray);
            die;
        }
        if ($apikey != API_KEY) {
            //verify api token
            $this->loadComponent('ApiRelated');
            $getApiTokenData = $this->ApiRelated->getMobileApiTokenData($apikey);
            if (!empty($getApiTokenData)) {
                if (!in_array($user_designation, $getApiTokenData['designations'])) {
                    $jsonArray['message'] = 'Unauthorized request';
                    echo json_encode($jsonArray);
                    die;
                }
            }
            //verify api token
        }

        $EmployeeOfficesTable = TableRegistry::get('EmployeeOffices');
        $employee_office = $EmployeeOfficesTable->find()->where(['office_unit_organogram_id' => $user_designation,
            'status' => 1])->first();

        $this->switchOffice($employee_office['office_id'], "apiOffice");

        $nothiMasterPermissionsTable = TableRegistry::get("NothiMasterPermissions");
        $nothiMasterCurrentUserTable = TableRegistry::get("NothiMasterCurrentUsers");

        $nothiPartsTable = TableRegistry::get("NothiParts");
        $nothiMasterMovementTable = TableRegistry::get("NothiMasterMovements");

        $start = isset($this->request->data['start']) ? intval($this->request->data['start']) : 0;
        $len = isset($this->request->data['length']) ? intval($this->request->data['length']) : 200;
        $page = !empty($this->request->data['page']) ? intval($this->request->data['page']) : (($start / $len) + 1);

        $condition = [];
        $condition2 = [];

        $subject = isset($this->request->data['subject']) ? h(trim($this->request->data['subject']))
            : '';
        if (!empty($subject)) {
            array_push($condition,['NothiParts.subject LIKE'=>"%".$subject."%"]);
            array_push($condition2,['NothiParts2.subject LIKE'=>"%".$subject."%"]);
        }

        $unit = isset($this->request->data['unit']) ? h(trim($this->request->data['unit']))
            : '';
        if (!empty($unit)) {
            array_push($condition,['NothiParts.office_units_id'=>$unit]);
            array_push($condition2,['NothiParts2.office_units_id'=>$unit]);
        }

        $type_name = isset($this->request->data['type_name']) ? h(trim($this->request->data['type_name']))
            : '';
        if (!empty($type_name)) {
            array_push($condition,['NothiTypes.type_name LIKE'=>$type_name]);
        }

        $nothi_no = isset($this->request->data['nothi_no']) ? h(trim($this->request->data['nothi_no']))
            : '';
        if (!empty($nothi_no)) {
            array_push($condition,['NothiParts.nothi_no LIKE'=>"%".$nothi_no."%"]);
            array_push($condition2,['NothiParts2.nothi_no LIKE'=>"%".$nothi_no."%"]);
        }

        $from_date = isset($this->request->data['from']) ? h(trim($this->request->data['from']))
            : '';
        if (!empty($from_date)) {
            array_push($condition,['date(NothiParts.modified) >='=>$from_date]);
            array_push($condition2,['date(NothiParts2.modified) >='=>$from_date]);
        }

        $end_date = isset($this->request->data['to']) ? h(trim($this->request->data['to']))
            : '';
        if (!empty($end_date)) {
            array_push($condition,['date(NothiParts.modified) <='=>$end_date]);
            array_push($condition2,['date(NothiParts2.modified) <='=>$end_date]);
        }

        $noteTable = TableRegistry::get('NothiNotes');

        if ($listType == 'other_sent') {
            $json_data = $this->nothiMasterListForOtherOfficesSent($nothiMasterCurrentUserTable, $this->request->data, $page, $len, $employee_office, 1);
            $this->response->body(json_encode($json_data));
            $this->response->type('application/json');
            return $this->response;
        }
        if ($listType == 'other') {
            $json_data = $this->nothiMasterListForOtherOffices($nothiMasterCurrentUserTable, $employee_office, $nothiMasterPermissionsTable, $condition, [], $page, $len, $this->request->data, 1);
            $this->response->body(json_encode($json_data));
            $this->response->type('application/json');
            return $this->response;
        }

        $data = [];
        $currentNothiMasterArray = array();
        $currentNothiMasterViewStatusArray = array();
        $currentNothiMasterTotalArray = array();

        $totalRec = array();
        $nothiMasterRecord = array();

        $show_only_important = isset($this->request->data['show_only_important']) ? $this->request->data['show_only_important'] : 0;

        $nothi_master_conditions = [];
        $nothi_master_conditions['office_unit_organogram_id'] = $employee_office['office_unit_organogram_id'];
        $nothi_master_conditions['office_unit_id'] = $employee_office['office_unit_id'];
        $nothi_master_conditions['nothi_office'] = $employee_office['office_id'];
        $nothi_master_conditions['is_archive'] = 0;
        //only inbox and other_inbox search option consider joruri params
        if ($listType == 'inbox') {
            $nothi_master_conditions['is_finished'] = 0;
            $nothi_master_conditions['is_new'] = 0;
            if (!empty($show_only_important)) {
                $nothi_master_conditions['priority'] = $show_only_important;
            }
        }
        $nothiMastersCurrentUser = $nothiMasterCurrentUserTable->find()->select(['nothi_master_id',
            'nothi_office', 'nothi_part_no', 'view_status', 'issue_date', 'priority'])->where($nothi_master_conditions)->order('issue_date desc')->toArray();

        //part 1
        $currentNothiMaster = [];
        $currentNothiIssueDate = array();
        if (!empty($nothiMastersCurrentUser)) {
            $minarray = array();
            $priority = [];
            foreach ($nothiMastersCurrentUser as $key => $currentUser) {
                $currentNothiMasterArray[] = $currentUser['nothi_part_no'];
                $minarray[$currentUser['nothi_master_id']][] = $currentUser['view_status'];

                if (!isset($priority[$currentUser['nothi_master_id']])) {
                    $priority[$currentUser['nothi_master_id']] = 0;
                } else if ($currentUser['priority'] == 1) {
                    $priority[$currentUser['nothi_master_id']] = $currentUser['priority'];
                }
                $currentNothiMasterViewStatusArray[$currentUser['nothi_master_id']]
                    = ($currentUser['view_status'] == 0 ? (!isset($currentNothiMasterViewStatusArray[$currentUser['nothi_master_id']])
                    ? 1 : (++$currentNothiMasterViewStatusArray[$currentUser['nothi_master_id']]))
                    : (!isset($currentNothiMasterViewStatusArray[$currentUser['nothi_master_id']])
                        ? 0 : $currentNothiMasterViewStatusArray[$currentUser['nothi_master_id']]));


                $firstsubject = $noteTable->getFirstNote($currentUser['nothi_part_no']);
                $currentNothiMasterTotalArray[$currentUser['nothi_master_id']][$currentUser['nothi_part_no']]
                    = $firstsubject['subject'];

                $prevmax = $currentUser['view_status'];
                $currentNothiMaster[] = $currentUser['nothi_part_no'];
                $currentNothiIssueDate[$currentUser['nothi_master_id']] = (!isset($currentNothiIssueDate[$currentUser['nothi_master_id']])
                || $currentNothiIssueDate[$currentUser['nothi_master_id']]
                < $currentUser['issue_date'] ? $currentUser['issue_date']
                    : $currentNothiIssueDate[$currentUser['nothi_master_id']]);
            }
        }


        //part 2
        $nothiCurrentUserCondition = [];

        if ($listType == 'inbox') {

            if (!empty($currentNothiMaster)) {
                array_push($condition,['NothiParts.id IN'=>$currentNothiMaster]);
            }
            else{
                goto rtn;
            }
        }
        else if ($listType == 'sent') {
            $sent_date_array = [];
            $sent_priority = [];
            $sentNothi = $nothiMasterMovementTable->find()->select(["nothimasterid" => "DISTINCT(nothi_part_no)", 'sent_date' => 'created', 'real_nothi_master_id' => 'nothi_master_id', 'priority'])->where(['from_office_unit_id' => $employee_office['office_unit_id'],
                'from_officer_designation_id' => $employee_office['office_unit_organogram_id']])->order(['modified desc'])->toArray();

            if (!empty($sentNothi)) {
                $sentNothiMaster = [];
                $real_nothi_master_id_list = [];
                foreach ($sentNothi as $ke => $sentNothiId) {
                    $sentNothiMaster[] = $sentNothiId['nothimasterid'];
                    if(!in_array($sentNothiId['real_nothi_master_id'],$real_nothi_master_id_list)){
                        $real_nothi_master_id_list[]=$sentNothiId['real_nothi_master_id'];
                    }
                    if (!empty($sent_date_array[$sentNothiId['real_nothi_master_id']]) && $sent_date_array[$sentNothiId['real_nothi_master_id']] < $sentNothiId['sent_date']) {
                        $sent_date_array[$sentNothiId['real_nothi_master_id']] = $sentNothiId['sent_date'];
                    } else if (empty($sent_date_array[$sentNothiId['real_nothi_master_id']])) {
                        $sent_date_array[$sentNothiId['real_nothi_master_id']] = $sentNothiId['sent_date'];
                    }
                    if (!empty($sentNothiId['priority']) && empty($sent_priority[$sentNothiId['real_nothi_master_id']])) {
                        $sent_priority[$sentNothiId['real_nothi_master_id']] = $sentNothiId['priority'];
                    } elseif (empty($sent_priority[$sentNothiId['real_nothi_master_id']])) {
                        $sent_priority[$sentNothiId['real_nothi_master_id']] = 0;
                    }
                }

                array_push($condition,['NothiParts.id IN'=>$sentNothiMaster]);
                array_push($condition2,['NothiParts2.id IN'=>$sentNothiMaster]);
                // No need to check if sent nothi current user is me.
//                    if (!empty($currentNothiMaster)) {
//                        $condition .= 'AND NothiParts.id NOT IN (' . substr($currentNothiMaster,
//                                0, -1) . ')';
//                    }
            }
            else{
                goto rtn;
            }
        }
        else if ($listType == 'office') {
            array_push($condition,['NothiParts.office_id'=>$employee_office['office_id']]);
            array_push($condition2,['NothiParts2.office_id'=>$employee_office['office_id']]);
        }
        else if ($listType == 'other' || $listType == 'other_sent') {

        }
        else {
            $nothiMastersPermitted = $nothiMasterPermissionsTable->getMasterNothiList($employee_office['office_id'],
                $employee_office['office_id'],
                $employee_office['office_unit_id'],
                $employee_office['office_unit_organogram_id'],
                $employee_office['office_id']);
            $nothiMastersId = [];
            if (!empty($nothiMastersPermitted)) {
                foreach ($nothiMastersPermitted as $nothiKey => $nothiId) {
                    $nothiMastersId[] = $nothiId['nothi_masters_id'];
                }

                array_push($condition,['NothiParts.nothi_masters_id IN'=>$nothiMastersId]);
                array_push($condition2,['NothiParts2.nothi_masters_id IN'=>$nothiMastersId]);
            }
        }

        if ($listType == 'inbox') {
            $totalRec = $nothiPartsTable->getAll($condition, $page,
                $len)->join([
                "NothiNotes" => [
                    'table' => 'nothi_notes', 'type' => 'inner',
                    'conditions' => ['NothiParts.id =NothiNotes.nothi_part_no']
                ]
            ])->group(['NothiParts.nothi_masters_id'])->order(['NothiParts.modified desc'])->count();


            $nothiMasterRecord = $nothiPartsTable->getAll($condition, $page,
                $len)->join([
                "NothiNotes" => [
                    'table' => 'nothi_notes', 'type' => 'inner',
                    'conditions' => ['NothiParts.id =NothiNotes.nothi_part_no']
                ]
            ])
                ->group(['NothiParts.nothi_masters_id'])->order(['NothiParts.modified desc'])->toArray();
        } else if ($listType == 'other') {
            $response = $this->nothiMasterListForOtherOffices($nothiMasterCurrentUserTable, $employee_office, $nothiMasterPermissionsTable, $condition, $nothiMasterRecord, $page, $len, $this->request->data);
            if (!empty($response)) {
                foreach ($response as $res_k => $res_v) {
                    $$res_k = $res_v;
                }
            }
        } else {
            $need_order = 1;
            if($listType == 'sent'){
                $need_order = 0;
            }
            $nothiMasterRecordQuery = $nothiPartsTable->getAll($condition, $page,
                $len,$need_order)->join([
                "NothiParts2" => [
                    'className' => 'NothiParts',
                    'table' => 'nothi_parts', 'type' => 'left',
                    'conditions' => ['NothiParts.nothi_masters_id' => 'NothiParts2.nothi_masters_id', 'NothiParts2.modified >'=> 'NothiParts.modified'] + $condition2
                ]
            ])->where(['NothiParts2.id is null'])->group(['NothiParts.nothi_masters_id']);

            $totalRec = $nothiPartsTable->getAll($condition)->join([
                "NothiParts2" => [
                    'className' => 'NothiParts',
                    'table' => 'nothi_parts', 'type' => 'left',
                    'conditions' => ['NothiParts.nothi_masters_id' => 'NothiParts2.nothi_masters_id', 'NothiParts2.modified >'=> 'NothiParts.modified'] + $condition2
                ]
            ])->where(['NothiParts2.id is null'])->group(['NothiParts.nothi_masters_id'])->count();
            // for sent list need to filter by sent time
            if($listType == 'sent' && !empty($real_nothi_master_id_list)){
                $sorted_sentNothiMaster_string = implode(',',$real_nothi_master_id_list);
                $nothiMasterRecord = $nothiMasterRecordQuery->order('IF(FIELD(NothiParts.nothi_masters_id,'.$sorted_sentNothiMaster_string.')=0,1,0),FIELD(NothiParts.nothi_masters_id,'.$sorted_sentNothiMaster_string.')')->toArray();

            }else{
                $nothiMasterRecord = $nothiMasterRecordQuery->toArray();
            }
        }

        $table_office_unit = TableRegistry::get('OfficeUnits');

        if (!empty($nothiMasterRecord)) {
            $i = 0;

            foreach ($nothiMasterRecord as $key => $record) {
                $show_only_important = isset($this->request->data['show_only_important']) ? $this->request->data['show_only_important'] : 0;
                if ($show_only_important == 1) {
//                    if ($listType == 'sent' && isset($sent_priority[$record['nothi_masters_id']]) && $sent_priority[$record['nothi_masters_id']] != 1) {
//                        unset($sent_priority[$record['nothi_masters_id']]);
//                        $totalRec--;
//                        continue;
//                    }
                    if ($listType == 'inbox' && isset($priority[$record['nothi_masters_id']]) && $priority[$record['nothi_masters_id']] != 1) {
                        unset($priority[$record['nothi_masters_id']]);
                        $totalRec--;
                        continue;
                    }
                }
                if($listType == 'sent'){
                    $lastNote = $nothiMasterMovementTable->getLastNoteByMovement($record['nothi_masters_id'],$employee_office['office_unit_organogram_id']);
                }else{
                    $lastNote = $nothiPartsTable->lastNote($record['nothi_masters_id']);
                }
                // add cache so that no extra query required
                if (($unit = Cache::read('getNameWithOffice_' . $record['office_units_id'], 'memcached')) === false) {
                    $unit = $table_office_unit->getNameWithOffice($record['office_units_id']);
                    Cache::write('getNameWithOffice_' . $record['office_units_id'], $unit, 'memcached');
                }
                // add cache so that no extra query required

                $i++;


                $crTime = ($lastNote['nothi_created_date'] != '0000-00-00' || $record['nothi_created_date']
                    != '') ? New Time($lastNote['nothi_created_date']) : New Time($lastNote['modified']);

                $data[] = array(
                    Number::format($i),
                    $record['nothi_no'],
                    ((isset($currentNothiMasterTotalArray[$record['nothi_masters_id']])
                    && count($currentNothiMasterTotalArray[$record['nothi_masters_id']])
                    > 1 ? (Number::format(count($currentNothiMasterTotalArray[$record['nothi_masters_id']])))
                        : 0)),
                    $record['NothiTypes']['type_name'],
                    $record['subject'],
                    $unit['unit_name_bng'] . ', ' . $unit['office_name_bng'],
                    $record['id'] . '/' . $record['office_id'],
                    $crTime->i18nFormat("dd-MM-YYYY"),
                    ((in_array($record['id'],
                            $currentNothiMasterArray) && isset($currentNothiMasterViewStatusArray[$record['nothi_masters_id']])
                        && $currentNothiMasterViewStatusArray[$record['nothi_masters_id']]
                        > 0) ? 1 : 0)
                );
            }

            $json_data = array(
                "status" => "success",
                "draw" => isset($this->request->data['draw']) ? intval($this->request->data['draw'])
                    : 1,
                "recordsTotal" => count($data),
                "recordsFiltered" => count($data),
                "data" => $data
            );
            $this->response->body(json_encode($json_data));
            $this->response->type('application/json');
            return $this->response;
        } else {
            rtn:
            $this->response->body(json_encode(array(
                "status" => "success",
                "draw" => 1,
                "recordsTotal" => 0,
                "recordsFiltered" => 0,
                "data" => array()
            )));
            $this->response->type('application/json');
            return $this->response;
        }
    }

    public function APInothiDetails($id, $nothi_office_id)
    {

        $user_designation = $this->request->query['user_designation'];
        $jsonArray = array(
            "status" => "error",
            "error_code" => 404,
            "message" => "Invalid Request"
        );
        if (!defined("PROJAPOTI_API") || PROJAPOTI_API == false) {
            echo json_encode($jsonArray);
            die;
        }
        $apikey = isset($this->request->query['api_key']) ? $this->request->query['api_key']
            : '';
        if (empty($apikey) || $this->checkToken($apikey) == FALSE) {
            echo json_encode($jsonArray);
            die;
        }

        if (empty($id)) {
            echo json_encode($jsonArray);
            die;
        }

        $this->switchOffice($nothi_office_id, 'MainNothiOffice');

        $result = array();

        $EmployeeOfficesTable = TableRegistry::get('EmployeeOffices');
        $employee_office = $EmployeeOfficesTable->find()->where(['office_unit_organogram_id' => $user_designation,
            'status' => 1])->first();

        $this->switchOffice($employee_office['office_id'], "apiOffice");

        $nothiMasterPermissionsTable = TableRegistry::get("NothiMasterPermissions");
        $nothiMasterCurrentUsersTable = TableRegistry::get("NothiMasterCurrentUsers");
        $nothiMastersTable = TableRegistry::get('NothiParts');
        $nothiPartsInformation = $nothiMastersTable->get($id);
        $nothiMasters = array();
        $nothiMastersCurrentUser = array();

        $canNote = 0;

        if (!empty($employee_office)) {
            try {
                $nothiMasters = $nothiMasterPermissionsTable->hasAccess($nothi_office_id,
                    $employee_office['office_id'],
                    $employee_office['office_unit_id'],
                    $employee_office['office_unit_organogram_id'],
                    $nothiPartsInformation['nothi_masters_id'], $id);

                $nothiMastersCurrentUser = $nothiMasterCurrentUsersTable->find()->where(['nothi_part_no' => $id,
                    'nothi_office' => $nothi_office_id])->first();
            } catch (\Exception $ex) {
                $jsonArray = array(
                    "status" => "error",
                    "error_code" => 404,
                    "message" => $ex->getMessage() . "দুঃখিত! কোনো তথ্য পাওয়া যায়নি"
                );
                echo json_encode($jsonArray);
                die;
            }
        }

        if (!empty($nothiMastersCurrentUser)) {

            if ($nothiMastersCurrentUser['office_unit_organogram_id'] == $employee_office['office_unit_organogram_id']) {
                $canNote = 1;
            } else {
                $canNote = 0;
            }
        }

        if (empty($nothiMasters)) {
            $jsonArray = array(
                "status" => "error",
                "error_code" => 404,
                "message" => "দুঃখিত! কোনো তথ্য পাওয়া যায়নি"
            );

            echo json_encode($jsonArray);
            die;
        }

        $nothiMastersCurrentUser = $nothiMasterCurrentUsersTable->find()->where(['nothi_master_id' => $id,
            'office_id' => $employee_office['office_id'], 'office_unit_id' => $employee_office['office_unit_id'],
            'office_unit_organogram_id' => $employee_office['office_unit_organogram_id'],
            'nothi_office' => $nothi_office_id])->first();

        if (!empty($nothiMastersCurrentUser) && $nothiMastersCurrentUser['view_status']
            == 0
        ) {
            $nothiMastersCurrentUser->view_status = 1;
            $nothiMasterCurrentUsersTable->save($nothiMastersCurrentUser);
        }

        try {
            $nothiRecord = $nothiMastersTable->getAll(['NothiParts.id' => $id])->first();
            $officeUnitTable = TableRegistry::get('OfficeUnits');
            $officeunit = $officeUnitTable->get($nothiRecord['office_units_id']);
            $nothiRecord['OfficeUnits']['unit_name_bng'] = $officeunit['unit_name_bng'];

            $nothiSheet = TableRegistry::get('NothiNoteSheets');

            $paginate = array('limit' => 1, 'model' => 'NothiNoteSheets');

            $query = $nothiSheet->find('all')->where(['nothi_part_no' => $id]);

            if ($query->count() != 0 && (empty($this->request->query['page']) || !isset($this->request->query['page'])
                    || (isset($this->request->query['page']) && intval($this->request->query['page'])
                        > $query->count()))
            ) {
                $page = $query->count();
                $paginate = $paginate + array('page' => $page);
            }

            $this->paginate = $paginate;
            $query = $this->paginate($query);

            $nothi_note_sheets_id = 1;
            if (!empty($query)) {
                foreach ($query as $row) {
                    $nothi_note_sheets_id = $row->sheet_no;
                }
            }

            $notes = TableRegistry::get('NothiNotes');

            $notesquery = $notes->find()
                ->where(['NothiNotes.nothi_part_no' => $id, 'NothiNotes.nothi_notesheet_id' => $nothi_note_sheets_id,
                    '(NothiNotes.office_organogram_id = ' . $employee_office['office_unit_organogram_id'] . ' OR NothiNotes.note_status <> "DRAFT")'])->join([
                    'NothiParts' => [
                        'table' => 'nothi_parts',
                        "conditions" => "NothiParts.id= NothiNotes.nothi_part_no",
                        "type" => "INNER"
                    ]
                ]);


            $noteAttachments = TableRegistry::get('NothiNoteAttachments');

            $noteAttachmentsquery = $noteAttachments->find()->where(['nothi_part_no' => $id,
                'nothi_notesheet_id' => $nothi_note_sheets_id])->toArray();

            $noteAttachmentsmap = array();
            if (!empty($noteAttachmentsquery)) {
                foreach ($noteAttachmentsquery as $key => $value) {
                    $noteAttachmentsmap[$value['note_no']][] = array(
                        'user_file_name' => $value['user_file_name'],
                        'attachment_type' => str_replace("; charset=binary", '',
                            $value['attachment_type']),
                        'file_name' => $value['file_name'],
                        'id' => $value['id']
                    );
                }
            }


            $this->set('noteAttachmentsmap', $noteAttachmentsmap);

            $employeeOfficeDesignation = array();
            $allPermittedUser = $nothiMasterPermissionsTable->getMasterNothiListbyMasterId($nothiPartsInformation['nothi_masters_id'],
                $nothi_office_id);

            if (!empty($allPermittedUser)) {
                foreach ($allPermittedUser as $key => $value) {
                    $employeeOfficeDesignation[$value['office_unit_organograms_id']]
                        = array(
                        $value['designation_level'], 1
                    );
                }
            }

            $jsonArray = array(
                "status" => "success",
                "data" => array(
                    "employeeOfficeDesignation" => $employeeOfficeDesignation,
                    "signatures" => $this->getSignatures($id),
                    "pagination" => $query,
                    "notes" => $notesquery,
                    "nothi_note_sheets_id" => $nothi_note_sheets_id,
                    "noteAttachments" => $noteAttachmentsmap,
                    'nothiInformation' => $nothiRecord,
                    'canNote' => $canNote
                )
            );
        } catch (\PDOException $ex) {
            $jsonArray = array(
                "status" => "error",
                "error_code" => 404,
                "message" => "দুঃখিত! কোনো তথ্য পাওয়া যায়নি"
            );
        }

        echo json_encode($jsonArray);
        die;
    }

    public function ApiNothiParts($part_id, $nothi_office, $list_type = null)
    {

        $user_designation = $this->request->query['user_designation'];
        $jsonArray = array(
            "status" => "error",
            "error_code" => 404,
            "message" => "Invalid Request"
        );

        $apikey = isset($this->request->query['api_key']) ? $this->request->query['api_key']
            : '';
        if (empty($apikey) || $this->checkToken($apikey) == FALSE) {
            echo json_encode($jsonArray);
            die;
        }

        if (!defined("PROJAPOTI_API") || PROJAPOTI_API == false) {
            echo json_encode($jsonArray);
            die;
        }
        if (empty($user_designation)) {
            echo json_encode($jsonArray);
            die;
        }
        if (empty($part_id)) {
            echo json_encode($jsonArray);
            die;
        }
        $result = array();
        try {
            $EmployeeOfficesTable = TableRegistry::get('EmployeeOffices');
            $employee_office = $EmployeeOfficesTable->find()->where(['office_unit_organogram_id' => $user_designation,
                'status' => 1])->first();

            if (!empty($nothi_office)) {
                $this->switchOffice($nothi_office, "apiOffice");
            } else {
                $this->switchOffice($employee_office['office_id'], "apiOffice");
            }


            $nothiMasterPermissionsTable = TableRegistry::get("NothiMasterPermissions");
            $nothiMasterCurrentUsersTable = TableRegistry::get("NothiMasterCurrentUsers");

            $nothiPartsTable = TableRegistry::get("NothiParts");

            $nothiMastersCurrentUser = array();

            $nothiPartsInformation = $nothiPartsTable->getAll(['NothiParts.id' => $part_id])->first();

            $allNothiMasterList = $nothiMasterPermissionsTable->getMasterNothiPartList($nothi_office,
                $employee_office['office_id'],
                $employee_office['office_unit_id'],
                $employee_office['office_unit_organogram_id'],
                $nothiPartsInformation['nothi_masters_id']);

            $permitted_nothi_list = $nothiMasterPermissionsTable->allNothiPermissionList($nothi_office,
                $employee_office['office_id'],
                $employee_office['office_unit_id'],
                $employee_office['office_unit_organogram_id']);

            $allNothiParts = array();

            $allNothiParts = $nothiPartsTable->find()->select(['id', 'nothi_part_no_bn',
                'NothiNotes.subject'])
                ->join([
                    'NothiNotes' => [
                        'table' => 'nothi_notes',
                        'type' => 'left',
                        'conditions' => "NothiNotes.nothi_part_no = NothiParts.id AND NothiNotes.note_no=0"
                    ]
                ])
                ->where(['NothiParts.nothi_masters_id' => $nothiPartsInformation['nothi_masters_id'],
                    'NothiParts.id IN' => $permitted_nothi_list])->order(['NothiParts.id  ASC'])->toArray();


            $nothiMasterPriviligeType = array();

            if (!empty($allNothiMasterList)) {
                foreach ($allNothiMasterList as $key => $nothiMasters) {

                    $nothiMastersCurrentUser = $nothiMasterCurrentUsersTable->find()->where(['nothi_part_no' => $nothiMasters['nothi_part_no'],
                        'office_id' => $employee_office['office_id'], 'office_unit_id' => $employee_office['office_unit_id'],
                        'office_unit_organogram_id' => $employee_office['office_unit_organogram_id'],
                        'nothi_office' => $nothi_office]);

                    if ($list_type == 'inbox') {
                        $nothiMastersCurrentUser = $nothiMastersCurrentUser->where(['is_finished' => 0, 'is_archive' => 0]);
                    }
                    $nothiMastersCurrentUser = $nothiMastersCurrentUser->first();
                    if (!empty($nothiMastersCurrentUser)) {
                        $nothiMasterPriviligeType[$nothiMasters['nothi_part_no']]
                            = 1;
                    }

                    if (!empty($nothiMastersCurrentUser) && $nothiMastersCurrentUser['view_status']
                        == 0
                    ) {
                        $nothiMastersCurrentUser->view_status = 1;
                        $nothiMasterCurrentUsersTable->save($nothiMastersCurrentUser);
                    }
                }
            }

            // Sometimes Notes have no onucched . We have to make sure subject is not null on that note
            if (!empty($allNothiParts)) {
                foreach ($allNothiParts as $an_k => $an_v) {
                    if (isset($an_v['NothiNotes']) && !isset($an_v['NothiNotes']['subject'])) {
                        $an_v['NothiNotes']['subject'] = 'কোন অনুচ্ছেদ দেওয়া হয় নি';
                    } else {
                        $an_v['NothiNotes']['subject'] = strip_tags($an_v['NothiNotes']['subject']);
                    }
                }
            }

            $jsonArray = array(
                "status" => "success",
                "parts_no" => $allNothiParts,
                "my_desk" => $nothiMasterPriviligeType,
            );
        } catch (\Exception $ex) {
            $jsonArray = array(
                "status" => "error",
                "error_code" => 404,
                "message" => "দুঃখিত! কোনো পত্র পাওয়া যায়নি"
            );
        }

        $this->response->body(json_encode($jsonArray));
        $this->response->type('application/json');
        return $this->response;
    }

    public function postApiNothiParts($part_id, $nothi_office, $list_type = null)
    {

        $user_designation = $this->request->data['user_designation'];
        $jsonArray = array(
            "status" => "error",
            "error_code" => 404,
            "message" => "Invalid Request"
        );

        $apikey = isset($this->request->data['api_key']) ? $this->request->data['api_key']
            : '';
        if (empty($apikey) || $this->checkToken($apikey) == FALSE
        ) {
            echo json_encode($jsonArray);
            die;
        }

        if (!defined("PROJAPOTI_API") || PROJAPOTI_API == false) {
            echo json_encode($jsonArray);
            die;
        }
        if (empty($user_designation)) {
            echo json_encode($jsonArray);
            die;
        }
        if (empty($part_id)) {
            echo json_encode($jsonArray);
            die;
        }
        $result = array();
        try {
            $EmployeeOfficesTable = TableRegistry::get('EmployeeOffices');
            $employee_office = $EmployeeOfficesTable->find()->where(['office_unit_organogram_id' => $user_designation,
                'status' => 1])->first();

            if (!empty($nothi_office)) {
                $this->switchOffice($nothi_office, "apiOffice");
            } else {
                $this->switchOffice($employee_office['office_id'], "apiOffice");
            }


            $nothiMasterPermissionsTable = TableRegistry::get("NothiMasterPermissions");
            $nothiMasterCurrentUsersTable = TableRegistry::get("NothiMasterCurrentUsers");

            $nothiPartsTable = TableRegistry::get("NothiParts");

            $nothiMastersCurrentUser = array();

            $nothiPartsInformation = $nothiPartsTable->getAll(['NothiParts.id' => $part_id])->first();

            $allNothiMasterList = $nothiMasterPermissionsTable->getMasterNothiPartList($nothi_office,
                $employee_office['office_id'],
                $employee_office['office_unit_id'],
                $employee_office['office_unit_organogram_id'],
                $nothiPartsInformation['nothi_masters_id']);

            $permitted_nothi_list = $nothiMasterPermissionsTable->allNothiPermissionList($nothi_office,
                $employee_office['office_id'],
                $employee_office['office_unit_id'],
                $employee_office['office_unit_organogram_id']);

            $allNothiParts = array();

            $allNothiParts = $nothiPartsTable->find()->select(['id', 'nothi_part_no_bn',
                'NothiNotes.subject'])
                ->join([
                    'NothiNotes' => [
                        'table' => 'nothi_notes',
                        'type' => 'left',
                        'conditions' => "NothiNotes.nothi_part_no = NothiParts.id AND NothiNotes.note_no=0"
                    ]
                ])
                ->where(['NothiParts.nothi_masters_id' => $nothiPartsInformation['nothi_masters_id'],
                    'NothiParts.id IN' => $permitted_nothi_list])->order(['NothiParts.id  ASC'])->toArray();


            $nothiMasterPriviligeType = array();

            if (!empty($allNothiMasterList)) {
                foreach ($allNothiMasterList as $key => $nothiMasters) {

                    $nothiMastersCurrentUser = $nothiMasterCurrentUsersTable->find()->where(['nothi_part_no' => $nothiMasters['nothi_part_no'],
                        'office_id' => $employee_office['office_id'], 'office_unit_id' => $employee_office['office_unit_id'],
                        'office_unit_organogram_id' => $employee_office['office_unit_organogram_id'],
                        'nothi_office' => $nothi_office]);

                    if ($list_type == 'inbox') {
                        $nothiMastersCurrentUser = $nothiMastersCurrentUser->where(['is_finished' => 0, 'is_archive' => 0]);
                    }
                    $nothiMastersCurrentUser = $nothiMastersCurrentUser->first();

                    if (!empty($nothiMastersCurrentUser)) {
                        $nothiMasterPriviligeType[$nothiMasters['nothi_part_no']]
                            = 1;
                    }

                    if (!empty($nothiMastersCurrentUser) && $nothiMastersCurrentUser['view_status']
                        == 0
                    ) {
                        $nothiMastersCurrentUser->view_status = 1;
                        $nothiMasterCurrentUsersTable->save($nothiMastersCurrentUser);
                    }
                }
            }
            // Sometimes Notes have no onucched . We have to make sure subject is not null on that note
            if (!empty($allNothiParts)) {
                foreach ($allNothiParts as $an_k => $an_v) {
                    if (isset($an_v['NothiNotes']) && !isset($an_v['NothiNotes']['subject'])) {
                        $an_v['NothiNotes']['subject'] = 'কোন অনুচ্ছেদ দেওয়া হয় নি';
                    } else {
                        $an_v['NothiNotes']['subject'] = strip_tags($an_v['NothiNotes']['subject']);
                    }
                }
            }

            $jsonArray = array(
                "status" => "success",
                "parts_no" => $allNothiParts,
                "my_desk" => $nothiMasterPriviligeType,
            );
        } catch (\Exception $ex) {
            $jsonArray = array(
                "status" => "error",
                "error_code" => 404,
                "message" => "দুঃখিত! কোনো পত্র পাওয়া যায়নি"
            );
        }

        $this->response->body(json_encode($jsonArray));
        $this->response->type('application/json');
        return $this->response;
    }

    public function ApiShowForwaredUsers($nothiPartId, $nothi_office)
    {

        $user_designation = $this->request->query['user_designation'];
        $jsonArray = array(
            "status" => "error",
            "error_code" => 404,
            "message" => "Invalid Request"
        );

        $apikey = isset($this->request->query['api_key']) ? $this->request->query['api_key']
            : '';
        if (empty($apikey) || $this->checkToken($apikey) == FALSE) {
            echo json_encode($jsonArray);
            die;
        }

        if (!defined("PROJAPOTI_API") || PROJAPOTI_API == false) {
            echo json_encode($jsonArray);
            die;
        }
        if (empty($user_designation)) {
            echo json_encode($jsonArray);
            die;
        }
        if (empty($nothiPartId)) {
            echo json_encode($jsonArray);
            die;
        }
        if ($apikey != API_KEY) {
            //verify api token
            $this->loadComponent('ApiRelated');
            $getApiTokenData = $this->ApiRelated->getMobileApiTokenData($apikey);
            if (!empty($getApiTokenData)) {
                if (!in_array($user_designation, $getApiTokenData['designations'])) {
                    $jsonArray['message'] = 'Unauthorized request';
                    echo json_encode($jsonArray);
                    die;
                }
            }
            //verify api token
        }
        $result = array();
        try {
            $EmployeeOfficesTable = TableRegistry::get('EmployeeOffices');
            $employee_office = $EmployeeOfficesTable->find()->where(['office_unit_organogram_id' => $user_designation,
                'status' => 1])->first();

            $this->switchOffice($nothi_office, "apiOffice");

            $api_nothi_part_flag_table = TableRegistry::get('ApiNothiPartFlag');
            $nothi_part_flag_count = $api_nothi_part_flag_table->find()->where(['part_id'=>$nothiPartId,'nothi_office_id' => $nothi_office,'office_unit_organogram_id'=>$user_designation])->first();
            if(!empty($nothi_part_flag_count)){
                $jsonArray = array(
                    "status" => "error",
                    "error_code" => 404
                );

                if($nothi_part_flag_count['type']== 1){
                    $jsonArray['message'] = 'দুঃখিত! খসড়াপত্র সংরক্ষন না করে নথি প্রেরন করা যাবে না। অনুগ্রহপুর্বক খসড়াপত্রটি সংরক্ষন করুন।';
                } else {
                    $jsonArray['message'] = 'দুঃখিত! অনুচ্ছেদ সংরক্ষন না করে নথি প্রেরন করা যাবে না। অনুগ্রহপুর্বক অনুচ্ছেদটি সংরক্ষন করুন।';
                }

                $this->response->body(json_encode($jsonArray));
                $this->response->type('application/json');
                return $this->response;
            }

            $nothiMasterPermissionsTable = TableRegistry::get("NothiMasterPermissions");
            $nothiPartsTable = TableRegistry::get('NothiParts');
            $nothiInformation = $nothiPartsTable->get($nothiPartId);

            $nothiMasterPermissionsRecord = $nothiMasterPermissionsTable->find()->where(['nothi_masters_id' => $nothiInformation['nothi_masters_id'],
                'nothi_part_no' => $nothiInformation['id'], 'nothi_office' => $nothi_office])->order(['designation_level asc'])->hydrate(false)->toArray();
            $permitted_users = array();

            if (!empty($nothiMasterPermissionsRecord)) {

                $employeeRecordTable = TableRegistry::get('EmployeeRecords');

                $officeTable = TableRegistry::get('Offices');
                $officeUnitTable = TableRegistry::get('OfficeUnits');

                foreach ($nothiMasterPermissionsRecord as $key => $nothiUserInfo) {

                    $employeeOfficeRecord = $EmployeeOfficesTable->find()->where(['office_id' => $nothiUserInfo['office_id'],
                        'office_unit_id' => $nothiUserInfo['office_unit_id'],
                        'office_unit_organogram_id' => $nothiUserInfo['office_unit_organograms_id'],
                        'office_unit_organogram_id <> ' => $employee_office['office_unit_organogram_id'],
                        'status' => 1])->first();

                    if (!empty($employeeOfficeRecord)) {
                        $employeeRecord = $employeeRecordTable->get($employeeOfficeRecord['employee_record_id']);

                        $officeUnitRecord = $officeUnitTable->get($nothiUserInfo['office_unit_id']);

                        $officeInfo = array();
                        if ($nothiUserInfo['office_id'] != $employee_office['office_id']) {
                            $officeInfo = $officeTable->get($nothiUserInfo['office_id']);
                        }

                        $permitted_users[] = [
                            'to_office_id' => $nothiUserInfo['office_id'],
                            'to_unit_id' => $nothiUserInfo['office_unit_id'],
                            'to_unit_name' => $officeUnitRecord['unit_name_bng'],
                            'to_officer_name' => $employeeRecord['name_bng'],
                            'to_org_id' => $nothiUserInfo['office_unit_organograms_id'],
                            'to_org_name' => $employeeOfficeRecord->designation,
                            'to_officer_id' => $employeeRecord['id']
                        ];
                    }
                }
            }
            $jsonArray = array(
                "status" => "success",
                "users" => $permitted_users
            );
        } catch (\Exception $ex) {
            $jsonArray = array(
                "status" => "error",
                "error_code" => 404,
                "message" => "দুঃখিত! কোনো তথ্য পাওয়া যায়নি"
            );
        }

        $this->response->body(json_encode($jsonArray));
        $this->response->type('application/json');
        return $this->response;
    }

    public function postApiShowForwaredUsers($nothiPartId, $nothi_office)
    {

        $user_designation = $this->request->data['user_designation'];
        $jsonArray = array(
            "status" => "error",
            "error_code" => 404,
            "message" => "Invalid Request"
        );

        $apikey = isset($this->request->data['api_key']) ? $this->request->data['api_key']
            : '';
        if (empty($apikey) || $this->checkToken($apikey) == FALSE
        ) {
            echo json_encode($jsonArray);
            die;
        }

        if (!defined("PROJAPOTI_API") || PROJAPOTI_API == false) {
            echo json_encode($jsonArray);
            die;
        }
        if (empty($user_designation)) {
            echo json_encode($jsonArray);
            die;
        }
        if (empty($nothiPartId)) {
            echo json_encode($jsonArray);
            die;
        }
        if ($apikey != API_KEY) {
            //verify api token
            $this->loadComponent('ApiRelated');
            $getApiTokenData = $this->ApiRelated->getMobileApiTokenData($apikey);
            if (!empty($getApiTokenData)) {
                if (!in_array($user_designation, $getApiTokenData['designations'])) {
                    $jsonArray['message'] = 'Unauthorized request';
                    echo json_encode($jsonArray);
                    die;
                }
            }
            //verify api token
        }
        $result = array();
        try {
            $EmployeeOfficesTable = TableRegistry::get('EmployeeOffices');
            $employee_office = $EmployeeOfficesTable->find()->where(['office_unit_organogram_id' => $user_designation,
                'status' => 1])->first();

            $this->switchOffice($nothi_office, "apiOffice");

            $api_nothi_part_flag_table = TableRegistry::get('ApiNothiPartFlag');
            $nothi_part_flag_count = $api_nothi_part_flag_table->find()->where(['part_id'=>$nothiPartId,'nothi_office_id' => $nothi_office,'office_unit_organogram_id'=>$user_designation])->first();
            if(!empty($nothi_part_flag_count)){
                $jsonArray = array(
                    "status" => "error",
                    "error_code" => 404
                );

                if($nothi_part_flag_count['type']== 1){
                    $jsonArray['message'] = 'দুঃখিত! খসড়াপত্র সংরক্ষন না করে নথি প্রেরন করা যাবে না। অনুগ্রহপুর্বক খসড়াপত্রটি সংরক্ষন করুন।';
                } else {
                    $jsonArray['message'] = 'দুঃখিত! অনুচ্ছেদ সংরক্ষন না করে নথি প্রেরন করা যাবে না। অনুগ্রহপুর্বক অনুচ্ছেদটি সংরক্ষন করুন।';
                }

                $this->response->body(json_encode($jsonArray));
                $this->response->type('application/json');
                return $this->response;
            }

            $nothiMasterPermissionsTable = TableRegistry::get("NothiMasterPermissions");
            $nothiPartsTable = TableRegistry::get('NothiParts');
            $nothiInformation = $nothiPartsTable->get($nothiPartId);

            $nothiMasterPermissionsRecord = $nothiMasterPermissionsTable->find()->where(['nothi_masters_id' => $nothiInformation['nothi_masters_id'],
                'nothi_part_no' => $nothiInformation['id'], 'nothi_office' => $nothi_office])->order(['designation_level asc'])->hydrate(false)->toArray();
            $permitted_users = array();

            if (!empty($nothiMasterPermissionsRecord)) {

                $employeeRecordTable = TableRegistry::get('EmployeeRecords');

                $officeTable = TableRegistry::get('Offices');
                $officeUnitTable = TableRegistry::get('OfficeUnits');

                foreach ($nothiMasterPermissionsRecord as $key => $nothiUserInfo) {

                    $employeeOfficeRecord = $EmployeeOfficesTable->find()->where(['office_id' => $nothiUserInfo['office_id'],
                        'office_unit_id' => $nothiUserInfo['office_unit_id'],
                        'office_unit_organogram_id' => $nothiUserInfo['office_unit_organograms_id'],
                        'office_unit_organogram_id <> ' => $employee_office['office_unit_organogram_id'],
                        'status' => 1])->first();

                    if (!empty($employeeOfficeRecord)) {
                        $employeeRecord = $employeeRecordTable->get($employeeOfficeRecord['employee_record_id']);

                        $officeUnitRecord = $officeUnitTable->get($nothiUserInfo['office_unit_id']);

                        $officeInfo = array();
                        if ($nothiUserInfo['office_id'] != $employee_office['office_id']) {
                            $officeInfo = $officeTable->get($nothiUserInfo['office_id']);
                        }

                        $permitted_users[] = [
                            'to_office_id' => $nothiUserInfo['office_id'],
                            'to_unit_id' => $nothiUserInfo['office_unit_id'],
                            'to_unit_name' => $officeUnitRecord['unit_name_bng'],
                            'to_officer_name' => $employeeRecord['name_bng'],
                            'to_org_id' => $nothiUserInfo['office_unit_organograms_id'],
                            'to_org_name' => $employeeOfficeRecord->designation,
                            'to_officer_id' => $employeeRecord['id']
                        ];
                    }
                }
            }
            $jsonArray = array(
                "status" => "success",
                "users" => $permitted_users
            );
        } catch (\Exception $ex) {
            $jsonArray = array(
                "status" => "error",
                "error_code" => 404,
                "message" => "দুঃখিত! কোনো তথ্য পাওয়া যায়নি"
            );
        }

        $this->response->body(json_encode($jsonArray));
        $this->response->type('application/json');
        return $this->response;
    }

    public function ApiNothiPotroLists($nothiPartId, $nothi_office)
    {
        $user_designation = $this->request->query['user_designation'];
        $jsonArray = array(
            "status" => "error",
            "error_code" => 404,
            "message" => "Invalid Request"
        );

        $apikey = isset($this->request->query['api_key']) ? $this->request->query['api_key']
            : '';
        if (empty($apikey) || $this->checkToken($apikey) == FALSE) {
            echo json_encode($jsonArray);
            die;
        }

        if (!defined("PROJAPOTI_API") || PROJAPOTI_API == false) {
            echo json_encode($jsonArray);
            die;
        }
        if (empty($user_designation)) {
            echo json_encode($jsonArray);
            die;
        }
        if (empty($nothiPartId)) {
            echo json_encode($jsonArray);
            die;
        }
        if ($apikey != API_KEY) {
            //verify api token
            $this->loadComponent('ApiRelated');
            $getApiTokenData = $this->ApiRelated->getMobileApiTokenData($apikey);
            if (!empty($getApiTokenData)) {
                if (!in_array($user_designation, $getApiTokenData['designations'])) {
                    $jsonArray['message'] = 'Unauthorized request';
                    echo json_encode($jsonArray);
                    die;
                }
            }
            //verify api token
        }
        $total = 0;
        $potroAttachmentRecord = array();
        try {
            $EmployeeOfficesTable = TableRegistry::get('EmployeeOffices');
            $employee_office = $EmployeeOfficesTable->find()->where(['office_unit_organogram_id' => $user_designation,
                'status' => 1])->first();

            $this->switchOffice($nothi_office, "apiOffice");

            $potroAttachmentTable = TableRegistry::get('NothiPotroAttachments');
            $nothiMasterPermissionsTable = TableRegistry::get("NothiMasterPermissions");
            $nothiPartsTable = TableRegistry::get("NothiParts");

            $nothiInformation = $nothiPartsTable->get($nothiPartId);

            $nothiMastersAccess = $nothiMasterPermissionsTable->hasAccess($nothi_office,
                $employee_office['office_id'],
                $employee_office['office_unit_id'],
                $employee_office['office_unit_organogram_id'],
                $nothiInformation['nothi_masters_id'], $nothiInformation['id']);

            if (!empty($nothiMastersAccess)) {
                $total = $potroAttachmentTable->find()->where(['nothi_part_no' => $nothiPartId,
                    '(is_summary_nothi IS NULL OR is_summary_nothi = 0)', 'nothijato' => 0, 'status' => 1])->group(['nothi_potro_id'])->count();

                $potroAttachmentRecord = $potroAttachmentTable->find()->select([
                    'id',
                    'sarok_no',
                    'total' => 'count(nothi_potro_id)'
                ])->where(['nothi_part_no' => $nothiPartId,
                    '(is_summary_nothi IS NULL OR is_summary_nothi = 0)', 'nothijato' => 0, 'status' => 1])->group(['nothi_potro_id'])->order(['id desc'])->toArray();
            }

            $jsonArray = array(
                "status" => "success",
                'total' => $total,
                'potros' => $potroAttachmentRecord
            );
        } catch (\Exception $ex) {
            $jsonArray = array(
                "status" => "error",
                "error_code" => 404,
                "message" => "দুঃখিত! কোনো পত্র পাওয়া যায়নি"
            );
        }

        $this->response->body(json_encode($jsonArray));
        $this->response->type('application/json');
        return $this->response;
    }

    public function postApiNothiPotroLists($nothiPartId, $nothi_office)
    {
        $user_designation = $this->request->data['user_designation'];
        $jsonArray = array(
            "status" => "error",
            "error_code" => 404,
            "message" => "Invalid Request"
        );

        $apikey = isset($this->request->data['api_key']) ? $this->request->data['api_key']
            : '';
        if (empty($apikey) || $this->checkToken($apikey) == FALSE
        ) {
            echo json_encode($jsonArray);
            die;
        }

        if (!defined("PROJAPOTI_API") || PROJAPOTI_API == false) {
            echo json_encode($jsonArray);
            die;
        }
        if (empty($user_designation)) {
            echo json_encode($jsonArray);
            die;
        }
        if (empty($nothiPartId)) {
            echo json_encode($jsonArray);
            die;
        }
        if ($apikey != API_KEY) {
            //verify api token
            $this->loadComponent('ApiRelated');
            $getApiTokenData = $this->ApiRelated->getMobileApiTokenData($apikey);
            if (!empty($getApiTokenData)) {
                if (!in_array($user_designation, $getApiTokenData['designations'])) {
                    $jsonArray['message'] = 'Unauthorized request';
                    echo json_encode($jsonArray);
                    die;
                }
            }
            //verify api token
        }
        $total = 0;
        $potroAttachmentRecord = array();
        try {
            $EmployeeOfficesTable = TableRegistry::get('EmployeeOffices');
            $employee_office = $EmployeeOfficesTable->find()->where(['office_unit_organogram_id' => $user_designation,
                'status' => 1])->first();

            $this->switchOffice($nothi_office, "apiOffice");

            $potroAttachmentTable = TableRegistry::get('NothiPotroAttachments');
            $nothiMasterPermissionsTable = TableRegistry::get("NothiMasterPermissions");
            $nothiPartsTable = TableRegistry::get("NothiParts");

            $nothiInformation = $nothiPartsTable->get($nothiPartId);

            $nothiMastersAccess = $nothiMasterPermissionsTable->hasAccess($nothi_office,
                $employee_office['office_id'],
                $employee_office['office_unit_id'],
                $employee_office['office_unit_organogram_id'],
                $nothiInformation['nothi_masters_id'], $nothiInformation['id']);

            if (!empty($nothiMastersAccess)) {
                $total = $potroAttachmentTable->find()->where(['nothi_part_no' => $nothiPartId,
                    '(is_summary_nothi IS NULL OR is_summary_nothi = 0)', 'nothijato' => 0, 'status' => 1])->group(['nothi_potro_id'])->count();

                $potroAttachmentRecord = $potroAttachmentTable->find()->select([
                    'id',
                    'sarok_no',
                    'total' => 'count(nothi_potro_id)'
                ])->where(['nothi_part_no' => $nothiPartId,
                    '(is_summary_nothi IS NULL OR is_summary_nothi = 0)', 'nothijato' => 0, 'status' => 1])->group(['nothi_potro_id'])->order(['id desc'])->toArray();
            }

            $jsonArray = array(
                "status" => "success",
                'total' => $total,
                'potros' => $potroAttachmentRecord
            );
        } catch (\Exception $ex) {
            $jsonArray = array(
                "status" => "error",
                "error_code" => 404,
                "message" => "দুঃখিত! কোনো পত্র পাওয়া যায়নি"
            );
        }

        $this->response->body(json_encode($jsonArray));
        $this->response->type('application/json');
        return $this->response;
    }

    public function ApiNothiPotros($id, $nothiPartId, $nothi_office)
    {
        $user_designation = $this->request->query['user_designation'];
        $jsonArray = array(
            "status" => "error",
            "error_code" => 404,
            "message" => "Invalid Request"
        );

        $apikey = isset($this->request->query['api_key']) ? $this->request->query['api_key']
            : '';
        if (empty($apikey) || $this->checkToken($apikey) == FALSE) {
            echo json_encode($jsonArray);
            die;
        }

        if (!defined("PROJAPOTI_API") || PROJAPOTI_API == false) {
            echo json_encode($jsonArray);
            die;
        }

        if (empty($user_designation)) {
            echo json_encode($jsonArray);
            die;
        }

        if (empty($nothiPartId)) {
            echo json_encode($jsonArray);
            die;
        }

        if (empty($id)) {
            echo json_encode($jsonArray);
            die;
        }
        if ($apikey != API_KEY) {
            //verify api token
            $this->loadComponent('ApiRelated');
            $getApiTokenData = $this->ApiRelated->getMobileApiTokenData($apikey);
            if (!empty($getApiTokenData)) {
                if (!in_array($user_designation, $getApiTokenData['designations'])) {
                    $jsonArray['message'] = 'Unauthorized request';
                    echo json_encode($jsonArray);
                    die;
                }
            }
            //verify api token
        }
        $potroAttachmentRecord = array();
        $all_potros = array();
        try {
            $EmployeeOfficesTable = TableRegistry::get('EmployeeOffices');
            $employee_office = $EmployeeOfficesTable->find()->where(['office_unit_organogram_id' => $user_designation,
                'status' => 1])->first();

            $this->switchOffice($nothi_office, "apiOffice");

            $potroAttachmentTable = TableRegistry::get('NothiPotroAttachments');
            $nothiMasterPermissionsTable = TableRegistry::get("NothiMasterPermissions");
            $nothiPartsTable = TableRegistry::get("NothiParts");

            $nothiInformation = $nothiPartsTable->get($nothiPartId);

            $nothiMastersAccess = $nothiMasterPermissionsTable->hasAccess($nothi_office,
                $employee_office['office_id'],
                $employee_office['office_unit_id'],
                $employee_office['office_unit_organogram_id'],
                $nothiInformation['nothi_masters_id'], $nothiInformation['id']);


            if (!empty($nothiMastersAccess)) {

                $potroAttachmentRecord = $potroAttachmentTable->get($id);

                if (!empty($potroAttachmentRecord)) {
                    $all_potros = $potroAttachmentTable->getAllPotro($potroAttachmentRecord['nothi_potro_id']);
                }
            }

            $jsonArray = array(
                "status" => "success",
                'potros' => $all_potros
            );
        } catch (\Exception $ex) {
            $jsonArray = array(
                "status" => "error",
                "error_code" => 404,
                "message" => "দুঃখিত! কোনো পত্র পাওয়া যায়নি"
            );
        }

        $this->response->body(json_encode($jsonArray));
        $this->response->type('application/json');
        return $this->response;
    }

    public function postApiNothiPotros($id, $nothiPartId, $nothi_office)
    {
        $user_designation = $this->request->data['user_designation'];
        $jsonArray = array(
            "status" => "error",
            "error_code" => 404,
            "message" => "Invalid Request"
        );

        $apikey = isset($this->request->data['api_key']) ? $this->request->data['api_key']
            : '';
        if (empty($apikey) || $this->checkToken($apikey) == FALSE
        ) {
            echo json_encode($jsonArray);
            die;
        }

        if (!defined("PROJAPOTI_API") || PROJAPOTI_API == false) {
            echo json_encode($jsonArray);
            die;
        }

        if (empty($user_designation)) {
            echo json_encode($jsonArray);
            die;
        }

        if (empty($nothiPartId)) {
            echo json_encode($jsonArray);
            die;
        }

        if (empty($id)) {
            echo json_encode($jsonArray);
            die;
        }
        if ($apikey != API_KEY) {
            //verify api token
            $this->loadComponent('ApiRelated');
            $getApiTokenData = $this->ApiRelated->getMobileApiTokenData($apikey);
            if (!empty($getApiTokenData)) {
                if (!in_array($user_designation, $getApiTokenData['designations'])) {
                    $jsonArray['message'] = 'Unauthorized request';
                    echo json_encode($jsonArray);
                    die;
                }
            }
            //verify api token
        }
        $potroAttachmentRecord = array();
        $all_potros = array();
        try {
            $EmployeeOfficesTable = TableRegistry::get('EmployeeOffices');
            $employee_office = $EmployeeOfficesTable->find()->where(['office_unit_organogram_id' => $user_designation,
                'status' => 1])->first();

            $this->switchOffice($nothi_office, "apiOffice");

            $potroAttachmentTable = TableRegistry::get('NothiPotroAttachments');
            $nothiMasterPermissionsTable = TableRegistry::get("NothiMasterPermissions");
            $nothiPartsTable = TableRegistry::get("NothiParts");

            $nothiInformation = $nothiPartsTable->get($nothiPartId);

            $nothiMastersAccess = $nothiMasterPermissionsTable->hasAccess($nothi_office,
                $employee_office['office_id'],
                $employee_office['office_unit_id'],
                $employee_office['office_unit_organogram_id'],
                $nothiInformation['nothi_masters_id'], $nothiInformation['id']);


            if (!empty($nothiMastersAccess)) {

                $potroAttachmentRecord = $potroAttachmentTable->get($id);

                if (!empty($potroAttachmentRecord)) {
                    $all_potros = $potroAttachmentTable->getAllPotro($potroAttachmentRecord['nothi_potro_id']);
                }
            }

            $jsonArray = array(
                "status" => "success",
                'potros' => $all_potros
            );
        } catch (\Exception $ex) {
            $jsonArray = array(
                "status" => "error",
                "error_code" => 404,
                "message" => "দুঃখিত! কোনো পত্র পাওয়া যায়নি"
            );
        }

        $this->response->body(json_encode($jsonArray));
        $this->response->type('application/json');
        return $this->response;
    }

    public function apiGetNothiMasterParts()
    {
        $user_designation = $this->request->query['user_designation'];
        $jsonArray = array(
            "status" => "error",
            "error_code" => 404,
            "message" => "Invalid Request"
        );

        $apikey = isset($this->request->query['api_key']) ? $this->request->query['api_key']
            : '';
        if (empty($apikey) || $this->checkToken($apikey) == FALSE) {
            echo json_encode($jsonArray);
            die;
        }

        if (!defined("PROJAPOTI_API") || PROJAPOTI_API == false) {
            echo json_encode($jsonArray);
            die;
        }

        if (empty($user_designation)) {
            echo json_encode($jsonArray);
            die;
        }
        if ($apikey != API_KEY) {
            //verify api token
            $this->loadComponent('ApiRelated');
            $getApiTokenData = $this->ApiRelated->getMobileApiTokenData($apikey);
            if (!empty($getApiTokenData)) {
                if (!in_array($user_designation, $getApiTokenData['designations'])) {
                    $jsonArray['message'] = 'Unauthorized request';
                    echo json_encode($jsonArray);
                    die;
                }
            }
            //verify api token
        }
        $result = [];

        try {
            $EmployeeOfficesTable = TableRegistry::get('EmployeeOffices');
            $employee_office = $EmployeeOfficesTable->find()->where(['office_unit_organogram_id' => $user_designation,
                'status' => 1])->first();

            $this->switchOffice($employee_office['office_id'], "apiOffice");

            TableRegistry::remove("NothiMasterPermissions");
            $nothiMasterPermissionsTable = TableRegistry::get("NothiMasterPermissions");
            $nothiMastersTable = TableRegistry::get("NothiMasters");

            $allNothiMasters = $nothiMasterPermissionsTable->getMasterNothiList($employee_office['office_id'],
                $employee_office['office_id'],
                $employee_office['office_unit_id'], $user_designation);

            $allMastersId = [];

            if (!empty($allNothiMasters)) {
                foreach ($allNothiMasters as $key => $value) {
                    $allMastersId[] = $value['nothi_masters_id'];
                }
            }
            $result = $nothiMastersTable->getPartsList($allMastersId);

            $jsonArray = array(
                "status" => "success",
                'data' => $result
            );
        } catch (\Exception $ex) {
            $jsonArray = array(
                "status" => "error",
                "error_code" => 404,
                "message" => "দুঃখিত! কোনো পত্র পাওয়া যায়নি"
            );
        }

        $this->response->body(json_encode($jsonArray));
        $this->response->type('application/json');
        return $this->response;
    }

    public function postApiGetNothiMasterParts()
    {
        $user_designation = $this->request->data['user_designation'];
        $jsonArray = array(
            "status" => "error",
            "error_code" => 404,
            "message" => "Invalid Request"
        );

        $apikey = isset($this->request->data['api_key']) ? $this->request->data['api_key']
            : '';
        if (empty($apikey) || $this->checkToken($apikey) == FALSE
        ) {
            echo json_encode($jsonArray);
            die;
        }

        if (!defined("PROJAPOTI_API") || PROJAPOTI_API == false) {
            echo json_encode($jsonArray);
            die;
        }

        if (empty($user_designation)) {
            echo json_encode($jsonArray);
            die;
        }
        if ($apikey != API_KEY) {
            //verify api token
            $this->loadComponent('ApiRelated');
            $getApiTokenData = $this->ApiRelated->getMobileApiTokenData($apikey);
            if (!empty($getApiTokenData)) {
                if (!in_array($user_designation, $getApiTokenData['designations'])) {
                    $jsonArray['message'] = 'Unauthorized request';
                    echo json_encode($jsonArray);
                    die;
                }
            }
            //verify api token
        }
        $result = [];

        try {
            $EmployeeOfficesTable = TableRegistry::get('EmployeeOffices');
            $employee_office = $EmployeeOfficesTable->find()->where(['office_unit_organogram_id' => $user_designation,
                'status' => 1])->first();

            $this->switchOffice($employee_office['office_id'], "apiOffice");

            TableRegistry::remove("NothiMasterPermissions");
            $nothiMasterPermissionsTable = TableRegistry::get("NothiMasterPermissions");
            $nothiMastersTable = TableRegistry::get("NothiMasters");

            $allNothiMasters = $nothiMasterPermissionsTable->getMasterNothiList($employee_office['office_id'],
                $employee_office['office_id'],
                $employee_office['office_unit_id'], $user_designation);

            $allMastersId = [];

            if (!empty($allNothiMasters)) {
                foreach ($allNothiMasters as $key => $value) {
                    $allMastersId[] = $value['nothi_masters_id'];
                }
            }
            $result = $nothiMastersTable->getPartsList($allMastersId);

            $jsonArray = array(
                "status" => "success",
                'data' => $result
            );
        } catch (\Exception $ex) {
            $jsonArray = array(
                "status" => "error",
                "error_code" => 404,
                "message" => "দুঃখিত! কোনো পত্র পাওয়া যায়নি"
            );
        }

        $this->response->body(json_encode($jsonArray));
        $this->response->type('application/json');
        return $this->response;
    }

    public function apiNothiVuktoKoron()
    {

        $user_designation = isset($this->request->query['user_designation']) ? $this->request->query['user_designation'] : (isset($this->request->data['user_designation']) ? $this->request->data['user_designation'] : '');

        $jsonArray = array(
            "status" => "error",
            "error_code" => 404,
            "message" => "Invalid Request",
            "msg" => "Invalid Request",
        );

        $apikey = isset($this->request->query['api_key']) ? $this->request->query['api_key']
            : (isset($this->request->data['api_key']) ? $this->request->data['api_key'] : '');

        if (empty($apikey) || $this->checkToken($apikey) == FALSE) {
            goto rtn;
        }


        if (!defined("PROJAPOTI_API") || PROJAPOTI_API == false) {
            goto rtn;
        }

        if (!isset($this->request->data['nothi_master_id'])) {
            $jsonArray['msg'] = $jsonArray['message'] = 'nothi master id is missing';
            goto rtn;
        }

        if (empty($user_designation)) {
            $jsonArray['msg'] = $jsonArray['message'] = 'user designation is missing';
            goto rtn;
        }
        if ($apikey != API_KEY) {
            //verify api token
            $this->loadComponent('ApiRelated');
            $getApiTokenData = $this->ApiRelated->getMobileApiTokenData($apikey);
            if (!empty($getApiTokenData)) {
                if (!in_array($user_designation, $getApiTokenData['designations'])) {
                    $jsonArray['msg'] = $jsonArray['message'] = 'Unauthorized request';
                    goto rtn;
                }
            }
            //verify api token
        }
        $EmployeeOfficesTable = TableRegistry::get('EmployeeOffices');
//        $employee_office = $EmployeeOfficesTable->find()->where(['office_unit_organogram_id' => $user_designation,
//            'status' => 1])->first();
        $employee_office = $this->setCurrentDakSection($user_designation);

        $this->switchOffice($employee_office['office_id'], "apiOffice");

        if (empty($this->request->data['nothi_part_no']) && !empty($this->request->data['nothijato']) && ($this->request->data['nothijato'] == 1)) {
            $nothiPartsTable = TableRegistry::get('NothiParts');
            $getFirstPart = $nothiPartsTable->firstNote($this->request->data['nothi_master_id']);
            if (!empty($getFirstPart)) {
                $this->request->data['nothi_part_no'] = $getFirstPart['id'];
            } else {
                $jsonArray['msg'] = $jsonArray['message'] = 'no data for given nothi master id';
                goto rtn;
            }
        }
        if (!empty($this->request->data)) {
            $requestedData = $this->request->data;

            if (empty($requestedData['nothi_part_no']) || empty($requestedData['nothi_master_id'])) {
                $jsonArray['msg'] = $jsonArray['message'] = 'দুঃখিত! নথি বাছাই করা হয়নি';
                goto rtn;
            }
            if (empty($requestedData['dak_id'])) {
                $jsonArray['msg'] = $jsonArray['message'] = 'দুঃখিত!  ডাক বাছাই করা হয়নি';
                goto rtn;
            }

            $nothiPotrosTable = TableRegistry::get("NothiPotros");
            $dakUsersTable = TableRegistry::get("DakUsers");
            $dakMovementsTable = TableRegistry::get("DakMovements");
            $dakAttachmentsTable = TableRegistry::get("DakAttachments");
            $nothiMastersTable = TableRegistry::get("NothiMasters");
            $nothiDakMovesTable = TableRegistry::get("NothiMastersDakMap");
            $nothiDakPotroMapsTable = TableRegistry::get("NothiDakPotroMaps");
            $nothiPotroAttachmentsTable = TableRegistry::get("NothiPotroAttachments");
            $nothiMasterCurrentUsersTable = TableRegistry::get("NothiMasterCurrentUsers");
            $nothiPartsTable = TableRegistry::get("NothiParts");


            $nothiPartId = $requestedData['nothi_part_no'];
            $successnothi = 0;

            $nothiPartInformation = array();
            if (!empty($nothiPartId)) {
                $nothiPartInformation = $nothiPartsTable->get($nothiPartId);
                $requestedData['nothi_master_id'] = $nothiPartInformation['nothi_masters_id'];
            }

            $dakId = $requestedData['dak_id'];

            if (empty($requestedData['dak_type'])) {
                $requestedData['dak_type'] = DAK_DAPTORIK;
            }

            if (empty($dakId)) {
                $jsonArray['msg'] = $jsonArray['message'] = 'দুঃখিত!  ডাক বাছাই করা হয়নি';
                goto rtn;
            }

            $nothi_potro_page = $nothiPotroAttachmentsTable->find()->select(['nothi_potro_page'])->where(['nothi_master_id' => $requestedData['nothi_master_id']])->order(['nothi_potro_page DESC'])->first();


            $requestedData['nothijato'] = !empty($requestedData['nothijato']) ? $requestedData['nothijato']
                : 0;

            $potroPage = !empty($nothi_potro_page->nothi_potro_page) ? ($nothi_potro_page->nothi_potro_page)
                : 0;

            try {
                $con = ConnectionManager::get('default');
                $dak_type = $requestedData['dak_type'];

                if ($dak_type == "Daptorik") {
                    $dakDaptoriksTable = TableRegistry::get("DakDaptoriks");
                } else {
                    $dakDaptoriksTable = TableRegistry::get("DakNagoriks");
                }

                $dakRecord = $dakDaptoriksTable->get($dakId);

                $dakLastMoves = $dakMovementsTable->getDak_for_nothiVukto($dakId,
                    $dak_type);

                if ($dakLastMoves['to_officer_designation_id'] != $employee_office['office_unit_organogram_id']) {
                    $con->rollback();
                    $jsonArray['msg'] = $jsonArray['message'] = 'দুঃখিত! ' . $dakRecord['subject'] . ' বিষয়ের ডাকটির অনুরোধ অগ্রহণযোগ্য';
                    goto rtn;
                }

                $dakLastMoveId = $dakLastMoves['id'];
                $nothiDakMovesRecord = $nothiDakMovesTable->newEntity();

                $attachments = $dakAttachmentsTable->find()->where(['dak_id' => $dakId,
                    'dak_type' => $dak_type])->order(['id ASC'])->toArray();
                if (empty($attachments)) {
                    $con->rollback();
                    echo json_encode(['status' => 'error', 'msg' => (($requestedData['nothijato']
                        == 0) ? 'নথিতে উপস্থাপন করা সম্ভব হচ্ছে না' : 'নথিজাত করা সম্ভব হচ্ছে না')]);
                    die;
                }
                $nothiDakPotroMapsRecord = $nothiDakPotroMapsTable->newEntity();
                $nothiPotroRecord = $nothiPotrosTable->newEntity();

                $nothiPotroRecord->nothi_master_id = $requestedData['nothi_master_id'];
                $nothiPotroRecord->nothi_part_no = $requestedData['nothi_part_no'];
                $nothiPotroRecord->dak_id = $dakId;
                $nothiPotroRecord->dak_type = $dak_type;
                $nothiPotroRecord->nothijato = ($requestedData['nothijato']
                    == 0 && $dakLastMoves['dak_actions'] == 'নথিজাত করুন') ? 1 : (($requestedData['nothijato']
                    == 1 && $dakLastMoves['dak_actions'] == 'নথিতে পেশ করুন') ? 0
                    : (isset($requestedData['nothijato']) ? $requestedData['nothijato']
                        : 0));
                $nothiPotroRecord->potro_media = (!isset($dakRecord['dak_sending_media'])
                    || empty($dakRecord['dak_sending_media'])) ? 1 : $dakRecord['dak_sending_media'];
                $nothiPotroRecord->potro_pages = count($attachments);
                $nothiPotroRecord->subject = $dakRecord['dak_subject'];
                $nothiPotroRecord->sarok_no = !empty($dakRecord['sender_sarok_no'])
                    ? $dakRecord['sender_sarok_no'] : '';
                $nothiPotroRecord->application_origin = !empty($dakRecord['application_origin'])?$dakRecord['application_origin']:'nothi';
                $nothiPotroRecord->application_meta_data = !empty($dakRecord['application_meta_data'])?$dakRecord['application_meta_data']:'';
                $nothiPotroRecord->issue_date = date("Y-m-d H:i:s");
                $nothiPotroRecord->due_date = date("Y-m-d H:i:s");
                $nothiPotroRecord->created = date("Y-m-d H:i:s");
                $nothiPotroRecord->created_by = $employee_office['employee_record_id'];
                $nothiPotroRecord->modified_by = $employee_office['employee_record_id'];

                $nothiPotroRecord = $nothiPotrosTable->save($nothiPotroRecord);

                $nothiDakPotroMapsRecord->dak_id = $nothiPotroRecord->dak_id;
                $nothiDakPotroMapsRecord->dak_type = $dak_type;
                $nothiDakPotroMapsRecord->nothi_masters_id = $nothiPotroRecord->nothi_master_id;
                $nothiDakPotroMapsRecord->nothi_part_no = $requestedData['nothi_part_no'];
                $nothiDakPotroMapsRecord->nothi_potro_id = $nothiPotroRecord->id;
                $nothiDakPotroMapsTable->save($nothiDakPotroMapsRecord);

                if (!empty($attachments)) {
                    foreach ($attachments as $attachmentKey => $attachmentValue) {
                        $potroPage++;
                        $nextPotroPagebn = Number::format($potroPage);
                        $nothiPotroAttachmentsRecord = $nothiPotroAttachmentsTable->newEntity();

                        $nothiPotroAttachmentsRecord->nothi_master_id = $requestedData['nothi_master_id'];
                        $nothiPotroAttachmentsRecord->nothi_part_no = $requestedData['nothi_part_no'];

                        $nothiPotroAttachmentsRecord->nothi_potro_id = $nothiPotroRecord->id;
                        $nothiPotroAttachmentsRecord->nothijato = $nothiPotroRecord->nothijato;
                        $nothiPotroAttachmentsRecord->sarok_no = $nothiPotroRecord->sarok_no;
                        $nothiPotroAttachmentsRecord->created_by = $employee_office['employee_record_id'];
                        $nothiPotroAttachmentsRecord->modified_by = $employee_office['employee_record_id'];
                        $nothiPotroAttachmentsRecord->created = date("Y-m-d H:i:s");
                        $nothiPotroAttachmentsRecord->modified = date("Y-m-d H:i:s");
                        $nothiPotroAttachmentsRecord->nothi_potro_page = $potroPage;
                        $nothiPotroAttachmentsRecord->nothi_potro_page_bn = $nextPotroPagebn;
                        $nothiPotroAttachmentsRecord->attachment_type = $attachmentValue['attachment_type'];
                        $nothiPotroAttachmentsRecord->application_origin = !empty($dakRecord['application_origin'])?$dakRecord['application_origin']:'nothi';
                        $nothiPotroAttachmentsRecord->application_meta_data = !empty($dakRecord['application_meta_data'])?$dakRecord['application_meta_data']:'';
                        $nothiPotroAttachmentsRecord->potro_cover = !empty($attachmentValue['content_cover'])
                            ? $attachmentValue['content_cover'] : '';
                        $nothiPotroAttachmentsRecord->content_body = $attachmentValue['content_body'];
                        $nothiPotroAttachmentsRecord->is_summary_nothi = $attachmentValue['is_summary_nothi'];
                        $nothiPotroAttachmentsRecord->potrojari_status = $attachmentValue['is_summary_nothi']
                        == 1 ? 'SummaryNothi' : '';

                        $nothiPotroAttachmentsRecord->attachment_description = $attachmentValue['attachment_description'];
                        $nothiPotroAttachmentsRecord->file_name = $attachmentValue['file_name'];
                        $nothiPotroAttachmentsRecord->file_dir = !empty($attachmentValue['file_dir']) ? $attachmentValue['file_dir'] : FILE_FOLDER;

                        $nothiPotroAttachmentsTable->save($nothiPotroAttachmentsRecord);
                    }
                }

                $nothiDakMovesRecord->dak_movements_id = $dakLastMoveId;
                $nothiDakMovesRecord->nothi_masters_id = $nothiPotroRecord->nothi_master_id;
                $nothiDakMovesRecord->nothi_part_no = $nothiPotroRecord->nothi_part_no;
                $nothiDakMovesTable->save($nothiDakMovesRecord);

                $nothiMastersRecord = $nothiMastersTable->get($nothiDakPotroMapsRecord->nothi_masters_id);

                $seq = $this->getDakMovementSequence($dakId,
                    $dak_type);
                /* Sender to Draft User Movement */
                $dak_move = $dakMovementsTable->newEntity();
                $dak_move->dak_type = $dak_type;
                $dak_move->dak_id = $dakId;
                $dak_move->from_office_id = $dakLastMoves['to_office_id'];
                $dak_move->from_office_name = $dakLastMoves['to_office_name'];
                $dak_move->from_office_address = $dakLastMoves['to_office_address'];
                $dak_move->from_officer_id = $dakLastMoves['to_officer_id'];
                $dak_move->from_officer_name = $dakLastMoves['to_officer_name'];
                $dak_move->from_officer_designation_id = $dakLastMoves['to_officer_designation_id'];
                $dak_move->from_officer_designation_label = $dakLastMoves['to_officer_designation_label'];
                $dak_move->from_office_unit_id = $dakLastMoves['to_office_unit_id'];
                $dak_move->from_office_unit_name = $dakLastMoves['to_office_unit_name'];

                $dak_move->to_office_id = $dakLastMoves['to_office_id'];
                $dak_move->to_office_name = $dakLastMoves['to_office_name'];
                $dak_move->to_office_address = $dakLastMoves['to_office_address'];
                $dak_move->to_officer_id = $dakLastMoves['to_officer_id'];
                $dak_move->to_office_unit_id = $dakLastMoves['to_office_unit_id'];
                $dak_move->to_office_unit_name = $dakLastMoves['to_office_unit_name'];
                $dak_move->to_officer_name = $dakLastMoves['to_officer_name'];
                $dak_move->to_officer_designation_id = $dakLastMoves['to_officer_designation_id'];
                $dak_move->to_officer_designation_label = $dakLastMoves['to_officer_designation_label'];

                $dak_move->attention_type = $dakLastMoves['attention_type'];
                $dak_move->docketing_no = $dakLastMoves['docketing_no'];
                $dak_move->from_sarok_no = $dakLastMoves['from_sarok_no'];
                $dak_move->to_sarok_no = $dakLastMoves['to_sarok_no'];
                $dak_move->operation_type = ($nothiPotroRecord->nothijato != 1) ? DAK_CATEGORY_NOTHIVUKTO
                    : DAK_CATEGORY_NOTHIJATO;
                $dak_move->sequence = $seq;
                $dak_move->dak_actions = ($nothiPotroRecord->nothijato != 1 ? ("নথিতে উপস্থাপন করা হয়েছে। " . "<a href='" . $this->request->webroot . "noteDetail/" . $nothiPotroRecord->nothi_part_no . "'  >নথি নম্বর " . $nothiMastersRecord['nothi_no'] . ", বিষয়: " . $nothiMastersRecord['subject'] . "</a>")
                    : "নথিজাত করা হয়েছে। ");
                $dakMovementsTable->save($dak_move);

                $dakRecord->dak_status = ($nothiPotroRecord->nothijato != 1 ? DAK_CATEGORY_NOTHIVUKTO
                    : DAK_CATEGORY_NOTHIJATO);
                $dakDaptoriksTable->save($dakRecord);

                $dakUsersTable->updateAll(['dak_category' => ($nothiPotroRecord->nothijato
                != 1 ? DAK_CATEGORY_NOTHIVUKTO : DAK_CATEGORY_NOTHIJATO),
                    'dak_view_status' => DAK_VIEW_STATUS_VIEW],
                    ['dak_id' => $dakId, 'dak_type' => $dak_type]);

                $feedbackmeta = !empty( $dakRecord['application_meta_data'])?jsonA( $dakRecord['application_meta_data']):[];
                if(!empty($feedbackmeta['feedback_url'])){
                    $feedbackdata = [
                        'api_key'=>!empty($feedbackmeta['feedback_api_key'])?$feedbackmeta['feedback_api_key']:'',
                        'aid'=>$feedbackmeta['tracking_id'],
                        'action'=>1,
                        'decision'=> 1,
                        'dak_id' => $dakId,
                        'nothi_id' => $nothiPotroRecord->nothi_master_id,
                        'note_id' => $nothiPotroRecord->nothi_part_no,
                        'potro_id' => 0,
                        'decision_note'=> ($nothiPotroRecord->nothijato != 1 ? "নথিতে উপস্থাপন করা হয়েছে।" : "নথিজাত করা হয়েছে। "),
                        'current_desk_id'=>$dakLastMoves['to_officer_designation_id']
                    ];

                    $this->notifyFeedback($feedbackmeta['feedback_url'],$feedbackdata);
                }
                $successnothi++;

                if ($successnothi == 0) {
                    $con->rollback();
                    $jsonArray['msg'] = $jsonArray['message'] = (($requestedData['nothijato']
                        == 0) ? 'নথিতে উপস্থাপন করা সম্ভব হচ্ছে না' : 'নথিজাত করা সম্ভব হচ্ছে না');
                    goto rtn;
                } else {

                    /*if ($requestedData['nothijato'] == 1) {
                        $nothiMasterCurrentUsersTable->updateAll(['is_archive' => 1],
                            ['nothi_part_no' => $nothiPartId, 'nothi_office' => $employee_office['office_id']]);
                    }*/
                    $con->commit();

                    $current_office_section = $employee_office;
                    $current_office_section['office_id'] = $dakLastMoves['to_office_id'];
                    $current_office_section['officer_id'] = $dakLastMoves['to_officer_id'];

                    if (!$this->NotificationSet($requestedData['nothijato'] != 0
                        ? "nothijat" : 'nothivukto',
                        array($successnothi, ""), 1,
                        $current_office_section, [], $dakRecord->dak_subject)
                    ) {

                    }
                    $jsonArray = array(
                        "status" => "success",
                        "message" => ($requestedData['nothijato']
                        == 0 ? 'ডাক নথিতে উপস্থাপন করা হয়েছে' : 'ডাক নথিজাত করা হয়েছে'),
                        "msg" => ($requestedData['nothijato']
                        == 0 ? 'ডাক নথিতে উপস্থাপন করা হয়েছে' : 'ডাক নথিজাত করা হয়েছে'),
                    );
                    goto rtn;
                }
            } catch (\Exception $ex) {
                $con->rollback();
                $jsonArray['msg'] = $jsonArray['message'] = (($requestedData['nothijato']
                    == 0) ? 'নথিতে উপস্থাপন করা সম্ভব হচ্ছে না' : 'নথিজাত করা সম্ভব হচ্ছে না');
                $jsonArray['error'] = $ex->getMessage();
                goto rtn;
            }
        }
        rtn:
        $this->response->type('application/json');
        $this->response->body(json_encode($jsonArray));
        return $this->response;
    }

    public function apiPotroPage($nothiMasterId, $nothi_office = 0)
    {
        $this->layout = 'online_dak';
        $user_designation = !empty($this->request->query['user_designation']) ? $this->request->query['user_designation'] : (!empty($this->request->data['user_designation']) ? $this->request->data['user_designation'] : 0);
        $jsonArray = array(
            "status" => "error",
            "error_code" => 404,
            "message" => "Invalid Request"
        );

        $apikey = isset($this->request->query['api_key']) ? $this->request->query['api_key'] : '';

        if (empty($apikey) || $this->checkToken($apikey) == FALSE) {
            echo json_encode($jsonArray);
            die;
        }

        if (!defined("PROJAPOTI_API") || PROJAPOTI_API == false) {
            echo json_encode($jsonArray);
            die;
        }
        if (empty($user_designation)) {
            echo json_encode($jsonArray);
            die;
        }
        if (empty($nothiMasterId)) {
            echo json_encode($jsonArray);
            die;
        }
        if ($apikey != API_KEY) {
            //verify api token
            $this->loadComponent('ApiRelated');
            $getApiTokenData = $this->ApiRelated->getMobileApiTokenData($apikey);
            if (!empty($getApiTokenData)) {
                if (!in_array($user_designation, $getApiTokenData['designations'])) {
                    $jsonArray['message'] = 'Unauthorized request';
                    echo json_encode($jsonArray);
                    die;
                }
            }
            //verify api token
        }

        $this->set('apikey', $apikey);
        $this->set('user_designation', $user_designation);

        $height = !empty($this->request->query['height']) ? $this->request->query['height']: (!empty($this->request->data['height'])?$this->request->data['height']:0);
        $width = !empty($this->request->query['width']) ? $this->request->query['width']: (!empty($this->request->data['width'])?$this->request->data['width']:0);

        $this->set('height', $height);
        $this->set('width', $width);

        $employee_office = $this->setCurrentDakSection($user_designation);

        $this->set(compact('employee_office'));

        $otherNothi = false;
        if ($nothi_office != $employee_office['office_id']) {
            $otherNothi = true;
        } else {
            $nothi_office = $employee_office['office_id'];
        }

        $this->switchOffice($nothi_office, "apiOffice");

        $this->set(compact('nothi_office'));
        $this->set(compact('otherNothi'));

        $this->set('id', $nothiMasterId);
        $potroAttachmentTable = TableRegistry::get('NothiPotroAttachments');
        $potrojariTable = TableRegistry::get('Potrojari');

        $nothiPartsTable = TableRegistry::get('NothiParts');
        $nothiPartsInformation = $nothiPartsTable->get($nothiMasterId);
        $nothiMasterPermissionsTable = TableRegistry::get("NothiMasterPermissions");
        $nothiMasterCurrentUsersTable = TableRegistry::get("NothiMasterCurrentUsers");

        $nothiMasters = array();
        $nothiMastersCurrentUser = array();

        if (!empty($employee_office)) {

            $nothiMasters = $nothiMasterPermissionsTable->hasAccess($nothi_office,
                $employee_office['office_id'],
                $employee_office['office_unit_id'],
                $employee_office['office_unit_organogram_id'],
                $nothiPartsInformation['nothi_masters_id'],
                $nothiPartsInformation['id']);

            $nothiMastersCurrentUser = $nothiMasterCurrentUsersTable->find()->where(['nothi_part_no' => $nothiMasterId,
                'office_id' => $employee_office['office_id'], 'office_unit_id' => $employee_office['office_unit_id'],
                'office_unit_organogram_id' => $employee_office['office_unit_organogram_id'],
                'nothi_office' => $nothi_office])->first();
        }

        if (empty($nothiMasters)) {
            die;
        }

        if (!empty($nothiMastersCurrentUser)) {
            $this->set('privilige_type', $nothiMasters['privilige_type']);
        } else {
            $this->set('privilige_type', 0);
        }

        $total = $potroAttachmentTable->find()->where(['nothi_part_no' => $nothiMasterId,
            '(is_summary_nothi IS NULL OR is_summary_nothi = 0)', 'nothijato' => 0, 'status' => 1])->count();

        $potroAttachmentRecord = $potroAttachmentTable->find()->where(['nothi_part_no' => $nothiMasterId,
            '(is_summary_nothi IS NULL OR is_summary_nothi = 0)', 'nothijato' => 0, 'status' => 1])->order(['id desc'])->page(1,
            5)->toArray();

        $this->set(compact('potroAttachmentRecord'));
        $this->set(compact('total'));

        $draftPotro = $potrojariTable->find()->where(['potro_status' => 'Draft',
            'nothi_part_no' => $nothiMasterId])->order(['id desc'])->toArray();

        $potrojariAttachmentsTable = TableRegistry::get('PotrojariAttachments');
        $potrojariAttachments = $potrojariAttachmentsTable->find()->select(['attachment_type',
            'file_name', 'potrojari_id', 'id', 'user_file_name'])->join(['Potrojari' => ['table' => 'potrojari',
            'type' => 'inner', 'conditions' => 'Potrojari.id = PotrojariAttachments.potrojari_id']])->where(['Potrojari.potro_status' => 'Draft',
            'Potrojari.nothi_part_no' => $nothiMasterId, 'PotrojariAttachments.attachment_type <> ' => 'text'])->order(['Potrojari.id desc'])->toArray();

        $draftPotroAttachments = array();
        if (!empty($potrojariAttachments)) {
            foreach ($potrojariAttachments as $ke => $val) {
                $draftPotroAttachments[$val['potrojari_id']][] = $val;
            }
        }

        $this->set(compact('draftPotro', 'draftPotroAttachments'));

        $draftSummary = $potrojariTable->find()->where(['potro_status' => 'SummaryDraft',
            'nothi_part_no' => $nothiMasterId, 'is_summary_nothi' => 1])->first();
        $is_approve = 0;
        $summaryNothiTable = TableRegistry::get('SummaryNothiUsers');
        if (!empty($draftSummary)) {
            $totalapp = 0;
            $getSummaryUserInfo = $summaryNothiTable->find()->where(['potrojari_id' => $draftSummary['id'], 'current_office_id' => $employee_office['office_id']])->toArray();
            if (!empty($getSummaryUserInfo)) {
                foreach ($getSummaryUserInfo as $k => $v) {
                    if ($v['is_approve']) {
                        $totalapp++;
                    }
                }
            }

            if ($totalapp > 0 && $totalapp == count($getSummaryUserInfo)) {
                $is_approve = 1;
            }
        }
        $this->set('is_approve', $is_approve);
        $summaryAttachmentRecord = $potroAttachmentTable->find()->where(['nothi_part_no' => $nothiMasterId, 'is_summary_nothi' => 1])->order(['id desc'])->toArray();
        $this->set(compact('draftSummary'));
        $this->set(compact('summaryAttachmentRecord'));


        //get summary nothi relation

        $potrosummarynothi = $summaryNothiTable->getData(
            [
                'current_office_id' => $employee_office['office_id'],
                'nothi_part_no' => $nothiMasterId
            ])->toArray();

        $potrosummarystatus = [];
        $potrosummarydraftstatus = [];
        $summaryApprove = false;
        if (!empty($potrosummarynothi)) {
            foreach ($potrosummarynothi as $key => $pair) {

                $potrosummarystatus[$pair['potro_id']][] = [
                    'is_approve' => $pair['is_approve'],
                    'designation_id' => $pair['designation_id'],
                    'is_sent' => $pair['is_sent'],
                    'can_approve' => $pair['can_approve'],
                ];
                if ($pair['is_approve']) {
                    $summaryApprove = true;
                }
                if ($employee_office['office_unit_organogram_id'] == $pair['designation_id'] && $pair['is_sent'] == 0 && $pair['can_approve'] == 1) {
                    $potrosummarydraftstatus[$pair['potrojari_id']] = [
                        'is_approve' => $pair['is_approve'],
                        'designation_id' => $pair['designation_id'],
                        'is_sent' => $pair['is_sent'],
                        'can_approve' => $pair['can_approve'],
                    ];
                }
            }
        }

        $api_nothi_part_flag_table = TableRegistry::get('ApiNothiPartFlag');
        $api_nothi_part_flag_table->deleteAll(['part_id'=>$nothiMasterId,'nothi_office_id' => $nothi_office,'office_unit_organogram_id'=>$employee_office['office_unit_organogram_id']]);

        $this->set('summaryApprove', $summaryApprove);
        $this->set('potrosummarystatus', $potrosummarystatus);
        $this->set('potrosummarydraftstatus', $potrosummarydraftstatus);
    }

    public function postApiPotroPage($nothiMasterId, $nothi_office = 0)
    {
        $this->layout = 'online_dak';
        $user_designation = !empty($this->request->query['user_designation']) ? $this->request->query['user_designation'] : (!empty($this->request->data['user_designation']) ? $this->request->data['user_designation'] : 0);
        $jsonArray = array(
            "status" => "error",
            "error_code" => 404,
            "message" => "Invalid Request"
        );

        $apikey = isset($this->request->data['api_key']) ? $this->request->data['api_key'] : '';
        if (empty($apikey) || $this->checkToken($apikey) == FALSE
        ) {
            echo json_encode($jsonArray);
            die;
        }

        if (!defined("PROJAPOTI_API") || PROJAPOTI_API == false) {
            echo json_encode($jsonArray);
            die;
        }
        if (empty($user_designation)) {
            echo json_encode($jsonArray);
            die;
        }
        if (empty($nothiMasterId)) {
            echo json_encode($jsonArray);
            die;
        }
        if ($apikey != API_KEY) {
            //verify api token
            $this->loadComponent('ApiRelated');
            $getApiTokenData = $this->ApiRelated->getMobileApiTokenData($apikey);
            if (!empty($getApiTokenData)) {
                if (!in_array($user_designation, $getApiTokenData['designations'])) {
                    $jsonArray['message'] = 'Unauthorized request';
                    echo json_encode($jsonArray);
                    die;
                }
            }
            //verify api token
        }
        $this->set('apikey', $apikey);
        $this->set('user_designation', $user_designation);

        $employee_office = $this->setCurrentDakSection($user_designation);

        $this->set(compact('employee_office'));
        $this->switchOffice($nothi_office, "apiOffice");

        $otherNothi = $nothi_office != $employee_office['office_id'] ? true : false;
        $this->set(compact('nothi_office'));
        $this->set(compact('otherNothi'));

        $this->set('id', $nothiMasterId);
        $potroAttachmentTable = TableRegistry::get('NothiPotroAttachments');
        $potrojariTable = TableRegistry::get('Potrojari');

        $nothiPartsTable = TableRegistry::get('NothiParts');
        $nothiPartsInformation = $nothiPartsTable->get($nothiMasterId);
        $nothiMasterPermissionsTable = TableRegistry::get("NothiMasterPermissions");
        $nothiMasterCurrentUsersTable = TableRegistry::get("NothiMasterCurrentUsers");

        $nothiMasters = array();
        $nothiMastersCurrentUser = array();

        if (!empty($employee_office)) {

            $nothiMasters = $nothiMasterPermissionsTable->hasAccess($nothi_office,
                $employee_office['office_id'],
                $employee_office['office_unit_id'],
                $employee_office['office_unit_organogram_id'],
                $nothiPartsInformation['nothi_masters_id'],
                $nothiPartsInformation['id']);

            $nothiMastersCurrentUser = $nothiMasterCurrentUsersTable->find()->where(['nothi_part_no' => $nothiMasterId,
                'office_id' => $employee_office['office_id'], 'office_unit_id' => $employee_office['office_unit_id'],
                'office_unit_organogram_id' => $employee_office['office_unit_organogram_id'],
                'nothi_office' => $nothi_office])->first();
        }

        if (empty($nothiMasters)) {
            die;
        }

        if (!empty($nothiMastersCurrentUser)) {
            $this->set('privilige_type', $nothiMasters['privilige_type']);
        } else {
            $this->set('privilige_type', 0);
        }

        $total = $potroAttachmentTable->find()->where(['nothi_part_no' => $nothiMasterId,
            '(is_summary_nothi IS NULL OR is_summary_nothi = 0)', 'nothijato' => 0, 'status' => 1])->count();

        $potroAttachmentRecord = $potroAttachmentTable->find()->where(['nothi_part_no' => $nothiMasterId,
            '(is_summary_nothi IS NULL OR is_summary_nothi = 0)', 'nothijato' => 0, 'status' => 1])->order(['id desc'])->page(1,
            5)->toArray();

        $api_nothi_part_flag_table = TableRegistry::get('ApiNothiPartFlag');
        $api_nothi_part_flag_table->deleteAll(['part_id'=>$nothiMasterId,'nothi_office_id' => $nothi_office,'office_unit_organogram_id'=>$employee_office['office_unit_organogram_id']]);

        $this->set(compact('potroAttachmentRecord'));
        $this->set(compact('total'));

        $draftPotro = $potrojariTable->find()->where(['potro_status' => 'Draft',
            'nothi_part_no' => $nothiMasterId])->order(['id desc'])->toArray();

        $potrojariAttachmentsTable = TableRegistry::get('PotrojariAttachments');
        $potrojariAttachments = $potrojariAttachmentsTable->find()->select(['attachment_type',
            'file_name', 'potrojari_id', 'id', 'user_file_name'])->join(['Potrojari' => ['table' => 'potrojari',
            'type' => 'inner', 'conditions' => 'Potrojari.id = PotrojariAttachments.potrojari_id']])->where(['Potrojari.potro_status' => 'Draft',
            'Potrojari.nothi_part_no' => $nothiMasterId])->order(['Potrojari.id desc'])->toArray();

        $draftPotroAttachments = array();
        if (!empty($potrojariAttachments)) {
            foreach ($potrojariAttachments as $ke => $val) {
                $draftPotroAttachments[$val['potrojari_id']][] = $val;
            }
        }

        $this->set(compact('draftPotro', 'draftPotroAttachments'));
    }

    public function apiNextPotroPage($nothiMasterId, $page = 1, $potroId = 0)
    {
        $this->layout = null;
        $jsonArray = array(
            "status" => "error",
            "error_code" => 404,
            "message" => "Invalid Request"
        );


        if ($this->request->is('ajax')) {
            $nothi_office = intval($this->request->data['nothi_office']);
            $user_designation = $this->request->data['user_designation'];


            if (!defined("PROJAPOTI_API") || PROJAPOTI_API == false) {
                echo json_encode($jsonArray);
                die;
            }
            if (empty($user_designation)) {
                echo json_encode($jsonArray);
                die;
            }
            if (empty($nothiMasterId)) {
                echo json_encode($jsonArray);
                die;
            }

            $EmployeeOfficesTable = TableRegistry::get('EmployeeOffices');
            $employee_office = $EmployeeOfficesTable->find()->where(['office_unit_organogram_id' => $user_designation,
                'status' => 1])->first();

            $otherNothi = false;
            if ($nothi_office != $employee_office['office_id']) {
                $otherNothi = true;
            } else {
                $nothi_office = $employee_office['office_id'];
            }
            $this->switchOffice($nothi_office, 'MainNothiOffice');

            $this->set('nothi_office', $nothi_office);
            $this->set(compact('otherNothi'));

            $nothiMasterCurrentUsersTable = TableRegistry::get("NothiMasterCurrentUsers");

            $nothiMastersCurrentUser = $nothiMasterCurrentUsersTable->find()->where(['nothi_part_no' => $nothiMasterId,
                'office_id' => $employee_office['office_id'], 'office_unit_id' => $employee_office['office_unit_id'],
                'office_unit_organogram_id' => $employee_office['office_unit_organogram_id'],
                'nothi_office' => $nothi_office])->first();

            $privilige_type = 0;
            if (!empty($nothiMastersCurrentUser)) {
                $privilige_type = 1;
            }
            $this->set('id', $nothiMasterId);
            $potroAttachmentTable = TableRegistry::get('NothiPotroAttachments');

            $conditions = '';
            if (!empty($potroId)) {
                $conditions = '(nothi_potro_page="' . $potroId . '" OR nothi_potro_page_bn = "' . $potroId . '")';
            }

            $potroAttachmentRecord = $potroAttachmentTable->find()->where(['nothi_part_no' => $nothiMasterId,
                'status' => 1])->where([$conditions])->order(['id DESC '])->page($page,
                5)->toArray();

            $html = '';
            $potroNo = '';
            if (!empty($potroAttachmentRecord)) {
                $i = 0;
                foreach ($potroAttachmentRecord as $row) {
                    if ($i == 0) {
                        $potroNo = $row['nothi_potro_page_bn'];
                    }
                    $html .= '<div id_en="' . $row['nothi_potro_page'] . '" id_bn="' . $row['nothi_potro_page_bn'] . '" class="attachmentsRecord ' . ($i
                        == 0 ? 'active ' : ($i == (count($potroAttachmentRecord)
                            - 1) ? 'last' : '')) . '">';

                    $html .= '<div  style="background-color: #a94442; padding: 5px; color: #fff;"> স্মারক নম্বর: &nbsp;&nbsp; ' . $row['sarok_no'] . '</div>';


                    if ($row['attachment_type'] == 'text') {
                        $header = jsonA($row['meta_data']);
                        $html .= (!empty($header['potro_header_banner']) ? ('<div style="text-align: ' . ($header['banner_position']) . '!important;"><img src="' . $this->request->webroot . 'getContent?file=' . base64_decode($header['potro_header_banner']) . '&token=' . sGenerateToken(['file' => base64_decode($header['potro_header_banner'])], ['exp' => time() + 60 * 300]) . '" style="width:' . ($header['banner_width']) . ';height: 60px;max-width: 100%;"/></div>') : '') . $row['content_body'];
                    } else if (substr($row['attachment_type'], 0,
                            strlen('application/vnd')) == 'application/vnd' || substr($row['attachment_type'],
                            0, strlen('application/ms')) == 'application/ms' || $row['attachment_type']
                        == 'application/pdf'
                    ) {
                        $html .= '<div class="text-center"><iframe style=" width:100%; height: 700px;" src="https://docs.google.com/gview?url=' . FILE_FOLDER . $row['file_name'] . '&embedded=true" style="width:600px; height:500px;" frameborder="0"></iframe></div>';
                    } else {
                        $html .= '<div class="text-center"><a href="' . $this->request->webroot . 'content/' . $row['file_name'] . '?token=' . $this->generateToken(['file' => $row['file_name']], ['exp' => time() + 60 * 300]) . '" onclick="return false;" data-title="পত্র নম্বর:  ' . $row['nothi_potro_page_bn'] . '" data-footer="" ><img class="zoomimg img-responsive"  src="' . $this->request->webroot . 'content/' . $row['file_name'] . '?token=' . $this->generateToken(['file' => $row['file_name']], ['exp' => time() + 60 * 300]) . '" alt="ছবি পাওয়া যায়নি"></a></div>';
                        '<img src="' . $this->request->webroot . 'content/' . $row['file_name'] . '?token=' . $this->generateToken(['file' => $row['file_name']], ['exp' => time() + 60 * 300]) . '" alt="ছবি পাওয়া যায়নি">';
                    }
                    $html .= "</div>";
                    $i++;
                }
            }

            echo json_encode(array('html' => $html, 'potrono' => $potroNo));
            die;
        }
    }

    /* API of this Controller */
    public function getNothiPartListByMasterID()
    {
        $response = ['status' => 'error', 'msg' => 'Something went wrong'];
        $master_id = $this->request->data['master_id'];
        $nothi_office = $this->request->data['nothi_office'];
        if (empty($master_id)) {
            $response['msg'] = 'Master ID missing';
            goto rtn;
        }
        if (empty($nothi_office)) {
            $response['msg'] = 'Nothi Office missing';
            goto rtn;
        }

        $session = $this->getCurrentDakSection();
        $nothiCurrentUserTable = TableRegistry::get('NothiMasterCurrentUsers');
        $allNothiList = $nothiCurrentUserTable->getAllPartByDesignation($master_id, $session['office_unit_organogram_id'], $session['office_unit_id'], $nothi_office);
        $data = [];
        if (!empty($allNothiList)) {
            $nothiParts = [];
            foreach ($allNothiList as $anl_k => $anl_v) {
                $nothiParts[] = $anl_k;
            }
            $data['name'] = TableRegistry::get('NothiMasters')->getName($master_id)->first()['subject'];
            $data['part_list'] = TableRegistry::get('NothiParts')->getNothiPartNoBnByID($nothiParts);
        }
        $response = ['status' => 'success', 'data' => $data];
        rtn:
        $this->response->body(json_encode($response));
        $this->response->type('application/json');
        return $this->response;
    }

    public function showAllPermittedNothiMasterList()
    {
        $this->layout = 'ajax';
        $nothi_office = isset($this->request->data['nothi_office']) ? $this->request->data['nothi_office'] : 0;
        $master_id = isset($this->request->data['master_id']) ? $this->request->data['master_id'] : 0;
        $potrojari_id = isset($this->request->data['potrojari_id']) ? $this->request->data['potrojari_id'] : 0;
        $this->set(compact('nothi_office', 'master_id', 'potrojari_id'));
    }

    public function PortalGuardFiles()
    {
        if ($this->request->is('ajax', 'post')) {

            $data = [
                0 => ['type' => 'জনপ্রশাসন মন্ত্রণালয়', 'name' => '২০১৬-১৭ অর্থবছরের বাজেটে জনপ্রশাসন মন্ত্রণালয়ের অধীন উপজেলা প্রশাসনের বিভিন্ন উপখাতের বাজেট বরাদ্দ বণ্টন।', 'link' => 'http://bangladesh.gov.bd/sites/default/files/files/bangladesh.gov.bd/gurd_files/51406b2b_c4b9_43aa_88f2_c667c4ac6007/3.pdf', 'date' => '12/09/2017'],
                1 => ['type' => 'জনপ্রশাসন মন্ত্রণালয়', 'name' => '২০১৬-১৭ অর্থবছরের বাজেটে জনপ্রশাসন মন্ত্রণালয়ের অধীন জেলা প্রশাসন এবং সার্কিট হাউজ এর বিভিন্ন উপখাতের বাজেট বরাদ্দ বণ্টন।', 'link' => 'http://bangladesh.gov.bd/sites/default/files/files/bangladesh.gov.bd/gurd_files/ae923d46_cde3_451a_a21f_98f68dc4c368/2.pdf', 'date' => '12/09/2017'],
                2 => ['type' => 'জনপ্রশাসন মন্ত্রণালয়', 'name' => '২০১৬-১৭ অর্থবছরের বাজেটে জনপ্রশাসন মন্ত্রণালয়ের অধীন বিভাগীয় প্রশাসনের বিভিন্ন উপখাতের বাজেট বরাদ্দ বণ্টন।', 'link' => 'http://bangladesh.gov.bd/sites/default/files/files/bangladesh.gov.bd/gurd_files/c5901928_9d57_44fe_b2b4_1b9ed314d275/1.pdf', 'date' => '12/09/2017'],
                3 => ['type' => 'জনপ্রশাসন মন্ত্রণালয়', 'name' => 'বিশেষ প্রাধিকার কোটায় নার্স পদে সরাসরি নিয়োগের ক্ষেত্রে কোটা।', 'link' => 'http://bangladesh.gov.bd/sites/default/files/files/bangladesh.gov.bd/gurd_files/65940b4d_74ef_4f16_85ab_f91ef5a20a26/reg1-2016-193.pdf', 'date' => '12/09/2017'],
                4 => ['type' => 'জনপ্রশাসন মন্ত্রণালয়', 'name' => 'সরকার বেসামরিক প্রশাসনে চাকরিরত অবস্থায় কোন সরকারী কর্মচারি মৃত্যুবরণ এবং গুরুতর আহত হওয়ার কারণে আর্থিক সহায়তার পরিমান পুনঃনির্ধারণ।', 'link' => 'http://bangladesh.gov.bd/sites/default/files/files/bangladesh.gov.bd/gurd_files/65940b4d_74ef_4f16_85ab_f91ef5a20a26/reg1-2016-193.pdf', 'date' => '12/09/2017'],
                5 => ['type' => 'জনপ্রশাসন মন্ত্রণালয়', 'name' => 'উপজেলা নির্বাহী অফিসারের কার্যালয়ে ২০১৫-১৬ অর্থবছরে বাংলা নববর্ষ ভাতা খাতে বরাদ্দ বণ্টন।', 'link' => 'http://bangladesh.gov.bd/sites/default/files/files/bangladesh.gov.bd/gurd_files/c864cc53_f6da_4afb_bf64_6217711c4f18/5.pdf', 'date' => '12/09/2017']
            ];

            $response = ['status' => 'success', 'data' => $data];
            $this->response->body(json_encode($response));
            $this->response->type('application/json');
            return $this->response;
        }
    }

    public function postApiGetPermittedNothiMasterList()
    {
		$condition = [];
        $response = ['status' => 'error', 'msg' => 'Something went wrong'];
        $nothi_office = isset($this->request->data['office_id']) ? $this->request->data['office_id'] : 0;
        $designation_id = isset($this->request->data['designation_id']) ? $this->request->data['designation_id'] : 0;
        $office_id = isset($this->request->data['office_id']) ? $this->request->data['office_id'] : 0;
        $office_unit_id = isset($this->request->data['office_unit_id']) ? $this->request->data['office_unit_id'] : 0;
        $apikey = isset($this->request->query['api_key']) ? $this->request->query['api_key'] : (isset($this->request->data['api_key']) ? $this->request->data['api_key'] : '');

        if (empty($designation_id)) {
            $response['msg'] = 'designation id missing';
            goto rtn;
        }
        if (empty($office_id)) {
            $response['msg'] = 'office id missing';
            goto rtn;
        }
        if (empty($office_unit_id)) {
            $response['msg'] = 'office unit id missing';
            goto rtn;
        }
        if (empty($nothi_office)) {
            $response['msg'] = 'nothi office missing';
            goto rtn;
        }
        if (empty($apikey) || $this->checkToken($apikey) == FALSE) {
            $response['msg'] = 'Unauthorized request';
            goto rtn;
        }
        if ($apikey != API_KEY) {
            //verify api token
            $this->loadComponent('ApiRelated');
            $getApiTokenData = $this->ApiRelated->getMobileApiTokenData($apikey);
            if (!empty($getApiTokenData)) {
                if (!in_array($designation_id, $getApiTokenData['designations'])) {
                    $response['msg'] = 'Unauthorized request';
                    goto rtn;
                }
            }
            //verify api token
        }
        try {
            $this->switchOffice($office_id, 'OfficeDB');
            TableRegistry::remove('NothiMasterPermissions');
            $nothiPriviligeTable = TableRegistry::get('NothiMasterPermissions');

            $nothiMastersTable = TableRegistry::get('NothiMasters');
            $nothiMastersPermitted = $nothiPriviligeTable->getMasterNothiList($nothi_office, $office_id, $office_unit_id, $designation_id);

            $nothiMastersId = [];
            if (!empty($nothiMastersPermitted)) {
                foreach ($nothiMastersPermitted as $nothiKey => $nothiId) {
                    $nothiMastersId[] = $nothiId['nothi_masters_id'];
                }
                array_push($condition,['NothiMasters.id IN'=>$nothiMastersId]);
                array_push($condition,['(NothiMasters.is_archived is NULL OR NothiMasters.is_archived = 0)']);
            }
//        }else{
//            $condition = ' NothiMasters.id =' . $id;
//        }


            $nothiPermissionListArray = $nothiMastersTable->getAll($condition, 0, 0, "NothiMasters.id DESC")->toArray();
            $response = ['status' => 'success', 'data' => $nothiPermissionListArray];
        } catch (\Exception $ex) {
            $response = ['status' => 'success', 'msg' => 'টেকনিক্যাল ত্রুটি হয়েছে', 'reason' => $this->makeEncryptedData($ex->getMessage())];
        }
//        if(empty($id)) {

        rtn:
        $this->response->type('application/json');
        $this->response->body(json_encode($response));
        return $this->response;
    }

    public function postApiNothiPartPermissionEditList($id = 0, $nothi_office = 0)
    {
        /*
     *  used for permission edit list generation.
     * 1. NothiOwnUnitPermissionList::updatePermission
     * 2. NothiOtherOfficePermissionList::display
     */

        $this->layout = 'online_dak';
        $designation_id = isset($this->request->query['designation_id']) ? $this->request->query['designation_id'] : (isset($this->request->data['designation_id']) ? $this->request->data['designation_id'] : 0);
        $response = ['status' => 'error', 'msg' => 'Invalid request'];
        $apikey = isset($this->request->query['api_key']) ? $this->request->query['api_key'] : (isset($this->request->data['api_key']) ? $this->request->data['api_key'] : '');
        if (empty($apikey) || $this->checkToken($apikey) == FALSE) {
            goto rtn;
        }
        if ($apikey != API_KEY) {
            //verify api token
            $this->loadComponent('ApiRelated');
            $getApiTokenData = $this->ApiRelated->getMobileApiTokenData($apikey);
            if (!empty($getApiTokenData)) {
                if (!in_array($designation_id, $getApiTokenData['designations'])) {
                    $response['msg'] = 'Unauthorized request';
                    goto rtn;
                }
            }
            //verify api token
        }

        if (!defined("PROJAPOTI_API") || PROJAPOTI_API == false) {
            goto rtn;
        }
        if (empty($designation_id)) {
            $response['msg'] = 'designation id missing';
            goto rtn;
        }
        if (empty($id)) {
            $response['msg'] = 'Nothi part no missing';
            goto rtn;
        }
        if (empty($nothi_office)) {
            $response['msg'] = 'nothi office missing';
            goto rtn;
        }
        $this->switchOffice($nothi_office,'OfficeDB');

        $api_nothi_part_flag_table = TableRegistry::get('ApiNothiPartFlag');
        $api_nothi_part_flag_table->deleteAll(['part_id'=>$id,'nothi_office_id' => $nothi_office,'office_unit_organogram_id'=>$designation_id]);

        $this->set('id', $id);
        $this->set('nothi_office', $nothi_office);
        $this->set('employee_office_info', $this->setCurrentDakSection($designation_id));
        $this->set('api', true);
        $response = ['status' => 'success', 'msg' => 'ok'];
//        $this->view= 'nothi_master_permission_edit_list';

        rtn:
        if ($response['status'] == 'error') {
            $this->response->type('json');
            $this->response->body(json_encode($response));
            return $this->response;
        }
    }

    public function apiGetSummaryPartList($type = 1)
    {
        $user_designation = $this->request->data['user_designation'];
        $jsonArray = array(
            "status" => "error",
            "error_code" => 404,
            "message" => "Invalid Request"
        );
        if (!defined("PROJAPOTI_API") || PROJAPOTI_API == false) {
            echo json_encode($jsonArray);
            die;
        }

        $apikey = isset($this->request->data['api_key']) ? $this->request->data['api_key'] : '';
        if (empty($apikey) || $this->checkToken($apikey) == FALSE
        ) {
            echo json_encode($jsonArray);
            die;
        }

        if (empty($user_designation)) {
            echo json_encode($jsonArray);
            die;
        }

        $start = isset($this->request->data['start']) ? intval($this->request->data['start']) : 0;
        $len = isset($this->request->data['length']) ? intval($this->request->data['length']) : 10;
        $page = !empty($this->request->data['page']) ? intval($this->request->data['page']) : (($start / $len) + 1);

        $employee_office = $this->setCurrentDakSection($user_designation);

        $this->switchOffice($employee_office['office_id'], 'CurrentOffice');

        $nothiPotrosTable = TableRegistry::get('NothiPotros');

        $potros = $nothiPotrosTable->find()->page($page, $len);

        if ($type == 1) {
            $potros = $potros
                ->select([
                    'nothi_potro_id' => 'NothiPotros.id',
                    'NothiPotros.nothi_master_id',
                    'NothiPotros.nothi_part_no',
                    "is_approved" => 'NothiPotroAttachments.is_approved',
                    'NothiPotros.sarok_no',
                    'subject',
                    'to_office_id' => 'NothiMasterCurrentUsers.nothi_office',
                    'nothi_type' => 'NothiTypes.type_name',
                    "nothi_no" => 'NothiParts.nothi_no',
                    "view_status" => 'NothiMasterCurrentUsers.view_status',
                    "issue_date" => 'NothiMasterCurrentUsers.issue_date'
                ])->join([
                    'NothiParts' => [
                        'table' => 'nothi_parts',
                        'type' => 'INNER',
                        'conditions' => 'NothiParts.id = NothiPotros.nothi_part_no AND NothiParts.nothi_masters_id = NothiPotros.nothi_master_id'
                    ],
                    'NothiTypes' => [
                        'table' => 'nothi_types',
                        'type' => 'INNER',
                        'conditions' => 'NothiParts.nothi_types_id = NothiTypes.id'
                    ],
                    'NothiMasterCurrentUsers' => [
                        'table' => 'nothi_master_current_users',
                        'type' => 'INNER',
                        'conditions' => 'NothiParts.id = NothiMasterCurrentUsers.nothi_part_no AND 
                        NothiParts.nothi_masters_id = NothiMasterCurrentUsers.nothi_master_id AND
                        NothiMasterCurrentUsers.nothi_office = NothiParts.office_id AND NothiMasterCurrentUsers.is_archive = 0'
                    ],
                    'NothiPotroAttachments' => [
                        'table' => 'nothi_potro_attachments',
                        'type' => 'INNER',
                        'conditions' => 'NothiPotros.id = NothiPotroAttachments.nothi_potro_id'
                    ]
                ])->where([
                    'NothiPotroAttachments.is_summary_nothi' => 1,
                    'NothiPotros.nothijato' => 0,
                    'NothiPotroAttachments.is_approved' => 0,
                    'NothiPotroAttachments.potrojari_status' => 'SummaryNothi',
                    'NothiPotroAttachments.attachment_type' => 'text',
                    'NothiMasterCurrentUsers.office_unit_organogram_id' => $employee_office['office_unit_organogram_id'],
                    'NothiMasterCurrentUsers.nothi_office' => $employee_office['office_id'],
                ])->order(['NothiMasterCurrentUsers.issue_date desc'])->toArray();
        } else {
            $officeUnitOrgranogramTable = TableRegistry::get('OfficeUnitOrganograms');
            $potros = $potros->select([
                'nothi_potro_id' => 'NothiPotros.id',
                'NothiPotros.nothi_master_id',
                'NothiPotros.nothi_part_no',
                "is_approved" => 'NothiPotroAttachments.is_approved',
                'NothiPotros.sarok_no',
                'subject',
                'to_office_id' => 'NothiMasterCurrentUsers.nothi_office',
                'nothi_type' => 'NothiTypes.type_name',
                "nothi_no" => 'NothiParts.nothi_no',
                "view_status" => 'NothiMasterCurrentUsers.view_status',
                "issue_date" => 'NothiMasterCurrentUsers.issue_date',
                "office_unit_organogram_id" => 'NothiMasterCurrentUsers.office_unit_organogram_id'
            ])->join([
                'NothiParts' => [
                    'table' => 'nothi_parts',
                    'type' => 'INNER',
                    'conditions' => 'NothiParts.id = NothiPotros.nothi_part_no AND NothiParts.nothi_masters_id = NothiPotros.nothi_master_id'
                ],
                'NothiTypes' => [
                    'table' => 'nothi_types',
                    'type' => 'INNER',
                    'conditions' => 'NothiParts.nothi_types_id = NothiTypes.id'
                ],
                'NothiMasterPermissions' => [
                    'table' => 'nothi_master_permissions',
                    'type' => 'INNER',
                    'conditions' => 'NothiParts.id = NothiMasterPermissions.nothi_part_no AND 
                     NothiMasterPermissions.nothi_office = NothiParts.office_id'
                ],
                'NothiMasterCurrentUsers' => [
                    'table' => 'nothi_master_current_users',
                    'type' => 'INNER',
                    'conditions' => 'NothiParts.nothi_masters_id = NothiMasterCurrentUsers.nothi_master_id and NothiParts.id = NothiMasterCurrentUsers.nothi_part_no AND 
                    NothiMasterCurrentUsers.nothi_office = NothiParts.office_id'
                ],
                'NothiPotroAttachments' => [
                    'table' => 'nothi_potro_attachments',
                    'type' => 'INNER',
                    'conditions' => 'NothiPotros.id = NothiPotroAttachments.nothi_potro_id'
                ]
            ])->where([
                'NothiPotroAttachments.is_summary_nothi' => 1,
                'NothiPotros.nothijato' => 0,
                'NothiMasterPermissions.office_unit_organograms_id' => $employee_office['office_unit_organogram_id'],
                'NothiMasterPermissions.nothi_office' => $employee_office['office_id'],
                'NothiMasterPermissions.visited' => 1,
                'NothiPotroAttachments.potrojari_status' => 'SummaryNothi',
                'NothiPotroAttachments.attachment_type' => 'text',
                'NothiMasterCurrentUsers.nothi_office' => $employee_office['office_id'],
            ])->order(['NothiMasterCurrentUsers.issue_date desc'])->toArray();

            if (!empty($potros)) {
                foreach ($potros as $key => &$value) {
                    $designation = $officeUnitOrgranogramTable->getDesignationUnitName($value['office_unit_organogram_id']);
                    $value['office_unit_organogram_id'] = $designation['designation_bng'] . ', ' . $designation['unit_name_bng'];
                }
            }
        }

        $this->response->type('json');
        $this->response->body(json_encode(['status' => 'success', 'data' => $potros]));
        return $this->response;
    }

    public function apiGetPartList($type = 'inbox', $organogram_id = false)
    {
        $partNo = intval($this->request->query('selected_part'));
//        $search_text = !empty($this->request->query('search'))?  $this->request->query('search') :'';
        $employee_office = $this->getCurrentDakSection();
		if ($organogram_id) {
			$OtherOrganogramActivitiesSettingsTable = TableRegistry::get('OtherOrganogramActivitiesSettings');
			$OtherOrganogramActivitiesSettings = $OtherOrganogramActivitiesSettingsTable->find()->where(['organogram_id' => $organogram_id, 'assigned_organogram_id' => $employee_office['office_unit_organogram_id'], 'status' => 1, 'permission_for IN' => [0,2]])->count();
			if ($OtherOrganogramActivitiesSettings > 0) {
				$employeeOfficeTable = TableRegistry::get('EmployeeOffices');
				$emp_office = designationInfo($organogram_id);
				$employee_office = $employeeOfficeTable->find()->where(['status' => 1, 'office_unit_organogram_id' => $organogram_id])->first();
				$employee_office = array_merge($employee_office->toArray(), $emp_office);
			}
		}
        $nothiNotesTable = TableRegistry::get('NothiNotes');
        $nothiList = [];
        $you_are_permitted_for_first_note = false;
        if ($type == 'inbox') {
            $nothiCurrentTable = TableRegistry::get('NothiMasterCurrentUsers');

            $query = $nothiCurrentTable->find()->select([
                "nothi_master_id" => 'NothiMasterCurrentUsers.nothi_master_id',
                'NothiMasterCurrentUsers.nothi_part_no',
                'NothiMasterCurrentUsers.nothi_office',
                'NothiMasterCurrentUsers.issue_date',
                'NothiMasterCurrentUsers.priority',
                'NothiMasterCurrentUsers.view_status',
                'NothiMasterCurrentUsers.is_new',
                'NothiParts.id',
                'NothiParts.nothi_no',
                'NothiParts.nothi_part_no_bn',
            ])->join([
                'NothiParts' => [
                    'type' => 'INNER',
                    'table' => 'nothi_parts',
                    'conditions' => 'NothiMasterCurrentUsers.nothi_part_no = NothiParts.id'
                ]
            ])->where([
                'NothiMasterCurrentUsers.nothi_master_id' => $partNo,
                'NothiMasterCurrentUsers.is_archive' => 0,
                'NothiMasterCurrentUsers.is_finished' => 0,
                'NothiMasterCurrentUsers.is_new' => 0,
                'NothiMasterCurrentUsers.nothi_office' => $employee_office['office_id'],
                'NothiMasterCurrentUsers.office_unit_organogram_id' => $employee_office['office_unit_organogram_id'],
            ])->order(['NothiParts.nothi_part_no' => 'ASC']);

            $nothiList = $this->Paginator->paginate($query, ['limit' => 20]);


            if (!empty($nothiList)) {
                foreach ($nothiList->toArray() as $key => &$value) {
                    $value['details'] = true;
                    $nothinotes = $nothiNotesTable->find()->select(['subject'])->where(['nothi_part_no' => $value['NothiParts']['id'], 'note_no >=' => 0])->first();
                    $value['subject'] = !empty($nothinotes) ? trim(strip_tags($nothinotes['subject'])) == '' ? '<small style="font-style: italic;color: gray;">বিষয় দেয়া হয়নি</small>' : h(strip_tags($nothinotes['subject'])) : 'অনুচ্ছেদ দেওয়া হয়নি';
                    $value['class'] = ($value['priority'] ? 'nothiImportant' : ($value['view_status'] == 0 ? 'new' : ($value['is_summary'] ? 'summaryList' : '')));
                    $issue_date = new Time($value['issue_date']);
                    $value['issue_date'] = $issue_date->i18nFormat("dd-MM-YY HH:mm:ss");
                    $value['current_desk'] = $employee_office['designation_label'] .
                        (!empty($employee_office['incharge_label']) ? " ({$employee_office['incharge_label']})" : '')
                        . ($employee_office['show_unit'] ? (", " . $employee_office['office_unit_name']) : '');
                }
            }
        } else if ($type == 'all') {
            $nothiMasterPermittedUserTable = TableRegistry::get('NothiMasterPermissions');
            $nothiCurrentTable = TableRegistry::get('NothiMasterCurrentUsers');
            $employeeOfficeTable = TableRegistry::get('EmployeeOffices');
            $officeUnitsTable = TableRegistry::get('OfficeUnits');
            $officesTable = TableRegistry::get('Offices');
            $officeUnits = $officeUnitsTable->getOfficeUnitsList($employee_office['office_id']);

            $query = $nothiMasterPermittedUserTable->find()->select([
                "nothi_master_id" => 'NothiMasterPermissions.nothi_masters_id',
                'NothiMasterPermissions.nothi_part_no',
                'NothiMasterPermissions.nothi_office',
                'NothiMasterPermissions.modified',
                'NothiMasterPermissions.visited',
                'NothiParts.id',
                'NothiParts.nothi_part_no_bn',
                'NothiParts.nothi_no',
                'NothiParts.subject',
                'NothiParts.modified'
            ])->join([
                'NothiParts' => [
                    'type' => 'INNER',
                    'table' => 'nothi_parts',
                    'conditions' => 'NothiMasterPermissions.nothi_part_no = NothiParts.id'
                ]
            ])->where([
                'NothiMasterPermissions.nothi_masters_id' => $partNo,
                'NothiMasterPermissions.nothi_office' => $employee_office['office_id'],
                'NothiMasterPermissions.office_unit_organograms_id' => $employee_office['office_unit_organogram_id'],
            ])->order(['NothiParts.nothi_part_no' => 'ASC']);

            $nothiList = $this->Paginator->paginate($query, ['limit' => 20]);


            $nothiMasterPermittedUserTableCheck = $nothiMasterPermittedUserTable->find()->select(['nothi_part_no', 'visited'])->where(['nothi_masters_id' => $partNo])->group(['nothi_part_no', 'visited'])->toArray();
            if (count($nothiMasterPermittedUserTableCheck) == 1) {
                if ($nothiMasterPermittedUserTableCheck[0]['visited'] == 0) {
                    $nothi_master_table = TableRegistry::get('NothiMasters');
                    $nothi_master = $nothi_master_table->get($partNo);
                    if ($nothi_master['office_id'] == $employee_office['office_id']) {
                        $you_are_permitted_for_first_note = true;
                    }
                }
            }
            if (!empty($nothiList)) {
                foreach ($nothiList->toArray() as $key => &$value) {
                    $getCurrentUser = $nothiCurrentTable->find()->select([
                        'is_archive',
                        'is_finished',
                        'is_summary',
                        'office_id',
                        'office_unit_id',
                        'office_unit_organogram_id',
                        'view_status',
                        'issue_date',
                        'priority',
                        'is_new',
                    ])->where(['nothi_office' => $value['nothi_office'], 'nothi_part_no' => $value['nothi_part_no']])->first();

                    $employeeInfo = $employeeOfficeTable->getDesignationInfo($getCurrentUser['office_unit_organogram_id'], 1);

                    $nothinotes = $nothiNotesTable->find()->select(['subject'])->where(['nothi_part_no' => $value['nothi_part_no'], 'note_no >=' => 0])->first();
                    $value['subject'] = !empty($nothinotes) ? trim(strip_tags($nothinotes['subject'])) == '' ? '<small style="font-style: italic;color: gray;">বিষয় দেয়া হয়নি</small>' : h(strip_tags($nothinotes['subject'])) : 'অনুচ্ছেদ দেওয়া হয়নি';
                    $value['class'] = (
                    $getCurrentUser['priority'] ? 'nothiImportant' : (($getCurrentUser['office_unit_organogram_id'] == $employee_office['office_unit_organogram_id'] &&
                        $getCurrentUser['view_status'] == 0) ? 'new' : ($getCurrentUser['is_summary'] ? 'summaryList' : ($getCurrentUser['office_unit_organogram_id'] == $employee_office['office_unit_organogram_id'] ? 'danger' : ''))));
                    $issue_date = new Time($getCurrentUser['issue_date']);
                    $value['issue_date'] = $issue_date->i18nFormat("dd-MM-YY HH:mm:ss");
                    $viewStatus = $getCurrentUser['view_status'] == 0 ? 'দেখেননি' : 'দেখেছেন';

                    $value['details'] = true;
                    if (!$value['visited']) {
                        $value['details'] = false;
                    }

					$designation_info = designationInfo($getCurrentUser['office_unit_organogram_id']);
					if ($employee_office['office_id'] !== $employeeInfo['office_id']) {
                    	if ($employeeInfo) {
							$otherOffices = $officesTable->get($employeeInfo['office_id']);
							$otherOfficeUnits = $officeUnitsTable->get($employeeInfo['office_unit_id']);

							$value['current_desk'] =
								$designation_info['officer_name'].', ' .
								$designation_info['designation_label'] .
								(!empty($designation_info['incharge_label']) ? " 
                        		({$designation_info['incharge_label']})" : '') .
								($employeeInfo['show_unit'] ? (", " . $otherOfficeUnits['unit_name_bng']) : '')
								. (", " . $otherOffices['office_name_bng'])
								. " ({$viewStatus})";
						} else {
                            $value['current_desk'] =
                                $designation_info['officer_name'].', ' .
                                $designation_info['designation_label'] .
                                (!empty($designation_info['incharge_label']) ? " 
                        		({$designation_info['incharge_label']})" : '') .
                                ", " . $designation_info['office_unit_name']
                                . (", " . $designation_info['office_name'])
                                . " ({$viewStatus})";
                        }
                    } else {
                        $value['current_desk'] =
                            $designation_info['officer_name'].', ' .
                            $designation_info['designation_label'] .
                            (!empty($designation_info['incharge_label']) ? " 
                        ({$designation_info['incharge_label']})" : '') .
                            ($employeeInfo['show_unit'] ? ((!empty($officeUnits[$getCurrentUser['office_unit_id']]) ? (", " .
                                $officeUnits[$getCurrentUser['office_unit_id']]) : '')) : '')
                            . " ({$viewStatus})";
                    }

                }
            }
        }
        $this->layout = null;
        $this->set(compact('you_are_permitted_for_first_note'));
        $this->set("nothiLists", $nothiList);
    }

    public function setMeFor1stNote($nothi_master_id, $nothi_part_no) {
        $session_info = $this->getCurrentDakSection();
        $nothi_master_table = TableRegistry::get('NothiMasters');
        $nothi_master = $nothi_master_table->get($nothi_master_id);

        $conn2 = [];
        if ($session_info['office_id'] == $nothi_master['office_id']) {
            $conn = ConnectionManager::get('default');
            $conn->begin();
            try {
                $nothi_master_permission_table = TableRegistry::get('NothiMasterPermissions');
                $nothi_master_permission_table->updateAll(['visited' => 0], ['nothi_masters_id' => $nothi_master_id]);
                $nothi_master_permission_table->updateAll(['visited' => 1], ['nothi_masters_id' => $nothi_master_id, 'office_unit_organograms_id' => $session_info['office_unit_organogram_id']]);

                $nothi_master_current_user_table = TableRegistry::get('NothiMasterCurrentUsers');
                $nothi_master_current_user_table->updateAll(['office_unit_id' => $session_info['office_unit_id'], 'office_unit_organogram_id' => $session_info['office_unit_organogram_id']], ['nothi_master_id' => $nothi_master_id, 'nothi_part_no' => $nothi_part_no]);

                $nothi_part_table = TableRegistry::get('NothiParts');
                $nothi_part_table->updateAll(['office_units_id' => $session_info['office_unit_id'], 'office_units_organogram_id' => $session_info['office_unit_organogram_id']], ['id' => $nothi_part_no]);


                $nothi_master_permission_table = TableRegistry::get('NothiMasterPermissions');
                $priviliges = $nothi_master_permission_table->find()->select([
                    'perm_value' => 'privilige_type',
                    'ofc_id' => 'office_id',
                    'unit_id' => 'office_unit_id',
                    'org_id' => 'office_unit_organograms_id',
                    'designation_level' => 'designation_level',
                    'visited' => 'visited'
                ])->where(['nothi_masters_id' => $nothi_master_id, 'nothi_part_no' => $nothi_part_no, 'nothi_office' => $session_info['office_id']])->toArray();

                $nothi_master_permission_table_other = $nothi_master_permission_table->find('list', ['keyField' => 'office_id', 'valueField' => 'office_id'])->where(['nothi_masters_id' => $nothi_master_id, 'office_id !=' => $nothi_master['office_id']])->toArray();
                foreach ($nothi_master_permission_table_other as $other_office_id) {
                    $is_connected = $this->switchOfficeWithStatus($other_office_id, 'MainNothiOffice', -1, true);
                    if (!$is_connected || (!empty($is_connected['status']) && $is_connected['status'] == 'error')) {
                        $officeDomainsTable = TableRegistry::get('OfficeDomains');
                        $otherOffice = $officeDomainsTable->find()->where(['office_id' => $other_office_id])->first();
                        $url = $otherOffice->domain_url . "/postUpdateOtherOfficePermissions";

                        $encrypted_data = $this->makeEncryptedData(serialize([
                            'nothi_office' => $session_info['office_id'],
                            'nothi_part_no' => $nothi_part_no,
                            'nothi_master_id' => $nothi_master_id
                        ]));

                        $data = [
                            'token' => base64_encode($encrypted_data),
                            'nothi_office' => $session_info['office_id'],
                            'nothi_part_no' => $nothi_part_no,
                            'nothi_master_id' => $nothi_master_id,
                            'current_users' => json_encode(['office_id' => $session_info['office_id'], 'office_unit_id' => $session_info['office_unit_id'], 'office_unit_organogram_id' => $session_info['office_unit_organogram_id']]),
                            'permissions' => json_encode($priviliges),
                            'target_office_id' => $other_office_id
                        ];

                        $auth_token_response = curlRequest($url, $data);
                        if (!empty($auth_token_response)) {
                            $auth_token_response = jsonA($auth_token_response);
                            if (!empty($auth_token_response['status']) && ($auth_token_response['status']) == 'success') {
                                continue;
                            }
                        }
                    } else {
                        $conn2[$other_office_id] = ConnectionManager::get('default');
                        $conn2[$other_office_id]->begin();
                        try {
                            TableRegistry::remove('NothiMasterCurrentUsers');
                            $nothi_master_current_user_table = TableRegistry::get('NothiMasterCurrentUsers');
                            $nothi_master_current_user_table->updateAll(['office_unit_id' => $session_info['office_unit_id'], 'office_unit_organogram_id' => $session_info['office_unit_organogram_id'], 'employee_id' => $session_info['officer_id']], ['nothi_master_id' => $nothi_master_id, 'nothi_part_no' => $nothi_part_no, 'nothi_office' => $session_info['office_id']]);


                            TableRegistry::remove('NothiMasterPermissions');
                            $nothi_master_permission_table = TableRegistry::get('NothiMasterPermissions');
                            $nothi_master_permission_table->deleteAll(['nothi_masters_id' => $nothi_master_id, 'nothi_part_no' => $nothi_part_no, 'nothi_office' => $session_info['office_id']]);
                            foreach ($priviliges as $key => $permission) {
                                if ($permission['perm_value'] == 0) continue;
                                if (!isset($permission['ofc_id'])) continue;
                                $optionsRadios = array();

                                $nothiPriviligeRecord = $nothi_master_permission_table->newEntity();

                                $optionsRadios['nothi_masters_id'] = $nothi_master_id;
                                $optionsRadios['nothi_part_no'] = $nothi_part_no;
                                $optionsRadios['office_id'] = $permission['ofc_id'];
                                $optionsRadios['office_unit_id'] = $permission['unit_id'];
                                $optionsRadios['office_unit_organograms_id'] = $permission['org_id'];
                                $optionsRadios['designation_level'] = $this->BngToEng($permission['designation_level']);
                                $optionsRadios['privilige_type'] = intval($permission['perm_value']);

                                if (isset($permission['visited'])) {
                                    $optionsRadios['visited'] = $permission['visited'];
                                }
                                /*if ($current_users['office_unit_organogram_id'] == $permission['org_id']
                                    && $current_users['office_unit_id'] == $permission['unit_id']
                                    && $current_users['office_id'] == $permission['ofc_id']
                                ) {
                                    $optionsRadios['visited'] = 1;
                                }*/

                                $optionsRadios['nothi_office'] = $session_info['office_id'];
                                $optionsRadios['created'] = date("Y-m-d H:i:s");
                                $optionsRadios['created_by'] = $session_info['officer_id'];

                                $otherOfficePriv[] = $optionsRadios;

                                $nothiPriviligeRecord = $nothi_master_permission_table->patchEntity($nothiPriviligeRecord,
                                    $optionsRadios, ['validate' => 'add']);

                                $priviligeError = $nothiPriviligeRecord->errors();

                                if (!$priviligeError) {
                                    $nothi_master_permission_table->save($nothiPriviligeRecord);
                                } else {
                                    $response = [
                                        'status' => 'error',
                                        'msg' => 'সংরক্ষিত করা সম্ভব হচ্ছে না। কোডঃ ১',
                                        'reason' => makeEncryptedData(json_encode($priviligeError))
                                    ];
                                }
                            }
                        } catch (\Exception $exception) {
                            throw new $exception->getMessage();
                        }
                    }
                }

            } catch (\Exception $exception) {
                $conn->rollback();
                foreach ($conn2 as $office_id => $connection) {
                    $connection->rollback();
                }

                $this->response->type('application/json');
                $this->response->body(json_encode(['status' => 'error', 'msg' => $exception->getMessage()]));
                return $this->response;
            }
            foreach ($conn2 as $office_id => $connection) {
                $connection->commit();
            }
            $conn->commit();

            $this->response->type('application/json');
            $this->response->body(json_encode(['status' => 'success', 'msg' => '১নং নোট আপনার দেয়ার অনুমতি দেয়া হয়েছে']));
            return $this->response;
        } else {
            $this->response->type('application/json');
            $this->response->body(json_encode(['status' => 'error', 'msg' => 'You are not permitted']));
            return $this->response;
        }
    }

    public function nothiInboxList($organogram_id = false)
    {
        $nothiMasterTable = TableRegistry::get('NothiMasters');
        $employeeOffice = $this->getCurrentDakSection();
        $officeUnitsTable = TableRegistry::get('OfficeUnits');
        $officeUnits = $officeUnitsTable->getOfficeUnitsList($employeeOffice['office_id']);
        $start = isset($this->request->data['start']) ? intval($this->request->data['start']) : 0;
        $len = isset($this->request->data['length']) ? intval($this->request->data['length']) : 200;
        $page = !empty($this->request->data['page']) ? intval($this->request->data['page']) : (($start / $len) + 1);

        if ($organogram_id) {
			$OtherOrganogramActivitiesSettingsTable = TableRegistry::get('OtherOrganogramActivitiesSettings');
			$OtherOrganogramActivitiesSettings = $OtherOrganogramActivitiesSettingsTable->find()->where(['organogram_id' => $organogram_id, 'assigned_organogram_id' => $employeeOffice['office_unit_organogram_id'], 'status' => 1, 'permission_for IN' => [0,2]])->count();
			if ($OtherOrganogramActivitiesSettings > 0) {
				$employeeOffice = designationInfo($organogram_id);
			}
		}
        $getListData = $nothiMasterTable->getNothiList($this, $employeeOffice, 'inbox', 0);

        if ($getListData['total'] == 0 && !empty($this->request->data['nothi_no'])) {
            $NothiDataChangeHistoryTable = TableRegistry::get('NothiDataChangeHistory');
            $history_data = $NothiDataChangeHistoryTable
                ->find('list', ['keyField' => 'id', 'valueField' => 'nothi_master_id'])
                ->where(['nothi_data LIKE' => "%" . enTobn($this->request->data['nothi_no']) . "%"])
                ->toArray();

            if (!empty($history_data)) {
                unset($this->request->data['nothi_no']);
                $getListData = $nothiMasterTable->getNothiList($this, $employeeOffice, 'inbox', $history_data);
            }
        }

        $list = $getListData['data'];
        $listCount = $getListData['total'];

        $json = [];
        if (!empty($list)) {
            $nothiMasterCurrentUserTable = TableRegistry::get('NothiMasterCurrentUsers');

            //for sorting issue date field as agoto nothi comes with sorbosesh noter tarikh
            foreach ($list as $key => &$value) {
                // getting last issue date
                $lass_issue_date = $nothiMasterCurrentUserTable->find()->select(['issue_date'])->where([
                    'nothi_office' => $employeeOffice['office_id'],
                    'office_unit_organogram_id' => $employeeOffice['office_unit_organogram_id'],
                    'nothi_master_id' => $value['nothi_master_id'],
                    'is_new' => '0',
                ])->order(['issue_date' => 'desc'])->first();

                $value['issue_date'] = $lass_issue_date['issue_date'];
            }
            usort($list,
                function ($a, $b) {
                    $time = new Time($a['issue_date']);
                    $a['issue_date'] = $time->i18nFormat(null, null, 'en-US');

                    $time = new Time($b['issue_date']);
                    $b['issue_date'] = $time->i18nFormat(null, null, 'en-US');

                    $vala = strtotime($a['issue_date']);
                    $valb = strtotime($b['issue_date']);
                    return ($valb - $vala);
                });
            //for sorting issue date field as agoto nothi comes with sorbosesh noter tarikh

            $i = 0;
            $json_only_important = [];
            $json_not_important = [];
            foreach ($list as $key => &$value) {
                $totalCount = $nothiMasterCurrentUserTable->find()->where([
                    'nothi_office' => $employeeOffice['office_id'],
                    'office_unit_organogram_id' => $employeeOffice['office_unit_organogram_id'],
                    'is_archive' => 0,
                    'is_finished' => 0,
                    'is_new' => 0,
                    'nothi_master_id' => $value['nothi_master_id']
                ])->count();
                $isNew = $nothiMasterCurrentUserTable->find()->where([
                    'nothi_office' => $employeeOffice['office_id'],
                    'office_unit_organogram_id' => $employeeOffice['office_unit_organogram_id'],
                    'is_archive' => 0,
                    'nothi_master_id' => $value['nothi_master_id'],
                    'view_status' => 0,
                ])->count();
                $isPrior = $nothiMasterCurrentUserTable->find()->where([
                    'nothi_office' => $employeeOffice['office_id'],
                    'office_unit_organogram_id' => $employeeOffice['office_unit_organogram_id'],
                    'is_archive' => 0,
                    'nothi_master_id' => $value['nothi_master_id'],
                    'priority' => 1,
                ])->count();
                $isSummary = $nothiMasterCurrentUserTable->find()->where([
                    'nothi_office' => $employeeOffice['office_id'],
                    'office_unit_organogram_id' => $employeeOffice['office_unit_organogram_id'],
                    'is_archive' => 0,
                    'nothi_master_id' => $value['nothi_master_id'],
                    'is_summary' => 1,
                ])->count();

                $totalCountbn = Number::format($totalCount);

                $crTime = new Time($value['issue_date']);

                $show_only_important = isset($this->request->data['show_only_important']) ? $this->request->data['show_only_important'] : 0;
                if ($show_only_important == 1) {
                    if ($isPrior == 0) {
                        continue;
                    }
                }
                $i++;
                $si = (($page - 1) * $len) + $i;
                if ($isPrior) {
                    $json_only_important['imp_' . $key] = [
                        '<div class="text-center">'.Number::format($si).'</div>',
                        h($value['NothiMasters']['nothi_no']) . ($totalCount > 1 ? " <label class='badge badge-primary'>
                         {$totalCountbn}</label>" : ''),
                        h($value['NothiMasters']['subject']),
                        '<div class="text-center">'.$crTime->i18nFormat('dd-MM-YYYY').'</div>',
                        '<div class="text-center">'.h($officeUnits[$value['NothiMasters']['office_units_id']]).'</div>'.
                        "<div class='text-center hidden'><button class='checkdetails btn btn btn-success btn-xs dbl_click' 
data-nothimasterid='{$value['nothi_master_id']}' data-nothipartno='{$value['nothi_part_no']}' data-type='inbox'><i class='a2i_gn_details2'></i></button></div>",
                        //"<div class='text-center'><input type='checkbox' class='checkarchive' data-nothimasterid='{$value['nothi_master_id']}' /></div>",
                        'DT_RowClass' => ($isPrior ? 'nothiImportant' : ($isNew ? 'new' : ($isSummary ? 'summaryList' : '')))
                        //'DT_RowClass'=>($isNew?'new':($isPrior?'nothiImportant':($isSummary?'summaryList':'')))
                    ];
                } else {
                    $json_not_important['not_imp_' . $key] = [
                        '<div class="text-center">'.Number::format($si).'</div>',
                        '<div class="text-left">'.h($value['NothiMasters']['nothi_no']) .
                        ($totalCount > 1 ? " <label class='badge badge-primary'>
                            {$totalCountbn}
                          </label>" : '').'</div>',
                        h($value['NothiMasters']['subject']),
                        '<div class="text-center">'.$crTime->i18nFormat('dd-MM-YYYY').'</div>',
                        '<div class="text-center">'.h($officeUnits[$value['NothiMasters']['office_units_id']]).'</div>'.
                        "<div class='text-center hidden'><button class='checkdetails btn btn btn-success btn-xs dbl_click' 
data-nothimasterid='{$value['nothi_master_id']}' data-nothipartno='{$value['nothi_part_no']}' data-type='inbox'><i class='a2i_gn_details2'></i></button></div>",
                        //"<div class='text-center'><input type='checkbox' class='checkarchive' data-nothimasterid='{$value['nothi_master_id']}' /></div>",
                        'DT_RowClass' => ($isPrior ? 'nothiImportant' : ($isNew ? 'new' : ($isSummary ? 'summaryList' : '')))
                        //'DT_RowClass'=>($isNew?'new':($isPrior?'nothiImportant':($isSummary?'summaryList':'')))
                    ];
                }
            }
        }
        if (!empty($json_only_important) && !empty($json_not_important)) {
            $json_array = array_merge($json_only_important, $json_not_important);
            $json = array_values($json_array);

            $i = 0;
            foreach ($json as $key => &$value) {
                $i++;
                $si = (($page - 1) * $len) + $i;
                $value[0] = Number::format($si);
            }
        } else if (!empty($json_only_important)) {
            $json = array_values($json_only_important);
        } else if (!empty($json_not_important)) {
            $json = array_values($json_not_important);
        }

        $json_data = array(
            "draw" => 0,
            "recordsTotal" => $listCount,
            "recordsFiltered" => $listCount,
            "data" => $json
        );
        $this->response->body(json_encode($json_data));
        $this->response->type('application/json');
        return $this->response;
    }

    public function nothiAllList()
    {

        $nothiMasterTable = TableRegistry::get('NothiMasters');
        $employeeOffice = $this->getCurrentDakSection();
        $officeUnitsTable = TableRegistry::get('OfficeUnits');
        $officeUnits = $officeUnitsTable->getOfficeUnitsList($employeeOffice['office_id']);
        $start = isset($this->request->data['start']) ? intval($this->request->data['start']) : 0;
        $len = isset($this->request->data['length']) ? intval($this->request->data['length']) : 200;
        $page = !empty($this->request->data['page']) ? intval($this->request->data['page']) : (($start / $len) + 1);

        $getListData = $nothiMasterTable->getNothiList($this, $employeeOffice, 'all');

        if ($getListData['total'] == 0 && !empty($this->request->data['nothi_no'])) {
            $NothiDataChangeHistoryTable = TableRegistry::get('NothiDataChangeHistory');
            $history_data = $NothiDataChangeHistoryTable
                ->find('list', ['keyField' => 'id', 'valueField' => 'nothi_master_id'])
                ->where(['nothi_data LIKE' => "%" . enTobn($this->request->data['nothi_no']) . "%"])
                ->toArray();

            if (!empty($history_data)) {
                unset($this->request->data['nothi_no']);
                $getListData = $nothiMasterTable->getNothiList($this, $employeeOffice, 'all', $history_data);
            }
        }

        $list = $getListData['data'];
        $listCount = $getListData['total'];

        $json = [];
        if (!empty($list)) {
            $nothiMasterCurrentUserTable = TableRegistry::get('NothiMasterCurrentUsers');
            $i = 0;

            foreach ($list as $key => &$value) {
                $value['nothi_master_id'] = $value['nothi_masters_id'];
                $totalCount = $nothiMasterCurrentUserTable->find()->where([
                    'nothi_office' => $employeeOffice['office_id'],
                    'office_unit_organogram_id' => $employeeOffice['office_unit_organogram_id'],
                    'nothi_master_id' => $value['nothi_master_id']
                ])->count();
                $isNew = $nothiMasterCurrentUserTable->find()->where([
                    'nothi_office' => $employeeOffice['office_id'],
                    'office_unit_organogram_id' => $employeeOffice['office_unit_organogram_id'],
                    'nothi_master_id' => $value['nothi_master_id'],
                    'view_status' => 0,
                ])->count();
                $isPrior = $nothiMasterCurrentUserTable->find()->where([
                    'nothi_office' => $employeeOffice['office_id'],
                    'office_unit_organogram_id' => $employeeOffice['office_unit_organogram_id'],
                    'is_archive' => 0,
                    'nothi_master_id' => $value['nothi_master_id'],
                    'priority' => 1,
                ])->count();
                $isSummary = $nothiMasterCurrentUserTable->find()->where([
                    'nothi_office' => $employeeOffice['office_id'],
                    'office_unit_organogram_id' => $employeeOffice['office_unit_organogram_id'],
                    'is_archive' => 0,
                    'nothi_master_id' => $value['nothi_master_id'],
                    'is_summary' => 1,
                ])->count();

                $totalCountbn = Number::format($totalCount);

                $show_only_important = isset($this->request->data['show_only_important']) ? $this->request->data['show_only_important'] : 0;
                if ($show_only_important == 1) {
                    if ($isPrior == 0) {
                        continue;
                    }
                }
                $i++;
                $si = (($page - 1) * $len) + $i;
                $json[] = [
                    '<div class="text-center">'.Number::format($si).'</div>',
                    h($value['NothiMasters']['nothi_no']) .
                    ($totalCount > 1 ? " <label class='badge badge-primary'>{$totalCountbn}</label>" : ''),
                    h($value['NothiMasters']['subject']),
                    '<div class="text-center">'.h($officeUnits[$value['NothiMasters']['office_units_id']]).'</div>'.
                    "<div class='text-center hidden'><button class='checkdetails btn btn btn-success btn-xs dbl_click' data-nothimasterid='{$value['nothi_master_id']}' data-nothipartno='{$value['nothi_part_no']}' data-type='all'><i class='a2i_gn_details2'></i></button></div>",
                    'DT_RowClass' => ($isPrior ? 'nothiImportant' : ($isNew ? 'new' : ($isSummary ? 'summaryList' : '')))
                    //'DT_RowClass'=>($isNew?'new':($isPrior?'nothiImportant':($isSummary?'summaryList':'')))
                ];
            }
        }

        $json_data = array(
            "draw" => 0,
            "recordsTotal" => $listCount,
            "recordsFiltered" => $listCount,
            "data" => $json
        );
        $this->response->body(json_encode($json_data));
        $this->response->type('application/json');
        return $this->response;
    }

    public function addNothiNotesTableForSummeryNote()
    {
        if ($this->request->is(['sarok_no', 'post'])) {
            $sarok_no = $this->request->data['sarok_no'];
            $summary_nothi_users_table = TableRegistry::get('SummaryNothiUsers');
            $summary_nothi_users = $summary_nothi_users_table->find()->where(['tracking_id' => $sarok_no, 'sequence_number' => 1])->order(['id' => 'desc'])->first();

            if ($summary_nothi_users) {
                $potrojariTable = TableRegistry::get('Potrojari');
                $potrojari = $potrojariTable->get($summary_nothi_users->potrojari_id);

                $potrojariTemplatesTable = TableRegistry::get('PotrojariTemplates');
                $potro_type = $potrojariTemplatesTable->get($potrojari->potro_type);

                try {
                    $notesTable = TableRegistry::get('NothiNotes');
                    $nothinotes = $notesTable->newEntity();
                    $lastNoteinfo = $notesTable->find()->where(['nothi_master_id' => $summary_nothi_users->nothi_master_id,
                        'nothi_part_no' => $summary_nothi_users->nothi_part_no])->order(['note_no desc'])->first();
                    $nothinotes->nothi_master_id = $summary_nothi_users->nothi_master_id;
                    $nothinotes->nothi_part_no = $summary_nothi_users->nothi_part_no;
                    $nothinotes->nothi_notesheet_id = !empty($lastNoteinfo) ? $lastNoteinfo['nothi_notesheet_id'] : 1;
                    $nothinotes->office_id = $potrojari->office_id;
                    $nothinotes->office_unit_id = $potrojari->office_unit_id;
                    $nothinotes->office_organogram_id = $potrojari->officer_designation_id;
                    $nothinotes->employee_id = $potrojari->officer_id;
                    $nothinotes->employee_designation = $potrojari->officer_designation_label;
                    $nothinotes->note_no = -1;
                    $nothinotes->subject = '';
                    $time = new \Cake\I18n\Time($potrojari->potrojari_date);
                    $potrojari->potrojari_date = $time->i18nFormat('YYYY-MM-dd',
                        null, 'bn-BD');
                    $nothinotes->note_description = base64_encode("অনুমোদিত সার-সংক্ষেপ নম্বর " . $potrojari->sarok_no . ', প্রেরণের তারিখ: ' . $potrojari->potrojari_date);
                    $nothinotes->note_status = "INBOX";
                    $nothinotes->is_potrojari = 1;

                    $notesTable->save($nothinotes);
                    $this->response->body(json_encode(['status' => 'success', 'msg' => 'সফলভাবে উপস্থাপন করা হয়েছে']));
                } catch (\Exception $exception) {
                    $this->response->body(json_encode(['status' => 'error', 'msg' => 'সাময়িকভাবে সমস্যার কারণে আমরা দুঃখিত']));
                }
            } else {
                $this->response->body(json_encode(['status' => 'error', 'msg' => 'সার-সংক্ষেপ পাওয়া যায়নি']));
            }
        } else {
            $this->response->body(json_encode(['status' => 'error', 'msg' => 'Requested method is not allowed']));
        }
        $this->response->type('application/json');
        return $this->response;
    }

    public function postApiNothiInfo($nothi_office_id, $nothi_master_id, $api_key = 'undefined')
    {
        // check is POST
        // check Application
        // check Device
        if ($api_key == 'API_KEY') {
            try {
                $this->switchOffice($nothi_office_id, 'OfficeDb');
            } catch (\Exception $exception) {
                $this->response->body(json_encode(['status' => 'error', 'msg' => 'Database could not connect']));
                $this->response->type('application/json');
                return $this->response;
            }
            $return_data = [];
            $msg = "Nothi master id not found";
            $nothi_masters_table = TableRegistry::get('NothiMasters');
            $nothi_masters = $nothi_masters_table->get($nothi_master_id);
            if ($nothi_masters) {
                $return_data['master_info'] = [
                    'id' => $nothi_masters->id,
                    'office_id' => $nothi_masters->office_id,
                    'office_units_id' => $nothi_masters->office_units_id,
                    'office_units_organogram_id' => $nothi_masters->office_units_organogram_id,
                    'nothi_no' => $nothi_masters->nothi_no,
                    'nothi_subject' => $nothi_masters->subject,
                    'nothi_created_date' => $nothi_masters->nothi_created_date,
                    'nothi_class' => $nothi_masters->nothi_class
                ];
                $nothi_parts_table = TableRegistry::get('NothiParts');
                $nothi_parts = $nothi_parts_table->find()->where(['nothi_masters_id' => $nothi_masters->id]);
                $return_data['master_info']['total_note'] = count($nothi_parts->toArray());
                foreach ($nothi_parts as $nothi_part_key => $nothi_part) {
                    $return_data['master_info']['part_info'][$nothi_part_key] = [
                        'nothi_part_id' => $nothi_part->id,
                        'nothi_part_no_en' => $nothi_part->nothi_part_no,
                        'nothi_part_no_bn' => $nothi_part->nothi_part_no_bn,
                        'part_subject' => $nothi_part->subject
                    ];
                    $nothi_master_permissions_table = TableRegistry::get('NothiMasterPermissions');
                    $nothi_master_permissions = $nothi_master_permissions_table->find()->where(['nothi_masters_id' => $nothi_masters->id, 'nothi_part_no' => $nothi_part->id]);
                    foreach ($nothi_master_permissions as $nothi_master_permission_key => $nothi_master_permission) {
                        $return_data['master_info']['part_info'][$nothi_part_key]['permissions'][$nothi_master_permission_key] = [
                            'officer_name' => TableRegistry::get('EmployeeRecords')->get(TableRegistry::get('EmployeeOffices')->find()->where(['office_id' => $nothi_master_permission->office_id, 'office_unit_id' => $nothi_master_permission->office_unit_id, 'office_unit_organogram_id' => $nothi_master_permission->office_unit_organograms_id, 'status' => 1])->first()->employee_record_id)->name_bng,
                            'office_id' => $nothi_master_permission->office_id,
                            'office_name' => TableRegistry::get('Offices')->get($nothi_master_permission->office_id)->office_name_bng,
                            'office_unit_id' => $nothi_master_permission->office_unit_id,
                            'office_unit_name' => TableRegistry::get('OfficeUnits')->get($nothi_master_permission->office_unit_id)->unit_name_bng,
                            'office_unit_organograms_id' => $nothi_master_permission->office_unit_organograms_id,
                            'office_unit_organograms_name' => TableRegistry::get('OfficeUnitOrganograms')->get($nothi_master_permission->office_unit_organograms_id)->designation_bng,
                            'designation_level' => $nothi_master_permission->designation_level,
                            'privilige_type' => $nothi_master_permission->privilige_type,
                        ];
                    }
                    $nothi_master_current_users_table = TableRegistry::get('NothiMasterCurrentUsers');
                    $nothi_master_current_users = $nothi_master_current_users_table->find()->where(['nothi_master_id' => $nothi_masters->id, 'nothi_part_no' => $nothi_part->id])->first();
                    if ($nothi_master_current_users) {
                        $return_data['master_info']['part_info'][$nothi_part_key]['current_user'] = [
                            'office_id' => $nothi_master_current_users->office_id,
                            'office_unit_id' => $nothi_master_current_users->office_unit_id,
                            'office_unit_organogram_id' => $nothi_master_current_users->office_unit_organogram_id,
                            'employee_id' => $nothi_master_current_users->employee_id,
                            'view_status' => $nothi_master_current_users->view_status,
                            'is_new' => $nothi_master_current_users->is_new,
                            'priority' => $nothi_master_current_users->priority,
                        ];
                    }

                    $nothi_notes_table = TableRegistry::get('NothiNotes');
                    $nothi_notes = $nothi_notes_table->find()->where(['nothi_master_id' => $nothi_masters->id, 'nothi_part_no' => $nothi_part->id]);
                    foreach ($nothi_notes as $nothi_note_key => $nothi_note) {
                        $return_data['master_info']['part_info'][$nothi_part_key]['note_info'][$nothi_note_key] = [
                            'nothi_note_id' => $nothi_note->id,
                            'office_id' => $nothi_note->office_id,
                            'office_unit_id' => $nothi_note->office_unit_id,
                            'office_organogram_id' => $nothi_note->office_organogram_id,
                            'employee_id' => $nothi_note->employee_id,
                            'employee_designation' => $nothi_note->employee_designation,
                            'note_no' => $nothi_note->note_no,
                            'note_subject' => $nothi_note->subject,
                            'note_description' => base64_decode($nothi_note->note_description),
                            'note_status' => $nothi_note->note_status,
                            'is_potrojari' => $nothi_note->is_potrojari,
                            'potrojari' => $nothi_note->potrojari,
                            'potrojari_status' => $nothi_note->potrojari_status,
                        ];
                        $nothi_note_attachments_table = TableRegistry::get('NothiNoteAttachments');
                        $nothi_note_attachments = $nothi_note_attachments_table->find()->where(['nothi_master_id' => $nothi_masters->id, 'nothi_part_no' => $nothi_part->id, 'note_no' => $nothi_note->id]);
                        foreach ($nothi_note_attachments as $nothi_note_attachment_key => $nothi_note_attachment) {
                            $return_data['master_info']['part_info'][$nothi_part_key]['note_info'][$nothi_note_key]['note_attachment'][$nothi_note_attachment_key] = [
                                'nothi_attachment_id' => $nothi_note_attachment->id,
                                'attachment_type' => $nothi_note_attachment->attachment_type,
                                'file_path' => $nothi_note_attachment->file_name,
                                'file_custom_name' => $nothi_note_attachment->user_file_name,
                                'file_dir' => $nothi_note_attachment->file_dir,
                            ];
                        }

                        $nothi_note_signatures_table = TableRegistry::get('NothiNoteSignatures');
                        $nothi_note_signatures = $nothi_note_signatures_table->find()->where(['nothi_master_id' => $nothi_masters->id, 'nothi_part_no' => $nothi_part->id, 'nothi_note_id' => $nothi_note->id]);
                        foreach ($nothi_note_signatures as $nothi_note_signature_key => $nothi_note_signature) {
                            $return_data['master_info']['part_info'][$nothi_part_key]['note_info'][$nothi_note_key]['note_signature'][$nothi_note_signature_key] = [
                                'nothi_note_signature_id' => $nothi_note_signature->id,
                                'office_id' => $nothi_note_signature->office_id,
                                'office_unit_id' => $nothi_note_signature->office_unit_id,
                                'office_organogram_id' => $nothi_note_signature->office_organogram_id,
                                'employee_id' => $nothi_note_signature->employee_id,
                                'employee_designation' => $nothi_note_signature->employee_designation,
                                'is_signature' => $nothi_note_signature->is_signature,
                                'cross_signature' => $nothi_note_signature->cross_signature,
                                'signature_date' => $nothi_note_signature->signature_date,
                                'username' => $this->getUsernameByEmployeeId($nothi_note_signature->employee_id),
                                //'signature_encoded' => $this->getSignatureByUsernameAndDate($nothi_note_signature->employee_id, $nothi_note_signature->signature_date),
                                'signature_encoded' => 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAUIAAACdCAMAAAD2bIHgAAAAkFBMVEX///8AAAD7+/v4+Pjt7e3i4uLT09PLy8v09PTExMTw8PDa2tr29vatra3q6urd3d2goKCLi4u5ubmxsbGamppnZ2eDg4OmpqacnJxERERtbW1dXV3BwcGRkZF7e3tiYmJQUFAjIyM4ODh1dXV+fn4bGxtJSUkxMTE/Pz8WFhYqKipUVFRra2sXFxcMDAwgICBgm1reAAATLUlEQVR4nO1d2YKqOBDtqIBKg2xCC6Iobq1e/f+/m1SFfVFAtNXxPMzcli1Uaq9K+Pr64IMPPnhqDEei2PvrQbwqusLPcUEYtL8ezOuhM1D3JInhX4/osRiNLhwzxpPJYXXhDCq70oHRbbmSBFEc8SdC+LYH+cTQ4PXlkoOjScBU+9LruxI7Z2LwHfaLsKV/dtsf6XOi4+LrCyWHpUguzZIzhDkenk1DkvUtUIfH7zsM9inxg++vlh2exqqtkCQdhUos+eeK4Q99yUOCljH12wFUFiVgOcPgYeQzveBo38TjkZbkFJToTdG5bwOZ43meC0m2ghdWLpxOmfBE+UkmZJ4/+A0EHIcaoKf5yKye9a6WeCgos0kslcdpT5YuijDCYmZmWmQbDLi6j/+UtRnzBv3pm2rAkWSfSCF+uctXCmRM/8sTYmSP6PRqh/6fmxoOu9dYunKzV0VPPyZptvXGM9M0V+y1T1f9jg1RZ785l0c22AQsSegMam9Kvi/RjeKF36MxHSUoBo5bnrdy4ILL59KAH3Ecxwua4v+Lp2Qxd7WLPvdLQ1RD6fUMoZ85SD2PvU2WV2+yA+rvCpWALfHvajgQnLtmL0ojhoLD1LBIqM86l+8C/AYu9UgzTHt8OMzHYx91wLHMFX8TfFuBkrK1QmXXX5BfVG5X0inodmdp1TtTAr55KmsQWMhVGaNQA7tj/yp2mANwoC79govfPH8gqwH9yl9Tivxk+RI1wG8+5Q5rzJV5X0w3zEMbXDiHRiSr4J/UcS69U7HbbZVnG94BPZZy2UoXnb1DwpVZoONcAA6mYpd39SgDuzeO8onBs5STedlJ6ywT6m9QkuLqzOBOBVpy8M48qKMLvb2WIemvk1RbFGdRMXT2C7wd6maXcO3L45tJsC1eO5GaDxKfRPXaNH+OCHZ4WcTLHVLBFX9JyCh2xM3GH3lwqVh3WESRrl8iw18QLpPrz3hBiKgCF1VynCKN6RKpKBqg5HhNJ6RU3VFWv2ToXxU8OjGbSsEW9YkniT/dPK04iIW9kow9vXzWbJDPjAGGcc5VFchOTvvEAiHnzBmgURdls0EV4anJGKuDdz3KDQ/N+gwwXWVXzNNN00wk51KAPKQlpNLrD/cN6zgzSP8UzVNvqrhWNUapA0ZAv2qdTEv7xN8km2AAm7T6KoVeEC23B/4Qp9Cyx2R3wQ4c2i1LC8taBAQKWIk/O4tMtnVQ5sgE6JFrabEbIHsseWsIUk7fik5M3PJWgPrgMQVqV084WWme6+wzpsQmaRLnMLmY0rkNrIawB2uvZLRLjxFwNuj1hHWLPTojnLR5jVK3kvahIURJJhCABb2Lw7PSxrxN9FlGHCdQzMiGER/6+jqVp0RqoofFJK+O4TLSFBQzvrNd6ktHz6Rn3Km6xCOVNuiyUxO3ST4VlVVI01VbctBBw7WtlXQ30lJspYI8cG7I4UrQsblWbW4M1rDDegH6aTdrgFm7fnxiO2lKBc1+vdZHNzV9MvjiCaFdXWVBtEV3cglZephNKKXgKTGVKMRRNKS1ZEwGv/GUVYabNBQCaoFYJrlzKEQX0CXlbV+3AePxHfNVemkKwkDjrBCl4LoFlwazoGRW805qpE1ENyhJxf6dcs0QI8aF/TQtAKN7m/2bo2yWeLVJym+lumdxOwWZEtzUVepq4L3IKlnjFCxj09Klln173a6DsrxLvc5JWIt0/P5N3dZFPDK3lSQbZlCKsnuXobJZ5jfkp//lkZPV/YrSM2ALqyTx1xejlubAgCRQw1oqlwvBZ4LvqcdwuPlpmEFpULSgFPToiJYTHk2axIbHphfUdZWg1yD3aTtHHgxUrJt6OY4k3cPOpI3QEu3Wpn7XKL1uAQw8Qm3yg78NAhI6FXP4Q3Kf4Bj1YDCFx5TXJabUoHxqoeAlNJNhRvk+Zbud4pC1FP9ISThcVF0VMr+PJrRjZ6a7TKV+05X+Kbl9+Up3TBrygRoM0nJsJbZCC6AIV8mOAOCF7uBVY3Mtoxtlum0ilc6nnC61ora5BA2edW5yF7XQZ4YRdvnqBgIyag0efgVuzINS5NYgYHzR5HapD/F7Y9f7t9PMjASjLMjTg78lVVcL+l28aivWg3Y6y8sn1Qb8caspRhbcN4rvgYK7/M8sJF1UVm53sSVaFLrJi1S4nqagS6p0kF5EZ96YBVmAmU+RykjB6tV09x7BMR/5g5THveQgRwkKDqkf9+/GXD8a4nOzYsxPOhKOcCa1Zrafzum0Ay5ijHlmLMlyjtaCGcN4rmH/ipLwWpPAVG0NF8G/WZLy6Idxca5NcRgZ6a+uU16LrQp5S7JPqA7U1gXZA7smBcV7pKrPgZKmLOKkNE1nHb0wZcHzrUV/VLhOw3KPVMK/mFmqk3LZ3iGymzAlze1zHtcynN7e5lIttiKwR6bpXTAhccz/jjxYpwqntPAmWWCcIIOuzibKnTACdVvQHn1I662bliqQgQu8KVYKq6Ea5HaLjggM9qfykvxmHdMgpQQMYN6cGeTr+R0ZYHtv3iEcsgU8daZ33VyVlIF5pasCRU2HvaX/G/wj/u26QyqxBdWAJZtt7mfKmsa8niIMFuu0WbcbkBDZI+DODGEh/aGF563i6LEB0JdcZH/lPTLvfu3rZH75c/CyLbYF8yRGxt2lQYohLMmyjaYdyOQuG69GxUEu0tIHqSQg3qzGTgn6lr2oqzjtdSKJ7J4npQcOYGosY3aojT1ZuluSzlvUA1JwH1JQ1u1fJ3ASGapOTcAuzQdy4aZg44FPklE661loZSHB8FRT46eBgwyEFdfvzEBcqCVecIEWqqxioVAV1Vq1dQshCssbY8rCz9ALvTCvPPPRrb7IHnMADZLTyUGyaOLbjhbvCFg9EQI+nFSU5aRUgZTd3B+sRZLaAR7Us4eKHdCpIvEjamSqpjrkmwwJM3eHYFAnKRDnLvOPMDg2pmZFC3uIp7KDavFfjYFM/eUuq0FZ0wL8Ciy3Sk4kSPE5Dod7kj0JLk+ooGpPRgo2j6y1SE5o9BmnOCb42zTUbEKlBKqdoCCZ8HKxpu9yYkEbxE/BO8tsRwj9S7Z+swsTwOP34rdgZwIjyYmtq6qJJlip3+YrEpRYIauJ3ICPieturNmmFeyenzhnC/cKvJDvER+t4BZWi4xy5SWRJfw8fZ0i4fcsIAR4mov001HBeOFfGluAjoGZHhOwol3r/N7U8mCGmgZHFWlfl5VINgmamFebkJIU/IK3Sez1Q8iGl3lpHP4V9VpZ8PaOwAqWy2SCR0lenba6fcwph81ccuDMgwhBtnkRXpTzc4uxTHgj9TFPqNFFbEotVjxZpcxBPkWSxjEnN+HmBBN/NiZpBGciy/ghkawEd/KL1Pmp+zJfJtTOwUSNgx1dXGQLfLFqVQon5xHXACxMJNtACQjxOHUmAVZa/3HkoiwfctqSZXgO+GsnIMVMx02WmO7l6awsB9gAT8+zgWjBu3Tg2jEI8kbEPFfCmGlpxsSojDWe9g5QoQUXTQAqV/MGqMH6bSzFXEpdzIKUEYwRcxXTLE3A0JXZlO9tLvZCQfwXmNhlRE1wADDkHtLp38MviR0wlHAEBILefSDwyTUHPNxJFULG5BizArU6Kp7WQ+o68Zt9Dy9RCB7V2JJk3Kp1SAKdeTh8fhqhc6/Y9NMY7DcjNlpygsClO+D9hZAHgU9QMVAZ90QmzCxHNjww2k5CruQiSYZmcWivGAQiz3wemPGOwVZBQ3lgg4aMh50lTdbWtzFKfDKZ3JAQAQH4l7g6DOMsptLFIkHwSjKBSrrVGYAUDDWbE4kVcDLoWegZZYkInzmlTqTdfoKZtSKVCWQB8yZQ2m5Rl0yAuJ3A4Khs+c4SWXGNXAWuxP4YkO843jKuLsC6ebVbXpK0ze8HDGYwIo2KfSq7YP3iV9/L1+4VQuI58MI0JhSHYIZkJ2p49INZEQPtNqWqUkXeO8cDhJtt8Voh+mEerdnxTSrNNtMYG8bbTPWefYtnfDEsKS37JFJfdYE2LU0jNvU+S4VwZZZDyI9DJ3lTjXIZRhrLUJUxl83k6dHzND4RVdEQs5UaVX+rfjigqAU9aLKdRyEYks8VvzrokP/Oolchgfoc6YP0XHf8gqLGoHEnOzoMXib9Qpm9A92IwIuj6vkjbgLZiMyPGBaGr7AM6rqDOGrYhXcH/4PRhRovFcywGun2Y6x3IZt8cpPKlh+Ez5TTOpgvlUst72qThm0/Q/TRci5eEFjCCPjKCzW6K1KQbNpEeu2LJRtgy1ZQUuvlZOOspIg0esyrKPnbZNJATrCR0MbmXlo246Q2rBZjb5RTYMdROEBOp5ULcKAP8k3c4M6tw0CHVWUpd5vTvDEncXpkQLwyq9kajmlvu9es7YfxWmH0DcE2lJ/ciulBcCPY+rc0eJK0z2z7vm2xXnDVh+4czKWLQH71XHIMFo6XVTVAg7mwo2+V9Sk9cMm2RXPhp3tadFPVn2V/21PSCew26L3RcEO48tUTwX5TVcy8AB7GoVhz88en3ZdrkxRdq3aKUMPSWq6SnUCQsbuarB8hrdVnYa0a2CRN8KbmkhSdbUl4UcmFu16al4jDq3An+zW3IARnLnq5enIcRELXS13fvAFBvzMtiry7vIU5pNmrbpiCMUuY1+JqNP4MAx1nVjQ/QwVE/neuWvpA4CmEgW6pPttYbm698N614Ct4iT8qkpAPkp2rOhkd3gx7EhI4qNoLqr8kwAuLvKtRRUHWg+jSrJ0S6wuSOxs7jjM/rlwptaHwy2KSCuiopF29YhRu4GK8w/vfjn7aHDjXQoi+FbS3lAQG/0OAUU0wE18WpiF60iZgQL/9nYJeFlmnDhJDZlHhqRt/Rmmn322PnReEkVvejUWblZAkUp+XZlFSbm+8uP1sGak1egG8gFRz03VddTYPvzsAWN49cfRqAArmQwIj77shbP2t989vhLLtOLhDlnoLX/+wXwEOJRT8AtPrTzAHePZsVxM/1qMYXqEUPxgdeSRMNevHdVe+fRzPHefgURwOB2c8Hh9tf0U1sqFYujYV+BEnfz/TbHYJmf3hR5FkQTKdRYnWvYzTfuPYpqvoU16U/+gVRMX22m39roORZhyTH+rc7yaU5fzZbGWaKnUCjB8KhYL+zzDAK1DN1Wrm+/bc8Ta7ZS5TsffGpqEPePlR+zMLrGXpL0R4KPzYIeMtbUPjm/NQdyiLgmaZWcO3nhz8latImsBzdyHpiOU2m3crNAcfPpqcbUVo0TvvcLymqPZhT4pw2k7GvqpIU5672SOTB244Y/NHc6D4E4bWjsLfUXl15RE/1RXXtOeb/bqAoFvHdy0NPrrV++5UMEwdyuycSO2d4tpRq91i1XwRRDOIYWrs8d+k6/Tw/a0fdWY7u23Rd/dOp3/n/X67XO4oJrvdcrnd78/r3+Jv9O18JffRr7uDfW6CbI3nyOz0ZY4faJJCXajDpFj0c1hu5jPKuwL3J9lR1q68V543MdGXRaDpj2rajrf9PSH3LeemQQ2SyF3sY30IsCn6+vc6/h6UkMJUt1zmqdqWJEm6rg2E0V9/1Qw6E3ZPyoDfnDigRmI29vZptbfe2F5ajseqJPxRggCa964XYh6Jrsxrluo72wKzMrFVayoGXNcZTZXZJnXCeWwUtJDdGfDg56iv9KjnaDqF1mNypJTjS+S1K2qGn/py5sF4ZLsFtoH8rTrucAOrOBDfzU2LBkfVbjPkJTMh3BvlUWI9+EM57uYFEbGdmwqlXKNsD41Oo6VW+5+HJJ9hIcj6EQ9KgvKdMvNylHNWPxrPtZAnk7VZYH3mD3A0cC3u/R8TPU6wzEPGuHq+obfvEY8M9i3mNvYOuQLYeP0BylcWJNVJB8QTnzLdPV06nm1TdKevDMSAKlbBno+tAWg3z5jZ3cwSHqKmRmiot3cOmGGx1V0+Nol8l6Hd6WBababQroP17t45fMFVAC1+bpLaWarvMkms7Vy1Bm1YitpgO3Tc2W1D05j/nnFtUNfY8CcZM7uhCk/808I20jC7zLJtsEaIcdMX7YsFbHemIvs3bJcDitm9A7Cwl7ie2oWoQh0v06Sb0ICiNBT7I0Ad4P7hQ5ct05xVcqJkQU9k2Klnd/BVqHWOnvTbqbBS/hGOWxAs7KRSgzkUB7rrb5KO8c6UnozjCnG8k9ORxTSs3ZGFp4Zz1odK0U9R5mnrW0+7zCkL/N7hYx4l6mqYMTlLml+QASDk38Z2pcf6djcDfd9HPvBb5gVNMlSTYhZ0LxiWBFXJ3p9XKZoBSPiHbTXvALB9n67LmzD5kPBW7B6sC98QjzYn7wdYA3qvD+T+TwD9/nf5JN//B+EmVh80BqRCWtjp5n+MB5fY3hHQOvl3TevvANym88/Xnbw0/IdkXN8Z/IcJbwX5aMIbARmG8/XTPigFbl33ArWJ5wU2M7xAG/nzgn9Q7e6Nsf8Y4xuhfnjwNnScjx68DdiO9FrF2icDbGy8e9Ga7VMAlxW2/6n1/w/wYwJV9zf/oADsA6cfNdgcuKtFK99o/L8CuvzKd+/+4DqoFHsfd/o2PGuv7QcffNAq/gPTNgYb8rscWwAAAABJRU5ErkJggg==',
                            ];
                        }
                    }
                }
                //echo "<pre>";
                //var_dump($return_data);die;
                $msg = "Success";
            }
            $this->response->body(json_encode(['status' => 'success', 'msg' => $msg, 'data' => $return_data]));
        } else {
            $this->response->body(json_encode(['status' => 'error', 'msg' => 'API_KEY not match']));
        }
        $this->response->type('application/json');
        return $this->response;
    }

    protected function getUsernameByEmployeeId($employee_id)
    {
        $users_table = TableRegistry::get('Users');
        return $users_table->find()->where(['employee_record_id' => $employee_id])->first()['username'];
    }

    protected function getSignatureByUsernameAndDate($employee_id, $signature_date)
    {
        $signature_date = date("Y-m-d H:i:s", strtotime($signature_date));
        $users_table = TableRegistry::get('Users');
        $username = $users_table->find()->where(['employee_record_id' => $employee_id])->first()['username'];

        $host = explode('content/', FILE_FOLDER)[0];
        $token = sGenerateToken(['file' => $username], ['exp' => time() + 60 * 300]);
        $url = $host . "getSignature/" . $username . "/1/" . $signature_date . "?token=" . $token;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_NOBODY, FALSE); // remove body
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $output = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        return $output;
    }

    public function indexNew()
    {
        $url = "http://nothi.tappware.com/postApiNothiInfo/11/62/API_KEY";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_NOBODY, FALSE); // remove body
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $nothis = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        pr($nothis);
        $this->set(compact('nothis'));
    }

    public function bulkNothiNumberUpdate()
    {

    }

    public function changeNothiNumberAccordingNothiDigitalCode()
    {
        $office_id = isset($this->request->data['office_id']) ? $this->request->data['office_id'] : 0;
        $nothi_master_id = isset($this->request->data['nothi_id']) ? $this->request->data['nothi_id'] : 0;
        $response = [
            'status' => 'error',
            'message' => 'Something went wrong.'
        ];
        if (empty($office_id) || empty($nothi_master_id)) {
            $response['message'] = 'সকল তথ্য সঠিকভাবে দেওয়া হয়নি।';
            goto rtn;
        }
        try {
            if (($office_info = Cache::read('get_office_digital_nothi_code_' . $office_id, 'memcached')) === false) {
                $office_info = TableRegistry::get('Offices')->getAll(['id' => $office_id], ['digital_nothi_code'])->first();
                Cache::write('get_office_digital_nothi_code_' . $office_id, $office_info, 'memcached');
            }
            if (!empty($office_info['digital_nothi_code'])) {
                $digital_nothi_code = $office_info['digital_nothi_code'];
            } else {
                $response['message'] = 'অফিসের ৮ ডিজিটের ডিজিটাল নথি কোড দেওয়া হয়নি।';
                goto rtn;
            }
            $this->switchOffice($office_id, 'OfficeDB');
            TableRegistry::remove('NothiMasters');
            TableRegistry::remove('NothiParts');
            $tbl_nothi_master = TableRegistry::get('NothiMasters');
            $tbl_nothi_parts = TableRegistry::get('NothiParts');
            $nothi_data = $tbl_nothi_master->get($nothi_master_id);
            if (!empty($nothi_data)) {
                $current_nothi_no = bntoen($nothi_data->nothi_no);
                if (mb_strlen($current_nothi_no) != 24) {
                    $response['message'] = 'দুঃখিত! নথি নম্বর সঠিক নয়। নথি নম্বর ১৮ ডিজিটের হতে হবে।';
                    goto rtn;
                }
                $first_10_digit = substr($current_nothi_no, 0, 10);
                $new_nothi_number = str_replace($first_10_digit, $digital_nothi_code, $current_nothi_no);
                $unique_nothi_check = $tbl_nothi_master->find()->where(['nothi_no' => enTobn($new_nothi_number)])->first();
                if ($unique_nothi_check) {
                    $response['message'] = 'নথি নম্বরটি (' . enTobn($new_nothi_number) . ') পূর্বে ব্যবহৃত হয়েছে।';
                    goto rtn;
                }

                if (($typesInfo = Cache::read('nothi_type_info_' . $nothi_data->nothi_types_id, 'memcached')) === false) {
                    $typesInfo = TableRegistry::get('NothiTypes')->get($nothi_data->nothi_types_id);
                    Cache::write('nothi_type_info_' . $nothi_data->nothi_types_id, $typesInfo, 'memcached');
                }

                if (($unitsInfo = Cache::read('unit_info_' . $nothi_data->office_units_id, 'memcached')) === false) {
                    $unitsInfo = TableRegistry::get('OfficeUnits')->getBanglaName($nothi_data->office_units_id);
                    Cache::write('unit_info_' . $nothi_data->office_units_id, $unitsInfo, 'memcached');
                }


                //saveHistoryRecord
                $nothi_data_change_history_table = TableRegistry::get('NothiDataChangeHistory');
                $nothi_data_change_history['nothi_master_id'] = $nothi_data->id;
                $nothi_data_change_history['nothi_data'] = serialize(['office_units_id' => $nothi_data->office_units_id, 'nothi_types_id' => $nothi_data->nothi_types_id, 'nothi_no' => $nothi_data->nothi_no, 'subject' => $nothi_data->subject, 'nothi_class' => $nothi_data->nothi_class, 'type_name' => !empty($typesInfo->type_name) ? $typesInfo->type_name : '', 'unit_name' => !empty($unitsInfo['unit_name_bng']) ? $unitsInfo['unit_name_bng'] : '']);
                $is_data_history_saved = $nothi_data_change_history_table->saveRecord($nothi_data_change_history);
                if ($is_data_history_saved['status'] == 'error') {
                    $response = $is_data_history_saved;
                    goto rtn;
                }
                // time to change Nothi Number
                $tbl_nothi_master->updateAll(['nothi_no' => enTobn($new_nothi_number), 'modified_by' => $this->Auth->user()['id']], ['id' => $nothi_data->id]);
                $tbl_nothi_parts->updateAll(['nothi_no' => enTobn($new_nothi_number)], ['nothi_masters_id' => $nothi_data->id]);
                $response = [
                    'status' => 'success',
                    'message' => 'নথি নাম্বার পরিবর্তন হয়েছে। নতুন নথি নম্বরঃ ' . enTobn($new_nothi_number),
                ];
            } else {
                $response['message'] = 'নথি সম্পর্কিত কোন তথ্য পাওয়া যায়নি।';
                goto rtn;
            }
        } catch (\exception $ex) {
            $response['msg'] = 'টেকনিক্যাল ত্রুটি হয়েছে।';
            $response['reason'] = $this->makeEncryptedData($ex->getMessage());
        }
        rtn:
        $this->response->type('application/json');
        $this->response->body(json_encode($response));
        return $this->response;
    }

    public function updatePotrojariBody()
    {
        try{
            $response = [
                'status' => 'error',
                'message' => 'Something went wrong.'
            ];
            $body = $this->request->data['body'];
            $potrojari_id = $this->request->data['potrojari_id'];
            $nothi_office = $this->request->data['nothi_office'];
            $nothi_part_no = $this->request->data['nothi_part_no'];
            if (empty($body) || empty($potrojari_id)) {
                $response = ['message' => __('Required info not given')];
                goto rtn;
            }

            $employee_office = $this->getCurrentDakSection();
            if ($nothi_office != $employee_office['office_id']) {
                $this->switchOffice($nothi_office, 'OtherOffice');
            }

            TableRegistry::remove("Potrojari");
            $potrojariTable = TableRegistry::get("Potrojari");
            TableRegistry::remove('PotrojariAttachments');
            $potrojariAttachmentsTable = TableRegistry::get("PotrojariAttachments");
            TableRegistry::remove('PotrojariReceiver');
            $potrojariPotrojariReceiver = TableRegistry::get("PotrojariReceiver");

            TableRegistry::remove('NothiMasterPermissions');
            TableRegistry::remove('NothiMasterCurrentUsers');
            $nothiMasterPermissionsTable  = TableRegistry::get("NothiMasterPermissions");
            $nothiMasterCurrentUsersTable = TableRegistry::get("NothiMasterCurrentUsers");

            if (!empty($employee_office['office_id'])) {
                $potrojari = $potrojariTable->get($potrojari_id);

                if(!empty($potrojari)){
                    $nothi_office = $potrojari['potrojari_draft_office_id'];
                    $nothi_master_id = $potrojari['nothi_master_id'];
                    $nothi_part_no = $potrojari['nothi_part_no'];
                }else{
                    $response = ['message' => __('Required info missing')];
                    goto rtn;
                }

                $nothiMasters = $nothiMasterPermissionsTable->hasAccess($nothi_office, $employee_office['office_id'], $employee_office['office_unit_id'],
                    $employee_office['office_unit_organogram_id'], $nothi_master_id, $nothi_part_no);

                $nothiMastersCurrentUser = $nothiMasterCurrentUsersTable->find()
                                                                        ->where(['nothi_part_no' => $nothi_part_no,
                                                                                 'office_id' => $employee_office['office_id'],
                                                                                 'office_unit_id' => $employee_office['office_unit_id'],
                                                                                 'office_unit_organogram_id' =>$employee_office['office_unit_organogram_id'],
                                                                                 'nothi_office' => $nothi_office])
                                                                        ->first();

                if (empty($nothiMasters) || $nothiMasters['privilige_type'] == 0) {
                    $response = ['message' => __('Required info missing')];
                    goto rtn;
                }
                if(empty($nothiMastersCurrentUser)){
                    $response = ['message' => __('Required info missing')];
                    goto rtn;
                }
            }

            //if CS then need to update potro_cover
            if($potrojari->potro_type == 30){
                $potrojari->potro_cover = $body;
            }else{
                $potrojari->potro_description = $body;
            }
            $potrojariTable->save($potrojari);
            $potrojariAttachmentsTable->updateAll(['content_body' => $body], ['potrojari_id' => $potrojari_id]);
            $response = [
                'status' => 'success',
                'message' => __('Data successfully updated')
            ];
        }catch (\Exception $ex){
            $response = ['message' => __('Technical error happen')];
            goto rtn;
        }


        rtn:
        $this->response->type('application/json');
        $this->response->body(json_encode($response));
        return $this->response;
    }

    public function potroDecisionSave($nothi_office, $nothi_part_id){
        $this->layout = null;
        $response = [];
        if($this->request->is('post')) {
            try {

                $employee_office = $this->getCurrentDakSection();
                if (empty($employee_office['office_unit_organogram_id'])) {
                    $user_designation = !empty($this->request->data['user_designation']) ? $this->request->data['user_designation'] : $employee_office['office_unit_organogram_id'];
                    if (empty($user_designation)) {
                        throw new \Exception(__("Invalid request"));
                    }

                    $apikey = isset($this->request->data['api_key']) ? $this->request->data['api_key'] : '';
                    if (empty($apikey) || $this->checkToken($apikey) == FALSE) {
                        throw new \Exception(__("Invalid request"));
                    }

                    $employee_office = $this->setCurrentDakSection($user_designation);
                }

                $request = $this->request->data;
                if (empty($request['potro_id'])) {
                    throw new \Exception(__("দুঃখিত! পত্র সম্পর্কিত কোন তথ্য পাওয়া যায়নি।এরর কোডঃ ১"));
                }

                $otherNothi = false;
                if ($nothi_office != 0 && $nothi_office != $employee_office['office_id']) {
                    $otherNothi = true;
                    $this->switchOffice($nothi_office, 'MainNothiOffice');
                } else {
                    $nothi_office = $employee_office['office_id'];
                }

                if(isset($this->request->data['decision_answer'])){
                    $answers = intval($this->request->data['decision_answer']);
                }
                $nothiMasterPermissionsTable = TableRegistry::get("NothiMasterPermissions");
                $potroAttachmentTable = TableRegistry::get('NothiPotroAttachments');
                $permitted_nothi_list = $nothiMasterPermissionsTable->hasAccessPartList($employee_office['office_id'], $employee_office['office_unit_id'], $employee_office['office_unit_organogram_id'], $nothi_part_id, $nothi_office);

                if (empty($permitted_nothi_list)) {
                    throw new \Exception(__("দুঃখিত! নথিতে অনুমতি দেওয়া হয়নি।"));
                }
                $potroAttachmentRecord = $potroAttachmentTable->find()
                    ->contain(['NothiPotros'])
                    ->where(['NothiPotroAttachments.nothi_part_no' => $nothi_part_id, 'NothiPotros.id' => $request['potro_id']])->first();

                if (empty($potroAttachmentRecord)) {
                    throw new \Exception(__("দুঃখিত! পত্র সম্পর্কিত কোন তথ্য পাওয়া যায়নি ।এরর কোডঃ ২"));
                }

                $potro = $potroAttachmentRecord['nothi_potro'];
                $potro_meta = jsonA($potro['application_meta_data']);

                if(isset($answers)){
                    $potro_meta['decision_answer'] = $answers;
                    $potro_meta['decision_desk'] = $employee_office['office_unit_organogram_id'];
                }

                if(isset($this->request->data['comment'])){
                    $this->request->data['comment'] = trim($this->request->data['comment']);
                    $comment = h($this->request->data['comment']);
                    $potro_meta['decision_note'] = $comment;
                }

                $presentation_date = !empty($this->request->data['presentation_date'])?$this->request->data['presentation_date']:(!empty($potro_meta['presentation_date'])?$potro_meta['presentation_date']:'');

                if(!empty($presentation_date)) {
                    $potro_meta['presentation_date'] = $presentation_date;
                }
                $certificate_url = !empty($this->request->data['certificate_url'])?$this->request->data['certificate_url']:(!empty($potro_meta['certificate_url'])?$potro_meta['certificate_url']:'');

                if(!empty($certificate_url)) {
                    $domain_name = $this->getMainDomainUrl();
                    if(stripos($certificate_url,$domain_name) == FALSE){
                        $certificate_url = $domain_name.$certificate_url;
                        $certificate_url = str_replace('//getContent','/getContent',$certificate_url);
                        $potro_meta['certificate_url'] = $certificate_url;
                    }
                }



                if(!empty($potro_meta['action_decisions']) && !empty($answers)){
                    $actionsdecision = $potro_meta['action_decisions'];
                    if(!empty($actionsdecision['completable']) && in_array($answers,$actionsdecision['completable'])){
                        $potro_meta['locked'] = 1;
                        if($potro_meta['api_client'] == 'BTRC') {
                            $potro_meta['sdesk'] = $employee_office['office_unit_organogram_id'];
                        }
                    }
                }
                if(!empty($potro_meta['api_client']) && (strtolower($potro_meta['api_client']) == 'btrc')){
                    //create API URL so that BTRC real certificate catch
                    $btrc_certificate_api_url = \Cake\Routing\Router::url(['controller' => "ApiManagement",
                                                                           'action' => "getOtherPartyCertificate",'?' => ['nothi_office' => $nothi_office,'potro_id'=>$request['potro_id'],'nothi_part_id' =>$nothi_part_id]], true);
                    $nothi_save_other_party_certificate_link = \Cake\Routing\Router::url(['controller' => "ApiManagement",
                        'action' => "saveOtherPartyCertificateFile",'?' => ['nothi_office' => $nothi_office,'potro_id'=>$request['potro_id'],'nothi_part_id' =>$nothi_part_id,'api_client'=>$potro_meta['api_client']]], true);

                    $potro_meta['nothi_certificate_link']=$btrc_certificate_api_url;
                    $potro_meta['nothi_save_other_party_certificate_link']= $nothi_save_other_party_certificate_link;
                }




                $potro['application_meta_data'] = json_encode($potro_meta);

                if(!$potroAttachmentTable->NothiPotros->save($potro)){
                    $response = ['status' => 'error', 'message' => 'Can not save required data.Code 4'];
                    goto rtn;
                }

                $response = ['status' => 'success', 'message' => __("সিদ্ধান্ত সংরক্ষণ করা হয়েছে")];

                if(!empty($certificate_url)){
                    if(!empty($potro_meta['token_url'])){
                        if(!empty($potro_meta['api_client']) && ($potro_meta['api_client'] == 'BTRC')){
                            $auth_token_response = $this->NotifiyFeedbackAuthTokenGeneration($potro_meta['token_url'],['user' => 'A2i','password' => '123456']);
                            if(!empty($auth_token_response['status']) && $auth_token_response['status'] == 'success'){
                                if(!empty($auth_token_response['token'])){
                                    $potro_meta['token'] = $auth_token_response['token'];
                                }
                            }
                        }
                    }
                    if(!empty($potro_meta['api_client']) && ($potro_meta['api_client'] == 'BTRC')){
                        //BTRC clients wants sdesk signature
                        if(!empty($potro_meta['sdesk'])){
                            $employeeOfficesTable = TableRegistry::get('EmployeeOffices');
                            $signDeskData =$employeeOfficesTable->find()->select(['employee_record_id'])->where(['status'=>1,'office_unit_organogram_id'=>$potro_meta['sdesk']])->first();
                            if(!empty($signDeskData)){
                                $usersTable = TableRegistry::get('Users');
                                $userData = $usersTable->find()->select(['username'])->where(['employee_record_id'=>$signDeskData['employee_record_id']])->hydrate(false)->first();
                                if(!empty($userData)){
                                    $sdesk_sign = $this->getSignature($userData['username'],1,1,false,null,['token' =>sGenerateToken(['file'=>$userData['username']],['exp'=>time() + 60*300])]);
                                    if(!empty($sdesk_sign)){
                                        $potro_meta['sdesk_sign'] = $sdesk_sign;
                                    }
                                }
                            }
                        }

                        $potro_meta['nothi_certificate_link']=$btrc_certificate_api_url;
                    }
                    $feedbackdata = $potro_meta + [
                            'api_key' => !empty($potro_meta['feedback_api_key']) ? $potro_meta['feedback_api_key'] : '',
                            'aid' => $potro_meta['tracking_id'],
                            'current_desk_id' => $employee_office['office_unit_organogram_id'],
                            'data'=>$potro['potro_content'],
                            'nothi_id' => $potro['nothi_master_id'],
                            'note_id' => $potro['nothi_part_no'],
                            'potro_id' => $potro['id'],
                        ];
                    $this->notifyFeedback($potro_meta['feedback_url'], $feedbackdata);
                }

            } catch (\Exception $ex) {
                $response = ['status' => 'error', 'message' => $ex->getMessage()];
            }
        }
        rtn:
        $this->response->body(json_encode($response));
        $this->response->type('json');
        return $this->response;
    }

    public function updateNothiNo() {
    	$new_nothi_no = json_decode($this->generateNothiNumber()->body());
    	$nothi_master_id = $this->request->data['nothi_master_id'];

    	try {
//            $role_id = $this->Auth->user('user_role_id');
//            if (($role_id == 1 || $role_id == 2) && !empty($this->request->data['officeId'])) {
//                $this->switchOffice($this->request->data['office_id'], 'OfficeDB');
//            }
    	    TableRegistry::remove('NothiMasters');
    	    TableRegistry::remove('NothiParts');

			$nothiMastersTable = TableRegistry::get('NothiMasters');
			$nothiMastersTable->updateAll(['nothi_no' => $new_nothi_no, 'office_units_id' => $this->request->data['office_unit_id']], ['id' => $nothi_master_id]);

			$nothiPartsTable = TableRegistry::get('NothiParts');
			$nothiPartsTable->updateAll(['nothi_no' => $new_nothi_no, 'office_units_id' => $this->request->data['office_unit_id']], ['nothi_masters_id' => $nothi_master_id]);
		} catch(\Exception $exception) {
    		$response = [
    			'status' => 'error',
				'msg' => 'Exception Error'
			];
    		return $response;
		}
		$response = [
			'status' => 'success',
			'msg' => 'Successfully updated nothi number',
			'data' => [
				'nothi_no' => $new_nothi_no
			]
		];
		$this->response->type('application/json');
		$this->response->body(json_encode($response));
		return $this->response;
	}


    public function updateNothiNoByOffice() {
        $new_nothi_no = json_decode($this->generateNothiNumberByOffice()->body());
        echo $new_nothi_no;die;
        $nothi_master_id = $this->request->data['nothi_master_id'];

        try {
//            $role_id = $this->Auth->user('user_role_id');
//            if (($role_id == 1 || $role_id == 2) && !empty($this->request->data['officeId'])) {
//                $this->switchOffice($this->request->data['office_id'], 'OfficeDB');
//            }
            TableRegistry::remove('NothiMasters');
            TableRegistry::remove('NothiParts');

            $nothiMastersTable = TableRegistry::get('NothiMasters');
            $nothiMastersTable->updateAll(['nothi_no' => $new_nothi_no, 'office_id' => intval($this->request->data['office_id'])], ['id' => $nothi_master_id]);

            $nothiPartsTable = TableRegistry::get('NothiParts');
            $nothiPartsTable->updateAll(['nothi_no' => $new_nothi_no, 'office_id' => intval($this->request->data['office_id'])], ['nothi_masters_id' => $nothi_master_id]);
        } catch(\Exception $exception) {
            $response = [
                'status' => 'error',
                'msg' => 'Exception Error'
            ];
            return $response;
        }
        $response = [
            'status' => 'success',
            'msg' => 'Successfully updated nothi number',
            'data' => [
                'nothi_no' => $new_nothi_no
            ]
        ];
        $this->response->type('application/json');
        $this->response->body(json_encode($response));
        return $this->response;
    }

    public function generateNothiNumberByOffice()
    {
        $officeid = intval($this->request->data['officeId']);
        $officetrasnferid = intval($this->request->data['office_id']);
        $this->switchOffice($officeid, 'OfficeDB');

        $nothitypeid = intval($this->request->data('id'));

        $officeTable = TableRegistry::get('Offices');
        $officeUnitTable = TableRegistry::get('OfficeUnits');
        TableRegistry::remove('NothiTypes');
        $nothiTypesTable = TableRegistry::get('NothiTypes');
        TableRegistry::remove('NothiParts');
        $nothiPartsTable = TableRegistry::get('NothiParts');
        TableRegistry::remove('NothiMasters');
        $nothiMastersTable = TableRegistry::get('NothiMasters');

        $office_nothi_code = $officeTable->find()->select(['digital_nothi_code'])->where(['id' => $officetrasnferid])->first();

        $query = $nothiTypesTable->find()->select(['type_code', 'type_last_number','office_unit_id'])->where(['id' => $nothitypeid])->first();

        $office_unit_id = $query['office_unit_id'];
        $unittabledata = $officeUnitTable->find()->select(['unit_nothi_code'])->where(['id' => $office_unit_id])->first();

        $unit_code = $unittabledata['unit_nothi_code'];

        $thisyear = date("Y");

        $query3 = $nothiMastersTable->find()->select(['nothi_no'])->where(['nothi_types_id' => $nothitypeid,
            'office_id' => $officeid, 'office_units_id' => $office_unit_id,
            'nothi_created_date >= ' => "{$thisyear}-01-01", 'nothi_created_date <= ' => "{$thisyear}-12-31"])->order(['id' => 'desc'])->first();

        $query3Data = [];
        if ($query3['nothi_no']) {
            $query3Data = explode('.', $query3['nothi_no']);
        }

        $nothiSerial = 0;

        if (!empty($query3Data) && count($query3Data) >= 7 && !empty($query3Data[5])) {
            $nothiSerial = (bnToen($query3Data[5]) + 1);
            $nothiSerial = str_pad(($nothiSerial), 3, "0", STR_PAD_LEFT);
        } else {
            $query4 = $nothiPartsTable->find()->select(['counttotal' => 'count(nothi_types_id)'])->where(['nothi_types_id' => $nothitypeid,
                'office_id' => $officeid, 'office_units_id' => $office_unit_id,
                'nothi_created_date >= ' => "{$thisyear}-01-01", 'nothi_created_date <= ' => "{$thisyear}-12-31"])->first();

            $bn_digits = array('০', '১', '২', '৩', '৪', '৫', '৬', '৭', '৮', '৯');
            $nothiSerial = str_replace(range(0, 9), $bn_digits,
                (str_pad(($query4['counttotal'] + $query['type_last_number'] + 1),
                    3, "0", STR_PAD_LEFT)));
        }

        if($nothiSerial > 999){
            $nothiSerial = str_pad(1,
                3, "0", STR_PAD_LEFT);
        }

        $nothi_code = $office_nothi_code['digital_nothi_code']."." . $unit_code."." . str_pad(bnToen($query['type_code']), 2, "0", STR_PAD_LEFT) . "." . $nothiSerial . "." . Number::format(substr($thisyear,
                -2));

        while($nothiMastersTable->find()->where(['nothi_no' => enTobn($nothi_code)])->count() > 0){
            if($nothiSerial > 999){
                break;
            }
            $nothiSerial = str_pad(($nothiSerial + 1),
                3, "0", STR_PAD_LEFT);

            $nothi_code = $office_nothi_code['digital_nothi_code']."." . $unit_code."." . str_pad(bnToen($query['type_code']), 2, "0", STR_PAD_LEFT) . "." . $nothiSerial . "." . Number::format(substr($thisyear,
                    -2));
        }


        $this->response->body(json_encode(enTobn($nothi_code)));
        $this->response->type('application/json');
        return $this->response;
    }

    public function permitted_nothi() {
    	$current_user = $this->getCurrentDakSection();
		$other_organogram_activities_settingsTable = TableRegistry::get('OtherOrganogramActivitiesSettings');
		$other_organogram_activities_settings = $other_organogram_activities_settingsTable->find()->where(['assigned_organogram_id' => $current_user['office_unit_organogram_id'], 'status' => 1, 'permission_for IN' => [0,2]])->toArray();

		$other_organogram_activities_setting = [];
		if (isset($this->request->data()['organogram_id']) && $this->request->data('organogram_id') > 0) {
			$other_organogram_activities_setting = $other_organogram_activities_settingsTable->find()->where([
				'assigned_organogram_id' => $current_user['office_unit_organogram_id'],
				'status' => 1,
				'permission_for IN' => [0, 1],
				'organogram_id' => $this->request->data('organogram_id')
			])->toArray();
		}

		$listtype = 'inbox';
		$this->set(compact('other_organogram_activities_settings', 'other_organogram_activities_setting', 'listtype'));

	}


	public function getNPdomain(){
        $employee_office = $this->getCurrentDakSection();

        $OfficesTable = TableRegistry::get('Offices');
        $office_info = $OfficesTable->get($employee_office['office_id']);

        $OfficeLayersTable = TableRegistry::get('OfficeLayers');
        $office_layer_info = $OfficeLayersTable->get($office_info['office_layer_id']);
        $layer_level = $office_layer_info['layer_level'];
        $OfficeDomainTable = TableRegistry::get('OfficeDomains');
        $office_domain = $OfficeDomainTable->getApiPortalDomainPrefixbyOffice($employee_office['office_id']);

        try {
            //share with curl
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://portal.gov.bd/api/v1/domain-by-prefix/'.$office_domain.'?api_key=7NCnGnrYAAjrz1yScbZL');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $errno = curl_errno($ch);
            if(!empty($errno)) {
                $error_message = curl_strerror($errno);
                $error = $errno.' : '.$error_message;

                $this->response->body(json_encode(['status'=>'error','msg'=>'Curl error. '.$error]));
                $this->response->type('application/json');
                return $this->response;

            }
            $result = curl_exec($ch);
            curl_close($ch);
            //end
            $result = json_decode($result,true);

            if(isset($result['_meta']['status']) && $result['_meta']['status']== 'SUCCESS' && !empty($result['records'][0]['subdomain'])) {
                if($layer_level ==1 || $layer_level ==2 || $layer_level ==3){
                    $data = $this->apiPortalContentType($result['records'][0]['subdomain']);
                    $this->response->body($data);
                    $this->response->type('application/json');
                    return $this->response;

                } else {
                    $this->response->body(json_encode(['status'=>'success','subdomain'=>$result['records'][0]['subdomain']]));
                    $this->response->type('application/json');
                    return $this->response;
                }

            } else {
                $this->response->body(json_encode(['status'=>'error','msg'=>'response status error or response empty subdomain']));
                $this->response->type('application/json');
                return $this->response;
            }


        } catch (\Exception $ex) {
            $this->response->body(json_encode(['status'=>'error','msg'=>'Exception: '.$ex]));
            $this->response->type('application/json');
            return $this->response;

        }
    }

    protected function apiPortalContentType($subdomain){
        try {
            //share with curl
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $subdomain.'/api/v1/content-type?api_key=7NCnGnrYAAjrz1yScbZL');
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            $errno = curl_errno($curl);
            if(!empty($errno)) {
                $error_message = curl_strerror($errno);
                $error = $errno.' : '.$error_message;
                return(json_encode(['status'=>'error','msg'=>'content-type api, curl error. '.$error]));
            }
            $result = curl_exec($curl);
            curl_close($curl);
            //end
            $result = json_decode($result,true);
            if(isset($result['_meta']['status']) && $result['_meta']['status']== 'SUCCESS') {
                $content_type=[];
                $i=0;
                foreach($result['records'] as $record){
                    if(in_array($record['name'],json_decode(PORTAL_PUBLISH_TYPE,true))){
                        $content_type[$i]['value'] = $record['name'];
                        $content_type[$i]['name'] = $record['human_name'];
                        $i++;
                    }
                }
                return json_encode(['status'=>'success','subdomain'=>$subdomain,'content_type'=>$content_type]);
            } else {
                return(json_encode(['status'=>'error','msg'=>'content-type api, response error.']));
            }
        } catch (\Exception $ex) {
            return(json_encode(['status'=>'error','msg'=>'content-type api, Exception error. '.$ex]));
        }
    }

    public function apiPortalContentTypeStructure()
    {

        $subdomain = $this->request->data['subdomain'];

        if (empty($subdomain)) {
            $this->response->body(json_encode(['status' => 'error', 'msg' => 'Empty Subdomain']));
            $this->response->type('application/json');
            return $this->response;
        }
        $content = $this->request->data['content'];
        if (empty($content)) {
            $this->response->body(json_encode(['status' => 'error', 'msg' => 'Empty Content']));
            $this->response->type('application/json');
            return $this->response;
        }

        $employee_office = $this->getCurrentDakSection();

        $OfficesTable = TableRegistry::get('Offices');
        $office_info = $OfficesTable->get($employee_office['office_id']);

        $OfficeLayersTable = TableRegistry::get('OfficeLayers');
        $office_layer_info = $OfficeLayersTable->get($office_info['office_layer_id']);
        $layer_level = $office_layer_info['layer_level'];

        if (($layer_level != 1) && ($layer_level != 2) && ($layer_level != 3)) {
            $this->response->body(json_encode(['status' => 'error', 'msg' => 'Invalid Layer Level']));
            $this->response->type('application/json');
            return $this->response;
        }

        try {
            //share with curl
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $subdomain . '/api/v1/content-type/' . $content . '/structure?api_key=7NCnGnrYAAjrz1yScbZL');
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            $errno = curl_errno($curl);
            if (!empty($errno)) {
                $error_message = curl_strerror($errno);
                $error = $errno . ' : ' . $error_message;
                $this->response->body(json_encode(['status' => 'error', 'msg' => 'Curl error. ' . $error]));
                $this->response->type('application/json');
                return $this->response;
            }
            $result = curl_exec($curl);
            curl_close($curl);
            //end
            $result = json_decode($result, true);
            if (isset($result['_meta']['status']) && $result['_meta']['status'] == 'SUCCESS') {
                $content_type_settings = [];
                foreach ($result['_meta']['defs']['flds'] as $record) {
                    if ($record['type'] == 'lookuptbl') {
                        $i = 0;
                        foreach ($record['lookup_data'] as $item) {
                            $content_type_settings[$i]['value'] = $item['id'];
                            $content_type_settings[$i]['name'] = $item['title'];
                            $i++;
                        }

                    }
                }
                if (!empty($content_type_settings)) {
                    $this->response->body(json_encode(['status' => 'success', 'content_type_settings' => $content_type_settings]));
                    $this->response->type('application/json');
                    return $this->response;
                } else {
                    $this->response->body(json_encode(['status' => 'error', 'msg' => 'No data found in api']));
                    $this->response->type('application/json');
                    return $this->response;
                }

            } else {
                $this->response->body(json_encode(['status' => 'error', 'msg' => 'Api response error.']));
                $this->response->type('application/json');
                return $this->response;
            }
        } catch (\Exception $ex) {
            $this->response->body(json_encode(['status' => 'error', 'msg' => 'Api exception error. ' . $ex]));
            $this->response->type('application/json');
            return $this->response;
        }
    }

    public function loadNextParts() {
        $permitted_nothi_list = $this->request->data['permitted_nothi_list'];
        $permitted_nothi_list = explode(',', $permitted_nothi_list);
        $loaded_parts = $this->request->data['loaded_parts'];

        TableRegistry::remove('NothiParts');
        $nothiPartsTable = TableRegistry::get('NothiParts');
        $allNothiParts = $nothiPartsTable->find()->select(['id', 'office_id', 'nothi_part_no',
            'nothi_part_no_bn', 'NothiNotes.subject'])
            ->join([
                'NothiNotes' => [
                    'table' => 'nothi_notes',
                    'type' => 'left',
                    'conditions' => "NothiNotes.nothi_part_no = NothiParts.id AND NothiNotes.note_no=0"
                ]
            ])
            ->where(['NothiParts.id IN' => array_values($permitted_nothi_list)])->order(['NothiParts.nothi_part_no  ASC']);
        $total_part = $allNothiParts->count();
        $allNothiParts = $allNothiParts->limit($this->load_part_at_a_time)->offset($loaded_parts)->toArray();

        $this->response->type('application/json');
        $this->response->body(json_encode($allNothiParts));
        return $this->response;
    }

    public function searchNote() {
        $search_key = $this->request->data['search_key'];
        $nothi_master_id = $this->request->data['nothi_master_id'];

        $permitted_nothi_list = $this->request->data['permitted_nothi_list'];
        $permitted_nothi_list = explode(',', $permitted_nothi_list);

        TableRegistry::remove('NothiParts');
        $nothiPartsTable = TableRegistry::get('NothiParts');
        $allNothiParts = $nothiPartsTable->find()->select(['id', 'office_id', 'nothi_part_no', 'nothi_part_no_bn', 'NothiNotes.subject'])
            ->join([
                'NothiNotes' => [
                    'table' => 'nothi_notes',
                    'type' => 'left',
                    'conditions' => "NothiNotes.nothi_part_no = NothiParts.id AND NothiNotes.note_no=0"
                ]
            ])
            ->where([
                'NothiParts.id IN' => array_values($permitted_nothi_list),
                'NothiParts.nothi_masters_id' => $nothi_master_id,

                'OR' => [
                    'NothiNotes.subject LIKE' => '%'.$search_key.'%',
                    /*'NothiParts.nothi_no LIKE' => '%'.$search_key.'%',*/
                    'NothiParts.nothi_part_no_bn LIKE' => '%'.$search_key.'%'
                ]

            ])->order(['NothiParts.nothi_part_no  ASC'])->toArray();

        echo '<table class="table table-bordered table-striped"><thead><td>নোট নং</td><td>নোটের বিষয়</td><td>বিস্তারিত</td></thead><tbody>';
        foreach ($allNothiParts as $allNothiPart) {
            echo '<tr>';
            echo '<td>'.$allNothiPart['nothi_part_no_bn'].'</td><td>'.$allNothiPart['NothiNotes']['subject'].'</td><td><a href="'. $this->request->webroot . 'noteDetail/' . $allNothiPart['id'] .'" class="btn btn-success btn-sm">বিস্তারিত</a></td>';
            echo '</tr>';
        }
        echo '</tbody></table>';

        die;
    }

    public function searchNothiSubject() {
        $search_key = $this->request->data['search_key'];
        $list_type = $this->request->data['list_type'];

        $current_session = $this->getCurrentDakSection();
        $nothiMasterCurrentUserTable = TableRegistry::get('NothiMasterCurrentUsers');
        $nothiMasterTable = TableRegistry::get('NothiMasters');
        $nothiMasterPermissionsTable = TableRegistry::get('NothiMasterPermissions');
        $nothiMasterMovementsTable = TableRegistry::get('NothiMasterMovements');
        $otherOfficeNothiMasterMovementsTable = TableRegistry::get('OtherOfficeNothiMasterMovements');
        $nothiNotesTable = TableRegistry::get('NothiNotes');

        if ($list_type == 'inbox') {
            $nothiMasterCurrentUser = $nothiMasterCurrentUserTable->find('list', ['keyField' => 'nothi_part_no', 'valueField' => 'nothi_master_id'])->where(['nothi_office' => $current_session['office_id'], 'office_unit_organogram_id' => $current_session['office_unit_organogram_id'], 'is_finished' => 0])->group(['nothi_master_id'])->toArray();

            foreach ($nothiMasterCurrentUser as $part_no => $master_id) {
                $nothiNotes = $nothiNotesTable->find()->where(['nothi_part_no' => $part_no])->first();
                if (!$nothiNotes) {
                    unset($nothiMasterCurrentUser[$part_no]);
                }
            }

            $nothiMasters = $nothiMasterTable->find()->select(['subject'])->where(['id IN' => $nothiMasterCurrentUser, 'subject like' => '%'.$search_key.'%'])->limit(50)->toArray();
            $json_data = $nothiMasters;
        } elseif ($list_type == 'sent') {
            $nothiMasterPermissions = $nothiMasterPermissionsTable->find('list', ['keyField' => 'nothi_masters_id', 'valueField' => 'nothi_masters_id'])->where(['nothi_office' => $current_session['office_id'], 'office_unit_organograms_id' => $current_session['office_unit_organogram_id']])->group(['nothi_masters_id'])->toArray();

            $nothiMasterMovements = $nothiMasterMovementsTable->find('list', ['keyField' => 'nothi_master_id', 'valueField' => 'nothi_master_id'])->where(['from_officer_designation_id' => $current_session['office_unit_organogram_id'], 'nothi_master_id IN' => $nothiMasterPermissions])->group(['nothi_master_id'])->toArray();

            $nothiMasters = $nothiMasterTable->find()->select(['subject'])->where(['id IN' => $nothiMasterMovements, 'subject like' => '%'.$search_key.'%'])->limit(50)->toArray();
            $json_data = $nothiMasters;
        } elseif ($list_type == 'other_sent') {
            $nothiMasterPermissions = $nothiMasterPermissionsTable->find('list', ['keyField' => 'nothi_masters_id', 'valueField' => 'nothi_office'])->where(['nothi_office !=' => $current_session['office_id'], 'office_unit_organograms_id' => $current_session['office_unit_organogram_id']])->group(['nothi_masters_id'])->limit(50)->toArray();

            $data = [];
            foreach ($nothiMasterPermissions as $master_id => $office_id) {
                $otherOfficeNothiMasterMovements = $otherOfficeNothiMasterMovementsTable->find()->select(['subject'])->where(['nothi_master_id' => $master_id, 'nothi_office' => $office_id, 'from_officer_designation_id' => $current_session['office_unit_organogram_id'], 'subject like' => '%'.$search_key.'%'])->first();
                if ($otherOfficeNothiMasterMovements) {
                    $data[] = ['subject' => $otherOfficeNothiMasterMovements['subject']];
                }
            }
            $json_data = $data;
        } elseif ($list_type == 'other') {
            $nothiMasterCurrentUser = $nothiMasterCurrentUserTable->find('list', ['keyField' => 'nothi_master_id', 'valueField' => 'nothi_office'])->where(['nothi_office !=' => $current_session['office_id'], 'office_unit_organogram_id' => $current_session['office_unit_organogram_id']])->group(['nothi_master_id'])->limit(50)->toArray();

            $data = [];
            foreach ($nothiMasterCurrentUser as $master_id => $office_id) {
                $otherOfficeNothiMasterMovements = $otherOfficeNothiMasterMovementsTable->find()->select(['subject'])->where(['nothi_master_id' => $master_id, 'nothi_office' => $office_id, 'subject like' => '%'.$search_key.'%'])->first();
                if ($otherOfficeNothiMasterMovements) {
                    $data[] = ['subject' => $otherOfficeNothiMasterMovements['subject']];
                }
            }
            $json_data = $data;
        } elseif ($list_type == 'all') {
            $nothiMasterPermissions = $nothiMasterPermissionsTable->find('list', ['keyField' => 'nothi_masters_id', 'valueField' => 'nothi_masters_id'])->where(['nothi_office' => $current_session['office_id'], 'office_unit_organograms_id' => $current_session['office_unit_organogram_id']])->group(['nothi_masters_id'])->toArray();

            $nothiMasters = $nothiMasterTable->find()->select(['subject'])->where(['id IN' => $nothiMasterPermissions, 'subject like' => '%'.$search_key.'%'])->limit(50)->toArray();
            $json_data = $nothiMasters;
        }

        $this->response->body(json_encode(['status' => 'success', 'data' => $json_data]));
        $this->response->type('application/json');
        return $this->response;
    }

    public function check_folder_permission($path) {
        $permission = is_writable($path);

        $this->response->body(json_encode($permission));
        $this->response->type('application/json');
        return $this->response;
    }
}