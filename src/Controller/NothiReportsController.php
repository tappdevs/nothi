<?php

namespace App\Controller;

use App\Controller\ProjapotiController;
use App\Model\Table;
use Aura\Intl\Exception;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Psr\Log\InvalidArgumentException;
use Cake\I18n\I18n;
use Cake\I18n\Time;
use Cake\I18n\Number;

class NothiReportsController extends ProjapotiController {

    /**
     * Section wise Nothi report
     */
    public function sectionWiseNothiListExcel($type='pdf',$startDate = "", $endDate = "",$dateRangeBn='',$unit_id = '') {
        $result = $this->sectionWiseNothiListContent($startDate, $endDate, $type,20, $unit_id);
        $unitInformation='';
        $DakList = Array();
        $j=0;

        if (!empty($result['data'])) {

            foreach ($result['data'] as $dak) {
                if( $unitInformation ==  $dak['unit_name_bng']){
                    $DakList[$j]['unit_name_bng'] ='';
                }
                else {
                    $DakList[$j]['unit_name_bng'] = h($dak['unit_name_bng']);
                    $unitInformation = $dak['unit_name_bng'];
                }
                $created = new Time($dak['nothi_created_date']);
                $dak['nothi_created_date'] = $created->i18nFormat(null, null, 'bn-BD');

                $DakList[$j]['nothi_class'] = h($result['nothiClass'][$dak['nothi_class']]);
                $DakList[$j]['type_name'] = h($dak['NothiTypes']['type_name']);
                $DakList[$j]['subject'] = h($dak['subject']);
                $DakList[$j]['nothi_no'] = h($dak['nothi_no']);
                $DakList[$j]['nothi_created_date'] = $dak['nothi_created_date'];
                $j++;

            }
        }
        if(empty($DakList)){
            $DakList= Array();
        }
        if($type == 'excel'){
            $this->printExcel($DakList, [
                ['key' => 'unit_name_bng', 'title' => 'শাখা'],
                ['key' => 'nothi_class', 'title' => 'নথির শ্রেণি'],
                ['key' => 'type_name', 'title' => 'নথির ধরন'],
                ['key' => 'subject', 'title' => 'নথির বিষয়'],
                ['key' => 'nothi_no', 'title' => 'নথি নম্বর'],
                ['key' => 'nothi_created_date', 'title' => 'নথি খোলার তারিখ'],
            ],[
                'name'=>((isset($result['office_name']))?$result['office_name'].' অফিসের ':'') .'শাখাভিত্তিক নথিসমূহ' ,
                'title' => [
                    ((isset($result['office_name']))?$result['office_name'].' অফিসের ':'') .'শাখাভিত্তিক নথিসমূহ' ,
                    $dateRangeBn,
                ]
            ]);
        } else {
            $this->printPdf($DakList, [
                ['key' => 'unit_name_bng', 'title' => 'শাখা'],
                ['key' => 'nothi_class', 'title' => 'নথির শ্রেণি'],
                ['key' => 'type_name', 'title' => 'নথির ধরন'],
                ['key' => 'subject', 'title' => 'নথির বিষয়'],
                ['key' => 'nothi_no', 'title' => 'নথি নম্বর'],
                ['key' => 'nothi_created_date', 'title' => 'নথি খোলার তারিখ'],
            ],[
                'name'=>((isset($result['office_name']))?$result['office_name'].' অফিসের ':'') .'শাখাভিত্তিক নথিসমূহ' ,
                'title' => [
                    ((isset($result['office_name']))?$result['office_name'].' অফিসের ':'') .'শাখাভিত্তিক নথিসমূহ' ,
                    $dateRangeBn,
                ]
            ]);
        }

        exit;
    }

    public function sectionWiseNothiList() {
        $session = $this->request->session();
        $employee = $session->read('logged_employee_record');
        $this->set("office", $employee);

        $employee_office = $this->getCurrentDakSection();

        $officeUnitTable = TableRegistry::get("OfficeUnits");

        $units = $officeUnitTable->getOfficeChildInfo($employee_office['office_unit_id']);
        $unitsOwn = $officeUnitTable->getOwnChildInfo($employee_office['office_unit_id']);

        $units = !empty($units[0]) ? $units[0] : '';
        $unitsOwn = !empty($unitsOwn[0]) ? ($employee_office['office_unit_id'] . ',' . $unitsOwn[0]) : $employee_office['office_unit_id'];

        $units = $units . (!empty($unitsOwn) ? ((!empty($units) ? ',' : '') . $unitsOwn) : '');
        $unit_id_array = explode(",",$units);
        $units_list = $officeUnitTable->find('list',['keyField'=>'id','valueField'=>'unit_name_bng'])->where(['id IN'=>$unit_id_array])->toArray();
        if(count($unit_id_array) > 1){
            $units_list = [''=>'সকল শাখা']+$units_list;
        }
        $this->set("units_list", $units_list);
    }

    public function sectionWiseNothiListContent($startDate = "", $endDate = "",$isExport="",$per_page = 20 ,$unit_id = "",$version=1) {

        $this->layout = null;
        $employee_office = $this->getCurrentDakSection();
        $condition = [];


        $startDate = !empty($startDate) ? h($startDate) : date('Y-m-d');
        $endDate = !empty($endDate) ? h($endDate) : date('Y-m-d');

        $officeUnitTable = TableRegistry::get("OfficeUnits");

        $units = $officeUnitTable->getOfficeChildInfo($employee_office['office_unit_id']);
        $unitsOwn = $officeUnitTable->getOwnChildInfo($employee_office['office_unit_id']);



        $units = !empty($units[0]) ? $units[0] : '';
        $unitsOwn = !empty($unitsOwn[0]) ? ($employee_office['office_unit_id'] . ',' . $unitsOwn[0]) : $employee_office['office_unit_id'];

        $units = $units . (!empty($unitsOwn) ? ((!empty($units) ? ',' : '') . $unitsOwn) : '');

        $unit_id_array = explode(",", $units);
        if (!empty($unit_id) && in_array((int)$unit_id, $unit_id_array)) {
            $unit_id_array = (int)$unit_id;
        }



        $office = TableRegistry::get('Offices');
        $office_name = $office->find()->select(['office_name_bng', 'office_address'])->where(['id' => $employee_office['office_id']])->first();


        /*Set Variables for further query*/
        $function_options = [];
        $function_options['startDate'] = $startDate;
        $function_options['endDate'] = $endDate;
        $function_options['isExport'] = $isExport;
        $function_options['per_page'] = $per_page;
        $function_options['unit_id'] = $unit_id;
        $function_options['condition'] = $condition;
        $function_options['selected_office_section'] = $employee_office;
        $function_options['unit_id_array'] = $unit_id_array;
        /*Set Variables for further query*/




        $response = ''; //for export
        if ($version == 1) {
            $response = $this->sectionWiseNothiListContentV1($function_options);
        } else if ($version == 2) {
            $response = $this->sectionWiseNothiListContentV2($function_options);
        }
        if (!empty($isExport) && ($isExport == 'excel' || $isExport == 'pdf')) {
            return $response;
        }

        $this->set('office_name', $office_name);


    }

    private function sectionWiseNothiListContentV1($options = []){
        $condition = [];
        /*Set Variables for further query*/
        $startDate = isset($options['startDate'])?$options['startDate']:'';
        $endDate = isset($options['endDate'])?$options['endDate']:'';
        $isExport = isset($options['isExport'])?$options['isExport']:'';
        $per_page = isset($options['per_page'])?$options['per_page']:'';
        $unit_id = isset($options['unit_id'])?$options['unit_id']:'';
        $condition = isset($options['condition'])?$options['condition']:[];

        $selected_office_section = isset($options['selected_office_section'])?
            $options['selected_office_section']:[];
        $unit_id_array = isset($options['unit_id_array'])?$options['unit_id_array']:[];
        $office_name = isset($options['office_name'])?$options['office_name']:[];
        /*Set Variables for further query*/

        $report_table = TableRegistry::get('Reports');
        $officeUnitTable = TableRegistry::get("OfficeUnits");

        array_push($condition,["NothiParts.office_units_id IN"=> $unit_id_array]);
        $startDate = !empty($startDate)?h($startDate):date('Y-m-d');
        $endDate = !empty($endDate)?h($endDate):date('Y-m-d');

        if($isExport=='excel' || $isExport=='pdf'){
            $reports = $report_table->getAllSectionWiseNothi($startDate,$endDate,$condition);

        } else {
            $reports = $this->Paginator->paginate($report_table->getAllSectionWiseNothi($startDate,$endDate,$condition),['limit' => $per_page]);

        }


        $preorder = '';
        $data = [];
        $i = 0;
        if (!empty($reports)) {
            foreach ($reports as $dak) {
                if (empty($preorder)) {
                    $preorder = $dak['office_units_id'];
                }
                $preorder = $preorder . ',' . $dak['office_units_id'];
            }
            if (!empty($preorder)) {
                $order_unit = $officeUnitTable->getOrderwiseUnits($preorder);
            }

            if (!empty($order_unit)) {
                $order_unit_with_office_names= $officeUnitTable->find()->select(['id','unit_name_bng'])->where(['id in'=>array_keys($order_unit)])->toArray();
                foreach ($order_unit_with_office_names as $order_unit_with_office_name) {
                    foreach ($reports as $dak) {
                        if ($dak['office_units_id'] == $order_unit_with_office_name['id']) {
                            $data[$i]['nothi_created_date'] = $dak['nothi_created_date'];
                            $data[$i]['id'] = $dak['id'];
                            $data[$i]['nothi_class'] = $dak['nothi_class'];
                            $data[$i]['nothi_part_no'] = $dak['nothi_part_no'];
                            $data[$i]['subject'] = $dak['subject'];
                            $data[$i]['nothi_no'] = $dak['nothi_no'];
                            $data[$i]['created'] = $dak['created'];
                            $data[$i]['modified'] = $dak['modified'];
                            $data[$i]['nothi_types_id'] = $dak['nothi_types_id'];
                            $data[$i]['office_units_id'] = $dak['office_units_id'];
                            $data[$i]['unit_name_bng'] = $order_unit_with_office_name['unit_name_bng'];
                            $data[$i]['office_units_organogram_id'] = $dak['office_units_organogram_id'];
                            $data[$i]['office_id'] = $dak['office_id'];
                            $data[$i]['NothiTypes'] = $dak['NothiTypes'];

                            $i++;
                        }
                    }
                }
            }
        }
        $nothiClass = json_decode(NOTHI_CLASS, true);
        if($isExport=='excel' || $isExport=='pdf'){
            return $result= Array('data'=> $data,'office_name'=> $office_name['office_name_bng'],'nothiClass'=> $nothiClass);
        }
        $this->set('data', $data);
        $this->set('office_name', $office_name);
        $this->set('nothiClass', $nothiClass);

    }


    private function sectionWiseNothiListContentV2($options = []){
        $condition = [];
        /*Set Variables for further query*/
        $startDate = isset($options['startDate'])?$options['startDate']:'';
        $endDate = isset($options['endDate'])?$options['endDate']:'';
        $isExport = isset($options['isExport'])?$options['isExport']:'';
        $per_page = isset($options['per_page'])?$options['per_page']:'';
        $unit_id = isset($options['unit_id'])?$options['unit_id']:'';
        $condition = isset($options['condition'])?$options['condition']:[];

        $selected_office_section = isset($options['selected_office_section'])?
            $options['selected_office_section']:[];
        $unit_id_array = isset($options['unit_id_array'])?$options['unit_id_array']:[];
        $office_name = isset($options['office_name'])?$options['office_name']:[];
        /*Set Variables for further query*/

        $report_table = TableRegistry::get('NothiRegisterLists');
        $officeUnitTable = TableRegistry::get("OfficeUnits");

        array_push($condition,["NothiRegisterLists.created_unit_id IN" => $unit_id_array]);

        if($isExport=='excel' || $isExport=='pdf'){
            $reports = $report_table->getAllSectionWiseNothi($startDate,$endDate,$condition);
        } else {
            $reports = $this->Paginator->paginate($report_table->getAllSectionWiseNothi($startDate,$endDate,$condition), ['limit' => $per_page]);
        }



        $preorder = '';
        $data = [];
        $i = 0;
        if (!empty($reports)) {
            foreach ($reports as $dak) {
                if (empty($preorder)) {
                    $preorder = $dak['created_unit_id'];
                }
                $preorder = $preorder . ',' . $dak['created_unit_id'];
            }
            if (!empty($preorder)) {

                $order_unit = $officeUnitTable->getOrderwiseUnits($preorder);
            }
            if (!empty($order_unit)) {

                foreach ($order_unit as $key => $val) {
                    foreach ($reports as $dak) {
                        if ($dak['created_unit_id'] == $key) {

                            $data[$i]['id'] = $dak['id'];
                            $data[$i]['modified'] = $dak['modified'];
                            $data[$i]['changes_history'] = $dak['modified'];
                            $data[$i]['nothi_class'] = $dak['nothi_class'];
                            $data[$i]['from_office_unit_id'] = $dak['created_unit_id'];
                            $data[$i]['office_units_id'] = $dak['created_unit_id'];
                            $data[$i]['unit_name_bng'] = $val;
                            $data[$i]['from_officer_name'] = $dak['from_officer_name'];
                            $data[$i]['nothi_created_date'] = $dak['nothi_created_date'];
                            $data[$i]['nothi_no'] = $dak['nothi_no'];
                            $data[$i]['subject'] = $dak['subject'];
                            $data[$i]['to_office_unit_id'] = $dak['to_office_unit_id'];
                            $data[$i]['from_office_name'] = $dak['from_office_name'];
                            $data[$i]['to_office_name'] = $dak['to_office_name'];
                            $data[$i]['to_officer_name'] = $dak['to_officer_name'];
                            $data[$i]['from_officer_designation_label'] = $dak['from_officer_designation_label'];
                            $data[$i]['to_officer_designation_label'] = $dak['to_officer_designation_label'];
                            $data[$i]['NothiParts']['id'] = $dak['NothiParts']['id'];
                            $data[$i]['NothiParts']['nothi_part_no'] = $dak['NothiParts']['nothi_part_no'];
                            $data[$i]['NothiParts']['subject'] = $dak['NothiParts']['subject'];
                            $data[$i]['NothiParts']['nothi_no'] = $dak['NothiParts']['nothi_no'];
                            $data[$i]['NothiParts']['created'] = $dak['NothiParts']['created'];
                            $data[$i]['NothiParts']['modified'] = $dak['NothiParts']['modified'];
                            $data[$i]['NothiParts']['type_name'] = $dak['NothiTypes']['type_name'];
                            $data[$i]['NothiTypes'] = $dak['NothiTypes'];

                            $i++;
                        }
                    }
                }
            }
        }
        if($isExport=='excel' || $isExport=='pdf'){
            return $result= Array('data'=> $data,'office_name'=> $office_name['office_name_bng']);
        }
        $nothiClass = json_decode(NOTHI_CLASS, true);
        $this->set('data', $data);
        $this->set('nothiClass', $nothiClass);
    }



//    public function sectionWiseNothiListContent($startDate = "", $endDate = "",$isExport="",$per_page = 20 ,$unit_id = "",$version=2) {
//
//        $this->layout = null;
//        $condition = [];
//        $employee_office = $this->getCurrentDakSection();
//
//        $nothiClass = json_decode(NOTHI_CLASS, true);
//        $officeUnitTable = TableRegistry::get("OfficeUnits");
//
//        $units = $officeUnitTable->getOfficeChildInfo($employee_office['office_unit_id']);
//        $unitsOwn = $officeUnitTable->getOwnChildInfo($employee_office['office_unit_id']);
//
//        $units = !empty($units[0]) ? $units[0] : '';
//        $unitsOwn = !empty($unitsOwn[0]) ? ($employee_office['office_unit_id'] . ',' . $unitsOwn[0]) : $employee_office['office_unit_id'];
//
//        $units = $units . (!empty($unitsOwn) ? ((!empty($units) ? ',' : '') . $unitsOwn) : '');
//        $unit_id_array = explode(",",$units);
//        if(!empty($unit_id) && in_array((int)$unit_id,$unit_id_array)){
//            $unit_id_array = (int)$unit_id;
//        }
//        array_push($condition,["NothiParts.office_units_id IN"=> $unit_id_array]);
//        $startDate = !empty($startDate)?h($startDate):date('Y-m-d');
//        $endDate = !empty($endDate)?h($endDate):date('Y-m-d');
//
//        $report_table = TableRegistry::get('Reports');
//        if($isExport=='excel' || $isExport=='pdf'){
//            $reports = $report_table->getAllSectionWiseNothi($startDate,$endDate,$condition);
//
//        } else {
//            $reports = $this->Paginator->paginate($report_table->getAllSectionWiseNothi($startDate,$endDate,$condition),['limit' => $per_page]);
//
//        }
//
//        $office = TableRegistry::get('Offices');
//        $office_name = $office->find()->select(['office_name_bng', 'office_address'])->where(['id' => $employee_office['office_id']])->first();
//
//        $preorder = '';
//        $data = [];
//        $i = 0;
//        if (!empty($reports)) {
//            foreach ($reports as $dak) {
//                if (empty($preorder)) {
//                    $preorder = $dak['office_units_id'];
//                }
//                $preorder = $preorder . ',' . $dak['office_units_id'];
//            }
//            if (!empty($preorder)) {
//                $order_unit = $officeUnitTable->getOrderwiseUnits($preorder);
//            }
//
//            if (!empty($order_unit)) {
//                $order_unit_with_office_names= $officeUnitTable->find()->select(['id','unit_name_bng'])->where(['id in'=>array_keys($order_unit)])->toArray();
//                foreach ($order_unit_with_office_names as $order_unit_with_office_name) {
//                    foreach ($reports as $dak) {
//                        if ($dak['office_units_id'] == $order_unit_with_office_name['id']) {
//                            $data[$i]['nothi_created_date'] = $dak['nothi_created_date'];
//                            $data[$i]['id'] = $dak['id'];
//                            $data[$i]['nothi_class'] = $dak['nothi_class'];
//                            $data[$i]['nothi_part_no'] = $dak['nothi_part_no'];
//                            $data[$i]['subject'] = $dak['subject'];
//                            $data[$i]['nothi_no'] = $dak['nothi_no'];
//                            $data[$i]['created'] = $dak['created'];
//                            $data[$i]['modified'] = $dak['modified'];
//                            $data[$i]['nothi_types_id'] = $dak['nothi_types_id'];
//                            $data[$i]['office_units_id'] = $dak['office_units_id'];
//                            $data[$i]['unit_name_bng'] = $order_unit_with_office_name['unit_name_bng'];
//                            $data[$i]['office_units_organogram_id'] = $dak['office_units_organogram_id'];
//                            $data[$i]['office_id'] = $dak['office_id'];
//                            $data[$i]['NothiTypes'] = $dak['NothiTypes'];
//
//                            $i++;
//                        }
//                    }
//                }
//            }
//        }
//        if($isExport=='excel' || $isExport=='pdf'){
//            return $result= Array('data'=> $data,'office_name'=> $office_name['office_name_bng'],'nothiClass'=> $nothiClass);
//        }
//
//        $this->set('data', $data);
//        $this->set('office_name', $office_name);
//        $this->set('nothiClass', $nothiClass);
//
//    }

    public function getMonthlyNothiExcel($startDate = '', $endDate = '',$dateRangeBn=''){
        $selected_office_section=  $this->getCurrentDakSection();

        $report = $this->getMonthlyNothiContent($startDate,$endDate,'excel');
        if(empty($datas)){
            $datas= Array();
        }
        $total_nothi = 0;
        $total_note = 0;
        $total_srijito = 0;
        $total_swa_uddok = 0;
        $columns = ['count','all_note','dak_srijito_note','swa_uddog_note'];
        if(!empty($report)){
            foreach ($report as $data){
                $total_nothi += isset($data['count'])?$data['count']:0;
                $total_note += isset($data['all_note'])?$data['all_note']:0;
                $total_srijito += isset($data['dak_srijito_note'])?$data['dak_srijito_note']:0;
                $total_swa_uddok += isset($data['swa_uddog_note'])?$data['swa_uddog_note']:0;

                if(!empty($columns)){
                    foreach ($columns as $column){
                        if(isset($data[$column])){
                            $data[$column] = enTobn($data[$column]);
                        }
                    }
                }

                $datas[] = $data;
            }
        }
        $this->printExcel($datas, [
            ['key' => 'si', 'title' => 'ক্রম'],
            ['key' => 'name', 'title' => 'শাখার নাম'],
            ['key' => 'count', 'title' => 'নথি'],
            ['key' => 'all_note', 'title' => 'নোট উপস্থাপন'],
            ['key' => 'dak_srijito_note', 'title' => 'ডাক থেকে সৃজিত নোট'],
            ['key' => 'swa_uddog_note', 'title' => 'স্বউদ্যোগে নোট'],
        ],[
            'name'=>((isset($selected_office_section['office_name']))?$selected_office_section['office_name'].' - এর ':'') .'উপস্থাপিত নথি সংখ্যা ',
            'title' => [
                ((isset($selected_office_section['office_name']))?$selected_office_section['office_name'].'  - এর ':'') .'উপস্থাপিত নথি সংখ্যা ' ,
                $dateRangeBn,
            ],
            'footer' => [
                isset($total_nothi)? ['মোট','',entobn($total_nothi),entobn($total_note),entobn($total_srijito),entobn($total_swa_uddok)]: [],
            ]
        ]);
        exit;
    }

    public function getMonthlyNothiDetailsExcel($startDate = '', $endDate = '',$dateRangeBn=''){
        $selected_office_section=  $this->getCurrentDakSection();

        $resullt = $this->getMonthlyNothiDetailsContent($startDate,$endDate,'excel');

        if(!empty($resullt)){
            $unit_id=0;
            $i=0;
            $datas= array();
            foreach($resullt as $data){
                if($unit_id==$data['unit_id']){

                   $datas[$i]['name'] ='';
                   $datas[$i]['nothi_count'] = '';
                   $datas[$i]['nothi_subject'] = $data['nothi_subject'];
                   $datas[$i]['nothi_no'] = $data['nothi_no'];
                   $datas[$i]['all_note']= $data['all_note'];
                   $datas[$i]['dak_srijito_note'] = $data['dak_srijito_note'];
                   $datas[$i]['swa_uddog_note'] = $data['swa_uddog_note'];
                   $datas[$i]['nisponno'] = $data['nisponno'];

                } else {
                   $unit_id= $data['unit_id'];

                   $datas[$i]['name'] =$data['name'];
                   $datas[$i]['nothi_subject'] = $data['nothi_subject'];
                   $datas[$i]['nothi_no'] = $data['nothi_no'];
                   $datas[$i]['all_note']= $data['all_note'];
                   $datas[$i]['dak_srijito_note'] = $data['dak_srijito_note'];
                   $datas[$i]['swa_uddog_note'] = $data['swa_uddog_note'];
                   $datas[$i]['nisponno'] = $data['nisponno'];
                }
                $i++;
            }
        }  else {
            $datas= Array();

        }

        $this->printExcel($datas, [
            ['key' => 'name', 'title' => 'শাখার নাম'],
            ['key' => 'nothi_subject', 'title' => 'উপস্থাপিত নথির নাম'],
            ['key' => 'nothi_no', 'title' => 'নথি নং'],
            ['key' => 'all_note', 'title' => 'উপস্থাপনের সংখ্যা'],
            ['key' => 'dak_srijito_note', 'title' => 'ডাক থেকে সৃজিত নোটের সংখ্যা'],
            ['key' => 'swa_uddog_note', 'title' => 'স্বউদ্যোগে নোটের সংখ্যা'],
            ['key' => 'nisponno', 'title' => 'মোট নিষ্পন্ন'],
        ],[
            'name'=>((isset($selected_office_section['office_name']))?$selected_office_section['office_name'].' অফিসের ':'') .'অফিস ভিত্তিক নথি বিবরনী' ,
            'title' => [
                ((isset($selected_office_section['office_name']))?$selected_office_section['office_name'].' অফিসের ':'') .'অফিস ভিত্তিক নথি বিবরনী' ,
                $dateRangeBn,
            ]
        ]);
        exit;
    }

    public function getMonthlyNothi(){
        $selected_office_section=  $this->getCurrentDakSection();
        $this->set('office_name',$selected_office_section['office_name']);
    }

    public function getMonthlyNothiContent($startDate = '', $endDate = '',$isExport="")
    {

        $this->layout = null;

        $startDate = !empty($startDate) ? $startDate : date("Y-m-d");
        $endDate = !empty($endDate) ? $endDate : date("Y-m-d");

        $selected_office_section = $this->getCurrentDakSection();
        $OfficeUnitsTable = TableRegistry::get('OfficeUnits');
        $ownChildInfo = $OfficeUnitsTable->getOfficeUnitsList($selected_office_section['office_id']);

        $NothiMastersTable = TableRegistry::get('NothiMasters');
        $NoteInitializeTable = TableRegistry::get('NoteInitialize');

        $data= array();
        foreach ($ownChildInfo as $unique_office_units_id => $unit_name) {
           $data[$unique_office_units_id]['count'] =  $NothiMastersTable->allMasterInfo($selected_office_section['office_id'],$unique_office_units_id,0,[$startDate,$endDate])->count();
           $data[$unique_office_units_id]['swa_uddog_note'] = $NoteInitializeTable->selfSrijitoNote($selected_office_section['office_id'],$unique_office_units_id,0,[$startDate,$endDate])->count();
          $data[$unique_office_units_id]['dak_srijito_note'] =  $NoteInitializeTable->dakSrijitoNote($selected_office_section['office_id'],$unique_office_units_id,0,[$startDate,$endDate])->count();
          $data[$unique_office_units_id]['all_note'] = ($data[$unique_office_units_id]['swa_uddog_note'] +  $data[$unique_office_units_id]['dak_srijito_note']) ;

          $data[$unique_office_units_id]['unit_id']=$unique_office_units_id;
          $data[$unique_office_units_id]['name']=$unit_name;
        }

        if($isExport=='excel' || $isExport=='pdf'){
            return  $data;
        }
        $this->set('datas', $data);
        $this->set(compact('selected_office_section','startDate','endDate'));
    }

    public function getMonthlyNothiDetails(){
        $selected_office_section=  $this->getCurrentDakSection();
        $this->set('office_name',$selected_office_section['office_name']);

    }

    public function getMonthlyNothiDetailsContent($startDate = '', $endDate = '',$isExport='')
    {

        $this->layout = null;

        $startDate = !empty($startDate) ? $startDate : date("Y-m-d");
        $endDate = !empty($endDate) ? $endDate : date("Y-m-d");

        $selected_office_section = $this->getCurrentDakSection();
        $OfficeUnitsTable = TableRegistry::get('OfficeUnits');
        $ownChildInfo = $OfficeUnitsTable->getOfficeUnitsList($selected_office_section['office_id']);

//        $NothiMastersTable = TableRegistry::get('NothiMasters');
        $NoteInitializeTable = TableRegistry::get('NoteInitialize');

//        $query = $NothiMastersTable->find()->select(['id','subject','office_units_id'])->where(['office_id'=>$selected_office_section['office_id']])->where(['DATE (created)>='=>$startDate])->where(['DATE (created)<='=>$endDate])->order([ 'office_units_id'=>'asc','created'=>'desc']);
        $query = $NoteInitializeTable->getNothiDetails($selected_office_section['office_id'],0,0,[$startDate,$endDate]);

        if($isExport=='excel' || $isExport=='pdf'){
            $nothi_details=$query->toArray();
        } else {
            $nothi_details= $this->paginate($query);
        }


        $NisponnoRecordsTable = TableRegistry::get('NisponnoRecords');

        $i = 0;
        $datas= array();
        foreach ($nothi_details as $nothi_detail) {
            foreach ($ownChildInfo as $key => $value){
                if ($nothi_detail['office_units_id'] == $key) {
                    $datas[$i]['unit_id'] = $key;
                    $datas[$i]['name'] = $value;
                    $datas[$i]['nothi_master_id'] = $nothi_detail['nothi_masters_id'];
                    $datas[$i]['nothi_subject'] = $nothi_detail['subject'];
                    $datas[$i]['nothi_part_no'] = $nothi_detail['nothi_part_no'];
                    $datas[$i]['office_id'] = $nothi_detail['office_id'];
                    $datas[$i]['office_units_id'] = $nothi_detail['office_units_id'];
                    $datas[$i]['office_units_organogram_id'] = $nothi_detail['office_units_organogram_id'];
                    $datas[$i]['nothi_no'] = $nothi_detail['nothi_no'];
                    $datas[$i]['swa_uddog_note'] = $NoteInitializeTable->selfSrijitoNoteByNoteId([$nothi_detail['nothi_part_no']],[$startDate,$endDate])->count();
                    $datas[$i]['dak_srijito_note'] =  $NoteInitializeTable->dakSrijitoNoteByNoteId([$nothi_detail['nothi_part_no']],[$startDate,$endDate])->count();
                    $datas[$i]['all_note'] = enTobn($datas[$i]['swa_uddog_note'] +  $datas[$i]['dak_srijito_note']) ;
                    $datas[$i]['swa_uddog_note'] = entobn($datas[$i]['swa_uddog_note']);
                    $datas[$i]['dak_srijito_note'] = enTobn( $datas[$i]['dak_srijito_note']);
                    $nisponno = $NisponnoRecordsTable->getNisponnoCount(0,0,0,[$startDate,$endDate],$nothi_detail['nothi_part_no']);
                    $datas[$i]['nisponno'] = enTobn($nisponno['potrojari'] + $nisponno['note']);
                    $i++;
                }

            }
        }
        if($isExport=='excel' || $isExport=='pdf'){
            return $datas;
        }
        $this->set('datas', $datas);
    }
    public function getNothiActivities(){
        $selected_office_section=  $this->getCurrentDakSection();
        $this->set('office_name',$selected_office_section['office_name']);
    }
    public function getNothiActivitiesContent($startDate = '', $endDate = '',$isExport="")
    {

        $this->layout = null;

        $startDate = !empty($startDate) ? $startDate : date("Y-m-d");
        $endDate = !empty($endDate) ? $endDate : date("Y-m-d");

        $selected_office_section = $this->getCurrentDakSection();
        $OfficeUnitsTable = TableRegistry::get('OfficeUnits');
        $ownChildInfo = $OfficeUnitsTable->getOfficeUnitsList($selected_office_section['office_id']);

        $NothiMastersTable = TableRegistry::get('NothiMasters');
        $archiveNothiMastersTable = TableRegistry::get('ArchiveNothiMasters');
        $NothiNotesTable = TableRegistry::get('NothiNotes');
        $NoteInitializeTable = TableRegistry::get('NoteInitialize');

        $data= array();
        foreach ($ownChildInfo as $unique_office_units_id => $unit_name) {
            $allNothiMaster = $NothiMastersTable->allMasterInfo($selected_office_section['office_id'],$unique_office_units_id,0,[$startDate,$endDate],1)->toArray();
            $data[$unique_office_units_id]['running_nothi'] = count($allNothiMaster);
            //check active notes
            $data[$unique_office_units_id]['active_nothi'] = $NothiNotesTable->getNotes(0,0)->where(['nothi_master_id IN' => $allNothiMaster])->distinct(['nothi_master_id'])->count();
            $data[$unique_office_units_id]['inactive_nothi'] = $data[$unique_office_units_id]['running_nothi'] -  $data[$unique_office_units_id]['active_nothi'];
            // archive nothi table can not be created if no nothi is archive able.
            try{
                $data[$unique_office_units_id]['archive_nothi'] = $archiveNothiMastersTable->allMasterInfo($selected_office_section['office_id'],$unique_office_units_id,0,[$startDate,$endDate])->count();
            }catch (\Exception $ex){
                $data[$unique_office_units_id]['archive_nothi'] = 0 ;
            }
            $data[$unique_office_units_id]['total_nothi'] = $data[$unique_office_units_id]['running_nothi'] +  $data[$unique_office_units_id]['archive_nothi'];



            $data[$unique_office_units_id]['unit_id']=$unique_office_units_id;
            $data[$unique_office_units_id]['name']=$unit_name;
        }

        if($isExport=='excel' || $isExport=='pdf'){
            return  $data;
        }
        $this->set('datas', $data);
        $this->set(compact('selected_office_section','startDate','endDate'));
    }
    function getNothiActivitiesExcel($startDate = '', $endDate = '',$dateRangeBn=''){
        $selected_office_section=  $this->getCurrentDakSection();

        $report = $this->getNothiActivitiesContent($startDate,$endDate,'excel');
        $datas= Array();
        $total_running_nothi = 0;
        $total_active_nothi = 0;
        $total_inactive_nothi = 0;
        $total_archive_nothi = 0;
        $total_nothi = 0;
        $columns = ['running_nothi','active_nothi','inactive_nothi','archive_nothi','total_nothi'];
        if(!empty($report)){
            foreach ($report as $data){
                $total_running_nothi += isset($data['running_nothi'])?$data['running_nothi']:0;
                $total_active_nothi += isset($data['active_nothi'])?$data['active_nothi']:0;
                $total_inactive_nothi += isset($data['inactive_nothi'])?$data['inactive_nothi']:0;
                $total_archive_nothi += isset($data['archive_nothi'])?$data['archive_nothi']:0;
                $total_nothi += isset($data['total_nothi'])?$data['total_nothi']:0;

                if(!empty($columns)){
                    foreach ($columns as $column){
                      if(isset($data[$column])){
                          $data[$column] = enTobn($data[$column]);
                      }  
                    }
                }

                $datas[] = $data;
            }
        }
//        if(!empty())
        $this->printExcel($datas, [
            ['key' => 'si', 'title' => 'ক্রম'],
            ['key' => 'name', 'title' => 'শাখার নাম'],
            ['key' => 'active_nothi', 'title' => 'চলমান নথি(সক্রিয়)'],
            ['key' => 'inactive_nothi', 'title' => 'চলমান নথি(নিষ্ক্রিয়)'],
            ['key' => 'archive_nothi', 'title' => 'আর্কাইভ নথি'],
            ['key' => 'total_nothi', 'title' => 'মোট'],
        ],[
            'name'=>((isset($selected_office_section['office_name']))?$selected_office_section['office_name'].' - এর ':'') .'তৈরিকৃত নথি তালিকা',
            'title' => [
                ((isset($selected_office_section['office_name']))?$selected_office_section['office_name'].'  - এর ':'') .'তৈরিকৃত নথি তালিকা' ,
                $dateRangeBn,
            ],
            'footer' => [
                isset($total_nothi)? ['মোট','',entobn($total_active_nothi),entobn($total_inactive_nothi),entobn($total_archive_nothi),entobn($total_nothi)]: [],
            ]
        ]);
        exit;
    }


}
