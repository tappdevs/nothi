<?php

namespace App\Controller;

use App\Controller\ProjapotiController;
use App\Model\Table;
use App\Model\Table\NotificationEventsTable;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

class NotificationEventsController extends ProjapotiController {

    public $paginate = [
        'fields' => ['Events.id'],
        'limit' => 25,
        'order' => [
            'Templates.id' => 'asc'
        ]
    ];

    public function initialize() {
        parent::initialize();
        $this->loadComponent('Paginator');
    }

    public function index() {
        $event = TableRegistry::get('NotificationEvents');
        $query = $event->find('all');
        $this->set(compact('query'));
    }

    public function add() {
        $event = $this->NotificationEvents->newEntity();
        if ($this->request->is('post')) {
            $event = $this->NotificationEvents->patchEntity($event, $this->request->data);
            if ($this->NotificationEvents->save($event)) {
                return $this->redirect(['action' => 'add']);
            }
        }

        $table_instance_nt = TableRegistry::get('NotificationTemplates');
        $templates = $table_instance_nt->find('all');

        $template_list = array();
        foreach ($templates as $template) {
            $template_list[] = array('id' => $template["id"], 'title_bng' => $template["title_bng"]);
        }
        $this->set('template_list', $template_list);

        $this->set('event', $event);
    }

    public function edit($id = null, $is_ajax = null) {
        if ($is_ajax == 'ajax') {
            $this->layout = null;
        }

        $table_instance_nt = TableRegistry::get('NotificationTemplates');
        $templates = $table_instance_nt->find('all');

        $template_list = array();
        foreach ($templates as $template) {
            $template_list[] = array('id' => $template["id"], 'title_bng' => $template["title_bng"]);
        }




        $scan_controllers = $this->ControllerList->getControllerList();
        $controllers = array();
        foreach ($scan_controllers as $controller) {
            $controllers[lcfirst(str_replace('Controller', '', $controller))] = $controller;
        }

        $this->set('template_list', $template_list);
        $this->set('controllers', $controllers);


        $event = $this->NotificationEvents->get($id);
        if ($this->request->is(['post', 'put'])) {
            $this->NotificationEvents->patchEntity($event, $this->request->data);
            if ($this->NotificationEvents->save($event)) {
                return $this->redirect(['action' => 'index']);
            }
        }
        $this->set('event', $event);
    }

    public function view($id = null) {
        $event = $this->NotificationEvents->get($id);
        $this->set(compact('event'));
    }

    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);

        $event = $this->NotificationEvents->get($id);
        if ($this->NotificationEvents->delete($event)) {
            return $this->redirect(['action' => 'index']);
        }
    }

}
