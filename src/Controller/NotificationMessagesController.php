<?php

namespace App\Controller;

use App\Controller\ProjapotiController;
use App\Model\Table;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\I18n\I18n;
use Cake\I18n\Time;
use Cake\I18n\Number;

class NotificationMessagesController extends ProjapotiController {

    public $paginate = [
        'fields' => ['Templates.id'],
        'limit' => 25,
        'order' => [
            'Templates.id' => 'asc'
        ]
    ];

    public function initialize() {
        parent::initialize();
        $this->loadComponent('Paginator');
    }

    public function index($date_from = "", $date_to = "") {
        $session = $this->request->session();
        $selected_office_section = $session->read('selected_office_section');

        $template_message = TableRegistry::get('NotificationMessages');

        if (($date_from != "") && ($date_to != "")) {

            $date_from = explode("-", $date_from);
            $date_to = explode("-", $date_to);

            $start_date = $date_from[1] . "-" . $date_from[0] . "-" . $date_from[2];
            $end_date = $date_to[1] . "-" . $date_to[0] . "-" . $date_to[2];

            $start_date = strtotime($start_date);
            $end_date = strtotime($end_date);

            $query = $template_message->find('all')->where(['from_office_id' => $selected_office_section['office_id'], 'created >' => $start_date, 'created <' => $end_date]);
        } else {
            $query = $template_message->find('all')->where(['from_office_id' => $selected_office_section['office_id']]);
        }
        $this->set(compact('query'));
    }

    public function outgoingMessages($date_from = 0, $date_to = 0) {
        $session = $this->request->session();
        $selected_office_section = $session->read('selected_office_section');

        $template_message = TableRegistry::get('NotificationMessages');
        $query = $template_message->find('all')->where(['from_office_id' => $selected_office_section['office_id']]);
        $this->set(compact('query'));
    }

    public function incomingMessages($date_from = 0, $date_to = 0) {
        $session = $this->request->session();
        $selected_office_section = $session->read('selected_office_section');

        $template_message = TableRegistry::get('NotificationMessages');
        $query = $template_message->find('all')->where(['to_office_id' => $selected_office_section['office_id']]);
        $this->set(compact('query'));
    }

    public function getNotifications() {
        $session = $this->request->session();
        $selected_office_section = $session->read('selected_office_section');

        $template_message = TableRegistry::get('NotificationMessages');

        $dak_id = intval($this->request->query['dak_id']);
        $nothi_id = intval($this->request->query['nothi_id']);

        if ($dak_id == 0 && $nothi_id == 0) {
            $notify_query = $template_message->find('all')
                            ->where(['to_office_id' => $selected_office_section['office_id'], 'to_user_id' => $selected_office_section['officer_id'], 'system' => 1])
                            ->order(['id desc'])->toArray();
        }
        else {
            if($dak_id>0){
                 $notify_query_dak = $template_message->find('all')
                            ->where(['to_office_id' => $selected_office_section['office_id'], 'to_user_id' => $selected_office_section['officer_id'], 'system' => 1, 'id >' => $dak_id, 'event_type' => 'Dak'])
                            ->order(['id desc'])->toArray();
            }else{
                $notify_query_dak = $template_message->find('all')
                            ->where(['to_office_id' => $selected_office_section['office_id'], 'to_user_id' => $selected_office_section['officer_id'], 'system' => 1, 'id >' => 0, 'event_type' => 'Dak'])
                            ->order(['id desc'])->toArray();
            }
            
            if(!empty($nothi_id)){
                $notify_query_nothi = $template_message->find('all')
                            ->where(['to_office_id' => $selected_office_section['office_id'], 'to_user_id' => $selected_office_section['officer_id'], 'system' => 1, 'id >' => $nothi_id, 'event_type' => 'Nothi'])
                            ->order(['id desc'])->toArray();
            }else{
                $notify_query_nothi = $template_message->find('all')
                            ->where(['to_office_id' => $selected_office_section['office_id'], 'to_user_id' => $selected_office_section['officer_id'], 'system' => 1, 'id >' => 0, 'event_type' => 'Nothi'])
                            ->order(['id desc'])->toArray();
            }
           
            
            $notify_query = array_merge($notify_query_dak, $notify_query_nothi);
        }

        $notifications = array();

        $i = 0;
        $notifications["Dak"] = 0;
        $notifications["Nothi"] = 0;
        $notifications['data'] = array();
        if (!empty($notify_query)) {
            foreach ($notify_query as $notify) {

                $notifications['data'][$i]["id"] = $notify["id"];
                $notifications['data'][$i]["message_bng"] = $notify["message_bng"];
                $notifications['data'][$i]["event_id"] = $notify["event_id"];
                $notifications['data'][$i]["event_type"] = $notify["event_type"];
                $notifications['data'][$i]["from_user_id"] = $notify["from_user_id"];
                $notifications['data'][$i]["from_office_id"] = $notify["to_user_id"];
                $notifications['data'][$i]["media"] = $notify["media"];

                $now = new Time($notify['created']);
                $now = $now->timeAgoInWords(
                        ['format' => 'h,i,s d, M, YYY', 'end' => '+1 year']
                );

                $changeWords = array(
                    "years" => 'বছর',
                    "year" => 'বছর',
                    "months" => 'মাস',
                    "month" => 'মাস',
                    "days" => 'দিন',
                    "day" => 'দিন',
                    "hour" => 'ঘন্টা',
                    "hours" => 'ঘন্টা',
                    "minutes" => 'মিনিট',
                    "minute" => 'মিনিট',
                    "seconds" => 'সেকেন্ড',
                    "second" => 'সেকেন্ড',
                    "week" => 'সপ্তাহ',
                    "weeks" => 'সপ্তাহ',
                    "ago" => 'আগে',
                );

                $newtime = "";

                $now = str_replace(",", '', $now);
                $times = explode(" ", $now);

                if (!empty($times)) {
                    foreach ($times as $k => $timevalue) {
                        if (is_numeric($timevalue)) {
                            $newtime .= Number::format($timevalue) . ' ';
                        } else {
                            $replace = str_replace($timevalue, $changeWords[$timevalue], $timevalue);
                            $newtime .= $replace . ' ';
                        }
                    }
                }

                $notifications['data'][$i]["ago"] = $newtime;
                $notifications[$notify["event_type"]] ++;

                $i++;
            }
        }
        $notifications["Dak"] = \Cake\I18n\Number::format($notifications["Dak"]);
        $notifications["Nothi"] = \Cake\I18n\Number::format($notifications["Nothi"]);


        $this->response->body(json_encode($notifications));
        $this->response->type('application/json');
        return $this->response;
    }

    public function notificationSettings() {

        if ($this->request->is('ajax')) {
            $this->layout = null;
        }
        $eventTable = TableRegistry::get('NotificationEvents');
       
        $selected_office_section = $this->getCurrentDakSection();

        $notificationSettingsTable = TableRegistry::get('NotificationSettings');

        $this->set('user', 0);

        if ($this->Auth->user('user_role_id') <= 2) {
            $this->set('user', 1);
            $existing = $notificationSettingsTable->find()->where(['employee_id' => 0, 'office_id' => 0])->toArray();
        } else {
            $existing = $notificationSettingsTable->find()->where(['employee_id' => $selected_office_section['officer_id'], 'office_id' => $selected_office_section['office_id']])->toArray();
            if (empty($existing)) {
                $existing = $notificationSettingsTable->find()->where(['employee_id' => 0, 'office_id' => 0])->toArray();
            }
        }

        $supermanSettings = $notificationSettingsTable->find()->where(['employee_id' => 0 , 'office_id' => $selected_office_section['office_id']])->toArray();

        $existingSetting = array();
        $supermanSetting = array();

        if (!empty($existing)) {
            foreach ($existing as $key => $value) {
                $existingSetting[$value['event_id']] = $value;
            }
        }
        if (!empty($supermanSettings)) {
            foreach ($supermanSettings as $key => $value) {
                if ($value['email'] == 0 && $value['sms'] == 0) {
                    continue;
                }
                $supermanSetting[$value['event_id']] = $value;
            }
        }

        $this->set('existingSetting', $existingSetting);
        $this->set('supermanSetting', $supermanSetting);

        $events = $eventTable->find()->toArray();

        $eventTabs = array();
        if (!empty($events)) {
            foreach ($events as $key => $value) {
                if (!array_key_exists($value['id'], $supermanSetting)) {
                    continue;
                }
                $eventTabs[$value['template_id']][] = $value;
            }
        }


        $this->set(compact('eventTabs'));
    }

    public function saveSettings() {

        $selected_office_section = $this->getCurrentDakSection();
        $notificationSettingsTable = TableRegistry::get('NotificationSettings');
        $json_array = array('status' => 'error', 'msg' => 'দুঃখিত! অনুরোধ সম্পুন্ন করা সম্ভব হচ্ছে না');
        if ($this->request->is('ajax')) {
            if (!empty($this->request->data)) {
                $event = array();
                foreach ($this->request->data as $key => $data) {
                    $name = explode('_', $key);
                    $event[$name[1]][] = $data;
                }

                if (!empty($event)) {
                    if ($this->Auth->user('user_role_id') <= 2) {
                        $selected_office_section['officer_id'] = 0;
                        $selected_office_section['office_id'] = 0;
                    }
                    $notificationSettingsTable->deleteAll(['employee_id' => $selected_office_section['officer_id'], 'office_id' => $selected_office_section['office_id']]);
                    
                    foreach ($event as $key => $value) {
                        $newEntity = $notificationSettingsTable->newEntity();

                        $newEntity->event_id = $key;
                        $newEntity->employee_id = $selected_office_section['officer_id'];
                        $newEntity->office_id = $selected_office_section['office_id'];
                        /** For Quick Notification Bar */
//                        $newEntity->system = $value[0];
                         /** For Email  */
                        if(empty( $value[0])){
                             $value[0] = 0;
                        }
                        $newEntity->email = $value[0];
                         /** For SMS */
                          if(empty( $value[1])){
                             $value[1] = 0;
                        }
                        $newEntity->sms = $value[1];
                         /** For App Notification  */
                          if(empty( $value[2])){
                             $value[2] = 0;
                        }
                        $newEntity->mobile_app = $value[2];

                        $notificationSettingsTable->save($newEntity);
                    }
                }

                $json_array = array('status' => 'success', 'msg' => 'নোটিফিকেশন সেটিংস সংরক্ষণ করা হয়েছে');
                ;
            }
        }
        echo json_encode($json_array);
        die;
    }
    public function officeNotificationSettings() {

          if ($this->request->is('ajax')) {
            $this->layout = null;
        }
        $eventTable = TableRegistry::get('NotificationEvents');

        $selected_office_section = $this->getCurrentDakSection();

        $notificationSettingsTable = TableRegistry::get('NotificationSettings');

        $this->set('user', 0);
        $this->set('officeId',0);
        $this->set('officeName','');
        if ($this->Auth->user('user_role_id') <= 2) {
            $this->set('user', 1);
             if(!empty($this->request->data['office_id'])){
                 if($this->request->data['office_id'] > 0){
                    $office_info = TableRegistry::get('Offices')->getBanglaName($this->request->data['office_id']);
                      $this->set('officeId',$this->request->data['office_id']);
                      $this->set('officeName',$office_info['office_name_bng']);
                 }
                
                  $existing = $notificationSettingsTable->find()->where(['employee_id' => 0, 'office_id' => $this->request->data['office_id'] ])->toArray();
            }else{
                  $existing = $notificationSettingsTable->find()->where(['employee_id' => 0, 'office_id' => 0])->toArray();
            }
        } else {
            $existing = $notificationSettingsTable->find()->where(['employee_id' => $selected_office_section['officer_id'], 'office_id' => $selected_office_section['office_id']])->toArray();
            if (empty($existing)) {
                $existing = $notificationSettingsTable->find()->where(['employee_id' => 0, 'office_id' => 0])->toArray();
            }
        }


        $existingSetting = array();

        if (!empty($existing)) {
            foreach ($existing as $key => $value) {
                $existingSetting[$value['event_id']] = $value;
            }
        }

        $this->set('existingSetting', $existingSetting);

        $events = $eventTable->find()->toArray();

        $eventTabs = array();
        if (!empty($events)) {
            foreach ($events as $key => $value) {
                $eventTabs[$value['template_id']][] = $value;
            }
        }


        $this->set(compact('eventTabs'));

    }
    public function officeSaveSettings(){
       $officer_id = $office_id =0;
        $notificationSettingsTable = TableRegistry::get('NotificationSettings');
        $json_array = array('status' => 'error', 'msg' => 'দুঃখিত! অনুরোধ সম্পুন্ন করা সম্ভব হচ্ছে না');
        if ($this->request->is('ajax')) {
            if (!empty($this->request->data)) {

                $event = array();
                foreach ($this->request->data as $key => $data) {
                    if($key == 'officeId'){
                        $office_id = $data;
                        continue;
                    }
                    $name = explode('_', $key);
                    $event[$name[1]][] = $data;
                }

                if (!empty($event)) {
                    $notificationSettingsTable->deleteAll(['employee_id' => $officer_id, 'office_id' => $office_id ]);
//                    pr($event);die;
                    foreach ($event as $key => $value) {
                        $newEntity = $notificationSettingsTable->newEntity();

                        $newEntity->event_id = $key;
                        $newEntity->employee_id = $officer_id;
                        $newEntity->office_id = $office_id;
                        /** For Quick Notification Bar */
//                        $newEntity->system = $value[0];
                         /** For Email  */
                        $newEntity->email = $value[0];
                         /** For SMS */
                        $newEntity->sms = $value[1];
                         /** For App Notification  */
                        $newEntity->mobile_app = $value[2];

                        $notificationSettingsTable->save($newEntity);
                    }
                }

                $json_array = array('status' => 'success', 'msg' => 'নোটিফিকেশন সেটিংস সংরক্ষণ করা হয়েছে');
                ;
            }
        }

        echo json_encode($json_array);

        die;
    }

}
