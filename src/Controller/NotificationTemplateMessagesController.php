<?php

namespace App\Controller;

use App\Controller\ProjapotiController;
use App\Model\Table;
use App\Model\Table\NotificationMessageTemplatesTable;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

class NotificationTemplateMessagesController extends ProjapotiController {

    public $paginate = [
        'fields' => ['Templates.id'],
        'limit' => 25,
        'order' => [
            'Templates.id' => 'asc'
        ]
    ];

    public function initialize() {
        parent::initialize();
        $this->loadComponent('Paginator');
    }

    public function index() {
        $template_message = TableRegistry::get('NotificationTemplateMessages');
        $query = $template_message->find('all');
        $this->set(compact('query'));
    }

    public function add() {
        $template_message = $this->NotificationTemplateMessages->newEntity();
        if ($this->request->is('post')) {
            $template_message = $this->NotificationTemplateMessages->patchEntity($template_message, $this->request->data);
            if ($this->NotificationTemplateMessages->save($template_message)) {
                return $this->redirect(['action' => 'add']);
            }
        }

        $table_instance_nt = TableRegistry::get('NotificationTemplates');
        $templates = $table_instance_nt->find('all');

        $template_list = array();
        foreach ($templates as $template) {
            $template_list[] = array('id' => $template["id"], 'title_bng' => $template["title_bng"]);
        }
        $this->set('template_list', $template_list);

        $this->set('template_message', $template_message);
    }

    public function edit($id = null, $is_ajax = null) {
        if ($is_ajax == 'ajax') {
            $this->layout = null;
        }
        $template_message = $this->NotificationTemplateMessages->get($id);
        if ($this->request->is(['post', 'put'])) {
            $this->NotificationTemplateMessages->patchEntity($template_message, $this->request->data);
            if ($this->NotificationTemplateMessages->save($template_message)) {
                return $this->redirect(['action' => 'index']);
            }
        }

        $table_instance_nt = TableRegistry::get('NotificationTemplates');
        $templates = $table_instance_nt->find('all');

        $template_list = array();
        foreach ($templates as $template) {
            $template_list[] = array('id' => $template["id"], 'title_bng' => $template["title_bng"]);
        }
        $this->set('template_list', $template_list);

        $this->set('template_message', $template_message);
    }

    public function view($id = null) {
        $template_message = $this->NotificationTemplateMessages->get($id);
        $this->set(compact('template_message'));
    }

    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);

        $template_message = $this->NotificationTemplateMessages->get($id);
        if ($this->NotificationTemplateMessages->delete($template_message)) {
            return $this->redirect(['action' => 'index']);
        }
    }

}
