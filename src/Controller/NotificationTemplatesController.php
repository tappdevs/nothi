<?php

namespace App\Controller;

use App\Controller\ProjapotiController;
use App\Model\Table;
use App\Model\Table\NotificationMessageTemplatesTable;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

class NotificationTemplatesController extends ProjapotiController {

    public $paginate = [
        'fields' => ['Templates.id'],
        'limit' => 25,
        'order' => [
            'Templates.id' => 'asc'
        ]
    ];

    public function initialize() {
        parent::initialize();
        $this->loadComponent('Paginator');
    }

    public function index() {
        $template = TableRegistry::get('NotificationTemplates');
        $query = $template->find('all');
        $this->set(compact('query'));
    }

    public function add() {
        $template = $this->NotificationTemplates->newEntity();
        if ($this->request->is('post')) {
            $template = $this->NotificationTemplates->patchEntity($template, $this->request->data);
            if ($this->NotificationTemplates->save($template)) {
                return $this->redirect(['action' => 'add']);
            }
        }
        $this->set('template', $template);
    }

    public function edit($id = null, $is_ajax = null) {
        if ($is_ajax == 'ajax') {
            $this->layout = null;
        }
        $template = $this->NotificationTemplates->get($id);
        if ($this->request->is(['post', 'put'])) {
            $this->NotificationTemplates->patchEntity($template, $this->request->data);
            if ($this->NotificationTemplates->save($template)) {
                return $this->redirect(['action' => 'index']);
            }
        }
        $this->set('template', $template);
    }

    public function view($id = null) {
        $template = $this->NotificationTemplates->get($id);
        $this->set(compact('template'));
    }

    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);

        $template = $this->NotificationTemplates->get($id);
        if ($this->NotificationTemplates->delete($template)) {
            return $this->redirect(['action' => 'index']);
        }
    }

}
