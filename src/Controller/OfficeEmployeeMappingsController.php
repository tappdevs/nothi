<?php
namespace App\Controller;

use App\Controller\ProjapotiController;
use App\Model\Table;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;

class OfficeEmployeeMappingsController extends ProjapotiController
{
    public function officeEmployeeMapping()
    {
        $office_hierarchy_table = TableRegistry::get('OfficeHierarchyTemplates');
        $office_hierarchys = $office_hierarchy_table->find()->select(['id', 'title_eng', 'title_bng'])->toArray();
        $office_hierarchy_list = array();
        foreach ($office_hierarchys as $oh) {
            $office_hierarchy_list[$oh['id']] = $oh['title_bng'] . "[" . $oh['title_eng'] . "]";
        }

        $this->set('office_hierarchy_list', $office_hierarchy_list);
    }


    /**
     * Load Office Organization View
     * Call Cell
     * request params: office hierarchy id
     **/
    public function loadOffices()
    {
        $this->layout = null;
        $this->set('ofc_hie_id', $this->request->query['ofc_hie_id']);
        $this->set('ofc_hie_name', $this->request->query['ofc_hie_name']);
        $this->set('parent_script', $this->request->query['parent_script_variable']);
    }

    /**
     * Load Office Organogram View
     * Call Cell
     * request params: office organization id
     **/
    public function loadOfficeOrganogram()
    {
        $this->layout = null;
        $this->set('ofc_orgnztn_id', $this->request->query['office_org_id']);
        $this->set('orgnztn_name', $this->request->query['office_org_name']);
    }

    /**
     * Load Office Organogram View
     * Call Cell
     * request params: office organization id
     **/
    public function loadOfficeEmployees()
    {
        $this->layout = null;
    }

    /**
     * Save Office Employee and Designation
     */
    public function save()
    {
        $office_organogram_id = $this->request->data['office_organogram_id'];
        $request_arr = explode("_", $office_organogram_id);
        $office_organogram_id = $request_arr[count($request_arr) - 1];
        $this->request->data['office_organogram_id'] = $office_organogram_id;

        $office_employee_mapping_table = TableRegistry::get('OfficeEmployeeMappings');
        $office_employee_mapping = $office_employee_mapping_table->newEntity();
        $office_employee_mapping = $office_employee_mapping_table->patchEntity($office_employee_mapping, $this->request->data);

        if ($result = $office_employee_mapping_table->save($office_employee_mapping)) {
            $this->response->body(json_encode(1));
            $this->response->type('application/json');
            return $this->response;
        } else {
            $this->response->body(json_encode(0));
            $this->response->type('application/json');
            return $this->response;
        }
    }

    public function officeFrontDeskManager()
    {
        $selectedOffice = $this->getCurrentDakSection();
        $officeid = $selectedOffice['office_id'];
        $officeFrontDeskTable = TableRegistry::get('OfficeFrontDesk');
        $currentFrontDesk = $officeFrontDeskTable->find()->where(['office_id' => $officeid])->first();
        $results = $this->officeDesignations($officeid);
        if (!empty($results)) {
            foreach ($results as $key => &$value) {
                $value['is_current'] = ($value['office_unit_id'] == $currentFrontDesk['office_unit_id'] && $value['office_unit_organogram_id'] == $currentFrontDesk['office_unit_organogram_id']) ? 1 : 0;
            }
        }
        $this->set(compact('results'));
    }

    public function assignFronDesk($id)
    {
        $officeEmployeeTable = TableRegistry::get('EmployeeOffices');
        $table_offices = TableRegistry::get('Offices');
        $officeFrontDeskTable = TableRegistry::get('OfficeFrontDesk');
        $employeeoffices = $this->getCurrentDakSection();
        try {
            $conn = ConnectionManager::get('default');

            $conn->begin();
            $getData = $officeEmployeeTable->find()
                ->select(['EmployeeOffices.id',
                    'EmployeeOffices.employee_record_id',
                    'EmployeeOffices.office_id',
                    'EmployeeOffices.office_unit_id',
                    'EmployeeOffices.office_unit_organogram_id',
                    'OfficeUnits.unit_name_bng',
                    'OfficeUnitOrganograms.designation_bng',
                    "name_bng" => 'EmployeeRecords.name_bng'
                ])
                ->join([
                    'EmployeeRecords' => [
                        'table' => 'employee_records',
                        'type' => 'inner',
                        'conditions' => 'EmployeeOffices.employee_record_id = EmployeeRecords.id'
                    ],
                    'OfficeUnits' => [
                        'table' => 'office_units',
                        'type' => 'inner',
                        'conditions' => 'OfficeUnits.id = EmployeeOffices.office_unit_id'
                    ],
                    'OfficeUnitOrganograms' => [
                        'table' => 'office_unit_organograms',
                        'type' => 'inner',
                        'conditions' => 'OfficeUnitOrganograms.id = EmployeeOffices.office_unit_organogram_id'
                    ]
                ])
                ->where(['EmployeeOffices.id' => $id])->first();

            $officeFrontDeskTable->deleteAll(['office_id' => $employeeoffices['office_id']]);

            $officeInformation = $table_offices->get($getData['office_id']);

            $newEntity = $officeFrontDeskTable->newEntity();
            $newEntity->office_id = $getData['office_id'];
            $newEntity->office_name = $officeInformation->office_name_bng;
            $newEntity->office_address = $officeInformation->office_address;
            $newEntity->office_unit_id = $getData['office_unit_id'];
            $newEntity->office_unit_name = $getData['OfficeUnits']['unit_name_bng'];
            $newEntity->office_unit_organogram_id = $getData['office_unit_organogram_id'];
            $newEntity->designation_label = $getData['OfficeUnitOrganograms']['designation_bng'];
            $newEntity->officer_id = $getData['employee_record_id'];
            $newEntity->officer_name = $getData['name_bng'];
            $officeFrontDeskTable->save($newEntity);
            $conn->commit();
            echo 1;
        } catch (Exception $e) {
            echo 0;
            $conn->rollback();
        }

        die;
    }

    public function officeHeadManager($office_id = 0)
    {
        try{
            $user = $this->Auth->user();
            if(!empty($user) && $user['user_role_id'] <= 2){
                if(empty($office_id)){
                    $this->set('redirect_url','officeEmployeeMappings/officeHeadManager/');
//                    $this->render('/officeManagement/update_office_info');
                    $this->view = '/OfficeManagement/update_office_info';
                    return;
                }
                $officeid = $office_id;
            }else{
                $session = $this->request->session();
                if($session->read('module_id') != 2){
                    $module_id = 2;
                    $session->write('module_id', $module_id);
                }
                $selectedOffice = $this->getCurrentDakSection();
                $officeid = $selectedOffice['office_id'];
            }
            $officeInfo = TableRegistry::get('Offices')->getBanglaName($officeid);
            if(!empty($officeInfo['office_name_bng'])){
                $this->set('office_name',$officeInfo['office_name_bng']);
            }
            $results = $this->officeDesignations($officeid);
            $this->set(compact('results'));
        }catch (\Exception $ex){
            $this->Flash->error(__('Technical error happen'));
        }

    }

    public function assignOfficeHead($id)
    {
        $officeEmployeeTable = TableRegistry::get('EmployeeOffices');
        $potrojariGroupsMigTable = TableRegistry::get('PotrojariGroupsUsers');
        try {
            $conn = ConnectionManager::get('default');
            $conn->begin();

            $getData = $officeEmployeeTable->get($id);

            //check if office admin doing this
            $user = $this->Auth->user();
            if(!empty($user) && $user['user_role_id'] > 2){
                $employee_office = $this->getCurrentDakSection();
                if($employee_office['office_id'] != $getData['office_id']){
                    //unauthorized access
                    echo 0;
                    die;
                }
            }
            //for PJ group
            $getPrevData= $officeEmployeeTable->find()->where(['office_id'=>$getData['office_id'],'office_head'=>1,'status' => 1])->first();
            //first reset all office head to 0
            if(!empty($getPrevData)){
                //update all row as multiple row can be for that designation
                $officeEmployeeTable->updateAll(['office_head' => 0],['office_id' => $getData['office_id'],'office_head' => 1,'status' => 1]);
//                $getPrevData->office_head = 0;
//            $officeEmployeeTable->save($getPrevData);
            }
            $getData->office_head = 1;
            $officeEmployeeTable->save($getData);
            if($potrojariGroupsMigTable->updatePotrojariGroupOfficeHead($getData->office_unit_organogram_id,(!empty($getPrevData)? $getPrevData->office_unit_organogram_id :''))){
                $conn->commit();
                echo 1;
            }
        } catch (Exception $e) {
            echo 0;
            $conn->rollback();
        }

        die;
    }

    private function officeDesignations($officeid)
    {
        $table_employee_office = TableRegistry::get('EmployeeOffices');
        $query = $table_employee_office->find()->select([
            'EmployeeOffices.office_unit_id',
            'EmployeeOffices.id',
            'EmployeeOffices.office_unit_organogram_id',
            'EmployeeOffices.designation',
            'EmployeeOffices.employee_record_id',
            'EmployeeOffices.office_head',
            "unit_name_bng" => 'OfficeUnits.unit_name_bng',
            "personal_mobile" => 'EmployeeRecords.personal_mobile',
            "personal_email" => 'EmployeeRecords.personal_email',
            "name_bng" => 'EmployeeRecords.name_bng',
            "name_eng" => 'EmployeeRecords.name_eng',
            "identity_no" => 'EmployeeRecords.identity_no',
            "is_cadre" => 'EmployeeRecords.is_cadre',
            "username" => 'Users.username'
        ])->join([
            'OfficeUnits' => [
                'table' => 'office_units',
                'type' => 'left',
                'conditions' => 'EmployeeOffices.office_unit_id = OfficeUnits.id'
            ],
            'EmployeeRecords' => [
                'table' => 'employee_records',
                'type' => 'inner',
                'conditions' => 'EmployeeOffices.employee_record_id = EmployeeRecords.id'
            ],
            'Users' => [
                'table' => 'users',
                'type' => 'inner',
                'conditions' => 'EmployeeRecords.id = Users.employee_record_id'
            ]
        ])->where(['EmployeeOffices.office_id' => $officeid, 'EmployeeOffices.status' => 1])->order(['EmployeeOffices.designation_level ASC, EmployeeOffices.designation_sequence ASC']);

        return $query->toArray();
    }
}
