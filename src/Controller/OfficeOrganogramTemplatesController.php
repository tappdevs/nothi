<?php
namespace App\Controller;

use App\Controller\ProjapotiController;
use App\Model\Table;
use App\Model\Table\OfficeOrganogramTemplatesTable;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\Entity;
use App\Model\Entity\OfficeOrganogramTemplates;
use Cake\ORM\TableRegistry;

class OfficeOrganogramTemplatesController extends ProjapotiController
{
//    public $paginate = [
//        'fields' => ['Districts.id'],
//        'limit' => 25,
//        'order' => [
//            'Districts.id' => 'asc'
//        ]
//    ];
//
//    public function initialize()
//    {
//        parent::initialize();
//        $this->loadComponent('Paginator');
//    }

    public function index()
    {
        $office_oraganogram_templates = TableRegistry::get('OfficeOrganogramTemplates');
        $query = $office_oraganogram_templates->find('all');
        $this->set(compact('query'));
    }

    public function add()
    {
        $this->layout = null;
        $parent_id = $this->request->query['parent_id'];
        $office_hierarchy_id = $this->request->query['office_hierarchy_id'];
        $office_oraganogram_templates = $this->OfficeOrganogramTemplates->newEntity();
        if ($this->request->is('post')) {
            parse_str($this->request->data['formdata'], $this->request->data);
            $office_oraganogram_templates = $this->OfficeOrganogramTemplates->patchEntity($office_oraganogram_templates, $this->request->data);
            if ($this->OfficeOrganogramTemplates->save($office_oraganogram_templates)) {
                $this->response->body(json_encode(1));
                $this->response->type('application/json');
                return $this->response;
            } else {
                $this->response->body(json_encode(0));
                $this->response->type('application/json');
                return $this->response;
            }
        }
        $this->set('parent', $parent_id);
        $this->set('office_hierarchy_id', $office_hierarchy_id);
        $this->set('office_oraganogram_templates', $office_oraganogram_templates);
    }

    public function edit($id = null)
    {
        $office_oraganogram_templates = $this->OfficeOrganogramTemplates->get($id);
        if ($this->request->is(['post', 'put'])) {
            $this->OfficeOrganogramTemplates->patchEntity($office_oraganogram_templates, $this->request->data);
            if ($this->OfficeOrganogramTemplates->save($office_oraganogram_templates)) {
                return $this->redirect(['action' => 'index']);
            }
        }
        $this->set('office_oraganogram_templates', $office_oraganogram_templates);
    }

    public function view($id = null)
    {
        $office_oraganogram_templates = $this->OfficeOrganogramTemplates->get($id);
        $this->set(compact('office_oraganogram_templates'));
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);

        $office_oraganogram_templates = $this->OfficeOrganogramTemplates->get($id);
        if ($this->OfficeOrganogramTemplates->delete($office_oraganogram_templates)) {
            return $this->redirect(['action' => 'index']);
        }
    }

    public function globalOfficeOrganogram()
    {
        $office_hierarchy_table = TableRegistry::get('OfficeHierarchyTemplates');
        $office_hierarchys = $office_hierarchy_table->find()->select(['id', 'title_eng', 'title_bng'])->toArray();
        $office_hierarchy_list = array();
        foreach ($office_hierarchys as $oh) {
            $office_hierarchy_list[$oh['id']] = $oh['title_bng'] . "[" . $oh['title_eng'] . "]";
            //$office_hierarchy_list[$oh['id']] = $oh['title_bng'];
        }

        $this->set('office_hierarchy_list', $office_hierarchy_list);
    }

    public function loadGlblOfcOrgHierarchy($parent = 0, $global_office_id = 0)
    {
        $request_data = $this->request->query['id'];
        $global_office_id = $this->request->query['office_hierarchy_id'];

        if ($request_data == '#') {
            $request_data = 'node_0_0';
        }

        $request_arr = explode("_", $request_data);
        foreach ($request_arr as $arr) {
            if (intval($arr) > 0) {
                continue;
            }
        }
        $parent = $request_arr[count($request_arr) - 1];

        $globalOrgTable = TableRegistry::get('OfficeOrganogramTemplates');
        $globalOfficeOrg = $globalOrgTable->find()->select(['id', 'title_eng', 'title_bng'])->where(['parent' => $parent, 'office_hierarchy_template_id' => $global_office_id])->order(['sequence'])->toArray();

        $data = array();

        /* Load Existing Data From Database*/
        foreach ($globalOfficeOrg as $div) {
            $row = array();
            $row["id"] = "node_" . $parent . "_" . $div['id'];
            $row["data-id"] = $div['id'];
            //$row["text"] = '<a href="javascript:;" data-parent-node="'.$request_data.'" data-id="'.$div['id'].'" data-parent="'.$parent.'" onclick="GeoTree.gotoEdit(this)">'.$div['title_bng'].'['.$div['title_eng'].']</a>';
            $row["text"] = '<a href="javascript:;" data-parent-node="' . $request_data . '" data-id="' . $div['id'] . '" data-parent="' . $parent . '" onclick="GeoTree.gotoEdit(this)">' . $div['title_bng'] . '</a>';
            $row["icon"] = "icon icon-arrow-right";
            $row["children"] = true;
            $row["type"] = "root";
            $data[] = $row;
        }

        /* Add node for new entry */
        $row = array();
        $row["id"] = "node_" . $parent . "_0";
        $row["data-id"] = 0;
        $row["text"] = '<a href="javascript:;" data-parent-node="' . $request_data . '" data-parent="' . $parent . '" data-id="0"  onclick="TreeView.newNode(this)"><i class="icon-plus"></i></a>';
        $row["icon"] = "icon icon-arrow-right";
        $row["children"] = false;
        $row["type"] = "root";
        $data[] = $row;

        $this->response->body(json_encode($data));
        $this->response->type('application/json');
        return $this->response;
    }
}
