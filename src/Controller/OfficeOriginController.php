<?php

namespace App\Controller;

use App\Controller\ProjapotiController;
use App\Model\Table;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\Log\Log;

class OfficeOriginController extends ProjapotiController
{
    /* Start: Office Unit Category */

    /**
     * Objective : Show All Office Unit Category
     */
    public function officeUnitCategories()
    {
        $table_instance = TableRegistry::get('OfficeUnitCategories');
        $data_items = $table_instance->find('all');
        $this->set(compact('data_items'));

        $this->render('/OfficeUnitCategories/index');
    }

    /**
     * Objective : Insert new office unit category
     */
    public function addCategory()
    {
        $table_instance = TableRegistry::get('OfficeUnitCategories');
        $new_entity = $table_instance->newEntity();
        $entity_with_data = $new_entity;

        if ($this->request->is('post')) {
            $entity_with_data = $table_instance->patchEntity($new_entity, $this->request->data);

            if ($table_instance->save($entity_with_data)) {
                $this->Flash->success("Content has been added successfully.");
                return $this->redirect(['action' => 'officeUnitCategories']);
            }
            $this->Flash->error("Failed to add content.");
        }

        $this->set('entity', $entity_with_data);
        $this->render('/OfficeUnitCategories/add');
    }

    /**
     * Objective : Update existing office unit category
     */
    public function editCategory($id = null)
    {
        $table_instance = TableRegistry::get('OfficeUnitCategories');

        /* Read Previous Data for view */
        $entity_with_data = $table_instance->get($id);

        if ($this->request->is(['post', 'put'])) {
            /* Replace old data with updated data before save  */
            $entity_with_data = $table_instance->patchEntity($entity_with_data, $this->request->data);

            if ($table_instance->save($entity_with_data)) {
                $this->Flash->success("Selected Content is updated.");
                return $this->redirect(['action' => 'officeUnitCategories']);
            }
            $this->Flash->error("Failed to update selected content.");
        }

        $this->set('entity', $entity_with_data);
        $this->render('/OfficeUnitCategories/edit');
    }

    /**
     * Objective : Delete existing office unit category
     */
    public function deleteCategory($id)
    {
        $this->request->allowMethod(['post', 'delete']);
        $table_instance = TableRegistry::get('OfficeUnitCategories');

        $entity_with_data = $table_instance->get($id);

        if ($table_instance->delete($entity_with_data)) {
            $this->Flash->success("Selected Content is deleted.");
        } else {
            $this->Flash->error("Failed to delete selected content.");
        }
        return $this->redirect(['action' => 'officeUnitCategories']);
    }

    /* |---------End: Office Unit Category ----------| */

    /* |--------- Start: Office Unit ----------| */

    public function officeOriginUnits()
    {
        $this->render('/OfficeOriginUnits/originUnits');
    }

    public function loadOfficeOriginUnits($parent = 0)
    {
        $node_id = $this->request->query['id'];
        $tree_type = $this->request->query['type']; // data update tree, selection tree
        $node_arr = explode("_", $node_id);
        $office_id = $this->request->query['office_id'];
        $root = $node_arr[count($node_arr) - 1];
        if ($root == '#') {
            $root = 0;
        }
        $table_instance = TableRegistry::get('OfficeOriginUnits');
        $data_items = $table_instance->find()->select(['id', 'unit_name_bng', 'office_origin_id'])->where(['office_origin_id' => $office_id, 'parent_unit_id' => $root, 'active_status' => 1])->order(['unit_level asc, parent_unit_id asc, id asc'])->toArray();

        $data = array();

        /* Load Existing Data From Database */
        foreach ($data_items as $div) {
            $row = array();
            $row["id"] = "node_" . $root . "_" . $div['id'];
            $row["data-id"] = $div['id'];
            $row["text"] = '<a href="javascript:;" data-parent-node="' . $node_id . '" data-id="' . $div['id'] . '" data-parent="' . $root . '" onclick="OfficeUnitView.gotoEdit(this)">' . $div['unit_name_bng'] . '</a>';
            $row["icon"] = "icon icon-arrow-right";
            $row["children"] = true;
            $row["type"] = "root";
            $data[] = $row;
        }

        if ($tree_type !== 'checkbox') {
            /* Add node for new entry */
            $row = array();
            $row["id"] = "node_" . $root . "_0";
            $row["data-id"] = 0;
            $row["text"] = '<a href="javascript:;" data-parent-node="' . $node_id . '" data-parent="' . $root . '" data-id="0"  onclick="OfficeUnitView.newNode(this)"><i class="icon-plus"></i></a>';
            $row["icon"] = "icon icon-arrow-right";
            $row["children"] = false;
            $row["type"] = "root";
            $data[] = $row;
        }

        $this->response->body(json_encode($data));
        $this->response->type('application/json');
        return $this->response;
    }

    public function addOfficeOriginUnit()
    {
        $table_instance = TableRegistry::get('OfficeOriginUnits');
        $new_entity = $table_instance->newEntity();
        $entity_with_data = $new_entity;

        if (isset($this->request->query['parent_id'])) {
            $this->set('parent', $this->request->query['parent_id']);
        }
        $this->layout = null;

        if ($this->request->is(['post', 'put'])) {
            parse_str($this->request->data['formdata'], $this->request->data);
            $entity_with_data = $table_instance->patchEntity($new_entity, $this->request->data);

            if ($table_instance->save($entity_with_data)) {
                $this->response->body(json_encode(1));
            } else {
                $this->response->body(json_encode(0));
            }

            $this->response->type('application/json');
            return $this->response;
        }

        $this->set('entity', $entity_with_data);
        $this->render('/OfficeOriginUnits/add');
    }

    public function editOfficeOriginUnit($id)
    {
        $table_instance = TableRegistry::get('OfficeOriginUnits');
        $entity_with_data = $table_instance->get($id);

        $parent_id = $table_instance->find('list', [
            'keyField' => 'id',
            'valueField' => 'unit_name_bng'
        ])->where(['office_origin_id' => $entity_with_data['office_origin_id'],'active_status'=>1])->order(['unit_level asc, parent_unit_id asc, id asc'])->toArray();
        $this->layout = null;

        if ($this->request->is(['post', 'put'])) {
            parse_str($this->request->data['formdata'], $this->request->data);
            $entity_with_data = $table_instance->patchEntity($entity_with_data, $this->request->data);

            if ($table_instance->save($entity_with_data)) {
                $this->response->body(json_encode(1));
            } else {
                $this->response->body(json_encode(0));
            }

            $this->response->type('application/json');
            return $this->response;
        }

        $this->set('parent_id', $parent_id);
        $this->set('entity', $entity_with_data);
        $this->render('/OfficeOriginUnits/edit');
    }

    public function deleteOfficeOriginUnit($id)
    {
        $table_instance = TableRegistry::get('OfficeOriginUnits');
        $officeUnits = TableRegistry::get('OfficeUnits');
        $officeOriginOrganogramUnits = TableRegistry::get('OfficeOriginUnitOrganograms');
        $entity_with_data = $table_instance->get($id);

        $this->layout = null;

        if (!empty($id) && !empty($entity_with_data)) {

            $isLinked = $officeOriginOrganogramUnits->getOriginUnitInfo($id);
            $isLinkedAnother = $officeUnits->getInfoByOfficeUnitId($id);
            $isParent = $table_instance->find()->where(['parent_unit_id' => $id, 'active_status' => 1])->toArray();
            if (empty($isLinked) && empty($isLinkedAnother) && empty($isParent)) {

                $entity_with_data->active_status = 0;

                if ($table_instance->save($entity_with_data)) {
                    $this->response->body(json_encode(1));
                } else {
                    $this->response->body(json_encode("Cannot delete the node!"));
                }
            } else {
                $this->response->body(json_encode((!empty($isLinked)?"This origin already been used as one or more offices Organogram":(!empty($isLinkedAnother)?'This origin already been used as one or more offices Units':(!empty($isParent)?"Please delete child units first":'Unknown error occurs')))));
            }
        }
        $this->response->type('application/json');
        return $this->response;
    }

    public function deleteOriginUnitOrganogram($id)
    {
        $table_instance = TableRegistry::get('OfficeOriginUnitOrganograms');
        $officeUnitOrganograms = TableRegistry::get('OfficeUnitOrganograms');
        $entity_with_data = $table_instance->get($id);

        $this->layout = null;

        if (!empty($id) && !empty($entity_with_data)) {
            $isLinked = $officeUnitOrganograms->getUnitOrganogramInfo($id);
            $isParent = $table_instance->find()->where(['superior_designation_id' => $id, 'status' => 1])->toArray();
            if (empty($isLinked) && empty($isParent)) {

                $entity_with_data->status = 0;

                if ($table_instance->save($entity_with_data)) {
                    $this->response->body(json_encode(1));
                } else {
                    $this->response->body(json_encode("Cannot delete the node!"));
                }
            } else {
                $this->response->body(json_encode((!empty($isLinked)?"This origin already been used as one or more offices Organogram":(!empty($isParent)?'Please delete child origin first':'Unknown error occurs'))));
            }
        }
        $this->response->type('application/json');
        return $this->response;
    }

    /* End: Office Origin Unit */

    /*
     * ||
     */
    /* |--------- Start: Office Unit ----------| */

    public function originUnitOrganograms()
    {
        $this->render('/OfficeOriginUnitOrganograms/originUnitOrganograms');
    }

    public function loadOriginUnitOrganograms($parent = 0)
    {
        /* Request Parameters */
        $node_id = $this->request->query['id'];
        $tree_type = $this->request->query['type']; // data update tree, selection tree
        $office_id = $this->request->query['office_id'];

        $node_arr = explode(":", $node_id);
        $parent_node = $node_arr[0];
        $root = isset($node_arr[1]) ? $node_arr[1] : "0";
        if ($root == '#') {
            $root = 0;
        }
        $parent_node_arr = explode("_", $parent_node);
        $node_type = $parent_node_arr[0];
        $child_node_prefix = str_replace(':', '_', $node_id);
        $child_node_prefix = str_replace('_unit', '', $child_node_prefix);
        $child_node_prefix = str_replace('_designation', '', $child_node_prefix);

        $data = array();

        /* Check is unit or designation */
        if ($node_type == 'unit') {
            if ($root == 'unit') {
                $child_node_arr = explode("_", $child_node_prefix);
                $parent_unit_id = $child_node_arr[count($child_node_arr) - 1];

                $table_instance = TableRegistry::get('OfficeOriginUnits');
                $data_item_units = $table_instance->find()->select(['id', 'unit_name_bng', 'office_origin_id'])->where(['office_origin_id' => $office_id, 'parent_unit_id' => $parent_unit_id, 'active_status' => 1])->order(['unit_level asc, parent_unit_id asc, id asc'])->toArray();

                /* Load Existing Data Units From Database */
                foreach ($data_item_units as $unit) {
                    $row = array();
                    $row["id"] = $child_node_prefix . ":" . $unit['id'];
                    $row["data-id"] = $unit['id'];
                    $row["text"] = '<a href="javascript:;" data-node-id="' . $row["id"] . '" data-id="' . $row["data-id"] . '" onclick="OfficeUnitOrgView.gotoEdit(this)">' . $unit['unit_name_bng'] . '</a>';
                    $row["icon"] = "icon icon-home";
                    $row["children"] = true;
                    $row["type"] = "root";
                    $data[] = $row;
                }
            } else {
                $row_designation = array();
                $row_designation["id"] = str_replace('unit', 'designation', $child_node_prefix) . ":designation";
                $row_designation["data-id"] = "designation";
                $row_designation["text"] = '<a href="javascript:;" data-node-id="' . $row_designation["id"] . '" data-id="' . $row_designation["data-id"] . '" onclick="OfficeUnitOrgView.gotoEdit(this)">Office Stuffs</a>';
                $row_designation["icon"] = "icon icon-user";
                $row_designation["children"] = true;
                $row_designation["type"] = "root";
                $data[] = $row_designation;

                $row_unit = array();
                $row_unit["id"] = $child_node_prefix . ":unit";
                $row_unit["data-id"] = "unit";
                $row_unit["text"] = '<a href="javascript:;" data-node-id="' . $row_unit["id"] . '" data-id="' . $row_unit["data-id"] . '" onclick="OfficeUnitOrgView.gotoEdit(this)">Doptor/Section</a>';
                $row_unit["icon"] = "icon icon-home";
                $row_unit["children"] = true;
                $row_unit["type"] = "root";
                $data[] = $row_unit;
            }
        } else if ($node_type == 'designation') {
            if ($root == 'designation') {
                $child_node_arr = explode("_", $child_node_prefix);
                $origin_unit_id = $child_node_arr[count($child_node_arr) - 1];

                $table_instance_origin_unit_org = TableRegistry::get('OfficeOriginUnitOrganograms');
                $data_item_designations = $table_instance_origin_unit_org->find()->select(['id', 'designation_bng', 'office_origin_unit_id'])->where(['office_origin_unit_id' => $origin_unit_id, 'status' => 1])->order(['designation_level ASC'])->toArray();

                /* Load Existing Designation From Database */
                foreach ($data_item_designations as $designation) {
                    $row = array();
                    $row["id"] = $child_node_prefix . ":" . $designation['id'];
                    $row["data-id"] = $designation['id'];
                    $row["text"] = '<a href="javascript:;" data-node-id="' . $row["id"] . '" data-id="' . $row["data-id"] . '" onclick="OfficeUnitOrgView.gotoEdit(this)">' . $designation['designation_bng'] . '</a>';
                    $row["icon"] = "icon icon-home";
                    $row["children"] = true;
                    $row["type"] = "root";
                    $data[] = $row;
                }
            }
        } else {
            /* Query Data : Unit */
            $table_instance = TableRegistry::get('OfficeOriginUnits');
            $data_item_units = $table_instance->find()->select(['id', 'unit_name_bng', 'office_origin_id'])->where(['office_origin_id' => $office_id, 'parent_unit_id' => $root, 'active_status' => 1])->order(['unit_level asc, parent_unit_id asc, id asc'])->toArray();

            /* Load Existing Data From Database */
            foreach ($data_item_units as $unit) {
                $row = array();
                $row["id"] = "unit_" . $root . ":" . $unit['id'];
                $row["data-id"] = $unit['id'];
                $row["text"] = '<a href="javascript:;" data-node-id="' . $row["id"] . '" data-id="' . $row["data-id"] . '" onclick="OfficeUnitOrgView.gotoEdit(this)">' . $unit['unit_name_bng'] . '</a>';
                $row["icon"] = "icon icon-home";
                $row["children"] = true;
                $row["type"] = "root";
                $data[] = $row;
            }
        }

        if ($root == 'designation') {
            /* Add node for new entry */
            $row = array();
            $row["id"] = str_replace('unit', 'designation', $child_node_prefix) . ":0";
            $row["data-id"] = 0;
            $row["text"] = '<a href="javascript:;" data-parent-node="' . $node_id . '" data-parent="' . $root . '" data-id="0"  onclick="OfficeUnitOrgView.newNode(this)"><i class="icon-plus"></i></a>';
            $row["icon"] = "icon icon-arrow-right";
            $row["children"] = false;
            $row["type"] = "root";
            $data[] = $row;
        }

        $this->response->body(json_encode($data));
        $this->response->type('application/json');
        return $this->response;
    }

    public function addOriginUnitOrganogram()
    {
        $table_instance = TableRegistry::get('OfficeOriginUnitOrganograms');
        $new_entity = $table_instance->newEntity();
        $entity_with_data = $new_entity;

        if (isset($this->request->query['parent_id'])) {
            $parent_id_arr = explode("_", $this->request->query['parent_id']);

            $this->set('parent', $parent_id_arr[count($parent_id_arr) - 2]);
        }

        if (isset($this->request->query['office_origin_id'])) {
            $this->set('office_origin_id', $this->request->query['office_origin_id']);
        }
        $this->layout = null;

        if ($this->request->is(['post', 'put'])) {
            parse_str($this->request->data['formdata'], $this->request->data);

            $entity_with_data = $table_instance->patchEntity($new_entity, $this->request->data);

            if(empty($entity_with_data->superior_unit_id)){
                $entity_with_data->superior_unit_id = 0;
            }
            if(empty($entity_with_data->short_name_bng)){
                $entity_with_data->short_name_bng = $entity_with_data->designation_bng;
            }
            if(empty($entity_with_data->short_name_eng)){
                $entity_with_data->short_name_eng = $entity_with_data->designation_eng;
            }
            if(empty($entity_with_data->designation_level)){
                $entity_with_data->designation_level = 0;
            }
            if(empty($entity_with_data->designation_sequence)){
                $entity_with_data->designation_sequence = 0;
            }
            $entity_with_data->created_by = $this->Auth->user('id');
            $entity_with_data->modified_by = $this->Auth->user('id');
            if ($table_instance->save($entity_with_data)) {
                $this->response->body(json_encode(1));
            } else {
                $this->response->body(json_encode(0));
            }

            $this->response->type('application/json');
            return $this->response;
        }

        $this->set('entity', $entity_with_data);
        $this->render('/OfficeOriginUnitOrganograms/add');
    }

    public function editOriginUnitOrganogram($id)
    {
        $table_instance = TableRegistry::get('OfficeOriginUnitOrganograms');
        $entity_with_data = $table_instance->get($id);

        $this->layout = null;

        if (isset($this->request->query['office_origin_id'])) {
            $this->set('office_origin_id', $this->request->query['office_origin_id']);
        }

        if ($this->request->is(['post', 'put'])) {
            parse_str($this->request->data['formdata'], $this->request->data);
            $entity_with_data = $table_instance->patchEntity($entity_with_data, $this->request->data);

            if ($table_instance->save($entity_with_data)) {
                $this->response->body(json_encode(1));
            } else {
                $this->response->body(json_encode(0));
            }

            $this->response->type('application/json');
            return $this->response;
        }

        $this->set('entity', $entity_with_data);
        $this->render('/OfficeOriginUnitOrganograms/edit');
    }

    public function loadOriginUnitStuffs()
    {
        $origin_unit_id = $this->request->data['origin_unit_id'];

        $table_instance = TableRegistry::get('OfficeOriginUnitOrganograms');
        $data_items = $table_instance->find('list')->where(['office_origin_unit_id' => $origin_unit_id, 'status' => 1])->order(['designation_level ASC'])->toArray();
        $this->response->body(json_encode($data_items));
        $this->response->type('application/json');
        return $this->response;
    }

    /* End: Office Origin Unit */
    /*
     * |----|
     */

    /*
     * Start: Actual Office Management 
     */

    public function officeManagement()
    {

    }

}
