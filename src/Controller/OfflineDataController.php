<?php
namespace App\Controller;

use App\Controller\ProjapotiController;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\TableRegistry;

class OfflineDataController extends ProjapotiController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
    }

    private function checkRequest(){
        if(!$this->request->is('ajax')){
            die;
        }
    }
    
    private function getEmployeeRecords(){
        $table = TableRegistry::get('EmployeeRecords');
        
        return $table->getAll(['status'=>1],['_id'=>'id','id',  'name_bng', 'date_of_birth', 'gender','personal_email','personal_mobile','is_cadre','identity_no','status'])->toArray();
    }
    
    private function getEmployeeOffices(){
        $table = TableRegistry::get('EmployeeOffices');
        
        return $table->getAll(['status'=>1],['_id'=>'id', 'id', 'employee_record_id', 'identification_number', 'office_id','office_unit_id','office_unit_organogram_id','designation','designation_level','designation_sequence','is_default_role','status'])->toArray();
    }
    
    private function getOfficeUnits(){
        $table = TableRegistry::get('OfficeUnits');
        
        return $table->getAll(['active_status'=>1],['_id'=>'id','id',  'office_ministry_id', 'office_layer_id','office_id', 'office_origin_unit_id', 'unit_name_bng', 'parent_unit_id', 'unit_nothi_code','active_status'])->toArray();
    }
    
    private function getOfficeUnitOrganograms(){
        $table = TableRegistry::get('OfficeUnitOrganograms');
        
        return $table->getAll(['status'=>1],['_id'=>'id','id',  'office_unit_id', 'superior_designation_id','office_id', 'superior_designation_id', 'designation_bng', 'short_name_bng', 'designation_level', 'designation_sequence','status'])->toArray();
    }
    
    private function getOffices(){
        $table = TableRegistry::get('Offices');
        
        return $table->getAll(['active_status'=>1],['_id'=>'id','id',  'office_ministry_id', 'office_layer_id','office_origin_id', 'office_name_bng', 'parent_office_id', 'office_address', 'digital_nothi_code', 'reference_code','office_code','office_phone','office_mobile','office_fax','office_email','office_web','active_status'])->toArray();
    }
    
    private function getOrigins(){
        $table = TableRegistry::get('OfficeOrigins');
        
        return $table->getAll(['active_status'=>1],['_id'=>'id','id',  'office_ministry_id', 'office_layer_id', 'office_name_bng', 'parent_office_id', 'office_level', 'office_sequence', 'active_status'])->toArray();
    }
    
    private function getLayers(){
        $table = TableRegistry::get('OfficeLayers');
        
        return $table->getAll("",['_id'=>'id', 'id', 'office_ministry_id', 'parent_layer_id', 'layer_name_bng', 'layer_level', 'layer_sequence', 'active_status'])->toArray();
    }
    
    private function getMinistries(){
        $table = TableRegistry::get('OfficeMinistries');
        
        return $table->getAll("",['_id'=>'id','id',  'office_type', 'name_bng', 'reference_code'])->toArray();
    }
    
    public function getData(){
        $this->checkRequest();
        $this->layout = null;
        
        $resultDataArray = array();

        $resultDataArray['employee_offices'] = $this->getEmployeeOffices();
        $resultDataArray['office_units'] = $this->getOfficeUnits();
        $resultDataArray['office_unit_organograms'] = $this->getOfficeUnitOrganograms();
        $resultDataArray['offices'] = $this->getOffices();
        $resultDataArray['origins'] = $this->getOrigins();
        $resultDataArray['layers'] = $this->getLayers();
        $resultDataArray['ministries'] = $this->getMinistries();
        
        echo json_encode(array('status'=>'success', 'data'=>$resultDataArray));
        die;
    }
    

}