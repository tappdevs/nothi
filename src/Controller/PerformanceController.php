<?php

namespace App\Controller;

use App\Controller\ProjapotiController;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\TableRegistry;
use Cake\I18n\Number;
use Cake\I18n\Time;
use Cake\I18n\DateTime;
use Cake\Cache\Cache;

class PerformanceController extends ProjapotiController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->loadComponent('Paginator');
        $this->Auth->allow(['recordFromBeginningforOffices', 'recordFromBeginningforUnits', 'recordFromBeginningforDesignations',
            'performaceOfficeUpdate', 'performanceUnitUpdate', 'performanceDesignationUpdate', 'getNoteData', 'superadminOfficeContent', 'testPerformanceOneMonth', 'updatePerformanceDesignations', 'updatePerformanceUnits', 'updatePerformanceOffices']);
        //recordFromBeginningforOffices
    }

    public function office()
    {
        $selected_office_section = $this->getCurrentDakSection();
        $this->set(compact('selected_office_section'));
        $OfficesTable = TableRegistry::get('Offices');
        if (empty($selected_office_section)) {
            $ChildOffices = $OfficesTable->find('list')->where(['active_status' => 1])->toArray();
        } else {
            $ChildOffices = $OfficesTable->getIdandNameofChildOffices($selected_office_section['office_id']);
            $selfOffice[$selected_office_section['office_id']] = $selected_office_section['office_name'];
            $ChildOffices = $selfOffice + $ChildOffices;
        }

        //pr($ChildOffices);die;
        $this->set(compact('ChildOffices'));
    }

    public function newOfficeContent($startDate = '', $endDate = '')
    {
        set_time_limit(0);
        $this->layout = '';
        $selected_office_section = $this->getCurrentDakSection();
        $reportsTable = TableRegistry::get('Reports');
        $performanceOfficesTable = TableRegistry::get('PerformanceOffices');
        $OfficesTable = TableRegistry::get('Offices');
        $employee_offices_table = TableRegistry::get('EmployeeOffices');
        $startDate = !empty($startDate) ? $startDate : date("Y-m-d");
        $endDate = !empty($endDate) ? $endDate : date("Y-m-d");

        $office_info = [];
        $data = [];
        $single_heading = 0;
        if (!empty($this->request->data)) {
            $single_heading = isset($this->request->data['single_heading'])?$this->request->data['single_heading']:0;
            $office_id = $this->request->data['office_id'];
            $office_name =isset($this->request->data['office_name'])?$this->request->data['office_name']:'';
            $this->set(compact('office_name'));
            if ($office_id == 0) {
                if (empty($selected_office_section)) {
                    $ChildOffices = $OfficesTable->find('list')->where(['active_status' => 1])->toArray();
                } else {
                    $request_come_from = strpos($this->referer(), 'districtWiseOfficeReport');
                    if($request_come_from === false) {
                        // from perforamcne/Office
						$request_come_from = strpos($this->referer(), 'customOfficeReport');
						if ($request_come_from === false) {
							$ChildOffices = $OfficesTable->getIdandNameofChildOffices($selected_office_section['office_id']);
							$selfOffice[$selected_office_section['office_id']] = $selected_office_section['office_name'];
							$ChildOffices = $selfOffice + $ChildOffices;
						} else {
							$OfficesTable = TableRegistry::get('Offices');
							$currentOffice = $OfficesTable->get($selected_office_section['office_id']);
							$office_origin_id = basename($this->referer());
							$districtLevelOffices = $OfficesTable->find('list')->where(['office_origin_id' => $office_origin_id, 'geo_district_id' => $currentOffice->geo_district_id])->toArray();
							$ChildOffices = [$selected_office_section['office_id'] => $selected_office_section['office_name']] + $districtLevelOffices;
						}
                    }else{
                        $currentOffice = $OfficesTable->get($selected_office_section['office_id']);
                        $districtOfficeLayers = TableRegistry::get('OfficeLayers')->find('list')->where(['layer_level' => 5])->toArray();
                        $districtLevelOffices = $OfficesTable->find('list')->where(['office_layer_id IN' => array_keys($districtOfficeLayers), 'geo_district_id' => $currentOffice->geo_district_id])->toArray();
                        $ChildOffices = $districtLevelOffices;
                        $selfOffice[$selected_office_section['office_id']] = $selected_office_section['office_name'];
                        $ChildOffices = $selfOffice + $ChildOffices;
                    }
                }

                foreach ($ChildOffices as $key => $val) {
                    try {
                        $today = date('Y-m-d');
                        $yesterday = date('Y-m-d', strtotime($today . ' -1 day'));
                            $condition = [
                                'totalInbox' => 'SUM(inbox)', 'totalOutbox' => 'SUM(outbox)', 'totalNothijato' => 'SUM(nothijat)',
                                'totalNothivukto' => 'SUM(nothivukto)', 'totalNisponnoDak' => 'SUM(nisponnodak)',
                                'totalSouddog' => 'SUM(selfnote)',
                                'totalDaksohoNote' => 'SUM(daksohonote)', 'totalNisponnoPotrojari' => 'SUM(nisponnopotrojari)',
                                'totalNisponnoNote' => 'SUM(nisponnonote)',
                                'totalNisponno' => 'SUM(nisponno)', 'totalONisponno' => 'SUM(onisponno)',
//                                'unassignedPendingDak' => 'SUM(unassigned_pending_dak)','unassignedPendingNote' => 'SUM(unassigned_pending_note)',
//                                'unassignedDesignation' => 'SUM(unassigned_designation)',
                                'internal_potrojari' => 'SUM(potrojari_nisponno_internal)','external_potrojari' => 'SUM(potrojari_nisponno_external)',
                                'totalONisponnoDak' => 'SUM(onisponnodak)','totalONisponnoNote' => 'SUM(onisponnonote)',
                                'totalID' => 'count(id)'
                            ];

                            $result = $performanceOfficesTable->getOfficeData($key,
                                [$startDate, $endDate])->select($condition)->group(['office_id'])->toArray();
                            if (!empty($result[0]['totalID']) && $result[0]['totalID'] > 0) {
                                $result[0]['totalUser'] = $employee_offices_table->getCountOfEmployeeOfOffices($key);
                                $lastonisponno = $performanceOfficesTable->getLastOnisponno($key,
                                    [$startDate, $endDate]);
                                $result[0]['totalONisponnoDak'] = $lastonisponno['onisponnodak'];
                                $result[0]['totalONisponnoNote'] = $lastonisponno['onisponnonote'];
                                 $result[0]['unassignedPendingDak'] = $lastonisponno['unassignedPendingDak'];
                                $result[0]['unassignedPendingNote'] = $lastonisponno['unassignedPendingNote'];
                                $result[0]['unassignedDesignation'] = $lastonisponno['unassignedDesignation'];
                                $result[0]['totalNisponnoPotrojari'] = $result[0]['internal_potrojari'] +$result[0]['external_potrojari'];
                            } else {
                                $this->switchOffice($key, 'OfficeDB');
                                $result = $reportsTable->performanceReport($key, 0, 0,
                                    ['date_start' => $startDate, 'date_end' => $startDate]);
                            }

//                        pr($result);
//                        die;
                        $result[0]['lastUpdate'] =$performanceOfficesTable->getLastUpdateTime($key)['record_date'];
                        $result[0]['lastUpdate'] =(!empty($result[0]['lastUpdate']))? $result[0]['lastUpdate']->format('Y-m-d'):'';
                        $data[$key] = $result[0];
                        $data[$key]['name'] = $val;
                    } catch (\Exception $ex) {
                        continue;
                    }
                }
            } else {
                $office_info = $OfficesTable->getBanglaName($office_id);
                try {
                    $today = date('Y-m-d');
                    $yesterday = date('Y-m-d', strtotime($today . ' -1 day'));
                    $condition = [
                        'totalInbox' => 'SUM(inbox)', 'totalOutbox' => 'SUM(outbox)', 'totalNothijato' => 'SUM(nothijat)',
                        'totalNothivukto' => 'SUM(nothivukto)', 'totalNisponnoDak' => 'SUM(nisponnodak)',
                        'totalSouddog' => 'SUM(selfnote)',
                        'totalDaksohoNote' => 'SUM(daksohonote)', 'totalNisponnoPotrojari' => 'SUM(nisponnopotrojari)',
                        'totalNisponnoNote' => 'SUM(nisponnonote)',
                        'totalNisponno' => 'SUM(nisponno)', 'totalONisponno' => 'SUM(onisponno)',
//                             'unassignedPendingDak' => 'SUM(unassigned_pending_dak)','unassignedPendingNote' => 'SUM(unassigned_pending_note)',
//                            'unassignedDesignation' => 'SUM(unassigned_designation)',
                         'internal_potrojari' => 'SUM(potrojari_nisponno_internal)','external_potrojari' => 'SUM(potrojari_nisponno_external)',
                         'totalONisponnoDak' => 'SUM(onisponnodak)','totalONisponnoNote' => 'SUM(onisponnonote)',
                        'totalID' => 'count(id)'
                    ];

                    $result = $performanceOfficesTable->getOfficeData($office_id, [$startDate, $endDate])->select($condition)->group(['office_id'])->toArray();
                    if (!empty($result[0]['totalID']) && $result[0]['totalID'] > 0) {
                        $result[0]['totalUser'] = $employee_offices_table->getCountOfEmployeeOfOffices($office_id);
                        $lastonisponno = $performanceOfficesTable->getLastOnisponno($office_id,
                                [$startDate, $endDate]);
                            $result[0]['totalONisponnoDak'] = $lastonisponno['onisponnodak'];
                            $result[0]['totalONisponnoNote'] = $lastonisponno['onisponnonote'];
                            $result[0]['unassignedPendingDak'] = $lastonisponno['unassignedPendingDak'];
                            $result[0]['unassignedPendingNote'] = $lastonisponno['unassignedPendingNote'];
                            $result[0]['unassignedDesignation'] = $lastonisponno['unassignedDesignation'];
                            $result[0]['totalNisponnoPotrojari'] = $result[0]['internal_potrojari'] +$result[0]['external_potrojari'];
                    } else {
                        try {
                            $this->switchOffice($office_id, 'OfficeDB');
                            $result = $reportsTable->performanceReport($office_id, 0, 0, ['date_start' => $startDate, 'date_end' => $endDate]);
                        } catch (\Exception $exception) {
                            if ($single_heading == 1) {
                                echo '<tr><td colspan="15"><span style="color:red;display:block;"><u><b>'.$office_info["office_name_bng"].'</b></u>-এর অফিস ডাটাবেজ সংযোগ হচ্ছে না, অনুগ্রহপূর্বক কিছুক্ষণ পর পুনরায় চেষ্টা করুন।</span></td></tr>';
                                die;
                            } else {
                                echo '<span style="color:red;display:block;"><u><b>' . $office_info["office_name_bng"] . '</b></u>-এর অফিস ডাটাবেজ সংযোগ হচ্ছে না, অনুগ্রহপূর্বক কিছুক্ষণ পর পুনরায় চেষ্টা করুন।</span>';
                                die;
                            }
                        }
                    }
                    $result[0]['lastUpdate'] =$performanceOfficesTable->getLastUpdateTime($office_id)['record_date'];
                    $result[0]['lastUpdate'] =(!empty($result[0]['lastUpdate']))? $result[0]['lastUpdate']->format('Y-m-d'):'';
                    $data[$office_id] = $result[0];
                    $data[$office_id]['name'] = $office_info['office_name_bng'];
                } catch (\Exception $ex) {
                    if ($single_heading == 1) {
                        echo '<tr><td colspan="15"><span style="color:red">রিপোর্ট এই মুহূর্তে লোড হচ্ছে না, অনুগ্রহপূর্বক কিছুক্ষণ পর পুনরায় চেষ্টা করুন।</span><br><span style="color:red;isplay: inline-block">Error: ' . $this->makeEncryptedData($ex->getMessage()) . '</span></td></tr>';
                        die;
                    } else {
                        echo '<span style="color:red">রিপোর্ট এই মুহূর্তে লোড হচ্ছে না, অনুগ্রহপূর্বক কিছুক্ষণ পর পুনরায় চেষ্টা করুন।</span><br><span style="color:red;isplay: inline-block">Error: ' . $this->makeEncryptedData($ex->getMessage()) . '</span>';
                        die;
                    }
                }
            }
        }
        $this->set(compact('data', 'office_info','startDate','endDate', 'single_heading'));
    }

//

    public function unit()
    {
        $selected_office_section = $this->getCurrentDakSection();
        $this->set(compact('selected_office_section'));
        $OfficeUnitsTable = TableRegistry::get('OfficeUnits');
        /*$selfunit                = $OfficeUnitsTable->find('list',
                ['keyField' => 'id', 'valueField' => 'unit_name_bng'])->where(['id' => $selected_office_section['office_unit_id']])->toArray();
        $ownChildInfo            = $OfficeUnitsTable->getOwnChildIdandName($selected_office_section['office_unit_id']);*/
        if (empty($selected_office_section)) {
            //$ownChildInfo = $OfficeUnitsTable->find('list')->where(['active_status' => 1])->order(['unit_level asc, parent_unit_id asc', 'id desc'])->toArray();
            $ownChildInfo = TableRegistry::get('Offices')->find('list')->where(['active_status' => 1])->toArray();
            $this->set(compact('ownChildInfo'));
            $this->render('superman_unit_performance');
        } else {
            if ($selected_office_section['is_admin'] == true) {
                $ownChildInfo = $OfficeUnitsTable->getOfficeUnitsList($selected_office_section['office_id']);
            } else {
                $ownChildInfo[$selected_office_section['office_unit_id']] = $selected_office_section['office_unit_name'];
            }
        }

        $this->set(compact('ownChildInfo'));
    }

    public function newUnitContent($startDate = '', $endDate = '')
    {
        set_time_limit(0);
        $this->layout = null;
        $reportsTable = TableRegistry::get('Reports');
        $OfficeUnitsTable = TableRegistry::get('OfficeUnits');
        $performanceOfficesTable = TableRegistry::get('PerformanceOffices');
        $performanceUnitsTable = TableRegistry::get('PerformanceUnits');
        $user_login_history_table = TableRegistry::get('UserLoginHistory');
        $employee_offices_table = TableRegistry::get('EmployeeOffices');
        $startDate = !empty($startDate) ? $startDate : date("Y-m-d");
        $endDate = !empty($endDate) ? $endDate : date("Y-m-d");

        $office_info = [];
        $data = [];
        $selected_office_section = $this->getCurrentDakSection();
        if (!empty($this->request->data)) {
            $unit_Id = intval($this->request->data['office_unit_id']);

            $ownChildInfo = [];
            if ($unit_Id == 0) {

//                $selfunit     = $OfficeUnitsTable->find('list',
//                        ['keyField' => 'id', 'valueField' => 'unit_name_bng'])->where(['id' => $selected_office_section['office_unit_id']])->toArray();
                if (empty($selected_office_section)) {
                    $ownChildInfo = $OfficeUnitsTable->find('list')->where(['active_status' => 1])->order(['unit_level asc, parent_unit_id asc',
                        'id desc'])->toArray();
                } else {
                    if ($selected_office_section['is_admin'] == true) {
                        $ownChildInfo = $OfficeUnitsTable->getOfficeUnitsList($selected_office_section['office_id']);
                    } else {
                        $ownChildInfo[$selected_office_section['office_unit_id']] = $selected_office_section['office_unit_name'];
                    }
                }

            } else {
                $selfunit = $OfficeUnitsTable->find()->select(['id', 'unit_name_bng'])->where(['id' => $unit_Id])->first();
                if ($selected_office_section['is_admin'] == true) {
                    $ownChildInfo[$selfunit['id']] = $selfunit['unit_name_bng'];
                } else {
                    $ownChildInfo[$selected_office_section['office_unit_id']] = $selected_office_section['office_unit_name'];
                }
            }
            $result = [];
            if (!empty($ownChildInfo)) {
                $result_index = 0;
                foreach ($ownChildInfo as $unit_Id => $unitname) {
                    try {
                        $today = date('Y-m-d');
                        $yesterday = date('Y-m-d', strtotime($today . ' -1 day'));
                        $office_info = $OfficeUnitsTable->getAll(['id' => $unit_Id], ['office_id'])->first();
                            $condition = [
                                'totalInbox' => 'SUM(inbox)', 'totalOutbox' => 'SUM(outbox)', 'totalNothijato' => 'SUM(nothijat)',
                                'totalNothivukto' => 'SUM(nothivukto)', 'totalNisponnoDak' => 'SUM(nisponnodak)',
                                'totalSouddog' => 'SUM(selfnote)',
                                'totalDaksohoNote' => 'SUM(daksohonote)', 'totalNisponnoPotrojari' => 'SUM(nisponnopotrojari)',
                                'totalNisponnoNote' => 'SUM(nisponnonote)',
                                'totalNisponno' => 'SUM(nisponno)', 'totalONisponno' => 'SUM(onisponno)',
                                'totalID' => 'count(id)'
                            ];
                            $data = $performanceUnitsTable->getUnitData($unit_Id,
                                [$startDate, $endDate])->select($condition)->group(['unit_id'])->toArray();
                            if (!empty($data[0]['totalID']) && $data[0]['totalID'] > 0) {
//                                $totalEmployee = $employee_offices_table->getAllEmployeeRecordID($office_info['office_id'],
//                                    $unit_Id, 0);
                                /* Total Login */
//                                $TotalLogin = $user_login_history_table->countTotalLogin($office_info['office_id'],
//                                    $unit_Id, 0, [$startDate, $endDate]);
                                $data[0]['totalUser'] = $employee_offices_table->getCountOfEmployeeOfOfficeUnites($office_info['office_id'], $unit_Id);
                                 $lastonisponno = $performanceUnitsTable->getLastOnisponno($unit_Id,
                                    [$startDate, $endDate]);
                                $data[0]['totalONisponnoDak'] = $lastonisponno['onisponnodak'];
                                $data[0]['totalONisponnoNote'] = $lastonisponno['onisponnonote'];
//                                $data[0]['totalLogin'] = $TotalLogin->count();
                            } else {
                                $this->switchOffice($office_info['office_id'], 'OfficeDB');
                                $data = $reportsTable->performanceReport($office_info['office_id'],
                                    $unit_Id, 0,
                                    ['date_start' => $startDate, 'date_end' => $endDate]);
                            }


                       $data[0]['lastUpdate'] =$performanceUnitsTable->getLastUpdateTime($unit_Id)['record_date'];
                       $data[0]['lastUpdate'] =(!empty($data[0]['lastUpdate']))? $data[0]['lastUpdate']->format('Y-m-d'):'';
                       $data[0]['totalSouddog'] = (($data[0]['totalSouddog'] < 0) ? 0: $data[0]['totalSouddog']);
                        $result[$unit_Id] = $data[0];
                        $result[$unit_Id]['name'] = $unitname;
                    } catch (\Exception $ex) {
                        continue;
                    }
                }
            }
        }
        $this->response->type('application/json');
        $this->response->body(json_encode($result[$unit_Id]));
        return $this->response;

        $this->set(compact('result'));
    }

    public function designation()
    {
        $selected_office_section = $this->getCurrentDakSection();
        $this->set(compact('selected_office_section'));
        $OfficeUnitsTable = TableRegistry::get('OfficeUnits');
        /*$selfunit                = $OfficeUnitsTable->find('list',
                ['keyField' => 'id', 'valueField' => 'unit_name_bng'])->where(['id' => $selected_office_section['office_unit_id']])->toArray();
        $ownChildInfo            = $OfficeUnitsTable->getOwnChildIdandName($selected_office_section['office_unit_id']);*/
        if (empty($selected_office_section)) {
            /*$ownChildInfo = $OfficeUnitsTable->find('list')->where(['active_status' => 1])->order(['unit_level asc, parent_unit_id asc',
                'id desc'])->toArray();*/
            $ownChildInfo = TableRegistry::get('Offices')->find('list')->where(['active_status' => 1])->toArray();
            $this->set(compact('ownChildInfo'));
            $this->render('superman_designation_performance');
        } else {
            if ($selected_office_section['is_admin'] == true) {
                $ownChildInfo = $OfficeUnitsTable->getOfficeUnitsList($selected_office_section['office_id']);
            } else {
                $ownChildInfo[$selected_office_section['office_unit_id']] = $selected_office_section['office_unit_name'];
            }
        }

        $this->set(compact('ownChildInfo'));
    }

    public function newDesignationContent($startDate = '', $endDate = '')
    {
        set_time_limit(0);
        $this->layout = null;
        $reportsTable = TableRegistry::get('Reports');
        $EmployeeRecordsTable = TableRegistry::get('EmployeeRecords');
        $performanceOfficesTable = TableRegistry::get('PerformanceOffices');
        $performanceDesignationsTable = TableRegistry::get('PerformanceDesignations');
        $employee_offices_table = TableRegistry::get('EmployeeOffices');
        $user_login_history_table = TableRegistry::get('UserLoginHistory');
        $OrganogramTable = TableRegistry::get('OfficeUnitOrganograms');
        $OfficeUnitsTable = TableRegistry::get('OfficeUnits');
        $startDate = !empty($startDate) ? $startDate : date("Y-m-d");
        $endDate = !empty($endDate) ? $endDate : date("Y-m-d");

        $office_info = [];
        $data = [];
        $selected_office_section = $this->getCurrentDakSection();
        if (!empty($this->request->data)) {
            $unit_Id = intval($this->request->data['office_unit_id']);

            $ownChildInfo = [];
            if ($unit_Id == 0) {
                if (empty($selected_office_section)) {
                    $ownChildInfo = $OfficeUnitsTable->find('list')->where(['active_status' => 1])->order(['unit_level asc, parent_unit_id asc',
                        'id desc'])->toArray();
                } else {
                    if ($selected_office_section['is_admin'] == true) {
                        $ownChildInfo = $OfficeUnitsTable->getOfficeUnitsList($selected_office_section['office_id']);
                    } else {
                        $ownChildInfo[$selected_office_section['office_unit_id']] = $selected_office_section['office_unit_name'];
                    }
                }
                //$selfunit = $OfficeUnitsTable->find('list', ['keyField' => 'id', 'valueField' => 'unit_name_bng'])->where(['id' => $selected_office_section['office_unit_id']])->toArray();
            } else {
                if(!empty($this->request->data['unit_name'])){
                    if ($selected_office_section['is_admin'] == true) {
                        $ownChildInfo[$unit_Id] = $this->request->data['unit_name'];
                    } else {
                        $ownChildInfo[$selected_office_section['office_unit_id']] = $selected_office_section['office_unit_name'];
                    }
                }else{
                    $selfunit = $OfficeUnitsTable->find()->select(['id', 'unit_name_bng'])->where(['id' => $unit_Id])->first();
                    if ($selected_office_section['is_admin'] == true) {
                        $ownChildInfo[$selfunit['id']] = $selfunit['unit_name_bng'];
                    } else {
                        $ownChildInfo[$selected_office_section['office_unit_id']] = $selected_office_section['office_unit_name'];
                    }
                }
                
            }

            if (!empty($ownChildInfo)) {
                foreach ($ownChildInfo as $unit_Id => $unitname) {
//                    $organogram_info = $OrganogramTable->getDesignationsByUnitId($unit_Id);
                    $office_info = $OfficeUnitsTable->getAll(['id' => $unit_Id], ['office_id'])->first();
                    $organogram_info = $performanceDesignationsTable->getAllDesignationByOfficeOrUnitIDWithDesignationSort($office_info['office_id'],$unit_Id,[$startDate,$endDate],1);
                    if(empty($selected_office_section) || ($selected_office_section['office_id'] != $office_info['office_id']) ){
                        $this->switchOffice($office_info['office_id'], 'OfficeDB');
                    }
                    if (!empty($organogram_info)) {
                        $data_index =0;
                        foreach ($organogram_info as $key => $val) {
                            if ($selected_office_section['is_admin'] != true) {
                                if ($selected_office_section['office_unit_organogram_id'] != $key) {
                                    continue;
                                }
                            }
                            try {
                                $today = date('Y-m-d');
                                $yesterday = date('Y-m-d', strtotime($today . ' -1 day'));
//                         pr($startDate);pr($endDate);pr($today);die;
                                    $condition = [
                                        'totalInbox' => 'SUM(inbox)', 'totalOutbox' => 'SUM(outbox)', 'totalNothijato' => 'SUM(nothijat)',
                                        'totalNothivukto' => 'SUM(nothivukto)', 'totalNisponnoDak' => 'SUM(nisponnodak)',
                                        'totalSouddog' => 'SUM(selfnote)',
                                        'totalDaksohoNote' => 'SUM(daksohonote)', 'totalNisponnoPotrojari' => 'SUM(nisponnopotrojari)',
                                        'totalNisponnoNote' => 'SUM(nisponnonote)',
                                        'totalNisponno' => 'SUM(nisponno)', 'totalONisponno' => 'SUM(onisponno)',
                                            'totalID' => 'count(id)'
                                    ];
                                    $result = $performanceDesignationsTable->getDesignationData($key,
                                        [$startDate, $endDate])->select($condition)->group(['designation_id'])->toArray();
                                    if (!empty($result[0]['totalID']) && $result[0]['totalID'] > 0) {
                                      $lastonisponno = $performanceDesignationsTable->getLastOnisponno($key,
                                            [$startDate, $endDate]);
                                        $result[0]['totalONisponnoDak'] = $lastonisponno['onisponnodak'];
                                        $result[0]['totalONisponnoNote'] = $lastonisponno['onisponnonote'];
                                    } else {
                                        $result = $reportsTable->performanceReport($office_info['office_id'], $unit_Id, $key,
                                            ['date_start' => $startDate, 'date_end' => $endDate]);
                                    }
                                     $EmployeeSummary = $EmployeeRecordsTable->getEmployeeInfoByDesignation($key);
        //                            pr($EmployeeSummary);die;
        //                            $datetime1 = date_create($startDate);
        //                            $datetime2 = date_create($endDate);
        //                            $interval = date_diff($datetime1, $datetime2);
        //                            $result[0]['totalLogin'] = (int)($result[0]['totalLogin'] / ($interval->format('%d')
        //                                    + 1));
                                    $result[0]['lastUpdate'] =$performanceDesignationsTable->getLastUpdateTime($key)['record_date'];
                                    $result[0]['lastUpdate'] =(!empty($result[0]['lastUpdate']))? $result[0]['lastUpdate']->format('Y-m-d'):'';
                                    $result[0]['totalSouddog'] = (($result[0]['totalSouddog'] < 0) ? 0: $result[0]['totalSouddog']);
                                    $result[0]['designation_id'] = $key;
                                    $data[$data_index]['result'] = $result[0];
                                    if (!empty($EmployeeSummary['name_bng'])) {
                                        $data[$data_index]['result']['name'] = $EmployeeSummary['name_bng'] . ", " . $val.", ".$unitname;
                                    } else {
                                        $data[$data_index]['result']['name'] = $val.", ".$unitname;
                                    }
                                $data_index++;
                            } catch (\Exception $ex) {
                                continue;
                            }
                        }
                    }
                }
            }
        }
        $this->response->type('application/json');
        $this->response->body(json_encode($data));
        return $this->response;
        $this->set(compact('data'));
    }

    public function recordFromBeginningforOffices($date_start = '')
    {
        ini_set('memory_limit', -1);
        set_time_limit(0);
        $office_domains = TableRegistry::get('OfficeDomains');
        $officesTable = TableRegistry::get('Offices');
        if ($this->request->is('get')) {
            $this->layout = 'dashboard';
            $all_offices = $office_domains->find()->where(['status' => 1])->toArray();
            $this->set(compact('all_offices'));
            $this->set(compact('date_start'));
            $this->view = 'performance_report_offices_from_beginning';
        }
        if ($this->request->is('post')) {
            $performanceOfficeTable = TableRegistry::get('PerformanceOffices');
            $reportsTable = TableRegistry::get('Reports');
            $MinistriesTable = TableRegistry::get('OfficeMinistries');
            $LayersTable = TableRegistry::get('OfficeLayers');
            $OriginsTable = TableRegistry::get('OfficeOrigins');
            $office_id = 0;
            $data = [
                'status' => 'Error',
                'msg' => 'No office Id'
            ];
            $count = 0;
            $begin = new \DateTime("2016-06-01");
            $end = new \DateTime(date('Y-m-d'));

            if (!empty($this->request->data['office_id'])) {
                $office_id = $this->request->data['office_id'];
                $office_record = $performanceOfficeTable->getLastUpdateTime($office_id);
                if (!empty($this->request->data['date_start'])) {
                    $begin = new \DateTime(($this->request->data['date_start']));
                } else if (!empty($office_record)) {
                    $begin = $office_record['record_date'];
                } else {
                    $begin = new \DateTime('2016-07-01');
                }
                $end = new \DateTime(date('Y-m-d'));
                $period = [];
                for ($i = $begin; $begin < $end; $i->modify('+1 day')) {
                    $period [] = $i->format("Y-m-d");
                }
                if (!empty($period)) {
                    foreach ($period as $dt) {
                        $date = $dt;
                        $office_record = $performanceOfficeTable->checkUpdate($office_id, $date);
                        if ($office_record < 1) {
                            $office_related_data = $officesTable->getAll(['id' => $office_id],
                                ['office_ministry_id', 'office_layer_id', 'office_origin_id', 'office_name_bng'])->first();
                            try {

                                $this->switchOffice($office_id, 'OfficeDB');
                                $returnSummary = $reportsTable->newPerformanceReport($office_id,
                                    0, 0, ['date_start' => $date, 'date_end' => $date]);
                                $performance_offices_entity = $performanceOfficeTable->newEntity();

                                $performance_offices_entity->inbox = $returnSummary['totalInbox'];
                                $performance_offices_entity->outbox = $returnSummary['totalOutbox'];
                                $performance_offices_entity->nothijat = $returnSummary['totalNothijato'];
                                $performance_offices_entity->nothivukto = $returnSummary['totalNothivukto'];
                                $performance_offices_entity->nisponnodak = $returnSummary['totalNisponnoDak'];
                                $performance_offices_entity->onisponnodak = $returnSummary['totalONisponnoDak'];

                                $performance_offices_entity->selfnote = $returnSummary['totalSouddog'];
                                $performance_offices_entity->daksohonote = $returnSummary['totalDaksohoNote'];
                                $performance_offices_entity->nisponnonote = $returnSummary['totalNisponnoNote'];
                                $performance_offices_entity->nisponnopotrojari = $returnSummary['totalNisponnoPotrojari'];
                                $performance_offices_entity->onisponnonote = $returnSummary['totalONisponnoNote'];

                                $performance_offices_entity->potrojari = $returnSummary['totalPotrojari'];

                                $performance_offices_entity->nisponno = $returnSummary['totalNisponno'];
                                $performance_offices_entity->onisponno = $returnSummary['totalONisponno'];

                                $performance_offices_entity->office_id = $office_id;
                                $performance_offices_entity->ministry_id = $office_related_data['office_ministry_id'];
                                $performance_offices_entity->layer_id = $office_related_data['office_layer_id'];
                                $performance_offices_entity->origin_id = $office_related_data['office_origin_id'];
                                $performance_offices_entity->office_name = $office_related_data['office_name_bng'];

                                $ministry_info = $MinistriesTable->getBanglaName($office_related_data['office_ministry_id']);
                                $performance_offices_entity->ministry_name = $ministry_info['name_bng'];
                                $layer_info = $LayersTable->getBanglaName($office_related_data['office_layer_id']);
                                $performance_offices_entity->layer_name = $layer_info['layer_name_bng'];
                                $origin_info = $OriginsTable->getBanglaName($office_related_data['office_origin_id']);
                                $performance_offices_entity->origin_name = $origin_info['office_name_bng'];

                                $performance_offices_entity->status = 1;
                                $performance_offices_entity->record_date = $date;
                                $performance_offices_entity->updated = 1;
//                            pr($performance_offices_entity);die;

                                if ($performanceOfficeTable->save($performance_offices_entity)) {
                                    $count++;
                                }
                            } catch (\Exception $ex) {
                                $data = [
                                    'status' => 'Error',
                                    'msg' => 'Office DB not set'
                                ];
                                $this->response->type('application/json');
                                $this->response->body(json_encode($data));
                                return $this->response;
                            }
                        }
                    }
                } else {
                    $data = [
                        'status' => 'Success',
                        'msg' => 'Already Updated'
                    ];
                }
            }
            if ($count > 0) {
                $data = [
                    'status' => 'Success',
                    'msg' => $count . " Day"
                ];
            }
            if (!empty($period) && $count == 0) {
                $data = [
                    'status' => 'Success',
                    'msg' => 'Already Updated'
                ];
            }
            $this->response->type('application/json');
            $this->response->body(json_encode($data));
            return $this->response;
        }
    }

    public function recordFromBeginningforUnits($date_start = '')
    {
        set_time_limit(0);
        ini_set('memory_limit', -1);
        $unitsTable = TableRegistry::get('OfficeUnits');
        if ($this->request->is('get')) {
            $this->layout = 'dashboard';
            $employee_offices = TableRegistry::get('EmployeeOffices');
//            $all_units    = $unitsTable->find('list')->where(['active_status' => 1])->toArray();
            $all_units = $employee_offices->getAllUnitID()->toArray();
            $this->set(compact('all_units'));
            $this->set(compact('date_start'));
            $this->view = 'performance_report_units_from_beginning';
        }
        if ($this->request->is('post')) {
            $performanceUnitTable = TableRegistry::get('PerformanceUnits');
            $reportsTable = TableRegistry::get('Reports');
            $MinistriesTable = TableRegistry::get('OfficeMinistries');
            $LayersTable = TableRegistry::get('OfficeLayers');
            $OfficesTable = TableRegistry::get('Offices');
            $UnitOriginsTable = TableRegistry::get('OfficeOriginUnits');
            $unit_id = 0;
            $data = [
                'status' => 'Error',
                'msg' => 'No office Id'
            ];
            $count = 0;

            if (!empty($this->request->data['unit_id'])) {
                $unit_id = $this->request->data['unit_id'];
                $unit_record = $performanceUnitTable->getLastUpdateTime($unit_id);
                $period = [];
                if (!empty($this->request->data['date_start'])) {
                    $begin = new \DateTime(($this->request->data['date_start']));
                } else if (!empty($unit_record)) {
                    $begin = ($unit_record['record_date']);
                } else {
                    $begin = new \DateTime('2016-07-01');
                }
                $end = new \DateTime(date('Y-m-d'));

                for ($i = $begin; $begin < $end; $i->modify('+1 day')) {
                    $period [] = $i->format("Y-m-d");
                }
                if (!empty($period)) {
                    foreach ($period as $dt) {
                        $date = $dt;
                        $unit_record = $performanceUnitTable->checkUpdate($unit_id, $date);
                        if ($unit_record < 1) {
                            $unit_related_data = $unitsTable->getAll(['id' => $unit_id],
                                ['office_ministry_id', 'office_layer_id', 'office_origin_unit_id',
                                    'office_id',
                                    'unit_name_bng'])->first();
//                        pr($unit_related_data['office_id']);die;
                            try {

                                $this->switchOffice($unit_related_data['office_id'], 'OfficeDB');
                                $returnSummary = $reportsTable->newPerformanceReport($unit_related_data['office_id'],
                                    $unit_id, 0, ['date_start' => $date, 'date_end' => $date]);
                                $performance_units_entity = $performanceUnitTable->newEntity();

                                $performance_units_entity->inbox = $returnSummary['totalInbox'];
                                $performance_units_entity->outbox = $returnSummary['totalOutbox'];
                                $performance_units_entity->nothijat = $returnSummary['totalNothijato'];
                                $performance_units_entity->nothivukto = $returnSummary['totalNothivukto'];
                                $performance_units_entity->nisponnodak = $returnSummary['totalNisponnoDak'];
                                $performance_units_entity->onisponnodak = $returnSummary['totalONisponnoDak'];

                                $performance_units_entity->selfnote = $returnSummary['totalSouddog'];
                                $performance_units_entity->daksohonote = $returnSummary['totalDaksohoNote'];
                                $performance_units_entity->nisponnopotrojari = $returnSummary['totalNisponnoPotrojari'];
                                $performance_units_entity->nisponnonote = $returnSummary['totalNisponnoNote'];
                                $performance_units_entity->onisponnonote = $returnSummary['totalONisponnoNote'];

                                $performance_units_entity->potrojari = $returnSummary['totalPotrojari'];

                                $performance_units_entity->nisponno = $returnSummary['totalNisponno'];
                                $performance_units_entity->onisponno = $returnSummary['totalONisponno'];

                                $performance_units_entity->office_id = $unit_related_data['office_id'];
                                $performance_units_entity->unit_id = $unit_id;
                                $performance_units_entity->ministry_id = $unit_related_data['office_ministry_id'];
                                $performance_units_entity->layer_id = $unit_related_data['office_layer_id'];
                                $performance_units_entity->origin_id = $unit_related_data['office_origin_unit_id'];
                                $performance_units_entity->unit_name = $unit_related_data['unit_name_bng'];

                                $office_info = $OfficesTable->getBanglaName($unit_related_data['office_id']);
                                $performance_units_entity->office_name = $office_info['office_name_bng'];

                                $ministry_info = $MinistriesTable->getBanglaName($unit_related_data['office_ministry_id']);
                                $performance_units_entity->ministry_name = $ministry_info['name_bng'];
                                $layer_info = $LayersTable->getBanglaName($unit_related_data['office_layer_id']);
                                $performance_units_entity->layer_name = $layer_info['layer_name_bng'];
                                $origin_info = $UnitOriginsTable->getBanglaName($unit_related_data['office_origin_unit_id']);
                                $performance_units_entity->origin_name = $origin_info['unit_name_bng'];

                                $performance_units_entity->status = 1;
                                $performance_units_entity->updated = 1;
                                $performance_units_entity->record_date = $date;

                                if ($performanceUnitTable->save($performance_units_entity)) {
                                    $count++;
                                }
                            } catch (\Exception $ex) {
                                $data = [
                                    'status' => 'Error',
                                    'msg' => 'Office DB: ' . $unit_related_data['office_id'] . ' not set.'
                                ];
                                $this->response->type('application/json');
                                $this->response->body(json_encode($data));
                                return $this->response;
                            }
                        }
                    }
                } else {
                    $data = [
                        'status' => 'Success',
                        'msg' => 'Already Updated'
                    ];
                }
            }
            if ($count > 0) {
                $data = [
                    'status' => 'Success',
                    'msg' => $count . " Day"
                ];
            }
            if (!empty($period) && $count == 0) {
                $data = [
                    'status' => 'Success',
                    'msg' => 'Already Updated'
                ];
            }
            $this->response->type('application/json');
            $this->response->body(json_encode($data));
            return $this->response;
        }
    }

    public function recordFromBeginningforDesignations($date_start = '')
    {
        set_time_limit(0);
        ini_set('memory_limit', -1);
        $employeeOfficesTable = TableRegistry::get('EmployeeOffices');
        if ($this->request->is('get')) {
            $this->layout = 'dashboard';
            $all_designations = $employeeOfficesTable->getAllDesignationID()->toArray();
//            pr($all_designations);die;
            $this->set(compact('all_designations'));
            $this->view = 'performance_report_designations_from_beginning';
            $this->set(compact('date_start'));
        }
        if ($this->request->is('post')) {
            $performanceDesignationTable = TableRegistry::get('PerformanceDesignations');
            $reportsTable = TableRegistry::get('Reports');
            $MinistriesTable = TableRegistry::get('OfficeMinistries');
            $LayersTable = TableRegistry::get('OfficeLayers');
            $OfficesTable = TableRegistry::get('Offices');
            $UnitsTable = TableRegistry::get('OfficeUnits');
            $OriginsTable = TableRegistry::get('OfficeOrigins');
            $designation_id = 0;
            $data = [
                'status' => 'Error',
                'msg' => 'No office Id'
            ];
            $count = 0;
            if (!empty($this->request->data['designation_id'])) {

                $designation_id = $this->request->data['designation_id'];
                $designation_record = $performanceDesignationTable->getLastUpdateTime($designation_id);
                if (!empty($this->request->data['date_start'])) {
                    $begin = new \DateTime($this->request->data['date_start']);
                } else if (!empty($designation_record)) {
                    $begin = ($designation_record['record_date']);
                } else {
                    $begin = new \DateTime('2016-07-01');
                }
                $end = new \DateTime(date('Y-m-d'));
                $period = [];
                for ($i = $begin; $begin < $end; $i->modify('+1 day')) {
                    $period [] = $i->format("Y-m-d");
                }
                if (!empty($period)) {
                    foreach ($period as $dt) {
                        $date = $dt;
                        $designation_record = $performanceDesignationTable->checkUpdate($designation_id,
                            $date);
                        if ($designation_record < 1) {

                            $designation_related_data = $employeeOfficesTable->getDesignationInfo($designation_id);

                            try {

                                $this->switchOffice($designation_related_data['office_id'],
                                    'OfficeDB');
                                $returnSummary = $reportsTable->newPerformanceReport($designation_related_data['office_id'],
                                    $designation_related_data['office_unit_id'], $designation_id,
                                    ['date_start' => $date, 'date_end' => $date]);
                                $performance_designatons_entity = $performanceDesignationTable->newEntity();

                                $performance_designatons_entity->inbox = $returnSummary['totalInbox'];
                                $performance_designatons_entity->outbox = $returnSummary['totalOutbox'];
                                $performance_designatons_entity->nothijat = $returnSummary['totalNothijato'];
                                $performance_designatons_entity->nothivukto = $returnSummary['totalNothivukto'];
                                $performance_designatons_entity->nisponnodak = $returnSummary['totalNisponnoDak'];
                                $performance_designatons_entity->onisponnodak = $returnSummary['totalONisponnoDak'];

                                $performance_designatons_entity->selfnote = $returnSummary['totalSouddog'];
                                $performance_designatons_entity->daksohonote = $returnSummary['totalDaksohoNote'];
                                $performance_designatons_entity->nisponnopotrojari = $returnSummary['totalNisponnoPotrojari'];
                                $performance_designatons_entity->nisponnonote = $returnSummary['totalNisponnoNote'];
                                $performance_designatons_entity->onisponnonote = $returnSummary['totalONisponnoNote'];

                                $performance_designatons_entity->potrojari = $returnSummary['totalPotrojari'];

                                $performance_designatons_entity->nisponno = $returnSummary['totalNisponno'];
                                $performance_designatons_entity->onisponno = $returnSummary['totalONisponno'];

                                $office_related_data = $OfficesTable->getAll(['id' => $designation_related_data['office_id']],
                                    ['office_ministry_id', 'office_layer_id', 'office_origin_id', 'office_name_bng'])->first();
                                $performance_designatons_entity->office_id = $designation_related_data['office_id'];
                                $performance_designatons_entity->ministry_id = $office_related_data['office_ministry_id'];
                                $performance_designatons_entity->layer_id = $office_related_data['office_layer_id'];
                                $performance_designatons_entity->origin_id = $office_related_data['office_origin_id'];
                                $performance_designatons_entity->office_name = $office_related_data['office_name_bng'];

                                $unit_info = $UnitsTable->getBanglaName($designation_related_data['office_unit_id']);
                                $performance_designatons_entity->unit_name = $unit_info['unit_name_bng'];
                                $performance_designatons_entity->unit_id = $designation_related_data['office_unit_id'];
                                $performance_designatons_entity->designation_id = $designation_id;
                                $performance_designatons_entity->designation_name = $designation_related_data['designation'];

                                $ministry_info = $MinistriesTable->getBanglaName($office_related_data['office_ministry_id']);
                                $performance_designatons_entity->ministry_name = $ministry_info['name_bng'];
                                $layer_info = $LayersTable->getBanglaName($office_related_data['office_layer_id']);
                                $performance_designatons_entity->layer_name = $layer_info['layer_name_bng'];
                                $origin_info = $OriginsTable->getBanglaName($office_related_data['office_origin_id']);
                                $performance_designatons_entity->origin_name = $origin_info['office_name_bng'];

                                $performance_designatons_entity->status = 1;
                                $performance_designatons_entity->updated = 1;
                                $performance_designatons_entity->record_date = $date;

                                if ($performanceDesignationTable->save($performance_designatons_entity)) {
                                    $count++;
                                }
                            } catch (Exception $ex) {
                                $data = [
                                    'status' => 'Error',
                                    'msg' => 'Office DB ' . $designation_related_data['office_id'] . ' not set'
                                ];
                                $this->response->type('application/json');
                                $this->response->body(json_encode($data));
                                return $this->response;
                            }
                        }
                    }
                } else {
                    $data = [
                        'status' => 'Success',
                        'msg' => 'Already Updated'
                    ];
                }
            }
            if ($count > 0) {
                $data = [
                    'status' => 'Success',
                    'msg' => $count . " Day"
                ];
            }
            if (!empty($period) && $count == 0) {
                $data = [
                    'status' => 'Success',
                    'msg' => 'Already Updated'
                ];
            }
            $this->response->type('application/json');
            $this->response->body(json_encode($data));
            return $this->response;
        }
    }

    public function performaceOfficeUpdate($date_start = '')
    {
        ini_set('memory_limit', -1);
        set_time_limit(0);
        $start_time = strtotime(date('Y-m-d H:i:s'));

        $officesTable = TableRegistry::get('Offices');
        $all_offices = $officesTable->find('list')->where(['active_status' => 1])->toArray();

        $performanceOfficeTable = TableRegistry::get('PerformanceOffices');
        $reportsTable = TableRegistry::get('Reports');
        $MinistriesTable = TableRegistry::get('OfficeMinistries');
        $LayersTable = TableRegistry::get('OfficeLayers');
        $OriginsTable = TableRegistry::get('OfficeOrigins');

        $total = count($all_offices);
        $error = 0;
        $success = 0;
        $error_id = [];


        foreach ($all_offices as $office_id => $office_name) {
            $period = [];
            $temp_error = 0;
            $office_record = $performanceOfficeTable->getLastUpdateTime($office_id);

            if (!empty($date_start)) {
                $begin = new \DateTime($date_start);
            } else if (!empty($office_record)) {
                $begin = ($office_record['record_date']);
            } else {
                $begin = new \DateTime('2016-07-01');
            }
            $end = new \DateTime(date('Y-m-d'));

            for ($i = $begin; $begin < $end; $i->modify('+1 day')) {
                $period [] = $i->format("Y-m-d");
            }
            if (!empty($period)) {
                foreach ($period as $dt) {
                    $date = $dt;
                    $office_related_data = $officesTable->getAll(['id' => $office_id],
                        ['office_ministry_id', 'office_layer_id', 'office_origin_id', 'office_name_bng'])->first();
                    try {

                        $this->switchOffice($office_id, 'OfficeDB');
                        $returnSummary = $reportsTable->newPerformanceReport($office_id,
                            0, 0, ['date_start' => $date, 'date_end' => $date]);
                        $performance_offices_entity = $performanceOfficeTable->newEntity();

                        $performance_offices_entity->inbox = $returnSummary['totalInbox'];
                        $performance_offices_entity->outbox = $returnSummary['totalOutbox'];
                        $performance_offices_entity->nothijat = $returnSummary['totalNothijato'];
                        $performance_offices_entity->nothivukto = $returnSummary['totalNothivukto'];
                        $performance_offices_entity->nisponnodak = $returnSummary['totalNisponnoDak'];
                        $performance_offices_entity->onisponnodak = $returnSummary['totalONisponnoDak'];

                        $performance_offices_entity->potrojari = $returnSummary['totalPotrojari'];
                        $performance_offices_entity->selfnote = $returnSummary['totalSouddog'];
                        $performance_offices_entity->daksohonote = $returnSummary['totalDaksohoNote'];
                        $performance_offices_entity->nisponnopotrojari = $returnSummary['totalNisponnoPotrojari'];

                        $performance_offices_entity->nisponnonote = $returnSummary['totalNisponnoNote'];
                        $performance_offices_entity->onisponnonote = $returnSummary['totalONisponnoNote'];

                        $performance_offices_entity->nisponno = $returnSummary['totalNisponno'];
                        $performance_offices_entity->onisponno = $returnSummary['totalONisponno'];

                        $performance_offices_entity->office_id = $office_id;
                        $performance_offices_entity->ministry_id = $office_related_data['office_ministry_id'];
                        $performance_offices_entity->layer_id = $office_related_data['office_layer_id'];
                        $performance_offices_entity->origin_id = $office_related_data['office_origin_id'];
                        $performance_offices_entity->office_name = $office_name;

                        $ministry_info = $MinistriesTable->getBanglaName($office_related_data['office_ministry_id']);
                        $performance_offices_entity->ministry_name = $ministry_info['name_bng'];
                        $layer_info = $LayersTable->getBanglaName($office_related_data['office_layer_id']);
                        $performance_offices_entity->layer_name = $layer_info['layer_name_bng'];
                        $origin_info = $OriginsTable->getBanglaName($office_related_data['office_origin_id']);
                        $performance_offices_entity->origin_name = $origin_info['office_name_bng'];

                        $performance_offices_entity->status = 1;
                        $performance_offices_entity->record_date = $date;
//                            pr($performance_offices_entity);die;

                        if ($performanceOfficeTable->save($performance_offices_entity)) {
                            $success++;
                        } else {
                            $error++;
                            if ($temp_error == 0) {
                                $error_id [] = "Office Id:" . $office_id . ", Office Name:" . $office_name . "  <br> Error:  Could not save";
                                $temp_error = 1;
                            }
                        }
                        sleep(1);
                    } catch (\Exception $ex) {
                        $error++;
                        if ($temp_error == 0) {
                            $error_id [] = "Office Id:" . $office_id . ", Office Name:" . $office_name . "  <br> Error: " . $ex->getMessage();
                            $temp_error = 1;
                        }
                    }
                }
            }
        }
        $response = "Total: " . $total;
        $end_time = strtotime(date('Y-m-d H:i:s'));
        $time_difference = round(abs($end_time - $start_time), 2);
        $response .= "<br> Time: " . $time_difference . "s";
        if (!empty($error_id)) {
            $response .= '<br>Error list: <ul>';
            foreach ($error_id as $id) {
                $response .= "<li>" . $id . "</li><br>";
            }
            $response .= "</ul> ";
        } else {
            $response .= "No Error Occured";
        }
        $this->sendCronMessage('tusharkaiser@gmail.com', 'eather.ahmed@gmail.com',
            'Cron Report ' . date('Y-m-d'), 'Performance Report Of Offices', $response);
        die;
    }

    public function performanceUnitUpdate($date_start = '')
    {
        set_time_limit(0);
        ini_set('memory_limit', -1);
        $start_time = strtotime(date('Y-m-d H:i:s'));

        $unitsTable = TableRegistry::get('OfficeUnits');
        $all_units = $unitsTable->find('list')->where(['active_status' => 1])->toArray();
        $performanceUnitTable = TableRegistry::get('PerformanceUnits');
        $reportsTable = TableRegistry::get('Reports');
        $MinistriesTable = TableRegistry::get('OfficeMinistries');
        $LayersTable = TableRegistry::get('OfficeLayers');
        $OfficesTable = TableRegistry::get('Offices');
        $UnitOriginsTable = TableRegistry::get('OfficeOriginUnits');

        $total = 0;
        $error = 0;
        $success = 0;
        $error_id = [];

        foreach ($all_units as $unit_id => $unit_name) {
            $temp_error = 0;
            $period = [];
            $unit_record = $performanceUnitTable->getLastUpdateTime($unit_id);
            if (!empty($date_start)) {
                $begin = new \DateTime($date_start);
            } else if (!empty($unit_record)) {
                $begin = ($unit_record['record_date']);
            } else {
                $begin = new \DateTime('2016-07-01');
            }
            $end = new \DateTime(date('Y-m-d'));

            for ($i = $begin; $begin < $end; $i->modify('+1 day')) {
                $period [] = $i->format("Y-m-d");
            }
            if (!empty($period)) {
                foreach ($period as $dt) {
                    $total++;
                    $date = $dt;
                    $unit_related_data = $unitsTable->getAll(['id' => $unit_id],
                        ['office_ministry_id', 'office_layer_id', 'office_origin_unit_id', 'office_id'])->first();
//                        pr($unit_related_data['office_id']);die;
                    try {

                        $this->switchOffice($unit_related_data['office_id'], 'OfficeDB');
                        $returnSummary = $reportsTable->newPerformanceReport($unit_related_data['office_id'],
                            $unit_id, 0, ['date_start' => $date, 'date_end' => $date]);
                        $performance_units_entity = $performanceUnitTable->newEntity();

                        $performance_units_entity->inbox = $returnSummary['totalInbox'];
                        $performance_units_entity->outbox = $returnSummary['totalOutbox'];
                        $performance_units_entity->nothijat = $returnSummary['totalNothijato'];
                        $performance_units_entity->nothivukto = $returnSummary['totalNothivukto'];
                        $performance_units_entity->nisponnodak = $returnSummary['totalNisponnoDak'];
                        $performance_units_entity->onisponnodak = $returnSummary['totalONisponnoDak'];

                        $performance_units_entity->selfnote = $returnSummary['totalSouddog'];
                        $performance_units_entity->daksohonote = $returnSummary['totalDaksohoNote'];
                        $performance_units_entity->nisponnopotrojari = $returnSummary['totalNisponnoPotrojari'];
                        $performance_units_entity->potrojari = $returnSummary['totalPotrojari'];
                        $performance_units_entity->nisponnonote = $returnSummary['totalNisponnoNote'];
                        $performance_units_entity->onisponnonote = $returnSummary['totalONisponnoNote'];

                        $performance_units_entity->nisponno = $returnSummary['totalNisponno'];
                        $performance_units_entity->onisponno = $returnSummary['totalONisponno'];

                        $performance_units_entity->office_id = $unit_related_data['office_id'];
                        $performance_units_entity->unit_id = $unit_id;
                        $performance_units_entity->ministry_id = $unit_related_data['office_ministry_id'];
                        $performance_units_entity->layer_id = $unit_related_data['office_layer_id'];
                        $performance_units_entity->origin_id = $unit_related_data['office_origin_unit_id'];
                        $performance_units_entity->unit_name = $unit_name;

                        $office_info = $OfficesTable->getBanglaName($unit_related_data['office_id']);
                        $performance_units_entity->office_name = $office_info['office_name_bng'];

                        $ministry_info = $MinistriesTable->getBanglaName($unit_related_data['office_ministry_id']);
                        $performance_units_entity->ministry_name = $ministry_info['name_bng'];
                        $layer_info = $LayersTable->getBanglaName($unit_related_data['office_layer_id']);
                        $performance_units_entity->layer_name = $layer_info['layer_name_bng'];
                        $origin_info = $UnitOriginsTable->getBanglaName($unit_related_data['office_origin_unit_id']);
                        $performance_units_entity->origin_name = $origin_info['unit_name_bng'];

                        $performance_units_entity->status = 1;
                        $performance_units_entity->record_date = $date;

                        if ($performanceUnitTable->save($performance_units_entity)) {
                            $success++;
                        } else {
                            $error++;
                            if ($temp_error == 0) {
                                $error_id [] = "Unit Id:" . $unit_id . ", Unit Name:" . $unit_name . ", Office Id:" . $unit_related_data['office_id'] . ", Office name:" . $office_info['office_name_bng'] . "<br>Error: Could not Save";
                                $temp_error = 1;
                            }
                        }
                        sleep(1);
                    } catch (\Exception $ex) {
                        $error++;
                        if ($temp_error == 0) {
                            $error_id [] = "Unit Id:" . $unit_id . ", Unit Name:" . $unit_name . ", Office Id:" . $unit_related_data['office_id'] . "<br> Error: " . $ex->getMessage();
                            $temp_error = 1;
                        }
                    }
                }
            }
        }
        $response = "Total Unit: " . count($all_units);
        $end_time = strtotime(date('Y-m-d H:i:s'));
        $time_difference = round(abs($end_time - $start_time), 2);
        $response .= "<br> Time: " . $time_difference . "s";
        if (!empty($error_id)) {
            $response .= '<br>Error list: <ul>';
            foreach ($error_id as $id) {
                $response .= "<li>" . $id . " </li><br> ";
            }
            $response .= "</ul>";
        } else {
            $response .= "No Error Occured";
        }
        $this->sendCronMessage('tusharkaiser@gmail.com', 'eather.ahmed@gmail.com',
            'Cron Report ' . date('Y-m-d'), 'Performance Report Of Units', $response);
        die;
    }

    public function performanceDesignationUpdate($date_start = '')
    {
        set_time_limit(0);
        ini_set('memory_limit', -1);
        $start_time = strtotime(date('Y-m-d H:i:s'));

        $employeeOfficesTable = TableRegistry::get('EmployeeOffices');
        $all_designations = $employeeOfficesTable->getAllDesignationID()->toArray();

        $performanceDesignationTable = TableRegistry::get('PerformanceDesignations');
        $reportsTable = TableRegistry::get('Reports');
        $MinistriesTable = TableRegistry::get('OfficeMinistries');
        $LayersTable = TableRegistry::get('OfficeLayers');
        $OfficesTable = TableRegistry::get('Offices');
        $UnitsTable = TableRegistry::get('OfficeUnits');
        $OriginsTable = TableRegistry::get('OfficeOrigins');

        $total = 0;
        $error = 0;
        $success = 0;
        $error_id = [];


        foreach ($all_designations as $designation_id => $designation_name) {
            $temp_error = 0;
            $period = [];
            $designation_record = $performanceDesignationTable->getLastUpdateTime($designation_id);
            if (!empty($date_start)) {
                $begin = new \DateTime($date_start);
            } else if (!empty($designation_record)) {
                $begin = ($designation_record['record_date']);
            } else {
                $begin = new \DateTime('2016-07-01');
            }
            $end = new \DateTime(date('Y-m-d'));
            for ($i = $begin; $begin < $end; $i->modify('+1 day')) {
                $period [] = $i->format("Y-m-d");
            }
            if (!empty($period)) {
                foreach ($period as $dt) {
                    $total++;
                    $date = $dt;
                    $designation_record = $performanceDesignationTable->checkUpdate($designation_id,
                        $date);
                    $designation_related_data = $employeeOfficesTable->getDesignationInfo($designation_id);
//                        pr($designation_related_data);die;
                    try {

                        $this->switchOffice($designation_related_data['office_id'], 'OfficeDB');
                        $returnSummary = $reportsTable->newPerformanceReport($designation_related_data['office_id'],
                            $designation_related_data['office_unit_id'], $designation_id,
                            ['date_start' => $date, 'date_end' => $date]);
                        $performance_designatons_entity = $performanceDesignationTable->newEntity();

                        $performance_designatons_entity->inbox = $returnSummary['totalInbox'];
                        $performance_designatons_entity->outbox = $returnSummary['totalOutbox'];
                        $performance_designatons_entity->nothijat = $returnSummary['totalNothijato'];
                        $performance_designatons_entity->nothivukto = $returnSummary['totalNothivukto'];
                        $performance_designatons_entity->nisponnodak = $returnSummary['totalNisponnoDak'];
                        $performance_designatons_entity->onisponnodak = $returnSummary['totalONisponnoDak'];

                        $performance_designatons_entity->selfnote = $returnSummary['totalSouddog'];
                        $performance_designatons_entity->daksohonote = $returnSummary['totalDaksohoNote'];
                        $performance_designatons_entity->potrojari = $returnSummary['totalPotrojari'];
                        $performance_designatons_entity->nisponnopotrojari = $returnSummary['totalNisponnoPotrojari'];
                        $performance_designatons_entity->nisponnonote = $returnSummary['totalNisponnoNote'];
                        $performance_designatons_entity->onisponnonote = $returnSummary['totalONisponnoNote'];

                        $performance_designatons_entity->nisponno = $returnSummary['totalNisponno'];
                        $performance_designatons_entity->onisponno = $returnSummary['totalONisponno'];

                        $office_related_data = $OfficesTable->getAll(['id' => $designation_related_data['office_id']],
                            ['office_ministry_id', 'office_layer_id', 'office_origin_id', 'office_name_bng'])->first();
                        $performance_designatons_entity->office_id = $designation_related_data['office_id'];
                        $performance_designatons_entity->ministry_id = $office_related_data['office_ministry_id'];
                        $performance_designatons_entity->layer_id = $office_related_data['office_layer_id'];
                        $performance_designatons_entity->origin_id = $office_related_data['office_origin_id'];
                        $performance_designatons_entity->office_name = $office_related_data['office_name_bng'];

                        $unit_info = $UnitsTable->getBanglaName($designation_related_data['office_unit_id']);
                        $performance_designatons_entity->unit_name = $unit_info['unit_name_bng'];
                        $performance_designatons_entity->unit_id = $designation_related_data['office_unit_id'];
                        $performance_designatons_entity->designation_id = $designation_id;
                        $performance_designatons_entity->designation_name = $designation_name;

                        $ministry_info = $MinistriesTable->getBanglaName($office_related_data['office_ministry_id']);
                        $performance_designatons_entity->ministry_name = $ministry_info['name_bng'];
                        $layer_info = $LayersTable->getBanglaName($office_related_data['office_layer_id']);
                        $performance_designatons_entity->layer_name = $layer_info['layer_name_bng'];
                        $origin_info = $OriginsTable->getBanglaName($office_related_data['office_origin_id']);
                        $performance_designatons_entity->origin_name = $origin_info['office_name_bng'];

                        $performance_designatons_entity->status = 1;
                        $performance_designatons_entity->record_date = $date;

                        if ($performanceDesignationTable->save($performance_designatons_entity)) {
                            $success++;
                        } else {
                            if ($temp_error == 0) {
                                $temp_error = 1;
                                $error_id [] = "Designation Id:" . $designation_id . ", Designation Name:" . $designation_name . ", Office Id:" . $designation_related_data['office_id'] . " Office Name: " . $office_related_data['office_name_bng'] . "<br> Error: Could not save";
                            }
                            $error++;
                        }
                        sleep(1);
                    } catch (\Exception $ex) {
                        $error++;
                        if ($temp_error == 0) {
                            $temp_error = 1;
                            $error_id [] = "Designation Id:" . $designation_id . ", Designation Name:" . $designation_name . ", Office Id:" . $designation_related_data['office_id'] . "<br> Error: " . $ex->getMessage();
                        }
                    }
                }
            }
        }
        $response = "Total Designations: " . count($all_designations);
        $end_time = strtotime(date('Y-m-d H:i:s'));
        $time_difference = round(abs($end_time - $start_time), 2);
        $response .= "<br> Time: " . $time_difference . "s";
        if (!empty($error_id)) {
            $response .= '<br>Error list: <ul>';
            foreach ($error_id as $id) {
                $response .= "<li>" . $id . " </li><br> ";
            }
            $response .= "</ul> ";
        } else {
            $response .= "No Error Occured";
        }
        $this->sendCronMessage('tusharkaiser@gmail.com', 'eather.ahmed@gmail.com',
            'Cron Report ' . date('Y-m-d'), 'Performance Report Of Designations', $response);
        die;
    }

    //    public function Office()
//    {
//        $selected_office_section                           = $this->getCurrentDakSection();
//        $this->set(compact('selected_office_section'));
//        $OfficesTable                                      = TableRegistry::get('Offices');
//        $ChildOffices                                      = $OfficesTable->getIdandNameofChildOffices($selected_office_section['office_id']);
//        $selfOffice[$selected_office_section['office_id']] = $selected_office_section['office_name'];
//        $ChildOffices                                      = $selfOffice + $ChildOffices;
////        pr($ChildOffices);die;
//        $this->set(compact('ChildOffices'));
//    }
//    public function OfficeContent($startDate = '', $endDate = '')
//    {
//        $this->layout            = null;
//        $selected_office_section = $this->getCurrentDakSection();
//        $reportsTable            = TableRegistry::get('Reports');
//        $OfficesTable            = TableRegistry::get('Offices');
//        $startDate               = !empty($startDate) ? $startDate : date("Y-m-d");
//        $endDate                 = !empty($endDate) ? $endDate : date("Y-m-d");
//
//        $office_info = [];
//        $data        = [];
//        if (!empty($this->request->data)) {
//            $office_id = $this->request->data['office_id'];
//            if ($office_id == 0) {
//
//                $ChildOffices                                      = $OfficesTable->getIdandNameofChildOffices($selected_office_section['office_id']);
//                $selfOffice[$selected_office_section['office_id']] = $selected_office_section['office_name'];
//                $ChildOffices                                      = $selfOffice + $ChildOffices;
//                foreach ($ChildOffices as $key => $val) {
//                    try {
//                        $this->switchOffice($key, 'OfficeDB');
//                        $result                  = $reportsTable->performanceReport($key, 0, 0,
//                            ['date_start' => $startDate, 'date_end' => $startDate]);
//                        $datetime1               = date_create($startDate);
//                        $datetime2               = date_create($startDate);
//                        $interval                = date_diff($datetime1, $datetime2);
//                        $result[0]['totalLogin'] = (int) ($result[0]['totalLogin'] / ($interval->format('%d')
//                            + 1));
//                        $data[$key]              = $result[0];
//                        $data[$key]['name']      = $val;
//                    } catch (Exception $ex) {
//                        continue;
//                    }
//                }
//            }
//            else {
//                try {
//                    $this->switchOffice($office_id, 'OfficeDB');
//                    $office_info = $OfficesTable->getBanglaName($office_id);
//
//                    $result                   = $reportsTable->performanceReport($office_id, 0, 0,
//                        ['date_start' => $startDate, 'date_end' => $endDate]);
//                    $datetime1                = date_create($startDate);
//                    $datetime2                = date_create($endDate);
//                    $interval                 = date_diff($datetime1, $datetime2);
//                    $result[0]['totalLogin']  = (int) ($result[0]['totalLogin'] / ($interval->format('%d')
//                        + 1));
//                    $data[$office_id]         = $result[0];
//                    $data[$office_id]['name'] = $office_info['office_name_bng'];
//                } catch (Exception $ex) {
//
//                }
//            }
//        }
//
//        $this->set(compact('data', 'office_info'));
//    }
//     public function Unit()
//    {
//        $selected_office_section = $this->getCurrentDakSection();
//        $this->set(compact('selected_office_section'));
//        $OfficeUnitsTable        = TableRegistry::get('OfficeUnits');
//        $selfunit                = $OfficeUnitsTable->find('list',
//                ['keyField' => 'id', 'valueField' => 'unit_name_bng'])->where(['id' => $selected_office_section['office_unit_id']])->toArray();
//        $ownChildInfo            = $OfficeUnitsTable->getOwnChildIdandName($selected_office_section['office_unit_id']);
//        $ownChildInfo            = $selfunit + $ownChildInfo;
//        $this->set(compact('ownChildInfo'));
//    }
//
//    public function UnitContent($startDate = '', $endDate = '')
//    {
//        $this->layout     = null;
//        $reportsTable     = TableRegistry::get('Reports');
//        $OfficeUnitsTable = TableRegistry::get('OfficeUnits');
//        $startDate        = !empty($startDate) ? $startDate : date("Y-m-d");
//        $endDate          = !empty($endDate) ? $endDate : date("Y-m-d");
//
//        $office_info = [];
//        $data        = [];
//        if (!empty($this->request->data)) {
//            $unit_Id = intval($this->request->data['office_unit_id']);
//
//            $ownChildInfo = [];
//            if ($unit_Id == 0) {
//                $selected_office_section = $this->getCurrentDakSection();
//
//                $selfunit     = $OfficeUnitsTable->find('list',
//                        ['keyField' => 'id', 'valueField' => 'unit_name_bng'])->where(['id' => $selected_office_section['office_unit_id']])->toArray();
//                $ownChildInfo = $OfficeUnitsTable->getOwnChildIdandName($selected_office_section['office_unit_id']);
//
//                $ownChildInfo = ($selfunit + $ownChildInfo);
//            }
//            else {
//                $selfunit                      = $OfficeUnitsTable->find()->select(['id',
//                        'unit_name_bng'])->where(['id' => $unit_Id])->first();
//                $ownChildInfo[$selfunit['id']] = $selfunit['unit_name_bng'];
//            }
//
//            $result = [];
//            if (!empty($ownChildInfo)) {
//                foreach ($ownChildInfo as $unit_Id => $unitname) {
//                    $data                     = $reportsTable->performanceReport(0, $unit_Id, 0,
//                        ['date_start' => $startDate, 'date_end' => $endDate]);
//                    $datetime1                = date_create($startDate);
//                    $datetime2                = date_create($endDate);
//                    $interval                 = date_diff($datetime1, $datetime2);
//                    $data[0]['totalLogin']    = (int) ($data[0]['totalLogin'] / ($interval->format('%d')
//                        + 1));
//                    $result[$unit_Id]         = $data[0];
//                    $result[$unit_Id]['name'] = $unitname;
//                }
//            }
//        }
//
//        $this->set(compact('result'));
//    }
//    public function Designation()
//    {
//        $selected_office_section = $this->getCurrentDakSection();
//        $this->set(compact('selected_office_section'));
//        $OfficeUnitsTable        = TableRegistry::get('OfficeUnits');
////        $selfunit                = $OfficeUnitsTable->find('list',
////                ['keyField' => 'id', 'valueField' => 'unit_name_bng'])->where(['id' => $selected_office_section['office_unit_id']])->toArray();
////        $ownChildInfo            = $OfficeUnitsTable->getOwnChildIdandName($selected_office_section['office_unit_id']);
//        $ownChildInfo            = $OfficeUnitsTable->getOfficeUnitsList($selected_office_section['office_id']);
//        $this->set(compact('ownChildInfo'));
//    }
//
//    public function DesignationContent($startDate = '', $endDate = '')
//    {
//        $this->layout         = null;
//        $reportsTable         = TableRegistry::get('Reports');
//        $EmployeeRecordsTable = TableRegistry::get('EmployeeRecords');
//
//        $OrganogramTable  = TableRegistry::get('OfficeUnitOrganograms');
//        $OfficeUnitsTable = TableRegistry::get('OfficeUnits');
//        $startDate        = !empty($startDate) ? $startDate : date("Y-m-d");
//        $endDate          = !empty($endDate) ? $endDate : date("Y-m-d");
//
//        $office_info = [];
//        $data        = [];
//        if (!empty($this->request->data)) {
//            $unit_Id = intval($this->request->data['office_unit_id']);
//
//            $ownChildInfo = [];
//            if ($unit_Id == 0) {
//                $selected_office_section = $this->getCurrentDakSection();
//
//                $selfunit     = $OfficeUnitsTable->find('list',
//                        ['keyField' => 'id', 'valueField' => 'unit_name_bng'])->where(['id' => $selected_office_section['office_unit_id']])->toArray();
//                $ownChildInfo = $OfficeUnitsTable->getOwnChildIdandName($selected_office_section['office_unit_id']);
//
//                $ownChildInfo = ($selfunit + $ownChildInfo);
//            }
//            else {
//                $selfunit                      = $OfficeUnitsTable->find()->select(['id',
//                        'unit_name_bng'])->where(['id' => $unit_Id])->first();
//                $ownChildInfo[$selfunit['id']] = $selfunit['unit_name_bng'];
//            }
//
//            if (!empty($ownChildInfo)) {
//                foreach ($ownChildInfo as $unit_Id => $unitname) {
//                    $organogram_info = $OrganogramTable->getDesignationsByUnitId($unit_Id);
//
//                    if (!empty($organogram_info)) {
//                        foreach ($organogram_info as $key => $val) {
//                            $result                  = $reportsTable->performanceReport(0, 0, $key,
//                                ['date_start' => $startDate, 'date_end' => $endDate]);
//                            $EmployeeSummary         = $EmployeeRecordsTable->getEmployeeInfoByDesignation($key);
////                            pr($EmployeeSummary);die;
//                            $datetime1               = date_create($startDate);
//                            $datetime2               = date_create($endDate);
//                            $interval                = date_diff($datetime1, $datetime2);
//                            $result[0]['totalLogin'] = (int) ($result[0]['totalLogin'] / ($interval->format('%d')
//                                + 1));
//                            $data[$key]['result']    = $result[0];
//                            if (!empty($EmployeeSummary['name_bng'])) {
//                                $data[$key]['result']['name'] = $EmployeeSummary['name_bng'].", ".$val;
//                            }
//                            else {
//                                $data[$key]['result']['name'] = $val;
//                            }
//                        }
//                    }
//                }
//            }
//        }
//
//        $this->set(compact('data'));
//    }
    /**
     * Office Performance For Superman
     */
    public function superadminOfficePerformance()
    {
        $this->render('superman_office_performance');
    }

    public function superadminOfficeContent($startDate = '', $endDate = '')
    {
        set_time_limit(0);
        $this->layout = null;
        $reportsTable = TableRegistry::get('Reports');
        $performanceOfficesTable = TableRegistry::get('PerformanceOffices');
        $OfficesTable = TableRegistry::get('Offices');
        $user_login_history_table = TableRegistry::get('UserLoginHistory');
        $employee_offices_table = TableRegistry::get('EmployeeOffices');
        $startDate = !empty($startDate) ? $startDate : date("Y-m-d");
        $endDate = !empty($endDate) ? $endDate : date("Y-m-d");

        $office_info = [];
        $data = [];
        if (!empty($this->request->data)) {
            $office_ids = $this->request->data['office_ids'];
            $office_id = explode(',', $office_ids);
            if (!empty($office_id)) {
                foreach ($office_id as $key => $val) {
//                    echo $val."<br>";
                    try {
                        $condition = [
                            'totalInbox' => 'SUM(inbox)', 'totalOutbox' => 'SUM(outbox)', 'totalNothijato' => 'SUM(nothijat)',
                            'totalNothivukto' => 'SUM(nothivukto)', 'totalNisponnoDak' => 'SUM(nisponnodak)',
                            'totalSouddog' => 'SUM(selfnote)',
                            'totalDaksohoNote' => 'SUM(daksohonote)', 'totalNiponnoPotrojari' => 'SUM(nisponnopotrojari)',
                            'totalNisponnoNote' => 'SUM(nisponnonote)',
                            'totalNisponno' => 'SUM(nisponno)', 'totalONisponno' => 'SUM(onisponno)',
                            'totalID' => 'count(id)'
                        ];

                        $result = $performanceOfficesTable->getOfficeData($val,
                            [$startDate, $endDate])->select($condition)->group(['office_id'])->toArray();
                        if (!empty($result[0]['totalID']) && $result[0]['totalID'] > 0) {
                            $totalEmployee = $employee_offices_table->getAllEmployeeRecordID($val,
                                0, 0);
                            /* Total Login */
                            $TotalLogin = $user_login_history_table->countTotalLogin($val,
                                0, 0, [$startDate, $endDate]);
                            $result[0]['totalUser'] = count($totalEmployee);
                            $result[0]['totalLogin'] = $TotalLogin->count();
                            $lastonisponno = $performanceOfficesTable->getLastOnisponno($val,
                                [$startDate, $endDate]);
                            $result[0]['totalONisponnoDak'] = $lastonisponno['onisponnodak'];
                            $result[0]['totalONisponnoNote'] = $lastonisponno['onisponnonote'];
                        } else {
                            $this->switchOffice($val, 'OfficeDB');
                            $result = $reportsTable->performanceReport($val, 0, 0,
                                ['date_start' => $startDate, 'date_end' => $startDate]);
                        }
//                        pr($result);
//                        die;
                        $datetime1 = date_create($startDate);
                        $datetime2 = date_create($startDate);
                        $interval = date_diff($datetime1, $datetime2);
                        $result[0]['totalLogin'] = (int)($result[0]['totalLogin'] / ($interval->format('%d')
                                + 1));
                        $data[$val] = $result[0];
                        $data[$val]['name'] = $OfficesTable->getBanglaName($val)['office_name_bng'];
                    } catch (\Exception $ex) {
                        continue;
                    }
                }
            }
        }

        $this->set(compact('data', 'office_info'));
        $this->render('new_office_content');
    }

    public function getNoteData($officeid, $unitid, $designationid, $date = '')
    {
        if (empty($date)) {
            die;
        }
        if (!empty($officeid)) {
            try {
                $this->switchOffice($officeid, 'Performance');
                $DakMovementsTable = TableRegistry::get('DakMovements');
                $potrojariTable = TableRegistry::get('Potrojari');
                $nothiPartTable = TableRegistry::get('NothiParts');
                $employee_offices_table = TableRegistry::get('employee_offices');
                $user_login_history_table = TableRegistry::get('UserLoginHistory');
                TableRegistry::remove('NothiMasterCurrentUsers');
                $NothiMasterCurrentUsersTable = TableRegistry::get('NothiMasterCurrentUsers');
                $NothiDakPotroMapsTable = TableRegistry::get('NothiDakPotroMaps');
                $returnSummary['nothiNotes'] = $nothiPartTable->selfSrijitoNotePart($officeid, $unitid, $designationid, $date);
                $returnSummary['nothiVuktoNotes'] = $nothiPartTable->notePartListfornothiVuktoDak($officeid, $unitid, $designationid, $date);
                $returnSummary['totaltodaydaksohonote'] = $nothiPartTable->dakSrijitoNoteCount($officeid,
                    $unitid, $designationid, [$date, $date]);
                $returnSummary['totaltodaysrijitonote'] = $nothiPartTable->selfSrijitoNoteCount($officeid,
                    $unitid, $designationid, [$date, $date]);
                $returnSummary['totaltodaypotrojari'] = $potrojariTable->getNisponnobyPotrojari($officeid,
                    $unitid, $designationid, [$date, $date]);
                $returnSummary['totaltodaynisponnonote'] = $NothiMasterCurrentUsersTable->nisponnoNoteCount($officeid,
                        $unitid, $designationid, [$date, $date]) - $returnSummary['totaltodaypotrojari'];
                $returnSummary['totaltodayOnisponnonote'] = $NothiMasterCurrentUsersTable->getOnisponnoNoteCount($officeid,
                    $unitid, $designationid, [$date, $date]);
                echo "SelfNotes:<br>";
                if (!empty($returnSummary['nothiNotes'])) {
                    $i10 = 0;
                    foreach ($returnSummary['nothiNotes'] as $val) {
                        echo $val;
                        $i10++;
                        if ($i10 == 10) {
                            $i10 = 0;
                            echo "<br>";
                        } else {
                            echo ',';
                        }
                    }
                }
                echo "<br><br><br>DakNotes:<br>";
                if (!empty($returnSummary['nothiVuktoNotes'])) {
                    $i10 = 0;
                    foreach ($returnSummary['nothiVuktoNotes'] as $val) {
                        echo $val;
                        $i10++;
                        if ($i10 == 10) {
                            $i10 = 0;
                            echo "<br>";
                        } else {
                            echo ',';
                        }
                    }
                }
                echo "<br><br><br>daksohonote:" . $returnSummary['totaltodaydaksohonote'] . "<br>";
                echo "<br>srijitonote:" . $returnSummary['totaltodaysrijitonote'] . "<br>";
                echo "<br>potrojari nisponno:" . $returnSummary['totaltodaypotrojari'] . "<br>";
                echo "<br>note nisponno :" . $returnSummary['totaltodaynisponnonote'] . "<br>";
                echo "<br>Onisponno note:" . $returnSummary['totaltodayOnisponnonote'] . "<br>";
                die;
            } catch (Exception $ex) {
                die;
            }
        }
    }

    public function testPerformanceOneMonth($officeId, $start_date, $end_date)
    {

        $table = TableRegistry::get('Reports');
        $this->switchOffice($officeId, "ReportDB");
        $data = $table->officeDateSummary($officeId, $start_date, $end_date);

        echo json_encode($data);
        die;
    }

    public function updatePerformanceOffices($date_start = '')
    {
        ini_set('memory_limit', -1);
        set_time_limit(0);
        $office_domains = TableRegistry::get('OfficeDomains');
        $officesTable = TableRegistry::get('Offices');
        if ($this->request->is('get')) {
            $this->layout = 'dashboard';
            $all_offices = $office_domains->find()->where(['status' => 1])->toArray();
            $this->set(compact('all_offices'));
            $this->set(compact('date_start'));
        }
        if ($this->request->is('post')) {
            $performanceOfficeTable = TableRegistry::get('PerformanceOffices');
            $office_id = 0;
            $data = [
                'status' => 'Error',
                'msg' => 'No office Id'
            ];
            $count = 0;
            $begin = new \DateTime("2016-06-01");
            $end = new \DateTime(date('Y-m-d'));

            if (!empty($this->request->data['office_id'])) {
                $office_id = $this->request->data['office_id'];
                $office_record = $performanceOfficeTable->getLastUpdateTime($office_id);
                if (!empty($this->request->data['date_start'])) {
                    $begin = new \DateTime(($this->request->data['date_start']));
                } else if (!empty($office_record)) {
                    $begin = $office_record['record_date'];
                } else {
                    $begin = new \DateTime('2016-07-01');
                }
                $end = new \DateTime(date('Y-m-d'));
                $period = [];
                for ($i = $begin; $begin < $end; $i->modify('+1 day')) {
                    $period [] = $i->format("Y-m-d");
                }
                if (!empty($period)) {
                    foreach ($period as $dt) {
                        $date = $dt;
                        $office_record = $performanceOfficeTable->checkNothiUpdate($office_id, $date);
                        if ($office_record > 0) {
                            try {

                                $this->switchOffice($office_id, 'OfficeDB');
                                $office_data = $performanceOfficeTable->getOfficeData($office_id,
                                    [$date, $date])->first();
                                $result = TableRegistry::get('Dashboard')->nothiDashboardCount($office_id,
                                    0, 0, [$date, $date]);
//                                pr($result);echo $date; die;
                                $office_data['selfnote'] = $result['selfNote'];
                                $office_data['daksohonote'] = $result['dakNote'];
                                $office_data['potrojari'] = TableRegistry::get('potrojari')->allPotrojariCount($office_id,
                                    0, 0, [$date, $date]);
                                $office_data['nisponnonote'] = $result['noteNisponno'];
                                $office_data['nisponnopotrojari'] = $result['potrojariNisponno'];
                                $office_data['onisponnonote'] = TableRegistry::get('Dashboard')->getOnisponnoNoteCount($office_id, 0, 0, [$date, $date]);
                                $office_data['updated'] = 1;

                                if ($performanceOfficeTable->save($office_data)) {
                                    $count++;
                                }
                            } catch (\Exception $ex) {
                                $data = [
                                    'status' => 'Error',
                                    'msg' => 'Office DB not set'
                                ];
                                $this->response->type('application/json');
                                $this->response->body(json_encode($data));
                                return $this->response;
                            }
                        }
                    }
                } else {
                    $data = [
                        'status' => 'Success',
                        'msg' => 'Already Updated'
                    ];
                }
            }
            if ($count > 0) {
                $data = [
                    'status' => 'Success',
                    'msg' => $count . " Day"
                ];
            }
            if (!empty($period) && $count == 0) {
                $data = [
                    'status' => 'Success',
                    'msg' => 'Already Updated'
                ];
            }
            $this->response->type('application/json');
            $this->response->body(json_encode($data));
            return $this->response;
        }
    }

    public function updatePerformanceUnits($date_start = '')
    {
        set_time_limit(0);
        ini_set('memory_limit', -1);
        $unitsTable = TableRegistry::get('OfficeUnits');
        if ($this->request->is('get')) {
            $this->layout = 'dashboard';
            $employee_offices = TableRegistry::get('EmployeeOffices');
//            $all_units    = $unitsTable->find('list')->where(['active_status' => 1])->toArray();
            $all_units = $employee_offices->getAllUnitID()->toArray();
            $this->set(compact('all_units'));
            $this->set(compact('date_start'));
//            $this->view       = 'performance_report_units_from_beginning';
        }
        if ($this->request->is('post')) {
            $performanceUnitTable = TableRegistry::get('PerformanceUnits');
            $unit_id = 0;
            $data = [
                'status' => 'Error',
                'msg' => 'No office Id'
            ];
            $count = 0;

            if (!empty($this->request->data['unit_id'])) {
                $unit_id = $this->request->data['unit_id'];
                $unit_record = $performanceUnitTable->getLastUpdateTime($unit_id);
                $period = [];
                if (!empty($this->request->data['date_start'])) {
                    $begin = new \DateTime(($this->request->data['date_start']));
                } else if (!empty($unit_record)) {
                    $begin = ($unit_record['record_date']);
                } else {
                    $begin = new \DateTime('2016-07-01');
                }
                $end = new \DateTime(date('Y-m-d'));

                for ($i = $begin; $begin < $end; $i->modify('+1 day')) {
                    $period [] = $i->format("Y-m-d");
                }
                if (!empty($period)) {
                    foreach ($period as $dt) {
                        $date = $dt;
                        $unit_record = $performanceUnitTable->checkNothiUpdate($unit_id, $date);
                        if ($unit_record > 0) {
                            try {
                                $unit_related_data = $unitsTable->getAll(['id' => $unit_id],
                                    ['office_ministry_id', 'office_layer_id', 'office_origin_unit_id',
                                        'office_id', 'unit_name_bng'])->first();

                                $this->switchOffice($unit_related_data['office_id'], 'OfficeDB');
                                $office_id = $unit_related_data['office_id'];

                                $unit_data = $performanceUnitTable->getUnitData($unit_id, [$date, $date])->first();

                                $result = TableRegistry::get('Dashboard')->nothiDashboardCount($office_id,
                                    $unit_id, 0, [$date, $date]);

//                                pr($result);echo $date; die;
                                $unit_data['selfnote'] = $result['selfNote'];
                                $unit_data['daksohonote'] = $result['dakNote'];
                                $unit_data['potrojari'] = TableRegistry::get('potrojari')->allPotrojariCount($office_id,
                                    $unit_id, 0, [$date, $date]);
                                $unit_data['nisponnonote'] = $result['noteNisponno'];
                                $unit_data['nisponnopotrojari'] = $result['potrojariNisponno'];
                                $unit_data['onisponnonote'] = TableRegistry::get('Dashboard')->getOnisponnoNoteCount($office_id, $unit_id, 0, [$date, $date]);
                                $unit_data['updated'] = 1;

                                if ($performanceUnitTable->save($unit_data)) {
                                    $count++;
                                }
                            } catch (\Exception $ex) {
                                $data = [
                                    'status' => 'Error',
                                    'msg' => 'Office DB not set'
                                ];
                                $this->response->type('application/json');
                                $this->response->body(json_encode($data));
                                return $this->response;
                            }
                        }
                    }
                } else {
                    $data = [
                        'status' => 'Success',
                        'msg' => 'Already Updated'
                    ];
                }
            }
            if ($count > 0) {
                $data = [
                    'status' => 'Success',
                    'msg' => $count . " Day"
                ];
            }
            if (!empty($period) && $count == 0) {
                $data = [
                    'status' => 'Success',
                    'msg' => 'Already Updated'
                ];
            }
            $this->response->type('application/json');
            $this->response->body(json_encode($data));
            return $this->response;
        }
    }

    public function updatePerformanceDesignations($date_start = '')
    {
        set_time_limit(0);
        ini_set('memory_limit', -1);
        $employeeOfficesTable = TableRegistry::get('EmployeeOffices');
        if ($this->request->is('get')) {
            $this->layout = 'dashboard';
            $all_designations = $employeeOfficesTable->getAllDesignationID()->toArray();
//            pr($all_designations);die;
            $this->set(compact('all_designations'));
//            $this->view       = 'performance_report_designations_from_beginning';
            $this->set(compact('date_start'));
        }
        if ($this->request->is('post')) {
            $performanceDesignationTable = TableRegistry::get('PerformanceDesignations');
            $designation_id = 0;
            $data = [
                'status' => 'Error',
                'msg' => 'No office Id'
            ];
            $count = 0;

            if (!empty($this->request->data['designation_id'])) {

                $designation_id = $this->request->data['designation_id'];
                $designation_record = $performanceDesignationTable->getLastUpdateTime($designation_id);
                if (!empty($this->request->data['date_start'])) {
                    $begin = new \DateTime($this->request->data['date_start']);
                } else if (!empty($designation_record)) {
                    $begin = ($designation_record['record_date']);
                } else {
                    $begin = new \DateTime('2016-07-01');
                }
                $end = new \DateTime(date('Y-m-d'));
                $period = [];
                for ($i = $begin; $begin < $end; $i->modify('+1 day')) {
                    $period [] = $i->format("Y-m-d");
                }
                if (!empty($period)) {
                    foreach ($period as $dt) {
                        $date = $dt;
                        $designation_record = $performanceDesignationTable->checkNothiUpdate($designation_id,
                            $date);
                        if ($designation_record > 0) {
                            try {
                                $designation_related_data = $employeeOfficesTable->getDesignationInfo($designation_id);

                                $this->switchOffice($designation_related_data['office_id'], 'OfficeDB');
                                $office_id = $designation_related_data['office_id'];

                                $designation_data = $performanceDesignationTable->getDesignationData($designation_id, [$date, $date])->first();

                                $result = TableRegistry::get('Dashboard')->nothiDashboardCount($office_id,
                                    $designation_related_data['office_unit_id'], $designation_id, [$date, $date]);

//                                pr($result);echo $date; die;
                                $designation_data['selfnote'] = $result['selfNote'];
                                $designation_data['daksohonote'] = $result['dakNote'];
                                $designation_data['potrojari'] = TableRegistry::get('potrojari')->allPotrojariCount($office_id,
                                    $designation_related_data['office_unit_id'], $designation_id, [$date, $date]);
                                $designation_data['nisponnonote'] = $result['noteNisponno'];
                                $designation_data['nisponnopotrojari'] = $result['potrojariNisponno'];
                                $designation_data['onisponnonote'] = TableRegistry::get('Dashboard')->getOnisponnoNoteCount($office_id, $designation_related_data['office_unit_id'], $designation_id, [$date, $date]);
                                $designation_data['updated'] = 1;

                                if ($performanceDesignationTable->save($designation_data)) {
                                    $count++;
                                }
                            } catch (\Exception $ex) {
                                $data = [
                                    'status' => 'Error',
                                    'msg' => 'Office DB not set'
                                ];
                                $this->response->type('application/json');
                                $this->response->body(json_encode($data));
                                return $this->response;
                            }
                        }
                    }
                } else {
                    $data = [
                        'status' => 'Success',
                        'msg' => 'Already Updated'
                    ];
                }
            }
            if ($count > 0) {
                $data = [
                    'status' => 'Success',
                    'msg' => $count . " Day"
                ];
            }
            if (!empty($period) && $count == 0) {
                $data = [
                    'status' => 'Success',
                    'msg' => 'Already Updated'
                ];
            }
            $this->response->type('application/json');
            $this->response->body(json_encode($data));
            return $this->response;
        }

    }

    public function unitPending()
    {
        $selected_office_section = $this->getCurrentDakSection();
        $this->set(compact('selected_office_section'));
        $OfficeUnitsTable = TableRegistry::get('OfficeUnits');
//        $selfunit                = $OfficeUnitsTable->find('list',
//                ['keyField' => 'id', 'valueField' => 'unit_name_bng'])->where(['id' => $selected_office_section['office_unit_id']])->toArray();
//        $ownChildInfo            = $OfficeUnitsTable->getOwnChildIdandName($selected_office_section['office_unit_id']);
        if (empty($selected_office_section)) {
            $ownChildInfo = $OfficeUnitsTable->find('list')->where(['active_status' => 1])->order(['unit_level asc, parent_unit_id asc', 'id desc'])->toArray();
        } else {
            $ownChildInfo = $OfficeUnitsTable->getOfficeUnitsList($selected_office_section['office_id']);
        }
        $data = [];
        $flag = 0;
        foreach ($ownChildInfo as $unit_id => $unit_name) {
            $data[$flag] = [
                $unit_id, $unit_name
            ];
            $flag++;
        }
        $this->set('ownChildInfo_sorted', $data);
        $this->set(compact('ownChildInfo'));
    }

    public function unitPendingReports()
    {
        set_time_limit(0);
        $office_id = $this->request->data['office_id'];
        $unit_id = $this->request->data['unit_id'];
        $today = date('Y-m-d');
        try {
            $this->switchOffice($office_id, 'DashboardOffices');
            TableRegistry::remove('DakMovements');
            TableRegistry::remove('DakUsers');

            $totalOnisponnodakall = TableRegistry::get('DakUsers')->getOnisponnoDak($office_id, $unit_id, 0,$today);
                $totalOnisponnonoteall = TableRegistry::get('Dashboard')->getOnisponnoNoteCount($office_id,
                    $unit_id, 0, [$today, $today]);

            if ($totalOnisponnodakall < 0) {
                $totalOnisponnodakall = 0;
            }
            if ($totalOnisponnonoteall < 0) {
                $totalOnisponnonoteall = 0;
            }
            $data = [
                'dak' => $totalOnisponnodakall,
                'note' => $totalOnisponnonoteall,
                'status' => true
            ];
        } catch (\Exception $ex) {
            $data['status'] = false;
        }
        $this->response->type('application/json');
        $this->response->body(json_encode($data));
        return $this->response;
    }

    public function designationPending()
    {
        $selected_office_section = $this->getCurrentDakSection();
        if ($this->request->is('get')) {
            $this->set(compact('selected_office_section'));
            $OfficeUnitsTable = TableRegistry::get('OfficeUnits');
            if (empty($selected_office_section)) {
                $ownChildInfo = $OfficeUnitsTable->find('list')->where(['active_status' => 1])->order(['unit_level asc, parent_unit_id asc',
                    'id desc'])->toArray();
            } else {
                $ownChildInfo = $OfficeUnitsTable->getOfficeUnitsList($selected_office_section['office_id']);
            }
            $this->set(compact('ownChildInfo'));
        }
        if ($this->request->is('post')) {
            $unit_id = $this->request->data['unit_id'];
            $show = isset($this->request->data['show'])?$this->request->data['show']:2;
            $data = [];

            if(empty($unit_id)){
                goto rtn;
            }
            else if($unit_id == -1){
                $employeeOfficesTable = TableRegistry::get('EmployeeOffices');
                if($show == 0){
                    $designation_info_status_1 = $employeeOfficesTable->getAllDesignationByOfficeOrUnitID($selected_office_section['office_id'],0,1)->order(['EmployeeOffices.designation_level ASC','EmployeeOffices.designation_sequence ASC'])->distinct(['office_unit_organogram_id'])->toArray();
                    $designation_info_status_0 = $employeeOfficesTable->getAllDesignationByOfficeOrUnitID($selected_office_section['office_id'],0,0)->order(['EmployeeOffices.designation_level ASC','EmployeeOffices.designation_sequence ASC'])->distinct(['office_unit_organogram_id'])->toArray();
                    $result = array_diff($designation_info_status_0,$designation_info_status_1);
                }
                else if($show == 1){
                    $result = $employeeOfficesTable->getAllDesignationByOfficeOrUnitID($selected_office_section['office_id'],0,1)->distinct(['office_unit_organogram_id'])->order(['EmployeeOffices.designation_level ASC','EmployeeOffices.designation_sequence ASC'])->toArray();
                }else{
                    $result = $employeeOfficesTable->getAllDesignationByOfficeOrUnitID($selected_office_section['office_id'])->distinct(['office_unit_organogram_id'])->order(['EmployeeOffices.designation_level ASC','EmployeeOffices.designation_sequence ASC'])->toArray();
                }
                if(!empty($result)){
                    foreach ($result as $designation_id){
                        $data[] = $designation_id;
                    }
                }
            }else{
                $employeeOfficesTable = TableRegistry::get('EmployeeOffices');
                if($show == 0){
                    $designation_info_status_1 = $employeeOfficesTable->getAllDesignationByOfficeOrUnitID($selected_office_section['office_id'],$unit_id,1)->distinct(['office_unit_organogram_id'])->order(['EmployeeOffices.designation_level ASC','EmployeeOffices.designation_sequence ASC'])->toArray();
                    $designation_info_status_0 = $employeeOfficesTable->getAllDesignationByOfficeOrUnitID($selected_office_section['office_id'],$unit_id,0)->distinct(['office_unit_organogram_id'])->order(['EmployeeOffices.designation_level ASC','EmployeeOffices.designation_sequence ASC'])->toArray();
                    $designation_info_stay = array_diff($designation_info_status_0,$designation_info_status_1);
                }
                else if($show == 1){
                    $designation_info_stay = $employeeOfficesTable->getAllDesignationByOfficeOrUnitID($selected_office_section['office_id'],$unit_id,1)->distinct(['office_unit_organogram_id'])->order(['EmployeeOffices.designation_level ASC','EmployeeOffices.designation_sequence ASC'])->toArray();
                }else{
                    $designation_info_stay = $employeeOfficesTable->getAllDesignationByOfficeOrUnitID($selected_office_section['office_id'],$unit_id)->distinct(['office_unit_organogram_id'])->order(['EmployeeOffices.designation_level ASC','EmployeeOffices.designation_sequence ASC'])->toArray();
                }
                $OfficeUnitOrganogramsTable = TableRegistry::get('OfficeUnitOrganograms');
                $designation_info = $OfficeUnitOrganogramsTable->getAllDesignationsByUnitId($unit_id);

                if (!empty($designation_info)) {
                    foreach ($designation_info as $key => $val) {
                        if(in_array($key,$designation_info_stay)){
                            $data[] = ['id' => $key, 'val' => $val];
                        }
                    }
                }
            }
            rtn:
            $this->response->type('application/json');
            $this->response->body(json_encode($data));
            return $this->response;
        }
    }

    public function designationPendingReports()
    {
        set_time_limit(0);
        $office_id = $this->request->data['office_id'];
        $unit_id = $this->request->data['unit_id'];
        $designation_id = $this->request->data['designation_id'];
        $today = date('Y-m-d');
        $EmployeeOfficesTable = TableRegistry::get('EmployeeOffices');
        try {
            $this->switchOffice($office_id, 'DashboardOffices');
            TableRegistry::remove('DakUsers');

            $totalOnisponnodakall = TableRegistry::get('DakUsers')->getOnisponnoDak($office_id, $unit_id, $designation_id, $today);
            $totalOnisponnonoteall = TableRegistry::get('Dashboard')->getOnisponnoNoteCount($office_id,
                    $unit_id, $designation_id, [$today, $today]);

            if ($totalOnisponnodakall < 0) {
                $totalOnisponnodakall = 0;
            }
            if ($totalOnisponnonoteall < 0) {
                $totalOnisponnonoteall = 0;
            }
            $designation_info = $EmployeeOfficesTable->getAllEmployeeRecords($office_id, $unit_id, $designation_id);
            $sts = 0;
            $name = '';
            if(!empty($designation_info)){
                foreach($designation_info as $val){
                    if(!empty($val['name_bng'])){
                        $name = $val['name_bng'];
                        $designation = $val['designation'];
                        if(!empty($val['office_unit_id'])){
                          $unit_id =  $val['office_unit_id'];
                        }
                    }
                    if($val['status'] == 1){
                        $sts = 1;
                        break;
                    }
                }
            }
            if (($unit_info = Cache::read('unit_name_' . $unit_id, 'memcached')) === false) {
                $unit_info = TableRegistry::get('OfficeUnits')->getBanglaName($unit_id);
                Cache::write('unit_name_' . $unit_id, $unit_info, 'memcached');
            }
            $data = [
                'dak' => $totalOnisponnodakall,
                'note' => $totalOnisponnonoteall,
                'name' => isset($name)?$name:'',
                'designation' => (isset($designation)?$designation:'').(isset($unit_info['unit_name_bng'])?', '.$unit_info['unit_name_bng']:''),
                'enable' => ($sts == 1)?' হ্যাঁ ': ' না ',
                'sts' => $sts,
                'status' => true
            ];
        } catch (\Exception $ex) {
            $data['status'] = false;
        }
        $this->response->type('application/json');
        $this->response->body(json_encode($data));
        return $this->response;
    }

    public function singleDesignationContent($startDate = '', $endDate = '')
    {
        set_time_limit(0);
        $this->layout = null;
        $reportsTable = TableRegistry::get('Reports');
        $EmployeeRecordsTable = TableRegistry::get('EmployeeRecords');
        $performanceOfficesTable = TableRegistry::get('PerformanceOffices');
        $performanceDesignationsTable = TableRegistry::get('PerformanceDesignations');
        $employee_offices_table = TableRegistry::get('EmployeeOffices');
        $user_login_history_table = TableRegistry::get('UserLoginHistory');
        $OrganogramTable = TableRegistry::get('OfficeUnitOrganograms');
        $OfficeUnitsTable = TableRegistry::get('OfficeUnits');
        $startDate = !empty($startDate) ? $startDate : date("Y-m-d");
        $endDate = !empty($endDate) ? $endDate : date("Y-m-d");

        $data = [];
        if (!empty($this->request->data)) {
            $key = intval($this->request->data['office_unit_organogram_id']);
            $unit_id = intval($this->request->data['office_unit_id']);
            $office_id = intval($this->request->data['office_id']);
            if(!empty($this->request->data['designation_name'])){
                 $val =$this->request->data['designation_name'];
            }else{
                 $val =$OrganogramTable->getBanglaName($key)['designation_bng'];
            }
            if(!empty($this->request->data['unit_name'])){
                 $unitname =$this->request->data['unit_name'];
            }else{
                $unitname =$OfficeUnitsTable->getBanglaName($unit_id)['unit_name_bng'];
            }
           
            
            if (!empty($key)) {
                            try {
                                $today = date('Y-m-d');
                                    $condition = [
                                        'totalInbox' => 'SUM(inbox)', 'totalOutbox' => 'SUM(outbox)', 'totalNothijato' => 'SUM(nothijat)',
                                        'totalNothivukto' => 'SUM(nothivukto)', 'totalNisponnoDak' => 'SUM(nisponnodak)',
                                        'totalSouddog' => 'SUM(selfnote)',
                                        'totalDaksohoNote' => 'SUM(daksohonote)', 'totalNisponnoPotrojari' => 'SUM(nisponnopotrojari)',
                                        'totalNisponnoNote' => 'SUM(nisponnonote)',
                                        'totalNisponno' => 'SUM(nisponno)', 'totalONisponno' => 'SUM(onisponno)',
                                            'totalID' => 'count(id)'
                                    ];
                                    $result = $performanceDesignationsTable->getDesignationData($key,
                                        [$startDate, $endDate])->select($condition)->group(['designation_id'])->toArray();
                                    if (!empty($result[0]['totalID']) && $result[0]['totalID'] > 0) {
                                      $lastonisponno = $performanceDesignationsTable->getLastOnisponno($key,
                                            [$startDate, $endDate]);
                                        $result[0]['totalONisponnoDak'] = $lastonisponno['onisponnodak'];
                                        $result[0]['totalONisponnoNote'] = $lastonisponno['onisponnonote'];
                                    } else {
                                        $this->switchOffice($office_id, 'OfficeDB');
                                        $result = $reportsTable->performanceReport($office_id, $unit_id, $key,
                                            ['date_start' => $startDate, 'date_end' => $endDate]);
                                    }
                                     $EmployeeSummary = $EmployeeRecordsTable->getEmployeeInfoByDesignation($key);
                                    $result[0]['lastUpdate'] =$performanceDesignationsTable->getLastUpdateTime($key)['record_date'];
                                    $result[0]['lastUpdate'] =(!empty($result[0]['lastUpdate']))? $result[0]['lastUpdate']->format('Y-m-d'):'';
                                    $result[0]['totalSouddog'] = (($result[0]['totalSouddog'] < 0) ? 0: $result[0]['totalSouddog']);
                                    $data[$key]['result'] = $result[0];
                                    if (!empty($EmployeeSummary['name_bng'])) {
                                        $data[$key]['result']['name'] = $EmployeeSummary['name_bng'] . ", " . $val.", ".$unitname;
                                    } else {
                                        $data[$key]['result']['name'] = $val.", ".$unitname;
                                    }

                            } catch (\Exception $ex) {
                                
                            }
                           
                    }
        }
        $this->response->type('application/json');
        $this->response->body(json_encode($data));
        return $this->response;
    }
    public function getAllDesignationByOfficeOrUnitID($office_id = 0, $unit_id = 0, $start_date = '',$end_date = '')
    {
        $selected_office_section = $this->getCurrentDakSection();
        if ($selected_office_section['is_admin'] == false) {
            if ($selected_office_section['office_unit_id'] != $unit_id) {
                $data = [];
                goto rtn;
            }
        }
        $data = [];
        if(empty($start_date) || empty($end_date)){
           goto rtn;
        }
        if(empty($office_id) && empty($unit_id)){
           goto rtn;
        }
//           $data = TableRegistry::get('PerformanceDesignations')->getAllDesignationByOfficeOrUnitID($office_id,$unit_id,[$start_date,$end_date],0,['designation_id','designation_name','office_id','office_name','unit_id','unit_name']);
            $data = TableRegistry::get('PerformanceDesignations')->getAllDesignationByOfficeOrUnitIDWithDesignationSort($office_id,$unit_id,[$start_date,$end_date],0,['designation_id','designation_name','office_id','office_name','unit_id','unit_name']);
            rtn:
                $this->response->type('application/json');
                $this->response->body(json_encode($data));
                return $this->response;
    }

    public function districtWiseOfficeReport()
    {
        $this->layout = null;
        $selected_office_section = $this->getCurrentDakSection();
        $office = TableRegistry::get('Offices')->get($selected_office_section['office_id']);
        if($office->office_origin_id == 16) {
            $this->set(compact('selected_office_section'));
            $OfficesTable = TableRegistry::get('Offices');
            if (empty($selected_office_section)) {
                $ChildOffices = $OfficesTable->find('list')->where(['active_status' => 1])->toArray();
            } else {
                $currentOffice = $OfficesTable->get($selected_office_section['office_id']);
                $districtOfficeLayers = TableRegistry::get('OfficeLayers')->find('list')->where(['layer_level' => 5])->toArray();
                $districtLevelOffices = $OfficesTable->find('list')->where(['office_layer_id IN' => array_keys($districtOfficeLayers), 'geo_district_id' => $currentOffice->geo_district_id])->toArray();
                $ChildOffices = $districtLevelOffices;
                $selfOffice[$selected_office_section['office_id']] = $selected_office_section['office_name'];
                $ChildOffices = $selfOffice + $ChildOffices;
            }
            $this->set(compact('ChildOffices'));
        } else {
            $this->Flash->error('এই অপশনটিতে আপনার অনুমতি নাই।');
            return $this->redirect('/dashboard');
        }
    }

    public function upazillaWiseOfficeReport()
    {
        $this->layout = null;
        $selected_office_section = $this->getCurrentDakSection();

        $this->set(compact('selected_office_section'));
        if (empty($selected_office_section)) {
            $geo_upazila_list = TableRegistry::get('GeoUpazilas')->find('list')->where(['status' => 1])->toArray();
            $this->set(compact('geo_upazila_list'));

            if ($this->request->is('post') && $this->request->is('ajax')) {
                $districtLevelOffices = TableRegistry::get('Offices')->find('list')->where(['geo_upazila_id' => $this->request->data['geo_upazilla_id']])->toArray();
                $this->response->type('application/json');
                $this->response->body(json_encode($districtLevelOffices));
                return $this->response;
            }
        } else {
            $currentOffice = TableRegistry::get('Offices')->get($selected_office_section['office_id']);

            if($currentOffice->office_origin_id != 17) {
                $this->Flash->error('এই অপশনটিতে আপনার অনুমতি নাই।');
                return $this->redirect('/dashboard');
            }
        }

        if (isset($currentOffice) && $currentOffice) {
            $OfficesTable = TableRegistry::get('Offices');

            $districtLevelOffices = $OfficesTable->find('list')->where(['geo_upazila_id' => $currentOffice->geo_upazila_id])->toArray();
            $ChildOffices = $districtLevelOffices;
            $selfOffice[$selected_office_section['office_id']] = $selected_office_section['office_name'];
            $ChildOffices = $selfOffice + $ChildOffices;

            $this->set(compact('ChildOffices'));
        }
    }

    public function customOfficeReport($origin_id)
    {
    	$originTable = TableRegistry::get('OfficeOrigins');
    	$orign = $originTable->get($origin_id);
    	$this->set('title', $orign->office_name_bng);
    	if (!$orign) {
			throw new NotFoundException();
		}

        $this->layout = null;
        $selected_office_section = $this->getCurrentDakSection();
		$OfficesTable = TableRegistry::get('Offices');
		$currentOffice = $OfficesTable->get($selected_office_section['office_id']);
		if($currentOffice->office_origin_id == 16) {
            $this->set(compact('selected_office_section'));
            if (empty($selected_office_section)) {
                $ChildOffices = $OfficesTable->find('list')->where(['active_status' => 1])->toArray();
            } else {
                $districtLevelOffices = $OfficesTable->find('list')->where(['office_origin_id' => $origin_id, 'geo_district_id' => $currentOffice->geo_district_id])->toArray();
                $ChildOffices = $districtLevelOffices;
                //$selfOffice[$selected_office_section['office_id']] = $selected_office_section['office_name'];
                //$ChildOffices = $selfOffice + $ChildOffices;
            }
            $this->set(compact('ChildOffices'));
        } else {
            $this->Flash->error('এই অপশনটিতে আপনার অনুমতি নাই।');
            return $this->redirect('/dashboard');
        }
    }

    public function districtWiseOfficeReportFromSuperman() {
        $this->layout = null;
        $selected_office_section = $this->getCurrentDakSection();
        //if(strpos($selected_office_section['office_name'], 'জেলা প্রশাসকের কার্যালয়') !== false) {
        $OfficesTable = TableRegistry::get('Offices');

        $geo_district_list = TableRegistry::get('GeoDistricts')->find('list')->where(['status' => 1])->toArray();
        if ($this->request->is('post')) {
            $districtOfficeLayers = TableRegistry::get('OfficeLayers')->find('list')->where(['layer_level' => 5])->toArray();
            //$districtLevelOffices = $OfficesTable->find('list')->where(['office_layer_id IN' => array_keys($districtOfficeLayers), 'geo_district_id' => $this->>request->data['geo_district_id']])->toArray();
            //$ChildOffices = $districtLevelOffices;
            //$selfOffice[$selected_office_section['office_id']] = $selected_office_section['office_name'];
            //$ChildOffices = $selfOffice + $ChildOffices;
        }
        //pr($geo_district_list);die;

        $this->set(compact('geo_district_list','ChildOffices'));
    }

    public function districtWiseOfficeListByGeoDistrictId() {
        $districtOfficeLayers = TableRegistry::get('OfficeLayers')->find('list')->where(['layer_level' => 5])->toArray();
        $districtLevelOffices = TableRegistry::get('Offices')->find('list')->where(['office_layer_id IN' => array_keys($districtOfficeLayers)]);
        if ($this->request->data['geo_district_id'] == 0) {
            $districtLevelOffices = $districtLevelOffices->toArray();
        } else {
            $districtLevelOffices = $districtLevelOffices->where(['geo_district_id' => $this->request->data['geo_district_id']])->toArray();
        }

        $this->response->type('application/json');
        $this->response->body(json_encode($districtLevelOffices));
        return $this->response;
    }
    public function categoryWiseOfficePerformance(){
        $reportCategoryTable = TableRegistry::get('ReportCategory');
        $reportMarkingTable = TableRegistry::get('ReportMarking');
        if($this->request->is('get')){
            try{
                $allCategory = $reportCategoryTable->find('list',['valueField'=>'name','keyField'=>'id'])->toArray();
                $columns =[
                    'dak_inbox','dak_outbox','dak_onisponno','dak_nothijat','dak_nothivukto','dak_nisponno',
                    'nothi_self_note','nothi_dak_note','nothi_note_niponno',
                    'nothi_potrojari_niponno','potrojari_nisponno_internal', 'potrojari_nisponno_external',
                    'nothi_onisponno','nothi_nisponno','potrojari',
                ];
                $allreportMarking = $reportMarkingTable->getData($columns,['status' => 1])->first();
                $this->set(compact('allCategory','allreportMarking','columns'));
            }catch (\Exception $ex){
                $this->Flash->error(__('Technical error happen'));
            }
        }else{
            $response = [
                'status' => 'error',
                'message' => 'Something went wrong'
            ];
            try{
                $data = $this->request->data;
                if(empty($data['category_id'])){
                    $response['message'] = 'ক্যাটাগরি নির্বাচন করা হয়নি';
                    goto rtn;
                }
                $reportCategoryEntity = $reportCategoryTable->get($data['category_id']);
                $offices_id = isset($reportCategoryEntity->offices)?jsonA($reportCategoryEntity->offices):[];
                if(!empty($offices_id)){
                    $offices_id = TableRegistry::get('Offices')->find('list')->where(['id IN' => $offices_id])->toArray();
                }
                $response = [
                    'status' => 'success',
                    'data' =>  $offices_id,
                ];
            }catch (\Exception $ex){
                $response = [
                    'reason' => $this->makeEncryptedData($ex->getMessage()),
                    'message' => __('Technical error happen'),
                ];
            }
            rtn:
            $this->response->type('json');
            $this->response->body(json_encode($response));
            return $this->response;
        }
    }
    public function performanceUnitUpdateByOfficeIDChecker($office_id, $date_start = '')
    {
        set_time_limit(0);
        ini_set('memory_limit', -1);
        $this->switchOffice($office_id, 'OfficeDB');


        $unitsTable = TableRegistry::get('OfficeUnits');
        $employee_offices = TableRegistry::get('EmployeeOffices');
        $all_units = $employee_offices->getAllUnitID()->where(['EmployeeOffices.office_id' => $office_id])->toArray();
        $performanceUnitTable = TableRegistry::get('PerformanceUnits');
        $reportsTable = TableRegistry::get('Reports');
        $MinistriesTable = TableRegistry::get('OfficeMinistries');
        $LayersTable = TableRegistry::get('OfficeLayers');
        $OfficesTable = TableRegistry::get('Offices');
        $UnitOriginsTable = TableRegistry::get('OfficeOriginUnits');
        $performance_designationTable = TableRegistry::get('PerformanceDesignations');
        TableRegistry::remove('DakUsers');
        TableRegistry::remove('NothiMasterCurrentUsers');


        foreach ($all_units as $unit_id => $unit_name) {
            $period = [];
            $unit_record = $performanceUnitTable->getLastUpdateTime($unit_id);
            if (!empty($date_start)) {
                $begin = new \DateTime($date_start);
            } else if (!empty($unit_record)) {
                $begin = ($unit_record['record_date']);
            } else {
                $begin = new \DateTime(date('Y-m-d', strtotime(date('Y-m-d') . ' -1 week')));
            }
            if (!empty($date_start)) {
                $end = new \DateTime($date_start);
            }else{
                $end = new \DateTime(date('Y-m-d'));
            }
            echo '<pre>';
            $performance_units_entity = [];

            for ($i = $begin; $begin <= $end; $i->modify('+1 day')) {
                $period [] = $i->format("Y-m-d");
            }
            if (!empty($period)) {
                try {
                    $unit_related_data = $unitsTable->getAll(['id' => $unit_id],
                        ['office_ministry_id', 'office_layer_id', 'office_origin_unit_id', 'office_id'])->first();
                    foreach ($period as $dt) {
//                        $has_record = $performanceUnitTable->find()->where(['record_date' => $dt, 'unit_id' => $unit_id])->count();
//                        if ($has_record > 0) {
//                            continue;
//                        }
                        $date = $dt;
//                        pr($unit_related_data['office_id']);die;
                        // Collect From Designation Table
                        $from_designation_report = true;
                        $all_designation = $employee_offices->getAllDesignationByOfficeOrUnitID($unit_related_data['office_id'],
                            $unit_id)->toArray();
                        $condition = [
                            'totalInbox' => 'SUM(inbox)', 'totalOutbox' => 'SUM(outbox)', 'totalNothijato' => 'SUM(nothijat)',
                            'totalNothivukto' => 'SUM(nothivukto)', 'totalNisponnoDak' => 'SUM(nisponnodak)', 'totalpotrojariall' => 'SUM(potrojari)',
                            'totalSouddog' => 'SUM(selfnote)', 'totalONisponnoDak' => 'SUM(onisponnodak)',
                            'totalDaksohoNote' => 'SUM(daksohonote)', 'totalNisponnoPotrojari' => 'SUM(nisponnopotrojari)',
                            'totalNisponnoNote' => 'SUM(nisponnonote)', 'totalONisponnoNote' => 'SUM(onisponnonote)',
                            'totalNisponno' => 'SUM(nisponno)', 'totalONisponno' => 'SUM(onisponno)',
                            'totalID' => 'count(id)', 'designation_id'
                        ];
                        if (!empty($all_designation)) {
                            $hasDataInPerformanceDesignation = $performance_designationTable->getUnitData([$unit_id], [$date, $date])->select($condition)->group(['unit_id'])->toArray();
                            if (!empty($hasDataInPerformanceDesignation)) {
                                $returnSummary['totalInbox'] = 0;
                                $returnSummary['totalOutbox'] = 0;
                                $returnSummary['totalNothijato'] = 0;
                                $returnSummary['totalNothivukto'] = 0;
                                $returnSummary['totalNisponnoDak'] = 0;
                                $returnSummary['totalONisponnoDak'] = 0;
                                $returnSummary['totalPotrojari'] = 0;
                                $returnSummary['totalSouddog'] = 0;
                                $returnSummary['totalDaksohoNote'] = 0;
                                $returnSummary['totalNisponnoNote'] = 0;
                                $returnSummary['totalNisponnoPotrojari'] = 0;
                                $returnSummary['totalONisponnoNote'] = 0;
                                $returnSummary['totalNisponno'] = 0;
                                $returnSummary['totalONisponno'] = 0;
                                foreach ($hasDataInPerformanceDesignation as $val_form_designation) {
                                    $returnSummary['totalInbox'] += $val_form_designation['totalInbox'];
                                    $returnSummary['totalOutbox'] += $val_form_designation['totalOutbox'];
                                    $returnSummary['totalNothijato'] += $val_form_designation['totalNothijato'];
                                    $returnSummary['totalNothivukto'] += $val_form_designation['totalNothivukto'];
                                    $returnSummary['totalNisponnoDak'] += $val_form_designation['totalNisponnoDak'];
//                              $returnSummary['totalONisponnoDak'] += TableRegistry::get('DakUsers')->getOnisponnoDak($unit_related_data['office_id'], $unit_id);
                                    $returnSummary['totalONisponnoDak'] += $val_form_designation['totalONisponnoDak'];
                                    $returnSummary['totalPotrojari'] += $val_form_designation['totalpotrojariall'];
                                    $returnSummary['totalSouddog'] += $val_form_designation['totalSouddog'];
                                    $returnSummary['totalDaksohoNote'] += $val_form_designation['totalDaksohoNote'];
                                    $returnSummary['totalNisponnoNote'] += $val_form_designation['totalNisponnoNote'];
                                    $returnSummary['totalNisponnoPotrojari'] += $val_form_designation['totalNisponnoPotrojari'];
//                              $returnSummary['totalONisponnoNote'] += TableRegistry::get('NothiMasterCurrentUsers')->getAllNothiPartNo($unit_related_data['office_id'], $unit_id,0, [$date,$date])->count();
                                    $returnSummary['totalONisponnoNote'] += $val_form_designation['totalONisponnoNote'];
                                    $returnSummary['totalNisponno'] += $val_form_designation['totalNisponno'];
                                    $returnSummary['totalONisponno'] += $val_form_designation['totalONisponno'];
                                }
                            } else {
                                $from_designation_report = false;
                            }
                        } else {
                            $from_designation_report = false;
                        }

                        if ($from_designation_report == false) {
                            $returnSummary = $reportsTable->newPerformanceReport($unit_related_data['office_id'],
                                $unit_id, 0, ['date_start' => $date, 'date_end' => $date]);
                        }

                        $performance_units_entity[$unit_id] = [];

                        $performance_units_entity[$unit_id]['inbox'] = $returnSummary['totalInbox'];
                        $performance_units_entity[$unit_id]['outbox'] = $returnSummary['totalOutbox'];
                        $performance_units_entity[$unit_id]['nothijat'] = $returnSummary['totalNothijato'];
                        $performance_units_entity[$unit_id]['nothivukto'] = $returnSummary['totalNothivukto'];
                        $performance_units_entity[$unit_id]['nisponnodak'] = $returnSummary['totalNisponnoDak'];
                        $performance_units_entity[$unit_id]['onisponnodak'] = $returnSummary['totalONisponnoDak'];

                        $performance_units_entity->potrojari = $returnSummary['totalPotrojari'];
                        if ($returnSummary['totalSouddog'] < 0) {
                            $returnSummary['totalSouddog'] = 0;
                        }
                        $performance_units_entity[$unit_id]['selfnote']  = $returnSummary['totalSouddog'];
                        $performance_units_entity[$unit_id]['daksohonote']  = $returnSummary['totalDaksohoNote'];
                        $performance_units_entity[$unit_id]['nisponnopotrojari']  = $returnSummary['totalNisponnoPotrojari'];
                        $performance_units_entity[$unit_id]['nisponnonote']  = $returnSummary['totalNisponnoNote'];
                        $performance_units_entity[$unit_id]['onisponnonote']  = $returnSummary['totalONisponnoNote'];

                        $performance_units_entity[$unit_id]['nisponno']  = $returnSummary['totalNisponno'];
                        $performance_units_entity[$unit_id]['onisponno']  = $returnSummary['totalONisponno'];

                        $performance_units_entity[$unit_id]['office_id']  = $unit_related_data['office_id'];
                        $performance_units_entity[$unit_id]['unit_id']  = $unit_id;
                        $performance_units_entity[$unit_id]['ministry_id']  = $unit_related_data['office_ministry_id'];
                        $performance_units_entity[$unit_id]['layer_id']  = $unit_related_data['office_layer_id'];
                        $performance_units_entity[$unit_id]['origin_id']  = $unit_related_data['office_origin_unit_id'];
                        $performance_units_entity[$unit_id]['unit_name']  = $unit_name;

                        if (!($office_info = Cache::read('office_info_' . $unit_related_data['office_id']))) {
                            $office_info = $OfficesTable->getBanglaName($unit_related_data['office_id']);
                            Cache::write('office_info_' . $unit_related_data['office_id'], $office_info);
                        }

                        $performance_units_entity[$unit_id]['office_name']  = $office_info['office_name_bng'];

                        if (!($ministry_info = Cache::read('ministry_info_' . $unit_related_data['office_ministry_id']))) {
                            $ministry_info = $MinistriesTable->getBanglaName($unit_related_data['office_ministry_id']);
                            Cache::write('ministry_info_' . $unit_related_data['office_ministry_id'], $ministry_info);
                        }

                        $performance_units_entity[$unit_id]['ministry_name']  = $ministry_info['name_bng'];

                        if (!($layer_info = Cache::read('layer_info_' . $unit_related_data['office_layer_id']))) {
                            $layer_info = $LayersTable->getBanglaName($unit_related_data['office_layer_id']);
                            Cache::write('layer_info_' . $unit_related_data['office_layer_id'], $layer_info);
                        }

                        $performance_units_entity[$unit_id]['layer_name']  = $layer_info['layer_name_bng'];

                        if (!($origin_info = Cache::read('origin_info_' . $unit_related_data['office_origin_unit_id']))) {
                            $origin_info = $UnitOriginsTable->getBanglaName($unit_related_data['office_origin_unit_id']);
                            Cache::write('origin_info_' . $unit_related_data['office_origin_unit_id'], $origin_info);
                        }

                        $performance_units_entity[$unit_id]['origin_name'] = $origin_info['unit_name_bng'];

                        $performance_units_entity[$unit_id]['status'] = 1;
                        $performance_units_entity[$unit_id]['record_date']  = $date;

//                        $performanceUnitTable->save($performance_units_entity);
                    }
//                    $this->updateIndividualUnit($unit_id);
//                    $this->deleteMultipleRowsOfUnits($unit_id, !empty($period[0]) ? $period[0] : '');
                    print_r($performance_units_entity);
                } catch (\Exception $ex) {
                    echo $ex->getMessage();
                }
            }
        }
        echo '<br>Unit Done For Office: ' . $office_id;
        die;
//         $this->runCronRequest();
    }

    public function divisionWiseOfficeReportFromSuperman() {
        $this->layout = null;
        $selected_office_section = $this->getCurrentDakSection();
        //if(strpos($selected_office_section['office_name'], 'জেলা প্রশাসকের কার্যালয়') !== false) {
        $OfficesTable = TableRegistry::get('Offices');

        $geo_division_list = TableRegistry::get('GeoDivisions')->find('list')->where(['status' => 1])->toArray();
        //$geo_district_list = TableRegistry::get('GeoDistricts')->find('list')->where(['status' => 1])->toArray();

        $this->set(compact('geo_division_list','ChildOffices'));
    }
    public function divisionWiseOfficeListByGeoDistrictId() {
        $districtLevelOffices = TableRegistry::get('Offices')->find('list'); //->where(['office_layer_id IN' => array_keys($districtOfficeLayers)]);
        if (isset($this->request->data['geo_upazila_id']) && $this->request->data['geo_upazila_id'] != 0) {
            $districtLevelOffices = $districtLevelOffices->where(['geo_upazila_id' => $this->request->data['geo_upazila_id']])->toArray();
        } elseif (isset($this->request->data['geo_district_id']) && $this->request->data['geo_district_id'] != 0) {
            $districtLevelOffices = $districtLevelOffices->where(['geo_district_id' => $this->request->data['geo_district_id']])->toArray();
        } elseif (isset($this->request->data['geo_division_id']) && $this->request->data['geo_division_id'] != 0) {
            $districtLevelOffices = $districtLevelOffices->where(['geo_division_id' => $this->request->data['geo_division_id']])->toArray();
        } else {
            $districtLevelOffices = [];
        }


        $this->response->type('application/json');
        $this->response->body(json_encode($districtLevelOffices));
        return $this->response;
    }

}
