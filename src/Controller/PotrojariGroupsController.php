<?php

namespace App\Controller;

use App\Controller\ProjapotiController;
use Cake\Datasource\ConnectionManager;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\TableRegistry;

class PotrojariGroupsController extends ProjapotiController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['ajaxGetGroupList']);
    }

    public function ajaxGetGroupList($office_id = 0)
    {

        if (empty($office_id)) {
            $user_info = $this->getCurrentDakSection();
            $office_id = $user_info['office_id'];
        }
        $search_key = !empty($this->request->query['search_key']) ? $this->request->query['search_key']
            : '';
        $table = TableRegistry::get('PotrojariGroups');
        $valueList = [];

        $groups = $table->getGroupList(['privacy_type' => 'private', 'creator_office_id' => $office_id],
            false)->orWhere(['privacy_type' => 'public'])->where(['group_name LIKE' => "%{$search_key}%"])->toArray();

        if (!empty($groups)) {
            foreach ($groups as $key => $value) {
                $row = array();
                $row['id'] = $value['id'];
                $row['value'] = $value['group_name'];
                $row['list'] = $value['group_value'];
                $valueList[] = $row;
            }
        }

        if (count($valueList) == 0) {
            $row = array();
            $row['id'] = 0;
            $row['value'] = "--কোনো তথ্য খুঁজে পাওয়া যায় নি--";
            $row['list'] = json_encode([]);
            $valueList[] = $row;
        }

        $this->response->body(json_encode($valueList));
        $this->response->type('json');
        return $this->response;
    }

    public function groupMail()
    {
        $page = isset($this->request->query['page']) ? $this->request->query['page'] : 1;
        if ($this->Cookie->check('PotrojariGroup_PageLimit')) {
            $limit = $this->Cookie->read('PotrojariGroup_PageLimit');
        } else {
            $this->Cookie->write('PotrojariGroup_PageLimit', 5);
            $limit = $this->Cookie->read('PotrojariGroup_PageLimit');
        }
        if (isset($this->request->query['limit'])) {
            $this->Cookie->write('PotrojariGroup_PageLimit', $this->request->query['limit']);
            $limit = $this->request->query['limit'];
        }
        $this->set(compact('limit'));
        if (isset($this->request->query['search'])) {
			$search = $this->request->query['search'];
		} else {
			$search = '';
		}
		$this->set('search', $search);
		$user_info = $this->getCurrentDakSection();
        $potrojari_groups_table = TableRegistry::get('PotrojariGroups');
        $potrojari_groups_users_table = TableRegistry::get('PotrojariGroupsUsers');
        $employee_table = TableRegistry::get('EmployeeRecords');
        $organogram_table = TableRegistry::get('OfficeUnitOrganograms');
        $unit_table = TableRegistry::get('OfficeUnits');
        if (empty($user_info)) {
            $user_info['office_id'] = $user_info['office_unit_id']
                = $user_info['office_unit_organogram_id'] = $user_info['officer_id']
                = 0;
            $all_created_groups = $potrojari_groups_table->getGroups(0, 0, 0, 0, 'public')->where(['group_name like' => '%'.$search.'%'])->order(['creator_office_unit_organogram_id asc']);
        } else {
            $all_created_groups = $potrojari_groups_table->getGroups($user_info['office_id'])->where(['group_name like' => '%'.$search.'%'])->order(['creator_office_unit_organogram_id desc']);
        }

//        pr($this->paginate($all_created_groups)); die;
        try {
            $all_created_groups = $this->Paginator->paginate($all_created_groups, ['limit' => $limit]);
            $group_id = [];
            $creator_employee_id = [];
            $creator_office_unit_organogram_id = [];
            $creator_unit_id = [];
            foreach ($all_created_groups as $all_created_group) {
                $group_id[] = $all_created_group['id'];
                $creator_employee_id[] = $all_created_group['creator_employee_id'];
                $creator_office_unit_organogram_id[] = $all_created_group['creator_office_unit_organogram_id'];
                $creator_unit_id[] = $all_created_group['creator_unit_id'];

            }
            $group_users = $potrojari_groups_users_table->find()->where(['group_id IN' => $group_id])->toArray();

            $creator_employee_info = $employee_table->find('list', [
                'keyField' => 'id',
                'valueField' => 'name_bng'
            ])->where(['id IN' => array_unique($creator_employee_id)])->toArray();

            $creator_office_unit_organogram_info = $organogram_table->find('list', [
                'keyField' => 'id',
                'valueField' => 'designation_bng'
            ])->where(['id IN' => array_unique($creator_office_unit_organogram_id)])->toArray();

            $creator_unit_info = $unit_table->find('list', [
                'keyField' => 'id',
                'valueField' => 'unit_name_bng'
            ])->where(['id IN' => array_unique($creator_unit_id)])->toArray();

            $this->set(compact('creator_employee_info', $creator_employee_info));
            $this->set(compact('creator_office_unit_organogram_info', $creator_office_unit_organogram_info));
            $this->set(compact('creator_unit_info', $creator_unit_info));

        } catch (NotFoundException $e) {
            return $this->redirect(['action' => 'groupMail']);
        }
//        $page = 1;
//        if (isset($this->request->query['page'])) {
//            if (($this->request->query['page'] == 1)) {
//                $page = 1;
//            } else {
//                $page = (intval($this->request->query['page'])) * 10 + $page;
//            }
//        }
        $this->set(compact('all_created_groups'));
        $this->set(compact('group_users'));
        $this->set(compact('user_info'));
        $this->set(compact('page'));
    }

    public function groupMailAdd($id=0)
    {
        if ($this->request->is('post')) {
            $user_info = $this->getCurrentDakSection();
            if (empty($user_info)) {
                $user_info['office_id'] = $user_info['office_unit_id']
                    = $user_info['office_unit_organogram_id'] = $user_info['officer_id']
                    = 0;
            }
            $potrojari_groups_table = TableRegistry::get('PotrojariGroups');
            $potrojari_groups_users_table = TableRegistry::get('PotrojariGroupsUsers');
            $permitteds = $this->request->data['permitted'];
            if(empty($permitteds)){
                $this->response->type('application/json');
                $this->response->body(json_encode('error'));
                return $this->response;
            }
            $saveDesignationTillNow = [];

            if(!empty($id)){
                $potrojari_entity = $potrojari_groups_table->get($id);
                if(empty($potrojari_entity)){
                    $this->response->type('application/json');
                    $this->response->body(json_encode('error'));
                    return $this->response;
                }
                $potrojari_entity->group_name = $this->request->data['group_name'];
                $potrojari_entity->total_users = count($permitteds);

            } else {

                $potrojari_entity = $potrojari_groups_table->newEntity();
                if ($user_info['office_id'] == 0) {
                    $potrojari_entity->privacy_type = 'public';
                } else {
                    $potrojari_entity->privacy_type = 'private';
                }
                $potrojari_entity->group_name = $this->request->data['group_name'];
                $potrojari_entity->group_value = '';
                $potrojari_entity->creator_employee_id = $user_info['officer_id'];
                $potrojari_entity->creator_office_unit_organogram_id = $user_info['office_unit_organogram_id'];
                $potrojari_entity->creator_unit_id = $user_info['office_unit_id'];
                $potrojari_entity->creator_office_id = $user_info['office_id'];
                $potrojari_entity->total_users = count($permitteds);
            }
            $msg = 'error';
            // sometimes multiple request can come for single user. need to filter it. Total user can be less after filter
            $total_record_saved = 0;
            if ($potrojari_id = $potrojari_groups_table->save($potrojari_entity)) {
                if(!empty($id)){
                    $potrojari_groups_users_table->deleteAll(['group_id'=>$potrojari_id->id]);
                }
                foreach ($permitteds as $permitted) {
                    $permitted = json_decode($permitted,true);
                    if (in_array($permitted['office_unit_organogram_id'], $saveDesignationTillNow)) {
                        //duplicate
                        continue;
                    }
                    $potrojari_user_entity = $potrojari_groups_users_table->newEntity();
                    $potrojari_user_entity->group_id = $potrojari_id->id;
                    $potrojari_user_entity->privacy_type = $potrojari_id->privacy_type;
                    $potrojari_user_entity->group_name = $potrojari_id->group_name;
                    $potrojari_user_entity->creator_employee_id = $potrojari_id->creator_employee_id;
                    $potrojari_user_entity->creator_office_unit_organogram_id = $potrojari_id->creator_office_unit_organogram_id;
                    $potrojari_user_entity->creator_unit_id = $potrojari_id->creator_unit_id;
                    $potrojari_user_entity->creator_office_id = $potrojari_id->creator_office_id;
                    $potrojari_user_entity->created = $potrojari_id->created;
                    $potrojari_user_entity->modified = $potrojari_id->modified;
                    $potrojari_user_entity->office_id = $permitted['office_id'];
                    $potrojari_user_entity->office_name_eng = $permitted['office_name_eng'];
                    $potrojari_user_entity->office_name_bng = $permitted['office_name'];
                    $potrojari_user_entity->office_unit_id = $permitted['office_unit_id'];
                    $potrojari_user_entity->office_unit_name_eng = $permitted['office_unit_name_eng'];
                    $potrojari_user_entity->office_unit_name_bng = $permitted['office_unit_name'];
                    $potrojari_user_entity->office_unit_organogram_id = $permitted['office_unit_organogram_id'];
                    $potrojari_user_entity->office_unit_organogram_name_eng = $permitted['office_unit_organogram_name_eng'];
                    $potrojari_user_entity->office_unit_organogram_name_bng = $permitted['office_unit_organogram_name'];
                    $potrojari_user_entity->employee_id = $permitted['employee_id'];
                    $potrojari_user_entity->employee_name_eng = $permitted['employee_name_eng'];
                    $potrojari_user_entity->employee_name_bng = $permitted['employee_name'];
                    $potrojari_user_entity->officer_email = $permitted['employee_personal_email'];
                    $potrojari_user_entity->officer_mobile = empty($permitted['employee_personal_mobile']) ? '' : $permitted['employee_personal_mobile'];
                    $potrojari_user_entity->office_head = $permitted['office_head'];
                    if($potrojari_groups_users_table->save($potrojari_user_entity)){
                        $total_record_saved++;
                        if(!empty($permitted['office_unit_organogram_id'])){
                            $saveDesignationTillNow[] = $permitted['office_unit_organogram_id'];
                        }
                    }
                }

                $msg = 'success';
            }
            // now update total user column - after filter total user can be less
            $potrojari_groups_table->updateAll(['total_users' => $total_record_saved],['id' => $potrojari_id->id]);
            $this->response->type('application/json');
            $this->response->body(json_encode($msg));
            return $this->response;
        } else {
            $user_info = $this->getCurrentDakSection();
            $this->set(compact('user_info'));
            $this->render('potrojari_mail_add');
        }
    }

    public function groupMailEdit($id = 0)
    {
        if (empty($id)) {
            return $this->redirect(['controller' => 'users', 'action' => 'login']);
        }
        $user = $this->Auth->user();
        $user_info = $this->getCurrentDakSection();
        $potrojari_groups_table = TableRegistry::get('PotrojariGroups');
        $potrojari_groups_users_table = TableRegistry::get('PotrojariGroupsUsers');
        $potrojari_groups_info = $potrojari_groups_table->get($id);
        if (($user_info['office_id'] == $potrojari_groups_info['creator_office_id']
                && $user_info['office_unit_id'] == $potrojari_groups_info['creator_unit_id']
                && $user_info['office_unit_organogram_id'] == $potrojari_groups_info['creator_office_unit_organogram_id']) || ($user_info['is_admin'] == 1 && $user_info['office_id'] == $potrojari_groups_info['creator_office_id']) || ($user['user_role_id'] == 1)) {

            $potrojari_groups_users_info = $potrojari_groups_users_table->find()->where(['group_id' => $id])->toArray();
            $this->set(compact('potrojari_groups_info'));
            $this->set(compact('potrojari_groups_users_info'));
            $this->set(compact('id'));
        }
        else {
            return $this->redirect(['controller' => 'users', 'action' => 'login']);
        }
        $this->set('user_info', $user_info);
        $this->set('id', $id);
        $this->render('potrojari_mail_edit');
    }

    public function groupMailSave()
    {
        if ($this->request->is('post')) {
            try {
                $user_info = $this->getCurrentDakSection();
                if (empty($user_info)) {
                    $user_info['office_id'] = $user_info['office_unit_id']
                        = $user_info['office_unit_organogram_id'] = $user_info['officer_id']
                        = 0;
                }
                $potrojari_groups_table = TableRegistry::get('PotrojariGroups');
                $potrojari_groups_users_table = TableRegistry::get('PotrojariGroupsUsers');
                $permitted = $this->request->data['permitted'];
                if (empty($permitted)) {
                    $this->response->type('application/json');
                    $this->response->body(json_encode('error'));
                    return $this->response;
                }
                //check Duplicate
                if (!empty($permitted['office_unit_organogram_id'])) {
                    $has_existed = $potrojari_groups_users_table->find()->where(['office_unit_organogram_id' => $permitted['office_unit_organogram_id'], 'group_id' => $this->request->data['id']])->count();
                    if ($has_existed > 0) {
                        //already existed. rare case.
                        $msg = 'success';
                        goto rtn;
                    }
                }
                $msg = 'error';
                $potrojari_entity = $potrojari_groups_table->get($this->request->data['id']);
                if (($user_info['office_id'] == $potrojari_entity['creator_office_id']
                        && $user_info['office_unit_id'] == $potrojari_entity['creator_unit_id']
                        && $user_info['office_unit_organogram_id'] == $potrojari_entity['creator_office_unit_organogram_id']) || ($user_info['is_admin'] == 1 && $user_info['office_id'] == $potrojari_entity['creator_office_id'])) {
                    if ($user_info['office_id'] == 0) {
                        $potrojari_entity->privacy_type = 'public';
                    } else {
                        $potrojari_entity->privacy_type = 'private';
                    }
                    $potrojari_entity->group_name = $this->request->data['group_name'];
                    $potrojari_entity->group_value = '';
                    $potrojari_entity->creator_employee_id = $user_info['officer_id'];
                    $potrojari_entity->creator_office_unit_organogram_id = $user_info['office_unit_organogram_id'];
                    $potrojari_entity->creator_unit_id = $user_info['office_unit_id'];
                    $potrojari_entity->creator_office_id = $user_info['office_id'];
                    $potrojari_entity->total_users = $potrojari_entity->total_users + 1;


                    if ($potrojari_groups_table->save($potrojari_entity)) {
                        $potrojari_user_entity = $potrojari_groups_users_table->newEntity();
                        $potrojari_user_entity->group_id = $potrojari_entity->id;
                        $potrojari_user_entity->privacy_type = $potrojari_entity->privacy_type;
                        $potrojari_user_entity->group_name = $potrojari_entity->group_name;
                        $potrojari_user_entity->creator_employee_id = $potrojari_entity->creator_employee_id;
                        $potrojari_user_entity->creator_office_unit_organogram_id = $potrojari_entity->creator_office_unit_organogram_id;
                        $potrojari_user_entity->creator_unit_id = $potrojari_entity->creator_unit_id;
                        $potrojari_user_entity->creator_office_id = $potrojari_entity->creator_office_id;
                        $potrojari_user_entity->created = $potrojari_entity->created;
                        $potrojari_user_entity->office_id = $permitted['office_id'];
                        $potrojari_user_entity->office_name_eng = $permitted['office_name_eng'];
                        $potrojari_user_entity->office_name_bng = $permitted['office_name'];
                        $potrojari_user_entity->office_unit_id = $permitted['office_unit_id'];
                        $potrojari_user_entity->office_unit_name_eng = $permitted['office_unit_name_eng'];
                        $potrojari_user_entity->office_unit_name_bng = $permitted['office_unit_name'];
                        $potrojari_user_entity->office_unit_organogram_id = $permitted['office_unit_organogram_id'];
                        $potrojari_user_entity->office_unit_organogram_name_eng = $permitted['office_unit_organogram_name_eng'];
                        $potrojari_user_entity->office_unit_organogram_name_bng = $permitted['office_unit_organogram_name'];
                        $potrojari_user_entity->employee_id = $permitted['employee_id'];
                        $potrojari_user_entity->employee_name_eng = $permitted['employee_name_eng'];
                        $potrojari_user_entity->employee_name_bng = $permitted['employee_name'];
                        $potrojari_user_entity->officer_email = isset($permitted['employee_personal_email']) ? $permitted['employee_personal_email'] : '';
                        $potrojari_user_entity->officer_mobile = isset($permitted['employee_personal_mobile']) ? $permitted['employee_personal_mobile'] : '';
                        $potrojari_user_entity->office_head = $permitted['office_head'];
                        $potrojari_groups_users_table->save($potrojari_user_entity);

                        $msg = 'success';
                    }
                }
            } catch (\Exception $ex) {
                $msg = 'error';
            }
            rtn:
            $this->response->type('application/json');
            $this->response->body(json_encode($msg));
            return $this->response;
        } else {
            $user_info = $this->getCurrentDakSection();
            $this->set(compact('user_info'));
            $this->render('potrojari_mail_add');
        }


    }

    public function groupMailDelete()
    {
        if ($this->request->is('post')) {
            $user_info = $this->getCurrentDakSection();
            if (empty($user_info)) {
                $user_info['office_id'] = $user_info['office_unit_id']
                    = $user_info['office_unit_organogram_id'] = $user_info['officer_id']
                    = 0;
            }
            $msg = 'error';
            $potrojari_groups_table = TableRegistry::get('PotrojariGroups');
            $potrojari_groups_users_table = TableRegistry::get('PotrojariGroupsUsers');
            $potrojari_entity = $potrojari_groups_table->get($this->request->data['id']);
            if (($user_info['office_id'] == $potrojari_entity['creator_office_id']
                    && $user_info['office_unit_id'] == $potrojari_entity['creator_unit_id']
                    && $user_info['office_unit_organogram_id'] == $potrojari_entity['creator_office_unit_organogram_id']) || ($user_info['is_admin'] == 1 && $user_info['office_id'] == $potrojari_entity['creator_office_id'])) {
                if ($potrojari_groups_table->delete($potrojari_entity)) {
                    $potrojari_groups_users_table->deleteAll(['group_id' => $this->request->data['id']]);
                    $msg = 'success';
                }
            }
            $this->response->type('application/json');
            $this->response->body(json_encode($msg));
            return $this->response;
        }
    }

    public function resetGroups($id = 0)
    {
        if ($this->request->is('post')) {
            $user_info = $this->getCurrentDakSection();
            if (empty($user_info)) {
                $user_info['office_id'] = $user_info['office_unit_id']
                    = $user_info['office_unit_organogram_id'] = $user_info['officer_id']
                    = 0;
            }
            $msg = ['status' => 'error'];
            $potrojari_groups_table = TableRegistry::get('PotrojariGroups');
            $potrojari_groups_users_table = TableRegistry::get('PotrojariGroupsUsers');
            $potrojari_entity = $potrojari_groups_table->get($id);
            if (($user_info['office_id'] == $potrojari_entity['creator_office_id']
                    && $user_info['office_unit_id'] == $potrojari_entity['creator_unit_id']
                    && $user_info['office_unit_organogram_id'] == $potrojari_entity['creator_office_unit_organogram_id']) || ($user_info['is_admin'] == 1 && $user_info['office_id'] == $potrojari_entity['creator_office_id'])) {

                $potrojari_entity->total_users = 0;
                $potrojari_groups_table->save($potrojari_entity);
                $potrojari_groups_users_table->deleteAll(['group_id' => $id]);
                $msg = ['status' => 'success'];

            }
            $this->response->type('application/json');
            $this->response->body(json_encode($msg));
            return $this->response;
        }
    }

    public function uploadCsv($employeeOffice = 4714)
    {
        $this->switchOffice($employeeOffice,'MigrateOffice');
        TableRegistry::remove('PotrojariReceiver');
        TableRegistry::remove('PotrojariOnulipi');
        $potrojariReceiver = TableRegistry::get('PotrojariReceiver');
        $potrojariOnulipi = TableRegistry::get('PotrojariOnulipi');

        $allreceiver = $potrojariReceiver->find()
            ->select([
                'potrojari_id', 'group_id', 'group_name',
                'receiving_office_name', 'group_member',
                'receiving_officer_designation_label', 'receiving_officer_name', 'receiving_officer_email'
            ])
            ->where(
                [
                    'group_id <>' => 0,
                    'receiving_officer_designation_id' => 0,
                    'receiving_officer_id' => 0
                ]
            )
            ->order(['potrojari_id'=>'DESC','group_id' => 'asc'])->toArray();

        $allOnulipi = $potrojariOnulipi->find()
            ->select([
                'potrojari_id', 'group_id', 'group_name',
                'receiving_office_name', 'group_member',
                'receiving_officer_designation_label', 'receiving_officer_name', 'receiving_officer_email'
            ])
            ->where(
                [
                    'group_id >' => 0,
                    'receiving_officer_designation_id' => 0,
                    'receiving_officer_id' => 0
                ]
            )
            ->order(['potrojari_id'=>'DESC','group_id' => 'asc'])->toArray();
        $PotrojariGroupsUsers = TableRegistry::get('PotrojariGroupsUsers');

        $usedGroups = [];
        $prevP = null;
        if (!empty($allreceiver)) {
            foreach ($allreceiver as $key => $cols) {

                if ($prevP != $cols['potrojari_id']) {
                    if (in_array($cols['group_id'], $usedGroups)) {
                        continue;
                    }
                }
                $prevP = $cols['potrojari_id'];
                $usedGroups[] = $cols['group_id'];

                $condition = '';
                $update = [];

                if (!empty($cols['group_id'])) {
                    $condition .= "group_id = " . $cols['group_id'];
                    $condition .= " AND creator_office_id = " .$employeeOffice;
                }

                if (!empty($cols['receiving_office_name'])) {
                    $condition .= " AND (office_name_bng = '" . $cols['receiving_office_name'] . "' OR office_name_eng = '" . $cols['receiving_office_name'] . "')";
                }

                if (!empty($cols['receiving_officer_designation_label'])) {
                    $condition .= " AND (office_unit_organogram_name_bng = '" . $cols['receiving_officer_designation_label'] . "' OR office_unit_organogram_name_bng = '" . $cols['receiving_officer_designation_label'] . "')";
                }


                if (!empty($cols['receiving_officer_name'])) {
                    $update['employee_name_bng'] = $cols['receiving_officer_name'];
                }

                if (!empty($cols['receiving_officer_email'])) {
                    $update['officer_email'] = $cols['receiving_officer_email'];
                }

                $has = $PotrojariGroupsUsers->find()->where($condition)->count();

                if ($has > 1) {
                    continue;
                }
                $PotrojariGroupsUsers->updateAll($update, $condition);
            }
        }

        if (!empty($allOnulipi)) {
            foreach ($allOnulipi as $key => $cols) {

                if ($prevP != $cols['potrojari_id']) {
                    if (in_array($cols['group_id'], $usedGroups)) {
                        continue;
                    }
                }
                $prevP = $cols['potrojari_id'];
                $usedGroups[] = $cols['group_id'];

                $condition = '';
                $update = [];

                if (!empty($cols['group_id'])) {
                    $condition .= "group_id = " . $cols['group_id'];
                    $condition .= " AND creator_office_id = " .$employeeOffice;
                }

                if (!empty($cols['receiving_office_name'])) {
                    $condition .= " AND (office_name_bng = '" . $cols['receiving_office_name'] . "' OR office_name_eng = '" . $cols['receiving_office_name'] . "')";
                }

                if (!empty($cols['receiving_officer_designation_label'])) {
                    $condition .= " AND (office_unit_organogram_name_bng = '" . $cols['receiving_officer_designation_label'] . "' OR office_unit_organogram_name_bng = '" . $cols['receiving_officer_designation_label'] . "')";
                }


                if (!empty($cols['receiving_officer_name'])) {
                    $update['employee_name_bng'] = $cols['receiving_officer_name'];
                }

                if (!empty($cols['receiving_officer_email'])) {
                    $update['officer_email'] = $cols['receiving_officer_email'];
                }

                $has = $PotrojariGroupsUsers->find()->where($condition)->count();


                if ($has > 1) {
                    continue;
                }
                $PotrojariGroupsUsers->updateAll($update, $condition);
            }
        }

    }
    public function superAdminOfficePotrojariGroupComparison(){
        if($this->request->is('get')){
            $potrojari_groups_by_super_admin = TableRegistry::get('PotrojariGroups')->find('list')->where(['creator_employee_id' => 0])->toArray();
            $this->set(compact('potrojari_groups_by_super_admin'));
        }

    }
    public function superAdminGetPJGroupUsers(){
        $office_id = isset($this->request->data['office_id'])?$this->request->data['office_id']:0;
        $potrojari_group_id = isset($this->request->data['potrojari_group_id'])?$this->request->data['potrojari_group_id']:0;

        $response = ['status' => 'error', 'message' => 'Something went wrong'];
        try{
            $conditions = [];
            if(empty($office_id) || empty($potrojari_group_id)){
                $response['message'] = 'প্রয়োজনীয় তথ্য দেওয়া হয়নি।';
                goto rtn;
            }else{
                $conditions['PotrojariGroupsUsers.office_id'] = $office_id;
                $conditions['PotrojariGroupsUsers.group_id'] = $potrojari_group_id;
            }

            $potrojariGroupUsersTable = TableRegistry::get('PotrojariGroupsUsers');
            $allUser = $potrojariGroupUsersTable->getAll(['employee_name_bng', 'office_name_bng','office_unit_name_bng','office_unit_organogram_name_bng'],$conditions)->toArray();

            $response = ['status' => 'success', 'data' => $allUser];

        }catch(\Exception $ex){
            $response['message'] = 'টেকনিক্যাল ত্রুটি হয়েছে।';
            $response['reason'] = $this->makeEncryptedData($ex->getMessage());
        }


        rtn:
        $this->response->type('application/json');
        $this->response->body(json_encode($response));
        return $this->response;
    }
    public function getPJGroupMappedUserOfOffice(){
        $this->layout = null;
        $this->view ='get_pj_group_mapped_user_of_office';
        $office_id = isset($this->request->data['office_id'])?$this->request->data['office_id']:0;
        $potrojari_group_id = isset($this->request->data['potrojari_group_id'])?$this->request->data['potrojari_group_id']:0;

        try{
            $conditions = [];
            if(empty($office_id) || empty($potrojari_group_id)){
                die('প্রয়োজনীয় তথ্য দেওয়া হয়নি।');
            }else{
                $conditions['PotrojariGroupsUsers.office_id'] = $office_id;
                $conditions['PotrojariGroupsUsers.group_id'] = $potrojari_group_id;
            }

            $potrojariGroupUsersTable = TableRegistry::get('PotrojariGroupsUsers');
            $allUserMappedInPJGroup= $potrojariGroupUsersTable->getAll(['office_unit_organogram_id'],$conditions)->toArray();
            $allMappedUserInGroup = [];
            foreach ($allUserMappedInPJGroup as $key => $value) {
                $allMappedUserInGroup[] = $value['office_unit_organogram_id'];
            }
            $allUserOfThisOffice = TableRegistry::get('EmployeeOffices')->getEmployeeOfficeRecordsByOfficeId($office_id,0,0,1,['EmployeeOffices.status', 'EmployeeOffices.office_unit_organogram_id','name_bng' => 'EmployeeRecords.name_bng','unit_name_bng' =>'OfficeUnits.unit_name_bng','designation' => 'OfficeUnitOrganograms.designation_bng']);
            $allUserOfThisOfficeUnassigned = TableRegistry::get('EmployeeOffices')->getEmployeeOfficeRecordsByOfficeId($office_id,0,0,0,['EmployeeOffices.status', 'EmployeeOffices.office_unit_organogram_id','name_bng' => 'EmployeeRecords.name_bng','unit_name_bng' =>'OfficeUnits.unit_name_bng','designation' => 'OfficeUnitOrganograms.designation_bng']);
            $assigned_organogram_id = array_map(function($e) {
                return is_object($e) ? $e->office_unit_organogram_id : $e['office_unit_organogram_id'];
            }, $allUserOfThisOffice);
            //pr($assigned_organogram_id);die;
            foreach ($allUserOfThisOfficeUnassigned as $key => $value) {
                if (in_array($value['office_unit_organogram_id'], $assigned_organogram_id)) {
                    continue;
                }
                $allUserOfThisOffice[] = [
                    'office_unit_organogram_id' => $value['office_unit_organogram_id'],
                    'name_bng' => '(শুন্যপদ)',
                    'unit_name_bng' => $value['unit_name_bng'],
                    'designation' => $value['designation'],
                ];
            }
            $this->set(compact('allMappedUserInGroup','allUserOfThisOffice'));
        }catch(\Exception $ex){
            die('টেকনিক্যাল ত্রুটি হয়েছে। '.$this->makeEncryptedData($ex->getMessage()));
        }
    }

    public function updatePotrojariGroup() {
        $officeId = $this->request->data['officeId'];
        if (!isset($this->request->data['organogramIds'])) {
            $organogramIds = [];
        } else {
            $organogramIds = $this->request->data['organogramIds'];
        }
        $potrojariGroupId = $this->request->data['potrojariGroupId'];

        $potrojariGroupTable = TableRegistry::get('PotrojariGroups');
        $potrojariGroup = $potrojariGroupTable->get($potrojariGroupId);
        if (!$potrojariGroup) {
            $response = [
                'status' => 'error',
                'msg' => 'পত্রজারি গ্রুপ পাওয়া যায় নি'
            ];
        }

        $connection = ConnectionManager::get('NothiAccessDb');
        $connection->begin();

        $potrojariGroupUserTable = TableRegistry::get('PotrojariGroupsUsers');
        $potrojariGroupUserTable->deleteAll(['group_id' => $potrojariGroupId, 'office_id' => $officeId]);

        $officeUnitOrganogramsTable = TableRegistry::get('OfficeUnitOrganograms');
        foreach ($organogramIds as $key => $organogram_id) {
            $officeUnitOrganograms = $officeUnitOrganogramsTable->get($organogram_id);

            $potrojari_user_entity = $potrojariGroupUserTable->newEntity();
            $potrojari_user_entity->group_id = $potrojariGroup->id;
            $potrojari_user_entity->privacy_type = $potrojariGroup->privacy_type;
            $potrojari_user_entity->group_name = $potrojariGroup->group_name;
            $potrojari_user_entity->creator_employee_id = $potrojariGroup->creator_employee_id;
            $potrojari_user_entity->creator_office_unit_organogram_id = $potrojariGroup->creator_office_unit_organogram_id;
            $potrojari_user_entity->creator_unit_id = $potrojariGroup->creator_unit_id;
            $potrojari_user_entity->creator_office_id = $potrojariGroup->creator_office_id;
            $potrojari_user_entity->created = $potrojariGroup->created;

            $office = TableRegistry::get('Offices')->get($officeId)->toArray();
            $potrojari_user_entity->office_id = $officeId;
            $potrojari_user_entity->office_name_eng = $office['office_name_eng'];
            $potrojari_user_entity->office_name_bng = $office['office_name_bng'];

            $office_unit = TableRegistry::get('OfficeUnits')->get($officeUnitOrganograms->office_unit_id)->toArray();
            $potrojari_user_entity->office_unit_id = $officeUnitOrganograms->office_unit_id;
            $potrojari_user_entity->office_unit_name_eng = $office_unit['unit_name_eng'];
            $potrojari_user_entity->office_unit_name_bng = $office_unit['unit_name_bng'];
            $potrojari_user_entity->office_unit_organogram_id = $officeUnitOrganograms['id'];
            $potrojari_user_entity->office_unit_organogram_name_eng = $officeUnitOrganograms['designation_eng'];
            $potrojari_user_entity->office_unit_organogram_name_bng = $officeUnitOrganograms['designation_bng'];

            $employee_office_info = TableRegistry::get('EmployeeOffices')->find()->where(['office_unit_organogram_id' => $officeUnitOrganograms->id, 'status' => 1])->first();
            if ($employee_office_info) {
                $employee_office_info = $employee_office_info->toArray();
                $employee_info = TableRegistry::get('EmployeeRecords')->get($employee_office_info['employee_record_id'])->toArray();
                $potrojari_user_entity->employee_id = $employee_info['id'];
                $potrojari_user_entity->employee_name_eng = $employee_info['name_eng'];
                $potrojari_user_entity->employee_name_bng = $employee_info['name_bng'];
                $potrojari_user_entity->officer_email = isset($employee_info['employee_personal_email']) ? $employee_info['employee_personal_email'] : '';
                $potrojari_user_entity->officer_mobile = isset($employee_info['employee_personal_mobile']) ? $employee_info['employee_personal_mobile'] : '';
            } else {
                $potrojari_user_entity->employee_id = 0;
                $potrojari_user_entity->employee_name_eng = 'Empty';
                $potrojari_user_entity->employee_name_bng = '(শুন্যপদ)';
                $potrojari_user_entity->officer_email = '';
                $potrojari_user_entity->officer_mobile = '';
            }

            if ($employee_office_info['office_head'] == 1) {
                $isShowSection = 1;
            } else {
                if ($employee_office_info['show_unit'] == 1) {
                    $isShowSection = 0;
                } else {
                    $isShowSection = 1;
                }
            }
            $potrojari_user_entity->office_head = $isShowSection;
            $potrojariGroupUserTable->save($potrojari_user_entity);
        }

        $potrojariGroup->total_users = $potrojariGroupUserTable->find()->where(['group_id' => $potrojariGroupId])->count();
        $potrojariGroupTable->save($potrojariGroup);

        $connection->commit();

        $response = [
            'status' => 'success',
            'msg' => 'পত্রজারি গ্রুপ সফলভাবে হালনাগাদ হয়েছে'
        ];

        $this->response->body(json_encode($response));
        $this->response->type('application/json');
        return $this->response;
    }

}