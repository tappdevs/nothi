<?php

namespace App\Controller;

use App\Controller\ProjapotiController;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;
use Cake\Cache\Cache;

class ReportSettingController extends ProjapotiController
{

    public function reportMarkDistribution(){
        $columns =[
            'dak_inbox','dak_outbox','dak_onisponno','dak_nothijat','dak_nothivukto','dak_nisponno',
            'nothi_self_note','nothi_dak_note','nothi_note_niponno',
            'nothi_potrojari_niponno','potrojari_nisponno_internal', 'potrojari_nisponno_external',
            'nothi_onisponno','nothi_nisponno','potrojari',
        ];
        $readOnly = ['dak_nisponno','nothi_potrojari_niponno'];
        $reportMarkingTable = TableRegistry::get('ReportMarking');
        if($this->request->is('get')){
            $data = $reportMarkingTable->getData(['id'],['status' => 1])->first();
            if(!empty($data)){
                $reportMarkingEntity = $reportMarkingTable->get($data['id']);
            }else{
                $reportMarkingEntity = $reportMarkingTable->newEntity();
            }
        }
        else{
            if(!empty($this->request->data)){
                $total = 0;
                $data =$this->request->data;
                if(isset($data['id'])){
//                    $old_id = $data['id'];
                    unset($data['id']);
                }
                $reportMarkingEntity = $reportMarkingTable->patchEntity($reportMarkingTable->newEntity(),$data);
                if($reportMarkingEntity->errors()){
                    $this->Flash->error(__('Unable to add your data.'));
                    goto rtn;
                }
                foreach($data as $col_name => $col_val){
                    if(in_array($col_name,$readOnly)){
                        continue;
                    }
                    $total +=$col_val;
                }
                if($total != 100){
                    $this->Flash->error('রিপোর্টের সব গুলো ফিল্ড মিলিয়ে মোট ১০০% মার্ক তৈরি করতে হবে ।');
                    goto rtn;
                }
                $reportMarkingEntity->created_by = $this->Auth->user()['id'];
                $reportMarkingEntity->status = 1;
                $data_to_save =$reportMarkingTable->save($reportMarkingEntity);
                if($data_to_save){
                    if(isset($data_to_save['id'])){
                        $reportMarkingTable->updateAll(['status' => 0],['id <>' => $data_to_save['id']]);
                    }
                    $this->Flash->success(__('Data successfully added'));
                    return $this->redirect($this->referer());
                }
            }
        }
        rtn:
        $this->set(compact('reportMarkingEntity','columns','readOnly'));
    }
    public function reportCategoryList(){
        try{
            $reportCategoryTable = TableRegistry::get('ReportCategory');
            $reportCategoryList = $reportCategoryTable->getData([],[],['serial asc'])->toArray();
            $this->set(compact('reportCategoryList'));
        }catch (\Exception $ex){
            $this->Flash->error(__('Technical error happen'));
        }
    }
    public function reportCategoryAdd($id = 0){
        $response = [
            'status' => 'error',
            'message' => 'Something went wrong'
        ];
        try{
            $reportCategoryTable = TableRegistry::get('ReportCategory');
            if($this->request->is('get')){
                $reportCategoryEntity = $reportCategoryTable->newEntity();
                if(!empty($id)){
                    $reportCategoryEntity = $reportCategoryTable->get($id);
                }
            }
            else{
                $data = $this->request->data;
                $edit_operation = false;
                if(empty($data['name'])){
                    $response['message'] = 'ক্যাটাগরি নাম দেওয়া হয়নি';
                    goto rtn;
                }
                $user_id = $this->Auth->user()['id'];
                if(isset($data['id'])){
                    $data['modified_by'] = $user_id;
                    $reportCategoryEntity = $reportCategoryTable->get($data['id']);
                    $data = $reportCategoryTable->patchEntity($reportCategoryEntity,$data);
                    $edit_operation = true;
                }else{
                    $data['created_by'] = $data['modified_by'] = $user_id;
                    $data = $reportCategoryTable->patchEntity($reportCategoryTable->newEntity(),$data);
                }


                if($data->errors()){
                    $response['message'] = __('Technical error happen');
                    goto rtn;
                }
                if($edit_operation && $data->created_by != $user_id){
                    $response['message'] = __('You are not permitted');
                    goto rtn;
                }
                if($reportCategoryTable->save($data)){
                    $response = [
                        'status' => 'success',
                        'message' =>  __('Data successfully added'),
                    ];
                }
            }

        }catch (\Exception $ex){
            $response = [
                'reason' => $this->makeEncryptedData($ex->getMessage()),
                'message' => __('Technical error happen'),
            ];
        }
        rtn:
        $this->response->type('json');
        $this->response->body(json_encode($response));
        return $this->response;
    }
    function reportCategoryDelete(){
        $response = [
            'status' => 'error',
            'message' => 'Something went wrong'
        ];
        try{
            $reportCategoryTable = TableRegistry::get('ReportCategory');
            if($this->request->is('post')){
                $data = $this->request->data;

                if(empty($data['id'])){
                    $response['message'] = __('Required info not given');
                    goto rtn;
                }
                $reportCategoryEntity = $reportCategoryTable->get($data['id']);
                $user_id = $this->Auth->user()['id'];
                if($reportCategoryEntity->created_by != $user_id){
                    $response['message'] = __('You are not permitted');
                    goto rtn;
                }
                $reportCategoryTable->deleteAll(['id' => $reportCategoryEntity->id]);
                $response = [
                    'status' => 'success',
                    'message' =>  __('Data successfully deleted'),
                ];
            }
        }catch (\Exception $ex){
            $response = [
                'reason' => $this->makeEncryptedData($ex->getMessage()),
                'message' => __('Technical error happen'),
            ];
        }

        rtn:
        $this->response->type('json');
        $this->response->body(json_encode($response));
        return $this->response;
    }
    public function reportCategoryOfficeMapping($id = 0){
        try{
            $reportCategoryTable = TableRegistry::get('ReportCategory');
            if(!empty($id)){
                $reportCategoryEntity = $reportCategoryTable->get($id);
                if(!empty($reportCategoryEntity->offices)){
                   $offices = jsonA($reportCategoryEntity->offices);
                   if(!empty($offices)){
                       $mappedOffices = TableRegistry::get('Offices')->find('list')->where(['id IN' => $offices])->toArray();
                       $this->set(compact('mappedOffices'));
                   }
                }
                $this->set(compact('reportCategoryEntity'));
            }
            $allCategory = $reportCategoryTable->find('list',['valueField'=>'name','keyField'=>'id'])->order(['serial asc'])->toArray();
            $layerLevels = jsonA(OFFICE_LAYER_TYPE);
            $options = [];
            if(!empty($layerLevels)){
                foreach($layerLevels as $level){
                    $options[$level] = __($level);
                }
            }
            $options['Other'] = __('Other');

            $allMinistry = TableRegistry::get('OfficeMinistries')->find('list',['valueField'=>'name_bng','keyField'=>'id'])->toArray();
            $this->set(compact('allCategory','options','allMinistry'));
        }catch (\Exception $ex){
            $this->Flash->error(__('Technical error happen'));
        }
    }
    public function mapCategoryWithOffices(){
        $response = [
            'status' => 'error',
            'message' => 'Something went wrong'
        ];
        try{
            if($this->request->is('post')){
                $data = $this->request->data;
                if(empty($data['category_id'])){
                    $response['message'] = 'ক্যাটাগরি নির্বাচন করা হয়নি';
                    goto rtn;
                }
                if(empty($data['offices_id'])){
                    $response['message'] = 'কোন অফিস নির্বাচন করা হয়নি';
                    goto rtn;
                }
                $office_id_array = explode('--',$data['offices_id']);
                if(empty($office_id_array)){
                    $response['message'] = 'কোন অফিস নির্বাচন করা হয়নি';
                    goto rtn;
                }
                $reportCategoryTable = TableRegistry::get('ReportCategory');
                $reportCategoryEntity = $reportCategoryTable->get($data['category_id']);
                $reportCategoryEntity->offices = json_encode($office_id_array);
                $user_id = $this->Auth->user()['id'];
                $reportCategoryEntity->modified_by = $user_id;

                $reportCategoryTable->save($reportCategoryEntity);
                $response = [
                    'status' => 'success',
                    'message' =>  __('Data successfully added'),
                ];
            }
        }catch (\Exception $ex){
            $response = [
                'reason' => $this->makeEncryptedData($ex->getMessage()),
                'message' => __('Technical error happen'),
            ];
        }

        rtn:
        $this->response->type('json');
        $this->response->body(json_encode($response));
        return $this->response;
    }
    function editCategorySerial(){
        $response = [
            'status' => 'error',
            'message' => 'Something went wrong'
        ];
        try{
            if($this->request->is('post')){
                $data = $this->request->data['data'];
                if(!empty($data)){
                    $reportCategoryTable = TableRegistry::get('ReportCategory');
                    foreach ($data as $item){
                        if(!empty($item)){
                            $ref = isset($item['ref'])?$this->getDecryptedData($item['ref']):0;
                            $val = isset($item['val'])?bnToen($item['val']):0;
                            if(!empty($ref) && !empty($val)){
                                $reportCategoryTable->updateAll(['serial' =>$val],['id' =>$ref]);
                            }
                        }
                    }
                    $response = [
                        'status' => 'success',
                        'message' =>  __('Data successfully updated'),
                    ];
                }
            }

        }catch (\Exception $ex){
            $response = [
                'reason' => $this->makeEncryptedData($ex->getMessage()),
                'message' => __('Technical error happen'),
            ];
        }
        rtn:
        $this->response->type('json');
        $this->response->body(json_encode($response));
        return $this->response;
    }
}