<?php

namespace App\Controller;

use App\Controller\ProjapotiController;
use App\Model\SSO\AppLoginRequest;
use App\Model\SSO\AppLoginResponse;
use App\Model\SSO\AppLogoutRequest;
use App\Model\SSO\InterAppLoginRequest;
use App\Model\SSO\InterAppLoginResponse;
use App\Model\SSO\Lib\SSOLogin;
use App\Model\SSO\SSOValues;
use App\Model\SSO\SSOToken;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;
use Cake\Utility\String;
use Cake\Auth\DefaultPasswordHasher;
use Cake\I18n\Time;
use Cake\Cache\Cache;
use Firebase\JWT\JWT;

class SSOController extends ProjapotiController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        // Allow users to register and logout.
        // You should not add the "login" action to allow list. Doing so would
        // cause problems with normal functioning of AuthComponent.
        $this->Auth->allow(['login', 'applogin', 'interapplogin','generateOISFToken','getLoginFailedResponse','loginCheckFromIDP']);
    }
    public function loginCheckFromIDP()
    {
        if ($this->request->is('get')) {
            $iSSOLogin = new SSOLogin();
            $url = $iSSOLogin->getRedirectUrl($this);

            return $this->redirect($url);
        }
        return $this->redirect(SSOConstants::LOGIN_PAGE_URI);
    }

    public function login()
    {
        if ($this->request->is('get')) {

            $CLS_SSOValues = new SSOValues();
            $client_id =  $CLS_SSOValues->getAppId();
//            $client_id =  'EOQ0GNKlJXzDzIHG4dqbFYYDhKR232Ho';
            $landing_page_uri =  $CLS_SSOValues->getLandingPageUrl();
            $idpLoginURL =  $CLS_SSOValues->getIdpUrl().'/verify';
            $nonce = time();

            $this->set(compact('client_id','landing_page_uri','idpLoginURL','nonce'));
            // maintenance
            if(date('Y-m-d') == '2019-01-23' && date('G') <= 20 && date('G') >= 18 && Live == 1){
                $this->layout = 'login_new';
                $this->view = 'maintenance';
                return;
            } else {
                $this->layout = 'login';
            }
            if(defined('NewLogin') && NewLogin == 1){
                $this->layout = '/Users/login_new';
                $this->view = 'login_new';
            }
            elseif(defined('NewLogin') && NewLogin == 2){
                $this->view = '/Users/login_new2';
                $this->layout = 'login_new2';
            }
            $session = $this->request->session();
            $session->write('nonce', $nonce);
            // maintenance

            /*
             * previous code
             */
//            $landingPageUrl = isset($this->request->query["landing_page_uri"]) ? $this->request->query["landing_page_uri"] : '';
//            $appLoginRequest = new AppLoginRequest();
//            $appLoginRequest->setLandingPageUrl($landingPageUrl);
//            $requestUrl = $appLoginRequest->buildRequest();
//            $nonce = $appLoginRequest->getReqNonce();
//
//            $session = $this->request->session();
//            $session->write('nonce', $nonce);

//            Cache::write('nonce', $nonce, 'memcached');

//            return $this->redirect($requestUrl);
        }
        if ($this->request->is('post') && isset($this->request->data['token'])) {
            $token = $this->request->data['token'];
            $this->request->data['username'] = ' ';
            $this->request->data['password'] = ' ';
            try {
                $session = $this->request->session();
                $nonce = $session->read('nonce');

                $appLoginResponse = new AppLoginResponse();
                $response = $appLoginResponse->parseResponse($token, $nonce);

                $session->write('username', $response->getUserName());
                $session->delete("nonce");

                return $this->redirect($response->getLandingPageUrl());
            } catch (\Exception $ex) {
            }
        }
    }

    public function applogin()
    {
        try {
            $token = !empty($this->request->data['token']) ? $this->request->data['token'] : '';
            $this->request->data['username'] = '';
            $this->request->data['password'] = '';
            $session = $this->request->session();
            $nonce = $session->read('nonce');
//            if(empty($nonce)){
//                $nonce = Cache::read('nonce', 'memcached');
//            }
            //first check token status to know if error occured
            $decoded_token_info = $this->decodeToken($token);
            if(!empty($decoded_token_info) && $decoded_token_info['status'] == 'success'){
                // check response status error or not
                $decode_token = $decoded_token_info['data'];
                if(!empty($decode_token) && $decode_token['status'] == 'error'){
                    $this->Flash->error($decode_token['message']);
                   return $this->redirect(['_name' => 'login']);
                }
            }

            $appLoginResponse = new AppLoginResponse();
            $response = $appLoginResponse->parseResponse($token, $nonce);
            if (!empty($response->getUserName())) {
                $username = $response->getUserName();
                $session->write('username', $username);
                $session->delete("nonce");
                // User successfully authenticated
                // Please provide him session
                // $response contains all the necessary user information.
                $userTable = TableRegistry::get('Users');
                $userData = $userTable->find()->where(['username' => $username])->orWhere(['user_alias' => $username])->first();
                if (!empty($userData)) {
                    $user = $userTable->get($userData['id']);
                    $user->force_password_change = 1;
                    unset($user->password);
                    $this->Auth->setUser($user->toArray());
                    $employee_info = TableRegistry::get('EmployeeOffices')->find()->select(['office_unit_organogram_id'])->where(['employee_record_id' => $user['employee_record_id'],'status' => 1])->first();
                    if (!empty($employee_info)) {
                        $session->write('selected_office_section', $this->setCurrentDakSection($employee_info['office_unit_organogram_id']));
                        $modules = jsonA(MODULES);
                        $session->write('modules', $modules);
                        if ($user['user_role_id'] > 2) {
                            $session->write('module_id', 5);
                        } else {
                            $session->write('module_id', 1);
                        }
                        if ($user['user_role_id'] > 2) {
                            $table_instance_emp_offices = TableRegistry::get('EmployeeOffices');
                            if (($employee_office_records = Cache::read('emp_offc_rcds_' . $user['employee_record_id'], 'memcached')) === false) {
                                $employee_office_records = $table_instance_emp_offices->getEmployeeOfficeRecords($user['employee_record_id']);
                                Cache::write('emp_offc_rcds_' . $user['employee_record_id'], $employee_office_records, 'memcached');
                            }

                            if (!empty($this->request->data['fingerprint'])) {
                                foreach ($employee_office_records as $key => $value) {
                                    $value['fingerprint'] = $this->request->data['fingerprint'];
                                }
                            }

                            $this->saveLoginHistory($employee_office_records);
                        }
                    }
                    return $this->redirect($response->getLandingPageUrl());
                } else {
                    $ssoRequest = new AppLoginRequest();
                    die('Empty DATA');
                    return $this->redirect($ssoRequest->buildRequest());
                }
            }else{
                pr($response);die;
            }
        } catch (\Exception $ex) {
            $this->Flash->error($ex->getMessage());
            print_r($ex->getMessage());
            die;
            // $ssoRequest = new AppLoginRequest();
            // return $this->redirect($ssoRequest->buildRequest());
        }

    }

    public function iALoginReqInfoAjax()
    {
        $toAppName = $this->request->data['toAppName'];
        $landingPageUrl = $this->request->data['landingPageUrl']; //null
        $username = $this->Auth->user()["username"];

        $obj = new InterAppLoginRequest();
        echo($obj->getIALoginReqInfoAjax($username, $toAppName, $landingPageUrl));
        exit();
    }

    public function interapplogin()
    {
        $token = !empty($this->request->data['token']) ? $this->request->data['token'] : '';
        if (isset($token)) {
            try {
                $iaLoginResponse = new InterAppLoginResponse();
                $response = $iaLoginResponse->parseResponse($token);
                if (!empty($response->getUserName())) {
                    $username = $response->getUserName();
                    $userTable = TableRegistry::get('Users');
                    $userData = $userTable->find()->where(['username' => $username])->orWhere(['user_alias' => $username])->first();
                    if (!empty($userData)) {
                        $user = $userTable->get($userData['id']);
                        $user->force_password_change = 1;
                        unset($user->password);
                        $this->Auth->setUser($user->toArray());
                        if (!empty($response->getLandingPageUrl())) {
                            if ($response->getLandingPageUrl() == 'dashboard') {
                                return $this->redirect(['controller' => 'dashboard', 'action' => 'dashboard']);
                            } else {

                                return $this->redirect($response->getLandingPageUrl());
                            }

                        } else {
                            return $this->redirect(['controller' => 'dashboard', 'action' => 'dashboard']);
                        }

                    } else {
                        $ssoRequest = new AppLoginRequest();
                        return $this->redirect($ssoRequest->buildRequest());
                    }
                }
            } catch (\Exception $ex) {
                var_dump($ex->getMessage());
                exit(0);
            }
        }
    }

    public function logout()
    {
        $session = $this->request->session();
        $employee_office = $this->getCurrentDakSection();
        if (!empty($employee_office)) {
            $this->updateLoginHistory($employee_office['officer_id']);
        }
        $session->destroy();
        $appLogoutRequest = new AppLogoutRequest();
        $requestUrl = $appLogoutRequest->buildRequest();
        return $this->redirect($requestUrl);
    }

    public function apps()

    {

        $session = $this->request->session();

        $employee_office = $this->getCurrentDakSection();

        if (!empty($employee_office)) {

            $designationId = $employee_office['office_unit_organogram_id'];

        }

        $getToken = $this->generateOISFToken();
        // pr($getToken);die;
        if(!empty($getToken['status']) && $getToken['status'] == 'success'){
            $token = $getToken['token']; // should be a token
        }else{
            $token = "ancsuyfaudhsfvhvt2rt8285fg2"; // should be a token
            die('token not generated');
        }
        // $format = "http://esb.beta.doptor.gov.bd:8280/identity/designation/". 103150 . "/apps";
        // $url = "http://esb.beta.doptor.gov.bd:8280/identity/designation/". $designationId . "/apps";
        $url = "http://esb.beta.doptor.gov.bd:8280/identity/designation/". 103150 . "/apps";


        // $url = sprintf($format, $designationId);

        $curl = curl_init();

        curl_setopt_array($curl, array(

            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => "",
            CURLOPT_HTTPHEADER => array(
                "Authorization: Bearer {$token}",
                "cache-control: no-cache"
            ),

        ));


        $result = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        }

        echo $result;

        die;

    }


    public function testAppPermission()

    {

        echo '<pre>';

        try {

            $session = $this->request->session();

            $employee_office = $this->getCurrentDakSection();

            if (!empty($employee_office)) {

                $designationId = $employee_office['office_unit_organogram_id'];

            }

            $getToken = $this->generateOISFToken();
            if(!empty($getToken['status']) && $getToken['status'] == 'success'){
                $token = $getToken['token']; // should be a token
            }else{
                $token = "ancsuyfaudhsfvhvt2rt8285fg2"; // should be a token
            }


            $format = "http://doptor.gov.bd:8090/identity/designation/" . $designationId . "/apps";

            $url = sprintf($format, $designationId);


            $curl = curl_init();

            curl_setopt_array($curl, array(

                CURLOPT_URL => $url,

                CURLOPT_RETURNTRANSFER => 1,

                CURLOPT_HTTPHEADER, array(

                    "Authorization: Bearer " . $token

                ),

            ));


            $result = curl_exec($curl);

            curl_close($curl);

            echo $result;

            die;

        } catch (\Exception $ex) {

            print_r($ex->getMessage);

        }


    }
    private function generateOISFToken(){
        $response = array("status" => 'error', 'message' => 'Invalid request');
        try{
            $ssoValues = new SSOValues();
            $url = "http://esb.beta.doptor.gov.bd:8280/token/create";
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => "",
                CURLOPT_HTTPHEADER => array(
                    "Authorization: Secret {$ssoValues->getSharedSecret()}",
                    "cache-control: no-cache"
                ),
            ));

            $result = curl_exec($curl);
            $result = jsonA($result);

            curl_close($curl);

            if(!empty($result['token'])){
                $response = array("status" => 'success', 'token' => $result['token']);
            }
        }catch (\Exception $ex){
            $response['message'] = $ex->getMessage();
        }
        return $response;
    }
    public function getLoginFailedResponse(){
        if(isset($this->request->query['message'])){
            $message = urldecode($this->request->query['message']);
        }
        $this->Flash->error(isset($message)?$message:'দুঃখিত লগইন করতে সমস্যা হচ্ছে।');
        $this->redirect(['_name' => 'login']);
    }
    protected function decodeToken($oisf_response){
        $response = array("status" => 'error', 'message' => 'Something went wrong');
        try{
            $CLS_SSOValues = new SSOValues();
            $decoded = json_decode(json_encode(JWT::decode($oisf_response,  $CLS_SSOValues->getSharedSecret(), array('HS256'))),true);
            $response = array("status" => 'success', 'data' => $decoded);
        }catch (\Exception $ex){
            $response = array("status" => 'error', 'message' => $ex->getMessage());
        }
        return $response;
    }

}
