<?php

namespace App\Controller;

use App\Controller\ProjapotiController;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;
use Cake\Cache\Cache;

class SearchController extends ProjapotiController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
    }
    public function searchNotes($subject = ''){
        $response = ['status' => 'error' , 'msg' => 'Something happen'];

        $user_info = $this->getCurrentDakSection();
        $nothi_office = isset($this->request->data['nothi_office'])?$this->request->data['nothi_office']:(!empty($user_info['office_id']))?$user_info['office_id']:0;
        $office_id = isset($this->request->data['office_id'])?$this->request->data['office_id']:(!empty($user_info['office_id']))?$user_info['office_id']:0;
        $office_unit_id = isset($this->request->data['office_unit_id'])?$this->request->data['office_unit_id']:(!empty($user_info['office_unit_id']))?$user_info['office_unit_id']:0;
        $designation_id = isset($this->request->data['designation_id'])?$this->request->data['designation_id']:(!empty($user_info['office_unit_organogram_id']))?$user_info['office_unit_organogram_id']:0;

        
        if(empty($subject) || empty($nothi_office) || empty($office_id) || empty($office_unit_id) || empty($designation_id)){
            $response['msg'] = 'Search parameters missing';
        }
        try{
            if($nothi_office != $office_id){
                $this->switchOffice($nothi_office, 'OfficeDB');
            }
            $nothiMasterPermissionsTable = TableRegistry::get("NothiMasterPermissions");
            $permitted_nothi_list = $nothiMasterPermissionsTable->allNothiPermissionList($nothi_office, $office_id, $office_unit_id, $designation_id);
             $nothiNotesTable = TableRegistry::get('NothiNotes');
             $searh_data = $nothiNotesTable->find('list',['keyField' => 'nothi_part_no','valueField' => 'subject'])->where(['subject LIKE' => '%'.h($subject).'%','nothi_part_no IN' => $permitted_nothi_list])->toArray();
             $response = ['status' => 'success' , 'data' => $searh_data];
        } catch (\Exception $ex) {
            $response['msg'] = $ex->getMessage();
        }
        rtn:
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
    }
    public function searchPotros($subject = ''){
        $response = ['status' => 'error' , 'msg' => 'Something happen'];

        $user_info = $this->getCurrentDakSection();
        $nothi_office = isset($this->request->data['nothi_office'])?$this->request->data['nothi_office']:(!empty($user_info['office_id']))?$user_info['office_id']:0;
        $office_id = isset($this->request->data['office_id'])?$this->request->data['office_id']:(!empty($user_info['office_id']))?$user_info['office_id']:0;
        $office_unit_id = isset($this->request->data['office_unit_id'])?$this->request->data['office_unit_id']:(!empty($user_info['office_unit_id']))?$user_info['office_unit_id']:0;
        $designation_id = isset($this->request->data['designation_id'])?$this->request->data['designation_id']:(!empty($user_info['office_unit_organogram_id']))?$user_info['office_unit_organogram_id']:0;


        if(empty($subject) || empty($nothi_office) || empty($office_id) || empty($office_unit_id) || empty($designation_id)){
            $response['msg'] = 'Search parameters missing';
        }
        if(empty($subject)){
            $response['msg'] = 'Search parameters missing';
        }
        try{
            if($nothi_office != $office_id){
                $this->switchOffice($nothi_office, 'OfficeDB');
            }
             $nothiMasterPermissionsTable = TableRegistry::get("NothiMasterPermissions");
            $potroAttachmentTable = TableRegistry::get('NothiPotroAttachments');
            $permitted_nothi_list = $nothiMasterPermissionsTable->allNothiPermissionList($nothi_office, $office_id, $office_unit_id, $designation_id);

            $potroAllAttachmentRecord = $potroAttachmentTable->find('list',['keyField' => 'nothi_part','valueField' => 'subject'])->select(['subject' =>  'CONCAT(NothiPotros.subject," ( ",PotrojariData.potro_subject," )")','nothi_part' => 'NothiPotroAttachments.nothi_part_no'])
                ->contain(['PotrojariData'])
                ->join([
                    'NothiPotros' => [
                        'table' => 'nothi_potros',
                        'type' => 'INNER',
                        'conditions' => 'NothiPotros.id = NothiPotroAttachments.nothi_potro_id'
                    ]
                ])
                ->where(['OR' =>['NothiPotros.subject LIKE' => '%'.h($subject).'%','PotrojariData.potro_subject LIKE' => '%'.h($subject).'%']])
                ->where(['NothiPotroAttachments.nothijato' => 0, 'NothiPotroAttachments.status' => 1])
                ->where(['NothiPotroAttachments.nothi_part_no IN' => $permitted_nothi_list])
                ->order(['NothiPotroAttachments.id desc'])->toArray();
             $response = ['status' => 'success' , 'data' => $potroAllAttachmentRecord];
        } catch (\Exception $ex) {
            $response['msg'] = $ex->getMessage();
        }
        rtn:
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
    }
}