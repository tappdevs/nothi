<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class EmployeeRanks extends Entity
{
    protected $_accessible = [
        'office_ministry_id' => true,
        'rank_name_eng' => true,
        'rank_name_bng' => true,
        'created_by' => true,
        'modified_by' => true,
        'office_ministries' => true,
    ];
}