<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * LoginPageSetting Entity.
 */
class LoginPageSetting extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'type' => true,
        'title' => true,
        'description' => true,
        'attachments' => true,
        'status' => true,
    ];
}
