<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * LoginPageSetting Entity.
 */
class MonthlyReport extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'title' => true,
        'year' => true,
        'month' => true,
        'status' => true,
        'rank' => true,
        'attachment_path' => true,
    ];
}
