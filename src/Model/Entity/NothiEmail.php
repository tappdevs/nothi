<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Entity;

/**
 * Description of NothiEmail
 *
 * @author shampa
 */
class NothiEmail {
    
    public function addEmailQueue($data = [])
    {
        if (!empty($data) && defined("EMAIL_QUEUE_URL")) {

            //set Domain Name so that user can not be confused
            if (!defined('Live') || Live == 0) {
                if (defined("TESTING_SERVER") && TESTING_SERVER) {
                    $mail_from = 'Test';
                }else{
                    $mail_from = 'Training';
                }
                if(isset($data['subject'])){
                    $data['subject'] = "[{$mail_from}]".$data['subject'];
                }
            }
            //set Domain Name so that user can not be confused

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => EMAIL_QUEUE_URL,
                CURLOPT_USERAGENT => 'Nothi Email Queue',
                CURLOPT_POST => 1,
                CURLOPT_POSTFIELDS => http_build_query($data)
            ));
            $resp = curl_exec($curl);
            curl_close($curl);
            return true;
        } else {
            return false;
        }
    }
}
