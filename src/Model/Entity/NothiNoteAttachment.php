<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * NothiNoteAttachment Entity.
 */
class NothiNoteAttachment extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'nothi_master_id' => true,
        'nothi_part_no' => true,
        'nothi_notesheet_id' => true,
        'note_no' => true,
        'attachment_type' => true,
        'file_name' => true,
        'user_file_name' => true,
        'file_dir' => true,
        'digital_sign' => true,
        'sign_info' => true,
        'created_by' => true,
        'modified_by' => true,
        'token' => true,
        'device_type' => true,
        'device_id' => true,
    ];
}
