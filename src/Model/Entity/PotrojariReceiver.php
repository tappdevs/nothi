<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PotrojariReceiver Entity.
 */
class PotrojariReceiver extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'id'=>true,
        'potrojari_id' => true,
        'nothi_master_id' => true,
        'nothi_part_no' => true,
        'nothi_notes_id' => true,
        'nothi_potro_id' => true,
        'potro_type' => true,
        'dak_id' => true,
        'receiving_office_id' => true,
        'receiving_office_name' => true,
        'receiving_office_unit_id' => true,
        'receiving_office_unit_name' => true,
        'receiving_officer_id' => true,
        'receiving_officer_designation_id' => true,
        'receiving_officer_designation_label' => true,
        'receiving_officer_name' => true,
        'receiving_officer_email' => true,
        'run_task' => true,
        'retry_count' => true,
        'task_reposponse' => true,
        'potro_status' => true,
        'is_sent' => true,
        'created_by' => true,
        'modified_by' => true,
        'options' => true,
    ];
}
