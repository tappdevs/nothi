<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * SummaryNothiUser Entity.
 */
class SummaryNothiUser extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'tracking_id' => true,
        'nothi_office' => true,
        'current_office_id' => true,
        'nothi_master_id' => true,
        'nothi_part_no' => true,
        'potrojari_id' => true,
        'dak_id' => true,
        'potro_id' => true,
        'designation_id' => true,
        'employee_record_id' => true,
        'position_number' => true,
        'can_approve' => true,
        'is_approve' => true,
        'current_office' => true,
        'nothi_master' => true,
        'potrojari' => true,
        'dak' => true,
        'potro' => true,
        'designation' => true,
        'employee_record' => true,
    ];
}
