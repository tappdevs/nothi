<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * UserDeviceRegistration Entity.
 */
class UserDeviceRegistration extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'device_id' => true,
        'activate' => true,
        'activation_code' => true,
        'activate_request' => true,
        'activate_date' => true,
        'expire_date' => true,
        'user' => true,
        'device' => true,
    ];
}
