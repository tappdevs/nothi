<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * UserSignature Entity.
 */
class UserSignature extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'username' => true,
        'signature_file' => true,
        'encode_sign' => true,
        'previous_signature' => true,
        'created_by' => true,
        'modified_by' => true,
    ];
}
