<?php

namespace App\Model\SSO\Lib;

use App\Model\SSO\AppLoginResponse;
use App\Model\SSO\AppLoginResponseDTO;
use App\Model\SSO\Lib\Interfaces\IHome;
use Cake\Controller\Controller;
use Exception;

class Home implements IHome
{
    private $userFullName;

    public function __construct()
    {
    }

    public function getUserData(Controller $object)
    {
        try {
            if (($response = $this->isUserAlreadyLogin($object)) != null) {
                $this->userFullName = $this->getUserName($object, $response);
                return $response;
            } else {
                return null;
            }
        } catch (Exception $e) {
            return null;
        }
    }

    private function isUserAlreadyLogin(Controller $object)
    {
        $nonce = $object->request->getSession()->read(LibConstants::NONCE);
        $token = $object->request->getSession()->read(LibConstants::USER_SESSION_KEY);

        if ($nonce == null || $token == null) return null;

        $appLoginResponse = new AppLoginResponse();
        try {
            $response = $appLoginResponse->parseResponse($token, $nonce);
            return $response;
        } catch (Exception $e) {
            return null;
        }
    }

    private function getUserName(Controller $object, AppLoginResponseDTO $appLoginResponseDTO)
    {
        if (($userName = $object->request->getSession()->read(LibConstants::USER_FULL_NAME)) != null) {
            return $userName;
        }

        $httpRequest = new SSOHttpRequest();
        $userInfoDTO = $httpRequest->getUserDetails($object, $appLoginResponseDTO->getUserName());

        $object->request->getSession()->write(LibConstants::USER_FULL_NAME, $userInfoDTO->getNameBng());
        return $userInfoDTO->getNameBng();
    }

    public function getUserFullName()
    {
        return $this->userFullName;
    }
}