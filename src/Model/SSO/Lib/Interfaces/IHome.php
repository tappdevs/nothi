<?php

namespace App\Model\SSO\Lib\Interfaces;

use Cake\Controller\Controller;

interface IHome
{
    public function getUserData(Controller $object);
}