<?php

namespace App\Model\SSO\Lib\Interfaces;

use Cake\Controller\Controller;

interface ISSOLogout
{
    public function getRedirectUrl(Controller $object);
}