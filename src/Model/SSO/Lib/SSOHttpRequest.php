<?php

namespace App\Model\SSO\Lib;

use App\Model\SSO\SSOConstants;
use Cake\Controller\Controller;

class SSOHttpRequest
{
    public function __construct()
    {
    }

    public function getWidgetList(Controller $object)
    {
        $token = $this->getSecretKey($object);
        $designation = $this->getDesignation($object);

        $headers = array(
            "Authorization: Bearer " . $token,
            "Content-Type: application/json"
        );
        $url = sprintf(LibConstants::WIDGET_URL, $designation);

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($curl);
        curl_close($curl);

        return $result;
    }

    public function getUserDetails(Controller $object, $userId)
    {
        $token = $this->getSecretKey($object);
        $headers = array(
            "Authorization: Bearer " . $token,
            "Content-Type: application/json"
        );
        $url = sprintf(LibConstants::EMPLOYEE_DETAILS_URL, $userId);

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($curl);
        curl_close($curl);

        $userInformationDTO = UserInformationDTO::parseJSON(json_decode($result, true));
        return $userInformationDTO;
    }

    private function getSecretKey(Controller $object)
    {
        if (($token = $this->hasApiAccessToken($object)) != null) return $token;

        $ch = curl_init(LibConstants::CREATE_TOKEN_URL);
        $options = array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HEADER => false,
            CURLOPT_FOLLOWLOCATION => false,
            CURLOPT_AUTOREFERER => true,
            CURLOPT_CONNECTTIMEOUT => 20,
            CURLOPT_TIMEOUT => 20,
            CURLOPT_POST => 1,
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_VERBOSE => 1,
            CURLOPT_HTTPHEADER => array(
                "Authorization: Secret " . SSOConstants::SHARED_SECRET,
                "Content-Type: application/json"
            )
        );

        curl_setopt_array($ch, $options);
        $data = curl_exec($ch);
        curl_close($ch);

        $arr = json_decode($data, true);
        $object->request->getSession()->write(LibConstants::API_ACCESS_TOKEN, $arr[LibConstants::TOKEN]);
        $object->request->getSession()->write(LibConstants::API_ACCESS_TOKEN_VALIDITY_TIME, $arr[LibConstants::TOKEN_VALIDITY_TIME] + LibConstants::getCurrentUTCTime());
        return $arr[LibConstants::TOKEN];
    }

    private function hasApiAccessToken(Controller $object)
    {
        if (($validity = $object->request->getSession()->read(LibConstants::API_ACCESS_TOKEN_VALIDITY_TIME)) != null) {
            if ($validity <= LibConstants::getCurrentUTCTime()) return null;
        }

        $token = ($object->request->getSession()->read(LibConstants::API_ACCESS_TOKEN));
        return $token;
    }

    private function getDesignation(Controller $object)
    {
        if ($designation = ($object->request->getSession()->read(LibConstants::SSO_DESIGNATION)) != null) {
            return $designation;
        }
        return null;
    }
}