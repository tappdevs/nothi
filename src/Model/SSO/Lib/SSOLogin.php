<?php

namespace App\Model\SSO\Lib;

use App\Model\SSO\AppLoginRequest;
use App\Model\SSO\AppLoginResponse;
use App\Model\SSO\Lib\Interfaces\ISSOLogin;
use App\Model\SSO\SSOConstants;
use Cake\Controller\Controller;
use Exception;

class SSOLogin implements ISSOLogin
{
    public function __construct()
    {
    }

    public function getRedirectUrl(Controller $object)
    {
        $session = $object->request->session();
        $userSessionKey = $session->read(LibConstants::USER_SESSION_KEY);
        if ($userSessionKey != null && $this->isUserAlreadyLogin($object) != null) {
            return SSOConstants::LANDING_PAGE_URI;
        }

        $appLoginRequest = new AppLoginRequest();
        $requestUrl = $appLoginRequest->buildRequest();
        $nonce = $appLoginRequest->getReqNonce();
        $session->write(LibConstants::NONCE, $nonce);

        return $requestUrl;
    }

    private function isUserAlreadyLogin(Controller $object)
    {
        $session = $object->request->session();
        $nonce = $session->read(LibConstants::NONCE);
        $token = $session->read(LibConstants::USER_SESSION_KEY);

        $appLoginResponse = new AppLoginResponse();
        try {
            $response = $appLoginResponse->parseResponse($token, $nonce);
            return $response;
        } catch (Exception $e) {
            return null;
        }
    }
}