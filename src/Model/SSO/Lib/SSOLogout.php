<?php

namespace App\Model\SSO\Lib;

use App\Model\SSO\AppLogoutRequest;
use App\Model\SSO\Lib\Interfaces\ISSOLogout;
use Cake\Controller\Controller;

class SSOLogout implements ISSOLogout
{
    public function __construct()
    {
    }

    public function getRedirectUrl(Controller $object)
    {
        $object->request->getSession()->destroy();

        $appLogoutRequest = new AppLogoutRequest();
        $requestUrl = $appLogoutRequest->buildRequest();

        return $requestUrl;
    }
}