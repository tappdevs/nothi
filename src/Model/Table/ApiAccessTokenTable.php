<?php
namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;
use Cake\Utility\Text;
use Cake\Auth\DefaultPasswordHasher;
use App\Controller\ProjapotiController;


class ApiAccessTokenTable extends ProjapotiTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $conn = ConnectionManager::get('ApiAuthDB');
        $this->connection($conn);
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
    }
    
    public function setToken($api_user_id, $request = []){
        $token = sGenerateToken(['api_uid' => makeEncryptedData($api_user_id),'r_ip' => makeEncryptedData($request['ip'])]);
        try{
            $entity = $this->newEntity();
            
            $entity->api_user_id = $api_user_id;
            $entity->api_token = ($token);
            $entity->token_valid = 1;
            $entity->last_request_time = date("Y-m-d H:i:s");
            $entity->last_access_time = date("Y-m-d H:i:s");
            $entity->request_ip = $request['ip'];
            $entity->request_info = json_encode($request);
            
            $this->save($entity);
            return ['token'=>$token,'status'=>true];
        }  catch (\Exception $ex){
            
            return $ex->getMessage();
        }
    }
    
    public function removeToken($api_user_id){
        try{
            $this->deleteAll(['api_user_id'=>$api_user_id]);
            return ['status'=>true];
        }  catch (\Exception $ex){
            
            return $ex->getMessage();
        }
    }
    
    public function expireToken($api_user_id){
        try{
            
            $this->updateAll(['token_valid'=>0],['api_user_id'=>$api_user_id]);
            return ['status'=>true];
        }  catch (\Exception $ex){
            
            return $ex->getMessage();
        }
    }
    
    public function checkToken($token, $ip){
        try{
           
            $checkToken = $this->find()->where(['request_ip IN'=>$ip,'api_token'=> $token, 'token_valid'=>1])->first();
            if(!empty($checkToken)){
                if(checkToken($token)==false){
                    return "Invalid Token";
                }
                return ['status'=>true];
            }else{
                if (!defined("TOKEN_CHECK") || TOKEN_CHECK == 0) {
                    return (API_KEY == $token ? ['status'=>true] : "Invalid Token");
                }
                return "Invalid Token";
            }
            
        }  catch (\Exception $ex){
            
            return $ex->getMessage();
        }
        
        return "Invalid Token";
    }
}