<?php
namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\Validation\Validator;
use  Cake\Datasource\ConnectionManager;

/**
 * Articles Model
 */
class ApiNothiPartFlagTable extends ProjapotiTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
    }



}
