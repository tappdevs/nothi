<?php

namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;

class ApiTokensTable extends ProjapotiTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $conn = ConnectionManager::get('projapotiDb');
        $this->connection($conn);
        $this->table('api_tokens');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->addBehavior('MobileTrack');

    }
    public function checkByUserName($username = ''){
        if(empty($username))
            return ;
        return $this->find()->where(['username' => $username])->first();
    }
    public function checkByEmployeeRecordId($employee_record_id = ''){
        if(empty($employee_record_id))
            return ;
        return $this->find()->where(['employee_record_id' => $employee_record_id])->first();
    }
}