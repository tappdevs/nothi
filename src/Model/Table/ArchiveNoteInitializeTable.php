<?php
namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

class ArchiveNoteInitializeTable extends ProjapotiTable
{

    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->table('note_initialize');
        $this->addBehavior('Timestamp');
        $this->addBehavior('MobileTrack');
    }


    public function validationAdd($validator)
    {
        $validator
            ->notEmpty('office_units_id', "এই তথ্য ফাকা রাখা যাবে না")
            ->notEmpty('office_units_organogram_id', "এই তথ্য ফাকা রাখা যাবে না")
            ->notEmpty('nothi_masters_id', "এই তথ্য ফাকা রাখা যাবে না")
            ->notEmpty('nothi_part_no', "এই তথ্য ফাকা রাখা যাবে না");

        return $validator;
    }
    public function saveData($requested_data = []){
        if($this->checkDuplicate($requested_data['nothi_part_no'],date('Y-m-d'))>0){
            return;
        }
        $entity = $this->newEntity();
        $entity->nothi_masters_id = $requested_data['nothi_masters_id'];
        $entity->nothi_part_no = $requested_data['nothi_part_no'];
        $entity->office_id = $requested_data['office_id'];
        $entity->office_units_id = $requested_data['office_units_id'];
        $entity->office_units_organogram_id = $requested_data['office_units_organogram_id'];
        $entity->created_by = $entity->modified_by =  $requested_data['user_id'];
        return $this->save($entity);
        
    }
    public function updateDakNoteStatus($nothi_part_no, $date){
        return $this->updateAll(['dak_note' => 1,'self_note' => 0],['nothi_part_no' => $nothi_part_no,'DATE(created)' => $date]);
    }
    public function checkDuplicate($nothi_part_no, $date){
        return $this->find()->where(['nothi_part_no' => $nothi_part_no,'DATE(created)' => $date])->count();
    }
     public function selfSrijitoNote($office_id = 0, $unit_id = 0, $designation_id = 0,
                                         $time = [])
    {
        $query = $this->find();

        if (!empty($office_id)) {
            $query = $query->where(['office_id' => $office_id]);
        }

        if (!empty($unit_id)) {
            $query = $query->where(['office_units_id' => $unit_id]);
        }

        if (!empty($designation_id)) {
            $query = $query->where(['office_units_organogram_id' => $designation_id]);
        }

        if (!empty($time[0])) {
            $query = $query->where(['DATE(created) >=' => $time[0]]);
        }

        if (!empty($time[1])) {
            $query = $query->where(['DATE(created) <=' => $time[1]]);
        }

        return $query->where(['self_note' => 1])->group(['nothi_part_no']);
    }

    public function dakSrijitoNote($office_id = 0, $unit_id = 0, $designation_id = 0,
                                        $time = [])
     {
        $query = $this->find();

        if (!empty($office_id)) {
            $query = $query->where(['office_id' => $office_id]);
        }

        if (!empty($unit_id)) {
            $query = $query->where(['office_units_id' => $unit_id]);
        }

        if (!empty($designation_id)) {
            $query = $query->where(['office_units_organogram_id' => $designation_id]);
        }

        if (!empty($time[0])) {
            $query = $query->where(['DATE(created) >=' => $time[0]]);
        }

        if (!empty($time[1])) {
            $query = $query->where(['DATE(created) <=' => $time[1]]);
        }

        return $query->where(['dak_note' => 1])->group(['nothi_part_no']);
    }
   
    public function selfSrijitoNoteByNoteId($note_ids = [],
                                         $time = [])
    {
        $query = $this->find();

        if (!empty($note_ids)) {
            $query->where(['nothi_part_no IN' => $note_ids]);
        }

        if (!empty($time[0])) {
            $query->where(['DATE(created) >=' => $time[0]]);
        }

        if (!empty($time[1])) {
            $query->where(['DATE(created) <=' => $time[1]]);
        }

        return $query->where(['self_note' => 1])->group(['nothi_part_no']);
    }
    public function dakSrijitoNoteByNoteId($note_ids = [],
                                         $time = [])
    {
        $query = $this->find();

        if (!empty($note_ids)) {
            $query->where(['nothi_part_no IN' => $note_ids]);
        }else{
            return $query;
        }

        if (!empty($time[0])) {
            $query->where(['DATE(created) >=' => $time[0]]);
        }

        if (!empty($time[1])) {
            $query->where(['DATE(created) <=' => $time[1]]);
        }

        return $query->where(['dak_note' => 1])->group(['nothi_part_no']);
    }
     public function getNothiDetails($office_id = 0, $unit_id = 0, $designation_id = 0,
                                        $time = [])
     {
        $query = $this->find()->select(['nothi_part_no' =>'NoteInitialize.nothi_part_no','office_id' =>'NoteInitialize.office_id','office_units_id' =>'NoteInitialize.office_units_id','office_units_organogram_id' =>'NoteInitialize.office_units_organogram_id','nothi_masters_id' =>'NoteInitialize.nothi_masters_id','subject' =>'NothiParts.subject','nothi_no' =>'NothiParts.nothi_no']);

        if (!empty($office_id)) {
            $query = $query->where(['NoteInitialize.office_id' => $office_id]);
        }

        if (!empty($unit_id)) {
            $query = $query->where(['NoteInitialize.office_units_id' => $unit_id]);
        }

        if (!empty($designation_id)) {
            $query = $query->where(['NoteInitialize.office_units_organogram_id' => $designation_id]);
        }

        if (!empty($time[0])) {
            $query = $query->where(['DATE(NoteInitialize.created) >=' => $time[0]]);
        }

        if (!empty($time[1])) {
            $query = $query->where(['DATE(NoteInitialize.created) <=' => $time[1]]);
        }

        return $query->join([
            "NothiParts" => [
                'table' => 'nothi_parts', 'type' => 'inner',
                'conditions' => ['NoteInitialize.nothi_part_no = NothiParts.id']
            ],
        ])->group(['NoteInitialize.nothi_masters_id']);
    }
}

