<?php
namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

class ArchiveNothiNotesTable extends ProjapotiTable
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->addBehavior('MobileTrack');
    }

    public function getNotes($nothiMasterId, $employeeId)
    {

        return $this->find()->where(['nothi_part_no' => $nothiMasterId, 'employee_id' => $employeeId]);
    }

    public function getNotesInfo($nothiMasterId, $noteid)
    {

        return $this->find()->where(['nothi_part_no' => $nothiMasterId, 'note_no' => $noteid])->first();
    }

    public function getLastNote($nothiMasterId){
        return $this->find()->where(['nothi_part_no' => $nothiMasterId])->order(['id DESC'])->first();
    }

    public function getFirstNote($nothiMasterId){
        return $this->find()->where(['nothi_part_no' => $nothiMasterId])->order(['subject desc'])->first();
    }
    
    public function getNotesbyNotesheet($noteSheet_no)
    {
        return $this->find()->where(['nothi_notesheet_id' => $noteSheet_no])->toArray();
    }

    public function checkEmptyNote($nothi_master_id, $nothi_parts, $count = true){
        if(!empty($nothi_master_id) && !empty($nothi_parts)){
            $query = $this->find()->where(['ArchiveNothiNotes.nothi_part_no IN'=>$nothi_parts,'ArchiveNothiNotes.nothi_master_id'=>$nothi_master_id,'note_no <>' => -1])->distinct(['ArchiveNothiNotes.nothi_part_no']);
            if($count==false){
                return $query->first();
            }
            return $query->count();
        }
        else if(empty ($nothi_master_id) && !empty ($nothi_parts)){
            $query = $this->find()
                    ->where(['ArchiveNothiNotes.nothi_part_no IN'=>$nothi_parts,'note_no <>' => -1])->distinct(['ArchiveNothiNotes.nothi_part_no']);

            if($count==false){
                return $query->first();
            }
            return $query->count();
        }
      
    }

    public function getLastNoteSheetId($nothimasterid, $nothi_office){
        return $this->find()->select([ 'nothi_notesheet_id' ])->where(['nothi_part_no'=>$nothimasterid,'office_id'=>$nothi_office])->first();
    }
    public function getLastNotetId($nothimasterid, $nothi_office){
        return $this->find()->select([ 'id' ])->where(['nothi_part_no'=>$nothimasterid,'office_id'=>$nothi_office])->first();
    }
    public function findfistNoteforDashboardCount($nothi_part_no = 0,$created_date = '',$office_id = 0,$date_range = []){
        if(empty($nothi_part_no))
            return 0 ;
        $query = $this->find()->where(['nothi_part_no']);
        if(!empty($created_date)){
            $query->where(['created >=' => $created_date]);
        }
        if(!empty($office_id)){
            $query->where(['office_id' => $office_id]);
        }
        if(!empty($date_range)){
            if(!empty($date_range[0])){
                $query->where(['date(created) >=' => $date_range[0]]);
            }
            if(!empty($date_range[1])){
                $query->where(['date(created) <=' => $date_range[1]]);
            }
        }
        return $query->count();

    }
    public function findNoteAfterNisponno($nothi_part_no,$nisponno_time){
        $query = $this->find();
        if(!empty($nothi_part_no)){
            $query->where(['nothi_part_no' => $nothi_part_no]);
        }
        if(!empty($nisponno_time)){
            $query->where(['created >' => $nisponno_time]);
        }
        return $query->count();
          
    }
}