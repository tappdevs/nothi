<?php

namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

class ArchiveNothiPartsTable extends ProjapotiTable
{

    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->addBehavior('Timestamp');
        $this->addBehavior('MobileTrack');
    }


    public function validationAdd($validator)
    {
        $validator
            ->notEmpty('office_units_id', "এই তথ্য ফাকা রাখা যাবে না")
            ->notEmpty('office_units_organogram_id', "এই তথ্য ফাকা রাখা যাবে না")
            ->notEmpty('office_units_organogram_name', "এই তথ্য ফাকা রাখা যাবে না")
            ->notEmpty('nothi_types_id', 'notBlank', "এই তথ্য ফাকা রাখা যাবে না")
            ->notEmpty('nothi_no', "এই তথ্য ফাকা রাখা যাবে না")
            ->notEmpty('subject', "এই তথ্য ফাকা রাখা যাবে না")
            ->notEmpty('nothi_no', "এই তথ্য ফাকা রাখা যাবে না")
            ->notEmpty('nothi_masters_id', "এই তথ্য ফাকা রাখা যাবে না")
            ->notEmpty('nothi_part_no', "এই তথ্য ফাকা রাখা যাবে না")
            ->notEmpty('nothi_part_no_bn', "এই তথ্য ফাকা রাখা যাবে না");

        return $validator;
    }

    public function hasThisType($typeId)
    {
        return $this->find()->select(['id'])->where(['nothi_types_id' => $typeId])->count();
    }

    public function getAll($condition = '', $page = 0, $len = 10)
    {

        $queryString =
            $this->find()->select([
                "ArchiveNothiParts.id",
                "ArchiveNothiParts.nothi_masters_id",
                "ArchiveNothiParts.office_units_organogram_id",
                "ArchiveNothiParts.nothi_part_no",
                "ArchiveNothiParts.nothi_part_no_bn",
                "ArchiveNothiParts.nothi_no",
                "ArchiveNothiParts.nothi_types_id",
                "ArchiveNothiParts.office_id",
                "ArchiveNothiParts.office_units_id",
                "ArchiveNothiParts.subject",
                "ArchiveNothiParts.nothi_created_date",
                "ArchiveNothiParts.created",
                "ArchiveNothiParts.nothi_class",
                "ArchiveNothiParts.modified",
                "ArchiveNothiParts.is_active",
                "NothiTypes.type_name"
            ])->where(["ArchiveNothiParts.is_deleted" => 0])
                ->join([
                    'NothiTypes' => [
                        'table' => 'nothi_types',
                        "conditions" => "ArchiveNothiParts.nothi_types_id= NothiTypes.id AND NothiTypes.is_deleted=0",
                        "type" => "INNER"
                    ]
                ])->order(['ArchiveNothiParts.modified desc']);

        if (!empty($condition)) {
            $queryString = $queryString->where([$condition]);
        }
        if (!empty($page)) {
            $queryString = $queryString->page($page, $len);
        }

        return $queryString;
    }

    public function getAllList($condition = '', $page = 0, $len = 10)
    {

        $queryString =
            $this->find()->select([
                "ArchiveNothiParts.id",
                "ArchiveNothiParts.nothi_masters_id",
                "ArchiveNothiParts.nothi_part_no",
                "ArchiveNothiParts.nothi_part_no_bn",
                "ArchiveNothiParts.nothi_no",
                "ArchiveNothiParts.nothi_types_id",
                "ArchiveNothiParts.office_id",
                "ArchiveNothiParts.office_units_id",
                "ArchiveNothiParts.subject",
                "ArchiveNothiParts.nothi_created_date",
                "ArchiveNothiParts.created",
                "ArchiveNothiParts.nothi_class",
                "ArchiveNothiParts.modified",
                "ArchiveNothiParts.is_active",
                "NothiTypes.type_name",
            ])->where(["ArchiveNothiParts.is_deleted" => 0])
                ->join([

                    'NothiTypes' => [
                        'table' => 'nothi_types',
                        "conditions" => "ArchiveNothiParts.nothi_types_id= NothiTypes.id AND NothiTypes.is_deleted=0",
                        "type" => "INNER"
                    ]
                ])->order(['ArchiveNothiParts.modified DESC']);

        if (!empty($condition)) {
            $queryString = $queryString->where([$condition]);
        }
        if (!empty($page)) {
            $queryString = $queryString->page($page, $len);
        }

        return $queryString;
    }

    public function getAllNothi($condition = '', $page = 0, $len = 10)
    {

        $queryString =
            $this->find()->select([
                "ArchiveNothiParts.id",
                "ArchiveNothiParts.nothi_no",
                "ArchiveNothiParts.nothi_types_id",
                "ArchiveNothiParts.office_id",
                "ArchiveNothiParts.office_units_id",
                "ArchiveNothiParts.subject",
                "ArchiveNothiParts.nothi_created_date",
                "ArchiveNothiParts.created",
                "ArchiveNothiParts.nothi_class",
                "ArchiveNothiParts.modified",
                "ArchiveNothiParts.is_active"
            ])->where(["ArchiveNothiParts.is_deleted" => 0])
                ->order("ArchiveNothiParts.modified DESC");

        if (!empty($condition)) {
            $queryString = $queryString->where([$condition]);
        }
        if (!empty($page)) {
            $queryString = $queryString->page($page, $len);
        }

        return $queryString;
    }


    public function canDelete($id = 0)
    {
        $nothi_potros_tb = TableRegistry::get('NothiPotros');
        $nothi_potro_attachments_tb = TableRegistry::get('NothiPotroAttachments');
        $nothi_note_signaturess_tb = TableRegistry::get('NothiNoteSignatures');
        $nothi_notes_tb = TableRegistry::get('NothiNotes');

        $hasPotro = $nothi_potros_tb->find()->where(['nothi_part_no' => $id, 'is_deleted' => 0])->count();
        $hasPotroAttach = $nothi_potro_attachments_tb->find()->where(['nothi_part_no' => $id, 'status' => 1])->count();
        $hasSent = $nothi_note_signaturess_tb->find()->where(['nothi_part_no' => $id])->count();
        $hasNote = $nothi_notes_tb->find()->where(['nothi_part_no' => $id])->count();

        return !($hasPotro || $hasPotroAttach || $hasSent || $hasNote);

    }

    public function updateNoteNumbers($nothi_master_id = 0)
    {

    }

    public function lastNote($nothi_mater_id)
    {
        return $this->find()->where(['nothi_masters_id' => $nothi_mater_id])->order(['nothi_created_date DESC'])->first();
    }

    public function firstNote($nothi_mater_id)
    {
        return $this->find()->where(['nothi_masters_id' => $nothi_mater_id])->order(['nothi_created_date ASC'])->first();
    }

    public function selfSrijitoNoteCount($office_id = 0, $unit_id = 0, $designation_id = 0, $time = [])
    {
        $query = $this->find()->join([
            "NothiNotes" => [
                'table' => 'nothi_notes',
                'type' => 'left',
                'conditions' => ['NothiNotes.nothi_part_no =NothiParts.id']
            ],
            "NothiMastersDakMap" => [
                'table' => 'nothi_masters_dak_map',
                'type' => 'left',
                'conditions' => ['NothiMastersDakMap.nothi_part_no =NothiParts.id']
            ]
        ]);

        if (!empty($office_id)) {
            $query = $query->where(['ArchiveNothiParts.office_id' => $office_id]);
//            $query = $query->where(['NothiNotes.office_id' => $office_id]);
        }

        if (!empty($unit_id)) {
            $query = $query->where(['ArchiveNothiParts.office_units_id' => $unit_id]);
//            $query = $query->where(['NothiNotes.office_unit_id' => $unit_id]);
        }

        if (!empty($designation_id)) {
            $query = $query->where(['ArchiveNothiParts.office_units_organogram_id' => $designation_id]);
//            $query = $query->where(['NothiNotes.office_organogram_id' => $designation_id]);
        }

        if (!empty($time[0])) {
            $query = $query->where(['DATE(NothiParts.nothi_created_date) >=' => $time[0]]);
//            $query = $query->where(['DATE(NothiNotes.created) >=' => $time[0]]);
        }

        if (!empty($time[1])) {
            $query = $query->where(['DATE(NothiParts.nothi_created_date) <=' => $time[1]]);
//            $query = $query->where(['DATE(NothiNotes.created) <=' => $time[1]]);
        }

        return $query->where(['NothiNotes.nothi_part_no is not NULL'])->where(['NothiMastersDakMap.id is NULL'])->group(['NothiNotes.nothi_part_no'])->count();
    }

    public function selfSrijitoNotePart($office_id = 0, $unit_id = 0, $designation_id = 0, $time = [])
    {
        $query = $this->find('list', ['keyField' => 'id', 'valueField' => 'id'])->join([
            "NothiNotes" => [
                'table' => 'nothi_notes',
                'type' => 'left',
                'conditions' => ['NothiNotes.nothi_part_no =NothiParts.id']
            ],
            "NothiMastersDakMap" => [
                'table' => 'nothi_masters_dak_map',
                'type' => 'left',
                'conditions' => ['NothiMastersDakMap.nothi_part_no =NothiParts.id']
            ]
        ]);

        if (!empty($office_id)) {
            $query = $query->where(['ArchiveNothiParts.office_id' => $office_id]);
//            $query = $query->where(['NothiNotes.office_id' => $office_id]);
        }

        if (!empty($unit_id)) {
            $query = $query->where(['ArchiveNothiParts.office_units_id' => $unit_id]);
//            $query = $query->where(['NothiNotes.office_unit_id' => $unit_id]);
        }

        if (!empty($designation_id)) {
            $query = $query->where(['ArchiveNothiParts.office_units_organogram_id' => $designation_id]);
//            $query = $query->where(['NothiNotes.office_organogram_id' => $designation_id]);
        }

        if (!empty($time[0])) {
            $query = $query->where(['DATE(NothiParts.nothi_created_date) >=' => $time[0]]);
//            $query = $query->where(['DATE(NothiNotes.created) >=' => $time[0]]);
        }

        if (!empty($time[1])) {
            $query = $query->where(['DATE(NothiParts.nothi_created_date) <=' => $time[1]]);
//            $query = $query->where(['DATE(NothiNotes.created) <=' => $time[1]]);
        }

        return $query->where(['NothiNotes.nothi_part_no is not NULL'])->where(['NothiMastersDakMap.id is NULL'])->group(['NothiNotes.nothi_part_no'])->toArray();
    }

    public function dakSrijitoNoteCount($office_id = 0, $unit_id = 0, $designation_id = 0, $time = [])
    {

        $query = $this->find()->join([
            "NothiMastersDakMap" => [
                'table' => 'nothi_masters_dak_map',
                'type' => 'inner',
                'conditions' => ['NothiMastersDakMap.nothi_part_no =NothiParts.id']
            ],
//                    "DakMovements" => [
//                        'table' => 'dak_movements',
//                        'type' => 'inner',
//                        'conditions' => ['NothiMastersDakMap.dak_movements_id =DakMovements.id']
//                    ]

        ]);
        if (!empty($office_id)) {
            $query = $query->where(['ArchiveNothiParts.office_id' => $office_id]);
        }

        if (!empty($unit_id)) {
            $query = $query->where(['ArchiveNothiParts.office_units_id' => $unit_id]);
        }

        if (!empty($designation_id)) {
            $query = $query->where(['ArchiveNothiParts.office_units_organogram_id' => $designation_id]);
        }
        if (!empty($time[0])) {
            $query = $query->where(['DATE(NothiParts.nothi_created_date) >=' => $time[0]]);
        }
        if (!empty($time[1])) {
            $query = $query->where(['DATE(NothiParts.nothi_created_date) <=' => $time[1]]);
        }

        return $query->group(['NothiMastersDakMap.nothi_part_no'])->count();

    }

    public function notePartListfornothiVuktoDak($office_id = 0, $unit_id = 0, $designation_id = 0, $time = [])
    {

        $query = $this->find('list', ['keyField' => 'id', 'valueField' => 'id'])->join([
            "NothiMastersDakMap" => [
                'table' => 'nothi_masters_dak_map',
                'type' => 'inner',
                'conditions' => ['NothiMastersDakMap.nothi_part_no =NothiParts.id']
            ],
//                    "DakMovements" => [
//                        'table' => 'dak_movements',
//                        'type' => 'inner',
//                        'conditions' => ['NothiMastersDakMap.dak_movements_id =DakMovements.id']
//                    ]
        ]);

        if (!empty($office_id)) {
            $query = $query->where(['ArchiveNothiParts.office_id' => $office_id]);
        }

        if (!empty($unit_id)) {
            $query = $query->where(['ArchiveNothiParts.office_units_id' => $unit_id]);
        }

        if (!empty($designation_id)) {
            $query = $query->where(['ArchiveNothiParts.office_units_organogram_id' => $designation_id]);
        }
        if (!empty($time[0])) {
            $query = $query->where(['DATE(NothiParts.nothi_created_date) >=' => $time[0]]);
        }
        if (!empty($time[1])) {
            $query = $query->where(['DATE(NothiParts.nothi_created_date) <=' => $time[1]]);
        }

        return $query->group(['NothiMastersDakMap.nothi_part_no'])->toArray();

    }


//    public  function selfSrijitoNoteList($office_id = 0, $unit_id = 0, $designation_id = 0, $time = []){
//
//        $query = $this->find('list',['keyField' => 'ArchiveNothiParts.id', 'valueField' => 'ArchiveNothiParts.id'])->join([
//                    "NothiMastersDakMap" => [
//                        'table' => 'nothi_masters_dak_map',
//                        'type' => 'left',
//                        'conditions' => ['NothiMastersDakMap.nothi_part_no =NothiParts.id']
//                    ]
//                ]);
//          if (!empty($office_id)) {
//                $query = $query->where(['ArchiveNothiParts.office_id' => $office_id]);
//        }
//
//        if (!empty($unit_id)) {
//                $query = $query->where(['ArchiveNothiParts.office_units_id' => $unit_id]);
//        }
//
//        if (!empty($designation_id)) {
//                $query = $query->where(['ArchiveNothiParts.office_units_organogram_id' => $designation_id]);
//        }
//        if (!empty($time[0])) {
//            $query = $query->where(['DATE(NothiParts.nothi_created_date) >=' => $time[0]]);
//        }
//        if (!empty($time[1])) {
//            $query = $query->where(['DATE(NothiParts.nothi_created_date) <=' => $time[1]]);
//        }
//         return $query->where(['NothiMastersDakMap.id is NULL'])->where(['ArchiveNothiParts.is_active' => 1])->group(['ArchiveNothiParts.nothi_part_no','NothiMastersDakMap.nothi_part_no'])->toArray();
//    }
    public function getNothiPartNoBnByID($noth_parts = [])
    {
        return $this->find('list', ['keyField' => 'id', 'valueField' => 'nothi_part_no_bn'])->where(['id IN' => $noth_parts]);
    }
}