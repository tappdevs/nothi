<?php
namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;


class DakActionEmployeesTable extends ProjapotiTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $conn = ConnectionManager::get('NothiAccessDb');
        $this->connection($conn);
        $this->primaryKey('id');
        $this->displayField('dak_action_id');
        $this->addBehavior('Timestamp');
    }

    public function GetDakActionsByEmployee($employee_id){
        $data= $this->find()->select(['id'=>'DakActionsMig.id','dak_action_name'=>'DakActionsMig.dak_action_name'])
            ->join([
                'DakActionsMig'=>[
                    'table'=>'dak_actions_mig',
                    'type'=>'inner',
                    'conditions' => [
                        'DakActionEmployees.dak_action_id= DakActionsMig.id'
                    ]
                ]
            ])
            ->where(['DakActionEmployees.employee_id'=>$employee_id])
            ->where(['DakActionsMig.status'=>1])
            ->order(['DakActionEmployees.id' => 'DESC'])
            ->toArray();
        if(empty($data)){
            $data= $this->find()->select(['id'=>'DakActionsMig.id','dak_action_name'=>'DakActionsMig.dak_action_name'])
                ->join([
                    'DakActionsMig'=>[
                        'table'=>'dak_actions_mig',
                        'type'=>'inner',
                        'conditions' => [
                            'DakActionEmployees.dak_action_id= DakActionsMig.id'
                        ]
                    ]
                ])
                ->where(['DakActionEmployees.employee_id'=>0])
                ->where(['DakActionsMig.status'=>1])
                ->order(['DakActionEmployees.id' => 'DESC'])
                ->toArray();
        }
        return $data;
    }

}