<?php
namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;


class DakActionsMigTable extends ProjapotiTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $conn = ConnectionManager::get('NothiAccessDb');
        $this->connection($conn);
        $this->primaryKey('id');
        $this->displayField('dak_action_name');
        $this->addBehavior('Timestamp');
    }
        public function GetDakActions( $employee_id=0)
    {
        if(!empty($employee_id)) {
            $query = $this->find()->select(['id', 'dak_action_name', 'creator'])
                ->where(['creator' => $employee_id])
                ->orWhere(['creator' => 0])
                ->where(['status' => 1]);

        } else {
            $query = $this->find()->select(['id', 'dak_action_name', 'creator'])
                ->where(['creator' => $employee_id])
                ->orWhere(['creator' => 0]);
        }

        return $query->order(['id desc']);
    }
}