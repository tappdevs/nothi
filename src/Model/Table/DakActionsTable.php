<?php
namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;


class DakActionsTable extends ProjapotiTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $conn = ConnectionManager::get('projapotiDb');
        $this->connection($conn);
        $this->primaryKey('id');
        $this->displayField('dak_action_name');
        $this->addBehavior('Timestamp');
        $this->addBehavior('MobileTrack');
    }
        public function GetDakActions($org_id = 0)
    {
        $query = $this->find()->select(['id','dak_action_name','organogram_id'])->where(['status'=>1]);
        
        if(!empty($org_id)){
            $query->where(['organogram_id'=>$org_id])->orWhere(['organogram_id'=>0]);
        }
        
        return $query->order(['organogram_id desc']);
    }
}