<?php
namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;


class DakAttachmentsTable extends ProjapotiTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        /*$this->addBehavior('Cake3Upload.Upload', [
                'fields' => [
                    'file_name' => [
                            'path' => 'upload/DAK/:filename_prefix',
                        ]
                    ]
                ]
        );*/
    }


    /**
     * Find All Attachments by dak id
     */
    public function loadAllAttachmentByDakId($dak_id, $dak_type = "Daptorik")
    {
        return $this->find()
            ->select(['id', 'file_name', 'attachment_type','dak_id','dak_type', 'attachment_description','user_file_name','is_main','content_body'])
            ->where(['dak_id' => $dak_id, 'dak_type' => $dak_type])->toArray();
    }
    /**
     * Find first Attachments by dak id
     */

    public function loadFirstAttachmentByDakId($dak_id, $attachmentType='', $dak_type = "Daptorik")
    {
        if(!empty($attachmentType)){
            return $this->find()
                ->select(['id', 'file_name', 'attachment_type', 'attachment_description','content_body'])
                ->where(['dak_id' => $dak_id, 'dak_type' => $dak_type])->where(["attachment_type LIKE '%%{$attachmentType}%%'"])->first();
        }
        return $this->find()
            ->select(['id', 'file_name', 'attachment_type', 'attachment_description','content_body'])
            ->where(['dak_id' => $dak_id, 'dak_type' => $dak_type])->first();
    }

}