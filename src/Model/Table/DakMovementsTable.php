<?php

namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\I18n\Number;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

class DakMovementsTable extends ProjapotiTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->addBehavior('MobileTrack');
    }

    public function validationDefault(Validator $validator)
    {
        $validator->notEmpty('dak_id', "এই তথ্য ফাকা রাখা যাবে না");

        return $validator;
    }

    public function validationAdd($validator)
    {
        $validator
            ->notEmpty('dak_id', "এই তথ্য ফাকা রাখা যাবে না");
        return $validator;
    }

    public function getSequenceNumber($dak_id, $daktype = "Daptorik")
    {

        return $this->find()->select(['sequence'])->distinct(['sequence'])->where(['dak_id' => $dak_id,
                                                                                   'dak_type' => $daktype])->order(['id desc'])->toArray();
    }

    public function getReceivedDakIdsBy_employee_designation($employee_record_id,
                                                             $designation_id,
                                                             $dak_type = "Daptorik")
    {
        $dak_ids = $this->find('list')->select(['dak_id'])->where(['to_officer_designation_id' => $designation_id,
                                                                   'dak_type' => $dak_type]);
        return $dak_ids = array_unique($dak_ids, SORT_REGULAR);
    }

    public function getInboxDakLastMoveBy_dakid_attention_type($dakId,
                                                               $attention_type = 1,
                                                               $dak_type = "Daptorik")
    {
        return $this->find()->where(['dak_id' => $dakId, 'attention_type' => $attention_type,
                                     'dak_type' => $dak_type])->order(['id desc'])->first();
    }

    public function getInboxDakLastMoveBy_dakid_officerId_designationId($dakId,
                                                                        $employee_record_id,
                                                                        $designation_id,
                                                                        $dak_type
                                                                        = "Daptorik")
    {
        return $this->find()->where(['dak_id' => $dakId, 'to_officer_designation_id' => $designation_id,
                                     'dak_type' => $dak_type])->order(['id desc'])->first();
    }

    public function getSentDakLastMoveBy_dakid_officerId_designationId($dakId,
                                                                       $employee_record_id,
                                                                       $designation_id,
                                                                       $dak_type
                                                                       = "Daptorik")
    {
        return $this->find()->where(['dak_id' => $dakId, 'from_officer_designation_id' => $designation_id,
                                     'dak_type' => $dak_type])->order(['id desc'])->first();
    }

    public function getDakLastMoveBy_dakid_officerId_designationId($dakId,
                                                                   $employee_record_id,
                                                                   $designation_id,
                                                                   $dak_type = "Daptorik")
    {
//        ,'attention_type' => 1
        return $this->find()
                    ->where(['from_officer_designation_id' => $designation_id])
                    ->orWhere(['to_officer_designation_id' => $designation_id])
                    ->where(['dak_id' => $dakId, 'dak_type' => $dak_type, 'attention_type' => 1])
                    ->order(['sequence desc, id desc'])->limit(2)->toArray();
    }

    public function getDakMovesBy_sequence_dakId($sequence_no, $dakId,
                                                 $dak_type = "Daptorik",
                                                 $attentiontype = -1)
    {

        $query = $this->find()->where(['dak_id' => $dakId, 'sequence' => $sequence_no,
                                       'dak_type' => $dak_type]);
        if ($attentiontype >= 0) {
            $query = $query->where(['attention_type' => $attentiontype]);
        }
        return $query->order(['attention_type DESC, id asc'])->toArray();
    }

    public function getDakMovesBydakId($dakId, $dak_type = "Daptorik")
    {

        $query = $this->find()->where(['dak_id' => $dakId, 'dak_type' => $dak_type]);

        return $query->order(['sequence desc, id desc'])->distinct(['sequence'])->toArray();
    }

    public function getDak_for_nothiVukto($dakId, $dak_type = "Daptorik")
    {
        return $this->find()->where(['dak_id' => $dakId, 'attention_type' => 1, 'dak_type' => $dak_type])->order(['id DESC'])->first();
    }

    public function getDakLastMove($dak_id, $dak_type = "Daptorik", $conditions = [])
    {
        $query = $this->find()->where(['dak_id' => $dak_id, 'dak_type' => $dak_type]);

        if (!empty($conditions)) {
            $query->where($conditions);
        }

        return $query->order(['sequence desc, id desc'])->first();
    }

    public function getDakLastMoveByDesignation($dak_id, $designation_id,
                                                $dak_type = "Daptorik")
    {
        return $this->find()->where(['dak_id' => $dak_id, 'to_officer_designation_id' => $designation_id,
                                     'dak_type' => $dak_type])->order(['sequence desc, id desc'])->first();
    }

    public function getLastofEachGroupOfDak($startDate, $endDate, $units)
    {

        $conditions = sprintf("DATE(modified) BETWEEN '%s' AND '%s'", $startDate, $endDate);
        $table_instance_dak_user = TableRegistry::get('DakUsers');
        $query = $table_instance_dak_user->find()->where(['dak_category' => DAK_CATEGORY_INBOX, 'dak_type' => DAK_DAPTORIK,
                                                          'to_office_unit_id IN' => explode(',', $units),
                                                          'is_archive' => 0])->where($conditions)->order(['modified ASC']);

        return $query;
    }

    public function getLastStatusofEachNaogirk($condition = [], $having = [])
    {
        $query = $this->find()
                      ->select([
                          'DakMovements.dak_type',
                          'DakMovements.dak_id',
                          'DakMovements.to_officer_name',
                          'DakMovements.to_officer_designation_label',
                          'DakMovements.to_office_unit_name',
                          'DakMovements.to_office_unit_id',
                          'DakMovements.to_office_id',
                          'DakMovements.to_officer_id',
                          'DakMovements.to_officer_designation_id',
                          'DakMovements.to_office_name',
                          'DakMovements.dak_actions',
                          'DakMovements.sequence',
                          "movecreated" => 'date(DakMovements.created)',
                          "movemodified" => 'date(DakMovements.modified)',
                          'DakMovements.dak_priority',
                          "dakcreated" => 'date(DakNagoriks.created)',
                          "dak_type_id" => 'DakNagoriks.dak_type_id',
                          "receiving_office_id" => 'DakNagoriks.receiving_office_id',
                          "dak_received_no" => 'DakNagoriks.dak_received_no',
                          "mobile_no" => 'DakNagoriks.mobile_no',
                          "nagorikstatus" => 'DakNagoriks.dak_status',
                          "dak_subject" => 'DakNagoriks.dak_subject',
                          "sender_name" => 'DakNagoriks.sender_name',
                          "feedback_type" => 'DakNagoriks.feedback_type',
                          "description" => 'DakNagoriks.description',
                          "receive_date" => 'DakNagoriks.receive_date',
                      ])
                      ->join([
                          /*'DakMovements2' => [
                              'table' => 'dak_movements',
                              'type' => 'left',
                              'conditions' => "DakMovements.id<DakMovements2.id AND DakMovements.dak_id=DakMovements2.dak_id AND DakMovements.dak_type=DakMovements2.dak_type AND DakMovements.dak_type='Nagorik'"
                          ],*/
                          'DakNagoriks' => [
                              'table' => 'dak_nagoriks',
                              'type' => 'INNER',
                              'conditions' => "DakMovements.dak_id=DakNagoriks.id AND DakMovements.dak_type='Nagorik'"
                          ]
                      ])
                      ->where([
                          "DakMovements.attention_type" => 1, #'DakMovements2.id IS NULL'
                      ]);

        if (!empty($condition)) {
            $query = $query->where($condition);
        }
        if (!empty($having)) {
            $query = $query->having($having);
        }

        $query = $query->order(['DakMovements.id DESC'])->limit(1)->toArray();

        return $query;
    }

    public function getFullofEachNaogirk($condition = "", $having = "")
    {

        $query = $this->find()
                      ->select([
                          'DakMovements.dak_type',
                          'DakMovements.dak_id',
                          'DakMovements.to_officer_name',
                          'DakMovements.to_officer_designation_label',
                          'DakMovements.to_office_unit_name',
                          'DakMovements.to_office_unit_id',
                          'DakMovements.to_office_id',
                          'DakMovements.to_officer_id',
                          'DakMovements.to_officer_designation_id',
                          'DakMovements.to_office_name',
                          'DakMovements.dak_actions',
                          'DakMovements.sequence',
                          "movecreated" => 'date(DakMovements.created)',
                          "movemodified" => 'date(DakMovements.modified)',
                          'DakMovements.dak_priority',
                          "dakcreated" => 'date(DakNagoriks.created)',
                          "dak_type_id" => 'DakNagoriks.dak_type_id',
                          "receiving_office_id" => 'DakNagoriks.receiving_office_id',
                          "dak_received_no" => 'DakNagoriks.dak_received_no',
                          "mobile_no" => 'DakNagoriks.mobile_no',
                          "nagorikstatus" => 'DakNagoriks.dak_status',
                          "dak_subject" => 'DakNagoriks.dak_subject',
                          "sender_name" => 'DakNagoriks.sender_name',
                          "feedback_type" => 'DakNagoriks.feedback_type',
                          "description" => 'DakNagoriks.description',
                          "receive_date" => 'DakNagoriks.receive_date',
                      ])
                      ->join([
                          'DakMovements2' => [
                              'table' => 'dak_movements',
                              'type' => 'left',
                              'conditions' => "DakMovements.id<DakMovements2.id AND DakMovements.dak_id=DakMovements2.dak_id AND DakMovements.dak_type=DakMovements2.dak_type AND DakMovements.dak_type='Nagorik'"
                          ],
                          'DakNagoriks' => [
                              'table' => 'dak_nagoriks',
                              'type' => 'INNER',
                              'conditions' => "DakMovements.dak_id=DakNagoriks.id AND DakMovements.dak_type='Nagorik'"
                          ]
                      ])
                      ->where([
                          'DakMovements2.id IS NULL'
                      ]);

        if (!empty($condition)) {
            $query = $query->where($condition);
        }
        if (!empty($having)) {
            $query = $query->having($having);
        }

        return $query->order(['DakMovements.dak_id ASC'])->toArray();
    }

    public function getLastofEachDaptorik($condition = [], $having = [])
    {
        $query = $this->find()
                      ->select([
                          'DakMovements.dak_type',
                          'DakMovements.dak_id',
                          'DakMovements.to_officer_name',
                          'DakMovements.to_officer_designation_label',
                          'DakMovements.to_office_unit_name',
                          'DakMovements.to_office_unit_id',
                          'DakMovements.to_office_id',
                          'DakMovements.to_officer_id',
                          'DakMovements.to_officer_designation_id',
                          'DakMovements.to_office_name',
                          'DakMovements.dak_actions',
                          'DakMovements.sequence',
                          "movecreated" => 'date(DakMovements.created)',
                          "movemodified" => 'date(DakMovements.modified)',
                          'DakMovements.dak_priority',
                          "dakcreated" => 'date(DakDaptoriks.created)',
                          "receiving_office_id" => 'DakDaptoriks.receiving_office_id',
                          "dak_received_no" => 'DakDaptoriks.dak_received_no',
                          "sender_mobile" => 'DakDaptoriks.sender_mobile',
                          "nagorikstatus" => 'DakDaptoriks.dak_status',
                          "dak_subject" => 'DakDaptoriks.dak_subject',
                          "sender_name" => 'DakDaptoriks.sender_name'
                      ])
                      ->join([
//                'DakMovements2'=>[
//                    'table'=>'dak_movements',
//                    'type'=>'left',
//                    'conditions'=>"DakMovements.id<DakMovements2.id AND DakMovements.dak_id=DakMovements2.dak_id AND DakMovements.dak_type=DakMovements2.dak_type AND DakMovements.dak_type='Daptorik'"
//                ],
'DakDaptoriks' => [
    'table' => 'dak_daptoriks',
    'type' => 'INNER',
    'conditions' => "DakMovements.dak_id=DakDaptoriks.id AND DakMovements.dak_type='Daptorik'"
]
                      ])
                      ->where([
                          "DakMovements.attention_type" => 1,#'DakMovements2.id IS NULL'
                      ]);

        if (!empty($condition)) {
            $query = $query->where($condition);
        }
        if (!empty($having)) {
            $query = $query->having($having);
        }

        $query = $query->order(['DakMovements.id DESC'])->limit(1)->toArray();

        return $query;
    }

    public function getDakCount($designationId = 0, $status = 'Sent')
    {
        if (!empty($designationId)) {
            return $this->find()->select(['id'])->where(['from_officer_designation_id IN' => $designationId,
                                                         'operation_type' => $status])->distinct(['id'])->count();
        }
    }

    public function countAllDakbyType($office_id = 0, $unit_id = 0,
                                      $designation_id = 0, $status = "Inbox",
                                      $time = [])
    {

        if ($status == 'Inbox') {
            $query = $this->find()->where(['operation_type IN' => ["Sent", "Forward"]]);
        } else if ($status == 'Outbox') {
            $query = $this->find()->where(['operation_type' => "Forward", 'from_potrojari' => 0]);
        } else {
            $query = $this->find()->where(['operation_type' => $status]);
        }

        if (!empty($office_id)) {
            if ($status == 'Inbox') {
                $query = $query->where(['to_office_id' => $office_id]);
            } else {
                $query = $query->where(['from_office_id' => $office_id]);
            }
        }

        if (!empty($unit_id)) {
            if ($status == 'Inbox') {
                $query = $query->where(['to_office_unit_id' => $unit_id]);
            } else {
                $query = $query->where(['from_office_unit_id' => $unit_id]);
            }
        }

        if (!empty($designation_id)) {
            if ($status == 'Inbox') {
                $query = $query->where(['to_officer_designation_id' => $designation_id]);
            } else {
                $query = $query->where(['from_officer_designation_id' => $designation_id]);
            }
        }
        if ($status == 'Outbox') {
            $query = $query->where(['attention_type' => 1]);
        }

        if (!empty($time[0])) {
            $query = $query->where(['DATE(created) >=' => $time[0]]);
        }
        if (!empty($time[1])) {
            $query = $query->where(['DATE(created) <=' => $time[1]]);
        }

        return $query->count();
    }

    public function countAllDakforOffice($office_id = 0, $unit_id = 0,
                                         $designation_id = 0, $status = "Inbox",
                                         $time = [])
    {

        if ($status == 'Inbox') {
            $query = $this->find()->where(['operation_type IN' => ["Sent", "Forward"]]);
        } else if ($status == 'Outbox') {
            $query = $this->find()->where(['operation_type' => "Forward", 'from_potrojari' => 0]);
        } else {
            $query = $this->find()->where(['operation_type' => $status]);
        }

        if (!empty($office_id)) {
            if ($status == 'Inbox') {
                $query = $query->where(['to_office_id' => $office_id]);
            } else {
                $query = $query->where(['from_office_id' => $office_id]);
            }
        }

        if (!empty($unit_id)) {
            if ($status == 'Inbox') {
                $query = $query->where(['to_office_unit_id' => $unit_id]);
            } else {
                $query = $query->where(['from_office_unit_id' => $unit_id]);
            }
        }

        if (!empty($designation_id)) {
            if ($status == 'Inbox') {
                $query = $query->where(['to_officer_designation_id' => $designation_id]);
            } else {
                $query = $query->where(['from_officer_designation_id' => $designation_id]);
            }
        }
//        if($status =='Outbox'){
        $query = $query->where(['attention_type' => 1]);
//        }

        if (!empty($time[0])) {
            $query = $query->where(['DATE(created) >=' => $time[0]]);
        }
        if (!empty($time[1])) {
            $query = $query->where(['DATE(created) <=' => $time[1]]);
        }

        if ($status == 'Inbox' || $status == 'Outbox') {
            $query->where(['sequence' => 2]);
        }

        return $query->group(['dak_id', 'dak_type'])->count();
    }

    public function countOnulipiDak($office_id = 0, $unit_id = 0, $designation_id = 0, $time = [])
    {

        $query = $this->find()->where(['attention_type' => 0]);

        if (!empty($office_id)) {
            $query = $query->where(['to_office_id' => $office_id]);
        }

        if (!empty($unit_id)) {
            $query = $query->where(['to_office_unit_id' => $unit_id]);
        }

        if (!empty($designation_id)) {
            $query = $query->where(['to_officer_designation_id' => $designation_id]);
        }


        if (!empty($time[0])) {
            $query = $query->where(['DATE(created) >=' => $time[0]]);
        }
        if (!empty($time[1])) {
            $query = $query->where(['DATE(created) <=' => $time[1]]);
        }

        return $query->count();
    }

    public function getOnisponnoDak($office_id = 0, $unit_id = 0, $designation_id = 0, $date = '')
    {

        set_time_limit(0);
        ini_set('memory_limit', '-1');

        $query = $this->find('list', ['keyField' => 'dak_id', 'valueField' => 'dak_type'])->where(['operation_type <>' => 'NothiJato', 'operation_type <>' => 'NothiVukto', 'attention_type' => 1]);
        if (!empty($office_id) && empty($unit_id) && empty($designation_id)) {
            $query = $query->where(['sequence' => 2]);
        }
        if (!empty($office_id)) {
            $query = $query->where(['to_office_id' => $office_id]);
        }
        if (!empty($unit_id)) {
            $query = $query->where(['to_office_unit_id' => $unit_id]);
        }
        if (!empty($designation_id)) {
            $query = $query->where(['to_officer_designation_id' => $designation_id]);
        }
        if (!empty($date)) {
            $query = $query->where(['DATE(created) <=' => $date]);
        }
        $query = $query->group(['dak_id', 'dak_type'])->toArray();
        $cnt = 0;

        if (!empty($query)) {
            foreach ($query as $dak_id => $dak_type) {
                $dak_data = $this->getDakLastMove($dak_id, $dak_type, ['DATE(created) <=' => $date]);
                if ($dak_data['operation_type'] != 'NothiJato' && $dak_data['operation_type'] != 'NothiVukto') {
                    $cnt++;
                }
            }
        }
        return $cnt;
    }


    public function canRevert($dak_id = 0, $dak_type = DAK_DAPTORIK, $designation_id = 0, $unit_id = 0, $office_id = 0)
    {

        $nothiDakMovesTable = TableRegistry::get("NothiMastersDakMap");
        $nothiDakPotroMapsTable = TableRegistry::get("NothiDakPotroMaps");
        $nothiPotroAttachmentsTable = TableRegistry::get("NothiPotroAttachments");
        TableRegistry::remove('NothiMasterCurrentUsers');
        $nothiMasterCurrentUsersTable = TableRegistry::get("NothiMasterCurrentUsers");
        $nothiPartsTable = TableRegistry::get("NothiParts");
        $potrojariTable = TableRegistry::get("Potrojari");
        $potroFlagsTable = TableRegistry::get("PotroFlags");

        $noPotrojari = 1;
        $noFlag = 1;
        $isLast = 1;
        $notCurrentUser = 0;

        $lastMovements = $this->getDak_for_nothiVukto($dak_id, $dak_type);

        if ($lastMovements['to_officer_designation_id'] == $designation_id && $lastMovements['operation_type'] == 'NothiVukto') {
            $nothiForDak = $nothiDakPotroMapsTable->getNothiInformationForDak($dak_id, $dak_type);

            if (!empty($nothiForDak)) {
                $currentNothiUsers = $nothiMasterCurrentUsersTable->getCurrentUserbyDesignation($nothiForDak['nothi_part_no'], $designation_id, 0, $office_id);

                if (!empty($currentNothiUsers) && !empty($nothiForDak['nothi_potro_id'])) {
                    $notCurrentUser = 1;
                    $potrojari = $potrojariTable->getNothiPotroIfPotrojari($nothiForDak['nothi_potro_id'], $nothiForDak['nothi_office_id'], $nothiForDak['nothi_master_id'])->count();

                    if (!empty($potrojari)) {
                        $noPotrojari = 0;
                    }
                }

                $flags = $potroFlagsTable->getFlag(['NothiPotroAttachments.nothi_potro_id' => $nothiForDak['nothi_potro_id']])->join([
                    'NothiPotroAttachments' => [
                        'table' => 'nothi_potro_attachments',
                        'type' => 'inner', 'conditions' => 'NothiPotroAttachments.id = PotroFlags.potro_attachment_id'
                    ]
                ])->count();

                if ($flags) {
                    $noFlag = 0;
                }
            }
        }

        if ($noPotrojari && $noFlag && $notCurrentUser) {
            return true;
        }

        return false;
    }

    protected function number($val)
    {
        $num = new Number();
        return $num::format($val);
    }


    public function getDakListBk($type, $employee_office, $controller, $nothiDecision = 0, $api = false)
    {
        $isSearch = false;
        $condition = '1 ';
        $subject = isset($controller->request->data['dak_subject']) ? h(trim($controller->request->data['dak_subject']))
            : '';
        if (!empty($subject)) {
            $isSearch = true;
            if (!empty($condition)) $condition .= ' AND ';
            $condition .= " (DakDaptoriks.dak_subject LIKE '%{$subject}%' OR DakNagoriks.dak_subject LIKE '%{$subject}%')";
        }

        $officername = isset($controller->request->data['receiving_officer_name']) ?
            h(trim($controller->request->data['receiving_officer_name']))
            : '';
        if (!empty($officername)) {
            $isSearch = true;
            if (!empty($condition)) $condition .= ' AND ';
            $condition .= "  (DakMovements.from_officer_name LIKE '%{$officername}%')";
        }


        $receiving_office_unit_id = isset($controller->request->data['receiving_office_unit_id']) ?
            h(trim($controller->request->data['receiving_office_unit_id']))
            : '';
        if (!empty($receiving_office_unit_id)) {
            $isSearch = true;
            if (!empty($condition)) $condition .= ' AND ';
            $condition .= "  DakMovements.from_office_unit_id = '{$receiving_office_unit_id}'";
        }

        $security_level_filter = isset($controller->request->data['dak_security_level']) ?
            h(trim($controller->request->data['dak_security_level']))
            : '';
        if (!empty($security_level_filter)) {
            $isSearch = true;
            if (!empty($condition)) $condition .= ' AND ';
            $condition .= " (DakDaptoriks.dak_security_level = $security_level_filter OR DakNagoriks.dak_security_level 
            = {$security_level_filter})";
        }

        $priority_level_filter = isset($controller->request->data['dak_priority_level']) ?
            h(trim($controller->request->data['dak_priority_level']))
            : '';
        if (!empty($priority_level_filter)) {
            $isSearch = true;
            if (!empty($condition)) $condition .= ' AND ';
            $condition .= "  DakMovements.dak_priority = {$priority_level_filter}";
        }

        $from_date = isset($controller->request->data['from']) ? h(trim($controller->request->data['from'])) : '';
        if (!empty($from_date)) {
            $isSearch = true;
            if (!empty($condition)) $condition .= ' AND ';
            $condition .= "  date(DakMovements.modified) >= '{$from_date}'";
        }

        $end_date = isset($controller->request->data['to']) ?
            h(trim($controller->request->data['to'])) : '';
        if (!empty($end_date)) {
            $isSearch = true;
            if (!empty($condition)) $condition .= ' AND ';
            $condition .= "  date(DakMovements.modified) <= '{$end_date}'";
        }

        return $type == 'sent' ?
            $this->sentListBk($condition, $employee_office, $controller, $nothiDecision, $api) :
            ($type == 'inbox' ?
                $this->inboxListBk($condition, $employee_office, $controller, $nothiDecision, $api, $isSearch) :
                ($type == 'onulipi' ?
                    $this->onulipiListBk($condition, $employee_office, $controller, $nothiDecision, $api) :
                    (($type == 'nothivukto' ?
                        $this->nothivuktoListBk($condition, $employee_office, $controller, $nothiDecision, $api) :
                        ($type == 'nothijato' ?
                            $this->nothijatoListBk($condition, $employee_office, $controller, $nothiDecision, $api) :
                            ($type == 'archive' ? $this->onulipiListBk($condition, $employee_office, $controller,
                                $nothiDecision, $api, 'archive') :
                                ['total' => 0, 'data' => []])
                        ))))
            );
    }

    public function getDakList($type, $employee_office, $controller, $nothiDecision = 0, $api = false, $only_view = false)
    {
        $isSearch = false;
        $condition = [];
        $orCondition = [];
        $subject = isset($controller->request->data['dak_subject']) ?
            h(trim($controller->request->data['dak_subject']))
            : '';
        if (!empty($subject)) {
            $isSearch = true;
            if ($type == 'inbox') {
                array_push($condition, ['dak_subject LIKE' => "%" . $subject . "%"]);
                array_push($orCondition, ['dak_subject LIKE' => "%" . $subject . "%"]);
            } else {
                array_push($condition, ['DakDaptoriks.dak_subject LIKE' => "%" . $subject . "%"]);
                array_push($orCondition, ['DakNagoriks.dak_subject LIKE' => "%" . $subject . "%"]);
            }

        }

        $officername = isset($controller->request->data['receiving_officer_name']) ?
            h(trim($controller->request->data['receiving_officer_name']))
            : '';
        if (!empty($officername)) {
            $isSearch = true;
            if ($type == 'inbox') {
                array_push($condition, ['sender_name LIKE' => "%" . $officername . "%"]);
                array_push($orCondition, ['sender_name LIKE' => "%" . $officername . "%"]);
            } else {
                array_push($condition, ['DakMovements.from_officer_name LIKE' => "%" . $officername . "%"]);
                array_push($orCondition, ['DakMovements.from_officer_name LIKE' => "%" . $officername . "%"]);
            }

        }


        $receiving_office_unit_id = isset($controller->request->data['receiving_office_unit_id']) ?
            h(trim($controller->request->data['receiving_office_unit_id']))
            : '';
        if (!empty($receiving_office_unit_id)) {
            $isSearch = true;
            if ($type == 'inbox') {
                array_push($condition, ['from_office_unit_id' => $receiving_office_unit_id]);
                array_push($orCondition, ['from_office_unit_id' => $receiving_office_unit_id]);
            } else {
                array_push($condition, ['DakMovements.from_office_unit_id' => $receiving_office_unit_id]);
                array_push($orCondition, ['DakMovements.from_office_unit_id' => $receiving_office_unit_id]);
            }

        }

        $security_level_filter = isset($controller->request->data['dak_security_level']) ?
            h(trim($controller->request->data['dak_security_level']))
            : '';
        if (!empty($security_level_filter)) {
            $isSearch = true;
            if ($type == 'inbox') {
                array_push($condition, ['dak_security_level' => $security_level_filter]);
                array_push($orCondition, ['dak_security_level' => $security_level_filter]);
            } else {
                array_push($condition, ['DakDaptoriks.dak_security_level' => $security_level_filter]);
                array_push($orCondition, ['DakNagoriks.dak_security_level' => $security_level_filter]);
            }
        }

        $priority_level_filter = isset($controller->request->data['dak_priority_level']) ?
            h(trim($controller->request->data['dak_priority_level']))
            : '';
        if (!empty($priority_level_filter)) {
            $isSearch = true;
            if ($type == 'inbox') {
                array_push($condition, ['dak_priority_level' => $priority_level_filter]);
                array_push($orCondition, ['dak_priority_level' => $priority_level_filter]);
            } else {
                array_push($condition, ['DakMovements.dak_priority' => $priority_level_filter]);
                array_push($orCondition, ['DakMovements.dak_priority' => $priority_level_filter]);
            }

        }

        $from_date = isset($controller->request->data['from']) ? h(trim($controller->request->data['from'])) : '';
        $end_date = isset($controller->request->data['to']) ? h(trim($controller->request->data['to'])) : '';
        if (!empty($from_date) && !empty($end_date)) {
            $isSearch = true;

        }


        return $type == 'sent' ?
            $this->sentList($condition, $employee_office, $controller, $nothiDecision, $api, $orCondition, $from_date, $end_date, $only_view) :
            ($type == 'inbox' ?
                $this->inboxList($condition, $employee_office, $controller, $nothiDecision, $api, $isSearch, $orCondition, $from_date, $end_date, $only_view) :
                ($type == 'onulipi' ?
                    $this->onulipiList($condition, $employee_office, $controller, $nothiDecision, $api, '', $orCondition, $from_date, $end_date, $only_view) :
                    (($type == 'nothivukto' ?
                        $this->nothivuktoList($condition, $employee_office, $controller, $nothiDecision, $api, $orCondition, $from_date, $end_date, $only_view) :
                        ($type == 'nothijato' ?
                            $this->nothijatoList($condition, $employee_office, $controller, $nothiDecision, $api, $orCondition, $from_date, $end_date, $only_view) :
                            ($type == 'archive' ? $this->onulipiList($condition, $employee_office, $controller, $nothiDecision, $api, 'archive', $orCondition, $from_date, $end_date, $only_view) :
                                ['total' => 0, 'data' => []])
                        ))))
            );
    }

    protected function inboxListBk($conditions, $employee_office, $controller, $nothiDecision, $api, $isSearch = false)
    {
        $start = isset($controller->request->data['start']) ? intval($controller->request->data['start']) : 0;
        $len = isset($controller->request->data['length']) ? intval($controller->request->data['length']) : 10;
        $page = !empty($controller->request->data['page']) ? intval($controller->request->data['page']) : (($start / $len) + 1);

        $dak_attachments_table = TableRegistry::get('DakAttachments');
        $dak_daptorik_table = TableRegistry::get('DakDaptoriks');
        $dak_nagorik_table = TableRegistry::get('DakNagoriks');
        $table_instance_dak_user = TableRegistry::get('DakUsers');

        $dak_daptoriks = array();
        $dak_nagoriks = array();
        $totalRec_daptoriks = array();
        $totalRec_nagoriks = array();
        $all_daks = array();
        $dakIds = array();

        $user_new_daks = $table_instance_dak_user->find()->select(['dak_id', 'dak_view_status',
                                                                   'dak_type'])->where(['dak_category' => DAK_CATEGORY_INBOX,
                                                                                        'to_officer_designation_id' => $employee_office['office_unit_organogram_id'],
                                                                                        'is_archive' => 0])->page($page, $len)->order(['modified DESC'])->toArray();

        $totalRec = $table_instance_dak_user->find()->where(['dak_category' => DAK_CATEGORY_INBOX,
                                                             'to_officer_designation_id' => $employee_office['office_unit_organogram_id'],
                                                             'is_archive' => 0])->count();

        $ng_ids = array();
        $dp_ids = array();
        $unreadDaptorik = array();
        $unreadNagorik = array();
        foreach ($user_new_daks as $new_dak) {
            if ($new_dak['dak_type'] == DAK_DAPTORIK) {
                if ($new_dak['dak_view_status'] == 'New') {
                    $unreadDaptorik[] = $new_dak['dak_id'];
                }
                $dp_ids[] = $new_dak['dak_id'];
            } else if ($new_dak['dak_type'] == DAK_NAGORIK) {
                $ng_ids[] = $new_dak['dak_id'];
                if ($new_dak['dak_view_status'] == 'New') {
                    $unreadNagorik[] = $new_dak['dak_id'];
                }
            }
        }

        $conditions = str_replace('DakNagoriks.', '', $conditions);
        $conditions = str_replace('DakDaptoriks.', '', $conditions);
        $conditions = str_replace('DakMovements.', '', $conditions);
        $conditions = str_replace('DakUsers.', '', $conditions);
        $conditions = str_replace('from_officer_name', 'sender_name', $conditions);
        $conditions = str_replace('dak_priority', 'dak_priority_level', $conditions);
        if (count($dp_ids) > 0) {
            $dak_daptoriks = $dak_daptorik_table->find()->where(['id IN' => $dp_ids])->where([$conditions])->order(['modified DESC']);
            pr($dak_daptoriks);
            die;
        }
        if (count($ng_ids) > 0) {
            $dak_nagoriks = $dak_nagorik_table->find()->where(['id IN' => $ng_ids])->where([$conditions])->order(['modified DESC'])->toArray();
        }

        $all_daks = array_merge($dak_daptoriks, $dak_nagoriks);

        if ($isSearch) {
            if (count($all_daks) < $len && $page <= 1) {
                $totalRec = count($all_daks);
            }
        }

        usort($all_daks,
            function ($a, $b) {
                $time = new Time($a['modified']);
                $a['modified'] = $time->i18nFormat("Y-M-d H:m:s", null, 'en-US');

                $time = new Time($b['modified']);
                $b['modified'] = $time->i18nFormat("Y-M-d H:m:s", null, 'en-US');

                $vala = strtotime($a['modified']);
                $valb = strtotime($b['modified']);
                return ($valb - $vala);
            });

        if ($api) {
            $daks = [];
            if (!empty($all_daks)) {
                foreach ($all_daks as $ke => &$val) {
                    $dak = [];
                    if (isset($val['dak_type_id'])) {
                        $dak['dak_type'] = DAK_NAGORIK;
                        $dak['DakNagoriks'] = $val;
                        if (in_array($val['id'], $unreadNagorik)) {
                            $dak['dak_view_status'] = 'New';
                        } else {
                            $dak['dak_view_status'] = 'View';
                        }
                    } else {
                        $dak['dak_type'] = DAK_DAPTORIK;
                        $dak['DakDaptoriks'] = $val;
                        if (in_array($val['id'], $unreadDaptorik)) {
                            $dak['dak_view_status'] = 'New';
                        } else {
                            $dak['dak_view_status'] = 'View';
                        }
                    }
                    $dak['dak_id'] = $val['id'];
                    $dak['modified'] = $val['modified'];

                    $daks[] = $dak;
                }
            }

            $response = [
                'data' => $daks,
                'total' => $totalRec,
            ];
            return $response;
        }

        $data = [];

        $priority_list = json_decode(DAK_PRIORITY_TYPE, true) + [1 => ''];
        $security_level_list = json_decode(DAK_SECRECY_TYPE, true) + [1 => ''];

        if (!empty($all_daks)) {
            foreach ($all_daks as $ke => $dak) {

                $si = (($page - 1) * $len) + $ke + 1;
                $receiver = "";
                $dak_decision = "";
                $meta_data = !empty($dak['application_meta_data']) ? $dak['application_meta_data'] : '';

                if (isset($dak['dak_type_id'])) {

                    $origin = h($dak['name_bng']);
                    $dak_subject = h($dak['dak_subject']);
                    $security_level = trim($security_level_list[$dak['dak_security_level']]);
                } else {
                    $origin = (!empty($dak['sender_name']) ? (h($dak['sender_name']) . ', ') : '') . (!empty($dak['sender_officer_designation_label'])
                            ? (h($dak['sender_officer_designation_label']) . ", ") : '') . (!empty($dak['sender_office_name'])
                            ? (h($dak['sender_office_name'])) : '');
                    $dak_subject = h($dak['dak_subject']);
                    $security_level = trim($security_level_list[$dak['dak_security_level']]);
                }

                $dak_last_move = $this->getInboxDakLastMoveBy_dakid_attention_type($dak['id'],
                    1, (!isset($dak['dak_type_id']) ? "Daptorik" : "Nagorik"));

                $showDetailsDak = 'showDetailsDakInbox';
                $priority = "";

                if ($dak['dak_priority'] > 1) {
                    $priority = trim($priority_list[$dak['dak_priority']]);
                }

                $move_date = $dak['modified'];

                if (!empty($dak_last_move)) {
                    if ($dak_last_move['dak_priority'] > 1) {
                        $priority = trim($priority_list[$dak_last_move['dak_priority']]);
                    }

                    if ($dak_last_move['dak_security_level'] > 1) {
                        $security_level = $security_level_list[$dak_last_move['dak_security_level']];
                    }

                    $dak_decision = ($dak_last_move['dak_actions']);
                    $receiver = h($dak_last_move['to_officer_name']) . (!empty($dak_last_move['to_officer_designation_label']) ? (", " . h($dak_last_move['to_officer_designation_label'])) : '');
                    $move_date = $dak_last_move['modified'];
                }
                $move_date = new Time($move_date);
                $move_date = $move_date->i18nFormat("dd-MM-yy HH:mm:ss", null, 'bn-BD');

                $totalAttachment = $dak_attachments_table->find()->where(['dak_type' => (!isset($dak['dak_type_id']) ? "Daptorik" : "Nagorik"),
                                                                          'dak_id' => $dak['id']])->count();


                $move_date = str_replace(" ", "</br>", $move_date);

                $data[] = array(
                    '<div class="text-center">' . (($dak_last_move['to_office_id'] == $employee_office['office_id']
                        && $dak_last_move['to_office_unit_id'] == $employee_office['office_unit_id'] && $dak_last_move['to_officer_designation_id']
                        == $employee_office['office_unit_organogram_id'] && $dak_last_move['attention_type']
                        == 1) ? ('<input data-meta=\'' . h($meta_data) . '\' class="dak_list_checkbox_to_select" type="checkbox" title="বাছাই করুন" data-toggle="tooltip" data-placement="left" data-dak-type="' . (!isset($dak['dak_type_id'])
                            ? "Daptorik" : "Nagorik") . '" name="id[]" value="' . $dak['id'] . '">') : '') . "</div>", //1st
                    '<div class="text-center"> ' . (($dak_last_move['to_office_id'] == $employee_office['office_id']
                        && $dak_last_move['to_office_unit_id'] == $employee_office['office_unit_id'] && $dak_last_move['to_officer_designation_id']
                        == $employee_office['office_unit_organogram_id'] && $dak_last_move['attention_type']
                        == 1) ? ('<div class="btn-group btn-group-round"><button title="বিস্তারিত" data-toggle="tooltip" data-placement="top" type="button"  data-dak-type="' . (!isset($dak['dak_type_id'])
                            ? "Daptorik" : "Nagorik") . '" class="btn btn-xs btn-primary ' . $showDetailsDak . '" data-messageid=' . $dak['id'] . ' data-si=' . $si . '>
                        <i class=" a2i_gn_details2" aria-hidden="true"></i>
                    </button><button data-toggle="tooltip" data-placement="top" type="button"  data-dak-type="' . (!isset($dak['dak_type_id'])
                            ? "Daptorik" : "Nagorik") . '" class="btn btn-xs purple forward-message" title="প্রেরণ"><i class="a2i_nt_cholomandak2" aria-hidden="true"></i></button></div><div style="display:none;" class="row dak_sender_cell_list dak-recepiant-seal"></div>')
                        : ('<button  data-dak-type="' . (!isset($dak['dak_type_id']) ? "Daptorik" : "Nagorik") . '" title="আর্কাইভ করুন" data-toggle="tooltip" data-placement="top" type="button" class="btn btn-xs btn-warning deleteThisDak" data-messageid=' . $dak['id'] . ' data-si=' . $si . '>
                        <i class=" efile-official1" aria-hidden="true"></i>
                    </button><button  data-dak-type="' . (!isset($dak['dak_type_id']) ? "Daptorik" : "Nagorik") . '" title="বিস্তারিত" data-toggle="tooltip" data-placement="top" type="button" class="btn btn-xs btn-primary ' . $showDetailsDak . '" data-messageid=' . $dak['id'] . ' data-si=' . $si . '>
                        <i class=" a2i_gn_details2" aria-hidden="true"></i>
                    </button>')) . "</div>", //2nd
                    (!empty($origin) ? h($origin) : ""), //3rd
                    "<b>" . (!empty($dak_subject) ? h($dak_subject) : "") . "</b>", //4th
                    (!empty($receiver) ? h($receiver) : ""), //5th
                    (!empty($dak_decision) ? ($dak_decision) : ""), //6th
                    $move_date,//7th
                    (!empty($priority) ? (h($priority) . ', ') : '') . h($security_level), //8th
                    "<div class='text-center'>" . ($dak['is_summary_nothi'] == 1 ? ("<span title='সার-সংক্ষেপ' data-original-title='সার-সংক্ষেপ' data-title='সার-সংক্ষেপ' class='glyphicon glyphicon-book'></span>")
                        : (!isset($dak['dak_type_id']) ? "<span title='দাপ্তরিক ডাক' data-original-title='দাপ্তরিক ডাক' data-title='দাপ্তরিক ডাক' class='glyphicon glyphicon-home'></span>"
                            : "<span title='নাগরিক ডাক' data-original-title='নাগরিক ডাক' data-title='নাগরিক ডাক' class='glyphicon glyphicon-user'></span>")) . "</div>" . (!empty($dak['application_origin']) && $dak['application_origin'] != 'nothi' ? ("<div class='text-center'>(" . strtoupper($dak['application_origin'])[0] . ")</div>") : ''),
                    //9th
                    (isset($totalAttachment) && $totalAttachment > 1) ? ('<i class="fa fa-paperclip" data-toggle="tooltip" data-placement="left"> <span class="badge badge-success">' . Number::format($totalAttachment
                            - 1) . '</span></i>') : '<div class="text-center">নাই</div>',
                    "DT_RowClass" => (!isset($dak['dak_type_id']) ? (in_array($dak['id'],
                        $unreadDaptorik) ? ($dak['is_summary_nothi'] == 1 ? 'summaryList' : 'new')
                        : '') : (in_array($dak['id'], $unreadNagorik) ? ($dak['is_summary_nothi']
                    == 1 ? 'summaryList' : 'new') : ''))//10th
                );
            }
        }


        $response = [
            'data' => $data,
            'total' => $totalRec,
        ];

        return $response;

    }

    protected function inboxList($conditions, $employee_office, $controller, $nothiDecision, $api, $isSearch = false, $orCondition, $from_date = '', $end_date = '', $only_view)
    {

        $start = isset($controller->request->data['start']) ? intval($controller->request->data['start']) : 0;
        $len = isset($controller->request->data['length']) ? intval($controller->request->data['length']) : 10;
        $page = !empty($controller->request->data['page']) ? intval($controller->request->data['page']) : (($start / $len) + 1);

        $dak_attachments_table = TableRegistry::get('DakAttachments');
        $dak_daptorik_table = TableRegistry::get('DakDaptoriks');
        $dak_nagorik_table = TableRegistry::get('DakNagoriks');
        $table_instance_dak_user = TableRegistry::get('DakUsers');

        $dak_daptoriks = array();
        $dak_nagoriks = array();
        $totalRec_daptoriks = array();
        $totalRec_nagoriks = array();
        $all_daks = array();
        $dakIds = array();

        $user_new_daks = $table_instance_dak_user->find()->select(['dak_id', 'dak_view_status',
                                                                   'dak_type'])->where(['dak_category' => DAK_CATEGORY_INBOX,
                                                                                        'to_officer_designation_id' => $employee_office['office_unit_organogram_id'],
                                                                                        'is_archive' => 0])->page($page, $len)->order(['modified DESC'])->toArray();

        $totalRec = $table_instance_dak_user->find()->where(['dak_category' => DAK_CATEGORY_INBOX,
                                                             'to_officer_designation_id' => $employee_office['office_unit_organogram_id'],
                                                             'is_archive' => 0])->count();

        $ng_ids = array();
        $dp_ids = array();
        $unreadDaptorik = array();
        $unreadNagorik = array();
        foreach ($user_new_daks as $new_dak) {
            if ($new_dak['dak_type'] == DAK_DAPTORIK) {
                if ($new_dak['dak_view_status'] == 'New') {
                    $unreadDaptorik[] = $new_dak['dak_id'];
                }
                $dp_ids[] = $new_dak['dak_id'];
            } else if ($new_dak['dak_type'] == DAK_NAGORIK) {
                $ng_ids[] = $new_dak['dak_id'];
                if ($new_dak['dak_view_status'] == 'New') {
                    $unreadNagorik[] = $new_dak['dak_id'];
                }
            }
        }

        if (count($dp_ids) > 0) {
            // get needed columns name so that no extra columns needed
            $daptorik_columns = $this->getDakDaptoriksColumnsSetting();
            $dak_daptoriks = $dak_daptorik_table->find()->select($daptorik_columns['needed'])->where($conditions)->orWhere($orCondition)->where(['id IN' => $dp_ids]);
            if (!empty($from_date) && !empty($end_date)) {
                $dak_daptoriks = $dak_daptoriks->where(function ($dak_daptoriks) use ($from_date, $end_date) {
                    return $dak_daptoriks->between('date(modified)', $from_date, $end_date);
                });
            };
            $dak_daptoriks = $dak_daptoriks->order(['modified DESC'])->toArray();

        }
        if (count($ng_ids) > 0) {
            // get needed columns name so that no extra columns needed
            $nagorik_columns = $this->getDakNagoriksColumnsSetting();
            $dak_nagoriks = $dak_nagorik_table->find()->select($nagorik_columns['needed'])->where($conditions)->orWhere($orCondition)->where(['id IN' => $ng_ids]);
            if (!empty($from_date) && !empty($end_date)) {
                $dak_nagoriks = $dak_nagoriks->where(function ($dak_nagoriks) use ($from_date, $end_date) {
                    return $dak_nagoriks->between('date(modified)', $from_date, $end_date);
                });
            };

            $dak_nagoriks = $dak_nagoriks->order(['modified DESC'])->toArray();

        }

        $all_daks = array_merge($dak_daptoriks, $dak_nagoriks);

        if ($isSearch) {
            if (count($all_daks) < $len && $page <= 1) {
                $totalRec = count($all_daks);
            }
        }

        usort($all_daks,
            function ($a, $b) {
                $time = new Time($a['modified']);
                $a['modified'] = $time->i18nFormat("Y-M-d H:m:s", null, 'en-US');

                $time = new Time($b['modified']);
                $b['modified'] = $time->i18nFormat("Y-M-d H:m:s", null, 'en-US');

                $vala = strtotime($a['modified']);
                $valb = strtotime($b['modified']);
                return ($valb - $vala);
            });

        if ($api) {
            $daks = [];
            if (!empty($all_daks)) {
                foreach ($all_daks as $ke => &$val) {
                    $dak = [];
                    if (isset($val['dak_type_id'])) {
                        $dak['dak_type'] = DAK_NAGORIK;
                        $dak['DakNagoriks'] = $val;
                        // add skipped columns for mobile app as it is caching columns - 2630
                        if (!empty($nagorik_columns['notNeeded'])) {
                            foreach ($nagorik_columns['notNeeded'] as $not_needed_columns) {
                                $dak['DakNagoriks'][$not_needed_columns] = '';
                            }
                        }
                        // add skipped columns for mobile app as it is caching columns - 2630

                        if (in_array($val['id'], $unreadNagorik)) {
                            $dak['dak_view_status'] = 'New';
                        } else {
                            $dak['dak_view_status'] = 'View';
                        }
                        $dak_last_move = $this->getInboxDakLastMoveBy_dakid_attention_type($val['id'], 1, 'Nagorik');
                    } else {
                        $dak['dak_type'] = DAK_DAPTORIK;
                        $dak['DakDaptoriks'] = $val;
                        // add skipped columns for mobile app as it is caching columns - 2630
                        if (!empty($daptorik_columns['notNeeded'])) {
                            foreach ($daptorik_columns['notNeeded'] as $not_needed_columns) {
                                $dak['DakDaptoriks'][$not_needed_columns] = '';
                            }
                        }
                        // add skipped columns for mobile app as it is caching columns - 2630

                        if (in_array($val['id'], $unreadDaptorik)) {
                            $dak['dak_view_status'] = 'New';
                        } else {
                            $dak['dak_view_status'] = 'View';
                        }
                        $dak_last_move = $this->getInboxDakLastMoveBy_dakid_attention_type($val['id'], 1, 'Daptorik');
                    }
                    $val['dak_priority_level'] = (string) $dak_last_move['dak_priority']; //intiger value convert to string for mobile application
                    $dak['dak_id'] = $val['id'];
                    $dak['modified'] = $val['modified'];

                    $daks[] = $dak;
                }
            }

            $response = [
                'data' => $daks,
                'total' => $totalRec,
            ];
            return $response;
        }

        $data = [];

        $priority_list = json_decode(DAK_PRIORITY_TYPE, true) + [0 => '', 1 => ''];
        $security_level_list = json_decode(DAK_SECRECY_TYPE, true) + [0 => '', 1 => ''];

        if (!empty($all_daks)) {
            foreach ($all_daks as $ke => $dak) {

                $si = (($page - 1) * $len) + $ke + 1;
                $receiver = "";
                $dak_decision = "";
                $meta_data = !empty($dak['application_meta_data']) ? $dak['application_meta_data'] : '';

                if (isset($dak['dak_type_id'])) {

                    $origin = h($dak['name_bng']);
                    $dak_subject = ($dak['dak_subject']);
                    $security_level = trim($security_level_list[$dak['dak_security_level']]);
                } else {
                    $origin = (!empty($dak['sender_name']) ? (h($dak['sender_name']) . ', ') : '') . (!empty($dak['sender_officer_designation_label'])
                            ? (h($dak['sender_officer_designation_label']) . ", ") : '') . (!empty($dak['sender_office_name'])
                            ? (h($dak['sender_office_name'])) : '');
                    $dak_subject = ($dak['dak_subject']);
                    $security_level = trim($security_level_list[$dak['dak_security_level']]);
                }

                $dak_last_move = $this->getInboxDakLastMoveBy_dakid_attention_type($dak['id'],
                    1, (!isset($dak['dak_type_id']) ? "Daptorik" : "Nagorik"));

                $showDetailsDak = 'showDetailsDakInbox';
                $priority = "";

                if ($dak['dak_priority'] > 1) {
                    $priority = trim($priority_list[$dak['dak_priority']]);
                }

                $move_date = $dak['modified'];

                if (!empty($dak_last_move)) {
                    if ($dak_last_move['dak_priority'] > 1) {
                        $priority = trim($priority_list[$dak_last_move['dak_priority']]);
                    }

                    if ($dak_last_move['dak_security_level'] > 1) {
                        $security_level = $security_level_list[$dak_last_move['dak_security_level']];
                    }

                    $dak_decision = ($dak_last_move['dak_actions']);
                    $receiver = h($dak_last_move['to_officer_name']) . (!empty($dak_last_move['to_officer_designation_label']) ? (", " . h($dak_last_move['to_officer_designation_label'])) : '');
                    $move_date = $dak_last_move['modified'];
                }
                $move_date = new Time($move_date);
                $move_date = $move_date->i18nFormat("dd-MM-yy HH:mm:ss", null, 'bn-BD');

                $totalAttachment = $dak_attachments_table->find()->where(['dak_type' => (!isset($dak['dak_type_id']) ? "Daptorik" : "Nagorik"),
                                                                          'dak_id' => $dak['id']])->count();

                $attachment_count_bn = "";
                if (isset($totalAttachment) && $totalAttachment > 1) {
                    $attachment_count_bn = "&nbsp;<span class='badge badge-primary'><i class='fa fa-paperclip'></i> " . enTobn($totalAttachment - 1) . "</span>";
                }

                $move_date = str_replace(" ", "</br>", $move_date);

                if ($only_view) {
                    $data[] = array(
                        '<div class="text-center"> ' . (($dak_last_move['to_office_id'] == $employee_office['office_id']
                            && $dak_last_move['to_office_unit_id'] == $employee_office['office_unit_id'] && $dak_last_move['to_officer_designation_id']
                            == $employee_office['office_unit_organogram_id'] && $dak_last_move['attention_type']
                            == 1) ? ('<div class="btn-group btn-group-round"><button title="বিস্তারিত" data-toggle="tooltip" data-placement="top" type="button"  data-dak-type="' . (!isset($dak['dak_type_id'])
                                ? "Daptorik" : "Nagorik") . '" class="btn btn-xs btn-primary ' . $showDetailsDak . '" data-messageid=' . $dak['id'] . ' data-si=' . $si . '>
                        <i class=" a2i_gn_details2" aria-hidden="true"></i>
                    </button></div><div style="display:none;" class="row dak_sender_cell_list dak-recepiant-seal"></div>')
                            : ('<button  data-dak-type="' . (!isset($dak['dak_type_id']) ? "Daptorik" : "Nagorik") . '" title="বিস্তারিত" data-toggle="tooltip" data-placement="top" type="button" class="btn btn-xs btn-primary ' . $showDetailsDak . '" data-messageid=' . $dak['id'] . ' data-si=' . $si . '>
                        <i class=" a2i_gn_details2" aria-hidden="true"></i>
                    </button>')) . "</div>", //2nd
                        (!empty($origin) ? h($origin) : ""), //3rd
                        "<b>" . (!empty($dak_subject) ? h($dak_subject) : "") . "</b>" . $attachment_count_bn, //4th
                        (!empty($receiver) ? h($receiver) : ""), //5th
                        (!empty($dak_decision) ? ($dak_decision) : ""), //6th
                        $move_date,//7th
                        (!empty($priority) ? (h($priority) . ', ') : '') . h($security_level), //8th
                        "<div class='text-center'>" . ($dak['is_summary_nothi'] == 1 ? ("<span title='সার-সংক্ষেপ' data-original-title='সার-সংক্ষেপ' data-title='সার-সংক্ষেপ' class='glyphicon glyphicon-book'></span>") : (!isset($dak['dak_type_id']) ? (($dak['dak_sending_media'] == '1') ? "<span title='দাপ্তরিক ডাক' data-original-title='দাপ্তরিক ডাক' data-title='দাপ্তরিক ডাক' class='fa fa-building-o'></span>" : "<span title='আপলোডকৃত দাপ্তরিক ডাক' data-original-title='আপলোডকৃত দাপ্তরিক ডাক' data-title='আপলোডকৃত দাপ্তরিক ডাক' class='fa fa-building'></span>") : (($dak['dak_sending_media'] == '1') ? "<span title='নাগরিক ডাক' data-original-title='নাগরিক ডাক' data-title='নাগরিক ডাক' class='fa fa-user-o'></span>" : "<span title='আপলোডকৃত নাগরিক ডাক' data-original-title='আপলোডকৃত নাগরিক ডাক' data-title='আপলোডকৃত নাগরিক ডাক' class='fa fa-user'></span>"))) . "</div>" . (!empty($dak['application_origin']) && $dak['application_origin'] != 'nothi' ? ("<div class='text-center'>(" . strtoupper($dak['application_origin'])[0] . ")</div>") : ''),
                        //9th
                        "DT_RowClass" => (!isset($dak['dak_type_id']) ? (in_array($dak['id'],
                            $unreadDaptorik) ? ($dak['is_summary_nothi'] == 1 ? 'summaryList' : 'new')
                            : '') : (in_array($dak['id'], $unreadNagorik) ? ($dak['is_summary_nothi']
                        == 1 ? 'summaryList' : 'new') : ''))//10th
                    );
                } else {
                    $data[] = array(
                        '<div class="text-center">' . (($dak_last_move['to_office_id'] == $employee_office['office_id']
                            && $dak_last_move['to_office_unit_id'] == $employee_office['office_unit_id'] && $dak_last_move['to_officer_designation_id']
                            == $employee_office['office_unit_organogram_id'] && $dak_last_move['attention_type']
                            == 1) ? ('<input data-meta=\'' . h($meta_data) . '\' class="dak_list_checkbox_to_select" type="checkbox" title="বাছাই করুন" data-toggle="tooltip" data-placement="left" data-dak-type="' . (!isset($dak['dak_type_id'])
                                ? "Daptorik" : "Nagorik") . '" name="id[]" value="' . $dak['id'] . '">') : '') . "</div>", //1st
                        '<div class="text-center"> ' . (($dak_last_move['to_office_id'] == $employee_office['office_id']
                            && $dak_last_move['to_office_unit_id'] == $employee_office['office_unit_id'] && $dak_last_move['to_officer_designation_id']
                            == $employee_office['office_unit_organogram_id'] && $dak_last_move['attention_type']
                            == 1) ? ('<div class="btn-group btn-group-round"><button title="বিস্তারিত" data-toggle="tooltip" data-placement="top" type="button"  data-dak-type="' . (!isset($dak['dak_type_id'])
                                ? "Daptorik" : "Nagorik") . '" class="btn btn-xs btn-primary ' . $showDetailsDak . '" data-messageid=' . $dak['id'] . ' data-si=' . $si . '>
                        <i class=" a2i_gn_details2" aria-hidden="true"></i>
                    </button><button data-toggle="tooltip" data-placement="top" type="button"  data-dak-type="' . (!isset($dak['dak_type_id'])
                                ? "Daptorik" : "Nagorik") . '" class="btn btn-xs purple forward-message" title="প্রেরণ"><i class="a2i_nt_cholomandak2" aria-hidden="true"></i></button></div><div style="display:none;" class="row dak_sender_cell_list dak-recepiant-seal"></div>')
                            : ('<div class="btn-group btn-group-round"><button  data-dak-type="' . (!isset($dak['dak_type_id']) ? "Daptorik" : "Nagorik") . '" title="আর্কাইভ করুন" data-toggle="tooltip" data-placement="top" type="button" class="btn btn-xs btn-warning deleteThisDak" data-messageid=' . $dak['id'] . ' data-si=' . $si . '>
                        <i class=" efile-official1" aria-hidden="true"></i>
                    </button><button  data-dak-type="' . (!isset($dak['dak_type_id']) ? "Daptorik" : "Nagorik") . '" title="বিস্তারিত" data-toggle="tooltip" data-placement="top" type="button" class="btn btn-xs btn-primary ' . $showDetailsDak . '" data-messageid=' . $dak['id'] . ' data-si=' . $si . '>
                        <i class=" a2i_gn_details2" aria-hidden="true"></i>
                    </button></div>')) . "</div>", //2nd
                        (!empty($origin) ? h($origin) : ""), //3rd
                        "<b>" . (!empty($dak_subject) ? h($dak_subject) : "") . "</b>" . $attachment_count_bn, //4th
                        (!empty($receiver) ? h($receiver) : ""), //5th
                        (!empty($dak_decision) ? ($dak_decision) : ""), //6th
                        $move_date,//7th
                        (!empty($priority) ? (h($priority) . ', ') : '') . h($security_level), //8th
                        "<div class='text-center'>" . ($dak['is_summary_nothi'] == 1 ? ("<span title='সার-সংক্ষেপ' data-original-title='সার-সংক্ষেপ' data-title='সার-সংক্ষেপ' class='glyphicon glyphicon-book'></span>") : (!isset($dak['dak_type_id']) ? (($dak['dak_sending_media'] == '1') ? "<span title='দাপ্তরিক ডাক' data-original-title='দাপ্তরিক ডাক' data-title='দাপ্তরিক ডাক' class='fa fa-building-o'></span>" : "<span title='আপলোডকৃত দাপ্তরিক ডাক' data-original-title='আপলোডকৃত দাপ্তরিক ডাক' data-title='আপলোডকৃত দাপ্তরিক ডাক' class='fa fa-building'></span>") : (($dak['dak_sending_media'] == '1') ? "<span title='নাগরিক ডাক' data-original-title='নাগরিক ডাক' data-title='নাগরিক ডাক' class='fa fa-user-o'></span>" : "<span title='আপলোডকৃত নাগরিক ডাক' data-original-title='আপলোডকৃত নাগরিক ডাক' data-title='আপলোডকৃত নাগরিক ডাক' class='fa fa-user'></span>"))) . "</div>" . (!empty($dak['application_origin']) && $dak['application_origin'] != 'nothi' ? ("<div class='text-center'>(" . strtoupper($dak['application_origin'])[0] . ")</div>") : ''),
                        //9th
                        "DT_RowClass" => (!isset($dak['dak_type_id']) ? (in_array($dak['id'],
                            $unreadDaptorik) ? ($dak['is_summary_nothi'] == 1 ? 'summaryList' : 'new')
                            : '') : (in_array($dak['id'], $unreadNagorik) ? ($dak['is_summary_nothi']
                        == 1 ? 'summaryList' : 'new') : ''))//10th
                    );
                }
            }
        }


        $response = [
            'data' => $data,
            'total' => $totalRec,
        ];

        return $response;

    }

    protected function sentListBk($conditions, $employee_office, $controller, $nothiDecision, $api)
    {

        $start = isset($controller->request->data['start']) ? intval($controller->request->data['start']) : 0;
        $len = isset($controller->request->data['length']) ? intval($controller->request->data['length']) : 10;
        $page = !empty($controller->request->data['page']) ? intval($controller->request->data['page']) : (($start / $len) + 1);

        $dak_attachments_table = TableRegistry::get('DakAttachments');

        $user_new_daks = $this->find()->select([

            "DakNagoriks.id",
            "DakDaptoriks.id",
            "DakNagoriks.docketing_no",
            "DakDaptoriks.docketing_no",
            "DakDaptoriks.sender_sarok_no",
            "DakDaptoriks.receiving_officer_name",
            "DakDaptoriks.receiving_officer_designation_label",
            "DakDaptoriks.receiving_office_unit_name",

            "DakNagoriks.dak_type_id",
            "DakNagoriks.sender_name",
            "DakNagoriks.name_bng",
            "DakNagoriks.dak_subject",
            "DakNagoriks.dak_priority_level",
            "DakNagoriks.dak_security_level",
            "DakNagoriks.receiving_officer_name",
            "DakNagoriks.receiving_officer_designation_label",
            "DakNagoriks.receiving_office_unit_name",
            "DakNagoriks.application_origin",

            "DakDaptoriks.dak_priority_level",
            "DakDaptoriks.dak_security_level",
            "DakDaptoriks.dak_subject",
            "DakDaptoriks.sender_name",
            "DakDaptoriks.sender_officer_designation_label",
            "DakDaptoriks.sender_office_name",
            "DakDaptoriks.is_summary_nothi",
            "dak_id" => 'DakMovements.dak_id',
            "modified" => 'DakMovements.modified',
            "dak_actions" => 'DakMovements.dak_actions',
            "dak_priority" => 'DakMovements.dak_priority',
            "attention_type" => 'DakMovements.attention_type',
            "dak_type" => 'DakMovements.dak_type', 'DakMovements.sequence',
            'DakMovements.to_officer_designation_id',
            'DakMovements.operation_type',
        ])
                              ->join([
                                  'DakMovements2' => [
                                      'table' => 'dak_movements',
                                      'type' => 'left',
                                      'conditions' => 'DakMovements.dak_id=DakMovements2.dak_id and DakMovements.dak_type=DakMovements2.dak_type and DakMovements.id < DakMovements2.id and DakMovements.from_officer_designation_id = DakMovements2.from_officer_designation_id'
                                  ],
                                  'DakDaptoriks' => [
                                      'table' => 'dak_daptoriks',
                                      'type' => 'left',
                                      'conditions' => 'DakMovements.dak_id=DakDaptoriks.id and DakMovements.dak_type="Daptorik"'
                                  ],
                                  'DakNagoriks' => [
                                      'table' => 'dak_nagoriks',
                                      'type' => 'left',
                                      'conditions' => 'DakMovements.dak_id=DakNagoriks.id and DakMovements.dak_type="Nagorik"'
                                  ],
                              ])
                              ->where(['DakMovements2.id IS NULL'])
                              ->where(['DakMovements.sequence >=' => 2])
                              ->where(['DakMovements.from_officer_designation_id' =>
                                           $employee_office['office_unit_organogram_id']])
                              ->where(['OR' => [['DakMovements.operation_type' => DAK_CATEGORY_FORWARD],
                                                ['DakMovements.operation_type' => DAK_CATEGORY_NOTHIVUKTO],
                                                ['DakMovements.operation_type' => DAK_CATEGORY_NOTHIJATO]]])
                              ->where($conditions)
                              ->page($page, $len)->order(['DakMovements.modified DESC'])->toArray();

        $totalRec = $this->find()->select(["dak_id" => 'DakMovements.dak_id', "dak_type" => 'DakMovements.dak_type'])->join([
            'DakMovements2' => [
                'table' => 'dak_movements',
                'type' => 'left',
                'conditions' => 'DakMovements.dak_id=DakMovements2.dak_id and DakMovements.dak_type=DakMovements2.dak_type and DakMovements.modified < DakMovements2.modified  and DakMovements.from_officer_designation_id = DakMovements2.from_officer_designation_id'
            ],
            'DakDaptoriks' => [
                'table' => 'dak_daptoriks',
                'type' => 'left',
                'conditions' => 'DakMovements.dak_id=DakDaptoriks.id and DakMovements.dak_type="Daptorik"'
            ],
            'DakNagoriks' => [
                'table' => 'dak_nagoriks',
                'type' => 'left',
                'conditions' => 'DakMovements.dak_id=DakNagoriks.id and DakMovements.dak_type="Nagorik"'
            ],
        ])->where(['DakMovements2.id IS NULL'])->where(['DakMovements.from_officer_designation_id' => $employee_office['office_unit_organogram_id']])->where(['OR' => [['DakMovements.operation_type' => DAK_CATEGORY_FORWARD], ['DakMovements.operation_type' => DAK_CATEGORY_NOTHIVUKTO],
                                                                                                                                                                       ['DakMovements.operation_type' => DAK_CATEGORY_NOTHIJATO]]])->where($conditions)->order(['DakMovements.modified DESC'])->count();


        if ($api) {
            $response = [
                'data' => $user_new_daks,
                'total' => $totalRec,
            ];
            return $response;
        }
        $data = array();

        $priority_list = json_decode(DAK_PRIORITY_TYPE, true) + [1 => ''];
        $security_level_list = json_decode(DAK_SECRECY_TYPE, true) + [1 => ''];

        if (!empty($user_new_daks)) {
            foreach ($user_new_daks as $ke => $dak) {
                $si = (($page - 1) * $len) + $ke + 1;

                if ($dak['dak_type'] == 'Nagorik') {
                    $origin = h($dak['DakNagoriks']['name_bng']);
                    $dak_subject = h($dak['DakNagoriks']['dak_subject']);
                    $previous_sender = $origin;
                    $security_level = trim(h($security_level_list[$dak['DakNagoriks']['dak_security_level']]));
                } else {
                    $origin = (!empty($dak['DakDaptoriks']['sender_name']) ? (h($dak['DakDaptoriks']['sender_name']) . ', ') : '') . (!empty($dak['DakDaptoriks']['sender_officer_designation_label'])
                            ? (h($dak['DakDaptoriks']['sender_officer_designation_label']) . ", ") : '') . (!empty($dak['DakDaptoriks']['sender_office_name'])
                            ? (h($dak['DakDaptoriks']['sender_office_name'])) : '');
                    $dak_subject = h($dak['DakDaptoriks']['dak_subject']);
                    $previous_sender = $origin;
                    $security_level = !empty($security_level_list[$dak['DakDaptoriks']['dak_security_level']]) ? trim(($security_level_list[$dak['DakDaptoriks']['dak_security_level']])) : '';
                }

                $next_receiver = "";
                $priority = '';
                $security_level = '';
                $move_date = $dak['modified'];
                $move_date = new Time($move_date);
                $move_date = $move_date->i18nFormat("dd-MM-yy HH:mm:ss", null, 'bn-BD');

                $dak_last_two_moves = $this->getDakLastMoveBy_dakid_officerId_designationId($dak['dak_id'],
                    $employee_office['officer_id'], $employee_office['office_unit_organogram_id'], $dak['dak_type']);
                $showDetailsDak = 'showDetailsDakSent';

                if ($dak['dak_priority'] > 1) {
                    $priority = trim($priority_list[$dak['dak_priority']]);
                }
                if (count($dak_last_two_moves) > 0) {
                    if (!empty($dak_last_two_moves[1])) {
                        $first_move = $dak_last_two_moves[1];

                        $previous_sender = (!empty($first_move['from_officer_name']) ? (h($first_move['from_officer_name']) . ', ')
                                : '') . h($first_move['from_officer_designation_label']);
                    }
                    if (!empty($dak_last_two_moves[0])) {
                        $second_move = $dak_last_two_moves[0];
                        $next_receiver = (!empty($second_move['to_officer_name']) ? (h($second_move['to_officer_name']) . ", ")
                                : '') . h($second_move['to_officer_designation_label']) . " (" . h($second_move['to_office_unit_name']) . " )";
                    }
                }
                $dak_decision = $dak['dak_actions'];
                $totalAttachment = $dak_attachments_table->find()->where(['dak_type' => $dak['dak_type'], 'dak_id' => $dak['dak_id']])->count();

                $data[] = array(
                    '<div class="text-center">
                        <button title="বিস্তারিত"  data-dak-type="' . (h($dak['dak_type'])) . '" data-toggle="tooltip" data-placement="top" type="button" class="btn btn-xs btn-primary ' . $showDetailsDak . '" data-messageid=' . $dak['dak_id'] . '  data-si=' . $si . ' >
                        <i class=" a2i_gn_details2" aria-hidden="true"></i>
                        </button>
                    </div>',
                    h($origin),
                    "<b>" . (!empty($dak_subject) ? h($dak_subject) : "") . "</b>",
                    h($next_receiver),
                    ($dak_decision),
                    str_replace(" ", "<br/>", $move_date),
                    str_replace(" ", "<br/>", (h($priority) . ' ' . h($security_level))),
                    "<div class='text-center'>" . (!empty($dak['DakDaptoriks']['is_summary_nothi']) ? ("<span title='সার-সংক্ষেপ' data-original-title='সার-সংক্ষেপ' data-title='সার-সংক্ষেপ' class='glyphicon glyphicon-book'></span>")
                        : ($dak['dak_type'] != DAK_NAGORIK ? "<span title='দাপ্তরিক ডাক' data-original-title='দাপ্তরিক ডাক' data-title='দাপ্তরিক ডাক' class='glyphicon glyphicon-home'></span>"
                            : "<span title='নাগরিক ডাক' data-original-title='নাগরিক ডাক' data-title='নাগরিক ডাক' class='glyphicon glyphicon-user'></span>")) . "</div>" . (!empty($dak['DakNagoriks']['application_origin']) && $dak['DakNagoriks']['application_origin'] != 'nothi' ? ("<div class='text-center'>(" . strtoupper($dak['DakNagoriks']['application_origin'])[0] . ")</div>") : ''),
                    (isset($totalAttachment) && $totalAttachment > 1) ? '<i class="fa fa-paperclip" data-toggle="tooltip" data-placement="left" > <span class="badge badge-success">' . $this->number($totalAttachment
                            - 1) . '</span></i>' : '<div class="text-center">নাই</div>'
                );
            }
        }

        $response = [
            'data' => $data,
            'total' => $totalRec,
        ];

        return $response;
    }

    protected function sentList($conditions, $employee_office, $controller, $nothiDecision, $api, $orConditions, $from_date = '', $end_date = '', $only_view)
    {

        $start = isset($controller->request->data['start']) ? intval($controller->request->data['start']) : 0;
        $len = isset($controller->request->data['length']) ? intval($controller->request->data['length']) : 10;
        $page = !empty($controller->request->data['page']) ? intval($controller->request->data['page']) : (($start / $len) + 1);

        $dak_attachments_table = TableRegistry::get('DakAttachments');

        $user_new_daks = $this->find()->select([

            "DakNagoriks.id",
            "DakDaptoriks.id",
            "DakNagoriks.docketing_no",
            "DakDaptoriks.docketing_no",
            "DakDaptoriks.sender_sarok_no",
            "DakDaptoriks.receiving_officer_name",
            "DakDaptoriks.receiving_officer_designation_label",
            "DakDaptoriks.receiving_office_unit_name",

            "DakNagoriks.dak_type_id",
            "DakNagoriks.sender_name",
            "DakNagoriks.name_bng",
            "DakNagoriks.dak_subject",
            "DakNagoriks.dak_priority_level",
            "DakNagoriks.dak_security_level",
            "DakNagoriks.receiving_officer_name",
            "DakNagoriks.receiving_officer_designation_label",
            "DakNagoriks.receiving_office_unit_name",
            "DakNagoriks.application_origin",

            "DakDaptoriks.dak_priority_level",
            "DakDaptoriks.dak_security_level",
            "DakDaptoriks.dak_subject",
            "DakDaptoriks.sender_name",
            "DakDaptoriks.sender_officer_designation_label",
            "DakDaptoriks.sender_office_name",
            "DakDaptoriks.is_summary_nothi",
            "dak_id" => 'DakMovements.dak_id',
            "modified" => 'DakMovements.modified',
            "dak_actions" => 'DakMovements.dak_actions',
            "dak_priority" => 'DakMovements.dak_priority',
            "attention_type" => 'DakMovements.attention_type',
            "dak_type" => 'DakMovements.dak_type', 'DakMovements.sequence',
            'DakMovements.to_officer_designation_id',
            'DakMovements.operation_type',
        ])
                              ->join([
                                  'DakMovements2' => [
                                      'table' => 'dak_movements',
                                      'type' => 'left',
                                      'conditions' => 'DakMovements.dak_id=DakMovements2.dak_id and DakMovements.dak_type=DakMovements2.dak_type and DakMovements.id < DakMovements2.id and DakMovements.from_officer_designation_id = DakMovements2.from_officer_designation_id'
                                  ],
                                  'DakDaptoriks' => [
                                      'table' => 'dak_daptoriks',
                                      'type' => 'left',
                                      'conditions' => 'DakMovements.dak_id=DakDaptoriks.id and DakMovements.dak_type="Daptorik"'
                                  ],
                                  'DakNagoriks' => [
                                      'table' => 'dak_nagoriks',
                                      'type' => 'left',
                                      'conditions' => 'DakMovements.dak_id=DakNagoriks.id and DakMovements.dak_type="Nagorik"'
                                  ],
                              ])
                              ->where($conditions)
                              ->orWhere($orConditions)
                              ->where(['DakMovements2.id IS NULL'])
                              ->where(['DakMovements.sequence >=' => 2])
                              ->where(['DakMovements.from_officer_designation_id' =>
                                           $employee_office['office_unit_organogram_id']])
                              ->where(['OR' => [['DakMovements.operation_type' => DAK_CATEGORY_FORWARD],
                                                ['DakMovements.operation_type' => DAK_CATEGORY_NOTHIVUKTO],
                                                ['DakMovements.operation_type' => DAK_CATEGORY_NOTHIJATO]]]);

        if (!empty($from_date) && !empty($end_date)) {
            $user_new_daks = $user_new_daks->where(function ($user_new_daks) use ($from_date, $end_date) {
                return $user_new_daks->between('date(DakMovements.modified)', $from_date, $end_date);
            });
        };
        $user_new_daks = $user_new_daks->page($page, $len)->order(['DakMovements.modified DESC'])->toArray();
        foreach ($user_new_daks as $key => &$user_new_dak) {
            if ($user_new_dak['DakDaptoriks']['id'] != null) {
                $dak_last_move = $this->getInboxDakLastMoveBy_dakid_attention_type($user_new_dak['DakDaptoriks']['id'], 1, 'Daptorik');
                $user_new_dak['DakDaptoriks']['dak_priority_level'] = (string) $dak_last_move['dak_priority']; //intiger value convert to string for mobile application
            } else {
                $dak_last_move = $this->getInboxDakLastMoveBy_dakid_attention_type($user_new_dak['DakNagoriks']['id'], 1, 'Nagorik');
                $user_new_dak['DakNagoriks']['dak_priority_level'] = (string) $dak_last_move['dak_priority']; //intiger value convert to string for mobile application
            }
        }

        $totalRec = $this->find()->select(["dak_id" => 'DakMovements.dak_id', "dak_type" => 'DakMovements.dak_type'])->join([
            'DakMovements2' => [
                'table' => 'dak_movements',
                'type' => 'left',
                'conditions' => 'DakMovements.dak_id=DakMovements2.dak_id and DakMovements.dak_type=DakMovements2.dak_type and DakMovements.modified < DakMovements2.modified  and DakMovements.from_officer_designation_id = DakMovements2.from_officer_designation_id'
            ],
            'DakDaptoriks' => [
                'table' => 'dak_daptoriks',
                'type' => 'left',
                'conditions' => 'DakMovements.dak_id=DakDaptoriks.id and DakMovements.dak_type="Daptorik"'
            ],
            'DakNagoriks' => [
                'table' => 'dak_nagoriks',
                'type' => 'left',
                'conditions' => 'DakMovements.dak_id=DakNagoriks.id and DakMovements.dak_type="Nagorik"'
            ],
        ])->where($conditions)->orWhere($orConditions)->where(['DakMovements2.id IS NULL'])->where(['DakMovements.from_officer_designation_id' => $employee_office['office_unit_organogram_id']])->where(['OR' => [['DakMovements.operation_type' => DAK_CATEGORY_FORWARD], ['DakMovements.operation_type' => DAK_CATEGORY_NOTHIVUKTO],
                                                                                                                                                                                                                   ['DakMovements.operation_type' => DAK_CATEGORY_NOTHIJATO]]]);
        if (!empty($from_date) && !empty($end_date)) {
            $totalRec = $totalRec->where(function ($totalRec) use ($from_date, $end_date) {
                return $totalRec->between('date(DakMovements.modified)', $from_date, $end_date);
            });
        }
        $totalRec = $totalRec->order(['DakMovements.modified DESC'])->count();


        if ($api) {
            $response = [
                'data' => $user_new_daks,
                'total' => $totalRec,
            ];
            return $response;
        }
        $data = array();

        $priority_list = json_decode(DAK_PRIORITY_TYPE, true) + [0 => '', 1 => ''];
        $security_level_list = json_decode(DAK_SECRECY_TYPE, true) + [0 => '', 1 => ''];

        if (!empty($user_new_daks)) {
            foreach ($user_new_daks as $ke => $dak) {
                $si = (($page - 1) * $len) + $ke + 1;

                if ($dak['dak_type'] == 'Nagorik') {
                    $origin = h($dak['DakNagoriks']['name_bng']);
                    $dak_subject = ($dak['DakNagoriks']['dak_subject']);
                    $previous_sender = $origin;
                    $security_level = trim(h($security_level_list[$dak['DakNagoriks']['dak_security_level']]));
                } else {
                    $origin = (!empty($dak['DakDaptoriks']['sender_name']) ? (h($dak['DakDaptoriks']['sender_name']) . ', ') : '') . (!empty($dak['DakDaptoriks']['sender_officer_designation_label'])
                            ? (h($dak['DakDaptoriks']['sender_officer_designation_label']) . ", ") : '') . (!empty($dak['DakDaptoriks']['sender_office_name'])
                            ? (h($dak['DakDaptoriks']['sender_office_name'])) : '');
                    $dak_subject = ($dak['DakDaptoriks']['dak_subject']);
                    $previous_sender = $origin;
                    $security_level = !empty($security_level_list[$dak['DakDaptoriks']['dak_security_level']]) ? trim(($security_level_list[$dak['DakDaptoriks']['dak_security_level']])) : '';
                }

                $next_receiver = "";
                $priority = '';
                $security_level = '';
                $move_date = $dak['modified'];
                $move_date = new Time($move_date);
                $move_date = $move_date->i18nFormat("dd-MM-yy HH:mm:ss", null, 'bn-BD');

                $dak_last_two_moves = $this->getDakLastMoveBy_dakid_officerId_designationId($dak['dak_id'],
                    $employee_office['officer_id'], $employee_office['office_unit_organogram_id'], $dak['dak_type']);
                $showDetailsDak = 'showDetailsDakSent';

                if ($dak['dak_priority'] > 1) {
                    $priority = trim($priority_list[$dak['dak_priority']]);
                }
                if (count($dak_last_two_moves) > 0) {
                    if (!empty($dak_last_two_moves[1])) {
                        $first_move = $dak_last_two_moves[1];

                        $previous_sender = (!empty($first_move['from_officer_name']) ? (h($first_move['from_officer_name']) . ', ')
                                : '') . h($first_move['from_officer_designation_label']);
                    }
                    if (!empty($dak_last_two_moves[0])) {
                        $second_move = $dak_last_two_moves[0];
                        $next_receiver = (!empty($second_move['to_officer_name']) ? (h($second_move['to_officer_name']) . ", ")
                                : '') . h($second_move['to_officer_designation_label']) . " (" . h($second_move['to_office_unit_name']) . " )";
                    }
                }
                $dak_decision = $dak['dak_actions'];
                $totalAttachment = $dak_attachments_table->find()->where(['dak_type' => $dak['dak_type'], 'dak_id' => $dak['dak_id']])->count();

                $attachment_count_bn = "";
                if (isset($totalAttachment) && $totalAttachment > 1) {
                    $attachment_count_bn = "&nbsp;<span class='badge badge-primary'><i class='fa fa-paperclip'></i> " . enTobn($totalAttachment - 1) . "</span>";
                }
                $data[] = array(
                    '<div class="text-center">
                        <button title="বিস্তারিত"  data-dak-type="' . (h($dak['dak_type'])) . '" data-toggle="tooltip" data-placement="top" type="button" class="btn btn-xs btn-primary ' . $showDetailsDak . '" data-messageid=' . $dak['dak_id'] . '  data-si=' . $si . ' >
                        <i class=" a2i_gn_details2" aria-hidden="true"></i>
                        </button>
                    </div>' .
                    h($origin),
                    "<b>" . (!empty($dak_subject) ? ($dak_subject) : "") . "</b>" . $attachment_count_bn,
                    h($next_receiver),
                    ($dak_decision),
                    str_replace(" ", "<br/>", $move_date),
                    str_replace(" ", "<br/>", (h($priority) . ' ' . h($security_level))),
                    "<div class='text-center'>" . ($dak['is_summary_nothi'] == 1 ? ("<span title='সার-সংক্ষেপ' data-original-title='সার-সংক্ষেপ' data-title='সার-সংক্ষেপ' class='glyphicon glyphicon-book'></span>") : (!isset($dak["Dak" . $dak['dak_type'] . "s"]['dak_type_id']) ? (($dak['dak_sending_media'] == '1') ? "<span title='দাপ্তরিক ডাক' data-original-title='দাপ্তরিক ডাক' data-title='দাপ্তরিক ডাক' class='fa fa-building-o'></span>" : "<span title='আপলোডকৃত দাপ্তরিক ডাক' data-original-title='আপলোডকৃত দাপ্তরিক ডাক' data-title='আপলোডকৃত দাপ্তরিক ডাক' class='fa fa-building'></span>") : (($dak['dak_sending_media'] == '1') ? "<span title='নাগরিক ডাক' data-original-title='নাগরিক ডাক' data-title='নাগরিক ডাক' class='fa fa-user-o'></span>" : "<span title='আপলোডকৃত নাগরিক ডাক' data-original-title='আপলোডকৃত নাগরিক ডাক' data-title='আপলোডকৃত নাগরিক ডাক' class='fa fa-user'></span>"))) . "</div>" . (!empty($dak['application_origin']) && $dak['application_origin'] != 'nothi' ? ("<div class='text-center'>(" . strtoupper($dak['application_origin'])[0] . ")</div>") : '')
                );
            }
        }

        $response = [
            'data' => $data,
            'total' => $totalRec,
        ];

        return $response;
    }

    protected function onulipiListBk($conditions, $employee_office, $controller, $nothiDecision, $api, $archive = '')
    {
        $start = isset($controller->request->data['start']) ? intval($controller->request->data['start']) : 0;
        $len = isset($controller->request->data['length']) ? intval($controller->request->data['length']) : 10;
        $page = !empty($controller->request->data['page']) ? intval($controller->request->data['page']) : (($start / $len) + 1);

        $dak_attachments_table = TableRegistry::get('DakAttachments');
        $dak_users = TableRegistry::get('DakUsers');

        $user_new_daks = $dak_users->find()->select([
            "DakNagoriks.id",
            "DakDaptoriks.id",
            "DakNagoriks.docketing_no",
            "DakDaptoriks.docketing_no",
            "DakDaptoriks.sender_sarok_no",
            "DakNagoriks.dak_type_id",
            "DakNagoriks.sender_name",
            "DakNagoriks.name_bng",
            "DakNagoriks.dak_subject",
            "DakNagoriks.dak_priority_level",
            "DakNagoriks.dak_security_level",
            "DakNagoriks.receiving_officer_id",
            "DakNagoriks.receiving_officer_designation_label",
            "DakNagoriks.receiving_officer_name",
            "DakNagoriks.application_origin",
            "DakDaptoriks.dak_priority_level",
            "DakDaptoriks.dak_security_level",
            "DakDaptoriks.dak_subject",
            "DakDaptoriks.sender_name",
            "DakDaptoriks.sender_officer_designation_label",
            "DakDaptoriks.sender_office_name",
            "DakDaptoriks.receiving_officer_id",
            "DakDaptoriks.receiving_officer_designation_label",
            "DakDaptoriks.receiving_officer_name",
            "to_officer_name" => 'DakUsers.to_officer_name',
            "to_officer_designation_label" => 'DakUsers.to_officer_designation_label',
            "is_summary_nothi" => 'DakUsers.is_summary_nothi',
            "dak_category" => 'DakUsers.dak_category',
            "dak_view_status" => 'DakUsers.dak_view_status',
            "dak_id" => 'DakUsers.dak_id',
            "dak_type" => 'DakUsers.dak_type',
            "modified" => 'DakUsers.modified',

        ])->join([
            'DakDaptoriks' => [
                'table' => 'dak_daptoriks',
                'type' => 'left',
                'conditions' => 'DakUsers.dak_id=DakDaptoriks.id and DakUsers.dak_type="Daptorik"'
            ],
            'DakNagoriks' => [
                'table' => 'dak_nagoriks',
                'type' => 'left',
                'conditions' => 'DakUsers.dak_id=DakNagoriks.id and DakUsers.dak_type="Nagorik"'
            ]
        ])->where(['DakUsers.to_officer_designation_id' => $employee_office['office_unit_organogram_id'], 'DakUsers.is_archive' => 1])
                                   ->where($conditions)->page($page, $len)->order(['DakUsers.modified DESC'])->toArray();

        $totalRec = $dak_users->find()
                              ->join([

                                  'DakDaptoriks' => [
                                      'table' => 'dak_daptoriks',
                                      'type' => 'left',
                                      'conditions' => 'DakUsers.dak_id=DakDaptoriks.id and DakUsers.dak_type="Daptorik"'
                                  ],
                                  'DakNagoriks' => [
                                      'table' => 'dak_nagoriks',
                                      'type' => 'left',
                                      'conditions' => 'DakUsers.dak_id=DakNagoriks.id and DakUsers.dak_type="Nagorik"'
                                  ]
                              ])->where(['DakUsers.to_officer_designation_id' => $employee_office['office_unit_organogram_id'], 'DakUsers.is_archive' => 1])->where($conditions)->order(['DakUsers.modified DESC'])->count();

        if ($api) {
            $response = [
                'data' => $user_new_daks,
                'total' => $totalRec,
            ];
            return $response;
        }
        $data = [];

        $priority_list = json_decode(DAK_PRIORITY_TYPE, true) + [1 => ''];
        $security_level_list = json_decode(DAK_SECRECY_TYPE, true) + [1 => ''];

        if (!empty($user_new_daks)) {
            foreach ($user_new_daks as $ke => $dak) {
                $si = (($page - 1) * $len) + $ke + 1;
                $dak_last_move = $this->getInboxDakLastMoveBy_dakid_attention_type($dak['dak_id'], 1, $dak['dak_type']);
                $sender = "";
                $receiver = h($dak['to_officer_name']) . (!empty($dak['to_officer_designation_label']) ? (", " . h($dak['to_officer_designation_label'])) : '');
                $dak_decision = sprintf("%s", $dak_last_move['dak_actions']);

                if ($dak['dak_type'] == 'Nagorik') {
                    $origin = h($dak['DakNagoriks']['name_bng']);
                    $dak_subject = h($dak['DakNagoriks']['dak_subject']);
                    $security_level = trim($security_level_list[$dak['DakNagoriks']['dak_security_level']]);
                } else {
                    $origin = (!empty($dak['DakDaptoriks']['sender_name']) ? (h($dak['DakDaptoriks']['sender_name']) . ', ') : '') . (!empty($dak['DakDaptoriks']['sender_officer_designation_label'])
                            ? (h($dak['DakDaptoriks']['sender_officer_designation_label']) . ", ") : '') . (!empty($dak['DakDaptoriks']['sender_office_name'])
                            ? (h($dak['DakDaptoriks']['sender_office_name'])) : '');
                    $dak_subject = h($dak['DakDaptoriks']['dak_subject']);
                    $security_level = trim($security_level_list[$dak['DakDaptoriks']['dak_security_level']]);
                }
                $move_date = $dak['modified'];
                $move_date = new Time($move_date);
                $move_date = $move_date->i18nFormat("dd-MM-yy HH:mm:ss", null, 'bn-BD');
                $showDetailsDak = 'showDetailsDakInbox';
                $priority = "";

                if ($dak['dak_priority'] > 1) {
                    $priority = trim($priority_list[$dak['dak_priority']]);
                }

                if (!empty($dak_last_move)) {
                    if ($dak_last_move['dak_priority'] > 1) {
                        $priority = trim($priority_list[$dak_last_move['dak_priority']]);
                    }

                    if ($dak_last_move['dak_security_level'] > 1) {
                        $security_level = $security_level_list[$dak_last_move['dak_security_level']];
                    }

                    $receiver = (!empty($dak_last_move['to_officer_name']) ? (h($dak_last_move['to_officer_name']) . ", ")
                            : '') . (!empty($dak_last_move['to_officer_designation_label']) ? (h($dak_last_move['to_officer_designation_label']))
                            : '');
                }

                $totalAttachment = $dak_attachments_table->find()->where(['dak_type' => $dak['dak_type'],
                                                                          'dak_id' => $dak['dak_id']])->count();


                $move_date = str_replace(" ", "</br>", $move_date);
                $data[] = array(
                    '<div class="text-center">' . "</div>",
                    '<div class="text-center">' . ('<button  data-dak-type="' . ($dak['dak_type']) . '" title="বিস্তারিত" data-toggle="tooltip" data-placement="top" type="button" class="btn btn-xs btn-primary ' . $showDetailsDak . '" data-archive=' . $archive . ' data-messageid=' . $dak['dak_id'] . ' data-si=' . $si . '>
                        <i class=" a2i_gn_details2" aria-hidden="true"></i>
                    </button>') . "</div>",
                    (!empty($origin) ? h($origin) : ""), //3rd
                    "<b>" . (!empty($dak_subject) ? h($dak_subject) : "") . "</b>", //4th
                    (!empty($receiver) ? h($receiver) : ""), //5th
                    (!empty($dak_decision) ? ($dak_decision) : ""), //6th
                    ($move_date), //7th
                    (!empty($priority) ? (h($priority) . ', ') : '') . h($security_level), //8th
                    "<div class='text-center'>" . (!empty($dak['is_summary_nothi']) ? ("<span title='সার-সংক্ষেপ' data-original-title='সার-সংক্ষেপ' data-title='সার-সংক্ষেপ' class='glyphicon glyphicon-book'></span>")
                        : ($dak['dak_type'] != DAK_NAGORIK ? "<span title='দাপ্তরিক ডাক' data-original-title='দাপ্তরিক ডাক' data-title='দাপ্তরিক ডাক' class='glyphicon glyphicon-home'></span>"
                            : "<span title='নাগরিক ডাক' data-original-title='নাগরিক ডাক' data-title='নাগরিক ডাক' class='glyphicon glyphicon-user'></span>")) . "</div>" . (!empty($dak['DakNagoriks']['application_origin']) && $dak['DakNagoriks']['application_origin'] != 'nothi' ? ("<div class='text-center'>(" . strtoupper($dak['DakNagoriks']['application_origin'])[0] . ")</div>") : ''), //9th
                    (isset($totalAttachment) && $totalAttachment > 1) ? ('<i class="fa fa-paperclip" data-toggle="tooltip" data-placement="left"> <span class="badge badge-success">' . $this->number($totalAttachment
                            - 1) . '</span></i>') : '<div class="text-center">নাই</div>',
                    "DT_RowClass" => (!empty($dak['is_summary_nothi']) ? 'summaryList' : strtolower($dak['dak_view_status']))//10th
                );
            }
        }


        $response = [
            'data' => $data,
            'total' => $totalRec,
        ];

        return $response;

    }

    protected function onulipiList($conditions, $employee_office, $controller, $nothiDecision, $api, $archive = '', $orConditions, $from_date = '', $end_date = '', $only_view)
    {
        $priority_list = json_decode(DAK_PRIORITY_TYPE, true) + [0 => '', 1 => ''];
        $security_level_list = json_decode(DAK_SECRECY_TYPE, true) + [0 => '', 1 => ''];

        $start = isset($controller->request->data['start']) ? intval($controller->request->data['start']) : 0;
        $len = isset($controller->request->data['length']) ? intval($controller->request->data['length']) : 10;
        $page = !empty($controller->request->data['page']) ? intval($controller->request->data['page']) :
            (($start / $len) + 1);

        $dak_attachments_table = TableRegistry::get('DakAttachments');
        $dak_users = TableRegistry::get('DakUsers');

        $user_new_daks = $dak_users->find()->select([
            "DakNagoriks.id",
            "DakDaptoriks.id",
            "DakNagoriks.docketing_no",
            "DakDaptoriks.docketing_no",
            "DakDaptoriks.sender_sarok_no",
            "DakNagoriks.dak_type_id",
            "DakNagoriks.sender_name",
            "DakNagoriks.name_bng",
            "DakNagoriks.dak_subject",
            "DakNagoriks.dak_priority_level",
            "DakNagoriks.dak_security_level",
            "DakNagoriks.receiving_officer_id",
            "DakNagoriks.receiving_officer_designation_label",
            "DakNagoriks.receiving_officer_name",
            "DakNagoriks.application_origin",
            "DakDaptoriks.dak_priority_level",
            "DakDaptoriks.dak_security_level",
            "DakDaptoriks.dak_subject",
            "DakDaptoriks.sender_name",
            "DakDaptoriks.sender_officer_designation_label",
            "DakDaptoriks.sender_office_name",
            "DakDaptoriks.receiving_officer_id",
            "DakDaptoriks.receiving_officer_designation_label",
            "DakDaptoriks.receiving_officer_name",
            "to_officer_name" => 'DakUsers.to_officer_name',
            "to_officer_designation_label" => 'DakUsers.to_officer_designation_label',
            "is_summary_nothi" => 'DakUsers.is_summary_nothi',
            "dak_category" => 'DakUsers.dak_category',
            "dak_view_status" => 'DakUsers.dak_view_status',
            "dak_id" => 'DakUsers.dak_id',
            "dak_type" => 'DakUsers.dak_type',
            "modified" => 'DakUsers.modified',

        ])->join([
            'DakDaptoriks' => [
                'table' => 'dak_daptoriks',
                'type' => 'left',
                'conditions' => 'DakUsers.dak_id=DakDaptoriks.id and DakUsers.dak_type="Daptorik"'
            ],
            'DakNagoriks' => [
                'table' => 'dak_nagoriks',
                'type' => 'left',
                'conditions' => 'DakUsers.dak_id=DakNagoriks.id and DakUsers.dak_type="Nagorik"'
            ]
        ])->where($conditions)->orWhere($orConditions)->where(['DakUsers.to_officer_designation_id' =>
            $employee_office['office_unit_organogram_id'], 'DakUsers.is_archive' => 1]);
        if (!empty($from_date) && !empty($end_date)) {
            $user_new_daks = $user_new_daks->where(function ($user_new_daks) use ($from_date, $end_date) {
                return $user_new_daks->between('date(DakUsers.modified)', $from_date, $end_date);
            });
        };
        $user_new_daks = $user_new_daks->page($page, $len)->order(['DakUsers.modified DESC'])->toArray();

        foreach ($user_new_daks as $key => &$user_new_dak) {
            if ($user_new_dak['DakDaptoriks']['id'] != null) {
                $dak_last_move = $this->getInboxDakLastMoveBy_dakid_attention_type($user_new_dak['DakDaptoriks']['id'], 1, 'Daptorik');
                $user_new_dak['DakDaptoriks']['dak_priority_level'] = (string) $dak_last_move['dak_priority']; //intiger value convert to string for mobile application
            } else {
                $dak_last_move = $this->getInboxDakLastMoveBy_dakid_attention_type($user_new_dak['DakNagoriks']['id'], 1, 'Nagorik');
                $user_new_dak['DakNagoriks']['dak_priority_level'] = (string) $dak_last_move['dak_priority']; //intiger value convert to string for mobile application
            }
        }

        $totalRec = $dak_users->find()
                              ->join([

                                  'DakDaptoriks' => [
                                      'table' => 'dak_daptoriks',
                                      'type' => 'left',
                                      'conditions' => 'DakUsers.dak_id=DakDaptoriks.id and DakUsers.dak_type="Daptorik"'
                                  ],
                                  'DakNagoriks' => [
                                      'table' => 'dak_nagoriks',
                                      'type' => 'left',
                                      'conditions' => 'DakUsers.dak_id=DakNagoriks.id and DakUsers.dak_type="Nagorik"'
                                  ]
                              ])->where($conditions)->orWhere($orConditions)->where(['DakUsers.to_officer_designation_id' =>
                                $employee_office['office_unit_organogram_id'], 'DakUsers.is_archive' => 1]);
        if (!empty($from_date) && !empty($end_date)) {
            $totalRec = $totalRec->where(function ($totalRec) use ($from_date, $end_date) {
                return $totalRec->between('date(DakUsers.modified)', $from_date, $end_date);
            });
        };
        $totalRec = $totalRec->order(['DakUsers.modified DESC'])->count();

        if ($api) {
            $response = [
                'data' => $user_new_daks,
                'total' => $totalRec,
            ];
            return $response;
        }
        $data = [];

        if (!empty($user_new_daks)) {
            foreach ($user_new_daks as $ke => $dak) {
                $si = (($page - 1) * $len) + $ke + 1;
                $dak_last_move = $this->getInboxDakLastMoveBy_dakid_attention_type($dak['dak_id'], 1, $dak['dak_type']);
                $sender = "";
                $receiver = h($dak['to_officer_name']) . (!empty($dak['to_officer_designation_label']) ? (", " .
                        h($dak['to_officer_designation_label'])) : '');
                $dak_decision = sprintf("%s", $dak_last_move['dak_actions']);

                if ($dak['dak_type'] == 'Nagorik') {
                    $origin = h($dak['DakNagoriks']['name_bng']);
                    $dak_subject = ($dak['DakNagoriks']['dak_subject']);
                    $security_level = trim($security_level_list[$dak['DakNagoriks']['dak_security_level']]);
                } else {
                    $origin = (!empty($dak['DakDaptoriks']['sender_name']) ?
                            (h($dak['DakDaptoriks']['sender_name']) . ', ') : '') .
                        (!empty($dak['DakDaptoriks']['sender_officer_designation_label'])
                            ? (h($dak['DakDaptoriks']['sender_officer_designation_label']) . ", ") : '') .
                        (!empty($dak['DakDaptoriks']['sender_office_name'])
                            ? (h($dak['DakDaptoriks']['sender_office_name'])) : '');
                    $dak_subject = ($dak['DakDaptoriks']['dak_subject']);
                    $security_level = trim($security_level_list[$dak['DakDaptoriks']['dak_security_level']]);
                }
                $move_date = $dak['modified'];
                $move_date = new Time($move_date);
                $move_date = $move_date->i18nFormat("dd-MM-yy HH:mm:ss", null, 'bn-BD');
                $showDetailsDak = 'showDetailsDakArchive';
                $priority = "";

                if ($dak['dak_priority'] > 1) {
                    $priority = trim($priority_list[$dak['dak_priority']]);
                }

                if (!empty($dak_last_move)) {
                    if ($dak_last_move['dak_priority'] > 1) {
                        $priority = trim($priority_list[$dak_last_move['dak_priority']]);
                    }

                    if ($dak_last_move['dak_security_level'] > 1) {
                        $security_level = $security_level_list[$dak_last_move['dak_security_level']];
                    }

                    $receiver = (!empty($dak_last_move['to_officer_name']) ? (h($dak_last_move['to_officer_name']) . ", ")
                            : '') . (!empty($dak_last_move['to_officer_designation_label']) ?
                            (h($dak_last_move['to_officer_designation_label']))
                            : '');
                }

                $totalAttachment = $dak_attachments_table->find()->where(['dak_type' => $dak['dak_type'],
                                                                          'dak_id' => $dak['dak_id']])->count();


                $attachment_count_bn = "";
                if (isset($totalAttachment) && $totalAttachment > 1) {
                    $attachment_count_bn = "&nbsp;<span class='badge badge-primary'><i class='fa fa-paperclip'></i> " . enTobn($totalAttachment - 1) . "</span>";
                }
                $move_date = str_replace(" ", "</br>", $move_date);
                $data[] = array(
                    '<div class="text-center">' . ('<button  data-dak-type="' . ($dak['dak_type']) . '" title="বিস্তারিত" data-toggle="tooltip" data-placement="top" type="button" class="btn btn-xs btn-primary ' . $showDetailsDak . '" data-archive=' . $archive . ' data-messageid=' . $dak['dak_id'] . ' data-si=' . $si . '>
                        <i class=" a2i_gn_details2" aria-hidden="true"></i>
                    </button>') . "</div>" .
                    (!empty($origin) ? h($origin) : ""), //3rd
                    "<b>" . (!empty($dak_subject) ? ($dak_subject) : "") . "</b>" . $attachment_count_bn, //4th
                    (!empty($receiver) ? h($receiver) : ""), //5th
                    (!empty($dak_decision) ? ($dak_decision) : ""), //6th
                    ($move_date), //7th
                    (!empty($priority) ? (h($priority) . ', ') : '') . h($security_level), //8th
                    "<div class='text-center'>" . ($dak['is_summary_nothi'] == 1 ? ("<span title='সার-সংক্ষেপ' data-original-title='সার-সংক্ষেপ' data-title='সার-সংক্ষেপ' class='glyphicon glyphicon-book'></span>") : (!isset($dak['dak_type_id']) ? (($dak['dak_sending_media'] == '1') ? "<span title='দাপ্তরিক ডাক' data-original-title='দাপ্তরিক ডাক' data-title='দাপ্তরিক ডাক' class='fa fa-building-o'></span>" : "<span title='আপলোডকৃত দাপ্তরিক ডাক' data-original-title='আপলোডকৃত দাপ্তরিক ডাক' data-title='আপলোডকৃত দাপ্তরিক ডাক' class='fa fa-building'></span>") : (($dak['dak_sending_media'] == '1') ? "<span title='নাগরিক ডাক' data-original-title='নাগরিক ডাক' data-title='নাগরিক ডাক' class='fa fa-user-o'></span>" : "<span title='আপলোডকৃত নাগরিক ডাক' data-original-title='আপলোডকৃত নাগরিক ডাক' data-title='আপলোডকৃত নাগরিক ডাক' class='fa fa-user'></span>"))) . "</div>" . (!empty($dak['application_origin']) && $dak['application_origin'] != 'nothi' ? ("<div class='text-center'>(" . strtoupper($dak['application_origin'])[0] . ")</div>") : ''), //9th
                    "DT_RowClass" => (!empty($dak['is_summary_nothi']) ? 'summaryList' : strtolower($dak['dak_view_status']))//10th
                );
            }
        }


        $response = [
            'data' => $data,
            'total' => $totalRec,
        ];

        return $response;

    }

    protected function nothivuktoListBk($conditions, $employee_office, $controller, $nothiDecision, $api)
    {

        $start = isset($controller->request->data['start']) ? intval($controller->request->data['start']) : 0;
        $len = isset($controller->request->data['length']) ? intval($controller->request->data['length']) : 10;
        $page = !empty($controller->request->data['page']) ? intval($controller->request->data['page']) : (($start / $len) + 1);

        $dak_attachments_table = TableRegistry::get('DakAttachments');

        $user_new_daks = $this->find()->select([

            "DakNagoriks.id",
            "DakDaptoriks.id",
            "DakNagoriks.docketing_no",
            "DakDaptoriks.docketing_no",
            "DakDaptoriks.sender_sarok_no",

            "DakNagoriks.dak_type_id",
            "DakNagoriks.sender_name",
            "DakNagoriks.name_bng",
            "DakNagoriks.dak_subject",
            "DakNagoriks.dak_priority_level",
            "DakNagoriks.dak_security_level",
            "DakNagoriks.application_origin",

            "DakDaptoriks.dak_priority_level",
            "DakDaptoriks.dak_security_level",
            "DakDaptoriks.dak_subject",
            "DakDaptoriks.sender_name",
            "DakDaptoriks.sender_officer_designation_label",
            "DakDaptoriks.sender_office_name",
            "DakDaptoriks.is_summary_nothi",
            "dak_id" => 'DakMovements.dak_id',
            "modified" => 'DakMovements.modified',
            "dak_actions" => 'DakMovements.dak_actions',
            "dak_priority" => 'DakMovements.dak_priority',
            "attention_type" => 'DakMovements.attention_type',
            "dak_type" => 'DakMovements.dak_type', 'DakMovements.sequence', 'DakMovements.to_officer_designation_id', 'DakMovements.operation_type'
        ])
                              ->join([
                                  'DakMovements2' => [
                                      'table' => 'dak_movements',
                                      'type' => 'left',
                                      'conditions' => 'DakMovements.dak_id=DakMovements2.dak_id and DakMovements.dak_type=DakMovements2.dak_type and DakMovements.id < DakMovements2.id and DakMovements.from_officer_designation_id = DakMovements2.from_officer_designation_id'
                                  ],
                                  'DakDaptoriks' => [
                                      'table' => 'dak_daptoriks',
                                      'type' => 'left',
                                      'conditions' => 'DakMovements.dak_id=DakDaptoriks.id and DakMovements.dak_type="Daptorik"'
                                  ],
                                  'DakNagoriks' => [
                                      'table' => 'dak_nagoriks',
                                      'type' => 'left',
                                      'conditions' => 'DakMovements.dak_id=DakNagoriks.id and DakMovements.dak_type="Nagorik"'
                                  ],
                              ])->where(['DakMovements2.id IS NULL'])->where(['DakMovements.from_officer_designation_id' => $employee_office['office_unit_organogram_id']])->where(['DakMovements.operation_type' => DAK_CATEGORY_NOTHIVUKTO])
                              ->where($conditions)
                              ->page($page, $len)->order(['DakMovements.modified DESC'])->toArray();

        $totalRec = $this->find()->select(["dak_id" => 'DakMovements.dak_id', "dak_type" => 'DakMovements.dak_type'])->join([
            'DakMovements2' => [
                'table' => 'dak_movements',
                'type' => 'left',
                'conditions' => 'DakMovements.dak_id=DakMovements2.dak_id and DakMovements.dak_type=DakMovements2.dak_type and DakMovements.id < DakMovements2.id and DakMovements.from_officer_designation_id = DakMovements2.from_officer_designation_id'
            ],
            'DakDaptoriks' => [
                'table' => 'dak_daptoriks',
                'type' => 'left',
                'conditions' => 'DakMovements.dak_id=DakDaptoriks.id and DakMovements.dak_type="Daptorik"'
            ],
            'DakNagoriks' => [
                'table' => 'dak_nagoriks',
                'type' => 'left',
                'conditions' => 'DakMovements.dak_id=DakNagoriks.id and DakMovements.dak_type="Nagorik"'
            ],
        ])->where(['DakMovements2.id IS NULL'])->where(['DakMovements.from_officer_designation_id' => $employee_office['office_unit_organogram_id']])->where(['DakMovements.operation_type' => DAK_CATEGORY_NOTHIVUKTO])->where($conditions)->order(['DakMovements.modified DESC'])->count();

        if ($api) {
            $response = [
                'data' => $user_new_daks,
                'total' => $totalRec,
            ];
            return $response;
        }

        $data = array();

        $priority_list = json_decode(DAK_PRIORITY_TYPE, true) + [1 => ''];
        $security_level_list = json_decode(DAK_SECRECY_TYPE, true) + [1 => ''];

        if (!empty($user_new_daks)) {
            foreach ($user_new_daks as $ke => $dak) {
                $si = (($page - 1) * $len) + $ke + 1;

                if ($dak['dak_type'] == 'Nagorik') {
                    $origin = h($dak['DakNagoriks']['name_bng']);
                    $dak_subject = h($dak['DakNagoriks']['dak_subject']);
                    $previous_sender = $origin;
                    $security_level = trim($security_level_list[$dak['DakNagoriks']['dak_security_level']]);
                } else {
                    $origin = (!empty($dak['DakDaptoriks']['sender_name']) ? (h($dak['DakDaptoriks']['sender_name']) . ', ') : '') . (!empty($dak['DakDaptoriks']['sender_officer_designation_label'])
                            ? (h($dak['DakDaptoriks']['sender_officer_designation_label']) . ", ") : '') . (!empty($dak['DakDaptoriks']['sender_office_name'])
                            ? (h($dak['DakDaptoriks']['sender_office_name'])) : '');
                    $dak_subject = h($dak['DakDaptoriks']['dak_subject']);
                    $previous_sender = $origin;
                    $security_level = trim($security_level_list[$dak['DakDaptoriks']['dak_security_level']]);
                }

                $next_receiver = "";
                $priority = '';
                $security_level = '';
                $move_date = $dak['modified'];
                $move_date = new Time($move_date);
                $move_date = $move_date->i18nFormat("dd-MM-yy HH:mm:ss", null, 'bn-BD');

                $dak_last_two_moves = $this->getDakLastMoveBy_dakid_officerId_designationId($dak['dak_id'],
                    $employee_office['officer_id'], $employee_office['office_unit_organogram_id'], $dak['dak_type']);
                $showDetailsDak = 'showDetailsNothiVukto';

                if ($dak['dak_priority'] > 1) {
                    $priority = trim($priority_list[$dak['dak_priority']]);
                }
                if (count($dak_last_two_moves) > 0) {
                    if (!empty($dak_last_two_moves[1])) {
                        $first_move = $dak_last_two_moves[1];

                        $previous_sender = (!empty($first_move['from_officer_name']) ? (h($first_move['from_officer_name']) . ', ')
                                : '') . h($first_move['from_officer_designation_label']);
                    }
                    if (!empty($dak_last_two_moves[0])) {
                        $second_move = $dak_last_two_moves[0];
                        $next_receiver = (!empty($second_move['to_officer_name']) ? (h($second_move['to_officer_name']) . ", ")
                                : '') . h($second_move['to_officer_designation_label']) . " (" . h($second_move['to_office_unit_name']) . " )";
                    }
                }
                $dak_decision = $dak['dak_actions'];
                $totalAttachment = $dak_attachments_table->find()->where(['dak_type' => $dak['dak_type'], 'dak_id' => $dak['dak_id']])->count();

                $data[] = array(
                    '<div class="text-center">
                        <button title="বিস্তারিত"  data-dak-type="' . ($dak['dak_type']) . '" data-toggle="tooltip" data-placement="top" type="button" class="btn btn-xs btn-primary ' . $showDetailsDak . '" data-messageid=' . $dak['dak_id'] . '  data-si=' . $si . ' >
                        <i class=" a2i_gn_details2" aria-hidden="true"></i>
                        </button>
                    </div>',
                    h($origin),
                    "<b>" . (!empty($dak_subject) ? h($dak_subject) : "") . "</b>",
                    h($next_receiver),
                    ($dak_decision),
                    str_replace(" ", "<br/>", $move_date),
                    str_replace(" ", "<br/>", (h($priority) . ' ' . h($security_level))),
                    "<div class='text-center'>" . (!empty($dak['DakDaptoriks']['is_summary_nothi']) ? ("<span title='সার-সংক্ষেপ' data-original-title='সার-সংক্ষেপ' data-title='সার-সংক্ষেপ' class='glyphicon glyphicon-book'></span>")
                        : ($dak['dak_type'] != DAK_NAGORIK ? "<span title='দাপ্তরিক ডাক' data-original-title='দাপ্তরিক ডাক' data-title='দাপ্তরিক ডাক' class='glyphicon glyphicon-home'></span>"
                            : "<span title='নাগরিক ডাক' data-original-title='নাগরিক ডাক' data-title='নাগরিক ডাক' class='glyphicon glyphicon-user'></span>")) . "</div>" . (!empty($dak['DakNagoriks']['application_origin']) && $dak['DakNagoriks']['application_origin'] != 'nothi' ? ("<div class='text-center'>(" . strtoupper($dak['DakNagoriks']['application_origin'])[0] . ")</div>") : ''),
                    (isset($totalAttachment) && $totalAttachment > 1) ? '<i class="fa fa-paperclip" data-toggle="tooltip" data-placement="left" > <span class="badge badge-success">' . $this->number($totalAttachment
                            - 1) . '</span></i>' : '<div class="text-center">নাই</div>'
                );
            }
        }

        $response = [
            'data' => $data,
            'total' => $totalRec,
        ];

        return $response;
    }

    protected function nothivuktoList($conditions, $employee_office, $controller, $nothiDecision, $api, $orCondition, $from_date = '', $end_date = '', $only_view)
    {

        $start = isset($controller->request->data['start']) ? intval($controller->request->data['start']) : 0;
        $len = isset($controller->request->data['length']) ? intval($controller->request->data['length']) : 10;
        $page = !empty($controller->request->data['page']) ? intval($controller->request->data['page']) : (($start / $len) + 1);

        $dak_attachments_table = TableRegistry::get('DakAttachments');

        $user_new_daks = $this->find()->select([

            "DakNagoriks.id",
            "DakDaptoriks.id",
            "DakNagoriks.docketing_no",
            "DakDaptoriks.docketing_no",
            "DakDaptoriks.sender_sarok_no",

            "DakNagoriks.dak_type_id",
            "DakNagoriks.sender_name",
            "DakNagoriks.name_bng",
            "DakNagoriks.dak_subject",
            "DakNagoriks.dak_priority_level",
            "DakNagoriks.dak_security_level",
            "DakNagoriks.application_origin",

            "DakDaptoriks.dak_priority_level",
            "DakDaptoriks.dak_security_level",
            "DakDaptoriks.dak_subject",
            "DakDaptoriks.sender_name",
            "DakDaptoriks.sender_officer_designation_label",
            "DakDaptoriks.sender_office_name",
            "DakDaptoriks.is_summary_nothi",
            "dak_id" => 'DakMovements.dak_id',
            "modified" => 'DakMovements.modified',
            "dak_actions" => 'DakMovements.dak_actions',
            "dak_priority" => 'DakMovements.dak_priority',
            "attention_type" => 'DakMovements.attention_type',
            "dak_type" => 'DakMovements.dak_type', 'DakMovements.sequence', 'DakMovements.to_officer_designation_id', 'DakMovements.operation_type'
        ])
                              ->join([
                                  'DakMovements2' => [
                                      'table' => 'dak_movements',
                                      'type' => 'left',
                                      'conditions' => 'DakMovements.dak_id=DakMovements2.dak_id and DakMovements.dak_type=DakMovements2.dak_type and DakMovements.id < DakMovements2.id and DakMovements.from_officer_designation_id = DakMovements2.from_officer_designation_id'
                                  ],
                                  'DakDaptoriks' => [
                                      'table' => 'dak_daptoriks',
                                      'type' => 'left',
                                      'conditions' => 'DakMovements.dak_id=DakDaptoriks.id and DakMovements.dak_type="Daptorik"'
                                  ],
                                  'DakNagoriks' => [
                                      'table' => 'dak_nagoriks',
                                      'type' => 'left',
                                      'conditions' => 'DakMovements.dak_id=DakNagoriks.id and DakMovements.dak_type="Nagorik"'
                                  ],
                              ])->where($conditions)->orWhere($orCondition)->where(['DakMovements2.id IS NULL'])->where(['DakMovements.from_officer_designation_id' => $employee_office['office_unit_organogram_id']])->where(['DakMovements.operation_type' => DAK_CATEGORY_NOTHIVUKTO]);

        if (!empty($from_date) && !empty($end_date)) {
            $user_new_daks = $user_new_daks->where(function ($user_new_daks) use ($from_date, $end_date) {
                return $user_new_daks->between('date(DakMovements.modified)', $from_date, $end_date);
            });
        };
        $user_new_daks = $user_new_daks->page($page, $len)->order(['DakMovements.modified DESC'])->toArray();

        $totalRec = $this->find()->select(["dak_id" => 'DakMovements.dak_id', "dak_type" => 'DakMovements.dak_type'])->join([
            'DakMovements2' => [
                'table' => 'dak_movements',
                'type' => 'left',
                'conditions' => 'DakMovements.dak_id=DakMovements2.dak_id and DakMovements.dak_type=DakMovements2.dak_type and DakMovements.id < DakMovements2.id and DakMovements.from_officer_designation_id = DakMovements2.from_officer_designation_id'
            ],
            'DakDaptoriks' => [
                'table' => 'dak_daptoriks',
                'type' => 'left',
                'conditions' => 'DakMovements.dak_id=DakDaptoriks.id and DakMovements.dak_type="Daptorik"'
            ],
            'DakNagoriks' => [
                'table' => 'dak_nagoriks',
                'type' => 'left',
                'conditions' => 'DakMovements.dak_id=DakNagoriks.id and DakMovements.dak_type="Nagorik"'
            ],
        ])->where($conditions)->orWhere($orCondition)->where(['DakMovements2.id IS NULL'])->where(['DakMovements.from_officer_designation_id' => $employee_office['office_unit_organogram_id']])->where(['DakMovements.operation_type' => DAK_CATEGORY_NOTHIVUKTO]);
        if (!empty($from_date) && !empty($end_date)) {
            $totalRec = $totalRec->where(function ($totalRec) use ($from_date, $end_date) {
                return $totalRec->between('date(DakMovements.modified)', $from_date, $end_date);
            });
        };
        $totalRec = $totalRec->order(['DakMovements.modified DESC'])->count();

        if ($api) {
            $response = [
                'data' => $user_new_daks,
                'total' => $totalRec,
            ];
            return $response;
        }

        $data = array();

        $priority_list = json_decode(DAK_PRIORITY_TYPE, true) + [0 => '', 1 => ''];
        $security_level_list = json_decode(DAK_SECRECY_TYPE, true) + [0 => '', 1 => ''];

        if (!empty($user_new_daks)) {
            foreach ($user_new_daks as $ke => $dak) {
                $si = (($page - 1) * $len) + $ke + 1;

                if ($dak['dak_type'] == 'Nagorik') {
                    $origin = h($dak['DakNagoriks']['name_bng']);
                    $dak_subject = ($dak['DakNagoriks']['dak_subject']);
                    $previous_sender = $origin;
                    $security_level = trim($security_level_list[$dak['DakNagoriks']['dak_security_level']]);
                } else {
                    $origin = (!empty($dak['DakDaptoriks']['sender_name']) ? (h($dak['DakDaptoriks']['sender_name']) . ', ') : '') . (!empty($dak['DakDaptoriks']['sender_officer_designation_label'])
                            ? (h($dak['DakDaptoriks']['sender_officer_designation_label']) . ", ") : '') . (!empty($dak['DakDaptoriks']['sender_office_name'])
                            ? (h($dak['DakDaptoriks']['sender_office_name'])) : '');
                    $dak_subject = ($dak['DakDaptoriks']['dak_subject']);
                    $previous_sender = $origin;
                    $security_level = trim($security_level_list[$dak['DakDaptoriks']['dak_security_level']]);
                }

                $next_receiver = "";
                $priority = '';
                $security_level = '';
                $move_date = $dak['modified'];
                $move_date = new Time($move_date);
                $move_date = $move_date->i18nFormat("dd-MM-yy HH:mm:ss", null, 'bn-BD');

                $dak_last_two_moves = $this->getDakLastMoveBy_dakid_officerId_designationId($dak['dak_id'],
                    $employee_office['officer_id'], $employee_office['office_unit_organogram_id'], $dak['dak_type']);
                $showDetailsDak = 'showDetailsDakNothiVukto';

                if ($dak['dak_priority'] > 1) {
                    $priority = trim($priority_list[$dak['dak_priority']]);
                }
                if (count($dak_last_two_moves) > 0) {
                    if (!empty($dak_last_two_moves[1])) {
                        $first_move = $dak_last_two_moves[1];

                        $previous_sender = (!empty($first_move['from_officer_name']) ? (h($first_move['from_officer_name']) . ', ')
                                : '') . h($first_move['from_officer_designation_label']);
                    }
                    if (!empty($dak_last_two_moves[0])) {
                        $second_move = $dak_last_two_moves[0];
                        $next_receiver = (!empty($second_move['to_officer_name']) ? (h($second_move['to_officer_name']) . ", ")
                                : '') . h($second_move['to_officer_designation_label']) . " (" . h($second_move['to_office_unit_name']) . " )";
                    }
                }
                $dak_decision = $dak['dak_actions'];
                $totalAttachment = $dak_attachments_table->find()->where(['dak_type' => $dak['dak_type'], 'dak_id' => $dak['dak_id']])->count();

                $attachment_count_bn = "";
                if (isset($totalAttachment) && $totalAttachment > 1) {
                    $attachment_count_bn = "&nbsp;<span class='badge badge-primary'><i class='fa fa-paperclip'></i> " . enTobn($totalAttachment - 1) . "</span>";
                }
                $data[] = array(
                    '<div class="text-center">
                        <button title="বিস্তারিত"  data-dak-type="' . ($dak['dak_type']) . '" data-toggle="tooltip" data-placement="top" type="button" class="btn btn-xs btn-primary ' . $showDetailsDak . '" data-messageid=' . $dak['dak_id'] . '  data-si=' . $si . ' >
                        <i class=" a2i_gn_details2" aria-hidden="true"></i>
                        </button>
                    </div>' .
                    h($origin),
                    "<b>" . (!empty($dak_subject) ? ($dak_subject) : "") . "</b>" . $attachment_count_bn,
                    h($next_receiver),
                    ($dak_decision),
                    str_replace(" ", "<br/>", $move_date),
                    str_replace(" ", "<br/>", (h($priority) . ' ' . h($security_level))),
                    "<div class='text-center'>" . ($dak['is_summary_nothi'] == 1 ? ("<span title='সার-সংক্ষেপ' data-original-title='সার-সংক্ষেপ' data-title='সার-সংক্ষেপ' class='glyphicon glyphicon-book'></span>") : (!isset($dak["Dak" . $dak['dak_type'] . "s"]['dak_type_id']) ? (($dak['dak_sending_media'] == '1') ? "<span title='দাপ্তরিক ডাক' data-original-title='দাপ্তরিক ডাক' data-title='দাপ্তরিক ডাক' class='fa fa-building-o'></span>" : "<span title='আপলোডকৃত দাপ্তরিক ডাক' data-original-title='আপলোডকৃত দাপ্তরিক ডাক' data-title='আপলোডকৃত দাপ্তরিক ডাক' class='fa fa-building'></span>") : (($dak['dak_sending_media'] == '1') ? "<span title='নাগরিক ডাক' data-original-title='নাগরিক ডাক' data-title='নাগরিক ডাক' class='fa fa-user-o'></span>" : "<span title='আপলোডকৃত নাগরিক ডাক' data-original-title='আপলোডকৃত নাগরিক ডাক' data-title='আপলোডকৃত নাগরিক ডাক' class='fa fa-user'></span>"))) . "</div>" . (!empty($dak['application_origin']) && $dak['application_origin'] != 'nothi' ? ("<div class='text-center'>(" . strtoupper($dak['application_origin'])[0] . ")</div>") : '')
                );
            }
        }

        $response = [
            'data' => $data,
            'total' => $totalRec,
        ];

        return $response;
    }

    protected function nothijatoListBk($conditions, $employee_office, $controller, $nothiDecision, $api)
    {

        $start = isset($controller->request->data['start']) ? intval($controller->request->data['start']) : 0;
        $len = isset($controller->request->data['length']) ? intval($controller->request->data['length']) : 10;
        $page = !empty($controller->request->data['page']) ? intval($controller->request->data['page']) : (($start / $len) + 1);

        $dak_attachments_table = TableRegistry::get('DakAttachments');

        $user_new_daks = $this->find()->select([

            "DakNagoriks.id",
            "DakDaptoriks.id",
            "DakNagoriks.docketing_no",
            "DakDaptoriks.docketing_no",
            "DakDaptoriks.sender_sarok_no",

            "DakNagoriks.dak_type_id",
            "DakNagoriks.sender_name",
            "DakNagoriks.name_bng",
            "DakNagoriks.dak_subject",
            "DakNagoriks.dak_priority_level",
            "DakNagoriks.dak_security_level",
            "DakNagoriks.application_origin",

            "DakDaptoriks.dak_priority_level",
            "DakDaptoriks.dak_security_level",
            "DakDaptoriks.dak_subject",
            "DakDaptoriks.sender_name",
            "DakDaptoriks.sender_officer_designation_label",
            "DakDaptoriks.sender_office_name",
            "DakDaptoriks.is_summary_nothi",
            "dak_id" => 'DakMovements.dak_id',
            "modified" => 'DakMovements.modified',
            "dak_actions" => 'DakMovements.dak_actions',
            "dak_priority" => 'DakMovements.dak_priority',
            "attention_type" => 'DakMovements.attention_type',
            "dak_type" => 'DakMovements.dak_type', 'DakMovements.sequence', 'DakMovements.to_officer_designation_id', 'DakMovements.operation_type'
        ])
                              ->join([
                                  'DakMovements2' => [
                                      'table' => 'dak_movements',
                                      'type' => 'left',
                                      'conditions' => 'DakMovements.dak_id=DakMovements2.dak_id and DakMovements.dak_type=DakMovements2.dak_type and DakMovements.id < DakMovements2.id and DakMovements.from_officer_designation_id = DakMovements2.from_officer_designation_id'
                                  ],
                                  'DakDaptoriks' => [
                                      'table' => 'dak_daptoriks',
                                      'type' => 'left',
                                      'conditions' => 'DakMovements.dak_id=DakDaptoriks.id and DakMovements.dak_type="Daptorik"'
                                  ],
                                  'DakNagoriks' => [
                                      'table' => 'dak_nagoriks',
                                      'type' => 'left',
                                      'conditions' => 'DakMovements.dak_id=DakNagoriks.id and DakMovements.dak_type="Nagorik"'
                                  ],
                              ])->where(['DakMovements2.id IS NULL'])->where(['DakMovements.from_officer_designation_id' => $employee_office['office_unit_organogram_id']])->where(['DakMovements.operation_type' => DAK_CATEGORY_NOTHIJATO])
                              ->where($conditions)
                              ->page($page, $len)->order(['DakMovements.modified DESC'])->toArray();

        $totalRec = $this->find()->select(["dak_id" => 'DakMovements.dak_id', "dak_type" => 'DakMovements.dak_type'])->join([
            'DakMovements2' => [
                'table' => 'dak_movements',
                'type' => 'left',
                'conditions' => 'DakMovements.dak_id=DakMovements2.dak_id and DakMovements.dak_type=DakMovements2.dak_type and DakMovements.id < DakMovements2.id  and DakMovements.from_officer_designation_id = DakMovements2.from_officer_designation_id'
            ],
            'DakDaptoriks' => [
                'table' => 'dak_daptoriks',
                'type' => 'left',
                'conditions' => 'DakMovements.dak_id=DakDaptoriks.id and DakMovements.dak_type="Daptorik"'
            ],
            'DakNagoriks' => [
                'table' => 'dak_nagoriks',
                'type' => 'left',
                'conditions' => 'DakMovements.dak_id=DakNagoriks.id and DakMovements.dak_type="Nagorik"'
            ],
        ])->where(['DakMovements2.id IS NULL'])->where(['DakMovements.from_officer_designation_id' => $employee_office['office_unit_organogram_id']])->where(['DakMovements.operation_type' => DAK_CATEGORY_NOTHIJATO])->where($conditions)->order(['DakMovements.modified DESC'])->count();

        if ($api) {
            $response = [
                'data' => $user_new_daks,
                'total' => $totalRec,
            ];
            return $response;
        }

        $data = array();

        $priority_list = json_decode(DAK_PRIORITY_TYPE, true) + [1 => ''];
        $security_level_list = json_decode(DAK_SECRECY_TYPE, true) + [1 => ''];
        if (!empty($user_new_daks)) {
            foreach ($user_new_daks as $ke => $dak) {
                $si = (($page - 1) * $len) + $ke + 1;

                if ($dak['dak_type'] == 'Nagorik') {
                    $origin = h($dak['DakNagoriks']['name_bng']);
                    $dak_subject = h($dak['DakNagoriks']['dak_subject']);
                    $previous_sender = $origin;
                    $security_level = trim($security_level_list[$dak['DakNagoriks']['dak_security_level']]);
                } else {
                    $origin = (!empty($dak['DakDaptoriks']['sender_name']) ? (h($dak['DakDaptoriks']['sender_name']) . ', ') : '') . (!empty($dak['DakDaptoriks']['sender_officer_designation_label'])
                            ? (h($dak['DakDaptoriks']['sender_officer_designation_label']) . ", ") : '') . (!empty($dak['DakDaptoriks']['sender_office_name'])
                            ? (h($dak['DakDaptoriks']['sender_office_name'])) : '');
                    $dak_subject = h($dak['DakDaptoriks']['dak_subject']);
                    $previous_sender = $origin;
                    $security_level = trim($security_level_list[$dak['DakDaptoriks']['dak_security_level']]);
                }

                $next_receiver = "";
                $priority = '';
                $security_level = '';
                $move_date = $dak['modified'];
                $move_date = new Time($move_date);
                $move_date = $move_date->i18nFormat("dd-MM-yy HH:mm:ss", null, 'bn-BD');

                $dak_last_two_moves = $this->getDakLastMoveBy_dakid_officerId_designationId($dak['dak_id'],
                    $employee_office['officer_id'], $employee_office['office_unit_organogram_id'], $dak['dak_type']);
                $showDetailsDak = 'showDetailsDakNothiJato';

                if ($dak['dak_priority'] > 1) {
                    $priority = trim($priority_list[$dak['dak_priority']]);
                }
                if (count($dak_last_two_moves) > 0) {
                    if (!empty($dak_last_two_moves[1])) {
                        $first_move = $dak_last_two_moves[1];

                        $previous_sender = (!empty($first_move['from_officer_name']) ? (h($first_move['from_officer_name']) . ', ')
                                : '') . h($first_move['from_officer_designation_label']);
                    }
                    if (!empty($dak_last_two_moves[0])) {
                        $second_move = $dak_last_two_moves[0];
                        $next_receiver = (!empty($second_move['to_officer_name']) ? (h($second_move['to_officer_name']) . ", ")
                                : '') . h($second_move['to_officer_designation_label']) . " (" . h($second_move['to_office_unit_name']) . " )";
                    }
                }
                $dak_decision = $dak['dak_actions'];
                $totalAttachment = $dak_attachments_table->find()->where(['dak_type' => $dak['dak_type'], 'dak_id' => $dak['dak_id']])->count();

                $data[] = array(
                    '<div class="text-center">
                        <button title="বিস্তারিত"  data-dak-type="' . ($dak['dak_type']) . '" data-toggle="tooltip" data-placement="top" type="button" class="btn btn-xs btn-primary ' . $showDetailsDak . '" data-messageid=' . $dak['dak_id'] . '  data-si=' . $si . ' >
                        <i class=" a2i_gn_details2" aria-hidden="true"></i>
                        </button>
                    </div>',
                    h($origin),
                    "<b>" . (!empty($dak_subject) ? h($dak_subject) : "") . "</b>",
                    h($next_receiver),
                    ($dak_decision),
                    str_replace(" ", "<br/>", $move_date),
                    str_replace(" ", "<br/>", (h($priority) . ' ' . h($security_level))),
                    "<div class='text-center'>" . (!empty($dak['DakDaptoriks']['is_summary_nothi']) ? ("<span title='সার-সংক্ষেপ' data-original-title='সার-সংক্ষেপ' data-title='সার-সংক্ষেপ' class='glyphicon glyphicon-book'></span>")
                        : ($dak['dak_type'] != DAK_NAGORIK ? "<span title='দাপ্তরিক ডাক' data-original-title='দাপ্তরিক ডাক' data-title='দাপ্তরিক ডাক' class='glyphicon glyphicon-home'></span>"
                            : "<span title='নাগরিক ডাক' data-original-title='নাগরিক ডাক' data-title='নাগরিক ডাক' class='glyphicon glyphicon-user'></span>")) . "</div>" . (!empty($dak['DakNagoriks']['application_origin']) && $dak['DakNagoriks']['application_origin'] != 'nothi' ? ("<div class='text-center'>(" . strtoupper($dak['DakNagoriks']['application_origin'])[0] . ")</div>") : ''),
                    (isset($totalAttachment) && $totalAttachment > 1) ? '<i class="fa fa-paperclip" data-toggle="tooltip" data-placement="left" > <span class="badge badge-success">' . $this->number($totalAttachment
                            - 1) . '</span></i>' : '<div class="text-center">নাই</div>'
                );
            }
        }

        $response = [
            'data' => $data,
            'total' => $totalRec,
        ];

        return $response;
    }

    protected function nothijatoList($conditions, $employee_office, $controller, $nothiDecision, $api, $orConditions, $from_date = '', $end_date = '', $only_view)
    {

        $start = isset($controller->request->data['start']) ? intval($controller->request->data['start']) : 0;
        $len = isset($controller->request->data['length']) ? intval($controller->request->data['length']) : 10;
        $page = !empty($controller->request->data['page']) ? intval($controller->request->data['page']) : (($start / $len) + 1);

        $dak_attachments_table = TableRegistry::get('DakAttachments');

        $user_new_daks = $this->find()->select([

            "DakNagoriks.id",
            "DakDaptoriks.id",
            "DakNagoriks.docketing_no",
            "DakDaptoriks.docketing_no",
            "DakDaptoriks.sender_sarok_no",

            "DakNagoriks.dak_type_id",
            "DakNagoriks.sender_name",
            "DakNagoriks.name_bng",
            "DakNagoriks.dak_subject",
            "DakNagoriks.dak_priority_level",
            "DakNagoriks.dak_security_level",
            "DakNagoriks.application_origin",

            "DakDaptoriks.dak_priority_level",
            "DakDaptoriks.dak_security_level",
            "DakDaptoriks.dak_subject",
            "DakDaptoriks.sender_name",
            "DakDaptoriks.sender_officer_designation_label",
            "DakDaptoriks.sender_office_name",
            "DakDaptoriks.is_summary_nothi",
            "dak_id" => 'DakMovements.dak_id',
            "modified" => 'DakMovements.modified',
            "dak_actions" => 'DakMovements.dak_actions',
            "dak_priority" => 'DakMovements.dak_priority',
            "attention_type" => 'DakMovements.attention_type',
            "dak_type" => 'DakMovements.dak_type', 'DakMovements.sequence', 'DakMovements.to_officer_designation_id', 'DakMovements.operation_type'
        ])
                              ->join([
                                  'DakMovements2' => [
                                      'table' => 'dak_movements',
                                      'type' => 'left',
                                      'conditions' => 'DakMovements.dak_id=DakMovements2.dak_id and DakMovements.dak_type=DakMovements2.dak_type and DakMovements.id < DakMovements2.id and DakMovements.from_officer_designation_id = DakMovements2.from_officer_designation_id'
                                  ],
                                  'DakDaptoriks' => [
                                      'table' => 'dak_daptoriks',
                                      'type' => 'left',
                                      'conditions' => 'DakMovements.dak_id=DakDaptoriks.id and DakMovements.dak_type="Daptorik"'
                                  ],
                                  'DakNagoriks' => [
                                      'table' => 'dak_nagoriks',
                                      'type' => 'left',
                                      'conditions' => 'DakMovements.dak_id=DakNagoriks.id and DakMovements.dak_type="Nagorik"'
                                  ],
                              ])->where($conditions)->orWhere($orConditions)->where(['DakMovements2.id IS NULL'])->where(['DakMovements.from_officer_designation_id' => $employee_office['office_unit_organogram_id']])->where(['DakMovements.operation_type' => DAK_CATEGORY_NOTHIJATO]);
        if (!empty($from_date) && !empty($end_date)) {
            $user_new_daks = $user_new_daks->where(function ($user_new_daks) use ($from_date, $end_date) {
                return $user_new_daks->between('date(DakMovements.modified)', $from_date, $end_date);
            });
        };
        $user_new_daks = $user_new_daks->page($page, $len)->order(['DakMovements.modified DESC'])->toArray();

        $totalRec = $this->find()->select(["dak_id" => 'DakMovements.dak_id', "dak_type" => 'DakMovements.dak_type'])->join([
            'DakMovements2' => [
                'table' => 'dak_movements',
                'type' => 'left',
                'conditions' => 'DakMovements.dak_id=DakMovements2.dak_id and DakMovements.dak_type=DakMovements2.dak_type and DakMovements.id < DakMovements2.id  and DakMovements.from_officer_designation_id = DakMovements2.from_officer_designation_id'
            ],
            'DakDaptoriks' => [
                'table' => 'dak_daptoriks',
                'type' => 'left',
                'conditions' => 'DakMovements.dak_id=DakDaptoriks.id and DakMovements.dak_type="Daptorik"'
            ],
            'DakNagoriks' => [
                'table' => 'dak_nagoriks',
                'type' => 'left',
                'conditions' => 'DakMovements.dak_id=DakNagoriks.id and DakMovements.dak_type="Nagorik"'
            ],
        ])->where($conditions)->orWhere($orConditions)->where(['DakMovements2.id IS NULL'])->where(['DakMovements.from_officer_designation_id' => $employee_office['office_unit_organogram_id']])->where(['DakMovements.operation_type' => DAK_CATEGORY_NOTHIJATO]);
        if (!empty($from_date) && !empty($end_date)) {
            $totalRec = $totalRec->where(function ($totalRec) use ($from_date, $end_date) {
                return $totalRec->between('date(DakMovements.modified)', $from_date, $end_date);
            });
        };

        $totalRec = $totalRec->order(['DakMovements.modified DESC'])->count();

        if ($api) {
            $response = [
                'data' => $user_new_daks,
                'total' => $totalRec,
            ];
            return $response;
        }

        $data = array();

        $priority_list = json_decode(DAK_PRIORITY_TYPE, true) + [0 => '', 1 => ''];
        $security_level_list = json_decode(DAK_SECRECY_TYPE, true) + [0 => '', 1 => ''];
        if (!empty($user_new_daks)) {
            foreach ($user_new_daks as $ke => $dak) {
                $si = (($page - 1) * $len) + $ke + 1;

                if ($dak['dak_type'] == 'Nagorik') {
                    $origin = h($dak['DakNagoriks']['name_bng']);
                    $dak_subject = ($dak['DakNagoriks']['dak_subject']);
                    $previous_sender = $origin;
                    $security_level = trim($security_level_list[$dak['DakNagoriks']['dak_security_level']]);
                } else {
                    $origin = (!empty($dak['DakDaptoriks']['sender_name']) ? (h($dak['DakDaptoriks']['sender_name']) . ', ') : '') . (!empty($dak['DakDaptoriks']['sender_officer_designation_label'])
                            ? (h($dak['DakDaptoriks']['sender_officer_designation_label']) . ", ") : '') . (!empty($dak['DakDaptoriks']['sender_office_name'])
                            ? (h($dak['DakDaptoriks']['sender_office_name'])) : '');
                    $dak_subject = ($dak['DakDaptoriks']['dak_subject']);
                    $previous_sender = $origin;
                    $security_level = trim($security_level_list[$dak['DakDaptoriks']['dak_security_level']]);
                }

                $next_receiver = "";
                $priority = '';
                $security_level = '';
                $move_date = $dak['modified'];
                $move_date = new Time($move_date);
                $move_date = $move_date->i18nFormat("dd-MM-yy HH:mm:ss", null, 'bn-BD');

                $dak_last_two_moves = $this->getDakLastMoveBy_dakid_officerId_designationId($dak['dak_id'],
                    $employee_office['officer_id'], $employee_office['office_unit_organogram_id'], $dak['dak_type']);
                $showDetailsDak = 'showDetailsDakNothiJato';

                if ($dak['dak_priority'] > 1) {
                    $priority = trim($priority_list[$dak['dak_priority']]);
                }
                if (count($dak_last_two_moves) > 0) {
                    if (!empty($dak_last_two_moves[1])) {
                        $first_move = $dak_last_two_moves[1];

                        $previous_sender = (!empty($first_move['from_officer_name']) ? (h($first_move['from_officer_name']) . ', ')
                                : '') . h($first_move['from_officer_designation_label']);
                    }
                    if (!empty($dak_last_two_moves[0])) {
                        $second_move = $dak_last_two_moves[0];
                        $next_receiver = (!empty($second_move['to_officer_name']) ? (h($second_move['to_officer_name']) . ", ")
                                : '') . h($second_move['to_officer_designation_label']) . " (" . h($second_move['to_office_unit_name']) . " )";
                    }
                }
                $dak_decision = $dak['dak_actions'];
                $totalAttachment = $dak_attachments_table->find()->where(['dak_type' => $dak['dak_type'], 'dak_id' => $dak['dak_id']])->count();

                $attachment_count_bn = "";
                if (isset($totalAttachment) && $totalAttachment > 1) {
                    $attachment_count_bn = "&nbsp;<span class='badge badge-primary'><i class='fa fa-paperclip'></i> " . enTobn($totalAttachment - 1) . "</span>";
                }
                $data[] = array(
                    '<div class="text-center">
                        <button title="বিস্তারিত"  data-dak-type="' . ($dak['dak_type']) . '" data-toggle="tooltip" data-placement="top" type="button" class="btn btn-xs btn-primary ' . $showDetailsDak . '" data-messageid=' . $dak['dak_id'] . '  data-si=' . $si . ' >
                        <i class=" a2i_gn_details2" aria-hidden="true"></i>
                        </button>
                    </div>' .
                    h($origin),
                    "<b>" . (!empty($dak_subject) ? ($dak_subject) : "") . "</b>" . $attachment_count_bn,
                    h($next_receiver),
                    ($dak_decision),
                    str_replace(" ", "<br/>", $move_date),
                    str_replace(" ", "<br/>", (h($priority) . ' ' . h($security_level))),
                    "<div class='text-center'>" . ($dak['is_summary_nothi'] == 1 ? ("<span title='সার-সংক্ষেপ' data-original-title='সার-সংক্ষেপ' data-title='সার-সংক্ষেপ' class='glyphicon glyphicon-book'></span>") : (!isset($dak['dak_type_id']) ? (($dak['dak_sending_media'] == '1') ? "<span title='দাপ্তরিক ডাক' data-original-title='দাপ্তরিক ডাক' data-title='দাপ্তরিক ডাক' class='fa fa-building-o'></span>" : "<span title='আপলোডকৃত দাপ্তরিক ডাক' data-original-title='আপলোডকৃত দাপ্তরিক ডাক' data-title='আপলোডকৃত দাপ্তরিক ডাক' class='fa fa-building'></span>") : (($dak['dak_sending_media'] == '1') ? "<span title='নাগরিক ডাক' data-original-title='নাগরিক ডাক' data-title='নাগরিক ডাক' class='fa fa-user-o'></span>" : "<span title='আপলোডকৃত নাগরিক ডাক' data-original-title='আপলোডকৃত নাগরিক ডাক' data-title='আপলোডকৃত নাগরিক ডাক' class='fa fa-user'></span>"))) . "</div>" . (!empty($dak['application_origin']) && $dak['application_origin'] != 'nothi' ? ("<div class='text-center'>(" . strtoupper($dak['application_origin'])[0] . ")</div>") : '')
                );
            }
        }

        $response = [
            'data' => $data,
            'total' => $totalRec,
        ];

        return $response;
    }

    public function countAllDakbyDesignations($designation_id = [], $status = "Inbox", $time = [])
    {

        if ($status == 'Inbox') {
            $query = $this->find()->where(['operation_type IN' => ["Sent", "Forward"]]);
        } else if ($status == 'Outbox') {
            $query = $this->find()->where(['operation_type' => "Forward"]);
        } else {
            $query = $this->find()->where(['operation_type' => $status]);
        }


        if (!empty($designation_id)) {
            if ($status == 'Inbox') {
                $query = $query->where(['to_officer_designation_id IN' => $designation_id]);
            } else {
                $query = $query->where(['from_officer_designation_id IN' => $designation_id]);
            }
        }
        if ($status == 'Outbox') {
            $query = $query->where(['attention_type' => 1]);
        }

        if (!empty($time[0])) {
            $query = $query->where(['DATE(created) >=' => $time[0]]);
        }
        if (!empty($time[1])) {
            $query = $query->where(['DATE(created) <=' => $time[1]]);
        }

        return $query->count();
    }

    public function backArchiveDak($dakData, $dak_type = 'Daptorik', $employeeOffice)
    {

        $response = [];

        try {
            $getLastMovement = $this->getDakLastMove($dakData['id'], $dak_type,
                [
                    'to_officer_designation_id' => $employeeOffice['office_unit_organogram_id']
                ]);

            if ($getLastMovement['attention_type'] != 0) {
                throw new \Exception("ডাক আর্কাইভ করা সম্ভব হচ্ছে না");
            }

            if ($dak_type == 'Daptorik') {
                TableRegistry::remove('DakDaptoriks');
                $table_instance_dd = TableRegistry::get('DakDaptoriks');
                $entity = $table_instance_dd->newEntity();
                $entity = $this->makeDaptorikTableDataset($entity, $dakData, $employeeOffice);
            } else if ($dak_type == 'Nagorik') {
                TableRegistry::remove('DakNagoriks');
                $table_instance_dd = TableRegistry::get('DakNagoriks');
                $entity = $table_instance_dd->newEntity();
                $entity = $this->makeNagorikTableDataset($entity, $dakData);
            }

            $table_instance_da = TableRegistry::get('DakAttachments');
            $dak_attachments = $table_instance_da->loadAllAttachmentByDakId($dakData['id'], $dak_type);

            $table_instance_du = TableRegistry::get('DakUsers');

            $dakUser = $table_instance_du->getData([
                'dak_id' => $dakData['id'],
                'dak_type' => $dak_type,
                'to_officer_designation_id' => $employeeOffice['office_unit_organogram_id'],
                'dak_category' => DAK_CATEGORY_INBOX,
                'is_archive' => 1,
            ])->order(['id' => 'desc'])->first();

            if (!empty($dakUser)) {
                $table_instance_du->updateAll(['is_rollback_dak' => 1], ['id' => $dakUser['id']]);
            }

            $entity['receiving_officer_id'] = $employeeOffice['officer_id'];
            $entity['receiving_officer_name'] = $employeeOffice['officer_name'];
            $entity['receiving_officer_designation_id'] = $employeeOffice['office_unit_organogram_id'];
            $entity['uploader_designation_id'] = $employeeOffice['office_unit_organogram_id'];
            $entity['receiving_officer_designation_label'] = $employeeOffice['designation_label'];
            $entity['receiving_office_id'] = $employeeOffice['office_id'];
            $entity['receiving_office_unit_id'] = $employeeOffice['office_unit_id'];
            $entity['receiving_office_unit_name'] = $employeeOffice['office_unit_name'];

            if (!empty($entity)) {
                $response = [
                    'status' => 'success', 'dak' => $entity, 'movement' => $getLastMovement, 'attachments' => $dak_attachments, 'msg' => "ডাক ফেরত আনা হয়েছে"
                ];
            }

        }
        catch (\Exception $ex) {
            $response = [
                'status' => 'error', 'dak' => [], 'movement' => [], 'msg' => ($ex->getMessage())
            ];
        }
        return $response;
    }

    private function makeNagorikTableDataset($dataset, $dak_entity)
    {
        $dataset['dak_type_id'] = $dak_entity->dak_type_id;
        $dataset['dak_subject'] = $dak_entity->dak_subject;
        $dataset['description'] = $dak_entity->description;
        $dataset['name_eng'] = $dak_entity->name_eng;
        $dataset['name_bng'] = $dak_entity->name_bng;
        $dataset['sender_name'] = $dak_entity->sender_name;
        $dataset['national_idendity_no'] = $dak_entity->national_idendity_no;
        $dataset['birth_registration_number'] = $dak_entity->birth_registration_number;
        $dataset['passport'] = $dak_entity->passport;
        $dataset['father_name'] = $dak_entity->father_name;
        $dataset['mother_name'] = $dak_entity->mother_name;
        $dataset['address'] = $dak_entity->address;
        $dataset['parmanent_address'] = $dak_entity->parmanent_address;
        $dataset['email'] = $dak_entity->email;
        $dataset['phone_no'] = $dak_entity->phone_no;
        $dataset['mobile_no'] = $dak_entity->mobile_no;
        $dataset['gender'] = $dak_entity->gender;
        $dataset['nationality'] = $dak_entity->nationality;
        $dataset['religion'] = $dak_entity->religion;
        $dataset['feedback_type'] = $dak_entity->feedback_type;
        $dataset['receive_date'] = $dak_entity->receive_date;
        $dataset['previous_receipt_no'] = $dak_entity->dak_received_no;
        $dataset['previous_docketing_no'] = $dak_entity->docketing_no;
        $dataset['dak_security_level'] = $dak_entity->dak_security_level;
        $dataset['dak_priority_level'] = $dak_entity->dak_priority_level;

        return $dataset;
    }

    private function makeDaptorikTableDataset($dataset, $dak_entity, $employee_office)
    {

        $dataset['sender_office_id'] = $dak_entity['sender_office_id'];
        $dataset['sender_officer_id'] = $dak_entity['sender_officer_id'];
        $dataset['sender_office_name'] = $dak_entity['sender_office_name'];
        $dataset['sender_office_unit_id'] = $dak_entity['sender_office_unit_id'];
        $dataset['sender_office_unit_name'] = $dak_entity['sender_office_unit_name'];
        $dataset['sender_officer_designation_id'] = $dak_entity['sender_officer_designation_id'];
        $dataset['sender_officer_designation_label'] = $dak_entity['sender_officer_designation_label'];
        $dataset['sending_date'] = date("Y-m-d H:i:s");
        $dataset['sender_name'] = $dak_entity['sender_name'];
        $dataset['sender_address'] = $dak_entity['sender_address'];
        $dataset['sender_email'] = $dak_entity['sender_email'];
        $dataset['sender_phone'] = $dak_entity['sender_phone'];
        $dataset['sender_mobile'] = $dak_entity['sender_mobile'];
        $dataset['dak_sending_media'] = $dak_entity['dak_sending_media'];
        $dataset['receiving_date'] = date("Y-m-d H:i:s");
        $dataset['dak_cover'] = $dak_entity['dak_cover'];
        $dataset['dak_subject'] = $dak_entity['dak_subject'];
        $dataset['dak_description'] = $dak_entity['dak_description'];
        $dataset['is_summary_nothi'] = $dak_entity['is_summary_nothi'];
        $dataset['previous_receipt_no'] = $dak_entity['dak_received_no'];
        $dataset['previous_docketing_no'] = $dak_entity['docketing_no'];
        $dataset['dak_security_level'] = $dak_entity->dak_security_level;
        $dataset['dak_priority_level'] = $dak_entity->dak_priority_level;

        return $dataset;
    }

    public function hasAccessInDak($dak_id, $dak_type, $designation_id)
    {
        if (empty($dak_id) || empty($dak_type) || empty($designation_id)) {
            return false;
        }
        $query = $this->find();
        if (!empty($dak_id)) {
            $query->where(['dak_id' => $dak_id]);
        }
        if (!empty($dak_type)) {
            $query->where(['dak_type' => $dak_type]);
        }
        if (!empty($designation_id)) {
            $query->where(['OR' => [['from_officer_designation_id' => $designation_id], ['to_officer_designation_id' => $designation_id]]]);
        }
        $dakMovementCount = $query->count();
        if ($dakMovementCount > 0) {
            return true;
        }

        $OtherOrganogramActivitiesSettingsTable = TableRegistry::get('OtherOrganogramActivitiesSettings');
        $OtherOrganogramActivitiesSettings = $OtherOrganogramActivitiesSettingsTable->find()->where(['assigned_organogram_id' => $designation_id, 'status' => 1])->count();
        if ($OtherOrganogramActivitiesSettings > 0) {
            return true;
        }
        return false;
    }

    public function potrojariDakTracking($dak_id, $dakType = DAK_DAPTORIK)
    {
        $response = array(
            'status' => 'error',
            'msg' => "দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না"
        );

        if ($dakType == DAK_DAPTORIK) {

            TableRegistry::remove('DakDaptoriks');
            $dak_daptoriksTable = TableRegistry::get('DakDaptoriks');
            $dakDaptoriksData = $dak_daptoriksTable->get($dak_id);
        } else {
            TableRegistry::remove('DakNagoriks');
            $dak_daptoriksTable = TableRegistry::get('DakNagoriks');
            $dakDaptoriksData = $dak_daptoriksTable->get($dak_id);
        }
        if (!empty($dakDaptoriksData)) {

            $data = [];
            TableRegistry::remove("DakMovements");

            $conditions['DakMovements.dak_id'] = $dakDaptoriksData['id'];
            $conditions['DakMovements.dak_type'] = $dakType;
            if ($dakType == DAK_DAPTORIK) {
                $tracking = $this->getLastofEachDaptorik($conditions);
            } else {
                $tracking = $this->getLastStatusofEachNaogirk($conditions);
            }
            if (empty($tracking)) $tracking = [];
            $data = array_merge($data, $tracking);

            if ($dakDaptoriksData['dak_status'] == 'NothiVukto' || $dakDaptoriksData['dak_status'] == 'NothiJato') {
                TableRegistry::remove('NothiDakPotroMaps');
                $nothiDakPotroMaps = TableRegistry::get('NothiDakPotroMaps')
                                                  ->getNothiInformationForDak($dakDaptoriksData['id'], $dakType);

                TableRegistry::remove('NothiPotroAttachments');
                $nothiPotroAttachments = TableRegistry::get('NothiPotroAttachments')
                                                      ->find()->where(['nothi_potro_id' => $nothiDakPotroMaps['nothi_potro_id'], 'potrojari_status' => 'Sent'])->count();
                if (!empty($nothiPotroAttachments)) {
                    $data[0]['dak_actions'] = "আপনার আবেদনটি পত্রজারির মাধ্যমে নিষ্পন্ন হয়েছে।";
                    $data[0]['dakcreated'] = $dakDaptoriksData['receive_date'];
                    $data[0]['dak_subject'] = $dakDaptoriksData['dak_subject'];

                    $response = array(
                        'status' => 'success',
                        'data' => $data
                    );
                    return $response;
                }

                TableRegistry::remove('NothiMasterCurrentUsers');
                $nothiMasterCurrentUsers = TableRegistry::get('NothiMasterCurrentUsers')
                                                        ->getCurrentUserDetailsWithData($nothiDakPotroMaps['nothi_masters_id'],
                                                            $nothiDakPotroMaps['nothi_part_no'],
                                                            $dakDaptoriksData['receiving_office_id']);

                if (!empty($nothiMasterCurrentUsers)) {
                    $isnisponno = $nothiMasterCurrentUsers['data']['is_finished'] === 1 ? "নিস্পত্তি করা হয়েছে" : "প্রক্রিয়াধীন আছে ";
                    $data[0]['dak_actions'] = "আপনার আবেদনটি নথিতে {$isnisponno} (বর্তমান ডেস্ক:{$nothiMasterCurrentUsers['text']} , উপস্থানের সময়: {$data[0]['movemodified']})";
                    $data[0]['dakcreated'] = $dakDaptoriksData['receive_date'];
                    $data[0]['dak_subject'] = $dakDaptoriksData['dak_subject'];

                    $response = array(
                        'status' => 'success',
                        'data' => $data
                    );
                } else {
                    $response = array(
                        'status' => 'error',
                        'msg' => "দুঃখিত! তথ্য পাওয়া যায়নি"
                    );
                }
            } else {
                if (!empty($data)) {
                    if ($data[0]['sequence'] > 1) {
                        $data[0]['dak_actions'] = "আপনার আবেদনটি প্রক্রিয়াধীন আছে (বর্তমান ডেস্ক: {$data[0]['to_officer_designation_label']}, {$data[0]['to_office_unit_name']}, গ্রহণের সময়: " . $data[0]['movemodified'] . ")";
                    }
                    $response = array(
                        'status' => 'success',
                        'data' => $data
                    );
                } else {
                    $response = array(
                        'status' => 'error',
                        'msg' => "দুঃখিত! তথ্য পাওয়া যায়নি"
                    );
                }
            }
        }
        return $response;
    }

    public function afterSave($event, $entity, $options = [])
    {
        $table_DakRegister = TableRegistry::get('DakRegister');
        $table_DakRegister->saveData($entity);
    }


    private function getAttentionType($attentionType = 0)
    {
        return ($attentionType == 0 ? ANULIPI : PRAPOK);
    }


    public function movementHistory($dak_id = null, $dakType = null)
    {
        $dak_id = isset($dak_id) ? $dak_id : $dak_id;
        $dakType = isset($dakType) ? $dakType : 'Daptorik';

        $employee_office = $this->getCurrentDakSection();
        $table_instance_dm = TableRegistry::get('DakMovements');

        $all_move_sequence = $table_instance_dm->getDakMovesBydakId($dak_id, $dakType);

        $move_data = [];
        $i = 0;
        foreach ($all_move_sequence as $move_seq) {
            $dak_moves_by_sequence = $table_instance_dm->getDakMovesBy_sequence_dakId($move_seq['sequence'],
                $dak_id, $dakType);
            $receiver_arr = array();

            foreach ($dak_moves_by_sequence as $move) {

                $receiver = $this->getAttentionType($move['attention_type']) . ": " . $move['to_officer_name'] . ", " . $move['to_officer_designation_label'];

                if ($move['to_office_id'] != $employee_office['office_id']) {
                    $receiver .= ', ' . $move['to_office_name'];
                }

                $receiver_arr[] = $receiver;
            }

            $i++;
            $row_move = isset($dak_moves_by_sequence[0]) ? $dak_moves_by_sequence[0] : array();

            $row = array();
            if (!empty($row_move)) {

                $sender = (!empty($row_move['from_officer_name']) ? ($row_move['from_officer_name'] . ', ')
                        : '') . $row_move['from_officer_designation_label'] . (($move['from_office_id']
                        != $employee_office['office_id']) ? (', ' . $row_move['from_office_name']) : '');


                $row['value'] = $row_move['dak_actions'];
                $row['move_date_time'] = $row_move['created'];
                $row['priority'] = $row_move['dak_priority'];
                $row['dak_type'] = $row_move['dak_type'];
                $row['receiver'] = $receiver_arr;
                $row['sender'] = $sender;
                $move_data[] = $row;
            }
        }
        $tracking_data = $table_instance_dm->potrojariDakTracking($dak_id, $dakType);

        if ($tracking_data['status'] == 'error') {
            $tracking_data = false;
        } else {
            $tracking_data = enTobn($tracking_data['data'][0]['dak_actions']);
        }

        $data['tracking_data'] = $tracking_data;
        $data['move_data'] = $move_data;
        return $data;
    }

    protected function getDakDaptoriksColumnsSetting()
    {
        $select = ['id', 'sender_sarok_no', 'sender_office_id', 'sender_officer_id', 'sender_office_name', 'sender_office_unit_id', 'sender_office_unit_name', 'sender_officer_designation_id', 'sender_officer_designation_label', 'sending_date', 'sender_name', 'sender_address', 'sender_email', 'sender_phone', 'sender_mobile', 'dak_sending_media', 'dak_received_no', 'docketing_no', 'receiving_date', 'dak_subject', 'dak_security_level', 'dak_priority_level', 'receiving_office_id', 'receiving_office_unit_id', 'receiving_office_unit_name', 'receiving_officer_id', 'receiving_officer_designation_id', 'receiving_officer_designation_label', 'receiving_officer_name', 'dak_status', 'is_summary_nothi', 'created_by', 'modified_by', 'uploader_designation_id', 'created', 'modified',];
        $notNeeded = ['dak_cover', 'dak_description', 'meta_data', 'previous_receipt_no', 'previous_docketing_no', 'is_rollback_to_dak'];
        return ['needed' => $select, 'notNeeded' => $notNeeded];
    }

    protected function getDakNagoriksColumnsSetting()
    {
        $select = ['id', 'docketing_no', 'dak_subject', 'dak_type_id', 'dak_received_no', 'dak_priority_level', 'dak_security_level', 'name_eng', 'name_bng', 'sender_name', 'receiving_office_id', 'receiving_office_unit_id', 'receiving_office_unit_name', 'receiving_officer_id', 'receiving_officer_designation_id', 'receiving_officer_designation_label', 'receiving_officer_name', 'receive_date', 'nothi_master_id', 'dak_status', 'uploader_designation_id', 'application_origin', 'created_by', 'modified_by', 'created', 'modified'];
        $notNeeded = ['description', 'national_idendity_no', 'birth_registration_number', 'passport', 'father_name', 'mother_name', 'address', 'parmanent_address', 'email', 'phone_no', 'mobile_no', 'gender', 'nationality', 'religion', 'feedback_type', 'application_meta_data', 'previous_receipt_no', 'previous_docketing_no', 'is_rollback_to_dak'];
        return ['needed' => $select, 'notNeeded' => $notNeeded];
    }
    public function getOnlikeDakCount($office_id = 0, $unit_id = 0, $designation_id = 0, $time = []){
        TableRegistry::remove('DakDaptoriks');
        TableRegistry::remove('DakNagoriks');
        $table_DakDaptoriks = TableRegistry::get('DakDaptoriks');
        $table_DakNagoriks = TableRegistry::get('DakNagoriks');

        $total_dak = 0;

        if(empty($office_id) && empty($unit_id) && empty($designation_id)){
            goto rtn;
        }

        $query_table_DakDaptoriks = $table_DakDaptoriks->find();
        if (!empty($office_id)) {
            $query_table_DakDaptoriks->where(['receiving_office_id' => $office_id]);
        }
        if (!empty($unit_id)) {
            $query_table_DakDaptoriks->where(['receiving_office_unit_id' => $unit_id]);
        }
        if (!empty($designation_id)) {
            $query_table_DakDaptoriks->where(['receiving_officer_designation_id' => $designation_id]);
        }
        if (!empty($time[0])) {
            $query_table_DakDaptoriks->where(['DATE(created) >=' => $time[0]]);
        }
        if (!empty($time[1])) {
            $query_table_DakDaptoriks->where(['DATE(created) <=' => $time[1]]);
        }

        $total_dak += $query_table_DakDaptoriks->where(['sender_sarok_no is not null','dak_sending_media' => 1,'dak_status <>' => 'Draft' ])->count();


        $query_table_DakNagoriks = $table_DakNagoriks->find();
        if (!empty($office_id)) {
            $query_table_DakNagoriks->where(['receiving_office_id' => $office_id]);
        }
        if (!empty($unit_id)) {
            $query_table_DakNagoriks->where(['receiving_office_unit_id' => $unit_id]);
        }
        if (!empty($designation_id)) {
            $query_table_DakNagoriks->where(['receiving_officer_designation_id' => $designation_id]);
        }
        if (!empty($time[0])) {
            $query_table_DakNagoriks->where(['DATE(created) >=' => $time[0]]);
        }
        if (!empty($time[1])) {
            $query_table_DakNagoriks->where(['DATE(created) <=' => $time[1]]);
        }

        $total_dak += $query_table_DakNagoriks->where(['application_origin <>' => 'nothi','dak_status <>' => 'Draft' ])->count();

        rtn:
        return $total_dak;
    }
}
