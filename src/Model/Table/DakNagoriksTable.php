<?php

namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

class DakNagoriksTable extends ProjapotiTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
    }

    public function validationAdd($validator)
    {
        $validator
            ->notEmpty('name_bng', "নাম (বাংলা) দিন")
            ->notEmpty('present_address', "বর্তমান ঠিকানা দিন")
            ->notEmpty('parmanent_address', "স্থায় ঠিকানা দিন")
            ->notEmpty('dak_subject', "বিষয় দিন");

        return $validator;
    }
    public function getNagorikDaks($ids){
        if(empty($ids))
            return;
        return $this->find()->select(['id','dak_subject', 'Utsho' =>'sender_name','receive_date'=>'modified','dak_type_id'])->where(['id IN'=>$ids]);
    }

}
