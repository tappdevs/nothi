<?php

namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

class DakOnlineAttachmentsTable extends ProjapotiTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->addBehavior('Cake3Upload.Upload', [
                'fields' => [
                    'file_name' => [
                        'path' => 'upload/DAK/:filename_prefix',
                    ]
                ]
            ]
        );


        $this->belongsTo('DakOnlineNagoriks', [
            'foreignKey' => 'dak_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Find All Attachments by dak id
     */
    public function loadAllAttachmentByDakId($dak_id)
    {
        return $this->find()
            ->select(['id', 'file_name', 'attachment_type'])
            ->where(['dak_id' => $dak_id])->toArray();
    }

}
