<?php

namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Aura\Intl\Exception;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;

class DakRegisterTable extends ProjapotiTable
{
    public function initialize(array $config) {
        $conn = ConnectionManager::get('default');
        $this->connection($conn);
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->table('dak_register');
    }
    public function saveData($dak_movement_entity){
        if(isset($dak_movement_entity['id'])){
            $dak_movement_entity['movement_id'] = $dak_movement_entity['id'];
        }else{
          throw new \Exception('DakRegister: Dak Movement Id not found');
        }
        if(isset($dak_movement_entity['dak_id']) && isset($dak_movement_entity['dak_type'])){
            if($dak_movement_entity['dak_type'] == 'Daptorik'){
                $coreTable = TableRegistry::get('DakDaptoriks');
                $coreInfo = $coreTable->find()->where(['id' => $dak_movement_entity['dak_id']])->first()->toArray();
            }
            elseif ($dak_movement_entity['dak_type'] == 'Nagorik'){
                $coreTable = TableRegistry::get('DakNagoriks');
                $coreInfo = $coreTable->find()->where([ 'id' =>$dak_movement_entity['dak_id']])->first()->toArray();
            }
            $time_of_movement = $dak_movement_entity['created'];
            $time_of_dak_created = $coreInfo['created'];

            $dak_movement_entity = json_encode($dak_movement_entity);
            $dak_movement_array = json_decode($dak_movement_entity,true);

            $data2Save = $coreInfo + $dak_movement_array;

            $data2Save['movement_time'] =  $time_of_movement;
            $data2Save['dak_created'] =  $time_of_dak_created;

            //if operation type nothivukto or nothijat
            if($data2Save['operation_type'] == DAK_CATEGORY_NOTHIJATO || $data2Save['operation_type'] == DAK_CATEGORY_NOTHIVUKTO){
                $data2Save =  $this->saveDataOfPotro($data2Save['dak_id'],$data2Save['dak_type'],$data2Save);
            }


            if(isset($data2Save['created'])){
                unset($data2Save['created']);
            }
            if(isset($data2Save['modified'])){
                unset($data2Save['modified']);
            }
            if(isset($data2Save['id'])){
                unset($data2Save['id']);
            }

            $data2Save = $this->patchEntity($this->newEntity(),$data2Save);

            if(!$this->save($data2Save)){
                throw new \Exception('DakRegister: Data not saved');
            }

        }else{
            throw new \Exception('DakRegister: Dak Id and Dak type not found');
        }
    }
    public function generatePotrojariDakList($options = []){
        /*Set Variables for further query*/
        $startDate = isset($options['startDate'])?$options['startDate']:'';
        $endDate = isset($options['endDate'])?$options['endDate']:'';
        $condition = isset($options['condition'])?$options['condition']:[];
        $unit_id_array = isset($options['unit_id_array'])?$options['unit_id_array']:[];
        /*Set Variables for further query*/


        $query = $this->find()->select([
            'dak_id',
            'sender_sarok_no',
            'created',
            'office_name' => 'sender_office_name',
            'officer_name' => 'sender_name',
            'office_unit_id' => 'sender_office_unit_id',
            'office_unit_name' => 'sender_office_unit_name',
            'officer_designation_label' => 'sender_officer_designation_label',
            'potro_subject' => 'dak_subject',
            'docketing_no',
            'dak_received_no',
            'potro_security_level' => 'dak_priority_level',
            'potro_security_level' => 'dak_security_level',
            'potrojari_date' => 'dak_created',
        ])
          ->where(["dak_sending_media" => 1,'sender_sarok_no IS NOT NULL', 'sender_officer_designation_id >' => 0])
          ->where(["receiving_office_unit_id IN" => $unit_id_array])
          ->where(function($query) use ($startDate,$endDate){
              return $query->between('date(dak_created)',$startDate,$endDate);
          })
          ->order(['DakRegister.dak_created' => 'DESC']);

        return $query;
    }
    public function saveDataOfPotro($dak_id,$dak_type,$data){
        //update nothi_master_id,nothi_part_id,nothi_potro_id,nothijato
        $entity = TableRegistry::get('NothiDakPotroMaps')->find()->where(['dak_id' => $dak_id,'dak_type' => $dak_type])->first();
        if(!empty($entity['nothi_masters_id']) && !empty($entity['nothi_part_no']) && !empty($entity['nothi_potro_id'])){
            $data['nothi_master_id'] = $entity['nothi_masters_id'];
            $data['nothi_part_id'] = $entity['nothi_part_no'];
            $data['nothi_potro_id'] = $entity['nothi_potro_id'];

        }
        return $data;
    }
}