<?php

namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\Datasource\ConnectionManager;
use Cake\Controller\Component;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

class DashboardUnitsTable extends ProjapotiTable
{

    public function initialize(array $config)
    {
        //parent::initialize($config);
        $conn = ConnectionManager::get('NothiReportsDb');
        $this->connection($conn);
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
    }
       public function checkUpdate($unit_id = 0, $date = '')
    {
        if (!empty($date) && !empty($unit_id)) {
            return $this->find()->where(['operation_date' => $date, 'unit_id' => $unit_id])->count();
        }
    }
       public function getLastUpdateDate($unit_id = 0)
    {
        if ( !empty($unit_id)) {
               return $this->find()->select(['operation_date'])->where(['unit_id' => $unit_id])->order(['operation_date desc'])->first();
        }
    }
     public function getData($unit_id = 0){
        if(empty($unit_id))
            return;
               $condition = [
            'totalInbox' => 'dak_inbox', 'totalOutbox' => 'dak_outbox', 'totalNothijato' => 'nothijat',
            'totalNothivukto' => 'nothivukto', 'totalNisponnoDak' => 'dak_nisponno','totalOnisponnodakall' => 'dak_onisponno',

            'totalSouddog' => 'self_note','totalDaksohoNote' => 'dak_note',
            'totalNisponnoNote' => 'note_nisponno','totalNisponnoPotrojari' =>'potrojari_nisponno',
            'totalONisponno' => 'onisponno_note',

            'totalyesterdayinbox'=>'yesterday_inbox','totalyesterdaynothivukto' => 'yesterday_nothivukto','totalyesterdaynothijato' => 'yesterday_nothijat',
            'totalyesterdaypotrojari' => 'yesterday_potrojari','totalyesterdayOnisponnodak' => 'yesterday_onisponno_dak',

            'totalyesterdaydaksohonote' => 'yesterday_dak_note', 'totalyesterdaysrijitonote' => 'yesterday_self_note','totalyesterdaynisponnonote' => 'yesterday_niponno_note','totalyesterdaynisponnopotrojari' => 'yesterday_nisponno_potrojari', 'totalyesterdayOnisponnonote'=> 'yesterday_onisponno_note',

            'totalID' => 'count(id)', 'totalPotrojari' => 'potrojari'
        ];
        return  $this->find()->select($condition)->where(['unit_id' => $unit_id])->first();
    }
}