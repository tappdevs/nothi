<?php
namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;


class DigitalSignatureTable extends ProjapotiTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
//    public $apiUrl = "https://103.48.17.166:8443/SignerApp-0.0.1-SNAPSHOT/sign";
//    public $apiUrlForCertificate = "https://103.48.17.166:8443/SignerApp-0.0.1-SNAPSHOT/certinfo";
    public $apiUrl = "https://signer.nothi.gov.bd:8443/SignerApp-0.0.1-SNAPSHOT/sign";
    public $apiUrlForCertificate = "https://signer.nothi.gov.bd:8443/SignerApp-0.0.1-SNAPSHOT/certinfo";
    public $passPhrase = "Pixel357";
    public $clientKey = "digital_signature/client.key";
    public $clientCrt =  "digital_signature/client.crt";
    public function requestToSign($data = [],$signables = []){
        $response = ['status' => 'error','msg' =>''];
        $signatureRequest = new \SignatureRequest($data['userId'],  $data['userIdtype'] ,$data['caName'] ,$data['certSerialNumber'], $data['passPhrase'], $signables);
        $signingManager = new \SigningManager($this->apiUrl, CONFIG.$this->clientKey, CONFIG.$this->clientCrt, $this->passPhrase);
//        pr($signatureRequest);
//        pr($signingManager);
        $signingResult = $signingManager->GetSigningResponseAsJsonArray($signatureRequest);
//        pr($signingResult);
//        die;
        if(!empty($signingResult)){
            if(!empty($signingResult->hasError)){
                $response['msg']=$signingResult->errDescription;
            }else{
                 $response = ['status' => 'success','data' =>$signingResult];
            }
            return $response;
        }
    }
    public function encodeObject($data = '',$type =''){
        $base64Encoder= new \Base64Encoder();
        if($type=='html'){
            return $base64Encoder->EncodeHtml($data);
        }
        else if($type == 'text'){
            return $base64Encoder->EncodeText($data);
        }
        else if($type == 'image'){
            return $base64Encoder->EncodeNonIntrinsic($data);//filename
        }
         // Pdf, Word, Excel, PowerPoint etc.
        else{
             return $base64Encoder->EncodeIntrinsic($data);//filename
        }
    }
    public function verifySign($data_to_verify,$dataType = 'html',$publickey,$signature){
        try{
            $signable = new \NonIntrinsicSignableObject(time(), $this->encodeObject($data_to_verify,$dataType), null);
            $PublicKey =  "-----BEGIN PUBLIC KEY-----\n" . chunk_split($publickey, 64,"\n") . '-----END PUBLIC KEY-----';
            return openssl_verify($signable->base64Data, base64_decode($signature), $PublicKey, OPENSSL_ALGO_SHA256);
        } catch (\Exception $ex) {
            return 0;
        }
       
    }
    public function getCertificate($identityProofType = '',$identityProofNo = ''){
        $response = ['status' => 'error','msg' =>' Something went wrong'];
        if(empty($identityProofNo) || empty($identityProofType)){
            $response['msg'] = 'সকল তথ্য সঠিকভাবে দেওয়া হয়নি।';
            goto rtn;
            }
            //prepare api request
            try{
                        $certInfo = new \UserCertInformation();
                        $certInfo->identityProofType = $identityProofType;
                        $certInfo->identityProofNo = $identityProofNo;
            //            $data = json_encode(['identityProofType' => $identityProofType,'identityProofNo' => $identityProofNo]);
                        $data = json_encode($certInfo);

                        $obj = \Httpful\Request::get($this->apiUrlForCertificate)
                                ->authenticateWithCert(CONFIG.$this->clientCrt, CONFIG.$this->clientKey, $this->passPhrase)
                                ->body($data)
                                ->send();
                        //prepare api request
                        if(!empty($obj) && isset($obj->body)){
                            $response=['status' => 'success','data' => $obj->body];
                        }else{
                              $response=['status' => 'error','msg' => 'Digital Signature unable to handle request'];
                        }
            } catch (\Exception $ex) {
                $response['msg'] = $ex->getMessage();
            }

        rtn:
            return $response;
    }
    public function saveSoftToken($employee_office,$soft_token){
        if(isset($employee_office['username']) && isset($employee_office['officer_id']) && isset($soft_token)){
            $tableDsUserInfo = TableRegistry::get('DsUserInfo');

            // if username not exist, need to fetch one
            if(empty($employee_office['username'])){
                $userIfo = TableRegistry::get('Users')->getData(['username'],['employee_record_id' => $employee_office['officer_id']])->first();
                if(!empty($userIfo['username'])){
                    $employee_office['username'] = $userIfo['username'];
                }
            }
//            // first check if any soft token exist
//            $has_data = $tableDsUserInfo->getData([],[
//                'username' => $employee_office['username'],
//                'employee_record_id' => $employee_office['officer_id'],
//                'expired_at >=' => date('Y-m-d H:i:s')
//            ])->count();
//            if($has_data > 0){
//                return true;
//            }
            //delete previous user data if exist
            $tableDsUserInfo->deleteAll([
                'username' => $employee_office['username'],
                'employee_record_id' => $employee_office['officer_id'],
            ]);
            // save new data
            $tableDsUserInfo->setData([
                'username' => $employee_office['username'],
                'employee_record_id' => $employee_office['officer_id'],
                'soft_token' => $soft_token,
                'expired_at' => date('Y-m-d H:i:s', strtotime("+30 min"))
            ]);
        }
    }
}
