<?php
namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use  Cake\Datasource\ConnectionManager;

class EmployeeCadresTable extends ProjapotiTable
{


	public function initialize(array $config)
	{
		$conn = ConnectionManager::get('projapotiDb');
		$this->connection($conn);
	}

}