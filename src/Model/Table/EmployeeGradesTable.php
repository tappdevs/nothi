<?php

namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;

class EmployeeGradesTable extends ProjapotiTable
{

    public function initialize(array $config)
    {
        $conn = ConnectionManager::get('projapotiDb');
        $this->connection($conn);
          $this->table('employee_grades');
        $this->displayField('grade_name_bng');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->belongsTo('EmployeeCadres',
            [
            'foreignKey' => 'employee_cadre_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('EmployeeRanks',
            [
            'foreignKey' => 'employee_rank_id',
            'joinType' => 'INNER'
        ]);
    }

    public function validationDefault(Validator $validator)
    {
        $validator = new Validator();
        $validator->notEmpty('grade_name_bng',
            ' গ্রেডের   নাম(বাংলা) দেওয়া হয় নি')->notEmpty('grade_name_eng',
            ' গ্রেডের   নাম(ইংরেজি) দেওয়া হয় নি')->notEmpty('employee_cadre_id',
            'ক্যাডার নির্বাচন করুন')->notEmpty('employee_rank_id',
            'পদ নির্বাচন করুন');
        return $validator;
    }
}