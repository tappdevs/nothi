<?php

namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;

class EmployeeOfficesTable extends ProjapotiTable
{

    public function initialize(array $config)
    {
        $conn = ConnectionManager::get('projapotiDb');
        $this->connection($conn);
        $this->displayField('designation');
        $this->addBehavior('Timestamp');

        $this->belongsTo('Offices', [
            'foreignKey' => 'office_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('OfficeUnits', [
            'foreignKey' => 'office_unit_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('OfficeUnitOrganograms', [
            'foreignKey' => 'office_unit_organogram_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('EmployeeRecords', [
            'foreignKey' => 'employee_record_id',
            'joinType' => 'INNER'
        ]);
    }

    public function beforeSave($event, $entity, $options){
        if (empty($entity['office_name_en']) || empty($entity['office_name_bn'])) {
            if (!empty($entity['office_id'])) {
                $offices_table = TableRegistry::get('Offices');
                $office = $offices_table->get($entity['office_id']);
                if (!empty($office['office_name_eng'])) {
                    $entity['office_name_en'] = $office['office_name_eng'];
                }
                if (!empty($office['office_name_bng'])) {
                    $entity['office_name_bn'] = $office['office_name_bng'];
                }
            }

        }
        if (empty($entity['unit_name_en']) || empty($entity['unit_name_bn'])) {
            if (!empty($entity['office_unit_id'])) {
                $office_units_table = TableRegistry::get('OfficeUnits');
                $office_unit = $office_units_table->get($entity['office_unit_id']);
                if (!empty($office_unit['unit_name_eng'])) {
                    $entity['unit_name_en'] = $office_unit['unit_name_eng'];
                }
                if (!empty($office_unit['unit_name_bng'])) {
                    $entity['unit_name_bn'] = $office_unit['unit_name_bng'];
                }
            }

        }
        if (empty($entity['designation_en'])) {
            if (!empty($entity['office_unit_organogram_id'])) {
                $office_unit_organogram_table = TableRegistry::get('OfficeUnitOrganograms');
                $office_unit_organogram = $office_unit_organogram_table->get($entity['office_unit_organogram_id']);
                if ($office_unit_organogram['designation_eng']) {
                    $entity['designation_en'] = $office_unit_organogram['designation_eng'];
                }
            }
        }
    }

    public function getEmployeeOffices($employee_record_id)
    {

        $data = $this->find()->select(['office_id'])->where(['employee_record_id' => $employee_record_id])->toArray();

        return $data;
    }

    public function getEmployeeOfficeRecords($employee_record_id,$api = 0)
    {

        $table_office = TableRegistry::get('Offices');
        $table_office_unit = TableRegistry::get('OfficeUnits');
        $data = array();
        $data_records = $this->find()->where(['employee_record_id' => $employee_record_id, 'status' => 1])
            ->order(['incharge_label ASC,designation_level ASC, designation_sequence ASC'])->toArray();
        foreach ($data_records as $record) {
            $row = $record;
            $office = $table_office->get($row['office_id']);
            $row['office_name'] = $office['office_name_bng'];
            $row['division_id'] = $office['geo_division_id'];
            $row['district_id'] = $office['geo_district_id'];
            $row['upazila_id'] = $office['geo_upazila_id'];
            $row['union_id'] = $office['geo_union_id'];
            if($api == 1){
                if($record['show_unit'] == 1){
                     $office_unit = $table_office_unit->find()->where(['id' => $row['office_unit_id']])->first();
                    if (!empty($office_unit)) {
                        $row['unit_name'] = $office_unit['unit_name_bng'];
                    } else $row['unit_name'] = '';
                }else{
                    $row['unit_name'] = '';
                }
            }else{
                 $office_unit = $table_office_unit->find()->where(['id' => $row['office_unit_id']])->first();
                if (!empty($office_unit)) {
                    $row['unit_name'] = $office_unit['unit_name_bng'];
                } else $row['unit_name'] = '';
            }
           
            $data[] = $row;
        }
        return $data;
    }

    public function getEmployeeOfficeRecordsByOfficeId($office_id, $unit_id = 0, $designation_id = 0,$status = 1,$select = [])
    {

        if(empty($select)){
            $select = [
                'EmployeeOffices.employee_record_id',
                'EmployeeOffices.office_id',
                'EmployeeOffices.office_unit_id',
                'EmployeeOffices.office_unit_organogram_id',
                'EmployeeOffices.designation_level',
                'EmployeeOffices.status',
                'EmployeeOffices.incharge_label',
                'EmployeeOffices.office_head',
                'EmployeeOffices.show_unit',
                'OfficeUnits.unit_name_bng',
                'soft_signature' => 'EmployeeRecords.soft_signature',
                'hard_signature' => 'EmployeeRecords.hard_signature',
                'name_bng' => 'EmployeeRecords.name_bng',
                'name_eng' => 'EmployeeRecords.name_eng',
                'personal_email' => 'EmployeeRecords.personal_email',
                'personal_mobile' => 'EmployeeRecords.personal_mobile',
                'designation' => 'OfficeUnitOrganograms.designation_bng',
                'designation_eng' => 'OfficeUnitOrganograms.designation_eng',
                ];
        }
        $data_records = $this->find();
        $data_records->select($select);
        if (!empty($office_id)) {
            $data_records = $data_records->where(['EmployeeOffices.office_id' => $office_id]);
        }
        if (!empty($unit_id)) {
            $data_records = $data_records->where(['EmployeeOffices.office_unit_id' => $unit_id]);
        }
        if (!empty($designation_id)) {
            if(is_array($designation_id)){
                $data_records = $data_records->where(['EmployeeOffices.office_unit_organogram_id IN' => $designation_id]);
            }else{
                $data_records = $data_records->where(['EmployeeOffices.office_unit_organogram_id' => $designation_id]);
            }
        }
        $data_records = $data_records->join([
            'EmployeeRecords' => [
                'table' => 'employee_records',
                'type' => 'inner',
                'conditions' => 'EmployeeRecords.id = EmployeeOffices.employee_record_id'
            ]
        ]);
        if ($status != 2) {
            $data_records->where(['EmployeeOffices.status' => $status]);
        }
        $data_records->where(['OfficeUnitOrganograms.status' => 1]); // show only active designation
        $data_records = $data_records->join([
            'OfficeUnitOrganograms' => [
                'table' => 'office_unit_organograms',
                'type' => 'inner',
                'conditions' => 'OfficeUnitOrganograms.id = EmployeeOffices.office_unit_organogram_id'
            ],
            'OfficeUnits' => [
                'table' => 'office_units',
                'type' => 'inner',
                'conditions' => 'OfficeUnits.id = EmployeeOffices.office_unit_id'
            ]
        ])->order(['EmployeeOffices.status DESC, OfficeUnits.id ASC, EmployeeOffices.designation_level ASC, EmployeeOffices.designation_sequence ASC'])
            ->group(['EmployeeOffices.office_unit_organogram_id', 'EmployeeOffices.status'])
            ->toArray();

        return $data_records;
    }

    public function getEmployeeOfficeRecordsWithOfficeUnitOrganogramByOfficeId($office_id)
    {
        $data = array();

        $employeeRecordTable = TableRegistry::get('EmployeeRecords');

        $data_records = $this->find()
            ->select([
                'EmployeeOffices.employee_record_id',
                'EmployeeOffices.office_id',
                'EmployeeOffices.office_unit_id',
                'EmployeeOffices.office_unit_organogram_id',
                'EmployeeOffices.status',
                'EmployeeOffices.incharge_label',
                'name_bng' => 'EmployeeRecords.name_bng',
                'designation_eng'=>'OfficeUnitOrganograms.designation_eng',
                'designation_bng'=>'OfficeUnitOrganograms.designation_bng'
            ]);
        if (!empty($office_id)) {
            $data_records = $data_records->where(['EmployeeOffices.office_id' => $office_id]);
        }
        $data_records = $data_records->join([
            'EmployeeRecords' => [
                'table' => 'employee_records',
                'type' => 'inner',
                'conditions' => 'EmployeeRecords.id = EmployeeOffices.employee_record_id'
            ],
            'OfficeUnitOrganograms' => [
                'table' => 'office_unit_organograms',
                'type' => 'inner',
                'conditions' => 'EmployeeOffices.office_unit_organogram_id = OfficeUnitOrganograms.id'
            ]
        ])->where(['EmployeeOffices.status' => 1])
            ->order(['EmployeeOffices.designation_level ASC, EmployeeOffices.designation_sequence ASC']);


        return $data_records;
    }

    public function getAllEmployeeRecords($office_id, $unit_id = 0, $designation_id = 0)
    {
        $data = array();

        $employeeRecordTable = TableRegistry::get('EmployeeRecords');

        $data_records = $this->find()
            ->select([
                'EmployeeOffices.employee_record_id',
                'EmployeeOffices.office_id',
                'EmployeeOffices.office_unit_id',
                'EmployeeOffices.office_unit_organogram_id',
                'EmployeeOffices.designation',
                'EmployeeOffices.designation_level',
                'EmployeeOffices.status',
                'name_bng' => 'EmployeeRecords.name_bng'
            ]);
        if (!empty($office_id)) {
            $data_records = $data_records->where(['EmployeeOffices.office_id' => $office_id]);
        }
        if (!empty($unit_id)) {
            $data_records = $data_records->where(['EmployeeOffices.office_unit_id' => $unit_id]);
        }
        if (!empty($designation_id)) {
            $data_records = $data_records->where(['EmployeeOffices.office_unit_organogram_id' => $designation_id]);
        }
        $data_records = $data_records->join([
            'EmployeeRecords' => [
                'table' => 'employee_records',
                'type' => 'left',
                'conditions' => 'EmployeeRecords.id = EmployeeOffices.employee_record_id'
            ]
        ])
            ->order(['designation_level ASC, designation_sequence ASC'])->toArray();


        return $data_records;
    }

    public function getEmployeeOfficeRecordsByOffice($office_id, $unit_id = 0)
    {
        $data_records = array();

        if (!empty($unit_id)) {
            $data_records = $this->find()->where(['office_id' => $office_id, 'office_unit_id' => $unit_id,
                'status' => 1])->order(['office_unit_id DESC, designation_level ASC'])->toArray();
        } else {
            $data_records = $this->find()->distinct(['office_unit_organogram_id'])->where(['office_id' => $office_id,
                'status' => 1])->order(['office_unit_id DESC,designation_level ASC'])->toArray();
        }

        return $data_records;
    }

    public function getEmployeeInfo($office_id, $unit_id, $designation_id)
    {
        return $this->find()->where(['office_id' => $office_id, 'office_unit_id' => $unit_id, 'office_unit_organogram_id' => $designation_id,
            'EmployeeOffices.status' => 1])->first();
    }

    public function getDesignationInfo($designation_id, $status = 1)
    {
        $query = $this->find()->where(['office_unit_organogram_id' => $designation_id]);
        if ($status != -1) {
            $query->where(['status' => $status]);
        }

        return $query->order(['id desc'])->first();
    }

    public function getDesignationDetailsInfo($designation_id)
    {
        return $this->find()->select(['EmployeeRecords.name_bng','EmployeeRecords.identity_no',
            'EmployeeRecords.personal_email', 'employee_record_id', 'EmployeeOffices.id',
            'office_unit_organogram_id', 'office_unit_id', 'office_id',
            "designation_label" => 'OfficeUnitOrganograms.designation_bng',
            "designation_description" => 'OfficeUnitOrganograms.designation_description'])->where(['office_unit_organogram_id' => $designation_id,
            'EmployeeOffices.status' => 1])
            ->join([
                'EmployeeRecords' => [
                    'table' => 'employee_records',
                    'type' => 'inner',
                    'conditions' => 'EmployeeRecords.id = EmployeeOffices.employee_record_id'
                ],
                'OfficeUnitOrganograms' => [
                    'table' => 'office_unit_organograms',
                    'type' => 'inner',
                    'conditions' => 'OfficeUnitOrganograms.id = EmployeeOffices.office_unit_organogram_id and OfficeUnitOrganograms.status = 1'
                ]
            ])->hydrate(false)->first();
    }

    public function getDesignationSeq($office_id, $unit_id, $designation_id)
    {
        return $this->find()->where(['office_id' => $office_id, 'office_unit_id' => $unit_id, 'office_unit_organogram_id' => $designation_id,
            'status' => 1])->first();
    }

    public function getEmployeeRoleHistory($employee_record_id, $status = 1)
    {
        $table_office = TableRegistry::get('Offices');
        $table_office_unit = TableRegistry::get('OfficeUnits');
        $data = array();
        $data_records = $this->find()->where(['employee_record_id' => $employee_record_id])->order(['is_default_role DESC, status DESC, joining_date DESC']);
        if($status != -1){
            $data_records->where(['status' => $status]);
        }
        $data_records = $data_records->toArray();
        foreach ($data_records as $record) {
            $row = $record;
            $office = $table_office->get($row['office_id']);
            $row['office_name'] = $office['office_name_bng'];
            $office_unit = $table_office_unit->get($row['office_unit_id']);
            $row['unit_name'] = $office_unit['unit_name_bng'];
            $data[] = $row;
        }
        return $data;
    }

    public function getAll($conditions = null, $select = null)
    {

        $queryString = $this->find();

        if (!empty($select)) {
            $queryString = $queryString->select($select);
        }

        if (!empty($conditions)) {
            $queryString = $queryString->where($conditions);
        }
        return $queryString->order(['designation_level ASC']);
    }

    public function getAllEmployeeRecordID($office_id = 0, $unit = 0, $designation = 0)
    {
        $query = $this->find('list', ['keyField' => 'id', 'valueField' => 'employee_record_id'])->where(['status' => 1])->distinct(['employee_record_id']);

        if (!empty($office_id)) {
            $query = $query->where(['office_id' => $office_id]);
        }

        if (!empty($unit)) {
            $query = $query->where(['office_unit_id' => $unit]);
        }

        if (!empty($designation)) {
            $query = $query->where(['office_unit_organogram_id' => $designation]);
        }
        return $query->toArray();
    }

    public function getAllDesignationID()
    {
//        $query = $this->find('list',
//                ['keyField' => 'office_unit_organogram_id', 'valueField' => 'designation'])->where(['status' => 1])->distinct(['office_unit_organogram_id']);
        $query = $this->find('list', ['keyField' => 'office_unit_organogram_id', 'valueField' => 'designation'])->select(['designation' => 'concat(designation,", ",Offices.office_name_bng)', 'EmployeeOffices.office_unit_organogram_id', 'office_id'])->join([
            "Offices" => [
                'table' => 'offices', 'type' => 'inner',
                'conditions' => ['Offices.id = EmployeeOffices.office_id']
            ],
            "OfficeDomains" => [
                'table' => 'office_domains', 'type' => 'inner',
                'conditions' => ['Offices.id = OfficeDomains.office_id']
            ]
        ])->where(['EmployeeOffices.status' => 1,'EmployeeOffices.office_unit_organogram_id <>' => 0])->distinct(['EmployeeOffices.office_unit_organogram_id'])->order([
            'EmployeeOffices.designation_level ASC','EmployeeOffices.designation_sequence ASC'
        ]);
        return $query;
    }

    public function getAllUnitID()
    {
//        $query = $this->find('list',
//                ['keyField' => 'office_unit_organogram_id', 'valueField' => 'designation'])->where(['status' => 1])->distinct(['office_unit_organogram_id']);
        $query = $this->find('list', ['keyField' => 'office_unit_id', 'valueField' => 'unit_name_bng'])->select(['unit_name_bng' => 'concat(Units.unit_name_bng,", ",Offices.office_name_bng)', 'EmployeeOffices.office_unit_id', 'office_id'])->join([
            "Offices" => [
                'table' => 'offices', 'type' => 'inner',
                'conditions' => ['Offices.id = EmployeeOffices.office_id']
            ],
            "Units" => [
                'table' => 'office_units', 'type' => 'inner',
                'conditions' => ['Units.id = EmployeeOffices.office_unit_id']
            ],
            "OfficeDomains" => [
                'table' => 'office_domains', 'type' => 'inner',
                'conditions' => ['Offices.id = OfficeDomains.office_id']
            ]
        ])->where(['EmployeeOffices.status' => 1,'EmployeeOffices.office_unit_id <>' => 0])->distinct(['EmployeeOffices.office_unit_id']);
        return $query;
    }

    public function getCountOfEmployeeOfOffices($office_id)
    {
        return $this->find()->where(['office_id' => $office_id, 'status' => 1])->distinct(['employee_record_id'])->count();
    }

    public function getCountOfEmployeeOfOfficeUnites($office_id, $unit_id)
    {
        if (!empty($office_id)) {
            return $this->find()->where(['office_id' => $office_id, 'office_unit_id' => $unit_id, 'status' => 1])->distinct(['employee_record_id'])->count();
        } else {
            return $this->find()->where(['office_unit_id' => $unit_id, 'status' => 1])->distinct(['employee_record_id'])->count();
        }
    }

    public function getAbsentId($office_id = 0, $unit_id = 0, $organogram_id = 0, $time = '', $order = [])
    {
        $query = $this->find()->select([
            "designation",
            "office_id",
            "office_unit_id",
            'employee_record_id'
        ]);

        if (!empty($office_id)) {
            $query->where(['EmployeeOffices.office_id' => $office_id]);
        }

        if (!empty($unit_id)) {
            $query->where(['EmployeeOffices.office_unit_id' => $unit_id]);
        }

        if (!empty($organogram_id)) {
            $query->where(['EmployeeOffices.office_unit_organogram_id' => $organogram_id]);
        }


        if (!empty($order)) {
            $query->order($order);
        }

        $query = $query->where(['EmployeeOffices.status' => 1])->distinct(['EmployeeOffices.office_unit_organogram_id'])->toArray();
        $userLoginHistoriesTable = TableRegistry::get('UserLoginHistory');
        $data = [];
        if (!empty($query)) {
            foreach ($query as $val) {
                $has_data = $userLoginHistoriesTable->find()->where(['employee_record_id' => $val['employee_record_id'], 'date(login_time)' => $time])->count();
                if ($has_data == 0) {
                    $data[] = $val;
                }
            }
        }
        return $data;
    }

    public function checkOfficeHeadExist($office_id = 0)
    {
        return $this->find()->where(['office_head' => 1, 'office_id' => $office_id])->count();
    }

    public function getOfficeDesignation($employee_record_id, $designation_id)
    {
        return $this->find()->where(['employee_record_id' => $employee_record_id, 'office_unit_organogram_id' => $designation_id])->order(['id desc'])->first();
    }

    public function getAllDesignationByOfficeOrUnitID($office_id = 0, $unit_id = 0,$status = 0){
        $query = $this->find('list',['valueField' => 'office_unit_organogram_id']);
        if(!empty($office_id)){
             $query = $query->where(['office_id' => $office_id]);
        }
        if(!empty($unit_id)){
            $query = $query->where(['office_unit_id' => $unit_id]);
        }
        if(!empty($status)){
            $query = $query->where(['status' => $status]);
        }

        return $query->where(['office_unit_organogram_id <>' => 0 ]) ;
    }

    public function getEmployeeEnglishInfo($emplyee_record_id = 0,$office_id = 0, $unit_id = 0,$designation_id = 0){
        $english_info = [];
        if(!empty($emplyee_record_id) && empty($office_id) && empty($unit_id) && empty($designation_id)){
           $query = $this->find()->select(['office_id','office_unit_id','office_unit_organogram_id'])->where(['employee_record_id' =>$emplyee_record_id,'status' => 1])->first();
           if(!empty($query['office_id'])){
               $office_id = $query['office_id'];
           }
           if(!empty($query['office_unit_id'])){
               $unit_id = $query['office_unit_id'];
           }
           if(!empty($query['office_unit_organogram_id'])){
               $designation_id = $query['office_unit_organogram_id'];
           }
        }
        if(!empty($office_id)){
             $english_info['office_name_eng'] = TableRegistry::get('Offices')->getEnglishName($office_id)['office_name_eng'];
        }
        if(!empty($unit_id)){
             $english_info['unit_name_eng'] = TableRegistry::get('OfficeUnits')->getEnglishName($unit_id)['unit_name_eng'];
             $english_info['unit_name_bng'] = TableRegistry::get('OfficeUnits')->getEnglishName($unit_id)['unit_name_bng'];
        }
        if(!empty($designation_id)){
             $english_info['designation_name_eng'] = TableRegistry::get('OfficeUnitOrganograms')->getEnglishName($designation_id)['designation_eng'];
        }
        return $english_info ;
    }

    public function getUnassingedEmolyeesDesignation($office_id = 0,$unit_id = 0){
        $getAllDesignations = $this->getAllDesignationByOfficeOrUnitID($office_id, $unit_id)->group(['office_unit_organogram_id'])->toArray();
        $getOnlyAssignedDesignations = $this->getAllDesignationByOfficeOrUnitID($office_id, $unit_id, 1)->toArray();
        $unassingedDesignations = array_diff($getAllDesignations, $getOnlyAssignedDesignations);
        return $unassingedDesignations;
    }

    public function getLastDesignationHistoryInfo($designation_id)
    {
        return $this->find()->select(['EmployeeRecords.name_bng', 'employee_record_id', 'EmployeeOffices.id',
            'office_unit_organogram_id', 'office_unit_id', 'office_id',
            "designation_label" => 'OfficeUnitOrganograms.designation_bng','EmployeeOffices.joining_date','EmployeeOffices.last_office_date',
            "designation_description" => 'OfficeUnitOrganograms.designation_description'])->where(['office_unit_organogram_id' => $designation_id])
            ->join([
                'EmployeeRecords' => [
                    'table' => 'employee_records',
                    'type' => 'inner',
                    'conditions' => 'EmployeeRecords.id = EmployeeOffices.employee_record_id'
                ],
                'OfficeUnitOrganograms' => [
                    'table' => 'office_unit_organograms',
                    'type' => 'inner',
                    'conditions' => 'OfficeUnitOrganograms.id = EmployeeOffices.office_unit_organogram_id and OfficeUnitOrganograms.status = 1'
                ]
            ])->order(['EmployeeOffices.id desc'])->first();
    }
      public function getAllUnitByOfficeID($office_id = 0,$status = 0){
        $query = $this->find('list',['valueField' => 'office_unit_id']);
        if(!empty($office_id)){
             $query = $query->where(['office_id' => $office_id]);
        }
        if(!empty($status)){
            $query = $query->where(['status' => $status]);
        }
        return $query ;
    }
    public function getEmployeeOfficeRecordsWithOfficeUnitAndDesignationOfficeId($office_id, $list = false)
    {
        $data = array();

        if($list){
            $data_records = $this->find('list',$list);
        }else{
            $data_records = $this->find();
        }

        $data_records = $data_records->select([
                'EmployeeOffices.employee_record_id',
                'EmployeeOffices.office_id',
                'EmployeeOffices.office_unit_id',
                'EmployeeOffices.office_unit_organogram_id',
                'EmployeeOffices.incharge_label',
                'name_bng' => 'EmployeeRecords.name_bng',
                'designation_eng'=>'OfficeUnitOrganograms.designation_eng',
                'designation_bng'=>'OfficeUnitOrganograms.designation_bng',
                'unit_name_bng'=>'OfficeUnits.unit_name_bng',
            ]);
        if (!empty($office_id)) {
            $data_records = $data_records->where(['EmployeeOffices.office_id' => $office_id]);
        }
        $data_records = $data_records->join([
            'EmployeeRecords' => [
                'table' => 'employee_records',
                'type' => 'inner',
                'conditions' => 'EmployeeRecords.id = EmployeeOffices.employee_record_id'
            ],
            'OfficeUnitOrganograms' => [
                'table' => 'office_unit_organograms',
                'type' => 'inner',
                'conditions' => 'EmployeeOffices.office_unit_organogram_id = OfficeUnitOrganograms.id'
            ],
            'OfficeUnits' => [
                'table' => 'office_units',
                'type' => 'inner',
                'conditions' => 'EmployeeOffices.office_unit_id = OfficeUnits.id'
            ]
        ])->where(['EmployeeOffices.status' => 1]);


        return $data_records;
    }

    public function assignDesignation($request_data, $options = [])
    {
        $potrojariGroupsMigTable = TableRegistry::get('PotrojariGroupsUsers');

        if(empty($options)){
            $options['office_unit_organogram_id'] = $request_data['office_unit_organogram_id'];
        }
        $employee_details = $this->find()->where(['office_unit_organogram_id'=>$options['office_unit_organogram_id']])->order(['id' => 'DESC'])->first();

        if($employee_details['status'] == 1){
            //someone is assigned
            return false;
        }

        $entity = $this->newEntity();

        $entity->employee_record_id = $request_data['employee_record_id'];
        $entity->identification_number = $request_data['identification_number'];
        $entity->office_id = $request_data['office_id'];
        $entity->is_default_role = 0;
        $entity->office_unit_id = $request_data['office_unit_id'];
        $entity->office_unit_organogram_id = $request_data['office_unit_organogram_id'];
        $entity->designation = $request_data['designation'];
        $entity->incharge_label = $request_data['incharge_label'];
        $entity->joining_date = date("Y-m-d H:i:s",strtotime($request_data['joining_date']));
        $entity->protikolpo_status = !empty($request_data['protikolpo_status'])?$request_data['protikolpo_status']:0;
        $entity->status = 1;
        $entity->designation_level = !empty($employee_details['designation_level'])?$employee_details['designation_level']:0;
        $entity->designation_sequence = !empty($employee_details['designation_sequence'])?$employee_details['designation_sequence']:0;
        $entity->office_head = !empty($employee_details['office_head'])?$employee_details['office_head']:0;
        $entity->is_admin = !empty($employee_details['is_admin'])?$employee_details['is_admin']:0;
        $entity->summary_nothi_post_type = !empty($employee_details['summary_nothi_post_type'])?$employee_details['summary_nothi_post_type']:0;
        $entity->status_changed_date = date('Y-m-d H:i:s');
        if ($this->save($entity)) {
            //update potrojariGroup table
            $res =  $potrojariGroupsMigTable->updatePotrojariGroupEmployeeDetails($request_data['employee_record_id'],$options['office_unit_organogram_id']);
            if(!empty($res['status']) && $res['status'] == 1){
                if(!empty($res['data'])){
                    // time to notify user
                    $update_employee_info = $res['data'];
                    if(!empty($update_employee_info['personal_email'])){
                        $message = 'প্রিয় ব্যবহারকারী,<br> আপনাকে নতুন পদবিতে নিযুক্ত করা হয়েছে।<br><b>' .
                            (!empty($request_data['designation'])?"পদবিঃ {$request_data['designation']}":"")."</b><br>নিযুক্ত করেছেনঃ <b>সিস্টেম</b>";

                        if( !empty($message) && !empty($update_employee_info['personal_email'])){
                            sendMailsByCron($update_employee_info['personal_email'], '', 'কর্মকর্তা নিযুক্তকরণ বার্তা', '', $message);
                        }
                    }
                }
                return $entity;
            }
        }

        return false;
    }

    public function unAssignDesignation($record_id, $data = []){

        $entity = $this->get($record_id);

        if($entity->status == 0){
            // no user assigned
            return false;
        }
        $entity->status = 0;
        $entity->status_change_date = !empty($data['last_release_date'])
            ? Time::parse($data['last_release_date']) : date('Y-m-d H:i:s');
        $entity->last_office_date = !empty($data['last_release_date'])
            ? Time::parse($data['last_release_date']) : date('Y-m-d');
        $real_record_id = $entity->employee_record_id;

        if ($this->save($entity)){
            $potrojariGroupsTable = TableRegistry::get('PotrojariGroupsUsers');
            $potrojariGroupsTable->unsetPotrojariGroupEmployeeDetails($entity['office_unit_organogram_id']);
            $update_employee_info = TableRegistry::get('EmployeeRecords')->getAllWithDetails(['EmployeeRecords.id' =>$real_record_id,'EmployeeOffices.id' => $record_id])->first();

            $session_table = TableRegistry::get('Sessions');
            $session_table->deleteAll(['data LIKE' => '%s:2:"id";i:'.$update_employee_info['user_id'].';%']);

            if(!empty($update_employee_info['personal_email'])){
                $message = 'প্রিয় ব্যবহারকারী,<br> আপনাকে নিম্নোক্ত পদবি হতে অব্যাহতি প্রদান করা হয়েছে।<br><b>' .(!empty($update_employee_info['designation_bng'])?"পদবিঃ {$update_employee_info['designation_bng']}":"").(!empty($update_employee_info['unit_name_bng'])?"<br>শাখাঃ {$update_employee_info['unit_name_bng']}":"").
                    (!empty($update_employee_info['office_name_bng'])?"<br>অফিসঃ {$update_employee_info['office_name_bng']}</b>":"</b>")."<br>অব্যাহতি প্রদান করেছেনঃ <b>সিস্টেম</b>";

                if( !empty($message) && !empty($update_employee_info['personal_email'])){
                    sendMailsByCron($update_employee_info['personal_email'], '', 'কর্মকর্তা অব্যাহতিকরণ বার্তা', '', $message);
                }
            }
            return true;
        }

        return false;
    }
    public function getSequenceWiseDesignationList($condition = [],$select = [],$list = 0){
        if($list && !empty($select)){
            $query = $this->find('list',['keyField' => $select[0] , 'valueField' => $select[1]]);
        }
        else {
            $query = $this->find();
        }
        if(!empty($select)){
            $query = $query->select($select);
        }
        if(!empty($condition)){
            $query = $query->condition($condition);
        }
        return $query->order(['EmployeeOffices.designation_level' => 'ASC', 'EmployeeOffices.designation_sequence' => 'ASC']);
    }

    public function setCurrentSection($designation)
    {
        $employee_office = [];
        if (!empty($designation)) {
            $table_instance_ministris = TableRegistry::get('OfficeMinistries');
            $employee_office_data = $this->find()->where(['office_unit_organogram_id' => $designation, 'EmployeeOffices.status' => 1])->contain(['Offices', 'OfficeUnits', 'OfficeUnitOrganograms', 'EmployeeRecords'])->first();

            $employee_office['office_id'] = $employee_office_data['office_id'];
            $employee_office['office_unit_id'] = $employee_office_data['office_unit_id'];
            $employee_office['office_unit_organogram_id'] = $employee_office_data['office_unit_organogram_id'];
            $employee_office['officer_id'] = $employee_office_data['employee_record_id'];


            $employee_office['office_name'] = $employee_office_data['office']['office_name_bng'];
            $employee_office['office_name_eng'] = $employee_office_data['office']['office_name_eng'];
            $employee_office['office_address'] = $employee_office_data['office']['office_address'];
            $employee_office['office_phone'] = $employee_office_data['office']['office_phone'];
            $employee_office['office_fax'] = $employee_office_data['office']['office_fax'];
            $employee_office['office_email'] = $employee_office_data['office']['office_email'];
            $employee_office['office_web'] = $employee_office_data['office']['office_web'];

            $employee_office['incharge_label'] = $employee_office_data['incharge_label'];
            $employee_office['designation'] = $employee_office_data['office_unit_organogram']['designation_bng'];
            $employee_office['designation_eng'] = $employee_office_data['office_unit_organogram']['designation_eng'];
            $employee_office['designation_label'] = $employee_office_data['office_unit_organogram']['designation_bng'];
            $employee_office['officer_name'] = $employee_office_data['employee_record']['name_bng'];
            $employee_office['officer_name_eng'] = $employee_office_data['employee_record']['name_eng'];
            $employee_office['office_unit_name'] = $employee_office_data['office_unit']['unit_name_bng'];
            $employee_office['office_unit_name_eng'] = $employee_office_data['office_unit']['unit_name_eng'];
            $employee_office['show_unit'] = $employee_office_data['show_unit'];

            $ministryInformation = $table_instance_ministris->get($employee_office_data['office']['office_ministry_id'])->toArray();
            $employee_office['ministry_name_bng'] = $ministryInformation['name_bng'];
            $employee_office['ministry_name_eng'] = $ministryInformation['name_eng'];
            $employee_office['ministry_records'] = $ministryInformation['name_bng'];

            $employee_office['name_bng'] = $employee_office_data['employee_record']['name_bng'];
            //Digital sign
            $employee_office['default_sign'] = intval($employee_office_data['employee_record']['default_sign']);
            $employee_office['cert_id'] = $employee_office_data['employee_record']['cert_id'];
            $employee_office['cert_type'] = $employee_office_data['employee_record']['cert_type'];
            $employee_office['cert_provider'] = $employee_office_data['employee_record']['cert_provider'];
            $employee_office['cert_serial'] = $employee_office_data['employee_record']['cert_serial'];

            //needed sometime.don't delete it.
            $employee_office['employee_record_id'] = $employee_office_data['employee_record_id'];
            if(isset($employee_office['employee_record_id'])){
                $user_info = TableRegistry::get('Users')->getData(['id','username','is_admin'],['employee_record_id' => $employee_office['employee_record_id']])->first();
                $employee_office['user_id'] = isset($user_info['id'])?$user_info['id']:0;
                $employee_office['is_admin'] = isset($user_info['is_admin'])?$user_info['is_admin']:0;
                $employee_office['username'] = isset($user_info['username'])?$user_info['username']:'';
            }
        }

        return $employee_office;
    }
    public function getSuggestion($search_key,$all_conditions = [],$office_id = 0,$mapped_status = 1,$limit = 500){
        /*
         * Current Strategy
         * Employee Office has All the search parameters designation_bng,unit_name_bng,office_name_bng
         * For further query we will join employee records
         */
        //first check if there is any office related query
        $offices_id_to_query = [];
        if(!empty($all_conditions['Offices'])){
            $table_Offices = TableRegistry::get('Offices');
            $office_query = $table_Offices->getAll($all_conditions['Offices'],['id'])->toArray();
            if(!empty($office_query)){
                foreach ($office_query as $result){
                    $offices_id_to_query[] = $result['id'];
                }
            }
        }


        $query = $this->find()->select(['name_bng'=>'EmployeeRecords.name_bng','name_eng' => 'EmployeeRecords.name_eng','personal_mobile'=>'EmployeeRecords.personal_mobile','EmployeeOffices.office_id','EmployeeOffices.office_unit_id','EmployeeOffices.office_unit_organogram_id','EmployeeOffices.designation','EmployeeOffices.designation_en','EmployeeOffices.unit_name_bn','EmployeeOffices.office_name_bn','EmployeeOffices.office_name_en']);

        $join_condition = 'EmployeeOffices.employee_record_id = EmployeeRecords.id';
        if(!empty($office_id)){
            $join_condition .= ' AND EmployeeOffices.office_id = '.$office_id;
        }
        if(!empty($mapped_status)){
            $join_condition .= ' AND EmployeeOffices.status = '.$mapped_status;
        }

        $query->join([
            'EmployeeRecords' => [
                'table' => 'employee_records',
                'type' => 'inner',
                'conditions' => $join_condition
            ]
        ]);

        //lets search
        if(!empty($search_key)){
            $query->Where(['OR' => [
                'EmployeeRecords.name_bn LIKE' => "$search_key%",
                 'EmployeeOffices.office_name_bn LIKE' => "$search_key%",
                 'EmployeeOffices.designation LIKE' => "$search_key%",
                 'CONCAT(designation,", ",unit_name_bn,", ",office_name_bn) LIKE' => "%{$search_key}%"
            ]]);

        }
        if(!empty($all_conditions)){
            $query->where($all_conditions);
        }

       $result = $query->order(['EmployeeOffices.designation_level ASC'])->limit($limit)->hydrate(false)->toArray();

        return $result;

    }
    public function getSuggestionBK($search_key,$all_conditions,$limit = 500){
        /*
         * Current Strategy
         * First check Employee Records - id, name_bng,name_eng,personal_mobile
         * Then OfficeUnitOrganograms - id, designation_bng,designation_eng
         * Then OfficeUnits - id,unit_name_bng
         * Then Offices - id, office_name_bng, office_address
         */
        $table_employee_records = TableRegistry::get('EmployeeRecords');
        $table_office_unit_organograms = TableRegistry::get('OfficeUnitOrganograms');
        $table_office_units = TableRegistry::get('OfficeUnits');
        $table_offices = TableRegistry::get('Offices');

        $employee_records_data = [];
        $employee_records_id = [];
        $employee_records_custom_query_data = [];

        $office_unit_organograms_data = [];
        $office_unit_organograms_id = [];

        $office_units_data = [];
        $office_units_id = [];

        $offices_data = [];
        $offices_id = [];

        //EmployeeRecords
        $employee_records_condition = [];
        if(!empty($all_conditions['EmployeeRecords'])){
            $employee_records_condition = $all_conditions['EmployeeRecords'];
        }
        $employee_records_condition['name_bng LIKE'] = "$search_key%";

        $employee_records_query = $table_employee_records
            ->getAll($employee_records_condition,['EmployeeRecords.id','EmployeeRecords.name_bng','EmployeeRecords.name_eng','EmployeeRecords.personal_mobile','office_id'=>'EmployeeOffices.office_id','office_unit_id' => 'EmployeeOffices.office_unit_id','office_unit_organogram_id' => 'EmployeeOffices.office_unit_organogram_id']);

        if(!empty($office_id)){
            $employee_records_query->join([
                'EmployeeOffices' => [
                    'table' => 'employee_offices',
                    'type' => 'inner',
                    'conditions' => 'EmployeeOffices.employee_record_id = EmployeeRecords.id AND EmployeeOffices.status = 1 AND EmployeeOffices.office_id  = ' . $office_id
                ]
            ]);
        }else{
            $employee_records_query->join([
                'EmployeeOffices' => [
                    'table' => 'employee_offices',
                    'type' => 'inner',
                    'conditions' => 'EmployeeOffices.employee_record_id = EmployeeRecords.id AND EmployeeOffices.status = 1'
                ]
            ]);
        }
        if(!empty($all_conditions['EmployeeOffices'])){
            $employee_records_query->where($all_conditions['EmployeeOffices']);
        }
        $employee_records_query->orWhere(['EmployeeOffices.designation Like' => '%'.$search_key.'%']);

        $employee_records_query_result = $employee_records_query->limit($limit)->toArray();

        if(!empty($employee_records_query_result)){
            foreach ($employee_records_query_result as $data){
                $employee_records_data[$data['id']] = $data;
                $employee_records_id[] = $data['id'];
                if(!empty($data['office_id'])){
                    $employee_records_custom_query_data['Offices'][] = $data['office_id'];
                }
                if(!empty($data['office_unit_id'])){
                    $employee_records_custom_query_data['Units'][] = $data['office_unit_id'];
                }
                if(!empty($data['office_unit_organogram_id'])){
                    $employee_records_custom_query_data['Designations'][] = $data['office_unit_organogram_id'];
                }
            }
        }
        // EmployeeRecords

        // OfficeUnitOrganograms
//        if(!empty($all_conditions['OfficeUnitOrganograms'])){
//            $office_unit_organograms_condition = $all_conditions['OfficeUnitOrganograms'];
//        }
//        $office_unit_organograms_condition['OfficeUnitOrganograms.designation_bng LIKE'] = "$search_key%";
//
//        $office_unit_organograms_query = $table_office_unit_organograms->getAll($office_unit_organograms_condition,['id','designation_bng','designation_eng','office_id','office_unit_id']);
//        if(!empty($employee_records_custom_query_data['Designations'])){
//            $office_unit_organograms_query->orWhere(['OfficeUnitOrganograms.id IN' => $employee_records_custom_query_data['Designations']]);
//        }
//        $office_unit_organograms_query = $office_unit_organograms_query->limit($limit)->toArray();
//
//        if(!empty($office_unit_organograms_query)){
//            foreach ($office_unit_organograms_query as $data){
//                $office_unit_organograms_data[$data['id']] = $data;
//                $office_unit_organograms_id[] = $data['id'];
//                if(!empty($data['office_id'])){
//                    $employee_records_custom_query_data['Offices'][] = $data['office_id'];
//                }
//                if(!empty($data['office_unit_id'])){
//                    $employee_records_custom_query_data['Units'][] = $data['office_unit_id'];
//                }
//            }
//        }
        // OfficeUnitOrganograms

        // OfficeUnits
        if(!empty($all_conditions['OfficeUnits'])){
            $office_units_condition = $all_conditions['OfficeUnits'];
        }
        $office_units_condition['OfficeUnits.unit_name_bng LIKE'] = "%$search_key%";

        $office_units_query = $table_office_units->getAll($office_units_condition,['OfficeUnits.id','unit_name_bng','office_id','Offices.office_name_eng','Offices.office_name_bng','Offices.office_address']);

        if(!empty($employee_records_custom_query_data['Units'])){
            $office_units_query->orWhere(['OfficeUnits.id IN' => $employee_records_custom_query_data['Units']]);
        }
        if(!empty($employee_records_custom_query_data['Offices'])){
            $office_units_query->orWhere(['Offices.id IN' => $employee_records_custom_query_data['Offices']]);
        }
        $office_units_query->orWhere(['Offices.office_name_bng Like' => "%{$search_key}%"]);

        $office_units_query = $office_units_query->join([
            'Offices' => [
                'table' => 'offices',
                'type' => 'inner',
                'conditions' => 'Offices.id = OfficeUnits.office_id'
            ]
        ])->limit($limit)->hydrate(false)->toArray();

        if(!empty($office_units_query)){
            foreach ($office_units_query as $data){
                $office_units_data[$data['id']] = $data;
                $office_units_id[] = $data['id'];
                if(!empty($data['office_id'])){
                    $employee_records_custom_query_data['Offices'][] = $data['office_id'];
                    $offices_id[] = $data['office_id'];
                }
            }
        }
        // OfficeUnits

        // Offices
//        if(!empty($all_conditions['Offices'])){
//            $offices_condition = $all_conditions['Offices'];
//        }
//        $offices_condition['Offices.office_name_bng LIKE'] = "%$search_key%";
//
//
//        $offices_query = $table_offices->getAll($offices_condition,['id','office_name_eng','office_name_bng','office_address']);
//
//        if(!empty($employee_records_custom_query_data['Offices'])){
//            $offices_query->orWhere(['Offices.id IN' => $employee_records_custom_query_data['Offices']]);
//        }
//        $offices_query = $offices_query->limit($limit)->toArray();
//
//        if(!empty($offices_query)){
//            foreach ($offices_query as $data){
//                $offices_id[] = $data['id'];
//                $offices_data[$data['id']] = $data;
//            }
//        }
        // Offices

        $employee_offices_query = $this->find()->select([
            'EmployeeOffices.office_head','EmployeeOffices.employee_record_id','EmployeeOffices.employee_record_id',
            'EmployeeOffices.incharge_label','EmployeeOffices.show_unit','EmployeeRecords.name_bng','EmployeeOffices.designation','EmployeeOffices.office_unit_organogram_id','EmployeeOffices.office_unit_id','EmployeeOffices.office_id'
        ]);

        if(!empty($all_conditions['EmployeeOffices'])){
            $employee_offices_query->orWhere($all_conditions['EmployeeOffices']);
        }

        if(!empty($employee_records_id)){
            $employee_offices_query->orWhere(['EmployeeOffices.employee_record_id IN'=>$employee_records_id]);
        }

        if(!empty($office_unit_organograms_id)){
            $employee_offices_query->orWhere(['EmployeeOffices.office_unit_organogram_id IN'=>$office_unit_organograms_id]);
        }

        if(!empty($office_units_id)){
            $employee_offices_query->orWhere(['EmployeeOffices.office_unit_id IN'=>$office_units_id]);
        }

        if(!empty($offices_id)){
            $employee_offices_query->orWhere(['EmployeeOffices.office_id IN'=>$offices_id]);
        }

        $results = $employee_offices_query->join([
            'EmployeeRecords' => [
                'table' => 'employee_records',
                'type' => 'inner',
                'conditions' => 'EmployeeOffices.employee_record_id = EmployeeRecords.id AND EmployeeOffices.status = 1'
            ]
        ])->order(['EmployeeOffices.designation_level ASC'])->limit($limit)->hydrate(false)->toArray();

        pr($results);
//        pr($office_units_query);

//        pr($office_unit_organograms_data);
//        pr($office_units_data);
//        pr($offices_data);
        die;
        if(!empty($results)){
            foreach($results as &$result){
                $result['Offices'] = '';
            }
        }

    }
}