<?php
namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use  Cake\Datasource\ConnectionManager;


class EmployeeRanksTable extends ProjapotiTable
{
    /* public function initialize(array $config)
    {
        $this->table('employee_ranks');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->belongsTo('OfficeMinistries', [
            'foreignKey' => 'office_ministry_id'
            
        ]);

    } */
	public function initialize(array $config)
    {
        $conn = ConnectionManager::get('projapotiDb');
        $this->connection($conn);

        $this->displayField('rank_name_bng');
    }
	
	
}