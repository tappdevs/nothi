<?php
namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use  Cake\Datasource\ConnectionManager;
use Cake\Cache\Cache;


class GeoDistrictsTable extends ProjapotiTable
{
    public function initialize(array $config)
    {

        $conn = ConnectionManager::get('projapotiDb');
        $this->connection($conn);

        $this->table('geo_districts');
        $this->displayField('district_name_bng');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->belongsTo('GeoDivisions', [
            'foreignKey' => 'geo_division_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('GeoCityCorporationWards', [
            'foreignKey' => 'geo_district_id'
        ]);
        $this->hasMany('GeoCityCorporations', [
            'foreignKey' => 'geo_district_id'
        ]);
        $this->hasMany('GeoMunicipalities', [
            'foreignKey' => 'geo_district_id'
        ]);
        $this->hasMany('GeoMunicipalityWards', [
            'foreignKey' => 'geo_district_id'
        ]);
        $this->hasMany('GeoPostOffices', [
            'foreignKey' => 'geo_district_id'
        ]);
        $this->hasMany('GeoThanas', [
            'foreignKey' => 'geo_district_id'
        ]);
        $this->hasMany('GeoUnions', [
            'foreignKey' => 'geo_district_id'
        ]);
        $this->hasMany('GeoUpazilas', [
            'foreignKey' => 'geo_district_id'
        ]);
        $this->hasMany('OfficeGeos', [
            'foreignKey' => 'geo_district_id'
        ]);
    }
          public function getBanglaNameandBBSCode($id)
    {
             if (($division_info = Cache::read('district_name_bng_'.$id, 'memcached')) === false) {
                $division_info =  $this->find()->select(['district_name_bng','bbs_code'])->where(['id'=>$id])->first();
                Cache::write('district_name_bng_'.$id, $division_info, 'memcached');
            }
            return $division_info;
    }

}