<?php
namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use  Cake\Datasource\ConnectionManager;
use Cake\Cache\Cache;


class GeoUnionsTable extends ProjapotiTable
{
    public function initialize(array $config)
    {
        $conn = ConnectionManager::get('projapotiDb');
        $this->connection($conn);

        $this->displayField('union_name_bng');
        $this->addBehavior('Timestamp');
    }
     public function getBanglaNameandBBSCode($id)
    {
             if (($division_info = Cache::read('union_name_bng_'.$id, 'memcached')) === false) {
                $division_info =  $this->find()->select(['union_name_bng','bbs_code'])->where(['id'=>$id])->first();
                Cache::write('union_name_bng_'.$id, $division_info, 'memcached');
            }
            return $division_info;
    }
}