<?php

namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\Datasource\ConnectionManager;
use Cake\Controller\Component;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

class GuardFileApiDatasTable extends ProjapotiTable
{

    public function initialize(array $config)
    {
        //parent::initialize($config);
        $conn = ConnectionManager::get('NothiAccessDb');
        $this->connection($conn);
        $this->table('guard_file_api_datas');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
    }

    /**
     *
     * @param int $id
     * @param date $date
     * @param string $name
     * @return object Query Object will return
     */


}