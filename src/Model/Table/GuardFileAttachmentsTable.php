<?php

namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;

class GuardFileAttachmentsTable extends ProjapotiTable {

    public function initialize(array $config) {
        $conn = ConnectionManager::get('NothiAccessDb');
        $this->connection($conn);
        
        $this->primaryKey('id');
        
    }

    public function getTotal($id){
        return $this->find()->where(['guard_file_id'=>$id])->count();
    }

    public function getAttachments($id){
        return $this->find()->where(['guard_file_id'=>$id])->order(['id asc'])->toArray();
    }

    public function getAttachment($id){
        return $this->find()->where(['guard_file_id'=>$id])->order(['id asc'])->first();
    }
}
