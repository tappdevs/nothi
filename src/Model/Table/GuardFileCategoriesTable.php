<?php

namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;

class GuardFileCategoriesTable extends ProjapotiTable {

    public function initialize(array $config) {
        $conn = ConnectionManager::get('NothiAccessDb');
        $this->connection($conn);
        
        $this->displayField('name_bng');
        $this->primaryKey('id');
        
    }
    
    
    public function validationAdd($validator) {
        $validator
            ->notEmpty('name_bng', "এই তথ্য ফাকা রাখা যাবে না")
            ->add('name_bng', 'isUnique', [
                'rule' => function($data, $provider) {

                    if ($this->find()->where(['name_bng' => $data, 'office_unit_id' => $provider['data']['office_unit_id'], 'office_id' => $provider['data']['office_id']])->count() > 0){
                        return "এটি পূর্বে ব্যবহৃত হয়েছে";
                    }
                    return true;
                },
            'message' => __('এটি পূর্বে ব্যবহৃত হয়েছে')
        ]);

        return $validator;
    }

    public function validationEdit($validator) {
        $validator
            ->notEmpty('name_bng', "এই তথ্য ফাকা রাখা যাবে না")
            ->add('name_bng', 'isUnique', [
                'rule' => function($data, $provider) {
                    if ($this->find()->where(['name_bng' => $data, 'id <>' => $provider['data']['id'], 'office_unit_id' => $provider['data']['office_unit_id'], 'office_id' => $provider['data']['office_id']])->count() > 0)
                        return "এটি পূর্বে ব্যবহৃত হয়েছে";
                    return true;
                },
            'message' => __('এটি পূর্বে ব্যবহৃত হয়েছে')
        ]);

        return $validator;
    }
    
    public function getAllTypes($page = 0, $len = 100, $employee = null) {
        $queryString = $this->find()->where(["GuardFileCategories.office_id" => $employee['office_id'], "GuardFileCategories.office_unit_id" => $employee['office_unit_id']])
                ->order("GuardFileCategories.id ASC");


        if (!empty($page)) {
            $queryString = $queryString->page($page, $len);
        }

        return $queryString;
    }

    public function getTypes($id = 0, $employee = null) {
        if(!empty($employee['office_unit_id'])){
            $queryString = $this->find('list', ['keyField' => 'id', 'valueField' => 'name_bng'])->where([ "GuardFileCategories.office_id" => $employee['office_id'], "GuardFileCategories.office_unit_id" => $employee['office_unit_id']])
                ->order("GuardFileCategories.name_bng ASC");
        } else {
            $queryString = $this->find('list', ['keyField' => 'id', 'valueField' => 'name_bng'])->where([ "GuardFileCategories.office_id" => $employee['office_id']])
                ->order("GuardFileCategories.name_bng ASC");
        }


        if (!empty($id)) {
            $queryString = $queryString->where(['id' => $id]);
        }

        return $queryString->hydrate(false)->toArray();
    }
}
