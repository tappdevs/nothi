<?php
namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\Validation\Validator;
use  Cake\Datasource\ConnectionManager;

/**
 * Articles Model
 */
class ModuleInfosTable extends ProjapotiTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $conn = ConnectionManager::get('projapotiDb');
        $this->connection($conn);

        $this->displayField('name_bn');
        $this->table('module_infos');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
    }

    public function getModuleList()
    {
        $moduleList = $this->find('list')->toArray();
        return $moduleList;
    }
}
