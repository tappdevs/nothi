<?php

namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;

class NisponnoRecordsTable extends ProjapotiTable
{

    public function initialize(array $config)
    {
        $conn = ConnectionManager::get('NothiAccessDb');
        $this->connection($conn);

        $this->addBehavior('Timestamp');
    }

    public function validationAdd($validator)
    {
        $validator
            ->notEmpty('nothi_masters_id', "এই তথ্য ফাকা রাখা যাবে না")
            ->notEmpty('nothi_part_no', "এই তথ্য ফাকা রাখা যাবে না")
            ->notEmpty('type', "এই তথ্য ফাকা রাখা যাবে না")
            ->notEmpty('employee_id', "এই তথ্য ফাকা রাখা যাবে না")
            ->notEmpty('operation_date', "এই তথ্য ফাকা রাখা যাবে না");

        return $validator;
    }

    public function hasData($nothi_master_id = 0, $nothi_part_no = 0, $nothi_onucched_id = 0,
                            $nothi_office = 0)
    {
        $query = $this->find();
        if (!empty($nothi_master_id)) {
            $query = $query->where(['nothi_master_id' => $nothi_master_id]);
        }
        if (!empty($nothi_part_no)) {
            $query = $query->where(['nothi_part_no' => $nothi_part_no]);
        }
        if (!empty($nothi_onucched_id)) {
            $query = $query->where(['nothi_onucched_id' => $nothi_onucched_id]);
        }
        if (!empty($nothi_office)) {
            $query = $query->where(['nothi_office_id' => $nothi_office]);
        }
        return $query->count();
    }

    public function getNisponnoCount($office_id = 0, $unit_id = 0, $designation_id = 0, $time = [],
                                     $nothi_part_no = 0)
    {
        $query1 = $this->find();
        $query2 = $this->find();
        if (!empty($office_id)) {
            $query1 = $query1->where(['office_id' => $office_id]);
            $query2 = $query2->where(['office_id' => $office_id]);
        }
        if (!empty($unit_id)) {
            $query1 = $query1->where(['unit_id' => $unit_id]);
            $query2 = $query2->where(['unit_id' => $unit_id]);
        }
        if (!empty($designation_id)) {
            $query1 = $query1->where(['designation_id' => $designation_id]);
            $query2 = $query2->where(['designation_id' => $designation_id]);
        }
        if (!empty($time[0])) {
            $query1 = $query1->where(['DATE(operation_date) >=' => $time[0]]);
            $query2 = $query2->where(['DATE(operation_date) >=' => $time[0]]);
        }
        if (!empty($time[1])) {
            $query1 = $query1->where(['DATE(operation_date) <=' => $time[1]]);
            $query2 = $query2->where(['DATE(operation_date) <=' => $time[1]]);
        }
        if (!empty($nothi_part_no)) {
            $query1 = $query1->where(['nothi_part_no' => $nothi_part_no]);
            $query2 = $query2->where(['nothi_part_no' => $nothi_part_no]);
        }
        $query1 = $query1->where(['type' => 'potrojari'])->group(['nothi_part_no'])->count();
        $query2 = $query2->group(['nothi_part_no'])->count();
//        $temp =$query;
//        pr($temp);
//         debug($query);
//        $query1 = $query1->where(['type' => 'potrojari'])->count();
//        debug($query);
//        $query2 =  $query2->where(['type' => 'note'])->count();
//          debug($query);die;
        $data   = [
            'potrojari' => $query1,
            'note' => (($query2 - $query1) < 0) ? 0 : ($query2 - $query1)
        ];
        return $data;
    }

    public function getFirstNisponno($office_id = 0, $unit_id = 0, $designation_id = 0,
                                     $nothi_part_no = 0)
    {
        $query = $this->find();
        if (!empty($office_id)) {
            $query = $query->where(['office_id' => $office_id]);
        }
        if (!empty($unit_id)) {
            $query = $query->where(['unit_id' => $unit_id]);
        }
        if (!empty($designation_id)) {
            $query = $query->where(['designation_id' => $designation_id]);
        }
        if (!empty($nothi_part_no)) {
            $query = $query->where(['nothi_part_no' => $nothi_part_no]);
        }
        $query = $query->order(['operation_date asc'])->first();

        return $query;
    }

    public function getLastNisponno($office_id = 0, $unit_id = 0, $designation_id = 0,
                                    $nothi_part_no = 0)
    {
        $query = $this->find();
        if (!empty($office_id)) {
            $query = $query->where(['office_id' => $office_id]);
        }
        if (!empty($unit_id)) {
            $query = $query->where(['unit_id' => $unit_id]);
        }
        if (!empty($designation_id)) {
            $query = $query->where(['designation_id' => $designation_id]);
        }
        if (!empty($nothi_part_no)) {
            $query = $query->where(['nothi_part_no' => $nothi_part_no]);
        }
        $query = $query->order(['operation_date desc'])->first();

        return $query;
    }

    public function getLastNisponnoByPotrojari($office_id = 0, $unit_id = 0, $designation_id = 0,
                                               $time = [], $nothi_part_no = 0)
    {
        $query1 = $this->find('list',
            ['keyField' => 'nothi_part_no', 'valueField' => 'operation_date']);
        if (!empty($office_id)) {
            $query1->where(['office_id' => $office_id]);
        }
        if (!empty($unit_id)) {
            $query1->where(['unit_id' => $unit_id]);
        }
        if (!empty($designation_id)) {
            $query1->where(['designation_id' => $designation_id]);
        }
        if (!empty($time[0])) {
            $query1->where(['DATE(operation_date) >=' => $time[0]]);
        }
        if (!empty($time[1])) {
            $query1->where(['DATE(operation_date) <=' => $time[1]]);
        }
        if (!empty($nothi_part_no)) {
            $query1->where(['nothi_part_no' => $nothi_part_no]);
        }
        return $query1->where(['type' => 'potrojari'])->group(['nothi_part_no'])->toArray();
    }

    public function getLastNisponnoByNote($office_id = 0, $unit_id = 0, $designation_id = 0,
                                          $time = [], $nothi_part_no = 0)
    {
        $query1 = $this->find('list',
            ['keyField' => 'nothi_part_no', 'valueField' => 'operation_date']);
        if (!empty($office_id)) {
            $query1->where(['office_id' => $office_id]);
        }
        if (!empty($unit_id)) {
            $query1->where(['unit_id' => $unit_id]);
        }
        if (!empty($designation_id)) {
            $query1->where(['designation_id' => $designation_id]);
        }
        if (!empty($time[0])) {
            $query1->where(['DATE(operation_date) >=' => $time[0]]);
        }
        if (!empty($time[1])) {
            $query1->where(['DATE(operation_date) <=' => $time[1]]);
        }
        if (!empty($nothi_part_no)) {
            $query1->where(['nothi_part_no' => $nothi_part_no]);
        }
        return $query1->where(['type' => 'note'])->group(['nothi_part_no'])->toArray();
    }

    public function allPotrojariCount($office_id = 0, $unit_id = 0, $designation_id = 0, $time = [])
    {
        $query1 = $this->find();
        if (!empty($office_id)) {
            $query1 = $query1->where(['office_id' => $office_id]);
        }
        if (!empty($unit_id)) {
            $query1 = $query1->where(['unit_id' => $unit_id]);
        }
        if (!empty($designation_id)) {
            $query1 = $query1->where(['designation_id' => $designation_id]);
        }
        if (!empty($time[0])) {
            $query1 = $query1->where(['DATE(operation_date) >=' => $time[0]]);
        }
        if (!empty($time[1])) {
            $query1 = $query1->where(['DATE(operation_date) <=' => $time[1]]);
        }
        return $query1->where(['type' => 'potrojari'])->count();
    }

    /**
     *
     * @param int $office_id
     * @param int $unit_id
     * @param int $designation_id
     * @param DateTime $time
     * @param String $type = 'Dak' for potrojari against Dak
     * @return int $count
     */
    public function allPotrojariIDArray($office_id = 0, $unit_id = 0, $designation_id = 0,
                                        $time = [])
    {
        $query = $this->find('list', ['keyField' => 'potrojari_id', 'valueField' => 'potrojari_id']);
        if (!empty($office_id)) {
            $query = $query->where(['office_id' => $office_id]);
        }

        if (!empty($unit_id)) {
            $query = $query->where(['unit_id' => $unit_id]);
        }

        if (!empty($designation_id)) {
            $query = $query->where(['designation_id' => $designation_id]);
        }
        if (!empty($time[0])) {
            $query = $query->where(['DATE(operation_date) >=' => $time[0]]);
        }
        if (!empty($time[1])) {
            $query = $query->where(['DATE(operation_date) <=' => $time[1]]);
        }

        return $query->where([ 'type' => 'potrojari'])->toArray();
    }

    public function allNothiPartArrayByPotrojari($office_id = 0, $unit_id = 0, $designation_id = 0,
                                                 $time = [])
    {
        $query = $this->find('list', ['keyField' => 'nothi_part_no', 'valueField' => 'nothi_part_no']);
        if (!empty($office_id)) {
            $query = $query->where(['office_id' => $office_id]);
        }

        if (!empty($unit_id)) {
            $query = $query->where(['unit_id' => $unit_id]);
        }

        if (!empty($designation_id)) {
            $query = $query->where(['designation_id' => $designation_id]);
        }
        if (!empty($time[0])) {
            $query = $query->where(['DATE(operation_date) >=' => $time[0]]);
        }
        if (!empty($time[1])) {
            $query = $query->where(['DATE(operation_date) <=' => $time[1]]);
        }

        return $query->where([ 'type' => 'potrojari'])->toArray();
    }

    public function generatePotrojariNisponnoInternalExternalCount($office_id = 0, $unit_id = 0,
                                                                   $designation_id = 0, $time = [])
    {
//        $allPotrojariNisponnoNoteArray = $this->allNothiPartArrayByPotrojari($office_id,$unit_id,$designation_id,$time);
        $allPotrojariIdArray = $this->allPotrojariIDArray($office_id, $unit_id, $designation_id,
            $time);
        if(empty($allPotrojariIdArray)){
            return 0;
        }
//        pr($allPotrojariNisponnoNoteArray);die;
        TableRegistry::remove('Potrojari');
        $potrojari_table     = TableRegistry::get('Potrojari');
//        return $potrojari_table->allPotrojariInternalCountByNothiPartNO($allPotrojariNisponnoNoteArray,$time);
        return $potrojari_table->allPotrojariInternalCountByPotrojariId($allPotrojariIdArray, $time,1);
    }
    public function saveData($nothi_master_id,$nothi_part_no,$type ='note',$nothi_notes_id = 0,$potrojari_id = 0,$nothi_office=0,$office_id=0,$office_unit_id=0,$officer_designation_id=0,$officer_id=0){
                $nisponnoEntity = $this->newEntity();
                $nisponnoEntity->nothi_master_id = $nothi_master_id;
                $nisponnoEntity->nothi_part_no = $nothi_part_no;
                $nisponnoEntity->type = $type;
                $nisponnoEntity->nothi_onucched_id = $nothi_notes_id;
                $nisponnoEntity->potrojari_id = $potrojari_id;
                $nisponnoEntity->nothi_office_id = $nothi_office;
                $nisponnoEntity->office_id = $office_id;
                $nisponnoEntity->unit_id = $office_unit_id;
                $nisponnoEntity->designation_id = $officer_designation_id;
                $nisponnoEntity->employee_id = $officer_id;
                $nisponnoEntity->operation_date = date('Y-m-d H:i:s',strtotime("+2 min")); //Important don't remove +2 min
                return $this->save($nisponnoEntity);
    }
}