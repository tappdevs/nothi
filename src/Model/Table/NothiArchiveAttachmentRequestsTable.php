<?php
namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;


class NothiArchiveAttachmentRequestsTable extends ProjapotiTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $conn = ConnectionManager::get('NothiAccessDb');
        $this->connection($conn);
        $this->addBehavior('Timestamp');
    }

    public function saveData($request = [])
    {
        if(!empty($request)) {

            $entity = $this->newEntity();
            $entity->office_id = $request['office_id'];
            $entity->nothi_master_id = $request['nothi_master_id'];
            $entity->requester_organogram_id = $request['requested_organogram_id'];
            $entity->status = 1;
            return $this->save($entity);
        }
    }
}