<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Network\Email\Email;
use Cake\Datasource\ConnectionManager;
use Cake\Controller\Component;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

class NothiDakPotroMapsTable extends ProjapotiTable
{

    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->table('nothi_dak_potro_maps');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
    }

    public function getAllNothiPartNo()
    {
        return $this->find()->select(['nothi_part_no','status'=>1])->toArray();
    }

    public function getAllNisponnoDakforOffices($office_id = 0, $unit_id = 0, $time = [])
    {
        $dakUsers         = TableRegistry::get('DakUsers');
        $allNothiVuktoDak = $dakUsers->getNothivuktoDakList($office_id, $unit_id, $time);
        $count            = 0;
        if (!empty($allNothiVuktoDak)) {
            foreach ($allNothiVuktoDak as $key => $val) {
                $is_niponno = $this->find()->where(['dak_id' => $val['dak_id'], 'dak_type' => $val['dak_type'], 'is_nisponno' => 1,'status'=>1])->count();
                $count+=$is_niponno;
            }
        }
        return $count;
    }

    public function getDakSohoNote($office_id = 0, $unit_id = 0, $designation_id = 0, $time = [])
    {
        TableRegistry::remove('NothiMasterCurrentUsers');
        $NothiMasterCurrentUsersTable = TableRegistry::get('NothiMasterCurrentUsers');
        $allNothipartNo               = $NothiMasterCurrentUsersTable->find('list',
            ['keyField' => 'id', 'valueField' => 'nothi_part_no']);
        
        $NothiPotros                  = TableRegistry::get('NothiPotros')->find('list',['keyField'=>'id','valueField'=>'nothi_part_no'])->select(['NothiPotros.id','NothiPotros.nothi_part_no'])->join([
            "NothiParts" => [
                'table' => 'nothi_parts',
                'type' => 'inner',
                'conditions' => ['NothiPotros.nothi_part_no =NothiParts.id']
            ]
        ]);
        if (!empty($office_id)) {
            $NothiPotros    = $NothiPotros->where(['NothiParts.office_id' => $office_id]);
            $allNothipartNo = $allNothipartNo->where(['office_id' => $office_id]);
        }
        if (!empty($unit_id)) {
            $NothiPotros    = $NothiPotros->where(['NothiParts.office_units_id' => $unit_id]);
            $allNothipartNo = $allNothipartNo->where(['office_unit_id' => $unit_id]);
        }
        if (!empty($designation_id)) {
            $NothiPotros    = $NothiPotros->where(['NothiParts.office_units_organogram_id' => $designation_id]);
            $allNothipartNo = $allNothipartNo->where(['office_unit_organogram_id' => $designation_id]);
        }
        if (!empty($time[0])) {
            $NothiPotros    = $NothiPotros->where(['DATE(NothiPotros.created) >=' => $time[0]]);
            $allNothipartNo = $allNothipartNo->where(['DATE(created) >=' => $time[0]]);
        }
        if (!empty($time[1])) {
            $NothiPotros    = $NothiPotros->where(['DATE(NothiPotros.created) <=' => $time[1]]);
            $allNothipartNo = $allNothipartNo->where(['DATE(created) <=' => $time[1]]);
        }
        $NothiPotros = $NothiPotros->where(['NothiPotros.nothijato' => 0,'NothiPotros.is_deleted'=>0])->group(['NothiPotros.nothi_part_no'])->toArray();
        
        $count       = 0;
        if (!empty($NothiPotros)) {
            $allNothipartNo = $allNothipartNo->where(['nothi_part_no in' => array_values($NothiPotros)])->distinct(['nothi_part_no'])->toArray();
            
            if (!empty($allNothipartNo)) {
                $count = $this->find()->where(['nothi_part_no in' => array_values($allNothipartNo), 'dak_id >' => 0,'status'=>1])->distinct(['nothi_part_no'])->count();
            }
        }

        return $count;
    }

    public function updateNisponnoField($nothi_part_no = 0)
    {
        if (!empty($nothi_part_no)) {
            $this->updateAll([ 'is_nisponno' => 1], [ '	nothi_part_no' => $nothi_part_no]);
        }
    }

    public function getNothiInformationForDak($dak_id, $dak_type = DAK_DAPTORIK){
        return $this->find()->where(['dak_id'=>$dak_id, 'dak_type'=>$dak_type])->first();
    }
    public function checkPartIsFromDak($nothi_part_no = 0, $date = ''){
        $query = $this->find();
        if(!empty($nothi_part_no)){
            $query = $query->where(['nothi_part_no' =>$nothi_part_no]);
        }
        if(!empty($date)){
                 $query = $query->where(['created >=' =>$date]);
        }
       return  $query->where(['dak_id >' => 0])->count();
    }
    public function checkIsDakSrijitoNote($nothi_part_no = '',$time = []){
        $query = $this->find();
        if(!empty($nothi_part_no)){
            $query->where(['nothi_part_no IN' =>$nothi_part_no]);
        }
        if(!empty($time[0])){
            $query->where(['date(created) >=' => $time[0]]);
        }
        if(!empty($time[1])){
            $query->where(['date(created) <=' => $time[1]]);
        }
      return  $query->group(['nothi_part_no'])->count();

    }
}