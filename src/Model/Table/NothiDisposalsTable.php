<?php
/**
 * Created by PhpStorm.
 * User: WINDOWS
 * Date: 10/1/2015
 * Time: 5:25 PM
 */

namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;


class NothiDisposalsTable extends ProjapotiTable
{

    public function initialize(array $config)
    {
        $this->displayField('disposal_name');
        $this->primaryKey('id');
    }
	
}