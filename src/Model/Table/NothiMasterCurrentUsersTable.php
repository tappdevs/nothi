<?php

namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;

class NothiMasterCurrentUsersTable extends ProjapotiTable
{

    public function initialize(array $config)
    {
        /*$conn = ConnectionManager::get('NothiAccessDb');
        $this->connection($conn);*/
        parent::initialize($config);
        $this->addBehavior('Timestamp');
        $this->addBehavior('MobileTrack');
    }

    public function validationAdd($validator)
    {
        $validator
            ->notEmpty('nothi_masters_id', "এই তথ্য ফাকা রাখা যাবে না")
            ->notEmpty('office_id', "এই তথ্য ফাকা রাখা যাবে না")
            ->notEmpty('office_unit_id', "এই তথ্য ফাকা রাখা যাবে না")
            ->notEmpty('employee_id', "এই তথ্য ফাকা রাখা যাবে না")
            ->notEmpty('office_unit_organograms_id', "এই তথ্য ফাকা রাখা যাবে না");

        return $validator;
    }

    public function validationEdit($validator)
    {

        return $validator;
    }

    public function hasThisType($typeId)
    {
        return $this->find()->select(['id'])->where(['nothi_types_id' => $typeId])->count();
    }

    public function getCurrentUserbyDesignation($nothi_part_no, $office_unit_organogram_id, $office_unit_id = 0, $nothi_office = 0)
    {
        return $this->find()->where(['nothi_part_no' => $nothi_part_no, 'office_unit_organogram_id' => $office_unit_organogram_id, 'nothi_office' => $nothi_office])->first();
    }

    public function getAllPartByDesignation($masterId, $office_unit_organogram_id, $office_unit_id = 0, $nothi_office = 0)
    {
        return $this->find('list', ['keyField' => 'nothi_part_no', 'valueField' => 'nothi_master_id'])->where(['nothi_master_id' => $masterId,
                'office_unit_organogram_id' => $office_unit_organogram_id,
                'nothi_office' => $nothi_office, 'is_finished' => 0])->toArray();
    }

    public function getFirstPendingNote($office_unit_organogram_id)
    {
        return $this->find()->select([
                'nothi_master_id',
                'nothi_part_no',
                'nothi_office',
                'view_status',
                'is_new',
            ])->join([
                            "NothiNotes" => [
                                'table' => 'nothi_notes', 'type' => 'inner',
                                'conditions' => ['NothiNotes.nothi_part_no =NothiMasterCurrentUsers.nothi_part_no']
                            ]
                        ])
            ->where(['office_unit_organogram_id' => $office_unit_organogram_id, 'is_archive' => 0,'is_finished' => 0])->order(['NothiMasterCurrentUsers.priority' => 'DESC', 'NothiMasterCurrentUsers.id' => 'ASC'])->first();
    }

    public function getMasterCurrentUserbyDesignation($masterId, $office_unit_organogram_id,
                                                      $office_unit_id = 0, $nothi_office,
                                                      $is_archive = 1)
    {
        $query = $this->find()->where(['nothi_master_id' => $masterId, 'office_unit_organogram_id' => $office_unit_organogram_id,
            'nothi_office' => $nothi_office]);

        $query->where(['is_archive' => $is_archive]);

        return $query->toArray();
    }

    public function getCurrentUserDetails($masterId, $partid, $nothi_office = 0,$api = 0)
    {
        $nothiMasterUser = $this->find()->where(['nothi_master_id' => $masterId, 'nothi_part_no' => $partid,
                'nothi_office' => $nothi_office])->first();

        $officesTable     = TableRegistry::get('Offices');
        $officeUnitTable     = TableRegistry::get('OfficeUnits');
        $employeeOfficeTable = TableRegistry::get('EmployeeOffices');

        $employeeInfo   = $employeeOfficeTable->getDesignationInfo($nothiMasterUser['office_unit_organogram_id'],-1);
        $officeUnitInfo = $officeUnitTable->getBanglaName($nothiMasterUser['office_unit_id']);
        $officeInfo = $officesTable->getBanglaName($nothiMasterUser['office_id']);

        return h($employeeInfo['designation']).', '.h($officeUnitInfo['unit_name_bng']).(isset($officeInfo['office_name_bng'])?(', '.$officeInfo['office_name_bng']):'').(($api == 0)?($nothiMasterUser['view_status']
            == 1 ? ' <p class="text-success">(দেখেছেন)</p>' : ' <p class="text-danger">(দেখেননি)</p>'):'');
    }


    public function getOnisponnoNoteCount($office_id = 0, $unit_id = 0, $designation_id = 0, $time = [])
    {
        $nothiPartsTable = TableRegistry::get('NothiParts');
        if(!empty($time[0])){
            $time[0] = '';
        }
        $nothiNotes = $nothiPartsTable->selfSrijitoNotePart($office_id, $unit_id, $designation_id, $time);
        $nothiVuktoNotes = $nothiPartsTable->notePartListfornothiVuktoDak($office_id, $unit_id, $designation_id, $time);

        $data = $this->find()->where(['is_finished' => 0]);

        if (!empty($designation_id)) {
            $data = $data->where(['office_unit_organogram_id' => $designation_id]);
        }
        if (!empty($unit_id)) {
            $data = $data->where(['office_unit_id' => $unit_id]);
        }
        if (!empty($office_id)) {
            $data = $data->where(['office_id' => $office_id]);
        }

        if (!empty($time[1])) {
            $data = $data->where(['DATE(created) <=' => $time[1]]);
        }

        $allNothi = array_merge($nothiVuktoNotes,$nothiNotes);

        if(!empty($allNothi)){
            return $data->where(['nothi_part_no IN' => $allNothi])->group(['nothi_part_no'])->count(); 
        }

        return 0;
    }

    public function getNothiInboxCount($designation_Id)
    {
        return $this->find()->where(['office_unit_organogram_id IN' => $designation_Id, 'is_archive' => 0])->count();
    }

    public function updateIsFinishField($nothi_part_no = 0, $nothi_office = 0, $is_archive = false)
    {
        if (!empty($nothi_part_no)) {
            if($is_archive){
                $this->updateAll([ 'is_finished' => 1,'is_archive'=>1], [ 'nothi_part_no' => $nothi_part_no,'nothi_office'=>$nothi_office]);
            }else{
                $this->updateAll([ 'is_finished' => 1], [ 'nothi_part_no' => $nothi_part_no,'nothi_office'=>$nothi_office]);
            }

        }
    }

    public function nisponnoNoteCount($office_id = 0, $unit_id = 0, $designation_id = 0, $time = [])
    {
        $nothiPartsTable = TableRegistry::get('NothiParts');
        $nothiNotes = $nothiPartsTable->selfSrijitoNotePart($office_id, $unit_id, $designation_id);
        $nothiVuktoNotes = $nothiPartsTable->notePartListfornothiVuktoDak($office_id, $unit_id, $designation_id);

        $query = $this->find()->where([ 'is_finished' => 1]);

        if (!empty($office_id)) {
            $query = $query->where([ 'office_id' => $office_id]);
        }
        if (!empty($unit_id)) {
            $query = $query->where([ 'office_unit_id' => $unit_id]);
        }
        if (!empty($designation_id)) {
            $query = $query->where([ 'office_unit_organogram_id' => $designation_id]);
        }
        if (!empty($time[0])) {
            $query = $query->where(['DATE(created) >=' => $time[0]]);
        }
        if (!empty($time[1])) {
            $query = $query->where(['DATE(created) <=' => $time[1]]);
        }

        $allnothi = array_merge($nothiNotes, $nothiVuktoNotes);
        
        return $query->where(['nothi_part_no IN'=>$allnothi])->group(['nothi_part_no'])->count();
    }

    public function checkNisponnobyNothiPatrs($office_id = 0, $unit_id = 0, $designation_id = 0,
                                              $time = [], $nothi_parts = [])
    {
        $query = $this->find()->where([ 'is_finished' => 1]);

        if (!empty($office_id)) {
            $query = $query->where([ 'office_id' => $office_id]);
        }
        if (!empty($unit_id)) {
            $query = $query->where([ 'office_unit_id' => $unit_id]);
        }
        if (!empty($designation_id)) {
            $query = $query->where([ 'office_unit_organogram_id' => $designation_id]);
        }
        if (!empty($time[0])) {
            $query = $query->where(['DATE(created) >=' => $time[0]]);
        }
        if (!empty($time[1])) {
            $query = $query->where(['DATE(created) <=' => $time[1]]);
        }
        return $query->where(['nothi_part_no IN' => $nothi_parts])->distinct(['nothi_part_no'])->count();
    }

    public function getAllPartByOffice($office_id = 0){
        $query = $this->find()->select(['nothi_office','nothi_part_no','issue_date','created']);

        if(!empty($office_id)){
            $query->where(['nothi_office'=>$office_id]);
        }

        return $query;
    }
    public function getCurrentUserofNote($nothi_master_id = 0 , $nothi_part_id = 0, $nothi_office = 0){
        
            $query = $this->find();
            if(!empty($nothi_master_id)){
                $query->where(['nothi_master_id' => $nothi_master_id ]);
            }
            if(!empty($nothi_part_id)){
                $query->where(['nothi_part_no' => $nothi_part_id]);
            }
            if(!empty($nothi_office)){
                $query->where(['nothi_office' => $nothi_office]);
            }
            if(empty($nothi_master_id) && empty($nothi_part_id)){
                    $query->where(['id'=>0]);
           }
            return $query;
        }
        /**
         *
         * @param int $office_id
         * @param int $unit_id
         * @param int $designation_id
         * @param array $time
         * @return Object All the fields for given query.
         */
        public function getAllNothiPartNo($office_id = 0, $unit_id = 0, $designation_id = 0, $time = [],$list = 0,$listType = null){
            if(!empty($list)){
                if(!empty($listType)){
                    $query = $this->find('list',$listType);
                }
                else{
                    $query = $this->find('list',['valueField' => 'nothi_part_no']);
                }
            }else{
                 $query = $this->find();
            }
            
            if(!empty($office_id)){
                $query = $query->where(['office_id' => $office_id ]);
            }
            if(!empty($unit_id)){
                  $query = $query->where(['office_unit_id' => $unit_id ]);
            }
            if(!empty($designation_id)){
                 $query = $query->where(['office_unit_organogram_id' => $designation_id ]);
            }
            if(!empty($time[0])){
                  $query = $query->where(['DATE(modified) >=' => $time[0] ]);
            }
            if(!empty($time[1])){
                 $query = $query->where(['DATE(modified) <=' => $time[1] ]);
            }
            return $query->group(['nothi_part_no']);
    }

    public function getCurrentUserDetailsWithData($masterId, $partid, $nothi_office = 0)
    {
        $nothiMasterUser = $this->find()->where(['nothi_master_id' => $masterId, 'nothi_part_no' => $partid,
            'nothi_office' => $nothi_office])->first();

        $officeUnitTable     = TableRegistry::get('OfficeUnits');
        $employeeOfficeTable = TableRegistry::get('EmployeeOffices');

        $employeeInfo   = $employeeOfficeTable->getDesignationInfo($nothiMasterUser['office_unit_organogram_id'],-1);
        $officeUnitInfo = $officeUnitTable->getOfficeUnitBy_designationId($nothiMasterUser['office_unit_organogram_id']);

        return ['data'=>$nothiMasterUser,'text'=>($employeeInfo['designation'].', '.$officeUnitInfo['unit_name_bng'])];
    }

    public function getOnisponnoNothiCount($office_unit_organogram_id =[], $current_nothi_office_id= 0){
            return $this->find('list', ['keyField' => 'nothi_part_no', 'valueField' => 'nothi_office'])->where(['office_unit_organogram_id IN ' => $office_unit_organogram_id, 'is_archive' => 0, 'is_finished' => 0, 'is_new' => 0, 'nothi_office' => $current_nothi_office_id])->count();
    }

    public function getOnisponnoOtherNothiCount($office_unit_organogram_id =[], $current_nothi_office_id= 0){
            return $this->find('list', ['keyField' => 'nothi_part_no', 'valueField' => 'nothi_office'])->where(['office_unit_organogram_id IN ' => $office_unit_organogram_id, 'is_archive' => 0, 'is_finished' => 0, 'is_new' => 0, 'nothi_office NOT IN' => $current_nothi_office_id])->count();
    }
    public function getOnisponnoNothiMasterCount($office_unit_organogram_id =[]){
        return $this->find('list', ['keyField' => 'nothi_masters_id', 'valueField' => 'nothi_office'])->where(['office_unit_organogram_id IN ' => $office_unit_organogram_id, 'is_archive' => 0, 'is_finished' => 0, 'is_new' => 0])->count();
    }

    public function getTotalNothiCount($office_unit_organogram_id =[], $current_nothi_office_id= 0){
        $onisponno_nothi_count = $this->getOnisponnoNothiCount($office_unit_organogram_id, $current_nothi_office_id);
        $onisponno_other_nothi_count =$this->getOnisponnoOtherNothiCount($office_unit_organogram_id, $current_nothi_office_id);
        return ($onisponno_nothi_count+$onisponno_other_nothi_count);
    }
   
}