<?php

namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

class NothiMasterMovementsTable extends ProjapotiTable {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->addBehavior('MobileTrack');
    }

    public function getLastMove($nothiPartId) {
        return $this->find()->where(['nothi_part_no' => $nothiPartId])->order(['id desc'])->first();
    }

    public function getInboxCountBy_employee_designation($designation_id) {
        return $this->find()->where(['to_officer_designation_id IN ' => $designation_id])->count();
    }

    public function getOutboxCountBy_employee_designation($designation_id) {
        return $this->find()->where(['from_officer_designation_id IN ' => $designation_id])->count();
    }

    public function getTotalMove($nothiPartId, $employee_office = false) {
        if ($employee_office) {
            return $this->find()->where(['nothi_part_no' => $nothiPartId,'movement_type' => 1, 'from_officer_designation_id !=' => $employee_office['office_unit_organogram_id']])->toArray();
        } else {
            return $this->find()->where(['nothi_part_no' => $nothiPartId,'movement_type' => 1])->toArray();
        }
    }
    public function countTotalMove($nothiPartId){
          return $this->find()->where(['nothi_part_no' => $nothiPartId,'movement_type' => 1])->count();
    }
    public function getLastNoteByMovement($nothi_master_id = 0,$designation_id = 0){
        if(empty($nothi_master_id) && empty($designation_id)){
            return [];
        }
        $last_part_info = $this->find()->select(['nothi_part_no'])->where(['nothi_master_id' => $nothi_master_id,'from_officer_designation_id' => $designation_id])->order(['id desc'])->first();

        if(!empty($last_part_info)){
            $part_info = TableRegistry::get('NothiParts')->find()->where(['id' => $last_part_info['nothi_part_no']])->first();
        }
        if(!empty($part_info)){
            return $part_info;
        }
        return [];
    }
    public function deleteLastMovementForError($nothi_part_no,$from_designation_id,$to_designation_id){
        $lastMove = $this->find()->where(['nothi_part_no' => $nothi_part_no])->order(['id desc'])->first();
        if(!empty($lastMove)){
            //check to from
            if($lastMove['from_officer_designation_id'] == $from_designation_id && $lastMove['to_officer_designation_id'] == $to_designation_id){
                // delete as error occurred
                $this->deleteAll(['id' => $lastMove['id']]);
            }
        }
    }

    public function afterSave($event, $entity, $options = []){
        if($entity->isNew())
        {
            $table_nothi_register_lists= TableRegistry::get('NothiRegisterLists');
            $table_nothi_register_lists->saveData($entity);
        }
    }

}
