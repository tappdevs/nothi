<?php

namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use mysql_xdevapi\Exception;

class NothiMastersTable extends ProjapotiTable
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->addBehavior('Timestamp');
        $this->hasMany('NothiParts', [
            'foreignKey' => 'nothi_masters_id'
        ]);
    }

    public function validationAdd($validator)
    {
        $validator
            ->notEmpty('office_units_id', "এই তথ্য ফাকা রাখা যাবে না")
            ->notEmpty('office_units_organogram_id', "এই তথ্য ফাকা রাখা যাবে না")
            ->notEmpty('office_units_organogram_name', "এই তথ্য ফাকা রাখা যাবে না")
            ->notEmpty('nothi_types_id', 'notBlank', "নথির ধরণ ফাকা রাখা যাবে না")
            ->notEmpty('nothi_no', "নথি নম্বর ফাকা রাখা যাবে না")
            ->notEmpty('subject', "নথির বিষয় ফাকা রাখা যাবে না")
            ->notEmpty('nothi_no', "নথি নম্বর ফাকা রাখা যাবে না")
            ->add('nothi_no', 'isUnique', [
                'rule' => function ($data, $provider) {
                    if (!empty($provider['data']['id'])) {
                        return true;
                    } else {
                        if ($this->find()->where(['nothi_no' => $data])->count() > 0) return "নথি নম্বরটি পূর্বে ব্যবহৃত হয়েছে";
                    }
                    return true;
                },
                'message' => __("নথি নম্বরটি পূর্বে ব্যবহৃত হয়েছে")
            ]);

        return $validator;
    }

    public function hasThisType($typeId)
    {
        return $this->find()->select(['id'])->where(['nothi_types_id' => $typeId])->count();
    }

    public function getAll($condition = '', $page = 0, $len = 10, $order = "NothiMasters.modified DESC")
    {

        $queryString =
            $this->find()->select([
                "NothiMasters.id",
                "NothiMasters.nothi_no",
                "NothiMasters.nothi_types_id",
                "NothiMasters.office_id",
                "NothiMasters.office_units_id",
                "NothiMasters.subject",
                "NothiMasters.nothi_created_date",
                "NothiMasters.created",
                "NothiMasters.nothi_class",
                "NothiMasters.modified",
                "NothiMasters.is_active",
                "NothiTypes.type_name"
            ])->where(["NothiMasters.is_deleted" => 0])
                ->join([
                    'NothiTypes' => [
                        'table' => 'nothi_types',
                        "conditions" => "NothiMasters.nothi_types_id= NothiTypes.id AND NothiTypes.is_deleted=0",
                        "type" => "INNER"
                    ]
                ])
                ->order($order);

        if (!empty($condition)) {
            $queryString = $queryString->where([$condition]);
        }
        if (!empty($page)) {
            $queryString = $queryString->page($page, $len);
        }

        return $queryString;
    }

    public function getAllNothi($condition = '', $page = 0, $len = 10, $order = "NothiMasters.modified DESC")
    {

        $queryString =
            $this->find()->select([
                "NothiMasters.id",
                "NothiMasters.nothi_no",
                "NothiMasters.nothi_types_id",
                "NothiMasters.office_id",
                "NothiMasters.office_units_id",
                "NothiMasters.subject",
                "NothiMasters.nothi_created_date",
                "NothiMasters.created",
                "NothiMasters.nothi_class",
                "NothiMasters.modified",
                "NothiMasters.is_active"
            ])->where(["NothiMasters.is_deleted" => 0])
                ->order($order);

        if (!empty($condition)) {
            $queryString = $queryString->where([$condition]);
        }
        if (!empty($page)) {
            $queryString = $queryString->page($page, $len);
        }

        return $queryString;
    }

    public function getPartsList($nothi_masters_id)
    {

        return $query = $this->find()->contain(['NothiParts'])->where(['id IN' => $nothi_masters_id]);
    }

    public function getName($nothi_masters_id)
    {
        return $this->find()->select(['subject'])->where(['id IN' => $nothi_masters_id]);
    }

    public function allMasterInfo($office_id = 0, $unit_id = 0, $designation_id = 0, $time = [],$list = 0, $list_column = ['keyFiled' => 'id', 'valueField' => 'id'])
    {
        if($list){
            $query = $this->find('list',$list_column);
        }else{
            $query = $this->find();
        }

        if (!empty($office_id)) {
            $query = $query->where(['office_id' => $office_id]);
        }

        if (!empty($unit_id)) {
            $query = $query->where(['office_units_id' => $unit_id]);
        }

        if (!empty($designation_id)) {
            $query = $query->where(['office_units_organogram_id' => $designation_id]);
        }

        if (!empty($time[0])) {
            $query = $query->where(['DATE(created) >=' => $time[0]]);
        }

        if (!empty($time[1])) {
            $query = $query->where(['DATE(created) <=' => $time[1]]);
        }

        return $query;
    }

    private $employeeOffice;

    public function getNothiListBk($controller, $employeeOffice, $type = 'all',$nothi_master_ids=0)
    {
        $this->employeeOffice = $employeeOffice;

        $start = isset($controller->request->data['start']) ? intval($controller->request->data['start']) : 0;
        $len = isset($controller->request->data['length']) ? intval($controller->request->data['length']) : 200;
        //$page = !empty($controller->request->data['page']) ? intval($controller->request->data['page']) : (($start / $len) + 1);

        $condition = [];
		$condition[]='(NothiMasters.is_archived is NULL OR NothiMasters.is_archived = 0 )';

        $subject = isset($controller->request->data['subject']) ? h(trim($controller->request->data['subject']))
            : '';
        if (!empty($subject)) {
            $condition['NothiMasters.subject LIKE '] =   "%{$subject}%";
        }
        $unit = isset($controller->request->data['unit']) ? h(trim($controller->request->data['unit']))
            : '';
        if (!empty($unit)) {
            $condition['NothiMasters.office_units_id LIKE '] =   "%{$unit}%";
        }

        $type_name = isset($controller->request->data['type_name']) ? h(trim($controller->request->data['type_name']))
            : '';
        if (!empty($type_name)) {
            $condition['NothiTypes.type_name LIKE '] =   "%{$type_name}%";
        }

        $nothi_no = isset($controller->request->data['nothi_no']) ? h(trim($controller->request->data['nothi_no']))
            : '';
        if (!empty($nothi_no)) {
            $condition['NothiMasters.nothi_no LIKE '] =   "%{$nothi_no}%";
        }

        $from_date = isset($controller->request->data['from']) ? h(trim($controller->request->data['from']))
            : '';
        if (!empty($from_date)) {
            $condition[' date(NothiMasters.modified) >= '] =   "{$from_date}";
        }

        $end_date = isset($controller->request->data['to']) ? h(trim($controller->request->data['to']))
            : '';
        if (!empty($end_date)) {
            $condition[' date(NothiMasters.modified) <= '] =   "{$end_date}";
        }

        if (!empty($nothi_master_ids)) {
            $condition['NothiMasters.id IN'] =  array_values($nothi_master_ids);
        }

        if($type=='inbox'){
            $dataList = $this->inboxList()
                ->where(['NothiMasterCurrentUsers.nothi_office' => $employeeOffice['office_id'],
                    'NothiMasterCurrentUsers.is_archive'=>0,
                    'NothiMasterCurrentUsers.is_new'=>0,
                    'NothiMasterCurrentUsers.is_finished'=>0
                ]);
        }else if ($type == 'all'){
            $dataList = $this->sokolList()
                ->where(['NothiMasterPermissions.nothi_office' => $employeeOffice['office_id']
                ]);
        }

        if(!empty($condition)){
            $dataList = $dataList->where($condition);
        }

        $totalCountData = $dataList->count();
        $totalListData = $dataList->offset($start)->limit($len)->toArray();

        return [
            'total'=>$totalCountData,
            'data'=>$totalListData,
        ];
    }

    public function getNothiList($controller, $employeeOffice, $type = 'all',$nothi_master_ids=0)
    {
        $this->employeeOffice = $employeeOffice;

        $start = isset($controller->request->data['start']) ? intval($controller->request->data['start']) : 0;
        $len = isset($controller->request->data['length']) ? intval($controller->request->data['length']) : 200;
        //$page = !empty($controller->request->data['page']) ? intval($controller->request->data['page']) : (($start / $len) + 1);

        $condition = [];
//		$condition[]='(NothiMasters.is_archived is NULL OR NothiMasters.is_archived = 0 )';

        $subject = isset($controller->request->data['subject']) ? h(trim($controller->request->data['subject']))
            : '';
        if (!empty($subject)) {
            array_push($condition,['NothiMasters.subject LIKE'=> "%".$subject."%"]);
        }
        $unit = isset($controller->request->data['unit']) ? h(trim($controller->request->data['unit']))
            : '';
        if (!empty($unit)) {
            array_push($condition,['NothiMasters.office_units_id LIKE'=> "%".$unit."%"]);
        }

        $type_name = isset($controller->request->data['type_name']) ? h(trim($controller->request->data['type_name']))
            : '';
        if (!empty($type_name)) {
            array_push($condition,['NothiTypes.type_name LIKE'=> "%".$type_name."%"]);
        }

        $nothi_no = isset($controller->request->data['nothi_no']) ? h(trim($controller->request->data['nothi_no']))
            : '';
        if (!empty($nothi_no)) {
            array_push($condition,['NothiMasters.nothi_no LIKE'=> "%".$nothi_no."%"]);
        }

        $nothi_type_id = isset($controller->request->data['nothi_type_id']) ? h(trim($controller->request->data['nothi_type_id'])) : '';
        if (!empty($nothi_type_id)) {
            array_push($condition,['NothiMasters.nothi_types_id'=> $nothi_type_id]);
        }

        $from_date = isset($controller->request->data['from']) ? h(trim($controller->request->data['from']))
            : '';
        $end_date = isset($controller->request->data['to']) ? h(trim($controller->request->data['to']))
            : '';

        if (!empty($nothi_master_ids)) {
            array_push($condition,['NothiMasters.id IN'=> array_values($nothi_master_ids)]);
        }

        if($type=='inbox'){
            $dataList = $this->inboxList()
                ->where(['NothiMasterCurrentUsers.nothi_office' => $employeeOffice['office_id'],
                    'NothiMasterCurrentUsers.is_archive'=>0,
                    'NothiMasterCurrentUsers.is_new'=>0,
                    'NothiMasterCurrentUsers.is_finished'=>0
                ]);
        }else if ($type == 'all'){
            $dataList = $this->sokolList()
                ->where(['NothiMasterPermissions.nothi_office' => $employeeOffice['office_id']
                ]);
        }

        if(!empty($condition)){
            $dataList = $dataList->where($condition);
        }
        if(!empty($from_date) && !empty($end_date)){
            $dataList = $dataList->where(function($dataList) use ($from_date,$end_date){
                return $dataList->between('date(NothiMasters.modified)',$from_date,$end_date);
            });
        };

        $dataList = $dataList->where(function ($dataList){
            $orCondition = $dataList->or_(['NothiMasters.is_archived IS NULL'])->add(['NothiMasters.is_archived = 0']);
            return $dataList->and_($orCondition);
        });
        //$totalCountData = $dataList->count();
        $totalListData = [];
        foreach ($dataList->hydrate(false)->toArray() as $data) {
            if (!empty($totalListData)) {
                if (isset($data['nothi_masters_id'])) {
                    $column = 'nothi_masters_id';
                } else {
                    $column = 'nothi_master_id';
                }
                if (!in_array($data[$column], array_column($totalListData, $column))) {
                    $totalListData[] = $data;
                }
            } else {
                $totalListData[] = $data;
            }
        }
        $totalCountData = count($totalListData);
        $totalListData = array_slice($totalListData, $start, $len);
        //$totalListData = $dataList->offset($start)->limit($len)->hydrate(false)->toArray();

        return [
            'total'=>$totalCountData,
            'data'=>$totalListData,
        ];
    }

    private function inboxList()
    {
        $nothiMasterCurrentUserTable = TableRegistry::get('NothiMasterCurrentUsers');

        return $nothiMasterCurrentUsers = $nothiMasterCurrentUserTable->find('all',
            [
                //'group' => ['NothiMasterCurrentUsers.nothi_master_id'],
                'order' => ['NothiMasterCurrentUsers.priority' => 'DESC', 'NothiMasterCurrentUsers.issue_date' => 'DESC']
            ]
        )->select([
            'NothiMasterCurrentUsers.nothi_master_id',
            'NothiMasterCurrentUsers.nothi_part_no',
            'NothiMasterCurrentUsers.nothi_office',
            'NothiMasterCurrentUsers.view_status',
            'NothiMasterCurrentUsers.issue_date',
            'NothiMasterCurrentUsers.forward_date',
            'NothiMasterCurrentUsers.priority',
            'NothiMasterCurrentUsers.is_new',
            'NothiMasterCurrentUsers.modified',
            //'nothiCount'=>'COUNT(NothiMasterCurrentUsers.nothi_part_no)',
            'NothiMasters.nothi_no',
            'NothiMasters.subject',
            'NothiMasters.office_units_id',
            'NothiMasters.modified',
            'NothiTypes.type_name',
        ])->join([
//            'NothiMasterCurrentUsers2' => [
//                'type' => 'left',
//                'table' => 'nothi_master_current_users',
//                'conditions' => 'NothiMasterCurrentUsers.nothi_master_id = NothiMasterCurrentUsers2.nothi_master_id AND
//                 NothiMasterCurrentUsers.nothi_office = NothiMasterCurrentUsers2.nothi_office AND
//                 NothiMasterCurrentUsers.issue_date < NothiMasterCurrentUsers2.issue_date AND
//                 NothiMasterCurrentUsers2.office_unit_organogram_id = NothiMasterCurrentUsers.office_unit_organogram_id AND
//                 NothiMasterCurrentUsers.is_new = NothiMasterCurrentUsers2.is_new AND
//                 NothiMasterCurrentUsers.is_finished = NothiMasterCurrentUsers2.is_finished AND
//                 NothiMasterCurrentUsers.is_archive = NothiMasterCurrentUsers2.is_archive
//                 '
//            ],
            'NothiMasters' => [
                'type' => 'INNER',
                'table' => 'nothi_masters',
                'conditions' => 'NothiMasterCurrentUsers.nothi_master_id = NothiMasters.id'
            ],
            'NothiTypes' => [
                'type' => 'INNER',
                'table' => 'nothi_types',
                'conditions' => 'NothiMasters.nothi_types_id=NothiTypes.id'
            ]
        ])
//            ->where(['NothiMasterCurrentUsers2.id IS NULL'])
            ->where([
                'NothiMasterCurrentUsers.office_unit_organogram_id' => $this->employeeOffice['office_unit_organogram_id']
            ]);
            //->group(['NothiMasterCurrentUsers.nothi_master_id']);
    }


    private function sentList()
    {
        $nothiMasterPermittedUserTable = TableRegistry::get('NothiMasterPermissions');
    }


    private function sokolList()
    {
        $nothiMasterPermittedUserTable = TableRegistry::get('NothiMasterPermissions');

        return $nothiMasterCurrentUsers = $nothiMasterPermittedUserTable->find()->select([
            'NothiMasterPermissions.nothi_masters_id',
            'NothiMasterPermissions.nothi_part_no',
            'NothiMasterPermissions.nothi_office',
            'NothiMasterPermissions.modified',
            'NothiMasters.nothi_no',
            'NothiMasters.subject',
            'NothiMasters.office_units_id',
            'NothiMasters.modified'
        ])
            ->join([
            'NothiMasters' => [
                'type' => 'INNER',
                'table' => 'nothi_masters',
                'conditions' => 'NothiMasterPermissions.nothi_masters_id = NothiMasters.id'
            ]
        ])
            ->where([
                'NothiMasterPermissions.office_unit_organograms_id' => $this->employeeOffice['office_unit_organogram_id']
            ])
            ->group(['NothiMasterPermissions.nothi_masters_id'])
            ->order(['NothiMasterPermissions.modified' => 'DESC']);
    }


    public function afterSave($event, $entity, $options = []){
        if($entity->isNew())
        {
            $table_nothi_register_lists= TableRegistry::get('NothiRegisterLists');
            $response = $table_nothi_register_lists->saveDataNothiParts($entity);
            if ($response['status'] == 'error') {
                throw new \Exception('NothiRegisterLists is not save. '.$response['msg']);
            }
        }
    }
}