<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Network\Email\Email;
use Cake\Datasource\ConnectionManager;
use Cake\Controller\Component;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;
class NothiNoteAttachmentRefsTable extends ProjapotiTable {
    
    public function initialize(array $config) {
        //parent::initialize($config);
        $this->table('nothi_note_attachment_refs');
        $this->primaryKey('id');
    }
}
