<?php
namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

class NothiNoteSignaturesTable extends ProjapotiTable
{
        public function initialize(array $config)
        {
          $this->addBehavior('MobileTrack');
      }
    public function validationAdd($validator)
    {
        $validator
            ->notEmpty('nothi_masters_id', "এই তথ্য ফাকা রাখা যাবে না")
            ->notEmpty('nothi_note_id', "এই তথ্য ফাকা রাখা যাবে না");

        return $validator;
    }

    public function validationEdit($validator)
    {

        return $validator;
    }

    public function getByNoteid($noteId, $nothimasterid)
    {

        return $this->find()->where(['nothi_note_id' => $noteId, 'nothi_part_no' => $nothimasterid])->order(['id desc']);
    }

    public function getByEmployeeId($designation_id, $nothimasterid, $is_signature = 0)
    {
        return $this->find()->where(['office_organogram_id' => $designation_id, 'nothi_part_no' => $nothimasterid,'is_signature'=>$is_signature])->order(['id desc']);
    }

    public function getByNothiMasterId($nothimasterid)
    {
        return $this->find()->where(['nothi_part_no' => $nothimasterid])->order(['id desc']);
    }
    public function getLastSignature($designation_id = 0,$nothimasterid = 0, $noteId = 0){
        $query = $this->find();
        if(!empty($designation_id)){
            $query = $query->where(['office_organogram_id' => $designation_id]);
        }
        if(!empty($nothimasterid)){
            $query = $query->where(['nothi_part_no' => $nothimasterid]);
        }
        if(!empty($noteId)){
            $query = $query->where(['nothi_note_id' => $noteId]);
        }

        return $query;

    }
    public function deleteLastSignatureForError($nothi_part_no = 0,$nothi_note_id = 0,$to_designation_id){
        $lastSingature = $this->find();
        if(!empty($nothi_part_no)){
            $lastSingature->where(['nothi_part_no' => $nothi_part_no]);
        }
        if(!empty($nothi_note_id)){
            $lastSingature->where(['nothi_note_id' => $nothi_note_id]);
        }
        $lastSignature = $lastSingature->order(['id desc'])->first();
        if(!empty($lastSingature)){
            //check to from
            if($lastSignature['office_organogram_id'] == $to_designation_id && $lastSignature['is_signature'] == 0){
                // delete as error occurred
                $this->deleteAll(['id' => $lastSignature['id']]);
            }
        }
    }
    public function unsetLastSignatureForError($nothi_part_no = 0,$nothi_note_id = 0,$from_designation_id){
        $lastSignature = $this->find();
        if(!empty($nothi_part_no)){
            $lastSignature->where(['nothi_part_no' => $nothi_part_no]);
        }
        if(!empty($nothi_note_id)){
            $lastSignature->where(['nothi_note_id' => $nothi_note_id]);
        }
        $lastSignature = $lastSignature->order(['id desc'])->first();
        if(!empty($lastSignature)){
            //check to from
            if($lastSignature['office_organogram_id'] == $from_designation_id && $lastSignature['is_signature'] == 1){
                // delete as error occurred
                $occurred_in_last_minutes = date('Y-m-d H:i:s', strtotime("-2 min"));
                if(!empty($lastSignature['created']) && $lastSignature['created'] >= $occurred_in_last_minutes){
                    $this->updateAll(['is_signature' => 0],['id' => $lastSignature['id']]);
                }
            }
        }
    }
}