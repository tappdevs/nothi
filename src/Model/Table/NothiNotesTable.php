<?php
namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

class NothiNotesTable extends ProjapotiTable
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->addBehavior('MobileTrack');
    }

    public function getNotes($nothiPartId = 0, $employeeId = 0)
    {
        $query = $this->find();
        if(!empty($nothiPartId)){
            $query->where(['nothi_part_no' => $nothiPartId]);
        }
        if(!empty($employeeId)){
            $query->where(['employee_id' => $employeeId]);
        }
        return $query;
    }

    public function getNotesInfo($nothiPartId, $noteid)
    {

        return $this->find()->where(['nothi_part_no' => $nothiPartId, 'note_no' => $noteid])->first();
    }

    public function getLastNote($nothiPartId){
        return $this->find()->where(['nothi_part_no' => $nothiPartId])->order(['id DESC'])->first();
    }

    public function getFirstNote($nothiPartId){
        return $this->find()->where(['nothi_part_no' => $nothiPartId])->order(['subject desc'])->first();
    }
    
    public function getNotesbyNotesheet($noteSheet_no)
    {
        return $this->find()->where(['nothi_notesheet_id' => $noteSheet_no])->toArray();
    }

    public function checkEmptyNote($nothi_master_id, $nothi_parts, $count = true){
        if(!empty($nothi_master_id) && !empty($nothi_parts)){
            $query = $this->find()->where(['NothiNotes.nothi_part_no IN'=>$nothi_parts,'NothiNotes.nothi_master_id'=>$nothi_master_id,'note_no <>' => -1])->distinct(['NothiNotes.nothi_part_no']);
            if($count==false){
                return $query->first();
            }
            return $query->count();
        }
        else if(empty ($nothi_master_id) && !empty ($nothi_parts)){
            $query = $this->find()
                    ->where(['NothiNotes.nothi_part_no IN'=>$nothi_parts,'note_no <>' => -1])->distinct(['NothiNotes.nothi_part_no']);

            if($count==false){
                return $query->first();
            }
            return $query->count();
        }
      
    }

    public function getLastNoteSheetId($nothimasterid, $nothi_office){
        return $this->find()->select([ 'nothi_notesheet_id' ])->where(['nothi_part_no'=>$nothimasterid,'office_id'=>$nothi_office])->first();
    }
    public function getLastNotetId($nothimasterid, $nothi_office){
        return $this->find()->select([ 'id' ])->where(['nothi_part_no'=>$nothimasterid,'office_id'=>$nothi_office])->first();
    }
    public function findfistNoteforDashboardCount($nothi_part_no = 0,$created_date = '',$office_id = 0,$date_range = []){
        if(empty($nothi_part_no))
            return 0 ;
        $query = $this->find()->where(['nothi_part_no']);
        if(!empty($created_date)){
            $query->where(['created >=' => $created_date]);
        }
        if(!empty($office_id)){
            $query->where(['office_id' => $office_id]);
        }
        if(!empty($date_range)){
            if(!empty($date_range[0])){
                $query->where(['date(created) >=' => $date_range[0]]);
            }
            if(!empty($date_range[1])){
                $query->where(['date(created) <=' => $date_range[1]]);
            }
        }
        return $query->count();

    }
    public function findNoteAfterNisponno($nothi_part_no,$nisponno_time){
        $query = $this->find();
        if(!empty($nothi_part_no)){
            $query->where(['nothi_part_no' => $nothi_part_no]);
        }
        if(!empty($nisponno_time)){
            $query->where(['created >' => $nisponno_time]);
        }
        return $query->count();
          
    }
    public function getEmptyNoteInfo($nothi_part_no = []){
        if(empty($nothi_part_no)){
            return;
        }
        else{
            $connection = ConnectionManager::get('default');

           $stmt = $connection->execute('SELECT a.id, (select count(*) from nothi_notes b where b.nothi_part_no = a.id) as total_note from nothi_parts a WHERE a.id IN ('.implode(',',$nothi_part_no).') HAVING total_note = 0');

            return $stmt->fetch('assoc');

//            $query = $this->find();
//            return $query->select(['NothiNotes.nothi_part_no','total_note'  =>$query->func()->count('NothiNotes.id')])
//            ->join([
//                "NothiParts" => [
//                    'table' => 'nothi_parts', 'type' => 'inner',
//                    'conditions' => ['NothiNotes.nothi_part_no <> NothiParts.id','NothiNotes.nothi_part_no IN' =>$nothi_part_no]
//                ]
//            ])
//            ->where(['NothiNotes.nothi_part_no IN' =>$nothi_part_no])->group(['NothiNotes.nothi_part_no'])->toArray();
        }
    }
}