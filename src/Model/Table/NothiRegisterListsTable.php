<?php

namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

class NothiRegisterListsTable extends ProjapotiTable
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->addBehavior('Timestamp');
    }


    public function getAllSentNothi($startDate,$endDate,$field,$condition=[],$orCondition=[])
    {
        //$table_dm    = TableRegistry::get('NothiRegisterLists');
        $table_dm    = $this;
        $queryString = $table_dm->find()->select([
            "NothiRegisterLists.id",
            "NothiRegisterLists.modified",
            "NothiRegisterLists.from_office_unit_id",
            "NothiRegisterLists.from_office_unit_name",
            "NothiRegisterLists.from_officer_name",
            "NothiRegisterLists.from_officer_designation_label",
            "NothiRegisterLists.to_officer_name",
            "NothiRegisterLists.created",
            "NothiRegisterLists.to_office_unit_id",
            "NothiRegisterLists.to_officer_designation_label",
            "NothiRegisterLists.nothi_part_no",
            "NothiRegisterLists.subject",
            "NothiRegisterLists.nothi_no",
            "NothiRegisterLists.sending_date",
            "NothiRegisterLists.created",
            "NothiRegisterLists.nothi_modified"

        ]);


        if (!empty($condition)) {
            $queryString = $queryString->where($condition);
        }
        if (!empty($orCondition)) {
            $queryString = $queryString->orWhere($orCondition);
        }
        $queryString = $queryString->order("NothiRegisterLists.sending_date DESC")->distinct(['NothiRegisterLists.id']);
        $queryString = $queryString->where(function($query) use ($startDate,$endDate,$field){
            return $query->between('date('.$field.')',$startDate,$endDate);
        });
        return $queryString;
    }



    public function getAllReceivingNothi($startDate,$endDate,$field,$condition=[],$orCondition=[])
    {
        //$table_dm    = TableRegistry::get('NothiRegisterLists');
        $table_dm    = $this;
        $queryString = $table_dm->find()->select([
            "NothiRegisterLists.id",
            "NothiRegisterLists.modified",
            "NothiRegisterLists.from_office_unit_id",
            "NothiRegisterLists.from_office_unit_name",
            "NothiRegisterLists.from_officer_name",
            "NothiRegisterLists.from_officer_designation_label",
            "NothiRegisterLists.to_officer_name",
            "NothiRegisterLists.created",
            "NothiRegisterLists.to_office_unit_id",
            "NothiRegisterLists.to_office_unit_name",
            "NothiRegisterLists.to_officer_designation_label",
            "NothiRegisterLists.nothi_part_no",
            "NothiRegisterLists.subject",
            "NothiRegisterLists.nothi_no",
            "NothiRegisterLists.sending_date",
            "NothiRegisterLists.created",
            "NothiRegisterLists.nothi_modified"

        ]);


        if (!empty($condition)) {
            $queryString = $queryString->where($condition);
        }
        if (!empty($orCondition)) {
            $queryString = $queryString->orWhere($orCondition);
        }
        $queryString = $queryString->order("NothiRegisterLists.sending_date DESC")->distinct(['NothiRegisterLists.id']);
        $queryString = $queryString->where(function($query) use ($startDate,$endDate,$field){
            return $query->between('date('.$field.')',$startDate,$endDate);
        });
        return $queryString;
    }


    public function getNothiDiary($startDate,$endDate,$field,$condition=[],$orCondition=[])
    {
        //$table_dm    = TableRegistry::get('NothiRegisterLists');
        $table_dm    = $this;
        $queryString = $table_dm->find()->join([

            'NothiTypes' => [
                'table' => 'nothi_types',
                'type' => 'INNER',
                'conditions' => ['NothiRegisterLists.nothi_type_id=NothiTypes.id']
            ]
        ])->select([
            'NothiRegisterLists.id',
            'NothiRegisterLists.modified',
            'NothiRegisterLists.nothi_created_date',
            'NothiRegisterLists.created_unit_id',
            'NothiRegisterLists.from_officer_name',
            'NothiRegisterLists.to_office_unit_id',
            'NothiRegisterLists.to_office_unit_name',
            'NothiRegisterLists.from_office_name',
            'NothiRegisterLists.to_office_name',
            'NothiRegisterLists.to_officer_name',
            'NothiRegisterLists.from_officer_designation_label',
            'NothiRegisterLists.to_officer_designation_label',
            'NothiRegisterLists.nothi_part_no',
            'NothiRegisterLists.sending_date',
            'NothiRegisterLists.nothi_class',
            'NothiRegisterLists.nothi_type_id',


            'NothiRegisterLists.nothi_no',
            'NothiRegisterLists.subject',
            'NothiRegisterLists.created_office_id',
            'NothiRegisterLists.created',
            'NothiTypes.type_name',

        ]);
        if (!empty($condition)) {
            $queryString = $queryString->where($condition);
        }
        if (!empty($orCondition)) {
            $queryString = $queryString->orWhere($orCondition);
        }
        $queryString = $queryString->order("NothiRegisterLists.sending_date DESC")->distinct(['NothiRegisterLists.id']);
        $queryString = $queryString->where(function($query) use ($startDate,$endDate,$field){
            return $query->between('date('.$field.')',$startDate,$endDate);
        });

        return $queryString;
    }



    public function getAllPotrojariNothi($startDate,$endDate,$field,$condition=[],$orCondition=[])
    {
        //$table_dm    = TableRegistry::get('NothiRegisterLists');
        $table_dm    = $this;
        $queryString = $table_dm->find()
        ->join([
            'NothiTypes' => [
                'table' => 'nothi_types',
                "conditions" => ["NothiTypes.id = NothiRegisterLists.nothi_type_id"],
                "type" => "INNER"
            ]
        ]);


        if (!empty($condition)) {
            $queryString = $queryString->where($condition);
        }
        if (!empty($orCondition)) {
            $queryString = $queryString->orWhere($orCondition);
        }
        $queryString = $queryString->order("NothiRegisterLists.sending_date DESC")->distinct(['NothiRegisterLists.id']);
        $queryString = $queryString->where(function($query) use ($startDate,$endDate,$field){
            return $query->between('date('.$field.')',$startDate,$endDate);
        });
        return $queryString;
    }


    public function saveData($entity)
    {
//        $conn = \Cake\Datasource\ConnectionManager::get('default');
//        $conn->begin();
//        $conn->commit();
//        $conn->rollback();

        try {
            $entity_data = json_encode($entity);
            $entity_data = json_decode($entity, true);
            unset($entity_data['id']);
            $nothi_master_id = $entity_data['nothi_master_id'];
            $table_nothi_master = TableRegistry::get('NothiMasters');
            $nothi_master_data = $table_nothi_master->find()->where(['id' => $nothi_master_id])->first();

            $entity_data['created_office_id'] = $nothi_master_data['office_id'];
            $entity_data['created_unit_id'] = $nothi_master_data['office_units_id'];
            $entity_data['created_designation_id'] = $nothi_master_data['office_units_organogram_id'];
            $entity_data['nothi_no'] = $nothi_master_data['nothi_no'];
            $entity_data['subject'] = $nothi_master_data['subject'];
            $entity_data['sending_date'] = $nothi_master_data['created'];
            $entity_data['nothi_created_date'] = $nothi_master_data['nothi_created_date'];
            $entity_data['nothi_class'] = $nothi_master_data['nothi_class'];
            $entity_data['movement_type'] = "Sent";
            $entity_data['nothi_type_id'] = $nothi_master_data['nothi_types_id'];
            $entity_data['nothi_modified'] = $entity_data['modified'];

            $nothi_register_lists_new = $this->newEntity();
            $nothi_register_lists_data = $this->patchEntity($nothi_register_lists_new, $entity_data);
            $this->save($nothi_register_lists_data);

//            if(isEmpty($entity_data['potrojari_id']))
//            $potrojariRegisterTable = TableRegistry::get("PotrojariRegisters");
//            $potrojariRegisterTable->updatePotrojariRegister($potrojari_id);

        } catch (\Exception $ex) {
            return $ex->getMessage();
        }
    }


    public function saveDataNothiParts($entity) {

        try{
            $entity_data = json_encode($entity);
            $entity_data = json_decode($entity,true);
            $nothi_master_id = $entity_data['id'];
            unset($entity_data['id']);
            $entity_data['nothi_master_id'] = $nothi_master_id;
            $entity_data['from_office_id'] = $entity_data['office_id'];
            $entity_data['from_office_unit_id'] = $entity_data['office_units_id'];
            $entity_data['from_officer_designation_id'] = $entity_data['office_units_organogram_id'];
            $nothi_master_data    = $entity;

            $entity_data['created_office_id'] = $nothi_master_data['office_id'];
            $entity_data['created_unit_id'] = $nothi_master_data['office_units_id'];
            $entity_data['created_designation_id'] = $nothi_master_data['office_units_organogram_id'];

            $entity_data['nothi_no'] = $nothi_master_data['nothi_no'];
            $entity_data['subject'] = $nothi_master_data['subject'];
            $entity_data['sending_date'] = $nothi_master_data['created'];
            $entity_data['nothi_created_date'] = $nothi_master_data['nothi_created_date'];
            $entity_data['nothi_class'] = $nothi_master_data['nothi_class'];
            $entity_data['movement_type'] = "Created";
            $entity_data['nothi_type_id'] = $nothi_master_data['nothi_types_id'];
            $entity_data['nothi_modified'] = date('Y-m-d', strtotime($entity_data['modified']));
            $entity_data['created'] = date('Y-m-d', strtotime($entity_data['modified']));
            $entity_data['modified'] = date('Y-m-d', strtotime($entity_data['modified']));

            $nothi_register_lists_new = $this->newEntity();
            $nothi_register_lists_data = $this->patchEntity($nothi_register_lists_new,$entity_data);
            $this->save($nothi_register_lists_data);

            return ['status' => 'success', 'msg' => 'Success'];

        } catch(\Exception $ex) {
            return ['status' => 'error', 'msg' => $ex->getMessage()];
        }


    }


    public function saveDataNothiPotrojari($entity)
    {

        try {
            $entity_data_tmp= json_encode($entity);
            $entity_data = json_decode($entity_data_tmp, true);
            $potrojari_id = $entity_data['id'];
            unset($entity_data['id']);

            $nothi_master_id = $entity_data['nothi_master_id'];
            $table_nothi_master = TableRegistry::get('NothiMasters');
            $nothi_master_data = $table_nothi_master->find()->where(['id' => $nothi_master_id])->first();

            $entity_data['potrojari_id'] = $potrojari_id;
            $entity_data['created_office_id'] = $entity_data['office_id'];
            $entity_data['created_unit_id'] = $entity_data['office_unit_id'];
            $entity_data['created_designation_id'] = $entity_data['officer_designation_id'];
            $entity_data['from_office_id'] = $entity_data['office_id'];
            $entity_data['from_office_name'] = $entity_data['office_name'];
            $entity_data['from_officer_name'] = $entity_data['officer_name'];
            $entity_data['from_office_unit_id'] = $entity_data['office_unit_id'];
            $entity_data['from_office_unit_name'] = $entity_data['office_unit_name'];
            $entity_data['from_officer_designation_id'] = $entity_data['officer_designation_id'];
            $entity_data['from_officer_designation_label'] = $entity_data['officer_designation_label'];

            $entity_data['nothi_no'] = $nothi_master_data['nothi_no'];
            $entity_data['subject'] = $nothi_master_data['subject'];
            $entity_data['sending_date'] = $nothi_master_data['created'];
            $entity_data['nothi_created_date'] = $nothi_master_data['nothi_created_date'];
            $entity_data['nothi_class'] = $nothi_master_data['nothi_class'];
            $entity_data['movement_type'] = "Potrojari";
            $entity_data['nothi_type_id'] = $nothi_master_data['nothi_types_id'];
            $entity_data['nothi_modified'] = date('Y-m-d', strtotime($entity_data['modified']));
            $entity_data['created'] = date('Y-m-d', strtotime($entity_data['modified']));
            $entity_data['modified'] = date('Y-m-d', strtotime($entity_data['modified']));

            $nothi_register_lists_new = $this->newEntity();
            $nothi_register_lists_data = $this->patchEntity($nothi_register_lists_new, $entity_data);
            $this->save($nothi_register_lists_data);

            return ['status' => 'success', 'msg' => 'Success'];

        } catch(\Exception $ex) {
            return ['status' => 'error', 'msg' => $ex->getMessage()];
        }
    }




    public function getAllSectionWiseNothi($startDate,$endDate,$condition)
    {

        $table_dm = $this;

        $queryString = $table_dm->find()->select([
            "nothi_created_date",
            "id",
            "nothi_class",
            "nothi_part_no",
            "subject",
            "nothi_no",
            "created",
            "modified",
            "nothi_type_id",
            "created_unit_id",
            "created_office_id",
            "nothi_type_id",
            "NothiTypes.type_name"
        ])
            ->join([
                'NothiTypes' => [
                    'table' => 'nothi_types',
                    "conditions" => ["NothiRegisterLists.nothi_type_id = NothiTypes.id"],
                    "type" => "LEFT"
                ],
            ])
            ->where(function($query) use ($startDate,$endDate){
                return $query->between('date(NothiRegisterLists.nothi_created_date)',$startDate,$endDate);
            })
            ->where(['NothiRegisterLists.movement_type' => 'Created'])
            ->order("NothiRegisterLists.created_unit_id ASC, NothiRegisterLists.nothi_created_date DESC");

        if (!empty($condition)) {
            $queryString = $queryString->where([$condition]);
        }


        return $queryString;
    }


}