<?php
/**
 * Created by PhpStorm.
 * User: WINDOWS
 * Date: 10/1/2015
 * Time: 5:25 PM
 */

namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;

class NothiTypesTable extends ProjapotiTable
{

    public function initialize(array $config)
    {
        $this->displayField('type_name');
        $this->primaryKey('id');
    }

    public function validationAdd($validator)
    {
        $validator
            ->notEmpty('type_name', "এই তথ্য ফাকা রাখা যাবে না")
            ->add('type_name', 'isUnique',
                [
                    'rule' => function ($data, $provider) {

                        if ($this->find()->where(['type_name' => $data, 'is_deleted' => 0, 'office_unit_id' => $provider['data']['office_unit_id'],
                                'office_id' => $provider['data']['office_id']])->count() > 0)
                            return false;
                        return true;
                    },
                    'message' => __('এটি পূর্বে ব্যবহৃত হয়েছে')
                ])
            ->add('type_code', 'isUnique',
                [
                    'rule' => function ($data, $provider) {
                        if ($this->find()->where(['type_code' =>enTobn($data), 'is_deleted' => 0, 'office_unit_id' => $provider['data']['office_unit_id'],
                                'office_id' => $provider['data']['office_id']])->count() > 0)
                            return false;
                        if ($this->find()->where(['type_code' =>bnToen($data), 'is_deleted' => 0, 'office_unit_id' => $provider['data']['office_unit_id'],
                                'office_id' => $provider['data']['office_id']])->count() > 0)
                            return false;
                        return true;
                    },
                    'message' => __('এটি পূর্বে ব্যবহৃত হয়েছে')
                ]);

        return $validator;
    }

    public function validationEdit($validator)
    {
        $validator
            ->notEmpty('type_name', "এই তথ্য ফাকা রাখা যাবে না")
            ->add('type_name', 'isUnique',
                [
                    'rule' => function ($data, $provider) {
                        if ($this->find()->where(['type_name' => $data, 'is_deleted' => 0, 'id <>' => $provider['data']['id'],
                                'office_unit_id' => $provider['data']['office_unit_id'], 'office_id' => $provider['data']['office_id']])->count()
                            > 0) return "এটি পূর্বে ব্যবহৃত হয়েছে";
                        return true;
                    },
                    'message' => __('এটি পূর্বে ব্যবহৃত হয়েছে')
                ])
            ->add('type_code', 'isUnique',
                [
                    'rule' => function ($data, $provider) {
                        if ($this->find()->where(['type_code' =>enTobn($data), 'is_deleted' => 0, 'office_unit_id' => $provider['data']['office_unit_id'],
                                'office_id' => $provider['data']['office_id'], 'id <>' => $provider['data']['id']])->count()
                            > 0) return false;
                        if ($this->find()->where(['type_code' =>bnToen($data), 'is_deleted' => 0, 'office_unit_id' => $provider['data']['office_unit_id'],
                                'office_id' => $provider['data']['office_id'],'id <>' => $provider['data']['id']])->count() > 0)
                            return false;
                        return true;
                    },
                    'message' => __('এটি পূর্বে ব্যবহৃত হয়েছে')
                ]);

        return $validator;
    }

    public function getAllTypes($page = 0, $len = 100, $employee = null,$doron=null)
    {
        $queryString = $this->find()->select([
            "NothiTypes.id",
            "NothiTypes.type_name",
            "NothiTypes.type_code",
            "NothiTypes.created",
            "NothiTypes.type_last_number",
            "totalTypes" => "(select count(NothiMasters.nothi_types_id) as countTotal from 
            nothi_masters as NothiMasters where NothiMasters.nothi_types_id = NothiTypes.id)"
        ])->where(["NothiTypes.is_deleted" => 0]);

        if (!empty($employee['office_id'])) {
            $queryString = $queryString->where(["NothiTypes.office_id" => $employee['office_id']]);
        }
        if (!empty($employee['office_unit_id'])) {
            $queryString = $queryString->where(["NothiTypes.office_unit_id" => $employee['office_unit_id']]);
        }
        if (!empty($doron)) {
            $queryString = $queryString->where(["NothiTypes.type_name like " => '%'.$doron.'%']);
        }
        if (!empty($page)) {
            $queryString = $queryString->page($page, $len);
        }

        return $queryString->order("NothiTypes.id ASC");
    }

    public function getTypes($id = 0, $employee = null)
    {
        $queryString = $this->find('list',
            ['keyField' => 'id', 'valueField' => 'type_name'])->where(["NothiTypes.is_deleted" => 0]);
        if (!empty($employee['office_id'])) {
            $queryString = $queryString->where(["NothiTypes.office_id" => $employee['office_id']]);
        }
        if (!empty($employee['office_unit_id'])) {
            $queryString = $queryString->where(["NothiTypes.office_unit_id" => $employee['office_unit_id']]);
        }
        if (!empty($id)) {
            $queryString = $queryString->where(['id' => $id]);
        }

        return $queryString->order("NothiTypes.type_name ASC")->hydrate(false)->toArray();
    }
}