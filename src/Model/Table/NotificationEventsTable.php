<?php
namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;

class NotificationEventsTable extends ProjapotiTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */

    protected $type = [
      1=>'Dak',
      2=>'Nothi'
    ];
    public function initialize(array $config)
    {
        $conn = ConnectionManager::get('projapotiDb');
        $this->connection($conn);
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
    }

    public function getType($eventId){
        $getType = $this->find()->select(['template_id'])->where(['id'=>$eventId])->first();

        return $this->type[$getType['template_id']];
    }

    public function getTypeDetail($eventId){
        $getType = $this->find()->where(['id'=>$eventId])->first();

        return $getType;
    }
}