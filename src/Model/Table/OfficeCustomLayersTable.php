<?php
namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;


class OfficeCustomLayersTable extends ProjapotiTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $conn = ConnectionManager::get('projapotiDb');
        $this->connection($conn);
        $this->primaryKey('id');
        $this->displayField('name');
        $this->addBehavior('Timestamp');
        $this->table('office_custom_layers');
    }
    public function getCustomLayerWiseOffices($custom_layer_id){
        $officeTbl = TableRegistry::get('Offices');
        $query = $officeTbl->find();
        if(!empty($custom_layer_id)){
           $query->where(['custom_layer_id' => $custom_layer_id]);
        }else if($custom_layer_id == 0){
            $query->where(['OR'=>['custom_layer_id' => 0,'custom_layer_id IS NULL']]);
        }
        return $query;
    }
    public function getCustomLayerList($keyField,$valueField,$condition =[]){
        return $this->find('list',['keyField' => $keyField,'valueField' => $valueField])->where($condition);
    }
}