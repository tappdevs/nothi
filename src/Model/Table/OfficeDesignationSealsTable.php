<?php
namespace App\Model\Table;

use App\Model\Entity\OfficeDesignationSeal;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

/**
 * OfficeDesignationSeals Model
 */
class OfficeDesignationSealsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('office_designation_seals');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->addBehavior('MobileTrack');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');
            
        $validator
            ->requirePresence('designation_name_bng', 'create')
            ->notEmpty('designation_name_bng');
            
        $validator
            ->requirePresence('designation_name_eng', 'create')
            ->notEmpty('designation_name_eng');
            
        $validator
            ->requirePresence('unit_name_bng', 'create')
            ->notEmpty('unit_name_bng');
            
        $validator
            ->requirePresence('unit_name_eng', 'create')
            ->notEmpty('unit_name_eng');
            
        $validator
            ->add('designation_seq', 'valid', ['rule' => 'numeric'])
            ->requirePresence('designation_seq', 'create')
            ->notEmpty('designation_seq');
            
        $validator
            ->add('designation_level', 'valid', ['rule' => 'numeric'])
            ->requirePresence('designation_level', 'create')
            ->notEmpty('designation_level');
            
        $validator
            ->add('created_by', 'valid', ['rule' => 'numeric'])
            ->requirePresence('created_by', 'create')
            ->notEmpty('created_by');
            
        $validator
            ->add('modified_by', 'valid', ['rule' => 'numeric'])
            ->requirePresence('modified_by', 'create')
            ->notEmpty('modified_by');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        return $rules;
    }

    public function getSeal($employee_office){
        $seal_designations = $this->getSealArray($employee_office);
        $seal_info = [];
        if(!empty($seal_designations)) {
            foreach ($seal_designations as $designation) {
                $designation_info = designationInfo($designation->office_unit_organogram_id);

                $seal_info[] = array(
                    'id' => $designation->id,
                    'employee_office_id' => $designation_info['employee_office_id'],
                    'designation_description' => $designation_info['designation_label'],
                    'designation_name_bng' => $designation->designation_name_bng,
                    'unit_name_bng' => $designation->unit_name_bng,
                    'office_unit_organogram_id' => $designation->office_unit_organogram_id,
                    'office_unit_id' => $designation->office_unit_id,
                    'employee_record_id' => $designation_info['officer_id'],
                    'name_bng' => $designation_info['officer_name'],
                );
            }
        }
        return $seal_info;
    }
    public function getSealArray($employee_office){
        $table_instance_unit = TableRegistry::get('OfficeUnitSeals');
        $seal_designations        = $this->find()->where(['office_id' => $employee_office['office_id'],
            'seal_owner_designation_id' => $employee_office['office_unit_organogram_id']])->order(['designation_level ASC, designation_seq ASC'])->toArray();
//'office_unit_organogram_id <>' => $employee_office['office_unit_organogram_id']
        if(empty($seal_designations)){
            $seal_designations = $table_instance_unit->find()->where(['office_id' => $employee_office['office_id'],
                'seal_owner_unit_id' => $employee_office['office_unit_id']])->order(['designation_level ASC, designation_seq ASC'])->toArray();
        }
        return $seal_designations;
    }

    public function getDesignationSealArray($employee_office){
        $seal_designations        = $this->find()->where(['office_id' => $employee_office['office_id'],
            'seal_owner_designation_id' => $employee_office['office_unit_organogram_id']])->order(['designation_level ASC, designation_seq ASC'])->toArray();

        return $seal_designations;
    }
    public function getEntry($owner_designation_id,$office_id = 0,$unit_id = 0 , $designation_id = 0,$list = 0){
        if($list){
            $query = $this->find('list',['keyField' => 'id','valueField' => 'office_unit_organogram_id']);
        }else{
            $query = $this->find();
        }

        if(!empty($owner_designation_id)){
            $query->where(['seal_owner_designation_id' => $owner_designation_id]);
        }else{
            return -1;
        }
        if(!empty($office_id)){
            $query->where(['office_id' => $office_id]);
        }
        if(!empty($unit_id)){
            $query->where(['office_unit_id' => $unit_id]);
        }
        if(!empty($designation_id)){
            if(is_array($designation_id)){
                $query->where(['office_unit_organogram_id IN' => $designation_id]);
            }else{
                $query->where(['office_unit_organogram_id' => $designation_id]);
            }

        }
        return $query;
    }
}
