<?php
namespace App\Model\Table;

use App\Model\Entity\DakType;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use App\Model\Table\ProjapotiTable;
use Cake\Validation\Validator;
use  Cake\Datasource\ConnectionManager;

/**
 * OfficeMinistry Model
 */
class OfficeMinistriesTable extends ProjapotiTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $conn = ConnectionManager::get('projapotiDb');
        $this->connection($conn);

        $this->table('office_ministries');
        $this->displayField('name_bng');
        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->requirePresence('id', 'update')
            ->notEmpty('id');

        $validator
            ->requirePresence('name_bng')
            ->notEmpty('name');

        $validator
            ->requirePresence('name_eng')
            ->notEmpty('name');

        $validator
            ->allowEmpty('modified_by');

        return $validator;
    }

    public function validationAdd($validator)
    {
        $validator
            ->notEmpty('office_type', "এই তথ্য ফাকা রাখা যাবে না")
            ->notEmpty('name_bng', "এই তথ্য ফাকা রাখা যাবে না")
            ->notEmpty('name_eng', "এই তথ্য ফাকা রাখা যাবে না")
            ->notEmpty('name_eng_short', "এই তথ্য ফাকা রাখা যাবে না")
//            
//            ->add('reference_code', 'notBlank', [
//                'rule' => 'notBlank',
//                'message' => __('You need to provide a Reference code'),
//            ])

//            ->add('reference_code', 'isUnique', [
//                'rule' => function($data, $provider){
//                    if($this->find()->where(['reference_code'=>$data,'active_status'=>1])->count()>0)return "Duplicate entry";                   
//                    return true;
//                },
//                'message' => __('You need to provide unique Reference code'),
//            ])

            ->add('name_bng', 'isUnique', [
                'rule' => function ($data, $provider) {
                    if ($this->find()->where(['name_bng' => $data, 'office_type' => $provider['data']['office_type'], 'active_status' => 1])->count() > 0) return "Duplicate entry";
                    return true;
                },
                'message' => __("এটি পূর্বে ব্যবহৃত হয়েছে"),
            ])
            ->add('name_eng', 'isUnique', [
                'rule' => function ($data, $provider) {
                    if ($this->find()->where(['name_eng' => $data, 'office_type' => $provider['data']['office_type'], 'active_status' => 1])->count() > 0) return "Duplicate entry";
                    return true;
                },
                'message' => __("এটি পূর্বে ব্যবহৃত হয়েছে"),
            ]);
        return $validator;
    }

    public function validationUpdate($validator)
    {
        $validator
            ->notEmpty('office_type', "এই তথ্য ফাকা রাখা যাবে না")
            ->notEmpty('name_bng', "এই তথ্য ফাকা রাখা যাবে না")
            ->notEmpty('name_eng', "এই তথ্য ফাকা রাখা যাবে না")
            ->notEmpty('name_eng_short', "এই তথ্য ফাকা রাখা যাবে না")
//            ->add('reference_code', 'notBlank', [
//                'rule' => 'notBlank',
//                'message' => __('You need to provide a Reference code'),
//            ])
//            
//            ->add('reference_code', 'isUnique', [
//                'rule' => function($data, $provider){
//                    if($this->find()->where(['reference_code'=>$data,'active_status'=>1,'id !='=>$provider['data']['id']])->count()>0)return "Duplicate entry";                   
//                    return true;
//                },
//                'message' => __('You need to provide unique Reference code'),
//            ])

            ->add('name_bng', 'isUnique', [
                'rule' => function ($data, $provider) {
                    if ($this->find()->where(['name_bng' => $data, 'office_type' => $provider['data']['office_type'], 'active_status' => 1, 'id !=' => $provider['data']['id']])->count() > 0) return "Duplicate entry";
                    return true;
                },
                'message' => __("এটি পূর্বে ব্যবহৃত হয়েছে"),
            ])
            ->add('name_eng', 'isUnique', [
                'rule' => function ($data, $provider) {
                    if ($this->find()->where(['name_eng' => $data, 'office_type' => $provider['data']['office_type'], 'active_status' => 1, 'id !=' => $provider['data']['id']])->count() > 0) return "Duplicate entry";
                    return true;
                },
                'message' => __("এটি পূর্বে ব্যবহৃত হয়েছে"),
            ]);
        return $validator;
    }


    public function getFullTree($ministry_id)
    {
        return $this->find()
            ->select([
                "OfficeMinistries.id",
                "OfficeMinistries.name_bng",
                "OfficeLayers.id",
                "OfficeLayers.office_ministry_id",
                "OfficeLayers.parent_layer_id",
                "OfficeLayers.layer_name_bng",
                "OfficeOrigins.id",
                "OfficeOrigins.office_ministry_id",
                "OfficeOrigins.office_layer_id",
                "OfficeOrigins.parent_office_id",
                "OfficeOrigins.office_name_bng",
                "OfficeOriginUnits.id",
                "OfficeOriginUnits.office_ministry_id",
                "OfficeOriginUnits.office_layer_id",
                "OfficeOriginUnits.office_origin_id",
                "OfficeOriginUnits.parent_unit_id",
                "OfficeOriginUnits.unit_name_bng",
                "OfficeOriginUnitOrganograms.id",
                "OfficeOriginUnitOrganograms.office_origin_unit_id",
                "OfficeOriginUnitOrganograms.superior_designation_id",
                "OfficeOriginUnitOrganograms.designation_bng",
            ])
            ->where([
                "OfficeMinistries.id" => $ministry_id,
                "OfficeMinistries.active_status" => 1
            ])
            ->join([
                "OfficeLayers" => [
                    "table" => "office_layers",
                    "conditions" => "OfficeLayers.office_ministry_id= OfficeMinistries.id AND OfficeLayers.active_status=1",
                    "type" => "LEFT"
                ],
                "OfficeOrigins" => [
                    "table" => "office_origins",
                    "conditions" => "OfficeOrigins.office_ministry_id = OfficeMinistries.id AND OfficeOrigins.office_layer_id=OfficeLayers.id AND OfficeOrigins.active_status=1",
                    "type" => "LEFT"
                ],
                "OfficeOriginUnits" => [
                    "table" => "office_origin_units",
                    "conditions" => "OfficeOriginUnits.office_ministry_id = OfficeMinistries.id AND OfficeOriginUnits.office_layer_id=OfficeLayers.id AND OfficeOriginUnits.office_origin_id = OfficeOrigins.id AND OfficeOriginUnits.active_status =1",
                    "type" => "LEFT"
                ],
                "OfficeOriginUnitOrganograms" => [
                    "table" => "office_origin_unit_organograms",
                    "conditions" => "OfficeOriginUnitOrganograms.office_origin_unit_id = OfficeOriginUnits.id AND OfficeOriginUnitOrganograms.status =1",
                    "type" => "LEFT"
                ]
            ])->order([
                "OfficeLayers.id ASC",
                "OfficeOrigins.id ASC",
                "OfficeOriginUnits.id ASC",
                "OfficeOriginUnitOrganograms.id ASC"
            ])
            ->hydrate(false)
            ->toArray();
    }
    
    
    
    public function getAll($conditions = '', $select=''){
        
        $queryString = $this->find();
        
        if(!empty($select)){
            $queryString = $queryString->select($select);
        }
        
        if(!empty($conditions)){
            $queryString = $queryString->where($conditions);
        }
                
        return $queryString->where(['active_status' => 1]);
    }
    
    
    public function getMinistryByOffice($office_id) {
        
        return $query = $this->find()->select()->join([
            'Offices'=>[
                'table'=>'offices',
                'type'=>'inner',
                'conditions'=>'Offices.office_ministry_id = OfficeMinistries.id'
            ]
        ])->where(['Offices.id'=>$office_id])->first();
    }
    public function getBanglaName($id)
    {
        return $this->find()->select(['name_bng'])->where(['id'=>$id])->first();
    }
}
