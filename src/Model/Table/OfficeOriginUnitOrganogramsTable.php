<?php

namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use  Cake\Datasource\ConnectionManager;

class OfficeOriginUnitOrganogramsTable extends ProjapotiTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $conn = ConnectionManager::get('projapotiDb');
        $this->connection($conn);

        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->displayField('designation_bng');
    }

    public function getOriginUnitOrgByOriginUnitId($origin_unit_id)
    {
        return $this->find()->where(['office_origin_unit_id' => $origin_unit_id, 'status' => 1])->toArray();
    }

    public function getOriginUnitInfo($origin_unit_id = 0)
    {
        if (!empty($origin_unit_id)) {
            return $this->find()->where(['office_origin_unit_id' => $origin_unit_id, 'status' => 1])->toArray();
        }
        return 0;
    }
}
