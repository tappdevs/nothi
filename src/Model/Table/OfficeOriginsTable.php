<?php
namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use  Cake\Datasource\ConnectionManager;

class OfficeOriginsTable extends ProjapotiTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $conn = ConnectionManager::get('projapotiDb');
        $this->connection($conn);

        $this->primaryKey('id');
        
        $this->displayField('office_name_bng');
        $this->addBehavior('Timestamp');
    }

    public function validationAdd($validator)
    {
        $validator
            ->notEmpty('office_ministry_id', "এই তথ্য ফাকা রাখা যাবে না")
            ->notEmpty('office_layer_id', "এই তথ্য ফাকা রাখা যাবে না")
            ->notEmpty('office_name_eng', "এই তথ্য ফাকা রাখা যাবে না")
            ->notEmpty('office_name_bng', "এই তথ্য ফাকা রাখা যাবে না")
            ->add('office_name_eng', 'isUnique', [
                'rule' => function ($data, $provider) {
                    if ($this->find()->where(['office_name_eng' => $data, 'office_layer_id' => $provider['data']['office_layer_id'], 'active_status' => 1])->count() > 0) return "Duplicate entry";
                    return true;
                },
                'message' => __("এটি পূর্বে ব্যবহৃত হয়েছে"),
            ])
            ->add('office_name_bng', 'isUnique', [
                'rule' => function ($data, $provider) {
                    if ($this->find()->where(['office_name_bng' => $data, 'office_layer_id' => $provider['data']['office_layer_id'], 'active_status' => 1])->count() > 0) return "Duplicate entry";
                    return true;
                },
                'message' => __("এটি পূর্বে ব্যবহৃত হয়েছে"),
            ]);
        return $validator;
    }

    public function validationUpdate($validator)
    {
        $validator
            ->notEmpty('office_ministry_id', "এই তথ্য ফাকা রাখা যাবে না")
            ->notEmpty('office_layer_id', "এই তথ্য ফাকা রাখা যাবে না")
            ->notEmpty('office_name_eng', "এই তথ্য ফাকা রাখা যাবে না")
            ->notEmpty('office_name_bng', "এই তথ্য ফাকা রাখা যাবে না")
            ->add('office_name_eng', 'isUnique', [
                'rule' => function ($data, $provider) {
                    if ($this->find()->where(['office_name_eng' => $data, 'office_layer_id' => $provider['data']['office_layer_id'], 'id !=' => $provider['data']['id'], 'active_status' => 1])->count() > 0) return "Duplicate entry";
                    return true;
                },
                'message' => __("এটি পূর্বে ব্যবহৃত হয়েছে"),
            ])
            ->add('office_name_bng', 'isUnique', [
                'rule' => function ($data, $provider) {
                    if ($this->find()->where(['office_name_bng' => $data, 'office_layer_id' => $provider['data']['office_layer_id'], 'id !=' => $provider['data']['id'], 'active_status' => 1])->count() > 0) return "Duplicate entry";
                    return true;
                },
                'message' => __("এটি পূর্বে ব্যবহৃত হয়েছে"),
            ]);
        return $validator;
    }

    
    public function getAll($conditions = '', $select=''){
        
        $queryString = $this->find();
        
        if(!empty($select)){
            $queryString = $queryString->select($select);
        }
        
        if(!empty($conditions)){
            $queryString = $queryString->where($conditions);
        }
                
        return $queryString;
    }
    
    public function getOriginsByLayer($layer_id = 0){
       
        return $this->find('list')->where(['office_layer_id' => $layer_id, 'active_status' => 1])->order(['office_level'])->toArray();
    
    }
     public function getBanglaName($id)
    {
        return $this->find()->select(['office_name_bng'])->where(['id'=>$id])->first();
    }

}