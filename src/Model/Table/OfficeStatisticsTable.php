<?php
namespace App\Model\Table;

use App\Model\Entity\OfficeStatistic;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * OfficeStatistics Model
 */
class OfficeStatisticsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('office_statistics');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->belongsTo('Offices', [
            'foreignKey' => 'office_id'
        ]);
        $this->belongsTo('OfficeMinistries', [
            'foreignKey' => 'office_ministry_id'
        ]);
        $this->belongsTo('OfficeLayers', [
            'foreignKey' => 'office_layer_id'
        ]);
        $this->belongsTo('GeoDivisions', [
            'foreignKey' => 'geo_division_id'
        ]);
        $this->belongsTo('GeoDistricts', [
            'foreignKey' => 'geo_district_id'
        ]);
        $this->belongsTo('GeoUpazilas', [
            'foreignKey' => 'geo_upazila_id'
        ]);
        $this->belongsTo('GeoUnions', [
            'foreignKey' => 'geo_union_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');
            
        $validator
            ->requirePresence('date', 'create')
            ->notEmpty('date');
            
        $validator
            ->allowEmpty('office_name_bng');
            
        $validator
            ->add('nothi_user', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('nothi_user');
            
        $validator
            ->add('nothi_user_female', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('nothi_user_female');
            
        $validator
            ->add('nothi_user_male', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('nothi_user_male');
            
        $validator
            ->add('active_nothi_user', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('active_nothi_user');
            
        $validator
            ->add('active_nothi_user_female', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('active_nothi_user_female');
            
        $validator
            ->add('active_nothi_user_male', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('active_nothi_user_male');
            
        $validator
            ->add('office_is_active', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('office_is_active');
            
        $validator
            ->add('nagorik_service', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('nagorik_service');
            
        $validator
            ->add('daptorikh_service', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('daptorikh_service');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */


    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'NothiReportsDb';
    }
}
