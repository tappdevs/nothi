<?php
namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;

class OfficeUnitCategoriesTable extends ProjapotiTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $conn = ConnectionManager::get('projapotiDb');
        $this->connection($conn);
        $this->primaryKey('id');
        $this->displayField('category_name_bng');
        $this->addBehavior('Timestamp');
    }

    /**
     * Category list for select box
     */
    public function getOfficeUnitCategoryList()
    {
        $data_items = $this->find('list');
        return array_combine($data_items, $data_items);
    }

}