<?php
namespace App\Model\Table;

use App\Model\Entity\OfficeUnitSeal;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * OfficeUnitSeals Model
 */
class OfficeUnitSealsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('office_unit_seals');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');
            
        $validator
            ->requirePresence('designation_name_bng', 'create')
            ->notEmpty('designation_name_bng');
            
        $validator
            ->requirePresence('designation_name_eng', 'create')
            ->notEmpty('designation_name_eng');
            
        $validator
            ->requirePresence('unit_name_bng', 'create')
            ->notEmpty('unit_name_bng');
            
        $validator
            ->requirePresence('unit_name_eng', 'create')
            ->notEmpty('unit_name_eng');
            
        $validator
            ->add('designation_seq', 'valid', ['rule' => 'numeric'])
            ->requirePresence('designation_seq', 'create')
            ->notEmpty('designation_seq');
            
        $validator
            ->add('designation_level', 'valid', ['rule' => 'numeric'])
            ->requirePresence('designation_level', 'create')
            ->notEmpty('designation_level');
            
        $validator
            ->add('created_by', 'valid', ['rule' => 'numeric'])
            ->requirePresence('created_by', 'create')
            ->notEmpty('created_by');
            
        $validator
            ->add('modified_by', 'valid', ['rule' => 'numeric'])
            ->requirePresence('modified_by', 'create')
            ->notEmpty('modified_by');

        return $validator;
    }
}
