<?php
namespace App\Model\Table;

use Cake\Datasource\EntityInterface;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;
/**
 * Office Model
 */
class OfficesTable extends ProjapotiTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $conn = ConnectionManager::get('projapotiDb');
        $this->connection($conn);

        $this->displayField('office_name_bng');
        $this->addBehavior('Timestamp');

        $this->hasOne('OfficeDomains', [
            'foreignKey' => false,
            'type' => 'leftJoin',
            'conditions' => 'Offices.id = OfficeDomains.office_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->requirePresence('id', 'update')
            ->notEmpty('id')
            ->requirePresence('office_name_bng')
            ->notEmpty('name')
            ->notEmpty('digital_nothi_code')
            ->requirePresence('office_name_eng')
            ->requirePresence('digital_nothi_code')
//            ->add('digital_nothi_code', [
//                'length' => [
//                    'rule' => ['minLength', 10],
//                    'message' => 'ডিজিটাল নথি কোড ৮ ডিজিটের হতে হবে',
//                ]
//            ])
            ->allowEmpty('modified_by');

        return $validator;
    }

    public function afterSave( $event, $entity, $options){
        $employee_offices_table = TableRegistry::get('EmployeeOffices');
        $employee_offices_table->updateAll(['office_name_en'=>$entity['office_name_eng'],'office_name_bn'=>$entity['office_name_bng']],['office_id'=>$entity['id'],'status'=>1]);
    }

    public function getOfficesByMinistry($ministry_id)
    {
        return $this->find('list')->where(['office_ministry_id' => $ministry_id, 'active_status' => 1])->toArray();
    }

    public function getOfficesByOrigin($origin_id)
    {
        return $this->find('list')->where(['office_origin_id' => $origin_id, 'active_status' => 1])->toArray();
    }

    public function getOfficesByid($id)
    {
        return $this->find('list')->where(['id' => $id, 'active_status' => 1])->toArray();
    }

    public function getMinistryInfo($ministy_id)
    {
        if (!empty($ministy_id) && $this->find()->where(['office_ministry_id' => $ministy_id, 'active_status' => 1])->count()) {
            return 1;
        }

        return 0;
    }
    
     
    public function getAll($conditions = '', $select=''){
        
        $queryString = $this->find();
        
        if(!empty($select)){
            $queryString = $queryString->select($select);
        }
        
        if(!empty($conditions)){
            $queryString = $queryString->where($conditions);
        }
                
        return $queryString;
    }
    
    public function getOfficeFullInfo($officeid)
    {
        $conn = ConnectionManager::get('projapotiDb');
        $stmt = $conn->query ( 'SELECT  group_concat(@id :=
        (
        SELECT  parent_office_id
        FROM    offices
        WHERE   id = @id AND parent_office_id > 0
        )) AS offices
FROM    (
        SELECT  @id := ' . $officeid . '
        ) vars
STRAIGHT_JOIN
        offices
WHERE   @id IS NOT NULL' );

        $stmt->execute ();

        return $stmt->fetch();
    }
    
    public function getChildOffices($office_id=0)
    {
        return $this->find()->where(['parent_office_id'=>$office_id,'active_status' => 1])->toArray();
    }

    public function getBanglaName($id)
    {
        return $this->find()->select(['office_name_bng'])->where(['id'=>$id])->first();
    }
    public function getEnglishName($id)
    {
        return $this->find()->select(['office_name_eng'])->where(['id'=>$id])->first();
    }
    public function getOfficeName($id)
    {
        return $this->find()->select(['office_name_eng','office_name_bng'])->where(['id'=>$id])->first();
    }

    public function getIdandNameofChildOffices($office_id=0)
    {
        return $this->find('list')->where(['parent_office_id'=>$office_id,'active_status' => 1])->toArray();
    }
    public function getOfficeListByDomain($domain_name = '')
    {
        return $this->find('list')->where(['office_name_bng LIKE'=>"%".$domain_name."%",'active_status' => 1])->orWhere(['office_name_eng LIKE'=>"%".$domain_name."%",'active_status' => 1])->order(['parent_office_id asc asc'])->toArray();
    }
     public function getOfficeAddress($id)
    {
        return $this->find()->select(['office_address'])->where(['id'=>$id])->first();
    }
    public function getCustomLayerInfo($custom_layer_id)
    {
        return $this->find()->where(['custom_layer_id'=>$custom_layer_id]);
    }

    public function getOfficesByDistrict($request)
    {
        $district_id = $request['district_id'];
        $active_status_id = $request['active_status_id'];
        $mapping_status_id = $request['mapping_status_id'];
        $office_name = $request['office_name']; 

        // $officeDomainTable = TableRegistry::get('OfficeDomains');
        $officeList = $this->find()->contain(['OfficeDomains']);

        if(!empty($district_id))
        {
            $officeList->where(['geo_district_id'=>$district_id]);
        }
        if(!empty($active_status_id) && $active_status_id==1 )
        {
            $officeList->where(['active_status'=>$active_status_id]);
        }elseif(!empty($active_status_id) && $active_status_id==2 )
        {
            $officeList->where(['active_status'=>0]);
        }
        if(!empty($office_name))
        {
            $officeList->where(['office_name_bng like '=>'%'.$office_name.'%']);
        }
        if(!empty($mapping_status_id) && $mapping_status_id == 1)
        {
            $officeList->where(['OfficeDomains.office_id > '=> 0]);
        }
        if(!empty($mapping_status_id) && $mapping_status_id == 2)
        {
            $officeList->where([' OfficeDomains.office_id is '=> null ]);
        }
        
        return $officeList;
    }

    public function getUserCountByDistrictBbsCode($district_bbs_codes){
        $geo_district_table = TableRegistry::get("GeoDistricts");
        $geo_upazilas_table = TableRegistry::get("GeoUpazilas");
        $result = array();
        foreach ($district_bbs_codes as $district_bbs_code) {

            $district_list = $geo_district_table->find('list', ['keyField' => 'id', 'valueField' => 'id'])->where(['bbs_code IN' => $district_bbs_code])->toArray();
            if (empty($district_list)) {
                $result[] =  ['district_bbs_code' => $district_bbs_code,'message' => 'District not found.'];
                continue;
            }

            $upazilla_bbs_codes = $geo_upazilas_table->find('list', ['keyField' => 'bbs_code', 'valueField' => 'bbs_code'])->where(['geo_district_id IN' => $district_list])->toArray();

            if (empty($upazilla_bbs_codes)) {
                $result[] = ['district_bbs_code' => $district_bbs_code,'message' => 'Upazila not found.'];
                continue;
            }
            $data = $this->getUserCountByUpazilaBbsCode($upazilla_bbs_codes);
            $result[] = ['district_bbs_code' => $district_bbs_code,'data'=>$data];
        }
        return $result;
    }

    public function getUserCountByUpazilaBbsCode($upazilla_bbs_codes){
        $geo_upazilas_table = TableRegistry::get("GeoUpazilas");
        $employee_offices_table = TableRegistry::get("EmployeeOffices");
        $result = array();

        foreach ($upazilla_bbs_codes as $upazilla_bbs_code) {

            $geo_upazila_id = $geo_upazilas_table->find('list', ['keyField' => 'id', 'valueField' => 'id'])->where(['bbs_code' => $upazilla_bbs_code])->toArray();
            if (empty($geo_upazila_id)) {
                $result[] = ['upazila_bbs_code' => $upazilla_bbs_code, 'message' => 'Upazila not found.'];
                continue;
            }

            $offices = $this->find('list', ['keyField' => 'id', 'valueField' => 'id'])->where(['geo_upazila_id IN' => $geo_upazila_id, 'active_status' => 1])->toArray();
            if (empty($offices)) {
                $result[] = ['upazila_bbs_code' => $upazilla_bbs_code, 'message' => 'No office found.'];
                continue;
            }

            $employee_count = $employee_offices_table->find()->where(['office_id IN' => $offices, 'status' => 1])->distinct(['employee_record_id'])->count();

            if (!empty($employee_count)) {
                $result[] = ['upazila_bbs_code' => $upazilla_bbs_code, 'data' => ['count' => $employee_count]];
            } else {
                $result[] = ['upazila_bbs_code' => $upazilla_bbs_code, 'message' => 'No active user found'];
            }
        }
        return $result;
    }

    public function getAllActiveOffices(){
        return  $this->find()
            ->select([
                'office_id'=>'Offices.id',
                'Offices.office_ministry_id',
                'Offices.office_layer_id',
                'Offices.office_name_bng',
                'Offices.geo_division_id',
                'Offices.geo_district_id',
                'Offices.geo_upazila_id',
                'Offices.geo_union_id'
            ])
            ->where(['active_status'=>1])->hydrate(false)->toArray();
    }

    public function getOfficeWiseNothiUserCount($offices,$month,$year){
        if(!empty($offices)) {
            $employee_offices_table = TableRegistry::get('EmployeeOffices');
            $user_login_history_table = TableRegistry::get('UserLoginHistory');
            $office_statistic_table = TableRegistry::get('OfficeStatistics');
            $employee_records_table = TableRegistry::get('EmployeeRecords');

            try{
                foreach ($offices as &$office) {
                    $users = $employee_offices_table->find()->contain(['EmployeeRecords'])->where(['office_id' => $office['office_id'], 'EmployeeOffices.status' => 1])->distinct(['employee_record_id'])->toArray();
                    if(!empty($users)){
                        $office_employee_record_ids =[];
                        $female= 0 ;
                        foreach ($users as $user){
                            array_push($office_employee_record_ids,$user['employee_record']['id']);
                            if($user['employee_record']['gender'] == 2){
                                $female++;
                            }
                        }
                        $office['nothi_user'] = count($users);
                        $office['nothi_user_female'] = $female;
                        $office['nothi_user_male'] = count($users) - $female;

                    } else {
                        $office['nothi_user'] = 0;
                        $office['nothi_user_female'] = 0;
                        $office['nothi_user_male'] = 0;
                    }

                    $office['date'] = $year.'-'.$month;
                    $active_users = $user_login_history_table->find('list',['keyField'=>'employee_record_id','valueField'=>'employee_record_id'])->where(['office_id'=> $office['office_id'],'MONTH(UserLoginHistory.created)'=>$month,'YEAR(UserLoginHistory.created)'=>$year,])->toArray();
                    if(!empty($active_users)){

                        $real_active_users = array_intersect($active_users,$office_employee_record_ids);
                        $active_female = $employee_records_table->find()->where(['id IN'=>$real_active_users,'gender'=>2])->count();
                        $active_male = count($real_active_users) - $active_female;

                        $office['active_nothi_user'] = $active_female + $active_male;
                        $office['active_nothi_user_female'] = $active_female;
                        $office['active_nothi_user_male'] =$active_male;
                        $office['office_is_active'] = (($active_female + $active_male) > 0)?1:0;
                    } else {
                        $office['active_nothi_user'] = 0;
                        $office['active_nothi_user_female'] = 0;
                        $office['active_nothi_user_male'] = 0;
                        $office['office_is_active'] = 0;
                    }
                    $office_statistic_table->deleteAll(['date'=>$office['date'],'office_id'=>$office['office_id']]);
                    $office_statistic_data = $office_statistic_table->newEntity($office);
                    $office_statistic_table->save($office_statistic_data);
                }
                $count = $office_statistic_table->find()->where(['date'=>$year.'-'.$month])->count();
                return ["status"=>"success","count"=>$count];
            } catch (\Exception $ex) {
                return ["status"=>"error", "message"=> $ex->getMessage()];
            }
        } else {
            return ["status"=>"error", "message"=> "No office found"];
        }
    }
}
