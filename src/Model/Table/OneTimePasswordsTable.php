<?php
namespace App\Model\Table;

use Cake\Datasource\ConnectionManager;

class OneTimePasswordsTable extends ProjapotiTable
{

    public function initialize(array $config)
    {
		$conn = ConnectionManager::get('NothiAccessDb');

		$this->connection($conn);
		$this->primaryKey('id');
		$this->displayField('otp');
		$this->addBehavior('Timestamp');
    }
}