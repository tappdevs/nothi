<?php

namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

class OtherOfficeNothiMasterMovementsTable extends ProjapotiTable {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->table('other_office_nothi_master_movements');
    }
    public function deletePreviousTransactions($nothi_master_id,$nothi_part_id,$nothi_office,$employee_office){
        $conditions = [];
        if(!empty($nothi_master_id)){
            $conditions['nothi_master_id'] = $nothi_master_id;
        }
        if(!empty($nothi_part_id)){
            $conditions['nothi_part_no']  = $nothi_part_id;
        }
        if(!empty($nothi_office)){
            $conditions['nothi_office']  = $nothi_office;
        }
        if(!empty($employee_office['office_id'])){
            $conditions['from_office_id']  = $employee_office['office_id'];
        }
        if(!empty($employee_office['office_unit_id'])){
            $conditions['from_office_unit_id']  =$employee_office['office_unit_id'] ;
        }
        if(!empty($employee_office['office_unit_organogram_id'])){
            $conditions['from_officer_designation_id']  =$employee_office['office_unit_organogram_id'] ;
        }
        if(!empty($conditions)){
            return $this->deleteAll($conditions);
        }
    }

}
