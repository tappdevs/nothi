<?php

namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\Datasource\ConnectionManager;
use Cake\Controller\Component;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

class PerformanceDesignationsTable extends ProjapotiTable
{

    public function initialize(array $config)
    {
        //parent::initialize($config);
        $conn = ConnectionManager::get('NothiReportsDb');
        $this->connection($conn);
        $this->table('performance_designations');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
    }

    public function checkUpdate($designation_id = 0, $date = '')
    {
        if (!empty($date) && !empty($designation_id)) {
            return $this->find()->where(['record_date' => $date, 'designation_id' => $designation_id])->count();
        }
    }
    public function checkNothiUpdate($designation_id = 0, $date = '')
    {
        if (!empty($date) && !empty($designation_id)) {
            return $this->find()->where(['record_date' => $date, 'designation_id' => $designation_id, 'updated' => 0 ])->count();
        }
    }
    /**
     *
     * @param int $office_id
     * @param DateTime $time
     * @return object  CollectionBetweenTwoDates
     */
    public function getDesignationData($designation_id = 0, $time = [])
    {
        if (!empty($designation_id)) {
            $query = $this->find()->where(['designation_id IN' => $designation_id]);
            if (!empty($time)) {
                 if (!empty($time[0])) {
                    $query = $query->where(['record_date >=' => $time[0]]);
                }
                if (!empty($time[1])) {
                    $query = $query->where(['record_date <=' => $time[1]]);
                }
            }
            return $query;
        }
        return;
    }
    public function getUnitData($unit_id = 0, $time = [])
    {
        if (!empty($unit_id)) {
            $query = $this->find()->where(['unit_id IN' => $unit_id]);
            if (!empty($time)) {
                if (!empty($time[0])) {
                    $query = $query->where(['record_date >=' => $time[0]]);
                }
                if (!empty($time[1])) {
                    $query = $query->where(['record_date <=' => $time[1]]);
                }
            }
            return $query;
        }
        return;
    }
    public function getOfficeData($office_id = 0, $time = [])
    {
        if (!empty($office_id)) {
            $query = $this->find()->where(['office_id IN' => $office_id]);
            if (!empty($time)) {
                 if (!empty($time[0])) {
                    $query = $query->where(['record_date >=' => $time[0]]);
                }
                if (!empty($time[1])) {
                    $query = $query->where(['record_date <=' => $time[1]]);
                }
            }
            return $query;
        }
        return;
    }
    public function getLastUpdateTime($designation_id = 0){
        $query =  $this->find()->select(['record_date'])->order(['record_date desc']);
        if(!empty($designation_id)){
            $query = $query->where(['designation_id' => $designation_id]);
        }
    return$query->first();
    }
      public function getLastOnisponno($designation_id = 0,$date = ''){
        $query =  $this->find()->select(['onisponnodak' , 'onisponnonote']);
        if(!empty($designation_id)){
            $query = $query->where(['designation_id' => $designation_id]);
        }
        if(!empty( $date[0])){
            $query = $query->where(['record_date >=' => $date[0]]);
        }
        if(!empty( $date[1])){
            $query = $query->where(['record_date <=' => $date[1]]);
        }
    return $query->order(['record_date desc'])->first();
    }
    public function deleteDataofOffices($designation_id = 0, $date =''){
        if(!empty($designation_id)){
            $condition = [
                'designation_id' => $designation_id,
            ];
              if(!empty($date)){
                     $condition = [
                'designation_id' => $designation_id,
                'updated' => $date
            ];
            }
            $query = $this->deleteAll([ $condition ]);
            return $query;
        }
       return 0;
    }
    public function getAllDesignationByOfficeOrUnitID($office_id = 0, $unit_id = 0, $record_date = [],$list = 0,$select = [])
    {
            if($list){
                $query = $this->find('list',['keyField' => 'designation_id' , 'valueField' => 'designation_name']);
            }
            else {
                $query = $this->find();
            }
            if(!empty($select)){
                $query = $query->select($select);
            }
        if (!empty($office_id)) {
            $query = $query->where(['office_id' => $office_id]);
        }
        if (!empty($unit_id)) {
            $query = $query->where(['unit_id' => $unit_id]);
        }
        if (!empty($record_date[0])) {
            $query = $query->where(['record_date >=' => $record_date[0]]);
        }
        if (!empty($record_date[1])) {
            $query = $query->where(['record_date <=' => $record_date[1]]);
        }
        return $query->distinct(['designation_id'])->toArray();
    }
    public function getAllDesignationByOfficeOrUnitIDWithDesignationSort($office_id = 0, $unit_id = 0, $record_date = [],$list = 0,$select = [])
    {
        $sort_query = TableRegistry::get('EmployeeOffices')->find('list',['keyField' => 'id' , 'valueField' => 'office_unit_organogram_id']);
        if(!empty($office_id)){
            $sort_query->where(['office_id' => $office_id]);
        }
        if(!empty($unit_id)){
            $sort_query->where(['office_unit_id' => $unit_id]);
        }
        $sorted_list_of_designation_ids = $sort_query->order(['designation_level ASC', 'designation_sequence ASC'])->where(['status' => 1])->toArray();
//        pr($sorted_list_of_designation_ids);die;
        if($list){
            $query = $this->find('list',['keyField' => 'designation_id' , 'valueField' => 'designation_name']);
        }
        else {
            $query = $this->find();
        }
        if(!empty($select)){
            $query = $query->select($select);
        }
        if (!empty($office_id)) {
            $query = $query->where(['office_id' => $office_id]);
        }
        if (!empty($unit_id)) {
            $query = $query->where(['unit_id' => $unit_id]);
        }
        if (!empty($record_date[0])) {
            $query = $query->where(['record_date >=' => $record_date[0]]);
        }
        if (!empty($record_date[1])) {
            $query = $query->where(['record_date <=' => $record_date[1]]);
        }
        $sorted_designations_string = implode(',',$sorted_list_of_designation_ids);
        return $query->distinct(['designation_id'])->order('IF(FIELD(designation_id,'.$sorted_designations_string.')=0,1,0),FIELD(designation_id,'.$sorted_designations_string.')')->toArray();
    }
}