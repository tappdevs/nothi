<?php

namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\Datasource\ConnectionManager;
use Cake\Controller\Component;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

class PerformanceUnitsTable extends ProjapotiTable
{

    public function initialize(array $config)
    {
        //parent::initialize($config);
        $conn = ConnectionManager::get('NothiReportsDb');
        $this->connection($conn);
        $this->table('performance_units');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
    }

    public function checkUpdate($unit_id = 0, $date = '')
    {
        if (!empty($date) && !empty($unit_id)) {
            return $this->find()->where(['record_date' => $date, 'unit_id' => $unit_id])->count();
        }
    }
    public function checkNothiUpdate($unit_id = 0, $date = '')
    {
        if (!empty($date) && !empty($unit_id)) {
            return $this->find()->where(['record_date' => $date, 'unit_id' => $unit_id,'updated' => 0 ])->count();
        }
    }
    /**
     *
     * @param int $office_id
     * @param DateTime $time
     * @return object  CollectionBetweenTwoDates
     */
    public function getUnitData($unit_id = 0, $time = [])
    {
        if (!empty($unit_id)) {
            $query = $this->find()->where(['unit_id' => $unit_id]);
            if (!empty($time)) {
              if (!empty($time[0])) {
                    $query = $query->where(['record_date >=' => $time[0]]);
                }
                 if (!empty($time[1])) {
                    $query = $query->where(['record_date <=' => $time[1]]);
                }
            }
            return $query;
        }
        return;
    }
    public function getLastUpdateTime($unit_id = 0){
        $query =  $this->find()->select(['record_date'])->order(['record_date desc']);
        if(!empty($unit_id)){
            $query = $query->where(['unit_id' => $unit_id]);
        }
    return $query->first();
    }
    public function getLastOnisponno($unit_id = 0,$date = ''){
        $query =  $this->find()->select(['onisponnodak' , 'onisponnonote']);
        if(!empty($unit_id)){
            $query = $query->where(['unit_id' => $unit_id]);
        }
        if(!empty( $date[0])){
            $query = $query->where(['record_date >=' => $date[0]]);
        }
        if(!empty( $date[1])){
            $query = $query->where(['record_date <=' => $date[1]]);
        }
    return $query->order(['record_date desc'])->first();
    }
    public function deleteDataofOffices($unit_id = 0, $date =''){
        if(!empty($unit_id)){
            $condition = [
                'unit_id' => $unit_id,
            ];
              if(!empty($date)){
                     $condition = [
                'unit_id' => $unit_id,
                'updated' => $date
            ];
            }
            $query = $this->deleteAll([ $condition ]);
            return $query;
        }
       return 0;
    }
     public function getAllUnitData($unit_id = [], $time = [])
    {
        if (!empty($unit_id)) {
            $query = $this->find()->where(['unit_id IN' => $unit_id]);
            if (!empty($time)) {
                if (!empty($time[0])) {
                    $query = $query->where(['record_date >=' => $time[0]]);
                }
                if (!empty($time[1])) {
                    $query = $query->where(['record_date <=' => $time[1]]);
                }
            }
            return $query;
        }
        return;
    }
      public function getOfficeData($office_id = 0, $time = [])
    {
        if (!empty($office_id)) {
            $query = $this->find()->where(['office_id IN' => $office_id]);
            if (!empty($time)) {
                 if (!empty($time[0])) {
                    $query = $query->where(['record_date >=' => $time[0]]);
                }
                if (!empty($time[1])) {
                    $query = $query->where(['record_date <=' => $time[1]]);
                }
            }
            return $query;
        }
        return;
    }
}