<?php

namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

class PotroFlagsTable extends ProjapotiTable
{

    public function initialize(array $config)
    {
        $this->primaryKey('id');
    }

    public function getFlag($condition)
    {
        return $this->find()->where($condition);
    }
}