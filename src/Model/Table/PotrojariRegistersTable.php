<?php
namespace App\Model\Table;

use App\Model\Entity\PotrojariReceiver;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

/**
 * PotrojariReceiver Model
 */
class PotrojariRegistersTable extends ProjapotiTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('potrojari_registers');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->belongsTo('Potrojari', [
            'foreignKey' => 'potrojari_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('NothiMasters', [
            'foreignKey' => 'nothi_master_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('NothiNotes', [
            'foreignKey' => 'nothi_notes_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('NothiPotros', [
            'foreignKey' => 'nothi_potro_id',
            'joinType' => 'INNER'
        ]);
       
        $this->belongsTo('Offices', [
            'foreignKey' => 'office_id',
            'joinType' => 'INNER'
        ]);
       
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */


    public function SentStatus ($potrojari_id, $conditions = [], $status = -1){

        $query = $this->find()->where(['potrojari_id'=>$potrojari_id]);

        if($status!=-1){
            $query->where(['is_sent'=>$status]);
        }

        if(!empty($conditions)){
            $query->where($conditions);
        }

        return $query;
    }
    public function allPotrojariIDArray($potrojari_ids = [])
    {
        return $this->find('list',['keyField' =>'potrojari_id','valueField'=>'potrojari_id'])->where(['receiving_officer_designation_id >' => 0, 'potrojari_id IN' => $potrojari_ids])->toArray();
    }

    public function saveDataPotrojariRegisters($potrojari_info,$request_data)
    {
        $dak_datas=[];
        $nothi_potro_attachment_id = $potrojari_info["nothi_potro_id"];
        $dak_received_no =  '';
        if($nothi_potro_attachment_id>0)
        {
            $nothi_potro_attach_table = TableRegistry::get("NothiPotroAttachments");
            $nothi_potros_table = TableRegistry::get("NothiPotros");
            $nothi_potro_id_data = $nothi_potro_attach_table->find()->
                            where(['id'=>$nothi_potro_attachment_id])->
                            select(['id','nothi_potro_id','sarok_no'])->firstOrFail();
            $nothi_potro_id = $nothi_potro_id_data['nothi_potro_id'];

            $dak_datas = $nothi_potros_table->find()->
            where(['id'=>$nothi_potro_id])->
            select(['id','dak_id','dak_type'])->firstOrFail();

            $dak_id = $dak_datas['dak_id'];
            $dak_type = $dak_datas['dak_type'];
            if($dak_type=="Daptorik")
            {
                $dak_daptoprik_table = TableRegistry::get("DakDaptoriks");
                $dak_daptorik_data = $dak_daptoprik_table->find()->select(['dak_received_no'])
                                    ->where(['id'=>$dak_id])->first();
                $dak_received_no = $dak_daptorik_data['dak_received_no'];
            }else
            {
                $dak_nagorik_table = TableRegistry::get("DakNagoriks");
                $dak_nagorik_data = $dak_nagorik_table->find()->select(['dak_received_no'])
                    ->where(['id'=>$dak_id])->firstOrFail();
                $dak_received_no = $dak_nagorik_data['dak_received_no'];
            }

        }

        $new_potrojari_register = $this->newEntity();
        $new_potrojari_register->potrojari_id = $potrojari_info["potrojari_id"];
        $new_potrojari_register->nothi_master_id = $potrojari_info["nothi_master_id"];
        $new_potrojari_register->nothi_part_no = $potrojari_info["nothi_part_no"];
        $new_potrojari_register->nothi_notes_id = $potrojari_info["nothi_notes_id"];
        $new_potrojari_register->nothi_potro_id = $potrojari_info["nothi_potro_id"];
        $new_potrojari_register->potro_type = $potrojari_info["potro_type"];
        $new_potrojari_register->dak_id = isset($dak_datas["dak_id"])?$dak_datas["dak_id"]:0;
        $new_potrojari_register->dak_type = isset($dak_datas["dak_type"])?$dak_datas["dak_type"]:'';
        $new_potrojari_register->dak_receive_no = $dak_received_no;
        $new_potrojari_register->created_by = $potrojari_info["created_by"];
        $new_potrojari_register->modified_by = $potrojari_info["modified_by"];
        $new_potrojari_register->receiver_info = $potrojari_info["receiver_info"];
        $new_potrojari_register->onulipi_info = $potrojari_info["onulipi_info"];
        $new_potrojari_register->potrojari_sharok_no = isset($potrojari_info["sarok_no"])?$potrojari_info["sarok_no"]:'';

        $new_potrojari_register->potrojari_date=isset($potrojari_info["potrojari_date"])?$potrojari_info["potrojari_date"]:date("Y-m-d");
        $new_potrojari_register->potrojari_subject=isset($potrojari_info["potro_subject"])?$potrojari_info["potro_subject"]:"";
        $new_potrojari_register->is_sent=isset($potrojari_info["is_sent"])?$potrojari_info["is_sent"]:0;
        $new_potrojari_register->potro_status=isset($potrojari_info['potro_status'])?$potrojari_info['potro_status']:'';

        $new_potrojari_register->approval_office_id=!empty($request_data["approval_office_id"])?$request_data["approval_office_id"]:$request_data['approval_office_id_final'];
        $new_potrojari_register->approval_office_name=!empty($request_data["approval_office_name"])?$request_data["approval_office_name"]:$request_data['approval_office_name_final'];
        $new_potrojari_register->approval_designation_id=!empty($request_data["approval_officer_designation_id"])?$request_data["approval_officer_designation_id"]:$request_data['approval_officer_designation_id_final'];
        $new_potrojari_register->approval_designation_name=!empty($request_data["approval_officer_designation_label"])?$request_data["approval_officer_designation_label"]:$request_data['approval_officer_designation_label_final'];
        $new_potrojari_register->approval_officer_id=!empty($request_data["approval_officer_id"])?$request_data["approval_officer_id"]:$request_data['approval_officer_id_final'];
        $new_potrojari_register->approval_officer_name=!empty($request_data["approval_officer_name"])?$request_data["approval_officer_name"]:$request_data['approval_officer_name_final'];
        $new_potrojari_register->approval_unit_id=!empty($request_data["approval_office_unit_id"])?$request_data["approval_office_unit_id"]:$request_data['approval_office_unit_id_final'];
        $new_potrojari_register->approval_unit_name=!empty($request_data["approval_office_unit_name"])?$request_data["approval_office_unit_name"]:$request_data['approval_office_unit_name_final'];

        $new_potrojari_register->sender_office_id=!empty($request_data["office_id"])?$request_data["office_id"]:$request_data['sender_office_id_final'];
        $new_potrojari_register->sender_office_name=!empty($request_data["office_name"])?$request_data["office_name"]:'';
        $new_potrojari_register->sender_officer_designation_id=!empty($request_data["officer_designation_id"])?$request_data["officer_designation_id"]:$request_data['sender_officer_designation_id_final'];
        $new_potrojari_register->sender_officer_designation_name=!empty($request_data["officer_designation_label"])?$request_data["officer_designation_label"]:$request_data['sender_officer_designation_label_final'];
        $new_potrojari_register->sender_officer_id=!empty($request_data["officer_id"])?$request_data["officer_id"]:$request_data['sender_officer_id_final'];
        $new_potrojari_register->sender_officer_name=!empty($request_data["officer_name"])?$request_data["officer_name"]:$request_data['sender_officer_name_final'];
        $new_potrojari_register->sender_office_unit_id=!empty($request_data["office_unit_id"])?$request_data["office_unit_id"]:$request_data['sender_office_unit_id'];
        $new_potrojari_register->sender_unit_name=!empty($request_data["office_unit_name"])?$request_data["office_unit_name"]:'';

        $new_potrojari_register->potro_type=$request_data['potro_type'];
        $new_potrojari_register->security_level=$request_data['potro_security_level'];
        $new_potrojari_register->priority_level=$request_data['potro_priority_level'];

        $this->save($new_potrojari_register);
    }


    public function updatePotrojariRegister($potrojari_id)
    {
            $this->updateAll(['potro_status' => 'Sent'],['potrojari_id' => $potrojari_id]);
    }
}


