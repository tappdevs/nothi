<?php

namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Cake\Cache\Cache;
use Cake\I18n\Number;

class PotrojariTable extends ProjapotiTable
{

    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->addBehavior('MobileTrack');
        $this->hasMany('PotrojariReceiver', [
            'foreignKey' => 'potrojari_id',
        ]);
        $this->hasMany('PotrojariOnulipi', [
            'foreignKey' => 'potrojari_id',
        ]);
    }

    public function getInfo($condition = '', $order = 'id desc')
    {
        $queryString = $this->find();

        if (!empty($condition)) {
            $queryString = $queryString->where([$condition]);
        }

        if (!empty($order)) {
            $queryString = $queryString->order([$order]);
        }


        return $queryString;
    }
    public function getData($select = [], $conditions = [], $order = []){
        $query = $this->find();
        if(!empty($select)){
            $query->select($select);
        }
        if(!empty($conditions)){
            $query->where($conditions);
        }
        if(!empty($order)){
            $query->order($order);
        }
        return $query;
    }

    /**
     *
     * @param int $office_id
     * @param int $unit_id
     * @param int $designation_id
     * @param DateTime $time
     * @param String $type = 'Dak' for potrojari against Dak
     * @return int $count
     */
    public function allPotrojariCount($office_id = 0, $unit_id = 0,
                                      $designation_id = 0, $time = [],
                                      $type = '')
    {
        $query = $this->find();
        if (!empty($office_id)) {
            $query = $query->where(['approval_office_id' => $office_id]);
        }

        if (!empty($unit_id)) {
            $query = $query->where(['approval_office_unit_id' => $unit_id]);
        }

        if (!empty($designation_id)) {
            $query = $query->where(['approval_officer_designation_id' => $designation_id]);
        }
        if (!empty($time[0])) {
            $query = $query->where(['DATE(potrojari_date) >=' => $time[0]]);
        }
        if (!empty($time[1])) {
            $query = $query->where(['DATE(potrojari_date) <=' => $time[1]]);
        }

        if (!empty($type) && $type == 'Dak') {
            $query = $query->where([ 'nothi_potro_id <>' => '0']);
        }

        return $query->where([ 'potro_status' => 'Sent'])->count();
    }
    /**
     *
     * @param int $office_id
     * @param int $unit_id
     * @param int $designation_id
     * @param DateTime $time
     * @param String $type = 'Dak' for potrojari against Dak
     * @return int $count
     */
    public function allPotrojariIDArray($office_id = 0, $unit_id = 0,
                                        $designation_id = 0, $time = [],
                                        $type = '')
    {
        $query = $this->find('list',['keyField' =>'id','valueField'=>'id']);
        if (!empty($office_id)) {
            $query = $query->where(['approval_office_id' => $office_id]);
        }

        if (!empty($unit_id)) {
            $query = $query->where(['approval_office_unit_id' => $unit_id]);
        }

        if (!empty($designation_id)) {
            $query = $query->where(['approval_officer_designation_id' => $designation_id]);
        }
        if (!empty($time[0])) {
            $query = $query->where(['DATE(potrojari_date) >=' => $time[0]]);
        }
        if (!empty($time[1])) {
            $query = $query->where(['DATE(potrojari_date) <=' => $time[1]]);
        }

        if (!empty($type) && $type == 'Dak') {
            $query = $query->where([ 'nothi_potro_id <>' => '0']);
        }

        return $query->where([ 'potro_status' => 'Sent'])->toArray();
    }

    public function getPotrojaribyNothiType($nothi_type = 0)
    {
        $nothi_master_table = TableRegistry::get('NothiMasters');
        $nothi_master_ids   = $nothi_master_table->find('list',
            [ 'keyField' => 'id', 'valueField' => 'id'])->where(['nothi_types_id' => $nothi_type])->toArray();
        return $this->find()->where([ 'nothi_master_id IN' => $nothi_master_ids])->distinct(['nothi_master_id']);
    }

    public function getNisponnobyPotrojari($office_id = 0, $unit_id = 0, $designation_id = 0, $time = [])
    {
        $NothiPartsListForNothiVukto = TableRegistry::get('NothiParts')->notePartListfornothiVuktoDak($office_id , $unit_id , $designation_id );
        $NothiPartsListForSelfNote = TableRegistry::get('NothiParts')->selfSrijitoNotePart($office_id, $unit_id, $designation_id);
        $allNothiPartForNothiVuktoAndSlefNote = array_merge($NothiPartsListForNothiVukto,$NothiPartsListForSelfNote);
        TableRegistry::remove('NothiMasterCurrentUsers');
        $NothiMasterCurrentUsersTable = TableRegistry::get('NothiMasterCurrentUsers');
        $allNothipartNo = $NothiMasterCurrentUsersTable->find('list',['keyField'=>'id','valueField'=>'nothi_part_no'])->where(['is_finished' => 1,'nothi_part_no IN' => $allNothiPartForNothiVuktoAndSlefNote]);

        $query = $this->find();
        if (!empty($office_id)) {
            $allNothipartNo = $allNothipartNo->where(['office_id' => $office_id]);
            $query = $query->where(['office_id' => $office_id]);
        }

        if (!empty($unit_id)) {
            $allNothipartNo = $allNothipartNo->where(['office_unit_id' => $unit_id]);
            $query = $query->where(['office_unit_id' => $unit_id]);
        }

        if (!empty($designation_id)) {
            $allNothipartNo = $allNothipartNo->where(['office_unit_organogram_id' => $designation_id]);
            $query = $query->where(['approval_officer_designation_id' => $designation_id]);
        }

        if (!empty($time[0])) {
            $allNothipartNo = $allNothipartNo->where(['DATE(issue_date) >=' => $time[0]]);
            $query = $query->where(['DATE(modified) >=' => $time[0]]);
        }

        if (!empty($time[1])) {
            $allNothipartNo = $allNothipartNo->where(['DATE(issue_date) <=' => $time[1]]);
            $query = $query->where(['DATE(modified) <=' => $time[1]]);
        }

        $allNothipartNo = $allNothipartNo->toArray();

        if(!empty($allNothipartNo)){
            return $query->where([ 'potro_status' => 'Sent'])->where(['nothi_part_no IN'=>  array_values($allNothipartNo) ])->group(['nothi_part_no'])->count();
        }

        return 0;

    }

    public function getNothiPotroIfPotrojari($nothi_potro_id = 0, $nothi_office = 0, $nothi_master_id = 0){

        return $this->find()->where(['nothi_potro_id'=>$nothi_potro_id,'potrojari_draft_office_id'=>$nothi_office,'nothi_master_id'=>$nothi_master_id]);
    }

    public function dateOfpotrojari($nothi_master_id = 0,$nothi_part_no = 0){
        $query = $this->find()->select(['potrojari_happened' => 'date(potrojari_date)']);
        if(!empty($nothi_master_id)){
            $query = $query->where(['nothi_master_id' => $nothi_master_id]);
        }
        if(!empty($nothi_part_no)){
            $query = $query->where(['nothi_part_no' => $nothi_part_no]);
        }
        return $query;
    }
    public function countPotrojariByNotesId($note_id,$nothi_part_no = 0 ){
        if(empty($nothi_part_no)){
            return $this->find()->where(['nothi_notes_id' => $note_id])->count();
        }
        else{
            return $this->find()->where(['nothi_notes_id' => $note_id,'nothi_part_no' => $nothi_part_no])->count();
        }
    }
    public function allPotrojariInternalCountByNothiPartNO($nothi_part_no = [], $time = [])
    {
        $query = $this->find();
        if (!empty($nothi_part_no)) {
            $query = $query->where(['nothi_part_no IN' => $nothi_part_no]);
        }
        if (!empty($time[0])) {
            $query = $query->where(['DATE(potrojari_date) >=' => $time[0]]);
        }
        if (!empty($time[1])) {
            $query = $query->where(['DATE(potrojari_date) <=' => $time[1]]);
        }

        return $query->where([ 'potro_status' => 'Sent' ,'potrojari_internal' => 1])->group(['nothi_part_no'])->count();
    }
    public function allPotrojariInternalCountByPotrojariId($potrojari_id = [], $time = [],$group_by = 1)
    {
        $query = $this->find();
        if (!empty($potrojari_id)) {
            $query = $query->where(['id IN' => $potrojari_id]);
        }else{
            return 0;
        }
//        if (!empty($time[0])) {
//            $query = $query->where(['DATE(potrojari_date) >=' => $time[0]]);
//        }
//        if (!empty($time[1])) {
//            $query = $query->where(['DATE(potrojari_date) <=' => $time[1]]);
//        }
        if($group_by == 1){
            return $query->where([ 'potro_status' => 'Sent' ,'potrojari_internal' => 1])->group(['nothi_part_no'])->count();
        }else{
            return $query->where([ 'potro_status' => 'Sent' ,'potrojari_internal' => 1])->count();
        }

    }
    public function allInternalPotrojariCountForMigration($office_id = 0, $unit_id = 0, $designation_id = 0, $time = []){
        $allpotrojariId = $this->allPotrojariIDArray($office_id,$unit_id,$designation_id,$time);
        $potrojariReceiverTable = TableRegistry::get('PotrojariReceiver');
        $potrojariOnulipiTable = TableRegistry::get('PotrojariOnulipi');
        $potrojari_from_receiver = $potrojariReceiverTable->allPotrojariIDArray($allpotrojariId);
        $potrojari_from_onulipi = $potrojariOnulipiTable->allPotrojariIDArray($allpotrojariId,$potrojari_from_receiver);
        return count($potrojari_from_receiver) + count($potrojari_from_onulipi);
    }
    public function getDesignationWisePotrojariPending($office_id = 0,$unit_id = 0,$designation_id = 0){
        if (($totalPotrojariPending = Cache::read('total_potrojari_pending_'.$designation_id, 'memcached')) === false) {
            //            $result = $table_reports->officeSummary($officeid);
            $allCurrentNothi = TableRegistry::get('NothiMasterCurrentUsers')->getAllNothiPartNo($office_id, $unit_id, $designation_id)->where(['nothi_office' => $office_id])->select(['nothi_part_no'])->hydrate(false)->find('list', ['valueField' => 'nothi_part_no'])->toArray();

            $receiverList = TableRegistry::get('PotrojariReceiver')->find()->join([
                "Potrojari" => [
                    'table' => 'potrojari', 'type' => 'inner',
                    'conditions' => ['PotrojariReceiver.potrojari_id =Potrojari.id']
                ]
            ])->where(['PotrojariReceiver.is_sent' => 0, 'Potrojari.nothi_part_no IN' => $allCurrentNothi,'Potrojari.potro_status <>' => 'Draft'])->count();
            $onulipiList = TableRegistry::get('PotrojariOnulipi')->find()->join([
                "Potrojari" => [
                    'table' => 'potrojari', 'type' => 'inner',
                    'conditions' => ['PotrojariOnulipi.potrojari_id =Potrojari.id']
                ]
            ])->where(['PotrojariOnulipi.is_sent' => 0, 'Potrojari.nothi_part_no IN' => $allCurrentNothi,'Potrojari.potro_status <>' => 'Draft'])->count();
            $totalPotrojariPending = $receiverList + $onulipiList;
            Cache::write('total_potrojari_pending_'.$designation_id, $totalPotrojariPending, 'memcached');
        }
        return $totalPotrojariPending;
    }

    public function checkSarokExists($sarok_no,$potrojari_id=0,$check_sarok_in_nothi_potros = 0){
        $sarok_count = $this->find()->where(['sarok_no' => $sarok_no]);

        if(!empty($potrojari_id)){
            $sarok_count = $sarok_count->where(['id <> ' => $potrojari_id]);
        }
        $sarok_count = $sarok_count->count();

        if ($sarok_count > 0) {
            return true;
        } else {
            // need to check in nothi potros also
            if(!empty($check_sarok_in_nothi_potros)){
                return TableRegistry::get('NothiPotros')->checkSarokExists($sarok_no);
            }
            return false;
        }
    }
    public function getUniqueSarok($sarok_no,$potrojari_id=0,$check_sarok_in_nothi_potros = 0){

        while($this->checkSarokExists($sarok_no,$potrojari_id,$check_sarok_in_nothi_potros)){
            $sarok_no= bnToen($sarok_no);
            $sarok_no++;
            $sarok_no= enTobn($sarok_no);
        }

        return $sarok_no;
    }

    public function getUniqueSarokCount($nothi_no,$total_potrojari,$check_sarok_in_nothi_potros = 0){

        while($this->checkSarokExists($nothi_no.'.'.$total_potrojari,0,$check_sarok_in_nothi_potros)){
            $total_potrojari= bnToen($total_potrojari);
            $total_potrojari++;
            $total_potrojari= enTobn($total_potrojari);
        }

        return $total_potrojari;
    }

    public function generatePotrojariSarokNumber($nothi_master_id,$unit_id,$office_id){
        $officeUnitsTable = TableRegistry::get('OfficeUnits');
        $ownUnitInfo = $officeUnitsTable->get($unit_id);

        $nothiMastersTable = TableRegistry::get('NothiMasters');
        $master_info = $nothiMastersTable->get($nothi_master_id);

        $totalPotrojari = $this->find()->where(['office_id' => $office_id,
            'YEAR(potrojari_date)' => date('Y'), 'potrojari_draft_office_id' => $office_id,
            'potrojari_draft_unit' => $unit_id])->count();

        $totalPotrojari = Number::format($ownUnitInfo['sarok_no_start'] + $totalPotrojari + 1,['pattern'=>'######']);
        return $master_info['nothi_no'].'.'.$totalPotrojari;
    }


    public function afterSave($event, $entity, $options = []){

        $potro_status=$entity->toArray();

        if($potro_status['potro_status']==="Sent" && ! $entity->isNew())
        {
            $table_nothi_register_lists= TableRegistry::get('NothiRegisterLists');
            $response = $table_nothi_register_lists->saveDataNothiPotrojari($entity);
            if ($response['status'] == 'error') {
                throw new \Exception('NothiRegisterLists is not save. '.$response['msg']);
            }
        }
    }

    public function getJarikritoPotroByDateAndUnit($selected_fields=[],$start_date='',$end_date='',$unit_id='',$page_no=1,$organogram_id='0'){
        $khosra_list = $this->find();
        if(!empty($selected_fields)){
            $khosra_list = $khosra_list->select($selected_fields);
        }
        if(!empty($unit_id) && $unit_id !='all_unit'){
            $khosra_list = $khosra_list->where(['office_unit_id'=>$unit_id]);
        }
        if(!empty($organogram_id) && $organogram_id !='0'){
            $khosra_list = $khosra_list->where(['created_by_designation_id'=>$organogram_id]);
        }
        if(!empty($start_date) && !empty($end_date)){
            $khosra_list = $khosra_list->where(function($khosra_list) use ($start_date,$end_date){
                return $khosra_list->between('date(created)',$start_date,$end_date);
            });
        }
        return $khosra_list->where(['potro_status'=>'Sent'])->order(['id'=>'DESC','potrojari_draft_unit'=>'ASC'])->limit(20)->page($page_no)->toArray();

    }



}