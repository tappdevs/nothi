<?php

namespace App\Model\Table;

use Cake\Network\Session;
use Cake\ORM\Table;

class ProjapotiTable extends Table
{
    public function getCurrentDakSection()
    {
        $session = new Session();
        return $session->read('selected_office_section');
    }

    public function columnList() {
		return $this->schema()->columns();
	}

	/**
	 * Persists multiple entities of a table.
	 *
	 * The records will be saved in a transaction which will be rolled back if
	 * any one of the records fails to save due to failed validation or database
	 * error.
	 *
	 * @param array|\Cake\ORM\ResultSet $entities Entities to save.
	save() for each entity.
	 * @return bool|array|\Cake\ORM\ResultSet False on failure, entities list on success.
	 */
	public function saveMany($entities, $options = [])
	{
		$isNew = [];

		$connection = $this->connection();
		$return = $connection->transactional(function () use ($entities, $options, &$isNew) {
				foreach ($entities as $key => $entity) {
					$isNew[$key] = $entity->isNew();
					if ($this->save($entity, $options) === false) {
						return false;
					}
				}
			});

		if ($return === false) {
			foreach ($entities as $key => $entity) {
				if (isset($isNew[$key]) && $isNew[$key]) {
					$entity->unsetProperty($this->primaryKey());
					$entity->isNew(true);
				}
			}

			return false;
		}

		return $entities;
	}
}