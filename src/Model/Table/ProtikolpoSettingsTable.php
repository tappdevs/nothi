<?php
namespace App\Model\Table;

use App\Model\Entity\ProtikolpoSetting;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ProtikolpoSettings Model
 */
class ProtikolpoSettingsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $conn = ConnectionManager::get('NothiAccessDb');
        $this->connection($conn);
        $this->table('protikolpo_settings');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');


        $this->belongsTo('ProtikolpoLog', [
            'foreignKey' => false,
            'conditions' => 'ProtikolpoLog.protikolpo_id=ProtikolpoSettings.id',
            'joinType' => 'LEFT'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('protikolpos', 'create')
            ->notEmpty('protikolpos');

        $validator
            ->add('selected_protikolpo', 'valid', ['rule' => 'numeric'])
            ->requirePresence('selected_protikolpo', 'create')
            ->notEmpty('selected_protikolpo');

        $validator
            ->add('start_date', 'valid', ['rule' => 'date'])
            ->allowEmpty('start_date');

        $validator
            ->add('end_date', 'valid', ['rule' => 'date'])
            ->allowEmpty('end_date');

        $validator
            ->add('active_status', 'valid', ['rule' => 'numeric'])
            ->requirePresence('active_status', 'create')
            ->notEmpty('active_status');

        $validator
            ->add('created_by', 'valid', ['rule' => 'numeric'])
            ->requirePresence('created_by', 'create')
            ->notEmpty('created_by');

        $validator
            ->add('modified_by', 'valid', ['rule' => 'numeric'])
            ->requirePresence('modified_by', 'create')
            ->notEmpty('modified_by');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        return $rules;
    }
}
