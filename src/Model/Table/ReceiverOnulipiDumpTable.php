<?php
namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

class ReceiverOnulipiDumpTable extends ProjapotiTable
{
        public function initialize(array $config)
        {
            $conn = ConnectionManager::get('NothiAccessDb');
            $this->connection($conn);
            $this->primaryKey('id');
            $this->addBehavior('Timestamp');
            $this->table('receiver_onulipi_dump');
      }
}