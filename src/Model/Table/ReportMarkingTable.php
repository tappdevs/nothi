<?php

namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

class ReportMarkingTable extends ProjapotiTable
{
    public function initialize(array $config)
    {
        //parent::initialize($config);
        $conn = ConnectionManager::get('NothiReportsDb');
        $this->connection($conn);
        $this->table('report_marking');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->addBehavior('MobileTrack');
    }
    public function getData($select = [], $conditions = [], $order = []){
        $query = $this->find();
        if(!empty($select)){
            $query->select($select);
        }
        if(!empty($conditions)){
            $query->where($conditions);
        }
        if(!empty($order)){
            $query->order($order);
        }
        return $query;
    }
}