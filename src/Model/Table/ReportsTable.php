<?php

namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

class ReportsTable extends ProjapotiTable
{

    public function getAll($selected_office_section = '', $page = 0, $len = 10, $condition)
    {


        $table_dm    = TableRegistry::get('DakMovements');
        $queryString = $table_dm->find()->select([
                "DakMovements.id",
                "DakMovements.modified",
                "DakMovements.dak_actions",
                "DakDaptoriks.sender_sarok_no",
                "DakDaptoriks.receiving_date",
                "DakDaptoriks.sender_name",
                "DakDaptoriks.dak_subject",
                "DakDaptoriks.dak_security_level",
                "DakDaptoriks.dak_priority_level",
                "DakNagoriks.receive_date",
                "DakNagoriks.dak_subject",
                "DakNagoriks.name_bng",
                "NothiMasters.nothi_no"
            ])->where(['DakMovements.to_office_id' => $selected_office_section['office_id'],
                'DakMovements.to_office_unit_id' => $selected_office_section['office_unit_id'],
                'DakMovements.to_officer_designation_id' => $selected_office_section['office_unit_organogram_id']])
            ->join([
                'DakDaptoriks' => [
                    'table' => 'dak_daptoriks',
                    "conditions" => "DakMovements.dak_id = DakDaptoriks.id AND DakMovements.dak_type = 'Daptorik'",
                    "type" => "LEFT"
                ],
                'DakNagoriks' => [
                    'table' => 'dak_nagoriks',
                    "conditions" => "DakMovements.dak_id = DakNagoriks.id AND DakMovements.dak_type = 'Nagoriks'",
                    "type" => "LEFT"
                ],
                'NothiMasters' => [
                    'table' => 'nothi_masters',
                    "conditions" => "DakMovements.dak_id = NothiMasters.id",
                    "type" => "LEFT"
                ]
            ])
            ->order("DakMovements.modified DESC");

        if (!empty($condition)) {
            $queryString = $queryString->where([$condition]);
        }

        if (!empty($page)) {
            $queryString = $queryString->page($page, $len);
        }


        return $queryString;
    }

    public function getAllPotrojari($selected_office_section = '', $page = 0, $len = 10, $condition)
    {
        $table_dm    = TableRegistry::get('Potrojari');
        $queryString = $table_dm->find()->select([
                "Potrojari.id",
                "Potrojari.modified",
                "Potrojari.sarok_no",
                "Potrojari.created",
                "Potrojari.potro_subject",
                "Potrojari.office_name",
                "Potrojari.officer_designation_label",
                "Potrojari.potrojari_date",
                "Potrojari.potro_security_level",
                "Potrojari.potro_priority_level",
                "Potrojari.potro_status"
            ])
            ->where(['Potrojari.office_id' => $selected_office_section['office_id'],
                'Potrojari.office_unit_id' => $selected_office_section['office_unit_id'],
                'Potrojari.officer_designation_id' => $selected_office_section['office_unit_organogram_id']])
            ->order("Potrojari.modified DESC");

        if (!empty($condition)) {
            $queryString = $queryString->where([$condition]);
        }


        if (!empty($page)) {
            $queryString = $queryString->page($page, $len);
        }

        return $queryString;
    }

    public function getAllSectionWiseNothi($startDate,$endDate,$condition)
    {

        $table_dm = TableRegistry::get('NothiParts');

        $queryString = $table_dm->find()->select([
                "NothiParts.nothi_created_date",
                "NothiParts.id",
                "NothiParts.nothi_class",
                "NothiParts.nothi_part_no",
                "NothiParts.subject",
                "NothiParts.nothi_no",
                "NothiParts.created",
                "NothiParts.modified",
                "NothiParts.nothi_types_id",
                "NothiParts.office_units_id",
                "NothiParts.office_units_organogram_id",
                "NothiParts.office_id",
                "NothiParts.nothi_types_id",
                "NothiTypes.type_name"
            ])
            ->join([
                'NothiTypes' => [
                    'table' => 'nothi_types',
                    "conditions" => ["NothiParts.nothi_types_id = NothiTypes.id"],
                    "type" => "LEFT"
                ],
            ])
            ->where(function($query) use ($startDate,$endDate){
                return $query->between('date(NothiParts.nothi_created_date)',$startDate,$endDate);
            })
            ->where(['NothiParts.nothi_part_no' => '1'])
            ->order("NothiParts.office_units_id ASC, NothiParts.nothi_created_date DESC");

        if (!empty($condition)) {
            $queryString = $queryString->where([$condition]);
        }


        return $queryString;
    }
//    public function getAllSectionWiseNothi($condition)
//    {
//
//        $table_dm = TableRegistry::get('NothiParts');
//
//        $queryString = $table_dm->find()->select([
//                "NothiParts.nothi_created_date",
//                "NothiParts.id",
//                "NothiParts.nothi_class",
//                "NothiParts.nothi_part_no",
//                "NothiParts.subject",
//                "NothiParts.nothi_no",
//                "NothiParts.created",
//                "NothiParts.modified",
//                "NothiParts.nothi_types_id",
//                "NothiParts.office_units_id",
//                "NothiParts.office_units_organogram_id",
//                "NothiParts.office_id",
//                "NothiParts.nothi_types_id",
//                "NothiTypes.type_name"
//            ])
//            ->join([
//                'NothiTypes' => [
//                    'table' => 'nothi_types',
//                    "conditions" => "NothiParts.nothi_types_id = NothiTypes.id",
//                    "type" => "LEFT"
//                ],
//            ])
//            ->where(['NothiParts.nothi_part_no' => '1'])
//            ->order("NothiParts.office_units_id ASC, NothiParts.nothi_created_date DESC");
//
//        if (!empty($condition)) {
//            $queryString = $queryString->where([$condition]);
//        }
//
//
//        return $queryString;
//    }

    public function getAllReceivedNothi($startDate,$endDate,$condition=[])
    {
        $table_dm    = TableRegistry::get('NothiMasterMovements');
        $queryString = $table_dm->find()->select([
                "NothiMasterMovements.id",
                "NothiMasterMovements.nothi_master_id",
                "NothiMasterMovements.modified",
                "NothiMasterMovements.from_officer_name",
                "NothiMasterMovements.from_office_unit_id",
                "NothiMasterMovements.from_office_unit_name",
                "NothiMasterMovements.created",
                "NothiMasterMovements.to_office_unit_id",
                "NothiMasterMovements.to_office_unit_name",
                "NothiMasterMovements.to_officer_name",
                "NothiMasterMovements.from_officer_designation_label",
                "NothiMasterMovements.to_officer_designation_label",
                "NothiParts.nothi_part_no",
                "NothiParts.id",
                "NothiParts.subject",
                "NothiParts.nothi_no",
                "NothiParts.created",
                "NothiParts.modified",
//                    "NothiMasterCurrentUsers.issue_date",
//                    "NothiMasterCurrentUsers.forward_date"
            ])
            ->join([
                'NothiParts' => [
                    'table' => 'nothi_parts',
                    "conditions" => ["NothiMasterMovements.nothi_master_id = NothiParts.nothi_masters_id","NothiMasterMovements.nothi_part_no = NothiParts.id"],
                    "type" => "INNER"
                ]
//                    ,
//                    'NothiMasterCurrentUsers' => [
//                        'table' => 'nothi_master_current_users',
//                        "conditions" => "NothiMasterMovements.nothi_part_no = NothiMasterCurrentUsers.nothi_part_no AND NothiMasterCurrentUsers.nothi_part_no = NothiParts.id",
//                        "type" => "INNER"
//                    ]
            ])
            ->where(function($query) use ($startDate, $endDate){
                return $query->between('date(NothiMasterMovements.created)',$startDate,$endDate);
            })
            ->order("NothiMasterMovements.modified DESC")->distinct(['NothiMasterMovements.id']);


        if (!empty($condition)) {
            $queryString = $queryString->where($condition);
        }


        return $queryString;
    }

    public function getAllSentNothi($startDate,$endDate,$field,$condition=[],$orCondition=[])
    {
        $table_dm    = TableRegistry::get('NothiMasterMovements');
        $queryString = $table_dm->find()->select([
                "NothiMasterMovements.id",
                "NothiMasterMovements.modified",
                "NothiMasterMovements.from_office_unit_id",
                "NothiMasterMovements.from_office_unit_name",
                "NothiMasterMovements.from_officer_name",
                "NothiMasterMovements.created",
                "NothiMasterMovements.to_office_unit_id",
                "NothiMasterMovements.to_office_unit_name",
                "NothiMasterMovements.from_office_name",
                "NothiMasterMovements.to_office_name",
                "NothiMasterMovements.to_officer_name",
                "NothiMasterMovements.from_officer_designation_label",
                "NothiMasterMovements.to_officer_designation_label",
                "NothiParts.id",
                "NothiParts.nothi_part_no",
                "NothiParts.subject",
                "NothiParts.nothi_no",
                "NothiParts.created",
                "NothiParts.modified",
//                    "NothiMasterCurrentUsers.issue_date",
//                    "NothiMasterCurrentUsers.forward_date"
            ])
            ->join([
                'NothiParts' => [
                    'table' => 'nothi_parts',
                    "conditions" => ["NothiMasterMovements.nothi_master_id = NothiParts.nothi_masters_id","NothiMasterMovements.nothi_part_no = NothiParts.id"],
                    "type" => "INNER"
                ]
//                    ,
//                    'NothiMasterCurrentUsers' => [
//                        'table' => 'nothi_master_current_users',
//                        "conditions" => "NothiMasterMovements.nothi_part_no = NothiMasterCurrentUsers.nothi_part_no AND NothiMasterCurrentUsers.nothi_part_no = NothiMasters.id",
//                        "type" => "LEFT"
//                    ]
            ]);

        if (!empty($condition)) {
            $queryString = $queryString->where($condition);
        }
        if (!empty($orCondition)) {
            $queryString = $queryString->orWhere($orCondition);
        }
        $queryString = $queryString->order("NothiMasterMovements.modified DESC")->distinct(['NothiMasterMovements.id']);
        $queryString = $queryString->where(function($query) use ($startDate,$endDate,$field){
            return $query->between('date('.$field.')',$startDate,$endDate);
        });
        return $queryString;
    }
//    public function getAllSentNothi($condition)
//    {
//        $table_dm    = TableRegistry::get('NothiMasterMovements');
//        $queryString = $table_dm->find()->select([
//                "NothiMasterMovements.id",
//                "NothiMasterMovements.modified",
//                "NothiMasterMovements.from_office_unit_id",
//                "NothiMasterMovements.from_office_unit_name",
//                "NothiMasterMovements.from_officer_name",
//                "NothiMasterMovements.created",
//                "NothiMasterMovements.to_office_unit_id",
//                "NothiMasterMovements.to_office_unit_name",
//                "NothiMasterMovements.from_office_name",
//                "NothiMasterMovements.to_office_name",
//                "NothiMasterMovements.to_officer_name",
//                "NothiMasterMovements.from_officer_designation_label",
//                "NothiMasterMovements.to_officer_designation_label",
//                "NothiParts.id",
//                "NothiParts.nothi_part_no",
//                "NothiParts.subject",
//                "NothiParts.nothi_no",
//                "NothiParts.created",
//                "NothiParts.modified",
////                    "NothiMasterCurrentUsers.issue_date",
////                    "NothiMasterCurrentUsers.forward_date"
//            ])
//            ->join([
//                'NothiParts' => [
//                    'table' => 'nothi_parts',
//                    "conditions" => "NothiMasterMovements.nothi_master_id = NothiParts.nothi_masters_id AND NothiMasterMovements.nothi_part_no = NothiParts.id ",
//                    "type" => "INNER"
//                ]
////                    ,
////                    'NothiMasterCurrentUsers' => [
////                        'table' => 'nothi_master_current_users',
////                        "conditions" => "NothiMasterMovements.nothi_part_no = NothiMasterCurrentUsers.nothi_part_no AND NothiMasterCurrentUsers.nothi_part_no = NothiMasters.id",
////                        "type" => "LEFT"
////                    ]
//            ])
//            ->order("NothiMasterMovements.modified DESC")->distinct(['NothiMasterMovements.id']);
//
//
//        if (!empty($condition)) {
//            $queryString = $queryString->where([$condition]);
//        }
//        //pr($queryString); die;
//        return $queryString;
//    }

    public function getAllSectionWiseDak($selected_office_section = '', $page = 0, $len = 10,
                                         $condition)
    {
        $table_dm    = TableRegistry::get('DakMovements');
        $queryString = $table_dm->find()->select([
                "DakDaptoriks.sender_office_unit_name",
                "DakDaptoriks.dak_received_no",
                "DakDaptoriks.sender_sarok_no",
                "DakDaptoriks.sender_office_name",
                "DakDaptoriks.sending_date",
                "DakDaptoriks.dak_subject",
                "DakMovements.to_office_name",
                "DakDaptoriks.receiving_date",
                "DakDaptoriks.dak_security_level",
                "DakDaptoriks.dak_priority_level",
                "DakDaptoriks.dak_status"
            ])
            ->where(['DakMovements.from_office_id' => $selected_office_section['office_id'],
                'DakMovements.from_office_unit_id' => $selected_office_section['office_unit_id']])
            ->join([
                'DakDaptoriks' => [
                    'table' => 'dak_daptoriks',
                    "conditions" => "DakMovements.dak_id = DakDaptoriks.id",
                    "type" => "INNER"
                ]
            ])
            ->order("DakMovements.modified DESC");

        if (!empty($condition)) {
            $queryString = $queryString->where([$condition]);
        }

        if (!empty($page)) {
            $queryString = $queryString->page($page, $len);
        }
        return $queryString;
    }

    public function getAllInboxDakReport($selected_office_section = '', $page = 0, $len = 10,
                                         $condition)
    {
        $table_dm    = TableRegistry::get('DakMovements');
        $queryString = $table_dm->find()->select([
                "DakDaptoriks.dak_received_no",
                "DakDaptoriks.sender_sarok_no",
                "DakDaptoriks.sending_date",
                "DakMovements.dak_type",
                "DakDaptoriks.dak_subject",
                "DakDaptoriks.sender_office_name",
                "DakMovements.from_office_name",
                "DakMovements.to_office_name",
                "DakDaptoriks.receiving_date",
                "DakDaptoriks.dak_security_level",
                "DakDaptoriks.dak_priority_level",
                "DakDaptoriks.dak_status"
            ])
            ->where(['DakMovements.from_office_id' => $selected_office_section['office_id'],
                'DakMovements.from_office_unit_id' => $selected_office_section['office_unit_id']])
            ->join([
                'DakDaptoriks' => [
                    'table' => 'dak_daptoriks',
                    "conditions" => "DakMovements.dak_id = DakDaptoriks.id",
                    "type" => "INNER"
                ]
            ])
            ->order("DakMovements.modified DESC");

        if (!empty($condition)) {
            $queryString = $queryString->where([$condition]);
        }

        if (!empty($page)) {
            $queryString = $queryString->page($page, $len);
        }
        return $queryString;
    }

    public function getAllsentDakReport($selected_office_section = '', $page = 0, $len = 10,
                                        $condition)
    {
        $table_dm    = TableRegistry::get('DakMovements');
        $queryString = $table_dm->find()->select([
                "DakDaptoriks.dak_received_no",
                "DakDaptoriks.sender_sarok_no",
                "DakDaptoriks.sending_date",
                "DakMovements.dak_type",
                "DakDaptoriks.dak_subject",
                "DakDaptoriks.sender_office_name",
                "DakMovements.from_office_name",
                "DakMovements.to_office_name",
                "DakMovements.to_sarok_no",
                "DakDaptoriks.receiving_date",
                "DakDaptoriks.dak_security_level",
                "DakDaptoriks.dak_priority_level",
                "DakDaptoriks.dak_status"
            ])
            ->where(['DakMovements.from_office_id' => $selected_office_section['office_id'],
                'DakMovements.from_office_unit_id' => $selected_office_section['office_unit_id']])
            ->join([
                'DakDaptoriks' => [
                    'table' => 'dak_daptoriks',
                    "conditions" => "DakMovements.dak_id = DakDaptoriks.id AND DakDaptoriks.dak_status = 'Sent'",
                    "type" => "INNER"
                ]
            ])
            ->order("DakMovements.modified DESC");

        if (!empty($condition)) {
            $queryString = $queryString->where([$condition]);
        }

        if (!empty($page)) {
            $queryString = $queryString->page($page, $len);
        }
        return $queryString;
    }

    public function getAllpendingDakReport($selected_office_section = '', $page = 0, $len = 10,
                                           $condition)
    {
        $table_dm    = TableRegistry::get('DakMovements');
        $queryString = $table_dm->find()->select([
                "DakDaptoriks.dak_received_no",
                "DakDaptoriks.sender_sarok_no",
                "DakDaptoriks.sending_date",
                "DakMovements.dak_type",
                "DakDaptoriks.dak_subject",
                "DakDaptoriks.sender_office_name",
                "DakMovements.from_office_name",
                "DakMovements.from_sarok_no",
                "DakMovements.to_office_name",
                "DakMovements.to_sarok_no",
                "DakDaptoriks.receiving_date",
                "DakDaptoriks.dak_security_level",
                "DakDaptoriks.dak_priority_level",
                "DakDaptoriks.dak_status"
            ])
            ->where(['DakMovements.from_office_id' => $selected_office_section['office_id'],
                'DakMovements.from_office_unit_id' => $selected_office_section['office_unit_id']])
            ->join([
                'DakDaptoriks' => [
                    'table' => 'dak_daptoriks',
                    "conditions" => "DakMovements.dak_id = DakDaptoriks.id ",
                    "type" => "INNER"
                ]
            ])
            ->order("DakMovements.modified DESC");

        if (!empty($condition)) {
            $queryString = $queryString->where([$condition]);
        }

        if (!empty($page)) {
            $queryString = $queryString->page($page, $len);
        }
        return $queryString;
    }

    public function getAllnothiVuktoDakReport($selected_office_section = '', $page = 0, $len = 10,
                                              $condition)
    {
        $table_dm    = TableRegistry::get('DakMovements');
        $queryString = $table_dm->find()->select([
                "DakDaptoriks.dak_received_no",
                "DakDaptoriks.sending_date",
                "DakMovements.dak_type",
                "DakMovements.created",
                "DakDaptoriks.dak_subject",
                "DakDaptoriks.sender_office_name",
                "DakMovements.from_office_name",
                "DakMovements.from_sarok_no",
                "DakMovements.to_office_name",
                "DakMovements.to_sarok_no",
                "DakDaptoriks.receiving_date",
                "DakDaptoriks.sender_sarok_no",
                "DakDaptoriks.dak_security_level",
                "DakDaptoriks.dak_priority_level",
                "DakDaptoriks.dak_status",
                "DakMovements.operation_type",
                "DakMovements.created"
            ])
            ->where(['DakMovements.from_office_id' => $selected_office_section['office_id'],
                'DakMovements.from_office_unit_id' => $selected_office_section['office_unit_id']])
            ->join([
                'DakDaptoriks' => [
                    'table' => 'dak_daptoriks',
                    "conditions" => "DakMovements.dak_id = DakDaptoriks.id AND DakDaptoriks.dak_status = 'NothiVukto'",
                    "type" => "INNER"
                ]
            ])
            ->order("DakMovements.modified DESC");

        if (!empty($condition)) {
            $queryString = $queryString->where([$condition]);
        }

        if (!empty($page)) {
            $queryString = $queryString->page($page, $len);
        }
        return $queryString;
    }

    public function getAllnothiJatDakReport($selected_office_section = '', $page = 0, $len = 10,
                                            $condition)
    {
        $table_dm    = TableRegistry::get('DakMovements');
        $queryString = $table_dm->find()->select([
                "DakDaptoriks.dak_received_no",
                "DakDaptoriks.sending_date",
                "DakMovements.dak_type",
                "DakMovements.created",
                "DakDaptoriks.dak_subject",
                "DakDaptoriks.sender_office_name",
                "DakMovements.from_office_name",
                "DakMovements.from_sarok_no",
                "DakMovements.to_office_name",
                "DakMovements.to_sarok_no",
                "DakDaptoriks.sender_sarok_no",
                "DakDaptoriks.receiving_date",
                "DakDaptoriks.dak_security_level",
                "DakDaptoriks.dak_priority_level",
                "DakDaptoriks.dak_status",
                "DakMovements.operation_type",
                "DakMovements.created"
            ])
            ->where(['DakMovements.from_office_id' => $selected_office_section['office_id'],
                'DakMovements.from_office_unit_id' => $selected_office_section['office_unit_id']])
            ->join([
                'DakDaptoriks' => [
                    'table' => 'dak_daptoriks',
                    "conditions" => "DakMovements.dak_id = DakDaptoriks.id AND DakDaptoriks.dak_status = 'NothiVukto'",
                    "type" => "INNER"
                ]
            ])
            ->order("DakMovements.modified DESC");

        if (!empty($condition)) {
            $queryString = $queryString->where([$condition]);
        }

        if (!empty($page)) {
            $queryString = $queryString->page($page, $len);
        }
        return $queryString;
    }

    public function getAllpresentedDakInNothiReport($selected_office_section = '', $page = 0,
                                                    $len = 10, $condition)
    {
        $table_dm    = TableRegistry::get('DakMovements');
        $queryString = $table_dm->find()->select([
                "DakDaptoriks.dak_received_no",
                "DakDaptoriks.sending_date",
                "DakMovements.dak_type",
                "DakMovements.created",
                "DakDaptoriks.dak_subject",
                "DakDaptoriks.sender_office_name",
                "DakMovements.from_office_name",
                "DakMovements.from_sarok_no",
                "DakMovements.to_office_name",
                "DakMovements.to_sarok_no",
                "DakDaptoriks.sender_sarok_no",
                "DakDaptoriks.receiving_date",
                "DakDaptoriks.dak_security_level",
                "DakDaptoriks.dak_priority_level",
                "DakDaptoriks.dak_status",
                "DakMovements.operation_type",
                "DakMovements.created"
            ])
            ->where(['DakMovements.from_office_id' => $selected_office_section['office_id'],
                'DakMovements.from_office_unit_id' => $selected_office_section['office_unit_id']])
            ->join([
                'DakDaptoriks' => [
                    'table' => 'dak_daptoriks',
                    "conditions" => "DakMovements.dak_id = DakDaptoriks.id ",
                    "type" => "INNER"
                ]
            ])
            ->order("DakMovements.modified DESC");

        if (!empty($condition)) {
            $queryString = $queryString->where([$condition]);
        }

        if (!empty($page)) {
            $queryString = $queryString->page($page, $len);
        }
        return $queryString;
    }

    public function getAllSentHardcopyReport($condition)
    {
        $table_dm    = TableRegistry::get('NothiMasters');
        $queryString = $table_dm->find()->select([
                "OfficeUnits.unit_name_bng",
                "NothiMasters.nothi_no",
                "NothiMasters.subject",
                "NothiFlowInfos.next_owner",
                "NothiPotros.sarok_no",
                "NothiPotros.created",
                "NothiPotros.issue_date",
                "NothiPotros.potro_media"
            ])
            //->where(['NothiMasters.office_id'=> $selected_office_section['office_id'],'NothiMasters.office_units_id' =>$selected_office_section['office_unit_id']])
            ->join([
                'OfficeUnits' => [
                    'table' => 'office_units',
                    "conditions" => "NothiMasters.office_units_id = OfficeUnits.id ",
                    "type" => "INNER"
                ],
                'NothiPotros' => [
                    'table' => 'nothi_potros',
                    "conditions" => "NothiMasters.id = NothiPotros.nothi_master_id ",
                    "type" => "INNER"
                ],
                'NothiFlowInfos' => [
                    'table' => 'nothi_flow_infos',
                    "conditions" => "NothiMasters.id = NothiFlowInfos.nothi_master_id ",
                    "type" => "LEFT"
                ]
            ])
            ->order("NothiMasters.modified DESC");

        if (!empty($condition)) {
            $queryString = $queryString->where([$condition]);
        }


        return $queryString;
    }

    public function getAlleFileLibraryRegisterReport($selected_office_section = '', $page = 0,
                                                     $len = 10, $condition)
    {
        $table_dm    = TableRegistry::get('NothiMasters');
        $queryString = $table_dm->find()->select([
                "NothiTypes.type_name",
                "NothiMasters.subject",
                "NothiMasters.nothi_created_date",
                "NothiTypes.type_code"
            ])
            ->where(['NothiMasters.office_id' => $selected_office_section['office_id'],
                'NothiMasters.office_units_id' => $selected_office_section['office_unit_id']])
            ->join([
                'NothiTypes' => [
                    'table' => 'nothi_types',
                    "conditions" => "NothiMasters.nothi_types_id= NothiTypes.id ",
                    "type" => "INNER"
                ]
            ])
            ->order("NothiMasters.modified DESC");

        if (!empty($condition)) {
            $queryString = $queryString->where([$condition]);
        }

        if (!empty($page)) {
            $queryString = $queryString->page($page, $len);
        }
        return $queryString;
    }

    public function getPotrojariRegister($condition)
    {

        $potrojariTable = TableRegistry::get('Potrojari');

        $potrojariRecords = $potrojariTable->find()->select([
                'Potrojari.id',
                'Potrojari.sarok_no',
                'PotrojariReceiver.receiving_office_id',
                'PotrojariReceiver.receiving_office_name',
                'PotrojariReceiver.receiving_office_unit_name',
                'PotrojariReceiver.receiving_officer_designation_label',
                'PotrojariReceiver.receiving_officer_email',
                'PotrojariReceiver.modified',
                'NothiParts.nothi_no'
            ])->join([
                'PotrojariReceiver' => [
                    'table' => 'potrojari_receiver',
                    'type' => 'INNER',
                    'conditions' => 'PotrojariReceiver.potrojari_id=Potrojari.id AND PotrojariReceiver.potro_status = "Sent"'
                ],
                'NothiParts' => [
                    'table' => 'nothi_parts',
                    'type' => 'INNER',
                    'conditions' => 'NothiParts.nothi_masters_id=Potrojari.nothi_master_id AND Potrojari.nothi_part_no = NothiParts.id'
                ]
            ])->where([$condition])->order("Potrojari.created DESC")->toArray();

        return $potrojariRecords;
    }

    public function getNothiDiary($startDate, $endDate, $condition=[])
    {


        $nothiMastersTable = TableRegistry::get('NothiMasters');

        $nothiRecords = $nothiMastersTable->find()->select([
                'NothiMasters.id',
                'NothiMasters.office_units_id',
                'NothiMasters.nothi_no',
                'NothiMasters.subject',
                'NothiMasters.nothi_types_id',
                'NothiMasters.nothi_created_date',
                'NothiMasters.nothi_class',
                'NothiMasters.nothi_no',
                'NothiTypes.type_name',
            ])->join([

                'NothiTypes' => [
                    'table' => 'nothi_types',
                    'type' => 'INNER',
                    'conditions' => ['NothiMasters.nothi_types_id=NothiTypes.id']
                ]
            ])->where([
                'NothiMasters.is_deleted' => 0])
            ->where(function($query) use ($startDate,$endDate){
                return $query->between('date(NothiMasters.nothi_created_date)',$startDate,$endDate);
            })
            ->where($condition)
            ->order(['NothiMasters.nothi_created_date DESC']);

        return $nothiRecords;
    }

    public function officeSummary($officeid = 0)
    {
        set_time_limit(0);
        ini_set('memory_limit', '-1');

        $performanceOfficesTable = TableRegistry::get('PerformanceOffices');
        $condition               = [
            'totalInbox' => 'SUM(inbox)', 'totalOutbox' => 'SUM(outbox)', 'totalNothijato' => 'SUM(nothijat)',
            'totalNothivukto' => 'SUM(nothivukto)', 'totalNisponnoDak' => 'SUM(nisponnodak)',
            'totalSouddog' => 'SUM(selfnote)',
            'totalDaksohoNote' => 'SUM(daksohonote)', 'totalPotrojari' => 'SUM(potrojari)',
            'totalNisponnoNote' => 'SUM(nisponnonote)',
            'totalNisponno' => 'SUM(nisponno)', 'totalONisponno' => 'SUM(onisponno)',
            'totalID' => 'count(id)'
        ];
//          $lastonisponno                   = $performanceOfficesTable->getLastOnisponno($officeid,  []);
//        $result                  = $performanceOfficesTable->getOfficeData($officeid, [])->select($condition)->group(['office_id'])->first();
        $result['totalID'] = 0;
//                        pr($result);
//                        pr($lastonisponno);die;

        $DakMovementsTable            = TableRegistry::get('DakMovements');
        $potrojariTable               = TableRegistry::get('Potrojari');
        $nothiPartTable               = TableRegistry::get('NothiParts');
        $employee_offices_table       = TableRegistry::get('employee_offices');
        $user_login_history_table     = TableRegistry::get('UserLoginHistory');
        TableRegistry::remove('NothiMasterCurrentUsers');
        $NothiMasterCurrentUsersTable = TableRegistry::get('NothiMasterCurrentUsers');
        $NothiDakPotroMapsTable       = TableRegistry::get('NothiDakPotroMaps');
        $today                        = date('Y-m-d');
        $yesterday                    = date('Y-m-d', strtotime($today.' -1 day'));

        /* Total Login */
        $employee_records = $employee_offices_table->getAllEmployeeRecordID($officeid);
        $TotalLogin       = $user_login_history_table->countLogin($employee_records);
        $yesterdayLogin   = $user_login_history_table->countLoginYesterday($employee_records);
        /* Total Login */

        /*         * Inbox Total* */
        $totaltodayinbox = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0, 'Inbox',
            [$today, $today]);

        $totalyesterdayinbox = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0, 'Inbox',
            [$yesterday, $yesterday]);

        if ($result['totalID'] > 0) {
            $totalprevinbox = $result['totalInbox'] + $totaltodayinbox;
        }
        else {
            $totalprevinbox = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0, 'Inbox');
        }

        /*         * Inbox Total* */

        /*         * Outbox Total* */
        $totaltodayoutbox     = 0;
        $totalyesterdayoutbox = 0;
        $totalprevoutbox      = 0;
//        $totaltodayoutbox = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0, 'Outbox',
//            [$today, $today]);
//
//        $totalyesterdayoutbox = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0, 'Outbox',
//            [$yesterday, $yesterday]);
//
//         if($result['totalID'] > 0){
//            $totalprevoutbox = $result['totalOutbox'];
//        }else{
//               $totalprevoutbox = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0, 'Outbox');
//        }

        /*         * Outbox Total* */

        /*         * Nothivukto * */
        $totaltodaynothivukto = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0,
            'NothiVukto', [$today, $today]);

        $totalyesterdaynothivukto = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0,
            'NothiVukto', [$yesterday, $yesterday]);

        if ($result['totalID'] > 0) {
            $totalprevnothivukto = $result['totalNothivukto'] + $totaltodaynothivukto;
        }
        else {
            $totalprevnothivukto = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0,
                'NothiVukto');
        }

        /*         * Nothivukto * */

        /*         * Nothijato * */
        $totaltodaynothijato     = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0,
            'NothiJato', [$today, $today]);
        $totalyesterdaynothijato = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0,
            'NothiJato', [$yesterday, $yesterday]);
        if ($result['totalID'] > 0) {
            $totalprevnothijato = $result['totalNothijato'] + $totaltodaynothijato;
        }
        else {
            $totalprevnothijato = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0,
                'NothiJato');
        }

        /*         * Nothijato * */

        /*         * Potrojari* */
        $totaltodaypotrojari     = $potrojariTable->getNisponnobyPotrojari($officeid, 0, 0,
            [$today, $today]);
        $totalyesterdaypotrojari = $potrojariTable->getNisponnobyPotrojari($officeid, 0, 0,
            [$yesterday, $yesterday]);
        if ($result['totalID'] > 0) {
            $totalprepotrojari = $result['totalPotrojari'] + $totaltodaypotrojari;
        }
        else {
            $totalprepotrojari = $potrojariTable->getNisponnobyPotrojari($officeid, 0, 0);
        }

        /*         * Potrojari* */


        /*         * Self Given Note* */
        $returnSummary['totaltodaysrijitonote']     = $nothiPartTable->selfSrijitoNoteCount($officeid,
            0, 0, [ $today, $today]);
        $returnSummary['totalyesterdaysrijitonote'] = $nothiPartTable->selfSrijitoNoteCount($officeid,
            0, 0, [ $yesterday, $yesterday]);
        if ($result['totalID'] > 0) {
            $returnSummary['totalsrijitonoteall'] = $result['totalSouddog'] + $returnSummary['totaltodaysrijitonote'];
        }
        else {
            $returnSummary['totalsrijitonoteall'] = $nothiPartTable->selfSrijitoNoteCount($officeid,
                0, 0);
        }

        /*         * Self Given Note* */

        /*         * Result* */

        $returnSummary['totallogin']     = $TotalLogin;
        $returnSummary['yesterdayLogin'] = $yesterdayLogin;

        $returnSummary['totaltodayinbox']     = $totaltodayinbox;
        $returnSummary['totalyesterdayinbox'] = $totalyesterdayinbox;
        $returnSummary['totalinboxall']       = $totalprevinbox;

        $returnSummary['totaltodayoutbox']     = $totaltodayoutbox;
        $returnSummary['totalyesterdayoutbox'] = $totalyesterdayoutbox;
        $returnSummary['totaloutboxall']       = $totalprevoutbox;

        $returnSummary['totaltodaynothivukto']     = $totaltodaynothivukto;
        $returnSummary['totalyesterdaynothivukto'] = $totalyesterdaynothivukto;
        $returnSummary['totalnothivuktoall']       = $totalprevnothivukto;

        $returnSummary['totaltodaynothijato']     = $totaltodaynothijato;
        $returnSummary['totalyesterdaynothijato'] = $totalyesterdaynothijato;
        $returnSummary['totalnothijatoall']       = $totalprevnothijato;

        $returnSummary['totaltodaypotrojari']     = $totaltodaypotrojari;
        $returnSummary['totalyesterdaypotrojari'] = $totalyesterdaypotrojari;
        $returnSummary['totalpotrojariall']       = $totalprepotrojari;

//        $returnSummary['totaltodaydaksohonote']     = $NothiDakPotroMapsTable->getDakSohoNote($officeid,
//            0, 0, [$today, $today]);
//        $returnSummary['totalyesterdaydaksohonote'] = $NothiDakPotroMapsTable->getDakSohoNote($officeid,
//            0, 0, [$yesterday, $yesterday]);
//        $returnSummary['totaldaksohonoteall']       = $NothiDakPotroMapsTable->getDakSohoNote($officeid,0,0);
//
        $returnSummary['totaltodaydaksohonote']     = $nothiPartTable->dakSrijitoNoteCount($officeid,
            0, 0, [$today, $today]);
        $returnSummary['totalyesterdaydaksohonote'] = $nothiPartTable->dakSrijitoNoteCount($officeid,
            0, 0, [$yesterday, $yesterday]);
        if ($result['totalID'] > 0) {
            $returnSummary['totaldaksohonoteall'] = $result['totalDaksohoNote'] + $returnSummary['totaltodaydaksohonote'];
        }
        else {
            $returnSummary['totaldaksohonoteall'] = $nothiPartTable->dakSrijitoNoteCount($officeid,
                0, 0);
        }

        $returnSummary['totaltodaynisponnodak'] = $returnSummary['totaltodaynothijato'] + $returnSummary['totaltodaynothivukto']
        /* + $DakMovementsTable->countOnulipiDak($officeid, 0, 0, [ $today, $today]) */;

        $returnSummary['totalyesterdaynisponnodak'] = $returnSummary['totalyesterdaynothijato'] + $returnSummary['totalyesterdaynothivukto']
        /* + $DakMovementsTable->countOnulipiDak($officeid, 0, 0, [ $yesterday, $yesterday]) */;

        $returnSummary['totalnisponnodakall'] = $returnSummary['totalnothijatoall'] + $returnSummary['totalnothivuktoall']
        /* + $DakMovementsTable->countOnulipiDak($officeid, 0, 0) */;

        $returnSummary['totaltodaynisponnonote']     = $NothiMasterCurrentUsersTable->nisponnoNoteCount($officeid,
                0, 0, [$today, $today]) - $returnSummary['totaltodaypotrojari'];
        $returnSummary['totalyesterdaynisponnonote'] = $NothiMasterCurrentUsersTable->nisponnoNoteCount($officeid,
                0, 0, [$yesterday, $yesterday]) - $returnSummary['totalyesterdaypotrojari'];
        if ($result['totalID'] > 0) {
            $returnSummary['totalnisponnonoteall'] = $result['totalNisponnoNote'] + $returnSummary['totaltodaynisponnonote'];
        }
        else {
            $returnSummary['totalnisponnonoteall'] = $NothiMasterCurrentUsersTable->nisponnoNoteCount($officeid,
                    0, 0) - $returnSummary['totalpotrojariall'];
        }

//
        $returnSummary['totaltodayOnisponnodak'] = TableRegistry::get('DakUsers')->getOnisponnoDak($officeid,
            0, 0, $today);

        $returnSummary['totalyesterdayOnisponnodak'] = TableRegistry::get('DakUsers')->getOnisponnoDak($officeid,
            0, 0, $yesterday);

        if ($result['totalID'] > 0) {
            $returnSummary['totalOnisponnodakall'] = $returnSummary['totaltodayOnisponnodak'];
        }
        else {
            $returnSummary['totalOnisponnodakall'] = TableRegistry::get('DakUsers')->getOnisponnoDak($officeid,
                0, 0);
        }


//        $returnSummary['totaltodayOnisponnodak'] = $DakMovementsTable->getOnisponnoDak($officeid,
//            0, 0,$today);
//
//        $returnSummary['totalyesterdayOnisponnodak'] = $DakMovementsTable->getOnisponnoDak($officeid,
//            0, 0, $yesterday);
//
//        $returnSummary['totalOnisponnodakall'] = $DakMovementsTable->getOnisponnoDak($officeid,
//            0, 0,$today);
//        $returnSummary['totalinboxall'] = $returnSummary['totalOnisponnodakall'] + $returnSummary['totalnisponnodakall'];

        if ($returnSummary['totaltodaynisponnonote'] < 0) {
            $returnSummary['totaltodaynisponnonote'] = 0;
        }
        if ($returnSummary['totalyesterdaynisponnonote'] < 0) {
            $returnSummary['totalyesterdaynisponnonote'] = 0;
        }
        if ($returnSummary['totalnisponnonoteall'] < 0) {
            $returnSummary['totalnisponnonoteall'] = 0;
        }
        if ($returnSummary['totaltodayOnisponnodak'] < 0) {
            $returnSummary['totaltodayOnisponnodak'] = 0;
        }
        if ($returnSummary['totalyesterdayOnisponnodak'] < 0) {
            $returnSummary['totalyesterdayOnisponnodak'] = 0;
        }
        if ($returnSummary['totalOnisponnodakall'] < 0) {
            $returnSummary['totalOnisponnodakall'] = 0;
        }

        $returnSummary['totaltodayOnisponnonote']     = $NothiMasterCurrentUsersTable->getOnisponnoNoteCount($officeid,
            0, 0, [$today, $today]);
        $returnSummary['totalyesterdayOnisponnonote'] = $NothiMasterCurrentUsersTable->getOnisponnoNoteCount($officeid,
            0, 0, [$yesterday, $yesterday]);
        if ($result['totalID'] > 0) {
            $returnSummary['totalOnisponnonoteall'] = $returnSummary['totaltodayOnisponnonote'];
        }
        else {
            $returnSummary['totalOnisponnonoteall'] = $NothiMasterCurrentUsersTable->getOnisponnoNoteCount($officeid,
                0, 0);
        }
        $returnSummary['totaltodayOnisponno']     = $returnSummary['totaltodayOnisponnodak'] + $returnSummary['totaltodayOnisponnonote'];
        $returnSummary['totalyesterdayOnisponno'] = $returnSummary['totalyesterdayOnisponnodak'] + $returnSummary['totalyesterdayOnisponnonote'];
        $returnSummary['totalOnisponnoall']       = $returnSummary['totalOnisponnodakall'] + $returnSummary['totalOnisponnonoteall'];

//        $totalsrijitonoteall_real = $returnSummary['totalpotrojariall'] + $returnSummary['totalnisponnonoteall']
//            + $returnSummary['totalOnisponnonoteall'] - $returnSummary['totaldaksohonoteall'];
//
//        if ($totalsrijitonoteall_real < 0) {
//            $returnSummary['totaldaksohonoteall'] += $totalsrijitonoteall_real;
//        }
//        else {
//            $returnSummary['totalsrijitonoteall'] = abs($totalsrijitonoteall_real);
//        }

        $returnSummary['totalyesterdayOnisponnodak'] = $returnSummary['totaltodayOnisponnodak'] - $returnSummary['totaltodayinbox']
            + $returnSummary['totaltodaynisponnodak'];

        $returnSummary['totalyesterdayOnisponnonote'] = $returnSummary['totaltodayOnisponnonote'] - $returnSummary['totaltodaydaksohonote']
            - $returnSummary['totaltodaysrijitonote'] + $returnSummary['totaltodaynisponnonote'] + $returnSummary['totaltodaypotrojari'];

        $returnSummary['totalinboxall']  = $returnSummary['totalOnisponnodakall'] + $returnSummary['totalnisponnodakall'];
        $returnSummary['totaloutboxall'] = $returnSummary['totalinboxall'];

        if ($returnSummary['totalsrijitonoteall'] < 0) {
            $returnSummary['totalsrijitonoteall'] = 0;
        }
        if ($returnSummary['totalyesterdayOnisponnodak'] < 0) {
            $returnSummary['totalyesterdayOnisponnodak'] = 0;
        }
        if ($returnSummary['totalyesterdayOnisponnonote'] < 0) {
            $returnSummary['totalyesterdayOnisponnonote'] = 0;
        }

        return $returnSummary;
    }

    public function officeDateSummary($officeid = 0, $start_date = '', $end_date = '')
    {
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        $DakMovementsTable            = TableRegistry::get('DakMovements');
        $potrojariTable               = TableRegistry::get('Potrojari');
        $nothiPartTable               = TableRegistry::get('NothiParts');
        $employee_offices_table       = TableRegistry::get('employee_offices');
        $user_login_history_table     = TableRegistry::get('UserLoginHistory');
        TableRegistry::remove('NothiMasterCurrentUsers');
        $NothiMasterCurrentUsersTable = TableRegistry::get('NothiMasterCurrentUsers');
        $NothiDakPotroMapsTable       = TableRegistry::get('NothiDakPotroMaps');

        /* Total Login */
        $employee_records = $employee_offices_table->getAllEmployeeRecordID($officeid);
        /* Total Login */

        /*         * Inbox Total* */
        $totaltodayinbox = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0, 'Inbox',
            [$start_date, $end_date]);


        /*         * Nothivukto * */
        $totaltodaynothivukto = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0,
            'NothiVukto', [$start_date, $end_date]);



        /*         * Nothijato * */
        $totaltodaynothijato = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0,
            'NothiJato', [$start_date, $end_date]);


        /*         * Potrojari* */
        $totaltodaypotrojari = $potrojariTable->getNisponnobyPotrojari($officeid, 0, 0,
            [$start_date, $end_date]);

        $returnSummary['totaltodayinbox'] = $totaltodayinbox;


        $returnSummary['totaltodaynothivukto'] = $totaltodaynothivukto;


        $returnSummary['totaltodaynothijato'] = $totaltodaynothijato;

        $returnSummary['totaltodaypotrojari'] = $totaltodaypotrojari;

        $returnSummary['totaltodaydaksohonote'] = $nothiPartTable->dakSrijitoNoteCount($officeid, 0,
            0, [$start_date, $end_date]);

        $returnSummary['totaltodaysrijitonote'] = $nothiPartTable->selfSrijitoNoteCount($officeid,
            0, 0, [ $start_date, $end_date]);

        $returnSummary['totaltodaynisponnonote'] = $NothiMasterCurrentUsersTable->nisponnoNoteCount($officeid,
            0, 0, [$start_date, $end_date]);



        return $returnSummary;
    }

    public function getDakRegisters( $startDate,$endDate,$conditions=[],$order=[] )
    {
        $table_instance_dm  = TableRegistry::get('DakMovements');


        $dak_list = $table_instance_dm->find()
                ->select([
                    'DakDaptoriks.sender_sarok_no',
                    'DakDaptoriks.sender_office_name',
                    'DakDaptoriks.sender_office_unit_name',
                    'DakDaptoriks.sender_officer_designation_label',
                    'DakDaptoriks.sender_name',
                    'DakDaptoriks.dak_received_no',
                    'DakDaptoriks.dak_sending_media',
                    'DakDaptoriks.docketing_no',
                    'DakDaptoriks.dak_subject',
                    'DakDaptoriks.dak_security_level',
                    "daptorikCreated" => 'DakDaptoriks.created',
                    'DakNagoriks.docketing_no',
                    'DakNagoriks.dak_subject',
                    'DakNagoriks.dak_received_no',
                    'DakNagoriks.dak_security_level',
                    'DakNagoriks.sender_name',
                    "nagorikCreated" => 'DakNagoriks.created',
                    'DakMovements.id',
                    'DakMovements.dak_type',
                    'DakMovements.from_office_name',
                    'DakMovements.from_office_unit_id',
                    'DakMovements.from_office_unit_name',
                    'DakMovements.from_officer_name',
                    'DakMovements.from_officer_designation_label',
                    'DakMovements.dak_id',
                    'DakMovements.to_office_name',
                    'DakMovements.to_office_unit_id',
                    'DakMovements.to_office_unit_name',
                    'DakMovements.to_officer_name',
                    'DakMovements.to_officer_designation_label',
                    'DakMovements.operation_type',
                    'DakMovements.sequence',
                    'DakMovements.attention_type',
                    'DakMovements.dak_actions',
                    'DakMovements.created',
                    'DakMovements.is_summary_nothi',
                    'DakMovements.dak_priority',
                ])->join([
                'DakDaptoriks' => [
                    'table' => 'dak_daptoriks',
                    'type' => 'LEFT',
                    'conditions' => ["DakMovements.dak_id = DakDaptoriks.id", "DakMovements.dak_type"=> DAK_DAPTORIK ]
                ],
                'DakNagoriks' => [
                    'table' => 'dak_nagoriks',
                    'type' => 'left',
                    'conditions' => ["DakMovements.dak_id = DakNagoriks.id",  "DakMovements.dak_type" => DAK_NAGORIK ]
                ]
            ]);
            $dak_list->where(function($query) use ($startDate,$endDate){
                return $query->between('date(DakMovements.created)',$startDate,$endDate);
            });
            if(!empty($conditions)){
                $dak_list->where($conditions);
            };

            $dak_list->where(['DakMovements.attention_type'=>1]);

            if(!empty($order)) {
                $dak_list->order($order);
            } else {
                $dak_list->order(['DakMovements.created DESC']);
            }

        return $dak_list;
    }
    public function getDakRegistersFromDumpTable( $startDate,$endDate,$conditions=[],$order=[] )
    {
        $table_instance_dr  = TableRegistry::get('DakRegister');


        $dak_list = $table_instance_dr->find()
                                      ->select([
                                          'DakRegister.sender_sarok_no',
                                          'DakRegister.sender_office_name',
                                          'DakRegister.sender_office_unit_name',
                                          'DakRegister.sender_officer_designation_label',
                                          'DakRegister.sender_name',

                                          'DakRegister.dak_received_no',
                                          'DakRegister.dak_sending_media',
                                          'DakRegister.docketing_no',
                                          'DakRegister.dak_subject',

                                          'DakRegister.dak_security_level',
                                          'DakRegister.movement_time',
                                          'DakRegister.movement_id',
                                          'DakRegister.dak_id',
                                          'DakRegister.dak_type',

                                          'DakRegister.from_office_name',
                                          'DakRegister.from_office_unit_id',
                                          'DakRegister.from_office_unit_name',
                                          'DakRegister.from_officer_name',
                                          'DakRegister.from_officer_designation_label',

                                          'DakRegister.to_office_name',
                                          'DakRegister.to_office_unit_id',
                                          'DakRegister.to_office_unit_name',
                                          'DakRegister.to_officer_name',
                                          'DakRegister.to_officer_designation_label',

                                          'DakRegister.operation_type',
                                          'DakRegister.dak_created',
                                          'DakRegister.attention_type',
                                          'DakRegister.dak_actions',

                                          'DakRegister.created',
                                          'DakRegister.is_summary_nothi',
                                          'DakRegister.dak_priority_level',
                                      ]);
        $dak_list->where(function($query) use ($startDate,$endDate){
            return $query->between('date(DakRegister.movement_time)',$startDate,$endDate);
        });
        if(!empty($conditions)){
            $dak_list->where($conditions);
        };

        $dak_list->where(['DakRegister.attention_type'=>1]);

        if(!empty($order)) {
            $dak_list->order($order);
        } else {
            $dak_list->order(['DakRegister.movement_time DESC']);
        }

        return $dak_list;
    }
//    public function getDakRegisters($conditions = '')
//    {
//
//        $dak_daptorik_table = TableRegistry::get('DakDaptoriks');
//        $dak_nagorik_table  = TableRegistry::get('DakNagoriks');
//        $table_instance_dm  = TableRegistry::get('DakMovements');
//
//
//        $dak_list = $table_instance_dm->find()
//                ->select([
//                    'DakDaptoriks.sender_sarok_no',
//                    'DakDaptoriks.sender_office_name',
//                    'DakDaptoriks.sender_office_unit_name',
//                    'DakDaptoriks.sender_officer_designation_label',
//                    'DakDaptoriks.sender_name',
//                    'DakDaptoriks.dak_received_no',
//                    'DakDaptoriks.dak_sending_media',
//                    'DakDaptoriks.docketing_no',
//                    'DakDaptoriks.dak_subject',
//                    'DakDaptoriks.dak_security_level',
//                    "daptorikCreated" => 'DakDaptoriks.created',
//                    'DakNagoriks.docketing_no',
//                    'DakNagoriks.dak_subject',
//                    'DakNagoriks.dak_received_no',
//                    'DakNagoriks.dak_security_level',
//                    'DakNagoriks.sender_name',
//                    "nagorikCreated" => 'DakNagoriks.created',
//                    'DakMovements.id',
//                    'DakMovements.dak_type',
//                    'DakMovements.from_office_name',
//                    'DakMovements.from_office_unit_id',
//                    'DakMovements.from_office_unit_name',
//                    'DakMovements.from_officer_name',
//                    'DakMovements.from_officer_designation_label',
//                    'DakMovements.dak_id',
//                    'DakMovements.to_office_name',
//                    'DakMovements.to_office_unit_id',
//                    'DakMovements.to_office_unit_name',
//                    'DakMovements.to_officer_name',
//                    'DakMovements.to_officer_designation_label',
//                    'DakMovements.operation_type',
//                    'DakMovements.sequence',
//                    'DakMovements.attention_type',
//                    'DakMovements.dak_actions',
//                    'DakMovements.created',
//                    'DakMovements.is_summary_nothi',
//                    'DakMovements.dak_priority',
//                ])->join([
//                'DakDaptoriks' => [
//                    'table' => 'dak_daptoriks',
//                    'type' => 'left',
//                    'conditions' => "DakMovements.dak_id = DakDaptoriks.id AND DakMovements.dak_type='".DAK_DAPTORIK."'"
//                ],
//                'DakNagoriks' => [
//                    'table' => 'dak_nagoriks',
//                    'type' => 'left',
//                    'conditions' => "DakMovements.dak_id = DakNagoriks.id AND DakMovements.dak_type='".DAK_NAGORIK."'"
//                ]
//            ])->where($conditions)->order(['DakMovements.created DESC']);
//
//        return $dak_list;
//    }

    public function dakinbox($office_id = 0, $unit_id = 0, $designation_id = 0, $condition = [],
                             $group = [])
    {

        $table = TableRegistry::get('DakUsers');
        $query = $table->find()->where(['is_archive' => 0,  'attention_type' => 1]);

        $ff = 0;
        $query->where(['dak_category' => 'Inbox']);
        if (!empty($condition)) {
            $query->where($condition);
        }

        if ($designation_id != 0) {
            $query->where(['to_officer_designation_id' => $designation_id]);
//               
            $ff = 1;
        }
        if ($unit_id != 0) {
            $query->where(['to_office_unit_id' => $unit_id]);
            if ($ff == 0) {
//                     $query->group('to_office_unit_id');
                $ff = 1;
            }
        }
        if ($office_id != 0) {
            $query->where(['to_office_id' => $office_id]);
            if ($ff == 0) {
//                     $query->group('to_office_id');
                $ff = 1;
            }
        }
        // pr ($query);


        if (!empty($group)) {
            $query->group($group);
        }

        return $query->count();
    }

    public function nothiinbox($office_id = 0, $unit_id = 0, $designation_id = 0, $condition = [],
                               $group = [])
    {
        TableRegistry::remove('NothiMasterCurrentUsers');
        $table = TableRegistry::get('NothiMasterCurrentUsers');
        $nothiNotesTable              = TableRegistry::get('NothiNotes');
        $query = $table->find('list',
                ['keyField' => 'nothi_part_no', 'valueField' => 'nothi_part_no'])->where(['is_finished' => 0,'is_archive' => 0]);

        if (!empty($condition)) {
            $query->where($condition);
        }

        if ($designation_id != 0) {
            $query->where(['office_unit_organogram_id' => $designation_id]);
        }


        if ($unit_id != 0) {
            $query->where(['office_unit_id' => $unit_id]);
        }
        if ($office_id != 0) {
            $query->where(['office_id' => $office_id]);
        }

        if (!empty($group)) {
            $query->group($group);
        }
        $allNothi = $query->toArray();
        if (!empty($allNothi)) {
            return $nothiNotesTable->find()->where(['nothi_part_no IN' => $allNothi])->group(['nothi_part_no'])->count();
        }
        return 0;
    }

    public function UnitSummary($unitid = 0, $officeid = 0)
    {

        $DakMovementsTable            = TableRegistry::get('DakMovements');
        $potrojariTable               = TableRegistry::get('Potrojari');
        $nothiPartTable               = TableRegistry::get('NothiParts');
        $employee_offices_table       = TableRegistry::get('employee_offices');
        $user_login_history_table     = TableRegistry::get('UserLoginHistory');
        TableRegistry::remove('NothiMasterCurrentUsers');
        $NothiMasterCurrentUsersTable = TableRegistry::get('NothiMasterCurrentUsers');
        $NothiDakPotroMapsTable       = TableRegistry::get('NothiDakPotroMaps');
        $today                        = date('Y-m-d');
        $yesterday                    = date('Y-m-d', strtotime($today.' -1 day'));


        $performanceUnitsTable = TableRegistry::get('performanceUnits');
        $condition             = [
            'totalInbox' => 'SUM(inbox)', 'totalOutbox' => 'SUM(outbox)', 'totalNothijato' => 'SUM(nothijat)',
            'totalNothivukto' => 'SUM(nothivukto)', 'totalNisponnoDak' => 'SUM(nisponnodak)',
            'totalSouddog' => 'SUM(selfnote)',
            'totalDaksohoNote' => 'SUM(daksohonote)', 'totalPotrojari' => 'SUM(potrojari)',
            'totalNisponnoNote' => 'SUM(nisponnonote)',
            'totalNisponno' => 'SUM(nisponno)', 'totalONisponno' => 'SUM(onisponno)',
            'totalID' => 'count(id)'
        ];
        $result                = $performanceUnitsTable->getUnitData($unitid, [])->select($condition)->group(['unit_id'])->first();
//                        pr($result);die;

        /* Total Login */
        $employee_records = $employee_offices_table->getAllEmployeeRecordID($officeid.$unitid);
        $TotalLogin       = $user_login_history_table->countLogin($employee_records);
        $yesterdayLogin   = $user_login_history_table->countLoginYesterday($employee_records);
        /* Total Login */

        /*         * Inbox Total* */
        $totaltodayinbox = $DakMovementsTable->countAllDakbyType($officeid, $unitid, 0, 'Inbox',
            [$today, $today]);

        $totalyesterdayinbox = $DakMovementsTable->countAllDakbyType($officeid, $unitid, 0, 'Inbox',
            [$yesterday, $yesterday]);

        if ($result['totalID'] > 0) {
            $totalprevinbox = $result['totalInbox'];
        }
        else {
            $totalprevinbox = $DakMovementsTable->countAllDakbyType($officeid, $unitid, 0, 'Inbox');
        }

        /*         * Inbox Total* */

        /*         * Outbox Total* */
        $totaltodayoutbox = $DakMovementsTable->countAllDakbyType($officeid, $unitid, 0, 'Outbox',
            [$today, $today]);

        $totalyesterdayoutbox = $DakMovementsTable->countAllDakbyType($officeid, $unitid, 0,
            'Outbox', [$yesterday, $yesterday]);

        if ($result['totalID'] > 0) {
            $totalprevoutbox = $result['totalOutbox'];
        }
        else {
            $totalprevoutbox = $DakMovementsTable->countAllDakbyType($officeid, $unitid, 0, 'Outbox');
        }

        /*         * Outbox Total* */

        /*         * Nothivukto * */
        $totaltodaynothivukto = $DakMovementsTable->countAllDakbyType($officeid, $unitid, 0,
            'NothiVukto', [$today, $today]);

        $totalyesterdaynothivukto = $DakMovementsTable->countAllDakbyType($officeid, $unitid, 0,
            'NothiVukto', [$yesterday, $yesterday]);

        if ($result['totalID'] > 0) {
            $totalprevnothivukto = $result['totalNothivukto'];
        }
        else {
            $totalprevnothivukto = $DakMovementsTable->countAllDakbyType($officeid, $unitid, 0,
                'NothiVukto');
        }

        /*         * Nothivukto * */

        /*         * Nothijato * */
        $totaltodaynothijato     = $DakMovementsTable->countAllDakbyType($officeid, $unitid, 0,
            'NothiJato', [$today, $today]);
        $totalyesterdaynothijato = $DakMovementsTable->countAllDakbyType($officeid, $unitid, 0,
            'NothiJato', [$yesterday, $yesterday]);

        if ($result['totalID'] > 0) {
            $totalprevnothijato = $result['totalNothijato'];
        }
        else {
            $totalprevnothijato = $DakMovementsTable->countAllDakbyType($officeid, $unitid, 0,
                'NothiJato');
        }

        /*         * Nothijato * */

        /*         * Potrojari* */
        $totaltodaypotrojari     = $potrojariTable->getNisponnobyPotrojari($officeid, $unitid, 0,
            [$today, $today]);
        $totalyesterdaypotrojari = $potrojariTable->getNisponnobyPotrojari($officeid, $unitid, 0,
            [$yesterday, $yesterday]);

        if ($result['totalID'] > 0) {
            $totalprepotrojari = $result['totalPotrojari'];
        }
        else {
            $totalprepotrojari = $potrojariTable->getNisponnobyPotrojari($officeid, $unitid, 0);
        }

        /*         * Potrojari* */


        /*         * Self Given Note* */
        $returnSummary['totaltodaysrijitonote']     = $nothiPartTable->selfSrijitoNoteCount($officeid,
            $unitid, 0, [ $today, $today]);
        $returnSummary['totalyesterdaysrijitonote'] = $nothiPartTable->selfSrijitoNoteCount($officeid,
            $unitid, 0, [ $yesterday, $yesterday]);
        if ($result['totalID'] > 0) {
            $returnSummary['totalsrijitonoteall'] = $result['totalSouddog'];
        }
        else {
            $returnSummary['totalsrijitonoteall'] = $nothiPartTable->selfSrijitoNoteCount($officeid,
                $unitid, 0);
        }

        /*         * Self Given Note* */

        /*         * Result* */

        $returnSummary['totallogin']     = $TotalLogin;
        $returnSummary['yesterdayLogin'] = $yesterdayLogin;

        $returnSummary['totaltodayinbox']     = $totaltodayinbox;
        $returnSummary['totalyesterdayinbox'] = $totalyesterdayinbox;
        $returnSummary['totalinboxall']       = $totalprevinbox;

        $returnSummary['totaltodayoutbox']     = $totaltodayoutbox;
        $returnSummary['totalyesterdayoutbox'] = $totalyesterdayoutbox;
        $returnSummary['totaloutboxall']       = $totalprevoutbox;

        $returnSummary['totaltodaynothivukto']     = $totaltodaynothivukto;
        $returnSummary['totalyesterdaynothivukto'] = $totalyesterdaynothivukto;
        $returnSummary['totalnothivuktoall']       = $totalprevnothivukto;

        $returnSummary['totaltodaynothijato']     = $totaltodaynothijato;
        $returnSummary['totalyesterdaynothijato'] = $totalyesterdaynothijato;
        $returnSummary['totalnothijatoall']       = $totalprevnothijato;

        $returnSummary['totaltodaypotrojari']     = $totaltodaypotrojari;
        $returnSummary['totalyesterdaypotrojari'] = $totalyesterdaypotrojari;
        $returnSummary['totalpotrojariall']       = $totalprepotrojari;

        $returnSummary['totaltodaydaksohonote']     = $nothiPartTable->dakSrijitoNoteCount
            ($officeid, $unitid, 0, [$today, $today]);
        $returnSummary['totalyesterdaydaksohonote'] = $nothiPartTable->dakSrijitoNoteCount
            ($officeid, $unitid, 0, [$yesterday, $yesterday]);
        if ($result['totalID'] > 0) {
            $returnSummary['totaldaksohonoteall'] = $result['totalDaksohoNote'];
        }
        else {
            $returnSummary['totaldaksohonoteall'] = $nothiPartTable->dakSrijitoNoteCount
                ($officeid, $unitid, 0);
        }


        $returnSummary['totaltodaynisponnodak'] = $returnSummary['totaltodayoutbox'] + $returnSummary['totaltodaynothijato']
            + $returnSummary['totaltodaynothivukto'] + $DakMovementsTable->countOnulipiDak($officeid,
                $unitid, 0, [ $today, $today]);

        $returnSummary['totalyesterdaynisponnodak'] = $returnSummary['totalyesterdayoutbox'] + $returnSummary['totalyesterdaynothijato']
            + $returnSummary['totalyesterdaynothivukto'] + $DakMovementsTable->countOnulipiDak($officeid,
                $unitid, 0, [ $yesterday, $yesterday]);

        if ($result['totalID'] > 0) {
            $returnSummary['totalnisponnodakall'] = $result['totalNisponnoDak'];
        }
        else {
            $returnSummary['totalnisponnodakall'] = $returnSummary['totaloutboxall'] + $returnSummary['totalnothijatoall']
                + $returnSummary['totalnothivuktoall'] + $DakMovementsTable->countOnulipiDak($officeid,
                    $unitid, 0);
        }


        $returnSummary['totaltodaynisponnonote']     = $NothiMasterCurrentUsersTable->nisponnoNoteCount($officeid,
                $unitid, 0, [$today, $today]) - $returnSummary['totaltodaypotrojari'];
        $returnSummary['totalyesterdaynisponnonote'] = $NothiMasterCurrentUsersTable->nisponnoNoteCount($officeid,
                $unitid, 0, [$yesterday, $yesterday]) - $returnSummary['totalyesterdaypotrojari'];

        if ($result['totalID'] > 0) {
            $returnSummary['totalnisponnonoteall'] = $result['totalNisponnoNote'];
        }
        else {
            $returnSummary['totalnisponnonoteall'] = $NothiMasterCurrentUsersTable->nisponnoNoteCount($officeid,
                    $unitid, 0) - $returnSummary['totalpotrojariall'];
        }


        $returnSummary['totaltodayOnisponnodak'] = TableRegistry::get('DakUsers')->getOnisponnoDak($officeid,
            $unitid, 0);

        $returnSummary['totalyesterdayOnisponnodak'] = TableRegistry::get('DakUsers')->getOnisponnoDak($officeid,
            $unitid, 0, $yesterday);

        if ($result['totalID'] > 0) {
            $returnSummary['totalOnisponnodakall'] = $returnSummary['totaltodayOnisponnodak'];
        }
        else {
               $returnSummary['totalOnisponnodakall'] = TableRegistry::get('DakUsers')->getOnisponnoDak($officeid,
            $unitid, 0);

        }
    
        if ($returnSummary['totaltodayOnisponnodak'] < 0) {
            $returnSummary['totaltodayOnisponnodak'] = 0;
        }
        if ($returnSummary['totalyesterdayOnisponnodak'] < 0) {
            $returnSummary['totalyesterdayOnisponnodak'] = 0;
        }
        if ($returnSummary['totalOnisponnodakall'] < 0) {
            $returnSummary['totalOnisponnodakall'] = 0;
        }
        if ($returnSummary['totaltodaynisponnonote'] < 0) {
            $returnSummary['totaltodaynisponnonote'] = 0;
        }
        if ($returnSummary['totalyesterdaynisponnonote'] < 0) {
            $returnSummary['totalyesterdaynisponnonote'] = 0;
        }
        if ($returnSummary['totalnisponnonoteall'] < 0) {
            $returnSummary['totalnisponnonoteall'] = 0;
        }

        $returnSummary['totaltodayOnisponnonote']     = $NothiMasterCurrentUsersTable->getOnisponnoNoteCount($officeid,
            $unitid, 0, [$today, $today]);
        $returnSummary['totalyesterdayOnisponnonote'] = $NothiMasterCurrentUsersTable->getOnisponnoNoteCount($officeid,
            $unitid, 0, [$yesterday, $yesterday]);

            if ($result['totalID'] > 0) {
            $returnSummary['totalOnisponnonoteall'] = $returnSummary['totaltodayOnisponnonote'];
        }
        else {
               $returnSummary['totalOnisponnonoteall']       = $NothiMasterCurrentUsersTable->getOnisponnoNoteCount($officeid,
            $unitid, 0);
        }
       

        $returnSummary['totaltodayOnisponno']     = $returnSummary['totaltodayOnisponnodak'] + $returnSummary['totaltodayOnisponnonote'];
        $returnSummary['totalyesterdayOnisponno'] = $returnSummary['totalyesterdayOnisponnodak'] + $returnSummary['totalyesterdayOnisponnonote'];
        $returnSummary['totalOnisponnoall']       = $returnSummary['totalOnisponnodakall'] + $returnSummary['totalOnisponnonoteall'];

        $totalsrijitonoteall_real = $returnSummary['totalpotrojariall'] + $returnSummary['totalnisponnonoteall']
            + $returnSummary['totalOnisponnonoteall'] - $returnSummary['totaldaksohonoteall'];

        if ($totalsrijitonoteall_real < 0) {
            $returnSummary['totaldaksohonoteall'] += $totalsrijitonoteall_real;
        }
        else {
            $returnSummary['totalsrijitonoteall'] = abs($totalsrijitonoteall_real);
        }

        $returnSummary['totalyesterdayOnisponnodak'] = $returnSummary['totaltodayOnisponnodak'] - $returnSummary['totaltodayinbox']
            + $returnSummary['totaltodaynisponnodak'];

        $returnSummary['totalyesterdayOnisponnonote'] = $returnSummary['totaltodayOnisponnonote'] - $returnSummary['totaltodaydaksohonote']
            - $returnSummary['totaltodaysrijitonote'] + $returnSummary['totaltodaynisponnonote'] + $returnSummary['totaltodaypotrojari'];

        $returnSummary['totalinboxall'] = $returnSummary['totalOnisponnodakall'] + $returnSummary['totalnisponnodakall'];

        if ($returnSummary['totalsrijitonoteall'] < 0) {
            $returnSummary['totalsrijitonoteall'] =0;
        }
        if ($returnSummary['totalyesterdayOnisponnodak'] < 0) {
            $returnSummary['totalyesterdayOnisponnodak'] = 0;
        }
        if ($returnSummary['totalyesterdayOnisponnonote'] < 0) {
            $returnSummary['totalyesterdayOnisponnonote'] = 0;
        }

        return $returnSummary;
    }

    public function DesignationSummary($organogramid = 0)
    {
        $DakMovementsTable            = TableRegistry::get('DakMovements');
        $potrojariTable               = TableRegistry::get('Potrojari');
        $nothiPartTable               = TableRegistry::get('NothiParts');
        $employee_offices_table       = TableRegistry::get('employee_offices');
        TableRegistry::remove('NothiMasterCurrentUsers');
        $NothiMasterCurrentUsersTable = TableRegistry::get('NothiMasterCurrentUsers');
        $NothiDakPotroMapsTable       = TableRegistry::get('NothiDakPotroMaps');
        $today                        = date('Y-m-d');
        $yesterday                    = date('Y-m-d', strtotime($today.' -1 day'));

        $employee_info = $employee_offices_table->getDesignationInfo($organogramid);
        $officeid      = $employee_info['office_id'];
        $unitid        = $employee_info['office_unit_id'];

        /*         * Inbox Total* */
        $totaltodayinbox = $DakMovementsTable->countAllDakbyType($officeid, $unitid, $organogramid,
            'Inbox', [$today, $today]);

        $totalyesterdayinbox = $DakMovementsTable->countAllDakbyType($officeid, $unitid,
            $organogramid, 'Inbox', [$yesterday, $yesterday]);

        $totalprevinbox = $DakMovementsTable->countAllDakbyType($officeid, $unitid, $organogramid,
            'Inbox');
        /*         * Inbox Total* */

        /*         * Outbox Total* */
        $totaltodayoutbox = $DakMovementsTable->countAllDakbyType($officeid, $unitid, $organogramid,
            'Outbox', [$today, $today]);

        $totalyesterdayoutbox = $DakMovementsTable->countAllDakbyType($officeid, $unitid,
            $organogramid, 'Outbox', [$yesterday, $yesterday]);

        $totalprevoutbox = $DakMovementsTable->countAllDakbyType($officeid, $unitid, $organogramid,
            'Outbox');
        /*         * Outbox Total* */

        /*         * Nothivukto * */
        $totaltodaynothivukto = $DakMovementsTable->countAllDakbyType($officeid, $unitid,
            $organogramid, 'NothiVukto', [$today, $today]);

        $totalyesterdaynothivukto = $DakMovementsTable->countAllDakbyType($officeid, $unitid,
            $organogramid, 'NothiVukto', [$yesterday, $yesterday]);

        $totalprevnothivukto = $DakMovementsTable->countAllDakbyType($officeid, $unitid,
            $organogramid, 'NothiVukto');
        /*         * Nothivukto * */

        /*         * Nothijato * */
        $totaltodaynothijato     = $DakMovementsTable->countAllDakbyType($officeid, $unitid,
            $organogramid, 'NothiJato', [$today, $today]);
        $totalyesterdaynothijato = $DakMovementsTable->countAllDakbyType($officeid, $unitid,
            $organogramid, 'NothiJato', [$yesterday, $yesterday]);
        $totalprevnothijato      = $DakMovementsTable->countAllDakbyType($officeid, $unitid,
            $organogramid, 'NothiJato');
        /*         * Nothijato * */

        /*         * Potrojari* */
        $totaltodaypotrojari     = $potrojariTable->getNisponnobyPotrojari($officeid, $unitid,
            $organogramid, [$today, $today]);
        $totalyesterdaypotrojari = $potrojariTable->getNisponnobyPotrojari($officeid, $unitid,
            $organogramid, [$yesterday, $yesterday]);
        $totalprepotrojari       = $potrojariTable->getNisponnobyPotrojari($officeid, $unitid,
            $organogramid);
        /*         * Potrojari* */


        /*         * Self Given Note* */
        $returnSummary['totaltodaysrijitonote']     = $nothiPartTable->selfSrijitoNoteCount($officeid,
            $unitid, $organogramid, [ $today, $today]);
        $returnSummary['totalyesterdaysrijitonote'] = $nothiPartTable->selfSrijitoNoteCount($officeid,
            $unitid, $organogramid, [ $yesterday, $yesterday]);
        $returnSummary['totalsrijitonoteall']       = $nothiPartTable->selfSrijitoNoteCount($officeid,
            $unitid, $organogramid);
        /*         * Self Given Note* */

        /*         * Result* */


        $returnSummary['totaltodayinbox']     = $totaltodayinbox;
        $returnSummary['totalyesterdayinbox'] = $totalyesterdayinbox;
        $returnSummary['totalinboxall']       = $totalprevinbox;

        $returnSummary['totaltodayoutbox']     = $totaltodayoutbox;
        $returnSummary['totalyesterdayoutbox'] = $totalyesterdayoutbox;
        $returnSummary['totaloutboxall']       = $totalprevoutbox;

        $returnSummary['totaltodaynothivukto']     = $totaltodaynothivukto;
        $returnSummary['totalyesterdaynothivukto'] = $totalyesterdaynothivukto;
        $returnSummary['totalnothivuktoall']       = $totalprevnothivukto;

        $returnSummary['totaltodaynothijato']     = $totaltodaynothijato;
        $returnSummary['totalyesterdaynothijato'] = $totalyesterdaynothijato;
        $returnSummary['totalnothijatoall']       = $totalprevnothijato;

        $returnSummary['totaltodaypotrojari']     = $totaltodaypotrojari;
        $returnSummary['totalyesterdaypotrojari'] = $totalyesterdaypotrojari;
        $returnSummary['totalpotrojariall']       = $totalprepotrojari;

        $returnSummary['totaltodaydaksohonote']     = $nothiPartTable->dakSrijitoNoteCount
            ($officeid, $unitid, $organogramid, [$today, $today]);
        $returnSummary['totalyesterdaydaksohonote'] = $nothiPartTable->dakSrijitoNoteCount
            ($officeid, $unitid, $organogramid, [$yesterday, $yesterday]);
        $returnSummary['totaldaksohonoteall']       = $nothiPartTable->dakSrijitoNoteCount
            ($officeid, $unitid, $organogramid);

        $returnSummary['totaltodaynisponnodak'] = $returnSummary['totaltodayoutbox'] + $returnSummary['totaltodaynothijato']
            + $returnSummary['totaltodaynothivukto'] + $DakMovementsTable->countOnulipiDak($officeid,
                $unitid, $organogramid, [ $today, $today]);

        $returnSummary['totalyesterdaynisponnodak'] = $returnSummary['totalyesterdayoutbox'] + $returnSummary['totalyesterdaynothijato']
            + $returnSummary['totalyesterdaynothivukto'] + $DakMovementsTable->countOnulipiDak($officeid,
                $unitid, $organogramid, [ $yesterday, $yesterday]);

        $returnSummary['totalnisponnodakall'] = $returnSummary['totaloutboxall'] + $returnSummary['totalnothijatoall']
            + $returnSummary['totalnothivuktoall'] + $DakMovementsTable->countOnulipiDak($officeid,
                $unitid, $organogramid);

        $returnSummary['totaltodaynisponnonote']     = $NothiMasterCurrentUsersTable->nisponnoNoteCount($officeid,
                $unitid, $organogramid, [$today, $today]) - $returnSummary['totaltodaypotrojari'];
        $returnSummary['totalyesterdaynisponnonote'] = $NothiMasterCurrentUsersTable->nisponnoNoteCount($officeid,
                $unitid, $organogramid, [$yesterday, $yesterday]) - $returnSummary['totalyesterdaypotrojari'];
        $returnSummary['totalnisponnonoteall']       = $NothiMasterCurrentUsersTable->nisponnoNoteCount($officeid,
                $unitid, $organogramid) - $returnSummary['totalpotrojariall'];

//        $returnSummary['totaltodayOnisponnodak'] = $returnSummary['totaltodayinbox'] - $returnSummary['totaltodaynisponnodak'];
        TableRegistry::remove('DakUsers');
        $returnSummary['totaltodayOnisponnodak'] = TableRegistry::get('DakUsers')->getOnisponnoDak($officeid,
            $unitid, $organogramid);

        $returnSummary['totalyesterdayOnisponnodak'] = TableRegistry::get('DakUsers')->getOnisponnoDak($officeid,
            $unitid, $organogramid, $yesterday);

        $returnSummary['totalOnisponnodakall'] = TableRegistry::get('DakUsers')->getOnisponnoDak($officeid,
            $unitid, $organogramid);

        if ($returnSummary['totaltodayOnisponnodak'] < 0) {
            $returnSummary['totaltodayOnisponnodak'] = 0;
        }
        if ($returnSummary['totalyesterdayOnisponnodak'] < 0) {
            $returnSummary['totalyesterdayOnisponnodak'] = 0;
        }
        if ($returnSummary['totalOnisponnodakall'] < 0) {
            $returnSummary['totalOnisponnodakall'] = 0;
        }
        if ($returnSummary['totaltodaynisponnonote'] < 0) {
            $returnSummary['totaltodaynisponnonote'] = 0;
        }
        if ($returnSummary['totalyesterdaynisponnonote'] < 0) {
            $returnSummary['totalyesterdaynisponnonote'] = 0;
        }
        if ($returnSummary['totalnisponnonoteall'] < 0) {
            $returnSummary['totalnisponnonoteall'] = 0;
        }
        $returnSummary['totalinboxall'] = $returnSummary['totalOnisponnodakall'] + $returnSummary['totalnisponnodakall'];

        $returnSummary['totaltodayOnisponnonote']     = $NothiMasterCurrentUsersTable->getOnisponnoNoteCount($officeid,
            $unitid, $organogramid, [$today, $today]);
        $returnSummary['totalyesterdayOnisponnonote'] = $NothiMasterCurrentUsersTable->getOnisponnoNoteCount($officeid,
            $unitid, $organogramid, [$yesterday, $yesterday]);
        $returnSummary['totalOnisponnonoteall']       = $NothiMasterCurrentUsersTable->getOnisponnoNoteCount($officeid,
            $unitid, $organogramid);

        $returnSummary['totaltodayOnisponno']     = $returnSummary['totaltodayOnisponnodak'] + $returnSummary['totaltodayOnisponnonote'];
        $returnSummary['totalyesterdayOnisponno'] = $returnSummary['totalyesterdayOnisponnodak'] + $returnSummary['totalyesterdayOnisponnonote'];
        $returnSummary['totalOnisponnoall']       = $returnSummary['totalOnisponnodakall'] + $returnSummary['totalOnisponnonoteall'];

        $returnSummary['totalsrijitonoteall'] = $returnSummary['totalpotrojariall'] + $returnSummary['totalnisponnonoteall']
            + $returnSummary['totalOnisponnonoteall'] - $returnSummary['totaldaksohonoteall'];

        $returnSummary['totalyesterdayOnisponnodak'] = $returnSummary['totaltodayOnisponnodak'] - $returnSummary['totaltodayinbox']
            + $returnSummary['totaltodaynisponnodak'];

        $returnSummary['totalyesterdayOnisponnonote'] = $returnSummary['totaltodayOnisponnonote'] - $returnSummary['totaltodaydaksohonote']
            - $returnSummary['totaltodaysrijitonote'] + $returnSummary['totaltodaynisponnonote'] + $returnSummary['totaltodaypotrojari'];

        $returnSummary['totalinboxall'] = $returnSummary['totalOnisponnodakall'] + $returnSummary['totalnisponnodakall'];

        if ($returnSummary['totalsrijitonoteall'] < 0) {
            $returnSummary['totalsrijitonoteall'] = $returnSummary['totalsrijitonoteall'] * (-1);
        }
        if ($returnSummary['totalyesterdayOnisponnodak'] < 0) {
            $returnSummary['totalyesterdayOnisponnodak'] = $returnSummary['totalyesterdayOnisponnodak']
                * (-1);
        }
        if ($returnSummary['totalyesterdayOnisponnonote'] < 0) {
            $returnSummary['totalyesterdayOnisponnonote'] = $returnSummary['totalyesterdayOnisponnonote']
                * (-1);
        }

        return $returnSummary;
    }

    public function performanceReport($office_id = 0, $unit_id = 0, $orgranogram_id = 0,
                                      $conditions = [])
    {
        TableRegistry::remove('DakMovements');
        TableRegistry::remove('Potrojari');
        TableRegistry::remove('NothiParts');
        TableRegistry::remove('NothiDakPotroMaps');
        TableRegistry::remove('DakUsers');

        $dakMovmentsTable             = TableRegistry::get('DakMovements');
        $potrojariTable               = TableRegistry::get('Potrojari');
        $employee_offices_table       = TableRegistry::get('EmployeeOffices');
        $DashboardTable = TableRegistry::get('Dashboard');

        $totalEmployee = $employee_offices_table->getAllEmployeeRecordID($office_id, $unit_id,
            $orgranogram_id);


        if (!empty($office_id) && empty($unit_id) && empty($orgranogram_id)) {
            /* Inbox */
            $totalinbox = $dakMovmentsTable->countAllDakforOffice($office_id, $unit_id,
                $orgranogram_id, 'Inbox', [ $conditions['date_start'], $conditions['date_end']]);

            /* Outbox */
            $totaloutbox = $dakMovmentsTable->countAllDakforOffice($office_id, $unit_id,
                $orgranogram_id, 'Outbox', [ $conditions['date_start'], $conditions['date_end']]);

            /*             * Nothivukto * */
            $totalnothivukto = $dakMovmentsTable->countAllDakforOffice($office_id, $unit_id,
                $orgranogram_id, 'Nothivukto', [ $conditions['date_start'], $conditions['date_end']]);


            /*             * Nothijato * */

            $totalnothijato = $dakMovmentsTable->countAllDakforOffice($office_id, $unit_id,
                $orgranogram_id, 'Nothijato', [ $conditions['date_start'], $conditions['date_end']]);
        }
        else {
            $totalinbox = $dakMovmentsTable->countAllDakbyType($office_id, $unit_id,
                $orgranogram_id, 'Inbox', [ $conditions['date_start'], $conditions['date_end']]);

            /* Outbox */
            $totaloutbox = $dakMovmentsTable->countAllDakbyType($office_id, $unit_id,
                $orgranogram_id, 'Outbox', [ $conditions['date_start'], $conditions['date_end']]);

            /*             * Nothivukto * */
            $totalnothivukto = $dakMovmentsTable->countAllDakbyType($office_id, $unit_id,
                $orgranogram_id, 'Nothivukto', [ $conditions['date_start'], $conditions['date_end']]);


            /*             * Nothijato * */

            $totalnothijato = $dakMovmentsTable->countAllDakbyType($office_id, $unit_id,
                $orgranogram_id, 'Nothijato', [ $conditions['date_start'], $conditions['date_end']]);

            /*             * Onulipi * */
            $totalonulipi = $dakMovmentsTable->countOnulipiDak($office_id, $unit_id,
                $orgranogram_id, [ $conditions['date_start'], $conditions['date_end']]);
        }


        /*         * Potrojari* */
        $totalpotrojari   = $potrojariTable->allPotrojariCount($office_id, $unit_id,
            $orgranogram_id, [ $conditions['date_start'], $conditions['date_end']]);

        $nothi_info = $DashboardTable->nothiDashboardCount($office_id,$unit_id,$orgranogram_id,[ $conditions['date_start'], $conditions['date_end']]);

        /*         * Dak Soho Note* */
//       $returnSummary['totalDaksohoNote']  = (($totalnothivukto < $nothi_info['dakNote'])?$totalnothivukto:$nothi_info['dakNote']);
       $returnSummary['totalDaksohoNote']  = $nothi_info['dakNote'];
        /*         * Self note* */
        $returnSummary['totalSouddog']    = (( $nothi_info['selfNote'] < 0)?0:$nothi_info['selfNote']);

          /*         * Nisponno * */
        $returnSummary['totalNisponnoNote']  = $nothi_info['noteNisponno'];

          $returnSummary['totalNisponnoPotrojari'] = $nothi_info['potrojariNisponno'];

            /*         * Onisponno * */
        $returnSummary['totalONisponnoNote'] =$DashboardTable->getOnisponnoNoteCount($office_id,$unit_id,$orgranogram_id,[ $conditions['date_start'], $conditions['date_end']]);


        $returnSummary['totalUser']        = count($totalEmployee);
        $returnSummary['totalInbox']       = $totalinbox;
        $returnSummary['totalOutbox']      = $totaloutbox;
        $returnSummary['totalNothijato']   = $totalnothijato;
        $returnSummary['totalNothivukto']  = $totalnothivukto;
        $returnSummary['totalPotrojari']   = $totalpotrojari;
        if (!empty($office_id) && empty($unit_id) && empty($orgranogram_id)) {
            $returnSummary['totalNisponnoDak'] = (/* $totalonulipi  + */ $returnSummary['totalNothijato']
                + $returnSummary['totalNothivukto']);
        }
        else {
            $returnSummary['totalNisponnoDak'] = ( $returnSummary['totalOutbox'] + $totalonulipi + $returnSummary['totalNothijato']
                + $returnSummary['totalNothivukto']);
        }
        $big_date = ( ($conditions['date_end'] > $conditions['date_start']) ? $conditions['date_end']
                    : $conditions['date_start'] );

        $returnSummary['totalONisponnoDak'] = TableRegistry::get('DakUsers')->getOnisponnoDak($office_id,
            $unit_id, $orgranogram_id, $big_date);
         if (!empty($office_id) && empty($unit_id) && empty($orgranogram_id)) {
               $returnSummary['internal_potrojari'] = TableRegistry::get('NisponnoRecords')->generatePotrojariNisponnoInternalExternalCount($office_id,0,0,[$conditions['date_start'], $conditions['date_end']]);
            $returnSummary['external_potrojari'] = $returnSummary['totalNisponnoPotrojari'] - $returnSummary['internal_potrojari'];
             $unassignedEmployees = $employee_offices_table->getUnassingedEmolyeesDesignation($office_id);
             $returnSummary['unassignedDesignation'] = count($unassignedEmployees);
             if($returnSummary['unassignedDesignation']  == 0){
                 $returnSummary['unassignedPendingDak'] = $returnSummary['unassignedPendingNote'] = 0;
             }else{
                  $returnSummary['unassignedPendingDak'] = TableRegistry::get('DakUsers')->getOnisponnoDakByMultipleDesignations($unassignedEmployees, $big_date);
                $returnSummary['unassignedPendingNote'] =$DashboardTable->getOnisponnoNoteCountByMultipleDesignations($unassignedEmployees,[ $conditions['date_start'], $conditions['date_end']]);
             }
             
         }

        if ($returnSummary['totalONisponnoDak'] < 0) {
            $returnSummary['totalONisponnoDak'] = 0;
        }
        if ($returnSummary['totalNisponnoNote'] < 0) {
            $returnSummary['totalNisponnoNote'] = 0;
        }

//        $returnSummary['totalInbox'] = $returnSummary['totalNisponnoDak'] + $returnSummary['totalONisponnoDak'];

        $returnSummary['totalONisponno'] = $returnSummary['totalONisponnoDak'] + $returnSummary['totalONisponnoNote'];

//        $returnSummary['totalSouddog'] = $returnSummary['totalPotrojari'] + $returnSummary['totalNisponnoNote']
//            + $returnSummary['totalONisponnoNote'] - $returnSummary['totalDaksohoNote'];

//        $returnSummary['totalInbox']  = $returnSummary['totalOutbox'] = $returnSummary['totalONisponnoDak']
//            + $returnSummary['totalNisponnoDak'];
        if ($returnSummary['totalSouddog'] < 0) {
            $returnSummary['totalSouddog'] = 0;
        }
        if ($returnSummary['totalInbox'] < 0) {
            $returnSummary['totalInbox'] = 0;
        }

        $data[] = $returnSummary;
        return $data;
    }

    public function onisponnoDakCountforOffices($office_id = 0, $unit_id = 0, $time = [])
    {
        $result                 = $inbox                  = $outbox                 = $onulipi                = $nothijat
            = 0;
        $NothiDakPotroMaps      = TableRegistry::get('NothiDakPotroMaps');
        $DakMovementsTable      = TableRegistry::get('DakMovements');
        $inbox                  = $DakMovementsTable->countAllDakbyType($office_id, $unit_id, 0,
            'Inbox', $time);
        $outbox                 = $DakMovementsTable->countAllDakbyType($office_id, $unit_id, 0,
            'Outbox', $time);
        $nothijat               = $DakMovementsTable->countAllDakbyType($office_id, $unit_id, 0,
            'Nothijat', $time);
        $onulipi                = $DakMovementsTable->countOnulipiDak($office_id, $unit_id, 0,
            'Nothijat', $time);
        $allPotrojariAgainstDak = $NothiDakPotroMaps->getAllNisponnoDakforOffices($office_id,
            $unit_id, $time);

        $result = ($inbox) - ( $outbox + $nothijat + $onulipi + $allPotrojariAgainstDak);
        return $result;
    }

    public function onisponnoNoteCountforOffices($office_id = 0, $unit_id = 0, $time = [])
    {
        $NothiParts      = TableRegistry::get('NothiParts');
        $SrijitoNoteList = $NothiParts->SrijitoNoteList($office_id, $unit_id, 0, $time);

        $nisponnonote     = TableRegistry::get('NothiMasterCurrentUsers')->checkNisponnobyNothiPatrs($office_id,
            $unit_id, 0, $time, $SrijitoNoteList);
        $countSrijitonote = count($SrijitoNoteList);

        $result = ($countSrijitonote) - ( $nisponnonote);
        return $result;
    }

    public function nisponnoDakCountforOffices($office_id = 0, $unit_id = 0, $time = [])
    {
        $result                 = 0;
        $NothiDakPotroMapsTable = TableRegistry::get('NothiDakPotroMaps');
        $DakMovementsTable      = TableRegistry::get('DakMovements');
        if (!empty($office_id)) {
            if (!empty($unit_id)) {
                $result += $NothiDakPotroMapsTable->getAllNisponnoDakforOffices($office_id,
                    $unit_id, $time);
                $result += $DakMovementsTable->countOnulipiDak($office_id, $unit_id, 0, $time);
            }
            else {
                $result += $NothiDakPotroMapsTable->getAllNisponnoDakforOffices($office_id, 0, $time);
                $result += $DakMovementsTable->countOnulipiDak($office_id, 0, 0, $time);
            }
        }
        else {
            if (!empty($unit_id)) {
                $result += $NothiDakPotroMapsTable->getAllNisponnoDakforOffices(0, $unit_id, $time);
                $result += $DakMovementsTable->countOnulipiDak(0, $unit_id, 0, $time);
            }
        }
        return $result;
    }

    public function newPerformanceReport($office_id = 0, $unit_id = 0, $orgranogram_id = 0,
                                         $conditions = [])
    {
        TableRegistry::remove('DakMovements');
        TableRegistry::remove('Potrojari');
        TableRegistry::remove('NothiParts');
        TableRegistry::remove('NothiDakPotroMaps');
        TableRegistry::remove('Dashboard');
        TableRegistry::remove('DakUsers');

        $DashboardTable             = TableRegistry::get('Dashboard');
        $dakMovmentsTable             = TableRegistry::get('DakMovements');
        TableRegistry::remove('NothiMasterCurrentUsers');
        $potrojariTable               = TableRegistry::get('Potrojari');
        $employee_offices_table       = TableRegistry::get('EmployeeOffices');

        if(empty($unit_id) && empty($orgranogram_id) && !empty($office_id)){
              $totalEmployee = $employee_offices_table->getCountOfEmployeeOfOffices($office_id);
        }
        else if(!empty($unit_id) && empty($orgranogram_id)){
             $totalEmployee = $employee_offices_table->getCountOfEmployeeOfOfficeUnites($office_id, $unit_id);
        }
        else{
            $totalEmployee = 1;
        }
      

        if (!empty($office_id) && empty($unit_id) && empty($orgranogram_id)) {
            /* Inbox */
            $totalinbox = $dakMovmentsTable->countAllDakforOffice($office_id, $unit_id,
                $orgranogram_id, 'Inbox', [ $conditions['date_start'], $conditions['date_end']]);

            /* Outbox */
            $totaloutbox = $dakMovmentsTable->countAllDakforOffice($office_id, $unit_id,
                $orgranogram_id, 'Outbox', [ $conditions['date_start'], $conditions['date_end']]);

            /*             * Nothivukto * */
            $totalnothivukto = $dakMovmentsTable->countAllDakforOffice($office_id, $unit_id,
                $orgranogram_id, 'Nothivukto', [ $conditions['date_start'], $conditions['date_end']]);


            /*             * Nothijato * */

            $totalnothijato = $dakMovmentsTable->countAllDakforOffice($office_id, $unit_id,
                $orgranogram_id, 'Nothijato', [ $conditions['date_start'], $conditions['date_end']]);
        }
        else {
            $totalinbox = $dakMovmentsTable->countAllDakbyType($office_id, $unit_id,
                $orgranogram_id, 'Inbox', [ $conditions['date_start'], $conditions['date_end']]);

            /* Outbox */
            $totaloutbox = $dakMovmentsTable->countAllDakbyType($office_id, $unit_id,
                $orgranogram_id, 'Outbox', [ $conditions['date_start'], $conditions['date_end']]);

            /*             * Nothivukto * */
            $totalnothivukto = $dakMovmentsTable->countAllDakbyType($office_id, $unit_id,
                $orgranogram_id, 'Nothivukto', [ $conditions['date_start'], $conditions['date_end']]);


            /*             * Nothijato * */

            $totalnothijato = $dakMovmentsTable->countAllDakbyType($office_id, $unit_id,
                $orgranogram_id, 'Nothijato', [ $conditions['date_start'], $conditions['date_end']]);

            /*             * Onulipi * */
            $totalonulipi = $dakMovmentsTable->countOnulipiDak($office_id, $unit_id,
                $orgranogram_id, [ $conditions['date_start'], $conditions['date_end']]);
        }


        /*         * Potrojari* */
        $totalpotrojari   = $potrojariTable->allPotrojariCount($office_id, $unit_id,
            $orgranogram_id, [ $conditions['date_start'], $conditions['date_end']]);


        $returnSummary['totalUser']        = $totalEmployee;
        $returnSummary['totalInbox']       = $totalinbox;
        $returnSummary['totalOutbox']      = $totaloutbox;
        $returnSummary['totalNothijato']   = $totalnothijato;
        $returnSummary['totalNothivukto']  = $totalnothivukto;

        /*         * Nisponno Dak* */
        if (!empty($office_id) && empty($unit_id) && empty($orgranogram_id)) {
            $returnSummary['totalNisponnoDak'] = ( /* $totalonulipi  + */ $returnSummary['totalNothijato']
                + $returnSummary['totalNothivukto']);
        }
        else {
            $returnSummary['totalNisponnoDak'] = ($returnSummary['totalOutbox'] + $totalonulipi + $returnSummary['totalNothijato']
                + $returnSummary['totalNothivukto']);
        }
           $big_date = ( ($conditions['date_end'] > $conditions['date_start']) ? $conditions['date_end']
                    : $conditions['date_start'] );


         /*         * Onisponno Dak* */
        $returnSummary['totalONisponnoDak'] = TableRegistry::get('DakUsers')->getOnisponnoDak($office_id,
            $unit_id, $orgranogram_id, $big_date);

          /*         * Nothi Part Start* */

        $nothi_info = $DashboardTable->nothiDashboardCount($office_id,$unit_id,$orgranogram_id,[ $conditions['date_start'], $conditions['date_end']]);

        /*         * Dak Soho Note* */
//       $returnSummary['totalDaksohoNote']  = (($totalnothivukto < $nothi_info['dakNote'])?$totalnothivukto:$nothi_info['dakNote']);
       $returnSummary['totalDaksohoNote']  = $nothi_info['dakNote'];
        /*         * Self note* */
        $returnSummary['totalSouddog']    = (( $nothi_info['selfNote'] < 0)?0:$nothi_info['selfNote']);
        
          /*         * Nisponno * */
        $returnSummary['totalNisponnoNote']  = $nothi_info['noteNisponno'];

          $returnSummary['totalNisponnoPotrojari'] = $nothi_info['potrojariNisponno'];

            /*         * Onisponno * */
        $returnSummary['totalONisponnoNote'] =$DashboardTable->getOnisponnoNoteCount($office_id,$unit_id,$orgranogram_id,[ $conditions['date_start'], $conditions['date_end']]);

        
        $returnSummary['totalPotrojari']   = $totalpotrojari;

        if ($returnSummary['totalONisponnoDak'] < 0) {
            $returnSummary['totalONisponnoDak'] = 0;
        }
        if ($returnSummary['totalNisponnoNote'] < 0) {
            $returnSummary['totalNisponnoNote'] = 0;
        }

//         $returnSummary['totalInbox'] =$returnSummary['totalONisponnoDak']  +$returnSummary['totalNisponnoDak'];

        $returnSummary['totalONisponno'] = $returnSummary['totalONisponnoDak'] + $returnSummary['totalONisponnoNote'];
        $returnSummary['totalNisponno']  = $returnSummary['totalNisponnoDak'] + $returnSummary['totalNisponnoNote'];

//         $returnSummary['totalSouddog']  =$returnSummary['totalPotrojari']  + $returnSummary['totalNisponnoNote'] + $returnSummary['totalONisponnoNote']  -   $returnSummary['totalDaksohoNote'] ;
//         $returnSummary['totalInbox']    = $returnSummary['totalOutbox']   =  $returnSummary['totalONisponnoDak']  + $returnSummary['totalNisponnoDak'];
//              if ($returnSummary['totalSouddog'] < 0) {
//            $returnSummary['totalSouddog'] = $returnSummary['totalSouddog']  * (-1);
//        }
//        if ($returnSummary['totalInbox'] < 0) {
//            $returnSummary['totalInbox'] = $returnSummary['totalInbox']  * (-1);
//        }
          if (!empty($office_id) && empty($unit_id) && empty($orgranogram_id)) {
            $returnSummary['internal_potrojari'] = TableRegistry::get('NisponnoRecords')->generatePotrojariNisponnoInternalExternalCount($office_id,0,0,[$conditions['date_start'], $conditions['date_end']]);
            $returnSummary['external_potrojari'] = $returnSummary['totalNisponnoPotrojari'] - $returnSummary['internal_potrojari'];
             $unassignedEmployees = $employee_offices_table->getUnassingedEmolyeesDesignation($office_id);
             $returnSummary['unassignedDesignation'] = count($unassignedEmployees);
               if($returnSummary['unassignedDesignation']  == 0){
                 $returnSummary['unassignedPendingDak'] = $returnSummary['unassignedPendingNote'] = 0;
             }else{
                  $returnSummary['unassignedPendingDak'] = TableRegistry::get('DakUsers')->getOnisponnoDakByMultipleDesignations($unassignedEmployees, $big_date);
                $returnSummary['unassignedPendingNote'] =$DashboardTable->getOnisponnoNoteCountByMultipleDesignations($unassignedEmployees,[ $conditions['date_start'], $conditions['date_end']]);
             }
         }
        return $returnSummary;
    }

    public function getNothiPreviousChanges($nothi_master_id) {
        $nothi_data_change_history_table = TableRegistry::get('NothiDataChangeHistory');
        $nothi_data_change_histories = $nothi_data_change_history_table->find()->where(['nothi_master_id' => $nothi_master_id]);
        foreach ($nothi_data_change_histories as $nothi_data_change_history) {
            $data = unserialize($nothi_data_change_history->nothi_data);
            $return_data[] = $data['nothi_no'];
        }
        if (isset($return_data)) {
            $return_data = implode(', ', $return_data);
        } else {
            $return_data = '-';
        }
        return $return_data;
    }

    public function getDakDiaryRegister($startDate,$endDate, $conditions=[],$order=[],$distinct=[]){
        $table_instance_dm = TableRegistry::get("DakMovements");

        $query = $table_instance_dm->find()->select([
            'dak_id',
            'dak_type',
            "sender_sarok_no" => 'DakDaptoriks.sender_sarok_no', 'DakMovements.created',
            'sender_name' => 'DakDaptoriks.sender_name',
            'sender_office_unit_name' => 'DakDaptoriks.sender_office_unit_name',
            'sender_officer_designation_label' => 'DakDaptoriks.sender_officer_designation_label',
            'sender_office_name' => 'DakDaptoriks.sender_office_name',
            'dak_subject' => 'DakDaptoriks.dak_subject',
            'NothiParts.nothi_no',
            'NothiPotros.nothi_master_id',
            'NothiPotros.created',
            'NothiPotros.sarok_no',
            'to_office_unit_id' => 'DakMovements.to_office_unit_id',
            'to_office_unit_name' => 'DakMovements.to_office_unit_name'
        ])->join([
                'DakDaptoriks' => [
                    'table' => 'dak_daptoriks',
                    'type' => 'INNER',
                    'conditions' =>[
                        'DakDaptoriks.id = DakMovements.dak_id',
                        'DakMovements.dak_type' =>DAK_DAPTORIK
                    ]
                ],
                'NothiPotros' => [
                    'table' => 'nothi_potros',
                    'type' => 'LEFT',
                    'conditions' => [
                        'NothiPotros.dak_id' => new \Cake\Database\Expression\IdentifierExpression('DakMovements.dak_id'),
                        'NothiPotros.dak_type' => DAK_DAPTORIK
                    ]
                ],
                'NothiParts' => [
                    'table' => 'nothi_parts',
                    'type' => 'LEFT',
                    'conditions' => [
                        'NothiParts.id = NothiPotros.nothi_part_no'
                    ]
                ],
            ])->where(function($query) use ($startDate,$endDate){
                return $query->between('date(DakMovements.created)',$startDate,$endDate);
            })
            ->where($conditions)
            ->order($order)
            ->distinct($distinct);

        return $query;
    }
    public function getDakDiaryRegisterFromDumpTable($startDate,$endDate, $conditions=[],$order=[],$distinct=[]){
        $table_instance_dr = TableRegistry::get("DakRegister");

        $query = $table_instance_dr->find()->select([
            'dak_id',
            'dak_type',
            'movement_time',

            "sender_sarok_no",
            'sender_name',
            'sender_office_unit_name',

            'sender_officer_designation_label',
            'sender_office_name',
            'sender_officer_designation_id',

            'dak_subject',
            'nothi_no' => 'NothiRegisterLists.nothi_no',
            'nothi_master_id' => 'NothiRegisterLists.nothi_master_id',

            'potro_created'=>'movement_time',
            'NothiRegisterLists.nothi_no',
            'DakRegister.to_office_unit_id',

            'DakRegister.to_office_unit_name',
            'dak_created',
        ])->join([
            'NothiRegisterLists' => [
                'table' => 'nothi_register_lists',
                'type' => 'Left',
                'conditions' =>[
                    'DakRegister.nothi_part_id = NothiRegisterLists.nothi_part_no',
                ]
            ]
        ])->where(function($query) use ($startDate,$endDate){
                return $query->between('date(DakRegister.movement_time)',$startDate,$endDate);
            })
            ->where($conditions)
            ->where(['dak_type' => DAK_DAPTORIK])
            ->order($order)
            ->distinct($distinct);

        return $query;
    }
}