<?php
namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

class SarokDumpByOfficeTable extends ProjapotiTable
{
        public function initialize(array $config)
        {
            $conn = ConnectionManager::get('NothiAccessDb');
            $this->connection($conn);
            $this->primaryKey('id');
            $this->addBehavior('Timestamp');
            $this->table('sarok_dump_by_office');
      }
}