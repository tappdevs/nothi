<?php
namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;

class SmsRequestTable extends ProjapotiTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $conn = ConnectionManager::get('projapotiDb');
        $this->connection($conn);
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->table('sms_request');
    }
    public function saveData($mobile,$message){
        $entity = $this->newEntity();
        $entity->cell_number = $mobile;
        $entity->message = $message;
        if($this->save($entity)){
            return true;
        }
        return false;
    }

}

