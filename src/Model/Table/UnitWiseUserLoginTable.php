<?php

namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\Datasource\ConnectionManager;
use Cake\Controller\Component;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

class UnitWiseUserLoginTable extends ProjapotiTable
{

    public function initialize(array $config)
    {
        //parent::initialize($config);
        $conn = ConnectionManager::get('NothiReportsDb');
        $this->connection($conn);
        $this->table('unit_wise_user_login');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
    }

    public function checkUpdate($unit_id = 0, $date = '')
    {
        if (!empty($date) && !empty($unit_id)) {
            return $this->find()->where(['date(created)' => $date, 'unit_id' => $unit_id])->count();
        }
    }
    /**
     *
     * @param int $office_id (can be multiple)
     * @param DateTime $time
     * @return object  CollectionBetweenTwoDates
     */
    public function getOfficeData($office_id = 0, $time = [])
    {
        if (!empty($office_id)) {
            $query =  $this->find()->select(['total_user' => 'SUM(total_user)', 'yesterday_login' => 'SUM(yesterday_login)'])->where(['office_id IN' => $office_id]);
            if (!empty($time)) {
                if (!empty($time[0])) {
                    $query = $query->where(['date(created) >=' => $time[0]]);
                }
                if (!empty($time[1])) {
                    $query = $query->where(['date(created) <=' => $time[1]]);
                }
            }
            return $query;
        }
        return;
    }
    public function getLastUpdateTime($unit_id = 0){
        $query =  $this->find()->select(['modified'])->order(['modified desc']);
        if(!empty($unit_id)){
            $query = $query->where(['unit_id' => $unit_id]);
        }
    return$query->first();
    }
    public function deleteDataofUnits($unit_id = 0, $date =''){
        if(!empty($unit_id)){
            $condition = [
                'unit_id' => $unit_id,
            ];
              if(!empty($date)){
                     $condition = [
                'unit_id' => $unit_id,
                'date(created)' => $date
            ];
            }
            $query = $this->deleteAll([ $condition ]);
            return $query;
        }
       return 0;
    }
    /**
     *
     * @param int $ministry_id
     * @param int $layer_id
     * @param int $origin_id
     * @param array $time
     * @return object
     */
    public function getOtherData($ministry_id = 0 , $layer_id = 0 , $origin_id = 0 , $time = []){
         $query =  $this->find()->select(['total_user' => 'SUM(total_user)', 'yesterday_login' => 'SUM(yesterday_login)']);
         if(!empty($ministry_id)){
             $query->where(['ministry_id' => $ministry_id]);
         }
         if(!empty($layer_id)){
               $query->where(['layer_id' => $layer_id]);
         }
         if(!empty($origin_id)){
               $query->where(['origin_id' => $origin_id]);
         }
         if(!empty($time)){
             if(!empty($time[0])){
                  $query->where(['date(created) >=' => $time[0]]);
             }
              if(!empty($time[1])){
                  $query->where(['date(created) <=' => $time[1]]);
             }
         }
         return $query;
    }
}