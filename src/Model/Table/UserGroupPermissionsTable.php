<?php
namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\Validation\Validator;

/**
 * Articles Model
 */
class UserGroupPermissionsTable extends ProjapotiTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');

        $this->belongsTo('UserTypes');
    }
}
