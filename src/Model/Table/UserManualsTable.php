<?php

namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;

class UserManualsTable extends ProjapotiTable
{

    public function initialize(array $config)
    {
        parent::initialize($config);
        $conn = ConnectionManager::get('projapotiDb');
        $this->connection($conn);
        $this->displayField('title');
        $this->addBehavior('Timestamp');
    }
}