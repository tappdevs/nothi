<?php
namespace App\Model\Table;

use App\Model\Entity\UserSignature;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;

/**
 * UserSignatures Model
 */
class UserSignaturesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $conn = ConnectionManager::get('projapotiDb');
        $this->connection($conn);
        $this->table('user_signatures');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');
            
        $validator
            ->requirePresence('username', 'create')
            ->notEmpty('username');
            
        $validator
            ->requirePresence('signature_file', 'create')
            ->notEmpty('signature_file');
            
        $validator
            ->requirePresence('encode_sign', 'create')
            ->notEmpty('encode_sign');
            
        $validator
            ->allowEmpty('previous_signature');
            
        $validator
            ->allowEmpty('created_by');
            
        $validator
            ->allowEmpty('modified_by');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        return $rules;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'projapotiDb';
    }

    public function getSignature($username, $date = null)
    {
        $query = $this->findByUsername($username);

        if(!empty($date)){
            $query->where(['created <=' => $date]);
        }

        $data = $query->order(['modified desc'])->first();

        if(empty($data)){
            $data = $this->findByUsername($username)->order(['modified asc'])->first();
        }

        return $data;
    }
}
