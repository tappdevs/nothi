<?php

namespace App\Shell;

use App\Controller\ArchiveController;
use Cake\Console\Shell;
use Cake\ORM\TableRegistry;
use App\Controller\ProjapotiController;
use Cake\Datasource\ConnectionManager;
use Cake\I18n\I18n;
use Cake\Network\Email\Email;
use Cake\I18n\Number;
use Cake\I18n\Time;
use Cake\Routing\Route\Route;
use Cake\Routing\Router;
use Exception;
use mikehaertl\wkhtmlto\Pdf;
use Cake\Cache\Cache;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;

/**
 * Report shell command.
 */
class ArchivingShell extends ProjapotiShell
{

    private function commands($office_id, $potrojari_id) {
        //$command = "bin/cake report potrojariReceiverSent {$office_id} {$potrojari_id}> /dev/null 2>&1 &";
    }

    public function archiveAllNothi() {
        try{
            ini_set('memory_limit', -1);
            set_time_limit(0);
            $start_time = strtotime(date('Y-m-d H:i:s'));

            $nothi_archive_requests_table = TableRegistry::get('NothiArchiveRequests');
            $nothi_archive_requests_data = $nothi_archive_requests_table->find()->where(['status' => 1])->toArray();
            $errors = [];

            $total = count($nothi_archive_requests_data);
            if ($total > 0) {
                foreach ($nothi_archive_requests_data as $key => $nothi_archive_data) {
                    // not needed as status = 3 or 4 will happen
                    $nothi_archive_data['status'] = 2;
                    $nothi_archive_requests_table->save($nothi_archive_data);

                    $archive_response = $this->archiveData($nothi_archive_data['office_id'], $nothi_archive_data['nothi_master_id']);

                    if (!empty($archive_response['status'])  && $archive_response['status'] == 'success') {
                        $nothi_archive_data['status'] = 3;
                        $nothi_archive_data['content_status'] = 1;
                        $nothi_archive_requests_table->save($nothi_archive_data);
                        unset($nothi_archive_data['id']);
                    $this->out('Archive Done');
                    } else {

                    $this->out('Archive Failed');
                        if(!empty($archive_response['message'])){
                            $errors[] = "Office Id: " . $nothi_archive_data['office_id ']. ", Master Id: " . $nothi_archive_data['nothi_master_id ']. "  <br> Error: " . $archive_response['message'];
                        }else{
                            $errors[] = "Office Id: " . $nothi_archive_data['office_id ']. ", Master Id: " . $nothi_archive_data['nothi_master_id ']. "  <br> Error: Archive Failed";
                        }
                        $nothi_archive_data['status'] = 4;
                        $nothi_archive_data['comments'] = isset($archive_response['message'])?$archive_response['message']:'';
                        $nothi_archive_requests_table->save($nothi_archive_data);
                    }
                }
            } else {
                $this->out('No data found for archive');
            }
                sleep(5);
                $this->runThisCommand(ROOT.'/bin/cake archiving archivingAttachments > /dev/null 2>&1 &');
        } catch (\Exception $ex){
            $this->out('End-point: archiveAllNothi Error: '.$ex->getMessage());
        }

        $response = "<br>Total archive request handled during this process: " . $total;
        $end_time = strtotime(date('Y-m-d H:i:s'));
        $time_difference = round(abs(($end_time - $start_time) / 60), 2);
        $response .= "<br>Duration of this process: " . $time_difference . " minutes";
        if (!empty($errors)) {
            $response .= '<br>Error occurred: ' . count($errors) . '.<br> Error details:<br> <ul>';
            foreach ($errors as $id) {
                $response .= "<li>" . $id . "</li><br>";
            }
            $response .= "</ul> ";
        } else {
            $response .= "<br><br><b>Archive generation process run successfully. No error occurred</b><br>";
        }
        if (NOTIFICATION_OFF == 0) {
            $params = ['to_email' => 'mhasan.a2i@gmail.com', 'to_cc' => 'nothi.team@gmail.com', 'subject' => "[NNS] Archive Process Summary [Date - " . date('Y-m-d') . "]".(!empty($errors)?(' Errors: '.count($errors)):''), 'email_body' => "Dear Concern,<br> Archive process summary report [Date - " . date('Y-m-d') . "] is ready. Detail of this process has been given below.<br>" . $response . "<br>Thanks,<br>Nothi Team"];
            $this->sendEmailByEmailer($params);
        }

    }
    public function archivingAttachments() {
        try{
            ini_set('memory_limit', -1);
            set_time_limit(0);
            $start_time = strtotime(date('Y-m-d H:i:s'));

            $nothi_archive_attachment_requests_table = TableRegistry::get('NothiArchiveRequests');
            $nothi_archive_requests_data = $nothi_archive_attachment_requests_table->find()->where(['status' => 3,'content_status'=>1])->toArray();
            $errors = [];

            $total = count($nothi_archive_requests_data);
            if ($total > 0) {
                foreach ($nothi_archive_requests_data as $key => $nothi_archive_data) {
                    $nothi_archive_data['content_status'] = 2;
                    $nothi_archive_attachment_requests_table->save($nothi_archive_data);

                    $archive_response = $this->archiveAttachmentContent($nothi_archive_data['office_id'],
                        $nothi_archive_data['nothi_master_id']);
                    if($archive_response['status']=='success')
                    {
                        $nothi_archive_data['content_status'] = 3;
                        $nothi_archive_attachment_requests_table->save($nothi_archive_data);
                        $this->out($archive_response);
                    }else
                    {
                        $this->out('Sorry, there has some thing wrong to copy archive content');
                        if(!empty($archive_response['message'])){
                            $errors[] = "Office Id: " . $nothi_archive_data['office_id ']. ", Master Id: " . $nothi_archive_data['nothi_master_id ']. "  <br> Error: " . $archive_response['message'];
                        }else{
                            $errors[] = "Office Id: " . $nothi_archive_data['office_id ']. ", Master Id: " . $nothi_archive_data['nothi_master_id ']. "  <br> Error: Content Archive Failed";
                        }
                        $nothi_archive_data['content_status'] = 4;
                        $nothi_archive_data['content_comment'] = isset($archive_response['message'])?$archive_response['message']:'';
                        $nothi_archive_attachment_requests_table->save($nothi_archive_data);
                    }
                }
            } else {
                $this->out('No data found for content archive');
            }
        }catch (\Exception $ex){
            $this->out('End-point: archivingAttachments Error: '.$ex->getMessage());

        }

        $response = "<br>Total content archive request handled during this process: " . $total;
        $end_time = strtotime(date('Y-m-d H:i:s'));
        $time_difference = round(abs(($end_time - $start_time) / 60), 2);
        $response .= "<br>Duration of this process: " . $time_difference . " minutes";
        if (!empty($errors)) {
            $response .= '<br>Error occurred: ' . count($errors) . '.<br> Error details:<br> <ul>';
            foreach ($errors as $id) {
                $response .= "<li>" . $id . "</li><br>";
            }
            $response .= "</ul> ";
        } else {
            $response .= "<br><br><b>Content Archive generation process run successfully. No error occurred</b><br>";
        }
        if (NOTIFICATION_OFF == 0) {
            $params = ['to_email' => 'mhasan.a2i@gmail.com', 'to_cc' => 'nothi.team@gmail.com', 'subject' => "[NNS] Content Archive Process Summary [Date - " . date('Y-m-d') . "]".(!empty($errors)?(' Errors: '.count($errors)):''), 'email_body' => "Dear Concern,<br> Archive process summary report [Date - " . date('Y-m-d') . "] is ready. Detail of this process has been given below.<br>" . $response . "<br>Thanks,<br>Nothi Team"];
            $this->sendEmailByEmailer($params);
        }
    }


    public function archiveAttachmentContent($office_id, $nothi_master_id) {
        $connection="";
        try {
            $response = [
                'status' => 'error',
                'message' => ' Something went wrong'
            ];

            $this->switchOffice($office_id, 'OfficeDb');
            $connection = ConnectionManager::get('default');

            $this->copyAttachments("archive_nothi_potro_attachments", $nothi_master_id,$connection);
            $this->copyAttachments("archive_nothi_note_attachments", $nothi_master_id,$connection);
            $this->copyAttachments("archive_nothi_note_attachment_refs", $nothi_master_id,$connection);
            $this->copyDakAttachments($nothi_master_id, $connection);
            //$this->copyPotrojariAttachments( $nothi_master_id, $connection);

            $response = [
                'status' => 'success',
                'message' => ' Thank you!'
            ];

        } catch (\Exception $exception) {
            $connection->rollback();
            $response['message'] = $exception->getMessage() ;
        }

        return $response;
    }


    public function copyAttachments($table_name, $nothi_master_id,$connection)
    {
        try{
            //nothi_potro_attachments
            TableRegistry::remove($table_name);
            $nothi_potro = TableRegistry::get($table_name);
            $nothi_potro_content = $nothi_potro->find()->where(['nothi_master_id'=>$nothi_master_id])->toArray();

            if (!empty($nothi_potro_content)) {
                foreach($nothi_potro_content as $potro){
                    if(!empty($potro['file_name'])){
                        $str_path = $potro['file_name'];
                        $root = FILE_FOLDER_DIR;
                        $file = new File($root.$str_path);
                        $file_folder_path_new = $root.'Archive/'.str_replace($file->name,'',$potro['file_name']);

                        $dir = new Folder($file_folder_path_new);
                        if(isset($dir) && $dir->path)
                        {
                            //$this->out("folder exists");
                        }else
                        {
                            $dir = mkdir($file_folder_path_new,0755,true);
                            $dir = new Folder($file_folder_path_new);
                        }
                        if ($file->exists()) {
                            $file->copy($dir->path . DS . $file->name);
                            unlink($root.$potro['file_name']);
                        }
                        $connection->execute("update $table_name set file_dir = concat(file_dir,'Archive/')  
                                      where id = ".$potro['id']);

                        // $this->out('table name: '.$table_name);
                    }
                }//end foreach

            }//end nothi_potro_attachments
        }catch (\Exception $ex){
            $this->out("End-point: copyAttachments. Table: {$table_name} Master ID: {$nothi_master_id} Error: {$ex->getMessage()}");
        }
    }

    public function copyDakAttachments($nothi_master_id,$connection)
    {
        try{
            //dak_attachments
            TableRegistry::remove('ArchiveNothiDakPotroMaps');

            $nothi_dak_porto_maps = TableRegistry::get('ArchiveNothiDakPotroMaps')->find()->
            where(['nothi_masters_id' => $nothi_master_id])->toArray();
            ### Dak archive start... ##
            $this->out($nothi_master_id);

            if ($nothi_dak_porto_maps) {
                foreach ($nothi_dak_porto_maps as $dak_porto_map) {
                    $dak_type = $dak_porto_map['dak_type'];
                    $dak_id = $dak_porto_map['dak_id'];
                    TableRegistry::remove('archive_dak_attachments');
                    $dak_attachments = TableRegistry::get('archive_dak_attachments')->find()->
                    where(['dak_id' => $dak_id,'dak_type'=>$dak_type,
                           'attachment_type IN '=> ['image/jpeg',
                                                    'application/pdf']])->toArray();

                    if (!empty($dak_attachments)) {
                        foreach($dak_attachments as $dak_att){
                            if(!empty($dak_att['file_name'])){
                                $this->out($dak_att);
                                $str_path = $dak_att['file_name'];
                                $root = FILE_FOLDER_DIR;
                                $file = new File($root.$str_path);
                                $file_folder_path_new = $root.'Archive/'.str_replace($file->name,'',$dak_att['file_name']);

                                $dir = new Folder($file_folder_path_new);
                                if(isset($dir) && $dir->path)
                                {
                                    //$this->out("folder exists");
                                }else
                                {
                                    $dir = mkdir($file_folder_path_new,0755,true);
                                    $dir = new Folder($file_folder_path_new);
                                }
                                if ($file->exists()) {
                                    $file->copy($dir->path . DS . $file->name);
                                }
                                $daks = $connection->execute("update archive_dak_attachments set 
                                file_dir = concat(file_dir,'Archive/') where id = ".$dak_att['id']);
                            }

                        }//end foreach
                    }
                }
            }//end dak attachments
        }catch (\Exception $ex){
            $this->out("End-point: copyDakAttachments.Master ID: {$nothi_master_id} Error: {$ex->getMessage()}");
        }
    }


    public function copyPotrojariAttachments($nothi_master_id, $connection)
    {
        try{
            //potrojari_attachments
            $potrojari_by_nothi_master_id = TableRegistry::get('potrojari')->find()->
            where(['nothi_master_id' => $nothi_master_id])->toArray();
            ### potrojari attachment archive start... ##
            if ($potrojari_by_nothi_master_id) {
                foreach ($potrojari_by_nothi_master_id as $potrojari) {
                    $potrojari_id = $potrojari['id'];

                    $potrojari_attachments = TableRegistry::get('archive_potrojari_attachments')->find()->
                    where(['potrojari_id' => $potrojari_id,
                           'attachment_type IN '=> ['image/jpeg',
                                                    'application/pdf']])->toArray();

                    if (!empty($potrojari_attachments)) {
                        foreach($potrojari_attachments as $potrojari_attach){
                            if(!empty($potrojari_attach['file_name'])){
                                $str_path = $potrojari_attach['file_name'];
                                $root = FILE_FOLDER_DIR;
                                $file = new File($root.$str_path);
                                $file_folder_path_new = $root.'Archive/'.str_replace($file->name,'',$potrojari_attach['file_name']);

                                $dir = new Folder($file_folder_path_new);
                                if(isset($dir) && $dir->path)
                                {
                                    //$this->out("folder exists");
                                }else
                                {
                                    $dir = mkdir($file_folder_path_new,0755,true);
                                    $dir = new Folder($file_folder_path_new);
                                }
                                if ($file->exists()) {
                                    $file->copy($dir->path . DS . $file->name);
                                }
                                $daks = $connection->execute("update archive_potrojari_attachments set 
                                file_dir = concat(file_dir,'Archive/') where 
                                id = ".$potrojari_attach['id'] );
                            }

                        }//end foreach
                        $this->out('potrojari id: '.$potrojari_attach['id']);
                    }
                }
            }//end potrojari attachments
        }catch (\Exception $ex){
            $this->out("End-point: copyPotrojariAttachments.Master ID: {$nothi_master_id} Error: {$ex->getMessage()}");
        }


    }


    public function archiveData($office_id, $nothi_master_id) {
        try {
            $response = [
                'status' => 'error',
                'message' => ' Something went wrong'
            ];

            $this->switchOffice($office_id, 'OfficeDb');
            $connection = ConnectionManager::get('default');
            $table_list['nothi'] = [
                'note_initialize' => 'nothi_masters_id',
                'nothi_dak_potro_maps' => 'nothi_masters_id',
                'nothi_data_change_history' => 'nothi_master_id',
                'nothi_masters_dak_map' => 'nothi_masters_id',
                'nothi_master_current_users' => 'nothi_master_id',
                'nothi_master_movements' => 'nothi_master_id',
                'nothi_master_permissions' => 'nothi_masters_id',
                'nothi_notes' => 'nothi_master_id',
                'nothi_note_attachments' => 'nothi_master_id',
                'nothi_note_attachment_refs' => 'nothi_master_id',
                'nothi_note_permissions' => 'nothi_masters_id',
                'nothi_note_sheets' => 'nothi_master_id',
                'nothi_note_signatures' => 'nothi_master_id',
                'nothi_parts' => 'nothi_masters_id',
                'nothi_potros' => 'nothi_master_id',
                'nothi_potro_attachments' => 'nothi_master_id',
                'potro_flags' => 'nothi_master_id',
            ];
            $table_list['dak'] = [
                'dak_attachments' => 'dak_id',
                'dak_daptoriks' => 'id',
                'dak_movements' => 'dak_id',
                'dak_nagoriks' => 'id',
                'dak_users' => 'dak_id',
                'dak_user_actions' => 'dak_id',
            ];
            $nothi_masters_initiate = TableRegistry::get("NothiMasters")->exists(['id' => $nothi_master_id]);
            if ($nothi_masters_initiate) {
                $nothi_dak_porto_maps = TableRegistry::get('nothi_dak_potro_maps')->find()->where(['nothi_masters_id' => $nothi_master_id])->toArray();

                ### Nothi archive start... ##
                $table_name = 'nothi_masters';
                $queries[] = "CREATE TABLE IF NOT EXISTS `archive_$table_name` LIKE `$table_name`;";
                $queries[] = "INSERT `archive_$table_name` SELECT * FROM `$table_name`  WHERE id = $nothi_master_id;";
                $queries[] = "DELETE FROM `$table_name`  WHERE id = $nothi_master_id;";
                foreach ($table_list['nothi'] as $table_name => $key) {
                    $queries[] = "CREATE TABLE IF NOT EXISTS `archive_$table_name` LIKE `$table_name`;";
                    $queries[] = "INSERT `archive_$table_name` SELECT * FROM `$table_name`  WHERE $key = $nothi_master_id;";
                    $queries[] = "DELETE FROM `$table_name`  WHERE $key = $nothi_master_id;";
                }
                ### Nothi archive end ##

                ### Dak archive start... ##
                $dak_ids = [];
                if ($nothi_dak_porto_maps) {
                    foreach ($nothi_dak_porto_maps as $dak_porto_map) {
                        $dak_ids[$dak_porto_map['dak_type']][] = $dak_porto_map['dak_id'];
                    }
                }
                foreach ($table_list['dak'] as $table_name => $key) {
                    $queries[] = "CREATE TABLE IF NOT EXISTS `archive_$table_name` LIKE `$table_name`;";
                    foreach ($dak_ids as $dak_type => $dak_id_list) {
                        foreach($dak_id_list as $dak_id) {
                            if ($dak_id > 0) {
                                if ($table_name == 'dak_daptoriks' || $table_name == 'dak_nagoriks') {
                                    if (($table_name == 'dak_daptoriks' && $dak_type == 'Daptorik') || ($table_name == 'dak_nagoriks' && $dak_type == 'Nagorik')) {
                                        $queries[] = "INSERT `archive_$table_name` SELECT * FROM `$table_name`  WHERE id = $dak_id;";
                                        $queries[] = "DELETE FROM `$table_name`  WHERE id = $dak_id;";
                                    }
                                } else {
                                    $queries[] = "INSERT `archive_$table_name` SELECT * FROM `$table_name`  WHERE dak_type = '" . $dak_type . "' and dak_id = $dak_id;";
                                    $queries[] = "DELETE FROM `$table_name`  WHERE dak_type = '" . $dak_type . "' and dak_id = $dak_id;";
                                }

                            }
                        }
                    }
                }
                ### Dak archive end ##
                $connection->transactional(function ($connection) use ($queries) {
                    foreach ($queries as $query) {
                        $connection->execute($query);
                    }
                });

                $response = [
                    'status' => 'success',
                    'message' => 'Data updated successfully'
                ];
            }
        } catch (\Exception $exception) {
            $connection->rollback();
            $response['message'] = $exception->getMessage() ;
            $this->out($exception->getMessage());
        }
        return $response;

    }
}