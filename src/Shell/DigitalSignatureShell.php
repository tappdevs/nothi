<?php

namespace App\Shell;

use App\Shell\ProjapotiShell;
use Cake\ORM\TableRegistry;
use App\Controller\ProjapotiController;
use Cake\Datasource\ConnectionManager;
use Cake\I18n\I18n;
use Cake\Network\Email\Email;
use Cake\I18n\Number;
use Cake\I18n\Time;
use Cake\Routing\Route\Route;
use Cake\Routing\Router;
use Exception;
use mikehaertl\wkhtmlto\Pdf;
use Cake\Cache\Cache;
use Cake\Database\Expression\QueryExpression;

/**
 * Report shell command.
 */
class DigitalSignatureShell extends ProjapotiShell
{

    /**
     * main() method.
     *
     * @return bool|int Success or error code.
     */
    public function main()
    {

    }

    private function commands($office_id, $potrojari_id)
    {
        $command = "bin/cake report potrojariReceiverSent {$office_id} {$potrojari_id}> /dev/null 2>&1 &";
    }
     /**
     *
     * @param type $noteIds
     * @param type $soft_token
     * @param type $nothi_part_no
     * @return type
     */
    public function signOnucchedAttachment($keystore = '',$soft_token,$nothi_part_no = 0,$designation_id = 0,$sign_type = 1,$nothi_office = 0){
        if(!empty($designation_id)){
            $employee_office = $this->setCurrentDakSection($designation_id);
        }else{
            return;
        }
        $tbl_CronRequest = TableRegistry::get('CronRequest');
        try{
             if(!empty($employee_office['office_id'])){
                 if(!empty($nothi_office)){
                     $this->switchOffice($nothi_office, 'OfficeDB');
                 }else{
                     $this->switchOffice($employee_office['office_id'], 'OfficeDB');
                 }
            }else{
                return;
            }
            
            $tbl_DS = TableRegistry::get('DigitalSignature');
            TableRegistry::remove('NothiNoteAttachments');
            $tbl_NothiNoteAttachments = TableRegistry::get('NothiNoteAttachments');
            $data = [];
            if(!empty($keystore)){
                $noteIds = Cache::read($keystore, 'memcached');
                if(!empty($noteIds)){
//                    Cache::delete($keystore);
                    $this->out('has Data');
                }else{
                    $this->out('no Data for '.$keystore);
                    return;
                }
                $data['userId'] = $employee_office['cert_id'];
                $data['userIdtype'] = $employee_office['cert_type'];
                $data['caName'] = $employee_office['cert_provider'];
                $data['certSerialNumber'] = $employee_office['cert_serial'];
                $data['passPhrase'] = ($sign_type == 1)? $soft_token: '';
                $signables = [];
                $allNoteAttachments = $tbl_NothiNoteAttachments->getAllAttachments($nothi_part_no,$noteIds,['id','attachment_type','file_name','file_dir'])->toArray();
                if(!empty($allNoteAttachments)){
                     $this->out('has Attachments');
                      foreach($allNoteAttachments as $att_info){
                          if($att_info['attachment_type'] == 'text'){
                               $data_type = 'text';
                          }else if($att_info['attachment_type'] == 'html'){
                              $data_type = 'html';
                          }
                          else if(substr($att_info['attachment_type'],0, 5) == 'image'){
                              $data_type = 'image';
                          }else{
                              $data_type = '';//other Intrinsic file
                          }
                          if(empty($data_type)){
                              $path_parts = pathinfo(FILE_FOLDER_DIR.$att_info['file_name']);
                               $signable = new \IntrinsicSignableObject( 'nothi_part_'.$nothi_part_no.'_attachment_'.$att_info['id'].'_emp_'. $employee_office['officer_id'].'_'.rand(1,1000), $tbl_DS->encodeObject(FILE_FOLDER_DIR.$att_info['file_name'],$data_type),FILE_FOLDER_DIR.$att_info['file_name'] , $path_parts['extension'],null);
                          }else{
                              $signable = new \NonIntrinsicSignableObject( 'nothi_part_'.$nothi_part_no.'_attachment_'.$att_info['id'].'_emp_'. $employee_office['officer_id'].'_'.rand(1,1000), $tbl_DS->encodeObject(FILE_FOLDER_DIR.$att_info['file_name'],$data_type), null);
                          }
                             array_push($signables, $signable);
                    }
                }
                if(!empty($signables)){
                   $response =  $tbl_DS->requestToSign($data,$signables);
                    $this->out($response['status']);
                   if(!empty($response) && $response['status'] == 'success'){
                       $response_data = $response['data'];
                       if(!empty($response_data->signedObjects)){
                           $data_to_save = [];
                           foreach($response_data->signedObjects as $signObj){
                               $trans_info = explode('_', $signObj->transactionId);
                               $id = (!empty($trans_info[4]))?$trans_info[4]:0;
                               if(empty($id))
                                   continue;
                               if(empty($signObj->intrinsicFileName)){
                                   $data_to_save = $signObj;
                                  
                               }
                               $data_to_save->issuer_id = $employee_office['officer_id'];
                               $data_to_save->publicKey = $response_data->publicKey;
                               $file_info = $tbl_NothiNoteAttachments->get($id);
                               $file_info->digital_sign= 1;
                               $file_info->sign_info = json_encode($data_to_save);
                               $tbl_NothiNoteAttachments->save($file_info);
                               if(!empty($signObj->intrinsicFileName)){
                                    $ifp = fopen($signObj->intrinsicFileName, 'wb');
                                    fwrite($ifp, base64_decode($signObj->intrinsicFileContentEncodedAsBase64));
                                    fclose($ifp);
                               }
                           }
                       }
                   }else{
                       print_r($response);
                         $command =ROOT. "/bin/cake DigitalSignature signOnucchedAttachment {$keystore} {$soft_token} {$nothi_part_no} {$designation_id}> /dev/null 2>&1 &";
                        $tbl_CronRequest->saveData($command);
                         exec(ROOT. "/bin/cake Cronjob runCronRequest > /dev/null 2>&1 &");
                        return $response;
                   }
                }
            }
        } catch (\Exception $ex) {
            $this->out($ex->getMessage());
              $command =ROOT. "/bin/cake DigitalSignature signOnucchedAttachment {$keystore} {$soft_token} {$nothi_part_no} {$designation_id}> /dev/null 2>&1 &";
            $tbl_CronRequest->saveData($command);
             exec(ROOT. "/bin/cake Cronjob runCronRequest > /dev/null 2>&1 &");
            return ['status' => 'error','msg' => $ex->getMessage()];
        }
         return ['status' => 'success','msg' => ''];
    }

         /**
     *
     * @param type $noteIds
     * @param type $soft_token
     * @param type $signature_type
     * @return type
     */
    public function signPotrojariAttachment($potro_id,$soft_token,$signature_type = 1,$designation_id = 0){
        if(!empty($designation_id)){
            $employee_office = $this->setCurrentDakSection($designation_id);
        }else{
            return;
        }
        $tbl_CronRequest = TableRegistry::get('CronRequest');
        try{
             if(!empty($employee_office['office_id'])){
                 $this->switchOffice($employee_office['office_id'], 'OfficeDB');
            }else{
                return;
            }

            $tbl_DS = TableRegistry::get('DigitalSignature');
            TableRegistry::remove('PotrojariAttachments');
            $tbl_PotrojariAttachments = TableRegistry::get('PotrojariAttachments');
            $data = [];
            if(!empty($potro_id)){
                $data['userId'] = $employee_office['cert_id'];
                $data['userIdtype'] = $employee_office['cert_type'];
                $data['caName'] = $employee_office['cert_provider'];
                $data['certSerialNumber'] = $employee_office['cert_serial'];
                $data['passPhrase'] = ($signature_type == 1)? $soft_token: '';
                $signables = [];
                $allPotrojariAttachments = $tbl_PotrojariAttachments->getAllAttachments($potro_id,[],['id','attachment_type','file_name','file_dir'])->toArray();
                if(!empty($allPotrojariAttachments)){
                     $this->out('has Attachments');
                      foreach($allPotrojariAttachments as $att_info){
                          if($att_info['attachment_type'] == 'text'){
                               $data_type = 'text';
                          }else if($att_info['attachment_type'] == 'html'){
                              $data_type = 'html';
                          }
                          else if(substr($att_info['attachment_type'],0, 5) == 'image'){
                              $data_type = 'image';
                          }else{
                              $data_type = '';//other Intrinsic file
                          }
                          if(empty($data_type)){
                              $path_parts = pathinfo(FILE_FOLDER_DIR.$att_info['file_name']);
                               $signable = new \IntrinsicSignableObject( 'nothi_potrojari_'.$potro_id.'_attachment_'.$att_info['id'].'_emp_'. $employee_office['officer_id'].'_'.rand(1,1000), $tbl_DS->encodeObject(FILE_FOLDER_DIR.$att_info['file_name'],$data_type),FILE_FOLDER_DIR.$att_info['file_name'] , $path_parts['extension'],null);
                          }else{
                              $signable = new \NonIntrinsicSignableObject( 'nothi_potrojari_'.$potro_id.'_attachment_'.$att_info['id'].'_emp_'. $employee_office['officer_id'].'_'.rand(1,1000), $tbl_DS->encodeObject(FILE_FOLDER_DIR.$att_info['file_name'],$data_type), null);
                          }
                             array_push($signables, $signable);
                    }
                }
                if(!empty($signables)){
                   $response =  $tbl_DS->requestToSign($data,$signables);
                    $this->out($response['status']);
                   if(!empty($response) && $response['status'] == 'success'){
                       $response_data = $response['data'];
                       if(!empty($response_data->signedObjects)){
                           $data_to_save = [];
                           foreach($response_data->signedObjects as $signObj){
                               $trans_info = explode('_', $signObj->transactionId);
                               $id = (!empty($trans_info[4]))?$trans_info[4]:0;
                               if(empty($id))
                                   continue;
                               if(empty($signObj->intrinsicFileName)){
                                   $data_to_save = $signObj;
                               }
                                $data_to_save->issuer_id = $employee_office['officer_id'];
                                $data_to_save->publicKey = $response_data->publicKey;

                               $file_info = $tbl_PotrojariAttachments->get($id);
                               $file_info->digital_sign= 1;
                               $file_info->sign_info = json_encode($data_to_save);
                               $tbl_PotrojariAttachments->save($file_info);
                               if(!empty($signObj->intrinsicFileName)){
                                    $ifp = fopen($signObj->intrinsicFileName, 'wb');
                                    fwrite($ifp, base64_decode($signObj->intrinsicFileContentEncodedAsBase64));
                                    fclose($ifp);
                               }
                           }
                       }
                   }else{
                       print_r($response);
                         $command =ROOT. "/bin/cake DigitalSignature signPotrojariAttachment {$potro_id} {$soft_token} {$signature_type} {$designation_id}> /dev/null 2>&1 &";
                        $tbl_CronRequest->saveData($command);
                         exec(ROOT. "/bin/cake Cronjob runCronRequest > /dev/null 2>&1 &");
                        return $response;
                   }
                }
            }
        } catch (\Exception $ex) {
            $this->out($ex->getMessage());
              $command =ROOT. "/bin/cake DigitalSignature signPotrojariAttachment {$potro_id} {$soft_token} {$signature_type} {$designation_id}> /dev/null 2>&1 &";
            $tbl_CronRequest->saveData($command);
             exec(ROOT. "/bin/cake Cronjob runCronRequest > /dev/null 2>&1 &");
            return ['status' => 'error','msg' => $ex->getMessage()];
        }
         return ['status' => 'success','msg' => ''];
    }
}