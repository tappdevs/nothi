<?php

namespace App\Shell;

use Cake\Console\Shell;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use App\Controller\ProjapotiController;
use Cake\Datasource\ConnectionManager;
use Cake\I18n\I18n;
use Cake\Network\Email\Email;
use Cake\I18n\Number;
use Cake\I18n\Time;
use Cake\Routing\Route\Route;
use Cake\Routing\Router;
use Exception;
use mikehaertl\wkhtmlto\Pdf;
use Cake\Cache\Cache;

/**
 * Report shell command.
 */
class MigrationShell extends ProjapotiShell
{

    /**
     * main() method.
     *
     * @return bool|int Success or error code.
     */
    public function main()
    {

    }

    private function commands($office_id, $potrojari_id)
    {
        $command = "bin/cake report potrojariReceiverSent {$office_id} {$potrojari_id}> /dev/null 2>&1 &";
    }
    public function migrateDakActions(){
        $tableDakActions = TableRegistry::get('DakActions');
        $tableDakActionsMig = TableRegistry::get('DakActionsMig');
        $tableDakActionEmployees = TableRegistry::get('DakActionEmployees');
        $tableEmployeeOffices = TableRegistry::get('EmployeeOffices');
        $tableUsers = TableRegistry::get('Users');
        //get all data for projapoti DB
        try{
                $allDakActions = $tableDakActions->find()->toArray();
                $tableDakActionsMig->deleteAll(['1 = 1']);
                $tableDakActionEmployees->deleteAll(['1 = 1']);
                $total = 0;$success = 0;
                if(!empty($allDakActions)){
                    foreach($allDakActions as $d_action){
                        $total++;
                        //prepare save in dak action mig
                        if(!empty($d_action['organogram_id'])){
                          $emp=  $tableEmployeeOffices->find()->select(['employee_record_id'])->where(['office_unit_organogram_id' => $d_action['organogram_id']])->order(['created desc'])->first();
                          if(empty($emp)){
                              $emp['employee_record_id'] = 0;
                              $user['id'] = 0;
                          }else{
                              $user = $tableUsers->find()->select(['id'])->where(['employee_record_id' => $emp['employee_record_id']])->first();
                          }
                        }else{
                               $emp['employee_record_id'] = 0;
                                $user['id'] = 0;
                        }
                       $tableDakActionsMigEntity = $tableDakActionsMig->newEntity();
                       $tableDakActionsMigEntity->dak_action_name  = $d_action['dak_action_name'];
                       $tableDakActionsMigEntity->status  = $d_action['status'];
                       $tableDakActionsMigEntity->creator  = $emp['employee_record_id'];
                       $tableDakActionsMigEntity->created_by  = $user['id'];
                       $tableDakActionsMigEntity->modified_by  = $user['id'];

                       $tableDakActionEmployeesEntity = $tableDakActionEmployees->newEntity();
                       $tableDakActionEmployeesEntity->employee_id = $emp['employee_record_id'];
                       $tableDakActionEmployeesEntity->created_by = $user['id'];
                       $tableDakActionEmployeesEntity->modified_by = $user['id'];
                       
                       $dak_action_record = $tableDakActionsMig->save($tableDakActionsMigEntity);
                       if($dak_action_record){
                            $tableDakActionEmployeesEntity->dak_action_id = $dak_action_record->id;
                            if($tableDakActionEmployees->save($tableDakActionEmployeesEntity)){
                                 $success++;
                            }
                       }
                    }
                   $this->out('Total: '.$total.' Saved: '.$success);
        }
        } catch (\Exception $ex) {
            $this->out('Error:' . $ex->getMessage());
        }

    }
    public function migrateNothiActions(){
        $tableNothiDecisions = TableRegistry::get('NothiDecisions');
        $tableNothiDecisionsMig = TableRegistry::get('NothiDecisionsMig');
        $tableNothiDecisionEmployees = TableRegistry::get('NothiDecisionEmployees');
        $tableEmployeeOffices = TableRegistry::get('EmployeeOffices');
        $tableUsers = TableRegistry::get('Users');
        //get all data for projapoti DB
        try{
                $allNothiDecisions = $tableNothiDecisions->find()->toArray();
                $tableNothiDecisionsMig->deleteAll(['1 = 1']);
                $tableNothiDecisionEmployees->deleteAll(['1 = 1']);
                $total = 0;$success = 0;
                if(!empty($allNothiDecisions)){
                    foreach($allNothiDecisions as $n_decision){
                        $total++;
                        //prepare save in dak action mig
                        if(!empty($n_decision['organogram_id'])){
                          $emp=  $tableEmployeeOffices->find()->select(['employee_record_id'])->where(['office_unit_organogram_id' => $n_decision['organogram_id']])->order(['created desc'])->first();
                          if(empty($emp)){
                              $emp['employee_record_id'] = 0;
                              $user['id'] = 0;
                          }else{
                              $user = $tableUsers->find()->select(['id'])->where(['employee_record_id' => $emp['employee_record_id']])->first();
                          }
                        }else{
                               $emp['employee_record_id'] = 0;
                                $user['id'] = 0;
                        }
                       $tableNothiDecisionsMigEntity = $tableNothiDecisionsMig->newEntity();
                       $tableNothiDecisionsMigEntity->decisions  = $n_decision['decisions'];
                       $tableNothiDecisionsMigEntity->status  = $n_decision['status'];
                       $tableNothiDecisionsMigEntity->creator  = $emp['employee_record_id'];
                       $tableNothiDecisionsMigEntity->created_by  = $user['id'];
                       $tableNothiDecisionsMigEntity->modified_by  = $user['id'];

                       $tableNothiDecisionEmployeesEntity = $tableNothiDecisionEmployees->newEntity();
                       $tableNothiDecisionEmployeesEntity->employee_id = $emp['employee_record_id'];
                       $tableNothiDecisionEmployeesEntity->created_by = $user['id'];
                       $tableNothiDecisionEmployeesEntity->modified_by = $user['id'];

                       $nothi_decision_record = $tableNothiDecisionsMig->save($tableNothiDecisionsMigEntity);
                       if($nothi_decision_record){
                            $tableNothiDecisionEmployeesEntity->nothi_decision_id = $nothi_decision_record->id;
                            if($tableNothiDecisionEmployees->save($tableNothiDecisionEmployeesEntity)){
                                 $success++;
                            }
                       }
                    }
                   $this->out('Total: '.$total.' Saved: '.$success);
        }
        } catch (\Exception $ex) {
            $this->out('Error:' . $ex->getMessage());
        }

    }
    public function potrojariGroupUsersNameUpdate(){
        ini_set('memory_limit', -1);
        set_time_limit(0);
        $potrojariGroupsMigTable= TableRegistry::get('PotrojariGroupsUsers');
        $employeeRecordsTable = TableRegistry::get('EmployeeRecords');

        try{
            $office_unit_organogram_ids = $potrojariGroupsMigTable->find('list', [
                'keyField' => 'office_unit_organogram_id',
                'valueField' => 'office_unit_organogram_id'
            ])->distinct(['office_unit_organogram_id'])->where(['office_unit_organogram_id <>'=>0])->toArray();

            $employee_records= $employeeRecordsTable->find()->select(['EmployeeRecords.id','EmployeeRecords.name_eng','EmployeeRecords.name_bng','EmployeeRecords.personal_email','office_unit_organogram_id'=>'EmployeeOffices.office_unit_organogram_id'])
                ->join([
                    'EmployeeOffices' => [
                        'table' => 'employee_offices',
                        "conditions" => "EmployeeOffices.employee_record_id= EmployeeRecords.id",
                        "type" => "INNER"
                    ]
                ])
                ->where(['EmployeeOffices.office_unit_organogram_id IN'=>$office_unit_organogram_ids])
                ->where(['EmployeeOffices.status'=>1])
                ->toArray();
            $total_record = 0;
            $updated=0;
            $not_updated=0;
            foreach ($employee_records as $employee_record){
                $total_record++;

                if($potrojariGroupsMigTable->updateAll(['employee_name_eng'=>$employee_record['name_eng'], 'employee_name_bng'=>$employee_record['name_bng'],'officer_email'=>$employee_record['personal_email']],['office_unit_organogram_id'=>$employee_record['office_unit_organogram_id']])){
                    $updated++;
                } else {
                    $not_updated++;
                }
            }
            $this->out(['Total Users: '.$total_record.', Updated: '.$updated.', Not Updated: '.$not_updated]);
        }catch(\Exception $ex){
            $this->out($ex->getMessage());
        }
    }
    public function NothiChangeHistoryJsonToSerialiseMigration(){
        ini_set('memory_limit', -1);
        set_time_limit(0);
        $office_domains = TableRegistry::get('OfficeDomains');
        $all_offices = $office_domains->getOfficesData();
        $total = 0;
        foreach ($all_offices as $office_id => $office_name) {
            if (empty($office_id)) {
                continue;
            }
            try {
                $this->switchOffice($office_id, 'OfficeDB');
                TableRegistry::remove('NothiDataChangeHistory');
                $nothi_data_change_history_table = TableRegistry::get('NothiDataChangeHistory');
                $all_data = $nothi_data_change_history_table->find()->toArray();
                if (empty($all_data)) {
                    continue;
                }

                $total++;
                $i = 0;
                foreach ($all_data as $data) {
                    $nothi_data = serialize(json_decode($data['nothi_data'], true));
                    $nothi_data_change_history_table->updateAll(['nothi_data' => $nothi_data], ['id' => $data['id']]);
                    $i++;
                }
                $this->out(['Office ' . $office_id . ', Total ' . $i . ' rows updated.']);
            } catch (\Exception $ex) {
                $this->out(['Office: ' . $office_id . ' Office name: ' . $office_name . ' msg: ' . $ex->getMessage()]);
                continue;
            }
        }
        $this->out(['Total '.$total.' office updated.']);
    }
    public function addCustomLayerID(){
        $officeTbl = TableRegistry::get('Offices');
        $offices =$officeTbl->find()->select(['id','office_layer_id'])->toArray();
        if(!empty($offices)){
            foreach($offices as $val){
                if(empty($val['office_layer_id'])){
                    $officeTbl->updateAll(['custom_layer_id' => 6],['id' => $val['id']]);
                }else{
                    $officeLayerInfo = TableRegistry::get('OfficeLayers')->getLayerLevel($val['office_layer_id']);
                    if(!empty($officeLayerInfo) && $officeLayerInfo['layer_level']>0 && $officeLayerInfo['layer_level']<6){
                        $officeTbl->updateAll(['custom_layer_id' => $officeLayerInfo['layer_level']],['id' => $val['id']]);
                    }else{
                        $officeTbl->updateAll(['custom_layer_id' => 6],['id' => $val['id']]);
                    }

                }
            }
        }
    }

    public function updatePotrojariGroupUsersOfficeHeadByEmployeeOfficesShowUnit(){
        ini_set('memory_limit', -1);
        set_time_limit(0);

        try {
            $potrojari_groups_users_table = TableRegistry::get('PotrojariGroupsUsers');
            $employee_offices_table =  TableRegistry::get('EmployeeOffices');
            $system_user = 0;
            $updated_user = 0;
            $updated_office_head = 0;
            $showed_unit = 0;
            $not_showed_unit = 0;

            $all_users = $potrojari_groups_users_table->find('all')->distinct(['office_unit_organogram_id'])->hydrate(false)->toArray();
            $total_user = count($all_users);
            foreach ($all_users as $user){
                if(!empty($user['office_unit_organogram_id'])){
                    ++$system_user;
                    $employee_offices_user = $employee_offices_table->find()->where(['office_unit_organogram_id'=>$user['office_unit_organogram_id'],'status'=>1])->first();

                    if($employee_offices_user['office_head']==1){
                        $potrojari_groups_users_table->updateAll(['office_head' => 1], ['office_unit_organogram_id' => $user['office_unit_organogram_id']]);
                        ++$updated_user;
                        ++$updated_office_head;
                    } else {
                        if($employee_offices_user['show_unit']==1){
                            $potrojari_groups_users_table->updateAll(['office_head' => 0], ['office_unit_organogram_id' => $user['office_unit_organogram_id']]);
                            ++$updated_user;
                            ++$showed_unit;
                        } else if($employee_offices_user['show_unit']==0){
                            $potrojari_groups_users_table->updateAll(['office_head' => 1], ['office_unit_organogram_id' => $user['office_unit_organogram_id']]);
                            ++$updated_user;
                            ++$not_showed_unit;
                        }
                    }
                }

            }

        } catch (\Exception $ex) {
            $this->out($ex->getMessage());
        }
        $this->out('all user: '.$total_user.', system user: '.$system_user.', updated user: '.$updated_user.', updated office head: '.$updated_office_head.', unit showed: '.$showed_unit.', unit not showed: '.$not_showed_unit);

    }

    public function OfficeWisePerformanceUnitTablesMigrationUpdate($date = '2018-12-31')
    {
        ini_set('memory_limit', -1);
        set_time_limit(0);

        $tbl_CronRequest = TableRegistry::get('CronRequest');
        $offcieDomainsTable = TableRegistry::get('OfficeDomains');
        $domain_array = $offcieDomainsTable->getUniqueDomainHost();
        $max_length = 0;
        $host = [];
        $offices_ids = [];
        if (!empty($domain_array)) {
            foreach ($domain_array as $DA) {
                $allOfficeIds = $offcieDomainsTable->getHostWiseOfficeID($DA);
                $toalOffices = count($allOfficeIds);
                if ($max_length < $toalOffices) {
                    $max_length = $toalOffices;
                }
                $host[$DA] = array_values($allOfficeIds);
            }
            for ($i = 0; $i < $max_length; $i++) {
                foreach ($domain_array as $DA) {
                    if (isset($host[$DA][$i])) {
                        $offices_ids[] = $host[$DA][$i];
                    }
                }
            }
        }
        if (!empty($offices_ids)) {
            foreach ($offices_ids as $id) {
                $command = ROOT . "/bin/cake migration performanceUnitUpdateByOfficeIDMigration {$id} {$date}> /dev/null 2>&1 &";
                $tbl_CronRequest->saveData($command);
//                $this->performanceUnitUpdateByOfficeIDMigration($id,$date);
            }
        }
        if (!empty($domain_array)) {
            $run_request = count($domain_array) * 2;
            for ($indx = 1; $indx <= $run_request; $indx++) {
                $command = ROOT . "/bin/cake projapoti runCronRequest";
                exec($command);
                sleep(1);
            }
        }
    }

    public function performanceUnitUpdateByOfficeIDMigration($office_id, $date_start = '')
    {
        set_time_limit(0);
        ini_set('memory_limit', -1);
        $this->switchOffice($office_id, 'OfficeDB');


        $unitsTable = TableRegistry::get('OfficeUnits');
        $employee_offices = TableRegistry::get('EmployeeOffices');
        $all_units = $employee_offices->getAllUnitID()->where(['EmployeeOffices.office_id' => $office_id])->toArray();
        $performanceUnitTable = TableRegistry::get('PerformanceUnits');
        $reportsTable = TableRegistry::get('Reports');
        $MinistriesTable = TableRegistry::get('OfficeMinistries');
        $LayersTable = TableRegistry::get('OfficeLayers');
        $OfficesTable = TableRegistry::get('Offices');
        $UnitOriginsTable = TableRegistry::get('OfficeOriginUnits');
        $performance_designationTable = TableRegistry::get('PerformanceDesignations');
        TableRegistry::remove('DakUsers');
        TableRegistry::remove('NothiMasterCurrentUsers');


        foreach ($all_units as $unit_id => $unit_name) {
            $period = [];
            $unit_record = $performanceUnitTable->getLastUpdateTime($unit_id);
            if (!empty($date_start)) {
                $begin = new \DateTime($date_start);
            } else if (!empty($unit_record)) {
                $begin = ($unit_record['record_date']);
            } else {
                $begin = new \DateTime(date('Y-m-d', strtotime(date('Y-m-d') . ' -1 week')));
            }
            if (!empty($date_start)) {
                $end = new \DateTime($date_start);
            }else{
                $end = new \DateTime(date('Y-m-d'));
            }

            for ($i = $begin; $begin <= $end; $i->modify('+1 day')) {
                $period [] = $i->format("Y-m-d");
            }
            if (!empty($period)) {
                try {
                    $unit_related_data = $unitsTable->getAll(['id' => $unit_id],
                        ['office_ministry_id', 'office_layer_id', 'office_origin_unit_id', 'office_id'])->first();
                    foreach ($period as $dt) {
                        $has_record = $performanceUnitTable->find()->where(['record_date' => $dt, 'unit_id' => $unit_id])->count();
                        if ($has_record == 0) {
                            $this->out('No record found.Unit id: '.$unit_id.' For: '.$dt);
                            continue;
                        }
                        $date = $dt;
//                        pr($unit_related_data['office_id']);die;
                        // Collect From Designation Table
                        $from_designation_report = true;
                        $all_designation = $employee_offices->getAllDesignationByOfficeOrUnitID($unit_related_data['office_id'],
                            $unit_id)->toArray();
                        $condition = [
                            'totalInbox' => 'SUM(inbox)', 'totalOutbox' => 'SUM(outbox)', 'totalNothijato' => 'SUM(nothijat)',
                            'totalNothivukto' => 'SUM(nothivukto)', 'totalNisponnoDak' => 'SUM(nisponnodak)', 'totalpotrojariall' => 'SUM(potrojari)',
                            'totalSouddog' => 'SUM(selfnote)', 'totalONisponnoDak' => 'SUM(onisponnodak)',
                            'totalDaksohoNote' => 'SUM(daksohonote)', 'totalNisponnoPotrojari' => 'SUM(nisponnopotrojari)',
                            'totalNisponnoNote' => 'SUM(nisponnonote)', 'totalONisponnoNote' => 'SUM(onisponnonote)',
                            'totalNisponno' => 'SUM(nisponno)', 'totalONisponno' => 'SUM(onisponno)',
                            'totalID' => 'count(id)', 'designation_id'
                        ];
                        if (!empty($all_designation)) {
                            $hasDataInPerformanceDesignation = $performance_designationTable->getUnitData([$unit_id], [$date, $date])->select($condition)->group(['unit_id'])->toArray();
                            if (!empty($hasDataInPerformanceDesignation)) {
                                $returnSummary['totalInbox'] = 0;
                                $returnSummary['totalOutbox'] = 0;
                                $returnSummary['totalNothijato'] = 0;
                                $returnSummary['totalNothivukto'] = 0;
                                $returnSummary['totalNisponnoDak'] = 0;
                                $returnSummary['totalONisponnoDak'] = 0;
                                $returnSummary['totalPotrojari'] = 0;
                                $returnSummary['totalSouddog'] = 0;
                                $returnSummary['totalDaksohoNote'] = 0;
                                $returnSummary['totalNisponnoNote'] = 0;
                                $returnSummary['totalNisponnoPotrojari'] = 0;
                                $returnSummary['totalONisponnoNote'] = 0;
                                $returnSummary['totalNisponno'] = 0;
                                $returnSummary['totalONisponno'] = 0;
                                foreach ($hasDataInPerformanceDesignation as $val_form_designation) {
                                    $returnSummary['totalInbox'] += $val_form_designation['totalInbox'];
                                    $returnSummary['totalOutbox'] += $val_form_designation['totalOutbox'];
                                    $returnSummary['totalNothijato'] += $val_form_designation['totalNothijato'];
                                    $returnSummary['totalNothivukto'] += $val_form_designation['totalNothivukto'];
                                    $returnSummary['totalNisponnoDak'] += $val_form_designation['totalNisponnoDak'];
//                              $returnSummary['totalONisponnoDak'] += TableRegistry::get('DakUsers')->getOnisponnoDak($unit_related_data['office_id'], $unit_id);
                                    $returnSummary['totalONisponnoDak'] += $val_form_designation['totalONisponnoDak'];
                                    $returnSummary['totalPotrojari'] += $val_form_designation['totalpotrojariall'];
                                    $returnSummary['totalSouddog'] += $val_form_designation['totalSouddog'];
                                    $returnSummary['totalDaksohoNote'] += $val_form_designation['totalDaksohoNote'];
                                    $returnSummary['totalNisponnoNote'] += $val_form_designation['totalNisponnoNote'];
                                    $returnSummary['totalNisponnoPotrojari'] += $val_form_designation['totalNisponnoPotrojari'];
//                              $returnSummary['totalONisponnoNote'] += TableRegistry::get('NothiMasterCurrentUsers')->getAllNothiPartNo($unit_related_data['office_id'], $unit_id,0, [$date,$date])->count();
                                    $returnSummary['totalONisponnoNote'] += $val_form_designation['totalONisponnoNote'];
                                    $returnSummary['totalNisponno'] += $val_form_designation['totalNisponno'];
                                    $returnSummary['totalONisponno'] += $val_form_designation['totalONisponno'];
                                }
                            } else {
                                $from_designation_report = false;
                            }
                        } else {
                            $from_designation_report = false;
                        }

                        if ($from_designation_report == false) {
                            $returnSummary = $reportsTable->newPerformanceReport($unit_related_data['office_id'],
                                $unit_id, 0, ['date_start' => $date, 'date_end' => $date]);
                        }

                        //get id of performanceUnit table record
                        $performanceUnitTableInfo = $performanceUnitTable->find()->select(['id'])->where(['record_date' => $dt, 'unit_id' => $unit_id])->first();
                        if(empty($performanceUnitTableInfo['id'])){
                            $this->out('No record found. For: '.$dt.' Unit id: '.$unit_id);
                            continue;
                        }

                        $performance_units_entity = $performanceUnitTable->get($performanceUnitTableInfo['id']);

//                        $performance_units_entity->inbox = $returnSummary['totalInbox'];
//                        $performance_units_entity->outbox = $returnSummary['totalOutbox'];
//                        $performance_units_entity->nothijat = $returnSummary['totalNothijato'];
//                        $performance_units_entity->nothivukto = $returnSummary['totalNothivukto'];
//                        $performance_units_entity->nisponnodak = $returnSummary['totalNisponnoDak'];
                        $performance_units_entity->onisponnodak = $returnSummary['totalONisponnoDak'];

//                        $performance_units_entity->potrojari = $returnSummary['totalPotrojari'];
                        if ($returnSummary['totalSouddog'] < 0) {
                            $returnSummary['totalSouddog'] = 0;
                        }
//                        $performance_units_entity->selfnote = $returnSummary['totalSouddog'];
//                        $performance_units_entity->daksohonote = $returnSummary['totalDaksohoNote'];
//                        $performance_units_entity->nisponnopotrojari = $returnSummary['totalNisponnoPotrojari'];
//                        $performance_units_entity->nisponnonote = $returnSummary['totalNisponnoNote'];
                        $performance_units_entity->onisponnonote = $returnSummary['totalONisponnoNote'];

//                        $performance_units_entity->nisponno = $returnSummary['totalNisponno'];
                        $performance_units_entity->onisponno = $returnSummary['totalONisponno'];

//                        $performance_units_entity->office_id = $unit_related_data['office_id'];
//                        $performance_units_entity->unit_id = $unit_id;
//                        $performance_units_entity->ministry_id = $unit_related_data['office_ministry_id'];
//                        $performance_units_entity->layer_id = $unit_related_data['office_layer_id'];
//                        $performance_units_entity->origin_id = $unit_related_data['office_origin_unit_id'];
//                        $performance_units_entity->unit_name = $unit_name;

//                        if (!($office_info = Cache::read('office_info_' . $unit_related_data['office_id']))) {
//                            $office_info = $OfficesTable->getBanglaName($unit_related_data['office_id']);
//                            Cache::write('office_info_' . $unit_related_data['office_id'], $office_info);
//                        }
//
//                        $performance_units_entity->office_name = $office_info['office_name_bng'];

//                        if (!($ministry_info = Cache::read('ministry_info_' . $unit_related_data['office_ministry_id']))) {
//                            $ministry_info = $MinistriesTable->getBanglaName($unit_related_data['office_ministry_id']);
//                            Cache::write('ministry_info_' . $unit_related_data['office_ministry_id'], $ministry_info);
//                        }
//
//                        $performance_units_entity->ministry_name = $ministry_info['name_bng'];

//                        if (!($layer_info = Cache::read('layer_info_' . $unit_related_data['office_layer_id']))) {
//                            $layer_info = $LayersTable->getBanglaName($unit_related_data['office_layer_id']);
//                            Cache::write('layer_info_' . $unit_related_data['office_layer_id'], $layer_info);
//                        }
//
//                        $performance_units_entity->layer_name = $layer_info['layer_name_bng'];
//
//                        if (!($origin_info = Cache::read('origin_info_' . $unit_related_data['office_origin_unit_id']))) {
//                            $origin_info = $UnitOriginsTable->getBanglaName($unit_related_data['office_origin_unit_id']);
//                            Cache::write('origin_info_' . $unit_related_data['office_origin_unit_id'], $origin_info);
//                        }
//
//                        $performance_units_entity->origin_name = $origin_info['unit_name_bng'];

//                        $performance_units_entity->status = 1;
//                        $performance_units_entity->record_date = $date;

                        $performanceUnitTable->save($performance_units_entity);
                    }
//                    $this->updateIndividualUnit($unit_id);
//                    $this->deleteMultipleRowsOfUnits($unit_id, !empty($period[0]) ? $period[0] : '');
                } catch (\Exception $ex) {
                    $this->out($ex->getMessage());
                }
            }
        }
        $this->out('Unit Migration Done For Office: ' . $office_id);
        $command = ROOT . "/bin/cake projapoti runCronRequest";
        exec($command);
    }
    public function deleteReportDataPeriodically($date_start = '2016-01-01',$end_date = '2016-12-31') {
        $period = [];
        if (!empty($date_start)) {
            $begin = new \DateTime($date_start);
        }
        if (!empty($end_date)) {
            $end = new \DateTime($end_date);
        }
        for ($i = $begin; $begin <= $end; $i->modify('+1 day')) {
            $period [] = $i->format("Y-m-d");
        }
        $tbl_CronRequest = TableRegistry::get('CronRequest');
        if (!empty($period)) {
            foreach ($period as $date) {
                $command = ROOT . "/bin/cake migration deleteReportData {$date} 'performance_designations' > /dev/null 2>&1 &";
                $tbl_CronRequest->saveData($command);
            }
        }
        $this->runCronRequest();
    }
	public function deleteReportData($date = '2017-12-31', $table = 'performance_designations') {
        try{
            $connection = ConnectionManager::get('NothiReportsDb');
            $connection->transactional(function ($connection) use ($date, $table) {
                $connection->execute("INSERT INTO `".$table."_copy` SELECT * FROM `performance_designations` where record_date = $date;");
                $connection->execute("DELETE FROM `".$table."` where record_date = $date;");
            });
            $command = ROOT . "/bin/cake projapoti runCronRequest";
            exec($command);
        }catch(\Exception $ex){
            $this->out($ex->getMessage());
        }

	}
	public function setStatusforPotrojariPending($office_id = 0,$start_date = '2015-01-01',$end_date = '2019-01-31'){
        if(empty($office_id)){
            return;
        }
        try{
            $this->switchOffice($office_id,'OfficeDB');
            TableRegistry::remove('PotrojariReceiver');
            TableRegistry::remove('PotrojariOnulipi');
            TableRegistry::remove('Potrojari');
            $receiverTable = TableRegistry::get('PotrojariReceiver');
            $onulipiTable = TableRegistry::get('PotrojariOnulipi');

            $receiver_info = $receiverTable->find()->select([ 'pj_id' => 'PotrojariReceiver.potrojari_id'])->join([
                "Potrojari" => [
                    'table' => 'potrojari', 'type' => 'inner',
                    'conditions' => ['PotrojariReceiver.potrojari_id =Potrojari.id']
                ]
            ])->where(['PotrojariReceiver.is_sent' => 0, 'Potrojari.potro_status <>' => 'Draft']);

            if(!empty($start_date)){
                $receiver_info->where(['DATE(Potrojari.potrojari_date) >=' => $start_date ]);
            }
            if(!empty($end_date)){
                $receiver_info->where(['DATE(Potrojari.potrojari_date) <=' => $end_date ]);
            }

            $receiver_info =  $receiver_info->group(['PotrojariReceiver.potrojari_id'])->toArray();

            if(!empty($receiver_info)){
                foreach ($receiver_info as $info){
                    if(!empty($info['pj_id'])){
                        $receiverTable->updateAll(['is_sent' => 1],['potrojari_id' => $info['pj_id'],'is_sent' => 0]);
                    }
                }
            }

            $onulipi_info = $onulipiTable->find()->select([ 'pj_id' => 'PotrojariOnulipi.potrojari_id'])->join([
                "Potrojari" => [
                    'table' => 'potrojari', 'type' => 'inner',
                    'conditions' => ['PotrojariOnulipi.potrojari_id =Potrojari.id']
                ]
            ])->where(['PotrojariOnulipi.is_sent' => 0, 'Potrojari.potro_status <>' => 'Draft']);

            if(!empty($start_date)){
                $onulipi_info->where(['Potrojari.potrojari_date >=' => $start_date ]);
            }
            if(!empty($end_date)){
                $onulipi_info->where(['Potrojari.potrojari_date <=' => $end_date ]);
            }

            $onulipi_info =  $onulipi_info->group(['PotrojariOnulipi.potrojari_id'])->toArray();

            if(!empty($onulipi_info)){
                foreach ($onulipi_info as $info){
                    if(!empty($info['pj_id'])){
                        $onulipiTable->updateAll(['is_sent' => 1],['potrojari_id' => $info['pj_id'],'is_sent' => 0]);
                    }
                }
            }

        }catch (\Exception $ex){
            $this->out('Exception: '.$ex->getMessage());
        }
    }
    public function generateOfficeListForPotrojariPendingMigration(){
//        $week_day = date('N');
//        $office_layers = (jsonA(OFFICE_LAYER_TYPE));
//        if(!empty($office_layers)){
//            $office_layers[7] = 'Other';
//        }

        if(!empty($office_layers) && !empty($week_day) && !empty($office_layers[$week_day])){
            $all_offices = TableRegistry::get('OfficeLayers')->getLayerwiseOfficeEmployeeCount($office_layers[$week_day]);
        }else{
            $office_domains = TableRegistry::get('OfficeDomains');
            $all_offices = $office_domains->getOfficesData();
        }

        $tbl_CronRequest = TableRegistry::get('CronRequest');
        if(!empty($all_offices)){
            foreach ($all_offices as $office_id => $office_name){
                $command = ROOT . "/bin/cake migration setStatusforPotrojariPending {$office_id} '2015-01-01' '2019-01-31' > /dev/null 2>&1 &";
                $tbl_CronRequest->saveData($command);
            }
            $total_request = count($all_offices)/1000;
            if(empty($total_request)){
                $total_request = 1;
            }
            for($i = 0;$i < $total_request ; $i++){
                $command = ROOT . "/bin/cake projapoti runCronRequest";
                exec($command);
            }
        }
    }

    public function updateAllPotrojariGroupUsers() {
        $potrojari_groups_users_table = TableRegistry::get('potrojari_groups_users');
        $potrojari_groups_users = $potrojari_groups_users_table->find('list', ['keyField' => 'id', 'valueField' => 'office_unit_organogram_id'])->hydrate(true)->all()->toArray();
        foreach ($potrojari_groups_users as $potrojari_groups_user_id => $groups_user_designation_id) {
            $designation_info = designationInfo($groups_user_designation_id);
            $potrojari_groups_users_table->updateAll([
                'office_id' => $designation_info['office_id'],
                'office_name_eng' => $designation_info['office_name_eng'],
                'office_name_bng' => $designation_info['office_name'],
                'office_unit_id' => $designation_info['office_unit_id'],
                'office_unit_name_eng' => $designation_info['office_unit_name_eng'],
                'office_unit_name_bng' => $designation_info['office_unit_name'],
                'office_unit_organogram_id' => $designation_info['office_unit_organogram_id'],
                'office_unit_organogram_name_eng' => $designation_info['designation_label_eng'],
                'office_unit_organogram_name_bng' => $designation_info['designation_label'],
                'employee_id' => $designation_info['officer_id'],
                'employee_name_eng' => $designation_info['officer_name_eng'],
                'employee_name_bng' => $designation_info['officer_name'],
                'officer_email' => $designation_info['officer_email'],
                'officer_mobile' => $designation_info['officer_mobile'],
            ], ['id' => $potrojari_groups_user_id]);
        }
    }
    public function checkAllPotrojariGroupMultipleUsers() {
        $potrojari_groups_users_table = TableRegistry::get('potrojari_groups_users');
        $potrojari_groups_users = $potrojari_groups_users_table->find()->select(['id', 'group_id', 'office_unit_organogram_id'])->where(['office_unit_organogram_id !=' => 0])->group(['group_id', 'office_unit_organogram_id'])->having(['count(1) >' => 1])->hydrate(true)->toArray();

        $groupByField = groupByField($potrojari_groups_users, 'group_id');
        foreach ($groupByField as $potrojari_group_id => $groups_users) {
            foreach($groups_users as $key => $groups_user) {
                $potrojari_groups_users_table->deleteAll(['group_id' => $groups_user['group_id'], 'office_unit_organogram_id' => $groups_user['office_unit_organogram_id'], 'id !=' => $groups_user['id']]);
            }
            $total_user = $potrojari_groups_users_table->find()->where(['group_id' => $potrojari_group_id])->count();
            TableRegistry::remove('potrojari_groups');
            $potrojari_groups_table = TableRegistry::get('potrojari_groups');
            $potrojari_groups_table->updateAll(['total_users' => $total_user], ['id' => $potrojari_group_id]);
        }

        $this->out('All done');
    }
    public function generateOfficeListForDakRegisterMigration(){
//        $week_day = date('N');
//        $office_layers = (jsonA(OFFICE_LAYER_TYPE));
//        if(!empty($office_layers)){
//            $office_layers[7] = 'Other';
//        }

        if(!empty($office_layers) && !empty($week_day) && !empty($office_layers[$week_day])){
            $all_offices = TableRegistry::get('OfficeLayers')->getLayerwiseOfficeEmployeeCount($office_layers[$week_day]);
        }else{
            $office_domains = TableRegistry::get('OfficeDomains');
            $all_offices = $office_domains->getOfficesData();
        }

        $tbl_CronRequest = TableRegistry::get('CronRequest');
        if(!empty($all_offices)){
            foreach ($all_offices as $office_id => $office_name){
                $command = ROOT . "/bin/cake migration generateDakRegisterMigrationDates {$office_id} > /dev/null 2>&1 &";
                $tbl_CronRequest->saveData($command);
            }
            $total_request = count($all_offices)/500;
            if(empty($total_request)){
                $total_request = 1;
            }
            for($i = 0;$i < $total_request ; $i++){
                $this->runCronRequest();
            }
        }
    }
    public function generateDakRegisterMigrationDates($office_id = 0){
        if(empty($office_id)){
            $this->out('No Office id given');
            return;
        }
        try{
            $this->switchOffice($office_id,'OfficeDB');

            TableRegistry::remove('DakMovements');
            TableRegistry::remove('DakRegister');
            $tbl_DakMovements = TableRegistry::get('DakMovements');
            $tbl_CronRequest = TableRegistry::get('CronRequest');

            // select first date of DM
            $info_DM = $tbl_DakMovements->find()->select(['last_date' => 'DATE(created)'])->order(['created'])->first();
            if(!empty($info_DM)){
                //generated a date range
                $begin = new \DateTime($info_DM['last_date']);
//                $today = $info_DM['created'];
                $today = new \DateTime(date('Y-m-d'));
//                $yesterday = new \DateTime(date('Y-m-d', strtotime(date('Y-m-d') . ' -1 day')));
                for ($i = $begin; $begin <= $today; $i->modify('+1 day')) {
                    $date = $i->format("Y-m-d");
                    $command = ROOT . "/bin/cake migration saveDakRegisterInfoDateWise {$office_id} '{$date}' > /dev/null 2>&1 &";
                    $tbl_CronRequest->saveData($command);
                }
            }
            $this->generateNothiRegisterMigrationDates($office_id);
            $this->runCronRequest();
        }catch (\Exception $ex){
            $this->out('Exception: '.$ex->getMessage());
        }
    }
    public function generateNothiRegisterMigrationDates($office_id = 0){
        if(empty($office_id)){
            $this->out('No Office id given');
            return;
        }
        try{
            $this->switchOffice($office_id,'OfficeDB');

            TableRegistry::remove('NothiMasterMovements');
            TableRegistry::remove('Potrojari');
            TableRegistry::remove('NothiMasters');
            $tbl_NothiMasterMovements= TableRegistry::get('NothiMasterMovements');
            $tbl_Potrojari= TableRegistry::get('Potrojari');
            $tbl_NothiMasters= TableRegistry::get('NothiMasters');
            $tbl_CronRequest = TableRegistry::get('CronRequest');

            // select first date
            $info_NMMov = $tbl_NothiMasterMovements->find()->select(['last_date' => 'DATE(created)'])->order(['created'])->first();
            $info_Potrojari = $tbl_Potrojari->find()->select(['last_date' => 'DATE(created)'])->where(['potro_status' => 'Sent'])->order(['created'])->first();
            $info_NMaster = $tbl_NothiMasters->find()->select(['last_date' => 'DATE(created)'])->order(['created'])->first();

            $begin = '';

            //generated a date range
            if(!empty($info_NMMov)){
                $begin = ($info_NMMov['last_date']);
            }
            if(!empty($info_Potrojari)){
                if($begin > $info_Potrojari['last_date']);
                $begin = ($info_Potrojari['last_date']);
            }
            if(!empty($info_NMaster)){
                if($begin > $info_NMaster['last_date']);
                $begin = ($info_NMaster['last_date']);
            }
            if(!empty($begin)){
                $begin = new \DateTime($begin);
                $today = new \DateTime(date('Y-m-d'));
                for ($i = $begin; $begin <= $today; $i->modify('+1 day')) {
                    $date = $i->format("Y-m-d");
                    $command = ROOT . "/bin/cake migration saveNothiRegisterInfoDateWise {$office_id} '{$date}' > /dev/null 2>&1 &";
                    $tbl_CronRequest->saveData($command);
                    $command = ROOT . "/bin/cake migration savePotrojariRegisterInfoDateWise {$office_id} '{$date}' > /dev/null 2>&1 &";
                    $tbl_CronRequest->saveData($command);
                }
            }
            $this->runCronRequest();
        }catch (\Exception $ex){
            $this->out('Exception: '.$ex->getMessage());
        }
    }
    public function saveDakRegisterInfoDateWise($office_id = 0, $date){
        if(empty($office_id) || empty($date)){
            $this->out('Required info not given');
            return;
        }
        try{
            $this->switchOffice($office_id,'OfficeDB');
            TableRegistry::remove('DakMovements');
            TableRegistry::remove('DakRegister');
            $tbl_DakMovements = TableRegistry::get('DakMovements');
            $tbl_DakRegister = TableRegistry::get('DakRegister');

            //search for data on that date
            $count_info_DM = $tbl_DakMovements->find()->where(['Date(created)' => $date])->count();
            $count_info_DR = $tbl_DakRegister->find()->where(['Date(movement_time)' => $date])->count();

            if($count_info_DM != $count_info_DR){
                // not match. need to migrate
                if($count_info_DR > 0){
                    //may be partially added. so clean all
                    $tbl_DakRegister->deleteAll(['Date(movement_time)' => $date]);
                }
                //get all info
               $infos2save = $tbl_DakMovements->find()->where(['Date(created)' => $date])->toArray();
                if(!empty($infos2save)){
                    foreach ($infos2save as $val){
//                        $make_dm_entity = $tbl_DakMovements->newEntity($val);
                        $tbl_DakRegister->saveData($val);
                    }
                }
            }
        }catch (\Exception $ex){
            $this->out("Exception saveDakRegisterInfoDateWise ({$office_id}) ({$date}): ".$ex->getMessage());
        }
        $this->runCronRequest();
    }
    public function savePotrojariRegisterInfoDateWise($office_id = 0, $date){
        if(empty($office_id) || empty($date)){
            $this->out('Required info not given');
            return;
        }
        try{
            $this->switchOffice($office_id,'OfficeDB');
            TableRegistry::remove('Potrojari');
            $tbl_Potrojari = TableRegistry::get('Potrojari');
            $tbl_PotrojariRegister = TableRegistry::get('PotrojariRegisters');
            $tbl_PotrojariReceiver = TableRegistry::get('PotrojariReceiver');
            $tbl_PotrojariOnulipi = TableRegistry::get('PotrojariOnulipi');

            //search for data on that date
            $count_info_Potrojari = $tbl_Potrojari->find()->where(['Date(created)' => $date])->count();
            $count_info_Potrojari_Register = $tbl_PotrojariRegister->find()->where(['Date(created)' => $date])->count();

            //dak_type,dak_receive_no, potrojari_sharok_no => sarok_no,potrojari_subject => potro_subject,onulipi_info,receiver_info

            if($count_info_Potrojari != $count_info_Potrojari_Register){
                // not match. need to migrate
                if($count_info_Potrojari_Register > 0){
                    //may be partially added. so clean all
//                    $tbl_PotrojariRegister->deleteAll(['Date(created)' => $date]);
                }
                //get all info
                $infos2save = $tbl_Potrojari->find()->where(['Date(created)' => $date])->toArray();
                if(!empty($infos2save)){
                    foreach ($infos2save as $val){
                        $pr_info = $val;
                        if(!empty($pr_info['id'])){
                            //get receiver info and onulipi info
                            $receiver_info = $tbl_PotrojariReceiver->getReceiverInfoForRegisterTable($pr_info['id']);
                            if(!empty($receiver_info)){
                                $pr_info['receiver_info'] = json_encode($receiver_info);

                            }
                            $onulipi_info = $tbl_PotrojariOnulipi->getOnulipiInfoForRegisterTable($pr_info['id']);
                            if(!empty($onulipi_info)){
                                $pr_info['onulipi_info'] = json_encode($onulipi_info);

                            }
                            $pr_info['potrojari_id'] = $pr_info['id'];
                            $tbl_PotrojariRegister->saveDataPotrojariRegisters($pr_info,$val);
                        }
                    }
                }
            }
        }catch (\Exception $ex){
            $this->out("Exception savePotrojariRegisterInfoDateWise ({$office_id}) ({$date}): ".$ex->getMessage());
        }
        $this->runCronRequest();
    }
    public function saveNothiRegisterInfoDateWise($office_id = 0, $date){
        if(empty($office_id) || empty($date)){
            $this->out('Required info not given');
            return;
        }
        try{
            $this->switchOffice($office_id,'OfficeDB');
            TableRegistry::remove('NothiMasterMovements');
            TableRegistry::remove('Potrojari');
            TableRegistry::remove('NothiMasters');
            $tbl_NothiMasterMovements= TableRegistry::get('NothiMasterMovements');
            $tbl_Potrojari= TableRegistry::get('Potrojari');
            $tbl_NothiMasters= TableRegistry::get('NothiMasters');
            $tbl_NothiRegister = TableRegistry::get('NothiRegisterLists');

            //search for data on that date
            $count_info_NothiMasterMovements = $tbl_NothiMasterMovements->find()->where(['Date(created)' => $date])->count();
            $count_info_Potrojari = $tbl_Potrojari->find()->where(['Date(potrojari_date)' => $date,'potro_status' => 'Sent'])->count();
            $count_info_NothiMasters = $tbl_NothiMasters->find()->where(['Date(created)' => $date])->count();
            $count_info_Nothi_Register = $tbl_NothiRegister->find()->where(['Date(created)' => $date])->count();


            //dak_type,dak_receive_no, potrojari_sharok_no => sarok_no,potrojari_subject => potro_subject,onulipi_info,receiver_info

            if(($count_info_Potrojari + $count_info_NothiMasterMovements + $count_info_NothiMasters) != $count_info_Nothi_Register){
                // not match. need to migrate
                if($count_info_Nothi_Register > 0){
                    //may be partially added. so clean all
                    $tbl_NothiRegister->deleteAll(['Date(created)' => $date]);
                }
                //get all info
                $infos2save = $tbl_NothiMasterMovements->find()->where(['Date(created)' => $date])->toArray();
                if(!empty($infos2save)){
                    foreach ($infos2save as $val){
                        $info = $val;
                        if(!empty($info['nothi_master_id'])){
                            unset($info['id']);
                           $core_info = $tbl_NothiMasters->get($info['nothi_master_id']);
                           $info['nothi_no'] = $core_info['nothi_no'];
                           $info['subject'] = $core_info['subject'];
                           $info['created_office_id'] = 0;
                           $info['created_unit_id'] = 0;
                           $info['created_designation_id'] = 0;
                           $info['sending_date'] = $info['created'];
                           $info['nothi_created_date'] = $core_info['nothi_created_date'];
                           $info['nothi_class'] = $core_info['nothi_class'];
                           $info['movement_type'] = 'Sent';
                           $info['nothi_type_id'] = $core_info['nothi_types_id'];
                           $info['nothi_modified'] = $info['modified'];
                           $info['is_finished'] = 0;
                           $info['is_archived'] = $core_info['is_archived'];
                           $info['created_by'] = 0;
                           $info['modified_by'] = 0;
                        }
                        $info = json_encode($info);
                        $info = json_decode($info,true);

                        $make_NR_entity = $tbl_NothiRegister->newEntity($info);
                        $tbl_NothiRegister->save($make_NR_entity);
                    }
                }
                //get all info
                $infos2save = $tbl_NothiMasters->find()->where(['Date(created)' => $date])->toArray();
                if(!empty($infos2save)){
                    foreach ($infos2save as $val){
                        $info = $val;
                        if(!empty($info['id'])){
                           $info['nothi_master_id'] = $info['id'];
                            unset($info['id']);

                           $info['subject'] = $info['subject'];
                           $info['created_office_id'] = $info['office_id'];
                           $info['created_unit_id'] = $info['office_units_id'];
                           $info['created_designation_id'] = $info['office_units_organogram_id'];
                           $info['movement_type'] = 'Created';
                           $info['nothi_type_id'] = $info['nothi_types_id'];
                           $info['nothi_modified'] = $info['modified'];
                           $info['is_finished'] = 0;
                           $info['is_archived'] = $info['is_archived'];
                           $info['created_by'] = 0;
                           $info['modified_by'] = 0;
                        }
                        $info = json_encode($info);
                        $info = json_decode($info,true);

                        $make_NR_entity = $tbl_NothiRegister->newEntity($info);
                        $tbl_NothiRegister->save($make_NR_entity);
                    }
                }
                //get all info
                $infos2save = $tbl_Potrojari->find()->where(['Date(created)' => $date,'potro_status' => 'Sent'])->toArray();
                if(!empty($infos2save)){
                    foreach ($infos2save as $val){
                        $info = $val;
                        if(!empty($info['id'])){
                           $info['potrojari_id'] = $info['id'];
                            unset($info['id']);

                            $core_info = $tbl_NothiMasters->get($info['nothi_master_id']);

                            $info['nothi_no'] = $core_info['nothi_no'];
                            $info['subject'] = $core_info['subject'];
                            $info['created_office_id'] = $info['office_id'];
                            $info['created_unit_id'] = $info['office_unit_id'];
                            $info['created_designation_id'] = $info['officer_designation_id'];
                            $info['sending_date'] = $info['created'];
                            $info['nothi_created_date'] = $core_info['nothi_created_date'];
                            $info['nothi_class'] = $core_info['nothi_class'];
                            $info['movement_type'] = 'Potrojari';
                            $info['nothi_type_id'] = $core_info['nothi_types_id'];
                            $info['nothi_modified'] = $info['modified'];
                            $info['is_finished'] = 0;
                            $info['is_archived'] = $core_info['is_archived'];
                            $info['created_by'] = 0;
                            $info['modified_by'] = 0;
                        }
                        $info = json_encode($info);
                        $info = json_decode($info,true);

                        $make_NR_entity = $tbl_NothiRegister->newEntity($info);
                        $tbl_NothiRegister->save($make_NR_entity);
                    }
                }
            }
        }catch (\Exception $ex){
            $this->out("Exception saveDakRegisterInfoDateWise ({$office_id}) ({$date}): ".$ex->getMessage());
        }
        $this->runCronRequest();
    }
    public function migrateInternalExternalDakCount(){
//        $week_day = date('N');
        $week_day = 1;
        $office_layers = (jsonA(OFFICE_LAYER_TYPE));
        if(!empty($office_layers)){
            $office_layers[7] = 'Other';
        }

        if(!empty($office_layers) && !empty($week_day) && !empty($office_layers[$week_day])){
            $all_offices = TableRegistry::get('OfficeLayers')->getLayerwiseOfficeEmployeeCount($office_layers[$week_day]);
        }else{
            $office_domains = TableRegistry::get('OfficeDomains');
            $all_offices = $office_domains->getOfficesData();
        }

        $html = "";
        $tbl_CronRequest = TableRegistry::get('CronRequest');
        if(!empty($all_offices)){
            foreach ($all_offices as $office_id => $office_name){
                // get first office record
                $performanceOfficesTable = TableRegistry::get('PerformanceOffices');
                $has_data= $performanceOfficesTable->find()->select(['record_date'])->where([ 'office_id' => $office_id])->order(['record_date asc'])->first();
                if(!empty($has_data)){
                    $start_date = $has_data['record_date'];
                    $start    = (new \DateTime($start_date))->modify('first day of this month');
                    $end      = (new \DateTime(date('Y-m-d')))->modify('first day of next month');
                    $interval = \DateInterval::createFromDateString('1 month');
                    $period   = new \DatePeriod($start, $interval, $end);

                    $date_ranges = [];
                    foreach ($period as $dt) {
                        $date_ranges[] = $dt->format("Y-m-d");
                    }
                    $html .= "Office ID:".$office_id."<hr><br>";
                    if(!empty($date_ranges)){
                        foreach ($date_ranges as $key => $date_range) {
                            if($key == 0){
                                continue;
                            }
                            $command = ROOT . "/bin/cake migration migrateInternalExternalDakCountOfOffice {$office_id} '{$date_ranges[$key - 1]}' '{$date_ranges[$key]}' > /dev/null 2>&1 &";
                            $html .= $command."<br>";
                            $tbl_CronRequest->saveData($command);
                        }
                    }
                }


            }
            $total_request = count($all_offices)/10;
            if(empty($total_request)){
                $total_request = 1;
            }
            for($i = 0;$i < $total_request ; $i++){
                $command = ROOT . "/bin/cake projapoti runCronRequest";
                exec($command);
            }
            $params = ['to_email' => 'tusharkaiser@gmail.com', 'subject' => "[Migration] Internal External Dak Shell Creation", 'email_body' => "Dear Concern,<br> Internal External Dak Cron has been created,Details:<br> {$html} <br>Thanks,<br>Nothi Team"];
            $this->sendEmailByEmailer($params);
        }
    }
    public function migrateInternalExternalDakCountOfOffice($office_id = 0,$start_date = '',$end_date = ''){
        try{
            if(empty($office_id) || empty($start_date)){
                $this->out('Required info not given');
                return;
            }
            $performanceOfficesTable = TableRegistry::get('PerformanceOffices');
            $period = [];
            if(!empty($date_range)){
                $begin = !empty($start_date) ? new \DateTime($start_date) : new \DateTime(date('Y-m-d', strtotime(date('Y-m-d') . ' -1 week')));
                $end = !empty($end_date) ? new \DateTime($end_date) : new \DateTime(date('Y-m-d'));

                for ($i = $begin; $begin <= $end; $i->modify('+1 day')) {
                    $period [] = $i->format("Y-m-d");
                }
            }
            if (!empty($period)) {
                foreach ($period as $day) {
                    $has_data= $performanceOfficesTable->find()->where(['record_date' => $day, 'office_id' => $office_id,'online_dak is NULL','uploaded_dak is NULL'])->first();
                    if (empty($has_data)) {
                        // fetch online dak count
                        if(isset($has_data['inbox'])){
                            TableRegistry::remove('DakMovements');
                            $table_DakMovements = TableRegistry::get('DakMovements');
                            $online_dak = $table_DakMovements->getOnlikeDakCount($office_id,0,0,[$day,$day]);
                            if($has_data['inbox'] < $online_dak){
                                $online_dak = $has_data['inbox'];
                                $uploaded_dak = 0;
                            }else{
                                $uploaded_dak = $has_data['inbox'] - $online_dak;
                            }
                            $performanceOfficesTable->updateAll(['online_dak' => $online_dak,'uploaded_dak' => $uploaded_dak],['id' => $has_data['id']]);
                        }
                    }
                }
            }
            $params = ['to_email' => 'tusharkaiser@gmail.com', 'subject' => "[Migration] Internal External Dak Count [Office ID - " . $office_id."]", 'email_body' => "Dear Concern,<br> Internal External Dak Count for Office id: {$office_id}, Duration: {$start_date} - {$end_date} is done. <br>Thanks,<br>Nothi Team"];
            $this->sendEmailByEmailer($params);
        }catch (\Exception $ex){
            $tbl_CronRequest = TableRegistry::get('CronRequest');
            $command = ROOT . "/bin/cake migration migrateInternalExternalDakCountOfOffice {$office_id} {$start_date} {$end_date}> /dev/null 2>&1 &";
            $tbl_CronRequest->saveData($command);
            $params = ['to_email' => 'tusharkaiser@gmail.com', 'subject' => "[Migration] Internal External Dak Count [Office ID - " . $office_id."]", 'email_body' => "Dear Concern,<br> Internal External Dak Count for Office id: {$office_id}, Duration: {$start_date} - {$end_date} is done. <br>Thanks,<br>Nothi Team"];
            $this->sendEmailByEmailer($params);
        }
        $command = ROOT . "/bin/cake projapoti runCronRequest";
        exec($command);

    }
    public function getInternalExternalDakCountOfOffice($office_id,$start_date = '2018-06-01',$end_date = '2018-07-01'){
        try{
//            $html = "";
            if(!empty($office_id)){
                        $this->switchOffice($office_id,'DashboardOffice');
                        // get first office record
                        TableRegistry::remove('DakMovements');
                        $table_DakMovements = TableRegistry::get('DakMovements');
                        $start    = (new \DateTime($start_date))->modify('first day of this month');
                        $end      = (new \DateTime($end_date))->modify('first day of this month');
                        $interval = \DateInterval::createFromDateString('1 month');
                        $period   = new \DatePeriod($start, $interval, $end);

                        $date_ranges = [];
                        $online_dak = 0;
                        foreach ($period as $dt) {
                            $date_ranges[] = $dt->format("Y-m-d");
                        }
//                        $html .= "Office ID:".$office_id."<hr><br>";
                        if(!empty($date_ranges)){
                            foreach ($date_ranges as $key => $date_range) {
                                if($key == 0){
                                    continue;
                                }
                                $online_dak += $table_DakMovements->getOnlikeDakCount($office_id,0,0,[$date_ranges[$key - 1],$date_ranges[$key]]);
//                                $html .= $command."<br>";
//                                $tbl_CronRequest->saveData($command);
                            }
                        }

                        $this->out("Office ID: {$office_id} Online DAK: {$online_dak}");
//                        $html .="Office ID: {$office_id} Online DAK: {$online_dak}<br>";

            }
        }catch (\Exception $ex){
            $this->out($ex->getMessage());
//            $html .="Process got error. Error: {$ex->getMessage()}<br>";
        }
//        $week_day = date('N');

//            $params = ['to_email' => 'tusharkaiser@gmail.com', 'subject' => "[Migration] Internal External Dak Shell Creation", 'email_body' => "Dear Concern,<br> Internal External Dak Cron has been created,Details:<br> {$html} <br>Thanks,<br>Nothi Team"];
//            $this->sendEmailByEmailer($params);

    }
}