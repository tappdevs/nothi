<?php

namespace App\Shell;

use Cake\Console\Shell;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
use Cake\Network\Email\Email;
use Exception;

class ProjapotiShell extends Shell
{
    protected function setCurrentDakSection($designation = null)
    {

        $employee_office = [];
        if (!empty($designation)) {
            $EmployeeOfficesTable = TableRegistry::get('EmployeeOffices');
            $table_instance_ministris = TableRegistry::get('OfficeMinistries');
            $employee_office_data = $EmployeeOfficesTable->find()->where(['office_unit_organogram_id' => $designation, 'EmployeeOffices.status' => 1])->contain(['Offices', 'OfficeUnits', 'OfficeUnitOrganograms', 'EmployeeRecords'])->first();

            $employee_office['office_id'] = $employee_office_data['office_id'];
            $employee_office['office_unit_id'] = $employee_office_data['office_unit_id'];
            $employee_office['office_unit_organogram_id'] = $employee_office_data['office_unit_organogram_id'];
            $employee_office['officer_id'] = $employee_office_data['employee_record_id'];

            $employee_office['office_name'] = $employee_office_data['office']['office_name_bng'];
            $employee_office['office_name_eng'] = $employee_office_data['office']['office_name_eng'];
            $employee_office['office_address'] = $employee_office_data['office']['office_address'];
            $employee_office['office_phone'] = $employee_office_data['office']['office_phone'];
            $employee_office['office_fax'] = $employee_office_data['office']['office_fax'];
            $employee_office['office_email'] = $employee_office_data['office']['office_email'];
            $employee_office['office_web'] = $employee_office_data['office']['office_web'];

            $employee_office['incharge_label'] = $employee_office_data['incharge_label'];
            $employee_office['designation'] = $employee_office_data['office_unit_organogram']['designation_bng'];
            $employee_office['designation_eng'] = $employee_office_data['office_unit_organogram']['designation_eng'];
            $employee_office['designation_label'] = $employee_office_data['office_unit_organogram']['designation_bng'];
            $employee_office['officer_name'] = $employee_office_data['employee_record']['name_bng'];
            $employee_office['officer_name_eng'] = $employee_office_data['employee_record']['name_eng'];
            $employee_office['office_unit_name'] = $employee_office_data['office_unit']['unit_name_bng'];
            $employee_office['office_unit_name_eng'] = $employee_office_data['office_unit']['unit_name_eng'];

            $ministryInformation = $table_instance_ministris->get($employee_office_data['office']['office_ministry_id']);
            $employee_office['ministry_name_bng'] = $ministryInformation['name_bng'];
            $employee_office['ministry_name_eng'] = $ministryInformation['name_eng'];
            $employee_office['ministry_records'] = $ministryInformation['name_bng'];

            $employee_office['show_unit'] = $employee_office_data['show_unit'];
            $employee_office['default_sign'] = $employee_office_data['employee_record']['default_sign'];
            $employee_office['cert_id'] = $employee_office_data['employee_record']['cert_id'];
            $employee_office['cert_type'] = $employee_office_data['employee_record']['cert_type'];
            $employee_office['cert_provider'] = $employee_office_data['employee_record']['cert_provider'];
            $employee_office['cert_serial'] = $employee_office_data['employee_record']['cert_serial'];
        }
        return $employee_office;
    }

    protected function switchOffice($office_id, $officeName)
    {
        $officeDomainTable = TableRegistry::get('OfficeDomains');
        $officeDomain = $officeDomainTable->find()->where(['office_id' => $office_id])->first();

        if (empty($officeDomain)) {
            throw new Exception("অফিস ডাটাবেজ পাওয়া যায় নি! সাপোর্ট টিমের সাথে যোগাযোগ করুন। অফিস আইডিঃ ".$office_id);
        }

        ConnectionManager::drop('default');
        ConnectionManager::drop($officeName);
        ConnectionManager::config($officeName,
            [
                'className' => 'Cake\Database\Connection',
                'driver' => 'Cake\Database\Driver\Mysql',
                'persistent' => false,
                'host' => $officeDomain['domain_host'],
                'username' => $officeDomain['domain_username'],
                'password' => $officeDomain['domain_password'],
                'database' => $officeDomain['office_db'],
                'encoding' => 'utf8',
                'cacheMetadata' => true,
            ]
        );

        ConnectionManager::alias($officeName, 'default');
    }

    public function saveCronLog($cron_name, $op_date, $start_time, $end_time, $total, $success,
                                $failed, $details)
    {
        $cronLogTable = TableRegistry::get('CronLog');
        $cronLogTable->saveData($cron_name, $op_date, $start_time, $end_time, $total, $success,
            $failed, $details);
    }

    public function sendCronMessage($to, $cc, $subject = '', $from = 'Cron Jobs', $message, $format = 'html', $layout = 'default', $template = 'notification', $body_subject = '', $instant_mail = true, $bcc = null)
    {
        if ($instant_mail) {
            $email = new Email('default');
            try {
                $email->emailFormat($format)->from(['nothi@nothi.org.bd' => $from])
                    ->to($to)
                    ->addCc($cc)
                    ->addBcc($bcc)
                    ->subject($subject)
                    ->viewVars([
                        'notification' => $message,
                        'notificationTime' => \Cake\I18n\Time::parse(date("Y-m-d H:i:s")),
                        'subject' => $subject,
                        'body_subject' => !empty($body_subject) ? $body_subject : $subject,
                    ])
                    ->template($template, $layout)
                    ->send();
            } catch (\Exception $ex) {
                return $ex->getMessage();
            }

            return true;
        }
    }

    public function sendEmailByEmailer($notification_params)
    {
        if (!empty($notification_params['to_email']) && !($this->emailValidate($notification_params['to_email']))) {
            return 'Invalid Email ( ' . $notification_params['to_email'] . ' )';
        }
        $data['to_email'] = $notification_params['to_email'];
        $data['to_cc'] = !empty($notification_params['to_cc']) ? $notification_params['to_cc'] : "";
        $data['to_name'] = !empty($notification_params['to_name']) ? $notification_params['to_name'] : "";
        $data['from_email'] = !empty($notification_params['from_email']) ? $notification_params['from_email'] : "nothi@nothi.org.bd";
        $data['from_name'] = !empty($notification_params['from_name']) ? $notification_params['from_name'] : "নথি ";
        $data['from_des'] = !empty($notification_params['from_des']) ? $notification_params['from_des'] : "";
        $data['format'] = !empty($notification_params['format']) ? $notification_params['format'] : "html";
        $data['template'] = !empty($notification_params['template']) ? $notification_params['template'] : "notification";
        $data['subject'] = $notification_params['subject'];
        $data['email_body'] = !empty($notification_params['email_body']) ? $notification_params['email_body'] : "";
        $data['layout'] = !empty($notification_params['layout']) ? $notification_params['layout'] : "default";
        $data['viewVars'] = !empty($notification_params['viewVars']) ? $notification_params['viewVars'] : [];
        $data['attachments'] = !empty($notification_params['attachments']) ? $notification_params['attachments'] : [];
        $data['type'] = !empty($notification_params['type']) ? $notification_params['type'] : [];
        $data['email_trace_id'] = !empty($notification_params['email_trace_id']) ? $notification_params['email_trace_id'] : [];
        $data['nothi_office'] = !empty($notification_params['nothi_office']) ? $notification_params['nothi_office'] : [];
        $data['office_id'] = !empty($notification_params['office_id']) ? $notification_params['office_id'] : 0;
        $data['office_name'] = !empty($notification_params['office_name']) ? $notification_params['office_name'] : "";
        //
        $data['service_id'] = !empty($notification_params['service_id']) ? $notification_params['service_id'] : EMAIL_USER;
        $nothi_email = new \App\Model\Entity\NothiEmail();
        try {
            $nothi_email->addEmailQueue($data);
            return true;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
        return 'Error occured';
    }

    public static function emailValidate($email)
    {
        return (boolean)filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    public function runThisCommand($command = '')
    {
        try {
            $command = str_replace("/nothi_new", "", $command);
            exec($command);
        } catch (\Exception $ex) {
            $this->out($ex->getMessage());
        }

    }

    public function saveSMSRequest($cell_number = '', $message = '')
    {
        if (empty($cell_number) || empty($message)) {
            return;
        }
        $ms = bnToen($cell_number);
        if (substr($ms, 0, 3) != '+88') {
            $ms = '+88' . $ms;
        }
        try {
            $SmsRequestTable = TableRegistry::get('SmsRequest');
            $SmsRequestTable->saveData($ms, $message);
        } catch (\Exception $ex) {
            return;
        }
    }

    public function makeDirectory($path = 'Dak', $office = 0)
    {

        if (!is_dir(FILE_FOLDER_DIR . $path . '/')) {
            mkdir(FILE_FOLDER_DIR . $path . '/', 777, true);
            $content = "<?php die('Digital Bangladesh'); ?>";
            $fp = fopen(FILE_FOLDER_DIR . $path . "/index.php", "wb");
            fwrite($fp, $content);
            fclose($fp);
        }

        //office
        if (!is_dir(FILE_FOLDER_DIR . $path . '/' . $office . '/')) {
            mkdir(FILE_FOLDER_DIR . $path . '/' . $office . '/', 777, true);
            $content = "<?php die('Digital Bangladesh'); ?>";
            $fp = fopen(FILE_FOLDER_DIR . $path . '/' . $office . "/index.php",
                "wb");
            fwrite($fp, $content);
            fclose($fp);
        }

        //year
        if (!is_dir(FILE_FOLDER_DIR . $path . '/' . $office . '/' . date('Y') . '/')) {
            mkdir(FILE_FOLDER_DIR . $path . '/' . $office . '/' . date('Y') . '/', 777,
                true);
            $content = "<?php die('Digital Bangladesh'); ?>";
            $fp = fopen(FILE_FOLDER_DIR . $path . '/' . $office . '/' . date("Y") . "/index.php",
                "wb");
            fwrite($fp, $content);
            fclose($fp);
        }

        //month
        if (!is_dir(FILE_FOLDER_DIR . $path . '/' . $office . '/' . date('Y') . '/' . date('m') . '/')) {
            mkdir(FILE_FOLDER_DIR . $path . '/' . $office . '/' . date('Y') . '/' . date('m') . '/',
                777, true);
            $content = "<?php die('Digital Bangladesh'); ?>";
            $fp = fopen(FILE_FOLDER_DIR . $path . '/' . $office . '/' . date('Y') . '/' . date('m') . "/index.php",
                "wb");
            fwrite($fp, $content);
            fclose($fp);
        }

        //day
        if (!is_dir(FILE_FOLDER_DIR . $path . '/' . $office . '/' . date('Y') . '/' . date('m') . '/' . date('d') . '/')) {
            mkdir(FILE_FOLDER_DIR . $path . '/' . $office . '/' . date('Y') . '/' . date('m') . '/' . date('d') . '/',
                777, true);
            $content = "<?php die('Digital Bangladesh'); ?>";
            $fp = fopen(FILE_FOLDER_DIR . $path . '/' . $office . '/' . date('Y') . '/' . date('m') . '/' . date('d') . "/index.php",
                "wb");
            fwrite($fp, $content);
            fclose($fp);
        }

        return FILE_FOLDER_DIR . $path . '/' . $office . '/' . date('Y') . '/' . date('m') . '/' . date('d') . '/';
    }

    public function NotificationSet($type, $options, $notificationtype,
                                    $employee = array(), $toUser = array(),
                                    $subject = '', $msg = '', $replace_msg = 0)
    {
        if (NOTIFICATION_OFF) {
            return true;
        }
        $notificationTable = TableRegistry::get('NotificationMessages');
        return $notificationTable->saveNotification($type, $options,
            $notificationtype, $employee, $toUser, $subject, $msg,
            $replace_msg);
    }
    public function runCronRequest()
    {
//         $start = strtotime(date('Y-m-d H:i:s'));
//        return;
        $time = date('G');
        if (intval($time) == 17 || intval($time) == 7 || intval($time) == 23) {
            return;
        }
        $cronRequests = TableRegistry::get('CronRequest')->getData(1)->toArray();
        $ids = [];
        try {
            $i = 0;
            if (!empty($cronRequests)) {
                foreach ($cronRequests as $request) {
                    $ids[] = $request['id'];
                    exec($request['command']);
                    $command = str_replace("/nothi_new", "", $request['command']);
                    if($command != $request['command']){
                        exec($command);
                    }
                    $i++;
                    if ($i % 5 == 0) {
                        sleep(60);
                    }
                }
                if (!empty($ids)) {
                    TableRegistry::get('CronRequest')->deleteAll(['id IN' => $ids]);
                }
            }
//             $end = strtotime(date('Y-m-d H:i:s'));
//             $this->out('Mins: '.round(abs($start - $end) / 60, 2));
        } catch (\Exception $ex) {
            $this->out($ex->getMessage());
        }

    }
    public function requestUrl($url = '')
    {
        if (empty($url)) {
            return false;
        }
        try {
            $curl_handle = curl_init();
            curl_setopt($curl_handle, CURLOPT_URL, $url);
            curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
            curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Nothi');
            curl_exec($curl_handle);
            return true;
        } catch (\Exception $ex) {
            $this->out($ex->getMessage());
            return false;
        }
    }
}

