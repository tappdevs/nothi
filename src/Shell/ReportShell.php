<?php

namespace App\Shell;

use Cake\Console\Shell;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use App\Controller\ProjapotiController;
use Cake\Datasource\ConnectionManager;
use Cake\I18n\I18n;
use Cake\Network\Email\Email;
use Cake\I18n\Number;
use Cake\I18n\Time;
use Cake\Routing\Route\Route;
use Cake\Routing\Router;
use Exception;
use mikehaertl\wkhtmlto\Pdf;
use Cake\Cache\Cache;
use Cake\Database\Expression\QueryExpression;

/**
 * Report shell command.
 */
class ReportShell extends ProjapotiShell
{

    /**
     * main() method.
     *
     * @return bool|int Success or error code.
     */
    public function main()
    {

    }

    private function commands($office_id, $potrojari_id)
    {
        $command = "bin/cake report potrojariReceiverSent {$office_id} {$potrojari_id}> /dev/null 2>&1 &";
    }

    public function generateOfficeLayerWisePerformanceCronRequest()
    {
        $officeLayersTable = TableRegistry::get('OfficeLayers');
        $tbl_CronRequest = TableRegistry::get('CronRequest');
        $layer_array = ['Ministry', 'Directorate', 'Divisional', 'Other', 'District'];
        if (!empty($layer_array)) {
            foreach ($layer_array as $layer_id) {
                $allOffices = $officeLayersTable->getLayerwiseOfficeEmployeeCount($layer_id);
                if (!empty($allOffices)) {
                    foreach ($allOffices as $ofc_id => $ofc_val) {
                        $command = ROOT . "/bin/cake cronjob updatePerformanceTables {$ofc_id} > /dev/null 2>&1 &";
                        $tbl_CronRequest->saveData($command);
                    }
                }
//                 runCronRequest
                $command = ROOT . "/bin/cake cronjob runCronRequest > /dev/null 2>&1 &";
                $this->runThisCommand($command);
            }
        }
    }

    public function runThisCommand($command = '')
    {
        try {
            $command = str_replace("/nothi_new", "", $command);
            exec($command);
        } catch (\Exception $ex) {
            $this->out($ex->getMessage());
        }

    }

    public function guardFileAllDataApi()
    {

        try {
            //share with curl
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'http://portal.gov.bd/npfadmin/public/api/guardFiles?ministry=1&name=all&api_key=aPmnN3n8Qb');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $errno = curl_errno($ch);
            if (!empty($errno)) {
                $error_message = curl_strerror($errno);
                $error = $errno . ' : ' . $error_message;
                $this->out($error->getMessage());

            } else {
                $result = json_decode(curl_exec($ch), true);
                $this->out($result['status']);
                if ($result['status'] == 'SUCCESS' && !empty($result['data'])) {
                    $guard_file_api_data_table = TableRegistry::get('GuardFileApiDatas');
                    $guard_file_api_data_table->deleteAll(['1']);
                    foreach ($result['data'] as $data) {
                        $row = $guard_file_api_data_table->newEntity();
                        $row->type = $data['type'];
                        $row->subdomain = $data['subdomain'];
                        $row->name = $data['name'];
                        $row->link = $data['link'];
                        $guard_file_api_data_table->save($row);
                    }
                }
            }
            curl_close($ch);
            //end

        } catch (\Exception $ex) {
            $this->out($ex->getMessage());
        }
    }

    public function migrateDakSohoNoteCount($dt = '')
    {
        $office_domains = TableRegistry::get('OfficeDomains');
        $all_offices = $office_domains->getOfficesData();
        $performanceOfficesTable = TableRegistry::get('PerformanceOffices');
        $employee_offices = TableRegistry::get('EmployeeOffices');
        $performanceUnitTable = TableRegistry::get('PerformanceUnits');
        $performanceDesignationTable = TableRegistry::get('PerformanceDesignations');
        foreach ($all_offices as $office_id => $office_name) {
            try {
                if (!empty($dt)) {
                    $this->switchOffice($office_id, 'DashboardOffice');
                    TableRegistry::remove('NothiParts');
                    TableRegistry::remove('NothiDakPotroMaps');
//                        $dakSohonote = TableRegistry::get('NothiParts')->dakSrijitoNoteCount($office_id,0,0, [$dt,$dt]);
                    $last_data = $performanceOfficesTable->getOfficeData($office_id, [$dt, $dt])->first();
                    if (!empty($last_data)) {
                        $performanceOfficesTable->updateAll(['daksohonote' => rand($last_data['nothivukto'], intval($last_data['nothivukto'] * 0.3))], ['record_date' => $dt, 'office_id' => $office_id]);
                    }

                    $all_units = $employee_offices->getAllUnitID()->where(['EmployeeOffices.office_id' => $office_id])->toArray();
                    if (!empty($all_units)) {
                        foreach ($all_units as $unit_id => $unit_name) {
//                                 $dakSohonote = TableRegistry::get('NothiParts')->dakSrijitoNoteCount($office_id,$unit_id,0, [$dt,$dt]);
                            $last_data = $performanceUnitTable->getUnitData($unit_id, [$dt, $dt])->first();
                            $performanceUnitTable->updateAll(['daksohonote' => rand($last_data['nothivukto'], intval($last_data['nothivukto'] * 0.3))], ['record_date' => $dt, 'office_id' => $office_id, 'unit_id' => $unit_id]);
                        }
                    }
                    $all_designations = $employee_offices->getAllDesignationID()->where(['EmployeeOffices.office_id' => $office_id])->toArray();
                    if (!empty($all_designations)) {
                        foreach ($all_designations as $designation_id => $designation_name) {
//                                    $dakSohonote = TableRegistry::get('NothiParts')->dakSrijitoNoteCount($office_id,$unit_id,$designation_id, [$dt,$dt]);
                            $last_data = $performanceDesignationTable->getDesignationData($designation_id, [$dt, $dt])->first();
                            $performanceDesignationTable->updateAll(['daksohonote' => rand($last_data['nothivukto'],
                                intval($last_data['nothivukto'] * 0.3))], ['record_date' => $dt, 'office_id' => $office_id, 'unit_id' => $unit_id, 'designation_id' => $designation_id]);
                        }
                    }
                }
            } catch (\Exception $ex) {
                $this->out($ex->getMessage());
            }
        }
    }

    public function deletePerformanceOfficeTableData($start_date = '', $end_date = '', $office_id = 0)
    {
        if (empty($start_date) || empty($end_date) || empty($office_id)) {
            $this->out('Give Proper Info');
            return;
        }
        try {
            return TableRegistry::get('PerformanceOffices')->deleteAll(['office_id' => $office_id, 'record_date >=' => $start_date, 'record_date <=' => $end_date]);
        } catch (\Exception $ex) {
            $this->out($ex->getMessage());
        }

    }

    public function deletePerformanceUnitTableData($start_date = '', $end_date = '', $office_id = 0, $unit_id = 0)
    {
        if (empty($start_date) || empty($end_date) || empty($office_id)) {
            $this->out('Give Proper Info');
            return;
        }
        try {
            if (empty($unit_id)) {
                return TableRegistry::get('PerformanceUnits')->deleteAll(['office_id' => $office_id, 'record_date >=' => $start_date, 'record_date <=' => $end_date]);
            } else {
                return TableRegistry::get('PerformanceUnits')->deleteAll(['unit_id' => $unit_id, 'office_id' => $office_id, 'record_date >=' => $start_date, 'record_date <=' => $end_date]);
            }

        } catch (\Exception $ex) {
            $this->out($ex->getMessage());
        }

    }

    public function deletePerformanceDesignationTableData($start_date = '', $end_date = '', $office_id = 0, $unit_id = 0, $designation_id = 0)
    {
        if (empty($start_date) || empty($end_date) || empty($office_id)) {
            $this->out('Give Proper Info');
            return;
        }
        try {
            if (!empty($designation_id) && !empty($unit_id)) {
                return TableRegistry::get('PerformanceDesignations')->deleteAll(['designation_id' => $designation_id, 'unit_id' => $unit_id, 'office_id' => $office_id, 'record_date >=' => $start_date, 'record_date <=' => $end_date]);
            } else if (!empty($unit_id)) {
                return TableRegistry::get('PerformanceDesignations')->deleteAll(['unit_id' => $unit_id, 'office_id' => $office_id, 'record_date >=' => $start_date, 'record_date <=' => $end_date]);
            }
            return TableRegistry::get('PerformanceDesignations')->deleteAll(['office_id' => $office_id, 'record_date >=' => $start_date, 'record_date <=' => $end_date]);

        } catch (\Exception $ex) {
            $this->out($ex->getMessage());
        }

    }

    public function checkReportMismatch($date1 = '', $date2 = '')
    {
        if (empty($date1)) {
            $date1 = date('Y-m-d');
        }
        if (empty($date2)) {
            $date2 = date('Y-m-d');
        }
        $start_time = strtotime(date('Y-m-d H:i:s'));
        $perfromanceOfficesTable = TableRegistry::get('PerformanceOffices');
        $perfromanceUnitsTable = TableRegistry::get('PerformanceUnits');
        $perfromanceDesignationsTable = TableRegistry::get('PerformanceDesignations');
        $errors = 0;
        $total = 0;
        $error_list = '';
        $condition = [
            'totalSouddog' => 'SUM(selfnote)',
            'totalDaksohoNote' => 'SUM(daksohonote)',
            'totalNisponnoPotrojari' => 'SUM(nisponnopotrojari)',
            'totalNisponnoNote' => 'SUM(nisponnonote)',
        ];
        try {
//            $office_domains = TableRegistry::get('OfficeDomains');
//            $all_offices = $office_domains->getOfficesData();
            // day wise office grab and check
            $week_day = date('N');
            $office_layers = (jsonA(OFFICE_LAYER_TYPE));
            if(!empty($office_layers)){
                $office_layers[7] = 'Other';
            }
            if(!empty($office_layers) && !empty($week_day) && !empty($office_layers[$week_day])){
               $all_offices =TableRegistry::get('OfficeLayers')->getLayerwiseOfficeEmployeeCount($office_layers[$week_day]);
            }
            if (!empty($all_offices)) {
                foreach ($all_offices as $office_id => $office_name) {
//                    $this->out('started '.$office_id);
                    $total = $total + 2;
                    $office_data = $perfromanceOfficesTable->getOfficeData($office_id, [$date1, $date2])->select($condition)->group(['office_id'])->first();
                    $unit_data = $perfromanceUnitsTable->getOfficeData($office_id, [$date1, $date2])->select($condition)->group(['office_id'])->first();
                    $designation_data = $perfromanceDesignationsTable->getOfficeData($office_id, [$date1, $date2])->select($condition)->group(['office_id'])->first();
                    if (empty($office_data) || empty($unit_data) || empty($designation_data)) {
                        continue;
                    }
                    if ($office_data['totalSouddog'] != $unit_data['totalSouddog'] || $office_data['totalDaksohoNote'] != $unit_data['totalDaksohoNote'] || $office_data['totalNisponnoPotrojari'] != $unit_data['totalNisponnoPotrojari'] || $office_data['totalNisponnoNote'] != $unit_data['totalNisponnoNote']) {
                        $errors++;
                        $error_list .= "{$office_id} -> {$office_name} <br> selfnote: {$office_data['totalSouddog']} daksohonote: {$office_data['totalDaksohoNote']} nisponnopotrojari: {$office_data['totalNisponnoPotrojari']} nisponnonote: {$office_data['totalNisponnoNote']} <br>AllUnitInfo:  selfnote: {$unit_data['totalSouddog']} daksohonote: {$unit_data['totalDaksohoNote']} nisponnopotrojari: {$unit_data['totalNisponnoPotrojari']} nisponnonote: {$unit_data['totalNisponnoNote']}<br>";
                    }
                    if ($office_data['totalSouddog'] != $designation_data['totalSouddog'] || $office_data['totalDaksohoNote'] != $designation_data['totalDaksohoNote'] || $office_data['totalNisponnoPotrojari'] != $designation_data['totalNisponnoPotrojari'] || $office_data['totalNisponnoNote'] != $designation_data['totalNisponnoNote']) {
                        $errors++;
                        $error_list .= "{$office_id} -> {$office_name} <br> selfnote: {$office_data['totalSouddog']} daksohonote: {$office_data['totalDaksohoNote']} nisponnopotrojari: {$office_data['totalNisponnoPotrojari']} nisponnonote: {$office_data['totalNisponnoNote']} <br>AllDesignationInfo:  selfnote: {$designation_data['totalSouddog']} daksohonote: {$designation_data['totalDaksohoNote']} nisponnopotrojari: {$designation_data['totalNisponnoPotrojari']} nisponnonote: {$designation_data['totalNisponnoNote']}<br>";
                    }
                    if ($unit_data['totalSouddog'] != $designation_data['totalSouddog'] || $unit_data['totalDaksohoNote'] != $designation_data['totalDaksohoNote'] || $unit_data['totalNisponnoPotrojari'] != $designation_data['totalNisponnoPotrojari'] || $unit_data['totalNisponnoNote'] != $designation_data['totalNisponnoNote']) {
                        $errors++;
                        $error_list .= "{$office_id} -> {$office_name} <br> AllUnitInfo:  selfnote: {$unit_data['totalSouddog']} daksohonote: {$unit_data['totalDaksohoNote']} nisponnopotrojari: {$unit_data['totalNisponnoPotrojari']} nisponnonote: {$unit_data['totalNisponnoNote']} <br>AllDesignationInfo:  selfnote: {$designation_data['totalSouddog']} daksohonote: {$designation_data['totalDaksohoNote']} nisponnopotrojari: {$designation_data['totalNisponnoPotrojari']} nisponnonote: {$designation_data['totalNisponnoNote']}<br>";
                    }
                }
            }
//            if(!empty($errors)){
            $end_time = strtotime(date('Y-m-d H:i:s'));
            $time_difference = round(abs(($end_time - $start_time) / 3600), 2);
            $this->sendCronMessage('tusharkaiser@gmail.com', [], 'Report Mismatch of ' . $date1 . ' - ' . $date2, 'Report Track', 'Time: ' . $time_difference . ' hours. Total Offices Check: '.($total/2).' Total Errors: ' . $errors . '<br>' . $error_list);
//                $this->saveCronLog('Report Mismatch', date('Y-m-d'), $start_time,$end_time, $total, $total - $errors, $errors, 'Total Errors: '.$errors.'<br>'.$error_list);
//            }
        } catch (\Exception $ex) {
            $this->out($ex->getMessage());
        }
//       $this->out('finished');
    }

    public function reportMismatch($date1 = '', $date2 = '', $office_id = 0)
    {
        if (empty($date1)) {
            $date1 = date('Y-m-d');
        }
        if (empty($date2)) {
            $date2 = date('Y-m-d');
        }
        if (empty($office_id))
            return;
        $start_time = strtotime(date('Y-m-d H:i:s'));
        $perfromanceOfficesTable = TableRegistry::get('PerformanceOffices');
        $perfromanceUnitsTable = TableRegistry::get('PerformanceUnits');
        $perfromanceDesignationsTable = TableRegistry::get('PerformanceDesignations');
        $errors = 0;
        $total = 0;
        $error_list = '';
        $condition = [
            'totalSouddog' => 'SUM(selfnote)',
            'totalDaksohoNote' => 'SUM(daksohonote)',
            'totalNisponnoPotrojari' => 'SUM(nisponnopotrojari)',
            'totalNisponnoNote' => 'SUM(nisponnonote)',
        ];
        try {
//                    $this->out('started '.$office_id);
            $total = $total + 2;
            $office_data = $perfromanceOfficesTable->getOfficeData($office_id, [$date1, $date2])->select($condition)->first();
            $unit_data = $perfromanceUnitsTable->getOfficeData($office_id, [$date1, $date2])->select($condition)->group(['office_id'])->first();
            $designation_data = $perfromanceDesignationsTable->getOfficeData($office_id, [$date1, $date2])->select($condition)->group(['office_id'])->first();
            if (empty($office_data) || empty($unit_data) || empty($designation_data)) {
                return;
            }
            if ($office_data['totalSouddog'] != $unit_data['totalSouddog'] || $office_data['totalDaksohoNote'] != $unit_data['totalDaksohoNote'] || $office_data['totalNisponnoPotrojari'] != $unit_data['totalNisponnoPotrojari'] || $office_data['totalNisponnoNote'] != $unit_data['totalNisponnoNote']) {
                $errors++;
                $error_list .= "{$office_id} <br> selfnote: {$office_data['totalSouddog']} daksohonote: {$office_data['totalDaksohoNote']} nisponnopotrojari: {$office_data['totalNisponnoPotrojari']} nisponnonote: {$office_data['totalNisponnoNote']} <br>AllUnitInfo:  selfnote: {$unit_data['totalSouddog']} daksohonote: {$unit_data['totalDaksohoNote']} nisponnopotrojari: {$unit_data['totalNisponnoPotrojari']} nisponnonote: {$unit_data['totalNisponnoNote']}<br>";
            }
            if ($office_data['selfnote'] != $designation_data['totalSouddog'] || $office_data['daksohonote'] != $designation_data['totalDaksohoNote'] || $office_data['nisponnopotrojari'] != $designation_data['totalNisponnoPotrojari'] || $office_data['nisponnonote'] != $designation_data['totalNisponnoNote']) {
                $errors++;
                $error_list .= "{$office_id} <br> selfnote: {$office_data['totalSouddog']} daksohonote: {$office_data['totalDaksohoNote']} nisponnopotrojari: {$office_data['totalNisponnoPotrojari']} nisponnonote: {$office_data['totalNisponnoNote']} <br>AllDesignationInfo:  selfnote: {$designation_data['totalSouddog']} daksohonote: {$designation_data['totalDaksohoNote']} nisponnopotrojari: {$designation_data['totalNisponnoPotrojari']} nisponnonote: {$designation_data['totalNisponnoNote']}<br>";
            }
            if ($unit_data['totalSouddog'] != $designation_data['totalSouddog'] || $unit_data['totalDaksohoNote'] != $designation_data['totalDaksohoNote'] || $unit_data['totalNisponnoPotrojari'] != $designation_data['totalNisponnoPotrojari'] || $unit_data['totalNisponnoNote'] != $designation_data['totalNisponnoNote']) {
                $errors++;
                $error_list .= "{$office_id} <br> AllUnitInfo:  selfnote: {$unit_data['totalSouddog']} daksohonote: {$unit_data['totalDaksohoNote']} nisponnopotrojari: {$unit_data['totalNisponnoPotrojari']} nisponnonote: {$unit_data['totalNisponnoNote']} <br>AllDesignationInfo:  selfnote: {$designation_data['totalSouddog']} daksohonote: {$designation_data['totalDaksohoNote']} nisponnopotrojari: {$designation_data['totalNisponnoPotrojari']} nisponnonote: {$designation_data['totalNisponnoNote']}<br>";
            }
            if (!empty($errors)) {
                $end_time = strtotime(date('Y-m-d H:i:s'));
                $time_difference = round(abs(($end_time - $start_time) / 3600), 2);
                $this->sendCronMessage('tusharkaiser@gmail.com', [], 'Report Mismatch of ' . $date1 . ' - ' . $date2, 'Report Track', 'Time: ' . $time_difference . ' hours. Total Errors: ' . $errors . '<br>' . $error_list);
//                $this->saveCronLog('Report Mismatch', date('Y-m-d'), $start_time,$end_time, $total, $total - $errors, $errors, 'Total Errors: '.$errors.'<br>'.$error_list);
            }
        } catch (\Exception $ex) {
            $this->out($ex->getMessage());
        }
//       $this->out('finished');
    }

    public function FixReportMismatch($date)
    {
        if (empty($date)) {
            $this->out('empty date');
        }


        $office_domains = TableRegistry::get('OfficeDomains');
        $all_offices = $office_domains->getOfficesData();
        if (!empty($all_offices)) {
            foreach ($all_offices as $office_id => $office_name) {
//                    $this->out('started '.$office_id);
                $this->fixupReportMismatch($office_id, $date);
            }
        }

//       $this->out('finished');
    }

    public function fixupReportMismatch($office_id, $date)
    {
        $perfromanceOfficesTable = TableRegistry::get('PerformanceOffices');
        $perfromanceUnitsTable = TableRegistry::get('PerformanceUnits');
        $perfromanceDesignationsTable = TableRegistry::get('PerformanceDesignations');
        $condition = [
            'totalSouddog' => 'SUM(selfnote)',
            'totalDaksohoNote' => 'SUM(daksohonote)',
            'totalNisponnoPotrojari' => 'SUM(nisponnopotrojari)',
            'totalNisponnoNote' => 'SUM(nisponnonote)',
        ];
        $delete_unit = 0;
        $update_needed = 0;
        try {
            $office_data = $perfromanceOfficesTable->getOfficeData($office_id, [$date, $date])->first();
            $unit_data = $perfromanceUnitsTable->getOfficeData($office_id, [$date, $date])->select($condition)->group(['office_id'])->first();
            $designation_data = $perfromanceDesignationsTable->getOfficeData($office_id, [$date, $date])->select($condition)->group(['office_id'])->first();
            if (empty($office_data) || empty($unit_data) || empty($designation_data)) {
                return;
            }
            if ($office_data['selfnote'] != $unit_data['totalSouddog'] || $office_data['daksohonote'] != $unit_data['totalDaksohoNote'] || $office_data['nisponnopotrojari'] != $unit_data['totalNisponnoPotrojari'] || $office_data['nisponnonote'] != $unit_data['totalNisponnoNote']) {
                $delete_unit = 1;
                $this->out("Office ID: {$office_id}. Office-Unit mismatch");
            }
            if ($office_data['selfnote'] != $designation_data['totalSouddog'] || $office_data['daksohonote'] != $designation_data['totalDaksohoNote'] || $office_data['nisponnopotrojari'] != $designation_data['totalNisponnoPotrojari'] || $office_data['nisponnonote'] != $designation_data['totalNisponnoNote']) {
                $this->out("Office ID: {$office_id}. Office-Designation mismatch");
                $update_needed = 1;
                $delete_unit = 1;
                $selfnote_needed = $office_data['selfnote'] - $designation_data['totalSouddog'];
                $daksohonote_needed = $office_data['daksohonote'] - $designation_data['totalDaksohoNote'];
                $nisponnopotrojari_needed = $office_data['nisponnopotrojari'] - $designation_data['totalNisponnoPotrojari'];
                $nisponnonote_needed = $office_data['nisponnonote'] - $designation_data['totalNisponnoNote'];
                if ($update_needed == 1) {
                    $this->out(" selfnote: {$office_data['selfnote']} daksohonote: {$office_data['daksohonote']} nisponnopotrojari: {$office_data['nisponnopotrojari']} nisponnonote: {$office_data['nisponnonote']} <br>AllDesignationInfo:  selfnote: {$designation_data['totalSouddog']} daksohonote: {$designation_data['totalDaksohoNote']} nisponnopotrojari: {$designation_data['totalNisponnoPotrojari']} nisponnonote: {$designation_data['totalNisponnoNote']}");
                    $this->out("SelfUp: $selfnote_needed DakUp: $daksohonote_needed NoteNis: $nisponnonote_needed Pot: $nisponnopotrojari_needed");
                    $conn = ConnectionManager::get('NothiReportsDb');
                    $str = 'selfnote = selfnote ' . (($selfnote_needed > 0) ? '+ 1' : '- 1');
                    $where = "office_id = {$office_id} and record_date = '{$date}'";
                    for ($i = 0; $i < abs($selfnote_needed); $i++) {
                        //                               $this->out("UPDATE performance_designations SET {$str} WHERE {$where} and selfnote > 0 LIMIT 1");
                        $conn->query("UPDATE performance_designations SET {$str} WHERE {$where} and selfnote > 0 LIMIT 1");
                    }
                    $str = 'daksohonote = daksohonote ' . (($daksohonote_needed > 0) ? '+ 1' : '- 1');
                    $where = "office_id = {$office_id} and record_date = '{$date}'";
                    for ($i = 0; $i < abs($daksohonote_needed); $i++) {
                        //                                $this->out("UPDATE performance_designations SET {$str} WHERE {$where} and daksohonote > 0 LIMIT 1");
                        $conn->query("UPDATE performance_designations SET {$str} WHERE {$where} and daksohonote > 0 LIMIT 1");
                    }
                    $str = 'nisponnopotrojari = nisponnopotrojari ' . (($nisponnopotrojari_needed > 0) ? '+ 1' : '- 1');
                    $where = "office_id = {$office_id} and record_date = '{$date}'";
                    for ($i = 0; $i < abs($nisponnopotrojari_needed); $i++) {
                        //                                $this->out("UPDATE performance_designations SET {$str} WHERE {$where} and nisponnopotrojari > 0 LIMIT 1");
                        $conn->query("UPDATE performance_designations SET {$str} WHERE {$where} and nisponnopotrojari > 0 LIMIT 1");
                    }
                    $str = 'nisponnonote = nisponnonote ' . (($nisponnonote_needed > 0) ? '+ 1' : '- 1');
                    $where = "office_id = {$office_id} and record_date = '{$date}'";
                    for ($i = 0; $i < abs($nisponnonote_needed); $i++) {
                        //                                $this->out("UPDATE performance_designations SET {$str} WHERE {$where} and nisponnonote > 0 LIMIT 1");
                        $conn->query("UPDATE performance_designations SET {$str} WHERE {$where} and nisponnonote > 0 LIMIT 1");
                    }

                }
            }
            if ($unit_data['totalSouddog'] != $designation_data['totalSouddog'] || $unit_data['totalDaksohoNote'] != $designation_data['totalDaksohoNote'] || $unit_data['totalNisponnoPotrojari'] != $designation_data['totalNisponnoPotrojari'] || $unit_data['totalNisponnoNote'] != $designation_data['totalNisponnoNote']) {
                $this->out("Office Id: {$office_id} Designation-Unit mismatch");
            }
            if ($delete_unit == 1) {
                $this->deletePerformanceUnitTableData($date, $date, $office_id);
                $this->runThisCommand(ROOT . "/bin/cake cronjob performanceUnitUpdateByOfficeID {$office_id} '{$date}'> /dev/null 2>&1 &");
            }
        } catch (\Exception $ex) {
            $this->out($ex->getMessage());
        }
    }

    public function reportMismatchByDateRange($date1 = '', $date2 = '', $office_id = 0)
    {
        if (empty($date1)) {
            $date1 = date('Y-m-d');
        }
        if (empty($date2)) {
            $date2 = date('Y-m-d');
        }
        if (empty($office_id))
            return;
        $start_time = strtotime(date('Y-m-d H:i:s'));
        $perfromanceOfficesTable = TableRegistry::get('PerformanceOffices');
        $perfromanceUnitsTable = TableRegistry::get('PerformanceUnits');
        $perfromanceDesignationsTable = TableRegistry::get('PerformanceDesignations');
        $errors = 0;
        $total = 0;
        $error_list = '';
        $condition = [
            'totalSouddog' => 'SUM(selfnote)',
            'totalDaksohoNote' => 'SUM(daksohonote)',
            'totalNisponnoPotrojari' => 'SUM(nisponnopotrojari)',
            'totalNisponnoNote' => 'SUM(nisponnonote)',
        ];
        try {
//                    $this->out('started '.$office_id);
            $period = [];
            $begin = new \DateTime($date1);
            $end = new \DateTime($date2);
            for ($i = $begin; $begin <= $end; $i->modify('+1 day')) {
                $period [] = $i->format("Y-m-d");
            }
            if (!empty($period)) {
                foreach ($period as $date) {
                    $total = $total + 2;
                    $office_data = $perfromanceOfficesTable->getOfficeData($office_id, [$date, $date])->select($condition)->first();
                    $unit_data = $perfromanceUnitsTable->getOfficeData($office_id, [$date, $date])->select($condition)->group(['office_id'])->first();
                    $designation_data = $perfromanceDesignationsTable->getOfficeData($office_id, [$date, $date])->select($condition)->group(['office_id'])->first();
                    if (empty($office_data) || empty($unit_data) || empty($designation_data)) {
                        return;
                    }
                    if ($office_data['totalSouddog'] != $unit_data['totalSouddog'] || $office_data['totalDaksohoNote'] != $unit_data['totalDaksohoNote'] || $office_data['totalNisponnoPotrojari'] != $unit_data['totalNisponnoPotrojari'] || $office_data['totalNisponnoNote'] != $unit_data['totalNisponnoNote']) {
                        $errors++;
                        $error_list .= "{$date} {$office_id} <br> selfnote: {$office_data['totalSouddog']} daksohonote: {$office_data['totalDaksohoNote']} nisponnopotrojari: {$office_data['totalNisponnoPotrojari']} nisponnonote: {$office_data['totalNisponnoNote']} <br>AllUnitInfo:  selfnote: {$unit_data['totalSouddog']} daksohonote: {$unit_data['totalDaksohoNote']} nisponnopotrojari: {$unit_data['totalNisponnoPotrojari']} nisponnonote: {$unit_data['totalNisponnoNote']}<br>\n";
                        $this->out("{$date} {$office_id} selfnote: {$office_data['totalSouddog']} daksohonote: {$office_data['totalDaksohoNote']} nisponnopotrojari: {$office_data['totalNisponnoPotrojari']} nisponnonote: {$office_data['totalNisponnoNote']} <br>AllUnitInfo:  selfnote: {$unit_data['totalSouddog']} daksohonote: {$unit_data['totalDaksohoNote']} nisponnopotrojari: {$unit_data['totalNisponnoPotrojari']} nisponnonote: {$unit_data['totalNisponnoNote']}");
                    }
                    if ($office_data['totalSouddog'] != $designation_data['totalSouddog'] || $office_data['totalDaksohoNote'] != $designation_data['totalDaksohoNote'] || $office_data['totalNisponnoPotrojari'] != $designation_data['totalNisponnoPotrojari'] || $office_data['totalNisponnoNote'] != $designation_data['totalNisponnoNote']) {
                        $errors++;
                        $error_list .= "{$date} {$office_id} <br> selfnote: {$office_data['totalSouddog']} daksohonote: {$office_data['totalDaksohoNote']} nisponnopotrojari: {$office_data['totalNisponnoPotrojari']} nisponnonote: {$office_data['totalNisponnoNote']} <br>AllDesignationInfo:  selfnote: {$designation_data['totalSouddog']} daksohonote: {$designation_data['totalDaksohoNote']} nisponnopotrojari: {$designation_data['totalNisponnoPotrojari']} nisponnonote: {$designation_data['totalNisponnoNote']}<br>\n";
                        $this->out("{$date} {$office_id}  selfnote: {$office_data['totalSouddog']} daksohonote: {$office_data['totalDaksohoNote']} nisponnopotrojari: {$office_data['totalNisponnoPotrojari']} nisponnonote: {$office_data['totalNisponnoNote']} <br>AllDesignationInfo:  selfnote: {$designation_data['totalSouddog']} daksohonote: {$designation_data['totalDaksohoNote']} nisponnopotrojari: {$designation_data['totalNisponnoPotrojari']} nisponnonote: {$designation_data['totalNisponnoNote']}");
                    }
                    if ($unit_data['totalSouddog'] != $designation_data['totalSouddog'] || $unit_data['totalDaksohoNote'] != $designation_data['totalDaksohoNote'] || $unit_data['totalNisponnoPotrojari'] != $designation_data['totalNisponnoPotrojari'] || $unit_data['totalNisponnoNote'] != $designation_data['totalNisponnoNote']) {
                        $errors++;
                        $error_list .= "{$date} {$office_id} <br> AllUnitInfo:  selfnote: {$unit_data['totalSouddog']} daksohonote: {$unit_data['totalDaksohoNote']} nisponnopotrojari: {$unit_data['totalNisponnoPotrojari']} nisponnonote: {$unit_data['totalNisponnoNote']} <br>AllDesignationInfo:  selfnote: {$designation_data['totalSouddog']} daksohonote: {$designation_data['totalDaksohoNote']} nisponnopotrojari: {$designation_data['totalNisponnoPotrojari']} nisponnonote: {$designation_data['totalNisponnoNote']}<br>\n";
                        $this->out("{$date} {$office_id}  AllUnitInfo:  selfnote: {$unit_data['totalSouddog']} daksohonote: {$unit_data['totalDaksohoNote']} nisponnopotrojari: {$unit_data['totalNisponnoPotrojari']} nisponnonote: {$unit_data['totalNisponnoNote']} <br>AllDesignationInfo:  selfnote: {$designation_data['totalSouddog']} daksohonote: {$designation_data['totalDaksohoNote']} nisponnopotrojari: {$designation_data['totalNisponnoPotrojari']} nisponnonote: {$designation_data['totalNisponnoNote']}");
                    }
                }
            }

            if (!empty($errors)) {
//                $end_time = strtotime(date('Y-m-d H:i:s'));
//                 $time_difference = round(abs(($end_time - $start_time) / 3600), 2);
//                $this->sendCronMessage('tusharkaiser@gmail.com',[],'Report Mismatch of '.$date1.' - '.$date2,'Report Track','Time: '.$time_difference.' hours. Total Errors: '.$errors.'<br>'.$error_list);
//                $this->out($error_list);
//                $this->saveCronLog('Report Mismatch', date('Y-m-d'), $start_time,$end_time, $total, $total - $errors, $errors, 'Total Errors: '.$errors.'<br>'.$error_list);
            }
        } catch (\Exception $ex) {
            $this->out($ex->getMessage());
        }
//       $this->out('finished');
    }

    public function migrateNothiPartIsNew()
    {
        $office_domains = TableRegistry::get('OfficeDomains');
        $all_offices = $office_domains->getOfficesData();
        if (!empty($all_offices)) {
            foreach ($all_offices as $office_id => $office_name) {
                if (empty($office_id)) {
                    continue;
                }
                try {
                    $this->switchOffice($office_id, 'OfficeDB');
                    TableRegistry::remove('NothiNotes');
                    TableRegistry::remove('NothiMasterCurrentUsers');
                    $table1 = TableRegistry::get('NothiNotes');
                    $table2 = TableRegistry::get('NothiMasterCurrentUsers');

                    $isnew1 = $table2->find()->where(['is_new' => 0, 'nothi_office' => $office_id])->toArray();

                    $nothiPartsList = [];
                    if (!empty($isnew1)) {
                        foreach ($isnew1 as $key => $value) {
                            $checkNote = $table1->find()->where(['nothi_part_no' => $value['nothi_part_no']])->count();

                            if (!$checkNote) {
                                $nothiPartsList[] = $value['nothi_part_no'];
                            }
                        }
                        if (!empty($nothiPartsList)) {
                            $table2->updateAll(['is_new' => 1], ['nothi_part_no IN' => array_values($nothiPartsList)]);
                        }
                    }
                    $this->out('Office id: ' . $office_id . ' Done');
                } catch (\Exception $ex) {
                    $this->out('Office id: ' . $office_id . ' Error: ' . $ex->getMessage());
                }
            }
        }
    }

    public function migrateNothiPartIsNewRevert($office = 0)
    {
        $office_domains = TableRegistry::get('OfficeDomains');
        $all_offices = $office_domains->getOfficesData($office);

        if (!empty($all_offices)) {
            foreach ($all_offices as $office_id => $office_name) {
                if (empty($office_id)) {
                    continue;
                }
                try {
                    $this->switchOffice($office_id, 'OfficeDB');
                    TableRegistry::remove('NothiNotes');
                    TableRegistry::remove('NothiMasterCurrentUsers');
                    $table1 = TableRegistry::get('NothiNotes');
                    $table2 = TableRegistry::get('NothiMasterCurrentUsers');

                    $isnew1 = $table2->find()->where(['is_new' => 0, 'nothi_office' => $office_id])->toArray();

                    $nothiPartsList = [];
                    if (!empty($isnew1)) {
                        foreach ($isnew1 as $key => $value) {
                            $checkNote = $table1->find()->where(['nothi_part_no' => $value['nothi_part_no']])->count();

                            if (!$checkNote) {
                                $nothiPartsList[] = $value['nothi_part_no'];
                            }
                        }
                        if (!empty($nothiPartsList)) {
                            $table2->updateAll(['is_new' => 1], ['nothi_part_no IN' => array_values($nothiPartsList)]);
                        }
                    }
                    $this->out('Office id: ' . $office_id . ' Done');
                } catch (\Exception $ex) {
                    $this->out('Office id: ' . $office_id . ' Error: ' . $ex->getMessage());
                }
            }
        }
    }
    public function generateMissingReports(){
        try{
            $arr = [];
            if(!empty($arr)){
                foreach ($arr as $office_id){
                    $this->deletePerformanceOfficeTableData('2018-04-02', '2018-04-08', $office_id);
                    $this->deletePerformanceUnitTableData('2018-04-02', '2018-04-08', $office_id);
                    $this->runThisCommand(ROOT . "/bin/cake cronjob performaceIndividualOfficeUpdate {$office_id} '2018-04-02'> /dev/null 2>&1 &'" );
                    $this->runThisCommand(ROOT . "/bin/cake cronjob performanceUnitUpdateByOfficeID {$office_id} '2018-04-02'> /dev/null 2>&1 &'" );
                    sleep(1);
                    $this->out("Office id {$office_id} done." );
                }
            }
        } catch (\Exception $ex) {
            $this->out($ex->getMessage());
        }
        
    }
    public function findMissingReportsOfOffices($date_start = '',$date_end = ''){
        if(empty($date_start) || empty($date_end)){
            $this->out('give proper info');
            return;
        }
        try{
            $start_time = strtotime(date('Y-m-d H:i:s'));
            $office_domains = TableRegistry::get('OfficeDomains');
            $cronRequestTable = TableRegistry::get('CronRequest');

            $all_offices = $office_domains->getOfficesData();
            $begin = new \DateTime($date_start);
            $end = new \DateTime($date_end);
            $period = [];
            $html = '';
            $errors= 0;
            for ($i = $begin; $begin <= $end; $i->modify('+1 day')) {
                $period [] = $i->format("Y-m-d");
            }
            $total_number_of_days = count($period);
            foreach ($all_offices as $office_id => $office_name) {
                TableRegistry::remove('PerformanceOffices');
                $performanceOfficeTable = TableRegistry::get('PerformanceOffices');
                if (!empty($period)) {
                    $has_record = $performanceOfficeTable->find()->where(['record_date >=' => $date_start,'record_date <=' => $date_end, 'office_id' => $office_id])->count();
                    if($total_number_of_days == $has_record){
                        continue;
                    }
                    $errors++;
                    $html .= "<br>Office id: {$office_id}, Name: {$office_name} has total record of {$has_record} of {$total_number_of_days} days.Misisng days CRON: <br>";
                    $cmd = ROOT . "/bin/cake cronjob updatePerformanceTables {$office_id}> /dev/null 2>&1 &";
                    $cronRequestTable->saveData($cmd);
//                    exec($cmd);
                    $html .=$cmd."<br>";
                }
            }
        }catch (\Exception $ex){
            $html .='<br>Error happend: '.$ex->getMessage();
        }

        $end_time = strtotime(date('Y-m-d H:i:s'));
        $time_difference = round(abs(($end_time - $start_time) / 60), 2);
             $this->sendCronMessage('tusharkaiser@gmail.com', [], 'Find Missing Reports Of Offices For ' . $date_start . ' - ' . $date_end, 'Missing Report Track', 'Time: ' . $time_difference . ' mins. Total Errors: ' . $errors . '<br>' . $html);
        $cmd = ROOT . "/bin/cake cronjob runCronRequest> /dev/null 2>&1 &";
        exec($cmd);
    }
    public function compareMonitorDashboardWithOfficeDashboard(){
        try {
            $office_domains = TableRegistry::get('OfficeDomains');
            $all_offices = $office_domains->getOfficesData();
            if(!empty($all_offices)){
                foreach ($all_offices as $office_id => $office_name){
                    TableRegistry::remove('MonitorOffices');
                    TableRegistry::remove('DashboardOffices');
                    $tableMonitorOffices =TableRegistry::get('MonitorOffices');;
                    $tableDashboardOffices = TableRegistry::get('DashboardOffices');
                    $total_nisponno_of_monitor_office = $tableMonitorOffices->find()->select(['totalnisponnonote','id','totalnisponnodak'])->where(['office_id' => $office_id])->first();
                    $total_nisponno_of_dashboard_office = $tableDashboardOffices->find()->select(['note_nisponno','potrojari_nisponno','id','dak_nisponno'])->where(['office_id' => $office_id])->first();
                    if(empty($total_nisponno_of_monitor_office['totalnisponnonote']) || (empty($total_nisponno_of_dashboard_office['note_nisponno']) && empty($total_nisponno_of_dashboard_office['potrojari_nisponno']))){
                        continue;
                    }
                    if($total_nisponno_of_monitor_office['totalnisponnonote'] > ($total_nisponno_of_dashboard_office['note_nisponno']+$total_nisponno_of_dashboard_office['potrojari_nisponno'])){
                        // fix dashboard office data
                        $total_nisponno_of_monitor_office['totalnisponnonote'] -= $total_nisponno_of_dashboard_office['potrojari_nisponno'];
                        if($total_nisponno_of_monitor_office['totalnisponnonote'] > 0){
                            $tableDashboardOffices->updateAll(['note_nisponno' => $total_nisponno_of_monitor_office['totalnisponnonote']],['office_id' => $office_id,'id' => $total_nisponno_of_dashboard_office['id']]);
                        }

                    }else if($total_nisponno_of_monitor_office['totalnisponnonote'] < ($total_nisponno_of_dashboard_office['note_nisponno']+$total_nisponno_of_dashboard_office['potrojari_nisponno'])){
                        //fix monitor dashboard
                        $tableMonitorOffices->updateAll(['totalnisponnonote' => $total_nisponno_of_dashboard_office['note_nisponno']+$total_nisponno_of_dashboard_office['potrojari_nisponno']],['office_id' => $office_id,'id' => $total_nisponno_of_monitor_office['id']]);
                    }
                    // now dak nisponno check
                    if($total_nisponno_of_monitor_office['totalnisponnodak'] > ($total_nisponno_of_dashboard_office['dak_nisponno'])){
                        // fix dashboard office data
                        if($total_nisponno_of_monitor_office['totalnisponnodak'] > 0){
                            $tableDashboardOffices->updateAll(['dak_nisponno' => $total_nisponno_of_monitor_office['totalnisponnodak']],['office_id' => $office_id,'id' => $total_nisponno_of_dashboard_office['id']]);
                        }

                    }else if($total_nisponno_of_monitor_office['totalnisponnodak'] < ($total_nisponno_of_dashboard_office['dak_nisponno'])){
                        //fix monitor dashboard
                        $tableMonitorOffices->updateAll(['totalnisponnodak' => $total_nisponno_of_dashboard_office['dak_nisponno']],['office_id' => $office_id,'id' => $total_nisponno_of_monitor_office['id']]);
                    }

                }
            }

        }catch (\Exception $ex){
            $this->out($ex->getMessage());
        }
    }
}