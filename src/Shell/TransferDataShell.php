<?php

namespace App\Shell;

use App\Controller\Component\TransferData;
use Cake\Console\Shell;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use App\Controller\ProjapotiController;
use Cake\Datasource\ConnectionManager;
use Cake\I18n\I18n;
use Exception;
use Cake\Cache\Cache;

/**
 * Report shell command.
 */
class TransferDataShell extends ProjapotiShell
{
	use TransferData;

    /**
     * main() method.
     *
     * @return bool|int Success or error code.
     */
    public function main()
    {

    }

    private function commands($office_id, $browser, $auth_user)
    {
        $command = "bin/cake API saveLoginHistory {$office_id} {$browser} {$auth_user}> /dev/null 2>&1 &";
    }

	public function countUserActivitiesDetailsCount($designation_id = false, $is_internal = false, $office_id)
	{
		$this->switchOffice($office_id, 'OfficeDb');
		if (!$designation_id) {
			$designation_id = $this->request->data['designation_id'];
		}
		$response = [];
		$response['dak_inbox'] = TableRegistry::get('DakUsers')->find()->where(['to_officer_designation_id' => $designation_id, 'is_archive' => 0, 'dak_category' => 'Inbox'])->count();
		$response['nothi_inbox'] = TableRegistry::get('NothiMasterCurrentUsers')->find()->where(['office_unit_organogram_id' => $designation_id, 'is_archive' => 0])->count();
		$response['nothi_sent'] = TableRegistry::get('NothiMasterMovements')->find()->where(['from_officer_designation_id' => $designation_id])->count();
		$response['nothi_other_sent'] = TableRegistry::get('OtherOfficeNothiMasterMovements')->find()->where(['from_officer_designation_id' => $designation_id])->count();

		if ($is_internal) {
			return $response;
		} else {
			$this->response->body(json_encode($response));
			$this->response->type('json');
			return $this->response;
		}
	}

    public function officeTransfer($index, $auth_user) {
		if (!empty($index)) {
			$request_data = Cache::read('office_transfer_request_data' . $index, 'memcached');
			if (!empty($request_data)) {
			    //please remove if success
//				Cache::delete('office_transfer_request_data' . $index, 'memcached');
			} else {
				$this->out('No data');
				return;
			}

			$this->out('Process for '.$request_data['selectedDesignation']);

			$officeToTransfer = intval($request_data['destinationOffice']);
			$selectedDesignations = (array)$request_data['selectedDesignation'];
			$from_unit_ids = (array)$request_data['from_unit'];
			$table_instance_unit_org = TableRegistry::get('OfficeUnitOrganograms');
			$table_instance_unit = TableRegistry::get('OfficeUnits');
			$table_instance_unit_origin = TableRegistry::get('OfficeOriginUnits');
			$table_instance_office = TableRegistry::get('Offices');

			$this->authUserId = $auth_user;
			$this->transferUnit = $from_unit_ids;
			$this->transferFromOffice = $request_data['office_id'];
			$this->employeeOffice = ['office_id' => $officeToTransfer];
			$this->will_move = false;

			$selectedDetailDesignations = $table_instance_unit_org->getAll(['id IN' => $selectedDesignations])->toArray();
			if ($selectedDetailDesignations) {
				$this->officeInformation = $table_instance_office->get($officeToTransfer);
				$connection = ConnectionManager::get('projapotiDb');
				try {
					$this->out('Process started....');
					$connection->begin();
					$units = [];
					#pr($selectedDesignations);
					#pr($selectedDetailDesignations);
					#pr($officeToTransfer);

					foreach ($selectedDetailDesignations as $key => &$value) {
						if ($value['office_id'] == $officeToTransfer) {
							//cannot move to same unit again
							continue;
						}
						if (!in_array($value['office_unit_id'], $units)) {
							$units[] = $value['office_unit_id'];
						}
						$this->unitInformation = $table_instance_unit->get($value['office_unit_id']);
						$existOrigin = $table_instance_unit_origin->find()->where(['office_ministry_id' => $this->officeInformation['office_ministry_id'], 'office_origin_id' => $this->officeInformation['office_origin_id'], 'active_status' => 1, 'unit_name_bng' => $this->unitInformation['unit_name_bng']])->first();
						if (empty($existOrigin)) {
							$exist = $this->createOriginUnit();
							$this->out('Unit origin created');
						} else {
							$exist = $table_instance_unit->find()->where(['office_id' => $this->officeInformation['id'], 'office_origin_unit_id' => $existOrigin['id'], 'active_status' => 1, 'unit_name_bng' => $this->unitInformation['unit_name_bng']])->first();
							if (empty($exist)) {
								$exist = $this->createUnit($existOrigin);
								$this->out('Unit created');
							}
						}
						$value['previous_office_unit_id'] = $value['office_unit_id'];
						$value['office_unit_id'] = $exist['id'];
					}
					$connection->commit();
					$connection->begin();
					$this->unitInformation = null;

					if (!empty($units)) {
						foreach ($selectedDetailDesignations as $key => $value) {
							if ($value['office_id'] == $officeToTransfer) {
								//cannot move to same unit again
								continue;
							}
							$this->unitInformation = $table_instance_unit->get($value['office_unit_id']);
							$this->selectedDesignation = $value;
							$this->selectedUnit = $value['office_unit_id'];
							$this->out('Process ongoing...');
							$success = $this->initiateTransfer();

							//finally change status

							if (!$success) {
								throw new \Exception('Sorry! Office segregation is not process at this moment');
								break;
							}
						}
					}
					$connection->commit();
					$this->out('Successfully office segregate');
				} catch (\Exception $ex) {
					$connection->rollback();
					$this->out($ex->getMessage());
				}
			} else {
				$this->out('This organogram not found as active');
			}
		}
	}

    public function officeTransfer2($index, $auth_user) {
		if (!empty($index)) {
			$request_data = Cache::read('office_transfer_request_data' . $index);
			if (!empty($request_data)) {
			    //please remove if success
				//Cache::delete('office_transfer_request_data' . $index);
			} else {
				$this->out('No data');
				return;
			}

			$prev_office_id = $request_data['prev_office_id'];
			$new_office_id = $request_data['new_office_id'];
			$where_organogram_id = $request_data['where_organogram_id'];

			$table_instance_unit_org = TableRegistry::get('OfficeUnitOrganograms');
			$table_instance_unit = TableRegistry::get('OfficeUnits');
			$table_instance_unit_origin = TableRegistry::get('OfficeOriginUnits');
			$table_instance_office = TableRegistry::get('Offices');

			$this->authUserId = $auth_user;
			//$this->transferUnit = $from_unit_ids;
			//$this->transferFromOffice = $request_data['office_id'];
			//$this->employeeOffice = ['office_id' => $officeToTransfer];
			$this->will_move = false;


			$success = false;
			$previous_designations_with_details = $table_instance_unit_org->getAll(['id IN' => array_keys($where_organogram_id)])->toArray();
			if ($previous_designations_with_details) {

				$previous_units = [];
				foreach ($previous_designations_with_details as $designation) {
					$previous_units[] = $designation['office_unit_id'];
				}
				$previous_units = array_unique($previous_units);

				$new_designations_with_details = $table_instance_unit_org->getAll(['id IN' => array_values($where_organogram_id)])->toArray();
				$new_units = [];
				foreach ($new_designations_with_details as $designation) {
					$new_units[] = $designation['office_unit_id'];
				}
				$new_units = array_unique($new_units);

				$connection_projapoti_db = ConnectionManager::get('projapotiDb');
				//$connection_projapoti_db->begin();

                $template = 'notification';
                $layout = 'default';

				try {
					$this->out('Process started....');

					/*$this->switchOffice($prev_office_id, 'OldOffice');
					$connection_old_office = ConnectionManager::get('OldOffice');
					$connection_old_office->begin();

					$this->switchOffice($new_office_id, 'NewOffice');
					$connection_new_office = ConnectionManager::get('NewOffice');
					$connection_new_office->begin();*/

					/// After that transfer process start
					/// 1st => All data transfer from CURRENT OFFICE to NEW OFFICE
                    //$success['status'] = 'success';
                    $success = $this->removeAlterOfModified($new_office_id);
                    if ($success['status'] == 'success') {
                        pr('Done pre step');

                        $success = $this->transferOfficeAllDataByUnitIds($previous_units, $prev_office_id, $new_office_id);
                        if ($success['status'] == 'success') {
                            pr('Done 1st step');

                            /// 2nd => Unassign/Assign USER
                            ///  	=> Potrojari-Group Update
                            /// 	=> Protikolpo Update
                            ///  	=> Update organogram id, unit id, office id NEW OFFICE
                            $success = $this->updateWithNewOrganograms($where_organogram_id, $prev_office_id, $new_office_id);
                            if ($success['status'] == 'success') {
                                pr('Done 2nd step');

                                /// 3rd => Update CURRENT OFFICE permission table
                                $success = $this->updateOtherOfficePrevOwnOfficePermissions($prev_office_id, $new_office_id, $where_organogram_id, $new_units, $previous_units);
                                if ($success['status'] == 'success') {
                                    pr('Done 3rd step');

                                    /// 4th => Previous own office other section MIGRATE to other office
                                    $success = $this->updateOtherOfficePermissions($prev_office_id, $new_office_id, $where_organogram_id);
                                    if ($success['status'] == 'success') {
                                        pr('Done 4th step');

                                        $success = $this->updateUnitId($prev_office_id, $new_office_id, $where_organogram_id, $new_units, $previous_units);
                                        if ($success['status'] == 'success') {
                                            pr('Done 5th step');

                                            $this->send_sms_instant('01731934573', "Office segregation successful.
-
Nothi Team");
                                            $this->sendCronMessage('jbhasan@gmail.com', 'jafrin.ahammed@a2i.gov.bd', 'SEGREGATION :: Done', 'nothi@nothi.org.bd', 'Office segregation successful', 'html', $layout, $template, 'SEGREGATION :: Done', true);
                                        } else {
                                            pr($success);

                                            $subject = 'SEGREGATION :: Error in 5th step';
                                            $body_subject = 'Error in 5th step';
                                            $message = $success['msg'];
                                        }
                                    } else {
                                        pr($success);

                                        $subject = 'SEGREGATION :: Error in 4th step';
                                        $body_subject = 'Error in 4th step';
                                        $message = $success['msg'];
                                    }
                                } else {
                                    pr($success);

                                    $subject = 'SEGREGATION :: Error in 3rd step';
                                    $body_subject = 'Error in 3rd step';
                                    $message = $success['msg'];
                                }
                            } else {
                                pr($success);

                                $subject = 'SEGREGATION :: Error in 2nd step';
                                $body_subject = 'Error in 2nd step';
                                $message = $success['msg'];
                            }
                        } else {
                            pr($success);

                            $subject = 'SEGREGATION :: Error in 1st step';
                            $body_subject = 'Error in 1st step';
                            $message = $success['msg'];
                        }
                    }


					/*$connection_projapoti_db->commit();
					$connection_old_office->commit();
					$connection_new_office->commit();*/
					if ($success['status'] == 'success') {
                        $this->out('Successfully office segregate');
                    } else {
                        $this->out('Some problem occurs when office segregate');

                        $this->sendCronMessage('jbhasan@gmail.com', 'jafrin.ahammed@a2i.gov.bd', $subject, 'nothi@nothi.org.bd', $message, 'html', $layout, $template, $body_subject, true, $bcc = null);
                    }
				} catch (\Exception $ex) {
					/*$connection_projapoti_db->rollback();
					$connection_old_office->rollback();
					$connection_new_office->rollback();*/
					$this->out($ex->getMessage());

                    $subject = 'SEGREGATION :: Error in Warm-up';
                    $body_subject = 'Error in warm-up';
                    $message = $ex->getMessage();

                    $this->sendCronMessage('jbhasan@gmail.com', 'jafrin.ahammed@a2i.gov.bd', $subject, 'nothi@nothi.org.bd', $message, 'html', $layout, $template, $body_subject, true, $bcc = null);
				}
			} else {
				$this->out('This organogram not found as active');
			}
		}
	}

    private function send_sms_instant($mobile_no, $messgae) {
        $messgae = urlencode($messgae);
        $url = "https://bulksms.teletalk.com.bd/link_sms_send.php?op=SMS&user=nothi_user&pass=R6uW9vRs&mobile={$mobile_no}&charset=UTF-8&sms={$messgae}";

        $curl_handle = curl_init();
        curl_setopt($curl_handle, CURLOPT_URL, $url);
        curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, false);
        curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Nothi');
        curl_exec($curl_handle);
        curl_close($curl_handle);

        return true;
    }
}