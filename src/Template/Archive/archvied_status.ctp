<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
	        আর্কাইভকৃত নথিসমূহের অবস্থা
        </div>
    </div>
    <div class="portlet-body">
        <?php if (!empty($return_data)): $i=0; ?>
            <div class="table-scrollable" style="border:none !important;">
                <ul class="nav nav-tabs" style="margin-bottom:0 !important;">
                    <?php foreach ($return_data as $nothi_class => $data_list): $i++; ?>
                        <li class="<?= $i==1?'active':'' ?>"><a data-toggle="tab" href="#home_<?=$nothi_class?>"><?=nothi_class_name($nothi_class)?> শ্রেণির নথিসমূহ</a></li>
                    <?php endforeach; ?>
                </ul>

                <div class="tab-content">
                    <?php $i=0; foreach ($return_data as $nothi_class => $data_list): $i++; ?>

                        <div id="home_<?=$nothi_class?>" class="tab-pane fade <?= $i==1?'in active':'' ?>">
                            <table class="table table-bordered table-hover">
                                <thead>
                                <tr class="text-center heading">
                                    <th class="text-center">ক্রম</th>
                                    <th class="text-center">নথি নং</th>
                                    <th class="text-center">নথির বিষয়</th>
                                    <th class="text-center">অবস্থা</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($data_list as $key => $data): ?>
                                    <tr class="text-center">
                                        <td><?=enTobn($key+1)?></td>
                                        <td><?=$data['nothi_no']?></td>
                                        <td><?=$data['subject']?></td>
                                        <td><?php
                                            if ($data['archive_status']==1) {
                                                echo "অপেক্ষমাণ";
                                            } elseif($data['archive_status']==2) {
                                                echo "চলমান আছে";
                                            } elseif($data['archive_status']==3) {
                                                echo "আর্কাইভ হয়েছে";
                                            } elseif($data['archive_status']==2) {
                                                echo "হয়নি";
                                            }
                                        ?></td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>

                    <?php endforeach; ?>
                </div>
            </div>
            <?php else: ?>
                <div class="alert alert-danger">আর্কাইভকৃত কোন নথি নাই।</div>
        <?php endif; ?>
    </div>
</div>
