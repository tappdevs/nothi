<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
            স্তরভিত্তিক নিষ্পত্তিযোগ্য অনিষ্পন্ন নথি সংখ্যা
        </div>
    </div>
    <div class="portlet-body">
        <?php if (!empty($layers)): $i=0; ?>
            <div style="border:none !important;">
                <select name="office_layer" id="office_layer" style="width:250px">
                    <option value="">-- স্তর বাছাই করুন --</option>
                    <?php foreach($layers as $layer_id => $layer_name): ?>
                    <option value="<?=$layer_id?>"><?=$layer_name?></option>
                    <?php endforeach; ?>
                    <option value="0">অন্যান্য</option>
                </select>
                <select name="office_ministry" id="office_ministry" style="width:250px">
                    <option value="">-- মন্ত্রণালয় বাছাই করুন --</option>
                    <?php foreach($ministries as $ministry_id => $ministry_name): ?>
                        <option value="<?=$ministry_id?>"><?=$ministry_name?></option>
                    <?php endforeach; ?>
                </select>
                <button class="btn btn-success btn-sm" onclick="showLayerWiseList(office_layer.value, office_ministry.value)">তথ্য দেখুন</button>
                <span id="loading" style="display: none;"><i class="fa fa-spinner fa-pulse"></i> লোড হচ্ছে, অনুগ্রহ করে অপেক্ষা করুন...</span>
            </div>
            <div style="margin-top:10px;">
                <table id="showLayerWiseReportTable" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>ক্রম</th>
                        <th>অফিসের নাম</th>
                        <th>অনিষ্পন্ন নথির সংখ্যা</th>
                        <th>অনিষ্পন্ন নোটের সংখ্যা</th>
                        <th>কার্যক্রম</th>
                    </tr>
                    </thead>

                    <tbody></tbody>
                </table>
            </div>
            <?php else: ?>
                <div class="alert alert-danger">আর্কাইভের জন্য অপেক্ষমাণ কোন নথি নাই।</div>
        <?php endif; ?>
    </div>
</div>

<div class="modal fade modal-purple" role="dialog" id="unitWiseReportModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Office Nmae</h4>
            </div>

            <div class="modal-body">
                <i class="fa fa-spinner fa-pulse"></i> লোড হচ্ছে, অনুগ্রহ করে অপেক্ষা করুন...
            </div>
        </div>
    </div>
</div>
<script>
    function showLayerWiseList(layerId, ministryId) {
        if (layerId != '') {
            $("#loading").show();
            $("#showLayerWiseReportTable").find('tbody').html('<tr><td colspan="5"><i class="fa fa-spinner fa-pulse"></i> লোড হচ্ছে...</td></tr>');
            Metronic.blockUI({
                target: '#showLayerWiseReportTable',
                boxed: true,
                message: 'অপেক্ষা করুন'
            });
            $.ajax({
                url: js_wb_root+'layerWiseOfficeList',
                method: "post",
                dataType: 'JSON',
                data: {'layer_id':layerId, 'ministry_id':ministryId}
            }).success(function (offices) {
                $("#showLayerWiseReportTable").find('tbody').html('');
                var sl = 0;
                $.each(offices, function (officeId, officeNameBng) {
                    sl++;
 
                    $("#showLayerWiseReportTable").find('tbody').append('<tr><td>'+BnFromEng(sl)+'</td><td>' + officeNameBng + '</td><td id="nothi_count_' + officeId + '">লোড হচ্ছে...</td><td id="note_count_' + officeId + '">লোড হচ্ছে...</td><td><button class="btn btn-success btn-sm" onclick="showUnitWiseReport('+officeId+', \'' + officeNameBng + '\')">শাখাভিত্তিক প্রতিবেদন</button></td></tr>');
                    $.ajax({
                        url: js_wb_root+'layerOfficeWiseArchiveReport',
                        method: "post",
                        data: {'office_id': officeId},
                        dataType: 'JSON',
                        async:true
                    }).success(function (response) {
                        if(response == 'db-error') {
                            $("#nothi_count_" + officeId).parent().find('button').addClass('disabled');
                            $("#nothi_count_" + officeId).parent().find('button').removeAttr('onclick');
                            $("#nothi_count_" + officeId).parent().find('button').removeClass('btn-success');
                            $("#nothi_count_" + officeId).parent().find('button').addClass('btn-default');
                            $("#nothi_count_" + officeId).parent().css('cursor','not-allowed');
                            $("#nothi_count_" + officeId).html('<span style="color:red;">অফিস ডাটাবেজ-এ সংযোগ স্থাপন করা সম্ভব হচ্ছে না।</span>');
                            $("#note_count_" + officeId).html('');
                        } else {
                            $("#nothi_count_" + officeId).html(BnFromEng(response.nothi));
                            $("#note_count_" + officeId).html(BnFromEng(response.note));
                        }
                    });
                });
                $("#loading").hide();
            });
            $( document ).ajaxStop(function() {
                Metronic.unblockUI('#showLayerWiseReportTable');
            });
        } else {
            toastr.error('অনুগ্রহ করে স্তর বাছাই করুন')
        }
    }

    function showUnitWiseReport(officeId, officeNameBng) {
        $("#unitWiseReportModal").modal('show');
        $("#unitWiseReportModal").find('.modal-title').html(officeNameBng+' এর শাখাভিত্তিক নিষ্পত্তিযোগ্য অনিষ্পন্ন নথি');

        $.ajax({
            url: js_wb_root+'layerOfficeUnitWiseArchiveReport',
            method: "post",
            data: {'office_id': officeId},
            dataType: 'JSON'
        }).success(function (report) {
            $("#unitWiseReportModal").find('.modal-body').html('<table id="showUnitWiseReportTable" class="table table-bordered table-striped"><thead><tr><th>ক্রম</th><th>শাখার নাম</th><th>নথির সংখ্যা</th><th>নোটের সংখ্যা</th></tr></thead><tbody></tbody></table>');
            var sl = 0;
            $.each(report, function(unitNameBng, unitNothiCount) {
                sl++;
                $("#showUnitWiseReportTable").find('tbody').append('<tr><td>'+BnFromEng(sl)+'</td><td>' + unitNameBng + '</td><td><a href="javascript:" onclick="showNothiName(this, '+unitNothiCount.unit_id+')">'+BnFromEng(unitNothiCount.nothi)+'</a><i class="fa fa-spinner fa-pulse fa-fw" style="display:none;"></i><button data-toggle="collapse" data-target="#nothiNameList_'+sl+'" style="display:none"></button><div id="nothiNameList_'+sl+'" class="collapse">Loading...</div></td><td>'+BnFromEng(unitNothiCount.note)+'</td></tr>');
            });

        });
    }

    function showNothiName(elm, unitId) {
        var button = $(elm).parent().find('button');
        var div = $(elm).parent().find('div');
        var loading = $(elm).parent().find('i');
        loading.show();
        div.html('');
        if(div.css('display') == 'none') {
            $.ajax({
                url: js_wb_root+'getNothiNameByUnitId',
                method: 'POST',
                data: {'unit_id': unitId},
                dataType: 'JSON'
            }).success(function (response) {
                var table = '<table class="table table-bordered"><tbody>';
                var serial = 0;
                $.each(response, function (key, value) {
                    serial++;
                    table += '<tr><td>'+BnFromEng(serial)+'</td><td>' + value.nothi_no + '</td><td>' + value.subject + '</td></tr>';
                });
                table += '</tbody></table>';
                div.append(table);
                button.trigger('click');
                loading.hide();
            });
        } else {
            button.trigger('click');
            loading.hide();
        }
    }
</script>
