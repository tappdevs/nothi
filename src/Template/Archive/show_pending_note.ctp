<style>
    #table-head-inverse>th {
        background-color: gray;
        color: white;
    }
</style>
<?php
if (isset($return_data) && count($return_data) > 0) {
    ?>
    <h3>অনিষ্পন্ন নোটসমূহ</h3>
    <table class="table table-bordered table-striped table-responsive table-hover">
        <thead id="table-head-inverse">
        <tr>
            <th>ক্রম</th>
            <th>নোট নং</th>
            <th>নোটের বিষয়</th>
            <th>নোটের বর্তমান অবস্থান</th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($return_data as $key => $data) {
            ?>
            <tr>
                <td><?=enTobn($key+1)?></td>
                <td><?=$data['note_no']?></td>
                <td><?=$data['note_name']?></td>
                <td><?=$data['designation_bng']?>, <?=$data['unit_bng']?></td>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>
    <?php
} else {
    ?><h4>No data found</h4><?php
}