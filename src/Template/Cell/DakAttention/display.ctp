<div class="row">
    <div class="col-md-12">
        <h3 class="form-section">
            <i class="fs1 a2i_gn_view1"></i>&nbsp;দৃষ্টি আকর্ষণ
        </h3>

        <div class="form-group form-horizontal">
            <div class="form-group">
                <label class="control-label col-md-4">Attention Type</label>

                <div class="col-md-5">
                    <?php echo $this->Form->input('attention_type', array('options' => json_decode(DAK_ATTENTION_TYPE, true), 'type' => 'select', 'label' => false, 'class' => 'form-control')); ?>
                </div>
            </div>
        </div>
        <hr>
        <div class="form-group form-horizontal">
            <div class="form-group">
                <label class="control-label col-md-4">Office Name</label>

                <div class="col-md-4">
                    <?php echo $this->Form->input('office_name', array('label' => false, 'class' => 'form-control typeahead_example_3')); ?>
                    <?php echo $this->Form->hidden('office_id'); ?>
                </div>
            </div>
        </div>
        <div class="form-group form-horizontal">
            <div class="form-group">
                <label class="control-label col-md-4">Office Address</label>

                <div class="col-md-4">
                    <?php echo $this->Form->input('office_address', array('label' => false, 'class' => 'form-control')); ?>
                </div>
            </div>
        </div>
        <div class="form-group form-horizontal">
            <div class="form-group">
                <label class="control-label col-md-4">Officer</label>

                <div class="col-md-4">
                    <?php echo $this->Form->input('officer_designation', array('label' => false, 'class' => 'form-control')); ?>
                    <?php echo $this->Form->hidden('officer_id'); ?>
                    <?php echo $this->Form->hidden('office_designation_id'); ?>
                </div>
            </div>
        </div>
        <div class="form-group form-horizontal">
            <div class="form-group">
                <label class="control-label col-md-4">Operation Type</label>

                <div class="col-md-5">
                    <?php echo $this->Form->input('operation_types', array('options' => json_decode(DAK_OPERATION_TYPE, true), 'type' => 'select', 'label' => false, 'class' => 'form-control')); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<script
    src="<?php echo CDN_PATH; ?>assets/admin/layout4/scripts/projapoti_autocomplete.js">
</script>

<script>
    $(function () {
        ProjapotiAutocomplete.init();
    });
</script>
<script type="text/javascript">
    var DakAttention = {
        loadAutoOfficeData: function () {
            PROJAPOTI.ajaxSubmitDataCallback('<?php echo $this->request->webroot?>', '');
        },
        loadAutoOfficeDesignationData: function () {

        },
    };
</script>


