<?php
if (!empty($data)) {
    foreach ($data as $key => $value) {
        ?>
        <div class="portlet box purple">

            <div class="portlet-title">
                <div class="caption">
                    <?= $value['name'] ?>
                </div>
                <div class="tools">
                    <a href="javascript:;" class="expand" data-original-title="" title=""></a>
                    <a class="unit_dashboard_details" title="বিস্তারিত" href="<?= $this->request->webroot ?>unitDashboardAjax/<?= $value['office_id'] ?>/<?= $value['unit_id'] ?>" style="color:#fff;text-decoration: none;">
                        <label class="badge badge-primary" style="font-size: 12pt!important;">  বিস্তারিত  </label></a>
                    <a target="_blank" class="unit_own_childs" title=" অধীনস্থ শাখা " href="<?= $this->request->webroot ?>unitDashboard/<?= $value['office_id'] ?>/<?= $value['unit_id'] ?>" style="color:#fff;text-decoration: none;">
                        <label class="badge badge-danger" style="font-size: 12pt!important;">  অধীনস্থ শাখা  </label></a>

                </div>
            </div>
            <div class="portlet-body" style="display: none;">
                <table class="table table-bordered" style="font-size:14pt!important;">
                    <tr class="text-center">
                        <th colspan="3" class="font-md text-center">
                            <a target="_blank" href="<?= $this->request->webroot ?>loginDetails/<?= $value['office_id'] ?>/<?= $value['unit_id'] ?>">  ব্যবহারকারী  তথ্য  দেখুন </a>
                        </th>
                    </tr>
                </table>
                <div class="">
                    <table class="table table-bordered table-hover">

                        <tr class="heading">

                            <th class="text-center">
                                চলমান কার্যক্রম
                            </th>
                            <th class="text-center">
                                আজকের
                            </th>
                            <th class="text-center">
                                গতকালের
                            </th>
                            <th class="text-center">
                                অদ্যাবধি
                            </th>

                        </tr>

                        <tr class="bg-grey">

                            <td>
                                ডাক গ্রহণ
                            </td>
                            <td class="text-center">
                                <?php echo $this->Number->format($value['totaltodayinbox']); ?>
                            </td>
                            <td class="text-center">
                                <?php echo $this->Number->format($value['totalyesterdayinbox']); ?>
                            </td>
                            <td class="text-center">
                                <?php echo $this->Number->format($value['totalinboxall']); ?>
                            </td>

                        </tr>
                        <tr>

                            <td>
                                নিষ্পন্ন ডাক সংখ্যা
                            </td>
                            <td class="text-center">
                                <?php echo $this->Number->format($value['totaltodaynisponnodak']); ?>

                            </td>
                            <td class="text-center">
                                <?php echo $this->Number->format($value['totalyesterdaynisponnodak']); ?>

                            </td>
                            <td class="text-center">
                                <?php echo $this->Number->format($value['totalnisponnodakall']); ?>

                            </td>

                        </tr>
                        <tr style="color:black!important;" class="bg-grey-silver">

                            <td>
                                অনিষ্পন্ন ডাক সংখ্যা
                            </td>
                            <td class="text-center">

                                <?php echo $this->Number->format($value['totaltodayOnisponnodak']); ?>

                            </td>
                            <td class="text-center">

                                <?php echo $this->Number->format($value['totalyesterdayOnisponnodak']); ?>

                            </td>
                            <td class="text-center">
                                <?php echo $this->Number->format($value['totalOnisponnodakall']); ?>
                            </td>

                        </tr>
                        <tr class="bg-grey">

                            <td>
                               সৃজিত
                                নোট
                            </td>
                            <td class="text-center">

                                <?php echo $this->Number->format($value['totaltodaysrijitonote'] + $value['totaltodaydaksohonote']); ?>
                            </td>
                            <td class="text-center">
                            <?php echo $this->Number->format($value['totalyesterdaysrijitonote'] + $value['totalyesterdaydaksohonote']); ?>
                            </td>
                                 <?php
                        if(!empty($value['ajax_request']) && $value['ajax_request']  == true){
                            echo '<td class="text-center" id="onisponno_note"><i class="fa fa-spinner" aria-hidden="true"></i></td>';
                        }else{
                        ?>
                            <td class="text-center">
                                <?php echo $this->Number->format($value['totalsrijitonoteall'] + $value['totaldaksohonoteall']); ?>
                            </td>
                        <?php } ?>
                        </tr>
                        <tr>
                                                    <td>
                                                   পত্রজারিতে নিষ্পন্ন নোট
                                                    </td>
                                                    <td class="text-center">
                                                        <?= $this->Number->format($value['totaltodaynisponnopotrojari']) ?>

                                                    </td>
                                                    <td class="text-center">
                                                        <?= $this->Number->format($value['totalyesterdaynisponnopotrojari']) ?>

                                                    </td>
                                                    <td class="text-center">
                                                        <?= $this->Number->format( $value['totalnisponnopotrojariall']) ?>
                                                    </td>
                            </tr>
                            <tr>
                                                    <td>
                                                   নোটে নিষ্পন্ন
                                                    </td>
                                                    <td class="text-center">
                                                        <?= $this->Number->format($value['totaltodaynisponnonote'] ) ?>

                                                    </td>
                                                    <td class="text-center">
                                                        <?= $this->Number->format($value['totalyesterdaynisponnonote']) ?>

                                                    </td>
                                                    <td class="text-center">
                                                        <?= $this->Number->format($value['totalnisponnonoteall']) ?>
                                                    </td>
                        </tr>

                        <tr style="color:black!important;" class="bg-grey-silver">

                            <td>
                                অনিষ্পন্ন নোট সংখ্যা
                            </td>
                            <td class="text-center">

                                <?php echo $this->Number->format($value['totaltodayOnisponnonote']); ?>

                            </td>
                            <td class="text-center">

                                <?php echo $this->Number->format($value['totalyesterdayOnisponnonote']); ?>

                            </td>
                                 <?php
                        if(!empty($value['ajax_request']) && $value['ajax_request']  == true){
                            echo '<td class="text-center" id="onisponno_note"><i class="fa fa-spinner" aria-hidden="true"></i></td>';
                        }else{
                        ?>
                            <td class="text-center">
                                <?php echo $this->Number->format($value['totalOnisponnonoteall']); ?>
                            </td>
                        <?php } ?>
                        </tr>
                        <tr >

                        <td>
                            পত্রজারি
                        </td>
                        <td class="text-center">
                            <?php echo $this->Number->format($value['totaltodaypotrojari']); ?>

                        </td>
                        <td class="text-center">
                            <?php echo $this->Number->format($value['totalyesterdaypotrojari']); ?>

                        </td>
                        <td class="text-center">
                            <?php echo $this->Number->format($value['totalpotrojariall']); ?>

                        </td>

                    </tr>


                    </table>

                    <table class="table table-bordered table-hover">


                        <tr class="heading">
                            <th class="text-center">অপেক্ষমাণ কার্যক্রম</th>
                            <th class="text-center">৫+ দিন</th>
                            <th class="text-center">১০+ দিন</th>
                            <th class="text-center">১৫+ দিন</th>
                            <th class="text-center">৩০+ দিন</th>
                        </tr>

                        <tr>
                            <td style='font-size: 11pt;text-align: center; '> ডাক </td>
                            <td class="text-center"><?= $value['dak']['d5'] ?></td>
                            <td class="text-center"><?= $value['dak']['d10'] ?></td>
                            <td class="text-center"><?= $value['dak']['d15'] ?></td>
                            <td class="text-center"><?= $value['dak']['d30'] ?></td>
                        </tr>
                        <tr>
                            <td style='font-size: 11pt;text-align: center;'> নথি </td>
                            <td class="text-center"><?= $value['nothi']['d5'] ?></td>
                            <td class="text-center"><?= $value['nothi']['d10'] ?></td>
                            <td class="text-center"><?= $value['nothi']['d15'] ?></td>
                            <td class="text-center"><?= $value['nothi']['d30'] ?></td>
                        </tr>

                    </table>

                </div>

            </div>
        </div>
        <?php
    }
}
else {
    echo ' <div class="portlet-body"><ul class="list-group stripe "><li class="list-group-item text-center" style="color:red;font-size:12pt!important;">দু:খিত কোন শাখা পাওয়া যায় নি।</li></ul></div>';
}
?>