<div class="portlet box purple">

    <div class="portlet-title">
        <div class="caption">
          <?php echo  isset($title)?$title:'অধীনস্থ অফিস' ?>
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse" data-original-title="" title=""></a>
        </div>
    </div>
    <div class="portlet-body">
        <ul class="list-group stripe ">

            <?php
            if (!empty($office_list)) {
                foreach ($office_list as $key => $value) {
                    ?>

            <li class="list-group-item">
                <div class="row">
                    <div class="col-md-9 col-sm-9 col-xs-9"><?= $value['office_name_bng']; ?></div>
                    <div class="col-md-3 col-sm-3 col-xs-3"><a target="_blank" title="বিস্তারিত" href="<?= $this->request->webroot ?>officeDashboard/<?= $value['id'] ?>" style="text-decoration: none;font-size: 14px!important;" class="badge badge-primary">বিস্তারিত</a></div>
                </div>
            </li>

                    <?php
                }
            }else
            {
            echo '<li class="list-group-item text-center" style="color:red;font-size:14px!important;">দু:খিত কোন অফিস পাওয়া যায় নি।</li>';
            }
            ?>
        </ul>
    </div>
</div>