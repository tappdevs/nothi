<style>
    .new_office_info {
        display: none;
    }
    .input.select {
        margin-right: 15px;
    }
    .form-control.select2-dropdown-open .select2-choice {
        border-color: #5797fb !important;
    }
    .form-control .select2-choice {
        border: 1px solid #e5e5e5 !important;
        background-color: #fff !important;
        background-image: none !important;
        filter: none !important;
        height: 34px !important;
        padding: 3px 0 0px 12px !important;
    }
    .select2-container .select2-choice .select2-arrow {
        background-image: none !important;
        background-color: #fff !important;
        filter: none !important;
        border-left: 1px solid #e5e5e5 !important;
    }

    .speech-bubble-top {
        margin-bottom: 10px;
        background: #f3f3f3;
        border: solid 1px #cacaca;
        padding: .35em .625em .75em
    }
    .speech-bubble-top:after {
        content: "";
        position: absolute;
        top: 253px;
        right: 276px;
        border-top: 10px solid grey;
        border-top-color: inherit;
        border-left: 10px solid transparent;
        border-right: 10px solid transparent;
    }
    .speech-bubble-bottom {
        margin-top: 10px;
        background: #f3f3f3;
        border: solid 1px #cacaca;
        padding: .35em .625em .75em
    }
    .speech-bubble-bottom:after {
        content: "";
        position: absolute;
        top: 189px;
        right: 136px;
        border-bottom: 10px solid grey;
        border-top-color: inherit;
        border-left: 10px solid transparent;
        border-right: 10px solid transparent;
    }
</style>

<div class="form-horizontal">
    <div class="row">
        <div class="col-md-12">
            <label class="control-label"> অফিসার বাছাই করুন</label>
        </div>
    </div>
    <div class="speech-bubble-top" id="office_selection_div" style="display: block">
        <div class="row" style="display: block;">
            <div class="col-md-12" style="margin: 0 15px; display: block">
                <div id="<?php echo $prefix; ?>office_selection_div" style="display: block;">
                    <?php echo $office_cell = $this->cell('OfficeUnitOrganogramNew', ['prefix' => $prefix, ['organogram' => 1]]); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <?php
            $type_prefix = explode("_", $prefix);
            $type_prefix = "typeahead_" . $type_prefix[0];

            $dak_office = "";
            if (isset($sender_office)) {
                $dak_office = $sender_office;
            }
            if (isset($receiver_office)) {
                $dak_office = $receiver_office;
            }
            echo $this->Form->input($prefix . 'office', array('id' => $prefix . 'autocomplete_office_id', 'label' => false, 'class' => 'form-control ' . $type_prefix, 'value' => $dak_office,'placeholder'=>'অফিসারের নাম ও পদবি দিয়ে খুঁজুন', 'data-prefix' => $prefix));
            ?>
        </div>
        <div class="col-md-4">
            <div class="btn-group">
                <?php if (!isset($response_params['is_self_office'])) { ?>
                    <button type="button" id="<?php echo $prefix; ?>office_toggle_btn" class="btn btn-default btn-md grey" style="display: block;padding: 4px 10px 5px 10px;" onclick="DakOffice.selectionOfDakOffice(this.id);" >অনুসন্ধান <i class="fa fa-chevron-up"></i></button>
                    <button type="button" id="<?php echo $prefix; ?>office_toggle_manually_btn" class="btn btn-default btn-md" style="display: block;padding: 4px 10px 5px 10px;" onclick="DakOffice.selectionOfDakOfficeManual(this.id);" >নিজে তথ্য লিখুন <i class="fa fa-chevron-down"></i></button>
                <?php } ?>
            </div>
        </div>
    </div>
    <fieldset class="new_office_info speech-bubble-bottom">
        <div class="row">
            <div class="col-md-6">
                <label class="control-label"> কার্যালয় <span class="required"> * </span></label>
                <?php echo $this->Form->input($prefix . 'office_name_new', array('label' => false, 'class' => 'form-control', 'value' => (isset($response_params[$prefix . 'office_name_new'])) ? $response_params[$prefix . 'office_name_new'] : '')); ?>
            </div>
            <div class="col-md-6">
                <label class="control-label"> দপ্তর/শাখা </label>
                <?php echo $this->Form->input($prefix . 'office_unit_name_new', array('label' => false, 'class' => 'form-control', 'value' => (isset($response_params[$prefix . 'office_unit_name_new'])) ? $response_params[$prefix . 'office_unit_name_new'] : '')); ?>
            </div>
            <div class="col-md-6">
                <label class="control-label"> পদ <span class="required"> * </span> </label>
                <?php echo $this->Form->input($prefix . 'officer_designation_label_new', array('label' => false, 'class' => 'form-control', 'value' => (isset($response_params[$prefix . 'officer_designation_label_new'])) ? $response_params[$prefix . 'officer_designation_label_new'] : '')); ?>
            </div>
            <div class="col-md-6">
                <label class="control-label"> অফিসার  </label>
                <?php echo $this->Form->input($prefix . 'officer_name', array('label' => false, 'class' => 'form-control','placeholder'=>'অফিসার', 'readonly' => false, 'value' => (isset($response_params[$prefix . 'officer_name'])) ? $response_params[$prefix . 'officer_name'] : '','autocomplete'=>'off')); ?>
                <?php echo $this->Form->hidden($prefix . 'officer_id', array('id' => $prefix . 'officer_id', 'value' => (isset($response_params[$prefix . 'officer_id'])) ? $response_params[$prefix . 'officer_id'] : '0')); ?>
            </div>
        </div>
    </fieldset>
</div>
<div id="<?php echo $prefix; ?>office_autocomplete_div" style="display: none;">
    <?php echo $this->Form->hidden($prefix . 'officer_designation_id_auto', array('type' => 'text', 'value' => (isset($response_params[$prefix . 'officer_designation_label_new'])) ? $response_params[$prefix . 'officer_designation_id_auto'] : '')); ?>
    <?php echo $this->Form->hidden($prefix . 'officer_designation_label_auto', array('type' => 'text', 'value' => (isset($response_params[$prefix . 'officer_designation_label_new'])) ? $response_params[$prefix . 'officer_designation_label_auto'] : '')); ?>
    <?php echo $this->Form->hidden($prefix . 'address_auto', array('type' => 'text', 'value' => (isset($response_params[$prefix . 'officer_designation_label_new'])) ? $response_params[$prefix . 'address_auto'] : '')); ?>
    <?php echo $this->Form->hidden($prefix . 'office_name_auto', array('type' => 'text', 'value' => (isset($response_params[$prefix . 'officer_designation_label_new'])) ? $response_params[$prefix . 'office_name_auto'] : '')); ?>
    <?php echo $this->Form->hidden($prefix . 'office_id_auto', array('type' => 'text', 'value' => (isset($response_params[$prefix . 'officer_designation_label_new'])) ? $response_params[$prefix . 'office_id_auto'] : '')); ?>
</div>


<script src="<?php echo CDN_PATH; ?>assets/admin/layout4/scripts/projapoti_autocomplete.js" type="text/javascript"></script>
<script type="text/javascript">
    var DakOffice = {
        prefix: '<?php echo $prefix;?>',
        selectionOfDakOffice: function (id) {
            DakOffice.getPrefix(id);
            $("#" + DakOffice.prefix + "office_selection_div").parent().parent().parent().toggle();
            $("#" + DakOffice.prefix + "office_selection_div").parent().parent().toggle();
            $("#" + DakOffice.prefix + "office_selection_div").parent().toggle();
            $("#" + DakOffice.prefix + "office_selection_div").toggle();
            $("#" + DakOffice.prefix + "office_toggle_btn > i").toggleClass("fa-chevron-up fa-chevron-down");
            $("#" + DakOffice.prefix + "office_toggle_btn").toggleClass("grey");

            if ($("#" + DakOffice.prefix + "office_selection_div").is(":visible")) {
                $("#" + DakOffice.prefix + "office_selection_div").closest("#office_selection_div").prev().find('label').text('অফিসার বাছাই করুন');
            } else {
                $("#" + DakOffice.prefix + "office_selection_div").closest("#office_selection_div").prev().find('label').text('অফিসার খুঁজুন');
            }

            if ($("#" + DakOffice.prefix + "office_toggle_btn").hasClass('grey')) {
                if ($('.new_office_info').css('display') == 'block') {
                    DakOffice.selectionOfDakOfficeManual(DakOffice.prefix + "office_toggle_manually_btn");
                }
            }
        },
        selectionOfDakOfficeManual: function (id) {
            DakOffice.getPrefix(id);
            $("#" + DakOffice.prefix + "office_toggle_manually_btn > i").toggleClass("fa-chevron-up fa-chevron-down");
            $("#" + DakOffice.prefix + "office_toggle_manually_btn").toggleClass("grey");

            if ($("#" + DakOffice.prefix + "office_toggle_manually_btn").hasClass('grey')) {
                if ($("#" + DakOffice.prefix + "office_selection_div").css('display') == 'block') {
                    DakOffice.selectionOfDakOffice(DakOffice.prefix + "office_toggle_btn");
                }
                $('.new_office_info').show();

                $("input[name=" + DakOffice.prefix + "office]").css('visibility', 'hidden');
                $('.new_office_info').parent().find('div:eq(0)').find('label').css('visibility', 'hidden');
            } else {
                $('.new_office_info').hide();

                $("input[name=" + DakOffice.prefix + "office]").css('visibility', 'initial');
                $('.new_office_info').parent().find('div:eq(0)').find('label').css('visibility', 'initial');
            }
        },
        checkfornull: function (id) {
            DakOffice.getPrefix(id);
            if ($('#' + DakOffice.prefix + "officer_id").val() == 0 || $('#' + DakOffice.prefix + "officer_id").val() == '') {
                $("#" + DakOffice.prefix + "office_selection_div").hide();
                $('input[name='+DakOffice.prefix+'office]').show();
                $('.new_office_info').show();                
            } else {
                if($('#' + DakOffice.prefix + "officer_id").val() == 0  || $('#' + DakOffice.prefix + "officer_id").val() == '')
                {
                    $("#" + DakOffice.prefix + "office_selection_div").show();
                }
                $('input[name='+DakOffice.prefix+'office]').hide();
                $('.new_office_info').hide();
            }
        },
        setAutoDataIntoHiddenFields: function (data, input_id) {
            DakOffice.getPrefix(input_id);

            if (data.id == 0) {
                $('.new_office_info').show();
            } else {
                //$('input[name='+DakOffice.prefix+'office]').hide();
                $('.new_office_info').hide();
            }

            $("#" + DakOffice.prefix + "office_autocomplete_div > input[name=" + DakOffice.prefix + "office_id_auto]").val(data.office_id);
            $("#" + DakOffice.prefix + "office_autocomplete_div > input[name=" + DakOffice.prefix + "address_auto]").val(data.address);
            $("#" + DakOffice.prefix + "office_autocomplete_div > input[name=" + DakOffice.prefix + "office_name_auto]").val(data.office_name);
            $("#" + DakOffice.prefix + "office_autocomplete_div > input[name=" + DakOffice.prefix + "officer_designation_id_auto]").val(data.id);
            $("#" + DakOffice.prefix + "office_autocomplete_div > input[name=" + DakOffice.prefix + "officer_designation_label_auto]").val(data.designation_label);
            //$("input[name=" + DakOffice.prefix + "officer_name]").val(data.officer_name).trigger('change');
            $("input[name=" + DakOffice.prefix + "officer_id]").val(data.officer_id);

            //DakOffice.setOfficerInfo();
        },
        setOfficerInfo: function () {
            PROJAPOTI.ajaxSubmitDataCallback('<?php echo $this->request->webroot ?>' + 'employeeRecords/getOfficerInfo',
                {'designation_id': $("#" + DakOffice.prefix + "office_autocomplete_div > input[name=" + DakOffice.prefix + "officer_designation_id_auto]").val()}, 'json',
                function (response) {
                    //$("input[name=" + DakOffice.prefix + "officer_name]").val(response.name_bng).trigger('change');
                    $("input[name=" + DakOffice.prefix + "officer_id]").val(response.id);
                });
        },
        getPrefix: function (input_value) {
            var arr = input_value.split("_");
            DakOffice.prefix = arr[0] + "_";
        }
    }
</script>