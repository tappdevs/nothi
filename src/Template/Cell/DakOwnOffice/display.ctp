<div class="form-horizontal">
    <div class="row">
        <div class="col-md-6">
            <label class="control-label"> অফিস বাছাই করুন
                <?php if (!isset($response_params['is_self_office'])) { ?>
                    <span id="<?php echo $prefix; ?>office_toggle_btn" class=""
                          onclick="DakOffice.selectionOfDakOffice(this.id);"><i
                            class="fa text-primary fa-chevron-circle-down"></i></span>
                <?php } ?>
            </label>
            <?php
            $type_prefix = explode("_", $prefix);
            $type_prefix = "typeahead_" . $type_prefix[0];

            $dak_office = "";
            if (isset($sender_office)) {
                $dak_office = $sender_office;
            }
            if (isset($receiver_office)) {
                $dak_office = $receiver_office;
            }
            echo $this->Form->input($prefix . 'office', array('id' => $prefix . 'autocomplete_office_id', 'label' => false, 'class' => 'form-control ' . $type_prefix, 'value' => $dak_office));
            ?>
        </div>
        <div class="col-md-12" style="margin: 0 15px; display: none">
            <div id="<?php echo $prefix; ?>office_selection_div" style="display: none;">
                <?php echo $office_cell = $this->cell('OfficeUnitOrganogram', ['prefix' => $prefix]); ?>
            </div>
        </div>
        <div class="col-md-6">
            <label class="control-label"> অফিসার <span class="required"> * </span> </label>
            <?php echo $this->Form->input($prefix . 'officer_name', array('label' => false, 'class' => 'form-control', 'readonly' => false, 'value' => (isset($response_params[$prefix . 'officer_name'])) ? $response_params[$prefix . 'officer_name'] : '')); ?>
            <?php echo $this->Form->hidden($prefix . 'officer_id', array('id' => $prefix . 'officer_id', 'value' => (isset($response_params[$prefix . 'officer_id'])) ? $response_params[$prefix . 'officer_id'] : '')); ?>
        </div>
    </div>

</div>
<div id="<?php echo $prefix; ?>office_autocomplete_div" style="display: none;">
    <?php echo $this->Form->hidden($prefix . 'officer_designation_id_auto', array('type' => 'text', 'value' => (isset($response_params[$prefix . 'officer_designation_label_new'])) ? $response_params[$prefix . 'officer_designation_id_auto'] : '')); ?>
    <?php echo $this->Form->hidden($prefix . 'officer_designation_label_auto', array('type' => 'text', 'value' => (isset($response_params[$prefix . 'officer_designation_label_new'])) ? $response_params[$prefix . 'officer_designation_label_auto'] : '')); ?>
    <?php echo $this->Form->hidden($prefix . 'address_auto', array('type' => 'text', 'value' => (isset($response_params[$prefix . 'officer_designation_label_new'])) ? $response_params[$prefix . 'address_auto'] : '')); ?>
    <?php echo $this->Form->hidden($prefix . 'office_id_auto', array('type' => 'text', 'value' => (isset($response_params[$prefix . 'officer_designation_label_new'])) ? $response_params[$prefix . 'office_id_auto'] : '')); ?>
</div>

<script src="<?php echo CDN_PATH; ?>assets/admin/layout4/scripts/projapoti_autocomplete.js"
        type="text/javascript"></script>
<script type="text/javascript">
    var DakOffice = {
        prefix: '<?php echo $prefix;?>',
        selectionOfDakOffice: function (id) {
            DakOffice.getPrefix(id);
            $("#" + DakOffice.prefix + "office_selection_div").parent().toggle();
            $("#" + DakOffice.prefix + "office_selection_div").toggle();
            $("#" + DakOffice.prefix + "office_toggle_btn > i").toggleClass("fa-chevron-circle-up fa-chevron-circle-down");
        },
        setAutoDataIntoHiddenFields: function (data, input_id) {
            DakOffice.getPrefix(input_id);
            $("#" + DakOffice.prefix + "office_autocomplete_div > input[name=" + DakOffice.prefix + "office_id_auto]").val(data.office_id);
            $("#" + DakOffice.prefix + "office_autocomplete_div > input[name=" + DakOffice.prefix + "address_auto]").val(data.address);
            $("#" + DakOffice.prefix + "office_autocomplete_div > input[name=" + DakOffice.prefix + "officer_designation_id_auto]").val(data.id);
            $("#" + DakOffice.prefix + "office_autocomplete_div > input[name=" + DakOffice.prefix + "officer_designation_label_auto]").val(data.designation_label);
            $("input[name=" + DakOffice.prefix + "officer_name]").val(data.officer_name).trigger('change');
            $("input[name=" + DakOffice.prefix + "officer_id]").val(data.officer_id);
            //DakOffice.setOfficerInfo();
        },
        setOfficerInfo: function () {
            PROJAPOTI.ajaxSubmitDataCallback('<?php echo $this->request->webroot ?>' + 'employeeRecords/getOfficerInfo',
                {'designation_id': $("#" + DakOffice.prefix + "office_autocomplete_div > input[name=" + DakOffice.prefix + "officer_designation_id_auto]").val()}, 'json',
                function (response) {
                    $("input[name=" + DakOffice.prefix + "officer_name]").val(response.name_bng);
                    $("input[name=" + DakOffice.prefix + "officer_id]").val(response.id);
                    $('input[name=' + DakOffice.prefix + 'officer_designation_label_auto]').trigger('change');
                });
        },
        getPrefix: function (input_value) {
            var arr = input_value.split("_");
            DakOffice.prefix = arr[0] + "_";
        }
    }
</script>