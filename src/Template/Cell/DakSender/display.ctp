<div class="row office-seal">
    <div class="col-md-12">
        <?php echo $to_cell = $this->cell('OfficeSeal', ['params' => $seal_section]) ?>
    </div>

    <div class="col-md-8">
        <input type="hidden" name="to_officer_id" value="">
        <input type="hidden" name="to_officer_name" value="">
        <input type="hidden" name="to_officer_level" value="">
    </div>
</div>

<script type="text/javascript">
   
$(document).off('click','input[name="office-employee"]');
$(document).on('click','input[name="office-employee"]',function(){
    $(this).closest('.office-seal').find("input[name=to_officer_id]").val('');
    $(this).closest('.office-seal').find("input[name=to_officer_name]").val('');
    $(this).closest('.office-seal').find("input[name=to_officer_level]").val('');
    
    var reply_to_ids = [];
    var reply_to_names = '';
    var selector = this;
    $.each($(selector).closest('.office-seal').find('input[name="office-employee"]:checked'),function(){
        reply_to_ids.push($(this).data('employee-office-id'));
        reply_to_names+= $(this).data('office-employee-name') + ';';
        $(selector).closest('.office-seal').find("input[name=to_officer_id]").val(reply_to_ids);
        $(selector).closest('.office-seal').find("input[name=to_officer_name]").val(reply_to_names);
        
        if($(selector).closest('.office-seal').find('.office_employee_to:checked').data('employee-office-id')==$(this).data('employee-office-id')){
            $(selector).closest('.office-seal').find("input[name=to_officer_level]").val($(this).data('employee-office-id'));
        }
        
    });
});
    
</script>
<script>
$(function(){
     $(document).find('[title]').tooltip({'placement':'bottom'});
})
</script>