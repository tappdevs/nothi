<div id="chart_<?=$chart_no?>" class="chart" style="height: 400px;">
</div>
<?php
	$data = array();
	$dataF = array();
	foreach ($agoto_nothi as $agoto_nothi_item) {
		$dataX["date"] = $agoto_nothi_item->created;
		$dataX["countIn"] = $agoto_nothi_item->count;
		$dataX["countOut"] = 0;
		$dataX["total"] = $agoto_nothi_item->count + 0;
		$data[] = $dataX;
	}
	
	foreach ($prerito_nothi as $prerito_nothi_item) {
		$was_updated = false;
		foreach ($data as &$dt) {
			if ($prerito_nothi_item->created == $dt["date"]) {
				$dt["countOut"] = $prerito_nothi_item->count;
				$dt["total"] += $prerito_nothi_item->count;
				$was_updated = true;
				break;
			}
		}
		if (!$was_updated) {
			$dataY["date"] = $prerito_nothi_item->created;
			$dataY["countIn"] = 0;
			$dataY["countOut"] = $prerito_nothi_item->count;
			$dataY["total"] = 0 + $prerito_nothi_item->count;
			$data[] = $dataY;
		}
	}
	
	$this->array_sort_by_column($data, 'date');
?>

<script type="text/javascript">
	var chart<?=$chart_no?> = AmCharts.makeChart("chart_<?=$chart_no?>", {
		"type": "serial",
		"theme": "light",

		"fontFamily": 'Open Sans',
		"color":    '#888888',

		"legend": {
			"equalWidths": false,
			"useGraphSettings": true,
			"valueAlign": "left",
			"valueWidth": 120
		},
		"dataProvider": <?php echo json_encode($data); ?>,
		"valueAxes": [{
			"id": "distanceAxis",
			"axisAlpha": 0,
			"gridAlpha": 0,
			"position": "left",
			"title": "সর্বমোট নথি"
		}, {
			"id": "latitudeAxis",
			"axisAlpha": 0,
			"gridAlpha": 0,
			"labelsEnabled": false,
			"position": "right"
		}, {
			"id": "durationAxis",
			"axisAlpha": 0,
			"gridAlpha": 0,
			"inside": true,
			"position": "right",
			"labelsEnabled": false
		}],
		"graphs": [{
			"alphaField": "alpha",
			"balloonText": "মোট নথি: [[value]]",
			"dashLengthField": "dashLength",
			"fillAlphas": 0.7,
			"legendPeriodValueText": "মোট নথি: [[value.sum]]",
			"legendValueText": "মোট নথি [[value]]",
			"title": "সর্বমোট নথি",
			"type": "column",
			"valueField": "total",
			"valueAxis": "distanceAxis"
		}, {
			"balloonText": "আগত নথি: [[value]]",
			"bullet": "round",
			"bulletBorderAlpha": 1,
			"useLineColorForBulletBorder": true,
			"bulletColor": "#FFFFFF",
			"bulletSizeField": "townSize",
			"labelPosition": "right",
			"legendValueText": "[[value]]",
			"title": "আগত নথি",
			"fillAlphas": 0,
			"valueField": "countIn",
			"valueAxis": "latitudeAxis"
		}, {
			"balloonText": "প্রেরিত নথি: [[value]]",
			"bullet": "square",
			"bulletBorderAlpha": 1,
			"bulletBorderThickness": 1,
			"legendValueText": "[[value]]",
			"title": "প্রেরিত নথি",
			"fillAlphas": 0,
			"valueField": "countOut",
			"valueAxis": "durationAxis"
		}],
		"chartCursor": {
			"categoryBalloonDateFormat": "DD",
			"cursorAlpha": 0.1,
			"cursorColor": "#000000",
			"fullWidth": true,
			"valueBalloonsEnabled": false,
			"zoomable": false
		},
		"dataDateFormat": "YYYY-MM-DD",
		"categoryField": "date",
		"categoryAxis": {
			"dateFormats": [{
				"period": "DD",
				"format": "DD"
			}, {
				"period": "WW",
				"format": "MMM DD"
			}, {
				"period": "MM",
				"format": "MMM"
			}, {
				"period": "YYYY",
				"format": "YYYY"
			}],
			"parseDates": true,
			"autoGridCount": false,
			"axisColor": "#555555",
			"gridAlpha": 0.1,
			"gridColor": "#FFFFFF",
			"gridCount": 50
		},
		"exportConfig": {
			"menuBottom": "20px",
			"menuRight": "22px",
			"menuItems": [{
				"icon": js_wb_root + "webroot/assets/global/plugins/amcharts/amcharts/images/export.png",
				"format": 'png'
			}]
		}
	});
	
	$('#chart_<?=$chart_no?>').closest('.portlet').find('.fullscreen').click(function() {
		chart<?=$chart_no?>.invalidateSize();
	});
	
	$(document).ready(function(e){	
		chart<?=$chart_no?>.addListener("rendered", function(e){
			$("#chart_<?=$chart_no?>").parent().hide();
		});
	});
	
</script>