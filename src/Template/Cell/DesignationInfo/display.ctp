<?php
$session = $this->request->session();
$loggedEmployeeRecords = $session->read('logged_employee_record');
$designations = $loggedEmployeeRecords['office_records'];

$senderOfficeDesignations = array();
foreach ($designations as $designation) {
    if (!empty($designation['incharge_label'])) {
        $senderOfficeDesignations[$designation['id']] = $designation['designation'] . "(" . $designation['incharge_label'] . ")";
    } else {
        $senderOfficeDesignations[$designation['id']] = $designation['designation'];
    }
}
$employee_info = $loggedEmployeeRecords['personal_info'];
?>
<div class="form-horizontal">
    <div class="row">
        <div class=" col-md-6">
            <div class="">
                <label class="control-label"> Officer Designation </label>
                <?php echo $this->Form->input('sender_office_designation_id', array('options' => $senderOfficeDesignations, 'empty' => '----', 'label' => false, 'class' => 'form-control')); ?>
            </div>
        </div>
        <div class=" col-md-6">
            <div class="">
                <label class="control-label"> Officer Name </label>
                <?php echo $this->Form->input('sender_officer_name', array('label' => false, 'class' => 'form-control', 'value' => $employee_info['name_bng'])); ?>
                <?php echo $this->Form->hidden('sender_officer_id', array('label' => false, 'class' => 'form-control', 'value' => $employee_info['id'])); ?>
            </div>
        </div>
    </div>
</div>
<div class="form-horizontal">
    <div class="row">
        <div class=" col-md-6">
            <div class="">
                <label class="control-label"> Office Name </label>
                <?php echo $this->Form->input('sender_office_name', array('label' => false, 'class' => 'form-control')); ?>
                <?php echo $this->Form->hidden('sender_office_id', array('id' => 'sender_office_id', 'label' => false, 'class' => 'form-control')); ?>
            </div>
        </div>
        <div class=" col-md-6">
            <div class="">
                <label class="control-label"> Office Address </label>
                <?php echo $this->Form->input('sender_address', array('type' => "text", 'label' => false, 'class' => 'form-control')); ?>
            </div>
        </div>
    </div>
</div>
<hr/>
<div class="form-horizontal">
    <div class="row">
        <div class=" col-md-4">
            <div class="">
                <label class="control-label"> Email </label>
                <?php echo $this->Form->input('sender_email', array('label' => false, 'class' => 'form-control')); ?>

            </div>
        </div>
        <div class=" col-md-4">
            <div class="">
                <label class="control-label"> Phone </label>
                <?php echo $this->Form->input('sender_phone', array('label' => false, 'class' => 'form-control')); ?>

            </div>
        </div>
        <div class=" col-md-4">
            <div class="">
                <label class="control-label"> Mobile </label>
                <?php echo $this->Form->input('sender_mobile', array('label' => false, 'class' => 'form-control')); ?>

            </div>
        </div>
    </div>
</div>

<script>
    $(function () {
        $("#sender-office-designation-id").bind('change', function () {
            DesignationInfo.getDesignatedOfficeInfo();
        });
    });
</script>
<script type="text/javascript">
    var DesignationInfo = {
        getDesignatedOfficeInfo: function () {
            var selected_designation_id = $("#sender-office-designation-id").val();
            var office_id = 0;
            var selected_record = '';
            <?php foreach($designations as $row){?>

            if (selected_designation_id == '<?php echo $row['id'];?>') {
                selected_record = '<?php echo json_encode($row);?>'
            }
            <?php }?>
            var selected_record = JSON.parse(selected_record);

            PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + '/officeManagement/getOfficeInfo',
                {'office_id': selected_record.office_id}, 'json',
                function (response) {
                    $("#sender-office-name").val(response.office_name_bng);
                    $("#sender-address").val(response.office_address);
                    $("#sender_office_id").val(response.id);
                    $("#sender-email").val(response.office_email);
                    $("#sender-phone").val(response.office_phone);
                    $("#sender-mobile").val(response.office_mobiles);
                });
        }
    };
</script>


