<style>
    .checkbox input[type=checkbox], .checkbox-inline input[type=checkbox], .radio input[type=radio], .radio-inline input[type=radio]{
        margin-left: -10px;
    }
</style>
<div class="row permissiondiv"  style="display: none;">
    <div class="col-md-12">
    <div class="col-md-6 col-xs-12">
        <div class="portlet box red">
            <div class="portlet-title">
                <div class="caption">
                    নিজ অফিসের কর্মচারীদের অনুমতি প্রদান করুন
                </div>
            </div>
            <div class="portlet-body">
                <?php echo $permissionUsersList ?>
            </div>
        </div>
    </div>
<!--    <div class="col-md-6 col-xs-12">
        <?php // echo $this->cell('NothiOtherOfficePermissionList', ['prefix' => '', 'id'=>$id]); ?>
    </div>-->
    </div>
</div>

<script src="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>

<script type="text/javascript">

    jQuery(document).ready(function () {
        //initTable1();
        $('.permissiondiv').show();
//        $('.dataTables_filter').css('float','left');
        $('.optionRadios').on('click',function(){

            $(this).closest('tr').find('.optionCheckbox').click();
        })
    });


    var initTable1 = function () {
        var table = $('#draftmasterpermissionlistownunit');

        var oTable = table.dataTable({

            loadingMessage: 'লোড করা হচ্ছে...',
            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "emptyTable": "কোনো তথ্য নেই",
                "info": "<span class='seperator'>|</span>মোট _TOTAL_ টি তথ্য পাওয়া গেছে",
                "infoEmpty": "কোনো তথ্য নেই",
                "infoFiltered": "<span class='seperator'>|</span>মোট _TOTAL_ টি তথ্য পাওয়া গেছে",
                "search": "খুঁজুন  ",
                "zeroRecords": "কোনো তথ্য নেই"
            },
            "order": [
                [1, 'asc']
            ],

            "serverSide": false,
            "paging":   false,
            "ordering": false,
            "info":     false,
            "columnDefs": [
                { "visible": false, "targets": 3 }
            ],
            "drawCallback": function ( settings ) {
                var api = this.api();
                var rows = api.rows( {page:'current'} ).nodes();
                var last=null;

                api.column(3, {page:'current'} ).data().each( function ( group, i ) {
                    if ( last !== group ) {
                        $(rows).eq( i ).before(
                            '<tr class="group"><td colspan="4" style="font-weight:bold;">'+group+'</td></tr>'
                        );

                        last = group;
                    }
                } );
            },
            "bStateSave": false,
            // set the initial value
            "pageLength": -1,
            "dom": "<'row'<'col-md-12 col-sm-12'f>r><'table-scrollable't>"
        });
    }

</script>