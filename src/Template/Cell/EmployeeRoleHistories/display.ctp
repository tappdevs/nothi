<div class="row">
    <div class="col-md-12">
        <p class="help-block text-danger  text-center">
            *** <?= __("Search Employee by employee login ID") ?> ***
        <hr>
        </p>

        <form class="alert alert-success alert-borderless">
            <div class="input-group">
                <div class="input-cont right">
                    <?= $this->Form->input('employee_identity_no',
                        ['list' => 'employee_identity_no_list', 'label' => false,
                        'class' => 'form-control', 'type' => 'text', 'placeholder' => '']); ?>
                    <datalist id="employee_identity_no_list">
                        <?php foreach ($employee_identity_nos as $identity_no) { ?>
                            <option value="<?php echo $identity_no ?>"></option>
<?php } ?>
                    </datalist>
                </div>
                <span class="input-group-btn">
                    <button class="btn green-haze" type="button" id="btn_employee_search">
<?= __("Search") ?> &nbsp; <i class="m-icon-swapright m-icon-white"></i>
                    </button>
                    <button class="btn blue" type="button" id="btn-history-print">
প্রিন্ট &nbsp; <i class="fa fa-print m-icon-white"></i>
                    </button>
                </span>

        </form>
    </div>
</div>
</div>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PORTLET -->
        <div class="portlet light ">
            <div class="portlet-body" id="search-result" >
                <div class="row" id="loding"></div>
                <div class="row">
                           <center><div id="p_img">

                            </div></center>

                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12 profile-info text-center">
<?php echo $this->Form->hidden('employee_record_id',
    array('id' => 'employee-record-id')); ?>
                                <h1 id="p_name">&nbsp;</h1>
                                <ul class="list-inline">
                                    <li id="p_email">

                                    </li>
                                </ul>
                                <ul class="list-inline">
                                    <li id="p_mobile">

                                    </li>
                                </ul>
                            </div>
                            <!--end col-md-12-->
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="timeline">

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PORTLET -->
    </div>
</div>
<script src="<?=$this->request->webroot?>assets/global/scripts/printThis.js"></script>
<script type="text/javascript">

    var EmployeeManagement = {
        encodeImage: function (imgurl) {

            $.ajax({
                url: js_wb_root + 'EmployeeRecords/getUserProfilePhoto/' + imgurl + '/1',
                cache: true,
                type: 'post',
                async: false,
                success: function (img) {
                    $('#p_img').html('<img src="' + img + '" alt="profile" class="img-responsive"  />');
                }
            });
        },
        getEmployeeJobStatus: function (identity_no) {
            $(".timeline").html("");
            $("#p_email").html("");
            $("#p_mobile").html("");
            $("#p_name").html("");
            $("#loding").html("<img src='<?php echo CDN_PATH ?>assets/global/img/loading-spinner-grey.gif' alt='' class='img-responsive' />");
            PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + 'employeeRecords/getEmployeeRoleHistories',
                    {'identity_no': identity_no}, 'json',
                    function (response) {
                        $("#loding").html("");
                        var personal_info = response.personal_info;
                        var office_records = response.office_records;
                        $("#p_name").text(personal_info.name_bng+" ("+enTobn(response.username)+"): কর্ম ইতিহাস");
                        $("#p_email").html('<i class="fa fa-envelope"></i> ' + personal_info.personal_email);
                        $("#p_mobile").html('<i class="fa fa-phone"></i> ' + personal_info.personal_mobile);
                        $("#employee-record-id").val(personal_info.id);

                        var data = '<table class="table table-striped">' +
                            '                            <thead>' +
                            '                            <tr>' +
                            '                                <th>পদবি</th>' +
                            '                                <th>শাখা</th>' +
                            '                                <th>অফিস</th>' +
                            '                                <th>সময়কাল</th>' +
                            '                            </tr>' +
                            '                            </thead>' +
                            '                            <tbody>';

                        $.each(office_records, function (i,record) {
                            data+= '<tr>';
                            data+= '<td>'+ record.designation +((!isEmpty(record.incharge_label))?' <span style="font-size: 12px">('+record.incharge_label+')</span>':'')+ '</td>';
                            data+= '<td>'+record.unit_name + '</td>';
                            data+= '<td>'+record.office_name+ '</td>';
                            if (record.last_office_date == null) {
                                record.last_office_date = "বর্তমান";
                            }
                            if (record.joining_date == null) {
                                record.joining_date = "জয়েন করার তারিখ পাওয়া যায় নি"
                            }
                            data+= '<td>'+enTobn(record.joining_date) +' - '+ enTobn(record.last_office_date) + '</td>';
                            data+= '</tr>';

                        });
                        data+= '</tbody></table>';
                        $(".timeline").html(data);
                    });
        },
        removeAssignment: function (record_id) {
            var r = confirm("Are you sure to release employee from this designation ?");
            if (r == true) {
                PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + 'employeeRecords/removeAssignment',
                        {'id': record_id, 'last_release_date': $("#last_release_date_" + record_id).val()}, 'json',
                        function (response) {
                            EmployeeManagement.getEmployeeJobStatus();
                        });
            } else {

            }
        }
    };
    $(function () {
        $("#btn_employee_search").bind('click', function (evt) {
            evt.preventDefault();
            $("#p_img").html("");
            var username = getusername();
            if(username=='' || typeof(username)=='undefined'){
                toastr.error("লগইন আইডি সঠিক নয়");
                return;
            }
            EmployeeManagement.getEmployeeJobStatus(username);
            EmployeeManagement.encodeImage(username, function (src) {
                $('#p_img').html('<img src="' + src + '" alt="signature" width="100" />');
            });
        });
    });
    $("#employee-identity-no").keypress(function (e) {
        if (e.which == 13 || e.keyCode == 13) {
            e.preventDefault();
            $("#p_img").html("");
            var username = getusername();
             if(username=='' || typeof(username)=='undefined'){
                 toastr.error("লগইন আইডি সঠিক নয়");
                return;
            }
            EmployeeManagement.getEmployeeJobStatus(username);
            EmployeeManagement.encodeImage(username);
        }
    });
    function getusername()
    {
        var identity_no = $("#employee-identity-no").val();
        if (identity_no == '') {
            return;
        }
        var len = identity_no.length;
        if (len > 0) {
            if (identity_no[0] == 0) {
                return;
            }
        }
        if (len < 12) {
            var fake_id = identity_no;
            identity_no = '';
            identity_no += fake_id[0];
            var i = 0;
            var j = 0;
            var k = 0;
            for (i = 1; i <= 12 - len; i++) {
                identity_no += '0';
            }

            for (j = i, k = 1; j + 1 <= 12; j++, k++) {
                identity_no += fake_id[k];
            }
        }
        $("#employee-identity-no").val(identity_no);
        return identity_no;
    }

    $("#btn-history-print").click(function (e) {
        e.preventDefault();
        $(document).find('#search-result').printThis({
            importCSS: true,
            debug: false,
            importStyle: true,
            printContainer: false,
            pageTitle: "",
            removeInline: false,
            header: null
        });
    });
</script>
