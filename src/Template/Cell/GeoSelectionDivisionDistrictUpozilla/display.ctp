<div class="row">
    <div class="col-md-3 form-group form-horizontal">
        <label class="control-label">বিভাগ </label>
        <?php echo $this->Form->input('geo_division_id', array('empty' => '--Select--', 'options' => $data_items, 'type' => 'select', 'label' => false, 'class' => 'form-control', 'placeholder' => 'দপ্তর / অধিদপ্তর ')); ?>
    </div>
    <div class="col-md-3 form-group form-horizontal">
        <label class="control-label">জেলা </label>
        <?php echo $this->Form->input('geo_district_id', array('empty' => '--Select--', 'type' => 'select', 'label' => false, 'class' => 'form-control', 'placeholder' => 'দপ্তর / অধিদপ্তর ')); ?>
    </div>
    <div class="col-md-3 form-group form-horizontal">
        <label class="control-label">উপজেলা </label>
        <?php echo $this->Form->input('geo_upazila_id', array('empty' => '--Select--', 'type' => 'select', 'label' => false, 'class' => 'form-control', 'placeholder' => 'দপ্তর / অধিদপ্তর ')); ?>
    </div>
</div>
<script
    src="<?php echo CDN_PATH; ?>assets/admin/layout4/scripts/geo_setup.js"
    type="text/javascript"></script>

<script type="text/javascript">
    $(function () {
        $("#geo-division-id").bind('change', function () {
            var geo_division_id = $(this).val();
            GeoSetup.loadDistricts(geo_division_id);
        });

        $("#geo-district-id").bind('change', function () {
            var geo_district_id = $(this).val();
            GeoSetup.loadUpazilas(geo_district_id);
        });
    });
</script>