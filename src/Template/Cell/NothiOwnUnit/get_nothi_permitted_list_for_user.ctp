<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<?php if($dont_show_new_nothi_create_option == false): ?>
<h4><a class="newNothiCreate btn btn-sm green" href="javascript: void(0);" style="font-size: 12px; vertical-align: bottom;"><i class="fs0 a2i_gn_add1"></i> নতুন নথি তৈরি করুন</a></h4>
<hr/>
<?php endif; ?>
<style>
    .selectPartNo{
        cursor: pointer;
    }
    .nothipermittedlist {
        cursor: pointer;
    }
/*    .nothiprmittedlst{
        width: 100%!important;
    }*/
        .dataTables_scrollHeadInner{
         width: 100%!important;
    }
       
/*    .nothiprmittedlst tr:after {
            content: ' ';
            display: block;
            visibility: hidden;
            clear: both;
        }
    .nothiprmittedlst thead,.nothiprmittedlst tbody,.nothiprmittedlst tr,.nothiprmittedlst td,.nothiprmittedlst th { display: block; }

    .nothiprmittedlst thead {
             fallback
        }


    .nothiprmittedlst tbody td,.nothiprmittedlst thead th {
            float: left;     word-wrap: break-word;
    }*/

</style>
<div >
    <?php echo $this->form->hidden('dont_show_new_note_option',['value' => $dont_show_new_note_option,'id' => 'dont_show_new_note_option']) ?>
    <div class="row">
        <div class="col-md-8 col-sm-8 col-lg-8 ">
            <?php if (!empty($nothiPermissionList)) {
                ?>
                <table class="table nothiprmittedlst">
                    <thead >
                        <tr role="row" class="heading">
                            <th class="text-center" >
                                <?php echo __("ক্রম") ?>
                            </th>
                            <th class="text-center" >
                                <?php echo __("শাখা") ?>
                            </th>
                            <th class="text-center" >
                                <?php echo __("ধরন") ?>
                            </th>
                            <th class="text-center" >
                                <?php echo __("নথি নম্বর") ?>
                            </th>
                            <th class="text-center" >
                                <?php echo __("বিষয়") ?>
                            </th>
                        </tr>
                    </thead>
                    <tbody >
                        <?php
                        if (!empty($nothiPermissionList)) {

                            foreach ($nothiPermissionList as $key => $row) {
                                $officeUnitTable = \Cake\ORM\TableRegistry::get('OfficeUnits');
                                $unitInformation = $officeUnitTable->getBanglaName($row['office_units_id']);
                                ?>
                                <tr  style="cursor: pointer;" class="nothipermittedlist">
                                    <td class="text-center" nothi_id="<?php echo $row['id'] ?>" >
                                        <?php echo $this->Number->format(++$key); ?>
                                    </td>
                                    <td >
                                        <?php echo $unitInformation['unit_name_bng'] ?>
                                    </td>
                                    <td >
                                        <?php echo $row['NothiTypes']['type_name'] ?>
                                    </td>
                                    <td >
                                        <?php echo $row['nothi_no'] ?>
                                    </td>
                                    <td >
                                        <?php echo $row['subject'] ?>
                                    </td>

                                </tr>
                                <?php
                            }
                        }
                        ?>
                    </tbody>
                </table>
                <?php
            }
            ?>
        </div>
        <div class="col-md-4 col-sm-4 col-lg-4 part_nothi_list scroller" style="" data-always-visible="1" data-rail-visible1="1">
            <p class="selectedNothiName text-center text-success ">নথি বাছাই করুন</p>
            <table class="table table-striped table-bordered table-hover" id="nothi_list_datatable">
                <thead>
                    <tr>
                        <th>
                            <input type="text" class="form-control" id="nothisubject" name="nothisubject" placeholder="নোট বাছাই করুন"/>
                        </th>
                    </tr>
                </thead>
                <tbody >

                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    $.ajaxSetup({ cache: true });
</script>
<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>

<script src="<?php echo CDN_PATH; ?>projapoti-nothi/js/get_nothi_permitted_list_for_user.js?v=<?= js_css_version ?>" type="text/javascript"></script>
