<div class="row permissiondiv">
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                নোটের অনুমতিপ্রাপ্ত ব্যক্তিবর্গের তালিকা
            </div>
            <div class="actions">
                <button class="btn green font-lg nothiPermissionUpdate">সংরক্ষণ</button>
            </div>
        </div>
        <div class="portlet-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="portlet box red">
                        <div class="portlet-title">
                            <div class="caption">
                                নিজ অফিস
                            </div>
                        </div>
                        <div class="portlet-body" style="overflow: auto">
                            <div class="panel panel-success">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-9" style="text-align: center;">
                                            অনুমতি প্রাপ্ত
                                        </div>
                                        <div class="col-md-6 col-sm-3" style="text-align: center;">
                                            ক্রম
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body" id='addownpermission'>

                                </div>
                            </div>
                            <div class="table-container">
                                <div class="panel-group accordion">
                                    <?= $data ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="portlet box red">
                        <div class="portlet-title">
                            <div class="caption">
                                অন্যান্য অফিস
                            </div>
                        </div>
                        <div class="portlet-body" style="overflow: auto">
                            <div class="panel panel-success">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-md-6  col-sm-9" style="text-align: center;">
                                            অনুমতি প্রাপ্ত
                                        </div>
                                        <div class="col-md-6  col-sm-3" style="text-align: center;">
                                            ক্রম
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="table-scrollable">
                                        <table class="table table-bordered">
                                            <tbody id="addotherpermission">
                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                            </div>
                            <?php echo $this->cell('NothiOtherOfficePermissionList',
                                ['prefix' => '', 'id' => $id, 'nothi_office' => $nothi_office, true]);
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<script>
    listown();
    listownother();

    function listown() {
        var ok = '<div class="table-scrollable"><table class="table table-bordered"><tbody>';
        $.each($('.permissiondiv').find('.optionsRadios:checked'), function (i, v) {

            var id = $(v).data('employee-id');
            var name = $(v).data('employee-name');
            var of_id = $(v).data('employee-office-id');
            var unit_id = $(v).data('office-unit-id');
            var org_id = $(v).data('office-unit-organogram-id');
            var designation = $(v).data('designation-name');
            var unit_name = $(v).data('unit-name');
            var designation_level = $(v).data('designation-level');
            var incharge_label = $(v).data('employee-incharge-label');
            var perm_value = parseInt($(v).val());
            $.each($('#addownpermission').find('tr'), function (ind, val) {
                if ($(val).find('.own_office_permission').data('office-unit-organogram-id') == org_id) {
                    designation_level = $(val).find('#designation_level').val();
                }
            });
            ok += '<tr><td style="min-width: 5px;text-align: center;">&nbsp;&nbsp;';
            ok += '<input type="hidden" class="own_office_permission" name="office-employee" data-office-id="' + of_id + '"' +
                'data-office-employee-name="' + name + '" data-office-employee-id="' + id + '"' +
                ' data-office-unit-id="' + unit_id + '" data-office-unit-organogram-id="' + org_id + '" ' + ' data-incharge-label="' + incharge_label + '" ' +
                'data-designation-name="' + designation + '" data-unit-name="' + unit_name + '" data-designation-level ="' + designation_level + '">' +
                '</td><td style="min-width: 5px;">&nbsp;' + name + ', ' + designation + ', ' + unit_name + '</td><td><input id="designation_level" type="text" value="' + designation_level + '"></td></tr>';

        });
        ok += '</tbody></table></div>';
        $('#addownpermission').html('');
        $('#addownpermission').html(ok);
    }

    function listownother() {
        $('#addotherpermission').html('');
        $.ajax({
            type: 'POST',
            url: "<?php echo $this->Url->build(['controller' => 'NothiMasters', 'action' => 'other_user']) ?>",
            data: {
                'api': 1,
                'employee_office': <?= json_encode($employee_office) ?>,
                "master_id": <?= $id ?>,
                "nothi_office": <?= $nothi_office ?>},
            success: function (data) {

                $('#addotherpermission').html(data);
            }
        });
    }

    function add_other_offices_by_selection(ok) {
        $.each($('.otherPermissionList').find('.optionsothers:checked'), function (i, v) {

            var id = $(v).data('employee-id');
            var name = $(v).data('employee-name');
            var of_id = $(v).data('employee-office-id');
            var unit_id = $(v).data('office-unit-id');
            var org_id = $(v).data('office-unit-organogram-id');
            var designation = $(v).data('designation-name');
            var unit_name = $(v).data('unit-name');
            var designation_level = $(v).data('designation-level');
            var perm_value = parseInt($(v).val());
            var incharge_label = $(v).data('employee-incharge-label');
            ok += '<tr><td style="min-width: 5px;text-align: center;">&nbsp;&nbsp;';
            ok += '<input type="hidden" class="other_office_permission" name="office-employee" data-office-id="' + of_id + '"' +
                'data-office-employee-name="' + name + '" data-office-employee-id="' + id + '"' +
                ' data-office-unit-id="' + unit_id + '" data-office-unit-organogram-id="' + org_id + '" ' + ' data-incharge-label="' + incharge_label + '" ' +
                'data-designation-name="' + designation + '" data-unit-name="' + unit_name + '" data-designation-level ="' + designation_level + '">' +
                '</td><td>&nbsp;&nbsp;' + name + ', ' + designation + ', ' + unit_name + '</td><td><input id="designation_level" type="text" value="' + designation_level + '"></td></tr></tr>';

        });

        $('#addotherpermission').append(ok);
    }

    function removeDuplicateRows($table) {
        function getVisibleRowText($row) {
            return $row.find('td:visible').text().toLowerCase();
        }

        $table.find('tr').each(function (index, row) {
            var $row = $(row);

            $row.nextAll('tr').each(function (index, next) {
                var $next = $(next);
                if (getVisibleRowText($next) == getVisibleRowText($row))
                    $next.remove();
            });
        });
    }

    function list_all(e_id) {
        $.each($('.otherPermissionList').find('.optionsothers'), function (i, v) {
            if ($(v).data('office-unit-organogram-id') == e_id) {
                if ($(v).is(':checked')) {
                    var ok = '';
                    var id = $(v).data('employee-id');
                    var name = $(v).data('employee-name');
                    var of_id = $(v).data('employee-office-id');
                    var unit_id = $(v).data('office-unit-id');
                    var org_id = $(v).data('office-unit-organogram-id');
                    var designation = $(v).data('designation-name');
                    var unit_name = $(v).data('unit-name');
                    var designation_level = $(v).data('designation-level');
                    var perm_value = parseInt($(v).val());
                    var incharge_label = $(v).data('employee-incharge-label');
                    ok += '<tr><td style="min-width: 5px;text-align: center;">&nbsp;&nbsp;';
                    ok += '<input type="hidden" class="other_office_permission" name="office-employee" data-office-id="' + of_id + '"' +
                        'data-office-employee-name="' + name + '" data-office-employee-id="' + id + '"' +
                        ' data-office-unit-id="' + unit_id + '" data-office-unit-organogram-id="' + org_id + '" ' + ' data-incharge-label="' + incharge_label + '" ' +
                        'data-designation-name="' + designation + '" data-unit-name="' + unit_name + '" data-designation-level ="' + designation_level + '">' +
                        '</td><td>&nbsp;&nbsp;' + name + ', ' + designation + ', ' + unit_name + '</td><td><input id="designation_level" type="text" value="' + designation_level + '"></td></tr></tr>';
                    $('#addotherpermission').append(ok);
                } else {
                    $.each($('#addotherpermission').find('.other_office_permission'), function () {
                        if ($(this).data('office-unit-organogram-id') == e_id) {
                            $(this).closest('tr').remove();
                        }
                    });
                }
            }
        });
    }

    $(document).off('click','.nothiPermissionUpdate').on('click', ".nothiPermissionUpdate", function (e) {

        Metronic.blockUI({
            target: '#ajax-content',
            boxed: true
        });
        var selectedPriviliges = {};
        var j = 0;

        var posi = 0;//$('.nothiPermissionUpdate').index(this);

        $.each($('.permissiondiv').find('.own_office_permission'), function (i, v) {
            selectedPriviliges[i] = {};
            selectedPriviliges[i]['ofc_id'] = $(v).data('office-id');
            selectedPriviliges[i]['unit_id'] = $(v).data('office-unit-id');
            selectedPriviliges[i]['org_id'] = $(v).data('office-unit-organogram-id');
            selectedPriviliges[i]['incharge_label'] = $(v).data('incharge-label');
            //            selectedPriviliges[i]['designation_level'] = $(v).data('designation-level');
            selectedPriviliges[i]['perm_value'] = 1;//parseInt($(v).val());
            j = j + 1;
        });
        $.each($('#addownpermission').find('tr'), function (i, v) {
            if ($(v).find('#designation_level').val() == '') {
                $(v).find('#designation_level').val(0);
            }
            selectedPriviliges[i]['designation_level'] = $(v).find('#designation_level').val();
        });
        $.each($('#addotherpermission').find('tr'), function (i, v) {
            selectedPriviliges[i + j] = {};
            if ($(v).find('#designation_level').val() == '') {
                $(v).find('#designation_level').val(0);
            }
            selectedPriviliges[i + j]['designation_level'] = $(v).find('#designation_level').val();
        });
        $.each($('.permissiondiv').find('.other_office_permission'), function (i, v) {

            selectedPriviliges[j]['ofc_id'] = $(v).data('office-id');
            selectedPriviliges[j]['unit_id'] = $(v).data('office-unit-id');
            selectedPriviliges[j]['org_id'] = $(v).data('office-unit-organogram-id');
            selectedPriviliges[i]['incharge_label'] = $(v).data('incharge-label');
            selectedPriviliges[j]['perm_value'] = 1;
            j = j + 1;
        });
        $.ajax({
            url: js_wb_root + "nothiMasters/nothiMasterPermissionUpdate/" + <?php echo $id ?> +'/<?php echo $nothi_office ?>',
            data: {'api': 1, 'employee_office': <?= json_encode($employee_office) ?>, priviliges: selectedPriviliges},
            dataType: 'JSON',
            method: 'POST',
            success: function (response) {

                Metronic.unblockUI('#ajax-content');
                if (response.status == 'error') {

                    if(posi===0){
                        toastr.error(response.msg,"ত্রুটি",{
                            "positionClass": "toast-top-right"
                        });
                    }else{
                        toastr.error(response.msg,"ত্রুটি",{
                            "positionClass": "toast-bottom-right"
                        });
                    }

                } else if (response.status == 'success') {
                    if(posi===0){
                        toastr.success(response.msg,"সফল",{
                            "positionClass": "toast-top-right"
                        });
                    }else{
                        toastr.success(response.msg,"সফল",{
                            "positionClass": "toast-bottom-right"
                        });
                    }
                    if(!isEmpty(response.extra_message)){
                        bootbox.dialog({
                            message: response.extra_message,
                            title: "অনুমতি সংশোধন সময়কার সমস্যা",
                            buttons: {

                                danger: {
                                    label: "বন্ধ করুন",
                                    className: "red",
                                    callback: function () {
                                    }
                                }
                            }
                        });
                    }
                }
            },
            error: function (status, xresponse) {

            }

        });
    });
</script>