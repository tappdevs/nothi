<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>

<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2.css" type="text/javascript">

<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2-bootstrap.css" type="text/javascript">

<div class="row permissiondiv">
    <div class="col-md-6">
	    <div class="portlet box green">
		    <div class="portlet-title"><h3 style="font-weight: bold;margin-top:7px;">পদবি নির্বাচন করুন</h3></div>
		    <div class="portlet-body">
			    <div class="tabbable-custom ">
				    <ul class="nav nav-tabs ">
					    <li class="active">
						    <a href="#ownOfficeSelection" data-toggle="tab" aria-expanded="false">নিজ অফিসের পদসমূহ</a>
					    </li>
					    <li class="">
						    <a href="#otherOfficeSelection" data-toggle="tab" aria-expanded="true">অন্য অফিসের পদসমূহ</a>
					    </li>
				    </ul>
				    <div class="tab-content">
					    <div class="tab-pane active" id="ownOfficeSelection">
						    <div class="table-container">
							    <div class="panel-group accordion">
									<?= $data; ?>
							    </div>
						    </div>
					    </div>
					    <div class="tab-pane" id="otherOfficeSelection">
						    <div class="permissiondiv">
								<?php echo $this->cell('OfficeUnitOrganogramByMinistry', ['',0,$nothi_office]); ?>
						    </div>
					    </div>
				    </div>
			    </div>
		    </div>
	    </div>
    </div>
    <div class="col-md-6">
        <div class="portlet box green">
            <div class="portlet-title"><h3 style="font-weight: bold;margin-top:7px;">নির্বাচিত পদসমূহ</h3></div>
            <div class="portlet-body">
                <div class="tabbable-custom ">
                    <ul class="nav nav-tabs ">
                        <li class="active">
                            <a href="#ownOfficeSelected" data-toggle="tab" aria-expanded="false">নিজ অফিসের পদসমূহ (<span id="ownOfficeTotalSelected">০</span>)</a>
                        </li>
                        <li class="">
                            <a href="#otherOfficeSelected" data-toggle="tab" aria-expanded="true">অন্য অফিসের পদসমূহ (<span id="otherOfficeTotalSelected">০</span>)</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="ownOfficeSelected">
                            <table class="table table-bordered table-hover table-striped">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>পদবি</th>
                                    <th class="hidden">ক্রম</th>
                                </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                        <div class="tab-pane" id="otherOfficeSelected">
                            <table class="table table-bordered table-hover table-striped">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>পদবি</th>
                                    <th>ক্রম</th>
                                </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $("a[href=#ownOfficeSelection]").on('click', function() {
            $("a[href=#ownOfficeSelected]").tab('show');
        });
        $("a[href=#ownOfficeSelected]").on('click', function() {
            $("a[href=#ownOfficeSelection]").tab('show');
        });

        $("a[href=#otherOfficeSelection]").on('click', function() {
            $("a[href=#otherOfficeSelected]").tab('show');
        });
        $("a[href=#otherOfficeSelected]").on('click', function() {
            $("a[href=#otherOfficeSelection]").tab('show');
        });

        setTimeout(function() {
            $("#ownOfficeUnitList").val(<?=$nothi_unit?>).trigger('change');
        }, 1000);
    });
    function checkAllEmployee(element, div_id) {
        if ($(element).is(':checked')) {
            $("#"+div_id+" input[type=checkbox]").prop('checked', true);
            listown();listownother();
        } else {
            $("#"+div_id+" input[type=checkbox]").not('[data-office-unit-organogram-id="<?=$current_organogram?>"]').prop('checked', false)
            listown();listownother();
        }
        //current_organogram

    }
</script>