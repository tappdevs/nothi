<style>

    .radio-list {
        display: inline;
    }

    .checkbox input[type=checkbox], .checkbox-inline input[type=checkbox], .radio input[type=radio], .radio-inline input[type=radio] {
        margin-left: 0px;
        position: relative;
    }

    .checkbox-inline, .radio-inline {
        padding-left: 5px;
        font-size: 12px;
    }

    .checkbox-inline + .checkbox-inline, .radio-inline + .radio-inline {
        margin-left: 2px;
    }
</style>

<div class="portlet light">
    <div class="portlet-window-body">
        <input type="hidden" class="office-id" value="<?php echo $office_id ?>"/>

        <div class="row" id="tree_view_div">
            <div class="col-md-6">
                <div class="portlet light">
                    <div class="portlet-window-body">
                        <div class="content_tree"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Start : Office Setup JS -->

<script type="text/javascript">
    $(function () {
        OfficeUnitOrgView.init();
    });
</script>
<script type="text/javascript">
    var OfficeUnitOrgView = {
        selected_node: "",
        parent_id: 0,
        parent_node: "",
        selected_ministry_id: 0,
        selected_layer_id: 0,
        selected_office_id: 0,

        unitTreeView: function () {
            OfficeUnitOrgView.selected_office_id = $(".office-id").val();

            $('.content_tree').jstree("refresh");
            $('.content_tree').jstree({
                "core": {
                    "themes": {
                        "variant": "large"
                    },
                    'data': {
                        'url': function (node) {
                            return js_wb_root + 'officeManagement/loadEmployeeUnitOrganogramsTree';
                        },
                        'data': function (node) {
                            return {'id': node.id, 'icon': 0, 'office_id': $(".office-id").val()};
                        }
                    }
                }
            });
        },

        init: function () {
            OfficeUnitOrgView.unitTreeView();
        }
    };


</script>

<!-- End : Office Setup JS -->