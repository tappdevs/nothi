<style>

    .radio-list {
        display: inline;
    }

    .checkbox input[type=checkbox], .checkbox-inline input[type=checkbox], .radio input[type=radio], .radio-inline input[type=radio] {
        margin-left: 0px;
        position: relative;
    }

    .checkbox-inline, .radio-inline {
        padding-left: 5px;
        font-size: 12px;
    }

    .checkbox-inline + .checkbox-inline, .radio-inline + .radio-inline {
        margin-left: 2px;
    }
</style>

<div class="portlet light">
    <div class="portlet-window-body">
        <input type="hidden" class="office-id" value="<?php echo $office_id ?>"/>
        <?php

        echo '<div class="inline-div"><div ><img class="icons noPermission"></img><span>নথি দেখতে ও কাজ করতে পারবে না</span> </div><div class="" ><img class="icons allPermission"></img> <span>নথি দেখতে ও কাজ করতে পারবে</span> </div>  <div class="" ><img class="icons canSee"></img><span>নথি দেখতে পারবে</span></div></div>';

        ?>
        <div class="row">
            <div class="col-md-6">
                <div class="portlet light">
                    <div class="portlet-window-body">
                        <div class="content_tree_with_icon"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Start : Office Setup JS -->

<script type="text/javascript">
    $(function () {
        OfficeUnitOrgIconView.init();
    });
</script>
<script type="text/javascript">
    var OfficeUnitOrgIconView = {
        selected_node: "",
        parent_id: 0,
        parent_node: "",
        selected_ministry_id: 0,
        selected_layer_id: 0,
        selected_office_id: 0,

        unitTreeView: function () {
            OfficeUnitOrgIconView.selected_office_id = $(".office-id").val();

            $('.content_tree_with_icon').jstree("refresh");
            $('.content_tree_with_icon').jstree({
                "core": {
                    "themes": {
                        "variant": "large"
                    },
                    'data': {
                        'url': function (node) {
                            return js_wb_root + 'officeManagement/loadEmployeeUnitOrganogramsTree';
                        },
                        'data': function (node) {
                            return {'id': node.id, 'icon': 1, 'office_id': $(".office-id").val()};
                        }
                    }
                }
            });
        },

        init: function () {
            OfficeUnitOrgIconView.unitTreeView();
        }
    };

    $(document).on('click', '.optionsRadios', function (e) {
        e.preventDefault();
        $('.optionsRadios[name=' + $(this).attr('name') + ']').removeAttr('checked');
        $(this).attr('checked');
        $('.optionsRadios[name=' + $(this).attr('name') + ']').closest('span').removeClass('checked');
        $(this).closest('span').addClass('checked');
    });

</script>

<!-- End : Office Setup JS -->