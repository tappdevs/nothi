<div class="row">
    <div class="form-group">
        <label class="col-sm-2 control-label text-right"><?= __("Office") ?> <?= __("Ministry") ?></label>

        <div class="col-sm-8">
            <?php
            echo $this->Form->input('office_ministry_id', array(
                'label' => false,
                'class' => 'form-control',
                'options' => $data_items,
                'value' => $value,
                'empty' => '----'
            ));
            ?>
        </div>
    </div>
</div>
