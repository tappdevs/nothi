<div class="row">
    <div class="col-md-12">
        <div class="portlet green-meadow box">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-cogs"></i>Search Your Office from <?= $hierarchy_name; ?></div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                    <a href="#portlet-config" data-toggle="modal" class="config"></a>
                    <a href="javascript:;" class="reload"></a>
                    <a href="javascript:" class="remove"></a>
                </div>
            </div>
            <div class="portlet-body form">
                <?= $this->Form->create("", ['class' => 'form-horizontal']); ?>
                <div class="form-body">
                    <div class="form-group">
                        <div class="col-md-6">
                            <?php echo $this->Form->input('office_organization', array('list' => 'office_organization_list', 'label' => false, 'class' => 'form-control')); ?>
                            <?php echo $this->Form->hidden('office_organization_id', array('id' => 'office-organization-id', 'label' => false, 'class' => 'form-control')); ?>
                        </div>
                        <div class="col-md-6">
                            <button type="button" class="btn btn-danger office_org_btn">Load Office Organogram</button>
                        </div>

                        <datalist id="office_organization_list">
                            <?php foreach ($office_organization_list as $ool) { ?>
                                <option value="<?= $ool; ?>"></option>
                            <?php } ?>
                        </datalist>
                    </div>
                </div>
                <?= $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('.office_org_btn').bind('click', function () {
        var json_data = <?php echo json_encode($office_organization_list);?>;
        var parent_script = '<?php echo $parent_script;?>';
        <?php echo ''.$parent_script.'.';?>loadOfficeOrganograms(json_data);
    });
</script>
