<?php $selected_org_id = $org_id; ?>
<div class="row">
    <div class="col-md-12">
        <div class="portlet green-meadow box">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-cogs"></i>Organogram of <?= $org_name; ?></div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                    <a href="#portlet-config" data-toggle="modal" class="config"></a>
                    <a href="javascript:" class="reload"></a>
                    <a href="javascript:" class="remove"></a>
                </div>
            </div>
            <div class="portlet-body form">
                <div id="orgngrm_tree">
                    <!-- // -->
                </div>
            </div>
        </div>
    </div>
</div>

