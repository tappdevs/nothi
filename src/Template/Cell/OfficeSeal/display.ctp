<style>
    .scroller {
        overflow: auto;
        height: auto!important;
    }
    .slimScrollDiv{
        height: auto!important;
    }

    .office_employee_checkbox {
        display: none;
    }
</style>

<table class="table table-bordered officeSealDiv">
    <thead>
        <tr>
            <th style="text-align: center; width: 10%">
                <a title="প্রাপক বাছাই"  data-title="প্রাপক বাছাই" href="javascript:void(0)" class="btn btn-xs green newSeal"><i class="fs1 a2i_gn_add1"></i> </a>
                <!-- <a href="javascript:void(0)" class="btn btn-xs purple tempSeal" title="অস্থায়ী সিল বানান"  data-title="অস্থায়ী সিল বানান"><i class="fs1 a2i_gn_add1"></i> </a>-->
            </th>
            <th >পদ</th>
            <th >নাম</th>
            <th>মূল প্রাপক</th>
            <th>অনুলিপি প্রাপক </th>

        </tr>
    </thead>
    <tbody>
        <?php
        for ($i = 0; $i < count($query); $i++) {
            ?>
            <tr>
                <td style="width:10%;text-align: center;">
                    <a class="btn btn-danger btn-xs deleteSeal delete" title="মুছে ফেলুন"  data-title="মুছে ফেলুন" data-id="<?php echo $query[$i]["id"]; ?>"><i class="fs1 a2i_gn_delete2 "></i></a>
                </td>
                <td style="width:40%;">
                    <span  title="<?= h($query[$i]["employee_name"]) ."\n". $query[$i]['designation_description'] ?>" >
                    <?php echo $query[$i]["designation_name_bng"] . ', ' . $query[$i]["unit_name_bng"]; ?>
                    </span>
                </td>
                <td style="width:30%;">
                    <?php echo h($query[$i]["employee_name"]) ?>
                </td>
                <td style="width: 10%; text-align: center;">
                    <input class="office_employee_to" type="radio" id="<?php echo $i; ?>" name="office-employee_to"
                           data-employee-office-id ="<?php echo $query[$i]['employee_office_id'] ?>"
                           data-main-office-unit-id="<?php echo $query[$i]["office_unit_id"]; ?>"
                           data-main-office-employee-id="<?php echo $query[$i]["employee_record_id"]; ?>"
                           data-main-office-employee-name="<?php echo h($query[$i]["employee_name"]); ?>"
                           data-main-office-unit-organogram-id="<?php echo $query[$i]["office_unit_organogram_id"]; ?>">
                </td>
                <td style="text-align: center; width: 10%">
                    <input class="office_employee_checkbox text-center" type="checkbox"
                           id="emp_<?php echo $query[$i]["office_unit_organogram_id"]; ?>" name="office-employee"
                           data-label="<?php echo $query[$i]["designation_name_bng"] . ',' . $query[$i]["unit_name_bng"]; ?>"
                           data-employee-office-id ="<?php echo $query[$i]['employee_office_id'] ?>"
                           data-office-employee-id="<?php echo $query[$i]["employee_record_id"]; ?>"
                           data-office-employee-name="<?php echo h($query[$i]["employee_name"]); ?>"
                           data-office-unit-id="<?php echo $query[$i]["office_unit_id"]; ?>"
                           data-office-unit-organogram-id="<?php echo $query[$i]["office_unit_organogram_id"]; ?>">
                </td>

            </tr>
            <?php
        }
        ?>

    </tbody>
</table>

<script>

    $(document).off('click', '.office_employee_to');
    $(document).on('click', '.office_employee_to', function () {
        $(this).closest('table').find('.office_employee_checkbox').show();
        $(this).closest('table').find('.checker').show();

        $(this).closest('table').find('.office_employee_to').closest('span.checked').removeClass('checked');
        $(this).closest('span').addClass('checked');

        $(this).closest('tr').find('.checker').hide();
        $(this).closest('tr').find('.office_employee_checkbox').hide();
        if ($(this).closest('tr').find('.office_employee_checkbox').is(':checked')) {
            $(this).closest('tr').find('.office_employee_checkbox').click();
            $(this).closest('tr').find('.office_employee_checkbox').click();
        } else {
            $(this).closest('tr').find('.office_employee_checkbox').click();
        }

    });

    $(document).off('click', '.tempSeal');
    $(document).on('click', '.tempSeal', function () {
        $("#save-seal").css("display", "none");
        $("#save-temp-seal").css("display", "none");
        $('.addSeal').modal('show');
        $('.addSeal').find('.scroller').html('<img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"><span>&nbsp;&nbsp;লোড করা হচ্ছে... </span>');

        $('#sealtype').val(1);

        $.ajax({
            url: '<?php echo $this->request->webroot ?>officeManagement/officeSeal/1',
            dataType: 'html',
            success: function (res) {
                $('.addSeal').find('.scroller').html(res);
                OfficeSeal.checkIfSealExists();
            }
        });

    });

    $(document).off('click', '.newSeal');
    $(document).on('click', '.newSeal', function () {
        $('#sealtype').val(0);
        $("#save-seal").css("display", "none");
        $("#save-temp-seal").css("display", "none");
        $('.addSeal').modal('show');
        $('.addSeal').find('.scroller').html('<img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"><span>&nbsp;&nbsp;লোড করা হচ্ছে... </span>');

        $.ajax({
            url: '<?php echo $this->request->webroot ?>officeManagement/officeSeal',
            dataType: 'html',
            success: function (res) {
                $('.addSeal').find('.scroller').html(res);
                OfficeSeal.checkIfSealExists();
            }
        });

    });

    $(document).on('hidden.bs.modal', '.addSeal', function () {
        $('.addSeal').find('.scroller').html('');
    });

$(function(){
     $(document).find('[title]').tooltip({'placement':'bottom'});
})

</script>



