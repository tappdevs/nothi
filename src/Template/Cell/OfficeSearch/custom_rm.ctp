<style>
    .saveButton{
        position: absolute;
        left:20px;
    }
    .tabbable-custom{
        margin-bottom: 0px;
        overflow-y: visible;
    }
</style>
<?php
$office_id = !empty($office_id)?$office_id:0;
?>
<div class="tabbable-custom  nav-justified">
    <ul class="nav  nav-tabs nav-justified">
        <li class="tabsearch <?= ($prefix != 'sender_' && $prefix != 'approval_' && $prefix != 'attension_')?'active':'' ?>">
            <a href="#<?php echo $prefix ?>tab_autosearch" data-toggle="tab"> অফিসার খুঁজুন </a>
        </li>
        <li class="tabsearch <?= ($prefix == 'sender_' || $prefix == 'approval_' || $prefix == 'attension_')?'active':'' ?>">
            <a href="#<?php echo $prefix ?>tab_dropdown" data-toggle="tab"> অফিসার বাছাই করুন</a>
        </li>
        <?php
        if ($prefix != 'sender_' && $prefix != 'approval_' && $prefix != 'sovapoti_'  && $prefix != 'attension_') {
            ?>
            <li class="tabsearch">
                <a href="#<?php echo $prefix ?>tab_office_show" data-toggle="tab"> অফিস বাছাই করুন</a>
            </li>
             <li class="tabsearch">
                <a href="#<?php echo $prefix ?>tab_newoffice" data-toggle="tab"> অন্যান্য  
                </a>
            </li>

        <?php } ?>
    </ul>
    <div class="tab-content"  style="min-height:320px;">
        <div class="tab-pane <?= ($prefix != 'sender_' && $prefix != 'approval_'  && $prefix != 'attension_')?'active':'' ?>" id="<?php echo $prefix ?>tab_autosearch">
            <?php
            echo $this->cell('OfficeSearch::officeSearch',
                ['prefix' => $prefix,[],$office_id]);
                echo $this->element('PotrojariTemplates/rm_cell_footer', [
                    "prefix" =>$prefix,
                    'refer' => 'tab_autosearch',
                    'potro_type' => $potro_type,
                ]);
                ?>
            
        </div>
        <div class="tab-pane <?= ($prefix == 'sender_' || $prefix == 'approval_'  || $prefix == 'attension_')?'active':'' ?>" id="<?php echo $prefix ?>tab_dropdown">
<?php
echo $this->cell('OfficeSearch::officeChoose',
    ['prefix' => $prefix,$office_id]);
    echo $this->element('PotrojariTemplates/rm_cell_footer', [
             "prefix" =>$prefix,
             'refer' => 'tab_dropdown',
             'potro_type' => $potro_type,
         ]);
    ?>
        </div>
            <?php if ($prefix != 'sender_' && $prefix != 'approval_' && $prefix
                != 'sovapoti_'  && $prefix != 'attension_') { ?>
         <div class="tab-pane" id="<?php echo $prefix ?>tab_office_show">
            <?php
            echo $this->cell('OfficeSearch::OfficeSelection',
                ['prefix' => $prefix,[],$office_id]);
               echo $this->element('PotrojariTemplates/rm_cell_footer', [
                    "prefix" =>$prefix,
                    'refer' => 'tab_office_show',
                    'potro_type' => $potro_type,
                ]);
                ?>
            </div>
            <div class="tab-pane" id="<?php echo $prefix ?>tab_newoffice">
    <?php
    echo $this->cell('OfficeSearch::officeNew',
        ['prefix' => $prefix]);
      echo $this->element('PotrojariTemplates/rm_cell_footer', [
                    "prefix" =>$prefix,
                    'refer' => 'tab_newoffice',
                    'potro_type' => $potro_type,
                ]);
        ?>
            </div>
    <?php }
?>
    </div>
</div>

<script>

    $('.tabsearch a').on('click', function (e) {
        // first close all the modal
//        $('.collapse').each(function(){
//	$(this).removeClass('in');
//        });
        $('a[href="' + $(this).attr('href') + '"]').tab('show');
        return false;
    });
    $('.closethis').on('click', function (e) {
       $(this).closest('.collapse').removeClass('in');
        return false;
    });

</script>