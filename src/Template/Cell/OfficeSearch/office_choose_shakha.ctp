<style>
    ul#ul-<?= $prefix?>office_unit_organogram_shakha li
    {
        padding-top:7px;
        padding-bottom:7px;
        border-bottom:1px solid #eee;
        list-style:none;
        margin-left:-20px;
    }

    input[type=checkbox]
    {
        width:17px;
        height:17px;
    }
</style>
<div class="row">
    <div class="col-md-12">
        <?= $officeSelectionCell = $this->cell('OfficeSelection::displayShakha', ['entity' => '', 'prefix' => $prefix]) ?>
        <div class="row">
            <div class="col-md-4 form-group form-horizontal">
                <label class="control-label"> কার্যালয় </label>
                <?php echo $this->Form->input($prefix . 'office_id_shakha', array('empty' => '--বাছাই করুন--', 'type' => 'select', 'label' => false, 'class' => 'form-control input-sm')); ?>
            </div>
            <div class="col-md-4 form-group form-horizontal">
                <label class="control-label"> দপ্তর/শাখা </label>
                <?php echo $this->Form->input($prefix . 'office_unit_id_shakha', array('empty' => '--বাছাই করুন--', 'type' => 'select', 'label' => false, 'class' => 'form-control input-sm')); ?>
            </div>
            
            <div class="col-md-4 form-group form-horizontal ">
                <label class="control-label">  </label>
                <?php //echo $this->Form->input($prefix . 'office_unit_organogram_id_shakha', array('empty' => '--বাছাই করুন--', 'type' => 'select', 'label' => false, 'class' => 'form-control input-sm')); ?>
                <?php //echo $this->Form->hidden($prefix . 'office_unit_organogram_shakha', array('id' => $prefix . 'office_unit_organogram_shakha')); ?>
            
                <?php
            //if($is_api==0): ?>
                <?php if($prefix == 'sovapoti_' || $prefix == 'receiver_' || $prefix == 'onulipi_' || $prefix == 'attension' ): ?>
                <!-- <div class="col-md-4 form-group form-horizontal"> -->
                    <br/>
                    <label class="control-label"> নিজ অফিস </label>
                    <?php echo $this->Form->checkbox($prefix . 'own_office',['id'=>$prefix . 'own_office_shakha','style'=>'vertical-align: sub']); ?>
                <!-- </div> -->
                <?php endif; ?>
                <?php //endif; ?>

            </div>
            <div class="col-md-12 col-sm-12 form-group form-horizontal" id='<?=$prefix?>loading-designations'>
                <div class='panel panel-default'>
                    <div class="panel-heading bg-info">
                        <label class="control-label " style="padding-top:0px;padding-left:5px"> সকল পদবি নির্বাচন করুন </label>
                        <?php echo $this->Form->checkbox("all_designation_select",['id'=>'all-designation-select-'.rtrim($prefix,'_'),'class'=>'pull-left']); ?>

                    </div>
                    <div class="panel-body pre-scrollable" >
                        <ul id='ul-<?= $prefix?>office_unit_organogram_shakha'></ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

    <script src="<?php echo CDN_PATH; ?>assets/admin/layout4/scripts/projapoti_ajax.js?v=<?= js_css_version ?>" type="text/javascript"></script>
<script>

    var is_api = '<?= isset($is_api)?($is_api):0 ?>';
    is_api = parseInt(is_api);
    // $(window).load(function() {
        (function($) {
        var prefix = '<?php echo $prefix;?>';
        var prefixmain = '<?php echo $prefix;?>';
        var office_id = '<?php echo isset($office_id)?$office_id:0;?>';

        prefix = prefix.split("_");
        prefix = prefix[0] + '-';

		<?php if($openOwnOffice && $office_id>0): ?>
            $('#<?= $prefix ?>own_office').click();

            $("#" + prefix + "office-id-shakha").val(office_id);
            $("#" + prefix + "office-ministry-id-shakha").closest('.row').hide();

            PROJAPOTI.projapoti_dropdown_map("#" + prefix + "office-id-shakha", <?php echo $officeArray; ?>, '', office_id);
            setTimeout(function(){
				$("#" + prefix + "office-id-shakha").change();
				$("#" + prefix + "office-unit-id-shakha").html('');
            },500)

        <?php endif; ?>

        $("#" + prefix + "office-ministry-id-shakha").bind('change', function () {
            OfficeOrgSelectionCellShakha.loadMinistryWiseLayers(prefix);
        });
        $("#" + prefix + "office-layer-id-shakha").bind('change', function () {
            OfficeOrgSelectionCellShakha.loadMinistryAndLayerWiseOfficeOrigin(prefix);
        });
        $("#" + prefix + "office-origin-id-shakha").bind('change', function () {
            OfficeOrgSelectionCellShakha.loadOriginOffices(prefix);
        });
        $("#" + prefix + "office-id-shakha").bind('change', function () {
            OfficeOrgSelectionCellShakha.loadOfficeUnits(prefix);
        });
        $("#" + prefix + "office-unit-id-shakha").bind('change', function () {
            OfficeOrgSelectionCellShakha.loadOfficeUnitDesignations(prefix);
        });
        $("#" + prefix + "office-unit-organogram-id-shakha").bind('change', function () {
            OfficeOrgSelectionCellShakha.getOfficerInfo($(this).val(), prefix, prefixmain);
        });
        
        if(office_id>0 && (prefixmain == 'sender_' || prefixmain == 'approval_')){
            $("#" + prefix + "office-id-shakha").val(office_id);
            $("#" + prefix + "office-ministry-id-shakha").closest('.row').hide();
            
            PROJAPOTI.projapoti_dropdown_map("#" + prefix + "office-id", <?php echo $officeArray; ?>, '',office_id);
            $("#" + prefix + "office-unit-id-shakha").html('');
            PROJAPOTI.ajaxSubmitDataCallback('<?php echo $this->request->webroot ?>' + 'officeManagement/loadOfficeUnits',
                {'office_id': office_id}, 'html',
                function (response) {
                    $("#" + prefix + "office-unit-id-shakha").html(response);
                });
        }

        $("#<?= $prefix ?>own_office_shakha").click(function(){

            if(this.checked){
                if(office_id > 0){
                    prefix_new = prefix.replace('-','_');
                    $("#" + prefix + "office-id-shakha").val(office_id);
                    $("#" + prefix + "office-ministry-id-shakha").closest('.row').hide();
                    $('#ul-'+prefix_new +'office_unit_organogram_shakha').find('li').remove();
                    PROJAPOTI.projapoti_dropdown_map("#" + prefix + "office-id-shakha", <?php echo $officeArray; ?>, '',office_id);
                    $("#" + prefix + "office-id-shakha").change();
                    $("#" + prefix + "office-unit-id-shakha").html('');
                }
            }else{
                $("#" + prefix + "office-id-shakha").val(0);
                $("#" + prefix + "office-ministry-id-shakha").closest('.row').show();
                $("#" + prefix + "office-office-id-shakha").html('');
                $("#" + prefix + "office-unit-id-shakha").html('');
            }
        })

        
    }(jQuery));

</script>
<script type="text/javascript">
    var prefix_new;
    var OfficeOrgSelectionCellShakha = {

        loadMinistryWiseLayers: function (prefix) {
            $("#" + prefix + "office-layer-id-shakha").find('option').remove().end().append('<option value=""> লোড হচ্ছে... </option>').val('');
            if(isEmpty(is_api)){
                $("#" + prefix + "office-layer-id-shakha").select2();
            }
            PROJAPOTI.ajaxSubmitDataCallback('<?php echo $this->request->webroot ?>' + 'officeSettings/loadLayersByMinistry',
                {'office_ministry_id': $("#" + prefix + "office-ministry-id-shakha").val()}, 'json',
                function (response) {
                    PROJAPOTI.projapoti_dropdown_map("#" + prefix + "office-layer-id-shakha", response, "--বাছাই করুন--");
                    if(isEmpty(is_api)){
                        $("#" + prefix + "office-layer-id-shakha").select2();
                    }
                });
        },
        loadMinistryAndLayerWiseOfficeOrigin: function (prefix) {
            $("#" + prefix + "office-origin-id-shakha").find('option').remove().end().append('<option value=""> লোড হচ্ছে... </option>').val('');
            if(isEmpty(is_api)){
                $("#" + prefix + "office-origin-id-shakha").select2();
            }
            PROJAPOTI.ajaxSubmitDataCallback('<?php echo $this->request->webroot ?>' + 'officeSettings/loadOfficesByMinistryAndLayer',
                {
                    'office_ministry_id': $("#" + prefix + "office-ministry-id-shakha").val(),
                    'office_layer_id': $("#" + prefix + "office-layer-id-shakha").val()
                }, 'json',
                function (response) {
                    PROJAPOTI.projapoti_dropdown_map("#" + prefix + "office-origin-id-shakha", response, "--বাছাই করুন--");
                    if(isEmpty(is_api)){
                        $("#" + prefix + "office-origin-id-shakha").select2();
                    }
                });
        },
        loadOriginOffices: function (prefix) {
            $("#" + prefix + "office-id-shakha").find('option').remove().end().append('<option value=""> লোড হচ্ছে... </option>').val('');
            if(isEmpty(is_api)){
                $("#" + prefix + "office-id-shakha").select2();
            }
            PROJAPOTI.ajaxSubmitDataCallback('<?php echo $this->request->webroot ?>' + 'officeManagement/loadOriginOffices',
                {'office_origin_id': $("#" + prefix + "office-origin-id-shakha").val()}, 'json',
                function (response) {
                    PROJAPOTI.projapoti_dropdown_map("#" + prefix + "office-id-shakha", response, "--বাছাই করুন--");
                    if(isEmpty(is_api)){
                        $("#" + prefix + "office-id-shakha").select2();
                    }
                });
        },
        loadOfficeUnits: function (prefix) {
            $("#" + prefix + "office-unit-id-shakha").find('option').remove().end().append('<option value=""> লোড হচ্ছে... </option>').val('');
            if(isEmpty(is_api)){
                $("#" + prefix + "office-unit-id-shakha").select2();
            }
            PROJAPOTI.ajaxSubmitDataCallback('<?php echo $this->request->webroot ?>' + 'officeManagement/loadOfficeUnits',
                {'office_id': $("#" + prefix + "office-id-shakha").val()}, 'html',
                function (response) {
                    $("#" + prefix + "office-unit-id-shakha").html(response);
                    if(isEmpty(is_api)){
                        $("#" + prefix + "office-unit-id-shakha").select2();
                    }
                });
        },
        loadOfficeUnitDesignations: function (prefix) {
//            console.log(prefix);
            prefix_new = prefix.replace('-', '_');
            var prefix2 = prefix.replace('-', '');
            var check_box = $("#all-designation-select-"+prefix2);
            check_box.prop('checked',false);
            check_box.parent().removeClass('checked');

            Metronic.blockUI({target: "#"+prefix_new+"loading-designations", animate: true});
            var url = '<?php echo $this->request->webroot ?>' + 'officeManagement/loadOfficeUnitOrganograms';
	        //if(isEmpty(is_api)){
                $("#" + prefix + "office-unit-organogram-id-shakha").select2();
                url = '<?php echo $this->request->webroot ?>' + 'officeManagement/loadOfficeUnitOrganogramListWithName';
            //}
            var office_unit_id_tmp = $("#" + prefix + "office-unit-id-shakha").val();
            if(office_unit_id_tmp==0 || office_unit_id_tmp=="0" )
            {
                Metronic.unblockUI("#"+prefix_new+"loading-designations");
                toastr.error("দুঃখিত! তথ্য সঠিক নয়");
                return false;
            }
            PROJAPOTI.ajaxSubmitDataCallback(url,
                {'office_unit_id': $("#" + prefix + "office-unit-id-shakha").val()}, 'json',
                function (response) {

                    Metronic.unblockUI("#"+prefix_new+"loading-designations");
                    $('#ul-'+prefix_new+'office_unit_organogram_shakha').find('li').remove();
                    if(response.length<1){   
                        $('#ul-'+prefix_new+'office_unit_organogram_shakha').append('<li>পদবি খুঁজে পাওয়া যায় নি!</li>');
                    }

                    check_box.prop('checked',false);
                    check_box.parent().removeClass('checked');

                    if(isEmpty(is_api)){
                     //run after add button


                        response.forEach(function(value) {

                            if (typeof (value.officer_id) === "undefined") {
                            }else{

                                if(potrojari_language=="bn")
                                {
                                    var incharge_label = '';
                                    if(value.incharge_label != "")
                                    {
                                        incharge_label = escapeHtml(value.incharge_label);
                                    }
                                    if(value.designation) {
                                        officerorg = escapeHtml(value.designation) + " ("+incharge_label+")";
                                    }

                                    var txt_str = {
                                        officerid: value.officer_id,
                                        officername: value.name,
                                        officerhead: value.office_head,
                                        officerofcid: value.office_id,
                                        officerofc: escapeHtml(value.office_name_bng),
                                        officerorgid: value.id,
                                        officerorg: officerorg,
                                        officerunitid: value.office_unit_id,
                                        officerunit: escapeHtml(value.unit_name_bng)
                                    };
                                }else if(potrojari_language=="eng")
                                {
                                    var error_msg = '';
                                    var officerorg;
                                    var incharge_label_eng = '';
                                    if(value.incharge_label_eng != '')
                                    {
                                        incharge_label_eng = escapeHtml(value.incharge_label_eng);
                                    }
                                    if(value.designation_name_eng && potrojari_language=='eng') {
                                        officerorg = escapeHtml(value.designation_name_eng) + " ("+incharge_label_eng+")";
                                    }
                                    var txt_str = {
                                        officerid: value.officer_id,
                                        officername: escapeHtml(value.name_eng),
                                        officerhead: value.office_head,
                                        officerofcid: value.office_id,
                                        officerofc: escapeHtml(value.office_name_eng),
                                        officerorgid: value.id,
                                        officerorg: officerorg,
                                        officerunitid: value.office_unit_id,
                                        officerunit: escapeHtml(value.unit_name_eng)
                                    };

                                }


                                //console.log(value);
                                //console.log(txt_str);

                                var json_str = JSON.stringify(txt_str);
                                var json_value = JSON.stringify(value);

                                $('#ul-' + prefix_new + 'office_unit_organogram_shakha').append("<li>" +
                                    "<input type=checkbox data-valueTmp='"+json_value+"' data-value='" + json_str + "'> " + value.designation + " - " + value.name +
                                    " "+
                                    "</li>");
                            }
                        });
                        Metronic.unblockUI("#"+prefix_new+"loading-designations");
                    }else{
                        
                            var officerofcid = $('#' + prefix + 'office-id-shakha option:selected').val();
                            var officerofc = $('#' + prefix + 'office-id-shakha option:selected').text();
                            var officerunitid = $('#' + prefix + 'office-unit-id-shakha option:selected').val();
                            var officerunit = $('#' + prefix + 'office-unit-id-shakha option:selected').text();

                        
                        response.forEach(function(value){

                           var txt_str = {
                                officerid:value.officer_id,
                                officername:value.name,
                                officerhead:value.office_head,
                                officerofcid:officerofcid,
                                officerofc:officerofc,
                                officerorgid:value.id,
                                officerorg:escapeHtml(value.designation),
                                officerunitid:officerunitid,
                                officerunit:officerunit
                            };


                            var json_str = JSON.stringify(txt_str);                  

                            $('#ul-'+prefix_new+'office_unit_organogram_shakha').append("<li><input type=checkbox data-value='"+json_str+"'> "+value.designation+" - "+value.name +" </li>");
                        });
                        
                    }

                });

        },
        getOfficerInfo: function (designation_id, prefix, prefixmain) {
            PROJAPOTI.ajaxSubmitDataCallback('<?php echo $this->request->webroot ?>' + 'employeeRecords/getOfficerInfo',
                {'designation_id': designation_id}, 'json',
                function (response) {
                    $("input[name=" + prefixmain + "officer_id]").val(response.id);
                    $("input[name=" + prefixmain + "officer_name]").val(response.name_bng);
                    $("input[name=" + prefixmain + "office_head]").val(response.office_head);
                    $("input[name=" + prefixmain + "incharge_label]").val(response.incharge_label);
                    $("input[name=" + prefixmain + "office_name_eng]").val(response.office_name_eng);
                    $("input[name=" + prefixmain + "unit_name_eng]").val(response.unit_name_eng);
                    $("input[name=" + prefixmain + "designation_name_eng]").val(response.designation_name_eng);
                    $("input[name=" + prefixmain + "name_eng").val(response.name_eng);
                    $("input[name=" + prefixmain + "incharge_label_eng").val(response.incharge_label_eng);
                    $("input[name=" + prefixmain + "personal_mobile").val(response.personal_mobile);

                }
            );
        },


    };

    $("#all-designation-select-<?= rtrim($prefix,'_');?>").on('click',function(e){

        var ul_list = $('#ul-<?= $prefix;?>office_unit_organogram_shakha').find('li input');
        if($("#all-designation-select-<?= rtrim($prefix,'_');?>").prop('checked'))
        {
            ul_list.each(function(el){
                $(this).prop('checked',true);
            });

        }else
        {
            ul_list.each(function(el){
                $(this).prop('checked',false);
            });

        }

    });


    $('#ul-<?= $prefix;?>office_unit_organogram_shakha').on('click','li input[type=checkbox]',function(e){
        if($(e.target).prop('checked'))
        {

        }else
        {
            var prefx = prefix_new.replace('_','');
            var all_select = $("#all-designation-select-"+prefx);
            all_select.prop('checked',false);
            all_select.parent().removeClass('checked');
        }
    });


</script>


