
    <div class="row">
        <div class="col-md-6 form-group ">
            <label class="control-label"> অফিসার খুঁজুন (নাম/পদবি, শাখা/কার্যালয়)</label>
            <?php
            $type_prefix = explode("_", $prefix);
            $type_prefix = "typeahead_" . $type_prefix[0];

            $dak_office = "";
            if (isset($sender_office)) {
                $dak_office = $sender_office;
            }
            if (isset($receiver_office)) {
                $dak_office = $receiver_office;
            }
            echo $this->Form->input($prefix . 'office', array('id' => $prefix . 'autocomplete_office_id', 'label' => false, 'class' => 'form-control input-sm  ' . $type_prefix, 'value' => $dak_office));
            ?>
        </div>
        <div class="col-md-6 form-group">
            <label class="control-label"> অফিসার  </label>
            <?php echo $this->Form->input($prefix . 'officer_name', array('label' => false, 'class' => 'form-control  input-sm', 'readonly' => false, 'value' => (isset($response_params[$prefix . 'officer_name'])) ? $response_params[$prefix . 'officer_name'] : '')); ?>
            <?php echo $this->Form->hidden($prefix . 'officer_id', array('id' => $prefix . 'officer_id', 'value' => (isset($response_params[$prefix . 'officer_id'])) ? $response_params[$prefix . 'officer_id'] : '')); ?>
            <?php echo $this->Form->hidden($prefix . 'office_head', array('id' => $prefix . 'office_head', 'value' => (isset($response_params[$prefix . 'office_head'])) ? $response_params[$prefix . 'office_head'] : 0)); ?>
        </div>
        <div id="<?php echo $prefix; ?>office_autocomplete_div" style="display: none;">
            <?php echo $this->Form->hidden($prefix . 'officer_designation_id_auto', array('type' => 'text', 'value' => isset($response_params[$prefix . 'officer_designation_id_auto'])?$response_params[$prefix . 'officer_designation_id_auto']:'')); ?>
            <?php echo $this->Form->hidden($prefix . 'officer_designation_label_auto', array('type' => 'text', 'value' => isset($response_params[$prefix . 'officer_designation_label_auto'])?$response_params[$prefix . 'officer_designation_label_auto']:'')); ?>
            <?php echo $this->Form->hidden($prefix . 'designation_name_eng', array('type' => 'text', 'value' => isset($response_params[$prefix . 'officer_designation_label_auto'])?$response_params[$prefix . 'officer_designation_label_auto']:'')); ?>
            <?php echo $this->Form->hidden($prefix . 'designation_label_eng', array('type' => 'text', 'value' => isset($response_params[$prefix . 'officer_designation_label_auto'])?$response_params[$prefix . 'officer_designation_label_auto']:'')); ?>
            <?php echo $this->Form->hidden($prefix . 'address_auto', array('type' => 'text', 'value' => isset($response_params[$prefix . 'address_auto'])?$response_params[$prefix . 'address_auto']:'')); ?>
            <?php echo $this->Form->hidden($prefix . 'office_name_auto', array('type' => 'text', 'value' => isset($response_params[$prefix . 'office_name_auto'])?$response_params[$prefix . 'office_name_auto']:'' )); ?>
            <?php echo $this->Form->hidden($prefix . 'office_name_eng', array('type' => 'text', 'value' => isset($response_params[$prefix . 'office_name_auto'])?$response_params[$prefix . 'office_name_auto']:'' )); ?>
            <?php echo $this->Form->hidden($prefix . 'office_id_auto', array('type' => 'text', 'value' => isset($response_params[$prefix . 'office_id_auto'])?$response_params[$prefix . 'office_id_auto']:'')); ?>
            <?php echo $this->Form->hidden($prefix . 'office_unit_name_auto', array('type' => 'text', 'value' => isset($response_params[$prefix . 'office_unit_name_auto'])?$response_params[$prefix . 'office_unit_name_auto']:'' )); ?>
            <?php echo $this->Form->hidden($prefix . 'unit_name_eng', array('type' => 'text', 'value' => isset($response_params[$prefix . 'office_unit_name_auto'])?$response_params[$prefix . 'office_unit_name_auto']:'' )); ?>
            <?php echo $this->Form->hidden($prefix . 'name_eng', array('type' => 'text', 'value' => isset($response_params[$prefix . 'officer_name'])?$response_params[$prefix . 'officer_name']:'' )); ?>
            <?php echo $this->Form->hidden($prefix . 'office_unit_id_auto', array('type' => 'text', 'value' => isset($response_params[$prefix . 'office_unit_id_auto'])?$response_params[$prefix . 'office_unit_id_auto']:'')); ?>
               <?php echo $this->Form->hidden($prefix . 'incharge_label_eng', array('type' => 'text', 'value' => isset($response_params[$prefix . 'incharge_label'])?$response_params[$prefix . 'incharge_label']:'' )); ?>
               <?php echo $this->Form->hidden($prefix . 'incharge_label', array('type' => 'text', 'value' => isset($response_params[$prefix . 'incharge_label'])?$response_params[$prefix . 'incharge_label']:'' )); ?>
               <?php echo $this->Form->hidden($prefix . 'personal_mobile', array('type' => 'text', 'value' => isset($response_params[$prefix . 'personal_mobile'])?$response_params[$prefix . 'personal_mobile']:'' )); ?>
        </div>
    </div>

<script src="<?php echo CDN_PATH; ?>assets/admin/layout4/scripts/projapoti_autocomplete.js" type="text/javascript"></script>
<script type="text/javascript">
    var DakOffice = {
        prefix: '<?php echo $prefix; ?>',
        setAutoDataIntoHiddenFields: function (data, input_id) {
            DakOffice.getPrefix(input_id);
            data.office_name_eng = !isEmpty(data.office_name_eng)?data.office_name_eng:'';
            data.designation_name_eng = !isEmpty(data.designation_name_eng)?data.designation_name_eng:'';
            data.office_unit_name_eng = !isEmpty(data.office_unit_name_eng)?data.office_unit_name_eng:'';
            data.officer_name_eng = !isEmpty(data.officer_name_eng)?data.officer_name_eng:'';
            data.incharge_label = !isEmpty(data.incharge_label)?data.incharge_label:'';
            $("#" + DakOffice.prefix + "office_autocomplete_div > input[name=" + DakOffice.prefix + "office_id_auto]").val(data.office_id);
            $("#" + DakOffice.prefix + "office_autocomplete_div > input[name=" + DakOffice.prefix + "address_auto]").val(data.address);
            $("#" + DakOffice.prefix + "office_autocomplete_div > input[name=" + DakOffice.prefix + "office_name_auto]").val(data.office_name);
            $("#" + DakOffice.prefix + "office_autocomplete_div > input[name=" + DakOffice.prefix + "office_name_eng]").val(data.office_name_eng);
            $("#" + DakOffice.prefix + "office_autocomplete_div > input[name=" + DakOffice.prefix + "officer_designation_id_auto]").val(data.id);
            $("#" + DakOffice.prefix + "office_autocomplete_div > input[name=" + DakOffice.prefix + "officer_designation_label_auto]").val(data.designation_label);
            $("#" + DakOffice.prefix + "office_autocomplete_div > input[name=" + DakOffice.prefix + "designation_label_eng]").val(data.designation_name_eng);
            $("#" + DakOffice.prefix + "office_autocomplete_div > input[name=" + DakOffice.prefix + "designation_name_eng]").val(data.designation_name_eng);
            $("#" + DakOffice.prefix + "office_autocomplete_div > input[name=" + DakOffice.prefix + "office_unit_name_auto]").val(data.office_unit_name);
            $("#" + DakOffice.prefix + "office_autocomplete_div > input[name=" + DakOffice.prefix + "unit_name_eng]").val(data.office_unit_name_eng);
            $("#" + DakOffice.prefix + "office_autocomplete_div > input[name=" + DakOffice.prefix + "name_eng]").val(data.officer_name_eng);
            $("#" + DakOffice.prefix + "office_autocomplete_div > input[name=" + DakOffice.prefix + "incharge_label_eng]").val(data.incharge_label);
            $("#" + DakOffice.prefix + "office_autocomplete_div > input[name=" + DakOffice.prefix + "incharge_label]").val(data.incharge_label);
            $("#" + DakOffice.prefix + "office_autocomplete_div > input[name=" + DakOffice.prefix + "office_unit_id_auto]").val(data.office_unit_id);
            $("#" + DakOffice.prefix + "office_autocomplete_div > input[name=" + DakOffice.prefix + "personal_mobile]").val(data.personal_mobile);
            
            $("input[name=" + DakOffice.prefix + "officer_name]").val(data.officer_name).trigger('change');
            $("input[name=" + DakOffice.prefix + "officer_id]").val(data.officer_id);
            $("input[name=" + DakOffice.prefix + "office_head]").val(data.office_head);

           // DakOffice.setOfficerInfo();
        },
        setOfficerInfo: function () {
            PROJAPOTI.ajaxSubmitDataCallback('<?php echo $this->request->webroot ?>' + 'employeeRecords/getOfficerInfo',
                    {'designation_id': $("#" + DakOffice.prefix + "office_autocomplete_div > input[name=" + DakOffice.prefix + "officer_designation_id_auto]").val()}, 'json',
                    function (response) {
                        $("input[name=" + DakOffice.prefix + "officer_name]").val(response.name_bng);
                        $("input[name=" + DakOffice.prefix + "officer_id]").val(response.id);
                    });
        },
        getPrefix: function (input_value) {
            var arr = input_value.split("_");
            DakOffice.prefix = arr[0] + "_";
        }
    }
</script>
