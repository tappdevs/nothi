<style>
    .saveButton{
        position: absolute;
        left:20px;
    }
    .tabbable-custom{
        margin-bottom: 0px;
        overflow-y: visible;
    }
</style>
<?php
$office_id = !empty($office_id)?$office_id:0;
?>
<div class="tabbable-custom  nav-justified">
    <ul class="nav  nav-tabs nav-justified">
        <li class="tabsearch <?= ($prefix != 'sender_' && $prefix != 'approval_' && $prefix != 'attension_')?'active':'' ?>">
            <a href="#<?php echo $prefix ?>tab_autosearch" data-toggle="tab"> অফিসার খুঁজুন </a>
        </li>
        <li class="tabsearch <?= ($prefix == 'sender_' || $prefix == 'approval_' || $prefix == 'attension_')?'active':'' ?>">
            <a href="#<?php echo $prefix ?>tab_dropdown" data-toggle="tab"> অফিসার বাছাই করুন</a>
        </li>
        <li class="tabsearch <?= ($prefix == 'sender_' || $prefix == 'approval_' || $prefix == 'attension_')?'active':'' ?>">
            <a href="#<?php echo $prefix ?>tab_dropdown" data-toggle="tab"> শাখা নির্বাচন করুন</a>
        </li>
        <?php
        if ($prefix != 'sender_' && $prefix != 'approval_' && $prefix != 'sovapoti_'  && $prefix != 'attension_') {
            ?>
            <li class="tabsearch">
                <a href="#<?php echo $prefix ?>tab_groupuser" data-toggle="tab"> অফিসার গ্রুপ</a>
            </li>
             <li class="tabsearch">
                <a href="#<?php echo $prefix ?>tab_newoffice" data-toggle="tab"> অন্যান্য 000
                </a>
            </li>

        <?php } ?>
    </ul>
    <div class="tab-content"  style="min-height:320px; padding-bottom: 50px;">
        <div class="tab-pane <?= ($prefix != 'sender_' && $prefix != 'approval_'  && $prefix != 'attension_')?'active':'' ?>" id="<?php echo $prefix ?>tab_autosearch">
            <?php echo $this->cell('OfficeSearch::officeSearch',
                ['prefix' => $prefix,[],$office_id]) ?>
            <div class="btn-group  saveButton"><a  class="btn green savethis margin-right-10" prefix="<?php echo $prefix ?>" refid="tab_autosearch"><i class="a2i_gn_approval1"></i></a><a  class="btn red closethis" prefix="<?php echo $prefix ?>" refid="tab_autosearch"><i class="a2i_gn_close2"></i></a></div>
        </div>
        <div class="tab-pane <?= ($prefix == 'sender_' || $prefix == 'approval_'  || $prefix == 'attension_')?'active':'' ?>" id="<?php echo $prefix ?>tab_dropdown">
            <?php echo $this->cell('OfficeSearch::officeChoose',
            ['prefix' => $prefix,$office_id,$is_api]) ?>
            <div class="btn-group saveButton"><a class="btn  green savethis margin-right-10" prefix="<?php echo $prefix ?>" refid="tab_dropdown"><i class="a2i_gn_approval1"></i></a><a  class="btn red closethis" prefix="<?php echo $prefix ?>" refid="tab_autosearch"><i class="a2i_gn_close2"></i></a></div>
        </div>
            <?php if ($prefix != 'sender_' && $prefix != 'approval_' && $prefix
                != 'sovapoti_'  && $prefix != 'attension_') { ?>
         <div class="tab-pane" id="<?php echo $prefix ?>tab_groupuser">
            <?php echo $this->cell('OfficeSearch::groupUser',
                ['prefix' => $prefix,[],$office_id]) ?>
                <div class="btn-group  saveButton"><a  class="btn  green savethis margin-right-10" prefix="<?php echo $prefix ?>" refid="tab_groupuser"><i class="a2i_gn_approval1"></i></a><a class="btn red closethis" prefix="<?php echo $prefix ?>" refid="tab_autosearch"><i class="a2i_gn_close2"></i></a></div>
            </div>
            <div class="tab-pane" id="<?php echo $prefix ?>tab_newoffice">
    <?php echo $this->cell('OfficeSearch::officeNew',
        ['prefix' => $prefix]) ?>
                <div class="btn-group  saveButton"><a  class="btn  green savethis margin-right-10" prefix="<?php echo $prefix ?>" refid="tab_newoffice"><i class="a2i_gn_approval1"></i></a><a  class="btn red closethis" prefix="<?php echo $prefix ?>" refid="tab_autosearch"><i class="a2i_gn_close2"></i></a></div>
            </div>
    <?php }
?>
    </div>
</div>

<script>

    $('.tabsearch a').on('click', function (e) {
        $('a[href="' + $(this).attr('href') + '"]').tab('show');
        return false;
    });
//    $('.potrojariOptions').off('click').on('click', function (e) {
//        var target = $(this).attr('data-target');
//        if($('.tabbable-custom').closest('.collapse').not('[data-target="'+target+'"]').hasClass('in')){
//            $('.tabbable-custom').closest('.collapse').removeClass('in');
//        }
//    });
    $('.closethis').on('click', function (e) {
       $(this).closest('.collapse').removeClass('in');
        return false;
    });
</script>