<div class="row">
    <?php if(!empty($options) && (isset($options['ministry']) && $options['ministry'] == 0)){
    }
    else{ ?>
    <div class="col-md-4 form-group form-horizontal">
        <label class="control-label">মন্ত্রণালয়</label>
        <?php echo $this->Form->input($prefix . 'office_ministry_id_shakha', array('empty' => '--বাছাই করুন--', 'options' => $officeMinistries, 'type' => 'select', 'label' => false, 'class' => 'form-control', 'placeholder' => 'মন্ত্রণালয়')); ?>
    </div>
    <?php } ?>
    <?php if(!empty($options) && (isset($options['division']) && $options['division'] == 0)){
    }
    else{ ?>
    <div class="col-md-4 form-group form-horizontal">
        <label class="control-label">মন্ত্রণালয়/বিভাগ</label>
        <?php echo $this->Form->input($prefix . 'office_layer_id_shakha', array('empty' => '--বাছাই করুন--', 'type' => 'select', 'label' => false, 'class' => 'form-control', 'placeholder' => 'দপ্তরের স্তর  ')); ?>
    </div>
    <?php } ?>
    <?php if(!empty($options) && (isset($options['origin']) && $options['origin'] == 0)){
    }
    else{ ?>
    <div class="col-md-4 form-group form-horizontal">
        <label class="control-label">দপ্তর / অধিদপ্তরের ধরন
        </label>
        <?php echo $this->Form->input($prefix . 'office_origin_id_shakha', array('empty' => '--বাছাই করুন--', 'type' => 'select', 'label' => false, 'class' => 'form-control', 'placeholder' => 'দপ্তর / অধিদপ্তর ')); ?>
    </div>
    <?php } ?>

   

</div>