<div class="row">
    <div class="col-md-4 form-group form-horizontal">
        <label class="control-label">মন্ত্রণালয়</label>
        <?php echo $this->Form->input($prefix . 'office_ministry_id', array('empty' => '--বাছাই করুন--', 'options' => $officeMinistries, 'type' => 'select', 'label' => false, 'class' => 'form-control', 'placeholder' => 'মন্ত্রণালয়')); ?>
    </div>
    <div class="col-md-4 form-group form-horizontal">
        <label class="control-label">মন্ত্রণালয়/বিভাগ</label>
        <?php echo $this->Form->input($prefix . 'office_layer_id', array('empty' => '--বাছাই করুন--', 'type' => 'select', 'label' => false, 'class' => 'form-control', 'placeholder' => 'দপ্তরের স্তর  ')); ?>
    </div>
    <div class="col-md-4 form-group form-horizontal">
        <label class="control-label">দপ্তর / অধিদপ্তরের ধরন </label>
        <?php echo $this->Form->input($prefix . 'office_origin_id', array('empty' => '--বাছাই করুন--', 'type' => 'select', 'label' => false, 'class' => 'form-control', 'placeholder' => 'দপ্তর / অধিদপ্তর ')); ?>
    </div>
</div>