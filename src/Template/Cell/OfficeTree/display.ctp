<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-check-square-o"></i>Office Unit Organogram Management
        </div>
        <div class="tools">
            <a href="javascript:" class="collapse"></a> <a href="#portlet-config"
                                                           data-toggle="modal" class="config"></a> <a href="javascript:"
                                                                                                      class="reload"></a>
            <a href="javascript:" class="remove"></a>
        </div>
    </div>
    <div class="portlet-window-body">
        <div class="row">
            <div class="form-group">
                <label class="col-sm-2 control-label">Office</label>

                <div class="col-sm-6">
                    <?php echo $this->Form->input($prefix . 'office_id',
                        array('empty' => '--বাছাই করুন--', 'options' => $offices, 'type' => 'select', 'label' => false, 'class' => 'form-control', 'placeholder' => 'অফিস')); ?>
                </div>
            </div>
        </div>
        <hr/>
        <div class="row">
            <div class="portlet light col-md-5">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-check-square-o"></i>Origin Unit Organogram
                    </div>
                </div>
                <div class="portlet-window-body">
                    <div class="" id="origin_unit_tree_panel">
                        <!--  -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    var OfficeUnitManagement = {
        checked_node_ids: [],
        getUnitOrganogramByOfficeId: function (office_id) {

            PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + 'officeManagement/getOriginUnitOrgByOfficeId',
                {'office_id': office_id}, 'json',
                function (response) {
                    if (response.length > 0) {
                        var data_str = "";
                        $.each(response[0], function (i) {
                            var node = response[0][i];
                            data_str += '{ "id" : "' + node.id + '", "parent" : "' + node.parent + '", "text" : "' + node.text + '", "icon" : "' + node.icon + '" },';
                        });
                        data_str= data_str.replace(/\\n/g, "\\n")
                            .replace(/\\'/g, "\\'")
                            .replace(/\\"/g, '\\"')
                            .replace(/\\&/g, "\\&")
                            .replace(/\\r/g, "\\r")
                            .replace(/\\t/g, "\\t")
                            .replace(/\\b/g, "\\b")
                            .replace(/\\f/g, "\\f");
             // remove non-printable and other non-valid JSON chars
                        data_str = data_str.replace(/[\u0000-\u0019]+/g,"");
                        var edited_data_str = "[" + data_str.replace(/^,|,$/g, '') + "]";

                        $('#origin_unit_tree_panel').jstree(true).settings.core.data = JSON.parse(edited_data_str);
                        $('#origin_unit_tree_panel').jstree(true).refresh();
                        $('#origin_unit_tree_panel').bind("refresh.jstree", function (event, data) {
                            $('#origin_unit_tree_panel').jstree("open_all");
                        });
                    }
                    else {
                        $('#origin_unit_tree_panel').jstree(true).settings.core.data = '';
                        $('#origin_unit_tree_panel').jstree(true).refresh();
                        $('#origin_unit_tree_panel').bind("refresh.jstree", function (event, data) {
                            $('#origin_unit_tree_panel').jstree("open_all");
                        });
                    }

                    OfficeUnitManagement.reloadOfficeUnitOrg(office_id);

                }
            );
        },

        reloadOfficeUnitOrg: function (office_id) {
            PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + 'officeManagement/getOfficeDesignationsByOfficeId',
                {'office_id': office_id}, 'json',
                function (response) {
                    if (response.length > 0) {
                        var data_str = "";
                        $.each(response[0], function (i) {
                            var node = response[0][i];
                            data_str += '{ "id" : "' + node.id + '", "parent" : "' + node.parent + '", "text" : "' + node.text + '", "icon" : "' + node.icon + '" },';
                        });
                        data_str= data_str.replace(/\\n/g, "\\n")
                            .replace(/\\'/g, "\\'")
                            .replace(/\\"/g, '\\"')
                            .replace(/\\&/g, "\\&")
                            .replace(/\\r/g, "\\r")
                            .replace(/\\t/g, "\\t")
                            .replace(/\\b/g, "\\b")
                            .replace(/\\f/g, "\\f");
             // remove non-printable and other non-valid JSON chars
                        data_str = data_str.replace(/[\u0000-\u0019]+/g,"");
                        var edited_data_str = "[" + data_str.replace(/^,|,$/g, '') + "]";

                        $('#office_unit_tree_panel').jstree(true).settings.core.data = JSON.parse(edited_data_str);
                        $('#office_unit_tree_panel').jstree(true).refresh();
                        $('#office_unit_tree_panel').bind("refresh.jstree", function (event, data) {
                            $('#office_unit_tree_panel').jstree("open_all");
                        });

                    }
                    else {
                        $('#office_unit_tree_panel').jstree(true).settings.core.data = '';
                        $('#office_unit_tree_panel').jstree(true).refresh();
                        $('#office_unit_tree_panel').bind("refresh.jstree", function (event, data) {
                            $('#office_unit_tree_panel').jstree("open_all");
                        });
                    }

                }
            );
        },


        selected_organogram: [],

        selectOrganogram: function (input) {
            var organogram_id = $(input).attr('id');
            var unit_id = $(input).data('unit-id');
            var org_unit = "unit_" + unit_id + ":" + "org_" + organogram_id;

            var index = OfficeUnitManagement.selected_organogram.indexOf(org_unit);
            if (index < 0) {
                OfficeUnitManagement.selected_organogram.push(org_unit);
            }
            else {
                OfficeUnitManagement.selected_organogram.splice(index, 1);
            }
        },

        loadOfficeOriginUnitTree: function () {

            $('#origin_unit_tree_panel').jstree(
                {
                    'core': {
                        "themes": {
                            "variant": "large",
                            "multiple": false
                        },
                        'data': ''
                    },

                    "checkbox": {
                        "keep_selected_style": false
                    },
                    "plugins": ["checkbox"]
                }
            ).bind("loaded.jstree", function (event, data) {
                    $(this).jstree("open_all");
                });
        }
    };

    $(function () {
        $("#office-id").bind('change', function () {
            OfficeUnitManagement.getUnitOrganogramByOfficeId($(this).val());
        });

        $(document).on('click', '#origin_unit_tree_panel .jstree-anchor', function () {
            OfficeUnitManagement.selectOrganogram($('#origin_unit_tree_panel .jstree-anchor').find('.org_checkbox'));
        });

        OfficeUnitManagement.loadOfficeOriginUnitTree();

    });
</script>