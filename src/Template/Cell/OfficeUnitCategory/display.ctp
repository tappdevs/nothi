<div class="form-group">
    <label class="col-sm-5 control-label">Office Unit Category</label>

    <div class="col-sm-7">
        <?php
        echo $this->Form->input('office_unit_category', array(
            'list' => 'office_unit_category_list',
            'label' => false,
            'type' => 'text',
            'class' => 'form-control  input-sm',
            'value' => $value
        ));
        ?>
        <datalist id="office_unit_category_list">
            <?php
            foreach ($officeUnitCategories as $category) {
                echo '<option value="' . $category . '">' . $category . '</option>';
            }
            ?>
        </datalist>
    </div>
</div>
