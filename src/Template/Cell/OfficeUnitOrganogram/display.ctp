<?php
$session = $this->request->session();
?>
<!--  -->
<div class="row">
    <div class="col-md-12">
        <?= $officeSelectionCell = $this->cell('OfficeSelection', ['entity' => '', 'prefix' => $prefix,'options'=>$options]) ?>
        <div class="row">
            <?php if (!empty($options) && (isset($options['office']) && $options['office'] == 0)) {
            } else { ?>
                <div class="col-md-4 form-group form-horizontal">
                    <label class="control-label"> কার্যালয় </label>
                    <?php echo $this->Form->input($prefix . 'office_id', array('empty' => '--বাছাই করুন--', 'type' => 'select', 'label' => false, 'class' => 'form-control')); ?>
                </div>
            <?php }
            if (!empty($options) && (isset($options['unit']) && $options['unit'] == 0)) {
            } else { ?>
                <div class="col-md-4 form-group form-horizontal">
                    <label class="control-label"> দপ্তর/শাখা </label>
                    <?php echo $this->Form->input($prefix . 'office_unit_id', array('empty' => '--বাছাই করুন--', 'type' => 'select', 'label' => false, 'class' => 'form-control')); ?>
                </div>
            <?php }
            if (!empty($options) && (isset($options['organogram']) && $options['organogram'] == 0)) {
            } else { ?>
                <div class="col-md-4 form-group form-horizontal">
                    <label class="control-label"> পদ </label>
                    <?php echo $this->Form->input($prefix . 'office_unit_organogram_id', array('empty' => '--বাছাই করুন--', 'type' => 'select', 'label' => false, 'class' => 'form-control')); ?>
                    <?php echo $this->Form->hidden($prefix . 'office_unit_organogram', array('id' => $prefix . 'office_unit_organogram')); ?>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<!--  -->

<script>

	(function ($) {
		var prefix = '<?php echo $prefix;?>';
		var prefixmain = '<?php echo $prefix;?>';

		prefix = prefix.split("_");
		prefix = prefix[0] + '-';

		$("#" + prefix + "office-ministry-id").bind('change', function () {
			OfficeOrgSelectionCell.loadMinistryWiseLayers(prefix);
		});
		$("#" + prefix + "office-layer-id").bind('change', function () {
			OfficeOrgSelectionCell.loadMinistryAndLayerWiseOfficeOrigin(prefix);
		});
		$("#" + prefix + "office-origin-id").bind('change', function () {
			OfficeOrgSelectionCell.loadOriginOffices(prefix);
		});
		$("#" + prefix + "office-id").bind('change', function () {
			OfficeOrgSelectionCell.loadOfficeUnits(prefix);
		});
		$("#" + prefix + "office-unit-id").bind('change', function () {
			OfficeOrgSelectionCell.loadOfficeUnitDesignations(prefix);
		});
		$("#" + prefix + "office-unit-organogram-id").bind('change', function () {
			OfficeOrgSelectionCell.getOfficerInfo($(this).val(), prefix, prefixmain);
		});

	}(jQuery));

</script>
<script type="text/javascript">
	var OfficeOrgSelectionCell = {

		loadMinistryWiseLayers: function (prefix) {
			PROJAPOTI.ajaxSubmitDataCallback('<?php echo $this->request->webroot ?>' + 'officeSettings/loadLayersByMinistry',
				{'office_ministry_id': $("#" + prefix + "office-ministry-id").val()}, 'json',
				function (response) {
					PROJAPOTI.projapoti_dropdown_map("#" + prefix + "office-layer-id", response, "--বাছাই করুন--");
				});
		},
		loadMinistryAndLayerWiseOfficeOrigin: function (prefix) {
			PROJAPOTI.ajaxSubmitDataCallback('<?php echo $this->request->webroot ?>' + 'officeSettings/loadOfficesByMinistryAndLayer',
				{
					'office_ministry_id': $("#" + prefix + "office-ministry-id").val(),
					'office_layer_id': $("#" + prefix + "office-layer-id").val()
				}, 'json',
				function (response) {
					PROJAPOTI.projapoti_dropdown_map("#" + prefix + "office-origin-id", response, "--বাছাই করুন--");
				});
		},
		loadOriginOffices: function (prefix) {
			PROJAPOTI.ajaxSubmitDataCallback('<?php echo $this->request->webroot ?>' + 'officeManagement/loadOriginOffices',
				{'office_origin_id': $("#" + prefix + "office-origin-id").val()}, 'json',
				function (response) {
					PROJAPOTI.projapoti_dropdown_map("#" + prefix + "office-id", response, "--বাছাই করুন--");
				});
		},
		loadOfficeUnits: function (prefix) {
			PROJAPOTI.ajaxSubmitDataCallback('<?php echo $this->request->webroot ?>' + 'officeManagement/loadOfficeUnits',
				{'office_id': $("#" + prefix + "office-id").val()}, 'html',
				function (response) {
					$("#" + prefix + "office-unit-id").html(response);
				});
		},
		loadOfficeUnitDesignations: function (prefix) {
			PROJAPOTI.ajaxSubmitDataCallback('<?php echo $this->request->webroot ?>' + 'officeManagement/loadOfficeUnitOrganogramsWithName',
				{'office_unit_id': $("#" + prefix + "office-unit-id").val()}, 'json',
				function (response) {
//                    PROJAPOTI.projapoti_dropdown_map("#" + prefix + "office-unit-organogram-id", response, "--বাছাই করুন--");
//                    $("#" + prefix + "office-unit-organogram-id").select2();
					PROJAPOTI.projapoti_dropdown_map_with_title("#" + prefix + "office-unit-organogram-id", response, "--বাছাই করুন--", '', {
						'key': 'id',
						'value': 'designation',
						'title': 'name'
					});
				});

		},
		getOfficerInfo: function (designation_id, prefix, prefixmain) {
			PROJAPOTI.ajaxSubmitDataCallback('<?php echo $this->request->webroot ?>' + 'employeeRecords/getOfficerInfo',
				{'designation_id': designation_id}, 'json',
				function (response) {

					$('input[name=' + prefixmain + 'officer_designation_id_auto]').val('');
					$('input[name=' + prefixmain + 'officer_designation_label_auto]').val('');
					$('input[name=' + prefixmain + 'address_auto]').val('');
					$('input[name=' + prefixmain + 'office_name_auto]').val('');
					$('input[name=' + prefixmain + 'office_id_auto]').val('');
					$('input[name=' + prefixmain + 'office]').val('');
					$("input[name=" + prefixmain + "officer_id]").val(response.id);
					$("input[name=" + prefix + "officer_id]").val(response.id);
					$("input[name=" + prefix + "officer-id]").val(response.id);
					$("#" + prefix + "officer-name").val(response.name_bng).trigger('change');
					$("#" + prefixmain + "office_unit_organogram").val($('#sender-office-unit-organogram-id option:selected').text());
				}
			);
		}
	};
</script>


