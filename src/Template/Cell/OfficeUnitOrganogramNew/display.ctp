<?php
$session = $this->request->session();
?>
<!--  -->
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-2 form-group form-horizontal">
                <label class="control-label">মন্ত্রণালয়</label>
                <?php echo $this->Form->input($prefix . 'office_ministry_id', array('empty' => '--বাছাই করুন--', 'options' => $officeMinistries, 'type' => 'select', 'label' => false, 'class' => 'form-control', 'placeholder' => 'মন্ত্রণালয়')); ?>
            </div>

            <div class="col-md-2 form-group form-horizontal">
                <label class="control-label">মন্ত্রণালয়/বিভাগ</label>
                <?php echo $this->Form->input($prefix . 'office_layer_id', array('empty' => '--বাছাই করুন--', 'type' => 'select', 'label' => false, 'class' => 'form-control', 'placeholder' => 'দপ্তরের স্তর  ')); ?>
            </div>

            <div class="col-md-3 form-group form-horizontal">
                <label class="control-label"> কার্যালয় </label>
                <?php echo $this->Form->input($prefix . 'office_id', array('empty' => '--বাছাই করুন--', 'type' => 'select', 'label' => false, 'class' => 'form-control')); ?>
            </div>

            <div class="col-md-3 form-group form-horizontal">
                <label class="control-label"> দপ্তর/শাখা </label>
                <?php echo $this->Form->input($prefix . 'office_unit_id', array('empty' => '--বাছাই করুন--', 'type' => 'select', 'label' => false, 'class' => 'form-control')); ?>
            </div>

            <?php if(isset($options['organogram']) && $options['organogram'] != 0): ?>
            <div class="col-md-3 form-group form-horizontal">
                <label class="control-label"> পদ </label>
                <?php echo $this->Form->input($prefix . 'office_unit_organogram_id', array('empty' => '--বাছাই করুন--', 'type' => 'select', 'label' => false, 'class' => 'form-control')); ?>
                <?php echo $this->Form->hidden($prefix . 'office_unit_organogram', array('id' => $prefix . 'office_unit_organogram')); ?>
            </div>
            <?php endif; ?>
        </div>
    </div>
</div>
<!--  -->

<script>

    (function ($) {
        var prefix = '<?php echo $prefix;?>';
        var prefixmain = '<?php echo $prefix;?>';

        prefix = prefix.split("_");
        prefix = prefix[0] + '-';

        $("#" + prefix + "office-ministry-id").bind('change', function () {
            OfficeOrgSelectionCell.loadMinistryWiseLayers(prefix);
        });
        $("#" + prefix + "office-layer-id").bind('change', function () {
            OfficeOrgSelectionCell.loadMinistryAndLayerWiseOfficeOrigin(prefix);
        });
        $("#" + prefix + "office-origin-id").bind('change', function () {
            OfficeOrgSelectionCell.loadOriginOffices(prefix);
        });
        $("#" + prefix + "office-id").bind('change', function () {
            OfficeOrgSelectionCell.loadOfficeUnits(prefix);
        });
        $("#" + prefix + "office-unit-id").bind('change', function () {
            OfficeOrgSelectionCell.loadOfficeUnitDesignations(prefix);
        });
        $("#" + prefix + "office-unit-organogram-id").bind('change', function () {
            OfficeOrgSelectionCell.getOfficerInfo($(this).val(), prefix, prefixmain);
        });

    }(jQuery));

</script>
<script type="text/javascript">
    var OfficeOrgSelectionCell = {

        loadMinistryWiseLayers: function (prefix) {
            $("#" + prefix + "office-layer-id").find('option').remove().end().append('<option value=""> লোডিং... </option>').val('');
            $("#" + prefix + "office-layer-id").select2();
            PROJAPOTI.ajaxSubmitDataCallback('<?php echo $this->request->webroot ?>' + 'officeSettings/loadLayersByMinistry',
                {'office_ministry_id': $("#" + prefix + "office-ministry-id").val()}, 'json',
                function (response) {
                    PROJAPOTI.projapoti_dropdown_map("#" + prefix + "office-layer-id", response, "--বাছাই করুন--");
                    $("#" + prefix + "office-layer-id").select2();
                });
        },
        loadMinistryAndLayerWiseOfficeOrigin: function (prefix) {
            $("#" + prefix + "office-id").find('option').remove().end().append('<option value=""> লোডিং... </option>').val('');
            $("#" + prefix + "office-id").select2();
            PROJAPOTI.ajaxSubmitDataCallback('<?php echo $this->request->webroot ?>' + 'officeSettings/loadAllOfficesByMinistryAndLayer',
                {
                    'office_ministry_id': $("#" + prefix + "office-ministry-id").val(),
                    'office_layer_id': $("#" + prefix + "office-layer-id").val(),
                    'for_dropdown': 1
                }, 'json',
                function (response) {
                    PROJAPOTI.projapoti_dropdown_map("#" + prefix + "office-id", response, "--বাছাই করুন--");
                    $("#" + prefix + "office-id").select2();
                });
        },
        loadOriginOffices: function (prefix) {
            PROJAPOTI.ajaxSubmitDataCallback('<?php echo $this->request->webroot ?>' + 'officeManagement/loadOriginOffices',
                {'office_origin_id': $("#" + prefix + "office-origin-id").val()}, 'json',
                function (response) {
                    PROJAPOTI.projapoti_dropdown_map("#" + prefix + "office-id", response, "--বাছাই করুন--");
                });
        },
        loadOfficeUnits: function (prefix) {
            $("#" + prefix + "office-unit-id").find('option').remove().end().append('<option value=""> লোডিং... </option>').val('');
            $("#" + prefix + "office-unit-id").select2();
            PROJAPOTI.ajaxSubmitDataCallback('<?php echo $this->request->webroot ?>' + 'officeManagement/loadOfficeUnits',
                {'office_id': $("#" + prefix + "office-id").val()}, 'html',
                function (response) {
                    $("#" + prefix + "office-unit-id").html(response);
                    $("#" + prefix + "office-unit-id").select2();
                });
        },
        loadOfficeUnitDesignations: function (prefix) {
            $("#" + prefix + "office-unit-organogram-id").find('option').remove().end().append('<option value=""> লোডিং... </option>').val('');
            $("#" + prefix + "office-unit-organogram-id").select2();
            PROJAPOTI.ajaxSubmitDataCallback('<?php echo $this->request->webroot ?>' + 'officeManagement/loadOfficeUnitOrganogramsWithName',
                {'office_unit_id': $("#" + prefix + "office-unit-id").val()}, 'json',
                function (response) {
                    PROJAPOTI.projapoti_dropdown_map_with_title("#" + prefix + "office-unit-organogram-id", response, "--বাছাই করুন--", '', {'key': 'id', 'value': 'designation', 'title': 'name'});
                    callback(response);
                });

        },
        getOfficerInfo: function (designation_id, prefix, prefixmain) {
            PROJAPOTI.ajaxSubmitDataCallback('<?php echo $this->request->webroot ?>' + 'employeeRecords/getOfficerInfo',
                {'designation_id': designation_id}, 'json',
                function (response) {

                    $('input[name=' + prefixmain + 'officer_designation_id_auto]').val('');
                    $('input[name=' + prefixmain + 'officer_designation_label_auto]').val('');
                    $('input[name=' + prefixmain + 'address_auto]').val('');
                    $('input[name=' + prefixmain + 'office_name_auto]').val('');
                    $('input[name=' + prefixmain + 'office_id_auto]').val('');
                    $('input[name=' + prefixmain + 'office]').val('');
                    $("input[name=" + prefixmain + "officer_id]").val(response.id);
                    $("input[name=" + prefix + "officer_id]").val(response.id);
                    $("input[name=" + prefix + "officer-id]").val(response.id);
                    $("#" + prefix + "officer-name").val(response.name_bng).trigger('change');
                    $("#" + prefixmain + "office_unit_organogram").val($('#sender-office-unit-organogram-id option:selected').text());
                    $("#" + prefixmain + "autocomplete_office_id").val(response.name_bng);
                    callback(response);
                }
            );
        }
    };
</script>


