<?php if(empty($potrojari)): ?>
<div class="row  form-group">
    <div class="col-lg-offset-2 col-md-offset-2  col-lg-4 col-md-3 col-sm-3 col-xs-6">
        পত্রের ধরন
    </div>
    <div class="col-lg-4 col-md-5 col-sm-9 col-xs-6">
        <?php
        echo $this->Form->input('potro_type', ['class' => 'form-control select', 'type' => 'select', 'empty' => 'পত্রের ধরন বাছাই করুন', 'options' => $template, 'label' => false, 'default' => 0])
        ?>
    </div>
</div>
<?php  endif; ?>
<?php echo $this->Form->hidden('uploaded_attachments', ['id' => 'uploaded_attachments']) ?>
<?php echo $this->Form->hidden('file_description', ['id' => 'file_description']) ?>

<div class="row form-group">
    <div class="col-lg-12">

        <div class="tabbable-custom nav-justified">
            <ul class="nav nav-tabs nav-justified">
                <li class="active">
                    <a href="#tab_1_1_1" data-toggle="tab">
                        কভার পাতা</a>
                </li>
                <li>
                    <a href="#tab_1_1_2" data-toggle="tab">
                        সার-সংক্ষেপ </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_1_1_1">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="template-body" style="max-width: 950px;min-height: 815px;    height: auto;    margin: 20px auto;    padding-bottom: 50px;    display: block;    background-color: rgb(255, 255, 255);    border: 2px solid #E6E6E6; <?php echo !empty($potrojari) ? "" : "display:none;" ?>">
                                <?php echo !empty($potrojari)?$potrojari->potro_cover:'' ?>
                            </div>
                        </div>
                        <textarea id="contentbody" name="contentbody" style="display: none"></textarea>
                    </div>
                </div>
                <div class="tab-pane" id="tab_1_1_2">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="template-body2" style="max-width: 950px;min-height: 815px;    height: auto;    margin: 20px auto;    padding-bottom: 50px;    display: block;    background-color: rgb(255, 255, 255);    border: 2px solid #E6E6E6; <?php echo !empty($potrojari) ? "" : "display:none;" ?>">
                                <?php echo !empty($potrojari)?$potrojari->potro_description:'' ?>
                            </div>
                        </div>
                        <textarea id="contentbody2" name="contentbody2" style="display: none"></textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>

    $('[href=#tab_1_1_2]').click(function(){
        $('.sonjukti').removeClass('hide');
    });

    $('[href=#tab_1_1_1]').click(function(){
        $('.sonjukti').addClass('hide');
    });
</script>