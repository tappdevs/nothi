<link href="<?=$this->request->webroot?>wizard/css/material-bootstrap-wizard.css" rel="stylesheet" />
<link href="<?=$this->request->webroot?>wizard/css/wizard.css" rel="stylesheet" />

<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            অফিস পৃথকীকরণ চেকিং
        </div>
    </div>
    <div class="portlet-body">
        Unit List: <pre><?php print_r($unit_list_comma_separeted) ?></pre><br/>
        Origin Office ID: <?=$origin_office_id?><br/>
        Transferred Office ID: <?=$transferred_office_id?>
        <div class="row">
            <div class="col-md-12 table-responsive">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                    <tr>
                        <th>Table Name</th>
                        <th>Befor Transfer Count</th>
                        <th>After Transfer Count</th>
                        <th>Difference</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($origin_data as $table_name => $count): ?>
                    <?php $diff = $count - $transferred_data[$table_name]; ?>
                    <tr class="<?php if($diff > 0) { echo 'danger'; } else if($diff < 0) { echo 'warning'; } ?>">
                        <td><?=$table_name?></td>
                        <td><?=$count?></td>
                        <td><?=$transferred_data[$table_name]?></td>
                        <td><?=$diff?></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
