<style>
	.details {
		position: relative;
		background: lightgray;

		width: 100%;
		/*height: calc(100vh - 228px);*/
		height: 150px;
		margin-top: 14px;
		color: black;
		padding: 10px;
		border-radius: 5px !important;
	}

	.details:after {
		content: '';
		position: absolute;
		top: 18px;
		left: 10%;
		width: 0;
		height: 0;
		border: 13px solid transparent;
		border-bottom-color: lightgray;
		border-top: 0;
		margin-left: -31px;
		margin-top: -31px;
	}
</style>
<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            পদবির কার্যক্রম স্থানান্তর
        </div>

    </div>
    <div class="portlet-window-body">
	    <?php
        echo $this->Form->input('office_id', array(
            'label' => false,
            'class' => 'form-control',
            'type'=>'hidden',
            'default' => $employee_offices['office_id']
        ));
	    ?>
        <div class="row hidden">
	        <div class="col-md-12">
	            <div class="panel panel-primary">
		            <div class="panel-heading" style="color: #fff;">
			            <div class="row">
				            <div class="col-md-6 office_name">Office Employee List</div>
				            <div class="col-md-6 pull-right" style="display:none;">
					            <div class="input-group-round form-inline" style="text-align:right;">
						            <select name="new_organogram_id" placeholder="Hello" class="form-control text-left" style="width:82%;"></select>
					                <button class="btn btn-warning btn-md" id="btnDeleteOrganogramModal" style="margin-left:-5px;">স্থানান্তর করুন</button>
					            </div>
				            </div>
			            </div>
		            </div>
		            <div class="panel-body" style="overflow:auto; height:calc(100vh - 180px);">
			            <div class="table-responsive">
				            <table class="table table-bordered table-striped table-hover">
					            <thead>
					            <tr>
						            <th>#</th>
						            <th>কর্মকর্তার নাম</th>
						            <th>পদের নাম</th>
						            <th>শাখার নাম</th>
						            <th>কার্যক্রম</th>
					            </tr>
					            </thead>
					            <tbody></tbody>
				            </table>
			            </div>
		            </div>
	            </div>
	        </div>
        </div>
    </div>
</div>

<div class="modal fade modal-purple height-auto" id="OrganogramDeleteModal" tabindex="-1" aria-hidden="true" data-backdrop="false" data-keyboard="false">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body">
				<div class="title"></div>
				<div class="details"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary btnDeleteOrganogram">পদবির কার্যক্রম স্থানান্তর</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal"><?=__('Close') ?></button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $("#office-id").bind('change', function () {
            var office_id = $(this).val();
            if (office_id > 0) {
                $(".panel").closest('.row').removeClass('hidden');
                $(".panel").find('.panel-heading .office_name').html($("#office-id option:selected").text() + ' <i class="fa fa-cog fa-spin"></i> লোড হচ্ছে...');
                $(".panel").find('.panel-body').hide();

                PROJAPOTI.ajaxSubmitAsyncDataCallbackWithoutAbrot(js_wb_root + 'showOfficeEmployeeList', {office_id: office_id}, 'json',
                    function (response) {
                        $(".panel").find('tbody').html("");
                        $("select[name=new_organogram_id]").parent().parent().hide()
                        onChangeRadioButton();
                        if (response.length != 0) {
                            $("select[name=new_organogram_id]").html("");
                            $("select[name=new_organogram_id]").append($('<option>', {
                                value: "",
                                text: "-- বাছাই করুন --"
                            }));
                            $.each(response, function (key, value) {
                                $(".panel").find('tbody').append('<tr><td>' + BnFromEng(key + 1) + '</td><td>' + value.employee_name + '</td><td>' + value.designation_name + '</td><td>' + value.unit_name + '</td><td onclick="onChangeRadioButton()"><input type="radio" class="make-switch" name="organogramId" value="' + value.organogram_id + '" /></td></tr>');
                                $("select[name=new_organogram_id]").append($('<option>', {
                                    value: value.organogram_id,
                                    text: value.employee_name + ', ' + value.designation_name + ', ' + value.unit_name
                                }));
                            });
                            $("select[name=new_organogram_id]").select2();
                            $(document).find('.make-switch').bootstrapSwitch();

                            $(".panel").find('.panel-heading .office_name').html('<small style="font-style:italic;margin: 0 10px;color:yellow;">একবারে শুধু একটি পদের কার্যক্রম স্থানান্তর করা যাবে</small>');
                            $(".panel").find('.panel-body').show();
                            $('html, body').animate({
                                scrollTop: $("#office-id").offset().top
                            }, 2000);
                        } else {
                            $(".panel").find('.panel-body').show();
                            $(".panel").find('tbody').append('<tr><td colspan="5">কোনো তথ্য পাওয়া যায় নি</td></tr>');
                        }
                    }
                );
            } else {
                $(".panel").closest('.row').addClass('hidden');
            }
        });

        $("#btnDeleteOrganogramModal").on('click', function () {
            if ($("select[name=new_organogram_id] option:selected").val() != "" || $("select[name=new_organogram_id] option:selected").val() != 0) {
                $("#OrganogramDeleteModal").modal('show');
                $("#OrganogramDeleteModal").find('.modal-title').html('পদবির কার্যক্রম স্থানান্তর');
                $("#OrganogramDeleteModal").find('.modal-body>.title').html('<span class="badge badge-primary" style="font-size:15px!important">' + $('input[name=organogramId]:checked').closest('tr').find('td:eq(1)').html() + ', ' + $('input[name=organogramId]:checked').closest('tr').find('td:eq(2)').html() + ', ' + $('input[name=organogramId]:checked').closest('tr').find('td:eq(3)').html() + '</span><i class="fa fa-arrow-right" style="margin: 0 5px!important"></i><span class="badge badge-primary" style="font-size:15px!important">' + $("select[name=new_organogram_id] option:selected").html() + '</span>');

                $("#OrganogramDeleteModal").find('.modal-body>.details').html('<div style="font-size: 25px;font-weight: bold;border-bottom: double;">লোডিং কার্যক্রম...</div>');
                $.ajax({
                    url: js_wb_root + 'countUserActivities',
                    type: 'POST',
                    dataType: 'json',
                    data: {designation_id: $('input[name=organogramId]:checked').val()},
                    success: function (response) {
                        $("#OrganogramDeleteModal").find('.modal-body>.details').html('<div style="font-size: 25px;font-weight: bold;border-bottom: double;">পেন্ডিং কার্যক্রমসমূহ</div><div style="font-size: 25px;">ডাক: ' + En2Bn(response.dak) + '</div><div style="font-size: 25px;">নথি: ' + En2Bn(response.nothi) + '</div>');
                    }
                });
            } else {
                toastr.error('পদবি বাছাই করুন');
            }
        });

        $(".btnDeleteOrganogram").on('click', function () {
            $(this).html('<i class="fa fa-cog fa-spin"></i> পদবির কার্যক্রম স্থানান্তর করা হচ্ছে...');
            $(this).addClass('disabled');
            $.ajax({
                url: js_wb_root+'designationDataTransfer',
                type: 'GET',
                dataType: 'json',
                data: {
                    selectedDesignation : $('input[name=organogramId]:checked').val(),
                    newDesignation : $('select[name=new_organogram_id] option:selected').val(),
                    office_id : $("#office-id").val(),
                },
                success: function (response) {
                    if (response.status == 'success') {
                        $("#OrganogramDeleteModal").modal('hide');
                        $("select[name=new_organogram_id]").parent().parent().hide();

                        $(this).html('পদবির কার্যক্রম স্থানান্তর');
                        $(this).removeClass('disabled');

                        onChangeRadioButton();
                        $("#office-id").trigger('change');
                        toastr.success('পদবির কার্যক্রম স্থানান্তর করা হয়েছে');
                    } else {
                        toastr.error('কার্যক্রম স্থানান্তর সম্ভব হচ্ছে না')
                    }
                }
            });
        });

        $("#office-id").trigger('change');
    });
    function onChangeRadioButton() {
        if ($("input[name=organogramId]").is(":checked")) {
            $("select[name=new_organogram_id]").parent().parent().show();

            $('select[name=new_organogram_id] option[disabled]').attr('disabled', false);
            $('select[name=new_organogram_id] option[value='+$('input[name=organogramId]:checked').val()+']').attr('disabled', true);
        } else {
            $("select[name=new_organogram_id]").parent().parent().hide()
        }
    }
</script>