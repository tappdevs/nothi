<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            অফিস কাঠামো ট্র্যান্সফার
        </div>

    </div>
    <div class="portlet-window-body">
        <?php if(empty($employee_offices)){ ?>
            <?= $officeSelectionCell = $this->cell('OfficeSelection', ['entity' => '']) ?>
            <div class="row">
                <div class="col-md-4 form-group form-horizontal">
                    <label class="control-label"> <?php echo __("Office") ?> </label>
                    <?php
                    echo $this->Form->input('office_id', array(
                        'label' => false,
                        'class' => 'form-control',
                        'empty' => '----'
                    ));
                    ?>
                </div>
            </div>
        <?php }else{
            echo $this->Form->input('office_id', array(
                'label' => false,
                'class' => 'form-control',
                'type'=>'hidden',
                'default' => $employee_offices['office_id']
            ));
        }  ?>

        <div class="row">
            <div class="portlet light col-md-5">
                <div class="portlet-title">
                    <div class="caption">
                        <?php echo __('Office') ?> <?php echo __('Unitr') ?> <?php echo __('Organogra') ?>
                    </div>
                </div>
                <div class="portlet-window-body">
                    <div class="" id="office_unit_tree_panel">

                    </div>
                </div>
            </div>
            <div class="portlet light col-md-1 text-center">
                <div class="portlet-title">
                    <div class="caption">
                        <?php echo __('Actions') ?>
                    </div>
                </div>
                <div class="portlet-window-body">
                    <button type="button" id="officeUnitTransfer" class="btn   btn-success"><i
                                class="fa fa-hand-o-right"></i></button>
                </div>
            </div>
            <div class="portlet light col-md-5">
                <div class="portlet-title">
                    <div class="caption">
                        <?php echo __('Office') ?> <?php echo __('Unit') ?>
                    </div>
                </div>
                <div class="portlet-window-body">
                    <div id="office-unit-id">
                        <?= $this->Form->input('office_unit',['class'=>'form-control','options'=>$office_units,'empty'=>__("Select"),'label'=>false]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
	var currentDesignationId = <?= isset($_SESSION['selected_office_section']['office_unit_organogram_id'])?$_SESSION['selected_office_section']['office_unit_organogram_id']:0?>;
	var OfficeUnitManagement = {
		checked_node_ids: [],
		loadMinistryWiseLayers: function () {
			OfficeSetup.loadLayers($("#office-ministry-id").val(), function (response) {

			});
		},
		loadMinistryAndLayerWiseOfficeOrigin: function () {
			OfficeSetup.loadOffices($("#office-ministry-id").val(), $("#office-layer-id").val(), function (response) {

			});
		},
		loadOriginOffices: function () {
			OfficeSetup.loadOriginOffices($("#office-origin-id").val(), function (response) {

			});
		},
		reloadOfficeUnitOrg: function (office_id) {
            Metronic.blockUI({
                target: $('#office_unit_tree_panel'),
                boxed: true,
                message: 'অপেক্ষা করুন'
            });

			PROJAPOTI.ajaxSubmitAsyncDataCallbackWithoutAbrot(js_wb_root + 'officeManagement/getOfficeDesignationsByOfficeId',
				{need_name:1,office_id:office_id}, 'json',
				function (response) {
					if (response.length > 0) {
						var data_str = "";
						$.each(response[0], function (i) {
							var node = response[0][i];
							data_str += '{ "id" : "' + node.id + '", "parent" : "' + node.parent + '", "text" : "' + node.text + '", "icon" : "' + node.icon + '" },';
						});
						data_str= data_str.replace(/\\n/g, "\\n")
							.replace(/\\'/g, "\\'")
							.replace(/\\"/g, '\\"')
							.replace(/\\&/g, "\\&")
							.replace(/\\r/g, "\\r")
							.replace(/\\t/g, "\\t")
							.replace(/\\b/g, "\\b")
							.replace(/\\f/g, "\\f");
						// remove non-printable and other non-valid JSON chars
						data_str = data_str.replace(/[\u0000-\u0019]+/g,"");
						var edited_data_str = "[" + data_str.replace(/^,|,$/g, '') + "]";

						$('#office_unit_tree_panel').jstree(true).settings.core.data = JSON.parse(edited_data_str);
						$('#office_unit_tree_panel').jstree(true).refresh();
						$('#office_unit_tree_panel').bind("refresh.jstree", function (event, data) {
							$('#office_unit_tree_panel').jstree("open_all");
						});

					}
					else {
						$('#office_unit_tree_panel').jstree(true).settings.core.data = '';
						$('#office_unit_tree_panel').jstree(true).refresh();
						$('#office_unit_tree_panel').bind("refresh.jstree", function (event, data) {
							$('#office_unit_tree_panel').jstree("open_all");
						});
					}

                    Metronic.unblockUI($('#office_unit_tree_panel'));
				}
			);

			PROJAPOTI.ajaxSubmitAsyncDataCallbackWithoutAbrot('<?= $this->Url->build(['_name'=>'loadOfficeUnits']) ?>',{office_id:office_id},'html',function(response){
				$('#office-unit').html(response);
				$('#office-unit').select2();
            })
		},

		transferOrganogram: function () {
			var selectedId = $('#office_unit_tree_panel').find('.jstree-clicked .org_checkbox').map(function () {
				return this.id;
			}).get();

			if(selectedId.length>0 && parseInt($('#office-unit').val()) > 0){
				bootbox.dialog({
					message: "আপনি কি অফিস কাঠামো ট্রান্সফার করতে ইচ্ছুক?",
					title: "অফিস কাঠামো ট্রান্সফার",
					buttons: {
						success: {
							label: "হ্যাঁ",
							className: "green",
							callback: function () {
                                Metronic.blockUI({
                                    target: $("#officeUnitTransfer").parent().parent().parent().parent(),
                                    boxed: true,
                                    message: 'অপেক্ষা করুন'
                                });
                                toastr.success('পদবি ট্রান্সফার চলছে। অনুগ্রহপূর্বক অপেক্ষা করুন।');
								PROJAPOTI.ajaxSubmitDataCallback('<?= $this->Url->build(['_name'=>'apiDesignationTransfer']) ?>',
									{selectedDesignation: selectedId, destinationUnit: $('#office-unit').val(),office_id:$("#office-id").val()},'json',function(response){
                                        if(response.status == 'success'){
											toastr.success(response.message);

                                            if ($.inArray( currentDesignationId.toString(), selectedId ) == -1) {
                                                OfficeUnitManagement.reloadOfficeUnitOrg($("#office-id").val());
                                            } else {
                                                window.location.replace(js_wb_root+'logout');
                                            }
										}else{
											toastr.error(response.message)
										}

                                        Metronic.unblockUI($("#officeUnitTransfer").parent().parent().parent().parent());
									})
							}
						},
						danger: {
							label: "না",
							className: "red",
							callback: function () {
                                Metronic.unblockUI($("#officeUnitTransfer").parent().parent().parent().parent());
							}
						}
					}
				});

            }
		},
		loadOfficeUnitTree: function () {

			$('#office_unit_tree_panel').jstree(
				{
					'core': {
						"themes": {
							"variant": "large",
							"multiple": false
						},
						'data': ''
					},

					"checkbox": {
						"keep_selected_style": false
					},
					"plugins": ["checkbox"]
				}
			).bind("loaded.jstree", function (event, data) {
				$(this).jstree("open_all");
			});
		}

	};

	$(function () {

        $("#office-ministry-id").bind('change', function () {
			OfficeUnitManagement.loadMinistryWiseLayers();
		});
		$("#office-layer-id").bind('change', function () {
			OfficeUnitManagement.loadMinistryAndLayerWiseOfficeOrigin();
		});
		$("#office-origin-id").bind('change', function () {
			OfficeUnitManagement.loadOriginOffices();
		});
		$("#office-id").bind('change', function () {
			OfficeUnitManagement.reloadOfficeUnitOrg($(this).val());
		});
		$("#officeUnitTransfer").bind('click', function () {
			OfficeUnitManagement.transferOrganogram();
		});

		$("#office_unit_tree_delete").bind("click", function () {
			OfficeUnitManagement.deleteOfficeUnitOrganogram();
		});

		OfficeUnitManagement.loadOfficeUnitTree();

		if($("#office-id").val()>0){
			OfficeUnitManagement.reloadOfficeUnitOrg($("#office-id").val());
		}
	});
</script>