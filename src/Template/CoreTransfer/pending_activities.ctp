<link href="<?=$this->request->webroot?>wizard/css/material-bootstrap-wizard.css" rel="stylesheet" />
<link href="<?=$this->request->webroot?>wizard/css/wizard.css" rel="stylesheet" />

<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

<div class="portlet light">
	<div class="portlet-title">
		<div class="caption">
			অসম্পন্ন কার্যক্রম সংক্ষিপ্ত বিবরণী
		</div>

	</div>
	<div class="portlet-body">
		<ul class="nav nav-tabs" id="myTab" role="tablist">
			<!--<li class="nav-item active">
				<a class="nav-link" id="dak-tab" data-toggle="tab" href="#dak" role="tab" aria-controls="dak" aria-selected="true">ডাক</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" id="nothi-tab" data-toggle="tab" href="#nothi" role="tab" aria-controls="nothi" aria-selected="false">নথি</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" id="other-nothi-tab" data-toggle="tab" href="#other-nothi" role="tab" aria-controls="other-nothi" aria-selected="false">অন্যান্য অফিসের নথি</a>
			</li>-->
			<li class="nav-item active">
				<a class="nav-link" id="khosora-tab" data-toggle="tab" href="#khosora" role="tab" aria-controls="khosora" aria-selected="false">খসড়া</a>
			</li>
		</ul>
		<div class="tab-content" id="myTabContent">
			<!--<div class="tab-pane fade in active" id="dak" role="tabpanel" aria-labelledby="dak-tab">
				<?php /*if (count($response['dak']) > 0): */?>
					<table class="table table-bordered">
						<thead>
						<tr class="heading">
							<th class="text-center"> ক্রমিক </th>
							<th class="text-center"> পদ </th>
							<th class="text-center"> শাখার নাম </th>
							<th class="text-center"> অফিস নাম </th>
							<th class="text-center"> মোট অসম্পন্ন ডাক </th>
						</tr>
						</thead>
						<tbody>
						<?php
/*						$count = 0;
						foreach ($response['dak'] as $key => $dak) {
							$count++;
							*/?>
							<tr>
								<td class="text-center"><?/*= $this->Number->format($count) */?> </td>
								<td class="text-center"><?/*= $dak['to_officer_name'] */?>, <?/*= $dak['to_officer_designation_label'] */?></td>
								<td class="text-center"><?/*= $dak['to_office_unit_name'] */?></td>
								<td class="text-center"><?/*= $dak['to_office_name'] */?></td>
								<td class="text-center"><?/*= enTobn($dak['total']) */?></td>
							</tr>
							<?php
/*						}
						*/?>
						</tbody>
					</table>
				<?php /*else: */?>
					<h3>কোনো তথ্য পাওয়া যায় নি</h3>
				<?php /*endif; */?>
			</div>
			<div class="tab-pane fade" id="nothi" role="tabpanel" aria-labelledby="nothi-tab">
				<?php /*if (count($response['nothi']) > 0): */?>
					<table class="table table-bordered">
						<thead>
						<tr class="heading">
							<th class="text-center"> ক্রমিক </th>
							<th class="text-center"> পদ </th>
							<th class="text-center"> শাখার নাম </th>
							<th class="text-center"> অফিস নাম </th>
							<th class="text-center"> মোট অসম্পন্ন ডাক </th>
						</tr>
						</thead>
						<tbody>
						<?php
/*						$count = 0;
						foreach ($response['nothi'] as $key => $dak) {
							$count++;
							$user_info = designationInfo($dak['office_units_organogram_id']);
							*/?>
							<tr>
								<td class="text-center"><?/*= $this->Number->format($count) */?> </td>
								<td class="text-center"><?/*= $user_info['officer_name'] */?>, <?/*= $user_info['officer_designation_label'] */?></td>
								<td class="text-center"><?/*= $user_info['office_unit_name'] */?></td>
								<td class="text-center"><?/*= $user_info['office_name'] */?></td>
								<td class="text-center"><?/*= enTobn($dak['total']) */?></td>
							</tr>
							<?php
/*						}
						*/?>
						</tbody>
					</table>
				<?php /*else: */?>
					<h3>কোনো তথ্য পাওয়া যায় নি</h3>
				<?php /*endif; */?>
			</div>
			<div class="tab-pane fade" id="other-nothi" role="tabpanel" aria-labelledby="other-nothi-tab">
				<?php /*if (count($response['other_nothi']) > 0): */?>
					<table class="table table-bordered">
						<thead>
						<tr class="heading">
							<th class="text-center"> ক্রমিক </th>
							<th class="text-center"> পদ </th>
							<th class="text-center"> শাখার নাম </th>
							<th class="text-center"> অফিস নাম </th>
							<th class="text-center"> মোট অসম্পন্ন ডাক </th>
						</tr>
						</thead>
						<tbody>
						<?php
/*						$count = 0;
						foreach ($response['other_nothi'] as $key => $dak) {
							$count++;
							$user_info = designationInfo($dak['office_unit_organogram_id']);
							*/?>
							<tr>
								<td class="text-center"><?/*= $this->Number->format($count) */?> </td>
								<td class="text-center"><?/*= $user_info['officer_name'] */?>, <?/*= $user_info['officer_designation_label'] */?></td>
								<td class="text-center"><?/*= $user_info['office_unit_name'] */?></td>
								<td class="text-center"><?/*= $user_info['office_name'] */?></td>
								<td class="text-center"><?/*= enTobn($dak['total']) */?></td>
							</tr>
							<?php
/*						}
						*/?>
						</tbody>
					</table>
				<?php /*else: */?>
					<h3>কোনো তথ্য পাওয়া যায় নি</h3>
				<?php /*endif; */?>
			</div>-->
			<div class="tab-pane fade in active" id="khosora" role="tabpanel" aria-labelledby="khosora-tab">
				<?php if (isset($response['khosora']) && count($response['khosora']) > 0): ?>
					<table class="table table-bordered">
						<thead>
						<tr class="heading">
							<th class="text-center" rowspan="2"> ক্রমিক </th>
							<th class="text-center" rowspan="2"> পদ </th>
							<th class="text-center" colspan="2"> অসম্পন্ন খসড়ার অবস্থান</th>
						</tr>
						<tr class="heading">
							<th class="text-center">নথি নং</th>
							<th class="text-center">নোট নং</th>
						</tr>
						</thead>
						<tbody>
						<?php
						$count = 0;
						foreach ($response['khosora'] as $office_unit_organogram_id => $data_list) {
							$count++;
							$user_info = designationInfo($office_unit_organogram_id);
							?>
							<tr>
								<td class="text-center" rowspan="<?=count($data_list)?>"><?= $this->Number->format($count) ?> </td>
								<td class="text-left" rowspan="<?=count($data_list)?>">
									<?= $user_info['officer_name'] ?><br/>
									<?= $user_info['officer_designation_label'] ?><br/>
									<?= $user_info['office_unit_name'] ?><br/>
									<?= $user_info['office_name'] ?>
								</td>
								<td class="text-center"><?= $data_list[0]['nothi_no'] ?></td>
								<td class="text-center"><?= $data_list[0]['note_no'] ?></td>
							</tr>

								<?php foreach($data_list as $k => $data): ?>
								<?php if($k == 0) continue; ?>
								<tr>
									<td class="text-center"><?= $data['nothi_no'] ?></td>
									<td class="text-center"><?= $data['note_no'] ?></td>
								</tr>
								<?php endforeach; ?>
							<?php
						}
						?>
						</tbody>
					</table>
				<?php else: ?>
					<h3>কোনো তথ্য পাওয়া যায় নি</h3>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>

<script>
    $('body').addClass('page-sidebar-closed');
    $('.page-sidebar-menu').addClass('page-sidebar-menu-closed');
</script>