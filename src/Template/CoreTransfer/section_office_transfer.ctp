<link href="<?=$this->request->webroot?>wizard/css/material-bootstrap-wizard.css" rel="stylesheet" />
<link href="<?=$this->request->webroot?>wizard/css/wizard.css" rel="stylesheet" />

<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

<style>
	.form-group {
		margin-bottom:0!important;
	}
</style>

<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            অন্যান্য অফিসে শাখা স্থানান্তর করুন
        </div>

    </div>
    <div class="portlet-window-body">
        <div style="padding:10px 20px">
            <?php if(empty($employee_offices)){ ?>
                <?= $officeSelectionCell = $this->cell('OfficeSelection', ['entity' => '']) ?>
                <div class="row">
                    <div class="col-md-4 form-group form-horizontal">
                        <label class="control-label"> <?php echo __("Office") ?> </label>
                        <?php
                        echo $this->Form->input('office_id', array(
                            'label' => false,
                            'class' => 'form-control',
                            'empty' => '----'
                        ));
                        ?>
                    </div>
                </div>
            <?php }else{
                echo $this->Form->input('office_id', array(
                    'label' => false,
                    'class' => 'form-control',
                    'type'=>'hidden',
                    'default' => $employee_offices['office_id']
                ));
            }  ?>
        </div>

        <div class="wizard-container">
            <div class="card wizard-card" data-color="green" id="wizardProfile" style="border-radius: 0 0 10px 10px !important;padding-top: 0;">
                <form action="" method="">
                    <?php
                    echo $this->Form->input('office_id',
                        array(
                            'label' => false,
                            'class' => 'form-control',
                            'empty' => '----',
                            'type'=>'hidden',
                        ));
                    ?>
                    <div class="wizard-navigation">
                        <ul>
                            <li><a href="#section" data-toggle="tab">শাখা নির্বাচন করুন</a></li>
                            <li onclick="showNothiFromAllSelectedSections()"><a href="#organogram" data-toggle="tab">পদবিসমূহ</a></li>
                        </ul>
                    </div>

                    <div class="tab-content">
                        <div class="tab-pane" id="section">
                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
	                                <div style="border: solid 1px blue;padding: 10px;"><i class="fa fa-info-circle"></i> বি.দ্রঃ কোন শাখায় অমিমাংসিত কার্যক্রম রেখে অফিস শাখা স্থানান্তর করা যাবেনা।</div>
                                    <div class="picture-container-unit">
                                        <div id="office_unit_tree_panel" style="height:calc(100vh - 311px);overflow:auto;">

                                        </div>
	                                    <div id="pending_msg" style="display:none;margin-top: 20px;border: solid 1px red;padding: 10px;"></div>
                                    </div>
                                </div>
                                <div class="col-sm-1 col-xs-12">
                                    <div style="height: calc(100vh - 255px);position:relative;">
                                        <i class="fa fa-chevron-right" style="font-size: 50px;position: absolute;top: 50%;transform: translate(-50%, -50%);left: 50%;"></i>
                                    </div>
                                </div>
                                <div class="col-sm-5 col-xs-12">
                                    <h4 class="info-text"> যে অফিসে স্থানান্তর করা হবে </h4>
                                    <?php
                                    echo $this->Form->input('office_unit_id',
                                        array(
                                            'label' => false,
                                            'class' => 'form-control hidden',
                                            'empty' => '----'
                                        ));
                                    ?>
                                    <div class="picture-container">
                                        <?= $this->cell('OfficeUnitOrganogram',["prefix"=>'transfer_','options'=>[
                                            'organogram'=> 0,'unit'=>0
                                        ]]) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="organogram">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4>যে পদসমূহ স্থানান্তর হবে</h4>
                                    <div class="table-responsive" style="height:calc(100vh - 311px);overflow:auto;">
                                        <table id="organogramList" class="table table-bordered table-striped table-hover">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>হস্তান্তরযোগ্য পদবি</th>
                                                <th style="vertical-align: middle;text-align: center;"> <i class="fa fa-forward"></i> </th>
                                                <th>হস্তান্তরিত নতুন পদবি</th>
                                            </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="wizard-footer">
                        <div class="pull-right">
                            <input type='button' class='btn btn-next btn-fill btn-success btn-wd' onclick="NextStepCheck()" name='next' value='পরবর্তী ধাপ' />
                            <input type='button' class='btn btn-finish btn-fill btn-success btn-wd' name='finish' value='শাখা স্থানান্তর প্রক্রিয়া শুরু করুন' />
                        </div>

                        <div class="pull-left">
                            <input type='button' class='btn btn-previous btn-fill btn-default btn-wd' name='previous' value='পূর্ববর্তী ধাপ' />
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>
<div id="where-unit-designation-div" style="display:none;">
	<select name="where_unit_id" class="form-control where_unit_id" onchange="changeUnit(this)">
		<option value="">-- শাখা বাছাই করুন --</option>
	</select>
	<select name="where_organogram_id" class="form-control">
		<option value="">-- পদবি বাছাই করুন --</option>
	</select>
</div>

<script src="<?=$this->request->webroot?>wizard/js/jquery.bootstrap.js" type="text/javascript"></script>
<!--  Plugin for the Wizard -->
<script src="<?=$this->request->webroot?>wizard/js/material-bootstrap-wizard.js"></script>
<!--  More information about jquery.validate here: http://jqueryvalidation.org/	 -->
<script src="<?=$this->request->webroot?>wizard/js/jquery.validate.min.js"></script>
<script>
	$('body').addClass('page-sidebar-closed');
	$('.page-sidebar-menu').addClass('page-sidebar-menu-closed');

	var whereTransfer;
	var fromTransfer = [];

    var OfficeUnitManagement = {
        checked_node_ids: [],
        loadMinistryWiseLayers: function () {
            OfficeSetup.loadLayers($("#office-ministry-id").val(), function (response) {

            });
        },
        loadMinistryAndLayerWiseOfficeOrigin: function () {
            OfficeSetup.loadOffices($("#office-ministry-id").val(), $("#office-layer-id").val(), function (response) {

            });
        },
        loadOriginOffices: function () {
            OfficeSetup.loadOriginOffices($("#office-origin-id").val(), function (response) {

                //OfficeUnitManagement.reloadOriginUnitTree();
            });
        },

        reloadOfficeUnitTree: function () {
            PROJAPOTI.ajaxSubmitAsyncDataCallbackWithoutAbrot(js_wb_root + 'officeOriginTree/populateFullOfficeUnitTree',
                {'office_id': $("#office-id").val()}, 'json',
                function (response) {
                    if (response.length > 0) {
                        var data_str = "";
                        $.each(response[0], function (i) {
                            var node = response[0][i];
                            data_str += '{ "id" : "' + node.id + '", "parent" : "' + ((isEmpty(node.parent))?"#":node.parent) + '", "text" : "' + node.text + '", "icon" : "' + node.icon + '" },';
                        });
                        data_str= data_str.replace(/\\n/g, "\\n")
                            .replace(/\\'/g, "\\'")
                            .replace(/\\"/g, '\\"')
                            .replace(/\\&/g, "\\&")
                            .replace(/\\r/g, "\\r")
                            .replace(/\\t/g, "\\t")
                            .replace(/\\b/g, "\\b")
                            .replace(/\\f/g, "\\f");
                        // remove non-printable and other non-valid JSON chars
                        data_str = data_str.replace(/[\u0000-\u0019]+/g,"");
                        var edited_data_str = "[" + data_str.replace(/^,|,$/g, '') + "]";

                        $('#office_unit_tree_panel').jstree(true).settings.core.data = JSON.parse(edited_data_str);
                        $('#office_unit_tree_panel').jstree(true).refresh();
                        $('#office_unit_tree_panel').bind("refresh.jstree", function (event, data) {
                            $('#office_unit_tree_panel').jstree("open_all");
                        });

                    }
                    else {
                        $('#office_unit_tree_panel').jstree(true).settings.core.data = '';
                        $('#office_unit_tree_panel').jstree(true).refresh();
                        $('#office_unit_tree_panel').bind("refresh.jstree", function (event, data) {
                            $('#office_unit_tree_panel').jstree("open_all");
                        });
                    }

                });
        },

        loadOfficeUnitTree: function () {

            $('#office_unit_tree_panel').jstree(
                {
                    'core': {
                        "themes": {
                            "variant": "large",
                            "multiple": false
                        },
                        'data': ''
                    },

                    "checkbox": {
                        "keep_selected_style": false
                    },
					<?php if($loggedUser['user_role_id']==1 || $loggedUser['user_role_id']==2) { ?>
                    "plugins": ["checkbox"]
					<?php } ?>
                }
            ).bind("loaded.jstree", function (event, data) {
                $(this).jstree("open_all");
            });
        },
    };

    var unit_ids = [];
	$(document).ready(function() {
		$("#office-ministry-id").bind('change', function () {
			OfficeUnitManagement.loadMinistryWiseLayers();
		});
		$("#office-layer-id").bind('change', function () {
			OfficeUnitManagement.loadMinistryAndLayerWiseOfficeOrigin();
		});
		$("#office-origin-id").bind('change', function () {
			OfficeUnitManagement.loadOriginOffices();
		});
		$("#office-id").bind('change', function () {
			var office_id = $(this).val();
			if (office_id > 0) {
				//OfficeUnitManagement.loadOfficeUnits();
                OfficeUnitManagement.loadOfficeUnitTree();
                OfficeUnitManagement.reloadOfficeUnitTree();
				$(document).ajaxStop(function () {
                    $("html, body").animate({scrollTop: 300}, 500);
					// if (office_id != $("input[name=office_id]").val()) {
                    //     $("input[name=office_id]").val(office_id);
                    //     if ($(".wizard-navigation").find("a[href=#section]").parent().hasClass('active')) {
                    //         $("#sectionList").html('<i class="fa fa-cog fa-spin"></i> লোড হচ্ছে...');
                    //         if ($("#office-unit-id").find('option[value!=0]').length > 0) {
                    //             $("#sectionList").html('<table class="table table-hover table-striped"><thead>' +
                    //                 '<tr><th></th><th>শাখার নাম</th></tr>' +
                    //                 '</thead><tbody></tbody></table>');
					//
                    //             $("#office-unit-id").find('option[value!=0]').each(function (key, value) {
                    //                 var unitId = $(value).val();
                    //                 var unitName = $(value).html();
                    //                 $("#sectionList").find('table>tbody').append('<tr><td><input type="checkbox" name="selected_units" onclick="checkUnitSelection(this)" class="sectionSelection" value="' + unitId + '"></td><td style="text-align:left">' + unitName + '</td></tr>');
                    //             });
                    //             $(".sectionSelection").uniform();
                    //         } else {
                    //             $("#sectionList").html('');
                    //         }
                    //     }
                    // }
				})
			} else {
				$('select[id=office-unit-id] option[value!=0]').remove();
				$('select[id=office-unit-id]').val(0).select2();
				$("#sectionList").html('');
			}
		});

        $("#where-unit-designation-div").find('select').select2('destroy');
	});

	function checkUnitSelection(checkboxElm) {
        var units = $("input[name=selected_units]:checked").map(function(i,v) {
            return $(this).val();
        }).get().join(',');
        if (units != "") {
            $('#pending_msg').show();
            $('#pending_msg').html('<span target="_self" style="color:black;">অমিমাংসিত কার্যক্রম এর তালিকা দেখতে <a href="' + js_wb_root + 'pending_activities?unit_ids=' + units + '" class="btn btn-danger btn-sm">এখানে ক্লিক করুন</a></span>');
            $('#pending_msg').find('a').attr('href', js_wb_root + 'pending_activities?unit_ids=' + units);
        } else {
            $('#pending_msg').hide();
        }

        if ($(checkboxElm).is(':checked')) {
			var selectedVal = $('select[id=office-unit-id]').val();
			$('select[id=office-unit-id] option[value='+$(checkboxElm).val()+']').attr('disabled', true);
			if (selectedVal == $(checkboxElm).val()) {
				$('select[id=office-unit-id]').val(0).select2();
			}
		} else {
			$('select[id=office-unit-id] option[value='+$(checkboxElm).val()+']').attr('disabled', false);
		}
	}
	function showNothiFromAllSelectedSections() {
        var checked_node_ids = $('#office_unit_tree_panel').jstree('get_checked');
        unit_ids = [];
        $.each(checked_node_ids, function(k,v){
            var u_id = v.split('_')[2];
            if (u_id == 0) return true;
            unit_ids.push(u_id);
        });

		if ($('#transfer-office-id').val() < 1 || unit_ids.length < 1) {
			window.setTimeout(function () {
				$(".wizard-navigation").find("a[href=#section]").trigger('click');
			}, 1000);
			toastr.error('কোনো অফিস নির্বাচন করা হয় নাই। দয়া করে অফিস নির্বাচন করুন।');
			return;
		}

		Metronic.blockUI({target: '#nothi', boxed: true});

		newUnit_ids = [];
        $.each(unit_ids, function (key, value) {
			newUnit_ids.push($(value).val());
		});
		var is_same = newUnit_ids.length == unit_ids.length && newUnit_ids.every(function(element, index) {
			return element === unit_ids[index];
		});
		if (is_same && whereTransfer == $('#transfer-office-id').val()) {
			Metronic.unblockUI('#nothi');
			return;
		}
		whereTransfer = $('#transfer-office-id').val();

		showOrganogramFromAllSelectedSections();
	}

	function NextStepCheck() {
		window.setTimeout(function() {
			$(".wizard-navigation").find('li.active').trigger('click');
		}, 1000);
	}

	function showOrganogramFromAllSelectedSections() {
	    if ($("input[name=office_id]").val() != whereTransfer) {
            Metronic.blockUI({target: '#organogram', boxed: true});
            $.ajax({
                url: js_wb_root + "coreTransfer/getOrganogramList",
                data: {
                    officeId: $("input[name=office_id]").val(),
                    from_unit: unit_ids,
                    whereTransfer: whereTransfer
                },
                type: "POST",
                dataType: 'JSON',
                success: function (response) {
                    if (response.status == 'success') {
                        if (Object.keys(response.where_office_unit).length > 0) {
                            $.each(response.where_office_unit, function (key, value) {
                                $("#where-unit-designation-div select[name=where_unit_id]").append('<option value="' + key + '">' + value + '</option>');
                            });
                        }
                        $("#pending_msg").show();
                        $("#pending_msg").html(response.msg);
                        $("#organogramList").find('tbody').html("");
                        if (response.organograms.length > 0) {
                            $.each(response.organograms, function (key, value) {
                                $("#organogramList").find('tbody').append('<tr>' +
	                                '<td>' + enTobn(key + 1) + '<input type="hidden" class="organogramIds" data-unitId="' + value.office_unit_id + '" value="' + value.office_unit_organogram_id + '" /></td>' +
	                                '<td><div style="float:left"><img src="' + value.user_photo + '" style="width:70px;height:70px;border: solid 1px;border-radius: 5px !important;margin-right: 8px;"/></div>' + value.officer_name + '<br/>' + value.designation_label + '<br/>' + value.office_unit_name + ', ' + value.office_name + '</td>' +
	                                '<td style="vertical-align: middle;text-align: center;"><i class="fa fa-forward"></i></td>' +
	                                '<td><div id="' + value.office_unit_organogram_id + '_where"><div onclick="loadUnitAndOrganogram(' + value.office_unit_organogram_id + ')" class="btn btn-sm btn-success">পদবি নির্বাচন</div></div></td>' +
	                                '</tr>');
                            });
                            Metronic.unblockUI('#organogram');
                        } else {
                            Metronic.unblockUI('#organogram');
                        }
                    } else {
                        window.setTimeout(function () {
                            $(".wizard-navigation").find("a[href=#section]").trigger('click');
                        }, 1000);
                        $("#pending_msg").show();
                        $("#pending_msg").html(response.msg);
                        toastr.error(response.msg);
                        Metronic.unblockUI('#organogram');
                    }
                }
            });
            return;
        } else {
            window.setTimeout(function () {
                $(".wizard-navigation").find("a[href=#section]").trigger('click');
            }, 1000);
            toastr.error('আপনি একই অফিসে তথ্য স্থানান্তর করতে পারবেন না, দয়া করে অন্য অফিস নির্বাচন করুন');
            Metronic.unblockUI('#organogram');
	    }
	}
	function loadUnitAndOrganogram(organogramId) {
        $("#"+organogramId+"_where").html($("#where-unit-designation-div").html());
        $("#"+organogramId+"_where select").each(function(k,v) {
            $(v).attr('data-organogram-id', organogramId);
            $(v).attr('name', $(v).attr('name')+'['+organogramId+']');
        });
        $("#"+organogramId+"_where").find('select').select2();
	}
    function changeUnit(element) {
        var office_unit_id = $(element).val();
        var organogramId = $(element).data('organogram-id');
        $("select[name='where_organogram_id["+organogramId+"]']").html('<option value="">-- লোডিং --</option>').select2();
        $.ajax({
            url: js_wb_root + "coreTransfer/getOfficeOrganogramListByUnitId",
            data: {office_unit_id:office_unit_id},
            type: "POST",
            dataType: 'JSON',
            success: function (response) {
                if (response.status == 'success') {
                    if (Object.keys(response.organograms).length > 0) {
                        $("select[name='where_organogram_id["+organogramId+"]']").html('<option value="">-- পদবি বাছাই করুন --</option>');
                        $.each(response.organograms, function (key, value) {
                            if ($("select[name^='where_organogram_id['] option:selected[value="+key+"]").length != 0) {
                                //$("select[name='where_organogram_id["+organogramId+"]']").append('<option disabled="disabled" value="' + key + '">' + value + '</option>');
                            } else {
                                $("select[name='where_organogram_id["+organogramId+"]']").append('<option value="' + key + '">' + value + '</option>');
                            }
                        });
                    } else {
                        $("select[name='where_organogram_id["+organogramId+"]']").html('<option value="">-- কোনো শুন্যপদ নাই --</option>');
                    }
                    $("select[name='where_organogram_id["+organogramId+"]']").select2();
                }
            }
        });
    }

	$(document).ready(function() {
		$("input[name=finish]").on('click', function(){
			if (typeof whereTransfer == 'undefined') {
				window.setTimeout(function () {
					$(".wizard-navigation").find("a[href=#section]").trigger('click');
				}, 1000);
				toastr.error('কোনো অফিস নির্বাচন করা হয় নাই। দয়া করে অফিস নির্বাচন করুন।');
				return;
			}
			if (unit_ids.length == 0) {
				window.setTimeout(function () {
					$(".wizard-navigation").find("a[href=#section]").trigger('click');
				}, 1000);
				toastr.error('কোনো শাখা নির্বাচন করা হয় নাই। দয়া করে অফিস নির্বাচন করুন।');
				return;
			}

			if ($(".organogramIds").length != $("select[name^='where_organogram_id['] option:selected[value!='']").length) {
                toastr.error('আপনি সকল পদবি বাছাই করেন নি। দয়া করে সকল পদবি বাছাই করুন।');
                return;
			}

			if ($(".organogramIds").closest('table').find('th:eq(4)').length == 0) {
				$(".organogramIds").closest('table').find('thead>tr').append('<th>অবস্থা</th>');
				$(".organogramIds").closest('table').find('tbody>tr').append('<td>অপেক্ষমান</td>');
			}
			var organogramProcessDone = 0;
			var total = 0;
			toastr.info('পদবি স্থানান্তর শুরু হয়েছে...');
			$("input[name=finish]").val('পদবি স্থানান্তর শুরু হয়েছে...').addClass('disabled');


            $.ajax({
                // url: js_wb_root + 'designationDataTransfer',
                url: js_wb_root + 'apiOfficeTransfer3',
                type: 'POST',
                async: false,
                dataType: 'json',
                data: $("select[name^='where_organogram_id[']").serialize()+'&prev_office_id='+$("#office-id").val()+'&new_office_id='+$("#transfer-office-id").val(),
                success: function (response) {
                    toastr.success('শাখা স্থানান্তর প্রক্রিয়া চলছে। অনুগ্রহপূর্বক অপেক্ষা করুন। প্রক্রিয়া শেষ হলে এসএমএস এর মাধ্যমে জানান হবে। ');
                    setTimeout(function () {
                        window.location.href = js_wb_root+'officeSectionTransferLogs';
                    }, 2000);
                }
            });

			// $.each($("select[name^='where_organogram_id['] option:selected[value!='']"), function(key, value) {
             //    if ($(value).val() > 0) {
             //        var organogramId = $(value).attr('data-organogram-id');
             //        var newOrganogramId = $(value).val();
             //        $(value).closest('tr').find('td:eq(4)').html('অপেক্ষমান');
             //        $.ajax({
             //            // url: js_wb_root + 'designationDataTransfer',
             //            url: js_wb_root + 'apiOfficeTransfer2',
             //            type: 'POST',
             //            async: false,
             //            dataType: 'json',
             //            data: {
             //                selectedDesignation : organogramId,
             //                newDesignation : newOrganogramId,
             //                office_id : $("#office-id").val(),
			//
             //                // selectedDesignation: organogramId,
             //                // newDesignation: $("select[name='organogram_id["+organogramId+"]']").val(),
             //                // destinationOffice: whereTransfer,
             //                // office_id: $("#office-id").val(),
             //                // from_unit: fromTransfer
             //            },
             //            success: function (response) {
             //                organogramProcessDone++;
             //                if (response.status == 'success') {
             //                    $(value).closest('tr').find('td:eq(4)').addClass('bg-green');
             //                    $(value).closest('tr').find('td:eq(4)').html('প্রক্রিয়া চলছে...');
             //                } else {
             //                    $(value).closest('tr').find('td:eq(4)').addClass('bg-red');
             //                    $(value).closest('tr').find('td:eq(4)').html('ত্রুটি');
             //                }
			//
             //                if (organogramProcessDone == $(".organogramIds").length) {
             //                    $("input[name=finish]").val('পদবি স্থানান্তর').removeAttr('disabled').removeClass('disabled');
             //                    toastr.success('শাখা স্থানান্তর প্রক্রিয়া চলছে...');
             //                    window.location.href = js_wb_root+'officeSectionTransferLogs';
             //                }
             //            }
             //        });
             //    }
			// });
		});

	})
</script>