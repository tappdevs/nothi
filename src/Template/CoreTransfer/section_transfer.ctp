<link href="<?=$this->request->webroot?>wizard/css/material-bootstrap-wizard.css" rel="stylesheet" />
<link href="<?=$this->request->webroot?>wizard/css/wizard.css" rel="stylesheet" />

<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

<div class="portlet light">
	<div class="portlet-title">
		<div class="caption">
			শাখা স্থানান্তর করুন
		</div>

	</div>
	<div class="portlet-window-body">
		<div style="padding:10px 20px">
            <?php if(empty($employee_offices)){ ?>
                <?= $officeSelectionCell = $this->cell('OfficeSelection', ['entity' => '']) ?>
				<div class="row">
					<div class="col-md-4 form-group form-horizontal">
						<label class="control-label"> <?php echo __("Office") ?> </label>
                        <?php
                        echo $this->Form->input('office_id', array(
                            'label' => false,
                            'class' => 'form-control',
                            'empty' => '----'
                        ));
                        ?>
					</div>
				</div>
            <?php }else{
                echo $this->Form->input('office_id', array(
                    'label' => false,
                    'class' => 'form-control',
                    'type'=>'hidden',
                    'default' => $employee_offices['office_id']
                ));
            }  ?>
		</div>
		<div class="wizard-container">
			<div class="card wizard-card" data-color="green" id="wizardProfile" style="border-radius: 0 0 10px 10px !important;padding-top: 0;">
				<form action="" method="">
                    <?php
                    echo $this->Form->input('office_id',
                        array(
                            'label' => false,
                            'class' => 'form-control',
                            'empty' => '----',
	                        'type'=>'hidden',
                        ));
                    ?>
					<div class="wizard-navigation">
						<ul>
							<li><a href="#section" data-toggle="tab">শাখা নির্বাচন করুন</a></li>
							<li onclick="showNothiFromAllSelectedSections()"><a href="#nothi" data-toggle="tab">নথি নম্বর পরিবর্তন</a></li>
							<li onclick="showOrganogramFromAllSelectedSections()"><a href="#organogram" data-toggle="tab">পদবিসমূহ</a></li>
						</ul>
					</div>

					<div class="tab-content">
						<div class="tab-pane" id="section">
							<div class="row">
								<div class="col-sm-6 col-xs-12">
									<h4 class="info-text"> শাখাসমূহ নির্বাচন করুন </h4>
									<div class="picture-container">
										<div id="sectionList" style="height:calc(100vh - 311px);overflow:auto;">

										</div>
									</div>
								</div>
								<div class="col-sm-1 col-xs-12">
									<div style="height: calc(100vh - 255px);position:relative;">
										<i class="fa fa-chevron-right" style="font-size: 50px;position: absolute;top: 50%;transform: translate(-50%, -50%);left: 50%;"></i>
									</div>
								</div>
								<div class="col-sm-5 col-xs-12">
									<h4 class="info-text"> যে শাখায় স্থানান্তর করা হবে </h4>
									<div class="picture-container">
										<?php
                                        echo $this->Form->input('office_unit_id',
                                            array(
                                                'label' => false,
                                                'class' => 'form-control',
                                                'empty' => '----'
                                            ));
										?>
									</div>
								</div>
							</div>
						</div>
						<div class="tab-pane" id="nothi">
							<div class="row">
								<div class="col-md-5">
									<h4>নির্বাচিত শাখাসমূহের নথিসমূহ</h4>
									<div class="table-responsive" style="height:calc(100vh - 311px);overflow:auto;">
										<table id="listedNothi" class="table table-bordered table-striped table-hover">
											<thead>
											<tr>
												<th><input type="checkbox" class="listedNothiCheckBoxCheckAll"></th>
												<th>নথির নাম</th>
												<th style="width:100px;">নথির ধরন</th>
												<th style="width:100px;">নথির শাখা</th>
											</tr>
											</thead>
											<tbody></tbody>
										</table>
									</div>
								</div>
								<div class="col-md-2">
									<h4>শাখার নথির ধরন নির্বাচন</h4>
									<div id="whereTransferSectionName"></div>
									<select id="nothi_types" style="width: 100%;"></select>

									<div style="height: calc(100vh - 362px);position:relative;">
										<i class="fa fa-chevron-right" style="font-size: 50px;position: absolute;top: 50%;transform: translate(-50%, -50%);left: 50%;"></i>
									</div>
								</div>
								<div class="col-md-5">
									<h4>পরিবর্তিত নথিসমূহ</h4>
									<div class="table-responsive" style="height:calc(100vh - 311px);overflow:auto;">
										<table id="changedNothi" class="table table-bordered table-striped table-hover">
											<thead>
											<tr>
												<th>#</th>
												<th>নথির নাম</th>
												<th style="width:100px;">নথির ধরন</th>
											</tr>
											</thead>
											<tbody></tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						<div class="tab-pane" id="organogram">
							<div class="row">
								<div class="col-md-12">
									<h4>যে পদসমূহ স্থানান্তর হবে</h4>
									<div class="table-responsive" style="height:calc(100vh - 311px);overflow:auto;">
										<table id="organogramList" class="table table-bordered table-striped table-hover">
											<thead>
											<tr>
												<th>#</th>
												<th>ব্যবহারকারীর নাম</th>
												<th>পদের নাম</th>
												<th>শাখার নাম</th>
											</tr>
											</thead>
											<tbody></tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="wizard-footer">
						<div class="pull-right">
							<input type='button' class='btn btn-next btn-fill btn-success btn-wd' onclick="NextStepCheck()" name='next' value='পরবর্তী ধাপ' />
							<input type='button' class='btn btn-finish btn-fill btn-success btn-wd' name='finish' value='শাখা স্থানান্তর প্রক্রিয়া শুরু করুন' />
						</div>

						<div class="pull-left">
							<input type='button' class='btn btn-previous btn-fill btn-default btn-wd' name='previous' value='পূর্ববর্তী ধাপ' />
						</div>
						<div class="clearfix"></div>
					</div>
				</form>
			</div>
		</div>

	</div>
</div>

<script src="<?=$this->request->webroot?>wizard/js/jquery.bootstrap.js" type="text/javascript"></script>
<!--  Plugin for the Wizard -->
<script src="<?=$this->request->webroot?>wizard/js/material-bootstrap-wizard.js"></script>
<!--  More information about jquery.validate here: http://jqueryvalidation.org/	 -->
<script src="<?=$this->request->webroot?>wizard/js/jquery.validate.min.js"></script>
<script>
    $('body').addClass('page-sidebar-closed');
    $('.page-sidebar-menu').addClass('page-sidebar-menu-closed');

    var whereTransfer;
    var fromTransfer = [];
    var nothiList = {};
    var OfficeUnitManagement = {
        checked_node_ids: [],
        loadMinistryWiseLayers: function () {
            OfficeSetup.loadLayers($("#office-ministry-id").val(), function (response) {

            });
        },
        loadMinistryAndLayerWiseOfficeOrigin: function () {
            OfficeSetup.loadOffices($("#office-ministry-id").val(), $("#office-layer-id").val(), function (response) {

            });
        },
        loadOriginOffices: function () {
            OfficeSetup.loadOriginOffices($("#office-origin-id").val(), function (response) {

            });
        },
        loadOfficeUnits: function () {
            OfficeSetup.loadOfficeUnits($("#office-id").val(), function (response) {

            });
        },
    };

    $(document).ready(function() {
        $("#office-ministry-id").bind('change', function () {
            OfficeUnitManagement.loadMinistryWiseLayers();
        });
        $("#office-layer-id").bind('change', function () {
            OfficeUnitManagement.loadMinistryAndLayerWiseOfficeOrigin();
        });
        $("#office-origin-id").bind('change', function () {
            OfficeUnitManagement.loadOriginOffices();
        });
        $("#office-id").bind('change', function () {
            var office_id = $(this).val();
            $("input[name=office_id]").val(office_id);
            if (office_id > 0) {
                OfficeUnitManagement.loadOfficeUnits();

                $(document).ajaxStop(function () {
                    if ($(".wizard-navigation").find("a[href=#section]").parent().hasClass('active')) {
                        $("html, body").animate({scrollTop:300}, 500)
                        $("#sectionList").html('<i class="fa fa-cog fa-spin"></i> লোড হচ্ছে...');
                        if ($("#office-unit-id").find('option[value!=0]').length > 0) {
                            $("#sectionList").html('<table class="table table-hover table-striped"><thead>' +
                                '<tr><th></th><th>শাখার নাম</th></tr>' +
                                '</thead><tbody></tbody></table>');

                            $("#office-unit-id").find('option[value!=0]').each(function (key, value) {
                                var unitId = $(value).val();
                                var unitName = $(value).html();
                                $("#sectionList").find('table>tbody').append('<tr><td><input type="checkbox" name="selected_units" onclick="checkUnitSelection(this)" class="sectionSelection" value="' + unitId + '"></td><td style="text-align:left">' + unitName + '</td></tr>');
                            });
                            $(".sectionSelection").uniform();
                        } else {
                            $("#sectionList").html('');
                        }
                    }
                })
            } else {
                $('select[id=office-unit-id] option[value!=0]').remove();
                $('select[id=office-unit-id]').val(0).select2();
                $("#sectionList").html('');
            }
        });

        $(".listedNothiCheckBoxCheckAll").on('click', function() {
            if ($(".listedNothiCheckBoxCheckAll").is(':checked')) {
                $(".listedNothiCheckBox:checked").trigger('click');
                $(".listedNothiCheckBox").trigger('click');
            } else {
                $(".listedNothiCheckBox:not(:checked)").trigger('click');
                $(".listedNothiCheckBox").trigger('click');
            }
        })
    });

    function checkUnitSelection(checkboxElm) {
        if ($(checkboxElm).is(':checked')) {
            var selectedVal = $('select[id=office-unit-id]').val();
            $('select[id=office-unit-id] option[value='+$(checkboxElm).val()+']').attr('disabled', true);
            if (selectedVal == $(checkboxElm).val()) {
                $('select[id=office-unit-id]').val(0).select2();
            }
        } else {
            $('select[id=office-unit-id] option[value='+$(checkboxElm).val()+']').attr('disabled', false);
        }
    }
    function showNothiFromAllSelectedSections() {
        if ($('select[id=office-unit-id]').val() < 1 || !$("input[type=checkbox][name=selected_units]").is(':checked')) {
            window.setTimeout(function () {
                $(".wizard-navigation").find("a[href=#section]").trigger('click');
            }, 1000);
            toastr.error('কোনো শাখা নির্বাচন করা হয় নাই। দয়া করে শাখা নির্বাচন করুন।');
            return;
        }

        Metronic.blockUI({target: '#nothi', boxed: true});

        newFromTransfer = [];
        $("input[type=checkbox][name=selected_units]:checked").each(function (key, value) {
            newFromTransfer.push($(value).val());
        });
        var is_same = newFromTransfer.length == fromTransfer.length && newFromTransfer.every(function(element, index) {
            return element === fromTransfer[index];
        });
	    if (is_same && whereTransfer == $('select[id=office-unit-id]').val()) {
            Metronic.unblockUI('#nothi');
	        return;
	    }
        fromTransfer = newFromTransfer;
        whereTransfer = $('select[id=office-unit-id]').val();

        $.ajax({
            url: js_wb_root + "coreTransfer/getNothiList",
            data: {
                officeId: $("input[name=office_id]").val(),
                whereTransfer: whereTransfer,
                fromTransfer: fromTransfer
            },
            type: "POST",
            dataType: 'JSON',
            success: function (response) {
                if (response.status == 'success') {
                    $(".listedNothiCheckBoxCheckAll").prop('checked', false).uniform();
                    $("#changedNothi").find('tbody').html('');
                    $("#listedNothi").find('tbody').html('');

                    if (response.whereTransfer) {
                        $("#whereTransferSectionName").html(response.whereTransfer.unit_name_bng);
                    }

                    $("#nothi_types").html("");
                    if (response.nothiType.length > 0) {
                        $.each(response.nothiType, function (key, value) {
                            $("#nothi_types").append('<option value="' + value.id + '">' + value.type_name + '-' + value.type_code + '</option>');
                        });
                    } else {
                        window.setTimeout(function () {
                            $(".wizard-navigation").find("a[href=#section]").trigger('click');
                        }, 1000);
                        toastr.error('আপনার নির্বাচিত শাখায় কোনো নথির ধরন পাওয়া যায় নি। দয়া করে অন্য শাখা নির্বাচন করুন।');
                        return;
                    }
                    $("#nothi_types").select2();

                    if ((!isEmpty(response.fromTransferNothiList)) && (response.fromTransferNothiList.length != 0)) {
                        $.each(response.fromTransferNothiList, function (key, value) {
                            $("#listedNothi").find('tbody').append('<tr><td><input type="checkbox" class="listedNothiCheckBox" onclick="addNothiWithNewNumber(this)" value="' + value.id + '"></td><td>' + value.subject + '<br/><small>' + value.nothi_no + '</small></td><td>' + value.NothiTypes.type_name + '</td><td>' + value.office_units_id + '</td></tr>');
                        });
                        $("input[type=checkbox]").uniform();
                        Metronic.unblockUI('#nothi');
                    } else {
                        $("input[type=checkbox]").uniform();
                        Metronic.unblockUI('#nothi');
                        $('.btn-next').trigger('click')
                    }
                }
            }
        });
    }
    function NextStepCheck() {
        window.setTimeout(function() {
            $(".wizard-navigation").find('li.active').trigger('click');
        }, 1000);
    }
    function addNothiWithNewNumber(checkboxElm) {
        var nothiId = $(checkboxElm).val();
        var nothiName = $(checkboxElm).closest('td').next().html();
        var nothiType = $("#nothi_types option:selected").html();
        if (typeof nothiList[whereTransfer] == 'undefined') {
            nothiList[whereTransfer] = {};
        }
        if (typeof nothiList[whereTransfer][$("#nothi_types").val()] == 'undefined') {
            nothiList[whereTransfer][$("#nothi_types").val()] = {};
        }
	    //var currentNothiIndex = $.inArray(nothiId.toString(), nothiList[whereTransfer][$("#nothi_types").val()]);
        if ($(checkboxElm).is(':checked')) {
            if (typeof nothiList[whereTransfer][$("#nothi_types").val()][nothiId] == 'undefined') {
                nothiList[whereTransfer][$("#nothi_types").val()][nothiId] = nothiId;
            }
            $("#changedNothi").find('tbody').append('<tr id="nothi_'+nothiId+'"><td><i onclick="removeNothiFromList('+nothiId+')" style="cursor:pointer;" class="fa fa-times removeNothiFromList"></i></td><td>'+nothiName+'</td><td>'+nothiType+'</td></tr>');
        } else {
            removeNothiFromList(nothiId, false);
            $("#changedNothi").find('tbody').find('tr#nothi_'+nothiId).remove();
        }
    }
    function removeNothiFromList(nothiId, trigger = true) {
        $.each(nothiList[whereTransfer], function(key, value) {
            delete value[nothiId];
        });
        if (trigger) {
            $('input[type=checkbox][value=' + nothiId + '].listedNothiCheckBox').trigger('click');
        }
    }
    function showOrganogramFromAllSelectedSections() {
        if (($(".listedNothiCheckBox:checked").length != $(".listedNothiCheckBox").length)) { // || Object.keys(nothiList).length == 0
            window.setTimeout(function () {
                $(".wizard-navigation").find("a[href=#nothi]").trigger('click');
            }, 1000);
            toastr.error('আপনি সকল নথি নির্বাচন করেন নি, দয়া করে সকল নথি নির্বাচন করুন।');
            return;
        }

        Metronic.blockUI({target: '#organogram', boxed: true});
        $.ajax({
            url: js_wb_root + "coreTransfer/getOrganogramList",
            data: {
                officeId: $("input[name=office_id]").val(),
                whereTransfer: whereTransfer,
                fromTransfer: fromTransfer
            },
            type: "POST",
            dataType: 'JSON',
            success: function (response) {
                if (response.status == 'success') {
                    $("#organogramList").find('tbody').html("");
                    if (response.organograms.length > 0) {
                        $.each(response.organograms, function (key, value) {
                            var employeeName = '-';
                            if (value.EmployeeRecords.name_bng != null) {
                                employeeName = value.EmployeeRecords.name_bng;
                            }
                            $("#organogramList").find('tbody').append('<tr><td>'+enTobn(key+1)+'<input type="hidden" class="organogramIds" data-unitId="'+value.unit+'" value="'+value.id+'" /></td><td>' + employeeName + '</td><td>' + value.d_bng + '</td><td>' + value.OfficeUnits.unit_name_bng + '</td></tr>');
                        });
                        Metronic.unblockUI('#organogram');
                    } else {
                        Metronic.unblockUI('#organogram');
                    }
                }
            }
        });
        return;
    }

    $(document).ready(function() {
        $("input[name=finish]").on('click', function(){
	        if (typeof whereTransfer == 'undefined') {
                window.setTimeout(function () {
                    $(".wizard-navigation").find("a[href=#section]").trigger('click');
                }, 1000);
                toastr.error('কোনো শাখা নির্বাচন করা হয় নাই। দয়া করে শাখা নির্বাচন করুন।');
	            return;
	        }
	        if (fromTransfer.length == 0) {
                window.setTimeout(function () {
                    $(".wizard-navigation").find("a[href=#section]").trigger('click');
                }, 1000);
                toastr.error('কোনো শাখা নির্বাচন করা হয় নাই। দয়া করে শাখা নির্বাচন করুন।');
                return;
	        }
            if (($(".listedNothiCheckBox:checked").length != $(".listedNothiCheckBox").length)) { // || Object.keys(nothiList).length == 0
                window.setTimeout(function () {
                    $(".wizard-navigation").find("a[href=#nothi]").trigger('click');
                }, 1000);
                toastr.error('আপনি সকল নথি নির্বাচন করেন নি, দয়া করে সকল নথি নির্বাচন করুন।');
                return;
            }

            if ($(".organogramIds").closest('table').find('th:eq(4)').length == 0) {
                $(".organogramIds").closest('table').find('thead>tr').append('<th>অবস্থা</th>');
                $(".organogramIds").closest('table').find('tbody>tr').append('<td>অপেক্ষমান</td>');
            }
            var organogramProcessDone = 0;
            toastr.info('পদবি স্থানান্তর শুরু হয়েছে...');
            $("input[name=finish]").val('পদবি স্থানান্তর শুরু হয়েছে...').addClass('disabled');
            $.each($(".organogramIds"), function(key, value) {
                $(value).closest('tr').find('td:eq(4)').html('অপেক্ষমান');
                var organogramId = $(value).val();
                $.ajax({
                    url: js_wb_root + 'apiDesignationTransfer',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        selectedDesignation: organogramId,
                        destinationUnit: whereTransfer,
                        office_id: $("#office-id").val(),
                    },
                    success: function (response) {
                        organogramProcessDone++;
                        if (response.status == 'success') {
                            $(value).closest('tr').find('td:eq(4)').addClass('bg-green');
                            $(value).closest('tr').find('td:eq(4)').html('সফল');
                        } else {
                            $(value).closest('tr').find('td:eq(4)').addClass('bg-red');
                            $(value).closest('tr').find('td:eq(4)').html('ত্রুটি');
                        }

                        nothiTransfer(organogramProcessDone);
                    }
                });
            });
        });

        function nothiTransfer(organogramProcessDone) {
            if ($(".organogramIds").length == organogramProcessDone) {
                var totalNothi = 0;
                var totalProcessedNothi = 0;
                if (typeof nothiList[whereTransfer] == 'undefined') {
                    toastr.success('পদবি স্থানান্তর সম্পন্ন হয়েছে');
                    updateUI(totalNothi, totalProcessedNothi);
                } else {
                    $.each(nothiList[whereTransfer], function (key, value) {
                        totalNothi += Object.keys(value).length
                    });
                    toastr.success('পদবি স্থানান্তর সম্পন্ন হয়েছে');
                    toastr.info('নথি স্থানান্তর শুরু হয়েছে...');
                    $("input[name=finish]").val('নথি স্থানান্তর শুরু হয়েছে...');
                    $.each(nothiList[whereTransfer], function (key, nothiIds) {
                        var nothiTypeId = key;
                        $.each(nothiIds, function (key, nothiId) {
                            $.ajax({
                                url: js_wb_root + "nothiMasters/updateNothiNo",
                                data: {
                                    officeId: $("input[name=office_id]").val(),
                                    office_unit_id: whereTransfer,
                                    id: nothiTypeId,
                                    nothi_master_id: nothiId,
                                },
                                type: "POST",
                                dataType: 'JSON',
                                success: function (response) {
                                    totalProcessedNothi++;
                                    updateUI(totalNothi, totalProcessedNothi);
                                }
                            });
                        });
                    });
                }

            }
        }
        function updateUI(totalNothi, totalProcessedNothi) {
            if (totalNothi == totalProcessedNothi) {
                toastr.success('নথি স্থানান্তর সম্পন্ন হয়েছে');
                toastr.success('শাখা স্থানান্তর সম্পন্ন হয়েছে');
                window.location.reload();
            }
        }
    })
</script>