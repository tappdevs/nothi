<div class="portlet light">
    <div class="portlet-title hidden-print">
        <div class="caption"><i class="fs1 a2i_gn_details1"></i> ক্রন লগ রিপোর্ট </div>
    </div>
    <div class="portlet-body">
        <div class="table-toolbar">
            <div class="row text-center">

            </div>

            <div class="row">
                <div class="col-md-12">

                    <!--  Date Range Begain -->

                                       <div class="hidden-print page-toolbar pull-right portlet-title">
                                            <div id="dashboard-report-range" class=" tooltips btn btn-sm btn-default"
                                                 data-container="body"
                                                 data-placement="bottom" data-original-title="তারিখ নির্বাচন করুন">
                                                <i class="icon-calendar"></i>&nbsp; <span
                                                    class="thin uppercase visible-lg-inline-block">তারিখ নির্বাচন করুন</span>&nbsp;
                                                <i class="glyphicon glyphicon-chevron-down"></i>
                                            </div>
                                        </div>

                    <!--  Date Range End  -->

                </div>

            </div>
            <div class="inbox-content">
                
            </div>
        </div>
        
    </div>
</div>



<script src="<?php echo CDN_PATH; ?>daptorik_preview/js/report_date_range.js"></script>
<script src="<?php echo CDN_PATH; ?>js/reports/cron_log_report.js" type="text/javascript"></script>
    <script>
        jQuery(document).ready(function () {
              $('.inbox-content').html('<img src="<?= CDN_PATH ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp; লোড হচ্ছে  <b>' + '</b> । একটু অপেক্ষা করুন... </span><br>');
            $(this).find('body').addClass('page-sidebar-closed');
            $(this).find('.page-sidebar-menu').addClass('page-sidebar-menu-closed');
             DateRange.init();
            DateRange.initDashboardDaterange();

            $(document).off('click', '.pagination a').on('click', '.pagination a', function (ev) {
                ev.preventDefault();
                PROJAPOTI.ajaxSubmitDataCallback($(this).attr('href'),{},'html',function(res){
                    $('.inbox-content').html(res);
                });
            });
        });
    </script>