<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption"><i class=""></i>অফিস স্থর সম্পাদনা করুন </div>
        <a class="btn   purple pull-right " href="<?=isset($redirect)?($redirect):($this->request->webroot.'cron/layerOffices') ?>">ফেরত যান</a>
    </div>
    <div class="portlet-body form"><br><br>
        <?php
        echo $this->Form->create();
        echo $this->Form->hidden('office_id',array('value'=>$data['Offices']['id']));
        ?>
        <div class="form-horizontal">
            <div class="form-group">
                <label class="col-md-4 control-label">অফিসের নাম </label>

                <div class="col-md-6">
                    <?php echo $this->Form->input('office', array('label' => false,'value'=>$data['Offices']['office_name_bng'], 'class' => 'form-control','disabled'=>'disabled')); ?>
                </div>
            </div>
            <?php
            echo $this->Form->hidden('layer', ['value' => $data['OfficeLayers']['id']]);
            ?>
            <div class="form-group">
                <label class="col-md-4 control-label">অফিসের স্থর </label>

                <div class="col-md-6">
                    <?php
                    echo $this->Form->input('layer_level',
                        array('empty' => '--বাছাই করুন--', 'type' => 'select', 'label' => false, 'class' => 'form-control',
                            'options' => $options,'default'=>$data['OfficeLayers']['layer_level']));
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label">অফিসের কাস্টম স্থর </label>
                <div class="col-md-6">
                    <?php
                    echo $this->Form->input('custom_layer_level',
                        array('empty' => '--বাছাই করুন--', 'type' => 'select', 'label' => false, 'class' => 'form-control',
                            'options' => $custom_layer_data+['-1' => 'কাস্টম ম্যাপিং বিহীন'],'empty'=>'--কাস্টম স্থর বাছাই--','default' => $data['Offices']['custom_layer_id']));
                    ?>
                </div>
            </div>
        </div>
        <?php echo $this->element('update'); ?>
        <?php echo $this->Form->end(); ?>
    </div>
</div>
