<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
            <?= __('Office').__('Performance Details') ?>
        </div>
    </div>
    <div class="portlet-body">
        <div class="well text-center">
            <div style = 'font-size: 12pt!important;' class="bold"><u>মার্ক বিন্যাস</u> </div>
            <?php
            if(!empty($allreportMarking) && !empty($columns)){
                foreach($columns as $val){
                    if(empty($allreportMarking[$val])){
                        continue;
                    }
                    ?>
                    <div class="badge badge-primary" style = 'font-size: 12pt!important;margin-right: 2px;'>
                        <?= __($val)?> - <?= enTobn($allreportMarking[$val]) ?>
                    </div>
                    <?php
                }
            }
            ?>
        </div>
        <hr>
        <div class="row"  id="searchPanel">
            <div class="col-md-4 form-group form-horizontal">
                <?php
                echo $this->Form->input('office_origin_id',
                    array('empty' => '--বাছাই করুন--', 'type' => 'select', 'label' => false, 'class' => 'form-control',
                    'options' => $options));
                ?>
                <?php
                echo $this->Form->create('', ['class' => 'searchForm'])
                ?>
                <?php
                echo $this->Form->hidden('start_date', ['class' => 'startdate'])
                ?>
                <?php
                echo $this->Form->hidden('end_date', ['class' => 'enddate']);
                echo $this->Form->hidden('office_ids_length', ['class' => 'office_ids_length'])
                ?>
                <?php echo $this->Form->end() ?>
            </div>

            <!--  Date Range Begain -->
            <div class="col-md-6 col-sm-12 pull-right">
            <div class="col-md-10 col-sm-10">
                <div class="hidden-print page-toolbar pull-right portlet-title">
                    <div id="dashboard-report-range" class=" tooltips btn btn-sm btn-default"
                         data-container="body"
                         data-placement="bottom" data-original-title="তারিখ নির্বাচন করুন">
                        <i class="icon-calendar"></i>&nbsp; <span
                            class="thin uppercase visible-lg-inline-block">তারিখ নির্বাচন করুন</span>&nbsp;
                        <i class="glyphicon glyphicon-chevron-down"></i>
                    </div>
                </div>
                </div>
                 <div class="col-md-2 col-sm-2">
                    <button type="button" class="btn  btn-primary btn-md btn-performance-print"> <i class="fa fa-print">&nbsp;<?=__('Print')?></i> </button>
                </div>
            </div>
        </div>
        <br>
        <br>
        <div class="row" id="showlist">

        </div>
        <div class="">
            <div class="table-container " id="DesignationPerformance">
                <table class="table table-bordered table-hover tableadvance loadDataTable">
                    <thead>
                        <tr class="heading">
                            <th colspan="14" class="text-center" id="headText"></th>
                        </tr>
                       <tr class="heading">
                            <th class="text-center" rowspan="2">অফিসের নাম</th>
                            <th class="text-center" rowspan="2"> সর্বশেষ হালনাগাদ</th>
                            <th colspan="2" class="text-center"> ডাক </th>
                            <th colspan="3" class="text-center"> নথি </th>
                            <th colspan="3" class="text-center" > পত্রজারিতে নিষ্পন্ন নোট</th>
                            <th rowspan="2" class="text-center" >পত্রজারির সংখ্যা </th>
                            <th class="text-center" rowspan="2">সময় ব্যবধান</th>
                            <th class="text-center" rowspan="2">রিপোর্ট মার্ক </th>
                            <th class="text-center" rowspan="2">অফিসের আইডি </th>


                       </tr>
                        <tr class="heading">
                            <th class="text-center" >গৃহীত </th>
                            <th class="text-center" >নিষ্পন্ন</th>

                            <th class="text-center" > স্ব- উদ্যোগে সৃজিত নোট </th>
                            <th class="text-center" > ডাক থেকে সৃজিত নোট </th>
                            <th class="text-center" > নোটে নিষ্পন্ন </th>

                            <th class="text-center" > আন্তঃসিস্টেম </th>
                            <th class="text-center" > ইমেইল ও অন্যান্য </th>
                            <th class="text-center" >মোট </th>
                        </tr>
                    </thead>
                    <tbody id ="addData">

                    </tbody>
                </table>
            </div>


            <!--Tools end -->


        </div>
        <br/>
        <div class="inbox-content">

        </div>
    </div>
</div>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">
<script src="<?=$this->request->webroot?>assets/global/scripts/printThis.js"></script>
<script src="<?php echo CDN_PATH; ?>daptorik_preview/js/report_date_range.js"></script>
<script src="<?php echo CDN_PATH; ?>js/reports/offices_reports.js?v=<?= js_css_version?>" type="text/javascript"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/floatthead/2.0.3/jquery.floatThead.min.js"></script>-->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>

<script>
    var mark = <?= json_encode($allreportMarking);?>;
        jQuery(document).ready(function () {
//              $('.table').floatThead({
//	                position: 'absolute',
//                    top: jQuery("div.navbar-fixed-top").height()
//            });
//            $('.table').floatThead('reflow');
        });
    var office_ids = [];
    var office_name = [];
    jQuery(document).ready(function () {
        $(this).find('body').addClass('page-sidebar-closed');
        $(this).find('.page-sidebar-menu').addClass('page-sidebar-menu-closed');
        DateRange.init();
        DateRange.initDashboardDaterange();
        $("#office-origin-id").bind('change', function () {
            if(!($(this).val())==''){
                getOfficesId($(this).val());
            }
            $('#addData').html('');
        });
        function getOfficesId(office_layer_id) {
//                        DateRange.init();
//                        DateRange.initDashboardDaterange();
            $('#showlist').html('<img src="<?= CDN_PATH ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp; লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');
            $.ajax({
                type: 'POST',
                url: "<?php echo $this->Url->build(['controller' => 'cron', 'action' => 'getLayerwiseOffices']) ?>",
                dataType: 'json',
                data: {"office_layer_type": office_layer_id},
                success: function (data) {
                    office_ids = [];
                    office_name = [];
                    var ind = 0;
//                     console.log();
                    $.each(data, function (i, v) {
                        office_ids.push(i);
                        office_name.push(v);

                    });

//                     callOffices(0);
                    $(".office_ids_length").val(office_ids.length);
//                         DateRange.init();
//                         DateRange.initDashboardDaterange();
                    SortingDate.init($('.startdate').val(), $('.enddate').val());
//                    console.log(office_ids);
                }
            });
        }
    });

    function callOffices(cnt, date_start, date_end) {
//        for(var i= 0; i < office_ids.length; i++){
//            console.log( office_ids.length);
//            console.log(cnt);
        if (cnt == office_ids.length) {
            $('#showlist').html('');
            Metronic.unblockUI('#searchPanel');
            loadDatatable($("#headText").text());
            return false;
        }else if(cnt==0){
            var table = $('.loadDataTable').DataTable();
            table.clear();
            table.destroy();
        }
//     console.log(date_start);
//     console.log(date_end);

        $('#showlist').html('<img src="<?= CDN_PATH ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp; লোড হচ্ছে  <b>' + office_name[cnt] + '</b> । একটু অপেক্ষা করুন... </span><br>');
        var office_id = office_ids[cnt];
        $.ajax({
            type: 'POST',
            url: "<?php echo $this->Url->build(['controller' => 'Cron', 'action' => 'officesReportsContent']) ?>",
            //                dataType: 'json',
            data: {"office_id": office_id, "type": 'dak', 'date_start': date_start, 'date_end': date_end},
            success: function (data_dak) {
                if (data_dak == 'false')
                {
                    callOffices(cnt + 1, date_start, date_end);
                } else {
                    $.ajax({
                        type: 'POST',
                        url: "<?php echo $this->Url->build(['controller' => 'Cron', 'action' => 'officesReportsContent']) ?>",
                        //                dataType: 'json',
                        data: {"office_id": office_id, "type": 'nothi', 'date_start': date_start, 'date_end': date_end},
                        success: function (data_nothi) {
                            var totalMark = generateMark($.extend({}, data_dak, data_nothi));
                            toAdd = '<tr>' +
                                    '<td class="text-center" ><b>' + office_name[cnt] + '</b></td> ' +
                                    '<td class="text-center"> <b>' + enTobn(data_nothi.lastUpdate) + '</b></td>' +
                                    '<td class="text-center"> <b>' + enTobn(data_dak.totalInbox) + '</b></td>' +
                                    '<td class="text-center"><b>' + enTobn(parseInt(data_dak.totalNothijato) + parseInt(data_dak.totalNothivukto)) + '</b></td>' +
                                    '<td class="text-center"><b>' + enTobn( parseInt(data_nothi.selfNote)) + '</b></td> ' +
                                    '<td class="text-center"><b>' + enTobn(parseInt(data_nothi.dakNote) ) + '</b></td> ' +
                                    '<td class="text-center"><b>' + enTobn(data_nothi.NisponnoNote) + '</b></td>' +
                                    '<td class="text-center"><b>' + enTobn(data_nothi.potrojari_nisponno_internal) + '</b></td> ' +
                                    '<td class="text-center"><b>' + enTobn(data_nothi.potrojari_nisponno_external) + '</b></td> ' +
                                    '<td class="text-center"><b>' + enTobn(data_nothi.NisponnoPotrojari) + '</b></td>' +
                                    '<td class="text-center"><b>' + enTobn(data_nothi.Potrojari) + '</b></td> ' +
                                    '<td class="text-center"><b>' + enTobn(data_nothi.date_range) + '</b></td> ' +
                                    '<td class="text-center"><b>' + (totalMark) + '</b></td> ' +
                                    '<td class="text-center"><b>' + enTobn(data_nothi.office_id) + '</b></td> ' +
                                    '</tr>';
                            $('#addData').append(toAdd);
                            callOffices(cnt + 1, date_start, date_end);
                        }
                    });
                }

            }
        });
//                break;
//        }

    }
</script>
