<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
            <?= __('Nagorik Dak') ?> এক্সপোর্ট
        </div>
    </div>
    <div class="portlet-body">
        <?= $this->Cell('OfficeSelection'); ?>
        <div class="row">
            <div class="col-md-4 col-sm-8 form-group form-horizontal">
                <label class="control-label"> <?php echo __("Office") ?> </label>
                <?php
                echo $this->Form->input('office_id',
                    array(
                        'label' => false,
                        'class' => 'form-control',
                        'empty' => '----'
                    ));
                ?>
            </div>
            <div class="col-md-4 col-sm-8 form-group form-horizontal">
                <br/>
                <input type="button" class="btn btn-primary btn-export" value="<?= __('Export') ?>" />
            </div>
        </div>
        <div class="row" id="showlist">
            <form id="form_submit"
                  action="<?= $this->Url->build(['controller' => 'Cron', 'action' => 'nagorikAbedon']) ?>"
                  method="post">
                <input type="hidden" name="ofc_id" id="ofc_id" value=""/>
            </form>
        </div>
        <br/>
    </div>
</div>
<script>
    var EmployeeAssignment = {
        checked_node_ids: [],
        loadMinistryWiseLayers: function () {
            OfficeSetup.loadLayers($("#office-ministry-id").val(), function (response) {

            });
        },
        loadMinistryAndLayerWiseOfficeOrigin: function () {
            OfficeSetup.loadOffices($("#office-ministry-id").val(), $("#office-layer-id").val(), function (response) {

            });
        },
        loadOriginOffices: function () {
            OfficeSetup.loadOriginOffices($("#office-origin-id").val(), function () {

            });
        },
    };
    $("#office-ministry-id").bind('change', function () {
        EmployeeAssignment.loadMinistryWiseLayers();
    });
    $("#office-layer-id").bind('change', function () {
        EmployeeAssignment.loadMinistryAndLayerWiseOfficeOrigin();
    });
    $("#office-origin-id").bind('change', function () {
        EmployeeAssignment.loadOriginOffices();
    });
    $("#office-id").bind('change', function () {
        var ofc_id = $("#office-id :selected").val();
        if (ofc_id == 0 || ofc_id == '' || typeof(ofc_id) == 'undefined') {
            return;
        }
        $("#ofc_id").val($("#office-id :selected").val());
    });

    $('.btn-export').click(function(){
        $("#form_submit").submit();
    })
</script>