<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
            <b>জেলা ভিত্তিক অফিস তালিকা</b>
        </div>
        <div class="col-md-1 form-group form-horizontal pull-right">
                <button type='button' id='btn-notesheet-print' class='btn btn-success pull-right'>
                    <i class='fa fa-print'></i>
                    <?= __('Print');?>
                </button>
        </div>
    </div>
    <div class="portlet-body">
        <div class="row">
            <?php
                echo $this->Form->create('', ['class' => 'searchForm','method'=>'post'])
            ?>
            <div class="col-md-3 form-group form-horizontal">
                <?php
                echo $this->Form->input('district_id',
                    array('empty' => '--জেলা বাছাই করুন--', 'type' => 'select', 'label' => false, 'class' => 'form-control',
                    'options' => $options ));
                ?>
            </div>
            <div class="col-md-3 form-group form-horizontal">
                <?php
                echo $this->Form->input('active_status_id',
                    array('empty' => '--অবস্থা বাছাই করুন--', 'type' => 'select', 'label' => false, 'class' => 'form-control',
                    'options' => [
                        '1'=>'সক্রিয়',
                        '2'=>'নিষ্ক্রিয়',
                        ],
                    ));
                ?>
            </div>
            <div class="col-md-3 form-group form-horizontal">
                <?php
                echo $this->Form->input('mapping_status_id',
                    array('empty' => '--ম্যাপিং বাছাই করুন--', 'type' => 'select', 'label' => false, 'class' => 'form-control',
                    'options' => [
                        '1'=>'আছে',
                        '2'=>'নেই',
                    ],
                ));
                ?>
            </div>

            <div class="col-md-6 form-group form-horizontal">
                <?php
                    echo $this->Form->input('office_name',
                        array('placeholder' => '--অফিসের নাম--', 'label' => false, 
                            'class' => 'form-control',
                            ));
                ?>
            </div>

            <div class="col-md-3 form-group form-horizontal">
                <?php
                echo $this->Form->button(__('Search'),
                    array('label' => false,
                        'class' => 'btn btn-success btn-block',
                        'id'=>'btnSearchs',
                        'type'=>'submit',
                    ));
                ?>
            </div>
            <?php echo $this->Form->end() ?> 
        </div>
        <br>
        <br>
        <div class="row" id="showlist">

        </div>
                <table class="table table-bordered table-hover tableadvance" id="myTable">
                    <thead>
                        <caption>মোট অফিস: <span id='totalOfficeCount'><?php echo enTobn(empty($data)?'0':count($data->toArray()));?></span> </caption>
                        <tr class="heading">
                            <th style='width:10% !important' class="text-center" > অফিসের আইডি </th>
                            <th class="text-center" style='width:40%' > অফিসের নাম </th>
                            <th class="text-center" > অবস্থা </th>
                            <th class="text-center" > ম্যাপিং তথ্য </th>
                        </tr>
                    </thead>
                    <tbody id ="addData">
                        <?php if(!empty($data)):?>
                            
                            <?php foreach($data as $office):?>
                                <tr>
                                    <td style='width:10% !important'  class="text-center"><?php echo enTobn($office->id);?></td>
                                    <td style='width:40%;word-wrap: none;height:40px'><?php echo $office->office_name_bng ;?></td>
                                    <td class="text-center"><?php echo ($office->active_status)?__('Active'):__('Deactive') ;?></td>
                                    <td class="text-center"><?php echo !empty($office->office_domain['office_id'])?__('have_'):__('has_no') ;?></td>
                                </tr>
                            <?php endforeach;?>
                        <?php endif;?>
                    </tbody>
                </table>
                 
            <!--Tools end -->

        <br/>
        <div class="inbox-content">

        </div>
    </div>
</div>
<script src="<?=$this->request->webroot?>assets/global/scripts/printThis.js"></script>
<script src="<?php echo CDN_PATH; ?>daptorik_preview/js/report_date_range.js"></script>
<script src="<?php echo CDN_PATH; ?>js/reports/offices_reports.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/floatthead/2.0.3/jquery.floatThead.min.js"></script>
<script>
        jQuery(document).ready(function () {
              $('.table').floatThead({
	                position: 'absolute',
                    top: jQuery("div.navbar-fixed-top").height()
            });
            $('.table').floatThead('reflow');
        });

    jQuery(document).ready(function () {
        $(this).find('body').addClass('page-sidebar-closed');
        $(this).find('.page-sidebar-menu').addClass('page-sidebar-menu-closed');


        $(document).on('click', '#btn-notesheet-print', function () {
        $(document).find('#myTable').printThis({
            importCSS: true,
            debug: false,
            importStyle: true,
            printContainer: true,
            pageTitle: "",
            removeInline: false,
            header: null
        });
    });

    });


    function myFunction(myControl,col) {
            var input, filter, table, tr, td, i, txtValue;
            // input = document.getElementById("active-status-id");
            input = myControl;
            filter = input.value.toUpperCase();
            table = document.getElementById("myTable");
            tr = table.getElementsByTagName("tr");
            var counter = 0;
            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[col];
                if (td) {
                txtValue = td.textContent || td.innerText;
                if (txtValue.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                    counter+=1;
                } else {
                    tr[i].style.display = "none";
                }
                }       
            }

            document.getElementById("totalOfficeCount").innerHTML = enTobn(counter);
        }

</script>
