<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
            অফিসের পত্রজারি  ট্র্যাকিং
        </div>
    </div>
    <div class="portlet-body">
        <div class="row">
            <?= $this->Cell('OfficeSelection'); ?>
            <div class="row">
                <div class="col-md-4 col-sm-8 form-group form-horizontal">
                    <label class="control-label"> <?php echo __("Office") ?> </label>
                    <?php
                    echo $this->Form->hidden('start_date', ['class' => 'startdate']);
                    echo $this->Form->hidden('end_date', ['class' => 'enddate']);
                    echo $this->Form->input('office_id',
                        array(
                        'label' => false,
                        'class' => 'form-control',
                        'empty' => '----'
                    ));
                    ?>
                </div>
                           <!--Tools end -->

            <!--  Date Range Begain -->
            <div class="col-md-8 col-sm-4 pull-right margin-top-20" >
                <div class="hidden-print page-toolbar pull-right portlet-title">
                    <div id="dashboard-report-range" class=" tooltips btn btn-sm btn-default"
                         data-container="body"
                         data-placement="bottom" data-original-title="তারিখ নির্বাচন করুন">
                        <i class="icon-calendar"></i>&nbsp; <span
                            class="thin uppercase visible-lg-inline-block">তারিখ নির্বাচন করুন</span>&nbsp;
                        <i class="glyphicon glyphicon-chevron-down"></i>
                    </div>
                </div>
            </div>
            <!--  Date Range End  -->

            </div>
        </div>
        <div class="row" id="showlist">

        </div>
        <div class="row" >
            <div class="table-container " id ="addData">
            </div>



        </div>
        <br/>
        <div id="inbox-content">

        </div>
    </div>
</div>
<script src="<?php echo CDN_PATH; ?>daptorik_preview/js/report_date_range.js"></script>
<script src="<?php echo CDN_PATH; ?>js/reports/potrojari_tracking.js" type="text/javascript"></script>
<script>
    jQuery(document).ready(function () {

        $(this).find('body').addClass('page-sidebar-closed');
        $(this).find('.page-sidebar-menu').addClass('page-sidebar-menu-closed');
        DateRange.init();
        DateRange.initDashboardDaterange();
        var EmployeeLoginHistory = {
            loadMinistryWiseLayers: function () {
                OfficeSetup.loadLayers($("#office-ministry-id").val(), function () {

                });
            },
            loadMinistryAndLayerWiseOfficeOrigin: function () {
                OfficeSetup.loadOffices($("#office-ministry-id").val(), $("#office-layer-id").val(), function () {

                });
            },
            loadOriginOffices: function () {
                OfficeSetup.loadOriginOffices($("#office-origin-id").val(), function () {

                });
            },
        };
        $("#office-ministry-id").bind('change', function () {
            EmployeeLoginHistory.loadMinistryWiseLayers();
        });
        $("#office-layer-id").bind('change', function () {
            EmployeeLoginHistory.loadMinistryAndLayerWiseOfficeOrigin();
        });
        $("#office-origin-id").bind('change', function () {
            EmployeeLoginHistory.loadOriginOffices();
        });
        $("#office-id").bind('change', function () {
            SortingDate.init($('.startdate').val(), $('.enddate').val());
        });
    });
    function  loadPotrojari(office_id,start_date,end_date) {
        Metronic.blockUI({target: '.portlet-body', boxed: true});
        $('#showlist').html('');
        $('#inbox-content').html('');
        $.ajax({
            type: 'POST',
            url: "<?php echo $this->Url->build(['controller' => 'Cron', 'action' => 'potrojariTracking']) ?>",
            data: {'office_id': office_id,'start_date' : start_date,'end_date' : end_date,'office_name' : $("#office-id :selected").text()},
            success: function (data) {
                Metronic.unblockUI('.portlet-body');
                $('#showlist').html(data);
            },
            error: function () {
                Metronic.unblockUI('.portlet-body');
            }
        });
    }
    function BntoEng(input) {
        var numbers = {
            0: '০',
            1: '১',
            2: '২',
            3: '৩',
            4: '৪',
            5: '৫',
            6: '৬',
            7: '৭',
            8: '৮',
            9: '৯'
        };

        var output = '';
        if (parseInt(input) != 0) {
            while (parseInt(input) != 0) {
                var n = parseInt(input % 10);
                if (!Number.isInteger(n))
                    break;
                input = input / 10;
                output += (numbers[n]);
            }
        } else
        {
            output = '০';
        }
        for (var i = output.length - 1, o = ''; i >= 0; i--)
        {
            o += output[i];
        }
        return o;

    }
</script>

