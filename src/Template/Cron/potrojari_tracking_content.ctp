                <table class="table table-bordered table-hover tableadvance">
                    <thead>
                        <tr class="heading">
                            <th class="text-center" >মোট পত্রজারি </th>
                            <th class="text-center" >মোট আন্তঃসিস্টেম পত্রজারি </th>
                            <th class="text-center">মোট অন্যান্য ও ইমেইল পত্রজারি</th>
                            <th class="text-center" ><?= __('Actions') ?></th>
                        </tr>
                    </thead>
                    <tbody >
                        <?php
                        if($data['status']==1){
                        ?>
                        <tr>
                            <td class="text-right"><?= enTobn($data['total_potrojari']) ?> </td>
                            <td class="text-right"><?= enTobn($data['potrojari_internal']) ?> </td>
                            <td class="text-right"><?= enTobn($data['potrojari_external']) ?> </td>
                            <td class="text-center">
                                <button type="button" class="btn btn-primary btn-sm" onclick="getDetails('<?=$office_id?>','<?=$start_date?>','<?=$end_date?>')">  <?=__('Details')?> </button>
                                <button type="button" class="btn btn-danger btn-sm" onclick="exportDetails('<?=$office_id?>','<?=$start_date?>','<?=$end_date?>')">  <?=__('Export')?> </button>
                            </td>
                        </tr>
                        <?php
                        }else{
                            ?>
                        <tr><td colspan="4"> কোন তথ্য পাওয়া যায়নি। <?= !empty($data['msg'])?('কারণঃ '.$data['msg'] ):'' ?> </td></tr>
                        <?php
                        }
                        ?>
                    </tbody>
                </table>
<form id="form_submit" action="<?= $this->Url->build(['controller' => 'Cron', 'action' => 'potrojariTrackingDetails','?' =>['downloadxls' => 'true'] ]) ?>" method="post">
    <input type="hidden" name="office_id" id="office_xl_id"  value="<?php echo $office_id?>"/>
    <input type="hidden" name="office_name" id="office_xl_name"  value="<?php echo $office_name?>"/>
    <input type="hidden" name="total_potrojari" id="total_potrojari" value="<?php echo isset($data['total_potrojari'])?$data['total_potrojari']:0?>"/>
    <input type="hidden" name="potrojari_internal" id="potrojari_internal" value="<?php echo isset($data['potrojari_internal'])?$data['potrojari_internal']:0?>"/>
    <input type="hidden" name="potrojari_external" id="potrojari_external" value="<?php echo isset($data['potrojari_external'])?$data['potrojari_external']:0?>"/>
    <input type="hidden" name="start_date" id="start_xl_id"  value="<?php echo $start_date?>"/>
    <input type="hidden" name="end_date" id="end_xl_id" value="<?php echo $end_date?>"/>
</form>
<script>
    jQuery(document).ready(function () {
        $(document).off('click','.pagination a').on('click','.pagination a',function(ev){
            ev.preventDefault();
            PROJAPOTI.ajaxSubmitAsyncDataCallback($(this).attr('href'),{'office_id': '<?=$office_id?>','start_date' : '<?=$start_date?>','end_date' : '<?=$end_date?>','office_name' : $("#office-id :selected").text()},'html',function(response){
                $('#inbox-content').html(response);
            });
        });
    });
    function getDetails(office_id,start_date,end_date){
            Metronic.blockUI({target: '.portlet-body', boxed: true});
            $.ajax({
            type: 'POST',
            url: "<?php echo $this->Url->build(['controller' => 'Cron', 'action' => 'potrojariTrackingDetails']) ?>",
            data: {'office_id': office_id,'start_date' : start_date,'end_date' : end_date,'office_name' : $("#office-id :selected").text()},
            success: function (data) {
                Metronic.unblockUI('.portlet-body');
                $('#inbox-content').html(data);
            },
            error: function () {
                Metronic.unblockUI('.portlet-body');
            }
        });
    }
     function viewDetails(office_id,potrojari_id) {
                Metronic.blockUI({target: '.portlet-body', boxed: true});
                $.ajax({
                    type: 'GET',
                    url: "<?php echo $this->Url->build(['controller' => 'Potrojari', 'action' => 'getPdfById']) ?>/" +potrojari_id+'/'+ office_id+"/0",
                    success: function (data) {
                        Metronic.unblockUI('.portlet-body');
                        if (data.status == 'success') {
                            $("#myModal").modal('show');
                             $('#preview').find('.showPreview').attr('src', data.src);
                        }else{
                            toastr.error(data.msg);
                        }
                    },
                    error: function () {
                        Metronic.unblockUI('.portlet-body');
                        toastr.error('Error Occured');
                    }
                });
            }
            function exportDetails(){
                $("#form_submit").submit();
            }
</script>