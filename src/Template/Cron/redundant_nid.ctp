<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="row">
        <div class="col-md-5 caption">
            প্রয়োজনাতিরিক্ত এন-আই-ডি ট্র্যাকিং
        </div>
            <div class="col-md-7">
                <div class="input-group ">
                    <input type="text" class="form-control input-sm" placeholder="এন আই ডি নম্বর দিয়ে খুঁজুন" name="search" id="search"
                           value="<?= $search ?>">
                    <span class="input-group-btn">
                            <button type="button" class="btn btn-sm green-haze " id="filter_submit_btn1">
                                &nbsp; <i class="fa fa-search" aria-hidden="true"></i>
                            </button>
                    <button type="reset" class="btn btn-sm red filter-cancel">
                                &nbsp; <i class="fa fa-refresh" aria-hidden="true"></i>
                            </button></span>
                </div>
            </div>
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-responsive">
            <table class="table table-bordered table-hover tableadvance">
                <thead>
                    <tr class="heading">
                        <th  class="text-center"> <?= __('Name English') ?> </th>
                        <th  class="text-center"> <?= __('Name Bangla') ?> </th>
                        <th  class="text-center"> জন্ম তারিখ </th>
                        <th  class="text-center"> এন আই ডি </th>
                        <th  class="text-center">  <?= __('Cadre') ?> </th>
                        <th  class="text-center">  <?= __('Identity No') ?> </th>
                        <th  class="text-center">  ইউজার আইডি </th>
                        <th  class="text-center"><?= __('Username') ?>  </th>
                        <th  class="text-center"><?= __('Created') ?>  </th>
                    </tr>
                </thead>
                <tbody >
                    <?php
                    if (!empty($duplicate_Nid)) {
                         $prev_nid = '';
                        foreach ($duplicate_Nid as $d_key => $val) {
                            if (!empty($val)) {
                                foreach ($val as $dd_key => $d_val) {
                                    ?>
                                    <tr>
                                        <td class="text-right"><?= h($d_val['name_eng']) ?> </td>
                                        <td class="text-right"><?= h($d_val['name_bng']) ?> </td>
                                        <td class="text-right"><?= h($d_val['date_of_birth']) ?> </td>
                                        <td class="text-right"><?= (($prev_nid == $d_val['nid']) ? ' একই ' : h($d_val['nid']) ) ?> </td>
                                        <td class="text-right"><?= (($d_val['is_cadre'] == 1 || $d_val['is_cadre'] == 5)) ? 'হ্যাঁ' : 'না' ?> </td>
                                        <td class="text-right"><?= h($d_val['identity_no']) ?> </td>
                                        <td class="text-right"><?= ($d_val['username']) ?> </td>
                                        <td class="text-right"><?= h($d_val['alias']) ?> </td>
                                        <td class="text-right"><?= ($d_val['created']) ?> </td>
                                    </tr>
                                    <?php
                                    $prev_nid = $d_val['nid'];
                                }
                            }
                        }
                    }
                    else {
                        ?>
                        <tr><td colspan="9"> কোন তথ্য পাওয়া যায়নি। </td></tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="actions text-center">
    <ul class="pagination pagination-sm">
        <?php
        echo $this->Paginator->first(__('প্রথম', true), array('class' => 'number-first'));
        echo $this->Paginator->prev('<<', array('tag' => 'li', 'escape' => false),
            '<a href="#" class="btn btn-sm blue">&laquo;</a>',
            array('class' => 'prev disabled btn btn-sm blue', 'tag' => 'li', 'escape' => false));
        echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true,
            'currentClass' => 'active', 'currentTag' => 'a', 'reverse' => FALSE));
        echo $this->Paginator->next('>>', array('tag' => 'li', 'escape' => false),
            '<a href="#" class="btn btn-sm blue ">&raquo;</a>',
            array('class' => 'next disabled', 'tag' => 'li', 'escape' => false));
        echo $this->Paginator->last(__('শেষ', true), array('class' => 'number-last'));
        ?>
    </ul>
</div>
<script>
    $(document).off('click', '#filter_submit_btn1');
    $(document).on('click', '#filter_submit_btn1', function () {
        window.location = "<?=$this->request->webroot?>redundant-nid-check?search="+$("#search").val();
    });
	$(document).on('click', '.filter-cancel', function () {
		window.location = "<?=$this->request->webroot?>redundant-nid-check";
	});
</script>