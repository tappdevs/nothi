<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
            <?= __('Office').__('Performance Details') ?>
        </div>
    </div>
    <div class="portlet-body">
        <div class="row" id="showlist">

        </div>
        <div class="row">
            <div class="table-container ">
                <table class="table table-bordered table-hover tableadvance">
                    <thead>
                        <tr class="heading">
                            <th class="text-center">অফিসের নাম</th>
                            <th class="text-center" > মোট নিষ্পন্ন নোট</th>
                            <th class="text-center" >মোট পত্রজারি </th>
                            <th class="text-center" >মোট পত্রজারি প্রাপক </th>
                            <th class="text-center" >মোট পত্রজারি অনুলিপি </th>
                            <th class="text-center" >মোট অনলাইন নাগরিক আবেদন </th>
                            <th class="text-center" >মোট ফর্ম পোর্টাল থেকে নাগরিক আবেদন </th>
                            <th class="text-center" >মোট নাগরিক আবেদন </th>
                        </tr>
                    </thead>
                    <tbody id ="addData">

                    </tbody>
                </table>
            </div>


            <!--Tools end -->


        </div>
        <br/>
        <div class="inbox-content">

        </div>
    </div>
</div>
<script src="<?php echo CDN_PATH; ?>daptorik_preview/js/report_date_range.js"></script>
<script src="<?php echo CDN_PATH; ?>js/reports/offices_reports.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/floatthead/2.0.3/jquery.floatThead.min.js"></script>
<script>
    jQuery(document).ready(function () {
        $('.table').floatThead({
            position: 'absolute',
            top: jQuery("div.navbar-fixed-top").height()
        });
        $('.table').floatThead('reflow');
    });
    jQuery(document).ready(function () {

        var office_ids = [<?php echo '"'.implode('","', array_keys($all_offices)).'"' ?>];
        var office_name = [<?php echo '"'.implode('","', array_values($all_offices)).'"' ?>];
        $(this).find('body').addClass('page-sidebar-closed');
        $(this).find('.page-sidebar-menu').addClass('page-sidebar-menu-closed');
        callOffices(0);
    var TotalOffices = 0;
    var nisponno = 0;
    var potrojari = 0;
    var nagorik_dak_online = 0;
    var nagorik_dak_portal = 0;
    var portojari_receiver = 0;
    var potrojari_onulipi = 0;
    var nagorik_total = 0;
        function callOffices(cnt) {
//        for(var i= 0; i < office_ids.length; i++){
//            console.log( office_ids.length);
//            console.log(cnt);
            if (cnt == office_ids.length) {
                $('#showlist').html('');
                showTotal();
                return;
            }
//     console.log(date_start);
//     console.log(date_end);

            $('#showlist').html('<img src="<?= CDN_PATH ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp; লোড হচ্ছে  <b>' + office_name[cnt] + '</b> । একটু অপেক্ষা করুন... </span><br>');
            var office_id = office_ids[cnt];
            $.ajax({
                type: 'POST',
                url: "<?php echo $this->Url->build(['controller' => 'Cron', 'action' => 'reportTillNowContent']) ?>",
                //                dataType: 'json',
                data: {"office_id": office_id},
                success: function (data) {
                    if (data.status == 1)
                    {
                        TotalOffices++;
                        nisponno = nisponno + data.nisponno;
                        potrojari = potrojari + data.potrojari;
                        nagorik_dak_online = nagorik_dak_online + data.portojari_receiver;
                        nagorik_dak_portal = nagorik_dak_portal +data.potrojari_onulipi;
                        portojari_receiver = portojari_receiver + data.nagorik_dak_online;
                        potrojari_onulipi = potrojari_onulipi + data.nagorik_dak_portal;
                        nagorik_total = nagorik_total + data.nagorik_total;
                        html = '<tr class="text-center">' +
                                '<td>' + office_name[cnt] + '</td>' +
                                ' <td>' + BntoEng(data.nisponno) + '</td>' +
                                '<td>' + BntoEng(data.potrojari) + '</td>' +
                                '<td>' + BntoEng(data.portojari_receiver) + '</td>' +
                                '<td>' + BntoEng(data.potrojari_onulipi) + '</td>' +
                                '<td>' + BntoEng(data.nagorik_dak_online) + '</td>' +
                                '<td>' + BntoEng(data.nagorik_dak_portal) + '</td>' +
                                '<td>' + BntoEng(data.nagorik_total) + '</td>' +
                                '</tr>';
                        $('#addData').append(html);
                    }
                    callOffices(cnt + 1);
                },
                error: function(){
                    callOffices(cnt + 1);
                }
            });
//                break;
//        }

        }
        function showTotal(){
            html = '<tr class="text-center bold">' +
                                '<td><b>মোট</b> (' + BntoEng(TotalOffices) + ')</td>' +
                                ' <td>' + BntoEng(nisponno) + '</td>' +
                                '<td>' + BntoEng(potrojari) + '</td>' +
                                '<td>' + BntoEng(nagorik_dak_online) + '</td>' +
                                '<td>' + BntoEng(nagorik_dak_portal) + '</td>' +
                                '<td>' + BntoEng(portojari_receiver) + '</td>' +
                                '<td>' + BntoEng(potrojari_onulipi) + '</td>' +
                                '<td>' + BntoEng(nagorik_total) + '</td>' +
                                '</tr>';
            $('#addData').append(html);
        }
    });


    function BntoEng(input) {
    return input;
        var numbers = {
            0: '০',
            1: '১',
            2: '২',
            3: '৩',
            4: '৪',
            5: '৫',
            6: '৬',
            7: '৭',
            8: '৮',
            9: '৯'
        };

        var output = '';
        if (parseInt(input) != 0) {
            while (parseInt(input) != 0) {
                var n = parseInt(input % 10);
                if (!Number.isInteger(n))
                    break;
                input = input / 10;
                output += (numbers[n]);
            }
        } else
        {
            output = '০';
        }
        for (var i = output.length - 1, o = ''; i >= 0; i--)
        {
            o += output[i];
        }
        return o;

    }
</script>

