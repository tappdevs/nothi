<table class="table table-bordered table-hover tableadvance">
    <thead>
        <tr class="heading">
            <th class="text-center" colspan="2">  পুরাতন শাখা</th>
            <th class="text-center" colspan="2">  পরিবর্তীত শাখা </th>
            <th class="text-center" rowspan="2">  শাখা </th>
            <th class="text-center" rowspan="2">  অফিস </th>
            <th class="text-center" rowspan="2">  তারিখ </th>
        </tr>
        <tr class="heading">
            <th class="text-center" > ইংরেজি </th>
            <th class="text-center" > বাংলা </th>
            <th class="text-center" > ইংরেজি </th>
            <th class="text-center" > বাংলা </th>
             
        </tr>
    </thead>
    <tbody >
        <?php
            if(!empty($unitHistory)){
                $i= 0;
                foreach ($unitHistory as $val){
                    $i =1;
            ?>
            <tr>
                <td class="text-center"><?= $val['old_unit_eng']?> </td>
                <td class="text-center"><?= $val['old_unit_bng']?> </td>
                <td class="text-center"><?= $val['unit_eng']?> </td>
                <td class="text-center"><?= $val['unit_bng']?> </td>
                <td class="text-center"><?= $val['Offices']['office_name_bng']?> </td>
                <td class="text-center"><?= $val['OfficeUnits']['unit_name_bng']?> </td>
                <td class="text-center"><?= $val['modified']?> </td>
            </tr>
            <?php
                }
        }
        if($i == 0) {
            ?>
            <tr><td colspan="7" class="text-center"> কোন তথ্য পাওয়া যায়নি। </td></tr>
            <?php
        }
        ?>
    </tbody>
</table>
<div class="actions text-center">
    <ul class="pagination pagination-sm">
        <?php
        echo $this->Paginator->first(__('প্রথম', true), array('class' => 'number-first'));
        echo $this->Paginator->prev('<<', array('tag' => 'li', 'escape' => false), '<a href="#" class="btn btn-sm blue">&laquo;</a>', array('class' => 'prev disabled btn btn-sm blue', 'tag' => 'li', 'escape' => false));
        echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a', 'reverse' => FALSE));
        echo $this->Paginator->next('>>', array('tag' => 'li', 'escape' => false), '<a href="#" class="btn btn-sm blue ">&raquo;</a>', array('class' => 'next disabled', 'tag' => 'li', 'escape' => false));
        echo $this->Paginator->last(__('শেষ', true), array('class' => 'number-last'));
        ?>
    </ul>
<script>
    $(document).ready(function(){
         $(document).off('click','.pagination a').on('click','.pagination a',function(ev){
            ev.preventDefault();
            PROJAPOTI.ajaxSubmitAsyncDataCallback($(this).attr('href'),{'office_id': $('#office-id').val(),'end_date' : $('.enddate').val(),'start_date' : $('.startdate').val()},'html',function(response){
               $('#showlist').html(response);
            });
        });
    });
</script>