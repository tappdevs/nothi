<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            কাস্টম অফিস লেয়ার
        </div>
        <div class="actions">
            <a href="<?php echo $this->Url->build(['controller' => 'CustomOfficeLayers', 'action' => 'mapInfos']) ?>" class="btn btn-sm btn-warning"><i class="fa fa-wrench"></i>  কাস্টম স্তর - অফিস ম্যাপিং</a>
            <a href="<?php echo $this->Url->build(['controller' => 'CustomOfficeLayers', 'action' => 'ministryWiseMap']) ?>" class="btn btn-sm btn-danger"><i class="fa fa-wrench"></i>  মন্ত্রণালয় ভিত্তিক কাস্টম স্তর ম্যাপিং</a>
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-scrollable">
            <table class="table table-hover table-bordered" id="datatable_table">
                <thead>
                <tr>
                    <th style="width:10%;" class="text-center" >
                        নম্বর
                    </th>
                    <th class="text-center heading" style="width:65%;">
                        কাস্টম লেয়ারসমূহ
                    </th>
                    <th class="text-center heading" style="width:65%;">
                        কাস্টম লেয়ারসমূহ (ইংরেজি)
                    </th>
                    <th class="text-center heading" style="width:65%;">
                        লেয়ার লেভেল
                    </th>
                    <th class="text-center">
                        <a class="btn   green btn-sm" style="text-decoration: none;padding-left: 10px;" data-toggle="modal" data-target="#myModal"><i class="fs1 a2i_gn_add1"></i>&nbsp;&nbsp;নতুন কাস্টম লেয়ার</a>
                    </th>
                </tr>
                </thead>
                <tbody>

                <?php
                $i = 0;
                if(!empty($data)){
                    foreach ($data as $key => $val) {
                        $i++;
                        ?>
                        <tr>
                            <td class="text-center"><?= $this->Number->format($i); ?></td>
                            <td id="update_<?= $i ?>" class="" style="font-size: 14px!important;"><?= h($val['name']); ?></td>
                            <td id="update_eng_<?= $i ?>" class="" style="font-size: 14px!important;"><?= h($val['name_eng']); ?></td>
                            <td id="update_layer_level_<?= $i ?>" class="" style="font-size: 14px!important;"><?= h($val['layer_level']); ?></td>
                            <td class="text-center">
                                <?php
                                if ($val['created_by'] == $loggedUser['id']) {
                                    ?>
                                    <a data-toggle="modal" title="সম্পাদনা" data-target="#yourModal" onclick="update('<?= makeEncryptedData($val['id']) ?>',<?=$i?>);"><i class="fa fa-edit"></i></a>&nbsp;&nbsp;&nbsp;
                                    <a data-toggle="modal" title="মুছে ফেলুন" data-target="#deleteModal" onclick="del('<?= makeEncryptedData($val['id']) ?>',<?=$i?>);"><i class="fa fa-times"></i></a>
                                    <?php
                                }
                                ?>

                            </td>
                        </tr>
                        <?php
                    }
                }
                ?>

                </tbody>
            </table>

        </div>
        <div class="actions text-center">
            <ul class="pagination pagination-sm">
                <?php
                echo $this->Paginator->first(__('প্রথম', true), array('class' => 'number-first'));
                echo $this->Paginator->prev('<<', array('tag' => 'li', 'escape' => false), '<a href="#" class="btn btn-sm blue">&laquo;</a>', array('class' => 'prev disabled btn btn-sm blue', 'tag' => 'li', 'escape' => false));
                echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a', 'reverse' => FALSE));
                echo $this->Paginator->next('>>', array('tag' => 'li', 'escape' => false), '<a href="#" class="btn btn-sm blue ">&raquo;</a>', array('class' => 'next disabled', 'tag' => 'li', 'escape' => false));
                echo $this->Paginator->last(__('শেষ', true), array('class' => 'number-last'));
                ?>
            </ul>

        </div>
        <div class="modal fade modal-purple height-auto" data-backdrop="static" id="myModal" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">নতুন কাস্টম অফিস লেয়ার</h4>
                    </div>
                    <div class="modal-body">
                        <form class="form-group">
                            <input type="text" class="form-control" id="save_text" placeholder="কাস্টম লেয়ার নাম (বাংলায়)">
                            <input type="text" class="form-control" id="save_text_eng" placeholder="কাস্টম লেয়ার নাম (ইংরেজিতে)">
                            <input type="number" class="form-control" id="save_layer_level" placeholder="লেয়ার লেভেল">
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" onclick="save_action();">সংরক্ষণ</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">বন্ধ করুন</button>
                    </div>
                </div>

            </div>
        </div>
        <div class="modal fade modal-purple height-auto" data-backdrop="static" id="yourModal" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">কাস্টম অফিস লেয়ার সংশোধন</h4>
                    </div>
                    <div class="modal-body">
                        <form class="form-group">
                            <input type="text" class="form-control" id="update_text" placeholder="কাস্টম লেয়ার নাম (বাংলায়)">
                            <input type="text" class="form-control" id="update_text_eng" placeholder="কাস্টম লেয়ার নাম (ইংরেজিতে)">
                            <input type="number" class="form-control" id="update_layer_level" placeholder="লেয়ার লেভেল">
                            <input type="hidden" class="form-control" id="update_id">
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" onclick="update_action();">সংরক্ষণ</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">বন্ধ করুন</button>
                    </div>
                </div>

            </div>
        </div>
        <div class="modal fade modal-purple height-auto" data-backdrop="static" id="deleteModal" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"> কাস্টম অফিস লেয়ার বাতিল</h4>
                    </div>
                    <div class="modal-body">
                        <form class="form-group">
                            <label>আপনি কি কাস্টম অফিস লেয়ারটি মুছে ফেলতে চান?</label>
                            <input type="hidden" class="form-control" id="delete_id">
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" onclick="delete_action();">মুছে ফেলুন</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">বন্ধ করুন</button>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

    function update(id,serial)
    {
        var text = $('#update_' + serial + '').text();
        var text_eng = $('#update_eng_' + serial + '').text();
        $('#update_text').val(text);
        $('#update_text_eng').val(text_eng);
        $('#update_id').val(id);
    }
    function del(id)
    {
        $('#delete_id').val(id);
    }
    function save_action()
    {
        var text = $('#save_text').val();
        var text_eng = $('#save_text_eng').val();
        var layer_level = $('#save_layer_level').val();
        $.ajax({
            type: 'POST',
            url: "<?php echo $this->Url->build(['controller' => 'CustomOfficeLayers', 'action' => 'add']) ?>",
            data: {"text": text, "text_eng": text_eng, "layer_level": layer_level},
            success: function (data) {
                if(data.status == 'success'){
                    window.location.reload();
                }else{
                    toastr.error(data.msg);
                }

            }
        });
    }
    function update_action()
    {
        var text = $('#update_text').val();
        var text_eng = $('#update_text_eng').val();
        var layer_level = $('#update_layer_level').val();
        var id = $('#update_id').val();
//       console.log(text+':'+id);
        $.ajax({
            type: 'POST',
            url: "<?php echo $this->Url->build(['controller' => 'CustomOfficeLayers', 'action' => 'update']) ?>",
            data: {"text": text, "text_eng": text_eng, "id": id, "layer_level": layer_level},
            success: function (data) {
                if(data.status == 'success'){
                    window.location.reload();
                }else{
                    toastr.error(data.msg);
                }
            }
        });
    }
    function delete_action()
    {
        var id = $('#delete_id').val();
//       console.log(id);
        $.ajax({
            type: 'POST',
            url: "<?=$this->Url->build(['controller' => 'CustomOfficeLayers', 'action' => 'delete']) ?>",
            data: {"id": id},
            success: function (data) {
                if(data.status == 'success'){
                    window.location.reload();
                }else{
                    toastr.error(data.msg);
                }
            }
        });
    }
</script>
