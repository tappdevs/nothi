<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            কাস্টম স্তর - অফিস ম্যাপিং
        </div>
        <div class="actions">
            <a href="<?php echo $this->Url->build(['controller' => 'CustomOfficeLayers', 'action' => 'index']) ?>" class="btn btn-sm btn-warning"><i class="fa fa-plus"></i>  কাস্টম স্তর যুক্তকরণ</a>
            <a href="<?php echo $this->Url->build(['controller' => 'CustomOfficeLayers', 'action' => 'ministryWiseMap']) ?>" class="btn btn-sm btn-danger"><i class="fa fa-wrench"></i>  মন্ত্রণালয় ভিত্তিক কাস্টম স্তর ম্যাপিং</a>
        </div>
    </div>
    <div class="portlet-body">
        <div class="row">
            <div class="col-md-2">
                <label class="pull-right form-label-stripped"> কাস্টম স্তরঃ</label>
            </div>
            <div class="col-md-4 form-group form-horizontal">
                <?php
                echo $this->Form->input('custom_layer_id',
                    array('empty' => '--বাছাই করুন--', 'type' => 'select', 'label' => false, 'class' => 'form-control',
                          'options' => $data+[makeEncryptedData(0)=> 'কাস্টম স্তর বিহীন'],'default' => isset($custom_layer_level)?$custom_layer_level:''));
                ?>
            </div>
        </div>
        <br>
        <br>
        <div class="row" id="showlist">

        </div>
        <div class="row">
            <div class="table-container">
                <table class="table table-bordered table-hover tableadvance">
                    <thead>
                    <tr class="heading">
                        <th class="text-center" > ক্রম </th>
                        <th class="text-center" >অফিসের নাম</th>
                        <th class="text-center" > কাস্টম স্তর </th>
                        <th class="text-center" > কার্যক্রম </th>
                    </tr>
                    </thead>
                    <tbody id ="addData">

                    </tbody>
                </table>
            </div>

            <!--Tools end -->

        </div>
        <br/>
    </div>
</div>
<script>
    $(document).ready(function(){
        $("#custom-layer-id").trigger('change');
    });
    $("#custom-layer-id").bind('change', function () {
        if(!isEmpty($(this).val())){
            getOfficesInfo($(this).val());
        }
        $('#addData').html('');

    });
    function getOfficesInfo(office_layer_id){
        $('#showlist').html('<img src="<?= CDN_PATH ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp; লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');
        $.ajax({
            type: 'POST',
            url: "<?php echo $this->Url->build(['controller' => 'CustomOfficeLayers', 'action' => 'getCustomLayerWiseOffices']) ?>",
            dataType: 'json',
            data: {"custom_office_layer_type": office_layer_id},
            success: function (data) {
                if(!(isEmpty(data)) && data.status == 'success' && !(isEmpty(data.data))){
                    var redirect_url = js_wb_root+"CustomOfficeLayers/mapInfos?custom_layer_level="+office_layer_id;
                    $.each(data.data, function (i, v) {
                        var edit_url = "<?php echo $this->Url->build(['controller' => 'cron', 'action' => 'layerOfficesLayerEdit']) ?>"+'/'+v.id+'?redirect='+encodeURIComponent(redirect_url);
                        toAdd = '<tr>' +
                            '<td class="text-center" >' + BnFromEng(i+1) + '</td> ' +
                            '<td class="text-center" >' + v.office_name_bng + '</td> ' +
                            '<td class="text-center"> <b>' + v.custom_layer_name + '</b></td>' +
                            '<td class="text-center"><a href="'+edit_url+'" class="btn btn-xs btn-primary">সম্পাদনা</a></td>' +
                            '</tr>';
                        $('#addData').append(toAdd);
//
                    });
                    $('#showlist').html('');
                    return false;
                } else {
                    toAdd = '<tr>' +
                        '<td class="text-center" colspan="4"><span style="color: red"> দুঃখিত! কোন তথ্য পাওয়া যায় নি। </span></td> ' +
                        '</tr>';
                    $('#addData').append(toAdd);
                    $('#showlist').html('');
                }
            }
        });
    }
</script>