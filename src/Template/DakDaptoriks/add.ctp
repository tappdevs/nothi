<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fs1 a2i_gn_edit2"></i>Dak Daptorik Register
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"></a> <a
                href="#portlet-config" data-toggle="modal" class="config"></a> <a
                href="javascript:;" class="reload"></a> <a href="javascript:"
                class="remove"></a>
        </div>
    </div>
    <div class="portlet-body form">
        <!--Start: Common Fields required only for create action -->
        <?php
        echo $this->Form->create('', ['type' => 'file']);
        echo $this->Form->hidden('dak_type', array('label' => false, 'class' => 'form-control', 'value' => DAK_DAPTORIK));
        echo $this->Form->hidden('dak_status', array('label' => false, 'class' => 'form-control', 'value' => 1));
        // Dak Status-> 0:draft, 1:Save, 2:Send and others.....
        ?>
        <!--End: Common Fields required only for create action -->

        <!--Start: Remaining Form elements -->
<?php echo $this->element('DakDaptoriks/dak_form'); ?>
        <!--End: Remaining Form elements -->

        <!--Start: Form Buttons -->
        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-4 col-md-9">
                    <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-primary']) ?>
<?= $this->Form->button(__('Cancel'), ['class' => 'btn btn-danger']) ?>
                </div>
            </div>
        </div>
        <!--End: Form Buttons -->
<?php echo $this->Form->end(); ?>
    </div>
</div>

<!-- Start: JavaScript -->
<!--START SCRIPT FOR THE TINYEDITOR-->
<script src="<?php echo CDN_PATH; ?>/js/tinymce/tinymce.min.js"></script>
<!--END SCRIPT FOR THE TINYEDITOR-->

<!-- Start: Common dak setup js -->
<script src="<?php echo CDN_PATH; ?>assets/admin/layout4/scripts/dak_setup.js"
type="text/javascript"></script>
<script type="text/javascript">
    $(function () {
        DakSetup.init();
    });
</script>
<!-- End: Common dak setup js -->

<script type="text/javascript">
    tinymce.init({//script for the tiny editor
        selector: "textarea",
        statusbar: false,
        menubar: false,
        plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste"
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | fontselect",
        font_formats: "Nikosh='Nikosh','Open Sans', sans-serif;Arial=arial;Helvetica=helvetica,Sans-serif=sans-serif;Courier New=courier new;Courier=courier,Monospace=monospace;Comic Sans MS=comic sans ms;Times New Roman=times new roman,times;Kalpurush=kalpurush;Siyamrupali=Siyamrupali;SolaimanLipi=SolaimanLipi;",
        table_default_border: "1",
        table_default_attributes: {
            width: "30%",
            cellpadding: "2",
            cellspacing: "0",
            border: "1"
        },
        table_default_styles: {
            border: "1px solid #000;"
        },
        paste_retain_style_properties: "all",
    });

</script>
<!-- End: JavaScript -->