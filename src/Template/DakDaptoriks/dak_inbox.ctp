<style>
    .inbox .inbox-nav li.active b {
        display: none !important;
    }

    .tooltip-inner {
        white-space: pre-line;
    }
</style>
<div class="portlet light">
    <div class="portlet-body">
        <div class="row inbox">

            <div class="col-md-12">

                <div class="inbox-loading" style="display: none;"><img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp;লোড হচ্ছে। একটু অপেক্ষা করুন... </span></div>
                <div class="inbox-content">

                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo CDN_PATH; ?>daptorik_preview/js/draft_dak_management.js" type="text/javascript"></script>

<div class="modal fade modal-purple height-auto" id="ajax" role="basic" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
                <div class="modal-title">আবেদনের রশিদ</div>
            </div>
            <div class="modal-body">
                <img src="<?php echo $this->request->webroot; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span> &nbsp;&nbsp;লোড করা হচ্ছে... </span>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        DraftDakMovement.init('<?php echo $dak_inbox_group; ?>');

        <?php if (!empty($sentdak)): ?>
        var link = '<?php echo $this->Url->build(['controller' => 'dakDaptoriks', 'action' => 'dakReceipt', (!empty($sentdak) ? $sentdak : 0)]); ?>';
        $("#ajax").modal('show').find(".modal-content").load(link);
        <?php endif; ?>
    });
</script>