<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fs1 a2i_gn_edit2"></i>Dak Daptorik Register
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"></a> <a
                href="#portlet-config" data-toggle="modal" class="config"></a> <a
                href="javascript:;" class="reload"></a> <a href="javascript:"
                class="remove"></a>
        </div>
    </div>
    <div class="portlet-body form">
        <div class="row">
            <div class=" col-md-6">
                <div class="">
                    <label class="control-label"> Description </label>
                    <textarea name="editor_content" id="myEditor"></textarea>
                </div>
            </div>
        </div>
        
    </div>
</div>
<button id="getData">Get Html</button>
</br>
<div id="html_data">Data should show here</div>
<br>
<button id="putData">Put Html</button>
<?= $this->element('froala_editor_js_css') ?>


<style>
    .fr-view{
        overflow: scroll;
    }
    #html_data{
        border-style: solid;
    }
    table th, table td {
        padding-bottom: 2px;
        padding-left: 5px;
        padding-right: 5px;
        padding-top: 2px;
        vertical-align: middle;
    }
    table{
        table-layout: fixed;
    }
    table td{
        width: auto;
        overflow: hidden;
        word-wrap: break-word;
    }
    h1, h3, h2{
        margin-bottom: 0px;
        margin-top: 0px;
    }
</style>
<script type="text/javascript">
    $.FroalaEditor.DefineIcon('বিবেচ্য পত্র', {NAME: 'envelope'});
    $.FroalaEditor.RegisterCommand('বিবেচ্য পত্র', {
        title: 'বিবেচ্য পত্র বাছাই করুন',
        type: 'dropdown',
        focus: false,
        undo: false,
        refreshAfterCallback: true,
        options: {
            'v1': 'Option 1',
            'v2': 'Option 2'
        },
        callback: function (cmd, val) {
            console.log (val);
        },
        // Callback on refresh.
        refresh: function ($btn) {
            console.log ('do refresh');
        },
        // Callback on dropdown show.
        refreshOnShow: function ($btn, $dropdown) {
            console.log ('do refresh when show');
        }
    });

    $.FroalaEditor.DefineIcon('পতাকা', {NAME: 'flag'});
    $.FroalaEditor.RegisterCommand('পতাকা', {
        title: 'পতাকা বাছাই করুন',
        type: 'dropdown',
        focus: false,
        undo: false,
        refreshAfterCallback: true,
        options: {
            'v1': 'Option 1',
            'v2': 'Option 2'
        },
        callback: function (cmd, val) {
            console.log (val);
        },
        // Callback on refresh.
        refresh: function ($btn) {
            console.log ('do refresh');
        },
        // Callback on dropdown show.
        refreshOnShow: function ($btn, $dropdown) {
            console.log ('do refresh when show');
        }
    });

    $.FroalaEditor.DefineIcon('অনুচ্ছেদ', {NAME: 'pencil-square-o'});
    $.FroalaEditor.RegisterCommand('অনুচ্ছেদ', {
        title: 'অনুচ্ছেদ বাছাই করুন',
        type: 'dropdown',
        focus: false,
        undo: false,
        refreshAfterCallback: true,
        options: {
            'v1': 'Option 1',
            'v2': 'Option 2'
        },
        callback: function (cmd, val) {
            console.log (val);
        },
        // Callback on refresh.
        refresh: function ($btn) {
            console.log ('do refresh');
        },
        // Callback on dropdown show.
        refreshOnShow: function ($btn, $dropdown) {
            console.log ('do refresh when show');
        }
    });

    $.FroalaEditor.DefineIcon('সংযুক্ত-রেফ', {NAME: 'paperclip'});
    $.FroalaEditor.RegisterCommand('সংযুক্ত-রেফ', {
        title: 'সংযুক্ত-রেফ বাছাই করুন',
        type: 'dropdown',
        focus: false,
        undo: false,
        refreshAfterCallback: true,
        options: {
            'v1': 'Option 1',
            'v2': 'Option 2'
        },
        callback: function (cmd, val) {
            console.log (val);
        },
        // Callback on refresh.
        refresh: function ($btn) {
            console.log ('do refresh');
        },
        // Callback on dropdown show.
        refreshOnShow: function ($btn, $dropdown) {
            console.log ('do refresh when show');
        }
    });

    $.FroalaEditor.DefineIcon('সিদ্ধান্ত', {NAME: 'gavel'});
    $.FroalaEditor.RegisterCommand('সিদ্ধান্ত', {
        title: 'সিদ্ধান্ত বাছাই করুন',
        type: 'dropdown',
        focus: false,
        undo: false,
        refreshAfterCallback: true,
        options: {
            'v1': 'Option 1',
            'v2': 'Option 2'
        },
        callback: function (cmd, val) {
            console.log (val);
        },
        // Callback on refresh.
        refresh: function ($btn) {
            console.log ('do refresh');
        },
        // Callback on dropdown show.
        refreshOnShow: function ($btn, $dropdown) {
            console.log ('do refresh when show');
        }
    });

    $.FroalaEditor.DefineIconTemplate('material_design', '<i class="fs0 [NAME]"></i>');
    $.FroalaEditor.DefineIcon('গার্ড ফাইল', {NAME: 'fs0 efile-guard_file1', template: 'material_design'});
    $.FroalaEditor.RegisterCommand('গার্ড ফাইল', {
        title: 'গার্ড ফাইল',
        focus: false,
        undo: false,
        showOnMobile: true,
        refreshAfterCallback: false,
        callback: function () {
            $('#responsiveModal').modal('show');
            $('#responsiveModal').find('.modal-title').text('গার্ড ফাইল');
            $('#selectpage').val('');
            var grid = new Datatable();

            grid.init({
                "destroy": true,
                src: $('#filelist'),
                onSuccess: function (grid) {
                    // execute some code after table records loaded
                },
                onError: function (grid) {
                    // execute some code on network or other general error
                },
                onDataLoad: function (grid) {
                    // execute some code on ajax data load

                },
                loadingMessage: 'লোড করা হচ্ছে...',
                dataTable: {
                    "destroy": true,
                    "scrollY": "500",
                    "dom": "<'row'<'col-md-8 col-sm-12'li><'col-md-4 col-sm-12'<'table-group-actions pull-right'p>>r>t",
                    "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

                    "lengthMenu": [
                        [10, 20, 50, 100, 500],
                        ["১০", "২০", "৫০", "১০০", "৫০০"] // change per page values here
                    ],
                    "pageLength": 50, // default record count per page
                    "ajax": {
                        "url": js_wb_root + 'GuardFiles/officeGuardFile', // ajax source
                    },
                    "bSort": false,
                    "ordering": false,
                    "language": {// language settings
                        // metronic spesific
                        "metronicGroupActions": "_TOTAL_ রেকর্ড নির্বাচন করা হয়েছে:  ",
                        "metronicAjaxRequestGeneralError": "অনুরোধ সম্মন্ন করা সম্ভব হচ্ছে না।",
                        // data tables spesific
                        "lengthMenu": "<span class='seperator'></span>দেখুন _MENU_ ধরন",
                        "info": "<span class='seperator'>|</span>মোট _TOTAL_ টি পাওয়া গেছে",
                        "infoEmpty": "",
                        "emptyTable": "কোনো রেকর্ড  নেই",
                        "zeroRecords": "কোনো রেকর্ড  নেই",
                        "paginate": {
                            "previous": "আগেরটা",
                            "next": "পরবর্তীটা",
                            "last": "শেষেরটা",
                            "first": "প্রথমটা",
                            "page": "পাতা",
                            "pageOf": "এর"
                        }
                    }
                }
            });
            $(document).off('click', '.showDetailAttach').on('click', '.showDetailAttach', function (ev) {
                ev.preventDefault();
                getPopUpPotro($(this).attr('href'), $(this).attr('data-title'));
            });
            $(document).off('click', '.tagfile').on('click', '.tagfile', function (ev) {
                ev.preventDefault();
                var href = $(this).closest('div').find('.showDetailAttach').attr('href');
                var value = $(this).closest('div').find('.showDetailAttach').attr('data-title');

                bootbox.dialog({
                    message: "বাছাইকৃত ফাইলটির কোন পেজটি রেফারেন্স করতে চান?<br/><div class='form-group'><input type='text' class='input-sm form-control' name='selectpage' id='selectpage' placeholder='সকল পেজ' /></div>",
                    title: "গার্ড ফাইল রেফারেন্স",
                    buttons: {
                        success: {
                            label: "হ্যাঁ",
                            className: "green",
                            callback: function () {
                                var page = $('#selectpage').val();
                                $('#noteComposer').froalaEditor('html.insert', " বিবেচ্য গার্ড ফাইল  <a class='showforPopup'  href='" + href + "/" + page + "' title=' গার্ড ফাইল'>" + value + "</a>, &nbsp;")
                                $('#responsiveModal').modal('hide');
                            }
                        },
                        danger: {
                            label: "না",
                            className: "red",
                            callback: function () {
                                $('#selectpage').val('');
                            }
                        }
                    }
                });
            });
        }
    });

    var buttons = ['bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', '|', 'fontFamily', 'fontSize', 'color', 'inlineStyle', 'paragraphStyle', '|', 'paragraphFormat', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote', 'insertLink', 'insertTable', '|', 'emoticons', 'insertHR', 'selectAll', 'clearFormatting', '|', 'print', 'undo', 'redo', '|','গার্ড ফাইল','বিবেচ্য পত্র','পতাকা','অনুচ্ছেদ','সংযুক্ত-রেফ','সিদ্ধান্ত'];
var x = screen.height;
var y = screen.width;
    var editor = $('#myEditor').froalaEditor({
        key: 'xc1We1KYi1Ta1WId1CVd1F==',
        toolbarSticky: false,
        wordPasteModal: true,
		wordAllowedStyleProps: ['font-size', 'background', 'color', 'text-align', 'vertical-align', 'background-color', 'padding', 'margin', 'height', 'margin-top', 'margin-left', 'margin-right', 'margin-bottom', 'text-decoration', 'font-weight'],
		wordDeniedTags: ['a','form'],
		wordDeniedAttrs: ['width'],
        tableResizerOffset: 10,
        tableResizingLimit: 20,
        toolbarButtons: buttons,
        toolbarButtonsMD: buttons,
        toolbarButtonsSM: buttons,
        toolbarButtonsXS: buttons,
        placeholderText: '',
        height: 400,
        // width: y,
        enter: $.FroalaEditor.ENTER_BR,
        fontSize: ['10','11', '12','13', '14','15','16','17', '18','19','20','21','22','23','24','25','26','27','28','29', '30'],
        fontSizeDefaultSelection: '13',
        tableEditButtons:['tableHeader', 'tableRemove', '|', 'tableRows', 'tableColumns','tableCells', '-',  'tableCellBackground', 'tableCellVerticalAlign', 'tableCellHorizontalAlign', 'tableCellStyle'],
        fontFamily: {
            "Nikosh": "Nikosh",
            "Arial": "Arial",
            "Kalpurush": "Kalpurush",
            "SolaimanLipi": "SolaimanLipi",
            "'times new roman'": "Times New Roman",
            "'Open Sans Condensed',sans-serif": 'Open Sans Condensed'
        }
    });
    $('#getData').on('click',function () {
        var data=  $('#myEditor').froalaEditor('html.get');
        $('#html_data').html(data);
        $('#myEditor').froalaEditor('html.set','');
    });
    $('#putData').on('click',function () {
        var data=$('#html_data').html();
        $('#myEditor').froalaEditor('html.set',data);
        $('#html_data').html('');
    });

</script>


