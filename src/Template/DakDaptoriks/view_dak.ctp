<style>
    .pager {
        margin-top: 0px;
    }

    .inbox .inbox-header h3 {
        margin-top: 0px;
    }

    .checkbox input[type=checkbox], .checkbox-inline input[type=checkbox], .radio input[type=radio], .radio-inline input[type=radio] {
        margin-left: -10px !important;
    }

    .editable {
        border: none;
        word-break: break-word;
    }
</style>
<div class="inbox-header inbox-view-header">
    <div class="pull-left ">
        <button class="btn btn-default btn-sm draft-discard-btn"><i
                class="glyphicon glyphicon-arrow-left"></i> <?php if ($dak_daptoriks['dak_status'] == DAK_CATEGORY_DRAFT) echo "খসড়া "; else echo "প্রেরিত খসড়া  "; ?>
            ডাকসমূহ
        </button>
    </div>
</div>
<div class="inbox-header inbox-view-info">
    <h3 class=""  >
        <div class="row">
            <div class="col-md-1 col-sm-2" style="padding-right:0;font-weight: bold;">
                বিষয়:
            </div>
          <div class="col-md-8 col-sm-8" style="word-break:break-word;">
                <?php echo h($dak_daptoriks['dak_subject']); ?>
            </div>
            <div class="col-md-3 col-sm-2 text-right">
                    <?php
                       echo '<button class="btn  btn-sm" data-title="গোপনীয়তা: ' . h($dak_security_txt) . '" title="গোপনীয়তা: ' . h($dak_security_txt) . '" ><i title="' . h($dak_security_txt) . '" class="glyphicon glyphicon glyphicon-lock" ' . ($dak_daptoriks['dak_security_level'] < 2 ? '' : ($dak_daptoriks['dak_security_level'] == 2 ? 'style="color:#FFCD32;"' : ($dak_daptoriks['dak_security_level'] == 3 ? 'style="color:red;"' : ($dak_daptoriks['dak_security_level'] == 4 ? 'style="color:#881C1C;"' : 'style="color:orange;"')))) . '> </i></button>';
        echo '<button class="btn  btn-sm" title="অগ্রাধিকার: ' . $dak_priority_txt . '"  data-title="অগ্রাধিকার: ' . $dak_priority_txt . '" ><i title="' . $dak_priority_txt . '" class="glyphicon glyphicon glyphicon-star" ' . ($dak_priority < 2 ? '' : ($dak_priority == 2 ? 'style="color:#FFCD32;"' : ($dak_priority == 3 ? 'style="color:green;"' : 'style="color:red;"'))) . '> </i></button>';
                    ?>

            <?php if ($dak_daptoriks['dak_status'] == DAK_CATEGORY_DRAFT) { ?>
                    <button class="btn   btn-sm dak_draft_send_btn_single"
                    data-dak-id="<?php echo $dak_daptoriks['id'] ?>"><i class="a2i_nt_cholomandak2"> </i>
                    </button>
                    <?php echo $this->Form->hidden('selected_dak_ids', array('id' => 'selected_dak_ids')) ?>
            <?php } ?>
        </div>
        </div>
    </h3>

</div>
<div class="inbox-view-info" style="vertical-align: middle;margin-bottom: 5px;padding-left: 5px; padding-right: 5px;">
    <div class="row">
        <div class="col-md-9 col-sm-9">
            <i class="icon-send"></i>
            <span
                class="bold"><?php echo h($sender) ?></span>
        </div>
         <div class="col-md-3 col-sm-3 text-right">
            <?php echo (isset($dak_attachments) && count($dak_attachments) > 0) ? '<i class="glyphicon glyphicon-paperclip"> </i>' : ''; ?> <?php echo h($dak_daptoriks['sending_date']); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-7 col-sm-7">
            <i class="icon-send"></i>
            <span class="bold"><?php echo $receiver_office ?></span>
        </div>
    </div>
    <div class="row">
        <div class="col-md-7 col-sm-7">
            <span class=""><?php echo $onulipiUser; ?></span>
        </div>
    </div>
</div>


<?php
$attachmentGroup = array();
$attachmentGroupimage = array();
if (isset($dak_attachments) && count($dak_attachments) > 0) {
    $i = 1;
    foreach ($dak_attachments as $single_data) {
        $file_name = $single_data['file_name'];
        $file_name = explode("/", $file_name);
        $file_name = $file_name[count($file_name) - 1];
        if($single_data['attachment_type']=='text')continue;
        $single_data['attachment_type'] = get_file_type($single_data['file_name']);
        $f_type = explode('/', $single_data['attachment_type']);
        $file_name_arr[0] = urldecode($file_name);
        $file_name_arr[1] = $f_type[1];
        
        $single_data['file_name'] = $single_data['file_name'];
        
        if (isset($file_name_arr[1])) {
            if (!empty($single_data['file_name'])) {
                if ($file_name_arr[1] == 'jpg' || $file_name_arr[1] == 'jpeg' || $file_name_arr[1] == 'png') {
                    $attachmentGroupimage['image'][] = array("file_type" => $single_data['attachment_type'], "file_name" => $single_data['file_name'], "name" => $file_name_arr[0], 'id' => $single_data['id'],'user_file_name' => h($single_data['user_file_name']));
                } elseif ($file_name_arr[1] == 'mp3' || $file_name_arr[1] == 'mpeg') {
                    $attachmentGroup['mp3'][] = array("file_type" => $single_data['attachment_type'], "file_name" => $single_data['file_name'], "name" => $file_name_arr[0], 'id' => $single_data['id'],'user_file_name' => h($single_data['user_file_name']));
                } elseif ($file_name_arr[1] == 'wmv' || $file_name_arr[1] == 'mp4' || $file_name_arr[1] == 'mkv' || $file_name_arr[1] == 'avi' || $file_name_arr[1] == '3gp' || $file_name_arr[1] == 'mpg') {
                    $attachmentGroup['video'][] = array("file_type" => $single_data['attachment_type'], "file_name" => $single_data['file_name'], "name" => $file_name_arr[0], 'id' => $single_data['id'],'user_file_name' => h($single_data['user_file_name']));
                } elseif ($file_name_arr[1] == 'pdf') {
                    $attachmentGroup['pdf'][] = array("file_type" => $single_data['attachment_type'], "file_name" => $single_data['file_name'], "name" => $file_name_arr[0], 'id' => $single_data['id'],'user_file_name' => h($single_data['user_file_name']));
                }
                else
                    $attachmentGroup["other"][] = array("file_type" => $single_data['attachment_type'], "file_name" => $single_data['file_name'], "name" => $file_name_arr[0], 'id' => $single_data['id'],'user_file_name' => h($single_data['user_file_name']));

            }
        } else {
            if (!empty($single_data['file_name'])) $attachmentGroup["other"][] = array("file_type" => $single_data['attachment_type'], "file_name" => $single_data['file_name'], 'id' => $single_data['id'],'user_file_name' => h($single_data['user_file_name']));
        }
    }
}
$attachmentGroup = $attachmentGroupimage + $attachmentGroup;
?>
<div class="inbox-view-detail">
    <div class="inbox-view">
        <?php if (!empty($dak_daptoriks['dak_description'])): ?>
        <div class="row">
            <div class="col-md-12 col-lg-12 col-sm-12">
                <span class="bold">বিবরণ :</span>
                <div style="background-color: #fff; max-width:950px; height: auto; margin:0 auto; padding-bottom: 10px;border-bottom: 1px solid #aaa;">
                    <?php echo ($dak_daptoriks['dak_description']); ?>
                </div>
            </div>
        </div>
        <?php endif; ?>
        <div class="row">

            <div class="row">
                <div class='col-md-12 col-lg-12 col-sm-12'>
                    <div class="portlet box green" style="border-top: 1px solid #4bc75e;">
                        <div class="portlet-body">
                            <?php if (isset($dak_attachments) && count($dak_attachments) > 0) { ?>
                                <div class="tabbable tabs-right" style="height: auto;">
                                    <?php
                                    $liList = "";
                                    $tabBody = $tabImageBody = "";
                                    $attachmentTab = "<table class='table table-bordered table-stripped'><thead><tr><th>ক্রম</th><th>ধরণ</th><th>নাম</th><th>ডাউনলোড</th></tr></thead><tbody>";
                                    if (!empty($attachmentGroup)) {
                                        $i = 0;
                                        foreach ($attachmentGroup as $key => $value) {

                                            $liList .= '<li class="' .  (( empty($dak_daptoriks['dak_description']) && $i == 0) ? 'active ' : ' ')  . '" >
                                        <a href="#attach_' . $i . '" data-toggle="tab">
                                            ' . $key . ' </a>
                                        </li>';

                                            $tabBody .= '<div class="tab-pane ' .  (( empty($dak_daptoriks['dak_description']) && $i == 0) ? 'active ' : 'fade') . '" id="attach_' . $i . '">';
                                            if ($key == 'image') {
                                                $tabBody .= '<div class="portlet ">
                                                        <div class="portlet-title">
                                                            <div class="actions">
                                                                <input type="button" value="<" class="btn btn-default image-prev" />
                                                                <input type="button" value=">" class="btn btn-default image-next" />
                                                            </div>
                                                        </div>
                                                        <div class="portlet-body">
                                                            <div class="projapoticanvas" style="position:relative; min-width:550px; width:100%; height:600px; overflow:hidden;"></div>
                                                        </div>
                                                    </div>';
                                            }
                                            foreach ($value as $kval => $val) {
                                                $attachmentTab .= "<tr><td>" . enTobn(++$i) . "</td><td>" . (!empty($val['file_type']) ? $key : "") . "</td><td>" . (!empty($val['user_file_name'])?h($val['user_file_name']):( !empty($val['name'])? h($val['name']) : "")) . "</td><td><a href='{$this->Url->build([
    "controller" => "dakMovements",
    "action" => "downloadDakAttachment",
    "?" => ["dak_id" => $dak_daptoriks['id'],"attachment_id"=>$val['id']]])}'>Download</a></td></tr>";
                                                if ($key == 'pdf') {
                                                    $tabBody .= '<embed src="' . $this->request->webroot .'content/' . $val['file_name'] .'?token='.sGenerateToken(['file'=>$val['file_name']],['exp'=>time() + 60*300]). '" style=" width:100%; height: 600px;" type="' . $val['file_type'] . '"></embed>';
                                                } elseif ($key == 'image') {
                                                    $tabBody .= '<div class="img-src" style="display:none;"><img id="' . $val['id'] . '" u="image" src="' .$this->request->webroot .'content/' . $val['file_name'] .'?token='.sGenerateToken(['file'=>$val['file_name']],['exp'=>time() + 60*300]). '" /></div>';
                                                } elseif ($key == 'mp3') {
                                                    $tabBody .= '<div class="col-lg-4 col-md-4 col-sm-6" style="width: auto;"><div class="portlet box red">
                        <div class="portlet-title">
                            <div class="caption">
                                ' . (isset($val['name']) ?h($val['name']) : 'Audio') . '
                            </div>
                            <div class="actions">
                            <a class="btn  btn-sm" href="' . $this->Url->build([
                                                            "controller" => "dakMovements",
                                                            "action" => "downloadDakAttachment",
                                                            "?" => ["dak_id" => $dak_daptoriks['id'], "attachment_id" => $val['id']]]) . '" title="Download"><i class="fa fa-download"></i></a>
                            </div>
                        </div>
                        <div class="portlet-body">
                        <audio style="margin-right: 10px"  controls><source src="' . $this->request->webroot .'content/'  . $val['file_name'] .'?token='.sGenerateToken(['file'=>$val['file_name']],['exp'=>time() + 60*300]). '" type="' . $val['file_type'] . '">Your browser does not support the audio element.</audio>
	</div>
</div></div>';
                                                } elseif ($key == 'video') {
                                                    $tabBody .= '<div class="col-lg-4 col-md-4 col-sm-6" style="width: auto;" ><div class="portlet box red">
                        <div class="portlet-title">
                            <div class="caption">
                                 ' . (isset($val['name']) ? h($val['name']) : 'Video') . '
                            </div>
                            <div class="actions">
                            <a class="btn  btn-sm" href="' . $this->Url->build([
                                                            "controller" => "dakMovements",
                                                            "action" => "downloadDakAttachment",
                                                            "?" => ["dak_id" => $dak_daptoriks['id'], "attachment_id" => $val['id']]]) . '" title="Download"><i class="fa fa-download"></i></a>
                            </div>
                        </div>
                        <div class="portlet-body">
                        <video style="margin-right: 10px; min-width:300px;" controls><source src="' . $this->request->webroot .'content/' . $val['file_name'] .'?token='.sGenerateToken(['file'=>$val['file_name']],['exp'=>time() + 60*300]). '" type="' . $val['file_type'] . '">Your browser does not support the audio element.</video>
	</div>
</div></div>';

                                                } else {

                                                    //$tabBody .= '<embed src="' . $val['file_name'] . '" style=" width:100%; height: 600px;" type="' . $val['file_type'] . '"></embed>';
                                                    $url = urlencode(FILE_FOLDER . $val['file_name'].'?token='.sGenerateToken(['file'=>$val['file_name']],['exp'=>time() + 60*300]));
                                                    $tabBody.='<iframe src="https://docs.google.com/gview?url='.$url.'&embedded=true" style=" width:100%; height: 600px;" frameborder="0"></iframe>';
                                                }
                                            }
                                            $tabBody .= '</div>';
                                        }
                                    }
                                    ?>
                                    <ul class="nav nav-tabs">
                                        <li><a href="#attach_<?php echo(isset($i) ? $i : 0); ?>"
                                               class="<?php echo(empty($i) ? "active" : ""); ?>" data-toggle="tab">
                                                <?php echo __(SHONGJUKTI) ?> </a></li>
                                        <?php echo $liList; ?>
                                    </ul>
                                    <div class="tab-content">
                                        <?php echo $tabBody; ?>
                                        <div class="tab-pane <?php echo(empty($i) ? "active" : "fade"); ?>"
                                             id="attach_<?php echo(isset($i) ? $i : 0); ?>">
                                            <?php echo $attachmentTab . "</tbody></table>"; ?>
                                        </div>
                                    </div>
                                </div>
                            <?php } else { ?>
                                <label class="label label-danger">কোন সংযুক্তি নেই</label>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if (isset($dak_moves) && count($dak_moves) > 0) { ?>
    <div class="timeline">
        <?php foreach ($dak_moves as $dak_move) {

            ?>
            <div class="timeline-item">
                <div class="timeline-badge">
                    <img src="<?php echo CDN_PATH; ?>assets/admin/pages/media/profile/avatar.png"
                         class="timeline-badge-userpic">
                </div>
                <div class="timeline-body">
                    <div class="timeline-body-arrow">
                    </div>
                    <div class="timeline-body-head">
                        <div class="timeline-body-head-caption">
                            <a class="timeline-body-title font-blue-madison"
                               href="#"><?php echo h($dak_move['name']) ?></a>
                            <span
                                class="timeline-body-time font-grey-cascade">সিদ্ধান্ত গ্রহণের তারিখ <?php echo h($dak_move['move_date_time']); ?></span>
                        </div>
                    </div>
                    <div class="timeline-body-content">
                <span class="font-grey-cascade">
                    <?php echo h($dak_move['value']); ?>
                </span>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
<?php } ?>

<?php if (count($attachmentGroupimage) > 0) { ?>
    <link href="<?php echo $this->request->webroot; ?>daptorik_preview/css/projapoticanvas.css" rel="stylesheet">
    <script src="<?php echo CDN_PATH; ?>daptorik_preview/js/react-with-addons.js"></script>
    <script src="<?php echo CDN_PATH; ?>daptorik_preview/js/projapoticanvas.js"></script>
    <script src="<?php echo $this->request->webroot; ?>daptorik_preview/js/projapoti_canvas_plugin.js"></script>
<?php } ?>

<script type="text/javascript">
    //Timeline.init();

    var OfficeDataAdapter = new Bloodhound({
        datumTokenizer: function (d) {
            return d.tokens;
        },
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: getBaseUrl() + 'officeManagement/autocompleteOfficeDesignation?search_key=%QUERY'
    });
    OfficeDataAdapter.initialize();

    $('.typeahead_to')
        .typeahead(null, {
            name: 'datypeahead_to',
            displayKey: 'value',
            source: OfficeDataAdapter.ttAdapter(),
            hint: (Metronic.isRTL() ? false : true),
            templates: {
                suggestion: Handlebars.compile([
                    '<div class="media">',
                    '<div class="media-body">',
                    '<h4 style="font-size:15px;" class="media-heading">{{value}}</h4>',
                    '</div>',
                    '</div>',
                ].join(''))
            }
        }
    )
        .on('typeahead:opened', onOpened)
        .on('typeahead:selected', onAutocompleted)
        .on('typeahead:autocompleted', onSelected);

    function onOpened($e) {
        //console.log('opened');
    }

    function onAutocompleted($e, datum) {
        DakOffice.setAutoDataIntoHiddenFields(datum, $e.currentTarget.id);
    }

    function onSelected($e, datum) {
        //console.log('selected');
    }
    $('.lc-picker').remove();

</script>


