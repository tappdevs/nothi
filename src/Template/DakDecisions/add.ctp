  <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/css/bootstrapValidator.min.css"/>
  <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.min.js"> </script>
<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class=""></i> নতুন সিদ্ধান্ত যোগ করুন
        </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <?= $this->Form->create($nothiTypes_records, ['type' => 'post', 'class' => 'form-horizontal' , 'id' => 'nothi_types']); ?>
        <div class="form-body">
            <div class="form-group">
                <label class="col-md-3 control-label">সিদ্ধান্ত লিখুন</label>

                <div class="col-md-4">
                    <div class="input-group">
							<span class="input-group-addon input-circle-left">
								<i class="fs1 a2i_gn_nothi2"></i>
							</span>
                        <?= $this->Form->input('decision_name', ['label' => false, 'class' => 'form-control input-circle-right', 'type' => 'text', 'placeholder' => 'সিদ্ধান্ত ধরন' ]); ?>
                    </div>
                </div>
            </div>
            
			<div class="form-group">
					<div class="col-md-9 col-md-offset-3">
					<div id="messages"></div>
			</div>
    </div>
        </div>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-3 col-md-9">
                    <button type="submit" class="btn   blue">সংরক্ষণ করুন</button>
                </div>
            </div>
        </div>
        <?= $this->Form->end(); ?>
    </div>
</div>


<script type="text/javascript">
$(document).ready(function() {
    $('#nothi_types').bootstrapValidator({
        container: '#messages',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
             decision_name: {
                validators: {
                    notEmpty: {
                        message: 'সিদ্ধান্ত লেখা হয়নি'
                    }
                }
            },
                      
        }
    });
});
</script>