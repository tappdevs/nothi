<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class=""></i>নথির ধরন
        </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <?= $this->Form->create($nothiTypes_records, ['type' => 'post', 'class' => 'form-horizontal']); ?>
        <div class="form-body">
            <div class="form-group">
                <label class="col-md-3 control-label">বিষয়ের ধরন</label>

                <div class="col-md-4">
                    <div class="input-group">
							<span class="input-group-addon input-circle-left">
							<i class="fs1 a2i_gn_nothi2"></i>
							</span>
                        <?= $this->Form->hidden('id') ?>
                        <?= $this->Form->input('type_name', ['label' => false, 'class' => 'form-control input-circle-right', 'type' => 'text', 'placeholder' => 'বিষয়ের  ধরন']); ?>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">কোড</label>

                <div class="col-md-4">
                    <div class="input-group">
							<span class="input-group-addon input-circle-left">
							<i class="fa fa-key"></i>
							</span>
                        <?= $this->Form->hidden('id') ?>
                        <?= $this->Form->input('office_id', ['label' => false, 'type' => 'hidden', 'value' => $office_id]); ?>
                        <?= $this->Form->input('office_unit_id', ['label' => false, 'type' => 'hidden', 'value' => $office_unit_id]); ?>
                        <?= $this->Form->input('type_code', ['label' => false, 'class' => 'form-control input-circle-right', 'type' => 'text', 'placeholder' => 'কোড']); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-3 col-md-9">
                    <button type="submit" class="btn   blue">সংরক্ষণ করুন</button>
                </div>
            </div>
        </div>
        <?= $this->Form->end(); ?>
    </div>
</div>
