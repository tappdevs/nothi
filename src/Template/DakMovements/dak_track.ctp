<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-datepicker/css/datepicker.css"/>
<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="panel panel-success">
            <div class="panel-heading">
                <div class="panel-title">
                    <?php echo __("সকল ডাক ট্র্যাকিং") ?>
	                <small style="color:#683091;font-weight:bold;">এক বা একাধিক তথ্য দিয়ে খুঁজতে পারবেন</small>
                </div>
            </div>
            <div class="panel-body">
                <div class="row form-group">

	                <div class="col-md-3 col-xs-12">
                        <?php
                        echo $this->Form->input('subject', ['label' => "বিষয়", "class" => 'form-control','placeholder'=>'বিষয় দিয়ে খুঁজুন'])
                        ?>
	                </div>

	                <div class="col-md-2 col-xs-12">
                        <?php
                        echo $this->Form->input('dak_received_no', ['label' => "আবেদন গ্রহণ নম্বর", "class" => 'form-control','placeholder'=>'আবেদন গ্রহণ নম্বর'])
                        ?>
                    </div>

                    <div class="col-md-2 col-xs-12">
                        <?php
                        echo $this->Form->input('sender_mobile', ['label' => "মোবাইল নম্বর", "class" => 'form-control','placeholder'=>'মোবাইল নম্বর'])
                        ?>
                    </div>

	                <div class="col-md-4 col-xs-12">
		                <label class="control-label ">পাঠানোর তারিখ</label>
		                <div class="input-group input-large date-picker_form input-daterange" data-date="<?php echo date('Y-m-d') ?>" data-date-format="yyyy-mm-dd">
			                <input type="text" class="form-control" name="from" id="search_date_from" placeholder="শুরুর তারিখ">
			                <span class="input-group-addon"> হইতে </span>
			                <input type="text" class="form-control" name="to" id="search_date_to" placeholder="শেষ তারিখ">
		                </div>
	                </div>

	                <div class="col-md-1 col-xs-12" style="padding:0; top:31px;">
		                <div class="btn-group btn-group-round">
		                <a title="খুঁজুন" class="btn green trackingformSearch btn-md">
			                <i class="fs1 a2i_gn_search1"></i>
		                </a>
		                <a title="রিসেট" class="btn red btn-md" onclick="reset_it()" >
			                <i class="fs1 a2i_gn_reset2" aria-hidden="true"></i>
		                </a>
		                </div>
	                </div>
                </div>


                <div class="row form-group responsetable"  style="display: none;">
                    <div class="col-md-12 ">
                        <hr/>
                        <table class="table table-bordered table-stripped ">
                            <thead>
                            <tr>
                                <th style="width: 15%" class="text-center  font-sm">গ্রহণ নম্বর</th>
                                <th style="width: 15%" class="text-center  font-sm">তারিখ</th>
                                <th style="width: 25%" class="text-center  font-sm">বিষয়</th>
                                <th style="width: 25%" class="text-center font-sm">বর্তমান টেবিল</th>
                                <th style="width: 20%" class="text-center font-sm">সর্বশেষ মন্তব্য</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-toastr/toastr.min.js"></script>
<script src="<?php echo CDN_PATH; ?>assets/admin/pages/scripts/ui-toastr.js"></script>

<script>
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "positionClass": "toast-bottom-right"
    };
    function reset_it(){
        $('#dak-received-no').val('');
        $('#sender-mobile').val('');
        $('#subject').val('');
        $('#search_date_from').val('');
        $('#search_date_to').val('');
    }

    $('.trackingformSearch').on('click', function () {
        var receive_no = $.trim($('#dak-received-no').val());
        var mobile_no = $.trim($('#sender-mobile').val());

        var subject = $.trim($('#subject').val());
        var date_from = $.trim($('#search_date_from').val());
        var date_to = $.trim($('#search_date_to').val());

        if (receive_no != '' || mobile_no != '' || subject != '' || (date_from != '' && date_to != '')) {
            $('.responsetable').show();
            $('.responsetable').find('tbody').html('<tr><td class="text-center bold-text" colspan=5><i class="fa fa-spinner fa-spin"></i></td></tr>');
            $.ajax({
                url: '<?php echo $this->Url->build(['controller' => 'DakMovements', 'action' => 'dakTrack']) ?>',
                type: 'post',
                data: {
                    receive_no: EngFromBn(receive_no),
	                mobile_no: EngFromBn(mobile_no),
                    subject: subject,
                    date_from:date_from,
	                date_to:date_to
                },
                dataType: 'json',
                success: function (response) {
                    $('.responsetable').find('tbody').html('');
                    if (response.status == 'success') {
                        $.each(response.data, function (i, v) {
                            if(!isEmpty(v.dak_received_no)){
                                console.log(response.data);
                                var montobbo = v.dak_actions;
                                $('.responsetable').find('tbody').append("<tr class='text-center'><td>" + v.dak_received_no + "</td><td>" + dateConvert(v.dakcreated) + "</td><td>" + escapeHtml(v.dak_subject) + "</td><td>" + escapeHtml(v.to_officer_designation_label) + "," + escapeHtml(v.to_office_unit_name) + "</td><td>" + BnFromEng(montobbo) + "</td></tr>");
                            }

                        });
                    } else {
                        $('.responsetable').find('tbody').append("<tr><td colspan=5 class='text-center text-danger'>" + response.msg + "</td></tr>");
                    }
                },
                error: function () {
                    $('.responsetable').find('tbody').html('');
                    toastr.error("দু:খিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না");
                }
            });
        }
    });
    function dateConvert(value) {
        var bDate = new Array("০", "১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯");
        var bMonth = new Array("জানুয়ারি", "ফেব্রুয়ারি", "মার্চ", "এপ্রিল", "মে", "জুন", "জুলাই", "আগস্ট", "সেপ্টেম্বর", "অক্টোবর", "নভেম্বর", "ডিসেম্বর");

        if (value!='' && value!=null) {
            var dt = new Date(value);
            var dtb = dt.getDate();
            var dtb1 = "", dtb2 = "";

            if (dtb >= 10) {
                dtb1 = Math.floor(dtb / 10);
                dtb2 = dtb % 10;
                dtb = bDate[dtb1] + "" + bDate[dtb2];
            } else {
                dtb = bDate[0] + "" + bDate[dtb];
            }

            var mnb;
            var mn = dt.getMonth();
            mnb = bMonth[mn];

            var yrb = "", yr1;
            var yr = dt.getFullYear();

            for (var i = 0; i < 3; i++) {
                yr1 = yr % 10;
                yrb = bDate[yr1] + yrb;
                yr = Math.floor(yr / 10);
            }

            yrb = bDate[yr] + "" + yrb;

            return (typeof(dtb)!='undefined' || dtb!='undefined')?(dtb + " " + mnb + ", " + yrb):dt.getDate();
        }else{
            return value;
        }
    }

    $(function(){
        $('.date-picker_form').datepicker({
            rtl: Metronic.isRTL(),
            orientation: "left",
            autoclose: true,
            endDate: '<?php echo date("Y-m-d") ?>'
        });
    })

</script>
