<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-datepicker/css/datepicker.css"/>
<link href="<?php echo CDN_PATH; ?>daptorik_preview/css/custom.css" rel="stylesheet" type="text/css"/>

<!-- END PAGE LEVEL STYLES -->
<style>
    tfoot {
        padding: 2px;
        background-color: rgba(241, 241, 241, 0.73);
    }

    .footer {
        display: none;
    }

    .dak_sender_cell_list {
        position: absolute;
        border: 2px solid #CECECE;
        background: #F5F5F5;
        padding-top: 15px;
        overflow: auto;
        z-index: 9000;
        color: #000;
        left: 15px;
        width: 100%;
        text-align: left;
    }

    .checkbox input[type=checkbox], .checkbox-inline input[type=checkbox], .radio input[type=radio], .radio-inline input[type=radio] {
        margin-left: -10px !important;
    }

    input[type=checkbox], input[type=radio] {
        margin-top: 2px;
    }

    div.radio, div.checker {
        margin-top: -2px;
    }

    .newInbox {
        background-color: #C7DAE0;
    }

    h3 {
        margin-top: 0px;
        margin-bottom: 0px;
    }
    .editable{
        border: none;
        word-break:break-word;
        word-wrap:break-word
    }
</style>

<div class="portlet-body">
    <div class="inbox-header inbox-view-header">
        <div class="col-md-8">
            <ul class="inbox-nav nav nav-pills">
                <li class="inbox  <?php if ($dak_inbox_group == 'inbox') echo 'active'; ?>"><a data-title="Inbox" title="<?php echo __("আগত ডাক "); ?>"  href="javascript:" class="btn btn-sm  green"> <i class="fs1 a2i_gn_inbox1">&nbsp;</i><?php echo __("আগত ডাক "); ?></a></li>
                <li class="archive <?php if ($dak_inbox_group == 'archive') echo 'active'; ?>"><a data-title="<?php echo __("আর্কাইভড ডাক "); ?>" title="<?php echo __("আর্কাইভড ডাক"); ?>" href="javascript:;"  class="btn btn-sm  btn-danger blue"> <i class="fs1 a2i_gn_inbox2"></i> <?php echo __("আর্কাইভড ডাক"); ?></a></li>

                <li class="sent <?php if ($dak_inbox_group == 'sent') echo 'active'; ?>"><a data-title="<?php echo __("প্রেরিত ডাক "); ?>" title="<?php echo __("প্রেরিত ডাক "); ?>" href="javascript:;"  class="btn btn-sm  btn-danger purple"> <i class="fs1 a2i_gn_send1"></i> <?php echo __("প্রেরিত ডাক "); ?></a></li>

                <li class="nothivukto <?php if ($dak_inbox_group == 'nothivukto') echo 'active'; ?>"><a data-title="<?php echo __("নথিতে উপস্থাপিত ডাক"); ?>" title="<?php echo __("নথিতে উপস্থাপিত ডাক"); ?>" href="javascript:;" class="btn btn-sm yellow-casablanca"> <i class="fs1 a2i_gn_nothi2"></i> <?php echo __("নথিতে উপস্থাপিত ডাক "); ?></a></li>

                <li class="nothijato <?php if ($dak_inbox_group == 'nothijato') echo 'active'; ?>"><a data-title="<?php echo __("নথিজাত ডাক"); ?>" title="<?php echo __("নথিজাত ডাক"); ?>" href="javascript:;" class="btn btn-sm  red"> <i class="fs1 a2i_gn_nothi3"></i> <?php echo __("নথিজাত ডাক"); ?></a></li>
            </ul>
        </div>
        <div class="col-md-4 text-center">
            <h3> <?php if ($dak_inbox_group == "inbox") { ?> আগত ডাক  <?php } elseif($dak_inbox_group == "archive") { ?> আর্কাইভড ডাক  <?php } elseif ($dak_inbox_group == "nothivukto") { ?> নথিতে উপস্থাপিত ডাক  <?php } elseif ($dak_inbox_group == "nothitepesh") { ?> নথিতে পেশকরণে  আগত ডাক <?php } elseif ($dak_inbox_group == "nothijat") { ?> নথিজাত করণে আগত ডাক <?php } elseif ($dak_inbox_group == "nothijato") { ?> নথিজাত ডাক <?php } else { ?> প্রেরিত ডাক  <?php } ?></h3>
        </div>

    </div>


    <hr/>
    <div class="table-container data-table-dak">
        <?php if ($dak_inbox_group == 'sent') {
            
        }
        ?>
        <table class="table table-bordered table-striped table-hover" id="datatable_dak">
            <thead>
                <tr role="row" class="filter">
<?php if ($dak_inbox_group == 'inbox' || $dak_inbox_group == 'archive' || $dak_inbox_group == 'nothitepesh' || $dak_inbox_group == 'nothijat') { ?>
                        <td colspan=3>
                            <input type="text" class="form-control form-filter input-sm" placeholder="প্রেরক" name="receiving_officer_name" id="filter_input_2">

                            <input type="text" class="form-control form-filter input-sm" placeholder="বিষয়" name="dak_subject"  id="filter_input_1">
                        </td>
<?php } else { ?>
                        <td colspan=2>
                            <input type="text" class="form-control form-filter input-sm" placeholder="প্রেরক" name="receiving_officer_name" id="filter_input_2">
                            <input type="text" class="form-control form-filter input-sm" placeholder="বিষয়" name="dak_subject" id="filter_input_1">
                        </td>
<?php } ?>

                    <td >
                        <?php
                        echo $this->Form->input('dak_security_level', ['options' => json_decode(DAK_SECRECY_TYPE), 'type' => 'select', 'class' => 'form-control  form-filter input-sm ', 'label' => false, 'empty' => 'গোপনীয়তা বাছাই করুন'])
                        ?>
                        <?php
                        echo $this->Form->input('dak_priority_level', ['options' => json_decode(DAK_PRIORITY_TYPE), 'type' => 'select', 'class' => 'form-control  form-filter input-sm ', 'label' => false, 'empty' => 'অগ্রাধিকার বাছাই করুন'])
                        ?>
                    </td>

                    <td colspan="2">
                        <div class=" input-group date-picker input-daterange" data-date="<?php echo date('Y-m-d') ?>" data-date-format="yyyy-mm-dd">
                            <input placeholder="হইতে " type="text" class="form-control input-sm form-filter" name="from" id="search_date_from">
                            <span class="input-group-addon input-sm"> হইতে </span>
                            <input placeholder="পর্যন্ত" type="text" class="form-control input-sm form-filter" name="to" id="search_date_to">
                        </div>

                    </td>
                    <td colspan="4" >
                        <div class="btn-group text-center">
                            <button title="<?php echo __(SEARCH) ?>" class="btn btn-sm yellow filter-submit margin-bottom" id="filter_submit_btn" name="filter_submit_btn"><i class="fs1 a2i_gn_search1"></i></button>
                            <button class="btn btn-sm red filter-cancel" title="<?php echo __(RESET) ?>"><i class="fs1 a2i_gn_reset2"></i>
                            </button>
<?php if ($dak_inbox_group == 'inbox' || $dak_inbox_group == 'archive' || $dak_inbox_group == 'nothitepesh' || $dak_inbox_group == 'nothijat') { ?>
                                <button class="btn btn-sm blue btnNothiVukto" title="<?php echo __("নথিতে উপস্থাপন করুন") ?>"><i  class="fs1 a2i_gn_nothi2"></i>
                                </button>
                                <button class="btn btn-sm purple btnNothiJato" title="<?php echo __("নথিজাত করুন") ?>"><i  class="fs1 a2i_gn_nothi3"></i>
                                </button>
<?php } ?>
                        </div>
                    </td>

                </tr>

<?php if ($dak_inbox_group == 'inbox' || $dak_inbox_group == 'archive') { ?>
                    <tr role="row" class="heading">
                        <th style='width: 5% !important; text-align: center;'><input type="checkbox" id="selectAllDraft" title="সকল ডাক বাছাই করুন"  /> </th>
                        <th style='width: 10%!important; text-align: center;'> কার্যক্রম</th>
                        <th style='width: 20%!important'><?php echo __(UTSHO) ?></th>
                        <th style='width: 20%!important'><?php echo __(BISHOY) ?></th>
                        <th style='width: 15%!important'><?php echo __(PRAPOK) ?></th>
                        <th style='width: 10%!important'><?php echo __(PURBOBORTI) . " " . SHIDDHANTO ?></th>
                        <th style='width: 10%!important'><?php echo __(TARIKH) ?></th>
                        <th style='width: 4%'><?php echo __(DHORON) ?></th>
                        <th style='width: 3%'>ধরন</th>
                        <th style='width: 3%'><?php echo __(SHONGJUKTI) ?></th>
                    </tr>
<?php } else { ?>
                    <tr role="row" class="heading">
                        <th style='width: 10%; text-align: center;'>বিস্তারিত</th>
                        <th style='width: 20%'><?php echo __(UTSHO) ?></th>
                        <th style='width: 20%'><?php echo __(BISHOY) ?></th>
                        <th style='width: 15%'><?php echo __(POROBORTI) . " " . PRAPOK ?>   </th>
                        <th style='width: 15%'><?php echo __(SHIDDHANTO) ?></th>
                        <th style='width: 10%'><?php echo __(TARIKH) ?></th>
                        <th style='width: 4%'><?php echo __(DHORON) ?></th>
                        <th style='width: 3%'>ধরন</th>
                        <th style='width: 3%'><?php echo __(SHONGJUKTI) ?></th>
                    </tr>
<?php } ?>
            </thead>
            <tbody>
            </tbody>
<?php if ($dak_inbox_group == 'inbox') { ?>
                <tfoot role="row" class="footer">
                    <tr>
                        <td colspan="10" class="dak_list_checkbox_to_action">
    <?php echo $this->cell('DakRecipient', ['params' => $selected_office_section, "type" => 'multiple']); ?>
                        </td>

                    </tr>

                </tfoot>
<?php } ?>
        </table>
    </div>
</div>
<?php
echo $this->element('sealjs');
?>
<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="<?php echo CDN_PATH; ?>daptorik_preview/js/tbl_dak_list_ajax.js?v=<?= js_css_version ?>"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script type="text/javascript">

    $.extend(true, $.fn.dataTable.defaults, {
        "ordering": false
    });
<?php if ($dak_inbox_group == 'inbox') { ?>
        TableAjax.init('<?php echo $dak_inbox_group ?>', 'dakMovements/daklist_inbox/');
<?php } elseif ($dak_inbox_group == 'archive') { ?>
        TableAjax.init('<?php echo $dak_inbox_group ?>', 'dakMovements/daklist_onulipi/');
<?php } elseif ($dak_inbox_group == 'nothitepesh') { ?>

        TableAjax.init('inbox', 'dakMovements/daklist_inbox/1/');
<?php } elseif ($dak_inbox_group == 'nothijat') { ?>
        TableAjax.init('inbox', 'dakMovements/daklist_inbox/2/');
<?php } elseif ($dak_inbox_group == 'nothivukto') { ?>
        TableAjax.init('<?php echo $dak_inbox_group ?>', 'dakMovements/daklist_nothivukto/');
<?php } elseif ($dak_inbox_group == 'nothijato') { ?>
        TableAjax.init('<?php echo $dak_inbox_group ?>', 'dakMovements/daklist_nothijato/');
<?php } else {
    ?>
        TableAjax.init('<?php echo $dak_inbox_group ?>', 'dakMovements/daklist_sent/');
<?php } ?>

    $(document).off('click', '.btn-close-seal-modal');
    $(document).on('click', '.btn-close-seal-modal', function () {
        $('.addSeal').find('.scroller').html('');
        if ($('#sealtype').val() == 0) {
            $('.dak_list_checkbox_to_action').html('<img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"><span>&nbsp;&nbsp;লোড করা হচ্ছে...</span>');

            $('.dak_list_checkbox_to_action').load('<?php echo $this->Url->build(['controller' => 'officeManagement', 'action' => 'reloadOfficeSeal', $selected_office_section['office_id'], $selected_office_section['office_unit_id'], 1, 1]); ?>');

            $('.dak_sender_cell_list').html('<img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"><span>&nbsp;&nbsp;লোড করা হচ্ছে...</span>');

            $.ajax({
                url: '<?php echo $this->Url->build(['controller' => 'officeManagement', 'action' => 'reloadOfficeSeal', $selected_office_section['office_id'], $selected_office_section['office_unit_id'], 1]); ?>',
                method: "post",
                success: function (response) {
                    $('.dak_sender_cell_list').html(response);
                    $(document).find('.dak_sender_cell_list').find('.forwardSingle').after('<button class="btn btn-sm btn-danger" onclick="javascript:$(this).parents(\'.dak_sender_cell_list\').toggle();" title="বন্ধ করুন" data-original-title="বন্ধ করুন"><i class="fs1 a2i_gn_close2"></i> বন্ধ করুন</button>')
                }, error: function (xrs) {
                    $('.dak_sender_cell_list').load('<?php echo $this->Url->build(['controller' => 'officeManagement', 'action' => 'reloadOfficeSeal', $selected_office_section['office_id'], $selected_office_section['office_unit_id'], 1]); ?>');
                    $(document).find('.dak_sender_cell_list').find('.forwardSingle').after('<button class="btn btn-sm btn-danger" onclick="javascript:$(this).parents(\'.dak_sender_cell_list\').toggle();" title="বন্ধ করুন" data-original-title="বন্ধ করুন"><i class="fs1 a2i_gn_close2"></i> বন্ধ করুন</button>')
                }
            })
        }
    });

    $(document).off('click', '.deleteSeal');
    $(document).on('click', '.deleteSeal', function () {
        if (confirm("আপনি কি নিশ্চিত মুছে ফেলতে চান")) {
            var sealId = $(this).data('id');
            $.ajax({
                url: '<?php echo $this->request->webroot ?>officeManagement/officeSealDelete',
                data: {id: sealId},
                type: "POST",
                dataType: 'json',
                success: function (res) {
                    if (res.status == "success")
                    {
                        $('.dak_list_checkbox_to_action').load('<?php echo $this->Url->build(['controller' => 'officeManagement', 'action' => 'reloadOfficeSeal', $selected_office_section['office_id'], $selected_office_section['office_unit_id'], 1, 1]); ?>');
                        $('.dak_sender_cell_list').load('<?php echo $this->Url->build(['controller' => 'officeManagement', 'action' => 'reloadOfficeSeal', $selected_office_section['office_id'], $selected_office_section['office_unit_id'], 1]); ?>');
                    } else
                        toastr.error(res.msg);
                }
            });
        }

    });


    var OfficeDataAdapter = new Bloodhound({
        datumTokenizer: function (d) {
            return d.tokens;
        },
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: getBaseUrl() + 'officeManagement/autocompleteOfficeDesignation?search_key=%QUERY'
    });
    OfficeDataAdapter.initialize();

    $('.typeahead_to')
            .typeahead(null, {
                name: 'datypeahead_to',
                displayKey: 'value',
                source: OfficeDataAdapter.ttAdapter(),
                hint: (Metronic.isRTL() ? false : true),
                templates: {
                    suggestion: Handlebars.compile([
                        '<div class="media">',
                        '<div class="media-body">',
                        '<h4 class="media-heading">{{value}}</h4>',
                        '</div>',
                        '</div>',
                    ].join(''))
                }
            }
            )
            .on('typeahead:opened', onOpened)
            .on('typeahead:selected', onAutocompleted)
            .on('typeahead:autocompleted', onSelected);

    function onOpened($e) {
        //console.log('opened');
    }

    function onAutocompleted($e, datum) {
        DakOffice.setAutoDataIntoHiddenFields(datum, $e.currentTarget.id);
    }

    function onSelected($e, datum) {
        //console.log('selected');
    }


    $('[data-toggle="tooltip"]').tooltip({'placement':'bottom'});
    $('[title]').tooltip({'placement':'bottom'});


    $(document).off('click', '.dak_actions_radio');
    $(document).on('click', '.dak_actions_radio', function () {
		$('.dak_actions_radio').closest('span').removeClass('checked')
		$(this).parent('span').addClass('checked')
        if ($(this).checked == true || this.checked) {

            if ($(this).val() != 0) {
                $(this).closest('.form-group').find('#dak-actions').val($(this).data('title'));
                $(this).closest('.form-group').find('#dak-actions').attr('readonly', 'readonly');
            } else {
                $(this).closest('.form-group').find('#dak-actions').val('');
                $(this).closest('.form-group').find('#dak-actions').val('');
                $(this).closest('.form-group').find('#dak-actions').removeAttr('readonly');
            }
        }
    });

    $(document).on('click', '#selectAllDraft', function () {
        if (this.checked == true) {
            $('.dak_list_checkbox_to_select').each(function () {
                if (this.checked == false) {
                    this.click();
                }
            });
        } else {
            $('.dak_list_checkbox_to_select').each(function () {
                if (this.checked == true) {
                    this.click();
                }
            });
        }
    });

    $(function () {
        $('.date-picker').datepicker({
            rtl: Metronic.isRTL(),
            orientation: "left",
            autoclose: true,
            endDate: '<?php echo date("Y-m-d") ?>'
        });
    });
    

function getMovementHistory(dak_id, dak_type) {

    $('.showHistory').html('<div class="text-center"><i class="fa fa-spin "></i></div>');
    PROJAPOTI.ajaxSubmitDataCallback('<?= $this->Url->build(['controller'=>'DakMovements','action'=>'movementHistory']) ?>', {dak_id:dak_id,dak_type:dak_type}, 'html', function(html){
        $('.showHistory').html(html);
        
        $('.showHistory').closest('.portlet').find('.actions').hide();

    });
}
    
function getPopUpPotro(href, title) {

    $('#responsiveModal').find('.modal-title').text('');
    $('#responsiveModal').find('.modal-body').html('');
    $.ajax({
        url: '<?php echo $this->request->webroot; ?>NothiMasters/showPopUp/' + href,
        dataType: 'html',
        data:{'nothi_office':<?php echo $selected_office_section['office_id'] ?>},
        type:'post',
        success: function (response) {
            $('#responsiveModal').modal('show');
            $('#responsiveModal').find('.modal-title').text(title);

            $('#responsiveModal').find('.modal-body').html(response);
        }
    });
}

$(document).off('click', '.showforPopup').on('click', '.showforPopup', function (e) {
    e.preventDefault();
    var title = $(this).attr('title').length > 0 ? $(this).attr('title') : $(this).attr('data-original-title');
    getPopUpPotro($(this).attr('href'), title);
})

</script>