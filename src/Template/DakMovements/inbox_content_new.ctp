<link href="<?php echo CDN_PATH; ?>daptorik_preview/css/custom.css" rel="stylesheet" type="text/css"/>

<!-- END PAGE LEVEL STYLES -->
<!-- END PAGE LEVEL STYLES -->
<style>
    tfoot {
        padding: 2px;
        background-color: rgba(241, 241, 241, 0.73);
    }

    .footer {
        display: none;
    }

    .dak_sender_cell_list {
        position: absolute;
        border: 2px solid #CECECE;
        background: #F5F5F5;
        padding-top: 15px;
        overflow: auto;
        z-index: 9000;
        color: #000;
        left: 15px;
        width: 99%;
        text-align: left;
    }

    .checkbox input[type=checkbox], .checkbox-inline input[type=checkbox], .radio input[type=radio], .radio-inline input[type=radio] {
        margin-left: -10px !important;
    }

    input[type=checkbox], input[type=radio] {
        margin-top: 2px;
    }

    div.radio, div.checker {
        margin-top: -2px;
    }

    .newInbox {
        background-color: #C7DAE0;
    }

    h3 {
        margin-top: 0px;
        margin-bottom: 0px;
    }
    .editable{
        border: none;
        word-break:break-word;
        word-wrap:break-word
    }
    .nav-tabs .dropdown-menu {
        font-size: 11pt!important;
    }
    .tab-content, .active{
        background-color:  #e9ffcc!important;
    }

    #dbl_click > tr > td{
        background-color: transparent;
    }

    #dbl_click > tr:hover {
        box-shadow: inset #cecece 0px 0px 4px 1px;
        cursor: pointer;
    }

</style>

<ul class="nav nav-tabs nav-justified">
    <li class="inbox bold <?php if ($dak_inbox_group == 'inbox') echo 'active'; ?>">
        <a href="#inbox" data-toggle="tab" aria-expanded="true">
           <?php if ($dak_inbox_group == 'inbox'){
               echo '<span class ="font-green"> আগত ডাক </span>';
           }else{
                echo '<span class ="font-purple"> আগত ডাক </span>';
           }

           ?>  
        </a>
    </li>
    <li class="sent bold <?php if ($dak_inbox_group == 'sent') echo 'active'; ?>">
        <a href="#sent" data-toggle="tab" aria-expanded="false">
              <?php if ($dak_inbox_group == 'sent'){
               echo '<span class ="font-green">প্রেরিত ডাক </span>';
           }else{
                echo '<span class ="font-purple"> প্রেরিত ডাক </span>';
           }

           ?>  
            
        </a>
    </li>
    <li class="dropdown bold <?php if ($dak_inbox_group == 'nothivukto' || $dak_inbox_group
    == 'nothijato' || $dak_inbox_group == 'archive') echo 'active'; ?>">
        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> 
                     <?php if ($dak_inbox_group == 'nothivukto' || $dak_inbox_group
    == 'nothijato' || $dak_inbox_group == 'archive'){
               echo '<span class ="font-green">অন্যান্য ডাক</span>';
           }else{
                echo '<span class ="font-purple"> অন্যান্য ডাক</span>';
           }

           ?>  
            <i class="glyphicon glyphicon-chevron-down"></i>
        </a>
        <ul class="dropdown-menu" role="menu">
            <li class="nothivukto <?php if ($dak_inbox_group == 'nothivukto') echo 'active'; ?>">
                <a href="#nothivukto" tabindex="-1" data-toggle="tab"> নথিতে উপস্থাপিত ডাক </a>
            </li>
            <li class="nothijato <?php if ($dak_inbox_group == 'nothijato') echo 'active'; ?>">
                <a href="#nothijato" tabindex="-1" data-toggle="tab"> নথিজাত ডাক </a>
            </li>
            <li class="archive <?php if ($dak_inbox_group == 'archive') echo 'active'; ?>">
                <a href="#archive" tabindex="-1" data-toggle="tab"> আর্কাইভড ডাক  </a>
            </li>
        </ul>
    </li>

</ul>
<div class="tab-content">
    <div class="tab-pane fade active in">
        <div class="table-container data-table-dak">
<?php if ($dak_inbox_group == "inbox") { ?>
                <div class="row" style="padding-right: 5%;margin-bottom: 5px;">
                    <div class="pull-right" >
	                    <div class="btn-group btn-group-round">
	                        <button class="btn btn-sm blue btnNothiVukto" title="<?php echo __("নথিতে উপস্থাপন") ?>"><i  class="fs1 a2i_gn_nothi2"></i>
	                        </button>
	                        <button class="btn btn-sm purple btnNothiJato" title="<?php echo __("নথিজাত") ?>"><i  class="fs1 a2i_gn_nothi3"></i>
	                        </button>
	                    </div>
                        <br>
                    </div>
                </div>

                    <?php } ?>

            <table class="table table-bordered table-striped table-hover" id="datatable_dak">
                <thead>

<?php if ($dak_inbox_group == 'inbox') {
    ?>
                        <tr role="row" class="heading">
                            <th style='width: 2% !important; text-align: center;'><input type="checkbox" id="selectAllDraft" title="সকল ডাক বাছাই করুন"  /> </th>
                            <th style='width: 7%!important; text-align: center;'> কার্যক্রম</th>
                            <th style='width: 17%!important'><?php echo __(UTSHO) ?></th>
                            <th style='width: 20%!important'><?php echo __(BISHOY) ?></th>
                            <th style='width: 25%!important'><?php echo __(PRAPOK) ?></th>
                            <th style='width: 10%!important'><?php echo __(PURBOBORTI)." ".SHIDDHANTO ?></th>
                            <th style='width: 9%!important'><?php echo __(TARIKH) ?></th>
                            <th style='width: 4%'><?php echo __(DHORON) ?></th>
                            <th style='width: 3%'>ধরন</th>
                        </tr>
<?php } else { ?>
                        <tr role="row" class="heading">
                            <th style='width: 17%'><?php echo __(UTSHO) ?></th>
                            <th style='width: 20%'><?php echo __(BISHOY) ?></th>
                            <th style='width: 25%'><?php echo __(POROBORTI)." ".PRAPOK ?></th>
                            <th style='width: 15%'><?php echo __(SHIDDHANTO) ?></th>
                            <th style='width: 9%'><?php echo __(TARIKH) ?></th>
                            <th style='width: 4%'><?php echo __(DHORON) ?></th>
                            <th style='width: 3%'>ধরন</th>
                        </tr>
<?php } ?>
                </thead>
                <tbody id="dbl_click">
                </tbody>
                            <?php if ($dak_inbox_group == 'inbox') { ?>
                    <tfoot role="row" class="footer">
                        <tr>
                            <td colspan="9" class="dak_list_checkbox_to_action">
    <?php
    echo $this->cell('DakRecipient',
        ['params' => $selected_office_section, "type" => 'multiple']);
    ?>
                            </td>

                        </tr>

                    </tfoot>
<?php } ?>
            </table>

        </div>
    </div>
</div>

<?php
echo $this->element('sealjs');
?>

<script type="text/javascript">

    $.extend(true, $.fn.dataTable.defaults, {
        "ordering": false
    });
<?php if ($dak_inbox_group == 'inbox') { ?>
        TableAjax.init('<?php echo $dak_inbox_group ?>', 'dakMovements/daklist_inbox/');
<?php } elseif ($dak_inbox_group == 'onulipi') { ?>
        TableAjax.init('<?php echo $dak_inbox_group ?>', 'dakMovements/daklist_onulipi/');
<?php } elseif ($dak_inbox_group == 'archive') { ?>
        TableAjax.init('<?php echo $dak_inbox_group ?>', 'dakMovements/daklist_onulipi/');
<?php } elseif ($dak_inbox_group == 'nothitepesh') { ?>

        TableAjax.init('inbox', 'dakMovements/daklist_inbox/1/');
<?php } elseif ($dak_inbox_group == 'nothijat') { ?>
        TableAjax.init('inbox', 'dakMovements/daklist_inbox/2/');
<?php } elseif ($dak_inbox_group == 'nothivukto') { ?>
        TableAjax.init('<?php echo $dak_inbox_group ?>', 'dakMovements/daklist_nothivukto/');
<?php } elseif ($dak_inbox_group == 'nothijato') { ?>
        TableAjax.init('<?php echo $dak_inbox_group ?>', 'dakMovements/daklist_nothijato/');
<?php } else {
    ?>
        TableAjax.init('<?php echo $dak_inbox_group ?>', 'dakMovements/daklist_sent/');
<?php } ?>

    $(document).ajaxStop(function() {
        $.uniform.restore($('input[type=radio]'));
        $.uniform.restore($('input[type=checkbox]'));

        $("#selectAllDraft").uniform();
        $(".dak_list_checkbox_to_select").uniform();
    });
    $(document).off('click', '.btn-close-seal-modal');
    $(document).on('click', '.btn-close-seal-modal', function () {
        $('.addSeal').find('.scroller').html('');
        if ($('#sealtype').val() == 0) {
            $('.dak_list_checkbox_to_action').html('<img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"><span>&nbsp;&nbsp;লোড করা হচ্ছে...</span>');

            $('.dak_list_checkbox_to_action').load('<?php
echo $this->Url->build(['controller' => 'officeManagement',
    'action' => 'reloadOfficeSeal', $selected_office_section['office_id'], $selected_office_section['office_unit_id'],
    1, 1]);
?>');

            $('.dak_sender_cell_list').html('<img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"><span>&nbsp;&nbsp;লোড করা হচ্ছে...</span>');

            $.ajax({
                url: '<?php
echo $this->Url->build(['controller' => 'officeManagement',
    'action' => 'reloadOfficeSeal', $selected_office_section['office_id'], $selected_office_section['office_unit_id'],
    1]);
?>',
                method: "post",
                success: function (response) {
                    $('.dak_sender_cell_list').html(response);
                    $(document).find('.dak_sender_cell_list').find('.forwardSingle').after('<button class="btn btn-sm btn-danger" onclick="javascript:$(this).parents(\'.dak_sender_cell_list\').toggle();" title="বন্ধ করুন" data-original-title="বন্ধ করুন"><i class="fs1 a2i_gn_close2"></i> বন্ধ করুন</button>')
                }, error: function (xrs) {
                    $('.dak_sender_cell_list').load('<?php
echo $this->Url->build(['controller' => 'officeManagement',
    'action' => 'reloadOfficeSeal', $selected_office_section['office_id'], $selected_office_section['office_unit_id'],
    1]);
?>');
                    $(document).find('.dak_sender_cell_list').find('.forwardSingle').after('<button class="btn btn-sm btn-danger" onclick="javascript:$(this).parents(\'.dak_sender_cell_list\').toggle();" title="বন্ধ করুন" data-original-title="বন্ধ করুন"><i class="fs1 a2i_gn_close2"></i> বন্ধ করুন</button>')
                }
            })
        }
    });

    $(document).off('click', '.deleteSeal');
    $(document).on('click', '.deleteSeal', function () {
        var sealId = $(this).data('id');
        bootbox.dialog({
            message: "আপনি কি নিশ্চিত মুছে ফেলতে চান?",
            title: "ডাক সিল মুছে ফেলুন",
            buttons: {
                success: {
                    label: "হ্যাঁ",
                    className: "green",
                    callback: function () {
                        $.ajax({
                            url: '<?php echo $this->request->webroot ?>officeManagement/officeSealDelete',
                            data: {id: sealId},
                            type: "POST",
                            dataType: 'json',
                            success: function (res) {
                                if (res.status == "success")
                                {
                                    $('.dak_list_checkbox_to_action').load('<?php
                                        echo $this->Url->build(['controller' => 'officeManagement',
                                            'action' => 'reloadOfficeSeal', $selected_office_section['office_id'], $selected_office_section['office_unit_id'],
                                            1, 1]);
                                        ?>');
                                    $('.dak_sender_cell_list').load('<?php
                                        echo $this->Url->build(['controller' => 'officeManagement',
                                            'action' => 'reloadOfficeSeal', $selected_office_section['office_id'], $selected_office_section['office_unit_id'],
                                            1]);
                                        ?>');
                                } else
                                    toastr.error(res.msg);
                            }
                        });
                    }
                },
                danger: {
                    label: "না",
                    className: "red",
                    callback: function () {
                    }
                }
            }
        });
    });


    var OfficeDataAdapter = new Bloodhound({
        datumTokenizer: function (d) {
            return d.tokens;
        },
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: getBaseUrl() + 'officeManagement/autocompleteOfficeDesignation?search_key=%QUERY'
    });
    OfficeDataAdapter.initialize();

    $('.typeahead_to')
            .typeahead(null, {
                name: 'datypeahead_to',
                displayKey: 'value',
                source: OfficeDataAdapter.ttAdapter(),
                hint: (Metronic.isRTL() ? false : true),
                templates: {
                    suggestion: Handlebars.compile([
                        '<div class="media">',
                        '<div class="media-body">',
                        '<h4 class="media-heading">{{value}}</h4>',
                        '</div>',
                        '</div>',
                    ].join(''))
                }
            }
            )
            .on('typeahead:opened', onOpened)
            .on('typeahead:selected', onAutocompleted)
            .on('typeahead:autocompleted', onSelected);

    function onOpened($e) {
        //console.log('opened');
    }

    function onAutocompleted($e, datum) {
        DakOffice.setAutoDataIntoHiddenFields(datum, $e.currentTarget.id);
    }

    function onSelected($e, datum) {
        //console.log('selected');
    }


    $('[data-toggle="tooltip"]').tooltip('destroy');
    $('[data-toggle="tooltip"]').tooltip({'container':'body'});

    $('[title]').tooltip('destroy');
    $('[title]').tooltip({'container':'body'});


    $(document).off('click', '.dak_actions_radio');
    $(document).on('click', '.dak_actions_radio', function () {
		$('.dak_actions_radio').closest('span').removeClass('checked')
		$(this).parent('span').addClass('checked')
        if ($(this).checked == true || this.checked) {

            if ($(this).val() != 0) {
                $(this).closest('.form-group').find('#dak-actions').val($(this).data('title'));
//                $(this).closest('.form-group').find('#dak-actions').attr('readonly', 'readonly');
            } else {
//                $(this).closest('.form-group').find('#dak-actions').val('');
//                $(this).closest('.form-group').find('#dak-actions').val('');
                $(this).closest('.form-group').find('#dak-actions').removeAttr('readonly');
                clickModal();
            }
        }
    });

    $(document).on('click', '#selectAllDraft', function () {
        if (this.checked == true) {
            $('.dak_list_checkbox_to_select').each(function () {
                if (this.checked == false) {
                    this.click();
                }
            });
        } else {
            $('.dak_list_checkbox_to_select').each(function () {
                if (this.checked == true) {
                    this.click();
                }
            });
        }
    });

    $(document).off('click', '.btn-printPotro').on('click', '.btn-printPotro', function () {

        var id = $(this).closest('.inbox-details').find('.nothiVuktoKoronSingle').data('id');
        var dak_type = $(this).closest('.inbox-details').find('.nothiVuktoKoronSingle').data('dak-type');
        if(id == '' || typeof(id) =='undefined' || id == null ){
            id = jQuery('#dak_id').val();
        }
        if(dak_type == '' || typeof(dak_type) =='undefined' || dak_type == null ){
            dak_type = jQuery('#dak_type').val();
        }
        if(id == '' || typeof(id) =='undefined' || id == null ){
            console.log('id missing');
            return false;
        }
        if(dak_type == '' || typeof(dak_type) =='undefined' || dak_type == null ){
            console.log('type missing');
            return false;
        }
        showPdf('প্রিন্ট প্রিভিউ','<?= $this->Url->build(['controller' => 'Potrojari', 'action' => 'getPdfByDak',$selected_office_section['office_id']]) ?>/'+id+'/'+dak_type);
        $(".btn-pdf-margin").click();
    });


	$(document).off('click', '.btn-printpopPotro').on('click', '.btn-printpopPotro', function () {
//        var node = $(this).closest('.tab-pane.active').find('.templateWrapper')
        var node = $(this).closest('.tab-pane.active').find('.dakBodyPotro');
		showPrintView(node);
	});

    function getMovementHistory(dak_id, dak_type) {

        $('.showHistory').html('<div class="text-center"><i class="fa fa-spin "></i></div>');
        PROJAPOTI.ajaxSubmitDataCallback('<?=$this->Url->build(['controller' => 'DakMovements', 'action' => 'movementHistory'])?>',
            {dak_id: dak_id, dak_type: dak_type}, 'html', function (html) {
            $('.showHistory').html(html);
            $('.showHistory').parent().show();
            $('.showHistory').show();

                $('html, body'). stop(). animate({
                    scrollTop: ($(".showHistory").offset().top - 130)
                }, 1000);

            $('.movementHistoryShow').hide();
            $('.movementHistoryHide').show();

        });
    }

    function hideMovementHistory(){
        $('.showHistory').parent().hide();
        $('.showHistory').hide();
        $('.movementHistoryShow').show();
        $('.movementHistoryHide').hide();
    }

    function getPopUpPotro(href, title) {

        $('#responsiveModal').find('.modal-title').text('');
        $('#responsiveModal').find('.modal-body').html('');
        $.ajax({
            url: '<?php echo $this->request->webroot; ?>NothiMasters/showPopUp/' + href+'?nothi_part=0&token='+'<?= sGenerateToken(['file' => 0], ['exp' => time() + 60 * 300]) ?>',
            dataType: 'html',
            data: {'nothi_office':<?php echo $selected_office_section['office_id'] ?>},
            type: 'post',
            success: function (response) {
                $('#responsiveModal').modal('show');
                $('#responsiveModal').find('.modal-title').text(title);

                $('#responsiveModal').find('.modal-body').html(response);
            }
        });
    }

    $(document).off('click', '.showforPopup').on('click', '.showforPopup', function (e) {
        e.preventDefault();
        var title = $(this).attr('title').length > 0 ? $(this).attr('title') : $(this).attr('data-original-title');
        getPopUpPotro($(this).attr('href'), title);
    })

    $(document).ready(function() {
        var clonedHtml = $(".btnNothiVukto").parent().clone();
        $(".btnNothiVukto").parent().parent().parent().remove();
        $(".dataTables_length").prepend(clonedHtml);
        $(".btnNothiVukto").parent().css('margin-right', '5px');
        $(".btnNothiVukto").parent().find('[data-original-title]').tooltip('destroy').tooltip({'container':'body'});

        $('.dataTables_paginate').find('.prev').css('cssText', 'border-radius:5px 0 0 5px !important');
        $('.dataTables_paginate').find('.next').css('cssText', 'border-radius:0 5px 5px 0 !important');
        $('.dataTables_paginate').find('input').css('cssText', 'margin:0 !important;text-align:center');
    })
</script>