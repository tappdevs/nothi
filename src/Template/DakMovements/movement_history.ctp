<link rel="stylesheet" href="<?= $this->request->webroot;?>css/timeline.css">
<div class="general-item-list">
    <?php if(!empty($tracking_data)): ?>
    <div class="item-body" style="padding-bottom: 5px;color: grey;border-bottom: solid 4px #e9f7cf;"><?php echo '<b> বর্তমান অবস্থা: </b> &nbsp;' .__($tracking_data) ?></div>
    <?php endif; ?>
    <?php
    if (!empty($move_data)):
        foreach ($move_data as $dak_move) {
            ?>
            <div class="timeline-item">
                <div class="image">
                    <div>
                        <img src="<?=$dak_move['photo']?>" />
                        <span><?php echo $dak_move['move_date_time'] ?></span>
                    </div>
                </div>
                <div class="details">
                    <div>
                        <h1><?php echo __(PREROK).": ". h($dak_move['sender']) ?></h1>
                        <p>
                            <?php if(isset($dak_move['receiver'][1][0])) : echo "<b>". __(PRAPOK) .":</b> ". __($dak_move['receiver'][1][0]); endif; ?>
                            <?php if(isset($dak_move['receiver'][0])){
                                echo "<br/>";
                                echo "<b>". __(ANULIPI) .":</b> ";
                                $total_anulipi = count($dak_move['receiver'][0]);
                                foreach ($dak_move['receiver'][0] as $key => $dak_receiver) { ?>
                                    <?php echo __($dak_receiver); ?>
                                    <?php
                                    if (($total_anulipi-1) != $key) {
                                        echo "<br/>";
                                    }
                                    ?>
                                <?php }
                            } ?>

                            <br/>
                            <p style="padding-top: 10px;"><?php echo '<b> সিদ্ধান্ত: </b>' .__($dak_move['value']) ?></p>
                        </p>
                    </div>
                </div>
            </div>


            <?php
        }
    endif;
    ?>
</div>
