<link href="<?php echo CDN_PATH; ?>daptorik_preview/css/custom.css" rel="stylesheet" type="text/css"/>

<!-- END PAGE LEVEL STYLES -->
<!-- END PAGE LEVEL STYLES -->
<style>
    tfoot {
        padding: 2px;
        background-color: rgba(241, 241, 241, 0.73);
    }

    .footer {
        display: none;
    }

    .dak_sender_cell_list {
        position: absolute;
        border: 2px solid #CECECE;
        background: #F5F5F5;
        padding-top: 15px;
        overflow: auto;
        z-index: 9000;
        color: #000;
        left: 15px;
        width: 99%;
        text-align: left;
    }

    .checkbox input[type=checkbox], .checkbox-inline input[type=checkbox], .radio input[type=radio], .radio-inline input[type=radio] {
        margin-left: -10px !important;
    }

    input[type=checkbox], input[type=radio] {
        margin-top: 2px;
    }

    div.radio, div.checker {
        margin-top: -2px;
    }

    .newInbox {
        background-color: #C7DAE0;
    }

    h3 {
        margin-top: 0px;
        margin-bottom: 0px;
    }
    .editable{
        border: none;
        word-break:break-word;
        word-wrap:break-word
    }
    .nav-tabs .dropdown-menu {
        font-size: 11pt!important;
    }
    .tab-content, .active{
        background-color:  #e9ffcc!important;
    }

    #dbl_click > tr > td{
        background-color: transparent;
    }

    #dbl_click > tr:hover {
        box-shadow: inset #cecece 0px 0px 4px 1px;
        cursor: pointer;
    }

</style>

<?php
if (isset($other_organogram_activities_setting[0]['organogram_id'])) {
	$organogram_id = $other_organogram_activities_setting[0]['organogram_id'];
} else {
	$organogram_id = $other_organogram_activities_settings[0]['organogram_id'];
}
?>

<div class="portlet light" style="padding: 25px 15px 15px 15px;">
	<div class="portlet-title">
		<h3>শেয়ারকৃত ডাকসমূহ</h3>
	</div>
	<div class="portlet-body">
		<div class="row">
			<div class="col-md-2">
				<span>পদবি নির্বাচন করুন</span>
			</div>
			<div class="col-md-10">
				<?php echo $this->Form->create('', ['method' => 'post']); ?>
				<select class="form-control select2" name="organogram_id" onchange="submit()">
					<?php foreach ($other_organogram_activities_settings as $other_organogram_activities_setting): ?>
					<?php $designation_info = designationInfo($other_organogram_activities_setting['organogram_id']); ?>
					<option  <?=$organogram_id == $other_organogram_activities_setting['organogram_id'] ? 'selected' : ''; ?> value="<?=$other_organogram_activities_setting['organogram_id']?>"><?= $designation_info['officer_name'] ?>, <?= $designation_info['officer_designation_label'] ?>, <?= $designation_info['office_unit_name'] ?></option>
					<?php endforeach;?>
				</select>
				<?php echo $this->Form->end(); ?>
			</div>
		</div>
		<hr/>

		<ul class="nav nav-tabs nav-justified">
			<li class="inbox bold active">
				<a href="#inbox" data-toggle="tab" aria-expanded="true">
					<span class ="font-green"> আগত ডাক </span>
				</a>
			</li>
			<li class="sent bold">
				<a href="#sent" data-toggle="tab" aria-expanded="true">
					<span class ="font-purple"> প্রেরিত ডাক </span>
				</a>
			</li>
		</ul>

		<div class="tab-content">
			<div class="tab-pane fade active in" id="inbox">
				<div class="table-container data-table-dak">
                    <div class="inbox-loading" style="display: none;"><img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp;লোড হচ্ছে। একটু অপেক্ষা করুন... </span></div>
					<div class="inbox-content">
						<div class="row">
							<div class="col-md-12">
								<div class="table-container data-table-dak">
									<table class="table table-bordered table-striped table-hover" id="datatable_dak">
										<thead>
										<tr role="row" class="heading">
											<th style='width: 7%!important; text-align: center;'> কার্যক্রম</th>
											<th style='width: 17%!important'><?php echo __(UTSHO) ?></th>
											<th style='width: 20%!important'><?php echo __(BISHOY) ?></th>
											<th style='width: 25%!important'><?php echo __(PRAPOK) ?></th>
											<th style='width: 10%!important'><?php echo __(PURBOBORTI)." ".SHIDDHANTO ?></th>
											<th style='width: 9%!important'><?php echo __(TARIKH) ?></th>
											<th style='width: 4%'><?php echo __(DHORON) ?></th>
											<th style='width: 3%'>ধরন</th>
										</tr>
										</thead>
										<tbody id="dbl_click">
										</tbody>
									</table>

								</div>
							</div>
						</div>
					</div>
					<div class="inbox-details" style="display: none;"></div>
				</div>
			</div>
			<div class="tab-pane fade" id="sent">
				<div class="table-container data-table-dak">
					<div class="inbox-content">
						<div class="row">
							<div class="col-md-12">
								<div class="table-container data-table-dak">
									<table class="table table-bordered table-striped table-hover" id="datatable_dak">
										<thead>
										<tr role="row" class="heading">
											<th style='width: 17%!important'><?php echo __(UTSHO) ?></th>
											<th style='width: 20%!important'><?php echo __(BISHOY) ?></th>
											<th style='width: 25%!important'><?php echo __(PRAPOK) ?></th>
											<th style='width: 10%!important'><?php echo __(PURBOBORTI)." ".SHIDDHANTO ?></th>
											<th style='width: 9%!important'><?php echo __(TARIKH) ?></th>
											<th style='width: 4%'><?php echo __(DHORON) ?></th>
											<th style='width: 3%'>ধরন</th>
										</tr>
										</thead>
										<tbody id="dbl_click">
										</tbody>
									</table>

								</div>
							</div>
						</div>
					</div>
					<div class="inbox-details" style="display: none;"></div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?php echo $this->request->webroot; ?>daptorik_preview/js/permitted_dak_movement.js?v=<?= js_css_version ?>"></script>

<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script src="<?php echo CDN_PATH; ?>assets/global/scripts/datatable.js"></script>
<script src="<?php echo CDN_PATH; ?>daptorik_preview/js/tbl_permitted_dak_list_ajax.js"></script>
<script src="<?php echo CDN_PATH; ?>assets/admin/pages/scripts/table-advanced.js"></script>


<script type="text/javascript">

    $.extend(true, $.fn.dataTable.defaults, {
        "ordering": false
    });
    $(function() {
        //"#datatable_dak"
        InboxDakMovement.init('inbox', '<?=$organogram_id?>');
	    TableAjax.init('inbox', 'dakMovements/permitted_dak_view/<?=$organogram_id?>/');
    });

    function onOpened($e) {
        //console.log('opened');
    }

    function onAutocompleted($e, datum) {
        DakOffice.setAutoDataIntoHiddenFields(datum, $e.currentTarget.id);
    }

    function onSelected($e, datum) {
        //console.log('selected');
    }

    $(document).ready(function() {
        var clonedHtml = $(".btnNothiVukto").parent().clone();
        $(".btnNothiVukto").parent().parent().parent().remove();
        $(".dataTables_length").prepend(clonedHtml);
        $(".btnNothiVukto").parent().css('margin-right', '5px');
        $(".btnNothiVukto").parent().find('[data-original-title]').tooltip('destroy').tooltip({'container':'body'});

        $('.dataTables_paginate').find('.prev').css('cssText', 'border-radius:5px 0 0 5px !important');
        $('.dataTables_paginate').find('.next').css('cssText', 'border-radius:0 5px 5px 0 !important');
        $('.dataTables_paginate').find('input').css('cssText', 'margin:0 !important;text-align:center');
    });

    function getMovementHistory(dak_id, dak_type) {

        $('.showHistory').html('<div class="text-center"><i class="fa fa-spin "></i></div>');
        PROJAPOTI.ajaxSubmitDataCallback('<?=$this->Url->build(['controller' => 'DakMovements', 'action' => 'movementHistory'])?>',
            {dak_id: dak_id, dak_type: dak_type}, 'html', function (html) {
                $('.showHistory').html(html);
                $('.showHistory').parent().show();
                $('.showHistory').show();

                $('html, body'). stop(). animate({
                    scrollTop: ($(".showHistory").offset().top - 130)
                }, 1000);

                $('.movementHistoryShow').hide();
                $('.movementHistoryHide').show();

            });
    }
    function hideMovementHistory(){
        $('.showHistory').parent().hide();
        $('.showHistory').hide();
        $('.movementHistoryShow').show();
        $('.movementHistoryHide').hide();
    }

    function clickOnDak(element, inbox_type) {
        var newElement = $(element).parent().find('.showDetailsDak'+inbox_type);
        InboxDakMovement.loadDetailsData(newElement, inbox_type, '<?=$organogram_id?>');
    }
    $(document).on('click', 'tbody#dbl_click>tr>td', function () {
        if ($('.nav.nav-tabs.nav-justified li.sent.active').length > 0) {
            clickOnDak(this, 'Sent');
        } else if ($('.nav.nav-tabs.nav-justified li.nothivukto.active').length > 0) {
            clickOnDak(this, 'NothiVukto');
        } else if ($('.nav.nav-tabs.nav-justified li.nothijato.active').length > 0) {
            clickOnDak(this, 'NothiJato');
        } else if ($('.nav.nav-tabs.nav-justified li.archive.active').length > 0) {
            clickOnDak(this, 'Archive');
        } else {
            if ($(this).context.cellIndex > 1) {
                clickOnDak(this, 'Inbox');
            }
        }
    });

    $(document).ajaxStop(function() {
        if($(".inbox-header").length > 0) {
            $(".inbox-header.inbox-view-info:visible").css('margin-left', '0px');
            $(".inbox-header.inbox-view-info:visible").css('margin-right', '0px');
        }
    })
</script>