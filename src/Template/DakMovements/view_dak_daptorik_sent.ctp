<style>
    .pager {
        margin-top: 0px;
    }
    .pager b {
        font-weight: initial;
    }

    .inbox .inbox-header h1 {
        margin-bottom: 0px;
    }
</style>
<div class="inbox-header inbox-view-header" style="margin-bottom: -10px!important;margin-top: -10px!important;height: 32px;">
    <div class="pull-left ">
        <button class="btn btn-primary  btn-sm sent-discard-btn">
            <i class="glyphicon glyphicon-arrow-left"></i> প্রেরিত ডাকসমূহ
        </button>
    </div>
    <div class="pull-right">
        <nav>
            <ul class="pager">
                <li>
                    <?php echo __("দাপ্তরিক ডাক"); ?> &nbsp; &nbsp; <b><?php echo $this->Number->format($si); ?></b> /
                    <b><?php echo $this->Number->format($totalRec); ?></b>
                </li>
                <li><a href="#" data-si="<?php echo $si - 1; ?>" data-dak-type='Daptorik'
                       class="btn btn-primary showPaginateDetailsDakSent" <?php echo($si == 1 ? "style='visibility: hidden'" : "style='visibility: show;background-color:#3071a9;'"); ?>><</a>
                </li>
                <li><a href="#" data-si="<?php echo $si + 1; ?>" data-dak-type='Daptorik'
                       class="btn btn-primary showPaginateDetailsDakSent" <?php echo($si == $totalRec ? "style='visibility: hidden'" : "style='visibility: show;background-color:#3071a9;'"); ?> >></a>
                </li>
            </ul>
        </nav>
    </div>
</div>
<div class="inbox-header inbox-view-info" style="margin-top: 16px;border-top: solid #e9f7cf 5px;">
    <h3 class="">
        <div class="row">
            <div class="col-md-1 col-sm-2" style="padding-right:0;">
                বিষয়:
            </div>
            <div class="col-md-8 col-sm-7" style="word-break:break-word;">
                <?php echo h($dak_daptoriks['dak_subject']); ?>
            </div>
            <div class="col-md-3 col-sm-3 text-right">
                <?php
                if (h($dak_daptoriks['dak_security_level']) > 1) {
                    echo '<button class="btn btn-default btn-sm" data-title="' . $dak_security_txt . '" title="' . $dak_security_txt . '"><i class="glyphicon glyphicon glyphicon-lock" ' . ($dak_daptoriks['dak_security_level'] < 2 ? '' : ($dak_daptoriks['dak_security_level'] == 2 ? 'style="color:#FFCD32;"' : ($dak_daptoriks['dak_security_level'] == 3 ? 'style="color:red;"' : ($dak_daptoriks['dak_security_level'] == 4 ? 'style="color:#881C1C;"' : 'style="color:orange;"')))) . '> </i></button>';
                }
                if ($dak_priority > 1) {
                    echo '<button class="btn btn-default btn-sm"  data-title="' . $dak_priority_txt . '" title="' . $dak_priority_txt . '"><i class="glyphicon glyphicon glyphicon-star" ' . ($dak_priority < 2 ? '' : ($dak_priority == 2 ? 'style="color:#FFCD32;"' : ($dak_priority == 3 ? 'style="color:green;"' : 'style="color:red;"'))) . '> </i></button>';
                }
                ?>
                <?php if ($is_revert_enabled == 1) { ?>
                    <button class="btn btn-danger btn-sm dak_revert" data-title="ডাক ফেরত আনুন" title="ডাক ফেরত আনুন"
                            data-message-id="<?php echo $dak_daptoriks['id']; ?>"
                            data-message-type="<?php echo DAK_DAPTORIK; ?>">
                        <i class="glyphicon glyphicon-retweet"> </i>
                    </button>
                <?php } ?>
            </div>
        </div>
    </h3>
</div>
<div class="well inbox-view-info"
     style="vertical-align: middle;margin-bottom: 5px;padding-left: 5px; padding-right: 5px;">
    <div class="row">
        <div class="col-md-3 col-sm-3 text-right">
            <?php echo "ডকেটিং নম্বর:  " . enTobn($dak_daptoriks['docketing_no']); ?>
        </div>
        <div class="col-md-4 col-sm-4 text-right">
            <?php echo "স্মারক নম্বর:  " . h($dak_daptoriks['sender_sarok_no']); ?>
        </div>
        <div class="col-md-3 col-sm-3 text-right">
            <?php echo (isset($dak_attachments) && count($dak_attachments) > 0) ? '<i class="glyphicon glyphicon-paperclip"> </i>' : '';
            echo '&nbsp;&nbsp;' . h($move_date); ?>
        </div>
    </div>
</div>
<div class="inbox-view-info">
    <div class="row">
        <div class="col-md-1 col-sm-2 text-right">
            <i class="icon-send"></i>
            <span class="bold"><?php echo UTSHO . ': '; ?></span>
        </div>
        <div class="col-md-10 col-sm-10">
            <span class=""><?php echo
                    (!empty($dak_daptoriks['sender_name']) ? (h($dak_daptoriks['sender_name']) . ", ") : '') . (!empty($dak_daptoriks['sender_officer_designation_label']) ? (h($dak_daptoriks['sender_officer_designation_label']) . ", ") : ' ') . h($dak_daptoriks['sender_office_name']);
                ?></span>
        </div>
    </div>
    <div class="row">
        <div class="col-md-1 col-sm-2 text-right">
            <span class="bold">  <?= PRAPOK . ": " ?></span>
        </div>
        <div class="col-md-10 col-sm-10">
            <i class="icon-send"></i>
            <span class=""><?php echo $sender_office; ?></span>
        </div>
    </div>
</div>
<?php
echo $this->element('dak_view',['dak_attachments'=>$dak_attachments,'dak'=>$dak_daptoriks]);
?>
<hr style="margin: 10px 0">
<div class="row">
    <div class="col-md-12 col-lg-12">

        <div class="pull-left" style="margin-left: 15px;">
            <button type="button" class="btn btn-sm btn-default movementHistoryShow" onclick="getMovementHistory(<?php echo $dak_daptoriks['id'] ?>, '<?=$dak_type?>')"><?= __(DAK_GOTIBIDHI) . " " . __("View") ?> <i class="fa fa-chevron-down"></i></button>
            <button type="button" class="btn btn-sm btn-default movementHistoryHide" style="display:none;" onclick="hideMovementHistory()"><?= __(DAK_GOTIBIDHI) . " " .__("Cancel") ?> <i class="fa fa-chevron-up"></i></button>
        </div>
    </div>
</div>
<hr style="margin: 10px 0 0 0">
<div class="portlet light" style="display:none;">
    <div class="portlet-body showHistory">

    </div>
</div>

<div id="responsivepotalaLog" class="modal fade" tabindex="-1" aria-hidden="true" data-backdrop="static"
     data-keyboard="false">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">সংলাগ</h4>
            </div>
            <div class="modal-body" style="background-color: #828282;">
                <div class="scroller" style="height:100%; max-height: 500px;" data-always-visible="1"
                     data-rail-visible1="1">

                </div>
            </div>
        </div>
    </div>
</div>
<?php
if (!empty($dak_id)) {
    echo $this->Form->hidden('dak_id', ['value' => $dak_id, 'id' => 'dak_id']);
}
if (!empty($dak_type)) {
    echo $this->Form->hidden('dak_type', ['value' => $dak_type, 'id' => 'dak_type']);
}
?>
<script type="text/javascript">

    Metronic.initSlimScroll('.scroller');
    $(document).off('click', '.songlapbutton').on('click', '.songlapbutton', function () {
        $('#responsivepotalaLog').modal('show');
        $('#responsivepotalaLog').find('.scroller').html('<img src="' + js_wb_root + 'assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp;লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');

        var id = $(this).attr('id');
        var url = $(this).data('url');
        var type = $(this).data('type');
        var officeid = parseInt($(this).data('nothi-office'));

        if (typeof(id) != 'undefined') {
            $.ajax({
                url: '<?php echo $this->Url->build(['controller' => 'NothiMasters', 'action' => 'showSonglap']) ?>/' + id + '/' + officeid,
                method: 'post',
                dataType: 'json',
                cache: false,
                success: function (response) {

                    var attachmenttype = response.attachment_type;
                    if (response.attachment_type == 'text') {
                        $('#responsivepotalaLog').find('.scroller').html('<div style="background-color: #fff; max-width:950px; min-height:815px; max-height: 815px; margin:0 auto; page-break-inside: auto;">' + response.potro_cover + "<br/>" + response.content_body + "</div>");
                    } else if ((attachmenttype.substring(0, 15)) == 'application/vnd' || (attachmenttype.substring(0, 15)) == 'application/ms') {
                        $('#responsivepotalaLog').find('.scroller').html('<iframe style=" width:100%; height: 700px;" src="https://docs.google.com/gview?url=' + encodeURIComponent('<?php echo FILE_FOLDER ?>' + response.file_name ) + '&embedded=true" style="width:600px; height:500px;" frameborder="0"></iframe>');

                    } else if ((attachmenttype.substring(0, 5)) != 'image') {
                        $('#responsivepotalaLog').find('.scroller').html('<embed src="' + '<?php echo $this->request->webroot . 'content/' ?>' + response.file_name + '" style=" width:100%; height: 700px;" type="' + response.attachment_type + '"></embed>');
                    } else {
                        $('#responsivepotalaLog').find('.scroller').html('<div class="text-center"><a href="' + '<?php echo $this->request->webroot . 'content/' ?>' + response.file_name + '" data-gallery="multiimages"  data-toggle="lightbox" data-title="পত্র নম্বর:  ' + response.nothi_potro_page_bn + '" data-footer="" ><img class="zoomimg img-responsive"  src="' + '<?php echo $this->request->webroot . 'content/' ?>' + response.file_name + '" alt="ছবি পাওয়া যায়নি"></a></div>');
                    }
                },
                error: function (err, status, rspn) {
                    $('#responsivepotalaLog').find('.scroller').html('');
                }
            })
        }
        else if (typeof(url) != 'undefined') {
            if ((type.substring(0, 15)) == 'application/vnd' || (type.substring(0, 15)) == 'application/ms') {
                $('#responsivepotalaLog').find('.scroller').html('<iframe style=" width:100%; height: 700px;" src="https://docs.google.com/gview?url=' + url + '&embedded=true" style="width:600px; height:500px;" frameborder="0"></iframe>');

            } else if ((type.substring(0, 5)) != 'image') {
                $('#responsivepotalaLog').find('.scroller').html('<embed src="' + url + '" style=" width:100%; height: 700px;" type="' + type + '"></embed>');
            } else {
                $('#responsivepotalaLog').find('.scroller').html('<div class="text-center"><a href="' + url + '" data-gallery="multiimages"  data-toggle="lightbox" data-title="সংলাগ" data-footer="" ><img class="zoomimg img-responsive"  src="' + url + '" alt="সংলাগ পাওয়া যায়নি"></a></div>');
            }
        }
    });


</script>