<style>
    .pager {
        margin-top: 0px;
    }

    .inbox .inbox-header h1 {
        margin-bottom: 0px;
    }

    .attachmentsRecordNothijato{
        display: none;
    }

    .attachmentsRecordNothijato.active{
        display: block;
    }
</style>
<div class="inbox-header " >
    <h3 class="">
        <div class="row">
            <div class="col-md-1 col-sm-2">
                বিষয়:
            </div>
            <div class="col-md-8 col-sm-7"  style="word-break:break-word;">
                <?php echo $dak_daptoriks['dak_subject'] ?>
            </div>
            <div class="col-md-3 col-sm-3 text-right">
            <?php
            echo '<button class="btn btn-default btn-sm"><i title="' . $dak_security_txt . '" class="glyphicon glyphicon glyphicon-lock" ' . ($dak_daptoriks['dak_security_level'] < 2 ? '' : ($dak_daptoriks['dak_security_level'] == 2 ? 'style="color:#FFCD32;"' : ($dak_daptoriks['dak_security_level'] == 3 ? 'style="color:red;"' : ($dak_daptoriks['dak_security_level'] == 4 ? 'style="color:#881C1C;"' : 'style="color:orange;"')))) . '> </i></button>';
            echo '<button class="btn btn-default btn-sm"><i title="' . $dak_priority_txt . '" class="glyphicon glyphicon glyphicon-star" ' . ($dak_priority < 2 ? '' : ($dak_priority == 2 ? 'style="color:#FFCD32;"' : ($dak_priority == 3 ? 'style="color:green;"' : 'style="color:red;"'))) . '> </i></button>';
            if($dak_daptoriks['is_rollback_to_dak'] == 1){
                echo '<p class="font-sm text-danger">এই ডাকটি ফেরত আনা হয়েছে</p>';
            }
            else if($is_allowed_to_sent_back){
                echo '<button class="btn btn-danger btn-sm btn-rollback-to-dak" data-toggle="tooltip" data-title="ডাক ফেরত আনুন" title="ডাক ফেরত আনুন"  data-placement="bottom"><i  class="glyphicon glyphicon-retweet"> </i></button>';
            }
        ?>
            </div>
        </div>
    </h3>
</div>
<div class="well inbox-view-info" style="vertical-align: middle;margin-bottom: 5px;padding-left: 5px; padding-right: 5px;padding: 5px 15px;">
    <div class="row">
          <div class="col-md-3 col-sm-3 text-right">
            <?php
            echo (isset($dak_attachments) && count($dak_attachments) > 0) ? '<i class="glyphicon glyphicon-paperclip"> </i>'
                    : '';
            echo '&nbsp;&nbsp;'.$move_date;
            ?>
        </div>
        <div class="col-md-3 col-sm-3 text-right">
            <?php echo "ডকেটিং নম্বর:  ".enTobn($dak_daptoriks['docketing_no']); ?>
        </div>
        <div class="col-md-4 col-sm-4 text-right">
            <?php echo "স্মারক নম্বর:  ".$dak_daptoriks['sender_sarok_no']; ?>
        </div>

    </div>
</div>
<div class="inbox-view-info">
    <div class="row">
         <div class="col-md-2 col-sm-2 text-right">
            <i class="icon-send"></i>
            <span
                class="bold"><?php echo UTSHO . ': ' ; ?></span>
        </div>
        <div class="col-md-10 col-sm-10">
            <span class=""><?php
                echo  (!empty($dak_daptoriks['sender_name'])?($dak_daptoriks['sender_name'] . ", "):'') . (!empty($dak_daptoriks['sender_officer_designation_label'])?($dak_daptoriks['sender_officer_designation_label'] . ", "):' '). $dak_daptoriks['sender_office_name'];
                ?></span>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 col-sm-2 text-right">
               <i class="icon-send"></i>
               <span class="bold"><?php echo PRAPOK.": "; ?></span>
           </div>
           <div class="col-md-10 col-sm-10">
               <span class=""><?php
                   echo $sender_office;
                   ?></span>
           </div>
    </div>
</div>


<?php
$attachmentGroup = array();
$attachmentGroupimage = array();
if (isset($dak_attachments) && count($dak_attachments) > 0) {
    $i = 1;
    foreach ($dak_attachments as $single_data) {
        $file_name = $single_data['file_name'];
        $file_name = explode("/", $file_name);
        $file_name = $file_name[count($file_name) - 1];
        if($single_data['attachment_type']=='text')continue;
        $single_data['attachment_type'] = get_file_type($single_data['file_name']);
        $f_type = explode('/', $single_data['attachment_type']);
        $file_name_arr[0] = urldecode($file_name);
        $file_name_arr[1] = $f_type[1];

        //$single_data['file_name'] = $this->request->webroot . 'content/' . $single_data['file_name'];

        if (isset($file_name_arr[1])) {
            if (!empty($single_data['file_name'])) {
                if (strtolower($file_name_arr[1]) == 'jpg' || strtolower($file_name_arr[1]) == 'jpeg' || strtolower($file_name_arr[1]) == 'png') {
                    $attachmentGroupimage['image'][] = array("file_type" => $single_data['attachment_type'], "file_name" => $single_data['file_name'], "name" => $file_name_arr[0],'user_file_name' => $single_data['user_file_name'], 'id' => $single_data['id']);
                } elseif (strtolower($file_name_arr[1]) == 'mp3' || strtolower($file_name_arr[1]) == 'mpeg') {
                    $attachmentGroup['mp3'][] = array("file_type" => $single_data['attachment_type'], "file_name" => $single_data['file_name'], "name" => $file_name_arr[0],'user_file_name' => $single_data['user_file_name'], 'id' => $single_data['id']);
                } elseif (strtolower($file_name_arr[1]) == 'wmv' || strtolower($file_name_arr[1]) == 'mp4' || strtolower($file_name_arr[1]) == 'mkv' || strtolower($file_name_arr[1]) == 'avi' || strtolower($file_name_arr[1]) == '3gp' || strtolower($file_name_arr[1]) == 'mpg') {
                    $attachmentGroup['video'][] = array("file_type" => $single_data['attachment_type'], "file_name" => $single_data['file_name'], "name" => $file_name_arr[0], 'id' => $single_data['id'],'user_file_name' => $single_data['user_file_name']);
                } elseif (strtolower($file_name_arr[1]) == 'pdf') {
                    $attachmentGroup['pdf'][] = array("file_type" => $single_data['attachment_type'], "file_name" => $single_data['file_name'], "name" => $file_name_arr[0], 'id' => $single_data['id'],'user_file_name' => $single_data['user_file_name']);
                } else
                    $attachmentGroup["other"][] = array("file_type" => $single_data['attachment_type'], "file_name" => $single_data['file_name'], "name" => $file_name_arr[0], 'id' => $single_data['id'],'user_file_name' => $single_data['user_file_name']);
//                                $attachmentGroup[strtolower($file_name_arr[1])][] = array("file_type"=>$single_data['attachment_type'],"file_name"=>$single_data['file_name'],"name"=>$file_name_arr[0]);
            }
        } else {
            if (!empty($single_data['file_name'])) $attachmentGroup["other"][] = array("file_type" => $single_data['attachment_type'], "file_name" => $single_data['file_name'], 'id' => $single_data['id'],'user_file_name' => $single_data['user_file_name']);
        }
    }
}
$attachmentGroup = $attachmentGroupimage + $attachmentGroup;
?>
<div class="inbox-view-detail">
    <div class="inbox-view">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-sm-12">
                <span class="bold">বিবরণ : </span><i class="fa fa-print btn-printPotro" style="cursor: pointer;"></i>
                <div class='dakBodyPotro'
                     style="overflow:auto; background-color: #fff; width:100%; max-height: 8in; margin:0 auto; page-break-inside: auto; padding: 10px; border: 1px solid #aaa;">
                    <div id="docatingNot"
                         style="position: relative; top: 0px; left:30px; font-weight: bold; font-size:12px; color: #733E3E;">
                        <?php echo "ডকেটিং নম্বর:  " . enTobn($dak_daptoriks['docketing_no']); ?>
                    </div>

                    <?php if (!empty($dak_daptoriks['dak_cover'])){ ?>
                        <div style="height: 815px; border-bottom: 2px solid #eee; margin-bottom: 10px;" >
                            <?php echo $dak_daptoriks['dak_cover']; ?>
                        </div>
                    <?php }else{
                        if(!empty($dak_daptoriks['meta_data'])) {
                            $header = jsonA($dak_daptoriks['meta_data']);
                            echo(!empty($header['potro_header_banner']) ? ('<div style="text-align: ' . ($header['banner_position']) . '!important;"><img src="' . $this->request->webroot . 'getContent?file=' . base64_decode($header['potro_header_banner']) . '&token=' . sGenerateToken(['file' => base64_decode($header['potro_header_banner'])], ['exp' => time() + 60 * 300]) . '" style="width:' . ($header['banner_width']) . ';height: 60px;max-width: 100%;"/></div>') : '');
                        }
                    } ?>
                    <?php echo empty($dak_daptoriks['description'])?$dak_daptoriks['dak_description']:$dak_daptoriks['description']; ?>
                </div>
            </div>
        </div>
        <br>
            <div class="row">
                <div class='col-md-12 col-lg-12 col-sm-12'>
                    <div class="portlet box green" style="border-top: 1px solid #4bc75e;">
                        <div class="portlet-body">
                            <?php
                             if (isset($dak_attachments) && count($dak_attachments) > 0) {
                                 $total = count($dak_attachments);
                             }
                            ?>
                            <div class="portlet light">
                                <div class="portlet-title">
                                    <div class="row">
                                        <div class="col-lg-7 col-md-7 col-sm-7 text-left">
                                            <nav>
                                                <ul class="pager">
                                                    <li><a href="javascript:void(0);"
                                                           class="imagePrevNothijato btn btn-sm default disabled"><<</a>
                                                    </li>
                                                    <li>
                                                        <span id="nothiJatoAttachementNumber">
                                                            <?= entobn(1) ?>
                                                        </span>
                                                    </li>
                                                    <li><a href="javascript:void(0);"
                                                           class="imageNextNothijato btn btn-sm default">>></a>
                                                    </li>
                                                </ul>
                                            </nav>
                                        </div>
                                        <div class="col-lg-5 col-md-5 col-sm-5 text-right">
                                            <p> মোট: <b><?php echo $this->Number->format($total); ?>টি</b></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="row divcenter">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="Nothijato_details">
                                                <?php
                                                if (!empty($dak_attachments)) {
                                                    echo "<hr>";
                                                    $i = 0;
                                                    foreach ($dak_attachments as $row) {
                                                        echo '<div id_ar="'.($i).'" id_en="'.$row['id'].'"  id_bn="'.$row['id'].'" class="attachmentsRecordNothijato '.($i
                                                        == 0 ? 'active first' : ($i == ($total - 1) ? 'last' : '')).'">';
                                                        ?>
                                                        <?php
                                                        if ($row['attachment_type'] == 'text') {

                                                                echo '<div style="background-color: #fff; max-width:950px; min-height:815px; max-height: 815px; margin:0 auto; page-break-inside: auto;">'.html_entity_decode($row['content_body'])."</div>";
                                                        }
                                                        elseif (substr($row['attachment_type'], 0, 5)
                                                            != 'image'
                                                        ) {
                                                            if (substr($row['attachment_type'], 0,
                                                                    strlen('application/vnd')) == 'application/vnd'
                                                                || substr($row['attachment_type'],
                                                                    0, strlen('application/ms')) == 'application/ms'
                                                            ) {
                                                                $url = urlencode(FILE_FOLDER.$row['file_name'].'?token='.sGenerateToken(['file'=>$row['file_name']],['exp'=>time() + 60*300]));
                                                                echo '<iframe style=" width:100%; height: 700px;" src="https://docs.google.com/gview?url='. $url.'&embedded=true" style="width:600px; height:500px;" frameborder="0"></iframe>';
                                                            }
                                                            else {
//                echo '<iframe style=" width:100%; height: 700px;" src="https://docs.google.com/gview?url='.$this->request->webroot . 'content/'.$row['file_name'].'&embedded=true" style="width:600px; height:500px;" frameborder="0"></iframe>';

                                                                echo '<embed src="'. $this->request->webroot . 'content/'. $row['file_name'].'?token='.sGenerateToken(['file'=>$row['file_name']],['exp'=>time() + 60*300]).'" style=" width:100%; height: 700px;" type="'.$row['attachment_type'].'"></embed>';
                                                            }
                                                        }
                                                        else {
                                                            echo '<div class="text-center"><img class=" img-responsive"  src="'.$this->request->webroot . 'content/' . $row['file_name'].'?token='.sGenerateToken(['file'=>$row['file_name']],['exp'=>time() + 60*300]).'" alt="ছবি পাওয়া যায়নি"></div>';
                                                        }
                                                        echo "</div>";
                                                        $i++;
                                                    }
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</div>
<?php
   echo $this->Form->hidden('dak_id',['value' => $dak_id,'id' => 'dak_id']);
   echo  $this->Form->hidden('dak_type',['value' => $dak_type,'id' => 'dak_type']);
?>
<script type="text/javascript" src="<?=CDN_PATH?>projapoti-nothi/js/nothijato_related.js?v=<?= js_css_version ?>"></script>
<script>
        $(document).off("click", '.imagePrevNothijato').on('click', '.imagePrevNothijato', function () {
         var firstndex = ($('.attachmentsRecordNothijato').first().index() == -1 ? 0 : $('.attachmentsRecordNothijato').first().index());
        var nowIndex = $('.attachmentsRecordNothijato.active').index();
        var lastIndex = $('.attachmentsRecordNothijato').last().index() == -1 ? 0 : $('.attachmentsRecordNothijato').last().index();
        if (nowIndex < lastIndex) {
            $('.imageNextNothijato').removeClass('disabled');
        }

        if (nowIndex <= 0 ) {
            $('.imageNextNothijato').removeClass('disabled');
            $(this).addClass('disabled');
            return;
        }
        if (nowIndex == firstndex ) {
            $('.imagePrevNothijato').removeClass('disabled');
            $(this).addClass('disabled');
            return;
        }

        if ($('.attachmentsRecordNothijato').eq(nowIndex).hasClass('first') == false) {
            $('.attachmentsRecordNothijato').removeClass('active');
            if(firstndex == 1){
                $('.attachmentsRecordNothijato').eq(nowIndex-2).addClass('active');
                $("#nothiJatoAttachementNumber").html(BntoEn(nowIndex - 1));
            }
            else{
                 $('.attachmentsRecordNothijato').eq(nowIndex-1).addClass('active');
                   $("#nothiJatoAttachementNumber").html(BntoEn(nowIndex));
            }
        } else {
            $('.imagePrevNothijato').addClass('disabled');
            return;
        }

    });
    $(document).off('click', '.imageNextNothijato').on('click', '.imageNextNothijato', function () {
        var firstndex = ($('.attachmentsRecordNothijato').first().index() == -1 ? 0 : $('.attachmentsRecordNothijato').first().index());
        var nowIndex = $('.attachmentsRecordNothijato.active').index();
        var lastIndex = $('.attachmentsRecordNothijato').last().index() == -1 ? 0 : $('.attachmentsRecordNothijato').last().index();
        if (nowIndex >= firstndex) {
            $('.imagePrevNothijato').removeClass('disabled');
        }
        if (lastIndex == 0 || nowIndex >= lastIndex) {
            $(this).addClass('disabled');
            return;
        }
            $('.attachmentsRecordNothijato').removeClass('active');
            if(firstndex == 1){
                $('.attachmentsRecordNothijato').eq(nowIndex).addClass('active');
                  $("#nothiJatoAttachementNumber").html(BntoEn(nowIndex+1));
            }
            else{
                 $('.attachmentsRecordNothijato').eq(nowIndex + 1).addClass('active');
                   $("#nothiJatoAttachementNumber").html(BntoEn(nowIndex + 2));
            }
            
            return;
    });
        $('[data-title]').tooltip({'placement':'bottom'});

        $(document).off('click', '.btn-printPotro').on('click', '.btn-printPotro', function () {

            var id = $(this).closest('.inbox-details').find('.nothiVuktoKoronSingle').data('id');
            var dak_type = $(this).closest('.inbox-details').find('.nothiVuktoKoronSingle').data('dak-type');
            if(id == '' || typeof(id) =='undefined' || id == null ){
                id = jQuery('#dak_id').val();
            }
            if(dak_type == '' || typeof(dak_type) =='undefined' || dak_type == null ){
                dak_type = jQuery('#dak_type').val();
            }
            if(id == '' || typeof(id) =='undefined' || id == null ){
                console.log('id missing');
                return false;
            }
            if(dak_type == '' || typeof(dak_type) =='undefined' || dak_type == null ){
                console.log('type missing');
                return false;
            }
            showPdf('প্রিন্ট প্রিভিউ','<?= $this->Url->build(['controller' => 'Potrojari', 'action' => 'getPdfByDak',$selected_office_section['office_id']]) ?>/'+id+'/'+dak_type)
        });

</script>