<div class="inbox-compose-btn">
    <button class="btn blue dak_forward_btn"
            data-message-id="<?php echo $dak_nagoriks['id']; ?>"
            data-dak-to="<?php echo $dak_nagoriks['receiving_officer_designation_id']; ?>"
            data-dak-from="<?php echo $dak_nagoriks['sender_officer_designation_id']; ?>"
        ><i class="fa fa-check"></i>Send
    </button>
    <!-- 		<button class="btn">Discard</button> -->
    <!-- 		<button class="btn">Draft</button> -->
</div>
<div class="inbox-view-info">
    <div class="row">
        <div class="col-md-7">
            <i class="icon-send"></i>
            <span class="bold"> প্রেরক : <?php echo $sender_office; ?></span>
        </div>
    </div>
    <hr/>
    <div class="row">
        <div class="col-md-7">
            <i class="icon-send"></i>
            <span class="bold"> প্রাপক : <?php echo $receiver_office; ?></span>
        </div>
    </div>
    <hr/>
    <div class="inbox-view">
        <?php echo $dak_nagoriks['dak_description']; ?>
    </div>
    <hr>
    <div class="inbox-attached">
        <?php
        if (isset($dak_attachments) && count($dak_attachments) > 0) {
            ?>
            <table class="table table-bordered att_tbl" id="att_tbl1">
                <tbody>
                <?php
                $i = 1;
                foreach ($dak_attachments as $single_data):
                    $file_name = $single_data['file_name'];
                    $file_name = explode("/", $file_name);
                    $file_name = $file_name[count($file_name) - 1];
                    ?>
                    <tr>
                        <td>
                            <div id="<?php echo 'data_' . $single_data['id']; ?>"><a class=""
                                                                                     href="<?php echo $this->request->webroot . $single_data['file_name']; ?>"><span
                                        class="fa fa-paperclip"></span>&nbsp;<?php echo $file_name; ?></a></div>
                        </td>
                    </tr>
                    <?php
                    $i++;
                endforeach;
                ?>
                </tbody>
            </table>
        <?php } ?>
    </div>
</div>

<div class="inbox-compose-btn">
    <button class="btn blue dak_forward_btn"><i class="fa fa-check"></i>Send</button>
    <!-- 		<button class="btn">Discard</button> -->
    <!-- 		<button class="btn">Draft</button> -->
</div>
	