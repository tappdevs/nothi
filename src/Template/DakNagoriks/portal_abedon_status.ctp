<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-toastr/toastr.min.css"/>
<style>
    .panel {
        background-color: rgba(255, 255, 255, 0.6);
    }
    .panel-success > .panel-heading {
        color: #693491;
        background-color: #dff0d8;
        border-bottom: 2px solid #35AA47;
    }
    .panel-title {
        margin-top: 0;
        margin-bottom: 0;
        font-size: 16px;
        color: inherit;
        font-weight: bold;
    }
</style>
<div class="row">
    <div class="col-md-10 col-sm-10 col-md-offset-1 ">
        <div class="panel panel-success">
            <div class="panel-heading">
                <div class="panel-title">
                    <?php echo __("ট্র্যাকিং") ?>
                </div>
            </div>
            <div class="panel-body">
                <div class="row form-group">
                    <div class="col-md-8 col-xs-12 col-md-offset-2">
                        <?php
                        echo $this->Form->input('dak_received_no', ['label' => "আবেদন গ্রহণ নম্বর", "class" => 'form-control','value'=>(isset($referenceCode)?$referenceCode:'')])
                        ?>
                    </div>
                </div>
                <hr/>
                <div class="row form-group">
                    <div class="col-md-8 col-xs-12  col-md-offset-2">
                        <?php
                        echo $this->Form->input('mobile_no', ['label' => "মোবাইল নম্বর", "class" => 'form-control'])
                        ?>
                    </div>
                </div>
                <hr/>
                <div class="row form-actions">
                    <div class="col-md-6 text-right">
                        <a class="btn green trackingformSearch"><i class="fs1 a2i_gn_search1"></i> &nbsp; <?php echo __(SEARCH) ?></a>
                    </div>
                    <div class="col-md-6 text-left">
                        <a class="btn red" onclick="reset_it()" >

                            <i class="fa fa-eraser" aria-hidden="true"></i> &nbsp; 
                            <?php echo __('রিসেট') ?>
                        </a>
                    </div>
                </div>
                
                <div class="row form-group responsetable"  style="display: none;">
                    <div class="col-md-12 ">
                        <hr/>
                        <table class="table table-bordered table-stripped ">
                            <thead>
                            <tr>
                                <th style="width: 15%" class="text-center  font-sm">গ্রহণ নম্বর</th>
                                <th style="width: 15%" class="text-center  font-sm">তারিখ</th>
                                <th style="width: 25%" class="text-center  font-sm">বিষয়</th>
                                <th style="width: 25%" class="text-center font-sm">বর্তমান টেবিল</th>
                                <th style="width: 20%" class="text-center font-sm">সর্বশেষ মন্তব্য</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>                
            </div>
        </div>
    </div>
</div>
<script src="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-toastr/toastr.min.js"></script>
<script src="<?php echo CDN_PATH; ?>assets/admin/pages/scripts/ui-toastr.js"></script>
<script>
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "positionClass": "toast-bottom-right"
    };
    $(document).ready(function(){
        var text='NagorikAbedon';
        <?php
        if(!empty($referenceCode)){ ?>
            if($('#dak-received-no').val()=='<?= $referenceCode?>' && text=='<?=$new ?>')
             {
                toastr.success( "আবেদন প্রেরণ করা হয়েছে। আপনার গ্রহণ নম্বর হচ্ছে <?= $referenceCode ?>। ভবিষ্যতে অনুসন্ধানের জন্য রেখে দিন" );
             }
         <?php }?>
         });
  
    $('.trackingformSearch').on('click',function(){
       var receive_no = $.trim($('#dak-received-no').val());
       var mobile_no = $.trim($('#mobile-no').val());
       
       if(receive_no != '' || mobile_no != ''){
           $('.responsetable').show();
           $('.responsetable').find('tbody').html('<tr><td class="text-center bold-text" colspan=5><i class="fa fa-spinner fa-spin"></i></td></tr>');
          
           $.ajax({
               url: '<?php echo $this->Url->build(['controller'=>'DakNagoriks','action'=>'getTracking']) ?>',
               type: 'post',
               data: {receive_no:receive_no,mobile_no:mobile_no},
               dataType: 'json',
               success: function (response){
                   $('.responsetable').find('tbody').html('');
                   if(response.status == 'success'){
                       $.each(response.data, function(i, v){
                           var montobbo = v.dak_actions;
                           $('.responsetable').find('tbody').append("<tr><td>" + v.dak_received_no + "</td><td>" + dateConvert(v.dakcreated) + "</td><td>" + v.dak_subject + "</td><td>" + v.to_officer_designation_label + "," + v.to_office_unit_name  +  "</td><td>" + ((v.nagorikstatus != "NothiVukto" && v.nagorikstatus != "NothiJato" )? montobbo.substr(0,100):'নথিতে উপস্থাপিত অথবা নথিজাত করা হয়েছে') + "</td></tr>");
                       });
                   }else{
                     $('.responsetable').find('tbody').append("<tr><td colspan=5 class='text-center text-danger'>" + response.msg + "</td></tr>");
                   }
               },
               error: function(){
                   $('.responsetable').find('tbody').html('');
                   toastr.error("দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না");
               }
           });
       }
        
    });
    
    function dateConvert(value){
        var bDate = new Array("০", "১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯");
        var bMonth = new Array("জানুয়ারি", "ফেব্রুয়ারি", "মার্চ", "এপ্রিল", "মে", "জুন", "জুলাই", "আগস্ট", "সেপ্টেম্বর", "অক্টোবর", "নভেম্বর", "ডিসেম্বর");
        var monthNames = ["January", "February", "March", "April", "May", "June","July", "August", "September", "October", "November", "December"];

        if (value!='' && value!=null) {
            var dt = new Date(value);
            var dtb = dt.getDate();
            var dtb1 = "", dtb2 = "";
            
            
            if (dtb >= 10) {
                dtb1 = Math.floor(dtb / 10);
                dtb2 = dtb % 10;
                dtb = bDate[dtb1] + "" + bDate[dtb2];
            } else {
                dtb = bDate[0] + "" + bDate[dtb];
            }

            var mnb;
            var mn = dt.getMonth();
            mnb = bMonth[mn];

            var yrb = "", yr1;
            var yr = dt.getFullYear();

            for (var i = 0; i < 3; i++) {
                yr1 = yr % 10;
                yrb = bDate[yr1] + yrb;
                yr = Math.floor(yr / 10);
            }

            yrb = bDate[yr] + "" + yrb;

//            return dtb + " " + mnb + "," + yrb + " খ্রিষ্টাব্দ";
            return dtb + " " + mnb + "," + yrb ;
        }
    }
     function reset_it(){
         $('#dak-received-no').val(''); 
         $('#mobile-no').val(''); 
         $('.responsetable').html(''); 
     }
</script>
