<style>

    .inbox .inbox-header h1 {
        margin-bottom: 0px;
    }

    .checkbox input[type=checkbox], .checkbox-inline input[type=checkbox], .radio input[type=radio], .radio-inline input[type=radio] {
        margin-left: -10px !important;
    }

    .inbox .inbox-header h1 {
        margin-bottom: 0px;
    }

    .checkbox input[type=checkbox], .checkbox-inline input[type=checkbox], .radio input[type=radio], .radio-inline input[type=radio] {
        margin-left: -10px !important;
    }

    .inbox .inbox-nav li.active b {
        display: none !important;
    }

    .tooltip-inner {
        white-space: pre-line;
    }

    h3 {
        margin-top: 0px;
        margin-bottom: 0px;
    }
</style>

<div class="portlet light">
    <div class="portlet-body">
        <div class="row inbox">
            <div class="col-md-12">
                <div class="inbox-content">
                    <div class="portlet-body">
                        <div class="inbox-header inbox-view-header">
                            <div class="col-md-12 col-sm-12 col-sm-12">
                                <h3 class="text-center">
                                    নাগরিক <?php echo __(DAK_GROHON) ?>
                                </h3>
                            </div>
                        </div>
                        <hr/>

                        <div class="portlet-body form">
                            <!--Start: Common Fields required only for create action -->
                            <?php
                            echo $this->Form->create($dak_nagoriks, ['type' => 'file', 'id' => 'uploadDakForm', 'autocomplete' => 'off']);
                            echo $this->Form->hidden('dak_type', array('label' => false, 'class' => 'form-control', 'value' => DAK_NAGORIK));
                            echo $this->Form->hidden('dak_status', array('label' => false, 'class' => 'form-control', 'value' => 1));
                            ?>
                            <!--End: Common Fields required only for create action -->

                            <!--Start: Remaining Form elements -->
                            <div class="form-body">


                                <h3 class="form-section dak-heading">
                                    <?php echo __(DAK_BIBORONI) ?>
                                </h3>
                                <div class="alert alert-danger display-hide">
                                    <button class="close" data-close="alert"></button>
                                    You have some form errors. Please check below.
                                </div>
                                <div class="alert alert-success display-hide">
                                    <button class="close" data-close="alert"></button>
                                    Your form validation is successful!
                                </div>
                                <div class="form-horizontal">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-4">ন্যাশনাল আইডি</label>

                                                <div class="col-md-8">
                                                    <?php echo $this->Form->input('national_idendity_no', array('label' => false, 'class' => 'form-control', 'data-ng-model' => 'tab.nid','value' => 'N/A','onclick'=>'$(this).select()')); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-4">জন্ম সনদ</label>

                                                <div class="col-md-8">
                                                    <?php echo $this->Form->input('birth_registration_number', array('label' => false, 'class' => 'form-control', 'data-ng-model' => 'tab.birth_certificate','value' => 'N/A','onclick'=>'$(this).select()')); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-4">পাসপোর্ট</label>

                                                <div class="col-md-8">
                                                    <?php echo $this->Form->input('passport', array('label' => false, 'class' => 'form-control', 'data-ng-model' => 'tab.passport','value' => 'N/A','onclick'=>'$(this).select()')); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">নাম (বাংলা) <span class="required"> * </span></label>

                                                <div class="col-md-8">
                                                    <?php echo $this->Form->input('name_bng', array('label' => false, 'class' => 'form-control', 'data-ng-model' => 'tab.name_bangla', 'data-required' => 1,'placeholder'=>'নাম লিখুন বাংলায়')); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">নাম (ইংরেজি) </label>

                                                <div class="col-md-8">
                                                    <?php echo $this->Form->input('name_eng', array('label' => false, 'class' => 'form-control', 'data-ng-model' => 'tab.name_english','placeholder'=>'TYPE NAME IN ENGLISH')); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-4">পিতার/স্বামীর নাম (বাংলা / ইংরেজি) </label>

                                                <div class="col-md-8">
                                                    <?php echo $this->Form->input('father_name', array('label' => false, 'class' => 'form-control','placeholder'=>'পিতা অথবা স্বামীর নাম লিখুন')); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-4">মাতার নাম (বাংলা / ইংরেজি) </label>

                                                <div class="col-md-8">
                                                    <?php echo $this->Form->input('mother_name', array('label' => false, 'class' => 'form-control','placeholder'=>'মাতার নাম লিখুন')); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-4">বর্তমান ঠিকানা <span
                                                        class="required"> * </span></label>

                                                <div class="col-md-8">
                                                    <?php echo $this->Form->input('address', array('label' => false, 'type' => 'textarea', 'rows' => '5', 'cols' => '55', 'class' => 'form-control', 'data-ng-model' => 'tab.address', 'data-required' => 1,'placeholder'=>'বর্তমান ঠিকানা')); ?>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-4">স্থায়ী ঠিকানা <span
                                                        class="required"> * </span></label>

                                                <div class="col-md-8">
                                                    <input type="checkbox" id="sameaspresent" /> বর্তমান ঠিকানা
                                                    <?php echo $this->Form->input('parmanent_address', array('label' => false, 'type' => 'textarea', 'rows' => '5', 'cols' => '55', 'class' => 'form-control', 'data-ng-model' => 'tab.parmanent_address', 'data-required' => 1,'placeholder'=>'স্থায়ী ঠিকানা')); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-4">ই-মেইল</label>

                                                <div class="col-md-8">
                                                    <?php echo $this->Form->input('email', array('label' => false, 'class' => 'form-control', 'data-ng-model' => 'tab.email', 'type' => 'email', 'data-required' => 1,'placeholder'=>'ইমেল লিখুন')); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-4">মোবাইল নম্বর 
                                                    <!--<span class="required"> * </span>-->
                                                </label>

                                                <div class="col-md-8">
                                                    <?php echo $this->Form->input('mobile_no', array('label' => false, 'class' => 'form-control', 'data-ng-model' => 'tab.mobile_no', 'data-required' => 1)); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label col-md-6">জাতীয়তা</label>

                                                <div class="col-md-6">
                                                    <?php
                                                    echo $this->Form->input('nationality', array('label' => false, 'class' => 'form-control', 'options' => array(
                                                            "bangladeshi" => "বাংলাদেশী",
                                                            "Other" => 'অন্যান্য'
                                                    )));
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label col-md-4">লিঙ্গ</label>

                                                <div class="col-md-8">
                                                    <?php echo $this->Form->input('gender', array('label' => false, 'class' => 'form-control', 'options' => array("Male" => 'পুরুষ', "Female" => 'মহিলা', "Other" => 'অন্যান্য'))); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label col-md-4">ধর্ম</label>

                                                <div class="col-md-8">
                                                    <?php
                                                    echo $this->Form->input('religion', array('label' => false, 'class' => 'form-control', 'options' => array(
                                                            "islam" => "ইসলাম",
                                                            "sonaton" => "হিন্দু",
                                                            "christian" => "খ্রিষ্টান",
                                                            "buddhism" => "বৌদ্ধ",
                                                            "Other" => 'অন্যান্য'
                                                    )));
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-2">বিষয় <span class="required"> * </span></label>

                                                <div class="col-md-10">
                                                    <?php echo $this->Form->input('dak_subject', array('label' => false, 'class' => 'form-control', 'data-ng-model' => 'tab.subject', 'data-required' => 1,'placeholder'=>'বিষয় লিখুন')); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-2"><?php echo __(GOPONIOTA) ?> </label>

                                                <div class="col-md-4">
                                                    <?php
                                                    $dak_security_levels = json_decode(DAK_SECRECY_TYPE, true);
                                                    $dak_security_levels[0] = 'গোপনীয়তা বাছাই করুন';
                                                    ksort($dak_security_levels);
                                                    echo $this->Form->input('dak_security_level', array('type' => 'select', 'label' => false, 'class' => 'form-control', 'options' => $dak_security_levels));
                                                    ?>
                                                </div>

                                                <label class="control-label col-md-2"><?php echo __(OGRADHIKAR) ?> </label>

                                                <div class="col-md-4">
                                                    <?php
                                                    $dak_priority_levels = json_decode(DAK_PRIORITY_TYPE, true);
                                                    $dak_priority_levels[0]='অগ্রাধিকার বাছাই করুন';
                                                    ksort($dak_priority_levels);
                                                    echo $this->Form->input('dak_priority_level', array('type' => 'select', 'label' => false, 'class' => 'form-control', 'options' => $dak_priority_levels));
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>


                                <h3 class="form-section">
                                    <?php echo __(PRAPOK) ?>
                                </h3>

                                <div class="row">
                                    <div class="col-md-12 col-lg-12 sealDiv">
                                        <?php echo $this->cell('DakSender', ['params' => $selected_office_section]); ?>
                                    </div>
                                </div>


                                <h3 class="form-section">
                                    <?php echo __('মূল ডাক ও সংযুক্তিসমূহ') ?>&nbsp;<span class="text-danger">*</span>
                                </h3>
                                <?php if (isset($dak_attachments) && count($dak_attachments) > 0) { ?>
                                    <table class="table table-bordered att_tbl" id="att_tbl1">
                                        <tbody>
                                            <?php
                                            $i = 1;
                                            foreach ($dak_attachments as $single_data):
                                                ?>
                                                <tr>
                                                    <td width="80%">
                                                        <div id="<?php echo 'data_' . $single_data['id']; ?>"><a class=""
                                                                                                                 href="<?php echo $this->request->webroot . 'content/' . $single_data['file_name']; ?>"><?php echo $single_data['file_name']; ?></a>
                                                        </div>
                                                    </td>
                                                    <td width="20%" class="text-center">
                                                        <span id="file_enable_div_<?php echo $single_data['id']; ?>">
                                                            <a target="_blank"
                                                               href="<?php echo $this->request->webroot . 'content/' . $single_data['file_name']; ?>"><i
                                                                    class="fs1 a2i_gn_view1"></i></a>
                                                            <a href="javascript:;" class="trash_attachment"
                                                               data-attachment-id="<?php echo $single_data['id']; ?>"><i
                                                                    class="fs1 a2i_gn_delete2"></i></a>
                                                        </span>
                                                        <span class="file_disable_div"
                                                              id="file_disable_div_<?php echo $single_data['id']; ?>">
                                                            <a href="javascript:;" class="reload_trash_attachment"
                                                               data-attachment-id="<?php echo $single_data['id']; ?>"><i
                                                                    class="fa fa-edit"></i></a>
                                                        </span>
                                                    </td>
                                                </tr>
                                                <?php
                                                $i++;
                                            endforeach;
                                            ?>
                                        </tbody>
                                    </table>
                                <?php } ?>
                                <?php echo $this->Form->hidden('uploaded_attachments', ['id' => 'uploaded_attachments']) ?>
                                <?php echo $this->Form->hidden('uploaded_attachments_names', ['id' => 'uploaded_attachments_names']) ?>
                                <?php echo $this->Form->hidden('uploaded_attachments_is_main', ['id' => 'uploaded_attachments_is_main']) ?>
                                <?php echo $this->Form->hidden('file_description', ['id' => 'file_description']) ?>
                                <!--End: Form Buttons -->
                                <?php echo $this->Form->end(); ?>
                                <!-- BEGIN PAGE CONTENT-->
                                <form id="fileupload" action="<?= $this->Url->build(['_name'=>'tempUpload']) ?>"
                                      method="POST" enctype="multipart/form-data">

                                    <input type="hidden" name="module_type" value="Dak" />
                                    <input type="hidden" name="module" value="Dak" />

                                    <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
                                    <div class="col-lg-6 form-group hidden">
                                        <input type="text" name="file_description_upload" id="file_description_upload" class="form-control " placeholder="সংযুক্তির বিবরণ">
                                    </div>

                                    <div class="row fileupload-buttonbar">
                                        <div class="col-lg-12">
	                                        <div class="alert alert-info font-lg text-center">সংযুক্তিসমূহের মধ্যে থেকে মূল ডাকটি চিহ্নিত করুন</div>
                                            <!-- The fileinput-button span is used to style the file input field as button -->
                                            <span class="btn green fileinput-button round-corner-5" title="সর্বোচ্চ ফাইল সাইজ ২০ এমবি হবে" data-toggle="tooltip">
                                                <i class="fs1 a2i_gn_add1"></i>
                                                <span>
                                                    ফাইল যুক্ত করুন </span>
                                                <input type="file" name="files[]" multiple="" accept="image/*,.pdf">
                                            </span>
                                            <button type="button" class="btn red delete hidden">
                                                <i class="fs1 a2i_gn_delete2"></i>
                                                <span>
                                                    সব মুছে ফেলুন </span>
                                            </button>
                                            <input type="checkbox" class="toggle hidden">
                                            <!-- The global file processing state -->
                                            <span class="fileupload-process">
                                            </span>
                                        </div>
                                        <!-- The global progress information -->
                                        <div class="col-lg-12 fileupload-progress fade">
                                            <!-- The global progress bar -->
                                            <div class="progress progress-striped active" role="progressbar"
                                                 aria-valuemin="0" aria-valuemax="100">
                                                <div class="progress-bar progress-bar-success" style="width:0%;">
                                                </div>
                                            </div>
                                            <!-- The extended global progress information -->
                                            <div class="progress-extended">
                                                &nbsp;
                                            </div>
                                        </div>

                                    </div>
                                    <!-- The table listing the files available for upload/download -->
                                    <table role="presentation" class="table table-striped clearfix">
                                        <tbody class="files">
                                        </tbody>
                                    </table>
                                </form>
                            </div>

                            <!--End: Remaining Form elements -->
                            <!--Start: Form Buttons -->
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-9">
	                                    <div class="btn-group btn-group-round">
	                                        <?= $this->Form->button('<i class="fs1 efile-save2"></i> '.__('খসড়া সংরক্ষণ'), ['class' => 'btn btn-primary', 'onclick' => 'DAK_FORM.submitForm()']) ?>
	                                        <?= $this->Form->button('<i class="fs1 efile-send3"></i> '.__(SEND), ['class' => 'btn purple', 'onclick' => 'DAK_FORM.submitAndSendForm()']) ?>
	                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
echo $this->element('OfficeSealModal/office_seal_modal');
?>

<!-- Start: Common dak setup js -->
<script src="<?php echo CDN_PATH; ?>assets/admin/layout4/scripts/dak_setup.js"
type="text/javascript"></script>

<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>assets/admin/pages/css/invoice.css"/>
<!-- END PAGE LEVEL STYLES -->

<script src="<?php echo CDN_PATH; ?>assets/global/scripts/printThis.js"></script>
<div class="modal fade modal-purple" id="receiptprint" role="basic" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
                <div class="modal-title">আবেদনের রশিদ</div>
            </div>
            <div class="modal-body">
                <img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span> &nbsp;&nbsp;লোড করা হচ্ছে... </span>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(function () {
        DakSetup.init();
    });
</script>
<!-- End: Common dak setup js -->
<style>
    div.disabled {
        pointer-events: none;
        opacity: 0.2;
    }
</style>

<script>

	var DAK_FORM = {
		attached_files: [],
		attached_files_names: [],
		generateSarokNo: function (office_id) {
			PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + '/DakNagoriks/generateSarokNo',
				{'office_id': office_id}, 'json',
				function (response) {
					$("#sender-sarok-no").val(response);
				});
		},
		submitForm: function () {
            $('[name=dak_subject]').val($('[name=dak_subject]').val().replace(/\n/g, ' '));
			$("#uploadDakForm").attr('action', '<?php echo $this->request->webroot ?>dakNagoriks/uploadDak');

			DAK_FORM.attached_files = [];
			DAK_FORM.attached_files_names = [];
            DAK_FORM.attached_files_is_main = [];
            if($(".template-download .main-dak").length == 1){
                $(".template-download .main-dak").click();
            }
            var is_main_selected = false;
			$('.template-download').each(function () {
				// var link_td = $(this).find('td:eq(2)');
				// var href = $(link_td).find('p.name > a').attr('href');
                var href = $(this).find('a').attr('hrefed');
                DAK_FORM.attached_files.push(href);
			});
			$("#uploaded_attachments").val(DAK_FORM.attached_files);
			$('.template-download .potro-attachment-input').each(function () {
				var name = $(this).val();
				if(isEmpty(name)){
					name = $(this).attr('title');
				}
                DAK_FORM.attached_files_names.push(name);
			});
			$("#uploaded_attachments_names").val(DAK_FORM.attached_files_names);
            $('.template-download .main-dak').each(function () {
                var is_main = $(this).val();
                if(is_main == 1){
                    is_main_selected = true;
                }
                DAK_FORM.attached_files_is_main.push(is_main);
            });
            $("#uploaded_attachments_is_main").val(DAK_FORM.attached_files_is_main);

			toastr.options = {
				"closeButton": true,
				"debug": false,
				"positionClass": "toast-bottom-right"
			};
			if ($("#uploaded_attachments").val().length == 0) {
				toastr.error("দুঃখিত! ডাক সংযুক্তি দেয়া হয়নি");
				return false;
			}

            if (is_main_selected == false) {
                toastr.error("দুঃখিত! মূল ডাক নির্বাচন করা হয়নি");
                return false;
            }

			if ($('input[name=to_officer_id]').val() == '' || $('input[name=to_officer_id]').val() == 0) {
				toastr.error("দুঃখিত! প্রাপক বাছাই করা হয়নি");
				return false;
			}

			if ($('input[name=to_officer_level]').val() == '' || $('input[name=to_officer_level]').val() == 0) {

				toastr.error("দুঃখিত! মূল প্রাপক বাছাই করা হয়নি");
				return false;
			}

			if ($('[name=dak_subject]').val() == '' || $('[name=dak_subject]').val() == 0) {
				$('[name=dak_subject]').focus();
				toastr.error("দুঃখিত! ডাকের বিষয় দিন");
				return false;
			}

			if ($('input[name=name_bng]').val() == '' || $('input[name=name_bng]').val() == 0) {
				$('input[name=name_bng]').focus();
				toastr.error("দুঃখিত! প্রাপকের নাম দিন");
				return false;
			}


			if ($('input[name=name_eng]').val() == '' || $('input[name=name_eng]').val() == 0) {
				$('input[name=name_eng]').val($('input[name=name_bng]').val());
//                toastr.error("দুঃখিত! আপনার নাম  দেয়া হয়নি");
//                return false;
			}

//            if ($('input[name=father_name]').val() == '' || $('input[name=father_name]').val() == 0) {
//                $('input[name=father_name]').focus();
//                toastr.error("দুঃখিত! পিতার নাম দিন");
//                return false;
//            }
//
//
//            if ($('input[name=mother_name]').val() == '' || $('input[name=mother_name]').val() == 0) {
//                $('input[name=mother_name]').focus();
//                toastr.error("দুঃখিত! মাতার নাম দিন");
//                return false;
//            }


			if ($('textarea[name=present_address]').val() == '' || $('textarea[name=present_address]').val() == 0) {
				$('textarea[name=present_address]').focus();
				toastr.error("দুঃখিত! বর্তমান ঠিকানা দিন");
				return false;
			}

			if ($('textarea[name=parmanent_address]').val() == '' || $('textarea[name=parmanent_address]').val() == 0) {
				$('textarea[name=parmanent_address]').focus();
				toastr.error("দুঃখিত! স্থায়ী ঠিকানা দিন");
				return false;
			}
			
			if(!(isEmpty($('input[name=email]').val())) && (regex.test($('input[name=email]').val()) == false) ){
                toastr.error("দুঃখিত! ইমেইল সঠিক নয়");
                return false;
            }


//            if ($('input[name=mobile_no]').val() == '' || $('input[name=mobile_no]').val() == 0) {
//                $('input[name=mobile_no]').focus();
//                toastr.error("দুঃখিত! মোবাইল নম্বর দিন");
//                return false;
//            }

			$("#uploadDakForm").next().find('button').attr('disabled', 'disabled');

			$("#file_description").val($("#file_description_upload").val());
			$("#uploadDakForm").submit();
		},
		submitAndSendForm: function () {
            $('[name=dak_subject]').val($('[name=dak_subject]').val().replace(/\n/g, ' '));
			$("#uploadDakForm").attr('action', '<?php echo $this->request->webroot ?>dakNagoriks/uploadDakSend');

			DAK_FORM.attached_files = [];
			DAK_FORM.attached_files_names = [];
            DAK_FORM.attached_files_is_main = [];
            if($(".template-download .main-dak").length == 1){
                $(".template-download .main-dak").click();
            }
            var is_main_selected = false;
			$('.template-download').each(function () {
				// var link_td = $(this).find('td:eq(2)');
				// var href = $(link_td).find('p.name > a').attr('href');
                var href = $(this).find('a').attr('hrefed');
				DAK_FORM.attached_files.push(href);
			});
			$("#uploaded_attachments").val(DAK_FORM.attached_files);

			$('.template-download .potro-attachment-input').each(function () {
				var name = $(this).val();
				if(isEmpty(name)){
					name = $(this).attr('title');
				}
				DAK_FORM.attached_files_names.push(name);
			});
			$("#uploaded_attachments_names").val(DAK_FORM.attached_files_names);
            $('.template-download .main-dak').each(function () {
                var is_main = $(this).val();
                if(is_main == 1){
                    is_main_selected = true;
                }
                DAK_FORM.attached_files_is_main.push(is_main);
            });
            $("#uploaded_attachments_is_main").val(DAK_FORM.attached_files_is_main);

			toastr.options = {
				"closeButton": true,
				"debug": false,
				"positionClass": "toast-bottom-right"
			};
			if ($("#uploaded_attachments").val().length == 0) {
				toastr.error("দুঃখিত! ডাক সংযুক্তি দেয়া হয়নি");
				return false;
			}

            if (is_main_selected == false) {
                toastr.error("দুঃখিত! মূল ডাক নির্বাচন করা হয়নি");
                return false;
            }

			if ($('input[name=to_officer_id]').val() == '' || $('input[name=to_officer_id]').val() == 0) {
				toastr.error("দুঃখিত! প্রাপক বাছাই করা হয়নি");
				return false;
			}

			if ($('input[name=to_officer_level]').val() == '' || $('input[name=to_officer_level]').val() == 0) {

				toastr.error("দুঃখিত! মূল প্রাপক বাছাই করা হয়নি");
				return false;
			}

			if ($('[name=dak_subject]').val() == '' || $('[name=dak_subject]').val() == 0) {
				$('[name=dak_subject]').focus();
				toastr.error("দুঃখিত! ডাকের বিষয় দিন");
				return false;
			}

			if ($('input[name=name_bng]').val() == '' || $('input[name=name_bng]').val() == 0) {
				$('input[name=name_bng]').focus();
				toastr.error("দুঃখিত! প্রাপকের নাম দিন");
				return false;
			}

			if ($('input[name=name_eng]').val() == '' || $('input[name=name_eng]').val() == 0) {
				$('input[name=name_eng]').val($('input[name=name_bng]').val());
//                toastr.error("দুঃখিত! আপনার নাম  দেয়া হয়নি");
//                return false;
			}



//            if ($('input[name=father_name]').val() == '' || $('input[name=father_name]').val() == 0) {
//                $('input[name=father_name]').focus();
//                toastr.error("দুঃখিত! পিতার নাম দিন");
//                return false;
//            }
//
//
//            if ($('input[name=mother_name]').val() == '' || $('input[name=mother_name]').val() == 0) {
//                $('input[name=mother_name]').focus();
//                toastr.error("দুঃখিত! মাতার নাম দিন");
//                return false;
//            }


			if ($('textarea[name=present_address]').val() == '' || $('textarea[name=present_address]').val() == 0) {
				$('textarea[name=present_address]').focus();
				toastr.error("দুঃখিত! বর্তমান ঠিকানা দিন");
				return false;
			}


			if ($('textarea[name=parmanent_address]').val() == '' || $('textarea[name=parmanent_address]').val() == 0) {
				$('textarea[name=parmanent_address]').focus();
				toastr.error("দুঃখিত! স্থায়ী ঠিকানা দিন");
				return false;
			}
            if(!(isEmpty($('input[name=email]').val())) && (regex.test($('input[name=email]').val()) == false) ){
                toastr.error("দুঃখিত! ইমেইল সঠিক নয়");
                return false;
            }
//            if ($('input[name=mobile_no]').val() == '' || $('input[name=mobile_no]').val() == 0) {
//                $('input[name=mobile_no]').focus();
//                toastr.error("দুঃখিত! মোবাইল নম্বর দিন");
//                return false;
//            }

			$("#uploadDakForm").next().find('button').attr('disabled', 'disabled');
			$("#file_description").val($("#file_description_upload").val());
			$("#uploadDakForm").submit();
		}
	};

    $(function () {
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-bottom-right"
        };
        FormFileUpload.init();
        $("#autocomplete_office_id").bind('blur', function () {
            DAK_FORM.generateSarokNo($(this).val());
        });

        var OfficeDataAdapter = new Bloodhound({
            datumTokenizer: function (d) {
                return d.tokens;
            },
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: getBaseUrl() + 'officeManagement/autocompleteOfficeDesignation?search_key=%QUERY'
        });
        OfficeDataAdapter.initialize();

        $('.typeahead_sender')
                .typeahead(null, {
                    name: 'datypeahead_sender',
                    displayKey: 'value',
                    source: OfficeDataAdapter.ttAdapter(),
                    hint: (Metronic.isRTL() ? false : true),
                    templates: {
                        suggestion: Handlebars.compile([
                            '<div class="media">',
                            '<div class="media-body">',
                            '<h5 style="font-size:14px;" class="media-heading"><div style="height:30px;float:left;"><img src="{{image}}" class="photo_icon" /></div>{{value}}</h5>',
                            '</div>',
                            '</div>',
                        ].join(''))
                    }
                }
                )
                .on('typeahead:opened', onOpened)
                .on('typeahead:selected', onAutocompleted)
                .on('typeahead:autocompleted', onSelected);

        $('.typeahead_receiver')
                .typeahead(null, {
                    name: 'datypeahead_receiver',
                    displayKey: 'value',
                    source: OfficeDataAdapter.ttAdapter(),
                    hint: (Metronic.isRTL() ? false : true),
                    templates: {
                        suggestion: Handlebars.compile([
                            '<div class="media">',
                            '<div class="media-body">',
                            '<h5 style="font-size:14px;" class="media-heading"><div style="height:30px;float:left;"><img src="{{image}}" class="photo_icon" /></div>{{value}}</h5>',
                            '</div>',
                            '</div>',
                        ].join(''))
                    }
                }
                )
                .on('typeahead:opened', onOpened)
                .on('typeahead:selected', onAutocompleted)
                .on('typeahead:autocompleted', onSelected);

        function onOpened($e) {
            //console.log('opened');
        }

        function onAutocompleted($e, datum) {
            DakOffice.setAutoDataIntoHiddenFields(datum, $e.currentTarget.id);
        }

        function onSelected($e, datum) {
            //console.log('selected');
        }

<?php if (!empty($sentdak)): ?>

            var link = '<?php echo $this->Url->build(['controller' => 'dakNagoriks', 'action' => 'dakReceipt', (!empty($sentdak) ? $sentdak : 0)]); ?>';
            $("#receiptprint").modal('show').find(".modal-content").load(link);
<?php endif; ?>

    });

    function PrintElem(elem) {
        $("#myDiv").printThis();
    }
//    $(document).off('click','#save-seal');
    $(document).on('click', '.btn-close-seal-modal', function () {
        $('.addSeal').find('.scroller').html('');
        $('.sealDiv').html('<img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"><span>&nbsp;&nbsp;লোড করা হচ্ছে...</span>');
        $('.sealDiv').load('<?php echo $this->Url->build(['controller' => 'officeManagement', 'action' => 'reloadOfficeSeal', $selected_office_section['office_id'], $selected_office_section['office_unit_id']]); ?>');
    });

//  $(document).off('click','.deleteSeal');
    $(document).on('click', '.deleteSeal', function () {
        var that = this;
        bootbox.dialog({
            message: "আপনি কি নিশ্চিত মুছে ফেলতে চান?",
            title: "ডাক সিল মুছে ফেলুন",
            buttons: {
                success: {
                    label: "হ্যাঁ",
                    className: "green",
                    callback: function () {
                        var sealId = $(that).data('id');
                        $.ajax({
                            url: '<?php echo $this->request->webroot ?>officeManagement/officeSealDelete',
                            data: {id: sealId},
                            type: "POST",
                            dataType: 'json',
                            success: function (res) {
                                if (res.status == "success")
                                {
                                    $('.sealDiv').load('<?php echo $this->Url->build(['controller' => 'officeManagement', 'action' => 'reloadOfficeSeal', $selected_office_section['office_id'], $selected_office_section['office_unit_id']]); ?>');
                                } else
                                    toastr.error(res.msg);
                            }
                        });
                    }
                },
                danger: {
                    label: "না",
                    className: "red",
                    callback: function () {
                    }
                }
            }
        });
    });

    $(function () {
        DakNagoriks.init();
    });

    $(document).on('click', '#sameaspresent', function () {
        if (this.checked == true) {
            $('#parmanent-address').val($('#address').val()).attr('readonly', 'readonly');
        } else {
            $('#parmanent-address').val('').removeAttr('readonly');
        }
    })
</script>
<script type="text/javascript">

    var DakNagoriks = {
        national_idendity_no: 0,
        birth_registration_number: 0,
        checkNid: function () {
            DakNagoriks.national_idendity_no = $("#national-idendity-no").val();
            $("#nid-check-sign").html("<span class='glyphicon glyphicon-ok-circle'></span>");

        },
        checkBirthCertificate: function () {
            DakNagoriks.birth_registration_number = $("#birth_registration_number").val();
            $("#birth-cirtificate-check-sign").html("Checking...");

            PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + 'DakNagoriks/checkBirthCertificate', {'birth_registration_number': DakNagoriks.birth_registration_number}, 'json', function (response) {

                if (response == 1) {
                    $("#birth-cirtificate-check-sign").html("<span class='glyphicon glyphicon-ok-circle'></span>");
                } else {
                    $("#birth-cirtificate-check-sign").html("<span class='glyphicon glyphicon-remove-circle'></span>");
                }
            });

        },
        init: function () {
            $("#national-idendity-no").bind('blur', function () {
//                DakNagoriks.checkNid();
            });

            $("#birth-registration-number").bind('blur', function () {
                DakNagoriks.checkBirthCertificate();
            });
        }
    };

    $('[type="email"]').change(function () {
        if (this.value != '') {
            if (regex.test(this.value) == false) {
                toastr.error("দুঃখিত! ইমেইল সঠিক নয়");
                $(this).focus();
            }
        }
    });

    var regex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;


    function mainDak(element) {
        $('.template-download .radio>span').removeClass('checked');
        $('.template-download .potro-attachment-input').each(function () {
            if($(this).is('[readonly]') == true){
                $(this).val("").attr("readonly", false);
            }

        });
        $(".template-download .main-dak").val(0);
        $(".template-download").css('background', '#fff');
        $(element).val(1);
        $(element).closest('.template-download').css('background', '#eef5f2');
        $(element).closest('.template-download').find('.potro-attachment-input').val("মূল ডাক").attr("readonly", true);
        $(element).closest('.template-download .radio>span').addClass('checked');
    }
</script>

<!-- End: JavaScript -->
<!-- The blueimp Gallery widget -->
<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
    <div class="slides">
    </div>
    <h3 class="title"></h3>
    <a class="prev">
        ÔøΩ </a>
    <a class="next">
        ÔøΩ </a>
    <a class="close white">
    </a>
    <a class="play-pause">
    </a>
    <ol class="indicator">
    </ol>
</div>
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<script id="template-upload" type="text/x-tmpl">
    {% for (var i=0, file; file=o.files[i]; i++) { %}
    <div class="col-md-2 template-upload">
        <div class="pull-right">
            <button class="btn cancel btn-link">
			    <i class="fa fa-ban" style="color:red;"></i>
		    </button>
        </div>
        <div style="padding: 10px 20px;width:151px;height:131px;">
            <span class="preview"></span>
        </div>
        <div>
            <p class="size">প্রক্রিয়াকরন চলছে...</p>
		    <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
		    <div class="progress-bar progress-bar-success" style="width:0%;"></div>
        </div>
        <div>
            <p class="name">{%=file.name%}</p>
            <strong class="error label label-danger"></strong>
        </div>
    <div>
    {% } %}
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
    {% for (var i=0, file; file=o.files[i]; i++) { %}
    <div class="col-md-2 template-download" style="border: silver solid 1px;padding: 10px;margin-left:5px;">
        <div class="pull-right">
            <button class="btn delete btn-sm btn-link" title="ফাইলটি মুছে ফেলুন" style="color:red;" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
			    <i class="fs1 a2i_gn_close2"></i>
		    </button>
        </div>
        <div>
            <label>
            <input title="মূল ডাক" class="main-dak radio" style="float:left" type="radio" name="main_dak" value="0" onclick="mainDak(this)">&nbsp;মূল ডাক
            </label>
        </div>
        <div style="padding: 10px 20px;width:151px;height:131px;">
            {% if (file.thumbnailUrl) { %}
		        <a hrefed="<?= FILE_FOLDER ?>{%=file.url%}" downloaded="{%=file.name%}" onclick="doModal('imgViewer', 'ইমেজ ভিউয়ার', '<img src=<?= FILE_FOLDER ?>{%=file.url%} style=\'height:auto;width:100%\' />')">
		            <img src="<?= FILE_FOLDER ?>{%=file.thumbnailUrl%}" style="width:100%">
	            </a>
		    {% } else { %}
		        {% if (file.type == 'application/pdf') { %}
		            <a hrefed="<?= FILE_FOLDER ?>{%=file.url%}" onclick="doModal('pdfViewer', 'পিডিএভ ভিউয়ার', '<embed src=<?= FILE_FOLDER ?>{%=file.url%} style=\'height:calc(100vh - 207px);width:100%\' type={%=file.type%}></embed>')" downloaded="{%=file.name%}">
			            <i class="fa fa-file-pdf-o" style="font-size: 111px;margin-top: 50px;"></i>
		            </a>
		        {% } %}
		    {% } %}
        </div>
        <div>
            <span class="size">{%=o.formatFileSize(file.size)%}</span>
        </div>
        <div>
            <input type="text" class="form-control potro-attachment-input" image="{%=file.url%}" file-type="{%=file.type%}" placeholder="সংযুক্তির নাম">
        </div>
    </div>
    {% } %}
</script>
