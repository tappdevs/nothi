<div class="portlet light">
    <div class="portlet-title">
        <div class="caption"><i class="fs1 a2i_gn_details1"></i>অনলাইন ডাকসমূহ </div>
        <div class="tools">
            <a href="javascript:" class="collapse"></a>
            <a href="#portlet-config" data-toggle="modal" class="config"></a>
            <a href="javascript:" class="reload"></a>
            <a href="javascript:" class="remove"></a>
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                        <?= $this->Html->link('নতুন নাগরিক আবেদন', ['action' => 'nagorik_dak'], ['class' => 'btn btn-default']) ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="btn-group pull-right">
                        <button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i
                                class="glyphicon glyphicon-chevron-down"></i></button>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="#">Print </a></li>
                            <li><a href="#">Save as PDF </a></li>
                            <li><a href="#">Export to Excel </a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th class="text-center">দিন</th>
                <th class="text-center">বিষয়</th>
                <th class="text-center">নাম</th>
                <th class="text-center">মোবাইল</th>
                <th class="text-center">ইমেইল</th>

                <th class="actions text-center"><?= __('Actions') ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($query as $rows): ?>

                <tr>
                    <td class="text-center"><?php echo $rows['created']; ?></td>
                    <td class="text-center"><?php echo $rows['subject']; ?></td>
                    <td class="text-center"><?php echo $rows['name_eng']; ?></td>
                    <td class="text-center"><?php echo $rows['mobile_no']; ?></td>
                    <td class="text-center"><?php echo $rows['email']; ?></td>
                    <td class="actions text-center">
                        <?= $this->Html->link(__(''), ['action' => 'view', $rows['id']], ['class' => 'btn btn-xs fs1 a2i_gn_view1 text-primary']) ?>
                        <?php
                        if ($rows['approval'] == 0) {
                            ?>
                            <?= $this->Html->link(__('Approve'), ['action' => 'approve', $rows['id']], ['class' => 'btn btn-xs text-warning']) ?>
                        <?php
                        } else {
                            ?>
                            <?= $this->Html->link(__('Approved'), ['action' => '', $rows['id']], ['class' => 'btn btn-xs fa fa-check text-success']) ?>
                        <?php
                        }
                        ?>


                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
