<style>

    .inbox .inbox-nav li.active b {
        display: none !important;
    }
    .tooltip-inner {
        white-space: pre-line;
    }

</style>
<div class="portlet light">

    <div class="portlet-title">
        <div class="caption"><i class="fs1 a2i_gn_details1"></i>ডাক প্রেরণ নিবন্ধন বহি</div>

    </div>

    <div class="portlet-body">

        <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="page-toolbar pull-left portlet-title">
                        <div id="dashboard-report-range" class=" tooltips btn btn-sm btn-default" data-container="body"
                             data-placement="bottom" data-original-title="Date Range">
                            <i class="icon-calendar"></i>&nbsp; <span class="thin uppercase visible-lg-inline-block">October 24, 2015 - November 22, 2015</span>&nbsp;
                            <i class="glyphicon glyphicon-chevron-down"></i>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="btn-group pull-right">
                        <button class="btn dropdown-toggle" data-toggle="dropdown"><?php echo __("Tools"); ?> <i
                                class="glyphicon glyphicon-chevron-down"></i></button>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="#"> </a></li>
                            <li><a href="#"><?php echo __("Save as PDF"); ?> </a></li>
                            <li><a href="#"><?php echo __("Export to Excel"); ?> </a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="row inbox">

            <div class="col-md-12">

                <div class="inbox-loading" style="display: none;"><?php echo __("লোডিং..."); ?>...</div>
                <div class="inbox-content">

                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo CDN_PATH; ?>js/reports/sent_dak_reports.js" type="text/javascript"></script>
<script src="<?php echo CDN_PATH; ?>daptorik_preview/js/report_date_range.js"></script>

<script>
    $(function () {

        SendDakReport.init('my-draft');

    });
</script>