<?php
$prev = "";
$count = 0;
if (!empty($dak_list)) {
    ?>
    <div class="table-container  table-scrollable">
        <table class="table table-bordered table-hover">
            <thead>
                <tr class="heading">
                    <th class="text-center" style="width: 5%">ক্রমিক নং</th>
                    <th class="text-center" style="width: 5%">গ্রহণ নম্বর</th>
                    <th class="text-center" style="width: 5%">ডকেট নং</th>
                    <th class="text-center" style="width: 10%">স্মারক নম্বর</th>
                    <th class="text-center" style="width: 5%">আবেদনের তারিখ</th>
                    <th class="text-center" style="width: 10%">ধরন</th>
                    <th class="text-center" style="width: 10%">বিষয়</th>
                    <th class="text-center" style="width: 10%">আবেদনকারী</th>
                    <th class="text-center" style="width: 10%">পূর্ববর্তী প্রেরক</th>
                    <th class="text-center" style="width: 10%">মূল প্রাপক</th>
                    <th class="text-center" style="width: 5%">প্রাপ্তির তারিখ</th>
                    <th class="text-center" style="width: 5%">গোপনীয়তা</th>
                    <th class="text-center" style="width: 5%">অগ্রাধিকার</th>
                    <th class="text-center" style="width: 10%">সর্বশেষ অবস্থা </th>
                </tr>
            </thead>
            <tbody>
            <?php
            foreach ($dak_list as $key => $value) {
                $unitInformation = $value['from_office_unit_name'];
                $unitId = $value['from_office_unit_id'];
                if ($prev != $unitId) {
                    $prev = $unitId;
                    echo '<tr>
                        <th class="text-center" colspan="14">'. h($unitInformation) . '<br>' . h($office_name['office_name_bng']).'</th>
                    </tr>';
                    $count = 0;
                }
                $count++;
                ?>
                <?php
                if($version == 1) {
                    ?>
                <tr>

                    <td class='text-center'><?= $this->Number->format($count) ?></td>
                    <td><?= ($value['dak_type'] == DAK_DAPTORIK ? h($value['DakDaptoriks']['dak_received_no']) : h($value['DakNagoriks']['dak_received_no'])) ?>
                    </td><td><?= ($value['dak_type'] == DAK_DAPTORIK ? enTobn(h($value['DakDaptoriks']['docketing_no'])) : enTobn(h($value['DakNagoriks']['docketing_no']))) ?></td>
                    <td><?= ($value['dak_type'] == DAK_DAPTORIK ? h($value['DakDaptoriks']['sender_sarok_no']) : '') ?></td>
                    <?php
                    $time = new Cake\I18n\Time($value['dak_type'] == DAK_DAPTORIK ? h($value['daptorikCreated']) : h($value['nagorikCreated']));
                    $time = $time->i18nFormat(null, null, 'bn-BD');
                    ?>
                    <td><?= $time ?></td>
                    <td><?= ($value['dak_type'] == DAK_DAPTORIK ? 'দাপ্তরিক' : 'নাগরিক') ?></td>
                    <td><?= h($value['dak_type'] == DAK_DAPTORIK ? h($value['DakDaptoriks']['dak_subject']) : h($value['DakNagoriks']['dak_subject'])) ?></td>

                    <td><?= ($value['dak_type'] == DAK_DAPTORIK ? (h($value['from_officer_designation_label']) . ', ' . h($value['from_office_unit_name']) . ', ' . h($value['from_office_name'])) : (h( $value['from_officer_name']) . (!empty($value['from_officer_designation_label']) ? (', ' . h($value['from_officer_designation_label'])) : '') . (!empty($value['from_office_name']) ? (', ' . h($value['from_office_name'])) : '') )) ?></td>

                    <td><?= ($value['dak_type'] == DAK_DAPTORIK ? (h($value['DakDaptoriks']['sender_officer_designation_label']) . ', ' . h($value['DakDaptoriks']['sender_office_unit_name']) . ', ' . h($value['DakDaptoriks']['sender_office_name'])) : h($value['DakNagoriks']['sender_name'])) ?></td>

                    <?php
                    if (!empty($value['mainPrapok'])) {
                        ?>
                        <td><?= h($value['mainPrapok'][0]['to_officer_designation_label']) . ', ' . h($value['mainPrapok'][0]['to_office_unit_name']) . ', ' . h($value['mainPrapok'][0]['to_office_name']) ?></td>
                        <?php
                    } else {
                        ?>
                        <td></td>
                        <?php
                    }
                    ?>
                    <td><?=Cake\I18n\Time::parse ($value['created']) ?></td>
                    <?php
                    $secIndex = ($value['dak_type'] == DAK_DAPTORIK ? h($value['DakDaptoriks']['dak_security_level']) : h($value['DakNagoriks']['dak_security_level']));
                    ?>
                    <td><?= h(isset($security_level_list[$secIndex]) ? $security_level_list[$secIndex] : '') ?></td>
                    <td><?= h(isset($priority_list[$value['dak_priority']]) ? $priority_list[$value['dak_priority']] : '') ?></td>
                    <td><?= h($value['dak_actions']) ?></td>

                </tr>
                    <?php
                }
                else if($version==2){
                    ?>
                    <tr>

                        <td class='text-center'><?= $this->Number->format($count) ?></td>
                        <td><?= $value['dak_received_no'] ?>
                        </td><td><?= enTobn(h($value['docketing_no']))?></td>
                        <td><?= h($value['sender_sarok_no']) ?></td>
                        <?php
//                        $time = (h($value['dak_created']));
                        $time = new Cake\I18n\Time(($value['dak_created']));
                        $time = $time->i18nFormat(null, null, 'bn-BD');

                        $movement_time = new Cake\I18n\Time(($value['movement_time']));
                        $movement_time = $movement_time->i18nFormat(null, null, 'bn-BD');
                        ?>
                        <td><?= $time ?></td>
                        <td><?= ($value['dak_type'] == DAK_DAPTORIK ? 'দাপ্তরিক' : 'নাগরিক') ?></td>
                        <td><?=  h($value['dak_subject']) ?></td>

                        <td><?= ($value['dak_type'] == DAK_DAPTORIK ? (h($value['from_officer_designation_label']) . ', ' . h($value['from_office_unit_name']) . ', ' . h($value['from_office_name'])) : (h( $value['from_officer_name']) . (!empty($value['from_officer_designation_label']) ? (', ' . h($value['from_officer_designation_label'])) : '') . (!empty($value['from_office_name']) ? (', ' . h($value['from_office_name'])) : '') )) ?></td>

                        <td><?= ($value['dak_type'] == DAK_DAPTORIK ? (h($value['sender_officer_designation_label']) . ', ' . h($value['sender_office_unit_name']) . ', ' . h($value['sender_office_name'])) : h($value['sender_name'])) ?></td>

                        <?php
                        if (!empty($value['to_officer_designation_label'])) {
                            ?>
                            <td><?= h($value['to_officer_designation_label']) . ', ' . h($value['to_office_unit_name']) . ', ' . h($value
                                ['to_office_name']) ?></td>
                            <?php
                        } else {
                            ?>
                            <td></td>
                            <?php
                        }
                        ?>
                        <td><?= ($movement_time) ?></td>
                        <?php
                        $secIndex =  h($value['dak_security_level']);
                        ?>
                        <td><?= h(isset($security_level_list[$secIndex]) ? $security_level_list[$secIndex] : '') ?></td>
                        <td><?= h(isset($priority_list[$value['dak_priority']]) ? $priority_list[$value['dak_priority']] : '') ?></td>
                        <td><?= h($value['dak_actions']) ?></td>

                    </tr>
                    <?php
                }
                ?>
                <?php
            }
            ?>
            </tbody>
                 </table>
        </div>
<?php
        }
        else{
            ?>
            <div class="table-container  table-scrollable">
                <table class="table table-bordered table-striped">
                        <tr class="heading">
                            <th class="text-center" style="width: 5%;">ক্রমিক নং</th>
                            <th class="text-center" style="width: 10%;" >গ্রহণ নম্বর</th>
                            <th class="text-center" style="width: 10%;" >ডকেট নং</th>
                            <th class="text-center" style="width: 10%;" >স্মারক নম্বর</th>
                            <th class="text-center" style="width: 5%;" >আবেদনের তারিখ</th>
                            <th class="text-center" style="width: 5%;" >ধরন</th>
                            <th class="text-center" style="width: 10%;" >বিষয়</th>
                            <th class="text-center" style="width: 10%;" >আবেদনকারী</th>
                            <th class="text-center" style="width: 10%;" >পূর্ববর্তী প্রেরক</th>
                            <th class="text-center" style="width: 10%;" >মূল প্রাপক</th>
                            <th class="text-center" style="width: 5%;" >প্রাপ্তির তারিখ</th>
                            <th class="text-center" style="width: 5%;" >গোপনীয়তা</th>
                            <th class="text-center" style="width: 5%;" >অগ্রাধিকার</th>
                            <th class="text-center" style="width: 10%;" >সর্বশেষ অবস্থা </th>
                        </tr>
                        <tr><td class="text-center" colspan="14" style="color:red;"> দুঃখিত কোন তথ্য পাওয়া যায় নি।  </td></tr>
                </table>
        </div>
            <?php
        }
        ?>
        <div class="actions text-center">
            <?= customPagination($this->Paginator) ?>

        </div>
<script>
    jQuery(document).ready(function () {
        $('.table').floatThead({
            position: 'absolute',
            top: jQuery("div.navbar-fixed-top").height()
        });
        $('.table').floatThead('reflow');
    });

    $(document).ajaxStop(function() {
        $("#total_result_count").text('<?=enTobn($this->Paginator->params()['count'])?>');
    })
</script>


