<?php
$prev = "";
$count = 0;
if (!empty($data)) {
    ?>
    <div class="table-container  table-scrollable">
        <table class="table table-bordered table-hover">
            <thead>
                <tr class="heading">
                    <th class="text-center" rowspan="2" style="width: 5%">ক্রমিক সংখ্যা</th>
                    <th class="text-center" colspan="2" style="width: 10%">প্রাপ্ত পত্রের</th>
                    <th class="text-center" rowspan="2" style="width: 10%">কাহার নিকট হইতে প্রাপ্ত</th>
                    <th class="text-center" rowspan="2" style="width: 10%">পত্রের সংক্ষিপ্ত বিষয়</th>
                    <th class="text-center" rowspan="2" style="width: 10%">নথি নম্বর</th>
                    <th class="text-center" rowspan="2" style="width: 5%">চূড়ান্ত ব্যবস্থা গ্রহণের তারিখ</th>
                    <th class="text-center" rowspan="2" style="width: 10%">মন্তব্য</th>
                    <th class="text-center" rowspan="2" style="width: 5%">গতিবিধি বিবরণ</th>

                </tr>
                <tr class="heading">
                    <th class="text-center" >স্মারক নম্বর</th>
                    <th class="text-center" >তারিখ</th>
                </tr>
            </thead>
            <tbody>
            <?php
            foreach ($data as $key => $value) {
                $unitInformation = $value['to_office_unit_name'];
                $unitId= $value['to_office_unit_id'];
                if ($prev != $unitId) {
                    $prev = $unitId;   ?>
                    <tr>
                        <th class="text-center" colspan="9"><?= h($unitInformation) . '<br>' . h($office_name['office_name_bng']) ?></th>
                    </tr>
                    <?php
                    $count = 0;
                }
                $count++;

                ?>
                <tr>
                    <td class="text-center"><?= $this->Number->format($count) ?></td>
                    <td class="text-center"><?= h($value['sender_sarok_no']) ?></td>
                    <td class="text-center"><?= Cake\I18n\Time::parse($value['created']) ?></td>
                    <td class="text-center"><?= h($value['sender_name']) . ', ' . h($value['sender_officer_designation_label']) . __(!empty($value['sender_office_unit_name']) ? (", " . h($value['sender_office_unit_name'])) : '') . __(!empty($value['sender_office_name']) ? (", " . h($value['sender_office_name'])) : '' ) ?></td>
                    <td class="text-center"><?= h($value['dak_subject']) ?></td>
                    <td class="text-center"><?= !empty($value['NothiParts']['nothi_no']) ? h($value['NothiParts']['nothi_no']) : "প্রযোজ্য নয়"; ?>
                    </td>
                    <td class="text-center"><?= !empty($value['NothiParts']['nothi_no']) ? Cake\I18n\Time::parse($value['NothiPotros']['created']) : "প্রযোজ্য নয়";  ?>
                    </td>

                    <td class="text-center" style="width: 10%"></td>
                    <td class="text-center" style="width: 10%;vertical-align: middle;">
                        <a href="javascript;" class="linkGotibidhi" data-toggle="modal" data-id="<?php echo $value['dak_id'];?>"  data-type="<?php echo $value['dak_type'];?>" data-target="#modalGotibidhiBiboron">গতিবিধির বিবরণ</a>
                    </td>
                </tr>

                <?php } ?>
        </table>
    </div>
    <?php
} else {
    ?>
    <div class="table-container  table-scrollable">
        <table class="table table-bordered table-striped">
            <tr class="heading">
                <th class="text-center" rowspan="2" style="width: 5%">ক্রমিক সংখ্যা</th>
                <th class="text-center" colspan="2" style="width: 20%">প্রাপ্ত পত্রের</th>
                <th class="text-center" rowspan="2" style="width: 10%">কাহার নিকট হইতে প্রাপ্ত</th>
                <th class="text-center" rowspan="2" style="width: 10%">পত্রের সংক্ষিপ্ত বিষয়</th>
                <th class="text-center" rowspan="2" style="width: 10%">নথি নম্বর</th>
                <th class="text-center" rowspan="2" style="width: 5%">চূড়ান্ত ব্যবস্থা গ্রহণের তারিখ</th>
                <th class="text-center" rowspan="2" style="width: 10%">মন্তব্য</th>
                <th class="text-center" rowspan="2" style="width: 5%">গতিবিধি বিবরণ</th>

            </tr>
            <tr class="heading">
                <th class="text-center" >স্মারক নম্বর</th>
                <th class="text-center" >তারিখ</th>
            </tr>
            <tr><td class="text-center" colspan="9" style="color:red;"> দুঃখিত কোন তথ্য পাওয়া যায় নি।  </td></tr>
            </tbody>
        </table>
    </div>
    <?php
}
?>
<div class="actions text-center">
    <?= customPagination($this->Paginator) ?>

</div>


<!-- Modal -->
<div id="modalGotibidhiBiboron" class="modal fade modal-purple height-auto" data-backdrop="static" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">গতিবিধির বিবরণ</h4>

            </div>
            <div class="modal-body" id="modalGotibidhiBiboron-body">



            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?=__('Close') ?></button>
            </div>
        </div>

    </div>
</div>



<script>
    jQuery(document).ready(function () {

        $('.table').floatThead({
            position: 'absolute',
            top: jQuery("div.navbar-fixed-top").height()
        });
        $('.table').floatThead('reflow');

        $('.linkGotibidhi').on('click',function(){

            // var url = js_wb_root + "DakRegisters/dakDiaryRegisterGotibidhi";
            var url = js_wb_root + "dak_movements/movementHistory";
            var dak_id =  $(this).attr('data-id');
            var dak_key =  $(this).attr('data-key');

            $('#modalGotibidhiBiboron-body').html('');
            $.ajax({
                type: "post",
                cache: false,
                url: url,
                data:{
                    dak_id:dak_id,
                    dak_type:dak_key
                },
                dataType: "html",
                success: function (res) {

                    $('#modalGotibidhiBiboron-body').html(res);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr);
                },
                async: false
            });
        });

    });

    $(document).ajaxStop(function() {
        $("#total_result_count").text('<?=enTobn($this->Paginator->params()['count'])?>');
    })
</script>





