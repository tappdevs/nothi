<div class="portlet light ">
    <div class="portlet-title">
        <div class="caption"><i class="fs1 a2i_gn_details1"></i>গতিবিধি নিবন্ধন বহি</div>
        <div class="pull-right"><button id="content-export-button" class="btn btn-sm btn-info">এক্সপোর্ট করুন</button>
            <form method="post" id="content-export-form" action="javascript:;" hidden></form>
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-toolbar">
            <div class="row text-center">
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="hidden-print page-toolbar pull-right portlet-title">
                        <input type="hidden" id="expDate" value="">
                        <div id="dashboard-report-range" class=" tooltips btn btn-sm btn-default"
                             data-container="body"
                             data-placement="bottom" data-original-title="তারিখ নির্বাচন করুন">
                            <i class="icon-calendar"></i>&nbsp; <span
                                class="thin uppercase visible-lg-inline-block">তারিখ নির্বাচন করুন</span>&nbsp;
                            <i class="glyphicon glyphicon-chevron-down"></i>
                        </div>
                    </div>

                    <!--  Date Range End  -->

                </div>

            </div>
        </div>
        <div id="table">
            <div class="portlet light">
                <div class="portlet-body inbox-content">



                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo CDN_PATH; ?>js/reports/dak_movement_reports.js" type="text/javascript"></script>
<script src="<?php echo CDN_PATH; ?>daptorik_preview/js/report_date_range.js"></script>

<script>
    jQuery(document).ready(function () {
        $(this).find('body').addClass('page-sidebar-closed');
        $(this).find('.page-sidebar-menu').addClass('page-sidebar-menu-closed');
        DateRange.init();
        DateRange.initDashboardDaterange();

        $(document).off('click','.pagination a').on('click','.pagination a',function(ev){
            ev.preventDefault();
            PROJAPOTI.ajaxLoad($(this).attr('href'),'.inbox-content');
        });
    });
    $('#content-export-button').on("click",
        function () {
            var range_date_en = $('#expDate').val();
            var range_date_bn = $('#dashboard-report-range').find('.visible-lg-inline-block').text();
            var _url="<?= $this->Url->build(["controller" => "DakRegisters", "action" => "dakMovementRegisterExcel"]); ?>"+"/"+range_date_en+"/"+range_date_bn;
            $("#content-export-form").attr("action", _url);
            $('#content-export-form').submit();
        });
</script>