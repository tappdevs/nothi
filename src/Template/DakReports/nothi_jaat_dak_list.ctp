<style>
    .select2-container .select2-choice {
        height: 36px !important;
        padding: 5px 8px !important;
    }
</style>
<div class="portlet light">
    <div class="portlet-title">
        <div class="caption"><i class="fs1 a2i_gn_details1"></i>নথিজাত ডাকসমূহ </div>
        <div class="pull-right">
            <div class="btn-group btn-group-round">
                <button title="এক্সেল ডাউনলোড করুন" data-type="excel" class="btn btn-md green content-export-button"><i class="fa fa-file-excel-o"></i> এক্সেল</button>
                <button title="পিডিএফ ডাউনলোড করুন" data-type="pdf" class="btn btn-md btn-danger content-export-button"><i class="fa fa-file-pdf-o"></i> পিডিএফ</button>
            </div>
            <form method="post" id="content-export-form" action="javascript:;" hidden></form>
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-toolbar">
            <div class="row text-center">
                <div class="col-md-12">

                    <h4><?php // echo $office['office_records'] [0]['office_name'];  ?></h4>
                    <h5><?php // echo $office['office_records'] [0]['office_address'];  ?></h5>


                </div>
            </div>
            <div class="row">
                <div class="col-md-12">

                    <!-- Tools start  -->

                    <!--<div class="hidden-print btn-group pull-left">
                        <button class="btn dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Tools <i
                                class="glyphicon glyphicon-chevron-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right">
                            <li>
                                <a href="javascript:;">
                                    Save as PDF </a>
                            </li>
                            <li>
                                <a href="javascript:window.print();">
                                    Print </a>
                            </li>
                        </ul>
                    </div>-->

                    <!--Tools end -->

                    <!--  Date Range Begain -->

                    <div class="hidden-print page-toolbar pull-right portlet-title">
                        <input type="hidden" id="expDate" value="">
                        <input type="hidden" id="start_date" value="">
                        <input type="hidden" id="end_date" value="">
                        <div id="dashboard-report-range" class=" tooltips btn btn-sm btn-default"
                             data-container="body"
                             data-placement="bottom" data-original-title="তারিখ নির্বাচন করুন">
                            <i class="icon-calendar"></i>&nbsp; <span
                                class="thin uppercase visible-lg-inline-block">তারিখ নির্বাচন করুন</span>&nbsp;
                            <i class="glyphicon glyphicon-chevron-down"></i>
                        </div>
                    </div>
                    <div class="hidden-print page-toolbar pull-right portlet-title">
                        <div style="width:350px">
                            <div style="float: left;border: solid 1px #eaeaea;padding: 6px 8px 4px;border-radius: 5px 0 0 5px !important;background: #eaeaea;">শাখা</div> <?= $this->Form->select('units_list',$units_list,['class' => 'from-control','id'=>'units_list', 'style' => 'width:300px']) ?>
                        </div>
                    </div>
                    <div class="hidden-print page-toolbar pull-left portlet-title col-md-2">
                        <div class="hidden" style="float: left;border: solid 1px #eaeaea;padding: 6px 8px 4px;border-radius: 5px 0 0 5px !important;background: #eaeaea;">তথ্য সংখ্যা</div>   <?= $this->Form->select('reports_page_limit',[10=>'১০', 20=>'২০', 50=>'৫০', 100=>'১০০'],['class' => 'from-control','id'=>'reports_page_limit'])
                        ?> মোট <span id="total_result_count">০</span> টি
                    </div>

                    <!--  Date Range End  -->

                </div>

            </div>
        </div>
        <div id="table">
            <div class="portlet light">
                <div class="portlet-body inbox-content">



                </div>
            </div>
        </div>
        <!--        <div class="row">
                    <div class="col-md-12">
                        <a class="btn pull-right btn-sm blue hidden-print margin-bottom-5" onclick="javascript:window.print();">
        <?php // echo __("Print") ?> <i class="fs1 a2i_gn_print2"></i> </a>
                    </div>
                </div>-->
    </div>
</div>


<script src="<?php echo CDN_PATH; ?>js/reports/nothi_jaat_dak_reports.js" type="text/javascript"></script>
<script src="<?php echo CDN_PATH; ?>daptorik_preview/js/report_date_range.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/floatthead/2.0.3/jquery.floatThead.min.js"></script>

<script>
    jQuery(document).ready(function () {
        $(this).find('body').addClass('page-sidebar-closed');
        $(this).find('.page-sidebar-menu').addClass('page-sidebar-menu-closed');

        $('#reports_page_limit').val(typeof ($.cookie("reports_page_limit")) != 'undefined'?$.cookie("reports_page_limit"):20);
        $('#reports_page_limit').select2();

        setTimeout(function () {
            DateRange.init();
            DateRange.initDashboardDaterange();
        },500);

        $(document).off('click','.pagination a').on('click','.pagination a',function(ev){
            ev.preventDefault();
            PROJAPOTI.ajaxLoad($(this).attr('href'),'.inbox-content');
        });
        $("#units_list").select2({ width: 'resolve' });
    });
    $('.content-export-button').on("click",
        function () {
            var type = $(this).data('type');
            var range_date_en = $('#expDate').val();
            var range_date_bn = $('#dashboard-report-range').find('.visible-lg-inline-block').text();
            var unit_id = $("#units_list").val();
            var _url="<?= $this->Url->build(["controller" => "DakReports", "action" => "nothiJaatDakListExcel"]); ?>"+"/"+type+"/"+range_date_en+"/"+range_date_bn+"/"+unit_id;
            $("#content-export-form").attr("action", _url);
            $('#content-export-form').submit();
        });
    $('#reports_page_limit').on('change', function () {
        $.cookie('reports_page_limit',$(this).val());
        SortingDate.init($('#start_date').val(), $('#end_date').val())
    });
    $('#units_list').change(function () {
        SortingDate.init($('#start_date').val(), $('#end_date').val());
    });
</script>