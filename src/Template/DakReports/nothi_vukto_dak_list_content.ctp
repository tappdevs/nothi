<?php
$prev = "";
$count = 0;
if (!empty($data)) { ?>
    <div class="table-scrollable ">
        <table class="table table-bordered table-hover">
            <thead>
                <tr class="heading">
                <th class="text-center" width="5%">ক্রমিক সংখ্যা</th>
                <th class="text-center">গ্রহণ নম্বর</th>
                <th class="text-center">স্মারক নম্বর</th>
                <th class="text-center">আবেদনের তারিখ</th>
                <th class="text-center">বিষয়</th>
                <th class="text-center">আবেদনকারী</th>
                <th class="text-center">নথিতে উপস্থাপনের তারিখ</th>
                <th class="text-center">গোপনীয়তা</th>
                <th class="text-center">অগ্রাধিকার</th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($data as $dak) {
                $unitInformation = h($dak['receiving_office_unit_name']);
                $unitId = h($dak['receiving_office_unit_id']);
                if ($prev != $unitId) {
                    $prev = $unitId;
                    $count = 0;
                    ?>
                    <tr>
                        <th class="text-center"
                            colspan="10"><?= h($unitInformation) . '<br>' . h($office_name['office_name_bng']) ?></th>
                    </tr>
                    <?php
                }
                $count++;
                ?>
                <tr>
                    <td class="text-center"><?= $this->Number->format($count) ?></td>
                    <td class="text-center"><?= $this->Number->format($dak['docketing_no']) ?></td>
                    <td class="text-center"><?= h($dak['sender_sarok_no']) ?></td>
                    <td class="text-center"><?= h($dak['created']) ?></td>
                    <td class="text-center"><?= h($dak['dak_subject']) ?></td>
                    <td class="text-center"><?= h($dak['sender_name']) . ', ' . h($dak['sender_officer_designation_label']) . __(!empty($dak['sender_office_unit_name']) ? (", " . h($dak['sender_office_unit_name'])) : '') . __(!empty($dak['sender_office_name']) ? (", " . h($dak['sender_office_name'])) : '') ?></td>
                    <td class="text-center"><?= h($dak['modified']) ?></td>
                    <td class="text-center"><?= (isset($security_level_list[$dak['dak_security_level']]) ? h($security_level_list[$dak['dak_security_level']]) : '') ?></td>
                    <td class="text-center"><?= (isset($priority_list[$dak['dak_priority_level']]) ? h($priority_list[$dak['dak_priority_level']]) : '') ?></td>
                </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
    </div>
    <?php
}
if (empty($data)) {
    ?>
    <div class="table-scrollable ">
        <table class="table table-bordered table-hover">
            <thead>
            <tr class="heading">
                <th class="text-center" rowspan="2" width="5%">ক্রমিক সংখ্যা</th>
                <th class="text-center" rowspan="2">গ্রহণ নম্বর</th>
                <th class="text-center" rowspan="2">স্মারক নম্বর</th>
                <th class="text-center" rowspan="2">আবেদনের তারিখ</th>
                <th class="text-center" rowspan="2">বিষয়</th>
                <th class="text-center" rowspan="2">আবেদনকারী</th>
                <th class="text-center" rowspan="2">নথিতে উপস্থাপনের তারিখ</th>
                <th class="text-center" rowspan="2">গোপনীয়তা</th>
                <th class="text-center" rowspan="2">অগ্রাধিকার</th>
            </tr>
            </thead>
            <tbody>


            <tr>
                <td class="text-center" colspan="9" style="color:red;"> দুঃখিত কোন তথ্য পাওয়া যায় নি।</td>
            </tr>
            </tbody>
        </table>
    </div>
    <?php
}
?>
<div class="actions text-center">
    <?= customPagination($this->Paginator) ?>

</div>
<script>
    jQuery(document).ready(function () {
        $('.table').floatThead({
            position: 'absolute',
            top: jQuery("div.navbar-fixed-top").height()
        });
        $('.table').floatThead('reflow');
    });

    $(document).ajaxStop(function() {
        $("#total_result_count").text('<?=enTobn($this->Paginator->params()['count'])?>');
    })
</script>
