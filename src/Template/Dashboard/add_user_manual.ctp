<link href="<?php echo CDN_PATH; ?>assets/global/plugins/jquery-tags-input/jquery.tagsinput.css" rel="stylesheet" />
<script src="<?php echo CDN_PATH; ?>assets/global/plugins/jquery-tags-input/jquery.tagsinput.js"></script>
<style>
	.tagsinput {
		width:100% !important;
		padding: 0 !important;
	}
	.tagsinput .tag {
		padding: 5px !important;
		margin: 3px !important;
		margin-right: 0px !important;
		border-radius: 5px !important;
	}
	.tagsinput .tag span {
		font-size: 13px !important;
	}
</style>
<div class="portlet light">
	<div class="portlet-body">
		<div class="row">
			<div class="col-md-6">
				<div class="form">
					<h3 class="text-center">
						<?= __('User Manual') ?> <small style="opacity:0.5">ভার্সন ২.০</small>
					</h3>
					<br>

					<?php echo $this->Form->create(null, ['autocomplete' => 'off']); ?>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label class="control-label">শিরোনাম</label>&nbsp;<span class="text-danger">*</span>
								<div><?php echo $this->Form->input('title', array('label' => false, 'class' => 'form-control', 'required' => 'required')); ?></div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label class="control-label">মডিউল</label>&nbsp;<span class="text-danger">*</span>
								<div><?php echo $this->Form->input('module', array('type'=>'select','label' => false, 'class' => 'form-control', 'required' => 'required', 'options' => ['নথি' => 'নথি','ডাক' => 'ডাক','দপ্তর' => 'দপ্তর','ড্যাশবোর্ড' => 'ড্যাশবোর্ড','রিপোর্ট' => 'রিপোর্ট'])); ?></div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label class="control-label">ট্যাগ</label>&nbsp;<span class="text-danger">*</span>
								<div><?php echo $this->Form->input('tags', array('type'=>'text','label' => false, 'class' => 'form-control', 'required' => 'required', 'id' => 'input-tags')); ?></div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label class="control-label">বিস্তারিত</label>&nbsp;<span class="text-danger">*</span>
								<div><?php echo $this->Form->input('description', array('type'=>'textarea','label' => false, 'class' => 'form-control', 'required' => 'required')); ?></div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 form-group">
							<button type="submit" class="btn blue saveNotice">সংরক্ষণ</button>
						</div>
					</div>
					<?php echo $this->Form->end(); ?>
				</div>
				<hr/>
			</div>
			<div class="col-md-6">
				<div class="portlet box blue">
					<div class="portlet-title">
						<div class="caption">
							ব্যবহার সহায়িকা তালিকা
						</div>
					</div>
					<div class="portlet-body">
						<div class="table-scrollable">
							<table class="table table-hover table-bordered" id="datatable_table">
								<thead>
								<tr>
									<th style="width:10%;" class="text-center">
										নম্বর
									</th>
									<th style="width:60%;" class="text-center">
										শিরোনাম
									</th>
									<th class="text-center heading" style="width:10%;">
										মডিউল
									</th>
									<th class="text-center heading" style="width:10%;">
										ট্যাগ
									</th>
									<th style="width:10%;"  class="text-center">
										কার্যক্রম
									</th>
								</tr>
								</thead>
								<tbody>

								<?php
								$i = 0;
								if (!empty($list)) {
									foreach ($list as $key => $val) {
										$i++;
										?>
										<tr data-id="<?= $val['id'] ?>">
											<td style="width:10%;" class="text-center" style="font-size: 14px!important;"><?= $this->Number->format($i); ?></td>
											<td style="width:60%;" id="update_<?= $val['id'] ?>" class="" style="font-size: 14px!important;">
												<?= $val['title']; ?>
											</td>
											<td style="width:10%;" class="text-center" style="font-size: 14px!important;"><?= $val['module']; ?></td>
											<td style="width:10%;" class="text-center" style="font-size: 14px!important;"><?= $val['tags']; ?></td>
											<td style="width:10%;" class="text-center" style="font-size: 14px!important;">
												<a data-toggle="modal" title="মুছে ফেলুন" data-target="#deleteModal"
												   onclick="del(<?= $val['id'] ?>);"><i class="fa fa-times"></i></a>
											</td>
										</tr>
										<?php
									}
								}
								?>

								</tbody>
							</table>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade modal-purple height-auto" data-backdrop="static" id="deleteModal"
     role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"> ডাক সিদ্ধান্ত বাতিল</h4>
			</div>
			<div class="modal-body">
				<form class="form-group">
					<label>আপনি কি সেটিংসটি মুছে ফেলতে চান?</label>
					<input type="hidden" class="form-control" id="delete_id">
					<input type="hidden" class="form-control" id="update_id">
				</form>
			</div>
			<div class="modal-footer">
				<div class="btn-group btn-group-round">
					<button type="button" class="btn green" onclick="delete_action();">মুছে ফেলুন</button>
					<button type="button" class="btn red" data-dismiss="modal">বন্ধ করুন</button>
				</div>
			</div>
		</div>

	</div>
</div>

<script src="<?php echo CDN_PATH; ?>projapoti-nothi/js/guard_file_upload.js?v=<?= js_css_version ?>" type="text/javascript"></script>
<?= $this->element('froala_editor_js_css') ?>
<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/froala_editor/js/plugins/image.min.js"></script>
<link rel="stylesheet" href="<?php echo CDN_PATH; ?>assets/global/plugins/froala_editor/css/plugins/image.css">

<script>
    $(document).ready(function () {
        var buttons = ['bold', 'italic', 'underline', 'insertImage', 'insertLink', 'insertTable', 'undo', 'redo'];
        $('#description').froalaEditor({
            key: 'xc1We1KYi1Ta1WId1CVd1F==',
            imageUploadURL: js_wb_root+'UserManual/text_image',
            toolbarSticky: false,
            wordPasteModal: true,
            wordAllowedStyleProps: ['font-size', 'background', 'color', 'text-align', 'vertical-align', 'background-color', 'padding', 'margin', 'height', 'margin-top', 'margin-left', 'margin-right', 'margin-bottom', 'text-decoration', 'font-weight'],
            wordDeniedTags: ['a','form'],
            wordDeniedAttrs: ['width'],
            tableResizerOffset: 10,
            tableResizingLimit: 20,
            toolbarButtons: buttons,
            toolbarButtonsMD: buttons,
            toolbarButtonsSM: buttons,
            toolbarButtonsXS: buttons,
            placeholderText: '',
            height: 300,
            enter: $.FroalaEditor.ENTER_BR,
            fontSize: ['10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30'],
            fontSizeDefaultSelection: '13',
            tableEditButtons: ['tableHeader', 'tableRemove', '|', 'tableRows', 'tableColumns', 'tableCells', 'tableCellBackground', 'tableCellVerticalAlign', 'tableCellHorizontalAlign', 'tableCellStyle'],
            fontFamily: {
                "Nikosh,SolaimanLipi,'Open Sans', sans-serif": "Nikosh",
                "Arial": "Arial",
                "Kalpurush": "Kalpurush",
                "SolaimanLipi": "SolaimanLipi",
                "'times new roman'": "Times New Roman",
                "'Open Sans Condensed',sans-serif": 'Open Sans Condensed'
            }
        });
    });

    $(document).ready(function() {
        $('#input-tags').tagsInput();
    });

    function del(id) {
        $('#delete_id').val(id);
    }

    function delete_action() {
        var id = $('#delete_id').val();
        $.ajax({
            type: 'POST',
            url: "<?=$this->Url->build(['controller' => 'Dashboard', 'action' => 'userManualDelete']) ?>",
            data: {"id": id},
            success: function (data) {
                window.location.reload();
            }
        });
    }
</script>