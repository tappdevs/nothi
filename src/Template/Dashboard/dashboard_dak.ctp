<?php
$session                 = $this->request->session();
$modules                 = $session->read('modules');
$selected_module         = $session->read('module_id');
$selected_office_section = $session->read('selected_office_section');
?>

<!-- BEGIN PAGE HEAD -->
<div class="page-head">
    <div class="row" style="padding-left: 15px;">
        <div class="portlet box">
            <div class="portlet-body">
                <?php
                if (!empty($niderror)) {
                    ?>
                <div class="alert alert-danger text-center bold">
                    <?= $niderror ?>
                </div>
                <?php
                }
                ?>
<!--                <div class="alert alert-danger text-center bold">
                  নথি অ্যাপ্লিকেশন সহজীকরনের কাজ চলছে। সাময়িক অসুবিধার জন্য আন্তরিক ভাবে দুঃখিত ।
                </div>-->
                <div class="row" style="padding-left: 5px;">
                    <div class="col-md-12 col-sm-12">
                        <div class="form-inline filter">
                            <input type="text" class="form-control input-md" placeholder="বিষয় দিয়ে  খুঁজুন" name="search" id="filter_input" style="width: 25%;">

                            <input type="text" class="form-control form-filter input-md" placeholder="প্রেরক" name="receiving_officer_name" id="receiving_officer_name" style="width: 17%;">

                            <select name="dak_security_level" class="form-control  form-filter input-md  select2-offscreen" style="width:12%;" id="dak-security-level" tabindex="-1" title="" data-original-title="">
                                <option value="">-: গোপনীয়তা :-</option>
                                <?php
                                foreach(json_decode(DAK_SECRECY_TYPE,false) as $k=>$v){
                                    echo "<option value={$k}>{$v}</option>";
                                }
                                ?>
                            </select>
                            <select name="dak_priority_level" class="form-control  form-filter input-md  select2-offscreen" style="width:12%;" id="dak-priority-level" tabindex="-1" title="" data-original-title="">
                                <option value="">-: অগ্রাধিকার :-</option>
                                <?php
                                foreach(json_decode(DAK_PRIORITY_TYPE,false) as $k=>$v){
                                    echo "<option value={$k}>{$v}</option>";
                                }
                                ?>
                            </select>

                            <div class=" input-group date-picker input-daterange" style="width: 25%;" data-date="<?php echo date('Y-m-d') ?>" data-date-format="yyyy-mm-dd">
                                <input placeholder="হইতে " type="text" class=" input-md form-filter" name="from" id="search_date_from" style="width:100%;height:34px;float:left;">
                                <span class="input-group-addon input-md"> হইতে </span>
                                <input placeholder="পর্যন্ত" type="text" class=" input-md form-filter" name="to" id="search_date_to"  style="width:100%;height:34px;float:left;">
                            </div>

                            <button title="বর্ধিত করুন" type="button" class="btn btn-md purple-medium hidden" onclick="showAdvance(this)"><i class="fa fa-chevron-down" aria-hidden="true"></i></button>
                            <div class="input-group filter1 btn-group">
                                <button type="button" title="খুঁজুন" class="btn btn-md green-haze " id="filter_submit_btn1"><i class="fs1 a2i_gn_search1" aria-hidden="true"></i></button>
                                <button class="btn btn-md red filter-cancel" title="<?php echo __(RESET) ?>"><i class="fs1 a2i_gn_reset2"></i></button>
                            </div>
                        </div>

                    </div>

                </div>

                <div class="content-dak">
                    <img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"><span>&nbsp;&nbsp;লোড করা হচ্ছে...</span>
                </div>
            </div>
        </div>

        <div class="clearfix margin-bottom-20">
        </div>
    </div>
</div>
<style>
    .form-control .select2-choice {
        border: 1px solid #e5e5e5 !important;
        background-color: #fff !important;
        background-image: none !important;
        filter: none !important;
        height: 34px !important;
        padding: 3px 0 0px 12px !important;
    }
    .select2-container .select2-choice .select2-arrow {
        background-image: none !important;
        background-color: #fff !important;
        filter: none !important;
        border-left: 1px solid #e5e5e5 !important;
    }
</style>
<?php
if (isset($dashboard_data) && !empty($dashboard_data)) {
    foreach ($dashboard_data as $dashboard) {
        ?>
        <form id="dak_unit_selection_form_<?php echo $dashboard['id'] ?>" method="post" action="">
            <?php
            echo $this->Form->hidden('selected_office_id',
                ['value' => $dashboard['office_id']])
            ?>
            <?php
            echo $this->Form->hidden('selected_office_unit_id',
                ['value' => $dashboard['office_unit_id']])
            ?>
            <?php
            echo $this->Form->hidden('selected_office_unit_organogram_id',
                ['value' => $dashboard['id']])
            ?>
            <?php
            echo $this->Form->hidden('selected_office_unit_organogram_label',
                ['value' => $dashboard['name']])
            ?>
            <?php echo $this->Form->hidden('office_selection_form') ?>
        </form>
        <?php
    }
}
?>
<script type="text/javascript">
    function submitSectionSelectionForm(action, org_id) {
        $("#dak_unit_selection_form_" + org_id).attr("action", action);
        $("#dak_unit_selection_form_" + org_id).submit();
    }
</script>

<script>
    jQuery.ajaxSetup({
        cache: true
    });
    $("#selectDesignation").on('change', function () {

        var url = '<?= $this->request->webroot ?>Dashboard/dashboard';

        submitSectionSelectionForm(url, $(this).val());

    });

    $(document).ready(function () {
        <?php
       if(!empty($selected_office_section['office_unit_organogram_id'])){
         ?>
                  PROJAPOTI.ajaxLoad('<?= $this->request->webroot ?>dakMovements/dakInbox/inbox', '.content-dak');
             <?php
       }else{
           ?>
                   $(".content-dak").html('');
                   $(".filter1").html('');
        <?php
       }
        ?>

    });
    function showAdvance(element) {
        if ($(".advanceSearch").css('display') == 'none') {
            $(element).find('i').removeClass('fa-chevron-down');
            $(element).find('i').addClass('fa-chevron-up');
            $(element).attr('data-original-title', 'বন্ধ করুন');
            $(".advanceSearch").toggle();
        } else {
            $(element).find('i').removeClass('fa-chevron-up');
            $(element).find('i').addClass('fa-chevron-down');
            $(".advanceSearch").toggle();
            $(element).attr('data-original-title', 'বর্ধিত করুন');
        }
    }
</script>
<script src="<?php echo CDN_PATH; ?>daptorik_preview/js/tbl_dak_list_ajax.js"></script>
<script>
    $(function(){
        $.ajax({
            url: '<?= $this->Url->build(['_name' => 'netSpeed']) ?>',
            dataType:'json',
            cache: false,
            async: true,
            success: function(res){
                if(parseInt(res)>=20){
                    toastr.options = {
                        "closeButton": true,
                        "positionClass": "toast-bottom-right",
                        timeOut: 15000
                    };
                    toastr.warning("আপনার ইন্টারনেট কানেকশন কিছুটা ধীর। তাই নথি ব্যবহারে কিছুটা বিঘ্ন ঘটতে পারে। দয়া করে ইন্টারনেট কানেকশন আপডেট করুন। ধন্যবাদ।");
                }
            },
            error: function(err,xstatus){

            }
        })
    })
</script>
<!-- END PAGE HEAD -->