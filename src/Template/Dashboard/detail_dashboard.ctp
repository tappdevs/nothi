<style>
    .portlet > .portlet-title > .tools > a{
        height:14px!important;
    }

    table td, table th{
        vertical-align: middle!important;
    }

    table th{
        text-align: center!important;
    }
</style>
<div class="alert alert-info font-lg text-center">
    <i class="fa fa-info-circle " aria-hidden="true"></i>
   প্রিয় ব্যবহারকারী, ড্যাশবোর্ডসমূহ এখন থেকে প্রতি ৩ ঘণ্টা পর পর আপডেট হবে।
</div>
<?php
if ($canSee != 1) {
    ?>
    <div class="well-sm font-lg text-center">
        <a class="btn btn-primary  " target="_blank" href ="<?= $this->request->webroot.'loginDetails/'.$owninfo['office_id'] ?>">
            <i class="fa fa-user-circle-o" aria-hidden="true"></i>
            নিজ অফিসের ব্যবহারকারির লগইন তথ্য
        </a>
    </div>
    <div class="clearfix"></div>
    <?php
}
else {
	?>
	<div class="well-sm font-lg text-center">
        <?php
        if(defined('Live') && Live == 0):
            ?>
            <a class="btn btn-primary" target="_blank" href="http://office.training.service.gov.bd">
                <i class="fs0 a2i_gn_dashboard2"></i>
                নাগরিক সেবার ড্যাশবোর্ড
            </a>
            <?php
        endif;
        ?>
	</div>
	<div class="clearfix"></div>
	<?php
}
if ($canSee == 1) {
    ?>
    <div class="row" >
        <?php
            if(!empty($show_geo_div) && $show_geo_div == 1):
                ?>
                <div class="col-md-4 col-sm-4">
                    <div id="officeDashboard"></div>
                </div>
                <div class="col-md-4 col-sm-4" id ="officeListGeo">
                </div>
                <div class="col-md-4 col-sm-4" id ="officeListCell">
                </div>
                <?php
            else:
                ?>
                <div class="col-md-8 col-sm-8">
                    <div id="officeDashboard"></div>
                </div>
                <div class="col-md-4 col-sm-4" id ="officeListCell">
                </div>
                <?php
            endif;
        ?>

    </div>
<?php
}
?>


<?php
echo $this->Cell('DakDashboard::ownSummary');
?>
<div class="clearfix"></div>
<div class="row" style="    
     margin-left: -15px;
     margin-right: -15px;">
    <div class="col-md-12 col-sm-12">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    কার্যক্রমের অগ্রগতি
                </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <h4>ব্যক্তিগত </h4>
                        <?php echo $this->Cell('DakDashboard::agingOwnDesk'); ?>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <h4>অধীনস্থ শাখা</h4>
                        <div id="unitList">

                        </div>
                        <?php // echo $this->Cell('DakDashboard::unitList',[$owninfo['office_id'], $owninfo['office_unit_id']]) ?>
                    </div>

                </div>
            </div>
        </div>
    </div>
<!--    <div class="col-md-4 col-sm-12">-->
        <?php //echo $this->Cell('DakDashboard::OfficeList', [$owninfo['office_id']]);
        ?>
<!--    </div>-->
</div>
<div class="clearfix"></div>

<script>

    $('body').addClass('page-quick-sidebar-over-content page-full-width');

</script>
<script>
    $(document).ready(function () {
<?php
if ($canSee == 1) {
    ?>
            $("#officeDashboard").html('<img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp; লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');
            $.ajax({
                url: "<?= $this->request->webroot ?>dashboard/officeSummary/<?= $owninfo['office_id'] ?>",
                            type: 'post',
                            cache: false,
                            success: function (data) {
                                $("#officeDashboard").html(data);
                                $('#officeListCell').html('<img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp; লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');
                                $.ajax({
                                    url: "<?= $this->Url->build(['controller' => 'dashboard' , 'action' => 'officeListCell']) ?>",
                                    type: 'post',
                                    cache: true,
                                    data: {"office_id": <?= $owninfo['office_id'] ?>},
                                    success: function (html) {
                                        $('#officeListCell').html(html);
                                        $('#officeListCell .portlet-body').css({
                                            "height": $("#officeDashboard .portlet-body").height(),
                                            "overflow": "auto"
                                        });
                                        <?php
                                        if(!empty($show_geo_div) && $show_geo_div == 1):
                                        ?>
                                        $('#officeListGeo').html('<img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp; লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');

                                        $.ajax({
                                            url: "<?= $this->request->webroot ?>dashboard/officeListForGeo",
                                            type: 'post',
                                            cache: false,
                                            data: {},
                                            success: function (html) {
                                                $('#officeListGeo').html(html);
                                                $('#officeListGeo .portlet-body').css({
                                                    "height": $("#officeDashboard .portlet-body").height(),
                                                    "overflow": "auto"
                                                });
                                                $('#unitList').html('<img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp; লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');
                                                $.ajax({
                                                    url: "<?= $this->request->webroot ?>dashboard/unitListCell",
                                                    type: 'post',
                                                    cache: false,
                                                    data: {"office_id": <?= $owninfo['office_id'] ?>, "office_unit_id": <?= $owninfo['office_unit_id'] ?>},
                                                    success: function (html) {
                                                        $('#unitList').html(html);
                                                    }
                                                });
                                            }
                                        });
                                        <?php
                                        else:
                                        ?>
                                        $('#unitList').html('<img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp; লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');
                                        $.ajax({
                                            url: "<?= $this->request->webroot ?>dashboard/unitListCell",
                                            type: 'post',
                                            cache: false,
                                            data: {"office_id": <?= $owninfo['office_id'] ?>, "office_unit_id": <?= $owninfo['office_unit_id'] ?>},
                                            success: function (html) {
                                                $('#unitList').html(html);
                                            }
                                        });
                                        <?php
                                        endif;
                                        ?>
                                    }
                                });
                            }
                        });
    <?php
}
else {
    ?>
                        $('#unitList').html('<img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp; লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');
                        $.ajax({
                            url: "<?= $this->request->webroot ?>dashboard/unitListCell",
                            type: 'post',
                            cache: false,
                            data: {"office_id": <?= $owninfo['office_id'] ?>, "office_unit_id": <?= $owninfo['office_unit_id'] ?>},
                            success: function (html) {
                                $('#unitList').html(html);
                            }
                        });
    <?php
}
?>

                });
</script>