<style>
    .portlet > .portlet-title > .tools > a{
        height:14px!important;
    }
    
    table td, table th{
        vertical-align: middle!important;
    }
    
    table th{
        text-align: center!important;
    }
</style>
<?php

?>

<?php
if ($canSee == 1) {
    echo $this->Cell('DakDashboard::officeSummary',[$owninfo['office_id'],1]);
}
?>  

<?php
echo $this->Cell('DakDashboard::ownSummary',[$owninfo['employee_record_id'],$owninfo['office_id'],1]);
?>
<div class="clearfix"></div>
<div class="row" style="    
    margin-left: -15px;
    margin-right: -15px;">
    <div class="<?php if(!isset($api) || $api = 0 ){ ?> col-md-8 col-sm-12 <?php }else{ ?> col-md-12 col-sm-12 <?php } ?>">
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption">
                        কার্যক্রমের অগ্রগতি
                    </div>

                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <h4>ব্যক্তিগত </h4>
                            <?php echo $this->Cell('DakDashboard::agingOwnDesk',[$owninfo['office_id'],0,0,$owninfo['employee_record_id'],1]); ?>                
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <h4>অধীনস্থ শাখা</h4>
                            <?php echo $this->Cell('DakDashboard::unitList',[$owninfo['office_id'],$owninfo['office_unit_id'],1]) ?>               
                        </div>

                    </div>
                </div>
            </div>
        </div>
    <?php if(!isset($api) || $api = 0 ): ?>
    <div class="col-md-4 col-sm-12">
            <?php echo $this->Cell('DakDashboard::OfficeList',[$owninfo['office_id']]); ?>
    </div>
    <?php endif; ?>
</div>
<div class="clearfix"></div>

<script>

    $('body').addClass('page-quick-sidebar-over-content page-full-width');

</script>