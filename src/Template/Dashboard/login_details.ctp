<?php
if ($tableType == 'Office') {
    $th = ' অফিস: ' . $tableHeader;
} else if ($tableType == 'Unit') {
    $th = ' শাখা: ' . $tableHeader;
} else if ($tableType == 'Designation') {
    $th = ' কর্মকর্তার পদবি ';
} else {
    $th = '';
}
?>
<div class="portlet box purple">
    <div class="portlet-title">
        <div class="caption"> <?= $tableHeader ?> </div>
        <div class="tools">
            <a target="_blank" class="login_history margin-bottom-10" title=" অনুপস্থিত ব্যবহারকারী " href="<?= $this->request->webroot ?>absentDetails<?=$url?>" style="color:#fff;text-decoration: none;">
                <span class="label bg-green" style="font-size: 16px!important;">  অনুপস্থিত ব্যবহারকারীর তালিকা  </span></a>
                <?php if($is_session == 1): ?>
                    <a class=" margin-bottom-10" title="ফেরত যান" href="<?= $this->request->webroot ?>Dashboard/detailDashboard" style="color:#fff;text-decoration: none;">
                    <span class="label bg-red" style="font-size: 16px!important;">ফেরত যান</span></a>
                <?php endif; ?>
        </div>
    </div>
    <div class="portlet-body">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    আজকের ব্যবহারকারী
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-scrollable">
                    <table class="table table-hover table-bordered">
                        <thead>
                            <tr class="heading">
                                <th class="text-center"> <?= $th ?> </th>
                                <th class="text-center">  লগইন সময় </th>
                                <th class="text-center">  লগইন আইপি </th>
                            </tr>
                        </thead>
                        <tbody>
<?php
if (!empty($data['today'])) {
//                                pr($data); die;
    foreach ($data['today'] as $key => $val) {
        ?>
                                    <tr class="text-center">
                                    <?php
//                                    $employee_info = \Cake\ORM\TableRegistry::get('EmployeeRecords')->getEmployeeInfoByRecordId($val['employee_record_id']);
                                    if(empty($val['employee_name']) ||  ($val['employee_name']) == NULL){
                                        
                                    }else{
                                        $val['employee_name'] = h($val['employee_name']).', ';
                                    }
                                    if ($tableType == 'Unit' || $tableType == 'Designation') {
                                        echo "  <td><b> " .  ($val['employee_name'])  . $val['designation'] . "</b></td>";
                                    } else {
                                        echo "  <td><b> " .  ($val['employee_name'])  . $val['designation'] . ", " . $val['unit_name'] . "</b></td>";
                                    }
                                    ?>
                                        <td> <?= (date($val['login_time'])) ?> </td>
                                        <td> <?= ($val['client_ip']) ?> </td>
                                    </tr>
                                        <?php
                                    }
                                } else {
                                    echo "<tr> <td colspan=3 class='danger font-md  text-center'> দুঃখিত কোন তথ্য পাওয়া যায়নি। </tr>";
                                }
                                ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    গতকালের ব্যবহারকারী
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-scrollable">
                    <table class="table table-hover table-bordered">
                        <thead>
                            <tr class="heading">
                                <th class="text-center"> <?= $th ?> </th>
                                <th class="text-center">  লগইন সময় </th>
                                <th class="text-center">  লগইন আইপি </th>
                            </tr>
                        </thead>
                        <tbody>
<?php
if (!empty($data['yesterday'])) {
//                                pr($data); die;
    foreach ($data['yesterday'] as $key => $val) {
        ?>
                                    <tr class="text-center">
                                    <?php
//                                    $employee_info = \Cake\ORM\TableRegistry::get('EmployeeRecords')->getEmployeeInfoByRecordId($val['employee_record_id']);
                                      if(empty($val['employee_name']) ||  ($val['employee_name']) == NULL){

                                    }else{
                                        $val['employee_name'] = h($val['employee_name']).', ';
                                    }
                                    if ($tableType == 'Unit' || $tableType == 'Designation') {
                                        echo "  <td><b> " . $val['employee_name'] . $val['designation'] . "</b></td>";
                                    } else {
                                        echo "  <td><b> " . $val['employee_name'] . $val['designation'] . ", " . $val['unit_name'] . "</b></td>";
                                    }
                                    ?>
                                        <td> <?= (date($val['login_time'])) ?> </td>
                                        <td> <?= ($val['client_ip']) ?> </td>
                                    </tr>
                                        <?php
                                    }
                                } else {
                                    echo "<tr> <td colspan=3 class='danger font-md text-center'> দুঃখিত কোন তথ্য পাওয়া যায়নি। </tr>";
                                }
                                ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>