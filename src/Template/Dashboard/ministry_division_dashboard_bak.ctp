
<style>
    .portlet > .portlet-title > .tools > a{
        height:14px!important;
    }

    table td, table th{
        vertical-align: middle!important;
    }

    table th{
        text-align: center!important;
    }
    .minwidth{
        min-width : 20%!important;
    }
</style>
<!--<div class="alert alert-danger font-lg text-center">
    <i class="fa fa-info-circle " aria-hidden="true"></i>
    ড্যাশবোর্ডের যৌক্তিক বিশ্লেষণ ও প্রয়োজনীয় পরিবর্তনের কার্যক্রম চলছে। সাময়িক অসুবিধার জন্য আমরা আন্তরিকভাবে দুঃখিত।
</div>-->
<div class="row page-all">
    <div class="col-md-12 ">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="panel-title text-center">
                    মনিটরিং ড্যাশবোর্ড
                </div>
            </div>
            <div class="panel-body">
                <div class="row" id="loadSummary">

                </div>
                <div class="row form-group ">
                    <div class="col-md-12" >
                        <div class="row">
                            <div class="table-scrollable">
                                <table class="table table-bordered table-hover" id="details" >
                                    <thead >
                                        <tr class="info">
                                            <th rowspan=3 class="minwidth">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                            <th colspan=2> অফিস সংখ্যা </th>
                                            <th colspan=2>ব্যবহারকারী </th>
                                            <th colspan=6>ডাক</th>
                                            <th colspan=5>নথি</th>
                                        </tr>
                                        <tr class="info" >
                                            <th rowspan="2"> মোট </th>
                                            <th rowspan="2"> কার্যকর </th>
                                            <th rowspan="2"> মোট </th>
                                            <th rowspan="2"> আজকের </th>
                                            <th colspan=4> গতকালের  </th>
                                            <th colspan=2> অদ্যাবধি  </th>
                                            <th colspan=3> গতকালের  </th>
                                            <th colspan=2> অদ্যাবধি  </th>
                                        </tr>
                                        <tr class="info" >
                                            <th>  গ্রহণ </th>
                                            <th>  প্রেরণ </th>
                                            <th> নথিজাত </th>
                                            <th> উপস্থাপন </th>
                                            <th> নিষ্পন্ন ডাক </th>
                                            <th> অনিষ্পন্ন ডাক </th>
                                            <th >  স্ব- উদ্যোগে 
                                                নোট
                                            </th>
                                            <th>  ডাক থেকে সৃজিত নোট </th>
                                            <th> পত্রজারি </th>
                                            <th> নিষ্পন্ন নোট </th>
                                            <th> অনিষ্পন্ন নোট </th>
                                        </tr>
                                    </thead>
                                    <tbody class="boldclass" id="AddData">
                                    </tbody>
                                    <tfoot>
                                        <tr id="waitMessage"></tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/floatthead/1.4.2/jquery.floatThead.min.js"></script>
<script>
<?php $ministry_ids = array_keys($officeMinistries); ?>

    $(document).ready(function () {
        loadSummary();
        $('#details').floatThead({
            position: 'absolute'
        });
        $(window).scroll(function () {
            var has_layout = $('#details[style*="table-layout: fixed"]');
            if (has_layout.length > 0) {
                jQuery("div.navbar-fixed-top").hide();
            } else {
                jQuery("div.navbar-fixed-top").show();
            }
        });
        var ministry_ids =<?php echo json_encode($ministry_ids); ?>;
          autoDashboard(ministry_ids[0], 0);

        $('[data-toggle="tooltip"]').tooltip({'placement':'bottom'});
        function loadSummary() {
             $('#loadSummary').html('<div class="text-center"><img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp; লোড হচ্ছে। একটু অপেক্ষা করুন... </span></div>');
            $.ajax({
                type: 'POST',
                url: "<?php echo $this->Url->build(['controller' => 'Dashboard', 'action' => 'ministryDivisionDashboard']) ?>",
                success: function (data) {
                    $('#loadSummary').html(data);
//                    autoDashboard(ministry_ids[0], 0);
                },
                error: function () {
//                    autoDashboard(ministry_ids[0], 0);
                }
            });
            $('#waitMessage').html('<td colspan="14"><div class="text-center"><img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp; লোড হচ্ছে। একটু অপেক্ষা করুন... </span></div><td>');
        }
        function autoDashboard(ministry_id, ministry_index) {
            $('#details').trigger('reflow');
            $.ajax({
                type: 'POST',
                url: "<?php
echo $this->Url->build(['controller' => 'officeSettings',
    'action' => 'loadLayersByMinistry'])
?>",
                dataType: 'json',
                data: {office_ministry_id: ministry_id},
                success: function (data) {
                    $.ajax({
                        type: 'POST',
                        url: "<?php
echo $this->Url->build(['controller' => 'Dashboard',
    'action' => 'ministryDivisionDashboardAjax'])
?>",
                        data: {office_ministry_id: ministry_id, "office_layer_id": data},
                        dataType: 'json',
                        success: function (data) {
                            ministry_index++;
                            print_all(data);
                            if (ministry_index < ministry_ids.length) {
                                autoDashboard(ministry_ids[ministry_index], ministry_index);
                            } else {
                                print_total();
                            }

                        },
                        error: function () {
                            ministry_index++;
                            if (ministry_index < ministry_ids.length) {
                                autoDashboard(ministry_ids[ministry_index], ministry_index);
                            } else {
                                print_total();
                            }
                        }
                    });
                }
            });
        }
    });

    var TotalOffices = 0;
    var Offices = 0;
    var TotalEmployee = 0;
    var TotalLogin = 0;
    var TotalYesterdayOutbox = 0;
    var TotalYesterdayInbox = 0;
    var TotalYesterdayNothijato = 0;
    var TotalYesterdayNothiVukto = 0;
    var TotalYesterdaySrijitoNote = 0;
    var TotalYesterdayDakSohoNote = 0;
    var TotalYesterdayPotrojari = 0;
    var TotalNisponnoDak = 0;
    var TotalOnisponnoDak = 0;
    var TotalNisponnoNote = 0;
    var TotalOnisponnoNote = 0;
    var office_ids = [];

    function print_all(alldata, type)
    {

        var dashboard = '';
        $.each(alldata, function (index1, val1) {
            if (val1.Offices) {
                dashboard = dashboard + '<tr id="mnstr' + index1 + '" >' +
                        '<td class="minwidth"><div><div class="pull-left"><b>';
                if (val1.TotalOffices != '০') {
                    dashboard += '<a onclick="hide_layer(' + index1 + ')" ><i class="fa fa-plus-square-o" aria-hidden="true"></i></a>&nbsp;&nbsp;';
                }
                dashboard += val1.Name + '</b></div></div></td>' +
                        '<td class="text-center" ><b>' + val1.TotalOffices + '</b></td><td class="text-center"><span class="badge badge-success" style="font-size:12pt!important;"  data-toggle="tooltip" title="কার্যকর অফিস সংখ্যা ">' + val1.Offices + '</span></td> ' +
                        '<td class="text-center"> <b>' + val1.TotalEmployee + '</b></td><td class="text-center"><span class="badge badge-primary" style="font-size:12pt!important;" data-toggle="tooltip" title="আজকের লগইন ব্যক্তিবর্গ " >' + val1.TotalLogin + '</span></td>' +
                        '<td class="text-center"><b>' + val1.TotalYesterdayInbox + '</b></td>' +
                        '<td class="text-center"><b>' + val1.TotalYesterdayOutbox + '</b></td>' +
                        '<td class="text-center"><b>' + val1.TotalYesterdayNothijato + '</b></td> ' +
                        '<td class="text-center"><b>' + val1.TotalYesterdayNothiVukto + '</b></td> ' +
                        '<td class="text-center"><b>' + val1.TotalNisponnoDak + '</b></td>' +
                        '<td class="text-center"><b>' + val1.TotalOnisponnoDak + '</b></td>' +
                        '<td class="text-center"><b>' + val1.TotalYesterdaySrijitoNote + '</b></td>' +
                        '<td class="text-center"><b>' + val1.TotalYesterdayDakSohoNote + '</b></td>' +
                        '<td class="text-center"><b>' + val1.TotalYesterdayPotrojari + '</b></td>' +
                        '<td class="text-center"><b>' + val1.TotalNisponnoNote + '</b></td>' +
                        '<td class="text-center"><b>' + val1.TotalOnisponnoNote + '</b></td>' +
                        '</tr>';
                TotalOffices += val1.EnTotalOffices;
                Offices += val1.EnOffices;
                TotalEmployee += val1.EnTotalEmployee;
                TotalLogin += val1.EnTotalLogin;
                TotalYesterdayOutbox += val1.EnTotalYesterdayOutbox;
                TotalYesterdayInbox += val1.EnTotalYesterdayInbox;
                TotalYesterdayNothijato += val1.EnTotalYesterdayNothijato;
                TotalYesterdayNothiVukto += val1.EnTotalYesterdayNothiVukto;
                TotalYesterdaySrijitoNote += val1.EnTotalYesterdaySrijitoNote;
                TotalYesterdayDakSohoNote += val1.EnTotalYesterdayDakSohoNote;
                TotalYesterdayPotrojari += val1.EnTotalYesterdayPotrojari;
                TotalNisponnoNote += val1.EnTotalNisponnoNote;
                TotalNisponnoDak += val1.EnTotalNisponnoDak;
                TotalOnisponnoDak += val1.EnTotalOnisponnoDak;
                TotalOnisponnoNote += val1.EnTotalOnisponnoNote;
            }
            if (typeof (val1) === 'object') {
                $.each(val1, function (index2, val2) {
                    if (val2.Offices) {
                        dashboard += '<tr class=" layer' + index1 + '"  style="display: none;" id="lyr' + index2 + '" >' +
                                '<td><div><div class="pull-left"  style="margin-left:10px!important;"><b>';
                        if (val2.TotalOffices != '০') {
                            dashboard += '<a onclick="hide_origin(' + index2 + ')" ><i class="fa fa-plus-square-o" aria-hidden="true"></i></a>&nbsp;&nbsp;';
                        }
                        dashboard += val2.Name + '</b></div></div></td>' +
                                '<td class="text-center"><b>' + val2.TotalOffices + '</b></td><td class="text-center"><span class="badge badge-danger" style="font-size:12pt!important;"  data-toggle="tooltip" title="কার্যকর অফিস সংখ্যা ">' + val2.Offices + '</span></td> ' +
                                '<td class="text-center"> <b>' + val2.TotalEmployee + '</b></td><td class="text-center"><span class="badge badge-primary" style="font-size:12pt!important;" data-toggle="tooltip" title="আজকের লগইন ব্যক্তিবর্গ " >' + val2.TotalLogin + '</span></td>' +
                                '<td class="text-center"><b>' + val2.TotalYesterdayInbox + '</b></td>' +
                                '<td class="text-center"><b>' + val2.TotalYesterdayOutbox + '</b></td>' +
                                '<td class="text-center"><b>' + val2.TotalYesterdayNothijato + '</b></td> ' +
                                '<td class="text-center"><b>' + val2.TotalYesterdayNothiVukto + '</b></td> ' +
                                '<td class="text-center"><b>' + val2.TotalNisponnoDak + '</b></td>' +
                                '<td class="text-center"><b>' + val2.TotalOnisponnoDak + '</b></td>' +
                                '<td class="text-center"><b>' + val2.TotalYesterdaySrijitoNote + '</b></td>' +
                                '<td class="text-center"><b>' + val2.TotalYesterdayDakSohoNote + '</b></td>' +
                                '<td class="text-center"><b>' + val2.TotalYesterdayPotrojari + '</b></td>' +
                                '<td class="text-center"><b>' + val2.TotalNisponnoNote + '</b></td>' +
                                '<td class="text-center"><b>' + val2.TotalOnisponnoNote + '</b></td>' +
                                '</tr>';
                    }

                    if (typeof (val2) === 'object') {
                        $.each(val2, function (index3, val3) {
                            if (val3.Offices) {
                                dashboard += '<tr class=" origin' + index2 + '" style="display: none;" id="org' + index3 + '">' +
                                        '<td><div><div class="pull-left" style="margin-left:20px!important;"><b>';
                                if (val3.TotalOffices != '০') {
                                    dashboard += '<a onclick="hide_office(' + index3 + ')" ><i class="fa fa-plus-square-o" aria-hidden="true"></i></a>&nbsp;&nbsp;';
                                }
                                dashboard += val3.Name + '</b></div></div></td>' +
                                        '<td class="text-center"><b>' + val3.TotalOffices + '</b></td><td class="text-center"><span class="badge badge-info" style="font-size:12pt!important;"  data-toggle="tooltip" title="কার্যকর অফিস সংখ্যা ">' + val3.Offices + '</span></td> ' +
                                        '<td class="text-center"> <b>' + val3.TotalEmployee + '</b></td><td class="text-center"><span class="badge badge-primary" style="font-size:12pt!important;" data-toggle="tooltip" title="আজকের লগইন ব্যক্তিবর্গ " >' + val3.TotalLogin + '</span></td>' +
                                        '<td class="text-center"><b>' + val3.TotalYesterdayInbox + '</b></td>' +
                                        '<td class="text-center"><b>' + val3.TotalYesterdayOutbox + '</b></td>' +
                                        '<td class="text-center"><b>' + val3.TotalYesterdayNothijato + '</b></td> ' +
                                        '<td class="text-center"><b>' + val3.TotalYesterdayNothiVukto + '</b></td> ' +
                                        '<td class="text-center"><b>' + val3.TotalNisponnoDak + '</b></td>' +
                                        '<td class="text-center"><b>' + val3.TotalOnisponnoDak + '</b></td>' +
                                        '<td class="text-center"><b>' + val3.TotalYesterdaySrijitoNote + '</b></td>' +
                                        '<td class="text-center"><b>' + val3.TotalYesterdayDakSohoNote + '</b></td>' +
                                        '<td class="text-center"><b>' + val3.TotalYesterdayPotrojari + '</b></td>' +
                                        '<td class="text-center"><b>' + val3.TotalNisponnoNote + '</b></td>' +
                                        '<td class="text-center"><b>' + val3.TotalOnisponnoNote + '</b></td>' +
                                        '</tr>';
                            }
                            if (typeof (val3) === 'object') {
                                $.each(val3, function (index4, val4) {
                                    if (typeof (val4.Name) != 'undefined') {
                                        if (val4.Status == '1') {
                                            dashboard += '<tr class=" office' + index3 + '" style="display: none;" >' +
                                                    '<td colspan="3"><a style="margin-left:40px!important;" href="<?= $this->request->webroot ?>officeDashboard/' + index4 + '" target="_blank"><b>' + val4.Name + '</b></a></td>';

                                        } else {
                                            dashboard += '<tr class="danger ofc'+index4+'  office' + index3 + '" style="display: none;" >' +
                                                    '<td colspan="3"><span style="margin-left:40px!important;" ><b>' + val4.Name + '</b></span></td>';
                                        }
                                        office_ids.push(index4);
                                        dashboard += '<td class="text-center"> <b>' + val4.TotalEmployee + '</b></td><td class="text-center"><span class="badge badge-primary" style="font-size:12pt!important;" data-toggle="tooltip" title="আজকের লগইন ব্যক্তিবর্গ " >' + val4.TotalLogin + '</span></td>' +
                                                '<td class="text-center"><b>' + val4.TotalYesterdayInbox + '</b></td>' +
                                                '<td class="text-center"><b>' + val4.TotalYesterdayOutbox + '</b></td>' +
                                                '<td class="text-center"><b>' + val4.TotalYesterdayNothijato + '</b></td> ' +
                                                '<td class="text-center"><b>' + val4.TotalYesterdayNothiVukto + '</b></td> ' +
                                                '<td class="text-center"><b>' + val4.TotalNisponnoDak + '</b></td>' +
                                                '<td class="text-center"><b>' + val4.TotalOnisponnoDak + '</b></td>' +
                                                '<td class="text-center"><b>' + val4.TotalYesterdaySrijitoNote + '</b></td>' +
                                                '<td class="text-center"><b>' + val4.TotalYesterdayDakSohoNote + '</b></td>' +
                                                '<td class="text-center"><b>' + val4.TotalYesterdayPotrojari + '</b></td>' +
                                                '<td class="text-center"><b>' + val4.TotalNisponnoNote + '</b></td>' +
                                                '<td class="text-center"><b>' + val4.TotalOnisponnoNote + '</b></td>' +
                                                '</tr>';
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
        dashboard += '';
        $('#AddData').append(dashboard);
        $('[data-toggle="tooltip"]').tooltip({'placement':'bottom'});
    }

    function print_total() {
//     $('#details').floatThead('destroy');
        dashboard = '<tr class="info total" id="total" >' +
                '<td><b>মোট</b>';

        dashboard += '</td>' +
                '<td class="text-center"><b>' + BntoEng(TotalOffices) + '</b></td><td class="text-center"><span class="badge badge-success" style="font-size:12pt!important;" data-toggle="tooltip" title="কার্যকর অফিস সংখ্যা ">' + BntoEng(Offices) + '</span></td> ' +
                '<td class="text-center"> <b>' + BntoEng(TotalEmployee) + '</b></td><td class="text-center"><span class="badge badge-primary" style="font-size:12pt!important;" data-toggle="tooltip" title="বর্তমানে লগইন ব্যক্তিবর্গ " >' + BntoEng(TotalLogin) + '</span></td>' +
                '<td class="text-center"><b>' + BntoEng(TotalYesterdayInbox) + '</b></td>' +
                '<td class="text-center"><b>' + BntoEng(TotalYesterdayOutbox) + '</b></td>' +
                '<td class="text-center"><b>' + BntoEng(TotalYesterdayNothijato) + '</b></td> ' +
                '<td class="text-center"><b>' + BntoEng(TotalYesterdayNothiVukto) + '</b></td> ' +
                '<td class="text-center"><b>' + BntoEng(TotalNisponnoDak) + '</b></td>' +
                '<td class="text-center"><b>' + BntoEng(TotalOnisponnoDak) + '</b></td>' +
                '<td class="text-center"><b>' + BntoEng(TotalYesterdaySrijitoNote) + '</b></td>' +
                '<td class="text-center"><b>' + BntoEng(TotalYesterdayDakSohoNote) + '</b></td>' +
                '<td class="text-center"><b>' + BntoEng(TotalYesterdayPotrojari) + '</b></td>' +
                '<td class="text-center"><b>' + BntoEng(TotalNisponnoNote) + '</b></td>' +
                '<td class="text-center"><b>' + BntoEng(TotalOnisponnoNote) + '</b></td>' +
                '</tr>';
        $('#AddData').append(dashboard);
        $('#waitMessage').html('');
//        compare_offices();
//         $('#details').floatThead({
//            position: 'absolute'
//        });
    }

    function BntoEng(input) {
        var numbers = {
            0: '০',
            1: '১',
            2: '২',
            3: '৩',
            4: '৪',
            5: '৫',
            6: '৬',
            7: '৭',
            8: '৮',
            9: '৯'
        };

        var output = '';
        if (parseInt(input) != 0) {
            while (parseInt(input) != 0) {
                var n = parseInt(input % 10);
                if (!Number.isInteger(n))
                    break;
                input = input / 10;
                output += (numbers[n]);
            }
        } else
        {
            output = '০';
        }
        for (var i = output.length - 1, o = ''; i >= 0; i--)
        {
            o += output[i];
        }
        return o;

    }

    function compare_offices() {
        $.ajax({
            type: 'POST',
            url: "<?php
echo $this->Url->build(['controller' => 'Dashboard',
    'action' => 'getOfficesId'])
?>",
            success: function (data) {
                var fake_offices = [];
                var i = 0;
                var j = 0;
                for (j = 0; j < data.length; j++) {
                    var f = 0;
                    for (i = 0; i < office_ids.length; i++) {
                        if (data[j] == office_ids[i]) {
                            f = 1;
                            break;
                        }
                    }
                    if (f == 0)
                    {
                        fake_offices.push(data[j]);
                    }
                }
            }
        });
    }
    function hide_layer(name) {

        $('.layer' + name).each(function (i, v) {
            var str = $(v).attr('id');
            if ($('i', '#' + str).hasClass("fa-minus-square-o")) {
                $("i", '#' + str).toggleClass("fa-minus-square-o fa-plus-square-o");
            }
            if ($('.origin' + str.substring(3, str.length)).is(":visible")) {
                $('.origin' + str.substring(3, str.length)).hide();
                var minus = $('.origin' + str.substring(3, str.length)).attr('id');
                if ($("i", '#' + minus).hasClass("fa-minus-square-o")) {
                    $("i", '#' + minus).toggleClass("fa-minus-square-o fa-plus-square-o");
                }
            }
            $('.origin' + str.substring(3, str.length)).each(function (ind, val) {
                var str1 = $(val).attr('id');
                if ($('.office' + str1.substring(3, str1.length)).is(":visible")) {
                    $('.office' + str1.substring(3, str1.length)).hide();
                }
            });

        });
        $('.layer' + name).slideToggle();
        $("i", '#mnstr' + name).toggleClass("fa-minus-square-o fa-plus-square-o");
    }

    function hide_origin(name) {
        $('.origin' + name).each(function (i, v) {
            var str = $(v).attr('id');
            if ($('i', '#' + str).hasClass("fa-minus-square-o")) {
                $("i", '#' + str).toggleClass("fa-minus-square-o fa-plus-square-o");
            }
            if ($('.office' + str.substring(3, str.length)).is(":visible")) {
                $('.office' + str.substring(3, str.length)).hide();
            }
        });
        $('.origin' + name).slideToggle();
        $("i", '#lyr' + name).toggleClass("fa-minus-square-o fa-plus-square-o");
    }

    function hide_office(name) {
        $('.office' + name).slideToggle();
        $("i", '#org' + name).toggleClass("fa-minus-square-o fa-plus-square-o");
    }

</script>