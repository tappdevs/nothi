<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-book"></i>নথির নতুন আপডেট/রিলিজ নোট তালিকা
        </div>
        <div class="actions">
            <a href="<?php
            echo $this->Url->build(['controller' => 'dashboard',
                'action' => 'nothiUpdate'])
            ?>" class="btn   green" ><i class="fa fa-plus-circle fa-2x"></i>&nbsp;<?= __('Add New') ?> আপডেট/রিলিজ নোট</a>
        </div>
    </div>
    <div class="portlet-body">
        <div class="">
            <div class="well text-center">
                <b> "রিলিজ নোট পেজে দেখা যাবে" </b> অপশনে যে বিবরণগুলো চিহ্নিত করা থাকবে, শুধুমাত্র সেসব বিবরণগুলো রিলিজ নোট পেজে দেখা যাবে। প্রত্যেকটি বিবরণ রিলিজ ডেট ও ভার্শনের উপর ভিত্তি করে রিলিজ নোট পেজে ক্রম পাবে (সর্বশেষ রিলিজ ডেট ও ভার্শন সবচেয়ে উপরে স্থান পাবে) ।
            </div>
            <div class="table-scrollable" id="tbl">
                <table class="table table-bordered text-center">
                    <thead>
                        <tr>
                            <th> ক্রম </th>
                            <th> বিবরণ </th>
                            <th> রিলিজ ডেট </th>
                            <th> ভার্শন</th>
                            <th>কার্যক্রম </th>
                            <th>রিলিজ নোট পেজে দেখা যাবে </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (!empty($data)) {
                            $i = 0;
                            foreach ($data as $dt) {
                                ?>
                                <tr>
                                    <td><?= enTobn( ++$i); ?></td>
                                    <td><button type="button" class="btn   btn-sm btn-primary" onclick="showData(<?= $dt['id'] ?>)"><?= __('Details') ?> দেখতে ক্লিক করুন</button>
                                        <?php //echo h((strlen($dt['body']) > 100) ? mb_substr($dt['body'], 0, 100).' ...': $dt['body']);
                                        ?></td>
                                    <td><?= Cake\I18n\Time::parse($dt['release_date'])->format('Y-m-d') ?></td>
                                    <td><?= entobn($dt['version']) ?></td>
                                    <td>
                                        <button type="button" class="btn   btn-sm btn-warning" onclick="editData('<?= $dt['id'] ?>', '<?= base64_encode($dt['id']) ?>')"><?= __('Edit') ?></button>
                                        <button type="button" class="btn   btn-sm btn-danger" onclick="deleteData('<?= $dt['id'] ?>', '<?= base64_encode($dt['id']) ?>')"><?= __('Delete') ?></button>

                                    </td>
                                    <td>
                                        <div class="form form-group">
                                            <input type="checkbox" id="check<?= $dt['id'] ?>" onclick="display_me('<?= $dt['id'] ?>')" class="form-control" <?=
                                            (($dt['is_display'] == 1) ? 'checked' : '')
                                            ?>>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                            }
                        }
                        else {
                            ?>
                            <tr><td colspan="6"> এখনো কোন তথ্য <?= __('Submit') ?> করা হয়নি। </td></tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>

    </div>
</div>
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" >নথির নতুন আপডেট/রিলিজ নোট বিস্তারিত</h4>
            </div>
            <div class="modal-body"  style="height: 400px;;overflow: auto">
                <div class="row">
                    <div class="col-md-6 col-sm-6 bold" id="md-version">
                        
                    </div>
                    <div class="col-md-6 col-sm-6 text-right bold" id="md-release">
                        
                    </div>
                </div>
                <hr>
                <div class="row" id="md-body">
                    
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= __("Close") ?></button>
            </div>
        </div>

    </div>
</div>
<!--<div id="yourModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

         Modal content
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" >নথির নতুন আপডেট <?= __('Edit') ?></h4>
            </div>
            <div class="modal-body" id="md-body-edit" style="height: 400px;overflow: auto">
                <textarea id="mytextarea"></textarea>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>-->
<script src = "<?php echo CDN_PATH; ?>js/tinymce/tinymce.min.js" type = "text/javascript" ></script>
<script>
function showData(id) {
    Metronic.blockUI({target: '#tbl', boxed: true});
    $.ajax({
        type: 'POST',
        url: "<?php
echo $this->Url->build(['controller' => 'Dashboard',
'action' => 'nothiUpdateList'])
?>",
        data: {"id": id},
        cache: true,
        success: function (data) {
            Metronic.unblockUI('#tbl');
            $("#md-version").html("ভার্শনঃ  "+data.version);
            $("#md-release").html("রিলিজ ডেটঃ "+ data.release_date);
            $("#md-body").html(data.body);
            $('#myModal').modal('show');
        },
        error: function (data) {
            toastr.error(data);
            Metronic.unblockUI('#tbl');
        }

    });

}
function editData(id, en_id) {
    window.location.href = "<?= $this->request->webroot ?>nothiUpdate/" + id + "/" + en_id;
}
function deleteData(id, en_id) {
    Metronic.blockUI({target: '#tbl', boxed: true});
    bootbox.dialog({
        message: "আপনি কি বিবরণটি মুছে ফেলতে ইচ্ছুক?",
        title: "মুছে ফেলুন",
        buttons: {
            success: {
                label: "হ্যাঁ",
                className: "green",
                callback: function () {
                    $.ajax({
                        type: 'GET',
                        url: "<?php
echo $this->Url->build(['controller' => 'Dashboard',
'action' => 'nothiUpdateEdit'])
?>",
                        data: {"id": id, 'en_id': en_id},
                        success: function (data) {
                            Metronic.unblockUI('#tbl');
                            if (data.status == 'success') {
                                toastr.success(data.msg);
                                window.location.href = "<?= $this->request->webroot ?>nothiUpdateList";
                            } else {
                                toastr.error(data.msg);
                            }

                        },
                        error: function (data) {
                            toastr.error(data);
                            Metronic.unblockUI('#tbl');
                        }

                    });
                }
            },
            danger: {
                label: "না",
                className: "red",
                callback: function () {
                     Metronic.unblockUI('#tbl');
                }
            }
        }
    });


}
function display_me(id) {
    var ok = 0;
    if ($("#check" + id).prop('checked')) {
       ok = 1;
    }
    Metronic.blockUI({target: '#tbl', boxed: true});
    bootbox.dialog({
        message: "আপনি কি বিবরণটি রিলিজ নোট পেজে দেখাতে "+ ((ok == 1) ? "ইচ্ছুক?" : "অনিচ্ছুক?"),
        title: "বিবরণটি রিলিজ নোট পেজে দেখানো",
        buttons: {
            success: {
                label: "হ্যাঁ",
                className: "green",
                callback: function () {
                    $.ajax({
                        type: 'POST',
                        url: "<?php
echo $this->Url->build(['controller' => 'Dashboard',
'action' => 'nothiUpdateEdit'])
?>",
                        data: {"id": id,'display': ok},
                        success: function (data) {
                            Metronic.unblockUI('#tbl');
                            if (data.status == 'success') {
                                toastr.success(data.msg);
                                window.location.href = "<?= $this->request->webroot ?>nothiUpdateList";
                            } else {
                                toastr.error(data.msg);
                            }

                        },
                        error: function (data) {
                            toastr.error(data);
                            Metronic.unblockUI('#tbl');
                        }

                    });
                }
            },
            danger: {
                label: "না",
                className: "red",
                callback: function () {
                    Metronic.unblockUI('#tbl');
                }
            }
        }
    });


}
</script>