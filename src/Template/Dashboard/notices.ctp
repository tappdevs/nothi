<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-info-circle fa-2x"></i>নথি অ্যাপ্লিকেশনের নোটিশসমূহ
        </div>
        <a type="button" class="btn btn-info pull-right" href="<?= $this->request->webroot ?>login">লগইন পেইজ</a>
    </div>
    <div class="portlet-body">
        <div class="panel-group accordion" id="accordion1">
            <?php
            if (!empty($data)){
            $len = count($data);
                for ($i = 0;$i < $len; $i++){
                ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <?php
                            if ($i == 0) {
                                ?>
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1"
                                   href="#collapse_<?= $i ?>">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6">
                                            <b><?= entobn($data[$i]['title']) ?></b>
                                        </div>
                                        <div class="col-md-6 col-sm-6 text-right">
                                            <b>তারিখ: <?= entobn(Cake\I18n\Time::parse($data[$i]['created'])->format('d-m-Y')) ?></b>
                                        </div>
                                    </div>
                                </a>
                                <?php
                            } else {
                                ?>
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1"
                                   href="#collapse_<?= $i ?>">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6">
                                            <b><?= entobn($data[$i]['title']) ?></b>
                                        </div>
                                        <div class="col-md-6 col-sm-6 text-right">
                                            <b>তারিখ: <?= entobn(Cake\I18n\Time::parse($data[$i]['created'])->format('d-m-y')) ?></b>
                                        </div>
                                    </div>
                                </a>
                            <?php } ?>
                        </h4>
                    </div>
                    <?php if ($i == 0){ ?>
                        <div id="collapse_<?= $i ?>" class="panel-collapse collapse in">
                    <?php } else { ?>
                        <div id="collapse_<?= $i ?>" class="panel-collapse collapse">
                    <?php } ?>

                        <div class="panel-body" style="font-size : 12pt!important;">
                            <?= $data[$i]['description'] ?>
                            <?php
                            $attachments = json_decode($data[$i]['attachments'], true);
                            if (count($attachments) > 0) {
                                echo "<hr/>";
                                foreach ($attachments as $attachment) {
                                    $file = explode('content', $attachment['href']);
                                    ?><a href="<?=$this->request->webroot.'getContent/?file='.end($file).'&token='.sGenerateToken(['file'=>end($file)],['exp'=>time() + 60*300])?>"><?=$attachment['name']?></a><?php
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <?php
                }
            } else {
                echo " খুব শীঘ্রয় তথ্য অন্তর্ভুক্ত করা হবে। ";
            }
            ?>
            </div>
        </div>
    </div>