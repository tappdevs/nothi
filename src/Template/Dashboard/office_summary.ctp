<div class="portlet box purple" >
    <div class="portlet-title">
        <div class="caption">
            <i class="fs0 a2i_gn_dashboard2"></i>অফিস ড্যাশবোর্ড
        </div>

    </div>
    <div class="portlet-body">
        <div class="form-group">
            <table class="table table-bordered" style="font-size:12pt!important;">
                <tr class="text-center">
                    <td colspan="3" class="text-center"><?php echo $officeinfo['office_name_bng']."<br/>".$officeinfo['office_address'] ?></td>
                </tr>
                <tr class="text-center">
                    <th colspan="3" class="font-md text-center">
                        <a target="_blank" href="<?= $this->request->webroot ?>loginDetails/<?= $officeinfo['id'] ?>">  ব্যবহারকারী  তথ্য  দেখুন </a>
                    </th>
                </tr>
            </table>
        </div>
        <div class="table-scrollable">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th class="text-center">
                            বিবরণ
                        </th>
                        <th class="text-center">
                            আজকের
                        </th>
                        <th class="text-center">
                            গতকালের
                        </th>
                        <th class="text-center">
                            অদ্যাবধি
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr style="font-size:12pt!important;" class="bg-grey">

                        <td>
                            ডাক গ্রহণ
                        </td>
                        <td class="text-center">
                            <?php echo $this->Number->format($result['totaltodayinbox']); ?>
                        </td>
                        <td class="text-center">
                            <?php echo $this->Number->format($result['totalyesterdayinbox']); ?>
                        </td>
                        <td class="text-center">
                            <?php echo $this->Number->format($result['totalinboxall']); ?>
                        </td>

                    </tr>


                    <tr style="font-size:12pt!important;" >

                        <td>
                            নিষ্পন্ন ডাক সংখ্যা
                        </td>
                        <td class="text-center">
                            <?php echo $this->Number->format($result['totaltodaynisponnodak']); ?>

                        </td>
                        <td class="text-center">
                            <?php echo $this->Number->format($result['totalyesterdaynisponnodak']); ?>

                        </td>
                        <td class="text-center">
                            <?php echo $this->Number->format($result['totalnisponnodakall']); ?>

                        </td>

                    </tr>
                    <tr style="font-size:12pt!important;color: black!important;" class="bg-grey-silver">
                        <td>
                            অনিষ্পন্ন ডাক সংখ্যা
                        </td>
                        <td class="text-center">

                            <?php echo $this->Number->format($result['totaltodayOnisponnodak']); ?>

                        </td>
                        <td class="text-center">

                            <?php echo $this->Number->format($result['totalyesterdayOnisponnodak']); ?>

                        </td>
                        <td class="text-center">
                            <?php echo $this->Number->format($result['totalOnisponnodakall']); ?>
                        </td>
                    </tr>

                    <tr style="font-size:12pt!important;" class="bg-grey">

                        <td>
                            স্ব- উদ্যোগে
                            নোট
                        </td>
                        <td class="text-center">
                            <?php echo $this->Number->format($result['totaltodaysrijitonote']); ?>

                        </td>
                        <td class="text-center">
                            <?php echo $this->Number->format($result['totalyesterdaysrijitonote']); ?>

                        </td>
                        <?php
                        if(!empty($result['ajax_request']) && $result['ajax_request']  == true){
                            echo '<td class="text-center" id="self_note"><i class="fa fa-spinner" aria-hidden="true"></i></td>';
                        }else{
                        ?>
                        <td class="text-center">
                            <?php echo $this->Number->format($result['totalsrijitonoteall']); ?>

                        </td>
                        <?php
                        }
                        ?>
                    </tr>
                    <tr style="font-size:12pt!important;" class="bg-grey">

                        <td>
                            ডাক থেকে সৃজিত নোট
                        </td>
                        <td class="text-center">
                            <?php echo $this->Number->format($result['totaltodaydaksohonote']); ?>
                        </td>
                        <td class="text-center">
                            <?php echo $this->Number->format($result['totalyesterdaydaksohonote']); ?>
                        </td>
                           <?php
                        if(!empty($result['ajax_request']) && $result['ajax_request']  == true){
                            echo '<td class="text-center" id="dak_note"><i class="fa fa-spinner" aria-hidden="true"></i></td>';
                        }else{
                        ?>
                        <td class="text-center">
                            <?php echo $this->Number->format($result['totaldaksohonoteall']); ?>
                        </td>
                        <?php
                        }
                        ?>
                    </tr>
                    <tr style="font-size:12pt!important;">

                        <td>
                            পত্রজারিতে নিষ্পন্ন নোট
                        </td>
                        <td class="text-center">
                            <?php echo $this->Number->format($result['totaltodaynisponnopotrojari']); ?>

                        </td>
                        <td class="text-center">
                            <?php echo $this->Number->format($result['totalyesterdaynisponnopotrojari']); ?>

                        </td>
                          <?php
                        if(!empty($result['ajax_request']) && $result['ajax_request']  == true){
                            echo '<td class="text-center" id="nisponno_potrojari"><i class="fa fa-spinner" aria-hidden="true"></i></td>';
                        }else{
                        ?>
                        <td class="text-center">
                            <?php echo $this->Number->format($result['totalnisponnopotrojariall']); ?>

                        </td>
                        <?php
                        }
                        ?>
                    </tr>
                    <tr style="font-size:12pt!important;">

                        <td>
                           নোটে নিষ্পন্ন
                        </td>
                        <td class="text-center">
                            <?php echo $this->Number->format($result['totaltodaynisponnonote']); ?>

                        </td>
                        <td class="text-center">
                            <?php echo $this->Number->format($result['totalyesterdaynisponnonote']);
                            ?>

                        </td>
                          <?php
                        if(!empty($result['ajax_request']) && $result['ajax_request']  == true){
                            echo '<td class="text-center" id="nisponno_note"><i class="fa fa-spinner" aria-hidden="true"></i></td>';
                        }else{
                        ?>
                        <td class="text-center">
                            <?php echo $this->Number->format($result['totalnisponnonoteall']);
                            ?>

                        </td>
                        <?php
                        }
                            ?>

                    </tr>
                    <tr style="font-size:12pt!important;color: black!important;" class="bg-grey-silver">
                        <td>
                            অনিষ্পন্ন নোট সংখ্যা
                        </td>
                        <td class="text-center">

                            <?php echo $this->Number->format($result['totaltodayOnisponnonote']); ?>

                        </td>
                        <td class="text-center">

                            <?php echo $this->Number->format($result['totalyesterdayOnisponnonote']); ?>

                        </td>
                          <?php
                        if(!empty($result['ajax_request']) && $result['ajax_request']  == true){
                            echo '<td class="text-center" id="onisponno_note"><i class="fa fa-spinner" aria-hidden="true"></i></td>';
                        }else{
                        ?>
                        <td class="text-center">
                            <?php echo $this->Number->format($result['totalOnisponnonoteall']); ?>
                        </td>
                    </tr>
                    <tr style="font-size:12pt!important;">

                        <td>
                            পত্রজারি
                        </td>
                        <td class="text-center">
                            <?php echo $this->Number->format($result['totaltodaypotrojari']); ?>

                        </td>
                        <td class="text-center">
                            <?php echo $this->Number->format($result['totalyesterdaypotrojari']); ?>

                        </td>
                        <td class="text-center">
                            <?php echo $this->Number->format($result['totalpotrojariall']); ?>

                        </td>

                    </tr>
                    <?php
                        }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip({'placement':'bottom'});
    });
</script>