
<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-edit"></i>অফিস এডমিন বাছাই করুন
        </div>
    </div>
    <div class="portlet-body">
        <div class="row">
            <div class="col-md-12">
                <table class="table table-striped table-bordered table-hover" id="sample_1">
                    <thead>
                    <tr>

                        <th class=""></th>
                        <th class="">ক্রমিক নং </th>
                        <th class=""><?php echo __('Name') ?></th>
                        <th class="">পদবি</th>
                        <th class="">শাখা</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                    <tfoot>
                    <tr>
                        <td class="form-actions" colspan="5"> <input class="btn btn-md btn-primary" type="button" id="assignOfficeAdmin" value="<?php echo __("Assign") ?>" /></td>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
$(document).ready(function() {
    $('#sample_1').find('tbody').html('');
    PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + 'EmployeeRecords/emoloyeeDesignationByOffice',
        {'office_id': <?= $office_id ?>, group:'EmployeeOffices.office_unit_organogram_id'}, 'json',
        function (response) {
            $.each(response, function(i, data){
                $('#sample_1').find('tbody').append("<tr><td>"+ '<input ' +(data.is_admin==1?"checked=checked":'') + ' type="radio" name="office_admin" data-user-id="'+data.employee_office_id+'" class="office_admin" />' + "</td><td>"+BnFromEng(i+1)+"</td><td>"+data.name_bng+"</td><td>"+data.designation+"</td><td>"+data.unit_name_bng+"</td></tr>");
            });
        }
    );
});

$(document).off('click','#assignOfficeAdmin');
$(document).on('click','#assignOfficeAdmin',function(){
    var done = 0;
    if($('.office_admin').length>0){
        Metronic.blockUI({
            target: '.portlet.box.green',
            boxed: true
        });
        var requested_user_id = $('.office_admin:checked').attr('data-user-id');
        $.ajax({
            url: '<?php echo $this->Url->build(['controller'=>'OfficeManagement','action'=>'AssignOfficeAdmin']) ?>',
            data: {'requested_user_id':requested_user_id, 'office_id':<?= $office_id ?>},
            cache: false,
            type: 'post',
            success: function(msg){
                Metronic.unblockUI('.portlet.box.green');
                toastr.success(msg);
                window.location.href = js_wb_root;
            }
        });
    }
});
</script>