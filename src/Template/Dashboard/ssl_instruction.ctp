<?php
$i = 1;
?>
<style>
    img{
        max-height: 1000px;
        max-width: 1000px;
    }

</style>
<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-question-circle"></i>  ডিজিটাল সার্টিফিকেট  প্রযুক্তি ব্যবহার করতে আপনার করনীয়
        </div>
    </div>
    <div class="portlet-body">
        <div class="panel-group accordion" id="accordion1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapse_<?= $i ?>">
                            <b> Google Chrome এর ক্ষেত্রে করনীয় </b>
                        </a>

                    </h4>
                </div>
                <div id="collapse_<?= $i ?>" class="panel-collapse collapse in">
                    <div class="panel-body" style="font-size : 12pt!important;">
                        <div class="row text-center">
                            <br>
                            www.nothi.gov.bd-তে ডিজিটাল সার্টিফিকেট  প্রযুক্তি ব্যবহার করার কারনে  <a href="https://www.google.com/chrome/" style="decoration: none">Google Chrome</a> -এ  নিচে প্রদর্শিত চিত্রের ন্যায় সতর্কবার্তা (warning message) পাবেন।
                            <br>
                            <h4 class="text-center bold"><u> Google Chrome এর পুরোনো ভার্সনে </u></h4>
                            <br>
                            <img class="text-center" src="<?= $this->request->webroot.'img/ssl_images/image016.jpg' ?>">
                            <br><br>
                            ওয়েবসাইটে  প্রবেশ করতে <b>“proceed anyway”</b> লিখার উপর ক্লিক করুণ  তাহলে আপনি ওয়েবসাইটে  প্রবেশ করবেন ।
                            <br><br>
                            <h4 class="text-center bold"><u> Google Chrome এর নতুন ভার্সনে </u></h4>
                            <br>
                            <span class="text-center bold"> ধাপ-১ </span>
                            <br>
                            <img class="text-center" src="<?= $this->request->webroot.'img/ssl_images/image031.jpg' ?>">
                            <br><br>
                            <b>“ADVANCED” লিখার উপর ক্লিক করুণ</b>
                            <br>
                            <span class="text-center bold"> ধাপ-২ </span>
                            <br>
                            <img class="text-center" src="<?= $this->request->webroot.'img/ssl_images/image032.jpg' ?>">
                            <br><br>
                            ওয়েবসাইটে  প্রবেশ করতে <b>“Proceed to www.nothi.gov.bd (unsafe)”</b> লিখার উপর ক্লিক করুণ  তাহলে আপনি পরবর্তী চিত্রের ন্যায় ওয়েবসাইটে  প্রবেশ করবেন ।
                            <br><br>
                            <span class="text-center bold"> ধাপ-৩ </span>
                            <br>

                            এখান থেকে আপনার পরবর্তী প্রয়োজনীয় কার্যাবলী সম্পাদন করতে পারবেন ।
                            <br>
                            <img class="text-center" src="<?= $this->request->webroot.'img/ssl_images/image004.jpg' ?>">
                        </div>

                    </div>
                </div>
            </div>
            <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#collapse_<?= ++$i ?>">
                        <b> Mozilla Firefox এর ক্ষেত্রে করনীয় </b>
                    </a>
                </h4>
            </div>
            <div id="collapse_<?= $i ?>" class="panel-collapse collapse">
                <div class="panel-body" style="font-size : 12pt!important;">
                    <br>
                    <div class="text-center ">
                        <br>
                        www.nothi.gov.bd-তে ডিজিটাল সার্টিফিকেট  প্রযুক্তি ব্যবহার করার কারনে   <a href="https://www.mozilla.org/en-US/firefox/new/" style="decoration: none">Mozilla Firefox</a> -এ  নিচে প্রদর্শিত চিত্রের ন্যায় সতর্কবার্তা (warning message) পাবেন।
                        <br>
                        <h4 class="text-center bold"><u>  Mozilla Firefox এর পুরোনো ভার্সনে </u></h4>
                        <br>
                        <span class="text-center bold"> ধাপ-১ </span>
                        <br>
                        <img class="text-center" src="<?= $this->request->webroot.'img/ssl_images/image006.jpg' ?>">
                        <br><br>
                        ওয়েবসাইটে  প্রবেশ করতে<b>“I Understand the Risk”</b> লিখার উপর ক্লিক করুণ ।তাহলে আপনি পরবর্তী চিত্রের ন্যায় আরেকটি অপশন পাবেন।
                        <br>
                        <span class="text-center bold"> ধাপ-২ </span>
                        <br>
                        <img class="text-center" src="<?= $this->request->webroot.'img/ssl_images/image032.jpg' ?>">
                        <br><br>
                        এখানে “Add Exception”বাটনে ক্লিক করতে হবে।
                        <br><br>
                        <span class="text-center bold"> ধাপ-৩ </span>
                        <br>
                        “Get Certificate ” বাটনে ক্লিক  করুণ ।
                        <br>
                        <img class="text-center" src="<?= $this->request->webroot.'img/ssl_images/image010.jpg' ?>">
                        <br><br>
                        <br><br>
                        <span class="text-center bold"> ধাপ-৪ </span>
                        <br>
                        <b>“Confirm Security Exception”</b> বাটনে ক্লিক করুণ করুণ  তাহলে আপনি পরবর্তী চিত্রের ন্যায় ওয়েবসাইটে  প্রবেশ করবেন ।
                        <br>
                        <img class="text-center" src="<?= $this->request->webroot.'img/ssl_images/image012.jpg' ?>">
                        <br><br>
                        <br><br>
                        <span class="text-center bold"> ধাপ-৫ </span>
                        <br>
                       এখান থেকে আপনার পরবর্তী প্রয়োজনীয় কার্যাবলী সম্পাদন করতে পারবেন ।
                        <br>
                        <img class="text-center" src="<?= $this->request->webroot.'img/ssl_images/image004.jpg' ?>">
                        <br><br>
                        <h4 class="text-center bold"><u> Mozilla Firefox এর নতুন ভার্সনে </u></h4>
                        <br>
                        <span class="text-center bold"> ধাপ-১ </span>
                        <br>
                        <img class="text-center" src="<?= $this->request->webroot.'img/ssl_images/image033.jpg' ?>">
                        <br><br>
                        ওয়েবসাইটে  প্রবেশ করতে<b>“ADVANCED”</b> লিখার উপর ক্লিক করুণ ।তাহলে আপনি পরবর্তী চিত্রের ন্যায় আরেকটি অপশন পাবেন।
                        <br>
                        <span class="text-center bold"> ধাপ-২ </span>
                        <br>
                        <img class="text-center" src="<?= $this->request->webroot.'img/ssl_images/image034.jpg' ?>">
                        <br><br>
                        এখানে <b>“Add Exception”</b>বাটনে ক্লিক করতে হবে।
                        <br><br>
                        <span class="text-center bold"> ধাপ-৩ </span>
                        <br>

                        <b>“Get Certificate ”</b> বাটনে ক্লিক  করুণ ।
                        <br>
                        <img class="text-center" src="<?= $this->request->webroot.'img/ssl_images/image035.jpg' ?>">
                         <span class="text-center bold"> ধাপ-৪ </span>
                        <br>
                        <b>“Confirm Security Exception”</b> বাটনে ক্লিক করুণ করুণ  তাহলে আপনি পরবর্তী চিত্রের ন্যায় ওয়েবসাইটে  প্রবেশ করবেন ।
                        <br>
                        <img class="text-center" src="<?= $this->request->webroot.'img/ssl_images/image012.jpg' ?>">
                        <br><br>
                        <br><br>
                        <span class="text-center bold"> ধাপ-৫ </span>
                        <br>
                       এখান থেকে আপনার পরবর্তী প্রয়োজনীয় কার্যাবলী সম্পাদন করতে পারবেন ।
                        <br>
                        <img class="text-center" src="<?= $this->request->webroot.'img/ssl_images/image036.jpg' ?>">
                        <br><br>
                    </div>
                </div>
            </div>
        </div>
             <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapse_<?= ++$i ?>">
                            <b> Opera এর ক্ষেত্রে করনীয় </b>
                        </a>

                    </h4>
                </div>
                <div id="collapse_<?= $i ?>" class="panel-collapse collapse">
                    <div class="panel-body" style="font-size : 12pt!important;">
                        <div class="row text-center">
                            <br>
                            www.nothi.gov.bd-তে ডিজিটাল সার্টিফিকেট  প্রযুক্তি ব্যবহার করার কারনে   <a href="http://www.opera.com/download" style="decoration: none">Opera</a>  -এ  নিচে প্রদর্শিত চিত্রের ন্যায় সতর্কবার্তা (warning message) পাবেন।
                            <br>

                            <span class="text-center bold"> ধাপ-১ </span>
                            <br>
                              ওয়েবসাইটে  প্রবেশ করতে <b>“Continue Anyway”</b> বাটনে ক্লিক করুণ  তাহলে আপনি পরবর্তী চিত্রের ন্যায় ওয়েবসাইটে  প্রবেশ করবেন ।
                              <br>
                            <img class="text-center" src="<?= $this->request->webroot.'img/ssl_images/image020.jpg' ?>">
                            <br><br>
                            <span class="text-center bold"> ধাপ-২ </span>
                            <br>
                            এখান থেকে আপনার পরবর্তী প্রয়োজনীয় কার্যাবলী সম্পাদন করতে পারবেন ।
                            <br>
                            <img class="text-center" src="<?= $this->request->webroot.'img/ssl_images/image004.jpg' ?>">
                        </div>

                    </div>
                </div>
            </div>
             <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapse_<?= ++$i ?>">
                            <b> Internet Explorer এর ক্ষেত্রে করনীয় </b>
                        </a>

                    </h4>
                </div>
                <div id="collapse_<?= $i ?>" class="panel-collapse collapse">
                    <div class="panel-body" style="font-size : 12pt!important;">
                        <div class="row text-center">
                            <br>
                            www.nothi.gov.bd-তে ডিজিটাল সার্টিফিকেট  প্রযুক্তি ব্যবহার করার কারনে  Internet Explorer  -এ  নিচে প্রদর্শিত চিত্রের ন্যায় সতর্কবার্তা (warning message) পাবেন।
                            <br>

                            <span class="text-center bold"> ধাপ-১ </span>
                            <br>
                              ওয়েবসাইটে  প্রবেশ করতে <b>“Continue to this website (not recommended)”</b> বাটনে ক্লিক করুণ  তাহলে আপনি পরবর্তী চিত্রের ন্যায় ওয়েবসাইটে  প্রবেশ করবেন ।
                              <br>
                            <img class="text-center" src="<?= $this->request->webroot.'img/ssl_images/image002.jpg' ?>">
                            <br><br>
                            <span class="text-center bold"> ধাপ-২ </span>
                            <br>
                            এখান থেকে আপনার পরবর্তী প্রয়োজনীয় কার্যাবলী সম্পাদন করতে পারবেন ।
                            <br>
                            <img class="text-center" src="<?= $this->request->webroot.'img/ssl_images/image004.jpg' ?>">
                        </div>

                    </div>
                </div>
            </div>
        </div>
        
    </div>
</div>
