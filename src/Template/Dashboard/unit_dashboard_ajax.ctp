

<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <?= $office_name_bng['unit_name_bng'] ?>
        </div>
        <div class="actions">
            <?php if ($auth == 1)  ?>
            <a href="<?php echo $this->Url->build(['controller' => 'dashboard', 'action' => 'detailDashboard']) ?>" class="btn btn-primary" >ড্যাশবোর্ড</a>
        </div>
    </div>

    <div class="portlet-body">
        <div class="row">
            <?php
            if (!empty($data)) {
                foreach ($data as $key => $value) {
                    ?>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="portlet box purple">

                            <div class="portlet-title">
                                <div class="caption">
                                    <?= $value['name'] ?>
                                </div>
                                <div class="tools">
                                    <a href="javascript:;" class="collapse" data-original-title="" title=""></a>
                                    <a target="_blank" class="login_history" title=" কার্যক্রম " href="<?= $this->request->webroot ?>loginDetails/<?= $value['office_id'] ?>/<?= $value['unit_id'] ?>/<?= $key ?>" style="color:#fff;text-decoration: none;">
                                        <label class="badge badge-danger" style="font-size: 12pt!important;">  কার্যক্রম  </label></a>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="">

                                    <table class="table table-bordered table-hover">


                                        <tr class="heading">

                                            <th class="text-center">
                                                চলমান কার্যক্রম
                                            </th>
                                            <th class="text-center">
                                                আজকের
                                            </th>
                                            <th class="text-center">
                                                গতকালের
                                            </th>
                                            <th class="text-center">
                                                অদ্যাবধি
                                            </th>

                                        </tr>
                                        <tr class="bg-grey">

                                                    <td>
                                                        ডাক গ্রহণ
                                                    </td>
                                                    <td class="text-center">
                                                        <?php echo $this->Number->format($value['totaltodayinbox']); ?>
                                                    </td>
                                                    <td class="text-center">
                                                        <?php echo $this->Number->format($value['totalyesterdayinbox']); ?>
                                                    </td>
                                                    <td class="text-center">
                                                        <?php echo $this->Number->format($value['totalinboxall']); ?>
                                                    </td>

                                                </tr>

                                                <tr>

                                                    <td>
                                                        নিষ্পন্ন ডাক সংখ্যা
                                                    </td>
                                                    <td class="text-center">
                                                        <?php echo $this->Number->format($value['totaltodaynisponnodak']); ?>

                                                    </td>
                                                    <td class="text-center">
                                                        <?php echo $this->Number->format($value['totalyesterdaynisponnodak']); ?>

                                                    </td>
                                                    <td class="text-center">
                                                        <?php echo $this->Number->format($value['totalnisponnodakall']); ?>

                                                    </td>
                                                </tr>
                                                <tr style="color:black!important;" class="bg-grey-silver">
                                                    <td>
                                                        অনিষ্পন্ন ডাক সংখ্যা
                                                    </td>
                                                    <td class="text-center">

                                                        <?php echo $this->Number->format($value['totaltodayOnisponnodak']); ?>

                                                    </td>
                                                    <td class="text-center">

                                                        <?php echo $this->Number->format($value['totalyesterdayOnisponnodak']); ?>

                                                    </td>
                                                    <td class="text-center">
                                                        <?php echo $this->Number->format($value['totalOnisponnodakall']); ?>
                                                    </td>
                                                </tr>
                                                <tr>

                                                    <td>
                                                   পত্রজারিতে নিষ্পন্ন নোট
                                                    </td>
                                                    <td class="text-center">
                                                        <?= $this->Number->format($value['totaltodaynisponnopotrojari']) ?>

                                                    </td>
                                                    <td class="text-center">
                                                        <?= $this->Number->format($value['totalyesterdaynisponnopotrojari']) ?>

                                                    </td>
                                                    <td class="text-center">
                                                        <?= $this->Number->format( $value['totalnisponnopotrojariall']) ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                   নোটে নিষ্পন্ন
                                                    </td>
                                                    <td class="text-center">
                                                        <?= $this->Number->format($value['totaltodaynisponnonote'] ) ?>

                                                    </td>
                                                    <td class="text-center">
                                                        <?= $this->Number->format($value['totalyesterdaynisponnonote']) ?>

                                                    </td>
                                                    <td class="text-center">
                                                        <?= $this->Number->format($value['totalnisponnonoteall']) ?>
                                                    </td>
                                                </tr>
                                                <tr style="color:black!important;" class="bg-grey-silver">

                                                    <td>
                                                        মোট অনিষ্পন্ন
                                                    </td>
                                                    <td class="text-center">

                                                        <?php echo $this->Number->format($value['totaltodayOnisponnonote']); ?>

                                                    </td>
                                                    <td class="text-center">

                                                        <?php echo $this->Number->format($value['totalyesterdayOnisponnonote']); ?>

                                                    </td>
                                                    <td class="text-center">
                                                        <?php echo $this->Number->format($value['totalOnisponnonoteall']); ?>
                                                    </td>
                                                </tr>
                                                 <tr>

                                                    <td>
                                                        পত্রজারি
                                                    </td>
                                                    <td class="text-center">
                                                        <?php echo $this->Number->format($value['totaltodaypotrojari']); ?>

                                                    </td>
                                                    <td class="text-center">
                                                        <?php echo $this->Number->format($value['totalyesterdaypotrojari']); ?>

                                                    </td>
                                                    <td class="text-center">
                                                        <?php echo $this->Number->format($value['totalpotrojariall']); ?>

                                                    </td>

                                                </tr>

                                    </table>
                                    <table class="table table-bordered table-hover">


                                        <tr class="heading">
                                            <th class="text-center">অপেক্ষমাণ কার্যক্রম</th>
                                            <th class="text-center">৫+ দিন</th>
                                            <th class="text-center">১০+ দিন</th>
                                            <th class="text-center">১৫+ দিন</th>
                                            <th class="text-center">৩০+ দিন</th>
                                        </tr>

                                        <tr>
                                            <td style='font-size: 11pt;text-align: center; '> ডাক </td>
                                            <td class="text-center"><?= $value['dak']['d5'] ?></td>
                                            <td class="text-center"><?= $value['dak']['d10'] ?></td>
                                            <td class="text-center"><?= $value['dak']['d15'] ?></td>
                                            <td class="text-center"><?= $value['dak']['d30'] ?></td>
                                        </tr>
                                        <tr>
                                            <td style='font-size: 11pt;text-align: center;'> নথি </td>
                                            <td class="text-center"><?= $value['nothi']['d5'] ?></td>
                                            <td class="text-center"><?= $value['nothi']['d10'] ?></td>
                                            <td class="text-center"><?= $value['nothi']['d15'] ?></td>
                                            <td class="text-center"><?= $value['nothi']['d30'] ?></td>
                                        </tr>

                                    </table>

                                </div>

                            </div>
                        </div>
                    </div>
                    <?php
                }
            }
            ?>
        </div>
    </div>
</div>
<script>

    //$(function(){
    $('body').addClass('page-quick-sidebar-over-content page-full-width');
    //});
</script>
