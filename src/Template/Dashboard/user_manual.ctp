<style>
	.descriptionText {
		font-size: 13pt !important;
	}
	.tag {
		font-size: 70%;

		color: #000;
		background: #fffae3;
		border: solid 1px #f8f3dc;
		border-radius: 38px !important;
		cursor: pointer;
		text-decoration: none;
		transition: 0.5s;
		padding:5px;
	}
	.tag:hover {
		text-decoration: none;
		background: #f3eed8;
		/*color: #fff;*/
	}

	.quick-list a{
		color: #000;
		background: #fffae3;
		border: solid 1px #f8f3dc;
		border-radius: 38px !important;
		padding:5px 8px;
		cursor: pointer;
		text-decoration: none;
		transition: 0.5s;
	}
	.quick-list a:hover{
		text-decoration: none;
		background: #f3eed8;
		/*color: #fff;*/
	}

	.quick-list-title a{
		color: #000;
		background: #d9edf7;
		border: solid 1px #d9edf7;
		border-radius: 5px !important;
		padding:2px 8px;
		cursor: pointer;
		text-decoration: none;
		transition: 0.5s;
	}
	.quick-list-title a:hover{
		text-decoration: none;
		background: #acc0c9;
		border: solid 1px #acc0c9;
		color: #fff;
	}

	.panel-custom {
		border-color: #979797;
	}
	.panel-custom > .panel-heading {
		color: #fff;
		background-color: #a9a9a9;
		border-color: #979797;
	}
	.panel-custom > .panel-heading + .panel-collapse > .panel-body {
		border-top-color: #979797;
	}
	.panel-custom > .panel-heading .badge {
		color: #a9a9a9;
		background-color: #31708f;
	}
	.panel-custom > .panel-footer + .panel-collapse > .panel-body {
		border-bottom-color: #979797;
	}

	.descriptionDiv span {
		display: -webkit-box;
		line-height: 1.3;
		-webkit-line-clamp: 3;
		-webkit-box-orient: vertical;
		overflow: hidden;
		text-overflow: ellipsis;
		max-height: 69px;
	}
	mark {
		padding: 0!important;
		background: yellow !important;
	}

</style>
<div class="portlet box purple">
    <div class="portlet-title">
	    <div class="pull-right">
		    <a href="<?=$this->request->webroot?>userManual" style="position: absolute;right: 14px;top: 10px;text-decoration: none;color: white;"><i class="fa fa-reply"></i></a>
	    </div>
        <div class="caption">
	        <?= __('User Manual') ?> <small style="opacity:0.5">ভার্সন ২.০</small>
        </div>
    </div>
    <div class="portlet-body">
        <div class="row">
	        <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
				<?php echo $this->Form->create('', ['type' => 'get']); ?>
		        <div class="input-group">
			        <input type="text" name="search" class="form-control" value="<?php if (isset($search_text)) echo $search_text ?>" placeholder="খুঁজুন..." style="height:38px;">
			        <span class="input-group-btn">
				        <button class="btn green" type="submit"><i class="fa fa-search"></i> খুঁজুন</button>
			        </span>
		        </div>
		        <?php echo $this->Form->end(); ?>

		        <hr/>

		        <?php foreach($userManual as $key => $manual): ?>
		        <div class="panel panel-default description">
			        <div class="panel-heading">
				        <?php if($details): ?>
				        <div class="pull-right" title="">
					        <i data-id="<?=$manual['id']?>" class="descriptionPlay fa fa-volume-up" style="cursor: pointer"></i>
				        </div>
						<?php endif; ?>
				        <h4 class="panel-title">
					        <div style="margin-bottom: 10px;">
								<span class="quick-list-title">
									<a href="?module=<?=$manual['module']?>"><?=$manual['module']?></a>
								</span>
						        <a href="?manual=<?=$manual['id']?>"><?=$manual['title']?></a>
					        </div>
					        <div class="tags">
						        <?php $tags = explode('|', $manual['tags']); ?>
								<?php foreach ($tags as $k => $tag):?>
						        <a href="?tag=<?=$tag?>" class="tag"><?=$tag?></a>
						        <?php endforeach; ?>
					        </div>
				        </h4>
			        </div>
					<?php if($details) { $ellipsis = ''; } else { $ellipsis = 'descriptionDiv'; } ?>
			        <div class="panel-body descriptionText <?=$ellipsis?>">
						<span><?=$manual['description']?></span>
						<?php if(!$details) { ?><span><a href="?manual=<?=$manual['id']?>">বিস্তারিত...</a></span><?php  } ?>
			        </div>
		        </div>
		        <?php endforeach; ?>
				<?php if(!$details): ?>
		        <?=customPagination($this->Paginator)?>
				<?php endif; ?>

	        </div>
	        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
		        <div class="panel panel-custom">
			        <div class="panel-heading">
				        <h4 class="panel-title">
					        মডিউল
				        </h4>
			        </div>
			        <div class="panel-body">
				        <div class="quick-list-title">
					        <a href="?module=নথি">নথি</a>
					        <a href="?module=ডাক">ডাক</a>
					        <a href="?module=দপ্তর">দপ্তর</a>
					        <a href="?module=ড্যাশবোর্ড">ড্যাশবোর্ড</a>
					        <a href="?module=রিপোর্ট">রিপোর্ট</a>
				        </div>
			        </div>
		        </div>

		        <div class="panel panel-custom">
			        <div class="panel-heading">
				        <h4 class="panel-title">
					        ট্যাগসমূহ
				        </h4>
			        </div>
			        <div class="panel-body">
				        <div class="quick-list">
					        <?php foreach ($tags as $tag):?>
					        <a href="?tag=<?=$tag?>"><?=$tag?></a>
							<?php endforeach;?>
				        </div>
			        </div>
		        </div>

		        <div class="panel panel-custom">
			        <div class="panel-heading">
				        <h4 class="panel-title">
					        সাম্প্রতিক
				        </h4>
			        </div>
			        <div class="panel-body">
				        <ul>
					        <?php foreach($recent as $key => $value): ?>
					        <li><a href="?manual=<?=$key?>"><?=$value?></a></li>
							<?php endforeach; ?>
				        </ul>
			        </div>
		        </div>

		        <?php if (isset($related)): ?>
		        <div class="panel panel-custom">
			        <div class="panel-heading">
				        <h4 class="panel-title">
					        সম্পর্কিত
				        </h4>
			        </div>
			        <div class="panel-body">
				        <ul>
							<?php foreach($related as $key => $value): ?>
						        <li><a href="?manual=<?=$key?>"><?=$value?></a></li>
							<?php endforeach; ?>
				        </ul>
			        </div>
		        </div>
		        <?php endif; ?>
	        </div>
        </div>
    </div>
</div>

<div id="playlist">
	<!-- This will only display the player, and enable the audio on the document-->
	<audio id="playlist_audio" autoplay="autoplay" type="audio/mpeg"></audio>
</div>

<script>
	var files = [];
    $('.descriptionPlay').on('click', function() {
        element = this;
        if ($(element).hasClass('fa-stop')) {
            $(element).removeClass('fa-stop').addClass('fa-volume-up');
            var audio_player = document.getElementById("playlist_audio");
            audio_player.pause();
        } else {
            $(element).removeClass('fa-volume-up').addClass('fa-spinner fa-pulse');
            var text = $(element).closest('.description').find('.descriptionText').text();
            playNow(text, element);
        }
    });

    function playNow(text, element) {
        var dataId = $(element).data('id');
        $.ajax({
            'url': js_wb_root+'userManual/playRequest',
            'method': 'post',
            'data': {id:dataId, text:text},
            'type': 'json',
            'success': function(response) {
                files = JSON.parse(response);
                playTextToSpeech(element);
            }
        })
    }

    function playTextToSpeech(element) {
        $(element).removeClass('fa-spinner fa-pulse').addClass('fa-stop');
        // Current index of the files array
        var current = 0;

        // Get the audio element
        var playlistPlayer = document.querySelector("#playlist audio");

        // function that will be call at the end of the previous
        function next() {
            // Check for last media in the playlist
            current++;

            // Change the audio element source
            if (current < files.length) {
                playlistPlayer.src = files[current];
            } else {
                $(element).removeClass('fa-stop').addClass('fa-volume-up');
            }
        }

        // Check if the player is in the DOM
        if (playlistPlayer === null) {
            throw "Playlist Player does not exists ...";
        } else {
            if (current < files.length) {
                // Start the player
                playlistPlayer.src = files[current];

                // Listen for the playback ended event, to play the next media
                playlistPlayer.addEventListener('ended', next, false);
                current = 0;
            }
        }
    }
</script>
<?php if (isset($search_text)): ?>
<script>
    $(document).ready(function() {
        $(".descriptionText p").each(function(k,v) {
            var src_str = $(this).text();
            var term = "<?=$search_text?>";
            term = term.replace(/(\s+)/,"(<[^>]+>)*$1(<[^>]+>)*");
            var pattern = new RegExp("("+term+")", "gi");

            src_str = src_str.replace(pattern, "<mark>$1</mark>");
            src_str = src_str.replace(/(<mark>[^<>]*)((<[^>]+>)+)([^<>]*<\/mark>)/,"$1</mark>$2<mark>$4");

            $(this).html(src_str);
        });
    })
</script>
<?php endif; ?>