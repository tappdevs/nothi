<div class="form-horizontal">
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __('Cadrer') ?> <?php echo __('Name English') ?></label>

        <div class="col-sm-3">
            <?php echo $this->Form->input('cadre_name_eng', array('label' => false, 'class' => 'form-control')); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __('Cadrer') ?> <?php echo __('Name Bangla') ?></label>

        <div class="col-sm-3">
            <?php echo $this->Form->input('cadre_name_bng', array('label' => false, 'class' => 'form-control')); ?>
        </div>
    </div>
</div>