<?php
$employeeCadres = array();
foreach ($employee_cadres as $cadre):
    $employeeCadres[$cadre->id] = $cadre['cadre_name_bng'];
endforeach;

$employeeRanks = array();
foreach ($employee_ranks as $ranks):
    $employeeRanks[$ranks->id] = $ranks['rank_name_bng'];
endforeach;

?>
<div class="form-horizontal">
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __('Grader') ?> <?php echo __('Name Bangla') ?></label>

        <div class="col-sm-3">
            <?php echo $this->Form->input('grade_name_bng', array('label' => false, 'type' => 'text', 'class' => 'form-control', 'placeholder' => 'Grade Name in Bangla')); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __('Grader') ?> <?php echo __('Name English') ?></label>

        <div class="col-sm-3">
            <?php echo $this->Form->input('grade_name_eng', array('label' => false, 'type' => 'text', 'class' => 'form-control', 'placeholder' => 'Grade Name in English')); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __('Cadre') ?></label>

        <div class="col-sm-3">
            <?php echo $this->Form->input('employee_cadre_id', array('label' => false, 'type' => 'select', 'class' => 'form-control', 'options' => $employeeCadres)); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __('Rank') ?></label>

        <div class="col-sm-3">
            <?php echo $this->Form->input('employee_rank_id', array('label' => false, 'type' => 'select', 'class' => 'form-control', 'options' => $employeeRanks)); ?>
        </div>
    </div>
</div>