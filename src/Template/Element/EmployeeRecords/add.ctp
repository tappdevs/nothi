<?php
$employeeRecords = array();
foreach ($employee_records as $employee):
    $employeeRecords[$employee->id] = $employee['name_eng'];
endforeach;

$officeRecords = array();
foreach ($office_records as $office):
    $officeRecords[$office->id] = $office['rank_name_eng'];
endforeach;

$officeOrganograms = array();
foreach ($office_organograms as $organogram):
    $officeOrganograms[$organogram->id] = $organogram['designation_eng'];
endforeach;

$employeeRoles = array();
foreach ($roles as $role):
    $employeeRoles[$role->id] = $role['name'];
endforeach;

?>
<div class="form-horizontal">
    <div class="form-group">
        <label class="col-sm-4 control-label">Employee</label>

        <div class="col-sm-3">
            <?php echo $this->Form->input('employee_id', array('label' => false, 'type' => 'select', 'class' => 'form-control', 'options' => $employeeRecords)); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label">Office Record</label>

        <div class="col-sm-3">
            <?php echo $this->Form->input('office_record_id', array('label' => false, 'type' => 'select', 'class' => 'form-control', 'options' => $officeRecords)); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label">Organogram</label>

        <div class="col-sm-3">
            <?php echo $this->Form->input('office_organogram_id', array('label' => false, 'type' => 'select', 'class' => 'form-control', 'options' => $officeOrganograms)); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label">Role</label>

        <div class="col-sm-3">
            <?php echo $this->Form->input('role_id', array('label' => false, 'type' => 'select', 'class' => 'form-control', 'options' => $employeeRoles)); ?>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-4 control-label">Joining Date</label>

        <div class="col-sm-3">
            <?php echo $this->Form->input('joining_date', array('label' => false, 'type' => 'text', 'class' => 'form-control', 'placeholder' => 'Joining date')); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label">Release Date</label>

        <div class="col-sm-3">
            <?php echo $this->Form->input('release_date', array('label' => false, 'type' => 'text', 'class' => 'form-control', 'placeholder' => 'Release date')); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label">Role Type</label>

        <div class="col-sm-3">
            <?php echo $this->Form->input('role_type', array('label' => false, 'type' => 'text', 'class' => 'form-control', 'placeholder' => 'Role Type')); ?>
        </div>
    </div>
</div>