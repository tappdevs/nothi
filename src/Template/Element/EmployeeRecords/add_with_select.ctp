<?php
$employeeRecords = array();
foreach ($employee_records as $employee):
    $employeeRecords[$employee->id] = $employee['name_eng'];
endforeach;

$officeRecords = array();
foreach ($office_records as $office):
    $officeRecords[$office->id] = $office['rank_name_eng'];
endforeach;

$officeOrganograms = array();
foreach ($office_organograms as $organogram):
    $officeOrganograms[$organogram->id] = $organogram['designation_eng'];
endforeach;

$employeeRoles = array();
foreach ($roles as $role):
    $employeeRoles[$role->id] = $role['name'];
endforeach;





?>

<div class="row">
    <div class="col-md-5">
        <label class="control-label col-md-4"><?php echo __('Employee') ?></label>
        <?php echo $this->Form->input('employee_id', array('label' => false, 'type' => 'select', 'class' => 'form-control input-medium', 'options' => $employeeRecords)); ?>
    </div>
    <div class="col-md-5">
        <label class="control-label col-md-4"><?php echo __('Office') ?> <?php echo __('Record') ?></label>
        <?php echo $this->Form->input('office_record_id', array('label' => false, 'type' => 'select', 'class' => 'form-control  input-medium', 'options' => $officeRecords)); ?>
    </div>

    <div class="col-md-5">
        <label class="control-label col-md-4"><?php echo __('Organogram') ?></label>
        <?php echo $this->Form->input('office_organogram_id', array('label' => false, 'type' => 'select', 'class' => 'form-control input-medium', 'options' => $officeOrganograms)); ?>
    </div>
    <div class="col-md-5">
        <label class="control-label col-md-4"><?php echo __('Role') ?></label>
        <?php echo $this->Form->input('role_id', array('label' => false, 'type' => 'select', 'class' => 'form-control  input-medium', 'options' => $employeeRoles)); ?>
    </div>

</div><br>

<hr><br>


<div class="form-horizontal">
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __('Joining Date') ?></label>

        <div class="col-sm-3">
            <?php echo $this->Form->input('joining_date', array('label' => false, 'type' => 'text', 'class' => 'form-control', 'placeholder' => 'Joining Date')); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __('Release Date') ?></label>

        <div class="col-sm-3">
            <?php echo $this->Form->input('release_date', array('label' => false, 'type' => 'text', 'class' => 'form-control', 'placeholder' => 'Release Date')); ?>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __('Role') ?> <?php echo __('Type') ?> </label>

        <div class="col-sm-3">
            <?php echo $this->Form->input('role_type', array('label' => false, 'type' => 'text', 'class' => 'form-control', 'placeholder' => 'Role Type')); ?>
        </div>
    </div>

</div>