<h3 class="form-section">দাপ্তরিক তথ্য </h3>
<?php echo $cell = $this->cell('OfficeSelection'); ?>
<div class="row">
    <div class="col-md-4 form-group form-horizontal">
        <label class="control-label">অফিস</label>
        <?php echo $this->Form->input('current_office_id', array('id' => 'office-id', 'label' => false, 'class' => 'form-control', 'placeholder' => 'অফিস')); ?>
    </div>
    <div class="col-md-4 form-group form-horizontal">
        <label class="control-label"> সংশ্লিষ্ট পদে যোগদানের তারিখ </label>
        <?php echo $this->Form->input('current_office_unit_id', array('id' => 'office-unit-id', 'label' => false, 'class' => 'form-control', 'placeholder' => 'অফিস')); ?>
    </div>
    <div class="col-md-4 form-group form-horizontal">
        <label class="control-label"> সংশ্লিষ্ট পদে যোগদানের তারিখ </label>
        <?php echo $this->Form->input('joining_date', array('label' => false, 'class' => 'form-control', 'placeholder' => ' সংশ্লিষ্ট পদে যোগদানের তারিখ ')); ?>
    </div>
</div>
<div class="row">
    <div class="col-md-6 form-group form-horizontal">
        <label class="control-label">অত্যাবশ্যকীয় কার্যাবলি</label>
        <?php echo $this->Form->input('role_id', array('empty' => __('Select'), 'type' => 'select', 'label' => false, 'class' => 'form-control', 'placeholder' => 'অত্যাবশ্যকীয় কার্যাবলি')); ?>
    </div>
    <div class="col-md-6 form-group form-horizontal">
        <label class="control-label">পদবি</label>
        <?php echo $this->Form->input('current_office_designation_id', array('id' => 'office-organogram-id', 'label' => false, 'class' => 'form-control', 'placeholder' => 'পদবি', 'options' => array())); ?>
    </div>
</div>



