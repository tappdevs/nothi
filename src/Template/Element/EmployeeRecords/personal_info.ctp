<h3 class="form-section">ব্যক্তিগত তথ্য</h3>
<div class="row">
    <div class="col-md-6 form-group form-horizontal">
        <label class="control-label">নাম <span class="text-danger">*</span></label>
        <?= $this->Form->input('name_bng', ['label' => false, 'class' => 'form-control', 'type' => 'text', 'placeholder' => 'নাম(বাংলা)','required'=>'required','autocomplete' => 'new-password']); ?>
    </div>
    <div class="col-md-6 form-group form-horizontal">
        <label class="control-label">&nbsp;</label>
        <?= $this->Form->input('name_eng', ['label' => false, 'class' => 'form-control', 'type' => 'text', 'placeholder' => 'নাম(ইংরেজি)','autocomplete' => 'new-password']); ?>
    </div>
</div>
<div class="row">
    <div class="col-md-6 form-group form-horizontal">
        <label class="control-label">পিতার নাম </label>
        <?php echo $this->Form->input('father_name_bng', array('label' => false, 'class' => 'form-control', 'placeholder' => 'পিতার নাম(বাংলা)','autocomplete' => 'new-password')); ?>
    </div>

    <div class="col-md-6 form-group form-horizontal">
        <label class="control-label">&nbsp;</label>
        <?php echo $this->Form->input('father_name_eng', array('label' => false, 'class' => 'form-control', 'placeholder' => 'পিতার নাম(ইংরেজি)','autocomplete' => 'new-password')); ?>
    </div>
</div>
<div class="row">
    <div class="col-md-6 form-group form-horizontal">
        <label class="control-label">মাতার নাম </label>
        <?php echo $this->Form->input('mother_name_bng', array('label' => false, 'class' => 'form-control', 'placeholder' => 'মাতার নাম(বাংলা)','autocomplete' => 'new-password')); ?>
    </div>

    <div class="col-md-6 form-group form-horizontal">
        <label class="control-label">&nbsp;</label>
        <?php echo $this->Form->input('mother_name_eng', array('label' => false, 'class' => 'form-control', 'placeholder' => 'মাতার নাম(ইংরেজি)','autocomplete' => 'new-password')); ?>
    </div>
</div>
<div class="row">
    <div class="col-md-3 form-group form-horizontal">
        <label class="control-label">জন্ম তারিখ <span class="text-danger">*</span></label>
        <?php echo $this->Form->input('date_of_birth', array('label' => false, 'class' => 'form-control date-picker', 'type'=>"text", 'placeholder' => 'জন্ম তারিখ','default'=>date("Y-m-d"))); ?>
    </div>
    <div class="col-md-3 form-group form-horizontal">
        <label class="control-label">জাতীয় পরিচয়পত্র নম্বর <span class="text-danger">*</span></label>
        <?php echo $this->Form->input('nid', array('label' => false, 'type' => 'text', 'class' => 'form-control', 'placeholder' => 'জাতীয় পরিচয়পত্র নম্বর','required'=>'required','autocomplete' => 'new-password')); ?>
           <span class="help-block font-red"><b>*</b> জাতীয় পরিচয়পত্র নম্বর ১০ অথবা ১৭ সংখ্যার  হতে হবে।</span>
    </div>
    <div class="col-md-3 form-group form-horizontal">
        <label class="control-label">জন্ম সনদ নম্বর</label>
        <?php echo $this->Form->input('bcn', array('label' => false, 'class' => 'form-control', 'placeholder' => 'জন্ম সনদ নম্বর','autocomplete' => 'new-password')); ?>
    </div>
    <div class="col-md-3 form-group form-horizontal">
        <label class="control-label">পাসপোর্ট নম্বর</label>
        <?php echo $this->Form->input('ppn', array('label' => false, 'class' => 'form-control', 'placeholder' => 'পাসপোর্ট নম্বর','autocomplete' => 'new-password')); ?>
    </div>
</div>
<div class="row">
    <div class="col-md-3 form-group form-horizontal">
        <label class="control-label">লিঙ্গ </label>
        <?php
        $options = array(
            "1" => "Male",
            "2" => "Female",
            "3" => "Others"
        );
        echo $this->Form->input('gender', array('empty' => __('Select'), 'type' => 'select', 'label' => false, 'class' => 'form-control', 'placeholder' => 'লিঙ্গ', 'options' => $options,'default'=>1));
        ?>
    </div>
    <div class="col-md-3 form-group form-horizontal">
        <label class="control-label">ধর্ম </label>
        <?php
        $options = array(
            "Islam" => "Islam",
            "Hindu" => "Hindu",
            "Christian" => "Christian",
            "Buddhist" => "Buddhist",
            "Others" => "Others"
        );
        echo $this->Form->input('religion', array('empty' => __('Select'), 'type' => 'select', 'label' => false, 'class' => 'form-control', 'placeholder' => 'ধর্ম', 'options' => $options,'default'=>1));
        ?>
    </div>
    <div class="col-md-3 form-group form-horizontal">
        <label class="control-label">রক্তের গ্রুপ</label>
        <?php
        $options = array(
            "A+" => "A+",
            "A-" => "A-",
            "B+" => "B+",
            "B-" => "B-",
            "O+" => "O+",
            "O-" => "O-",
            "AB+" => "AB+",
            "AB-" => "AB-"
        );
        echo $this->Form->input('blood_group', array('empty' => __('Select'), 'type' => 'select', 'label' => false, 'class' => 'form-control', 'placeholder' => 'রক্তের গ্রুপ', 'options' => $options));
        ?>
    </div>
    <div class="col-md-3 form-group form-horizontal">
        <label class="control-label">বৈবাহিক অবস্থা</label>
        <?php
        $options = array(
            "Single" => "Single",
            "Married" => "Married",
            "Widowed" => "Widowed",
            "Separated" => "Separated"
        );
        echo $this->Form->input('marital_status', array('empty' => __('Select'), 'type' => 'select', 'label' => false, 'class' => 'form-control', 'placeholder' => 'বৈবাহিক অবস্থা', 'options' => $options));
        ?>
    </div>
</div>

<div class="row">
    <div class="col-md-3 form-group form-horizontal">
        <label class="control-label"> ব্যক্তিগত ই-মেইল</label>
        <?php echo $this->Form->input('personal_email', array('label' => false, 'class' => 'form-control', 'placeholder' => 'ই-মেইল','autocomplete' => 'new-password')); ?>
    </div>
    <div class="col-md-3 form-group form-horizontal">
        <label class="control-label"> ব্যক্তিগত মোবাইল নম্বর <span class="text-danger">*</span></label>
        <?php echo $this->Form->input('personal_mobile', array('label' => false, 'class' => 'form-control', 'placeholder' => 'মোবাইল','required'=>'required','autocomplete' => 'new-password')); ?>
    </div>
    <div class="col-md-6 form-group form-horizontal">
        <label class="control-label">বিকল্প মোবাইল নম্বর </label>
        <?php echo $this->Form->input('alternative_mobile', array('label' => false, 'class' => 'form-control', 'placeholder' => 'বিকল্প মোবাইল ','autocomplete' => 'new-password')); ?>
    </div>
</div>

