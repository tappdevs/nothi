<?php
//$class = 'message';
//if (!empty($params['class'])) {
//    $class .= ' ' . $params['class'];
//}
?>
<!--<div class="<?= h($class) ?>"><?= h($message) ?></div>-->
<link rel="stylesheet" type="text/css"
      href="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-toastr/toastr.min.css"/>

<div class="alert alert-info message success">
    <button class="close" data-close="alert"></button>
    <?php if(!is_array($message))echo h($message) ?>
</div>

<script>
    $('.message').hide();
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "positionClass": "toast-bottom-right",
    };
    toastr.info('<?php echo $message ?>');
</script>
