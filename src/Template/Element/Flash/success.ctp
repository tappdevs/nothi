<link rel="stylesheet" type="text/css"
      href="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-toastr/toastr.min.css"/>

<div class="alert alert-warning message success">
    <button class="close" data-close="alert"></button>
    <?php if(!is_array($message))echo h($message) ?>
</div>

<script>
    $('.message').hide();
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "positionClass": "toast-bottom-right",
    };
    toastr.success('<?php echo h($message) ?>');
</script>