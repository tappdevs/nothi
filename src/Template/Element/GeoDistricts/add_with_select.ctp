<?php
$geoDivisions = array();
foreach ($geo_divisions as $divisions):
    $geoDivisions[$divisions->id] = $divisions['division_name_eng'];
endforeach;
?>

<div class="form-horizontal">
    <div class="form-group">
        <label class="col-sm-4 control-label">&nbsp;</label>

        <div class="col-sm-3">
            <?php // echo $this->Form->input('geo_division_id', array('label' => false, 'type'=>'select', 'class' =>'form-control', 'options'=>$geoDivisions)); ?>
            <?php echo $this->cell('GeoSelectionDivision',
                ['params' => $geoDivisions])
            ?>
            <?php
            if (!empty($errors['geo_division_id'])) {
                  echo "<div class='font-red'>".$errors['geo_division_id']['_empty']."</div>";
}
            ?>
        </div>
    </div>
</div>
<hr><br>

<div class="form-horizontal">
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __("District Name in Bangla"); ?></label>

        <div class="col-sm-3">
            <?php echo $this->Form->input('district_name_bng',
                array('label' => false, 'class' => 'form-control'));
            ?>
            <?php
            if (!empty($errors['district_name_bng'])) {
                echo "<div class='font-red'>".$errors['district_name_bng']['_empty']."</div>";
            }
            ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __("District Name in English"); ?></label>

        <div class="col-sm-3">
                        <?php echo $this->Form->input('district_name_eng',
                array('label' => false, 'class' => 'form-control'));
            ?>
            <?php
            if (!empty($errors['district_name_eng'])) {
                          echo "<div class='font-red'>".$errors['district_name_eng']['_empty']."</div>";
}
            ?>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __("BBS Code"); ?></label>

        <div class="col-sm-3">
                        <?php echo $this->Form->input('bbs_code',
                array('label' => false, 'class' => 'form-control'));
            ?>
            <?php
            if (!empty($errors['bbs_code'])) {
                 echo "<div class='font-red'>".$errors['bbs_code']['_empty']."</div>";
}
            ?>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __("Status"); ?></label>

        <div class="col-sm-3">
                        <?php echo $this->Form->checkbox('status',
                array('label' => false, 'checked' => true));
            ?>
            <?php
            if (!empty($errors['status'])) {
                            echo "<div class='font-red'>".$errors['status']['_empty']."</div>";
}
            ?>
        </div>
    </div>
</div>