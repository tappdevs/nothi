<div class="form-horizontal">
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __('Upazila Name in Bangla') ?></label>

        <div class="col-sm-3">
            <?php echo $this->Form->input('upazila_name_bng',
                array('label' => false, 'class' => 'form-control', 'placeholder' => 'উপজেলার নাম (বাংলা)')); ?>
            <?php
            if (!empty($errors['upazila_name_bng'])) {
                echo "<div class='font-red'>".$errors['upazila_name_bng']['_empty']."</div>";
            }
            ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __('Upazila Name in English') ?></label>

        <div class="col-sm-3">
            <?php echo $this->Form->input('upazila_name_eng',
                array('label' => false, 'class' => 'form-control', 'placeholder' => 'উপজেলার নাম (ইংরেজি)')); ?>
            <?php
            if (!empty($errors['upazila_name_eng'])) {
                echo "<div class='font-red'>".$errors['upazila_name_eng']['_empty']."</div>";
            }
            ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __('Division') ?></label>

        <div class="col-sm-3">
            <?php echo $this->Form->input('geo_division_id',
                array('label' => false, 'type' => 'select', 'class' => 'form-control','options' => $geo_divisions)); ?>
            <?php
            if (!empty($errors['geo_division_id'])) {
                echo "<div class='font-red'>".$errors['geo_division_id']['_empty']."</div>";
            }
            ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __('District') ?></label>

        <div class="col-sm-3">
            <?php echo $this->Form->input('geo_district_id',
                array('label' => false, 'type' => 'select', 'class' => 'form-control','options' => $geo_districts)); ?>
            <?php
            if (!empty($errors['geo_district_id'])) {
                echo "<div class='font-red'>".$errors['geo_district_id']['_empty']."</div>";
            }
            ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __('UBBS Code') ?></label>

        <div class="col-sm-3">
            <?php echo $this->Form->input('bbs_code',
                array('label' => false, 'class' => 'form-control')); ?>
            <?php
            if (!empty($errors['bbs_code'])) {
                echo "<div class='font-red'>".$errors['bbs_code']['_empty']."</div>";
            }
            ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __('BBS Code') ?></label>

        <div class="col-sm-3">
            <?php echo $this->Form->input('district_bbs_code',
                array('label' => false, 'class' => 'form-control')); ?>
            <?php
            if (!empty($errors['district_bbs_code'])) {
                echo "<div class='font-red'>".$errors['district_bbs_code']['_empty']."</div>";
            }
            ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __('Status') ?></label>

        <div class="col-sm-3">
            <?php echo $this->Form->input('status',
                array('label' => false, 'type' => 'checkbox', 'class' => 'form-control')); ?>
            <?php
            if (!empty($errors['status'])) {
                echo "<div class='font-red'>".$errors['status']['_empty']."</div>";
            }
            ?>
        </div>
    </div>
</div>