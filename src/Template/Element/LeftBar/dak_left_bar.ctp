<?php
if (!empty($loggedUser['user_role_id']) && $loggedUser['user_role_id'] >= 2) {
?>
    <li>
	    <a href="javascript:;"><i class="fs0 efile-add2" aria-hidden="true"></i>
		    <span class="title">ডাক আপলোড</span>
		    <span class="arrow "></span></a>
	    <ul class="sub-menu">
		    <li>
			    <a href="<?= $this->request->webroot ?>dakDaptoriks/uploadDak"><i class="fa fa-building" aria-hidden="true"></i>
				    <span class="title">দাপ্তরিক ডাক</span>
			    </a>
		    </li>
		    <li>
			    <a href="<?= $this->request->webroot ?>dakNagoriks/uploadDak"><i class="fa fa-user" aria-hidden="true"></i>
				    <span class="title">নাগরিক ডাক</span>
			    </a>
		    </li>
			<li>
				<a href="javascript:;"><i class="fs0 efile-dak_draft2" aria-hidden="true"></i>
					<span class="title">আপলোডকৃত ডাক</span>
					<span class="arrow "></span></a>
				<ul class="sub-menu">
					<li>
						<a href="<?= $this->request->webroot ?>dakDaptoriks/dakInbox/draft"><i class="fa fa-building" aria-hidden="true"></i>
							<span class="title">দাপ্তরিক ডাক</span>
						</a>
					</li>
					<li>
						<a href="<?= $this->request->webroot ?>dakNagoriks/dakInbox"><i class="fa fa-user" aria-hidden="true"></i>
							<span class="title">নাগরিক ডাক</span>
						</a>
					</li>
				</ul>
			</li>
	    </ul>
    </li>

	<li>
		<a href="<?= $this->request->webroot ?>dakMovements/dakTrack"><i class="fs0 efile-search3" aria-hidden="true"></i>
			<span class="title">সকল ডাক ট্র্যাকিং</span>
		</a>
	</li>

	<li class="">
	    <a href="javascript:;"><i class="fs0 a2i_gn_register2" aria-hidden="true"></i>
		    <span class="title">নিবন্ধন বহি</span>
	        <span class="arrow"></span></a>
	    <ul class="sub-menu" style="display: none;">
	        <li class="">
	            <a href="<?= $this->request->webroot ?>dakRegisters/dakGrohonRegister"><i class="fs0 efile-grohon_register1" aria-hidden="true"></i>
		            <span class="title">ডাক গ্রহণ নিবন্ধন বহি</span>
	            </a>
	        </li>
	        <li>
	            <a href="<?= $this->request->webroot ?>dakRegisters/dakBiliRegister"><i class="fs0 efile-preron_register1" aria-hidden="true"></i>
		            <span class="title">ডাক বিলি নিবন্ধন বহি</span>
	            </a>
	        </li>
	        <li>
	            <a href="<?= $this->request->webroot ?>dakRegisters/dakDiaryRegister"><i class="fs0 efile-register1" aria-hidden="true"></i>
		            <span class="title">শাখা ডায়েরি নিবন্ধন বহি</span>
	            </a>
	        </li>
	        <li class="hidden">
	            <a href="<?= $this->request->webroot ?>dakRegisters/dakMovementRegister"><i class="fs0 efile-nothi_gotibidhi1" aria-hidden="true"></i>
		            <span class="title">ডাক গতিবিধি নিবন্ধন বহি</span>
	            </a>
	        </li>
	    </ul>
	</li>
	<li>
	    <a href="javascript:;"><i class="fs0 a2i_ld_protibedon3" aria-hidden="true"></i>
		    <span class="title">প্রতিবেদনসমূহ</span>
	        <span class="arrow "></span></a>
	    <ul class="sub-menu">
	        <li>
	            <a href="<?= $this->request->webroot ?>dakReports/pendingDakList"><i class="fs0 efile-choloman_dak5" aria-hidden="true"></i>
		            <span class="title">অমীমাংসিত ডাক তালিকা</span>
	            </a>
	        </li>
	        <li>
	            <a href="<?= $this->request->webroot ?>dakReports/resolvedDakList"><i class="fs0 efile-nishponno_dak5" aria-hidden="true"></i>
		            <span class="title">মীমাংসিত ডাক তালিকা</span>
	            </a>
	        </li>
	        <li>
	            <a href="<?= $this->request->webroot ?>dakReports/nothiVuktoDakList"><i class="fs0 efile-nothi1" aria-hidden="true"></i>
		            <span class="title">নথিতে উপস্থাপিত ডাক তালিকা</span>
	            </a>
	        </li>
	        <li>
	            <a href="<?= $this->request->webroot ?>dakReports/potrojariDakList"><i class="fs0 efile-potrojari1" aria-hidden="true"></i>
		            <span class="title">পত্রজারি ডাক তালিকা</span>
	            </a>
	        </li>
	        <li>
	            <a href="<?= $this->request->webroot ?>dakReports/nothiJaatDakList"><i class="fa fa-bar-chart" aria-hidden="true"></i>
		            <span class="title">নথিজাত ডাক তালিকা</span>
	            </a>
	        </li>
	    </ul>
	</li>
<?php
    }

    
    //Menus for For Super Admin
//    if (!empty($loggedUser['user_role_id']) && $loggedUser['user_role_id'] <= 2) {
?>
<li>
    <a href="<?= $this->request->webroot ?>honorBoards"><i class="efile-user_manage3" aria-hidden="true"></i>
        <span class="title">শাখা অনার বোর্ড</span>
    </a>
</li>
<li>
    <a href="javascript:;"><i class="fs0 efile-settings2" aria-hidden="true"></i>
	    <span class="title">সেটিংস</span>
        <span class="arrow "></span></a>
    <ul class="sub-menu">
        <li>
            <a href="<?= $this->request->webroot ?>dakActions/index"><i class="fa fa-edit" aria-hidden="true"></i>
	            <span class="title">ডাক সিদ্ধান্ত</span>
            </a>
        </li>
    </ul>
</li>
<?php if ($other_organogram_activities_permission['dak'] == 1): ?>
<li>
	<a href="<?= $this->request->webroot ?>permitted_dak"><i class="efile-email4" aria-hidden="true"></i>
		<span class="title">শেয়ারকৃত ডাকসমূহ</span>
	</a>
</li>
<?php endif; ?>
<!--    --><?php //} ?>