<li class=" ">
    <a href="javascript:;"> <i class="icon-briefcase"></i> <span class="title">কর্মকর্তা ব্যবস্থাপনা</span>
        <span class="arrow "></span></a>
    <ul class="sub-menu">
        <li>
            <a href="<?= $this->request->webroot; ?>employeeRecords/employeeManagement"> <i class="icon-briefcase"></i>
                <span class="title"> কর্মকর্তা ব্যবস্থাপনা</span>
            </a>
        </li>
    </ul>
</li>
<li class=" ">
    <a href="javascript:;"><i class="fs0 a2i_gn_settings2" aria-hidden="true"></i> <span class="title">অফিস সেটআপ</span>
        <span class="arrow "></span></a>
    <ul class="sub-menu">
        <li>
            <a href="<?= $this->request->webroot; ?>officeManagement/updateOwnOffice"><i class="fs0 a2i_gn_settings2"
                                                                                         aria-hidden="true"></i> <span
                        class="title">অফিস তথ্য সংশোধন</span>
            </a>
        </li>
        <li>
            <a href="<?= $this->request->webroot; ?>officeManagement/officeUnitManagement"><i
                        class="fs0 a2i_gn_settings2"
                        aria-hidden="true"></i>
                <span class="title"><?= ($loggedUser['user_role_id'] == 1 || $loggedUser['user_role_id']
                        == 2) ? "অফিস শাখা সংশোধন" : "অফিস শাখা" ?></span>
            </a>
        </li>
        <li>
            <a href="<?= $this->request->webroot; ?>officeManagement/officeUnitOrganogramManagement"><i
                        class="fs0 a2i_gn_settings2" aria-hidden="true"></i> <span
                        class="title"><?= ($loggedUser['user_role_id']
                        == 1 || $loggedUser['user_role_id'] == 2) ? "অফিস কাঠামো সংশোধন" : "অফিস কাঠামো" ?></span>
            </a>
        </li>
        <li>
            <a href="<?= $this->request->webroot; ?>officeEmployees/editUnitsInfo"><i class="fs0 a2i_gn_settings2"
                                                                                      aria-hidden="true"></i>
                <span class="title">শাখার নাম সংশোধন</span>
            </a>
        </li>
        <li>
            <a href="<?= $this->request->webroot; ?>officeEmployees/designationNameUpdate"><i
                        class="fs0 a2i_gn_settings2"
                        aria-hidden="true"></i>
                <span class="title">পদবির নাম সংশোধন</span>
            </a>
        </li>
        <li>
            <a href="<?= $this->Url->build(['_name' => 'designationTransfer']) ?>">
                <i class="fs0 a2i_gn_settings2" aria-hidden="true"></i> অফিস কাঠামো ট্র্যান্সফার</a>
        </li>
        <li>
            <a href="<?= $this->Url->build(['_name' => 'sectionTransfer']) ?>">
                <i class="fs0 a2i_gn_settings2" aria-hidden="true"></i> শাখা ট্র্যান্সফার</a>
        </li>
        <li>
            <a href="<?= $this->Url->build(['_name' => 'officeSectionTransfer']) ?>">
                <i class="fs0 a2i_gn_settings2" aria-hidden="true"></i> অন্যান্য অফিসে শাখা ট্র্যান্সফার</a>
        </li>
	    <li>
            <a href="<?= $this->Url->build(['_name' => 'officeSectionTransferLogs']) ?>">
                <i class="fs0 a2i_gn_settings2" aria-hidden="true"></i> অন্যান্য অফিসে শাখা ট্র্যান্সফার লগ</a>
        </li>
    </ul>
</li>
<li class=" ">
    <a href="javascript:;">
        <i class="fs0 a2i_gn_hardcopy2" aria-hidden="true"></i>
        <span class="title"> রিপোর্ট  </span>
        <span class="arrow "></span>
    </a>
    <ul class="sub-menu">
        <li>
            <a href="<?= $this->request->webroot; ?>users/loginAudit"> <i class="icon-briefcase"></i> <span
                        class="title">লগইন তথ্য</span>
            </a>
        </li>
        <li class=" ">
            <a href="javascript:;">
                <i class="fs0 a2i_gn_hardcopy2" aria-hidden="true"></i>
                <span class="title"> কার্যক্রম বিবরণী  </span>
                <span class="arrow "></span>
            </a>
            <ul class="sub-menu">
                <li>
                    <a href="<?= $this->request->webroot; ?>Performance/office">
                        <i class="fs0 a2i_gn_hardcopy2" aria-hidden="true"></i>
                        <span class="title">অফিস</span>
                    </a>
                </li>
                <li>
                    <a href="<?= $this->request->webroot; ?>Performance/unit">
                        <i class="fs0 a2i_gn_hardcopy2" aria-hidden="true"></i>
                        <span class="title">শাখা</span>
                    </a>
                </li>
                <li>
                    <a href="<?= $this->request->webroot; ?>Performance/designation">
                        <i class="fs0 a2i_gn_hardcopy2" aria-hidden="true"></i>
                        <span class="title">পদবি</span>
                    </a>
                </li>
            </ul>
        </li>
    </ul>
</li>
