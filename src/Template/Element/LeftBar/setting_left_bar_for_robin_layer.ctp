<?php
//please add new url to projapoti_static.php getRobinLayerAccess function. Otherwise normal user can access it.
?>
    <li class=" ">
        <a href="javascript:;">
            <i class="fs0 a2i_gn_home1"></i>
            <span class="title">লগইন পেইজ সেটিংস</span>
            <span class="arrow "></span>
        </a>
        <ul class="sub-menu">
            <li>
                <a href="<?= $this->request->webroot; ?>addLoginMenuNotice">
                    <i class="icon-plus"></i>
                    <span class="title">নোটিশ যুক্ত করুন</span>
                </a>
            </li>
            <li>
                <a href="<?= $this->Url->build('/monthlyReport'); ?>">
                    <i class="fs0 a2i_gn_another"></i>
                    <span class="title">মাসিক প্রতিবেদন যুক্ত করুন</span>
                </a>
            </li>
            <li>
                <a href="<?= $this->Url->build('/efileRelatedReport'); ?>">
                    <i class="fs0 a2i_gn_another"></i>
                    <span class="title">নির্দেশনাসমূহ</span>
                </a>
            </li>
        </ul>
    </li>
<li class=" ">
    <a href="javascript:;">
        <i class="fs0 a2i_gn_home1"></i>
        <span class="title">অফিস</span>
        <span class="arrow "></span>
    </a>
    <ul class="sub-menu">
        <li>
            <a href="<?= $this->request->webroot; ?>OfficeManagement/OfficeAdmin">
                <i class="icon-plus"></i>
                <span class="title">অফিস অ্যাডমিন দায়িত্ব প্রদান</span>
            </a>
        </li>
        <li>
            <a href="<?= $this->request->webroot; ?>officeEmployeeMappings/officeHeadManager">
                <i class="fs0 efile-head_of_dept1"></i>
                <span class="title">দপ্তর প্রধান নির্বাচন</span>
            </a>
        </li>
        <li class=" ">
            <a href="<?= $this->request->webroot; ?>cron/layerOffices"><i class="fa fa-building-o" aria-hidden="true"></i>
                <span class="title"> স্থর ভিত্তিক অফিস তালিকা </span>
            </a>
        </li>
        <li class=" ">
            <a href="<?= $this->request->webroot; ?>cron/officeListDistrictWise"><i class="fa fa-building-o" aria-hidden="true"></i>
                <span class="title"> জেলা ভিত্তিক অফিস তালিকা </span>
            </a>
        </li>
    </ul>
</li>
    <li class=" ">
        <a href="javascript:;"> <i class="icon-briefcase"></i> <span class="title">কর্মকর্তা / কর্মচারী ব্যবস্থাপনা</span>
            <span class="arrow "></span></a>
        <ul class="sub-menu">
            <li>
                <a href="javascript:;">
                    <i class="icon-globe"></i>
                    <span class="title">কর্মকর্তা</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a href="<?= $this->request->webroot; ?>employeeRecords/employeeManagement"><i class="icon-tag"></i>
                            <span class="title">অফিস ও কর্মকর্তা ব্যবস্থাপনা</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?= $this->request->webroot; ?>employeeRecords/employeeRecordList"><i class="icon-tag"></i>
                            <span class="title">কর্মকর্তাসমূহ</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?= $this->request->webroot; ?>employeeRecords/employeeRoleHistories"><i
                                    class="icon-tag"></i>
                            <span class="title">কর্মকর্তাবৃন্দের কর্ম ইতিহাস</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?= $this->Url->build(['controller' => 'Potrojari', 'action' => 'getDesignationListPotrojariHeader']) ?>">
                            <i class="fs0 a2i_gn_service3" aria-hidden="true"></i>
                            <span class="title">শাখার নাম দৃশ্যমান সেটিং </span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </li>
<li class=" ">
    <a href="javascript:;"> <i class="fa fa-bookmark"></i> <span class="title"> অন্যান্য </span>
        <span class="arrow "></span></a>
    <ul class="sub-menu">
        <li class=" ">
            <a href="<?= $this->request->webroot; ?>nothiUpdateList"><i class="fa fa-book" aria-hidden="true"></i>
                <span class="title"> নথির নতুন আপডেট/রিলিজ নোট তালিকা </span>
            </a>
        </li>
        <li class=" ">
            <a href="<?= $this->request->webroot; ?>cron/cronLogReport"><i class="fa fa-tachometer" aria-hidden="true"></i>
                <span class="title">ক্রন রিপোর্ট লগ </span>
            </a>
        </li>
    </ul>
</li>
<li class=" ">
    <a href="javascript:;">  <span aria-hidden="true" class="icon-magnifier-add"></span><span class="title"> ট্র্যাকিং </span>
        <span class="arrow "></span></a>
    <ul class="sub-menu">
         <li class=" ">
            <a href="<?= $this->Url->build(['_name' => 'potrojari-audit']); ?>"><i class="fa fa-tachometer" aria-hidden="true"></i>
                <span class="title"> অফিসের পত্রজারি  ট্র্যাকিং </span>
            </a>
        </li>
        <li class=" ">
            <a href="<?= $this->Url->build(['_name' => 'redundant-nid-check']); ?>"><i class="fa fa-book" aria-hidden="true"></i>
                <span class="title"> প্রয়োজনাতিরিক্ত এন-আই-ডি ট্র্যাকিং </span>
            </a>
        </li>
    </ul>
</li>
<li class=" ">
    <a href="javascript:;">
        <i class="fa fa-bar-chart" aria-hidden="true"></i>
        <span class="title"> কার্যক্রম বিবরণী  </span>
        <span class="arrow "></span>
    </a>
    <ul class="sub-menu">
        <li>
            <a href="<?= $this->request->webroot; ?>Performance/office">
                <i class="fa fa-line-chart" aria-hidden="true"></i>
                <span class="title">অফিস</span>
            </a>
        </li>
        <li>
            <a href="<?= $this->request->webroot; ?>Performance/unit">
                <i class="fa fa-line-chart" aria-hidden="true"></i>
                <span class="title">শাখা</span>
            </a>
        </li>
        <li>
            <a href="<?= $this->request->webroot; ?>Performance/designation">
                <i class="fa fa-line-chart" aria-hidden="true"></i>
                <span class="title">পদবি</span>
            </a>
        </li>
	    <li class=" ">
		    <a href="<?= $this->request->webroot; ?>Performance/districtWiseOfficeReportFromSuperman">
			    <i class="fa fa-laptop"></i>

			    <span class="title"> জেলা পর্যায়ের অফিসভিত্তিক প্রতিবেদন
                </span>
		    </a>
	    </li>
    </ul>
</li>
<?php
//please add new url to projapoti_static.php getRobinLayerAccess function. Otherwise normal user can access it.
?>