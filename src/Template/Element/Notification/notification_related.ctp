<script src="https://www.gstatic.com/firebasejs/5.5.8/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.5.8/firebase-messaging.js"></script>
<link rel="manifest" href="<?=$this->request->webroot?>manifest.json">
<script>
    // Initialize Firebase
    var configFirebase = {
        apiKey: "AIzaSyC4BwPe1ezY1iaLed8hAVPHpmE7M-_bdzc",
        authDomain: "nothi-ce562.firebaseapp.com",
        databaseURL: "https://nothi-ce562.firebaseio.com",
        projectId: "nothi-ce562",
        storageBucket: "nothi-ce562.appspot.com",
        messagingSenderId: "543387927535",
    };
    var fireApp = firebase.initializeApp(configFirebase);
    $(document).ready(function(){
        var fcm = fireApp.messaging();
        setTimeout(function () {
//            fcm.usePublicVapidKey("AAAAfoRxw-8:APA91bFK8NwcWYHqWWnJcThaqfNIHmyU863Uo_kJZuWmyU1SnSRIN34ygpUFlEvnMRamDypH8iZRor9L5f_IqtFALNMUfWHtPp5pnV8ixMH3mK4tOrHeWdpFSPJDOFZWFJlKWEhW3eMW");
            fcm.requestPermission().then(function() {
                //console.log('Notification permission granted.');
            }).catch(function(err) {
                //console.log('Unable to get permission to notify.', err);
            });
            // Get Instance ID token. Initially this makes a network call, once retrieved
// subsequent calls to getToken will return from cache.
            fcm.getToken().then(function(currentToken) {
                if (currentToken) {
                    sendTokenToServer(currentToken);
                    updateUIForPushEnabled(currentToken);
                } else {
                    // Show permission request.
                   // console.log('No Instance ID token available. Request permission to generate one.');
                    // Show permission UI.
                    updateUIForPushPermissionRequired();
                    setTokenSentToServer(false);
                }
            }).catch(function(err) {
               // console.log('An error occurred while retrieving token. ', err);
                showToken('Error retrieving Instance ID token. ', err);
                setTokenSentToServer(false);
            });
        },2000);
    })
</script>