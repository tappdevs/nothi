<div class="form-horizontal">
    <div class="form-group">
        <label class="col-sm-4 control-label">সেকশন</label>

        <div class="col-sm-3">
            <?php echo $this->Form->input('unit_name_bng', array('label' => false, 'class' => 'form-control characters_only readonly', 'id' => 'name_character','readonly')); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label">শাখা কোড</label>

        <div class="col-sm-2">
            <?php echo $this->Form->input('unit_nothi_code', array('label' => false, 'class' => 'form-control two-digits')); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label">সর্বশেষ পত্রজারি স্মারক ক্রম</label>

        <div class="col-sm-2">
            <?php echo $this->Form->input('sarok_no_start', array('label' => false, 'class' => 'form-control ')); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label"> ক্রম</label>

        <div class="col-sm-2">
            <?php echo $this->Form->input('unit_level', array('label' => false, 'class' => 'form-control ')); ?>
        </div>
    </div>
   
    <?php echo $this->Form->hidden('status', array('label' => false, 'checked' => true, 'value' => 1)); ?>
</div>

<script type="text/javascript">
    $('.characters_only').keyup(function () {
        this.value = this.value.replace(/[0-9 \&\*\!\@\#\$\^\(\_\-\+\=\;\:\)\.]/g, '');
    });
</script>