<h3 class="form-section">ব্যক্তিগত তথ্য</h3>
<div class="row">
    <div class="col-md-6 form-group form-horizontal">
        <label class="control-label">নাম <span class="text-danger">*</span></label>
        <?= $this->Form->input('fullname_bng', ['label' => false, 'class' => 'form-control', 'type' => 'text', 'placeholder' => 'নাম(বাংলা)','required'=>'required']); ?>
    </div>
    <div class="col-md-6 form-group form-horizontal">
        <label class="control-label">&nbsp; </label>
        <?= $this->Form->input('fullname_eng', ['label' => false, 'class' => 'form-control', 'type' => 'text', 'placeholder' => 'নাম(ইংরেজি)','required'=>'required']); ?>
    </div>
</div>
<div class="row">
    <div class="col-md-6 form-group form-horizontal">
        <label class="control-label">পিতার নাম </label>
        <?php echo $this->Form->input('fathername_bng', array('label' => false, 'class' => 'form-control', 'placeholder' => 'পিতার নাম(বাংলা)')); ?>
    </div>

    <div class="col-md-6 form-group form-horizontal">
        <label class="control-label">&nbsp;</label>
        <?php echo $this->Form->input('fathername_eng', array('label' => false, 'class' => 'form-control', 'placeholder' => 'পিতার নাম(ইংরেজি)')); ?>
    </div>
</div>
<div class="row">
    <div class="col-md-6 form-group form-horizontal">
        <label class="control-label">মাতার নাম </label>
        <?php echo $this->Form->input('mothername_bng', array('label' => false, 'class' => 'form-control', 'placeholder' => 'মাতার নাম(বাংলা)')); ?>
    </div>

    <div class="col-md-6 form-group form-horizontal">
        <label class="control-label">&nbsp;</label>
        <?php echo $this->Form->input('mothername_eng', array('label' => false, 'class' => 'form-control', 'placeholder' => 'মাতার নাম(ইংরেজি)')); ?>
    </div>
</div>
<div class="row">
    <div class="col-md-3 form-group form-horizontal">
        <label class="control-label">জন্ম তারিখ <span class="text-danger">*</span></label>
        <?php echo $this->Form->input('date_of_birth', array('label' => false, 'class' => 'form-control', 'placeholder' => 'জন্ম তারিখ','required'=>'required')); ?>
    </div>
    <div class="col-md-3 form-group form-horizontal">
        <label class="control-label">জাতীয় পরিচয়পত্র নম্বর <span class="text-danger">*</span></label>
        <?php echo $this->Form->input('nid', array('label' => false, 'class' => ' form-control', 'placeholder' => 'জাতীয় পরিচয়পত্র নম্বর','required'=>'required')); ?>
         <span class="help-block font-red"><b>*</b> জাতীয় পরিচয়পত্র নম্বর ১৭ সংখ্যার  হতে হবে। প্রথম চার সংখ্যা  জন্মসন । </span>
    </div>
    <div class="col-md-3 form-group form-horizontal">
        <label class="control-label">জন্ম সনদ নম্বর</label>
        <?php echo $this->Form->input('bcn', array('label' => false, 'class' => 'form-control', 'placeholder' => 'জন্ম সনদ নম্বর')); ?>
    </div>
    <div class="col-md-3 form-group form-horizontal">
        <label class="control-label">পাসপোর্ট নম্বর</label>
        <?php echo $this->Form->input('ppn', array('label' => false, 'class' => 'form-control', 'placeholder' => 'পাসপোর্ট নম্বর')); ?>
    </div>
</div>
<div class="row">
    <div class="col-md-3 form-group form-horizontal">
        <label class="control-label">লিঙ্গ <span class="text-danger">*</span></label>
        <?php echo $this->Form->input('gender', array('empty' => '--Select--', 'type' => 'select', 'label' => false, 'class' => 'form-control', 'placeholder' => 'লিঙ্গ','required'=>'required')); ?>
    </div>
    <div class="col-md-3 form-group form-horizontal">
        <label class="control-label">ধর্ম  <span class="text-danger">*</span></label>
        <?php echo $this->Form->input('religion', array('empty' => '--Select--', 'type' => 'select', 'label' => false, 'class' => 'form-control', 'placeholder' => 'ধর্ম','required'=>'required')); ?>
    </div>
    <div class="col-md-3 form-group form-horizontal">
        <label class="control-label">রক্তের গ্রুপ</label>
        <?php echo $this->Form->input('blood_group', array('empty' => '--Select--', 'type' => 'select', 'label' => false, 'class' => 'form-control', 'placeholder' => 'রক্তের গ্রুপ')); ?>
    </div>
    <div class="col-md-3 form-group form-horizontal">
        <label class="control-label">বৈবাহিক অবস্থা</label>
        <?php echo $this->Form->input('marital_status', array('empty' => '--Select--', 'type' => 'select', 'label' => false, 'class' => 'form-control', 'placeholder' => 'বৈবাহিক অবস্থা')); ?>
    </div>
</div>

<div class="row">
    <div class="col-md-3 form-group form-horizontal">
        <label class="control-label">ব্যক্তিগত ই-মেইল</label>
        <?php echo $this->Form->input('email', array('label' => false, 'class' => 'form-control', 'placeholder' => 'ই-মেইল')); ?>
    </div>
    <div class="col-md-3 form-group form-horizontal">
        <label class="control-label"> ব্যক্তিগত মোবাইল নম্বর <span class="text-danger">*</span></label>
        <?php echo $this->Form->input('mobile', array('label' => false, 'class' => 'form-control', 'placeholder' => 'মোবাইল','required'=>'required')); ?>
    </div>
    <div class="col-md-6 form-group form-horizontal">
        <label class="control-label">বিকল্প মোবাইল নম্বর </label>
        <?php echo $this->Form->input('alternate_mobiles', array('label' => false, 'class' => 'form-control', 'placeholder' => 'বিকল্প মোবাইল ')); ?>
    </div>
</div>

