<h3 class="form-section">পেশাগত তথ্য </h3>
<!-- Type Selection Div START-->
<div class="row">

</div>
<!-- Type Selection Div END-->
<div class="form-group">
    <div class="radio-list">
        <label class="radio-inline">
            <input type="radio" class="cadre_type" name="cadre_type" id="cadre" value="1" <?php echo ((isset($entity) && $entity->is_cadre==1)?'checked=checked':(empty($entity->is_cadre)?'checked':'')) ?> > ক্যাডার </label>
        <label class="radio-inline">
            <input type="radio" class="cadre_type" name="cadre_type" id="non_cadre" value="2" <?php echo ((isset($entity) && $entity->is_cadre==2)?'checked=checked':'') ?> > নন ক্যাডার </label>
        <label class="radio-inline">
            <input type="radio" class="cadre_type" name="cadre_type" id="montri" value="7" <?php echo ((isset($entity) && $entity->is_cadre==7)?'checked=checked':'') ?> > জনপ্রতিনিধি </label>
    </div>
</div>
<hr/>
<!-- CADRE DIV START-->
<div id="cadre_div">
    <div class="row">
        <div class="col-md-4 form-group form-horizontal">
            <label class="control-label">ক্যাডার নম্বর</label>
            <?php echo $this->Form->input('cadre_name', array('label' => false, 'class' => 'form-control', 'placeholder' => 'ক্যাডারের নাম ')); ?>
        </div>
        <div class="col-md-4 form-group form-horizontal">
            <label class="control-label">ব্যাচ নম্বর </label>
            <?php echo $this->Form->input('batch_no', array('label' => false, 'class' => 'form-control', 'placeholder' => 'ব্যাচ নম্বর ')); ?>
        </div>
        <div class="col-md-4 form-group form-inline">
            <label class="control-label">পরিচিতি নম্বর</label>
            <?php
            echo $this->Form->input('no_service_id', array('label' => false, 'type'=>'checkbox','checked'=>((isset($entity) && $entity->is_cadre==3)?false:true))); ?>
           
            <?php echo $this->Form->input('identity_no', array('label' => false, 'class' => 'form-control', 'placeholder' => 'পরিচিতি নম্বর', 'maxlength' => 11, 'disabled'=>((isset($entity) && $entity->is_cadre==3)?true:false))); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 form-group form-horizontal">
            <label class="control-label">যোগদানের তারিখ</label>
            <?php echo $this->Form->input('joining_date', array('label' => false, 'class' => 'form-control', 'placeholder' => '')); ?>
        </div>
<!--        <div class="col-md-6 form-group form-horizontal">
            <label class="control-label">পদমর্যাদা</label>
            <?php // echo $this->Form->input('rank', array('empty' => '--Select--', 'type' => 'select', 'label' => false, 'class' => 'form-control', 'place-holder' => 'পদমর্যাদা')); ?>
        </div>-->
<!--        <div class="col-md-3 form-group form-horizontal">
            <label class="control-label">গ্রেড</label>
            <?php // echo $this->Form->input('grade', array('empty' => '--Select--', 'type' => 'select', 'label' => false, 'class' => 'form-control', 'placeholder' => 'গ্রেড')); ?>
        </div>-->
    </div>
</div>

<!-- CADRE DIV END-->

<!-- NON CADRE DIV START-->
<div style="display:none;" id="non_cadre_div">
    <div class="row">
        <div class="col-md-8 form-group form-horizontal">
            <label class="control-label">নিয়োগ পত্রের স্মারক নম্বর </label>
            <?php echo $this->Form->input('sharok_no', array('label' => false, 'class' => 'form-control', 'placeholder' => 'নিয়োগ পত্রের স্মারক নম্বর ')); ?>
        </div>
        <div class="col-md-4 form-group form-horizontal">
            <label class="control-label">আইডি(যদি থাকে)</label>
            <?php echo $this->Form->input('cadre_no', array('label' => false, 'class' => 'form-control', 'placeholder' => 'আইডি')); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 form-group form-horizontal">
            <label class="control-label">মন্ত্রণালয়</label>
            <?php echo $this->Form->input('ministry_id', array('empty' => '--Select--', 'type' => 'select', 'label' => false, 'class' => 'form-control', 'placeholder' => 'মন্ত্রণালয়')); ?>
        </div>
        <div class="col-md-4 form-group form-horizontal">
            <label class="control-label">দপ্তর / অধিদপ্তর </label>
            <?php echo $this->Form->input('office_id', array('empty' => '--Select--', 'type' => 'select', 'label' => false, 'class' => 'form-control', 'placeholder' => 'দপ্তর / অধিদপ্তর ')); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 form-group form-horizontal">
            <label class="control-label">যোগদানের তারিখ</label>
            <?php echo $this->Form->input('joining_date', array('label' => false, 'class' => 'form-control', 'placeholder' => '')); ?>
        </div>
        <div class="col-md-6 form-group form-horizontal">
            <label class="control-label">পদমর্যাদা</label>
            <?php echo $this->Form->input('rank', array('empty' => '--Select--', 'type' => 'select', 'label' => false, 'class' => 'form-control', 'place-holder' => 'পদমর্যাদা')); ?>
        </div>
        <div class="col-md-3 form-group form-horizontal">
            <label class="control-label">গ্রেড</label>
            <?php echo $this->Form->input('grade', array('empty' => '--Select--', 'type' => 'select', 'label' => false, 'class' => 'form-control', 'placeholder' => 'গ্রেড')); ?>
        </div>
    </div>
</div>
<!-- NON CADRE DIV END-->
<script type="text/javascript">
    $(".cadre_type").click(function () {
        if ($(this).val() == "1") {
            $("#cadre_div").show('slow');
            $("#non_cadre_div").hide('slow');
        }
        else {
            $("#cadre_div").hide('slow');
            $("#non_cadre_div").show('slow');
        }
    });
    
    $("#no-service-id").click(function () {
        if (this.checked==true) {
            $('#identity-no').removeAttr('disabled');
        }
        else {
            $('#identity-no').attr('disabled','disabled');
        }
    });
</script>



