<div class="form-body">
    <div class="row">
        <div class="col-md-4 form-group form-horizontal">
            <label class="control-label"><?php echo __('Office') ?> <?php echo __('Ministry') ?> </label>
            <?= $this->Form->input('office_ministry_id', ['empty' => '--বাছাই করুন--', 'label' => false, 'class' => 'form-control', 'type' => 'select']); ?>
        </div>
        <div class="col-md-4 form-group form-horizontal">
            <label class="control-label"><?php echo __('Office') ?> <?php echo __('Layer') ?> </label>
            <?= $this->Form->input('office_layer_id', ['empty' => '--বাছাই করুন--', 'label' => false, 'class' => 'form-control', 'type' => 'select']); ?>
        </div>
        <div class="col-md-4 form-group form-horizontal">
            <label class="control-label"><?php echo __('Parent') ?> <?php echo __('Office') ?></label>
            <?= $this->Form->input('parent_office_id', ['id' => 'parent-office-id', 'empty' => '--বাছাই করুন--', 'label' => false, 'class' => 'form-control', 'type' => 'select']); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 form-group form-horizontal">
            <label class="control-label">নাম</label>
            <?= $this->Form->input('office_name_bng', ['label' => false, 'class' => 'form-control', 'type' => 'text', 'placeholder' => 'নাম(বাংলায়)']); ?>
        </div>
        <div class="col-md-6 form-group form-horizontal">
            <label class="control-label">&nbsp;</label>
            <?= $this->Form->input('office_name_eng', ['label' => false, 'class' => 'form-control', 'type' => 'text', 'placeholder' => 'নাম(ইংরেজি)']); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 form-group form-horizontal">
            <label class="control-label"><?php echo __('Level') ?></label>
            <?= $this->Form->input('office_level', ['label' => false, 'class' => 'form-control', 'type' => 'text', 'placeholder' => '']); ?>
        </div>
        <div class="col-md-6 form-group form-horizontal">
            <label class="control-label"><?php echo __('Sequence') ?></label>
            <?= $this->Form->input('office_sequence', ['label' => false, 'class' => 'form-control', 'type' => 'text', 'placeholder' => '']); ?>
        </div>
    </div>
</div>


