<div class="form-horizontal">

    <?php echo $this->cell('OriginUnit', ['office_origin_id' => $office_origin_id, 'entity' => $entity]); ?>

    <div class="form-group">
        <label class="col-sm-5 control-label">Superior Designation</label>

        <div class="col-sm-7">
            <?php echo $this->Form->input('superior_designation_id', array('label' => false, 'type' => 'select', 'class' => 'form-control')); ?>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-5 control-label">Designation Name (Bangla)</label>

        <div class="col-sm-7">
            <?php echo $this->Form->input('designation_bng', array('label' => false, 'type' => 'text', 'class' => 'form-control')); ?>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-5 control-label">Designation Name (English)</label>

        <div class="col-sm-7">
            <?php echo $this->Form->input('designation_eng', array('label' => false, 'type' => 'text', 'class' => 'form-control')); ?>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-5 control-label">Designation Level</label>

        <div class="col-sm-7">
            <?php echo $this->Form->input('designation_level', array('label' => false, 'type' => 'text', 'class' => 'form-control')); ?>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-5 control-label">Designation Sequence</label>

        <div class="col-sm-7">
            <?php echo $this->Form->input('designation_sequence', array('label' => false, 'type' => 'text', 'class' => 'form-control')); ?>
        </div>
    </div>
</div>

