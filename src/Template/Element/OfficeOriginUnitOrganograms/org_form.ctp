<div class="form-horizontal">

    <?php echo $this->cell('OriginUnit', ['office_origin_id' => $office_origin_id, 'entity' => $entity]); ?>

    <div class="form-group">
        <label class="col-sm-5 control-label">Superior Post</label>

        <div class="col-sm-7">
            <?php echo $this->Form->input('superior_designation_id', array('label' => false, 'type' => 'select', 'class' => 'form-control input-sm')); ?>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-5 control-label">Post Name (Bangla)</label>

        <div class="col-sm-7">
            <?php echo $this->Form->input('designation_bng', array('label' => false, 'type' => 'text', 'class' => 'form-control input-sm')); ?>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-5 control-label">Post Name (English)</label>

        <div class="col-sm-7">
            <?php echo $this->Form->input('designation_eng', array('label' => false, 'type' => 'text', 'class' => 'form-control input-sm')); ?>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-5 control-label">Post Level</label>

        <div class="col-sm-7">
            <?php echo $this->Form->input('designation_level', array('label' => false, 'type' => 'text', 'class' => 'form-control input-sm')); ?>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-5 control-label">Post Sequence</label>

        <div class="col-sm-7">
            <?php echo $this->Form->input('designation_sequence', array('label' => false, 'type' => 'text', 'class' => 'form-control input-sm')); ?>
        </div>
    </div>
</div>

