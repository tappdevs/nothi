<style>
    .tab-content,.tab-pane.active, li.active{
        background-color:  #FFFFFF!important;
    }
</style>
<?php
$attachments = [];
$mainAttachments = [];
$totalAttachments = 0;
$description = '';
$descriptionused = false;
if(!empty($dak['description'])){
    $description = $dak['description'];
}else{
    if (!empty($dak['dak_cover'])) {
        $description = $dak['dak_cover'] . '<hr/>' . $dak['dak_description'];
    } else {
        $description = $dak['dak_description'];
    }
}
if (!empty($dak_attachments)) {

    foreach ($dak_attachments as $keys => $values) {
        $type = 'অন্যান্য';
        if ($values['attachment_type'] == 'text' || $values['attachment_type'] == 'text/html' || $values['is_main']) {
            $values['attachment_description'] = trim($values['attachment_description']);
            $texts = ($values['is_summary_nothi'] ? ('<div style="height: 815px; border-bottom: 2px solid #eee; margin-bottom: 10px;" >' . $values['content_cover'] . '</div>' . $values['content_body']) : (!empty($values['attachment_description'])?$values['attachment_description']:(!empty($values['content_body'])?$values['content_body']:'')));
            if($values['attachment_type'] == 'text' || $values['attachment_type'] == 'text/html' && empty($texts)){
                if(!$descriptionused && !empty($description)){
                    $texts = $description;
                    $descriptionused = true;
                }else{
                    continue;
                }
            }
            $mainAttachments['main'][] = [
                'id' => $values['id'],
                'dak_id' => $values['dak_id'],
                'dak_type' => $values['dak_type'],
                'attachment_type' => $values['attachment_type'],
                'attachment_description' => $texts,
                'meta_data' => $values['meta_data'],
                'file_name' => $values['file_name'],
                'user_file_name' => $values['user_file_name'],
            ];
        }

        if ($values['attachment_type'] != 'text' && $values['attachment_type'] != 'text/html') {
            $totalAttachments++;
            if (substr($values['attachment_type'], 0, 5) == 'image') {
                $type = 'ছবি';
            } elseif ($values['attachment_type'] == 'application/pdf') {
                $type = 'পিডিএফ';
            }

            $attachments[$type][] = [
                'id' => $values['id'],
                'dak_id' => $values['dak_id'],
                'dak_type' => $values['dak_type'],
                'attachment_type' => $values['attachment_type'],
                'file_name' => $values['file_name'],
                'meta_data' => $values['meta_data'],
                'is_summary_nothi' => $values['is_summary_nothi'],
                'user_file_name' => $values['user_file_name'],
            ];
        }
    }
}

if (!empty($attachments) || !empty($mainAttachments)) {
    $isActiveTab = 'active';
    $isActiveTabBody = 'active';
    ?>

    <div class="tabbable-custom">
        <ul class="nav nav-tabs">
            <?php
            if (!empty($mainAttachments)):
                ?>
                <li class="<?= $isActiveTab ?>">
                    <a href="#main_potro" data-toggle="tab"> মূল পত্র
                        (<?= $this->Number->format(count($mainAttachments['main'])) ?>)</a>
                </li>
                <?php $isActiveTab = ''; endif; ?>
            <?php
            if (!empty($totalAttachments)):?>
                <li class="<?= $isActiveTab ?>">
                    <a href="#tab_all" data-toggle="tab"> সংযুক্তি
                        (<?= $this->Number->format($totalAttachments) ?>)</a>
                </li>
            <?php $isActiveTab = ''; endif; ?>
        </ul>
        <div class="tab-content">
            <?php
            if (!empty($mainAttachments)):
                ?>
                <div class="tab-pane <?= $isActiveTabBody ?>" id="main_potro">

                    <?php $itm = 0;
                    $itbm = 0; ?>
                    <div class="portlet-body">
                        <ul class="nav nav-tabs">
                            <?php foreach ($mainAttachments['main'] as $index => $value): ?>
                                <li class="<?= $itm == 0 ? 'active' : '' ?>">
                                    <a href="#tab_main_<?= $value['id'] ?>"
                                       data-toggle="tab"> <?= $this->Number->format(++$index); ?> </a>
                                </li>
                                <?php $itm++; endforeach; ?>
                        </ul>
                        <div class="tab-content">
                            <?php foreach ($mainAttachments['main'] as $index => $value): ?>
                                <div class="tab-pane fade <?= $itbm == 0 ? 'active in' : '' ?>"
                                     id="tab_main_<?= $value['id'] ?>">
                                    <p class="help-block text-center">
                                        <?= $value['user_file_name'] ?>
                                    </p>
                                    <?= showFileView($value, $this) ?>
                                </div>
                                <?php $itbm++; endforeach; ?>
                        </div>
                    </div>
                </div>
                <?php $isActiveTabBody = ''; endif; ?>
            <?php
            if (!empty($attachments)):?>
                <div class="tab-pane <?= $isActiveTabBody ?>" id="tab_all">

                    <ul class="nav nav-tabs nav-justified">
                        <li class="active">
                            <a href="#tab_all_list" data-toggle="tab"> তালিকা</a>
                        </li>
                    <?php
                    if (!empty($attachments)):
                        foreach ($attachments as $keys => $values):
                            ?>
                            <li class="">
                                <a href="#tab_all_<?= $keys ?>" data-toggle="tab"> <?= $keys ?>
                                    (<?= $this->Number->format(count($values)) ?>)</a>
                            </li>
                            <?php
                            $isActiveTab = '';
                        endforeach;
                        ?>
                    <?php endif; ?>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_all_list">
                            <div class="panel-group accordion">
                            <?php
                            foreach ($attachments as $key => $values):
                                $key = ($key == 'main' ? 'মূল পত্র' : $key);
                                ?>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="accordion-toggle accordion-toggle-styled" data-toggle="collapse"
                                               data-parent="#accordion3" href="#collapse_<?= $key ?>"
                                               aria-expanded="true"> <?= $key ?> </a>
                                        </h4>
                                    </div>
                                    <div id="collapse_<?= $key ?>" class="panel-collapse collapse in" aria-expanded="true">

                                        <div class="panel-body">
                                            <table class="table table-advance table-bordered table-striped">
                                                <thead>
                                                <tr>
                                                    <th class="text-center">ক্রম</th>
                                                    <th class="text-left">নাম</th>
                                                    <th class="text-center">কার্যক্রম</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php foreach ($values as $index => $value): ?>
                                                    <tr>
                                                        <td class="text-center"><?= $this->Number->format(++$index) ?></td>
                                                        <td><?= $value['user_file_name'] ?></td>
                                                        <td class="text-center"><?= $this->Html->link(__('Download'), [
                                                                "controller" => "dakMovements",
                                                                "action" => "downloadDakAttachment",
                                                                "?" => ["dak_id" => $value['dak_id'], "attachment_id" => $value['id']]], ['class' => 'btn btn-link btn-primary btn-sm']) ?></td>
                                                    </tr>
                                                <?php endforeach; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                        </div>

                        <?php
                        if (!empty($attachments)):
                            foreach ($attachments as $keys => $values):
                                ?>
                                <div class="tab-pane" id="tab_all_<?= $keys ?>">
                                    <?php $it = 0;
                                    $itb = 0; ?>
                                    <div class="portlet-body">
                                        <ul class="nav nav-tabs">
                                            <?php foreach ($values as $index => $value): ?>
                                                <li class="<?= $it == 0 ? 'active' : '' ?>">
                                                    <a href="#tab_attach_<?= $value['id'] ?>"
                                                       data-toggle="tab"> <?= $this->Number->format(++$index) ?> </a>
                                                </li>
                                                <?php $it++; endforeach; ?>
                                        </ul>
                                        <div class="tab-content">
                                            <?php foreach ($values as $index => $value): ?>
                                                <div class="tab-pane fade <?= $itb == 0 ? 'active in' : '' ?>"
                                                     id="tab_attach_<?= $value['id'] ?>">
                                                    <p class="help-block text-center">
                                                        <?= $value['user_file_name'] ?>
                                                    </p>
                                                    <?= showFileView($value, $this) ?>
                                                </div>
                                                <?php $itb++; endforeach; ?>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            endforeach;
                            ?>
                        <?php endif; ?>
                    </div>
                </div>
                <?php $isActiveTabBody = ''; endif; ?>
        </div>
    </div>
<!--    <script>-->
<!--		$('[title]').tooltip('destroy');-->
<!--		$('[title]').tooltip({'placement':'top','container':'body'});-->
<!--    </script>-->
    <?php
}
