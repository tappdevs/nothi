<?php
$session = $this->request->session();
$user_id = $session->read('user_id');
?>
<!-- BEGIN CHOOSE PASSWORD FORM -->
<form class="choosepassword-form" action="choosepassword" method="post">
    <div class="form-group">
        <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
        <label class="control-label visible-ie8 visible-ie9">Password</label>
        <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off"
               placeholder="Password" name="password" id="password" required="true"/>
    </div>
    <div class="form-group">
        <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
        <label class="control-label visible-ie8 visible-ie9">Confirm Password</label>
        <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off"
               placeholder="Confirm password" id="cpassword" required="true"/>
        <input type="hidden" name="id" value="<?php echo $user_id; ?>"/>
    </div>
    <div class="form-actions">
        <button type="submit" id="btn_submit" class="btn green uppercase">Submit</button>
        <?= $this->Html->link('Back to login', ['action' => 'login'], ['class' => 'forget-password']) ?>
    </div>
</form>
<!-- END CHOOSE PASSWORD FORM -->

<script type="text/javascript">
    $(function () {
        $("#btn_submit").click(function (e) {
            if ($("#password").val() != $("#cpassword").val()) {
                alert("The passwords didn't match!");
                return false;
            }
        });
    });
</script>