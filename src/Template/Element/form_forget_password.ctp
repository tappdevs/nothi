<!-- BEGIN FORGET PASSWORD FORM -->
<form class="forgetpassword-form" action="forgetpassword" method="post">
    <div class="form-group">
        <label class="control-label">Send activation code to </label>

        <div class="radio-list">
            <label class="radio-inline">
                <input type="radio" class="activation_type" name="activation_type" id="email" value="email" checked>
                Email </label>
            <label class="radio-inline">
                <input type="radio" class="activation_type" name="activation_type" id="sms" value="sms"> SMS </label>
        </div>
    </div>
    <hr/>
    <div class="form-group">
        <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
        <label class="control-label visible-ie8 visible-ie9">Username</label>
        <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off"
               placeholder="Username" name="username" required="true"/>
        <!--input type="hidden" name="activation_token" value="<?php echo $activation_page_token; ?>" /-->
    </div>
    <div class="form-actions">
        <button type="submit" class="btn green uppercase">Reset Password</button>
        <?= $this->Html->link('Back to login', ['action' => 'login'], ['class' => 'forget-password']) ?>
    </div>
</form>
<!-- END FORGET PASSWORD FORM -->