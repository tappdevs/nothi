<!-- BEGIN LOGIN FORM -->
<form class="login-form" action="login" method="post">
    <h3 class="form-title">নথি</h3>

    <div class="form-group">
        <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
        <label class="control-label visible-ie8 visible-ie9">লগইন আইডি</label>
        <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off"
               placeholder="লগইন আইডি" name="username"/>
    </div>
    <div class="form-group">
        <label class="control-label visible-ie8 visible-ie9">পাসওয়ার্ড</label>
        <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off"
               placeholder="পাসওয়ার্ড" name="password"/>
    </div>
    <div class="form-actions">
        <button type="submit" class="btn green uppercase">লগইন</button>
        <label class="rememberme check" style="display: none;">
            <input style="display: none;" type="checkbox" name="remember" value="1"/>Remember </label>
        <a style="display: none;" href="javascript:" id="forget-password" class="forget-password">Forgot Password?</a>
    </div>
</form>
<!-- END LOGIN FORM -->