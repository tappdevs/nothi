<style>
    /*   .rd-val{
            padding-top:2px!important;
        }*/
</style>
<!-- BEGIN LOGIN FORM -->
<div id="Script-disable-div" class="alert alert-danger">
    <noscript>দুঃখিত, আপনার ব্রাউজার জাভাস্ক্রিপ্ট সমর্থন করে না! অনুগ্রহপুর্বক জাভাস্ক্রিপ্ট চালু করুন অথবা ব্রাউজার আপডেট করুন।</noscript>
</div>
<div class="alert alert-danger" id="GChrome" style="display: none;">
    অনুগ্রহপূর্বক নথি অ্যাপ্লিকেশনের সকল কার্যক্রম উপভোগ করতে Google Chrome ব্যবহার করুন। Google Chrome ইন্সটল না থাকলে
    <a target="_blank" href="https://www.google.com/chrome/"> এখানে ক্লিক করুন </a>।
</div>
<div id="form-div" style="display: none;">
<form class="login-form" action="login" method="post">
    <div class="form-group">
        <div class="radio-list">
            <label class="radio-inline font-purple-seance" style="font-size: 12pt!important;">
                <div class="radio" id="uniform-optionsRadios1">
                    <input type="radio" name="optionsRadios" id="optionsRadios1" checked="">
                </div>
                <span class="rd-val"> &nbsp; ইউজার আইডি</span>
            </label>
            <div class="pull-right">
                <label class="radio-inline font-green-jungle" style="font-size: 12pt!important;">
                    <div class="radio" id="uniform-optionsRadios2">
                        <input type="radio" name="optionsRadios" id="optionsRadios2">
                    </div>
                    <span class="rd-val">&nbsp; ইউজার নেম</span>
                </label>
            </div>
        </div>
    </div>
    <div class="form-group" id="real_username">
        <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
        <label class="control-label visible-ie8 visible-ie9">ইউজার আইডি</label>
        <div class="input-icon">
            <i class="fa fa-user"></i>
            <input class="form-control placeholder-no-fix" id="username" type="text" autocomplete="off"
                   placeholder="ইউজার আইডি" name="username"/>
        </div>
    </div>
    <div class="form-group" id="alias_username">
        <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
        <label class="control-label visible-ie8 visible-ie9">ইউজার নেম</label>
        <div class="input-icon">
            <i class="fa fa-user"></i>
            <input class="form-control placeholder-no-fix" id="alias" type="text" autocomplete="off"
                   placeholder="ইউজার নেম" name="alias"/>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label visible-ie8 visible-ie9">পাসওয়ার্ড</label>
        <div class="input-icon">
            <i class="fa fa-lock" aria-hidden="true"></i>
            <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="পাসওয়ার্ড"
                   name="password"/>
        </div>
    </div>
    <?= $this->Form->hidden('fingerprint', ['id' => 'fingerprint']) ?>
    <div class="">
        <!--<label class="checkbox">-->
        <!--<input type="checkbox" name="remember" value="1"/> মনে রাখুন </label>-->
        <button type="submit" class="btn green pull-right">
            প্রবেশ <i class="fs1 a2i_gn_login3"></i>
        </button>
    </div>
</form>

<!-- END LOGIN FORM -->

<!-- BEGIN FORGOT PASSWORD FORM -->
<form class="forget-form" method="post">

    <div class="form-group">
        <div class="input-icon">
            <i class="fs1 a2i_gn_secrecy3"></i>
            <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="ইউজার আইডি"
                   name="username"/>
        </div>
    </div>
    <div class="form-group">
        <div class="input-icon">
            <i class="fa fa-envelope"></i>
            <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="ইমেইল"
                   name="email"/>
        </div>
    </div>

    <div class="form-actions">
        <button type="button" id="back-btn" class="btn btn-danger">
            <i class="fs1 a2i_gn_previous1"></i> বাতিল করুন
        </button>
        <button type="submit" class="btn blue pull-right">
            অনুরোধ করুন <i class="fs1 a2i_gn_send1"></i>
        </button>
    </div>
</form>
    <div class="forget-password text-left ">
        <a href="javascript:;" id="forget-password"><i class="fa fa-key"></i> &nbsp;পাসওয়ার্ড ভুলে গেছেন?</a>
    </div>
</div>
<!-- END FORGOT PASSWORD FORM -->
<script>
    $("#alias").hide();
    function pad(str, max) {
        str = str.toString();
        return str.length < max ? pad("0" + str, max) : str;
    }

    $(document).on('change', '[name=username]', function () {
        var loginid = $(this).val();
        var start = loginid.substr(0, 1);
        var restof = loginid.substr(1);
        loginid = start + pad(restof, 11);
        $(this).val(loginid);
    });
    $("#optionsRadios1").click(function () {
        $("#alias").hide();
        $("#username").show();
        $("#alias").val('');
    });
    $("#optionsRadios2").click(function () {
        $("#username").hide();
        $("#username").val('');
        $("#alias").show();
    });
</script>