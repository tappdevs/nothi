<link href='<?php echo CDN_PATH; ?>assets/global/plugins/froala_editor/css/froala_editor.min.css' rel='stylesheet' type='text/css' />
<link href='<?php echo CDN_PATH; ?>assets/global/plugins/froala_editor/css/froala_style.min.css' rel='stylesheet' type='text/css' />

<link rel="stylesheet" href="<?php echo CDN_PATH; ?>assets/global/plugins/froala_editor/css/froala_style.css">
<link rel="stylesheet" href="<?php echo CDN_PATH; ?>assets/global/plugins/froala_editor/css/plugins/colors.css">
<link rel="stylesheet" href="<?php echo CDN_PATH; ?>assets/global/plugins/froala_editor/css/plugins/line_breaker.css">
<link rel="stylesheet" href="<?php echo CDN_PATH; ?>assets/global/plugins/froala_editor/css/plugins/table.css">

<!--dictionary related css-->
<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>assets/global/plugins/jquery-contextMenu/jquery-contextMenu.min.css"/>

<!-- Include JS file. -->
<script type='text/javascript' src='<?php echo CDN_PATH; ?>assets/global/plugins/froala_editor/js/froala_editor.min.js'></script>
<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/froala_editor/js/plugins/align.min.js"></script>
<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/froala_editor/js/plugins/code_beautifier.min.js"></script>
<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/froala_editor/js/plugins/colors.min.js"></script>
<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/froala_editor/js/plugins/draggable.min.js"></script>
<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/froala_editor/js/plugins/font_size.min.js"></script>
<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/froala_editor/js/plugins/font_family.min.js"></script>
<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/froala_editor/js/plugins/fullscreen.min.js"></script>
<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/froala_editor/js/plugins/line_breaker.min.js"></script>
<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/froala_editor/js/plugins/link.min.js"></script>
<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/froala_editor/js/plugins/lists.min.js"></script>
<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/froala_editor/js/plugins/paragraph_format.min.js"></script>
<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/froala_editor/js/plugins/paragraph_style.min.js"></script>
<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/froala_editor/js/plugins/table.min.js"></script>
<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/froala_editor/js/plugins/entities.min.js"></script>
<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/froala_editor/js/plugins/inline_style.min.js"></script>
<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/froala_editor/js/plugins/quote.min.js"></script>
<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/froala_editor/js/plugins/word_paste.min.js"></script>

<!--Dictionary related js file-->
<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/jquery-ui-position/jquery-ui-position.js"></script>
<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/jquery-contextMenu/jquery-contextMenu.js"></script>
<script type="text/javascript" src="<?php echo CDN_PATH; ?>js/jquery.get-word-by-event.js"></script>

<script>
    $.FroalaEditor.RegisterCommand('tableInsert', {
        callback: function (cmd, rows, cols) {
            this.table.insert(rows, cols);
            this.popups.hide('table.insert');
            $('.fr-basic table').addClass('class2');
        }
    });
    $(document).off('click', '.rmWordList').on('click', '.rmWordList',function() {
        removeSpellChecker();
    });
    function removeSpellChecker() {
        $('.fr-wrapper .misspelled-word').each(function(i,v) {
            var word= $(this).text();
            $(this).replaceWith(word.toString());
        });
    }
    var words_separated = [];
    $(document).off('click', '.wordList').on('click', '.wordList', function (e) {
        getWordSeperated();
    });
    function getWordSeperated(){
        removeSpellChecker();
        var text='';
        var textBody = document.getElementsByClassName("fr-view");
        for (var i = 0; i < textBody.length; i++) {
            var string = textBody[i].innerText;
            text += string;
        };
        // var data=[];
        words_separated = [];
        words_separated= text.split(/[\s+._"'<*>;\-|,/=():।!#$%@^&?+{}\[\]1234567890১২৩৪৫৬৭৮৯০]/).sort().filter( function(v,i,o){
            if(v==''){return false;}
            if(v.length <2 ){return false;}
            return v!==o[i-1];
        });

        if(words_separated.length > 0){
            requestToCheckDictionary(0,words_separated.length);
        }
    }
    function requestToCheckDictionary(indx,len){
        if(indx >= len ){
            return;
        }

        redMark(words_separated.slice(indx,indx+50),indx+50,len);
    }
    function redMark(data,indx,len){
        $.ajax({
            type: 'POST',
            url: 'https://ovidhan.tappware.com/search.php',
            data: {'word':data},
            success: function (data) {
                var newData = jQuery.parseJSON(data);
                $.each(newData,function(i,v) {
                    var misspelledReplacedText = "<span class='misspelled-word'>"+v.word+"</span>";
                    $('.fr-wrapper').children().each(function() {
                        var wrap =v.word;
                        $(this).html($(this).html().replace(new RegExp(wrap,'g'),misspelledReplacedText));
                        // $('.fr-view').html().find(wrap).replaceWith(misspelledReplacedText);
                    });
                });
                requestToCheckDictionary(indx+1,len);
            }
        });
    }
    var sel = '';
    var range = '';
    function getCaretPosition() {
        sel = window.getSelection();
        range = window.getSelection().getRangeAt(0);
    }
    function pasteHtmlAtCaret(html) {
        var el = document.createElement("div");
        el.innerHTML = html;
        var frag = document.createDocumentFragment(), node, lastNode;
        while ( (node = el.firstChild) ) {
            lastNode = frag.appendChild(node);
        }
        range.insertNode(frag);
        if (lastNode) {
            range = range.cloneRange();
            range.setStartAfter(lastNode);
            range.collapse(true);
            sel.removeAllRanges();
            sel.addRange(range);
        }
    }
</script>
<style>
    .context-menu-list{
        z-index: 9996 !important;
    }
    .misspelled-word {
        background: url(<?= $this->request->webroot ?>underline.gif) bottom repeat-x;
    }
    .fr-command.fr-btn+.fr-dropdown-menu .fr-dropdown-wrapper .fr-dropdown-content ul.fr-dropdown-list li a{
        white-space:normal!important;
    }
  
    #noteView, #noteComposer, #cs-body-editor
    {
        overflow-x:scroll;
    }

    #noteView table tr td, #noteComposer table tr td,#cs-body-editor table tr td
    {
        width:fit-content;
        word-break:normal;
        /*white-space: nowrap;*/
    }

    #noteView table tr td:first-child, #noteComposer table tr td:first-child, #cs-body-editor table tr td:first-child
	{
		min-width:65px !important;
	}
    .fr-command{
        border-bottom: inset;
    }
 
</style>