<div class="print-margin">
  <form id="print_margin">
      <div class="input-group p-2">
        <input type="text" autofocus class="form-control form-control-sm" name="top" placeholder="⇧" value=".8">
        <input type="text" class="form-control form-control-sm" name="right" placeholder="⇨" value=".8">
        <input type="text" class="form-control form-control-sm" name="bottom" placeholder="⇩" value=".8">
        <input type="text" class="form-control form-control-sm" name="left" placeholder="⇦" value=".8">
        <div class="input-group-append">
          <button type="submit" id="saveMargin" class="btn btn-sm btn-primary">💾</button>
        </div>
      </div>
  </form>
</div>