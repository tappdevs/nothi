<style>
    .strikethrough {
        position: relative;
    }
    .strikethrough:before {
        position: absolute;
        content: "";
        left: 0;
        top: 50%;
        right: 0;
        border-top: 1px solid;
        border-color: inherit;

        -webkit-transform:rotate(-14deg);
        -moz-transform:rotate(-14deg);
        -ms-transform:rotate(-14deg);
        -o-transform:rotate(-14deg);
        transform:rotate(-14deg);
    }
</style>
<div class="bg-grey text-center font-lg" id="SSLMessageForWindows" hidden>
</div>
<?php
if(date("Y-m-d") == '2019-01-23' and date("G") <= 19 && Live == 1):
?>
<div class="bg-red text-center font-lg">
    নথি অ্যাপ্লিকেশন এর উন্নীতকরণের লক্ষ্যে  আজ (২৩ জানুয়ারি ২০১৯) সন্ধ্যা ৫.৩০ টা হতে ৭:৩০ পর্যন্ত বন্ধ থাকবে । সাময়িক অসুবিধার জন্য আমরা দুঃখিত।
</div>
<?php
endif;
?>
<script src="<?php echo CDN_PATH; ?>js/client.min.js"
type="text/javascript"></script>
<script>

    $(document).ready(function () {
        $("#Script-disable-div").css("display", "none");

        var client = new ClientJS();
        var fingerprint = client.getFingerprint(); // Calculate Device/Browser
        if (!client.isChrome()) {
            $("#GChrome").show();
			$("#form-div").show();
        }else{
			$("#form-div").css("display", "block");
        }
        <?php
        if(Live == 1):
        ?>
        if(client.isWindows()){
//            $("#SSLMessageForWindows").show();
        }
        if(client.isMobileAndroid()){
            $("#SSLMessageForAndroid").show();
        }
        if( client.isMobileIOS()){
            $("#SSLMessageForIOS").show();
        }
        <?php
        endif;
        ?>
        $("#fingerprint").val(fingerprint);
    });
</script>