<div class="form-actions">
    <div class="row">
        <div class="col-md-offset-4 col-md-9">
            <button type="submit" class="btn   blue"><?php echo __("Submit"); ?></button>
            <button type="reset" class="btn   default"><?php echo __("Reset"); ?></button>
        </div>
    </div>
</div>