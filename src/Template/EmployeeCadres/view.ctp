<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption"><i class=""></i>View Cadre</div>
        <div class="tools">
            <a href="javascript:" class="collapse"></a>
            <a href="#portlet-config" data-toggle="modal" class="config"></a>
            <a href="javascript:" class="reload"></a>
            <a href="javascript:" class="remove"></a>
        </div>
    </div>
    <div class="portlet-body">

        <div class="panel-body">
            <table class="table table-bordered">

                <tr>
                    <td class="text-right">Cadre Name (English)</td>
                    <td><?= h($employee_cadre->cadre_name_eng) ?></td>
                </tr>
                <tr>
                    <td class="text-right">Cadre Name (Bangla)</td>
                    <td><?= h($employee_cadre->cadre_name_bng) ?></td>
                </tr>

                <tr>
                    <td><?= $this->Html->link('Edit This Cadre', ['action' => 'edit', $employee_cadre->id], array('class' => 'btn btn-primary pull-right')) ?></td>
                    <td><?= $this->Html->link('Back to Cadre List', ['action' => 'index'], array('class' => 'btn btn-primary pull-left')) ?></td>
                </tr>

            </table>
        </div>
    </div>
</div>
