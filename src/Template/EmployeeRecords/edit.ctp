<div class="portlet box green">
  <div class="portlet-title">
    <div class="caption"><i class=""></i> <?php echo __('Employeer') ?> <?php echo __('Information') ?>  </div>
    <div class="tools">
      <a href="javascript:" class="collapse"></a>
      <a href="#portlet-config" data-toggle="modal" class="config"></a>
      <a href="javascript:" class="reload"></a>
      <a href="javascript:;" class="remove"></a>
    </div>
  </div>
  <div class="portlet-body form">
      <?php
      echo $this->Form->create($entity, array('id' => 'EmployeeRecordForm'));
      echo $this->Form->hidden('id');
      ?>
    <div class="form-body">
        <?php echo $this->element('EmployeeRecords/personal_info'); ?>
        <?php // echo $this->element('EmployeeRecords/professional_info'); ?>
      <!--      <?php /*echo $this->element('EmployeeRecords/office_info'); */ ?>
            --><?php /*echo $this->element('EmployeeRecords/login_info'); */ ?>
    </div>
    <div class="form-actions">
      <div class="row">
        <div class="col-md-offset-4 col-md-9">
          <button class="btn blue" type="button" onclick="submitForm()"><?php echo __("Submit"); ?></button>
          <button type="reset" class="btn default"><?php echo __("Reset"); ?></button>
        </div>
      </div>
    </div>
      <?php echo $this->Form->end(); ?>
  </div>
</div>
<script type="text/javascript"
        src="<?php echo CDN_PATH; ?>assets/global/plugins/jQuery-Mask/jquery.mask.min.js"></script>
<script type="text/javascript">
  $(function () {
    OfficeSetup.init();
    $('.date-picker').datepicker({
      rtl: Metronic.isRTL(),
      orientation: "left",
      autoclose: true,
      format: "yyyy-mm-dd"
    });

    $('#personal-mobile').mask('AAAAAAAAAAA'
      , {
        translation: {
          A: {pattern: /[\u09E6-\u09EF0-9]/},
        },
        placeholder: "01XXXXXXXXX"
      });
    $('#alternative-mobile').mask('AAAAAAAAAAA'
      , {
        translation: {
          A: {pattern: /[\u09E6-\u09EF0-9]/},
        },
        placeholder: "01XXXXXXXXX"
      });
  });

  function submitForm() {
    if (isEmpty($("#name-bng").val())) {
      toastr.error(' নাম দেওয়া হয়নি। ');
      return false;
    }
    if ($("#nid").val().length != 17 && $("#nid").val().length != 10) {
      toastr.error(' জাতীয় পরিচয়পত্র নম্বর ১০ অথবা ১৭ সংখ্যার হতে হবে। ');
      return false;
    }
    if ($("#date-of-birth").val().length == 0) {
      toastr.error(' জন্ম তারিখ দেওয়া হয়নি । ');
      return false;
    }
    //#2385
    var regex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
    if (regex.test($("#personal-email").val()) == false) {
      toastr.error("দুঃখিত! ইমেইল সঠিক নয়");
      $("#personal-email").focus();
      return false;
    }

    if ($('#personal-mobile').val().length != 11) {
      toastr.error("দুঃখিত! ব্যক্তিগত মোবাইল নম্বর সঠিক নয়");
      $("#personal-mobile").focus();
      return false;
    } else {
      $('#personal-mobile').val(bnToen($('#personal-mobile').val()));
    }

    if (!isEmpty($('#alternative-mobile').val()) && $('#alternative-mobile').val().length != 11) {
      toastr.error("দুঃখিত! বিকল্প মোবাইল নম্বর সঠিক নয়");
      $("#alternative-mobile").focus();
      return false;
    } else {
      if (!isEmpty($('#alternative-mobile').val())) {
        $('#alternative-mobile').val(bnToen($('#alternative-mobile').val()));
      }
    }
    $("#EmployeeRecordForm").submit();
  };
</script>

