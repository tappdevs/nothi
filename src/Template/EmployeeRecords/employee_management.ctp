<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<div class="portlet light">
    <div class="portlet-title">
        <div class="caption"><i class="fa fa-group"></i><?php echo __('Employee') ?> <?php echo __('Management') ?>
        </div>
    </div>
    <div class="portlet-body form">
        <?= $employeeCell = $this->cell('EmployeeJobStatus'); ?>
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET -->
                <div class="portlet light ">
                    
                    <div class="portlet-body">
                        <?php if(empty($employee_offices)){ ?>
                        <?= $officeSelectionCell = $this->cell('OfficeSelection', ['entity' => '']) ?>
                        <div class="row">
                            <div class="col-md-4 form-group form-horizontal">
                                <label class="control-label"> <?php echo __("Office") ?> </label>
                                <?php
                                echo $this->Form->input('office_id', array(
                                    'label' => false,
                                    'class' => 'form-control',
                                    'empty' => '----'
                                ));
                                ?>
                            </div>
                        </div>
                        <?php }else{
                            echo $this->Form->input('office_id', array(
                                'label' => false,
                                'class' => 'form-control',
                                'type'=>'hidden',
                                'default' => $employee_offices['office_id']
                            ));
                        }  ?>

                        <hr/>
                        <div class="row">
                            <div class="portlet light col-md-12">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-check-square-o"></i><?php echo __('Office') ?> <?php echo __('Unit') ?> <?php echo __('Organogram') ?>
                                    </div>
                                </div>
                                <div class="portlet-window-body">
                                    <div class="" id="office_unit_tree_panel">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PORTLET -->
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script type="text/javascript">
    var EmployeeAssignment = {
        checked_node_ids: [],
        loadMinistryWiseLayers: function () {
            OfficeSetup.loadLayers($("#office-ministry-id").val(), function (response) {

            });
        },
        loadMinistryAndLayerWiseOfficeOrigin: function () {
            OfficeSetup.loadOffices($("#office-ministry-id").val(), $("#office-layer-id").val(), function (response) {

            });
        },
        loadOriginOffices: function () {
            OfficeSetup.loadOriginOffices($("#office-origin-id").val(), function (response) {

            });
        },

        getUnitOrganogramByOfficeId: function (office_id) {
            $("#office_unit_tree_panel").html("");
               Metronic.blockUI({
                    target: '.page-container',
                    boxed: true,
                    message: 'অপেক্ষা করুন'
                });
            PROJAPOTI.ajaxSubmitAsyncDataCallbackWithoutAbrot(js_wb_root + 'officeManagement/getOfficeDesignationsDataByOfficeId',
                {'office_id': office_id}, 'json',
                function (response_data) {
                  

                    var str_unit_org = '';

                    str_unit_org += '<div class="table-container">';
                    str_unit_org += '<table class="table table-striped table-bordered table-hover" style="width: 100%;" id="organogramTable">';
                    str_unit_org += '<tbody>';

                    var unit_name_with_id = [];
                    $.each(response_data, function (i, v) {
                        unit_name_with_id[v.office_unit.id] = v.office_unit.unit_name_bng;
                    });

                    $.each(response_data, function (i) {
                        response = response_data[i];
                        var office_info = response.office;
                        var office_name = office_info.office_name_bng;
                        if(isEmpty(office_name)){
                            office_name = '';
                        }
                        var unit = response.office_unit;
                        var unit_name_bng = '';
                         if(!isEmpty(unit.unit_name_bng)){
                              unit_name_bng =  unit.unit_name_bng;
                            }
//                        console.log(office_name,unit.unit_name_bng);
                        var orgs = response.designations;

                        $.each(orgs, function (i) {
                            var org = orgs[i];

                            str_unit_org += '<tr id="checked_unit_' + unit.id + '_org_' + org.id + '">';

                            if (org.active_status == 0) {
                                str_unit_org += '<td style="width: 40%;"><input disabled="true" checked="true" onclick="EmployeeAssignment.selectOrganogram(this)" type="checkbox" class="icheck" data-checkbox="icheckbox_flat-red" data-unit-id="' + unit.id + '" id="' + org.id + '" data-designation-name="' + org.designation_bng + '" data-unit-name="' + unit_name_bng + '" data-office-name="' + office_name + '"> &nbsp;' + org.designation_bng + '</td>';
                                str_unit_org += '<td style="width: 60%;" class="form-group">';
                                str_unit_org += org.employee;
                                str_unit_org += '</td>';

                            }
                            else {
//                                str_unit_org += '<td style="width: 40%;"><input onclick="EmployeeAssignment.selectOrganogram(this)" type="checkbox" class="icheck" data-checkbox="icheckbox_flat-red" data-unit-id="' + unit.id + '" id="' + org.id + '" data-designation-name="' + org.designation_bng + '"> &nbsp;' + org.designation_bng + ' <button type="button" onclick="EmployeeAssignment.historyDesignation(checked_unit_' + unit.id + '_org_' + org.id + ')" class="btn   green"><i class="fa fa-history"></i></button></td>';
                                str_unit_org += '<td style="width: 40%;"><input onclick="EmployeeAssignment.selectOrganogram(this)" type="checkbox" class="icheck" data-checkbox="icheckbox_flat-red" data-unit-id="' + unit.id + '" id="' + org.id + '" data-designation-name="' + org.designation_bng + '" data-unit-name="' + unit_name_bng + '" data-office-name="' + office_name + '"> &nbsp;' + org.designation_bng+(!isEmpty(org.employee)?' (পূর্ববর্তী  কর্মকর্তাঃ '+org.employee+')':'') + ' </td>';

                                str_unit_org += '<td style="width: 60%;" class="form-group">';
                                str_unit_org += '<div class="col-md-5 col-lg-5 col-sm-6  selection_unit"><?php echo $this->Form->input ( 'incharge_label', array ('label' => false,'class' => 'form-control','options'=>$officeInchargeTypes,'value' => '','empty' => '--Select Incharge Type--' ));?></div>';

                                str_unit_org += '<div class="col-md-5 col-lg-5 col-sm-4  selection_unit"><?php echo $this->Form->input('joining_date', array('type'=>'text','label' => false, 'class' => 'form-control form-control-inline input-md date-picker', 'placeholder' => 'যোগদানের দিবস','data-date-end-date'=>"0d")); ?></div>';
                                str_unit_org += '<div class="col-md-2 col-lg-2 col-sm-2 selection_unit"><button type="button" onclick="EmployeeAssignment.assignDesignation(checked_unit_' + unit.id + '_org_' + org.id + ')" class="btn   green"><i class="fa fa-hand-o-right"></i></button></div>';
                                str_unit_org += '</td>';
                            }

                            str_unit_org += '<td style="display:none">';
                            if (unit.parent_unit_id != 0) {
                                str_unit_org += unit_name_with_id[unit.parent_unit_id] + ' <i class="fa fa-caret-right"></i> ';
                            }
                            str_unit_org += unit.unit_name_bng + '</td>';

                            str_unit_org += '</tr>';
                        });
                    });
                    str_unit_org += '</tbody>';

                    $("#office_unit_tree_panel").append(str_unit_org);
                    $(".selection_unit").hide();
                    initTable1();
                    $('.dataTables_filter').css('float','left');
                    Metronic.unblockUI('.page-container');
                }
            );
        },

        selected_organogram: [],

        selectOrganogram: function (input) {
            var organogram_id = $(input).attr('id');
            var unit_id = $(input).data('unit-id');
            var org_unit = "checked_unit_" + unit_id + "_" + "org_" + organogram_id;

            $('#'+organogram_id).parents('tr').find(".selection_unit").toggle();
            reloadPicker();
        },

        historyDesignation: function (row) {
            Metronic.blockUI({
                target: '.page-container',
                boxed: true,
                message: 'অপেক্ষা করুন'
            });
            var row_id = $(row).attr('id');
            var org_checkbox = $("#" + row_id).find("input[type=checkbox]");
            var designation_id = org_checkbox[0].id;
            PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + 'employeeRecords/getDesignationHistoryInfo',
                {'designation_id' : designation_id}, 'json',
                function (response) {
                    if (response.status == 'success') {
                        bootbox.dialog({
                            message: response.msg,
                            title: "কর্মকর্তা যোগদান",
                            buttons: {
                                danger: {
                                    label: "বন্ধ করুন",
                                    className: "red",
                                    callback: function () {
                                        Metronic.unblockUI('.page-container');
                                    }
                                }
                            }
                        });
                    }
                    else {
                        Metronic.unblockUI('.page-container');
                        toastr.error('দুঃখিত কিছুক্ষণ পর আবার চেষ্টা করুন।');
                    }
                });
        },
        assignDesignation: function (row) {
             Metronic.blockUI({
                target: '.page-container',
                boxed: true,
                message: 'অপেক্ষা করুন'
            });
             var row_id = $(row).attr('id');
             var org_checkbox = $("#" + row_id).find("input[type=checkbox]");
            var designation_id = org_checkbox[0].id;
            bootbox.dialog({
                message: "আপনি কি নিশ্চিত পদবিতে নির্বাচিত কর্মকর্তাকে দায়িত্ব অর্পণ করতে?",
                title: "কর্মকর্তা যোগদান",
                buttons: {
                    success: {
                        label: "হ্যাঁ",
                        className: "green",
                        callback: function () {
                           EmployeeAssignment.confirmDesignation(row);
                        }
                    },
                    danger: {
                        label: "না",
                        className: "red",
                        callback: function () {
                            Metronic.unblockUI('.page-container');
                        }
                    }
                }
            });
        },
        confirmDesignation: function (row) {
            var row_id = $(row).attr('id');

            var employee_record_id = $("#employee-record-id").val();
            if(employee_record_id=='' || typeof(employee_record_id) == 'undefined'){
                $("#employee-identity-no").focus();
                toastr.error("কর্মকর্তা বাছাই করা হয়নি, দয়া করে কর্মকর্তা বাছাই করুন।" );
                return;
            }
            var identification_number = $("#employee-identity-no").val();
            var office_id = $("#office-id").val();

            var org_checkbox = $("#" + row_id).find("input[type=checkbox]");
            var incharge_type = $("#" + row_id).find("select[name=incharge_label]").val();
            var joining_date = $("#" + row_id).find("input[name=joining_date]").val();
            var office_unit_id = org_checkbox[0].dataset.unitId;
            var designation_name = org_checkbox[0].dataset.designationName;
            var unit_name = org_checkbox[0].dataset.unitName;
            var office_name = org_checkbox[0].dataset.officeName;
            var designation_id = org_checkbox[0].id;
            if(joining_date=='' || typeof(joining_date) == 'undefined'){
               $("#" + row_id).find("input[name=joining_date]").focus();
                toastr.error("যোগদানের কার্যদিবস দেওয়া হয়নি।" );
                return;
            }
            var data = {
                'employee_record_id': employee_record_id,
                'identification_number': identification_number,
                'office_id': office_id,
                'office_unit_id': office_unit_id,
                'office_unit_organogram_id': designation_id,
                'designation': designation_name,
                'incharge_label': incharge_type,
                'joining_date': joining_date,
                'office_name': office_name,
                'unit_name': unit_name,
            };

            PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + 'employeeRecords/assignDesignation',
                data, 'json',
                function (response) {
                    if (response == 1) {
                        toastr.success('পদবিতে নির্বাচিত কর্মকর্তাকে দায়িত্ব অর্পণ করা হয়েছে।');
                        org_checkbox.prop("disabled", true);
                        $("#" + row_id + " .selection_unit").toggle();
                        Metronic.unblockUI('.page-container');
                        EmployeeManagement.getEmployeeJobStatus();
                        EmployeeAssignment.getUnitOrganogramByOfficeId($("#office-id").val());
                    }
                    else {
                        toastr.success('পদবিত নির্বাচন করা সম্ভব হচ্ছে না। কিছুক্ষণ পর পুনরায় চেষ্টা করুন।');
                        Metronic.unblockUI('.page-container');
                        EmployeeManagement.getEmployeeJobStatus();
                        EmployeeAssignment.getUnitOrganogramByOfficeId($("#office-id").val());
                    }
                });
        }

    };

    $(function () {
        
        if($("#office-id").val()>0){
            EmployeeAssignment.getUnitOrganogramByOfficeId($("#office-id").val());
        }
        
        $("#office-ministry-id").bind('change', function () {
            EmployeeAssignment.loadMinistryWiseLayers();
        });
        $("#office-layer-id").bind('change', function () {
            EmployeeAssignment.loadMinistryAndLayerWiseOfficeOrigin();
        });
        $("#office-origin-id").bind('change', function () {
            EmployeeAssignment.loadOriginOffices();
        });
        $("#office-id").bind('change', function () {
            EmployeeAssignment.getUnitOrganogramByOfficeId($(this).val());

        });
        $("#officeUnitTransfer").bind('click', function () {
            EmployeeAssignment.transferOrganogram();
        });
        $(".selection_unit").hide();
    });


    var initTable1 = function () {
        var table = $('#organogramTable');

        var oTable = table.dataTable({

            loadingMessage: 'লোড করা হচ্ছে...',
            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "emptyTable": "কোনো তথ্য নেই",
                "info": "<span class='seperator'>|</span>মোট _TOTAL_ টি তথ্য পাওয়া গেছে",
                "infoEmpty": "কোনো তথ্য নেই",
                "infoFiltered": "<span class='seperator'>|</span>মোট _TOTAL_ টি তথ্য পাওয়া গেছে",
                "search": "খুঁজুন  ",
                "zeroRecords": "কোনো তথ্য নেই"
            },
            "order": [
                [1, 'asc']
            ],

            "serverSide": false,
            "paging":   false,
            "ordering": false,
            "info":     false,
            "columnDefs": [
                { "visible": false, "targets": 2 }
            ],
            "drawCallback": function ( settings ) {
                var api = this.api();
                var rows = api.rows( {page:'current'} ).nodes();
                var last=null;

                api.column(2, {page:'current'} ).data().each( function ( group, i ) {
                    if ( last !== group ) {
                        $(rows).eq( i ).before(
                            '<tr class="group"><td colspan="2" style="font-weight:bold;">'+group+'</td></tr>'
                        );

                        last = group;
                    }
                } );
            },
            "bStateSave": false,
            // set the initial value
            "pageLength": -1,
            "dom": "<'row'<'col-md-12 col-sm-12'>r><'table-scrollable table table-bordered table-stripped't>"
        });
    }
    function reloadPicker(){
    $('.date-picker').datepicker({
//                rtl: Metronic.isRTL(),
                orientation: "left",
                autoclose: true
            });
        }
</script>