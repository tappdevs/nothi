<div class="row">
    <div class="col-md-12">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-history"></i><?php echo __("Work_history") ?>
                </div>
                <div class="actions">
                    <a class="btn btn-sm yellow" id="btn-history-print" href="javascript;"><i class="fa fa-print"></i>প্রিন্ট</a>
                </div>
            </div>

            <div class="portlet-body">
                <div class="row">
                    <div class="col-md-12">
                        <div id="loding"></div>
                        <div id="search-result">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?=$this->request->webroot?>assets/global/scripts/printThis.js"></script>
<script type="text/javascript">

    var EmployeeManagement = {
        getEmployeeJobStatus: function () {
            $("#loding").html("<img src='<?php echo CDN_PATH ?>assets/global/img/loading-spinner-grey.gif' alt='' class='img-responsive' />");
            PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + 'employeeRecords/getEmployeeOwnRoleHistories',
                {}, 'json',
                function (response) {
                    $("#loding").html("");
                    var office_records = response.office_records;
                    var data = '<u><h2 class="text-center">'+response.name+ ' ('+ enTobn(response.username) +')' + ': কর্ম ইতিহাস' +'</h2></u>' +
                        '<table class="table table-striped">' +
                        '                            <thead>' +
                        '                            <tr>' +
                        '                                <th>পদবি</th>' +
                        '                                <th>শাখা</th>' +
                        '                                <th>অফিস</th>' +
                        '                                <th>সময়কাল</th>' +
                        '                            </tr>' +
                        '                            </thead>' +
                        '                            <tbody>';

                    $.each(office_records, function (i,record) {
                        data+= '<tr>';
                        data+= '<td>'+ record.designation +((!isEmpty(record.incharge_label))?' <span style="font-size: 12px">('+record.incharge_label+')</span>':'')+ '</td>';
                        data+= '<td>'+record.unit_name + '</td>';
                        data+= '<td>'+record.office_name+ '</td>';
                        if (record.last_office_date == null) {
                            record.last_office_date = "বর্তমান";
                        }
                        if (record.joining_date == null) {
                            record.joining_date = "জয়েন করার তারিখ পাওয়া যায় নি"
                        }
                        data+= '<td>'+enTobn(record.joining_date) +' - '+ enTobn(record.last_office_date) + '</td>';
                        data+= '</tr>';
                    });
                    data+= '</tbody></table>';
                    $("#search-result").html(data);

                });
        }
    };
    $(function () {
        EmployeeManagement.getEmployeeJobStatus();
    });
    $("#profile_information").click(function (e) {
        e.preventDefault();
        $(".profileli a").click();
    });
        $("#btn-history-print").click(function (e) {
            e.preventDefault();
            $(document).find('#search-result').printThis({
                importCSS: true,
                debug: false,
                importStyle: true,
                printContainer: false,
                pageTitle: "",
                removeInline: false,
                header: null
            });
    });
</script>