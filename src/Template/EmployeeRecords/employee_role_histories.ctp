<div class="row">
    <div class="col-md-12">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fs1 a2i_gn_edit2"></i><?php echo __("Employeer") ?> <?php echo __("Roler") ?> <?php echo __("Histories") ?>
                </div>
            </div>

            <div class="portlet-body">
                <?= $employeeRoleHistoryCell = $this->cell('EmployeeRoleHistories'); ?>
            </div>
        </div>
    </div>
</div>