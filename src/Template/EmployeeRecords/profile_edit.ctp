
<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">প্রোফাইল তথ্য </div>

    </div>
    <div class="portlet-body">
        <?php
        echo $this->Form->create($entity, array('id' => 'EmployeeRecordForm'));
        echo $this->Form->hidden('id');
        ?>
        <div class="form-body">
            <div class="row">
                <div class="col-md-6 form-group form-horizontal">
                    <label class="control-label">নাম <span class="text-danger">*</span></label>
                    <?= $this->Form->input('name_bng',
                        ['label' => false, 'class' => 'form-control', 'disabled' => true, 'type' => 'text', 'placeholder' => 'নাম(বাংলা)']); ?>
                </div>
                <div class="col-md-6 form-group form-horizontal">
                    <label class="control-label">&nbsp;</label>
<?= $this->Form->input('name_eng',
    ['label' => false, 'class' => 'form-control', 'disabled' => true, 'type' => 'text', 'placeholder' => 'নাম(ইংরেজি)']); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 form-group form-horizontal">
                    <label class="control-label">পিতার নাম </label>
<?php echo $this->Form->input('father_name_bng',
    array('label' => false, 'class' => 'form-control', 'placeholder' => 'পিতার নাম(বাংলা)')); ?>
                </div>

                <div class="col-md-6 form-group form-horizontal">
                    <label class="control-label">&nbsp;</label>
<?php echo $this->Form->input('father_name_eng',
    array('label' => false, 'class' => 'form-control', 'placeholder' => 'পিতার নাম(ইংরেজি)')); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 form-group form-horizontal">
                    <label class="control-label">মাতার নাম </label>
                    <?php echo $this->Form->input('mother_name_bng', array('label' => false, 'class' => 'form-control', 'placeholder' => 'মাতার নাম(বাংলা)')); ?>
                </div>

                <div class="col-md-6 form-group form-horizontal">
                    <label class="control-label">&nbsp;</label>
<?php echo $this->Form->input('mother_name_eng',
    array('label' => false, 'class' => 'form-control', 'placeholder' => 'মাতার নাম(ইংরেজি)')); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 form-group form-horizontal">

                    <label class="control-label">জন্ম তারিখ <span class="text-danger">*</span></label>
                    <div class="input-group">
                        <?php
                            if(!empty($entity->date_of_birth)){
                                $time =$entity->date_of_birth;
                                $dob = $time->format('Y-m-d');
                            }
                        ?>
                    <?= $this->Form->input('date_of_birth',
                        ['label' => false, 'class' => 'form-control date-picker', 'disabled' => true, 'type' => 'text', 'readonly' => false,
                        'value' => !empty($dob) ? $dob : '']); ?>

                    </div>
                </div>
                <div class="col-md-3 form-group form-horizontal">
                    <label class="control-label">জাতীয় পরিচয়পত্র নম্বর <span class="text-danger">*</span></label>
                    <?php echo $this->Form->input('nid',
                        array('label' => false, 'type' => 'text', 'class' => 'form-control', 'disabled' => true, 'placeholder' => 'জাতীয় পরিচয়পত্র নম্বর')); ?>
                    <span class="help-block font-red"><b>*</b> জাতীয় পরিচয়পত্র নম্বর ১৭ সংখ্যার  হতে হবে। প্রথম চার সংখ্যা  জন্মসন । </span>
                </div>
                <div class="col-md-3 form-group form-horizontal">
                    <label class="control-label">জন্ম সনদ নম্বর</label>
                    <?php echo $this->Form->input('bcn',
                        array('label' => false, 'class' => 'form-control', 'placeholder' => 'জন্ম সনদ নম্বর')); ?>
                </div>
                <div class="col-md-3 form-group form-horizontal">
                    <label class="control-label">পাসপোর্ট নম্বর</label>
                    <?php echo $this->Form->input('ppn',
                        array('label' => false, 'class' => 'form-control', 'placeholder' => 'পাসপোর্ট নম্বর')); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 form-group form-horizontal">
                    <label class="control-label">লিঙ্গ</label>
                    <?php
                    $options = array(
                        "1" => "পুরুষ",
                        "2" => "মহিলা",
                        "3" => "অন্যান্য"
                    );
                    echo $this->Form->input('gender',
                        array('empty' => __('Select'), 'type' => 'select', 'label' => false, 'class' => 'form-control',
                        'placeholder' => 'লিঙ্গ', 'options' => $options));
                    ?>
                </div>
                <div class="col-md-3 form-group form-horizontal">
                    <label class="control-label">ধর্ম</label>
                    <?php
                    $options = array(
                        "Islam" => "ইসলাম",
                        "Hindu" => "হিন্দু",
                        "Christian" => "খ্রিষ্টান",
                        "Buddhist" => "বৌদ্ধ",
                        "Others" => "অন্যান্য"
                    );
                    echo $this->Form->input('religion',
                        array('empty' => __('Select'), 'type' => 'select', 'label' => false, 'class' => 'form-control',
                        'placeholder' => 'ধর্ম', 'options' => $options));
                    ?>
                </div>
                <div class="col-md-3 form-group form-horizontal">
                    <label class="control-label">রক্তের গ্রুপ</label>
                    <?php
                    $options = array(
                        "A+" => "A+",
                        "A-" => "A-",
                        "B+" => "B+",
                        "B-" => "B-",
                        "O+" => "O+",
                        "O-" => "O-",
                        "AB+" => "AB+",
                        "AB-" => "AB-"
                    );
                    echo $this->Form->input('blood_group',
                        array('empty' => __('Select'), 'type' => 'select', 'label' => false, 'class' => 'form-control',
                        'placeholder' => 'রক্তের গ্রুপ', 'options' => $options));
                    ?>
                </div>
                <div class="col-md-3 form-group form-horizontal">
                    <label class="control-label">বৈবাহিক অবস্থা</label>
                    <?php
                    $options = array(
                        "Single" => "Single",
                        "Married" => "Married",
                        "Widowed" => "Widowed",
                        "Separated" => "Separated"
                    );
                    echo $this->Form->input('marital_status',
                        array('empty' => __('Select'), 'type' => 'select', 'label' => false, 'class' => 'form-control',
                        'placeholder' => 'বৈবাহিক অবস্থা', 'options' => $options));
                    ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3 form-group form-horizontal">
                    <label class="control-label"> ব্যক্তিগত ই-মেইল <span class="text-danger">*</span></label>
        <?php echo $this->Form->input('personal_email',
            array('label' => false, 'class' => 'form-control', 'placeholder' => 'ই-মেইল', 'required'=>true)); ?>
                </div>
                <div class="col-md-3 form-group form-horizontal">
                    <label class="control-label"> ব্যক্তিগত মোবাইল নম্বর <span class="text-danger">*</span></label>
<?php echo $this->Form->input('personal_mobile',
    array('label' => false, 'class' => 'form-control', 'placeholder' => 'মোবাইল')); ?>
                </div>
                <div class="col-md-3 form-group form-horizontal">
                    <label class="control-label">বিকল্প মোবাইল নম্বর </label>
<?php echo $this->Form->input('alternative_mobile',
    array('label' => false, 'class' => 'form-control', 'placeholder' => 'বিকল্প মোবাইল ')); ?>
                </div>
                <div class="col-md-3 form-group form-horizontal">

            <label class="control-label">লগইন নেম</label>
            <?= $this->Form->input('user_alias', ['label' => false, 'class' => 'form-control', 'type' => 'text', 'placeholder' => 'লগইন নেম','minLength'=>4,'maxLength'=>50]); ?>
        
                </div>
            </div>
        </div>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-4 col-md-9">
                    <input type="button" class="btn   blue" value="<?php echo __("Submit"); ?>" onclick="buttonSumbit()"/>
                    <button type="reset" class="btn   red" onclick="profileShow();"><?php echo __(RESET); ?></button>
                </div>
            </div>
        </div>
<?php echo $this->Form->end(); ?>
    </div>
</div>
<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/jQuery-Mask/jquery.mask.min.js"></script>
<script>
    $(document).ready(function(){
        $(document).find('.date-picker').datepicker({
            rtl: Metronic.isRTL(),
            autoclose: true,
            format: "yyyy-mm-dd"

    });

    $('#user-alias').mask('AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA'
      ,{
        translation:{
          A: {pattern: /[a-zA-Z0-9]/},
        },
        placeholder: "স্পেশাল ক্যারেক্টার ছাড়া লগইন নেম লিখুন"
      });
    $('#personal-mobile').mask('AAAAAAAAAAA'
      ,{
        translation:{
          A: {pattern: /[\u09E6-\u09EF0-9]/},
        },
        placeholder: "01XXXXXXXXX"
      });
    $('#alternative-mobile').mask('AAAAAAAAAAA'
      ,{
        translation:{
          A: {pattern: /[\u09E6-\u09EF0-9]/},
        },
        placeholder: "01XXXXXXXXX"
      });
  });
  function buttonSumbit(){
    /*if ($("#nid").val().length != 17 && $("#nid").val().length != 10) {
        toastr.error(' জাতীয় পরিচয়পত্র নম্বর ১০ অথবা ১৭ সংখ্যার হতে হবে। ');
        return false;
    }
    if ($("#date-of-birth").val().length == 0) {
        toastr.error(' জন্ম তারিখ দেওয়া হয়নি । ');
        return false;
    }*/
    if(($("#personal-email").val().length == 0) || (regex.test($("#personal-email").val()) == false) ){
      toastr.error("দুঃখিত! ইমেইল সঠিক নয়");
      return false;
    }
    if($('#personal-mobile').val().length !=11){
      toastr.error("দুঃখিত! ব্যক্তিগত মোবাইল নম্বর সঠিক নয়");
      return false;
    }else {
      $('#personal-mobile').val(bnToen($('#personal-mobile').val()));
    }

    if( !isEmpty($('#alternative-mobile').val()) && $('#alternative-mobile').val().length !=11){
      toastr.error("দুঃখিত! বিকল্প মোবাইল নম্বর সঠিক নয়");
      return false;
    }else {
      if (!isEmpty($('#alternative-mobile').val())) {
        $('#alternative-mobile').val(bnToen($('#alternative-mobile').val()));
      }
    }
    if((!isEmpty($("#user-alias").val()) && $("#user-alias").val().length <3) || (!isEmpty($("#user-alias").val()) && $("#user-alias").val().length >32)){
      toastr.error("দুঃখিত! লগইন নেম স্পেশাল ক্যারেক্টার ছাড়া ও ৩-৩২ ক্যারেক্টারের মধ্যে হতে হবে।");
      return false;
    }
    $.ajax({
      url: $('#EmployeeRecordForm').attr('action'),
      method: 'post',
      data: $('#EmployeeRecordForm').serialize(),
      cache: false,
      success: function (res) {
        if (res == 1) {
          toastr.success("তথ্য সংশোধিত হয়েছে");
          window.location.href = '<?= $this->Url->build(['controller' => 'EmployeeRecords', 'action' => 'myProfile']) ?>';
        } else {
          if(typeof(res.personal_mobile)!='undefined'){
            toastr.error(res.personal_mobile._empty)  ;
          }
          /*if(typeof(res.name_eng)!='undefined'){
              toastr.error(res.name_eng._empty)  ;
          }
          if(typeof(res.name_bng)!='undefined'){
              toastr.error(res.name_bng._empty)  ;
          }
          if(typeof(res.date_of_birth)!='undefined'){
              toastr.error(res.date_of_birth._empty)  ;
          }*/
          if(typeof(res.gender)!='undefined'){
            toastr.error(res.gender._empty)  ;
          }
          if(typeof(res.religion)!='undefined'){
            toastr.error(res.religion._empty)  ;
          }
          if (typeof (res.error) != 'undefined')
          {
            toastr.error(res.error);
          }
        }
      }
    });
    return false;
  }
  var regex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

    $('#personal-email').change(function () {
        if (this.value != '') {

            if (regex.test(this.value) == false) {
                toastr.error("দুঃখিত! ইমেইল সঠিক নয়");
                $(this).focus();
            }
        }
    });
   
</script>