<script>
    $('body').addClass('page-sidebar-closed');
    $('.page-sidebar-menu').addClass('page-sidebar-menu-closed');
</script>
<link href="<?php echo $this->request->webroot; ?>croppie/croppie.css" rel="stylesheet" type="text/css"/>
<!--<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>-->
<style>
	.upload-demo .upload-demo-wrap,
	.upload-demo .upload-result,
	.upload-demo.ready .upload-msg {
		display: none;
	}
	.upload-demo.ready .upload-demo-wrap {
		display: block;
	}
	.upload-demo.ready .upload-result {
		display: inline-block;
	}
	.upload-demo-wrap {
		width: 400px;
		height: 400px;
		margin: 0 auto;
	}

	.upload-msg {
		text-align: center;
		padding: 50px;
		font-size: 22px;
		color: #aaa;
		width: 260px;
		margin: 50px auto;
		border: 1px solid #aaa;
	}
</style>

<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">প্রোফাইল ছবি পরিবর্তন </div>
	    <div class="actions">
		    <a class="btn btn-sm green" href="<?php echo $this->Url->build(['controller' => 'Dashboard', 'action' => 'dashboard']) ?>"><i class="fa fa-home"></i> হোম </a>
	    </div>
    </div>
    <div class="portlet-body">
        <div class="row">
            <div class="col-md-4" id="previous_image">
                <img id="prev_image" src="<?php echo $this->request->webroot . 'content/'.'Personal/profile/'.h($loggedUser['username']).'.png?token='.sGenerateToken(['file'=>'Personal/profile/' . h($loggedUser['username']) . '.png'],['exp'=>time() + 60*300]) ?>" />
	            <img src="" id="cropped_image" style="display: none;" width="200" height="200" />
            </div>
            <div class="col-md-8 demo-wrap upload-demo" id="editor_canvas">
                <!--<canvas class="js-editorcanvas"></canvas>-->
	            <div class="upload-demo-wrap js-editorcanvas">
		            <div id="upload-demo"></div>
	            </div>
            </div>
        </div>
        <hr/>
        <div class="row">
            <div class="col-md-5">
                <label class="img-upload-label" id="showCroppedImage">
                    <label for="upload" class="btn btn-default">ছবি  বাছাই করুন </label>
	                <input type="file" class="img-upload hidden" id="upload" value="Choose a file" accept="image/*" />
                </label>
                <!--<button type="submit" class="js-export img-export" id="js-export" style="display:none">Submit</button>-->
                <button type="submit" class="upload-result" id="upload-result" style="display:none">Submit</button>

                <button onclick="saveImage()" class="btn btn-success submitbutton"><?= __("Submit") ?></button>
            </div>
            <div class="col-md-7">
            </div>
        </div>
    </div>
</div>

<script src="<?php echo $this->request->webroot; ?>croppie/croppie.js" type="text/javascript"></script>
<script src="<?php echo $this->request->webroot; ?>croppie/upload.js" type="text/javascript"></script>
<script>
    $(document).ready(function() {
        $(".submitbutton").hide();
        $("#js-previewcanvas").css('display', 'none');
        $("#prev_image").css('display', 'initial');

        demoUpload(200, 200);
        $(".cr-boundary").on('click', function() {
            $(".submitbutton").show();
            $("#upload-result").trigger('click');
        });
        $(".cr-boundary").on('drag', function() {
            $(".submitbutton").show();
            $("#upload-result").trigger('click');
        });
    });
    function saveImage() {
        $("#upload-result").trigger('click');
        setTimeout(function() {
            var cropped_image = document.getElementById("cropped_image").src;
            var _url = '<?php echo $this->Url->build(["controller" => "EmployeeRecords","action" => "profileUpload"]); ?>';
            $.ajax({
                type: "POST",
                url: _url,
                data: { img_data:cropped_image },
                cache: false,
                contentType: "application/x-www-form-urlencoded",
                success: function (result) {
                    window.location.href = js_wb_root+"employee_records/myProfile?page=profilephoto";
                }
            });
        }, 1000);
    }

    $('#editor_canvas').bind('DOMNodeInserted DOMNodeRemoved', function() {
        $("#prev_image").hide();
        $("#cropped_image").show();
    });
</script>