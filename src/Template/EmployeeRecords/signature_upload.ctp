<script>
    $('body').addClass('page-sidebar-closed');
    $('.page-sidebar-menu').addClass('page-sidebar-menu-closed');
</script>
<link href="<?php echo $this->request->webroot; ?>croppie/croppie.css" rel="stylesheet" type="text/css"/>
<style>
	.upload-demo .upload-demo-wrap,
	.upload-demo .upload-result,
	.upload-demo.ready .upload-msg {
		display: none;
	}
	.upload-demo.ready .upload-demo-wrap {
		display: block;
	}
	.upload-demo.ready .upload-result {
		display: inline-block;
	}
	.upload-demo-wrap {
		width: 400px;
		height: 400px;
		margin: 0 auto;
	}

	.upload-msg {
		text-align: center;
		padding: 50px;
		font-size: 22px;
		color: #aaa;
		width: 260px;
		margin: 50px auto;
		border: 1px solid #aaa;
	}
    fieldset.scheduler-border {
        border: 1px groove #ddd !important;
        padding: 0 1.4em 1.4em 1.4em !important;
        margin: 0 0 1.5em 0 !important;
        -webkit-box-shadow:  0px 0px 0px 0px #000;
        box-shadow:  0px 0px 0px 0px #000;
    }

    legend.scheduler-border {
        font-size: 1.2em !important;
        font-weight: bold !important;
        text-align: left !important;
        width:auto;
        padding:0 10px;
        border-bottom:none;
    }
</style>
<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">স্বাক্ষর  সেটিংস </div>
	    <div class="actions">
		    <a class="btn btn-sm green" href="<?php echo $this->Url->build(['controller' => 'Dashboard', 'action' => 'dashboard']) ?>"><i class="fa fa-home"></i> হোম </a>
	    </div>
    </div>
    <div class="portlet-body">
        <div class="row text-center font-lg">
            <div class="form-check form-check-inline">
                <label class="form-check-label  bold" for="inlineCheckbox0">স্বাক্ষর ধরনঃ</label>
              
                <input class="form-check-input" name="rcrd"  id="rcrd_id" type="hidden" value="<?= $employee_record['id']?>" >
                <input class="form-check-input" name="nid"  id="nid" type="hidden" value="<?= $employee_record['nid']?>" >
                <input class="form-check-input default_sign" name="default_sign"  id="electronic" type="radio" value="<?= ($employee_record['default_sign'] == 0)?1: 0?>" <?= ($employee_record['default_sign'] == 0)?'checked': ''?>>
                <label class="form-check-label  bold" for="inlineCheckbox1"> ইলেক্ট্রনিক </label>
                  <?php
                if($employee_record['soft_signature']  == 1){
                    ?>
                <input class="form-check-input default_sign" name="default_sign" id="soft_signature" type="radio" value="<?= ($employee_record['default_sign'] == 1)?1: 0?>" <?= (intval($employee_record['default_sign']) == 1)?'checked': ''?>>
                <label class="form-check-label  bold" for="inlineCheckbox2"> ডিজিটাল সফট টোকেন  </label>
                <?php
                }
                ?>
                  <?php
                if($employee_record['hard_signature']  == 1){
                    ?>
                <input class="form-check-input default_sign" id="hard_signature" name="default_sign"  type="radio" value="<?= ($employee_record['default_sign'] == 2)?1: 0?>" <?= (intval($employee_record['default_sign']) == 2)?'checked': ''?>>
                <label class="form-check-label bold" for="inlineCheckbox3"> ডিজিটাল হার্ড  টোকেন  </label>
                <?php
                }
                ?>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-5" id="previous_image">
                <?php
                $url =$this->Url->build(['_name'=>'getSignature',$user['username'],0,'?'=>['token'=>sGenerateToken(['file'=>$user['username']],['exp'=>time() + 60*300])]], true);
                ?>
                <img  id="prev_image" src="<?php echo $url ?>" alt="signature"/>
                <!--<canvas class="js-previewcanvas" id="js-previewcanvas" style="display:none;">Your browser does not support the HTML5 canvas tag.</canvas>-->
	            <img src="" id="cropped_image" style="display: none;" width="150" height="50" />
            </div>
            <div class="col-md-7 demo-wrap upload-demo" id="editor_canvas">
                <!--<canvas class="js-editorcanvas"></canvas>-->
	            <div class="upload-demo-wrap js-editorcanvas">
		            <div id="upload-demo"></div>
	            </div>
            </div>
        </div>
        <hr/>
        <div class="row">
            <div class="col-md-4">
                <label class="img-upload-label" id="showCroppedImage">
	                <label for="upload" class="btn btn-default">স্বাক্ষর বাছাই করুন </label>
	                <input type="file" class="img-upload hidden" id="upload" value="Choose a file" accept="image/*" />
                </label>
                <!--<button type="submit" class="js-export img-export" id="js-export" style="display:none"><?/*= __("Submit") */?></button>-->
	            <button type="submit" class="upload-result" id="upload-result" style="display:none">Submit</button>


                <button onclick="saveImage()" class="btn btn-success submitbutton"><?= __("Submit") ?></button>
            </div>
            <div class="col-md-8"></div>
        </div>
    </div>
</div>
<div id="signatureSetting" class="modal fade modal-purple" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">ডিজিটাল সিগনেচার সংযুক্তিকরণ</h4>
      </div>
      <div class="modal-body">
          <div class="form-horizontal">
               <div class="form-group">
                      <label class="control-label col-sm-3 col-md-3"> স্বাক্ষরের মাধ্যমঃ </label>
                      <div class="col-sm-9 col-md-9">
                          <select class="form form-control" name="ProofType"  id="ProofType" >
                          <option value="0"> --বাছাই করুন--</option>
                          <option value="1">জাতীয় পরিচয়পত্র </option>
                          <option value="2">পাসপোর্ট </option>
                          <option value="3">আয়কর সনাক্তকরণ </option>
                      </select>
                      </div>
                </div>
              <div class="form-group">
                    <label class="control-label col-sm-3 col-md-3"> নং </label>
                    <div class="col-sm-9 col-md-9">
                        <input class="form-check-input" name="ProofNo"  id="ProofNo" type="text" value="" >
                    </div>
              </div>
              <div class="form-group">
                  <div class="col-sm-3 col-md-3">
                  </div>
                    <div class="col-sm-9 col-md-9">
                        <button type="button" class="btn btn-primary btn-sm" onclick="digitalSignature.getUserSignature()"><i class="efile-approval2"></i> ডিজিটাল সিগনেচার তথ্য লোড করুন </button>
                    </div>
              </div>
        </div>
          <hr>
              <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                  <div class="panel panel-default">
                      <div class="panel-heading" role="tab" id="headingThree">
                          <h4 class="panel-title">
                              <a class="collapsed DS-file-attach" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                  সার্টিফিকেট ফাইল সংযুক্তিকরণ <i class="glyphicon glyphicon-chevron-down"></i>
                              </a>
                          </h4>
                      </div>
                      <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                          <div class="panel-body">
                              <div class="col-md-12 col-sm-12 form-group">
                                  <label class="control-label col-sm-5 col-md-5"> স্বাক্ষরের মাধ্যমঃ </label>
                                  <div class="col-sm-7 col-md-7">
                                      <select class="form form-control" name="identityProfType"  id="identityProfType" >
                                          <option value="0"> --বাছাই করুন--</option>
                                          <option value="1">জাতীয় পরিচয়পত্র </option>
                                          <option value="2">পাসপোর্ট </option>
                                          <option value="3">আয়কর সনাক্তকরণ </option>
                                      </select>
                                  </div>
                              </div>

                              <div class="col-md-12 col-sm-12 form-group">
                                  <label class="control-label col-sm-5 col-md-5"> নং </label>
                                  <div class="col-sm-7 col-md-7">
                                      <input class="form-check-input" name="identityProofNo"  id="identityProofNo" type="text" value="" >
                                  </div>
                              </div>
                              <div class="col-md-12 col-sm-12 form-group">
                                  <label class="control-label col-sm-5 col-md-5"> সার্টিফিকেট ফাইলঃ </label>
                                  <div class="col-sm-7 col-md-7">
                                      <input class="form-check-input" name="cert-file"  id="cert-file" type="file">
                                  </div>
                              </div>

                              <div class="col-md-12 col-sm-12 form-group">
                                  <label class="control-label col-sm-5 col-md-5"> সার্টিফিকেট সরবরাহকারী প্রতিষ্ঠানের নাম  </label>
                                  <div class="col-sm-7 col-md-7">
                                      <select class="form form-control" name="caName"  id="caName" >
                                          <option value="0"> --বাছাই করুন--</option>
                                          <option value="mango">Mango</option>
                                          <option value="banglaphone">Bangla Phone </option>
                                          <option value="datasoft">Data Soft</option>
                                          <option value="bcc">BCC</option>
                                          <option value="dohatech">Doha Tech</option>
                                      </select>
                                  </div>
                              </div>
                              <div class="col-md-12 col-sm-12 form-group">
                                  <label class="control-label col-sm-5 col-md-5"> সিরিয়াল নাম্বার </label>
                                  <div class="col-sm-7 col-md-7">
                                      <input class="form-check-input" name="serialNumber"  id="serialNumber" type="text">
                                  </div>
                              </div>
                              <div class="col-md-12 col-sm-12 form-group">
                                  <label class="control-label col-sm-5 col-md-5"> মেয়াদউত্তীর্ণের সাল </label>
                                  <div class="col-sm-7 col-md-7">
                                      <select class="form form-control" name="certificateValidity"  id="certificateValidity" >
                                          <option value="0"> --বাছাই করুন--</option>
                                          <option value="1">১</option>
                                          <option value="2">২ </option>
                                      </select>
                                  </div>
                              </div>

                              <div class="col-md-12 col-sm-12 form-group">
                                  <label class="control-label col-sm-5 col-md-5"> ইস্যুর তারিখ </label>
                                  <div class="col-sm-7 col-md-7">
                                      <input class="form-check-input" name="issueDate"  id="issueDate" value="<?= date("m/d/Y") ?>" type="text">
                                  </div>
                              </div>

                              <div class="col-md-12 col-sm-12 form-group">
                                  <label class="control-label col-sm-5 col-md-5"> পাসওয়ার্ড </label>
                                  <div class="col-sm-7 col-md-7">
                                      <input class="form-check-input" name="password"  id="password" type="password">
                                  </div>
                              </div>

                              <div class="col-md-12 col-sm-12 form-group">
                                  <label class="control-label col-sm-5 col-md-5">পাসওয়ার্ড নিশ্চিতকরণ</label>
                                  <div class="col-sm-7 col-md-7">
                                      <input class="form-check-input" name="cnfm_password"  id="cnfm_password" type="password">
                                  </div>
                              </div>

                              <div class="row text-center">
                                  <button class="btn btn-primary btn-sm" onclick="setSignatureByCertFile()" style="display: none" id="cert-file-use">সার্টিফিকেট ফাইল ব্যবহার করুন</button>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          <div class="row">
              <table class="table table-bordered table-bordered" hidden id="showSignOptions">
                  <thead>
                      <tr class="header">
                          <td colspan="4"> ডিজিটাল সিগনেচার সম্পর্কিত তথ্য (নিম্নোক্ত তথ্য হতে ডিফল্ট সিগনেচার নির্বাচন করুন)
                          </td>
                      </tr>
                      <tr class="header">
                          <td> কর্তৃপক্ষ </td>
                          <td> সিরিয়াল নাম্বার  </td>
                          <td> ধরন </td>
                          <td> <?= __('Actions')?></td>
                      </tr>
                  </thead>
                  <tbody id="availableSignatureBody">
                      
                  </tbody>
              </table>
          </div>
         
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal"> বন্ধ করুন </button>
      </div>
    </div>

  </div>
</div>
<script src="<?php echo $this->request->webroot; ?>croppie/croppie.js" type="text/javascript"></script>
<script src="<?php echo $this->request->webroot; ?>croppie/upload.js" type="text/javascript"></script>
<script>
    var api = 0;
    $(document).ready(function() {
        $(".submitbutton").hide();
        $("#js-previewcanvas").css('display', 'none');
        $("#prev_image").css('display', 'initial');

        demoUpload(150, 50);
        $(".cr-boundary").on('click', function() {
            $(".submitbutton").show();
            $("#upload-result").trigger('click');
        });
        $(".cr-boundary").on('drag', function() {
            $(".submitbutton").show();
            $("#upload-result").trigger('click');
        });
    });
    function saveImage() {
        $("#upload-result").trigger('click');
        setTimeout(function() {
            var cropped_image = document.getElementById("cropped_image").src;
            var _url = '<?php echo $this->Url->build(["controller" => "EmployeeRecords","action" => "signatureUpload"]); ?>';
            $.ajax({
                type: "POST",
                url: _url,
                data: { img_data:cropped_image },
                cache: false,
                contentType: "application/x-www-form-urlencoded",
                success: function (result) {
                    window.location.href = js_wb_root+"employee_records/myProfile?page=signature";
                }
            });
        }, 1000);
    }
    $('#editor_canvas').bind('DOMNodeInserted DOMNodeRemoved', function() {
        $("#prev_image").hide();
        $("#cropped_image").show();
    });

</script>
<script src="<?php echo $this->request->webroot; ?>js/image_crop/image_crop.js" type="text/javascript"></script>
<script src="<?php echo $this->request->webroot; ?>projapoti-nothi/js/digital_sign.js" type="text/javascript"></script>