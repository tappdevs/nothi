<div class="portlet light">
    <div class="portlet-title">
        <div class="caption showContent"> জাতীয় পরিচয়পত্র নম্বর  সংশোধন
            </div>
    </div>
    <div class="portlet-body form">
        <?php echo $this->Form->create($entity,['method'=>'post','id'=>'EmployeeRecordForm']); ?>

        <div class="row">
            <div class="col-md-3 form-group form-horizontal">
                <label class="control-label">জন্ম তারিখ <span class="text-danger">*</span></label>
                <?php echo $this->Form->input('date_of_birth', array('label' => false, 'class' => 'form-control date-picker', 'type'=>"text", 'placeholder' => 'জন্ম তারিখ','default'=>date("Y-m-d"),'value' => !empty($entity->date_of_birth) ? date('Y-m-d',strtotime($entity->date_of_birth)) : '')); ?>
            </div>
            <div class="col-md-3 form-group form-horizontal">
                <label class="control-label">জাতীয় পরিচয়পত্র নম্বর <span class="text-danger">*</span></label>
                <?php echo $this->Form->input('nid', array('label' => false, 'type' => 'text', 'class' => 'form-control', 'placeholder' => 'জাতীয় পরিচয়পত্র নম্বর','required'=>'required')); ?>
                   <span class="help-block font-red"><b>*</b> জাতীয় পরিচয়পত্র নম্বর ১৭ সংখ্যার  হতে হবে। প্রথম চার সংখ্যা  জন্মসন । </span>
            </div>

        </div>
        <div class="form-actions">
            <button type="submit" id="btn_submit" class="btn green uppercase"><?php echo __(SAVE) ?></button>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
</div>


<script type="text/javascript">
    $(function () {
        $('.date-picker').datepicker({
            rtl: Metronic.isRTL(),
            orientation: "left",
            autoclose: true,
            format: "yyyy-mm-dd"
        });
    });
    
    $("#EmployeeRecordForm").submit(function (event) {
        if ($("#nid").val().length != 17) {
            event.preventDefault();
            toastr.error(' জাতীয় পরিচয়পত্র নম্বর ১৭ সংখ্যার হতে হবে। ');
        }
        if ($("#date-of-birth").val().length == 0) {
            event.preventDefault();
            toastr.error(' জন্ম তারিখ দেওয়া হয়নি । ');
        }

    });
</script>