<div class="portlet light ">
    <div class="portlet-title">
        <div class="caption"><i class="fa fa-table"></i> অফিস ড্যাশবোর্ড দেখার অনুমতিপ্রাপ্ত কর্মকর্তাগণ  </div>
    </div>
    <div class="portlet-body">

        <div id="showlist"></div><br>
        <div class="table-container ">
            <table class="table table-bordered table-hover tableadvance">
                <thead>
                    <tr class="heading">
                        <th class="text-center" >  অনুমতিপ্রাপ্ত </th>
                        <th class="text-center" > নাম </th>
                        <th class="text-center" > পদবি </th>
                    </tr>
                </thead>
                <tbody id="addData" data-url="<?php echo $this->Url->build(['controller'=>'EmployeeRecords','action'=>'viewOfficeDashboard' ]) ?>">
                    <?php
                    if (!empty($all_record)) {
                        foreach ($all_record as $val) {
                             $display = in_array($val['office_unit_organogram_id'], $designation_array);
                            ?>
                            <tr>
                                <td>
                                    <div class="form form-group">
                                        <input type="checkbox" data-designation="<?= $val['office_unit_organogram_id'] ?>" class="form-control permitted" <?=
                                        (($display == 1) ? 'checked' : '')
                                        ?>>
                                    </div>
                                </td>
                                <td> <?= $val['name_bng'] ?></td>
                                <td> <?= $val['designation'] ?> </td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                </tbody>
            </table>
        </div>
        <div class="row text-center">
            <button type="button" class="btn btn-primary" id="svDT"><?= __('Submit') ?></button>
        </div>
    </div>
</div>
<script src="<?php echo CDN_PATH; ?>js/reports/view_office_dashboard.js" type="text/javascript"></script>

