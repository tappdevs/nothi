<?php

use Cake\Core\Configure;

    $this->layout = 'customize_error';
if (Configure::read('debug')):

    $this->assign('title', $message);
    $this->assign('templateName', 'error400.ctp');

    $this->start('file');
    ?>
    <?php if (!empty($error->queryString)) : ?>
    <p class="notice">
        <strong>SQL Query: </strong>
        <?= h($error->queryString) ?>
    </p>
<?php endif; ?>
    <?php if (!empty($error->params)) : ?>
    <strong>SQL Query Params: </strong>
    <?= Debugger::dump($error->params) ?>
<?php endif; ?>
    <?= $this->element('auto_table_warning') ?>
    <?php
    if (extension_loaded('xdebug')):
        xdebug_print_function_stack();
    endif;

    $this->end();
endif;
?>
    <?php $this->start('subheading') ?>
    
    <table>
        <tr>
            <th>
                সমস্যা নম্বর:
            </th>
            <td class="text-left">
                &nbsp;<?php echo Cake\I18n\Number::format($code); ?>
            </td>
        </tr>
        <tr>
            <th style="width:100px;">
                প্রাযুক্তিক বর্ণনা:
            </th>
            <td class="text-left" style="word-break:break-word;word-wrap: break-word;">
                <?= sprintf(  __d('cake', '%s পেজ পাওয়া যায়নি'), $url) ?>
            </td>
        </tr>
    </table>
    <?php $this->end() ?>
