<?php
use Cake\Core\Configure;
use Cake\Error\Debugger;
$this->layout = 'customize_error';
if (Configure::read('debug')):
    
//    $this->assign('title', $message);
    $this->assign('title', 'টেকনিক্যাল ত্রুটি');
    $this->assign('templateName', 'error500.ctp');

    $this->start('file');
    ?>
    <?php if (!empty($error->queryString)) : ?>
    <p class="notice">
        <strong>SQL Query: </strong>
        <?= h($error->queryString) ?>
    </p>
<?php endif; ?>
    <?php if (!empty($error->params)) : ?>
    <strong>SQL Query Params: </strong>
    <?= Debugger::dump($error->params) ?>
<?php endif; ?>
    <?php
    echo $this->element('auto_table_warning');

    if (extension_loaded('xdebug')):
        xdebug_print_function_stack();
    endif;

    $this->end();
endif;
?>
    <?php $this->start('subheading') ?>
    
    <table>
        <tr>
            <th>
                সমস্যা নম্বর:
            </th>
            <td class="text-left">
                &nbsp;<?php echo Cake\I18n\Number::format($code); ?>
            </td>
        </tr>
        <tr >
            <th style="width:100px;">
                প্রাযুক্তিক বর্ণনা:
            </th>
            <td class="text-left" style="word-break:break-word;word-wrap: break-word;">
                <?=Live==1 ? h(makeEncryptedData($message)) : h($message); ?>
            </td>
        </tr>
        <tr >
            <th style="width:100px;">
                করণীয়:
            </th>
            <td class="text-left" style="word-break:break-word;word-wrap: break-word;">
                দয়া করে সাপোর্ট টিমের সাথে <b>প্রয়োজনীয় তথ্য ও সমস্যাটির প্রাযুক্তিক বর্ণনা</b> সহ যোগাযোগ করুন।
            </td>
        </tr>
    </table>
    <?php $this->end() ?>
  