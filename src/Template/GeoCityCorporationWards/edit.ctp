<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption"><i
                class=""></i><?php echo __("City Corporation") ?> <?php echo __("Ward") ?> <?php echo __("Create") ?>
        </div>
        <div class="tools">
            <a  href="<?=
            $this->Url->build(['controller' => 'GeoCityCorporationWards',
                'action' => 'index'])
            ?>"><button class="btn   blue margin-bottom-5"  style="margin-top: -5px;"> সিটি কর্‌পোরেশন ওয়ার্ড  তালিকা  </button></a>
        </div>
        <!-- <div class="tools">
             <a href="javascript:" class="collapse"></a>
             <a href="#portlet-config" data-toggle="modal" class="config"></a>
             <a href="javascript:" class="reload"></a>
             <a href="javascript:" class="remove"></a>
         </div>-->
    </div>
    <div class="portlet-body form"><br><br>
        <?php echo $this->Form->create($geo_city_corporation_ward); ?>
        <?php echo $this->element('GeoCityCorporationWards/add'); ?>
        <?php echo $this->element('update'); ?>
        <?php echo $this->Form->end(); ?>
    </div>
</div>