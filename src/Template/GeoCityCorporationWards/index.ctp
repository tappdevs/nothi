<div class="portlet light">
    <div class="portlet-title">
        <div class="caption"><i
                class="fs1 a2i_gn_details1"></i><?php echo __("City Corporation") ?> <?php echo __("Wardr") ?> <?php echo __("List") ?>
        </div>
        <!--<div class="tools">
            <a href="javascript:" class="collapse"></a>
            <a href="#portlet-config" data-toggle="modal" class="config"></a>
            <a href="javascript:" class="reload"></a>
            <a href="javascript:" class="remove"></a>
        </div>-->
    </div>
    <div class="portlet-window-body">
        <?= $this->Html->link(__('Add New'), ['action' => 'add'], ['class' => 'btn btn-default']) ?><br><br>

        <table class="table table-bordered">
            <thead>
            <tr>
                <th><?php echo __("City Corporation") ?></th>
                <th><?php echo __("District") ?></th>
                <th><?php echo __("Geo Division") ?></th>
                <th><?php echo __("City Wardr BBS Code") ?></th>
                <th><?php echo __("City BBS Code") ?></th>
                <th><?php echo __("BBS Code") ?></th>
                <th><?php echo __("Division BBS Code") ?></th>
                <th><?php echo __("Wardr") ?> <?php echo __("Name Bangla") ?></th>
                <th><?php echo __("Wardr") ?> <?php echo __("Name English") ?></th>
                <th><?php echo __("Status") ?></th>
                <th><?php echo __("Actions") ?></th>
            </tr>
            </thead>
            <tbody>

            <?php
            foreach ($query as $rows):
            ?>
            <tr>
                <td><?php echo $rows['geo_city_corporation']['city_corporation_name_bng']; ?></td>
                <td><?php echo $rows['geo_district']['district_name_bng']; ?></td>
                <td><?php echo $rows['geo_division']['division_name_bng']; ?></td>
                <td><?php echo $rows['bbs_code']; ?></td>
                <td><?php echo $rows['city_corporation_bbs_code']; ?></td>
                <td><?php echo $rows['district_bbs_code']; ?></td>
                <td><?php echo $rows['division_bbs_code']; ?></td>
                <td><?php echo $rows['ward_name_bng']; ?></td>
                <td><?php echo $rows['ward_name_eng']; ?></td>
                <td><?php echo $rows['status']; ?></td>
                <td>
                    <!--  <?= $this->Form->postLink(
                        'Delete',
                        ['action' => 'delete', $rows->id],
                        ['class' => 'btn btn-danger'],
                        ['confirm' => 'Are you sure to delete this item?'])
                    ?>-->
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $rows->id], ['class' => 'btn btn-primary']) ?>
                    <!-- <?= $this->Html->link('View', ['action' => 'view', $rows->id], ['class' => 'btn btn-success']) ?>-->

                </td>
                <?php
                endforeach;
                ?>
            </tr>
            </tbody>
        </table>
         <div class="actions text-center">
            <ul class="pagination pagination-sm">
                <?php
                echo $this->Paginator->last(__('শেষ', true),
                    array('class' => 'number-last'));
                echo $this->Paginator->next('<<',
                    array('tag' => 'li', 'escape' => false),
                    '<a href="#" class="btn btn-sm blue ">&raquo;</a>',
                    array('class' => 'next disabled', 'tag' => 'li', 'escape' => false));
                echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li',
                    'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a',
                    'reverse' => true));
                echo $this->Paginator->prev('>>',
                    array('tag' => 'li', 'escape' => false),
                    '<a href="#" class="btn btn-sm blue">&laquo;</a>',
                    array('class' => 'prev disabled btn btn-sm blue', 'tag' => 'li',
                    'escape' => false));
                echo $this->Paginator->first(__('প্রথম', true),
                    array('class' => 'number-first'));
                ?>
            </ul>

        </div>
    </div>
</div>



