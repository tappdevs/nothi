<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption"><i class="fs1 a2i_gn_edit2"></i><?php echo __("Create New District"); ?></div>
        <div class="tools">
            <a  href="<?=
            $this->Url->build(['controller' => 'GeoDistricts',
                    'action' => 'index'])
            ?>"><button class="btn    blue margin-bottom-10"  style="margin-top: -5px;"> জেলা তালিকা  </button></a>
        </div>
    </div>
    <div class="portlet-body form"><br><br>
        <?php echo $this->Form->create(); ?>
        <?php echo $this->element('GeoDistricts/add_with_select') ?>
        <?php echo $this->element('submit'); ?>
        <?php echo $this->Form->end(); ?>
    </div>
</div>