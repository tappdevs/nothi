<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-toastr/toastr.min.css"/>
<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption"><i class=""></i><?php echo __('Division') ?> <?php echo __('Edit') ?> </div>
        <div class="tools">
            <a  href="<?=
            $this->Url->build(['controller' => 'GeoDivisions',
                'action' => 'index'])
            ?>" id="redirect"><button class="btn blue margin-bottom-10"  style="margin-top: -5px;">  বিভাগ তালিকা  </button></a>
        </div>
    </div>
    <div class="portlet-body form"><br><br>
        <?php echo $this->Form->create($geo_division); ?>
        <?php echo $this->element('GeoDivisions/add'); ?>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-12 text-center">
                    <button class="btn   blue" id="sbt" type="submit"><?php echo __('Submit') ?></button>
                    <button class="btn   default" type="reset"><?php echo __('Reset') ?></button>
                </div>
            </div>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
</div>
<script src="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-toastr/toastr.min.js"></script>
<script src="<?php echo CDN_PATH; ?>assets/admin/pages/scripts/ui-toastr.js"></script>
<script>
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "positionClass": "toast-bottom-right"
    };
    $(document).ready(function () {
        $('#sbt').on('click', function (evt) {
            evt.preventDefault();
            if ($('#name_character').val() == '' || typeof($('#name_character').val())=='undefined') {
                toastr.error(' প্রশাসনিক বিভাগের নাম (বাংলা) দেওয়া হয়নি। ');
                return false;
            }
            if ($('#division-name-eng').val() == '' || typeof($('#division-name-eng').val())=='undefined') {
                toastr.error(' প্রশাসনিক বিভাগের নাম (ইংরেজি) দেওয়া হয়নি।  ');
                return false;
            }
            if ($('#bbs-code').val() == '' || typeof($('#bbs-code').val())=='undefined') {
                toastr.error(' বিভাগ কোড দেওয়া হয়নি। ');
                return false;
            }
              Metronic.blockUI({
                target:  '#ajax-content',boxed: true
            });
            $.ajax({
                type: 'POST',
                url: "<?php
                echo $this->Url->build(['controller' => 'GeoDivisions', 'action' => 'edit', $geo_division['id']])
    ?>",
                data: {"name_character": $('#name_character').val(), "division_name_eng": $('#division-name-eng').val(), "bbs_code": $('#bbs-code').val()},
                success: function (data) {
                    Metronic.unblockUI('#ajax-content');
                    $("#redirect").trigger('click');
                    window.location="<?= $this->Url->build(['controller' => 'GeoDivisions',
        'action' => 'index'])
    ?>";
//                    if (data.status == 1)
//                        toastr.success(' সংরক্ষিত হয়েছে। ');
//                    else
//                        toastr.error(' সংরক্ষিত করা যাচ্ছে না। আবার চেষ্টা করুন। ');
                }
            });
        });
    });
</script>