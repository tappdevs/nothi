<div class="row">
    <div class="col-md-6">
        <div class="portlet green-meadow box">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-cogs"></i><?php echo __(GEOTREE) ?></div>

            </div>
            <div class="portlet-body">
                <div id="geotree">

                </div>
            </div>
        </div>
        <div class="col-md-6" id="geo_desc_div">
        </div>
    </div>

</div>

<script type="text/javascript">
    var GeoTree = {
        gotoEdit: function (input) {
            var id = $(input).attr('data-id');
            var type = $(input).attr('data-type');
            var url = "<?php echo $this->request->webroot;?>";
            if (type == "division") {
                url += "geoDivisions/edit/" + id;
            }
            PROJAPOTI.ajaxSubmitDataCallback(url, "", function (response) {
                $("#geo_desc_div").html(response);
            });
        }
    };

    $(function () {

        $('#geotree').jstree({
            "core": {
                "themes": {
                    "variant": "large"
                },
                'data': {
                    'url': function (node) {
                        return node.id === '#' ?
                            'getDivisionsTree' : 'getDistrictsTree/';
                    },
                    'data': function (node) {
                        return {'id': node.id};
                    }
                }
            }
        });
    });
</script>