<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption"><i class=""></i>পৌরসভা <?= __('Edit') ?></div>
        <div class="tools">
            <a  href="<?=
            $this->Url->build(['controller' => 'GeoMunicipalityWards',
                    'action' => 'index'])
            ?>"><button class="btn   blue margin-bottom-5"  style="margin-top: -5px;"> পৌরসভা ওয়ার্ড  তালিকা  </button></a>
        </div>
    </div>
    <div class="portlet-body form"><br><br>
        <?php echo $this->Form->create($geo_municipality_wards); ?>
        <?php echo $this->element('GeoMunicipalityWards/add'); ?>
        <?php echo $this->element('update'); ?>
        <?php echo $this->Form->end(); ?>
    </div>
</div>
