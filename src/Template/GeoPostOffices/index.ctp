<div class="portlet light">
    <div class="portlet-title">
        <div class="caption"><i class="fs1 a2i_gn_details1"></i><?php echo __('Post Officer') ?> <?php echo __('List') ?> </div>
        <!--<div class="tools">
            <a href="javascript:" class="collapse"></a>
            <a href="#portlet-config" data-toggle="modal" class="config"></a>
            <a href="javascript:" class="reload"></a>
            <a href="javascript:" class="remove"></a>
        </div>-->
    </div>
    <div class="portlet-window-body">
        <?= $this->Html->link(__('Add New'), ['action' => 'add'], ['class' => 'btn btn-default']) ?><br><br>

        <table class="table table-bordered">
            <thead>
            <tr>
                <th class="text-center"><?php echo __('Division') ?></th>
                <th class="text-center"><?php echo __('District') ?></th>
                <th class="text-center"><?php echo __('Upazila') ?></th>
                <th class="text-center"><?php echo __('Thana') ?></th>
                <th class="text-center"><?php echo __('Post Office') ?><?php echo __('Code') ?></th>
                <th class="text-center"><?php echo __('BBS Code') ?></th>
                <th class="text-center"><?php echo __('Post Officer') ?> <?php echo __('Name Bangla') ?></th>
                <th class="text-center"><?php echo __('Post Officer') ?> <?php echo __('Name English') ?></th>
                <th class="text-center"><?php echo __('Status') ?></th>
                <th class="text-center"><?php echo __('Actions') ?></th>
            </tr>
            </thead>
            <tbody>

            <?php
            foreach ($query as $rows):
            ?>
            <tr>
                <td class="text-center"><?php echo $rows['geo_division']['division_name_bng']; ?></td>
                <td class="text-center"><?php echo $rows['geo_district']['district_name_bng']; ?></td>
                <td class="text-center"><?php echo $rows['geo_upazila']['upazila_name_bng']; ?></td>
                <td class="text-center"><?php echo $rows['geo_thana']['thana_name_bng']; ?></td>
                <td class="text-center"><?php echo $rows['bbs_code']; ?></td>
                <td class="text-center"><?php echo $rows['district_bbs_code']; ?></td>
                <td class="text-center"><?php echo $rows['postoffice_name_bng']; ?></td>
                <td class="text-center"><?php echo $rows['postoffice_name_eng']; ?></td>
                <td class="text-center"><?php echo $rows['status']; ?></td>
                <td class="text-center">
                    <!-- <?= $this->Form->postLink(
                        'Delete',
                        ['action' => 'delete', $rows->id],
                        ['class' => 'btn btn-danger'],
                        ['confirm' => 'Are you sure to delete this item?'])
                    ?>-->
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $rows->id], ['class' => 'btn btn-primary']) ?>
                    <!--<?= $this->Html->link('View', ['action' => 'view', $rows->id], ['class' => 'btn btn-success']) ?>-->

                </td>
                <?php
                endforeach;
                ?>
            </tr>
            </tbody>
        </table>
         <div class="actions text-center">
            <ul class="pagination pagination-sm">
                <?php
                echo $this->Paginator->last(__('শেষ', true),
                    array('class' => 'number-last'));
                echo $this->Paginator->next('<<',
                    array('tag' => 'li', 'escape' => false),
                    '<a href="#" class="btn btn-sm blue ">&raquo;</a>',
                    array('class' => 'next disabled', 'tag' => 'li', 'escape' => false));
                echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li',
                    'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a',
                    'reverse' => true));
                echo $this->Paginator->prev('>>',
                    array('tag' => 'li', 'escape' => false),
                    '<a href="#" class="btn btn-sm blue">&laquo;</a>',
                    array('class' => 'prev disabled btn btn-sm blue', 'tag' => 'li',
                    'escape' => false));
                echo $this->Paginator->first(__('প্রথম', true),
                    array('class' => 'number-first'));
                ?>
            </ul>

        </div>
    </div>
</div>
