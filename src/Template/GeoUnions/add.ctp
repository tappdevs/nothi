<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption"><i class="fs1 a2i_gn_edit2"></i><?php echo __("Union"); ?> <?php echo __("Create New"); ?>
        </div>

    </div>
    <div class="portlet-body form"><br><br>
        <?php echo $this->Form->create(); ?>
        <?php echo $this->element('GeoUnions/add_with_select'); ?>
        <?php echo $this->element('submit'); ?>
        <?php echo $this->Form->end(); ?>
    </div>
</div>
