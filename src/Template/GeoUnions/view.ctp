<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption"><i class=""></i><?php echo __('Union') ?> <?php echo __('View') ?></div>

    </div>
    <div class="portlet-body">
        <div class="panel-body">
            <table class="table table-bordered">

                <tr>
                    <td class="text-right"><?php echo __('Union') ?> <?php echo __('Name Bangla') ?></td>
                    <td><?= h($geo_union->union_name_bng) ?></td>
                </tr>
                <tr>
                    <td class="text-right"><?php echo __('Union') ?> <?php echo __('Name English') ?></td>
                    <td><?= h($geo_union->union_name_eng) ?></td>
                </tr>
                <tr>
                    <td class="text-right"><?php echo __('Geo') ?> <?php echo __('Upazila') ?> </td>
                    <td><?= h($geo_union->geo_upazila_id) ?></td>
                </tr>
                <tr>
                    <td class="text-right"><?php echo __('Geo') ?> <?php echo __('District') ?></td>
                    <td><?= h($geo_union->geo_district_id) ?></td>
                </tr>
                <tr>
                    <td class="text-right"><?php echo __('BBS Code') ?></td>
                    <td><?= h($geo_union->bbs_code) ?></td>
                </tr>
                <tr>
                    <td class="text-right"><?php echo __('UBBS Code') ?></td>
                    <td><?= h($geo_union->upazila_bbs_code) ?></td>
                </tr>
                <tr>
                    <td class="text-right"><?php echo __('District BBS Code') ?></td>
                    <td><?= h($geo_union->district_bbs_code) ?></td>
                </tr>
                <tr>
                    <td class="text-right"><?php echo __('Status') ?></td>
                    <td><?= h($geo_union->status) ?></td>
                </tr>

                <tr>
                    <td><?= $this->Html->link('Edit This Union', ['action' => 'edit', $geo_union->id], array('class' => 'btn btn-primary pull-right')) ?></td>
                    <td><?= $this->Html->link('Back to Union List', ['action' => 'index'], array('class' => 'btn btn-primary pull-left')) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>