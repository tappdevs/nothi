<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption"><i class=""></i><?php echo __('Upazila') ?> <?php echo __('Edit') ?></div>
        <div class="tools">
            <a  href="<?=
            $this->Url->build(['controller' => 'GeoUpazilas',
                'action' => 'index'])
            ?>"><button class="btn   blue margin-bottom-5"  style="margin-top: -5px;"> উপজেলা  তালিকা  </button></a>
        </div>
    </div>
    <div class="portlet-body form"><br><br>
        <?php echo $this->Form->create($geo_upazila); ?>
        <?php echo $this->element('GeoUpazilas/add'); ?>
        <?php echo $this->element('update'); ?>
        <?php echo $this->Form->end(); ?>
    </div>
</div>