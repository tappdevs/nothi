<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption"><i class=""></i><?php echo __('View Upazila') ?></div>

    </div>
    <div class="portlet-body">
        <div class="panel-body">
            <table class="table table-bordered">

                <tr>
                    <td class="text-right"><?php echo __('Upazila Name in Bangla') ?></td>
                    <td><?= h($geo_upazila->upazila_name_bng) ?></td>
                </tr>
                <tr>
                    <td class="text-right"><?php echo __('Upazila Name in English') ?></td>
                    <td><?= h($geo_upazila->upazila_name_eng) ?></td>
                </tr>
                <tr>
                    <td class="text-right"><?php echo __('Geo District') ?></td>
                    <td><?= h($geo_upazila->geo_district_id) ?></td>
                </tr>
                <tr>
                    <td class="text-right"><?php echo __('UBBS Code') ?></td>
                    <td><?= h($geo_upazila->bbs_code) ?></td>
                </tr>
                <tr>
                    <td class="text-right"><?php echo __('BBS Code') ?></td>
                    <td><?= h($geo_upazila->district_bbs_code) ?></td>
                </tr>
                <tr>
                    <td class="text-right"><?php echo __('Status') ?></td>
                    <td><?= h($geo_upazila->status) ?></td>
                </tr>

                <tr>
                    <td><?= $this->Html->link(__('Edit'), ['action' => 'edit', $geo_upazila->id], array('class' => 'btn btn-primary pull-right')) ?></td>
                    <td><?= $this->Html->link(__('Back'), ['action' => 'index'], array('class' => 'btn btn-primary pull-left')) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>