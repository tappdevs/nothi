<style>
	fieldset.scheduler-border {
		border: 1px groove #ddd !important;
		padding: 0 10px 0px 10px !important;
		margin: 0 0 1em 0 !important;
		-webkit-box-shadow:  0px 0px 0px 0px #000;
		box-shadow:  0px 0px 0px 0px #000;
	}

	legend.scheduler-border {
		font-size: 1.2em !important;
		font-weight: bold !important;
		text-align: left !important;
		width:auto;
		padding:0 10px;
		border-bottom:none;
	}
</style>
<div class="portlet light">
	<div class="portlet-title">
		<div class="caption"><i class="fa fa-envelope-o"></i> সার্বজনীন বার্তা</div>
		<div class="pull-right"><button class="btn btn-sm btn-success" onclick="$('#createNewMessage').modal('show')"><i class="fa fa-plus"></i> বার্তা প্রদান করুন </button></div>
	</div>
	<div class="portlet-body">
		<?php if (count($messages) > 0): ?>
		<table class="table table-bordered table-hover">
			<thead>
			<tr class="heading">
				<th class="text-center"> ক্রমিক </th>
				<th class="text-center"> শিরোনাম </th>
				<th class="text-center"> বার্তা </th>
				<th class="text-center"> প্রাপক </th>
				<th class="text-center"> প্রাপকের আইডি </th>
				<th class="text-center"> প্রেরক </th>
				<th class="text-center"> পাঠানোর তারিখ </th>
				<th class="text-center"> কার্যক্রম </th>
			</tr>
			</thead>
			<tbody>
			<?php
			$count = 0;
			foreach ($messages as $key => $message) {
				$count++;
				if ($message['message_for']=='all') {
					$message['for'] = 'সবাই';
				} elseif ($message['message_for']=='layer') {
					$message['for'] = 'লেয়ার ভিত্তিক';
				} elseif ($message['message_for']=='office') {
					$message['for'] = 'অফিস ভিত্তিক';
				} elseif ($message['message_for']=='unit') {
					$message['for'] = 'শাখা ভিত্তিক';
				} elseif ($message['message_for']=='organogram') {
					$message['for'] = 'পদবি ভিত্তিক';
				} elseif ($message['message_for']=='office_origin') {
					$message['for'] = 'মৌলিক অফিস ভিত্তিক';
				} elseif ($message['message_for']=='office_layer') {
					$message['for'] = 'অফিসের লেয়ার ভিত্তিক';
				} elseif ($message['message_for']=='office_ministry') {
					$message['for'] = 'মন্ত্রনালয় ভিত্তিক';
				}
				$user = \Cake\ORM\TableRegistry::get('Users')->get($message['message_by']);
				?>
				<tr>
					<td class="text-center"><?= $this->Number->format($count) ?> </td>
					<td class="text-center"><?= $message['title'] ?></td>
					<td class="text-center"><div style="width: 390px;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;"><?= $message['message'] ?></div></td>
					<td class="text-center"><?= $message['for'] ?></td>
					<td class="text-center"><?= enTobn($message['related_id']) ?></td>
					<td class="text-center"><?= $user->username ?></td>
					<td class="text-center"><?= Cake\I18n\Time::parse($message['created']) ?></td>
					<td>
						<div class="btn-group btn-group-round">
							<button class="btn btn-sm btn-danger" onclick="deleteMessage(<?=$message['id']?>)"><i class="fa fa-trash"></i></button>
						</div>
					</td>
				</tr>
				<?php
			}
			?>
			</tbody>
		</table>
		<?=customPagination($this->Paginator)?>
		<?php else: ?>
		<h3>কোনো বার্তা পাওয়া যায় নি</h3>
		<?php endif; ?>
	</div>
</div>
<script>
	function deleteMessage(messageId) {
        bootbox.dialog({
            message: "আপনি কি বার্তাটি মুছে ফেলতে চান?",
            title: "আপনি কি নিশ্চিত ?",
            buttons: {
                success: {
                    label: "হ্যাঁ",
                    className: "green",
                    callback: function () {
                        $.ajax({
                            type: 'POST',
                            url: "<?php echo $this->Url->build(['action' => 'delete']) ?>",
                            data: {"message_id": messageId},
                            success: function (data) {
                                window.location.reload();
                            }
                        });
                    }
                },
                danger: {
                    label: "না",
                    className: "red",
                    callback: function () {
                    }
                }
            }
        });
    }
</script>

<div id="createNewMessage" class="modal fade modal-purple height-auto" data-backdrop="static" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><i class="fa fa-plus"></i> নতুন বার্তা</h4>
			</div>
			<?php echo $this->Form->create('', ['action' => 'create', 'type' => 'post']); ?>
			<div class="modal-body">
				<?php $prefix = 'message_'; ?>
				<fieldset class="scheduler-border">
					<legend class="scheduler-border">প্রাপক নির্বাচন</legend>
					<div class="tabbable-custom ">
						<ul class="nav nav-tabs ">
							<li class="active">
								<a href="#layerWise" data-toggle="tab" aria-expanded="true">স্তরভিত্তিক</a>
							</li>
							<li class="">
								<a href="#ministryWise" data-toggle="tab" aria-expanded="true">মন্ত্রণালয়/দপ্তর ভিত্তিক</a>
							</li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane active" id="layerWise">
								<div class="row">
									<label class="control-label col-md-1 text-right font-lg"><?=__('Layer')?></label>
									<div class="col-md-4">
										<?= $this->Form->input('layer_id',
											array('label' => false, 'type' => 'select', 'class' => 'form-control',
												'options' =>  [0 => __("Select")]  + $options));
										?>
									</div>
									<div class="col-md-2">
										অথবা
									</div>
									<div class="col-md-5">
										<label class="control-label col-md-8 text-right font-lg">সকল ব্যবহারকারী</label>
										<input type="checkbox" value="all" name="message_for" />
									</div>
								</div>
							</div>
							<div class="tab-pane" id="ministryWise">
								<?= $this->cell('OfficeSearch::officeChoose', ['prefix' => $prefix]); ?>
							</div>
						</div>
					</div>
				</fieldset>

				<?php echo $this->Form->input('title', array('label' => 'শিরোনাম', 'type' => 'text', 'class' => 'form-control')); ?>
				<?php echo $this->Form->input('message', array('label' => 'বার্তা', 'type' => 'textarea', 'class' => 'form-control')); ?>
			</div>
			<div class="modal-footer">
				<div class="btn-group btn-group-round">
					<button class="btn btn-sm btn-success" type="submit"><i class="fa fa-reply"></i> <?="বার্তা প্রদান করুন" ?></button>
					<button class="btn btn-sm btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> <?=__('Close') ?></button>
				</div>
			</div>
			<?php echo $this->Form->end(); ?>
		</div>

	</div>
</div>
<script>
	$(function() {
        $(".tabbable-custom").find('.tab-pane').not('.active').find('input, select, radio, checkbox').attr('disabled', true);
        $(".tabbable-custom").find('li').click(function() {
            setTimeout(function(){
                $(".tabbable-custom").find('.tab-pane').not('.active').find('input, select, radio, checkbox').attr('disabled', true);
                $(".tabbable-custom").find('.tab-pane.active').find('input, select, radio, checkbox').attr('disabled', false);
            }, 500);
        });
	})
</script>