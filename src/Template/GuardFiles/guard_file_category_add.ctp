<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class=""></i>গার্ড ফাইলের ধরন
        </div>
	    <div class="tools">
		    <a  href="<?=
            $this->Url->build([
                'action' => 'guardFileCategories'])
            ?>"><button class="btn   purple btn-sm margin-bottom-5 round-corner-5"  ><i class="fa fa-reply"></i> গার্ড ফাইলের ধরন  </button></a>
	    </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <?= $this->Form->create($guardFileCategories_records, ['type' => 'post', 'class' => 'form-horizontal']); ?>
        <div class="form-body">
            <div class="form-group">
                <label class="col-md-3 control-label">ধরন</label>

                <div class="col-md-4">
                    <div class="input-group">
                        <?= $this->Form->input('name_bng', ['label' => false, 'class' => 'form-control', 'type' => 'text']); ?>
                    </div>
                </div>
            </div>
            
        </div>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-3 col-md-9">
                    <button type="submit" class="btn  round-corner-5 blue">সংরক্ষণ করুন</button>
                </div>
            </div>
        </div>
       
        <?= $this->Form->end(); ?>
    </div>
</div>
