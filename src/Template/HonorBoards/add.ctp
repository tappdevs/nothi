<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>-->

<?php
/**
 * Created by PhpStorm.
 * User: sayeed
 * Date: 3/20/19
 * Time: 6:21 PM
 */
?>
<style>
    .div-dropdown-select {
        display: block;
        text-decoration: none;
        border-bottom: solid 1px;
        padding: 2px 5px;
        color: black;
        transition: 0.2s;
        cursor: pointer;
    }
    .div-dropdown-select:hover {
        background: #e0e0e0;
        text-decoration: none;
        color: black;
    }
</style>
<div class="portlet box purple">
    <div class="portlet-title">
        <div class="caption"><i class=""></i><?php echo __("শাখা অনার বোর্ড"); ?></div>
    </div>
    <div class="portlet-body">

        <div class="panel-body">


            <form action="" method="get" onsubmit="preventDefault()">
                <div class="row">
                    <div class="col-md-6">
                        <?= $this->Form->input('unit_id', array('label' => 'শাখা নির্বাচন করুন', 'type' => 'select', 'class' => 'form-control', 'options' =>  ['' => __("-- বাছাই করুন --")] + $office_units, 'default' => $selected_unit_id, 'onchange' => 'submit()')); ?>
                    </div>
                </div>

                <?php if ($selected_unit_id > 0): ?>
                <div class="row" style="margin-top: 10px;">
                    <div class="col-md-2">
                        <input type="text" name="name" data-unit-id="<?=$selected_unit_id?>" class="form-control name-field" required="required" placeholder="কর্মকর্তা বাছাই করুন" autocomplete="off"/>
                        <input type="hidden" name="employee_record_id" />
                        <div id="name_result" style="border-left: solid 1px;border-right: solid 1px;"></div>
                    </div>
                    <div class="col-md-2">
                        <input type="text" name="organogram_name" data-unit-id="<?=$selected_unit_id?>" class="form-control organogram-field" required="required" placeholder="পদবি বাছাই করুন" autocomplete="off"/>
                        <input type="hidden" name="organogram_id" />
                        <div id="organogram_result" style="border-left: solid 1px;border-right: solid 1px;"></div>
                    </div>
                    <div class="col-md-2">
                        <?php echo $this->Form->input('incharge_label', array('empty' => '--দায়িত্বের ধরণ--', 'type' => 'select', 'label' => false, 'class' => 'form-control input-sm','options' => $officeInchargeTypes));
                        ?>
                    </div>
                    <div class="col-md-2">
                        <input type="text" class="form-control date-picker" name="join_date" placeholder="হইতে" autocomplete="off" data-date-format="dd-mm-yyyy" data-date-end-date="0d"/>
                    </div>
                    <div class="col-md-3">
                        <input type="text" class="form-control date-picker pull-left" style="width: 162px;" name="release_date" placeholder="পর্যন্ত" autocomplete="off" data-date-format="dd-mm-yyyy" data-date-end-date="0d"/>
                        <input type="checkbox" id="is_continue" onchange="if ($('#is_continue').is(':checked')) {$('input[name=release_date]').prop('readonly', true).val('').datepicker('remove')} else {$('input[name=release_date]').prop('readonly', false).datepicker()}" /> চলমান
                    </div>
                    <div class="col-md-1" style="padding-left: 0;">
                        <input type="submit" class="btn btn-success btn-md" name="btnAddHororOfficer" value="যোগ করুন"/>
                    </div>
                </div>
                <?php endif; ?>
            </form>
            <hr/>
            <table class="table table-bordered table-hover table-striped" id="loadedhHonorBoard" style="display: none;">
                <thead>
                <tr>
                    <th rowspan="2">ক্রঃ নং</th>
                    <th rowspan="2">নাম</th>
                    <th rowspan="2">পদবি</th>
                    <th colspan="2">দায়িত্বকাল</th>
                    <th rowspan="2">কার্যক্রম</th>
                </tr>
                <tr>
                    <th>হইতে</th>
                    <th>পর্যন্ত</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    $(function() {
        loadHonorBoard();
    });

    function loadHonorBoard() {
        $("#loadedhHonorBoard").find('tbody').html("");
        var newData = '<tr>'+
            '<td colspan="6"><i class="fa fa-spinner fa-pulse"></i> লোডিং...</td>'+
            '</tr>';
        $("#loadedhHonorBoard").find('tbody').append(newData);

        $.ajax({
            url: js_wb_root+'honorBoards/loadHonorBoard',
            data: {
                unit_id: <?=$selected_unit_id?>,
            },
            method: 'post',
            dataType: 'json',
            success: function (response) {
                $("#loadedhHonorBoard").css('display', 'table');
                $("#loadedhHonorBoard").find('tbody').html("");
                if(response.length > 0) {
                    $.each(response, function(key, value) {
                        var releaseDate = 'releaseDate';
                        if (value.release_date == null) {
                            releaseDate = 'চলমান';
                        } else {
                            releaseDate = enTobn(dateFormat(value.release_date, 'dd-mm-yy'));
                        }
                        var incharge_label = (value.incharge_label != null && value.incharge_label != "") ? ' <small>('+value.incharge_label+')</small>' : '';
                        var newData = '<tr>'+
                                        '<td>'+ enTobn(key+1) +'</td>'+
                                        '<td>'+ value.name + incharge_label +'</td>'+
                                        '<td>'+ value.organogram_name +'</td>'+
                                        '<td>'+ enTobn(dateFormat(value.join_date, 'dd-mm-yy')) +'</td>'+
                                        '<td>'+ releaseDate +'</td>'+
                                        '<td><button class="btn btn-sm btn-danger" onclick="deleteInfo('+value.id+')"><i class="fa fa-trash"></i> মুছে ফেলুন</button></td>'+
                                      '</tr>';

                        $("#loadedhHonorBoard").find('tbody').append(newData);
                    })
                } else {
                    var newData = '<tr>'+
                        '<td colspan="6">কোনো তথ্য পাওয়া যায়নি</td>'+
                        '</tr>';

                    $("#loadedhHonorBoard").find('tbody').append(newData);
                }
            }
        });
    }

    var _changeInterval = null;
    $(".name-field").keyup(function() {
        var nameKey = $(this).val();
        var unit_id = $(this).data('unit-id');
        if (nameKey.length > 1) {
            clearInterval(_changeInterval);
            _changeInterval = setInterval(function() {
                $("#name_result").html('<i class="fa fa-spinner fa-pulse" style="float: right;top: -25px;position: relative;right: 10px;"></i>');
                $("#name_result").css('display', 'block');

                $("input[name=employee_record_id]").val('');
                $.ajax({
                    url: js_wb_root+'honorBoards/name_search',
                    data: {
                        name_key: nameKey,
                        unit_id: unit_id,
                    },
                    method: 'post',
                    dataType: 'json',
                    success: function (response) {
                        $("#name_result").html("");
                        $("#name_result").css('display', 'block');
                        if(response.length > 0) {
                            $.each(response, function(key, value) {
                                var newData = '<div onclick="selectThis(this)" data-employee_record_id="'+ value.employee_record_id +'" class="div-dropdown-select for-name" ">'+ value.EmployeeRecords.name_bng +'<br/>'+ value.designation +'</div>';
                                $("#name_result").append(newData);
                            })
                        }
                    }
                });
                clearInterval(_changeInterval)
            }, 100);
        }
    });
    var _changeInterval = null;
    $(".organogram-field").keyup(function() {
        var organogramKey = $(this).val();
        var unit_id = $(this).data('unit-id');
        if (organogramKey.length > 1) {
            clearInterval(_changeInterval);
            _changeInterval = setInterval(function() {
                $("#organogram_result").html('<i class="fa fa-spinner fa-pulse" style="float: right;top: -25px;position: relative;right: 10px;"></i>');
                $("#organogram_result").css('display', 'block');

                $("input[name=organogram_id]").val('');
                $.ajax({
                    url: js_wb_root+'honorBoards/organogram_search',
                    data: {
                        organogram_key: organogramKey,
                        unit_id: unit_id,
                    },
                    method: 'post',
                    dataType: 'json',
                    success: function (response) {
                        $("#organogram_result").html("");
                        $("#organogram_result").css('display', 'block');
                        if(response.length > 0) {
                            $.each(response, function(key, value) {
                                var newData = '<div onclick="selectThis(this)" data-organogram_id="'+ value.id +'" class="div-dropdown-select for-organogram" ">'+ value.designation_bng +'</div>';
                                $("#organogram_result").append(newData);
                            })
                        }
                    }
                });
                clearInterval(_changeInterval)
            }, 100);
        }
    });

    function selectThis (element) {
        if ($(element).hasClass('for-name')) {
            var employee_record_id = $(element).data('employee_record_id');
            var name = $(element).html().split('<br>')[0];
            $("input[name=employee_record_id]").val(employee_record_id);
            $("input[name=name]").val(name);
            $("#name_result").css('display', 'none');
        } else if ($(element).hasClass('for-organogram')) {
            var organogram_id = $(element).data('organogram_id');
            var organogram_name = $(element).text();
            $("input[name=organogram_id]").val(organogram_id);
            $("input[name=organogram_name]").val(organogram_name);
            $("#organogram_result").css('display', 'none');
        }
    }

    $('input[name=btnAddHororOfficer]').on('click', function(e) {
        e.preventDefault();
        var form = $(this).closest('form');

        var join_date = new Date($(".date-picker[name=join_date]").val().replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
        var release_date = new Date($(".date-picker[name=release_date]").val().replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));

        if ($("input[name=name]").val() == "") {
            toastr.error('কর্মকর্তার নাম দেয়া হয় নি');
            return true;
        }
        if ($("input[name=organogram_name]").val() == "") {
            toastr.error('কর্মকর্তার পদবি দেয়া হয় নি');
            return true;
        }
        if ($("input[name=join_date]").val() == "") {
            toastr.error('যোগদানের তারিখ দেয়া হয় নি');
            return true;
        }
        if ($("input[name=release_date]").val() == "" && !$("#is_continue").is(':checked')) {
            toastr.error('অব্যহতির তারিখ দেয়া হয় নি');
            return true;
        }
        if (join_date > release_date) {
            toastr.error('যোগদানের তারিখ অব্যহতির তারিখের চেয়ে বড় হতে পারে না।  দয়া করে পুনরায় যাচাই করুন।');
            return true;
        }

        $.ajax({
            url: js_wb_root+'honorBoards/add',
            data: form.serialize(),
            method: 'post',
            dataType: 'json',
            success: function (response) {
                if (response.status == 'success') {
                    loadHonorBoard();
                    form.find('input[type=text]').val('');
                    toastr.success('সফলভাবে যোগ করা হয়েছে');
                } else {
                    toastr.error(response.msg);
                }
            }
        });
    });

    function deleteInfo (data_id) {
        $.ajax({
            url: js_wb_root+'honorBoards/delete',
            data: {data_id:data_id, unit_id:<?=$selected_unit_id?>},
            method: 'post',
            dataType: 'json',
            success: function (response) {
                if (response.status == 'success') {
                    loadHonorBoard();
                    toastr.success('সফলভাবে মুছে ফেলা হয়েছে');
                } else {
                    toastr.error('দুঃখিত! কিছুক্ষণ পর পুনরায় চেষ্টা করুন');
                }
            }
        });
    }
</script>