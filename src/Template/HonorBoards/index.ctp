<?php
/**
 * Created by PhpStorm.
 * User: sayeed
 * Date: 3/20/19
 * Time: 5:37 PM
 */
?>
<style>
    .tabled {
        width: 100%;
        border-spacing: 5px;
        border-collapse: separate;
    }
    .tabled td, .tabled th {
        vertical-align: middle !important;
        text-align: center !important;
        border: solid 1px black;
        padding: 7px;
    }
</style>
<script src="<?php echo CDN_PATH; ?>assets/global/scripts/printThis.js"></script>
<div class="portlet box purple">
    <div class="portlet-title">
        <div class="caption"><i class=""></i><?php echo __("শাখা অনার বোর্ড"); ?></div>
        <div class="caption pull-right"><a onclick="$('#ajax-content .panel-body').printThis({importCSS: true,debug: false,importStyle: true,printContainer: false, pageTitle: '', removeInline: false, header: null })" style="color: white;"><i class="fa fa-print"></i> প্রিন্ট</a></div>
    </div>
    <div class="portlet-body">

        <div class="panel-body">
            <h2 style="text-align:center"><?=$get_dak_current_section['office_name']?></h2>
            <h3 style="text-align:center;margin-bottom: 20px;"><div style="box-shadow: silver 0px 0px 5px 5px;display: initial;padding: 7px 20px;font-size: 21px;font-weight: bold;"><?=$get_dak_current_section['office_unit_name']?></div></h3>

            <?php if (count($honor_boards) > 0): ?>
            <table class="tabled">
                <thead>
                <tr>
                    <th rowspan="2">ক্রঃ নং</th>
                    <th rowspan="2">নাম</th>
                    <th rowspan="2">পদবি</th>
                    <th colspan="2">দায়িত্বকাল</th>
                </tr>
                <tr>
                    <th>হইতে</th>
                    <th>পর্যন্ত</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($honor_boards as $key => $honor_board): ?>
                <?php
                    $time                     = new \Cake\I18n\Time($honor_board['join_date']);
                    $honor_board['join_date'] = $time->i18nFormat('dd-MM-Y', null, 'bn-BD');

                    if ($honor_board['release_date'] != null) {
                        $time = new \Cake\I18n\Time($honor_board['release_date']);
                        $honor_board['release_date'] = $time->i18nFormat('dd-MM-Y', null, 'bn-BD');
                    } else {
                        $honor_board['release_date'] = 'চলমান';
                    }
                    ?>
                <tr>
                    <td><?=enTobn($key+1)?></td>
                    <td><?=$honor_board['name']?><?=($honor_board['incharge_label']!=null && $honor_board['incharge_label'] != "") ? " <small>(".$honor_board['incharge_label'].")</small>" : ''?></td>
                    <td><?=$honor_board['organogram_name']?></td>
                    <td><?=$honor_board['join_date']?></td>
                    <td><?=$honor_board['release_date']?></td>
                </tr>
                <?php endforeach;?>
                </tbody>
            </table>
            <?php else: ?>
            <h4 class="alert alert-danger">কোনো তথ্য পাওয়া যায়নি</h4>
            <?php endif; ?>
        </div>
    </div>
</div>
