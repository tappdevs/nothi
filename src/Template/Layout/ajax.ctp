<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>
<script type="text/javascript">
        var js_wb_root = '<?php echo $this->request->webroot ?>';
</script>
<?php
if (Live == 1) {
    ?>
    <script id="fr-fek">try {
            (function (k) {
                localStorage.FEK = k;
                t = document.getElementById('fr-fek');
                t.parentNode.removeChild(t);
            })('xc1We1KYi1Ta1WId1CVd1F==')
        } catch (e) {
        }</script>
    <?php
}
?>
<style>
    .col-12,.col-6,.col-3{
        padding-right:15px;
        padding-left:15px;
    }
    .col-12{
        flex: 0 0 100%;
        max-width: 100%;
    }
    .col-6{
        flex: 0 0 50%;
        max-width: 50%;
    }

    .col-3{
        flex: 0 0 25%;
        max-width: 25%;
    }
</style>
<?= $this->fetch('content') ?>
