<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.2
Version: 3.6.2
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->

<html lang="en" class="no-js">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title>নথি | অফিস ব্যবস্থাপনা</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <meta content="" name="description"/>
        <meta content="" name="author"/>
    <?php
$path = $this->request->webroot;
if(CDN == 1){
    $path = 'http://cdn1.nothi.gov.bd/webroot/';
}

?>
        <link rel="stylesheet" href="<?php echo $path; ?>a2i/demo-files/demo.css">
        <link rel="stylesheet" href="<?php echo $path; ?>a2i/style.css">

        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="<?php echo $path; ?>assets/global/plugins/font-awesome/css/font-awesome.min.css"
              rel="stylesheet" type="text/css"/>
        <link href="<?php echo $path; ?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css"
              rel="stylesheet" type="text/css"/>
        <link href="<?php echo $path; ?>assets/global/plugins/bootstrap/css/bootstrap.min.css"
              rel="stylesheet" type="text/css"/>
        <link href="<?php echo $path; ?>assets/global/plugins/uniform/css/uniform.default.css"
              rel="stylesheet" type="text/css"/>
        <link href="<?php echo $path; ?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
        <!-- END GLOBAL MANDATORY STYLES -->

        <!-- BEGIN PLUGINS USED BY X-EDITABLE -->
        <link rel="stylesheet" type="text/css"      href="<?php echo $path; ?>assets/global/plugins/select2/select2.css"/>

        <link rel="stylesheet" type="text/css"         href="<?php echo $path; ?>assets/global/plugins/bootstrap-datepicker/css/datepicker.css"/>
        <link rel="stylesheet" type="text/css"          href="<?php echo $path; ?>assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css"/>
        <link rel="stylesheet" type="text/css"          href="<?php echo $path; ?>assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"/>

        <!-- END PLUGINS USED BY X-EDITABLE -->


        <link rel="stylesheet" type="text/css" href="<?php echo $path; ?>assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"/>

        <link rel="stylesheet" type="text/css" href="<?php echo $path; ?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>

        <link rel="stylesheet" type="text/css"
              href="<?php echo $path; ?>assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css">


        <link href="<?php echo $path; ?>assets/admin/pages/css/error.css" rel="stylesheet"
              type="text/css"/>
        <!-- END PAGE STYLES -->
        <!-- BEGIN THEME STYLES -->
        <link href="<?php echo $path; ?>assets/global/css/components.css" id="style_components" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $path; ?>assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $path; ?>assets/admin/layout4/css/layout.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $path; ?>assets/admin/layout4/css/themes/light.css" rel="stylesheet" type="text/css" id="style_color"/>
        <!-- END THEME STYLES -->

        <!-- END:File Upload Plugin CSS files-->
        <!-- END PAGE LEVEL STYLES -->

        <link rel="stylesheet" type="text/css"
              href="<?php echo $path; ?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css"/>
        <link rel="stylesheet" type="text/css"
              href="<?php echo $path; ?>assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css"/>
        <link rel="stylesheet" type="text/css"
              href="<?php echo $path; ?>assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"/>

        <link rel="stylesheet" type="text/css" href="<?php echo $path; ?>assets/global/plugins/bootstrap-toastr/toastr.min.css"/>

        <link rel="shortcut icon" href="<?php echo $path; ?>favicon.ico"/>
        <script src="<?php echo $path; ?>assets/global/plugins/jquery.min.js"            type="text/javascript"></script>


        <link rel="shortcut icon" href="<?php echo $path; ?>favicon.ico"/>

        <style>

            .nav .open>a, .nav .open>a:focus, .nav .open>a:hover{
                background-color: transparent;
            }
            .page-header.navbar .page-logo .logo-default{
                margin-left:0px;
            }
            body{
                background:url(<?= $path; ?>img/bg_image.jpg) no-repeat center center #CDE7A8 fixed;
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;
                background-size: cover;
            }

            .page-content{
                background: transparent;
            }

            .page-container{
                padding:0px 5px;
            }

            .page-header.navbar .top-menu .navbar-nav > li.dropdown-user .dropdown-toggle > img {
                margin-top: -20px;
                margin-left: 5px;
                 height: 60px; 
                display: inline-block;
            }


        </style>
        <link rel="stylesheet" type="text/css" href="<?php echo $path; ?>assets/global/plugins/bootstrap-toastr/toastr.min.css"/>

        <?php
        if (Live == 1) {
            ?>
            <script id="fr-fek">try {
                    (function (k) {
                        localStorage.FEK = k;
                        t = document.getElementById('fr-fek');
                        t.parentNode.removeChild(t);
                    })('xc1We1KYi1Ta1WId1CVd1F==')
                } catch (e) {
                }</script>
            <?php
        }
        ?>
    </head>
    <!-- END HEAD -->
    <script type="text/javascript">
        var js_wb_root = '<?= $this->request->webroot ?>';
    </script>
    <body  class="page-header-fixed page-boxed page-full-width">

        <div class="page-header md-shadow-z-1-i navbar navbar-fixed-top">
            <!-- BEGIN HEADER INNER -->
            <div class="page-header-inner">
                <!-- BEGIN LOGO -->

                <div class="page-logo">
                    <a href="<?= $this->request->webroot; ?>">
                        <img src="<?= $path; ?>img/logo-body.png" alt="logo" class="logo-default" style="width: 180px; vertical-align: middle;"/>
                    </a>
                    

                </div>
               
                <!-- END HEADER INNER -->
            </div>
        </div>
        <div class="clearfix"></div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content" style="font-size:12pt;">
                    <?php echo $this->fetch('content'); ?>
                </div>
            </div>
            <!-- END CONTENT -->
        </div>

        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <div class="page-footer">
            <div class=" text-center">

                <a href="http://bangladesh.gov.bd" target="_blank">
                    <img src="<?php echo $path; ?>assets/admin/layout4/img/bd.png" alt="">
                </a>
                কপিরাইট <?= $Year ?>, একসেস টু ইনফরমেশন
                <a href="http://www.a2i.gov.bd" target="_blank">
                    <img src="<?php echo $path; ?>assets/admin/layout4/img/a2i.png" alt="">
                </a>
            </div>

        </div>
       
        <!-- END FOOTER -->
        <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
        <!-- BEGIN CORE PLUGINS -->
        <!--[if lt IE 9]>
        <script src="<?php echo $path; ?>assets/global/plugins/respond.min.js"></script>
        <script src="<?php echo $path; ?>assets/global/plugins/excanvas.min.js"></script>
        <![endif]-->
        <script src="<?php echo $path; ?>assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
        <script src="<?php echo $path; ?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo $path; ?>assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="<?php echo $path; ?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="<?php echo $path; ?>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="<?php echo $path; ?>assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
        <script src="<?php echo $path; ?>assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <script src="<?php echo $path; ?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>

        <script type="text/javascript" src="<?php echo $path; ?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
        <script type="text/javascript" src="<?php echo $path; ?>assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>
        <script src="<?php echo $path; ?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <script src="<?php echo $path; ?>assets/global/plugins/jquery-tags-input/jquery.tagsinput.min.js" type="text/javascript"></script>
        <script src="<?php echo $path; ?>assets/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
        <script src="<?php echo $path; ?>assets/global/plugins/bootstrap-touchspin/bootstrap.touchspin.js" type="text/javascript"></script>
        <script src="<?php echo $path; ?>assets/global/plugins/typeahead/handlebars.min.js" type="text/javascript"></script>
        <script src="<?php echo $path; ?>assets/global/plugins/typeahead/typeahead.bundle.min.js"
        type="text/javascript"></script>
        <script src="<?php echo $path; ?>assets/admin/layout4/scripts/projapoti_ajax.js?v=<?= js_css_version ?>" type="text/javascript"></script>
        <script src="<?php echo $path; ?>assets/admin/pages/scripts/form-validation.js"></script>
        <!-- END PAGE LEVEL SCRIPTS -->

        <script type="text/javascript" src="<?php echo $path; ?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
        <script type="text/javascript"   src="<?php echo $path; ?>assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>

        <script type="text/javascript" src="<?php echo $path; ?>assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>


        <script src="<?php echo $path; ?>assets/global/scripts/metronic.js" type="text/javascript"></script>
        <script src="<?php echo $path; ?>assets/admin/layout4/scripts/layout.js" type="text/javascript"></script>
        <script src="<?php echo $path; ?>assets/admin/layout4/scripts/office_setup.js" type="text/javascript"></script>
        <script src="<?php echo $path; ?>assets/admin/layout4/scripts/projapoti_autocomplete.js"  type="text/javascript"></script>

        <script src="<?php echo $path; ?>assets/global/plugins/bootstrap-toastr/toastr.min.js"></script>
        <script src="<?php echo $path; ?>assets/admin/pages/scripts/ui-toastr.js"></script>
        <!-- END -->
        <script type="text/javascript">
        jQuery(document).ready(function () {
            Metronic.init(); // init metronic core componets
            Layout.init(); // init layout
            ProjapotiAutocomplete.init();
        });
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-bottom-right"
        };
        /***Custom Bangla From English (Important)***/
        function enTobn(input){
            return BnFromEng(input);
        }
        function bnToen(input){
            return EngFromBn(input);
        }
        function BntoEng(input){
            return EngFromBn(input);
        }
        function BnFromEng(input) {
            if(typeof input == 'undefined' || input == null){
                return '';
            }
            var numbers = {
                0: '০',
                1: '১',
                2: '২',
                3: '৩',
                4: '৪',
                5: '৫',
                6: '৬',
                7: '৭',
                8: '৮',
                9: '৯'
            };

            var output = '';

            if (typeof (input) == 'number') {
                input = input.toString();
            }
            for (var i = 0; i < input.length; ++i) {
                if (numbers.hasOwnProperty(input[i])) {
                    output += numbers[input[i]];
                } else {
                    output += input[i];
                }
            }
            return output;
        }
        function EngFromBn(input) {
            if(typeof input == 'undefined' || input == null){
                return '';
            }
            var numbers = {
                '০':0,
                '১':1,
                '২':2,
                '৩':3,
                '৪':4,
                '৫':5,
                '৬':6,
                '৭':7,
                '৮':8,
                '৯':9
            };
            var output = '';

            if (typeof (input) == 'number') {
                input = input.toString();
            }
            for (var i = 0; i < input.length; ++i) {
                if (numbers.hasOwnProperty(input[i])) {
                    output += numbers[input[i]];
                } else {
                    output += input[i];
                }
            }
            return output;
        }
        function isEmpty(value){
            if(typeof(value) == 'undefined' || value == '' || value == null ||  value == 0){
                return true;
            }
            return false;
        }
        var entityMap = {
            '&': '&amp;',
            '<': '&lt;',
            '>': '&gt;',
            '"': '&quot;',
            "'": '&#39;',
            '/': '&#x2F;',
            '`': '&#x60;',
            '=': '&#x3D;'
        };
        function escapeHtml (string) {
            return String(string).replace(/[&<>"'`=\/]/g, function (s) {
                return entityMap[s];
            });
        }
        function number_format(input){
            var output = '';

            if (typeof (input) == 'number') {
                input = input.toString();
            }
            var len = (input.indexOf('.') == -1)? input.length:input.indexOf('.');
            console.log(len,input.length,input.indexOf('.'));
            if(len < input.length){
                for (var i = input.indexOf('.'); i < input.length; i++) {
                    output += input[i];
                }
            }
            var thousandsCount = 0;
            for (var i = len; i > 0; i--) {
                var char_ = input.substr(i - 1, 1);
                console.log(char_);
                if(!isNaN(char_)){
                    thousandsCount++;
                }
                if (thousandsCount % 3 == 0){
                    thousandsCount = 0;
                    if(i != 1){
                        char_ = ',' + char_;
                    }
                }
                output = char_ + output;
            }
            return output;

        }
        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>