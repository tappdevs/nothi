<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.2
Version: 3.3.0
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title>নথি | অফিস ব্যবস্থাপনা</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta content="" name="description"/>
    <meta content="" name="author"/>

    <?php
    $path = $this->request->webroot;
    if (defined('CDN') && CDN == 1) {
        $path = 'http://cdn1.nothi.gov.bd/webroot/';
    }
    ?>

    <meta property="og:title" content="নথি | অফিস ব্যবস্থাপনা">
    <meta property="og:image" content="<?php echo str_replace('content/', '', FILE_FOLDER); ?>img/nothi_logo_login.png">

    <link rel="stylesheet" href="<?= $path; ?>a2i/demo-files/demo.css">
    <link rel="stylesheet" href="<?= $path; ?>a2i/style.css">
    <link rel="stylesheet" href="<?= $path; ?>css/style.css">

    <link href="<?= $path ?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?= $path ?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet"
          type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->
    <link href="<?php echo $path; ?>assets/admin/pages/css/login-soft.css" rel="stylesheet" type="text/css"/>
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="<?= $path ?>assets/global/css/components.css" rel="stylesheet" id="style_components"
          type="text/css"/>
    <link href="<?= $path ?>assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
    <link href="<?= $path ?>assets/global/plugins/bootstrap-toastr/toastr.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?= $path ?>assets/admin/pages/css/blog.css" rel="stylesheet" type="text/css" />

    <script src="<?= $path ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>

    <link rel="shortcut icon" href="<?php echo $path; ?>favicon.ico"/>

    <link rel="stylesheet" type="text/css"
          href="<?php echo $path; ?>assets/global/plugins/bootstrap-toastr/toastr.min.css"/>
    <style>
        .login {

            background: url(<?= $path; ?>img/bg_image.jpg) no-repeat center center #CDE7A8 fixed;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }
        input {
            width: 100% !important;
            padding: 10px !important;
            border: none #a0a9b4 !important;
            border-bottom: 1px solid !important;
            color: #868e97 !important;
            font-size: 13px !important;
            margin-bottom: 20px !important;
        }
        input[type=text],input[type=password] {
            font-size: 13pt !important;
        }
        * {
            font-size: 13pt!important;
        }
    </style>

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->

<body class="login" style="font-family: Nikosh!important;font-size: 13pt!important;">
<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
<div class="menu-toggler sidebar-toggler">
</div>
<!-- END SIDEBAR TOGGLER BUTTON -->
<!-- BEGIN LOGO -->
<style>

    .panel-primary > .panel-heading {
        background-color: #596c47 !important;
    }

    .panel-primary > .panel-heading + .panel-collapse .panel-body {
        border-top-color: #596c47 !important;
    }

    .login .logo {
        margin: 10px auto 5px auto;
        padding: 15px;
        text-align: center;
    }

    .login .content h3 {
        color: #404040;
    }

    .login .content{
        margin: 0 auto;
        margin-bottom: 0px;
        padding: 5px;
        padding-top: 10px;
        padding-bottom: 5px;
    }
</style>

<?= $this->element('ssl_message_and_mobile_detection'); ?>
<div class="logo" style="color:#000; ">
    <img style="width: 200px" src="<?php echo $path; ?>img/nothi_logo_login.png" alt=""/>
</div>
<div class="text-center font-lg" id="SSLMessageForAndroid" hidden>

    <a class="btn btn-primary" href="https://play.google.com/store/apps/details?id=com.tappware.nothi&hl=en">
        ডাউনলোড করুন নথির অ্যান্ড্রয়েড অ্যাপ
    </a>
</div>
<div class="text-center font-lg" id="SSLMessageForIOS" hidden>

    <a class="btn btn-primary" href="https://itunes.apple.com/us/app/id1187955540">
        ডাউনলোড করুন নথির আই ও এস অ্যাপ
    </a>
</div>
<!-- END LOGO -->
<!-- BEGIN LOGIN -->
<div class="content" style="width: 480px!important;">
    <!--    <p class="alert alert-info">নথির নতুন আপডেট এর কারনে অনুগ্রহ করে ব্রাউজারের কেশ পরিষ্কার করুন</p>-->
    <?php echo $this->Flash->render(); ?>
    <?php echo $this->fetch('content'); ?>
</div>
<div class="content" style="padding-top: 10px!important;width: 480px!important;margin-top:0px">
    <div class="logobd" align="left">
        <a href="http://bangladesh.gov.bd" target="_blank">
            <img src="<?php echo $path ?>assets/admin/layout4/img/bd.png" alt="" style="margin-top:10px">
        </a>
        <a href="http://www.a2i.gov.bd" target="_blank">
            <img src="<?php echo $path ?>assets/admin/layout4/img/a2i.png" alt="" style="margin-top:5px">
        </a>
    </div>
    <div class="logoa2i" align="right">
        <a href="<?php
        echo $this->Url->build(['controller' => 'DakNagoriks',
            'action' => 'NagorikAbedon'])
        ?>" class="btn btn-link" style="text-decoration: none;text-align:right">
            <img src="img/ekseba-logo.png" style="width:120px;margin-top:0px;margin-right:0px">
            <!--                <strong>&nbsp;একসেবা</strong>-->
            <!--                <small style="font-size:75% !important;">সবার জন্য আবেদন</small>-->
        </a>
    </div>
    <br>
    <div style="clear:both"></div>
</div>
<br>
<div class="content" style="padding-top: 10px!important;width: 480px!important;margin-top:0px">
    <div class="text-center" style="font-size: 8pt!important;">
        <img src="<?= $this->request->webroot ?>img/img_tapp_logo.png" style="height: 12px;">
        কারিগরি সহায়তায় <a style="font-size: 8pt!important;" href="http://tappware.com" target="_tab">
            ট্যাপওয়ার <span class="panel-body"></span>
        </a>
    </div>
</div>

<!-- END LOGIN -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="<?php echo $path; ?>assets/global/plugins/respond.min.js"></script>
<script src="<?php echo $path; ?>assets/global/plugins/excanvas.min.js"></script>
<![endif]-->

<script src="<?= $path; ?>assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?= $path; ?>js/client.min.js" type="text/javascript"></script>
<script src="<?= $path ?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?= $path ?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<script src="<?= $path ?>assets/global/plugins/bootstrap-toastr/toastr.min.js" type="text/javascript"/>
<script src="<?= $path ?>assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>


<!-- END PAGE LEVEL SCRIPTS -->
<script type="text/javascript">
	$(function () {
		handleForgetPassword();
	});


	var handleForgetPassword = function () {
		$('.forget-form').validate({
			errorElement: 'span', //default input error message container
			errorClass: 'help-block', // default input error message class
			focusInvalid: false, // do not focus the last invalid input
			ignore: "",
			rules: {
				username: {
					required: true,
				}
			},
			messages: {
				username: {
					required: "লগইন আইডি দেয়া হয়নি"
				}
			},
			invalidHandler: function (event, validator) { //display error alert on form submit

			},
			highlight: function (element) { // hightlight error inputs
				$(element)
					.closest('.form-group').addClass('has-error'); // set error class to the control group
			},
			success: function (label) {
				label.closest('.form-group').removeClass('has-error');
				label.remove();
			},
			errorPlacement: function (error, element) {
				error.insertAfter(element.closest('.input-icon'));
			},
			submitHandler: function (form) {
				$.ajax({
					url: '<?php
                        echo $this->Url->build(['controller' => 'Users',
                            'action' => 'forgetPassword'])
                        ?>',
					data: $('.forget-form').serialize(),
					method: 'post',
					cache: false,
					dataType: 'JSON',
					success: function (response) {
						$('.forget-form').find('.form-actions').show();
						$('.message').hide();
						toastr.options = {
							"closeButton": true,
							"debug": false,
							"positionClass": "toast-bottom-right"
						};
						if (response.status == 'error') {
							toastr.error(response.msg);
						} else {
							toastr.success(response.msg);
						}
					}
				})
			}
		});


	}

	$('.forget-form input').keypress(function (e) {
		if (e.which == 13) {
			if ($('.forget-form').validate().form()) {
				$('.forget-form').find('.form-actions').hide();
				$('.forget-form').submit();
			}
			return false;
		}
	});

	$('.forget-form button').click(function (e) {
		if ($('.forget-form').validate().form()) {
			$('.forget-form').find('.form-actions').hide();
			$('.forget-form').submit();
		}
		return false;

	});

	jQuery('#register-btn').click(function () {
		jQuery('.login-form').hide();
		jQuery('.register-form').show();
	});

	jQuery('#register-back-btn').click(function () {
		jQuery('.login-form').show();
		jQuery('.register-form').hide();
	});

</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>