<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
<link href="<?= $this->request->webroot ?>potrojariPrintPreview/bootstrap.min.css" rel="stylesheet">
<link href="<?= $this->request->webroot ?>potrojariPrintPreview/style.css" rel="stylesheet">

</head>
<body><span class="page_id">20</span>
<?= $this->element('print-preview/print-margin'); ?>
<div class="containerx">
  <?= $this->element('print-preview/print-margin'); ?>
  <?php echo $this->fetch('content'); ?>
  </div>
<script src="<?= $this->request->webroot ?>potrojariPrintPreview/printPreviewFunctions.js"></script>
<script>

    var left_slogan = document.querySelector('#left_slogan');
    var sharok_no2 = document.querySelector('#sharok_no2');
    var sharok_no = document.querySelector('#sharok_no');
    var sovapotiname = document.querySelector('#sovapotiname');
    var tableTagclass2 = document.querySelectorAll('table.class2');
    var getarthe = document.querySelector('#getarthe');
    var sender_name = document.querySelector('#sender_name');
    var office_organogram_id = document.querySelector('#office_organogram_id');
    var elemToRemoveStyle = document.querySelectorAll('p,td,th,table span');
    var elemToAddStyle = document.querySelectorAll('td,th');
    var canedit = document.querySelectorAll('.canedit');


    var english = /^[a-zA-Z0-9$@$!%*?&#^-_. +]+$/;
    var isUrl = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;


    replaceClass('offset-md-7', 'col-md-offset-7');
    replaceClass('offset-sm-7', 'col-sm-offset-7');
    removeClass('col-xs-offset-7');
    document.addEventListener('DOMContentLoaded', function () {

      if (tableTagclass2) {
        for (var i = 0; tableTagclass2.length > i; i++) {
          setAttributes(tableTagclass2[i], {
            'border': 1
          });
        }
      }
    })


    if (sender_name) {
      // sender_name.parentElement.parentElement.parentElement.setAttribute('class', 'col-sm-5 offset-7 text-center');
      var sender_name_center = sender_name.parentElement.parentElement.parentElement.querySelectorAll('.row')
      for (var i = 0; i < sender_name_center.length; i++) {
        sender_name_center[i].classList.remove('row');
      }
    }


    if (office_organogram_id) {
      office_organogram_id.parentElement.setAttribute('class', 'col-md-12 col-xs-12 col-sm-12 order-1 text-left row flex-column');
      office_organogram_id.parentElement.parentElement.setAttribute('class', 'col row');
    }


    if (elemToRemoveStyle) {
      for (var i = 0; i < elemToRemoveStyle.length; i++) {
        // elemToRemoveStyle[i].setAttribute('style', '');
      }
    }

    if (elemToAddStyle) {
      for (var i = 0; i < elemToAddStyle.length; i++) {
        elemToAddStyle[i].setAttribute('style', 'padding:0 5px');
      }
    }

    if (getarthe) {
      getarthe.innerHTML = getarthe.innerHTML.replace('<br>', '');
    }
    if (sharok_no2) {
      setAttributes(sharok_no2.parentElement.parentElement, {
        "style": "margin-bottom:50px;",
        "class": "row customFontSize align-items-center"
      });
    }
    if (sharok_no) {

      setAttributes(sharok_no.parentElement.parentElement, {
        "style": "",
        "class": "row customFontSize align-items-center my-3"
      });
    }


    if (left_slogan) {
      left_slogan.parentElement.parentElement.classList.add('row');
    }


    if (sovapotiname) {
      sovapotiname.parentElement.parentElement.firstElementChild.setAttribute('style', 'padding:0 5px;width:100px;')
    }

</script>
</body>
</html>