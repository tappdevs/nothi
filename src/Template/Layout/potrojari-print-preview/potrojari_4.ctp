<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
<link href="<?= $this->request->webroot ?>potrojariPrintPreview/bootstrap.min.css" rel="stylesheet">
<link href="<?= $this->request->webroot ?>potrojariPrintPreview/style.css" rel="stylesheet">

</head>
<body><span class="page_id">4</span>
<div class="containerx">
  <?= $this->element('print-preview/print-margin'); ?>
  <?php echo $this->fetch('content'); ?>
  </div>
<script src="<?= $this->request->webroot ?>potrojariPrintPreview/printPreviewFunctions.js"></script>
<script>

    var left_slogan = document.querySelector('#left_slogan');
    var sharok_no2 = document.querySelector('#sharok_no2');

    var sender_signature2 = document.querySelectorAll('#sender_name2,#sender_designation2');

    var sharok_no = document.querySelector('#sharok_no');
    var getarthe = document.querySelector('#getarthe');
    var sender_name = document.querySelector('#sender_name');
    var office_organogram_id = document.querySelector('#office_organogram_id');
    var canedit = document.querySelectorAll('.canedit');


    var english = /^[a-zA-Z0-9$@$!%*?&#^-_. +]+$/;
    var isUrl = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;


    replaceClass('offset-md-8', 'col-sm-offset-8');
    replaceClass('offset-md-7', 'col-md-offset-7');
    replaceClass('offset-sm-6', 'col-sm-offset-6');
    replaceClass('offset-sm-7', 'col-sm-offset-7');
    removeClass('col-md-offset-6');
    removeClass('col-xs-offset-6');
    removeClass('col-xs-offset-7');
    removeClass('col-sm-offset-8');

    // console.log(filterEnglish('kaiser, sqa, test আমাদের desh আমাদের'));


    if (sender_signature2) {
      for (var i = 0; sender_signature2.length > i; i++) {
        sender_signature2[i].parentElement.parentElement.setAttribute('class','col-md-5 col-sm-5 col-xs-5 text-center offset-sm-1');
        sender_signature2[i].parentElement.classList.remove('row');
      }
    }
    if (office_organogram_id) {
      office_organogram_id.parentElement.parentElement.classList.add('row');
    }
    if (getarthe) {
      getarthe.innerHTML = getarthe.innerHTML.replace('<br>', '');
    }
    if (sharok_no2) {
      setAttributes(sharok_no2.parentElement.parentElement, {
        "style": "margin-bottom:50px;",
        "class": "row customFontSize align-items-center"
      });
    }


    if (left_slogan) {
      left_slogan.parentElement.parentElement.classList.add('row');
    }

</script>
</body>
</html>

