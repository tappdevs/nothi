<div class="portlet light">
    <div class="portlet-body">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption">
	                        ই-নথি বাস্তবায়ন সংক্রান্ত নির্দেশনা
                        </div>
                    </div>
                    <div class="portlet-body">
                        <form action="" method="post" class="form-horizontal hidden">
                            <div class="col-md-4">
                                <div class="form-group" style="width:100%;">
                                    <label class="control-label">বছর</label>&nbsp;<span class="text-danger">*</span>
                                    <div><?php echo $this->Form->input('year', array('type'=>'select','label' => false, 'class' => 'form-control')); ?></div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input type="submit" class="btn btn-success" style="margin-top: 28px;" name="btnSearch" value="খুঁজুন">
                                </div>
                            </div>
                        </form>
                        <div class="table-scrollable">
                            <table class="table table-hover table-bordered" id="datatable_table">
                                <thead>
                                <tr>
                                    <th style="width:10%;" class="text-center">
                                        নম্বর
                                    </th>
                                    <th style="width:60%;" class="text-center">
                                        শিরোনাম
                                    </th>
                                    <th class="text-center heading" style="width:10%;">
                                        তারিখ
                                    </th>
                                    <th style="width:10%;" class="text-center">
                                        ডাউনলোড
                                    </th>
                                </tr>
                                </thead>
                                <tbody>

                                <?php
                                $i = 0;
                                if (!empty($list)) {
                                    foreach ($list as $key => $val) {
                                        $i++;
                                        ?>
                                        <tr data-id="<?= $val['id'] ?>">
                                            <td style="width:10%;" class="text-center" style="font-size: 14px!important;"><?= $this->Number->format($i); ?></td>
                                            <td style="width:60%;" id="update_<?= $val['id'] ?>" class="" style="font-size: 14px!important;"><?= $val['title']; ?></td>
                                            <td style="width:10%;" class="text-center" style="font-size: 14px!important;"><?= enTobn($val['date_of_submit']->format('d-m-Y')); ?></td>
                                            <td style="width:60%;" id="update_<?= $val['id'] ?>" class="" style="font-size: 14px!important;">
                                                <a href="<?= $this->request->webroot.'downloadContentFile?file='.$val['attachment_path'] ?>&name=<?= $val['title']; ?>" title="<?= $val['title']; ?>"><i class="fa fa-download"></i> ডাউনলোড</a>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>

                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>