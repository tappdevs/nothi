<table class="table table-hover table-bordered ">
    <thead>
    <?php if (!$you_are_permitted_for_first_note): ?>
    <tr class="text-center">
        <th class="text-center" style="width: 10%">নোট নং</th>
        <th class="text-center" style="width: 40%">বিষয়</th>
        <th class="text-center" style="width: 20%">গ্রহণের সময়</th>
        <th class="text-center" style="width: 30%">বর্তমান ডেস্ক</th>
        <th class="text-center hidden">বিস্তারিত</th>
    </tr>
    <?php endif; ?>
    </thead>
    <tbody id="dbl_click">
    <?php
    if(!empty($nothiLists)){
        if ($you_are_permitted_for_first_note) {
            ?>
            <tr>
                <td colspan="4">এই নথিতে এখনো কোনো নোট দেয়া হয় নি। আপনি কি ১নং নোট দিতে চান?</td>
                <td class='text-center'>
                    <div class="btn-group">
                        <a title='হ্যাঁ' class='btn btn-success btn-sm' data-title='হ্যাঁ' onclick="setMeFor1stNote(this)"><i class='fa fa-check'></i> হ্যাঁ</a>
                        <a title='না' class='btn btn-danger btn-sm' data-title='না' data-dismiss="modal"><i class='fa fa-times'></i> না</a>
                    </div>
                </td>
            </tr>
            <?php
        } else {
            foreach ($nothiLists as $nothiList) { ?>
                <tr class="<?= $nothiList['class'] ?>">
                    <td class="text-center"><?= h($nothiList['NothiParts']['nothi_part_no_bn']) ?></td>
                    <td data-search-term="<?= h($nothiList['NothiParts']['nothi_part_no_bn']) . ':' . h($nothiList['subject']) ?>">
                        <?= ($nothiList['subject']) ?>
                        <?php if ($nothiList['details'] != 1): ?>
                            <small style="color: gray;">(আপনার ডেস্ক এ আসেনি)</small>
                        <?php endif; ?>
                    </td>
                    <td class="text-center"><?= h($nothiList['issue_date']) ?></td>
                    <td><?= h($nothiList['current_desk']) ?></td>
                    <td class='text-center hidden'>
                        <?php
                        if ($nothiList['details'] == 1) {
                            ?>
                            <a title='বিস্তারিত' class='btn btn-success btn-xs dbl_click' data-title='বিস্তারিত'
                               href="<?= $this->request->webroot; ?>noteDetail/<?= $nothiList['NothiParts']['id'] ?>"><i
                                        class='a2i_gn_details2'></i></a>
                            <?php
                        } else { ?>
                            আপনার ডেস্ক এ আসেনি
                        <?php } ?>
                    </td>
                </tr>
            <?php }
        }
    } else { ?>
        <tr>
            <td class="text-center" colspan="4" style="color:red;">কোনো তথ্য পাওয়া যায়নি!
            </td>
        </tr>
   <?php } ?>
    </tbody>
</table>
<div class="actions text-center">
    <ul class="pagination pagination-sm">
        <?php
        echo $this->Paginator->first(__('প্রথম', true), array('class' => 'number-first'));
        echo $this->Paginator->prev('<<', array('tag' => 'li', 'escape' => false), '<a href="#" class="btn btn-sm blue">&laquo;</a>', array('class' => 'prev disabled btn btn-sm blue', 'tag' => 'li', 'escape' => false));
        echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a', 'reverse' => FALSE));
        echo $this->Paginator->next('>>', array('tag' => 'li', 'escape' => false), '<a href="#" class="btn btn-sm blue ">&raquo;</a>', array('class' => 'next disabled', 'tag' => 'li', 'escape' => false));
        echo $this->Paginator->last(__('শেষ', true), array('class' => 'number-last'));
        ?>
    </ul>

</div>

<script>
    jQuery(document).ready(function () {
        $(document).off('click','.pagination a').on('click','.pagination a',function(ev){
            ev.preventDefault();
            PROJAPOTI.ajaxLoad($(this).attr('href'),'.details-modal-inbox-content');
        });
    });

    function setMeFor1stNote(element) {
        $(element).find('.fa').toggleClass('fa-spinner fa-pulse');
        $(element).toggleClass('disabled');
        var url = "<?=$this->request->webroot; ?>setMeFor1stNote/<?=$nothiLists->first()['nothi_master_id']?>/<?=$nothiLists->first()['nothi_part_no']?>";
        $.ajax({
            url: url,
            method: 'post',
            dataType: 'json',
            cache: false,
            success: function (response) {
                if (response.status == 'success') {
                    toastr.success(response.msg);
                    window.location.href = js_wb_root + "noteDetail/" + <?=$nothiLists->first()['nothi_part_no']?>;
                    NothiMasterMovement.showPartNothis(<?=$nothiLists->first()['nothi_master_id']?>, <?=$nothiLists->first()['nothi_part_no']?>, 'all');
                } else {
                    console.log(response.msg);
                    toastr.error(response.msg);
                    $(element).find('.fa').toggleClass('fa-spinner fa-pulse');
                    $(element).toggleClass('disabled');
                }
            }
        });
    }

    <?php if(!$you_are_permitted_for_first_note):?>
        $('#detailsModal').find('.newPartCreate').show();
        $('#detailsModal').find('.btn-sokol').show();
    <?php endif;?>

</script>

