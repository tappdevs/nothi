<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css"
      href="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2.css"/>

<!-- END PAGE LEVEL STYLES -->
<style>
    tfoot {
        padding: 2px;
        background-color: rgba(241, 241, 241, 0.73);
    }

    .checkbox input[type=checkbox], .checkbox-inline input[type=checkbox], .radio input[type=radio], .radio-inline input[type=radio] {
        margin-left: -10px !important;
    }

    input[type=checkbox], input[type=radio] {
        margin-top: 2px;
    }

    div.radio, div.checker {
        margin-top: -2px;
    }

    .newInbox {
        background-color: #C7DAE0;
    }

    .dashboard-stat .visual {
        width: 80px;
        height: auto;
        display: block;
        float: left;
        padding-top: 10px;
        padding-left: 15px;
        margin-bottom: 15px;
        font-size: 35px;
        line-height: 35px;
    }

    .dashboard-stat .details .desc {
        text-align: right;
        font-size: 12px;
        letter-spacing: 0px;
        font-weight: 300;
    }
    .dashboard-stat .details .number {
        padding-top: 18px;
        text-align: right;
        font-size: 16px;
        line-height: 22px;
        letter-spacing: 0px;
        margin-bottom: 4px;
        font-weight: 300;
    }
    .dashboard-stat .more {
        clear: both;
        display: block;
        padding: 6px 10px 6px 2px;
        position: relative;
        text-transform: uppercase;
        font-weight: 300;
        font-size: 11px;
        opacity: 0.7;
    }
    .dashboard-stat .details {
        position: absolute;
        right: 10px;
        padding-right: 10px;
    }
</style>

<div class="portlet-body">
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <table class="table table-striped table-bordered table-hover" id="folder-nothi-masters">
            <thead>
            <tr role="row" class="filter">
                <td colspan=2>
                    <input type="text" class="form-control form-filter input-sm" placeholder="নথির শাখা"
                           name="unit_name_bng" id="filter_input_2">
                </td>
                <td>
                    <input type="text" class="form-control form-filter input-sm" placeholder="নথির ধরন" name="type_name"
                           id="filter_input_1">
                </td>
                <td>
                    <input type="text" class="form-control form-filter input-sm" placeholder="বিষয়" name="subject"
                           id="filter_input_3">
                </td>
                <td colspan>
                    <input type="text" class="form-control form-filter input-sm" placeholder="নথি নম্বর" name="nothi_no"
                           id="filter_input_4">
                </td>
                <td colspan="3">
                    <button class="btn btn-sm yellow filter-submit margin-bottom" id="filter_submit_btn" name="filter_submit_btn"><i class="fs1 a2i_gn_search1"></i> <?php echo __(SEARCH) ?></button>
                    <button class="btn btn-sm red filter-cancel"><i class="fs1 a2i_gn_reset2"></i> <?php echo __(RESET) ?>
                    </button>
                </td>
            </tr>
            </thead>
        </table>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-lg-12 folder-content">

        </div>
    </div>
</div>


<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2.min.js"></script>
<script src="<?php echo CDN_PATH; ?>projapoti-nothi/js/tbl_nothi_masters.js?v=<?=js_css_version?>"></script>


<script type="text/javascript">

    <?php if($listtype == 'inbox'){?>
    TableAjax.folderInit('nothiMasters/folderView/inbox','');
    <?php }elseif($listtype == 'sent'){?>
    TableAjax.folderInit('nothiMasters/folderView/sent','');
    <?php }elseif($listtype == 'office') {?>
    TableAjax.folderInit('nothiMasters/folderView/office','');
    <?php }elseif(empty($listtype) || $listtype == 'all') {?>
    TableAjax.folderInit('nothiMasters/folderView/','');
    <?php }?>

    $('#filter_submit_btn').on('click', function(){

        var condition = {'subject': $('#filter_input_3').val(), 'type_name': $('#filter_input_1').val(), 'unit_name_bng': $('#filter_input_2').val(), 'nothi_no': $('#filter_input_4').val()};

        <?php if($listtype == 'inbox'){?>
        TableAjax.folderInit('nothiMasters/folderView/inbox', condition);
        <?php }elseif($listtype == 'sent'){?>
        TableAjax.folderInit('nothiMasters/folderView/sent', condition);
        <?php }elseif($listtype == 'office') {?>
        TableAjax.folderInit('nothiMasters/folderView/office', condition);
        <?php }elseif(empty($listtype) || $listtype == 'all') {?>
        TableAjax.folderInit('nothiMasters/folderView/', condition);
        <?php }?>
    });

    $('.filter-cancel').on('click', function(){
        var condition = {};
        $('#filter_input_1').val('');
        $('#filter_input_2').val('');
        $('#filter_input_3').val('');
        $('#filter_input_4').val('');

        <?php if($listtype == 'inbox'){?>
        TableAjax.folderInit('nothiMasters/folderView/inbox','');
        <?php }elseif($listtype == 'sent'){?>
        TableAjax.folderInit('nothiMasters/folderView/sent','');
        <?php }elseif($listtype == 'office') {?>
        TableAjax.folderInit('nothiMasters/folderView/office','');
        <?php }elseif(empty($listtype) || $listtype == 'all') {?>
        TableAjax.folderInit('nothiMasters/folderView/','');
        <?php }?>
    });

    $('.nothi-list-title').text('<?php if ($listtype == "office") { ?> নিজ শাখার নথিসমূহ  <?php }elseif ($listtype == "inbox") { ?> আগত নথিসমূহ   <?php }elseif (empty($listtype) || $listtype == "all") { ?>সকল নথিসমূহ <?php } else { ?> প্রেরিত ডাকসমূহ  <?php } ?>');

    $('[data-toggle="tooltip"]').tooltip({placement: 'bottom'});

</script>