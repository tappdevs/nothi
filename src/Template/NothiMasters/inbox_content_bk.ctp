 <!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css"
      href="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css"
      href="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css"
      href="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>

<link href="<?php echo CDN_PATH; ?>daptorik_preview/css/custom.css" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL STYLES -->
<style>
    tfoot {
        padding: 2px;
        background-color: rgba(241, 241, 241, 0.73);
    }

    .checkbox input[type=checkbox], .checkbox-inline input[type=checkbox], .radio input[type=radio], .radio-inline input[type=radio] {
        margin-left: -10px !important;
    }

    input[type=checkbox], input[type=radio] {
        margin-top: 2px;
    }

    div.radio, div.checker {
        margin-top: -2px;
    }

    .newInbox {
        background-color: #C7DAE0;
    }
</style>

<div class="portlet-body">
    <div class="table-container data-table-dak">
        <table class="table table-striped table-bordered table-hover" id="datatable-nothi-masters">
            <thead>
            <tr role="row" class="filter">
                <td colspan="2">
                    <input type="text" class="form-control form-filter input-sm" placeholder="নথি নম্বর" name="nothi_no"
                           id="filter_input_4">
                </td>

                <td colspan=1>
                    <input type="text" class="form-control form-filter input-sm" placeholder="বিষয়" name="subject"
                           id="filter_input_3">
                </td>
                <td colspan="2">
                    <div class="input-group input-large date-picker input-daterange" data-date="<?php echo date('Y-m-d') ?>" data-date-format="yyyy-mm-dd">
                        <input placeholder=" " type="text" class="form-control input-sm form-filter" name="from" id="search_date_from">
                        <span class="input-group-addon input-sm"> হইতে </span>
                        <input placeholder="" type="text" class="form-control input-sm form-filter" name="to" id="search_date_to">
                    </div>
                </td>
                
                <td colspan="4" class="">
                    <button class="btn btn-sm yellow filter-submit margin-bottom" id="filter_submit_btn"
                            name="filter_submit_btn" data-title="<?php echo __(SEARCH) ?>" title="<?php echo __(SEARCH) ?>"><i class="fs1 a2i_gn_search1"></i> </button>
<!--                            <button data-title="<?php echo __(RESET) ?>" title="<?php echo __(RESET) ?>" class="btn btn-sm red filter-cancel"><i class="fs1 a2i_gn_reset2"></i>
                    </button>-->
                    <?php if($listtype=='inbox' ){ ?>
                            <button data-title="<?php echo __("Archive") ?>" title="<?php echo __("Archive") ?>" class="btn btn-sm purple movetoArchive"><i class="fs1 a2i_gn_nothi3"></i> 
                                
                    </button>
                    <?php } ?>
                </td>
            </tr>

            <tr role="row" class="heading">
                
                <th style="width: 5%" class='text-center'>
                    ক্রম
                </th>
                
                <th class='text-center' style="width: 20%">
                    নথি নম্বর
                </th>

                <th class='text-center' style="width: 20%">
                   বিষয়
                </th>
                
                <th class='text-center' style="width: 20%">
                    <?php if ($listtype != 'inbox' &&  $listtype != 'other') { ?>নথির অবস্থান <?php }else{
                        ?>সর্বশেষ নোটের তারিখ<?php
                    } ?>
                </th>
                 <th class='text-center'>
                    <?php if ($listtype != 'inbox') { ?>নথির অবস্থান<?php }
                        ?>

                </th>
                <th class='text-center' style="width: 20%">
                    নথির শাখা
                </th>
               
                <?php if ($listtype != 'office') : ?>
                    <th class='text-center' style="width: 5%">
                        কার্যক্রম
                    </th>
                <?php endif; ?>
                    <?php if($listtype == 'inbox'){?>
                <th style="width: 5%" class='text-center'>
                   আর্কাইভ
                </th>
                <?php } ?>
            </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
</div>


<script src="<?php echo CDN_PATH; ?>projapoti-nothi/js/tbl_nothi_masters.js"></script>


<script type="text/javascript">
    $('#view_nothi_list').show();
    $.extend(true, $.fn.dataTable.defaults, {
        "ordering": false
    });

    <?php if($listtype == 'inbox'){?>
    TableAjax.init('nothiMasters/nothiInboxList');
    <?php }elseif($listtype == 'sent'){?>
    TableAjax.init('nothiMasters/nothiMastersList/sent');
    <?php }elseif($listtype == 'office') {?>
    TableAjax.init('nothiMasters/nothiMastersList/office');
    <?php }elseif($listtype == 'other') {?>
        $('#view_nothi_list').hide();
    TableAjax.init('nothiMasters/nothiMastersList/other');
    <?php }elseif(empty($listtype) || $listtype == 'all') {?>
    TableAjax.init('nothiMasters/nothiMastersList/');
    <?php }?>
    $('.nothi-list-title').text('<?php if ($listtype == "office") { ?> নিজ শাখার নথিসমূহ  <?php }elseif ($listtype == "other") { ?>অন্যান্য অফিসের নথিসমূহ  <?php }elseif ($listtype == "inbox") { ?> আগত নথিসমূহ   <?php }elseif (empty($listtype) || $listtype == "all") { ?>সকল নথিসমূহ <?php } else { ?> প্রেরিত নথিসমূহ  <?php } ?>');

    $('[data-toggle="tooltip"]').tooltip({'placement':'bottom'});
    $('[title]').tooltip({'placement':'bottom'});
    $('[data-title]').tooltip({'placement':'bottom'});

</script>