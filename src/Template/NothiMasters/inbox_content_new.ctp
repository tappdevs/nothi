
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css"
      href="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css"
      href="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css"
      href="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-datepicker/css/datepicker.css"/>
<link href="<?php echo CDN_PATH; ?>daptorik_preview/css/custom.css" rel="stylesheet" type="text/css"/>

<!-- END PAGE LEVEL STYLES -->
<!-- END PAGE LEVEL STYLES -->
<style>
    tfoot {
        padding: 2px;
        background-color: rgba(241, 241, 241, 0.73);
    }

    .footer {
        display: none;
    }

    .dak_sender_cell_list {
        position: absolute;
        border: 2px solid #CECECE;
        background: #F5F5F5;
        padding-top: 15px;
        overflow: auto;
        z-index: 9000;
        color: #000;
        left: 15px;
        width: 100%;
        text-align: left;
    }

    .checkbox input[type=checkbox], .checkbox-inline input[type=checkbox], .radio input[type=radio], .radio-inline input[type=radio] {
        margin-left: -10px !important;
    }

    input[type=checkbox], input[type=radio] {
        margin-top: 2px;
    }

    div.radio, div.checker {
        margin-top: -2px;
    }

    .newInbox {
        background-color: #C7DAE0;
    }

    h3 {
        margin-top: 0px;
        margin-bottom: 0px;
    }
    .editable{
        border: none;
        word-break:break-word;
        word-wrap:break-word
    }
      .nav-tabs .dropdown-menu {
        font-size: 11pt!important;
    }
    .tab-content, .active{
        background-color:  #e9ffcc!important;
    }

    #dbl_click > tr > td{
        background-color: transparent;
    }

    #dbl_click > tr:hover {
        box-shadow: inset #cecece 0px 0px 4px 1px;
        cursor: pointer;
    }
</style>
<?php
$session = $this->request->session();
if ($session->check('refer_url')) {
    $listtype = $session->read('refer_url');
}
if (empty($listtype)) {
    $listtype = 'inbox';
}
?>
<ul class="nav nav-tabs nav-justified">
    <li class="inbox bold <?php if ($listtype == 'inbox') echo 'active'; ?>">
        <a href="#inbox" data-toggle="tab" aria-expanded="true" class="font-lg"> 
                        <?php if ($listtype == 'inbox' ){
               echo '<span class ="font-green">আগত নথি  <span class="badge badge-primary" style="display:none;padding: 2px 6px 3px 6px;" id="total_note_count">০</span></span>';
           }else{
                echo '<span class ="font-purple"> আগত নথি  <span class="badge badge-primary" style="display:none;padding: 2px 6px 3px 6px;" id="total_note_count">০</span></span>';
           }

           ?>

        </a>
    </li>
    <li class="sent bold <?php if ($listtype == 'sent') echo 'active'; ?>">
        <a href="#sent" data-toggle="tab" aria-expanded="false" class="font-lg"> 
                          <?php if ($listtype == 'sent' ){
               echo '<span class ="font-green">প্রেরিত নথি  </span>';
           }else{
                echo '<span class ="font-purple"> প্রেরিত নথি  </span>';
           }

           ?>
        </a>
    </li>
    <li class="dropdown bold other_office <?php if ($listtype == 'other' || $listtype == 'other_sent') echo 'active'; ?>">
        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" class="font-lg">
                             <?php if ($listtype == 'other' ||  $listtype == 'other_sent'){
               echo '<span class ="font-green"> অন্যান্য অফিসের নথি   <span class="badge badge-primary" style="display:none;padding: 2px 6px 3px 6px;" id="total_other_note_count">০</span></span>';
           }else{
                echo '<span class ="font-purple">  অন্যান্য অফিসের নথি   <span class="badge badge-primary" style="display:none;padding: 2px 6px 3px 6px;" id="total_other_note_count">০</span></span>';
           }
           ?>
            <i class="glyphicon glyphicon-chevron-down"></i>
        </a>
        <ul class="dropdown-menu" role="menu">
            <li class="other <?php if ($listtype == 'other') echo 'active'; ?>">
                <a href="#other" tabindex="-1" data-toggle="tab">অন্যান্য অফিস থেকে আগত নথি</a>
            </li>
            <li class="other_sent <?php if ($listtype == 'other_sent') echo 'active'; ?>">
                <a href="#other_sent" tabindex="-1" data-toggle="tab">অন্যান্য অফিসে প্রেরিত নথি</a>
            </li>
        </ul>
    </li>
    <li class="all bold <?php if ($listtype == 'all') echo 'active'; ?>">
        <a href="#all" data-toggle="tab" aria-expanded="false" class="font-lg">
                    <?php if ($listtype == 'all' ){
               echo '<span class ="font-green"> সকল নথি   </span>';
           }else{
                echo '<span class ="font-purple">  সকল নথি   </span>';
           }

           ?>
        </a>
    </li>
    <li class="archiveNothi bold <?php if ($listtype == 'archiveNothi') echo 'active'; ?> hidden">
        <a href="#archiveNothi" data-toggle="tab" aria-expanded="false" class="font-lg">
            <?php if ($listtype == 'archiveNothi' ){
                echo '<span class ="font-green"> নিষ্পত্তিকৃত নথি   </span>';
            }else{
                echo '<span class ="font-purple">  নিষ্পত্তিকৃত নথি   </span>';
            }

            ?>
        </a>
    </li>

</ul>
<div class="tab-content">
    <div class="tab-pane fade active in">
        <div class="table-container data-table-dak">
            <div class="pull-right" style="display: none;" id="Archive">
                
                    <?php if ($listtype == 'inbox' && 'achive'=='true') { ?>
                        <button data-title="<?php echo __("Archive") ?>" title="<?php echo __("Archive") ?>" class="btn btn-sm purple movetoArchive"><i class="fs1 a2i_gn_nothi3"></i>

                        </button>
                    <?php } ?>
                
            </div>
            <div style="clear:both"></div>
            <table class="table table-striped table-bordered table-hover" id="datatable-nothi-masters">
                <thead>

                    <tr role="row" class="heading">

                        <th style="width: 5%" class='text-center'>
                            ক্রম
                        </th>

                      
                        <?php
                        if($listtype == 'sent' || $listtype == 'other_sent'){
                        ?>
                          <th class='text-center' style="width: 15%">
                            নথি নম্বর
                        </th>
                        <th class='text-center' style="width: 20%">
                            শিরোনাম
                        </th>
                        <th class='text-center' style="width: 10%">
                           প্রেরণের তারিখ
                        </th>
                        <?php }else{?>
                          <th class='text-center' style="width: 20%">
                            নথি নম্বর
                        </th>
                        <th class='text-center' style="width: 25%">
                            শিরোনাম
                        </th>
                        <?php } ?>
                        <?php
                            if($listtype !='all' && $listtype !='archiveNothi'):
                        ?>
                        <th class='text-center' style="width: 20%">
                            <?php
                            if ($listtype != 'inbox' && $listtype != 'other') {
                                ?>নথির অবস্থান <?php
                            } else {
                                ?>সর্বশেষ নোটের তারিখ<?php }
                            ?>
                        </th>
                        <?php endif; ?>

                        <th class='text-center' style="width: 20%">
                            নথির শাখা
                        </th>
                        <?php /*if ($listtype != 'office') : */?><!--
                            <th class='text-center' style="width: 5%">
                                বিস্তারিত
                            </th>
                        <?php /*endif; */?>
                        <?php /*if ($listtype == 'inbox' && 'archive'=='true') { */?>
                            <th style="width: 5%" class='text-center'>
                                আর্কাইভ
                            </th>
                        --><?php /*} */?>
                    </tr>
                </thead>
                <tbody id="dbl_click">

                </tbody>
            </table>
        </div>


    </div>
</div>


<!-- Modal -->
<div id="archiveModal" class="modal fade modal-purple height-auto" data-backdrop="static" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">আর্কাইভ</h4>
      </div>
      <div class="modal-body">
          <p class="font-lg">
            নির্বাচিত নথির শুধু মাত্র  নিষ্পন্ন নোটগুলো আর্কাইভ হবে।  আপনি কি নথিটি  আর্কাইভ করতে চান?
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" id="goforArchive"><?=__('Archive'). " করুন" ?></button>
        <button type="button" class="btn btn-danger" data-dismiss="modal"><?=__('Close') ?></button>
      </div>
    </div>

  </div>
</div>

<div id="detailsModal" class="modal fade modal-purple" data-backdrop="static" role="dialog">
    <div class="modal-dialog  modal-full">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">বিস্তারিত</h4>
            </div>
            <div class="modal-body">
<!--                <div class="input-group ">-->
<!--                    <input type="text" class="form-control input-sm" placeholder="বিষয় দিয়ে  খুঁজুন" id="sub_search">-->
<!--                    <input type="hidden" id="data-nothimasterid" value="">-->
<!--                    <input type="hidden" id="data-type" value="">-->
<!--                    <span class="input-group-btn">-->
<!--                            <button type="button" class="btn btn-sm green-haze " id="filter_submit_btn1">-->
<!--                                &nbsp; <i class="fa fa-search" aria-hidden="true"></i>-->
<!--                            </button></span>-->
<!--                </div>-->
                <div class="details-modal-inbox-content">
                </div>

            </div>
            <div class="modal-footer">
	            <div class="btn-group btn-group-round">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fs1 efile-close1"></i> <?=__('Close') ?></button>
                    <button type="button" class="btn btn-warning newPartCreate" nothi_parts_id=0 nothi_masters_id=0><i class="fs1 a2i_gn_note2"></i> নতুন নোট</button>
                    <a type="button" class="btn btn-primary btn-sokol" href=""><i class="fs1 a2i_gn_note1"></i> সকল নোট</a>
	            </div>
            </div>
        </div>

    </div>
</div>

<script src="<?php echo CDN_PATH; ?>projapoti-nothi/js/tbl_nothi_masters.js?v=<?=js_css_version?>"></script>


<script type="text/javascript">
    $('#view_nothi_list').show();
    $('.inbox-loading').html('');

    $.extend(true, $.fn.dataTable.defaults, {
        "ordering": false
    });
<?php if ($listtype == 'inbox') {
    ?>
        $("#Archive").show();
        $("#nothi-type-id").prop("disabled", false);
        $("#show-only-important").prop("disabled", false)
        TableAjax.init('nothiInbox');
    <?php
} elseif ($listtype == 'sent') {
    ?>
    $("#show-only-important").val(0);
    $("#show-only-important").select2();
    $("#nothi-type-id").prop("disabled", false);
    $("#show-only-important").prop("disabled", true);
        TableAjax.init('nothiMasters/nothiMastersList/sent');
    <?php
} elseif ($listtype == 'office') {
    ?>
        TableAjax.init('nothiMasters/nothiMastersList/office');
    <?php
} elseif ($listtype == 'other') {
    ?>
        $('#view_nothi_list').hide();
        $("#nothi-type-id").prop("disabled", true);
        $("#show-only-important").prop("disabled", false);
        TableAjax.init('nothiMasters/nothiMastersList/other');
    <?php
} 
elseif ($listtype == 'other_sent') {
    ?>
        $('#view_nothi_list').hide();
    $("#show-only-important").val(0);
    $("#show-only-important").select2();
    $("#nothi-type-id").prop("disabled", true);
    $("#show-only-important").prop("disabled", true);
        TableAjax.init('nothiMasters/nothiMastersList/other_sent');
    <?php
}
elseif (empty($listtype) || $listtype == 'all') {
    ?>
        $("#nothi-type-id").prop("disabled", false);
        $("#show-only-important").prop("disabled", false);
        TableAjax.init('nothiAll');
    <?php
}
elseif (empty($listtype) || $listtype == 'archiveNothi') {
    ?>
        $("#nothi-type-id").prop("disabled", false);
        TableAjax.init('archiveNothi');
    <?php
} ?>

    $('[data-toggle="tooltip"]').tooltip({placement: 'bottom'});
    $('[title]').tooltip({placement: 'bottom'});
    $('[data-title]').tooltip({placement: 'bottom'});

    $(document).on('click', '#dbl_click', function (e) {
        var target = $(e.target);
        if (target.is('.dbl_click')) {
            //console.log('clicked on row');
            if (target.prop('href') != undefined) {
                window.location.href = target.prop('href');
            }
        } else {
            if ($('.nav.nav-tabs.nav-justified li.inbox.active').length > 0 || $('.nav.nav-tabs.nav-justified li.all.active').length > 0) {
                target.closest('tr').find('.dbl_click').trigger('click');
            } else {
                if (target.closest('tr').find('.dbl_click').prop('href') != undefined) {
                    var url = target.closest('tr').find('.dbl_click').prop('href');
                    window.location.href = url;
                }
            }
        }
    });


    // $('#dbl_click').on("click", 'tr', function (e) {
    //     console.log($( e.currentTarget ));
    //     console.log($(this).children('td').find('.dbl_click'));
    //     $(this).children('td').find('.dbl_click').trigger('click');
    //     // var si =  $(this).children('td').find('a.dbl_click').attr('href');
    //     // if(typeof(si) == 'undefined' || si == '' || si == null)
    //     //   return;
    //     // window.location.href = si;
    // });

    <?php if ($listtype == 'all' || $listtype == 'inbox'): ?>
    $('.newPartCreate').on('click', function () {
        var that = this;
        bootbox.dialog({
            message: "আপনি কি নতুন নোট তৈরি করতে চান?",
            title: "নতুন নোট",
            buttons: {
                success: {
                    label: "হ্যাঁ",
                    className: "green",
                    callback: function () {
                        createNoteNew();
                    }
                },
                danger: {
                    label: "না",
                    className: "red",
                    callback: function () {
                        Metronic.unblockUI('.page-container');
                    }
                }
            }
        });

        var nothimasterid = parseInt($(that).attr('nothi_masters_id'));

        if (nothimasterid > 0) {
        } else {
            return false;
        }

        //if (confirm("আপনি কি নতুন নোট তৈরি করতে চান?")) {
        function createNoteNew() {
            Metronic.blockUI({
                target: '.page-container',
                boxed: true
            });
            $('.nothiCreateButton').attr('disabled', 'disabled');
            $.ajax({
                url: '<?php echo $this->Url->build(['controller' => 'NothiMasters', 'action' => 'add']) ?>',
                data: {formPart: {nothi_master_id: nothimasterid}, priviliges: {}},
                type: "POST",
                dataType: 'JSON',
                success: function (response) {
                    $('div.error-message').remove();
                    if (response.status == 'error') {
                        Metronic.unblockUI('.page-container');
                        $('.nothiCreateButton').removeAttr('disabled');
                        toastr.error(response.msg);

                        if (typeof (response.data) != 'undefined') {
                            var errors = response.data;
                            $.each(errors, function (i, value) {
                                $.each(value, function (key, val) {
                                    if (i != 'office_units_organogram_id') {
                                        $('[name=' + i + ']').focus();
                                        $('[name=' + i + ']').after('<div class="error-message">' + val + '</div>');
                                    }
                                });
                            });
                        }
                    } else if (response.status == 'success') {
                        if(!isEmpty(response.extra_message)){
                            bootbox.dialog({
                                message: response.extra_message,
                                title: "নোট তৈরির সময়কার সমস্যা",
                                buttons: {
                                    success: {
                                        label: "নতুন তৈরিকৃত নোটে গমন",
                                        className: "green",
                                        callback: function () {
                                            window.location.href = js_wb_root + 'noteDetail/' + response.id;
                                        }
                                    },
//                                danger: {
//                                    label: "না",
//                                    className: "red",
//                                    callback: function () {
//                                        Metronic.unblockUI('.page-container');
//                                    }
//                                }
                                }
                            });
                            return;
                        }
                        window.location.href = js_wb_root + 'noteDetail/' + response.id;
                        //window.location.href = js_wb_root + 'nothiMasters/nothiDetails/' + response.id;
                    }
                },
                error: function (status, xresponse) {

                }

            });
        }

        return false;
    });
    <?php endif; ?>

	$(document).ready(function() {
	    <?php if($totalNoteCount > 0):?>
            $("#total_note_count").text(enTobn('<?=$totalNoteCount?>'));
            $("#total_note_count").show();
	    <?php endif;?>

        <?php if($totalOtherNoteCount > 0):?>
            $("#total_other_note_count").text(enTobn('<?=$totalOtherNoteCount?>'));
            $("#total_other_note_count").show();
	    <?php endif;?>
        $('.dataTables_paginate').find('.prev').css('cssText', 'border-radius:5px 0 0 5px !important');
        $('.dataTables_paginate').find('.next').css('cssText', 'border-radius:0 5px 5px 0 !important');
        $('.dataTables_paginate').find('input').css('cssText', 'margin:0 !important;text-align:center');
        <?php
          if($listtype == 'other' || $listtype == 'other_sent'){
        ?>
            $(".nothi_inbox_list_title").html("<b>অন্যান্য অফিসের  <?=  (($listtype == 'other')? ' আগত ' : ' প্রেরিত ' )?> নথি</b>");
        <?php } ?>
	});
</script>