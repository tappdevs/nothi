<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"/>

<style>
    .inbox .inbox-nav li.active b {
        display: none !important;
    }

    .tooltip-inner {
        white-space: pre-line;
    }

    h3 {
        margin-top: 0px;
        margin-bottom: 0px;
    }

    select.input-xsmall{
        height: 25px!important;
        line-height: 25px!important;
        font-size: 10pt!important;
        padding: 2px;
    }

    .nav-tabs > li > a, .nav-pills > li > a{
        font-size: 11pt;
    }
</style>

<div class="portlet light" style="padding: 0px 0px 0px 0px;">
    <!--    <div class="portlet-title">
            <div class="caption">

            </div>

            <div class="actions">
                <select class="form-control input-sm input-xsmall view_nothi_list" id="view_nothi_list">
                    <option value="list" selected="selected"><?php // echo __("লিস্ট")  ?></option>
                    <option value="folder"><?php // echo __("ফোল্ডার")  ?></option>
                </select>
            </div>
        </div>-->
    <div class="portlet-body">
        <div class="row inbox">
            <div class="col-md-12">
                <div class="inbox-loading" style="display: none;">
<!--                    <img src="<?php // echo CDN_PATH;  ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp;লোড হচ্ছে। একটু অপেক্ষা করুন... </span>-->
                </div>
                <div class="inbox-content">

                </div>
            </div>
        </div>
    </div>
</div>
</div>


<div id="responsiveNothiUsers" class="modal fade " tabindex="-1" aria-hidden="true"
     data-backdrop="static"
     data-keyboard="false">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-hidden="true"></button>
                <h4 class="modal-title">পরবর্তী কার্যক্রমের জন্য প্রেরণ করুন</h4>
            </div>
            <div class="modal-body">
                <div class="scroller" style="height:100%" data-always-visible="1"
                     data-rail-visible1="1">
                    <input type="hidden" name="nothimasterid"/>

                    <div class="user_list">

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn green sendNothi">প্রেরণ করুন</button>
                <button type="button" data-dismiss="modal" class="btn  btn-danger">
                    বন্ধ করুন
                </button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<script src="<?php echo $this->request->webroot; ?>projapoti-nothi/js/nothi_master_movements.js?v=<?= js_css_version ?>"></script>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript"
src="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript"
src="<?php echo $this->request->webroot; ?>assets/global/plugins/datatables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript"
src="<?php echo $this->request->webroot; ?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script src="<?php echo $this->request->webroot; ?>assets/global/scripts/datatable.js"></script>
<script src="<?php echo CDN_PATH; ?>assets/admin/pages/scripts/table-advanced.js"></script>
<script>

    NothiMasterMovement.init('<?php echo $listtype; ?>');


    jQuery(document).ready(function () {
        $(".form_date").datepicker({
            autoclose: true,
            isRTL: Metronic.isRTL(),
            format: "yyyy-mm-dd",
            pickerPosition: (Metronic.isRTL() ? "bottom-right" : "bottom-left")
        });
    });
</script>
