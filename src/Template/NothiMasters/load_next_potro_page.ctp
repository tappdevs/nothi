<?php
foreach ($draftPotro as $row) {
    echo '<div id_en="' . $row['nothi_potro_page'] . '"  id_bn="' . $row['nothi_potro_page_bn'] . '" class="DraftattachmentsRecord">';
    ?>
    <div style="background-color: #a94442; padding: 5px; color: #fff;">
        <div class="pull-left"
             style="padding-top: 0px;    white-space: nowrap;">
            <?php if ($row['cloned_potrojari_id']): ?>
                <i class="fs1 a2i_gn_hardcopy3"
                   title="এই পত্রটি ক্লোন করা হয়েছে"
                   data-toggle="tooltip"></i>
            <?php endif; ?>
            <?php echo mb_substr($row['potro_subject'], 0, 50) . (mb_strlen(h($row['potro_subject'])) > 50 ? '...' : ''); ?>
        </div>
        <div class="pull-right text-right">
            <div class="btn-group">
                <button type="button"
                        class="btn btn-success btn-xs dropdown-toggle"
                        data-toggle="dropdown"
                        aria-expanded="false">
                    <i class="fa fa-ellipsis-horizontal"></i>
                    প্রিন্ট <i class="fa fa-angle-down"></i>
                </button>
                <ul class="dropdown-menu pull-right">
                    <li>
                        <a title="প্রিন্ট প্রিভিউ"
                           data-id="<?= $row['id'] ?>"
                           data-potro-type="<?= $row['potro_type'] ?>"
                           class="btn-printdraft"><i
                                    class="fs1 a2i_gn_print2"></i>
                            প্রিন্ট প্রিভিউ</a>
                    </li>
                </ul>
            </div>

            <a title="ড্রাফট পরিবর্তন দেখুন"
               nothi_master_id="<?php echo $row['nothi_part_no'] ?>"
               potrojari="<?php echo $row['id'] ?>"
               class="btn   btn-primary btn-xs btn-changelog">
                <i class="fs1 a2i_gn_history3"></i>
            </a>
            <?php
            //RM url different
            if (($row['potro_type'] == 21)) {
                $url = $this->Url->build(['_name' => 'editRm', $row['nothi_part_no'], 0, ($row['id'] == 0) ? 'note' : 'potro', 0, $row['id']]);
            } // formal
            else if (($row['potro_type'] == 27)) {
                $url = $this->Url->build(['_name' => 'edit-formal-letter', $row['nothi_part_no'], 0, ($row['id'] == 0) ? 'note' : 'potro', 0, $row['id']]);
            } // CS
            else if (($row['potro_type'] == 30)) {
                $url = $this->Url->build(['_name' => 'edit-cs-letter', $row['nothi_part_no'], 0, ($row['id'] == 0) ? 'note' : 'potro', 0, $row['id']]);
            } else {
                $url = $this->Url->build(['controller' => 'Potrojari', 'action' => 'potroDraft', $row['nothi_part_no'], $row['id'], ($row['id'] == 0) ? 'note' : 'potro', $nothi_office]);
            }
            ?>
            <a data-title-orginal="খসড়া পত্র দেখুন"
               title="খসড়া পত্র দেখুন"
               href="<?php echo $url; ?>"
               class="btn   btn-xs green">
                <i class="fs1 a2i_nt_potrojari3"></i>
            </a>
            <?php
            // endif;

            if ($privilige_type == 1):
                ?>
                <?php
                if ($row['can_potrojari']
                    == 1 && $row['approval_officer_designation_id']
                    == $employee_office['office_unit_organogram_id']
                ) {
                    ?>
                    <!--
               <?php
                    echo $this->Form->postLink('<i class="fs1 a2i_gn_send1"></i>', ['controller' => 'Potrojari',
                        'action' => 'sendDraftCronPotrojari', $id, $row['id'], ($row['nothi_notes_id'] == 0) ? 'potro' : 'note'], ['confirm' => __('আপনি কি খসড়া পত্র পত্রজারি করতে চান?'), 'class' => "btn   btn-xs purple-medium", 'escape' => false, 'data' => ['nothi_office' => $nothi_office, 'redirect' => 1]]);
                    ?>
            -->
                    <a data-title-orginal="পত্রজারি করুন"
                       title="পত্রজারি করুন"
                       class="btn   btn-xs purple btn-send-draft-cron"
                       nothinotesid="<?php echo $row['nothi_notes_id'] ?>"
                       nothipotroid="<?php echo $row['nothi_potro_id'] ?>"
                       potrojari="<?php echo $row['id'] ?>"
                       part_no="<?php echo $row['nothi_part_no'] ?>"
                       nothi_office="<?php echo $nothi_office ?>"
                    >
                        <i class="fs1 a2i_gn_send1"></i>
                    </a>
                    <?php
                } // if onumodon and sender same , then anyone can potrojari
                else if ($row['can_potrojari']
                    == 1 && $row['approval_officer_designation_id']
                    == $row['officer_designation_id'] && $row['potro_type'] != 17
                ) {
                    ?>
                    <!--
               <?php
                    echo $this->Form->postLink('<i class="fs1 a2i_gn_send1"></i>', ['controller' => 'Potrojari',
                        'action' => 'sendDraftCronPotrojari', $id, $row['id'], ($row['nothi_notes_id'] == 0) ? 'potro' : 'note'], ['confirm' => __('আপনি কি খসড়া পত্র পত্রজারি করতে চান?'), 'class' => "btn   btn-xs purple-medium", 'escape' => false, 'data' => ['nothi_office' => $nothi_office, 'redirect' => 1]]);
                    ?>
            -->
                    <a data-title-orginal="পত্রজারি করুন"
                       title="পত্রজারি করুন"
                       class="btn   btn-xs purple btn-send-draft-cron"
                       nothinotesid="<?php echo $row['nothi_notes_id'] ?>"
                       nothipotroid="<?php echo $row['nothi_potro_id'] ?>"
                       potrojari="<?php echo $row['id'] ?>">
                        <i class="fs1 a2i_gn_send1"></i>
                    </a>

                    <?php
                } elseif ($row['potro_type']
                    != 17 && $employee_office['office_unit_organogram_id']
                    == $row['officer_designation_id']
                ) {
                    ?>
                    <input type="checkbox"
                           class=" approveDraftNothi <?php
                           echo ($row['can_potrojari'])
                               ? 'checked' : ''
                           ?>" <?php
                    echo ($row['can_potrojari'])
                        ? 'checked=checked'
                        : ''
                    ?>
                           potrojari_id="<?= $row['id'] ?>"/><?php echo __('অনুমোদন  ') ?>
                    <?php
                } else if ($row['potro_type']
                    == 17 && $employee_office['office_unit_organogram_id']
                    == $row['sovapoti_officer_designation_id']
                ) {
                    ?>
                    <input type="checkbox"
                           class=" approveDraftNothi <?php
                           echo ($row['can_potrojari'])
                               ? 'checked' : ''
                           ?>" <?php
                    echo ($row['can_potrojari'])
                        ? 'checked=checked'
                        : ''
                    ?>
                           potrojari_id="<?= $row['id'] ?>"/><?php echo __('অনুমোদন  ') ?>
                <?php } ?>
                <?php
                if ($row['can_potrojari']
                    != 1
                ):

                    ?>
                    <a data-title-orginal="খসড়া পত্র মুছে ফেলুন"
                       title="খসড়া পত্র মুছে ফেলুন"
                       class="btn   btn-xs red btn-delete-draft"
                       nothinotesid="<?php echo $row['nothi_notes_id'] ?>"
                       nothipotroid="<?php echo $row['nothi_potro_id'] ?>"
                       potrojari="<?php echo $row['id'] ?>">
                        <i class="fs1 a2i_gn_delete2 "></i>
                    </a>
                <?php endif; ?>
            <?php endif; ?>
        </div>
        <div style="clear: both;"></div>
    </div>
    <?php
    $header = jsonA($row->meta_data);
    $cs_checker = (($privilige_type == 0) || (isset($header['has_signature']) && $header['has_signature'] == $employee_office['office_unit_organogram_id'])) ? '' :
        '(<input type="checkbox" class ="checker cs-body-approve" data-cs="' . $row['id'] . '" /> ' . __('Approval') . ' )';

    if (isset($row['potro_type']) && $row['potro_type'] == 30) {
        $hasCsTemplate = 1;
        echo '<ul class="nav nav-tabs nav-justified">
                <li class="active">
                    <a href="#csCover-' . $row['id'] . '" data-toggle="tab" aria-expanded="true">ফরওয়ার্ডিং পত্র</a>
                </li>
                <li class="">
                    <a href="#csBody-' . $row['id'] . '" data-toggle="tab" aria-expanded="true">প্রতিবেদন ' . $cs_checker . '</a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active csCover" id="csCover-' . $row['id'] . '"">' . (!empty($header['potro_header_banner']) ? ('<div class="potrojari_header_banner" style="text-align: ' . ($header['banner_position']) . '!important;"><img src="' . $this->request->webroot . 'getContent?file=' . base64_decode($header['potro_header_banner']) . '&token=' . sGenerateToken(['file' => base64_decode($header['potro_header_banner'])], ['exp' => time() + 60 * 300]) . '" style="width:' . ($header['banner_width']) . ';height: 60px;max-width: 100%;"/></div>') : '')
            . '<div class="showimageforce">' . html_entity_decode($row['attached_potro']) . '</div><br/>' . $row['potro_cover'] . '</div>';
        echo '<div class="tab-pane csBody" id="csBody-' . $row['id'] . '"">';
        echo '<div id="template-body" potrojari_language="' . h($row['potrojari_language']) . '" style="background-color: #fff; max-width:950px; min-height:815px; height: auto; margin:0 auto;" potro_type="' . $row['potro_type'] . '">'
            . html_entity_decode($row['potro_description']) . "</div>";
        echo '</div></div>';
    } else {
        echo '<div id="template-body" potrojari_language="' . h($row['potrojari_language']) . '" style="background-color: #fff; max-width:950px; min-height:815px; height: auto; margin:0 auto;" potro_type="' . $row['potro_type'] . '">' . (!empty($header['potro_header_banner']) ? ('<div class="potrojari_header_banner" style="text-align: ' . ($header['banner_position']) . '!important;"><img src="' . $this->request->webroot . 'getContent?file=' . base64_decode($header['potro_header_banner']) . '&token=' . sGenerateToken(['file' => base64_decode($header['potro_header_banner'])], ['exp' => time() + 60 * 300]) . '" style="width:' . ($header['banner_width']) . ';height: 60px;max-width: 100%;"/></div>') : '') . '<div class="showimageforce">' . html_entity_decode($row['attached_potro']) . '</div><br/>' . html_entity_decode($row['potro_description']) . "</div>";
    }
    ?>
    <div class="table">
        <table role="presentation"
               class="table table-bordered table-striped">

            <tbody class="files">
            <?php

            if (isset($draftPotroAttachments[$row['id']])
                && count($draftPotroAttachments[$row['id']])
                > 0
            ) {
                $total_attachment = 0;
                echo '<tr class="text-center">
                    <td colspan="3">' . __('Attachments') . '</td>
            </tr>';
                foreach ($draftPotroAttachments[$row['id']] as $ke => $single_data) {

                    if (($single_data['attachment_type']
                            == 'text' || $single_data['attachment_type']
                            == 'text/html') && empty($single_data['potro_id'])
                    ) {
                        continue;
                    }
                    if (!(empty($single_data['file_name']))) {
                        $fileName = explode('/',
                            $single_data['file_name']);
                        $attachmentHeaders
                            = get_file_type($single_data['file_name']);
                        $value = array(
                            'name' => urldecode($fileName[count($fileName)
                            - 1]),
                            'thumbnailUrl' => $this->request->webroot . 'content/' . $single_data['file_name'],
                            'url' => $this->request->webroot . 'content/' . $single_data['file_name'],
                            'visibleName' => (!empty($single_data['user_file_name']) ? $single_data['user_file_name'] : urldecode($fileName[count($fileName) - 1])),
                        );
                    } else {
                        $value = array(
                            'name' => 'পত্রঃ ' . $single_data['sarok_no'],
                            'thumbnailUrl' => '',
                            'url' => '',
                            'visibleName' => '',
                        );
                    }
                    $total_attachment++;
                    echo '<tr class="template-download fade in">

<td>
<p class="name">
' . (isset($value['name']) && (substr($single_data['attachment_type'], 0, 5) == 'image' ||
                            $single_data['attachment_type'] == 'application/pdf') ?
                            '<a href="' . $single_data['id'] . '/showPotroAttachment/' . $row['nothi_part_no'] . '" 
title="' . $value['name'] . '" class="showforPopup">' . urldecode($value['name']) . '</a>' : (isset($value['name']) ? urldecode($value['name']) : '')) . '
</p>
</td>
<td>' . $value['visibleName'] . '</td>
<td>
' . (($single_data['attachment_type'] != 'text' && $single_data['attachment_type'] != 'text/html') ? $this->Html->link('<i class="fs1 fa fa-download"></i> &nbsp;', ['controller' => 'Potrojari',
                            'action' => 'downloadPotro',
                            $nothi_office,
                            $single_data['id']], ['escape' => false]) : '') . '
</td>

</tr>';
                }
                if ($total_attachment == 0) {
                    echo '<tr class="template-download fade in"><td colspan="3" class="danger text-center"> ' . __('Sorry no attachment found') . '</td></tr>';
                }
            }
            ?>
            </tbody>
        </table>
    </div>
    <?php
    echo "</div>";
}