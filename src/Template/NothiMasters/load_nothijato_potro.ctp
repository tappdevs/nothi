
    <div class="portlet light ">
        <div class="portlet-title">
            <div class="row">
                <div class="col-lg-7 col-md-7 col-sm-7 text-left">
                    <nav>
                        <ul class="pager">
                            <li><a href="javascript:void(0);"
                                   class="NothijatoimagePrev btn btn-sm default disabled"><</a>
                            </li>
                            <li><a href="javascript:void(0);"
                                   class="NothijatoimageNext btn btn-sm default">></a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="col-lg-5 col-md-5 col-sm-5 text-right">
                    <p> মোট:
                        <b><?php echo $this->Number->format(count($potroNothijatoAttachmentRecord)); ?>
                            টি</b></p>
                </div>
            </div>

        </div>
        <div class="portlet-body">
            <div class="row divcenter">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="potroview">
                        <?php
                        if (!empty($potroNothijatoAttachmentRecord)) {
                            $i = 0;
                            foreach ($potroNothijatoAttachmentRecord as $row) {
                                echo '<div id_en="' . $row['nothi_potro_page'] . '"  id_bn="' . $row['nothi_potro_page_bn'] . '" class="NothijatoattachmentsRecord ' . ($i
                                    == 0 ? 'active first' : ($i == (count($potroNothijatoAttachmentRecord)
                                        - 1) ? 'last' : '')) . '">';
                                ?>
                                <div style="background-color: #a94442; padding: 5px; color: #fff;">
                                    <div class="pull-left"
                                         style="padding-top: 0px;    white-space: nowrap;">
                                        স্মারক নম্বর:
                                        &nbsp;&nbsp;<?php echo $row['sarok_no']; ?>
                                    </div>
                                    <div class="pull-right text-right">

                                        <?php
                                        if ($row['attachment_type'] == 'text'
                                            || substr($row['attachment_type'],
                                                0, 5) == 'image'
                                        ) {
                                            ?>
                                            <a title="প্রিন্ট করুন"
                                               href="javascript:void(0)"
                                               class="btn green   btn-sm btn-printnothijato"><i
                                                    class="fs1 a2i_gn_print2"></i></a>
                                        <?php } ?>

                                    </div>
                                    <div style="clear: both;"></div>
                                </div>
                                <?php
                                $row['attachment_type'] = str_replace("; charset=binary", "", $row['attachment_type']);
                                if ($row['attachment_type']== 'text') {
                                    if ($row['is_summary_nothi'] == 1) {
                                        echo '<div data-type="text" style="background-color: #fff; max-width:950px; min-height:815px; max-height: 815px; margin:0 auto; page-break-inside: auto;"><div style="background-color: #fff; max-width:950px; min-height:815px; max-height: 815px;">' . html_entity_decode($row['potro_cover']) . "</div>" . html_entity_decode($row['content_body']) . "</div>";
                                    } else {
                                        echo '<div  data-type="text" style="background-color: #fff; max-width:950px; min-height:815px; max-height: 815px; margin:0 auto; page-break-inside: auto;">' . html_entity_decode($row['content_body']) . "</div>";
                                    }
                                }
                                elseif (substr($row['attachment_type'], 0, 5) != 'image') {
                                    if (substr($row['attachment_type'], 0, strlen('application/vnd')) == 'application/vnd' || substr($row['attachment_type'], 0, strlen('application/ms')) == 'application/ms') {
                                        $url = urlencode(FILE_FOLDER.$row['file_name'].'?token='.sGenerateToken(['file'=>$row['file_name']],['exp'=>time() + 60*300]));
                                        echo '<iframe style=" width:100%; height: 700px;" src="https://docs.google.com/gview?url='. $url.'&embedded=true" style="width:600px; height:500px;" frameborder="0"></iframe>';
                                    } else {
                                        echo '<embed src="'. $this->request->webroot . 'getContent?file='. $row['file_name'].'&token='.sGenerateToken(['file'=>$row['file_name']],['exp'=>time() + 60*300]).'" style=" width:100%; height: 700px;" type="'.$row['attachment_type'].'"></embed>';
                                    }
                                } else {
                                    echo '<div class="text-center"><img class=" img-responsive"  src="'.$this->request->webroot . 'getContent?file=' . $row['file_name'].'&token='.sGenerateToken(['file'=>$row['file_name']],['exp'=>time() + 60*300]).'" alt="ছবি পাওয়া যায়নি"></div>';
                                }
                                echo "</div>";
                                $i++;
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
