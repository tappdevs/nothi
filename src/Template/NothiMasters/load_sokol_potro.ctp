<div class="portlet light">
    <div class="portlet-title">
        <div class="row">
            <div class="col-lg-7 col-md-7 col-sm-7 text-left">
                <nav>
                    <ul class="pager">
                        <li><a href="javascript:void(0);"
                               class="imageallPrev btn btn-sm default disabled"><</a>
                        </li>
                        <li>
                            <input value="<?php
                            echo $this->Number->format(!empty($potroAllAttachmentRecord)
                                ? $potroAllAttachmentRecord[0]['nothi_potro_page']
                                : 0)
                            ?>" type="text"
                                   class="imageNo pagination-panel-input form-control input-mini input-inline input-sm potroallNo"
                                   maxlength="20"
                                   style="text-align:center; margin: 0 5px;">
                        </li>
                        <li><a href="javascript:void(0);"
                               class="imageallNext btn btn-sm default">></a>
                        </li>
                        <?php
                        if (count($potroAllAttachmentRecord) > 0):
                            ?>
                            <li class="bookmarkImg" title="পতাকা">
                                <i class="fa fa-bookmark-o bookmarkallImgPotro"></i>
                            </li>
                        <?php endif; ?>
                    </ul>
                </nav>
            </div>
            <div class="col-lg-5 col-md-5 col-sm-5 text-right">
                <p> মোট:
                    <b><?php echo $this->Number->format(count($potroAllAttachmentRecord)); ?>
                        টি</b></p>
            </div>
        </div>

    </div>
    <div class="portlet-body">
        <div class="row divcenter">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="potroview">
                    <?php
                    if (!empty($potroAllAttachmentRecord)) {
                        $i = 0;
                        foreach ($potroAllAttachmentRecord as $row) {

                            echo '<div id_ar="' . $row['id'] . '" id_en="' . $row['nothi_potro_page'] . '"  id_bn="' . $row['nothi_potro_page_bn'] . '" data-content-type="'.$row['attachment_type'].'" class="attachmentsallRecord ' . ($i
                                == 0 ? 'active first' : ($i == (count($potroAllAttachmentRecord)
                                    - 1) ? 'last' : '')) . '">';
                            ?>
                            <div style="background-color: #a94442; padding: 5px; color: #fff;">
                                <div class="pull-left"
                                     style="padding-top: 0px;    white-space: nowrap;">
                                    <?php if($row['potrojari_data']['cloned_potrojari_id']): ?>
		                                <i class="fs1 a2i_gn_hardcopy3" title="এই পত্রটি ক্লোন করা হয়েছে" data-toggle="tooltip"></i>
                                    <?php endif; ?>
                                    <?php echo mb_substr(h($row['nothi_potro']['subject']),0,50) . (mb_strlen(h($row['nothi_potro']['subject']))> 50 ?'...' : ''); ?>
                                </div>
                                <div class="pull-right text-right">
                                    <?php
                                    if ($row['attachment_type'] == 'text' ) {

                                        if($row['is_summary_nothi'] == 0 &&  !empty($row['potrojari_id'])
                                            && empty($row['potrojari_data']['attached_potro_id'])
                                            && $row['potrojari_data']['potrojari_draft_office_id'] == $employee_office['office_id']){
											if(isset($_GET['permitted_by']) && $_GET['permitted_by'] != '') {
												//
											} else {
												?>
												<button type="button" class="btn   btn-sm btn-warning btn-resizecss"
												        onclick="createClonePotrojari(<?= $row['potrojari_id'] ?>,<?= $row['nothi_master_id'] ?>)"
												        data-toggle="tooltip" title="পত্রজারি ক্লোন করুন">
													<span class="glyphicon glyphicon-copy"></span></button>
												<?php
											}
                                        }
                                        ?>
                                        <a data-id="<?= $row['id'] ?>" title="প্রিন্ট করুন"
                                           href="javascript:void(0)"
                                           class="btn    green btn-sm btn-printall"><i
                                                class="fs1 a2i_gn_print2"></i></a>
                                    <?php } ?>
                                    <?php if ($row['attachment_type'] != 'text' && substr($row['attachment_type'], 0, 5) != 'image') { ?>
                                        <a title="ডাউনলোড করুন" href="<?=
                                        $this->Url->build(['controller' => 'NothiNoteSheets',
                                            'action' => 'downloadPotro',
                                            h($nothi_office),
                                            $row['id']])
                                        ?>" class="btn   blue btn-xs"><i
                                                    class="fs1 fa fa-download"></i></a>
                                    <?php  } ?>
                                </div>
                                <div style="clear: both;"></div>
                            </div>
                            <?php
                            if ($row['attachment_type'] == 'text') {

                                if ($row['is_summary_nothi'] == 1) {
                                    echo '<div style="background-color: #fff; max-width:950px; min-height:815px;  margin:0 auto; page-break-inside: auto; ">' . html_entity_decode($row['potro_cover']) . '<br/>' . html_entity_decode($row['content_body']) . "</div>";
                                } else {
                                    $header = jsonA($row['meta_data']);
                                    echo '<div style="background-color: #fff; max-width:950px; min-height:815px;  margin:0 auto; page-break-inside: auto; ">' . (!empty($header['potro_header_banner'])?('<div style="text-align: '.($header['banner_position']).'!important;"><img src="'.$this->request->webroot . 'getContent?file='. base64_decode($header['potro_header_banner']).'&token=' . sGenerateToken(['file'=>base64_decode($header['potro_header_banner'])],['exp'=>time() + 60*300]) .'" style="width:'.($header['banner_width']).';height: 60px;max-width: 100%;"/></div>'):'') . html_entity_decode($row['content_body']) . "</div>";
                                }
                            } elseif (substr($row['attachment_type'],
                                    0, 5) != 'image'
                            ) {
                                if (substr($row['attachment_type'],
                                        0, strlen('application/vnd'))
                                    == 'application/vnd' || substr($row['attachment_type'],
                                        0, strlen('application/ms'))
                                    == 'application/ms'
                                ) {
                                    $url = urlencode(FILE_FOLDER. $row['file_name'].'?token='.sGenerateToken(['file'=>$row['file_name']],['exp'=>time() + 60*300]));
                                    echo '<iframe style=" width:100%; height: 700px;" src="https://docs.google.com/gview?url=' . $url. '&embedded=true" style="width:600px; height:500px;" frameborder="0"></iframe>';
                                } else {
                                    echo '<embed src="' . $this->request->webroot . 'getContent?file=' . $row['file_name'] .'&token='.sGenerateToken(['file'=>$row['file_name']],['exp'=>time() + 60*300]). '" style=" width:100%; height: 700px;" type="' . $row['attachment_type'] . '"></embed>';
//                echo '<iframe style=" width:100%; height: 700px;" src="https://docs.google.com/gview?url='.$this->request->webroot . 'content/'.$row['file_name'].'&embedded=true" style="width:600px; height:500px;" frameborder="0"></iframe>';
                                }
                            } else {
                                echo '<div class="text-center"><a onclick="return false;" href="' . $this->request->webroot . 'content/' . $row['file_name'] .'?token='.sGenerateToken(['file'=>$row['file_name']],['exp'=>time() + 60*300]). '" data-title="পত্র নম্বর:  ' . $row['nothi_potro_page_bn'] . '" data-footer="" ><img class="zoomimg img-responsive"  src="' . $this->request->webroot . 'content/' . $row['file_name'] .'?token='.sGenerateToken(['file'=>$row['file_name']],['exp'=>time() + 60*300]). '" alt="ছবি পাওয়া যায়নি"></a></div>';
                            }
                            echo "</div>";
                            $i++;
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo $this->request->webroot; ?>projapoti-nothi/js/master_file_related.js?v=<?= time() ?>" type="text/javascript"></script>