<style>
	.note_show_details {
		border: solid 1px silver;
		margin: 5px 0;
		padding: 5px;
	}
	.note_creation_time {
		font-size: 15px;
		color: gray;
	}
</style>
<?php
$lastNoteNo = 0;
$i = 0;

if (!empty($nothi_notes)) {
    $sarok_onucched_array = [];
    foreach ($nothi_notes as $row) {
        if ($row->note_no >= 0) {
            $lastNoteNo = $row->note_no;
        }
        if ($ordering_type == 'DESC') {
            if ($row->note_no == '-1') {
                $sarok_onucched_array[] = $row;
                continue;
            }

            if(!empty($row->digital_sign)){
                //verify digital sign
                $sign_info = json_decode($row->sign_info,true);
                $is_vefied_signature = verifySign($row['note_description'],'html',$sign_info['publicKey'],$sign_info['signature']);
                if(empty($is_vefied_signature)){
                    $is_vefied_signature = 1;
                }
            }
            echo '<div ' . ($row['note_status'] == 'DRAFT' ? 'class="noteDetails note_show_details"' : 'class="note_show_details"') . ' id="note_' . $row->id . '">';
            echo '<div class="noteContent noMarginPadding" id="' . $this->Number->format($row->note_no) . '">';
            if ($this->request->params['action'] != 'notesheetPageAll') {
                if ($row['note_status'] == 'DRAFT' && (is_null($row['potrojari_status']) || $row['potrojari_status'] != 'Sent')) {
                    if(!isset($_GET['permitted_by']) || $_GET['permitted_by'] == ''):
                        ?>
                        <div class='pull-right  hidden-print btn-group btn-group-round margin-right-10'>
                            <?php if ($otherNothi == false): ?>
                                <button  title="পত্রের খসড়া তৈরি করুন"  url="<?= $this->Url->build(['controller' => 'potrojari','action' => 'makeDraft', $row['nothi_part_no'], $row['id'], 'note']) ?>" class="btn btn-xs green btn-onucced-potrojari potroCreate" data-title-orginal="পত্রের খসড়া তৈরি করুন">
                                    <i class="fs1 a2i_nt_dakdraft4"></i>
                                </button>
                            <?php endif; ?>
                            <button noteid='<?=$row->note_no?>' class='btn-onucced-edit   btn btn-xs btn-primary' title='অনুচ্ছেদ সম্পাদন করুন' data-title-orginal='অনুচ্ছেদ সম্পাদন করুন'  >
                                <i class='fs1 a2i_gn_edit2'></i>
                            </button>
                            <button noteid='<?=$row->note_no?>' class='btn-onucced-delete  delete btn btn-xs btn-danger' title='মুছে ফেলুন' data-title-orginal='মুছে ফেলুন'>
                                <i style='vertical-align:baseline!important;' class='fs1 a2i_gn_delete2 '></i>
                            </button>
                        </div>
                        <?php
                    endif;
                }
            }

            echo "<div class='printArea'>";
            //if (!empty($row->subject))
            //echo '<div class="noteNo noteSubject"  notesubjectid=' . $row->id . '> বিষয়: <b>' . $row->subject . '</b></div>';

            echo '<div class="note_creation_time">'.\Cake\I18n\Time::parse($row->created)->i18nFormat('dd-MM-YYYY hh:mm:ss a', null, 'bn-BD').'</div>';
            if ($row->is_potrojari == 0){
                if(!empty($row->digital_sign)){
                    //verify digital sign
                    if(empty($is_vefied_signature)){
                        echo '<span class="noteNo"> অনুচ্ছেদ: ' . $row->nothi_part_no_bn . '.' . $this->Number->format($row->note_no) . '<i class="fa fa-times"  data-original-title="ডিজিটাল সাইন নিশ্চিত সম্ভব হয়নি।"  title="ডিজিটাল সাইন নিশ্চিত সম্ভব হয়নি।" style="background-color: red;color: white;padding:  2px;border-radius:  5px;margin: 2px;border:  solid 1px white;"></i></span>';
                    }else{
                        echo '<span class="noteNo"> অনুচ্ছেদ: ' . $row->nothi_part_no_bn . '.' . $this->Number->format($row->note_no) . '<span style="color: green"><i class="fs0 a2i_gn_onumodondelevery2"  data-original-title="ডিজিটাল সাইনকৃত।"  title="ডিজিটাল সাইনকৃত।" style="padding:  2px;border-radius:  5px;margin: 2px;border:  solid 1px white;"></i></span></span>';
                    }
                }else{
                    echo '<span class="noteNo"> অনুচ্ছেদ: ' . $row->nothi_part_no_bn . '.' . $this->Number->format($row->note_no) . '</span>';
                }
            }

            echo "<div class='noteDescription' id=" . $row->id . "  " . ($row->is_potrojari
                    ? "style='color:red;' title='".$this->Time->format($row->created, "d-MM-y H:m:ss", null, null)."' " : '') . " >" . base64_decode($row->note_description) . "</div>";


            if (!empty($noteAttachmentsmap[$row['id']])) {

                echo "<hr/><div class='table' style='font-size:11px;'><table class='table table-bordered table-stripped hidden-print'><tr><th colspan='4' class='text-left'>" . __(SHONGJUKTI) . "</th></tr>";

                foreach ($noteAttachmentsmap[$row['id']] as $attachmentKey => $attachment) {
                    //dd($attachment);
                    if (empty($attachment['user_file_name'])) {
                        $attachment['user_file_name'] = 'কোন নাম দেওয়া হয়নি';
                    }
                    $fileName = explode('/', $attachment['file_name']);
                    echo "<tr>";
                    echo "<td style='width:10%;'  class='text-center'>" . $this->Number->format($attachmentKey
                            + 1) . "</td>";
                    $user_file_name = htmlspecialchars($attachment['user_file_name']);
                    $url_file_name = "{$attachment['id']}/showAttachment/{$row->nothi_part_no}";

                    echo "<td style='width:35%;' class='preview_attachment' noteid='{$row->note_no}'><span class='preview'>
".(($attachment['attachment_type'] == 'text' ||  $attachment['attachment_type'] == 'text/html' ||
                            substr($attachment['attachment_type'],0,5) == 'image' ||
                            $attachment['attachment_type'] == 'application/pdf') ? "<a class='showforPopup' data-gallery user_file_name='{$user_file_name}' type='{$attachment['attachment_type']}'  
download='{$attachment['file_name']}' href='{$url_file_name}' title='" . urldecode($fileName[count($fileName) - 1]) . "'>" . urldecode($fileName[count($fileName) - 1]) . "</a>" : urldecode($fileName[count($fileName)- 1])) ."</span></td>";
                    echo "<td style='width:35%; text-align:center; font-size:14px;' class='attachment_user_title'><span class='preview user_file_name'>{$attachment['user_file_name']}</span></td>";
                    if ($this->request->params['action'] != 'notesheetPageAll') {
                        echo "<td style='width:20%;' class='text-center download-attachment'>
<a  href='" . $this->Url->build([
                                "controller" => "nothiNoteSheets",
                                "action" => "downloadNoteAttachment",
                                "?" => ["id" => $attachment['id'], 'nothimaster' => $row->nothi_part_no,
                                    'nothi_office' => $nothi_office_id]]) . "' title='ডাউনলোড'><i class='fa fa-download'></i></a>
" . ($row['note_status'] == 'DRAFT' ? "<a class='deleteAttachment' data-note-id='{$row->id}' data-attachment-id='{$attachment['id']}' ><i style='vertical-align:baseline!important;' class='fs1 a2i_gn_delete2 '></i></a>" : "") .(($attachment['digital_sign'] ==1)?'<span><i class="fa fa-check" style="background-color: green;color: white;padding:  2px;border-radius:  5px;margin: 2px;border:  solid 1px white;" data-original-title="ডিজিটাল সাইনকৃত।" title="ডিজিটাল সাইনকৃত।"></i></span>':''). "</td>";
                    }
                    echo "</tr>";
                }
                echo "</table></div>";
            }

            if (isset($signatures[$row['id']])) {
                $n = count($signatures[$row['id']]);
                $col = 4;

                $data = $signatures[$row['id']];
                $finalArray = array();
                $finalArray[0][0] = $data[0];

                $designationSeqPrev = array();

                $j = 0;
                $rowC = 0;
                $i = 1;

                if (!isset($data[$i]['office_organogram_id'])) {
                    $designationSeq = null;
                } else {
                    $designationSeq = isset($employeeOfficeDesignation[$data[$i]['office_organogram_id']]) ? $employeeOfficeDesignation[$data[$i]['office_organogram_id']] : null;
                }

                if(isset($employeeOfficeDesignation[$data[$i - 1]['office_organogram_id']])) {
                    $designationSeqPrev = $employeeOfficeDesignation[$data[$i - 1]['office_organogram_id']];
                }else{
                    $designationSeqPrev[0] = 0;
                }

                while ($i < $n) {

                    if ($designationSeq[0] <= $designationSeqPrev[0]) {
                        if ($j < $col - 1) {
                            $finalArray[$rowC][++$j] = $data[$i];
                        } else {
                            $finalArray[++$rowC][$j] = $data[$i];
                        }
                    } else {
                        $rowC++;
                        if ($j > 0) {
                            $finalArray[$rowC][--$j] = $data[$i];
                        } else {
                            $finalArray[$rowC][$j] = $data[$i];
                        }
                    }

                    $i++;
                    if (isset($data[$i]['office_organogram_id']) && isset($employeeOfficeDesignation[$data[$i]['office_organogram_id']]))
                        $designationSeq = $employeeOfficeDesignation[$data[$i]['office_organogram_id']];
                    if (isset($data[$i]['office_organogram_id']) && isset($employeeOfficeDesignation[$data[$i - 1]['office_organogram_id']]))
                        $designationSeqPrev = $employeeOfficeDesignation[$data[$i - 1]['office_organogram_id']];
                }
                if (!empty($finalArray)) {
                    for ($findex = 0; $findex < sizeof($finalArray); $findex++) {
                        echo "<div class='row' style='margin:0!important;padding:0!important'>";
                        for ($fj = 0; $fj < $col; $fj++) {
                            echo '<div class="col-md-3 col-sm-3 col-lg-3 col-xs-3 text-center" style="padding:2px;"><div style="line-height: 1.2!important;font-size: 10px; color: darkviolet; display: block; ' . (!empty($finalArray[$findex][$fj]['cross_signature']) ? ' text-decoration: line-through;' : '') . ' vertical-align: bottom;page-break-inside:auto!important;margin:0!important;padding:0!important; ">';
                            if (isset($finalArray[$findex][$fj])) {
                                echo "<div class='btn btn-block ' style='height: 40px;margin:0!important;padding: 0px!important;margin-bottom: 2px!important;'>";
                                if (!empty($finalArray[$findex][$fj]['is_signature']) && empty($finalArray[$findex][$fj]['cross_signature'])) {
                                    $englishdate = new \Cake\I18n\Time($finalArray[$findex][$fj]['signature_date'], "d-MM-y H:m:ss", null, null);
                                    if(!empty($finalArray[$findex][$fj]['digital_sign'])){
                                        if(empty($is_vefied_signature)){
                                            echo '<i class="fa fa-times" style="left:5px;position: absolute;background-color: red;color: white;padding:  0px;border-radius:  5px;margin: 0px 1px;border: none;" data-original-title="ডিজিটাল সাইন নিশ্চিত সম্ভব হয়নি।" title="ডিজিটাল সাইন নিশ্চিত সম্ভব হয়নি।"></i>';
                                        }else{
                                            echo '<i class="fa fa-check" style="left:5px;position: absolute;background-color: green;color: white;padding:  0px;border-radius:  5px;margin: 0px 1px;border: none;" data-original-title="ডিজিটাল সাইনকৃত।" title="ডিজিটাল সাইনকৃত।"></i>';
                                        }
                                    }
                                    echo '<img class="signatures" style="height: 40px!important;padding:0!important;margin:0!important;" data-signdate="'. $englishdate->i18nFormat("Y-M-d H:m:s", null, 'en-US') .'" data-id="' . $finalArray[$findex][$fj]['userInfo'] . '" data-token_id="' . sGenerateToken(['file'=>$finalArray[$findex][$fj]['userInfo']],['exp'=>time() + 60*300]) . '" alt="সাইন যোগ করা হয়নি" />';
                                } else {
                                    echo '<img class="img-responsive" style="height: 40px!important;padding:0!important;margin:0!important; visibility: hidden;" />';
                                }
                                echo "</div>";

                                echo "<span style='border-top:1px solid #9400d3;font-size: 8pt;display: block;'>" . $this->Time->format(
                                        $finalArray[$findex][$fj]['signature_date'], "d-MM-y H:m:ss", null, null) . "</span><span style='font-size: 8pt;display: block;'>" . h($finalArray[$findex][$fj]['name']) . "</span><span style='font-size: 8pt;display: block;'>" . $finalArray[$findex][$fj]['employee_designation'] . "</span>";
                            } else
                                echo "<div class='btn btn-block ' style='height: 40px;margin:0!important;padding: 0px!important;margin-bottom: 2px!important;'>&nbsp;</div>";
                            echo '</div></div>';
                        }
                        echo "</div>";
                    }
                }
            }
            echo '</div>';
            echo '</div>';
            echo '</div>';

            if (!empty($sarok_onucched_array)) {
                foreach ($sarok_onucched_array as $sarok_onucched) {
                    if (!empty($sarok_onucched->digital_sign)) {
                        //verify digital sign
                        $sign_info = json_decode($sarok_onucched->sign_info, true);
                        $is_vefied_signature = verifySign($sarok_onucched['note_description'], 'html', $sign_info['publicKey'], $sign_info['signature']);
                        if (empty($is_vefied_signature)) {
                            $is_vefied_signature = 1;
                        }
                    }
                    echo '<div ' . ($sarok_onucched['note_status'] == 'DRAFT' ? 'class="noteDetails note_show_details"' : 'class="note_show_details"') . ' id="note_' . $sarok_onucched->id . '">';
                    echo '<div class="noteContent noMarginPadding" id="' . $this->Number->format($sarok_onucched->note_no) . '">';
                    if ($this->request->params['action'] != 'notesheetPageAll') {
                        if ($sarok_onucched['note_status'] == 'DRAFT' && (is_null($sarok_onucched['potrojari_status']) || $sarok_onucched['potrojari_status'] != 'Sent')) {
                            if(!isset($_GET['permitted_by']) || $_GET['permitted_by'] == ''):
                            ?>
                            <div class='pull-right  hidden-print btn-group btn-group-round margin-right-10'>
                                <?php if ($otherNothi == false): ?>
                                    <button title="পত্রের খসড়া তৈরি করুন"
                                            url="<?= $this->Url->build(['controller' => 'potrojari', 'action' => 'makeDraft', $sarok_onucched['nothi_part_no'], $sarok_onucched['id'], 'note']) ?>"
                                            class="btn btn-xs green btn-onucced-potrojari potroCreate"
                                            data-title-orginal="পত্রের খসড়া তৈরি করুন">
                                        <i class="fs1 a2i_nt_dakdraft4"></i>
                                    </button>
                                <?php endif; ?>
                                <button noteid='<?= $sarok_onucched->note_no ?>'
                                        class='btn-onucced-edit   btn btn-xs btn-primary' title='অনুচ্ছেদ সম্পাদন করুন'
                                        data-title-orginal='অনুচ্ছেদ সম্পাদন করুন'>
                                    <i class='fs1 a2i_gn_edit2'></i>
                                </button>
                                <button noteid='<?= $sarok_onucched->note_no ?>'
                                        class='btn-onucced-delete  delete btn btn-xs btn-danger' title='মুছে ফেলুন'
                                        data-title-orginal='মুছে ফেলুন'>
                                    <i style='vertical-align:baseline!important;' class='fs1 a2i_gn_delete2 '></i>
                                </button>
                            </div>
                            <?php
                            endif;
                        }
                    }

                    echo "<div class='printArea'>";
                    //if (!empty($sarok_onucched->subject))
                    //echo '<div class="noteNo noteSubject"  notesubjectid=' . $sarok_onucched->id . '> বিষয়: <b>' . $sarok_onucched->subject . '</b></div>';

                    echo '<div class="note_creation_time">' . \Cake\I18n\Time::parse($sarok_onucched->created)->i18nFormat('dd-MM-YYYY hh:mm:ss a', null, 'bn-BD') . '</div>';
                    if ($sarok_onucched->is_potrojari == 0) {
                        if (!empty($sarok_onucched->digital_sign)) {
                            //verify digital sign
                            if (empty($is_vefied_signature)) {
                                echo '<span class="noteNo"> অনুচ্ছেদ: ' . $sarok_onucched->nothi_part_no_bn . '.' . $this->Number->format($sarok_onucched->note_no) . '<i class="fa fa-times"  data-original-title="ডিজিটাল সাইন নিশ্চিত সম্ভব হয়নি।"  title="ডিজিটাল সাইন নিশ্চিত সম্ভব হয়নি।" style="background-color: red;color: white;padding:  2px;border-radius:  5px;margin: 2px;border:  solid 1px white;"></i></span>';
                            } else {
                                echo '<span class="noteNo"> অনুচ্ছেদ: ' . $sarok_onucched->nothi_part_no_bn . '.' . $this->Number->format($sarok_onucched->note_no) . '<span style="color: green"><i class="fs0 a2i_gn_onumodondelevery2"  data-original-title="ডিজিটাল সাইনকৃত।"  title="ডিজিটাল সাইনকৃত।" style="padding:  2px;border-radius:  5px;margin: 2px;border:  solid 1px white;"></i></span></span>';
                            }
                        } else {
                            echo '<span class="noteNo"> অনুচ্ছেদ: ' . $sarok_onucched->nothi_part_no_bn . '.' . $this->Number->format($sarok_onucched->note_no) . '</span>';
                        }
                    }

                    echo "<div class='noteDescription' id=" . $sarok_onucched->id . "  " . ($sarok_onucched->is_potrojari
                            ? "style='color:red;' title='" . $this->Time->format($sarok_onucched->created, "d-MM-y H:m:ss", null, null) . "' " : '') . " >" . base64_decode($sarok_onucched->note_description) . "</div>";


                    if (!empty($noteAttachmentsmap[$sarok_onucched['id']])) {

                        echo "<hr/><div class='table' style='font-size:11px;'><table class='table table-bordered table-stripped hidden-print'><tr><th colspan='4' class='text-left'>" . __(SHONGJUKTI) . "</th></tr>";

                        foreach ($noteAttachmentsmap[$sarok_onucched['id']] as $attachmentKey => $attachment) {
                            //dd($attachment);
                            if (empty($attachment['user_file_name'])) {
                                $attachment['user_file_name'] = 'কোন নাম দেওয়া হয়নি';
                            }
                            $fileName = explode('/', $attachment['file_name']);
                            echo "<tr>";
                            echo "<td style='width:10%;'  class='text-center'>" . $this->Number->format($attachmentKey
                                    + 1) . "</td>";
                            $user_file_name = htmlspecialchars($attachment['user_file_name']);
                            $url_file_name = "{$attachment['id']}/showAttachment/{$sarok_onucched->nothi_part_no}";

                            echo "<td style='width:35%;' class='preview_attachment' noteid='{$sarok_onucched->note_no}'><span class='preview'>
" . (($attachment['attachment_type'] == 'text' || $attachment['attachment_type'] == 'text/html' ||
                                    substr($attachment['attachment_type'], 0, 5) == 'image' ||
                                    $attachment['attachment_type'] == 'application/pdf') ? "<a class='showforPopup' data-gallery user_file_name='{$user_file_name}' type='{$attachment['attachment_type']}'  
download='{$attachment['file_name']}' href='{$url_file_name}' title='" . urldecode($fileName[count($fileName) - 1]) . "'>" . urldecode($fileName[count($fileName) - 1]) . "</a>" : urldecode($fileName[count($fileName) - 1])) . "</span></td>";
                            echo "<td style='width:35%; text-align:center; font-size:14px;' class='attachment_user_title'><span class='preview user_file_name'>{$attachment['user_file_name']}</span></td>";
                            if ($this->request->params['action'] != 'notesheetPageAll') {
                                echo "<td style='width:20%;' class='text-center download-attachment'>
<a  href='" . $this->Url->build([
                                        "controller" => "nothiNoteSheets",
                                        "action" => "downloadNoteAttachment",
                                        "?" => ["id" => $attachment['id'], 'nothimaster' => $sarok_onucched->nothi_part_no,
                                            'nothi_office' => $nothi_office_id]]) . "' title='ডাউনলোড'><i class='fa fa-download'></i></a>
" . ($sarok_onucched['note_status'] == 'DRAFT' ? "<a class='deleteAttachment' data-note-id='{$sarok_onucched->id}' data-attachment-id='{$attachment['id']}' ><i style='vertical-align:baseline!important;' class='fs1 a2i_gn_delete2 '></i></a>" : "") . (($attachment['digital_sign'] == 1) ? '<span><i class="fa fa-check" style="background-color: green;color: white;padding:  2px;border-radius:  5px;margin: 2px;border:  solid 1px white;" data-original-title="ডিজিটাল সাইনকৃত।" title="ডিজিটাল সাইনকৃত।"></i></span>' : '') . "</td>";
                            }
                            echo "</tr>";
                        }
                        echo "</table></div>";
                    }

                    if (isset($signatures[$sarok_onucched['id']])) {
                        $n = count($signatures[$sarok_onucched['id']]);
                        $col = 4;

                        $data = $signatures[$sarok_onucched['id']];
                        $finalArray = array();
                        $finalArray[0][0] = $data[0];

                        $designationSeqPrev = array();

                        $j = 0;
                        $sarok_onucchedC = 0;
                        $i = 1;

                        if (!isset($data[$i]['office_organogram_id'])) {
                            $designationSeq = null;
                        } else {
                            $designationSeq = isset($employeeOfficeDesignation[$data[$i]['office_organogram_id']]) ? $employeeOfficeDesignation[$data[$i]['office_organogram_id']] : null;
                        }

                        if (isset($employeeOfficeDesignation[$data[$i - 1]['office_organogram_id']])) {
                            $designationSeqPrev = $employeeOfficeDesignation[$data[$i - 1]['office_organogram_id']];
                        } else {
                            $designationSeqPrev[0] = 0;
                        }

                        while ($i < $n) {

                            if ($designationSeq[0] <= $designationSeqPrev[0]) {
                                if ($j < $col - 1) {
                                    $finalArray[$sarok_onucchedC][++$j] = $data[$i];
                                } else {
                                    $finalArray[++$sarok_onucchedC][$j] = $data[$i];
                                }
                            } else {
                                $sarok_onucchedC++;
                                if ($j > 0) {
                                    $finalArray[$sarok_onucchedC][--$j] = $data[$i];
                                } else {
                                    $finalArray[$sarok_onucchedC][$j] = $data[$i];
                                }
                            }

                            $i++;
                            if (isset($data[$i]['office_organogram_id']) && isset($employeeOfficeDesignation[$data[$i]['office_organogram_id']]))
                                $designationSeq = $employeeOfficeDesignation[$data[$i]['office_organogram_id']];
                            if (isset($data[$i]['office_organogram_id']) && isset($employeeOfficeDesignation[$data[$i - 1]['office_organogram_id']]))
                                $designationSeqPrev = $employeeOfficeDesignation[$data[$i - 1]['office_organogram_id']];
                        }
                        if (!empty($finalArray)) {
                            for ($findex = 0; $findex < sizeof($finalArray); $findex++) {
                                echo "<div class='row' style='margin:0!important;padding:0!important'>";
                                for ($fj = 0; $fj < $col; $fj++) {
                                    echo '<div class="col-md-3 col-sm-3 col-lg-3 col-xs-3 text-center" style="padding:2px;"><div style="line-height: 1.2!important;font-size: 10px; color: darkviolet; display: block; ' . (!empty($finalArray[$findex][$fj]['cross_signature']) ? ' text-decoration: line-through;' : '') . ' vertical-align: bottom;page-break-inside:auto!important;margin:0!important;padding:0!important; ">';
                                    if (isset($finalArray[$findex][$fj])) {
                                        echo "<div class='btn btn-block ' style='height: 40px;margin:0!important;padding: 0px!important;margin-bottom: 2px!important;'>";
                                        if (!empty($finalArray[$findex][$fj]['is_signature']) && empty($finalArray[$findex][$fj]['cross_signature'])) {
                                            $englishdate = new \Cake\I18n\Time($finalArray[$findex][$fj]['signature_date'], "d-MM-y H:m:ss", null, null);
                                            if (!empty($finalArray[$findex][$fj]['digital_sign'])) {
                                                if (empty($is_vefied_signature)) {
                                                    echo '<i class="fa fa-times" style="left:5px;position: absolute;background-color: red;color: white;padding:  0px;border-radius:  5px;margin: 0px 1px;border: none;" data-original-title="ডিজিটাল সাইন নিশ্চিত সম্ভব হয়নি।" title="ডিজিটাল সাইন নিশ্চিত সম্ভব হয়নি।"></i>';
                                                } else {
                                                    echo '<i class="fa fa-check" style="left:5px;position: absolute;background-color: green;color: white;padding:  0px;border-radius:  5px;margin: 0px 1px;border: none;" data-original-title="ডিজিটাল সাইনকৃত।" title="ডিজিটাল সাইনকৃত।"></i>';
                                                }
                                            }
                                            echo '<img class="signatures" style="height: 40px!important;padding:0!important;margin:0!important;" data-signdate="' . $englishdate->i18nFormat("Y-M-d H:m:s", null, 'en-US') . '" data-id="' . $finalArray[$findex][$fj]['userInfo'] . '" data-token_id="' . sGenerateToken(['file' => $finalArray[$findex][$fj]['userInfo']], ['exp' => time() + 60 * 300]) . '" alt="সাইন যোগ করা হয়নি" />';
                                        } else {
                                            echo '<img class="img-responsive" style="height: 40px!important;padding:0!important;margin:0!important; visibility: hidden;" />';
                                        }
                                        echo "</div>";

                                        echo "<span style='border-top:1px solid #9400d3;font-size: 8pt;display: block;'>" . $this->Time->format(
                                                $finalArray[$findex][$fj]['signature_date'], "d-MM-y H:m:ss", null, null) . "</span><span style='font-size: 8pt;display: block;'>" . h($finalArray[$findex][$fj]['name']) . "</span><span style='font-size: 8pt;display: block;'>" . $finalArray[$findex][$fj]['employee_designation'] . "</span>";
                                    } else
                                        echo "<div class='btn btn-block ' style='height: 40px;margin:0!important;padding: 0px!important;margin-bottom: 2px!important;'>&nbsp;</div>";
                                    echo '</div></div>';
                                }
                                echo "</div>";
                            }
                        }
                    }
                    echo '</div>';
                    echo '</div>';
                    echo '</div>';
                }
                $sarok_onucched_array = [];
            }
        } else {
            if(!empty($row->digital_sign)){
                //verify digital sign
                $sign_info = json_decode($row->sign_info,true);
                $is_vefied_signature = verifySign($row['note_description'],'html',$sign_info['publicKey'],$sign_info['signature']);
                if(empty($is_vefied_signature)){
                    $is_vefied_signature = 1;
                }
            }
            echo '<div ' . ($row['note_status'] == 'DRAFT' ? 'class="noteDetails note_show_details"' : 'class="note_show_details"') . ' id="note_' . $row->id . '">';
            echo '<div class="noteContent noMarginPadding" id="' . $this->Number->format($row->note_no) . '">';
            if ($this->request->params['action'] != 'notesheetPageAll') {
                if ($row['note_status'] == 'DRAFT' && (is_null($row['potrojari_status']) || $row['potrojari_status'] != 'Sent')) {
                    if(!isset($_GET['permitted_by']) || $_GET['permitted_by'] == ''):
                    ?>
                    <div class='pull-right  hidden-print btn-group btn-group-round margin-right-10'>
                        <?php if ($otherNothi == false): ?>
                            <button  title="পত্রের খসড়া তৈরি করুন"  url="<?= $this->Url->build(['controller' => 'potrojari','action' => 'makeDraft', $row['nothi_part_no'], $row['id'], 'note']) ?>" class="btn btn-xs green btn-onucced-potrojari potroCreate" data-title-orginal="পত্রের খসড়া তৈরি করুন">
                                <i class="fs1 a2i_nt_dakdraft4"></i>
                            </button>
                        <?php endif; ?>
                        <button noteid='<?=$row->note_no?>' class='btn-onucced-edit   btn btn-xs btn-primary' title='অনুচ্ছেদ সম্পাদন করুন' data-title-orginal='অনুচ্ছেদ সম্পাদন করুন'  >
                            <i class='fs1 a2i_gn_edit2'></i>
                        </button>
                        <button noteid='<?=$row->note_no?>' class='btn-onucced-delete  delete btn btn-xs btn-danger' title='মুছে ফেলুন' data-title-orginal='মুছে ফেলুন'>
                            <i style='vertical-align:baseline!important;' class='fs1 a2i_gn_delete2 '></i>
                        </button>
                    </div>
                    <?php
                    endif;
                }
            }

            echo "<div class='printArea'>";
            //if (!empty($row->subject))
            //echo '<div class="noteNo noteSubject"  notesubjectid=' . $row->id . '> বিষয়: <b>' . $row->subject . '</b></div>';

            echo '<div class="note_creation_time">'.\Cake\I18n\Time::parse($row->created)->i18nFormat('dd-MM-YYYY hh:mm:ss a', null, 'bn-BD').'</div>';
            if ($row->is_potrojari == 0){
                if(!empty($row->digital_sign)){
                    //verify digital sign
                    if(empty($is_vefied_signature)){
                        echo '<span class="noteNo"> অনুচ্ছেদ: ' . $row->nothi_part_no_bn . '.' . $this->Number->format($row->note_no) . '<i class="fa fa-times"  data-original-title="ডিজিটাল সাইন নিশ্চিত সম্ভব হয়নি।"  title="ডিজিটাল সাইন নিশ্চিত সম্ভব হয়নি।" style="background-color: red;color: white;padding:  2px;border-radius:  5px;margin: 2px;border:  solid 1px white;"></i></span>';
                    }else{
                        echo '<span class="noteNo"> অনুচ্ছেদ: ' . $row->nothi_part_no_bn . '.' . $this->Number->format($row->note_no) . '<span style="color: green"><i class="fs0 a2i_gn_onumodondelevery2"  data-original-title="ডিজিটাল সাইনকৃত।"  title="ডিজিটাল সাইনকৃত।" style="padding:  2px;border-radius:  5px;margin: 2px;border:  solid 1px white;"></i></span></span>';
                    }
                }else{
                    echo '<span class="noteNo"> অনুচ্ছেদ: ' . $row->nothi_part_no_bn . '.' . $this->Number->format($row->note_no) . '</span>';
                }
            }

            echo "<div class='noteDescription' id=" . $row->id . "  " . ($row->is_potrojari
                    ? "style='color:red;' title='".$this->Time->format($row->created, "d-MM-y H:m:ss", null, null)."' " : '') . " >" . base64_decode($row->note_description) . "</div>";


            if (!empty($noteAttachmentsmap[$row['id']])) {

                echo "<hr/><div class='table' style='font-size:11px;'><table class='table table-bordered table-stripped hidden-print'><tr><th colspan='4' class='text-left'>" . __(SHONGJUKTI) . "</th></tr>";

                foreach ($noteAttachmentsmap[$row['id']] as $attachmentKey => $attachment) {
                    //dd($attachment);
                    if (empty($attachment['user_file_name'])) {
                        $attachment['user_file_name'] = 'কোন নাম দেওয়া হয়নি';
                    }
                    $fileName = explode('/', $attachment['file_name']);
                    echo "<tr>";
                    echo "<td style='width:10%;'  class='text-center'>" . $this->Number->format($attachmentKey
                            + 1) . "</td>";
                    $user_file_name = htmlspecialchars($attachment['user_file_name']);
                    $url_file_name = "{$attachment['id']}/showAttachment/{$row->nothi_part_no}";

                    echo "<td style='width:35%;' class='preview_attachment' noteid='{$row->note_no}'><span class='preview'>
".(($attachment['attachment_type'] == 'text' ||  $attachment['attachment_type'] == 'text/html' ||
                            substr($attachment['attachment_type'],0,5) == 'image' ||
                            $attachment['attachment_type'] == 'application/pdf') ? "<a class='showforPopup' data-gallery user_file_name='{$user_file_name}' type='{$attachment['attachment_type']}'  
download='{$attachment['file_name']}' href='{$url_file_name}' title='" . urldecode($fileName[count($fileName) - 1]) . "'>" . urldecode($fileName[count($fileName) - 1]) . "</a>" : urldecode($fileName[count($fileName)- 1])) ."</span></td>";
                    echo "<td style='width:35%; text-align:center; font-size:14px;' class='attachment_user_title'><span class='preview user_file_name'>{$attachment['user_file_name']}</span></td>";
                    if ($this->request->params['action'] != 'notesheetPageAll') {
                        echo "<td style='width:20%;' class='text-center download-attachment'>
<a  href='" . $this->Url->build([
                                "controller" => "nothiNoteSheets",
                                "action" => "downloadNoteAttachment",
                                "?" => ["id" => $attachment['id'], 'nothimaster' => $row->nothi_part_no,
                                    'nothi_office' => $nothi_office_id]]) . "' title='ডাউনলোড'><i class='fa fa-download'></i></a>
" . ($row['note_status'] == 'DRAFT' ? "<a class='deleteAttachment' data-note-id='{$row->id}' data-attachment-id='{$attachment['id']}' ><i style='vertical-align:baseline!important;' class='fs1 a2i_gn_delete2 '></i></a>" : "") .(($attachment['digital_sign'] ==1)?'<span><i class="fa fa-check" style="background-color: green;color: white;padding:  2px;border-radius:  5px;margin: 2px;border:  solid 1px white;" data-original-title="ডিজিটাল সাইনকৃত।" title="ডিজিটাল সাইনকৃত।"></i></span>':''). "</td>";
                    }
                    echo "</tr>";
                }
                echo "</table></div>";
            }

            if (isset($signatures[$row['id']])) {
                $n = count($signatures[$row['id']]);
                $col = 4;

                $data = $signatures[$row['id']];
                $finalArray = array();
                $finalArray[0][0] = $data[0];

                $designationSeqPrev = array();

                $j = 0;
                $rowC = 0;
                $i = 1;

                if (!isset($data[$i]['office_organogram_id'])) {
                    $designationSeq = null;
                } else {
                    $designationSeq = isset($employeeOfficeDesignation[$data[$i]['office_organogram_id']]) ? $employeeOfficeDesignation[$data[$i]['office_organogram_id']] : null;
                }

                if(isset($employeeOfficeDesignation[$data[$i - 1]['office_organogram_id']])) {
                    $designationSeqPrev = $employeeOfficeDesignation[$data[$i - 1]['office_organogram_id']];
                }else{
                    $designationSeqPrev[0] = 0;
                }

                while ($i < $n) {

                    if ($designationSeq[0] <= $designationSeqPrev[0]) {
                        if ($j < $col - 1) {
                            $finalArray[$rowC][++$j] = $data[$i];
                        } else {
                            $finalArray[++$rowC][$j] = $data[$i];
                        }
                    } else {
                        $rowC++;
                        if ($j > 0) {
                            $finalArray[$rowC][--$j] = $data[$i];
                        } else {
                            $finalArray[$rowC][$j] = $data[$i];
                        }
                    }

                    $i++;
                    if (isset($data[$i]['office_organogram_id']) && isset($employeeOfficeDesignation[$data[$i]['office_organogram_id']]))
                        $designationSeq = $employeeOfficeDesignation[$data[$i]['office_organogram_id']];
                    if (isset($data[$i]['office_organogram_id']) && isset($employeeOfficeDesignation[$data[$i - 1]['office_organogram_id']]))
                        $designationSeqPrev = $employeeOfficeDesignation[$data[$i - 1]['office_organogram_id']];
                }
                if (!empty($finalArray)) {
                    for ($findex = 0; $findex < sizeof($finalArray); $findex++) {
                        echo "<div class='row' style='margin:0!important;padding:0!important'>";
                        for ($fj = 0; $fj < $col; $fj++) {
                            echo '<div class="col-md-3 col-sm-3 col-lg-3 col-xs-3 text-center" style="padding:2px;"><div style="line-height: 1.2!important;font-size: 10px; color: darkviolet; display: block; ' . (!empty($finalArray[$findex][$fj]['cross_signature']) ? ' text-decoration: line-through;' : '') . ' vertical-align: bottom;page-break-inside:auto!important;margin:0!important;padding:0!important; ">';
                            if (isset($finalArray[$findex][$fj])) {
                                echo "<div class='btn btn-block ' style='height: 40px;margin:0!important;padding: 0px!important;margin-bottom: 2px!important;'>";
                                if (!empty($finalArray[$findex][$fj]['is_signature']) && empty($finalArray[$findex][$fj]['cross_signature'])) {
                                    $englishdate = new \Cake\I18n\Time($finalArray[$findex][$fj]['signature_date'], "d-MM-y H:m:ss", null, null);
                                    if(!empty($finalArray[$findex][$fj]['digital_sign'])){
                                        if(empty($is_vefied_signature)){
                                            echo '<i class="fa fa-times" style="left:5px;position: absolute;background-color: red;color: white;padding:  0px;border-radius:  5px;margin: 0px 1px;border: none;" data-original-title="ডিজিটাল সাইন নিশ্চিত সম্ভব হয়নি।" title="ডিজিটাল সাইন নিশ্চিত সম্ভব হয়নি।"></i>';
                                        }else{
                                            echo '<i class="fa fa-check" style="left:5px;position: absolute;background-color: green;color: white;padding:  0px;border-radius:  5px;margin: 0px 1px;border: none;" data-original-title="ডিজিটাল সাইনকৃত।" title="ডিজিটাল সাইনকৃত।"></i>';
                                        }
                                    }
                                    echo '<img class="signatures" style="height: 40px!important;padding:0!important;margin:0!important;" data-signdate="'. $englishdate->i18nFormat("Y-M-d H:m:s", null, 'en-US') .'" data-id="' . $finalArray[$findex][$fj]['userInfo'] . '" data-token_id="' . sGenerateToken(['file'=>$finalArray[$findex][$fj]['userInfo']],['exp'=>time() + 60*300]) . '" alt="সাইন যোগ করা হয়নি" />';
                                } else {
                                    echo '<img class="img-responsive" style="height: 40px!important;padding:0!important;margin:0!important; visibility: hidden;" />';
                                }
                                echo "</div>";

                                echo "<span style='border-top:1px solid #9400d3;font-size: 8pt;display: block;'>" . $this->Time->format(
                                        $finalArray[$findex][$fj]['signature_date'], "d-MM-y H:m:ss", null, null) . "</span><span style='font-size: 8pt;display: block;'>" . h($finalArray[$findex][$fj]['name']) . "</span><span style='font-size: 8pt;display: block;'>" . $finalArray[$findex][$fj]['employee_designation'] . "</span>";
                            } else
                                echo "<div class='btn btn-block ' style='height: 40px;margin:0!important;padding: 0px!important;margin-bottom: 2px!important;'>&nbsp;</div>";
                            echo '</div></div>';
                        }
                        echo "</div>";
                    }
                }
            }
            echo '</div>';
            echo '</div>';
            echo '</div>';
        }
    }
} else {
    if (!isset($lastListedNote) || $lastListedNote != -1) {
        echo "<p class='text-center text-danger'>এই পেজ এ কোন অনুচ্ছেদ দেয়া হয়নি</p>";
    }
}

if ($privilige_type == 1 && $this->request->params['action'] != 'notesheetPageAll') {
?>
<!--<a-->
<!--    data-original-title="নতুন অনুচ্ছেদ তৈরি করুন"-->
<!--    title="নতুন অনুচ্ছেদ তৈরি করুন"-->
<!--    class="btn btn-default green margin-right-10 margin-top-10 addNewNote hide">-->
<!--    <i class="fs1 a2i_gn_note2"></i> নতুন অনুচ্ছেদ-->
<!--</a>-->
<!--<script>-->
<!--    $('.addNewNote').hide()-->
<!--    $('.addNewNote').eq($('.addNewNote').length-1).show()-->
<!--</script>-->

<?php } ?>

<script>
    $(function(){
        $(document).find('[title]').tooltip({'placement':'bottom', 'container': 'body'});
    })
</script>
