<style>
    input[type=checkbox]{
        height: 16px;
    }
</style>
    <?php echo $this->cell('NothiOwnUnitPermissionList::updatePermission', ['prefix' => '', 'id'=>$id,'nothi_office'=>$nothi_office]); ?>
    
</div>
<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2.css"/>
<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2.min.js"></script>
<script>
      $(document).find('[title]').tooltip({'placement':'bottom'});

      $("#ownOfficeUnitList").on("change", function() {
          var id = $(this).val();
          if (id == 0) {
              $('[id^="collapse_"]').parent().show();
          } else {
              $('[id^="collapse_"]').parent().hide();
              $('[id="collapse_'+id+'"]').parent().show();
              $('[id="collapse_'+id+'"]').parent().find('.accordion-toggle').trigger('click');
          }
      });
</script>