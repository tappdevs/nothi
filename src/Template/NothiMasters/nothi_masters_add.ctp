
<link rel="stylesheet" type="text/css"
      href="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-summernote/summernote.css">

<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2.css" type="text/javascript">

<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2-bootstrap.css" type="text/javascript">

<style>
    .modal-open .modal {
        overflow-x: auto;
    }

    .error-message {
        color: red;
    }

    @media (min-width: 1010px) {
        .modal-dialog {
            width: 900px;
            margin: 30px auto;
        }
    }
    
    .checkbox input[type=checkbox], .checkbox-inline input[type=checkbox], .radio input[type=radio], .radio-inline input[type=radio]{
        margin-left: 0px;
    }

</style>
<div class="form">
    <?= $this->Form->create($nothiMastersRecords, ['id' => 'NothiCreateForm']); ?>
    <?= $this->Form->hidden('id'); ?>
    <div class="form-body">
        <div class="row form-group">
           <label class="col-md-2 col-sm-4 control-label">মূল নথি</label>
             <div class="col-lg-6 col-md-6 col-sm-12 ">
                 <?php echo $this->Form->input('nothi_master_id',['label'=>false, 'class'=>'form-control','type'=>'select','empty'=>'নাই','options' => $nothiPermissionList]) ?>
            </div>
        </div>
    </div>
    <div class="form-body" >

        <div class="form-group row is_new_nothi">
            <label class="col-md-2 control-label">নথির ধরন *</label>

            <div class="col-md-4">
                <?= $this->Form->input('nothi_types_id', ['label' => false, 'class' => 'form-control', 'type' => 'select', 'empty' => '--', 'options' => $nothiTypes]); ?>
            </div>

            <label class="col-md-2 control-label">নথি নম্বর * </label>

            <div class="col-md-4">
                <?= $this->Form->input('nothi_no', ['label' => false, 'class' => 'form-control font-bijoy-bangla', 'type' => 'text']); ?>
            </div>
        </div>

        <div class="form-group row is_new_nothi">
            <label class="col-md-2 control-label">বিষয় * </label>

            <div class="col-md-10">
                <?= $this->Form->input('subject', ['label' => false, 'class' => 'form-control', 'type' => 'text','default'=>(!empty($dak_subject)?$dak_subject:'')]); ?>
            </div>
        </div>

        <div class="form-group row is_new_nothi">
            <label class="col-md-2 control-label">নথির শ্রেনি </label>

            <div class="col-md-4">
                <?php $nothiClass = json_decode(NOTHI_CLASS); echo $this->Form->input('nothi_class', ['label' => false, 'class' => 'form-control', 'type' => 'select', 'options' => ($nothiClass)]); ?>
            </div>
            
            <label class="col-md-2 control-label">নথির শাখা *</label>

            <div class="col-md-4">
                <?= $this->Form->input('office_units_id', ['label' => false, 'class' => 'form-control', 'type' => 'select', 'empty' => '--', 'options' => $unitList, 'readonly' => true, 'disabled' => 'disabled']); ?>
            </div>

        </div>

        <div class="form-group row is_new_nothi">
            <label class="col-md-2 control-label">বিবরণ </label>

            <div class="col-md-10">
                <?= $this->Form->text('description', ['label' => false, 'class' => 'form-control ']); ?>
            </div>
        </div>

        <div class="form-group row is_new_nothi">
            
            <label class="col-md-2 control-label">নথি তৈরির তারিখ </label>

            <div class="col-md-4">
                <div class="input-group">
                    <?= $this->Form->input('nothi_created_date', ['label' => false, 'class' => 'form-control',
                        'type' => 'text', 'readonly' => true, 'value'=>date("Y-m-d")]); ?>
                    
                </div>
            </div>
        </div>
    </div>
    <?= $this->Form->end(); ?>
    <input type="button" class="btn btn-danger btn-sm btn-nothi-permission" value="নথিতে অনুমতি প্রদান করুন"/>
    <div id="permission-Div"></div><br/>
</div>
<script src="<?php echo CDN_PATH; ?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js"
        type="text/javascript"></script>
<script src="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-summernote/summernote.min.js"  type="text/javascript"></script>

<script src="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2.js" type="text/javascript"></script>

<script type="text/javascript">

    jQuery(document).ready(function () {
        $('.nothiCreateButton').hide();
        $('#nothi-types-id').select2();
        $('textarea').summernote({
            height: 200,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'italic', 'underline', 'clear']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'hr']]
            ]
        });
        
    });
    
    $(document).on('change','#nothi-master-id', function(){

        if($(this).val()>0){
            $('.nothiCreateButton').show();
           // $('.btn-nothi-permission').hide();
            //$("#permission-Div").html('');
            $('.is_new_nothi').hide();
        }else{
            $('.nothiCreateButton').hide();
            //$('.btn-nothi-permission').show();
            //$("#permission-Div").html('');
            $('.is_new_nothi').show();
        }
    })

    var bn_digits = new Array('০', '১', '২', '৩', '৪', '৫', '৬', '৭', '৮', '৯');

    $("#nothi-types-id").change(function (e) {
        $.ajax({
            url: js_wb_root + "nothiMasters/generateNothiNumber",
            data: {id: $(this).val()},
            type: "POST",
            dataType: 'JSON',
            success: function (response) {
                $("#nothi-no").val(response);
            },
            error: function (status, xresponse) {

            }

        });
    });

    $(document).on('click',".btn-nothi-permission",function (e) {
        if (!($("#nothi-no").val().replace(/\./g, '').length==18)) {
            toastr.error('দুঃখিত! নথি নম্বর সঠিক নয়। নথি নম্বর ১৮ ডিজিটের হতে হবে ');
            return;
        }
        $("#permission-Div").html('<img src="' + js_wb_root + 'assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp;লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');
        $.ajax({
            url: js_wb_root + "nothiMasters/nothiMasterPermissionList/" + $('#nothi-master-id').val(),
            success: function (response) {

                $('.btn-nothi-permission').hide();
                $('.nothiCreateButton').show();
                $("#permission-Div").html(response);
               
                $('.dataTables_filter').css('float','left');
            },
            error: function (status, xresponse) {

            }

        });
    });
</script>