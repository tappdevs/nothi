<style>
    input[type=checkbox]{
        height: 16px;
    }
</style>
<div>
    <?php
        if(!empty($api) && ($api == true || $api == 1) && !empty($employee_office_info)){
            echo '<div style="position:fixed;top: 0;width: 100%;background: #e9f7cf;z-index: 99999;padding: 10px;">নোটের অনুমতিপ্রাপ্ত ব্যক্তিবর্গের তালিকা <button type="button" class="btn btn-md green nothiPermissionUpdate" aria-hidden="true"> সংরক্ষণ</button></div>';
            echo '<div style="margin-top: 85px;">';
            echo $this->cell('NothiOwnUnitPermissionList::updatePermission', ['', $id,$nothi_office,$employee_office_info,true])->render('update_permission');
            echo '</div>';
            ?>
            <script>
                $(document).on('click', ".nothiPermissionUpdate", function (e) {
                    $(this).addClass('disabled');
                    $(this).html('সংরক্ষণ হচ্ছে, অপেক্ষা করুন...');
                    var currentElement = this;
                    toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "positionClass": "toast-bottom-right"
                    };


                    var selectedPriviliges = {};
                    var j = 0;
                    var posi = 0;
                    $.each($('#ownOfficeSelected').find('.own_office_permission'), function (i, v) {
                        selectedPriviliges[j] = {};
                        selectedPriviliges[j]['ofc_id'] = $(v).data('office-id');
                        selectedPriviliges[j]['unit_id'] = $(v).data('office-unit-id');
                        selectedPriviliges[j]['org_id'] = $(v).data('office-unit-organogram-id');
//            selectedPriviliges[j]['designation_level'] = $(v).data('designation-level');
                        selectedPriviliges[j]['perm_value'] = 1;//parseInt($(v).val());
                        j = j + 1;
                    });
                    $.each($('#ownOfficeSelected').find('tbody>tr'), function (i, v) {
                        if ($(v).find('#designation_level').val() == '') {
                            $(v).find('#designation_level').val(0);
                        }
                        selectedPriviliges[i]['designation_level'] = $(v).find('#designation_level').val();
                    });
                    $.each($('#otherOfficeSelected').find('tbody>tr'), function (i, v) {
                        selectedPriviliges[i + j] = {};
                        if ($(v).find('#designation_level').val() == '') {
                            $(v).find('#designation_level').val(0);
                        }
                        selectedPriviliges[i + j]['designation_level'] = $(v).find('#designation_level').val();
                    });
                    $.each($('#otherOfficeSelected').find('.other_office_permission'), function (i, v) {
//            selectedPriviliges[j] = {};
                        selectedPriviliges[j]['ofc_id'] = $(v).data('office-id');
                        selectedPriviliges[j]['unit_id'] = $(v).data('office-unit-id');
                        selectedPriviliges[j]['org_id'] = $(v).data('office-unit-organogram-id');
//            selectedPriviliges[j]['designation_level'] = $(v).data('designation-level');
                        selectedPriviliges[j]['perm_value'] = 1;//parseInt($(v).val());
                        j = j + 1;
                    });

                    $.ajax({
                        url: js_wb_root + "nothiMasters/nothiMasterPermissionUpdate/" + <?php echo $id ?> +'/<?php echo $nothi_office ?>',
                        data: {'api': 1, 'employee_office': <?= json_encode($employee_office_info) ?>, priviliges: selectedPriviliges},
                        dataType: 'JSON',
                        method: 'POST',
                        success: function (response) {

                            if (response.status == 'error') {

                                toastr.error(response.msg);

                            } else if (response.status == 'success') {
                                Metronic.unblockUI('#ajax-content');
                                if (response.status == 'error') {

                                    if(posi===0){
                                        toastr.error(response.msg,"ত্রুটি",{
                                            "positionClass": "toast-top-right"
                                        });
                                    }else{
                                        toastr.error(response.msg,"ত্রুটি",{
                                            "positionClass": "toast-bottom-right"
                                        });
                                    }

                                } else if (response.status == 'success') {
                                    if(posi===0){
                                        toastr.success(response.msg,"সফল",{
                                            "positionClass": "toast-top-right"
                                        });
                                    }else{
                                        toastr.success(response.msg,"সফল",{
                                            "positionClass": "toast-bottom-right"
                                        });
                                    }
                                    if(!isEmpty(response.extra_message)){
                                        bootbox.dialog({
                                            message: response.extra_message,
                                            title: "অনুমতি সংশোধন সময়কার সমস্যা",
                                            buttons: {

                                                danger: {
                                                    label: "বন্ধ করুন",
                                                    className: "red",
                                                    callback: function () {
                                                    }
                                                }
                                            }
                                        });
                                    }
                                }
                            }

                            $(currentElement).removeClass('disabled');
                            $(currentElement).html('সংরক্ষণ');
                        },
                        error: function (status, xresponse) {

                        }

                    });
                });
            </script>
            <?php
        }else{
            echo $this->cell('NothiOwnUnitPermissionList::updatePermission', ['', $id,$nothi_office]);
        }

    ?>
</div>
<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2.css"/>
<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2.min.js"></script>
<script>
    jQuery(document).ready(function(){
         $(document).find('[title]').tooltip({placement: 'top'});
		$('select').select2('destroy');
    });

    $(document).find('[title]').tooltip({'placement':'bottom'});

    $("#ownOfficeUnitList").on("change", function() {
        var id = $(this).val();
        if (id == 0) {
            $('[id^="collapse_"]').parent().show();
        } else {
            $('[id^="collapse_"]').parent().hide();
            $('[id="collapse_'+id+'"]').parent().show();
            $('[id="collapse_'+id+'"]').parent().find('.accordion-toggle').trigger('click');
        }
    });
</script>
