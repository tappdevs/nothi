<style>

    .portlet-body img {
        max-width: 100%;
        height: auto;
    }

    div.attachmentsRecord {
        display: none;
    }

    div.attachmentsallRecord {
        display: none;
    }

    div.attachmentsRecord.active {
        display: block;
    }

    div.attachmentsallRecord.active {
        display: block;
    }

    div.DraftattachmentsRecord {
        display: none;
    }

    div.DraftattachmentsRecord.active {
        display: block;
    }

    div.NothijatoattachmentsRecord {
        display: none;
    }

    div.NothijatoattachmentsRecord.active {
        display: block;
    }

    div.summaryDraftattachmentsRecord {
        display: none;
    }

    div.summaryDraftattachmentsRecord.active {
        display: block;
    }

    .pager li > a, .pager li > span {
        display: inline-block;
        padding: 5px 8px;
        background-color: #fff;
        border: 1px solid #ddd;
        border-radius: 0;
    }

    .potroview {
        margin: 0 auto;
    }

    div.potroAccordianPaginationBody {
        display: none;
    }

    div.potroAccordianPaginationBody.active {
        display: block;
    }

    a {
        cursor: pointer;
    }

    .editable {
        border: none;
        word-break: break-word;
        word-wrap: break-word
    }

	 
		/* #noteView  
		{
			overflow:scroll;
		}
	 
		#noteView table tr td:first-child
		{
			width:65px  !important;
		}
		#noteView table tr td
		{
			width:fit-content;
			word-break:normal;
			/*white-space: nowrap;*/
		/* } */

		/* #noteView table tr td:first-child
		{
			width:65px !important;
		} */
  
 


    #potrodraft_list #sovapoti_signature, #potrodraft_list #sender_signature, #potrodraft_list #sender_signature2, #potrodraft_list #sender_signature3 {
        visibility: hidden;
    }

    #potrodraft_list #sovapoti_signature_date, #potrodraft_list #sender_signature_date, #potrodraft_list #sender_signature2_date, #potrodraft_list #sender_signature3_date {
        visibility: hidden;
    }

    .showimageforce #sovapoti_signature, .showimageforce #sender_signature, .showimageforce #sender_signature2, .showimageforce #sender_signature3,
    .showimageforce #sovapoti_signature_date, .showimageforce #sender_signature_date, .showimageforce #sender_signature2_date, .showimageforce #sender_signature3_date {
        visibility: visible !important;
    }

    #sarnothidraft_list img[alt=signature] {
        visibility: hidden;
    }

    .showImage img {
        visibility: visible !important;
    }

    #template-bodybody img[alt=signature] {
        visibility: hidden;
    }

    #note {
        margin-left: 0px !important;
        overflow-wrap: break-word;
        word-wrap: break-word;

        /* Adds a hyphen where the word breaks */
        -webkit-hyphens: auto;
        -ms-hyphens: auto;
        -moz-hyphens: auto;
        hyphens: auto;
    }

    .bangladate {
        border-bottom: 1px solid #000;
    }

    .A4 {
        background: white;
        width: 21cm;
        height: 29.7cm;
        display: block;
        margin: 0 auto;
        padding: 10px 25px;
        margin-bottom: 0.5cm;
        box-shadow: 0 0 0.5cm rgba(0, 0, 0, 0.5);
        overflow-y: auto;
        box-sizing: border-box;
        font-size: 12pt;
    }

    .glyphicon.glyphicon-copy {
        font-size: 16px;
    }

    .scroller {
        overflow: auto !important;
    }
    .alert.alert-silver {
        background: #ebebeb;
    }
    .potro_receiver_list_user {
        border: solid 1px silver;
        padding: 7px;
        margin-top:2px;
        background: #fff;
    }
    .modal_receiver_list legend {
        background: #ebebeb;
        border: solid 1px silver;
    }
    .modal_receiver_list fieldset>div {
        height: 200px;
        overflow: auto;
    }
</style>
<?php
$bookMarkLi = '';
$currentLi = array();
//if there is cs templates we need to add an extra js file
$hasCsTemplate = 0;
//dd($privilige_type);
?>
<?= $this->Form->hidden('nothi_office', ['id' => 'nothi_office', 'value' => $nothi_office]) ?>
<?= $this->Form->hidden('nothi_part_no', ['id' => 'nothi_part_no', 'value' => $id]) ?>
<div class="portlet light">
    <div class="portlet-title" style="border-bottom: 1px solid #eee;">
        <div class="row">
            <div class="col-lg-2 col-md-3 col-sm-3">
                <ul class="pager">
                    <li style="cursor: pointer;" id="potroLeftMenuToggle" title="সব পতাকা দেখুন"
                        data-original-title="সব পতাকা দেখুন">
                        <i class="fs0 a2i_gn_view1" style="color:green;"></i>
                    </li>
                    <?php if ($privilige_type != 0 && $otherNothi == false): ?>
                        <li>
                            <a id="master_file_page"
                               href="<?= $this->Url->build(['controller' => 'nothiMasters', 'action' => 'masterFile', '?' => ['nothi_master' => $nothimastersid, 'nothi_part' => $id]]) ?>"
                               data-original-title="মাস্টার ফাইল থেকে ক্লোন করুন" title="মাস্টার ফাইল থেকে ক্লোন করুন"
                               style='border:none !important;padding:0px;'><i class="fs1 a2i_gn_hardcopy3"
                                                                              aria-hidden="true"
                                                                              style="font-size: 13px;"></i></a>
                        </li>
                    <?php endif; ?>
                </ul>
            </div>
            <div class="col-lg-10 col-md-9 col-sm-9">
                <div class="row">
                    <div class="col-md-12 input-group btn-group btn-group-round">
                        <input type="text" name="potroSearch" class="form-control input-md potroSearch"
                               style="width:70%;z-index:1" placeholder="পত্রের বিষয় অথবা স্মারক নম্বর দিয়ে খুজুন"
                               id="potrosearch">

                        <?= $this->Form->button('<i class="fs1 efile-search3"></i>', ['class' => 'btn btn-primary btn-md potroSearchButton', 'type' => 'button', 'title' => 'খুঁজুন', 'style' => 'margin:0px!important;']) ?>
                        <?= $this->Form->button('<i class="fs1 a2i_gn_reset2"></i>', ['class' => 'btn btn-danger btn-md potroSearchResetButton', 'type' => 'button', 'title' => 'রিসেট', 'style' => 'margin:0px!important;']) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="portlet-body">
        <div class="row divcenter">
            <div class="col-lg-0 col-md-0 col-sm-0 col-xs-0 potroLeftMenu" style="display: none;;">
                <?php
                if (!empty($potroFlags)) {
                    echo '<div class="scroller" style="height: 700px;" data-always-visible="1" data-rail-visible1="1">';

                    foreach ($potroFlags as $key => $value) {
                        $value['title'] = h($value['title']);
                        echo "<div title='পত্র নম্বর: {$value['potro_no_bn']}' style='font-size:8pt;width: 100%; padding: 1px 1px; border-radius: 0;word-wrap: break-word; white-space: pre-line;'  href='{$value['attachment_id']}/potaka/{$value['nothi_master_id']}/{$value['page']}' class='potroFlagButton showforPopup btn btn-default btn-{$value['class']}' btn_potro_no=" . $value['potro_no'] . ">{$value['title']}</div>";
                    }
                    echo '</div>';
                }
                ?>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 potroContentSide">
                <div class="potroDetailsPage">
                    <div class="tabbable-custom nav-justified">
                        <ul class="nav nav-tabs nav-justified potroview_tab">
                            <?php if (!empty($draftSummary) || !empty($draftPotro)):
                                ?>
                                <li class="dropdown active">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:;"
                                       aria-expanded="true">খসড়া&nbsp;<b class="caret"></b></a>
                                    <ul class="dropdown-menu" role="menu">

                                        <?php if (!empty($draftSummary)): ?>
                                            <li class="active">
                                                <a href="#sarnothidraft_list" data-toggle="tab">
                                                    খসড়া সার-সংক্ষেপ
                                                </a>
                                            </li>
                                        <?php endif; ?>
                                        <?php if (!empty($draftPotro)): ?>
                                            <li>
                                                <a href="#potrodraft_list" data-toggle="tab">
                                                    খসড়া পত্র
                                                </a>
                                            </li>
                                        <?php endif; ?>
                                    </ul>
                                </li>
                            <?php endif; ?>


                            <?php if (!empty($summaryAttachmentRecord)): ?>
                                <li class="<?php
                                echo(empty($draftSummary) && empty($draftPotro) ? 'active' : '')
                                ?>">
                                    <a href="#summaryfinal_list" data-toggle="tab">
                                        সার-সংক্ষেপ
                                    </a>
                                </li>
                            <?php endif; ?>
                            <li class=" <?php
                            echo(empty($draftSummary) && empty($summaryAttachmentRecord) ? (!empty($draftPotro)
                                ? '' : 'active') : '')
                            ?>">
                                <a href="#potroview_list" data-toggle="tab">
                                    নোটের পত্র
                                </a>
                            </li>
                            <li class="loadsokolpotro">
                                <a href="#potroview_alllist" data-toggle="tab">
                                    সকল পত্র
                                </a>
                            </li>
                            <li class="sokolnothijatopotro">
                                <a href="#nothijato_list" data-toggle="tab">
                                    নথিজাত পত্র
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <?php if (!empty($draftSummary)): ?>
                                <div class="tab-pane active" id="sarnothidraft_list">
                                    <div class="portlet light ">
                                        <div class="portlet-body" style="overflow: auto;">
                                            <div class="row divcenter">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="potroview">

                                                        <div style="background-color: #a94442; padding: 5px; color: #fff;">
                                                            <div class="pull-left"
                                                                 style="padding-top: 0px;    white-space: nowrap;">
                                                                স্মারক নম্বর:
                                                                &nbsp;&nbsp;<?php echo h($draftSummary['sarok_no']); ?>
                                                            </div>
                                                            <div class="pull-right text-right">
                                                                <div class="btn-group">
                                                                    <button type="button"
                                                                            class="btn btn-success btn-xs dropdown-toggle"
                                                                            data-toggle="dropdown"
                                                                            aria-expanded="false">
                                                                        <i class="fa fa-ellipsis-horizontal"></i>
                                                                        প্রিন্ট <i class="fa fa-angle-down"></i>
                                                                    </button>
                                                                    <ul class="dropdown-menu pull-right" role="menu">
                                                                        <li>
                                                                            <a title="প্রিন্ট প্রিভিউ"
                                                                               data-id="<?= $draftSummary['id'] ?>"
                                                                               class="btn-printsummarydraft"><i
                                                                                        class="fs1 a2i_gn_print2"></i>
                                                                                প্রিন্ট প্রিভিউ</a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                                <?php
                                                                if ($otherNothi == false):
                                                                    ?>
                                                                    <a title="ড্রাফট পরিবর্তন দেখুন"
                                                                       nothi_master_id="<?php echo $draftSummary['nothi_part_no'] ?>"
                                                                       potrojari="<?php echo $draftSummary['id'] ?>"
                                                                       class="btn   btn-primary btn-sm btn-changelog">
                                                                        <i class="fs1 a2i_gn_history3"></i>
                                                                    </a>
                                                                    <a data-title-orginal="খসড়া সার-সংক্ষেপ দেখুন"
                                                                       title="খসড়া সার-সংক্ষেপ দেখুন"
                                                                       href="<?php echo $this->request->webroot ?>SummaryNothi/EditSarNothiDraft/<?php echo $draftSummary['nothi_part_no'] . '/' . $draftSummary['id'] ?>"
                                                                       class="btn  btn-xs green"> <i
                                                                                class="fs1 a2i_nt_potrojari3"></i> </a>
                                                                    <?php
                                                                endif;
                                                                if ($privilige_type == 1 && $otherNothi
                                                                    == false
                                                                ):
                                                                    if (!empty($potrosummarydraftstatus) && $employee_office['office_unit_organogram_id']
                                                                        == $potrosummarydraftstatus[$draftSummary['id']]['designation_id']
                                                                    ) {
                                                                        ?>
                                                                        <input type="checkbox"
                                                                               class=" summary_draft_approve <?php
                                                                               echo ($potrosummarydraftstatus[$draftSummary['id']]['is_approve'])
                                                                                   ? 'checked' : ''
                                                                               ?>" <?php
                                                                        echo ($potrosummarydraftstatus[$draftSummary['id']]['is_approve'])
                                                                            ? 'checked=checked'
                                                                            : ''
                                                                        ?>
                                                                               potrojari_id="<?= $draftSummary['id'] ?>"/><?php echo __('অনুমোদন  ') ?>
                                                                        <?php
                                                                    }

                                                                    ?>
                                                                    <a data-title-orginal="সার-সংক্ষেপ জারি করুন"
                                                                       title="সার-সংক্ষেপ জারি করুন"
                                                                       class="btn btn-xs  green <?= ($is_approve ? 'btn-send-summary-draft' : (!empty($potrosummarydraftstatus[$draftSummary['id']]['is_approve']) ? 'btn-check-summary-draft' : 'hide')) ?>"
                                                                       potrojari="<?php echo $draftSummary['id'] ?>">
                                                                        <i class="fs1 a2i_gn_send1"></i> </a>

                                                                    <?php if (!$summaryApprove): ?>
                                                                    <a data-title-orginal="খসড়া পত্র মুছে ফেলুন"
                                                                       title="খসড়া পত্র মুছে ফেলুন"
                                                                       class="btn  btn-xs red btn-delete-summary-draft"
                                                                       potrojari="<?php echo $draftSummary['id'] ?>"> <i
                                                                                class="fs1 a2i_gn_delete2 "></i> </a>
                                                                <?php endif; ?>
                                                                <?php endif; ?>
                                                            </div>
                                                            <div style="clear: both;"></div>
                                                        </div>
                                                        <div class="summaryContentBody">
                                                            <div class="tabbable-custom nav-justified">
                                                                <ul class="nav nav-tabs nav-justified">
                                                                    <li class="active">
                                                                        <a href="#tab_cover" data-toggle="tab">
                                                                            কভার পাতা</a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#tab_sar_body" data-toggle="tab">
                                                                            সার-সংক্ষেপ </a>
                                                                    </li>
                                                                </ul>
                                                                <div class="tab-content">
                                                                    <div class="tab-pane active" id="tab_cover">
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <div id="template-body"
                                                                                     style="background-color: #fff; max-width:950px; min-height:815px; height: auto; margin:0 auto;">
                                                                                    <?php echo $draftSummary->potro_cover; ?>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="tab-pane" id="tab_sar_body">
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <div id="template-body2"
                                                                                     style="background-color: #fff; max-width:950px; min-height:815px; height: auto; margin:0 auto;">
                                                                                    <?php echo $draftSummary->potro_description; ?>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <?php if (!empty($draftPotro)): ?>
                                <div class="tab-pane <?php
                                echo(!empty($draftSummary) ? '' : 'active')
                                ?>" id="potrodraft_list">
                                    <div class="portlet light">
                                        <div class="portlet-title">
                                            <div class="row">
                                                <div class="col-lg-7 col-md-7 col-sm-7 text-left">
                                                    <nav>
                                                        <ul class="pager">
                                                            <li><a href="javascript:void(0);"
                                                                   class="DraftimagePrev btn btn-sm default disabled"><</a>
                                                            </li>
                                                            <li><a href="javascript:void(0);"
                                                                   class="DraftimageNext btn btn-sm default">></a>
                                                            </li>
                                                        </ul>
                                                    </nav>
                                                </div>
                                                <div class="col-lg-5 col-md-5 col-sm-5 text-right">
                                                    <p> মোট: <b><?php echo $this->Number->format($draftPotroCount); ?>
                                                            টি</b></p>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="portlet-body" style="overflow-y: auto;overflow-x:  hidden;">
                                            <div class="row divcenter">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="potroview">
                                                        <?php
                                                        if (!empty($draftPotro)) {
                                                            $i = 0;

                                                            foreach ($draftPotro as $row) {
                                                                echo '<div id_en="' . $row['nothi_potro_page'] . '"  id_bn="' . $row['nothi_potro_page_bn'] . '" class="DraftattachmentsRecord ' . ($i
                                                                    == 0 ? 'active first' : ($i == (count($draftPotro)
                                                                        - 1) ? 'last' : '')) . '">';
                                                                ?>
                                                                <div style="background-color: #a94442; padding: 5px; color: #fff;">
                                                                    <div class="pull-left"
                                                                         style="padding-top: 0px;    white-space: nowrap;">
                                                                        <?php if ($row['cloned_potrojari_id']): ?>
                                                                            <i class="fs1 a2i_gn_hardcopy3"
                                                                               title="এই পত্রটি ক্লোন করা হয়েছে"
                                                                               data-toggle="tooltip"></i>
                                                                        <?php endif; ?>
                                                                        <?php echo mb_substr($row['potro_subject'], 0, 50) . (mb_strlen(h($row['potro_subject'])) > 50 ? '...' : ''); ?>
                                                                    </div>
                                                                    <div class="pull-right text-right">
                                                                        <div class="btn-group">
                                                                            <button type="button"
                                                                                    class="btn btn-success btn-xs dropdown-toggle"
                                                                                    data-toggle="dropdown"
                                                                                    aria-expanded="false">
                                                                                <i class="fa fa-ellipsis-horizontal"></i>
                                                                                প্রিন্ট <i class="fa fa-angle-down"></i>
                                                                            </button>
                                                                            <ul class="dropdown-menu pull-right">
                                                                                <li>
                                                                                    <a title="প্রিন্ট প্রিভিউ"
                                                                                       data-id="<?= $row['id'] ?>"
                                                                                       data-potro-type="<?= $row['potro_type'] ?>"
                                                                                       class="btn-printdraft"><i
                                                                                                class="fs1 a2i_gn_print2"></i>
                                                                                        প্রিন্ট প্রিভিউ</a>
                                                                                </li>
                                                                            </ul>
                                                                        </div>

                                                                        <a title="ড্রাফট পরিবর্তন দেখুন"
                                                                           nothi_master_id="<?php echo $row['nothi_part_no'] ?>"
                                                                           potrojari="<?php echo $row['id'] ?>"
                                                                           class="btn   btn-primary btn-xs btn-changelog">
                                                                            <i class="fs1 a2i_gn_history3"></i>
                                                                        </a>
                                                                        <?php
                                                                        //RM url different
                                                                        if (($row['potro_type'] == 21)) {
                                                                            $url = $this->Url->build(['_name' => 'editRm', $row['nothi_part_no'], 0, ($row['id'] == 0) ? 'note' : 'potro', 0, $row['id']]);
                                                                        } // formal
                                                                        else if (($row['potro_type'] == 27)) {
                                                                            $url = $this->Url->build(['_name' => 'edit-formal-letter', $row['nothi_part_no'], 0, ($row['id'] == 0) ? 'note' : 'potro', 0, $row['id']]);
                                                                        } // CS
                                                                        else if (($row['potro_type'] == 30)) {
                                                                            $url = $this->Url->build(['_name' => 'edit-cs-letter', $row['nothi_part_no'], 0, ($row['id'] == 0) ? 'note' : 'potro', 0, $row['id']]);
                                                                        } else {
                                                                            $url = $this->Url->build(['controller' => 'Potrojari', 'action' => 'potroDraft', $row['nothi_part_no'], $row['id'], ($row['id'] == 0) ? 'note' : 'potro', $nothi_office]);
                                                                        }
                                                                        ?>

                                                                        <a data-title-orginal="খসড়া পত্র দেখুন"
                                                                           title="খসড়া পত্র দেখুন"
                                                                           href="<?php echo $url; ?>?potro_edit=yes"
                                                                           class="btn   btn-xs green">
                                                                            <i class="fs1 a2i_nt_potrojari3"></i>
                                                                        </a>
                                                                        <?php
                                                                        // endif;

                                                                        if ($privilige_type == 1):
                                                                            ?>
                                                                            <?php
                                                                            if ($row['can_potrojari']
                                                                                == 1 && $row['approval_officer_designation_id']
                                                                                == $employee_office['office_unit_organogram_id']
                                                                            ) {
                                                                                ?>
                                                                                <!--
                                                                           <?php
                                                                                echo $this->Form->postLink('<i class="fs1 a2i_gn_send1"></i>', ['controller' => 'Potrojari',
                                                                                    'action' => 'sendDraftCronPotrojari', $id, $row['id'], ($row['nothi_notes_id'] == 0) ? 'potro' : 'note'], ['confirm' => __('আপনি কি খসড়া পত্র পত্রজারি করতে চান?'), 'class' => "btn   btn-xs purple-medium", 'escape' => false, 'data' => ['nothi_office' => $nothi_office, 'redirect' => 1]]);
                                                                                ?>
                                                                        -->
                                                                                <a data-title-orginal="পত্রজারি করুন"
                                                                                   title="পত্রজারি করুন"
                                                                                   class="btn   btn-xs purple btn-send-draft-cron"
                                                                                   nothinotesid="<?php echo $row['nothi_notes_id'] ?>"
                                                                                   nothipotroid="<?php echo $row['nothi_potro_id'] ?>"
                                                                                   potrojari="<?php echo $row['id'] ?>"
                                                                                   part_no="<?php echo $row['nothi_part_no'] ?>"
                                                                                   nothi_office="<?php echo $nothi_office ?>"
                                                                                >
                                                                                    <i class="fs1 a2i_gn_send1"></i>
                                                                                </a>
                                                                                <?php
                                                                            } // if onumodon and sender same , then anyone can potrojari
                                                                            else if ($row['can_potrojari']
                                                                                == 1 && $row['approval_officer_designation_id']
                                                                                == $row['officer_designation_id'] && $row['potro_type'] != 17
                                                                            ) {
                                                                                ?>
                                                                                <!--
                                                                           <?php
                                                                                echo $this->Form->postLink('<i class="fs1 a2i_gn_send1"></i>', ['controller' => 'Potrojari',
                                                                                    'action' => 'sendDraftCronPotrojari', $id, $row['id'], ($row['nothi_notes_id'] == 0) ? 'potro' : 'note'], ['confirm' => __('আপনি কি খসড়া পত্র পত্রজারি করতে চান?'), 'class' => "btn   btn-xs purple-medium", 'escape' => false, 'data' => ['nothi_office' => $nothi_office, 'redirect' => 1]]);
                                                                                ?>
                                                                        -->
                                                                                <a data-title-orginal="পত্রজারি করুন"
                                                                                   title="পত্রজারি করুন"
                                                                                   class="btn   btn-xs purple btn-send-draft-cron"
                                                                                   nothinotesid="<?php echo $row['nothi_notes_id'] ?>"
                                                                                   nothipotroid="<?php echo $row['nothi_potro_id'] ?>"
                                                                                   potrojari="<?php echo $row['id'] ?>">
                                                                                    <i class="fs1 a2i_gn_send1"></i>
                                                                                </a>

                                                                                <?php
                                                                            } elseif ($row['potro_type']
                                                                                != 17 && $employee_office['office_unit_organogram_id']
                                                                                == $row['officer_designation_id']
                                                                            ) {
                                                                                ?>
                                                                                <input type="checkbox"
                                                                                       class=" approveDraftNothi <?php
                                                                                       echo ($row['can_potrojari'])
                                                                                           ? 'checked' : ''
                                                                                       ?>" <?php
                                                                                echo ($row['can_potrojari'])
                                                                                    ? 'checked=checked'
                                                                                    : ''
                                                                                ?>
                                                                                       potrojari_id="<?= $row['id'] ?>"/><?php echo __('অনুমোদন  ') ?>
                                                                                <?php
                                                                            } else if ($row['potro_type']
                                                                                == 17 && $employee_office['office_unit_organogram_id']
                                                                                == $row['sovapoti_officer_designation_id']
                                                                            ) {
                                                                                ?>
                                                                                <input type="checkbox"
                                                                                       class=" approveDraftNothi <?php
                                                                                       echo ($row['can_potrojari'])
                                                                                           ? 'checked' : ''
                                                                                       ?>" <?php
                                                                                echo ($row['can_potrojari'])
                                                                                    ? 'checked=checked'
                                                                                    : ''
                                                                                ?>
                                                                                       potrojari_id="<?= $row['id'] ?>"/><?php echo __('অনুমোদন  ') ?>
                                                                            <?php } ?>
                                                                            <?php
                                                                            if ($row['can_potrojari']
                                                                                != 1
                                                                            ):

                                                                                ?>
                                                                                <a data-title-orginal="খসড়া পত্র মুছে ফেলুন"
                                                                                   title="খসড়া পত্র মুছে ফেলুন"
                                                                                   class="btn   btn-xs red btn-delete-draft"
                                                                                   nothinotesid="<?php echo $row['nothi_notes_id'] ?>"
                                                                                   nothipotroid="<?php echo $row['nothi_potro_id'] ?>"
                                                                                   potrojari="<?php echo $row['id'] ?>">
                                                                                    <i class="fs1 a2i_gn_delete2 "></i>
                                                                                </a>
                                                                            <?php endif; ?>
                                                                        <?php endif; ?>
                                                                    </div>
                                                                    <div style="clear: both;"></div>
                                                                </div>
                                                                <?php
                                                                $header = jsonA($row->meta_data);
                                                                $cs_checker = (($privilige_type == 0) || (isset($header['has_signature']) && $header['has_signature'] == $employee_office['office_unit_organogram_id'])) ? '' :
                                                                    '(<input type="checkbox" class ="checker cs-body-approve" data-cs="' . $row['id'] . '" /> ' . __('Approval') . ' )';

                                                                if (isset($row['potro_type']) && $row['potro_type'] == 30) {
                                                                    $hasCsTemplate = 1;
                                                                    echo '<ul class="nav nav-tabs nav-justified">
                                                                            <li class="active">
                                                                                <a href="#csCover-' . $row['id'] . '" data-toggle="tab" aria-expanded="true">ফরওয়ার্ডিং পত্র</a>
                                                                            </li>
                                                                            <li class="">
                                                                                <a href="#csBody-' . $row['id'] . '" data-toggle="tab" aria-expanded="true">প্রতিবেদন ' . $cs_checker . '</a>
                                                                            </li>
                                                                        </ul>
                                                                        <div class="tab-content">
                                                                            <div class="tab-pane active csCover" id="csCover-' . $row['id'] . '"">' . (!empty($header['potro_header_banner']) ? ('<div class="potrojari_header_banner" style="text-align: ' . ($header['banner_position']) . '!important;"><img src="' . $this->request->webroot . 'getContent?file=' . base64_decode($header['potro_header_banner']) . '&token=' . sGenerateToken(['file' => base64_decode($header['potro_header_banner'])], ['exp' => time() + 60 * 300]) . '" style="width:' . ($header['banner_width']) . ';height: 60px;max-width: 100%;"/></div>') : '')
                                                                        . '<div class="showimageforce">' . html_entity_decode($row['attached_potro']) . '</div><br/>' . $row['potro_cover'] . '</div>';
                                                                    echo '<div class="tab-pane csBody" id="csBody-' . $row['id'] . '"">';
                                                                    echo '<div id="template-body" potrojari_language="' . h($row['potrojari_language']) . '" style="background-color: #fff; max-width:950px; min-height:815px; height: auto; margin:0 auto;" potro_type="' . $row['potro_type'] . '">'
                                                                        . html_entity_decode($row['potro_description']) . "</div>";
                                                                    echo '</div></div>';
                                                                } else {
                                                                    echo '<div id="template-body" potrojari_language="' . h($row['potrojari_language']) . '" style="background-color: #fff; max-width:950px; min-height:815px; height: auto; margin:0 auto;" potro_type="' . $row['potro_type'] . '">' . (!empty($header['potro_header_banner']) ? ('<div class="potrojari_header_banner" style="text-align: ' . ($header['banner_position']) . '!important;"><img src="' . $this->request->webroot . 'getContent?file=' . base64_decode($header['potro_header_banner']) . '&token=' . sGenerateToken(['file' => base64_decode($header['potro_header_banner'])], ['exp' => time() + 60 * 300]) . '" style="width:' . ($header['banner_width']) . ';height: 60px;max-width: 100%;"/></div>') : '') . '<div class="showimageforce">' . html_entity_decode($row['attached_potro']) . '</div><br/>' . html_entity_decode($row['potro_description']) . "</div>";
                                                                }
                                                                ?>
                                                                <div class="table">
                                                                    <table role="presentation"
                                                                           class="table table-bordered table-striped">

                                                                        <tbody class="files">
                                                                        <?php

                                                                        if (isset($draftPotroAttachments[$row['id']])
                                                                            && count($draftPotroAttachments[$row['id']])
                                                                            > 0
                                                                        ) {
                                                                            $total_attachment = 0;
                                                                            echo '<tr class="text-center">
                                                                                <td colspan="3">' . __('Attachments') . '</td>
                                                                        </tr>';
                                                                            foreach ($draftPotroAttachments[$row['id']] as $ke => $single_data) {

                                                                                if (($single_data['attachment_type']
                                                                                        == 'text' || $single_data['attachment_type']
                                                                                        == 'text/html') && empty($single_data['potro_id'])
                                                                                ) {
                                                                                    continue;
                                                                                }
                                                                                if (!(empty($single_data['file_name']))) {
                                                                                    $fileName = explode('/',
                                                                                        $single_data['file_name']);
                                                                                    $attachmentHeaders
                                                                                        = get_file_type($single_data['file_name']);
                                                                                    $value = array(
                                                                                        'name' => urldecode($fileName[count($fileName)
                                                                                        - 1]),
                                                                                        'thumbnailUrl' => $this->request->webroot . 'content/' . $single_data['file_name'],
                                                                                        'url' => $this->request->webroot . 'content/' . $single_data['file_name'],
                                                                                        'visibleName' => (!empty($single_data['user_file_name']) ? $single_data['user_file_name'] : urldecode($fileName[count($fileName) - 1])),
                                                                                    );
                                                                                } else {
                                                                                    $value = array(
                                                                                        'name' => 'পত্রঃ ' . $single_data['sarok_no'],
                                                                                        'thumbnailUrl' => '',
                                                                                        'url' => '',
                                                                                        'visibleName' => '',
                                                                                    );
                                                                                }
                                                                                $total_attachment++;
                                                                                echo '<tr class="template-download fade in">

<td>
<p class="name">
' . (isset($value['name']) && (substr($single_data['attachment_type'], 0, 5) == 'image' ||
                                                                                        $single_data['attachment_type'] == 'application/pdf') ?
                                                                                        '<a href="' . $single_data['id'] . '/showPotroAttachment/' . $row['nothi_part_no'] . '" 
title="' . $value['name'] . '" class="showforPopup">' . urldecode($value['name']) . '</a>' : (isset($value['name']) ? urldecode($value['name']) : '')) . '
</p>
</td>
<td>' . $value['visibleName'] . '</td>
<td>
' . (($single_data['attachment_type'] != 'text' && $single_data['attachment_type'] != 'text/html') ? $this->Html->link('<i class="fs1 fa fa-download"></i> &nbsp;', ['controller' => 'Potrojari',
                                                                                        'action' => 'downloadPotro',
                                                                                        $nothi_office,
                                                                                        $single_data['id']], ['escape' => false]) : '') . '
</td>

</tr>';
                                                                            }
                                                                            if ($total_attachment == 0) {
                                                                                echo '<tr class="template-download fade in"><td colspan="3" class="danger text-center"> ' . __('Sorry no attachment found') . '</td></tr>';
                                                                            }
                                                                        }
                                                                        ?>
                                                                        </tbody>
                                                                    </table>
                                                                </div>

                                                                <div class="potro_receiver_list" style="display:none">
                                                                    <div class="alert alert-info">
                                                                        <i class="fa fa-info-circle"></i>
                                                                        এই পত্রের প্রাপক ও অনুলিপিসমূহ
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <fieldset class="alert alert-silver">
                                                                                <legend class="alert alert-info" style="width: auto !important;margin-bottom: 5px !important;">প্রাপকসমূহ (<?=enTobn(count($potro_receiver_list[$row['id']]['receiver']['System'])+count($potro_receiver_list[$row['id']]['receiver']['Other']))?>)</legend>
                                                                                <div class="row">
                                                                                    <div class="col-md-6">
                                                                                        <div class="bold">সিস্টেমের ব্যবহারকারী</div>
                                                                                        <?php foreach($potro_receiver_list[$row['id']]['receiver']['System'] as $key => $list):?>
                                                                                        <div class="potro_receiver_list_user"><?= '<b>'.$list['receiving_officer_name'].'</b>,'.
                                                                                            $list['receiving_officer_designation_label'].','.
                                                                                            $list['receiving_office_unit_name'].','.
                                                                                            $list['receiving_office_name'] ?></div>
                                                                                        <?php endforeach;?>
                                                                                    </div>
                                                                                    <div class="col-md-6">
                                                                                        <div class="bold">সিস্টেম বহির্ভূত ব্যবহারকারী</div>
                                                                                        <?php foreach($potro_receiver_list[$row['id']]['receiver']['Other'] as $key => $list):?>
                                                                                            <div class="potro_receiver_list_user"><?= ($list['receiving_officer_name']) ? '<b>'.$list['receiving_officer_name'].'</b>,' :'' ?>
                                                                                                <?= ($list['receiving_officer_designation_label']) ? $list['receiving_officer_designation_label'].',' : '' ?>
                                                                                                <?= ($list['receiving_office_unit_name']) ? $list['receiving_office_unit_name'].',' : '' ?>
                                                                                                <?= ($list['receiving_office_name']) ? $list['receiving_office_name'].',' : '' ?>
                                                                                                <?= ($list['officer_mobile']) ? implode(', ', array_filter(explode('--', $list['officer_mobile']))).',' : '' ?>
                                                                                                <?= ($list['receiving_officer_email']) ? '('. $list['receiving_officer_email'] .')' : '(ইমেইল দেওয়া হয়নি)' ?></div>
                                                                                        <?php endforeach;?>
                                                                                    </div>
                                                                                </div>
                                                                            </fieldset>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <fieldset class="alert alert-silver">
                                                                                <legend class="alert alert-info" style="width: auto !important;margin-bottom: 5px !important;">অনুলিপিসমূহ (<?=enTobn(count($potro_receiver_list[$row['id']]['onulipi']['System'])+count($potro_receiver_list[$row['id']]['onulipi']['Other']))?>)</legend>
                                                                                <div class="row">
                                                                                    <div class="col-md-6">
                                                                                        <div class="bold">সিস্টেমের ব্যবহারকারী</div>
                                                                                        <?php foreach($potro_receiver_list[$row['id']]['onulipi']['System'] as $key => $list):?>
                                                                                            <div class="potro_receiver_list_user"><?= '<b>'.$list['receiving_officer_name'].'</b>,'.
                                                                                                $list['receiving_officer_designation_label'].','.
                                                                                                $list['receiving_office_unit_name'].','.
                                                                                                $list['receiving_office_name'] ?></div>
                                                                                        <?php endforeach;?>
                                                                                    </div>
                                                                                    <div class="col-md-6">
                                                                                        <div class="bold">সিস্টেম বহির্ভূত ব্যবহারকারী</div>
                                                                                        <?php foreach($potro_receiver_list[$row['id']]['onulipi']['Other'] as $key => $list):?>
                                                                                            <div class="potro_receiver_list_user"><?= ($list['receiving_officer_name']) ? '<b>'.$list['receiving_officer_name'].'</b>,' :'' ?>
                                                                                                <?= ($list['receiving_officer_designation_label']) ? $list['receiving_officer_designation_label'].',' : '' ?>
                                                                                                <?= ($list['receiving_office_unit_name']) ? $list['receiving_office_unit_name'].',' : '' ?>
                                                                                                <?= ($list['receiving_office_name']) ? $list['receiving_office_name'].',' : '' ?>
                                                                                                <?= ($list['officer_mobile']) ? implode(', ', array_filter(explode('--', $list['officer_mobile']))).',' : '' ?>
                                                                                                <?= ($list['receiving_officer_email']) ? '('. $list['receiving_officer_email'] .')' : '(ইমেইল দেওয়া হয়নি)' ?></div>
                                                                                        <?php endforeach;?>
                                                                                    </div>
                                                                                </div>
                                                                            </fieldset>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <?php
                                                                echo "</div>";
                                                                $i++;
                                                            }
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <?php if (!empty($summaryAttachmentRecord)): ?>
                                <div class="tab-pane <?php
                                echo((empty($draftSummary) && empty($draftPotro)) ? 'active' : '')
                                ?>" id="summaryfinal_list">
                                    <div class="portlet light">
                                        <div class="portlet-title">
                                            <div class="row">
                                                <div class="col-lg-7 col-md-7 col-sm-7 text-left">
                                                    <nav>
                                                        <ul class="pager">
                                                            <li><a href="javascript:void(0);"
                                                                   class="summaryDraftimagePrev btn btn-sm default disabled"><</a>
                                                            </li>
                                                            <li><a href="javascript:void(0);"
                                                                   class="summaryDraftimageNext btn btn-sm default">></a>
                                                            </li>
                                                        </ul>
                                                    </nav>
                                                </div>
                                                <div class="col-lg-5 col-md-5 col-sm-5 text-right">
                                                    <p> মোট: <b><?php
                                                            $totalSum = !empty($summaryAttachmentRecord)
                                                                ? count($summaryAttachmentRecord)
                                                                : 0;
                                                            echo $this->Number->format($totalSum);
                                                            ?>টি</b></p>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="portlet-body" style="overflow-y: auto;overflow-x:  hidden;">
                                            <div class="row divcenter">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="potroview">
                                                        <?php
                                                        if (!empty($summaryAttachmentRecord)) {
                                                            $i = 0;
                                                            foreach ($summaryAttachmentRecord as $row) {
                                                                echo '<div id_en="' . $row['nothi_potro_page'] . '"  id_bn="' . $row['nothi_potro_page_bn'] . '" class="summaryDraftattachmentsRecord ' . ($i
                                                                    == 0 ? 'active first' : ($i == (count($draftPotro)
                                                                        - 1) ? 'last' : '')) . '">';
                                                                ?>
                                                                <div style="background-color: #a94442; padding: 5px; color: #fff;">
                                                                    <div class="pull-left"
                                                                         style="padding-top: 0px;    white-space: nowrap;">
                                                                        <?php if ($row['cloned_potrojari_id']): ?>
                                                                            <i class="fs1 a2i_gn_hardcopy3"
                                                                               title="এই পত্রটি ক্লোন করা হয়েছে"
                                                                               data-toggle="tooltip"></i>
                                                                        <?php endif; ?>
                                                                        স্মারক নম্বর:
                                                                        &nbsp;&nbsp;<?php echo $row['sarok_no']; ?>
                                                                    </div>
                                                                    <div class="pull-right text-right">
                                                                        <div class="btn-group">
                                                                            <button type="button"
                                                                                    class="btn btn-success btn-xs dropdown-toggle"
                                                                                    data-toggle="dropdown"
                                                                                    aria-expanded="false">
                                                                                <i class="fa fa-ellipsis-horizontal"></i>
                                                                                প্রিন্ট <i class="fa fa-angle-down"></i>
                                                                            </button>
                                                                            <ul class="dropdown-menu pull-right">
                                                                                <li>
                                                                                    <a title="প্রিন্ট করুন"
                                                                                       data-id="<?= $row['id'] ?>"
                                                                                       href="javascript:void(0)"
                                                                                       class="btn-summaryprintdraft"><i
                                                                                                class="fs1 a2i_gn_print2"></i>
                                                                                        প্রিন্ট প্রিভিউ</a
                                                                                </li>
                                                                            </ul>
                                                                        </div>

                                                                        <a data-title-orginal="সার-সংক্ষেপ দেখুন"
                                                                           title="সার-সংক্ষেপ দেখুন"
                                                                           href="<?php echo $this->request->webroot ?>SummaryNothi/summaryPotroShow/<?php echo $row['nothi_part_no'] . '/' . $row['nothi_potro_id'] ?>"
                                                                           class="btn   btn-xs green"> <i
                                                                                    class="fs1 a2i_nt_potrojari3"></i>
                                                                        </a>
                                                                        <?php
                                                                        $class = "btn-danger";
                                                                        $classicon = "fa fa-exclamation-triangle";
                                                                        if (!empty($row['potrojari_id'])) {
                                                                            $class = "btn-primary";
                                                                            $classicon = "fs1 a2i_gn_refresh1";
                                                                            ?>
                                                                            <a href="<?php
                                                                            echo $this->Url->build(['controller' => 'potrojari',
                                                                                'action' => 'pendingList',
                                                                                $nothi_office,
                                                                                $row['potrojari_id'], $row['nothi_part_no']])
                                                                            ?>" target="_tab"
                                                                               class="btn  btn-xs <?= $class ?>"
                                                                               title="পত্রজারি প্রেরণ অবস্থা দেখুন"
                                                                               data-title-orginal="পত্রজারি প্রেরণ অবস্থা দেখুন">
                                                                                <i class="<?= $classicon ?>"></i> </a>
                                                                            <a href="<?php
                                                                            echo $this->Url->build(['controller' => 'potrojari',
                                                                                'action' => 'potrojariDakTracking',
                                                                                $nothi_office,
                                                                                $row['potrojari_id'], $row['nothi_part_no']])
                                                                            ?>" target="_tab"
                                                                               class="btn  btn-xs green"
                                                                               title="জারিকৃত পত্রের অবস্থা"
                                                                               data-title-orginal="জারিকৃত পত্রের অবস্থা">
                                                                                <i class="fs1 a2i_nt_nothigotibidhi3"></i>
                                                                            </a>
                                                                            <?php
                                                                        }
                                                                        if ($privilige_type == 1):
                                                                            $can_potrojari_sum = false;
                                                                            if (isset($potrosummarystatus[$row['nothi_potro_id']])) {
                                                                                foreach ($potrosummarystatus[$row['nothi_potro_id']] as $kk => $vv) {
                                                                                    if ($vv['is_sent'] == 1) {
                                                                                        continue;
                                                                                    }
                                                                                    if ($vv['is_approve'] == 0) {
                                                                                        $can_potrojari_sum = false;
                                                                                        break;
                                                                                    } else {
                                                                                        $can_potrojari_sum = true;
                                                                                    }
                                                                                }
                                                                            }

                                                                            if ($can_potrojari_sum) {
                                                                                ?>
                                                                                <a data-title-orginal="পত্রজারি করুন"
                                                                                   title="পত্রজারি করুন"
                                                                                   class="btn   btn-xs green btn-send-potrojari"
                                                                                   nothipotroid="<?php echo $row['nothi_potro_id'] ?>">
                                                                                    <i class="fs1 a2i_gn_send1"></i>
                                                                                </a>
                                                                            <?php }
                                                                        endif; ?>
                                                                    </div>

                                                                    <div style="clear: both;"></div>
                                                                </div>
                                                                <?php
                                                                if ($row['attachment_type'] == 'text') {
                                                                        ?>
                                                                    <div class="summaryContentBody">
                                                                        <div class="tabbable-custom nav-justified">
                                                                            <ul class="nav nav-tabs nav-justified">
                                                                                <li class="active">
                                                                                    <a href="#tab_cover_<?=$row['id']?>" data-toggle="tab">
                                                                                        কভার পাতা</a>
                                                                                </li>
                                                                                <li>
                                                                                    <a href="#tab_sar_body_<?=$row['id']?>" data-toggle="tab">
                                                                                        সার-সংক্ষেপ </a>
                                                                                </li>
                                                                            </ul>
                                                                            <div class="tab-content">
                                                                                <div class="tab-pane active" id="tab_cover_<?=$row['id']?>">
                                                                                    <div class="row">
                                                                                        <div class="col-md-12">
                                                                                            <div id="template-body"
                                                                                                 style="background-color: #fff; max-width:950px; min-height:815px; height: auto; margin:0 auto;">
                                                                                                <?php echo $row['potro_cover']; ?>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="tab-pane" id="tab_sar_body_<?=$row['id']?>">
                                                                                    <div class="row">
                                                                                        <div class="col-md-12">
                                                                                            <div id="template-body2" style="background-color: #fff; max-width:950px; min-height:815px; height: auto; margin:0 auto;">
                                                                                                <?php echo $row['content_body']; ?>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <?php
                                                                } elseif (substr($row['attachment_type'],
                                                                        0, 5) != 'image'
                                                                ) {
                                                                    if (substr($row['attachment_type'],
                                                                            0,
                                                                            strlen('application/vnd'))
                                                                        == 'application/vnd' || substr($row['attachment_type'],
                                                                            0,
                                                                            strlen('application/ms'))
                                                                        == 'application/ms'
                                                                    ) {
                                                                        $url = urlencode(FILE_FOLDER . $row['file_name'] . '?token=' . sGenerateToken(['file' => $row['file_name']], ['exp' => time() + 60 * 300]));
                                                                        echo '<iframe style=" width:100%; height: 700px;" src="https://docs.google.com/gview?url=' . $url . '&embedded=true" style="width:600px; height:500px;" frameborder="0"></iframe>';
                                                                    } else {
//                    echo '<iframe style=" width:100%; height: 700px;" src="https://docs.google.com/gview?url='.$this->request->webroot . 'content/'.$row['file_name'].'&embedded=true" style="width:600px; height:500px;" frameborder="0"></iframe>';
                                                                        echo '<embed src="' . $this->request->webroot . 'content/' . $row['file_name'] . '?token=' . sGenerateToken(['file' => $row['file_name']], ['exp' => time() + 60 * 300]) . '" style=" width:100%; height: 700px;" type="' . $row['attachment_type'] . '"></embed>';
                                                                    }
                                                                } else {
                                                                    echo '<div class="text-center"><a onclick="return false;" href="' . $this->request->webroot . 'content/' . $row['file_name'] . '?token=' . sGenerateToken(['file' => $row['file_name']], ['exp' => time() + 60 * 300]) . '" data-title="পত্র নম্বর:  ' . $row['nothi_potro_page_bn'] . '" data-footer="" ><img class="zoomimg img-responsive"  src="' . $this->request->webroot . 'content/' . $row['file_name'] . '?token=' . sGenerateToken(['file' => $row['file_name']], ['exp' => time() + 60 * 300]) . '" alt="ছবি পাওয়া যায়নি"></a></div>';
                                                                }
                                                                echo "</div>";
                                                                $i++;
                                                            }
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <div class="tab-pane <?php
                            echo(empty($draftSummary) && empty($summaryAttachmentRecord) ? (!empty($draftPotro)
                                ? '' : 'active') : '')
                            ?>" id="potroview_list">

                                <div class="portlet light">
                                    <div class="portlet-title">
                                        <div class="row">
                                            <div class="col-lg-7 col-md-7 col-sm-7 text-left">
                                                <nav>
                                                    <ul class="pager">
                                                        <li><a href="javascript:void(0);"
                                                               class="imagePrev btn btn-sm default disabled"><</a>
                                                        </li>
                                                        <li>
                                                            <input value="<?php
                                                            echo $this->Number->format(!empty($potroAttachmentRecord)
                                                                ? $potroAttachmentRecord[0]['nothi_potro_page']
                                                                : 0)
                                                            ?>" type="text"
                                                                   class="imageNo pagination-panel-input form-control input-mini input-inline input-sm potroNo"
                                                                   maxlength="10"
                                                                   style="text-align:center; margin: 0 5px;">
                                                        </li>
                                                        <li><a href="javascript:void(0);"
                                                               class="imageNext btn btn-sm default">></a>
                                                        </li>
                                                        <?php if (!empty($total)): ?>
                                                            <li class="bookmarkImg" title="পতাকা">
                                                                <i class="fa fa-bookmark-o bookmarkImgPotro"></i>
                                                            </li>
                                                        <?php endif; ?>
                                                    </ul>
                                                </nav>
                                            </div>
                                            <div class="col-lg-5 col-md-5 col-sm-5 text-right">
                                                <p> মোট: <b><?php echo $this->Number->format($total); ?>টি</b></p>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="portlet-body" style="overflow-y: auto;overflow-x:  hidden;">
                                        <div class="row divcenter">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="potroview">
                                                    <?php

                                                    if (!empty($potroAttachmentRecord)) {
                                                        $i = 0;
                                                        foreach ($potroAttachmentRecord as $row) {
                                                            echo '<div id_ar="' . $row['id'] . '" id_en="' . $row['nothi_potro_page'] . '"  id_bn="' . $row['nothi_potro_page_bn'] . '" data-content-type="' . $row['attachment_type'] . '" class="attachmentsRecord ' . ($i == 0 ? 'active first' : ($i == (count($potroAttachmentRecord) - 1) ? 'last' : '')) . '">';
                                                            ?>
                                                            <div style="background-color: #a94442; padding: 5px; color: #fff;">
                                                                <div class="pull-left"
                                                                     style="padding-top: 0px;    white-space: nowrap;">
                                                                    <?php if ($row['potrojari_data']['cloned_potrojari_id']): ?>
                                                                        <i class="fs1 a2i_gn_hardcopy3"
                                                                           title="এই পত্রটি ক্লোন করা হয়েছে"
                                                                           data-toggle="tooltip"></i>
                                                                    <?php endif; ?>
                                                                    <?php echo mb_substr(h($row['nothi_potro']['subject']), 0, 50) . (mb_strlen(h($row['nothi_potro']['subject'])) > 50 ? '...' : ''); ?>
                                                                    <?php echo(!empty($row['nothi_potro']['application_origin']) && $row['nothi_potro']['application_origin'] != 'nothi' ? (" (" . strtoupper($row['nothi_potro']['application_origin'])[0] . ")") : ''); ?>
                                                                    <?php if($row['is_main'] == 1) { echo "[মূল ডাক]"; } ?>
                                                                </div>
                                                                <div class="pull-right text-right">
                                                                    <?php if($row['is_main'] == 1): ?>
                                                                        <a title="ড্রাফট পরিবর্তন দেখুন"
                                                                           nothi_master_id="<?php echo $row['nothi_part_no'] ?>"
                                                                           potrojari="<?php echo $row['potrojari_id'] ?>"
                                                                           class="btn   btn-primary btn-xs btn-changelog">
                                                                            <i class="fs1 a2i_gn_history3"></i>
                                                                        </a>
                                                                    <?php endif; ?>
                                                                    <?php

                                                                    if ($row['attachment_type'] == 'text') {
                                                                        if ($privilige_type == 1 && !empty($row['nothi_potro']['application_origin']) && $row['nothi_potro']['application_origin'] != 'nothi') {
                                                                            $decision_meta = jsonA($row['nothi_potro']['application_meta_data']);
                                                                            $decision = !empty($decision_meta['decision']) ? ($decision_meta['decision']) : '{}';
//                                                                            $decision = !empty($decision_meta['decision']) ? (!is_string($decision_meta['decision'])?json_encode($decision_meta['decision']) :$decision_meta['decision']):'{}';
                                                                            $signdesk = !empty($decision_meta['sdesk']) ? ($decision_meta['sdesk']) : 0;
                                                                            $action_decisions = !empty($decision_meta['action_decisions']) ? $decision_meta['action_decisions'] : [];
                                                                            $decision_answer = isset($decision_meta['decision_answer']) ? $decision_meta['decision_answer'] : -1;
                                                                            $presentation_date = isset($decision_meta['presentation_date']) ? $decision_meta['presentation_date'] :'';
                                                                            $decision_note = isset($decision_meta['decision_note']) ? $decision_meta['decision_note'] : '';
                                                                            $decision_id = isset($decision_meta['decision_id']) ? $decision_meta['decision_id'] : -1;
                                                                            $same_desk = !empty($decision_meta['decision_desk']) && $employee_office['office_unit_organogram_id'] == $decision_meta['decision_desk'] ? 1 : 0;
                                                                            $is_locked = !empty($decision_meta['locked']) ? 1: 0;
                                                                            $decision_locked = $is_locked ? 1 : $same_desk;
                                                                            ?>
                                                                            <?php if(empty($decision_meta['other_party_certificate']) && $row['nothi_potro']['application_origin'] != 'ARGS'): ?>
                                                                            <button type="button" class="btn btn-info btn-xs showDecisionModal" data-presentation_date="<?= $presentation_date ?>" data-id="<?= $row['id'] ?>" data-potro-id='<?= $row['nothi_potro']['id'] ?>' data-decision='<?= h(json_encode($decision)) ?>' data-action-decisions="<?= !empty($action_decisions)?h(json_encode($action_decisions)):'{}' ?>" data-decision-id='<?= $decision_id ?>' data-comment='<?= h($decision_note) ?>' data-decision-answer='<?= $decision_answer ?>' data-locked='<?= $decision_locked ?>' data-nothi-office='<?=$nothi_office?>' data-part-id="<?=$id?>" data-potro-subject="<?= h($row['nothi_potro']['subject']) ?>">সিদ্ধান্ত</button>
                                                                                <?php
                                                                            elseif($row['nothi_potro']['application_origin'] != 'ARGS'):

                                                                                $file_name = explode("content/",$decision_meta['other_party_certificate']);
                                                                                if(!empty($file_name[1])){
                                                                                    $href = $decision_meta['other_party_certificate'].'?token=' . sGenerateToken(['file' => $file_name[1]], ['exp' => time() + 60 * 300]);
                                                                                }else{
                                                                                    $href =  $decision_meta['other_party_certificate'];
                                                                                }

                                                                                ?>
                                                                                <a target="__blank" class="btn btn-info btn-xs" href="<?= $href ?>">সার্টিফিকেট</a>
                                                                            <?php
                                                                            endif;
                                                                                ?>
                                                                            <?php
                                                                            if(($privilige_type == 1 && !$is_locked && !empty($action_decisions['editable']) && in_array($decision_answer,$action_decisions['editable'])) || $row['nothi_potro']['application_origin'] == 'ARGS'){
                                                                                ?>
                                                                                <a title="সম্পাদনা করুন" href="<?= $this->Url->build(['_name' => 'editPotro', $nothi_office,$row['id']]) ?>" class="btn blue btn-xs"><i class="fs1 fa fa-pencil"></i></a>
                                                                                <?php
                                                                            }
                                                                        }
                                                                        ?>
                                                                        <div class="btn-group ">
                                                                            <button type="button"
                                                                                    class="btn btn-success btn-xs dropdown-toggle"
                                                                                    data-toggle="dropdown"
                                                                                    aria-expanded="false">
                                                                                <i class="fa fa-ellipsis-horizontal"></i>
                                                                                প্রিন্ট <i class="fa fa-angle-down"></i>
                                                                            </button>
                                                                            <ul class="dropdown-menu pull-right">
                                                                                <li>
                                                                                    <a title="প্রিন্ট প্রিভিউ"
                                                                                       data-id="<?= $row['id'] ?>"
                                                                                       href="javascript:void(0)"
                                                                                       class="btn-print"><i
                                                                                                class="fs1 a2i_gn_print2"></i>
                                                                                        প্রিন্ট প্রিভিউ</a>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                        <?php
                                                                    }
                                                                    if ($row['attachment_type'] != 'text') {
                                                                        ?>
                                                                        <a title="ডাউনলোড করুন" href="<?=
                                                                        $this->Url->build(['controller' => 'NothiNoteSheets',
                                                                            'action' => 'downloadPotro',
                                                                            $nothi_office,
                                                                            $row['id']])
                                                                        ?>" class="btn   blue btn-xs"><i
                                                                                    class="fs1 fa fa-download"></i></a>
                                                                    <?php }
                                                                    if ($privilige_type == 1 && ($row['attachment_type'] == 'text' ||
                                                                            substr($row['attachment_type'], 0, 5) == 'image') || ($row['attachment_type'] == 'application/pdf' && PDF_TO_IMAGE)) {
                                                                        if (empty($row['potrojari_data']['attached_potro_id']) && $row['potrojari_id'] == 0) :
                                                                            ?>
                                                                            <a class="btn  btn-xs btn-danger btn-endorsment"
                                                                               url="<?php
                                                                               echo $this->Url->build(['controller' => 'potrojari',
                                                                                   'action' => 'makeDraft',
                                                                                   $row['nothi_part_no'],
                                                                                   $row['id'], 'potro', $row['id'], $row['potrojari_id']])
                                                                               ?>"
                                                                               title="পৃষ্ঠাঙ্কন করুন"
                                                                               data-title-orginal="পৃষ্ঠাঙ্কন করুন">
                                                                                <span class="fa fa-certificate"></span></a>

                                                                            <?php
                                                                        endif;
                                                                    }

                                                                    $class = "btn-danger";
                                                                    $classicon = "fa fa-exclamation-triangle";

                                                                    if ($row['attachment_type'] == 'text' && $privilige_type == 1 && !empty($row['potrojari_data'])) {

                                                                        $class = "btn-primary";
                                                                        $classicon = "fs1 a2i_gn_refresh1";
                                                                        if (empty($row['potrojari_data']['attached_potro_id']) && $otherNothi == false):
                                                                            ?>
                                                                            <a class="btn  btn-xs btn-warning"
                                                                               onclick="clonePotrojari('<?php echo $this->Url->build(['controller' => 'potrojari', 'action' => 'potrojariClone', $row['potrojari_id']]) ?>')"
                                                                               title="পত্রজারি ক্লোন করুন"
                                                                               data-title-orginal="পত্রজারি ক্লোন করুন">
                                                                                <span class="glyphicon glyphicon-copy"></span></a>
                                                                        <?php endif; ?>
                                                                        <a href="<?php
                                                                        echo $this->Url->build(['controller' => 'potrojari',
                                                                            'action' => 'pendingList',
                                                                            $nothi_office,
                                                                            $row['potrojari_id'], $row['nothi_part_no']])
                                                                        ?>" target="_tab"
                                                                           class="btn  btn-xs <?= $class ?>"
                                                                           title="পত্রজারি প্রেরণ অবস্থা দেখুন"
                                                                           data-title-orginal="পত্রজারি প্রেরণ অবস্থা দেখুন">
                                                                            <i class="<?= $classicon ?>"></i> </a>
                                                                        <a href="<?php
                                                                        echo $this->Url->build(['controller' => 'potrojari',
                                                                            'action' => 'potrojariDakTracking',
                                                                            $nothi_office,
                                                                            $row['potrojari_id'], $row['nothi_part_no']])
                                                                        ?>" target="_tab"
                                                                           class="btn  btn-xs green"
                                                                           title="জারিকৃত পত্রের অবস্থা"
                                                                           data-title-orginal="জারিকৃত পত্রের অবস্থা">
                                                                            <i class="fs1 a2i_nt_nothigotibidhi3"></i>
                                                                        </a>
                                                                        <?php
                                                                    }
                                                                    if ($privilige_type == 1 && $otherNothi
                                                                        == false
                                                                    ) {
                                                                        ?>
                                                                        <a url="<?php
                                                                        echo $this->Url->build(['controller' => 'potrojari',
                                                                            'action' => 'makeDraft',
                                                                            $row['nothi_part_no'],
                                                                            $row['id']])
                                                                        ?>?potro_new=yes" href="javascript: void(0);"
                                                                           class="btn   btn-xs green potroCreate"
                                                                           title="পত্রের খসড়া তৈরি করুন"
                                                                           data-title-orginal="পত্রের খসড়া তৈরি করুন">
                                                                            <i class="fs1 a2i_nt_dakdraft4"></i> </a>

                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </div>
                                                                <div style="clear: both;"></div>
                                                            </div>
                                                            <?php
                                                            if ($row['attachment_type'] == 'text') {

                                                                if ($row['is_summary_nothi'] == 1) {
                                                                    echo '<div style="background-color: #fff; max-width:950px; min-height:815px;  margin:0 auto; page-break-inside: auto;">' . html_entity_decode($row['potro_cover']) . "<br/>" . html_entity_decode($row['content_body']) . "</div>";
                                                                } else {
                                                                    $header = jsonA($row['meta_data']);
                                                                    echo '<div style="background-color: #fff; max-width:950px; min-height:815px;  margin:0 auto; page-break-inside: auto;">' . (!empty($header['potro_header_banner']) ? ('<div style="text-align: ' . ($header['banner_position']) . '!important;"><img src="' . $this->request->webroot . 'getContent?file=' . base64_decode($header['potro_header_banner']) . '&token=' . sGenerateToken(['file' => base64_decode($header['potro_header_banner'])], ['exp' => time() + 60 * 300]) . '" style="width:' . ($header['banner_width']) . ';height: 60px;max-width: 100%;"/></div>') : '') . html_entity_decode($row['content_body']) . "</div>";
                                                                    ?>
                                                                    <div class="table">
                                                                        <table role="presentation"
                                                                               class="table table-bordered table-striped">
                                                                            <tbody class="files">
                                                                            <?php
                                                                            $total_attachment = 0;

                                                                            if (!empty($potroAttachments[$row['potrojari_id']])) {
                                                                                echo '<tr class="text-center">
                                                                                <td colspan="2">' . __('Attachments') . '</td>
                                                                        </tr>';
                                                                                foreach ($potroAttachments[$row['potrojari_id']] as $ke => $single_data) {
                                                                                    if ($single_data['id'] == $row['id']) continue;

                                                                                    if (!(empty($single_data['file_name']))) {
                                                                                        $fileName = explode('/',
                                                                                            $single_data['file_name']);
                                                                                        $attachmentHeaders
                                                                                            = get_file_type($single_data['file_name']);
                                                                                        $value = array(
                                                                                            'name' => urldecode($fileName[count($fileName)
                                                                                            - 1]),
                                                                                            'thumbnailUrl' => $this->request->webroot . 'content/' . $single_data['file_name'],
                                                                                            'url' => $this->request->webroot . 'content/' . $single_data['file_name'],
                                                                                            'visibleName' => (!empty($single_data['user_file_name']) ? $single_data['user_file_name'] : urldecode($fileName[count($fileName) - 1])),
                                                                                        );
                                                                                    } else {
                                                                                        $value = array(
                                                                                            'name' => 'পত্রঃ ' . $single_data['sarok_no'],
                                                                                            'thumbnailUrl' => '',
                                                                                            'url' => '',
                                                                                            'visibleName' => '',
                                                                                        );
                                                                                    }
                                                                                    $total_attachment++;
                                                                                    ?>
                                                                                    <tr class="template-download fade in">
                                                                                        <td>
                                                                                            <p class="name">
                                                                                                <?php if (isset($value['name']) && (substr($single_data['attachment_type'], 0, 5) == 'image' || $single_data['attachment_type'] == 'application/pdf')): ?>
                                                                                                    <a href="<?= $single_data['id'] . '/potro/' . $row['potrojari_data']['nothi_master_id'] ?>"
                                                                                                       title="<?= $value['name'] ?>"
                                                                                                       class="showforPopup">
                                                                                                        <?= strlen($value['visibleName']) > 0 ? $value['visibleName'] : urldecode($value['name']) ?>
                                                                                                    </a>
                                                                                                <?php else: echo(isset($value['name']) ? urldecode($value['name']) : '') ?>
                                                                                                <?php endif; ?>
                                                                                            </p>
                                                                                        </td>
                                                                                        <td>
                                                                                            <?= (($single_data['attachment_type'] != 'text' && $single_data['attachment_type'] != 'text/html') ? $this->Html->link('<i class="fs1 fa fa-download"></i> &nbsp;', ['controller' => 'nothi_note_sheets', 'action' => 'downloadPotro',
                                                                                                $nothi_office,
                                                                                                $single_data['id']], ['escape' => false]) : '') ?>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <?php
                                                                                }
                                                                            }
                                                                            if ($total_attachment == 0) {
                                                                                echo '<tr class="template-download fade in text-center"><td colspan="2" class="danger"> ' . __('Sorry no attachment found') . '</td></tr>';
                                                                            }
                                                                            ?>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                    <?php
                                                                }
                                                            } elseif (substr($row['attachment_type'],
                                                                    0, 5) != 'image'
                                                            ) {
                                                                if (substr($row['attachment_type'],
                                                                        0, strlen('application/vnd'))
                                                                    == 'application/vnd' || substr($row['attachment_type'],
                                                                        0, strlen('application/ms'))
                                                                    == 'application/ms'
                                                                ) {
                                                                    $url = urlencode(str_replace('content/', '', FILE_FOLDER) . 'getContent?file=' . $row['file_name'] . '&token=' . sGenerateToken(['file' => $row['file_name']], ['exp' => time() + 60 * 300]));
                                                                    echo '<iframe style=" width:100%; height: 700px;" src="https://docs.google.com/gview?url=' . $url . '&embedded=true" style="width:600px; height:500px;" frameborder="0"></iframe>';
                                                                } else {
                                                                    echo '<embed src="' . str_replace('content/', '', FILE_FOLDER) . 'getContent?file=' . $row['file_name'] . '&token=' . sGenerateToken(['file' => $row['file_name']], ['exp' => time() + 60 * 300]) . '" style=" width:100%; height: 700px;" type="' . $row['attachment_type'] . '"></embed>';
                                                                }
                                                            } else {
                                                                echo '<div class="text-center"><a onclick="return false;" href="' . $this->request->webroot . 'content/' . $row['file_name'] . '?token=' . sGenerateToken(['file' => $row['file_name']], ['exp' => time() + 60 * 300]) . '" data-title="পত্র নম্বর:  ' . $row['nothi_potro_page_bn'] . '" data-footer="" ><img class="zoomimg img-responsive"  src="' . $this->request->webroot . 'content/' . $row['file_name'] . '?token=' . sGenerateToken(['file' => $row['file_name']], ['exp' => time() + 60 * 300]) . '" alt="ছবি পাওয়া যায়নি"></a></div>';
                                                            }
                                                            echo "</div>";
                                                            $i++;
                                                        }
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane " id="potroview_alllist" style="overflow-y: auto;overflow-x:  hidden;">

                            </div>

                            <?php // if (!empty($potroNothijatoAttachmentRecord)): ?>
                            <div class="tab-pane " id="nothijato_list">
                                <div class="tab-pane fade notloaded selectedpotrogroup"
                                     id="nothijatopotroview_accordian">
                                    <div class="portlet light ">
                                        <div class="portlet-body" style="min-height: 200px;">
                                            <div class="nothijatopotroview_div">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php // endif; ?>
                            <div class="tab-pane fade notloaded selectedpotrogroup" id="potroview_accordian">
                                <div class="portlet light ">
                                    <div class="portlet-body">
                                        <div class="row divcenter">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="potroview">
                                                    <div class="portlet-body">
                                                        <div class="panel-group accordion potroview_div">
                                                        </div>
                                                        <button class="btn   btn-sm purple btn-choose-potro"
                                                                style="display: none;"><span
                                                                    class="glyphicon glyphicon-new-window"></span>
                                                            একসাথে
                                                            দেখুন
                                                        </button>
                                                    </div>
                                                </div>
                                                <div class="selectedpotroview" style="display: none;">
                                                    <div class="portlet-body">
                                                        <button class="btn   btn-sm btn-primary back_btn-choose-potro">
                                                            <span class="glyphicon glyphicon-share-alt"></span> ফেরত যান
                                                        </button>
                                                        <br/>

                                                        <div class="panel-group accordion scrollable potroselected_view">
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<div id="responsiveChangeLog" class="modal fade modal-purple" tabindex="-1" aria-hidden="true" data-backdrop="static"
     data-keyboard="false">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">খসড়া পত্রের পরিবর্তনসমূহ</h4>
            </div>
            <div class="modal-body" style="background-color: #828282;">
                <div class="scroller" style="height:100%; max-height: 500px;" data-always-visible="1"
                     data-rail-visible1="1">

                </div>
            </div>
        </div>
    </div>
</div>

<div id="responsivepotalaLog" class="modal fade modal-purple" tabindex="-1" aria-hidden="true" data-backdrop="static"
     data-keyboard="false">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">সংলাগ</h4>
            </div>
            <div class="modal-body" style="background-color: #828282;">
                <div class="scroller" style="height:100%; max-height: 500px;" data-always-visible="1"
                     data-rail-visible1="1">

                </div>
            </div>
        </div>
    </div>
</div>


<div id="potrojariSendModal" class="modal fade modal-purple" tabindex="-1" aria-hidden="true" data-backdrop="static"
     data-keyboard="false">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">খসড়া পত্র পত্রজারি</h4>
            </div>
            <div class="modal-body">
                <div style="height:100%" data-always-visible="1" data-rail-visible1="1">
                    <div class="form-group">
                        <input type="hidden" id="potrojariSendId"/>
                        <input type="hidden" id="potrojariSendType"/>
                        <!--                        --><?php //if(!defined("POTROJARI_UPDATE") || POTROJARI_UPDATE == false): ?>
                        <div class="alert alert-danger"><i class="fa fa-info-circle"></i> পত্রজারি একটি অপরিবর্তনযোগ্য কার্যক্রম। অনুগ্রহ করে উল্লেখিত বিষয়গুলো যাচাই করুন
                            <span class="badge badge-primary" style="font-size: 100% !important;">পত্রজারি হেডার</span>
                            <span class="badge badge-primary" style="font-size: 100% !important;">পত্রের তারিখ</span>
                            <span class="badge badge-primary" style="font-size: 100% !important;">পত্রের বিষয়</span>
                            <span class="badge badge-primary" style="font-size: 100% !important;">পত্রের বিবরণ</span>
                            <span class="badge badge-primary" style="font-size: 100% !important;">পত্রের প্রেরক/অনুমোদনকারী/প্রাপক/অনুলিপি এর তথ্য</span>
                            <span class="badge badge-primary" style="font-size: 100% !important;">পত্রের স্বাক্ষর</span>
                        </div>
                        <!--                        --><?php //endif; ?>
                        <h4 style='color:red;text-align: center'>
                            বিঃদ্রঃ একই ব্যবহারকারী পত্রের খসড়া করে পত্রজারি করলে
                            <b> "পত্রজারিতে নিষ্পন্ন নোট"</b> গণনা করা হবে না।
                            <!--                            বিঃদ্রঃ নথি চালাচালি না করে পত্রজারি করলে সেই পত্রজারির জন্য
                                                        <b>"পত্রজারিতে নিষ্পন্ন নোট"</b> গণনা করা হবে না।-->
                        </h4>
                    </div>
                    <div class="dateChange well text-center" hidden>
                        <label class="bold font-lg">
                            খসড়া তৈরির তারিখ এবং স্বাক্ষর করার তারিখ এক নয় । খসড়ার তারিখ পরিবর্তন করে অনুমোদনকারী
                            স্বাক্ষরের তারিখ দিতে চাইলে
                            <button class="btn blue round-corner-5" id="potroKhosraEditUrl"
                                    onclick="getSignDateToPotroDate(this)">এখানে ক্লিক করুন
                            </button>
                        </label>
                        <!--<a href="#" class="btn blue" id="potroKhosraEditUrl">খসড়ার তারিখ পরিবর্তন করুন</a>-->
                    </div>
                    <div class='A4 form-group'></div>
                    <?php $style = '';
                    if (defined("POTROJARI_UPDATE") && POTROJARI_UPDATE == true) { ?>
                        <div class="row list-group-for-checkbox">
                            <h4 style='color:red' class="bold text-center">পত্রজারি একটি অপরিবর্তনযোগ্য কার্যক্রম।
                                অনুগ্রহ করে নিম্নোক্ত বিষয়গুলো যাচাই করুনঃ</h4>
                            <div class="col-md-6 col-sm-6">
                                <ul class="list-group">
                                    <li class="list-group-item"><input type="checkbox"
                                                                       class="btn-group-hide-for-checkbox" checked/>
                                        পত্রজারি
                                        হেডার চেক করা হয়েছে
                                    </li>
                                    <li class="list-group-item"><input type="checkbox"
                                                                       class="btn-group-hide-for-checkbox" checked/>
                                        পত্রের
                                        তারিখ চেক করা হয়েছে
                                    </li>
                                    <li class="list-group-item"><input type="checkbox"
                                                                       class="btn-group-hide-for-checkbox" checked/>
                                        পত্রের বিষয়
                                        চেক করা হয়েছে
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <ul class="list-group ">
                                    <li class="list-group-item"><input type="checkbox"
                                                                       class="btn-group-hide-for-checkbox" checked/>
                                        পত্রের
                                        বিবরণ চেক করা হয়েছে
                                    </li>
                                    <li class="list-group-item"><input type="checkbox"
                                                                       class="btn-group-hide-for-checkbox" checked/>
                                        পত্রের
                                        প্রেরক/অনুমোদনকারী/প্রাপক/অনুলিপি এর তথ্য চেক করা হয়েছে
                                    </li>
                                    <li class="list-group-item"><input type="checkbox"
                                                                       class="btn-group-hide-for-checkbox" checked/> <b>পত্রের
                                            স্বাক্ষর</b> চেক করা হয়েছে
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <?php
                        $style = 'display:none';
                    } ?>

                    <div class="modal_receiver_list"></div>

                    <div class="well text-center btn-group-hide-for-checking" style="<?= $style ?>">
                        <label class="bold font-lg">আপনি কি খসড়া পত্রজারি করতে চান?
                            <div class="btn-group btn-group-round">
                                <button type="button" class="btn green sendPotrojariApprove">হ্যাঁ</button>
                                <button type="button" data-dismiss="modal" class="btn btn-danger">না</button>
                            </div>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <div class="btn-group btn-group-round">
                    <button type="button" data-dismiss="modal" class="btn btn-danger" aria-hidden="true">বন্ধ করুন
                    </button>
                    <!--                <div class="row">-->
                    <!--                    <div class="col-sm-8 text-left">-->
                    <!--                        <p class="bold font-lg">আপনি কি খসড়া পত্রজারি করতে চান?</p>-->
                    <!--                    </div>-->
                    <!--                    <div class="col-sm-4">-->
                    <!--                        <button type="button" class="btn purple sendPotrojariApprove">হ্যাঁ</button>-->
                    <!--                        <button type="button" data-dismiss="modal" class="btn  btn-danger">না</button>-->
                    <!--                    </div>-->
                    <!--                </div>-->
                </div>
            </div>
        </div>
    </div>
</div>


<div id="modalSummaryNothi" class="modal fade modal-purple" tabindex="-1" aria-hidden="true" data-backdrop="static"
     data-keyboard="false">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">সার-সংক্ষেপ অনুমোদন</h4>
            </div>
            <div class="modal-body">
                <div class="panel panel-primary showUsers">
                    <div class="panel-body">

                    </div>
                    <div class="panel-footer form-actions">
                        <input type="button" data-id="" class="btn btn-primary btn-sm btn-updatesummary"
                               value="<?= __("Submit") ?>">
                    </div>
                </div>
                <div class="tabbable-custom nav-justified">
                    <ul class="nav nav-tabs nav-justified">
                        <li class="active">
                            <a href="#tab_cover2" data-toggle="tab">
                                কভার পাতা</a>
                        </li>
                        <li>
                            <a href="#tab_sar_body2" data-toggle="tab">
                                সার-সংক্ষেপ </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_cover2">
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="template-bodycover"
                                         style="background-color: #fff; max-width:950px; min-height:815px; height: auto; margin:0 auto;">

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_sar_body2">
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="template-bodybody"
                                         style="background-color: #fff; max-width:950px; min-height:815px; height: auto; margin:0 auto;">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="potroDecision" class="modal fade modal-purple" tabindex="-1" aria-hidden="true" data-backdrop="static"
     data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
                <div class="modal-title">পত্র সিদ্ধান্ত</div>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn purple decisionSave" data-nothi-office='<?=$nothi_office?>' data-part-id="<?=$id?>"><?= SAVE ?></button>
                <button type="button" data-dismiss="modal" class="btn  btn-danger">
                    বন্ধ করুন
                </button>
            </div>
        </div>
    </div>
</div>

<datalist id="selectReason">
    <option value="বিদেশ গমন করছেন">বিদেশ গমন করছেন</option>
    <option value="ছুটিতে আছেন">ছুটিতে আছেন</option>
</datalist>
<?= $this->element('preview_ele'); ?>
<?php echo $this->Html->script(((CDN == 1) ? CDN_PATH : '') . '/assets/global/scripts/printThis.js'); ?>

<script>

    $(document).on('click', '.btn-changelog', function () {
        $('#responsiveChangeLog').modal('show');
        $('#responsiveChangeLog').find('.scroller').html('<img src="' + js_wb_root + 'assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp;লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');

        var potrojari_id = $(this).attr('potrojari');
        $.ajax({
            url: "<?= $this->Url->build(['controller' => 'Potrojari', 'action' => 'showChangeLog']) ?>",
            data: {
                nothi_master_id: '<?php echo $id ?>',
                potrojari_id: potrojari_id,
                nothi_office:<?php echo $nothi_office ?>},
            method: 'post',
            dataType: 'html',
            cache: false,
            success: function (response) {
                $('#responsiveChangeLog').find('.scroller').html(response);
            },
            error: function (err, status, rspn) {
                $('#responsiveChangeLog').find('.scroller').html('');
            }
        });
    });
</script>
<script>

	$('.btn-group-hide-for-checkbox').on("change", function () {
		var checkboxlenth = $('.btn-group-hide-for-checkbox').length;
		var checkboxcheckedlenth = $('.btn-group-hide-for-checkbox:checked').length;

		if (checkboxcheckedlenth > 0 && checkboxlenth == checkboxcheckedlenth) {
			$('.btn-group-hide-for-checking').show()
			$('.list-group-for-checkbox').hide()
		} else {
			$('.btn-group-hide-for-checking').hide()
			$('.list-group-for-checkbox').show()
		}
	})

	var numberArray = {
		1: '১',
		2: '২',
		3: '৩',
		4: '৪',
		5: '৫',
		6: '৬',
		7: '৭',
		8: '৮',
		9: '৯',
		0: '০'
	};

	//summary nothi
	var summarydraftpotroPageCount = 1;
	$(document).off('click', '.summaryDraftimageNext');
	$(document).on('click', '.summaryDraftimageNext', function () {

		var firstndex = ($('.summaryDraftattachmentsRecord').first().index() == -1 ? 0 : $('.summaryDraftattachmentsRecord').first().index());
		var nowIndex = $('.summaryDraftattachmentsRecord.active').index();
		var lastIndex = $('.summaryDraftattachmentsRecord').last().index() == -1 ? 0 : $('.summaryDraftattachmentsRecord').last().index();

		if (nowIndex >= firstndex) {
			$('.summaryDraftimagePrev').removeClass('disabled');
		}

		if (lastIndex == 0 || nowIndex >= (<?php
                echo !empty($summaryAttachmentRecord) ? (count($summaryAttachmentRecord) - 1) : 0
                ?>)) {
			$(this).addClass('disabled');
			return;
		}

		if ($('.summaryDraftattachmentsRecord').eq(nowIndex).hasClass('last') == false) {
			nowIndex++;
			$('.summaryDraftattachmentsRecord').removeClass('active');
			$('.summaryDraftattachmentsRecord').eq(nowIndex).addClass('active');
		}
		heightlimit();
	});

	$(document).on('click', '.summaryDraftimagePrev', function () {
		var firstndex = ($('.summaryDraftattachmentsRecord').first().index() == -1 ? 0 : $('.summaryDraftattachmentsRecord').first().index());
		var nowIndex = $('.summaryDraftattachmentsRecord.active').index();
		var lastIndex = $('.summaryDraftattachmentsRecord').last().index() == -1 ? 0 : $('.summaryDraftattachmentsRecord').last().index();

		if (nowIndex < lastIndex) {
			$('.summaryDraftimageNext').removeClass('disabled');
		}

		if (nowIndex <= 0) {
			$('.summaryDraftimageNext').removeClass('disabled');
			$(this).addClass('disabled');
			return;
		}


		if ($('.summaryDraftattachmentsRecord').eq(nowIndex).hasClass('first') == false) {
			$('.summaryDraftattachmentsRecord').removeClass('active');
			$('.summaryDraftattachmentsRecord').eq(nowIndex - 1).addClass('active');
		} else {
			$('.summaryDraftimagePrev').addClass('disabled');
			return;
		}
		heightlimit();

	});
	//draft
	var draftpotroPageCount = 1;
	$(document).off('click', '.DraftimageNext');
	$(document).on('click', '.DraftimageNext', function () {

		var firstndex = ($('.DraftattachmentsRecord').first().index() == -1 ? 0 : $('.DraftattachmentsRecord').first().index());
		var nowIndex = $('.DraftattachmentsRecord.active').index();
		var lastIndex = $('.DraftattachmentsRecord').last().index() == -1 ? 0 : $('.DraftattachmentsRecord').last().index();

		if (nowIndex >= firstndex) {
			$('.DraftimagePrev').removeClass('disabled');
		}

		if ($('.DraftattachmentsRecord').eq(nowIndex).hasClass('last') == false) {
			nowIndex++;
			$('.DraftattachmentsRecord').removeClass('active');
			$('.DraftattachmentsRecord').eq(nowIndex).addClass('active');
		}

        if (nowIndex == (lastIndex-2)) {
		    var page = Math.ceil(lastIndex/5) + 1;
            loadNextPotroPage(page);
        }
        if (lastIndex == 0 || nowIndex >= $('.DraftattachmentsRecord').length - 1) {
            $(this).addClass('disabled');
            return;
        }
		heightlimit();
	});
	function loadNextPotroPage(page) {
		$.ajax({
            url: js_wb_root + 'nothiMasters/loadNextPotroPage/<?=$id?>/<?=$nothi_office?>?page=' + page,
            cache: false,
            type: 'post',
            async: true,
            success: function (result) {
                $('.DraftattachmentsRecord').parent().append(result);

                setTimeout(function() {
                    $('.DraftattachmentsRecord').removeClass('last');
                    $('.DraftattachmentsRecord:last-child').addClass('last')
                }, 200);
            }
		});
	}

	$(document).on('click', '.DraftimagePrev', function () {
		var firstndex = ($('.DraftattachmentsRecord').first().index() == -1 ? 0 : $('.DraftattachmentsRecord').first().index());
		var nowIndex = $('.DraftattachmentsRecord.active').index();
		var lastIndex = $('.DraftattachmentsRecord').last().index() == -1 ? 0 : $('.DraftattachmentsRecord').last().index();

		if (nowIndex < lastIndex) {
			$('.DraftimageNext').removeClass('disabled');
		}

		if (nowIndex <= 0) {
			$('.DraftimageNext').removeClass('disabled');
			$(this).addClass('disabled');
			return;
		}

		if ($('.DraftattachmentsRecord').eq(nowIndex).hasClass('first') == false) {
			$('.DraftattachmentsRecord').removeClass('active');
			$('.DraftattachmentsRecord').eq(nowIndex - 1).addClass('active');
		} else {
			$('.DraftimagePrev').addClass('disabled');
			return;
		}
		heightlimit();

	});

	//nothijato
	var draftpotroPageCount = 1;
	$(document).off('click', '.NothijatoimageNext');
	$(document).on('click', '.NothijatoimageNext', function () {

		var firstndex = ($('.NothijatoattachmentsRecord').first().index() == -1 ? 0 : $('.NothijatoattachmentsRecord').first().index());
		var nowIndex = $('.NothijatoattachmentsRecord.active').index();
		var lastIndex = $('.NothijatoattachmentsRecord').last().index() == -1 ? 0 : $('.NothijatoattachmentsRecord').last().index();

		if (nowIndex >= firstndex) {
			$('.NothijatoimagePrev').removeClass('disabled');
		}

		if (lastIndex == 0 || nowIndex >= (<?php
                echo ($potroNothijatoAttachmentRecord) - 1
                ?>)) {
			$(this).addClass('disabled');
			return;
		}

		if ($('.NothijatoattachmentsRecord').eq(nowIndex).hasClass('last') == false) {
			nowIndex++;
			$('.NothijatoattachmentsRecord').removeClass('active');
			$('.NothijatoattachmentsRecord').eq(nowIndex).addClass('active');
		}
		heightlimit();
	});

	$(document).on('click', '.NothijatoimagePrev', function () {
		var firstndex = ($('.NothijatoattachmentsRecord').first().index() == -1 ? 0 : $('.NothijatoattachmentsRecord').first().index());
		var nowIndex = $('.NothijatoattachmentsRecord.active').index();
		var lastIndex = $('.NothijatoattachmentsRecord').last().index() == -1 ? 0 : $('.NothijatoattachmentsRecord').last().index();

		if (nowIndex < lastIndex) {
			$('.NothijatoimageNext').removeClass('disabled');
		}

		if (nowIndex <= 0) {
			$('.NothijatoimageNext').removeClass('disabled');
			$(this).addClass('disabled');
			return;
		}

		if ($('.NothijatoattachmentsRecord').eq(nowIndex).hasClass('first') == false) {
			$('.NothijatoattachmentsRecord').removeClass('active');
			$('.NothijatoattachmentsRecord').eq(nowIndex - 1).addClass('active');
		} else {
			$('.NothijatoimagePrev').addClass('disabled');
			return;
		}
		heightlimit();

	});
	//potro

	var potroallPageCount = 1;
	$(document).off('click', '.imageallNext').on('click', '.imageallNext', function () {

		var firstndex = ($('.attachmentsallRecord').first().index() == -1 ? 0 : $('.attachmentsallRecord').first().index());
		var nowIndex = $('.attachmentsallRecord.active').index();
		var lastIndex = $('.attachmentsallRecord').last().index() == -1 ? 0 : $('.attachmentsallRecord').last().index();

		if (nowIndex >= firstndex) {
			$('.imageallPrev').removeClass('disabled');
		}

		if (lastIndex == 0 || nowIndex >= (<?php echo $potroAllAttachmentRecord - 1 ?>)) {
			$(this).addClass('disabled');
			return;
		}

		if ($('.attachmentsallRecord').eq(nowIndex).hasClass('last') == false) {
			nowIndex++;
			$('.attachmentsallRecord').removeClass('active');
			$('.attachmentsallRecord').eq(nowIndex).addClass('active');
			var potroPage = $('.attachmentsallRecord').eq(nowIndex).attr('id_bn');
			$('.potroallNo').val(potroPage);
		}
		heightlimit();
	});

	$(document).off("click", '.imageallPrev').on('click', '.imageallPrev', function () {
		var firstndex = ($('.attachmentsallRecord').first().index() == -1 ? 0 : $('.attachmentsallRecord').first().index());
		var nowIndex = $('.attachmentsallRecord.active').index();
		var lastIndex = $('.attachmentsallRecord').last().index() == -1 ? 0 : $('.attachmentsallRecord').last().index();

		if (nowIndex < lastIndex) {
			$('.imageallNext').removeClass('disabled');
		}

		if (nowIndex <= 0) {
			$('.imageallNext').removeClass('disabled');
			$(this).addClass('disabled');
			return;
		}


		if ($('.attachmentsallRecord').eq(nowIndex).hasClass('first') == false) {
			$('.attachmentsallRecord').removeClass('active');
			$('.attachmentsallRecord').eq(nowIndex - 1).addClass('active');
			var potroPage = $('.attachmentsallRecord').eq(nowIndex - 1).attr('id_bn');
			$('.potroallNo').val(potroPage);

		} else {
			$('.imageallPrev').addClass('disabled');
			return;
		}
		heightlimit();

	});

	var potroPageCount = 1;
	$(document).off('click', '.imageNext').on('click', '.imageNext', function () {

		var firstndex = ($('.attachmentsRecord').first().index() == -1 ? 0 : $('.attachmentsRecord').first().index());
		var nowIndex = $('.attachmentsRecord.active').index();
		var lastIndex = $('.attachmentsRecord').last().index() == -1 ? 0 : $('.attachmentsRecord').last().index();

		if (nowIndex >= firstndex) {
			$('.imagePrev').removeClass('disabled');
		}

		if (lastIndex == 0 || nowIndex >= (<?php echo $total - 1 ?>)) {
			$(this).addClass('disabled');
			return;
		}

		if ($('.attachmentsRecord').eq(nowIndex).hasClass('last') == false) {
			nowIndex++;
			$('.attachmentsRecord').removeClass('active');
			$('.attachmentsRecord').eq(nowIndex).addClass('active');
			var potroPage = $('.attachmentsRecord').eq(nowIndex).attr('id_bn');
			getpotropage(potroPage);
		} else {
			$('.attachmentsRecord').removeClass('active');
			$('.attachmentsRecord').removeClass('last');
			potroPageCount++;
			$.ajax({
				url: '<?php echo $this->request->webroot ?>nothiMasters/nextPotroPage/<?php echo $id . '/'; ?>' + potroPageCount,
				dataType: 'JSON', type: 'post',
				data: {nothi_office:<?php echo $nothi_office ?>},
				success: function (response) {
					heightlimit();
					if (response.html == '') {
						$('.imageNext').addClass('disabled');
					} else {
						$('#potroview_list .potroview').append(response.html);
						getpotropage(response.potrono);
						$('.imageNext').removeClass('disabled');
					}

				}

			});
		}
	});

	$(document).off("click", '.imagePrev').on('click', '.imagePrev', function () {
		var firstndex = ($('.attachmentsRecord').first().index() == -1 ? 0 : $('.attachmentsRecord').first().index());
		var nowIndex = $('.attachmentsRecord.active').index();
		var lastIndex = $('.attachmentsRecord').last().index() == -1 ? 0 : $('.attachmentsRecord').last().index();

		if (nowIndex < lastIndex) {
			$('.imageNext').removeClass('disabled');
		}

		if (nowIndex <= 0) {
			$('.imageNext').removeClass('disabled');
			$(this).addClass('disabled');
			return;
		}


		if ($('.attachmentsRecord').eq(nowIndex).hasClass('first') == false) {
			$('.attachmentsRecord').removeClass('active');
			$('.attachmentsRecord').eq(nowIndex - 1).addClass('active');
			var potroPage = $('.attachmentsRecord').eq(nowIndex - 1).attr('id_bn');
			getpotropage(potroPage);

		} else {
			$('.imagePrev').addClass('disabled');
			return;
		}
		heightlimit();

	});

	$(document).on('keypress', '.potroNo', function (key) {
		if (key.keyCode == 13) {
			goToPage($(this).val());
		}
	});
	$(document).on('keypress', '.potroallNo', function (key) {
		if (key.keyCode == 13) {
			goToallPage($(this).val());
		}
	});

	$(document).on('click', '.potroview_tab li', function () {

		var thisIndex = $(this).index();
		var isNotLoaded = $('.potroDetailsPage .tab-pane').eq(thisIndex).hasClass('notloaded');

		if (isNotLoaded) {
			var idIs = $('.potroDetailsPage .tab-pane').eq(thisIndex).attr('id');


			if (idIs == 'potroview_list') {
				$('#potroview_list .potroview').html('<img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp;লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');
				goToPage();
			} else {
				$('#potroview_accordian .potroview .potroview_div').html('<img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp;লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');
				potroAccordianView();
				$('.potroDetailsPage .tab-pane').eq(thisIndex).removeClass('notloaded');
			}
		}

	});

	$(document).on('click', '.potroview_div a.collapsed', function () {

		var hasPreviousExpand = $(this).hasClass('expand-already');

		if (hasPreviousExpand == false) {
			potroforAccordian($(this).attr('id'), $(this).attr('src'));
			$(this).addClass('expand-already');
		}
	});
	$(document).on('click', '.nothijatopotroview_div a.collapsed', function () {

		var hasPreviousExpand = $(this).hasClass('expand-already');

		if (hasPreviousExpand == false) {
			nothijatoPotroforAccordian($(this).attr('id'), $(this).attr('src'));
			$(this).addClass('expand-already');
		}
	});

	$(document).on('click', '.potro_choose', function () {
		if ($('.potro_choose:checked').length > 0) {
			$('.btn-choose-potro').show();
		} else {
			$('.btn-choose-potro').hide();
		}
	});

	$(document).on('click', '.btn-choose-potro', function () {
		var hasChoosen = $('.potro_choose:checked').length;

		if (hasChoosen != 0) {
			var selected = "<div class='row' id='sortable_portlets'>";

			$.each($('.potro_choose:checked'), function (index) {
				var datahtml = $(this).next('.img').length == 1 ? $(this).next('.img').html() : ($(this).next('iframe').length == 1 ? $(this).next('iframe').html() : ($(this).next('embed').length == 1 ? $(this).next('embed').html() : $(this).next('div').html()))
				selected += "<div class='col-md-6  column sortable'><div class='portlet portlet-sortable box green-haze'><div class='portlet-title'><div class='caption'>" + $(this).attr('potro_page_no') + "</div></div><div class='portlet-body'>" + datahtml + '</div></div><div class="portlet portlet-sortable-empty"></div></div>';
			});
			selected += "</div>";

			gotoSelectedPotors(selected);
		}
	});

	$(document).on('click', '.potroAccordianPaginator .nextImageAccordian', function () {

		var accordianId = $(this).parents('ul').attr('id');

		var firstndex = $('.potroAccordianPaginationBody.potro_details_accordian_div_' + accordianId).first().index();
		var nowIndex = $('.potroAccordianPaginationBody.potro_details_accordian_div_' + accordianId + '.active').index();
		var lastIndex = $('.potroAccordianPaginationBody.potro_details_accordian_div_' + accordianId).last().index();

		if (nowIndex < lastIndex) {
			nowIndex++;
			$('.potroAccordianPaginator .prevImageAccordian').removeAttr('disabled');
		} else {
			$(this).attr('disabled', 'disabled');
		}

		$('.potroAccordianPaginationBody.potro_details_accordian_div_' + accordianId).removeClass('active');
		$('.potroAccordianPaginationBody.potro_details_accordian_div_' + accordianId).eq(nowIndex).addClass('active');
	});

	$(document).on('click', '.potroAccordianPaginator .prevImageAccordian', function () {

		var accordianId = $(this).parents('ul').attr('id');

		var firstndex = $('.potroAccordianPaginationBody.potro_details_accordian_div_' + accordianId).first().index();
		var nowIndex = $('.potroAccordianPaginationBody.potro_details_accordian_div_' + accordianId + '.active').index();
		var lastIndex = $('.potroAccordianPaginationBody.potro_details_accordian_div_' + accordianId).last().index();

		if (nowIndex > 0) {
			$('.potroAccordianPaginator .nextImageAccordian').removeAttr('disabled');
			nowIndex--;
		} else {
			$(this).attr('disabled', 'disabled');
		}
		$('.potroAccordianPaginationBody.potro_details_accordian_div_' + accordianId).removeClass('active');
		$('.potroAccordianPaginationBody.potro_details_accordian_div_' + accordianId).eq(nowIndex).addClass('active');
	});

	$(document).on('click', '.back_btn-choose-potro', function () {
		$('#potroview_accordian .selectedpotroview .potroselected_view').html('');
		$('.potroview').show();
		$('.selectedpotroview').hide();
	});

	$(document).off('change', '.approveDraftNothi').on('change', '.approveDraftNothi', function () {
		var thisObj = $(this);
		var potrojari = thisObj.attr('potrojari_id');
        var receiver_list = $('.DraftattachmentsRecord.active').find('.potro_receiver_list').html();
        var footer_message = "আপনি কি খসড়া পত্রটি " + ($(".approveDraftNothi").is(':checked') == true ? "অনুমোদন" : "অনুমোদন বাতিল") + " করতে ইচ্ছুক?";
		var bootboxElement = bootbox.dialog({
			message: receiver_list,
			//message: "আপনি কি খসড়া পত্রটি " + ($(".approveDraftNothi").is(':checked') == true ? "অনুমোদন" : "অনুমোদন বাতিল") + " করতে ইচ্ছুক?",
			title: "খসড়া পত্র অনুমোদন",
			buttons: {
                footerLabel:{
                    label: footer_message,
                    className: "btn-default footer_label"
                },
				success: {
					label: "হ্যাঁ",
					className: "green",
					callback: function () {
						if ($(".approveDraftNothi").is(':checked') == true && signature > 0) {
							getSignatureToken('potroApproval', thisObj, potrojari);
						} else {
							potroApproval(thisObj, potrojari);
						}
					}
				},
				danger: {
					label: "না",
					className: "red",
					callback: function () {
						if ($(".approveDraftNothi").is(':checked') == false) {
							$(".approveDraftNothi").attr('checked', 'checked');
							$(".approveDraftNothi").closest('span').addClass('checked');
						}
						else {
							$(".approveDraftNothi").removeAttr('checked');
							$(".approveDraftNothi").closest('span').removeClass('checked');
						}
					}
				}
			}
		});
        bootboxElement.find('.modal-dialog').addClass('modal-full');
        bootboxElement.find(".footer_label").prop('disabled', true);
	});

	function potroApproval(thisObj, potrojari) {
		var data2send = {potrojari_id: potrojari, status: thisObj.is(':checked') ? 1 : 0};
		if (signature > 0) {
			data2send.soft_token = $("#soft_token").val();
		}
		PROJAPOTI.ajaxSubmitDataCallback(((!isEmpty(signature) && signature > 0)?ds_url:js_wb_root)+'Potrojari/ajaxApprovePotrojari/<?=$nothi_office
            ?>', data2send, 'json', function (response) {
			if (response.status == 'success') {
				potroPageShow('<?php echo $this->request->webroot; ?>nothiMasters/potroPage/<?php echo $id ?>/<?php echo $nothi_office ?>');
			} else if (response.status == 'error') {
				if (!isEmpty(response.msg)) {
					toastr.error(response.msg);
				} else {
					toastr.error('অনুরোধ সম্পন্ন করা সম্পন্ন হচ্ছে না।');
					if ($(".approveDraftNothi").is(':checked') == false) {
						$(".approveDraftNothi").attr('checked', 'checked');
						$(".approveDraftNothi").closest('span').addClass('checked');
					}
					else {
						$(".approveDraftNothi").removeAttr('checked');
						$(".approveDraftNothi").closest('span').removeClass('checked');
					}
				}
			}
		});
	}

	function potroShow(href) {
		$('.potroview').html('<img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp;লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');
		$.ajax({
			url: href,
			success: function (response) {
				$('.potroview').html(response);
				//Metronic.initSlimScroll('.scroller');

				if (typeof (pageno) != 'undefined') {
					goToPage(pageno);
					getpotropage(pageno);
				}

				$('[data-title-orginal]').tooltip();

				$('li').tooltip();

			}
		})

		$('[data-title-orginal]').tooltip();

		$('li').tooltip();

	}

	function checkBookmark() {
		var isBooked = false;
		$.each($('.potroFlagButton'), function (i, v) {
			if (
				$('.attachmentsRecord.active').attr('id_en') == $(this).attr('btn_potro_no') ||
				$('.attachmentsRecord.active').attr('id_bn') == $(this).attr('btn_potro_no')
				|| $('.attachmentsallRecord.active').attr('id_en') == $(this).attr('btn_potro_no') ||
				$('.attachmentsallRecord.active').attr('id_bn') == $(this).attr('btn_potro_no')) {
				isBooked = true;
			}
		});

		if (isBooked) {
			$('.bookmarkallImgPotro').removeClass('fa-bookmark-o').addClass('fa-bookmark');
			$('.bookmarkImgPotro').removeClass('fa-bookmark-o').addClass('fa-bookmark');
		} else {
			$('.bookmarkallImgPotro').addClass('fa-bookmark-o').removeClass('fa-bookmark');
			$('.bookmarkImgPotro').addClass('fa-bookmark-o').removeClass('fa-bookmark');
		}
	}

	function getpotropage(potroPage) {
		$('.potroNo').val(potroPage);
		heightlimit();
//        checkBookmark();
	}

	function goToPage(pageNo) {
		var hasDivbn = $('.attachmentsRecord ').index($('.attachmentsRecord[id_bn=' + pageNo + ']'));
		var hasDiv = $('.attachmentsRecord ').index($('.attachmentsRecord[id_en=' + pageNo + ']'));

		if (hasDivbn != -1 || hasDiv != -1) {
			$('.attachmentsRecord').removeClass('active');
			if (hasDivbn != -1) {
				$('.attachmentsRecord').eq(hasDivbn).addClass('active');
			} else if (hasDiv != -1) {
				$('.attachmentsRecord').eq(hasDiv).addClass('active');
			}
		} else {
			$('.attachmentsRecord').removeClass('active');
			$('.attachmentsRecord').removeClass('last');
			$('.imagePrev').removeClass('disabled');
			$('.imageNext').removeClass('disabled');
			if (typeof (pageNo) == 'undefined')
				pageNo = 0;
			$.ajax({
				url: '<?php echo $this->request->webroot ?>nothiMasters/nextPotroPage/<?php echo $id . '/1/'; ?>' + pageNo,
				dataType: 'JSON', type: 'post',
				data: {nothi_office:<?php echo $nothi_office; ?>},
				success: function (response) {

					if (response.html == '') {

					} else {
						$('#potroview_list .potroview').append(response.html);
						getpotropage(response.potrono);
					}
					$('.attachmentsRecord').eq(0).addClass('first');
//                                    checkBookmark();
					$('[data-title-orginal]').tooltip();

					$('li').tooltip();
					heightlimit();
				}
			});
		}
//                        checkBookmark();
	}

	function goToallPage(pageNo) {
		var hasDivbn = $('.attachmentsallRecord').index($('.attachmentsallRecord[id_bn=' + pageNo + ']'));
		var hasDiv = $('.attachmentsallRecord').index($('.attachmentsallRecord[id_en=' + pageNo + ']'));
		$('.attachmentsallRecord').removeClass('active');
		if (hasDivbn != -1) {
			$('.attachmentsallRecord').eq(hasDivbn).addClass('active');
		} else if (hasDiv != -1) {
			$('.attachmentsallRecord').eq(hasDiv).addClass('active');
		}
	}

	$('.potroSearchButton').click(function () {
		$('.potroview_tab li').removeClass('active');
		$('.loadsokolpotro').removeClass('loaded').addClass('active');
		$('.loadsokolpotro a').text('অনুসন্ধানকৃত পত্র');
		$('.tab-pane').removeClass('active');
		$('#potroview_alllist').addClass('active');
		setTimeout(function () {
			$('.loadsokolpotro').trigger('click');
		}, 500);
	})
	$('.potroSearchResetButton').click(function () {
		$('.potroSearch').val('');
		$('.potroview_tab li').removeClass('active');
		$('.loadsokolpotro').removeClass('loaded').addClass('active');
		$('.tab-pane').removeClass('active');
		$('#potroview_alllist').addClass('active');
		setTimeout(function () {
			$('.loadsokolpotro a').text('সকল পত্র');
			$('.loadsokolpotro').trigger('click');
		}, 500);
	})
	$('.loadsokolpotro').click(function () {
		if ($(this).hasClass('loaded') == false) {
			Metronic.blockUI({
				target: '.loadsokolpotro',
				boxed: true,
				message: 'অপেক্ষা করুন'
			});
			$.ajax({
				url: '<?php
                    echo $this->Url->build(['controller' => 'NothiMasters',
                        'action' => 'loadSokolPotro', $nothimastersid, $nothi_office])
                    ?>'+permitted_by,
				method: 'post',
				data: {q: $('.potroSearch').val()},
				cache: false,
				dataType: 'html',
				success: function (response) {
					$('.loadsokolpotro').addClass('loaded');
					$('#potroview_alllist').html(response);
					Metronic.unblockUI('.loadsokolpotro');
					heightlimit();
				},
				error: function (xhr, status, errorThrown) {
					$('.loadsokolpotro').removeClass('loaded');
					Metronic.unblockUI('.loadsokolpotro');
				}
			});
		}
	});

	$('.sokolnothijatopotro').click(function () {
		if ($(this).hasClass('loaded') == false) {
			Metronic.blockUI({
				target: '#nothijato_list',
				boxed: true,
				message: 'অপেক্ষা করুন'
			});
			$.ajax({
				url: '<?php
                    echo $this->Url->build(['controller' => 'NothiMasters',
                        'action' => 'loadNothijatoPotrowithPagination', $nothimastersid, $nothi_office, $id])
                    ?>'+permitted_by,
				method: 'post',
				cache: true,
				dataType: 'html',
				success: function (response) {
					$('.sokolnothijatopotro').addClass('loaded');
					$('#nothijatopotroview_accordian').removeClass('fade');
//                    $('#nothijato_list').html(response);
					$('.nothijatopotroview_div').html(response);
					Metronic.unblockUI('#nothijato_list');
					heightlimit();
				},
				error: function (xhr, status, errorThrown) {
					$('.sokolnothijatopotro').removeClass('loaded');
					Metronic.unblockUI('#nothijato_list');
				}
			});
		}
	});

	function potroAccordianView() {

		$.ajax({
			url: '<?php echo $this->request->webroot ?>nothiMasters/potroList/<?php echo $id . '/' . $nothi_office; ?>'+permitted_by,
			dataType: 'JSON',
			success: function (response) {
				$('#potroview_accordian .potroview .potroview_div').html(response.html);

				Metronic.initSlimScroll('.scroller');
				$('[data-title-orginal]').tooltip();

				$('li').tooltip();
			}
		});

	}

	function potroforAccordian(id, src) {

		if (id != 0 || id != '') {

			$(' #' + src + ' .panel-body').html('<img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp;লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');
			$.ajax({
				url: '<?php echo $this->request->webroot ?>nothiMasters/attachmentsForPotro/<?php echo $id; ?>/' + id + '/<?php echo $nothi_office; ?>'+permitted_by,
				dataType: 'JSON',
				success: function (response) {
					$('#' + src + ' .panel-body').html(response.html);
					if (response.html == '') {

					}
					$('[data-title-orginal]').tooltip();

					$('li').tooltip();
				}
			});
		}
	}

	function nothijatoPotroforAccordian(id, src) {

		if (id != 0 || id != '') {

			$(' #' + src + ' .panel-body').html('<img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp;লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');
			$.ajax({
				url: '<?php echo $this->request->webroot ?>nothiMasters/attachmentsForPotro/<?php echo 0; ?>/' + id + '/<?php echo $nothi_office . '/' . $nothimastersid; ?>'+permitted_by,
				dataType: 'JSON',
				success: function (response) {
					$('#' + src + ' .panel-body').html(response.html);
					if (response.html == '') {

					}
					$('[data-title-orginal]').tooltip();

					$('li').tooltip();
				}
			});
		}
	}

	function gotoSelectedPotors(selected) {

		if (selected.length != 0) {

			$('.selectedpotrogroup .potroview').hide();
			$('.selectedpotroview').show();

			$('#potroview_accordian .selectedpotroview .potroselected_view').html('<img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp;লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');

			$('#potroview_accordian .selectedpotroview .potroselected_view').html(selected);
		}
	}

    <?php if (!empty($draftPotro)): ?>
	$(document).off('click', '.btn-send-draft');
	$(document).on('click', '.btn-send-draft', function () {
		var serialPermission = 0;
		var potrojari = $(this).attr('potrojari');
		var nothinote = $(this).attr('nothinotesid') == 0 ? 'potro' : 'note';
		bootbox.dialog({
			message: "আপনি কি খসড়া পত্র পত্রজারি করতে চান?",
			title: "খসড়া পত্র পত্রজারি",
			buttons: {
				success: {
					label: "হ্যাঁ",
					className: "green",
					callback: function () {
						Metronic.blockUI({
							target: '.page-container',
							boxed: true
						});

						$.ajax({
							url: '<?php
                                echo $this->Url->build(['controller' => 'Potrojari',
                                    'action' => 'sendDraftPotrojari', $id])
                                ?>/' + potrojari + '/' + nothinote + permitted_by,
							method: 'post',
							cache: false,
							dataType: 'JSON',
							success: function (response) {

								if (response.status == 'error') {
									toastr.error(response.msg);
									Metronic.unblockUI('.page-container');
									;
								} else {
									toastr.success(response.msg);
									setTimeout(function () {
										window.location.reload();
									}, 2000);
								}
							},
							error: function (xhr, status, errorThrown) {
								toastr.options = {
									"closeButton": true,
									"debug": false,
									"positionClass": "toast-bottom-right"
								};
								toastr.error("দুঃখিত! ইন্টারনেট সংযোগ বিঘ্নিত হয়েছে। কিছুক্ধষণ পর আবার চেষ্টা করুন। ধন্যবাদ");
								Metronic.unblockUI('.page-container');
								;
							}
						});
					}
				},
				danger: {
					label: "না",
					className: "red",
					callback: function () {
					}
				}
			}
		});

	});

	$(document).off('click', '.btn-send-draft-cron');
	$(document).on('click', '.btn-send-draft-cron', function () {
        <?php if(defined("POTROJARI_UPDATE") && POTROJARI_UPDATE == true) { ?>
//		$('.list-group-for-checkbox input').removeAttr('checked')
//		$('.list-group-for-checkbox .checked').removeClass('checked')
		$('.btn-group-hide-for-checking').show()
		$('.list-group-for-checkbox').hide()
        <?php } ?>
		var potrojari = $(this).attr('potrojari');
		var part_no = $(this).attr('part_no');
		var nothi_office = $(this).attr('nothi_office');
		var nothinote = $(this).attr('nothinotesid') == 0 ? 'potro' : 'note';
		var elem = $(this);
		var potro_type = $('.DraftattachmentsRecord.active').find(' #template-body').attr('potro_type');
		if (!isEmpty(potro_type) && potro_type == 30) {
			var template_body = $('.DraftattachmentsRecord.active').find('.csCover').html();
		} else {
			var template_body = $('.DraftattachmentsRecord.active').find('#template-body').html();
		}

		var potrojari_language = $('.DraftattachmentsRecord.active').find(' #template-body').attr('potrojari_language');
		var receiver_list = $('.DraftattachmentsRecord.active').find('.potro_receiver_list').html();
		$(".modal_receiver_list").html(receiver_list);

		$('#potrojariSendId').val(potrojari)
		$('#potrojariSendType').val(nothinote)

		$('#potrojariSendModal').modal('show');
		$('#potrojariSendModal').find('.A4').html(template_body);
		checkKhosraAndSignatureDate(potrojari_language, '<?php echo $this->request->webroot ?>potrojari/customPotroEdit/' + part_no + '/' + potrojari + '/' + nothinote + '/' + nothi_office, potro_type);
	});

	$(document).off('click', '.sendPotrojariApprove');
	$(document).on('click', '.sendPotrojariApprove', function () {
		var elem = $(this).closest('.A4');
		var that = this;
		if (signature > 0) {
			getSignatureToken('makePotrojariRequest', elem, that);
		} else {
			makePotrojariRequest(elem, that)
		}
	});

	function makePotrojariRequest(elem, that) {
		if (isEmpty(that)) {
			toastr.error("দুঃখিত! কিছুক্ষন পর আবার চেষ্টা করুন। ধন্যবাদ");
			return false;
		}
		$(that).attr('disabled', 'disabled');
		var potrojari = $('#potrojariSendId').val();
		var nothinote = $('#potrojariSendType').val();
		Metronic.blockUI({
			target: '.modal-full',
			boxed: true
		});
		if (elem.closest('.DraftattachmentsRecord.active').find('#template-body').find('#sender_signature').length == 0 || (elem.closest('.DraftattachmentsRecord.active').find('#template-body').find('#sender_signature img').attr('src') != '' && typeof(elem.closest('.DraftattachmentsRecord.active').find('#template-body').find('#sender_signature img').attr('src')) != 'undefined')) {
			var data2send = {nothi_office:<?php  echo $nothi_office; ?>};
			if (signature > 0) {
				data2send.soft_token = $("#soft_token").val();
			}
			$.ajax({
				url:((!isEmpty(signature) && signature > 0)?ds_url:js_wb_root)+'Potrojari/sendDraftCronPotrojari/<?php echo  $id ?>/' + potrojari + '/' + nothinote,
				method: 'post',
				cache: false,
				dataType: 'JSON',
				data: data2send,
				success: function (response) {

					if (response.status == 'error') {
						$(that).removeAttr('disabled', 'disabled');
						toastr.error(response.msg);
						Metronic.unblockUI('.modal-full');
					} else {
						toastr.success(response.msg);
						window.location.reload();
					}
				},
				error: function (xhr, status, errorThrown) {
					$(that).removeAttr('disabled', 'disabled');
					toastr.options = {
						"closeButton": true,
						"debug": false,
						"positionClass": "toast-bottom-right"
					};
					toastr.error("দুঃখিত! কিছুক্ষন পর আবার চেষ্টা করুন। ধন্যবাদ");
					Metronic.unblockUI('.modal-full');
				}
			});
		} else {
			toastr.error("দুঃখিত! স্বাক্ষর লোড হয়নি। পুনরায় সংশোধন করে সংরক্ষণ করুন।");
			Metronic.unblockUI('.modal-full');
		}

	}

	$(document).on('click', '.btn-delete-draft', function () {

		var potrojari = $(this).attr('potrojari');
		var nothinote = $(this).attr('nothinotesid') == 0 ? 'potro' : 'note';
		bootbox.dialog({
			message: "আপনি কি খসড়া পত্রটি মুছে ফেলতে ইচ্ছুক?",
			title: " খসড়া পত্র মুছুন ",
			buttons: {
				success: {
					label: "হ্যাঁ",
					className: "green",
					callback: function () {
						Metronic.blockUI({
							target: '.page-container',
							boxed: true
						});
						$.ajax({
							url: '<?php
                                echo $this->Url->build(['controller' => 'Potrojari',
                                    'action' => 'deleteDraftPotrojari', $id])
                                ?>/' + potrojari + '/' + nothinote,
							method: 'post',
							cache: false,
							dataType: 'JSON',
							success: function (response) {

								if (response.status == 'error') {
									Metronic.unblockUI('.page-container');
									toastr.error(response.msg);

								} else {

									toastr.success(response.msg);
									//                                offlineRemove(($(this).attr('nothipotroid') == 0 ? $(this).attr('nothinotesid') : $(this).attr('nothinotesid')), nothinote);
									setTimeout(function () {
										window.location.reload();
									}, 2000);
								}
							},
							error: function (xhr, status, errorThrown) {
								toastr.options = {
									"closeButton": true,
									"debug": false,
									"positionClass": "toast-bottom-right"
								};
								Metronic.unblockUI('.page-container');
								toastr.error("দুঃখিত! ইন্টারনেট সংযোগ বিঘ্নিত হয়েছে। কিছুক্ধষণ পর আবার চেষ্টা করুন। ধন্যবাদ");
							}
						});
					}
				},
				danger: {
					label: "না",
					className: "red",
					callback: function () {
					}
				}
			}
		});


	});

	function offlineRemove(id, type) {
		var db = ProjapotiOffline.createDatabase('offline_potro_draft');
		var docofflinepotro = '<?php echo $employee_office['office_id'] . '_' . $employee_office['officer_id'] . '_' . $employee_office['office_unit_organogram_id'] . '_' . $id . '_' ?>_' + id + '_' + type;

		db.get(docofflinepotro).then(function (doc) {
			return db.remove(doc);
		}).then(function (response) {

		}).catch(function (err) {
		});
	}
    <?php endif; ?>

    <?php if (!empty($draftSummary)): ?>
	$(document).on('click', '.btn-send-summary-draft', function () {

		var potrojari = $(this).attr('potrojari');
		bootbox.dialog({
			message: "আপনি কি সার-সংক্ষেপ জারি করতে চান?",
			title: "সার-সংক্ষেপ জারি",
			buttons: {
				success: {
					label: "হ্যাঁ",
					className: "green",
					callback: function () {
						Metronic.blockUI({
							target: '.page-container',
							boxed: true
						});
						$.ajax({
							url: '<?php echo $this->Url->build(['controller' => 'Potrojari', 'action' => 'sendSummaryDraftPotrojari', $id]) ?>/' + potrojari,
							method: 'post',
							cache: false,
							dataType: 'JSON',
							success: function (response) {

								if (response.status == 'error') {
									Metronic.unblockUI('.page-container');
									toastr.error(response.msg);
								} else {
									toastr.success(response.msg);
									setTimeout(function () {
										window.location.reload();
									}, 2000);
								}
							},
							error: function (xhr, status, errorThrown) {
								toastr.options = {
									"closeButton": true,
									"debug": false,
									"positionClass": "toast-bottom-right"
								};
								toastr.error("দুঃখিত! ইন্টারনেট সংযোগ বিঘ্নিত হয়েছে। কিছুক্ধষণ পর আবার চেষ্টা করুন। ধন্যবাদ");
							}
						});
					}
				},
				danger: {
					label: "না",
					className: "red",
					callback: function () {
					}
				}
			}
		});

	});

	$(document).on('click', '.btn-delete-summary-draft', function () {
		var potrojari = $(this).attr('potrojari');
		bootbox.dialog({
			message: "আপনি কি খসড়া মুছে ফেলতে ইচ্ছুক?",
			title: "খসড়া মুছে ফেলুন",
			buttons: {
				success: {
					label: "হ্যাঁ",
					className: "green",
					callback: function () {
						Metronic.blockUI({
							target: '.page-container',
							boxed: true
						});
						$.ajax({
							url: '<?php
                                echo $this->Url->build(['controller' => 'SummaryNothi',
                                    'action' => 'deleteSummaryDraftPotrojari', $id])
                                ?>/' + potrojari,
							method: 'post',
							cache: false,
							dataType: 'JSON',
							success: function (response) {

								if (response.status == 'error') {
									Metronic.unblockUI('.page-container');
									toastr.error(response.msg);

								} else {
									toastr.success(response.msg);
									setTimeout(function () {
										window.location.reload();
									}, 2000);
								}
							},
							error: function (xhr, status, errorThrown) {
								toastr.options = {
									"closeButton": true,
									"debug": false,
									"positionClass": "toast-bottom-right"
								};
								toastr.error("দুঃখিত! ইন্টারনেট সংযোগ বিঘ্নিত হয়েছে। কিছুক্ধষণ পর আবার চেষ্টা করুন। ধন্যবাদ");
							}
						});
					}
				},
				danger: {
					label: "না",
					className: "red",
					callback: function () {
					}
				}
			}
		});


	});

	$(document).off('click', '.summary_draft_approve').on('click', '.summary_draft_approve', function () {
		Metronic.blockUI({
			target: '.page-container',
			boxed: true,
			message: 'অপেক্ষা করুন'
		});
		var potrojari = $(this).attr('potrojari_id');
		var prm = new Promise(function (res, rej) {
			$.ajax({
				url: '<?= $this->Url->build(['controller' => 'SummaryNothi', 'action' => 'getPosition', $id]) ?>/' + potrojari,
				method: "post",
				dataType: 'JSON',
				async: false,
				data: {},
				cache: false,
				success: function (response) {
					if (response.status == 'error') {
						rej(response.msg);
					} else {
						res(response);
					}
				},
				error: function (xhr, status, errorThrown) {
					Metronic.unblockUI('.page-container');
				}
			});
		}).then(function (data) {
			Metronic.unblockUI('.page-container');
			var approves = ($('.summary_draft_approve').is(':checked') === true ? 1 : 0);
			if (approves == 1) {
				$('#sarnothidraft_list').find('#template-body2').find('#' + data.data.position_number).addClass('showImage');
			} else {
				$('#sarnothidraft_list').find('#template-body2').find('#' + data.data.position_number).removeClass('showImage');
			}

			var url = '<?php echo $this->Url->build(['controller' => 'SummaryNothi', 'action' => 'draftApproved', $id]) ?>/' + potrojari + '/' + approves + '/' +<?= $nothi_office ?>;

			$.ajax({
				url: url,
				method: "post",
				dataType: 'JSON',
				async: false,
				cache: false,
				data: {
					description: $('#sarnothidraft_list').find('#template-body2').html()
				},
				success: function (response) {
					if (response.status == 'error') {
						$('.summary_draft_approve').closest('.checked').removeClass('checked');
						$('.summary_draft_approve').removeAttr('checked');
						toastr.error(response.msg);
					} else {
						window.location.reload();
					}
				},
				error: function (xhr, status, errorThrown) {
					Metronic.unblockUI('.page-container');
				}
			});
		}).catch(function (err) {
			taostr.error(err);
		});
	});

	$(document).off('click', '.btn-check-summary-draft').on('click', '.btn-check-summary-draft', function () {
		var potrojari = $(this).attr('potrojari');
		$('.btn-updatesummary').attr('data-id', potrojari);
		var newprom = new Promise(function (resolve, reject) {
			Metronic.blockUI({
				target: '.page-container',
				boxed: true,
				message: 'অপেক্ষা করুন'
			});

			$.ajax({
				url: '<?php echo $this->Url->build(['controller' => 'SummaryNothi', 'action' => 'approvalCheck', $id]) ?>/' + potrojari + '/' + <?= $nothi_office ?>,
				method: "post",
				dataType: 'JSON',
				success: function (response) {
					Metronic.unblockUI('.page-container');
					if (response.status == 'success') {
						if (parseInt(response.count) == 0) {
							$('.btn-check-summary-draft').addClass('btn-send-summary-draft').removeClass('btn-check-summary-draft');
							$('.btn-send-summary-draft').trigger('click');
						} else {
							resolve(potrojari)
						}
					} else {
						reject(response.msg);
					}
				},
				error: function (xhr, status, errorThrown) {
					reject(errorThrown)
				}
			});
		});

		newprom.then(function (potrojari) {
			$.ajax({
				url: '<?php echo $this->Url->build(['controller' => 'SummaryNothi', 'action' => 'approvalUsers', $id]) ?>/' + potrojari + '/' + <?= $nothi_office ?>,
				method: "post",
				dataType: 'JSON',
				success: function (response) {
					$('#modalSummaryNothi').modal('show');
					if (response.status == 'success') {
						$('#modalSummaryNothi').find('#template-bodycover').html(response.potrojari.potro_cover);
						$('#modalSummaryNothi').find('#template-bodybody').html(response.potrojari.potro_description);
						var userDiv = "<table class='table table-bordered'>";

						$.each(response.data, function (i, v) {
							userDiv += "<tr class='approvaluserlist' id='" + v.position_number + "'><td>" + v.employee_name + "</td><td>" + v.designation_name + "</td><td>" + '<input type="text" list="selectReason" class="form-control" placeholder="অনুমোদন না করার কারন লিখুন">' + "</td></tr>";
						});
						userDiv += "</table>";
						$('#modalSummaryNothi').find('.panel-body').html(userDiv);
					} else {

					}
				},
				error: function (xhr, status, errorThrown) {
					newprom.reject(errorThrown)
				}
			});
		}).catch(function (err) {
			toastr.error(err);
		})
	});

	$('.btn-updatesummary').click(function () {
		var userlist = $(this).closest('.panel').find('.approvaluserlist');

		var prm = new Promise(function (resolve, reject) {
			var data = [];
			$.each(userlist, function (i, v) {
				var onumodontext = 0;
				var checkonumodon = 0;

				if ($.trim($(v).find('[type=text]').val()).length > 0) {
					onumodontext = $.trim($(v).find('[type=text]').val());
					checkonumodon = 1;
				} else {
					onumodontext = '';
					checkonumodon = 0;
				}

				data.push({'position': $(v).attr('id'), 'status': checkonumodon});
				if (checkonumodon != 0 && onumodontext != 0) {
					$('#template-bodybody').find('#' + $(v).attr('id')).text(onumodontext).css({
						'color': 'red',
						'font-style': 'italic'
					});
				}
			});

			resolve(data);
		})

		prm.then(function (getdata) {
			var potrojari = $('.btn-updatesummary').data('id');
			$.ajax({
				url: '<?php echo $this->Url->build(['controller' => 'SummaryNothi', 'action' => 'approvalStatus', $id]) ?>/' + potrojari + '/' + <?= $nothi_office ?>,
				method: "post",
				dataType: 'JSON',
				data: {data: getdata, description: $('#template-bodybody').html()},
				success: function (response) {
					if (response.status == 'success') {
						if (parseInt(response.canpotrojari) == 0) {
							setTimeout(function () {
								$.ajax({
									url: '<?php echo $this->Url->build(['controller' => 'Potrojari', 'action' => 'sendSummaryDraftPotrojari', $id]) ?>/' + potrojari,
									method: 'post',
									cache: false,
									dataType: 'JSON',
									success: function (response) {

										if (response.status == 'error') {
											Metronic.unblockUI('.page-container');
											toastr.error(response.msg);
										} else {
											toastr.success(response.msg);
											setTimeout(function () {
												window.location.reload();
											}, 1000);
										}
									},
									error: function (xhr, status, errorThrown) {
										toastr.options = {
											"closeButton": true,
											"debug": false,
											"positionClass": "toast-bottom-right"
										};
										toastr.error("দুঃখিত! ইন্টারনেট সংযোগ বিঘ্নিত হয়েছে। কিছুক্ধষণ পর আবার চেষ্টা করুন। ধন্যবাদ");
									}
								});
							}, 1000)
						} else {
							window.location.reload();
						}
						//
					} else {
						toastr.error(response.msg);
					}
				},
				error: function (xhr, status, errorThrown) {
					toastr.error("দুঃখিত! কিছুক্ধষণ পর আবার চেষ্টা করুন। ধন্যবাদ");
				}
			});
		});

	});

    <?php endif; ?>

	$(document).on('click', '.btn-send-potrojari', function () {
		var serialPermission = 0;

		var nothinotesid = $(this).attr('nothipotroid');
		Metronic.blockUI({
			target: '.page-container',
			boxed: true
		});
		bootbox.dialog({
			message: "আপনি কি সার-সংক্ষেপ জারি করতে চান?",
			title: "সার-সংক্ষেপ জারি",
			buttons: {
				success: {
					label: "হ্যাঁ",
					className: "green",
					callback: function () {
						$.ajax({
							url: '<?php
                                echo $this->Url->build(['controller' => 'Potrojari',
                                    'action' => 'sendPotrojaryByMinistry', $id])
                                ?>/' + nothinotesid,
							method: 'post',
							cache: false,
							dataType: 'JSON',
							success: function (response) {

								if (response.status == 'error') {
									toastr.error(response.msg);
									Metronic.unblockUI('.page-container');
								} else {
									toastr.success(response.msg);

									setTimeout(function () {
										window.location.reload();
									}, 2000);
								}
							},
							error: function (xhr, status, errorThrown) {
								toastr.options = {
									"closeButton": true,
									"debug": false,
									"positionClass": "toast-bottom-right"
								};
								Metronic.unblockUI('.page-container');
								toastr.error("দুঃখিত! ইন্টারনেট সংযোগ বিঘ্নিত হয়েছে। কিছুক্ধষণ পর আবার চেষ্টা করুন। ধন্যবাদ");
							}
						});
					}
				},
				danger: {
					label: "না",
					className: "red",
					callback: function () {
						Metronic.unblockUI('.page-container');
					}
				}
			}
		});
	});

	$(document).on('click', '.btn-printsummarydraft', function () {
		var id = $(this).data('id')
		showPdf('সার-সংক্ষেপ প্রিন্ট প্রিভিউ', '<?= $this->Url->build(['controller' => 'Potrojari', 'action' => 'getPdfById']) ?>/' + id + '/<?= $nothi_office ?>');
		$(".btn-pdf-margin").click();
	});

	$(document).on('click', '.btn-summaryprintdraft', function () {
		var id = $(this).data('id')
		showPdf('সার-সংক্ষেপ প্রিন্ট প্রিভিউ', '<?= $this->Url->build(['controller' => 'Potrojari', 'action' => 'getPdfByPotro']) ?>/' + id + '/<?= $nothi_office ?>');
		$(".btn-pdf-margin").click();
	});
	$(document).on('click', '.btn-printdraft', function () {
		var id = $(this).data('id');
		var potro_type = !isEmpty($(this).data('potro-type')) ? $(this).data('potro-type') : 0;
		console.log(potro_type);
		if (potro_type == 30) {
			if ($('.potroview').find('.DraftattachmentsRecord.active').find('.tab-pane.active').hasClass("csCover")) {
				showPdf('পত্র প্রিন্ট প্রিভিউ', '<?= $this->Url->build(['controller' => 'Potrojari', 'action' => 'getPdfById']) ?>/' + id + '/<?= $nothi_office ?>' + '?show=cover');
			} else {
				showPdf('পত্র প্রিন্ট প্রিভিউ', '<?= $this->Url->build(['controller' => 'Potrojari', 'action' => 'getPdfById']) ?>/' + id + '/<?= $nothi_office ?>' + '?show=body');
			}
		} else {
			showPdf('পত্র প্রিন্ট প্রিভিউ', '<?= $this->Url->build(['controller' => 'Potrojari', 'action' => 'getPdfById']) ?>/' + id + '/<?= $nothi_office ?>');
		}
		$(".btn-pdf-margin").click();
	});

	$(document).on('click', '.btn-printpopPotro', function () {
		var node = $(this).closest('div.active').find('.templateWrapper')
		showPrintView(node);
	});
	$(document).on('click', '.btn-printnothijato', function () {
		$(this).closest('.NothijatoattachmentsRecord').children('div').eq(1).printThis({
			importCSS: true,
			debug: false,
			importStyle: true,
			printContainer: false,
			pageTitle: "",
			removeInline: false,
			header: null
		});
	});
	$(document).on('click', '.btn-print', function () {
		var id = $(this).data('id')
		showPdf('পত্র প্রিন্ট প্রিভিউ', '<?= $this->Url->build(['controller' => 'Potrojari', 'action' => 'getPdfByPotro']) ?>/' + id + '/<?= $nothi_office ?>');
		$(".btn-pdf-margin").click();
	});
	$(document).on('click', '.btn-printall', function () {
		var id = $(this).data('id')
		showPdf('পত্র প্রিন্ট প্রিভিউ', '<?= $this->Url->build(['controller' => 'Potrojari', 'action' => 'getPdfByPotro']) ?>/' + id + '/<?= $nothi_office ?>');
		$(".btn-pdf-margin").click();
	});

	$(document).off('click', '.songlapbutton').on('click', '.songlapbutton', function () {
		$('#responsivepotalaLog').modal('show');
		$('#responsivepotalaLog').find('.scroller').html('<img src="' + js_wb_root + 'assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp;লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');

		var id = $(this).attr('id');
		var url = $(this).data('url');
		var type = $(this).data('type');
		var officeid = parseInt($(this).data('nothi-office'));

		if (typeof (id) != 'undefined') {
			$.ajax({
				url: '<?php
                    echo $this->Url->build(['controller' => 'NothiMasters',
                        'action' => 'showSonglap'])
                    ?>/' + id + '/' + officeid,
				method: 'post',
				dataType: 'json',
				cache: false,
				success: function (response) {

					var attachmenttype = response.attachment_type;
					if (response.attachment_type == 'text') {
						$('#responsivepotalaLog').find('.scroller').html('<div style="background-color: #fff; max-width:950px; min-height:815px;  margin:0 auto; page-break-inside: auto; ">' + response.potro_cover + "<br/>" + response.content_body + "</div>");
					} else if ((attachmenttype.substring(0, 15)) == 'application/vnd' || (attachmenttype.substring(0, 15)) == 'application/ms') {
						$('#responsivepotalaLog').find('.scroller').html('<iframe style=" width:100%; height: 700px;" src="https://docs.google.com/gview?url=' + encodeURIComponent('<?php echo FILE_FOLDER ?>' + response.file_name) + '&embedded=true" style="width:600px; height:500px;" frameborder="0"></iframe>');

					} else if ((attachmenttype.substring(0, 5)) != 'image') {
						$('#responsivepotalaLog').find('.scroller').html('<embed src="' + '<?php echo $this->request->webroot . 'content/' ?>' + response.file_name + '" style=" width:100%; height: 700px;" type="' + response.attachment_type + '"></embed>');
					} else {
						$('#responsivepotalaLog').find('.scroller').html('<div class="text-center"><a onclick="return false;" href="' + '<?php echo $this->request->webroot . 'content/' ?>' + response.file_name + '" data-title="পত্র নম্বর:  ' + response.nothi_potro_page_bn + '" data-footer="" ><img class="zoomimg img-responsive"  src="' + '<?php echo $this->request->webroot . 'content/' ?>' + response.file_name + '" alt="ছবি পাওয়া যায়নি"></a></div>');
					}
				},
				error: function (err, status, rspn) {
					$('#responsivepotalaLog').find('.scroller').html('');
				}
			})
		} else if (typeof (url) != 'undefined') {
			if ((type.substring(0, 15)) == 'application/vnd' || (type.substring(0, 15)) == 'application/ms') {
				$('#responsivepotalaLog').find('.scroller').html('<iframe style=" width:100%; height: 700px;" src="https://docs.google.com/gview?url=' + url + '&embedded=true" style="width:600px; height:500px;" frameborder="0"></iframe>');

			} else if ((type.substring(0, 5)) != 'image') {
				$('#responsivepotalaLog').find('.scroller').html('<embed src="' + url + '" style=" width:100%; height: 700px;" type="' + type + '"></embed>');
			} else {
				$('#responsivepotalaLog').find('.scroller').html('<div class="text-center"><a onclick="return false;" href="' + url + '" data-title="সংলাগ" data-footer="" ><img class="zoomimg img-responsive"  src="' + url + '" alt="সংলাগ পাওয়া যায়নি" /></a></div>');
			}
		}
	});

	function clonePotrojari(url) {
		if (!checkBrowserIsChrome()) {
			return;
		}
		Metronic.blockUI({
			target: '.page-container',
			boxed: true,
			message: 'অপেক্ষা করুন'
		});
		bootbox.dialog({
			message: "আপনি কি পত্রজারি ক্লোন করতে ইচ্ছুক?",
			title: "পত্রজারি ক্লোন করুন",
			buttons: {
				success: {
					label: "হ্যাঁ",
					className: "green",
					callback: function () {
						bootbox.hideAll();
						window.location.href = url;
					}
				},
				danger: {
					label: "না",
					className: "red",
					callback: function () {
						Metronic.unblockUI('.page-container');
					}
				}
			}
		});
	}

	function getSignDateToPotroDate(element) {
		$(element).html("<i class='fa fa-cog fa-spin'></i> হালনাগাদ হচ্ছে...");
		var potrojari_language = $('.DraftattachmentsRecord.active').find(' #template-body').attr('potrojari_language');
		if (potrojari_language == 'eng') {
			var signDate = $.trim($('.modal#potrojariSendModal .A4:last').find("#sender_signature_date").text());

			if (signDate.split('/').length == 3) {
				var signDate = signDate.split('/');
			} else {
				var signDate = signDate.split('-');
			}
			signDate = new Date(signDate[2] + '/' + signDate[1] + '/' + signDate[0]);

			$('.modal#potrojariSendModal .A4:last').find("#sending_date").text(signDate.getDate() + '/' + (signDate.getMonth() + 1) + '/' + signDate.getFullYear());
			$('.DraftattachmentsRecord.active #template-body:last').find("#sending_date").text(signDate.getDate() + '/' + (signDate.getMonth() + 1) + '/' + signDate.getFullYear());
			if ($('.modal#potrojariSendModal .A4:last').find("#sending_date_2").length > 0) {
				$('.modal#potrojariSendModal .A4:last').find("#sending_date_2").text(signDate.getDate() + '/' + (signDate.getMonth() + 1) + '/' + signDate.getFullYear());
				$('.DraftattachmentsRecord.active #template-body:last').find("#sending_date_2").text(signDate.getDate() + '/' + (signDate.getMonth() + 1) + '/' + signDate.getFullYear());
			}

			var potrojari_id = $('.modal#potrojariSendModal').find("#potrojariSendId").val();
			$('.modal#potrojariSendModal .A4').find(".potrojari_header_banner").remove();
			var potrojari_body = $('.modal#potrojariSendModal .A4').html();
			$.ajax({
				url: js_wb_root + 'NothiMasters/updatePotrojariBody',
				type: 'POST',
				cache: false,
				data: {'potrojari_id': potrojari_id, 'body': potrojari_body, 'nothi_office': '<?=$nothi_office?>', 'nothi_part_no': <?=$id?>},
				async: true,
				success: function (response) {
					if (response.status == 'success') {
						toastr.success('তারিখ সফলভাবে হালনাগাদ করা হয়েছে। আপনি এখন পত্রজারি করতে পারেন।');
						$(".dateChange").hide();
					} else {
						$(element).html("এখানে পুনরায় চেষ্টা করুন");
						toastr.error('তারিখ হালনাগাদ এই মুহূর্তে সম্ভব হচ্ছে। একটু পর আবার চেষ্টা করুন।');
					}
				}
			});
		} else {
			var signDate = EngFromBn($.trim($('.modal#potrojariSendModal .A4:last').find("#sender_signature_date").text())).split('-');
			var engDateInBengali = dateConvert(signDate[2] + "-" + signDate[1] + "-" + signDate[0]);
			$.ajax({
				url: js_wb_root + "banglaDate",
				data: {date: signDate[2] + "-" + signDate[1] + "-" + signDate[0]},
				type: 'POST',
				cache: false,
				dataType: 'JSON',
				async: true,
				success: function (bangladate) {
					var bngDateInBengali = bangladate.date + " " + bangladate.month + " " + bangladate.year;
					$('.modal#potrojariSendModal .A4:last').find("#sending_date").text(engDateInBengali.replace(',', ' '));
					$('.modal#potrojariSendModal .A4:last').find("#sending_date").prev().prev(".bangladate").text(bngDateInBengali);

					$('.DraftattachmentsRecord.active #template-body:last').find("#sending_date").text(engDateInBengali.replace(',', ' '));
					$('.DraftattachmentsRecord.active #template-body:last').find("#sending_date").prev().prev(".bangladate").text(bngDateInBengali);

					if ($('.modal#potrojariSendModal .A4:last').find("#sending_date_2").length > 0) {
						$('.modal#potrojariSendModal .A4:last').find("#sending_date_2").text(engDateInBengali.replace(',', ' '));
						$('.DraftattachmentsRecord.active #template-body:last').find("#sending_date_2").text(engDateInBengali.replace(',', ' '));

						$('.modal#potrojariSendModal .A4:last').find("#sending_date_2").prev().prev(".bangladate").text(bngDateInBengali);
						$('.DraftattachmentsRecord.active #template-body:last').find("#sending_date_2").prev().prev(".bangladate").text(bngDateInBengali);
					}

					var potrojari_id = $('.modal#potrojariSendModal').find("#potrojariSendId").val();
					$('.modal#potrojariSendModal .A4').find(".potrojari_header_banner").remove();
					var potrojari_body = $('.modal#potrojariSendModal .A4').html();
					$.ajax({
						url: js_wb_root + 'NothiMasters/updatePotrojariBody',
						type: 'POST',
						cache: false,
                        data: {'potrojari_id': potrojari_id, 'body': potrojari_body, 'nothi_office': '<?=$nothi_office?>', 'nothi_part_no': <?=$id?>},
						async: true,
						success: function (response) {
							if (response.status == 'success') {
								toastr.success('তারিখ সফলভাবে হালনাগাদ করা হয়েছে। আপনি এখন পত্রজারি করতে পারেন।');
								$(".dateChange").hide();
							} else {
								$(element).html("এখানে পুনরায় চেষ্টা করুন");
								toastr.error('তারিখ হালনাগাদ এই মুহূর্তে সম্ভব হচ্ছে। একটু পর আবার চেষ্টা করুন।');
							}
						}
					});
				}
			});
		}
	}

	function dateConvert(value) {
		var bDate = new Array("০", "১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯");
		var bMonth = new Array("জানুয়ারি", "ফেব্রুয়ারি", "মার্চ", "এপ্রিল", "মে", "জুন", "জুলাই", "আগস্ট", "সেপ্টেম্বর", "অক্টোবর", "নভেম্বর", "ডিসেম্বর");
		var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

		if (value != '' && value != null) {
			var dt = new Date(value);
			var dtb = dt.getDate();
			var dtb1 = "", dtb2 = "";


			if (dtb >= 10) {
				dtb1 = Math.floor(dtb / 10);
				dtb2 = dtb % 10;
				dtb = bDate[dtb1] + "" + bDate[dtb2];
			} else {
				dtb = bDate[0] + "" + bDate[dtb];
			}

			var mnb;
			var mn = dt.getMonth();
			mnb = bMonth[mn];

			var yrb = "", yr1;
			var yr = dt.getFullYear();

			for (var i = 0; i < 3; i++) {
				yr1 = yr % 10;
				yrb = bDate[yr1] + yrb;
				yr = Math.floor(yr / 10);
			}

			yrb = bDate[yr] + "" + yrb;

			//return dtb + " " + mnb + "," + yrb + " খ্রিষ্টাব্দ";
			return dtb + " " + mnb + "," + yrb;
		}
	}

	$(function () {
		$(document).find('[title]').tooltip({'placement': 'bottom', 'container': 'body'});
	})
</script>
<?php
if (isset($hasCsTemplate) && $hasCsTemplate == 1) {
    echo $this->Html->script('/projapoti-nothi/js/cs_related.js');
    $user_signature = sGenerateToken(['file' => $loggedUser['username']], ['exp' => time() + 60 * 300]);
    echo $this->Form->hidden('user_signature_token', ['id' => 'user_signature_token', 'value' => $user_signature]);
    echo $this->Form->hidden('username', ['id' => 'username', 'value' => $loggedUser['username']]);
    ?>
    <script>
		setTimeout(csSettings.init(<?=json_encode($employee_office)?>), 2000);
    </script>
    <?php
}
?>