
<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-toastr/toastr.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>projapoti-nothi/css/styles.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2.css"/>
<style>
    .portlet-body img {
        max-width: 100%;
        height: auto;
    }

    .potroview {
        margin: 0 auto;
    }

    div.potroAccordianPaginationBody {
        display: none;
    }

    div.potroAccordianPaginationBody.active {
        display: block;
    }

    a{
        cursor: pointer;
    }

    .editable {
        border: none;
        word-break: break-word;
    }

    #potrodraft_list #sovapoti_signature, #potrodraft_list #sender_signature,#potrodraft_list #sender_signature2,#potrodraft_list #sender_signature3{
        visibility: hidden;
    }

    table{
        width: 100%!important;
    }

    .bangladate{
        border-bottom: 1px solid #000;
    }

    .btn-changelog, .btn-forward-nothi,.btn-nothiback ,.btn-print, .btn-approve{
        padding: 3px 5px !important;
        border-width: 1px;
    }
    .editable {
        border:none!important;
        word-break:break-word;
        word-wrap:break-word
    }

    #sovapoti_signature, #sender_signature, #sender_signature2, #sender_signature3{
        visibility: hidden;
    }
    #note{
        overflow: hidden;
        word-break:break-word;
        word-wrap: break-word;
        height: 100%;
    }
</style>
<style>
    .editable-click, a.editable-click {
        border: none;
        word-break:break-word;
        word-wrap:break-word
    }
    .cc_list {
        white-space: pre-wrap;
    }

    .popover-content {
        padding: 10px 30px;
    }
    .mega-menu-dropdown > .dropdown-menu{
        top:10px!important;
    }
</style>

<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <?php echo "শাখা: ".$officeUnitsName."; নথি নম্বর: ".$nothiRecord['nothi_no'].'; বিষয়: '.$nothiRecord['subject']; ?>
        </div>

        <div class="actions">
            <a  title="নথিতে ফেরত যান"  class=" btn   btn-primary btn-nothiback" href="<?php echo $this->Url->build(['_name' => 'noteDetail', $nothimasterid, $nothi_office]) ?>">
                <i class="fs1 a2i_gn_details1"></i> নথিসমূহ
            </a>

        </div>

    </div>
    <div class="portlet-body">
        <div class="row ">
            <div class="col-md-12 " style="margin: 0 auto;">
<?php // if ($privilige_type == 1) {  ?>
                <div class="form">

                    <?php echo $this->Form->create('',
                        array('onsubmit' => 'return false;', 'type' => 'file', 'id' => 'potrojariDraftForm')); ?>
                    <?php
                    echo $this->Form->hidden('id');
                    ?>

                    <div class="row form-group">
                        <div class="col-lg-12">

                            <div class="tabbable-custom nav-justified">
                                <ul class="nav nav-tabs nav-justified">
                                    <li class="active">
                                        <a href="#tab_1_1_1" data-toggle="tab">
                                            কভার পাতা</a>
                                    </li>
                                    <li>
                                        <a href="#tab_1_1_2" data-toggle="tab">
                                            সার-সংক্ষেপ পাতা </a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_1_1_1">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div id="template-body" style="max-width: 950px;min-height: 815px;    height: auto;    margin: 20px auto;    padding-bottom: 50px;    display: block;    background-color: rgb(255, 255, 255);    border: 2px solid #E6E6E6; <?php echo!empty($mainpotro)
                            ? "" : "display:none;" ?>">
<?php echo!empty($mainpotro)
        ? $mainpotro->potro_cover : '' ?>
                                                </div>
                                            </div>
                                            <textarea id="contentbody" name="contentbody" style="display: none"></textarea>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab_1_1_2">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div id="template-body2" style="max-width: 950px;min-height: 815px;    height: auto;    margin: 20px auto;    padding-bottom: 50px;    display: block;    background-color: rgb(255, 255, 255);    border: 2px solid #E6E6E6; <?php echo!empty($mainpotro)
        ? "" : "display:none;" ?>">
<?php echo!empty($mainpotro)
        ? $mainpotro->content_body : '' ?>
                                                </div>
                                            </div>
                                            <textarea id="contentbody2" name="contentbody2" style="display: none"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
<?php echo $this->Form->end(); ?>

                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <h3 > <?php echo __(SHONGJUKTI) ?> </h3>

                                <!-- The table listing the files available for upload/download -->
                                <table role="presentation" class="table table-striped clearfix">
                                    <tbody class="files">
                                        <?php
                                        if (isset($attachments) && count($attachments)
                                            > 0) {
                                            foreach ($attachments as $single_data) {
                                                if ($single_data['attachment_type']
                                                    == 'text' || $single_data['attachment_type']
                                                    == 'text/html') continue;

                                                $fileName          = explode('/',
                                                    $single_data['file_name']);
                                                $attachmentHeaders = get_file_type($single_data['file_name']);
                                                $value             = array(
                                                    'name' => urldecode($fileName[count($fileName)
                                                        - 1]),
                                                    'thumbnailUrl' => $this->request->webroot . 'content/'.$single_data['file_name'].'?token='.sGenerateToken(['file'=>$single_data['file_name']],['exp'=>time() + 60*300]),
                                                    'size' => '-',
                                                    'type' => $attachmentHeaders,
                                                    'url' => $this->request->webroot . 'content/'.$single_data['file_name'].'?token='.sGenerateToken(['file'=>$single_data['file_name']],['exp'=>time() + 60*300])
                                                );

                                                echo '<tr class="template-download fade in">
    <td>
    <input type="checkbox" name="delete" value="1" class="toggle">
    <span class="preview">
    '.(isset($value['thumbnailUrl']) ? '<a href="'.$value['url'].'" title="'.$value['name'].'" download="'.$value['url'].'" data-gallery><img style="width:50px;height:50px;" src="'.$value['thumbnailUrl'].'"></a>'
                                                        : '').'
    </span>
    </td>
    <td>
    <p class="name">
    '.(isset($value['name']) ? '<a href="'.$value['url'].'" title="'.$value['name'].'" download="'.$value['name'].'" data-gallery="">'.$value['name'].'</a>'
                                                        : '').'
    </p>
    </td>
    <td>
    '.(isset($value['size']) ? '<span class="size">'.$value['size'].'</span>' : '').'
    </td>
  
    </tr>';
                                            }
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
<?php if ($privilige_type == 1 && $can_approve && $approved
    == 0): ?>
                        <div class="form-actions">
                            অনুমোদন দিন  <input type="checkbox"  class="checkbox btn-approve" />
                            <!--<input type="button" class="btn btn-success approveDraftNothi" value="<?php echo __("Give Permission") ?>" />-->
                            <input type="button" class="btn btn-primary saveDraftNothi" value="<?php echo __(SAVE) ?>" />
                        </div>
<?php endif; ?>                    
                </div>

            </div>
        </div>
    </div>
</div>

<?php if ($privilige_type == 1): ?>
    <div id="responsiveNothiUsers" class="modal fade modal-purple" tabindex="-1" aria-hidden="true" data-backdrop="static"
         data-keyboard="false">
        <div class="modal-dialog modal-full">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">পরবর্তী কার্যক্রমের জন্য প্রেরণ করুন</h4>
                </div>
                <div class="modal-body">
                    <div class="scroller" style="height:100%" data-always-visible="1" data-rail-visible1="1">
                        <input type="hidden" name="nothimasterid"/>

                        <div class="user_list">

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn green sendDraftNothi">প্রেরণ করুন</button>
                    <button type="button" data-dismiss="modal" class="btn  btn-danger">
                        বন্ধ করুন
                    </button>
                </div>
            </div>
        </div>
    </div>


    <script src="<?php echo $this->request->webroot; ?>projapoti-nothi/js/nothi_master_movements.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-toastr/toastr.min.css"/>
    <script src="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-toastr/toastr.min.js"></script>
    <script src="<?php echo CDN_PATH; ?>assets/admin/pages/scripts/ui-toastr.js"></script>

    <script src="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2.js" type="text/javascript"></script>

    <script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/jquery.mockjax.js"></script>
    <script src="<?php echo CDN_PATH; ?>assets/admin/pages/scripts/form-editable.js"></script>


    <script>
        var numbers = {
            1: '১',
            2: '২',
            3: '৩',
            4: '৪',
            5: '৫',
            6: '৬',
            7: '৭',
            8: '৮',
            9: '৯',
            0: '০'
        };

        function replaceNumbers(input) {
            var output = [];
            for (var i = 0; i < input.length; ++i) {
                if (numbers.hasOwnProperty(input[i])) {
                    output.push(numbers[input[i]]);
                } else {
                    output.push(input[i]);
                }
            }
            return output.join('');
        }

        var DRAFT_FORM = {
            setform: function () {

                if ($('#potrojariDraftForm').find('#ministry_note').text() == '...') {
                    $('#potrojariDraftForm').find('#ministry_note').remove();
                } else {
                    var txt = $('#template-body2').find('#ministry_note').html().replace(/\n/g, '<br/>');
                    var id = $('#template-body2').find('#ministry_note').attr('id');
                    $('#template-body2').find('#ministry_note').replaceWith("<div id='" + id + "' >" + txt + "</div>");
                }

                var templatebody = $('#template-body2').html();
                $('#contentbody2').text(templatebody);

                var templatebody = $('#template-body').html();
                $('#contentbody').text(templatebody);
            }
        };

    </script>

    <script>
        $(function () {
            NothiMasterMovement.init('nothing');
            $('select').select2();
    <?php if ($privilige_type == 1 && $can_approve && $approved == 0): ?>
                sayWhoCan();
                function sayWhoCan() {
                    if ($('#potrojariDraftForm').find('#ministry_note').length == 0) {
                        if ($.trim($('#second_receiver_signature').html()) == '' || $.trim($('#second_receiver_signature').html()) == '&nbsp;') {
                            $('#second_receiver_signature').before('<div class="col-md-12 col-xs-12 col-sm-12 text-center"> <a href="javascript:;" class="editable editable-click " id="ministry_note" data-type="text" data-pk="1" data-original-title="">...</a></div>');
                            
                        } else if ($.trim($('#third_receiver_signature').html()) == '' || $.trim($('#third_receiver_signature').html()) == '&nbsp;') {
                            $('#third_receiver_signature').before('<div class="col-md-12 col-xs-12 col-sm-12 text-center"> <a href="javascript:;" class="editable editable-click " id="ministry_note" data-type="text" data-pk="1" data-original-title="">...</a></div>');
                       
                        } else if ($.trim($('#fourth_receiver_signature').html()) == '' || $.trim($('#fourth_receiver_signature').html()) == '&nbsp;') {
                            $('#fourth_receiver_signature').before('<div class="col-md-12 col-xs-12 col-sm-12 text-center"> <a href="javascript:;" class="editable editable-click " id="ministry_note" data-type="textarea" data-pk="1" data-original-title="">...</a></div>');
                           
                        } else if ($.trim($('#fifth_receiver_signature').html()) == '' || $.trim($('#fifth_receiver_signature').html()) == '&nbsp;') {
                            $('#fifth_receiver_signature').before('<div class="col-md-12 col-xs-12 col-sm-12 text-center"> <a href="javascript:;" class="editable editable-click " id="ministry_note" data-type="textarea" data-pk="1" data-original-title="">...</a></div>');
                        }
                    }
                }
    <?php endif; ?>
        });

    </script>

    <!-- End: JavaScript -->
    <!-- The blueimp Gallery widget -->
    <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
        <div class="slides">
        </div>
        <h3 class="title"></h3>
        <a class="prev">
            ÔøΩ </a>
        <a class="next">
            ÔøΩ </a>
        <a class="close white">
        </a>
        <a class="play-pause">
        </a>
        <ol class="indicator">
        </ol>
    </div>
    <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
    <script id="template-upload" type="text/x-tmpl">
        {% for (var i=0, file; file=o.files[i]; i++) { %}
        <tr class="template-upload fade">
        <td>
        <span class="preview"></span>
        </td>
        <td>
        <p class="name">{%=file.name%}</p>
        <strong class="error label label-danger"></strong>
        </td>
        <td>
        <p class="size">প্রক্রিয়াকরন চলছে...</p>
        <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
        <div class="progress-bar progress-bar-success" style="width:0%;"></div>
        </div>
        </td>
        <td>
        {% if (!i && !o.options.autoUpload) { %}
        <button class="btn blue start" disabled>
        <i class="fa fa-upload"></i>
        <span>আপলোড করুন</span>
        </button>
        {% } %}
        {% if (!i) { %}
        <button class="btn red cancel">
        <i class="fa fa-ban"></i>
        <span>বাতিল করুন</span>
        </button>
        {% } %}
        </td>
        </tr>
        {% } %}

    </script>
    <!-- The template to display files available for download -->
    <script id="template-download" type="text/x-tmpl">
        {% for (var i=0, file; file=o.files[i]; i++) { %}
        <tr class="template-download fade">
        <td>

        <span class="preview">
        {% if (file.thumbnailUrl) { %}
        <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
        {% } %}
        </span>
        </td>
        <td>
        <p class="name">
        {% if (file.url) { %}
        <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
        {% } else { %}
        <span>{%=file.name%}</span>
        {% } %}
        </p>
        {% if (file.error) { %}
        <div><span class="label label-danger"> ত্রুটি: </span> {%=file.error%}</div>
        {% } %}
        </td>
        <td>
        <span class="size">{%=o.formatFileSize(file.size)%}</span>
        </td>

        </tr>
        {% } %}

    </script>

    <script>
        function saveOnucced(){
            if($('#potrojariDraftForm').find('#ministry_note').length >0 ){
                $.ajax({
                        url: '<?= $this->Url->build(['controller' => 'NothiNoteSheets',
'action' => 'saveNote']) ?>',
                    data: {id: (typeof ($('#potrojariDraftForm').find('#ministry_note').data("id")) != 'undefined' ? $('#potrojariDraftForm').find('#ministry_note').data("id") : 0), nothimasterid:<?= $nothiRecord['id'] ?>, body: $('#potrojariDraftForm').find('#ministry_note').html(), notesheetid:<?= $notesheedid['nothi_notesheet_id'] ?>, notesubject: '', nothi_office:<?= $nothi_office ?>, notedecision: ''},
                    type: 'POST',
                    cache: false,
                    success: function (response) {

                        if (response > 0) {
                            $('#potrojariDraftForm').find('#ministry_note').attr("data-id", response);
                        }
                    }
                });
            }
        }
        $(document).on('click', '.saveDraftNothi', function () {
//            if (confirm("আপনি কি অনুমতি " + ($('.btn-approve').is(':checked') ? "" : "না") + " দিয়ে পাঠাতে চান?")) {
                approvesign();
                if (DRAFT_FORM.setform() != false) {

                    $.ajax({
                        url: '<?php echo $this->Url->build(['controller' => 'NothiMasters',
        'action' => 'DraftUpdateByMinistry', $nothimasterid, $nothipotroid, $nothi_office]) ?>',
                        data: $("#potrojariDraftForm").serialize(),
                        method: "post",
                        dataType: 'JSON',
                        async: false,
                        cache: false,
                        success: function (response) {
                            
                            if (response.status == 'error') {
                                toastr.error(response.msg);
                            } else {
                                saveOnucced();
                                setTimeout(function(){
                                    window.location.href = '<?php echo $this->Url->build(['_name' => 'noteDetail', $nothimasterid, $nothi_office]) ?>';
                                },1500);
                                
                            }
                        },
                        error: function (xhr, status, errorThrown) {

                        }
                    });
                }
//            }
        });


        function approvesign() {

            var url = '<?php echo $this->Url->build(['controller' => 'Potrojari', 'action' => 'draftApprovedByMinistry']) ?>/' + 0 + '/' +<?= $nothi_office ?>;
            if ($('.btn-approve').is(':checked')) {
                url = '<?php echo $this->Url->build(['controller' => 'Potrojari', 'action' => 'draftApprovedByMinistry']) ?>/' + 1 + '/' +<?= $nothi_office ?>;
            }
            $.ajax({
                url: url,
                method: "post",
                data: {'potrojari_id':<?php echo $mainpotro->id ?>},
                dataType: 'JSON',
                async: false,
                cache: false,
                success: function (response) {
                    if (response.status == 'error') {
                        $('.btn-approve').closest('.checked').removeClass('checked');
                        $('.btn-approve').removeAttr('checked');
                        toastr.error(response.msg);
                    } else {
                        if (response.data['will_sign'] == 1) {
                            if ($.trim($('#second_receiver_signature').html()) == '' || $.trim($('#second_receiver_signature').html()) == '&nbsp;') {
                                $('#second_receiver_signature').html('<img src="' + response.data['signature'] + '" alt="signature" width="100" />');
                            } else if ($.trim($('#third_receiver_signature').html()) == '' || $.trim($('#third_receiver_signature').html()) == '&nbsp;') {
                                $('#third_receiver_signature').html('<img src="' + response.data['signature'] + '" alt="signature" width="100" />');
                            } else if ($.trim($('#fourth_receiver_signature').html()) == '' || $.trim($('#fourth_receiver_signature').html()) == '&nbsp;') {
                                $('#fourth_receiver_signature').html('<img src="' + response.data['signature'] + '" alt="signature" width="100" />');
                            } else if ($.trim($('#fifth_receiver_signature').html()) == '' || $.trim($('#fifth_receiver_signature').html()) == '&nbsp;') {
                                $('#fifth_receiver_signature').html('<img src="' + response.data['signature'] + '" alt="signature" width="100" />');
                            }
                        }
                    }
                },
                error: function (xhr, status, errorThrown) {

                }
            });
        }
        ;
    </script>
<?php endif; ?>

<?php echo $this->Html->script('assets/global/scripts/printThis.js'); ?>
<script>
    $(document).on('click', '.btn-print', function () {
        $('#template-body').printThis({
            importCSS: true,
            debug: false,
            importStyle: true,
            printContainer: true,
            pageTitle: "",
            removeInline: false,
            header: null
        });
    })
</script>