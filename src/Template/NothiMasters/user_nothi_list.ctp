<style>
    .radio-list {
        display: inline;
    }

    .checkbox input[type=checkbox], .checkbox-inline input[type=checkbox], .radio input[type=radio], .radio-inline input[type=radio] {
        margin-left: 0px;
        position: relative;
    }

    .checkbox-inline, .radio-inline {
        padding-left: 5px;
        font-size: 12px;
    }

    .checkbox-inline + .checkbox-inline, .radio-inline + .radio-inline {
        margin-left: 2px;
    }

    .checkbox input[type="checkbox"], .checkbox-inline input[type="checkbox"], .radio input[type="radio"], .radio-inline input[type="radio"] {
        margin-left: 0px !important;
    }

    input[type="checkbox"], input[type="radio"] {
        margin-top: 0px;
    }
</style>


<div class="portlet-body form">
    <div class="row" style="display: none;">
        <label class="col-md-2 control-label">তারিখ </label>
        <div class="col-md-10">
            <div class="input-group date form_date">
                <?= $this->Form->input('due_date', ['label' => false, 'class' => 'form-control','type' => 'hidden', 'readonly' => true, 'value' => Date("Y-m-d")]); ?>
                <span class="input-group-btn">
                    <button class="btn default date-set" type="button"><i class="fa fa-calendar"></i>
                    </button>
                </span>
            </div>
        </div>
    </div>
    <div class="row">
        <!--<div class="col-md-offset-6 col-md-6">
            <input type="submit" class="btn btn-primary " value="<?php echo __(SAVE) ?>"/>
            <button type="button" data-dismiss="modal" class="btn  btn-danger">বন্ধ করুন</button>
        </div>-->
        <div class="col-md-12">
            <?php echo $this->cell('NothiOwnUnit',['subject'=>$dak_subject,'nothijato'=>(isset($nothijato)?$nothijato:0),'selectedMeta'=>$selectedMeta]) ?>
        </div>
    </div>

    <!--<div class="form-actions">
        <div class="row">
            <div class="col-md-offset-1 col-md-11">
                <input type="submit" class="btn btn-primary " value="<?php echo __(SAVE) ?>"/>
                <button type="button" data-dismiss="modal" class="btn  btn-danger">বন্ধ করুন</button>
            </div>
        </div>
    </div>-->
</div>
