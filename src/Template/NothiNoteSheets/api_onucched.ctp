<style>
    #সংরক্ষণ-1 {
        width: 76px;
        color: #0098f7;
        border-color: #0098f7;
        /*background: #0098f7;*/
    }

    #ফেরত-যান-1 {
        width: 90px;
        color: red;
    }
</style>
<form action="<?php echo $this->Url->build(['controller' => 'NothiNoteSheets',
    'action' => 'apiNotesheetPage', $part_id, $nothi_office, '?' => ['data_ref' => 'api', 'user_designation' => $user_designation, 'api_key' => $api_key, 'is_new' => 1]]);
?>" method="post" id="postApiNothiNotesheetPage">
    <input type="hidden" name="nothimasterid" value="<?= $nothimasterid; ?>">
    <input type="hidden" name="notesheetid" value="<?= $notesheetid ?>">
    <input type="hidden" name="notesubject" id="nothi-note-subject" value="<?= $notesubject ?>">
    <input type="hidden" name="nothi_office" value="<?= $nothi_office ?>">
    <input type="hidden" name="user_designation" value="<?= $user_designation ?>">
    <input type="hidden" name="noteno" value="0">
    <input type="hidden" name="body" id="body" value="">
    <input type="hidden" name="id" value="">
    <input type="hidden" name="is_new" value="1">
    <input type="hidden" name="height" value="<?= $height ?>">
    <input type="hidden" name="width" value="<?= $width ?>">
</form>
<?= $this->element('froala_editor_js_css') ?>
<textarea name="editor_content"
          id="noteComposer"><?= !empty($notes['note_description']) ? base64_decode($notes['note_description']) : '' ?></textarea>
<script>
    document.documentElement.scrollTop = 0;
	$(document).ready(function () {
		toastr.options = {
			"closeButton": true,
			"debug": false,
			"positionClass": "toast-top-center"
		};
		getAutolist();
	});
	$.FroalaEditor.DefineIcon('bibeccoPotro', {NAME: 'envelope'});
	$.FroalaEditor.RegisterCommand('bibeccoPotro', {
		title: 'বিবেচ্য পত্র বাছাই করুন',
		type: 'dropdown',
		focus: true,
		undo: false,
		refreshAfterCallback: true,
        options: { '0':'--' },
		callback: function (cmd, val) {
			if (val != '0') {
				var value = $('[id^=dropdown-menu-bibeccoPotro] ul li a[data-param1="' + val + '"]').attr('nothi_potro_page_bn');
				$('#noteComposer').froalaEditor('html.insert', " বিবেচ্য পত্র: <a class='showforPopup'  href='" + EngFromBn(val) + "/potro' title='পত্র'>" + value + "</a>, &nbsp;");

			}
		}
	});

	$.FroalaEditor.DefineIcon('bibeccoOnucced', {NAME: 'pencil-square-o'});
	$.FroalaEditor.RegisterCommand('bibeccoOnucced', {
		title: 'অনুচ্ছেদ বাছাই করুন',
		type: 'dropdown',
		focus: false,
		undo: false,
		refreshAfterCallback: true,
		options: {
			'--': '--'
		},
		callback: function (cmd, val) {
			var text_value = $('[id^=dropdown-menu-bibeccoOnucced] ul li a[data-param1="' + val + '"]').text();
			if (text_value.length > 0 && text_value != '--') {
				if ($('[id^=dropdown-menu-bibeccoOnucced] ul li a[data-param1="' + val + '"]').hasClass('fullNote')) {

					$('#noteComposer').froalaEditor('html.insert', "অনুচ্ছেদ <a target='__tab' href='<?php
                        echo $this->Url->build(['_name' => 'noteDetail'], true)
                        ?>" + $('[id^=dropdown-menu-bibeccoOnucced] ul li a[data-param1="' + text_value + '"]').attr('nothi_part_no') + "' title='নোট'> " + text_value + "</a>, &nbsp;", false);

				} else {

					$('#noteComposer').froalaEditor('html.insert', 'অনুচ্ছেদ <a class="showforPopup" href="' + val + '" title="অনুচ্ছেদ">' + text_value + '</a>, &nbsp;', false);
				}

			}
		}
	});

	$.FroalaEditor.DefineIcon('noteDecision', {NAME: 'gavel'});
	$.FroalaEditor.RegisterCommand('noteDecision', {
		title: 'সিদ্ধান্ত বাছাই করুন',
		type: 'dropdown',
		focus: false,
		undo: false,
		refreshAfterCallback: true,
		options: {
			'--': '--',
            <?php
            $indx = 0;
            if (!empty($noteDecision)) {
                foreach ($noteDecision as $key => $value) {
                    if (!($indx == 0)) {
                        echo ',';
                    }
                    echo "'" . h($value) . "' : '" . h($value) . "'";
                    $indx++;
                }
            }?>
		},
		callback: function (cmd, val) {
			if (val.length > 0 && val != '--') {
				$('#noteComposer').froalaEditor('html.insert', val + ' ', false);
			}
		}
	});
	// $.FroalaEditor.DefineIcon('সংরক্ষণ', {NAME: 'save'});
	$.FroalaEditor.RegisterCommand('সংরক্ষণ', {
		title: 'অনুচ্ছেদ সংরক্ষণ করুন',
		focus: false,
		undo: false,
		showOnMobile: true,
		refreshAfterCallback: false,
		callback: function (cmd, val) {
			saveOnucched();
		}
	});
	// $.FroalaEditor.DefineIcon('নোটাংশ', {NAME: 'arrow-left'});
	$.FroalaEditor.RegisterCommand('ফেরত-যান', {
		title: 'নোটাংশে ফিরে যান',
		focus: false,
		undo: false,
		showOnMobile: true,
		refreshAfterCallback: false,
		callback: function (cmd, val) {
			$("#postApiNothiNotesheetPage").submit();
		}
	});
	var buttons = ['bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', '|', 'fontFamily', 'fontSize', 'color', 'inlineStyle', 'paragraphStyle', '|', 'paragraphFormat', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote', 'insertLink', 'insertTable', '|', 'emoticons', 'insertHR', 'selectAll', 'clearFormatting', '|', 'print', 'undo', 'redo', '|', 'bibeccoPotro', 'bibeccoOnucced', 'noteDecision', '|', 'ফেরত-যান', 'সংরক্ষণ'];
	var buttonsSm = ['bold', 'underline', '|', 'fontSize', 'align', '|', 'formatOL', 'formatUL', 'outdent', 'indent', '|', 'bibeccoPotro', 'bibeccoOnucced', 'noteDecision', '|', 'ফেরত-যান', 'সংরক্ষণ'];
	$('#noteComposer').froalaEditor({
		key: 'xc1We1KYi1Ta1WId1CVd1F==',
		toolbarSticky: false,
		wordPasteModal: true,
		wordAllowedStyleProps: ['font-size', 'background', 'color', 'text-align', 'vertical-align', 'background-color', 'padding', 'margin', 'height', 'margin-top', 'margin-left', 'margin-right', 'margin-bottom', 'text-decoration', 'font-weight'],
		wordDeniedTags: ['a', 'form'],
		wordDeniedAttrs: ['width'],
		tableResizerOffset: 10,
		tableResizingLimit: 20,
		toolbarButtons: buttons,
		toolbarButtonsMD: buttons,
		toolbarButtonsSM: buttonsSm,
		toolbarButtonsXS: buttonsSm,
		placeholderText: '',
		height: <?= (empty($height) ? 740 : $height) ?>,
		width: <?= (empty($width) ? 360 : $width) ?>,
		enter: $.FroalaEditor.ENTER_BR,
		fontSize: ['10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30'],
		fontSizeDefaultSelection: '13',
		tableEditButtons: ['tableHeader', 'tableRemove', '|', 'tableRows', 'tableColumns', 'tableCells', '-', 'tableCellBackground', 'tableCellVerticalAlign', 'tableCellHorizontalAlign', 'tableCellStyle'],
		fontFamily: {
			"Nikosh": "Nikosh",
			"Arial": "Arial",
			"Kalpurush": "Kalpurush",
			"SolaimanLipi": "SolaimanLipi",
			"'times new roman'": "Times New Roman",
			"'Open Sans Condensed',sans-serif": 'Open Sans Condensed'
		}
	});

	function getAutolist() {
        var bibecchoPotroList = '<li role="presentation"><a class="fr-command" tabindex="-1" role="option" attachment_id="--" data-cmd="bibeccoPotro" data-param1="0" title="" aria-selected="false">--</a></li>';

        var bibecchePotroJson = <?= !empty($potroAttachmentRecord)?json_encode($potroAttachmentRecord):'{}'; ?>;
        $.each(bibecchePotroJson, function (i, v) {
            bibecchoPotroList += '<li role="presentation"><a class="fr-command" tabindex="-1" role="option" data-cmd="bibeccoPotro" data-param1="' + v.id + '" title="'+ v.nothi_potro_page_bn +' - '+escapeHtml(v.subject)+'" nothi_potro_page_bn= "'+ v.nothi_potro_page_bn +'" aria-selected="false">'+ v.nothi_potro_page_bn +' - '+escapeHtml(v.subject)+'</a></li>';
        });

        $('[id^=dropdown-menu-bibeccoPotro] ul').html(bibecchoPotroList);

        var onuccedList = '<li role="presentation"><a class="fr-command" tabindex="-1" role="option" data-cmd="bibeccoOnucced" data-param1="--" title="" aria-selected="false">--</a></li>';

		$('[id^=dropdown-menu-bibeccoOnucced] ul').html(onuccedList);

		$.ajax({
			url: js_wb_root + 'nothiMasters/onuccedList/<?= $part_id ?>/<?= $nothi_office . '/' . $user_designation ?>',
			dataType: 'JSON',
			success: function (response) {

				var allstar = '';
				$.each(response, function (i, v) {
					if (allstar != v.nothi_part_no) {
						onuccedList += '<li role="presentation"><a class="fr-command fullNote" tabindex="-1" role="option" data-cmd="bibeccoOnucced" data-param1="' + v.nothi_part_no_bn + " *" + '" nothi_part_no=' + v.nothi_part_no + ' notesheet=' + v.nothi_notesheet_id + ' title="' + v.nothi_part_no_bn + '" aria-selected="false">' + v.nothi_part_no_bn + " *" + '</a></li>';
						allstar = v.nothi_part_no;
					}
					onuccedList += '<li role="presentation"><a class="fr-command" tabindex="-1" role="option" data-cmd="bibeccoOnucced" data-param1="' + v.id + '" nothi_part_no="' + v.nothi_part_no + '" notesheet="' + v.nothi_notesheet_id + '" title="' + v.note_no + '" aria-selected="false">' + v.note_no + '</a></li>';
				});

				$('[id^=dropdown-menu-bibeccoOnucced] ul').html(onuccedList);
			}
		});
	}

	function saveOnucched(from_other_function) {

		var body = $('#noteComposer').froalaEditor('html.get');
		if (($('#noteComposer').froalaEditor('html.get') == "") || ($('#noteComposer').froalaEditor('html.get') == "<br><br>")) {
			toastr.error('কোনো অনুচ্ছেদ দেয়া হয়নি');
			$('.fr-view').focus();
			return false;
		}
		Metronic.blockUI({
			target: '.fr-box',
			boxed: true
		});
		$.ajax({
			method: 'POST',
			async: false,
			dataType: 'json',
			url: '<?php
                echo $this->Url->build(['controller' => 'NothiNoteSheets',
                    'action' => 'ApiNoteOnuccedAdd', '?' => ['data_ref' => 'api'], $part_id, $nothi_office]);
                ?>',
			data: {
				nothimasterid: <?= $nothimasterid; ?>,
				notesheetid: <?= $notesheetid ?>,
				id: '<?= (!empty($id)) ? $id : 0?>',
				body: body,
				noteno: 0,
				notesubject: $("#nothi-note-subject").val(),
				nothi_office: <?php echo $nothi_office ?>,
				user_designation: <?= $user_designation ?>,
				apikey: "<?= $api_key ?>"

			},
			success: function (msg) {

				if (msg.status == 'success') {
//                    if(isEmpty(from_other_function)){
					$("#postApiNothiNotesheetPage").submit();
//                    }else{
//                        return true;
//                    }
				} else {
					Metronic.unblockUI('.fr-box');

					toastr.error(msg.msg);
					return false;
				}

			}
		});
	}

	var $onucched_count = '<?= !empty($onucched_count) ? $onucched_count : 0?>';
    <?php
    if(empty($device_type) || $device_type == 'android'){
    ?>
	function hasUnfinishedOnucched() {
		MyAndroid.receiveValueFromJs(!saveOnucched(1));
	}

	function emptyOnucchedCheck() {
		if ($onucched_count > 0) {
			// MyAndroid.receiveValueFromJs(false);
		} else {
			//MyAndroid.receiveValueFromJs(true);
		}
	}
    <?php
    }else{
    ?>
	function hasUnfinishedOnucched() {
		return !saveOnucched(1);
	}

	function emptyOnucchedCheck() {
		if ($onucched_count > 0) {
			return false;
		} else {
			return true;
		}
	}
    <?php
    }
    ?>

</script>