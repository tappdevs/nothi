<div class="portlet light">
    <div class="portlet-title hidden-print">
        <div class="caption"><i class="fs1 a2i_gn_details1"></i>বিনষ্টযোগ্য নথিসমূহের নিবন্ধনবহি</div>
    </div>
    <div class="portlet-body">
        <div class="table-toolbar">
            <div class="row text-center">

                <h4><?php // echo $office['office_records'] [0]['office_name']; ?></h4>
                <h5><?php // echo $office['office_records'] [0]['office_address']; ?></h5>

            </div>

            <div class="row">
                <div class="col-md-12">


                    <!-- Tools start  -->

                    <!--<div class="hidden-print btn-group pull-left">
                        <button class="btn dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Tools <i
                                class="glyphicon glyphicon-chevron-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right">
                            <li>
                                <a  href="javascript:void(0);">
                                    Save as PDF </a>
                            </li>
                            <li>
                                <a href="javascript:window.print();">
                                    Print </a>
                            </li>
                        </ul>
                    </div>-->

                    <!--Tools end -->

                    <!--  Date Range Begain -->

                    <div class="hidden-print page-toolbar pull-right portlet-title">
                        <div id="dashboard-report-range" class=" tooltips btn btn-sm btn-default"
                             data-container="body"
                             data-placement="bottom" data-original-title="তারিখ নির্বাচন করুন">
                            <i class="icon-calendar"></i>&nbsp; <span
                                class="thin uppercase visible-lg-inline-block">তারিখ নির্বাচন করুন</span>&nbsp;
                            <i class="glyphicon glyphicon-chevron-down"></i>
                        </div>
                    </div>

                    <!--  Date Range End  -->

                </div>

            </div>
        </div>
        <div id="table">
            <div class="portlet light">
                <div class="portlet-body inbox-content">
                  
                </div>
            </div>
        </div>
<!--        <div class="row">
            <div class="col-md-12">
                <a class="btn pull-right btn-sm blue hidden-print margin-bottom-5" onclick="javascript:window.print();">
                    <?php // echo __("Print") ?> <i class="fs1 a2i_gn_print2"></i> </a>
            </div>
        </div>-->
    </div>
</div>

<script src="<?php echo CDN_PATH; ?>js/reports/binostojoggo_nothi_reports.js" type="text/javascript"></script>
<script src="<?php echo CDN_PATH; ?>daptorik_preview/js/report_date_range.js"></script>

<script>
    jQuery(document).ready(function () {
        $(this).find('body').addClass('page-sidebar-closed');
        $(this).find('.page-sidebar-menu').addClass('page-sidebar-menu-closed');
        DateRange.init();
        DateRange.initDashboardDaterange();

        $(document).off('click','.pagination a').on('click','.pagination a',function(ev){
            ev.preventDefault();
            PROJAPOTI.ajaxLoad($(this).attr('href'),'.inbox-content');
        });
    });
</script>






