<?php
$prev = "";
$count = 0;
if (isset($_GET['page'])) {
    $count = ($_GET['page'] * 20) - 20;
}
if (!empty($data)) {
    ?>
    <div class="table-container  table-scrollable">
        <table class="table table-bordered table-hover">
            <tr class="heading">
                <th class="text-center"  style="width: 5%;">ক্রমিক সংখ্যা</th>
                <th class="text-center"  style="width: 20%;">নথির নম্বর</th>
                <th class="text-center"  style="width: 20%;">পূর্ববর্তী নথির নম্বরসমূহ</th>
                <th class="text-center"  style="width: 30%;">অফিস/শাখার নাম</th>
                <th class="text-center"  style="width: 30%;">ধরন/শিরোনাম</th>
                <th class="text-center" style="width: 45%;">শ্রেণিবিন্যাসসহ নথিভুক্ত করবার তারিখ</th>
            </tr>
            <?php
            foreach ($data as $key => $value) {
                $unitInformation = $value['office_units_name'];
                $unitId = $value['office_units_id'];
                $count++;
                ?>
                <tr>
                    <td class="text-center"><?= $this->Number->format($count) ?></td>
                    <td class="text-center"><?= h($value['nothi_no']) ?></td>
                    <td class="text-center"><?= $value['changes_history'] ?></td>
                    <td class="text-center"><?= $value['office_units_name'] ?></td>
                    <td class="text-left"><?= h($value['NothiTypes']['type_name']) . ' - ' . h($value['subject']) ?></td>

                    <td class="text-center"><?= $nothiClass[$value['nothi_class']] . ', ' . Cake\I18n\Time::parse($value['nothi_created_date'])->i18nFormat('dd-MM-yyyy') ?></td>
                </tr>
                <?php
            }
            ?>
        </table>
    </div>
    <script>
        $("#content-export-button").css('display', 'initial');
        $("#report_pagination").css('display', 'block');
    </script>
    <?php
} else {
    ?>
    <div class="alert alert-danger">
        দুঃখিত কোন তথ্য পাওয়া যায় নি।
    </div>
    <script>
        $("#content-export-button").css('display', 'none');
        $("#report_pagination").css('display', 'none');
    </script>

    <!--<div class="table-container  table-scrollable">
        <table class="table table-bordered table-striped">
            <tr class="heading">
                <th class="text-center"  style="width: 5%;">ক্রমিক সংখ্যা</th>
                <th class="text-center"  style="width: 20%;">নথির নম্বর</th>
                <th class="text-center"  style="width: 30%;">ধরন/শিরোনাম</th>
                <th class="text-center" style="width: 45%;">শ্রেণিবিন্যাসসহ নথিভুক্ত করবার তারিখ</th>
            </tr>

            <tr><td class="text-center" colspan="4" style="color:red;"> দুঃখিত কোন তথ্য পাওয়া যায় নি।  </td></tr> 
        </table>
    </div>-->
    <?php
}
?>
<div class="actions text-center" id="report_pagination">
    <ul class="pagination pagination-sm">
        <?php
        echo $this->Paginator->first(__('প্রথম', true), array('class' => 'number-first'));
        echo $this->Paginator->prev('<<', array('tag' => 'li', 'escape' => false), '<a href="#" class="btn btn-sm blue">&laquo;</a>', array('class' => 'prev disabled btn btn-sm blue', 'tag' => 'li', 'escape' => false));
        echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a', 'reverse' => FALSE));
        echo $this->Paginator->next('>>', array('tag' => 'li', 'escape' => false), '<a href="#" class="btn btn-sm blue ">&raquo;</a>', array('class' => 'next disabled', 'tag' => 'li', 'escape' => false));
        echo $this->Paginator->last(__('শেষ', true), array('class' => 'number-last'));
        ?>
    </ul>

</div>























