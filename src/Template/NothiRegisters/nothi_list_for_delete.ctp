<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/media/js/jquery.dataTables.js"></script>
<style>
	.table td, .table th {
		padding:2px;
	}

	.dataTables_filter{
		margin: 5px 10px;
	}
	.dataTables_filter input{
		padding: 2px 10px;
		border-radius: 5px !important;
		margin: 5px;
	}
</style>
<div class="table-scrollable">
	<?php
	if (!empty($NothiMaster)) {
		foreach ($NothiMaster as $id => $sub) {
			?>
			<div data-toggle="collapse" data-parent="#MasterList" data-target="#row_<?=$id?>" onclick="loadNote(<?=$id?>, this)" aria-expanded="false" style="cursor: pointer;">
				<div id="<?= $id ?>" colspan="4" style="padding: 10px;background-color:#dff0d8;border:solid 1px #bdceb7;"><strong>নথির বিষয়:</strong> <?= $sub ?> <div class="pull-right"><i class="glyphicon glyphicon-chevron-down"></i></div> </div>
			</div>
			<div class="collapse" id="row_<?=$id?>"><table class="table table-striped table-hover table-bordered"><tr><td colspan="4">লোড হচ্ছে...</td></tr></table></div>
			<?php
		}
	}
	?>
</div>
<script>
    $('.checker').uniform();
    $(document).ready( function () {
        $("#RemovePermissionOnNothiList").dataTable().fnDestroy();
        $("#RemovePermissionOnNothiList").dataTable({
            "bLengthChange": false,
            "pageLength": 50,
            "language": {
                "url": '//cdn.datatables.net/plug-ins/1.10.19/i18n/Bangla.json',
                "sSearch": "",
                "searchPlaceholder": "নথি খুঁজুন"
            },
        });
        $('.dataTables_filter input').addClass('form-control');
    } );


    function loadNote(nothiMasterId, element) {
        if (!$("#row_"+nothiMasterId).hasClass('in')) {
            $(element).find('.glyphicon-chevron-down').addClass('glyphicon-chevron-up');
            $(element).find('.glyphicon-chevron-down').removeClass('glyphicon-chevron-down');
            if ($("#row_"+nothiMasterId).find('tr').length < 2) {
                $.ajax({
                    type: 'GET',
                    url: "<?php echo $this->Url->build(['controller' => 'NothiRegisters', 'action' => 'showMasterPermissionListUsingOrganogram']) ?>/" + nothiMasterId + "/1/" + $("#office-unit-organogram-id").val(),
                    success: function (data) {
                        if (data.status == 'success') {
                            $("#row_" + nothiMasterId).find('table').html('<tr><th onclick="checkAll(this)"><input type="checkbox" class="checkAll"></th><th> নোট নং </th><th> নোটের বিষয় </th><th> অনুমতিপ্রাপ্তগণের তালিকা </th></tr>');
                            $.each(data.data, function (key, value) {
                                if ($("#office-unit-organogram-id").val() == value.office_units_organogram_id) {
                                    var checkbox = '<i class="fa fa-lock" title="নোট তৈরিকারী"></i>';
                                } else {
                                    var checkbox = '<input type="checkbox" class="checker" class="form-control" value="' + value.id + '">';
                                }
                                $("#row_" + nothiMasterId).find('table').append('<tr>' +
                                    '<td>' + checkbox + '</td>' +
                                    '<td>' + (value.nothi_part_no_bn) + '</td>' +
                                    '<td>' + (value.subject) + '</td>' +
                                    '<td><button type="button" class="btn btn-sm btn-primary round-corner-5" onclick="loadPermissionUsers(' + nothiMasterId + ', ' + value.id + ')"> অনুমতিপ্রাপ্তগণ </button></td>' +
                                    '</tr>');
                            });
                        }
                        $(".checker").uniform();
                        $(".checkAll").uniform();
                    }
                });
            }
        } else {
            $(element).find('.glyphicon-chevron-up').addClass('glyphicon-chevron-down');
            $(element).find('.glyphicon-chevron-up').removeClass('glyphicon-chevron-up');
        }
    }
    function loadPermissionUsers(nothiMasterId, nothiPartId) {
        $("#myModal").modal('show');
        $('#loadPrivilage').html('<img src="<?= CDN_PATH ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp; লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');
        $.ajax({
            type: 'POST',
            url: "<?php echo $this->Url->build(['controller' => 'NothiRegisters', 'action' => 'showMasterPermissionList']) ?>",
            data: {'part': nothiPartId,master: nothiMasterId},
            success: function (data) {
                if (data.status == 'success') {
                    var html = '';
                    $.each(data.data, function (i, v) {
                        html += '<li class="list-group-item">' + v + '</li>';
                    });
                    $("#loadPrivilage").html(html);
                }
            }
        });
    }

    function checkAll(element) {
        if ($(element).find('.checkAll').is(':checked')) {
            $(element).closest('tbody').find('input[type=checkbox].checker').prop('checked', true).uniform('refresh');
        } else {
            $(element).closest('tbody').find('input[type=checkbox].checker').prop('checked', false).uniform('refresh')
        }
    }
</script>