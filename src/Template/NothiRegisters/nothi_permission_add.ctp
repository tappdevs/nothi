<style>
	.panel-heading  a:before {
		font-family: 'Glyphicons Halflings';
		content: "\e114";
		float: right;
		transition: all 0.5s;
	}
	.panel-heading.active a:before {
		-webkit-transform: rotate(180deg);
		-moz-transform: rotate(180deg);
		transform: rotate(180deg);
	}
</style>
<div class="portlet light">
    <div class="portlet-title hidden-print">
        <div class="caption"><i class="fa fa-unlock-alt" aria-hidden="true"></i> <span
                class="title">নথিতে অনুমতি প্রদান</span></div>
    </div>
    <div class="portlet-body">
        <div class="row">
            <?=$this->Cell('OfficeSelection'); ?>
            <div class="row">
                <div class="col-md-4 form-group form-horizontal">
                    <label class="control-label"> <?php echo __("Office") ?> </label>
                    <?php
                    echo $this->Form->input('office_id',
                        array(
                        'label' => false,
                        'class' => 'form-control',
                        'empty' => '----',
                        'options' => array($employee_office['office_id'] => $employee_office['office_name'])
                    ));
                    ?>
                </div>
                <div class="col-md-4 form-group form-horizontal">
                    <label class="control-label"> <?php echo __("Unit") ?> </label>
                    <?php
                    echo $this->Form->input('office_unit_id',
                        array(
                        'label' => false,
                        'class' => 'form-control',
                        'empty' => '----'
                    ));
                    ?>
                </div>
            </div>
            <div class="row text-center">
                <button class=" btn btn-primary round-corner-5" id="sub_grp_potrojari">অনুমতি প্রদান করুন</button>
            </div>
            <hr>
        </div>
        <div class="row">
            <div class="col-md-6 col-sm-6 margin-right-10">
                <div class="row">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-md-12" style="text-align: center;">
                                    যেসব নথিতে আপনার অনুমতি আছে
                                </div>
                            </div>
                        </div>
                        <div class="panel-body"  style="min-height: calc(100vh - 250px); max-height: 500px; overflow: auto;">
	                        <div class="pull-right" style="margin-bottom: 5px;">
		                        <div class="input-group">
			                        <input type="text" class="form-control" name="searchNothi" placeholder="নথি খুঁজুন" onchange="searchNothi(this.value)" />
			                        <div class="input-group-btn">
				                        <button title="খুঁজুন" class="btn btn-success" type="submit" style="padding: 5px 14px;">
					                        <i class="glyphicon glyphicon-search"></i>
				                        </button>
			                        </div>
	                            </div>
                            </div>
	                        <div class="table-scrollable">
                                <div id="MasterList">
	                                <div onclick="checkAllNothi(this)"><input type="checkbox" class="checkAllNothi form-control"> সকল নথি নির্বাচন করুন</div>
                                    <?php
                                    if (!empty($NothiMaster)) {
                                        foreach ($NothiMaster as $id => $sub) {
                                            ?>
                                            <div data-toggle="collapse" data-parent="#MasterList" data-target="#row_<?=$id?>" onclick="loadNote(<?=$id?>, this)" aria-expanded="false" style="cursor: pointer;">
                                                <div id="<?= $id ?>" colspan="4" style="padding: 10px;background-color:#dff0d8;border:solid 1px #bdceb7;">
	                                                <input type="checkbox" class="checkerNothi form-control" value="<?=$id?>">
	                                                <strong>নথির বিষয়:</strong> <?= $sub ?>
	                                                <div class="pull-right"><i class="glyphicon glyphicon-chevron-down"></i></div>
                                                </div>
                                            </div>
                                            <div class="collapse" id="row_<?=$id?>"><table class="table table-striped table-hover table-bordered"><tr><td colspan="4">লোড হচ্ছে...</td></tr></table></div>
                                            <?php
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-md-5 col-sm-5">
                <div class="row">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-md-12" style="text-align: center;">
                                    অনুমতিপ্রাপ্ত ব্যক্তিবর্গ  <label class="badge badge-primary totalgroupmember"></label>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body" >
                            <table class="table table-bordered">
                                <tbody id="addotherpermission">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="">
                    <div class="row">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-md-12" style="text-align: center;">
                                        শাখাভিত্তিক পদবি-তালিকা
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body"  id="showlist" style="max-height: 500px; overflow: auto;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="myModal" class="modal fade modal-purple" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">অনুমতিপ্রাপ্তগণের তালিকা </h4>
                    </div>
                    <div class="modal-body">
                        <div class="" >
                            <ul class="list-group" id="loadPrivilage" style="max-height: calc(100vh - 195px); margin-bottom:0px; min-height:100px; overflow: auto;">

                            </ul>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal"><?= __('Close') ?></button>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
        <script>
            var initial = 0;
            function showPermission(master) {
                Metronic.blockUI({target: '.portlet-body', boxed: true});
                $.ajax({
                    type: 'GET',
                    url: "<?php echo $this->Url->build(['controller' => 'NothiRegisters', 'action' => 'showMasterPermissionList']) ?>/" + master,
                    success: function (data) {
                        Metronic.unblockUI('.portlet-body');
                        if (data.status == 'success') {
                            $('#nothi-part-no').val('');
                            $('#loadPrivilage').html('');
                            $('#nothi-name').val('');
                            $('#mster').val('');
                            $("#nothi-name").val($("#" + master).text());
                            $("#mster").val(master);
                            PROJAPOTI.projapoti_dropdown_map("#nothi-part-no", data.data, "--নোট নম্বর বাছাই করুন--");
                            $("#myModal").modal('show');
                            loadPrivilages();
                            initial++;
                            
                        }
                    },
                    error: function () {
                        Metronic.unblockUI('.portlet-body');
                    }
                });
            }
            function loadPrivilages() {
            if(initial == 0){
                $("#nothi-part-no").on('change', function () {
                    var part = $("#nothi-part-no :selected").val();
                    if (part == 0)
                        return;
                    $('#loadPrivilage').html('<img src="<?= CDN_PATH ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp; লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');
                    $.ajax({
                        type: 'POST',
                        url: "<?php echo $this->Url->build(['controller' => 'NothiRegisters', 'action' => 'showMasterPermissionList']) ?>",
                        data: {'part': part,master: $("#mster").val()},
                        success: function (data) {
                            if (data.status == 'success') {
                                var html = '';
                                $.each(data.data, function (i, v) {
                                    html += '<li class="list-group-item">' + v + '</li>';
                                });
                                $("#loadPrivilage").html(html);
                            }
                        }
                    });
                });
                }
                 $("#nothi-part-no").trigger('change');
            }
            var EmployeeAssignment = {
                loadMinistryWiseLayers: function () {
                    OfficeSetup.loadLayers($("#office-ministry-id").val(), function (response) {

                    });
                },
                loadMinistryAndLayerWiseOfficeOrigin: function () {
                    OfficeSetup.loadOffices($("#office-ministry-id").val(), $("#office-layer-id").val(), function (response) {

                    });
                },
                loadOriginOffices: function () {
                    OfficeSetup.loadOriginOffices($("#office-origin-id").val(), function (response) {

                    });
                },
                loadOfficeUnits: function () {
                    OfficeSetup.loadOfficeUnits($("#office-id").val(), function (response) {

                    });
                    $('html, body').animate({
                        scrollTop: $("#sub_grp_potrojari").offset().top - 80
                    }, 2000);
                },
            };
            $(function () {

                if ($("#office-unit-id").val() > 0) {
                    getNotification($("#office-id").val(), $("#office-unit-id").val());
                }

                $("#office-ministry-id").bind('change', function () {
                    EmployeeAssignment.loadMinistryWiseLayers();
                });
                $("#office-layer-id").bind('change', function () {
                    EmployeeAssignment.loadMinistryAndLayerWiseOfficeOrigin();
                });
                $("#office-origin-id").bind('change', function () {
                    EmployeeAssignment.loadOriginOffices();
                });
                $("#office-id").bind('change', function () {
                    EmployeeAssignment.loadOfficeUnits();
                    getNotification($(this).val());
                });
                $("#office-unit-id").bind('change', function () {
                    getNotification($("#office-id").val(), $(this).val());
                });
                function getNotification(office_id, unit_id) {
                    $('#showlist').html('<img src="<?= CDN_PATH ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp; লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');
                    $.ajax({
                        type: 'POST',
                        url: "<?php echo $this->Url->build(['controller' => 'NothiMasters', 'action' => 'getOtherOfficePermissionList']) ?>",
                        data: {office_id: office_id, office_unit_id: unit_id, nothi_id: 0, nothi_office: office_id},
                        success: function (data) {
                            $('#showlist').html('');
                            $('#showlist').html('<div class="font-lg">সকল পদবি দেখার জন্য  <b>&darr;শাখার নামে</b> ক্লিক করুন </div>' + data);
                            $(".checkthis").uniform();
                        }
                    });
                }

                var officeId = <?=$employee_office['office_id']?>;
                $("#office-id").val(officeId).trigger('change');
                $( document ).ajaxStop(function() {
                    var officeUnitId = <?=$employee_office['office_unit_id']?>;
                    if ($("#office-unit-id").val() == 0) {
                        if ($("#office-unit-id option[value=" + officeUnitId + "]").length == 0) {
                            if ($("#office-unit-id").val() != 0) {
                                $("#office-unit-id").val(0).trigger('change');
                            }
                        } else {
                            if ($("#office-unit-id").val() != officeUnitId) {
                                $("#office-unit-id").val(officeUnitId).trigger('change');
                            }
                        }
                    }
                });

                $(".checkerNothi").uniform();
                $(".checkAllNothi").uniform();
            });
            var numbers = {
                1: '১',
                2: '২',
                3: '৩',
                4: '৪',
                5: '৫',
                6: '৬',
                7: '৭',
                8: '৮',
                9: '৯',
                0: '০'
            };

            function replaceNumbers(input) {
                var output = [];
                for (var i = 0; i < input.length; ++i) {
                    if (numbers.hasOwnProperty(input[i])) {
                        output.push(numbers[input[i]]);
                    } else {
                        output.push(input[i]);
                    }
                }
                return output.join('');
            }
            function list_all(e_id)
            {
                $.each($('#showlist').find('.optionsothers'), function (i, v) {
                    if ($(v).data('office-unit-organogram-id') == e_id)
                    {
                        if ($(v).is(':checked'))
                        {
                            var f = 0;
                            $.each($('#addotherpermission').find('.deleterow'), function () {
                                if ($(this).find('input').data('officeUnitOrganogramId') == e_id) {
                                    toastr.error("এই ব্যক্তি পূর্বে থেকে অনুমতিপ্রাপ্ত");
                                    f = 1;
                                }
                            });
                            if (f === 0) {
                                var ok = '';
                                var id = $(v).data('employee-id');
                                var name = $(v).data('employee-name');
                                var of_id = $(v).data('employee-office-id');
                                var unit_id = $(v).data('office-unit-id');
                                var org_id = $(v).data('office-unit-organogram-id');
                                var designation = $(v).data('designation-name');
                                var unit_name = $(v).data('unit-name');
                                var designation_level = $(v).data('designation-level');
                                var office_name = $(v).data('office-name');
                                ok += '<tr><td class="deleterow" style="width: 30px;text-align: center;"><i class="fa fa-times"></i>';
                                ok += '<input type="hidden" class="group_mail_permission" name="office-employee" data-office-name="' + office_name + '" data-office-id="' + of_id + '"' +
                                        'data-office-employee-name="' + name + '" data-office-employee-id="' + id + '"' +
                                        ' data-office-unit-id="' + unit_id + '" data-office-unit-organogram-id="' + org_id + '" ' +
                                        'data-designation-name="' + designation + '" data-unit-name="' + unit_name + '" data-designation-level ="' + designation_level + '">' +
                                        '</td><td class="text-left">&nbsp;&nbsp;' + name + ', ' + designation + ', ' + unit_name + ', ' + office_name + '</td></tr>';
                                $('#addotherpermission').append(ok);
                            }
                        } else {
                            $.each($('#addotherpermission').find('.deleterow'), function () {
                                if ($(this).find('input').data('officeUnitOrganogramId') == e_id) {
                                    $(this).closest('tr').remove();
                                }
                            });
                        }
                    }
                });
                refresh_delete();
            }
            $("#sub_grp_potrojari").on('click', function () {
                Metronic.blockUI({target: '.portlet-body', boxed: true, message: '<br/>অনুমতি প্রদান প্রক্রিয়া চলছে... ০%'});
                var nothi_masters = {};
                var array = {};
                $.each($("input.checkerNothi:checked"), function (i, v) {
                    nothi_masters[$(v).val()] = 'all';
                });
                $.each($("input.checker:checked"), function (i, v) {
                    var masterId = $(v).closest('.collapse').attr('id').split('_').pop();
                    if (isEmpty(nothi_masters[masterId])) {
                        nothi_masters[masterId] = {};
                    }
                    if (nothi_masters[masterId] != 'all') {
                        nothi_masters[masterId][i] = $(v).val();
                    }
                });
                //        var group_type = $("input[type=radio][name=group_type]:checked").val();
                var selectedPriviliges = {};
                $.each($('#addotherpermission').find('.group_mail_permission'), function (j, v) {
                    selectedPriviliges[j] = {};
                    selectedPriviliges[j]['office_id'] = $(v).data('office-id');
                    selectedPriviliges[j]['office_unit_id'] = $(v).data('office-unit-id');
                    selectedPriviliges[j]['office_unit_organogram_id'] = $(v).data('office-unit-organogram-id');
                    selectedPriviliges[j]['employee_id'] = $(v).data('office-employee-id');
                    selectedPriviliges[j]['office_unit_name'] = $(v).data('unit-name');
                    selectedPriviliges[j]['office_unit_organogram_name'] = $(v).data('designation-name');
                    selectedPriviliges[j]['office_name'] = $(v).data('office-name');
                    selectedPriviliges[j]['employee_name'] = $(v).data('office-employee-name');
                    selectedPriviliges[j]['designation_level'] = $(v).data('designation-level');
                    if ($(v).data('officer-email') == '' || $(v).data('officer-email') == undefined || $(v).data('officer-email') == null || typeof ($(v).data('officer-email')) == 'undefined') {

                    } else {
                        selectedPriviliges[j]['officer_email'] = $(v).data('officer-email');
                    }
                });

                if (jQuery.isEmptyObject(selectedPriviliges)) {
                    toastr.error('অনুমতিপ্রাপ্ত ব্যক্তিবর্গ দেওয়া হয়নি।');
                    Metronic.unblockUI('.portlet-body');
                    return false;
                }
                if (jQuery.isEmptyObject(nothi_masters)) {
                    toastr.error(' কোন নথি বাছাই করা হয় নি।');
                    Metronic.unblockUI('.portlet-body');
                    return false;
                }

                var master_data = {};
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                    "timeOut": 0
                };
                var perRequestValue = Math.ceil(100 / Object.keys(nothi_masters).length);
                var newValue = 0;
                var flag = 0;
                $.each(nothi_masters, function(master_id, master_values) {
	                master_data[master_id] = master_values;
                    $.ajax({
                        url: '<?= $this->Url->build(['controller' => 'NothiRegisters', 'action' => 'nothiPermissionAddNew']) ?>',
                        type: 'POST',
                        data: {master_data: master_data, permitted: selectedPriviliges},
                        success: function (data) {
                            flag++;
                            if (data == 'success') {
                                newValue = newValue + perRequestValue;
                                if (newValue >= 100) {
                                    newValue = 100;
                                }
                                Metronic.blockUI({target: '.portlet-body', boxed: true, message: '<br/>অনুমতি প্রদান প্রক্রিয়া চলছে... '+enTobn(newValue)+'%'});
                                if (flag == Object.keys(nothi_masters).length) {
                                    if (newValue == 100) {
                                        toastr.success('সংরক্ষণ করা হয়েছে');
                                        Metronic.unblockUI('.portlet-body');
                                        $(".checker:checked").trigger('click');
                                        $(".checkthis:checked").trigger('click');

                                        $(".checkerNothi").prop('checked', false).uniform();
                                        $(".checkAllNothi").prop('checked', false).uniform();
                                    } else {
                                        toastr.error(enTobn(newValue)+'% তথ্য সংরক্ষণ করা সম্ভব হয়েছে। কিছুক্ষণ পর আবার চেষ্টা করুন');
                                        Metronic.unblockUI('.portlet-body');
                                    }
                                }
                            } else {

                            }
                        }
                    });
                    master_data = {};
                });

                //$.ajax({
                //    url: '<?//= $this->Url->build(['controller' => 'NothiRegisters', 'action' => 'nothiPermissionAdd']) ?>//',
                //    type: 'POST',
                //    data: {ids: nothi_masters, permitted: selectedPriviliges},
                //    success: function (data) {
                //        if (data == 'success') {
                //            toastr.success('সংরক্ষণ করা হয়েছে');
                //            Metronic.unblockUI('.portlet-body');
                //            $(".checker:checked").trigger('click');
                //            $(".checkthis:checked").trigger('click');
                //        } else {
                //            toastr.error('সংরক্ষণ করা সম্ভব হয়নি। কিছুক্ষণ পর আবার চেষ্টা করুন');
                //            Metronic.unblockUI('.portlet-body');
                //        }
                //    }
                //});
                //        console.log(group_name,group_type,selectedPriviliges);
            });
            function refresh_delete() {
                var bn = replaceNumbers("'" + ($('.deleterow').length) + "'");
                bn = bn.replace("'", '');
                bn = bn.replace("'", '');
                $('.totalgroupmember').text(bn);
                $(".deleterow").on("click", function () {
                    $(this).closest('tr').remove();
                    var bn = replaceNumbers("'" + ($('.deleterow').length) + "'");
                    bn = bn.replace("'", '');
                    bn = bn.replace("'", '');
                    $('.totalgroupmember').text(bn);
                });
            }
            function loadNote(nothiMasterId, element) {
                if (!$("#row_"+nothiMasterId).hasClass('in')) {
                    $(element).find('.glyphicon-chevron-down').addClass('glyphicon-chevron-up');
                    $(element).find('.glyphicon-chevron-down').removeClass('glyphicon-chevron-down');
                    if ($("#row_"+nothiMasterId).find('tr').length < 2) {
                        $.ajax({
                            type: 'GET',
                            url: "<?php echo $this->Url->build(['controller' => 'NothiRegisters', 'action' => 'showMasterPermissionList']) ?>/" + nothiMasterId + "/1",
                            success: function (data) {
                                if (data.status == 'success') {
                                    var disabled = '';
                                    if ($("input[type=checkbox].checkerNothi[value="+nothiMasterId+"]").prop('checked')) {
                                        disabled = 'disabled';
                                    }
                                    $("#row_" + nothiMasterId).find('table').html('<tr><th onclick="checkAll(this)"><input type="checkbox" '+disabled+' class="checkAll form-control"></th><th> নোট নং </th><th> নোটের বিষয় </th><th> অনুমতিপ্রাপ্তগণের তালিকা </th></tr>');
                                    $.each(data.data, function (key, value) {
                                        $("#row_" + nothiMasterId).find('table').append('<tr>' +
                                            '<td><input type="checkbox" class="checker form-control" '+disabled+' value="' + value.id + '"></td>' +
                                            '<td>' + (value.nothi_part_no_bn) + '</td>' +
                                            '<td>' + (value.subject) + '</td>' +
                                            '<td><button type="button" class="btn btn-sm btn-primary round-corner-5" onclick="loadPermissionUsers(' + nothiMasterId + ', ' + value.id + ')"> অনুমতিপ্রাপ্তগণ </button></td>' +
                                            '</tr>');
                                    });
                                }
                                $(".checker").uniform();
                                $(".checkAll").uniform();
                            }
                        });
                    }
                } else {
                    $(element).find('.glyphicon-chevron-up').addClass('glyphicon-chevron-down');
                    $(element).find('.glyphicon-chevron-up').removeClass('glyphicon-chevron-up');
                }
            }
            function loadPermissionUsers(nothiMasterId, nothiPartId) {
                $("#myModal").modal('show');
                $('#loadPrivilage').html('<img src="<?= CDN_PATH ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp; লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');
                $.ajax({
                    type: 'POST',
                    url: "<?php echo $this->Url->build(['controller' => 'NothiRegisters', 'action' => 'showMasterPermissionList']) ?>",
                    data: {'part': nothiPartId,master: nothiMasterId},
                    success: function (data) {
                        if (data.status == 'success') {
                            var html = '';
                            $.each(data.data, function (i, v) {
                                html += '<li class="list-group-item">' + v + '</li>';
                            });
                            $("#loadPrivilage").html(html);
                        }
                    }
                });
            }

	        function searchNothi(content) {
                $.ajax({
                    url: '<?= $this->Url->build(['controller' => 'NothiRegisters', 'action' => 'nothiPermissionAdd']) ?>',
                    type: 'GET',
                    data: {subject:content},
                    success: function (data) {
                        $("#MasterList").html("");
                        $.each(data, function (nothiMasterId, nothiSubject) {
                            $("#MasterList").append('<div data-toggle="collapse" data-parent="#MasterList" data-target="#row_'+nothiMasterId+'" onclick="loadNote('+nothiMasterId+', this)" aria-expanded="false" style="cursor: pointer;" class="collapsed">' +
                                '<div id="'+nothiMasterId+'" style="padding: 10px;background-color:#dff0d8;border:solid 1px #bdceb7;"><strong>নথির বিষয়:</strong> '+nothiSubject+'<div class="pull-right"><i class="glyphicon glyphicon-chevron-down"></i></div> </div>' +
                                '</div>' +
                                '<div class="collapse" id="row_'+nothiMasterId+'"><table class="table table-striped table-hover table-bordered"><tr><td colspan="4">লোড হচ্ছে...</td></tr></table></div>');
                        })
                    }
                });
	        }

	        function checkAll(element) {
                if ($(element).find('.checkAll').is(':checked')) {
                    $(element).closest('tbody').find('input[type=checkbox].checker').prop('checked', true).uniform('refresh');
                } else {
                    $(element).closest('tbody').find('input[type=checkbox].checker').prop('checked', false).uniform('refresh')
                }
            }
            function checkAllNothi(element) {
                if ($(element).find('.checkAllNothi').is(':checked')) {
                    $(element).parent().find('input[type=checkbox].checkerNothi').prop('checked', true).uniform('refresh');
                } else {
                    $(element).parent().find('input[type=checkbox].checkerNothi').prop('checked', false).uniform('refresh')
                }

                $('input[type=checkbox].checkerNothi').each(function (k, v) {
                    var masterId = $(v).val();
                    if ($(v).prop('checked')) {
                        $("#row_"+masterId).find('input.checkAll').prop('disabled', true).prop('checked', false).uniform('refresh');
                        $("#row_"+masterId).find('input.checker').prop('disabled', true).prop('checked', false).uniform('refresh');
                    } else {
                        $("#row_"+masterId).find('input.checkAll').prop('disabled', false).prop('checked', false).uniform('refresh');
                        $("#row_"+masterId).find('input.checker').prop('disabled', false).prop('checked', false).uniform('refresh');
                    }
                })
            }
            $(function() {
                $('.checkerNothi').on('change', function(){
                    var masterId = $(this).val();
                    if ($(this).prop('checked')) {
                        $("#row_"+masterId).find('input.checkAll').prop('disabled', true).prop('checked', false).uniform('refresh');
                        $("#row_"+masterId).find('input.checker').prop('disabled', true).prop('checked', false).uniform('refresh');
                    } else {
                        $("#row_"+masterId).find('input.checkAll').prop('disabled', false).prop('checked', false).uniform('refresh');
                        $("#row_"+masterId).find('input.checker').prop('disabled', false).prop('checked', false).uniform('refresh');
                    }
                });
            });
        </script>






