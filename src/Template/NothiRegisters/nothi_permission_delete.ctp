<div class="portlet light">
    <div class="portlet-title hidden-print">
        <div class="caption"><i class="fa fa-ban" aria-hidden="true"></i> <span
                class="title">নথি হতে অনুমতি প্রত্যাহার</span></div>
    </div>
    <div class="portlet-body">
        <div class="row">
            <?= $this->Cell('OfficeSelection'); ?>
            <div class="row">
                <div class="col-md-4 form-group form-horizontal">
                    <label class="control-label"> <?php echo __("Office") ?> </label>
                    <?php
                    echo $this->Form->input('office_id',
                        array(
                        'label' => false,
                        'class' => 'form-control',
                        'empty' => '----',
                        'options' => array($employee_office['office_id'] => $employee_office['office_name'])
                    ));
                    ?>
                </div>
                <div class="col-md-4 form-group form-horizontal">
                    <label class="control-label"> <?php echo __("Unit") ?> </label>
                    <?php
                    echo $this->Form->input('office_unit_id',
                        array(
                        'label' => false,
                        'class' => 'form-control',
                        'empty' => '----'
                    ));
                    ?>
                </div>
                <div class="col-md-4 form-group form-horizontal">
                    <label class="control-label"> <?php echo __("Organogra") ?> </label>
                    <?php
                    echo $this->Form->input('office_unit_organogram_id',
                        array(
                        'label' => false,
                        'class' => 'form-control',
                        'empty' => '----'
                    ));
                    ?>
                </div>
            </div>
            <div class="row text-center">
                <button class=" btn btn-danger round-corner-5" id="sub_grp_potrojari"> অনুমতি প্রত্যাহার করুন </button>
            </div>
            <hr>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="row">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-md-12" style="text-align: center;">
                                    যেসব নথিতে পদবির অনুমতি আছে
                                </div>
                            </div>
                        </div>
                        <div class="panel-body"  style="max-height: 500px; overflow: auto;">
	                        <div class="pull-right" style="margin-bottom: 5px;">
		                        <div class="input-group">
			                        <input type="text" class="form-control" name="searchNothi" placeholder="নথি খুঁজুন" onchange="searchNothi(this.value)" />
			                        <div class="input-group-btn">
				                        <button title="খুঁজুন" class="btn btn-success" type="submit" style="padding: 5px 14px;">
					                        <i class="glyphicon glyphicon-search"></i>
				                        </button>
			                        </div>
		                        </div>
	                        </div>

	                        <div id="MasterList"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	    <div id="myModal" class="modal fade modal-purple" role="dialog">
		    <div class="modal-dialog">

			    <!-- Modal content-->
			    <div class="modal-content">
				    <div class="modal-header">
					    <button type="button" data-dismiss="modal">&times;</button>
					    <h4 class="modal-title">অনুমতিপ্রাপ্তগণের তালিকা </h4>
				    </div>
				    <div class="modal-body">
					    <div class="" >
						    <ul class="list-group" id="loadPrivilage" style="max-height: calc(100vh - 195px); margin-bottom:0px; min-height:100px; overflow: auto;">

						    </ul>
					    </div>
				    </div>
				    <div class="modal-footer">
					    <button type="button" class="btn btn-danger" data-dismiss="modal"><?= __('Close') ?></button>
				    </div>
			    </div>

		    </div>
	    </div>
        <div id="myModal2" class="modal fade modal-purple" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">অনুমতিপ্রাপ্তগণের তালিকা </h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6 col-sm-6 form-group form-horizontal">
                                <label class="control-label"> নথির নাম </label>
                                <?php
                                echo $this->Form->hidden('mster', ['id' => 'mster']);
                                echo $this->Form->input('nothi_name',
                                    array(
                                    'label' => false,
                                    'class' => 'form-control',
                                    'type' => 'text',
                                    'readonly' => true
                                ));
                                ?>
                            </div>
                            <div class="col-md-7 col-sm-7 form-group form-horizontal">
                                <label class="control-label"> নোট নম্বর </label>
                                <?php
                                echo $this->Form->input('nothi_part_no',
                                    array(
                                    'label' => false,
                                    'class' => 'form-control',
                                    'empty' => '----',
                                    'type' => 'select',
                                ));
                                ?>
                            </div>
                        </div>
                        <hr>
                        <div class="" >
                            <ul class="list-group" id="loadPrivilage" style="max-height: calc(100vh - 332px); min-height:100px; overflow: auto;">

                            </ul>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal"><?= __('Close') ?></button>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<script>
    var initial = 0;
     function loadPrivilages() {
         if(initial == 0){
            $("#nothi-part-no").on('change', function () {
                var part = $("#nothi-part-no :selected").val();
                if (part == 0)
                    return;
                $('#loadPrivilage').html('<img src="<?= CDN_PATH ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp; লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');
                $.ajax({
                    type: 'POST',
                    url: "<?php echo $this->Url->build(['controller' => 'NothiRegisters', 'action' => 'showMasterPermissionList']) ?>",
                    data: {'part': part,master: $("#mster").val()},
                    success: function (data) {
                        if (data.status == 'success') {
                            var html = '';
                            $.each(data.data, function (i, v) {
                                html += '<li class="list-group-item">' + v + '</li>';
                            });
                            $("#loadPrivilage").html(html);
                        }
                    }
                });
            });
            }
         $("#nothi-part-no").trigger('change');
     }
    var EmployeeLoginHistory = {
        loadMinistryWiseLayers: function () {
            OfficeSetup.loadLayers($("#office-ministry-id").val(), function () {

            });
        },
        loadMinistryAndLayerWiseOfficeOrigin: function () {
            OfficeSetup.loadOffices($("#office-ministry-id").val(), $("#office-layer-id").val(), function () {

            });
        },
        loadOriginOffices: function () {
            OfficeSetup.loadOriginOffices($("#office-origin-id").val(), function () {

            });
        },
        loadOfficeUnits: function () {
            PROJAPOTI.ajaxSubmitDataCallback('<?php echo $this->request->webroot ?>' + 'officeManagement/loadOfficeUnits',
                    {'office_id': $("#office-id").val()}, 'html',
                    function (response) {
                        $("#office-unit-id").html(response);
                        $("#office-unit-id").select2();
                    });
        },
        loadOfficeUnitDesignations: function () {
            $("#office-unit-organogram-id").find('option').remove().end().append('<option value=""> লোডিং... </option>').val('');
            $("#office-unit-organogram-id").select2();
            PROJAPOTI.ajaxSubmitDataCallback('<?php echo $this->request->webroot ?>' + 'officeManagement/loadOfficeUnitOrganogramsWithName',
                    {'office_unit_id': $("#office-unit-id").val()}, 'json',
                    function (response) {
//                        PROJAPOTI.projapoti_dropdown_map("#office-unit-organogram-id", response, "--বাছাই করুন--");
//                        $("#office-unit-organogram-id").select2();
                        PROJAPOTI.projapoti_dropdown_map_with_title("#office-unit-organogram-id", response, "--বাছাই করুন--",'',{'key' : 'id','value' : 'designation','title' : 'name'  });
                    });
        },
        loadPermission: function () {
            Metronic.blockUI({target: '.portlet-body', boxed: true});
            $.ajax({
                type: 'POST',
                url: "<?php echo $this->Url->build(['controller' => 'NothiRegisters', 'action' => 'nothiPermissionDelete']) ?>",
                data: {'office_id': $("#office-id").val(), office_unit_id: $("#office-unit-id").val(), office_unit_organogram_id: $("#office-unit-organogram-id").val()},
                success: function (data) {
                    Metronic.unblockUI('.portlet-body');
                    $('#MasterList').html(data);
                },
                error: function(){
                    Metronic.unblockUI('.portlet-body');
                }
            });
        }
    };

    function searchNothi(content) {
        $.ajax({
            url: '<?= $this->Url->build(['controller' => 'NothiRegisters', 'action' => 'nothiPermissionDelete']) ?>',
            type: 'GET',
            data: {
                subject:content,
                office_id:$("#office-id").val(),
                office_unit_id:$("#office-unit-id").val(),
                office_unit_organogram_id:$("#office-unit-organogram-id").val(),
            },
            success: function (data) {
                $('#MasterList').html(data);
            }
        });
    }

     $(function() {
         $("#office-ministry-id").bind('change', function () {
             EmployeeLoginHistory.loadMinistryWiseLayers();
         });
         $("#office-layer-id").bind('change', function () {
             EmployeeLoginHistory.loadMinistryAndLayerWiseOfficeOrigin();
         });
         $("#office-origin-id").bind('change', function () {
             EmployeeLoginHistory.loadOriginOffices();
         });
         $("#office-id").bind('change', function () {
             EmployeeLoginHistory.loadOfficeUnits();
         });
         $("#office-unit-id").bind('change', function () {
             EmployeeLoginHistory.loadOfficeUnitDesignations();
         });
         $("#office-unit-organogram-id").bind('change', function () {
             EmployeeLoginHistory.loadPermission();
         });

         $("#sub_grp_potrojari").on('click', function () {
             Metronic.blockUI({target: '.portlet-body', boxed: true});
             var all_cheked = [];
             $(".checker:checked").each(function () {
                 all_cheked.push($(this).val());
             });
             if (all_cheked.length == 0) {
                 toastr.error(' কোন নথি বাছাই করা হয় নি।');
                 Metronic.unblockUI('.portlet-body');
                 return false;
             }
             if ($("#office-unit-organogram-id").val() == null || typeof ($("#office-unit-organogram-id").val()) == undefined || $("#office-unit-organogram-id").val() == '') {
                 toastr.error('পদবি বাছাই করা হয় নি।');
                 $("#office-unit-organogram-id").focus();
                 Metronic.unblockUI('.portlet-body');
                 return;
             }
             $.ajax({
                 type: 'POST',
                 url: "<?php echo $this->Url->build(['controller' => 'NothiRegisters', 'action' => 'nothiPermissionUpdate']) ?>",
                 data: {
                     'office_id': $("#office-id").val(),
                     office_unit_id: $("#office-unit-id").val(),
                     office_unit_organogram_id: $("#office-unit-organogram-id").val(),
                     'masters': all_cheked
                 },
                 success: function (data) {
                     if (data == 'success') {
                         toastr.success('অনুমতি প্রত্যাহার করা হয়েছে');
                         Metronic.unblockUI('.portlet-body');
                         window.location.reload();
                     } else {
                         toastr.error('অনুমতি প্রত্যাহার করা সম্ভব হয়নি। কিছুক্ষণ পর আবার চেষ্টা করুন');
                         Metronic.unblockUI('.portlet-body');
                     }
                 }
             });
         });

         var officeId = <?=$employee_office['office_id']?>;
         $("#office-id").val(officeId).trigger('change');
         $( document ).ajaxStop(function() {
             var officeUnitId = <?=$employee_office['office_unit_id']?>;
             if ($("#office-unit-id").val() == 0) {
                 if ($("#office-unit-id option[value=" + officeUnitId + "]").length == 0) {
                     if ($("#office-unit-id").val() != 0) {
                         $("#office-unit-id").val(0).trigger('change');
                     }
                 } else {
                     if ($("#office-unit-id").val() != officeUnitId) {
                         $("#office-unit-id").val(officeUnitId).trigger('change');
                     }
                 }
             }
         });
     });
    function showPermission(master) {
        Metronic.blockUI({target: '.portlet-body', boxed: true});
        $.ajax({
            type: 'GET',
            url: "<?php echo $this->Url->build(['controller' => 'NothiRegisters', 'action' => 'showMasterPermissionList']) ?>/" + master,
            success: function (data) {
                Metronic.unblockUI('.portlet-body');
                if (data.status == 'success') {
                    $('#nothi-part-no').val('');
                    $('#loadPrivilage').html('');
                    $('#nothi-name').val('');
                    $('#mster').val('');
                    $("#nothi-name").val($("#" + master).text());
                    $("#mster").val(master);
                    PROJAPOTI.projapoti_dropdown_map("#nothi-part-no", data.data, "--নোট নম্বর বাছাই করুন--");
                    $("#myModal").modal('show');
                    loadPrivilages();
                    initial++;

                }
            },
            error: function () {
                Metronic.unblockUI('.portlet-body');
            }
        });
    }
</script>