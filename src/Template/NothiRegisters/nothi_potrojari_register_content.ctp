<?php
$prev = "";
$count = 0;
if (!empty($data)) {
    if($version==2)
    {
    ?>
    <div class="table-container  table-scrollable">
        <table class="table table-bordered table-hover">
            <thead>
                <tr class="heading">
                <th class="text-center"  width="5%">ক্রমিক সংখ্যা</th>
                <th class="text-center"  width="10%">বিষয়</th>
                <th class="text-center"  width="20%">প্রেরক </th>
                <th class="text-center"  width="10%">স্মারক নম্বর</th>
                <th class="text-center"  width="10%"> প্রেরণের তারিখ  </th>
                <th class="text-center"  width="15%">প্রাপকসমূহ  </th>
                <th class="text-center"  width="15%">অনুলিপিসমূহ</th>
                <th class="text-center"  width="5%">পত্র ধরন</th>
            </tr>
            </thead>
            <tbody>
            <?php
			if ($this->Paginator->params()['page'] > 1) {
				$count = (($this->Paginator->params()['page']-1) * $this->Paginator->params()['perPage']);
			} else {
				$count = 0;
			}
            foreach ($data as $key => $value) {
                $unitInformation = h($value['office_unit_name']);
                $unitId = h($value['office_unit_id']);
                if ($prev != $unitId) {
                    $prev = $unitId;
                    $count=0;
                    ?>
                    <tr>
                        <th class="text-center" colspan="10"><?= $unitInformation ?><br><?= h($office_name['office_name_bng']) ?></th>
                    </tr>
                    <?php
                }
                $count++;
                ?>
                <tr>
                    <td class="text-center" style="vertical-align: middle"><?= $this->Number->format($count) ?></td>
                    <td class="text-center" style="vertical-align: middle"><?= h($value['potro_subject']) ?></td>
                    <td class="text-center" style="vertical-align: middle"><?=
                        h($value['officer_name']) . ',<br> ' .
                        h($value['officer_designation_label']) . ',<br> ' .
                        h($value['office_unit_name']) ?></td>
                    <td class="text-center" style="vertical-align: middle" onclick="showPotrojari('<?=$value['id']?>',
                            '<?= h($value['nothi_no']) ?>')"><u style="cursor: pointer;color: blue;"><?= h($value['nothi_no']) ?></u>
                    </td>
                    <td class="text-center" style="vertical-align: middle"><?= Cake\I18n\Time::parse(($value['potrojari_date'])) ?></td>

                    <td class="text-center" style="vertical-align: middle"><?php

                        if(isset($value['receiver_info'])):
                            foreach($value['receiver_info'] as $key => $info)
                            {
                                echo "<div style='line-height:15px;margin-top:5px'><b>";
                                echo $info['receiving_officer_name'];
                                echo "</b>";
                                echo "<br>";
                                echo $info['receiving_officer_designation_label'];
                                echo ",";
                                echo $info['receiving_office_unit_name'];
                                echo "<br>";
                                echo $info['receiving_office_name'];
                                echo "</div>";
                            }
                        endif;
                        ?>

                    </td>

                    <td class="text-center" style="vertical-align: middle"><?php

                                    if(isset($value['onulipi_info'])):
                                           foreach($value['onulipi_info'] as $key => $info_onulipi)
                                           {
                                               echo "<div style='line-height:15px;margin-top:5px'><b>";
                                               echo $info_onulipi['onulipi_officer_name'];
                                               echo "</b>";
                                               echo "<br>";
                                               echo $info_onulipi['onulipi_officer_designation_label'];
                                               echo ",";
                                               echo $info_onulipi['onulipi_office_unit_name'];
                                               echo "<br>";
                                               echo $info_onulipi['onulipi_office_name'];
                                               echo "</div>";
                                           }
                                   endif;
                                           ?>

                   </td>
                    <td>
                        <?php //echo h($value['potro_type']);?>
                        <?php echo h($value['type_name']);?>
                    </td>
                </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
    </div>
    <script>
        $("#content-export-button").css('display', 'initial');
        $("#report_pagination").css('display', 'block');
    </script>
    <?php
    }//version
    else{

        ?>
 <div class="table-container  table-scrollable">
        <table class="table table-bordered table-hover">
            <thead>
                <tr class="heading">
                <th class="text-center"  width="5%">ক্রমিক সংখ্যা</th>
                <th class="text-center"  width="10%">বিষয়</th>
                <th class="text-center"  width="20%">প্রেরক </th>
                <th class="text-center"  width="10%">স্মারক নম্বর</th>
                <th class="text-center"  width="10%"> প্রেরণের তারিখ  </th>
                <th class="text-center"  width="15%">প্রাপকসমূহ  </th>
                <th class="text-center"  width="15%">অনুলিপিসমূহ</th>
                <th class="text-center"  width="5%">পত্র ধরন</th>
            </tr>
            </thead>
            <tbody>
            <?php
			if ($this->Paginator->params()['page'] > 1) {
				$count = (($this->Paginator->params()['page']-1) * $this->Paginator->params()['perPage']);
			} else {
				$count = 0;
			}
            foreach ($data as $key => $value) {
                $unitInformation = h($value['office_unit_name']);
                $unitId = h($value['office_unit_id']);
                if ($prev != $unitId) {
                    $prev = $unitId;
                    $count=0;
                    ?>
                    <tr>
                        <th class="text-center" colspan="10"><?= $unitInformation ?><br><?= h($office_name['office_name_bng']) ?></th>
                    </tr>
                    <?php
                }
                $count++;

                ?>
                <tr>
                    <td class="text-center" style="vertical-align: middle"><?= $this->Number->format($count) ?></td>
                    <td class="text-center" style="vertical-align: middle"><?= h($value['potro_subject']) ?></td>
                    <td class="text-center" style="vertical-align: middle"><?= h($value['officer_designation_label']) . ', ' . h($value['office_unit_name']) ?></td>
                    <td class="text-center" style="vertical-align: middle" onclick="showPotrojari('<?=$value['id']?>', '<?= h($value['sarok_no']) ?>')"><u style="cursor: pointer;color: blue;"><?= h($value['sarok_no']) ?></u></td>
                    <td class="text-center" style="vertical-align: middle"><?= Cake\I18n\Time::parse(($value['potrojari_date'])) ?></td>
                    <td class="text-center" style="vertical-align: middle"><?= !empty($prapoks[$value['id']])?implode("<hr/>", $prapoks[$value['id']]):'' ?></td>
                    <td class="text-center" style="vertical-align: middle"><?= !empty($onulipis[$value['id']])?implode("<hr/>", $onulipis[$value['id']]):'' ?></td>
                    <td class="text-center" style="vertical-align: middle"><?= h($potroTypeInfo[$value['potro_type']]) ?></td>
                </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
    </div>
    <script>
        $("#content-export-button").css('display', 'initial');
        $("#report_pagination").css('display', 'block');
    </script>


        <?php
    }

} else {
    ?>
    <div class="alert alert-danger">
        দুঃখিত কোন তথ্য পাওয়া যায় নি।
    </div>
    <script>
        $("#content-export-button").css('display', 'none');
        $("#report_pagination").css('display', 'none');
    </script>
    <?php
}
?>
<?= customPagination($this->Paginator) ?>
<script>
    jQuery(document).ready(function () {
        $('.table').floatThead({
            position: 'absolute',
            top: jQuery("div.navbar-fixed-top").height()
        });
        $('.table').floatThead('reflow');
    });

    function showPotrojari(potrojariId, sarok_no) {
        doModal('show_potrojari', 'স্মারক নং '+sarok_no, '', '', '');
        $("#show_potrojari").find('.modal-body').load(js_wb_root + 'potrojari/show_potrojari/'+potrojariId);
    }

    $(document).ajaxStop(function() {
        $("#total_result_count").text('<?=enTobn($this->Paginator->params()['count'])?>');
    })
</script>

