<?php
$prev = "";
$count = 0;
if (!empty($datas)) {
    ?>
    <div class= "table-scrollable">
        <table class="table table-bordered table-hover">
            <thead>
            <tr class="heading">
                <th class="text-center" colspan="6"><?=isset($selected_office_section['office_name'])?($selected_office_section['office_name'].' - এর তৈরিকৃত নথি তালিকা '):''?><?= isset($startDate) && isset($endDate)? (' ('.entobn($startDate).' - '.entobn($endDate).' )' ):'' ?></th>
            </tr>
            <tr class="heading">
                <th class="text-center" rowspan="2">ক্রম</th>
                <th class="text-center" rowspan="2">শাখার নাম</th>
                <th class="text-center" colspan="2">চলমান নথি</th>
                <th class="text-center" rowspan="2">আর্কাইভ নথি</th>
                <th class="text-center" rowspan="2">মোট</th>
            </tr>
            <tr class="heading">
                <th class="text-center" >সক্রিয়</th>
                <th class="text-center" >নিষ্ক্রিয় </th>
            </tr>
            </thead>
            <tbody>
            <?php
            $total_running_nothi = 0;
            $total_active_nothi = 0;
            $total_inactive_nothi = 0;
            $total_archive_nothi = 0;
            $total_nothi = 0;
            foreach ($datas as $data) {
                $count++;
                $total_running_nothi += isset($data['running_nothi'])?$data['running_nothi']:0;
                $total_active_nothi += isset($data['active_nothi'])?$data['active_nothi']:0;
                $total_inactive_nothi += isset($data['inactive_nothi'])?$data['inactive_nothi']:0;
                $total_archive_nothi += isset($data['archive_nothi'])?$data['archive_nothi']:0;
                $total_nothi += isset($data['total_nothi'])?$data['total_nothi']:0;
                ?>
                <tr>
                    <td class="text-center"><?= enTobn($count) ?></td>
                    <td class="text-center"><?= $data['name'] ?></td>
                    <td class="text-center"><?= enTobn($data['active_nothi']) ?></td>
                    <td class="text-center"><?= enTobn($data['inactive_nothi']) ?></td>
                    <td class="text-center"><?= enTobn($data['archive_nothi']) ?></td>
                    <td class="text-center"><?= enTobn($data['total_nothi']) ?></td>

                </tr>
                <?php
            }
            ?>
            </tbody>
            <tfoot>
            <?php
            if(isset($total_nothi)){
                ?>
                <tr class="heading">
                    <td class="text-center bold" colspan="2"><?= __('Total') ?></td>
                    <td class="text-center bold"><?= enTobn($total_active_nothi) ?></td>
                    <td class="text-center bold"><?= enTobn($total_inactive_nothi) ?></td>
                    <td class="text-center bold"><?= enTobn($total_archive_nothi) ?></td>
                    <td class="text-center bold"><?= enTobn($total_nothi) ?></td>

                </tr>
                <?php
            }
            ?>
            </tfoot>
        </table>
    </div>
    <?php
}
if (empty($data)) {
    ?>
    <div class= "table-scrollable">
        <table class="table table-bordered table-striped table-hover">
            <thead>
            <tr class="heading">
                <th class="text-center" colspan="6"><?=isset($selected_office_section['office_name'])?($selected_office_section['office_name'].' - এর উপস্থাপিত নথি সংখ্যা'):''?><?= isset($startDate) && isset($endDate)? (' ('.entobn($startDate).' - '.entobn($endDate).' )' ):'' ?></th>
            </tr>
            <tr class="heading">
                <th class="text-center" rowspan="2">ক্রম</th>
                <th class="text-center" rowspan="2">শাখার নাম</th>
                <th class="text-center" colspan="2">চলমান নথি</th>
                <th class="text-center" rowspan="2">আর্কাইভ নথি</th>
                <th class="text-center" rowspan="2">মোট</th>
            </tr>
            <tr class="heading">
                <th class="text-center" >সক্রিয়</th>
                <th class="text-center" >নিষ্ক্রিয় </th>
            </tr>
            </thead>
            <tbody>

            <tr><td class="text-center" colspan="6" style="color:red;"> দুঃখিত কোন তথ্য পাওয়া যায় নি।  </td></tr>
            </tbody> </table> </div>
    <?php
}
?>