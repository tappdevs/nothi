<?php
$prev = "";
$count = 0;
if (!empty($data)) {
    ?>
    <div class="table-container  table-scrollable">
        <table class="table table-bordered table-hover">
            <thead>
                <tr class="heading">
                <th class="text-center" style="width: 5%;">ক্রমিক সংখ্যা</th>
                <!--<th class="text-center" style="width: 20%;">শাখার নাম</th>-->
                <th class="text-center" style="width: 10%;">নথির শ্রেণি</th>
                <th class="text-center" style="width: 15%;">নথির ধরন</th>
                <th class="text-center" style="width: 20%;">নথির শিরোনাম</th>
                <th class="text-center" style="width: 15%;">নথি নম্বর</th>
                <th class="text-center" style="width: 10%;">নথি খোলার তারিখ</th>
                <!--<th class="text-center" style="width: 5%;">গোপনীয়তা</th>-->
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($data as $key => $value) {
                $unitInformation = $value['office_units_id'];
                $unitId = $value['office_units_id'];
                if ($prev != $unitId) {
                    $prev = $unitId;
                    $count=0;
                    ?>
                    <tr>
                        <th class="text-center" colspan="6"><?= h($value['unit_name_bng']) ?><br><?= h($office_name['office_name_bng']) ?></th>
                    </tr>
                    <?php
                }
                $count++;

                ?>
                <tr>
                    <td class="text-center"><?= $this->Number->format($count) ?></td>
                    <td class="text-center"><?= h($nothiClass[$value['nothi_class']]) ?></td>
                    <td class="text-center"><?= h($value['NothiTypes']['type_name']) ?></td>
                    <td class="text-center"><?= h($value['subject']) ?> </td>
                    <td class="text-center"><?= h($value['nothi_no']) ?></td>
                    <td class="text-center"><?= Cake\I18n\Time::parse($value['nothi_created_date']) ?></td>
                </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
    </div>
    <script>
        $("#content-export-button").css('display', 'initial');
        $("#report_pagination").css('display', 'block');
    </script>
    <?php
} else {
    ?>
    <div class="alert alert-danger">
        দুঃখিত কোন তথ্য পাওয়া যায় নি।
    </div>
    <script>
        $("#content-export-button").css('display', 'none');
        $("#report_pagination").css('display', 'none');
    </script>
    <!--<div class="table-container  table-scrollable">
        <table class="table table-bordered table-striped">
            <tr class="heading">
                <th class="text-center" style="width: 5%;">ক্রমিক সংখ্যা</th>
                <th class="text-center" style="width: 10%;">নথির শ্রেণি</th>
                <th class="text-center" style="width: 15%;">নথির ধরন</th>
                <th class="text-center" style="width: 20%;">নথির শিরোনাম</th>
                <th class="text-center" style="width: 15%;">নথি নম্বর</th>
                <th class="text-center" style="width: 10%;">নথি খোলার তারিখ</th>
            </tr>
            <tr><td class="text-center" colspan="6" style="color:red;"> দুঃখিত কোন তথ্য পাওয়া যায় নি।  </td></tr>
        </table>
    </div>-->
    <?php
}
?>
<div class="actions text-center" id="report_pagination">
    <?= customPagination($this->Paginator) ?>

</div>
<script>
    jQuery(document).ready(function () {
        $('.table').floatThead({
            position: 'absolute',
            top: jQuery("div.navbar-fixed-top").height()
        });
        $('.table').floatThead('reflow');
    });

    $(document).ajaxStop(function() {
        $("#total_result_count").text('<?=enTobn($this->Paginator->params()['count'])?>');
    })
</script>










