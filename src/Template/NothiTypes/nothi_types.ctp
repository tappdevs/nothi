<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css"
      href="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>

<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            <i class="fs1 a2i_gn_nothi2"></i>ধরনের তালিকা
        </div>
        <div class="actions">
            <a href="<?php echo $this->request->webroot ?>NothiTypes/nothiTypesAdd" class="btn green btn-sm round-corner-5">
                <i class="fs1 a2i_gn_add1"></i> নতুন </a>
        </div>
    </div>
    <div class="portlet-body">
        <form action="" method="post">
            <div class="form-group">
                <div class="row">
                    <div class="col-md-4" style="padding:0px;margin-left:15px">
                        <input type="text" class="form-control form-control-sm" placeholder="নথির ধরন দিয়ে খুঁজুন" name="doron" id="doron" value="<?php echo $doron;?>">
                    </div>
                    <div class="col-md-3"  style="padding:0px;height:34px;">
                        <button type="submit" class="btn btn-success btn-sm" style="height:34px;width:100px">
                            <i class="fa fa-search"></i>
                            খুঁজুন
                        </button>

                        <button type="reseet" onclick="document.getElementById('doron').value=''" class="btn btn-success btn-sm" style="height:34px;width:100px">
                            <i class="fa fa-list"></i>
                            সকল ধরন
                        </button>

                    </div>
                </div>
            </div>
        </form>
        <table class="table table-striped table-bordered table-hover" id="datatable-nothitype">
            <thead>

            <tr>
                <th style="width: 10%"  class='text-center'>ক্রমিক নং
                </th>
                <th class='text-center' style="width: 50%">
                    নথির বিষয়ের ধরন
                </th>
                <th class='text-center' style="width: 10%">
                    নথির কোড
                </th>
                <th class='text-center' style="width: 10%">
                    নথির সংখ্যা
                </th>
<!--                <th class='text-center' style="width: 15%">
                    তারিখ
                </th>-->
                <th class='text-center' style="width: 14%">
                    কার্যক্রম
                </th>
            </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
</div>

<style>
    #datatable-nothitype_length select {margin-top:-5px;}
</style>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript"
        src="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript"
        src="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript"
        src="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script src="<?php echo CDN_PATH; ?>assets/global/scripts/datatable.js"></script>
<script src="<?php echo CDN_PATH; ?>projapoti-nothi/js/tbl_nothi_types.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo CDN_PATH; ?>assets/admin/pages/scripts/table-advanced.js"></script>


<script>
    function searchKeyup(value)
    {
        var searchDoron = value.value;
        var _changeInterval = null;
        if (searchDoron.length > 2) {
            clearInterval(_changeInterval);
            _changeInterval = setInterval(function() {
                // $("#searched_result").html('');

                $("#datatable-nothitype").DataTable().destroy();
                TableAjax.init('nothiTypes/nothiTypesList?doron='+searchDoron, 100);
                $('[title]').tooltip({container: 'body'});
                // $.ajax({
                //     url: js_wb_root+'nothiTypes/nothiTypesList',
                //     data: {
                //         doron: searchDoron
                //     },
                //     method: 'get',
                //     success: function (response) {
                //         // if (response.status == 'success') {
                //         //     // $.each(response.data, function (key, item) {
                //         //     //     $("#searched_result").append('<option value="'+item.subject+'"></option>');
                //         //     // })
                //         //
                //         // }
                //
                //         console.log(response);
                //     }
                // });
                clearInterval(_changeInterval)
            }, 500);
        }


    }

    $.extend(true, $.fn.dataTable.defaults, {
        "ordering": false
    });
      function deleteType(id){
          if(isEmpty(id)){
              toastr.error(' ধরনটি সঠিকভাবে নির্বাচন করা হয়নি। ');
              return false;
          }
          var link ="<?= $this->request->webroot ?>nothiTypes/nothiTypesDelete/"+id;
           bootbox.dialog({
                    message: "আপনি কি ধরনটি মুছে ফেলতে ইচ্ছুক?",
                    title: "ধরনটি মুছে ফেলুন",
                    buttons: {
                        success: {
                            label: "হ্যাঁ",
                            className: "green",
                            callback: function () {
                                window.location.href = link;
                            }
                        },
                        danger: {
                            label: "না",
                            className: "red",
                            callback: function () {
                            }
                        }
                    }
                });
        }


    function newNothiMasters(id){
        if(isEmpty(id)){
            toastr.error(' ধরনটি সঠিকভাবে নির্বাচন করা হয়নি। ');
            return false;
        }
        var link ="<?= $this->request->webroot ?>nothiMasters/add?type_id="+id;
        bootbox.dialog({
            message: "আপনি কি নতুন একটি নথি তৈরি করতে ইচ্ছুক?",
            title: "নতুন নথি তৈরি",
            buttons: {
                success: {
                    label: "হ্যাঁ",
                    className: "green",
                    callback: function () {
                        window.location.href = link;
                    }
                },
                danger: {
                    label: "না",
                    className: "red",
                    callback: function () {
                    }
                }
            }
        });
    }

    jQuery(document).ready(function () {


        TableAjax.init('nothiTypes/nothiTypesList?doron=<?php echo isset($doron)?$doron:'';?>', <?=$len?>);
        $('[title]').tooltip({container: 'body'});

    });
</script>