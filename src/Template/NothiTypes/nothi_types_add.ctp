<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-plus"></i>ধরন তৈরি
        </div>
         <div class="tools">
            <a  href="<?=
            $this->Url->build([
                'action' => 'nothiTypes'])
            ?>"><button class="btn purple btn-sm margin-bottom-5 round-corner-5"  > ধরনের তালিকা  </button></a>
        </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <?= $this->Form->create($nothiTypes_records, ['type' => 'post', 'class' => 'form-horizontal']); ?>
        <div class="form-body">
            <div class="form-group">
                <label class="col-md-3 control-label">বিষয়ের ধরন</label>

                <div class="col-md-7" style="padding-right: 0px;">
                    <div class="">
                        <?= $this->Form->input('type_name', ['label' => false, 'class' => 'form-control', 'type' => 'text', 'placeholder' => 'বিষয়ের ধরন','list'=>'nothi-types', 'autocomplete' => 'off']); ?>

                      <datalist id="nothi-types">
                        <option value="অডিট আপত্তি/অর্থ আত্মসাৎ/আর্থিক ক্ষতি">০১</option>
                        <option value="অর্থ/অগ্রিম">০২</option>
                        <option value="অনিষ্পন্ন বিষয়ের তালিকা প্রণয়ন">০৩</option>
                        <option value="আইনগত/মামলা পরিচালনা কার্যক্রম গ্রহণ সংক্রান্ত বিষয়াদি">০৪</option>
                        <option value="কর্মবণ্টন/কর্মপরিকল্পনা">০৫</option>
                        <option value="কার্যবিবরণী/মাসিক সমন্বয় সভা/বিশেষ সভা/নিয়মিত অন্যান্য সভা/কমিটি গঠন/কমিটির নিয়মিত সভা">০৬</option>
                        <option value="ক্রয় প্রক্রিয়াকরণ">০৭</option>
                        <option value="ছুটি">০৮</option>
                        <option value="জাতীয় সংসদে প্রশ্নোত্তর/মুলতবি প্রস্তাব/অন্যান্য সংসদ বিষয়ক কার্যক্রম">০৯</option>
                        <option value="টেলিফোন প্রাপ্যতা/সংযোগসংক্রান্ত যাবতীয় কার্যক্রম">১০</option>
                        <option value="নিয়োগসংক্রান্ত সকল কার্যক্রম">১১</option>
                        <option value="পদোন্নতিসংক্রান্ত যাবতীয় কার্যক্রম">১২</option>
                        <option value="পেনশনসংক্রান্ত যাবতীয় কার্যক্রম">১৩</option>
                        <option value="প্রকল্প প্রণয়ন/উন্নয়ন প্রকল্পসংক্রান্ত/প্রকল্প বাস্তবায়ন/প্রকল্প অনুমোদন">১৪</option>
                        <option value="প্রকল্প থেকে রাজস্ব খাতে স্থানান্তর/পদ সৃষ্টি/সংরক্ষণ/আত্তীকরণ/উদ্বৃত্ত ঘোষণা">১৫</option>
                        <option value="প্রতিবেদন প্রেরণ/সংরক্ষণ/সংগ্রহসংক্রান্ত">১৬</option>
                        <option value="প্রেসনোট/পেপার কাটিং-এর উপর কার্যক্রম/প্রতিবাদলিপি">১৭</option>
                        <option value="প্রশাসনিক কার্যক্রম/মন্ত্রণালয়ের অভ্যন্তরীণ অফিস আদেশ">১৮</option>
                        <option value="বদলি/পদায়ন/প্রেষণসংক্রান্ত সকল কার্যক্রম">১৯</option>
                        <option value="বাজেট প্রণয়ন/বাজেট বরাদ্দসংক্রান্ত কার্যক্রম">২০</option>
                        <option value="বার্ষিক গোপনীয় প্রতিবেদন প্রেরণ/সংরক্ষণ সংক্রান্ত কার্যক্রম">২১</option>
                        <option value="বিধি/আইন/নীতি প্রণয়ন, সংশোধন, ব্যাখ্যা, পরিমার্জন ও বিলুপ্তি">২২</option>
                        <option value="বিভিন্ন দিবস উদযাপন/পুরস্কার বিতরণ">২৩</option>
                        <option value="বৈদেশিক সাহায্য/বৈদেশিক যোগাযোগ/দাতা সংস্থার সহিত যোগাযোগ ইত্যাদি বিষয়াদি">২৪</option>
                        <option value="ভ্রমণ/প্রশিক্ষণ">২৫</option>
                        <option value="যানবাহন ক্রয়/রক্ষণাবেক্ষণ/জ্বালানি/মেরামত">২৬</option>
                        <option value="শৃঙ্খলা/অভিযোগ/তদন্ত/আপিল/রিভিউ সংক্রান্ত কার্যক্রম">২৭</option>
                        <option value="সাংগঠনিক কাঠামো অনুমোদন/অন্তর্ভুক্তকরণ/বিলুপ্তকরণ সংক্রান্ত যাবতীয় কার্যক্রম">২৮</option>
                        <option value="সেমিনার/ওয়ার্কসপ দেশের অভ্যন্তরে আয়োজন/ব্যবস্থাপনা">২৯</option>
                        <option value="স্টেশনারি সামগ্রীর চাহিদাপত্র/মালামাল সরবরাহ">৩০</option>
                        <option value="বিবিধ">৯৯</option>
                      </datalist>
                    </div>
                </div>
              <div class="col-md-2" style="padding-left: 5px;margin-top: 7px;">
                <a href="" style="text-decoration: none;" title="সচিবালয়ের নির্দেশমালা" data-target="#SecretariatGuidelinesModalPage73" data-toggle="modal"><i class="efile-help4"></i></a>
              </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">কোড</label>

                <div class="col-md-7" style="padding-right: 0px;">
                    <div class="">
                        <?= $this->Form->input('type_code', ['label' => false, 'class' => 'form-control', 'type' => 'text', 'placeholder' => '২ ডিজিটের ধরন কোড','maxlength' => 2,'minlength' => 2]); ?>
                    </div>
                </div>
            </div>
            <div class="form-group hidden">
                <label class="col-md-3 control-label">নথির সর্বশেষ নম্বর</label>

                <div class="col-md-4">
                    <div class="input-group">
                       
                        <?= $this->Form->input('type_last_number', ['label' => false, 'class' => 'form-control input-circle', 'type' => 'text', 'placeholder' => 'নথির সর্বশেষ নাম্বার']); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-3 col-md-9">
                    <button type="submit" class="btn blue round-corner-5">সংরক্ষণ করুন</button>
                </div>
            </div>
        </div>
        <?= $this->Form->input('office_id', ['label' => false, 'type' => 'hidden', 'value' => $office_id]); ?>
        <?= $this->Form->input('office_unit_id', ['label' => false, 'type' => 'hidden', 'value' => $office_unit_id]); ?>
        <?= $this->Form->end(); ?>
    </div>
</div>

<div id="SecretariatGuidelinesModalPage73" class="modal fade modal-purple" data-backdrop="static" role="dialog">
	<div class="modal-dialog" style="width: 99%;">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">সচিবালয়ের নির্দেশমালা</h4>
			</div>
			<div class="modal-body text-center">
				<img src="<?= $this->request->webroot ?>img/bangladesh_secretariat_page73.png" alt="Image" style="width:100%">
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default red round-corner-5" data-dismiss="modal">বন্ধ করুন</button>
			</div>
		</div>

	</div>
</div>
<script>
  $("#type-name").on('input', function () {
    var val = this.value;
    $('#nothi-types option').filter(function () {
      if (this.value === val){
        $("#type-code").val($(this).text());
      }
    })
  });
</script>