<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
            নথি   ধরনসমূহ
        </div>
    </div>
    <div class="portlet-body">
        <?= $this->Cell('OfficeSelection'); ?>
        <div class="row">
            <div class="col-md-4 form-group form-horizontal">
                <label class="control-label"> <?php echo __("Office") ?> </label>
                <?php
                echo $this->Form->input('office_id',
                    array(
                    'label' => false,
                    'class' => 'form-control',
                    'empty' => '----'
                ));
                ?>
            </div>
            <div class="col-md-4 form-group form-horizontal">
                <label class="control-label"> <?php echo __("Unit") ?> </label>
                <?php
                echo $this->Form->input('office_unit_id',
                    array(
                    'label' => false,
                    'class' => 'form-control',
                    'empty' => '----'
                ));
                ?>
            </div>
        </div>
        <hr>
        <div class="row" >
            <table class="table table-striped table-bordered table-hover" id="datatable-nothitype">
                <thead>

                    <tr>
                        <th style="width: 10%"  class='text-center'>ক্রমিক নং
                        </th>
                        <th class='text-center' style="width: 50%">
                            নথির বিষয়ের ধরন
                        </th>
                        <th class='text-center' style="width: 10%">
                            নথির কোড
                        </th>
                        <th class='text-center' style="width: 10%">
                            নথির সর্বশেষ নম্বর
                        </th>
                        <th class='text-center' style="width: 10%">
                            নথির সংখ্যা
                        </th>
        <!--                <th class='text-center' style="width: 15%">
                            তারিখ
                        </th>-->
                        <th class='text-center' style="width: 10%">
                            কার্যক্রম
                        </th>
                    </tr>
                </thead>
                <tbody id="showlist">

                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    var EmployeeAssignment = {
        checked_node_ids: [],
        loadMinistryWiseLayers: function () {
            OfficeSetup.loadLayers($("#office-ministry-id").val(), function (response) {

            });
        },
        loadMinistryAndLayerWiseOfficeOrigin: function () {
            OfficeSetup.loadOffices($("#office-ministry-id").val(), $("#office-layer-id").val(), function (response) {

            });
        },
        loadOriginOffices: function () {
            OfficeSetup.loadOriginOffices($("#office-origin-id").val(), function (response) {

            });
        },
        loadOfficeUnits: function () {
            OfficeSetup.loadOfficeUnits($("#office-id").val(), function (response) {

            });
        },
    };

    $(function () {

        if ($("#office-unit-id").val() > 0) {
            getNotification($("#office-unit-id").val());
        }

        $("#office-ministry-id").bind('change', function () {
            EmployeeAssignment.loadMinistryWiseLayers();
        });
        $("#office-layer-id").bind('change', function () {
            EmployeeAssignment.loadMinistryAndLayerWiseOfficeOrigin();
        });
        $("#office-origin-id").bind('change', function () {
            EmployeeAssignment.loadOriginOffices();
        });
        $("#office-id").bind('change', function () {
            EmployeeAssignment.loadOfficeUnits();
        });
        $("#office-unit-id").bind('change', function () {
            getNotification($(this).val());
        });
        function getNotification(unit_id) {
            $("#datatable-nothitype").DataTable().destroy();
            TableAjax.init('nothiTypes/officeNothiTypes/' + $("#office-id").val() + '/' + unit_id);
//            $('#showlist').html('<img src="<?= CDN_PATH ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp; লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');
//            $.ajax({
//                type: 'POST',
//                url: "<?php //echo $this->Url->build(['controller' => 'NothiTypes', 'action' => 'officeNothiTypes'])   ?>",
//                data: {"office_id": office_id},
//                success: function (data) {
//                    $('#showlist').html('');
//                    $('#showlist').html(data);
//                }
//            });
        }
    });
</script>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript"
src="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript"
src="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript"
src="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script src="<?php echo CDN_PATH; ?>assets/global/scripts/datatable.js"></script>
<script src="<?php echo CDN_PATH; ?>projapoti-nothi/js/tbl_nothi_types.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo CDN_PATH; ?>assets/admin/pages/scripts/table-advanced.js"></script>


<script>

    jQuery(document).ready(function () {
        $.extend(true, $.fn.dataTable.defaults, {
            "ordering": false
        });
    });
</script>
