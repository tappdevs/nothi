<div class="portlet light">
    <div class="portlet-title">
        <div class="caption"><i class="fs1 a2i_gn_details1"></i><?php echo __("Template Message List"); ?></div>

    </div>
    <div class="portlet-body">
        <div class="table-toolbar">
            <div class="row">
                <div class="col-md-10">
                    <div class="btn-group">
                        <ul class="inbox-nav nav nav-pills">
                            <li class="">
                                <a href="/Nothi/notificationMessages/incomingMessages" class="btn btn-sm btn-primary"> <i class="fs1 a2i_nt_dakmanagement4"></i>Incoming Messages</a>
                            </li>
                            <li class="draft">
                                <a href="/Nothi/notificationMessages/outgoingMessages" class="btn btn-sm green"> <i class="fs1 a2i_nt_dakdraft4"></i>Outgoing Messages</a>
                            </li>
                            
                            <li class="">
                                Date Range:<input type="text" style="width: 100px;" id="datepicker_from"> to <input type="text" style="width: 100px;" id="datepicker_to">
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="col-md-2 text-center">
                    <h4> Message List </h4>
                </div>
            </div>
        </div>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th class="text-center"><?php echo __("ID"); ?></th>
                    <th class="text-center"><?php echo __("Title (Bangla)"); ?></th>
                    <th class="text-center"><?php echo __("Title (English)"); ?></th>
                    <th class="text-center"><?php echo __("Media"); ?></th>
                    <th class="text-center"><?php echo __("Template"); ?></th>
                    <th class="text-center"><?php echo __("Status"); ?></th>
                    <th class="text-center"><?php echo __("Actions"); ?></th>
                </tr>
            </thead>
            <tbody>

                <?php
                foreach ($query as $rows):
                    ?>
                    <tr>
                        <td class="text-center"><?php echo $rows['id']; ?></td>
                        <td class="text-center"><?php echo $rows['title_bng']; ?></td>
                        <td class="text-center"><?php echo $rows['title_eng']; ?></td>
                        <td class="text-center"><?php echo $rows['media']; ?></td>
                        <td class="text-center"><?php echo $rows['template_id']; ?></td>
                        <td class="text-center"><?php echo $rows['status']; ?></td>
                        <td class="text-center">
                            <?=
                            $this->Form->postLink(
                                    'Delete', ['action' => 'delete', $rows->id], ['class' => 'btn btn-danger'], ['confirm' => 'Are you sure to delete this item?'])
                            ?>
                            <?= $this->Html->link(__('Edit'), ['action' => 'edit', $rows->id], ['class' => 'btn btn-primary']) ?>
                            <?= $this->Html->link('View', ['action' => 'view', $rows->id], ['class' => 'btn green']) ?>

                        </td>
                        <?php
                    endforeach;
                    ?>
                </tr>
            </tbody>
        </table>
    </div>
</div>

  <script>
  $(function() {

    $( "#datepicker_from" ).datepicker();
    $( "#datepicker_to" ).datepicker();
    
    $( "#datepicker_to" ).change(function(){
        if( ($("#datepicker_to").val() != "") && ($("#datepicker_from").val() != "") ){
            
            var start_date = "";
            var end_date = "";
            
            start_date = $("#datepicker_from").val().replace(/\//g, "-");
            end_date = $("#datepicker_to").val().replace(/\//g, "-");

            window.location.href = "/Nothi/notificationMessages/index/"+start_date+"/"+end_date;
            
        }
    });
  });
  </script>
