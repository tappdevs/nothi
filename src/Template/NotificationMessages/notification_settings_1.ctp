<?php
$session = $this->request->session();
$modules = $session->read('modules');
$selected_module = $session->read('module_id');
?>

<!-- BEGIN PAGE HEAD -->

<div class="portlet box purple">
    <div class="portlet-title">
        <div class="caption">
            নোটিফিকেশন
        </div>
    </div>
    <div class="portlet-body">
        <div class="tabbable-custom nav-justified">
            <!--            <ul class="nav nav-tabs nav-justified">
                            <li class="active">
                                <a href="#tab_1_1_1" data-toggle="tab">
                                    ডাক
                                </a>
                            </li>
                            <li>
                                <a href="#tab_1_1_2" data-toggle="tab">
                                    নথি </a>
                            </li>
                            <li>
                                <a href="#tab_1_1_3" data-toggle="tab">
                                    নোটিফিকেশন </a>
                            </li>
                        </ul>-->
            <div class="tab-content">
                <form id="notification_settings" method="post" onsubmit="return false;">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <!-- BEGIN PORTLET-->
                            <h3>ডাক</h3>
                            <table class="table table-striped">
                                <tr>
                                    <td></td>
                                    <td>নোটিফিকেশন বার</td>
                                    <td>ইমেইল</td>
                                    <td>এস এম এস</td>
                                </tr>

                                <?php
                                if (!empty($eventTabs[1])) {
                                    foreach ($eventTabs[1] as $key => $value):
                                        ?>
                                        <tr>
                                            <td><?php echo $value['event_name_bng'] ?></td>
                                            <td><?php echo $this->Form->input('system_' . $value['id'], ['type' => 'checkbox', 'label' => false, 'class' => 'make-switch', 'data-on-text' => 'Yes', 'data-off-text' => 'No', 'value' => 1, 'checked'=>(!empty($existingSetting[$value['id']]['system'])?(true):(false))]); ?></td>
                                            <td><?php echo $this->Form->input('email_' . $value['id'], ['type' => 'checkbox', 'label' => false, 'class' => 'make-switch', 'data-on-text' => 'Yes', 'data-off-text' => 'No', 'value' => 1, 'checked' => (!empty($existingSetting[$value['id']]['email'])?(true):(false))]); ?></td>
                                            <td><?php echo $this->Form->input('sms_' . $value['id'], ['type' => 'checkbox', 'label' => false, 'class' => 'make-switch', 'data-on-text' => 'Yes', 'data-off-text' => 'No', 'value' => 1, 'checked'   => (!empty($existingSetting[$value['id']]['sms'])?(true):(false))]); ?></td>
                                        </tr>
                                        <?php
                                    endforeach;
                                }

                                if (!empty($eventTabs[2])) {
                                    foreach ($eventTabs[2] as $key => $value):
                                        ?>
                                        <tr>
                                            <td><?php echo $value['event_name_bng'] ?></td>
                                             <td><?php echo $this->Form->input('system_' . $value['id'], ['type' => 'checkbox', 'label' => false, 'class' => 'make-switch', 'data-on-text' => 'Yes', 'data-off-text' => 'No', 'value' => 1, 'checked'=>(!empty($existingSetting[$value['id']]['system'])?(true):(false))]); ?></td>
                                            <td><?php echo $this->Form->input('email_' . $value['id'], ['type' => 'checkbox', 'label' => false, 'class' => 'make-switch', 'data-on-text' => 'Yes', 'data-off-text' => 'No', 'value' => 1, 'checked' => (!empty($existingSetting[$value['id']]['email'])?(true):(false))]); ?></td>
                                            <td><?php echo $this->Form->input('sms_' . $value['id'], ['type' => 'checkbox', 'label' => false, 'class' => 'make-switch', 'data-on-text' => 'Yes', 'data-off-text' => 'No', 'value' => 1, 'checked'   => (!empty($existingSetting[$value['id']]['sms'])?(true):(false))]); ?></td>
                                        </tr>

                                        <?php
                                    endforeach;
                                }
                                ?>
                            </table>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-4 col-sm-offset-4 col-xs-offset-4 col-md-8 col-sm-8 col-xs-8">
                                <button type="submit" class="btn blue btn-settings"><i class="fa fa-check"></i> Submit</button>
                            </div>
                        </div>
                    </div>
                    <?php echo $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).on('click', '.btn-settings', function () {

        $.ajax({
            url: '<?php echo $this->Url->build(['controller' => 'NotificationMessages', 'action' => 'saveSettings']) ?>',
            data: $('#notification_settings').serialize(),
            type: 'post',
            dataType: 'json',
            cache: false,
            success: function (response) {
                if(response.status == 'success'){
                    toastr.success(response.msg);
                }else{
                    toastr.error(response.msg);
                }
            }
        })

    })
</script>