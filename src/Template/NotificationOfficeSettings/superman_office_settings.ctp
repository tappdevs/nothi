<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
            <?= __('Office').__('Notification').__('Setting') ?>
        </div>
    </div>
    <div class="portlet-body">
        <?= $this->Cell('OfficeSelection'); ?>
        <div class="row">
            <div class="col-md-4 form-group form-horizontal">
                <label class="control-label"> <?php echo __("Office") ?> </label>
                <?php
                echo $this->Form->input('office_id',
                    array(
                    'label' => false,
                    'class' => 'form-control',
                    'empty' => '----'
                ));
                ?>
            </div>
        </div>
        <div class="row" id="showlist">
            
        </div>
    </div>
</div>
<script>
    var EmployeeAssignment = {
        checked_node_ids: [],
        loadMinistryWiseLayers: function () {
            OfficeSetup.loadLayers($("#office-ministry-id").val(), function (response) {

            });
        },
        loadMinistryAndLayerWiseOfficeOrigin: function () {
            OfficeSetup.loadOffices($("#office-ministry-id").val(), $("#office-layer-id").val(), function (response) {

            });
        },
        loadOriginOffices: function () {
            OfficeSetup.loadOriginOffices($("#office-origin-id").val(), function (response) {

            });
        },
    };

    $(function () {

        if ($("#office-id").val() > 0) {
            getNotification($("#office-id").val());
        }

        $("#office-ministry-id").bind('change', function () {
            EmployeeAssignment.loadMinistryWiseLayers();
        });
        $("#office-layer-id").bind('change', function () {
            EmployeeAssignment.loadMinistryAndLayerWiseOfficeOrigin();
        });
        $("#office-origin-id").bind('change', function () {
            EmployeeAssignment.loadOriginOffices();
        });
        $("#office-id").bind('change', function () {
            getNotification($(this).val());

        });
        function getNotification(office_id) {
             $('#showlist').html('<img src="<?= CDN_PATH?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp; লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');
            $.ajax({
                type: 'POST',
                url: "<?php echo $this->Url->build(['controller' => 'NotificationMessages', 'action' => 'officeNotificationSettings']) ?>",
                data: {"office_id": office_id},
                success: function (data) {
                     $('#showlist').html('');
                    $('#showlist').html(data);
                }
            });
        }
    });
</script>
