<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption"><i class=""></i><?php echo __("View Template"); ?></div>
        <div class="tools">
            <a href="javascript:" class="collapse"></a>
            <a href="#portlet-config" data-toggle="modal" class="config"></a>
            <a href="javascript:" class="reload"></a>
            <a href="javascript:" class="remove"></a>
        </div>
    </div>
    <div class="portlet-body">

        <div class="panel-body">
            <table class="table table-bordered">

                <tr>
                    <td class="text-right"><?php echo __("Title (Bangla)"); ?></td>
                    <td><?= h($template_message->title_bng) ?></td>
                </tr>
                <tr>
                    <td class="text-right"><?php echo __("Title (English)"); ?></td>
                    <td><?= h($template_message->title_eng) ?></td>
                </tr>
                
                <tr>
                    <td class="text-right"><?php echo __("Template (Bangla)"); ?></td>
                    <td><?= h($template_message->template_message_bng) ?></td>
                </tr>
                <tr>
                    <td class="text-right"><?php echo __("Template (English)"); ?></td>
                    <td><?= h($template_message->template_message_eng) ?></td>
                </tr>
                <tr>
                    <td class="text-right"><?php echo __("Status"); ?></td>
                    <td><?= h($template_message->status) ?></td>
                </tr>

                <tr>
                    <td><?= $this->Html->link('Edit This Temlate', ['action' => 'edit', $template_message->id], array('class' => 'btn btn-primary pull-right')) ?></td>
                    <td><?= $this->Html->link('Back to Template List', ['action' => 'index'], array('class' => 'btn btn-primary pull-left')) ?></td>
                </tr>

            </table>
        </div>
    </div>
</div>
