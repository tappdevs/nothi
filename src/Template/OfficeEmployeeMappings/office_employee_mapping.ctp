<?php $office_organizations = array();
$selected_org_id = 0;
?>
<?= $cell = $this->cell('OfficeHierarchyTemplates'); ?>
<div id="office_organization_div">
    <!-- // -->
</div>
<div id="office_mapping_div">
    <div class="row">
        <div class="col-md-6" id="tree_div"></div>
        <div class="col-md-6" id="employee_div"></div>
    </div>
    <hr/>
    <div class="form-actions">
        <div class="row text-center">
            <button type="button" onclick="OfficeEmployeeMapping.save()" class="btn   blue ajax_submit">
                Assign
            </button>
        </div>
    </div>
</div>

<script type="text/javascript">
    var OfficeEmployeeMapping =
    {
        checked_nodes: [],
        loadOfficeOrganizations: function () {
            $("#office_organization_div").hide('slow');
            $("#office_mapping_div").hide('slow');

            OfficeEmployeeMapping.getSelectedOfficeHiararchyId();

            var office_hierarchy_id = $("#office-hierarchy-id").val();
            var office_hierarchy_name = $("#office-hierarchy").val();

            PROJAPOTI.ajaxLoadCallback('<?= $this->request->webroot ?>officeEmployeeMappings/loadOffices?ofc_hie_id=' + office_hierarchy_id + '&&ofc_hie_name=' + office_hierarchy_name + '&&parent_script_variable=OfficeEmployeeMapping', function (html_response) {
                $("#office_organization_div").show('slow');
                $("#office_organization_div").html(html_response);
            });
        },
        getSelectedOfficeHiararchyId: function () {
            var id = "";
            var selected_office_text = $("#office-hierarchy").val();
            <?php
                foreach($office_hierarchy_list as $key=>$ohl){
            ?>
            if (selected_office_text == '<?= $ohl;?>') {
                id = '<?= $key?>';
            }
            <?php
             }
             ?>
            $("#office-hierarchy-id").val(id);
        },
        getSelectedOrganizationId: function (offce_org_list) {
            var id = "";
            var selected_office_text = $("#office-organization").val();
            var office_list = offce_org_list;
            //var office_list = $.parseJSON(offce_org_list);
            $.each(office_list, function (key, value) {
                if (value == selected_office_text) {
                    id = key;

                }
            });
            $("#office-organization-id").val(id);
        },
        loadOfficeOrganograms: function (offce_org_list) {
            $("#office_mapping_div").hide('slow');

            OfficeEmployeeMapping.getSelectedOrganizationId(offce_org_list);

            var office_org_id = $("#office-organization-id").val();
            var office_org_name = $("#office-organization").val();

            PROJAPOTI.ajaxLoadCallback('<?= $this->request->webroot ?>officeEmployeeMappings/loadOfficeOrganogram?office_org_id=' + office_org_id + '&&office_org_name=' + office_org_name, function (html_response) {
                $("#office_mapping_div").show('slow');
                $("#tree_div").html(html_response);
                OfficeEmployeeMapping.loadTree();
                PROJAPOTI.ajaxLoadCallback('<?= $this->request->webroot ?>officeEmployeeMappings/loadOfficeEmployees', function (html_response) {
                    $("#employee_div").html(html_response);
                });

            });
        },
        loadTree: function () {
            $('#orgngrm_tree').jstree({
                "core": {
                    "themes": {
                        "variant": "large",
                        "theme": "classic",
                        "dots": true,
                    },
                    'data': {
                        'url': function (node) {
                            return node.id === '#' ?
                                '<?php echo $this->request->webroot?>officeOrganograms/loadOfficeOrganogramData' : '<?php echo $this->request->webroot?>officeOrganograms/loadOfficeOrganogramData';
                        },
                        'data': function (node) {
                            return {
                                'type': 'checkbox',
                                'id': node.id,
                                'office_organization_id': $("#office-organization-id").val()
                            };
                        }
                    }
                },
                "checkbox": {
                    "keep_selected_style": false,
                    "three_state": false
                },
                "plugins": ["checkbox"]
            });

            OfficeEmployeeMapping.checked_nodes = $("#orgngrm_tree").jstree('get_checked');
        },
        save: function () {
            var office_organization_id = $("#office-organization-id").val();
            var office_organogram_id = "";
            var office_employee_id = $('input:radio[name=office_employee]:checked').val();

            $.each($("#orgngrm_tree").jstree("get_checked", true), function () {
                office_organogram_id = this.id;
            });

            var data = {
                'office_organization_id': office_organization_id,
                'office_organogram_id': office_organogram_id,
                'office_employee_id': office_employee_id
            };

            PROJAPOTI.ajaxSubmitDataCallback('<?= $this->request->webroot ?>officeEmployeeMappings/save', data, function (json_response) {
                if (json_response == 1) {
                    alert("Success !");
                    location.reload();
                }
                else {
                    alert("Failed to assign !");
                }
            });
        }
    };
    $(function () {
        $('.office_hierarchy_btn').bind('click', function () {
            OfficeEmployeeMapping.loadOfficeOrganizations()
        });
        $("#office-hierarchy").bind('input', function () {
            $("#office_organization_div").hide('slow');
            $("#office_mapping_div").hide('slow');
        });
        $("#office_mapping_div").hide();
        $('.office_org_btn').bind('click', function () {
            OfficeEmployeeMapping.loadOfficeOrganograms()
        });
    });
</script>
