<div class="row">
    <div class="col-md-12">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    দপ্তর সেকশনের তালিকা
                </div>
            </div>
            <div class="portlet-body">
                <div class="text-center font-lg alert alert-info">
                    পদবি স্তর শূন্য অথবা ফাঁকা হলে ডিফল্টভাবে '৯৯৯' বসবে।
                </div>
                <table class="table table-striped table-bordered table-hover" id="sample_1">
                    <tr class="heading">
                        <th></th>
                        <th class="text-center">পদবি</th>
                        <th class="text-center">পদবি স্তর</th>
                        <th class="text-center">পদবি ক্রম</th>
                    </tr>
                    <tbody>
                    <tr class="odd gradeX">
                        <?php
                        $i = 0;
                        foreach ($allOrganogram as $rows):
                        ?>
                    <tr class="designationLabel">
                        <td class="text-center"><?php echo $this->Number->format(++$i) ?></td>
                        <td class="designation_id" id="<?php echo $rows['id'] ?>"><?php echo $rows['designation_bng'] . ', <b>' . $rows['OfficeUnits']['unit_name_bng'] . "</b>"; ?></td>
                       
                        <td class="text-center"><input  type="number" class="form-control text-center" required="required" maxlength="3" name="designation_level" value="<?= !empty($rows['designation_level'])?$rows['designation_level']:999 ?>"/></td>
                        <td class="text-center"><input  class="form-control text-center" type="text" name="designation_sequence" value="<?php echo $rows['designation_sequence']; ?>"/></td>
                        
                        <?php
                        endforeach;
                        ?>
                    </tr>
                    <tr class="form-actions">
                        <td colspan="4" class="text-center">
                            <input type="button" class="btn green saveDesignation" value="<?php echo __(SAVE); ?>" />
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<script>
    
    
    $(document).on('click','.saveDesignation', function(){
        var i =0;
        Metronic.blockUI({
            target: '.portlet.box.green',
            boxed: true
        });

        var data = [];
        var prom = new Promise(function(resolve,reject){
            data.push($('.designationLabel').map(function(){
                return {
                    id:$(this).find('.designation_id').attr('id'),
                    designation_level: $(this).find('[name=designation_level]').val(),
                    designation_sequence: $(this).find('[name=designation_sequence]').val()
                };
            }));

            if(data.length>0){
                resolve(data);
            }else{
                reject("Error");
            }

        }).then(function (data) {
            updateLevel(0,Object.values(data)[0],Object.values(data)[0].length);
        }).catch(function(err){
            toastr.error(err);
            Metronic.unblockUI('.portlet.box.green');
        });
    });

    function updateLevel(i,data, len){

        if(i>=len){
            toastr.success("সংরক্ষিত হয়েছে");
            Metronic.unblockUI('.portlet.box.green');
            return;
        }

        $.ajax({
            url: '<?php echo $this->Url->build(['controller'=>'officeEmployees','action'=>'officeEmployeesDesignationUpdate']) ?>',
            data: data[i],
            method: 'post',
            success: function(res){
                if(res == 1){
                    $('.designationLabel').eq(i).addClass('success');
                }else{
                    $('.designationLabel').eq(i).addClass('danger');
                }
                updateLevel(++i,data,len);
            }
        })

    }
    
 </script>