<div class="row">
    <div class="col-md-12" >
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fs1 a2i_gn_edit2"></i>পদবিসমূহ
                </div>
            </div>
            <div class="portlet-body">
                <?= $this->Cell('OfficeSelection'); ?>
                <div class="row">
                    <div class="col-md-4 form-group form-horizontal">
                        <label class="control-label"> <?php echo __("Office") ?> </label>
                        <?php
                        echo $this->Form->input('office_id',
                            array(
                            'label' => false,
                            'class' => 'form-control',
                            'empty' => '----',
                        ));
                        ?>
                    </div>
                    <div class="col-md-4 form-group form-horizontal">
                        <label class="control-label"> <?php echo __("Unit") ?> </label>
                        <?php
                            echo $this->Form->input('office_unit_id',
                                array(
                                'label' => false,
                                'class' => 'form-control',
                                'empty' => '----',
                            ));
                        ?>
                    </div>
                </div>
                <hr>
                <div class="well text-center">
                    <div class=" form-inline">
                        <div class="form-group">
                            <?php echo $this->Form->input('search_designation', ['label' => false, 'class' => 'form-control input-medium', 'placeholder' => ' পদবি খুঁজুন বাংলা অথবা ইংরেজিতে ', 'default' => !empty($search_text) ? $search_text : '']); ?>
                        </div>
                        <div class="form-group">
                            <?php echo $this->Form->input('search_by_name', ['label' => false, 'class' => 'form-control input-medium', 'placeholder' => ' পদবি খুঁজুন নাম দিয়ে ', 'default' => !empty($search_by_name) ? $search_by_name : '']); ?>
                        </div>
                        <div class="form-group">
                            <button id="searchDesignation" type="button" class="btn btn-primary"> খুঁজুন</button>
                            <button id="refresh" type="button" class="btn btn-danger"> রিসেট </button>
                        </div>
                    </div>
                </div>
                <div id="sample_1">
                </div>
                <div class="modal fade" id="yourModal" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">পদবি সংশোধন</h4>
                            </div>
                            <div class="modal-body">
                                <form class="form-group">
                                    <label class="form-cntrol">পদবির নাম ( বাংলা )</label>
                                    <input type="text" class="form-control" id="update_text_bng">
                                </form>
                                <form class="form-group">
                                    <label class="form-cntrol">পদবির নাম ( ইংরেজি )</label>
                                    <input type="text" class="form-control" id="update_text_eng">
                                    <input type="hidden" class="form-control" id="update_id">
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-success" onclick="update_action();">সংরক্ষণ</button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal">বন্ধ করুন</button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo CDN_PATH; ?>projapoti-nothi/js/designation_name_update.js?v=<?= time() ?>" type="text/javascript"></script>