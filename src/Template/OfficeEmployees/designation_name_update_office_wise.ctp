<div class="row">
    <div class="col-md-12" >
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fs1 a2i_gn_edit2"></i>পদবিসমূহ
                </div>
            </div>
            <div class="portlet-body">
                <div class="well text-center">
                    <div class=" form-inline">
                        <form action="?" method="get">
                            <div class="form-group">
                                <?php echo $this->Form->input('search', ['label' => false, 'class' => 'form-control input-medium', 'placeholder' => ' পদবি খুঁজুন বাংলা অথবা ইংরেজিতে ', 'default' => !empty($search_text) ? $search_text : '']); ?>
                            </div>
                            <div class="form-group">
                                <?php echo $this->Form->input('search_by_name', ['label' => false, 'class' => 'form-control input-medium', 'placeholder' => ' পদবি খুঁজুন নাম দিয়ে ', 'default' => !empty($search_by_name) ? $search_by_name : '']); ?>
                            </div>
                            <div class="form-group">
                                <?php echo $this->Form->select('unit_id', ['0' => __('All')]+$unit_id_list, ['label' => false, 'class' => 'form-control input-medium', 'placeholder' => ' পদবি খুঁজুন নাম দিয়ে ', 'default' => !empty($unit_id) ? $unit_id : '']); ?>
                            </div>
                            <div class="form-group">
                                <input type="submit" id="searchDesignation" class="btn btn-primary" value="খুঁজুন" />
                                <a href="<?= $this->request->webroot; ?>officeEmployees/designationNameUpdateOfficeWise" class="btn btn-danger"> রিসেট </a>
                            </div>
                        </form>
                    </div>
                </div>
                <div id="sample_1">
                    <table class="table table-striped table-bordered table-hover" >
                        <tr class="heading">
                            <th rowspan="2"></th>
                            <th class="text-center" colspan="2">পদবির নাম </th>
                            <th class="text-center" rowspan="2"> শাখা</th>
                            <th class="text-center" rowspan="2">অফিস </th>
                            <th class="text-center" rowspan="2">কার্যক্রম </th>
                        </tr>
                        <tr class="heading">
                            <th  class="text-center"> বাংলা </th>
                            <th  class="text-center"> ইংরেজি</th>

                        </tr>

                        <tbody class="designationLabel" id="designationList">
                        <?php
                        $i = $page;
                        foreach ($allOrganogram as $rows):

                            $office_name = Cake\ORM\TableRegistry::get('Offices')->getBanglaName($rows['office']);
                            $unit_name   = Cake\ORM\TableRegistry::get('OfficeUnits')->getBanglaName($rows['unit']);
                            ?>
                            <tr >
                                <td class="text-center"><?php echo $this->Number->format($i++) ?></td>
                                <td class="text-center " id = "<?= "designation_bn".$rows['id'] ?>">
                                    <input type="hidden" id="designation_bn_<?=$rows['id']?>" value="<?= $rows['d_bng'] ?>">
                                    <?php echo $rows['d_bng'] . (!empty($rows['EmployeeRecords']['name_bng'])?(' (' . h($rows['EmployeeRecords']['name_bng']) . ')'):''); ?>
                                </td>
                                <td class="text-center" id = "<?= "designation_en".$rows['id'] ?>">
                                    <input type="hidden" id="designation_en_<?=$rows['id']?>" value="<?= $rows['d_eng'] ?>">
                                    <?php echo $rows['d_eng'] . (!empty($rows['EmployeeRecords']['name_eng'])?(' (' . h($rows['EmployeeRecords']['name_eng']) . ')'):''); ?>
                                </td>
                                <td class="text-center" ><?php echo $unit_name['unit_name_bng']; ?></td>
                                <td class="text-center" ><?php echo $office_name['office_name_bng']; ?></td>
                                <td class="text-center">
                                    <a class="btn   btn-primary" data-toggle="modal" data-target="#yourModal" onclick="update(<?= $rows['id'] ?>);"><?= __('Edit') ?></a>
                                </td>
                            </tr>
                        <?php
                        endforeach;
                        ?>

                        </tbody>
                    </table>

                    <div class="actions text-center">
                        <ul class="pagination pagination-sm">
                            <?php
                            echo $this->Paginator->first(__('প্রথম', true),
                                array('class' => 'number-first'));

                            echo $this->Paginator->prev('<<',
                                array('tag' => 'li', 'escape' => false),
                                '<a href="#" class="btn btn-sm blue ">&raquo;</a>',
                                array('class' => 'next disabled', 'tag' => 'li', 'escape' => false));
                            echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true,
                                'currentClass' => 'active', 'currentTag' => 'a', 'reverse' => false));
                            echo $this->Paginator->next('>>',
                                array('tag' => 'li', 'escape' => false),
                                '<a href="#" class="btn btn-sm blue">&laquo;</a>',
                                array('class' => 'prev disabled btn btn-sm blue', 'tag' => 'li', 'escape' => false));
                            echo $this->Paginator->last(__('শেষ', true),
                                array('class' => 'number-last'));
                            ?>
                        </ul>

                    </div>
                </div>
                <div class="modal fade" id="yourModal" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">পদবি সংশোধন</h4>
                            </div>
                            <div class="modal-body">
                                <form class="form-group">
                                    <label class="form-cntrol">পদবির নাম ( বাংলা )</label>
                                    <input type="text" class="form-control" id="update_text_bng">
                                </form>
                                <form class="form-group">
                                    <label class="form-cntrol">পদবির নাম ( ইংরেজি )</label>
                                    <input type="text" class="form-control" id="update_text_eng">
                                    <input type="hidden" class="form-control" id="update_id">
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-success" onclick="update_action();">সংরক্ষণ</button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal">বন্ধ করুন</button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function getNotification() {
        PROJAPOTI.ajaxLoadWithRequestData(js_wb_root + 'officeEmployees/designationNameUpdateOfficeWise', {}, '#designationList');
    }
    function update(id)
    {
        var bn_text = $('#designation_bn_' + id + '').val();
        var en_text = $('#designation_en_' + id + '').val();
        $('#update_text_bng').val(bn_text);
        $('#update_text_eng').val(en_text);
        $('#update_id').val(id);
    }
    function update_action()
    {
        var bn_text = $('#update_text_bng').val();
        var en_text = $('#update_text_eng').val();
        var id = $('#update_id').val();
        if (bn_text == '' || typeof (bn_text) == 'undefined' || bn_text == 'undefined' || bn_text == null) {
            toastr.error('পদবির নাম খালি রাখা যাবে না।');
            return;
        }
        $.ajax({
            type: 'POST',
            url: js_wb_root +'OfficeEmployees/designationNameSaveOfficeWise',
            data: {"bn_text": bn_text, "en_text": en_text, "id": id},
            success: function (data) {
                if (data.status == 'success') {
					toastr.success(data.msg);
                	if(data.redirect == true){
                		window.location.href = '<?= $this->Url->build('/logout') ?>';
                    }else {
						getNotification();
						$("#yourModal").modal('toggle');
					}
                } else {
                    toastr.error(data.msg);
                }
            }
        });
    }
</script>