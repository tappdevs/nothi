<div class="row">
    <div class="col-md-12" >
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fs1 a2i_gn_edit2"></i>শাখাসমূহ
                </div>
            </div>
            <div class="portlet-body">
                <?= $this->Cell('OfficeSelection'); ?>
                <div class="row">
                    <div class="col-md-4 form-group form-horizontal">
                        <label class="control-label"> <?php echo __("Office") ?> </label>
                        <?php
                        echo $this->Form->input('office_id',
                            array(
                            'label' => false,
                            'class' => 'form-control',
                            'empty' => '----',
                        ));
                        ?>
                    </div>
                </div>
                <hr>
                <div id="sample_1">
                </div>
                <div class="modal fade" id="yourModal" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">শাখা সংশোধন</h4>
                            </div>
                            <div class="modal-body">
                                <form class="form-group">
                                    <label class="form-cntrol">শাখা নাম ( বাংলা )</label>
                                    <input type="text" class="form-control" id="update_text_bng">
                                </form>
                                <form class="form-group">
                                    <label class="form-cntrol">শাখা নাম ( ইংরেজি )</label>
                                    <input type="text" class="form-control" id="update_text_eng">
                                    <input type="hidden" class="form-control" id="update_id">
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-success" onclick="update_action();">সংরক্ষণ</button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal">বন্ধ করুন</button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo CDN_PATH; ?>projapoti-nothi/js/edit_units_info.js?v=<?= js_css_version ?>" type="text/javascript"></script>

