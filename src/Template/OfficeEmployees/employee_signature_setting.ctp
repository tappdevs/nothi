<div class="row">
    <div class="col-md-12">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fs1 a2i_gn_edit2"></i>ব্যবহারকারীর তালিকা
                </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="sample_1">
                    <tr class="heading">
                        <th class="text-center" rowspan="3"></th>
                        <th class="text-center" rowspan="3">নাম</th>
                        <th class="text-center" rowspan="3"> কর্মকর্তা তথ্য </th>
                        <th class="text-center" colspan="3">সিগনেচার </th>
                    </tr>
                    <tr class="heading">
                        <th class="text-center" rowspan="2"> ইলেক্ট্রনিক </th>
                        <th class="text-center" colspan="2"> ডিজিটাল </th>
                      
                    </tr>
                    <tr class="heading">
                          <th class="text-center"> সফট টোকেন </th>
                          <th class="text-center"> হার্ড টোকেন </th>
                    </tr>
                    <tbody>
                    <tr class="odd gradeX">
                        <?php
                        $i = 0;
                        foreach ($data as $rows):
                        ?>
                    <tr class="designationLabel">
                        <td class="text-center"><?php echo $this->Number->format(++$i) ?></td>
                        <td class="text-center"><?php echo $rows['name_bng'] ?></td>
                        <td class="designation_id" id="<?php echo $rows['employee_record_id'] ?>"><?php echo $rows['designation'] ?></td>
                        <td class="text-center">
                            <input type="checkbox" data-designation="<?= $rows['employee_record_id'] ?>" class="form-check-input signature" checked disabled>
                        </td>
                        <td class="text-center"><input type="checkbox" name="soft_signature_<?= $rows['employee_record_id'] ?>" data-designation="<?= $rows['employee_record_id'] ?>" class="form-control soft_signature" <?=(($rows['soft_signature'] == 1) ? 'checked' : '')?> ></td>
                        <td class="text-center"><input type="checkbox" name="hard_signature_<?= $rows['employee_record_id'] ?>" data-designation="<?= $rows['employee_record_id'] ?>" class="form-control hard_signature" <?=(($rows['hard_signature'] == 1) ? 'checked' : '')?> ></td>

                        <?php
                        endforeach;
                        ?>
                    </tr>
                    <tr class="form-actions">
                        <td colspan="6" class="text-center">
                            <input type="button" class="btn green saveSignature" value="<?php echo __(SAVE); ?>" />
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<script>


    $(document).on('click','.saveSignature', function(){
        var i =0;
        Metronic.blockUI({
            target: '.portlet.box.green',
            boxed: true
        });
//$('#soft_signature_76761').is(':checked');
        var data = [];
        var prom = new Promise(function(resolve,reject){
            data.push($('.designationLabel').map(function(){
                return {
                    id:$(this).find('.designation_id').attr('id'),
                    soft_signature: $('[name=soft_signature_'+$(this).find('.designation_id').attr('id')+']').is(':checked'),
                    hard_signature: $('[name=hard_signature_'+$(this).find('.designation_id').attr('id')+']').is(':checked')
                };
            }));

            if(data.length>0){
                resolve(data);
            }else{
                reject("Error");
            }

        }).then(function (data) {
            updateLevel(0,Object.values(data)[0],Object.values(data)[0].length);
        }).catch(function(err){
            toastr.error(err);
            Metronic.unblockUI('.portlet.box.green');
        });
    });

    function updateLevel(i,data, len){

        if(i>=len){
            toastr.success("সংরক্ষিত হয়েছে");
            Metronic.unblockUI('.portlet.box.green');
            return;
        }

        $.ajax({
            url: '<?php echo $this->Url->build(['controller'=>'officeEmployees','action'=>'employeeSignatureSetting']) ?>',
            data: data[i],
            method: 'post',
            success: function(res){
                if(res.status == 'success'){
                    $('.designationLabel').eq(i).addClass('success');
                }else{
                    $('.designationLabel').eq(i).addClass('danger');
                    toastr.error(res.msg);
                }
                updateLevel(++i,data,len);
            }
        })

    }

 </script>