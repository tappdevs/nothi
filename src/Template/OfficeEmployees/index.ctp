<div class="row">
    <div class="col-md-12">
        <div class="portlet box purple">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fs1 a2i_gn_edit2"></i>দপ্তর কর্মকর্তার তালিকা
                </div>
                <div class="tools  hidden-print">
                    <a style="color:white; font-size: 16px"  href="<?php
                    echo $this->Url->build(['controller' => 'EmployeeRecords',
                        'action' => 'ownOfficeAdd'])
                    ?>"  title="দপ্তর কর্মকর্তা তৈরি">
                        <i class="fs a2i_gn_add1"></i>  </a>
                    <a  title="দপ্তর কর্মকর্তা তালিকা প্রিন্ট" style="color:white; font-size: 16px"  class="btn-print">
                        <i class="fs a2i_gn_print2"></i>  </a>
                    <a  title="দপ্তর কর্মকর্তা তালিকা এক্সপোর্ট" style="color:white; font-size: 16px" href="<?= $this->Url->build(['?'=>['print'=>1],'']) ?>">
                        <i class="fa fa-file-excel-o fa-2x"></i>  </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h4 class="panel-title"><?= __("Search") ?></h4>
                    </div>
                    <div class="panel-body">
                        <?php echo $this->Form->create($this->request,['action'=>'index','type'=>'post']) ?>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-3">
                                    <?php
                                    echo $this->Form->input('name_bng',
                                        array(
                                        'label' => __('Name').' '.__('Bangla'),
                                        'class' => 'form-control',
                                        'empty' => '----',
                                        'placeholder'=>'নাম লিখুন বাংলায়'
                                    ));
                                    ?>
                                </div>
                                <div class="col-sm-3">
                                    <?php
                                    echo $this->Form->input('username',
                                        array(
                                        'label' => __('Login ID'),
                                        'class' => 'form-control',
                                        'placeholder'=>'লগইন আইডি'
                                    ));
                                    ?>
                                </div>
                                <div class="col-sm-3">
                                    <?php
                                    echo $this->Form->input('personal_mobile',
                                        array(
                                        'label' => __('মোবাইল'),
                                        'class' => 'form-control',
                                        'placeholder'=>'মোবাইল'
                                    ));
                                    ?>
                                </div>
                                <div class="col-sm-3">
                                    <?php
                                    echo $this->Form->input('national_id',
                                        array(
                                            'label' => __('NID'),
                                            'class' => 'form-control', 'type' => 'text',
                                            'placeholder'=>'জাতীয় পরিচয়পত্র'
                                        ));
                                    ?>
                                </div>
                            </div>
                            <br/>
                            <div class="form-actions">
                                <?php echo $this->Form->button(__("Search"),
                                    ['type' => 'submit', 'class' => 'btn btn-sm btn-primary '])
                                ?>
                            </div>
                        </div>

                    <?php echo $this->Form->end() ?>
                    </div>
                </div>
                <?= $this->Paginator->counter(['format' => '<b>মোট: {{count}} জন</b>']) ?>
                <div class="table-scrollable">
                    <table class="table table-striped table-bordered " id="sample_1" style="width: 100%;">
                    <thead>
                        <tr class="heading">
                            <th class="text-center" style="width: 4%;">ক্রম</th>
                            <th class="text-center" style="width: 8%;">ছবি</th>

                            <th  class="text-center" style="width: 11%;"><?php
                                echo __('Name')
                                ?>
                            </th>
                            <th class="text-center" style="width: 7%;">মোবাইল</th>
                            <th class="text-center" style="width: 9%;">ইমেইল</th>
                            <th class="text-center" style="width: 16%;">পদবি</th>
                            <th class="text-center" style="width: 19%;">সেকশন</th>
                            <th class="text-center" style="width: 9%;">ইউজার আইডি</th>
                            <th class="text-center" style="width: 9%;">অবস্থা</th>
                            <th class="text-center" style="width: 9%;">স্বাক্ষর</th>
                            <th class="text-center" style="width: 9%;">ছুটিপ্রাপ্ত কর্মকর্তা</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php
                        $i = 0;
                        foreach ($results as $key=>$rows):
                            ?>
                            <tr class="<?php echo $rows['active']==1 ? "" : "danger"; ?>">
                                <td class="text-center" style="word-wrap: break-word;"><?= Cake\I18n\Number::format((($page-1)*$limit) + $key + 1) ?></td>
                                <td class="text-center" style="word-wrap: break-word;"><img src="<?php echo $this->Url->build(['_name' => 'getPhoto', $rows['username']])
                                ?>" style="width: 64px; " class="img-responsive" /></td>
                                <td class="" style="word-wrap: break-word;"><?php echo !empty($rows['protikolpo'])?(h($rows['name_bng']).$rows['protikolpo']):(h($rows['name_bng']));?></td>
                                <td style="word-wrap: break-word;"><?php echo h($rows['personal_mobile']); ?></td>
                                <td style="word-wrap: break-word;"><?php echo h($rows['personal_email']); ?></td>
                                <td style="word-wrap: break-word;"><?php echo h($rows['designation']); ?></td>
                                <td style="word-wrap: break-word;"><?php echo h($rows['unit_name_bng']); ?></td>
                                <td style="word-wrap: break-word;"><?php echo h($rows['username']); ?></td>
                                <td style="word-wrap: break-word;"><?php echo $rows['active']==1 ? "সক্রিয়" : "নিষ্ক্রিয়"; ?></td>
                                <td class="text-center" style="word-wrap: break-word;">
                                    <?php
                                    $url =$this->Url->build(['_name'=>'getSignature',$rows['username'],0,'?'=>['token'=>sGenerateToken(['file'=>$rows['username']],['exp'=>time() + 60*300])]], true);
                                    ?>
                                    <img src="<?= $url?>" style="height:48px; " class="img-responsive" /></td>
                                <td style="word-wrap: break-word;"><?php echo !empty($rows['main_employee_name'])?h($rows['main_employee_name']):''; ?></td>
                            </tr>
    <?php
endforeach;
?>

                    </tbody>
                </table>
                </div>
                <div class="actions text-center">
                    <ul class="pagination pagination-sm">
                        <?php
                        echo $this->Paginator->first(__('প্রথম', true),
                            array('class' => 'number-first'));
                        echo $this->Paginator->prev('<<',
                            array('tag' => 'li', 'escape' => false),
                            '<a href="#" class="btn btn-sm blue ">&raquo;</a>',
                            array('class' => 'next disabled', 'tag' => 'li', 'escape' => false));
                        echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li',
                            'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a',
                            'reverse' => FALSE, 'modulus' => 10));
                        echo $this->Paginator->next('>>',
                            array('tag' => 'li', 'escape' => false),
                            '<a href="#" class="btn btn-sm blue">&laquo;</a>',
                            array('class' => 'prev disabled btn btn-sm blue', 'tag' => 'li',
                            'escape' => false));

                        echo $this->Paginator->last(__('শেষ', true),
                            array('class' => 'number-last'));
                        ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $this->Html->script('assets/global/scripts/printThis.js'); ?>
<script>
    $(document).on('click', '.btn-print', function () {
        $('#sample_1').printThis({
            importCSS: true,
            debug: false,
            importStyle: true,
            printContainer: true,
            pageTitle: "",
            removeInline: false,
            header: null
        });
    });

    $(function(){
        $('[title]').tooltip({'placement':'bottom'});
    })
</script>