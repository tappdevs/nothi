<div class="row">
    <div class="col-md-12">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fs1 a2i_gn_edit2"></i><?= __("Office Employees List") ?>
                </div>
                <div class="tools">
                    <a href="javascript:" class="collapse"></a>
                    <a href="#portlet-config" data-toggle="modal" class="config"></a>
                    <a href="javascript:" class="reload"></a>
                    <a href="javascript:;" class="remove"></a>
                </div>
            </div>

            <div class="portlet-body">
                <?= $cell = $this->cell('OfficeSelection', ['entity' => '']) ?>
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo __("Office"); ?></label>

                        <div class="col-sm-6">
                            <?php
                            echo $this->Form->input('office_id', array(
                                'label' => false,
                                'class' => 'form-control',
                                'empty' => '----'
                            ));
                            ?>
                        </div>
                    </div>
                </div>
                <hr/>
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a data-toggle="tab" href="#tab_1_1" aria-expanded="true">
                            <?= __("Active") ?> </a>
                    </li>
                    <li class="">
                        <a data-toggle="tab" href="#tab_1_2" aria-expanded="false">
                            <?= __("Past") ?> </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div id="tab_1_1" class="tab-pane fade active in">
                        <div class="table-scrollable">
                            <table id="datatable_office_employees_active"
                                   class="table table-striped table-bordered table-hover dataTable no-footer"
                                   aria-describedby="datatable_office_employees_info" role="grid">
                                <thead>
                                <tr class="heading" role="row">
                                    <th width="20%" class="sorting_asc" tabindex="0"
                                        aria-controls="datatable_office_employees_active" rowspan="1" colspan="1">
                                        <?= __("Name") ?>
                                    </th>
                                    <th width="20%" class="sorting" tabindex="0"
                                        aria-controls="datatable_office_employees_active" rowspan="1" colspan="1">
                                        <?= __("Designations") ?>
                                    </th>
                                    <th width="20%" class="sorting" tabindex="0"
                                        aria-controls="datatable_office_employees_active" rowspan="1" colspan="1">
                                        <?= __("Office") ?>
                                    </th>
                                    <th width="5%" class="sorting" tabindex="0"
                                        aria-controls="datatable_office_employees_active" rowspan="1" colspan="1">
                                        <?= __("Start Date") ?>
                                    </th>
                                    <th width="5%" class="sorting" tabindex="0"
                                        aria-controls="datatable_office_employees_active" rowspan="1" colspan="1">
                                        <?= __("Release Date") ?>
                                    </th>
                                    <th width="30%" class="sorting" tabindex="0"
                                        aria-controls="datatable_office_employees_active" rowspan="1" colspan="1">
                                        <?= __("Actions") ?>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div id="tab_1_2" class="tab-pane fade">
                        <div class="table-scrollable">
                            <table id="datatable_office_employees_inactive"
                                   class="table table-striped table-bordered table-hover dataTable no-footer"
                                   aria-describedby="datatable_office_employees_info" role="grid">
                                <thead>
                                <tr class="heading" role="row">
                                    <th width="20%" class="sorting_asc" tabindex="0"
                                        aria-controls="datatable_office_employees_inactive" rowspan="1" colspan="1">
                                        <?= __("Name") ?>
                                    </th>
                                    <th width="20%" class="sorting" tabindex="0"
                                        aria-controls="datatable_office_employees_inactive" rowspan="1" colspan="1">
                                        <?= __("Designations") ?>
                                    </th>
                                    <th width="20%" class="sorting" tabindex="0"
                                        aria-controls="datatable_office_employees_inactive" rowspan="1" colspan="1">
                                        <?= __("Office") ?>
                                    </th>
                                    <th width="5%" class="sorting" tabindex="0"
                                        aria-controls="datatable_office_employees_inactive" rowspan="1" colspan="1">
                                        <?= __("Start Date") ?>
                                    </th>
                                    <th width="5%" class="sorting" tabindex="0"
                                        aria-controls="datatable_office_employees_inactive" rowspan="1" colspan="1">
                                        <?= __("Release Date") ?>
                                    </th>
                                    <th width="30%" class="sorting" tabindex="0"
                                        aria-controls="datatable_office_employees_inactive" rowspan="1" colspan="1">
                                        <?= __("Actions") ?>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var OfficeUnitManagement = {
        checked_node_ids: [],
        loadMinistryWiseLayers: function () {
            OfficeSetup.loadLayers($("#office-ministry-id").val(), function (response) {
                //
            });
        },
        loadMinistryAndLayerWiseOfficeOrigin: function () {
            OfficeSetup.loadOffices($("#office-ministry-id").val(), $("#office-layer-id").val(), function (response) {
                //
            });
        },
        loadOriginOffices: function () {
            OfficeSetup.loadOriginOffices($("#office-origin-id").val(), function (response) {

            });
        },

        getEmployeesByOfficeId: function (office_id) {
            $("#datatable_office_employees_active > tbody").html("");
            $("#datatable_office_employees_inactive > tbody").html("");
            // Actives
            PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + 'officeEmployees/getOfficeEmployees',
                {'office_id': office_id, 'status': 1}, 'json',
                function (response_data) {
                    var str = "";

                    $.each(response_data, function (i) {
                        var odd_even_class = "odd";
                        if (i % 2 == 0) {
                            odd_even_class = "even";
                        }

                        response = response_data[i];

                        str += '<tr role="row" class="' + odd_even_class + '">';
                        str += '<td>' + response['employee_name_bng'] + '</td>';
                        str += '<td>' + response['designation_name_bng'] + '</td>';
                        str += '<td>' + response['unit_name_bng'] + '</td>';
                        str += '<td>' + response['joining_date'] + '</td>';
                        str += '<td>' + response['last_office_date'] + '</td>';
                        str += '<td>';
                        str += '<a class="btn btn-xs default btn-editable" href="javascript:;"><i class="fa fa-share"></i> View</a>';
                        str += '</td>';
                        str += '</tr>';
                    });

                    $("#datatable_office_employees_active > tbody").html(str);

                    // Inactives
                    PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + 'officeEmployees/getOfficeEmployees',
                        {'office_id': office_id, 'status': 0}, 'json',
                        function (response_data) {
                            $.each(response_data, function (i) {
                                var str = "";
                                var odd_even_class = "odd";
                                if (i % 2 == 0) {
                                    odd_even_class = "even";
                                }

                                response = response_data[i];

                                str += '<tr role="row" class="' + odd_even_class + '">';
                                str += '<td>' + response['employee_name_bng'] + '</td>';
                                str += '<td>' + response['designation_name_bng'] + '</td>';
                                str += '<td>' + response['unit_name_bng'] + '</td>';
                                str += '<td>' + response['joining_date'] + '</td>';
                                str += '<td>' + response['last_office_date'] + '</td>';
                                str += '<td>';
                                str += '<a class="btn btn-xs default btn-editable" href="javascript:;"><i class="fa fa-share"></i> View</a>';
                                str += '</td>';
                                str += '</tr>';
                                $("#datatable_office_employees_inactive > tbody").append(str);
                            });
                        }
                    );

                }
            );
        }
    };

    $(function () {
        $("#office-ministry-id").bind('change', function () {
            OfficeUnitManagement.loadMinistryWiseLayers();
        });
        $("#office-layer-id").bind('change', function () {
            OfficeUnitManagement.loadMinistryAndLayerWiseOfficeOrigin();
        });
        $("#office-origin-id").bind('change', function () {
            OfficeUnitManagement.loadOriginOffices();
        });
        $("#office-id").bind('change', function () {
            OfficeUnitManagement.getEmployeesByOfficeId($(this).val());
        });

    });
</script>