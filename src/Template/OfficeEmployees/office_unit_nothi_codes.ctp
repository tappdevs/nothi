<div class="row">
    <div class="col-md-12">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <?php echo __("Office"); ?>
                    <?php echo __("Unit"); ?>
                </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="sample_1">
                    <thead>
                    <tr>
                        <th class="text-center">নাম</th>
                        <th class="text-center">শাখা কোড</th>
                        <th class="text-center">শাখা ক্রম</th>
                        <th class="text-center">সর্বশেষ পত্রজারি স্মারক ক্রম</th>
                        <th class="text-center">কার্যক্রম</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr class="odd gradeX">
                        <?php
                        foreach ($results as $rows):
                        ?>
                    <tr>
                        <td class=""><?php echo $rows['unit_name_bng']; ?></td>
                        <td class="text-center"><?php echo enTobn($rows['unit_nothi_code']); ?></td>
                        <td class="text-center"><?php echo enTobn($rows['unit_level']); ?></td>
                        <td class="text-center"><?php echo $this->Number->format($rows['sarok_no_start'],['pattern'=>'#####']); ?></td>
                        <td class="text-center">
                            <?= $this->Html->link(__('Edit'), ['action' => 'editOfficeUnitNothiCode', $rows['id']], ['class' => 'btn btn-primary']) ?>
                        </td>
                        <?php
                        endforeach;
                        ?>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>