<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption"><i class=""></i>New Office (Global)</div>
        <div class="tools">
            <a href="javascript:" class="collapse"></a>
            <a href="#portlet-config" data-toggle="modal" class="config"></a>
            <a href="javascript:" class="reload"></a>
            <a href="javascript:" class="remove"></a>
        </div>
    </div>
    <div class="portlet-body form"><br><br>
        <?php echo $this->Form->create('OfficeHierarchyTemplate', array('id' => 'OfficeHierarchyTemplateForm')); ?>
        <div class="form-horizontal">
            <div class="form-group">
                <label class="col-sm-4 control-label">Structure Type</label>

                <div class="col-sm-7">
                    <?php echo $this->Form->input('structure_type', array('type' => 'select', 'empty' => '--Choose--', 'label' => false, 'class' => 'form-control')); ?>
                </div>
            </div>
            <?php echo $this->element('OfficeHierarchy/add', array('parent' => $parent)); ?>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-4 col-md-9">
                        <button type="button" onclick="OfficeHierarchy.save()" class="btn   blue ajax_submit">
                            Submit
                        </button>
                        <button type="reset" class="btn   default">Reset</button>
                    </div>
                </div>
            </div>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
</div>

<script type="text/javascript">
    var OfficeHierarchy =
    {
        save: function () {
            TreeView.saveNode($("#OfficeHierarchyTemplateForm").serialize());
        }
    }
</script>
