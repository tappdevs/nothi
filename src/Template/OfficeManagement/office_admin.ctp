<div class="row">
    <div class="col-md-12">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fs1 a2i_gn_edit2"></i><?php echo __('Office Admin') ?> <?php echo __('Assign') ?>
                </div>
              
            </div>

            <div class="portlet-body">
                <?= $cell = $this->cell('OfficeSelection', ['entity' => '']) ?>
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo __('Office') ?> </label>

                        <div class="col-sm-6">
                            <?php
                            echo $this->Form->input('office_id', array(
                                'label' => false,
                                'class' => 'form-control',
                                'empty' => '----'
                            ));
                            ?>
                        </div>
                    </div>
                </div>
                <hr/>
              
                <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover" id="sample_1">
                    <thead>
                    <tr>
                        
                        <th class=""></th>
                        <th class="">ক্রমিক নং </th>
                        <th class=""><?php echo __('Name') ?></th>
                        <th class="">পদবি</th>
                        <th class="">শাখা</th>
                        <th class=""><?=  __('Username') ?></th>
	                    <th class="">মোবাইল</th>
	                    <th class="">ইমেইল</th>
                    </tr>
                    </thead>
                    <tbody>
                  
                    </tbody>
                    <tfoot>
                        <tr>
                            <td class="form-actions" colspan="8"> <input class="btn btn-md btn-primary" type="button" id="assignOfficeAdmin" value="<?php echo __("Assign") ?>" /></td>
                        </tr>
                    </tfoot>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    var EmployeeAssignment = {
        checked_node_ids: [],
        loadMinistryWiseLayers: function () {
            OfficeSetup.loadLayers($("#office-ministry-id").val(), function (response) {
                //
            });
        },
        loadMinistryAndLayerWiseOfficeOrigin: function () {
            OfficeSetup.loadOffices($("#office-ministry-id").val(), $("#office-layer-id").val(), function (response) {
                //
            });
        },
        loadOriginOffices: function () {
            OfficeSetup.loadOriginOffices($("#office-origin-id").val(), function (response) {

            });
        },
        
        
         loadEmployees: function(val,employeeid){
             
             
            $('#sample_1').find('tbody').html('');
            PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + 'EmployeeRecords/emoloyeeDesignationByOffice',
            {'office_id': val,officer_id:employeeid,group:'EmployeeOffices.office_unit_organogram_id'}, 'json',
                function (response)
                {
                    $.each(response, function(i, data){
                        $('#sample_1').find('tbody').append("<tr><td>"+ '<input ' +(data.is_admin==1?"checked=checked":'') + ' type="radio" name="office_admin" data-user-id="'+data.employee_office_id+'" class="office_admin" />' + "</td><td>"+BnFromEng(i+1)+"</td><td>"+data.name_bng+"</td><td>"+data.designation+"</td><td>"+data.unit_name_bng+"</td><td>"+data.username+"</td><td>"+data.personal_mobile+"</td><td>"+data.personal_email+"</td></tr>");
                    })
                    
                    
                }
            );
        },
    };

    $(function () {
        $("#office-ministry-id").bind('change', function () {
            EmployeeAssignment.loadMinistryWiseLayers();
        });
        $("#office-layer-id").bind('change', function () {
            EmployeeAssignment.loadMinistryAndLayerWiseOfficeOrigin();
        });
        $("#office-origin-id").bind('change', function () {
            EmployeeAssignment.loadOriginOffices();
        });
        $("#office-id").bind('change', function () {
            EmployeeAssignment.loadEmployees($(this).val());
        });
        
        $(document).off('click','#assignOfficeAdmin');
        $(document).on('click','#assignOfficeAdmin',function(){
            
            var done = 0;
            if($('.office_admin').length>0){
                Metronic.blockUI({
                    target: '.portlet.box.green',
                    boxed: true
                });
                var requested_user_id = $('.office_admin:checked').attr('data-user-id');
                $.ajax({
                    url: '<?php echo $this->Url->build(['controller'=>'OfficeManagement','action'=>'AssignOfficeAdmin']) ?>',
                    data: {'requested_user_id':requested_user_id, 'office_id':$('#office-id').val()},
                    cache: false,
                    type: 'post',
                    success: function(msg){
                        Metronic.unblockUI('.portlet.box.green');
                        toastr.success(msg);
                    }
                });
            }
        });
    });
</script>