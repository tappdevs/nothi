<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-check-square-o"></i><?php echo __('Office') ?> <?php echo __('Management') ?>
        </div>

    </div>
    <div class="portlet-body form">
        <?php echo $this->Form->create($entity, ['method' => 'post']) ?>
        <?= $cell = $this->cell('OfficeSelection', ['entity' => '']) ?>
        <div class="row form-group">

            <label class="col-sm-2 control-label"><?php echo __('Office') ?></label>

            <div class="col-sm-6">
                <?php
                echo $this->Form->input('office_id', array(
                    'label' => false,
                    'class' => 'form-control',
                    'empty' => '----',
                    'required' => true
                ));
                ?>
            </div>
        </div>
        <div class="row form-group">

            <label class="col-sm-2 control-label"><?php echo __('Domain') . ' ' . __('Url') ?> <span
                        class="required">*</span></label>

            <div class="col-sm-6">
                <?php
                echo $this->Form->input('domain_url', array(
                    'label' => false,
                    'class' => 'form-control',
                    'required' => true
                ));
                ?>
            </div>
        </div>
        <div class="row form-group">

            <label class="col-sm-2 control-label"><?php echo __('Domain') . ' ' . __('Prefix') ?> <span
                        class="required">*</span></label>

            <div class="col-sm-6">
                <?php
                echo $this->Form->input('domain_prefix', array(
                    'label' => false,
                    'class' => 'form-control',
                    'required' => true
                ));
                ?>
            </div>
        </div>
        <div class="row form-group">

            <label class="col-sm-2 control-label"><?php echo __('Domain') . ' ' . __('Host') ?></label>

            <div class="col-sm-6">
                <div class="form-inline">

                    <div class="form-group">
                        <input type="checkbox" class="" name="default_host" checked/> <label
                                class="control-label"><?= __("Default") ?></label>
                    </div>
                    <div class="form-group">
                        <?php
                        echo $this->Form->input('domain_host', array(
                            'label' => false,
                            'class' => 'form-control',
                            'empty' => '----',
                            'readonly' => true
                        ));
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row form-group">

            <label class="col-sm-2 control-label"><?php echo __('Domain') . ' ' . __('Username') ?></label>

            <div class="col-sm-6">
                <div class="form-inline">

                    <div class="form-group">
                        <input type="checkbox" class="" name="default_username" checked/> <label
                                class="control-label"><?= __("Default") ?></label>
                    </div>
                    <div class="form-group">
                        <?php
                        echo $this->Form->input('domain_username', array(
                            'label' => false,
                            'class' => 'form-control',
                            'empty' => '----',
                            'readonly' => true
                        ));
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row form-group">

            <label class="col-sm-2 control-label"><?php echo __('Domain') . ' ' . __('Password') ?></label>

            <div class="col-sm-6">
                <div class="form-inline">

                    <div class="form-group">
                        <input type="checkbox" class="" name="default_password" checked/> <label
                                class="control-label"><?= __("Default") ?></label>
                    </div>
                    <div class="form-group">
                        <?php
                        echo $this->Form->input('domain_password', array(
                            'label' => false,
                            'class' => 'form-control',
                            'empty' => '----',
                            'type' => 'password',
                            'readonly' => true
                        ));
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-actions">
            <input type="submit" class="btn btn-primary " value="<?php echo __("Create Database") ?>"
                   id="office_setup"/>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
</div>


<script type="text/javascript">

    var OfficeManagement = {
        enableEditMode: function (input) {

        },
        init: function () {

        }
    };

    var OfficeUnitManagement = {
        checked_node_ids: [],
        loadMinistryWiseLayers: function () {
            OfficeSetup.loadLayers($("#office-ministry-id").val(), function (response) {

            });
        },
        loadMinistryAndLayerWiseOfficeOrigin: function () {
            OfficeSetup.loadOffices($("#office-ministry-id").val(), $("#office-layer-id").val(), function (response) {

            });
        },
        loadOriginOffices: function () {
            OfficeSetup.loadOriginOffices($("#office-origin-id").val(), function (response) {

                OfficeUnitManagement.reloadOriginUnitTree();
            });
        },
    };
    $(function () {
        $("#office-ministry-id").bind('change', function () {
            OfficeUnitManagement.loadMinistryWiseLayers();
        });
        $("#office-layer-id").bind('change', function () {
            OfficeUnitManagement.loadMinistryAndLayerWiseOfficeOrigin();
        });
        $("#office-origin-id").bind('change', function () {
            OfficeUnitManagement.loadOriginOffices();
        });
        $("#office-id").bind('change', function () {
            $.ajax({
                url: '<?php echo $this->Url->build(['controller' => 'OfficeManagement', 'action' => 'getOfficeDomain']) ?>',
                data: {office_id: $(this).val()},
                cache: false,
                type: 'post',
                dataType: 'json',
                success: function (json) {
                    if (json.status == 'success') {
                        $('#domain-url').val(json.domain_url);
                        $('#domain-host').val(json.domain_host);
                        $('#domain-prefix').val(json.domain_prefix);
                    }
                }
            })
        });

        $('[name=default_host]').click(function () {
            if (this.checked) {
                $('#domain-host').attr('readonly', 'readonly');
                $('#domain-host').val('');
            } else {
                $('#domain-host').removeAttr('readonly');
            }
        })

        $('[name=default_username]').click(function () {
            if (this.checked) {
                $('#domain-username').attr('readonly', 'readonly');
                $('#domain-username').val('');
            } else {
                $('#domain-username').removeAttr('readonly');
            }
        })

        $('[name=default_password]').click(function () {
            if (this.checked) {
                $('#domain-password').attr('readonly', 'readonly');
                $('#domain-password').val('');
            } else {
                $('#domain-password').removeAttr('readonly');
            }
        })
    });
</script>
