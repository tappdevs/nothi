<?php
$session = $this->request->session();
$logged_user = $session->read('logged_employee_record');
$office_records = $logged_user['office_records'];
$office_unit_array = [];
$office_unit_options = array();
if (!empty($office_records)) {
    foreach ($office_records as $k => $record) {
        if (!in_array($record['office_unit_id'], $office_unit_array)) {
            $office_unit_array[] = $record['office_unit_id'];
            $office_unit_options[] = [
                'text' => $record['unit_name'],
                'value' => $record['office_unit_id'],
                'office-id' => $record['office_id'],
            ];
        }
    }
}

?>

<div class="portlet light">
    <!--<div class="portlet-title">
        <div class="caption">অফিস সিল তৈরি</div>
    </div>-->
    <div class="portlet-body form">
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET -->
                <div class="">
                    <div class="portlet-title">
                        <div class="row">
                            <div class="col-md-5 form-group form-horizontal">
                                <lebel>
                                    পদের নাম: <b><?= $currentOffice['designation'].', '.$currentOffice['office_unit_name'] ?></b>
                                </lebel>
                                <input type="hidden" id="office-unit-id" value="<?= $currentOffice['office_unit_id']?>" office-id="<?= $currentOffice['office_id']?>">
                                <input type="hidden" id="prev_value">
                            </div>

                        </div>
                        <hr/>
                        <div class="row">
                            <div class="col-md-6 office-seal form-group form-horizontal">
                                <div class="form-inline">
                                    প্রাপক খুঁজুন
                                    <select id="organogram_with_name" style="width:100%;" placeholder="প্রাপক খুঁজুন"></select>
                                </div>
                                <h4>অফিস শাখা</h4>
                                <div id="office_unit_tree_panel">
                                    <img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp;লোড হচ্ছে। একটু অপেক্ষা করুন... </span>
                                </div>


                            </div>
                            <div class="col-md-6 office-seal form-group form-horizontal">
                                <h4>প্রাপকের তালিকা</h4>
                                <div id="office_unit_seal_panel"><img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp;লোড হচ্ছে। একটু অপেক্ষা করুন... </span></div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="portlet-body">

                </div>
            </div>
            <!-- END PORTLET -->
        </div>
    </div>
</div>
</div>

<script>
    $(document).ready(function () {
        $("#organogram_with_name").on('change', function() {
            var organogramId= $(this).val();
            var element = $("#office_unit_tree_panel input[type=checkbox][data-office-unit-organogram-id="+organogramId+"]");
            console.log(element.closest('.panel-default').find('.accordion-toggle').hasClass('in'));
            if (element.closest('.panel-default').find('.accordion-toggle').hasClass('in') == false) {
                //alert(1);
                element.closest('.panel-default').find('.accordion-toggle').click();
            } else {
                //alert(2);
            }

            if (element.is(":checked") == false) {
                element.click();
            }

            element.focus();
            $(element.next()).selectText();
        })
    });

    jQuery.fn.selectText = function(){
        this.find('input').each(function() {
            if($(this).prev().length == 0 || !$(this).prev().hasClass('p_copy')) {
                $('<p class="p_copy" style="position: absolute; z-index: -1;"></p>').insertBefore($(this));
            }
            $(this).prev().html($(this).val());
        });
        var doc = document;
        var element = this[0];
        if (doc.body.createTextRange) {
            var range = document.body.createTextRange();
            range.moveToElementText(element);
            range.select();
        } else if (window.getSelection) {
            var selection = window.getSelection();
            var range = document.createRange();
            range.selectNodeContents(element);
            selection.removeAllRanges();
            selection.addRange(range);
        }
    };
</script>