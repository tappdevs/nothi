<div id="office_tree">
    <!-- Load Data of Office as Tree -->
</div>

<script src="<?php echo CDN_PATH; ?>assets/global/plugins/jstree/dist/jstree.min.js"></script>
<script type="text/javascript">
    $('#office_tree').jstree('refresh');
    $('#office_tree').jstree({
        'core': {
            "themes": {
                "variant": "large",
                "multiple": false
            },
            'data': [
                <?php
                if(!empty($tree_nodes)){
                 foreach($tree_nodes as $node){
                          echo '{ "id" : "'.$node['id'].'", "parent" : "'.$node['parent'].'", "text" : "'.$node['text'].'", "icon" : "'.$node['icon'].'" },';
                 }
                }
                ?>
            ]
        }
    }).bind("loaded.jstree", function (event, data) {
        // you get two params - event & data - check the core docs for a detailed description
        $(this).jstree("open_all");
    });
</script>