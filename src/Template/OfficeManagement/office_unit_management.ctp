<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-check-square-o"></i><?php echo __('Office') ?> <?php echo __('Management') ?>
        </div>
 
    </div>
    <div class="portlet-window-body">
        <?php if(empty($employee_offices)){ ?>
        <?= $cell = $this->cell('OfficeSelection', ['entity' => '']) ?>
        <div class="row">
            <div class="form-group">
                <label class="col-sm-2 control-label"><?php echo __('Office') ?></label>

                <div class="col-sm-6">
                    <?php
                    echo $this->Form->input('office_id', array(
                        'label' => false,
                        'class' => 'form-control',
                        'empty' => '----'
                    ));
                    ?>
                </div>
            </div>
        </div>
          <hr/>
        <?php }else {
            
            echo $this->Form->input('office_id', array(
                        'label' => false,
                        'class' => 'form-control',
                        'type' => 'hidden',
                        'default'=>$employee_offices['office_id']
                    ));
            
            echo $this->Form->input('office_origin_id', array(
                        'label' => false,
                        'class' => 'form-control',
                        'type' => 'hidden',
                        'default'=>$employee_offices['office_origin_id']
                    ));
        } ?> 
      
        <div class="row">
            <div class="portlet light col-md-5">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-check-square-o"></i><?php echo __('Origin') ?> <?php echo __('Units') ?>
                    </div>
                  
                </div>
                <div class="portlet-window-body">
                    <div class="col-md-6" id="origin_unit_tree_panel">
                      
                    </div>
                </div>
            </div>
            <?php if($loggedUser['user_role_id']==1 || $loggedUser['user_role_id']==2) { ?>
            <div class="portlet light col-md-1 text-center">
                <div class="portlet-title">
                    <div class="caption">
                        <?php echo __('Actions') ?>
                    </div>
                </div>
                <div class="portlet-window-body">
                    <button type="button" id="officeUnitTransfer" class="btn   btn-success"><i
                            class="fa fa-hand-o-right"></i></button>
                    <button type="button" id="office_unit_delete" class="btn   btn-danger"><i
                            class="fa fa-hand-o-left"></i></button>
                </div>
            </div>
            <?php } ?>
            <div class="portlet light col-md-5">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-check-square-o"></i><?php echo __('Office') ?> <?php echo __('Units') ?>
                    </div>
                    
                </div>
                <div class="portlet-window-body">
                    <div id="office_unit_tree_panel">
                      
                    </div>
                   
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    var OfficeManagement = {
        enableEditMode: function (input) {

        },

        init: function () {

        }
    };

    var OfficeUnitManagement = {
        checked_node_ids: [],
        loadMinistryWiseLayers: function () {
            OfficeSetup.loadLayers($("#office-ministry-id").val(), function (response) {
                
            });
        },
        loadMinistryAndLayerWiseOfficeOrigin: function () {
            OfficeSetup.loadOffices($("#office-ministry-id").val(), $("#office-layer-id").val(), function (response) {
                
            });
        },
        loadOriginOffices: function () {
            OfficeSetup.loadOriginOffices($("#office-origin-id").val(), function (response) {
                
                OfficeUnitManagement.reloadOriginUnitTree();
            });
        },
        reloadOriginUnitTree: function () {
            PROJAPOTI.ajaxSubmitAsyncDataCallbackWithoutAbrot(js_wb_root + 'officeOriginTree/populateFullOriginUnitTree',
                {'office_origin_id': $("#office-origin-id").val()}, 'json',
                function (response) {
                    if (response.length > 0) {
                        var data_str = "";
                        $.each(response[0], function (i) {
                            var node = response[0][i];
                            data_str += '{ "id" : "' + node.id + '", "parent" : "' + ((isEmpty(node.parent))?"#":node.parent) + '", "text" : "' + node.text + '", "icon" : "' + node.icon + '" },';
                        });
                        data_str= data_str.replace(/\\n/g, "\\n")
                            .replace(/\\'/g, "\\'")
                            .replace(/\\"/g, '\\"')
                            .replace(/\\&/g, "\\&")
                            .replace(/\\r/g, "\\r")
                            .replace(/\\t/g, "\\t")
                            .replace(/\\b/g, "\\b")
                            .replace(/\\f/g, "\\f");
             // remove non-printable and other non-valid JSON chars
                        data_str = data_str.replace(/[\u0000-\u0019]+/g,"");
                        var edited_data_str = "[" + data_str.replace(/^,|,$/g, '') + "]";

                        $('#origin_unit_tree_panel').jstree(true).settings.core.data = JSON.parse(edited_data_str);
                        $('#origin_unit_tree_panel').jstree(true).refresh();
                        $('#origin_unit_tree_panel').bind("refresh.jstree", function (event, data) {
                            $('#origin_unit_tree_panel').jstree("open_all");
                        });

                    }
                    else {
                        $('#origin_unit_tree_panel').jstree(true).settings.core.data = '';
                        $('#origin_unit_tree_panel').jstree(true).refresh();
                        $('#origin_unit_tree_panel').bind("refresh.jstree", function (event, data) {
                            $('#origin_unit_tree_panel').jstree("open_all");
                        });
                    }
                    
                    if($("#office-id").val()>0){
                        OfficeUnitManagement.reloadOfficeUnitTree();
                    }

                });
        },

        reloadOfficeUnitTree: function () {
            PROJAPOTI.ajaxSubmitAsyncDataCallbackWithoutAbrot(js_wb_root + 'officeOriginTree/populateFullOfficeUnitTree',
                {'office_id': $("#office-id").val()}, 'json',
                function (response) {
                    if (response.length > 0) {
                        var data_str = "";
                        $.each(response[0], function (i) {
                            var node = response[0][i];
                            data_str += '{ "id" : "' + node.id + '", "parent" : "' + ((isEmpty(node.parent))?"#":node.parent) + '", "text" : "' + node.text + '", "icon" : "' + node.icon + '" },';
                        });
                        data_str= data_str.replace(/\\n/g, "\\n")
                            .replace(/\\'/g, "\\'")
                            .replace(/\\"/g, '\\"')
                            .replace(/\\&/g, "\\&")
                            .replace(/\\r/g, "\\r")
                            .replace(/\\t/g, "\\t")
                            .replace(/\\b/g, "\\b")
                            .replace(/\\f/g, "\\f");
             // remove non-printable and other non-valid JSON chars
                        data_str = data_str.replace(/[\u0000-\u0019]+/g,"");
                        var edited_data_str = "[" + data_str.replace(/^,|,$/g, '') + "]";

                        $('#office_unit_tree_panel').jstree(true).settings.core.data = JSON.parse(edited_data_str);
                        $('#office_unit_tree_panel').jstree(true).refresh();
                        $('#office_unit_tree_panel').bind("refresh.jstree", function (event, data) {
                            $('#office_unit_tree_panel').jstree("open_all");
                        });

                    }
                    else {
                        $('#office_unit_tree_panel').jstree(true).settings.core.data = '';
                        $('#office_unit_tree_panel').jstree(true).refresh();
                        $('#office_unit_tree_panel').bind("refresh.jstree", function (event, data) {
                            $('#office_unit_tree_panel').jstree("open_all");
                        });
                    }

                });
        },

        loadOfficeUnitTree: function () {

            $('#office_unit_tree_panel').jstree(
                {
                    'core': {
                        "themes": {
                            "variant": "large",
                            "multiple": false
                        },
                        'data': ''
                    },

                    "checkbox": {
                        "keep_selected_style": false
                    },
                    <?php if($loggedUser['user_role_id']==1 || $loggedUser['user_role_id']==2) { ?>
                    "plugins": ["checkbox"]
                    <?php } ?>
                }
            ).bind("loaded.jstree", function (event, data) {
                    $(this).jstree("open_all");
                });
        },

        loadOfficeOriginUnitTree: function () {

            $('#origin_unit_tree_panel').jstree(
                {
                    'core': {
                        "themes": {
                            "variant": "large",
                            "multiple": false
                        },
                        'data': ''
                    },

                    "checkbox": {
                        "keep_selected_style": false
                    },
                    <?php if($loggedUser['user_role_id']==1 || $loggedUser['user_role_id']==2) { ?>
                    "plugins": ["checkbox"]
                    <?php } ?>
                }
            ).bind("loaded.jstree", function (event, data) {
                    $(this).jstree("open_all");
                });
        },

        transferUnits: function () {
            var office_id = $("#office-id").val();
            if (!office_id) {
                alert("Please select your office first.");
                return false;
            }
            OfficeUnitManagement.checked_node_ids = $('#origin_unit_tree_panel').jstree('get_checked');
            if (OfficeUnitManagement.checked_node_ids.length > 0) {
                PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + 'officeManagement/transferOfficeUnits',
                    {'origin_unit_ids': OfficeUnitManagement.checked_node_ids, 'office_id': office_id}, 'json',
                    function (response) {
                        
                        OfficeUnitManagement.reloadOfficeUnitTree();
                    });
            }
        },

        deleteOfficeUnits: function () {
            if (confirm("Are you sure want to delete?")) {
            } else {
                return false;
            }
            var office_id = $("#office-id").val();
            if (!office_id) {
                alert("Please select your office first.");
                return false;
            }
            OfficeUnitManagement.checked_node_ids = $('#office_unit_tree_panel').jstree('get_checked');

            if (OfficeUnitManagement.checked_node_ids.length > 0) {
                PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + 'officeManagement/deleteOfficeUnits',
                    {'office_unit_ids': OfficeUnitManagement.checked_node_ids, 'office_id': office_id}, 'json',
                    function (response) {
                        if(response.status==0){
                            toastr.error(response.msg);
                        }else{
                             toastr.success(response.msg);
                        }
                        OfficeUnitManagement.reloadOfficeUnitTree();
                    });
            }

        }
    };
    $(function () {
        $("#office-ministry-id").bind('change', function () {
            OfficeUnitManagement.loadMinistryWiseLayers();
        });
        $("#office-layer-id").bind('change', function () {
            OfficeUnitManagement.loadMinistryAndLayerWiseOfficeOrigin();
        });
        $("#office-origin-id").bind('change', function () {
            OfficeUnitManagement.loadOriginOffices();
        });
        $("#office-id").bind('change', function () {
            OfficeUnitManagement.reloadOfficeUnitTree();
        });
        $("#officeUnitTransfer").bind('click', function () {
            OfficeUnitManagement.transferUnits();
        });
        $("#office_unit_delete").bind('click', function () {
            OfficeUnitManagement.deleteOfficeUnits();
        });
        OfficeUnitManagement.loadOfficeUnitTree();
        OfficeUnitManagement.loadOfficeOriginUnitTree();
        
        if($("#office-origin-id").val()>0){
            OfficeUnitManagement.reloadOriginUnitTree();
        }
        

    });
</script>