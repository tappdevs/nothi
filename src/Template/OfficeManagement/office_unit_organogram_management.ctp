<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-check-square-o"></i><?php echo __('Office') ?> <?php echo __('Unitr') ?> <?php echo __('Organogram') ?> <?php echo __('Management') ?>
        </div>

    </div>
    <div class="portlet-window-body">
        <?php if(empty($employee_offices)){ ?>
        <?= $cell = $this->cell('OfficeSelection', ['entity' => '']) ?>
        <div class="row">
            <div class="form-group">
                <label class="col-sm-2 control-label"><?php echo __('Office') ?></label>

                <div class="col-sm-6">
                    <?php
                    echo $this->Form->input('office_id', array(
                        'label' => false,
                        'class' => 'form-control',
                        'empty' => '----'
                    ));
                    ?>
                </div>
            </div>
        </div>
        <hr/>
        <?php }else {
            echo $this->Form->input('office_id', array(
                        'label' => false,
                        'class' => 'form-control',
                        'type' => 'hidden',
                        'default'=>$employee_offices['office_id']
                    ));
        } ?> 
       
        <div class="row">
            <div class="portlet light col-md-5">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-check-square-o"></i><?php echo __('Origin') ?> <?php echo __('Unitr') ?> <?php echo __('Organogra') ?>
                    </div>
                </div>
                <div class="portlet-window-body">
                    <div class="" id="origin_unit_tree_panel">
                        <!--  -->
                    </div>
                </div>
            </div>
            <?php if($loggedUser['user_role_id']==1 || $loggedUser['user_role_id']==2 || $office_info_count==1) { ?>
            <div class="portlet light col-md-1 text-center">
                <div class="portlet-title">
                    <div class="caption">
                        <?php echo __('Actions') ?>
                    </div>
                </div>
                <div class="portlet-window-body">
                    <button type="button" id="officeUnitTransfer" class="btn   btn-success"><i
                            class="fa fa-hand-o-right"></i></button>
                    <br/><br/>
                    <button type="button" id="office_unit_tree_delete" class="btn   btn-danger"><i
                            class="fa fa-hand-o-left"></i></button>
                </div>
            </div>
            <?php } ?>
            <div class="portlet light col-md-5">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-check-square-o"></i><?php echo __('Office') ?> <?php echo __('Unit') ?> <?php echo __('Organogra') ?>
                    </div>
                </div>
                <div class="portlet-window-body">
                    <div id="office_unit_tree_panel">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    var OfficeUnitManagement = {
        checked_node_ids: [],
        loadMinistryWiseLayers: function () {
            OfficeSetup.loadLayers($("#office-ministry-id").val(), function (response) {
                //
            });
        },
        loadMinistryAndLayerWiseOfficeOrigin: function () {
            OfficeSetup.loadOffices($("#office-ministry-id").val(), $("#office-layer-id").val(), function (response) {
                //
            });
        },
        loadOriginOffices: function () {
            OfficeSetup.loadOriginOffices($("#office-origin-id").val(), function (response) {

            });
        },
        getUnitOrganogramByOfficeId: function (office_id) {

            PROJAPOTI.ajaxSubmitAsyncDataCallbackWithoutAbrot(js_wb_root + 'officeManagement/getOriginUnitOrgByOfficeId',
                {'office_id': office_id}, 'json',
                function (response) {
                    if (response.length > 0) {
                        var data_str = "";
                        $.each(response[0], function (i) {
                            var node = response[0][i];
                            data_str += '{ "id" : "' + node.id + '", "parent" : "' + node.parent + '", "text" : "' + node.text + '", "icon" : "' + node.icon + '" },';
                        });
                         data_str= data_str.replace(/\\n/g, "\\n")
                            .replace(/\\'/g, "\\'")
                            .replace(/\\"/g, '\\"')
                            .replace(/\\&/g, "\\&")
                            .replace(/\\r/g, "\\r")
                            .replace(/\\t/g, "\\t")
                            .replace(/\\b/g, "\\b")
                            .replace(/\\f/g, "\\f");
             // remove non-printable and other non-valid JSON chars
                        data_str = data_str.replace(/[\u0000-\u0019]+/g,"");

                        var edited_data_str = "[" + data_str.replace(/^,|,$/g, '') + "]";

                        $('#origin_unit_tree_panel').jstree(true).settings.core.data = JSON.parse(edited_data_str);
                        $('#origin_unit_tree_panel').jstree(true).refresh();
                        $('#origin_unit_tree_panel').bind("refresh.jstree", function (event, data) {
                            $('#origin_unit_tree_panel').jstree("open_all");
                        });
                    }
                    else {
                        $('#origin_unit_tree_panel').jstree(true).settings.core.data = '';
                        $('#origin_unit_tree_panel').jstree(true).refresh();
                        $('#origin_unit_tree_panel').bind("refresh.jstree", function (event, data) {
                            $('#origin_unit_tree_panel').jstree("open_all");
                        });
                    }

                    OfficeUnitManagement.reloadOfficeUnitOrg(office_id);

                }
            );
        },

        reloadOfficeUnitOrg: function (office_id) {
            PROJAPOTI.ajaxSubmitAsyncDataCallbackWithoutAbrot(js_wb_root + 'officeManagement/getOfficeDesignationsByOfficeId',
                {'office_id': office_id}, 'json',
                function (response) {
                    if (response.length > 0) {
                        var data_str = "";
                        $.each(response[0], function (i) {
                            var node = response[0][i];
                            data_str += '{ "id" : "' + node.id + '", "parent" : "' + node.parent + '", "text" : "' + node.text + '", "icon" : "' + node.icon + '" },';
                        });
                        data_str= data_str.replace(/\\n/g, "\\n")
                            .replace(/\\'/g, "\\'")
                            .replace(/\\"/g, '\\"')
                            .replace(/\\&/g, "\\&")
                            .replace(/\\r/g, "\\r")
                            .replace(/\\t/g, "\\t")
                            .replace(/\\b/g, "\\b")
                            .replace(/\\f/g, "\\f");
             // remove non-printable and other non-valid JSON chars
                        data_str = data_str.replace(/[\u0000-\u0019]+/g,"");
                        var edited_data_str = "[" + data_str.replace(/^,|,$/g, '') + "]";

                        $('#office_unit_tree_panel').jstree(true).settings.core.data = JSON.parse(edited_data_str);
                        $('#office_unit_tree_panel').jstree(true).refresh();
                        $('#office_unit_tree_panel').bind("refresh.jstree", function (event, data) {
                            $('#office_unit_tree_panel').jstree("open_all");
                        });

                    }
                    else {
                        $('#office_unit_tree_panel').jstree(true).settings.core.data = '';
                        $('#office_unit_tree_panel').jstree(true).refresh();
                        $('#office_unit_tree_panel').bind("refresh.jstree", function (event, data) {
                            $('#office_unit_tree_panel').jstree("open_all");
                        });
                    }

                }
            );
        },
        deleteOrigins: function () {

        },

        selected_organogram: [],

        selectOrganogram: function (input) {
            var organogram_id = $(input).attr('id');
            var unit_id = $(input).data('unit-id');
            var org_unit = "unit_" + unit_id + ":" + "org_" + organogram_id;

            var index = OfficeUnitManagement.selected_organogram.indexOf(org_unit);
            if (index < 0) {
                OfficeUnitManagement.selected_organogram.push(org_unit);
            }
            else {
                OfficeUnitManagement.selected_organogram.splice(index, 1);
            }
        },

        transferOrganogram: function (element) {
            var parent_div = $(element).closest('div.row');
            Metronic.blockUI({
                target: parent_div,
                boxed: true,
                message: 'অপেক্ষা করুন'
            });
            OfficeUnitManagement.selected_organogram = [];
            if ($('#origin_unit_tree_panel').find('.jstree-clicked .org_checkbox').length == 0) {
                Metronic.unblockUI(parent_div);
                return false;
            } else {
                $('#origin_unit_tree_panel').find('.jstree-clicked .org_checkbox').each(function (i) {
                    OfficeUnitManagement.selectOrganogram($(this));
                });
            }

            if (OfficeUnitManagement.selected_organogram.length > 0) {

                PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + 'officeManagement/transferOrganogram',
                    {
                        'origin_unit_org_ids': OfficeUnitManagement.selected_organogram,
                        'office_id': $("#office-id").val()
                    }, 'json',
                    function (response) {
                    if(response.status=='success'){
                        toastr.success('সকল পদবি সংযোজন করা হয়েছে।');
                    } else if(response.status=='error'){
                        toastr.error(response.msg);
                    }
                        OfficeUnitManagement.reloadOfficeUnitOrg($("#office-id").val());
                        Metronic.unblockUI(parent_div);
                    });
            }
        },
        deleteOfficeUnitOrganogram: function (element) {
            var parent_div = $(element).closest('div.row');
            Metronic.blockUI({
                target: parent_div,
                boxed: true,
                message: 'অপেক্ষা করুন'
            });
            var selectedId = $('#office_unit_tree_panel').find('.jstree-clicked .org_checkbox').map(function () {
                return this.id;
            }).get();
            if (selectedId.length > 0) {

                PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + 'officeManagement/deleteOfficeOrganogram',
                    {'office_unit_org_ids': selectedId, 'office_id': $("#office-id").val()}, 'json',
                    function (response) {
                        if (response.status == 1){
                            toastr.success(response.msg);
                        } else {
                            toastr.error(response.msg);
                        }
                        OfficeUnitManagement.reloadOfficeUnitOrg($("#office-id").val());
                        Metronic.unblockUI(parent_div);
                    });
            }

        },

        loadOfficeOriginUnitTree: function () {

            $('#origin_unit_tree_panel').jstree(
                {
                    'core': {
                        "themes": {
                            "variant": "large",
                            "multiple": false
                        },
                        'data': ''
                    },

                    "checkbox": {
                        "keep_selected_style": false
                    },
                    <?php if($loggedUser['user_role_id']==1 || $loggedUser['user_role_id']==2 || $office_info_count==1) { ?>
                    "plugins": ["checkbox"]
                    <?php } ?>
                }
            ).bind("loaded.jstree", function (event, data) {
                    $(this).jstree("open_all");
                });
        },
        loadOfficeUnitTree: function () {

            $('#office_unit_tree_panel').jstree(
                {
                    'core': {
                        "themes": {
                            "variant": "large",
                            "multiple": false
                        },
                        'data': ''
                    },

                    "checkbox": {
                        "keep_selected_style": false
                    },
                    <?php if($loggedUser['user_role_id']==1 || $loggedUser['user_role_id']==2 || $office_info_count==1) { ?>
                    "plugins": ["checkbox"]
                    <?php } ?>
                }
            ).bind("loaded.jstree", function (event, data) {
                    $(this).jstree("open_all");
                });
        }

    };

    $(function () {
        
        $("#office-ministry-id").bind('change', function () {
            OfficeUnitManagement.loadMinistryWiseLayers();
        });
        $("#office-layer-id").bind('change', function () {
            OfficeUnitManagement.loadMinistryAndLayerWiseOfficeOrigin();
        });
        $("#office-origin-id").bind('change', function () {
            OfficeUnitManagement.loadOriginOffices();
        });
        $("#office-id").bind('change', function () {
            OfficeUnitManagement.getUnitOrganogramByOfficeId($(this).val());
        });
        $("#officeUnitTransfer").bind('click', function () {
            OfficeUnitManagement.transferOrganogram(this);
        });
        $(".deleteUnitOrganogram").bind("click", function () {
            OfficeUnitManagement.deleteOrigins();
        });
        $("#office_unit_tree_delete").bind("click", function () {
            OfficeUnitManagement.deleteOfficeUnitOrganogram(this);
        });
        $(document).on('click', '#origin_unit_tree_panel .jstree-anchor', function () {
            OfficeUnitManagement.selectOrganogram($('#origin_unit_tree_panel .jstree-anchor').find('.org_checkbox'));
        });

        OfficeUnitManagement.loadOfficeOriginUnitTree();
        OfficeUnitManagement.loadOfficeUnitTree();
        
        if($("#office-id").val()>0){
            OfficeUnitManagement.getUnitOrganogramByOfficeId($("#office-id").val());
        }
    });
</script>