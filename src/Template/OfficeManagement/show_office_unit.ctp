<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <?php echo __("Office"); ?>
            <?php echo __("Unit"); ?>
            <?php echo __("Information"); ?>
            <?php echo __("Edit"); ?>
        </div>
    </div>
    <div class="portlet-body">
        <div class="form">
            <?php
           
            echo $this->Form->create($entity, ['method' => 'post', 'id' => 'OfficeUnitForm']);

            echo $this->Form->hidden('id');
            
            ?>
            <div class="form-body">
                <div class="row">
                    <div class="col-md-6 form-group form-horizontal">
                        <label class="control-label">নাম (বাংলা)</label>
                        <?= $this->Form->input('unit_name_bng', ['label' => false, 'class' => 'form-control', 'type' => 'text','required'=>'required','disabled'=>true]); ?>
                    </div>
                    <div class="col-md-6 form-group form-horizontal">
                        <label class="control-label">নাম (ইংরেজি)</label>
<?= $this->Form->input('unit_name_eng', ['label' => false, 'class' => 'form-control', 'type' => 'text','disabled'=>true]); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 form-group form-horizontal">
                        <label class="control-label">ঊর্ধ্বতন শাখা</label>
                        <?= $this->Form->input('parent_unit_id', ['label' => false, 'class' => 'form-control', 'type' => 'select','options'=>array(0=>'ঊর্ধ্বতন অফিস')+$office_units,'required'=>'required']); ?>
                    </div>
                    <div class="col-md-6 form-group form-horizontal">
                        <label class="control-label">ধরন</label>
<?= $this->Form->input('office_unit_category', ['label' => false, 'class' => 'form-control', 'type' => 'select','options'=>$unitCategory]); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 form-group form-horizontal">
                        <label class="control-label">শাখা কোড</label>
                        <?= $this->Form->input('unit_nothi_code', ['label' => false, 'class' => 'form-control', 'type' => 'text']); ?>
                    </div>
                    <div class="col-md-6 form-group form-horizontal">
                        <label class="control-label">পত্রজারি স্মারক ক্রম </label>
<?= $this->Form->input('sarok_no_start', ['label' => false, 'class' => 'form-control', 'type' => 'text']); ?>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-6 form-group form-horizontal">
                        <label class="control-label"> ক্রম </label>
<?= $this->Form->input('unit_level', ['label' => false, 'class' => 'form-control', 'type' => 'text']); ?>
                    </div>
                </div>

                <div class="row">
                    
                    <div class="col-md-6">
<?= $this->Form->button(__('Submit'), ['type' => 'submit', 'class' => 'btn btn-primary']) ?>

                    </div>
                </div>
                <p class="alert alert-block error_msg">
                    
                </p>
            </div>
        </div>
    </div>
</div>


<script>
    
    $(document).off('submit','#OfficeUnitForm').on('submit','#OfficeUnitForm', function(e){
        e.preventDefault();
        $.ajax({
            url: '<?php echo $this->Url->build(['controller'=>'OfficeManagement','action'=>'editOfficeUnit']) ?>',
            data: $(this).serializeArray(),
            type: 'post',
            dataType: 'json',
            cache: false,
            success: function(ret){
                
                if(ret.status == 'success'){
                    $('.error_msg').text(ret.msg).removeClass('alert-danger').addClass('alert-success');
                    toastr.success(ret.msg);
                    $(document).find('#office_unit_edit_panel').html('');
                    OfficeUnitManagement.reloadOfficeUnitTree();
                }else{
                    $('.error_msg').text(ret.msg).removeClass('alert-success').addClass('alert-danger');
                }
            }
        })
    })
</script>