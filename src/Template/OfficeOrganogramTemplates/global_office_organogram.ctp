<div class="row">
    <div class="col-md-12">
        <div class="portlet green-meadow box">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-cogs"></i>Search Office</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                    <a href="#portlet-config" data-toggle="modal" class="config"></a>
                    <a href="javascript:;" class="reload"></a>
                    <a href="javascript:;" class="remove"></a>
                </div>
            </div>
            <div class="portlet-body form">
                <?= $this->Form->create("", ['class' => 'form-horizontal']); ?>
                <div class="form-body">
                    <div class="form-group">
                        <div class="col-md-6">
                            <?php echo $this->Form->input('office_hierarchy', array('id' => 'office_hierarchy', 'list' => 'office_hierarchy_list', 'label' => false, 'class' => 'form-control')); ?>
                            <?php echo $this->Form->hidden('office_hierarchy_id', array('id' => 'office_hierarchy_id', 'label' => false, 'class' => 'form-control')); ?>
                        </div>
                        <div class="col-md-6">
                            <button type="button" class="btn btn-danger office_org_btn">Load Office Organogram</button>
                        </div>
                        <datalist id="office_hierarchy_list">
                            <?php foreach ($office_hierarchy_list as $ohl) { ?>
                                <option value="<?= $ohl; ?>"></option>
                            <?php } ?>
                        </datalist>
                    </div>
                </div>
                <?= $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>
<div class="row" id="ofc_org_div" style="display: none;">
    <div class="col-md-6">
        <div class="portlet green-meadow box">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-cogs"></i>Global Office Organogram</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                    <a href="#portlet-config" data-toggle="modal" class="config"></a>
                    <a href="javascript:;" class="reload"></a>
                    <a href="javascript:" class="remove"></a>
                </div>
            </div>
            <div class="portlet-body">
                <div id="org_tree_div">

                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6" id="update_div">
    </div>
</div>

<script type="text/javascript">
    var TreeView = {
        selected_node: "",
        parent_id: 0,
        parent_node: "",
        checked_node: "",
        office_tree: "",
        newNode: function (input) {
            $("#update_div").show('slow');
            var parent_id = $(input).attr('data-parent');
            var dataid = $(input).attr('data-id');
            var ofc_hierarchy_id = parseInt($("#office_hierarchy_id").val());

            if (ofc_hierarchy_id == 'NaN' || ofc_hierarchy_id == 0) {
                return;
            }

            TreeView.parent_id = parent_id;
            TreeView.selected_node = dataid;
            TreeView.parent_node = $(input).attr('data-parent-node');

            var url = "<?php echo $this->request->webroot;?>officeOrganogramTemplates/add?parent_id=" + parent_id + "&&office_hierarchy_id=" + ofc_hierarchy_id;
            PROJAPOTI.ajaxLoadCallback(url, function (response) {
                $("#update_div").html(response);
            });
        },
        saveNode: function (formdata) {
            var ofc_hierarchy_id = parseInt($("#office_hierarchy_id").val());
            var url = "<?php echo $this->request->webroot;?>officeOrganogramTemplates/add?parent_id=" + TreeView.parent_id + "&&office_hierarchy_id=" + ofc_hierarchy_id;
            PROJAPOTI.ajaxSubmitDataCallback(url, {formdata}, function (response) {
                if (response == 1) {
                    alert("Node has been added successfully.");
                    $("#update_div").hide('slow');
                    $("#org_tree_div").jstree("load_node", $('#' + TreeView.parent_node));
                }
                else {
                    alert("Failed to add.Please try again.");
                }
            });
        },

        getSelectedOfficeId: function () {
            var selected_office_text = $("#office_hierarchy").val();
            <?php
                foreach($office_hierarchy_list as $key=>$ohl){
            ?>
            if (selected_office_text == '<?= $ohl;?>') {
                console.debug(selected_office_text);
                TreeView.checked_node = '<?= $key?>';
            }
            <?php
             }
             ?>
            $("#office_hierarchy_id").val(TreeView.checked_node);
        },

        loadOfficeOrganogram: function () {
            $("#org_tree_div").jstree('destroy');
            TreeView.checked_node = "";
            TreeView.getSelectedOfficeId();
            $("#ofc_org_div").show('slow');
            $('#org_tree_div').jstree({
                "core": {
                    "themes": {
                        "variant": "large",
                        "theme": "classic",
                        "dots": true,
                    },
                    'data': {
                        'url': function (node) {
                            return node.id === '#' ?
                                'loadGlblOfcOrgHierarchy' : 'loadGlblOfcOrgHierarchy';
                        },
                        'data': function (node) {
                            return {'id': node.id, 'office_hierarchy_id': $("#office_hierarchy_id").val()};
                        }
                    }
                }
            });
        }
    };

    $(function () {
        $(".office_org_btn").bind('click', function () {
            TreeView.loadOfficeOrganogram();
        });
        $("#office_hierarchy").bind('input', function () {
            $("#ofc_org_div").hide('slow');
            $("#org_tree_div").jstree('destroy');
        });


        /*
         TreeView.office_tree = $('#office_tree_div').jstree({
         "core" : {
         "themes" : {
         "variant" : "large"
         },
         'data' : {
         'url' : function (node) {
         return node.id === '#' ?
         '
        <?php echo $this->request->webroot?>officeHierarchyTemplates/loadGlobalOfficeHierarchy':'
        <?php echo $this->request->webroot?>officeHierarchyTemplates/loadGlobalOfficeHierarchy';
         },
         'data' : function (node) {
         return { 'id' : node.id,'type':'checkbox' };
         }
         }
         },
         "checkbox" : {
         "keep_selected_style" : false
         },
         "plugins" : ["checkbox" ],

         });
         office_tree.bind("check_node.jstree uncheck_node.jstree", function(e) {
         console.log("Event", e);
         console.log('Cheked:', office_tree.jstree('get_checked'));
         console.log('Uncheked:', office_tree.jstree('get_unchecked'));
         });
         */

    });
</script>