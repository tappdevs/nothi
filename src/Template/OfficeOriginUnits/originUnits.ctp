<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            <i class=""></i><?php echo __('Office') ?> <?php echo __('Origin') ?> <?php echo __('Unit') ?>
        </div>
    </div>
    <div class="portlet-window-body">
        <?= $cell = $this->cell('OfficeSelection'); ?>
        <div class="row" id="unit_tree_div">
            <div class="col-md-6">
                <div class="portlet light">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class=""></i>Unit Tree
                        </div>
                    </div>
                    <div class="portlet-window-body">
                        <div id="unit_tree"></div>
                    </div>
                </div>
            </div>
            <div id="unit_tree_expand_view" class="col-md-6"></div>

        </div>
    </div>
</div>
<style>
    #unit_tree_expand_view{
        position: fixed;
        right:5px;
    }
</style>
<!-- Start : Office Setup JS -->
<script type="text/javascript">
    var OfficeUnitView = {
        selected_node: "",
        parent_id: 0,
        parent_node: "",
        selected_ministry_id: 0,
        selected_layer_id: 0,
        selected_office_id: 0,
        newNode: function (input) {
            $("#unit_tree_expand_view").hide('slow');
            var parent_id = $(input).attr('data-parent');
            var dataid = $(input).attr('data-id');
            OfficeUnitView.parent_id = parent_id;
            OfficeUnitView.selected_node = dataid;
            OfficeUnitView.parent_node = $(input).attr('data-parent-node');
            var url = "<?php echo $this->request->webroot;?>officeOrigin/addOfficeOriginUnit?parent_id=" + parent_id;
            PROJAPOTI.ajaxLoad(url, "#unit_tree_expand_view");
            $("#unit_tree_expand_view").show('slow');
        },

        saveNode: function (formdata) {
            $("#OfficeOriginUnitForm #office_ministry_id").val(OfficeUnitView.selected_ministry_id);
            $("#OfficeOriginUnitForm #office_layer_id").val(OfficeUnitView.selected_layer_id);
            $("#OfficeOriginUnitForm #office_origin_id").val(OfficeUnitView.selected_office_id);

            var url = "<?php echo $this->request->webroot;?>officeOrigin/addOfficeOriginUnit";

            PROJAPOTI.ajaxSubmitDataCallback(url, {'formdata': $("#OfficeOriginUnitForm").serialize()}, "json", function (response) {
                if (response == 1) {
                    $("#unit_tree_expand_view").hide('slow');
                    if (OfficeUnitView.parent_id == 0) {
                        OfficeUnitView.unitTreeView();
                    }
                    else {
                        $("#unit_tree").jstree("load_node", $('#' + OfficeUnitView.parent_node));
                    }
                }
                else {
                    alert("Failed to add Node.");
                }
            });
        },

        editNode: function (id) {
            var url = "<?php echo $this->request->webroot;?>officeOrigin/editOfficeOriginUnit/" + id;
            PROJAPOTI.ajaxSubmitDataCallback(url, {'formdata': $("#OfficeOriginUnitForm").serialize()}, "json", function (response) {
                if (response == 1) {
                    $("#unit_tree_expand_view").hide('slow');
                    if (OfficeUnitView.parent_id == 0) {
                        OfficeUnitView.unitTreeView();
                    }
                    else {
                        $("#unit_tree").jstree("load_node", $('#' + OfficeUnitView.parent_node));
                    }
                }
                else {
                    alert("Failed to update Node.");
                }
            });
        },

        deleteNode: function (id) {
            if (confirm("Are you sure want to delete?")) {
            } else {
                return false;
            }
            var url = "<?php echo $this->request->webroot;?>officeOrigin/deleteOfficeOriginUnit/" + id;
            PROJAPOTI.ajaxSubmitDataCallback(url, {}, "json", function (response) {
                if (response == 1) {
                    $("#unit_tree_expand_view").hide('slow');
                    if (OfficeUnitView.parent_id == 0) {
                        OfficeUnitView.unitTreeView();
                    }
                    else {
                        $("#unit_tree").jstree("load_node", $('#' + OfficeUnitView.parent_node));
                    }
                }
                else {
                    toastr.error(response);
                }
            });
        },

        gotoEdit: function (input) {
            $("#unit_tree_expand_view").hide('slow');
            var id = $(input).attr('data-id');
            var url = "<?php echo $this->request->webroot;?>officeOrigin/editOfficeOriginUnit/" + id;
            PROJAPOTI.ajaxLoad(url, "#unit_tree_expand_view");
            $("#unit_tree_expand_view").show('slow');
        },

        unitTreeView: function () {
            OfficeUnitView.selected_ministry_id = $("#office-ministry-id").val();
            OfficeUnitView.selected_layer_id = $("#office-layer-id").val();
            OfficeUnitView.selected_office_id = $("#office-origin-id").val();

            $('#unit_tree').jstree("refresh");
            $('#unit_tree').jstree({
                "core": {
                    "themes": {
                        "variant": "large",
                    },
                    'data': {
                        'url': function (node) {
                            return node.id === '#' ?
                                'loadOfficeOriginUnits' : 'loadOfficeOriginUnits';
                        },
                        'data': function (node) {
                            return {'id': node.id, 'type': "123", 'office_id': $("#office-origin-id").val()};
                        }
                    }
                }
            });
        },
        init: function () {
            $("#office-origin-id").bind('change', function () {
                $("#unit_tree_div").hide('slow');
                OfficeUnitView.unitTreeView();
                $("#unit_tree_div").show('slow');
            });
        }
    };

    $(function () {
        OfficeSetup.init();
        OfficeUnitView.init();
        $("#unit_tree_div").hide();
    });
</script>
<!-- End : Office Setup JS -->