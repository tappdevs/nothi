<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            <i class="fs1 a2i_gn_edit2"></i><?php echo __('Add New') ?> <?php echo __('Office') ?>
        </div>
        <!--  <div class="tools">
              <a href="javascript:;" class="collapse"></a> <a
                  href="#portlet-config" data-toggle="modal" class="config"></a> <a
                  href="javascript:;" class="reload"></a> <a href="javascript:"
                                                             class="remove"></a>
          </div>-->
    </div>
    <div class="portlet-body form">
		<?php if(Live == 0): ?>
        <?php echo $this->Form->create($entity, array('id' => 'OfficeOriginForm')); ?>

        <?php echo $this->element('OfficeOrigin/origin_form') ?>

        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-4 col-md-9">
                    <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-primary']) ?>
                    <?= $this->Form->button(__('Cancel'), ['class' => 'btn btn-danger']) ?>
                </div>
            </div>
        </div>
        <?php echo $this->Form->end(); ?>
	    <?php endif; ?>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        $("#OfficeOriginForm #office-ministry-id").bind('change', function () {
            var office_ministry_id = $(this).val();
            OfficeSetup.loadParentOfficesAndLayersByMinistry(office_ministry_id);
        });
    });
</script>

