<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            <i class="fs1 a2i_gn_edit2"></i>Update Office Origin
        </div>
        <div class="tools">
            <a href="javascript:" class="collapse"></a> <a
                href="#portlet-config" data-toggle="modal" class="config"></a> <a
                href="javascript:;" class="reload"></a> <a href="javascript:;"
                                                           class="remove"></a>
        </div>
    </div>
    <div class="portlet-body form">

        <?php echo $this->Form->create($entity, array('id' => 'OfficeOriginForm')); ?>
        <?= $this->Form->input('id'); ?>
        <?php echo $this->element('OfficeOrigin/origin_form') ?>

        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-4 col-md-9">
                    <?= $this->Form->button(__('Update'), ['class' => 'btn btn-primary']) ?>
                </div>
            </div>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
</div>


<script type="text/javascript">

    $(function () {
        $("#OfficeOriginForm #office-ministry-id").bind('change', function () {
            var office_ministry_id = $(this).val();
            OfficeSetup.loadParentOfficesAndLayersByMinistry(office_ministry_id, function () {

                $("#OfficeOriginForm #office-layer-id").val('<?php echo $entity->office_layer_id?>');
                $("#OfficeOriginForm #parent-office-id").val('<?php echo $entity->parent_office_id?>');
            });
        });
        $("#OfficeOriginForm #office-ministry-id").trigger('change');

    });

</script>

