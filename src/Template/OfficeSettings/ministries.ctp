<div class="portlet light">
    <div class="portlet-title">
        <div class="caption"><i
                class="fs1 a2i_gn_details1"></i><?php echo __('Ministry') ?><?php echo('/') ?> <?php echo __('_Division List') ?>
        </div>
        <!--<div class="tools">
            <a href="javascript:" class="collapse"></a>
            <a href="#portlet-config" data-toggle="modal" class="config"></a>
            <a href="javascript:" class="reload"></a>
            <a href="javascript:" class="remove"></a>
        </div>-->
    </div>
    <div class="portlet-body">
        <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                        <?= $this->Html->link(__('Add New'), ['action' => 'addMinistry'], ['class' => 'btn btn-default']) ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="btn-group pull-right">
                        <button class="btn dropdown-toggle" data-toggle="dropdown"><?php echo __('Tools') ?> <i
                                class="glyphicon glyphicon-chevron-down"></i></button>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="#"> </a></li>
                            <li><a href="#"><?php echo __('Save as PDF') ?> </a></li>
                            <li><a href="#">Export to Excel </a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th class="text-center">আইডি</th>
                <th class="text-center"><?php echo __('Type') ?></th>
                <th class="text-center">নাম(বাংলা)</th>
                <th class="text-center">নাম(ইংরেজি)</th>
                <th class="text-center">সংক্ষিপ্ত নাম</th>
                <th class="actions text-center">বর্তমান অবস্থা</th>
                <th class="actions text-center">কার্যক্রম</th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($officeMinistries as $rows): ?>
                <tr>
                    <td class="text-center"><?php echo $rows['id']; ?></td>
                    <td class="text-center">
                        <?php
                        $office_types = json_decode(TOP_OFFICE_TYPE, true);
                        echo $office_types[$rows['office_type']];
                        ?>
                    </td>
                    <td class="text-center"><?php echo h($rows['name_bng']); ?></td>
                    <td class="text-center"><?php echo h($rows['name_eng']); ?></td>
                    <td class="text-center"><?php echo $rows['name_eng_short']; ?></td>
                    <td class="text-center"><?php echo $rows['reference_code']; ?></td>
                    <td class="actions text-center">
                        <?= $this->Html->link(__('Edit'), ['action' => 'editMinistry', $rows['id']], ['class' => 'btn btn-success']) ?>

                        <!--<?php echo $this->Html->link('Delete', array('action' => 'deleteMinistry', $rows['id']),
                            array('onclick' => 'return confirm("Are you sure want to delete?");', 'class' => 'btn btn-danger')); ?>-->
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
