<div class="portlet light">
    <div class="portlet-title">
        <div class="caption"><i class="fs1 a2i_gn_details1"></i>Office Layer List</div>
        <div class="tools">
            <a href="javascript:" class="collapse"></a>
            <a href="#portlet-config" data-toggle="modal" class="config"></a>
            <a href="javascript:" class="reload"></a>
            <a href="javascript:" class="remove"></a>
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                        <?= $this->Html->link('Add New', ['action' => 'addLayer'], ['class' => 'btn btn-default']) ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="btn-group pull-right">
                        <button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i
                                class="glyphicon glyphicon-chevron-down"></i></button>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="#">Print </a></li>
                            <li><a href="#">Save as PDF </a></li>
                            <li><a href="#">Export to Excel </a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th class="text-center">আইডি</th>
                <th class="text-center">Office Ministry</th>
                <th class="text-center">নাম(বাংলায়)</th>
                <th class="text-center">নাম(ইংরেজি)</th>
                <th class="text-center">Level</th>
                <th class="text-center">Sequence</th>
                <th class="actions text-center">কার্যক্রম</th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($officeLayers as $rows): ?>
                <tr>
                    <td class="text-center"><?php echo $rows['id']; ?></td>
                    <td class="text-center"><?php echo $rows['office_ministry']['name_bng']; ?></td>
                    <td class="text-center"><?php echo $rows['layer_name_bng']; ?></td>
                    <td class="text-center"><?php echo $rows['layer_name_eng']; ?></td>
                    <td class="text-center"><?php echo $rows['layer_level']; ?></td>
                    <td class="text-center"><?php echo $rows['layer_sequence']; ?></td>
                    <td class="actions text-center">
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit'], ['class' => 'btn btn-success']) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete'], ['class' => 'btn btn-danger'], ['confirm' => __('Are you sure you want to delete # {0}?')]) ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
