<style>
    .inbox .inbox-nav li.active b {
        display: none !important;
    }

    .tooltip-inner {
        white-space: pre-line;
    }

    .nav-tabs > li > a, .nav-pills > li > a{
        font-size: 12px;
    }

    h3 {
        margin-top: 0px;
        margin-bottom: 0px;
    }
    .search-form{
      padding-bottom: 5px;
    }

</style>
<?php
    function hide_data($string_to_encode,$first_range =1,$last_range = 1) {
    $len = strlen($string_to_encode);
    return substr($string_to_encode, 0, $first_range).str_repeat('*', $len - $last_range).substr($string_to_encode, $len - $last_range, $last_range);
}
?>
<div class="portlet light">
    <div class="portlet-body">
        <?= $this->Form->create('search',['type'=>'get','class'=>'form-inline search-form pull-right']) ?>
      <input type="text" name="search" style="width: 450px !important;" class="form-control" id="search" value="<?= !empty($this->request->query('search'))?$this->request->query('search'):'' ?>" placeholder="অফিসের নাম, আইডি, ডোমেইন,সম্পূর্ণ ডাটাবেস হোস্ট দিয়ে অনুসন্ধান করুন">
        <?= $this->Form->button(__('Search'),['class'=>'btn btn-success']) ?>
        <?= $this->Form->end() ?>
        <table class="table table-bordered table-advance table-stripped table-striped">
            <?php
            echo "<tr class='heading'>
                        <th>ক্রম</th>
                        <th>মন্ত্রণালয়</th>
                        <th>মন্ত্রণালয়/বিভাগ</th>
                        <th>দপ্তর / অধিদপ্তরের ধরন</th>
                        <th>অফিস</th>
                        <th>ডোমেইন</th>
                        <th>অ্যাপ্লিকেশন হোস্ট</th>
                        <th>ডাটাবেস হোস্ট</th>
                  </tr>";
            $param = $this->Paginator->params();
            $index =(($param['page'] - 1) * $param['perPage']) + 1;
            foreach($offices as $key=>$value){
                $url_without_protocol = str_replace(['http://','https://'],'',$value['domain_url']);
                $publicIPFunction = "getOfficePublicIP(\"$url_without_protocol\",\"office-{$value['office_id']}\")";
            echo "<tr class='eachOffice' data-office='{$value['office_id']}' id='office-{$value['office_id']}'>
                        <td>".entobn($index++)."</td>
                        <td>".$value['ministry_name']."</td>
                        <td>".$value['layer_name']."</td>
                        <td>".$value['origin_name']."</td>
                        <td>{$value['office_name']} ({$value['office_id']}) </td>
                        <td>".$value['domain_url']."</td>
                        <td class='public_ip'><button class='btn btn-xs btn-primary' onclick='$publicIPFunction'>".__('View')."</button></td>
                        <td class='status'>".hide_data($value['domain_host'],2,3)."</td>
                  </tr>";
        }
        ?>
        </table>
        <?= customPagination($this->Paginator,1) ?>
    </div>
</div>
<?php echo $this->Html->script('assets/admin/layout4/scripts/projapoti_ajax.js?v='.js_css_version) ?>
<script>

//hitIndi(0);

function hitIndi(i){
    $('.eachOffice').eq(i).removeClass('danger');
    var office_id = $('.eachOffice').eq(i).data('office');
    if(typeof(office_id) != 'undefined') {
        $('.eachOffice').eq(i).find('td.status').text("Loading...")

        PROJAPOTI.ajaxSubmitDataCallbackError('<?= $this->Url->build(['controller' => 'Potrojari', 'action' => 'getError']) ?>/' + office_id, {}, 'json', function (res) {
            if (res.status != 'success') {
                $('.eachOffice').eq(i).addClass('danger');
                $('.eachOffice').eq(i).find('td.status').text(res.count)
            }
            hitIndi(++i);
        }, function (err) {
            hitIndi(++i);
        })
    }else{
        hitIndi(++i);
    }
}
function getOfficePublicIP(domain_name,id) {
    if(!isEmpty(domain_name)){
        var url = '<?= $this->Url->build(['controller' => 'OfficeSettings','action' => 'officeList']) ?>';
        PROJAPOTI.ajaxSubmitDataCallback(url,{'getOfficePublicIP' : 1,'domain' : domain_name},'json',function (response) {
            if(!isEmpty(response)){
                $("#"+id).find(".public_ip").html(response);
            }
        });
    }
}
</script>