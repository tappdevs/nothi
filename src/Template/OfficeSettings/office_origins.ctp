<div class="portlet light">
    <div class="portlet-title">
        <div class="caption"><i
                class="fs1 a2i_gn_details1"></i><?php echo __('Office') ?> <?php echo __('Origin') ?> <?php echo __('List') ?>
        </div>
        <!--<div class="tools">
            <a href="javascript:" class="collapse"></a>
            <a href="#portlet-config" data-toggle="modal" class="config"></a>
            <a href="javascript:" class="reload"></a>
            <a href="javascript:" class="remove"></a>
        </div>-->
    </div>
    <div class="portlet-body">
        <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
						<?php if(Live == 0): ?>
                        <?= $this->Html->link(__('Add New'), ['action' => 'addOfficeOrigin'], ['class' => 'btn btn-default']) ?>
						<?php endif; ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="btn-group pull-right">
                        <button class="btn dropdown-toggle" data-toggle="dropdown"><?php echo __('Tools') ?> <i
                                class="glyphicon glyphicon-chevron-down"></i></button>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="#"><?php echo __('Print') ?> </a></li>
                            <li><a href="#"><?php echo __('Save as PDF') ?> </a></li>
                            <li><a href="#">Export to Excel </a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th class="text-center">আইডি</th>
                <th class="text-center"><?php echo __('Office') ?> <?php echo __('Ministry') ?> </th>
                <th class="text-center"><?php echo __('Office') ?> <?php echo __('Layer') ?></th>
                <th class="text-center">নাম(বাংলা)</th>
                <th class="text-center">নাম(ইংরেজি)</th>
                <th class="text-center"><?php echo __('Level') ?></th>
                <th class="text-center"><?php echo __('Sequence') ?></th>
                <th class="text-center"><?php echo __('Parent') ?> <?php echo __('Office') ?></th>
                <?php if(Live == 0): ?>
	            <th class="actions text-center">কার্যক্রম</th>
	            <?php endif; ?>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($officeOrigins as $rows): ?>
                <tr>
                    <td class="text-center"><?php echo $rows['id']; ?></td>
                    <td class="text-center"><?php echo $rows['office_ministry_name_bng']; ?></td>
                    <td class="text-center"><?php echo $rows['office_layer_name_bng']; ?></td>
                    <td class="text-center"><?php echo $rows['office_name_bng']; ?></td>
                    <td class="text-center"><?php echo $rows['office_name_eng']; ?></td>
                    <td class="text-center"><?php echo $rows['office_level']; ?></td>
                    <td class="text-center"><?php echo $rows['office_sequence']; ?></td>
                    <td class="text-center"><?php echo $rows['parent']; ?></td>
	                <?php if(Live == 0): ?>
                    <td class="actions text-center">
                        <?= $this->Html->link('', ['action' => 'editOfficeOrigin', $rows['id']], ['class' => 'fa fa-edit btn   btn-xs btn-primary']) ?>
                        <?php echo $this->Html->link('', array('action' => 'officeOrigindelete', $rows['id']), array('onclick' => 'return confirm("Are you sure want to delete?");', 'class' => 'fa fa-remove btn   btn-xs btn-danger')); ?>
                    </td>
	                <?php endif; ?>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
