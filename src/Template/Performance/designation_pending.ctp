<div class="portlet light ">
    <div class="portlet-title">
        <div class="caption"><i class="fs1 a2i_gn_details1"></i> পদবি ভিত্তিক অনিষ্পন্ন  </div>
    </div>
    <div class="portlet-body">
        <div class="row">
            <label class="control-label col-md-2 text-right font-lg" >শাখাঃ</label>
            <div class="col-md-4">
                <?php
                echo $this->Form->create('', ['class' => 'searchForm'])
                ?>
                <?php
                echo $this->Form->hidden('start_date', ['class' => 'startdate'])
                ?>
                <?php
                echo $this->Form->hidden('end_date', ['class' => 'enddate'])
                ?>
                <?php
//                if (empty($selected_office_section)) {
                    echo $this->Form->input('office_unit_id',
                        array('label' => false, 'type' => 'select', 'class' => 'form-control',
                        'options' => [-1 => __("-- বাছাই করুন --")] + $ownChildInfo + [0 => __("All")]));
//                }
//                else {
//                    echo $this->Form->input('office_unit_id',
//                        array('label' => false, 'type' => 'select', 'class' => 'form-control',
//                        'options' =>[0 => __("-- বাছাই করুন -- ")] + $ownChildInfo));
//                }
                ?>


            </div>
            <label class="control-label col-md-2 text-right font-lg" >পদঃ</label>
            <div class="col-md-2">
                <?php
                echo $this->Form->input('show',
                    array('label' => false, 'type' => 'select', 'class' => 'form-control',
                          'options' => [2 => __("All"),0=> 'শূন্য পদ',1 => 'কর্মরত']));
                ?>
            </div>
            <div class="col-md-2">
                <button type="button" class="btn  btn-primary btn-md btn-pending-print"> <i class="fa fa-print">&nbsp;প্রিন্ট</i> </button>
            </div>
            <?php echo $this->Form->end() ?>


        </div>
        <br/>
        <div class="inbox-content">
        </div>
        <div class="table-container ">
            <table class="table table-bordered table-hover tableadvance">
                <thead>
                    <tr class="heading">
                        <th class="text-center"> ক্রম</th>
                        <th class="text-center" rowspan="2" id="showSearchParams"> নাম ও পদবি</th>
                        <th  class="text-center">অনিষ্পন্ন ডাক </th>
                        <th  class="text-center">অনিষ্পন্ন নথি </th>
                        <th  class="text-center">কর্মরত </th>

                    </tr>
                </thead>
                <tbody id ="addData">
                    
                </tbody>
            </table>
        </div>
    </div>

</div>

<style>
    @media print {
        table {
            border:1px solid grey !important;
        }
        th, td {
            border:1px solid grey !important;
            padding: 10px!important;
    }
    }
</style>
<script>
    var dak = 0;
    var note = 0;
    var totalUser = 0;
    var unit_ids = [];
    var designation_id = [];
    var designation_name = [];
    jQuery(document).ready(function () {
        $('#office-unit-id').change(function () {
            unit_ids = [];
            designation_id = [];
            designation_name = [];
            dak = 0;
            note = 0;
            totalUser = 0;
             $('#addData').html('');
            if($('#office-unit-id :selected').val() == -1){
                return;
            }
            else if($('#office-unit-id :selected').val() == 0){
                   $('.inbox-content').html('<img src="<?= CDN_PATH ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp; লোড হচ্ছে  সকল শাখার সকল পদবি। বিষয়টি সময় সাপেক্ষ। একটু অপেক্ষা করুন... </span>');
                   getAllDesgination();
//                   $.each($('#office-unit-id > option'), function (i, v) {
//                    if ($(this).val() != 0 || $(this).val() != -1) {
//                         unit_ids.push($(this).val());
//                    }
//                });
            }
            else{
                $('.inbox-content').html('<img src="<?= CDN_PATH ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp; লোড হচ্ছে  । একটু অপেক্ষা করুন... </span>');
                unit_ids.push($('#office-unit-id :selected').val());
                callajax(0);
            }
            
        });
        $('#show').change(function () {
            $('#office-unit-id').trigger('change');
        });

           function DesignationPendingData(index) {
            if (index >= designation_id.length) {
                printTotal(totalUser);
                 $('.inbox-content').html('');
                return;
            }
            $('.inbox-content').html('<img src="<?= CDN_PATH ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp; লোড হচ্ছে <b>'+(!isEmpty(designation_name[index])?designation_name[index]:'') + '</b> । একটু অপেক্ষা করুন... </span>');
            $.ajax({
                type: 'POST',
                url: "<?php echo $this->Url->build(['controller' => 'Performance', 'action' => 'designationPendingReports']) ?>",
                data: {"designation_id": designation_id[index], 'office_id': <?= $selected_office_section['office_id'] ?>,'unit_id' :$('#office-unit-id :selected').val()},
                success: function (data) {
                    if (data.status == 'false' || data.status == false) {
                        DesignationPendingData(index + 1);
                    } else {
                        totalUser++;
                        dak += parseInt(data.dak);
                        note += parseInt(data.note);
                        if(data.sts == 0){
                             $('#addData').append('<tr class="text-center danger"><td>' + BntoEng(totalUser) + '</td><td>' + data.name+', '+ data.designation + '</td><td>' + BntoEng(data.dak) + '</td><td>' + BntoEng(data.note) + '</td><td>' + (data.enable) + '</td><tr>');
                        }else{
                              $('#addData').append('<tr class="text-center"><td>' + BntoEng(totalUser) + '</td><td>' + data.name+', '+ data.designation + '</td><td>' + BntoEng(data.dak) + '</td><td>' + BntoEng(data.note) + '</td><td>' + (data.enable) + '</td><tr>');
                        }
                        DesignationPendingData(index + 1);
                    }

                }
            });
        }
     function BntoEng(input) {
        var numbers = {
            0: '০',
            1: '১',
            2: '২',
            3: '৩',
            4: '৪',
            5: '৫',
            6: '৬',
            7: '৭',
            8: '৮',
            9: '৯'
        };

        var output = '';
        if (parseInt(input) != 0) {
            while (parseInt(input) != 0) {
                var n = parseInt(input % 10);
                if (!Number.isInteger(n))
                    break;
                input = input / 10;
                output += (numbers[n]);
            }
        } else
        {
            output = '০';
        }
        for (var i = output.length - 1, o = ''; i >= 0; i--)
        {
            o += output[i];
        }
        return o;

    }
       function printTotal(total){
          $('#addData').append('<tr class="text-center"><td colspan="2"><b>মোটঃ ' +BntoEng(total) + '</b></td><td><b>' + BntoEng(dak) + '</b></td><td><b>' + BntoEng(note) + '</b></td><td></td><tr>');
    }
    function callajax(cnt){
         if (cnt >= unit_ids.length) {
             $("#showSearchParams").html('নাম ও পদবি (শাখাঃ '+$('#office-unit-id :selected').text()+', পদঃ '+$('#show :selected').text()+' )');
              DesignationPendingData(0);
                return;
            }
        var show = $('#show :selected').val();
         $.ajax({
                type: 'POST',
                url: "<?php echo $this->Url->build(['controller' => 'performance', 'action' => 'designationPending']) ?>",
                data: {"unit_id": unit_ids[cnt],show:show},
                success: function (data) {
                 $.each(data, function (index, val) {
                        designation_id.push(val.id);
                        designation_name.push(val.val);
                    });
                    callajax(cnt+1);
//                    $('#showlist').html(data);
                },
                error: function (data) {
                     callajax(cnt+1);
                }
          });
        }
         function getAllDesgination(){
            var show = $('#show :selected').val();
          $.ajax({
                type: 'POST',

                url: "<?php echo $this->Url->build(['controller' => 'Performance', 'action' => 'designationPending',$selected_office_section['office_id']]) ?>",
                        data: {'unit_id':-1,show : show},
                success: function (data_all) {
                    $.each(data_all,function(i,v){
                        designation_id.push(v);
//                        designation_name.push(v.designation);
                    });
                    $("#showSearchParams").html('নাম ও পদবি (শাখাঃ '+$('#office-unit-id :selected').text()+', পদঃ '+$('#show :selected').text()+' )');
                   DesignationPendingData(0);
                },
                 error: function () {
                }
            });
    }
    });
    $(document).on('click', '.btn-pending-print', function () {
        $(document).find('.table').printThis({
            importCSS: true,
            debug: false,
            importStyle: true,
            printContainer: false,
            pageTitle: "",
            removeInline: false,
            header: null
        });
    });
    
</script>
<script src="<?=$this->request->webroot?>assets/global/scripts/printThis.js"></script>
