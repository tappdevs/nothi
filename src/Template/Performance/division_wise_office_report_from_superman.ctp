<div class="portlet light ">
    <div class="portlet-title">
        <div class="caption"><i class="fs1 a2i_gn_details1"></i> অফিসসমূহের কার্যক্রম বিবরণী  </div>
    </div>
    <div class="portlet-body">
        <div class="row"   id="searchPanel">
            <?php echo $this->Form->create('', ['class' => 'searchForm', 'onsubmit' => 'return false;']) ?>
            <?php echo $this->Form->hidden('single_heading', ['value' => 1]); ?>
            <?php echo $this->Form->hidden('geo_district_name',['id' => 'geo-district-name']); ?>
            <div class="col-md-2">
                <label class="control-label text-right font-lg">বিভাগ</label>
                <?php echo $this->Form->input('geo_division_id', array('label' => false, 'type' => 'select', 'class' => 'form-control', 'options' =>  $geo_division_list, 'empty' => '-- বিভাগ বাছাই করুন --'  )); ?>
            </div>
            <div class="col-md-2" style="display: none">
                <label class="control-label text-right font-lg">জেলা</label>
                <?php echo $this->Form->input('geo_district_id', array('label' => false, 'type' => 'select', 'class' => 'form-control', 'empty' => '-- জেলা বাছাই করুন --'  )); ?>
            </div>
            <div class="col-md-2" style="display: none">
                <label class="control-label text-right font-lg">উপজেলা</label>
                <?php echo $this->Form->input('geo_upazila_id', array('label' => false, 'type' => 'select', 'class' => 'form-control', 'empty' => '-- উপজেলা বাছাই করুন --'  )); ?>
            </div>
            <div class="col-md-2">
                <label class="control-label text-right font-lg">সময়সীমা শুরু</label>
                <?php echo $this->Form->input('start_date', array('label' => false, 'type' => 'text', 'class' => 'form-control datepicker', 'value'=>date('Y-m-d'), 'placeholder' => 'সময়সীমা শুরু'  )); ?>
            </div>
            <div class="col-md-2">
                <label class="control-label text-right font-lg">সময়সীমা শেষ</label>
                <?php echo $this->Form->input('end_date', array('label' => false, 'type' => 'text', 'class' => 'form-control datepicker', 'value'=>date('Y-m-d'), 'placeholder' => 'সময়সীমা শেষ'  )); ?>
            </div>
            <div class="col-md-2" style="display: none;">
                <label class="control-label text-right font-lg" style="display: block;">&nbsp;</label>
                <?php echo $this->Form->button('রিপোর্ট দেখুন', array('label' => false, 'type' => 'submit', 'onclick' => 'showReport(); return false', 'class' => 'btn btn-success btn-md', 'id' => 'btnShowReport'  )); ?>
            </div>
            <?php echo $this->Form->end() ?>


            <!--Tools end -->

            <!--  Date Range Begain -->
            <div class="col-md-6 col-sm-12 hidden">
                <div class="col-md-10 col-sm-10">
                    <div class="hidden-print page-toolbar pull-right portlet-title">
                        <div id="dashboard-report-range" class=" tooltips btn btn-sm btn-default"
                             data-container="body"
                             data-placement="bottom" data-original-title="তারিখ নির্বাচন করুন">
                            <i class="icon-calendar"></i>&nbsp; <span
                                    class="thin uppercase visible-lg-inline-block">তারিখ নির্বাচন করুন</span>&nbsp;
                            <i class="glyphicon glyphicon-chevron-down"></i>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-sm-2">
                    <button type="button" class="btn  btn-primary btn-md btn-performance-print"> <i class="fa fa-print">&nbsp;<?=__('Print')?></i> </button>
                </div>
            </div>

            <!--  Date Range End  -->

        </div>
        <br/>
        <div class="inbox-content" id="OfficePerformance" style="display: none;">
            <div class="table-container ">
                <table class="table table-bordered table-hover tableadvance"  >
                    <thead>
                    <tr class="heading heading-title">
                        <th colspan="15" class="text-center"></th>
                    </tr>
                    <tr class="heading">
                        <th class="text-center" rowspan="2">অফিসের নাম</th>
                        <th class="text-center" rowspan="2"> ব্যবহারকারী</th>
                        <th class="text-center" rowspan="2"> সর্বশেষ হালনাগাদ</th>
                        <th colspan="3" class="text-center"> ডাক </th>
                        <th colspan="4" class="text-center"> নথি </th>
                        <th class="text-center" colspan="3"> পত্রজারিতে নিষ্পন্ন নোট</th>
                        <th colspan="2" class="text-center"> কর্মকর্তা বিহীন পদবি </th>

                    </tr>
                    <tr class="heading">
                        <th class="text-center" >মোট  গ্রহণ </th>
                        <th class="text-center" >মোট নিষ্পন্ন  </th>
                        <th class="text-center" >মোট অনিষ্পন্ন </th>

                        <th class="text-center" >মোট  স্ব- উদ্যোগে নোট </th>
                        <th class="text-center" >মোট ডাক থেকে সৃজিত নোট </th>
                        <th class="text-center" >মোট নোটে নিষ্পন্ন </th>
                        <th class="text-center" >মোট অনিষ্পন্ন নোট </th>

                        <th class="text-center" > আন্তঃসিস্টেম </th>
                        <th class="text-center" > ইমেইল ও অন্যান্য </th>
                        <th class="text-center" >মোট </th>

                        <th class="text-center" >মোট অনিষ্পন্ন ডাক</th>
                        <th class="text-center" >মোট অনিষ্পন্ন নোট </th>

                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script src="<?=$this->request->webroot?>assets/global/scripts/printThis.js"></script>
<script src="<?php echo CDN_PATH; ?>js/reports/monitoring_office_performance_report.js?v=<?= js_css_version ?>" type="text/javascript"></script>
<script src="<?php echo CDN_PATH; ?>daptorik_preview/js/report_date_range.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/floatthead/2.0.3/jquery.floatThead.min.js"></script>

<script>
    jQuery(document).ready(function () {
        $(this).find('body').addClass('page-sidebar-closed');
        $(this).find('.page-sidebar-menu').addClass('page-sidebar-menu-closed');
        // DateRange.init();
        // DateRange.initDashboardDaterange();

        $('.datepicker').datepicker({
            rtl: Metronic.isRTL(),
            orientation: "left",
            autoclose: true,
            format: "yyyy-mm-dd"
        });

        $('#geo-division-id').change(function () {
            var geo_division_id = $(this).val();
            $('#geo-district-id').closest('div[class^=col-md-]').show();
            $('#geo-district-id').html('<option value="">-- লোড হচ্ছে... --</option>');
            $('#geo-district-id').select2();
            $.ajax({
                url: js_wb_root+"GeoDistricts/getDistrictsByDivision",
                data: {geo_division_id: geo_division_id},
                method: "post",
                success: function(response) {
                    $('#geo-district-id').html('<option value="">-- জেলা বাছাই করুন --</option>');
                    $.each(response, function(i, k) {
                        $('#geo-district-id').append('<option value="'+i+'">'+k+'</option>');
                        $('#geo-district-id').select2();

                        $("#btnShowReport").closest('div[class^=col-md-]').show();
                    });
                }
            })
        });
        $('#geo-district-id').change(function () {
            var geo_district_id = $(this).val();
            $('#geo-upazila-id').closest('div[class^=col-md-]').show();
            $('#geo-upazila-id').html('<option value="">-- লোড হচ্ছে... --</option>');
            $('#geo-upazila-id').select2();
            $.ajax({
                url: js_wb_root+"GeoUpazilas/getUpazillasByDistrict",
                data: {geo_district_id: geo_district_id},
                method: "post",
                success: function(response) {
                    $('#geo-upazila-id').html('<option value="">-- উপজেলা বাছাই করুন --</option>');
                    $.each(response, function(i, k) {
                        $('#geo-upazila-id').append('<option value="'+i+'">'+k+'</option>');
                        $('#geo-upazila-id').select2();
                        $('#geo-upazila-id').closest('div[class^=col-md-]').show();
                    });
                }
            });
        });
    });

    function showReport() {
        SortingDate.init($('#start-date').val(), $('#end-date').val())
        $('.table').floatThead({
            position: 'absolute',
            top: jQuery("div.navbar-fixed-top").height()
        });
        $('.table').floatThead('reflow');
    }
</script>