<div class="portlet light ">
    <div class="portlet-title">
        <div class="caption"><i class="fs1 a2i_gn_details1"></i> পদবি ভিত্তিক ড্যাশবোর্ড  </div>
    </div>
    <div class="portlet-body">
        <div class="alert alert-info font-lg text-center">
            প্রিয় ব্যবহারকারী,<br>
           প্রতিদিন রাতে রিপোর্ট সার্ভার হালনাগাদ করা হয়। সর্বশেষ হালনাগাদ তারিখ পর্যন্ত রিপোর্ট দেখতে পাবেন। যদি বর্তমানে কোন পদবিতে কতগুলো অনিষ্পন্ন ডাক ও নথি আছে জানার প্রয়োজন হয় তবে
           <a href="<?= $this->request->webroot; ?>Performance/designationPending"> "অনিষ্পন্ন কার্যক্রম বিবরণী" </a> দেখুন। ।
        </div>
        <div class="row"  id="searchPanel">
            <label class="control-label col-md-1 col-sm-2 text-right font-lg">অফিসঃ</label>
            <div class="col-md-2 col-sm-4">
                <?php
                echo $this->Form->input('office_id',
                    array('label' => false, 'type' => 'select', 'class' => 'form-control',
                        'options' =>  !empty($ownChildInfo)?$ownChildInfo:[],'empty'=>__("বাছাই করুন")));
                ?>

            </div>
              <label class="control-label col-md-1 col-sm-2 text-right font-lg">শাখাঃ</label>
            <div class="col-md-2 col-sm-4">
                        <!--<select class="form-control select2-offscreen">-->
                <?php
                echo $this->Form->create('', ['class' => 'searchForm'])
                ?>
                <?php
                echo $this->Form->hidden('start_date', ['class' => 'startdate'])
                ?>
                <?php
                echo $this->Form->hidden('end_date', ['class' => 'enddate']);
                     echo $this->Form->hidden('unit_ids_length', ['class' => 'unit_ids_length'])
                ?>
                <?php
                echo $this->Form->input('office_unit_id',
                    array('label' => false, 'type' => 'select', 'class' => 'form-control',
                    'options' => [0 => '--শাখা বাছাই করুন--']));
                ?>
                <?php echo $this->Form->end() ?>
                <!--</select>-->

            </div>


            <!--Tools end -->

            <!--  Date Range Begain -->
            <div class="col-md-6 col-sm-12 pull-right">
                <div class="col-md-10 col-sm-10">
                    <div class="hidden-print page-toolbar pull-right portlet-title">
                        <div id="dashboard-report-range" class=" tooltips btn btn-sm btn-default"
                             data-container="body"
                             data-placement="bottom" data-original-title="তারিখ নির্বাচন করুন">
                            <i class="icon-calendar"></i>&nbsp; <span
                                class="thin uppercase visible-lg-inline-block">তারিখ নির্বাচন করুন</span>&nbsp;
                            <i class="glyphicon glyphicon-chevron-down"></i>
                        </div>
                    </div>
                </div>
                  <div class="col-md-2 col-sm-2">
                    <button type="button" class="btn  btn-primary btn-md btn-performance-print"> <i class="fa fa-print">&nbsp;<?=__('Print')?></i> </button>
                </div>
            </div>
            <!--  Date Range End  -->

        </div>
        <br/> <div id="showlist"></div><br>
        <div class="inbox-content">
            <div class="table-container  " id="UnitPerformance">
                <table class="table table-bordered table-hover tableadvance">
                    <thead>
                           <tr class="heading">
                            <th colspan="12" class="text-center" id="headText"></th>
                        </tr>
                        <tr class="heading">
                            <th class="text-center" rowspan="2">ক্রমিক</th>
                            <th class="text-center" rowspan="2">কর্মকর্তার পদবি</th>
                            <th class="text-center" rowspan="2"> সর্বশেষ হালনাগাদ</th>
                            <th colspan="3" class="text-center"> ডাক </th>
                            <th colspan="5" class="text-center"> নথি </th>
                        </tr>
                        <tr class="heading">
                            <th class="text-center" >মোট  গ্রহণ </th>
                            <th class="text-center" >মোট নিষ্পন্ন  </th>
                            <th class="text-center" >মোট অনিষ্পন্ন </th>

                            <th class="text-center" >মোট ডাক থেকে সৃজিত নোট </th>
                            <th class="text-center" >মোট  স্ব- উদ্যোগে নোট </th>
                            <th class="text-center" > পত্রজারিতে নিষ্পন্ন নোট
                            </th>
                            <th class="text-center" >মোট  নোটে নিষ্পন্ন
                            </th>
                            <th class="text-center" >মোট অনিষ্পন্ন নোট </th>
                        </tr>
                    </thead>
                    <tbody id ="addData">

                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>

<script src="<?=$this->request->webroot?>assets/global/scripts/printThis.js"></script>
<script src="<?php echo CDN_PATH; ?>js/reports/new_designation_performance.js?v=<?= js_css_version?>" type="text/javascript"></script>
<script src="<?php echo CDN_PATH; ?>daptorik_preview/js/report_date_range.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/floatthead/2.0.3/jquery.floatThead.min.js"></script>

<script>
    var unit_ids = [];
    var unit_names = [];
    var totalUser = 0;
    var totalInbox = 0;
    var totalNisponnoDak = 0;
    var totalONisponnoDak = 0;
    var totalDaksohoNote = 0;
    var totalSouddog = 0;
    var totalNisponnoPotrojari = 0;
    var totalNisponnoNote = 0;
    var totalONisponnoNote = 0;
    jQuery(document).ready(function () {
        $(this).find('body').addClass('page-sidebar-closed');
        $(this).find('.page-sidebar-menu').addClass('page-sidebar-menu-closed');
        DateRange.init();
        DateRange.initDashboardDaterange();


        $('#office-unit-id').change(function () {
            unit_ids = [];
             unit_names = [];
               if ($(this).val() == -1) {
                  return;
              }
            if ($(this).val() == 0) {
//                $.each($('#office-unit-id > option'), function (i, v) {
//                    if ($(this).val() != 0) {
//                        unit_ids.push($(this).val());
//                    }
//                });
//                $(".unit_ids_length").val(unit_ids.length);
//                SortingDate.init($('.startdate').val(), $('.enddate').val());

            } else {
                unit_ids.push($(this).val());
                unit_names.push($('#office-unit-id :selected').text());
                $(".unit_ids_length").val(unit_ids.length);
              
            }
              SortingDate.init($('.startdate').val(), $('.enddate').val());

        });
        $('#office-unit-id').trigger('change');
           $('.table').floatThead({
	position: 'absolute',
                    top: jQuery("div.navbar-fixed-top").height()
            });
            $('.table').floatThead('reflow');
    });
     function callUnits(cnt, date_start, date_end,indx) {
           if (cnt == 0) {
                  initializeCount();
                 Metronic.blockUI({
                    target: '#searchPanel',
                    boxed: true,
                    message: 'অপেক্ষা করুন'
                });
            }
            if (cnt == unit_ids.length) {
                 printTotal();
               Metronic.unblockUI('#searchPanel');
                $('#showlist').html('');
                return;
            }
            if(isEmpty(indx)){
                indx = cnt;
            }
            $('#showlist').html('<img src="<?= CDN_PATH ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp; লোড হচ্ছে  <b>' + '</b> । একটু অপেক্ষা করুন... </span><br>');
            $("#headText").html(""+ $("#office-id :selected").text()+"</br><q>"+ $("#office-unit-id :selected").text() +"</q> শাখার পদবিভিত্তিক কার্যবিবরণী ( "+BnFromEng($('.startdate').val())+" - "+BnFromEng($('.enddate').val()) +")");

            var unit_id = unit_ids[cnt];
            var unit_name = unit_names[cnt];
            $.ajax({
                type: 'POST',

                url: "<?php echo $this->Url->build(['controller' => 'Performance', 'action' => 'newDesignationContent']) ?>/"+ date_start + '/' + date_end,
                        //                dataType: 'json',
                        data: {"office_unit_id": unit_id,'unit_name':unit_name},
                success: function (data_all) {
                    $.each(data_all,function(i,v){
                        var data = v.result;
                    totalUser = parseInt(indx+1);
                    totalInbox += parseInt(data.totalInbox);
                    totalNisponnoDak += parseInt(data.totalNisponnoDak);
                    totalONisponnoDak += parseInt(data.totalONisponnoDak);
                    totalDaksohoNote += parseInt(data.totalDaksohoNote);
                    totalSouddog += parseInt(data.totalSouddog);
                    totalNisponnoPotrojari += parseInt(data.totalNisponnoPotrojari);
                    totalNisponnoNote += parseInt(data.totalNisponnoNote);
                    totalONisponnoNote += parseInt(data.totalONisponnoNote);
//                        console.log(data.name);
                  var  toAdd = '<tr>' +
                           '<td class="text-center" ><b>' +BntoEng(++indx) + '</b></td> ' +
                            '<td class="text-center" ><b>' + data.name + '</b></td> ' +
                            '<td class="text-center"> <b>' + BntoEng(data.lastUpdate) + '</b></td>' +

                            '<td class="text-center"> <b>' + BntoEng(data.totalInbox) + '</b></td>' +
                            '<td class="text-center"><b>' + BntoEng(data.totalNisponnoDak) + '</b></td>' +
                            '<td class="text-center"><b>' + BntoEng(data.totalONisponnoDak) + '</b></td>' +

                            '<td class="text-center"><b>' + BntoEng(data.totalDaksohoNote) + '</b></td>' +
                            '<td class="text-center"><b>' + BntoEng(data.totalSouddog) + '</b></td>' +
                            '<td class="text-center"><b>' + BntoEng(parseInt(data.totalNisponnoPotrojari)) + '</b></td>' +
                            '<td class="text-center"><b>' + BntoEng(parseInt(data.totalNisponnoNote)) + '</b></td>' +
                            '<td class="text-center"><b>' + BntoEng(data.totalONisponnoNote) + '</b></td> ' +
                            '</tr>';
                    $('#addData').append(toAdd);
                    });
                    callUnits(cnt + 1, date_start, date_end,indx);
                },
                 error: function () {
                     callUnits(cnt + 1, date_start, date_end,indx);
                }
            });
//                break;
//        }

        }
         function BntoEng(input) {
            var numbers = {
                    0: '০',
                    1: '১',
                    2: '২',
                    3: '৩',
                    4: '৪',
                    5: '৫',
                    6: '৬',
                    7: '৭',
                    8: '৮',
                    9: '৯'
                };
                var output = '';

                if (typeof (input) == 'number') {
                    input = input.toString();
                }
                for (var i = 0; i < input.length; ++i) {
                    if (numbers.hasOwnProperty(input[i])) {
                        output += numbers[input[i]];
                    } else {
                        output += input[i];
                    }
                }
                return output;

    }
     $('#office-id').change(function () {
            if(isEmpty($('#office-id').val())){
                return;
          }
            var promise = new Promise(function (resolve, reject) {
                PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + 'officeManagement/loadOfficeUnits',{'office_id': $('#office-id').val()}, 'html',
                function (response) {
                    $("#office-unit-id").html(response);
                    resolve(1);
                });
            }).then(function (res) {
                $("#office-unit-id > option[value=0]").val(-1);
                $('#office-unit-id').trigger('change');
                 $("#office-unit-id").append('<option value="0">সকল</option>');
            }).catch(function (err) {
                return false;
            });

        });
         function printTotal(){
        var  toAdd = '<tr>' +
                            '<td class="text-center" colspan="2"><b>মোটঃ ' +BntoEng(totalUser) + '</b></td> ' +
                            '<td class="text-center"> <b></b></td>' +
                            '<td class="text-center"> <b>' + BntoEng(totalInbox) + '</b></td>' +
                            '<td class="text-center"><b>' + BntoEng(totalNisponnoDak) + '</b></td>' +
                            '<td class="text-center"><b>' + BntoEng(totalONisponnoDak) + '</b></td>' +

                            '<td class="text-center"><b>' + BntoEng(totalDaksohoNote) + '</b></td>' +
                            '<td class="text-center"><b>' + BntoEng(totalSouddog) + '</b></td>' +
                            '<td class="text-center"><b>' + BntoEng(parseInt(totalNisponnoPotrojari)) + '</b></td>' +
                            '<td class="text-center"><b>' + BntoEng(parseInt(totalNisponnoNote)) + '</b></td>' +
                            '<td class="text-center"><b>' + BntoEng(totalONisponnoNote) + '</b></td> ' +
                            '</tr>';
                    $('#addData').append(toAdd);
    }
    function initializeCount(){
    totalUser = 0;
    totalInbox = 0;
    totalNisponnoDak = 0;
    totalONisponnoDak = 0;
    totalDaksohoNote = 0;
    totalSouddog = 0;
    totalNisponnoPotrojari = 0;
    totalNisponnoNote = 0;
    totalONisponnoNote = 0;
    }
  var  all_designation = [];
  var  designation_unit = [];
  var  designation_names = [];
    function getAllDesignation(date_start, date_end){
        all_designation = [];
        designation_unit = [];
         designation_names = [];
        unit_names = [];
        if(isEmpty($('#office-id').val())){
            return;
        }
          $.ajax({
                type: 'POST',

                url: "<?php echo $this->Url->build(['controller' => 'Performance', 'action' => 'getAllDesignationByOfficeOrUnitID']) ?>/"+$('#office-id').val()+"/0"+'/'+date_start+'/'+date_end,
                        data: {},
                success: function (data_all) {
                    $.each(data_all,function(i,v){
                         all_designation.push(v.designation_id);
                        designation_unit.push(v.unit_id);
                        designation_names.push(v.designation_name);
                        unit_names.push(v.unit_name);
                    });
                    loadSingleDesignation(0,date_start,date_end);
                },
                 error: function () {
                }
            });
    }
     function loadSingleDesignation(cnt, date_start, date_end,indx) {
               if (cnt == 0) {
                   initializeCount();
                 Metronic.blockUI({
                    target: '#searchPanel',
                    boxed: true,
                    message: 'অপেক্ষা করুন'
                });
            }
            if (cnt == all_designation.length) {

                printTotal();
               Metronic.unblockUI('#searchPanel');
                $('#showlist').html('');
                return;
            }
            if(isEmpty(indx)){
                indx = cnt;
            }
            $('#showlist').html('<img src="<?= CDN_PATH ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp; লোড হচ্ছে  <b>' + '</b> । একটু অপেক্ষা করুন... </span><br>');
            $("#headText").html("<q>" + $("#office-unit-id :selected").text() +"</q> শাখার পদবিভিত্তিক কার্যবিবরণী ( "+BnFromEng($('.startdate').val())+" - "+BnFromEng($('.enddate').val()) +")");

            var designation_id = all_designation[cnt];
            var unit_id = designation_unit[cnt];
            var office_id =$('#office-id').val();
             var designation_name = designation_names[cnt];
            var unit_name = unit_names[cnt];
            $.ajax({
                type: 'POST',

                url: "<?php echo $this->Url->build(['controller' => 'Performance', 'action' => 'singleDesignationContent']) ?>/"+ date_start + '/' + date_end,
                        //                dataType: 'json',
                        data: {"office_unit_organogram_id": designation_id,'office_unit_id' : unit_id,'office_id' : office_id,'designation_name':designation_name,'unit_name':unit_name},
                success: function (data_all) {
                    $.each(data_all,function(i,v){
                        var data = v.result;
                    totalUser = parseInt(indx+1);
                    totalInbox += parseInt(data.totalInbox);
                    totalNisponnoDak += parseInt(data.totalNisponnoDak);
                    totalONisponnoDak += parseInt(data.totalONisponnoDak);
                    totalDaksohoNote += parseInt(data.totalDaksohoNote);
                    totalSouddog += parseInt(data.totalSouddog);
                    totalNisponnoPotrojari += parseInt(data.totalNisponnoPotrojari);
                    totalNisponnoNote += parseInt(data.totalNisponnoNote);
                    totalONisponnoNote += parseInt(data.totalONisponnoNote);
//                        console.log(data.name);
                  var  toAdd = '<tr>' +
                           '<td class="text-center" ><b>' +BntoEng(++indx) + '</b></td> ' +
                            '<td class="text-center" ><b>' + data.name + '</b></td> ' +
                            '<td class="text-center"> <b>' + BntoEng(data.lastUpdate) + '</b></td>' +

                            '<td class="text-center"> <b>' + BntoEng(data.totalInbox) + '</b></td>' +
                            '<td class="text-center"><b>' + BntoEng(data.totalNisponnoDak) + '</b></td>' +
                            '<td class="text-center"><b>' + BntoEng(data.totalONisponnoDak) + '</b></td>' +

                            '<td class="text-center"><b>' + BntoEng(data.totalDaksohoNote) + '</b></td>' +
                            '<td class="text-center"><b>' + BntoEng(data.totalSouddog) + '</b></td>' +
                            '<td class="text-center"><b>' + BntoEng(parseInt(data.totalNisponnoPotrojari)) + '</b></td>' +
                            '<td class="text-center"><b>' + BntoEng(parseInt(data.totalNisponnoNote)) + '</b></td>' +
                            '<td class="text-center"><b>' + BntoEng(data.totalONisponnoNote) + '</b></td> ' +
                            '</tr>';
                    $('#addData').append(toAdd);
                    });
                    loadSingleDesignation(cnt + 1, date_start, date_end,indx);
                },
                 error: function () {
                     loadSingleDesignation(cnt + 1, date_start, date_end,indx);
                }
            });
//                break;
//        }

        }
</script>