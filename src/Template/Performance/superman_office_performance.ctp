<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
            <?= __('Office').__('Performance Details') ?>
        </div>
    </div>
    <div class="portlet-body">
        <?= $this->Cell('OfficeSelection'); ?>
        <div class="row" id="showlist">

        </div>
        <div class="row">
            <div class="col-md-4">
                        <!--<select class="form-control select2-offscreen">-->
                <?php
                echo $this->Form->create('', ['class' => 'searchForm'])
                ?>
                <?php
                echo $this->Form->hidden('start_date', ['class' => 'startdate'])
                ?>
                <?php
                echo $this->Form->hidden('end_date', ['class' => 'enddate']);
//                echo $this->Form->hidden('office_id', [])
                ?>
                <?php
                echo $this->Form->hidden('office_ids', ['class' => 'officeids']);
//                echo $this->Form->hidden('office_id', [])
                ?>
                <?php echo $this->Form->end() ?>
                <!--</select>-->

            </div>


            <!--Tools end -->

            <!--  Date Range Begain -->
            <div class="col-md-6 col-sm-12 pull-right">
                <div class="hidden-print page-toolbar pull-right portlet-title">
                    <div id="dashboard-report-range" class=" tooltips btn btn-sm btn-default"
                         data-container="body"
                         data-placement="bottom" data-original-title="তারিখ নির্বাচন করুন">
                        <i class="icon-calendar"></i>&nbsp; <span
                            class="thin uppercase visible-lg-inline-block">তারিখ নির্বাচন করুন</span>&nbsp;
                        <i class="glyphicon glyphicon-chevron-down"></i>
                    </div>
                </div>
            </div>
            <!--  Date Range End  -->

        </div>
        <br/>
        <div class="inbox-content">

        </div>
    </div>
</div>
<script src="<?php echo CDN_PATH; ?>js/reports/superadmin_office_performance.js" type="text/javascript"></script>
<script src="<?php echo CDN_PATH; ?>daptorik_preview/js/report_date_range.js"></script>
<script>
    var EmployeeAssignment = {
        checked_node_ids: [],
        loadMinistryWiseLayers: function () {
            OfficeSetup.loadLayers($("#office-ministry-id").val(), function (response) {

            });
        },
        loadMinistryAndLayerWiseOfficeOrigin: function () {
            OfficeSetup.loadOffices($("#office-ministry-id").val(), $("#office-layer-id").val(), function (response) {

            });
        },
    };

</script>


<script>
    jQuery(document).ready(function () {
        $(this).find('body').addClass('page-sidebar-closed');
        $(this).find('.page-sidebar-menu').addClass('page-sidebar-menu-closed');
        DateRange.init();
        DateRange.initDashboardDaterange();

        $("#office-ministry-id").bind('change', function () {
            EmployeeAssignment.loadMinistryWiseLayers();
        });
        $("#office-layer-id").bind('change', function () {
            EmployeeAssignment.loadMinistryAndLayerWiseOfficeOrigin();
        });
        $("#office-origin-id").bind('change', function () {
            getOfficesId($(this).val());

        });
        function getOfficesId(office_origin_id) {
            $('#showlist').html('<img src="<?= CDN_PATH ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp; লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');
            $.ajax({
                type: 'POST',
                url: "<?php echo $this->Url->build(['controller' => 'officeManagement', 'action' => 'loadOriginOffices']) ?>",
                dataType: 'json',
                data: {"office_origin_id": office_origin_id},
                success: function (data) {
                    $('#showlist').html('');
                    var office_ids = '';
                    var ind = 0;
//                     console.log();
                    $.each(data, function (i, v) {
                        if (ind != 0) {
                            office_ids = office_ids + ',';
                        }
                        office_ids = office_ids + i;
                        ind++;
                        if (ind == Object.keys(data).length) {
//                           console.log(office_ids);

                            $('#showlist').html('<div class="hidden" id="office_ids_list">' + office_ids + '</div> ');
                            callData();
                        }
                    });

                }
            });
        }
    });
    function callData() {
        DateRange.init();
        DateRange.initDashboardDaterange();
//         SortingDate.init($('.startdate').val(), $('.enddate').val());
    }
</script>
