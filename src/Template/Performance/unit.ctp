<script src="<?php echo CDN_PATH; ?>daptorik_preview/js/report_date_range.js"></script>
<script src="<?php echo CDN_PATH; ?>js/reports/new_unit_performance.js?v=<?= js_css_version ?>" type="text/javascript"></script>
<div class="portlet light ">
    <div class="portlet-title">
        <div class="caption"><i class="fs1 a2i_gn_details1"></i> শাখা ভিত্তিক ড্যাশবোর্ড  </div>
    </div>
    <div class="portlet-body">
        <div class="row"   id="searchPanel">
            <label class="control-label col-md-2 text-right font-lg">শাখাঃ</label>
            <div class="col-md-4">
                        <!--<select class="form-control select2-offscreen">-->
                <?php
                echo $this->Form->create('', ['class' => 'searchForm'])
                ?>
                <?php
                echo $this->Form->hidden('start_date', ['class' => 'startdate'])
                ?>
                <?php
                echo $this->Form->hidden('end_date', ['class' => 'enddate']);
                echo $this->Form->hidden('unit_ids_length', ['class' => 'unit_ids_length'])
                ?>

                <?php
                if (empty($selected_office_section)) {
                    echo $this->Form->input('office_unit_id',
                        array('label' => false, 'type' => 'select', 'class' => 'form-control',
                            'options' =>  !empty($ownChildInfo)?$ownChildInfo:[],'empty'=>__("All")));
                }
                else {
                    echo $this->Form->input('office_unit_id',
                        array('label' => false, 'type' => 'select', 'class' => 'form-control',
                            'options' =>  !empty($ownChildInfo)?$ownChildInfo:[],'empty'=>__("All"), 'default' => $selected_office_section['office_unit_id']));
                }
                ?>
                <?php echo $this->Form->end() ?>
                <!--</select>-->

            </div>


            <!--Tools end -->

            <!--  Date Range Begain -->
            <div class="col-md-6 col-sm-12 pull-right">
                <div class="col-md-10 col-sm-10">
                         <div class="hidden-print page-toolbar pull-right portlet-title">
                            <div id="dashboard-report-range" class=" tooltips btn btn-sm btn-default"
                                 data-container="body"
                                 data-placement="bottom" data-original-title="তারিখ নির্বাচন করুন">
                                <i class="icon-calendar"></i>&nbsp; <span
                                    class="thin uppercase visible-lg-inline-block">তারিখ নির্বাচন করুন</span>&nbsp;
                                <i class="glyphicon glyphicon-chevron-down"></i>
                            </div>
                        </div>
                </div>
                  <div class="col-md-2 col-sm-2">
                    <button type="button" class="btn  btn-primary btn-md btn-performance-print"> <i class="fa fa-print">&nbsp;<?=__('Print')?></i> </button>
                </div>
            </div>
            <!--  Date Range End  -->

        </div>
        <br/>
        <div class="inbox-content">
            <div id="showlist"></div><br>
            <div class="table-container " id="UnitPerformance">
                <table class="table table-bordered table-hover tableadvance">
                    <thead>
                        <tr class="heading">
                            <th colspan="12" class="text-center" id="headText"></th>
                        </tr>
                        <tr class="heading">
                            <th class="text-center" rowspan="2">ক্রমিক</th>
                            <th class="text-center" rowspan="2">শাখার নাম</th>
                            <th class="text-center" rowspan="2"> ব্যবহারকারী</th>
                            <th class="text-center" rowspan="2"> সর্বশেষ হালনাগাদ</th>
                            <th colspan="3" class="text-center"> ডাক </th>
                            <th colspan="5" class="text-center"> নথি </th>
                        </tr>
                        <tr class="heading">
                            <th class="text-center" >মোট  গ্রহণ </th>
                            <th class="text-center" >মোট নিষ্পন্ন  </th>
                            <th class="text-center" >মোট অনিষ্পন্ন </th>

                            <th class="text-center" >মোট ডাক থেকে সৃজিত নোট </th>
                            <th class="text-center" >মোট  স্ব- উদ্যোগে নোট </th>
                            <th class="text-center" > পত্রজারিতে নিষ্পন্ন নোট
                            </th>
                            <th class="text-center" >মোট  নোটে নিষ্পন্ন
                            </th>
                            <th class="text-center" >মোট অনিষ্পন্ন নোট </th>

                        </tr>
                    </thead>
                    <tbody id="addData">

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<script src="<?=$this->request->webroot?>assets/global/scripts/printThis.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/floatthead/2.0.3/jquery.floatThead.min.js"></script>
<script>
    var unit_ids = [];
    var totalUser = 0;
    var totalInbox = 0;
    var totalNisponnoDak = 0;
    var totalONisponnoDak = 0;
    var totalDaksohoNote = 0;
    var totalSouddog = 0;
    var totalNisponnoPotrojari = 0;
    var totalNisponnoNote = 0;
    var totalONisponnoNote = 0;
    jQuery(document).ready(function () {
        $(this).find('body').addClass('page-sidebar-closed');
        $(this).find('.page-sidebar-menu').addClass('page-sidebar-menu-closed');
        DateRange.init();
        DateRange.initDashboardDaterange();
        $('#office-unit-id').change(function () {
             unit_ids = [];
            if ($(this).val() == 0) {
                $.each($('#office-unit-id > option'), function (i, v) {
                    if ($(this).val() != 0) {
                        unit_ids.push($(this).val());
                    }
                });
                 $(".unit_ids_length").val(unit_ids.length);
                 SortingDate.init($('.startdate').val(), $('.enddate').val());
               
            } else {
                    unit_ids.push($(this).val());
                  $(".unit_ids_length").val(unit_ids.length);
                SortingDate.init($('.startdate').val(), $('.enddate').val());
            }
        });
         $('#office-unit-id').trigger('change');
        $('.table').floatThead({
	position: 'absolute',
                    top: jQuery("div.navbar-fixed-top").height()
        });
            $('.table').floatThead('reflow');

    });
    function callUnits(cnt, date_start, date_end) {
            if (cnt == 0) {
                initializeCount();
                 Metronic.blockUI({
                    target: '#searchPanel',
                    boxed: true,
                    message: 'অপেক্ষা করুন'
                });
            }
            if (cnt == unit_ids.length) {
                printTotal();
               Metronic.unblockUI('#searchPanel');
                $('#showlist').html('');
                return;
            }
            $('#showlist').html('<img src="<?= CDN_PATH ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp; লোড হচ্ছে  <b>' + '</b> । একটু অপেক্ষা করুন... </span><br>');
             $("#headText").html("<q>" + $("#office-unit-id :selected").text() +"</q> শাখার কার্যবিবরণী ( "+BnFromEng($('.startdate').val())+" - "+BnFromEng($('.enddate').val()) +")");
            var unit_id = unit_ids[cnt];
            $.ajax({
                type: 'POST',

                url: "<?php echo $this->Url->build(['controller' => 'Performance', 'action' => 'newUnitContent']) ?>/"+ date_start + '/' + date_end,
                        //                dataType: 'json',
                        data: {"office_unit_id": unit_id},
                success: function (data) {
                    totalUser += parseInt(data.totalUser);
                    totalInbox += parseInt(data.totalInbox);
                    totalNisponnoDak += parseInt(data.totalNisponnoDak);
                    totalONisponnoDak += parseInt(data.totalONisponnoDak);
                    totalDaksohoNote += parseInt(data.totalDaksohoNote);
                    totalSouddog += parseInt(data.totalSouddog);
                    totalNisponnoPotrojari += parseInt(data.totalNisponnoPotrojari);
                    totalNisponnoNote += parseInt(data.totalNisponnoNote);
                    totalONisponnoNote += parseInt(data.totalONisponnoNote);
                  var  toAdd = '<tr>' +
                            '<td class="text-center" ><b>' +enTobn(cnt+1) + '</b></td> ' +
                            '<td class="text-center" ><b>' + data.name + '</b></td> ' +
                            '<td class="text-center"> <b>' + enTobn(data.totalUser) + '</b></td>' +
                            '<td class="text-center"> <b>' + enTobn(data.lastUpdate) + '</b></td>' +
                            '<td class="text-center"> <b>' + enTobn(data.totalInbox) + '</b></td>' +
                            '<td class="text-center"><b>' + enTobn(data.totalNisponnoDak) + '</b></td>' +
                            '<td class="text-center"><b>' + enTobn(data.totalONisponnoDak) + '</b></td>' +
                            
                            '<td class="text-center"><b>' + enTobn(data.totalDaksohoNote) + '</b></td>' +
                            '<td class="text-center"><b>' + enTobn(data.totalSouddog) + '</b></td>' +
                            '<td class="text-center"><b>' + enTobn(parseInt(data.totalNisponnoPotrojari)) + '</b></td>' +
                            '<td class="text-center"><b>' + enTobn(parseInt(data.totalNisponnoNote)) + '</b></td>' +
                            '<td class="text-center"><b>' + enTobn(data.totalONisponnoNote) + '</b></td> ' +
                            '</tr>';
                    $('#addData').append(toAdd);
                    callUnits(cnt + 1, date_start, date_end);

                },
                error: function () {
                     callUnits(cnt + 1, date_start, date_end);
                }
            });
//                break;
//        }

        }

    function printTotal(){
        var  toAdd = '<tr>' +
                            '<td class="text-center" colspan="2"><b>মোটঃ ' +enTobn(unit_ids.length) + '</b></td> ' +
                            '<td class="text-center"> <b>' + enTobn(totalUser) + '</b></td>' +
                            '<td class="text-center"> <b></b></td>' +
                            '<td class="text-center"> <b>' + enTobn(totalInbox) + '</b></td>' +
                            '<td class="text-center"><b>' + enTobn(totalNisponnoDak) + '</b></td>' +
                            '<td class="text-center"><b>' + enTobn(totalONisponnoDak) + '</b></td>' +

                            '<td class="text-center"><b>' + enTobn(totalDaksohoNote) + '</b></td>' +
                            '<td class="text-center"><b>' + enTobn(totalSouddog) + '</b></td>' +
                            '<td class="text-center"><b>' + enTobn(parseInt(totalNisponnoPotrojari)) + '</b></td>' +
                            '<td class="text-center"><b>' + enTobn(parseInt(totalNisponnoNote)) + '</b></td>' +
                            '<td class="text-center"><b>' + enTobn(totalONisponnoNote) + '</b></td> ' +
                            '</tr>';
                    $('#addData').append(toAdd);
    }
    function initializeCount(){
    totalUser = 0;
    totalInbox = 0;
    totalNisponnoDak = 0;
    totalONisponnoDak = 0;
    totalDaksohoNote = 0;
    totalSouddog = 0;
    totalNisponnoPotrojari = 0;
    totalNisponnoNote = 0;
    totalONisponnoNote = 0;
    }
</script>