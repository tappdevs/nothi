<div class="portlet light ">
    <div class="portlet-title">
        <div class="caption"><i class="fs1 a2i_gn_details1"></i> শাখা ভিত্তিক অনিষ্পন্ন  </div>
    </div>
    <div class="portlet-body">
    <div class="row">
        <div class="col-md-10">
            <div class="inbox-content">
            </div>
        </div>
        <div class="col-md-2 text-right">
            <button type="button" class="btn  btn-primary btn-md btn-pending-print" style="display: none"> <i class="fa fa-print">&nbsp;প্রিন্ট</i> </button>
        </div>
    </div>
        <div class="table-container ">
            <table class="table table-bordered table-hover tableadvance">
                <thead>
                    <tr class="heading">
                        <th class="text-center"> ক্রম</th>
                        <th class="text-center" rowspan="2">শাখার নাম</th>
                        <th  class="text-center">অনিষ্পন্ন ডাক </th>
                        <th  class="text-center">অনিষ্পন্ন নথি </th>

                    </tr>
                </thead>
                <tbody id ="addData">

                </tbody>
            </table>
        </div>
    </div>
</div>
<style>
    @media print {
        table {
            border:1px solid grey !important;
        }
        th, td {
            border:1px solid grey !important;
            padding: 10px!important;
        }
    }
</style>
<script>
        var dak = 0;
        var note = 0;
    jQuery(document).ready(function () {
        var units_info = <?= json_encode($ownChildInfo_sorted); ?>;
        var units = [];
        for (var ind  in units_info) {
            units.push(ind);
            if (units.length == <?= count($ownChildInfo_sorted); ?>) {
                UnitPendingData(0);
            }
        }
//        console.log(units_info);
//       if(units.lentgh > 0){
//             UnitPendingData(0);
//       }
        function UnitPendingData(index) {
            if (index >= <?= count($ownChildInfo); ?>) {
                printTotal(index);
                 $('.inbox-content').html('');
                return;
            }
            $('.inbox-content').html('<img src="<?= CDN_PATH ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp; লোড হচ্ছে <b>'+ units_info[index][1] + '</b> । একটু অপেক্ষা করুন... </span>');
            $.ajax({
                type: 'POST',
                url: "<?php echo $this->Url->build(['controller' => 'Performance', 'action' => 'unitPendingReports']) ?>",
                data: {"unit_id": units_info[index][0], 'office_id': <?= $selected_office_section['office_id'] ?>},
                success: function (data) {
                    if (data.status == 'false' || data.status == false) {
                        UnitPendingData(index + 1);
                    } else {
                        dak += parseInt(data.dak);
                        note += parseInt(data.note);
                        $('#addData').append('<tr class="text-center"><td>' + BntoEng(index + 1)+ '</td><td>' + units_info[index][1] + '</td><td>' + BntoEng(data.dak) + '</td><td>' + BntoEng(data.note) + '</td><tr>');
                        UnitPendingData(index + 1);
                    }

                }
            });
        }
    });
     function BntoEng(input) {
        var numbers = {
            0: '০',
            1: '১',
            2: '২',
            3: '৩',
            4: '৪',
            5: '৫',
            6: '৬',
            7: '৭',
            8: '৮',
            9: '৯'
        };

        var output = '';
        if (parseInt(input) != 0) {
            while (parseInt(input) != 0) {
                var n = parseInt(input % 10);
                if (!Number.isInteger(n))
                    break;
                input = input / 10;
                output += (numbers[n]);
            }
        } else
        {
            output = '০';
        }
        for (var i = output.length - 1, o = ''; i >= 0; i--)
        {
            o += output[i];
        }
        return o;

    }
    function printTotal(total){
          $('#addData').append('<tr class="text-center"><td colspan="2"><b>মোটঃ ' +BntoEng(total) + '</b></td><td><b>' + BntoEng(dak) + '</b></td><td><b>' + BntoEng(note) + '</b></td><tr>');
          $('.btn-pending-print').show();
    }
        $(document).on('click', '.btn-pending-print', function () {
            $(document).find('.table').printThis({
                importCSS: true,
                debug: false,
                importStyle: true,
                printContainer: false,
                pageTitle: "",
                removeInline: false,
                header: null
            });
        });
</script>
<script src="<?=$this->request->webroot?>assets/global/scripts/printThis.js"></script>