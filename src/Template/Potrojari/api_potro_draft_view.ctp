<style>
    #সংরক্ষণ-1{
        width: 76px;
        color: #0098f7;
        border-color: #0098f7;
        /*background: #0098f7;*/
    }
    #ফেরত-যান-1{
        width: 90px;
        color: red;
    }
</style>
    <form action="<?php echo $this->Url->build(['_name'=>'postApiPotroDraft', $nothi_part_id, $potrojari_id,$type,$nothi_office,true]);
    ?>" method="post" id="postApiPotroDraftReturn">
    <input type="hidden" name="api_key" value="<?= $api_key ?>">
    <input type="hidden" name="user_designation" value="<?= $user_designation ?>">
    <input type="hidden" name="data_ref" value="api">
    <input type="hidden" name="noteViewData" id="noteViewData" value="">
    <input type="hidden" name="height" id="height" value="<?= $height ?>">
    <input type="hidden" name="width" id="width" value="<?= $width ?>">
    <input type="hidden" name="is_approaved_flag" id="is_approaved_flag" value="<?= $is_approaved_flag ?>">

</form>
<?= $this->element('froala_editor_js_css') ?>
<div id="editor">
    <textarea name="editor_content" id="noteComposer"><?= !empty($noteViewData)?$noteViewData:'' ?></textarea>
</div>
<script>
    $(document).ready(function(){
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-top-center"
        };
    });


    $.FroalaEditor.RegisterCommand('সংরক্ষণ', {
        title: 'খসড়া সংরক্ষণ করুন',
        focus: false,
        undo: false,
        showOnMobile: true,
        refreshAfterCallback: false,
        callback: function (cmd, val) {
            var body = $('#noteComposer').froalaEditor('html.get');
            if ( ($('#noteComposer').froalaEditor('html.get') == "") || ($('#noteComposer').froalaEditor('html.get') == "<br><br>")) {
                toastr.error('কোনো খসড়া দেয়া হয়নি');
                $('.fr-view').focus();
                return;
            }
            Metronic.blockUI({
                target: '.fr-box',
                boxed: true
            });
            $("#noteViewData").val(body);
            $("#postApiPotroDraftReturn").submit();


        }
    });
    $.FroalaEditor.RegisterCommand('ফেরত-যান', {
        title: 'খসড়ায় ফিরে যান',
        focus: false,
        undo: false,
        showOnMobile: true,
        refreshAfterCallback: false,
        callback: function (cmd, val) {
            $("#noteViewData").val('');
            $("#postApiPotroDraftReturn").submit();
        }
    });
    var buttons = ['bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', '|', 'fontFamily', 'fontSize', 'color', 'inlineStyle', 'paragraphStyle', '|', 'paragraphFormat', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote', 'insertLink', 'insertTable', '|', 'emoticons', 'insertHR', 'selectAll', 'clearFormatting', '|', 'print', 'undo', 'redo','|','ফেরত-যান','সংরক্ষণ', 'fullscreen'];
    var buttonsSm = ['bold', 'underline','|', 'fontSize', 'align', '|','formatOL', 'formatUL', 'outdent', 'indent','|','ফেরত-যান','সংরক্ষণ', 'fullscreen'];
    $('#noteComposer').froalaEditor({
        key: 'xc1We1KYi1Ta1WId1CVd1F==',
        toolbarSticky: false,
        wordPasteModal: true,
		wordAllowedStyleProps: ['font-size', 'background', 'color', 'text-align', 'vertical-align', 'background-color', 'padding', 'margin', 'height', 'margin-top', 'margin-left', 'margin-right', 'margin-bottom', 'text-decoration', 'font-weight'],
		wordDeniedTags: ['a','form'],
		wordDeniedAttrs: ['width'],
        tableResizerOffset: 10,
        tableResizingLimit: 20,
        toolbarButtons: buttons,
        toolbarButtonsMD: buttons,
        toolbarButtonsSM: buttonsSm,
        toolbarButtonsXS: buttonsSm,
        placeholderText: '',
        height: <?= (empty($height)?500:$height) ?>,
        //width: <?= (empty($width)?360:$width) ?>,
        enter: $.FroalaEditor.ENTER_BR,
        fontSize: ['10','11', '12','13', '14','15','16','17', '18','19','20','21','22','23','24','25','26','27','28','29', '30'],
        fontSizeDefaultSelection: '13',
        tableEditButtons:['tableHeader', 'tableRemove', '|', 'tableRows', 'tableColumns','tableCells', '-',  'tableCellBackground', 'tableCellVerticalAlign', 'tableCellHorizontalAlign', 'tableCellStyle'],
        fontFamily: {
            "Nikosh": "Nikosh",
            "Arial": "Arial",
            "Kalpurush": "Kalpurush",
            "SolaimanLipi": "SolaimanLipi",
            "'times new roman'": "Times New Roman",
            "'Open Sans Condensed',sans-serif": 'Open Sans Condensed'
        }
    });
    $('#noteComposer').froalaEditor('events.focus', true);
    $('#noteComposer').froalaEditor('fullscreen.toggle');
    window.onload = function(e){
        // setTimeout(function(){
        //     document.documentElement.scrollTop = 0;
        // }, 1000);
        $('html, body').animate({
            scrollTop: $("#editor").offset().top
        }, 2000);
    }
</script>