<?php
if (trim($data['potro_description']) != '') {
	?>
	<style>
		.A4-max {
			background: white;
			max-width: 21cm;
			min-height: 29.7cm;
			display: block;
			margin: 0 auto;
			padding-left: 0.5in;
			padding-right: 0.5in;
			padding-top: 0.5in;
			padding-bottom: 0.5in;
			margin-bottom: 0.5cm;
			box-shadow: 0 0 0.5cm rgba(0, 0, 0, 0.5);
			overflow-y: auto;
			box-sizing: border-box;
			font-size: 12pt;
		}
	</style>
    <div class="A4-max"><?= html_entity_decode($data['potro_description'])?></div>
	<?php
} else {
    echo "<p class='text-danger text-center'>পাওয়া যায়নি</p>";
}
