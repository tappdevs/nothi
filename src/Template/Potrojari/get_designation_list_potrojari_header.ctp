
<div class="row">
    <div class="col-md-12">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fs1 a2i_gn_edit2"></i> কর্মকর্তা তালিকা
                </div>
            </div>
            <div class="portlet-body">
                <div class="alert alert-info font-lg text-center">
                    <i class="fa fa-info-circle " aria-hidden="true"></i>
                   নিম্নোক্ত ব্যবহারকারীগণকে পত্রজারিতে নির্বাচন করলে অথবা লগইন অবস্থায় টপবারে শাখা দেখাবে কি দেখাবে না  সেই সিদ্ধান্ত নিতে পারেন নিম্নের তালিকার সাহায্যে (<b>অফিস প্রধানের জন্য শাখা দেখাবে না</b>)।
                </div>
	            <style>
		            .checkbox {
			            margin:0!important
		            }
	            </style>
                <table class="table table-striped table-bordered table-hover" id="sample_1">
                    <tr class="heading">
                        <th></th>
                        <th class="text-center">পদবি</th>
                        <th class="text-center">শাখার নাম </th>
                    </tr>
                    <tbody>
                    <tr class="odd gradeX">
                        <?php
                        $i = 0;
                        foreach ($allOrganogram as $rows):
                        ?>
                    <tr class="designationLabel">
                        <td class="text-center" style="padding:8px;"><?php echo $this->Number->format(++$i) ?></td>
                        <td class="designation_id" style="padding:8px;" id="<?php echo $rows['office_unit_organogram_id'] ?>" designation = "<?=$rows['designation']?>"><?php echo (!empty($rows['name_bng'])?$rows['name_bng'].', ':'').$rows['designation'] . ', ' . $rows['OfficeUnits']['unit_name_bng'] ; ?></td>
                        <td class="text-center" style="padding:5px;">
                            <?php
                            if($rows['office_head'] == 1){
                                 echo $this->Form->input('show_unit', ['type' => 'checkbox', 'label' => false, 'class' => 'make-switch', 'data-on-text' => 'দেখাবে','data-off-text' => 'দেখাবে না', 'value' => 0, 'readonly'=>'readonly' , 'checked' => false]);
                            } else {
                                echo $this->Form->input('show_unit', ['type' => 'checkbox', 'label' => false, 'class' => 'make-switch', 'data-on-text' => 'দেখাবে', 'data-off-text' => 'দেখাবে না', 'value' => $rows['show_unit'], 'checked' => (!empty($rows['show_unit']) ? (true) : (false))]);
                            }
                            ?>
                        </td>

                        <?php
                        endforeach;
                        ?>
                    </tr>
                    <tr class="form-actions">
                        <td colspan="3" class="text-center">
                            <input type="button" class="btn green saveDesignation" value="<?php echo __(SAVE); ?>" />
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<script>
jQuery(document).ready(function () {
     $(".show_unit").bootstrapSwitch();
});

    $(document).on('click','.saveDesignation', function(){
        Metronic.blockUI({
            target: '.portlet.box.green',
            boxed: true
        });
        var data2send = [];
        $('.designationLabel').each( function(i,v){
     
            var data = {
                desigantion_id:$(v).find('.designation_id').attr('id'),
                desigantion:$(v).find('.designation_id').attr('designation'),
                show_unit: (($(v).find('[name=show_unit]').is(':checked'))?1:0),
            };
            data2send.push(data);
        });
          $.ajax({
                url: '<?php echo $this->Url->build(['controller'=>'potrojari','action'=>'getDesignationListPotrojariHeader']) ?>',
                data: {data: data2send},
                method: 'post',
                success: function(res){
                    if(res.status == 'success'){
                         toastr.success(res.msg);
                    }else{
                         toastr.error(res.msg);
                    }
                    Metronic.unblockUI('.portlet.box.green');
                }
            })

    });

 </script>