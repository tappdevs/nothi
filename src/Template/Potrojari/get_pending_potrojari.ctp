<?php $canRepeat = 0; ?>
<div class="portlet box purple">
    <div class="portlet-title">
        <div class="caption">
            পত্রজারি প্রেরণ অবস্থা
        </div>
        <div class="actions">
            <button title="পুনরায় চেস্টা করুন" <?php if($total['receiver']+$total['onulipi'] == 0) { echo 'style="display:none"'; } ?> class="btn btn-sm btn-danger   btn-retry hide"><i class="fa fa-repeat"></i></button>
        </div>
    </div>
    <div class="portlet-body">
        <div class="row">
	        <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
		        <ul class="nav nav-tabs">
			        <li class="active" onclick="loadList('receiver')">
				        <a href="#receiverList" data-toggle="tab" aria-expanded="true">প্রাপক <span class="badge badge-primary"><?=enTobn($total['receiver'])?></span></a>
			        </li>
			        <li class="" onclick="loadList('onulipi')">
				        <a href="#onulipiList" data-toggle="tab" aria-expanded="false">অনুলিপি <span class="badge badge-primary"><?=enTobn($total['onulipi'])?></span></a>
			        </li>
		        </ul>
		        <div class="tab-content">
			        <div class="tab-pane fade active in" id="receiverList">

			        </div>
			        <div class="tab-pane fade" id="onulipiList">

			        </div>
		        </div>
	        </div>
        </div>
    </div>
</div>

<script>
	$(document).ready(function() {
        loadList('receiver');

        $(document).off('click','.pagination a').on('click','.pagination a',function(ev){
            ev.preventDefault();
            var params =getParams($(this).attr('href'));
            loadList(params.data_for, params.page);
        });
    });
	function loadList($list_type, page) {
	    if (page == undefined) {
            page = 1;
	    }
	    Metronic.blockUI({
		    target: '#'+$list_type+'List',
            boxed: true,
	    });
        $.ajax({
            url: 'Potrojari/pendingPotrojariGet',
            method: 'get',
            data: {
                data_for: $list_type,
	            page: page
            },
            dataType: 'html',
            success: function(response) {
                $('#'+$list_type+'List').html(response);
                Metronic.unblockUI('#'+$list_type+'List');
            }
        });
	}
    $(function () {
        var canrepeat = <?= $canRepeat ?>;
        var organogramId = <?= $employee_office['office_unit_organogram_id'] ?>;
        if (canrepeat == 1) {
            $('.btn-retry').removeClass('hide');
        }

        var potrojaridata = [];
        $.map($('tbody>tr'), function (val, i) {
            if (typeof ($(val).data('potrojari-id')) != 'undefined') {
                potrojaridata[$(val).data('potrojari-id')] = $(val).data('potrojari-id');
            }
        });

        $('.btn-retry').click(function () {
            Metronic.blockUI({target:'#ajax-content',animate: true});
            setTimeout(function(){
                PROJAPOTI.ajaxSubmitAsyncDataCallbackWithoutAbrot('<?= $this->Url->build(['controller'=>'Potrojari','action'=>'retrySentAll',$office_id]) ?>/'+organogramId,{},'json',function(response){
                    if(response.status=='success'){
                        Metronic.unblockUI('#ajax-content');
                        toastr.success("ব্যাকগ্রাউন্ড প্রসেস চলছে। কিছুক্ষণ পর রিফ্রেশ করলে প্রেরণ অবস্থা জানা যাবে। ধন্যবাদ। ");
                    } else {
                        Metronic.unblockUI('#ajax-content');
                        toastr.error("দুঃখিত! কিছুক্ষণ পর পুনরায় চেষ্টা করুন। ");
                    }
                });
            },1000);
            
        });

    })
</script>
