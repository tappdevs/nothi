<style>
    .htmlPreview {
        height: 300pt;
        text-align: center;
        padding: 10px;
    }
</style>
<div class="panel panel-primary">
    <div class="panel-heading">
        <div class="panel-title"><i class="fa fa-pencil-square" aria-hidden="true"></i> শাখাভিত্তিক পত্রজারি হেডিং সংশোধন </div>
    </div>
    <div class="panel-body">
        <div class="row">
            <label class="control-label col-md-2 col-sm-2 text-right"> <?php echo __("Unit") ?> </label>
            <div class="col-md-4 col-sm-4">
                <?php
                echo $this->Form->input('office_unit_id',
                    array(
                    'label' => false,
                    'class' => 'form-control ',
                    'empty' => '----'
                ));
                ?>
            </div>
        </div>
        <hr>
        <div class="row" id="heading">
          
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        
        PROJAPOTI.ajaxSubmitDataCallback('<?php echo $this->request->webroot ?>' + 'officeManagement/loadOfficeUnits',
                {'office_id': <?= $employee_office['office_id'] ?>}, 'html',
                function (response) {
                    $("#office-unit-id").html(response);
                });
       $("#office-unit-id").bind('change',function(){
            $('#heading').html('<img src="<?= CDN_PATH ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp; লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');
           PROJAPOTI.ajaxLoadWithRequestData('<?php echo $this->Url->build(['controller' => 'Potrojari' , 'action' => 'setPotrojariHeader']) ?>/'+ $("#office-unit-id").val(), {}, '#heading');
       });
    });

</script>