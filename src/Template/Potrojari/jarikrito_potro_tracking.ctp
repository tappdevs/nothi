<style>
  #showlist{
    height: 100vh;
    overflow: auto;
  }
  .portlet-title{
    min-height: 0px !important;
    margin-bottom: 0px !important;
  }
</style>
<div class="portlet light bordered">
    <div class="portlet-title tracking-title">
        <div class="caption">
            অফিসের জারিকৃত পত্র ট্র্যাকিং
        </div>
    </div>
    <div class="portlet-body">
        <div class="tracking-filter">
            <div class="row">
                <div class="col-md-4 col-sm-6 form-group form-horizontal">
                    <?php
                    echo $this->Form->hidden('start_date', ['class' => 'startdate']);
                    echo $this->Form->hidden('end_date', ['class' => 'enddate']);
                    echo $this->Form->hidden('input_unit_id', ['id' => 'input_unit_id']);
                    echo $this->Form->hidden('input_start_date', ['id' => 'input_start_date']);
                    echo $this->Form->hidden('input_end_date', ['id' => 'input_end_date']);
                    echo $this->Form->hidden('data_end', ['id' => 'data_end','value'=>0]);
                    echo $this->Form->hidden('page_no', ['id' => 'page_no','value'=>1]);
                    echo $this->Form->input('unit_id',
                        array(
                        'label' => false,
                        'class' => 'form-control',
                        'empty' => 'শাখা নির্বাচন করুন',
                            'options'=>$units_list
                    ));
                    ?>
                </div>
                <div class="col-md-4 col-sm-6 form-group form-horizontal" style="display:none;">
                    <div class="input-group" style="width: 100%">
                        <?php
                        echo $this->Form->input('office_unit_organogram_id',
                            array(
                                'label' => false,
                                'class' => 'form-control',
                                'empty' => 'পদবি নির্বাচন করুন',
                                'options'=> []
                            ));
                        ?>
                    </div>
                </div>
                           <!--Tools end -->

            <!--  Date Range Begain -->
            <div class="col-md-4 col-sm-12 pull-right" >
                <div class="hidden-print page-toolbar pull-right portlet-title">
                    <div id="dashboard-report-range" class=" tooltips btn btn-sm btn-default"
                         data-container="body"
                         data-placement="bottom" data-original-title="তারিখ নির্বাচন করুন">
                        <i class="icon-calendar"></i>&nbsp; <span
                            class="thin uppercase visible-lg-inline-block">তারিখ নির্বাচন করুন</span>&nbsp;
                        <i class="glyphicon glyphicon-chevron-down"></i>
                    </div>
                </div>
            </div>
            <!--  Date Range End  -->

            </div>
        </div>
        <div class="" id="showlist">

        </div>
      <div id="loader" style="display: none"><img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp; লোড হচ্ছে। একটু অপেক্ষা করুন... </span></div>
    </div>
</div>
<script src="<?php echo CDN_PATH; ?>daptorik_preview/js/report_date_range.js"></script>
<script src="<?php echo CDN_PATH; ?>js/reports/jarikrito_potro_tracking.js" type="text/javascript"></script>
<script>
  var pageUrl = "<?php echo $this->Url->build(['controller' => 'Potrojari', 'action' => 'getJarikritoPotroTracking']) ?>";
  jQuery(document).ready(function () {
    $(this).find('body').addClass('page-sidebar-closed');
    $(this).find('.page-sidebar-menu').addClass('page-sidebar-menu-closed');

    DateRange.init();
    DateRange.initDashboardDaterange();

    $("#office-unit-organogram-id").bind('change', function () {
        SortingDate.init($('.startdate').val(), $('.enddate').val());
    });
    $("#unit-id").bind('change', function () {
        var unit_id = $(this).val();
        var prefix = '';
        if (unit_id == 'all_unit' || unit_id == '') {
            $("#office-unit-organogram-id").closest('.form-horizontal').hide();
        } else {
            $("#office-unit-organogram-id").closest('.form-horizontal').show();
            OfficeSetup.loadOfficeUnitOrganograms(unit_id, {}, prefix);
        }
        SortingDate.init($('.startdate').val(), $('.enddate').val());
    });

    var height = $(".navbar-fixed-top").outerHeight() + $(".tracking-title").outerHeight() + $(".tracking-filter").outerHeight() + 120;

    $("#showlist").css('height', 'calc(100vh - ' + height + 'px)');
    $('#showlist').scroll(function () {
      if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
        if ($("#loader").css('display') == 'none') {
          lazyloading();
        }
      }
    });
  });

  function loadJarikritoPotro(unit_id, start_date, end_date) {
    Metronic.blockUI({target: '.portlet-body', boxed: true});
    $('#showlist').html('');
    $('#inbox-content').html('');
    var organogram_id = $("#office-unit-organogram-id").val();
    $.ajax({
      type: 'POST',
      url: pageUrl,
      data: {'unit_id': unit_id, 'start_date': start_date, 'end_date': end_date, 'organogram_id': organogram_id},
      success: function (response) {
        if (response.status == 'success') {
          var data = '<table class="table table-bordered table-hover jariPotroTrackingTable">' +
            '           <thead>\n' +
            '                <th class="text-center">ক্রম</th>\n' +
            '                <th class="text-center">স্বারক নং</th>\n' +
            '                <th class="text-center">বিষয়</th>\n' +
            '                <th class="text-center">খসড়া তৈরিকারীর নাম</th>\n' +
            '                <th class="text-center">খসড়া তৈরির তারিখ</th>\n' +
            '            </tr>\n' +
            '            </thead><tbody>';
          if (isEmpty(response.data)) {
            data += '<tr>\n' +
              '                    <td class="text-center" colspan="5" style="color: red">দুঃখিত। কোন তথ্য পাওয়া যায়নি।</td>\n' +
              '                </tr>';
          } else {
            var i=1;
            $.each(response.data, function (key, value) {
              data += '<tr>\n' +
                '                    <td class="text-center">' + enTobn(i++) + '</td>\n' +
                '                    <td class="text-center">' + value.sarok_no + '</td>\n' +
                '                    <td class="text-center">' + value.potro_subject + '</td>\n' +
                '                    <td class="text-center">' + value.created_by + ', ' + (isEmpty(value.created_by_designation_lebel) ? '' : (value.created_by_designation_lebel + ', ')) + value.potrojari_draft_unit + '</td>\n' +
                '                    <td class="text-center">' + value.created + '</td>\n' +
                '                </tr>';
            });
          }
          data += '</tbody></table>';
          $('#showlist').html(data);
          $("#page_no").val(1);

        } else {
          toastr.error("দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না।");
        }
        $("#input_unit_id").val(unit_id);
        $("#input_start_date").val(start_date);
        $("#input_end_date").val(end_date);
        $("#data_end").val(0);
        Metronic.unblockUI('.portlet-body');
      },
      error: function () {
        Metronic.unblockUI('.portlet-body');
      }
    });
  }

  function lazyloading() {
    if($("#loader").css('display') != 'none'){
      return false;
    }
    if($("#data_end").val() == 1){
      return false;
    }

    $("#loader").show();
    var unit_id = $("#input_unit_id").val();
    var start_date = $("#input_start_date").val();
    var end_date = $("#input_end_date").val();
    var prev_page_no = $("#page_no").val();
    prev_page_no = parseInt(prev_page_no) ;
    var new_page_no = prev_page_no+1;
    $("#page_no").val(new_page_no);

    $.ajax({
      type: 'POST',
      url: pageUrl+'/'+new_page_no,
      data: {'unit_id': unit_id, 'start_date': start_date, 'end_date': end_date},
      success: function (response) {
        if (response.status == 'success') {
          var data = '';
          if (isEmpty(response.data)) {
            $("#loader").hide();
            $("#data_end").val(1);
          } else {
            var i=1;
            $.each(response.data, function (key, value) {
              data += '<tr>\n' +
                '                    <td class="text-center">' + enTobn((prev_page_no*20)+i++) + '</td>\n' +
                '                    <td class="text-center">' + value.sarok_no + '</td>\n' +
                '                    <td class="text-center">' + value.potro_subject + '</td>\n' +
                '                    <td class="text-center">' + value.created_by + ', ' + (isEmpty(value.created_by_designation_lebel) ? '' : (value.created_by_designation_lebel + ', ')) + value.potrojari_draft_unit + '</td>\n' +
                '                    <td class="text-center">' + value.created + '</td>\n' +
                '                </tr>';
            });
            $('.jariPotroTrackingTable').append(data);
            $("#loader").hide();
          }
        } else {
          $("#loader").hide();
          toastr.error("দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না।");
        }
        Metronic.unblockUI('.portlet-body');
      },
      error: function () {
        $("#loader").hide();
        toastr.error("দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না।");
      }
  });
  }
</script>

