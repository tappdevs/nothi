<style>
    #sovapoti_signature, #sender_signature, #sender_signature2, #sender_signature3 {
        visibility: hidden;
    }

    #sovapoti_signature_date, #sender_signature_date, #sender_signature2_date, #sender_signature3_date {
        visibility: hidden;
    }

    .editable-click, a.editable-click {
        border: none;
        word-break: break-word;
        word-wrap: break-word
    }

    .cc_list {
        white-space: pre-wrap;
    }

    .popover-content {
        padding: 10px 30px;
    }

    .mega-menu-dropdown > .dropdown-menu {
        top: 10px !important;
    }

    #note {
        overflow: hidden;
        word-break: break-word;
        word-wrap: break-word;
        height: 100%;
    }

    .ms-container {
        width: 100% !important;
    }

    .bangladate {
        border-bottom: 1px solid #000;
    }

</style>

<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2.css"
      type="text/javascript">
<link rel="stylesheet" type="text/css"
      href="<?php echo CDN_PATH; ?>assets/global/plugins/jquery-multi-select/css/multi-select.css"/>
<link rel="stylesheet" type="text/css"
      href="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css"
      href="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>

<div class="form">

    <?php
    echo $this->Form->create(null,
        array('action' => '#', 'type' => 'file', 'id' => 'potrojariDraftForm'));
    ?>
    <?php
    echo $this->Form->hidden('dak_type',
        array('label' => false, 'class' => 'form-control', 'value' => DAK_DAPTORIK));
    echo $this->Form->hidden('dak_status',
        array('label' => false, 'class' => 'form-control', 'value' => 1));
    echo $this->Form->hidden('dak_subject',
        array('label' => false, 'class' => 'form-control'));
    echo $this->Form->hidden('sender_sarok_no',
        array('label' => false, 'class' => 'form-control'));
    echo $this->Form->hidden('meta_data',
        array('label' => false, 'class' => 'form-control'));
    echo $this->Form->hidden('attached_potro_id',
        array('label' => false, 'class' => 'form-control','value'=>$attached_id));

    //approvalinformation
    echo $this->Form->hidden('approval_office_id_final',
        array('label' => false, 'class' => 'form-control'));
    echo $this->Form->hidden('approval_officer_id_final',
        array('label' => false, 'class' => 'form-control'));
    echo $this->Form->hidden('approval_office_unit_id_final',
        array('label' => false, 'class' => 'form-control'));
    echo $this->Form->hidden('approval_officer_designation_id_final',
        array('label' => false, 'class' => 'form-control'));
    echo $this->Form->hidden('approval_officer_designation_label_final',
        array('label' => false, 'class' => 'form-control'));
    echo $this->Form->hidden('approval_officer_name_final',
        array('label' => false, 'class' => 'form-control'));
    echo $this->Form->hidden('approval_office_name_final',
        array('label' => false, 'class' => 'form-control'));
    echo $this->Form->hidden('approval_office_unit_name_final',
        array('label' => false, 'class' => 'form-control'));
    echo $this->Form->hidden('approval_visibleName',
        array('label' => false, 'class' => 'form-control'));
    echo $this->Form->hidden('approval_visibleDesignation',
        array('label' => false, 'class' => 'form-control'));


    //sovapotiinformation
    echo $this->Form->hidden('sovapoti_office_id_final',
        array('label' => false, 'class' => 'form-control'));
    echo $this->Form->hidden('sovapoti_officer_designation_id_final',
        array('label' => false, 'class' => 'form-control'));
    echo $this->Form->hidden('sovapoti_officer_designation_label_final',
        array('label' => false, 'class' => 'form-control'));
    echo $this->Form->hidden('sovapoti_officer_id_final',
        array('label' => false, 'class' => 'form-control'));
    echo $this->Form->hidden('sovapoti_officer_name_final',
        array('label' => false, 'class' => 'form-control'));
    echo $this->Form->hidden('sovapoti_visibleName',
        array('label' => false, 'class' => 'form-control'));
    echo $this->Form->hidden('sovapoti_visibleDesignation',
        array('label' => false, 'class' => 'form-control'));

    //senderinformation
    echo $this->Form->hidden('sender_office_id_final',
        array('label' => false, 'class' => 'form-control'));
    echo $this->Form->hidden('sender_officer_designation_id_final',
        array('label' => false, 'class' => 'form-control'));
    echo $this->Form->hidden('sender_officer_designation_label_final',
        array('label' => false, 'class' => 'form-control'));
    echo $this->Form->hidden('sender_officer_id_final',
        array('label' => false, 'class' => 'form-control'));
    echo $this->Form->hidden('sender_officer_name_final',
        array('label' => false, 'class' => 'form-control'));
    echo $this->Form->hidden('sender_officer_visibleName',
        array('label' => false, 'class' => 'form-control'));
    echo $this->Form->hidden('sender_officer_visibleDesignation',
        array('label' => false, 'class' => 'form-control'));
    //receiverinformation
    echo $this->Form->hidden('receiver_group_id_final',
        array('label' => false, 'class' => 'form-control'));
    echo $this->Form->hidden('receiver_group_designation_final',
        array('label' => false, 'class' => 'form-control'));
    echo $this->Form->hidden('receiver_group_name_final',
        array('label' => false, 'class' => 'form-control'));
    echo $this->Form->hidden('receiver_group_member_final',
        array('label' => false, 'class' => 'form-control'));
    echo $this->Form->hidden('receiver_group_sms_final',
        array('label' => false, 'class' => 'form-control'));

    echo $this->Form->hidden('receiver_office_id_final',
        array('label' => false, 'class' => 'form-control'));
    echo $this->Form->hidden('receiver_office_name_final',
        array('label' => false, 'class' => 'form-control'));
    echo $this->Form->hidden('receiver_officer_designation_id_final',
        array('label' => false, 'class' => 'form-control'));
    echo $this->Form->hidden('receiver_office_unit_id_final',
        array('label' => false, 'class' => 'form-control'));
    echo $this->Form->hidden('receiver_office_unit_name_final',
        array('label' => false, 'class' => 'form-control'));
    echo $this->Form->hidden('receiver_officer_designation_label_final',
        array('label' => false, 'class' => 'form-control'));
    echo $this->Form->hidden('receiver_officer_id_final',
        array('label' => false, 'class' => 'form-control'));
    echo $this->Form->hidden('receiver_officer_name_final',
        array('label' => false, 'class' => 'form-control'));
    echo $this->Form->hidden('receiver_officer_email_final',
        array('label' => false, 'class' => 'form-control'));
    echo $this->Form->hidden('receiver_office_head_final',
        array('label' => false, 'class' => 'form-control'));
    echo $this->Form->hidden('receiver_visibleName_final',
        array('label' => false, 'class' => 'form-control'));
    echo $this->Form->hidden('receiver_officer_mobile_final',
        array('label' => false, 'class' => 'form-control'));
    echo $this->Form->hidden('receiver_sms_message_final',
        array('label' => false, 'class' => 'form-control'));

    //onulipiinformation
    echo $this->Form->hidden('onulipi_group_id_final',
        array('label' => false, 'class' => 'form-control'));
    echo $this->Form->hidden('onulipi_group_designation_final',
        array('label' => false, 'class' => 'form-control'));
    echo $this->Form->hidden('onulipi_group_name_final',
        array('label' => false, 'class' => 'form-control'));
    echo $this->Form->hidden('onulipi_group_member_final',
        array('label' => false, 'class' => 'form-control'));
    echo $this->Form->hidden('onulipi_office_id_final',
        array('label' => false, 'class' => 'form-control'));
    echo $this->Form->hidden('onulipi_office_name_final',
        array('label' => false, 'class' => 'form-control'));
    echo $this->Form->hidden('onulipi_officer_designation_id_final',
        array('label' => false, 'class' => 'form-control'));
    echo $this->Form->hidden('onulipi_office_unit_id_final',
        array('label' => false, 'class' => 'form-control'));
    echo $this->Form->hidden('onulipi_office_unit_name_final',
        array('label' => false, 'class' => 'form-control'));
    echo $this->Form->hidden('onulipi_officer_designation_label_final',
        array('label' => false, 'class' => 'form-control'));
    echo $this->Form->hidden('onulipi_officer_id_final',
        array('label' => false, 'class' => 'form-control'));
    echo $this->Form->hidden('onulipi_officer_name_final',
        array('label' => false, 'class' => 'form-control'));
    echo $this->Form->hidden('onulipi_officer_email_final',
        array('label' => false, 'class' => 'form-control'));
    echo $this->Form->hidden('onulipi_office_head_final',
        array('label' => false, 'class' => 'form-control'));
    echo $this->Form->hidden('onulipi_visibleName_final',
        array('label' => false, 'class' => 'form-control'));
    echo $this->Form->hidden('onulipi_officer_mobile_final',
        array('label' => false, 'class' => 'form-control'));
    echo $this->Form->hidden('onulipi_sms_message_final',
        array('label' => false, 'class' => 'form-control'));
    echo $this->Form->hidden('onulipi_group_sms_final',
        array('label' => false, 'class' => 'form-control'));
    //potrojari_language
    echo $this->Form->hidden('potrojari_language',
        array('label' => false, 'class' => 'form-control', 'id' => 'potro_language', 'value' => (!empty($potrojari_language)?$potrojari_language:'bn')));

        //attension
    echo $this->Form->hidden('attension_office_id_final',
        array('label' => false, 'class' => 'form-control'));
    echo $this->Form->hidden('attension_officer_id_final',
        array('label' => false, 'class' => 'form-control'));
    echo $this->Form->hidden('attension_office_unit_id_final',
        array('label' => false, 'class' => 'form-control'));
    echo $this->Form->hidden('attension_officer_designation_id_final',
        array('label' => false, 'class' => 'form-control'));
    echo $this->Form->hidden('attension_officer_designation_label_final',
        array('label' => false, 'class' => 'form-control'));
    echo $this->Form->hidden('attension_officer_name_final',
        array('label' => false, 'class' => 'form-control'));
    echo $this->Form->hidden('attension_office_name_final',
        array('label' => false, 'class' => 'form-control'));
    echo $this->Form->hidden('attension_office_unit_name_final',
        array('label' => false, 'class' => 'form-control'));
    ?>
    <?php
        echo $this->Cell('Potrojari', ['template' => $template_list,[],[],'is_endorse' =>
            isset($is_endorse)?$is_endorse:0,0, 'endorse_sender_info' =>
            isset($is_endorse)?(object)$employee_info:[]])
    ?>
    <!--dictionary input-->
    <div>
        <input type="hidden" id="word_potro" value="">
        <input type="hidden" id="space_potro" value="">
        <input type="hidden" id="office_id" value="<?= $office_id ?>">
        <input type="hidden" id="part_no" value="<?= $nothiMasterId ?>">
    </div>

    <div class="portlet-body">
        <h3> <?php echo __(SHONGJUKTI) ?> </h3>
        <div class="tabbable-line">
            <ul class="nav nav-tabs ">
                <li class="active">
                    <a href="#tab_prapto_potro" data-toggle="tab" aria-expanded="true">
                        প্রাপ্ত পত্রসমূহ </a>
                </li>
                <li class="">
                    <a href="#tab_other_potro" data-toggle="tab" aria-expanded="false">
                        অন্যান্য </a>
                </li>

            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_prapto_potro">
                    <?php
                    $selectmulti = [];

                    if (!empty($potroAttachmentRecord)) {
                        foreach ($potroAttachmentRecord as $k => $v) {
                            $potr = explode(';', $v);
                            $oth = array_slice($potr, 1);

                            $selectmulti[$k] = "পত্র: " . $potr[0] . " - " . implode(' ', $oth);
                        }
                    }
                    //echo $this->Form->input('prapto_potro', ['class' => 'multi-select', 'options' => $selectmulti, 'multiple' => 'multiple', 'label' => false])
                    ?>
	                <div class="row">
		                <div class="col-sm-5">
			                <select name="prapto_potro_all" id="multiselect_left" class="form-control no-select2 multiselect" size="5" multiple="multiple" style="padding: 6px 0;">
				                <?php foreach($selectmulti as $key => $value): ?>
				                <option title="<?=$value?>" value="<?=$key?>"><?=$value?></option>
				                <?php endforeach; ?>
			                </select>
		                </div>
		                <div class="col-sm-2">
			                <!--<button type="button" id="btn_rightAll" class="btn btn-block"><i class="glyphicon glyphicon-forward"></i></button>-->
			                <button type="button" id="btn_rightSelected" class="btn btn-block  col-md-2 col-sm-2"><i class="glyphicon glyphicon-chevron-right"></i></button>
			                <button type="button" id="btn_leftSelected" class="btn btn-block  col-md-2 col-sm-2"><i class="glyphicon glyphicon-chevron-left"></i></button>
			                <!--<button type="button" id="btn_leftAll" class="btn btn-block"><i class="glyphicon glyphicon-backward"></i></button>-->
		                </div>
		                <div class="col-sm-5">
			                <select name="prapto_potro[]" id="multiselect_right" class="form-control no-select2 multiselect" size="5" multiple="multiple" style="padding: 6px 0;"></select>
		                </div>
	                </div>
                </div>
                <?php echo $this->Form->end(); ?>
                <div class="tab-pane" id="tab_other_potro">
                    <form id="fileuploadpotrojari" action="<?= $this->Url->build(['_name'=>'tempUpload']) ?>" method="POST" enctype="multipart/form-data">

                        <input type="hidden" name="module_type" value="Nothi"/>
                        <input type="hidden" name="module" value="Potrojari" />

                        <div class="row fileupload-buttonbar">
                            <div class="col-lg-12">
                                <!-- The fileinput-button span is used to style the file input field as button -->
                                <span class="btn green btn-sm fileinput-button">
                                    <i class="fs1 a2i_gn_add1"></i>
                                    <span>
                                        ফাইল যুক্ত করুন </span>
                                    <input type="file" name="files[]" multiple="">
                                </span>

                                <button type="button" class="btn btn-sm red delete">
                                    <i class="fs1 a2i_gn_delete2"></i>
                                    <span>
                                        সব মুছে ফেলুন </span>
                                </button>
                                <!--<input type="checkbox" class="toggle">-->
                                <!-- The global file processing state -->
                                <span class="fileupload-process">
                                </span>
                            </div>
                            <!-- The global progress information -->
                            <div class="col-lg-5 fileupload-progress fade">
                                <!-- The global progress bar -->
                                <div class="progress progress-striped active"
                                     role="progressbar"
                                     aria-valuemin="0" aria-valuemax="100">
                                    <div class="progress-bar progress-bar-success"
                                         style="width:0%;">
                                    </div>
                                </div>
                                <!-- The extended global progress information -->
                                <div class="progress-extended">
                                    &nbsp;
                                </div>
                            </div>

                        </div>
                        <!-- The table listing the files available for upload/download -->
                        <table role="presentation" class="table table-striped clearfix">
                            <tbody class="files">

                            </tbody>
                        </table>
                    </form>
                </div>

            </div>
        </div>
    </div>

    <div class="form-actions">
        <div class="row">
            <div class="col-md-12">
                <?=
                $this->Form->button(__('সংরক্ষণ করুন'),
                    ['class' => 'btn btn-primary  btn-sm', 'onclick' => 'DRAFT_FORM.saveDraft()'])
                ?>

            </div>
        </div>
    </div>
</div>
<div class="modal fade modal-purple  UIBlockPotro" id="responsiveModal" tabindex="-1" role="dialog"
     aria-hidden="true">
    <div class="modal-dialog modal-full">

        <div class="modal-content">
            <div class="modal-header">
                <div class="">
                    <button type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <div type="button" class="btn btn-success pull-right btn-sm" id="portalGuardFileImportPotro" aria-hidden="false">পোর্টালের গার্ড ফাইল</div>
                    <div class="modal-title">গার্ড ফাইল</div>
                </div>
            </div>
            <div class="modal-body ">
                <div class="row form-group">
                    <label class="col-md-2 control-label">ধরন</label>
                    <div class="col-md-6">
                        <?= $this->Form->input('guard_file_category_id', ['label' => false, 'type' => 'select', 'class' => 'form-control','id'=> 'guard_file_category_potro', 'options' => ["0" => "সকল"] + $guarfilesubjects]) ?>
                    </div>
                </div>
                <br/>

                <table class='table table-striped table-bordered table-hover' id="filelist">
                    <thead>
                    <tr>
                        <th class="text-center" style="width: 10%">ক্রম</th>
                        <th class="text-center" style="width: 30%">ধরন</th>
                        <th class="text-center" style="width: 40%">নাম</th>
                        <th class="text-center" style="width: 20%">কার্যক্রম</th>
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><?=__('বন্ধ করুন') ?></button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal fade modal-purple" id="previewModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-full">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="bootbox-close-button" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <?= $this->Form->create(null, ['id' => 'marginform']) ?>
                    <?= $this->Form->hidden('reset', ['value' => true]) ?>
                    <div class="col-md-1 col-sm-2 col-xs-3 col-lg-1 form-group">
                        <?= $this->Form->input('margin_top', ['class' => 'form-control input-sm ', 'label' => 'উপর', 'default' => '1.0', 'type' => 'number', 'precision' => 2, 'step' => .25, 'min' => 0]) ?>
                    </div>
                    <div class="col-md-1 col-sm-2 col-xs-3 col-lg-1 form-group">
                        <?= $this->Form->input('margin_bottom', ['class' => 'form-control input-sm', 'label' => 'নিচ', 'default' => '.75', 'type' => 'number', 'precision' => 2, 'step' => .25, 'min' => 0]) ?>
                    </div>
                    <div class="col-md-1 col-sm-2 col-xs-3 col-lg-1 form-group">
                        <?= $this->Form->input('margin_left', ['class' => 'form-control input-sm', 'label' => 'বাম', 'default' => '0.75', 'type' => 'number', 'precision' => 2, 'step' => .25, 'min' => 0]) ?>
                    </div>
                    <div class="col-md-1 col-sm-2 col-xs-3 col-lg-1 form-group">
                        <?= $this->Form->input('margin_right', ['class' => 'form-control input-sm', 'label' => 'ডান', 'default' => '0.75', 'type' => 'number', 'precision' => 2, 'step' => .25, 'min' => 0]) ?>
                    </div>
                    <div class="col-md-2 col-sm-3 col-xs-4 col-lg-2 form-group">
                        <?= $this->Form->input('orientation', ['class' => 'form-control input-sm', 'label' => 'ধরন', 'type' => 'select', 'options' => ['portrait' => 'Portrait', 'landscape' => 'Landscape']]) ?>
                    </div>
                    <div class="col-md-2 col-sm-3 col-xs-4 col-lg-2 form-group">
                        <?= $this->Form->input('potro_header', ['class' => 'form-control input-sm', 'label' => __("Banner"), 'type' => 'select', 'options' => [0 => 'হ্যাঁ', 1 => 'না']]) ?>
                    </div>
                    <div class="col-md-4 col-sm-3 col-xs-4 col-lg-4 form-group">
	                    <br/>
	                    <div class="btn-group btn-group-round">
	                        <button class="btn btn-success btn-sm btn-save-pdf-margin" type="button"><i class="fa fa-check"></i>
	                            সংরক্ষণ
	                        </button>
	                        <button class="btn blue btn-sm btn-pdf-margin" type="button"><i class="fa fa-binoculars"></i>
	                            প্রিভিউ
	                        </button>
	                        <a class="btn btn-info btn-sm btn-pdf-download hide" target="_blank" type="button"><i class="fa fa-download"></i>
	                            ডাউনলোড
	                        </a>
	                    </div>
                    </div>
                    <?= $this->Form->end() ?>
                </div>
                <div class="loading text-center font-lg" style="display:none;"><i class="fa fa-spinner fa-2x fa-spin fa-pulse"></i></div>
                <embed class="showPreview" src="" style="width:100%; height: calc(100vh - 250px);" type="application/pdf"></embed>
            </div>
            <div class="modal-footer">
                <button aria-hidden="true" class="btn red pull-right round-corner-5" type="button"
                        onclick="$('#previewModal').modal('hide')" data-dismiss="modal"></i>
                    বন্ধ করুন
                </button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade modal-purple " id="portalResponsiveModalPotro" role="dialog"
     aria-hidden="true">
    <div class="modal-dialog modal-full">

        <div class="modal-content">
            <div class="modal-header">
                <div class="">
                    <button type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <div class="modal-title">পোর্টালের গার্ড ফাইল</div>
                </div>
            </div>
            <div class="modal-body ">
                <div class="row form-group">
                    <label class="col-md-2 control-label text-right">অফিসের ধরন</label>
                    <div class="col-md-4">
                        <?= $this->Form->input('layer_ids_potro', ['label' => false, 'type' => 'select', 'class' => 'form-control', 'options' => ["0" => "--","2"=>"মন্ত্রণালয়/বিভাগ","3"=>"অধিদপ্তর/সংস্থা"]]) ?>
                    </div>
                    <label class="col-md-1 control-label" style="padding-right: 0px;">অফিস</label>
                    <div class="col-md-5" style="padding-left: 0px">
                        <?= $this->Form->input('portal_guard_file_types', ['label' => false, 'type' => 'select', 'class' => 'form-control', 'options' => ["0" => "--"]]) ?>
                    </div>
                </div>
                <div class="col-md-6"></div>
                <div class="col-md-6 pull right" style="padding-bottom: 5px">
                    <input type="text" class="form-control" id="search" placeholder=" খুজুন">
                </div>
                <table class='table table-striped table-bordered table-hover' id="portalfilelistPotro">
                    <thead>
                    <tr>
                        <th class="text-center" style="width: 10%">ক্রম</th>
                        <th class="text-center" style="width: 30%">ধরন</th>
                        <th class="text-center" style="width: 40%">নাম</th>
                        <th class="text-center" style="width: 20%">কার্যক্রম</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><?=__('বন্ধ করুন') ?></button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<input type="hidden" value="<?php echo isset($potroInfo->id)?$potroInfo->id:'' ;?>" id="txtPotroIDTmp">
<input type="hidden" value="make_draft" id="txtPotrojariDraftTmp">
<div class="hide referencebody">

    <?= !empty($potroInfo)?$potroInfo->content_body:'' ?>
</div>

<script src="<?php echo CDN_PATH; ?>projapoti-nothi/js/potrojari_editable.js?v=<?= js_css_version ?>" type="text/javascript"></script>
<script src="<?php echo CDN_PATH; ?>projapoti-nothi/js/potro_offline.js" type="text/javascript"></script>

<script type="text/javascript"
        src="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript"
        src="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript"
        src="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script src="<?php echo CDN_PATH; ?>assets/global/scripts/datatable.js"></script>
<script src="<?php echo CDN_PATH; ?>projapoti-nothi/js/tbl_guard_files.js"></script>
<script src="<?php echo CDN_PATH; ?>daptorik_preview/js/Sortable.js"></script>
<script src="<?php echo CDN_PATH; ?>projapoti-nothi/js/potrojari_related.js?v=<?= js_css_version ?>" type="text/javascript"></script>
<script src="<?php echo CDN_PATH; ?>js/multiple-selection.js?v=<?= js_css_version ?>" type="text/javascript"></script>
<script>

    jQuery.ajaxSetup({
        cache: true
    });
    var offline_potro_draft_key = '<?= $offline_potro_draft_key ?>';
    $('.btn-forward-nothi').hide();
    $('a').tooltip({'placement':'bottom'});
    $('[data-toggle="tooltip"]').tooltip({'placement':'bottom'});
    Metronic.initSlimScroll('.receiver_group_list');
    Metronic.initSlimScroll('.onulipi_group_list');
    var numbers = {
        1: '১',
        2: '২',
        3: '৩',
        4: '৪',
        5: '৫',
        6: '৬',
        7: '৭',
        8: '৮',
        9: '৯',
        0: '০'
    };

    function replaceNumbers(input) {
        var output = [];
        for (var i = 0; i < input.length; ++i) {
            if (numbers.hasOwnProperty(input[i])) {
                output.push(numbers[input[i]]);
            } else {
                output.push(input[i]);
            }
        }
        return output.join('');
    }

    var DRAFT_FORM = {
        attached_files: [],
        sender_users: [],
        approval_users: [],
        receiver_users: [],
        onulipi_users: [],
        setform: function () {
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "positionClass": "toast-bottom-right"
            };

            if ($('#khosrapotrobody').find('#potrojariDraftForm').find('.fr-toolbar').length > 0) {
                $("#pencil").click();
            };

            var potrojari_language = ($('#potrojari_language').bootstrapSwitch('state') == true) ? 'bn' : 'eng';

            var senders = $('.sender_users');
            var approvals = $('.approval_users');
            var receivers = $('.receiver_users');
            var onulipis = $('.onulipi_users');
            var sovapotis = $('.sovapoti_users');

            $('#khosrapotrobody').find('#potrojariDraftForm').find('#sovapoti_signature').html("");
            $('#khosrapotrobody').find('#potrojariDraftForm').find('#sovapoti_designation').html("");
            $('#khosrapotrobody').find('#potrojariDraftForm').find('#sovapotiname').html("");
            $('#khosrapotrobody').find('#potrojariDraftForm').find('#sovapoti_designation2').html("");
            $('#khosrapotrobody').find('#potrojariDraftForm').find('#sovapotiname2').html("");
            $('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_signature').html("");
            $('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_signature2').html("");
            $('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_designation').html("");
            $('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_name').html("");
            $('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_designation2').html("");
            $('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_designation3').html("");
            $('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_name2').html("");
            $('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_name3').html("");
            //$('#khosrapotrobody').find('#potrojariDraftForm').find('#office_organogram_id').html("");
            $('#khosrapotrobody').find('#potrojariDraftForm').find('#address').html("");

            if (isEmpty($('#potro-type').val())) {
                toastr.error("দুঃখিত! পত্রের ধরন বাছাই করা হয়নি");
                return false;
            }
            //LM
            if($('[name=potro_type]').val() == 20){
                check_attachment();
                if($("#reference").length == 0 || $.trim($("#reference").text())=='...'){
                    $(".remove_div_if_empty").hide();
                }
                if($("#attachment_list tbody tr").length == 0){
                    $(".sologni_div").hide();
                }
                if($(".cc_div_item").length == 0){
                    $(".getarthe").hide();
                }
            }
            try{
                if(DRAFT_FORM.setSovapotiForm(sovapotis) == false){
                    return false;
                }
                if(DRAFT_FORM.setSenderForm(senders) == false){
                    return false;
                }
                if(DRAFT_FORM.setApprovalForm(approvals) == false){
                    return false;
                }
                if(DRAFT_FORM.setReceiverForm(receivers) == false){
                    return false;
                }
                if(DRAFT_FORM.setOnulipiForm(onulipis) == false){
                    return false;
                }
            }
            catch(e){
                return false;
            }

            if ($('#khosrapotrobody').find('#potrojariDraftForm').find('#office_organogram_id').length == 0) {
                if ($('#khosrapotrobody').find('#potrojariDraftForm').find('#cc_list_div').length != 0) {
                    if (onulipis.length == 0) {
                        toastr.error("দুঃখিত! অনুলিপি/বিতরন দেয়া হয়নি");
                        return false;
                    }
                }
            } else {
                if (receivers.length == 0 && $('#potro-type').val() != 19) {
                    toastr.error("দুঃখিত! প্রাপক দেয়া হয়নি");
                    return false;
                }
            }

            DRAFT_FORM.attached_files = [];
            DRAFT_FORM.attached_files_names = [];
            $('#fileuploadpotrojari').find('.template-download').each(function () {
                var link_td = $(this).find('[class=name]');
                var href = $(link_td).find('a').attr('href');
                DRAFT_FORM.attached_files.push(href);
            });
            $('.template-download .potro-attachment-input').each(function () {
                var name = $(this).val();
                if(isEmpty(name)){
                    name = $(this).attr('title');
                }
              DRAFT_FORM.attached_files_names.push(name);
            });
            $("#uploaded_attachments_names").val(DRAFT_FORM.attached_files_names);
            $("#uploaded_attachments").val(DRAFT_FORM.attached_files);

            $("#file_description").val($("#file_description_upload").val());

            var subject = $('#khosrapotrobody').find('#potrojariDraftForm').find('#subject').text();

            if ($('#potro-type').val() == 13) {
                subject = $('#khosrapotrobody').find('#potrojariDraftForm').find('#blank-subject').val();
            }

            var sender_sarok_no = $('#khosrapotrobody').find('#potrojariDraftForm').find('#sharok_no').text();

            if ($('#potro-type').val() == 13) {
                sender_sarok_no = $('#khosrapotrobody').find('#potrojariDraftForm').find('#blank-sarok').val();
            }

            $('input[name=sender_sarok_no]').val(sender_sarok_no);
            $('input[name=dak_subject]').val(subject);

            if($('#potro-type').val()==30){
				var temp = $('#khosrapotrobody').find('#potrojariDraftForm').find('#template-body2').html();
				var temp2 = $('#khosrapotrobody').find('#potrojariDraftForm').find('#template-body3').html();
            }else{
				var temp = $('#khosrapotrobody').find('#potrojariDraftForm').find('#template-body').html();
            }


            if (temp.length == 0 || $('#potro-type').val() == '') {
                toastr.error("দুঃখিত! পত্র দেয়া হয়নি");
                return false;
            }

            if ($('#khosrapotrobody').find('#potrojariDraftForm').find('input[name=dak_subject]').val().length == 0) {
                $('#khosrapotrobody').find('#potrojariDraftForm').find('input[name=dak_subject]').val($('#potro-type option:selected').text());
            }

            if ($('#khosrapotrobody').find('#potrojariDraftForm').find('input[name=sender_sarok_no]').val().length == 0) {
                toastr.error("দুঃখিত! স্মারক নম্বর দেয়া হয়নি");
                return false;
            }

            if ($('#khosrapotrobody').find('#potrojariDraftForm').find('#office_organogram_id').length > 0 && $('.receiver_users').length > 0) {
                if ($('#khosrapotrobody').find('#potrojariDraftForm').find('#cc_list_div').length > 0) {
                    if ($('.onulipi_users').length == 0) {
                        $('#khosrapotrobody').find('#potrojariDraftForm').find('#cc_list_div').closest('.row').nextAll('.row').hide()
                        $('#khosrapotrobody').find('#potrojariDraftForm').find('#cc_list_div').closest('.row').hide();
                        $('#khosrapotrobody').find('#potrojariDraftForm').find('#sharok_no2').closest('.row').hide();
                        $('#khosrapotrobody').find('#potrojariDraftForm').find('#sending_date_2').closest('.row').hide();
                    }
                }
            }

            if(potrojari_language == 'bn'){
               var heading_settings = <?= isset($heading['potrojari_head'])?$heading['potrojari_head']:[]; ?>;
            } else {
                var heading_settings = <?= isset($heading['potrojari_head_eng'])?$heading['potrojari_head_eng']:[]; ?>;
            }

            if($('#potro-type').val()!=4) {
                var logo = "<?= isset(json_decode($heading['potrojari_head_img'], true)['head_logo']) ? json_decode($heading['potrojari_head_img'], true)['head_logo'] : '' ?>";
                var logoremove = isEmpty(heading_settings.remove_header_logo) ? 0 : heading_settings.remove_header_logo;

                if (logoremove != 1) {
                    var head_logo_type = isEmpty(heading_settings.head_logo_type) ? '' : heading_settings.head_logo_type;
                    var head_text = isEmpty(heading_settings.head_text) ? '' : heading_settings.head_text;

                    if ((!isEmpty(head_logo_type)) && (head_logo_type == "image") && logo.length != 0) {

                        if(potrojari_language == 'bn') {
                            $('#khosrapotrobody').find('#potrojariDraftForm').find('.col-md-6.text-center').eq(0).prepend('<img class="head_logo" id="head_logo" src="' + logo + '" style="max-height: 80px; max-width: 180px;margin:5px;" /><br/>');
                        } else {
                            $('#khosrapotrobody').find('#potrojariDraftForm .templateWrapper').prepend('<div class="row"><div class="col-md-3"></div><div class="col-md-6 text-center"><img class="head_logo" id="head_logo" src="' + logo + '" style="max-height: 80px; max-width: 180px;margin:5px;" /></div><div class="col-md-3"></div></div>');
                        }
                    } else if ((!isEmpty(head_logo_type)) && (head_logo_type == "text") && head_logo_type.length != 0) {

                        $('#khosrapotrobody').find('#potrojariDraftForm').find('.col-md-6.text-center').eq(0).prepend('<a href="javascript:" class="head_logo editable editable-click" id="head_logo" data-type="textarea" data-pk="1">' + head_text + '</a><br/>');
                    } else {
                        $('#khosrapotrobody').find('#potrojariDraftForm').find('#head_logo').remove();
                        $('#khosrapotrobody').find('#potrojariDraftForm').find('.head_logo').remove();
                    }

                } else {
                    $('#khosrapotrobody').find('#potrojariDraftForm').find('#head_logo').remove();
                    $('#khosrapotrobody').find('#potrojariDraftForm').find('.head_logo').remove();
                }
            }

			if($('#potro-type').val()==30) {
				$('#khosrapotrobody').find('#potrojariDraftForm').find('#template-body2').find('#pencil').remove();
				$('#khosrapotrobody').find('#potrojariDraftForm').find('#template-body3').find('#pencil').remove();
			}
            $.each($('#khosrapotrobody').find('#potrojariDraftForm').find('a'), function (i, v) {

                if($('[name=potro_type]').val() == 5){
                    if($(this).attr('id') == 'subject'){
                        if ($(this).text().length == 0 || $(this).text() == '...') {
                            $(this).closest('.row').remove();
                        }
                    }
                }
                if ($(this).hasClass('closethis') ||
                    $(this).hasClass('savethis') ||
                    $(this).attr('id') == 'sovadate' ||
                    $(this).attr('id') == 'sovatime' ||
                    $(this).attr('id') == 'sovaplace' ||
                    $(this).attr('id') == 'sovapresent' ||
                    $(this).attr('id') == 'subject') {
                }
                else if($('[name=potro_type]').val() == 20){
                    if ($(this).attr('id') == 'gov_name_noc' || $(this).attr('id') == 'form_name_noc' || $(this).attr('id') == 'ministry_name_noc' || $(this).attr('id') == 'sharok_no' || $(this).attr('id') == 'reference' || $(this).attr('id') == 'sender_unit' || $(this).attr('id') == 'extension' || $(this).attr('id') == 'LM_date_format') {
                        if($.trim($(this).text()) == '...') {
                            $(this).text('');
                        }
                    }
                    if ($(this).attr('class') == 'lm_songlogni_set ' || $.trim($(this).text()) == '...'){
                        $(this).text('');
                    }
                }
                else {
                    if ($("a[id^=sotro-extra-]").length > 0) {
                        $.each($("a[id^=sotro-extra-]"), function (i, v) {
                            var id = $(this).attr('id');
                            var dataType = $(this).attr('data-type');
                            if (dataType == 'textarea') {
                                var txt = $(this).html();
                            } else {
                                var txt = $(this).text();
                            }
                            $(this).replaceWith("<span style='white-space:pre-wrap;' id='" + ((typeof (id) != 'undefined') ? id : '') + "' data-type='" + dataType + "'>" + txt + "</span><br/>");
                        });
                    }
                    if ($(this).text().length == 0 || $.trim($(this).text()) == '...') {
                        if ($(this).attr('id') == 'left_slogan' || $(this).attr('id') == 'right_slogan') {
                            $(this).text('');
                        }
                    }
                }
            });

            if ($('#potro-type').val() == 30) {
				var contentbody2 = $('#khosrapotrobody').find('#potrojariDraftForm').find('#template-body3').html();
				$('#khosrapotrobody').find('#potrojariDraftForm').find('#contentbody3').html(contentbody2);
				$('#khosrapotrobody').find('#potrojariDraftForm').find('#template-body3').html(temp2);
            }

            $('#khosrapotrobody').find('#potrojariDraftForm').find('#template-body').find('#pencil').remove();
			$.each($('#khosrapotrobody').find('#potrojariDraftForm').find('#template-body').find('a.editable'), function (i, v) {
				var id = $(this).attr('id');
				var dataType = $(this).attr('data-type');
				if (dataType == 'textarea') {
					var txt = $(this).html();
				} else {
					var txt = $(this).text();
				}
				if(id=="to_div_item") {
					$(this).replaceWith("<span class='canedit' style='white-space:pre-wrap;' id='" + ((typeof (id) != 'undefined') ? id : '') + "' data-type='" + dataType + "'>" + txt + "</span>");
				}else {

					if($(this).hasClass('potro_security')){
						$(this).replaceWith("<span class='canedit potro_security' id='" + ((typeof (id) != 'undefined') ? id : '') + "' data-type='" + (dataType == 'date' || dataType == 'textarea' ? dataType : 'text') + "'>" + txt.replace(/(?:\r\n|\r|\n)/g, '<br />') + "</span>");

					}else{
						$(this).replaceWith("<span class='canedit' id='" + ((typeof (id) != 'undefined') ? id : '') + "' data-type='" + (dataType == 'date' || dataType == 'textarea' ? dataType : 'text') + "'>" + txt.replace(/(?:\r\n|\r|\n)/g, '<br />') + "</span>");
					}
				}

			});
			$.each($('#khosrapotrobody').find('#potrojariDraftForm').find('.canedit'), function(i,j) {

				if($.trim($(this).text())  == '...' || $.trim($(this).text())  == '(যদি থাকে) ...' || $.trim($(this).text()).length == 0) {
					if ($(this).attr('id') == 'reference' || $(this).attr('id') == 'sender_phone'
						|| $(this).attr('id') == 'sender_fax' || $(this).attr('id') == 'sender_email') {
                        if ($(this).attr('id') == 'reference') {
                            if ($(this).parent().find('span').length > 1) {
                                $(this).hide();
                            } else {
                                $(this).closest('.row').hide();
                            }
                        } else {
                            $(this).closest('.row').hide();
                        }
					}
					else if($(this).attr('id') == 'gov_name' || $(this).attr('id') == 'office_ministry' ||
						$(this).attr('id') == 'offices' || $(this).attr('id') == 'unit_name_editable' || $(this).attr('id') == 'web_url_or_office_address' || $(this).attr('id') == 'office_address') {
						$(this).prev('br').remove();
						$(this).remove();
					}
					else if($(this).attr('id') == 'dynamic_header') {
						$(this).next('br').remove();
						$(this).remove();
					}
				}
			});

			var contentbody = $('#khosrapotrobody').find('#potrojariDraftForm').find('#template-body').html();
			$('#khosrapotrobody').find('#potrojariDraftForm').find('#contentbody').html(contentbody);
			$('#khosrapotrobody').find('#potrojariDraftForm').find('#template-body').html(temp);
//            return false;
        },
        setSovapotiForm: function(sovapotis){
            if ($('#potro-type').val() == 17) {
                if (sovapotis.length == 0) {
                    toastr.error("দুঃখিত! সভাপতি তথ্য দেয়া হয়নি");
                    return false;
                } else {
                    $.each($('.sovapoti_users'), function (i, data) {
                        var sendename = $(this).text().split(', ' + $(this).attr('unit_name'));
                        if (sendename.length == 0) {
                            sendename[0] = sendename;
                        }
                        $('input[name=sovapoti_office_id_final]').val($(this).attr('ofc_id'));
                        $('input[name=sovapoti_officer_designation_id_final]').val($(this).attr('designation_id'));
                        $('input[name=sovapoti_officer_designation_label_final]').val($.trim($(this).attr('designation')));
                        $('input[name=sovapoti_officer_id_final]').val($(this).attr('officer_id'));
                        $('input[name=sovapoti_officer_name_final]').val($.trim(sendename[0]));
                        $('input[name=sovapoti_visibleName]').val(isEmpty($(this).attr('visibleName'))?$.trim(sendename[0]): $.trim($(this).attr('visibleName')));
                        $('input[name=sovapoti_visibleDesignation]').val(isEmpty($(this).attr('visibleDesignation'))?$.trim($(this).attr('designation')): $.trim($(this).attr('visibleDesignation')));

                        var url =  '<?php echo $this->Url->build(['controller' => 'EmployeeRecords','action' => 'userDetails'])?>/' + $(this).attr('officer_id');
                        if(!isEmpty($(this).attr('unit_id'))){
                            url = url +'/'+$(this).attr('unit_id');
                        }
                        $.ajax({
                            url:url,
                            type: 'post',
                            dataType: 'json',
                            async: false,
                            success: function (data) {
                                PotrojariFormEditable.encodeImage(data.username, data.en_username, function (src) {
                                  if(src.indexOf('base64') == -1){
                                    toastr.error("দুঃখিত! সভাপতির স্বাক্ষর লোড হয়নি! ERR: MD-STM01");
                                      throw new Error("Signature is not loaded");
                                    return false;
                                  }
                                    $('#khosrapotrobody').find('#potrojariDraftForm').find('#sovapoti_signature').html('<img src="' + src + '" alt="signature" width="100" />');
                                    if (potrojari_language == 'eng') {
                                        $('#khosrapotrobody').find('#potrojariDraftForm').find('#sovapoti_signature_date').html('<?= bnToen($signature_date_bangla) ?>');
                                    } else {
                                        $('#khosrapotrobody').find('#potrojariDraftForm').find('#sovapoti_signature_date').html('<?= $signature_date_bangla ?>');
                                    }

                                    if (i == 0) {
                                        $('#khosrapotrobody').find('#potrojariDraftForm').find('#sovapoti_signature2').html('<img src="' + src + '" alt="signature" width="100" />');
                                        if (potrojari_language == 'eng') {
                                            $('#khosrapotrobody').find('#potrojariDraftForm').find('#sovapoti_signature_date').html('<?= bnToen($signature_date_bangla) ?>');
                                        } else {
                                            $('#khosrapotrobody').find('#potrojariDraftForm').find('#sovapoti_signature2_date').html('<?= $signature_date_bangla ?>');
                                        }

                                    }
                                });

                            }
                        });

                        if (i == 0) {
                            $('#khosrapotrobody').find('#potrojariDraftForm').find('#sovapoti_designation').text($('input[name=sovapoti_visibleDesignation]').val());
                            $('#khosrapotrobody').find('#potrojariDraftForm').find('#sovapotiname').text($('input[name=sovapoti_visibleName]').val());
                            $('#khosrapotrobody').find('#potrojariDraftForm').find('#sovapoti_designation2').text($('input[name=sovapoti_visibleDesignation]').val());
                            $('#khosrapotrobody').find('#potrojariDraftForm').find('#sovapotiname2').text($('input[name=sovapoti_visibleName]').val());
                        }
                    });
                }
            }
        },
        setSenderForm: function(senders){
            if (senders.length == 0) {
                if ($('#potro-type').val() == 17) {
                    $.each($('.approval_users'), function (i, data) {
                        var sendename = $(this).text().split(', ' + $(this).attr('unit_name'));

                        if (i > 0) {
                            return;
                        }
                        $('input[name=sender_office_id_final]').val($(this).attr('ofc_id'));
                        $('input[name=sender_officer_designation_id_final]').val($(this).attr('designation_id'));
                        $('input[name=sender_officer_designation_label_final]').val($(this).attr('designation'));
                        $('input[name=sender_officer_id_final]').val($(this).attr('officer_id'));
                        $('input[name=sender_officer_name_final]').val(sendename[0]);

                        $('input[name=sender_officer_visibleName]').val(isEmpty($(this).attr('visibleName'))?sendename[0]: $.trim($(this).attr('visibleName')));
                        $('input[name=sender_officer_visibleDesignation]').val(isEmpty($(this).attr('visibleDesignation'))?$.trim($(this).attr('designation')): $.trim($(this).attr('visibleDesignation')));

                        // if ($('#potro-type').val() == 17) {

                        $('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_name').text($('input[name=sender_officer_visibleName]').val());
                        $('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_designation').text($('input[name=sender_officer_visibleDesignation]').val());

                         var url =  '<?php echo $this->Url->build(['controller' => 'EmployeeRecords','action' => 'userDetails'])?>/' + $('.approval_users').eq(0).attr('officer_id');
                        if(!isEmpty($('.approval_users').eq(0).attr('unit_id'))){
                            url =url + '/'+$('.approval_users').eq(0).attr('unit_id');
                        }
                            $.ajax({
                                url:url,
                                type: 'post',
                                dataType: 'json',
                                async: false,
                                success: function (data) {
                                    PotrojariFormEditable.encodeImage(data.username, data.en_username, function (src) {
                                        if(src.indexOf('base64') == -1){
                                          toastr.error("দুঃখিত! অনুমোদনকারী স্বাক্ষর লোড হয়নি! ERR: MD-STM02");
                                            throw new Error("Signature is not loaded");
                                          return false;
                                        }
                                        $('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_signature').html('<img src="' + src + '" alt="signature" width="100" />');
                                    if (potrojari_language == 'eng') {
                                        $('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_signature_date').html('<?= bnToen($signature_date_bangla) ?>');
                                    } else {
                                        $('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_signature_date').html('<?= $signature_date_bangla ?>');
                                    }

                                });

                                if ($.trim($('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_phone').text()).length > 0 && $.trim($('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_phone').text()) != '...') {

                                    if (data.personal_mobile.length > 0) {
                                        if (potrojari_language == 'eng') {
                                            $('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_phone').text(EngFromBn(data.personal_mobile));
                                        } else {
                                            $('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_phone').text(data.personal_mobile);
                                        }

                                    }
                                }
                            }
                        });

                        // }
                    });
                } else {
                    toastr.error("দুঃখিত! অনুমোদনকারী তথ্য দেয়া হয়নি");
                    return false;
                }
            }
            else {
                $.each($('.sender_users'), function (i, data) {
                    if (i > 0) {
                        toastr.error("একাধিক পদবি দেয়া যাবে না। ");
                        return
                    }
                    var sendename = $(this).text().split(', ' + $(this).attr('unit_name'));
                   

                    $('input[name=sender_office_id_final]').val($(this).attr('ofc_id'));
                    $('input[name=sender_officer_designation_id_final]').val($(this).attr('designation_id'));
                    $('input[name=sender_officer_designation_label_final]').val($(this).attr('designation'));
                    $('input[name=sender_officer_id_final]').val($(this).attr('officer_id'));
                    $('input[name=sender_officer_name_final]').val(sendename[0]);
                    $('input[name=sender_officer_visibleName]').val(isEmpty($(this).attr('visibleName'))?sendename[0]: $(this).attr('visibleName'));
                    $('input[name=sender_officer_visibleDesignation]').val(isEmpty($(this).attr('visibleDesignation'))?$(this).attr('designation'): $(this).attr('visibleDesignation'));

                    $('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_designation').text($.trim($('input[name=sender_officer_visibleDesignation]').val()));
                    $('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_name').text($.trim($('input[name=sender_officer_visibleName]').val()));
                    
                     var url =  '<?php echo $this->Url->build(['controller' => 'EmployeeRecords','action' => 'userDetails'])?>/' + $('.sender_users').eq(0).attr('officer_id');
                        if(!isEmpty($('.sender_users').eq(0).attr('unit_id'))){
                            url = url +'/'+$('.sender_users').eq(0).attr('unit_id');
                        }
                    $.ajax({
                        url: url,
                        type: 'post',
                        dataType: 'json',
                        async: false,
                        success: function (data) {

                            if ($('[name=potro_type]').val() != 13) {
                                PotrojariFormEditable.encodeImage(data.username, data.en_username, function (src) {
                                  if(src.indexOf('base64') == -1){
                                    toastr.error("দুঃখিত! প্রেরকের স্বাক্ষর লোড হয়নি! ERR: MD-STM03");
                                      throw new Error("Signature is not loaded");
                                    return false;
                                  }
                                    $('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_signature').html('<img src="' + src + '" alt="signature" width="100" />');
                                    if (potrojari_language == 'eng') {
                                        $('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_signature_date').html('<?= bnToen($signature_date_bangla) ?>');
                                    } else {
                                        $('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_signature_date').html('<?= $signature_date_bangla ?>');
                                    }

                                });
                            }

//                            if ($.trim($('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_email').text()).length > 0
//                                && $.trim($('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_email').text()) != '...') {
//
//                            } else {
//                                if (data.personal_email.length > 0) {
//                                    $('#sender_email').text($.trim(data.personal_email));
//                                }
//                            }
//                            if ($.trim($('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_phone').text()).length > 0 && $.trim($('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_phone').text()) != '...') {
//                            } else {
//                                if (data.personal_mobile.length > 0) {
//                                    if (potrojari_language == 'eng') {
//                                        $('#sender_phone').text(EngFromBn(data.personal_mobile));
//                                    } else {
//                                        $('#sender_phone').text(data.personal_mobile);
//                                    }
//
//                                }
//                            }
//                            if ($.trim($('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_fax').text()).length > 0 && $.trim($('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_fax').text()) != '...') {
//                            } else {
//                                if (typeof(data.fax) != 'undefined') {
//                                    if (potrojari_language == 'eng') {
//                                        $('#sender_fax').text(EngFromBn(data.fax));
//                                    } else {
//                                        $('#sender_fax').text(data.fax);
//                                    }
//
//                                }
//                            }

                        }
                    });
                });
            }
        },
        setApprovalForm: function(approvals){
             if (approvals.length > 0) {
                $('#khosrapotrobody').find('#potrojariDraftForm').find('#sending_date_2').closest('.row').show();
                $('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_name2').closest('.row').show();
                $('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_designation2').closest('.row').show();
                $('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_signature2').closest('.row').show();

                $.each($('.approval_users'), function (i, data) {
                    if ($('input[name=sender_officer_designation_id_final]').val() == $(this).attr('designation_id') && $('#potro-type').val() != 4) {
                        $('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_name2').closest('.row').hide();
                        $('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_designation2').closest('.row').hide();
                        $('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_signature2').closest('.row').hide();
                    }
                    var sendename = $(this).text().split(', ' + $(this).attr('unit_name'));
                    var url =  '<?php echo $this->Url->build(['controller' => 'EmployeeRecords','action' => 'userDetails'])?>/' +$(this).attr('officer_id');
                        if(!isEmpty($(this).attr('unit_id'))){
                            url =url + '/'+$(this).attr('unit_id');
                        }
                    $.ajax({
                        url:url,
                        type: 'post',
                        dataType: 'json',
                        async: false,
                        success: function (data) {
                            PotrojariFormEditable.encodeImage(data.username, data.en_username, function (src) {
                              if(src.indexOf('base64') == -1){
                                toastr.error("দুঃখিত! অনুমোদনকারীর স্বাক্ষর লোড হয়নি! ERR: MD-STM04");
                                  throw new Error("Signature is not loaded");
                                return false;
                              }

                                if($('[name=potro_type]').val() ==17){
                                    $('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_signature').html('<img src="' + src + '" alt="signature" width="100" />');
                                    if (potrojari_language == 'eng') {
                                        $('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_signature_date').html('<?= enTobn($signature_date_bangla) ?>');
                                    } else {
                                        $('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_signature_date').html('<?= $signature_date_bangla ?>');
                                    }
                                }

                                $('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_signature2').html('<img src="' + src + '" alt="signature" width="100" />');
                                if (potrojari_language == 'eng') {
                                    $('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_signature2_date').html('<?= enTobn($signature_date_bangla) ?>');
                                } else {
                                    $('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_signature2_date').html('<?= $signature_date_bangla ?>');
                                }

                            });

                        }
                    });

                    if (i > 0) {
                        $('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_designation2').append(', ');
                        $('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_name2').append(', ');
                        $('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_designation3').append(', ');
                        $('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_name3').append(', ');
                    }
                    $('input[name=approval_visibleName]').val(isEmpty($(this).attr('visibleName'))?$.trim(sendename[0]): $.trim($(this).attr('visibleName')));
                    $('input[name=approval_visibleDesignation]').val(isEmpty($(this).attr('visibleDesignation'))?$.trim($(this).attr('designation')): $.trim($(this).attr('visibleDesignation')));

                    $('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_designation2').text($('input[name=approval_visibleDesignation]').val());
                    $('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_name2').text($('input[name=approval_visibleName]').val());
                    $('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_designation3').text($('input[name=approval_visibleDesignation]').val());
                    $('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_name3').text($('input[name=approval_visibleName]').val());

                    if($('[name=potro_type]').val() ==17){
                        $('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_designation').text($('input[name=approval_visibleDesignation]').val());
                        $('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_name').text($('input[name=approval_visibleName]').val());
                    }

                    $('input[name=approval_office_id_final]').val($(this).attr('ofc_id'));
                    $('input[name=approval_officer_designation_id_final]').val($(this).attr('designation_id'));
                    $('input[name=approval_officer_designation_label_final]').val($(this).attr('designation'));
                    $('input[name=approval_officer_id_final]').val($(this).attr('officer_id'));
                    $('input[name=approval_office_unit_id_final]').val($(this).attr('unit_id'));
                    $('input[name=approval_office_unit_name_final]').val($(this).attr('unit_name'));
                    $('input[name=approval_officer_name_final]').val(sendename[0]);
                    $('input[name=approval_office_name_final]').val($(this).attr('ofc_name'));

                });
            }
            else {

                $.each($('.sender_users'), function (i, data) {
                    var sendename = $(this).text().split(', ' + $(this).attr('unit_name'));
                    var url =  '<?php echo $this->Url->build(['controller' => 'EmployeeRecords','action' => 'userDetails'])?>/' +$(this).attr('officer_id');
                        if(!isEmpty($(this).attr('unit_id'))){
                            url = url +'/'+$(this).attr('unit_id');
                        }
                    $.ajax({
                        url: url,
                        type: 'post',
                        dataType: 'json',
                        async: false,
                        success: function (data) {
                            PotrojariFormEditable.encodeImage(data.username, data.en_username, function (src) {
                              if(src.indexOf('base64') == -1){
                                toastr.error("দুঃখিত! প্রেরকের স্বাক্ষর লোড হয়নি! ERR: MD-STM05");
                                  throw new Error("Signature is not loaded");
                                return false;
                              }
                                $('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_signature2').html('<img src="' + src + '" alt="signature" width="100" />');
                                if (potrojari_language == 'eng') {
                                    $('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_signature2_date').html('<?= enTobn($signature_date_bangla) ?>');
                                } else {
                                    $('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_signature2_date').html('<?= $signature_date_bangla ?>');
                                }
                            });

                        }
                    });

                    if (i > 0) {
                        $('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_designation2').append(', ');
                        $('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_name2').append(', ');
                        $('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_designation3').append(', ');
                        $('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_name3').append(', ');
                    }
                    $('input[name=approval_visibleName]').val(isEmpty($(this).attr('visibleName'))?$.trim(sendename[0]): $.trim($(this).attr('visibleName')));
                    $('input[name=approval_visibleDesignation]').val(isEmpty($(this).attr('visibleDesignation'))?$.trim($(this).attr('designation')): $.trim($(this).attr('visibleDesignation')));

                    $('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_designation2').text($('input[name=approval_visibleDesignation]').val());
                    $('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_name2').text($('input[name=approval_visibleName]').val());

                    $('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_designation3').text($('input[name=approval_visibleDesignation]').val());
                    $('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_name3').text($('input[name=approval_visibleName]').val());

                    $('input[name=approval_office_id_final]').val($(this).attr('ofc_id'));
                    $('input[name=approval_officer_designation_id_final]').val($(this).attr('designation_id'));
                    $('input[name=approval_officer_designation_label_final]').val($(this).attr('designation'));
                    $('input[name=approval_officer_id_final]').val($(this).attr('officer_id'));
                    $('input[name=approval_office_unit_id_final]').val($(this).attr('unit_id'));
                    $('input[name=approval_office_unit_name_final]').val($(this).attr('unit_name'));
                    $('input[name=approval_officer_name_final]').val(sendename[0]);
                    $('input[name=approval_office_name_final]').val($(this).attr('ofc_name'));

                    if ($('input[name=sender_officer_designation_id_final]').val() == $(this).attr('designation_id') && $('#potro-type').val() != 4) {
                        $('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_name2').closest('.row').hide();
                        $('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_designation2').closest('.row').hide();
                        $('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_signature2').closest('.row').hide();
                    }
                });
            }
        },
        setReceiverForm: function(receivers){
            if (receivers.length == 0) {
                if ($('#potro-type').val() != 19 && ($('#khosrapotrobody').find('#potrojariDraftForm').find('#office_organogram_id').length > 0 || $('#potro-type').val() == 13)) {
                    toastr.error("দুঃখিত! প্রাপক তথ্য দেয়া হয়নি");
                    return false;
                } else {
                    $('input[name=receiver_group_id_final]').val('');
                    $('input[name=receiver_group_name_final]').val('');
                    $('input[name=receiver_group_member_final]').val('');
                    $('input[name=receiver_group_designation_final]').val('');
                    $('input[name=receiver_group_sms_final]').val('');
                    $('input[name=receiver_office_id_final]').val('');
                    $('input[name=receiver_office_name_final]').val('');
                    $('input[name=receiver_officer_designation_id_final]').val('');
                    $('input[name=receiver_officer_designation_label_final]').val('');
                    $('input[name=receiver_office_unit_id_final]').val('');
                    $('input[name=receiver_office_unit_name_final]').val('');
                    $('input[name=receiver_officer_id_final]').val('');
                    $('input[name=receiver_officer_name_final]').val('');
                    $('input[name=receiver_officer_email_final]').val('');
                    $('input[name=receiver_office_head_final]').val('');
                    $('input[name=receiver_visibleName_final]').val('');
                    $('input[name=receiver_officer_mobile_final]').val('');
                    $('input[name=receiver_sms_message_final]').val('');
                }
            }
            else {
                if ($('#khosrapotrobody').find('#potrojariDraftForm').find('#office_organogram_id').length > 0 || $('#potro-type').val() == 13) {
                    if (receivers.length == 1) {
                        $.each(receivers, function (i, data) {
                            if (typeof ($(this).attr('group_id')) != 'undefined') {

                                $('input[name=receiver_group_id_final]').val($(this).attr('group_id'));
                                $('input[name=receiver_group_name_final]').val($(this).attr('group_name'));
                                $('input[name=receiver_group_designation_final]').val($(this).attr('group_designation'));
                                $('input[name=receiver_group_member_final]').val($(this).attr('group_member'));
                                $('input[name=receiver_group_sms_final]').val($(this).attr('group_sms_message'));
                            }
                            $('input[name=receiver_office_id_final]').val($(this).attr('ofc_id'));
                            $('input[name=receiver_office_name_final]').val($(this).attr('ofc_name'));
                            $('input[name=receiver_officer_designation_id_final]').val($(this).attr('designation_id'));
                            $('input[name=receiver_officer_designation_label_final]').val($(this).attr('designation'));
                            $('input[name=receiver_office_unit_id_final]').val($(this).attr('unit_id'));
                            $('input[name=receiver_office_unit_name_final]').val($(this).attr('unit_name'));
                            $('input[name=receiver_officer_id_final]').val($(this).attr('officer_id'));
                            $('input[name=receiver_officer_name_final]').val($(this).attr('officer_name'));
                            $('input[name=receiver_officer_email_final]').val($(this).attr('officer_email'));
                            $('input[name=receiver_office_head_final]').val($(this).attr('office_head'));
                             var visibleName = (!isEmpty($(this).attr('visibleName')))?$(this).attr('visibleName'):'';
                            var officer_mobile = (!isEmpty($(this).attr('officer_mobile')))?$(this).attr('officer_mobile'):'';
                            var sms_message = (!isEmpty($(this).attr('sms_message')))?$(this).attr('sms_message'):'';
                            $('input[name=receiver_visibleName_final]').val(visibleName);
                            $('input[name=receiver_officer_mobile_final]').val(officer_mobile);
                            $('input[name=receiver_sms_message_final]').val(sms_message);
                        });
                    } else {
                        var prevgroupId = '';
                        var prevgroupNm = '';
                        var prevgroupNmMem = '';
                        var prevgroupDes = '';
                        var prevgroupSMS = '';
                        var prevOfficeId = '';
                        var prevOfficeNm = '';
                        var prevOfficeOrgId = '';
                        var prevOfficeOrgLb = '';
                        var prevOfficeUnitId = '';
                        var prevOfficeUnitLb = '';
                        var prevOfficerId = '';
                        var prevOfficerNm = '';
                        var prevOfficerEm = '';
                        var prevOfficerHd = '';
                        var prevOfficerVisibleName = '';
                        var prevOfficerMobile = '';
                        var prevOfficerSMSMessage = '';
                        $.each(receivers, function (i, data) {
                            if (typeof ($(this).attr('group_id')) != 'undefined') {
                                $('input[name=receiver_group_id_final]').val(prevgroupId + ";" + $(this).attr('group_id'));
                                prevgroupId = $('input[name=receiver_group_id_final]').val();
                                $('input[name=receiver_group_name_final]').val(prevgroupNm + ";" + $(this).attr('group_name'));
                                prevgroupNm = $('input[name=receiver_group_name_final]').val();
                                $('input[name=receiver_group_designation_final]').val(prevgroupDes + ";" + $(this).attr('group_designation'));
                                prevgroupDes = $('input[name=receiver_group_designation_final]').val();

                                $('input[name=receiver_group_member_final]').val(prevgroupNmMem + ";" + $(this).attr('group_member'));
                                prevgroupNmMem = $('input[name=receiver_group_member_final]').val();
                                $('input[name=receiver_group_sms_final]').val(prevgroupSMS + ";" + $(this).attr('group_sms_message'));
                                prevgroupSMS = $('input[name=receiver_group_sms_final]').val();
                            }
                            $('input[name=receiver_office_id_final]').val(prevOfficeId + ";" + $(this).attr('ofc_id'));
                            prevOfficeId = $('input[name=receiver_office_id_final]').val();
                            $('input[name=receiver_office_name_final]').val(prevOfficeNm + ";" + $(this).attr('ofc_name'));
                            prevOfficeNm = $('input[name=receiver_office_name_final]').val();
                            $('input[name=receiver_officer_designation_id_final]').val(prevOfficeOrgId + ";" + $(this).attr('designation_id'));
                            prevOfficeOrgId = $('input[name=receiver_officer_designation_id_final]').val();
                            $('input[name=receiver_officer_designation_label_final]').val(prevOfficeOrgLb + ";" + $(this).attr('designation'));
                            prevOfficeOrgLb = $('input[name=receiver_officer_designation_label_final]').val();
                            $('input[name=receiver_office_unit_id_final]').val(prevOfficeUnitId + ";" + $(this).attr('unit_id'));
                            prevOfficeUnitId = $('input[name=receiver_office_unit_id_final]').val();
                            $('input[name=receiver_office_unit_name_final]').val(prevOfficeUnitLb + ";" + $(this).attr('unit_name'));
                            prevOfficeUnitLb = $('input[name=receiver_office_unit_name_final]').val();
                            $('input[name=receiver_officer_id_final]').val(prevOfficerId + ";" + $(this).attr('officer_id'));
                            prevOfficerId = $('input[name=receiver_officer_id_final]').val();
                            $('input[name=receiver_officer_name_final]').val(prevOfficerNm + ";" + $(this).attr('officer_name'));
                            prevOfficerNm = $('input[name=receiver_officer_name_final]').val();
                            $('input[name=receiver_officer_email_final]').val(prevOfficerEm + ";" + $(this).attr('officer_email'));
                            prevOfficerEm = $('input[name=receiver_officer_email_final]').val();
                            $('input[name=receiver_office_head_final]').val(prevOfficerHd + ";" + $(this).attr('office_head'));
                            prevOfficerHd = $('input[name=receiver_office_head_final]').val();

                            var visibleName = (!isEmpty($(this).attr('visibleName')))?$(this).attr('visibleName'):'';
                            var officer_mobile = (!isEmpty($(this).attr('officer_mobile')))?$(this).attr('officer_mobile'):'';
                            var sms_message = (!isEmpty($(this).attr('sms_message')))?$(this).attr('sms_message'):'';
                            $('input[name=receiver_visibleName_final]').val(prevOfficerVisibleName + ";" + visibleName);
                            prevOfficerVisibleName = $('input[name=receiver_visibleName_final]').val();
                            $('input[name=receiver_officer_mobile_final]').val(prevOfficerMobile + ";" + officer_mobile);
                            prevOfficerMobile = $('input[name=receiver_officer_mobile_final]').val();
                            $('input[name=receiver_sms_message_final]').val(prevOfficerSMSMessage + ";" + sms_message);
                            prevOfficerSMSMessage = $('input[name=receiver_sms_message_final]').val();
                        });
                    }
                } else {
                    $('input[name=receiver_group_id_final]').val('');
                    $('input[name=receiver_group_name_final]').val('');
                    $('input[name=receiver_group_member_final]').val('');
                    $('input[name=receiver_group_designation_final]').val('');
                    $('input[name=receiver_group_sms_final]').val('');
                    $('input[name=receiver_office_id_final]').val('');
                    $('input[name=receiver_office_name_final]').val('');
                    $('input[name=receiver_officer_designation_id_final]').val('');
                    $('input[name=receiver_officer_designation_label_final]').val('');
                    $('input[name=receiver_office_unit_id_final]').val('');
                    $('input[name=receiver_office_unit_name_final]').val('');
                    $('input[name=receiver_officer_id_final]').val('');
                    $('input[name=receiver_officer_name_final]').val('');
                    $('input[name=receiver_officer_email_final]').val('');
                    $('input[name=receiver_office_head_final]').val('');
                    $('input[name=receiver_visibleName_final]').val('');
                    $('input[name=receiver_officer_mobile_final]').val('');
                    $('input[name=receiver_sms_message_final]').val('');
                    toastr.error("দুঃখিত! পত্রে প্রাপক দেয়া যাবে না!");
                }
            }
        },
        setOnulipiForm: function(onulipis){
             if (onulipis.length == 0) {
                $('input[name=onulipi_group_id_final]').val('');
                $('input[name=onulipi_group_name_final]').val('');
                $('input[name=onulipi_group_member_final]').val('');
                $('input[name=onulipi_group_designation_final]').val('');
                $('input[name=onulipi_group_sms_final]').val('');
                $('input[name=onulipi_office_id_final]').val('');
                $('input[name=onulipi_office_name_final]').val('');
                $('input[name=onulipi_officer_designation_id_final]').val('');
                $('input[name=onulipi_officer_designation_label_final]').val('');
                $('input[name=onulipi_office_unit_id_final]').val('');
                $('input[name=onulipi_office_unit_name_final]').val('');
                $('input[name=onulipi_officer_id_final]').val('');
                $('input[name=onulipi_officer_name_final]').val('');
                $('input[name=onulipi_officer_email_final]').val('');
                $('input[name=onulipi_office_head_final]').val('');
                $('input[name=onulipi_visibleName_final]').val('');
                $('input[name=onulipi_officer_mobile_final]').val('');
                $('input[name=onulipi_sms_message_final]').val('');
            }
            else {
                if ($('#khosrapotrobody').find('#potrojariDraftForm').find('#cc_list_div').length > 0) {
                    var prevgroupId = '';
                    var prevgroupNm = '';
                    var prevgroupNmMem = '';
                    var prevgroupDes = '';
                    var prevOfficeId = '';
                    var prevOfficeNm = '';
                    var prevOfficeOrgId = '';
                    var prevOfficeOrgLb = '';
                    var prevOfficeUnitId = '';
                    var prevOfficeUnitLb = '';
                    var prevOfficerId = '';
                    var prevOfficerNm = '';
                    var prevOfficerEm = '';
                    var prevOfficerHd = '';
                    var prevOfficerVisibleName = '';
                    var prevOfficerMobile = '';
                    var prevOfficerSMSMessage = '';
                    var prevgroupSMS = '';

                    var totalmember = $.map(onulipis,function(data){
                        return parseInt($(data).attr('group_member'));
                    }).reduce(function(total,num){return total+num; });

                    var bn = replaceNumbers("'" + totalmember + "'");
                    bn = bn.replace("'", '');
                    bn = bn.replace("'", '');
                    var sarok = $('#sharok_no').text();
                    if ($('#potro-type').val() == 13) {
                        sarok = $('#khosrapotrobody').find('#potrojariDraftForm').find('#blank-sarok').val();
                    }
                    if (potrojari_language == 'eng') {
                        $('#khosrapotrobody').find('#potrojariDraftForm').find('#sharok_no2').text(sarok + "/1" + (totalmember > 1 ? ("(" + totalmember + ")") : ""));
                    } else {
                        $('#khosrapotrobody').find('#potrojariDraftForm').find('#sharok_no2').text(sarok + "/" + replaceNumbers('1') + (totalmember > 1 ? ("(" + bn + ")") : ""));
                    }

                    $('#khosrapotrobody').find('#potrojariDraftForm').find('#cc_list_div').closest('.row').nextAll('.row').show()
                    $('#khosrapotrobody').find('#potrojariDraftForm').find('#cc_list_div').closest('.row').show();
                    $('#khosrapotrobody').find('#potrojariDraftForm').find('#sharok_no2').closest('.row').show();
                    $('#khosrapotrobody').find('#potrojariDraftForm').find('#sending_date_2').closest('.row').show();
                    $('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_name2').closest('.row').show();
                    $('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_designation2').closest('.row').show();
                    $('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_signature2').closest('.row').show();

                    $.each(onulipis, function (i, data) {

                        if (typeof ($(this).attr('group_id')) != 'undefined') {
                            $('input[name=onulipi_group_id_final]').val(prevgroupId + ";" + $(this).attr('group_id'));
                            prevgroupId = $('input[name=onulipi_group_id_final]').val();
                            $('input[name=onulipi_group_name_final]').val(prevgroupNm + ";" + $(this).attr('group_name'));
                            prevgroupNm = $('input[name=onulipi_group_name_final]').val();

                            $('input[name=onulipi_group_designation_final]').val(prevgroupDes + ";" + $(this).attr('group_designation'));
                            prevgroupDes = $('input[name=onulipi_group_designation_final]').val();
                            $('input[name=onulipi_group_member_final]').val(prevgroupNmMem + ";" + $(this).attr('group_member'));
                            prevgroupNmMem = $('input[name=onulipi_group_member_final]').val();
                            $('input[name=onulipi_group_sms_final]').val(prevgroupSMS + ";" + $(this).attr('group_sms_message'));
                            prevgroupSMS = $('input[name=onulipi_group_sms_final]').val();
                        }

                        $('input[name=onulipi_office_id_final]').val(prevOfficeId + ";" + $(this).attr('ofc_id'));
                        prevOfficeId = $('input[name=onulipi_office_id_final]').val();
                        $('input[name=onulipi_office_name_final]').val(prevOfficeNm + ";" + $(this).attr('ofc_name'));
                        prevOfficeNm = $('input[name=onulipi_office_name_final]').val();
                        $('input[name=onulipi_officer_designation_id_final]').val(prevOfficeOrgId + ";" + $(this).attr('designation_id'));
                        prevOfficeOrgId = $('input[name=onulipi_officer_designation_id_final]').val();
                        $('input[name=onulipi_officer_designation_label_final]').val(prevOfficeOrgLb + ";" + $(this).attr('designation'));
                        prevOfficeOrgLb = $('input[name=onulipi_officer_designation_label_final]').val();
                        $('input[name=onulipi_office_unit_id_final]').val(prevOfficeUnitId + ";" + $(this).attr('unit_id'));
                        prevOfficeUnitId = $('input[name=onulipi_office_unit_id_final]').val();
                        $('input[name=onulipi_office_unit_name_final]').val(prevOfficeUnitLb + ";" + $(this).attr('unit_name'));
                        prevOfficeUnitLb = $('input[name=onulipi_office_unit_name_final]').val();
                        $('input[name=onulipi_officer_id_final]').val(prevOfficerId + ";" + $(this).attr('officer_id'));
                        prevOfficerId = $('input[name=onulipi_officer_id_final]').val();
                        $('input[name=onulipi_officer_name_final]').val(prevOfficerNm + ";" + $(this).attr('officer_name'));
                        prevOfficerNm = $('input[name=onulipi_officer_name_final]').val();
                        $('input[name=onulipi_officer_email_final]').val(prevOfficerEm + ";" + $(this).attr('officer_email'));
                        prevOfficerEm = $('input[name=onulipi_officer_email_final]').val();
                        $('input[name=onulipi_office_head_final]').val(prevOfficerHd + ";" + $(this).attr('office_head'));
                        prevOfficerHd = $('input[name=onulipi_office_head_final]').val();

                        var visibleName = (!isEmpty($(this).attr('visibleName')))?$(this).attr('visibleName'):'';
                        var officer_mobile = (!isEmpty($(this).attr('officer_mobile')))?$(this).attr('officer_mobile'):'';
                        var sms_message = (!isEmpty($(this).attr('sms_message')))?$(this).attr('sms_message'):'';
                        $('input[name=onulipi_visibleName_final]').val(prevOfficerVisibleName + ";" + visibleName);
                        prevOfficerVisibleName = $('input[name=onulipi_visibleName_final]').val();
                        $('input[name=onulipi_officer_mobile_final]').val(prevOfficerMobile + ";" + officer_mobile);
                        prevOfficerMobile = $('input[name=onulipi_officer_mobile_final]').val();
                        $('input[name=onulipi_sms_message_final]').val(prevOfficerSMSMessage + ";" + sms_message);
                        prevOfficerSMSMessage = $('input[name=onulipi_sms_message_final]').val();
                    });
                } else {
                    $('input[name=onulipi_group_id_final]').val('');
                    $('input[name=onulipi_group_name_final]').val('');
                    $('input[name=onulipi_group_member_final]').val('');
                    $('input[name=onulipi_group_designation_final]').val('');
                    $('input[name=onulipi_group_sms_final]').val('');
                    $('input[name=onulipi_office_id_final]').val('');
                    $('input[name=onulipi_office_name_final]').val('');
                    $('input[name=onulipi_officer_designation_id_final]').val('');
                    $('input[name=onulipi_officer_designation_label_final]').val('');
                    $('input[name=onulipi_office_unit_id_final]').val('');
                    $('input[name=onulipi_office_unit_name_final]').val('');
                    $('input[name=onulipi_officer_id_final]').val('');
                    $('input[name=onulipi_officer_name_final]').val('');
                    $('input[name=onulipi_officer_email_final]').val('');
                    $('input[name=onulipi_office_head_final]').val('');
                    $('input[name=onulipi_visibleName_final]').val('');
                    $('input[name=onulipi_officer_mobile_final]').val('');
                    $('input[name=onulipi_sms_message_final]').val('');
                    $('#khosrapotrobody').find('#potrojariDraftForm').find('#cc_list_div').html("");

                    toastr.error("দুঃখিত! পত্রে অনুলিপি দেয়া যাবে না!");
                }
            }
        },
        saveDraft: function () {
          
            Metronic.blockUI({
                target: '#ajax-content'
            });
            $("select[name='prapto_potro[]'] option").prop('selected', true);

            $prom = new Promise(function (resolve, reject) {


                //start ajax 01
                var sharok_tmp=$("#sharok_no").html();
                $.ajax({
                    url: '<?php
                        echo $this->Url->build(['controller' => 'dakDaptoriks',
                            'action' => 'checkSarokPotrojariDuplicate']);
                        ?>',
                    data: {sarok_no:sharok_tmp},
                    method: "post",
                    dataType: 'JSON',
                    async: false,
                    cache: false,
                    success: function (response) {
                        if (response.status == 'error') {
                            toastr.error("দুঃখিত! খসড়া সংরক্ষণ করা সম্ভব হচ্ছে না।");
                            Metronic.unblockUI('#ajax-content');
                            $("#potrojariDraftForm").next().next().find('button').removeAttr('disabled');
                            reject("দুঃখিত! স্মারক নম্বরটি ব্যবহৃত হয়ে গেছে")
                        } else {
                            // $('#potrojariDraftForm').find('#reference').parent().find('a').eq(1).remove();
                            // return;
                            resolve();
                            //console.log('success it can be saved');
                        }
                    },
                    error: function (xhr, status, errorThrown) {
                        toastr.options = {
                            "closeButton": true,
                            "debug": false,
                            "positionClass": "toast-bottom-right"
                        };
                        toastr.error("দুঃখিত! খসড়া সংরক্ষণ করা সম্ভব হচ্ছে না। পুনরায় চেষ্টা করুন");
                        Metronic.unblockUI('#ajax-content');
                        reject("দুঃখিত!সভাপতির স্বাক্ষর লোড হয়নি।")
                    }
                });
                //end ajax 01



                if ($('#khosrapotrobody').find('#potrojariDraftForm').find('.fr-toolbar').length > 0) {
                    $("#pencil").click();
                    var khosrabody = $("#noteView").html();
                    if(isEmpty(khosrabody) || khosrabody=="<br><br>"){
                        reject("দুঃখিত! খসড়া দেওয়া হয়নি।");
                    } else{
                        resolve("done");
                    }
                } else {
                    var khosrabody = $("#noteView").html();
                    if(isEmpty(khosrabody) || khosrabody=="<br><br>"){
                        reject("দুঃখিত! খসড়া দেওয়া হয়নি।");
                    } else{
                        resolve("done");
                    }
                }


            })
            .then(function(r){
                if (DRAFT_FORM.setform() != false) {
                    var checkpromise = new Promise (function(resolve, reject){
                        if($('#khosrapotrobody').find('#potrojariDraftForm').find('#contentbody').text().length==0){
                            reject("দুঃখিত! পুনরায় সংশোধন করে সংরক্ষণ করুন।");
                        }else if($('#potro-type').val() == 13){
                            resolve();
                        }else {
                            var attr2check_sender_signature_img = $('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_signature img');
                            var attr2check_sender_signature2_img = $('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_signature2 img');
                            var attr2check_sender_signature3_img = $('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_signature3 img');
                            var attr2check_sovapoti_signature_img = $('#khosrapotrobody').find('#potrojariDraftForm').find('#sovapoti_signature img');
                            var attr2check_sovapoti_signature2_img = $('#khosrapotrobody').find('#potrojariDraftForm').find('#sovapoti_signature2 img');

                            if (($('#potro-type').val() != 4) && (attr2check_sender_signature_img.attr('src') == '' || typeof(attr2check_sender_signature_img.attr('src')) == 'undefined' || (attr2check_sender_signature_img.attr('src').indexOf('base64') == -1) ) ) {
                                reject("দুঃখিত! প্রেরক/অনুমোদনকারীর স্বাক্ষর লোড হয়নি।")
                            }
                            else if ($('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_signature2').length > 0
                                && (attr2check_sender_signature2_img.attr('src') == '' ||
                                    typeof(attr2check_sender_signature2_img.attr('src')) == 'undefined' || (attr2check_sender_signature2_img.attr('src').indexOf('base64') == -1))) {
                                reject("দুঃখিত! প্রেরক/অনুমোদনকারীর স্বাক্ষর লোড হয়নি।")
                            }
                            else if ($('#khosrapotrobody').find('#potrojariDraftForm').find('#sender_signature3').length > 0
                                && (attr2check_sender_signature3_img.attr('src') == '' ||
                                    typeof(attr2check_sender_signature3_img.attr('src')) == 'undefined' || (attr2check_sender_signature3_img.attr('src').indexOf('base64') == -1))) {
                                reject("দুঃখিত! প্রেরক/অনুমোদনকারীর স্বাক্ষর লোড হয়নি।")
                            }

                            else if ($('#khosrapotrobody').find('#potrojariDraftForm').find('#sovapoti_signature').length > 0
                                && (attr2check_sovapoti_signature_img.attr('src') == '' ||
                                    typeof(attr2check_sovapoti_signature_img.attr('src')) == 'undefined' || (attr2check_sovapoti_signature_img.attr('src').indexOf('base64') == -1))) {
                                reject("দুঃখিত! সভাপতির স্বাক্ষর লোড হয়নি।")
                            }

                            else if ($('#khosrapotrobody').find('#potrojariDraftForm').find('#sovapoti_signature2').length > 0
                                && (attr2check_sovapoti_signature2_img.attr('src') == '' ||
                                    typeof(attr2check_sovapoti_signature2_img.attr('src')) == 'undefined' || (attr2check_sovapoti_signature2_img.attr('src').indexOf('base64') == -1))) {
                                reject("দুঃখিত!সভাপতির স্বাক্ষর লোড হয়নি।")
                            } else {
                                resolve();
                            }
                        }

                        }).then(function () {
                            $("#potrojariDraftForm").next().next().find('button').attr('disabled', 'disabled');

                            //ajx start 02
                            $.ajax({
                                url: '<?php
                                    echo $this->Url->build(['controller' => 'Potrojari',
                                            'action' => 'saveDraft']) . '/' . $nothiMasterId . '/' . $potroid . '/' . $type;
                                    ?>',
                                data: $("#potrojariDraftForm").serialize(),
                                method: "post",
                                dataType: 'JSON',
                                async: false,
                                cache: false,
                                success: function (response) {
                                    if (response.status == 'error') {
                                        $("#potrojariDraftForm").next().next().find('button').removeAttr('disabled');
                                        toastr.error(response.msg);
                                        Metronic.unblockUI('#ajax-content');
                                    } else {
                                        if(typeof(deleteOfflineDraft) == 'function'){
                                            deleteOfflineDraft($("#potro-type").val(),potrojari_language);
                                            setTimeout(function(){
                                                window.location.reload();
                                            },5000);
                                        }else{
                                            setTimeout(function(){
                                                window.location.reload();
                                            },1000)
                                        }
                                        toastr.success(response.msg);

                                    }
                                },
                                error: function (xhr, status, errorThrown) {
                                    toastr.options = {
                                        "closeButton": true,
                                        "debug": false,
                                        "positionClass": "toast-bottom-right"
                                    };
                                    toastr.error("দুঃখিত! খসড়া সংরক্ষণ করা সম্ভব হচ্ছে না। পুনরায় চেষ্টা করুন");
                                    Metronic.unblockUI('#ajax-content');
                                }
                            });
                            //end ajax 02

                             }).catch(function (err) {
                            toastr.error(err);
                            Metronic.unblockUI('#ajax-content');
                        });
                    } else {
                        toastr.error("দুঃখিত! খসড়া সংরক্ষণ করা সম্ভব হচ্ছে না। পুনরায় চেষ্টা করুন");
                        Metronic.unblockUI('#ajax-content');
                    }
                }).catch(function(err){
                    toastr.error(err);
                    Metronic.unblockUI('#ajax-content');
                });

        }
    };

    (function ($) {

        $('select').select2();
        $('.no-select2').select2('destroy');
        $('select>option[title]').tooltip({'placement':'top', 'container':'body'});
        $('[data-toggle="tooltip"]').tooltip({'placement':'bottom'});
        PotroJariFileUpload.init();
        // get last potro draft
        if(typeof(getLastDraftInfo) == 'function'){
            getLastDraftInfo(offline_potro_draft_key);
        }
    }(jQuery));


    $("#potro-type").change(function (e) {
        if (isEmpty($("#potro-type").val())) {
            return;
        }
        if($("#potro-type").val() == 30){
            //cs
            window.location = '<?=$this->Url->build(['_name' => 'make-cs-letter',$nothiMasterId,$potroid,$type,$attached_id,0,$office_id]) ?>';
            return;
        }
        $('.sovabox').hide();
        $('.sender_list').show();
        $('.approval_list').show();
        $('.receiver_list').show();
        $('.onulipi_list').show();
        $('.attension_list').show();
        $("#template-body").show("slow");
        $("#template-body").html('<img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading text-center"><span>&nbsp;&nbsp;লোড করা হচ্ছে...</span>');
        var potrojari_language = ($('#potrojari_language').bootstrapSwitch('state') == true) ? 'bn' : 'eng';
        $.ajax({
            url: js_wb_root + 'potrojariTemplates/getTemplate/' + $("#potro-type").val() + '/1/' + potrojari_language,
            dataType: 'json',
            cache: false,
            success: function (response) {
                if (typeof response.html_content != 'undefined') {
                    $('.receiver_list').show();
                    $('.sender_list').show();
                    $('.onulipi_list').show();
                    $('.sender_list').find('a').closest('.potrojariOptions').html('অনুমোদনকারী <i class="glyphicon glyphicon-chevron-down pull-right"></i>');
                    $("a[href=#tab_other_potro]").text("অন্যান্য ");
					if($('#potro-type').val() == 30){
						$('#template-csbody').show()
						$('#template-body2').show()
						$('#template-body3').show()
						$('#template-body2').html(response[0].html_content);
						$('#template-body3').html(response[1].html_content);
						$('#template-body').remove()
						$('#contentbody').remove()
						$('#template-body2').attr('id','template-body2')
						$('#contentbody2').attr('id','contentbody')
					}
                    else{
						$('#template-body2').remove()
						$('#template-csbody').hide()
						$('#template-body').html(response.html_content);
                    }
                    $('#subject').attr('data-value','...');
                    if ($('#potro-type').val() == 13) {
                        $('.khalipotro').show();
                        $('#khosrapotrobody').find('#potrojariDraftForm').find('#blank-sarok').val('<?php echo $nothiMasterInfo['nothi_no'] ?>-<?php echo $totalPotrojari; ?>');
                        $('#khosrapotrobody').find('#potrojariDraftForm').find('#blank-subject').val("<?php echo htmlspecialchars(trim($potroSubject)); ?>");

                    } else if ($('#potro-type').val() == 17) {
                        $('.sovabox').show();
                        $('.sender_list').hide();
                        removeSelections('sender_');
                        $('.receiver_list').hide();
                        removeSelections('receiver_');
						$('.attension_list').hide();
                        removeSelections('attension_');
                    } else if ($('#potro-type').val() == 11) {
                        if(potrojari_language == 'eng'){
                            $('#subject').attr('data-value','Notification');
                        } else{
                            $('#subject').attr('data-value','প্রজ্ঞাপন');
                        }
						$('.approval_list').hide();
                        removeSelections('approval_');
						$('.receiver_list').hide();
                        removeSelections('receiver_');
					} else if ($('#potro-type').val() == 19) {
                            $('.attentionbox').hide();
                        removeSelections('attension_');
                    }
					else if ($('#potro-type').val() == 9) {
						$('.attentionbox').hide();
                        removeSelections('attension_');
					} else if($('#potro-type').val() == 4){
                        // $('#subject').attr('data-value','আধা-সরকারি পত্র');
                        $('.onulipi_list').hide();
                        removeSelections('onulipi_');
						$('.attentionbox').hide();
                        removeSelections('attension_');
                    } else if($('#potro-type').val() == 5){
                        $('.receiver_list').hide();
                        removeSelections('receiver_');
						$('.attentionbox').hide();
                        removeSelections('attension_');
                    } else if($('#potro-type').val() == 10){
                        $('.receiver_list').hide();
                        removeSelections('receiver_');
						$('.attentionbox').hide();
                        removeSelections('attension_');
                    }  else if($('#potro-type').val() == 12){
					    if(potrojari_language == 'eng'){
                            $('#subject').attr('data-value','Office Order');
                        } else {
                            $('#subject').attr('data-value','অফিস আদেশ');
                        }
                        $('.receiver_list').hide();
                        removeSelections('receiver_');
                    } else if($('#potro-type').val() == 19){
                         $("#form_name_noc").html('<?=h($employee_office['office_name'])?>');
                         $("#ministry_name_noc").html('<?='( '. h($employee_office['office_unit_name']) .' )'?>');
                    } else if($('#potro-type').val() == 20){
                        $('.approval_list').hide();
                        removeSelections('approval_');
                        $('.attension_list').hide();
                        removeSelections('attension_');
                        $('.sender_list').find('a').closest('.potrojariOptions').html('প্রেরক <i class="glyphicon glyphicon-chevron-down pull-right"></i>');
                        $("#form_name_noc").html('<?=h($employee_office['office_name'])?>');
                        $("#ministry_name_noc").html('<?= '( '. h($employee_office['office_unit_name']).' )'?>');
                        $("#sender_unit").html('<?= h($employee_office['office_unit_name'])?>');
                        $("a[href=#tab_other_potro]").text("সংলগ্নী ");
                    } else if($('#potro-type').val() == 22){
                        $('.approval_list').hide();
                        removeSelections('approval_');
                        $('.onulipi_list').hide();
                        removeSelections('onulipi_');
                        $('.attension_list').hide();
                        removeSelections('attension_');
                        $('.sender_list').find('a').closest('.potrojariOptions').html('এন.ও.সি প্রদানকারী <i class="glyphicon glyphicon-chevron-down pull-right"></i>');
                        $("#office_website").html('<?= h($employee_office['office_web'])?>');
                        $("#office_name").html('<?= 'সরকারি/স্বায়ত্তশাসিত/রাষ্ট্রায়াত্ত সংস্থার নামঃ '.h($employee_office['office_name'])?>');
                    }
                    else if($('#potro-type').val() == 31){
                        $('.attension_list').hide();
                        removeSelections('attension_');
                    }
                    else {
                        $('.khalipotro').hide();
                    }
                    //initialzie and check for version
                    var head = '';
                    if (potrojari_language == 'eng') {
                        head = (<?php echo !empty($heading['potrojari_head_eng'])?(str_replace(["\r\n"], ["<br/>"],$heading['potrojari_head_eng'])):json_encode([]) ?>);
					} else {
                        head = JSON.parse('<?php echo str_replace(["'",'"',"\\\\","\\r\\n"], ["\'",'\"',"\\","<br/>"],$heading['potrojari_head']) ?>');
                    }

                    if(head.remove_header_left_slogan==1){
						$('#khosrapotrobody').find('#potrojariDraftForm').find('#left_slogan').remove();
                    }

                    if(head.remove_header_right_slogan==1){
						$('#khosrapotrobody').find('#potrojariDraftForm').find('#right_slogan').next('br').remove();
						$('#khosrapotrobody').find('#potrojariDraftForm').find('#right_slogan').remove();
                    }

                    if(head.remove_header_head_1==1){
						$('#khosrapotrobody').find('#potrojariDraftForm').find('#gov_name').prev('br').remove();
						$('#khosrapotrobody').find('#potrojariDraftForm').find('#gov_name').remove();
                    }

                    if(head.remove_header_head_2==1){
						$('#khosrapotrobody').find('#potrojariDraftForm').find('#office_ministry').prev('br').remove();
						$('#khosrapotrobody').find('#potrojariDraftForm').find('#office_ministry').remove();
                    }

                    if(head.remove_header_head_3==1){
						$('#khosrapotrobody').find('#potrojariDraftForm').find('#offices').prev('br').remove();
						$('#khosrapotrobody').find('#potrojariDraftForm').find('#offices').remove();
                    }

                    if(head.remove_header_unit==1){
						$('#khosrapotrobody').find('#potrojariDraftForm').find('#unit_name_editable').prev('br').remove();
						$('#khosrapotrobody').find('#potrojariDraftForm').find('#unit_name_editable').remove();
                    }
                    if(head.remove_header_head_4==1){
						$('#khosrapotrobody').find('#potrojariDraftForm').find('#web_url_or_office_address').prev('br').remove();
						$('#khosrapotrobody').find('#potrojariDraftForm').find('#web_url_or_office_address').remove();
                    }
                    if(head.remove_header_head_5==1){
						$('#khosrapotrobody').find('#potrojariDraftForm').find('#office_address').prev('br').remove();
						$('#khosrapotrobody').find('#potrojariDraftForm').find('#office_address').remove();
					}


					$('#khosrapotrobody').find('#potrojariDraftForm').find('#dynamic_header').attr('data-type','text');
					$('#khosrapotrobody').find('#potrojariDraftForm').find('#dynamic_header').text('...');
                    //initialise and check for version
                    PotrojariFormEditable.init(head, '<?php echo $nothiMasterInfo['nothi_no'] ?>', ($('.referencebody').find('#sharok_no').text() + ' ' + $('.referencebody').find('#sending_date').text()), <?php echo json_encode($employee_office) ?>, "<?php echo htmlspecialchars(trim($potroSubject)); ?>", '', '<?php echo $totalPotrojari; ?>', potrojari_language,<?php echo !empty($heading['write_unit'])?$heading['write_unit']:0 ?>,<?php echo ($heading['potrojari_head_img']) ?>);
                    //check offline draft
                    if(typeof(getPotroOffline) == 'function'){
                        getPotroOffline($("#potro-type").val(),potrojari_language);
                    }
                    //check offline draft
                    if(typeof(setInformation) == 'function'){
                        setInformation();
                    }
                }
                else {
                    $("#template-body").hide("slow");
                }
                $("#potroShow").css('min-height', $("#potroShow").css('height')).css('height', $("#khosrapotrobody").css('height'));
            }
        });
    });

    $(document).on('click', '.remove_list', function () {
        $(this).closest('li').remove();
        setInformation();
    });

    $(document).on('click', '#khosrapotrobody #potrojariDraftForm #dynamic_header', function () {
    	if($(this).text() == '...'){
			$('#potrojariDraftForm').find('#dynamic_header').editable(
                'setValue', potrojari_language=='eng'?'(Replaced by same date and memorandum)':'(একই তারিখ ও স্মারকে প্রতিস্থাপিত)'
            )
    		//$(this).val(potrojari_language=='eng'?'(Replaced by same date and memorandum)':'(একই তারিখ ও স্মারকে প্রতিস্থাপিত)')
        }
    });


    $(document).on('change', '#potro-priority-level', setInformation);
    $(document).on('change', '#potro-security-level', setInformation);


    $('select#multiselect_left option').dblclick(function() {
        var title = "";
        var id = $(this).val();
        PROJAPOTI.ajaxSubmitDataCallback('<?php echo $this->request->webroot; ?>NothiMasters/showPopUp/' + id + '/potro/' + <?= $nothiMasterInfo['nothi_masters_id'] ?>+'?nothi_part='+'<?= $nothiMasterId ?>'+'&token='+'<?= sGenerateToken(['file' => $nothiMasterId], ['exp' => time() + 60 * 300]) ?>' , {'nothi_office':<?php echo $office_id ?>}, 'html', function (response) {
            $('#responsiveOnuccedModal').modal('show');
            $('#responsiveOnuccedModal').find('.modal-title').text(title);
            $('#responsiveOnuccedModal').find('.modal-body').html(response);
        });
    });
    $('#prapto-potro').multiSelect({
        dblClick: true,
        afterSelect: function (values) {
            var title = "";
            var id = values;
            $('#responsiveOnuccedModal').find('.modal-title').text('');
            $('#responsiveOnuccedModal').find('.modal-body').html('');
            PROJAPOTI.ajaxSubmitDataCallback('<?php echo $this->request->webroot; ?>NothiMasters/showPopUp/' + id + '/potro/' + <?= $nothiMasterInfo['nothi_masters_id'] ?>+'?nothi_part='+'<?= $nothiMasterId ?>'+'&token='+'<?= sGenerateToken(['file' => $nothiMasterId], ['exp' => time() + 60 * 300]) ?>' , {'nothi_office':<?php echo $office_id ?>}, 'html', function (response) {
                $('#responsiveOnuccedModal').modal('show');
                $('#responsiveOnuccedModal').find('.modal-title').text(title);
                $('#responsiveOnuccedModal').find('.modal-body').html(response);
            });
        }
    });


    $(document).on('click', '.btn-printpreview', function () {
        $('#previewModal').find('.modal-title').text('');
        if ($('#khosrapotrobody').find('#potrojariDraftForm').find('#note').attr('contenteditable') == "true") {
            $('#khosrapotrobody').find('#potrojariDraftForm').find('#note').removeAttr('contenteditable');
            $('#khosrapotrobody').find('#potrojariDraftForm').find('#pencil').html(' <i class="fs1 a2i_gn_edit2"></i> [সম্পাদন  করুন] <br/>');
            $('#note').froalaEditor('destroy');
        }

        var metadata = $.cookie('meta_data_val');
        if(typeof metadata !='undefined') {
            metadata = JSON.parse($.cookie('meta_data_val'));
            $('#marginform').find('#margin-top').val(metadata.margin_top)
            $('#marginform').find('#margin-bottom').val(metadata.margin_bottom)
            $('#marginform').find('#margin-right').val(metadata.margin_right)
            $('#marginform').find('#margin-left').val(metadata.margin_left)
            $('#marginform').find('#orientation').val(metadata.orientation)
            $('#marginform').find('#potro-header').val(metadata.potro_header)
        }

        var body = $('#template-body').html();
        $('#previewModal').modal('show');
        $('#previewModal').find('.modal-title').text('পত্রজারি প্রিন্ট প্রিভিউ');
        $('#previewModal').find('.showPreview').removeAttr('src');

        $(document).off('click', '.btn-pdf-margin').on('click', '.btn-pdf-margin', function () {
            $('#previewModal').find('.loading').show();
            $('#previewModal').find('.btn-pdf-download').removeAttr('href').addClass('hide');
            PROJAPOTI.ajaxSubmitAsyncDataCallback('<?= $this->Url->build(['controller' => 'Potrojari', 'action' => 'getPdfByBody']) ?>',
                {
                    name: '<?= $nothiMasterId . '_draft_' . $office_id ?>',
                    body: body,
                    reset: true,
                    margin_top: parseFloat($('#marginform').find('#margin-top').val()),
                    margin_right: parseFloat($('#marginform').find('#margin-right').val()),
                    margin_bottom: parseFloat($('#marginform').find('#margin-bottom').val()),
                    margin_left: parseFloat($('#marginform').find('#margin-left').val()),
                    orientation: $('#marginform').find('#orientation').val(),
					potro_header: $('#marginform').find('#potro-header').val()
                }, 'json', function (response) {
                    if (response.status == 'success') {
                        $('#previewModal').find('.loading').hide();
                        $('#previewModal').find('.btn-pdf-download').attr('href', response.src).removeClass('hide');
                        $('#previewModal').find('.showPreview').attr('src', response.src);
                    } else {
                        $('#previewModal').find('.loading').hide();
                        $('#previewModal').find('.btn-pdf-download').removeAttr('href').addClass('hide');
                        toastr.error(response.msg);
                    }
                }
            );
        });

        $(document).off('click', '.btn-save-pdf-margin').on('click', '.btn-save-pdf-margin', function () {
            var obj = {
                margin_top: parseFloat($('#marginform').find('#margin-top').val()),
                margin_right: parseFloat($('#marginform').find('#margin-right').val()),
                margin_bottom: parseFloat($('#marginform').find('#margin-bottom').val()),
                margin_left: parseFloat($('#marginform').find('#margin-left').val()),
                orientation: $('#marginform').find('#orientation').val(),
				potro_header: $('#marginform').find('#potro-header').val(),
            };
	        // change live page
            $("#template-body").css("padding-top", obj.margin_top*2.54+"cm");
            $("#template-body").css("padding-right", obj.margin_right*2.54+"cm");
            $("#template-body").css("padding-bottom", obj.margin_bottom*2.54+"cm");
            $("#template-body").css("padding-left", obj.margin_left*2.54+"cm");

            $('input[name=meta_data]').val(JSON.stringify(obj));

            $.cookie('meta_data_val', JSON.stringify(obj));

            toastr.success('মার্জিন যুক্ত করা হয়েছে');
            $('#previewModal').modal('hide');
        });
    });

	$(document).ready(function() {
        // var metaData = $.parseJSON($("input[name=meta_data]").val());
		$("#template-body").addClass('A4-draft');
		<?php if(isset($is_endorse)){ ?>
        $('#khosrapotrobody').closest('.collapsable').find('.btn-maximize').click();

    <?php } ?>
        // typeof metaData.margin_left
	});

    function check_attachment() {
        if ($('#potro-type').val() == 20) {
            if( $('#fileuploadpotrojari .potro-attachment-input').length > 0){
                $(".sologni_div").show();
            }else{
                $(".sologni_div").hide();
            }
            var i=1;
            var songlogni_text = '';
            $('#fileuploadpotrojari .potro-attachment-input').each(function () {
                var tx = $(this).val();
                var songlogni_set_text = $("#lm_songlogni_set_"+i).text();
                if(isEmpty(songlogni_set_text)){
                    songlogni_set_text='...';
                }
                songlogni_text += "<tr><td>"+BnFromEng(i)+"। "+"</td><td>"+tx+"</td><td><a class='lm_songlogni_set' id='lm_songlogni_set_"+i+"'>"+songlogni_set_text+"</a></td></tr>";
                i++;
            });
            $('#attachment_list').html('<table style="width:100%">'+songlogni_text+'</table>');
            $('#potrojariDraftForm').find('.lm_songlogni_set').editable({
                inputclass: 'form-control input-medium',
                url: '/post',
                type: 'text',
                pk: 1,
                name: 'lm_songlogni_set'
            });
        }
        check_potro_ref_title();
    }
</script>


<!-- End: JavaScript -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<script id="template-upload2" type="text/x-tmpl">
    {% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
    <td>
    <span class="preview"></span>
    </td>
    <td>
    <p class="name">{%=file.name%}</p>
    <strong class="error label label-danger"></strong>
    </td>
    <td>
    <p class="size">প্রক্রিয়াকরন চলছে...</p>
    <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
    <div class="progress-bar progress-bar-success" style="width:0%;"></div>
    </div>
    </td>
    <td>
    {% if (!i && !o.options.autoUpload) { %}
    <button class="btn blue start" disabled>
    <i class="fa fa-upload"></i>
    <span>আপলোড করুন</span>
    </button>
    {% } %}
    {% if (!i) { %}
    <button class="btn red cancel">
    <i class="fa fa-ban"></i>
    <span>বাতিল করুন</span>
    </button>
    {% } %}
    </td>
    </tr>
    {% } %}




</script>
<!-- The template to display files available for download -->
<script id="template-download2" type="text/x-tmpl">
    {% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade">
    <td width="20%">
    <span class="preview">
    {% if (file.thumbnailUrl) { %}
    <a href="<?= FILE_FOLDER ?>{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="<?= FILE_FOLDER ?>{%=file.thumbnailUrl%}"></a>
    {% } %}
    </span>
    </td>
    <td width="25%">
    <p class="name">
    {% if (file.url) { %}
    <a href="<?= FILE_FOLDER ?>{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
    {% } else { %}
    <span>{%=file.name%}</span>
    {% } %}
    </p>
    {% if (file.error) { %}
    <div><span class="label label-danger"> ত্রুটি: </span> {%=file.error%}</div>
    {% } %}
    </td>
    <td width="10%">
    <span class="size">{%=o.formatFileSize(file.size)%}</span>
    </td>
    <td width="40%" style ="min-width: 100px!important;">

        <input type="text" title="{%=file.name%}" class="form-control potro-attachment-input" image="{%=file.url%}" file-type="{%=file.type%}" onkeyup="check_attachment()" placeholder="সংযুক্তির নাম">

    </td>
    <td width="5%">
    {% if (file.deleteUrl) { %}
    <button class="btn red delete btn-sm" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
    <i class="fs1 a2i_gn_delete2"></i>
    <span>মুছে ফেলুন</span>
    </button>

    {% } else { %}
    <button class="btn yellow cancel btn-sm">
    <i class="fa fa-ban"></i>
    <span>বাতিল করুন</span>
    </button>
    {% } %}
    </td>
    </tr>
    {% } %}



</script>