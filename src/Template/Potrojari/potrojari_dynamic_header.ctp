<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
    <meta charset="utf-8"/>
    <title>নথি | অফিস ব্যবস্থাপনা</title>
    <style>
        *{
            margin:0!important;
            padding:0!important;
        }
        body{
            font-family: Nikosh, SolaimanLipi, 'Open Sans', sans-serif !important;
        }
    </style>
</head>
<?php
if(!empty($header)) {
    $head = !empty($heading)?$heading:[
      'banner_position'=>'left','banner_width'=>'auto'
    ];
//    $background_image_style = 'height: 60px!important;width: auto!important;background: url(' . trim($header) . ') no-repeat ';
//
//    if(!empty($head['banner_position'])){
//        $background_image_style .= $head['banner_position'];
//    }else{
//        $background_image_style .= 'left';
//    }
//    if(!empty($head['banner_position'])){
//        $background_image_style .= $head['banner_position'].' top';
//    }else{
//        $background_image_style .= 'left top';
//    }

    $background_image_style = 'height: 60px!important;width: auto!important;
    background: url(' . trim($header) . ') no-repeat '. (!empty($head['banner_position'])?$head['banner_position']:'left').' top;background-size:' . (!empty($head['banner_width'])?($head['banner_width']=='auto'?'contain':'100% 100%'):'contain');
}else{
    $background_image_style = '';
}
?>
<body style="margin:0!important;padding:0px;<?= $background_image_style ?>">
<?= isset($title)?$title:'' ?>
</body>
</html>