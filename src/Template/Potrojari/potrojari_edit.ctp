<div class="portlet box purple">
    <div class="portlet-title">
        <div class="caption">
            পত্রজারি টেম্পলেট
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-scrollable">
            <?= $this->Form->create($template,['method'=>'post']) ?>
            <div class="row form-body">
                <div class="col-md-6 col-sm-12">
                    <?= $this->Form->textarea('html_content', ['class' => 'form-control', 'style' => 'height:500px']) ?>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="preview" style="margin:10px auto;border: 1px solid #0c0c0c; padding:5px;">
                        <?= $template->html_content ?>
                    </div>
                </div>
            </div>
            <div class="form-actions">
                <?= $this->Form->button('Save', ['class' => 'btn btn-primary']) ?>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>

<script>
    $('body').addClass('page-quick-sidebar-over-content page-full-width');
    $('[name=html_content]').on("input", function () {
        $('.preview').html($(this).val());
    })
</script>