<div class="portlet box purple">
    <div class="portlet-title">
        <div class="caption">
            পত্রজারি টেম্পলেট
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-scrollable">
            <table class="table table-hover table-bordered table-stripped">
                <thead>
                <tr class="heading">
                    <th><?= __("No") ?></th>
                    <th><?= __("Title") ?></th>
                    <th><?= __("Body") ?></th>
                    <th><?= __("Action") ?></th>
                </tr>
                </thead>
                <?php if (!empty($potrojari)): ?>
                    <?php foreach ($potrojari as $key => $value): ?>
                        <tr>
                            <td style="width:5%"><?= $value['id'] ?></td>
                            <td style="width:10%"><?= $value['template_name'] ?></td>
                            <td style="width:75%"><?= html_entity_decode($value['html_content']) ?></td>
                            <td style="width:10%"><?= $this->Html->link('Edit',['controller'=>'Potrojari','action'=>'potrojariEdit',$value['id']],['class'=>'btn btn-primary']) ?></td>

                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
            </table>
        </div>
    </div>
</div>
<script src = "<?php echo CDN_PATH; ?>js/tinymce/tinymce.min.js" type = "text/javascript" ></script>

<script>

</script>
