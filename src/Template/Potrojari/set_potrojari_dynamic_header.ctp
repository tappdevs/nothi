<link rel="stylesheet" href="<?= $this->request->webroot ?>css/trumbowyg.min.css">
<style>
    .htmlPreview{
        overflow: auto;
        width: 8.2in
    }
</style>
<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">
            পত্রজারি হেডার ব্যানার
        </h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-6">
                <?= $this->Form->create($heading,
                    ['class' => 'form-body headerSetting','url' =>['controller' => 'Potrojari','action' => 'setPotrojariDynamicHeader'],'type'=>'file'])
                ?>

                <label for="head_logo"><?= __("Banner") ?></label><span style="font-size: 10pt;"> (উচ্চতাঃ ৬০পিক্সেল, সাইজঃ সর্বোচ্চ ১এমবি)</span>

                <?=
                $this->Form->file('head_logo', ['class' => 'form-control form-group'])
                ?>

                <?= $this->Form->input(__("remove_header"), ['type' => 'checkbox','class'=>'selectall','value'=>1,'label'=> "সকল হেডার থাকবে না"]) ?>
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <ul class="list-group">
                            <li class="list-group-item">
                                <?= $this->Form->input(__("remove_header_logo"), ['type' => 'checkbox','value'=>1,'label'=> "লোগো / মনোগ্রাম থাকবে না"]) ?>
                            </li>
                            <li class="list-group-item">
                                <?= $this->Form->input(__("remove_header_left_slogan"), ['type' => 'checkbox','value'=>1,'label'=> "বাম শিরোনাম থাকবে না"]) ?>
                            </li>
                            <li class="list-group-item">
                                <?= $this->Form->input(__("remove_header_right_slogan"), ['type' => 'checkbox','value'=>1,'label'=> "ডান শিরোনাম থাকবে না"]) ?>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <ul class="list-group">
                            <li class="list-group-item">
                                <?= $this->Form->input(__("remove_header_head_1"), ['type' => 'checkbox','value'=>1,'label'=> "শিরোনাম ১ থাকবে না"]) ?>
                            </li>
                            <li class="list-group-item">
                                <?= $this->Form->input(__("remove_header_head_2"), ['type' => 'checkbox','value'=>1,'label'=> "শিরোনাম ২ থাকবে না"]) ?>
                            </li>
                            <li class="list-group-item">
                                <?= $this->Form->input(__("remove_header_head_3"), ['type' => 'checkbox','value'=>1,'label'=> "শিরোনাম ৩ থাকবে না"]) ?>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <ul class="list-group">
                            <li class="list-group-item">
                                <?= $this->Form->input(__("remove_header_unit"), ['type' => 'checkbox','value'=>1,'label'=> "শাখা থাকবে না"]) ?>
                            </li>
                            <li class="list-group-item">
                                <?= $this->Form->input(__("remove_header_head_4"), ['type' => 'checkbox','value'=>1,'label'=> "শিরোনাম ৪ থাকবে না"]) ?>
                            </li>
                            <li class="list-group-item">
                                <?= $this->Form->input(__("remove_header_head_5"), ['type' => 'checkbox','value'=>1,'label'=> "শিরোনাম ৫ থাকবে না"]) ?>
                            </li>
                        </ul>
                    </div>
                </div>

                <?= $this->Form->input(__("remove_image"), ['type' => 'hidden','value'=>0,'label'=>__("Banner") . " মুছে ফেলুন"]) ?>
                <?= $this->Form->button(__("Submit"), ['class' => 'btn btn-primary', 'type' => 'submit']) ?>
                <?= $this->Form->button(__("Banner") . " মুছে ফেলুন", ['class' => 'btn btn-danger btn-remove-header', 'type' => 'button']) ?>
                <?= $this->Form->end() ?>
            </div>
        </div>
        <hr/>
        <?php if(!empty($heading['header_photo'])): ?>
            <img src="<?= FILE_FOLDER . $heading['header_photo'] . '?token=' . sGenerateToken(['file'=>$heading['header_photo']],['exp'=>time() + 60*300])  ?>"
             style="max-height: 60px;width:auto;max-width: 100%"
             alt="Preview" />
        <?php endif; ?>
    </div>
</div>
<script>

    $('.btn-remove-header').click(function(){
    	$('[name=remove_image]').val(1)
        $('.headerSetting').submit()
    })

    $('.selectall').click(function () {
        if($(this).is(':checked')){
			$('.list-group-item [type=checkbox]').removeAttr('checked','checked')
        	$('.list-group-item [type=checkbox]').trigger('click')
        }else{
			$('.list-group-item [type=checkbox]').attr('checked','checked')
			$('.list-group-item [type=checkbox]').trigger('click')
        }
	})
</script>