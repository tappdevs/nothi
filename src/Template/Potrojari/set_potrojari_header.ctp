<!--<link rel="stylesheet" href="<?= $this->request->webroot ?>css/trumbowyg.min.css">-->
<link rel="stylesheet" href="<?= $this->request->webroot ?>css/trumbowyg.min.css">
<style>
    .htmlPreview{
        width: 8.2in;
    }
    .heading-style{
        background-color: gray;
        color: white;
        width: 8.2in;
        padding: 5px 10px;
        border-radius: 6px 6px 0 0;
    }
</style>
<?php
$head = !empty($heading['potrojari_head']) ? json_decode($heading['potrojari_head'],
    true) : [];
?>
<?= $this->Form->create($heading,
    ['class' => 'form-body headerSetting','url' =>['controller' => 'Potrojari','action' => 'setPotrojariHeader'],'id'=>'potrjari_heading_form'])
?>
<?php
$head_eng = !empty($heading['potrojari_head_eng']) ? json_decode($heading['potrojari_head_eng'],
    true) : [];
?>
<?php
$head_img = !empty($heading['potrojari_head_img']) ? json_decode($heading['potrojari_head_img'],
    true) : [];
?>
<div class="rwo">
    <div class="col-md-5" style="height: calc(100vh - 140px); overflow: auto; ">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i>পত্রজারি হেডিং
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>
                    <a href="#portlet-config" data-toggle="modal" class="config">
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="tabbable-custom ">
                    <ul class="nav nav-tabs ">
                        <li class="active">
                            <a href="#tab_1" data-toggle="tab">
                                লোগো / মনোগ্রাম </a>
                        </li>
                        <li>
                            <a href="#tab_2" data-toggle="tab">
                                বাম শিরোনাম </a>
                        </li>
                        <li>
                            <a href="#tab_3" data-toggle="tab">
                                ডান শিরোনাম </a>
                        </li>
                        <li>
                            <a href="#tab_4" data-toggle="tab">
                                অন্যান্য শিরোনাম </a>
                        </li>
                        <li>
                            <a href="#tab_5" data-toggle="tab">
                                হেডার ব্যানার </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <br>
                            <label class="control-label">লোগো / মনোগ্রামের ধরন নির্বাচন করুন</label>
                            <?= $this->Form->input('head_logo_type', array('options' => ['image'=>'ছবি','text'=>'লিখিত'], 'type' => 'select', 'label' => false, 'class' => 'form-control', 'placeholder' => 'ধরন ','default'=>(!empty($head['head_logo_type']) ? $head['head_logo_type']
                                : ''))); ?>
                            <br>
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="head_logo"><?= __("Logo") ?> (ছবি)</label>
                                    <?=
                                    $this->Form->hidden('head_logo_input',
                                        ['class' => 'form-control form-group', 'value' => (!empty($head_img['head_logo']) ? $head_img['head_logo']
                                            : '')])
                                    ?>
                                    <?=
                                    $this->Form->file('head_logo',
                                        ['class' => 'form-control form-group', 'onchange' => 'readURL(this);', 'value' => (!empty($head_img['head_logo'])
                                            ? $head_img['head_logo'] : ''), 'accept'=>'image/*'])
                                    ?>
                                </div>
                                <div class="col-md-6">
                                    <img class="logo-preview" height="80" width="80"
                                         src="<?= (!empty($head_img['head_logo']) ? $head_img['head_logo'] : '') ?>"/>
                                    <button type="button" class="logo-remove bnt btn-xs btn-danger  "><?= __("Delete") ?></button>
                                </div>
                                <div class="col-md-12">
                                    <?=
                                    $this->Form->input('head_text',
                                        ['class' => 'form-control form-group hide','type'=>'textarea', 'label' => __("Logo").' লিখিত (বাংলা)', 'value' => (!empty($head['head_text'])
                                            ? stripcslashes($head['head_text']) : '')])
                                    ?>
                                </div>
                                <div class="col-md-12">
                                    <?=
                                    $this->Form->input('head_text_eng',
                                        ['class' => 'form-control form-group hide','type'=>'textarea', 'label' => __("Logo").' লিখিত (ইংরেজি)', 'value' => (!empty($head_eng['head_text'])
                                            ? stripcslashes($head_eng['head_text']) : '')])
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_2">
                            <br>
                            <label class="control-label"><?=__("Left").' '.__("Title") ?> ধরন নির্বাচন করুন</label>
                            <?= $this->Form->input('head_left_type', array('options' => ['text'=>'লিখিত','image'=>'ছবি'], 'type' => 'select', 'label' => false, 'class' => 'form-control', 'placeholder' => 'ধরন ','default'=>(!empty($head['head_left_type']) ? $head['head_left_type']
                                : ''))); ?>
                            <br>
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="head_left_logo"><?=__("Left").' '.__("Title") ?> (ছবি)</label>
                                    <?=
                                    $this->Form->hidden('head_left_logo_input',
                                        ['class' => 'form-control form-group', 'value' => (!empty($head_img['head_left_logo']) ? $head_img['head_left_logo']
                                            : '')])
                                    ?>
                                    <?=
                                    $this->Form->file('head_left_logo',
                                        ['class' => 'form-control form-group', 'onchange' => 'readLeftSloganURL(this);', 'value' => (!empty($head_img['head_left_logo'])
                                            ? $head_img['head_left_logo'] : ''), 'accept'=>'image/*'])
                                    ?>
                                </div>
                                <div class="col-md-6">
                                    <img class="head-left-logo-preview" height="80" width="80"
                                         src="<?= (!empty($head_img['head_left_logo']) ? $head_img['head_left_logo'] : '') ?>"/>
                                    <button type="button" class="head-left-logo-remove bnt btn-xs btn-danger  "><?= __("Delete") ?></button>
                                </div>
                                <div class="col-md-12">
                                    <?=
                                    $this->Form->input('head_left_slogan',
                                        ['class' => 'form-control form-group hide','type'=>'textarea', 'label' => __("Left").' '.__("Title").' লিখিত  '.__("Bangla"), 'value' => (!empty($head['head_left_slogan'])
                                            ? stripcslashes($head['head_left_slogan']) : '')])
                                    ?>
                                </div>
                                <div class="col-md-12">
                                    <?=
                                    $this->Form->input('head_left_slogan_eng',
                                        ['class' => 'form-control form-group hide','type'=>'textarea', 'label' => __("Left").' '.__("Title").' লিখিত '.__("English"), 'value' => (!empty($head_eng['head_left_slogan'])
                                            ? stripcslashes($head_eng['head_left_slogan']) : '')])
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_3">
                            <br>
                            <label class="control-label"><?=  __("Right").' '.__("Title") ?> ধরন নির্বাচন করুন</label>
                            <?= $this->Form->input('head_right_type', array('options' => ['text'=>'লিখিত','image'=>'ছবি'], 'type' => 'select', 'label' => false, 'class' => 'form-control', 'placeholder' => 'ধরন ','default'=>(!empty($head['head_right_type']) ? $head['head_right_type']
                                : ''))); ?>
                            <br>
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="head_right_logo"><?=  __("Right").' '.__("Title") ?> (ছবি)</label>
                                    <?=
                                    $this->Form->hidden('head_right_logo_input',
                                        ['class' => 'form-control form-group', 'value' => (!empty($head_img['head_right_logo']) ? $head_img['head_right_logo']
                                            : '')])
                                    ?>
                                    <?=
                                    $this->Form->file('head_right_logo',
                                        ['class' => 'form-control form-group', 'onchange' => 'readRightSloganURL(this);', 'value' => (!empty($head_img['head_right_logo'])
                                            ? $head_img['head_right_logo'] : ''), 'accept'=>'image/*'])
                                    ?>
                                </div>
                                <div class="col-md-6">
                                    <img class="head-right-logo-preview" height="80" width="80"
                                         src="<?= (!empty($head_img['head_right_logo']) ? $head_img['head_right_logo'] : '') ?>"/>
                                    <button type="button" class="head-right-logo-remove bnt btn-xs btn-danger  "><?= __("Delete") ?></button>
                                </div>
                                <div class="col-md-12">
                                    <?=
                                    $this->Form->input('head_right_slogan',
                                        ['class' => 'form-control form-group hide','type'=>'textarea', 'label' => __("Right").' '.__("Title").' লিখিত '.__("Bangla"), 'value' => (!empty($head['head_right_slogan'])
                                            ? stripcslashes($head['head_right_slogan']) : '')])
                                    ?>
                                </div>
                                <div class="col-md-12">
                                    <?=
                                    $this->Form->input('head_right_slogan_eng',
                                        ['class' => 'form-control form-group hide','type'=>'textarea', 'label' => __("Right").' '.__("Title").' লিখিত '.__("English"), 'value' => (!empty($head_eng['head_right_slogan'])
                                            ? stripcslashes($head_eng['head_right_slogan']) : '')])
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_4">
                            <br>
                            <div class="row">
                                <div class="col-md-12">
                                    <?=
                                    $this->Form->input('head_title',
                                        ['class' => 'form-control form-group', 'label' => __("Title")." ১".' '.__("Bangla"), 'value' => (!empty($head['head_title'])
                                            ? $head['head_title'] : 'গণপ্রজাতন্ত্রী বাংলাদেশ সরকার')])
                                    ?>
                                </div>
                                <div class="col-md-12">
                                    <?=
                                    $this->Form->input('head_title_eng',
                                        ['class' => 'form-control form-group', 'label' => __("Title")." ১".' '.__("English"), 'value' => (!empty($head_eng['head_title'])
                                            ? $head_eng['head_title'] : 'Government of the People\'s Republic of Bangladesh')])
                                    ?>
                                </div>
                                <div class="col-md-12">
                                    <?=
                                    $this->Form->input('head_ministry',
                                        ['class' => 'form-control form-group', 'label' => __("Title")." ২".' '.__("Bangla")  . ' (' . __("Ministry") . ')', 'value' => (!empty($head['head_ministry'])
                                            ? $head['head_ministry'] : $employee_office['ministry_records'])])
                                    ?>
                                </div>
                                <div class="col-md-12">
                                    <?=
                                    $this->Form->input('head_ministry_eng',
                                        ['class' => 'form-control form-group', 'label' => __("Title")." ২".' '.__("English") . ' (' . __("Ministry") . ')', 'value' => (!empty($head_eng['head_ministry'])
                                            ? $head_eng['head_ministry'] : $employee_office['ministry_records'])])
                                    ?>
                                </div>
                                <div class="col-md-12">
                                    <?=
                                    $this->Form->input('head_office',
                                        ['class' => 'form-control form-group', 'label' => __("Title")." ৩".' '.__("Bangla") . ' (' . __("Office") . ')', 'value' => (!empty($head['head_office'])
                                            ? $head['head_office'] : $employee_office['office_name'])])
                                    ?>
                                </div>
                                <div class="col-md-12">
                                    <?=
                                    $this->Form->input('head_office_eng',
                                        ['class' => 'form-control form-group', 'label' => __("Title")." ৩".' '.__("English") . ' (' . __("Office") . ')', 'value' => (!empty($head_eng['head_office'])
                                            ? $head_eng['head_office'] : $employee_office['office_name'])])
                                    ?>
                                </div>
                                <div class="col-md-12">
                                    <?=  $this->Form->input('write_unit',
                                        ['type'=>'checkbox', 'class'=>'form-control form-group','hiddenField' => false, 'label' => __("Unit") . ' ' . __("Name") . ' ( সিস্টেম থেকে অটো আসবে )', 'checked' => (!empty($heading['write_unit'])
                                            ? 'checked' : false)]) ?>
                                </div>
                                <div class="col-md-12">
                                    <?=
                                    $this->Form->input('head_other',
                                        ['class' => 'form-control form-group', 'label' => __("Title")." ৪".' '.__("Bangla"), 'value' => (!empty($head['head_other'])
                                            ? $head['head_other'] : $employee_office['office_web'])])
                                    ?>
                                </div>
                                <div class="col-md-12">
                                    <?=
                                    $this->Form->input('head_other_eng',
                                        ['class' => 'form-control form-group', 'label' => __("Title")." ৪".' '.__("English"), 'value' => (!empty($head_eng['head_other'])
                                            ? $head_eng['head_other'] : $employee_office['office_web'])])
                                    ?>
                                </div>
                                <div class="col-md-12">
                                    <?=
                                    $this->Form->input('head_office_address',
                                        ['class' => 'form-control form-group', 'label' => __("Title")." ৫".' '.__("Bangla"), 'value' => (!empty($head['head_office_address'])
                                            ? $head['head_office_address'] : $employee_office['office_address'])])
                                    ?>
                                </div>
                                <div class="col-md-12">
                                    <?=
                                    $this->Form->input('head_office_address_eng',
                                        ['class' => 'form-control form-group', 'label' => __("Title")." ৫".' '.__("English"), 'value' => (!empty($head_eng['head_office_address'])
                                            ? $head_eng['head_office_address'] : $employee_office['office_address'])])
                                    ?>
                                </div>
                            </div>
                            <?= $this->Form->end() ?>
                        </div>
                        <div class="tab-pane" id="tab_5">
                            <br>
                            <?php if(defined("POTROJARI_UPDATE") && POTROJARI_UPDATE): ?>
                                <?= $this->Form->create($heading,
                                    ['class' => 'form-body headerSetting','url' =>['controller' => 'Potrojari','action' => 'setPotrojariDynamicHeader'],'type'=>'file'])
                                ?>

                                <label for="head_logo"><?= __("Banner") ?></label><span style="font-size: 10pt;"> (উচ্চতাঃ ৬০পিক্সেল, সাইজঃ সর্বোচ্চ ১এমবি)</span>

                                <?=
                            $this->Form->file('head_logo', ['class' => 'form-control form-group', 'accept'=>'image/*'])
                                ?>

                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <?= $this->Form->input(__("banner_position"), ['type' => 'select','class'=>'form-control','options'=>[
                                            'left'=>__("Left"),
                                            'right'=>__("Right"),
                                            'center'=>__("Center"),
                                        ],'label'=>__("Banner")  . ' ' . __("Position"),'value'=>(!empty($head['banner_position']) ? $head['banner_position'] : 0)]) ?>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <?= $this->Form->input(__("banner_width"), ['type' => 'select','class'=>'form-control','options'=>[
                                            'auto'=>__("Auto"),
                                            '100%'=>"১০০%"
                                        ],'label'=>__("Banner")  . ' ' . __("Width"),'value'=>(!empty($head['banner_width']) ? $head['banner_width'] : 0)]) ?>
                                    </div>
                                </div>
                                <hr/>
                                <?= $this->Form->input(__("remove_header"), ['type' => 'checkbox','class'=>'selectall','value'=>1,'label'=> "সকল হেডার থাকবে না"]) ?>
                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <ul class="list-group">
                                            <li class="list-group-item">
                                                <?= $this->Form->input(__("remove_header_logo"), ['type' => 'checkbox','value'=>1,'label'=> "লোগো/ মনোগ্রাম থাকবে না",'default'=>(!empty($head['remove_header_logo']) ? $head['remove_header_logo'] : 0)]) ?>
                                            </li>
                                            <li class="list-group-item">
                                                <?= $this->Form->input(__("remove_header_left_slogan"), ['type' => 'checkbox','value'=>1,'label'=> "বাম শিরোনাম থাকবে না",'default'=>(!empty($head['remove_header_left_slogan']) ? $head['remove_header_left_slogan'] : 0)]) ?>
                                            </li>
                                            <li class="list-group-item">
                                                <?= $this->Form->input(__("remove_header_right_slogan"), ['type' => 'checkbox','value'=>1,'label'=> "ডান শিরোনাম থাকবে না",'default'=>(!empty($head['remove_header_right_slogan']) ? $head['remove_header_right_slogan'] : 0)]) ?>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <ul class="list-group">
                                            <li class="list-group-item">
                                                <?= $this->Form->input(__("remove_header_head_1"), ['type' => 'checkbox','value'=>1,'label'=> "শিরোনাম ১ থাকবে না",'default'=>(!empty($head['remove_header_head_1']) ? $head['remove_header_head_1'] : 0)]) ?>
                                            </li>
                                            <li class="list-group-item">
                                                <?= $this->Form->input(__("remove_header_head_2"), ['type' => 'checkbox','value'=>1,'label'=> "শিরোনাম ২ থাকবে না",'default'=>(!empty($head['remove_header_head_2']) ? $head['remove_header_head_2'] : 0)]) ?>
                                            </li>
                                            <li class="list-group-item">
                                                <?= $this->Form->input(__("remove_header_head_3"), ['type' => 'checkbox','value'=>1,'label'=> "শিরোনাম ৩ থাকবে না",'default'=>(!empty($head['remove_header_head_3']) ? $head['remove_header_head_3'] : 0)]) ?>
                                            </li>
                                            <li class="list-group-item">
                                                <?= $this->Form->input(__("remove_header_head_4"), ['type' => 'checkbox','value'=>1,'label'=> "শিরোনাম ৪ থাকবে না",'default'=>(!empty($head['remove_header_head_4']) ? $head['remove_header_head_4'] : 0)]) ?>
                                            </li>
                                            <li class="list-group-item">
                                                <?= $this->Form->input(__("remove_header_head_5"), ['type' => 'checkbox','value'=>1,'label'=> "শিরোনাম ৫ থাকবে না",'default'=>(!empty($head['remove_header_head_5']) ? $head['remove_header_head_5'] : 0)]) ?>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                                <?= $this->Form->input(__("remove_image"), ['type' => 'hidden','value'=>0,'label'=>__("Banner") . " মুছে ফেলুন"]) ?>
                                <?= $this->Form->button(__("Banner")  ." ".__("Submit"), ['class' => 'btn btn-primary', 'type' => 'submit']) ?>
                                <?= $this->Form->button(__("Banner") . " মুছে ফেলুন", ['class' => 'btn btn-danger btn-remove-header', 'type' => 'button']) ?>
                                <?= $this->Form->end() ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <button class="btn btn-primary" id="potrjari_heading_submit_button">সংরক্ষণ</button>
            </div>
        </div>
    </div>
    <div class="col-md-7">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i> পত্রজারি হেডিং প্রিভিউ
                </div>
            </div>
            <div class="portlet-body" style="overflow: auto;">

                <h4 class="heading-style">বাংলা</h4>
                <div class="htmlPreview">
                    <?php if(!empty($heading['header_photo'])): ?>
                        <img src="<?= FILE_FOLDER . $heading['header_photo'] . '?token=' . sGenerateToken(['file'=>$heading['header_photo']],['exp'=>time() + 60*300])  ?>"
                             style="max-height: 60px;width:auto;max-width: 100%;text-align: left!important;"
                             alt="Preview" id="preview_banner" />
                    <?php endif; ?>
                    <div class=" text-center">
                        <?php
                        if (!empty($head_img['head_logo'])):
                            ?>
                            <img class="logo-preview" height="80" width="80" style="margin:5px;"
                                 src="<?= (!empty($head_img['head_logo']) ? $head_img['head_logo'] : '') ?>"/>
                        <?php endif; ?>
                        <a href="javascript:;" id="head_text" class="head_text" data-type="text"
                           data-pk="1"><?= (!empty($head['head_text']) ? stripcslashes($head['head_text'])
                                : '')
                            ?></a>
                        <div class="row">
                            <div class="col-md-3 col-sm-3 col-xs-3">
                                <img class="head-left-logo-preview" height="80" width="80" style="margin:5px;"
                                     src="<?= (!empty($head_img['head_left_logo']) ? $head_img['head_left_logo'] : '') ?>"/>
                                <a href="javascript:;" id="left_slogan" data-type="text"
                                   data-pk="1"><?= (!empty($head['head_left_slogan']) ? stripcslashes($head['head_left_slogan'])
                                        : '')
                                    ?></a>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <a href="javascript:;" id="gov_name" data-type="text" data-pk="1">
                                    <strong><?= (!empty($head['head_title']) ? $head['head_title'] : 'গণপ্রজাতন্ত্রী বাংলাদেশ সরকার') ?></strong>
                                </a>
                                <br/>
                                <a href="javascript:;" id="office_ministry" data-type="text"
                                   data-pk="1"><?= (!empty($head['head_ministry']) ? $head['head_ministry']
                                        : $employee_office['ministry_records'])
                                    ?></a>
                                <br/> <a href="javascript:;" s id="offices" data-type="text"
                                         data-pk="1"><?= (!empty($head['head_office']) ? $head['head_office']
                                        : $employee_office['office_name'])
                                    ?></a>
                                <br/> <a href="javascript:;" id="unit_name" data-type="text"
                                         data-pk="1"><?= 'প্রেরকের শাখা' ?></a>
                                <br/><a href="javascript:;" id="web_url" data-type="text" data-pk="1"
                                        data-original-title="অফিসের ওয়েবসাইট -এর ঠিকানা লিখুন"><?=
                                    (!empty($head['head_other']) ? $head['head_other'] : (!empty($employee_office['office_web']) ? $employee_office['office_web']
                                        : '...'))
                                    ?></a>
                                <br/><a href="javascript:;" id="office_address" data-type="text" data-pk="1"
                                        data-original-title="অফিসের ঠিকানা লিখুন"><?=
                                    (!empty($head['head_office_address']) ? $head['head_office_address'] : (!empty($employee_office['office_address'])
                                        ? $employee_office['office_address'] : '...'))
                                    ?></a>

                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-3">
                                <img class="head-right-logo-preview" height="80" width="80" style="margin:5px;"
                                     src="<?= (!empty($head_img['head_right_logo']) ? $head_img['head_right_logo'] : '') ?>"/>
                                <a href="javascript:;" id="right_slogan" data-type="text"
                                   data-pk="1"><?= (!empty($head['head_right_slogan']) ? $head['head_right_slogan']
                                        : '')
                                    ?></a>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <h4 class="heading-style">English</h4>
                <div class="htmlPreview">
                    <?php if(!empty($heading['header_photo'])): ?>
                        <img src="<?= FILE_FOLDER . $heading['header_photo'] . '?token=' . sGenerateToken(['file'=>$heading['header_photo']],['exp'=>time() + 60*300])  ?>"
                             style="max-height: 60px;width:auto;max-width: 100%;text-align: left!important;"
                             alt="Preview" id="preview_banner_eng" />
                    <?php endif; ?>
                    <div class="text-center">
                        <?php
                        if (!empty($head_img['head_logo'])):
                            ?>
                            <img class="logo-preview" height="80" width="80" style="margin:5px;"
                                 src="<?= (!empty($head_img['head_logo']) ? $head_img['head_logo'] : '') ?>"/>
                        <?php endif; ?>
                        <a href="javascript:;" id="head_text_eng" class="head_text" data-type="text"
                           data-pk="1"><?= (!empty($head_eng['head_text']) ? stripcslashes($head_eng['head_text'])
                                : '')
                            ?></a>
                        <div class="row">
                            <div class="col-md-3 col-sm-3 col-xs-3">
                                <img class="head-left-logo-preview" height="80" width="80" style="margin:5px;"
                                     src="<?= (!empty($head_img['head_left_logo']) ? $head_img['head_left_logo'] : '') ?>"/>
                                <a href="javascript:;" id="left_slogan_eng" data-type="text"
                                   data-pk="1"><?= (!empty($head_eng['head_left_slogan']) ? stripcslashes($head_eng['head_left_slogan'])
                                        : '')
                                    ?></a>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <a href="javascript:;" id="gov_name_eng" data-type="text" data-pk="1">
                                    <strong><?= (!empty($head_eng['head_title']) ? $head_eng['head_title'] : 'Government of the People\'s Republic of Bangladesh') ?></strong>
                                </a>
                                <br/>
                                <a href="javascript:;" id="office_ministry_eng" data-type="text"
                                   data-pk="1"><?= (!empty($head_eng['head_ministry']) ? $head_eng['head_ministry']
                                        : $employee_office['ministry_records'])
                                    ?></a>
                                <br/> <a href="javascript:;" id="offices_eng" data-type="text"
                                         data-pk="1"><?= (!empty($head_eng['head_office']) ? $head_eng['head_office']
                                        : $employee_office['office_name'])
                                    ?></a>
                                <br/> <a href="javascript:;" id="unit_name_eng" data-type="text"
                                         data-pk="1"><?=  'Sender\'s branch' ?></a>
                                <br/><a href="javascript:;" id="web_url_eng" data-type="text" data-pk="1"
                                        data-original-title="Enter the website address of the office"><?=
                                    (!empty($head_eng['head_other']) ? $head_eng['head_other'] : (!empty($employee_office['office_web']) ? $employee_office['office_web']
                                        : '...'))
                                    ?></a>
                                <br/><a href="javascript:;" id="office_address_eng" data-type="text" data-pk="1"
                                        data-original-title="Enter the office address"><?=
                                    (!empty($head_eng['head_office_address']) ? $head_eng['head_office_address'] : (!empty($employee_office['office_address_eng'])
                                        ? $employee_office['office_address'] : '...'))
                                    ?></a>

                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-3">
                                <img class="head-right-logo-preview" height="80" width="80" style="margin:5px;"
                                     src="<?= (!empty($head_img['head_right_logo']) ? $head_img['head_right_logo'] : '') ?>"/>
                                <a href="javascript:;" id="right_slogan_eng" data-type="text"
                                   data-pk="1"><?= (!empty($head_eng['head_right_slogan']) ? stripcslashes($head_eng['head_right_slogan'])
                                        : '')
                                    ?></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--<script src="<?= $this->request->webroot ?>js/tinymce/tinymce.min.js"></script>-->
<script src="<?= $this->request->webroot ?>js/trumbowyg.min.js"></script>

<script>

    $(function(){
        $('textarea').trumbowyg({
            btns: [
                ['undo', 'redo'],['strong', 'em', 'del']
            ],
            autogrow: true
        });

		// tinymce.init({
		// 	selector: "textarea",
		// 	height: 100,
		// 	mobile: { theme: 'mobile' },
        //     toolbar: "bold italic underline | alignleft aligncenter alignright alignjustify | fontselect fontsizeselect outdent indent blockquote | removeformat",
		// 	menubar: false,
		// 	fontsize_formats: '9pt 11pt 13pt 15pt',
		// 	font_formats: 'Nikosh="Nikosh, SolaimanLipi, sans-serif"',
		// 	toolbar_items_size: 'small',
		// 	setup : function(ed) {
		// 		ed.on('change', function(e) {
		// 			getChange()
		// 		});
		// 	},
        //
		// 	init_instance_callback: function () {
		// 		window.setTimeout(function() {
		// 			$(".textarea").show();
		// 		}, 1000);
		// 	}
		// });

        $('body').addClass('page-sidebar-closed');
        $('.page-sidebar-menu').addClass('page-sidebar-menu-closed');
        senderOffice();
        window.setTimeout(function() {
            getChange();
        }, 1300);
        $('select').change(function(){
            getChange ();
        });

    });

    $('.headerSetting input').change(getChange);

	function getChange () {
        // if(!($("#remove-banner").is(':checked'))) {
        //
        //     $("#preview_banner").removeClass('hide');
        //     if($("#preview_banner").next('br').length == 0){
        //         $("#preview_banner").after("<br>");
        //     }
        //     $("#preview_banner_eng").removeClass('hide');
        //     if($("#preview_banner_eng").next('br').length == 0){
        //         $("#preview_banner_eng").after("<br>");
        //     }
        // } else {
        //     $("#preview_banner").next("br").remove();
        //     $("#preview_banner").addClass('hide');
        //     $("#preview_banner_eng").next("br").remove();
        //     $("#preview_banner_eng").addClass('hide');
        // }

	    if(!($("#remove-header-logo").is(':checked'))) {
            if ($("#head-logo-type").val() == "image") {
                $(".head_text").addClass('hide');
                if (isEmpty($('.logo-preview').eq(0).attr('src'))) {
                    $('.logo-preview').addClass('hide');
                    $('.logo-preview:first').removeClass('hide');
                } else {
                    $('.logo-preview').removeClass('hide');
                }
            } else {
                $('.logo-preview').addClass('hide');
                $('.logo-preview:first').removeClass('hide');
                $(".head_text").removeClass('hide');

                $('#head_text').html(isEmpty($('#head-text').trumbowyg('html')) ? '...' : $('#head-text').trumbowyg('html'));
                $('#head_text_eng').html(isEmpty($('#head-text-eng').trumbowyg('html')) ? '...' : $('#head-text-eng').trumbowyg('html'));

            }
        } else {
            $('.logo-preview').addClass('hide');
            $('.logo-preview:first').removeClass('hide');
            $(".head_text").addClass('hide');
        }

        if(!($("#remove-header-left-slogan").is(':checked'))) {
            if ($("#head-left-type").val() == "image") {
                $("#left_slogan").addClass('hide');
                $("#left_slogan_eng").addClass('hide');
                if (isEmpty($('.head-left-logo-preview').eq(0).attr('src'))) {
                    $('.head-left-logo-preview').addClass('hide');
                    $('.head-left-logo-preview:first').removeClass('hide');
                } else {
                    $('.head-left-logo-preview').removeClass('hide');
                }
            } else {
                $('.head-left-logo-preview').addClass('hide');
                $('.head-left-logo-preview:first').removeClass('hide');
                $("#left_slogan").removeClass('hide');
                $("#left_slogan_eng").removeClass('hide');

                $('#left_slogan').html(isEmpty($('#head-left-slogan').trumbowyg('html')) ? '...' : $('#head-left-slogan').trumbowyg('html'));
                $('#left_slogan_eng').html(isEmpty($('#head-left-slogan-eng').trumbowyg('html')) ? '...' : $('#head-left-slogan-eng').trumbowyg('html'));

            }
        } else {
            $('.head-left-logo-preview').addClass('hide');
            $('.head-left-logo-preview:first').removeClass('hide');
            $("#left_slogan").addClass('hide');
            $("#left_slogan_eng").addClass('hide');
        }


        if(!($("#remove-header-right-slogan").is(':checked'))) {
            if ($("#head-right-type").val() == "image") {
                $("#right_slogan").addClass('hide');
                $("#right_slogan_eng").addClass('hide');
                if (isEmpty($('.head-right-logo-preview').eq(0).attr('src'))) {
                    $('.head-right-logo-preview').addClass('hide');
                    $('.head-right-logo-preview:first').removeClass('hide');
                } else {
                    $('.head-right-logo-preview').removeClass('hide');
                }
            } else {
                $('.head-right-logo-preview').addClass('hide');
                $('.head-right-logo-preview:first').removeClass('hide');
                $("#right_slogan").removeClass('hide');
                $("#right_slogan_eng").removeClass('hide');

                $('#right_slogan').html(isEmpty($('#head-right-slogan').trumbowyg('html')) ? '...' : $('#head-right-slogan').trumbowyg('html'));
                $('#right_slogan_eng').html(isEmpty($('#head-right-slogan-eng').trumbowyg('html')) ? '...' : $('#head-right-slogan-eng').trumbowyg('html'));

            }
        } else {
            $('.head-right-logo-preview').addClass('hide');
            $('.head-right-logo-preview:first').removeClass('hide');
            $("#right_slogan").addClass('hide');
            $("#right_slogan_eng").addClass('hide');
        }

        $('#gov_name').text($('#head-title').val() == '' ? '' : $('#head-title').val());
        $('#gov_name_eng').text($('#head-title-eng').val() == '' ? '' : $('#head-title-eng').val());

        if(!($("#remove-header-head-1").is(':checked'))) {
            if ($('#head-title').val() == '...' || isEmpty($('#head-title').val())) {

                $('#gov_name').addClass('hide');
                $("#gov_name").next("br").remove();

            } else {
                $('#gov_name').removeClass('hide');

                if($("#gov_name").next('br').length == 0){
                    $("#gov_name").after("<br>");
                }
            }
            if ($('#head-title-eng').val() == '...' ||isEmpty($('#head-title-eng').val())) {
                $('#gov_name_eng').addClass('hide');
                $("#gov_name_eng").next("br").remove();
            } else {
                $('#gov_name_eng').removeClass('hide');
                if($("#gov_name_eng").next('br').length == 0){
                    $("#gov_name_eng").after("<br>");
                }
            }
        } else {
            $('#gov_name').addClass('hide');
            $('#gov_name_eng').addClass('hide');
            $("#gov_name").next("br").remove();
            $("#gov_name_eng").next("br").remove();
        }

        $('#office_ministry').text($('#head-ministry').val() == '' ? '' : $('#head-ministry').val());
        $('#office_ministry_eng').text($('#head-ministry-eng').val() == '' ? '' : $('#head-ministry-eng').val());

        if(!($("#remove-header-head-2").is(':checked'))) {
            if ($('#head-ministry').val() == '...' || isEmpty($('#head-ministry').val())) {

                $('#office_ministry').addClass('hide');
                $("#office_ministry").next("br").remove();

            } else {
                $('#office_ministry').removeClass('hide');

                if($("#office_ministry").next('br').length == 0){
                    $("#office_ministry").after("<br>");
                }
            }
            if ($('#head-ministry-eng').val() == '...' ||isEmpty($('#head-ministry-eng').val())) {
                $('#office_ministry_eng').addClass('hide');
                $("#office_ministry_eng").next("br").remove();
            } else {
                $('#office_ministry_eng').removeClass('hide');
                if($("#office_ministry_eng").next('br').length == 0){
                    $("#office_ministry_eng").after("<br>");
                }
            }
        } else {
            $('#office_ministry').addClass('hide');
            $('#office_ministry_eng').addClass('hide');
            $("#office_ministry").next("br").remove();
            $("#office_ministry_eng").next("br").remove();
        }

        $('#offices').text($('#head-office').val() == '' ? '' : $('#head-office').val());
        $('#offices_eng').text($('#head-office-eng').val() == '' ? '' : $('#head-office-eng').val());


        if(!($("#remove-header-head-3").is(':checked'))) {
            if ($('#head-office').val() == '...' || isEmpty($('#head-office').val())) {

                $('#offices').addClass('hide');
                $("#offices").next("br").remove();

            } else {
                $('#offices').removeClass('hide');

                if($("#offices").next('br').length == 0){
                    $("#offices").after("<br>");
                }
            }
            if ($('#head-office-eng').val() == '...' ||isEmpty($('#head-office-eng').val())) {
                $('#offices_eng').addClass('hide');
                $("#offices_eng").next("br").remove();
            } else {
                $('#offices_eng').removeClass('hide');
                if($("#offices_eng").next('br').length == 0){
                    $("#offices_eng").after("<br>");
                }
            }
        } else {
            $('#offices').addClass('hide');
            $('#offices_eng').addClass('hide');
            $("#offices").next("br").remove();
            $("#offices_eng").next("br").remove();
        }

        $('#web_url').text($('#head-other').val() == '' ? '...' : $('#head-other').val());
        $('#web_url_eng').text($('#head-other-eng').val() == '' ? '...' : $('#head-other-eng').val());

        if(!($("#remove-header-head-4").is(':checked'))) {
            if ($('#head-other').val() == '...' || isEmpty($('#head-other').val())) {

                $('#web_url').addClass('hide');
                $("#web_url").next("br").remove();

            } else {
                $('#web_url').removeClass('hide');

                if($("#web_url").next('br').length == 0){
                    $("#web_url").after("<br>");
                }
            }
            if ($('#head-other-eng').val() == '...' ||isEmpty($('#head-other-eng').val())) {
                $('#web_url_eng').addClass('hide');
                $("#web_url_eng").next("br").remove();
            } else {
                $('#web_url_eng').removeClass('hide');
                if($("#web_url_eng").next('br').length == 0){
                    $("#web_url_eng").after("<br>");
                }
            }
        } else {
            $('#web_url').addClass('hide');
            $('#web_url_eng').addClass('hide');
            $("#web_url").next("br").remove();
            $("#web_url_eng").next("br").remove();
        }

        $('#office_address').text($('#head-office-address').val() == '' ? '...' : $('#head-office-address').val());
        $('#office_address_eng').text($('#head-office-address-eng').val() == '' ? '...' : $('#head-office-address-eng').val());

        if(!($("#remove-header-head-5").is(':checked'))) {
            if ($('#head-office-address').val() == '...' || isEmpty($('#head-office-address').val())) {

                $('#office_address').addClass('hide');

            } else {
                $('#office_address').removeClass('hide');
            }
            if ($('#head-office-address-eng').val() == '...' ||isEmpty($('#head-office-address-eng').val())) {
                $('#office_address_eng').addClass('hide');
            } else {
                $('#office_address_eng').removeClass('hide');
            }
        } else {
            $('#office_address').addClass('hide');
            $('#office_address_eng').addClass('hide');
        }
	}

    function senderOffice(){
        if ($('#write-unit').is(":checked")) {
            $('#unit_name').removeClass('hide');
            $('#unit_name_eng').removeClass('hide');
            $('#unit_name').after("<br />");
            $('#unit_name_eng').after("<br />");
        } else {
            $('#unit_name').addClass('hide');
            $('#unit_name_eng').addClass('hide');
            $('#unit_name').next().remove();
            $('#unit_name_eng').next().remove();
        }
    }
    $('#write-unit').change(function (){
        senderOffice();
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.logo-preview').attr('src', e.target.result);
                $('input[name=head_logo_input]').val(e.target.result);
                getChange();
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
    function readLeftSloganURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.head-left-logo-preview').attr('src', e.target.result);
                $('input[name=head_left_logo_input]').val(e.target.result);
                getChange();
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
    function readRightSloganURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.head-right-logo-preview').attr('src', e.target.result);
                $('input[name=head_right_logo_input]').val(e.target.result);
                getChange();
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $('.logo-remove').click(function () {
        $('input[name=head_logo_input]').val('');
        $('.logo-preview').removeAttr('src');
        $('.logo-preview').addClass('hide');
        $('.logo-preview:first').removeClass('hide');
    });
    $('.head-left-logo-remove').click(function () {
        $('input[name=head_left_logo_input]').val('');
        $('.head-left-logo-preview').removeAttr('src');
        $('.head-left-logo-preview').addClass('hide');
        $('.head-left-logo-preview:first').removeClass('hide');
    });
    $('.head-right-logo-remove').click(function () {
        $('input[name=head_right_logo_input]').val('');
        $('.head-right-logo-preview').removeAttr('src');
        $('.head-right-logo-preview').addClass('hide');
        $('.head-right-logo-preview:first').removeClass('hide');
    });

	$('.btn-remove-header').click(function(){
		$('[name=remove_image]').val(1);
		$('.headerSetting').submit()
	})

	$('.selectall').click(function () {
		if($(this).is(':checked')){
			$('.list-group-item [type=checkbox]').removeAttr('checked','checked')
			$('.list-group-item [type=checkbox]').trigger('click')
		}else{
			$('.list-group-item [type=checkbox]').attr('checked','checked')
			$('.list-group-item [type=checkbox]').trigger('click')
		}
	});

	$("#potrjari_heading_submit_button").click(function(){
	    $("#potrjari_heading_form").submit();
    });

    jQuery.ajaxSetup({
        cache: true
    });
</script>