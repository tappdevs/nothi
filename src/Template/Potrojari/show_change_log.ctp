<link href="<?php echo CDN_PATH; ?>assets/admin/pages/css/timeline.css" rel="stylesheet" type="text/css"/>
<style>
    .timeline-body-content{
        font-size:12px;
        max-width: 835px;
    }
    .timeline-body-alerttitle{
        font-size:13px;
    }
    
    ins {
        background-color: #c6ffc6;
        text-decoration: none;
    }

    del {
        background-color: #ffc6c6;
    }
    .showdetail{
        cursor: pointer;
    }
    
    
    #sovapoti_signature, #sender_signature, #sender_signature2, #sender_signature3{
        visibility: hidden;
    }
    .A4-draft {
        background: white;
        width: 21cm !important;
        min-height: 29.7cm !important;
        display: block;
        margin: 0 auto;
        padding-left: 0.75in;
        padding-right: 0.75in;
        padding-top: 1in;
        padding-bottom: 0.75in;
        margin-bottom: 0.5cm;
        box-shadow: 0 0 0.5cm rgba(0, 0, 0, 0.5);
        overflow-y: auto;
        box-sizing: border-box;
        font-size: 12pt;
    }
    
</style>

<?php if(!empty($allDrafts)): ?>
<?php foreach($allDrafts as $key=>$draftversion): ?>
<div class="timeline">
    <div class="timeline-item">
        <div class="timeline-badge">
            <div class="timeline-icon">
                <i class=" a2i_gn_user1 font-green-soft"></i>
            </div>
        </div>
        <div class="timeline-body">
            <div class="timeline-body-arrow">
            </div>
            <div class="timeline-body-head">
                <div class="timeline-body-head-caption">
                    <span class="timeline-body-alerttitle font-green-haze"><?php echo h($draftversion['officer_name']) . ', ' . $draftversion['designation_label']; ?></span>
                    <span class="timeline-body-time font-grey-cascade"><?php echo $draftversion['modified'] ?></span>
                </div>
                <div class="pull-right">
                    <span class="glyphicon glyphicon-chevron-down showdetail"></span>
                </div>
            </div>
            <div class="timeline-body-content">
                <div class="origina-cont" style="display: none;">
                    <div class="A4-draft">
                        <?php echo $draftversion['updated_content'] ?>
                    </div>
                </div>
               
            </div>
        </div>
    </div>
</div>
<?php endforeach; ?>

<script>
    $(document).on('click','.showdetail', function(){
        $(this).removeClass('glyphicon-chevron-down').removeClass('showdetail').addClass('glyphicon-chevron-up').addClass('hidedetail');
        $(this).closest('.timeline-body').find('.timeline-body-content').find('.origina-cont').show();
    });
    $(document).on('click','.hidedetail', function(){
        $(this).removeClass('glyphicon-chevron-up').removeClass('hidedetail').addClass('glyphicon-chevron-down').addClass('showdetail');
        $(this).closest('.timeline-body').find('.timeline-body-content').find('.origina-cont').hide();
    });
</script>
<?php endif; ?>