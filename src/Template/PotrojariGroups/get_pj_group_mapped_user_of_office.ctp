<div class="portlet light ">
    <div class="portlet-title">
        <div class="caption"><i class="fs0 a2i_ld_namjarikarjakrom2"></i>  অফিসভিত্তিক - পত্রজারি গ্রুপ অন্তর্ভুক্ত ব্যক্তিবর্গ </div>
    </div>
    <div class="portlet-body">
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                  <th></th>
                  <th> কর্মকর্তার নাম </th>
                  <th> শাখার নাম </th>
                  <th> অফিসের নাম </th>
                </tr>
            </thead>
            <tbody>
            <?php
                if(!empty($allUserOfThisOffice)){
                    foreach ($allUserOfThisOffice as $val){
                        $isChecked = false;
                    	if (in_array($val['office_unit_organogram_id'], $allMappedUserInGroup)) {
                    		$isChecked = true;
	                    }
                        ?>
                        <tr>
                            <td><div class="form form-group" style="margin: 0 !important;">
                                    <input type="checkbox" value="<?=$val['office_unit_organogram_id']?>" class="checker" <?= ($isChecked) ? 'checked' : '' ?> class="form-control">
                                </div></td>
                            <td><?= $val['name_bng'] ?></td>
                            <td><?= (isset($val['designation'])?($val['designation']):'') ?></td>
                            <td><?= (isset($val['unit_name_bng'])?($val['unit_name_bng']):'') ?></td>
                        </tr>
                        <?php
                    }
                }
            ?>

            </tbody>
        </table>
    </div>
</div>