<style>
    .portlet-body > .form-horizontal > label{
        font-size: 11pt!important;
    }
    .form-horizontal .checkbox, .form-horizontal .checkbox-inline, .form-horizontal .radio, .form-horizontal .radio-inline{
        padding-top:2px!important;
    }
    .deleterow,.editrow { cursor: pointer; }
    .deleterow:hover,.editrow:hover {
        background-color: #3379b5;
        color: white;
    }
    .disabled-edit{
        width: 30px;
        background: #eaeaea;
        cursor: not-allowed;
        color: #c1c1c1;
    }
</style>
<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
            পত্রজারি গ্রুপ  সংশোধন
        </div>
        <div class="tools">
            <a  href="<?=
            $this->Url->build(['controller' => 'potrojariGroups',
                    'action' => 'groupMail'])
            ?>"><button class="btn   blue margin-bottom-5"  style="margin-top: -5px;"> পত্রজারি গ্রুপ   তালিকা  </button></a>
        </div>
    </div>
    <div class="portlet-body">
        <div class="row">
            <div class="col-md-12 col-sm-12 form-group form-horizontal">
                <label class="control-label col-md-2 col-sm-2 font-lg">গ্রুপের নামঃ </label>
                <div class=" col-md-9 col-sm-9">
                    <?php
                echo $this->Form->input('group_name',
                    array(
                    'label' => false,
                    'class' => 'form-control',
                    'empty' => '----',
                    'default' => ($potrojari_groups_info['group_name'])
                ));
                ?>
                </div>
                
            </div>
<!--            <div class="col-md-6 col-sm-6 form-group form-horizontal margin-top-20 pull-right" style="font-size: 12pt!important;">
                 <label class="control-label">গ্রুপের ধরনঃ </label>
                <?php
//                echo $this->Form->radio('group_type',
//                    array(
//                   array ('value' => 'public', 'text' => 'পাবলিক','label' => false,  'class' => 'form-control', 'style' => "font-size: 12pt!important;",'checked' => ($potrojari_groups_info['privacy_type'] == 'public')?'checked':''),
//                   array ('value' => 'private', 'text' => 'প্রাইভেট','label' => false,'class' => 'form-control', 'style' => "font-size: 12pt!important;",'checked' => ($potrojari_groups_info['privacy_type'] == 'private')?'checked':''),
//                ));
                ?>
            </div>-->
        </div>
          <div class="tabbable-custom ">
            <ul class="nav nav-tabs nav-justified">
                <!--            <li class="active">
                                <a href="#typeAhead_search" data-toggle="tab" aria-expanded="false">
                                    <span class ="font-purple">  অফিসার খুঁজুন  </span>
                                </a>
                            </li>-->
                <li class="active">
                    <a href="#default" id="officer-select" data-toggle="tab" aria-expanded="false">
                        <span class ="font-purple">  অফিসার বাছাই করুন </span>
                    </a>
                </li>
                <li class="">
                    <a href="#new_email" data-toggle="tab" id="officer-new" aria-expanded="false">
                        <span class ="font-purple">  নতুন অফিসার </span>
                    </a>
                </li>

            </ul>
            <div class="tab-content">
                <!--            <div class="tab-pane fade active in" id="typeAhead_search">
                            </div>-->
                <div class="tab-pane active" id="default">
                    <?= $this->Cell('OfficeSelection'); ?>
                    <div class="row">
                        <div class="col-md-4 form-group form-horizontal">
                            <label class="control-label"> <?php echo __("Office") ?> </label>
                            <?php
                            echo $this->Form->input('office_id',
                                array(
                                'label' => false,
                                'class' => 'form-control',
                                'empty' => '----'
                            ));
                            ?>
                        </div>
                        <div class="col-md-4 form-group form-horizontal">
                            <label class="control-label"> <?php echo __("Unit") ?> </label>
                            <?php
                            echo $this->Form->input('office_unit_id',
                                array(
                                'label' => false,
                                'class' => 'form-control',
                                'empty' => '----'
                            ));
                            ?>
                        </div>
                        <?php if(!empty($user_info['office_id'])){ ?>
                        <div class="col-md-4 form-group form-horizontal">
		                    <label class="control-label" >  </label>
							<?php
							echo $this->Form->input('own_office',
								array(
									'type' => 'checkbox',
									'label' => 'নিজ অফিস',
									'class' => 'form-control',
									'empty' => '----'
								));
							?>
	                    </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="tab-pane" id="new_email">
                     <div class="row">
                         <div class="col-md-4 form-group form-horizontal">
                            <label class="control-label"> নাম (বাংলায়)</label>
                            <?php
                            echo $this->Form->input('officer_name',
                                array(
                                'label' => false,
                                'class' => 'form-control',
                                'empty' => '----'
                            ));
                            ?>
                        </div>
                         <div class="col-md-4 form-group form-horizontal">
                             <label class="control-label"> নাম (ইংরেজিতে)</label>
                             <?php
                             echo $this->Form->input('officer_name_eng',
                                 array(
                                     'label' => false,
                                     'class' => 'form-control',
                                     'empty' => '----'
                                 ));
                             ?>
                         </div>
                        <div class="col-md-4 form-group form-horizontal">
                            <label class="control-label">ইমেইল <span class="required" >*</span>
                            </label>
                            <?php
                            echo $this->Form->input('employee_personal_email',
                                array(
                                'label' => false,
                                'class' => 'form-control',
                                'empty' => '----',
                                'type' => 'email'
                            ));
                            ?>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-md-4 form-group form-horizontal">
                            <label class="control-label">পদ (বাংলায়) <span class="required" >*</span></label>
                            <?php
                            echo $this->Form->input('officer_designation',
                                array(
                                'label' => false,
                                'class' => 'form-control',
                                'empty' => '----'
                            ));
                            ?>
                        </div>
                        <div class="col-md-4 form-group form-horizontal">
                            <label class="control-label">পদ (ইংরেজিতে)</label>
                            <?php
                            echo $this->Form->input('officer_designation_eng',
                                array(
                                    'label' => false,
                                    'class' => 'form-control',
                                    'empty' => '----'
                                ));
                            ?>
                        </div>
                        <div class="col-md-4 form-group form-horizontal">
                            <label class="control-label">মোবাইল নাম্বার
                            </label>
                            <?php
                            echo $this->Form->input('employee_personal_mobile',
                                array(
                                    'label' => false,
                                    'class' => 'form-control',
                                    'empty' => '----',
                                ));
                            ?>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-md-4 form-group form-horizontal">
                            <label class="control-label"><?php echo __("Unit")." (বাংলায়)" ?>  </label>
                            <?php
                            echo $this->Form->input('officer_unit',
                                array(
                                    'label' => false,
                                    'class' => 'form-control',
                                    'empty' => '----'
                                ));
                            ?>
                        </div>
                        <div class="col-md-4 form-group form-horizontal">
                            <label class="control-label"><?php echo __("Unit")." (ইংরেজিতে)" ?>  </label>
                            <?php
                            echo $this->Form->input('officer_unit_eng',
                                array(
                                    'label' => false,
                                    'class' => 'form-control',
                                    'empty' => '----'
                                ));
                            ?>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-md-4 form-group form-horizontal">
                            <label class="control-label">কার্যালয় (বাংলায়)  <span class="required" >*</span></label>
                            <?php
                            echo $this->Form->input('officer_office',
                                array(
                                    'label' => false,
                                    'class' => 'form-control',
                                    'empty' => '----'
                                ));
                            ?>
                        </div>
                        <div class="col-md-4 form-group form-horizontal">
                            <label class="control-label">কার্যালয় (ইংরেজিতে)</label>
                            <?php
                            echo $this->Form->input('officer_office_eng',
                                array(
                                    'label' => false,
                                    'class' => 'form-control',
                                    'empty' => '----'
                                ));
                            ?>
                        </div>
                    </div>
                    <div class="row text-center">
                         <button onclick="new_user();" type="button" class="btn btn-md bg-green "><i class="fa fa-check" aria-hidden="true"></i></button>
                    </div>

                </div>
            </div>
        </div>
        <hr>
        <div class="row" >
            <div class="col-md-6" style="text-align: center;">

                <div class="panel panel-success">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-12" style="text-align: center;">
                                গ্রুপে অন্তর্ভুক্ত ব্যক্তিবর্গ  <label class="badge badge-primary totalgroupmember"><?php if(!empty($potrojari_groups_info['group_value'])){
//                                     $data = json_decode($potrojari_groups_info['group_value'], true);
                                     echo $this->Number->format($potrojari_groups_info['total_users']);
                                }?></label>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body" >
                        <table class="table table-bordered">
                            <tbody id="addotherpermission">
                                <?php
//                                if(!empty($potrojari_groups_info['group_value'])){
                                if(!empty($potrojari_groups_users_info)){
//                                     $data = json_decode($potrojari_groups_info['group_value'], true);
                                        foreach ($potrojari_groups_users_info as $v) {
                                            if (!empty($v['officer_email'])) {
                                                        echo '<tr><td title="মুছুন" class="deleterow" style="width: 30px;text-align: center;"><i class="fa fa-times"></i><input type="hidden" class="group_mail_permission" name="office-employee" data-office-name="'.h($v['office_name_bng']).'" data-office-name-eng="'.h($v['office_name_eng']).'" data-office-id="'.$v['office_id'].'" data-office-employee-name="'.h($v['employee_name_bng']).'" data-office-employee-name-eng="'.h($v['employee_name_eng']).'" data-office-employee-id="'.$v['employee_id'].'"'.' data-office-unit-id="'.$v['office_unit_id'].'" data-office-unit-organogram-id="'.$v['office_unit_organogram_id'].'" '.'data-designation-name="'.h($v['office_unit_organogram_name_bng']).'" data-designation-name-eng="'.h($v['office_unit_organogram_name_eng']).'" data-unit-name="'.h($v['office_unit_name_bng']).'" data-unit-name-eng="'.h($v['office_unit_name_eng']).'" data-office-head="'.$v['office_head'].'" data-employee-personal-email="'.h($v['officer_email']).'" data-employee-personal-mobile="'.h($v['officer_mobile']).'" >'.'</td>'.(empty($v['office_unit_organogram_id'])?'<td title="সম্পাদনা করুন"  class="editrow" style="width: 30px;text-align: center;"><i class="fa fa-pencil-square-o"></i></td>':'<td class="disabled-edit" title="সিস্টেম ইউজার সম্পাদনাযোগ্য নয়"><i class="fa fa-pencil-square-o"></i></td>').'<td class="text-left">&nbsp;&nbsp;'.(!empty($v['employee_name_bng'])
                    ? h($v['employee_name_bng']).', ' : '').h($v['office_unit_organogram_name_bng']).', '.(!empty($v['office_unit_name_bng'])
                    ? h($v['office_unit_name_bng']).', ' : '').h($v['office_name_bng']).(empty($v['office_id'])
                    ? ' (<b> '.h($v['officer_email']).(!empty($v['officer_mobile'])?', '.h($v['officer_mobile']):'').') ' : '').'</b></td></tr>';
                                            }
                                else {
                                    echo '<tr><td title="মুছুন" class="deleterow" style="width: 30px;text-align: center;"><i class="fa fa-times"></i><input type="hidden" class="group_mail_permission" name="office-employee" data-office-name="'.h($v['office_name_bng']).'" data-office-name-eng="'.h($v['office_name_eng']).'" data-office-id="'.$v['office_id'].'" data-office-employee-name="'.h($v['employee_name_bng']).'" data-office-employee-name-eng="'.h($v['employee_name_eng']).'" data-office-employee-id="'.$v['employee_id'].'"'.' data-office-unit-id="'.$v['office_unit_id'].'" data-office-unit-organogram-id="'.$v['office_unit_organogram_id'].'" '.'data-designation-name="'.h($v['office_unit_organogram_name_bng']).'" data-designation-name-eng="'.h($v['office_unit_organogram_name_eng']).'" data-unit-name="'.h($v['office_unit_name_bng']).'" data-unit-name-eng="'.h($v['office_unit_name_eng']).'" data-office-head="'.$v['office_head'].'">'.'</td>'.(empty($v['office_unit_organogram_id'])?'<td title="সম্পাদনা করুন" class="editrow" style="width: 30px;text-align: center;"><i class="fa fa-pencil-square-o"></i></td>':'<td class="disabled-edit" title="সিস্টেম ইউজার সম্পাদনাযোগ্য নয়"><i class="fa fa-pencil-square-o"></i></td>').'<td class="text-left">&nbsp;&nbsp;'.(!empty($v['employee_name_bng'])
                                            ? h($v['employee_name_bng']).', ' : '').h($v['office_unit_organogram_name_bng']).', '.(!empty($v['office_unit_name_bng'])
                                            ? h($v['office_unit_name_bng']).', ' : '').h($v['office_name_bng']).'</td></tr>';
                                }
    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                  <div class="row text-center">
                    <button class="btn btn-primary btn-lg" id="sub_grp_potrojari"><?= __('Submit') ?></button>
                </div>
            </div>
            <div class="col-md-6">
                <div class="" id="showlist">

                </div>
            </div>
        </div>
      
    </div>
</div>

<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2.css" type="text/javascript">
<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>assets/global/plugins/jquery-multi-select/css/multi-select.css"/>
<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/jQuery-Mask/jquery.mask.min.js"></script>
<script>
    var EmployeeAssignment = {
        loadMinistryWiseLayers: function () {
            OfficeSetup.loadLayers($("#office-ministry-id").val(), function (response) {

            });
        },
        loadMinistryAndLayerWiseOfficeOrigin: function () {
            OfficeSetup.loadOffices($("#office-ministry-id").val(), $("#office-layer-id").val(), function (response) {

            });
        },
        loadOriginOffices: function () {
            OfficeSetup.loadOriginOffices($("#office-origin-id").val(), function (response) {

            });
        },
        loadOfficeUnits: function () {
            OfficeSetup.loadOfficeUnits($("#office-id").val(), function (response) {

            });
        },
    };

    $(function () {

        if ($("#office-unit-id").val() > 0) {
            getNotification($("#office-id").val(),$("#office-unit-id").val());
        }

        $("#office-ministry-id").bind('change', function () {
            EmployeeAssignment.loadMinistryWiseLayers();
        });
        $("#office-layer-id").bind('change', function () {
            EmployeeAssignment.loadMinistryAndLayerWiseOfficeOrigin();
        });
        $("#office-origin-id").bind('change', function () {
            EmployeeAssignment.loadOriginOffices();
        });
        $("#office-id").bind('change', function () {
            EmployeeAssignment.loadOfficeUnits();
             getNotification($(this).val() );
        });
        $("#office-unit-id").bind('change', function () {
            getNotification($("#office-id").val(), $(this).val());
        });
        function getNotification(office_id, unit_id) {
            $('#showlist').html('<img src="<?= CDN_PATH ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp; লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');
            $.ajax({
                type: 'POST',
                url: "<?php echo $this->Url->build(['controller' => 'NothiMasters', 'action' => 'getOtherOfficePermissionList']) ?>",
                data: {office_id: office_id, office_unit_id: unit_id, nothi_id: 0, nothi_office: office_id},
                success: function (data) {
                    $('#showlist').html('');
                    $('#showlist').html('<div class="font-lg">সকল পদবি দেখার জন্য  <b>&darr;শাখার নামে</b> ক্লিক করুন </div>'+data );
                    setTimeout(function(){
                        $(document).find('.optionsothers.checkclick').trigger('click')
                    },5)
                }
            });
        }

        $('#employee-personal-mobile').mask('AAAAAAAAAAA'
            ,{
                translation:{
                    A: {pattern: /[\u09E6-\u09EF0-9]/},
                },
                placeholder: "01XXXXXXXXX"
            });
    });
    var numbers = {
        1: '১',
        2: '২',
        3: '৩',
        4: '৪',
        5: '৫',
        6: '৬',
        7: '৭',
        8: '৮',
        9: '৯',
        0: '০'
    };

    function replaceNumbers(input) {
        var output = [];
        for (var i = 0; i < input.length; ++i) {
            if (numbers.hasOwnProperty(input[i])) {
                output.push(numbers[input[i]]);
            } else {
                output.push(input[i]);
            }
        }
        return output.join('');
    }
    function list_all(e_id)
    {
        $.each($('#showlist').find('.optionsothers'), function (i, v) {
            if ($(v).data('office-unit-organogram-id') == e_id)
            {
                if ($(v).is(':checked'))
                {
                    var f = 0;
                    $.each($('#addotherpermission').find('.deleterow'), function () {
                        if ($(this).find('input').data('officeUnitOrganogramId') == e_id) {
                            toastr.error("এই ব্যক্তি গ্রুপে অন্তর্ভুক্ত আছে");
                            f = 1;
                        }
                    });
                    if(f === 0){
                    var ok = '';
                    var id = escapeHtml($(v).data('employee-id'));
                    var name = escapeHtml($(v).data('employee-name'));
                    var name_eng = escapeHtml($(v).data('employee-name-eng'));
                    var of_id = escapeHtml($(v).data('employee-office-id'));
                    var unit_id = escapeHtml($(v).data('office-unit-id'));
                    var org_id = escapeHtml($(v).data('office-unit-organogram-id'));
                    var designation = escapeHtml($(v).data('designation-name'));
                    var designation_eng = escapeHtml($(v).data('designation-name-eng'));
                    var unit_name = escapeHtml($(v).data('unit-name'));
                    var unit_name_eng = escapeHtml($(v).data('unit-name-eng'));
                    var designation_level = escapeHtml($(v).data('designation-level'));
                    var office_name =  escapeHtml($(v).data('office-name'));
                    var office_name_eng =  escapeHtml($(v).data('office-name-eng'));
                    var personal_email =  escapeHtml($(v).data('employee-personal-email'));
                    var personal_mobile =  escapeHtml($(v).data('employee-personal-mobile'));
                    var office_head =  escapeHtml($(v).data('office-head'));
                    ok += '<tr><td title="মুছুন" class="deleterow" style="width: 30px;text-align: center;"><i class="fa fa-times"></i>';
                    ok += '<input type="hidden" class="group_mail_permission" name="office-employee" data-office-name="'+office_name+'" data-office-name-eng="'+office_name_eng+'" data-employee-personal-email="'+personal_email+'" data-employee-personal-mobile="'+personal_mobile+'" data-office-id="' + of_id + '"' +
                            'data-office-employee-name="' + name + '" data-office-employee-name-eng="' + name_eng + '" data-office-employee-id="' + id + '"' +
                            ' data-office-unit-id="' + unit_id + '" data-office-unit-organogram-id="' + org_id + '" ' +
                            'data-designation-name="' + designation + '" data-designation-name-eng="' + designation_eng + '" data-unit-name="' + unit_name + '" data-unit-name-eng="' + unit_name_eng + '" data-designation-level ="' + designation_level + '" data-office-head ="' + office_head + '">' +
                            '</td><td style="width: 30px;"></td><td class="text-left">&nbsp;&nbsp;' + name + ', ' + designation + ', ' + unit_name + ', ' + office_name + '</td></tr>';
                    $('#addotherpermission').append(ok);
                }
                } else {}
            }
        });
         refresh_delete();
    }
    var selectedPriviliges = {};
    $("#sub_grp_potrojari").on('click',function(){
        $("#sub_grp_potrojari").attr('disabled', 'disabled');
        var group_name = $("#group-name").val();
        selectedPriviliges = [];
       $.each($('#addotherpermission').find('.group_mail_permission'), function (j, v) {
            var o_office_id = escapeHtml($(v).data('office-id'));
            var o_office_unit_id = escapeHtml($(v).data('office-unit-id'));
            var o_office_unit_organogram_id = escapeHtml($(v).data('office-unit-organogram-id'));
            var o_employee_id = escapeHtml($(v).data('office-employee-id'));
            var o_office_unit_name = escapeHtml($(v).data('unit-name'));
            var o_office_unit_name_eng = escapeHtml($(v).data('unit-name-eng'));
            var o_office_unit_organogram_name = escapeHtml($(v).data('designation-name'));
            var o_office_unit_organogram_name_eng = escapeHtml($(v).data('designation-name-eng'));
            var o_office_name = escapeHtml($(v).data('office-name'));
            var o_office_name_eng = escapeHtml($(v).data('office-name-eng'));
            var o_employee_name = escapeHtml($(v).data('office-employee-name'));
            var o_employee_name_eng = escapeHtml($(v).data('office-employee-name-eng'));
            var o_employee_personal_email = escapeHtml($(v).data('employee-personal-email'));
           if(!(isEmpty($(v).data('employee-personal-mobile')))){
               var o_employee_personal_mobile = escapeHtml($(v).data('employee-personal-mobile'));
           }
            var o_office_head = escapeHtml($(v).data('office-head'));
            if($(v).data('officer-email') == '' || $(v).data('officer-email') == undefined || $(v).data('officer-email') == null|| typeof($(v).data('officer-email')) == 'undefined'){
                var o_officer_email= '';
            }else{
                 var o_officer_email = escapeHtml($(v).data('employee-personal-email'));
            }

           selectedPriviliges[j] = JSON.stringify({
               office_id:o_office_id,
               office_unit_id:o_office_unit_id,
               office_unit_organogram_id:o_office_unit_organogram_id,
               employee_id:o_employee_id,
               office_unit_name:o_office_unit_name,
               office_unit_name_eng:o_office_unit_name_eng,
               office_unit_organogram_name:o_office_unit_organogram_name,
               office_unit_organogram_name_eng:o_office_unit_organogram_name_eng,
               office_name:o_office_name,
               office_name_eng:o_office_name_eng,
               employee_name:o_employee_name,
               employee_name_eng:o_employee_name_eng,
               employee_personal_email:o_employee_personal_email,
               employee_personal_mobile:o_employee_personal_mobile,
               office_head:o_office_head,
               officer_email:o_officer_email
           });


        });
        if(group_name == '' || typeof(group_name) == 'undefined' || group_name == null || group_name == 'undefined'){
            toastr.error('গ্রুপের নাম দেওয়া হয়নি।');
            $("#sub_grp_potrojari").removeAttr('disabled');
            return false;
        }
        if( jQuery.isEmptyObject(selectedPriviliges)){
             toastr.error('গ্রুপ মেইলে অন্তর্ভুক্ত ব্যক্তিবর্গ দেওয়া হয়নি।');
            $("#sub_grp_potrojari").removeAttr('disabled');
              return false;
        }

        $.ajax({
            url: '<?=$this->Url->build(['controller' => 'potrojariGroups' , 'action' => 'groupMailAdd',$id])?>',
            type: 'POST',
            data: {group_name : group_name, permitted: selectedPriviliges},
            success: function(data){
                if(data == 'success'){
                    toastr.success('সংরক্ষণ করা হয়েছে');
                    $("#sub_grp_potrojari").removeAttr('disabled');
                    window.location.href = '<?= $this->Url->build(['controller' => 'potrojariGroups' , 'action' => 'groupMail'])?>';
                }
                else{
                    toastr.error('আপডেট করা সম্ভব হয়নি। কিছুক্ষণ পর আবার চেষ্টা করুন');
                    $("#sub_grp_potrojari").removeAttr('disabled');
                }
            }
        });

        // saveGroup(group_name,selectedPriviliges,0);
//        PROJAPOTI.ajaxSubmitAsyncDataCallback('<?//= $this->Url->build(['_name'=>'groupReset',$id]) ?>//',
//            {},'json',function(response){
//
//            if(response.status == 'success'){
//
//            }else{
//                Metronic.unblockUI('.page-container');
//                $("#sub_grp_potrojari").removeAttr('disabled');
//                toastr.error('সংরক্ষণ করা সম্ভব হয়নি। কিছুক্ষণ পর আবার চেষ্টা করুন');
//            }
//        })
    });

    function saveGroup(group_name,selectedPriviliges,i){

        if(typeof selectedPriviliges[i] == 'undefined'){
            toastr.success('সংরক্ষণ কার্যক্রম সম্পন্ন করা হয়েছে');
            Metronic.unblockUI('.page-container');
             setTimeout(function () {
                window.location.href = js_wb_root + 'potrojariGroups/groupMail';
            }, 1000);
            return false;
        }
        Metronic.blockUI({
            target: '.page-container',
            boxed: true,
            message : BnFromEng(Object.keys(selectedPriviliges).length)+' এর মধ্যে '+BnFromEng(i + 1)+ ' নং রিকোয়েস্ট চলছে। অপেক্ষা করুন।'
        });
        $.ajax({
            url: '<?=$this->Url->build(['controller' => 'potrojariGroups' , 'action' => 'groupMailSave'])?>',
            type: 'POST',
            data: {group_name : group_name, permitted: selectedPriviliges[i],id:'<?=$id?>'},
            success: function(data){
                if(data == 'success'){
                    $('.deleterow').eq(i).closest('tr').addClass('success');
                    //toastr.success('সংরক্ষণ করা হয়েছে');
                }
                else{
                    $('.deleterow').eq(i).closest('tr').addClass('danger');
                    //toastr.error('সংরক্ষণ করা সম্ভব হয়নি। কিছুক্ষণ পর আবার চেষ্টা করুন');
                }
                saveGroup(group_name,selectedPriviliges,++i);
            }
        });
    }

    refresh_delete();
    function refresh_delete(){
        var bn = replaceNumbers("'" + ($('.deleterow').length) + "'");
        bn = bn.replace("'", '');
        bn = bn.replace("'", '');
        $('.totalgroupmember').text(bn);

    }
    $(document).on("click",".deleterow",function(){
        deleteRow($(this));
    });
    function deleteRow(that){
        that.closest('tr').remove();
        var bn = replaceNumbers("'" + ($('.deleterow').length) + "'");
        bn = bn.replace("'", '');
        bn = bn.replace("'", '');
        $('.totalgroupmember').text(bn);
    }
        $(document).on("click",".editrow",function(){
        var employee_name = $(this).prev(".deleterow").find('.group_mail_permission').attr('data-office-employee-name');
        var employee_name_eng = $(this).prev(".deleterow").find('.group_mail_permission').attr('data-office-employee-name-eng');
        var designation_name = $(this).prev(".deleterow").find('.group_mail_permission').attr('data-designation-name');
        var designation_name_eng = $(this).prev(".deleterow").find('.group_mail_permission').attr('data-designation-name-eng');
        var office_name = $(this).prev(".deleterow").find('.group_mail_permission').attr('data-office-name');
        var office_name_eng = $(this).prev(".deleterow").find('.group_mail_permission').attr('data-office-name-eng');
        var unit_name = $(this).prev(".deleterow").find('.group_mail_permission').attr('data-unit-name');
        var unit_name_eng = $(this).prev(".deleterow").find('.group_mail_permission').attr('data-unit-name-eng');
        var personal_email = $(this).prev(".deleterow").find('.group_mail_permission').attr('data-employee-personal-email');
        var personal_mobile = $(this).prev(".deleterow").find('.group_mail_permission').attr('data-employee-personal-mobile');


        $("#officer-name").val(employee_name);
        $("#officer-name-eng").val(employee_name_eng);
        $("#officer-designation").val(designation_name);
        $("#officer-designation-eng").val(designation_name_eng);
        $("#officer-unit").val(unit_name);
        $("#officer-unit-eng").val(unit_name_eng);
        $("#officer-office").val(office_name);
        $("#officer-office-eng").val(office_name_eng);
        $("#employee-personal-email").val(personal_email);
        $("#employee-personal-mobile").val(personal_mobile);

        deleteRow($(this));
        refresh_delete();
        $("#officer-new").click();
        $("#officer-name").focus()
    });
    function new_user(){
        var officer_name = escapeHtml($("#officer-name").val());
        var officer_name_eng = escapeHtml($("#officer-name-eng").val());
        var officer_designation = escapeHtml($("#officer-designation").val());
        var officer_designation_eng = escapeHtml($("#officer-designation-eng").val());
        var officer_office = escapeHtml($("#officer-office").val());
        var officer_office_eng = escapeHtml($("#officer-office-eng").val());
        var officer_unit = escapeHtml($("#officer-unit").val());
        var officer_unit_eng = escapeHtml($("#officer-unit-eng").val());
        var officer_email = escapeHtml($("#employee-personal-email").val());
        var officer_mobile = escapeHtml($("#employee-personal-mobile").val());
            if (isEmpty(officer_email) ||  regex.test(officer_email) == false) {
                toastr.error("দুঃখিত! অফিসারের ইমেইল সঠিক নয়");
                return false;
            }
        if(!isEmpty(officer_mobile)){
            officer_mobile = EngFromBn(officer_mobile);
            if(officer_mobile.length != 11){
                toastr.error("দুঃখিত! মোবাইল নম্বর সঠিক নয়");
                return false;
            }
            if(!/[0-9]{11}/.test(officer_mobile)){
                toastr.error("দুঃখিত! মোবাইল নম্বর সঠিক নয়");
                return false;
            }
        }

        if(officer_designation == '' || officer_designation == undefined || officer_designation == null){
            toastr.error(' অফিসারের পদ দেওয়া হয় নি');
            return false;
        }
        if(officer_office == '' || officer_office == undefined || officer_office == null){
               toastr.error(' অফিসারের কার্যালয়  দেওয়া হয় নি');
            return false;
        }

        $("#officer-name").val(''); $("#officer-name-eng").val('');  $("#officer-designation").val(''); $("#officer-designation-eng").val('');  $("#officer-office").val(''); $("#officer-office-eng").val('');  $("#employee-personal-email").val(''); $("#officer-unit").val(''); $("#officer-unit-eng").val('');$("#employee-personal-mobile").val('');
         var ok = '';
          ok += '<tr><td title="মুছুন" class="deleterow" style="width: 30px;text-align: center;"><i class="fa fa-times"></i>';
                    ok += '<input type="hidden" class="group_mail_permission" name="office-employee" data-office-name="'+officer_office+'" data-office-name-eng="'+officer_office_eng+'" data-office-id="' + 0 + '"' +
                            'data-office-employee-name="' + officer_name + '" data-office-employee-name-eng="' + officer_name_eng + '" data-office-employee-id="' + 0 + '"' +
                            ' data-office-unit-id="' + 0 + '" data-office-unit-organogram-id="' + 0 + '" ' +
                            'data-designation-name="' + officer_designation + '" data-designation-name-eng="' + officer_designation_eng + '" data-unit-name="' + officer_unit + '" data-unit-name-eng="' + officer_unit_eng + '" data-designation-level ="' + 0 + '" data-employee-personal-email ="'+officer_email+'" data-employee-personal-mobile ="'+officer_mobile+'"  data-office-head ="'+ 0 +'">' +
                        '</td><td title="সম্পাদনা করুন" class="editrow" style="width: 30px;text-align: center;"><i class="fa fa-pencil-square-o"></i></td><td class="text-left">&nbsp;&nbsp;' + ((officer_name == '' || officer_name == undefined || officer_name == null)?'':officer_name+', ') + officer_designation+', ' +((officer_unit == '' || officer_unit == undefined || officer_unit == null)?'':officer_unit+', ') +officer_office+ (officer_email.length > 0?' (<b> '+officer_email+((officer_mobile == '' || officer_mobile == undefined || officer_mobile == null)?'':', '+officer_mobile) + ' </b>)' : '') + '</td></tr>';
                    $('#addotherpermission').append(ok);
                    refresh_delete();
    }

    $('[type="email"]').change(function () {
        if (this.value != '') {
            if (regex.test(this.value) == false) {
                toastr.error("দুঃখিত! অফিসারের ইমেইল সঠিক নয়");
                $(this).focus();
            }
        }
    });

    var regex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

    <?php if(!empty($user_info['office_id'])){ ?>
    $("#own-office").click(function() {
        var officeId = <?=$user_info['office_id']?>;
        if ($("#office-id option[value="+officeId+"]").length == 0) {
            $("#office-id").append('<option value="'+officeId+'"><?=$user_info['office_name']?></option>');
            $("#office-id").val(officeId).select2().trigger('change');
        }
    });
    <?php } ?>
</script>