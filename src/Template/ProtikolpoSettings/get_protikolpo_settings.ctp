<script>
    $('body').addClass('page-sidebar-closed');
    $('.page-sidebar-menu').addClass('page-sidebar-menu-closed');
</script>
<div class="UIblock">
<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">দায়িত্ব হস্তান্তরের তারিখ নির্বাচন করুন</div>
	    <div class="actions">
		    <a class="btn btn-sm green" href="<?php echo $this->Url->build(['controller' => 'Dashboard', 'action' => 'dashboard']) ?>"><i class="fa fa-home"></i> হোম </a>
	    </div>
    </div>
    <div class="portlet-body form">
        <div class="form-horizontal">
            <div class="form-group">
                <div class="row">
                    <div class="form-group">
                        <label class="control-label col-md-3">ছুটির সময় </label>
                        <div class="col-md-5">
                            <div class="input-group input-large date-picker input-daterange" data-date-format="mm/dd/yyyy" style="width: 100% !important;">
                                <input type="text" id="protikolpo_start_date" class="form-control"
                                       value="<?= !empty($protikolpo_settings[0]['start_date'])?$protikolpo_settings[0]['start_date']->format("Y-m-d"):'' ?>"/>
                                <span class="input-group-addon"> হইতে </span>
                                <input type="text" id="protikolpo_end_date" class="form-control" value="<?= !empty($protikolpo_settings[0]['end_date'])?$protikolpo_settings[0]['end_date']->format("Y-m-d"):'' ?>"> </div>
                        </div>
                        <div class="col-md-4">
                            <input type="checkbox" id="protikolpo_show_acting" <?= !empty($protikolpo_settings[0]['is_show_acting'])&&$protikolpo_settings[0]['is_show_acting']==1?'checked':'' ?> />
                            <label for="protikolpo_show_acting" class="form-inline">
                                পদবিতে
                                <select name="acting_level" class="form-control">
                                    <?php foreach ($acting_levels as $key => $acting_level): ?>
                                    <option <?=((isset($protikolpo_settings[0]['acting_level']) && $protikolpo_settings[0]['acting_level'])==$acting_level) ? "selected" : ""?> value="<?=$acting_level?>"><?=$acting_level?></option>
                                    <?php endforeach; ?>
                                </select>
                                দেখাবে
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="sample_1">
                    <tr class="heading">
                        <th class="text-center">ক্রম</th>
                        <th class="text-center">পদবি</th>
                        <th class="text-center">অবস্থা</th>
                    </tr>
                    <tbody>
                    <tr class="odd gradeX">
                        <?php

                        if(empty($protikolpo_settings)){
                           echo "<tr>
                            <td colspan='3' class='text-center' ><span style='color: red !important'>দুঃখিত! প্রতিকল্প সেটিংস পাওয়া যায়নি।</span></td>
                            </td>";
                        }
                        $i = 0;
                        foreach ($protikolpo_settings as $row => $protikolpo):
                        ?>
                    <tr class="designation_id" data-designation-id="<?= !empty($protikolpo['designation_id'])?$protikolpo['designation_id']:0 ?>">
                        <td class="text-center" ><?php echo $this->Number->format(++$i) ?></td>
                        <td width="30%"><?php echo $protikolpo['designation_name_bng'] ?></td>
                        <td class="text-center status" width="50%">
                            <?php
                            if(!empty($protikolpo['start_date']) && !empty($protikolpo['end_date'])) {
                                if ($protikolpo['selected_protikolpo'] == 1) {
                                    echo "প্রতিকল্প ১ (<b>".$protikolpo['protikolpo_info']."</b>) নির্বাচন করা হয়েছে।";
                                } else if ($protikolpo['selected_protikolpo'] == 2) {
                                    echo "প্রতিকল্প ২ (<b>".$protikolpo['protikolpo_info']."</b>) নির্বাচন করা হয়েছে।";
                                }else {
                                    echo "সকল প্রতিকল্প এ সময় ছুটিতে রয়েছে।";
                                }
                            }
                            ?></td>

                    </tr>
                    <?php endforeach; ?>
                    <tr class="form-actions">
                        <td colspan="3" class="text-center">
                            <input type="button" class="btn green protikolpo_submit" value="<?php echo __(SAVE); ?>"/>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
    </div>
</div>
</div>
<script>
    $(document).find('.date-picker').datepicker({
        rtl: Metronic.isRTL(),
        autoclose: true,
        format: "yyyy-mm-dd",
		startDate: '<?= date("Y-m-d",strtotime("+1 days")) ?>'
    });

    $('#protikolpo').select2({
        width: '100%'
    })

    $(document).off('click','.protikolpo_submit').on('click', '.protikolpo_submit', function (e) {
        var protikolpo_start_date =$('#protikolpo_start_date').val();
        var protikolpo_end_date = $('#protikolpo_end_date').val();
        var protikolpo_show_acting = 0;
        var protikolpo_show_acting_level = '';
        if ($("#protikolpo_show_acting").is(':checked')) {
            protikolpo_show_acting = 1;
            protikolpo_show_acting_level = $("select[name=acting_level]").val();
        }
        if(isEmpty(protikolpo_start_date) || isEmpty(protikolpo_end_date)){
            toastr.error("অনুগ্রহ করে দায়িত্ব হস্তান্তরের তারিখ নির্বাচন করুন।");
            return;
        }

        if(Date.parse(protikolpo_start_date) > Date.parse(protikolpo_end_date)){
            toastr.error("অনুগ্রহ করে সঠিক তারিখ সীমা নির্বাচন করুন।");
            return;
        }

        Metronic.blockUI({
            target: '.UIblock',
            boxed: true
        });
        checkAvailablity(protikolpo_start_date,protikolpo_end_date, protikolpo_show_acting, protikolpo_show_acting_level);
    });
    function checkAvailablity(start_date,end_date, show_acting, protikolpo_show_acting_level) {
        $.ajax({
            url: '<?php echo $this->Url->build(['controller'=>'ProtikolpoSettings','action'=>'setLeaveDate']) ?>',
            data: {start_date:start_date,end_date:end_date, show_active:show_acting, acting_level:protikolpo_show_acting_level},
            method: 'post',
            success: function(res){

                if(res.status == 'success') {
                    $.each(res.data,function (i,v) {
                        if(v.protikolpo_1==1){
                            $('.designation_id[data-designation-id='+i+']').find('td:last').html('প্রতিকল্প ১ (<b>'+v.protikolpo_1_info +'</b>) নির্বাচন করা হয়েছে।');
                        } else if(v.protikolpo_2 == 1){
                            $('.designation_id[data-designation-id='+i+']').find('td:last').html('প্রতিকল্প ২ (<b>'+v.protikolpo_2_info +'</b>) নির্বাচন করা হয়েছে।');
                        } else if(v.protikolpo_2 == 0 && v.protikolpo_1 == 0){
							$('.designation_id[data-designation-id='+i+']').find('td:last').text('সকল প্রতিকল্প এ সময় ছুটিতে রয়েছে।');
						} else {
                            $('.designation_id[data-designation-id='+i+']').find('td:last').text('প্রতিকল্প পাওয়া যায় নি।');
                        }
                    });
                    toastr.success("প্রতিকল্প ব্যবস্থাপনা সংরক্ষণ করা হয়েছে।");
                    Metronic.unblockUI('.UIblock');
                } else {
                    toastr.error("দুঃখিত! সংরক্ষণ করা সম্ভব হচ্ছে না। কিছুক্ষন পরে আবার চেস্টা করুন।");
                    Metronic.unblockUI('.UIblock');

                }
            }
        });
    }
</script>