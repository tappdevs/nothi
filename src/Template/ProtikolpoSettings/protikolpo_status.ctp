<style>
	.btn-group .btn {
		margin: 0!important;
	}
</style>
<div class="row">
    <div class="col-md-12">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    প্রতিকল্প অবস্থা
                </div>
            </div>
	        <span class="hidden" id="logged_officer_id"><?=$employee_offices['officer_id']?></span>
            <div class="portlet-body protikolpo-status">
                <div class="alert alert-info font-lg text-center">
                    <i class="fa fa-info-circle " aria-hidden="true"></i>
                    প্রতিকল্পের তারিখ পরিবর্তন করার পরে সংরক্ষন করুন। আজকের তারিখে প্রতিকল্প নির্বাচন করলে সংরক্ষনের পরে প্রয়োগ করুন।
                </div>
                <table class="table table-striped table-bordered table-hover">
                    <tr class="heading">
                        <th class="text-center">ক্রম</th>
                        <th class="text-center">পদবি</th>
                        <th class="text-center">প্রতিকল্পকৃত পদবি</th>
                        <th class="text-center">প্রতিকল্পকারি ব্যক্তি</th>
                        <th class="text-center">ছুটি শুরু</th>
                        <th class="text-center">ছুটি শেষ</th>
                        <th class="text-center">কার্যক্রম</th>
                    </tr>
                    <?php

                    use Cake\I18n\Number;
                    use Cake\I18n\Time;
                    use Cake\Routing\Route\Route;
                    use Cake\Routing\Router;

                    if(!empty($protikolpo)){

                        foreach($protikolpo as $key=>$value):
                    ?>
                         <tr>
                             <td class="text-center"><?= Number::format($key+1) ?></td>
                             <td><?= !empty($value['designation'])?$value['designation']:'' ?></td>
                             <td>
                                 <?= !empty($value['selected_protikolpo'])?$value['selected_protikolpo']:'' ?>
                             </td>
                             <td><?= $value['employee_record_id']>0?h($value['employee_name']):'' ?></td>
                             <td class="text-center"><?= $value['employee_record_id']>0?
                                     (Time::parse($value['start_date'])->getTimestamp() - time() >0?
                                     $this->Form->input('start_date',['class'=>'date-picker start-date form-control', 'style'=>'width:150px;',
                                         'value'=> $value['start_date']->format("Y-m-d"),
                                         'label'=>false]) :  $value['start_date']->format("Y-m-d")):'' ?></td>
                             <td class="text-center"><?= $value['employee_record_id']>0?
                                     (Time::parse($value['end_date'])->getTimestamp() - time() > 0?
                                     $this->Form->input('end_date',['class'=>'date-picker end-date form-control', 'style'=>'width:150px;',
                                         'value'=>$value['end_date']->format("Y-m-d"),
                                         'label'=>false]):$value['end_date']->format("Y-m-d")):'' ?></td>
                             <td class="text-center">
	                             <input type="hidden" name="officer_id" value="<?=$value['employee_record_id']?>" />

	                             <div class="btn-group btn-group-round" style="width:130px;">
                                 <?= $value['total']>0 ?($this->Html->link('<i class="fa fa-file"></i>',
                                         ['_name'=>'viewLog',$value['id']],['class'=>'btn btn-sm btn-success btn-log', 'data-toggle'=>"tooltip",'title'=>__('Log'), 'data-id'=>$value['id'], 'escape'=>false])
                                 ):''  ?>
                                 <?= $value['employee_record_id']>0?
                                     ($this->Form->button('<i class="fa fa-save"></i>',
                                         ['class'=>'btn btn-sm green btn-save', 'data-toggle'=>"tooltip",'title'=>__('SAVE'), 'data-id'=>$value['id']]) .
                                         $this->Form->button('<i class="fa fa-random"></i>',
                                             ['class'=>'btn btn-sm btn-warning btn-start-now','data-toggle'=>"tooltip",'title'=>__('Run'),'data-id'=>$value['id']])
                                     ):'' ?>
                                 <?= $value['employee_record_id']>0?$this->Form->button('<i class="fa fa-trash"></i>',['class'=>'btn btn-sm btn-danger btn-delete', 'data-toggle'=>"tooltip",'title'=>__('Delete'),'data-id'=>$value['id']]):
                                     $this->Html->link('<i class="fa fa-edit"></i>',['_name'=>'protikolpoSetup'],['class'=>'btn btn-sm btn-primary', 'data-toggle'=>"tooltip",'title'=>__('Edit'),'target'=>'_blank', 'escape'=>false]) ?>
	                             </div>
                             </td>
                         </tr>
                    <?php
                        endforeach;;

                    }
                    ?>
                </table>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo CDN_PATH ?>assets/global/plugins/bootbox/bootbox.min.js" type="text/javascript"></script>
<script>
	$(function(){
        $('[data-toggle="tooltip"]').tooltip({'placement':'bottom',trigger: "hover" });
		$('.date-picker').datepicker({
			autoclose: true,
			format: "yyyy-mm-dd",
			startDate: '<?= date("Y-m-d") ?>'
		});

		if($('.protikolpo-status input').length > 0) {
			$('.btn-save').click(function () {

				var data = {};
				var that = this;
				if($(that).closest('tr').find('.start-date').length > 0){
					data.start_date = $(that).closest('tr').find('.start-date').val();
                }
				if($(that).closest('tr').find('.end-date').length > 0){
					data.end_date = $(that).closest('tr').find('.end-date').val();
				}

				if(Object.keys(data).length > 0){
					data.id = $(this).data('id');
					bootbox.dialog({
						message: "আপনি কি সংরক্ষন করতে ইচ্ছুক?",
						title: "সংরক্ষন করুন",
						buttons: {
							success: {
								label: "হ্যাঁ",
								className: "green",
								callback: function () {
									PROJAPOTI.ajaxSubmitAsyncDataCallback('<?= Router::url(['_name'=>'protikolpoEditStatus']) ?>',data,'json',function(response){

										if(response.status == 'success'){
											$(that).closest('tr').addClass('success').removeClass('danger')
										}else {
											$(that).closest('tr').addClass('danger').removeClass('success')
											toastr.error(response.message)
										}
									})
								}
							},
							danger: {
								label: "না",
								className: "red",
								callback: function () {

								}
							}
						}
					});
                }
			})
			function yyyymmdd(returnDate) {
				var x = new Date(returnDate);
				var y = x.getFullYear().toString();
				var m = (x.getMonth() + 1).toString();
				var d = x.getDate().toString();
				(d.length == 1) && (d = '0' + d);
				(m.length == 1) && (m = '0' + m);
				var yyyymmdd = y + '-' + m + '-' + d;
				return yyyymmdd;
			}
			$('.btn-delete').click(function () {
				var data = {};
				data.id = $(this).data('id');
				var that = this;
				if($(that).closest('tr').find('.start-date').length > 0){
					data.start_date = $(that).closest('tr').find('.start-date').val();
                }
				if($(that).closest('tr').find('.end-date').length > 0){
					data.end_date = yyyymmdd(new Date(Date.now() - 864e5));
				}
				if(Object.keys(data).length > 0){
					data.id = $(this).data('id');
					bootbox.dialog({
						message: "আপনি কি প্রতিকল্প বাতিল করতে ইচ্ছুক?",
						title: "প্রতিকল্প বাতিল করুন",
						buttons: {
							success: {
								label: "হ্যাঁ",
								className: "green",
								callback: function () {
									PROJAPOTI.ajaxSubmitAsyncDataCallback('<?= Router::url(['_name'=>'protikolpoCancel']) ?>',data,'json',function(response){

										if(response.status == 'success'){
											toastr.success(response.message);
                                            window.location.reload();
										}else {
											$(that).closest('tr').addClass('danger').removeClass('success')
											toastr.error(response.message)
										}
									})
								}
							},
							danger: {
								label: "না",
								className: "red",
								callback: function () {

								}
							}
						}
					});
				}

			})
		}

		$('.btn-start-now').click(function () {
			var data = {};
			data.id = $(this).data('id');
			var that = this;
			var isRequestLogout = false;
            if ($(this).parent().parent().find('input[name=officer_id]').val() == $("#logged_officer_id").text()) {
                isRequestLogout = true;
            }
			if(Object.keys(data).length > 0){
				bootbox.dialog({
					message: "আপনি কি প্রয়োগ করতে ইচ্ছুক?",
					title: "প্রয়োগ করুন",
					buttons: {
						success: {
							label: "হ্যাঁ",
							className: "green",
							callback: function () {
								PROJAPOTI.ajaxSubmitAsyncDataCallback('<?= Router::url(['_name'=>'protikolpoStart']) ?>',data,'json',function(response){

									if(response.status == 'success'){
									    if (isRequestLogout) {
                                            window.location.href = js_wb_root + 'logout';
									    } else {
                                            window.location.reload();
                                        }
									}else {
										$(that).closest('tr').addClass('danger').removeClass('success')
										toastr.error(response.message)
									}
								})
							}
						},
						danger: {
							label: "না",
							className: "red",
							callback: function () {

							}
						}
					}
				});
			}

		})
    })

</script>