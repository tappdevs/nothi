<?php
$row_options = [];
$selec1 = [];
$selec2 = [];
?>
<style>
    .select2-container.protikolpo_1, .select2-container.protikolpo_2{
        width: 340px !important;
    }
</style>
<div class="row">
    <div class="col-md-12">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    প্রতিকল্প ব্যবস্থাপনার তালিকা
                </div>
            </div>
            <div class="portlet-body" style="overflow: scroll">
                <div class="col-md-6" style="padding-bottom: 5px; padding-left: 0px !important;">
                    <input type="text" class="form-control" id="search" placeholder=" ব্যবহারকারী খুজুন">
                </div>
                <table class="table table-striped table-bordered table-hover" id="sample_1">
                    <tr class="heading">
                        <th style="width: 4%!important;"></th>
                        <th class="text-center" style="width: 31%!important;">পদবি</th>
                        <th class="text-center" style="width: 31%!important;">প্রথম প্রতিকল্প</th>
                        <th class="text-center" style="width: 31%!important;">দ্বিতীয় প্রতিকল্প</th>
                        <th class="text-center" style="width: 5%!important;">এ্যাকশন</th>
                    </tr>
                    <tbody>
                    <tr class="odd gradeX">
                        <?php
                        $i = 0;
                        $other = [];
                        foreach ($allOrganogram as $key=>$rows):
                        $selected1 = [];
                        $selected2 = [];
                        $row_options[] = ['text' => $rows['name_bng'] . ', ' . $rows['designation_bng'] . ', ' . $rows['unit_name_bng'],
                            'value' => $rows['office_unit_organogram_id'],
                            'data-employee-record_id' =>$rows['employee_record_id'],
                            'data_employee_record_id' =>$rows['employee_record_id'],
                            'data-office-unit-id' => $rows['office_unit_id'],
                            'data_office_unit_id' => $rows['office_unit_id'],
                            'data-office-unit-organogram-id' => $rows['office_unit_organogram_id'],
                            'data_office_unit_organogram_id' => $rows['office_unit_organogram_id'],
                            'data-office-id' => $rows['office_id'],
                            'data_office_id' => $rows['office_id']];
                        ?>
                    <tr class="designationLabel" data-search-term="<?php echo $rows['name_bng'] . ', ' . $rows['designation_bng'] . ', ' . $rows['unit_name_bng'] ; ?>">
                        <td class="text-center"><?php echo $this->Number->format(++$i) ?></td>
                        <td class="designation_id" data-office-id="<?php echo $rows['office_id'] ?>"
                            data-unit-id="<?php echo $rows['office_unit_id'] ?>"
                            data-designation-id="<?php echo $rows['office_unit_organogram_id'] ?>"
                            ><?php echo $rows['name_bng'] . ', ' . $rows['designation_bng'] . ', <b>' . $rows['unit_name_bng'] . "</b>"; ?></td>

                        <td class="text-left">
                            <?php
                            if (!empty($formatted_previous_settings[$rows['office_unit_organogram_id']]['protikolpo_1_designation_id'])) {
                                if(!empty($formatted_previous_settings[$rows['office_unit_organogram_id']]['protikolpo_1_other_designation_info'])){
                                    if(!in_array($rows['office_unit_organogram_id'],$other)) {
                                        $other[] = $rows['office_unit_organogram_id'];
                                        $row_options[] = $formatted_previous_settings[$rows['office_unit_organogram_id']]['protikolpo_1_other_designation_info'];
                                    }
                                    $selected1[]  = $formatted_previous_settings[$rows['office_unit_organogram_id']]['protikolpo_1_other_designation_info'];
                                }
                                else{
                                    $selected = array_search($formatted_previous_settings[$rows['office_unit_organogram_id']]['protikolpo_1_designation_id'], array_column($allOrganogram,'office_unit_organogram_id'));
                                    $selected1[] = ['text' => $allOrganogram[$selected]['name_bng'] . ', ' .
                                        $allOrganogram[$selected]['designation_bng'] . ', ' . $allOrganogram[$selected]['unit_name_bng'],
                            'value' => $allOrganogram[$selected]['office_unit_organogram_id'],
                            'data-employee-record-id' =>$allOrganogram[$selected]['employee_record_id'],
                            'data_employee_record_id' =>$allOrganogram[$selected]['employee_record_id'],
                            'data-office-unit-id' => $allOrganogram[$selected]['office_unit_id'],
                            'data_office_unit_id' => $allOrganogram[$selected]['office_unit_id'],
                            'data-office-unit-organogram-id' => $allOrganogram[$selected]['office_unit_organogram_id'],
                            'data_office_unit_organogram_id' => $allOrganogram[$selected]['office_unit_organogram_id'],
                            'data-office-id' => $allOrganogram[$selected]['office_id'],
                            'data_office_id' => $allOrganogram[$selected]['office_id']];

                                }
                                echo $this->Form->input('protikolpo_1',
                                    array(
                                        'label' => false,
                                        'class' => 'form-control protikolpo_1',
                                        'id'=> 'dropdown_protikolpo_1_'.$rows["office_unit_organogram_id"],
                                        'empty' => '----',
                                        'options' => $selected1,
                                        'value' => $formatted_previous_settings[$rows['office_unit_organogram_id']]['protikolpo_1_designation_id']
                                    ));
                            } else {
                                echo $this->Form->input('protikolpo_1',
                                    array(
                                        'label' => false,
                                        'class' => 'form-control protikolpo_1',
                                        'id'=> 'dropdown_protikolpo_1_'.$rows["office_unit_organogram_id"],
                                        'empty' => '----',
                                        'options' => $selected1
                                    ));
                            }
                            ?>
	                        <div class="btn-group btn-group-round">
                                <button class="btn btn-xs btn-warning other_office" id="protikolpo_1_<?= $rows['office_unit_organogram_id'] ?>">অফিসার বাছাই করুন</button>
	                        </div>

                        </td>
                        <td class="text-left" >
                            <?php
                            if (!empty($formatted_previous_settings[$rows['office_unit_organogram_id']]['protikolpo_2_designation_id'])) {

                                if(!empty($formatted_previous_settings[$rows['office_unit_organogram_id']]['protikolpo_2_other_designation_info'])){
                                    if(!in_array($rows['office_unit_organogram_id'],$other)) {
                                        $other[] = $rows['office_unit_organogram_id'];
                                        $row_options[] = $formatted_previous_settings[$rows['office_unit_organogram_id']]['protikolpo_2_other_designation_info'];
                                    }
                                    $selected2[]  = $formatted_previous_settings[$rows['office_unit_organogram_id']]['protikolpo_2_other_designation_info'];
                                }
                                else{
                                    $selected = array_search($formatted_previous_settings[$rows['office_unit_organogram_id']]['protikolpo_2_designation_id'], array_column($allOrganogram,'office_unit_organogram_id'));
                                    $selected2[] = ['text' => $allOrganogram[$selected]['name_bng'] . ', ' .
                                        $allOrganogram[$selected]['designation_bng'] . ', ' . $allOrganogram[$selected]['unit_name_bng'],
                                        'value' => $allOrganogram[$selected]['office_unit_organogram_id'],
                                        'data-employee-record-id' =>$allOrganogram[$selected]['employee_record_id'],
                                        'data_employee_record_id' =>$allOrganogram[$selected]['employee_record_id'],
                                        'data-office-unit-id' => $allOrganogram[$selected]['office_unit_id'],
                                        'data_office_unit_id' => $allOrganogram[$selected]['office_unit_id'],
                                        'data-office-unit-organogram-id' => $allOrganogram[$selected]['office_unit_organogram_id'],
                                        'data_office_unit_organogram_id' => $allOrganogram[$selected]['office_unit_organogram_id'],
                                        'data-office-id' => $allOrganogram[$selected]['office_id'],
                                        'data_office_id' => $allOrganogram[$selected]['office_id']];
                                }

                                echo $this->Form->input('protikolpo_2',
                                    array(
                                        'label' => false,
                                        'class' => 'form-control protikolpo_2',
                                        'id'=> 'dropdown_protikolpo_2_'.$rows["office_unit_organogram_id"],
                                        'empty' => '----',
                                        'options' => $selected2,
                                        'value' => $formatted_previous_settings[$rows['office_unit_organogram_id']]['protikolpo_2_designation_id']
                                    ));
                            } else {
                                echo $this->Form->input('protikolpo_2',
                                    array(
                                        'label' => false,
                                        'class' => 'form-control protikolpo_2',
                                        'id'=> 'dropdown_protikolpo_2_'.$rows["office_unit_organogram_id"],
                                        'empty' => '----',
                                        'options' => $selected2
                                    ));
                            }
                            ?>
	                        <div class="btn-group btn-group-round">
                                <button class="btn btn-xs btn-warning other_office" id="protikolpo_2_<?= $rows['office_unit_organogram_id'] ?>">অফিসার বাছাই করুন</button>
	                        </div>
                        </td>
                        <td><button class="btn btn-primary btn-sm set_protikolpo_date" id="set_date_<?= $rows['office_unit_organogram_id'] ?>" title="দায়িত্ব হস্তান্তরের তারিখ নির্বাচন করুন"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></td>

                        <?php
                        endforeach;
                        ?>
                    </tr>
                    <tr class="form-actions">
                        <td colspan="5" class="text-center">
                            <input type="button" class="btn green saveDesignation" value="ব্যবহারকারীর প্রতিকল্প সংরক্ষন"/>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="other_office_selected_designation" value="">
<input type="hidden" id="set_date_selected_designation" value="">
<!-- Other Office Modal -->
<div id="otherOfficeModal" class="modal fade modal-purple" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">অফিসার নির্বাচন করুন</h4>
            </div>
            <div class="modal-body">
                <div class="">
                    <?php
                    echo $this->cell('OfficeSearch::officeChooseProtikolpo', ['prefix' => 'sovapoti_','office_id'=>65,'is_api'=>0,'openOwnOffice'=>true]);
                    ?>
                    <div class="btn-group  saveButton"><a  class="btn green savethis margin-right-10" prefix="sovapoti_"><i class="a2i_gn_approval1"></i></a></div>
                </div>
            </div>
            <div class="modal-footer">
	            <div class="btn-group btn-group-round">
                    <button type="button" class="btn btn-default red" data-dismiss="modal">বন্ধ করুন</button>
	            </div>
            </div>
        </div>

    </div>
</div>
<div id="setDateModal" class="modal fade modal-purple" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close_button" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">দায়িত্ব হস্তান্তরের তারিখ নির্বাচন করুন</h4>
            </div>
            <div class="modal-body">
                <div class="UIblock">
                    <div class="portlet light">
                        <div class="portlet-body form">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="control-label col-md-2">ছুটির সময় </label>
                                            <div class="col-md-5">
                                                <div class="input-group input-large protikolpo-daterangepicker input-daterange" data-date-format="mm/dd/yyyy" style="width: 100% !important;">
                                                    <input type="text" id="protikolpo_start_date" class="form-control"
                                                           value=""/>
                                                    <span class="input-group-addon"> হইতে </span>
                                                    <input type="text" id="protikolpo_end_date" class="form-control" value=""> </div>
                                            </div>
                                            <div class="col-md-5">
                                                <input type="checkbox" id="protikolpo_show_acting"/>
                                                <label for="protikolpo_show_acting" class="form-inline">
                                                    পদবিতে
                                                    <select name="acting_level" class="form-control" style="width:120px;">
                                                        <?php foreach ($acting_levels as $key => $acting_level): ?>
                                                            <option value="<?=$acting_level?>"><?=$acting_level?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                    দেখাবে
                                                </label>
                                            </div>

                                            <div class="col-md-12 text-center">
                                                <br>
                                                <input type="button" class="btn green protikolpo_submit" value="<?php echo __(SAVE); ?>"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet-body">
                                <table class="table table-striped table-bordered table-hover" id="protikolpo_result">
                                    <thead>
                                        <tr class="heading">
                                            <th class="text-center">পদবি</th>
                                            <th class="text-center">অবস্থা</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
	            <div class="btn-group btn-group-round">
                    <button type="button" class="btn btn-default red close_button" data-dismiss="modal">বন্ধ করুন</button>
	            </div>
            </div>
        </div>

    </div>
</div>
<!-- Other Office Modal End-->

<script>

    $(function(){
        $('body').addClass('page-sidebar-closed');
        $('.page-sidebar-menu').addClass('page-sidebar-menu-closed');
        $(document).find('.protikolpo-daterangepicker').datepicker({
            rtl: Metronic.isRTL(),
            autoclose: true,
            format: "yyyy-mm-dd",
            startDate: '<?= date("Y-m-d",strtotime("+1 days")) ?>'
        });
    });
    $(document).on('click', '.other_office', function (e) {
        var selected_designation = $(this).attr('id');
        $("#other_office_selected_designation").val(selected_designation);
        $("#otherOfficeModal").modal('show');
    });
    $(document).on('click', '.set_protikolpo_date', function (e) {
        var selected_designation = $(this).attr('id');
        var protikolpo_1_office_unit_organogram_id = $("#"+selected_designation).closest('tr').find('.protikolpo_1 option:selected').data('office-unit-organogram-id');
        var protikolpo_2_office_unit_organogram_id = $("#"+selected_designation).closest('tr').find('.protikolpo_2 option:selected').data('office-unit-organogram-id');

        if (isEmpty(protikolpo_1_office_unit_organogram_id) && isEmpty(protikolpo_2_office_unit_organogram_id)) {
            toastr.error("অনুগ্রহ করে প্রতিকল্প নির্বাচন করুন।");
            return;
        }
        $("#set_date_selected_designation").val(selected_designation);
        $("#protikolpo_result tbody").html('')
        $("#setDateModal").modal('show');
    });

    $('.savethis').off('click');
    $('.savethis').on('click', function () {
        var prefix = $(this).attr('prefix');

            var officerid = $('input[name=' + prefix + 'officer_id]').val();
            var officername = $('input[name=' + prefix + 'officer_name]').val();
            var officerhead = $('input[name=' + prefix + 'office_head]').val();
            var officerofcid = $('select[name=' + prefix + 'office_id] option:selected').val();
            var officerofc = $('select[name=' + prefix + 'office_id] option:selected').text();
            var officerorgid = $('select[name=' + prefix + 'office_unit_organogram_id] option:selected').val();
            var officerorg = $('select[name=' + prefix + 'office_unit_organogram_id] option:selected').text() + ($('input[name=' + prefix + 'incharge_label]').val().length > 0 ? (" (" + $('input[name=' + prefix + 'incharge_label]').val() + ")") : "");
            var officerunitid = $('select[name=' + prefix + 'office_unit_id] option:selected').val();
            var officerunit = $('select[name=' + prefix + 'office_unit_id] option:selected').text();


            if (officerorgid.length == 0 || officerid.length == 0 || officerofcid.length == 0) {
                toastr.error("দুঃখিত! তথ্য সঠিক নয়");
                return false;
            } else if (officerid>0 && (isEmpty(officerunitid) || isEmpty(officerunitid) || isEmpty(officerorgid))) {
                toastr.error("দুঃখিত! তথ্য সঠিক নয়");
                return false;
            }else {
                if($('input[name=' + prefix + 'own_office]').is(':checked') == false) {
                    var option = '<option value="' + officerorgid + '" data-employee-record-id="' + officerid + '" data-office-unit-id="' + officerunitid + '" data-office-unit-organogram-id="' + officerorgid + '" data-office-id="' + officerofcid + '" data-other-office="true" selected="selected">'+officername+ ', ' + officerorg + ', ' + officerunit + ', ' + officerofc + '</option>';
                } else {
                    var option = '<option value="' + officerorgid + '" data-employee-record-id="' + officerid + '" data-office-unit-id="' + officerunitid + '" data-office-unit-organogram-id="' + officerorgid + '" data-office-id="' + officerofcid + '" selected="selected">' +officername+ ', '+ officerorg + ', ' + officerunit + '</option>';
                }

                var selected_designation = $("#other_office_selected_designation").val();

                $("#dropdown_"+selected_designation).append(option);
                $("#dropdown_"+selected_designation).select2();
            }
        $("#otherOfficeModal").modal('hide');
    });

    $('.closethis').on('click', function (e) {
        $("#otherOfficeModal").modal('hide');
    });

	$(document).on('click', '.saveDesignation', function () {
		Metronic.blockUI({
			target: '.portlet.box.green',
			boxed: true
		});
		var i = 0;
		var all_data = {};

		$('.designationLabel').each(function () {
			var protikolpo_1_office_unit_organogram_id = $(this).find('.protikolpo_1 option:selected').data('office-unit-organogram-id');
			var protikolpo_2_office_unit_organogram_id = $(this).find('.protikolpo_2 option:selected').data('office-unit-organogram-id');

			if (isEmpty(protikolpo_1_office_unit_organogram_id) && isEmpty(protikolpo_2_office_unit_organogram_id)) {
				return;
			}

			if(!isEmpty(protikolpo_1_office_unit_organogram_id)
                && $(this).find('.designation_id').data('designation-id') != protikolpo_1_office_unit_organogram_id) {
				var protikolpo_1_employee_record_id = $(this).find('.protikolpo_1 option:selected').data('employee-record-id');
				var protikolpo_1_office_unit_id = $(this).find('.protikolpo_1 option:selected').data('office-unit-id');
				var protikolpo_1_office_id = $(this).find('.protikolpo_1 option:selected').data('office-id');
				var other_officer_is_true = $(this).find('.protikolpo_1 option:selected').data('other-office');
				if(other_officer_is_true ==true){
				    var other_officer_info = $(this).find('.protikolpo_1 option:selected').text();
                }
			}else{
				protikolpo_1_office_unit_organogram_id = 0;
            }
			var protikolpo_1 = {
				'office_id': isEmpty(protikolpo_1_office_id) ? 0 : protikolpo_1_office_id,
				'employee_record_id': isEmpty(protikolpo_1_employee_record_id) ? 0 : protikolpo_1_employee_record_id,
				'office_unit_id': isEmpty(protikolpo_1_office_unit_id) ? 0 : protikolpo_1_office_unit_id,
				'designation_id': isEmpty(protikolpo_1_office_unit_organogram_id) ? 0 : protikolpo_1_office_unit_organogram_id,
                'other_officer_info':isEmpty(other_officer_info)?'':other_officer_info
			};

			if(!isEmpty(protikolpo_2_office_unit_organogram_id)
				&& $(this).find('.designation_id').data('designation-id') != protikolpo_2_office_unit_organogram_id) {
				var protikolpo_2_employee_record_id = $(this).find('.protikolpo_2 option:selected').data('employee-record-id');
				var protikolpo_2_office_unit_id = $(this).find('.protikolpo_2 option:selected').data('office-unit-id');
				var protikolpo_2_office_id = $(this).find('.protikolpo_2 option:selected').data('office-id');
                var other_officer_is_true = $(this).find('.protikolpo_2 option:selected').data('other-office');
                if(other_officer_is_true ==true){
                    var other_officer_info_2 = $(this).find('.protikolpo_2 option:selected').text();
                }
			}else{
				protikolpo_2_office_unit_organogram_id = 0;
            }
			var protikolpo_2 = {
				'office_id': isEmpty(protikolpo_2_office_id) ? 0 : protikolpo_2_office_id,
				'employee_record_id': isEmpty(protikolpo_2_employee_record_id) ? 0 : protikolpo_2_employee_record_id,
				'office_unit_id': isEmpty(protikolpo_2_office_unit_id) ? 0 : protikolpo_2_office_unit_id,
				'designation_id': isEmpty(protikolpo_2_office_unit_organogram_id) ? 0 : protikolpo_2_office_unit_organogram_id,
                'other_officer_info':isEmpty(other_officer_info_2)?'':other_officer_info_2
			};

			if(protikolpo_1.designation_id != protikolpo_2.designation_id) {
				var protikolpos = {'protikolpo_1': protikolpo_1, 'protikolpo_2': protikolpo_2};
			}else{
				var protikolpos = {'protikolpo_1': protikolpo_1, 'protikolpo_2': {
					'office_id': 0,
					'employee_record_id': 0,
					'office_unit_id': 0,
					'designation_id': 0,
                    'other_officer_info':''
                }};
            }

			all_data[i] = {
				'office_id': $(this).find('.designation_id').data('office-id'),
				'unit_id': $(this).find('.designation_id').data('unit-id'),
				'designation_id': $(this).find('.designation_id').data('designation-id'),
				'protikolpos': protikolpos
			};
			i++;
		});


			PROJAPOTI.ajaxSubmitAsyncDataCallback('<?php echo $this->Url->build(['controller' => 'ProtikolpoSettings', 'action' => 'setProtikolpoSettings']) ?>',
				{data: all_data}, 'json', function (res) {
					if (res.status == 'success') {
						toastr.success("প্রতিকল্প ব্যবস্থাপনা সংরক্ষণ করা হয়েছে।");
						Metronic.unblockUI('.portlet.box.green');
						setTimeout(function () {
							window.location.reload()
						}, 1000)
					} else {
						toastr.error("দুঃখিত! সংরক্ষণ করা সম্ভব হচ্ছে না। কিছুক্ষন পরে আবার চেস্টা করুন।");
						Metronic.unblockUI('.portlet.box.green');
					}
				})
	});

    $(document).off('click','.protikolpo_submit').on('click', '.protikolpo_submit', function (e) {
        var selected_designation = "#"+$("#set_date_selected_designation").val();

        var protikolpo_start_date =$('#protikolpo_start_date').val();
        var protikolpo_end_date = $('#protikolpo_end_date').val();
        var protikolpo_show_acting = 0;
        var protikolpo_show_acting_level = '';
        if ($("#protikolpo_show_acting").is(':checked')) {
            protikolpo_show_acting = 1;
            protikolpo_show_acting_level = $("select[name=acting_level]").val();
        }
        if(isEmpty(protikolpo_start_date) || isEmpty(protikolpo_end_date)){
            toastr.error("অনুগ্রহ করে দায়িত্ব হস্তান্তরের তারিখ নির্বাচন করুন।");
            return;
        }

        if(Date.parse(protikolpo_start_date) > Date.parse(protikolpo_end_date)){
            toastr.error("অনুগ্রহ করে সঠিক তারিখ সীমা নির্বাচন করুন।");
            return;
        }

        var protikolpo_1_office_unit_organogram_id = $(selected_designation).closest('tr').find('.protikolpo_1 option:selected').data('office-unit-organogram-id');
        var protikolpo_2_office_unit_organogram_id = $(selected_designation).closest('tr').find('.protikolpo_2 option:selected').data('office-unit-organogram-id');

        if (isEmpty(protikolpo_1_office_unit_organogram_id) && isEmpty(protikolpo_2_office_unit_organogram_id)) {
            toastr.error("অনুগ্রহ করে প্রতিকল্প নির্বাচন করুন।");
            return;
        }

        if(!isEmpty(protikolpo_1_office_unit_organogram_id)
            && $(selected_designation).find('.designation_id').data('designation-id') != protikolpo_1_office_unit_organogram_id) {
            var protikolpo_1_employee_record_id = $(selected_designation).closest('tr').find('.protikolpo_1 option:selected').data('employee-record-id');
            var protikolpo_1_office_unit_id = $(selected_designation).closest('tr').find('.protikolpo_1 option:selected').data('office-unit-id');
            var protikolpo_1_office_id = $(selected_designation).closest('tr').find('.protikolpo_1 option:selected').data('office-id');
            var other_officer_is_true = $(selected_designation).closest('tr').find('.protikolpo_1 option:selected').data('other-office');
            if(other_officer_is_true ==true){
                var other_officer_info = $(selected_designation).closest('tr').find('.protikolpo_1 option:selected').text();
            }
        }else{
            protikolpo_1_office_unit_organogram_id = 0;
        }
        var protikolpo_1 = {
            'office_id': isEmpty(protikolpo_1_office_id) ? 0 : protikolpo_1_office_id,
            'employee_record_id': isEmpty(protikolpo_1_employee_record_id) ? 0 : protikolpo_1_employee_record_id,
            'office_unit_id': isEmpty(protikolpo_1_office_unit_id) ? 0 : protikolpo_1_office_unit_id,
            'designation_id': isEmpty(protikolpo_1_office_unit_organogram_id) ? 0 : protikolpo_1_office_unit_organogram_id,
            'other_officer_info':isEmpty(other_officer_info)?'':other_officer_info
        };

        if(!isEmpty(protikolpo_2_office_unit_organogram_id)
            && $(selected_designation).closest('tr').find('.designation_id').data('designation-id') != protikolpo_2_office_unit_organogram_id) {
            var protikolpo_2_employee_record_id = $(selected_designation).closest('tr').find('.protikolpo_2 option:selected').data('employee-record-id');
            var protikolpo_2_office_unit_id = $(selected_designation).closest('tr').find('.protikolpo_2 option:selected').data('office-unit-id');
            var protikolpo_2_office_id = $(selected_designation).closest('tr').find('.protikolpo_2 option:selected').data('office-id');
            var other_officer_is_true = $(selected_designation).closest('tr').find('.protikolpo_2 option:selected').data('other-office');
            if(other_officer_is_true ==true){
                var other_officer_info_2 = $(selected_designation).closest('tr').find('.protikolpo_2 option:selected').text();
            }
        }else{
            protikolpo_2_office_unit_organogram_id = 0;
        }
        var protikolpo_2 = {
            'office_id': isEmpty(protikolpo_2_office_id) ? 0 : protikolpo_2_office_id,
            'employee_record_id': isEmpty(protikolpo_2_employee_record_id) ? 0 : protikolpo_2_employee_record_id,
            'office_unit_id': isEmpty(protikolpo_2_office_unit_id) ? 0 : protikolpo_2_office_unit_id,
            'designation_id': isEmpty(protikolpo_2_office_unit_organogram_id) ? 0 : protikolpo_2_office_unit_organogram_id,
            'other_officer_info':isEmpty(other_officer_info_2)?'':other_officer_info_2
        };

        if(protikolpo_1.designation_id != protikolpo_2.designation_id) {
            var protikolpos = {'protikolpo_1': protikolpo_1, 'protikolpo_2': protikolpo_2};
        }else{
            var protikolpos = {'protikolpo_1': protikolpo_1, 'protikolpo_2': {
                    'office_id': 0,
                    'employee_record_id': 0,
                    'office_unit_id': 0,
                    'designation_id': 0,
                    'other_officer_info':''
                }};
        }

        all_data = {
            'office_id': $(selected_designation).closest('tr').find('.designation_id').data('office-id'),
            'unit_id': $(selected_designation).closest('tr').find('.designation_id').data('unit-id'),
            'designation_id': $(selected_designation).closest('tr').find('.designation_id').data('designation-id'),
            'protikolpos': protikolpos
        };


        $(".close_button").attr("disabled", true);
        $(".protikolpo_submit").attr("disabled", true);
        checkAvailablity(protikolpo_start_date,protikolpo_end_date, protikolpo_show_acting,all_data, protikolpo_show_acting_level);
    });

    $("#search").on("keyup", function () {
        var text = $(this).val().toLowerCase();
        $("table tr").each(function () {
            var data = $(this).attr('data-search-term');
            if (typeof(data) == 'undefined') {
                return;
            }
            if ($(this).filter('[data-search-term *= ' + text + ']').length > 0 || text.length < 1) {
                $(this).closest('tr').show();
            }
            else {
                $(this).closest('tr').hide();
            }
        });
    });
    function checkAvailablity(start_date,end_date, show_active,data,protikolpo_show_acting_level) {
        $.ajax({
            url: '<?php echo $this->Url->build(['controller'=>'ProtikolpoSettings','action'=>'setLeaveDateByAdmin']) ?>',
            data: {start_date:start_date,end_date:end_date, show_active:show_active,data:data,acting_level:protikolpo_show_acting_level},
            method: 'post',
            success: function(res){

                if(res.status == 'success') {
                    $("#protikolpo_result tbody").html('');
                    $.each(res.data,function (i,v) {
                        var newRowContent = '';

                        if(v.protikolpo_1==1){
                            var p_info = 'প্রতিকল্প ১ (<b>'+v.protikolpo_1_info +'</b>) নির্বাচন করা হয়েছে।';
                        } else if(v.protikolpo_2 == 1){
                            var p_info ='প্রতিকল্প ২ (<b>'+v.protikolpo_2_info +'</b>) নির্বাচন করা হয়েছে।';
                        } else if(v.protikolpo_2 == 0 && v.protikolpo_1 == 0){
                            var p_info = 'সকল প্রতিকল্প এ সময় ছুটিতে রয়েছে।';
                        } else {
                            var p_info ='প্রতিকল্প পাওয়া যায় নি।';
                        }
                        newRowContent  = '<tr><td>'+ v.designation_bng +'</td><td class="text-center">'+p_info+'</td></tr>';
                        $("#protikolpo_result tbody").append(newRowContent);
                    });
                    toastr.success("প্রতিকল্প ব্যবস্থাপনা সংরক্ষণ করা হয়েছে।");
                    $(".close_button").attr("disabled", false);
                    $(".protikolpo_submit").attr("disabled", false);
                } else {
                    toastr.error("দুঃখিত! সংরক্ষণ করা সম্ভব হচ্ছে না। কিছুক্ষন পরে আবার চেস্টা করুন।");
                    $(".close_button").attr("disabled", false);
                    $(".protikolpo_submit").attr("disabled", false);
                }
            }
        });
    }

</script>