<style>
    #addHear{
        font-size:12px!important;
    }
    .inbox-content tr td{
        font-size:10px!important;
        padding: 0px!important;
    }
    .inbox-content tr td li{
        font-size:10px!important;
        padding: 5px 5px!important;
    }
    .list-group{
        margin-bottom:  0px;
    }
    @media print {
         a[href]:after {
            content: "";
        }
        body{
            margin-top: 0px!important;
            padding-top: 0px!important;
        }
    }
</style>
<div class="portlet light">
    <div class="portlet-title hidden-print">
        <div class="caption"><i class="fs1 a2i_gn_details1"></i> মনিটরিং ড্যাশবোর্ড মাসিক রিপোর্ট </div>
    </div>
    <div class="portlet-body">
        <div class="table-toolbar">
            <div class="row">
                <div class="col-md-4 form-group form-horizontal">
                    <label class="control-label col-md-2 text-right font-lg" > বছরঃ </label>
                    <?php
                    echo $this->Form->year('year',
                        [
                        'minYear' => 2017,
                        'maxYear' => date('Y'),
                        'class' => 'form-control',
                        'id' => 'year'
                    ]);
                    ?>
                </div>
                <div class="col-md-4 form-group form-horizontal">
                    <label class="control-label col-md-2 text-right font-lg" > মাসঃ </label>
                    <?php
                    echo $this->Form->month('mob', [ 'class' => 'form-control', 'id' => 'month']);
                    ?>
                </div>
                <div class="col-md-4 form-group form-horizontal">
                    <button class="btn   btn-primary margin-top-20" id="export"> রিপোর্ট তৈরি করুন </button>
                </div>
            </div>
            <div class="row">
                <div id="showlist">
                   
                </div>
                <div class="print-portion">
                    <table class="table table-bordered">
                        <thead id="addHeader">
                            <tr class="heading">
                                <th class="text-center" > ক্রম </th>
                                <th class="text-center" >তারিখ </th>
                                <th class="text-center" >সর্বমোট অফিস </th>
                                <th class="text-center" >ব্যর্থ হয়েছে </th>
                                <th class="text-center" > বিস্তারিত </th>
                            </tr>
                        </thead>
                        <tbody class="inbox-content">
                        </tbody>
                    </table>

                </div>
            </div>
        </div>

    </div>
</div>
<script src="<?=$this->request->webroot?>assets/global/scripts/printThis.js"></script>
<script src="<?php echo CDN_PATH; ?>js/reports/generate_monitoring_monthly_report.js?v=<?= js_css_version ?>" type="text/javascript"></script>