<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
           <span aria-hidden="true" class="icon-book-open"></span> অফিসভিত্তিক পত্রজারি পেন্ডিং
        </div>
    </div>
    <div class="portlet-body">
        <div class="row">
            <?= $this->Cell('OfficeSelection'); ?>
            <div class="row">
                <div class="col-md-4 col-sm-8 form-group form-horizontal">
                    <label class="control-label"> <?php echo __("Office") ?> </label>
                    <?php
                    echo $this->Form->hidden('start_date', ['class' => 'startdate']);
                    echo $this->Form->hidden('end_date', ['class' => 'enddate']);
                    echo $this->Form->input('office_id',
                        array(
                        'label' => false,
                        'class' => 'form-control',
                        'empty' => '----'
                    ));
                    ?>
                </div>
                           <!--Tools end -->

            <!--  Date Range Begain -->
            <div class="col-md-8 col-sm-4 pull-right margin-top-20" >
                <div class="hidden-print page-toolbar pull-right portlet-title">
                    <div id="dashboard-report-range" class=" tooltips btn btn-sm btn-default"
                         data-container="body"
                         data-placement="bottom" data-original-title="তারিখ নির্বাচন করুন">
                        <i class="icon-calendar"></i>&nbsp; <span
                            class="thin uppercase visible-lg-inline-block">তারিখ নির্বাচন করুন</span>&nbsp;
                        <i class="glyphicon glyphicon-chevron-down"></i>
                    </div>
                </div>
            </div>
            <!--  Date Range End  -->

            </div>
        </div>
        <div class="row" id="showlist_receiver">

        </div>
        <div class="row" id="showlist_onulipi">

        </div>
    </div>
</div>
<script src="<?php echo CDN_PATH; ?>daptorik_preview/js/report_date_range.js"></script>
<script src="<?php echo CDN_PATH; ?>js/reports/search_by_date_range_office_id.js" type="text/javascript"></script>
<script>
    jQuery(document).ready(function () {

        $(this).find('body').addClass('page-sidebar-closed');
        $(this).find('.page-sidebar-menu').addClass('page-sidebar-menu-closed');
        DateRange.init();
        DateRange.initDashboardDaterange();
        var EmployeeLoginHistory = {
            loadMinistryWiseLayers: function () {
                OfficeSetup.loadLayers($("#office-ministry-id").val(), function () {

                });
            },
            loadMinistryAndLayerWiseOfficeOrigin: function () {
                OfficeSetup.loadOffices($("#office-ministry-id").val(), $("#office-layer-id").val(), function () {

                });
            },
            loadOriginOffices: function () {
                OfficeSetup.loadOriginOffices($("#office-origin-id").val(), function () {

                });
            },
        };
        $("#office-ministry-id").bind('change', function () {
            EmployeeLoginHistory.loadMinistryWiseLayers();
        });
        $("#office-layer-id").bind('change', function () {
            EmployeeLoginHistory.loadMinistryAndLayerWiseOfficeOrigin();
        });
        $("#office-origin-id").bind('change', function () {
            EmployeeLoginHistory.loadOriginOffices();
        });
        $("#office-id").bind('change', function () {
            SortingDate.init($('.startdate').val(), $('.enddate').val());
        });
    });
    function  loadData(office_id,start_date,end_date) {
        Metronic.blockUI({target: '.portlet-body', boxed: true});
        $('#showlist_receiver').html('');
        $('#showlist_showlist_onulipi').html('');
        $.ajax({
            type: 'POST',
            url: "<?php echo $this->Url->build(['controller' => 'Report', 'action' => 'generatePotrojariReceiverPending']) ?>",
            data: {'office_id': office_id,'start_date' : start_date,'end_date' : end_date},
            success: function (data) {
                Metronic.unblockUI('.portlet-body');
                $('#showlist_receiver').html(data);
                loadOnulipi(office_id,start_date,end_date);
            },
            error: function () {
                Metronic.unblockUI('.portlet-body');
            }
        });
    }
    function loadOnulipi(office_id,start_date,end_date){
    Metronic.blockUI({target: '#showlist_onulipi', boxed: true});
    $.ajax({
            type: 'POST',
            url: "<?php echo $this->Url->build(['controller' => 'Report', 'action' => 'generatePotrojariOnulipiPending']) ?>",
            data: {'office_id': office_id,'start_date' : start_date,'end_date' : end_date},
            success: function (data) {
                 Metronic.unblockUI('#showlist_onulipi');
                $('#showlist_onulipi').html(data);
            },
            error: function () {
                 Metronic.unblockUI('#showlist_onulipi');
            }
        });
    }
</script>

