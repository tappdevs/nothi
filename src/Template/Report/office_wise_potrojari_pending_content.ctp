<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <span aria-hidden="true" class="icon-book-open"></span> পত্রজারি <?= $header ?> পেন্ডিং
        </div>
         <div class="actions">
             <button type="button"  class="btn btn-danger btn-sm" onclick="resendPotrojariAll()"><i class="fa fa-repeat"></i>  <?= __('Resend') .' ( '.__("All").' )' ?> </button>
        </div>
    </div>
    <div class="portlet-body">
        <div class="">
            <table class="table table-bordered table-hover tableadvance">
                <thead>
                    <tr class="heading">
                        <th  class="text-center"> <?= __('Sequence') ?> </th>
                        <th  class="text-center" style="width : 20%!important;"> <?= __('Subject') ?> </th>
                        <th  class="text-center"  style="width : 20%!important;"> <?= __('Receiver') ?> </th>
                        <th  class="text-center"  style="width : 10%!important;"> <?= __('Email') ?> </th>
                        <th  class="text-center"> <?= __('Group') ?> </th>
                        <th  class="text-center"> <?= __('Status') ?> </th>
                        <th  class="text-center"> <?= __('Created') ?> </th>
                        <th  class="text-center"> পত্রজারি হয়েছে </th>
                        <th  class="text-center"> <?= __('Actions') ?> </th>
                    </tr>
                </thead>
                <tbody >
                    <?php
                    $indx = 1;
                    if (isset($this->Paginator->request->query['page'])) {
                        $indx = ($this->Paginator->request->query['page'] - 1 ) * 20 + 1;
                    }
                    $last_potrojari_id = 0;
                    if ($response['status'] == 1 && !empty($response['data']->toArray())) {
                        foreach ($response['data'] as $data) {
                            if(!empty($data['created'])){
                                $data['created'] = new \Cake\I18n\Time($data['created']);
                                $data['potrojari_date'] = new \Cake\I18n\Time($data['created']);
                            }

                            ?>
                            <tr style="word-break:break-word!important;">
                                <td class="text-right"><?= entobn($indx++); ?> </td>
                                <td class="text-right"><?= $data['potro_subject'] ?> </td>
                                <td class="text-right"><?=
                                    (!empty($data['receiving_officer_name']) ? $data['receiving_officer_name']
                                            : '')
                                    .(!empty($data['receiving_officer_designation_label']) ? '<br>'.$data['receiving_officer_designation_label']
                                            : '')
                                    .(!empty($data['receiving_office_unit_name']) ? ' ,'.$data['receiving_office_unit_name']
                                            : '')
                                    .(!empty($data['receiving_office_name']) ? ' ,'.$data['receiving_office_name']
                                            : '')
                                    ?>
                                </td>
                                <td class="text-right"><?= $data['receiving_officer_email'] ?> </td>
                                <td class="text-right"><?= $data['group_name'] ?> </td>
                                <td class="text-right"><?= $data['task_reposponse'] ?> </td>
                                <td class="text-right"><?= !empty($data['created'])?$data['created']:'';?> </td>
                                <td class="text-right"><?= !empty($data['potrojari_date'])?$data['potrojari_date']:''; ?> </td>
                                <td class="text-center" id="<?=$data['office_id'].'_'.$data['potrojari_id'].'_'.$data['id']?>">
                                    <?php
                                        if($last_potrojari_id != $data['potrojari_id']):
                                    ?>
                                        <button type="button"  class="btn btn-primary btn-sm  p_button" onclick="resendPotrojari('<?=$data['office_id']?>','<?=$data['potrojari_id']?>','<?= $from ?>','<?= $data['id'] ?>')"><i class="fa fa-repeat"></i>  <?= __('Resend') ?> </button>
                                            <?php
                                            endif;
                                    $last_potrojari_id = $data['potrojari_id'];
                                    ?>
                                </td>
                            </tr>
                            <?php
                        }
                    }
                    else {
                        ?>
                        <tr><td colspan="9"> কোন তথ্য পাওয়া যায়নি। </td></tr>
    <?php
}
?>
                </tbody>
            </table>
        </div>
        <br>

        <div class="actions text-center <?= $from ?>">
            <ul class="pagination pagination-sm">
                <?php
                echo $this->Paginator->first(__('প্রথম', true), array('class' => 'number-first'));
                echo $this->Paginator->prev('<<', array('tag' => 'li', 'escape' => false),
                    '<a href="#" class="btn btn-sm blue">&laquo;</a>',
                    array('class' => 'prev disabled btn btn-sm blue', 'tag' => 'li', 'escape' => false));
                echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true,
                    'currentClass' => 'active', 'currentTag' => 'a', 'reverse' => FALSE));
                echo $this->Paginator->next('>>', array('tag' => 'li', 'escape' => false),
                    '<a href="#" class="btn btn-sm blue ">&raquo;</a>',
                    array('class' => 'next disabled', 'tag' => 'li', 'escape' => false));
                echo $this->Paginator->last(__('শেষ', true), array('class' => 'number-last'));
                ?>
            </ul>

        </div>
    </div>
</div>

<script>
    jQuery(document).ready(function () {
        $(document).off('click', '.Receiver .pagination a').on('click', '.Receiver .pagination a', function (ev) {
            ev.preventDefault();
            PROJAPOTI.ajaxSubmitAsyncDataCallback($(this).attr('href'), {'office_id': '<?= $office_id ?>', 'start_date': '<?= $time[0] ?>', 'end_date': '<?= $time[1] ?>'}, 'html', function (response) {
                $('#showlist_receiver').html(response);
            });
        });
        $(document).off('click', '.Onulipi .pagination a').on('click', '.Onulipi .pagination a', function (ev) {
            ev.preventDefault();
            PROJAPOTI.ajaxSubmitAsyncDataCallback($(this).attr('href'), {'office_id': '<?= $office_id ?>', 'start_date': '<?= $time[0] ?>', 'end_date': '<?= $time[1] ?>'}, 'html', function (response) {
                $('#showlist_onulipi').html(response);
            });
        });
    });
    function resendPotrojari(ofc_id,potro_id,param,id){
    Metronic.blockUI({target: '.portlet-body', boxed: true});
    $("#"+ofc_id+'_'+potro_id+'_'+id).html('<i class="fa fa-spin">Pending...</i>');
          PROJAPOTI.ajaxSubmitAsyncDataCallbackWithoutAbrot('<?= $this->Url->build(['controller' => 'Potrojari', 'action' => 'makeCronRequestforPotrojari']); ?>', {'office_id': ofc_id, 'potrojari_id': potro_id, 'param': param }, 'json', function (response) {
                if(response.status == 1){
                    $("#"+ofc_id+'_'+potro_id+'_'+id).html(response.msg);
                     Metronic.unblockUI('.portlet-body');
//                    toastr.success(response.msg);
                }
                else{
                    toastr.error(response.msg);
                     Metronic.unblockUI('.portlet-body');
                }
            });
    }
    function resendPotrojariAll(){
      $('.p_button').each(function(){
          $(this).click();
        });
    }
</script>