<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0" />
<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-toastr/toastr.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>projapoti-nothi/css/styles.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>assets/global/plugins/jquery-multi-select/css/multi-select.css"/>

<style>
    .btn-changelog, .btn-forward-nothi,.btn-nothiback ,.btn-print{
        padding: 3px 5px !important;

    }

    .btn-icon-only {

    }

    .editable {
        border:none!important;
        word-break:break-word;
        word-wrap:break-word
    }

    #sovapoti_signature,#sender_signature, #sender_signature2, #sender_signature3{
        visibility: hidden;
    }
    #potrojariDraftForm [alt=signature]{
        visibility: hidden;
    }
    #note{
        overflow: hidden;
        word-break:break-word;
        word-wrap: break-word;
        height: 100%;
    }

    .editable-click, a.editable-click {
        border: none;
        word-break:break-word;
        word-wrap:break-word
    }
    .cc_list {
        white-space: pre-wrap;
    }

    .popover-content {
        padding: 10px 30px;
    }
    .mega-menu-dropdown > .dropdown-menu{
        top:10px!important;
    }
</style>

<?php if ($privilige_type == 1): ?>
    <div class="row ">
        <div class="col-md-12 " style="margin: 0 auto;">

            <div class="form">

                <?php echo $this->Form->create('',
                    array('onsubmit' => 'return false;', 'type' => 'file', 'id' => 'potrojariDraftForm'));
                ?>
                <?php
                echo $this->Form->hidden('id');
                echo $this->Form->hidden('dak_type',
                    array('label' => false, 'class' => 'form-control', 'value' => DAK_DAPTORIK));
                echo $this->Form->hidden('is_summary_nothi',
                    array('label' => false, 'class' => 'form-control', 'value' => 1));
                echo $this->Form->hidden('dak_status',
                    array('label' => false, 'class' => 'form-control', 'value' => 1));
                echo $this->Form->hidden('dak_subject',
                    array('label' => false, 'class' => 'form-control'));
                echo $this->Form->hidden('sender_sarok_no',
                    array('label' => false, 'class' => 'form-control'));
                echo $this->Form->hidden('inline_attachment',
                    array('label' => false, 'class' => 'form-control'));
                ?>
                <?php echo $this->Cell('Potrojari::sarNothiDraft',
                    ['template' => $template_list, array()])
                ?>


    <?php // echo $this->Form->end(); ?>
                <div class="form-body sonjukti hide">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tabbable-line">
                                <ul class="nav nav-tabs ">
                                    <li class="active">
                                        <a href="#tab_prapto_potro" data-toggle="tab" aria-expanded="true">
                                            প্রাপ্ত পত্রসমূহ </a>
                                    </li>
                                    <li class="">
                                        <a href="#tab_other_potro" data-toggle="tab" aria-expanded="false">
                                            অন্যান্য সংযুক্তি</a>
                                    </li>

                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_prapto_potro">
                                        <?php
                                        if (!empty($potroAttachmentRecord)) {
                                            echo '<div id="summary_potro_attachment_input_form" >
                                            <table role="presentation" class="table table-striped clearfix">
                                                <tbody >';
                                            foreach ($potroAttachmentRecord as $key => $value) {
//                                                echo '<a href="javascript:;" class="btn btn-xs blue songlap-button" id="'.$key.'"> '.str_replace(';', '-', $value).'&nbsp;<i class="fa fa-link?"></i></a>&nbsp;';
                                               echo '<tr><td width="50%" class="text-center">'.str_replace(';',
                                                       '-', $value).'&nbsp;</td>
                                                <td width="50%" style="min-width: 100px!important;">
                                                <input type="text" class="form-control summary-potro-attachment-input" id="summary-potro-attachment-input" onkeyup="check_potro_title()" potro_id="'.$key.'" placeholder="সংলাগের নাম লিখুন">
                                                </td></tr>';
                                            }
                                            echo '</tbody></table></div>';
                                        }
                                        ?>
                                    </div>
                                    <?php echo $this->Form->end(); ?>

                                    <div class="tab-pane" id="tab_other_potro">
                                        <form id="fileuploadpotrojari" action="<?= $this->Url->build(['_name'=>'tempUpload']) ?>" method="POST" enctype="multipart/form-data">

                                            <input type="hidden" name="attachment_refs_url" value="<?php echo $this->Url->build(['controller' => 'NothiMasters', 'action' => 'save_nothi_attachment_refs']) ?>" />
                                            <input type="hidden" name="module_type" value="Nothi" />
                                            <input type="hidden" name="part_no" value="<?= $nothiMasterInfo['id'] ?>" />
                                            <input type="hidden" name="nothi_masters_id" value="<?= $nothiMasterInfo['nothi_masters_id'] ?>" />
                                            <input type="hidden" name="office_id" value="<?= $officeid ?>" />
                                            <input type="hidden" name="module" value="Potrojari" />

                                            </br>
                                            <div class="row fileupload-buttonbar">
                                                <div class="col-lg-12">
                                                    <!-- The fileinput-button span is used to style the file input field as button -->
                                                    <span class="btn green fileinput-button" title="সর্বোচ্চ ফাইল সাইজ ২০ এমবি হবে" data-toggle="tooltip">

                                                        <i class="fs1 a2i_gn_add1"></i>
                                                        <span>
                                                            ফাইল যুক্ত করুন </span>
                                                        <input type="file" name="files[]" multiple="">
                                                    </span>

                                                    <!-- The global file processing state -->
                                                    <span class="fileupload-process">
                                                    </span>
                                                </div>
                                                <!-- The global progress information -->
                                                <div class="col-lg-5 fileupload-progress fade">
                                                    <!-- The global progress bar -->
                                                    <div class="progress progress-striped active"
                                                         role="progressbar"
                                                         aria-valuemin="0" aria-valuemax="100">
                                                        <div class="progress-bar progress-bar-success"
                                                             style="width:0%;">
                                                        </div>
                                                    </div>
                                                    <!-- The extended global progress information -->
                                                    <div class="progress-extended">
                                                        &nbsp;
                                                    </div>
                                                </div>

                                            </div>
                                            <!-- The table listing the files available for upload/download -->
                                            <table role="presentation" class="table table-striped clearfix">
                                                <tbody class="files">

                                                </tbody>
                                            </table>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-actions">
                    <input type="button" class="btn btn-primary " onclick="DRAFT_FORM.saveDraft()" value="<?php echo __(SAVE) ?>" />
                </div>
            </div>
        </div>
    </div>

    <script src="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-toastr/toastr.min.js"></script>
    <script src="<?php echo CDN_PATH; ?>assets/admin/pages/scripts/ui-toastr.js"></script>

    <script src="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2.js" type="text/javascript"></script>
    <script src="<?php echo CDN_PATH; ?>assets/admin/layout4/scripts/dak_setup.js" type="text/javascript"></script>
    <script src="<?php echo  $this->request->webroot; ?>projapoti-nothi/js/potrojari_file_update.js" type="text/javascript"></script>

    <script type="text/javascript" src="<?php echo $this->request->webroot; ?>assets/global/plugins/jquery.mockjax.js"></script>

    <script src="<?php echo $this->request->webroot; ?>projapoti-nothi/js/sarnothi_editable.js" type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo  $this->request->webroot; ?>assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
    <?= $this->element('froala_editor_js_css') ?>

    <?php $template_keys = array_keys($template_list); ?>
    <script>
        $('#prapto-potro').multiSelect({dblClick: true});
                        var numbers = {
                            1: '১',
                            2: '২',
                            3: '৩',
                            4: '৪',
                            5: '৫',
                            6: '৬',
                            7: '৭',
                            8: '৮',
                            9: '৯',
                            0: '০'
                        };

                        function replaceNumbers(input) {
                            var output = [];
                            for (var i = 0; i < input.length; ++i) {
                                if (numbers.hasOwnProperty(input[i])) {
                                    output.push(numbers[input[i]]);
                                } else {
                                    output.push(input[i]);
                                }
                            }
                            return output.join('');
                        }


        $("#potro-type").change(function (e) {
            $('#potrojariDraftForm').find("#template-body2").show("slow");
            $('#potrojariDraftForm').find("#template-body2").html('<img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading text-center"><span>&nbsp;&nbsp;লোড করা হচ্ছে...</span>');
            $.ajax({
                url: js_wb_root + 'potrojariTemplates/getTemplate/' + $("#potro-type").val(),
                dataType: 'JSON',
                success: function (response) {
                    if (typeof response.html_content != 'undefined') {
                        $('#potrojariDraftForm').find('#template-body2').html(response.html_content);

                        SarNothiFormEditable.init('<?php echo $nothiMasterInfo['nothi_no'] ?>');

                        $('#potrojariDraftForm').find('#sarnothi_for').text($("#potro-type option:selected").text());
                        $('#potrojariDraftForm').find('.montronaloy').text('<?php echo $ministryName->name_bng; ?>');


                        <?php if (!empty($getSochibInformation['name_bng'])): ?>
                        $('#potrojariDraftForm').find('#sochib_name').text('<?php echo(!empty($getSochibInformation['name_bng'])
                            ? $getSochibInformation['name_bng'] : '')
                            ?>');
                        $('#potrojariDraftForm').find('#sochib_designation').text('<?php echo(!empty($getSochibInformation['designation'])
                            ? $getSochibInformation['designation'] : '')
                            ?>');
                        SarNothiFormEditable.encodeImage('<?= $getSochibInformation['username'] ?>', '<?= $getSochibInformation['en_username'] ?>', function (src) {
                            $('#potrojariDraftForm').find('#sochib_signature').html('<img src="' + src + '" alt="signature" width="100" style="height: 50px;" />');
                        });
                        <?php endif; ?>

                        <?php if (!empty($getProtiMontriInformation['name_bng'])): ?>
                        $('#potrojariDraftForm').find('#second_receiver_name').text('<?php echo(!empty($getProtiMontriInformation['name_bng'])
                            ? $getProtiMontriInformation['name_bng'] : '')
                            ?>');
                        $('#potrojariDraftForm').find('#second_receiver_designation').text('<?php echo(!empty($getProtiMontriInformation['designation'])
                            ? $getProtiMontriInformation['designation'] : '')

                            ?>');
                        SarNothiFormEditable.encodeImage('<?= $getProtiMontriInformation['username'] ?>','<?= $getProtiMontriInformation['en_username'] ?>', function (src) {
                            $('#potrojariDraftForm').find('#second_receiver_signature').html('<img src="' + src + '" alt="signature" width="100" style="height: 50px;" />');
                        });
                        <?php endif; ?>

                        <?php if (!empty($getUpoMontriInformation['name_bng'])): ?>
                        $('#potrojariDraftForm').find('#upomontri_receiver_name').text('<?php echo(!empty($getUpoMontriInformation['name_bng'])
                            ? $getUpoMontriInformation['name_bng'] : '')
                            ?>');
                        $('#potrojariDraftForm').find('#upomontri_receiver_designation').text('<?php echo(!empty($getUpoMontriInformation['designation'])
                            ? $getUpoMontriInformation['designation'] : '')
                            ?>');
                        SarNothiFormEditable.encodeImage('<?= $getUpoMontriInformation['username'] ?>','<?= $getUpoMontriInformation['en_username'] ?>', function (src) {
                            $('#potrojariDraftForm').find('#upomontri_receiver_signature').html('<img src="' + src + '" alt="signature" width="100" style="height: 50px;" />');
                        });
                        <?php endif; ?>

                        <?php if (!empty($getMontriInformation['name_bng'])): ?>
                        <?php if (empty($getProtiMontriInformation['name_bng'])) { ?>
                        $('#potrojariDraftForm').find('#second_receiver_name').text('<?php echo(!empty($getMontriInformation['name_bng'])
                            ? $getMontriInformation['name_bng'] : '')
                            ?>');
                        $('#potrojariDraftForm').find('#second_receiver_designation').text('<?php echo(!empty($getMontriInformation['designation'])
                            ? $getMontriInformation['designation'] : '')
                            ?>');
                        SarNothiFormEditable.encodeImage('<?= $getMontriInformation['username'] ?>','<?= $getMontriInformation['en_username'] ?>', function (src) {
                            $('#potrojariDraftForm').find('#second_receiver_signature').html('<img src="' + src + '" alt="signature" width="100" style="height: 50px;" />');
                        });
                        <?php }
                        else {
                        ?>
                        $('#potrojariDraftForm').find('#third_receiver_name').text('<?php echo(!empty($getMontriInformation['name_bng'])
                            ? $getMontriInformation['name_bng'] : '')
                            ?>');
                        $('#potrojariDraftForm').find('#third_receiver_designation').text('<?php echo(!empty($getMontriInformation['designation'])
                            ? $getMontriInformation['designation'] : '')
                            ?>');
                        SarNothiFormEditable.encodeImage('<?= $getMontriInformation['username'] ?>','<?= $getMontriInformation['en_username'] ?>', function (src) {
                            $('#potrojariDraftForm').find('#third_receiver_signature').html('<img src="' + src + '" alt="signature" width="100" style="height: 50px;" />');
                        });
                        <?php } endif; ?>

                        <?php if (!empty($getMontriInformation['name_bng'])) { ?>
                        <?php if (empty($getProtiMontriInformation['name_bng'])) { ?>
                        $('#potrojariDraftForm').find('#third_receiver_name').text($("#potro-type").val() == 15 ? '' : '');
                        $('#potrojariDraftForm').find('#third_receiver_designation').text('মাননীয় প্রধানমন্ত্রী');

                        SarNothiFormEditable.encodeImage('<?= $getPrimInformation['username'] ?>','<?= $getPrimInformation['en_username'] ?>', function (src) {
                            $('#potrojariDraftForm').find('#third_receiver_signature').html('<img src="' + src + '" alt="signature" width="100" style="height: 50px;display:none;" />');
                        });

                        if ($("#potro-type").val() == 16) {
                            $('#potrojariDraftForm').find('#fourth_receiver_name').text($("#potro-type").val() == 15 ? '' : '');
                            $('#potrojariDraftForm').find('#fourth_receiver_designation').text('মাননীয় রাষ্ট্রপতি');

                            SarNothiFormEditable.encodeImage('<?= $getPresidentInformation['username'] ?>','<?= $getPresidentInformation['en_username'] ?>', function (src) {
                                $('#potrojariDraftForm').find('#fourth_receiver_signature').html('<img src="' + src + '" alt="signature" width="100" style="height: 50px;display:none;" />');
                            });
                        }
                        <?php }
                        else {
                        ?>
                        $('#potrojariDraftForm').find('#fourth_receiver_name').text($("#potro-type").val() == 15 ? '' : '');
                        $('#potrojariDraftForm').find('#fourth_receiver_designation').text('মাননীয় প্রধানমন্ত্রী');

                        SarNothiFormEditable.encodeImage('<?= $getPrimInformation['username'] ?>','<?= $getPrimInformation['en_username'] ?>', function (src) {
                            $('#potrojariDraftForm').find('#fourth_receiver_signature').html('<img src="' + src + '" alt="signature" width="100" style="height: 50px;display:none;" />');
                        });
                        if ($("#potro-type").val() == 16) {
                            $('#potrojariDraftForm').find('#fifth_receiver_name').text($("#potro-type").val() == 15 ? '' : '');
                            $('#potrojariDraftForm').find('#fifth_receiver_designation').text('মাননীয় রাষ্ট্রপতি');

                            SarNothiFormEditable.encodeImage('<?= $getPresidentInformation['username'] ?>','<?= $getPresidentInformation['en_username'] ?>', function (src) {
                                $('#potrojariDraftForm').find('#fifth_receiver_signature').html('<img src="' + src + '" alt="signature" width="100" style="height: 50px;display:none;" />');
                            });
                        }
                        <?php
                        }
                        }
                        else {
                        ?>
                        <?php if (empty($getProtiMontriInformation['name_bng'])) { ?>
                        $('#potrojariDraftForm').find('#second_receiver_name').text($("#potro-type").val() == 15 ? '' : '');
                        $('#potrojariDraftForm').find('#second_receiver_designation').text('মাননীয় প্রধানমন্ত্রী');

                        SarNothiFormEditable.encodeImage('<?= $getPrimInformation['username'] ?>','<?= $getPrimInformation['en_username'] ?>', function (src) {
                            $('#potrojariDraftForm').find('#second_receiver_signature').html('<img src="' + src + '" alt="signature" width="100" style="height: 50px;display:none;" />');
                        });
                        if ($("#potro-type").val() == 16) {
                            $('#potrojariDraftForm').find('#third_receiver_name').text($("#potro-type").val() == 15 ? '' : '');
                            $('#potrojariDraftForm').find('#third_receiver_designation').text('মাননীয় রাষ্ট্রপতি');
                            SarNothiFormEditable.encodeImage('<?= $getPresidentInformation['username'] ?>','<?= $getPresidentInformation['en_username'] ?>', function (src) {
                                $('#potrojariDraftForm').find('#third_receiver_signature').html('<img src="' + src + '" alt="signature" width="100" style="height: 50px;display:none;" />');
                            });
                        }
                        <?php }
                        else {
                        ?>
                        $('#potrojariDraftForm').find('#third_receiver_designation').text($("#potro-type").val() == 15 ? '' : '');
                        $('#potrojariDraftForm').find('#third_receiver_designation').text('মাননীয় প্রধানমন্ত্রী');

                        SarNothiFormEditable.encodeImage('<?= $getPrimInformation['username'] ?>','<?= $getPrimInformation['en_username'] ?>', function (src) {
                            $('#potrojariDraftForm').find('#third_receiver_signature').html('<img src="' + src + '" alt="signature" width="100" style="height: 50px;display:none;" />');
                        });
                        if ($("#potro-type").val() == 16) {
                            $('#potrojariDraftForm').find('#fourth_receiver_name').text($("#potro-type").val() == 15 ? '' : '');
                            $('#potrojariDraftForm').find('#fourth_receiver_designation').text('মাননীয় রাষ্ট্রপতি');
                            SarNothiFormEditable.encodeImage('<?= $getPresidentInformation['username'] ?>','<?= $getPresidentInformation['en_username'] ?>', function (src) {
                                $('#potrojariDraftForm').find('#fourth_receiver_signature').html('<img src="' + src + '" alt="signature" width="100" style="height: 50px;display:none;" />');
                            });
                        }
                        <?php
                        }
                        }
                        ?>

                    } else {
                        $('#potrojariDraftForm').find("#template-body2").hide("slow");
                    }
                }
            });
        });

                        var DRAFT_FORM = {
                            attached_files: [],
                            nothi_potro: [],
                            receiver_users: [],
                            onulipi_users: [],
                            setform: function () {
                                toastr.options = {
                                    "closeButton": true,
                                    "debug": false,
                                    "positionClass": "toast-bottom-right"
                                };

//                                if ($('#potrojariDraftForm').find('.mce-tinymce').length > 0) {
//                                    $("#pencil").click();
//                                }
                                if ($('#potrojariDraftForm').find('.fr-toolbar').length > 0) {
                                    $("#pencil").click();
                                }

                                DRAFT_FORM.attached_files = [];
                                $('#fileuploadpotrojari').find('.template-download').each(function () {
                                    var link_td = $(this).find('.name');
                                    var href = $(link_td).find('a').attr('href');
                                    var attachment_input = $(this).find('.note-attachment-input').val();
                                    DRAFT_FORM.attached_files.push(href+';'+attachment_input);
                                });

                                $('#potrojariDraftForm').find("#uploaded_attachments").val(DRAFT_FORM.attached_files);

                                $('#potrojariDraftForm').find("#file_description").val($("#file_description_upload").val());

                                var subject = $('#potrojariDraftForm').find('#subject').text();

                                var sender_sarok_no = $('#potrojariDraftForm').find('#sharok_no').text();
                                $('#potrojariDraftForm').find('input[name=sender_sarok_no]').val(sender_sarok_no);
                                $('#potrojariDraftForm').find('input[name=dak_subject]').val(subject);

                                var temp = $('#potrojariDraftForm').find('#template-body2').html();

                                if (temp.length == 0 || $('#potro-type').val() == '') {
                                    toastr.error("দুঃখিত! পত্র দেয়া হয়নি");
                                    return false;
                                }

                                if ($('#potrojariDraftForm').find('input[name=dak_subject]').val().length == 0) {
                                    $('#potrojariDraftForm').find('input[name=dak_subject]').val($('#potro-type option:selected').text());
                                }

                                if ($('#potrojariDraftForm').find('input[name=sender_sarok_no]').val().length == 0) {
                                    toastr.error("দুঃখিত! স্মারক নম্বর দেয়া হয়নি");
                                    return false;
                                }

                                $('#potrojariDraftForm').find('#template-body2').find('#pencil').remove();

                                $.each($('#potrojariDraftForm').find('#template-body2').find('a').not('.showAttachmentOfContent'), function (i, v) {
                                    var txt = $(this).text();
                                    var id = $(this).attr('id');
                                    var dataType = $(this).attr('data-type');
                                    $(this).replaceWith("<span class='canedit' id='" + id + "' data-type='" + dataType + "'>" + txt + "</span>");
                                });

                                var templatebody = $('#potrojariDraftForm').find('#template-body2').html();
                                $('#potrojariDraftForm').find('#contentbody2').text(templatebody);
                                $('#potrojariDraftForm').find('#template-body2').html(temp);

                                var temp2 = $('#potrojariDraftForm').find('#template-body').html();

                                $.each($('#potrojariDraftForm').find('#template-body').find('a').not('.showAttachmentOfContent'), function (i, v) {
                                    var txt = $(this).text();
                                    var id = $(this).attr('id');
                                    var dataType = $(this).attr('data-type');
                                    $(this).replaceWith("<span class='canedit' id='" + id + "' data-type='" + dataType + "'>" + txt + "</span>");
                                });

                                var templatebody = $('#potrojariDraftForm').find('#template-body').html();
                                $('#potrojariDraftForm').find('#contentbody').text(templatebody);
                                $('#potrojariDraftForm').find('#template-body').html(temp2);

                                DRAFT_FORM.nothi_potro = [];
                                $('.songlapbutton').each(function () {
                                    var id = $(this).attr('id');
                                    if(typeof(id)!='undefined'){
                                        DRAFT_FORM.nothi_potro.push(id);
                                    }
                                });

                            },
                            saveDraft: function () {
             
                                if (DRAFT_FORM.setform() != false) {

                                    $("#potrojariDraftForm").find('input[name=inline_attachment]').val(DRAFT_FORM.nothi_potro);

                                    $("#potrojariDraftForm").next().next().find('button').attr('disabled', 'disabled');
                                    $.ajax({
                                        url: '<?php echo $this->Url->build(['controller' => 'SummaryNothi',
        'action' => 'saveSummaryNothi']).'/'.$nothimasterid;
    ?>',
                                        data: $("#potrojariDraftForm").serialize(),
                                        method: "post",
                                        dataType: 'JSON',
                                        async: false,
                                        cache: false,
                                        success: function (response) {
                                            if (response.status == 'error') {
                                                $("#potrojariDraftForm").next().next().find('button').removeAttr('disabled');
                                                toastr.error(response.msg);
                                            } else {
                                                toastr.success(response.msg);
                                                window.location.href = '<?php echo $this->Url->build(['_name' => 'noteDetail']).'/'.$nothimasterid;
    ?>';
                                            }
                                        },
                                        error: function (xhr, status, errorThrown) {

                                        }
                                    });
                                }
                            }
                        };

    </script>

    <script>

        function loadCover() {
            $('#potrojariDraftForm').find("#template-body").show("slow");
            $('#potrojariDraftForm').find("#template-body").html('<img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading text-center"><span>&nbsp;&nbsp;লোড করা হচ্ছে...</span>');
            $.ajax({
                url: js_wb_root + 'potrojariTemplates/getTemplate/' + <?php
    $k = array_keys($coverTemplate);
    echo $k[0]
    ?>,
                dataType: 'JSON',
                success: function (response) {
                    if (typeof response.html_content != 'undefined') {
                        $('#potrojariDraftForm').find('#template-body').html(response.html_content);

                        SarNothiFormEditable.init('<?php echo $nothiMasterInfo['nothi_no'] ?>');


                    } else {
                        $('#potrojariDraftForm').find("#template-body").hide("slow");
                    }
                }
            });
        }
        (function ($) {

            $('#potrojariDraftForm').find('select').select2();
            DakSetup.init();
            PotroJariFileUpload.init();
            loadCover();

            $('#potrojariDraftForm').find('#pencil').remove();
            $.each($('#potrojariDraftForm').find('span.canedit'), function (i, v) {
                var id = $(this).attr('id');
                var txt = $(this).text();
                var dataType = $(this).attr('data-type');

                if (id == 'sending_date') {
                    $(this).replaceWith('<a data-placement="right"  data-pk="1" data-viewformat="dd.mm.yyyy" data-type="date" id="sending_date" href="#" class="editable editable-click">' + txt + '</a>');
                } else {
                    $(this).replaceWith("<a data-type='text' data-pk='1' class='editable editable-click' href='javascript:;' id='" + id + "' data-type='" + dataType + "'>" + txt + "</a>");
                }
            });
            $('#potrojariDraftForm').find('#note').before('<a id="pencil" href="#"> <i class="fs1 a2i_gn_edit2"></i>[সম্পাদন  করুন] <br/></a>');

            SarNothiFormEditable.init('<?php echo $nothiMasterInfo['nothi_no'] ?>');

        }(jQuery));


    </script>
<?php endif; ?>
    <!-- End: JavaScript -->
    <!-- The blueimp Gallery widget -->
    <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
        <div class="slides">
        </div>
        <h3 class="title"></h3>
        <a class="prev">
            ÔøΩ </a>
        <a class="next">
            ÔøΩ </a>
        <a class="close white">
        </a>
        <a class="play-pause">
        </a>
        <ol class="indicator">
        </ol>
    </div>

    <script id="template-upload2" type="text/x-tmpl">
        {% for (var i=0, file; file=o.files[i]; i++) { %}
        <tr class="template-upload fade">
        <td class="name" width="30%"><span>{%=file.name%}</span></td>
        <td class="size" width="40%"><span>{%=o.formatFileSize(file.size)%}</span></td>
        {% if (file.error) { %}
        <td class="error" width="20%" colspan="2"><span class="label label-danger">ত্রুটি</span> {%=file.error%}</td>
        {% } else if (o.files.valid && !i) { %}
        <td>
        <p class="size">{%=o.formatFileSize(file.size)%}</p>
        <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
        <div class="progress-bar progress-bar-success" style="width:0%;"></div>
        </div>
        </td>
        {% } else { %}
        <td colspan="2"></td>
        {% } %}
        <td class="cancel" width="10%" align="right">{% if (!i) { %}
        <button class="btn btn-sm red cancel">
        <i class="fa fa-ban"></i>
        <span>বাতিল করুন</span>
        </button>
        {% } %}</td>
        </tr>
        {% } %}
    </script>
    <script id="template-download2" type="text/x-tmpl">
        {% for (var i=0, file; file=o.files[i]; i++) { %}
        <tr class="template-download fade">
        {% if (file.error) { %}
        <td class="name" width="30%"><span>{%=file.name%}</span></td>
        <td class="size" width="20%"><span>{%=o.formatFileSize(file.size)%}</span></td>
        <td class="error" width="30%" colspan="2"><span class="label label-danger">ত্রুটি</span> {%=file.error%}</td>
        {% } else { %}
        <td class="name" width="40%">
        <a href="<?= FILE_FOLDER ?>{%=file.url%}" title="{%=file.name%}" data-gallery="<?= FILE_FOLDER ?>{%=file.thumbnail_url&&'gallery'%}" download="{%=file.name%}">{%=file.name%}</a>
        </td>
        <td class="size" width="15%"><span>{%=o.formatFileSize(file.size)%}</span></td>
        <td width="40%" style ="min-width: 100px!important;">

        <input type="text" class="form-control note-attachment-input" id="note-attachment-input" onkeyup="check_title()" image="{%=file.url%}" file-type="{%=file.type%}">

        </td>
        {% } %}
        <td width="5%">
            {% if (file.deleteUrl) { %}
            <button class="btn red delete btn-sm" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
            <i class="fs1 a2i_gn_delete2"></i>
            <span>মুছে ফেলুন</span>
            </button>

            {% } else { %}
            <button class="btn yellow cancel btn-sm">
            <i class="fa fa-ban"></i>
            <span>বাতিল করুন</span>
            </button>
            {% } %}
        </td>
        </tr>
        {% } %}

    </script>

<script>
    function check_title() {

        $('[id^=dropdown-menu-summary-attachment-ref] ul').html('');
        $('[id^=dropdown-menu-summary-attachment-ref] ul').append($('<li role="presentation"><a class="fr-command" tabindex="-1" role="option" data-cmd="summary-attachment-ref" data-param1="--" title="" aria-selected="false">--</a></li>'));

        $('#fileuploadpotrojari .note-attachment-input').each(function () {
            var tx = $(this).val();
            var sanitized = $(this).val().replace(/;/g, '');
            $(this).val(sanitized);
            sanitized = $(this).val().replace(/,/g, '');
            $(this).val(sanitized);
            var file_handle = $(this).attr('image');
            var file_type = $(this).attr('file-type');

            $('[id^=dropdown-menu-summary-attachment-ref] ul').append('<li role="presentation"><a class="fr-command" tabindex="-1" role="option" data-cmd="summary-attachment-ref" data-param1="'+ file_handle + '" ft="' + file_type +'" title="" aria-selected="false">'+tx+'</a></li>');

        });
        if (parseInt($('#noteId').val()) > 0) {
            $.each($('#responsiveNoteEdit').next().next().find('.preview_attachment a'), function (i, v) {
                var tx = '';
                var d_text = 'No_user_input_file';
                if ($(this).attr('user_file_name') == d_text) {
                    tx = v.text;
                } else {
                    tx = $(this).attr('user_file_name');
                }
                var file_type = v.type;
                var file_handle = v.href;

                $('[id^=dropdown-menu-summary-attachment-ref] ul').append('<li role="presentation"><a class="fr-command" tabindex="-1" role="option" data-cmd="summary-attachment-ref" data-param1="'+ file_handle + '" ft="' + file_type +'" title="" aria-selected="false">'+tx+'</a></li>');
            });
        }
    }

</script>