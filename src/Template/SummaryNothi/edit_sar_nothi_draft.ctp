
<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-toastr/toastr.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>assets/global/plugins/jquery-multi-select/css/multi-select.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>projapoti-nothi/css/styles.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2.css"/>
<style>
    .btn-changelog, .btn-forward-nothi,.btn-nothiback ,.btn-print{
        padding: 3px 5px !important;

    }

    .btn-icon-only {

    }

    .editable {
        border:none!important;
        word-break:break-word;
        word-wrap:break-word
    }

    #sovapoti_signature, #sender_signature, #sender_signature2, #sender_signature3{
        visibility: hidden;
    }

    #note{
        overflow: hidden;
        word-break:break-word;
        word-wrap: break-word;
        height: 100%;
    }

    .ms-container{
        width: 100%!important;
    }

    .bangladate{
        border-bottom: 1px solid #000;
    }

    .editable-click, a.editable-click {
        border: none;
        word-break:break-word;
        word-wrap:break-word
    }
    .cc_list {
        white-space: pre-wrap;
    }

    .popover-content {
        padding: 10px 30px;
    }

    .mega-menu-dropdown > .dropdown-menu{
        top:10px!important;
    }

    img[alt=signature]{
        visibility: hidden;
    }

    .showImage img {
        visibility: visible!important;
    }

</style>


<div class="portlet box green-seagreen">
    <div class="portlet-title">
        <div class="caption">
            <?php echo "শাখা: ".$officeUnitsName."; নথি নম্বর: ".$nothiRecord['nothi_no'].'; বিষয়: '.$nothiRecord['subject']; ?>
        </div>

        <div class="actions">

            <a title="নথিতে ফেরত যান" href="<?php echo $this->Url->build(['_name' => 'noteDetail', $nothimasterid, $nothi_office])
            ?>" class="  btn btn-danger  btn-nothiback" >
                <i class="fa fa-list"></i> নথিতে ফেরত যান
            </a>
            <a title="ড্রাফট পরিবর্তন দেখুন" nothi_master_id="<?php echo $nothimasterid ?>" potrojari="<?php echo $summaryDraft->id ?>" class="  btn btn-info  btn-changelog" >
                <i class="fs1 a2i_gn_history3"></i> ড্রাফট পরিবর্তন
            </a>
<?php if ($privilige_type == 1): ?>
        <!--                <a  title=" প্রেরণ  করুন" nothi_master_id="<?php echo $nothimasterid ?>"  class="  btn purple  btn-forward-nothi" >
                            <i class="fs1 a2i_gn_send2"></i> &nbsp;  প্রেরণ  করুন
                        </a>-->
<?php endif; ?>



        </div>

    </div>
    <div class="portlet-body">
        <div class="row ">
            <div class="col-md-12 " style="margin: 0 auto;">
<?php if ($privilige_type == 1) { ?>
                    <div class="form">

                        <?php echo $this->Form->create('',
                            array('onsubmit' => 'return false;', 'type' => 'file', 'id' => 'potrojariDraftForm'));
                        ?>
                        <?php
                        echo $this->Form->hidden('id');
                        echo $this->Form->hidden('dak_type',
                            array('label' => false, 'class' => 'form-control', 'value' => DAK_DAPTORIK));
                        echo $this->Form->hidden('is_summary_nothi',
                            array('label' => false, 'class' => 'form-control', 'value' => 1));
                        echo $this->Form->hidden('dak_status',
                            array('label' => false, 'class' => 'form-control', 'value' => 1));
                        echo $this->Form->hidden('dak_subject',
                            array('label' => false, 'class' => 'form-control'));
                        echo $this->Form->hidden('sender_sarok_no',
                            array('label' => false, 'class' => 'form-control'));
                        echo $this->Form->hidden('potro_type',
                            ['value' => $summaryDraft->potro_type]);
                        echo $this->Form->hidden('inline_attachment',
                            array('label' => false, 'class' => 'form-control'));
                        ?>
    <?php echo $this->Cell('Potrojari::sarNothiDraft',
        ['template' => $template_list, 'potrojari' => $summaryDraft])
    ?>


    <?php // echo $this->Form->end(); ?>
                        <div class="form-body sonjukti hide">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="tabbable-line">
                                        <ul class="nav nav-tabs ">
                                            <li class="active">
                                                <a href="#tab_prapto_potro" data-toggle="tab" aria-expanded="true">
                                                    প্রাপ্ত পত্রসমূহ </a>
                                            </li>
                                            <li class="">
                                                <a href="#tab_other_potro" data-toggle="tab" aria-expanded="false">
                                                    অন্যান্য সংযুক্তি</a>
                                            </li>

                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="tab_prapto_potro">
                                                <?php
                                                if (!empty($potroAttachmentRecord)) {
                                                    echo '<div id="summary_potro_attachment_input_form" >
                                            <table role="presentation" class="table table-striped clearfix">
                                                <tbody >';
                                                    foreach ($potroAttachmentRecord as $key => $value) {
//                                                echo '<a href="javascript:;" class="btn btn-xs blue songlap-button" id="'.$key.'"> '.str_replace(';', '-', $value).'&nbsp;<i class="fa fa-link?"></i></a>&nbsp;';
                                                        echo '<tr><td width="50%" class="text-center">'.str_replace(';',
                                                                '-', $value).'&nbsp;</td>
                                                <td width="50%" style="min-width: 100px!important;">
                                                <input type="text" class="form-control summary-potro-attachment-input" id="summary-potro-attachment-input" onkeyup="check_potro_title()" potro_id="'.$key.'" placeholder="সংলাগের নাম লিখুন">
                                                </td></tr>';
                                                    }
                                                    echo '</tbody></table></div>';
                                                }
                                                ?>
                                            </div>
    <?php echo $this->Form->end(); ?>
                                            <div class="tab-pane" id="tab_other_potro">
                                                <form id="fileuploadpotrojari" action="<?= $this->Url->build(['_name'=>'tempUpload']) ?>" method="POST" enctype="multipart/form-data">

                                                    <input type="hidden" name="module_type" value="Nothi" />
                                                    <input type="hidden" name="office_id" value="<?php echo $summaryDraft->office_id ?>" />
                                                    <input type="hidden" name="part_no" value="<?= $nothiMasterInfo['id'] ?>" />
                                                    <input type="hidden" name="nothi_masters_id" value="<?= $nothiMasterInfo['nothi_masters_id'] ?>" />
                                                    <input type="hidden" name="module" value="Potrojari" />

                                                    <div class="col-lg-6 form-group">
                                                        <input type="text" name="file_description_upload" id="file_description_upload" class="form-control hidden" placeholder="সংযুক্তির বিবরণ">
                                                    </div>

                                                    <div class="row fileupload-buttonbar">
                                                        <div class="col-lg-12">
                                                            <!-- The fileinput-button span is used to style the file input field as button -->
                                                            <span class="btn green btn-sm fileinput-button">
                                                                <i class="fs1 a2i_gn_add1"></i>
                                                                <span>
                                                                    ফাইল যুক্ত করুন </span>
                                                                <input type="file" name="files[]" multiple="">
                                                            </span>

                                                            <button type="button" class="btn btn-sm red delete hidden">
                                                                <i class="fs1 a2i_gn_delete2"></i>
                                                                <span>
                                                                    সব মুছে ফেলুন </span>
                                                            </button>
                                                            <!--<input type="checkbox" class="toggle">-->
                                                            <!-- The global file processing state -->
                                                            <span class="fileupload-process">
                                                            </span>
                                                        </div>
                                                        <!-- The global progress information -->
                                                        <div class="col-lg-5 fileupload-progress fade">
                                                            <!-- The global progress bar -->
                                                            <div class="progress progress-striped active"
                                                                 role="progressbar"
                                                                 aria-valuemin="0" aria-valuemax="100">
                                                                <div class="progress-bar progress-bar-success"
                                                                     style="width:0%;">
                                                                </div>
                                                            </div>
                                                            <!-- The extended global progress information -->
                                                            <div class="progress-extended">
                                                                &nbsp;
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <!-- The table listing the files available for upload/download -->
                                                    <table role="presentation" class="table table-striped clearfix">
                                                        <tbody class="files">
                                                            <?php
                                                            if (isset($attachments) && count($attachments)
                                                                > 0) {
                                                                foreach ($attachments as $single_data) {
                                                                    if ($single_data['is_inline'])
                                                                            continue;
                                                                    if ($single_data['attachment_type']
                                                                        == 'text' || $single_data['attachment_type']
                                                                        == 'text/html') continue;

                                                                    $fileName = explode('/',
                                                                        $single_data['file_name']);

                                                                    $attachmentHeaders = get_file_type($single_data['file_name']);

                                                                    $value = array(
                                                                        'attachment_name'=>$single_data['attachment_name'],
                                                                        'name' => urldecode($fileName[count($fileName) - 1]),
                                                                        'thumbnailUrl' => (substr($attachmentHeaders,0,5) == 'image'?
                                                                            (FILE_FOLDER .$single_data['file_name']) : null),
                                                                        'size' => '-',
                                                                        'type' => $attachmentHeaders,
                                                                        'url' => FILE_FOLDER . $single_data['file_name'],
                                                                        'deleteUrl' => $this->Url->build(['_name'=>'secureDelete','?'=>[
                                                                            'id'=>$single_data['id'],
                                                                            'token'=>$temp_token
                                                                        ]]),
                                                                        'deleteType' => "GET",
                                                                    );

                                                                    echo '<tr class="template-download fade in">
    <td>
    <p class="name">
    '.(isset($value['name']) ? '<a href="'.$value['url'].'" title="'.$value['name'].'" download="'.$value['name'].'" data-gallery="">'.$value['name'].'</a>'
                                                                            : '').'
    </p>
    </td>
    <td>
    '.(isset($value['size']) ? '<span class="size">'.$value['size'].'</span>' : '').'
    </td>
     <td width="40%" style="min-width: 100px!important;">

                                                <input type="text" class="form-control note-attachment-input" id="note-attachment-input" onkeyup="check_title()" image="'.$value['url'].'"  value="'.$value['attachment_name'].'" file-type="'.$attachmentHeaders.'">

                                                </td>
    <td>
    <button class="btn red delete btn-sm" data-type="'.$value['deleteType'].'" data-url="'.$value['deleteUrl'].'" >
              <i class="fa fa-times"></i></button>
    </td>
    </tr>';
                                                                }
                                                            }
                                                            ?>
                                                        </tbody>
                                                    </table>
                                                </form>
                                            </div>

                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>

                        <div class="form-actions">
                    <?php if ($privilige_type == 1 && $can_approve): ?>
                                অনুমোদন দিন <input type="checkbox" <?php echo ($is_approve == 1 ? 'checked=checked'
                                : '')
                        ?> class="checkbox btn-approve" />
                        <?php endif; ?>
                            <input type="button" class="btn btn-primary saveDraftNothi" value="<?php echo __(SAVE) ?>" />
                        </div>
                    </div>
<?php }else { ?>
                    <div class="form">
    <?php
    echo $this->Cell('Potrojari::sarNothiDraft',
        ['template' => $template_list, 'potrojari' => $summaryDraft,
        'nothi_office' => $nothi_office])
    ?>


                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <h3 > <?php echo __(SHONGJUKTI) ?> </h3>
                                    <!-- The table listing the files available for upload/download -->
                                    <table role="presentation" class="table table-striped clearfix">
                                        <tbody class="files">
                                            <?php
                                            if (isset($attachments) && count($attachments) > 0) {
                                                foreach ($attachments as $single_data) {
                                                    if ($single_data['attachment_type'] == 'text' || $single_data['attachment_type']
                                                        == 'text/html') continue;

                                                    $fileName          = explode('/',
                                                        $single_data['file_name']);
                                                    $attachmentHeaders = get_file_type($single_data['file_name']);

                                                    $value = array(
                                                        'name' => urldecode($fileName[count($fileName) - 1]),
                                                        'thumbnailUrl' => (substr($attachmentHeaders,0,5) == 'image'?
                                                            (FILE_FOLDER .$single_data['file_name']) : null),
                                                        'size' => '-',
                                                        'type' => $attachmentHeaders,
                                                        'url' => FILE_FOLDER . $single_data['file_name'],
                                                        'deleteUrl' => $this->Url->build(['_name'=>'secureDelete','?'=>[
                                                            'id'=>$single_data['id'],
                                                            'token'=>$temp_token
                                                        ]]),
                                                        'deleteType' => "GET",
                                                    );


                                                    echo '<tr class="template-download fade in" style="vertical-align: middle;">
    <td>
    <input type="checkbox" name="delete" value="1" class="toggle">
    <span class="preview">
    '.(isset($value['thumbnailUrl']) ? '<a href="'.$value['url'].'" title="'.$value['name'].'" download="'.$value['url'].'" data-gallery><img  style="height:80px" src="'.$value['thumbnailUrl'].'"></a>'
                                                            : '').'
    </span>
    </td>
    <td>
    <p class="name">
    '.(isset($value['name']) ? '<a href="'.$value['url'].'" title="'.$value['name'].'" download="'.$value['name'].'" data-gallery="">'.$value['name'].'</a>'
                                                            : '').'
    </p>
    </td>
    <td>
    '.(isset($value['size']) ? '<span class="size">'.$value['size'].'</span>' : '').'
    </td>
    <td>
    <button class="btn red delete btn-sm" data-type="'.$value['deleteType'].'" data-url="'.$value['deleteUrl'].'" >
                <i class="glyphicon glyphicon-trash"></i>
                <span>মুছে ফেলুন</span>
            </button>
    </td>
    </tr>';
                                                }
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
<?php } ?>
            </div>
        </div>
    </div>
</div>

<?php if ($privilige_type == 1): ?>
    <div id="responsiveNothiUsers" class="modal fade modal-purple" tabindex="-1" aria-hidden="true" data-backdrop="static"
         data-keyboard="false">
        <div class="modal-dialog modal-full">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">পরবর্তী কার্যক্রমের জন্য প্রেরণ করুন</h4>
                </div>
                <div class="modal-body">
                    <div class="scroller" style="height:100%" data-always-visible="1" data-rail-visible1="1">
                        <input type="hidden" name="nothimasterid"/>
                        
                        <div class="user_list">

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn green sendDraftNothi">প্রেরণ করুন</button>
                    <button type="button" data-dismiss="modal" class="btn  btn-danger">
                        বন্ধ করুন
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div id="responsivesonglap" class="modal fade" tabindex="-1" aria-hidden="true" data-backdrop="static"  data-keyboard="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">সংলাগ</h4>
                </div>
                <div class="modal-body">
                    <div class="scroller" style="height:100%" data-always-visible="1" data-rail-visible1="1">
                        <div class="row">
                            <div class="col-md-12 form-group">
                                <label>সংলাগের নাম</label>
                                <input type="text"  class="form-control songlapDes" />
                            </div>

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn green songlapDesSave" >সংলাগ দিন</button>
                    <button type="button" data-dismiss="modal" class="btn  btn-danger">
                        বন্ধ করুন
                    </button>
                </div>
            </div>
        </div>
    </div>


    <div id="responsivesonglapattac" class="modal fade" tabindex="-1" aria-hidden="true" data-backdrop="static"  data-keyboard="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">সংলাগ</h4>
                </div>
                <div class="modal-body">
                    <div class="scroller" style="height:100%" data-always-visible="1" data-rail-visible1="1">
                        <div class="row">
                            <div class="col-md-12 form-group">
                                <label>সংলাগের নাম</label>
                                <input type="text"  class="form-control songlapAttachDes" />
                                <input type="hidden" name="url"/>
                        <input type="hidden" name="type"/>

                            </div>

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn green songlapDesAttachmentSave" >সংলাগ দিন</button>
                    <button type="button" data-dismiss="modal" class="btn  btn-danger">
                        বন্ধ করুন
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!--dictionary input-->
    <div>
        <input type="hidden"id="word" value="">
        <input type="hidden"id="space" value="">
    </div>
    <?= $this->element('froala_editor_js_css') ?>
<!--    <script src="--><?php //echo CDN_PATH; ?><!--js/tinymce/tinymce.min.js?v=--><?//=js_css_version?><!--" type="text/javascript"></script>-->

    <script src="<?php echo CDN_PATH; ?>projapoti-nothi/js/nothi_master_movements.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-toastr/toastr.min.css"/>
    <script src="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-toastr/toastr.min.js"></script>
    <script src="<?php echo CDN_PATH; ?>assets/admin/pages/scripts/ui-toastr.js"></script>

    <script src="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2.js" type="text/javascript"></script>
    <script src="<?php echo CDN_PATH; ?>assets/admin/layout4/scripts/dak_setup.js" type="text/javascript"></script>
    <script src="<?php echo  $this->request->webroot; ?>projapoti-nothi/js/potrojari_file_update.js" type="text/javascript"></script>

    <script type="text/javascript" src="<?php echo $this->request->webroot; ?>assets/global/plugins/jquery.mockjax.js"></script>
    <script src="<?php echo  $this->request->webroot; ?>assets/admin/pages/scripts/form-editable.js"></script>

    <script src="<?php echo $this->request->webroot; ?>projapoti-nothi/js/sarnothi_editable.js" type="text/javascript"></script>
      <script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>

    <?php $template_keys = array_keys($template_list); ?>
    <script>
           $(function () {
        $('#prapto-potro').multiSelect({dblClick: true});

    })
        var numbers = {
            1: '১',
            2: '২',
            3: '৩',
            4: '৪',
            5: '৫',
            6: '৬',
            7: '৭',
            8: '৮',
            9: '৯',
            0: '০'
        };

        function replaceNumbers(input) {
            var output = [];
            for (var i = 0; i < input.length; ++i) {
                if (numbers.hasOwnProperty(input[i])) {
                    output.push(numbers[input[i]]);
                } else {
                    output.push(input[i]);
                }
            }
            return output.join('');
        }

        var DRAFT_FORM = {
            attached_files: [],
            nothi_potro: [],
            sender_users: [],
            receiver_users: [],
            onulipi_users: [],
            setform: function () {
                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "positionClass": "toast-bottom-right"
                };

//                if ($('.mce-tinymce').length > 0) {
//                    $("#pencil").click();
//                    toastr.error("দুঃখিত! পত্র সম্পাদনা শেষ করা হয়নি ");
//                    return false;
//                }

                if ($('#potrojariDraftForm').find('.fr-toolbar').length > 0) {
                    $("#pencil").click();
                }

                DRAFT_FORM.attached_files = [];
                $('.template-download').each(function () {
                    var link_td = $(this).find('.name');
                    var href = $(link_td).find('a').attr('href');
                    var attachment_input = $(this).find('.note-attachment-input').val();
                    DRAFT_FORM.attached_files.push(href+';'+attachment_input);
                });



                $("#uploaded_attachments").val(DRAFT_FORM.attached_files);

                $("#file_description").val($("#file_description_upload").val());

                var subject = $('#subject').text();

                var sender_sarok_no = $('#sharok_no').text();
                $('input[name=sender_sarok_no]').val(sender_sarok_no);
                $('input[name=dak_subject]').val(subject);

                var temp = $('#template-body2').html();

                if (temp.length == 0 || $('#potro-type').val() == '') {
                    toastr.error("দুঃখিত! পত্র দেয়া হয়নি");
                    return false;
                }

                if ($('input[name=dak_subject]').val().length == 0) {
                    $('input[name=dak_subject]').val($('#potro-type option:selected').text());
                }

                if ($('input[name=sender_sarok_no]').val().length == 0) {
                    toastr.error("দুঃখিত! স্মারক নম্বর দেয়া হয়নি");
                    return false;
                }

                $('#template-body2').find('#pencil').remove();

                $.each($('#template-body2').find('a').not('.showAttachmentOfContent'), function (i, v) {
                    var txt = $(this).text();
                    var id = $(this).attr('id');
                    var dataType = $(this).attr('data-type');
                    $(this).replaceWith("<span class='canedit' id='" + id + "' data-type='" + dataType + "'>" + txt + "</span>");
                });

                var templatebody = $('#template-body2').html();
                $('#contentbody2').text(templatebody);
                $('#template-body2').html(temp);

                var temp2 = $('#template-body').html();

                $.each($('#template-body').find('a').not('.showAttachmentOfContent'), function (i, v) {
                    var txt = $(this).text();
                    var id = $(this).attr('id');
                    var dataType = $(this).attr('data-type');
                    $(this).replaceWith("<span class='canedit' id='" + id + "' data-type='" + dataType + "'>" + txt + "</span>");
                });

                var templatebody = $('#template-body').html();
                $('#contentbody').text(templatebody);
                $('#template-body').html(temp2);

                DRAFT_FORM.nothi_potro = [];
                $('.songlapbutton').each(function () {
                    var id = $(this).attr('id');
                    if(typeof(id)!='undefined'){
                        DRAFT_FORM.nothi_potro.push(id);
                    }
                });
            }
        };

    </script>

    <script>
        $(function () {
            NothiMasterMovement.init('nothing');
            $('select').select2();
            DakSetup.init();
            PotroJariFileUpload.init();

            $('#pencil').remove();
            $.each($('span.canedit'), function (i, v) {
                var id = $(this).attr('id');
                var txt = $(this).text();
                var dataType = $(this).attr('data-type');

                if (id == 'sending_date') {
                    $(this).replaceWith('<a data-placement="right"  data-pk="1" data-viewformat="dd.mm.yyyy" data-type="date" id="sending_date" href="#" class="editable editable-click">' + txt + '</a>');
                } else {
                    $(this).replaceWith("<a data-type='text' data-pk='1' class='editable editable-click' href='javascript:;' id='" + id + "' data-type='" + dataType + "'>" + txt + "</a>");
                }
            });
            $('#note').before('<a id="pencil" href="#"> <i class="fs1 a2i_gn_edit2"></i>[সম্পাদন  করুন] <br/></a>');

            SarNothiFormEditable.init('<?php echo $nothiMasterInfo['nothi_no'] ?>', '<?php echo $summaryDraft->potro_subject ?>');

        });


    </script>

    <!-- End: JavaScript -->
    <!-- The blueimp Gallery widget -->
    <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
        <div class="slides">
        </div>
        <h3 class="title"></h3>
        <a class="prev">
            ÔøΩ </a>
        <a class="next">
            ÔøΩ </a>
        <a class="close white">
        </a>
        <a class="play-pause">
        </a>
        <ol class="indicator">
        </ol>
    </div>
    <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
    <script id="template-upload2" type="text/x-tmpl">
                                                {% for (var i=0, file; file=o.files[i]; i++) { %}
                                                <tr class="template-upload fade">
                                                <td class="name" width="30%"><span>{%=file.name%}</span></td>
                                                <td class="size" width="40%"><span>{%=o.formatFileSize(file.size)%}</span></td>
                                                {% if (file.error) { %}
                                                <td class="error" width="20%" colspan="2"><span class="label label-danger">ত্রুটি</span> {%=file.error%}</td>
                                                {% } else if (o.files.valid && !i) { %}
                                                <td>
                                                <p class="size">{%=o.formatFileSize(file.size)%}</p>
                                                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                                                <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                                                </div>
                                                </td>
                                                {% } else { %}
                                                <td colspan="2"></td>
                                                {% } %}
                                                <td class="cancel" width="10%" align="right">{% if (!i) { %}
                                                <button class="btn btn-sm red cancel">
                                                <i class="fa fa-ban"></i>
                                                <span>বাতিল করুন</span>
                                                </button>
                                                {% } %}</td>
                                                </tr>
                                                {% } %}



                                            </script>
    <!-- The template to display files available for download -->
    <script id="template-download2" type="text/x-tmpl">
                                                {% for (var i=0, file; file=o.files[i]; i++) { %}
                                                <tr class="template-download fade">
                                                {% if (file.error) { %}
                                                <td class="name" width="30%"><span>{%=file.name%}</span></td>
                                                <td class="size" width="20%"><span>{%=o.formatFileSize(file.size)%}</span></td>
                                                <td class="error" width="30%" colspan="2"><span class="label label-danger">ত্রুটি</span> {%=file.error%}</td>
                                                {% } else { %}
                                                <td class="name" width="40%">
                                                <a href="<?= FILE_FOLDER ?>{%=file.url%}" title="{%=file.name%}" data-gallery="<?= FILE_FOLDER ?>{%=file.thumbnail_url&&'gallery'%}" download="{%=file.name%}">{%=file.name%}</a>
                                                </td>
                                                <td class="size" width="15%"><span>{%=o.formatFileSize(file.size)%}</span></td>
                                                <td width="40%" style ="min-width: 100px!important;">

                                                <input type="text" class="form-control note-attachment-input" id="note-attachment-input" onkeyup="check_title()" image="{%=file.url%}" file-type="{%=file.type%}">

                                                </td>
                                                {% } %}
                                                <td width="5%">
                                                    {% if (file.deleteUrl) { %}
                                                    <button class="btn red delete btn-sm" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                                                    <i class="fs1 a2i_gn_delete2"></i>
                                                    <span>মুছে ফেলুন</span>
                                                    </button>

                                                    {% } else { %}
                                                    <button class="btn yellow cancel btn-sm">
                                                    <i class="fa fa-ban"></i>
                                                    <span>বাতিল করুন</span>
                                                    </button>
                                                    {% } %}
                                                </td>
                                                </tr>
                                                {% } %}



                                            </script>

    <script>
        $(document).on('click', '.saveDraftNothi', function () {
            var serialPermission = 0;

            if (DRAFT_FORM.setform() != false) {
                $("#potrojariDraftForm").find('input[name=inline_attachment]').val(DRAFT_FORM.nothi_potro);
                $.ajax({
                    url: '<?php echo $this->Url->build(['controller' => 'Potrojari', 'action' => 'SummaryNothiDraftUpdate']).'/'.$summaryDraft->id.'/'.$nothi_office; ?>',
                    data: $("#potrojariDraftForm").serialize(),
                    method: "post",
                    dataType: 'JSON',
                    async: false,
                    cache: false,
                    success: function (response) {
                        if (response.status == 'error') {
                            toastr.error(response.msg);
                        } else {
                            toastr.success("খসড়া সংশোধিত হয়েছে");
                            setTimeout(function(){
                                window.location.href = '<?php echo $this->Url->build(['_name' => 'noteDetail']).'/' . $nothimasterid ?>';
                            }, 500)
                        }
                    },
                    error: function (xhr, status, errorThrown) {

                    }
                });
            }
        });
        $(document).on('click', '.sendDraftNothi', function () {
            var serialPermission = 0;

            if (DRAFT_FORM.setform() != false) {

                $.ajax({
                    url: '<?php echo $this->Url->build(['controller' => 'Potrojari', 'action' => 'SummaryNothiDraftUpdate']).'/'.$summaryDraft->id.'/'.$nothi_office; ?>',
                    data: $("#potrojariDraftForm").serialize(),
                    method: "post",
                    dataType: 'JSON',
                    async: false,
                    cache: false,
                    success: function (response) {
                        if (response.status == 'error') {

                            toastr.error(response.msg);
                        } else {
                            NothiMasterMovement.sendNothi();
                        }
                    },
                    error: function (xhr, status, errorThrown) {

                    }
                });
            }
        });
        $(document).on('click', '.btn-approve', function () {

            Metronic.blockUI({
                target: '.page-container',
                boxed: true,
                message: 'অপেক্ষা করুন'
            });

            var prm = new Promise(function(res, rej){
                $.ajax({
                    url: '<?= $this->Url->build(['controller' => 'SummaryNothi', 'action' => 'getPosition', $nothimasterid,$summaryDraft->id]) ?>',
                    method: "post",
                    dataType: 'JSON',
                    async: false,
                    data: {},
                    cache: false,
                    success: function (response) {
                        if(response.status == 'error'){
                            rej(response.msg);
                        }else{
                            res(response);
                        }
                    },
                    error: function (xhr, status, errorThrown) {
                        Metronic.unblockUI('.page-container');
                    }
                });
            }).then(function(data){
                Metronic.unblockUI('.page-container');
                var approves = ($('.btn-approve').is(':checked') === true ? 1 : 0);
                if(approves==1){
                    $('#template-body2').find('#'+data.data.position_number).addClass('showImage');
                }else{
                    $('#template-body2').find('#'+data.data.position_number).removeClass('showImage');
                }

                if (approves) {
                    var url = '<?php echo $this->Url->build(['controller' => 'SummaryNothi', 'action' => 'draftApproved',
                        $nothimasterid, $summaryDraft->id])
                        ?>/' + 1 + '/' +<?= $nothi_office ?>;
                }else{
                    var url = '<?php echo $this->Url->build(['controller' => 'SummaryNothi', 'action' => 'draftApproved',
                        $nothimasterid, $summaryDraft->id])
                        ?>/' + 0 + '/' +<?= $nothi_office ?>;
                }

                $.ajax({
                    url: url,
                    method: "post",
                    dataType: 'JSON',
                    async: false,
                    cache: false,
                    data: {
                        description: $('#template-body2').html()
                    },
                    success: function (response) {
                        if (response.status == 'error') {
                            $('.summary_draft_approve').closest('.checked').removeClass('checked');
                            $('.summary_draft_approve').removeAttr('checked');
                            toastr.error(response.msg);
                        } else {

                        }
                    },
                    error: function (xhr, status, errorThrown) {
                        Metronic.unblockUI('.page-container');
                    }
                });
            }).catch(function(err){
                taostr.error(err);
            });
        });
    </script>
<?php endif; ?>


<div id="responsiveChangeLog" class="modal fade" tabindex="-1" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">খসড়া পত্রের পরিবর্তনসমূহ</h4>
            </div>
            <div class="modal-body" style="background-color: #828282;">
                <div class="scroller" style="height:100%; max-height: 500px;" data-always-visible="1" data-rail-visible1="1">

                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade modal-purple bs-modal-lg" id="responsiveOnuccedModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">

        <div class="modal-content">
            <div class="modal-header">

                <button type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<?php echo $this->Html->script('assets/global/scripts/printThis.js'); ?>
<script>
    $(document).on('click', '.btn-changelog', function () {
        $('#responsiveChangeLog').modal('show');
        $('#responsiveChangeLog').find('.scroller').html('<img src="' + js_wb_root + 'assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp;লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');
        $.ajax({
            url: '<?php echo $this->Url->build(['controller' => 'potrojari', 'action' => 'showChangeLog']) ?>',
            data: {nothi_master_id:<?php echo $nothimasterid ?>, potrojari_id:<?php echo $summaryDraft->id ?>, nothi_office: <?= $nothi_office ?>},
            method: 'post',
            dataType: 'html',
            cache: false,
            success: function (response) {
                $('#responsiveChangeLog').find('.scroller').html(response);
            },
            error: function (err, status, rspn) {
                $('#responsiveChangeLog').find('.scroller').html('');
            }
        })
    });

    $(document).off('click', '.songlapbutton').on('click', '.songlapbutton', function () {
        $('#responsivepotalaLog').modal('show');
        $('#responsivepotalaLog').find('.scroller').html('<img src="' + js_wb_root + 'assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp;লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');

        var id = $(this).attr('id');
        var url = $(this).data('url');
        var type = $(this).data('type');
        var officeid = parseInt($(this).data('nothi-office'));

        if(typeof(id)!='undefined'){
            $.ajax({
            url: '<?php echo $this->Url->build(['controller' => 'NothiMasters', 'action' => 'showSonglap']) ?>/' + id + '/' + officeid,
            method: 'post',
            dataType: 'json',
            cache: false,
            success: function (response) {

                var attachmenttype = response.attachment_type;
                if (response.attachment_type == 'text') {
                    $('#responsivepotalaLog').find('.scroller').html('<div style="background-color: #fff; max-width:950px; min-height:815px; margin:0 auto; page-break-inside: auto;">' + response.potro_cover + "<br/>" + response.content_body + "</div>");
                } else if ((attachmenttype.substring(0, 15)) == 'application/vnd' || (attachmenttype.substring(0, 15)) == 'application/ms') {
                    $('#responsivepotalaLog').find('.scroller').html('<iframe style=" width:100%; height: 700px;" src="https://docs.google.com/gview?url=' + encodeURIComponent('<?php echo FILE_FOLDER ?>' + response.file_name) + '&embedded=true" style="width:600px; height:500px;" frameborder="0"></iframe>');

                } else if ((attachmenttype.substring(0, 5)) != 'image') {
                    $('#responsivepotalaLog').find('.scroller').html('<embed src="' + '<?php echo $this->request->webroot . 'content/' ?>' + response.file_name + '" style=" width:100%; height: 700px;" type="' + response.attachment_type + '"></embed>');
                } else {
                    $('#responsivepotalaLog').find('.scroller').html('<div class="text-center"><a href="' + '<?php echo $this->request->webroot . 'content/' ?>' + response.file_name + '" data-gallery="multiimages"  data-toggle="lightbox" data-title="পত্র নম্বর:  ' + response.nothi_potro_page_bn + '" data-footer="" ><img class="zoomimg img-responsive"  src="' + '<?php echo $this->request->webroot . 'content/' ?>' + response.file_name + '" alt="ছবি পাওয়া যায়নি"></a></div>');
                }
            },
            error: function (err, status, rspn) {
                $('#responsivepotalaLog').find('.scroller').html('');
            }
        })
        }
        else if(typeof(url)!='undefined'){
            if ((type.substring(0, 15)) == 'application/vnd' || (type.substring(0, 15)) == 'application/ms') {
                    $('#responsivepotalaLog').find('.scroller').html('<iframe style=" width:100%; height: 700px;" src="https://docs.google.com/gview?url=' + url + '&embedded=true" style="width:600px; height:500px;" frameborder="0"></iframe>');

                } else if ((type.substring(0, 5)) != 'image') {
                    $('#responsivepotalaLog').find('.scroller').html('<embed src="' + url + '" style=" width:100%; height: 700px;" type="' + type + '"></embed>');
                } else {
                    $('#responsivepotalaLog').find('.scroller').html('<div class="text-center"><a href="' + url + '" data-gallery="multiimages"  data-toggle="lightbox" data-title="সংলাগ" data-footer="" ><img class="zoomimg img-responsive"  src="' + url + '" alt="সংলাগ পাওয়া যায়নি"></a></div>');
                }
        }
    });


    $(document).on('click', '.btn-print', function () {
        $('#template-body').printThis({
            importCSS: true,
            debug: false,
            importStyle: true,
            printContainer: true,
            pageTitle: "",
            removeInline: false,
            header: null
        });
    });

    function check_title() {

        $('[id^=dropdown-menu-summary-attachment-ref] ul').html('');
        $('[id^=dropdown-menu-summary-attachment-ref] ul').append($('<li role="presentation"><a class="fr-command" tabindex="-1" role="option" data-cmd="summary-attachment-ref" data-param1="--" title="" aria-selected="false">--</a></li>'));

        $('#fileuploadpotrojari .note-attachment-input').each(function () {
            var tx = $(this).val();
            var sanitized = $(this).val().replace(/;/g, '');
            $(this).val(sanitized);
            sanitized = $(this).val().replace(/,/g, '');
            $(this).val(sanitized);
            var file_handle = $(this).attr('image');
            var file_type = $(this).attr('file-type');

            $('[id^=dropdown-menu-summary-attachment-ref] ul').append('<li role="presentation"><a class="fr-command" tabindex="-1" role="option" data-cmd="summary-attachment-ref" data-param1="'+ file_handle + '" ft="' + file_type +'" title="" aria-selected="false">'+tx+'</a></li>');

        });
        if (parseInt($('#noteId').val()) > 0) {
            $.each($('#responsiveNoteEdit').next().next().find('.preview_attachment a'), function (i, v) {
                var tx = '';
                var d_text = 'No_user_input_file';
                if ($(this).attr('user_file_name') == d_text) {
                    tx = v.text;
                } else {
                    tx = $(this).attr('user_file_name');
                }
                var file_type = v.type;
                var file_handle = v.href;

                $('[id^=dropdown-menu-summary-attachment-ref] ul').append('<li role="presentation"><a class="fr-command" tabindex="-1" role="option" data-cmd="summary-attachment-ref" data-param1="'+ file_handle + '" ft="' + file_type +'" title="" aria-selected="false">'+tx+'</a></li>');
            });
        }
    }
</script>