<div class="portlet-body">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
            <?php if($data['search']=='success'){ ?>
            <div>
                <table class="table table-hover table-bordered table-stripped">
                    <thead>
                    <tr class="heading">
                        <th class="text-center">সর্বশেষ অবস্থা : </th>
                        <th class="text-center"> <?php if($data['flag']==3){
                            echo ' পত্রজারি করা হয়েছে।';
                            } elseif($data['flag']==2){
                            echo ' ডাক হিসেবে এসেছে।';
                            } else {
                                echo ' নিজ অফিসে অবস্থান করছে।';
                            } ?></th>
                    </tr>
                    </thead>
                </table>
            </div>
            <h4 class="text-center">সার-সংক্ষেপের অবস্থান সমুহ</h4>
            <div class="table-scrollable">
                <table class="table table-hover table-bordered table-stripped">
                    <thead>
                    <tr class="heading">
                        <th>ক্রম </th>
                        <th>অফিসের নাম </th>
                        <th>নথির নাম্বার</th>
                        <th>নথির বিষয়</th>
                        <th>অনুমোদনকারীর নাম</th>
                        <th>অনুমোদন</th>
                    </tr>
                    </thead>
                    <?php $key=0; ?>
                    <?php if (!empty($data['status'])){ ?>
                        <?php foreach ($data['status'] as $value){?>
                            <tr>
                                <td><?= $this->Number->format((++$key)) ?></td>
                            <td><?= $data['formatted_office_infos'][$value['current_office_id']]['office_name_bng'] ?></td>
                            <td><?= !empty($data['nothi_master_info_formatted'][$value['current_office_id']][$value['nothi_master_id']]['nothi_no'])? $data['nothi_master_info_formatted'][$value['current_office_id']][$value['nothi_master_id']]['nothi_no'] :'' ?></td>
                            <td><?= !empty($data['nothi_master_info_formatted'][$value['current_office_id']][$value['nothi_master_id']]['subject'])? $data['nothi_master_info_formatted'][$value['current_office_id']][$value['nothi_master_id']]['subject'] :'' ?></td>
                            <td><?= h($data['formatted_employee_record_infos'][$value['employee_record_id']]['name_bng']).', '.$data['formatted_designation_infos'][$value['designation_id']]['designation_bng'] ?></td>
                            <td><?= $value['is_approve']==1?'হ্যাঁ':'না' ?></td>
                            </tr>
                        <?php } ?>
                    <?php } ?>
                </table>
            </div>
            <?php } else if($data['search']=='fail') { ?>
                <div>
                    <table class="table table-hover table-bordered table-stripped">
                        <thead>
                        <tr class="heading">
                            <th class="text-center">দুঃখিত! কোন ডাটা খুজে পাওয়া যায়নি।</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            <?php }; ?>
        </div>
    </div>
</div>
