<style>
    .checkbox input[type=checkbox], .checkbox-inline input[type=checkbox], .radio input[type=radio], .radio-inline input[type=radio] {
        margin-left: -10px;
    }
</style>

<div class="row">
    <div class="col-md-12">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fs1 a2i_gn_edit2"></i>সার সংক্ষেপ ব্যবস্থাপনা
                </div>
            </div>
            <div class="portlet-body">
                <form class="summarynothiuser" action="#" method="post">
                    <table class="table table-striped table-bordered table-hover" id="">
                        <thead>
                        <tr>
                            <th class="text-center">মাননীয় প্রধানমন্ত্রী</th>
                            <th class="text-center">মাননীয় প্রতিমন্ত্রী</th>
                            <th class="text-center">মুখ্য সচিব</th>
                            <th class="text-center">সচিব</th>
                            <th class="text-center">পত্রের গ্রাহক</th>
                            <th class="text-center">নাম</th>
                            <th class="text-center">পদবি</th>
                            <th class="text-center">সেকশন</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="hasradio"><input type="radio" name="pmontri" value="-1"/></td>
                            <td class="hasradio"><input type="radio" name="protimontri" value="-1"/></td>
                            <td class="hasradio"><input type="radio" name="mukkhosochib" value="-1"/></td>
                            <td class="hasradio"><input type="radio" name="sochib" value="-1"/></td>
                            <td class="hasradio"><input type="radio" name="grahokPmo" value="-1"/></td>
                            <td class="text-center">নাই</td>
                            <td class="text-center">নাই</td>
                            <td class="text-center">নাই</td>
                        </tr>
                        <?php
                        foreach ($results as $rows):

                            ?>
                            <tr>
                                <td class="hasradio">
                                    <input <?php echo(($rows['summary_nothi_post_type'] == 4) ? 'checked=checked' : ''); ?>
                                            type="radio" name="pmontri" value="<?php echo $rows['id']; ?>"/></td>
                                <td class="hasradio">
                                    <input <?php echo(($rows['summary_nothi_post_type'] == 2) ? 'checked=checked' : ''); ?>
                                            type="radio" name="protimontri" value="<?php echo $rows['id']; ?>"/></td>
                                <td class="hasradio">
                                    <input <?php echo(($rows['summary_nothi_post_type'] == 6) ? 'checked=checked' : ''); ?>
                                            type="radio" name="mukkhosochib" value="<?php echo $rows['id']; ?>"/></td>
                                <td class="hasradio">
                                    <input <?php echo($rows['summary_nothi_post_type'] == 1 ? 'checked=checked' : ''); ?>
                                            type="radio" name="sochib" value="<?php echo $rows['id']; ?>"/></td>
                                <td class="hasradio">
                                    <input <?php echo(($rows['summary_nothi_post_type'] == 8) ? 'checked=checked' : ''); ?>
                                            type="radio" name="grahokPmo" value="<?php echo $rows['id']; ?>"/></td>
                                <td class=""><?php echo h($rows['name_bng']); ?></td>
                                <td class="text-center"><?php echo $rows['designation']; ?></td>
                                <td class="text-center"><?php echo $rows['unit_name_bng']; ?></td>
                            </tr>
                            <?php
                        endforeach;
                        ?>

                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="7"><input type="button" class="btn btn-primary" id="assign_fron_desk"
                                                   value="<?php echo __(SAVE) . ' করুন' ?>"/></td>
                        </tr>
                        </tfoot>
                    </table>
                </form>
            </div>
        </div>
    </div>
</div>


<script>

    $(document).on('click', '#assign_fron_desk', function () {
        Metronic.blockUI({
            target: '.portlet.box.green',
            boxed: true
        });
        $.ajax({
            url: '<?php echo $this->Url->build(['controller' => 'SummaryNothi', 'action' => 'assignUserforSummary']) ?>',
            method: 'post',
            data: $('.summarynothiuser').serialize(),
            success: function (res) {
                Metronic.unblockUI('.portlet.box.green');
                if (res == 1) {
                    toastr.success("সংরক্ষিত হয়েছে");
                } else {
                    toastr.error("সংরক্ষণ করা সম্ভব হচ্ছে না");
                }
            }
        });
    });

</script>