<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="panel panel-success">
            <div class="panel-heading">
                <div class="panel-title">
                    <?php echo __("সার-সংক্ষেপ ট্রাকিং") ?>
                </div>
            </div>
            <div class="panel-body">
                <div class="row form-group">
                    <div class="col-md-8 col-xs-12">
                        <?php
                        echo $this->Form->input('summary-nothi-sarok-no', ['label' => "ট্রাকিং নম্বর", "class" => 'form-control','placeholder'=>'ট্রাকিং নম্বর দিয়ে খুঁজুন'])
                        ?>
                    </div>

                </div>

                <div class="row form-actions">
                    <div class="col-md-6 text-right">
                        <a class="btn green btn-sm trackingformSearch">

                            <i class="fs1 a2i_gn_search1"></i> &nbsp;
                            <?php echo __(SEARCH) ?>
                        </a>
                    </div>
                    <div class="col-md-6 text-left">
                        <a class="btn red btn-sm" onclick="reset_it()" >

                            <i class="fa fa-eraser" aria-hidden="true"></i> &nbsp;
                            <?php echo __('রিসেট') ?>
                        </a>
                    </div>
                </div>
                <br>
                <div class="row form-group responsetable">
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-toastr/toastr.min.js"></script>
<script src="<?php echo CDN_PATH; ?>assets/admin/pages/scripts/ui-toastr.js"></script>
<script>
    function reset_it(){
        $('#summary-nothi-sarok-no').val('');
        $('.responsetable').html('');
    }

    toastr.options = {
        "closeButton": true,
        "debug": false,
        "positionClass": "toast-bottom-right"
    };

    $('.trackingformSearch').on('click', function () {
        var sarok_no = $.trim($('#summary-nothi-sarok-no').val());
        $('.responsetable').html('');

        if (sarok_no != '') {
            $('.responsetable').show();
            $('.responsetable').find('tbody').html('<tr><td class="text-center bold-text" colspan=5><i class="fa fa-spinner fa-spin"></i></td></tr>');
            $.ajax({
                type: "POST",
                url: '<?php echo $this->Url->build(['controller' => 'SummaryNothi', 'action' => 'getTracking']) ?>',
                dataType: "html",
                data: {sarok_no:EngFromBn(sarok_no)},
                success: function (response) {
                    $('.responsetable').html(response);
                },
                error: function () {
                    $('.responsetable').html('');
                    toastr.error("দু:খিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না");
                }
            });
        }
    });

</script>