<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption"><i class=""></i>নতুন ব্যবহারকারী   </div>
        <div class="tools">
        </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <?= $this->Form->create($user, ['action' => 'add', 'class' => 'form-horizontal', 'type' => 'file', 'id' => 'myUserForm']); ?>
        <div class="form-body">
            <div class="form-group">
                <label class="col-md-3 control-label">ব্যবহারকারীর নাম</label>

                <div class="col-md-4">
                    <div class="input-group">
							<span class="input-group-addon input-circle-left">
							<i class="fa fa-user"></i>
							</span>
                        <?= $this->Form->input('username', ['label' => false, 'class' => 'form-control input-circle-right', 'type' => 'text', 'placeholder' => 'ব্যবহারকারীর নাম']); ?>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">পাসওয়ার্ড</label>

                <div class="col-md-4">
                    <div class="input-group">
							<span class="input-group-addon input-circle-left">
							<i class="fa fa-key"></i>
							</span>
                        <?= $this->Form->input('password', ['label' => false, 'class' => 'form-control input-circle-right', 'type' => 'password', 'autocomplete'=>'new-password', 'placeholder' => 'পাসওয়ার্ড']); ?>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">পুনরায় পাসওয়ার্ড</label>

                <div class="col-md-4">
                    <div class="input-group">
							<span class="input-group-addon input-circle-left">
							<i class="fa fa-key"></i>
							</span>
                        <?= $this->Form->input('c_password', ['label' => false, 'class' => 'form-control input-circle-right', 'type' => 'password', 'placeholder' => 'পুনরায় পাসওয়ার্ড']); ?>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">Role (ভূমিকা)</label>

                <div class="col-md-4">
                    <?= $this->Form->input('user_role_id', ['options' => $userTypes, 'label' => false, 'class' => 'form-control', 'required'=>true, 'empty' => '--Select Role (ভূমিকা)--']); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">ছবি</label>

                <div class="col-md-4">
                    <input type='file' onchange="readURL(this);" name="photo_file" accept="image/jpeg, image/png" class="form-control"/>
                    <img id="image" src="#" alt="Upload Your Avatar" style="display:none"/>
                </div>
            </div>
        </div>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-3 col-md-9">
                    <button type="submit" class="btn   blue"><i class="fa fa-save"></i> সংরক্ষণ</button>
                    <button type="reset" class="btn   default"><i class="fa fa-repeat"></i> বাতিল</button>
                </div>
            </div>
        </div>
        <?= $this->Form->end(); ?>
        <!-- END FORM-->
    </div>
</div>


<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#image')
                    .attr('src', e.target.result)
                    .width(150)
                    .height(200);
                $('#image').show();
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    $(document).ready(function () {
        jQuery.extend(jQuery.validator.messages, {
            required: "ঘরটি অবশ্যই পূরণ করতে হবে।",
            email: "একটি বৈধ ইমেইল ঠিকানা লিখুন।",
            url: "একটি বৈধ URL প্রবেশ করুন।",
            date: "একটি বৈধ তারিখ লিখুন দয়া করে।",
            number: "দয়া করে একটি বৈধ নম্বর লিখুন।",
            digits: "অনুগ্রহ করে শুধুমাত্র সংখ্যা প্রবেশ করান।",
            equalTo: "আবার একই মান লিখুন দয়া করে।",
        });

        jQuery.validator.addMethod("minlength", function(value, element, length) {
            if (value.length >= length) {
                return this.optional(element) || /^[\w.]+$/i.test(value);
            }
        }, function (length, element) {
            if ($(element).val().length >= length) {
                return '';
            }
            return "কমপক্ষে "+enTobn(length)+" অক্ষর দিন";
        });

        jQuery.validator.addMethod("alphanumeric", function(value, element) {
            return this.optional(element) || /^[\w.]+$/i.test(value);
        }, "দয়া করে ইংরেজি অক্ষর, নম্বর ও আন্ডারস্কোর দিয়ে চেষ্টা করুন");

        jQuery.validator.addMethod("letters", function(value, element) {
            if (value.match(/[A-z]/)) {
                return this.optional(element) || /^[\w.]+$/i.test(value);
            }
        }, "অন্তত একটি A-Z অথবা a-z");

        jQuery.validator.addMethod("numbers", function(value, element) {
            if (value.match(/\d/)) {
                return this.optional(element) || /^[\w.]+$/i.test(value);
            }
        }, "অন্তত একটি নম্বর দিতে হবে");

        $('#myUserForm').validate({ // initialize the plugin
            rules: {
                username: {
                    required: true,
                    alphanumeric: true,
                    minlength: 3
                },
                password: {
                    required: true,
                    nowhitespace: true,
                    letters: true,
                    numbers: true,
                    minlength: 6
                },
                c_password: {
                    equalTo : "#password"
                }
            },
            // submitHandler: function (form) { // for demo
            //     alert('valid form submitted'); // for demo
            //     return false; // for demo
            // }
        });

    });
</script>