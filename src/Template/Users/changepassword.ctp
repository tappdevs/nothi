<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption"><i class=""></i>Change password</div>
    </div>
    <div class="portlet-body form"><br><br>
        <?php echo $this->Form->create($user); ?>

        <div class="form-horizontal">
            <div class="form-group">
                <label class="col-sm-4 control-label">Password</label>

                <div class="col-sm-3">
                    <?php echo $this->Form->input('password', array('id' => 'password', 'label' => false, 'value' => '', 'type' => 'text', 'required' => true, 'class' => 'form-control', 'placeholder' => 'Password')); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">Confirm Password</label>

                <div class="col-sm-3">
                    <?php echo $this->Form->input('cpassword', array('id' => 'cpassword', 'label' => false, 'type' => 'text', 'class' => 'form-control', 'required' => true, 'placeholder' => 'Confirm Password')); ?>
                </div>
            </div>
        </div>
        <?php //echo $this->element('update'); ?>
        <div class="form-actions">
            <button type="submit" id="btn_submit" class="btn btn-success uppercase">Submit</button>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
    <script type="text/javascript">
        $(function () {
            $("#btn_submit").click(function (e) {
                if ($("#password").val() != $("#cpassword").val()) {
                    alert("The passwords didn't match!");
                    return false;
                }
            });
        });
    </script>