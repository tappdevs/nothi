<div class="portlet box red">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-warning"></i> ডাটাবেজ আপডেট করা হয়নি
        </div>
    </div>
    <div class="portlet-body">
        <div class="alert-box alert alert-danger">
                দুঃখিত! আপনার অফিসের ডাটাবেজ আপডেট করা হয়নি। বিষয়টি নথি সাপোর্ট টিমকে অবহিত করা হয়েছে। অতি শীঘ্রই সমস্যার সমাধান করা হবে। কিছুক্ষণ পর পুনরায় চেষ্টা করুন।<br/><br/> সাময়িক অসুবিধার জন্য আমরা আন্তরিকভাবে দুঃখিত।<br/><br/>
                ধন্যবাদ।
        </div>

    </div>
</div>