
<div class="portlet box red">
    <div class="portlet-title">
        <div class="caption">
            কর্মকর্তার লগইন বর্ণনা
        </div>

    </div>
    <div class="portlet-body">
        <?php if($user['user_role_id']<=1) : ?>
        <?= $cell = $this->cell('OfficeSelection', ['entity' => '']) ?>
        <?php endif; ?>
        <div class="row">

            <div class="col-sm-4 form-group form-horizontal">
                <?php
                if($user['user_role_id']<=1) {
                    echo $this->Form->input('office_id', array(
                        'label' => __('Office'),
                        'class' => 'form-control',
                        'empty' => '--বাছাই করুন--'
                    ));
                }else{
                    echo $this->Form->input('office_id', array(
                        'label' => __('Office'),
                        'class' => 'form-control',
                        'options'=>[$office_id=>$office_name]
                    ));
                }
                
                ?>
            </div>
            <div class="col-sm-4 form-group form-horizontal">
                <?php
                echo $this->Form->input('office_unit_id', array(
                    'label' => __('Unit'),
                    'class' => 'form-control',
                    'empty' => '--বাছাই করুন--'
                ));
                ?>
            </div>
            <div class="col-sm-4 form-group form-horizontal">
                <?php
                echo $this->Form->input('office_unit_organogram_id', array(
                    'label' => __('Organogra'),
                    'class' => 'form-control',
                    'empty' => '--বাছাই করুন--'
                ));
                ?>

            </div>
        </div>
        <div class="row">
            <div class="col-sm-5 form-group form-horizontal">
                <div class="hidden-print ">
                        <div id="dashboard-report-range" class=" tooltips btn btn-sm btn-default"
                             data-container="body"
                             data-placement="bottom" data-original-title="তারিখ নির্বাচন করুন">
                            <i class="icon-calendar"></i>&nbsp; <span
                                class="thin uppercase block">তারিখ নির্বাচন করুন</span>&nbsp;
                            <i class="glyphicon glyphicon-chevron-down"></i>
                        </div>
                    </div>
            </div>
            <div class="col-sm-3 form-group form-horizontal">
                <button class="btn btn-info" id="search"><?= __('Search') ?></button>
            </div>
        </div>
        <div class="text-center">
            <div class="help-block">
                **একদিনে একজন কর্মকর্তা একাধিকবার লগইন করলে,সর্বপ্রথম লগইন তথ্য দেখানো হবে।
            </div>
        </div>

        <div class="table-scrollable showHistory">
            
        </div>

    </div>
</div>


<script src="<?php echo CDN_PATH; ?>daptorik_preview/js/report_date_range.js"></script>

<script type="text/javascript">
    
    var EmployeeLoginHistory = {
        checked_node_ids: [],
        start_time:'',
        end_time:'',
        loadMinistryWiseLayers: function () {
            OfficeSetup.loadLayers($("#office-ministry-id").val(), function () {

            });
        },
        loadMinistryAndLayerWiseOfficeOrigin: function () {
            OfficeSetup.loadOffices($("#office-ministry-id").val(), $("#office-layer-id").val(), function () {

            });
        },
        loadOriginOffices: function () {
            OfficeSetup.loadOriginOffices($("#office-origin-id").val(), function () {
            });
        },
        loadOfficeUnits: function () {
            PROJAPOTI.ajaxSubmitDataCallback('<?php echo $this->request->webroot ?>' + 'officeManagement/loadOfficeUnits',
                    {'office_id': $("#office-id").val()}, 'html',
                    function (response) {
                        $("#office-unit-id").html(response);
                    });
        },
        loadOfficeUnitDesignations: function () {
            PROJAPOTI.ajaxSubmitDataCallback('<?php echo $this->request->webroot ?>' + 'officeManagement/loadOfficeUnitOrganogramsWithName',
                    {'office_unit_id': $("#office-unit-id").val()}, 'json',
                    function (response) {
                        PROJAPOTI.projapoti_dropdown_map_with_title("#office-unit-organogram-id", response, "--বাছাই করুন--",'',{'key' : 'id','value' : 'designation','title' : 'name'  });
                    });
        },
        loadHistory: function() {
            PROJAPOTI.ajaxLoadWithRequestData('<?php echo $this->request->webroot ?>' + 'users/loginAuditAjax',
                    {'office_id':$("#office-id").val(),office_unit_id:$("#office-unit-id").val(),office_unit_organogram_id:$("#office-unit-organogram-id").val(),start_time:this.start_time, end_time: this.end_time},'.showHistory');
            $("#search").removeAttr('disabled');
        },
        loadPagination: function(url) {
            PROJAPOTI.ajaxLoad(url,'.showHistory');
        }
    };
    
    var SortingDate = function () {

        return {
            //main function to initiate the module
            init: function (startDate, endDate) {
                EmployeeLoginHistory.start_time = startDate;
                EmployeeLoginHistory.end_time = endDate;
            }

        };

    }();

    $(function () {
         DateRange.init();
        DateRange.initDashboardDaterange();
          <?php if($user['user_role_id']> 2) { ?>
                    EmployeeLoginHistory.loadOfficeUnits();
        <?php } ?>
        
        $("#office-ministry-id").bind('change', function () {
            EmployeeLoginHistory.loadMinistryWiseLayers();
        });
        $("#office-layer-id").bind('change', function () {
            EmployeeLoginHistory.loadMinistryAndLayerWiseOfficeOrigin();
        });
        $("#office-origin-id").bind('change', function () {
            EmployeeLoginHistory.loadOriginOffices();
        });
        $("#office-id").bind('change', function () {
            EmployeeLoginHistory.loadOfficeUnits();
        });
        $("#office-unit-id").bind('change', function () {
            EmployeeLoginHistory.loadOfficeUnitDesignations();
        });
        $("#search").bind('click', function () {
            $("#search").attr('disabled','disabled');
             EmployeeLoginHistory.loadHistory();
        });
        
        $(document).off('click','.pagination a').on('click','.pagination a',function(ev){
            ev.preventDefault();
            EmployeeLoginHistory.loadPagination($(this).attr('href'));
        });
    });

</script>
