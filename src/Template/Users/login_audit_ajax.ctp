<table class="table table-bordered table-advance table-hover">
    <thead>
        <tr>
            <th class="text-center"><?php echo __("No") ?></th>
            <th class="text-center"><?php echo __("Name") ?></th>
            <th class="text-center"><?php echo __("Office") ?></th>
            <th class="text-center"><?php echo __("Unit") ?></th>
            <th class="text-center"><?php echo __("Organogra") ?></th>
            <?php
            if ($is_superAmdin) {
                ?>
                <th class="text-center"><?php echo __("Login").' '.__("Information") ?></th>
            <?php } ?>
            <th class="text-center"><?php echo __("Login").' '.__("Time") ?></th>
        </tr>
    </thead>
    <tbody >


        <?php
        if (!empty($loginHistory)) {
            foreach ($loginHistory as $key => $value) {
                $network_information = json_decode($value['network_information']);
//                 pr($network_information);die;
                ?>
                <tr>
                    <td class="text-center"><?= Cake\I18n\Number::format((($page - 1) * $limit) + $key + 1) ?></td>
                    <td class="text-center"><?= h($value['employee_name']) ?></td>
                    <td class="text-center"><?= $value['office_name'] ?></td>
                    <td class="text-center"><?= $value['unit_name'] ?></td>
                    <td class="text-center"><?= $value['designation'] ?></td>
                    <?php
                    if ($is_superAmdin) {
                        ?>
                        <td class="text-center"><?= __('IP').': '.$value['client_ip']."<br/>".__('Device').': '.$value['device_name']."<br/>".__('Browser').': '.$value['browser_name']."<br/>".__('Browser').' আইডি : '.$network_information->DeviceId ?></td>
                    <?php } ?>
                    <td class="text-center"><?= $value['login_time'] ?></td>
                </tr>

                <?php
            }
        }
        ?>
    </tbody>
</table>
<div class="actions text-center">
    <ul class="pagination pagination-sm">
        <?php
        echo $this->Paginator->first(__('প্রথম', true), array('class' => 'number-first'));
        echo $this->Paginator->prev('<<', array('tag' => 'li', 'escape' => false),
            '<a href="#" class="btn btn-sm blue">&laquo;</a>',
            array('class' => 'prev disabled btn btn-sm blue', 'tag' => 'li', 'escape' => false));
        echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true,
            'currentClass' => 'active', 'currentTag' => 'a', 'reverse' => false));

        echo $this->Paginator->next('>>', array('tag' => 'li', 'escape' => false),
            '<a href="#" class="btn btn-sm blue ">&raquo;</a>',
            array('class' => 'next disabled', 'tag' => 'li', 'escape' => false));
        echo $this->Paginator->last(__('শেষ', true), array('class' => 'number-last'));
        ?>
    </ul>

</div>