<?php
$path = $this->request->webroot;
if (CDN == 1) {
    $path = 'http://cdn1.nothi.gov.bd/webroot/';
}
?>
<style>
    .login-6.leftside {
        margin: 10pt 0 0 0pt;
        float: left;
        padding: 2px;
        background: #FFFFFF;
    }

    .login-6.rightside {
        margin: 10pt 0pt 0 0;
        float: right;
    }

    .login-form {
        clear: both;
        padding-top: 10%;
    }

    .user-login-5 .login-container > .login-content {
        margin-top: 2.5%;
    }

    .portlet.light {
        padding: 0px;
        padding-top: 10px;
    }

    input {
        width: 100% !important;
        padding: 10px !important;
        border: none #a0a9b4 !important;
        border-bottom: 1px solid !important;
        color: #868e97 !important;
        font-size: 14px !important;
        margin-bottom: 30px !important;
    }

    .login-copyright {
        font-family: "helvetica";
        font-size: 8pt;
    }

    .user-login-5 .login-container > .login-footer .login-copyright {
        margin-top: 0px;
    }

    .login-right {
        background: #ffffff;
        height: 100vh;
        opacity: .85;
    }

    .user-login-5{
        background: url(<?= $path; ?>img/bg_image.jpg) no-repeat center center #CDE7A8 fixed;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
    }

    .blog-quote-label:before {
        color: #fff;
        content: open-quote;
        font-size: 1.4em;
    }
    .blog-quote-label:after {
        color: #fff;
        content: close-quote;
        font-size: 1.4em;
    }

</style>
<div class="user-login-5">
    <div class="row bs-reset">
        <div class="col-md-6 login-container bs-reset">
            <div class="login-content">
                <div class="text-center">
                    <img src="<?= $path . 'img/nothi_logo_login.png' ?>" alt="BD Gov" height="40">
                </div>
                <div style="clear:both"></div>
                <div style="padding-top:5%;">
                    <noscript>
                        <div id="Script-disable-div" class="alert alert-danger">
                            দুঃখিত, আপনার ব্রাউজার জাভাস্ক্রিপ্ট সমর্থন করে না! অনুগ্রহপুর্বক জাভাস্ক্রিপ্ট চালু করুন
                            অথবা ব্রাউজার আপডেট করুন।
                        </div>
                    </noscript>
                    <div class="alert alert-danger" id="GChrome" style="display: none;">
                        অনুগ্রহপূর্বক নথি অ্যাপ্লিকেশনের সকল কার্যক্রম উপভোগ করতে Google Chrome ব্যবহার করুন। Google
                        Chrome ইন্সটল না থাকলে
                        <a target="_blank" href="https://www.google.com/chrome/"> এখানে ক্লিক করুন </a>।
                    </div>
                    <div class="portlet light GChrome" style="display: none;">
                        <div class="portlet-body">
                            <?php echo $this->Flash->render(); ?>
                            <div class="tabbable-custom nav-justified">
                                <ul class="nav nav-tabs nav-justified">
                                    <li class="active">
                                        <a href="#tab_loginname" data-toggle="tab">ইউজার আইডি</a>
                                    </li>
                                    <li>
                                        <a href="#tab_userid" data-toggle="tab">ইউজার নেম</a>
                                    </li>
                                    <li>
                                        <a href="#tab_password" data-toggle="tab">পাসওয়ার্ড ভুলে গেছেন?</a>
                                    </li>
                                </ul>

                                <div class="tab-content">
                                    <!-- >ইউজার আইডি TAB -->
                                    <div class="tab-pane active" id="tab_loginname">
                                        <?= $this->Form->create(null,['class'=>'login-form','url'=>['_name'=>'login'],'autocomplete'=>'off']) ?>
                                        <?= $this->Form->hidden('fingerprint', ['id' => 'fingerprint']) ?>
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <?= $this->Form->input('username', ['class' => 'form-control', 'autocomplete'=>'off', 'placeholder' => 'ইউজার আইডি','label'=>false]) ?>
                                            </div>
                                            <div class="col-xs-6">
                                                <?= $this->Form->input('password', ['class' => 'form-control', 'autocomplete'=>'off', 'placeholder' => 'পাসওয়ার্ড','label'=>false,'type'=>'password']) ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <a href="<?= $this->Url->build(['_name' => 'nagorikAbedon']) ?>"
                                                   class="btn purple form-group"
                                                   style="text-decoration: none;"><i class="efile-citizen1"></i>&nbsp;নাগরিক কর্নার&nbsp;</a>
                                            </div>
                                            <div class="col-xs-6 text-right">
                                                <button class="btn blue" type="submit">প্রবেশ <i class="fs1 a2i_gn_login3"></i></button>
                                            </div>
                                        </div>
                                        <?= $this->Form->end() ?>
                                    </div>
                                    <!-- END >ইউজার আইডি TAB -->
                                    <!-- ইউজার নেম TAB -->
                                    <div class="tab-pane" id="tab_userid">
                                        <?= $this->Form->create(null,['class'=>'login-form','url'=>['_name'=>'login']]) ?>
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <?= $this->Form->input('alias', ['class' => 'form-control', 'autocomplete'=>'off', 'placeholder' => 'ইউজার নেম','label'=>false]) ?>
                                            </div>
                                            <div class="col-xs-6">
                                                <?= $this->Form->input('password', ['class' => 'form-control', 'autocomplete'=>'off', 'placeholder' => 'পাসওয়ার্ড','id'=>'password2','label'=>false,'type'=>'password']) ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <a href="<?= $this->Url->build(['_name' => 'nagorikAbedon']) ?>"
                                                   class="btn purple form-group"
                                                   style="text-decoration: none;"><i class="efile-citizen1"></i>&nbsp;নাগরিক কর্নার&nbsp;</a>
                                            </div>
                                            <div class="col-xs-6 text-right">
                                                <button class="btn blue" type="submit">প্রবেশ <i class="fs1 a2i_gn_login3"></i></button>
                                            </div>
                                        </div>
                                        <?= $this->Form->end() ?>
                                    </div>
                                    <!-- END ইউজার নেম TAB -->
                                    <!-- পাসওয়ার্ড ভুলে গেছেন TAB -->
                                    <div class="tab-pane" id="tab_password">
                                        <?= $this->Form->create(null,['class'=>'login-form forget-form','action'=>'javascript:;']) ?>
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <input type="text" placeholder="ইউজার আইডি" class="form-control"
                                                       autocomplete="off" name="username" autocomplete='off'/></div>
                                            <div class="col-xs-6">
                                                <input type="text" placeholder="ইমেইল" autocomplete='off' class="form-control"
                                                       name="email"/></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <a href="<?= $this->Url->build(['_name' => 'nagorikAbedon']) ?>"
                                                   class="btn purple form-group"
                                                   style="text-decoration: none;"><i class="efile-citizen1"></i>&nbsp;নাগরিক কর্নার&nbsp;</a>
                                            </div>
                                            <div class="col-xs-6 text-right">
                                                <button class="btn green" type="button">অনুরোধ করুন <i class="fs1 a2i_gn_send1"></i></button>
                                            </div>
                                        </div>
                                        <?= $this->Form->end() ?>
                                    </div>
                                    <!-- END পাসওয়ার্ড ভুলে গেছেন TAB -->
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="efile-help4"></i>&nbsp;হেল্প ডেস্ক
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-7">
                                    <i class="glyphicon glyphicon-phone"></i> +৮৮ ০১৩১৫-৬৫৪০৪৭
                                    <br/>
                                    <i class="glyphicon glyphicon-phone"></i> +৮৮ ০১৩১৫-৬৫৪০৪৮
                                    <br/>
                                    <i class="glyphicon glyphicon-phone"></i> +৮৮ ০১৩১৫-৬৫৪০৪৯
                                    <br/>
                                    <i class="glyphicon glyphicon-envelope"></i> support@nothi.org.bd
                                </div>
                                <div class="col-xs-5">
                                    <div class="form-group ">
                                        <icon class="glyphicon glyphicon-bullhorn"></icon>
                                        <a target="__tab" class="btn-link bold font-md" style="text-decoration: none;"
                                           href="<?= \Cake\Routing\Router::url(['_name' => 'release-note']); ?>">আপডেট </a><br/>
                                        <icon class="glyphicon glyphicon-info-sign"></icon>
                                        <a target="__tab" class="btn-link bold font-md" style="text-decoration: none;"
                                           href="<?= $this->request->webroot ?>FAQ">আপনার জিজ্ঞাসা</a><br/>
                                        <icon class="glyphicon glyphicon-play-circle"></icon>
                                        <a target="__tab" class="btn-link bold font-md" style="text-decoration: none;"
                                           href="https://www.youtube.com/watch?v=hmUUBt7G8Gs&amp;list=PLVCEN663EzMFwATrFC6ULiM64U1FRyeAu">ভিডিও
                                            টিউটোরিয়াল</a><br/>
                                        <img src="<?= $this->request->webroot ?>img/fb.png" style="margin-left:-2px;"/>
                                        <a target="__tab" class="btn-link bold font-md" style="text-decoration: none;"
                                           href="https://fb.com/groups/nothi/">নথি (ফেসবুক গ্রুপ)</a>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center" style="font-size:12pt">
                                <a target="__tab"
                                   href="<?= $this->Url->build(['controller' => 'Dashboard', 'action' => 'showUserManualList']) ?>">
                                    <i class="glyphicon glyphicon-file"></i> ব্যবহার সহায়িকা (ভার্সনঃ ১৬)
                                </a>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="text-center">
                                    একসেস টু ইনফরমেশন &copy;<?= $Year ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix" style="background-color: #FFFFFF; padding:5px 1px;">
                    <div class="row">
                        <div class="col-xs-6">
                            <img src="<?= $path . 'img/ban-gov_logo.png' ?>" alt="BD Gov" height="30">
                            <img src="<?= $path . 'img/a_a2i-logo.jpg' ?>" alt="BD Gov" height="30">
<!--                            <img src="--><?//= $path . 'img/doict_logo.jpg' ?><!--" alt="BD Gov" height="30">-->
<!--                            <img src="--><?//= $path . 'img/bcc_logo.png' ?><!--" alt="BD Gov" height="30">-->
                        </div>
<!--                        <div class="col-xs-6 text-right"  style="font-size:10pt;padding-top: 5px;">-->
<!--                            <img src="--><?//= $this->request->webroot ?><!--img/tappware_final_logo.png" style="height: 20px;">-->
<!--                            উন্নয়ন ও বাস্তবায়ন সহযোগিতায় <a href="http://tappware.com" target="_tab">ট্যাপওয়্যার </a>-->
<!--                        </div>-->
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6 login-right bs-reset">
            <div class="panel" style="background: transparent;border:0px">
                <div class="panel-body" style="padding:0px">
                    <?php if(!empty($office_messages['notice'])){ ?>

                        <div class="col-xs-12" style="padding-left:1px">
                            <div class="mt-element-ribbon bg-grey" style="background-color: white !important">
                                <div class="ribbon ribbon-border-hor ribbon-clip ribbon-color-primary uppercase">
                                    <div class="ribbon-sub ribbon-clip"></div>
                                    নোটিশ
                                </div>
                                <div class="ribbon-content marquee" style="max-height:300px;overflow:hidden;overflow-y: auto">
                                    <?php
                                    foreach ($office_messages['notice'] as $officeNotices){
                                        echo "<div><h3>{$officeNotices['title']}</h3><div>". $officeNotices['description'] ."</div></div><hr/>";
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>

                       <?php } ?>
                  <?php
                        if(!empty($office_messages['other'])){ $other = count($office_messages['other']); ?>
                        <?php foreach ($office_messages['other'] as $office_message){?>

                            <div class="col-xs-<?=$other>1?'6':'12' ?>"  style="padding-left:1px">
                                <div class="mt-element-ribbon bg-grey" style="background-color: white !important">
                                    <div class="ribbon ribbon-border-hor ribbon-clip ribbon-color-danger uppercase">
                                        <div class="ribbon-sub ribbon-clip"></div>
                                        <?= $office_message['title'];  ?>
                                    </div>
                                    <div class="ribbon-content"style="max-height:300px;overflow:hidden;overflow-y: auto">
                                        <?= $office_message['description'];  ?>
                                    </div>
                                </div>
                            </div>
                            <?php  } ?>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade modal-purple" id="responsiveOnuccedModal" role="dialog" >
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">

                <button type="button" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<script src="//cdn.jsdelivr.net/npm/jquery.marquee@1.5.0/jquery.marquee.min.js" type="text/javascript"></script>
<script>
    function pad(str, max) {
        str = str.toString();
        return str.length < max ? pad("0" + str, max) : str;
    }

    $(function () {
        $('.marquee').marquee({
            speed: 50,
            gap: 50,
            delayBeforeStart: 0,
            direction: 'up',
            duplicated: true,
            pauseOnHover: true
        });
        $(document).on('change', '[name=username]', function () {
            var loginid = $(this).val();
            var start = loginid.substr(0, 1);
            var restof = loginid.substr(1);
            loginid = start + pad(restof, 11);
            $(this).val(loginid);
        });

        var client = new ClientJS();
        var fingerprint = client.getFingerprint(); // Calculate Device/Browser
        if (!client.isChrome()) {
            $("#GChrome").show();
			$(".GChrome").remove();
        }else{
			$(".GChrome").show();
        }
        $("#fingerprint").val(fingerprint);
    });
    function getPopUp(href, title) {
        $('#responsiveOnuccedModal').find('.modal-title').text('');
        $('#responsiveOnuccedModal').find('.modal-body').html('');
        $.ajax({
            type: 'POST',
            url: "<?php echo $this->request->webroot; ?>LoginPageSettings/NoticePopup/" + href,
            data: {},
            success: function (response) {
                $('#responsiveOnuccedModal').modal('show');
                $('#responsiveOnuccedModal').find('.modal-title').text(title);
                $('#responsiveOnuccedModal').find('.modal-body').html(response);
            }
        });

    }
    $(document).off('click', '.NoticePopup').on('click', '.NoticePopup', function (e) {
        e.preventDefault();
        var title = $(this).attr('title').length > 0 ? $(this).attr('title') : $(this).attr('data-original-title');
        getPopUp($(this).attr('href'), title);
    });
</script>