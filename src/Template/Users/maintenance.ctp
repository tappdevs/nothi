<?php
$path = $this->request->webroot;
if (CDN == 1) {
    $path = 'http://cdn1.nothi.gov.bd/webroot/';
}
?>
<style>
    .login-6.leftside {
        margin: 10pt 0 0 0pt;
        float: left;
    }

    .login-6.rightside {
        margin: 10pt 0pt 0 0;
        float: right;
    }

    .login-form {
        clear: both;
        padding-top: 10%;
    }

    .user-login-5 .login-container > .login-content {
        margin-top: 5%;
    }

    .portlet.light {
        padding: 0px;
        padding-top: 10px;
    }

    input {
        width: 100% !important;
        padding: 10px !important;
        border: none #a0a9b4 !important;
        border-bottom: 1px solid !important;
        color: #868e97 !important;
        font-size: 14px !important;
        margin-bottom: 30px !important;
    }

    .login-copyright {
        font-family: "helvetica";
        font-size: 8pt;
    }

    .user-login-5 .login-container > .login-footer .login-copyright {
        margin-top: 0px;
    }

    .list-group-item{
        border:none;
    }
</style>
<div class="user-login-5">
    <div class="row bs-reset">
        <div class="col-md-6 login-container bs-reset">
            <div class="login-content">
                <div class="login-6 leftside">
                    <img src="<?= $path . 'img/ban-gov_logo.png' ?>" alt="BD Gov" height="40">
                    <img src="<?= $path . 'img/a_a2i-logo.jpg' ?>" alt="BD Gov" height="40">
                    <img src="<?= $path . 'img/doict_logo.jpg' ?>" alt="BD Gov" height="40">
                    <img src="<?= $path . 'img/bcc_logo.png' ?>" alt="BD Gov" height="40">
                </div>
                <div class="login-6 rightside">
                    <img src="<?= $path . 'img/nothi_logo_login.png' ?>" alt="BD Gov" height="40">
                </div>
                <div style="clear:both"></div>
                <div style="padding-top:15%;margin-bottom: 10%;">
                    <div class="alert alert-danger">
<!--                    নথি অ্যাপ্লিকেশন উন্নতিকরণের কাজ চলছে। সাময়িক অসুবিধার জন্য আমরা দুঃখিত।-->
                        নথি অ্যাপ্লিকেশন এর জরুরি কারিগরি মানউন্নয়নের কাজ চলছে। সাময়িক অসুবিধার জন্য আমরা দুঃখিত।
                    </div>
                    <div class="portlet light">
                        <div class="portlet-body">
                            <?php echo $this->Flash->render(); ?>
                        </div>
                    </div>
                </div>

                <!-- END PROFILE CONTENT -->

                <div class="row">
                    <div class="col-md-12">
                        <ul class=" list-group">
                            <li class="list-group-item">
                                <a class="btn-link form-group"
                                   href="/nothi/Dashboard/downloadUsermanual?file=eFile_User_Manual_v13&amp;ext=pdf"
                                   target="__tab"><i class="glyphicon glyphicon-download"></i> ব্যবহার সহায়িকা (ভার্শনঃ ১৬)</a>
                            </li>
                            <li class="list-group-item">
                                <a href="#" class="btn-link form-group" style="text-decoration: none;"><i
                                            class="fa fa-user"></i>&nbsp;হেল্প
                                    ডেস্ক
                                    <i class="glyphicon glyphicon-phone"></i> +৮৮ ০১৩১৫-৬৫৪০৪৭ | <i class="glyphicon glyphicon-phone"></i> +৮৮ ০১৩১৫-৬৫৪০৪৮ |
                                            <i class="glyphicon glyphicon-phone"></i> +৮৮ ০১৩১৫-৬৫৪০৪৯ |</a>
                            </li>
                    </div>
                </div>
            </div>
            <div class="login-footer">
                <div class="row bs-reset">
                    <div class="col-xs-12 bs-reset">
                        <ul class="login-social">
                            <li>
                                <a target="__tab" class="bnt-link font-md" style="text-decoration: none;"
                                   title="আপডেট"
                                   href="<?= $this->Url->build(['_name' => 'release-note']) ?>">
                                    <i style="font-size: 28px!important;" class="fa fa-folder-open fa-lg"
                                       aria-hidden="true"> </a></i>
                            </li>
                            <li>
                                <a target="__tab" class="bnt-link font-md" style="text-decoration: none;"
                                   title="আপনার জিজ্ঞাসা" href="<?= $this->Url->build(['_name' => 'faq']) ?>"> <i
                                            style="font-size: 28px!important;" class="fa fa-question-circle fa-lg"
                                            aria-hidden="true"></i></a>
                            </li>
                            <li>
                                <a target="__tab" class="btn-link font-md" style="text-decoration: none;"
                                   title="ফেসবুক গ্রুপ" href="https://fb.com/groups/nothi/"><i
                                            style="font-size: 28px!important;"
                                            class="fa fa-facebook-official fa-lg" aria-hidden="true"> </i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6 bs-reset">
            <div class="login-bg">
                <div class="login-copyright ">
<!--                    <div class="pull-left">-->
<!--                        <img src="--><?//= $this->request->webroot ?><!--img/tappware_final_logo.png" style="height: 24px;"> উন্নয়ন ও বাস্তবায়ন সহযোগিতায় <a href="http://tappware.com" target="_tab">ট্যাপওয়্যার </a>-->
<!--                    </div>-->
                    <div class="pull-right">
                        <img src="<?= $this->request->webroot ?>assets/admin/layout4/img/a2i.png" style="height: 24px;"> কপিরাইট <?= $Year ?>, একসেস টু ইনফরমেশন</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo CDN_PATH; ?>js/client.min.js" type="text/javascript"></script>
<script>
    function pad(str, max) {
        str = str.toString();
        return str.length < max ? pad("0" + str, max) : str;
    }
    $(function () {
        $(document).on('change', '[name=username]', function () {
            var loginid = $(this).val();
            var start = loginid.substr(0, 1);
            var restof = loginid.substr(1);
            loginid = start + pad(restof, 11);
            $(this).val(loginid);
        });

        var client = new ClientJS();
        var fingerprint = client.getFingerprint(); // Calculate Device/Browser
        if (!client.isChrome()) {
            $("#GChrome").show();
        }
        $("#fingerprint").val(fingerprint);
    });

</script>