<div class="panel-group accordion" id="accordion1">
    <?php foreach ($video_tutorial_links as $key => $video_tutorial_link): ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapse_<?=$key?>">
                        <img src="https://img.youtube.com/vi/<?=$video_tutorial_link['id']?>/2.jpg" style="width: 50px;" />
                        <?=$video_tutorial_link['name']?>
                    </a>
                </h4>
            </div>
            <div id="collapse_<?=$key?>" class="panel-collapse collapse">
                <div class="panel-body" style="font-size : 12pt!important;">
                    <iframe style="width:100%;height:calc(100vh - 204px)" src="<?=$video_tutorial_link['url']?>" frameborder="0" allow="encrypted-media" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>