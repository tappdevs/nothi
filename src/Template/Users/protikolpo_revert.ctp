
<div class="modal fade modal-purple height-auto" id="protikolpoSetting" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">

		<div class="modal-content">
			<div class="modal-header">
				<?php if ($is_logout_button): ?>
					<a href="<?=$this->request->webroot?>logout" class="btn btn-danger btn-sm pull-right">লগ আউট</a>
                <?php else: ?>
					<button type="button" data-dismiss="modal" aria-hidden="true">×</button>
                <?php endif; ?>
				<h4 class="modal-title">প্রতিকল্প ব্যবস্থাপনা</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<h3><i><?= $protikolpo_list['user_name'] ?></i> আপনার পদসমূহ</h3>
						<table class="table table-bordered table-striped table-hover">
							<thead>
							<tr>
								<th>#</th>
								<th>পদের নাম</th>
								<th>প্রতিকল্প অবস্থা</th>
							</tr>
							</thead>
							<tbody>
							<?php foreach ($protikolpo_list['data'] as $key => $protikolpo): ?>
							<tr>
								<td><?=enTobn($key+1)?></td>
								<td><?=$protikolpo['protikolpo']['office_unit_organogram_name']?></td>
								<td>
									<?=$protikolpo['protikolpo']['employee_name']?>,
									<?=$protikolpo['protikolpo']['office_unit_name']?>,
									<?=$protikolpo['protikolpo']['office_name']?>
								</td>
							</tr>
                            <?php endforeach; ?>
							</tbody>
						</table>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						প্রতিকল্প কার্যকরঃ
						<?=enTobn($protikolpo_list['data'][0]['start_date']->format('d-m-Y')) ?> থেকে
						<?=enTobn($protikolpo_list['data'][0]['end_date']->format('d-m-Y')) ?>
						<input type="hidden" name="employee_record_id" value="<?=$protikolpo_list['employee_record_id']?>" />
						<hr/>
						<fieldset style="    border: solid 1px #582071;padding: 10px">
							<legend style="width: auto !important;background-color: #8e44ad;padding: 5px 10px;border: solid 1px #582071;margin-bottom: 5px;color: white;">প্রতিকল্প ব্যবস্থাপনা পরিবর্তন</legend>
							<div class="row">
								<div class="col-md-4">প্রতিকল্প শেষ তারিখ</div>
								<div class="col-md-4"><input type="text" name="protikolpo_end_date" class="form-control date-picker" value="<?=$protikolpo_list['data'][0]['end_date']->format('m/d/Y')?>" data-date-start-date="+0d" /></div>
								<div class="col-md-4"><button onclick="changeProtikolpoEndDate(this)" class="btn btn-success"><i class="fa fa-cog"></i> পরিবর্তন করুন</button></div>
							</div>
						</fieldset>
						<hr/>
						<div class="text-center">
							<button class="btn btn-danger" onclick="stopProtikolpo(this)"><i class="fa fa-trash"></i> প্রতিকল্প স্থগিত</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	$(document).ready(function() {
	    $("#protikolpoSetting").modal('show');
	});
    function changeProtikolpoEndDate(element) {
        $(element).html('<i class="fa fa-cog fa-spin"></i> হালনাগাদ হচ্ছে...');
        $(element).addClass('disabled');
        if(isNaN(new Date($("input[name=protikolpo_end_date]").val()))) {
            toastr.error('দয়া করে প্রতিকল্পের শেষ তারিখ নির্বাচন করুন');
            return false;
        }
        var endDate = $("input[name=protikolpo_end_date]").val();
        var employeeRecordId = $("input[name=employee_record_id]").val();
        $.ajax({
            type: 'POST',
            url: "<?php echo $this->Url->build(['controller' => 'ProtikolpoSettings', 'action' => 'updateEndDate']) ?>",
            data: {"end_date": endDate, 'employee_record_id' : employeeRecordId},
	        async: true,
            success: function (response) {
                if (response.status == 'success') {
                    toastr.success(response.msg);
                    $(element).html('<i class="fa fa-cog"></i> পরিবর্তন করুন');
                    $(element).removeClass('disabled');
                } else {
                    toastr.error(response.msg);
                    $(element).html('<i class="fa fa-cog"></i> পরিবর্তন করুন');
                    $(element).removeClass('disabled');
                }
            }
        });
    }
	function stopProtikolpo(element) {
        $(element).html('<i class="fa fa-cog fa-spin"></i> প্রতিকল্প হালনাগাদ হচ্ছে...');
        $(element).addClass('disabled');
        toastr.success('প্রতিকল্প হালনাগাদ হচ্ছে...');
        var employeeRecordId = $("input[name=employee_record_id]").val();
        $.ajax({
            type: 'POST',
            url: "<?php echo $this->Url->build(['controller' => 'ProtikolpoSettings', 'action' => 'stopProtikolpo']) ?>",
            data: {'employee_record_id' : employeeRecordId},
            async: true,
            success: function (response) {
                if (response.status == 'success') {
                    //toastr.success(response.msg);
                    window.location.href = js_wb_root+'logout';
                }
            }
        });
	}
</script>