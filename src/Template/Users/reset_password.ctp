<style>
    #custommsg{
        font-size:12px;
    }
    .short{
        font-weight:bold;
        color:#FF0000;
        font-size:larger;
    }
    .weak{
        font-weight:bold;
        color:orange;
        font-size:larger;
    }
    .good{
        font-weight:bold;
        color:#2D98F3;
        font-size:larger;
    }
    .strong{
        font-weight:bold;
        color: limegreen;
        font-size:larger;
    }
</style>
<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption"><i class=""></i>রিসেট পাসওয়ার্ড</div>
    </div>
    <div class="portlet-body form"><br><br>
        <?php
        echo $this->Form->create($user, ['action' => 'renewPassword','method'=>'post']);
        echo $this->Form->hidden('id',['value' => $user['id']]);
        echo $this->Form->hidden('token',['value' => $token]);
        ?>

        <div class="form-horizontal">
            <div class="form-group">
                <label class="col-sm-4 control-label">Password</label>

                <div class="col-sm-6">
                    <?php echo $this->Form->input('password', array('id' => 'password', 'label' => false, 'value' => '', 'type' => 'password', 'required' => true, 'class' => 'form-control', 'placeholder' => 'Password')); ?>
                    <span id="custommsg"></span>
                    <span class="help-block font-red"><b>*</b> পাসওয়ার্ড নূন্যতম ৬ অক্ষরের হতে হবে। অন্তত একটি A-Z অথবা a-z থাকতে হবে।  </span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">Confirm Password</label>

                <div class="col-sm-6">
                    <?php echo $this->Form->input('cpassword', array('id' => 'cpassword', 'label' => false, 'type' => 'password', 'class' => 'form-control', 'required' => true, 'placeholder' => 'Confirm Password')); ?>
                </div>
            </div>
        </div>
        <div class="form-actions">
            <button type="submit" id="btn_submit" class="btn green uppercase"><?php echo __(SAVE) ?></button>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
    <script type="text/javascript">

        $(document).off('click', "#btn_submit").on('click', "#btn_submit", function (e) {
            if ($("#password").val() != $("#cpassword").val()) {
                toastr.error("দুঃখিত! নতুন দেওয়া পাসওয়ার্ডটি  মিলেনি");
                $("#cpassword").focus(function () {
                    $(this).closest('.password').addClass('has-error');
                });
                $("#cpassword").focus();
                return false;
            }

            var pass_length = $("#password").val().length;
            if (pass_length < 6) {
                toastr.error("পাসওয়ার্ড সর্বনিম্ন ৬ অক্ষরের হতে হবে। ");
                $("#password").focus(function () {
                    $(this).closest('.password').addClass('has-error');
                });
                $("#password").focus();
                return false;
            }
            var password = $("#password").val();

            if (!password.match(/([a-zA-Z])/)){
                toastr.error("অন্তত একটি A-Z অথবা a-z থাকতে হবে। ");
                $("#password").focus(function () {
                    $(this).closest('.password').addClass('has-error');
                });
                $("#password").focus();
                return false;
            }
        });


        $(document).ready(function() {
            $('#password').keyup(function() {
                $('#custommsg').html('');
                $('#custommsg').html(checkStrength($('#password').val()))
            })
            function checkStrength(password) {
                var strength = 0
                if (password.length < 6) {
                    $('#custommsg').removeClass()
                    $('#custommsg').addClass('short')
                    return 'Too short'
                }
                if (password.length > 7) strength += 1
// If password contains both lower and uppercase characters, increase strength value.
                if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/)) strength += 1
// If it has numbers and characters, increase strength value.
                if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/)) strength += 1
// If it has one special character, increase strength value.
                if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/)) strength += 1
// If it has two special characters, increase strength value.
                if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,%,&,@,#,$,^,*,?,_,~])/)) strength += 1
// Calculated strength value, we can return messages
// If value is less than 2
                if (strength < 2) {
                    $('#custommsg').removeClass()
                    $('#custommsg').addClass('weak')
                    return 'Weak'
                } else if (strength == 2) {
                    $('#custommsg').removeClass()
                    $('#custommsg').addClass('good')
                    return 'Good'
                } else {
                    $('#custommsg').removeClass()
                    $('#custommsg').addClass('strong')
                    return 'Strong'
                }
            }
        });
    </script>