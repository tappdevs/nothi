<?php

namespace App\Utility;

use Migrations\Migrations;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;

class Migration
{

    public function __construct($data)
    {
        if (empty($data)) {
            $result =  "No database configuration is found!";
        } else {
            $result = $this->createDatabase($data['office_db'], $data['domain_username'], $data['domain_password'], $data['domain_host']);
        }

        return $result;
    }


    private function createDatabase($db_name, $user_name, $password, $host)
    {
        try {
            $dsn = "mysql://{$user_name}:{$password}@{$host}/";
            ConnectionManager::config('newOfficeDb', ['url' => $dsn]);
            $connection = ConnectionManager::get('newOfficeDb');
            $connection->query("CREATE DATABASE IF NOT EXISTS {$db_name} CHARACTER SET utf8 COLLATE utf8_general_ci;");
            return $this->runMigration($db_name, $user_name, $password, $host);
        } catch (\Exception $ex) {
            return $ex->getMessage();
        }
        return false;
    }

    private function runMigration($db_name, $user_name, $password, $host)
    {
        $dsn = "mysql://{$user_name}:{$password}@{$host}/{$db_name}";
        ConnectionManager::drop('newOfficeDb');
        ConnectionManager::config('newOfficeDb', ['url' => $dsn]);
        $migrations = new Migrations(['connection' => 'newOfficeDb']);
        $migrate = $migrations->migrate();

        if($migrate){
            return true;
        }
        return false;
    }
}