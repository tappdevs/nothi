<?php

namespace App\View\Cell;

use Cake\View\Cell;
use Cake\ORM\TableRegistry;
use Cake\Cache\Cache;
use Cake\Datasource\ConnectionManager;

/**
 * App View class
 */
class DakDashboardCell extends Cell
{

    public function display()
    {
        
    }

    public function ownDeskDateBase()
    {
        
    }

    public function officeSummary($officeid = 0, $api = 0)
    {
//        if (empty($api)) {
////            $session         = $this->request->session();
////            $employee_office = $session->read('selected_office_section');
////            if (empty($officeid))
////                $officeid        = $employee_office['office_id'];
//
//        }else {
//            $this->__switchDb($officeid, 'OwnOfficeDb');
//        }
          if(!empty($officeid)){
                try{
                     $this->__switchDb($officeid, 'OwnOfficeDb');
                    $this->set(compact('api'));
//                    TableRegistry::remove('Reports');
//                    $table_reports   = TableRegistry::get('Reports');
                    TableRegistry::remove('Dashboard');
                    $table_Dashboard = TableRegistry::get('Dashboard');
                    $officeTable     = TableRegistry::get('Offices');

                    $officeinfo = $officeTable->get($officeid);
                    if (($result     = Cache::read('dashboard_result_'.$officeid, 'memcached')) === false) {
                        //            $result = $table_reports->officeSummary($officeid);
                        $result = $table_Dashboard->officeSummary($officeid);
                        Cache::write('dashboard_result_'.$officeid, $result, 'memcached');
                }

                $this->set('result', $result);
                $this->set('officeinfo', $officeinfo);
            } catch (Exception $ex) {
                }
            }

    }

    public function ownDeskDakSummary()
    {
        $session         = $this->request->session();
        $employee_office = $session->read('selected_office_section');

        $resultArray = array();
        if (!empty($employee_office['office_id'])) {
            $officeId                 = $employee_office['office_id'];
            $officeUnitId             = $employee_office['office_unit_id'];
            $officeUnitOrganogramId   = $employee_office['office_unit_organogram_id'];
            $officeUnitOrganogramName = $employee_office['designation_label'];
            $officer_id               = $employee_office['officer_id'];
        }

        if (($resultArray = Cache::read('own_desk_dak_summary_'.$officeId.'_'.$officeUnitId.'_'.$officeUnitOrganogramId,
                'memcached')) === false) {


            $table_instance_dak_user = TableRegistry::get('DakUsers');

            //total inbox
            $dak_daptorik_table = TableRegistry::get('DakDaptoriks');
            $dak_nagorik_table  = TableRegistry::get('DakNagoriks');
            $condition          = '1 ';

            $user_new_daks = $table_instance_dak_user->find()->select(['dak_id',
                    'dak_view_status', 'dak_type'])->where(['dak_category' => DAK_CATEGORY_INBOX,
                    'to_officer_id' => $employee_office['officer_id'], 'to_officer_designation_id' => $employee_office['office_unit_organogram_id']])->order(['modified DESC'])->toArray();

            $ng_ids         = array();
            $dp_ids         = array();
            $unreadDaptorik = array();
            $unreadNagorik  = array();
            foreach ($user_new_daks as $new_dak) {
                if ($new_dak['dak_type'] == DAK_DAPTORIK) {
                    if ($new_dak['dak_view_status'] == 'New') {
                        $unreadDaptorik[] = $new_dak['dak_id'];
                    }
                    $dp_ids[] = $new_dak['dak_id'];
                }
                else if ($new_dak['dak_type'] == DAK_NAGORIK) {
                    $ng_ids[] = $new_dak['dak_id'];
                    if ($new_dak['dak_view_status'] == 'New') {
                        $unreadNagorik[] = $new_dak['dak_id'];
                    }
                }
            }

            $totalRec           = 0;
            $totalRec_daptoriks = array();
            $totalRec_nagoriks  = array();
            if (count($dp_ids) > 0) {
                $totalRec_daptoriks = $dak_daptorik_table->find()->where(['id IN' => $dp_ids])->where([$condition])->toArray();
            }
            if (count($ng_ids) > 0) {
                $totalRec_nagoriks = $dak_nagorik_table->find()->where(['id IN' => $ng_ids])->where([$condition])->toArray();
            }
            $totalRec = count($totalRec_daptoriks) + count($totalRec_nagoriks);

            $resultArray['total_new']       = count($unreadDaptorik) + count($unreadNagorik);
            $resultArray['total_inbox']     = $totalRec;
            $resultArray['total_dap_inbox'] = count($totalRec_daptoriks);
            $resultArray['total_nag_inbox'] = count($totalRec_nagoriks);
            $resultArray['total_dap_new']   = count($unreadDaptorik);
            $resultArray['total_nag_new']   = count($unreadNagorik);
            //end
            //sent
            $user_new_daks                  = $table_instance_dak_user->find()->select(['dak_id',
                    'dak_type'])->where(['dak_category' => DAK_CATEGORY_FORWARD,
                    'to_officer_id' => $employee_office['officer_id'], 'to_officer_designation_id' => $employee_office['office_unit_organogram_id']])->toArray();

            $ng_ids = array();
            $dp_ids = array();
            foreach ($user_new_daks as $new_dak) {
                if ($new_dak['dak_type'] == DAK_DAPTORIK) {
                    $dp_ids[] = $new_dak['dak_id'];
                }
                else if ($new_dak['dak_type'] == DAK_NAGORIK) {
                    $ng_ids[] = $new_dak['dak_id'];
                }
            }

            $totalRec = array();
            if (count($dp_ids) > 0) {
                $totalRec_daptoriks = $dak_daptorik_table->find()->where(['id IN' => $dp_ids])->where([$condition])->toArray();
            }
            if (count($ng_ids) > 0) {
                $totalRec_nagoriks = $dak_nagorik_table->find()->where(['id IN' => $ng_ids])->where([$condition])->toArray();
            }

            $totalRec = count($totalRec_daptoriks) + count($totalRec_nagoriks);

            $resultArray['total_sent']     = $totalRec;
            $resultArray['total_dap_sent'] = count($totalRec_daptoriks);
            $resultArray['total_nag_sent'] = count($totalRec_nagoriks);

            //nothi
            $user_new_daks = $table_instance_dak_user->find()->select(['dak_id',
                    'dak_type'])->where(['dak_category' => DAK_CATEGORY_NOTHIVUKTO,
                    'to_officer_id' => $employee_office['officer_id'], 'to_officer_designation_id' => $employee_office['office_unit_organogram_id']])->toArray();

            $ng_ids = array();
            $dp_ids = array();
            foreach ($user_new_daks as $new_dak) {
                if ($new_dak['dak_type'] == DAK_DAPTORIK) {
                    $dp_ids[] = $new_dak['dak_id'];
                }
                else if ($new_dak['dak_type'] == DAK_NAGORIK) {
                    $ng_ids[] = $new_dak['dak_id'];
                }
            }

            $totalRec = array();
            if (count($dp_ids) > 0) {
                $totalRec_daptoriks = $dak_daptorik_table->find()->where(['id IN' => $dp_ids])->where([$condition])->toArray();
            }
            if (count($ng_ids) > 0) {
                $totalRec_nagoriks = $dak_nagorik_table->find()->where(['id IN' => $ng_ids])->where([$condition])->toArray();
            }

            $totalRec = count($totalRec_daptoriks) + count($totalRec_nagoriks);

            $resultArray['total_nothi']     = $totalRec;
            $resultArray['total_dap_nothi'] = count($totalRec_daptoriks);
            $resultArray['total_nag_nothi'] = count($totalRec_nagoriks);


            Cache::write('own_desk_dak_summary_'.$officeId.'_'.$officeUnitId.'_'.$officeUnitOrganogramId,
                $resultArray, 'memcached');
        }

        $returnArray = [

            array(
                'label' => 'নতুন',
                'value' => $resultArray['total_new'],
                'total' => \Cake\I18n\Number::format($resultArray['total_new']).' টি',
                'ban' => "দাপ্তরিক: ".\Cake\I18n\Number::format($resultArray['total_dap_new']).' টি<br/>'."নাগরিক ".\Cake\I18n\Number::format($resultArray['total_nag_new']).' টি',
            ), array(
                'label' => 'আগত',
                'value' => $resultArray['total_inbox'],
                'total' => \Cake\I18n\Number::format($resultArray['total_inbox']).' টি',
                'ban' => "দাপ্তরিক: ".\Cake\I18n\Number::format($resultArray['total_dap_inbox']).' টি<br/>'."নাগরিক ".\Cake\I18n\Number::format($resultArray['total_nag_inbox']).' টি',
            ),
            array(
                'label' => 'প্রেরিত',
                'value' => $resultArray['total_sent'],
                'total' => \Cake\I18n\Number::format($resultArray['total_sent']).' টি',
                'ban' => "দাপ্তরিক: ".\Cake\I18n\Number::format($resultArray['total_dap_sent']).' টি<br/>'."নাগরিক ".\Cake\I18n\Number::format($resultArray['total_nag_sent']).' টি',
            ), array(
                'label' => 'নথিতে উপস্থাপিত',
                'value' => $resultArray['total_nothi'],
                'total' => \Cake\I18n\Number::format($resultArray['total_nothi']).' টি',
                'ban' => "দাপ্তরিক: ".\Cake\I18n\Number::format($resultArray['total_dap_nothi']).' টি<br/>'."নাগরিক ".\Cake\I18n\Number::format($resultArray['total_nag_nothi']).' টি',
            )
        ];

        //end
        $this->set('resultArray', json_encode($returnArray));
    }

    public function ownDeskNothiSummary()
    {
        $session         = $this->request->session();
        $employee_office = $session->read('selected_office_section');

        $resultArray = array();

        if (!empty($employee_office['office_id'])) {
            $officeId                 = $employee_office['office_id'];
            $officeUnitId             = $employee_office['office_unit_id'];
            $officeUnitOrganogramId   = $employee_office['office_unit_organogram_id'];
            $officeUnitOrganogramName = $employee_office['designation_label'];
            $officer_id               = $employee_office['officer_id'];
        }
        if (($returnArray = Cache::read('own_desk_nothi_summary_'.$officeId.'_'.$officeUnitId.'_'.$officeUnitOrganogramId,
                'memcached')) === false) {


            $nothiMastersTable = TableRegistry::get("NothiParts");

            $nothiMasterPermissionsTable = TableRegistry::get("NothiMasterPermissions");
            TableRegistry::remove('NothiMasterCurrentUsers');
            $nothiMasterCurrentUserTable = TableRegistry::get("NothiMasterCurrentUsers");
            $nothiMasterMovementTable    = TableRegistry::get("NothiMasterMovements");

            //total inbox
            $nothiMastersCurrentUser = $nothiMasterCurrentUserTable->find()->select(['nothi_part_no',
                        'view_status'])->where(['office_unit_organogram_id' => $employee_office['office_unit_organogram_id'],
                        'office_unit_id' => $employee_office['office_unit_id']])
                    ->join(['NothiParts' => [
                            'table' => 'nothi_parts',
                            "conditions" => "NothiMasterCurrentUsers.nothi_part_no = NothiParts.id AND NothiParts.nothi_part_no = 1",
                            "type" => "INNER"
                    ]])->toArray();

            $currentNothiMaster = '';
            $newNothiMaster     = 0;
            if (!empty($nothiMastersCurrentUser)) {
                foreach ($nothiMastersCurrentUser as $key => $currentUser) {
                    if ($currentUser['view_status'] == "New") {
                        $newNothiMaster++;
                    }
                    $currentNothiMaster .= $currentUser['nothi_part_no'].',';
                }
            }

            if (!empty($currentNothiMaster)) {
                $condition = ' NothiParts.id IN ('.substr($currentNothiMaster, 0, -1).')';
            }
            else {
                $condition = " 0 ";
            }

            $totalRec = $nothiMastersTable->getAllNothi($condition)->count();

            $resultArray['inbox'] = $totalRec;

            $sentNothi = $nothiMasterMovementTable->find()->select(["nothimasterid" => "DISTINCT(NothiMasterMovements.nothi_part_no)"])->where(['from_office_unit_id' => $employee_office['office_unit_id'],
                    'from_officer_designation_id' => $employee_office['office_unit_organogram_id']])->join(['NothiParts' => [
                        'table' => 'nothi_parts',
                        "conditions" => "NothiMasterMovements.nothi_part_no = NothiParts.id AND NothiParts.nothi_part_no = 1",
                        "type" => "INNER"
                ]])->toArray();

            $condition = '';
            if (!empty($sentNothi)) {
                $sentNothiMaster = '';

                foreach ($sentNothi as $ke => $sentNothiId) {
                    $sentNothiMaster .= $sentNothiId['nothimasterid'].',';
                }

                $condition = ' NothiParts.id IN ('.substr($sentNothiMaster, 0, -1).')';

                if (!empty($currentNothiMaster)) {
                    $condition .= 'AND NothiParts.id NOT IN ('.substr($currentNothiMaster, 0, -1).')';
                }
            }
            else {
                $condition = " 0 ";
            }

            $totalRec            = $nothiMastersTable->getAllNothi($condition)->count();
            $resultArray['sent'] = $totalRec;




            $returnArray = [
                array(
                    'label' => 'নতুন',
                    'value' => $newNothiMaster,
                    'total' => \Cake\I18n\Number::format($newNothiMaster).' টি',
                    'ban' => \Cake\I18n\Number::format($newNothiMaster).' টি',
                ),
                array(
                    'label' => 'অগত',
                    'value' => $resultArray['inbox'],
                    'total' => \Cake\I18n\Number::format($resultArray['inbox']).' টি',
                    'ban' => \Cake\I18n\Number::format($resultArray['inbox']).' টি',
                ),
                array(
                    'label' => 'প্রেরিত',
                    'value' => $resultArray['sent'],
                    'total' => \Cake\I18n\Number::format($resultArray['sent']).' টি',
                    'ban' => \Cake\I18n\Number::format($resultArray['sent']).' টি',
                )
            ];
            Cache::write('own_desk_nothi_summary_'.$officeId.'_'.$officeUnitId.'_'.$officeUnitOrganogramId,
                $returnArray, 'memcached');
        }
        //end
        $this->set('resultArray', json_encode($returnArray));
    }

    public function dateWiseOwnDeskSummary()
    {
        $session         = $this->request->session();
        $employee_office = $session->read('selected_office_section');

        $returnData = array();

        if (!empty($employee_office['office_id'])) {
            $officeId                 = $employee_office['office_id'];
            $officeUnitId             = $employee_office['office_unit_id'];
            $officeUnitOrganogramId   = $employee_office['office_unit_organogram_id'];
            $officeUnitOrganogramName = $employee_office['designation_label'];
            $officer_id               = $employee_office['officer_id'];
        }

        $startDate = date("Y-m-d", strtotime('1 Month ago'));
        $endDate   = date("Y-m-d");

        if (($returnData = Cache::read('date_wise_own_desk_summary_'.$officeId.'_'.$officeUnitId.'_'.$officeUnitOrganogramId,
                'memcached')) === false) {
            $table_instance_dak_user = TableRegistry::get('DakUsers');

            //agoto
            $user_inbox_daks = $table_instance_dak_user->find()->select([ 'dak_type',
                    'dakcreated' => 'DATE(created)', 'total' => 'count(dak_type)'])->where(['dak_category' => DAK_CATEGORY_INBOX,
                    'to_officer_id' => $employee_office['officer_id'], 'to_officer_designation_id' => $employee_office['office_unit_organogram_id']])->where(['date(created) >= ' => $startDate,
                    'date(created) <= ' => $endDate])->group(['DATE(created)', 'dak_type'])->order(['created ASC'])->toArray();

            //action not taken
            $user_sent_daks = $table_instance_dak_user->find()->select([ 'dak_category',
                        'dak_type', 'dakcreated' => 'DATE(created)', 'total' => 'count(dak_category)'])
                    ->where(['dak_category IN ' => array(DAK_CATEGORY_FORWARD), 'to_officer_id' => $employee_office['officer_id'],
                        'to_officer_designation_id' => $employee_office['office_unit_organogram_id']])->where(['date(created) >= ' => $startDate,
                    'date(created) <= ' => $endDate])->group(['DATE(created)', 'dak_category',
                    'dak_type'])->order(['modified ASC'])->toArray();

            //action not taken
            $user_nothi_daks = $table_instance_dak_user->find()->select([ 'dak_category',
                        'dak_type', 'dakcreated' => 'DATE(created)', 'total' => 'count(dak_category)'])
                    ->where(['dak_category IN ' => array(DAK_CATEGORY_NOTHIVUKTO),
                        'to_officer_id' => $employee_office['officer_id'], 'to_officer_designation_id' => $employee_office['office_unit_organogram_id']])->where(['date(created) >= ' => $startDate,
                    'date(created) <= ' => $endDate])->group(['DATE(created)', 'dak_category',
                    'dak_type'])->order(['modified ASC'])->toArray();


            $inboxTotal = 0;

            if (!empty($user_inbox_daks)) {
                foreach ($user_inbox_daks as $key => $value) {
                    if (isset($returnData[$value['dakcreated']]['inbox'][$value['dak_type']]['total']))
                            $returnData[$value['dakcreated']]['inbox'][$value['dak_type']] += $value['total'];
                    else {
                        $returnData[$value['dakcreated']]['inbox'][$value['dak_type']] = $value['total'];
                    }
                    if (isset($returnData[$value['dakcreated']]['inbox']['total']))
                            $returnData[$value['dakcreated']]['inbox']['total'] += $value['total'];
                    else {
                        $returnData[$value['dakcreated']]['inbox']['total'] = $value['total'];
                    }
                }
            }

            $sentTotal = 0;
            $sents     = array();
            if (!empty($user_sent_daks)) {
                foreach ($user_sent_daks as $key => $value) {
                    $returnData[$value['dakcreated']]['sent'][$value['dak_type']][$value['dak_category']]
                        = $value['total'];
                    $sentTotal += $value['total'];
                    if (isset($returnData[$value['dakcreated']]['sent'][$value['dak_type']]['total']))
                            $returnData[$value['dakcreated']]['sent'][$value['dak_type']]['total'] += $value['total'];
                    else {
                        $returnData[$value['dakcreated']]['sent'][$value['dak_type']]['total'] = $value['total'];
                    }
                    if (isset($returnData[$value['dakcreated']]['sent']['total']))
                            $returnData[$value['dakcreated']]['sent']['total'] += $value['total'];
                    else {
                        $returnData[$value['dakcreated']]['sent']['total'] = $value['total'];
                    }
                }
            }

            $nothiTotal  = 0;
            $nothivuktos = array();
            if (!empty($user_nothi_daks)) {
                foreach ($user_nothi_daks as $key => $value) {
                    $returnData[$value['dakcreated']]['nothi'][$value['dak_type']][$value['dak_category']]
                        = $value['total'];
                    $nothiTotal += $value['total'];
                    if (isset($returnData[$value['dakcreated']]['nothi'][$value['dak_type']]['total']))
                            $returnData[$value['dakcreated']]['nothi'][$value['dak_type']]['total'] += $value['total'];
                    else {
                        $returnData[$value['dakcreated']]['nothi'][$value['dak_type']]['total'] = $value['total'];
                    }
                    if (isset($returnData[$value['dakcreated']]['nothi']['total']))
                            $returnData[$value['dakcreated']]['nothi']['total'] += $value['total'];
                    else {
                        $returnData[$value['dakcreated']]['nothi']['total'] = $value['total'];
                    }
                }
            }

            if (!empty($returnData)) ksort($returnData);

            Cache::write('date_wise_own_desk_summary_'.$officeId.'_'.$officeUnitId.'_'.$officeUnitOrganogramId,
                $returnData, 'memcached');
        }

        $finalGraph = array();
        if (!empty($returnData)) {
            foreach ($returnData as $key => $value) {
                $finalGraph[] = array(
                    'date' => $key,
                    "prerito_dak" => (!empty($value['sent']['total']) ? $value['sent']['total'] : 0),
                    "grihito_dak" => (!empty($value['inbox']['total']) ? $value['inbox']['total'] : 0),
                    "nothi_vukto" => (!empty($value['nothi']['total']) ? $value['nothi']['total'] : 0),
                    "prerito_nagorik_dak" => \Cake\I18n\Number::format(!empty($value['sent']['Nagorik']['total'])
                                ? $value['sent']['Nagorik']['total'] : 0).' টি',
                    "prerito_daptorik_dak" => \Cake\I18n\Number::format(!empty($value['sent']['Daptorik']['total'])
                                ? $value['sent']['Daptorik']['total'] : 0).' টি',
                    "grihito_nagorik_dak" => \Cake\I18n\Number::format(!empty($value['inbox']['Nagorik']['total'])
                                ? $value['inbox']['Nagorik']['total'] : 0).' টি',
                    "grihito_daptorik_dak" => \Cake\I18n\Number::format(!empty($value['inbox']['Daptorik']['total'])
                                ? $value['inbox']['Daptorik']['total'] : 0).' টি',
                    "nothi_vukto_nagorik_dak" => \Cake\I18n\Number::format(!empty($value['nothi']['Nagorik']['total'])
                                ? $value['nothi']['Nagorik']['total'] : 0).' টি',
                    "nothi_vukto_daptorik_dak" => \Cake\I18n\Number::format(!empty($value['nothi']['Daptorik']['total'])
                                ? $value['nothi']['Daptorik']['total'] : 0).' টি'
                );
            }
        }

        $this->set('resultArray', json_encode($finalGraph));
    }

    public function dateWiseOwnDeskNothiSummary()
    {
        $session         = $this->request->session();
        $employee_office = $session->read('selected_office_section');

        $resultArray = array();

        $returnData = array();
        $startDate  = date("Y-m-d", strtotime('1 Month ago'));
        $endDate    = date("Y-m-d");

        if (!empty($employee_office['office_id'])) {
            $officeId                 = $employee_office['office_id'];
            $officeUnitId             = $employee_office['office_unit_id'];
            $officeUnitOrganogramId   = $employee_office['office_unit_organogram_id'];
            $officeUnitOrganogramName = $employee_office['designation_label'];
            $officer_id               = $employee_office['officer_id'];
        }

        if (($returnData = Cache::read('date_wise_own_desk_nothi_summary_'.$officeId.'_'.$officeUnitId.'_'.$officeUnitOrganogramId,
                'memcached')) === false) {

            $table_instance_nothi_move = TableRegistry::get('NothiMasterMovements');

            //agoto
            $user_inbox_nothis = $table_instance_nothi_move->find()->select([ 'nothicreated' => 'DATE(NothiMasterMovements.created)',
                        'total' => 'count(*)'])->where(['to_officer_id' => $employee_office['officer_id'],
                        'to_officer_designation_id' => $employee_office['office_unit_organogram_id']])
                    ->join(['NothiParts' => [
                            'table' => 'nothi_parts',
                            "conditions" => "NothiMasterMovements.nothi_part_no = NothiParts.id AND NothiParts.nothi_part_no = 1",
                            "type" => "INNER"
                    ]])
                    ->where(['date(NothiMasterMovements.created) >= ' => $startDate,
                        'date(NothiMasterMovements.created) <= ' => $endDate])
                    ->group(['DATE(NothiMasterMovements.created)'])->order(['NothiMasterMovements.created ASC'])->toArray();

            //action not taken
            $user_sent_nothis = $table_instance_nothi_move->find()->select([ 'nothicreated' => 'DATE(NothiMasterMovements.created)',
                        'total' => 'count(*)'])->where(['from_officer_id' => $employee_office['officer_id'],
                        'from_officer_designation_id' => $employee_office['office_unit_organogram_id']])
                    ->join(['NothiParts' => [
                            'table' => 'nothi_parts',
                            "conditions" => "NothiMasterMovements.nothi_part_no = NothiParts.id AND NothiParts.nothi_part_no = 1",
                            "type" => "INNER"
                    ]])
                    ->where(['date(NothiMasterMovements.created) >= ' => $startDate,
                        'date(NothiMasterMovements.created) <= ' => $endDate])
                    ->group(['DATE(NothiMasterMovements.created)'])->order(['NothiMasterMovements.created ASC'])->toArray();

            if (!empty($user_inbox_nothis)) {
                foreach ($user_inbox_nothis as $key => $value) {

                    if (isset($returnData[$value['nothicreated']]['inbox']['total']))
                            $returnData[$value['nothicreated']]['inbox']['total'] += $value['total'];
                    else {
                        $returnData[$value['nothicreated']]['inbox']['total'] = $value['total'];
                    }
                }
            }

            if (!empty($user_sent_nothis)) {
                foreach ($user_sent_nothis as $key => $value) {
                    if (isset($returnData[$value['nothicreated']]['sent']['total']))
                            $returnData[$value['nothicreated']]['sent']['total'] += $value['total'];
                    else {
                        $returnData[$value['nothicreated']]['sent']['total'] = $value['total'];
                    }
                }
            }

            if (!empty($returnData)) {
                ksort($returnData);
            }

            Cache::write('date_wise_own_desk_nothi_summary_'.$officeId.'_'.$officeUnitId.'_'.$officeUnitOrganogramId,
                $returnData, 'memcached');
        }
        $finalGraph = array();
        if (!empty($returnData)) {
            foreach ($returnData as $key => $value) {
                $finalGraph[] = array(
                    'date' => $key,
                    "prerito_nothi" => (!empty($value['sent']['total']) ? $value['sent']['total'] : 0),
                    "grihito_nothi" => (!empty($value['inbox']['total']) ? $value['inbox']['total'] : 0),
                    "grihito_nothi_bn" => \Cake\I18n\Number::format(!empty($value['inbox']['total'])
                                ? $value['inbox']['total'] : 0).' টি',
                    "prerito_nothi_bn" => \Cake\I18n\Number::format(!empty($value['sent']['total']) ? $value['sent']['total']
                                : 0).' টি'
                );
            }
        }


        $this->set('resultArray', json_encode($finalGraph));
    }

    public function sentDeskSummary()
    {
        $session         = $this->request->session();
        $employee_office = $session->read('selected_office_section');

        $resultArray = array();
        $finalGraph  = array();

        $users = array();

        if (!empty($employee_office['office_id'])) {
            $officeId                 = $employee_office['office_id'];
            $officeUnitId             = $employee_office['office_unit_id'];
            $officeUnitOrganogramId   = $employee_office['office_unit_organogram_id'];
            $officeUnitOrganogramName = $employee_office['designation_label'];
            $officer_id               = $employee_office['officer_id'];
        }

        if (($finalGraph = Cache::read('sent_desk_summary_'.$officeId.'_'.$officeUnitId.'_'.$officeUnitOrganogramId,
                'memcached')) === false) {
            $table_instance_dak_movements = TableRegistry::get('DakMovements');

            $user_sent_daks = $table_instance_dak_movements->find()->select([ 'dak_id',
                        'dak_type', 'sequence', 'operation_type', 'to_officer_id',
                        'to_officer_designation_id', 'to_officer_designation_label'])
                    ->where(['operation_type' => DAK_CATEGORY_FORWARD, 'attention_type' => 1,
                        'from_officer_id' => $employee_office['officer_id'], 'from_officer_designation_id' => $employee_office['office_unit_organogram_id']])->order(['id ASC'])->toArray();


            if (!empty($user_sent_daks)) {
                foreach ($user_sent_daks as $key => $value) {
                    $users[$value['to_officer_designation_id']]['label']    = $value['to_officer_designation_label'];
                    if (empty($users[$value['to_officer_designation_id']]['received']))
                            $users[$value['to_officer_designation_id']]['received'] = 1;
                    else $users[$value['to_officer_designation_id']]['received'] ++;

                    $getUsersSentStatus = $table_instance_dak_movements->find()->select(['from_officer_designation_id',
                            'dak_id', 'dak_type', 'sequence', 'operation_type', 'to_office_id',
                            'to_office_unit_id', 'to_officer_id', 'to_officer_designation_id',
                            'to_officer_designation_label', 'to_office_unit_name'])->where(['dak_id' => $value['dak_id'],
                            'sequence > ' => ($value['sequence']), 'dak_type' => $value['dak_type'],
                            'from_officer_id' => $value['to_officer_id'], 'from_officer_designation_id' => $value['to_officer_designation_id'],
                            'attention_type' => 1])->order(['created ASC'])->order(['id desc'])->toArray();

                    if (!empty($getUsersSentStatus)) {
                        if (empty($users[$value['to_officer_designation_id']]['out']))
                                $users[$value['to_officer_designation_id']]['out'] = 1;
                        else $users[$value['to_officer_designation_id']]['out'] ++;
                    }
                }
            }

            if (!empty($users)) {
                foreach ($users as $key => $value) {
                    $mailLabel      = $value['label'];
                    $value['label'] = str_replace("(", "( ", $value['label']);
                    $value['label'] = str_replace(")", " )", $value['label']);
                    $name           = explode(" ", $value['label']);

                    $short_name = "";
                    if (!empty($name)) {
                        if (count($name) == 1) {
                            $short_name = $name[0];
                        }
                        else {
                            foreach ($name as $k => $v) {
                                $short_name .= mb_substr(trim($v), 0, 1).' ';
                            }
                        }
                    }
                    $finalGraph[] = array(
                        'designation' => trim($short_name),
                        'designation_full' => $mailLabel,
                        'received' => !empty($value['received']) ? $value['received'] : 0,
                        'received_bn' => !empty($value['received']) ? \Cake\I18n\Number::format($value['received'])
                                : \Cake\I18n\Number::format(0),
                        'out' => !empty($value['out']) ? $value['out'] : 0,
                        'out_bn' => !empty($value['out']) ? \Cake\I18n\Number::format($value['out'])
                                : \Cake\I18n\Number::format(0),
                    );
                }
            }
            Cache::write('sent_desk_summary_'.$officeId.'_'.$officeUnitId.'_'.$officeUnitOrganogramId,
                $finalGraph, 'memcached');
        }

        $this->set('resultArray', json_encode($finalGraph));
    }

    public function agingOwnDesk($officeid = 0, $office_unit_id = 0, $officeUnitOrganogramId = 0,
                                 $officer_id = 0, $api = 0)
    {
        $table           = TableRegistry::get('Reports');
        $Dashboardtable           = TableRegistry::get('Dashboard');
        $session         = $this->request->session();
        $employee_office = $session->read('selected_office_section');
        $this->set(compact('api'));
        if (empty($api)) {
            $officeid = $employee_office['office_id'];
        }
        else {
            $this->__switchDb($officeid, "OwnOfficeDb");
        }
        if (empty($office_unit_id)) {
            $office_unit_id = $employee_office['office_unit_id'];
        }

        if (empty($officeUnitOrganogramId)) {
            $officeUnitOrganogramId = $employee_office['office_unit_organogram_id'];
        }

        if (!empty($officer_id)) {
            $employee_office['officer_id'] = $officer_id;
        }

        $employeeofficesTable = TableRegistry::get('EmployeeOffices');


        $data = array();
        
            $EmployeeOfficeRecords = $employeeofficesTable->getEmployeeOfficeRecords($employee_office['officer_id']);

            if (!empty($EmployeeOfficeRecords)) {
                foreach ($EmployeeOfficeRecords as $key => $value) {
                    if (($result = Cache::read('aging_own_desk_'.$officeid.'_'.$office_unit_id.'_'.$value['office_unit_organogram_id'],
                'memcached')) === false) {
                            $result = $Dashboardtable->DesignationSummary($officeid,$office_unit_id,$value['office_unit_organogram_id']);
                             $result['dak5']                                            = $table->dakinbox($value['office_id'],
                                $value['office_unit_id'], $value['office_unit_organogram_id'],
                                'DATEDIFF(NOW(),date(modified))>=5');
                            $result['dak10']                                           = $table->dakinbox($value['office_id'],
                                $value['office_unit_id'], $value['office_unit_organogram_id'],
                                'DATEDIFF(NOW(),date(modified))>=10');
                            $result['dak15']                                           = $table->dakinbox($value['office_id'],
                                $value['office_unit_id'], $value['office_unit_organogram_id'],
                                'DATEDIFF(NOW(),date(modified))>=15');
                            $result['dak30']                                           = $table->dakinbox($value['office_id'],
                                $value['office_unit_id'], $value['office_unit_organogram_id'],
                                'DATEDIFF(NOW(),date(modified))>=30');

                             $result['nothi5']  = $table->nothiinbox($value['office_id'], $value['office_unit_id'],
                                $value['office_unit_organogram_id'],
                                'DATEDIFF(NOW(),date(modified))>=5');
                            $result['nothi10'] = $table->nothiinbox($value['office_id'], $value['office_unit_id'],
                                $value['office_unit_organogram_id'],
                                'DATEDIFF(NOW(),date(modified))>=10');
                            $result['nothi15'] = $table->nothiinbox($value['office_id'], $value['office_unit_id'],
                                $value['office_unit_organogram_id'],
                                'DATEDIFF(NOW(),date(modified))>=15');
                            $result['nothi30'] = $table->nothiinbox($value['office_id'], $value['office_unit_id'],
                                $value['office_unit_organogram_id'], 'DATEDIFF(NOW(),date(modified))>=30');

                             Cache::write('aging_own_desk_'.$officeid.'_'.$office_unit_id.'_'.$value['office_unit_organogram_id'],
                $result, 'memcached');
                }
                            $data[$value['office_unit_organogram_id']]['totaltodayinbox']     = $result['totaltodayinbox'];
                            $data[$value['office_unit_organogram_id']]['totalyesterdayinbox'] = $result['totalyesterdayinbox'];
                            $data[$value['office_unit_organogram_id']]['totalinboxall']       = $result['totalinboxall'];

                            $data[$value['office_unit_organogram_id']]['totaltodayoutbox']     = $result['totaltodayoutbox'];
                            $data[$value['office_unit_organogram_id']]['totalyesterdayoutbox'] = $result['totalyesterdayoutbox'];
                            $data[$value['office_unit_organogram_id']]['totaloutboxall']       = $result['totaloutboxall'];

                            $data[$value['office_unit_organogram_id']]['totaltodaynothivukto']     = $result['totaltodaynothivukto'];
                            $data[$value['office_unit_organogram_id']]['totalyesterdaynothivukto'] = $result['totalyesterdaynothivukto'];
                            $data[$value['office_unit_organogram_id']]['totalnothivuktoall']       = $result['totalnothivuktoall'];

                            $data[$value['office_unit_organogram_id']]['totaltodaynothijato']     = $result['totaltodaynothijato'];
                            $data[$value['office_unit_organogram_id']]['totalyesterdaynothijato'] = $result['totalyesterdaynothijato'];
                            $data[$value['office_unit_organogram_id']]['totalnothijatoall']       = $result['totalnothijatoall'];

                            $data[$value['office_unit_organogram_id']]['totaltodaysrijitonote']     = $result['totaltodaysrijitonote'];
                            $data[$value['office_unit_organogram_id']]['totalyesterdaysrijitonote'] = $result['totalyesterdaysrijitonote'];
                            $data[$value['office_unit_organogram_id']]['totalsrijitonoteall']       = !empty($result['totalsrijitonoteall'])?$result['totalsrijitonoteall']:0;

                            $data[$value['office_unit_organogram_id']]['totaltodaypotrojari']     = $result['totaltodaypotrojari'];
                            $data[$value['office_unit_organogram_id']]['totalyesterdaypotrojari'] = $result['totalyesterdaypotrojari'];
                            $data[$value['office_unit_organogram_id']]['totalpotrojariall']       = $result['totalpotrojariall'];

                            $data[$value['office_unit_organogram_id']]['totaltodaydaksohonote']     = $result['totaltodaydaksohonote'];
                            $data[$value['office_unit_organogram_id']]['totalyesterdaydaksohonote'] = $result['totalyesterdaydaksohonote'];
                            $data[$value['office_unit_organogram_id']]['totaldaksohonoteall']       = !empty($result['totaldaksohonoteall'])?$result['totaldaksohonoteall']:0;

                            $data[$value['office_unit_organogram_id']]['totaltodaynisponnodak']     = $result['totaltodaynisponnodak'];
                            $data[$value['office_unit_organogram_id']]['totalyesterdaynisponnodak'] = $result['totalyesterdaynisponnodak'];
                            $data[$value['office_unit_organogram_id']]['totalnisponnodakall']       =  !empty($result['totalnisponnodakall'])?$result['totalnisponnodakall']:0;

                            $data[$value['office_unit_organogram_id']]['totaltodaynisponnonote']     = $result['totaltodaynisponnonote'];
                            $data[$value['office_unit_organogram_id']]['totalyesterdaynisponnonote'] = $result['totalyesterdaynisponnonote'];
                            $data[$value['office_unit_organogram_id']]['totalnisponnonoteall']       = !empty($result['totalnisponnonoteall'])?$result['totalnisponnonoteall']:0;

                            $data[$value['office_unit_organogram_id']]['totaltodaynisponnopotrojari']     = $result['totaltodaynisponnopotrojari'];
                            $data[$value['office_unit_organogram_id']]['totalyesterdaynisponnopotrojari'] = $result['totalyesterdaynisponnopotrojari'];
                            $data[$value['office_unit_organogram_id']]['totalnisponnopotrojariall']       = !empty($result['totalnisponnopotrojariall'])?$result['totalnisponnopotrojariall']:0;

                            $data[$value['office_unit_organogram_id']]['totaltodayOnisponnodak']     = $result['totaltodayOnisponnodak'];
                            $data[$value['office_unit_organogram_id']]['totalyesterdayOnisponnodak'] = $result['totalyesterdayOnisponnodak'];
                            $data[$value['office_unit_organogram_id']]['totalOnisponnodakall']       = $result['totalOnisponnodakall'];

                            $data[$value['office_unit_organogram_id']]['totaltodayOnisponnonote']     = $result['totaltodayOnisponnonote'];
                            $data[$value['office_unit_organogram_id']]['totalyesterdayOnisponnonote'] = $result['totalyesterdayOnisponnonote'];
                            $data[$value['office_unit_organogram_id']]['totalOnisponnonoteall']       =!empty($result['totalOnisponnonoteall'])?$result['totalOnisponnonoteall']:0;

        //                    $data[$value['office_unit_organogram_id']]['totaltodayOnisponno']     = $result['totaltodayOnisponno'];
        //                    $data[$value['office_unit_organogram_id']]['totalyesterdayOnisponno'] = $result['totalyesterdayOnisponno'];
        //                    $data[$value['office_unit_organogram_id']]['totalOnisponnoall']       = $result['totalOnisponnonoteall'];

                            $data[$value['office_unit_organogram_id']]['name']      = $value['designation'].', '.$value['unit_name'];
                            $data[$value['office_unit_organogram_id']]['unit_id']   = $value['office_unit_id'];
                            $data[$value['office_unit_organogram_id']]['office_id'] = $value['office_id'];

                            $data[$value['office_unit_organogram_id']]['ajax_request'] = $result['ajax_request'];

                           
                            $data[$value['office_unit_organogram_id']]['dak'] = [
                                'd5' => \Cake\I18n\Number::format($result['dak5']),
                                'd10' => \Cake\I18n\Number::format($result['dak10']),
                                'd15' => \Cake\I18n\Number::format($result['dak15']),
                                'd30' => \Cake\I18n\Number::format($result['dak30'])
                            ];

                            $data[$value['office_unit_organogram_id']]['nothi'] = [
                                'd5' => \Cake\I18n\Number::format($result['nothi5']),
                                'd10' => \Cake\I18n\Number::format($result['nothi10']),
                                'd15' => \Cake\I18n\Number::format($result['nothi15']),
                                'd30' => \Cake\I18n\Number::format($result['nothi30'])
                            ];
                    
                }
            }


        $this->set('data', $data);
    }

    public function agingOtherDesk()
    {

        $session         = $this->request->session();
        $employee_office = $session->read('selected_office_section');
        $officeUnitTable = TableRegistry::get('OfficeUnits');
        $table           = TableRegistry::get('Reports');
        $Dashboardtable           = TableRegistry::get('Dashboard');

        $employeeOfficeTable = TableRegistry::get('EmployeeOffices');

        $owninfo = $employeeOfficeTable->getDesignationInfo($employee_office['office_unit_organogram_id']);

        $canSee = 0;
        if ($owninfo['designation_level'] == 1) {
            $canSee = 1;
        }


        $unitsOwn = $officeUnitTable->getOwnChildInfo($employee_office['office_unit_id']);

        $unitsOwn = ($canSee == 1 ? ($employee_office['office_unit_id'].',') : '').(!empty($unitsOwn[0])
                    ? ($unitsOwn[0]) : '');

        $unitarray = explode(',', $unitsOwn);

        $data = array();

        $officeid = $employee_office['office_id'];
        if (!empty($unitarray[0])) {
            $office_unit_organogram_id = 0;
            foreach ($unitarray as $key => $office_unit_id) {

                $unitname = $officeUnitTable->get($office_unit_id);
                if (($result   = Cache::read('unit_summary_'.$officeid.'_'.$unitname['id'],
                        'memcached')) === false) {
//                    $result = $table->UnitSummary($unitname['id'], $officeid);
                    $result = $Dashboardtable->unitSummary( $officeid, $unitname['id']);
                     $result['nothi5']                              = $table->nothiinbox($officeid, $office_unit_id,
                    0, 'DATEDIFF(NOW(),date(issue_date))>=5');
                  $result['nothi10']                              = $table->nothiinbox($officeid, $office_unit_id,
                    0, 'DATEDIFF(NOW(),date(issue_date))>=10');
                $result['nothi15']                             = $table->nothiinbox($officeid, $office_unit_id,
                    0,
                    'DATEDIFF(NOW(),date(issue_date))>=15');
                $result['nothi30']                             = $table->nothiinbox($officeid, $office_unit_id,
                    0,'DATEDIFF(NOW(),date(issue_date))>=30');
                
                $result['dak5']                        = $table->dakinbox($officeid, $office_unit_id,
                    $office_unit_organogram_id,
                    'DATEDIFF(NOW(),date(created))>=5');
                $result['dak10']                        = $table->dakinbox($officeid, $office_unit_id,
                    $office_unit_organogram_id,
                    'DATEDIFF(NOW(),date(created))>=10');
                $result['dak15']                       = $table->dakinbox($officeid, $office_unit_id,
                    $office_unit_organogram_id,
                    'DATEDIFF(NOW(),date(created))>=15');
                $result['dak30']                       = $table->dakinbox($officeid, $office_unit_id,
                    $office_unit_organogram_id,
                    'DATEDIFF(NOW(),date(created))>=30');
                    Cache::write('unit_summary_'.$officeid.'_'.$unitname['id'], $result, 'memcached');
                }

                $data[$office_unit_id]['totallogin']     = $result['totallogin'];
                $data[$office_unit_id]['yesterdayLogin'] = $result['yesterdayLogin'];

                $data[$office_unit_id]['totaltodayinbox']     = $result['totaltodayinbox'];
                $data[$office_unit_id]['totalyesterdayinbox'] = $result['totalyesterdayinbox'];
                $data[$office_unit_id]['totalinboxall']       = $result['totalinboxall'];

                $data[$office_unit_id]['totaltodayoutbox']     = $result['totaltodayoutbox'];
                $data[$office_unit_id]['totalyesterdayoutbox'] = $result['totalyesterdayoutbox'];
                $data[$office_unit_id]['totaloutboxall']       = $result['totaloutboxall'];

                $data[$office_unit_id]['totaltodaynothivukto']     = $result['totaltodaynothivukto'];
                $data[$office_unit_id]['totalyesterdaynothivukto'] = $result['totalyesterdaynothivukto'];
                $data[$office_unit_id]['totalnothivuktoall']       = !empty($result['totalnothivuktoall'])?$result['totalnothivuktoall']:0;

                $data[$office_unit_id]['totaltodaynothijato']     = $result['totaltodaynothijato'];
                $data[$office_unit_id]['totalyesterdaynothijato'] = $result['totalyesterdaynothijato'];
                $data[$office_unit_id]['totalnothijatoall']       = !empty($result['totalnothijatoall'])?$result['totalnothijatoall']:0;

                $data[$office_unit_id]['totaltodaypotrojari']     = $result['totaltodaypotrojari'];
                $data[$office_unit_id]['totalyesterdaypotrojari'] = $result['totalyesterdaypotrojari'];
                $data[$office_unit_id]['totalpotrojariall']       =  !empty($result['totalpotrojariall'])?$result['totalpotrojariall']:0;

                $data[$office_unit_id]['totaltodaysrijitonote']     = $result['totaltodaysrijitonote'];
                $data[$office_unit_id]['totalyesterdaysrijitonote'] = $result['totalyesterdaysrijitonote'];
                $data[$office_unit_id]['totalsrijitonoteall']       = !empty($result['totalsrijitonoteall'])?$result['totalsrijitonoteall']:0;

                $data[$office_unit_id]['totaltodaydaksohonote']     = $result['totaltodaydaksohonote'];
                $data[$office_unit_id]['totalyesterdaydaksohonote'] = $result['totalyesterdaydaksohonote'];
                $data[$office_unit_id]['totaldaksohonoteall']       = !empty($result['totaldaksohonoteall'])?$result['totaldaksohonoteall']:0;

                $data[$office_unit_id]['totaltodaynisponnodak']     = $result['totaltodaynisponnodak'];
                $data[$office_unit_id]['totalyesterdaynisponnodak'] = $result['totalyesterdaynisponnodak'];
                $data[$office_unit_id]['totalnisponnodakall']       = !empty($result['totalnisponnodakall'])?$result['totalnisponnodakall']:0;

                $data[$office_unit_id]['totaltodaynisponnonote']     = $result['totaltodaynisponnonote'];
                $data[$office_unit_id]['totalyesterdaynisponnonote'] = $result['totalyesterdaynisponnonote'];
                $data[$office_unit_id]['totalnisponnonoteall']       = !empty($result['totalnisponnonoteall'])?$result['totalnisponnonoteall']:0;

                $data[$office_unit_id]['totaltodaynisponnopotrojari']     = $result['totaltodaynisponnopotrojari'];
                $data[$office_unit_id]['totalyesterdaynisponnopotrojari'] = $result['totalyesterdaynisponnopotrojari'];
                $data[$office_unit_id]['totalnisponnopotrojariall']       = !empty($result['totalnisponnopotrojariall'])?$result['totalnisponnopotrojariall']:0;

                $data[$office_unit_id]['totaltodayOnisponnodak']     = $result['totaltodayOnisponnodak'];
                $data[$office_unit_id]['totalyesterdayOnisponnodak'] = $result['totalyesterdayOnisponnodak'];
                $data[$office_unit_id]['totalOnisponnodakall']       = $result['totalOnisponnodakall'];

                $data[$office_unit_id]['totaltodayOnisponnonote']     = $result['totaltodayOnisponnonote'];
                $data[$office_unit_id]['totalyesterdayOnisponnonote'] = $result['totalyesterdayOnisponnonote'];
                $data[$office_unit_id]['totalOnisponnonoteall']       = $result['totalOnisponnonoteall'];

//                $data[$office_unit_id]['totaltodayOnisponno']     = $result['totaltodayOnisponno'];
//                $data[$office_unit_id]['totalyesterdayOnisponno'] = $result['totalyesterdayOnisponno'];
//                $data[$office_unit_id]['totalOnisponnonoteall']   = $result['totalOnisponnonoteall'];


                $data[$office_unit_id]['name']      = $unitname['unit_name_bng'];
                $data[$office_unit_id]['unit_id']   = $unitname['id'];
                $data[$office_unit_id]['office_id'] = $officeid;
               
                $data[$office_unit_id]['nothi']     = [
                 'd5' => \Cake\I18n\Number::format($result['nothi5']),
                'd10' => \Cake\I18n\Number::format($result['nothi10']),
                'd15' => \Cake\I18n\Number::format($result['nothi15']),
                'd30' => \Cake\I18n\Number::format($result['nothi30'])
                ];
                $data[$office_unit_id]['dak'] = [
                    'd5' => \Cake\I18n\Number::format($result['dak5']),
                    'd10' => \Cake\I18n\Number::format($result['dak10']),
                    'd15' => \Cake\I18n\Number::format($result['dak15']),
                    'd30' => \Cake\I18n\Number::format($result['dak30'])
                ];
            }
        }
        // pr($data);//die;
        $this->set('data', $data);
    }

    public function pendingDashboard()
    {
        $session         = $this->request->session();
        $employee_office = $session->read('selected_office_section');
        $officeUnitTable = TableRegistry::get('OfficeUnits');
        $table           = TableRegistry::get('Reports');


        $unitsOwn = $officeUnitTable->getOwnChildInfo($employee_office['office_unit_id']);

        $unitsOwn = !empty($unitsOwn[0]) ? ($employee_office['office_unit_id'].','.$unitsOwn[0]) : $employee_office['office_unit_id'];


        $unitarray = explode(',', $unitsOwn);

        $data = array();

        $officeid = $employee_office['office_id'];
        if (!empty($unitarray)) {
            $office_unit_organogram_id = 0;
            foreach ($unitarray as $key => $office_unit_id) {

                $unitname                                = $officeUnitTable->get($office_unit_id);
                $data[$office_unit_id]['name']           = $unitname['unit_name_bng'];
                $employeeOfiicestable                    = TableRegistry::get('EmployeeOffices');
                $data[$office_unit_id]['total']          = $employeeOfiicestable->find()
                        ->where(['office_id' => $officeid, 'office_unit_id' => $office_unit_id])->count();
                $DakUserstable                           = TableRegistry::get('DakUsers');
                $data[$office_unit_id]['mail']           = $DakUserstable->find()
                        ->where(['to_office_id' => $officeid, 'to_office_unit_id' => $office_unit_id,
                            'DATE(created)' => 'CURRENTDATE()'])->count();
                $data[$office_unit_id]['nisponnodak']    = 0;
                $data[$office_unit_id]['onisponnodak30'] = 0;
            }
        }
        $this->set('data', $data);
    }

    public function ownSummary($officer_id = 0, $office_id = 0, $api = 0)
    {

        if (empty($api)) {
            $session     = $this->request->session();
            $designation = $session->read('selected_office_section');
        }
        else {
            $this->__switchDb($office_id, 'OwnOfficeDb');
            $designation['officer_id'] = $officer_id;
        }

        $this->set(compact('api'));
        $table_instance_emp_offices = TableRegistry::get('EmployeeOffices');

        $employee_office_records = $table_instance_emp_offices->getEmployeeOfficeRecords($designation['officer_id']);
        $allunits                = array();

        foreach ($employee_office_records as $key => $value) {
            $allunits['unit'][] = $value['office_unit_id'];
            $allunits['org'][]  = $value['office_unit_organogram_id'];
        }

        TableRegistry::remove('NothiMasterMovements');
        TableRegistry::remove('DakMovements');
        TableRegistry::remove('DakUsers');
        TableRegistry::remove('NothiMasterCurrentUsers');
        $tableNothiMovements     = TableRegistry::get('NothiMasterMovements');
        $table_instance_dak_movements = TableRegistry::get('DakMovements');
        $table_instance_dak_users = TableRegistry::get('DakUsers');
        $table_instance_nothi_master_current_users = TableRegistry::get('NothiMasterCurrentUsers');

        $sum_onisponno = 0;$sum_nisponno = 0;
//        $sum_onisponno = $table_instance_dak_user->getDakCountBy_employee_designation(DAK_CATEGORY_INBOX,
//            $allunits['org']);
//        $sum_onisponno += $table_instance_dak_user->getDakCountBy_employee_designation(DAK_CATEGORY_INBOX,
//            $allunits['org'], DAK_NAGORIK);
//        $sum_nisponno += $table_instance_dak_user->getDakCountBy_employee_designation(DAK_CATEGORY_NOTHIVUKTO,
//            $allunits['org']);
//        $sum_nisponno += $table_instance_dak_user->getDakCountBy_employee_designation(DAK_CATEGORY_NOTHIVUKTO,
//            $allunits['org'], DAK_NAGORIK);
//        $sum_nisponno += $table_instance_dak_user->getDakCountBy_employee_designation(DAK_CATEGORY_NOTHIJATO,
//            $allunits['org']);
//        $sum_nisponno += $table_instance_dak_user->getDakCountBy_employee_designation(DAK_CATEGORY_NOTHIJATO,
//            $allunits['org'], DAK_NAGORIK);
        $sum_onisponno += $table_instance_dak_users->getTotalDakCountBy_employee_designation(DAK_CATEGORY_INBOX,$allunits['org']);
//        $sum_onisponno += $table_instance_dak_movements->countAllDakbyDesignations($allunits['org'],DAK_CATEGORY_INBOX);
        $sum_nisponno += $table_instance_dak_movements->countAllDakbyDesignations($allunits['org'],DAK_CATEGORY_NOTHIJATO);
        $sum_nisponno += $table_instance_dak_movements->countAllDakbyDesignations($allunits['org'],DAK_CATEGORY_NOTHIVUKTO );
        $sum_nisponno += $table_instance_dak_movements->countAllDakbyDesignations($allunits['org'],'Outbox');

        $totalOutboxNothi = $tableNothiMovements->getOutboxCountBy_employee_designation($allunits['org']);
        $totalInboxNothi = $table_instance_nothi_master_current_users->getOnisponnoNothiMasterCount($allunits['org']);
//        if($sum_nisponno > $sum_onisponno)
//        {
//            $sum_nisponno = $sum_onisponno;
//        }
//        if($totalOutboxNothi > $totalInboxNothi)
//        {
//            $totalOutboxNothi = $totalInboxNothi;
//        }
        $this->set('totalOutboxDak', ($sum_nisponno));
        $this->set('totalInboxDak', $sum_onisponno);
        $this->set('totalInboxNothi', $totalInboxNothi);
        $this->set('totalOutboxNothi', $totalOutboxNothi);
    }

    public function OfficeList($office_id = 0)
    {

        $officesTable       = TableRegistry::get('Offices');
        $child_offices_data = $officesTable->getChildOffices($office_id);
        $this->set('office_list', $child_offices_data);
    }
    public function OfficeListForRender($office_ids = [],$title = '')
    {

        $this->set('office_list', $office_ids);
        $this->set('title', $title);
    }

    public function agingAllUnit($office_id = 0)
    {

        $officeUnitTable = TableRegistry::get('OfficeUnits');
        $table           = TableRegistry::get('Reports');
        $Dashboardtable = TableRegistry::get('Dashboard');


        $unitarray = $officeUnitTable->find('list',
                ['keyField' => 'id', 'valueField' => 'unit_name_bng'])->where(['office_id' => $office_id])->order(['parent_unit_id asc',
                'id desc'])->toArray();

        $data = array();

        $officeid = $office_id;
        if (!empty($unitarray)) {
            $office_unit_organogram_id = 0;
//            pr($unitarray);die;
            foreach ($unitarray as $office_unit_id => $unitname) {

                if (($result = Cache::read('unit_summary_'.$office_id.'_'.$office_unit_id,
                        'memcached')) === false) {
//                    $result = $table->UnitSummary($office_unit_id, $office_id);
                    $result = $Dashboardtable->unitSummary($office_id, $office_unit_id);
//                    pr($result);die;
                    Cache::write('unit_summary_'.$office_id.'_'.$office_unit_id, $result,
                        'memcached');
                }

                $data[$office_unit_id]['totallogin']     = $result['totallogin'];
                $data[$office_unit_id]['yesterdayLogin'] = $result['yesterdayLogin'];

                $data[$office_unit_id]['totaltodayinbox']     = $result['totaltodayinbox'];
                $data[$office_unit_id]['totalyesterdayinbox'] = $result['totalyesterdayinbox'];
                $data[$office_unit_id]['totalinboxall']       = $result['totalinboxall'];

                $data[$office_unit_id]['totaltodayoutbox']     = $result['totaltodayoutbox'];
                $data[$office_unit_id]['totalyesterdayoutbox'] = $result['totalyesterdayoutbox'];
                $data[$office_unit_id]['totaloutboxall']       = $result['totaloutboxall'];

                $data[$office_unit_id]['totaltodaynothivukto']     = $result['totaltodaynothivukto'];
                $data[$office_unit_id]['totalyesterdaynothivukto'] = $result['totalyesterdaynothivukto'];
                $data[$office_unit_id]['totalnothivuktoall']       = $result['totalnothivuktoall'];

                $data[$office_unit_id]['totaltodaynothijato']     = $result['totaltodaynothijato'];
                $data[$office_unit_id]['totalyesterdaynothijato'] = $result['totalyesterdaynothijato'];
                $data[$office_unit_id]['totalnothijatoall']       = $result['totalnothijatoall'];

                $data[$office_unit_id]['totaltodaypotrojari']     = $result['totaltodaypotrojari'];
                $data[$office_unit_id]['totalyesterdaypotrojari'] = $result['totalyesterdaypotrojari'];
                $data[$office_unit_id]['totalpotrojariall']       = $result['totalpotrojariall'];

                $data[$office_unit_id]['totaltodaysrijitonote']     = $result['totaltodaysrijitonote'];
                $data[$office_unit_id]['totalyesterdaysrijitonote'] = $result['totalyesterdaysrijitonote'];
                $data[$office_unit_id]['totalsrijitonoteall']       = !empty($result['totalsrijitonoteall'])?$result['totalsrijitonoteall']:0;

                $data[$office_unit_id]['totaltodaydaksohonote']     = $result['totaltodaydaksohonote'];
                $data[$office_unit_id]['totalyesterdaydaksohonote'] = $result['totalyesterdaydaksohonote'];
                $data[$office_unit_id]['totaldaksohonoteall']       = !empty($result['totaldaksohonoteall'])?$result['totaldaksohonoteall']:0;

                $data[$office_unit_id]['totaltodaynisponnodak']     = $result['totaltodaynisponnodak'];
                $data[$office_unit_id]['totalyesterdaynisponnodak'] = $result['totalyesterdaynisponnodak'];
                $data[$office_unit_id]['totalnisponnodakall']       =  !empty($result['totalnisponnodakall'])?$result['totalnisponnodakall']:0;

                $data[$office_unit_id]['totaltodaynisponnonote']     = $result['totaltodaynisponnonote'];
                $data[$office_unit_id]['totalyesterdaynisponnonote'] = $result['totalyesterdaynisponnonote'];
                $data[$office_unit_id]['totalnisponnonoteall']       = !empty($result['totalnisponnonoteall'])?$result['totalnisponnonoteall']:0;

                $data[$office_unit_id]['totaltodaynisponnopotrojari']     = $result['totaltodaynisponnopotrojari'];
                $data[$office_unit_id]['totalyesterdaynisponnopotrojari'] = $result['totalyesterdaynisponnopotrojari'];
                $data[$office_unit_id]['totalnisponnopotrojariall']       = !empty($result['totalnisponnopotrojariall'])?$result['totalnisponnopotrojariall']:0;

                $data[$office_unit_id]['totaltodayOnisponnodak']     = $result['totaltodayOnisponnodak'];
                $data[$office_unit_id]['totalyesterdayOnisponnodak'] = $result['totalyesterdayOnisponnodak'];
                $data[$office_unit_id]['totalOnisponnodakall']       =  !empty($result['totalOnisponnodakall'])?$result['totalOnisponnodakall']:0;

                $data[$office_unit_id]['totaltodayOnisponnonote']     = $result['totaltodayOnisponnonote'];
                $data[$office_unit_id]['totalyesterdayOnisponnonote'] = $result['totalyesterdayOnisponnonote'];
                $data[$office_unit_id]['totalOnisponnonoteall']       = !empty($result['totalOnisponnonoteall'])?$result['totalOnisponnonoteall']:0;

                $data[$office_unit_id]['ajax_request']       = !empty($result['ajax_request'])?$result['ajax_request']:false;

//                $data[$office_unit_id]['totaltodayOnisponno']     = $result['totaltodayOnisponno'];
//                $data[$office_unit_id]['totalyesterdayOnisponno'] = $result['totalyesterdayOnisponno'];
//                $data[$office_unit_id]['totalOnisponnonoteall']   = $result['totalOnisponnonoteall'];


                $data[$office_unit_id]['name']      = $unitname;
                $data[$office_unit_id]['unit_id']   = $office_unit_id;
                $data[$office_unit_id]['office_id'] = $officeid;
                $data3                              = $table->nothiinbox($officeid, $office_unit_id,
                    0, 'DATEDIFF(NOW(),date(issue_date))>=3 and DATEDIFF(NOW(),date(issue_date))<5');
                $data5                              = $table->nothiinbox($officeid, $office_unit_id,
                    0, 'DATEDIFF(NOW(),date(issue_date))>=5 and DATEDIFF(NOW(),date(issue_date))<10');
                $data10                             = $table->nothiinbox($officeid, $office_unit_id,
                    0,
                    'DATEDIFF(NOW(),date(issue_date))>=10 and DATEDIFF(NOW(),date(issue_date))<15');
                $data15                             = $table->nothiinbox($officeid, $office_unit_id,
                    0,
                    'DATEDIFF(NOW(),date(issue_date))>=15 and DATEDIFF(NOW(),date(issue_date))<30');
                $data30                             = $table->nothiinbox($officeid, $office_unit_id,
                    0, 'DATEDIFF(NOW(),date(issue_date))>=30');
                $data[$office_unit_id]['nothi']     = [
                    'd3' => \Cake\I18n\Number::format($data3),
                    'd5' => \Cake\I18n\Number::format($data5),
                    'd10' => \Cake\I18n\Number::format($data10),
                    'd15' => \Cake\I18n\Number::format($data15),
                    'd30' => \Cake\I18n\Number::format($data30)
                ];


                $data3                        = $table->dakinbox($officeid, $office_unit_id, 0,
                    'DATEDIFF(NOW(),date(created))>=3 and DATEDIFF(NOW(),date(created))<5');
                $data5                        = $table->dakinbox($officeid, $office_unit_id, 0,
                    'DATEDIFF(NOW(),date(created))>=5 and DATEDIFF(NOW(),date(created))<10');
                $data10                       = $table->dakinbox($officeid, $office_unit_id, 0,
                    'DATEDIFF(NOW(),date(created))>=10 and DATEDIFF(NOW(),date(created))<15');
                $data15                       = $table->dakinbox($officeid, $office_unit_id, 0,
                    'DATEDIFF(NOW(),date(created))>=15 and DATEDIFF(NOW(),date(created))<30');
                $data30                       = $table->dakinbox($officeid, $office_unit_id, 0,
                    'DATEDIFF(NOW(),date(created))>=30');
                $data[$office_unit_id]['dak'] = [
                    'd3' => \Cake\I18n\Number::format($data3),
                    'd5' => \Cake\I18n\Number::format($data5),
                    'd10' => \Cake\I18n\Number::format($data10),
                    'd15' => \Cake\I18n\Number::format($data15),
                    'd30' => \Cake\I18n\Number::format($data30)
                ];
            }
        }
        // pr($data);//die;
        $this->set('data', $data);
//        pr($data);die;
    }
    public function agingSingleUnit($office_id = 0,$unit_id = 0,$unit_name = '')
    {

        $table          = TableRegistry::get('Reports');
        $Dashboardtable = TableRegistry::get('Dashboard');
        if(empty($unit_id)){
            return;
        }
        if (empty($unit_name) || empty($office_id)) {
            $officeUnitTable = TableRegistry::get('OfficeUnits');
            $unitInfo        = $officeUnitTable->find()->select(['unit_name_bng', 'office_id'])->where(['id' => $unit_id])->first();
            $unit_name       = $unitInfo['unit_name_bng'];
            $office_id       = $unitInfo['office_id'];
        }
        $unitarray[$unit_id] = $unit_name;

        $data = array();

        if (!empty($unitarray)) {
            foreach ($unitarray as $office_unit_id => $unitname) {

                if (($data = Cache::read('unit_summary_'.$office_id.'_'.$office_unit_id, 'memcached'))
                    === false) {
//                    $result = $table->UnitSummary($office_unit_id, $office_id);
                    $result = $Dashboardtable->unitSummary($office_id, $office_unit_id);

                    $data[$office_unit_id]['totallogin']     = $result['totallogin'];
                    $data[$office_unit_id]['yesterdayLogin'] = $result['yesterdayLogin'];

                    $data[$office_unit_id]['totaltodayinbox']     = $result['totaltodayinbox'];
                    $data[$office_unit_id]['totalyesterdayinbox'] = $result['totalyesterdayinbox'];
                    $data[$office_unit_id]['totalinboxall']       = $result['totalinboxall'];

                    $data[$office_unit_id]['totaltodayoutbox']     = $result['totaltodayoutbox'];
                    $data[$office_unit_id]['totalyesterdayoutbox'] = $result['totalyesterdayoutbox'];
                    $data[$office_unit_id]['totaloutboxall']       = $result['totaloutboxall'];

                    $data[$office_unit_id]['totaltodaynothivukto']     = $result['totaltodaynothivukto'];
                    $data[$office_unit_id]['totalyesterdaynothivukto'] = $result['totalyesterdaynothivukto'];
                    $data[$office_unit_id]['totalnothivuktoall']       = $result['totalnothivuktoall'];

                    $data[$office_unit_id]['totaltodaynothijato']     = $result['totaltodaynothijato'];
                    $data[$office_unit_id]['totalyesterdaynothijato'] = $result['totalyesterdaynothijato'];
                    $data[$office_unit_id]['totalnothijatoall']       = $result['totalnothijatoall'];

                    $data[$office_unit_id]['totaltodaypotrojari']     = $result['totaltodaypotrojari'];
                    $data[$office_unit_id]['totalyesterdaypotrojari'] = $result['totalyesterdaypotrojari'];
                    $data[$office_unit_id]['totalpotrojariall']       = $result['totalpotrojariall'];

                    $data[$office_unit_id]['totaltodaysrijitonote']     = $result['totaltodaysrijitonote'];
                    $data[$office_unit_id]['totalyesterdaysrijitonote'] = $result['totalyesterdaysrijitonote'];
                    $data[$office_unit_id]['totalsrijitonoteall']       = !empty($result['totalsrijitonoteall'])
                            ? $result['totalsrijitonoteall'] : 0;

                    $data[$office_unit_id]['totaltodaydaksohonote']     = $result['totaltodaydaksohonote'];
                    $data[$office_unit_id]['totalyesterdaydaksohonote'] = $result['totalyesterdaydaksohonote'];
                    $data[$office_unit_id]['totaldaksohonoteall']       = !empty($result['totaldaksohonoteall'])
                            ? $result['totaldaksohonoteall'] : 0;

                    $data[$office_unit_id]['totaltodaynisponnodak']     = $result['totaltodaynisponnodak'];
                    $data[$office_unit_id]['totalyesterdaynisponnodak'] = $result['totalyesterdaynisponnodak'];
                    $data[$office_unit_id]['totalnisponnodakall']       = !empty($result['totalnisponnodakall'])
                            ? $result['totalnisponnodakall'] : 0;

                    $data[$office_unit_id]['totaltodaynisponnonote']     = $result['totaltodaynisponnonote'];
                    $data[$office_unit_id]['totalyesterdaynisponnonote'] = $result['totalyesterdaynisponnonote'];
                    $data[$office_unit_id]['totalnisponnonoteall']       = !empty($result['totalnisponnonoteall'])
                            ? $result['totalnisponnonoteall'] : 0;

                    $data[$office_unit_id]['totaltodaynisponnopotrojari']     = $result['totaltodaynisponnopotrojari'];
                    $data[$office_unit_id]['totalyesterdaynisponnopotrojari'] = $result['totalyesterdaynisponnopotrojari'];
                    $data[$office_unit_id]['totalnisponnopotrojariall']       = !empty($result['totalnisponnopotrojariall'])
                            ? $result['totalnisponnopotrojariall'] : 0;

                    $data[$office_unit_id]['totaltodayOnisponnodak']     = $result['totaltodayOnisponnodak'];
                    $data[$office_unit_id]['totalyesterdayOnisponnodak'] = $result['totalyesterdayOnisponnodak'];
                    $data[$office_unit_id]['totalOnisponnodakall']       = !empty($result['totalOnisponnodakall'])
                            ? $result['totalOnisponnodakall'] : 0;

                    $data[$office_unit_id]['totaltodayOnisponnonote']     = $result['totaltodayOnisponnonote'];
                    $data[$office_unit_id]['totalyesterdayOnisponnonote'] = $result['totalyesterdayOnisponnonote'];
                    $data[$office_unit_id]['totalOnisponnonoteall']       = !empty($result['totalOnisponnonoteall'])
                            ? $result['totalOnisponnonoteall'] : 0;

                    $data[$office_unit_id]['ajax_request'] = !empty($result['ajax_request']) ? $result['ajax_request']
                            : false;

//                $data[$office_unit_id]['totaltodayOnisponno']     = $result['totaltodayOnisponno'];
//                $data[$office_unit_id]['totalyesterdayOnisponno'] = $result['totalyesterdayOnisponno'];
//                $data[$office_unit_id]['totalOnisponnonoteall']   = $result['totalOnisponnonoteall'];


                    $data[$office_unit_id]['name']      = $unitname;
                    $data[$office_unit_id]['unit_id']   = $office_unit_id;
                    $data[$office_unit_id]['office_id'] = $office_id;
                    $data5                              = $table->nothiinbox($office_id,
                        $office_unit_id, 0,
                        'DATEDIFF(NOW(),date(modified))>=5');
                    $data10                             = $table->nothiinbox($office_id,
                        $office_unit_id, 0,
                        'DATEDIFF(NOW(),date(modified))>=10');
                    $data15                             = $table->nothiinbox($office_id,
                        $office_unit_id, 0,
                        'DATEDIFF(NOW(),date(modified))>=15');
                    $data30                             = $table->nothiinbox($office_id,
                        $office_unit_id, 0, 'DATEDIFF(NOW(),date(modified))>=30');
                    $data[$office_unit_id]['nothi']     = [
                        'd5' => \Cake\I18n\Number::format($data5),
                        'd10' => \Cake\I18n\Number::format($data10),
                        'd15' => \Cake\I18n\Number::format($data15),
                        'd30' => \Cake\I18n\Number::format($data30)
                    ];


                    $data5                        = $table->dakinbox($office_id, $office_unit_id, 0,
                        'DATEDIFF(NOW(),date(modified))>=5');
                    $data10                       = $table->dakinbox($office_id, $office_unit_id, 0,
                        'DATEDIFF(NOW(),date(modified))>=10');
                    $data15                       = $table->dakinbox($office_id, $office_unit_id, 0,
                        'DATEDIFF(NOW(),date(modified))>=15');
                    $data30                       = $table->dakinbox($office_id, $office_unit_id, 0,
                        'DATEDIFF(NOW(),date(modified))>=30');
                    $data[$office_unit_id]['dak'] = [
                        'd5' => \Cake\I18n\Number::format($data5),
                        'd10' => \Cake\I18n\Number::format($data10),
                        'd15' => \Cake\I18n\Number::format($data15),
                        'd30' => \Cake\I18n\Number::format($data30)
                    ];
//                    pr($result);die;
                    Cache::write('unit_summary_'.$office_id.'_'.$office_unit_id, $data, 'memcached');
                }
            }
        }
        // pr($data);//die;
        $this->set('data', $data);
//        pr($data);die;
    }

    public function unitList($office_id = 0, $unit_id = 0, $api = 0)
    {

        if (!empty($api)) {
            $this->__switchDb($office_id, 'OfficeDbApi');
        }

        $this->set(compact('api'));
        $OfficeUnitsTable = TableRegistry::get('OfficeUnits');
        $table            = TableRegistry::get('Reports');
        $Dashboardtable            = TableRegistry::get('Dashboard');

        $unitarray = $OfficeUnitsTable->find('list',
                ['keyField' => 'id', 'valueField' => 'unit_name_bng'])->where(['parent_unit_id' => $unit_id,
                'office_id' => $office_id])->toArray();


        $data = array();

        $officeid = $office_id;
        if (!empty($unitarray)) {
            $office_unit_organogram_id = 0;
            foreach ($unitarray as $office_unit_id => $unitname) {

                if (($result = Cache::read('unit_summary_'.$office_id.'_'.$office_unit_id,
                        'memcached')) === false) {
                    $result = $Dashboardtable->unitSummary( $office_id, $office_unit_id);
                    Cache::write('unit_summary_'.$office_id.'_'.$office_unit_id, $result,
                        'memcached');
                }

                $data[$office_unit_id]['totallogin']     = $result['totallogin'];
                $data[$office_unit_id]['yesterdayLogin'] = $result['yesterdayLogin'];

                $data[$office_unit_id]['totaltodayinbox']     = $result['totaltodayinbox'];
                $data[$office_unit_id]['totalyesterdayinbox'] = $result['totalyesterdayinbox'];
                $data[$office_unit_id]['totalinboxall']       = $result['totalinboxall'];

                $data[$office_unit_id]['totaltodayoutbox']     = $result['totaltodayoutbox'];
                $data[$office_unit_id]['totalyesterdayoutbox'] = $result['totalyesterdayoutbox'];
                $data[$office_unit_id]['totaloutboxall']       = $result['totaloutboxall'];

                $data[$office_unit_id]['totaltodaynothivukto']     = $result['totaltodaynothivukto'];
                $data[$office_unit_id]['totalyesterdaynothivukto'] = $result['totalyesterdaynothivukto'];
                $data[$office_unit_id]['totalnothivuktoall']       = $result['totalnothivuktoall'];

                $data[$office_unit_id]['totaltodaynothijato']     = $result['totaltodaynothijato'];
                $data[$office_unit_id]['totalyesterdaynothijato'] = $result['totalyesterdaynothijato'];
                $data[$office_unit_id]['totalnothijatoall']       = $result['totalnothijatoall'];

                $data[$office_unit_id]['totaltodaypotrojari']     = $result['totaltodaypotrojari'];
                $data[$office_unit_id]['totalyesterdaypotrojari'] = $result['totalyesterdaypotrojari'];
                $data[$office_unit_id]['totalpotrojariall']       = $result['totalpotrojariall'];

                $data[$office_unit_id]['totaltodaysrijitonote']     = !empty($result['totaltodaysrijitonote'])?$result['totaltodaysrijitonote']:0;
                $data[$office_unit_id]['totalyesterdaysrijitonote'] = !empty($result['totalyesterdaysrijitonote'])?$result['totalyesterdaysrijitonote']:0;
                $data[$office_unit_id]['totalsrijitonoteall']       = !empty($result['totalsrijitonoteall'])?$result['totalsrijitonoteall']:0;

                $data[$office_unit_id]['totaltodaydaksohonote']     = !empty($result['totaltodaydaksohonote'])?$result['totaltodaydaksohonote']:0;
                $data[$office_unit_id]['totalyesterdaydaksohonote'] = !empty($result['totalyesterdaydaksohonote'])?$result['totalyesterdaydaksohonote']:0;
                $data[$office_unit_id]['totaldaksohonoteall']       = !empty($result['totaldaksohonoteall'])?$result['totaldaksohonoteall']:0;

                $data[$office_unit_id]['totaltodaynisponnodak']     = $result['totaltodaynisponnodak'];
                $data[$office_unit_id]['totalyesterdaynisponnodak'] = $result['totalyesterdaynisponnodak'];
                $data[$office_unit_id]['totalnisponnodakall']       = $result['totalnisponnodakall'];

                $data[$office_unit_id]['totaltodaynisponnonote']     = $result['totaltodaynisponnonote'];
                $data[$office_unit_id]['totalyesterdaynisponnonote'] = $result['totalyesterdaynisponnonote'];
                $data[$office_unit_id]['totalnisponnonoteall']       = !empty($result['totalnisponnonoteall'])?$result['totalnisponnonoteall']:0;

                $data[$office_unit_id]['totaltodaynisponnopotrojari']     = $result['totaltodaynisponnopotrojari'];
                $data[$office_unit_id]['totalyesterdaynisponnopotrojari'] = $result['totalyesterdaynisponnopotrojari'];
                $data[$office_unit_id]['totalnisponnopotrojariall']       = !empty($result['totalnisponnopotrojariall'])?$result['totalnisponnopotrojariall']:0;

                $data[$office_unit_id]['totaltodayOnisponnodak']     = $result['totaltodayOnisponnodak'];
                $data[$office_unit_id]['totalyesterdayOnisponnodak'] = $result['totalyesterdayOnisponnodak'];
                $data[$office_unit_id]['totalOnisponnodakall']       = !empty($result['totalOnisponnodakall'])?$result['totalOnisponnodakall']:0;

                $data[$office_unit_id]['totaltodayOnisponnonote']     = $result['totaltodayOnisponnonote'];
                $data[$office_unit_id]['totalyesterdayOnisponnonote'] = $result['totalyesterdayOnisponnonote'];
                $data[$office_unit_id]['totalOnisponnonoteall']       = !empty($result['totalOnisponnonoteall'])?$result['totalOnisponnonoteall']:0;

//                $data[$office_unit_id]['totaltodayOnisponno']     = $result['totaltodayOnisponno'];
//                $data[$office_unit_id]['totalyesterdayOnisponno'] = $result['totalyesterdayOnisponno'];
//                $data[$office_unit_id]['totalOnisponnonoteall']   = $result['totalOnisponnonoteall'];

                $data[$office_unit_id]['name']      = $unitname;
                $data[$office_unit_id]['unit_id']   = $office_unit_id;
                $data[$office_unit_id]['office_id'] = $officeid;
                $data5                              = $table->nothiinbox($officeid, $office_unit_id,
                    0, 'DATEDIFF(NOW(),date(modified))>=5');
                $data10                             = $table->nothiinbox($officeid, $office_unit_id,
                    0,
                    'DATEDIFF(NOW(),date(modified))>=10');
                $data15                             = $table->nothiinbox($officeid, $office_unit_id,
                    0,
                    'DATEDIFF(NOW(),date(modified))>=15');
                $data30                             = $table->nothiinbox($officeid, $office_unit_id,
                    0, 'DATEDIFF(NOW(),date(modified))>=30');
                $data[$office_unit_id]['nothi']     = [
                    'd5' => \Cake\I18n\Number::format($data5),
                    'd10' => \Cake\I18n\Number::format($data10),
                    'd15' => \Cake\I18n\Number::format($data15),
                    'd30' => \Cake\I18n\Number::format($data30)
                ];


                $data5                        = $table->dakinbox($officeid, $office_unit_id, 0,
                    'DATEDIFF(NOW(),date(created))>=5');
                $data10                       = $table->dakinbox($officeid, $office_unit_id, 0,
                    'DATEDIFF(NOW(),date(created))>=10');
                $data15                       = $table->dakinbox($officeid, $office_unit_id, 0,
                    'DATEDIFF(NOW(),date(created))>=15');
                $data30                       = $table->dakinbox($officeid, $office_unit_id, 0,
                    'DATEDIFF(NOW(),date(created))>=30');
                $data[$office_unit_id]['dak'] = [
                    'd5' => \Cake\I18n\Number::format($data5),
                    'd10' => \Cake\I18n\Number::format($data10),
                    'd15' => \Cake\I18n\Number::format($data15),
                    'd30' => \Cake\I18n\Number::format($data30)
                ];
            }
        }

        $this->set('data', $data);
    }
}