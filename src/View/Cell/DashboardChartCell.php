<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     3.0.0
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\View\Cell;

use Cake\View\Cell;
use Cake\ORM\TableRegistry;

/**
 * App View class
 */
class DashboardChartCell extends Cell
{
    public function display($field_value)
    {
        /* $table_instance = TableRegistry::get('OfficeMinistries');
        $data_items = $table_instance->find('list')->toArray();
        $this->set('data_items', $data_items);
        $this->set('value', $field_value); */
    }
	
	public function displayLineAreaChart($field_value, $type = "0"){
		
		$session = $this->request->session();
                $employee = $session->read('selected_office_section');
                
		$loggedin_employee_id = $employee["officer_id"];
		$office_unit_organogram_id = $employee["office_unit_organogram_id"];
		
		if ($type == "1") {
			// আগত ডাক
			$table_instance_dd = TableRegistry::get('DakDaptoriks');
			$table_instance_dn = TableRegistry::get('DakNagoriks');
			
			$daptorik_items = $table_instance_dd->find();
			$daptorik_items->select(['count' => $daptorik_items->func()->count('*'), 'created' => 'DATE(created)'])->where(["receiving_officer_id" => $loggedin_employee_id, "receiving_officer_designation_id" => $office_unit_organogram_id, "DATE(created) >= DATE_SUB(now(), INTERVAL 6 MONTH)"])->group("DATE(created)");
			
			$nagorik_items = $table_instance_dn->find();
			$nagorik_items->select(['count' => $nagorik_items->func()->count('*'), 'created' => 'DATE(created)'])->where(["receiving_officer_id" => $loggedin_employee_id, "receiving_officer_designation_id" => $office_unit_organogram_id, "DATE(created) >= DATE_SUB(now(), INTERVAL 6 MONTH)"])->group("DATE(created)");
			
			$this->set('daptorik_items', $daptorik_items);
			$this->set('nagorik_items', $nagorik_items);
		} else if ($type == "2") {
			// আগত ও প্রেরিত নথি
			$table_instance_nm = TableRegistry::get('NothiMasterMovements');
			
			$agoto_nothi = $table_instance_nm->find();
			$agoto_nothi->select(['count' => $agoto_nothi->func()->count('*'), 'created' => 'DATE(created)'])->where([ "to_officer_designation_id" => $office_unit_organogram_id, "DATE(created) >= DATE_SUB(now(), INTERVAL 6 MONTH)"])->group("DATE(created)");
			//debug($agoto_nothi);
			$this->set('agoto_nothi', $agoto_nothi);
			
			$prerito_nothi = $table_instance_nm->find();
			$prerito_nothi->select(['count' => $prerito_nothi->func()->count('*'), 'created' => 'DATE(created)'])->where(["from_officer_id" => $loggedin_employee_id, "from_officer_designation_id" => $office_unit_organogram_id, "DATE(created) >= DATE_SUB(now(), INTERVAL 6 MONTH)"])->group("DATE(created)");
			
			$this->set('prerito_nothi', $prerito_nothi);
		}
		$this->set("chart_no", $field_value);
	}
	
	public function displayPolarChart($field_value){
		$this->set("chart_no", $field_value);
	}
	
	public function displayRadarChart($field_value){
		
		$this->set("chart_no", $field_value);
	}
	
	public function displayStockChart($field_value){
		$this->set("chart_no", $field_value);
	}
        
}
