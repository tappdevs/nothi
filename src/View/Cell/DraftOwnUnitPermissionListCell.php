<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     3.0.0
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\View\Cell;

use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\View\Cell;
use Cake\ORM\TableRegistry;

/**
 * App View class
 */
class DraftOwnUnitPermissionListCell extends Cell
{
    public function display($draftmasterid = 0, $canEdit = 0)
    {
        $session = $this->request->session();
        $employee_office = $session->read('selected_office_section');

        if (!empty($employee_office['office_id'])) {
            $officeId = $employee_office['office_id'];
            $officeUnitId = $employee_office['office_unit_id'];
            $officeUnitOrganogramId = $employee_office['office_unit_organogram_id'];
            $officeUnitOrganogramName = $employee_office['designation_label'];
        }

        $employeeOfficeTable = TableRegistry::get('EmployeeOffices');
        $employeeRecordTable = TableRegistry::get('EmployeeRecords');
        $draftMasterTable = TableRegistry::get('DraftMaster');

        $draftUsers = $draftMasterTable->getUsers($draftmasterid)->toArray();

        $officeUnitsTable = TableRegistry::get('OfficeUnits');

        $unitList = $officeUnitsTable->getOfficeUnitsList($officeId);
        $ownOfficeEmployees = array();
        try{
            $ownOfficeEmployees = $employeeOfficeTable->getEmployeeOfficeRecordsByOffice($officeId);
        }catch(RecordNotFoundException $ex){
            $ownOfficeEmployees = array();
        }

        $permissionUsersList = '<table class="table table-bordered table-stripped" id="draftmasterpermissionlistownunit">';
        $permissionUsersList .= '<thead>';
        $permissionUsersList .= "<tr><th style='width: 15%' class='text-center'>অনুমোদন</th><th style='width: 40%' class='text-center'>কর্মকর্তার নাম</th><th style='width: 40%' class='text-center'>পদবি</th><th style='width: 5%' class='text-center'>শাখা</th></tr>";
        $permissionUsersList .= '</thead><tbody>';

        if (!empty($ownOfficeEmployees)) {
            foreach ($ownOfficeEmployees as $key => $value) {
                $employeeName = array();

                try{
                    $employeeName = $employeeRecordTable->get($value['employee_record_id']);
                }catch(RecordNotFoundException $ex){
                    $employeeName= array();
                }

                $isThere = array();
                if(!empty($draftUsers)){
                    foreach($draftUsers as $draftk=>$draftuser){
                        if($draftuser['office_id'] == $officeId
                        && $draftuser['office_unit_id'] == $value['office_unit_id']
                        && $draftuser['office_orgranogram_id'] == $value['office_unit_organogram_id']
                        && $draftuser['employee_id'] == $value['employee_record_id']
                    ){
                            $isThere = $draftuser;
                            break;
                        }
                    }
                }

                $permissionUsersList .= '<tr><td class="text-right">
                                <p >পরবর্তী কার্যক্রম করবে
                                 <input type="radio" class="optionRadios form-control" name="optionsone" data-org-id = ' . $value['office_unit_organogram_id'] . ' employeeid="' . $value['employee_record_id'] . ' data-office-id=' . $officeId . ' data-office-unit-id=' . $value['office_unit_id'] . '  value="1" /> </p>

                                  <p '.($canEdit==0?'style="display:none"':'') .' >অনুমতি  দেয়া হলো <input '.(!empty($isThere)?($isThere['permission_type']==1?"checked=checked":''):'') .' type="checkbox" class="optionCheckbox form-control" name="optionsRadios' . $value['office_unit_organogram_id'] . '" employeeid="' . $value['employee_record_id'] . ' " data-org-id = ' . $value['office_unit_organogram_id'] . ' data-office-id=' . $officeId . ' data-office-unit-id=' . $value['office_unit_id'] . '  value="1" /></p>
                        <p '.($canEdit==0?'style="display:none"':'') .'>অন্যকে শেয়ার করবেন
                                  <input '.(!empty($isThere)?($isThere['can_invite_other']==1?"checked=checked":''):'').' type="checkbox" class="optionShare form-control" name="optionsone' . $value['office_unit_organogram_id'] . '" data-org-id = ' . $value['office_unit_organogram_id'] . ' employeeid="' . $value['employee_record_id'] . ' data-office-id=' . $officeId . ' data-office-unit-id=' . $value['office_unit_id'] . '  value="1" /></p> </td>

                           <td>
                                <a class="employee_info" href="javascript:;" data-employee-record-id=' . $value['employee_record_id'] . ' data-office-id=' . $officeId . ' data-office-unit-id=' . $value['office_unit_id'] . '" data-office-unit-organogram-id=' . $value['office_unit_organogram_id'] . '" >' . (isset($employeeName['name_bng']) ? $employeeName['name_bng'] : '') . '</a>
                            </td><td class="designation">
                                ' . $value['designation'] . '
                            </td><td>
                                ' . $unitList[$value['office_unit_id']] . '
                            </td></tr>';
            }
        }
        $permissionUsersList .= "</tbody></table>";
        $this->set(compact('permissionUsersList'));

    }

}
