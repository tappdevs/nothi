<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     3.0.0
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\View\Cell;

use Cake\View\Cell;
use Cake\ORM\TableRegistry;

/**
 * App View class
 */
class EmployeeJobStatusCell extends Cell
{
    public function display()
    {
        $session = $this->request->session();
        $employee_offices = $session->read('selected_office_section');
        $data = [];
        $table = TableRegistry::get('Users');
        if(!empty($employee_offices)){
            $office_id = $employee_offices['office_id'];

            $data = $table->find('list')->select(['username'])->join([
                'EmployeeOffices'=>[
                    'table'=>'employee_offices',
                    'type'=>'inner',
                    'conditions'=>'EmployeeOffices.employee_record_id = Users.employee_record_id AND EmployeeOffices.office_id= ' . $office_id
                ]
            ])->toArray();

        }
        $this->set('employee_identity_nos', $data);
    }
    
}
