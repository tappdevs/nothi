<?php

/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     3.0.0
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */

namespace App\View\Cell;

use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\View\Cell;
use Cake\ORM\TableRegistry;

/**
 * App View class
 */
class NothiOwnUnitPermissionListCell extends Cell {

    public function display($prefix = "", $id = 0,$nothi_office=0) {
        $session = $this->request->session();
        $employee_office = $session->read('selected_office_section');

        if (!empty($employee_office['office_id'])) {
            $officeId = $employee_office['office_id'];
            $officeUnitId = $employee_office['office_unit_id'];
        }

        $employeeOfficeTable = TableRegistry::get('EmployeeOffices');
        TableRegistry::remove('NothiMasterPermissions');
        $nothiPriviligeTable = TableRegistry::get('NothiMasterPermissions');
        $officeUnitsTable = TableRegistry::get('OfficeUnits');

        $unitList = $officeUnitsTable->getOfficeUnitsList($officeId);

        $data = '';

        if (!empty($unitList)) {
			$options = '<option value="0">শাখা বাছাই করুন</option>';
            foreach ($unitList as $key => $value) {
            	$options .= '<option value="'.$key.'">'.h($value).'</option>';
                $data .= '<div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <input type="checkbox" onclick="checkAllEmployee(this, \'collapse_' . $key . '\')" style="position: absolute;margin: 12px 11px;">
                                    <a class="accordion-toggle accordion-toggle-styled collapsed" style="margin-left: 13px;" data-toggle="collapse" 
                                       data-parent="#accordion3" href="#collapse_' . $key . '" >' . h($value) . '</a>
                                </h4>
                            </div>
                            <div id="collapse_' . $key . '" class="panel-collapse collapse">
                                <div class="panel-body">';

                $employee = $employeeOfficeTable->getEmployeeOfficeRecordsByOfficeId($officeId, $key);
                if (!empty($employee)) {
                    foreach ($employee as $ekey => $eVal) {
                        $designation_description = '';
                        $designation_info = TableRegistry::get('OfficeUnitOrganograms')->get($eVal['office_unit_organogram_id']);
                        if(!empty($designation_info)){
                            $designation_description = $designation_info['designation_description'] ;
                        }
//                        <span title="'+org.designation_description + '">' + org.designation_bng + "</span>
                        if ($employee_office['office_unit_organogram_id'] == $eVal['office_unit_organogram_id']){
							$disabled = 'disabled="disabled"';
                        } else {
                            $disabled = '';
                        }
						//$data .= '<input type="checkbox" onclick="listown();" class="org_checkbox checkthis optionsRadios" data-office-unit-id="' . $key . '" ' . ($officeUnitId == $key ? 'checked=checked' : '') . ' data-office-unit-organogram-id="' . $eVal['office_unit_organogram_id'] . '"  data-unit-name="' . h($value) . '" data-designation-name="' . h($eVal['designation']) . '"  data-employee-name="' . h($eVal['name_bng']) . '" data-employee-id="' . $eVal['employee_record_id'] . '"  value="1" data-employee-office-id="' . $eVal['office_id'] . '" data-designation-level="'.h($eVal['designation_level']).'" '.$disabled.'> <span title="'.h($eVal['name_bng']).",\n".h($designation_description).'"> &nbsp;&nbsp;' . h($eVal['name_bng']) . ', ' . h($eVal['designation']) . (!empty($eVal['incharge_label'])?(' (' . h($eVal['incharge_label']). ')'):'') . '</span> <br>';
						$data .= '<input type="checkbox" onclick="listown();" class="org_checkbox checkthis optionsRadios" data-office-unit-id="' . $key . '" ' . ($employee_office['office_unit_organogram_id'] == $eVal['office_unit_organogram_id'] ? 'checked=checked' : '') . ' data-office-unit-organogram-id="' . $eVal['office_unit_organogram_id'] . '"  data-unit-name="' . h($value) . '" data-designation-name="' . h($eVal['designation']) . '"  data-employee-name="' . h($eVal['name_bng']) . '" data-employee-id="' . $eVal['employee_record_id'] . '"  value="1" data-employee-office-id="' . $eVal['office_id'] . '" data-designation-level="'.h($eVal['designation_level']).'" '.$disabled.'> <span title="'.h($eVal['name_bng']).",\n".h($designation_description).'"> &nbsp;&nbsp;' . h($eVal['name_bng']) . ', ' . h($eVal['designation']) . (!empty($eVal['incharge_label'])?(' (' . h($eVal['incharge_label']). ')'):'') . '</span> <br>';
                    }
                }

                $data .= '</div> </div> </div>';
            }
			$data = '<div class="panel panel-default"><select name="ownOfficeUnitList" id="ownOfficeUnitList" class="form-control">'.$options.'</select></div>' . $data;
        }
        $this->set('data', $data);
        $this->set('nothi_office', $officeId);
        $this->set('nothi_unit', $officeUnitId);
        $this->set('current_organogram', $employee_office['office_unit_organogram_id']);
    }

    public function updatePermission($prefix = "", $id = 0, $nothi_office = 0,
                                     $employee_office_info = [], $api = false)
    {
        $this->set('id', $id);
        $this->set('nothi_office', $nothi_office);
        $this->set('api', $api);
        if ($api == true && !empty($employee_office_info)) {
            $employee_office = $employee_office_info;
//            $this->render('api_update_permission');
        }
        else {
            $session         = $this->request->session();
            $employee_office = $session->read('selected_office_section');
        }
        if (!empty($employee_office['office_id'])) {
            $officeId     = $employee_office['office_id'];
            $officeUnitId = $employee_office['office_unit_id'];
        }
        $this->set(compact('employee_office'));
        
        $employeeOfficeTable = TableRegistry::get('EmployeeOffices');
        $real_nothi_master_id = 0;
        if (!empty($officeId)) {
            try {
                $pro                   = new \App\Controller\ProjapotiController();
                if(!empty($nothi_office)){
                    $pro->switchOffice($nothi_office, 'OwnOffice');
                }else{
                    $pro->switchOffice($officeId, 'OwnOffice');
                }
                TableRegistry::remove('NothiParts');
                $nothiPartsTable       = TableRegistry::get('NothiParts');
                TableRegistry::remove('NothiMasterPermissions');
                $nothiPriviligeTable   = TableRegistry::get('NothiMasterPermissions');
                TableRegistry::remove('NothiMasterCurrentUsers');
                $nothiCurrentUserTable = TableRegistry::get('NothiMasterCurrentUsers');
                $nothiPartsInfo = $nothiPartsTable->get($id);
                $real_nothi_master_id  = $nothiPartsInfo['nothi_master_id'];
                $current_user_info     = $nothiCurrentUserTable->getCurrentUserofNote($real_nothi_master_id,
                        $id, $nothi_office)->first();

                $officeUnitsTable = TableRegistry::get('OfficeUnits');

                $unitList = $officeUnitsTable->getOfficeUnitsList($officeId);

                // Get all permissions of that note - start
                $designationWiseAccessList = [];
                $AllEmployeeAccessList     = $nothiPriviligeTable->getPermissionListForNothiPart($officeId,
                        0, 0, $id, $nothi_office, $real_nothi_master_id)->toArray();
                if (!empty($AllEmployeeAccessList)) {
                    foreach ($AllEmployeeAccessList as $permit_list) {
                        if (!empty($permit_list['office_unit_organograms_id'])) {
                            $designationWiseAccessList[$permit_list['office_unit_organograms_id']]['privilige_type']
                                = $permit_list['privilige_type'];
                            $designationWiseAccessList[$permit_list['office_unit_organograms_id']]['designation_level']
                                = $permit_list['designation_level'];
                        }
                    }
                }
                // Get all permissions of that note - end
                $data = '';
                if (!empty($unitList)) {
                	$options = '<option value="0">শাখা বাছাই করুন</option>';
                    foreach ($unitList as $key => $value) {
						$options .= '<option value="'.$key.'">'.h($value).'</option>';
                        $data .= '<div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse"
                                       data-parent="#accordion3" href="#collapse_'.$key.'" >'.h($value).'</a>
                                </h4>
                            </div>
                            <div id="collapse_'.$key.'" class="panel-collapse collapse">
                                <div class="panel-body">';

                        $employee = $employeeOfficeTable->getEmployeeOfficeRecordsByOfficeId($officeId,
                            $key);
                        if (!empty($employee)) {
                            foreach ($employee as $ekey => $eVal) {
                                $designation_description = '';
                                $designation_info        = TableRegistry::get('OfficeUnitOrganograms')->get($eVal['office_unit_organogram_id']);
                                if (!empty($designation_info)) {
                                    $designation_description = $designation_info['designation_description'];
                                    if (!empty($designation_description)) {
                                        $designation_description = ", ".$designation_description;
                                    }
                                }
                                if (!empty($id)) {
//                            $hasThisEmployeeAccess = $nothiPriviligeTable->hasAccessPartList($officeId, $eVal['office_unit_id'], $eVal['office_unit_organogram_id'], $id,$nothi_office,$real_nothi_master_id);
                                    $hasThisEmployeeAccess = !empty($designationWiseAccessList[$eVal['office_unit_organogram_id']])
                                            ? $designationWiseAccessList[$eVal['office_unit_organogram_id']]
                                            : [];
                                    $is_current_user       = (( $eVal['office_id'] == $current_user_info['office_id'] )
                                        && ( $eVal['office_unit_id'] == $current_user_info['office_unit_id'] )
                                        && ( $eVal['office_unit_organogram_id'] == $current_user_info['office_unit_organogram_id'] )
                                                ? true : false );

                                    if ((!empty($hasThisEmployeeAccess['privilige_type']) && intval($hasThisEmployeeAccess['privilige_type'])
                                        > 0 ) || $is_current_user == true) {
                                        $eVal['check'] = 1;
                                    }
                                    else {
                                        $eVal['check'] = 0;
                                    }
                                }
                                else {

                                    $eVal['check'] = 0;
                                }
                                if ($eVal['check'] == 1) {
                                    if (!empty($hasThisEmployeeAccess['designation_level'])) {
                                        $eVal['designation_level'] = $hasThisEmployeeAccess['designation_level'];
                                    }
                                    if ($employee_office['office_unit_organogram_id'] == $eVal['office_unit_organogram_id']
                                        || $is_current_user == true) {
                                        $data .= '<input type="checkbox" onclick=" listown();" class="org_checkbox checkthis optionsRadios" data-employee-incharge-label="'.h($eVal['incharge_label']).'" data-office-unit-id="'.h($key).'"  data-office-unit-organogram-id="'.$eVal['office_unit_organogram_id'].'"  data-unit-name="'.h($value).'" data-designation-name="'.h($eVal['designation']).'"
                                                   data-employee-name="'.h($eVal['name_bng']).'" data-employee-id="'.$eVal['employee_record_id'].'"
                        value="1" data-employee-office-id="'.$eVal['office_id'].'" data-designation-level="'.h($eVal['designation_level']).'" checked="checked" disabled="disabled"  > <span title="'.h($eVal['name_bng']).h($designation_description).'"> &nbsp;&nbsp;'.h($eVal['name_bng']).', '.h($eVal['designation']).(!empty($eVal['incharge_label'])
                                                    ? (' ('.h($eVal['incharge_label']).')') : '').'</span> <br>';
                                    }
                                    else {
                                        $data .= '<input type="checkbox" onclick=" listown();" class="org_checkbox checkthis optionsRadios" data-employee-incharge-label="'.h($eVal['incharge_label']).'" data-office-unit-id="'.h($key).'"  data-office-unit-organogram-id="'.$eVal['office_unit_organogram_id'].'"  data-unit-name="'.h($value).'" data-designation-name="'.h($eVal['designation']).'"
                                                       data-employee-name="'.h($eVal['name_bng']).'" data-employee-id="'.$eVal['employee_record_id'].'"
                                                       value="1" data-employee-office-id="'.$eVal['office_id'].'" data-designation-level="'.$eVal['designation_level'].'" checked="checked" > <span title="'.h($eVal['name_bng']).h($designation_description).'"> &nbsp;&nbsp;'.h($eVal['name_bng']).', '.h($eVal['designation']).(!empty($eVal['incharge_label'])
                                                    ? (' ('.h($eVal['incharge_label']).')') : '').'</span> <br>';
                                    }
                                }
                                else {
                                    $data .= '<input type="checkbox" onclick="listown();" class="org_checkbox checkthis optionsRadios" data-employee-incharge-label="'.h($eVal['incharge_label']).'" data-office-unit-id="'.h($key).'" data-office-unit-organogram-id="'.$eVal['office_unit_organogram_id'].'"  data-unit-name="'.h($value).'" data-designation-name="'.h($eVal['designation']).'"
                                                   data-employee-name="'.h($eVal['name_bng']).'" data-employee-id="'.$eVal['employee_record_id'].'" value="1" data-employee-office-id="'.$eVal['office_id'].'" data-designation-level="'.h($eVal['designation_level']).'"> <span title="'.h($eVal['name_bng']).h($designation_description).'"> &nbsp;&nbsp;'.h($eVal['name_bng']).', '.h($eVal['designation']).(!empty($eVal['incharge_label'])
                                                ? (' ('.h($eVal['incharge_label']).')') : '').'</span> <br>';
                                }
                            }
                        }

                        $data .= '</div> </div> </div>';
                    }
					$data = '<div class="panel panel-default"><select name="ownOfficeUnitList" id="ownOfficeUnitList" class="form-control">'.$options.'</select></div>' . $data;
                }

                $this->set(compact('data', 'nothiPartsInfo'));
            } catch (\Exception $ex) {

            }
        }
    }
}
