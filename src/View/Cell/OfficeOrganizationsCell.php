<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     3.0.0
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\View\Cell;

use Cake\View\Cell;
use Cake\ORM\TableRegistry;

/**
 * App View class
 */
class OfficeOrganizationsCell extends Cell
{

    public function display($hierarchy_id, $hierarchy_name, $parent_script)
    {
        $table = TableRegistry::get('OfficeOrganizations');
        $data = $table->find()->select(['id', 'org_title_eng', 'org_title_bng'])->where(['office_hierarchy_template_id' => $hierarchy_id])->toArray();
        $data_array = array();
        foreach ($data as $row) {
            $data_array[$row['id']] = $row['org_title_bng'] . "[" . $row['org_title_eng'] . "]";
        }

        $this->set('office_organization_list', $data_array);
        $this->set('hierarchy_name', $hierarchy_name);
        $this->set('parent_script', $parent_script);
    }
}
