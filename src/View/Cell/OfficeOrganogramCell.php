<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     3.0.0
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\View\Cell;

use Cake\View\Cell;
use Cake\ORM\TableRegistry;

/**
 * App View class
 */
class OfficeOrganogramCell extends Cell
{
    public function display($office_org_id, $org_name)
    {
        $table = TableRegistry::get('OfficeOrganograms');
        $data = $table->find()->select(['id', 'title_eng', 'title_bng'])->where(['office_organization_id' => $office_org_id])->toArray();
        $data_array = array();
        foreach ($data as $row) {
            $data_array[$row['id']] = $row['title_bng'] . "[" . $row['title_eng'] . "]";
        }
        $this->set('office_organization_list', $data_array);
        $this->set('org_name', $org_name);
        $this->set('org_id', $office_org_id);
    }
}
