<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     3.0.0
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\View\Cell;

use Cake\View\Cell;
use Cake\ORM\TableRegistry;

/**
 * App View class
 */
class OfficeSearchCell extends Cell
{
    public function display($prefix = "", $office_id = 0,$is_api=0)
    {
        $this->set('is_api',$is_api);
        $this->set(compact('prefix','office_id'));
    }
    
    public function potrojariDisplay($prefix = "", $office_id = 0,$is_api=0)
    {
        $this->set('is_api',$is_api);
        $this->set(compact('prefix','office_id'));
    }
    
    public function officeSearch($prefix = "",$response_params = array(), $office_id = 0){
        $session = $this->request->session();
        $employee_office = $session->read('selected_office_section');

        $this->set('office_id',!empty($employee_office['office_id'])?$employee_office['office_id']:$office_id);
        $this->set(compact('prefix','response_params'));
    }
    
    public function officeChoose($prefix = "",$office_id = 0,$is_api=0,$openOwnOffice = false){

        $session = $this->request->session();
        $employee_office = $session->read('selected_office_section');
        $this->set('officeArray',  json_encode(array()));
        $this->set('office_id',0);
        $office_id = !empty($employee_office['office_id'])?$employee_office['office_id']:$office_id;

        if($prefix=="sender_" || $prefix=="approval_" || $prefix == 'sovapoti_' || $prefix == 'onulipi_' || $prefix == 'receiver_' || $prefix == 'attension_'){
            
            $table_instance = TableRegistry::get('Offices');
            $office = $table_instance->getOfficesByid($office_id);
            
            $this->set('officeArray',json_encode($office));
            $this->set('office_id',$office_id);
            $this->set('office_name',!empty($office[$office_id])?$office[$office_id]:'');
        }
        $this->set(compact('prefix'));
        $this->set('is_api',$is_api);
        $this->set('openOwnOffice',$openOwnOffice);
    }

    public function officeChooseShakha($prefix = "",$office_id = 0,$is_api=0,$openOwnOffice = false){

        $session = $this->request->session();
        $employee_office = $session->read('selected_office_section');
        $this->set('officeArray',  json_encode(array()));
        $this->set('office_id',0);
        $office_id = !empty($employee_office['office_id'])?$employee_office['office_id']:$office_id;

        if($prefix=="sender_" || $prefix=="approval_" || $prefix == 'sovapoti_' || $prefix == 'onulipi_' || $prefix == 'receiver_' || $prefix == 'attension_'){
            
            $table_instance = TableRegistry::get('Offices');
            $office = $table_instance->getOfficesByid($office_id);
            
            $this->set('officeArray',json_encode($office));
            $this->set('office_id',$office_id);
            $this->set('office_name',!empty($office[$office_id])?$office[$office_id]:'');
        }
        $this->set(compact('prefix'));
        $this->set('is_api',$is_api);
        $this->set('openOwnOffice',$openOwnOffice);
    }

    
    public function officeNew($prefix = ""){
        $this->set(compact('prefix'));
    }
    public function groupUser ($prefix = "",$response_params = array(), $office_id = 0){
        $session = $this->request->session();
        $employee_office = $session->read('selected_office_section');
        $office_id = !empty($employee_office['office_id'])?$employee_office['office_id']:$office_id;
        $tableGroupPotrojari = TableRegistry::get("PotrojariGroups");
        $conditions = [
            'OR' =>
            [
                'AND' => [
                    'privacy_type' => 'private', 'creator_office_id' => $office_id,
                ],
                'privacy_type' => 'public',
            ]
        ];

        $groups = $tableGroupPotrojari->getGroupList($conditions,0)->order(['privacy_type'=>'asc'])->toArray();

        $valueList = [];
        if (!empty($groups)) {
            foreach ($groups as $key => $value) {
                $row          = array();
                $row['id']    = $value['id'];
                $row['value'] = $value['group_name'];
                $row['list']  =  json_encode($value['potrojari_groups_users']);
                $valueList[]  = $row;
            }
        }

        $this->set('office_id',$office_id);
        $this->set(compact('prefix','response_params','valueList'));
    }
    public function customRm($prefix = "", $office_id = 0,$potro_type = 21)
    {
        $this->set(compact('prefix','office_id','potro_type'));
    }
    public function OfficeSelection($prefix = "",$office_id = 0){
        $session = $this->request->session();
        $employee_office = $session->read('selected_office_section');
        $this->set('officeArray',  json_encode(array()));
        $this->set('office_id',0);
        $office_id = !empty($employee_office['office_id'])?$employee_office['office_id']:$office_id;
        if($prefix=="sender_" || $prefix=="approval_" || $prefix == 'sovapoti_'){

            $table_instance = TableRegistry::get('Offices');
            $office = $table_instance->getOfficesByid($office_id);

            $this->set('officeArray',json_encode($office));
            $this->set('office_id',$office_id);
        }
        $this->set(compact('prefix'));
    }

    public function officeChooseProtikolpo($prefix = "",$office_id = 0,$is_api=0,$openOwnOffice = false){

        $session = $this->request->session();
        $employee_office = $session->read('selected_office_section');
        $this->set('officeArray',  json_encode(array()));
        $this->set('office_id',0);
        $office_id = !empty($employee_office['office_id'])?$employee_office['office_id']:$office_id;

        if($prefix=="sender_" || $prefix=="approval_" || $prefix == 'sovapoti_' || $prefix == 'onulipi_' || $prefix == 'receiver_' || $prefix == 'attension_'){

            $table_instance = TableRegistry::get('Offices');
            $office = $table_instance->getOfficesByid($office_id);

            $this->set('officeArray',json_encode($office));
            $this->set('office_id',$office_id);
            $this->set('office_name',!empty($office[$office_id])?$office[$office_id]:'');
        }
        $this->set(compact('prefix'));
        $this->set('is_api',$is_api);
        $this->set('openOwnOffice',$openOwnOffice);
    }
}
