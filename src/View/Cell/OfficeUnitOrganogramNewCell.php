<?php
namespace App\View\Cell;

use Cake\ORM\TableRegistry;
use Cake\View\Cell;

/**
 * OfficeUnitOrganogramNew cell
 */
class OfficeUnitOrganogramNewCell extends Cell
{

    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    /**
     * Default display method.
     *
     * @return void
     */
    public function display($prefix = "", $options= [])
    {
        $table_instance = TableRegistry::get('OfficeMinistries');
        $data_items = $table_instance->find('list')->toArray();
        $this->set('officeMinistries', $data_items);
        $this->set('prefix', $prefix);
        $this->set('options', $options);
    }
}
