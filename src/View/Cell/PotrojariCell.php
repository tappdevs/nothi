<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     3.0.0
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\View\Cell;

use Cake\View\Cell;
use Cake\ORM\TableRegistry;

/**
 * App View class
 */
class PotrojariCell extends Cell
{
    public function display($template = array(), $potrojari=array(),$templates = array(),$is_endorse = 0,$potro_type = 0, $endorse_sender_info = array())
    {
        $session = $this->request->session();
        $my_office = $session->read('selected_office_section');
        $this->set('office_id',!empty($my_office['office_id'])?$my_office['office_id']:0);
        $this->set(compact('template'));
        $this->set(compact('is_endorse'));

        $this->set(compact('potro_type'));
        $attachments = array();
        $receivers = array();
        $receivergroups = array();
        $onulipis = array();
        $onulipigroups = array();
        
        if(!empty($potrojari)){
            $potrojari_receiver = TableRegistry::get('PotrojariReceiver');
            $potrojari_onulipi = TableRegistry::get('PotrojariOnulipi');
            
            $receivers = $potrojari_receiver->find()->where(['potrojari_id'=>$potrojari->id]);
            $onulipis = $potrojari_onulipi->find()->where(['potrojari_id'=>$potrojari->id]);

            if(!empty($receivers)){
                foreach($receivers as $key=>$value){
                     if ($value['group_id'] > 0) {
                         $receivergroups[$value['group_id']][] = !empty($value['receiving_officer_designation_id'])?$value['receiving_officer_designation_id']:(!empty($value['receiving_officer_email'])?$value['receiving_officer_email']:$value['receiving_officer_designation_label']);
                     }
                }
            }
            if(!empty($onulipis)){
                foreach($onulipis as $key=>$value){
                     if ($value['group_id'] > 0) {
                         $onulipigroups[$value['group_id']][] = !empty($value['receiving_officer_designation_id'])?$value['receiving_officer_designation_id']:(!empty($value['receiving_officer_email'])?$value['receiving_officer_email']:$value['receiving_officer_designation_label']);
                     }
                }
            }
        }

        $this->set(compact('receivers', 'onulipis', 'potrojari','receivergroups','onulipigroups','templates','endorse_sender_info'));
    }
    public function apiDisplay($template = array(), $potrojari=array(),$templates = array(),$is_endorse = 0, $employee_office = [],$is_api=1)
    {

        $my_office = $employee_office;
        $this->set('office_id',!empty($my_office['office_id'])?$my_office['office_id']:0);
        $this->set(compact('template'));
        $this->set(compact('is_endorse'));
        $this->set(compact('is_api'));

        $attachments = array();
        $receivers = array();
        $receivergroups = array();
        $onulipis = array();
        $onulipigroups = array();

        if(!empty($potrojari)){
            $potrojari_receiver = TableRegistry::get('PotrojariReceiver');
            $potrojari_onulipi = TableRegistry::get('PotrojariOnulipi');

            $receivers = $potrojari_receiver->find()->where(['potrojari_id'=>$potrojari->id]);
            $onulipis = $potrojari_onulipi->find()->where(['potrojari_id'=>$potrojari->id]);

            if(!empty($receivers)){
                foreach($receivers as $key=>$value){
                    if ($value['group_id'] > 0) {
                        $receivergroups[$value['group_id']][] = !empty($value['receiving_officer_designation_id'])?$value['receiving_officer_designation_id']:(!empty($value['receiving_officer_email'])?$value['receiving_officer_email']:$value['receiving_officer_designation_label']);
                    }
                }
            }
            if(!empty($onulipis)){
                foreach($onulipis as $key=>$value){
                    if ($value['group_id'] > 0) {
                        $onulipigroups[$value['group_id']][] = !empty($value['receiving_officer_designation_id'])?$value['receiving_officer_designation_id']:(!empty($value['receiving_officer_email'])?$value['receiving_officer_email']:$value['receiving_officer_designation_label']);
                    }
                }
            }
        }

        $this->set(compact('receivers', 'onulipis', 'potrojari','receivergroups','onulipigroups','templates'));
    }
    
    public function sarNothiDraft($template = array(), $potrojari=array(),$nothi_office=0)
    {
        
          $session = $this->request->session();
            $employee_office = $session->read('selected_office_section');
            $otherNothi = false;
            if($nothi_office!=0 && $nothi_office!=$employee_office['office_id']){
                $otherNothi = true;

                $this->switchOffice($nothi_office, 'MainNothiOffice');
            }else{
                $nothi_office = $employee_office['office_id'];
            }
        $this->set('nothi_office',$nothi_office);    
        $this->set(compact('template'));       
        $this->set(compact('potrojari'));
      
    }
     public function displayRm($template = array(), $potrojari=array(),$templates = array(),$is_endorse = 0,$potro_type = 0)
    {
        $session = $this->request->session();
        $my_office = $session->read('selected_office_section');
        $this->set('office_id',!empty($my_office['office_id'])?$my_office['office_id']:0);
        $this->set(compact('template'));
        $this->set(compact('is_endorse'));
        if(empty($potro_type)){
            $potro_type = 21;
        }
        $this->set(compact('potro_type'));

        $attachments = array();
        $receivers = array();
        $receivergroups = array();
        $onulipis = array();
        $onulipigroups = array();

        if(!empty($potrojari)){
            $potrojari_receiver = TableRegistry::get('PotrojariReceiver');
            $potrojari_onulipi = TableRegistry::get('PotrojariOnulipi');

            $receivers = $potrojari_receiver->find()->where(['potrojari_id'=>$potrojari->id]);
            $onulipis = $potrojari_onulipi->find()->where(['potrojari_id'=>$potrojari->id]);

            if(!empty($receivers)){
                foreach($receivers as $key=>$value){
                     if ($value['group_id'] > 0) {
                         $receivergroups[$value['group_id']][] = !empty($value['receiving_officer_designation_id'])?$value['receiving_officer_designation_id']:(!empty($value['receiving_officer_email'])?$value['receiving_officer_email']:$value['receiving_officer_designation_label']);
                     }
                }
            }
            if(!empty($onulipis)){
                foreach($onulipis as $key=>$value){
                     if ($value['group_id'] > 0) {
                         $onulipigroups[$value['group_id']][] = !empty($value['receiving_officer_designation_id'])?$value['receiving_officer_designation_id']:(!empty($value['receiving_officer_email'])?$value['receiving_officer_email']:$value['receiving_officer_designation_label']);
                     }
                }
            }
        }

        $this->set(compact('receivers', 'onulipis', 'potrojari','receivergroups','onulipigroups','templates'));
    }
}
