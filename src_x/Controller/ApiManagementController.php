<?php

namespace App\Controller;

use App\Controller\ProjapotiController;
use App\Model\Table;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\ORM\Exception\MissingEntityException;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
use Cake\Event\Event;

class ApiManagementController extends ProjapotiController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);

        $this->Auth->allow(['apiGetAccess', 'apiGetUserServices',
            'apiGetOffices', 'apiGetMinistries', 'apiGetLayers', 'apiGetOfficeOrigins',
            'apiGeoDivisions', 'apiGeoDistrics', 'apiGeoThanas', 'apiGeoUpazilas',
            'apiGeoUnions', 'apiGeoMunicipalities', 'apiGeoMunicipalityWards', 'apiGeoCityCorporations',
            'apiGeoCityCorporationWards', 'apiGetEmployee', 'apiGetUserbyAuthentication',
            'apiGetUserInformation', 'apiGetUserDetailInformation', 'apiGetUnitInfo',
            'apiGetOfficeInfo', 'apiGetDesignationInfo', 'apiGetOfficeTree', 'apiGetOfficeFronDesk','apiGetOfficeOriginTree',
            'apiGetOfflineOffices', 'apiGetUnitwiseDesignation', 'apiServiceReport', 'apiGetMultipleOfficesFrontDesk', 'apiGetUnitEmployee',
            'apiUserLoginService','changeUserPassword','resetUserPassword','getOtherPartyCertificate',
        ]);
    }

    public function apiGetAccess()
    {

        $table_nothi_access_token = TableRegistry::get('ApiAccessToken');
        $table_api_users = TableRegistry::get('ApiUsers');
        $return = array(
            'status' => 'error',
            'msg' => 'Invalid request!!!'
        );
        if ($this->request->is('post')) {
            $this->request->query = $this->request->data;
        }

        $username = $this->request->query['username'];
        $password = $this->request->query['password'];
        $ip[] = $this->request->clientIp();
        $ip[] = getIP();

        if (!empty($username) && !empty($password) && !empty($ip)) {
            $getUserInfomation = $table_api_users->getAccess(['username' => $username,
                'password' => $password, 'ip' => $ip]);

            if (empty($getUserInfomation)) {
                $return = array(
                    'status' => 'error',
                    'msg' => 'Not a registerd access!!! Please register your api access',
                    'ip' => $ip,
                );
            } else {

                $expireToken = $table_nothi_access_token->expireToken($getUserInfomation['id']);
                if (isset($expireToken['status']) && $expireToken['status'] === true) {
                    $getToken = $table_nothi_access_token->setToken($getUserInfomation['id'],
                        ['ip' => $ip[0], 'request' => $this->request]);

                    if (isset($getToken['status']) && $getToken['status'] === true) {
                        $return = array(
                            'status' => 'success',
                            'token' => $getToken['token']
                        );
                    } else {
                        $return = array(
                            'status' => 'error',
                            'msg' => "Cannot create token! Reason: " . $getToken
                        );
                    }
                } else {
                    $return = array(
                        'status' => 'error',
                        'msg' => $expireToken
                    );
                }
            }
        }

        $this->response->body(json_encode($return));
        $this->response->type('application/json');
        return $this->response;
    }

    public function apiGetUserServices($token, $service)
    {

        $table_nothi_access_token = TableRegistry::get('ApiAccessToken');

        $return = array(
            'status' => 'error',
            'msg' => 'Invalid request!!!'
        );

        if ($this->request->is('post')) {
            $ip[] = $this->request->clientIp();
            $ip[] = getIP();


            $checkToken = $table_nothi_access_token->checkToken($token, $ip);

            if (isset($checkToken['status']) && $checkToken['status'] === true) {
                if (method_exists($this, $service)) {

                    $return = $this->{$service}();
                } else {
                    $return = array(
                        'status' => 'error',
                        'msg' => 'Unregistered service!!!'
                    );
                }
            } else {
                $return = array(
                    'status' => 'error',
                    'msg' => $checkToken
                );
            }
        }

        $this->response->body(json_encode($return));
        $this->response->type('application/json');
        return $this->response;
    }

    public function apiGetOffices()
    {

        $this->layout = 'ajax';
        $office_table = TableRegistry::get('Offices');

        $return = array(
            'status' => 'error',
            'msg' => "দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না"
        );

        if ($this->request->is('post')) {

            $apikey = isset($this->request->data['api_key']) ? $this->request->data['api_key']
                : '';
            $active = isset($this->request->data['active']) ? $this->request->data['active']
                : 1;

            $conditions = array();

            if (isset($this->request->data['office_ministry_id']) && !empty($this->request->data['office_ministry_id'])) {
                if (is_string($this->request->data['office_ministry_id'])) {
                    $conditions[] = array(
                        'Offices.office_ministry_id IN' => explode(',',
                            $this->request->data['office_ministry_id'])
                    );
                } else {
                    $conditions[] = array(
                        'Offices.office_ministry_id' => intval($this->request->data['office_ministry_id'])
                    );
                }
            }

            if (isset($this->request->data['office_layer_id']) && !empty($this->request->data['office_layer_id'])) {
                if (is_string($this->request->data['office_layer_id'])) {
                    $conditions[] = array(
                        'office_layer_id IN' => explode(',',
                            $this->request->data['office_layer_id'])
                    );
                } else {
                    $conditions[] = array(
                        'office_layer_id' => intval($this->request->data['office_layer_id'])
                    );
                }
            }

            $officelayertype = '';
            $layer_level_eng = '';

            if (isset($this->request->data['office_layer_type']) && !empty($this->request->data['office_layer_type'])) {
//                $officelayertype = $officeLayerTypeArray[intval($this->request->data['office_layer_type'])];
                $officelayertype = intval($this->request->data['office_layer_type']);
            }
            if (isset($this->request->data['layer_level_bng']) && !empty($this->request->data['layer_level_bng'])) {
                $layer_level_bng = $this->request->data['layer_level_bng'];
            }

            if (isset($this->request->data['layer_level_eng']) && !empty($this->request->data['layer_level_eng'])) {
                $layer_level_eng = $this->request->data['layer_level_eng'];
            }

            if (isset($this->request->data['office_origin_id']) && !empty($this->request->data['office_origin_id'])) {
                if (is_string($this->request->data['office_origin_id'])) {
                    $conditions[] = array(
                        'office_origin_id IN' => explode(',',
                            $this->request->data['office_origin_id'])
                    );
                } else {
                    $conditions[] = array(
                        'office_origin_id' => intval($this->request->data['office_origin_id'])
                    );
                }
            }
            
            $division_name_bng = '';

            if (isset($this->request->data['division_name_bng']) && !empty($this->request->data['division_name_bng'])) {
                $division_name_bng = ($this->request->data['division_name_bng']);
            }

            if (isset($this->request->data['geo_division_id']) && !empty($this->request->data['geo_division_id'])) {
                if (is_string($this->request->data['geo_division_id'])) {
                    $conditions[] = array(
                        'geo_division_id IN' => explode(',',
                            $this->request->data['geo_division_id'])
                    );
                } else {
                    $conditions[] = array(
                        'geo_division_id' => intval($this->request->data['geo_division_id'])
                    );
                }
            }

            if (isset($this->request->data['geo_district_id']) && !empty($this->request->data['geo_district_id'])) {
                if (is_string($this->request->data['geo_district_id'])) {
                    $conditions[] = array(
                        'geo_district_id IN' => explode(',',
                            $this->request->data['geo_district_id'])
                    );
                } else {
                    $conditions[] = array(
                        'geo_district_id' => intval($this->request->data['geo_district_id'])
                    );
                }
            }

            $district_name_bng = '';

            if (isset($this->request->data['district_name_bng']) && !empty($this->request->data['district_name_bng'])) {
                $district_name_bng = ($this->request->data['district_name_bng']);
            }

            if (isset($this->request->data['geo_upazila_id']) && !empty($this->request->data['geo_upazila_id'])) {
                if (is_string($this->request->data['geo_upazila_id'])) {
                    $conditions[] = array(
                        'geo_upazila_id IN' => explode(',',
                            $this->request->data['geo_upazila_id'])
                    );
                } else {
                    $conditions[] = array(
                        'geo_upazila_id' => intval($this->request->data['geo_upazila_id'])
                    );
                }
            }

            $upazila_name_bng = '';

            if (isset($this->request->data['upazila_name_bng']) && !empty($this->request->data['upazila_name_bng'])) {
                $upazila_name_bng = ($this->request->data['upazila_name_bng']);
            }

            if (isset($this->request->data['geo_union_id']) && !empty($this->request->data['geo_union_id'])) {
                if (is_string($this->request->data['geo_union_id'])) {
                    $conditions[] = array(
                        'geo_union_id IN' => explode(',',
                            $this->request->data['geo_union_id'])
                    );
                } else {
                    $conditions[] = array(
                        'geo_union_id' => intval($this->request->data['geo_union_id'])
                    );
                }
            }
            try {
                $officelist = [];
                if ($apikey == API_KEY) {
                    $officelist = $office_table->find('list',
                        ['keyField' => 'id', 'valueField' => 'office_name_bng']);


                    if (!empty($conditions)) {
                        $officelist = $officelist->where($conditions);
                    }

                    if (!empty($officelayertype)) {
                        $officelist = $officelist->join([
                            'OfficeLayers' => [
                                'table' => 'office_layers',
                                'type' => 'inner',
                                'conditions' => "OfficeLayers.id = Offices.office_layer_id"
                            ]
                        ])->where(['OfficeLayers.layer_level' => $officelayertype]);

                        if(!empty($layer_level_bng)){
                            $officelist->where(['OfficeLayers.layer_name_bng LIKE ' => "{$layer_level_bng}%"]);
                        }
                        if(!empty($layer_level_eng)){
                            $officelist->where(['OfficeLayers.layer_name_eng LIKE ' => "{$layer_level_eng}%"]);
                        }

                    }
                    else{
                        if(!empty($layer_level_bng) || !empty($layer_level_eng)){
                            $officelist = $officelist->join([
                                'OfficeLayers' => [
                                    'table' => 'office_layers',
                                    'type' => 'inner',
                                    'conditions' => "OfficeLayers.id = Offices.office_layer_id"
                                ]
                            ]);

                            if(!empty($layer_level_bng)){
                                $officelist->where(['OfficeLayers.layer_name_bng LIKE ' => "{$layer_level_bng}%"]);
                            }
                            if(!empty($layer_level_eng)){
                                $officelist->where(['OfficeLayers.layer_name_eng LIKE ' => "{$layer_level_eng}%"]);
                            }
                        }
                    }
                    
                    if (!empty($division_name_bng)) {
                        $officelist = $officelist->join([
                            'GeoDivisions' => [
                                'table' => 'geo_divisions',
                                'type' => 'inner',
                                'conditions' => "GeoDivisions.id = Offices.geo_division_id"
                            ]
                        ])->where(['GeoDivisions.division_name_bng LIKE' => "%{$division_name_bng}%"]);
                    }

                    if (!empty($district_name_bng)) {
                        $officelist = $officelist->join([
                            'GeoDistricts' => [
                                'table' => 'geo_districts',
                                'type' => 'inner',
                                'conditions' => "GeoDistricts.id = Offices.geo_district_id"
                            ]
                        ])->where(['GeoDistricts.district_name_bng LIKE' => "%{$district_name_bng}%"]);
                    }

                    if (!empty($upazila_name_bng)) {
                        $officelist = $officelist->join([
                            'GeoUpazilas' => [
                                'table' => 'geo_upazilas',
                                'type' => 'inner',
                                'conditions' => "GeoUpazilas.id = Offices.geo_upazila_id"
                            ]
                        ])->where(['GeoUpazilas.upazila_name_bng LIKE' => "%{$upazila_name_bng}%"]);
                    }

                    if ($active == -1) {

                    } else {
                        $officelist->where(['Offices.active_status' => $active]);
                    }

                    $officelist = $officelist->order(['Offices.parent_office_id asc, Offices.office_name_bng asc'])->toArray();

                    $return = array("status" => 'success', 'data' => $officelist);
                } else {
                    $return = array(
                        'status' => 'error',
                        'msg' => "দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না"
                    );
                }
            } catch (\Exception $ex) {
                $return = array(
                    'status' => 'error',
                    'msg' => "দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না" . $ex->getMessage()
                );
            }
        }

        $this->response->body(json_encode($return));
        $this->response->type('application/json');
        return $this->response;
    }

    public function apiGetMinistries()
    {

        $this->layout = 'ajax';
        $office_ministry_table = TableRegistry::get('OfficeMinistries');

        $return = array(
            'status' => 'error',
            'msg' => "দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না"
        );

        if ($this->request->is('post')) {
            $apikey = isset($this->request->data['api_key']) ? $this->request->data['api_key']
                : '';

            $office_type = isset($this->request->data['office_type']) ? intval($this->request->data['office_type'])
                : 0;

            if ($apikey == API_KEY) {
                $ministrylist = $office_ministry_table->find('list',
                    ['keyField' => 'id', 'valueField' => 'name_bng']);

                if (!empty($office_type)) {
                    $ministrylist = $ministrylist->where(['office_type' => $office_type,
                        'active_status' => 1])->order(['name_bng asc'])->toArray();
                }

                $return = array("status" => 'success', 'data' => $ministrylist);
            } else {
                $return = array(
                    'status' => 'error',
                    'msg' => "দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না"
                );
            }
        }

        $this->response->body(json_encode($return));
        $this->response->type('application/json');
        return $this->response;
    }

    public function apiGetLayers()
    {

        $this->layout = 'ajax';

        $return = array(
            'status' => 'error',
            'msg' => "দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না"
        );

        $officeLayers = TableRegistry::get('OfficeLayers');

        if ($this->request->is('post')) {
            $apikey = isset($this->request->data['api_key']) ? $this->request->data['api_key']
                : '';

            $ministry_id = isset($this->request->data['office_ministry_id']) ? intval($this->request->data['office_ministry_id'])
                : 0;

            if ($apikey == API_KEY) {

                $layerslist = $officeLayers->find('list',
                    ['keyField' => 'id', 'valueField' => 'layer_name_bng'])->where(['office_ministry_id' => $ministry_id,
                    'OfficeLayers.active_status' => 1])->order(['layer_name_bng asc']);

                $return = array("status" => 'success', 'data' => $layerslist);
            } else {
                $return = array(
                    'status' => 'error',
                    'msg' => "দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না"
                );
            }
        }

        $this->response->body(json_encode($return));
        $this->response->type('application/json');
        return $this->response;
    }

    public function apiGetOfficeOrigins()
    {

        $this->layout = 'ajax';

        $return = array(
            'status' => 'error',
            'msg' => "দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না"
        );

        $officeOriginsTable = TableRegistry::get('OfficeOrigins');

        if ($this->request->is('post')) {
            $apikey = isset($this->request->data['api_key']) ? $this->request->data['api_key']
                : '';

            $ministry_id = isset($this->request->data['office_ministry_id']) ? intval($this->request->data['office_ministry_id'])
                : 0;
            $layer_id = isset($this->request->data['office_layer_id']) ? intval($this->request->data['office_layer_id'])
                : 0;

            if ($apikey == API_KEY) {

                $officeOriginlist = $officeOriginsTable->find('list',
                    ['keyField' => 'id', 'valueField' => 'layer_name_bng'])->where(['office_ministry_id' => $ministry_id,
                    'office_layer_id' => $layer_id, 'active_status' => 1])->toArray();

                $return = array("status" => 'success', 'data' => $officeOriginlist);
            } else {
                $return = array(
                    'status' => 'error',
                    'msg' => "দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না"
                );
            }
        }

        $this->response->body(json_encode($return));
        $this->response->type('application/json');
        return $this->response;
    }

    public function apiGeoDivisions()
    {

        $this->layout = 'ajax';

        $return = array(
            'status' => 'error',
            'msg' => "দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না"
        );

        $geoDivisionsTable = TableRegistry::get('GeoDivisions');

        if ($this->request->is('post')) {
            $apikey = isset($this->request->data['api_key']) ? $this->request->data['api_key']
                : '';

            if ($apikey == API_KEY) {

                $divisionslist = $geoDivisionsTable->find('list',
                    ['keyField' => 'id', 'valueField' => 'division_name_bng'])->order(['division_name_bng asc'])->toArray();

                $return = array("status" => 'success', 'data' => $divisionslist);
            } else {
                $return = array(
                    'status' => 'error',
                    'msg' => "দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না"
                );
            }
        }

        $this->response->body(json_encode($return));
        $this->response->type('application/json');
        return $this->response;
    }

    public function apiGeoDistrics()
    {

        $this->layout = 'ajax';

        $return = array(
            'status' => 'error',
            'msg' => "দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না"
        );

        $geoDistrictsTable = TableRegistry::get('GeoDistricts');

        if ($this->request->is('post')) {
            $apikey = isset($this->request->data['api_key']) ? $this->request->data['api_key']
                : '';

            if ($apikey == API_KEY) {

                $geo_division_id = isset($this->request->data['geo_division_id'])
                    ? $this->request->data['geo_division_id'] : 0;

                $districtslist = $geoDistrictsTable->find('list',
                    ['keyField' => 'id', 'valueField' => 'district_name_bng'])->where(['geo_division_id' => $geo_division_id])->order(['district_name_bng asc'])->toArray();

                $return = array("status" => 'success', 'data' => $districtslist);
            } else {
                $return = array(
                    'status' => 'error',
                    'msg' => "দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না"
                );
            }
        }

        $this->response->body(json_encode($return));
        $this->response->type('application/json');
        return $this->response;
    }

    public function apiGeoThanas()
    {

        $this->layout = 'ajax';

        $return = array(
            'status' => 'error',
            'msg' => "দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না"
        );

        $geoThanasTable = TableRegistry::get('GeoThanas');

        if ($this->request->is('post')) {
            $apikey = isset($this->request->data['api_key']) ? $this->request->data['api_key']
                : '';

            if ($apikey == API_KEY) {

                $geo_division_id = isset($this->request->data['geo_division_id'])
                    ? $this->request->data['geo_division_id'] : 0;
                $geo_district_id = isset($this->request->data['geo_district_id'])
                    ? $this->request->data['geo_district_id'] : 0;

                $districtslist = $geoThanasTable->find('list',
                    ['keyField' => 'id', 'valueField' => 'thana_name_bng'])->where(['geo_division_id' => $geo_division_id,
                    'geo_district_id' => $geo_district_id])->order(['thana_name_bng asc'])->toArray();

                $return = array("status" => 'success', 'data' => $districtslist);
            } else {
                $return = array(
                    'status' => 'error',
                    'msg' => "দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না"
                );
            }
        }

        $this->response->body(json_encode($return));
        $this->response->type('application/json');
        return $this->response;
    }

    public function apiGeoUpazilas()
    {

        $this->layout = 'ajax';

        $return = array(
            'status' => 'error',
            'msg' => "দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না"
        );

        $geoUpazilasTable = TableRegistry::get('GeoUpazilas');

        if ($this->request->is('post')) {
            $apikey = isset($this->request->data['api_key']) ? $this->request->data['api_key']
                : '';

            if ($apikey == API_KEY) {

                $geo_division_id = isset($this->request->data['geo_division_id'])
                    ? $this->request->data['geo_division_id'] : 0;
                $geo_district_id = isset($this->request->data['geo_district_id'])
                    ? $this->request->data['geo_district_id'] : 0;

                $upazilalist = $geoUpazilasTable->find('list',
                    ['keyField' => 'id', 'valueField' => 'upazila_name_bng'])->where(['geo_division_id' => $geo_division_id,
                    'geo_district_id' => $geo_district_id])->order(['upazila_name_bng asc'])->toArray();

                $return = array("status" => 'success', 'data' => $upazilalist);
            } else {
                $return = array(
                    'status' => 'error',
                    'msg' => "দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না"
                );
            }
        }

        $this->response->body(json_encode($return));
        $this->response->type('application/json');
        return $this->response;
    }

    public function apiGeoUnions()
    {

        $this->layout = 'ajax';

        $return = array(
            'status' => 'error',
            'msg' => "দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না"
        );

        $geoUnionTable = TableRegistry::get('GeoUnions');

        if ($this->request->is('post')) {
            $apikey = isset($this->request->data['api_key']) ? $this->request->data['api_key']
                : '';

            if ($apikey == API_KEY) {

                $geo_division_id = isset($this->request->data['geo_division_id'])
                    ? $this->request->data['geo_division_id'] : 0;
                $geo_district_id = isset($this->request->data['geo_district_id'])
                    ? $this->request->data['geo_district_id'] : 0;
                $geo_upazila_id = isset($this->request->data['geo_upazila_id'])
                    ? $this->request->data['geo_upazila_id'] : 0;

                $unionlist = $geoUnionTable->find('list',['keyField' => 'id', 'valueField' => 'union_name_bng']);
                if(!empty($geo_division_id)){
                    $unionlist->where(['geo_division_id' => $geo_division_id]);
                }
                if(!empty($geo_district_id)){
                    $unionlist->where(['geo_district_id' => $geo_district_id]);
                }
                if(!empty($geo_upazila_id)){
                    $unionlist->where(['geo_upazila_id' => $geo_upazila_id]);
                }
                $unionlist->order(['union_name_bng asc'])->toArray();
                $return = array("status" => 'success', 'data' => $unionlist);
            } else {
                $return = array(
                    'status' => 'error',
                    'msg' => "দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না"
                );
            }
        }

        $this->response->body(json_encode($return));
        $this->response->type('application/json');
        return $this->response;
    }

    public function apiGeoMunicipalities()
    {

        $this->layout = 'ajax';

        $return = array(
            'status' => 'error',
            'msg' => "দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না"
        );

        $geoMunicipalitiesTable = TableRegistry::get('GeoMunicipalities');

        if ($this->request->is('post')) {
            $apikey = isset($this->request->data['api_key']) ? $this->request->data['api_key']
                : '';

            if ($apikey == API_KEY) {

                $geo_division_id = isset($this->request->data['geo_division_id'])
                    ? $this->request->data['geo_division_id'] : 0;
                $geo_district_id = isset($this->request->data['geo_district_id'])
                    ? $this->request->data['geo_district_id'] : 0;
                $geo_upazila_id = isset($this->request->data['geo_upazila_id'])
                    ? $this->request->data['geo_upazila_id'] : 0;

                $geoMunicipalities = $geoMunicipalitiesTable->find('list',
                    ['keyField' => 'id', 'valueField' => 'municipality_name_bng'])->where(['geo_division_id' => $geo_division_id,
                    'geo_district_id' => $geo_district_id, 'geo_upazila_id' => $geo_upazila_id])->order(['municipality_name_bng asc'])->toArray();

                $return = array("status" => 'success', 'data' => $geoMunicipalities);
            } else {
                $return = array(
                    'status' => 'error',
                    'msg' => "দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না"
                );
            }
        }

        $this->response->body(json_encode($return));
        $this->response->type('application/json');
        return $this->response;
    }

    public function apiGeoMunicipalityWards()
    {

        $this->layout = 'ajax';

        $return = array(
            'status' => 'error',
            'msg' => "দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না"
        );

        $geoMunicipalityWardsTable = TableRegistry::get('GeoMunicipalityWards');

        if ($this->request->is('post')) {
            $apikey = isset($this->request->data['api_key']) ? $this->request->data['api_key']
                : '';

            if ($apikey == API_KEY) {

                $geo_division_id = isset($this->request->data['geo_division_id'])
                    ? $this->request->data['geo_division_id'] : 0;
                $geo_district_id = isset($this->request->data['geo_district_id'])
                    ? $this->request->data['geo_district_id'] : 0;
                $geo_upazila_id = isset($this->request->data['geo_upazila_id'])
                    ? $this->request->data['geo_upazila_id'] : 0;
                $geo_municipality_id = isset($this->request->data['geo_municipality_id'])
                    ? $this->request->data['geo_municipality_id'] : 0;

                $geoMunicipalityWards = $geoMunicipalityWardsTable->find('list',
                    ['keyField' => 'id', 'valueField' => 'ward_name_bng'])->where(['geo_division_id' => $geo_division_id,
                    'geo_district_id' => $geo_district_id, 'geo_upazila_id' => $geo_upazila_id,
                    'geo_municipality_id' => $geo_municipality_id])->toArray();

                $return = array("status" => 'success', 'data' => $geoMunicipalityWards);
            } else {
                $return = array(
                    'status' => 'error',
                    'msg' => "দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না"
                );
            }
        }

        $this->response->body(json_encode($return));
        $this->response->type('application/json');
        return $this->response;
    }

    public function apiGeoCityCorporations()
    {

        $this->layout = 'ajax';

        $return = array(
            'status' => 'error',
            'msg' => "দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না"
        );

        $geoCityCorporationsTable = TableRegistry::get('GeoCityCorporations');

        if ($this->request->is('post')) {
            $apikey = isset($this->request->data['api_key']) ? $this->request->data['api_key']
                : '';

            if ($apikey == API_KEY) {

                $geo_division_id = isset($this->request->data['geo_division_id'])
                    ? $this->request->data['geo_division_id'] : 0;
                $geo_district_id = isset($this->request->data['geo_district_id'])
                    ? $this->request->data['geo_district_id'] : 0;

                $geoCityCorporations = $geoCityCorporationsTable->find('list',
                    ['keyField' => 'id', 'valueField' => 'city_corporation_name_bng'])->where(['geo_division_id' => $geo_division_id,
                    'geo_district_id' => $geo_district_id])->toArray();

                $return = array("status" => 'success', 'data' => $geoCityCorporations);
            } else {
                $return = array(
                    'status' => 'error',
                    'msg' => "দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না"
                );
            }
        }

        $this->response->body(json_encode($return));
        $this->response->type('application/json');
        return $this->response;
    }

    public function apiGeoCityCorporationWards()
    {

        $this->layout = 'ajax';

        $return = array(
            'status' => 'error',
            'msg' => "দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না"
        );

        $geoCityCorporationWardsTable = TableRegistry::get('GeoCityCorporationWards');

        if ($this->request->is('post')) {
            $apikey = isset($this->request->data['api_key']) ? $this->request->data['api_key']
                : '';

            if ($apikey == API_KEY) {

                $geo_division_id = isset($this->request->data['geo_division_id'])
                    ? $this->request->data['geo_division_id'] : 0;
                $geo_district_id = isset($this->request->data['geo_district_id'])
                    ? $this->request->data['geo_district_id'] : 0;
                $geo_city_corporation_id = isset($this->request->data['geo_city_corporation_id'])
                    ? $this->request->data['geo_city_corporation_id'] : 0;

                $geoCityCorporationWards = $geoCityCorporationWardsTable->find('list',
                    ['keyField' => 'id', 'valueField' => 'ward_name_bng'])->where(['geo_division_id' => $geo_division_id,
                    'geo_district_id' => $geo_district_id, 'geo_city_corporation_id' => $geo_city_corporation_id])->toArray();

                $return = array("status" => 'success', 'data' => $geoCityCorporationWards);
            } else {
                $return = array(
                    'status' => 'error',
                    'msg' => "দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না"
                );
            }
        }

        $this->response->body(json_encode($return));
        $this->response->type('application/json');
        return $this->response;
    }

    public function apiGetUserProfilePhoto($username = '', $encode = 0)
    {

        return $this->getProfile($username, $encode, 0);
    }

    public function apiGetUserSignature($username = '', $encode = 1)
    {

        $options['token'] = $this->makeEncryptedData($username);
        return $this->getSignature($username, $encode,0,false,null,$options);
    }

    //loginAPI
    public function APILogin()
    {
        $return = array(
            "status" => "error",
            "message" => "Invalid Request"
        );

        if ($this->request->is('post')) {
            if (!empty($this->request->data)) {
                $apikey = isset($this->request->data['api_key']) ? $this->request->data['api_key']
                    : '';

                if ($apikey == API_KEY) {
                    $user = $this->Auth->identify();
                    if (!empty($user)) {
                        $return = array(
                            'status' => 'success',
                            'data' => $user
                        );
                    } else {
                        $return = array(
                            "status" => "error",
                            "message" => 'দুঃখিত! লগইন আইডি অথবা পাসওয়ার্ড সঠিক নয়'
                        );
                    }
                } else {
                    $return = array(
                        'status' => 'error',
                        'msg' => "দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না"
                    );
                }
            }
        }

        echo json_encode($return);
        die;
    }

    /*     * New API By KAT - Start* */

    public function apiGetUserInformation()
    {
        if ($this->request->is('post')) {
            $api_key = '';
            $result = [
                'status' => 'error',
                'msg' => "দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না"
            ];
            if (!empty($this->request->data['api_key'])) {
                $api_key = $this->request->data['api_key'];

                if ($api_key == API_KEY) {
                    if (!empty($this->request->data['employee_id'])) {
                        $employee_id = $this->request->data['employee_id'];

                        $table = TableRegistry::get('EmployeeRecords');
                        $query = $table->find()->where(['id' => $employee_id])->first();

                        if (!empty($query)) {
                            $result = [
                                'status' => 'success',
                                'data' => $query
                            ];
                        }
                    }
                }
            }
        }
        $this->response->body(json_encode($result));
        $this->response->type('application/json');
        return $this->response;
    }

    public function apiGetUserbyAuthentication()
    {
        $result = [
            'status' => 'error',
            'msg' => "Invalid request!!!"
        ];

        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if (!empty($user)) {
                $result = [];
                $employee_offices = TableRegistry::get('EmployeeOffices');
                $offices = $employee_offices->getEmployeeOffices($user['employee_record_id']);
                $result['status'] = 'success';
                $result['data'] = ['user' => $user, 'profile_photo' => $this->apiGetUserProfilePhoto($user['username'],
                    1), 'signature_photo' => $this->apiGetUserSignature($user['username'],
                    1), 'offices' => $offices];
            } else {
                $result = [
                    'status' => 'error',
                    'msg' => "Wrong username or password!!!"
                ];
            }
        }

        return $result;
    }

    public function apiGetEmployee()
    {

        $return = array(
            'status' => 'error',
            'msg' => "Invalid request!!!"
        );

        if ($this->request->is('post')) {
            $apikey = isset($this->request->data['api_key']) ? $this->request->data['api_key']
                : '';

            if (!empty($apikey) && ($apikey == API_KEY || $this->checkToken($apikey) == TRUE)) {

                $officeid = !empty($this->request->data['office_id']) ? intval($this->request->data['office_id'])
                    : 0;
                $officerid = !empty($this->request->data['officer_id']) ? intval($this->request->data['officer_id'])
                    : 0;
                $user_unit = !empty($this->request->data['office_unit_id']) ? intval($this->request->data['office_unit_id'])
                    : 0;
                $user_designation = !empty($this->request->data['user_designation']) ? intval($this->request->data['user_designation'])
                    : 0;

                $table_employee_office = TableRegistry::get('EmployeeOffices');

                try {
                    $query = $table_employee_office->find()->select([
                        "id" => 'EmployeeRecords.id',
                        'office_unit_id',
                        'office_unit_organogram_id',
                        'designation',
                        'employee_record_id',
                        'EmployeeOffices.office_id',
                        'office_name' => 'Offices.office_name_bng',
                        "unit_name_bng" => 'OfficeUnits.unit_name_bng',
                        "unit_name_eng" => 'OfficeUnits.unit_name_eng',
                        "personal_mobile" => 'EmployeeRecords.personal_mobile',
                        "personal_email" => 'EmployeeRecords.personal_email',
                        "name_bng" => 'EmployeeRecords.name_bng',
                        "name_eng" => 'EmployeeRecords.name_eng',
                        "identity_no" => 'EmployeeRecords.identity_no',
                        "is_cadre" => 'EmployeeRecords.is_cadre',
                        "username" => 'Users.username',
                        "role_id" => "Users.id",
                        "is_admin" => "OfficeUnitOrganograms.is_admin",
                        'designation_name_bng' => 'OfficeUnitOrganograms.designation_bng',
                        'designation_name_eng' => 'OfficeUnitOrganograms.designation_eng'
                    ])->join([
                        'OfficeUnitOrganograms' => [
                            'table' => 'office_unit_organograms',
                            'type' => 'left',
                            'conditions' => 'EmployeeOffices.office_unit_organogram_id = OfficeUnitOrganograms.id'
                        ],
                        'Offices' => [
                            'table' => 'offices',
                            'type' => 'INNER',
                            'conditions' => 'EmployeeOffices.office_id = Offices.id'
                        ],
                        'OfficeUnits' => [
                            'table' => 'office_units',
                            'type' => 'INNER',
                            'conditions' => 'EmployeeOffices.office_unit_id = OfficeUnits.id'
                        ],
                        'EmployeeRecords' => [
                            'table' => 'employee_records',
                            'type' => 'inner',
                            'conditions' => 'EmployeeRecords.id = EmployeeOffices.employee_record_id'
                        ],
                        'Users' => [
                            'table' => 'users',
                            'type' => 'inner',
                            'conditions' => 'Users.employee_record_id = EmployeeOffices.employee_record_id'
                        ]
                    ])->where(['EmployeeOffices.status' => 1]);

                    if (!empty($officeid)) {
                        $query = $query->where(['EmployeeOffices.office_id' => $officeid]);
                    }

                    if (!empty($officerid)) {
                        $query = $query->where(['EmployeeOffices.employee_record_id' => $officerid]);
                    }

                    if (!empty($user_unit)) {
                        $query = $query->where(['EmployeeOffices.office_unit_id' => $user_unit]);
                    }

                    if (!empty($user_designation)) {
                        $query = $query->where(['EmployeeOffices.office_unit_organogram_id' => $user_designation]);
                    }

                    if (!empty($group)) {
                        $query = $query->group([$group]);
                    }
                    $results = $query->order(['EmployeeOffices.designation_level ASC, EmployeeOffices.designation_sequence ASC'])->toArray();

                    if (!empty($results)) {
                        $return = array(
                            'status' => 'success',
                            'data' => $results
                        );
                    } else {
                        $return = array(
                            'status' => 'error',
                            'msg' => "Invalid request! "
                        );
                    }
                } catch (\Exception $ex) {
                    $return = array(
                        'status' => 'error',
                        'msg' => "Invalid request! " . $ex->getMessage()
                    );
                }
            } else {
                $return = array(
                    'status' => 'error',
                    'msg' => "দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না"
                );
            }
        }

        $this->response->body(json_encode($return));
        $this->response->type('application/json');
        return $this->response;
    }
    
    public function apiGetUnitEmployee()
    {

        $return = array(
            'status' => 'error',
            'msg' => "Invalid request!!!"
        );

        if ($this->request->is('post')) {

            $office_id = !empty($this->request->data['office_id']) ? intval($this->request->data['office_id'])
                : 0;

            $table_employee_office = TableRegistry::get('EmployeeOffices');

            try {
                $table_instance_unit = TableRegistry::get('OfficeUnits');
                $table_instance_unit_org = TableRegistry::get('OfficeUnitOrganograms');
                $table_instance_emp_offices = TableRegistry::get('EmployeeOffices');
                $office_units = $table_instance_unit->find()->where(['office_id' => $office_id, 'active_status' => 1])->order(['parent_unit_id asc', 'id asc'])->toArray();
                $active_designation_rows = $table_instance_emp_offices->find()->select(['office_unit_organogram_id', 'identification_number', "office_id", 'office_unit_id', 'employee_record_id', 'EmployeeOffices.id', 'EmployeeRecords.name_bng'])->join([
                    'EmployeeRecords' => [
                        'table' => 'employee_records',
                        'type' => 'inner',
                        'conditions' => 'EmployeeRecords.id = EmployeeOffices.employee_record_id'
                    ]
                ])->where(['EmployeeOffices.office_id' => $office_id, 'EmployeeOffices.status' => 1])->order(['designation_level ASC, designation_sequence ASC']);

                if (!empty($active_designation_rows)) {
                    foreach ($active_designation_rows as $active) {
                        $active_designations[] = $active->office_unit_organogram_id;
                        $identification_number[] = $active->identification_number;
                        $employee_record_id[] = $active->employee_record_id;
                        $employee_office_id[] = $active->id;
                        $employee_unit_id[] = $active->office_unit_id;
                        $employee_record_name[] = $active['EmployeeRecords']['name_bng'];
                    }
                }

                foreach ($office_units as $office_unit) {
                    $org = $table_instance_unit_org->find()->where(['office_unit_id' => $office_unit['id'], 'status' => 1])->order(['designation_level asc'])->toArray();
                    $row = array();
                    $row['office_unit'] = $office_unit;
                    if (count($org) > 0) {
                        foreach ($org as $org_row) {
                            $org_row['active_status'] = 0;
                            $key = array_search($org_row['id'], $active_designations);
                            $org_row['identification_number'] = $identification_number[$key];
                            $org_row['employee_record_id'] = $employee_record_id[$key];
                            $org_row['employee_office_id'] = $employee_office_id[$key];
                            $org_row['employee_unit_id'] = $employee_unit_id[$key];
                            $org_row['office_unit_organogram_id'] = $active_designations[$key];
                            $org_row['office_employee_name'] = $employee_record_name[$key];
                        }
                    }
                    $row['designations'] = $org;
                    $data_items[] = $row;
                }

                if (!empty($data_items)) {
                    $return = array(
                        'status' => 'success',
                        'data' => $data_items
                    );
                } else {

                }
            } catch (\Exception $ex) {
                $return = array(
                    'status' => 'error',
                    'msg' => "Invalid request! " . $ex->getMessage()
                );
            }
        }

        $this->response->body(json_encode($return));
        $this->response->type('application/json');
        return $this->response;
    }

    public function apiGetUserDetailInformation()
    {
        $api_key = '';
        $result = [
            'status' => 'error',
            'msg' => "দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না"
        ];
        if ($this->request->is('post')) {
            if (!empty($this->request->data['api_key'])) {
                $api_key = $this->request->data['api_key'];

                if ($api_key == API_KEY) {

                    if (!empty($this->request->data['employee_id'])) {
                        $employee_id = $this->request->data['employee_id'];

                        $employee_offices = TableRegistry::get('EmployeeOffices');
                        $offices = TableRegistry::get('Offices');

                        $query = $employee_offices->find()->select(
                            ['employee_record_id', 'identification_number',
                                'office_id', 'office_unit_id', 'office_unit_organogram_id',
                                'designation',
                                'designation_level', 'designation_sequence',
                                'incharge_label', 'joining_date', 'last_office_date',
                                'office_name_eng' => 'Offices.office_name_eng',
                                'office_name_bng' => 'Offices.office_name_bng',
                                'unit_name_bng' => 'OfficeUnits.unit_name_bng',
                                'unit_name_eng' => 'OfficeUnits.unit_name_eng',
                                'designation_eng' => 'OfficeUnitOrganograms.designation_eng',
                                'designation_bng' => 'OfficeUnitOrganograms.designation_bng',
                                'short_name_eng' => 'OfficeUnitOrganograms.short_name_eng',
                                'short_name_bng' => 'OfficeUnitOrganograms.short_name_bng'
                            ])->join([
                            'Offices' => [
                                'table' => 'offices',
                                'type' => 'inner',
                                'conditions' => 'EmployeeOffices.office_id  =   Offices.id'
                            ],
                            'OfficeUnits' => [
                                'table' => 'office_units',
                                'type' => 'inner',
                                'conditions' => 'EmployeeOffices.office_unit_id  =   OfficeUnits.id'
                            ],
                            'OfficeUnitOrganograms' => [
                                'table' => 'office_unit_organograms',
                                'type' => 'inner',
                                'conditions' => 'EmployeeOffices.office_unit_organogram_id  =   OfficeUnitOrganograms.id'
                            ],
                        ])->where(['EmployeeOffices.id' => $employee_id, 'EmployeeOffices.status' => 1])->toArray();

                        if (!empty($query)) {
                            $result = [
                                'status' => 'success',
                                'data' => $query
                            ];
                        }
                    }
                }
            }
        }
        $this->response->body(json_encode($result));
        $this->response->type('application/json');
        return $this->response;
    }

    public function apiGetUnitwiseDesignation()
    {
        $result = [
            'status' => 'error',
            'msg' => "দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না"
        ];

        $api_key = '';
        if (!empty($this->request->query['api_key'])) {
            $api_key = $this->request->query['api_key'];

            if ($api_key == API_KEY) {
                if (!empty($this->request->query['office_id'])) {
                    $office_id = $this->request->query['office_id'];
                    $unit_id = !empty($this->request->query['unit_id']) ? $this->request->query['unit_id']
                        : 0;

                    $table = TableRegistry::get('OfficeUnits');
                    $query = $table->getUnitWiseDesignation($office_id, $unit_id)->toArray();

                    if (!empty($query)) {
                        $result = [
                            'status' => 'success',
                            'data' => $query
                        ];
                    }
                }
            }
        }

        $this->response->body(json_encode($result));
        $this->response->type('application/json');
        return $this->response;
    }

    public function apiPostUnitwiseDesignation()
    {
        $result = [
            'status' => 'error',
            'msg' => "দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না"
        ];
        if ($this->request->is('post')) {
            $api_key = '';
            if (!empty($this->request->data['api_key'])) {
                $api_key = $this->request->data['api_key'];

                if ($api_key == API_KEY) {
                    if (!empty($this->request->data['office_id'])) {
                        $office_id = $this->request->data['office_id'];
                        $unit_id = !empty($this->request->data['unit_id']) ? $this->request->data['unit_id']
                            : 0;

                        $table = TableRegistry::get('OfficeUnits');
                        $query = $table->getUnitWiseDesignation($office_id,
                            $unit_id)->toArray();

                        if (!empty($query)) {
                            $result = [
                                'status' => 'success',
                                'data' => $query
                            ];
                        }
                    }
                }
            }
        }
        $this->response->body(json_encode($result));
        $this->response->type('application/json');
        return $this->response;
    }

    public function apiGetUnitInfo()
    {
        $result = [
            'status' => 'error',
            'msg' => "দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না"
        ];
        if ($this->request->is('post')) {
            $api_key = '';

            if (!empty($this->request->data['api_key'])) {
                $api_key = $this->request->data['api_key'];

                if ($api_key == API_KEY) {
                    if (!empty($this->request->data['unit_id'])) {
                        $unit_id = $this->request->data['unit_id'];

                        $table = TableRegistry::get('OfficeUnits');
                        $query = $table->find()->where(['id' => $unit_id])->first();

                        if (!empty($query)) {
                            $result = [
                                'status' => 'success',
                                'data' => $query
                            ];
                        }
                    }
                }
            }
        }
        $this->response->body(json_encode($result));
        $this->response->type('application/json');
        return $this->response;
    }

    public function apiGetOfficeInfo()
    {
        $api_key = '';
        $result = [
            'status' => 'error',
            'msg' => "দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না"
        ];
        if ($this->request->is('post')) {

            if (!empty($this->request->data['api_key'])) {
                $api_key = $this->request->data['api_key'];

                if ($api_key == API_KEY) {
                    if (!empty($this->request->data['office_id'])) {
                        $office_id = $this->request->data['office_id'];

                        $table = TableRegistry::get('Offices');
                        try {
                            $query = $table->find()->where(['id' => $office_id])->first();

                            if (!empty($query)) {
                                $result = [
                                    'status' => 'success',
                                    'data' => $query
                                ];
                            } else {
                                $result = [
                                    'status' => 'error',
                                    'msg' => "দুঃখিত! অফিসের তথ্য পাওয়া যায়নি"
                                ];
                            }
                        } catch (Exception $ex) {
                            $result = [
                                'status' => 'error',
                                'msg' => "দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না"
                            ];
                        }
                    }
                }
            }
        }
        $this->response->body(json_encode($result));
        $this->response->type('application/json');
        return $this->response;
    }

    public function apiGetDesignationInfo()
    {
        $api_key = '';
        $result = [
            'status' => 'error',
            'msg' => "দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না"
        ];
        if ($this->request->is('post')) {
            if (!empty($this->request->data['api_key'])) {
                $api_key = $this->request->data['api_key'];

                if ($api_key == API_KEY) {
                    if (!empty($this->request->data['designation_id'])) {
                        $designation_id = $this->request->data['designation_id'];

                        $table = TableRegistry::get('OfficeUnitOrganograms');
                        $query = $table->find()->where(['id' => $designation_id])->first();

                        if (!empty($query)) {
                            $result = [
                                'status' => 'success',
                                'data' => $query
                            ];
                        }
                    }
                }
            }
        }
        $this->response->body(json_encode($result));
        $this->response->type('application/json');
        return $this->response;
    }

    public function apiGetOfficeTree()
    {
        $api_key = '';
        $result = [
            'status' => 'error',
            'msg' => "দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না"
        ];
        if ($this->request->is('post')) {
            if (!empty($this->request->data['api_key'])) {
                $api_key = $this->request->data['api_key'];

                if ($api_key == API_KEY) {
                    if (!empty($this->request->data['office_id'])) {
                        $office_id = $this->request->data['office_id'];

                        $offices = TableRegistry::get('Offices');
                        $office_units = TableRegistry::get('OfficeUnits');
                        $office_unit_organograms = TableRegistry::get('OfficeUnitOrganograms');
                        $query = $offices->find()->select(
                            ['id', 'office_ministry_id', 'office_layer_id',
                                'office_origin_id', 'office_name_eng', 'office_name_bng',
                                'geo_division_id', 'geo_district_id', 'geo_upazila_id',
                                'geo_union_id', 'office_address', 'digital_nothi_code',
                                'reference_code', 'office_code', 'office_phone',
                                'office_mobile', 'office_fax', 'office_email',
                                'office_web', 'parent_office_id',
                                'office_unit_id' => 'OfficeUnits.id', 'unit_name_bng' => 'OfficeUnits.unit_name_bng',
                                'unit_name_eng' => 'OfficeUnits.unit_name_eng',
                                'parent_unit_id' => 'OfficeUnits.parent_unit_id',
                            ])->join([
                            'OfficeUnits' => [
                                'table' => 'office_units',
                                'type' => 'inner',
                                'conditions' => 'OfficeUnits.office_id  =   Offices.id'
                            ],
                        ])->where(['Offices.id' => $office_id, 'Offices.active_status' => 1])->toArray();

                        if (!empty($query)) {
                            foreach ($query as $key => &$val) {
                                $ans = $office_unit_organograms->find()
                                    ->select(['office_unit_organograms_id' => 'id',
                                        'designation_eng', 'designation_bng',
                                        'short_name_eng', 'short_name_bng'])
                                    ->where(['OfficeUnitOrganograms.office_unit_id' => $val['office_unit_id'],
                                        'OfficeUnitOrganograms.office_id' => $val['id']])->toArray();

                                $val['OfficeUnitOrganograms'] = $ans;
                            }
                        }


                        if (!empty($query)) {
                            $result = [
                                'status' => 'success',
                                'data' => $query
                            ];
                        }
                    }
                }
            }
        }

        $this->response->body(json_encode($result));
        $this->response->type('application/json');
        return $this->response;
    }

    /*     * New API By KAT - End* */

    public function apiGetOfficeFronDesk()
    {
        $api_key = '';
        $result = [
            'status' => 'error',
            'msg' => "দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না"
        ];
        if ($this->request->is('post')) {
            if (!empty($this->request->data['api_key'])) {
                $api_key = $this->request->data['api_key'];
            }else{
                $api_key = '';
            }
                $is_allowed = false; 
                if($api_key == API_KEY || !empty($this->Auth->user())){
                    $is_allowed = true;
                }
                if ($is_allowed == true) {
                    if (!empty($this->request->data['office_id'])) {
                        $office_id = intval($this->request->data['office_id']);

                        $table = TableRegistry::get('OfficeFrontDesk');
                        $table_ofc = TableRegistry::get('Offices');
                        try {
                            $query = $table->find()->where(['office_id' => $office_id])->first();
                            if (!empty($query)) {
                                $result = [
                                    'status' => 'success',
                                    'data' => $query
                                ];
                            } else {
                                $query = $table_ofc->find()->select(['office_name_bng',
                                    'office_email'])->where(['id' => $office_id])->first();
                                if (!empty($query)) {
                                    $result = [
                                        'status' => 'success',
                                        'data' => $query
                                    ];
                                } else {
                                    $result = [
                                        'status' => 'error',
                                        'msg' => "দুঃখিত! অফিসের ফ্রন্ট ডেস্কের তথ্য পাওয়া যায়নি"
                                    ];
                                }
                            }
                        } catch (\Exception $ex) {
                            $result = [
                                'status' => 'error',
                                'msg' => "দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না"
                            ];
                        }
                    }
                }
        }
        $this->response->body(json_encode($result));
        $this->response->type('application/json');
        return $this->response;
    }

    public function apiGetOfflineOffices()
    {

        $this->layout = 'ajax';
        $office_table = TableRegistry::get('Offices');

        $return = array(
            'status' => 'error',
            'msg' => "দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না"
        );

        if ($this->request->is('post')) {

            $apikey = isset($this->request->data['api_key']) ? $this->request->data['api_key']
                : '';
            $active = isset($this->request->data['active']) ? $this->request->data['active']
                : 1;

            $conditions = array();

            if (isset($this->request->data['office_ministry_id']) && !empty($this->request->data['office_ministry_id'])) {
                if (is_string($this->request->data['office_ministry_id'])) {
                    $conditions[] = array(
                        'Offices.office_ministry_id IN' => explode(',',
                            $this->request->data['office_ministry_id'])
                    );
                } else {
                    $conditions[] = array(
                        'Offices.office_ministry_id' => intval($this->request->data['office_ministry_id'])
                    );
                }
            }

            if (isset($this->request->data['office_layer_id']) && !empty($this->request->data['office_layer_id'])) {
                if (is_string($this->request->data['office_layer_id'])) {
                    $conditions[] = array(
                        'office_layer_id IN' => explode(',',
                            $this->request->data['office_layer_id'])
                    );
                } else {
                    $conditions[] = array(
                        'office_layer_id' => intval($this->request->data['office_layer_id'])
                    );
                }
            }

            $officelayertype = '';
            $officeLayerTypeArray = json_decode(OFFICE_LAYER_TYPE, true);

            if (isset($this->request->data['office_layer_type']) && !empty($this->request->data['office_layer_type'])) {
                $officelayertype = $officeLayerTypeArray[intval($this->request->data['office_layer_type'])];
            }

            if (isset($this->request->data['office_origin_id']) && !empty($this->request->data['office_origin_id'])) {
                if (is_string($this->request->data['office_origin_id'])) {
                    $conditions[] = array(
                        'office_origin_id IN' => explode(',',
                            $this->request->data['office_origin_id'])
                    );
                } else {
                    $conditions[] = array(
                        'office_origin_id' => intval($this->request->data['office_origin_id'])
                    );
                }
            }

            if (isset($this->request->data['geo_division_id']) && !empty($this->request->data['geo_division_id'])) {
                if (is_string($this->request->data['geo_division_id'])) {
                    $conditions[] = array(
                        'geo_division_id IN' => explode(',',
                            $this->request->data['geo_division_id'])
                    );
                } else {
                    $conditions[] = array(
                        'geo_division_id' => intval($this->request->data['geo_division_id'])
                    );
                }
            }

            if (isset($this->request->data['geo_district_id']) && !empty($this->request->data['geo_district_id'])) {
                if (is_string($this->request->data['geo_district_id'])) {
                    $conditions[] = array(
                        'geo_district_id IN' => explode(',',
                            $this->request->data['geo_district_id'])
                    );
                } else {
                    $conditions[] = array(
                        'geo_district_id' => intval($this->request->data['geo_district_id'])
                    );
                }
            }

            if (isset($this->request->data['geo_upazila_id']) && !empty($this->request->data['geo_upazila_id'])) {
                if (is_string($this->request->data['geo_upazila_id'])) {
                    $conditions[] = array(
                        'geo_upazila_id IN' => explode(',',
                            $this->request->data['geo_upazila_id'])
                    );
                } else {
                    $conditions[] = array(
                        'geo_upazila_id' => intval($this->request->data['geo_upazila_id'])
                    );
                }
            }

            if (isset($this->request->data['geo_union_id']) && !empty($this->request->data['geo_union_id'])) {
                if (is_string($this->request->data['geo_union_id'])) {
                    $conditions[] = array(
                        'geo_union_id IN' => explode(',',
                            $this->request->data['geo_union_id'])
                    );
                } else {
                    $conditions[] = array(
                        'geo_union_id' => intval($this->request->data['geo_union_id'])
                    );
                }
            }
            try {

                if ($apikey == API_KEY) {
                    $officelist = $office_table->find();

                    if (!empty($conditions)) {
                        $officelist = $officelist->where($conditions);
                    }

                    if (!empty($officelayertype)) {
                        $officelist = $officelist->join([
                            'OfficeLayers' => [
                                'table' => 'office_layers',
                                'type' => 'inner',
                                'conditions' => "OfficeLayers.id = Offices.office_layer_id"
                            ]
                        ])->where(['OfficeLayers.layer_name_eng LIKE' => "%{$officelayertype}%"]);
                    }

                    if ($active == -1) {

                    } else {
                        $officelist->where(['Offices.active_status' => $active]);
                    }

                    $officelist->join([
                        'OfficeFrontDesk' => [
                            'table' => 'office_front_desk',
                            'type' => 'left',
                            'conditions' => 'OfficeFrontDesk.office_id = Offices.id'
                        ]
                    ])->where(['OfficeFrontDesk.id is null']);

                    $officelist->order(['Offices.parent_office_id asc, Offices.office_name_bng asc'])->toArray();

                    $return = array("status" => 'success', 'data' => $officelist);
                } else {
                    $return = array(
                        'status' => 'error',
                        'msg' => "দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না"
                    );
                }
            } catch (\Exception $ex) {
                $return = array(
                    'status' => 'error',
                    'msg' => "দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না" . $ex->getMessage()
                );
            }
        }

        $this->response->body(json_encode($return));
        $this->response->type('application/json');
        return $this->response;
    }

    /**
     * @param api_key ,ministry_id,layer_id,origin_id,office_id,date_start,date_end,service_name,receipt_no
     * @return json
     */
    public function apiServiceReport()
    {
        ini_set('memory_limit', -1);
        set_time_limit(0);
        $this->layout = 'ajax';
        $office_table = TableRegistry::get('Offices');

        $return = array(
            'status' => 'error',
            'msg' => "দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না"
        );

        if ($this->request->is('post')) {

            $apikey = isset($this->request->data['api_key']) ? $this->request->data['api_key']
                : '';
            if ($apikey == API_KEY) {
                $ministry_id = isset($this->request->data['ministry_id']) ? $this->request->data['ministry_id']
                    : '';
                $layer_id = isset($this->request->data['layer_id']) ? $this->request->data['layer_id']
                    : '';
                $origin_id = isset($this->request->data['origin_id']) ? $this->request->data['origin_id']
                    : '';
                $office_id = isset($this->request->data['office_id']) ? $this->request->data['office_id']
                    : '';
                $date_start = isset($this->request->data['date_start']) ? $this->request->data['date_start']
                    : '';
                $date_end = isset($this->request->data['date_end']) ? $this->request->data['date_end']
                    : '';
                $service_name = isset($this->request->data['service_name']) ? strtolower($this->request->data['service_name'])
                    : '';
                $receipt_no = isset($this->request->data['receipt_no']) ? $this->request->data['receipt_no']
                    : '';

                $DakTracksTable = TableRegistry::get('DakTracks');
                $OfficesTable = TableRegistry::get('Offices');

                $officeQuery = $OfficesTable->find('list',
                    ['keyField' => 'id', 'valueField' => 'office_name_bng']);
                $query = $DakTracksTable->find()->select(['dak_id', 'dak_type',
                    'receiving_office_id']);
                $f = 0;
                if (!empty($ministry_id)) {
                    $officeQuery = $officeQuery->where(['office_ministry_id' => $ministry_id]);
                    $f = 1;
                }
                if (!empty($layer_id)) {
                    $officeQuery = $officeQuery->where(['office_layer_id' => $layer_id]);
                    $f = 1;
                }
                if (!empty($origin_id)) {
                    $officeQuery = $officeQuery->where(['office_origin_id' => $origin_id]);
                    $f = 1;
                }
                if (!empty($office_id)) {
                    $officeQuery = $officeQuery->where(['id' => $office_id]);
                    $f = 1;
                }
                if ($f == 1) {
                    $officeQuery = $officeQuery->toArray();
                }
                if ($f == 1) {
                    $query = $query->where(['receiving_office_id IN' => array_keys($officeQuery)]);
                }
                if (!empty($date_start)) {
                    $query = $query->where(['date(receiving_date) >=' => $date_start]);
                }
                if (!empty($date_end)) {
                    $query = $query->where(['date(receiving_date) <=' => $date_end]);
                }
                if (!empty($service_name)) {
                    $query = $query->where(['service_name' => $service_name]);
                }
                if (!empty($receipt_no)) {
                    $query = $query->where(['dak_received_no IN' => $receipt_no]);
                }
//               pr($query);die;
                $allDaks = $query->order(['receiving_office_id'])->toArray();
//                pr($allDaks);die;
                if ($f == 1) {
                    $data_result = $this->calculateServiceReport($allDaks,
                        $officeQuery);
                } else {
                    $data_result = $this->calculateServiceReport($allDaks);
                }

                $return = array(
                    'status' => 'success',
                    'msg' => $data_result
                );
            }
        }
        $this->response->body(json_encode($return));
        $this->response->type('application/json');
        return $this->response;
    }

    protected function calculateServiceReport($allDaks, $officeQuery = '')
    {
        $count = 0;
        $officeID = 0;
        $nothivukto = 0;
        $nothijat = 0;
        $pending = 0;
        $nisponno = 0;
        $total = 0;
        $total_nisponno = 0;
        $total_pending = 0;
        $data = [];
        $current_office_id = 0;
        $officesTable = TableRegistry::get('Offices');
        if (!empty($allDaks)) {
            foreach ($allDaks as $key => $val) {
                try {
                    if (!empty($val['receiving_office_id'])) {
                        $current_office_id = $val['receiving_office_id'];
                        if ($val['receiving_office_id'] != $officeID) {

                            $this->switchOffice($val['receiving_office_id'],
                                'ReportDB');
                            $count = 0;
                            $nothivukto = 0;
                            $nothijat = 0;
                            $pending = 0;
                            $nisponno = 0;
                            $officeID = $val['receiving_office_id'];
                        }
                        TableRegistry::remove('DakUsers');
                        $DakUsersTable = TableRegistry::get('DakUsers');
                        $dak_info = $DakUsersTable->find()->select('dak_category')->where(['dak_id' => $val['dak_id'],
                            'dak_type' => $val['dak_type'], 'to_office_id' => $val['receiving_office_id']])->order(['modified desc'])->first();
                        if (!empty($dak_info)) {
                            if ($dak_info['dak_category'] == 'NothiJato') {
                                $nothijat++;
                                $nisponno++;
                                $total_nisponno++;
                            } else if ($dak_info['dak_category'] == 'NothiVukto') {
                                $nothivukto++;
                                $nisponno++;
                                $total_nisponno++;
                            } else {
                                $pending++;
                                $total_pending++;
                            }
                        }
                        $count++;
                        if ($officeID != 0) {
                            if (empty($officeQuery)) {
                                $name = $officesTable->getBanglaName($officeID);
                                $name = $name['office_name_bng'];
                            } else {
                                $name = $officeQuery[$officeID];
                            }
                            $data[$officeID] = [
                                'id' => $officeID,
                                'name' => $name,
                                'total' => $count,
                                'nothijat' => $nothijat,
                                'nothivukto' => $nothivukto,
                                'pending' => $pending,
                                'nisponno' => $nisponno,
                            ];
                        }
                    }
                    $total++;
                } catch (\Exception $ex) {
                    continue;
                }
            }
        }
        return ['total' => $total, 'pending' => $total_pending, 'nisponno' => $total_nisponno,
            'data' => $data];
    }

    public function apiGetMultipleOfficesFrontDesk()
    {
        $api_key = '';
        $total = 0;
        $data = [
            'total' => $total,
            'data' => ''
        ];
        $result = [];
        $found = '';
        $notFound = '';
        if ($this->request->is('post')) {
            if (!empty($this->request->data['api_key'])) {
                $api_key = $this->request->data['api_key'];
                $table_ofc = TableRegistry::get('Offices');
                if ($api_key == API_KEY) {
                    if (!empty($this->request->data['office_ministry_id'])) {
                        $ministry_id = explode(',', $this->request->data['office_ministry_id']);
                        $all_offices = $table_ofc->find()->select(['id'])->where(['office_ministry_id IN' => $ministry_id, 'active_status' => 1])->toArray();
                        $office_list = '';
                        $i = 0;
                        if (!empty($all_offices)) {
                            foreach ($all_offices as $val) {
                                if ($i != 0) {
                                    $office_list .= ',' . $val['id'];
                                } else {
                                    $office_list .= $val['id'];
                                }
                                $i++;
                            }
                        }
                        $this->request->data['offices_id'] = $office_list;
                    }
                    if (!empty($this->request->data['offices_id'])) {
                        $offices_id = explode(',', $this->request->data['offices_id']);

                        $table = TableRegistry::get('OfficeFrontDesk');

                        try {

                            if (!empty($offices_id)) {
                                foreach ($offices_id as $office_id) {
                                    $total++;
                                    $query = $table->find()->where(['office_id' => $office_id])->first();
                                    if (!empty($query)) {
                                        $result[] = [
                                            'status' => 'success',
                                            'data' => $query
                                        ];
                                        $found .= $query['office_name'] . ', ';
                                    } else {
                                        $query = $table_ofc->find()->select(['office_name_bng',
                                            'office_email'])->where(['id' => $office_id])->first();
                                        if (!empty($query)) {
                                            $result[] = [
                                                'status' => 'error',
                                                'msg' => $query['office_name_bng'] . ' অফিসের ফ্রন্ট ডেস্কের তথ্য পাওয়া যায়নি। মেল আইডিঃ  ' . $query['office_email']
                                            ];
                                            $notFound .= $query['office_name_bng'] . ', ';
                                        } else {
                                            $result[] = [
                                                'status' => 'error',
                                                'msg' => "দুঃখিত! অফিসের ফ্রন্ট ডেস্কের তথ্য পাওয়া যায়নি"
                                            ];
                                        }
                                    }
                                }
                            }
                        } catch (\Exception $ex) {
                            $result[] = [
                                'status' => 'error',
                                'msg' => "দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না"
                            ];
                        }
                    }
                }
            }
        }
        if (!empty($result)) {
            $data = [
                'total' => $total,
                'data' => ['found' => $found, 'notFound' => $notFound]
            ];
        }
        $this->response->body(json_encode($data));
        $this->response->type('application/json');
        return $this->response;
    }


    public function apiGetOfficeOriginTree()
    {
        $api_key = '';
        $result = [
            'status' => 'error',
            'msg' => "দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না"
        ];
        if ($this->request->is('post')) {
            if (!empty($this->request->data['api_key'])) {
                $api_key = $this->request->data['api_key'];

                if ($api_key == API_KEY) {
                    if(!empty($this->request->data['office_domain'])){
                        $officeDomainTable = TableRegistry::get('OfficeDomains');
                        $domain = str_replace('http://', '',$this->request->data['office_domain']);
                        $domain = str_replace('https://', '',$domain);
                        $domain = str_replace('www.', '',$domain);
                        $domain = str_replace('.gov.bd', '',$domain);
                        $officeDomain = $officeDomainTable->find()->select(['office_id'])->where(['domain_prefix'=>$domain])->first();
                        if(!empty($officeDomain)){
                            $officesTable = TableRegistry::get('Offices');
                            $offices = $officesTable->get($officeDomain['office_id']);
                            $this->request->data['office_origin_id'] = $offices['office_origin_id'];
                        }
                    }
                    if (!empty($this->request->data['office_origin_id'])) {
                        $office_id = $this->request->data['office_origin_id'];

                        $offices = TableRegistry::get('OfficeOrigins');
                        $officeInfo = $offices->get($office_id);
                        $officeUnitOrigins = TableRegistry::get('OfficeOriginUnits');
                        $query = $officeUnitOrigins->find()->hydrate(false)->select(['id','unit_name_bng','unit_name_eng','parent_unit_id','unit_level'])->where(['OfficeOriginUnits.office_origin_id' => $office_id, 'OfficeOriginUnits.active_status' => 1])->order(['OfficeOriginUnits.parent_unit_id asc'])->toArray();
                        $return['office'] = [
                            'id' => $officeInfo['id'],
                            'name' => $officeInfo['office_name_bng'],
                            'office_ministry_id' => $officeInfo['office_ministry_id'],
                            'office_layer_id' => $officeInfo['office_layer_id'],
                        ];
                        $return['units'] = $this->childTree($query);

                        if (!empty($query)) {
                            $result = [
                                'status' => 'success',
                                'data' => $return
                            ];
                        }
                    }
                }
            }
        }

        $this->response->body(json_encode($result));
        $this->response->type('application/json');
        return $this->response;
    }

    private function childTree($data, $parent = 0)
    {
        set_time_limit(0);
        $officeUnitOrganogramOrigins = TableRegistry::get('OfficeOriginUnitOrganograms');
        $return = [];
        foreach ($data as $key=>$value){
            $value['designations'] = $officeUnitOrganogramOrigins->find()->hydrate(false)->select(['id','office_origin_unit_id','designation_eng','designation_bng','short_name_eng','short_name_bng','designation_level'])->where(['office_origin_unit_id' => $value['id'], 'status' => 1])->order(['designation_level asc','designation_sequence asc'])->toArray();
            if($value['parent_unit_id'] == $parent){
                $child = $this->childTree($data, $value['id']) ;
                if(!empty($child)) {
                    $value['child_units'] = $child;
                }
                $return[] = $value;
            }
        }
        return $return;
    }
      private function apiUserLoginService()
    {
        $jsonArray = array(
            "status" => "error",
            "error_code" => 404,
            "message" => "Invalid Request"
//            "message" => " নথি অs্যাপ্লিকেশন উন্নতিকরণের কাজ চলছে। সাময়িক অসুবিধার জন্য আমরা দুঃখিত।"
        );
           // maintenance
        if(date('Y-m-d') == '2017-10-01' && date('G') <= 10 && date('G') >= 7 && Live == 1){
           $jsonArray['message'] =  " নথি অ্যাপ্লিকেশন উন্নতিকরণের কাজ চলছে। সাময়িক অসুবিধার জন্য আমরা দুঃখিত।";
          goto rtn;
        }
         // maintenance
         $data = [];

        if ($this->request->is('post')) {
            $this->request->data['username'] = !empty($this->request->data['username'])
                ? trim($this->request->data['username']) : '';

            $bn_digits = array('০', '১', '২', '৩', '৪', '৫', '৬', '৭', '৮', '৯');
            $output = str_replace($bn_digits, range(0, 9),
                $this->request->data['username']);

            $this->request->data['username'] = $output;

            $user = $this->Auth->identify();
            if (empty($user) && !empty($this->request->data['alias'])) {
                $user = $this->Users->find()->where(['user_alias' => $this->request->data['alias']])->hydrate(false)->first();
                if (!empty($user)) {
                    $this->request->data['username'] = $user['username'];
                }
                $user = $this->Auth->identify();
            }


            if (strlen($this->request->data['username']) <= 8) {
                $jsonArray = array(
                    "status" => "error",
                    "error_code" => 404,
                    "message" => "দুঃখিত! লগইন আইডি অথবা পাসওয়ার্ড সঠিক নয়"
                );
                echo json_encode($jsonArray);
                die;
            }


            $employee_records = [];

            if ($user) {
                $this->Auth->setUser($user);

                if ($this->request->data['password'] == DEFAULT_PASSWORD) {

                    $uservalue = $this->Users->get($user['id']);
                    $uservalue->force_password_change = 1;
                    $this->Users->save($uservalue);

                     $jsonArray['message'] = 'দুঃখিত! দয়া করে ওয়েব থেকে আপনার পাসওয়ার্ড পরিবর্তন করুন। ';
                    goto rtn;
                }
                if ($user['user_role_id'] > 2) {

                    $table_instance_emp_records = TableRegistry::get('EmployeeRecords');
                    $table_instance_emp_offices = TableRegistry::get('EmployeeOffices');
                    $table_instance_geo_division = TableRegistry::get('GeoDivisions');
                    $table_instance_geo_districts = TableRegistry::get('GeoDistricts');
                    $table_instance_geo_upazilas = TableRegistry::get('GeoUpazilas');
                    $table_instance_geo_unions = TableRegistry::get('GeoUnions');

                    $employee_records = $table_instance_emp_records->find()->select([
                        'name_bng', 'personal_mobile', 'personal_email', 'id','nid'
                    ])->where(['id' => $user['employee_record_id']])->first();

                    $employee_working_info = $table_instance_emp_offices->getEmployeeOfficeRecords($user['employee_record_id']);
                    $ind = 0;
                    if(!empty($employee_working_info)){
                        foreach($employee_working_info as $emp_info){
                           $data['working_info'][$ind]['office_id'] = $emp_info['office_id'];
                           $data['working_info'][$ind]['office_name'] = $emp_info['office_name'];

                           $data['working_info'][$ind]['office_unit_id'] = $emp_info['office_unit_id'];
                           $data['working_info'][$ind]['unit_name'] = $emp_info['unit_name'];
                           
                           $data['working_info'][$ind]['office_unit_organogram_id'] = $emp_info['office_unit_organogram_id'];
                           $data['working_info'][$ind]['designation'] = $emp_info['designation'];
                           $data['working_info'][$ind]['designation_level'] = $emp_info['designation_level'];
                           $data['working_info'][$ind]['incharge_label'] = $emp_info['incharge_label'];

                           $data['working_info'][$ind]['office_head'] = $emp_info['office_head'];

                           $division_info =$table_instance_geo_division->getBanglaNameandBBSCode($emp_info['division_id']);
                           $data['working_info'][$ind]['division_id'] = $emp_info['division_id'];
                           $data['working_info'][$ind]['division_name'] = $division_info['division_name_bng'];
                           $data['working_info'][$ind]['division_bbs'] = $division_info['bbs_code'];

                           $district_info =$table_instance_geo_districts->getBanglaNameandBBSCode($emp_info['district_id']);
                           $data['working_info'][$ind]['district_id'] = $emp_info['district_id'];
                           $data['working_info'][$ind]['district_name'] = $district_info['district_name_bng'];
                           $data['working_info'][$ind]['district_bbs'] = $district_info['bbs_code'];

                           $upazila_info =$table_instance_geo_unions->getBanglaNameandBBSCode($emp_info['upazila_id']);
                           $data['working_info'][$ind]['upazila_id'] = $emp_info['upazila_id'];
                           $data['working_info'][$ind]['upazila_name'] = $upazila_info['upazila_name_bng'];
                           $data['working_info'][$ind]['upazila_bbs'] = $upazila_info['bbs_code'];

                           $union_info =$table_instance_geo_upazilas->getBanglaNameandBBSCode($emp_info['union_id']);
                           $data['working_info'][$ind]['union_id'] = $emp_info['union_id'];
                           $data['working_info'][$ind]['union_name'] = $union_info['union_name_bng'];
                           $data['working_info'][$ind]['union_bbs'] = $union_info['bbs_code'];
                           $ind++;
                        }
                    }
                }
                $data['user_info']['username'] = $user['username'];
                $data['user_info']['user_alias'] = $user['user_alias'];
                $data['user_info']['user_role_id'] = $user['user_role_id'];
                $data['user_info']['is_admin'] = 0;
                $data['user_info']['employee_record_id'] = $user['employee_record_id'];
                $data['user_info']['name_bng'] = $employee_records['name_bng'];
                $data['user_info']['personal_mobile'] = $employee_records['personal_mobile'];
                $data['user_info']['personal_email'] = $employee_records['personal_email'];
                $data['user_info']['nid'] = $employee_records['nid'];
//                $data['working_info'] = $employee_working_info;
                $jsonArray = array(
                    "status" => "success",
                    "data" =>$data,
                );
            } else
                $jsonArray = array(
                    "status" => "error",
                    "error_code" => 404,
                    "message" => 'দুঃখিত! লগইন আইডি অথবা পাসওয়ার্ড সঠিক নয়'
                );
        }
        rtn:
        return $jsonArray;
    }
    public function changeUserPassword(){
        $response = array(
            "status" => "error",
            "error_code" => 401,
            "message" => "অনুমতি প্রাপ্ত নয়।"
        );

        $token = !empty($this->request->data['token']) ? trim($this->request->data['token']) : '';

        $usersTable = TableRegistry::get('Users');

        if ($this->checkAuthorization($token)) {
            if ($this->request->is('post')) {
                $username = !empty($this->request->data['username']) ? trim($this->request->data['username']) : '';
                $alias = !empty($this->request->data['alias']) ? trim($this->request->data['alias']) : '';
                $current_password = !empty($this->request->data['current_password']) ? trim($this->request->data['current_password']) : '';
                $new_password = !empty($this->request->data['new_password']) ? trim($this->request->data['new_password']) : '';

                $this->request->data['password'] =$current_password;
                //Get User Info
                $user = $this->Auth->identify();
                if (empty($user) && !empty($alias)) {
                    $user = $usersTable->find()->where(['user_alias' => $alias])->hydrate(false)->first();
                    if (!empty($user)) {
                        $this->request->data['username'] = $user['username'];
                    }
                    $user = $this->Auth->identify();
                }
                if(empty($user)){
                    $response['message'] = 'ইউজারের কোন তথ্য পাওয়া যায়নি।';
                    $response['error_code'] = 403;
                    goto rtn;
                }else{
                    $user = $usersTable->get($user['id']);
                }
                //password validation check
                if($usersTable->passwordValidation($new_password) === true){
                    $user->password = $new_password;
                    if($usersTable->save($user)){
                        $response = array(
                            "status" => "success",
                            "message" => "পাসওয়ার্ড হালনাগাদ হয়েছে।"
                        );
                    }

                }else{
                    $response['message'] = 'পাসওয়ার্ড নূন্যতম ৬ অক্ষরের হতে হবে। অন্তত একটি A-Z অথবা a-z থাকতে হবে।';
                    $response['error_code'] = 403;
                    goto rtn;
                }
            }
        }else{
            $response['message'] = 'ভুল টোকেন';
            $response['error_code'] = 403;
        }

        rtn:
        $this->response->type('json');
        $this->response->body(json_encode($response));
        return $this->response;
    }
    public function resetUserPassword(){
        $response = array(
            "status" => "error",
            "error_code" => 401,
            "message" => "অনুমতি প্রাপ্ত নয়।"
        );
        $token = !empty($this->request->data['token']) ? trim($this->request->data['token']) : '';
        if ($this->checkAuthorization($token)) {
            if ($this->request->is('post')) {
                $username = !empty($this->request->data['username']) ? trim($this->request->data['username']) : '';
                $alias = !empty($this->request->data['alias']) ? trim($this->request->data['alias']) : '';
                $email = !empty($this->request->data['email']) ? trim($this->request->data['email']) : '';
                $new_password = !empty($this->request->data['new_password']) ? trim($this->request->data['new_password']) : '';

                $usersTable = TableRegistry::get('Users');
                $employeeRecordsTable = TableRegistry::get('EmployeeRecords');
                $conditions = [];
                if (!empty($alias)) {
                    $conditions['user_alias'] =$alias;
                }
                else{
                    $conditions['username'] = $username;
                }
                $user = $usersTable->find()->where($conditions)->hydrate(false)->first();
                if(!empty($user)){

                    $employeeInfo = $employeeRecordsTable->get($user['employee_record_id']);
                    if(empty($employeeInfo) || $employeeInfo->personal_email != $email){
                        $response['message'] = 'ইউজারের ব্যক্তিগত তথ্য পাওয়া যায়নি।';
                        $response['error_code'] = 403;
                        goto rtn;
                    }

                    //password validation check
                    if($usersTable->passwordValidation($new_password) === true){
                        $user = $usersTable->get($user['id']);
                        $user->password = $new_password;
                        if($usersTable->save($user)){
                            $response = array(
                                "status" => "success",
                                "message" => "পাসওয়ার্ড হালনাগাদ হয়েছে।"
                            );
                        }

                    }else{
                        $response['message'] = 'পাসওয়ার্ড নূন্যতম ৬ অক্ষরের হতে হবে। অন্তত একটি A-Z অথবা a-z থাকতে হবে।';
                        $response['error_code'] = 403;
                        goto rtn;
                    }
                }else{
                    $response['message'] = 'ইউজারের কোন তথ্য পাওয়া যায়নি।';
                    $response['error_code'] = 403;
                    goto rtn;
                }
            }
        }

        rtn:
        $this->response->type('json');
        $this->response->body(json_encode($response));
        return $this->response;
    }
    public function getOtherPartyCertificate(){
        $response = array("status" => 'error', 'message' => 'Invalid request');

        if ($this->request->is('post')) {

            $apikey = isset($this->request->data['api_key'])?$this->request->data['api_key']:
                (isset($this->request->data['token'])?$this->request->data['token']:'');

            if ($this->checkToken($apikey) == FALSE) {
                $response['message'] = 'Un-authorize Request';
                goto rtn;
            }
            $nothi_office = isset($this->request->query['nothi_office'])?$this->request->query['nothi_office']:'';
            $potro_id = isset($this->request->query['potro_id'])?$this->request->query['potro_id']:'';
            $nothi_part_id = isset($this->request->query['nothi_part_id'])?$this->request->query['nothi_part_id']:'';
            $other_party_certificate = isset($this->request->data['other_party_certificate'])?$this->request->data['other_party_certificate']:'';

            if(!empty($nothi_office) && !empty($potro_id) && !empty($nothi_part_id)){
                try{
                    $this->switchOffice($nothi_office,'APIOffice');
                    TableRegistry::remove('NothiPotroAttachments');
                    $potroAttachmentTable = TableRegistry::get('NothiPotroAttachments');
                    $potroAttachmentRecord = $potroAttachmentTable->find()
                                                                  ->contain(['NothiPotros'])
                                                                  ->where(['NothiPotroAttachments.nothi_part_no' => $nothi_part_id, 'NothiPotros.id' => $potro_id])->first();

                    if (empty($potroAttachmentRecord)) {
                        throw new \Exception(__("দুঃখিত! পত্র সম্পর্কিত কোন তথ্য পাওয়া যায়নি ।এরর কোডঃ ২"));
                    }

                    $potro = $potroAttachmentRecord['nothi_potro'];
                    $potro_meta = jsonA($potro['application_meta_data']);
                    if(!empty($potro_meta)){
                        if(!empty($other_party_certificate)){
                            $potro_meta['other_party_certificate'] = $other_party_certificate;
                        }
                    }
                    $potro['application_meta_data'] = json_encode($potro_meta);

                    if(!$potroAttachmentTable->NothiPotros->save($potro)){
                        $response['message'] = 'Can not Save required Data. Code: 3';
                        goto rtn;

                    }
                    $response = array("status" => 'success', 'message' => __('Data successfully updated'));

                }catch (\Exception $ex){
                    $response['message'] = $this->makeEncryptedData($ex->getMessage());
                }
            }
        }
        rtn:
        $this->response->type('json');
        $this->response->body(json_encode($response));
        return $this->response;
    }
}