<?php

namespace App\Controller;

use App\Controller\ProjapotiController;
use App\Shell\ArchivingShell;
use Cake\Database\Exception;
use Cake\Datasource\ConnectionManager;
use Cake\Network\Request;
use Cake\Network\Response;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Utility\Inflector;

class ArchiveController extends ProjapotiController
{
    public function __construct(Request $request = null, Response $response = null, $name = null, \Cake\Event\EventManager $eventManager = null)
    {
        parent::__construct($request, $response, $name, $eventManager);
//        if($this->Auth->user()['user_role_id'] != 1 && $this->getCurrentDakSection()['is_admin'] != 1) {
//            $this->Flash->success('এই কার্যক্রমটিতে আপনার অনুমতি নাই।');
//            return $this->redirect('/');
//        }
    }

    public function archivable_list() {
    	$session_data = $this->getCurrentDakSection();
        $nothi_master = TableRegistry::get('NothiMasters');
        if ($session_data['is_admin'] == 1) {
			$archived_list = $nothi_master->find()->where(['nothi_class !=' => 1, 'is_archived is null']);
		} else {
			$archived_list = $nothi_master->find()->where(['office_units_id' => $session_data['office_unit_id'], 'nothi_class !=' => 1, 'is_archived is null']);
		}

        foreach ($archived_list as $archived_data) {
            $is_archivable = self::is_archivable($archived_data->nothi_class, $archived_data->nothi_created_date);
            if ($is_archivable) {
                $nothi_master_current_users_table = TableRegistry::get('NothiMasterCurrentUsers');
                $nothi_master_current_users = $nothi_master_current_users_table->find()->where(['nothi_master_id' => $archived_data->id, 'is_finished' => 0])->count();
                if ($nothi_master_current_users == 0) {
                    $return_data[$archived_data->nothi_class][] = $archived_data->toArray();
                }
            }
        }
        $this->set(compact('return_data'));
    }
    public function pending_archivable_list() {
        $nothi_master = TableRegistry::get('NothiMasters');
        $archived_list = $nothi_master->find()->where(['nothi_class !=' => 1, 'is_archived is null']);

        foreach ($archived_list as $archived_data) {
            $is_archivable = self::is_archivable($archived_data->nothi_class, $archived_data->nothi_created_date);
            if ($is_archivable) {
                $nothi_master_current_users_table = TableRegistry::get('NothiMasterCurrentUsers');
                $nothi_master_current_users = $nothi_master_current_users_table->find()->where(['nothi_master_id' => $archived_data->id, 'is_finished' => 0])->count();
                if ($nothi_master_current_users > 0) {
                    $archived_data_array = $archived_data->toArray();
                    $archived_data_array['pending_note'] = $nothi_master_current_users;
                    $return_data[$archived_data->nothi_class][] = $archived_data_array;
                }
            }
        }
        $this->set(compact('return_data'));
    }
    public function archvied_status() {
        $nothi_archive_requests_table = TableRegistry::get('NothiArchiveRequests');
        $nothi_archive_requests = $nothi_archive_requests_table->find()->where(['office_id' => $this->getCurrentDakSection()['office_id']]);

        foreach ($nothi_archive_requests as $archived_data) {
            //dd($archived_data->toArray());
            if ($archived_data->status == 3) {
                // search in archived data
                $nothi_masters_table = TableRegistry::get('ArchiveNothiMasters');
            } else {
                // search in current data
                $nothi_masters_table = TableRegistry::get('NothiMasters');
            }
            $nothi_master = $nothi_masters_table->get($archived_data->nothi_master_id);
            if ($nothi_master) {
                $nothi_master_array = $nothi_master->toArray();
                $nothi_master_array['archive_status'] = $archived_data->status;
                $return_data[$nothi_master->nothi_class][] = $nothi_master_array;
            }
        }
        $this->set(compact('return_data'));
    }

    public function show_pending_note() {
        if ($this->request->is('post')) {
            $master_id = $this->request->data['master_id'];
            $nothi_master_current_users_table = TableRegistry::get('NothiMasterCurrentUsers');
			$return_data = [];

			$nothi_master_current_users = $nothi_master_current_users_table->find()->where(['nothi_master_id' => $master_id, 'is_finished' => 0])->toArray();
			if ($nothi_master_current_users) {
				foreach ($nothi_master_current_users as $key => $nothi_master_current_user) {
					$nothi_parts_table = TableRegistry::get('NothiParts');
					$nothi_part = $nothi_parts_table->get($nothi_master_current_user['nothi_part_no']);

					$nothi_notes_table = TableRegistry::get('NothiNotes');
					$nothi_note = $nothi_notes_table->find()->where(['nothi_master_id' => $master_id, 'nothi_part_no' => $nothi_master_current_user['nothi_part_no'], 'note_no' => 0])->first();

					//dd($nothi_master_current_user);
					$office_unit_ogranograms_table = TableRegistry::get('OfficeUnitOrganograms');
					$office_unit_ogranogram = $office_unit_ogranograms_table->get($nothi_master_current_user['office_unit_organogram_id']);
					$designation_bng = '';
					$unit_name_bng = '';
					if ($office_unit_ogranogram) {
						//$office_unit_ogranogram = $office_unit_ogranogram->toArray();
						$office_units_table = TableRegistry::get('OfficeUnits');
						$office_unit = $office_units_table->get($office_unit_ogranogram['office_unit_id']);

						$designation_bng = $office_unit_ogranogram['designation_bng'];
						$unit_name_bng = $office_unit['unit_name_bng'];
					}

					$return_data[] = [
						'note_no' => enTobn($nothi_part['nothi_part_no']),
						'note_name' => isset($nothi_note) ? $nothi_note['subject'] : 'অনুচ্ছেদ দেয়া হয় নি',
						'office_units_organogram_id' => $nothi_master_current_user['office_unit_organogram_id'],
						'designation_bng' => $designation_bng,
						'unit_bng' => $unit_name_bng,
					];

				}
			}

            $this->set(compact('return_data'));
            $this->layout = null;
        }
    }
    public static function is_archivable($nothi_class, $nothi_created_date) {
        $time = new \Cake\I18n\Time($nothi_created_date);
        $nothi_created_date = $time->i18nFormat(null, null, 'en-US');

        $created_date = new \DateTime($nothi_created_date);
        $current_date = new \DateTime();
        $interval = $current_date->diff($created_date);
        $nothi_age = $interval->format('%a');

        $archivable = 0;
        if ($nothi_class == 4) {
            $nothi_alive_days = 365;
            if ($nothi_age >= ($nothi_alive_days)) {
                $archivable = 1;
            }
        } elseif ($nothi_class == 3) {
            $nothi_alive_days = 365 * 5;
            if ($nothi_age >= ($nothi_alive_days)) {
                $archivable = 1;
            }
        } elseif ($nothi_class == 2) {
            $nothi_alive_days = 365 * 10;
            if ($nothi_age >= ($nothi_alive_days)) {
                $archivable = 1;
            }
        }

        return $archivable;
    }
    public function doArchive() {
        //dd($this->getCurrentDakSection()['office_id']);
        if ($this->request->is(['post'])) {
            $nothi_master_id = $this->request->data['nothi_master_id'];
            $nothi_master_table = TableRegistry::get('NothiMasters');
            $nothi_master = $nothi_master_table->get($nothi_master_id);

			$nothi_master_permission_table = TableRegistry::get('NothiMasterPermissions');
			$nothi_master_permissions = $nothi_master_permission_table->find('list', ['keyField'=>'office_unit_organograms_id', 'valueField'=>'office_unit_organograms_id'])->where(['nothi_masters_id' => $nothi_master_id])->toArray();

            $nothi_master['is_archived'] = 1;
            $nothi_master['archived_date'] = date('Y-m-d H:i:s');
            $nothi_master['archived_organogram_id'] = $this->getCurrentDakSection()['office_unit_organogram_id'];

            try{
                $nothi_archive_requests_table = TableRegistry::get('NothiArchiveRequests');
                $prev_data = $nothi_archive_requests_table->exists(['office_id'=>$this->getCurrentDakSection()['office_id'], 'nothi_master_id'=>$nothi_master_id]);
                if ($prev_data) {
                    $nothi_master_table->save($nothi_master);
                    $this->Flash->error('নথিটি ইতোমধ্যে আর্কাইভের জন্য প্রেরণ করা হয়েছে।');
                    return $this->redirect($this->referer());
                }

                $data = $nothi_archive_requests_table->newEntity();
                $data['office_id'] = $this->getCurrentDakSection()['office_id'];
                $data['nothi_master_id'] = $nothi_master_id;
                $data['requested_date'] = date('Y-m-d H:i:s');
                $data['requested_organogram_id'] = $this->getCurrentDakSection()['office_unit_organogram_id'];
                $data['status'] = 1;
                $data['responsed_date'] = null;
                $data['comments'] = null;
                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');

                $nothi_archive_requests_table->save($data);


				$messagesTable = TableRegistry::get('Messages');
				foreach ($nothi_master_permissions as $key => $permitted_organogram) {
					$message = $messagesTable->newEntity();
					$message->title = 'নথি আর্কাইভ';
					$message->message = 'নথির নম্বর: '.$nothi_master['nothi_no'].', নথির বিষয়: '.$nothi_master['subject'].'</br>আর্কাইভ করেছেন: '.$this->getCurrentDakSection()['designation'].', '.$this->getCurrentDakSection()['office_unit_name'].', '.$this->getCurrentDakSection()['office_name'];
					$message->message_by = $this->Auth->user()['id'];
					$message->is_deleted = 0;
					$message->message_for = 'organogram';
					$message->related_id = $permitted_organogram;
					$messagesTable->save($message);

				}
            } catch (Exception $e) {
                $this->Flash->error('নথিটি এই মুহূর্তে আর্কাইভ করা সম্ভব হচ্ছে না, কিছুক্ষণ পর পুনরায় চেষ্টা করুন।');
                return $this->redirect($this->referer());
            }
            $nothi_master_table->save($nothi_master);

            $this->Flash->success('নথিটি আর্কাইভের জন্য প্রেরণ করা হল।');
            return $this->redirect(['action'=>'archivable_list']);
        } else {
            $this->Flash->error('আপনি ভুল পথে পরিচালিত হয়েছেন।');
            return $this->redirect(['action'=>'archivable_list']);
        }
    }

    // archiveable nothi show for SUPERMAN
    public function layerwiseArchiveNothiList() {
        if($this->Auth->user()['user_role_id'] != 1) {
            $this->Flash->success('এই কার্যক্রমটিতে আপনার অনুমতি নাই।');
            return $this->redirect('/');
        }
        $office_layers = TableRegistry::get('OfficeLayers');
        $layerLavel = $office_layers->layerLavel;
        $layers = [];
        foreach ($layerLavel as $layer_en => $layer_id) {
            //dd(__('Directorate'));
            $layers[$layer_id] = __($layer_en);
        }

        $office_ministries = TableRegistry::get('OfficeMinistries');
        $ministries = $office_ministries->find('list', ['keyField'=>'id', 'valueField'=>'name_bng'])->toArray();
        $this->set(compact('layers', 'ministries'));
    }
    public function layerwiseArchiveDoneNothiList() {
        if($this->Auth->user()['user_role_id'] != 1) {
            $this->Flash->success('এই কার্যক্রমটিতে আপনার অনুমতি নাই।');
            return $this->redirect('/');
        }
        $office_layers = TableRegistry::get('OfficeLayers');
        $layerLavel = $office_layers->layerLavel;
        $layers = [];
        foreach ($layerLavel as $layer_en => $layer_id) {
            //dd(__('Directorate'));
            $layers[$layer_id] = __($layer_en);
        }

        $office_ministries = TableRegistry::get('OfficeMinistries');
        $ministries = $office_ministries->find('list', ['keyField'=>'id', 'valueField'=>'name_bng'])->toArray();
        $this->set(compact('layers', 'ministries'));
    }
    public function layerWiseOfficeList() {
        $layer_id = $this->request->data['layer_id'];
        $ministry_id = $this->request->data['ministry_id'];
        if ($layer_id != '') {
            if ($layer_id != 0) {
                $office_layers_table = TableRegistry::get('OfficeLayers');
                $office_layers = $office_layers_table->find('list', ['keyField'=>'id', 'valueField'=>'layer_name_bng'])->where(['layer_level' => $layer_id])->toArray();
            } else {
                $office_layers_table = TableRegistry::get('OfficeLayers');
                $office_layers = $office_layers_table->find('list', ['keyField'=>'id', 'valueField'=>'layer_name_bng'])->where(['layer_level  NOT IN' => [1,2,3,4,5]])->toArray();
            }
            $offices_table = TableRegistry::get('Offices');
            if ($ministry_id != '') {
                $offices = $offices_table->find('list', ['keyField'=>'id', 'valueField'=>'office_name_bng'])->where(['office_layer_id IN' => array_keys($office_layers), 'office_ministry_id' => $ministry_id])->toArray();
            } else {
                $offices = $offices_table->find('list', ['keyField'=>'id', 'valueField'=>'office_name_bng'])->where(['office_layer_id IN' => array_keys($office_layers)])->toArray();
            }

            $this->response->body(json_encode($offices));
            $this->response->type('application/json');
            return $this->response;
        } else {
            $this->response->body(json_encode(['layer id not found']));
            $this->response->type('application/json');
            return $this->response;
        }
    }
    public function layerOfficeWiseArchiveReport() {
        $office_id = $this->request->data['office_id'];
        if (isset($this->request->data['done'])) {
            $is_done = true;
        } else {
            $is_done = false;
        }
        if (is_array($this->total_archivable_nothi_count($office_id, $is_done))) {
            $total['nothi'] = array_sum($this->total_archivable_nothi_count($office_id, $is_done)['nothi']);
            $total['note'] = array_sum($this->total_archivable_nothi_count($office_id, $is_done)['note']);
        } else {
            $total = $this->total_archivable_nothi_count($office_id, $is_done);
        }
        $this->response->body(json_encode($total));
        $this->response->type('application/json');
        return $this->response;
    }
    private function total_archivable_nothi_count($office_id, $isAllDone = false) {
        try {
            $this->switchOffice($office_id, 'officeDb');

            $nothi_masters_table = TableRegistry::get('NothiMasters');
            $archived_list = $nothi_masters_table->find()->where(['nothi_class !=' => 1, 'is_archived is null']);
            $return['nothi'] = [];
            $return['note'] = [];
            foreach ($archived_list as $archived_data) {
                $is_archivable = self::is_archivable($archived_data->nothi_class, $archived_data->nothi_created_date);
                if ($is_archivable) {
                    $nothi_master_current_users_table = TableRegistry::get('NothiMasterCurrentUsers');
                    $nothi_master_current_users = $nothi_master_current_users_table->find()->where(['nothi_master_id' => $archived_data->id, 'is_finished' => 0])->count();
                    if ($isAllDone) {
                        if ($nothi_master_current_users == 0) {
                            if (isset($return['nothi'][$archived_data->office_units_id])) {
                                $return['nothi'][$archived_data->office_units_id]++;
                            } else {
                                $return['nothi'][$archived_data->office_units_id] = 1;
                            }
                            if (isset($return['note'][$archived_data->office_units_id])) {
                                $return['note'][$archived_data->office_units_id] += $nothi_master_current_users;
                            } else {
                                $return['note'][$archived_data->office_units_id] = $nothi_master_current_users;
                            }
                        }
                    } else {
                        if ($nothi_master_current_users > 0) {
                            if (isset($return['nothi'][$archived_data->office_units_id])) {
                                $return['nothi'][$archived_data->office_units_id]++;
                            } else {
                                $return['nothi'][$archived_data->office_units_id] = 1;
                            }
                            if (isset($return['note'][$archived_data->office_units_id])) {
                                $return['note'][$archived_data->office_units_id] += $nothi_master_current_users;
                            } else {
                                $return['note'][$archived_data->office_units_id] = $nothi_master_current_users;
                            }
                        }
                    }
                }
            }
        } catch (\Exception $exception) {
            $return = 'db-error';
        }
        return $return;
    }
    private function archivable_nothi_list($office_id) {
        try {
            $this->switchOffice($office_id, 'officeDb');

            $nothi_masters_table = TableRegistry::get('NothiMasters');
            $archived_list = $nothi_masters_table->find()->where(['nothi_class !=' => 1, 'is_archived is null']);
            $return = [];
            foreach ($archived_list as $archived_data) {
                $is_archivable = self::is_archivable($archived_data->nothi_class, $archived_data->nothi_created_date);
                if ($is_archivable) {
                    $nothi_master_current_users_table = TableRegistry::get('NothiMasterCurrentUsers');
                    $nothi_master_current_users = $nothi_master_current_users_table->find()->where(['nothi_master_id' => $archived_data->id, 'is_finished' => 0])->count();
                    if ($nothi_master_current_users == 0) {
                        $return[] = ['nothi_no' => $archived_data->nothi_no, 'subject' => $archived_data->subject, 'office_unit_id' => $archived_data->office_units_id];
                    }
                }
            }
        } catch (\Exception $exception) {
            $return = 'db-error';
        }
        return $return;
    }

    public function getNothiNameByUnitId() {
        $unit_id = $this->request->data['unit_id'];
        $units = TableRegistry::get('OfficeUnits');
        $unit = $units->get($unit_id);
        $office_id = $unit->office_id;
        if (is_array($this->get_nothi_name($office_id, $unit_id))) {
            $nothi_name = $this->get_nothi_name($office_id, $unit_id);
        } else {
            $nothi_name = $this->get_nothi_name($office_id, $unit_id);
        }
        $this->response->body(json_encode($nothi_name));
        $this->response->type('application/json');
        return $this->response;
    }
    private function get_nothi_name($office_id, $unit_id, $isAllDone = false) {
        try {
            $this->switchOffice($office_id, 'officeDb');
            $nothi_masters_table = TableRegistry::get('NothiMasters');
            $archived_list = $nothi_masters_table->find()->where(['office_units_id'=>$unit_id, 'nothi_class !=' => 1, 'is_archived is null']);
            $return = [];
            foreach ($archived_list as $key => $archived_data) {
                $is_archivable = self::is_archivable($archived_data->nothi_class, $archived_data->nothi_created_date);
                if ($is_archivable) {
                    $nothi_master_current_users_table = TableRegistry::get('NothiMasterCurrentUsers');
                    $nothi_master_current_users = $nothi_master_current_users_table->find()->where(['nothi_master_id' => $archived_data->id, 'is_finished' => 0])->count();
                    if ($isAllDone) {
                        if ($nothi_master_current_users == 0) {
                            $return[$key]['subject'] = $archived_data->subject;
                            $return[$key]['nothi_no'] = $archived_data->nothi_no;
                        }
                    } else {
                        if ($nothi_master_current_users > 0) {
                            $return[$key]['subject'] = $archived_data->subject;
                            $return[$key]['nothi_no'] = $archived_data->nothi_no;
                        }
                    }
                }
            }
        } catch (\Exception $exception) {
            $return = 'db-error';
        }
        return $return;
    }

    public function layerOfficeUnitWiseArchiveReport() {
        $office_id = $this->request->data['office_id'];
        $total_archivable_nothi = $this->total_archivable_nothi_count($office_id)['nothi'];
        $total_archivable_note = $this->total_archivable_nothi_count($office_id)['note'];
        $return_data = [];
        $office_units_table = TableRegistry::get('OfficeUnits');
        foreach ($total_archivable_nothi as $office_unit_id => $archivable_nothi_count) {
            $office_unit = $office_units_table->get($office_unit_id);
            $return_data[$office_unit->unit_name_bng]['nothi'] = $archivable_nothi_count;
            $return_data[$office_unit->unit_name_bng]['unit_id'] = $office_unit_id;
        }
        foreach ($total_archivable_note as $office_unit_id => $archivable_nothi_count) {
            $office_unit = $office_units_table->get($office_unit_id);
            $return_data[$office_unit->unit_name_bng]['note'] = $archivable_nothi_count;
        }
        $this->response->body(json_encode($return_data));
        $this->response->type('application/json');
        return $this->response;
    }
    public function layerOfficeArchiveReport() {
        $office_id = $this->request->data['office_id'];
        $total_archivable_nothi = $this->archivable_nothi_list($office_id);
        $return_data = [];
        $office_units_table = TableRegistry::get('OfficeUnits');
        foreach ($total_archivable_nothi as $key => $nothi_info) {
            $office_unit = $office_units_table->get($nothi_info['office_unit_id']);
            $return_data[] = [
                'subject' => $nothi_info['subject'],
                'nothi_no' => $nothi_info['nothi_no'],
                'unit_name' => $office_unit->unit_name_bng,
            ];
        }
        $this->response->body(json_encode($return_data));
        $this->response->type('application/json');
        return $this->response;
    }

    // nothi archive cron
    public function archiveAllNothi() {
        die;
        $nothi_archive_requests_table = TableRegistry::get('NothiArchiveRequests');
        $nothi_archive_requests_data = $nothi_archive_requests_table->find()->where(['status' => 1]);
        if ($nothi_archive_requests_data->count() > 0) {
            foreach ($nothi_archive_requests_data as $key => $nothi_archive_data) {
                $nothi_archive_data['status'] = 2;
                $nothi_archive_requests_table->save($nothi_archive_data);

                $return_status = $this->archive_data($nothi_archive_data->office_id, $nothi_archive_data->nothi_master_id);

                if ($return_status) {
                    $nothi_archive_data['status'] = 3;
                    $nothi_archive_requests_table->save($nothi_archive_data);

                    pr('Archive Done');
                } else {
                    $nothi_archive_data['status'] = 4;
                    $nothi_archive_requests_table->save($nothi_archive_data);

                    pr('Archive Failed');
                }
            }
        } else {
            pr('No data found for archive');
        }
    }
    private function archive_data($office_id, $nothi_master_id) {
        try {
            $this->switchOffice($office_id, 'OfficeDb');
            $connection = ConnectionManager::get('default');
            $table_list = [
                'note_initialize' => 'nothi_masters_id',
                'nothi_dak_potro_maps' => 'nothi_masters_id',
                'nothi_data_change_history' => 'nothi_master_id',
                'nothi_masters_dak_map' => 'nothi_masters_id',
                'nothi_master_current_users' => 'nothi_master_id',
                'nothi_master_movements' => 'nothi_master_id',
                'nothi_master_permissions' => 'nothi_masters_id',
                'nothi_notes' => 'nothi_master_id',
                'nothi_note_attachments' => 'nothi_master_id',
                'nothi_note_attachment_refs' => 'nothi_master_id',
                'nothi_note_permissions' => 'nothi_masters_id',
                'nothi_note_sheets' => 'nothi_master_id',
                'nothi_note_signatures' => 'nothi_master_id',
                'nothi_parts' => 'nothi_masters_id',
                'nothi_potros' => 'nothi_master_id',
                'nothi_potro_attachments' => 'nothi_master_id',
                'potro_flags' => 'nothi_master_id',
            ];
            $nothi_masters_initiate = TableRegistry::get("NothiMasters")->exists(['id' => $nothi_master_id]);
            if ($nothi_masters_initiate) {
                $table_name = 'nothi_masters';
                $queries[] = "CREATE TABLE IF NOT EXISTS `archive_$table_name` LIKE `$table_name`;";
                $queries[] = "INSERT `archive_$table_name` SELECT * FROM `$table_name`  WHERE id = $nothi_master_id;";
                $queries[] = "DELETE FROM `$table_name`  WHERE id = $nothi_master_id;";
                foreach ($table_list as $table_name => $key) {
                    $queries[] = "CREATE TABLE IF NOT EXISTS `archive_$table_name` LIKE `$table_name`;";
                    $queries[] = "INSERT `archive_$table_name` SELECT * FROM `$table_name`  WHERE $key = $nothi_master_id;";
                    $queries[] = "DELETE FROM `$table_name`  WHERE id = $nothi_master_id;";
                }

                $connection->transactional(function ($connection) use ($queries) {
                    foreach ($queries as $query) {
                        $connection->execute($query);
                    }
                });

                return 1;
            }
        } catch (\Exception $exception) {
            $connection->rollback();
            pr($exception->getMessage());
            return 0;
        }

    }

    // show archived nothi
    public function archiveNothiList(){
        $this->view = null;
		$tableList = ConnectionManager::get('default')->schemaCollection()->listTables();
		if (in_array(Inflector::underscore('ArchiveNothiMasters'), $tableList)) {
            $nothiMasterTable = TableRegistry::get('ArchiveNothiMasters');
            $employeeOffice = $this->getCurrentDakSection();
            $officeUnitsTable = TableRegistry::get('OfficeUnits');
            $officeUnits = $officeUnitsTable->getOfficeUnitsList($employeeOffice['office_id']);
            $start = isset($this->request->data['start']) ? intval($this->request->data['start']) : 0;
            $len = isset($this->request->data['length']) ? intval($this->request->data['length']) : 200;
            $page = !empty($this->request->data['page']) ? intval($this->request->data['page']) : (($start / $len) + 1);

            $nothiMasterPermissionsTable = TableRegistry::get("ArchiveNothiMasterPermissions");
            $nothiMasterPermissions = $nothiMasterPermissionsTable->find('list', ['keyField' => 'id', 'valueField' => 'nothi_masters_id'])
                ->where(['office_unit_organograms_id' => $employeeOffice['office_unit_organogram_id']])
                ->select('nothi_masters_id')
                ->group(['nothi_masters_id'])
                ->toArray();

            $permitted_nothi_master_id = array_values($nothiMasterPermissions);
            $getListData = $nothiMasterTable->find()->where(['id IN' => $permitted_nothi_master_id])->toArray();

            $list = $getListData;
            $listCount = count($getListData);

            $json = [];
            if (!empty($list)) {
                $nothiMasterCurrentUserTable = TableRegistry::get('ArchiveNothiMasterCurrentUsers');
                $i = 0;
                foreach ($list as $key => &$value) {
                    $value['nothi_master_id'] = $value['id'];
                    $totalCount = $nothiMasterCurrentUserTable->find()->where([
                        'nothi_office' => $employeeOffice['office_id'],
                        'office_unit_organogram_id' => $employeeOffice['office_unit_organogram_id'],
                        'nothi_master_id' => $value['nothi_master_id']
                    ])->count();
                    $isNew = $nothiMasterCurrentUserTable->find()->where([
                        'nothi_office' => $employeeOffice['office_id'],
                        'office_unit_organogram_id' => $employeeOffice['office_unit_organogram_id'],
                        'nothi_master_id' => $value['nothi_master_id'],
                        'view_status' => 0,
                    ])->count();
                    $isPrior = $nothiMasterCurrentUserTable->find()->where([
                        'nothi_office' => $employeeOffice['office_id'],
                        'office_unit_organogram_id' => $employeeOffice['office_unit_organogram_id'],
                        'is_archive' => 0,
                        'nothi_master_id' => $value['nothi_master_id'],
                        'priority' => 1,
                    ])->count();
                    $isSummary = 0;

                    $totalCountbn = enTobn($totalCount);

                    $nothi_parts_table = TableRegistry::get('ArchiveNothiParts');
                    $nothi_part = $nothi_parts_table->find()->where(['nothi_masters_id' => $value['nothi_master_id']])->first();
                    $i++;
                    $si = (($page - 1) * $len) + $i;
                    $json[] = [
                        enTobn($si),
                        $value['nothi_no'] . ($totalCount > 1 ? " <label class='badge badge-primary'>{$totalCountbn}</label>" : ''),
                        $value['subject'],
                        $officeUnits[$value['office_units_id']] . ', ' . $employeeOffice['office_name'],
                        "<div class='text-center'><a href='/archiveNothiDetail/{$nothi_part['id']}' class='btn btn btn-success btn-xs'><i class='a2i_gn_details2'></i></a></div>",
                        'DT_RowClass' => ($isPrior ? 'nothiImportant' : ($isNew ? 'new' : ($isSummary ? 'summaryList' : '')))
                        //'DT_RowClass'=>($isNew?'new':($isPrior?'nothiImportant':($isSummary?'summaryList':'')))
                    ];
                }
            }

            $json_data = array(
                "draw" => 0,
                "recordsTotal" => $listCount,
                "recordsFiltered" => $listCount,
                "data" => $json
            );
        } else {
            $json = [];
            $json_data = array(
                "draw" => 0,
                "recordsTotal" => 0,
                "recordsFiltered" => 0,
                "data" => $json
            );
        }
        $this->response->body(json_encode($json_data));
        $this->response->type('application/json');
        return $this->response;
    }
    public function archiveNothiDetail($part_id, $nothi_office = 0) {
        $employee_office = $this->getCurrentDakSection();
        $otherNothi = false;
        if ($nothi_office != 0 && $nothi_office != $employee_office['office_id']) {
            $otherNothi = true;

            $this->switchOffice($nothi_office, 'MainNothiOffice');
        } else {
            $nothi_office = $employee_office['office_id'];
        }

        $this->set('nothi_office', $nothi_office);
        $this->set(compact('otherNothi','part_id'));

        $nothiMasterPermissionsTable = TableRegistry::get("ArchiveNothiMasterPermissions");
        $nothiMasterCurrentUsersTable = TableRegistry::get("ArchiveNothiMasterCurrentUsers");

        TableRegistry::remove('ArchiveNothiParts');
        $nothiPartsTable = TableRegistry::get("ArchiveNothiParts");

        $nothiMastersCurrentUser = array();
        $nothiPartsInformation = $nothiPartsTable->get($part_id);
        /**
         * Has access in this nothi part
         * if not visited redirect to dashboard
         */
        $has_access = $nothiMasterPermissionsTable->hasAccessPartList($employee_office['office_id'],
            $employee_office['office_unit_id'],
            $employee_office['office_unit_organogram_id'], $part_id,
            $nothi_office);
        if (empty($has_access)) {
            /**
             * Last Nothi part does not have permission but one of the Nothi part of this nothi has permission
             * let's find that nothi
             */
            if (!empty($nothiPartsInformation['nothi_masters_id'])) {
                $hasAccessinNothiMaster = $nothiMasterPermissionsTable->hasAccess($nothi_office, $employee_office['office_id'],
                    $employee_office['office_unit_id'],
                    $employee_office['office_unit_organogram_id'], $nothiPartsInformation['nothi_masters_id']);
                if (empty($hasAccessinNothiMaster)) {
                    $this->Flash->error("দুঃখিত! কোন তথ্য পাওয়া যায় নি। ");
                    $this->redirect(['controller' => 'Dashboard', 'action' => 'dashboard']);
                } else {
                    $this->redirect(['_name' => 'nothiDetail', $hasAccessinNothiMaster['nothi_part_no'], $hasAccessinNothiMaster['nothi_office']]);
                }
            }

        }
//        else if($has_access['visited'] <1){
//              $this->Flash->error("দুঃখিত! কোন তথ্য পাওয়া যায় নি। ");
//                    $this->redirect(['controller' => 'Dashboard', 'action' => 'dashboard']);
//        }
        $officeUnitTable = TableRegistry::get('OfficeUnits');
        $officeunit = $officeUnitTable->get($nothiPartsInformation['office_units_id']);

        $nothiUnit = $officeunit->unit_name_bng;

        $this->set('nothiUnit', $nothiUnit);

        $allNothiMasterList = $nothiMasterPermissionsTable->getMasterNothiPartList($nothi_office,
            $employee_office['office_id'], $employee_office['office_unit_id'],
            $employee_office['office_unit_organogram_id'],
            $nothiPartsInformation['nothi_masters_id']);

        /**
         * Only visited nothi will be viewed to user
         */
        /* $permitted_nothi_list = $nothiMasterPermissionsTable->allNothiPermissionList($nothi_office,
             $employee_office['office_id'], $employee_office['office_unit_id'],
             $employee_office['office_unit_organogram_id'], 1);*/

        $allNothiParts =$a =array();

        $allNothiParts = $nothiPartsTable->find()->select(['id', 'nothi_part_no_bn',
            'ArchiveNothiNotes.subject'])
            ->join([
                'ArchiveNothiNotes' => [
                    'table' => 'archive_nothi_notes',
                    'type' => 'left',
                    'conditions' => "ArchiveNothiNotes.nothi_part_no = ArchiveNothiParts.id AND ArchiveNothiNotes.note_no=0"
                ]
            ])
            ->where(['ArchiveNothiParts.nothi_masters_id' => $nothiPartsInformation['nothi_masters_id'],
                'ArchiveNothiParts.id IN' => array_map(function($val){
                    return $val['nothi_part_no'];
                },$allNothiMasterList)])->order(['ArchiveNothiParts.id  ASC'])->toArray();

        $nothiMasterPriviligeType = array();

        if (!empty($allNothiParts)) {
            foreach ($allNothiParts as $key => $nothiMasters) {

                $nothiMastersCurrentUser = $nothiMasterCurrentUsersTable->find()->where(['nothi_part_no' => $nothiMasters['id'],
                    'office_id' => $employee_office['office_id'], 'office_unit_id' => $employee_office['office_unit_id'],
                    'office_unit_organogram_id' => $employee_office['office_unit_organogram_id'],
                    'nothi_office' => $nothi_office])->first();
                if (!empty($nothiMastersCurrentUser)) {
                    $nothiMasterPriviligeType[$nothiMasters['id']] = 1;
                }
                if (!empty($nothiMastersCurrentUser)) {
                    $nothiMasters['is_finished'] = $nothiMastersCurrentUser['is_finished'];
                } else {
                    $nothiMasters['is_finished'] = 0;
                }
                if ($part_id == $nothiMastersCurrentUser['nothi_part_no'] && !empty($nothiMastersCurrentUser['priority']) && $nothiMastersCurrentUser['priority'] == 1) {
                    $this->set('note_priority', $nothiMastersCurrentUser['priority']);
                    $this->set('note_priority_text', 'নোটের অগ্রাধিকারঃ ' . (($nothiMastersCurrentUser['priority'] == 1) ? 'জরুরি' : 'সাধারণ'));
                }
                if (!empty($nothiMastersCurrentUser) && $nothiMastersCurrentUser['view_status'] == 0) {
                    $nothiMastersCurrentUser->view_status = 1;
                    $nothiMasterCurrentUsersTable->save($nothiMastersCurrentUser);
                }
            }
        }

        $this->set(compact('allNothiParts'));
        $this->set(compact('nothiMasterPriviligeType'));
        $this->set('nothiRecord', $nothiPartsInformation);
        $this->set('nothi_office', $nothi_office);
        $this->set('noteNos', $this->showNoteList($nothiPartsInformation['nothi_masters_id'], $nothi_office,$employee_office));

    }
    public function showNoteList($nothimasterid = 0, $nothi_office = 0, $employee_office = []) {

        TableRegistry::remove("ArchiveNothiNotes");
        $noteTable = TableRegistry::get('ArchiveNothiNotes');

        TableRegistry::remove('ArchiveNothiMasterPermissions');
        $nothiMastersPermissionTable = TableRegistry::get('ArchiveNothiMasterPermissions');
        $permitted_nothi_list = $nothiMastersPermissionTable->allNothiPermissionList($nothi_office, $employee_office['office_id'], $employee_office['office_unit_id'], $employee_office['office_unit_organogram_id']);


        if ($nothi_office != 0 && $nothi_office != $employee_office['office_id']) {

            $this->switchOffice($nothi_office, 'MainNothiOffice');
        } else {
            $nothi_office = $employee_office['office_id'];
        }

        $noteTablequery = $noteTable->find()
            ->select(['ArchiveNothiNotes.id', 'ArchiveNothiNotes.nothi_master_id', 'ArchiveNothiNotes.nothi_part_no', 'nothi_part_no_en' => 'ArchiveNothiParts.nothi_part_no', 'nothi_part_no_bn' => 'ArchiveNothiParts.nothi_part_no_bn', 'ArchiveNothiNotes.nothi_notesheet_id', 'ArchiveNothiNotes.note_no'])
            ->where(['ArchiveNothiNotes.nothi_master_id' => $nothimasterid, 'note_no >= ' => 0])
            ->join([
                'ArchiveNothiParts' => [
                    'table' => 'archive_nothi_parts',
                    "conditions" => "ArchiveNothiParts.id= ArchiveNothiNotes.nothi_part_no",
                    "type" => "INNER"
                ]
            ])
            ->where(['ArchiveNothiNotes.nothi_part_no IN' => $permitted_nothi_list, '(ArchiveNothiNotes.office_organogram_id = ' . $employee_office['office_unit_organogram_id'] . ' OR ArchiveNothiNotes.note_status <> "DRAFT")'])
            ->order(['nothi_part_no_en asc,cast(ArchiveNothiNotes.note_no as signed) asc'])->toArray();



        $returnData = array();


        if (!empty($noteTablequery)) {
            foreach ($noteTablequery as $key => $row) {
                $returnData[] = array(
                    'nothi_master_id' => $row['nothi_master_id'],
                    'nothi_part_no' => $row['nothi_part_no'],
                    'nothi_part_no_bn' => ($row['nothi_part_no_bn']),
                    'nothi_part_no_en' => ($row['nothi_part_no_en']),
                    'nothi_notesheet_id' => $row['nothi_notesheet_id'],
                    'note_no' => enTobn($row['note_no']),
                    'note_no_en' => $row['note_no'],
                    'id' => $row['id'],
                );
            }
        }

        return $returnData;
    }
    public function notesheetPageAllNew($id, $nothi_office = 0, $print = 0) {
        if($print){
            $this->layout = 'single';
        }else{
            $this->layout = null;
        }
        $this->view = '/Archive/notesShowPageAll';

        $employee_office = $this->getCurrentDakSection();
        $otherNothi = false;
        if ($nothi_office != 0 && $nothi_office != $employee_office['office_id']) {
            $otherNothi = true;

            $this->switchOffice($nothi_office, 'MainNothiOffice');
        } else {
            $nothi_office = $employee_office['office_id'];
        }

        $this->set('nothi_office', $nothi_office);
        $this->set('otherNothi', $otherNothi);

        TableRegistry::remove("ArchiveNothiMasterMovements");
        TableRegistry::remove("ArchiveNothiParts");
        TableRegistry::remove("ArchiveNothiNoteSheets");
        TableRegistry::remove("ArchiveNothiPotroAttachments");
        TableRegistry::remove("ArchiveNothiDecisions");
        TableRegistry::remove("ArchiveNothiNoteAttachments");
        TableRegistry::remove("ArchiveNothiNotes");
        $nothiPartsTable = TableRegistry::get("ArchiveNothiParts");
        $nothiMasterPermissionsTable = TableRegistry::get("ArchiveNothiMasterPermissions");

        $nothiPartsInformation = $nothiPartsTable->getAll('ArchiveNothiParts.id=' . $id)->first();

        $allNothiMasterList = array();
        $allNothiMasterList = $nothiMasterPermissionsTable->getMasterNothiPartList($nothi_office, $employee_office['office_id'], $employee_office['office_unit_id'], $employee_office['office_unit_organogram_id'], $nothiPartsInformation['nothi_masters_id']);

        $nothiPartId = array_map(function($val){
            return $val['nothi_part_no'];
        },$allNothiMasterList);

        $nothi_notes_table = TableRegistry::get('ArchiveNothiNotes');
        $limit = 10;
        //limitStart
        $nothi_notes = $nothi_notes_table->find()
            ->select(['ArchiveNothiNotes.nothi_master_id', 'ArchiveNothiNotes.nothi_part_no','ArchiveNothiNotes.id',
                'nothi_part_no_en' => 'ArchiveNothiParts.nothi_part_no', 'nothi_part_no_bn' => 'ArchiveNothiParts.nothi_part_no_bn', 'ArchiveNothiNotes.nothi_notesheet_id', 'ArchiveNothiNotes.note_no', 'ArchiveNothiNotes.id', 'ArchiveNothiNotes.nothi_notesheet_id', 'ArchiveNothiNotes.subject', 'ArchiveNothiNotes.note_description', 'ArchiveNothiNotes.note_status', 'ArchiveNothiNotes.is_potrojari', 'ArchiveNothiNotes.potrojari_status', 'ArchiveNothiNotes.created', 'ArchiveNothiNotes.modified'])
            ->where(['ArchiveNothiNotes.nothi_part_no IN' => $nothiPartId,
                '(ArchiveNothiNotes.office_organogram_id = ' . $employee_office['office_unit_organogram_id'] . ' OR ArchiveNothiNotes.note_status <> "DRAFT")'])
            ->join([
                'ArchiveNothiParts' => [
                    'table' => 'archive_nothi_parts',
                    "conditions" => "ArchiveNothiParts.id= ArchiveNothiNotes.nothi_part_no",
                    "type" => "INNER"
                ]
            ])->order(['ArchiveNothiNotes.nothi_part_no'])->limit($limit);


        $total_page = ceil($nothi_notes->count() / $limit);
        if (!isset($this->request->data['limitStart'])) {
            $this->request->data['limitStart'] = 0;
        }
        $requested_page_no = ceil($this->request->data['limitStart'] / $limit);
        if ($requested_page_no > $total_page) {
            $requested_page_no = $total_page;
        }
        $nothi_notes = $nothi_notes->page($requested_page_no+1);
        if (count($nothi_notes->toArray()) == 0) {
            die;
        }
        $this->set('notesquery', $nothi_notes);

        $noteAttachments = TableRegistry::get('ArchiveNothiNoteAttachments');

        $noteAttachmentsquery = $noteAttachments->find()->where(['nothi_part_no IN' => $nothiPartId])->toArray();

        $noteAttachmentsmap = array();
        if (!empty($noteAttachmentsquery)) {
            foreach ($noteAttachmentsquery as $key => $value) {
                $noteAttachmentsmap[$value['note_no']][] = array(
                    'user_file_name' => $value['user_file_name'],
                    'attachment_type' => str_replace("; charset=binary", '', $value['attachment_type']),
                    'file_name' => $value['file_name'],
                    'id' => $value['id']
                );
            }
        }

        $this->set('noteAttachmentsmap', $noteAttachmentsmap);

        $this->set('signatures', $this->getSignaturesAll($nothiPartId, 0, $nothi_office));

        $employeeOfficeDesignation = array();
        $allPermittedUser = $nothiMasterPermissionsTable->getMasterNothiListbyMasterId($nothiPartsInformation['nothi_masters_id'], $nothi_office);

        if (!empty($allPermittedUser)) {
            foreach ($allPermittedUser as $key => $value) {

                $employeeOfficeDesignation[$value['office_unit_organograms_id']] = array(
                    $value['designation_level'], 1
                );
            }
        }

        $this->set('noteNos', $this->showNoteList($nothiPartsInformation['nothi_masters_id'], $nothi_office,$employee_office));

        $this->set('employeeOfficeDesignation', $employeeOfficeDesignation);
        $this->set('nothi_office', $nothi_office);
    }
    private function getSignaturesAll($masterId, $nothiNoteId = 0) {

        TableRegistry::remove('ArchiveNothiNoteSignatures');
        $noteSignatureTable = TableRegistry::get('ArchiveNothiNoteSignatures');
        $employeeRecordsTable = TableRegistry::get('EmployeeRecords');
        $employeeOfficesTable = TableRegistry::get('EmployeeOffices');
        $userTable = TableRegistry::get('Users');

        $allSignatures = $noteSignatureTable->find()->where(['nothi_part_no IN' => $masterId]);

        if (!empty($nothiNoteId)) {
            $allSignatures = $allSignatures->where(['nothi_note_id' => $nothiNoteId]);
        }

        $allSignatures = $allSignatures->toArray();

        $returnData = array();
        if (!empty($allSignatures)) {
            foreach ($allSignatures as $key => $value) {
                $userInfo = $userTable->find()->where(['employee_record_id' => $value['employee_id']])->first();
                $employeeinfo = $employeeOfficesTable->getOfficeDesignation($value['employee_id'], $value['office_organogram_id']);
                $employeeRecords = $employeeRecordsTable->get($value['employee_id']);
                $returnData[$value['nothi_part_no']][$value['nothi_note_id']][] = array(
                    'office_id' => $value['office_id'],
                    'office_unit_id' => $value['office_unit_id'],
                    'office_organogram_id' => $value['office_organogram_id'],
                    'employee_id' => $value['employee_id'],
                    'employee_designation' => $value['employee_designation'],
                    'note_decision' => $value['note_decision'],
                    'cross_signature' => $value['cross_signature'],
                    'signature_date' => $value['signature_date'],
                    'is_signature' => $value['is_signature'],
                    'cross_signature' => $value['cross_signature'],
                    'signature' => $employeeRecords['d_sign'],
                    'name' => $employeeRecords['name_bng'],
                    'userInfo' => $userInfo['username'],
                );
            }
        }

        return $returnData;
    }

	public function potroPageAll($part_id, $nothi_office = 0)
	{
		$this->layout = null;
		$employee_office = $this->getCurrentDakSection();
		$otherNothi = false;
		if ($nothi_office != 0 && $nothi_office != $employee_office['office_id']) {
			$otherNothi = true;

			$this->switchOffice($nothi_office, 'MainNothiOffice');
		} else {
			$nothi_office = $employee_office['office_id'];
		}

		$this->set('otherNothi', $otherNothi);
		$this->set('nothi_office', $nothi_office);

		$nothiMasterPermissionsTable = TableRegistry::get("ArchiveNothiMasterPermissions");
		$nothiMasterMovementsTable = TableRegistry::get("ArchiveNothiMasterMovements");

		TableRegistry::remove("ArchiveNothiParts");
		TableRegistry::remove("ArchiveNothiPotros");
		$nothiPartsTable = TableRegistry::get("ArchiveNothiParts");
		$nothiPotroTable = TableRegistry::get("ArchiveNothiPotros");

		$nothiMastersCurrentUser = array();
		$potroAttachmentRecord = array();

		$nothiPartsInformation = $nothiPartsTable->getAll(['ArchiveNothiParts.id' => $part_id])->first();
		//real nothi_master_id of this part
		$nothimastersid = $nothiPartsInformation['nothi_masters_id'];
		$this->set(compact('nothimastersid'));
		$this->set('part_id', $part_id);

		$allNothiMasterList = $nothiMasterPermissionsTable->getMasterNothiPartList($nothi_office,
			$employee_office['office_id'], $employee_office['office_unit_id'],
			$employee_office['office_unit_organogram_id'],
			$nothiPartsInformation['nothi_masters_id']);

		$nothiPartId = $draftSummary = $summaryAttachmentRecord
			= array();

		$this->set(compact('draftSummary'));
		$this->set(compact('summaryAttachmentRecord'));

		if (!empty($allNothiMasterList)) {
			foreach ($allNothiMasterList as $key => $value) {
				$nothiPartId[] = $value['nothi_part_no'];
			}
		}

		TableRegistry::remove('ArchiveNothiPotroAttachments');
		TableRegistry::remove('Potrojari');
		$potroAttachmentTable = TableRegistry::get('ArchiveNothiPotroAttachments');
		$potrojariTable = TableRegistry::get('Potrojari');


		$total = $potroAttachmentTable->find()->where(['nothi_part_no IN' => $nothiPartId,
			'nothijato' => 0, 'status' => 1])->count();

		$potroAttachmentRecord = $potroAttachmentTable->find()->where(['ArchiveNothiPotroAttachments.nothi_part_no IN' => $nothiPartId,
			'ArchiveNothiPotroAttachments.nothijato' => 0, 'ArchiveNothiPotroAttachments.status' => 1])->contain(['PotrojariData', 'ArchiveNothiPotros'])->order(['ArchiveNothiPotroAttachments.id desc'])->page(1,
			10)->toArray();

		$this->set(compact('potroAttachmentRecord'));
		$this->set(compact('total'));

		$potroNothijatoAttachmentRecord = $potroAttachmentTable->find()->where(['nothi_part_no IN' => $nothiPartId,
			'is_summary_nothi' => 0, 'nothijato' => 1, 'status' => 1])->order(['id desc'])->toArray();

		$this->set(compact('potroNothijatoAttachmentRecord'));

		$draftPotro = $potrojariTable->find()->where(['potro_status' => 'Draft',
			'nothi_part_no IN' => $nothiPartId])->order(['id desc'])->toArray();

		TableRegistry::remove('PotrojariAttachments');
		$potrojariAttachmentsTable = TableRegistry::get('PotrojariAttachments');
		$potrojariAttachments = $potrojariAttachmentsTable->find()->select(['attachment_type',
			'file_name', 'potrojari_id', 'id'])->join(['Potrojari' => ['table' => 'potrojari',
			'type' => 'inner', 'conditions' => 'Potrojari.id = PotrojariAttachments.potrojari_id']])->where(['Potrojari.potro_status' => 'Draft',
			'Potrojari.nothi_part_no IN' => $nothiPartId, 'PotrojariAttachments.attachment_type <> ' => 'text'])->order(['Potrojari.id desc'])->toArray();


		$draftPotroAttachments = array();
		if (!empty($potrojariAttachments)) {
			foreach ($potrojariAttachments as $ke => $val) {
				$draftPotroAttachments[$val['potrojari_id']][] = $val;
			}
		}

		$this->set(compact('draftPotro', 'draftPotroAttachments'));

		$this->set('potroFlags', $this->showPotroFlagAll($nothiPartId));
		$this->set('nothi_office', $nothi_office);
	}

	private function showPotroFlagAll($nothimasterid)
	{

		$this->set('id', $nothimasterid);
		TableRegistry::remove('ArchivePotroFlags');
		$bookmarkedTable = TableRegistry::get('ArchivePotroFlags');

		$bookmarkedTablequery = $bookmarkedTable->find()
			->select([
				'ArchivePotroFlags.nothi_master_id',
				'ArchivePotroFlags.nothi_part_no',
				'ArchivePotroFlags.color',
				'ArchivePotroFlags.title',
				'ArchivePotroFlags.page_no',
				'ArchivePotroFlags.potro_attachment_id',
				"nothi_potro_page_bn" => 'ArchiveNothiPotroAttachments.nothi_potro_page_bn',
				"nothi_potro_page" => 'ArchiveNothiPotroAttachments.nothi_potro_page'
			])
			->where(["ArchivePotroFlags.nothi_part_no IN" => $nothimasterid])
			->join([
				'ArchiveNothiPotroAttachments' => [
					'table' => 'archive_nothi_potro_attachments',
					'type' => 'inner',
					'conditions' => "ArchiveNothiPotroAttachments.id = ArchivePotroFlags.potro_attachment_id"
				]
			])
			->toArray();

		$returnData = array();

		if (!empty($bookmarkedTablequery)) {
			foreach ($bookmarkedTablequery as $key => $row) {
				$returnData[] = array(
					'class' => $row['color'],
					'attachment_id' => $row['potro_attachment_id'],
					'potro_no' => $row['nothi_potro_page'],
					'potro_no_bn' => $row['nothi_potro_page_bn'],
					'nothi_master_id' => $row['nothi_master_id'],
					'nothi_part_id' => $row['nothi_part_no'],
					'title' => $row['title'],
					'page' => $row['page_no']
				);
			}
		}

		return $returnData;
	}

	public function nextAllPotroPage($nothiMasterId = 0, $page = 1, $search = 0)
	{
		$this->layout = null;
		if ($this->request->is('ajax')) {
			$nothi_office = intval($this->request->data['nothi_office']);
			$employee_office = $this->getCurrentDakSection();
			$otherNothi = false;
			if ($nothi_office != 0 && $nothi_office != $employee_office['office_id']) {
				$otherNothi = true;

				$this->switchOffice($nothi_office, 'MainNothiOffice');
			} else {
				$nothi_office = $employee_office['office_id'];
			}

			TableRegistry::remove("ArchiveNothiParts");
			$nothiPartsTable = TableRegistry::get("ArchiveNothiParts");
			$nothiPartsInformation = $nothiPartsTable->getAll(['ArchiveNothiParts.id' => $nothiMasterId])->first();

			$nothiMasterPermissionsTable = TableRegistry::get("ArchiveNothiMasterPermissions");

			$allNothiMasterList = $nothiMasterPermissionsTable->getMasterNothiPartList($nothi_office,
				$employee_office['office_id'], $employee_office['office_unit_id'],
				$employee_office['office_unit_organogram_id'],
				$nothiPartsInformation['nothi_masters_id']);

			$nothiPartId = $draftSummary = $summaryAttachmentRecord
				= array();

			if (!empty($allNothiMasterList)) {
				foreach ($allNothiMasterList as $key => $value) {
					$nothiPartId[] = $value['nothi_part_no'];
				}
			}

			TableRegistry::remove('ArchiveNothiPotroAttachments');
			$potroAttachmentTable = TableRegistry::get('ArchiveNothiPotroAttachments');

			$potroAttachmentRecord = $potroAttachmentTable->find()->where(['ArchiveNothiPotroAttachments.nothi_part_no IN' => $nothiPartId,
				'ArchiveNothiPotroAttachments.is_summary_nothi' => 0, 'ArchiveNothiPotroAttachments.nothijato' => 0, 'ArchiveNothiPotroAttachments.status' => 1])
				->contain(['PotrojariData', 'ArchiveNothiPotros']);

			$limit = 10;
			if ($search) {
				$page = 1;

				$searchData = !empty($this->request->data['q']) ? h($this->request->data['q']) : '';
				if (!empty($searchData)) {
					$potroAttachmentRecord->join([
						'ArchiveNothiPotros' => [
							'table' => 'archive_nothi_potros',
							'type' => 'INNER',
							'conditions' => 'ArchiveNothiPotros.id = ArchiveNothiPotroAttachments.nothi_potro_id'
						]
					]);
					$potroAttachmentRecord->where(["OR" => [
						'ArchiveNothiPotros.sarok_no' => $searchData,
						'ArchiveNothiPotros.subject LIKE' => "%{$searchData}%"
					]
					]);
				}

				if (!empty($searchData)) {
					$limit = 100;
				}
			}

			$potroAttachmentRecord = $potroAttachmentRecord->order(['ArchiveNothiPotroAttachments.id desc'])->page($page, $limit)->toArray();

			$html = '';
			$potroNo = '';
			if (!empty($potroAttachmentRecord)) {
				$i = 0;
				foreach ($potroAttachmentRecord as $row) {
					if ($i == 0) {
						$potroNo = $row['nothi_potro_page_bn'];
					}
					$html .= '<div id_en="' . $row['nothi_potro_page'] . '" id_bn="' . $row['nothi_potro_page_bn'] . '" class="attachmentsRecord ' . ($i
						== 0 ? 'active ' : ($i == (count($potroAttachmentRecord)
							- 1) ? 'last' : '')) . '">';

					$html .= '<div  style="background-color: #a94442; padding: 5px; color: #fff;"> <div class="pull-left" style="padding-top: 0px;">' .
						mb_substr($row['nothi_potro']['subject'], 0, 50) . (mb_strlen($row['nothi_potro']['subject']) > 50 ? '...' : '') . '</div>
    <div class="pull-right text-right"> ' .
						(($row['is_summary_nothi'] == 0 && $row['attachment_type'] == 'text' && !empty($row['potrojari_id']) && empty($row['potrojari_data']['attached_potro_id'])) ? (
							'<button type="button" class="btn   btn-sm btn-warning" onclick="selectNote(' . $row['potrojari_id'] . ')" data-toggle="tooltip" title="পত্রজারি ক্লোন করুন" >
                            <span class="glyphicon glyphicon-copy"></span></button> ') : '')
						. ($row['attachment_type'] == 'text' || substr($row['attachment_type'], 0, 5) == 'image' ? '<a data-id="' . $row['id'] . '" title="প্রিন্ট করুন" href="javascript:void(0)" class="btn    green btn-sm btn-print"><i class="fs1 a2i_gn_print2"></i></a> ' : '')
						. ($row['attachment_type'] != 'text' && substr($row['attachment_type'], 0, 5) != 'image' && $row['attachment_type'] != 'application/pdf' ? '<a onclick="'.Router::url(['_name'=>'downloadPotro',$nothi_office,$row['id']]).'" href="javascript:void(0)" class="btn green btn-sm btn-download" title="ডাউনলোড করুন"><i class="glyphicon glyphicon-download-alt"></i></a>' : '')
						. '</div><div  style="clear: both;" ></div>
</div>';

					if ($row['attachment_type'] == 'text') {
						if ($row['is_summary_nothi'] == 1) {
							$html .= '<div style="background-color: #fff; max-width:950px; min-height:815px;  margin:0 auto; page-break-inside: auto;">' . html_entity_decode($row['potro_cover']) . "<br/>" . html_entity_decode($row['content_body']) . "</div>";
						} else {
							$header = jsonA($row['meta_data']);
							$html .= (!empty($header['potro_header_banner']) ? ('<div style="text-align: ' . ($header['banner_position']) . '!important;"><img src="' . $this->request->webroot . 'getContent?file=' . base64_decode($header['potro_header_banner']) . '&token=' . sGenerateToken(['file' => base64_decode($header['potro_header_banner'])], ['exp' => time() + 60 * 300]) . '" style="width:' . ($header['banner_width']) . ';height: 60px;max-width: 100%;"/></div>') : '') . $row['content_body'];
						}
					} else if (substr($row['attachment_type'], 0, 5) != 'image') {
						if (substr($row['attachment_type'], 0, strlen('application/vnd')) == 'application/vnd' || substr($row['attachment_type'], 0, strlen('application/ms')) == 'application/ms') {
							$url = urlencode($this->request->webroot . 'getContent?file=' . $row['file_name'] . '&token=' . sGenerateToken(['file' => $row['file_name']], ['exp' => time() + 60 * 300]));
							$html .= '<iframe style=" width:100%; height: 700px;" src="https://docs.google.com/gview?url=' . $url . '&embedded=true" style="width:600px; height:500px;" frameborder="0"></iframe>';
						} else {
							$html .= '<embed src="' . $this->request->webroot . 'getContent?file=' . $row['file_name'] . '&token=' . sGenerateToken(['file' => $row['file_name']], ['exp' => time() + 60 * 300]) . '" style=" width:100%; height: 700px;" type="' . $row['attachment_type'] . '"></embed>';
						}
					} else {
						$html .= '<div class="text-center"><a href="' . $this->request->webroot . 'content/' . $row['file_name'] . '?token=' . sGenerateToken(['file' => $row['file_name']], ['exp' => time() + 60 * 300]) . '" onclick="return false;" data-title="পত্র নম্বর:  ' . $row['nothi_potro_page_bn'] . '" data-footer="" ><img class="zoomimg img-responsive"  src="' . $this->request->webroot . 'content/' . $row['file_name'] . '?token=' . sGenerateToken(['file' => $row['file_name']], ['exp' => time() + 60 * 300]) . '" alt="ছবি পাওয়া যায়নি"></a></div>';
					}

					$html .= "</div>";
					$i++;
				}
			}

			echo json_encode(array('html' => $html, 'potrono' => $potroNo));
			die;
		}
	}
}