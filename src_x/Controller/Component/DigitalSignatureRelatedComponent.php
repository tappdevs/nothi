<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use ReflectionClass;
use ReflectionMethod;
use Cake\ORM\TableRegistry;
use Cake\Cache\Cache;

class DigitalSignatureRelatedComponent extends Component
{
    public function getCurrentDakSection()
    {
        $session = $this->request->session();
        return $session->read('selected_office_section');
    }
    public function verifyDigitalSignature($sign_type = 1,$token = ''){
        $employee_office = $this->getCurrentDakSection();
        $data['userId'] = $employee_office['cert_id'];
        $data['userIdtype'] = $employee_office['cert_type'];
        $data['caName'] = $employee_office['cert_provider'];
        $data['certSerialNumber'] = $employee_office['cert_serial'];
        $data['passPhrase'] = ($sign_type == 1)?$token:'';
//        $tbl_DS = ClassRegistry::init('DigitalSignature');
        $tbl_DS = TableRegistry::get('DigitalSignature');
        $signables = [];
        array_push($signables,new \NonIntrinsicSignableObject( time(), $tbl_DS->encodeObject('Digital Signature','text'), null));
        $response =  $tbl_DS->requestToSign($data,$signables);
        if(!empty($response)){
            return $response;
        }
    }
    /**
     *
     * @param type $allNotes
     * @param type $soft_token
     * @param type $nothi_part_no
     * @return type
     */
    public function signOnucched($allNotes,$soft_token,$nothi_part_no = 0,$sign_type = 1,$employee_office = [],$nothi_office = 0){
        if(empty($employee_office)){
            $employee_office = $this->getCurrentDakSection();
        }

        $tbl_DS = TableRegistry::get('DigitalSignature');
        TableRegistry::remove('NothiNotes');
        TableRegistry::remove('NothiNoteSignatures');
        $tbl_NothiNotes = TableRegistry::get('NothiNotes');
        $tbl_NothiNoteSignatures = TableRegistry::get('NothiNoteSignatures');
        $data = [];
        $note_ids = [];
        $signables = [];
        $response = [];
        try{
            //check if soft token dummy provided
            $soft_token = $this->getSoftToken($soft_token,$employee_office);

            $data['userId'] = $employee_office['cert_id'];
            $data['userIdtype'] = $employee_office['cert_type'];
            $data['caName'] = $employee_office['cert_provider'];
            $data['certSerialNumber'] = $employee_office['cert_serial'];
            $data['passPhrase'] =($sign_type == 1)? $soft_token: '';
            if(!empty($allNotes)){
                foreach($allNotes as $n_id => $n_body){
                   $signable = new \NonIntrinsicSignableObject( 'nothi_part_'.$nothi_part_no.'_note_'.$n_id.'_emp_'. $employee_office['officer_id'].'_'.rand(1,1000), $tbl_DS->encodeObject($n_body,'html'), null);
                    array_push($signables, $signable);
                }
                if(!empty($signables)){
                   $response =  $tbl_DS->requestToSign($data,$signables);
                }
            }else{
                //user did not create any onucched
                $last_note = $tbl_NothiNotes->find()->select(['id'])->where(['nothi_part_no' => $nothi_part_no])->order(['created desc'])->first();
                if(!empty($last_note['id'])){
                    $signable = new \NonIntrinsicSignableObject( 'nothi_part_'.$nothi_part_no.'_note_'.$last_note['id'].'_emp_'. $employee_office['officer_id'].'_'.rand(1,1000), $tbl_DS->encodeObject($last_note['note_description'],'html'), null);
                    array_push($signables, $signable);
                    $response =  $tbl_DS->requestToSign($data,$signables);
                }
            }
            if(!empty($response) && $response['status'] == 'error'){
                return $response;
            }
            if(!empty($response) && $response['status'] == 'success'){
                $response_data = $response['data'];
                if(!empty($response_data->signedObjects)){
                    $data_to_save = [];
                    foreach($response_data->signedObjects as $signObj){
                        $trans_info = explode('_', $signObj->transactionId);
                        $id = (!empty($trans_info[4]))?$trans_info[4]:0;
                        if(empty($id))
                            continue;
                        $data_to_save = $signObj;
                        $data_to_save->publicKey = $response_data->publicKey;
                        $data_to_save->issuer_id = $employee_office['officer_id'];
                        $tbl_NothiNotes->updateAll(['digital_sign' => 1,'sign_info' => json_encode($data_to_save)], ['id' => $id]);
                        $note_ids[] = $id;
//                               $tbl_NothiNoteSignatures->updateAll(['digital_sign' => 1], ['digital_sign' => 0,'nothi_note_id' => $id,'office_organogram_id' => $employee_office['office_unit_organogram_id'],'cross_signature' => 0,'nothi_part_no' => $nothi_part_no]);
                    }
                }
                //save soft Token
                $tbl_DS->saveSoftToken($employee_office,$soft_token);

                $attachementSign =  $this->signOnucchedAttachment($note_ids,$soft_token,$nothi_part_no,$employee_office,0,$sign_type,$nothi_office);
                if(!empty($attachementSign) && $attachementSign['status'] == 'error'){
                    return $attachementSign;
                }
            }else{
                return $response;
            }
        } catch (\Exception $ex) {
            return ['status' => 'error','msg' => $ex->getMessage()];
        }
         return ['status' => 'success','note_ids' => $note_ids];
    }
    /**
     *
     * @param type $noteIds
     * @param type $soft_token
     * @param type $nothi_part_no
     * @return type
     */
    private function signOnucchedAttachment($noteIds = [],$soft_token,$nothi_part_no = 0,$employee_office = [],$shell = 1,$sign_type = 1,$nothi_office = 0){
        if($shell){
            if(!empty($noteIds)){
                $keystore = 'signOnucchedAttachment_'.$nothi_part_no.'_'.$employee_office['office_unit_organogram_id'].'_'.rand(1,1000);
                    Cache::write($keystore, $noteIds, 'memcached');
            }
             $command =ROOT. "/bin/cake DigitalSignature signOnucchedAttachment {$keystore} {$soft_token} {$nothi_part_no} {$employee_office['office_unit_organogram_id']} {$sign_type} {$nothi_office}> /dev/null 2>&1 &";
             exec($command);
             return ['status' => 'success','msg' => ''];
        }
        if(empty($employee_office)){
            $employee_office = $this->getCurrentDakSection();
        }
        $tbl_DS = TableRegistry::get('DigitalSignature');
        TableRegistry::remove('NothiNoteAttachments');
        $tbl_NothiNoteAttachments = TableRegistry::get('NothiNoteAttachments');
        $data = [];
        try{
            if(!empty($noteIds)){
                $data['userId'] = $employee_office['cert_id'];
                $data['userIdtype'] = $employee_office['cert_type'];
                $data['caName'] = $employee_office['cert_provider'];
                $data['certSerialNumber'] = $employee_office['cert_serial'];
                $data['passPhrase'] =($sign_type == 1)? $soft_token: '';
                $signables = [];
                $allNoteAttachments = $tbl_NothiNoteAttachments->getAllAttachments($nothi_part_no,$noteIds,['id','attachment_type','file_name','file_dir'])->toArray();
                if(!empty($allNoteAttachments)){
                      foreach($allNoteAttachments as $att_info){
                          if($att_info['attachment_type'] == 'text'){
                               $data_type = 'text';
                          }else if($att_info['attachment_type'] == 'html'){
                              $data_type = 'html';
                          }
                          else if(substr($att_info['attachment_type'],0, 5) == 'image'){
                              $data_type = 'image';
                          }else{
                              $data_type = '';//other Intrinsic file
                          }
                          if(empty($data_type)){
                              $path_parts = pathinfo(FILE_FOLDER_DIR.$att_info['file_name']);
                               $signable = new \IntrinsicSignableObject( 'nothi_part_'.$nothi_part_no.'_attachment_'.$att_info['id'].'_emp_'. $employee_office['officer_id'].'_'.rand(1,1000), $tbl_DS->encodeObject(FILE_FOLDER_DIR.$att_info['file_name'],$data_type),FILE_FOLDER_DIR.$att_info['file_name'] , $path_parts['extension'],null);
                          }else{
                              $signable = new \NonIntrinsicSignableObject( 'nothi_part_'.$nothi_part_no.'_attachment_'.$att_info['id'].'_emp_'. $employee_office['officer_id'].'_'.rand(1,1000), $tbl_DS->encodeObject(FILE_FOLDER_DIR.$att_info['file_name'],$data_type), null);
                          }
                             array_push($signables, $signable);
                    }
                }
                if(!empty($signables)){
                   $response =  $tbl_DS->requestToSign($data,$signables);
                   if(!empty($response) && $response['status'] == 'success'){
                       $response_data = $response['data'];
                       if(!empty($response_data->signedObjects)){
                           foreach($response_data->signedObjects as $signObj){
                               $trans_info = explode('_', $signObj->transactionId);
                               $id = (!empty($trans_info[4]))?$trans_info[4]:0;
                               $data_to_save = [];
                               if(empty($id))
                                   continue;
                               if(empty($signObj->intrinsicFileName)){
                                   $data_to_save = $signObj;
                                   $data_to_save->issuer_id = $employee_office['officer_id'];
                                   $data_to_save->publicKey = $response_data->publicKey;
                               }else{
                                   $data_to_save['issuer_id'] = $employee_office['officer_id'];
                                   $data_to_save['publicKey'] = $response_data->publicKey;
                               }
                               $file_info = $tbl_NothiNoteAttachments->get($id);
                               $file_info->digital_sign= 1;
                               $file_info->sign_info = json_encode($data_to_save);
                               $tbl_NothiNoteAttachments->save($file_info);
                               if(!empty($signObj->intrinsicFileName)){
                                    $ifp = fopen($signObj->intrinsicFileName, 'wb');
                                    fwrite($ifp, base64_decode($signObj->intrinsicFileContentEncodedAsBase64));
                                    fclose($ifp);
                               }
                           }
                       }
                   }else{
                       return $response;
                   }
                }
            }
        } catch (\Exception $ex) {
            return ['status' => 'error','msg' => $ex->getMessage()];
        }
         return ['status' => 'success','msg' => ''];
    }

    /**
     *
     * @param type $potro_id
     * @param type $soft_token
     * @param type $signature_type
     * @return type
     */
    public function signPotrojari($potro_id, $soft_token, $signature_type = 1,$current_office_selection = []){
        if(!empty($current_office_selection)){
            $employee_office = $current_office_selection;
        }else{
            $employee_office = $this->getCurrentDakSection();
        }
        $tbl_DS = TableRegistry::get('DigitalSignature');
        TableRegistry::remove('Potrojari');
        $tbl_Potrojari = TableRegistry::get('Potrojari');
        $data = [];
        try{
            //check if sodt token dummy provided
            $soft_token = $this->getSoftToken($soft_token,$employee_office);

            if(!empty($potro_id)){
                $data['userId'] = $employee_office['cert_id'];
                $data['userIdtype'] = $employee_office['cert_type'];
                $data['caName'] = $employee_office['cert_provider'];
                $data['certSerialNumber'] = $employee_office['cert_serial'];
                $data['passPhrase'] = ($signature_type == 1)? $soft_token: '';
                $signables = [];
                $potro_info = $tbl_Potrojari->get($potro_id);
                if(!empty($potro_info)){
                    $potro_body = (!empty($potro_info->potro_cover)?$potro_info->potro_cover.'<br>':'').(!empty($potro_info->attached_potro)?$potro_info->attached_potro.'<br>':''). $potro_info->potro_description;
                    $signable = new \NonIntrinsicSignableObject( 'nothi_potrojari_'.$potro_info['id'].'_emp_'. $employee_office['officer_id'].'_'.rand(1,1000), $tbl_DS->encodeObject($potro_body,'html'), null);
                    array_push($signables, $signable);
                }
                if(!empty($signables)){
                   $response =  $tbl_DS->requestToSign($data,$signables);
                   if(!empty($response) && $response['status'] == 'success'){
                       $response_data = $response['data'];
                       if(!empty($response_data->signedObjects)){
                           $data_to_save = [];
                           foreach($response_data->signedObjects as $signObj){
//                               $trans_info = explode('_', $signObj->transactionId);
//                               $id = (!empty($trans_info[2]))?$trans_info[2]:0;
//                               if(empty($id))
//                                   continue;
                               $data_to_save = $signObj;
                               $data_to_save->publicKey = $response_data->publicKey;
                               $data_to_save->issuer_id = $employee_office['officer_id'];
                               $tbl_Potrojari->updateAll(['digital_sign' => 1,'sign_info' => json_encode($data_to_save)], ['id' => $potro_id]);
                           }
                       }
                       //save soft Token
                       $tbl_DS->saveSoftToken($employee_office,$soft_token);

                    $attachementSign =  $this->signPotrojariAttachment($potro_id,$soft_token,$signature_type,$employee_office,1);
                    if(!empty($attachementSign) && $attachementSign['status'] == 'error'){
                        return $attachementSign;
                    }
                   }else{
                       return $response;
                   }
                }
            }
        } catch (\Exception $ex) {
            return ['status' => 'error','msg' => 'দুঃখিত টেকনিক্যাল ত্রুটি হয়েছে। '.($ex->getMessage()),'reason' => makeEncryptedData($ex->getMessage())];
        }
         return ['status' => 'success'];
    }
    /**
     *
     * @param type $noteIds
     * @param type $soft_token
     * @param type $nothi_part_no
     * @return type
     */
    public function signPotrojariAttachment($potro_id,$soft_token,$signature_type = 1,$employee_office = [],$shell = 1){
        if($shell){
            if(empty($potro_id)){
               return;
            }
             $command =ROOT. "/bin/cake DigitalSignature signPotrojariAttachment {$potro_id} {$soft_token} {$signature_type} {$employee_office['office_unit_organogram_id']}> /dev/null 2>&1 &";
             exec($command);
            return;
        }
        $employee_office = $this->getCurrentDakSection();
        $tbl_DS = TableRegistry::get('DigitalSignature');
        TableRegistry::remove('PotrojariAttachments');
        $tbl_PotrojariAttachments = TableRegistry::get('PotrojariAttachments');
        $data = [];
        try{
            if(!empty($potro_id)){
                $data['userId'] = $employee_office['cert_id'];
                $data['userIdtype'] = $employee_office['cert_type'];
                $data['caName'] = $employee_office['cert_provider'];
                $data['certSerialNumber'] = $employee_office['cert_serial'];
                $data['passPhrase'] = ($signature_type == 1)? $soft_token: '';
                $signables = [];
                $allPotrojariAttachments = $tbl_PotrojariAttachments->getAllAttachments($potro_id,[],['id','attachment_type','file_name','file_dir'])->toArray();
                if(!empty($allPotrojariAttachments)){
                      foreach($allPotrojariAttachments as $att_info){
                          if($att_info['attachment_type'] == 'text'){
                               $data_type = 'text';
                          }else if($att_info['attachment_type'] == 'html'){
                              $data_type = 'html';
                          }
                          else if(substr($att_info['attachment_type'],0, 5) == 'image'){
                              $data_type = 'image';
                          }else{
                              $data_type = '';//other Intrinsic file
                          }
                          if(empty($data_type)){
                              $path_parts = pathinfo(FILE_FOLDER_DIR.$att_info['file_name']);
                               $signable = new \IntrinsicSignableObject( 'nothi_potrojari_'.$potro_id.'_attachment_'.$att_info['id'].'_emp_'. $employee_office['officer_id'].'_'.rand(1,1000), $tbl_DS->encodeObject(FILE_FOLDER_DIR.$att_info['file_name'],$data_type),FILE_FOLDER_DIR.$att_info['file_name'] , $path_parts['extension'],null);
                          }else{
                              $signable = new \NonIntrinsicSignableObject( 'nothi_potrojari_'.$potro_id.'_attachment_'.$att_info['id'].'_emp_'. $employee_office['officer_id'].'_'.rand(1,1000), $tbl_DS->encodeObject(FILE_FOLDER_DIR.$att_info['file_name'],$data_type), null);
                          }
                             array_push($signables, $signable);
                    }
                }
                if(!empty($signables)){
                   $response =  $tbl_DS->requestToSign($data,$signables);
                   
                   if(!empty($response) && $response['status'] == 'success'){
                       $response_data = $response['data'];
                       if(!empty($response_data->signedObjects)){
                           foreach($response_data->signedObjects as $signObj){
                               $data_to_save = [];

                               $trans_info = explode('_', $signObj->transactionId);
                               $id = (!empty($trans_info[4]))?$trans_info[4]:0;
                               if(empty($id))
                                   continue;
                               if(empty($signObj->intrinsicFileName)){
                                   $data_to_save = $signObj;
                                   $data_to_save->issuer_id = $employee_office['officer_id'];
                                   $data_to_save->publicKey = $response_data->publicKey;
                               }else{
                                   $data_to_save['issuer_id'] = $employee_office['officer_id'];
                                   $data_to_save['publicKey'] = $response_data->publicKey;
                               }
                              
                               $file_info = $tbl_PotrojariAttachments->get($id);
                               $file_info->digital_sign= 1;
                               $file_info->sign_info = json_encode($data_to_save);
                               $tbl_PotrojariAttachments->save($file_info);
                                   if(!empty($signObj->intrinsicFileName)){
                                    $ifp = fopen($signObj->intrinsicFileName, 'wb');
                                    fwrite($ifp, base64_decode($signObj->intrinsicFileContentEncodedAsBase64));
                                    fclose($ifp);
                               }
                           }
                       }
                   }else{
                       return $response;
                   }
                }
            }
        } catch (\Exception $ex) {
            return ['status' => 'error','msg' => $ex->getMessage()];
        }
         return ['status' => 'success','msg' => ''];
    }
    public function getSoftToken($soft_token = '',$employee_office){
        if(empty($soft_token)){
            return '';
        }
        // check format
        $is_dummy =preg_match('/(st-\d{1,3}-[A-Z0-9]{1,3}-[A-Z0-9]{1,3})/',$soft_token,$matches);
        if($is_dummy){
            //need to find real soft token
            $tableDsUserInfo = TableRegistry::get('DsUserInfo');
            $has_data = $tableDsUserInfo->getData(['soft_token'],[
                'employee_record_id' => $employee_office['officer_id'],
                'expired_at >=' => date('Y-m-d H:i:s')
            ])->first();
            if(!empty($has_data['soft_token'])){
                return base64_decode($has_data['soft_token']);
            }
        }
        return $soft_token;
    }
}