<?php
    namespace App\Controller\Component;

    require_once(ROOT . DS . 'vendor' . DS . "qrgenerator" . DS . "lib". DS . "qrlib.php");
    use Cake\Controller\Component;
    use QRcode;
    
    class QRGeneratorComponent extends Component
    {
        /**
         * @param $data
         * @param array $options
         *              - @file_path - if needed custom path
         *              - @errorCorrectionLevel, @matrixPointSize
         *              - @format - png,base64String (only base64 string) ,base64Image (base64 image src link), all for all previous options
         * @return array - if @error - status = error and message
         *                  @success = status = success required path or string
         */
        public function generateQRcode($data,$options = [])
        {
            $return_data = ['status' => 'error'];

            try{
                if(!empty($options['file_path'])){
                    $path_dir =FILE_FOLDER_DIR. $options['file_path'];
                }else{
                    $path_dir =FILE_FOLDER_DIR.'qr-codes';
                }
                if(is_array($data)){
                    $data_2_write = '';
                    foreach ($data as $index => $row){
                        if(!is_numeric($index)){
                            $index = str_replace('-',' ',$index);
                            $index = str_replace('_',' ',$index);
                            $data_2_write .= ($index.': '. $row);
                        }else{
                            $data_2_write .= $row;
                        }
                        $data_2_write .= "\n\n";
                    }
                    $data = nl2br($data_2_write);
                }

                $filename 				= 	QRcode::generateFileName($path_dir);
                $errorCorrectionLevel 	= 	isset($options['errorCorrectionLevel'])?$options['errorCorrectionLevel']:'H';
                $matrixPointSize 		= 	isset($options['matrixPointSize'])?$options['matrixPointSize']:10;

//                if((!empty($options['format']) && $options['format'] == 'png') || (!empty($options['format']) && $options['format'] == 'all')){
                    $return_data['file_path'] 	= 	QRcode::png($data, $filename, $errorCorrectionLevel, $matrixPointSize, 1);

                    $file_path_new = explode(FILE_FOLDER_DIR, $return_data['file_path']);
                    $return_data['file_url'] = $this->request->webroot.$file_path_new[1];
//                }
                if((!empty($options['format']) && $options['format'] == 'base64String') || (!empty($options['format']) && $options['format'] == 'all')){
                    $return_data['base64String'] = 	QRcode::genBase64String($filename);
                }
                if((!empty($options['format']) && $options['format'] == 'base64Image')  || (!empty($options['format']) && $options['format'] == 'all')){
                    $return_data['base64Image']  = 	QRcode::genBase64Src($filename);
                }
                $return_data['status'] = 'success';
            }catch (\Exception $ex){
                $return_data['message'] =$ex->getMessage();
            }

            return $return_data;
        }
    }

?>