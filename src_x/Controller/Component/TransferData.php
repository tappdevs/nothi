<?php

namespace App\Controller\Component;

use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;


/**
 * UploadFile component
 */
trait TransferData
{
    public $selectedUnit = null;
    public $selectedDesignation = null;
    private $officeInformation = null;
    private $unitInformation = null;
    private $transferFromOffice = null;
    private $transferUnit = null;
    private $employeeOffice = null;
    private $assign = null;
    private $assignedUserDetail = null;
    private $will_move = true;
    private $authUserId = null;

    public function initiateTransfer()
    {
    	if ($this->authUserId == null) {
    		$this->authUserId = $this->Auth->user('id');
		}
		//$this->out('Process started');
        return $this->createOriginOrganogram();
    }

    private function createOriginUnit()
    {

        $table_instance_unit_origin = TableRegistry::get('OfficeOriginUnits');

        $originUnit = $table_instance_unit_origin->newEntity();

        $originUnit->office_ministry_id = $this->officeInformation->office_ministry_id;
        $originUnit->office_layer_id = $this->officeInformation->office_layer_id;
        $originUnit->office_origin_id = $this->officeInformation->office_origin_id;
        $originUnit->unit_name_bng = $this->unitInformation['unit_name_bng'];
        $originUnit->unit_name_eng = $this->unitInformation['unit_name_eng'];
        $originUnit->office_unit_category = $this->unitInformation['office_unit_category'];
        $originUnit->parent_unit_id = 0;
        $originUnit->unit_level = $this->unitInformation['unit_level'];
        $originUnit->active_status = 1;
        $originUnit->created_by = $this->authUserId;
        $originUnit->modified_by = $this->authUserId;
        $originUnit->created = date("Y-m-d H:i:s");
        $originUnit->modified = date("Y-m-d H:i:s");

        $table_instance_unit_origin->save($originUnit);

        return $this->createUnit($originUnit);

    }

    private function createUnit($originUnits)
    {
        $table_instance_unit = TableRegistry::get('OfficeUnits');
        $unit = $table_instance_unit->newEntity();
        $unit->office_ministry_id = $originUnits['office_ministry_id'];
        $unit->office_layer_id = $originUnits['office_layer_id'];
        $unit->office_id = $this->officeInformation['id'];
        $unit->office_origin_unit_id = $originUnits['id'];
        $unit->unit_name_bng = $originUnits['unit_name_bng'];
        $unit->unit_name_eng = $originUnits['unit_name_eng'];
        $unit->office_unit_category = $originUnits['office_unit_category'];
        $unit->parent_unit_id = 0;
        $unit->parent_origin_unit_id = 0;
        $unit->unit_nothi_code = $this->unitInformation['unit_nothi_code'];
        $unit->unit_level = $this->unitInformation['unit_level'];
        $unit->sarok_no_start = $this->unitInformation['sarok_no_start'];
        $unit->email = $this->unitInformation['email'];
        $unit->phone = $this->unitInformation['phone'];
        $unit->fax = $this->unitInformation['fax'];
        $unit->active_status = 1;
        $unit->created_by = $this->authUserId;
        $unit->modified_by = $this->authUserId;
        $unit->created = date("Y-m-d H:i:s");
        $unit->modified = date("Y-m-d H:i:s");

        $table_instance_unit->save($unit);

        return $unit;
    }

    private function createOriginOrganogram()
    {

        $table_instance_unit_origin_org = TableRegistry::get('OfficeOriginUnitOrganograms');

        $originDesignations = $table_instance_unit_origin_org->find()
            ->where([
                'office_origin_unit_id' => $this->unitInformation->office_origin_unit_id,
                'designation_bng' => $this->selectedDesignation['designation_bng']
            ])->order(['status' => 'desc', 'id' => 'desc'])->first();


        if (empty($originDesignations)) {
			$originDesignations = $table_instance_unit_origin_org->newEntity();

			$originDesignations->office_origin_unit_id = $this->unitInformation->office_origin_unit_id;
			$originDesignations->superior_unit_id = 0;
			$originDesignations->superior_designation_id = 0;
			$originDesignations->designation_bng = $this->selectedDesignation['designation_bng'];
			$originDesignations->short_name_bng = $this->selectedDesignation['designation_bng'];
			$originDesignations->designation_eng = $this->selectedDesignation['designation_eng'];
			$originDesignations->short_name_eng = $this->selectedDesignation['designation_eng'];
			$originDesignations->designation_level = !empty($this->selectedDesignation['designation_level']) ? $this->selectedDesignation['designation_level'] : 0;
			$originDesignations->designation_sequence = !empty($this->selectedDesignation['designation_sequence']) ? $this->selectedDesignation['designation_sequence'] : 0;
			$originDesignations->status = 1;
			$originDesignations->created_by = $this->authUserId;
			$originDesignations->modified_by = $this->authUserId;
			$originDesignations->created = date("Y-m-d H:i:s");
			$originDesignations->modified = date("Y-m-d H:i:s");

			$table_instance_unit_origin_org->save($originDesignations);
        } else {
            $table_instance_unit_origin_org->updateAll(['status' => 1], ['id' => $originDesignations['id']]);
			$originDesignations['status'] = 1;
        }
		//$this->out('Origin Organogram Created');
        return $this->createOrganogram($originDesignations);

    }

    private function createOrganogram($originDesignations)
    {

        $table_instance_org = TableRegistry::get('OfficeUnitOrganograms');
//        $designations = $table_instance_org->find()
//            ->where([
//                'office_id' => $this->employeeOffice['office_id'],
//                'office_unit_id' => $this->selectedUnit,
//                'ref_origin_unit_org_id' => $originDesignations['id']
//            ])->first();
//

//        if (empty($designations)) {
        $designations = $table_instance_org->newEntity();
        $designations->office_id = $this->employeeOffice['office_id'];
        $designations->office_unit_id = $this->selectedUnit;
        $designations->ref_origin_unit_org_id = $originDesignations['id'];
        $designations->superior_unit_id = $this->unitInformation->parent_unit_id;
        $designations->superior_designation_id = 0;
        $designations->ref_sup_origin_unit_id = 0;
        $designations->ref_sup_origin_unit_desig_id = 0;
        $designations->designation_bng = $originDesignations['designation_bng'];
        $designations->short_name_bng = $originDesignations['designation_bng'];
        $designations->designation_eng = $originDesignations['designation_eng'];
        $designations->short_name_eng = $originDesignations['designation_eng'];
        $designations->designation_sequence = $originDesignations['designation_sequence'];
        $designations->designation_level = $originDesignations['designation_level'];
        $designations->status = 1;
        $designations->is_admin = $this->selectedDesignation['is_admin'];
        $designations->created_by = $this->authUserId;
        $designations->modified_by = $this->authUserId;
        $designations->created = date("Y-m-d H:i:s");
        $designations->modified = date("Y-m-d H:i:s");

        $table_instance_org->save($designations);
//        } else {
//            $table_instance_org->updateAll(['status' => 1], ['id' => $designations['id']]);
//        }
		//$this->out('Organogram Created');
        if ($this->assignAllNewOrganogram($designations)) {
			$table_instance_org->updateAll(['status' => 0], ['id' => $this->selectedDesignation['id']]);
		} else {
            return false;
        }

        return true;
    }

    private function assignAllNewOrganogram($designations)
    {
        $tableEmployeeOffice = TableRegistry::get('EmployeeOffices');

        $requestedData = $tableEmployeeOffice->getAll([
            'status' => 1, 'office_unit_organogram_id' => $this->selectedDesignation['id']
        ])->first();

        if (!empty($requestedData)) {
            if ($tableEmployeeOffice->unAssignDesignation($requestedData['id'])) {
                $requestedData['office_id'] = $designations['office_id'];
                $requestedData['office_unit_id'] = $designations['office_unit_id'];
                $requestedData['office_unit_organogram_id'] = $designations['id'];
                $requestedData['joining_date'] = date("Y-m-d H:i:s");
                $assign = $tableEmployeeOffice->assignDesignation($requestedData, [
                    'office_unit_organogram_id' => $this->selectedDesignation['id'],
                ]);

                if ($assign) {
                    $this->assign = $assign;
					//$this->out('Unassign & Assign to new organogram');
                    return $this->movePendingData($designations);
                }
            } else {
                return false;
            }
        } else {
            return $this->movePendingData($designations);
            return true;
        }
        return false;
    }

    public function movePendingData($designations)
    {
		//$this->out('Move pending data...');
        $tableEmployeeRecord = TableRegistry::get('EmployeeRecords');
        if(!empty($this->assign['employee_record_id'])){
            $this->assignedUserDetail = $tableEmployeeRecord->get($this->assign['employee_record_id']);
        }

        $potrojariGroupsMigTable = TableRegistry::get('PotrojariGroupsUsers');
        $potrojariGroupsMigTable->updateAll([
            'office_id' => $designations['office_id'],
            'office_unit_id' => $designations['office_unit_id'],
            'office_unit_name_eng' => $this->unitInformation->unit_name_eng,
            'office_unit_name_bng' => $this->unitInformation->unit_name_bng,
            'office_unit_organogram_id' => $designations['id'],
            'office_unit_organogram_name_bng' => $designations['designation_bng'],
            'office_unit_organogram_name_eng' => $designations['designation_eng'],
        ], ['office_unit_organogram_id' => $this->selectedDesignation['id']]);

		//$this->out('Potrojari group updated');
        if($this->will_move) {
            try {
                $this->switchOffice($designations['office_id'], 'CurrentOffice');

                if ($this->moveSeal($designations)) {
                    if ($this->moveSummary($designations)) {
                        if ($this->moveAllDak($designations)) {
                            if ($this->moveAllNothiPermissionandCurrentUser($designations)) {
                                if ($this->moveProtikolpos($designations)) {

                                    return true;
                                }
                            }
                        }
                    }
                }
            } catch (\Exception $ex) {

                return false;
            }
        }else{
            if ($this->moveProtikolpos($designations,$this->officeInformation['id'])) {
				//$this->out('Protikolpo updated');
				if ($this->transferOfficeData($designations,$this->officeInformation['id'])) {
					return true;
				}
                return true;
            }
        }

        return false;
    }

    private function moveSeal($designations)
    {
        TableRegistry::remove('OfficeDesignationSeals');
        TableRegistry::remove('OfficeUnitSeals');

        $tableOfficeUnitSeals = TableRegistry::get('OfficeUnitSeals');

        $tableOfficeUnitSeals->updateAll([
            'office_unit_organogram_id' => $designations['id'],
            'office_unit_id' => $designations['office_unit_id'],
            'unit_name_bng' => $this->unitInformation->unit_name_bng,
            'unit_name_eng' => $this->unitInformation->unit_name_eng,
        ], [
            'office_unit_organogram_id' => $this->selectedDesignation['id'],
        ]);

        $tableOfficeDesignationSeals = TableRegistry::get('OfficeDesignationSeals');

        $tableOfficeDesignationSeals->updateAll([
            'designation_name_bng' => $designations['designation_bng'],
            'designation_name_eng' => $designations['designation_eng'],
            'office_unit_organogram_id' => $designations['id'],
            'office_unit_id' => $designations['office_unit_id'],
            'unit_name_bng' => $this->unitInformation->unit_name_bng,
            'unit_name_eng' => $this->unitInformation->unit_name_eng,
        ], [
            'office_unit_organogram_id' => $this->selectedDesignation['id'],
        ]);

        $tableOfficeDesignationSeals->updateAll([
            'seal_owner_designation_id' => $designations['id'],
            'seal_owner_unit_id' => $designations['office_unit_id']
        ], [
            'seal_owner_designation_id' => $this->selectedDesignation['id'],
        ]);

        return true;
    }

	private function transferOfficeData($designations, $office_id)
	{
		//$this->out('Start office data transfer...');
		$this->switchOffice($this->transferFromOffice, 'fromOfficeDb');
		TableRegistry::remove('OfficeUnitSeals');
		$tableOfficeUnitSeals = TableRegistry::get('OfficeUnitSeals');
		TableRegistry::remove('OfficeDesignationSeals');
		$tableOfficeDesignationSeals = TableRegistry::get('OfficeDesignationSeals');

		TableRegistry::remove('DakUsers');
		$DakUsersTable = TableRegistry::get('DakUsers');
		TableRegistry::remove('DakMovements');
		$DakMovementsTable = TableRegistry::get('DakMovements');
		TableRegistry::remove('DakAttachments');
		$DakAttachmentsTable = TableRegistry::get('DakAttachments');
		TableRegistry::remove('DakUserActions');
		$DakUserActionsTable = TableRegistry::get('DakUserActions');
		TableRegistry::remove('NothiDakPotroMaps');
		$NothiDakPotroMapsTable = TableRegistry::get('NothiDakPotroMaps');
		TableRegistry::remove('NothiMastersDakMap');
		$NothiMastersDakMapTable = TableRegistry::get('NothiMastersDakMap');

		TableRegistry::remove('NothiMasters');
		$NothiMastersTable = TableRegistry::get('NothiMasters');
		TableRegistry::remove('NothiTypes');
		$NothiTypesTable = TableRegistry::get('NothiTypes');
		TableRegistry::remove('NoteFlags');
		$NoteFlagsTable = TableRegistry::get('NoteFlags');
		TableRegistry::remove('NoteInitialize');
		$NoteInitializeTable = TableRegistry::get('NoteInitialize');
		TableRegistry::remove('NothiDataChangeHistory');
		$NothiDataChangeHistoryTable = TableRegistry::get('NothiDataChangeHistory');
		TableRegistry::remove('NothiMasterPermissions');
		$NothiMasterPermissionsTable = TableRegistry::get('NothiMasterPermissions');
		TableRegistry::remove('NothiMasterCurrentUsers');
		$NothiMasterCurrentUsersTable = TableRegistry::get('NothiMasterCurrentUsers');
		TableRegistry::remove('NothiMasterMovements');
		$NothiMasterMovementsTable = TableRegistry::get('NothiMasterMovements');
		TableRegistry::remove('NothiNoteSignatures');
		$NothiNoteSignaturesTable = TableRegistry::get('NothiNoteSignatures');
		TableRegistry::remove('NothiNoteSheets');
		$NothiNoteSheetsTable = TableRegistry::get('NothiNoteSheets');
		TableRegistry::remove('NothiNotes');
		$NothiNotesTable = TableRegistry::get('NothiNotes');
		TableRegistry::remove('NothiNoteAttachments');
		$NothiNoteAttachmentsTable = TableRegistry::get('NothiNoteAttachments');
		TableRegistry::remove('NothiNoteAttachmentRefs');
		$NothiNoteAttachmentRefsTable = TableRegistry::get('NothiNoteAttachmentRefs');
		TableRegistry::remove('NothiNotePermissions');
		$NothiNotePermissionsTable = TableRegistry::get('NothiNotePermissions');
		TableRegistry::remove('NothiPotros');
		$NothiPotrosTable = TableRegistry::get('NothiPotros');
		TableRegistry::remove('NothiPotroAttachments');
		$NothiPotroAttachmentsTable = TableRegistry::get('NothiPotroAttachments');
		TableRegistry::remove('OtherOfficeNothiMasterMovements');
		$OtherOfficeNothiMasterMovementsTable = TableRegistry::get('OtherOfficeNothiMasterMovements');
		TableRegistry::remove('PotroFlags');
		$PotroFlagsTable = TableRegistry::get('PotroFlags');
		TableRegistry::remove('NothiParts');
		$NothiPartsTable = TableRegistry::get('NothiParts');
		TableRegistry::remove('Potrojari');
		$PotrojariTable = TableRegistry::get('Potrojari');
		TableRegistry::remove('PotrojariAttachments');
		$PotrojariAttachmentsTable = TableRegistry::get('PotrojariAttachments');
		TableRegistry::remove('PotrojariVersions');
		$PotrojariVersionsTable = TableRegistry::get('PotrojariVersions');
		TableRegistry::remove('PotrojariReceiver');
		$PotrojariReceiverTable = TableRegistry::get('PotrojariReceiver');
		TableRegistry::remove('PotrojariOnulipi');
		$PotrojariOnulipiTable = TableRegistry::get('PotrojariOnulipi');
		TableRegistry::remove('PortalPublishes');
		$PortalPublishesTable = TableRegistry::get('PortalPublishes');

		$dak_list = [];
		$dak_attachment_ids = [];
		$dak_user_action_ids = [];
		$nothi_dak_potro_map_ids = [];
		$nothi_masters_dak_map_ids = [];
		$dak_user_ids = [];
		$dak_movement_ids = [];

		## Seal
		$table['OfficeUnitSeals'] = $tableOfficeUnitSeals->find()->where(['office_unit_organogram_id' => $this->selectedDesignation['id']])->toArray();
		$table['OfficeDesignationSeals'] = $tableOfficeDesignationSeals->find()->where(['office_unit_organogram_id' => $this->selectedDesignation['id']])->toArray();
		$table['office_designation_seal_owners'] = $tableOfficeDesignationSeals->find()->where(['seal_owner_designation_id' => $this->selectedDesignation['id']])->toArray();

		## Dak
		$table['DakUsers'] = $DakUsersTable->getData(['dak_category' => DAK_CATEGORY_INBOX, 'to_officer_designation_id' => $this->selectedDesignation['id'], 'is_archive' => 0])->toArray();
		if (!empty($table['DakUsers'])) {
			foreach ($table['DakUsers'] as $key => $dakUserDetails) {
				$dak_id = $dakUserDetails['dak_id'];
				$dak_type = $dakUserDetails['dak_type'];
				$dak_list[$dak_type][] = $dak_id;

				$dak_last_move = $DakMovementsTable->getDakLastMove($dak_id, $dak_type, ['attention_type' => 1]);
				if ($dak_last_move['to_officer_designation_id'] == $this->selectedDesignation['id'] && ($dak_last_move['operation_type'] == 'Forward' || $dak_last_move['operation_type'] == 'Sent')) {
					$dak_user_ids[] = $dakUserDetails['id'];
					$dak_movement_ids[] = $dak_last_move['id'];
				}
			}

			foreach($dak_list as $dak_type => $dak_list_typewise) {
				$dakAttachmentIds = $DakAttachmentsTable->find('list', ['keyField' => 'id', 'valueField' => 'id'])->where(['dak_type' => $dak_type, 'dak_id IN' => $dak_list_typewise])->toArray();
				$dak_attachment_ids = array_merge($dak_attachment_ids, $dakAttachmentIds);

				$dakUserActionIds = $DakUserActionsTable->find('list', ['keyField' => 'id', 'valueField' => 'id'])->where(['dak_type' => $dak_type, 'dak_id IN' => $dak_list_typewise])->toArray();
				$dak_user_action_ids = array_merge($dak_user_action_ids, $dakUserActionIds);

				$NothiDakPotroMapsIds = $NothiDakPotroMapsTable->find('list', ['keyField' => 'id', 'valueField' => 'id'])->where(['dak_type' => $dak_type, 'dak_id IN' => $dak_list_typewise])->toArray();
				$nothi_dak_potro_map_ids = array_merge($nothi_dak_potro_map_ids, $NothiDakPotroMapsIds);
			}

			$NothiMastersDakMapIds = $NothiMastersDakMapTable->find('list', ['keyField' => 'id', 'valueField' => 'id'])->where(['dak_movements_id IN' => $dak_movement_ids])->toArray();
			$nothi_masters_dak_map_ids = array_merge($nothi_masters_dak_map_ids, $NothiMastersDakMapIds);
		}
		$table['DakAttachments'] = $DakAttachmentsTable->find()->where(['id IN' => $dak_attachment_ids])->toArray();
		$table['DakDaptoriks'] = [];
		$table['DakNagoriks'] = [];
		if (isset($dak_list['Daptorik']) && count($dak_list['Daptorik']) > 0) {
			$table['DakDaptoriks'] = TableRegistry::get('DakDaptoriks')->find()->where(['id IN' => $dak_list['Daptorik']])->toArray();
		}
		$table['DakMovements'] = $DakMovementsTable->find()->where(['id IN' => $dak_movement_ids])->toArray();
		if (isset($dak_list['Nagorik']) && count($dak_list['Nagorik']) > 0) {
			$table['DakNagoriks'] = TableRegistry::get('DakNagoriks')->find()->where(['id IN' => $dak_list['Nagorik']])->toArray();
		}
		$table['DakUserActions'] = $DakUserActionsTable->find()->where(['id IN' => $dak_user_action_ids])->toArray();
		$table['NothiDakPotroMaps'] = $NothiDakPotroMapsTable->find()->where(['id IN' => $nothi_dak_potro_map_ids])->toArray();
		$table['NothiMastersDakMap'] = $NothiMastersDakMapTable->find()->where(['id IN' => $nothi_masters_dak_map_ids])->toArray();

		## NOTHI
		$nothi_master_ids = $NothiMastersTable->find('list', ['keyField' => 'id', 'valueField' => 'id'])->where(['office_units_organogram_id' => $this->selectedDesignation['id']])->toArray();
		$nothi_part_ids = $NothiPartsTable->find('list', ['keyField' => 'id', 'valueField' => 'id'])->where(['office_units_organogram_id' => $this->selectedDesignation['id'], 'office_units_id' => $this->selectedDesignation['previous_office_unit_id']])->toArray();
		$table['NothiTypes'] = $NothiTypesTable->find()->where(['office_id' => $this->transferFromOffice])->toArray();
		$table['NothiMasters'] = $NothiMastersTable->find()->where(['id IN' => $nothi_master_ids])->toArray();
		$table['NoteFlags'] = $NoteFlagsTable->find()->where(['office_organogram_id' => $this->selectedDesignation['id']])->toArray();
		$table['NoteInitialize'] = $NoteInitializeTable->find()->where(['office_units_organogram_id' => $this->selectedDesignation['id']])->toArray();
		$table['NothiDataChangeHistory'] = $NothiDataChangeHistoryTable->find()->where(['office_unit_organogram_id' => $this->selectedDesignation['id']])->toArray();
		$table['NothiMasterCurrentUsers'] = $NothiMasterCurrentUsersTable->find()->where(['office_unit_organogram_id' => $this->selectedDesignation['id']])->toArray();

		$table['NothiMasterMovements'] = $NothiMasterMovementsTable->find()->where(['to_office_unit_id' => $this->selectedDesignation['previous_office_unit_id']])->orWhere(['from_office_unit_id' => $this->selectedDesignation['previous_office_unit_id']])->toArray();
		$table['NothiNoteAttachments'] = $NothiNoteAttachmentsTable->find()->where(['nothi_part_no IN' => $nothi_part_ids])->toArray();
		$table['NothiNoteAttachmentRefs'] = $NothiNoteAttachmentRefsTable->find()->where(['nothi_part_no IN' => $nothi_part_ids])->toArray();
		$table['NothiNoteSheets'] = $NothiNoteSheetsTable->find()->where(['nothi_part_no IN' => $nothi_part_ids])->toArray();
		$table['NothiPotros'] = $NothiPotrosTable->find()->where(['nothi_part_no IN' => $nothi_part_ids])->toArray();
		$table['NothiPotroAttachments'] = $NothiPotroAttachmentsTable->find()->where(['nothi_part_no IN' => $nothi_part_ids])->toArray();

		$table['NothiMasterPermissions'] = $NothiMasterPermissionsTable->find()->where(['office_unit_organograms_id' => $this->selectedDesignation['id']])->toArray();
		$table['NothiNotes'] = $NothiNotesTable->find()->where(['office_organogram_id' => $this->selectedDesignation['id']])->toArray();
		$table['NothiNotePermissions'] = $NothiNotePermissionsTable->find()->where(['office_unit_organograms_id' => $this->selectedDesignation['id']])->toArray();
		$table['NothiNoteSignatures'] = $NothiNoteSignaturesTable->find()->where(['office_organogram_id' => $this->selectedDesignation['id']])->toArray();
		$table['NothiParts'] = $NothiPartsTable->find()->where(['office_units_organogram_id' => $this->selectedDesignation['id']])->toArray();
		$table['OtherOfficeNothiMasterMovements'] = $OtherOfficeNothiMasterMovementsTable->find()->where(['from_officer_designation_id' => $this->selectedDesignation['id']])->toArray();
		$table['PortalPublishes'] = $PortalPublishesTable->find()->where(['nothi_master_id IN' => $nothi_master_ids])->toArray();
		$table['Potrojari'] = $PotrojariTable->find()->where(['nothi_master_id IN' => $nothi_master_ids])->toArray();

		$potrojari_ids = $PotrojariTable->find('list', ['keyField' => 'id', 'valueField' => 'id'])->where(['nothi_master_id IN' => $nothi_master_ids])->toArray();
		$table['PotrojariAttachments'] = $PotrojariAttachmentsTable->find()->where(['potrojari_id IN' => $potrojari_ids])->toArray();

		$table['PotrojariOnulipi'] = $PotrojariOnulipiTable->find()->where(['receiving_officer_designation_id' => $this->selectedDesignation['id']])->toArray();
		$table['PotrojariReceiver'] = $PotrojariReceiverTable->find()->where(['receiving_officer_designation_id' => $this->selectedDesignation['id']])->toArray();
		$table['PotrojariVersions'] = $PotrojariVersionsTable->find()->where(['office_organogram_id' => $this->selectedDesignation['id']])->toArray();
		$table['PotroFlags'] = $PotroFlagsTable->find()->where(['office_organogram_id' => $this->selectedDesignation['id']])->toArray();


		if ($this->assignedUserDetail) {
			$officer_name = $this->assignedUserDetail['name_bng'];
		} else {
			$officer_name = '';
		}
		/// Log Table ENTRY
		$prev_office = TableRegistry::get('Offices')->get($this->transferFromOffice);
		$prev_unit = TableRegistry::get('OfficeUnits')->get($this->selectedDesignation['office_unit_id']);
		$prev_organogram = TableRegistry::get('OfficeUnitOrganograms')->get($this->selectedDesignation['id']);
		$prev_activities = $this->countUserActivitiesDetailsCount($prev_organogram->id, true, $prev_office->id);
		$office = TableRegistry::get('Offices')->get($office_id);
		$unit = TableRegistry::get('OfficeUnits')->get($this->selectedUnit);
		$organogram = TableRegistry::get('OfficeUnitOrganograms')->get($designations['id']);

		$office_segregation_logs = [
			'prev_office_id' => $prev_office->id,
			'prev_office_origin_id' => $prev_office->office_origin_id,
			'prev_unit_id' => $prev_unit->id,
			'prev_unit_origin_id' => $prev_unit->office_origin_unit_id,
			'prev_organogram_id' => $prev_organogram->id,
			'prev_organogram_origin_id' => $prev_organogram->ref_origin_unit_org_id,

			'office_id' => $office->id,
			'office_origin_id' => $office->office_origin_id,
			'unit_id' => $unit->id,
			'unit_origin_id' => $unit->office_origin_unit_id,
			'organogram_id' => $organogram->id,
			'organogram_origin_id' => $organogram->ref_origin_unit_org_id,

			'prev_dak_inbox_count' => $prev_activities['dak_inbox'],
			'prev_nothi_inbox_count' => $prev_activities['nothi_inbox'],
			'prev_nothi_sent_count' => $prev_activities['nothi_sent'],
			'prev_nothi_other_sent_count' => $prev_activities['nothi_other_sent'],
			'prev_potro_count' => null,
			'dak_inbox_count' => null,
			'nothi_inbox_count' => null,
			'nothi_sent_count' => null,
			'nothi_other_sent_count' => null,
			'potro_count' => null,
			'created' => date('Y-m-d H:i:s'),
			'modified' => date('Y-m-d H:i:s'),
		];

		$office_segregation_logsTable = TableRegistry::get('OfficeSegregationLogs');
		$office_segregation_logsPatchData = $office_segregation_logsTable->patchEntity($office_segregation_logsTable->newEntity(), $office_segregation_logs);
		$office_segregation_logs_data = $office_segregation_logsTable->save($office_segregation_logsPatchData);
		/// Log Table ENTRY END

		$manupulating_data = [
			'designation_name_bng' => $designations['designation_bng'],
			'designation_name_eng' => $designations['designation_eng'],
			'office_unit_id' => $designations['office_unit_id'],
			'unit_name_bng' => $this->unitInformation->unit_name_bng,
			'unit_name_eng' => $this->unitInformation->unit_name_eng,
			'seal_owner_unit_id' => $designations['office_unit_id'],
			'to_office_name' => $this->officeInformation['office_name_eng'],
			'to_office_unit_id' => $this->unitInformation->id,
			'to_office_unit_name' => $this->unitInformation->unit_name_bng,
			'dak_actions' => "পদবি স্থানান্তরিত করা হয়েছে",
			'employee_designation' => $designations['designation_bng'],
			'office_unit_name' => $this->unitInformation->unit_name_bng,
			'officer_designation_label' => $designations['designation_bng'],

			'approval_office_unit_id' => $this->unitInformation->id,
			'approval_office_unit_name' => $this->unitInformation->unit_name_bng,
			'approval_officer_designation_label' => $designations['designation_bng'],
			'sovapoti_officer_designation_label' => $designations['designation_bng'],
			'attension_office_unit_id' => $this->unitInformation->id,
			'attension_office_unit_name' => $this->unitInformation->unit_name_bng,
			'attension_officer_designation_label' => $designations['designation_bng'],
			'receiving_office_unit_id' => $this->unitInformation->id,
			'receiving_office_unit_name' => $this->unitInformation->unit_name_bng,
			'receiving_officer_designation_label' => $designations['designation_bng'],
			'office_units_id' => $this->unitInformation->id,

			'modified_by' => $this->authUserId,
			'modified' => date("Y-m-d H:i:s")
		];

		//if ($this->selectedDesignation['id'] == $prev_organogram->id)
		$manupulating_data2 = [
			'from_officer_designation_id' => $designations['id'],
			'office_unit_organogram_id' => $designations['id'],
			'seal_owner_designation_id' => $designations['id'],
			'to_officer_designation_id' => $designations['id'],
			'office_organogram_id' => $designations['id'],
			'officer_designation_id' => $designations['id'],
			'approval_officer_designation_id' => $designations['id'],
			'sovapoti_officer_designation_id' => $designations['id'],
			'attension_officer_designation_id' => $designations['id'],
			'receiving_officer_designation_id' => $designations['id'],
			'office_units_organogram_id' => $designations['id'],
			'office_unit_organograms_id' => $designations['id'],
		];

		$manupulating_data3 = [
			'office_id' => $designations['office_id'],
			'to_office_id' => $designations['office_id'],
			'from_office_id' => $designations['office_id'],
			'nothi_office' => $this->employeeOffice['office_id'],
		];
		$manupulating_data = array_merge($manupulating_data, $manupulating_data2, $manupulating_data3);

		foreach ($table as $table_name => $data_array) {
			foreach ($data_array as $key => $data) {
				if (count($data) > 0) {
					$this->switchOffice($designations['office_id'], 'NewOffice');
					if ($table_name == 'office_designation_seal_owners') {
						$table_name = 'office_designation_seals';
					}
					TableRegistry::remove($table_name);
					$nothiMastersTable = TableRegistry::get($table_name);
					$single_data_array = $data->toArray();
					if ($table_name == 'NothiPotroAttachments') {
						unset($single_data_array['potrojari']);
					}
					if ($nothiMastersTable->find()->where(['id' => $single_data_array['id']])->count() > 0) {
						if ($table_name == 'NothiMasterMovements') {
							if ($single_data_array['to_officer_designation_id'] == $this->selectedDesignation['id'] || $single_data_array['from_officer_designation_id'] == $this->selectedDesignation['id']) {
								if ($single_data_array['to_officer_designation_id'] == $this->selectedDesignation['id']) {
									unset($single_data_array['from_officer_designation_id']);
								} elseif ($single_data_array['from_officer_designation_id'] == $this->selectedDesignation['id']) {
									unset($single_data_array['to_officer_designation_id']);
								}
							} else {
								continue;
							}
						}
					}

					$array_intersect = array_intersect(array_keys($single_data_array), array_keys($manupulating_data));
					foreach ($array_intersect as $k => $common_field) {
						if(in_array($common_field, array_keys($manupulating_data2))) {
							if ($single_data_array[$common_field] == $this->selectedDesignation['id']) {
								$single_data_array[$common_field] = $manupulating_data[$common_field];
							} else {
								continue;
							}
						} elseif(in_array($common_field, array_keys($manupulating_data3))) {
							if ($single_data_array[$common_field] == $this->transferFromOffice) {
								$single_data_array[$common_field] = $manupulating_data[$common_field];
							} else {
								continue;
							}
						} else {
							$single_data_array[$common_field] = $manupulating_data[$common_field];
						}
					}

					$masters = $nothiMastersTable->patchEntity($nothiMastersTable->newEntity(), $single_data_array);
					$nothiMastersTable->save($masters);
				}
			}
		}

		/// Log Table ENTRY
		$activities = $this->countUserActivitiesDetailsCount($organogram->id, true, $office->id);
		$office_segregation_logs = [
			'id' => $office_segregation_logs_data['id'],
			'dak_inbox_count' => $activities['dak_inbox'],
			'nothi_inbox_count' => $activities['nothi_inbox'],
			'nothi_sent_count' => $activities['nothi_sent'],
			'nothi_other_sent_count' => $activities['nothi_other_sent'],
			'potro_count' => null,
			'modified' => date('Y-m-d H:i:s'),
		];
		$office_segregation_logsTable = TableRegistry::get('OfficeSegregationLogs');
		$office_segregation_logsPatchData = $office_segregation_logsTable->patchEntity($office_segregation_logsTable->newEntity(), $office_segregation_logs);
		$office_segregation_logsTable->save($office_segregation_logsPatchData);
		/// Log Table ENTRY END

		###====> INSERT END
		//$this->out('Office data transfer end');
		return true;
	}

    private function moveSummary($designations)
    {
        TableRegistry::remove('SummaryNothiUser');

        $tableSummaryNothiUser = TableRegistry::get('SummaryNothiUsers');

        $tableSummaryNothiUser->updateAll([
            'designation_id' => $designations['id']
        ], [
            'designation_id' => $this->selectedDesignation['id'],
        ]);

        return true;
    }

    private function moveAllDak($designations)
    {

        TableRegistry::remove('DakMovements');
        TableRegistry::remove('DakUsers');

        $DakMovementsTable = TableRegistry::get('DakMovements');
        $DakUsersTable = TableRegistry::get('DakUsers');

        $daks = $DakUsersTable->getData(['dak_category' => DAK_CATEGORY_INBOX, 'to_officer_designation_id' => $this->selectedDesignation['id'], 'is_archive' => 0])->toArray();

        if (!empty($daks)) {
            foreach ($daks as $key => $dakUserDetails) {
                $dak_id = $dakUserDetails['dak_id'];
                $dak_type = $dakUserDetails['dak_type'];

                $dak_last_move = $DakMovementsTable->getDakLastMove($dak_id, $dak_type, ['attention_type' => 1]);
                if ($dak_last_move['to_officer_designation_id'] == $this->selectedDesignation['id'] && ($dak_last_move['operation_type'] == 'Forward' || $dak_last_move['operation_type'] == 'Sent')) {
//Dak Users Update
                    if ($this->assignedUserDetail) {
                        $officer_name = $this->assignedUserDetail['name_bng'];
                        $DakUsersTable->updateAll([
                            'to_officer_name' => $officer_name,
                            'to_office_unit_id' => $this->unitInformation->id,
                            'to_office_unit_name' => $this->unitInformation->unit_name_bng,
                            'to_officer_designation_id' => $designations['id'],
                            'to_officer_designation_label' => $designations['designation_bng'],
                            'modified_by' => $this->authUserId,
                            'modified' => date("Y-m-d H:i:s"),
                        ],
                            [
                                'id' => $dakUserDetails['id']
                            ]);

                        //Dak Movement Update
                        $move = $DakMovementsTable->get($dak_last_move['id']);
                        $move['to_officer_name'] = $officer_name;
                        $move['to_office_unit_id'] = $this->unitInformation->id;
                        $move['to_office_unit_name'] = $this->unitInformation->unit_name_bng;
                        $move['to_officer_designation_id'] = $designations['id'];
                        $move['to_officer_designation_label'] = $designations['designation_bng'];

                        $move['dak_actions'] = "পদবি স্থানান্তরিত করা হয়েছে";

                        $DakMovementsTable->save($move);
                    } else {
                        $DakUsersTable->updateAll([
                            'to_office_unit_id' => $this->unitInformation->id,
                            'to_office_unit_name' => $this->unitInformation->unit_name_bng,
                            'to_officer_designation_id' => $designations['id'],
                            'to_officer_designation_label' => $designations['designation_bng'],
                            'modified_by' => $this->authUserId,
                            'modified' => date("Y-m-d H:i:s"),
                        ],
                            [
                                'id' => $dakUserDetails['id']
                            ]);

                        //Dak Movement Update
                        $move = $DakMovementsTable->get($dak_last_move['id']);
                        $move['to_office_unit_id'] = $this->unitInformation->id;
                        $move['to_office_unit_name'] = $this->unitInformation->unit_name_bng;
                        $move['to_officer_designation_id'] = $designations['id'];
                        $move['to_officer_designation_label'] = $designations['designation_bng'];

                        $move['dak_actions'] = "পদবি স্থানান্তরিত করা হয়েছে";

                        $DakMovementsTable->save($move);
                    }
                }
            }
        }

        return true;
    }

    private function moveAllNothiPermissionandCurrentUser($designations)
    {
        TableRegistry::remove('NothiMasterPermissions');
        TableRegistry::remove('NothiMasterCurrentUsers');
        TableRegistry::remove('NothiMasterMovements');
        TableRegistry::remove('NothiNoteSignatures');
        TableRegistry::remove('NothiNotes');
        TableRegistry::remove('PotroFlags');
        TableRegistry::remove('NothiParts');

        TableRegistry::remove('Potrojari');
        TableRegistry::remove('PotrojariReceiver');
        TableRegistry::remove('PotrojariOnulipi');

        $NothiMasterPermissionsTable = TableRegistry::get('NothiMasterPermissions');
        $NothiMasterCurrentUsersTable = TableRegistry::get('NothiMasterCurrentUsers');
        $NothiMasterMovementsTable = TableRegistry::get('NothiMasterMovements');
        $NothiNoteSignaturesTable = TableRegistry::get('NothiNoteSignatures');
        $NothiNotesTable = TableRegistry::get('NothiNotes');
        $PotroFlagsTable = TableRegistry::get('PotroFlags');
        $NothiPartsTable = TableRegistry::get('NothiParts');

        $PotrojariTable = TableRegistry::get('Potrojari');
        $PotrojariReceiverTable = TableRegistry::get('PotrojariReceiver');
        $PotrojariOnulipiTable = TableRegistry::get('PotrojariOnulipi');

        $getPendingNothi = $NothiMasterCurrentUsersTable->find('list', ['keyField' => 'id', 'valueField' => 'nothi_part_no'])->where([
            'office_unit_organogram_id' => $this->selectedDesignation['id'],
            'nothi_office' => $this->employeeOffice['office_id']
        ])->toArray();
//
//        $getDoneNothi = $NothiMasterCurrentUsersTable->find('list',['keyField'=>'id','valueField'=>'nothi_part_no'])->where([
//            'office_unit_organogram_id' => $this->selectedDesignation['id'],
//            'OR'=>[
//                'is_archive' => 1,
//                'is_finished'=>1,
//            ],
//            'nothi_office'=>$this->employeeOffice['office_id']
//        ])->toArray();

        $NothiMasterCurrentUsersTable->updateAll([
            'office_unit_id' => $this->unitInformation->id,
            'office_unit_organogram_id' => $designations['id'],
            'modified' => date("Y-m-d H:i:s"),
            'modified_by' => $this->authUserId,
        ], [
            'office_unit_organogram_id' => $this->selectedDesignation['id'],
            //'is_archive' => 0,
            //'is_finished' => 0,
            'nothi_office' => $this->employeeOffice['office_id']
        ]);
//            echo "current user moved";

        $NothiNotesTable->updateAll(
            [
                'office_unit_id' => $this->unitInformation->id,
                'office_organogram_id' => $designations['id'],
                'employee_designation' => $designations['designation_bng'],
                'modified' => date("Y-m-d H:i:s"),
                'modified_by' => $this->authUserId,
            ], [
            'office_id' => $this->employeeOffice['office_id'],
            'nothi_part_no IN' => $getPendingNothi,
            'office_organogram_id' => $this->selectedDesignation['id'],
            'note_status' => "DRAFT"
        ]);
//            echo "notes moved";

        $PotroFlagsTable->updateAll(
            [
                'office_unit_id' => $this->unitInformation->id,
                'office_organogram_id' => $designations['id']
            ], [
            'office_id' => $this->employeeOffice['office_id'],
            'nothi_part_no IN' => $getPendingNothi,
            'office_organogram_id' => $this->selectedDesignation['id']
        ]);
//            echo "flag moved";

        $pendingPotrojari = $PotrojariTable->find('list', ['keyField' => 'id', 'valueField' => 'nothi_part_no'])
            ->where(['office_id' => $this->employeeOffice['office_id'],
                'nothi_part_no IN' => $getPendingNothi,
                'potro_status' => 'Draft', 'onulipi_sent' => 0, 'receiver_sent' => 0, 'can_potrojari' => 0])->toArray();

        $PotrojariTable->updateAll(
            [
                'office_unit_id' => $this->unitInformation->id,
                'office_unit_name' => $this->unitInformation->unit_name_bng,
                'officer_designation_id' => $designations['id'],
                'officer_designation_label' => $designations['designation_bng'],
                'modified' => date("Y-m-d H:i:s"),
                'modified_by' => $this->authUserId,
            ], [
            'office_id' => $this->employeeOffice['office_id'],
            'nothi_part_no IN' => $getPendingNothi,
            'officer_designation_id' => $this->selectedDesignation['id'],
            'potro_status' => 'Draft', 'onulipi_sent' => 0, 'receiver_sent' => 0, 'can_potrojari' => 0
        ]);
//            echo "pending potrojari office moved";
        $PotrojariTable->updateAll(
            [
                'approval_office_unit_id' => $this->unitInformation->id,
                'approval_office_unit_name' => $this->unitInformation->unit_name_bng,
                'approval_officer_designation_id' => $designations['id'],
                'approval_officer_designation_label' => $designations['designation_bng'],
                'modified' => date("Y-m-d H:i:s"),
                'modified_by' => $this->authUserId,
            ], [
            'approval_office_id' => $this->employeeOffice['office_id'],
            'nothi_part_no IN' => $getPendingNothi,
            'approval_officer_designation_id' => $this->selectedDesignation['id'],
            'potro_status' => 'Draft', 'onulipi_sent' => 0, 'receiver_sent' => 0, 'can_potrojari' => 0
        ]);
        $PotrojariTable->updateAll(
            [
                'office_unit_id' => $this->unitInformation->id,
                'office_unit_name' => $this->unitInformation->unit_name_bng,
                'officer_designation_id' => $designations['id'],
                'officer_designation_label' => $designations['designation_bng'],
                'modified' => date("Y-m-d H:i:s"),
                'modified_by' => $this->authUserId,
            ], [
            'office_id' => $this->employeeOffice['office_id'],
            'nothi_part_no IN' => $getPendingNothi,
            'officer_designation_id' => $this->selectedDesignation['id'],
            'potro_status' => 'Draft', 'onulipi_sent' => 0, 'receiver_sent' => 0, 'can_potrojari' => 0
        ]);
//            echo "pending potrojari approval moved";
        $PotrojariTable->updateAll(
            [
                'sovapoti_officer_designation_id' => $designations['id'],
                'sovapoti_officer_designation_label' => $designations['designation_bng'],
                'modified' => date("Y-m-d H:i:s"),
                'modified_by' => $this->authUserId,
            ], [
            'nothi_part_no IN' => $getPendingNothi,
            'sovapoti_officer_designation_id' => $this->selectedDesignation['id'],
            'potro_status' => 'Draft', 'onulipi_sent' => 0, 'receiver_sent' => 0, 'can_potrojari' => 0
        ]);
//            echo "pending potrojari sovapoti moved";
        $PotrojariTable->updateAll(
            [
                'attension_office_unit_id' => $this->unitInformation->id,
                'attension_office_unit_name' => $this->unitInformation->unit_name_bng,
                'attension_officer_designation_id' => $designations['id'],
                'attension_officer_designation_label' => $designations['designation_bng'],
                'modified' => date("Y-m-d H:i:s"),
                'modified_by' => $this->authUserId,
            ], [
            'nothi_part_no IN' => $getPendingNothi,
            'attension_officer_designation_id' => $this->selectedDesignation['id'],
            'potro_status' => 'Draft', 'onulipi_sent' => 0, 'receiver_sent' => 0, 'can_potrojari' => 0
        ]);
//            echo "pending potrojari attension moved";

        $PotrojariReceiverTable->updateAll([
            'receiving_office_unit_id' => $this->unitInformation->id,
            'receiving_office_unit_name' => $this->unitInformation->unit_name_bng,
            'receiving_officer_designation_id' => $designations['id'],
            'receiving_officer_designation_label' => $designations['designation_bng'],
            'modified' => date("Y-m-d H:i:s"),
            'modified_by' => $this->authUserId,
        ], [
            'potrojari_id IN' => array_keys($pendingPotrojari),
            'receiving_officer_designation_id' => $this->selectedDesignation['id'],
            'is_sent' => 0
        ]);
//            echo "pending potrojari rec moved";
        $PotrojariOnulipiTable->updateAll([
            'receiving_office_unit_id' => $this->unitInformation->id,
            'receiving_office_unit_name' => $this->unitInformation->unit_name_bng,
            'receiving_officer_designation_id' => $designations['id'],
            'receiving_officer_designation_label' => $designations['designation_bng'],
            'modified' => date("Y-m-d H:i:s"),
            'modified_by' => $this->authUserId,
        ], [
            'potrojari_id IN' => array_keys($pendingPotrojari),
            'receiving_officer_designation_id' => $this->selectedDesignation['id'],
            'is_sent' => 0
        ]);
//            echo "pending potrojari onulipi moved";


        //nothimaster information & nothi part information
        /*
         * How to update these information? Because not all nothi will move, so only few parts cannot be changed to another unit. Need discussion
         */
        $NothiPartsTable->updateAll([
            'office_units_id' => $this->unitInformation->id,
            'office_units_organogram_id' => $designations['id']
        ], [
            'office_units_organogram_id' => $this->selectedDesignation['id'],
//            'id IN' => $getPendingNothi
        ]);

        // nothi master table update
        $NothiMastersTable = TableRegistry::get('NothiMasters');
        $NothiMastersTable->updateAll([
            'office_units_id' => $this->unitInformation->id,
            'office_units_organogram_id' => $designations['id']
        ], [
            'office_units_organogram_id' => $this->selectedDesignation['id']
        ]);

        //move pending
//            echo "movement nothi";
        foreach ($getPendingNothi as $key => $nothi) {
            $totalMove = $NothiMasterMovementsTable->countTotalMove($nothi);
            $lastMove = $NothiMasterMovementsTable->getLastMove($nothi);
            $lastSignature = $NothiNoteSignaturesTable->getLastSignature(0, $nothi)
                ->order(['id desc'])->first();

            //nothi movement and signature
            if ($totalMove > 0 && $lastMove['to_officer_designation_label'] == $this->selectedDesignation['id']) {

                $moveEntity = $NothiMasterMovementsTable->newEntity();

                $moveEntity->nothi_master_id = $lastMove->nothi_master_id;
                $moveEntity->nothi_part_no = $lastMove->nothi_part_no;
                $moveEntity->nothi_office = $lastMove->nothi_part_no;
                $moveEntity->view_status = $lastMove->view_status;
                $moveEntity->movement_type = 3;
                $moveEntity->priority = $lastMove->priority;
                $moveEntity->token = $lastMove->token;
                $moveEntity->device_type = $lastMove->device_type;
                $moveEntity->device_id = $lastMove->device_id;

                $moveEntity->from_office_id = $lastMove->to_office_id;
                $moveEntity->from_office_name = $lastMove->to_office_name;
                $moveEntity->from_office_unit_id = $lastMove->to_office_unit_id;
                $moveEntity->from_office_unit_name = $lastMove->to_office_unit_name;
                $moveEntity->from_officer_id = $lastMove->to_officer_id;
                $moveEntity->from_officer_name = $lastMove->to_officer_name;
                $moveEntity->from_officer_designation_id = $lastMove->to_officer_designation_id;
                $moveEntity->from_officer_designation_label = $lastMove->to_officer_designation_label;

                $moveEntity->to_office_id = $lastMove->to_office_id;
                $moveEntity->to_office_name = $lastMove->to_office_name;
                $moveEntity->to_office_unit_id = $this->unitInformation->id;
                $moveEntity->to_office_unit_name = $this->unitInformation->unit_name_bng;
                $moveEntity->to_officer_id = $lastMove->to_officer_id;
                $moveEntity->to_officer_name = $lastMove->to_officer_name;
                $moveEntity->to_officer_designation_id = $designations['id'];
                $moveEntity->to_officer_designation_label = $designations['designation_bng'];


                $moveEntity->created_by = $this->authUserId;
                $moveEntity->modified_by = $this->authUserId;
                $moveEntity->created = date("Y-m-d H:i:s");
                $moveEntity->modified = date("Y-m-d H:i:s");

                $NothiMasterMovementsTable->save($moveEntity);

                if ($lastSignature['office_organogram_id'] == $this->selectedDesignation['id']) {
                    $signature = $NothiNoteSignaturesTable->get($lastSignature['id']);

                    $signature['office_unit_id'] = $this->unitInformation->id;
                    $signature['office_organogram_id'] = $designations['id'];
                    $signature['employee_designation'] = $this->assign['designation'] . (!empty($this->assign['incharge_label']) ? (' (' . $this->assign['incharge_label'] . ')') : '');
                    $signature['modified'] = date("Y-m-d H:i:s");
                    $signature['modified_by'] = $this->authUserId;

                    $NothiNoteSignaturesTable->save($signature);
                }
            }
        }

        $condition = [];
        $condition[] = ['nothi_office' => $this->employeeOffice['office_id']];
        $condition[] = ['office_unit_organograms_id' => $this->selectedDesignation['id']];

        $existingPermittedUser = $NothiMasterPermissionsTable->find('list', ['keyField' => 'nothi_part_no', 'valueField' => 'office_unit_organograms_id'])
            ->where(['office_unit_organograms_id' => $designations['id'], 'nothi_office' => $this->employeeOffice['office_id']])->toArray();

        if (!empty($existingCurrentUser)) {
            $condition[] = ['nothi_part_no NOT IN' => array_keys($existingPermittedUser)];

        }

        $NothiMasterPermissionsTable->updateAll(
            [
                'office_unit_id' => $this->unitInformation->id,
                'office_unit_organograms_id' => $designations['id'],
                'modified' => date("Y-m-d H:i:s"),
                'modified_by' => $this->authUserId,
            ], $condition);

        $NothiMasterPermissionsTable
            ->deleteAll(['nothi_office' => $this->employeeOffice['office_id'], 'office_unit_organograms_id' => $this->selectedDesignation['id']]);

        return true;
    }

    private function moveProtikolpos($designations, $office = null)
    {
        TableRegistry::remove('ProtikolpoSettings');
        $ProtikolpoSettingsTable = TableRegistry::get('ProtikolpoSettings');

        $ProtikolpoSettingsTable->updateAll(
            ['designation_id' => $designations['id'], 'unit_id' => $this->unitInformation->id,'office_id'=>$designations['office_id']],
            ['office_id' => !empty($office)?$office:$this->employeeOffice['office_id'], 'designation_id' => $this->selectedDesignation['id']]);

        $allProtikolpos = $ProtikolpoSettingsTable->find()->where(['office_id' => !empty($office)?$office:$this->employeeOffice['office_id']])->toArray();

        if (!empty($allProtikolpos)) {
            foreach ($allProtikolpos as $key => $value) {
                $protikolpos = jsonA($value['protikolpos']);

                if (isset($protikolpos['protikolpo_1']['designation_id']) && $protikolpos['protikolpo_1']['designation_id'] == $this->selectedDesignation['id']) {
                    $protikolpos['protikolpo_1']['office_unit_id'] = $this->unitInformation->id;
                    $protikolpos['protikolpo_1']['designation_id'] = $designations['id'];
                }
                if (isset($protikolpos['protikolpo_2']['designation_id']) && $protikolpos['protikolpo_2']['designation_id'] == $this->selectedDesignation['id']) {
                    $protikolpos['protikolpo_2']['office_unit_id'] = $this->unitInformation->id;
                    $protikolpos['protikolpo_2']['designation_id'] = $designations['id'];
                }
                $ProtikolpoSettingsTable->updateAll(['protikolpos' => json_encode($protikolpos)], ['id' => $value['id']]);
            }
        }

        return true;
    }

}
