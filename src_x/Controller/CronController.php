<?php

namespace App\Controller;

use App\Controller\ProjapotiController;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;
use Cake\Cache\Cache;

class CronController extends ProjapotiController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow([
            'officeDashboardCron', 'unitDashboardCron', 'designationDashboardCron',
            'officeDashboardCronUpdate', 'designationDashboardCronUpdate', 'unitDashboardCronUpdate',
            'OfficeDashboardUpdateDak', 'nothiDashboardCount', 'cronForOfficeDashboard','runOfficeRelatedTasks','runReportUpdateChecker'
        ]);
    }

    public function officeDashboardCron()
    {
        $this->layout = 'dashboard';
        TableRegistry::remove('OfficeDomains');
        $office_domains = TableRegistry::get('OfficeDomains');
        $all_offices = $office_domains->find()->where(['status' => 1])->toArray();
        $this->set(compact('all_offices'));
    }

    public function officeDashboardCronUpdate()
    {
        if ($this->request->is('post')) {
            TableRegistry::remove('Dashboard');
            if (empty($this->request->data['office_id'])) {
                return;
            }
            if (empty($this->request->data['type'])) {
                return;
            }
            $DashboardTable = TableRegistry::get('Dashboard');
            TableRegistry::remove('DashboardOffices');
            $dashboardOfficesTable = TableRegistry::get('DashboardOffices');

            $office_id = $this->request->data['office_id'];
            $type = $this->request->data['type'];
            $data = [];
            if ($type == 'check') {
                try {
                    $this->switchOffice($office_id, 'DashboardOffice');
                    $f = $dashboardOfficesTable->checkUpdate($office_id,
                        date('Y-m-d'));
                    if ($f == 0) {
                        $last_update = $dashboardOfficesTable->getLastUpdateDate($office_id);
                        //  date('Y-m-d',  strtotime("+1 day",strtotime($last_update['operation_date']) ) )
                        if (!empty($last_update['operation_date'])) {
                            $time = new Time($last_update['operation_date']);
                            $last_update['operation_date'] = $time->i18nFormat(null,
                                null, 'en-US');
                        }
                        $last_update = !empty($last_update['operation_date']) ? date('Y-m-d',
                            strtotime($last_update['operation_date'])) : '';
                        $data = [
                            'status' => 'success',
                            'last_time' => $last_update
                        ];
                    } else if ($f > 0) {
                        $data = [
                            'status' => 'error',
                            'msg' => 'Already Updated'
                        ];
                    }
                } catch (\Exception $ex) {
                    $data = [
                        'status' => 'error',
                        'msg' => $ex->getMessage()
                    ];
                }
                $this->response->type('application/json');
                $this->response->body(json_encode($data));
                return $this->response;
            } else if ($type == 'dak') {
                try {
                    $dt = '';
                    if (!empty($this->request->data['last_time'])) {
                        $dt = $this->request->data['last_time'];
                    }

                    $this->switchOffice($office_id, 'DashboardOffice');
                    $data = $DashboardTable->OfficeDashboardUpdateDak($office_id,
                        $dt);
                } catch (\Exception $ex) {

                }
            } else if ($type == 'nothi') {
                try {
                    $dt = '';
                    if (!empty($this->request->data['last_time'])) {
                        $dt = $this->request->data['last_time'];
                    }
                    $this->switchOffice($office_id, 'DashboardOffice');
                    $data = $DashboardTable->OfficeDashboardUpdateNothi($office_id,
                        $dt);
                } catch (\Exception $ex) {

                }
            } else if ($type == 'save') {
                try {
                    $save_type = 'full';
                    if (!empty($this->request->data['role'])) {
                        $save_type = $this->request->data['role'];
                    }
                    $this->switchOffice($office_id, 'DashboardOffice');


                    $dak = $this->request->data['dak'];
                    $nothi = $this->request->data['nothi'];
                    if (empty($dak) || empty($nothi) || !isset($dak['totalinboxall'])
                        || !isset($nothi['totalsrijitonoteall'])) {
                        $data = [
                            'status' => 'error',
                            'msg' => 'Empty Data'
                        ];
                        $this->response->type('application/json');
                        $this->response->body(json_encode($data));
                        return $this->response;
                    }
                    if ($save_type == 'full') {
                        $save_data = [
                            'office_id' => $office_id, 'operation_date' => date('Y-m-d'),
                            'dak_inbox' => $dak['totalinboxall'], 'dak_outbox' => $dak['totaloutboxall'],
                            'nothivukto' => $dak['totalnothivuktoall'],
                            'nothijat' => $dak['totalnothijatoall'], 'dak_nisponno' => $dak['totalnisponnodakall'],
                            'dak_onisponno' => $dak['totalOnisponnodakall'],
                            'self_note' => $nothi['totalsrijitonoteall'], 'dak_note' => $nothi['totaldaksohonoteall'],
                            'note_nisponno' => $nothi['totalnisponnonoteall'],
                            'potrojari_nisponno' => $nothi['totalnisponnopotrojariall'],
                            'onisponno_note' => $nothi['totalOnisponnonoteall'],
                            'potrojari' => $dak['totalpotrojariall']
                        ];
                    } else {
                        $last_data = $dashboardOfficesTable->getData($office_id);
                        $save_data = [
                            'office_id' => $office_id, 'operation_date' => date('Y-m-d'),
                            'dak_inbox' => $dak['totalinboxall'] + $last_data['totalInbox'],
                            'dak_outbox' => $dak['totaloutboxall'] + $last_data['totalOutbox'],
                            'nothivukto' => $dak['totalnothivuktoall'] + $last_data['totalNothivukto'],
                            'nothijat' => $dak['totalnothijatoall'] + $last_data['totalNothijato'],
                            'dak_nisponno' => $dak['totalnisponnodakall'] + $last_data['totalNisponnoDak'],
                            'dak_onisponno' => $dak['totalOnisponnodakall'],
                            'self_note' => $nothi['totalsrijitonoteall'] + $last_data['totalSouddog'],
                            'dak_note' => $nothi['totaldaksohonoteall'] + $last_data['totalDaksohoNote'],
                            'note_nisponno' => $nothi['totalnisponnonoteall'] + $last_data['totalNisponnoNote'],
                            'potrojari_nisponno' => $nothi['totalnisponnopotrojariall']
                                + $last_data['totalNisponnoPotrojari'],
                            'onisponno_note' => $DashboardTable->getOnisponnoNoteCount($office_id, 0, 0, ['', date('Y-m-d', strtotime(date('Y-m-d') . ' -1 day'))]),
                            'potrojari' => $dak['totalpotrojariall'] + $last_data['totalPotrojari']
                        ];
                    }

                    $dashboardOfficesTable->deleteAll(['office_id' => $office_id]);
                    $DashboardOfficesEntity = $dashboardOfficesTable->newEntity();
                    $DashboardOfficesEntity->office_id = $save_data['office_id'];
                    $DashboardOfficesEntity->operation_date = $save_data['operation_date'];
                    $DashboardOfficesEntity->dak_inbox = $save_data['dak_inbox'];
                    $DashboardOfficesEntity->dak_outbox = $save_data['dak_outbox'];
                    $DashboardOfficesEntity->nothivukto = $save_data['nothivukto'];
                    $DashboardOfficesEntity->nothijat = $save_data['nothijat'];
                    $DashboardOfficesEntity->dak_nisponno = $save_data['dak_nisponno'];
                    $DashboardOfficesEntity->dak_onisponno = $save_data['dak_onisponno'];
                    $DashboardOfficesEntity->self_note = $save_data['self_note'];
                    $DashboardOfficesEntity->dak_note = $save_data['dak_note'];
                    $DashboardOfficesEntity->potrojari_nisponno = $save_data['potrojari_nisponno'];
                    $DashboardOfficesEntity->note_nisponno = $save_data['note_nisponno'];
                    $DashboardOfficesEntity->onisponno_note = $save_data['onisponno_note'];
                    $DashboardOfficesEntity->potrojari = $save_data['potrojari'];

                    $f = $dashboardOfficesTable->save($DashboardOfficesEntity);
                    if ($f == TRUE) {
                        $data = [
                            'status' => 'success',
                            'msg' => 'Update Complete ' . $save_type
                        ];
                    } else if ($f == FALSE) {
                        $data = [
                            'status' => 'error',
                            'msg' => 'Can not update'
                        ];
                    }
                } catch (\Exception $ex) {
                    $data = [
                        'status' => 'error',
                        'msg' => $ex->getMessage()
                    ];
                }
//                 saveData
            }
            $this->response->type('application/json');
            $this->response->body(json_encode($data));
            return $this->response;
        }
    }

    public function cronForOfficeDashboard()
    {
        ini_set('memory_limit', -1);
        set_time_limit(0);
        $start_time = strtotime(date('Y-m-d H:i:s'));
        $office_domains = TableRegistry::get('OfficeDomains');
        $all_offices = $office_domains->getOfficesData();
        $data = [];
        $total = 0;
//            pr($all_offices);
        TableRegistry::remove('Dashboard');
        $DashboardTable = TableRegistry::get('Dashboard');
        TableRegistry::remove('DashboardOffices');
        $dashboardOfficesTable = TableRegistry::get('DashboardOffices');
        foreach ($all_offices as $office_id => $office_name) {
            $total++;

            $f = $dashboardOfficesTable->checkUpdate($office_id, date('Y-m-d'));
            if ($f > 0) {
                continue;
            }
            try {
                $dt = '';
                $this->switchOffice($office_id, 'DashboardOffice');
                $dak = $DashboardTable->OfficeDashboardUpdateDak($office_id,
                    $dt);
                $nothi = $DashboardTable->OfficeDashboardUpdateNothi($office_id,
                    $dt);
                $save_type = 'full';

                if (empty($dak) || empty($nothi) || !isset($dak['totalinboxall'])
                    || !isset($nothi['totalsrijitonoteall'])) {
                    $data[] = [
                        'office_id' => $office_id,
                        'office_name' => $office_name,
                        'msg' => 'Empty Data',
                    ];
                    continue;
                }
                if ($save_type == 'full') {
                    $save_data = [
                        'office_id' => $office_id, 'operation_date' => date('Y-m-d'),
                        'dak_inbox' => $dak['totalinboxall'], 'dak_outbox' => $dak['totaloutboxall'],
                        'nothivukto' => $dak['totalnothivuktoall'],
                        'nothijat' => $dak['totalnothijatoall'], 'dak_nisponno' => $dak['totalnisponnodakall'],
                        'dak_onisponno' => $dak['totalOnisponnodakall'],
                        'self_note' => $nothi['totalsrijitonoteall'], 'dak_note' => $nothi['totaldaksohonoteall'],
                        'note_nisponno' => $nothi['totalnisponnonoteall'],
                        'potrojari_nisponno' => $nothi['totalnisponnopotrojariall'],
                        'onisponno_note' => $nothi['totalOnisponnonoteall'],
                        'potrojari' => $dak['totalpotrojariall']
                    ];
                } else {
                    $last_data = $dashboardOfficesTable->getData($office_id);
                    $save_data = [
                        'office_id' => $office_id, 'operation_date' => date('Y-m-d'),
                        'dak_inbox' => $dak['totalinboxall'] + $last_data['totalInbox'],
                        'dak_outbox' => $dak['totaloutboxall'] + $last_data['totalOutbox'],
                        'nothivukto' => $dak['totalnothivuktoall'] + $last_data['totalNothivukto'],
                        'nothijat' => $dak['totalnothijatoall'] + $last_data['totalNothijato'],
                        'dak_nisponno' => $dak['totalnisponnodakall'] + $last_data['totalNisponnoDak'],
                        'dak_onisponno' => $dak['totalOnisponnodakall'],
                        'self_note' => $nothi['totalsrijitonoteall'] + $last_data['totalSouddog'],
                        'dak_note' => $nothi['totaldaksohonoteall'] + $last_data['totalDaksohoNote'],
                        'note_nisponno' => $nothi['totalnisponnonoteall'] + $last_data['totalNisponnoNote'],
                        'potrojari_nisponno' => $nothi['totalnisponnopotrojariall']
                            + $last_data['totalNisponnoPotrojari'],
                        'onisponno_note' => $nothi['totalOnisponnonoteall'] + $last_data['totalONisponno'],
                        'potrojari' => $dak['totalpotrojariall'] + $last_data['totalPotrojari']
                    ];
                }

                $dashboardOfficesTable->deleteAll(['office_id' => $office_id]);
                $DashboardOfficesEntity = $dashboardOfficesTable->newEntity();
                $DashboardOfficesEntity->office_id = $save_data['office_id'];
                $DashboardOfficesEntity->operation_date = $save_data['operation_date'];
                $DashboardOfficesEntity->dak_inbox = $save_data['dak_inbox'];
                $DashboardOfficesEntity->dak_outbox = $save_data['dak_outbox'];
                $DashboardOfficesEntity->nothivukto = $save_data['nothivukto'];
                $DashboardOfficesEntity->nothijat = $save_data['nothijat'];
                $DashboardOfficesEntity->dak_nisponno = $save_data['dak_nisponno'];
                $DashboardOfficesEntity->dak_onisponno = $save_data['dak_onisponno'];
                $DashboardOfficesEntity->self_note = $save_data['self_note'];
                $DashboardOfficesEntity->dak_note = $save_data['dak_note'];
                $DashboardOfficesEntity->potrojari_nisponno = $save_data['potrojari_nisponno'];
                $DashboardOfficesEntity->note_nisponno = $save_data['note_nisponno'];
                $DashboardOfficesEntity->onisponno_note = $save_data['onisponno_note'];
                $DashboardOfficesEntity->potrojari = $save_data['potrojari'];

                $f = $dashboardOfficesTable->save($DashboardOfficesEntity);
                if ($f == FALSE) {
                    $data[] = [
                        'office_id' => $office_id,
                        'office_name' => $office_name,
                        'msg' => 'Can not update'
                    ];
                    continue;
                }
            } catch (\Exception $ex) {
                $data[] = [
                    'office_id' => $office_id,
                    'office_name' => $office_name,
                    'msg' => $ex->getMessage()
                ];
                continue;
            }
        }
        if (NOTIFICATION_OFF == 0) {
            $response .= "Total: " . $total;
            $end_time = strtotime(date('Y-m-d H:i:s'));
            $time_difference = round(abs(($end_time - $start_time) / 60), 2);
            $response .= "<br> Time: " . $time_difference . " minutes";
            if (!empty($data)) {
                $response .= '<br>Error list (' . count($data) . ') : <ul>';
                foreach ($data as $id) {
                    $response .= "<li>Office ID: " . $id['office_id'] . " Name: " . $id['office_name'] . " Error: " . $id['msg'] . "</li><br>";
                }
                $response .= "</ul> ";
            } else {
                $response .= "No Error Occured";
            }
            $this->sendCronMessage('tusharkaiser@gmail.com', [],
                'Cron Report ' . date('Y-m-d'), 'Office Dashboard Report', $response);
//                 saveData
        }
        die;
    }

    public function unitDashboardCron()
    {
        $this->layout = 'dashboard';
        TableRegistry::remove('EmployeeOffices');
        $employee_offices = TableRegistry::get('EmployeeOffices');
        $all_units = $employee_offices->getAllUnitID()->toArray();
        $this->set(compact('all_units'));
    }

    public function unitDashboardCronUpdate()
    {
        if ($this->request->is('post')) {
            TableRegistry::remove('Dashboard');
            if (empty($this->request->data['unit_id'])) {
                return;
            }
            if (empty($this->request->data['type'])) {
                return;
            }
            $DashboardTable = TableRegistry::get('Dashboard');
            TableRegistry::remove('DashboardUnits');
            $dashboardUnitsTable = TableRegistry::get('DashboardUnits');
            TableRegistry::get('OfficeUnits');
            $OfficeUnits = TableRegistry::get('OfficeUnits');

            $unit_id = $this->request->data['unit_id'];
            $unit_info = $OfficeUnits->get($unit_id);
            $office_id = $unit_info['office_id'];
            $type = $this->request->data['type'];
            $data = [];
            if ($type == 'check') {
                try {
                    $this->switchOffice($office_id, 'DashboardOffice');
                    $f = $dashboardUnitsTable->checkUpdate($unit_id,
                        date('Y-m-d'));
                    if ($f == 0) {
                        $last_update = $dashboardUnitsTable->getLastUpdateDate($unit_id);
                        //  date('Y-m-d',  strtotime("+1 day",strtotime($last_update['operation_date']) ) )
                        if (!empty($last_update['operation_date'])) {
                            $time = new Time($last_update['operation_date']);
                            $last_update['operation_date'] = $time->i18nFormat(null,
                                null, 'en-US');
                        }
                        $last_update = !empty($last_update['operation_date']) ? date('Y-m-d',
                            strtotime($last_update['operation_date'])) : '';
                        $data = [
                            'status' => 'success',
                            'last_time' => $last_update
                        ];
                    } else if ($f > 0) {
                        $data = [
                            'status' => 'error',
                            'msg' => 'Already Updated'
                        ];
                    }
                } catch (\Exception $ex) {
                    $data = [
                        'status' => 'error',
                        'msg' => $ex->getMessage()
                    ];
                }
                $this->response->type('application/json');
                $this->response->body(json_encode($data));
                return $this->response;
            } else if ($type == 'dak') {
                try {
                    $this->switchOffice($office_id, 'DashboardOffice');
                    $dt = '';
                    if (!empty($this->request->data['last_time'])) {
                        $dt = $this->request->data['last_time'];
                    }
                    $data = $DashboardTable->OtherDashboardUpdateDak($office_id,
                        $unit_id, 0, $dt);
                } catch (\Exception $ex) {

                }
            } else if ($type == 'nothi') {
                try {
                    $this->switchOffice($office_id, 'DashboardOffice');
                    $dt = '';
                    if (!empty($this->request->data['last_time'])) {
                        $dt = $this->request->data['last_time'];
                    }
                    $data = $DashboardTable->OtherDashboardUpdateNothi($office_id,
                        $unit_id, 0, $dt);
                } catch (\Exception $ex) {

                }
            } else if ($type == 'save') {
                try {
                    $this->switchOffice($office_id, 'DashboardOffice');
                    $save_type = 'full';
                    if (!empty($this->request->data['role'])) {
                        $save_type = $this->request->data['role'];
                    }

                    $dak = $this->request->data['dak'];
                    $nothi = $this->request->data['nothi'];
                    if (empty($dak) || empty($nothi) || !isset($dak['totalinboxall'])
                        || !isset($nothi['totalsrijitonoteall'])) {
                        $data = [
                            'status' => 'error',
                            'msg' => 'Empty Data'
                        ];
                        $this->response->type('application/json');
                        $this->response->body(json_encode($data));
                        return $this->response;
                    }
                    if ($save_type == 'full') {
                        $save_data = [
                            'unit_id' => $unit_id, 'operation_date' => date('Y-m-d'),
                            'dak_inbox' => $dak['totalinboxall'], 'dak_outbox' => $dak['totaloutboxall'],
                            'nothivukto' => $dak['totalnothivuktoall'],
                            'nothijat' => $dak['totalnothijatoall'], 'dak_nisponno' => $dak['totalnisponnodakall'],
                            'dak_onisponno' => $dak['totalOnisponnodakall'],
                            'self_note' => $nothi['totalsrijitonoteall'], 'dak_note' => $nothi['totaldaksohonoteall'],
                            'note_nisponno' => $nothi['totalnisponnonoteall'],
                            'potrojari_nisponno' => $nothi['totalnisponnopotrojariall'],
                            'onisponno_note' => $nothi['totalOnisponnonoteall'],
                            'potrojari' => $dak['totalpotrojariall']
                        ];
                    } else {
                        $last_data = $dashboardUnitsTable->getData($unit_id);
                        $save_data = [
                            'unit_id' => $unit_id, 'operation_date' => date('Y-m-d'),
                            'dak_inbox' => $dak['totalinboxall'] + $last_data['totalInbox'],
                            'dak_outbox' => $dak['totaloutboxall'] + $last_data['totalOutbox'],
                            'nothivukto' => $dak['totalnothivuktoall'] + $last_data['totalNothivukto'],
                            'nothijat' => $dak['totalnothijatoall'] + $last_data['totalNothijato'],
                            'dak_nisponno' => $dak['totalnisponnodakall'] + $last_data['totalNisponnoDak'],
                            'dak_onisponno' => $dak['totalOnisponnodakall'],
                            'self_note' => $nothi['totalsrijitonoteall'] + $last_data['totalSouddog'],
                            'dak_note' => $nothi['totaldaksohonoteall'] + $last_data['totalDaksohoNote'],
                            'note_nisponno' => $nothi['totalnisponnonoteall'] + $last_data['totalNisponnoNote'],
                            'potrojari_nisponno' => $nothi['totalnisponnopotrojariall']
                                + $last_data['totalNisponnoPotrojari'],
                            'onisponno_note' => $DashboardTable->getOnisponnoNoteCount($office_id, $unit_id, 0, ['', date('Y-m-d', strtotime(date('Y-m-d') . ' -1 day'))]),
                            'potrojari' => $dak['totalpotrojariall'] + $last_data['totalPotrojari']
                        ];
                    }
                    $dashboardUnitsTable->deleteAll(['unit_id' => $unit_id]);
                    $DashboardUnitsEntity = $dashboardUnitsTable->newEntity();
                    $DashboardUnitsEntity->unit_id = $save_data['unit_id'];
                    $DashboardUnitsEntity->operation_date = $save_data['operation_date'];
                    $DashboardUnitsEntity->dak_inbox = $save_data['dak_inbox'];
                    $DashboardUnitsEntity->dak_outbox = $save_data['dak_outbox'];
                    $DashboardUnitsEntity->nothivukto = $save_data['nothivukto'];
                    $DashboardUnitsEntity->nothijat = $save_data['nothijat'];
                    $DashboardUnitsEntity->dak_nisponno = $save_data['dak_nisponno'];
                    $DashboardUnitsEntity->dak_onisponno = $save_data['dak_onisponno'];
                    $DashboardUnitsEntity->self_note = $save_data['self_note'];
                    $DashboardUnitsEntity->dak_note = $save_data['dak_note'];
                    $DashboardUnitsEntity->potrojari_nisponno = $save_data['potrojari_nisponno'];
                    $DashboardUnitsEntity->note_nisponno = $save_data['note_nisponno'];
                    $DashboardUnitsEntity->onisponno_note = $save_data['onisponno_note'];
                    $DashboardUnitsEntity->potrojari = $save_data['potrojari'];

                    $f = $dashboardUnitsTable->save($DashboardUnitsEntity);
                    if ($f == TRUE) {
                        $data = [
                            'status' => 'success',
                            'msg' => 'Update Complete'
                        ];
                    } else if ($f == FALSE) {
                        $data = [
                            'status' => 'error',
                            'msg' => 'Can not update'
                        ];
                    }
                } catch (\Exception $ex) {
                    $data = [
                        'status' => 'error',
                        'msg' => $ex->getMessage()
                    ];
                }
//                 saveData
            }
            $this->response->type('application/json');
            $this->response->body(json_encode($data));
            return $this->response;
        }
    }

    public function cronForUnitDashboard()
    {
        ini_set('memory_limit', -1);
        set_time_limit(0);
        $start_time = strtotime(date('Y-m-d H:i:s'));
        TableRegistry::remove('EmployeeOffices');
        $employee_offices = TableRegistry::get('EmployeeOffices');
        $all_units = $employee_offices->getAllUnitID()->toArray();
        $data = [];
        $total = 0;
//            pr($all_offices);
        TableRegistry::remove('Dashboard');
        $DashboardTable = TableRegistry::get('Dashboard');
        TableRegistry::remove('DashboardUnits');
        $dashboardUnitsTable = TableRegistry::get('DashboardUnits');
        TableRegistry::get('OfficeUnits');
        $OfficeUnits = TableRegistry::get('OfficeUnits');
        foreach ($all_units as $unit_id => $unit_name) {
            $total++;

            $f = $dashboardUnitsTable->checkUpdate($unit_id, date('Y-m-d'));
            if ($f > 0) {
                continue;
            }
            try {
                $unit_info = $OfficeUnits->get($unit_id);
                $office_id = $unit_info['office_id'];
                $dt = '';
                $this->switchOffice($office_id, 'DashboardOffice');
                $dak = $DashboardTable->OtherDashboardUpdateDak($office_id,
                    $unit_id, 0, $dt);
                $nothi = $DashboardTable->OtherDashboardUpdateNothi($office_id,
                    $unit_id, 0, $dt);
                $save_type = 'full';

                if (empty($dak) || empty($nothi) || !isset($dak['totalinboxall'])
                    || !isset($nothi['totalsrijitonoteall'])) {
                    $data[] = [
                        'office_id' => $office_id,
                        'unit_id' => $unit_id,
                        'unit_name' => $unit_name,
                        'msg' => 'Empty Data',
                    ];
                    continue;
                }
                if ($save_type == 'full') {
                    $save_data = [
                        'unit_id' => $unit_id, 'operation_date' => date('Y-m-d'),
                        'dak_inbox' => $dak['totalinboxall'], 'dak_outbox' => $dak['totaloutboxall'],
                        'nothivukto' => $dak['totalnothivuktoall'],
                        'nothijat' => $dak['totalnothijatoall'], 'dak_nisponno' => $dak['totalnisponnodakall'],
                        'dak_onisponno' => $dak['totalOnisponnodakall'],
                        'self_note' => $nothi['totalsrijitonoteall'], 'dak_note' => $nothi['totaldaksohonoteall'],
                        'note_nisponno' => $nothi['totalnisponnonoteall'],
                        'potrojari_nisponno' => $nothi['totalnisponnopotrojariall'],
                        'onisponno_note' => $nothi['totalOnisponnonoteall'],
                        'potrojari' => $dak['totalpotrojariall']
                    ];
                } else {
                    $last_data = $dashboardUnitsTable->getData($unit_id);
                    $save_data = [
                        'unit_id' => $unit_id, 'operation_date' => date('Y-m-d'),
                        'dak_inbox' => $dak['totalinboxall'] + $last_data['totalInbox'],
                        'dak_outbox' => $dak['totaloutboxall'] + $last_data['totalOutbox'],
                        'nothivukto' => $dak['totalnothivuktoall'] + $last_data['totalNothivukto'],
                        'nothijat' => $dak['totalnothijatoall'] + $last_data['totalNothijato'],
                        'dak_nisponno' => $dak['totalnisponnodakall'] + $last_data['totalNisponnoDak'],
                        'dak_onisponno' => $dak['totalOnisponnodakall'],
                        'self_note' => $nothi['totalsrijitonoteall'] + $last_data['totalSouddog'],
                        'dak_note' => $nothi['totaldaksohonoteall'] + $last_data['totalDaksohoNote'],
                        'note_nisponno' => $nothi['totalnisponnonoteall'] + $last_data['totalNisponnoNote'],
                        'potrojari_nisponno' => $nothi['totalnisponnopotrojariall']
                            + $last_data['totalNisponnoPotrojari'],
                        'onisponno_note' => $nothi['totalOnisponnonoteall'] + $last_data['totalONisponno'],
                        'potrojari' => $dak['totalpotrojariall'] + $last_data['totalPotrojari']
                    ];
                }
                $dashboardUnitsTable->deleteAll(['unit_id' => $unit_id]);
                $DashboardUnitsEntity = $dashboardUnitsTable->newEntity();
                $DashboardUnitsEntity->unit_id = $save_data['unit_id'];
                $DashboardUnitsEntity->operation_date = $save_data['operation_date'];
                $DashboardUnitsEntity->dak_inbox = $save_data['dak_inbox'];
                $DashboardUnitsEntity->dak_outbox = $save_data['dak_outbox'];
                $DashboardUnitsEntity->nothivukto = $save_data['nothivukto'];
                $DashboardUnitsEntity->nothijat = $save_data['nothijat'];
                $DashboardUnitsEntity->dak_nisponno = $save_data['dak_nisponno'];
                $DashboardUnitsEntity->dak_onisponno = $save_data['dak_onisponno'];
                $DashboardUnitsEntity->self_note = $save_data['self_note'];
                $DashboardUnitsEntity->dak_note = $save_data['dak_note'];
                $DashboardUnitsEntity->potrojari_nisponno = $save_data['potrojari_nisponno'];
                $DashboardUnitsEntity->note_nisponno = $save_data['note_nisponno'];
                $DashboardUnitsEntity->onisponno_note = $save_data['onisponno_note'];
                $DashboardUnitsEntity->potrojari = $save_data['potrojari'];

                $f = $dashboardUnitsTable->save($DashboardUnitsEntity);
                if ($f == FALSE) {
                    $data[] = [
                        'office_id' => $office_id,
                        'unit_id' => $unit_id,
                        'unit_name' => $unit_name,
                        'msg' => 'Can not update'
                    ];
                    continue;
                }
            } catch (\Exception $ex) {
                $data[] = [
                    'office_id' => $office_id,
                    'unit_id' => $unit_id,
                    'unit_name' => $unit_name,
                    'msg' => $ex->getMessage()
                ];
                continue;
            }
        }
        if (NOTIFICATION_OFF == 0) {
            $response .= "Total: " . $total;
            $end_time = strtotime(date('Y-m-d H:i:s'));
            $time_difference = round(abs(($end_time - $start_time) / 60), 2);
            $response .= "<br> Time: " . $time_difference . " minutes";
            if (!empty($data)) {
                $response .= '<br>Error list (' . count($data) . ') : <ul>';
                foreach ($data as $id) {
                    $response .= "<li>Office ID: " . $id['office_id'] . " Unit ID: " . $id['unit_id'] . " Name: " . $id['unit_name'] . " Error: " . $id['msg'] . "</li><br>";
                }
                $response .= "</ul> ";
            } else {
                $response .= "No Error Occured";
            }
            $this->sendCronMessage('tusharkaiser@gmail.com', [],
                'Cron Report ' . date('Y-m-d'), 'Unit Dashboard Report', $response);
//                 saveData
        }
        die;
    }

    public function designationDashboardCron()
    {
        $this->layout = 'dashboard';
        TableRegistry::remove('EmployeeOffices');
        $employeeOfficesTable = TableRegistry::get('EmployeeOffices');
        $all_designations = $employeeOfficesTable->getAllDesignationID()->toArray();
//            pr($all_designations);die;
        $this->set(compact('all_designations'));
    }

    public function designationDashboardCronUpdate()
    {
        if ($this->request->is('post')) {
            TableRegistry::remove('Dashboard');
            if (empty($this->request->data['designation_id'])) {
                return;
            }
            if (empty($this->request->data['type'])) {
                return;
            }
            $DashboardTable = TableRegistry::get('Dashboard');
            TableRegistry::remove('DashboardDesignations');
            $dashboardDesignationsTable = TableRegistry::get('DashboardDesignations');
            TableRegistry::get('OfficeUnitOrganograms');
            $designationTable = TableRegistry::get('OfficeUnitOrganograms');

            $designation_id = $this->request->data['designation_id'];
            $designation_info = $designationTable->get($designation_id);
            $office_id = $designation_info['office_id'];
            $unit_id = $designation_info['office_unit_id'];
            $type = $this->request->data['type'];
            $data = [];
            if ($type == 'check') {
                try {
                    $this->switchOffice($office_id, 'DashboardOffice');
                    $f = $dashboardDesignationsTable->checkUpdate($designation_id,
                        date('Y-m-d'));
                    if ($f == 0) {
                        $last_update = $dashboardDesignationsTable->getLastUpdateDate($designation_id);
                        //  date('Y-m-d',  strtotime("+1 day",strtotime($last_update['operation_date']) ) )
                        if (!empty($last_update['operation_date'])) {
                            $time = new Time($last_update['operation_date']);
                            $last_update['operation_date'] = $time->i18nFormat(null,
                                null, 'en-US');
                        }
                        $last_update = !empty($last_update['operation_date']) ? date('Y-m-d',
                            strtotime($last_update['operation_date'])) : '';
                        $data = [
                            'status' => 'success',
                            'last_time' => $last_update
                        ];
                    } else if ($f > 0) {
                        $data = [
                            'status' => 'error',
                            'msg' => 'Already Updated'
                        ];
                    }
                } catch (\Exception $ex) {
                    $data = [
                        'status' => 'error',
                        'msg' => $ex->getMessage()
                    ];
                }
                $this->response->type('application/json');
                $this->response->body(json_encode($data));
                return $this->response;
            } else if ($type == 'dak') {
                try {
                    $this->switchOffice($office_id, 'DashboardOffice');
                    $dt = '';
                    if (!empty($this->request->data['last_time'])) {
                        $dt = $this->request->data['last_time'];
                    }
                    $data = $DashboardTable->OtherDashboardUpdateDak($office_id,
                        $unit_id, $designation_id, $dt);
                } catch (\Exception $ex) {

                }
            } else if ($type == 'nothi') {
                try {
                    $this->switchOffice($office_id, 'DashboardOffice');
                    $dt = '';
                    if (!empty($this->request->data['last_time'])) {
                        $dt = $this->request->data['last_time'];
                    }
                    $data = $DashboardTable->OtherDashboardUpdateNothi($office_id,
                        $unit_id, $designation_id, $dt);
                } catch (\Exception $ex) {

                }
            } else if ($type == 'save') {
                try {
                    $this->switchOffice($office_id, 'DashboardOffice');
                    $save_type = 'full';
                    if (!empty($this->request->data['role'])) {
                        $save_type = $this->request->data['role'];
                    }

                    $dak = $this->request->data['dak'];
                    $nothi = $this->request->data['nothi'];
                    if (empty($dak) || empty($nothi) || !isset($dak['totalinboxall'])
                        || !isset($nothi['totalsrijitonoteall'])) {
                        $data = [
                            'status' => 'error',
                            'msg' => 'Empty Data'
                        ];
                        $this->response->type('application/json');
                        $this->response->body(json_encode($data));
                        return $this->response;
                    }
                    if ($save_type == 'full') {
                        $save_data = [
                            'designation_id' => $designation_id, 'operation_date' => date('Y-m-d'),
                            'dak_inbox' => $dak['totalinboxall'], 'dak_outbox' => $dak['totaloutboxall'],
                            'nothivukto' => $dak['totalnothivuktoall'],
                            'nothijat' => $dak['totalnothijatoall'], 'dak_nisponno' => $dak['totalnisponnodakall'],
                            'dak_onisponno' => $dak['totalOnisponnodakall'],
                            'self_note' => $nothi['totalsrijitonoteall'], 'dak_note' => $nothi['totaldaksohonoteall'],
                            'note_nisponno' => $nothi['totalnisponnonoteall'],
                            'potrojari_nisponno' => $nothi['totalnisponnopotrojariall'],
                            'onisponno_note' => $nothi['totalOnisponnonoteall'],
                            'potrojari' => $dak['totalpotrojariall']
                        ];
                    } else {
                        $last_data = $dashboardDesignationsTable->getData($designation_id);
                        $save_data = [
                            'designation_id' => $designation_id, 'operation_date' => date('Y-m-d'),
                            'dak_inbox' => $dak['totalinboxall'] + $last_data['totalInbox'],
                            'dak_outbox' => $dak['totaloutboxall'] + $last_data['totalOutbox'],
                            'nothivukto' => $dak['totalnothivuktoall'] + $last_data['totalNothivukto'],
                            'nothijat' => $dak['totalnothijatoall'] + $last_data['totalNothijato'],
                            'dak_nisponno' => $dak['totalnisponnodakall'] + $last_data['totalNisponnoDak'],
                            'dak_onisponno' => $dak['totalOnisponnodakall'],
                            'self_note' => $nothi['totalsrijitonoteall'] + $last_data['totalSouddog'],
                            'dak_note' => $nothi['totaldaksohonoteall'] + $last_data['totalDaksohoNote'],
                            'note_nisponno' => $nothi['totalnisponnonoteall'] + $last_data['totalNisponnoNote'],
                            'potrojari_nisponno' => $nothi['totalnisponnopotrojariall']
                                + $last_data['totalNisponnoPotrojari'],
                            'onisponno_note' => $DashboardTable->getOnisponnoNoteCount($office_id, $unit_id, $designation_id, ['', date('Y-m-d', strtotime(date('Y-m-d') . ' -1 day'))]),
                            'potrojari' => $dak['totalpotrojariall'] + $last_data['totalPotrojari']
                        ];
                    }
                    $dashboardDesignationsTable->deleteAll(['designation_id' => $designation_id]);
                    $DashboardDesignationsEntity = $dashboardDesignationsTable->newEntity();
                    $DashboardDesignationsEntity->designation_id = $designation_id;
                    $DashboardDesignationsEntity->operation_date = $save_data['operation_date'];
                    $DashboardDesignationsEntity->dak_inbox = $save_data['dak_inbox'];
                    $DashboardDesignationsEntity->dak_outbox = $save_data['dak_outbox'];
                    $DashboardDesignationsEntity->nothivukto = $save_data['nothivukto'];
                    $DashboardDesignationsEntity->nothijat = $save_data['nothijat'];
                    $DashboardDesignationsEntity->dak_nisponno = $save_data['dak_nisponno'];
                    $DashboardDesignationsEntity->dak_onisponno = $save_data['dak_onisponno'];
                    $DashboardDesignationsEntity->self_note = $save_data['self_note'];
                    $DashboardDesignationsEntity->dak_note = $save_data['dak_note'];
                    $DashboardDesignationsEntity->potrojari_nisponno = $save_data['potrojari_nisponno'];
                    $DashboardDesignationsEntity->note_nisponno = $save_data['note_nisponno'];
                    $DashboardDesignationsEntity->onisponno_note = $save_data['onisponno_note'];
                    $DashboardDesignationsEntity->potrojari = $save_data['potrojari'];

                    $f = $dashboardDesignationsTable->save($DashboardDesignationsEntity);
                    if ($f == TRUE) {
                        $data = [
                            'status' => 'success',
                            'msg' => 'Update Complete'
                        ];
                    } else if ($f == FALSE) {
                        $data = [
                            'status' => 'error',
                            'msg' => 'Can not update'
                        ];
                    }
                } catch (\Exception $ex) {
                    $data = [
                        'status' => 'error',
                        'msg' => $ex->getMessage()
                    ];
                }
//                 saveData
            }
            $this->response->type('application/json');
            $this->response->body(json_encode($data));
            return $this->response;
        }
    }

    public function cronForDesignationDashboard()
    {
        ini_set('memory_limit', -1);
        set_time_limit(0);
        $start_time = strtotime(date('Y-m-d H:i:s'));
        TableRegistry::remove('EmployeeOffices');
        $employeeOfficesTable = TableRegistry::get('EmployeeOffices');
        $all_designations = $employeeOfficesTable->getAllDesignationID()->toArray();
        $data = [];
        $total = 0;
//            pr($all_offices);
        $DashboardTable = TableRegistry::get('Dashboard');
        TableRegistry::remove('DashboardDesignations');
        $dashboardDesignationsTable = TableRegistry::get('DashboardDesignations');
        TableRegistry::get('OfficeUnitOrganograms');
        $designationTable = TableRegistry::get('OfficeUnitOrganograms');
        foreach ($all_designations as $designation_id => $designation_name) {
            $total++;

            $f = $dashboardDesignationsTable->checkUpdate($designation_id,
                date('Y-m-d'));
            if ($f > 0) {
                continue;
            }
            $designation_info = $designationTable->get($designation_id);
            $office_id = $designation_info['office_id'];
            $unit_id = $designation_info['office_unit_id'];
            try {
                $dt = '';
                $this->switchOffice($office_id, 'DashboardOffice');
                $dak = $DashboardTable->OtherDashboardUpdateDak($office_id,
                    $unit_id, $designation_id, $dt);
                $nothi = $DashboardTable->OtherDashboardUpdateNothi($office_id,
                    $unit_id, $designation_id, $dt);
                $save_type = 'full';

                if (empty($dak) || empty($nothi) || !isset($dak['totalinboxall'])
                    || !isset($nothi['totalsrijitonoteall'])) {
                    $data[] = [
                        'office_id' => $office_id,
                        'unit_id' => $unit_id,
                        'designation_id' => $designation_id,
                        'designation_name' => $designation_name,
                        'msg' => 'Empty Data',
                    ];
                    continue;
                }
                if ($save_type == 'full') {
                    $save_data = [
                        'designation_id' => $designation_id, 'operation_date' => date('Y-m-d'),
                        'dak_inbox' => $dak['totalinboxall'], 'dak_outbox' => $dak['totaloutboxall'],
                        'nothivukto' => $dak['totalnothivuktoall'],
                        'nothijat' => $dak['totalnothijatoall'], 'dak_nisponno' => $dak['totalnisponnodakall'],
                        'dak_onisponno' => $dak['totalOnisponnodakall'],
                        'self_note' => $nothi['totalsrijitonoteall'], 'dak_note' => $nothi['totaldaksohonoteall'],
                        'note_nisponno' => $nothi['totalnisponnonoteall'],
                        'potrojari_nisponno' => $nothi['totalnisponnopotrojariall'],
                        'onisponno_note' => $nothi['totalOnisponnonoteall'],
                        'potrojari' => $dak['totalpotrojariall']
                    ];
                } else {
                    $last_data = $dashboardDesignationsTable->getData($designation_id);
                    $save_data = [
                        'designation_id' => $designation_id, 'operation_date' => date('Y-m-d'),
                        'dak_inbox' => $dak['totalinboxall'] + $last_data['totalInbox'],
                        'dak_outbox' => $dak['totaloutboxall'] + $last_data['totalOutbox'],
                        'nothivukto' => $dak['totalnothivuktoall'] + $last_data['totalNothivukto'],
                        'nothijat' => $dak['totalnothijatoall'] + $last_data['totalNothijato'],
                        'dak_nisponno' => $dak['totalnisponnodakall'] + $last_data['totalNisponnoDak'],
                        'dak_onisponno' => $dak['totalOnisponnodakall'],
                        'self_note' => $nothi['totalsrijitonoteall'] + $last_data['totalSouddog'],
                        'dak_note' => $nothi['totaldaksohonoteall'] + $last_data['totalDaksohoNote'],
                        'note_nisponno' => $nothi['totalnisponnonoteall'] + $last_data['totalNisponnoNote'],
                        'potrojari_nisponno' => $nothi['totalnisponnopotrojariall']
                            + $last_data['totalNisponnoPotrojari'],
                        'onisponno_note' => $nothi['totalOnisponnonoteall'] + $last_data['totalONisponno'],
                        'potrojari' => $dak['totalpotrojariall'] + $last_data['totalPotrojari']
                    ];
                }
                $dashboardDesignationsTable->deleteAll(['designation_id' => $designation_id]);
                $DashboardDesignationsEntity = $dashboardDesignationsTable->newEntity();
                $DashboardDesignationsEntity->designation_id = $designation_id;
                $DashboardDesignationsEntity->operation_date = $save_data['operation_date'];
                $DashboardDesignationsEntity->dak_inbox = $save_data['dak_inbox'];
                $DashboardDesignationsEntity->dak_outbox = $save_data['dak_outbox'];
                $DashboardDesignationsEntity->nothivukto = $save_data['nothivukto'];
                $DashboardDesignationsEntity->nothijat = $save_data['nothijat'];
                $DashboardDesignationsEntity->dak_nisponno = $save_data['dak_nisponno'];
                $DashboardDesignationsEntity->dak_onisponno = $save_data['dak_onisponno'];
                $DashboardDesignationsEntity->self_note = $save_data['self_note'];
                $DashboardDesignationsEntity->dak_note = $save_data['dak_note'];
                $DashboardDesignationsEntity->potrojari_nisponno = $save_data['potrojari_nisponno'];
                $DashboardDesignationsEntity->note_nisponno = $save_data['note_nisponno'];
                $DashboardDesignationsEntity->onisponno_note = $save_data['onisponno_note'];
                $DashboardDesignationsEntity->potrojari = $save_data['potrojari'];

                $f = $dashboardDesignationsTable->save($DashboardDesignationsEntity);
                if ($f == FALSE) {
                    $data[] = [
                        'office_id' => $office_id,
                        'unit_id' => $unit_id,
                        'designation_id' => $designation_id,
                        'designation_name' => $designation_name,
                        'msg' => 'Can not update'
                    ];
                    continue;
                }
            } catch (\Exception $ex) {
                $data[] = [
                    'office_id' => $office_id,
                    'unit_id' => $unit_id,
                    'designation_id' => $designation_id,
                    'designation_name' => $designation_name,
                    'msg' => $ex->getMessage()
                ];
                continue;
            }
        }
        if (NOTIFICATION_OFF == 0) {
            $response = "Total: " . $total;
            $end_time = strtotime(date('Y-m-d H:i:s'));
            $time_difference = round(abs(($end_time - $start_time) / 60), 2);
            $response .= "<br> Time: " . $time_difference . " minutes";
            if (!empty($data)) {
                $response .= '<br>Error list (' . count($data) . ') : <ul>';
                foreach ($data as $id) {
                    $response .= "<li>Office ID: " . $id['office_id'] . " Unit ID: " . $id['unit_id'] . " Designation ID: " . $id['designation_id'] . " Name: " . $id['designation_name'] . " Error: " . $id['msg'] . "</li><br>";
                }
                $response .= "</ul> ";
            } else {
                $response .= "No Error Occured";
            }
            $this->sendCronMessage('tusharkaiser@gmail.com',
                ['eather.ahmed@gmail.com'],
                'Cron Report ' . date('Y-m-d'), 'Designation Dashboard Report',
                $response);
//                 saveData
        }
        die;
    }

    public function officesReports()
    {
        $reportMarkingTable = TableRegistry::get('ReportMarking');
        $columns =[
            'dak_inbox','dak_outbox','dak_onisponno','dak_nothijat','dak_nothivukto','dak_nisponno',
            'nothi_self_note','nothi_dak_note','nothi_note_niponno',
            'nothi_potrojari_niponno','potrojari_nisponno_internal', 'potrojari_nisponno_external',
            'nothi_onisponno','nothi_nisponno','potrojari',
        ];
        $allreportMarking = $reportMarkingTable->getData($columns,['status' => 1])->first();
        $this->set(compact('allreportMarking','columns'));
    }

    public function officesReportsContent()
    {
        if ($this->request->is('post')) {
            $office_id = $this->request->data['office_id'];
            $type = $this->request->data['type'];
            $time[0] = $this->request->data['date_start'];
            $time[1] = $this->request->data['date_end'];
            $performanceOfficesTable = TableRegistry::get('PerformanceOffices');
//            $time = [ '2016-11-01' , '2016-11-30'];
            try {
                $this->switchOffice($office_id, 'DashboardReport');
                if ($type == 'dak') {
                    $condition = [
                        'totalInbox' => 'SUM(inbox)', 'totalNothijato' => 'SUM(nothijat)',
                        'totalNothivukto' => 'SUM(nothivukto)',
                        'totalID' => 'count(id)'
                    ];

                    $result = $performanceOfficesTable->getOfficeData($office_id,
                        $time)->select($condition)->group(['office_id'])->first();
                    if (!empty($result['totalID']) && $result['totalID'] > 0) {
                        $data = $result;
                    } else {
                        $data = 'false';
                    }
//                    $data = $this->OfficeDashboardUpdateDak($office_id, 0, 0,
//                        $time);
                } else {
                    $condition = [
                        'selfNote' => 'SUM(selfnote)',
                        'dakNote' => 'SUM(daksohonote)', 'NisponnoPotrojari' => 'SUM(nisponnopotrojari)',
                        'Potrojari' => 'SUM(potrojari)', 'NisponnoNote' => 'SUM(nisponnonote)',
                        'potrojari_nisponno_internal' => 'SUM(potrojari_nisponno_internal)', 'potrojari_nisponno_external' => 'SUM(potrojari_nisponno_external)',
                        'unassignedPendingDak' => 'SUM(unassigned_pending_dak)','unassignedPendingNote' => 'SUM(unassigned_pending_note)',
                        'unassignedDesignation' => 'SUM(unassigned_designation)',
                        'totalID' => 'count(id)'
                    ];

                    $result = $performanceOfficesTable->getOfficeData($office_id,
                        $time)->select($condition)->group(['office_id'])->first();
                    if (!empty($result['totalID']) && $result['totalID'] > 0) {
                         $result['lastUpdate'] =$performanceOfficesTable->getLastUpdateTime($office_id)['record_date'];
                        $result['lastUpdate'] =(!empty($result['lastUpdate']))? $result['lastUpdate']->format('Y-m-d'):'';
                        $result['office_id'] =$office_id;
                        $result['date_range'] = $time[0] .' - '.$time[1];
                        $result['NisponnoPotrojari'] = $result['potrojari_nisponno_internal'] +$result['potrojari_nisponno_external'];
                        $data = $result;
                    } else {
                        $data = 'false';
                    }
//                    $data = $this->nothiDashboardCount($office_id, 0, 0, $time);
//                         $data['selfNote']=   $nothiPartTable->selfSrijitoNoteCount($office_id,0, 0,$time);
//                           $data['dakNote']= $nothiPartTable->dakSrijitoNoteCount($office_id,0, 0,$time);
                }
            } catch (\Exception $ex) {
                $data = 'false';
            }

            $this->response->type('application/json');
            $this->response->body(json_encode($data));
            return $this->response;
        }
    }

    public function OfficeDashboardUpdateDak($officeid = 0, $unit_id = 0,
                                             $designation_id = 0, $time = [])
    {
        set_time_limit(0);
        ini_set('memory_limit', '-1');

        TableRegistry::remove('DakMovements');
        $DakMovementsTable = TableRegistry::get('DakMovements');
        TableRegistry::remove('Potrojari');
        $potrojariTable = TableRegistry::get('Potrojari');


        /*         * Inbox Total* */
        $totalprevinbox = $DakMovementsTable->countAllDakforOffice($officeid, 0,
            0, 'Inbox', $time);

        /*         * Inbox Total* */


        /*         * Nothivukto * */
        $totalprevnothivukto = $DakMovementsTable->countAllDakforOffice($officeid,
            0, 0, 'NothiVukto', $time);

        /*         * Nothivukto * */

        /*         * Nothijato * */
        $totalprevnothijato = $DakMovementsTable->countAllDakforOffice($officeid,
            0, 0, 'NothiJato', $time);

        /*         * Nothijato * */

        /*         * Potrojari Nisponno* */
        $totalprepotrojari = $potrojariTable->allPotrojariCount($officeid, 0, 0,
            $time);

        /*         * Potrojari Nisponno* */

        /*         * Result* */

        $returnSummary['totalinboxall'] = $totalprevinbox;

        $returnSummary['totalnothivuktoall'] = $totalprevnothivukto;

        $returnSummary['totalnothijatoall'] = $totalprevnothijato;

        $returnSummary['totalpotrojariall'] = $totalprepotrojari;


        /* --- Dak Part End --- */


        return $returnSummary;
    }

    public function nothiDashboardCount($office_id = 0, $unit_id = 0,
                                        $designation_id = 0, $time = [])
    {
        set_time_limit(0);
        TableRegistry::remove('NothiMasterCurrentUsers');
        $NothiMasterCurrentUserTable = TableRegistry::get('NothiMasterCurrentUsers');
        $NisponnoRecordsTable = TableRegistry::get('NisponnoRecords');
        $NothiDakPotroMapsTable = TableRegistry::get('NothiDakPotroMaps');
        $nothiNotesTable = TableRegistry::get('NothiNotes');

        $getAllParts = $NothiMasterCurrentUserTable->getAllNothiPartNo($office_id,
            $unit_id, $designation_id, $time)->toArray();
        $self_note = 0;
        $dak_note = 0;
        $nisponno_potrojari = 0;
        $nisponno_note = 0;
        $onisponno = 0;
        if (!empty($getAllParts)) {

//            $total = 0;
//            $has = 0;
            foreach ($getAllParts as $parts) {
//                $total++;
                $part_no = $parts['nothi_part_no'];
                $part_finish = $parts['is_finished'];
                if ($part_finish == 0) {
                    $nisponno_count = $NisponnoRecordsTable->getNisponnoCount($office_id,
                        $unit_id, $designation_id, $time, $part_no);
                    if ($nisponno_count['potrojari'] + $nisponno_count['note'] >= 1) {
                        $first_nisponno = $NisponnoRecordsTable->getLastNisponno($office_id,
                            $unit_id, $designation_id, $part_no);
                        if ($first_nisponno['type'] == 'potrojari') {
                            $nisponno_potrojari++;
                        } else if ($first_nisponno['type'] == 'note') {
                            $nisponno_note++;
                        }
                        $part_from = $NothiDakPotroMapsTable->checkPartIsFromDak($part_no,
                            $first_nisponno['operaton_date'], 'previous');
                        if ($part_from >= 1) $dak_note++;
                        else $self_note++;
                        //Checking last Note type
                        $last_nisponno = $NisponnoRecordsTable->getLastNisponno($office_id,
                            $unit_id, $designation_id, $part_no);
                        $part_from = $NothiDakPotroMapsTable->checkPartIsFromDak($part_no,
                            $last_nisponno['operaton_date'], 'next');
                        if ($part_from >= 1) $dak_note++;
                        else $self_note++;
                        $onisponno++;
                    } //If this note does not have any nisponno record
                    else {
                        $note_exist = $nothiNotesTable->checkEmptyNote(0,
                            $part_no);
                        if ($note_exist) {
                            $part_from = $NothiDakPotroMapsTable->checkPartIsFromDak($part_no,
                                '', '');
                            if ($part_from >= 1) $dak_note++;
                            else $self_note++;
                            $onisponno++;
                        } else {
//                            $has++;
                        }
                    }
                } //Note is Currently in Nisponno State
                else {
                    $nisponno_count = $NisponnoRecordsTable->getNisponnoCount($office_id,
                        $unit_id, $designation_id, $time, $part_no);
                    if ($nisponno_count['potrojari'] + $nisponno_count['note'] >= 1) {
                        $first_nisponno = $NisponnoRecordsTable->getLastNisponno($office_id,
                            $unit_id, $designation_id, $part_no);
                        if ($first_nisponno['type'] == 'potrojari') {
                            $nisponno_potrojari++;
                        } else if ($first_nisponno['type'] == 'note') {
                            $nisponno_note++;
                        }
                        $part_from = $NothiDakPotroMapsTable->checkPartIsFromDak($part_no,
                            $first_nisponno['operaton_date'], 'previous');
                        if ($part_from >= 1) $dak_note++;
                        else $self_note++;
                        if ($nisponno_count['potrojari'] + $nisponno_count['note']
                            > 1) {
                            //Checking last Note type
                            $last_nisponno = $NisponnoRecordsTable->getLastNisponno($office_id,
                                $unit_id, $designation_id, $part_no);
                            $part_from = $NothiDakPotroMapsTable->checkPartIsFromDak($part_no,
                                $last_nisponno['operaton_date'], 'next');
                            if ($last_nisponno['type'] == 'potrojari') {
                                $nisponno_potrojari++;
                            } else if ($last_nisponno['type'] == 'note') {
                                $nisponno_note++;
                            }
                            if ($part_from >= 1) $dak_note++;
                            else $self_note++;
                        }
                    }
                }
            }
        }
        $data = [
            'selfNote' => $self_note,
            'dakNote' => $dak_note,
            'potrojariNisponno' => $nisponno_potrojari,
            'noteNisponno' => $nisponno_note,
            'onisponno' => $onisponno,
//            'total' => $total,
//            'noNotes' => $has,
        ];
//        if(!empty($office_id)){
//
//        }
        return $data;
    }

    public function checkArchive($office_id = 0)
    {
        set_time_limit(0);
        if ($this->request->is('post')) {
            $office_id = $this->request->data['office_id'];
        } else {
            die;
        }
        if (!empty($office_id)) {
            try {
                $this->switchOffice($office_id, 'Archive');
                TableRegistry::remove('NothiMasterMovements');
                TableRegistry::remove('NothiNotes');
                TableRegistry::remove('NothiMasterCurrentUsers');
                $nothiMasterCurrentUserTable = TableRegistry::get('NothiMasterCurrentUsers');
                $nothiMasterMovementsTable = TableRegistry::get('NothiMasterMovements');
                $nothiNotesTable = TableRegistry::get('NothiNotes');
                $NisponnoRecordsTable = TableRegistry::get('NisponnoRecords');

                $all_archive_onisponno_note = $nothiMasterCurrentUserTable->find()->select(['nothi_master_id',
                    'nothi_part_no', 'office_unit_id', 'office_unit_organogram_id',
                    'employee_id'])->where(['is_archive' => 1, 'is_finished' => 0,
                    'nothi_office' => $office_id])->toArray();
                $count = 0;
                $total = 0;
                $inbox = 0;
//               pr($all_archive_onisponno_note);die;
                if (!empty($all_archive_onisponno_note)) {
                    foreach ($all_archive_onisponno_note as $note_part) {
                        $total++;
//                       echo $note_part['nothi_part_no']."<br>";
                        $has_onnuched = $nothiNotesTable->checkEmptyNote(0,
                            $note_part['nothi_part_no']);
                        $cnt = $nothiMasterMovementsTable->find()->where(['nothi_part_no' => $note_part['nothi_part_no']])->count();
                        if ($cnt >= 2 && $has_onnuched > 0) {
                            $last_note = $nothiNotesTable->getLastNote($note_part['nothi_part_no']);
                            $last_note['note_no'] = !empty($last_note['note_no'])
                                ? $last_note['note_no'] : 0;
                            $nisponnoEntity = $NisponnoRecordsTable->newEntity();
                            $nisponnoEntity->nothi_master_id = $note_part['nothi_master_id'];
                            $nisponnoEntity->nothi_part_no = $note_part['nothi_part_no'];
                            $nisponnoEntity->type = 'note';
                            $nisponnoEntity->nothi_onucched_id = ($last_note['note_no']
                                != 0 && $last_note['note_no'] != -1) ? $last_note['note_no']
                                : null;
                            $nisponnoEntity->nothi_office_id = $office_id;
                            $nisponnoEntity->office_id = $office_id;
                            $nisponnoEntity->unit_id = $note_part['office_unit_id'];
                            $nisponnoEntity->designation_id = $note_part['office_unit_organogram_id'];
                            $nisponnoEntity->employee_id = $note_part['employee_id'];
                            $nisponnoEntity->operation_date = '2016-12-09 00:00:00';
                            if ($NisponnoRecordsTable->save($nisponnoEntity)) {
                                $nothiMasterCurrentUserTable->updateAll(['is_finished' => 1,
                                    'modified' => '2016-12-09 00:00:00'],
                                    ['nothi_master_id' => $note_part['nothi_master_id'],
                                        'nothi_part_no' => $note_part['nothi_part_no'],
                                        'nothi_office' => $office_id, 'is_archive' => 1]);
                                $count++;
                            }
                        } else {
                            $nothiMasterCurrentUserTable->updateAll(['is_archive' => 0,
                                'modified' => '2016-12-09 00:00:00'],
                                ['nothi_master_id' => $note_part['nothi_master_id'],
                                    'nothi_part_no' => $note_part['nothi_part_no'], 'nothi_office' => $office_id,
                                    'is_finished' => 0]);
                            $inbox++;
                        }
                    }
                }
                $data = [
                    'status' => 'success',
                    'msg' => "total: $total, nisponno: $count, Inbox: $inbox"
                ];
            } catch (\Exception $ex) {
                $data = [
                    'status' => 'error',
                    'msg' => $ex->getMessage()
                ];
            }
        } else {
            $data = [
                'status' => 'error',
                'msg' => 'No Office ID'
            ];
        }
        $this->response->type('application/json');
        $this->response->body(json_encode($data));
        return $this->response;
    }

    public function archiveUpdate()
    {
        $this->layout = 'dashboard';
        TableRegistry::remove('OfficeDomains');
        $office_domains = TableRegistry::get('OfficeDomains');
        $all_offices = $office_domains->find()->where(['status' => 1])->toArray();
        $this->set(compact('all_offices'));
    }

    public function layersReports()
    {
        $layerLevels = jsonA(OFFICE_LAYER_TYPE);
        $options = [];
        if(!empty($layerLevels)){
            foreach($layerLevels as $level){
                $options[$level] = __($level);
            }
        }
        $options['Other'] = __('Other');

        $reportMarkingTable = TableRegistry::get('ReportMarking');
        $columns =[
            'dak_inbox','dak_outbox','dak_onisponno','dak_nothijat','dak_nothivukto','dak_nisponno',
            'nothi_self_note','nothi_dak_note','nothi_note_niponno',
            'nothi_potrojari_niponno','potrojari_nisponno_internal', 'potrojari_nisponno_external',
            'nothi_onisponno','nothi_nisponno','potrojari',
        ];
        $allreportMarking = $reportMarkingTable->getData($columns,['status' => 1])->first();
        $this->set(compact('options','allreportMarking','columns'));
    }

    public function layerOffices()
    {
        $layerLevels = jsonA(OFFICE_LAYER_TYPE);
        $options = [];
        if(!empty($layerLevels)){
            foreach($layerLevels as $level){
                $options[$level] = __($level);
            }
        }
        $options['Other'] = __('Other');
        $this->set(compact('options'));
    }
    
    public function officeListDistrictWise()
    {
        $districtTable = TableRegistry::get('GeoDistricts');
        $options = $districtTable->find('list',['keyField'=>'id','valueField'=>'district_name_bng'])->toArray();
        $this->set(compact('options'));
        if($this->request->is(['post','put'])){
            $reqData = $this->request->data();
            $district_id =$reqData['district_id'];
            $officeTable = TableRegistry::get('Offices');
            $data = $officeTable->getOfficesByDistrict($reqData);
            $this->set('data',$data);
        }
    }

    public function potrojariPending()
    {
        if($this->Auth->user('user_role_id')<=2) {
            $options = ['Ministry' => __('Ministry'), 'Directorate' => __('Directorate'),
                'Divisional' => __('Divisional'), 'District' => __('District'), 'Upazilla' => __('Upazilla'),
                'Other' => __('Other')];
            $this->set(compact('options'));
        } else {
            $this->Flash->error(' দুঃক্ষিত! আপনি অনুমতি প্রাপ্তনন। ');
            return $this->redirect('/');

        }
    }

    public function layerOfficesLayerEdit($id)
    {
        $options = [1 => __('Ministry'), 2 => __('Directorate'),
            3 => __('Divisional'), 4 => __('District'), 5 => __('Upazilla'), 0 => __('Other')];

        if($this->request->is('get')){
            $data = $this->getLayerById($id);
            $custom_layer_data = TableRegistry::get('OfficeCustomLayers')->find('list',['keyField'=> 'id','valueField' =>'name'])->toArray();
            if(!empty($this->request->query['redirect'])){
                $this->set('redirect',$this->request->query['redirect']);
            }
        }

        $office_layers_entity = TableRegistry::get('OfficeLayers');
        if ($this->request->is(['post', 'put'])) {

            if (isset($this->request->data['layer']) && isset($this->request->data['layer_level'])) {

                $office_layer = $office_layers_entity->get($this->request->data['layer']);
                $office_layer->layer_level = $this->request->data['layer_level'];
                $custom_layer_level = !empty($this->request->data['custom_layer_level'])?$this->request->data['custom_layer_level']:'';
                $office_id = !empty($this->request->data['office_id'])?$this->request->data['office_id']:'';
                if(!empty($custom_layer_level) && intval($custom_layer_level) >0  && !empty($office_id)){
                    TableRegistry::get('Offices')->updateAll(['custom_layer_id' => $custom_layer_level],['id' => $office_id]);
                }else if(isset($custom_layer_level) && $custom_layer_level== -1){
                    TableRegistry::get('Offices')->updateAll(['custom_layer_id' => NUll],['id' => $office_id]);
                }
                if ($office_layers_entity->save($office_layer)) {
                    $this->Flash->set('অফিসের স্থর সফলভাবে পরিবর্তন হয়েছে। ', [
                        'element' => 'success'
                    ]);
                    if(empty($this->request->query['redirect'])){
                        $this->redirect(['action' => 'layerOffices']);
                    }else{
                        $this->redirect($this->request->query['redirect']);
                    }

                } else {
                    $this->Flash->set('দুঃখিত অফিসের স্থর পরিবর্তন করা সম্ভব হচ্ছে না। অনুগ্রহ করে কিছুক্ষন পর আবার চেস্টা করুন।', [
                        'element' => 'error'
                    ]);
                    if(empty($this->request->query['redirect'])){
                        $this->redirect(['action' => 'layerOffices']);
                    }else{
                        $this->redirect($this->request->query['redirect']);
                    }
                }
            } else {
                $this->Flash->set('দুঃখিত অফিসের স্থর পরিবর্তন করা সম্ভব হচ্ছে না। অনুগ্রহ করে কিছুক্ষন পর আবার চেস্টা করুন।', [
                    'element' => 'error'
                ]);
                if(empty($this->request->query['redirect'])){
                    $this->redirect(['action' => 'layerOffices']);
                }else{
                    $this->redirect($this->request->query['redirect']);
                }
            }
        }


        $this->set(compact('options', 'data','custom_layer_data'));
    }

    public function getLayerById($id)
    {
        $office_ministries_table = TableRegistry::get('OfficeMinistries');
        $offices_info = $office_ministries_table->find()
            ->join([
                'OfficeLayers' => [
                    'table' => 'office_layers',
                    'type' => 'inner',
                    'conditions' => "OfficeLayers.office_ministry_id = OfficeMinistries.id"
                ],
                'OfficeOrigins' => [
                    'table' => 'office_origins',
                    'type' => 'inner',
                    'conditions' => "OfficeLayers.id = OfficeOrigins.office_layer_id"
                ],
                "Offices" => [
                    'table' => 'offices',
                    'type' => 'inner',
                    'conditions' => 'OfficeOrigins.id = Offices.office_origin_id'
                ]
            ])
            ->select(['Offices.id', 'OfficeLayers.id', 'OfficeLayers.layer_level', 'Offices.office_name_bng','Offices.custom_layer_id'])
            ->where(['Offices.id' => $id])
            ->first();
        return $offices_info;
    }

    public function getLayerwiseOffices()
    {
        $officesTable = TableRegistry::get('Offices');
        $officeLayersTable = TableRegistry::get('OfficeLayers');
        $ministry_id = isset($this->request->data['ministry_id'])?$this->request->data['ministry_id']:0;
        $data = $officeLayersTable->getLayerwiseOfficeEmployeeCount($this->request->data['office_layer_type'],$ministry_id);
        $this->response->type('application/json');
        $this->response->body(json_encode($data));
        return $this->response;
    }
    public function getLayerwiseOfficesPendingPotrojari()
    {
        if($this->Auth->user('user_role_id')<=2) {
            if($this->request->is('ajax')){
                $time[0] = isset($this->request->data['start_date'])?$this->request->data['start_date']:date('Y-m-d', strtotime("-2 days"));
                $time[1] =isset($this->request->data['end_date'])?$this->request->data['end_date']:date('Y-m-d');
                $office_id =  isset($this->request->data['office_id'])?$this->request->data['office_id']:0;
                $response = ['status' => 0,'msg' => 'Something Wrong'];
                try {
                    $this->switchOffice($office_id, 'DashboardOffices');
                    TableRegistry::remove('PotrojariReceiver');TableRegistry::remove('PotrojariOnulipi');TableRegistry::remove('Potrojari');
                    $p_receiver = TableRegistry::get('PotrojariReceiver');
                    $failed_Potrojari_receiver = $p_receiver->find()->select(['PotrojariReceiver.potrojari_id'])->where(['PotrojariReceiver.is_sent'=>0])->join([
                            "Potrojari" => [
                                'table' => 'potrojari', 'type' => 'inner',
                                'conditions' => ['Potrojari.id =PotrojariReceiver.potrojari_id']
                            ]
                        ])->where(['Potrojari.potro_status <>' => 'Draft']);
                    if(!empty($time[0])){
                        $failed_Potrojari_receiver = $failed_Potrojari_receiver->where(['date(PotrojariReceiver.created) >=' => $time[0]]);
                    }
                    if(!empty($time[1])){
                        $failed_Potrojari_receiver = $failed_Potrojari_receiver->where(['date(PotrojariReceiver.created) <=' => $time[1]]);
                    }
                    $receiver_potrojari_id= $failed_Potrojari_receiver->toArray();
                    $receiver_count = count($receiver_potrojari_id);

                    $p_onulipi = TableRegistry::get('PotrojariOnulipi');
                    $failed_Potrojari_onulipi = $p_onulipi->find()->select(['PotrojariOnulipi.potrojari_id'])->where(['PotrojariOnulipi.is_sent'=>0])->join([
                            "Potrojari" => [
                                'table' => 'potrojari', 'type' => 'inner',
                                'conditions' => ['Potrojari.id =PotrojariOnulipi.potrojari_id']
                            ]
                        ])->where(['Potrojari.potro_status <>' => 'Draft']);
                    if(!empty($time[0])){
                        $failed_Potrojari_onulipi = $failed_Potrojari_onulipi->where(['date(PotrojariOnulipi.created) >=' => $time[0]]);
                    }
                    if(!empty($time[1])){
                        $failed_Potrojari_onulipi = $failed_Potrojari_onulipi->where(['date(PotrojariOnulipi.created) <=' => $time[1]]);
                    }
                    $onulipi_potrojari_id = $failed_Potrojari_onulipi->toArray();
                    $onulipi_count= count($onulipi_potrojari_id);
                    $potrojari_ids = array_unique(array_merge($receiver_potrojari_id, $onulipi_potrojari_id));
                    $ids=[];
                    foreach ($potrojari_ids as $potrojari_id){
                        $ids[]=$potrojari_id['potrojari_id'];
                    }

                    $response = ['status' => 1,'receiver' => $receiver_count,'onulipi'=>$onulipi_count,'potrojari_id'=>json_encode($ids)];
                } catch (\Exception $ex) {
                    $response = ['status' => 0,'msg' => $ex->getMessage()];
                }

            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        }
        }
    }

    public function MonitorSummaryUpdate()
    {
        $monitorOfficesTable = TableRegistry::get('MonitorOffices');
        $monitorOfficeSummaryTable = TableRegistry::get('MonitorOfficeSummary');
        $officeDomainsTable = TableRegistry::get('OfficeDomains');
        $table_eo = TableRegistry::get('employee_offices');
        $user_login_history_table = TableRegistry::get('UserLoginHistory');
        $all_offices = $officeDomainsTable->getOfficesData();
        if (!empty($all_offices)) {
            foreach ($all_offices as $k => $v) {

                $employee_records = $table_eo->getAllEmployeeRecordID($k);
                $total_employee = count($employee_records);
                $totalLogin = $user_login_history_table->countLogin(array_values($employee_records));

                $office_report = $monitorOfficesTable->getOfficeData($k)->first();

                $monitorOfficeSummaryTable->deleteDataofOffices($k);

                $monitorOfficeSummaryEntity = $monitorOfficeSummaryTable->newEntity();
                $monitorOfficeSummaryEntity->office_id = $office_report['office_id'];
                $monitorOfficeSummaryEntity->ministry_id = $office_report['office_id'];
                $monitorOfficeSummaryEntity->layer_id = $office_report['office_id'];
                $monitorOfficeSummaryEntity->origin_id = $office_report['office_id'];
                $monitorOfficeSummaryEntity->total = $total_employee;
                $monitorOfficeSummaryEntity->today = $totalLogin;
                $monitorOfficeSummaryEntity->yesterdayinbox = $office_report['yesterdayinbox'];
                $monitorOfficeSummaryEntity->yesterdayoutbox = $office_report['yesterdayoutbox'];
                $monitorOfficeSummaryEntity->yesterdaynothijat = $office_report['yesterdaynothijat'];
                $monitorOfficeSummaryEntity->yesterdaynothivukto = $office_report['yesterdaynothivukto'];
                $monitorOfficeSummaryEntity->totalnisponnodak = $office_report['totalnisponnodak'];
                $monitorOfficeSummaryEntity->totalonisponnodak = $office_report['totalonisponnodak'];
                $monitorOfficeSummaryEntity->yesterdayselfnote = $office_report['yesterdayselfnote'];
                $monitorOfficeSummaryEntity->yesterdaydaksohonote = $office_report['yesterdaydaksohonote'];
                $monitorOfficeSummaryEntity->yesterdaypotrojari = $office_report['yesterdaypotrojari'];
                $monitorOfficeSummaryEntity->totalnisponnonote = $office_report['totalnisponnonote'];
                $monitorOfficeSummaryEntity->totalonisponnonote = $office_report['totalonisponnonote'];
                $monitorOfficeSummaryEntity->totalnisponno = $office_report['totalnisponno'];
                $monitorOfficeSummaryEntity->totalonisponno = $office_report['totalonisponno'];
                $monitorOfficeSummaryEntity->updated = date('Y-m-d');
                $monitorOfficeSummaryEntity->status = $office_report['status'];

                $monitorOfficeSummaryTable->save($monitorOfficeSummaryEntity);
            }
        }

    }

    public function migratePotrojariNisponno($office_id = 0)
    {
        if (empty($office_id)) {
            die;
        }
        set_time_limit(0);
        Time::setToStringFormat('yyyy-MM-dd');
        $this->switchOffice($office_id, 'DashboardOffice');
        $PotrojariTable = TableRegistry::get('Potrojari');
        $NisponnoRecordsTable = TableRegistry::get('NisponnoRecords');
        $PerformanceOfficesTable = TableRegistry::get('PerformanceOffices');
        $PerformanceUnitsTable = TableRegistry::get('PerformanceUnits');
        $PerformanceDesignationsTable = TableRegistry::get('PerformanceDesignations');
        $total = 0;
        $all_potrojaris = $PotrojariTable->find()->select(['nothi_master_id',
            'nothi_part_no',
            'nothi_notes_id',
            'officer_id', 'office_id', 'office_unit_id', 'officer_designation_id',
            'potrojari_date' => 'date(potrojari_date)'])->where(['potro_status' => 'sent'])->toArray();
        if (!empty($all_potrojaris)) {
            foreach ($all_potrojaris as $potrojari) {
                $time = new Time($potrojari['potrojari_date']);
                $potrojari['potrojari_date'] = $time->i18nFormat(null, null,
                    'en-US');
                $nisponnoEntity = $NisponnoRecordsTable->newEntity();
                $nisponnoEntity->nothi_master_id = $potrojari['nothi_master_id'];
                $nisponnoEntity->nothi_part_no = $potrojari['nothi_part_no'];
                $nisponnoEntity->type = 'potrojari';
                $nisponnoEntity->nothi_onucched_id = $potrojari['nothi_notes_id'];
                $nisponnoEntity->nothi_office_id = $potrojari['office_id'];
                $nisponnoEntity->office_id = $potrojari['office_id'];
                $nisponnoEntity->unit_id = $potrojari['office_unit_id'];
                $nisponnoEntity->designation_id = $potrojari['officer_designation_id'];
                $nisponnoEntity->employee_id = $potrojari['officer_id'];
                $nisponnoEntity->operation_date = $potrojari['potrojari_date'];
                $NisponnoRecordsTable->save($nisponnoEntity);

                $getPerformanceOfficeInfo = $PerformanceOfficesTable->find()->select(['id', 'selfnote', 'nisponnopotrojari', 'nisponno'])->where(['record_date' => $potrojari['potrojari_date'], 'office_id' => $office_id])->first();
                if (!empty($getPerformanceOfficeInfo)) {
                    $getPerformanceOfficeInfo->nisponnopotrojari = $getPerformanceOfficeInfo->nisponnopotrojari + 1;
                    $getPerformanceOfficeInfo->selfnote = $getPerformanceOfficeInfo->selfnote + 1;
                    $getPerformanceOfficeInfo->nisponno = $getPerformanceOfficeInfo->nisponno + 1;
                    $PerformanceOfficesTable->save($getPerformanceOfficeInfo);
                }
                $getPerformanceUnitInfo = $PerformanceUnitsTable->find()->select(['id', 'selfnote', 'nisponnopotrojari', 'nisponno'])->where(['record_date' => $potrojari['potrojari_date'], 'office_id' => $office_id, 'unit_id' => $potrojari['office_unit_id']])->first();
                if (!empty($getPerformanceUnitInfo)) {
                    $getPerformanceUnitInfo->nisponnopotrojari = $getPerformanceUnitInfo->nisponnopotrojari + 1;
                    $getPerformanceUnitInfo->selfnote = $getPerformanceUnitInfo->selfnote + 1;
                    $getPerformanceUnitInfo->nisponno = $getPerformanceUnitInfo->nisponno + 1;
                    $PerformanceUnitsTable->save($getPerformanceUnitInfo);

                }
                $getPerformanceDesignationInfo = $PerformanceDesignationsTable->find()->select(['id', 'selfnote', 'nisponnopotrojari', 'nisponno'])->where(['record_date' => $potrojari['potrojari_date'], 'office_id' => $office_id, 'unit_id' => $potrojari['office_unit_id'], 'designation_id' => $potrojari['officer_designation_id']])->first();
                if (!empty($getPerformanceDesignationInfo)) {
                    $getPerformanceDesignationInfo->nisponnopotrojari = $getPerformanceDesignationInfo->nisponnopotrojari + 1;
                    $getPerformanceDesignationInfo->selfnote = $getPerformanceDesignationInfo->selfnote + 1;
                    $getPerformanceDesignationInfo->nisponno = $getPerformanceDesignationInfo->nisponno + 1;
                    $PerformanceDesignationsTable->save($getPerformanceDesignationInfo);

                }
                $total++;
            }
        }
        echo $total;
        die;
    }

    public function getOfficeUnits()
    {
        $table_instance = TableRegistry::get('OfficeUnits');
        $office_id = $this->request->data['office_id'];
        $office_units = $table_instance->getOfficeUnitsList($office_id);
        $this->response->type('application/json');
        $this->response->body(json_encode($office_units));
        return $this->response;
    }

    public function cronLogReport($start = '', $end = '')
    {
        if ($this->request->is('post')) {
            $this->layout = 'ajax';
            $cronLogTable = TableRegistry::get('CronLog');
            $allData = $this->paginate($cronLogTable->getData(0, ['start' => $start, 'end' => $end], '')->order(['operation_date desc']));
//            pr($allData);die;
            $this->set(compact('allData'));
            $this->render('cron_log_report_content');
        }
    }

    public function cronLogDetails($id = 0)
    {
        if (empty($id)) {
            $this->Flash->error(' কোন তথ্য পাওয়া যায়নি। ');
            return $this->redirect(['controller' => 'Cron', 'action' => 'cronLogReport']);
        }
        $cronLogTable = TableRegistry::get('CronLog');
        $allData = $cronLogTable->getData($id)->first();
        if (!empty($allData)) {
            $this->set(compact('allData'));
        } else {
            $this->Flash->error(' কোন তথ্য পাওয়া যায়নি। ');
            return $this->redirect(['controller' => 'Cron', 'action' => 'cronLogReport']);
        }
    }

    public function generateMonitoringDashboardMonthlyReport()
    {

    }
//    public function pera(){
//        $officesTable = TableRegistry::get('Offices');
////        $officesTable = TableRegistry::get('OfficeFrontDesk');
//
//        $allData = $officesTable->find()->select(['OfficeMinistries.name_bng','OfficeLayers.layer_name_bng','Offices.office_name_bng'])->join([
//                            "OfficeFrontDesk" => [
//                                'table' => 'office_front_desk', 'type' => 'left',
//                                'conditions' => ['OfficeFrontDesk.office_id = Offices.id']
//                            ],
//                              "OfficeMinistries" => [
//                                'table' => 'office_ministries', 'type' => 'left',
//                                'conditions' => ['OfficeMinistries.id =Offices.office_ministry_id']
//                            ],
//                              "OfficeLayers" => [
//                                'table' => 'office_layers', 'type' => 'left',
//                                'conditions' => ['OfficeLayers.id =Offices.office_layer_id']
//                            ],
//                        ])->where(['OfficeFrontDesk.office_id is NULL','Offices.active_status' => 1,'Offices.office_ministry_id IN' => [13,41,5,45,33,37,14,15,20,22,38,46,39,4,34,3,32,12,17,58,48,7,18,27]])->order(['Offices.office_ministry_id'])->group(['Offices.id']);
//        pr($allData);die;
//    }
    public function reportTillNow()
    {
        $office_domains = TableRegistry::get('OfficeDomains');
        $all_offices = $office_domains->getOfficesData();
        $this->set(compact('all_offices'));
    }

    public function reportTillNowContent()
    {
        set_time_limit('0');
        ini_set('memory_limit', '-1');
        $office_id = $this->request->data['office_id'];
        $data = ['status' => 0, 'msg' => 'Something Wrong'];
        if (($data = Cache::read('report_till_now_' . $office_id, 'memcached')) === false) {
            //            $result = $table_reports->officeSummary($officeid);
            $tbl_DO = TableRegistry::get('DashboardOffices');
            try {
                $this->switchOffice($office_id, 'DashboardOffices');
                $dak_track_table = TableRegistry::get('DakTracks');
                TableRegistry::remove('PotrojariReceiver');
                TableRegistry::remove('PotrojariOnulipi');
                TableRegistry::remove('DakNagoriks');
                $dak_nagorik = TableRegistry::get('DakNagoriks');
                $receiver_own = TableRegistry::get('PotrojariReceiver')->find()->where(['receiving_office_id' => $office_id])->count();
                $receiver_other = TableRegistry::get('PotrojariReceiver')->find()->where(['receiving_office_id <>' => $office_id])->count();
                $onulipi_own = TableRegistry::get('PotrojariOnulipi')->find()->where(['receiving_office_id' => $office_id])->count();
                $onulipi_other = TableRegistry::get('PotrojariOnulipi')->find()->where(['receiving_office_id <>' => $office_id])->count();
                $nagorik_dak_total = $dak_nagorik->find()->count();
                $reportDataFromserver = $tbl_DO->find()->select(['potrojari_nisponno', 'note_nisponno',
                    'potrojari'])->where(['office_id' => $office_id])->first();
                $nagorik_dak_online = $dak_nagorik->find()->where(['dak_type_id' => 2])->count();
                $nagorik_dak_portal = $dak_track_table->find()->where(['service_name' => 'online.forms.gov.bd', 'receiving_office_id' => $office_id, 'dak_type' => 'Nagorik'])->count();
                if (empty($reportDataFromserver['potrojari_nisponno'])) {
                    $reportDataFromserver['potrojari_nisponno'] = 0;
                }
                if (empty($reportDataFromserver['note_nisponno'])) {
                    $reportDataFromserver['note_nisponno'] = 0;
                }
                if (empty($reportDataFromserver['potrojari'])) {
                    $reportDataFromserver['potrojari'] = 0;
                }
                $data = [
                    'nisponno' => $reportDataFromserver['potrojari_nisponno'] + $reportDataFromserver['note_nisponno'],
                    'potrojari' => $reportDataFromserver['potrojari'],
                    'nagorik_dak_online' => $nagorik_dak_online,
                    'nagorik_dak_portal' => $nagorik_dak_portal,
                    'portojari_receiver' => $receiver_own + $receiver_other,
                    'potrojari_onulipi' => $onulipi_own + $onulipi_other,
                    'nagorik_total' => $nagorik_dak_total,
                    'status' => 1
                ];
                Cache::write('report_till_now_' . $office_id, $data, 'memcached');


            } catch (\Exception $ex) {
                $data = ['status' => 0, 'msg' => $ex->getMessage()];
            }

        }
        $this->response->type('application/json');
        $this->response->body(json_encode($data));
        return $this->response;
    }

    public function potrojariTracking()
    {
        $auth = $this->Auth->user();
        if ($auth['user_role_id'] > 2) {
            $this->Flash->error('Unauthorized Access');
            return $this->redirect(['controller' => 'Users', 'action' => 'login']);
        }
        if ($this->request->is('post')) {
            set_time_limit('0');
            ini_set('memory_limit', '-1');
            $office_id = $this->request->data['office_id'];
            $time[0] = isset($this->request->data['start_date']) ? $this->request->data['start_date'] : '';
            $time[1] = isset($this->request->data['end_date']) ? $this->request->data['end_date'] : '';
            $office_name = isset($this->request->data['office_name']) ? $this->request->data['office_name'] : '';
            $this->set('start_date', $time[0]);
            $this->set('end_date', $time[1]);
            $data = ['status' => 0, 'msg' => 'Something Wrong'];
            $this->set(compact(['office_id', 'start_date', 'end_date', 'office_name']));
            try {
                $this->switchOffice($office_id, 'DashboardOffices');
                TableRegistry::remove('PotrojariReceiver');
                TableRegistry::remove('PotrojariOnulipi');
                TableRegistry::remove('Potrojari');
                TableRegistry::remove('NisponnoRecords');
                $potrojariTable = TableRegistry::get('Potrojari');
                $data['status'] = 1;
                $total_potrojari_array = $potrojariTable->allPotrojariIDArray($office_id, 0, 0, $time);
                $data['total_potrojari'] = count($total_potrojari_array);
                $data['potrojari_internal'] = $potrojariTable->allPotrojariInternalCountByPotrojariId($total_potrojari_array, $time, 0);
                $data['potrojari_external'] = $data['total_potrojari'] - $data['potrojari_internal'];
                if ($data['potrojari_external'] < 0) {
                    $data['potrojari_external'] = 0;
                }

            } catch (\Exception $ex) {
                $data = ['status' => 0, 'msg' => $ex->getMessage()];
            }
            $this->layout = 'ajax';
            $this->set(compact('data'));
            $this->render('potrojari_tracking_content');
        }
    }

    public function potrojariTrackingDetails()
    {
        $auth = $this->Auth->user();
        if ($auth['user_role_id'] > 2) {
            $this->Flash->error('Unauthorized Access');
            return $this->redirect(['controller' => 'Users', 'action' => 'login']);
        }
        if ($this->request->is('post')) {
            set_time_limit('0');
            ini_set('memory_limit', '-1');
            $office_id = $this->request->data['office_id'];
            $office_name = isset($this->request->data['office_name']) ? $this->request->data['office_name'] : '';
            $time[0] = isset($this->request->data['start_date']) ? $this->request->data['start_date'] : '';
            $time[1] = isset($this->request->data['end_date']) ? $this->request->data['end_date'] : '';
            $data = ['status' => 0, 'msg' => 'Something Wrong'];
            $this->set(compact(['office_id', 'time', 'office_name']));
            try {
                $this->switchOffice($office_id, 'DashboardOffices');
                TableRegistry::remove('Potrojari');
                $data['status'] = 1;
                $query = TableRegistry::get('Potrojari')
                    ->find()->select(['id', 'approval_officer_name', 'approval_office_unit_name', 'approval_officer_designation_label', 'approval_office_name', 'approval_office_id', 'Potrojari.officer_designation_label','Potrojari.office_unit_name','Potrojari.officer_name','Potrojari.nothi_part_no','potro_subject', 'potrojari_date', 'created', 'potro_type'])->contain([
                'PotrojariReceiver' => [
                     'fields' => [
                     'PotrojariReceiver.receiving_officer_name','PotrojariReceiver.receiving_office_unit_name',
                     'PotrojariReceiver.receiving_officer_designation_label','PotrojariReceiver.potrojari_id','PotrojariReceiver.officer_mobile',
                     'PotrojariReceiver.receiving_officer_email','PotrojariReceiver.receiving_officer_designation_id'
                    ]
                ],
                'PotrojariOnulipi' =>   [
                      'fields' => [
                          'PotrojariOnulipi.receiving_officer_name','PotrojariOnulipi.receiving_office_unit_name',
                          'PotrojariOnulipi.receiving_officer_designation_label','PotrojariOnulipi.potrojari_id', 'PotrojariOnulipi.officer_mobile',
                          'PotrojariOnulipi.receiving_officer_email','PotrojariOnulipi.receiving_officer_designation_id'
                    ]
                ]
                ])->where(["Potrojari.potro_status" => DAK_CATEGORY_SENT, 'date(Potrojari.potrojari_date) >=' => $time[0], 'date(Potrojari.potrojari_date) <=' => $time[1]])
                    ->order(['Potrojari.potrojari_date','Potrojari.nothi_part_no']);

                $data['p_templates'] = TableRegistry::get('PotrojariTemplates')->find('list', ['keyField' => 'id', 'valueField' => 'template_name'])->toArray();

                if (!empty($this->request->query['downloadxls']) && $this->request->query['downloadxls'] == 'true') {
                    $this->layout = null;

                    $this->set('start_date', $this->EngToBng($time[0]));
                    $this->set('end_date', $this->EngToBng($time[1]));
                    $total_potrojari = (isset($this->request->data['total_potrojari']) ? $this->request->data['total_potrojari'] : 0);
                    $potrojari_internal = (isset($this->request->data['potrojari_internal']) ? $this->request->data['potrojari_internal'] : 0);
                    $potrojari_external = (isset($this->request->data['potrojari_external']) ? $this->request->data['potrojari_external'] : 0);
                    $this->set('total_potrojari', (isset($this->request->data['total_potrojari']) ? $this->request->data['total_potrojari'] : 0));
                    $this->set(compact('potrojari_internal'));
                    $this->set(compact('potrojari_external'));
                    $data['data'] = $query;
                    $this->potrojariTrackingExcel($office_name, $time[0], $time[1], $total_potrojari, $potrojari_internal, $potrojari_external, $data);
                } else {
                    $obj = $this->Paginator->paginate($query, ['limit' => 20]);
//                    $obj = $query->toArray();
                    $data['data'] = $obj;
                }
            } catch (\Exception $ex) {
                $data = ['status' => 0, 'msg' => $ex->getMessage()];
            }
            $this->layout = 'ajax';
            $this->set(compact('data'));
        }
    }

    public function potrojariDetails($office_id, $potrojari_id)
    {
        $this->response->type('application/json');
        try {
            $this->switchOffice($office_id, 'DashboardOffices');
            $potrojari = TableRegistry::get('Potrojari')->find()->select(['potro_description'])->where(['id' => $potrojari_id])->first();
            $this->response->body(json_encode(['status' => 'success', 'data' => $potrojari['potro_description']]));
        } catch (\Exception $ex) {
            $this->response->body(json_encode(['status' => 'error', 'data' => $ex->getMessage()]));
        }
        return $this->response;

    }

    public function designationTracking()
    {
        $start_date = '';
        $end_date = '';
        if ($this->request->is('post')) {
            try {
                $table = TableRegistry::get('DesignationUpdateHistory');
                $condition = '1';
               
                
               

                $query = $table->find()
                    ->select(['old_designation_eng', 'old_designation_bng', 'designation_eng', 'designation_bng',
                        'Offices.office_name_bng', 'OfficeUnits.unit_name_bng','DesignationUpdateHistory.modified'])
                    ->join([
                        'Offices' => [
                            'table' => 'offices',
                            'type' => 'left',
                            'conditions' => [
                                'Offices.id = DesignationUpdateHistory.office_id'
                            ]
                        ],
                        'OfficeUnits' => [
                            'table' => 'office_units',
                            'type' => 'left',
                            'conditions' => [
                                'OfficeUnits.id = DesignationUpdateHistory.office_unit_id'
                            ]
                        ],
                    ]);
                    
                     
                    $query->where(['DesignationUpdateHistory.office_id'=>$this->request->data['office_id']]);
                    
                    if (!empty($this->request->data['office_unit_id'])) {
                        $query->where(['DesignationUpdateHistory.office_unit_id'=>$this->request->data['office_unit_id']]);
                    }
                    if (!empty($this->request->data['office_designation_id'])) {
                       $query->where(['DesignationUpdateHistory.designation_id'=>$this->request->data['office_designation_id']]);
                    }
                    if (!empty($this->request->data['start_date'])) {
                        $query->where(['date(DesignationUpdateHistory.created) >= '=>$this->request->data['start_date']]);
                        $start_date = $this->request->data['start_date'];
                    }
                    if (!empty($this->request->data['end_date'])) {
                        $query->where(['date(DesignationUpdateHistory.created) <= '=>$this->request->data['end_date']]);
                        $end_date = $this->request->data['end_date'];
                    }
   
                    $this->set(compact(['start_date', 'end_date']));
                $designationHistory = $this->Paginator->paginate($query, ['limit' => 50]);
                $this->set(compact(['designationHistory']));
                $this->layout = 'ajax';
                $this->render('designation_tracking_detail');
            } catch (\Exception $ex) {
                pr($ex->getMessage());
                die;
            }
        }
        $this->set(compact(['start_date', 'end_date']));
    }

    public function unitTracking()
    {
        $start_date = '';
        $end_date = '';
        if ($this->request->is('post')) {
            try {
                $condition = '1';
               
                $table = TableRegistry::get('UnitUpdateHistory');

                $query = $table->find()
                    ->select(['old_unit_eng', 'old_unit_bng', 'unit_eng', 'unit_bng',
                        'Offices.office_name_bng', 'OfficeUnits.unit_name_bng','UnitUpdateHistory.modified'])
                    ->join([
                        'Offices' => [
                            'table' => 'offices',
                            'type' => 'left',
                            'conditions' => [
                                'Offices.id = UnitUpdateHistory.office_id'
                            ]
                        ],
                        'OfficeUnits' => [
                            'table' => 'office_units',
                            'type' => 'left',
                            'conditions' => [
                                'OfficeUnits.id = UnitUpdateHistory.office_unit_id'
                            ]
                        ],
                    ]);

                    
                    $query->where(['UnitUpdateHistory.office_id'=>$this->request->data['office_id']]);
                    
                    if (!empty($this->request->data['office_unit_id'])) {
                        $query->where(['UnitUpdateHistory.office_unit_id'=>$this->request->data['office_unit_id']]);
                    }
                    if (!empty($this->request->data['start_date'])) {
                        $query->where(['date(UnitUpdateHistory.created) >= '=>$this->request->data['start_date']]);
                        $start_date = $this->request->data['start_date'];
                    }
                    if (!empty($this->request->data['end_date'])) {
                        $query->where(['date(UnitUpdateHistory.created) <= '=>$this->request->data['end_date']]);
                        $end_date = $this->request->data['end_date'];
                    }

                $this->set(compact(['start_date', 'end_date']));
                
                $unitHistory = $this->Paginator->paginate($query, ['limit' => 50]);
                $this->set(compact(['unitHistory']));
                $this->layout = 'ajax';
                $this->render('unit_tracking_detail');
            } catch (\Exception $ex) {
                pr($ex->getMessage());
                die;
            }
        }
        $this->set(compact(['start_date', 'end_date']));
    }

    private function potrojariTrackingExcel($office_name, $start_date, $end_date, $total_potrojari, $potrojari_internal, $potrojari_external, $data)
    {

        try {
            set_time_limit('0');
            ini_set('memory_limit', '-1');
            require ROOT . DS . 'vendor' . DS . 'phpoffice' . DS . 'phpexcel' . DS . 'Classes' . DS . 'PHPExcel.php';
            require ROOT . DS . 'vendor' . DS . 'phpoffice' . DS . 'phpexcel' . DS . 'Classes' . DS . 'PHPExcel' . DS . 'IOFactory.php';

            $book = new \PHPExcel();
            $text = $office_name . ' অফিসের পত্রজারি ট্র্যাকিং (' . enTobn($start_date) . ' - ' . enTobn($end_date) . ')';
            $book->getActiveSheet()->setTitle('Potrojari Tracking');
            $sheet = $book->getActiveSheet();

            $style = array(
                'font' => array('bold' => true),
                'alignment' => array('horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,),
            );
            $sheet->setCellValueByColumnAndRow(0, 1, $text); // Sets cell 'a1' to value 'ID
            $sheet->mergeCells('A1:D1');

            $sheet->setCellValueByColumnAndRow(0, 2, 'মোট পত্রজারি '); // Sets cell 'a1' to value 'ID
            $sheet->setCellValueByColumnAndRow(1, 2, 'মোট আন্তঃসিস্টেম পত্রজারি '); // Sets cell 'a1' to value 'ID
            $sheet->setCellValueByColumnAndRow(2, 2, 'মোট অন্যান্য ও ইমেইল পত্রজারি '); // Sets cell 'a1' to value 'ID
            $sheet->setCellValueByColumnAndRow(3, 2, 'মোট অন্য অফিসের পত্রজারি'
            ); // Sets cell 'a1' to value 'ID

            $data_tmp = array(enTobn($total_potrojari), enTobn($potrojari_internal), enTobn($potrojari_external));
            for ($col = 0; $col < 3; $col++) {
                $sheet->setCellValueByColumnAndRow($col, 3, $data_tmp[$col]);
            }

            $sheet->setCellValueByColumnAndRow(0, 6, ' পত্রজারি '); // Sets cell 'a1' to value 'ID
            $sheet->mergeCells('a6:e6');
            $sheet->setCellValueByColumnAndRow(0, 7, '  ধরন'); // Sets cell 'a1' to value 'ID
            $sheet->setCellValueByColumnAndRow(1, 7, '  বিষয় '); // Sets cell 'a1' to value 'ID
            $sheet->setCellValueByColumnAndRow(2, 7, ' প্রেরক'); // Sets cell 'a1' to value 'ID
            $sheet->setCellValueByColumnAndRow(3, 7, ' খসড়া তৈরি হয়েছে'); // Sets cell 'a1' to value 'ID
            $sheet->setCellValueByColumnAndRow(4, 7, ' সময়'); // Sets cell 'a1' to value 'ID

            $indx = 8;

            if ($data['status'] == 1) {
                $appdata = $data['data']->toArray();
                if (!empty($appdata)) {
                    $p_id = 0;
                    foreach ($appdata as $val) {

                        $i = 0;
                        $s_name = '';
                        $s_unit = '';
                        $s_designation = '';

                        if (!empty($val['approval_officer_name']) && $val['approval_officer_name'] != '0') {
                            $s_name = $val['approval_officer_name'] . ', ';
                        }
                        if (!empty($val['approval_officer_designation_label']) && $val['approval_officer_designation_label']
                            != '0') {
                            $s_designation = $val['approval_officer_designation_label'] . ', ';
                        }
                        if (!empty($val['approval_office_unit_name']) && $val['approval_office_unit_name']
                            != '0') {
                            $s_unit = $val['approval_office_unit_name'];
                        }
                        if ($val['PotrojariReceiver']['potrojari_id'] != $p_id) {
                            $p_id = $val['PotrojariReceiver']['potrojari_id'];
                            $i = 1;
                        }
                        $data_tmp = array($data['p_templates'][$val['potro_type']], $val['potro_subject'],
                            $s_name . $s_designation . $s_unit, $val['created'], $val['potrojari_date']);
                        for ($col = 0; $col < 5; $col++) {
                            $sheet->setCellValueByColumnAndRow($col, $indx, $data_tmp[$col]);
                        }
                        $indx++;

                        //                    $sheet->fromArray($data_tmp, ' ', 'A'.$indx++);
                    }
                } else {
                    $sheet->setCellValueByColumnAndRow(0, 5, 'কোন তথ্য পাওয়া যায়নি।');
                    $sheet->mergeCells('A8:E8');
                    for ($col = 0; $col < 5; $col++) {
                        $sheet->getStyleByColumnAndRow($col, 5)->applyFromArray(array(
                            'font' => array('bold' => true, 'size' => 12, 'color' => ['rgb' => 'ff0000']),
                            'alignment' => array('horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,),
                        ));
                    }
                }
            } else {
                $sheet->setCellValueByColumnAndRow(0, 5, 'কোন তথ্য পাওয়া যায়নি।');
                $sheet->mergeCells('A8:E8');
                for ($col = 0; $col < 5; $col++) {
                    $sheet->getStyleByColumnAndRow($col, 5)->applyFromArray(array(
                        'font' => array('bold' => true, 'size' => 12, 'color' => ['rgb' => 'ff0000']),
                        'alignment' => array('horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,),
                    ));
                }
            }

            for ($col = 0; $col < 5; $col++) {
                $sheet->getStyleByColumnAndRow($col, 1)->applyFromArray(array(
                    'font' => array('bold' => true, 'size' => 12),
                    'alignment' => array('horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,),
                ));
                $sheet->getStyleByColumnAndRow($col, 2)->applyFromArray($style);
                $sheet->getStyleByColumnAndRow($col, 3)->applyFromArray($style);
                $sheet->getStyleByColumnAndRow($col, 6)->applyFromArray($style);
                $sheet->getStyleByColumnAndRow($col, 7)->applyFromArray($style);
                $sheet->getColumnDimensionByColumn($col)->setAutoSize(true);
            }
            header("Content-Type: application/vnd.ms-excel");
            header("Content-Disposition: attachment; filename=\".$text.xls\"");
            header("Cache-Control: max-age=0");
            $writer = \PHPExcel_IOFactory::createWriter($book, 'Excel2007');

            // $writer->setIncludeCharts(true);
            ob_end_clean();
            $writer->save('php://output');
            exit;
        } catch (\Exception $ex) {
            pr($ex->getMessage());
        }

    }

    public function permittedReportModuleUser()
    {
        if ($this->request->is('get')) {
            $all_permitted_user = TableRegistry::get('ViewReports')->find()->toArray();
            $this->set(compact('all_permitted_user'));
        } else {
            $v_R_tbl = TableRegistry::get('ViewReports');
            $username = isset($this->request->data['username']) ? bnToen($this->request->data['username']) : '';
            if (empty($username)) {
                echo $this->Flash->error('ব্যবহারকারীর ইউজার আইডি দেওয়া হয় নি');
                return $this->redirect(['action' => 'permittedReportModuleUser']);
            }
            if (!is_numeric($username)) {
                echo $this->Flash->error('ব্যবহারকারীর ইউজার আইডি সঠিক নয়');
                return $this->redirect(['action' => 'permittedReportModuleUser']);
            }
            // creating a 12 Digit user Id
            $prefix = $username[0];
            $rest = substr($username, 1);
            $rest_num = intval($rest);
            $new_username = $prefix . str_pad($rest_num, 11, '0', STR_PAD_LEFT);
            $has_entry = $v_R_tbl->find()->where(['username' => $new_username])->count();
            if ($has_entry > 0) {
                echo $this->Flash->error('পুরবেই অন্তর্ভুক্ত করা হয়েছে ');
                return $this->redirect(['action' => 'permittedReportModuleUser']);
            }
            $creator = $this->Auth->user()['id'];
            $v_R_entity = $v_R_tbl->newEntity();
            $v_R_entity->username = $new_username;
            $v_R_entity->created_by = $creator;
            $v_R_entity->modified_by = $creator;
            if ($v_R_tbl->save($v_R_entity)) {
                echo $this->Flash->success('অন্তর্ভুক্ত করা হয়েছে ');
                return $this->redirect(['action' => 'permittedReportModuleUser']);
            }
        }
    }

    public function deletepermittedReportModuleUser()
    {
        if ($this->request->is('post')) {
            $response = [
                'status' => 'error',
                'msg' => 'something went wrong'
            ];
            $this->response->type('application/json');
            $id = isset($this->request->data['id']) ? $this->request->data['id'] : '';
            $username = isset($this->request->data['id']) ? $this->request->data['username'] : '';
            $v_R_tbl = TableRegistry::get('ViewReports');
            if (empty($username) || empty($id)) {
                $response['msg'] = 'ব্যবহারকারীর ইউজার আইডি দেওয়া হয় নি';
                goto rtn;
            }
            if (!is_numeric($username)) {
                $response['msg'] = 'ব্যবহারকারীর ইউজার আইডি সঠিক নয়';
                goto rtn;
            }
            if ($v_R_tbl->deleteAll(['id' => $id, 'username' => $username])) {
                $response = [
                    'status' => 'success',
                    'msg' => ' অনুমতি বাতিল করা হয়েছে। পরবর্তী লগইন থেকে  ব্যবহারকারী রিপোর্ট মডুল পাবে না।'
                ];

            }
            goto rtn;
            rtn:
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        }
    }

    public function redundantNid()
    {
        $search = '';
        try {
            $employeeRecordTable = TableRegistry::get('EmployeeRecords');
            $query = $employeeRecordTable->find()->select(['nid'])->where(['nid >' => 0])->group(['nid'])->having(['count(nid) > 1']);

            if (!empty($this->request->query['search'])) {
                $search = $this->BngToEng($this->request->query['search']);
                $duplicate_Nid_numbers = $this->paginate($query->where(['nid like' => '%' . $search . '%']));
            } else {
                $duplicate_Nid_numbers = $this->paginate($query);

            }

            $duplicate_Nid = [];
            if (!empty($duplicate_Nid_numbers)) {
                foreach ($duplicate_Nid_numbers as $D_k => $D_v) {
                    $duplicate_Nid [] = $employeeRecordTable->find()->select(['name_eng', 'name_bng', 'date_of_birth', 'nid', 'is_cadre', 'identity_no', 'username' => 'Users.username', 'alias' => 'Users.user_alias', 'EmployeeRecords.created'])->where(['nid' => $D_v['nid']])->join([
                        "Users" => [
                            'table' => 'users', 'type' => 'inner',
                            'conditions' => ['Users.employee_record_id =EmployeeRecords.id']
                        ]
                    ])->toArray();
                }
            }
            $this->set(compact('duplicate_Nid','search'));
        } catch (Exception $ex) {
            return;
        }

    }

    public function nagorikAbedon()
    {
        if ($this->request->is('post')) {
            set_time_limit('0');
            ini_set('memory_limit', '-1');
            $office_id = isset($this->request->data['ofc_id']) ? $this->request->data['ofc_id'] : 0;

            if (!empty($office_id)) {
                try {
                    $ofiice_info = TableRegistry::get('Offices')->getBanglaName($office_id);
                    $office_name = $ofiice_info['office_name_bng'];
                    $this->switchOffice($office_id, 'OfficeDB');
                    TableRegistry::remove('DakNagoriks');
                    $DakNogoriksData = TableRegistry::get('DakNagoriks')->find()->toArray();
                    $this->nagorikAbedonExcel($DakNogoriksData, $office_name);
                } catch (\Exception $ex) {
                    echo $ex->getMessage();
                    die;
                }
            }
        }
    }

    private function nagorikAbedonExcel($data, $office_name)
    {

        set_time_limit('0');
        ini_set('memory_limit', '-1');
        try {
            require ROOT . DS . 'vendor' . DS . 'phpoffice' . DS . 'phpexcel' . DS . 'Classes' . DS . 'PHPExcel.php';
            require ROOT . DS . 'vendor' . DS . 'phpoffice' . DS . 'phpexcel' . DS . 'Classes' . DS . 'PHPExcel' . DS . 'IOFactory.php';

            $book = new \PHPExcel();
            $text = $office_name . ' অফিসের নাগরিক ডাক ';
            $book->getActiveSheet()->setTitle('Nagorik Dak');
            $sheet = $book->getActiveSheet();

            $style = array(
                'font' => array('bold' => true),
                'alignment' => array('horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,),
            );

            if (!empty($data)) {
                $columns = [];
                $appdata = $data[0]->toArray();
                if (!empty($appdata)) {
                    foreach ($appdata as $k => $dt) {
                        $columns[] = $k;
                    }
                    $col_length = count($columns);
                    $index = 3;
                    $sheet->setCellValueByColumnAndRow(0, 1, $text); // Sets cell 'a1' to value 'ID
                    $sheet->mergeCellsByColumnAndRow(0, 1, $col_length, 1);
                    foreach ($data as $NgD) {
                        for ($col = 0; $col < $col_length; $col++) {
                            if ($index == 3) {
                                $sheet->setCellValueByColumnAndRow($col, 2, $columns[$col]);
                            }
                            $sheet->setCellValueByColumnAndRow($col, $index,
                                $NgD[$columns[$col]]);
                        }
                        $index++;
                    }
                }
                for ($col = 0; $col < $col_length; $col++) {
                    $sheet->getStyleByColumnAndRow($col, 1)->applyFromArray(array(
                        'font' => array('bold' => true, 'size' => 12),
                        'alignment' => array('horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,),
                    ));
                    $sheet->getStyleByColumnAndRow($col, 2)->applyFromArray($style);
//                $sheet->getColumnDimensionByColumn($col)->setAutoSize(true);
                }
                header("Content-Type: application/vnd.ms-excel");
                header("Content-Disposition: attachment; filename=\".$text.xls\"");
                header("Cache-Control: max-age=0");
                $writer = \PHPExcel_IOFactory::createWriter($book, 'Excel2007');

                // $writer->setIncludeCharts(true);
                ob_end_clean();
                $writer->save('php://output');
                exit;
            } else {
                $this->Flash->Error('No Data Found');
            }
        } catch (\Exception $ex) {
            echo $ex->getMessage();
        }
    }
    public function runOfficeRelatedTasks($office_id = 0){
        if(empty($office_id)){
            return;
        }
        $command = ROOT . "/bin/cake cronjob updatePerformanceTables {$office_id} > /dev/null 2>&1 &";
        exec($command);
        die;
    }
    public function runReportUpdateChecker(){
        $command = ROOT."/bin/cake notification reportUpdateChecker > /dev/null 2>&1 &";
        exec($command);
        die;
    }
}