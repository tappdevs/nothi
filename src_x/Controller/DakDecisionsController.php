<?php

namespace App\Controller;

use App\Controller\ProjapotiController;
use App\Model\Table;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

class DakDecisionsController extends ProjapotiController
{

    public function Add()
    {
        $dakDecisions_table   = TableRegistry::get("DakActions");
        $dakDecisions_records = $dakDecisions_table->newEntity();

        $session = $this->request->session();
        if ($this->request->is('post')) {

            if (!empty($this->request->data)) {
                $dakDecisions_records          = $dakDecisions_records->patchEntity($dakDecisions_records,
                    $this->request->data, ['validate' => 'add']);
                $dakDecisions_records->created = date("Y-m-d");
                $errors                        = $dakDecisions_records->errors();
                if (!$errors) {
                    try {
                        $dakDecisions_table->save($dakDecisions_records);
                        $this->Flash->success('??? ???????? ??????');
                        $this->redirect(['action' => 'nothiTypes']);
                    } catch (InvalidArgumentException $e) {
                        $this->Flash->error('??? ???????? ??? ????? ????? ???');
                    }
                } else {
                    $this->Flash->error('??? ???????? ??? ????? ????? ???');
                }
            }
        }

        $this->set(compact('nothiTypes_records'));
    }

    public function index()
    {
        $dak_decisions_table = TableRegistry::get('DakActions');
        $query               = $dak_decisions_table->find('all');
        $this->set(compact('query'));
    }
}