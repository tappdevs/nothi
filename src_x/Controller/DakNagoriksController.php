<?php

namespace App\Controller;


use App\Controller\ProjapotiController;
use App\Model\Table;
use App\Model\Table\DakDaptoriksTable;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\View\CellTrait;
use Psr\Log\InvalidArgumentException;
use Cake\I18n\Number;
use Cake\I18n\I18n;
use Cake\I18n\Time;
use Cake\Datasource\ConnectionManager;

class DakNagoriksController extends ProjapotiController
{

    use CellTrait;

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['NagorikAbedon', 'OnlineAbedon', 'onlinePortalAbedon', 'getTracking', 'apiOnlineAbedon',
            'apiGetCurrentStatus', 'apiGetMovementDetails', 'portalAbedonStatus', 'apiApplication']);
    }

    public function index()
    {
        $dak_nagoriks = TableRegistry::get('DakNagoriks');
        $query = $dak_nagoriks->find('all');
        $this->set(compact('query'));
    }

    public function checkBirthCertificate()
    {
        $this->response->body(json_encode(1));
        $this->response->type('application/json');
        return $this->response;
    }

    public function dakInbox()
    {
        $this->layout = null;
        $this->selectedOfficeSection();
    }

    private function selectedOfficeSection()
    {

        $this->set('selected_office_section', $this->getCurrentDakSection());
    }

    public function inboxContent($dak_inbox_group)
    {
        $this->layout = null;
        $this->view = 'inbox_content_new';

        $user = $this->Auth->user();

        $EmployeeOfficesTable = TableRegistry::get('EmployeeOffices');
        $employee_office = $EmployeeOfficesTable->find()->where(['employee_record_id' => $user['employee_record_id'],
            'status' => 1])->first();

        $this->set('dak_inbox_group', $dak_inbox_group);
        $table_instance_dak_actions = TableRegistry::get('DakActions');
        $this->set('dak_actions', $table_instance_dak_actions->GetDakActions($employee_office['office_unit_organogram_id']));
        $session = $this->request->session();
        $selected_office_section = $session->read('selected_office_section');
        $this->set(compact('selected_office_section'));
    }

    public function daklistBk($dak_inbox_group)
    {
        $this->layout = null;

        $dak_priority_type_table = TableRegistry::get('DakPriorityTypes');
        $dak_security_level_table = TableRegistry::get('DakSecurityLevels');

        $session = $this->request->session();

        $this->selectedOfficeSection();

        $selected_office_section = $session->read('selected_office_section');
        $this->set(compact('selected_office_section'));

        $start = intval($this->request->data['start']);
        $len = intval($this->request->data['length']);
        $page = ($start / $len) + 1;

        $condition = '1 ';
        $subject = isset($this->request->data['dak_subject']) ? h(trim($this->request->data['dak_subject']))
            : '';
        if (!empty($subject)) {
            if (!empty($condition)) $condition .= ' AND ';
            $condition .= "dak_subject LIKE '%{$subject}%'";
        }

        $officername = isset($this->request->data['receiving_officer_name']) ? h(trim($this->request->data['receiving_officer_name']))
            : '';
        if (!empty($officername)) {
            if (!empty($condition)) $condition .= ' AND ';
            $condition .= "  sender_name LIKE '%{$officername}%'";
        }


        $security_level_filter = isset($this->request->data['dak_security_level'])
            ? h(trim($this->request->data['dak_security_level'])) : '';
        if (!empty($security_level_filter)) {
            if (!empty($condition)) $condition .= ' AND ';
            $condition .= "  dak_security_level = {$security_level_filter}";
        }


        $priority_level_filter = isset($this->request->data['dak_priority_level'])
            ? h(trim($this->request->data['dak_priority_level'])) : '';
        if (!empty($priority_level_filter)) {
            if (!empty($condition)) $condition .= ' AND ';
            $condition .= "  dak_priority_level = {$priority_level_filter}";
        }

        $dak_nagoriks = array();
        $dak_daptorik_table = TableRegistry::get('DakNagoriks');

        $dak_attachments_table = TableRegistry::get('DakAttachments');

        $totalRec = array();

        if ($dak_inbox_group == 'draft') {
            $totalRec = $dak_daptorik_table->find()->where(['dak_status' => DAK_CATEGORY_DRAFT,
                'uploader_designation_id' => $selected_office_section['office_unit_organogram_id']])->where([$condition])->count();
            $dak_nagoriks = $dak_daptorik_table->find()->where(['dak_status' => DAK_CATEGORY_DRAFT,
                'uploader_designation_id' => $selected_office_section['office_unit_organogram_id']])->where([$condition])->order("modified DESC")->page($page,
                $len)->toArray();
        } else if ($dak_inbox_group == 'my-draft') {
            $totalRec = $dak_daptorik_table->find()->where(['dak_status' => DAK_CATEGORY_SENT,
                'uploader_designation_id' => $selected_office_section['office_unit_organogram_id']])->where([$condition])->count();
            $dak_nagoriks = $dak_daptorik_table->find()->where(['dak_status' => DAK_CATEGORY_SENT,
                'uploader_designation_id' => $selected_office_section['office_unit_organogram_id']])->where([$condition])->order("modified DESC")->page($page,
                $len)->toArray();
        }

        $data = array();

        $priorities = json_decode(DAK_PRIORITY_TYPE);
        $priority_list = array();

        foreach ($priorities as $key => $value) {
            $priority_list[$key] = $value;
        }

        $security_level_list = array();
        $security_levels = json_decode(DAK_SECRECY_TYPE);

        foreach ($security_levels as $key => $value) {
            $security_level_list[$key] = $value;;
        }

        if (!empty($dak_nagoriks)) {
            $showDetailsDak = 'showDetailsDak';
            if ($dak_inbox_group == 'draft') {
                foreach ($dak_nagoriks as $dak) {
                    $priority = $priority_list[$dak['dak_priority_level']];
                    $security_level = trim($security_level_list[$dak['dak_security_level']]);
                    $date_in_bd = __("{$dak['modified']}", "bn_BD");
                    $date__time_separator = explode(' ', $date_in_bd);
                    $date_only = !empty($date__time_separator[0]) ? $date__time_separator[0]
                        : '';
                    $time_only = (!empty($date__time_separator[1]) ? $date__time_separator[1]
                            : '') . (!empty($date__time_separator[2]) ? ' ' . $date__time_separator[2]
                            : '');

                    $attachmentCount = $dak_attachments_table->find()->where(['dak_type' => "Nagorik", 'dak_id' => $dak['id']])->count();
                    $data[] = array(
                        '<input class="dak_draft_list_checkbox_to_select" type="checkbox" name="id[]" value="' . $dak['id'] . '">',
                        '<button data-toggle="tooltip" data-dak-id="' . $dak['id'] . '" data-placement="top" type="button" class="btn btn-xs purple dak_draft_send_btn_single" title="ডাক প্রেরণ করুন">
	                        <i class="a2i_nt_cholomandak2" aria-hidden="true"></i>
	                        </button>',
                        '<button data-toggle="tooltip" data-messageid="' . $dak['id'] . '" data-placement="top" type="button" class="' . $showDetailsDak . ' btn btn-xs btn-primary" title="বিস্তারিত">
	                        <i class=" a2i_gn_details2" aria-hidden="true"></i>
	                        </button>',
                        $date_only . "<br>" . $time_only,
                        /* (!empty($dak['sender_name']) ? $dak['sender_name'] . ", " . $dak['sender_officer_designation_label'] : ""), */
                        (!empty($dak['name_bng']) ? h($dak['name_bng']) : ""),
                        (!empty($dak['receiving_officer_name']) ? h($dak['receiving_officer_name']) . ", " . h($dak['receiving_officer_designation_label'])
                            : ""),
                        h($dak ['dak_subject']),
                        !empty($security_level) ? ($security_level . ', ') : '' . $priority,
                        '<i class="fa fa-paperclip" data-toggle="tooltip" data-placement="left" title=""> <span class="badge badge-danger">' . ((!empty($attachmentCount)
                            && $attachmentCount > 1) ? Number::format(($attachmentCount - 1)) : "নাই") . '</span></i>',
                        '<div class="col-lg-2 col-md-2 col-sm-3"><button data-toggle="modal" href="' . $this->request->webroot . 'dakNagoriks/dakReceipt/' . $dak['id'] . '" data-target="#ajax" data-messageid="' . $dak['id'] . '" type="button" class=" btn btn-sm btn-danger" title="প্রাপ্তি স্বীকার রসিদ প্রিন্ট করুন">
	                            <i class="a2i_gn_print2" aria-hidden="true"></i>
	                        </button></div>',
                        '<a href="' . $this->request->webroot . 'dakNagoriks/editDak/' . $dak['id'] . '" data-toggle="tooltip" data-messageid="' . $dak['id'] . '" data-placement="top" type="button" class="edit btn btn-xs btn-success" title="সম্পাদনা">
	                        <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
	                        </a>'
                    );
                }
            } else if ($dak_inbox_group == 'my-draft') {
                foreach ($dak_nagoriks as $dak) {
                    $priority = $priority_list[$dak['dak_priority_level']];
                    $security_level = trim($security_level_list[$dak['dak_security_level']]);
                    $date_in_bd = __("{$dak['modified']}", "bn_BD");
                    $date__time_separator = explode(' ', $date_in_bd);
                    $date_only = !empty($date__time_separator[0]) ? $date__time_separator[0]
                        : '';
                    $time_only = (!empty($date__time_separator[1]) ? $date__time_separator[1]
                            : '') . (!empty($date__time_separator[2]) ? ' ' . $date__time_separator[2]
                            : '');

                    $attachmentCount = $dak_attachments_table->find()->where(['dak_type' => "Nagorik", 'dak_id' => $dak['id']])->count();
                    $data[] = array(
                        '<button data-toggle="tooltip" data-messageid="' . $dak['id'] . '" data-placement="top" type="button" class="' . $showDetailsDak . ' btn btn-xs btn-primary" title="বিস্তারিত">
	                        <i class=" a2i_gn_details2" aria-hidden="true"></i>
	                        </button>',
                        $date_only . "<br>" . $time_only,
                        /* (!empty($dak['sender_name']) ? $dak['sender_name'] . ", " . $dak['sender_officer_designation_label'] : ""), */
                        (!empty($dak['name_bng']) ? h($dak['name_bng']) : ""),
                        (!empty($dak['receiving_officer_name']) ? h($dak['receiving_officer_name']) . ", " . h($dak['receiving_officer_designation_label'])
                            : ""),
                        h($dak ['dak_subject']),
                        !empty($security_level) ? ($security_level . ', ') : '' . $priority,
                        '<i class="fa fa-paperclip" data-toggle="tooltip" data-placement="left" title=""> <span class="badge badge-danger">' . ((!empty($attachmentCount)
                            && $attachmentCount > 1) ? Number::format(($attachmentCount - 1)) : "নাই") . '</span></i>',
                        '<div class="col-lg-2 col-md-2 col-sm-3"><button data-toggle="modal" href="' . $this->request->webroot . 'dakNagoriks/dakReceipt/' . $dak['id'] . '" data-target="#ajax" data-messageid="' . $dak['id'] . '" type="button" class=" btn btn-sm btn-danger" title="প্রাপ্তি স্বীকার রসিদ প্রিন্ট করুন">
	                            <i class="a2i_gn_print2" aria-hidden="true"></i>
	                        </button></div>'
                    );
                }
            }
        }

        $json_data = array(
            "draw" => intval($this->request->data['draw']),
            "recordsTotal" => $totalRec,
            "recordsFiltered" => $totalRec,
            "data" => $data
        );
        echo json_encode($json_data);
        die;
    }

    public function daklist($dak_inbox_group)
    {
        $this->layout = null;

        $dak_priority_type_table = TableRegistry::get('DakPriorityTypes');
        $dak_security_level_table = TableRegistry::get('DakSecurityLevels');

        $session = $this->request->session();

        $this->selectedOfficeSection();

        $selected_office_section = $session->read('selected_office_section');
        $this->set(compact('selected_office_section'));

        $start = intval($this->request->data['start']);
        $len = intval($this->request->data['length']);
        $page = ($start / $len) + 1;

        $condition = [];
        $subject = isset($this->request->data['dak_subject']) ? h(trim($this->request->data['dak_subject']))
            : '';
        if (!empty($subject)) {
            array_push($condition, ['dak_subject LIKE ' => "%" . $subject . "%"]);
        }

        $officername = isset($this->request->data['receiving_officer_name']) ? h(trim($this->request->data['receiving_officer_name']))
            : '';
        if (!empty($officername)) {
            array_push($condition, ['sender_name LIKE ' => "%" . $officername . "%"]);
        }


        $security_level_filter = isset($this->request->data['dak_security_level'])
            ? h(trim($this->request->data['dak_security_level'])) : '';
        if (!empty($security_level_filter)) {
            array_push($condition, ['dak_security_level' => $security_level_filter]);
        }


        $priority_level_filter = isset($this->request->data['dak_priority_level'])
            ? h(trim($this->request->data['dak_priority_level'])) : '';
        if (!empty($priority_level_filter)) {
            array_push($condition, ['dak_priority_level' => $priority_level_filter]);
        }

        $dak_nagoriks = array();
        $dak_daptorik_table = TableRegistry::get('DakNagoriks');

        $dak_attachments_table = TableRegistry::get('DakAttachments');

        $totalRec = array();

        if ($dak_inbox_group == 'draft') {
            $totalRec = $dak_daptorik_table->find()->where(['dak_status' => DAK_CATEGORY_DRAFT,
                'uploader_designation_id' => $selected_office_section['office_unit_organogram_id']])->where($condition)->count();
            $dak_nagoriks = $dak_daptorik_table->find()->where(['dak_status' => DAK_CATEGORY_DRAFT,
                'uploader_designation_id' => $selected_office_section['office_unit_organogram_id']])->where($condition)->order("modified DESC")->page($page,
                $len)->toArray();
        } else if ($dak_inbox_group == 'my-draft') {
            $totalRec = $dak_daptorik_table->find()->where(['dak_status' => DAK_CATEGORY_SENT,
                'uploader_designation_id' => $selected_office_section['office_unit_organogram_id']])->where($condition)->count();
            $dak_nagoriks = $dak_daptorik_table->find()->where(['dak_status' => DAK_CATEGORY_SENT,
                'uploader_designation_id' => $selected_office_section['office_unit_organogram_id']])->where($condition)->order("modified DESC")->page($page,
                $len)->toArray();
        }

        $data = array();

        $priorities = json_decode(DAK_PRIORITY_TYPE);
        $priority_list = array();

        foreach ($priorities as $key => $value) {
            $priority_list[$key] = $value;
        }

        $security_level_list = array();
        $security_levels = json_decode(DAK_SECRECY_TYPE);

        foreach ($security_levels as $key => $value) {
            $security_level_list[$key] = $value;;
        }

        if (!empty($dak_nagoriks)) {
            $showDetailsDak = 'showDetailsDak';
            if ($dak_inbox_group == 'draft') {
                foreach ($dak_nagoriks as $dak) {
                    $priority = $priority_list[$dak['dak_priority_level']];
                    $security_level = trim($security_level_list[$dak['dak_security_level']]);
                    $date_in_bd = __("{$dak['modified']}", "bn_BD");
                    $date__time_separator = explode(' ', $date_in_bd);
                    $date_only = !empty($date__time_separator[0]) ? $date__time_separator[0]
                        : '';
                    $time_only = (!empty($date__time_separator[1]) ? $date__time_separator[1]
                            : '') . (!empty($date__time_separator[2]) ? ' ' . $date__time_separator[2]
                            : '');

                    $attachmentCount = $dak_attachments_table->find()->where(['dak_type' => "Nagorik", 'dak_id' => $dak['id']])->count();
                    $data[] = array(
                        '<input class="dak_draft_list_checkbox_to_select" type="checkbox" name="id[]" value="' . $dak['id'] . '">',
                        '<button data-toggle="tooltip" data-dak-id="' . $dak['id'] . '" data-placement="top" type="button" class="btn btn-xs purple dak_draft_send_btn_single" title="ডাক প্রেরণ করুন">
	                        <i class="a2i_nt_cholomandak2" aria-hidden="true"></i>
	                        </button>',
                        '<button data-toggle="tooltip" data-messageid="' . $dak['id'] . '" data-placement="top" type="button" class="' . $showDetailsDak . ' btn btn-xs btn-primary" title="বিস্তারিত">
	                        <i class=" a2i_gn_details2" aria-hidden="true"></i>
	                        </button>',
                        $date_only . "<br>" . $time_only,
                        /* (!empty($dak['sender_name']) ? $dak['sender_name'] . ", " . $dak['sender_officer_designation_label'] : ""), */
                        (!empty($dak['name_bng']) ? h($dak['name_bng']) : ""),
                        (!empty($dak['receiving_officer_name']) ? h($dak['receiving_officer_name']) . ", " . h($dak['receiving_officer_designation_label'])
                            : ""),
                        h($dak ['dak_subject']),
                        !empty($security_level) ? ($security_level . ', ') : '' . $priority,
                        '<i class="fa fa-paperclip" data-toggle="tooltip" data-placement="left" title=""> <span class="badge badge-danger">' . ((!empty($attachmentCount)
                            && $attachmentCount > 1) ? Number::format(($attachmentCount - 1)) : "নাই") . '</span></i>',
                        '<div class="col-lg-2 col-md-2 col-sm-3"><button data-toggle="modal" href="' . $this->request->webroot . 'dakNagoriks/dakReceipt/' . $dak['id'] . '" data-target="#ajax" data-messageid="' . $dak['id'] . '" type="button" class=" btn btn-sm btn-danger" title="প্রাপ্তি স্বীকার রসিদ প্রিন্ট করুন">
	                            <i class="a2i_gn_print2" aria-hidden="true"></i>
	                        </button></div>',
                        '<a href="' . $this->request->webroot . 'dakNagoriks/editDak/' . $dak['id'] . '" data-toggle="tooltip" data-messageid="' . $dak['id'] . '" data-placement="top" type="button" class="edit btn btn-xs btn-success" title="সম্পাদনা">
	                        <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
	                        </a>'
                    );
                }
            } else if ($dak_inbox_group == 'my-draft') {
                foreach ($dak_nagoriks as $dak) {
                    $priority = $priority_list[$dak['dak_priority_level']];
                    $security_level = trim($security_level_list[$dak['dak_security_level']]);
                    $date_in_bd = __("{$dak['modified']}", "bn_BD");
                    $date__time_separator = explode(' ', $date_in_bd);
                    $date_only = !empty($date__time_separator[0]) ? $date__time_separator[0]
                        : '';
                    $time_only = (!empty($date__time_separator[1]) ? $date__time_separator[1]
                            : '') . (!empty($date__time_separator[2]) ? ' ' . $date__time_separator[2]
                            : '');

                    $attachmentCount = $dak_attachments_table->find()->where(['dak_type' => "Nagorik", 'dak_id' => $dak['id']])->count();
                    $data[] = array(
                        '<button data-toggle="tooltip" data-messageid="' . $dak['id'] . '" data-placement="top" type="button" class="' . $showDetailsDak . ' btn btn-xs btn-primary" title="বিস্তারিত">
	                        <i class=" a2i_gn_details2" aria-hidden="true"></i>
	                        </button>',
                        $date_only . "<br>" . $time_only,
                        /* (!empty($dak['sender_name']) ? $dak['sender_name'] . ", " . $dak['sender_officer_designation_label'] : ""), */
                        (!empty($dak['name_bng']) ? h($dak['name_bng']) : ""),
                        (!empty($dak['receiving_officer_name']) ? h($dak['receiving_officer_name']) . ", " . h($dak['receiving_officer_designation_label'])
                            : ""),
                        h($dak ['dak_subject']),
                        !empty($security_level) ? ($security_level . ', ') : '' . $priority,
                        '<i class="fa fa-paperclip" data-toggle="tooltip" data-placement="left" title=""> <span class="badge badge-danger">' . ((!empty($attachmentCount)
                            && $attachmentCount > 1) ? Number::format(($attachmentCount - 1)) : "নাই") . '</span></i>',
                        '<div class="col-lg-2 col-md-2 col-sm-3"><button data-toggle="modal" href="' . $this->request->webroot . 'dakNagoriks/dakReceipt/' . $dak['id'] . '" data-target="#ajax" data-messageid="' . $dak['id'] . '" type="button" class=" btn btn-sm btn-danger" title="প্রাপ্তি স্বীকার রসিদ প্রিন্ট করুন">
	                            <i class="a2i_gn_print2" aria-hidden="true"></i>
	                        </button></div>'
                    );
                }
            }
        }

        $json_data = array(
            "draw" => intval($this->request->data['draw']),
            "recordsTotal" => $totalRec,
            "recordsFiltered" => $totalRec,
            "data" => $data
        );
        echo json_encode($json_data);
        die;
    }

    public function dakReceipt($dakId)
    {
        $this->layout = null;
        $session = $this->request->session();
        $current_office = $session->read('selected_office_section');

        $table_instance_office = TableRegistry::get('Offices');
        $office = $table_instance_office->get($current_office['office_id']);
        $office_name = h($office['office_name_bng']);
        $receipt_data = array();
        $receipt_data['received_office_name'] = $office_name;

        $table_instance_dd = TableRegistry::get('DakNagoriks');
        $dak = $table_instance_dd->get($dakId);
        $receipt_data['dak_receipt_no'] = h($dak['dak_received_no']);
        $receipt_data['dak_subject'] = h($dak['dak_subject']);
        $receipt_data['received_date'] = $dak['created'];
        $receipt_data['sender_name'] = h($dak['sender_name']);

        $receipt_data['sender_mobile'] = h($dak['mobile_no']);
        $receipt_data['dak_type'] = 'Nagorik';

        $this->set('receipt_data', $receipt_data);
    }

    public function viewDak()
    {

        $session = $this->request->session();
        $this->selectedOfficeSection();

        $employee_office = $session->read('selected_office_section');

        $this->layout = null;
        $id = $this->request->query['message_id'];

        $table_instance_dak_user = TableRegistry::get('DakUsers');

        $dakUsers = $table_instance_dak_user->find()->where(['dak_id' => $id, 'to_officer_designation_id' => $employee_office['office_unit_organogram_id'],
            'dak_type' => DAK_NAGORIK])->toArray();
        //Check has access
        if (!TableRegistry::get('DakMovements')->hasAccessInDak($id, DAK_DAPTORIK, $employee_office['office_unit_organogram_id'])) {
            if (empty($dakUsers)) {
                die('Invalid request');
            }
        }
        if (!empty($dakUsers)) {
            foreach ($dakUsers as $dakUser) {
                $dakUser->dak_view_status = DAK_VIEW_STATUS_VIEW;
                $table_instance_dak_user->save($dakUser);
            }
        }

        $table_instance_dd = TableRegistry::get('DakNagoriks');
        $dak_nagoriks_entity = $table_instance_dd->get($id);
        $this->set('dak_nagoriks', $dak_nagoriks_entity);

        $table_instance_da = TableRegistry::get('DakAttachments');
        $this->set('dak_attachments',
            $table_instance_da->loadAllAttachmentByDakId($id, DAK_NAGORIK));

        $table_instance_dak_actions = TableRegistry::get('DakActions');
        $this->set('dak_actions', $table_instance_dak_actions->GetDakActions($employee_office['office_unit_organogram_id']));

        /* Show Sender Information */
        $table_instance = TableRegistry::get('OfficeUnitOrganograms');
        $table_instance_unit = TableRegistry::get('OfficeUnits');
        $table_instance_employee = TableRegistry::get('EmployeeRecords');
        $table_instance_office = TableRegistry::get('Offices');
        $table_instance_dm = TableRegistry::get('DakMovements');

        $priorities = json_decode(DAK_PRIORITY_TYPE);
        $priority_list = array();

        foreach ($priorities as $key => $value) {
            $dak_priority_type_list[$key] = $value;
        }

        $security_level_list = array();
        $security_levels = json_decode(DAK_SECRECY_TYPE);

        foreach ($security_levels as $key => $value) {
            $dak_security_level_list[$key] = $value;;
        }

        $dak_priority = 1;

        $receiver_office = PRAPOK . ": " . h($dak_nagoriks_entity['receiving_officer_name']) . ',' . h($dak_nagoriks_entity['receiving_officer_designation_label']) . ' (' . h($dak_nagoriks_entity['receiving_office_unit_name']) . ')';


        $dak_last_move = $table_instance_dm->getInboxDakLastMoveBy_dakid_officerId_designationId($id,
            $employee_office['officer_id'],
            $employee_office['office_unit_organogram_id'], DAK_NAGORIK);

        $sender = PREROK . ": " . $dak_nagoriks_entity['name_bng'];

        $otherUsers = $table_instance_dak_user->find()->where(['dak_id' => $id, 'to_officer_designation_id <> ' => $dak_nagoriks_entity['uploader_designation_id']])->where(['dak_type' => DAK_NAGORIK])->toArray();


        if (!empty($dak_last_move)) {
            $dak_priority = $dak_last_move['dak_priority'] == 0 ? 1 : $dak_last_move['dak_priority'];
        }

        $onulipiUser = "<b>" . ANULIPI . ": </b>";

        if (!empty($otherUsers)) {
            foreach ($otherUsers as $otherUser) {
                $onulipiUser .= h($otherUser['to_officer_name']) . ', ' . h($otherUser['to_officer_designation_label']) . ' (' . h($otherUser['to_office_unit_name']) . ')' . "<br/>";
            }
        }

        $this->set('dak_priority', $dak_priority);
        $this->set('dak_priority_txt',
            isset($dak_priority_type_list[$dak_priority]) ? $dak_priority_type_list[$dak_priority]
                : '');
        $this->set('dak_security_txt',
            isset($dak_security_level_list[$dak_nagoriks_entity['dak_security_level']])
                ? $dak_security_level_list[$dak_nagoriks_entity['dak_security_level']]
                : '');
        $this->set('receiver_office', $receiver_office);
        $this->set('sender', $sender);
        $this->set('onulipiUser', (!empty($otherUsers) ? $onulipiUser : ''));

        $session = $this->request->session();
        $selected_office_section = $session->read('selected_office_section');
    }

    public function prepareDak()
    {
        if ($this->add()) {
            $this->redirect(['action' => 'dakInbox']);
        }
    }

    public function uploadDak($id = null)
    {

        $this->set('sentdak', $id);
        $session = $this->request->session();
        $logged_user_info = $session->read('logged_employee_record');
        $this->set('logged_user_info', $logged_user_info);
        if ($dakId = $this->add()) {
            $this->Flash->success(__('ডাক সংরক্ষণ করা হয়েছে।'));
            $this->redirect(['action' => 'uploadDak', $dakId]);
        }
    }

    public function uploadDakSend()
    {
        $dakId = $this->add();
        if ($dakId) {
            $this->sendDak($dakId);
            $this->Flash->success(__('ডাক সংরক্ষণ এবং পাঠানো হয়েছে'));
        } else {

        }
        $this->redirect(['action' => 'uploadDak', $dakId]);
    }

    public function editDakSend($id = null)
    {
        if (empty($id)) {
            $this->Flash->error(__("দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না"));
            $this->redirect(['action' => 'dakInbox', 'draft']);
        }

        if ($this->edit($id)) {
            if ($this->sendDak($id)) {
                $this->Flash->success(__('ডাক সংরক্ষণ এবং প্রেরণ করা  হয়েছে'));
                $this->redirect(['action' => 'dakInbox', 'draft', $id]);
            } else {
                $this->Flash->error(__("দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না"));
                $this->redirect(['action' => 'dakInbox', 'draft']);
            }
        }
    }

    private function add()
    {
        $this->selectedOfficeSection();

        $session = $this->request->session();
        $selected_office_section = $session->read('selected_office_section');

        $table_instance_office = TableRegistry::get('Offices');
        $table_instance_unit = TableRegistry::get('OfficeUnits');
        $table_instance_dd = TableRegistry::get('DakNagoriks');
        $dak_nagoriks = $table_instance_dd->newEntity();

        $hasErrors = false;

        if ($this->request->is('post')) {
            $table_instance_eo = TableRegistry::get('EmployeeOffices');


            if (!empty($this->request->data['to_officer_level'])) {
                $mainOfficer = intval($this->request->data['to_officer_level']);
                $toOfficer = explode(",",
                    $this->request->data['to_officer_id']);
                $this->request->data['to_officer_name'] = substr($this->request->data['to_officer_name'],
                    0, -1);
                $toOfficerName = explode(";",
                    $this->request->data['to_officer_name']);
                $toOfficeInfo = array_combine($toOfficer,
                    $toOfficerName);

                $employee_office = $table_instance_eo->get($mainOfficer);
                $this->request->data['receiving_officer_id'] = $employee_office['employee_record_id'];
                $this->request->data['receiving_officer_name'] = $toOfficeInfo[$mainOfficer];
                $this->request->data['receiving_officer_designation_id'] = $employee_office['office_unit_organogram_id'];
                $this->request->data['receiving_officer_designation_label'] = $employee_office['designation'];
                $this->request->data['receiving_office_id'] = $employee_office['office_id'];
                $this->request->data['dak_received_no'] = $this->generateDakReceivedNo($employee_office['office_id']);

                $this->request->data['dak_status'] = DAK_CATEGORY_DRAFT;
                $this->request->data['sender_name'] = $this->request->data['name_bng'];

                if (!empty($this->request->data['receiving_officer_designation_id'])) {
                    $receiving_office_unit = $table_instance_unit->getOfficeUnitBy_designationId($this->request->data['receiving_officer_designation_id']);
                    $this->request->data['receiving_office_unit_id'] = $receiving_office_unit['id'];
                    $this->request->data['receiving_office_unit_name'] = $receiving_office_unit['unit_name_bng'];
                }


                $user = $this->Auth->user();
                $this->request->data['created_by'] = $user['id'];
                $this->request->data['modified_by'] = $user['id'];

                $this->request->data['uploader_designation_id'] = $selected_office_section['office_unit_organogram_id'];

                /* Prepare Docketing NO: */
                $this->request->data['docketing_no'] = 0;

                $dak_nagoriks = $table_instance_dd->patchEntity($dak_nagoriks,
                    $this->request->data, ['validate' => 'add']);

                $dak_nagoriks->sending_date = date("Y-m-d H:i:s");
                $error = $dak_nagoriks->errors();

                if (empty($error)) {
                    try {
                        $dak_nagoriks->receive_date = date("Y-m-d H:i:s");
                        $dak = $table_instance_dd->save($dak_nagoriks);
                        $table_instance_da = TableRegistry::get('DakAttachments');

                        $this->request->data['attachment'] = explode(',', $this->request->data['uploaded_attachments']);
                        $attachment_names = explode(',', $this->request->data['uploaded_attachments_names']);
                        $attachment_is_main_data = explode(',', $this->request->data['uploaded_attachments_is_main']);
                        $attachmentcount = 0;
                        foreach ($this->request->data['attachment'] as $k => $file) {
                            if (!empty($file)) {
                                $attachment_data = array();
                                $attachment_data['dak_type'] = $this->request->data['dak_type'];
                                $attachment_data['dak_id'] = $dak->id;
                                $attachment_data['attachment_description'] = $this->request->data['file_description'];
                                $attachment_data['user_file_name'] = (!empty($attachment_names[$k]) ? $attachment_names[$k] : '');
                                $attachment_data['is_main'] = (!empty($attachment_is_main_data[$k]) ? $attachment_is_main_data[$k] : 0);

                                $attachment_data['attachment_type'] = $this->getMimeType($file);

                                $file_path_arr = explode(FILE_FOLDER, $file);

                                $attachment_data['file_dir'] = $this->request->webroot . 'content/';
                                $attachment_data['file_name'] = ($file_path_arr[1]);
                                $explode = explode('?token=', $attachment_data['file_name']);
                                $attachment_data['file_name'] = $explode[0];
                                $dak_attachment = $table_instance_da->newEntity();
                                $dak_attachment = $table_instance_da->patchEntity($dak_attachment,
                                    $attachment_data);
                                $dak_attachment->created_by = $this->Auth->user('id');
                                $dak_attachment->modified_by = $this->Auth->user('id');
                                if ($table_instance_da->save($dak_attachment)) {
                                    $attachmentcount++;
                                }
                            }
                        }

                        if ($attachmentcount == 0) {
                            $hasErrors = true;
                            throw  new \Exception(__('দুঃখিত! ডাক সংরক্ষণ সম্ভব হচ্ছে না. কোনো সংযুক্তি দেওয়া হয়নি'));
                        }

                        $table_instance_du = TableRegistry::get('DakUsers');
                        $table_instance_dua = TableRegistry::get('DakUserActions');

                        $drafter = $table_instance_du->newEntity();
                        $drafter->dak_type = DAK_NAGORIK;
                        $drafter->dak_id = $dak->id;
                        $drafter->to_office_id = $selected_office_section['office_id'];
                        $drafter->to_office_name = $selected_office_section['office_name'];
                        $drafter->to_office_unit_id = $selected_office_section['office_unit_id'];
                        $drafter->to_office_unit_name = $selected_office_section['office_unit_name'];
                        $drafter->to_office_address = $selected_office_section['office_address'];
                        $drafter->to_officer_id = $selected_office_section['officer_id'];
                        $drafter->to_officer_name = $selected_office_section['officer_name'];
                        $drafter->to_officer_designation_id = $selected_office_section['office_unit_organogram_id'];
                        $drafter->to_officer_designation_label = $selected_office_section['designation_label'];
                        $drafter->dak_view_status = DAK_VIEW_STATUS_VIEW;
                        $drafter->dak_priority = 0;
                        $drafter->attention_type = 0;
                        $drafter->dak_category = DAK_CATEGORY_DRAFT;
                        $drafter->created_by = $this->Auth->user('id');
                        $drafter->modified_by = $this->Auth->user('id');

                        $drafter = $table_instance_du->save($drafter);

                        $user_action = $table_instance_dua->newEntity();
                        $user_action->dak_id = $dak->id;
                        $user_action->dak_type = DAK_NAGORIK;
                        $user_action->dak_user_id = $drafter->id;
                        $user_action->dak_action = DAK_CATEGORY_DRAFT;
                        $user_action->created_by = $this->Auth->user('id');
                        $user_action->modified_by = $this->Auth->user('id');
                        $table_instance_dua->save($user_action);

                        if (!empty($toOfficeInfo)) {
                            foreach ($toOfficeInfo as $key => $value) {
                                $other_receiver = $table_instance_eo->get($key);
                                $table_instance_office_record = $table_instance_office->get($other_receiver['office_id']);
                                $table_instance_unit_record = $table_instance_unit->get($other_receiver['office_unit_id']);

                                $drafter = $table_instance_du->newEntity();
                                $drafter->dak_type = DAK_NAGORIK;
                                $drafter->dak_id = $dak->id;
                                $drafter->to_office_id = $other_receiver['office_id'];
                                $drafter->to_office_name = $table_instance_office_record['office_name_bng'];
                                $drafter->to_office_unit_id = $other_receiver['office_unit_id'];
                                $drafter->to_office_unit_name = $table_instance_unit_record['unit_name_bng'];
                                $drafter->to_office_address = $other_receiver['office_address'];
                                $drafter->to_officer_id = $other_receiver['employee_record_id'];
                                $drafter->to_officer_name = $value;
                                $drafter->to_officer_designation_id = $other_receiver['office_unit_organogram_id'];
                                $drafter->to_officer_designation_label = $other_receiver['designation'];
                                $drafter->dak_view_status = DAK_VIEW_STATUS_VIEW;
                                $drafter->dak_priority = 0;
                                $drafter->attention_type = $key == $mainOfficer ? 1 : 0;
                                $drafter->dak_category = DAK_CATEGORY_DRAFT;
                                $drafter->created_by = $this->Auth->user('id');
                                $drafter->modified_by = $this->Auth->user('id');
                                $drafter = $table_instance_du->save($drafter);
                            }
                        }

                        if (!$this->NotificationSet('upload',
                            array(1, "Nagorik Dak"), 1,
                            $selected_office_section, $selected_office_section, $dak_nagoriks['dak_subject'])) {

                        }

                        return $dak->id;
                    } catch (Exception $e) {
                        $this->Flash->error(__('দুঃখিত! ডাক সংরক্ষণ সম্ভব হচ্ছে না'));
                        $hasErrors = true;
                    } catch (\Exception $ex) {
                        $this->Flash->error(__('দুঃখিত! ডাক সংরক্ষণ সম্ভব হচ্ছে না') . $ex->getMessage());
                        $hasErrors = true;
                    }
                } else {
                    $errorMsg = '<ul class="list-group">';

                    foreach ($error as $key => $value) {
                        $val = array_values($value);
                        $errorMsg .= '<li class="list-group-item list-group-item-danger">' . $val[0] . '</li>';
                    }
                    $errorMsg = '</ul>';
                    $this->Flash->error($errorMsg);
                }
            } else {
                $this->Flash->error(__('দুঃখিত! মূল প্রাপক বাছাই করা হয়নি'));
                $hasErrors = true;
            }
        }

        $this->set('dak_nagoriks', $dak_nagoriks);
        if ($hasErrors) return false;
    }

    private function edit($id = 0)
    {
        $selected_office_section = $this->getCurrentDakSection();
        $this->set('selected_office_section', $selected_office_section);
        $table_instance_office = TableRegistry::get('Offices');
        $table_instance_unit = TableRegistry::get('OfficeUnits');
        $table_instance_du = TableRegistry::get('DakUsers');
        $table_instance_dd = TableRegistry::get('DakNagoriks');
        $table_instance_da = TableRegistry::get('DakAttachments');

        if (!empty($id)) {
            $dak_nagoriks = $this->DakNagoriks->get($id);
        }

        if (empty($dak_nagoriks) || $dak_nagoriks['dak_status'] != DAK_CATEGORY_DRAFT) {
            $this->Flash->error("দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না");
            $this->redirect(['controller' => 'dakNagoriks', 'action' => 'dakInbox',
                'draft']);
            return;
        }

        $attachments = $table_instance_da->loadAllAttachmentByDakId($id, 'Nagorik');

        $this->set('dak_attachments', $attachments);
        $this->set('dak_id', $id);
        $dak_users = $table_instance_du->getAllUsers($id, 'Nagorik');
        $this->set('dak_users', $dak_users);
        $hasErrors = false;

        if ($this->request->is(['post', 'put'])) {
            $table_instance_eo = TableRegistry::get('EmployeeOffices');

            if (!empty($this->request->data['to_officer_level'])) {
                $mainOfficer = intval($this->request->data['to_officer_level']);
                $toOfficer = explode(",",
                    $this->request->data['to_officer_id']);
                $this->request->data['to_officer_name'] = substr($this->request->data['to_officer_name'],
                    0, -1);
                $toOfficerName = explode(";",
                    $this->request->data['to_officer_name']);
                $toOfficeInfo = array_combine($toOfficer,
                    $toOfficerName);

                $employee_office = $table_instance_eo->get($mainOfficer);
                $this->request->data['receiving_officer_id'] = $employee_office['employee_record_id'];
                $this->request->data['receiving_officer_name'] = $toOfficeInfo[$mainOfficer];
                $this->request->data['receiving_officer_designation_id'] = $employee_office['office_unit_organogram_id'];
                $this->request->data['receiving_officer_designation_label'] = $employee_office['designation'];
                $this->request->data['receiving_office_id'] = $employee_office['office_id'];
                $this->request->data['dak_received_no'] = $this->generateDakReceivedNo($employee_office['office_id']);

                $this->request->data['dak_status'] = DAK_CATEGORY_DRAFT;
                $this->request->data['sender_name'] = $this->request->data['name_bng'];

                if (!empty($this->request->data['receiving_officer_designation_id'])) {
                    $receiving_office_unit = $table_instance_unit->getOfficeUnitBy_designationId($this->request->data['receiving_officer_designation_id']);
                    $this->request->data['receiving_office_unit_id'] = $receiving_office_unit['id'];
                    $this->request->data['receiving_office_unit_name'] = $receiving_office_unit['unit_name_bng'];
                }


                $user = $this->Auth->user();
                $this->request->data['created_by'] = $user['id'];
                $this->request->data['modified_by'] = $user['id'];

                $this->request->data['uploader_designation_id'] = $selected_office_section['office_unit_organogram_id'];

                /* Prepare Docketing NO: */
                $this->request->data['docketing_no'] = 0;

                $dak_nagoriks = $table_instance_dd->patchEntity($dak_nagoriks,
                    $this->request->data, ['validate' => 'add']);

                $dak_nagoriks->sending_date = date("Y-m-d H:i:s");
                $error = $dak_nagoriks->errors();

                if (empty($error)) {
                    try {
                        $dak_nagoriks->receive_date = date("Y-m-d H:i:s");
                        $dak = $table_instance_dd->save($dak_nagoriks);

                        $this->request->data['attachment'] = explode(',', $this->request->data['uploaded_attachments']);
                        $table_instance_da->deleteAll(['dak_id' => $id, 'dak_type' => DAK_NAGORIK]);
                        $attachment_names = explode(',', $this->request->data['uploaded_attachments_names']);
                        $attachment_is_main_data = explode(',', $this->request->data['uploaded_attachments_is_main']);
                        $attachmentcount = 0;

                        foreach ($this->request->data['attachment'] as $k => $file) {
                            if (!empty($file)) {
                                $attachment_data = array();
                                $attachment_data['dak_type'] = $this->request->data['dak_type'];
                                $attachment_data['dak_id'] = $dak->id;
                                $attachment_data['attachment_description'] = $this->request->data['file_description'];
                                $attachment_data['user_file_name'] = (!empty($attachment_names[$k]) ? $attachment_names[$k] : '');
                                $attachment_data['is_main'] = (!empty($attachment_is_main_data[$k]) ? $attachment_is_main_data[$k] : 0);
                                $attachment_data['attachment_type'] = $this->getMimeType($file);

                                $file_path_arr = explode(FILE_FOLDER, $file, 2);
                                $attachment_data['file_dir'] = FILE_FOLDER;
                                $attachment_data['file_name'] = ($file_path_arr[1]);
                                $explode = explode('?token=', $attachment_data['file_name']);
                                $attachment_data['file_name'] = $explode[0];
                                $dak_attachment = $table_instance_da->newEntity();
                                $dak_attachment = $table_instance_da->patchEntity($dak_attachment,
                                    $attachment_data);
                                $dak_attachment->created_by = $this->Auth->user('id');
                                $dak_attachment->modified_by = $this->Auth->user('id');
                                if ($table_instance_da->save($dak_attachment)) {
                                    $attachmentcount++;
                                }
                            }
                        }

                        if ($attachmentcount == 0) {
                            $hasErrors = true;
                            $this->Flash->error(__('দুঃখিত! ডাক সংরক্ষণ সম্ভব হচ্ছে না. কোনো সংযুক্তি দেওয়া হয়নি'));

                            return false;
                        }

                        $table_instance_dua = TableRegistry::get('DakUserActions');
                        $table_instance_dua->deleteAll(['dak_id' => $dak->id, 'dak_type' => DAK_NAGORIK]);
                        $table_instance_du->deleteAll(['dak_id' => $dak->id, 'dak_type' => DAK_NAGORIK]);

                        $drafter = $table_instance_du->newEntity();
                        $drafter->dak_type = DAK_NAGORIK;
                        $drafter->dak_id = $dak->id;
                        $drafter->to_office_id = $selected_office_section['office_id'];
                        $drafter->to_office_name = $selected_office_section['office_name'];
                        $drafter->to_office_unit_id = $selected_office_section['office_unit_id'];
                        $drafter->to_office_unit_name = $selected_office_section['office_unit_name'];
                        $drafter->to_office_address = $selected_office_section['office_address'];
                        $drafter->to_officer_id = $selected_office_section['officer_id'];
                        $drafter->to_officer_name = $selected_office_section['officer_name'];
                        $drafter->to_officer_designation_id = $selected_office_section['office_unit_organogram_id'];
                        $drafter->to_officer_designation_label = $selected_office_section['designation_label'];
                        $drafter->dak_view_status = DAK_VIEW_STATUS_VIEW;
                        $drafter->dak_priority = 0;
                        $drafter->attention_type = 0;
                        $drafter->dak_category = DAK_CATEGORY_DRAFT;
                        $drafter->created_by = $this->Auth->user('id');
                        $drafter->modified_by = $this->Auth->user('id');
                        $drafter = $table_instance_du->save($drafter);

                        $user_action = $table_instance_dua->newEntity();
                        $user_action->dak_id = $dak->id;
                        $user_action->dak_type = DAK_NAGORIK;
                        $user_action->dak_user_id = $drafter->id;
                        $user_action->dak_action = DAK_CATEGORY_DRAFT;
                        $user_action->created_by = $this->Auth->user('id');
                        $user_action->modified_by = $this->Auth->user('id');
                        $table_instance_dua->save($user_action);

                        if (!empty($toOfficeInfo)) {
                            foreach ($toOfficeInfo as $key => $value) {
                                $other_receiver = $table_instance_eo->get($key);
                                $table_instance_office_record = $table_instance_office->get($other_receiver['office_id']);
                                $table_instance_unit_record = $table_instance_unit->get($other_receiver['office_unit_id']);

                                $drafter = $table_instance_du->newEntity();
                                $drafter->dak_type = DAK_NAGORIK;
                                $drafter->dak_id = $dak->id;
                                $drafter->to_office_id = $other_receiver['office_id'];
                                $drafter->to_office_name = $table_instance_office_record['office_name_bng'];
                                $drafter->to_office_unit_id = $other_receiver['office_unit_id'];
                                $drafter->to_office_unit_name = $table_instance_unit_record['unit_name_bng'];
                                $drafter->to_office_address = $other_receiver['office_address'];
                                $drafter->to_officer_id = $other_receiver['employee_record_id'];
                                $drafter->to_officer_name = $value;
                                $drafter->to_officer_designation_id = $other_receiver['office_unit_organogram_id'];
                                $drafter->to_officer_designation_label = $other_receiver['designation'];
                                $drafter->dak_view_status = DAK_VIEW_STATUS_VIEW;
                                $drafter->dak_priority = 0;
                                $drafter->attention_type = $key == $mainOfficer ? 1 : 0;
                                $drafter->dak_category = DAK_CATEGORY_DRAFT;
                                $drafter->created_by = $this->Auth->user('id');
                                $drafter->modified_by = $this->Auth->user('id');
                                $drafter = $table_instance_du->save($drafter);
                            }
                        }

                        if (!$this->NotificationSet('upload',
                            array(1, "Nagorik Dak"), 1,
                            $selected_office_section, $selected_office_section, $dak_nagoriks['dak_subject'])) {

                        }

                        return $dak->id;
                    } catch (Exception $e) {
                        $this->Flash->error(__('দুঃখিত! ডাক সংরক্ষণ সম্ভব হচ্ছে না'));
                        $hasErrors = true;
                    } catch (\Exception $e) {
                        $this->Flash->error(__('দুঃখিত! ডাক সংরক্ষণ সম্ভব হচ্ছে না'));
                        $hasErrors = true;
                    }
                } else {
                    $errorMsg = '<ul class="list-group">';

                    foreach ($error as $key => $value) {
                        $val = array_values($value);
                        $errorMsg .= '<li class="list-group-item list-group-item-danger">' . $val[0] . '</li>';
                    }
                    $errorMsg = '</ul>';
                    $this->Flash->error($errorMsg);
                }
            } else {
                $this->Flash->error(__('দুঃখিত! মূল প্রাপক বাছাই করা হয়নি'));
                $hasErrors = true;
            }
        }

        $token = $this->generateToken([
            'office_id' => $selected_office_section['office_id'],
            'office_unit_organogram_id' => $selected_office_section['office_unit_organogram_id'],
            'parent_id' => $id,
            'dak_type' => DAK_NAGORIK,
            'module' => 'Dak'
        ], ['exp' => time() + 3600 * 3]);
        $this->set('temp_token', $token);
        $this->set('dak_nagoriks', $dak_nagoriks);
        if ($hasErrors) return false;

    }

    public function generateDakReceivedNo($office_id, $offline = 0)
    {

        TableRegistry::remove('DakNagoriks');
        $table_instance_dd = TableRegistry::get('DakNagoriks');
        $daktype = 1;

        $dak_count = $table_instance_dd->find()->where(['receiving_office_id' => $office_id, 'date(created)' => date('Y-m-d')])->count();

        startagain:
        $dak_count++;
        $year = date('y');
        $month = date('m');
        $day = date('d');
        $offline = 1;

        $received_no = "";

        $received_no = $daktype . $offline . str_pad($office_id, 5, "0", STR_PAD_LEFT) . $year . $month . $day . str_pad($dak_count, 3, "0", STR_PAD_LEFT);

        $exists = $table_instance_dd->find()->where(['dak_received_no' => $received_no])->count();
        if ($exists) {
            goto startagain;
        }
        return $received_no;
    }

//    public function editDak($id = '')
//    {
//        if (empty($id)) {
//            $this->Flash->error(__("দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না"));
//            $this->redirect(['action' => 'dakInbox', 'draft']);
//        }
////        $id           = $this->request->query['message_id'];
//        $dak_nagoriks = $this->DakNagoriks->get($id);
//       // pr($dak_nagoriks);
//        if ($this->request->is(['post'])) {
//            $this->DakNagoriks->patchEntity($dak_nagoriks, $this->request->data);
//
//            if ($dak = $this->DakNagoriks->save($dak_nagoriks)) {
//                $this->loadModel('DakAttachments');
//                /*
//                 * Delete selected delete files from edit form
//                 */
//                foreach (explode(',', $this->request->data['deleted_files']) as $attachment_id) {
//                    $dak_attachment = $this->DakAttachments->get($attachment_id);
//                    $file_path      = FILE_FOLDER_DIR.$dak_attachment['file_name'];
//                    $this->DakAttachments->delete($dak_attachment);
//                    unlink($file_path);
//                }
//
//                /*
//                 * Add new attachment
//                 */
//                if (count($this->request->data['attachment']) > 0) {
//                    foreach ($this->request->data['attachment'] as $file) {
//                        if (!empty($file['file_name_file'])) {
//                            $attachment_data                           = array();
//                            $attachment_data['dak_type']               = $dak_nagoriks->dak_type;
//                            $attachment_data['dak_id']                 = $dak_nagoriks->id;
//                            $attachment_data['attachment_description'] = $file['file_description'];
//                            $attachment_data['file_dir']               = '';
//                            $attachment_data['attachment_type']        = $file['file_name_file']['type'];
//                            $attachment_data['file_name_file']         = $file['file_name_file'];
//
//// required for file uploader: file name of upload file
//                            $attachment_data['filename_prefix'] = $attachment_data['dak_type']."/".$attachment_data['dak_id']."/".time()."_".$attachment_data['file_name_file']['name'];
//
//                            $dak_attachment = $this->DakAttachments->newEntity();
//                            $dak_attachment = $this->DakAttachments->patchEntity($dak_attachment,
//                                $attachment_data);
//                            $this->DakAttachments->save($dak_attachment);
//                        }
//                    }
//                }
//                return $this->redirect(['action' => 'index']);
//            }
//        }
//        $this->loadModel('DakAttachments');
//        $dak_attachments = $this->DakAttachments->loadAllAttachmentByDakId($id);
//
//        $this->set('dak_nagoriks', $dak_nagoriks);
//        $this->set('dak_attachments',$dak_attachments);
//    }

    public function editDak($id = '')
    {
        if (empty($id)) {
            $this->Flash->error(__("দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না"));
            $this->redirect(['action' => 'dakInbox', 'draft']);
        }
        if ($this->edit($id)) {
            $this->Flash->success(__('ডাক সংশোধন করা হয়েছে।'));
            $this->redirect(['action' => 'dakInbox', 'draft']);
        }
    }

    public function previewDak($id = null)
    {
        $dak_nagoriks = $this->DakNagoriks->get($id);
        $this->set(compact('dak_nagoriks'));
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);

        $dak_nagoriks = $this->DakNagoriks->get($id);
        if ($this->DakNagoriks->delete($dak_nagoriks)) {
            return $this->redirect(['action' => 'index']);
        }
    }

    public function generateSarokNo()
    {
        $office_id = $this->request->data['office_id'];
        $dak_nagoriks = TableRegistry::get('DakNagoriks');
        $dak_nagoriks_count = $dak_nagoriks->getDakCount($office_id);

        $this->response->body(json_encode(($dak_nagoriks_count + 1)));
        $this->response->type('application/json');
        return $this->response;
    }

    public function dakReply()
    {
        $this->layout = null;
        $id = $this->request->query['message_id'];
        $dak_nagoriks_entity = $this->DakNagoriks->get($id);
        $this->set('dak_nagoriks', $dak_nagoriks_entity);
        $this->loadModel('DakAttachments');
        $this->set('dak_attachments',
            $this->DakAttachments->loadAllAttachmentByDakId($id));

        /* Show Sender Information */
        $table_instance = TableRegistry::get('OfficeUnitOrganograms');
        $table_instance_unit = TableRegistry::get('OfficeUnits');
        $table_instance_office = TableRegistry::get('Offices');

        /* Sender Information */
        $organograms = $table_instance->get($dak_nagoriks_entity['sender_officer_designation_id']);
        $unit = $table_instance_unit->get($organograms['office_unit_id']);
        $office = $table_instance_office->get($organograms['office_id']);
        $sender = $dak_nagoriks_entity['sender_officer_designation_label'] . ',' . $unit['unit_name_bng'] . ',' . $office['office_name_bng'];

        /* Receiver Information */
        $receiver_organograms = $table_instance->get($dak_nagoriks_entity['receiving_officer_designation_id']);
        $receiver_unit = $table_instance_unit->get($receiver_organograms['office_unit_id']);
        $receiver_office = $table_instance_office->get($receiver_organograms['office_id']);
        $receiver = $receiver_organograms['designation_bng'] . ',' . $receiver_unit['unit_name_bng'] . ',' . $receiver_office['office_name_bng'];

        /* Set up office information */
        $this->set('sender_office', $sender);
        $this->set('receiver_office', $receiver);
    }

    public function sendDak($dakId = "")
    {
        /* Add entry for draft-nd user */
        $session = $this->request->session();

        $current_office = $this->getCurrentDakSection();
        $logged_user_info = $session->read('logged_employee_record');

        $this->layout = null;
        $dak_ids = empty($dakId) ? $this->request->data['message_id'] : $dakId;
        $dak_arr = explode(',', $dak_ids);

        if (count($dak_arr) == 0) {
            if (empty($dakId)) {
                $this->response->body(json_encode(0));
                $this->response->type('application/json');
                return $this->response;
            } else {
                return false;
            }
        }
        $subject = '';

        foreach ($dak_arr as $dak_id) {
            /* Update Dak Status From Draft->Sent */
            $data = [];
            $DakNagoriksTable = TableRegistry::get('DakNagoriks');
            $DakDaptoriksTable = TableRegistry::get('DakDaptoriks');
            $DakNagoriks = $DakNagoriksTable->get($dak_id); // article with id 12

            $dak_countdp = $DakDaptoriksTable->find()->where(['receiving_office_id' => $current_office['office_id']])->order(['cast(docketing_no as signed) desc'])->first();
            $dak_countng = $DakNagoriksTable->find()->where(['receiving_office_id' => $current_office['office_id']])->order(['cast(docketing_no as signed) desc'])->first();
            $dak_count = max([(!empty($dak_countdp['docketing_no']) ? $dak_countdp['docketing_no'] : 0), (!empty($dak_countng['docketing_no']) ? $dak_countng['docketing_no'] : 0)]) + 1;

            $DakNagoriks->docketing_no = $dak_count;
            $DakNagoriks->dak_status = DAK_CATEGORY_SENT;
            $DakNagoriks->receive_date = date("Y-m-d H:i:s");
            $saved_nagorik_dak = $DakNagoriksTable->save($DakNagoriks);

            $DakUsersTable = TableRegistry::get('DakUsers');

            $DakUsers_draft = $this->getExistingDakUser($dak_id,
                $current_office['office_unit_id'],
                $current_office['office_unit_organogram_id'], DAK_NAGORIK);
            if (empty($DakUsers_draft)) {
                $this->response->body(json_encode(0));
                $this->response->type('application/json');
                return $this->response;
            } else if ($DakUsers_draft['dak_category'] != DAK_CATEGORY_INBOX && $DakUsers_draft['dak_category'] != DAK_CATEGORY_DRAFT) {
                $this->response->body(json_encode(0));
                $this->response->type('application/json');
                return $this->response;
            }
            $DakUsersTable->updateAll(['dak_category' => DAK_CATEGORY_SENT], ['id' => $DakUsers_draft['id']]);

            /* Update Dak Movement Table */
            $DakMovementsTable = TableRegistry::get('DakMovements');

            $getAllDakUsersforThisDak = $this->getNotExistingDakUser($dak_id,
                $current_office['office_unit_organogram_id'], DAK_NAGORIK);

            if (!empty($getAllDakUsersforThisDak)) {
                foreach ($getAllDakUsersforThisDak as $key => $dakUser) {

                    /* Prepare Dak Users */
                    if ($DakNagoriks['receiving_officer_designation_id'] == $dakUser->to_officer_designation_id) {
                        $dakUser->attention_type = 1;
                    } else {
                        $dakUser->attention_type = 0;
                    }
                    $dakUser->dak_view_status = DAK_VIEW_STATUS_NEW;
                    $dakUser->dak_category = DAK_CATEGORY_INBOX;
                    $DakUsersTable->save($dakUser);

                    /*                     * Dak Tracks in Projapoti* */
                    $data['docketing_no'] = $dak_count;
                    $data['receive_date'] = $saved_nagorik_dak['receive_date'];
                    $data['dak_subject'] = $saved_nagorik_dak['dak_subject'];
                    $data['email'] = $saved_nagorik_dak['email'];
                    $data['mobile_no'] = $saved_nagorik_dak['mobile_no'];
                    $data['sender_officer_designation_id'] = $saved_nagorik_dak['uploader_designation_id'];
                    $data['sender_name'] = $saved_nagorik_dak['sender_name'];
                    $data['sender_office_id'] = $current_office["office_id"];
                    $data['dak_received_no'] = $saved_nagorik_dak['dak_received_no'];
                    $data['receiving_office_id'] = $dakUser['to_office_id'];
                    $data['receiving_officer_designation_label'] = $dakUser['to_officer_designation_label'];
                    $data['receiving_officer_name'] = $dakUser['to_officer_name'];
                    $data['dak_type'] = DAK_NAGORIK;
                    $data['dak_id'] = $dak_id;
                    $this->addDakTracks($data);
                    /*                     * Dak Tracks in Projapoti* */

                    /* Sender to Draft User Movement */
                    $second_move = $DakMovementsTable->newEntity();
                    $second_move->dak_type = DAK_NAGORIK;
                    $second_move->dak_id = $dak_id;
                    $second_move->from_office_id = $current_office["office_id"];
                    $second_move->from_office_name = $current_office["office_name"];
                    $second_move->from_office_unit_id = $current_office["office_unit_id"];
                    $second_move->from_office_unit_name = $current_office["office_unit_name"];
                    $second_move->from_office_address = $logged_user_info["personal_info"]["current_office_address"];
                    $second_move->from_officer_id = $current_office["officer_id"];
                    $second_move->from_officer_name = $logged_user_info["personal_info"]["name_bng"];
                    $second_move->from_officer_designation_id = $logged_user_info["personal_info"]["current_office_designation_id"];
                    $second_move->from_officer_designation_label = $current_office["designation_label"];

                    $second_move->to_office_id = $dakUser['to_office_id'];
                    $second_move->to_office_name = $dakUser['to_office_name'];
                    $second_move->to_office_unit_id = $dakUser['to_office_unit_id'];
                    $second_move->to_office_unit_name = $dakUser['to_office_unit_name'];
                    $second_move->to_office_address = $dakUser['to_office_address'];
                    $second_move->to_officer_id = $dakUser['to_officer_id'];
                    $second_move->to_officer_name = $dakUser['to_officer_name'];
                    $second_move->to_officer_designation_id = $dakUser['to_officer_designation_id'];
                    $second_move->to_officer_designation_label = $dakUser['to_officer_designation_label'];
                    $second_move->attention_type = $dakUser['attention_type'];
                    $second_move->docketing_no = $DakNagoriks['docketing_no'];
                    $second_move->from_sarok_no = "";
                    $second_move->to_sarok_no = "";
                    $second_move->operation_type = DAK_CATEGORY_FORWARD;
                    $second_move->sequence = 2;
                    $second_move->dak_actions = FIRST_SUBJECT;
                    $second_move->dak_priority = $DakNagoriks['dak_priority_level'];
                    $second_move->created_by = $this->Auth->user('id');
                    $second_move->modified_by = $this->Auth->user('id');
                    $second_move = $DakMovementsTable->save($second_move);

                    $toOfficers['office_id'] = $dakUser['to_office_id'];
                    $toOfficers['officer_id'] = $dakUser['to_officer_id'];

                    $priority = json_decode(DAK_PRIORITY_TYPE, true);
                    $security = json_decode(DAK_SECRECY_TYPE, true);

                    $status = (isset($priority[$DakNagoriks['dak_priority_level']])
                        && $DakNagoriks['dak_priority_level'] > 0 ? ('"' . $priority[$DakNagoriks['dak_priority_level']] . '" ')
                            : '') . (isset($security[$DakNagoriks['dak_security_level']])
                        && $DakNagoriks['dak_security_level'] > 0 ? ('"' . $security[$DakNagoriks['dak_security_level']] . '" ')
                            : '');

                    $subject = $DakNagoriks['dak_subject'];
                    if (!$this->NotificationSet('inbox',
                        array(1, "Nagorik Dak"),
                        1, $current_office, $toOfficers, $subject)) {

                    }
                    // Give Receiver Notification
                    $title = 'আপনার ডেস্কে নতুন ডাক এসেছে';
                    $mail_sender = [
                        'sender' => h($logged_user_info["personal_info"]["name_bng"] . ', ' . $current_office["designation_label"] . ', ' . $current_office['office_unit_name']),
                        'subject' => h($subject),
                        'dak_type' => __('Nagorik Dak'),
                        'time' => Time::parse(date('Y-m-d H:i:s'))->i18nFormat(null, null, 'bn-BD'),
                    ];
                    $mail_sender_notify = 'প্রেরকঃ ' . $mail_sender['sender'] . "\n বিষয়ঃ " . h($mail_sender['subject']) . "\n ডাকের ধরনঃ " . $mail_sender['dak_type'] . "\n সময়ঃ " . $mail_sender['time'];
                    $this->notifyAppUser('', $dakUser['to_officer_id'], ['title' => $title, 'body' => $mail_sender], ['title' => $title, 'body' => $mail_sender_notify]);
                    // Give Receiver Notification
                }
            }

            $DakUserActionsTable = TableRegistry::get('DakUserActions');

            $DakUserAction = $DakUserActionsTable->newEntity();
            $DakUserAction->dak_user_id = $DakUsers_draft['id'];
            $DakUserAction->dak_id = $dak_id;
            $DakUserAction->dak_action = "Sent";
            $DakUserAction->created_by = $this->Auth->user('id');
            $DakUserAction->modified_by = $this->Auth->user('id');
            $DakUserActionsTable->save($DakUserAction);
        }


        if (empty($dakId)) {
            if (!$this->NotificationSet('sent',
                array(1, "Nagorik Dak"), 1,
                $current_office, [], $subject)) {

            }
            $this->response->body(json_encode(1));
            $this->response->type('application/json');
            return $this->response;
        } else {
            return true;
        }
    }

    public function getExistingDakUser($dak_id = 0, $dak_officer_unit_id = 0,
                                       $dak_office_organogram_id = 0,
                                       $dakType = DAK_DAPTORIK)
    {
        $DakUsersTable = TableRegistry::get('DakUsers');
        $data = $DakUsersTable->find()->where(['dak_id' => $dak_id, 'to_office_unit_id' => $dak_officer_unit_id,
            'to_officer_designation_id' => $dak_office_organogram_id, 'dak_type' => $dakType])->first();
        return $data;
    }

    public function getNotExistingDakUser($dak_id = 0, $dak_officer_des_id = 0,
                                          $dakType = DAK_NAGORIK)
    {
        $DakUsersTable = TableRegistry::get('DakUsers');
        $data = $DakUsersTable->find()->where(['dak_id' => $dak_id, 'dak_type' => $dakType, 'dak_category <>' => 'Sent']);
        return $data;
    }

    public function NagorikAbedon($referenceCode = null)
    {
        $this->layout = "nagorik";

        if (!empty($referenceCode)) {
            $this->set('referenceCode', $referenceCode);
            if (!empty($this->request->query['type']))
                $this->set('new', $this->request->query['type']);
            else $this->set('new', '0');
        }
    }

    public function portalAbedonStatus($referenceCode = null)
    {
        $this->layout = "portal";

        if (!empty($referenceCode)) {
            $this->set('referenceCode', $referenceCode);
            if (!empty($this->request->query['type']))
                $this->set('new', $this->request->query['type']);
            else $this->set('new', '0');
        }
    }

    public function OnlineAbedon($id = 0)
    {
        $this->layout = "nagorik";

        $office_front_desk_table = TableRegistry::get('OfficeFrontDesk');

        $con = ConnectionManager::get('default');

        if ($this->request->is('post', 'put')) {
            if (!empty($this->request->data)) {
                if (!empty($this->request->data['office_id'])) {
                    $this->switchOffice($this->request->data['office_id'],
                        'ReceiverOffice');
                    TableRegistry::remove('DakNagoriks');
                    $dak_nagorik_table = TableRegistry::get('DakNagoriks');
                    if (empty($id)) {
                        $dak_nagorik = $dak_nagorik_table->newEntity();
                        $dak_nagorik->feedback_type = 3;
                    } else {
                        $dak_nagorik = $dak_nagorik_table->get($id);
                    }
                    try {
                        $con->begin();
                        $DakDaptoriksTable = TableRegistry::get('DakDaptoriks');
                        $dak_countdp = $DakDaptoriksTable->find()->where(['receiving_office_id' => $this->request->data['office_id']])->order(['cast(docketing_no as signed)' => 'desc'])->first();
                        $dak_countng = $dak_nagorik_table->find()->where(['receiving_office_id' => $this->request->data['office_id']])->order(['cast(docketing_no as signed)' => 'desc'])->first();
                        $dak_count = max([(!empty($dak_countdp['docketing_no'])
                                ? $dak_countdp['docketing_no'] : 0), (!empty($dak_countng['docketing_no'])
                                ? $dak_countng['docketing_no'] : 0)]) + 1;

                        $this->request->data['sender_name'] = $this->request->data['name_bng'];
                        $this->request->data['dak_type_id'] = 2;
                        $this->request->data['docketing_no'] = $dak_count;

                        $employee_office = $office_front_desk_table->find()->where(['office_id' => $this->request->data['office_id']])->first();


                        if (!empty($this->request->data['uploaded_attachments'])) {
                            $this->set('attachments',
                                explode(',',
                                    $this->request->data['uploaded_attachments']));
                        }
                        if (empty($employee_office)) {
                            $this->Flash->error("দুঃখিত! অফিসের ফ্রন্ট ডেস্ক এর তথ্য পাওয়া যায়নি");
                            $con->rollback();
                        } else {


                            $this->request->data['uploader_designation_id'] = $employee_office['office_unit_organogram_id'];
                            $this->request->data['receiving_officer_id'] = $employee_office['officer_id'];
                            $this->request->data['receiving_officer_name'] = $employee_office['officer_name'];
                            $this->request->data['receiving_officer_designation_id']
                                = $employee_office['office_unit_organogram_id'];
                            $this->request->data['receiving_officer_designation_label']
                                = $employee_office['designation_label'];
                            $this->request->data['receiving_office_id'] = $employee_office['office_id'];
                            $this->request->data['receiving_office_unit_id'] = $employee_office['office_unit_id'];
                            $this->request->data['receiving_office_unit_name'] = $employee_office['office_unit_name'];

                            $this->request->data['dak_received_no'] = $this->generateDakReceivedNo($employee_office['office_id'],
                                1);
                            $this->request->data['receive_date'] = date("Y-m-d H:i:s");

                            $this->request->data['dak_status'] = DAK_CATEGORY_SENT;
                            $this->request->data['sender_name'] = $this->request->data['name_bng'];

                            $dak_nagorik = $dak_nagorik_table->patchEntity($dak_nagorik,
                                $this->request->data, ['validate' => 'add']);

                            $error = $dak_nagorik->errors();

                            if (empty($error)) {
                                $dak_nagorik->receive_date = date("Y-m-d H:i:s");
                                $dak_nagorik = $dak_nagorik_table->save($dak_nagorik);
                                $this->request->data['dak_id'] = $dak_nagorik->id;
                                $table_instance_da = TableRegistry::get('DakAttachments');

                                $this->request->data['attachment'] = explode(',',
                                    $this->request->data['uploaded_attachments']);

                                foreach ($this->request->data['attachment'] as $file) {
                                    if (!empty($file)) {
                                        $dak_attachment = array();
                                        $dak_attachment = $table_instance_da->newEntity();
                                        $dak_attachment['dak_type'] = DAK_NAGORIK;
                                        $dak_attachment['dak_id'] = $dak_nagorik->id;
                                        $dak_attachment['attachment_description']
                                            = !empty($this->request->data['file_description'])
                                            ? $this->request->data['file_description']
                                            : '';


                                        $dak_attachment['attachment_type'] = $this->getMimeType($file);

                                        $file_path_arr = explode(FILE_FOLDER,
                                            $file);

                                        $dak_attachment['file_dir'] = FILE_FOLDER;
                                        $dak_attachment['file_name'] = $file_path_arr[1];
                                        $explode = explode('?token=', $dak_attachment['file_name']);
                                        $dak_attachment['file_name'] = $explode[0];
                                        $table_instance_da->save($dak_attachment);
                                    }
                                }


                                if (!empty($dak_nagorik->description)) {
                                    $attachment_data = array();
                                    $attachment_data['dak_type'] = DAK_NAGORIK;
                                    $attachment_data['dak_id'] = $dak_nagorik->id;
                                    $attachment_data['is_main'] = 1;
                                    $attachment_data['attachment_description'] = !empty($this->request->data['file_description'])
                                        ? $this->request->data['file_description']
                                        : '';
                                    $attachment_data['attachment_type'] = "text";
                                    $file_path_arr = '';
                                    $attachment_data['file_dir'] = '';
                                    $attachment_data['file_name'] = '';
                                    $attachment_data['content_body'] = $dak_nagorik->description;

                                    $dak_attachment = $table_instance_da->newEntity();
                                    $dak_attachment = $table_instance_da->patchEntity($dak_attachment,
                                        $attachment_data);
                                    $table_instance_da->save($dak_attachment);
                                }

                                if ($this->addNagorikOnlineAbedorn($dak_nagorik,
                                    $employee_office)) {
                                    $this->addDakTracks($this->request->data);

                                    $con->commit();

                                    if (!$this->NotificationSet('inbox',
                                        array(1, "Online Nagorik Dak"),
                                        1, ['officer_name' => $this->request->data['name_bng']], $employee_office)) {

                                    }

                                    if (!empty($this->request->data['email'])) {
//                                        $this->SendEmailMessage($this->request->data['email'],
//                                            "প্রিয় ".$this->request->data['name_bng'].",<br/><br/>আপনার আবেদন ".$employee_office['office_name']." এর নিকট পাঠানো হয়েছে। আপনার আবেদন নম্বর হলো ".$this->request->data['dak_received_no']."<br/>ভবিষ্যৎ অনুসন্ধানের জন্যআবেদন নম্বররটি সংরক্ষণ করুন।<br/>ধন্যবাদ।",
//                                            'অনলাইন নাগরিক আবেদন');

                                        $email_options['email_type'] = EMAIL_DAK;
                                        $email_options['subject'] = "অনলাইন নাগরিক আবেদন";
                                        $email_options['office_id'] = $employee_office['office_id'];
                                        $email_options['office_name'] = $employee_office['office_name'];
                                        $email_message = "প্রিয় " . $this->request->data['name_bng'] . ",<br/><br/>আপনার আবেদন " . $employee_office['office_name'] . " এর নিকট পাঠানো হয়েছে। আপনার আবেদন নম্বর হলো " . $this->request->data['dak_received_no'] . "<br/>ভবিষ্যৎ অনুসন্ধানের জন্যআবেদন নম্বররটি সংরক্ষণ করুন।<br/>ধন্যবাদ।";
                                        $this->SendEmailMessage($this->request->data['email'], $email_message, $email_options);
                                    }
                                    if (!empty($this->request->data['mobile_no'])) {
                                        $this->SendSmsMessage($this->request->data['mobile_no'],
                                            "Dear " . $this->request->data['name_eng'] . ",\nYour request is sent. Your receipt no is " . $this->request->data['dak_received_no'] . "\nThank you.");
                                    }

                                    $this->redirect(['action' => 'NagorikAbedon',
                                        $this->request->data['dak_received_no'],
                                        '?' => ['type' => 'NagorikAbedon']]);
                                } else {
                                    $this->Flash->error("দুঃখিত! সবগুলো তথ্য সঠিক ভাবে দেয়া হয়নি");
                                    $con->rollback();
                                }
                            } else {
                                $this->Flash->error("দুঃখিত! সবগুলো তথ্য সঠিক ভাবে দেয়া হয়নি");
                                $con->rollback();
                            }
                        }
                    } catch (Exception $ex) {
                        $this->Flash->error("দুঃখিত! সবগুলো তথ্য সঠিক ভাবে দেয়া হয়নি");
                        $con->rollback();
                    } catch (NotFoundException $ex) {
                        $this->Flash->error("দুঃখিত! সবগুলো তথ্য সঠিক ভাবে দেয়া হয়নি");
                        $con->rollback();
                    } catch (\PDOException $ex) {
                        $this->Flash->error("দুঃখিত! সবগুলো তথ্য সঠিক ভাবে দেয়া হয়নি");
                        $con->rollback();
                    }
                } else {
                    $this->Flash->error('দুঃখিত! অফিস বাছাই করা হয়নি');
                }
            } else {

            }
        }

        $this->set(compact('dak_nagorik'));
    }

    public function onlinePortalAbedon($domain = '', $id = 0)
    {
        $this->layout = "portal";

        $table_office = TableRegistry::get('Offices');
        $office_front_desk_table = TableRegistry::get('OfficeFrontDesk');

        $this->set('offices', $table_office->getOfficeListByDomain());

        $con = ConnectionManager::get('default');

        if ($this->request->is('post', 'put')) {
            if (!empty($this->request->data)) {
                if (!empty($this->request->data['office_id'])) {
                    $this->switchOffice($this->request->data['office_id'],
                        'ReceiverOffice');
                    TableRegistry::remove('DakNagoriks');
                    $dak_nagorik_table = TableRegistry::get('DakNagoriks');
                    if (empty($id)) {
                        $dak_nagorik = $dak_nagorik_table->newEntity();
                        $dak_nagorik->feedback_type = 3;
                    } else {
                        $dak_nagorik = $dak_nagorik_table->get($id);
                    }
                    try {
                        $con->begin();
                        $DakDaptoriksTable = TableRegistry::get('DakDaptoriks');
                        $dak_countdp = $DakDaptoriksTable->find()->where(['receiving_office_id' => $this->request->data['office_id']])->order(['cast(docketing_no as signed)' => 'desc'])->first();
                        $dak_countng = $dak_nagorik_table->find()->where(['receiving_office_id' => $this->request->data['office_id']])->order(['cast(docketing_no as signed)' => 'desc'])->first();
                        $dak_count = max([(!empty($dak_countdp['docketing_no']) ? $dak_countdp['docketing_no'] : 0), (!empty($dak_countng['docketing_no']) ? $dak_countng['docketing_no'] : 0)]) + 1;

                        $this->request->data['sender_name'] = $this->request->data['name_bng'];
                        $this->request->data['dak_type_id'] = 2;
                        $this->request->data['mobile_no'] = !empty($this->request->data['mobile']) ? $this->BngToEng($this->request->data['mobile']) : (!empty($this->request->data['mobile_no']) ? $this->BngToEng($this->request->data['mobile_no']) : '');
                        $this->request->data['docketing_no'] = $dak_count;

                        $employee_office = $office_front_desk_table->find()->where(['office_id' => $this->request->data['office_id']])->first();


                        if (!empty($this->request->data['uploaded_attachments'])) {
                            $this->set('attachments', explode(',', $this->request->data['uploaded_attachments']));
                        }
                        if (empty($employee_office)) {
                            $this->Flash->error("দুঃখিত! অফিসের ফ্রন্ট ডেস্ক এর তথ্য পাওয়া যায়নি");
                            $con->rollback();
                        } else {


                            $this->request->data['uploader_designation_id'] = $employee_office['office_unit_organogram_id'];
                            $this->request->data['receiving_officer_id'] = $employee_office['officer_id'];
                            $this->request->data['receiving_officer_name'] = $employee_office['officer_name'];
                            $this->request->data['receiving_officer_designation_id']
                                = $employee_office['office_unit_organogram_id'];
                            $this->request->data['receiving_officer_designation_label']
                                = $employee_office['designation_label'];
                            $this->request->data['receiving_office_id'] = $employee_office['office_id'];
                            $this->request->data['receiving_office_unit_id'] = $employee_office['office_unit_id'];
                            $this->request->data['receiving_office_unit_name'] = $employee_office['office_unit_name'];

                            $this->request->data['dak_received_no'] = $this->generateDakReceivedNo($employee_office['office_id'],
                                1);
                            $this->request->data['receive_date'] = date("Y-m-d H:i:s");

                            $this->request->data['dak_status'] = DAK_CATEGORY_SENT;
                            $this->request->data['sender_name'] = $this->request->data['name_bng'];
                            $this->request->data['application_origin'] = !empty($this->request->data['service_name']) ? $this->request->data['service_name'] : 'nothi';

                            $dak_nagorik = $dak_nagorik_table->patchEntity($dak_nagorik,
                                $this->request->data, ['validate' => 'add']);

                            $error = $dak_nagorik->errors();

                            if (empty($error)) {
                                $dak_nagorik->receive_date = date("Y-m-d H:i:s");
                                $dak_nagorik = $dak_nagorik_table->save($dak_nagorik);
                                $this->request->data['dak_id'] = $dak_nagorik->id;
                                $table_instance_da = TableRegistry::get('DakAttachments');

                                $this->request->data['attachment'] = explode(',',
                                    $this->request->data['uploaded_attachments']);

                                foreach ($this->request->data['attachment'] as $file) {
                                    if (!empty($file)) {
                                        $dak_attachment = array();
                                        $dak_attachment = $table_instance_da->newEntity();
                                        $dak_attachment['dak_type'] = DAK_NAGORIK;
                                        $dak_attachment['dak_id'] = $dak_nagorik->id;
                                        $dak_attachment['attachment_description']
                                            = !empty($this->request->data['file_description'])
                                            ? $this->request->data['file_description']
                                            : '';


                                        $dak_attachment['attachment_type'] = $this->getMimeType($file);

                                        $file_path_arr = explode(FILE_FOLDER,
                                            $file);

                                        $dak_attachment['file_dir'] = FILE_FOLDER;
                                        $dak_attachment['file_name'] = $file_path_arr[1];

                                        $explode = explode('?token=', $dak_attachment['file_name']);
                                        $dak_attachment['file_name'] = $explode[0];
                                        $table_instance_da->save($dak_attachment);
                                    }
                                }


                                if (!empty($dak_nagorik->description)) {
                                    $attachment_data = array();
                                    $attachment_data['dak_type'] = DAK_NAGORIK;
                                    $attachment_data['is_main'] = 1;
                                    $attachment_data['dak_id'] = $dak_nagorik->id;
                                    $attachment_data['attachment_description'] = !empty($this->request->data['file_description'])
                                        ? $this->request->data['file_description']
                                        : '';
                                    $attachment_data['attachment_type'] = "text";
                                    $file_path_arr = '';
                                    $attachment_data['file_dir'] = '';
                                    $attachment_data['file_name'] = '';
                                    $attachment_data['content_body'] = $dak_nagorik->description;

                                    $dak_attachment = $table_instance_da->newEntity();
                                    $dak_attachment = $table_instance_da->patchEntity($dak_attachment,
                                        $attachment_data);
                                    $table_instance_da->save($dak_attachment);
                                }

                                if ($this->addNagorikOnlineAbedorn($dak_nagorik,
                                    $employee_office)) {
                                    $this->addDakTracks($this->request->data);

                                    $con->commit();

                                    if (!$this->NotificationSet('inbox',
                                        array(1, "Online Nagorik Dak"),
                                        1, ['officer_name' => $this->request->data['name_bng']], $employee_office)) {

                                    }

                                    if (!empty($this->request->data['email'])) {
//                                        $this->SendEmailMessage($this->request->data['email'],
//                                            "প্রিয় ".$this->request->data['name_bng'].",<br/><br/>আপনার আবেদন ".$employee_office['office_name']." এর নিকট পাঠানো হয়েছে। আপনার আবেদন নম্বর হলো ".$this->request->data['dak_received_no']."<br/>ভবিষ্যৎ অনুসন্ধানের জন্যআবেদন নম্বররটি সংরক্ষণ করুন।<br/>ধন্যবাদ।",
//                                            'অনলাইন নাগরিক আবেদন');

                                        $email_options['email_type'] = EMAIL_DAK;
                                        $email_options['subject'] = "অনলাইন নাগরিক আবেদন";
                                        $email_options['office_id'] = $employee_office['office_id'];
                                        $email_options['office_name'] = $employee_office['office_name'];
                                        $email_message = "প্রিয় " . $this->request->data['name_bng'] . ",<br/><br/>আপনার আবেদন " . $employee_office['office_name'] . " এর নিকট পাঠানো হয়েছে। আপনার আবেদন নম্বর হলো " . $this->request->data['dak_received_no'] . "<br/>ভবিষ্যৎ অনুসন্ধানের জন্যআবেদন নম্বররটি সংরক্ষণ করুন।<br/>ধন্যবাদ।";
                                        $this->SendEmailMessage($this->request->data['email'], $email_message, $email_options);

                                    }
                                    if (!empty($this->request->data['mobile_no'])) {
                                        $this->SendSmsMessage($this->request->data['mobile_no'],
                                            "Dear " . $this->request->data['name_eng'] . ",\nYour request is sent. Your receipt no is " . $this->request->data['dak_received_no'] . "\nThank you.");
                                    }

                                    $this->redirect(['action' => 'OnlinePortalAbedon']);
                                } else {
                                    $this->Flash->error("দুঃখিত! সবগুলো তথ্য সঠিক ভাবে দেয়া হয়নি");
                                    $con->rollback();
                                }
                            } else {
                                $this->Flash->error("দুঃখিত! সবগুলো তথ্য সঠিক ভাবে দেয়া হয়নি");
                                $con->rollback();
                            }
                        }
                    } catch (Exception $ex) {
                        $this->Flash->error("দুঃখিত! সবগুলো তথ্য সঠিক ভাবে দেয়া হয়নি");
                        $con->rollback();
                    } catch (NotFoundException $ex) {
                        $this->Flash->error("দুঃখিত! সবগুলো তথ্য সঠিক ভাবে দেয়া হয়নি");
                        $con->rollback();
                    } catch (\PDOException $ex) {
                        $this->Flash->error("দুঃখিত! সবগুলো তথ্য সঠিক ভাবে দেয়া হয়নি");
                        $con->rollback();
                    }
                } else {
                    $this->Flash->error('দুঃখিত! অফিস বাছাই করা হয়নি');
                }
            } else {

            }
        }

        $this->set(compact('dak_nagorik'));
    }

    private function addDakTracks($data, $meta = [])
    {
        $table = TableRegistry::get('DakTracks');
        $dt = $table->newEntity();

        if (!empty($data['dak_id'])) $dt->dak_id = $data['dak_id'];
        if (!empty($data['dak_type'])) $dt->dak_type = $data['dak_type'];
        if (!empty($data['service_name'])) $dt->service_name = !empty($data['service_name']) ? strtolower($data['service_name']) : 'nothi';
        if (!empty($data['sarok_no'])) $dt->sarok_no = $data['sarok_no'];
        if (!empty($data['tracking_id'])) $dt->forms_track_no = !empty($data['tracking_id']) ? $data['tracking_id'] : '';
        if (!empty($data['sender_office_id'])) $dt->sender_office_id = $data['sender_office_id'];
        if (!empty($data['sender_officer_designation_id'])) $dt->sender_officer_designation_id = $data['sender_officer_designation_id'];
        if (!empty($data['sender_name'])) $dt->sender_name = $data['sender_name'];
        if (!empty($data['email'])) $dt->sender_email = $data['email'];
        if (!empty($data['mobile'])) $dt->sender_mobile = $this->BngToEng($data['mobile']);
        if (!empty($data['mobile_no'])) $dt->sender_mobile = $this->BngToEng($data['mobile_no']);
        if (!empty($data['dak_received_no'])) $dt->dak_received_no = $data['dak_received_no'];
        if (!empty($data['docketing_no'])) $dt->docketing_no = $data['docketing_no'];
        if (!empty($data['receive_date'])) $dt->receiving_date = $data['receive_date'];
        if (!empty($data['dak_subject'])) $dt->dak_subject = $data['dak_subject'];
        if (!empty($data['receiving_office_id'])) $dt->receiving_office_id = $data['receiving_office_id'];
        if (!empty($data['receiving_officer_designation_label'])) $dt->receiving_officer_designation_label = !empty($data['receiving_officer_designation_label']) ? $data['receiving_officer_designation_label'] : '';
        if (!empty($data['receiving_officer_name'])) $dt->receiving_officer_name = !empty($data['receiving_officer_name']) ? $data['receiving_officer_name'] : '';

        if ($table->save($dt)) {
            return true;
        }

        return false;
    }

    private function addNagorikOnlineAbedorn($dak_nagoriks,
                                             $selected_office_section)
    {


        try {
            $table_instance_du = TableRegistry::get('DakUsers');
            $table_instance_dua = TableRegistry::get('DakUserActions');
            $DakMovementsTable = TableRegistry::get('DakMovements');

            $drafter = $table_instance_du->newEntity();
            $drafter->dak_type = DAK_NAGORIK;
            $drafter->dak_id = $dak_nagoriks->id;
            $drafter->to_office_id = $selected_office_section['office_id'];
            $drafter->to_office_name = $selected_office_section['office_name'];
            $drafter->to_office_unit_id = $selected_office_section['office_unit_id'];
            $drafter->to_office_unit_name = $selected_office_section['office_unit_name'];
            $drafter->to_office_address = !empty($selected_office_section['office_address']) ? $selected_office_section['office_address'] : '';
            $drafter->to_officer_id = $selected_office_section['officer_id'];
            $drafter->to_officer_name = $selected_office_section['officer_name'];
            $drafter->to_officer_designation_id = $selected_office_section['office_unit_organogram_id'];
            $drafter->to_officer_designation_label = $selected_office_section['designation_label'];
            $drafter->dak_view_status = DAK_VIEW_STATUS_NEW;
            $drafter->dak_priority = 0;
            $drafter->attention_type = 1;
            $drafter->dak_category = DAK_CATEGORY_INBOX;
            $drafter->created_by = 0;
            $drafter->modified_by = 0;

            $drafter = $table_instance_du->save($drafter);

            $user_action = $table_instance_dua->newEntity();
            $user_action->dak_id = $dak_nagoriks->id;
            $user_action->dak_type = DAK_NAGORIK;
            $user_action->dak_user_id = $drafter->id;
            $user_action->dak_action = DAK_CATEGORY_SENT;
            $table_instance_dua->save($user_action);

            $second_move = $DakMovementsTable->newEntity();
            $second_move->dak_type = DAK_NAGORIK;
            $second_move->dak_id = $dak_nagoriks->id;

            $second_move->from_office_address = $dak_nagoriks->address;
            $second_move->from_officer_name = $dak_nagoriks->name_bng;

            $second_move->to_office_id = $selected_office_section['office_id'];
            $second_move->to_office_name = $selected_office_section['office_name'];
            $second_move->to_office_unit_id = $selected_office_section['office_unit_id'];
            $second_move->to_office_unit_name = $selected_office_section['office_unit_name'];
            $second_move->to_office_address = !empty($selected_office_section['office_address']) ? $selected_office_section['office_address'] : '';
            $second_move->to_officer_id = $selected_office_section['officer_id'];
            $second_move->to_officer_name = $selected_office_section['officer_name'];
            $second_move->to_officer_designation_id = $selected_office_section['office_unit_organogram_id'];
            $second_move->to_officer_designation_label = $selected_office_section['designation_label'];
            $second_move->attention_type = 1;
            $second_move->dak_dagorik_type = 2;
            $second_move->docketing_no = $dak_nagoriks->docketing_no;
            $second_move->from_sarok_no = "";
            $second_move->to_sarok_no = "";
            $second_move->operation_type = DAK_CATEGORY_FORWARD;
            $second_move->sequence = 2;
            $second_move->dak_actions = API_FIRST_SUBJECT;
            $second_move->dak_priority = 0;

            $second_move = $DakMovementsTable->save($second_move);
            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

    public function getTracking()
    {
        $this->layout = null;
        $employee_office = $this->getCurrentDakSection();

        $response = array(
            'status' => 'error',
            'msg' => "দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না"
        );

        try {
            if (!empty($this->request->data)) {

//            if (isset($this->request->data['api_key'])) {
//                if ($this->request->data['api_key'] != API_KEY) {
//                    echo json_encode($response);
//                    die;
//                }
//            }
//
                $conditions = [];

                $receive = !empty($this->request->data['receive_no']) ? $this->request->data['receive_no']
                    : null;
                $receive = $this->BngToEng($receive);
                $mobile = !empty($this->request->data['mobile_no']) ? $this->request->data['mobile_no']
                    : null;
                $mobile = $this->BngToEng($mobile);
                $date_from = !empty($this->request->data['date_from']) ? $this->request->data['date_from']
                    : null;
                $date_to = !empty($this->request->data['date_to']) ? $this->request->data['date_to']
                    : null;

                $data = [];

                if (empty($employee_office)) {

                    $table = TableRegistry::get('DakTracks');

                    if (!empty($receive)) {
                        $office_id = $table->find()->select(['receiving_office_id', 'dak_received_no'])->where(['OR' => [['forms_track_no' => $receive], ['dak_received_no' => $receive]], ['dak_type' => 'Nagorik']])->first();

                        if (!empty($office_id['receiving_office_id'])) {
                            $this->switchOffice($office_id['receiving_office_id'],
                                'ReceiverOffice');

                            TableRegistry::remove('DakNagoriks');
                            $dakNagoriksData = TableRegistry::get('DakNagoriks')->find()
                                ->where(["FIND_IN_SET({$office_id['dak_received_no']},DakNagoriks.previous_receipt_no)"])
                                ->first();

                            if (empty($dakNagoriksData)) {
                                $dakNagoriksData = TableRegistry::get('DakNagoriks')->find()
                                    ->where(['dak_received_no' => $office_id['dak_received_no']])->first();
                            }

                            if (!empty($dakNagoriksData)) {

                                if (!empty($dakNagoriksData['dak_received_no'])) {
                                    $receive = $dakNagoriksData['dak_received_no'];
                                }

                                if ($dakNagoriksData['dak_status'] == 'NothiVukto' || $dakNagoriksData['dak_status'] == 'NothiJato') {
                                    TableRegistry::remove('NothiDakPotroMaps');
                                    $nothiDakPotroMaps = TableRegistry::get('NothiDakPotroMaps')
                                        ->getNothiInformationForDak($dakNagoriksData['id'], 'Nagorik');

                                    TableRegistry::remove('NothiMasterCurrentUsers');
                                    $nothiMasterCurrentUsers = TableRegistry::get('NothiMasterCurrentUsers')
                                        ->getCurrentUserDetailsWithData($nothiDakPotroMaps['nothi_masters_id'],
                                            $nothiDakPotroMaps['nothi_part_no'],
                                            $dakNagoriksData['receiving_office_id']);

                                    if (!empty($nothiMasterCurrentUsers)) {
                                        $isnisponno = $nothiMasterCurrentUsers['data']['is_finished'] === 1 ? "নিস্পত্তি করা হয়েছে" : "প্রক্রিয়াধীন আছে ";
                                        $data[0]['dak_actions'] = "আপনার আবেদনটি নথিতে {$isnisponno} (বর্তমান ডেস্ক:{$nothiMasterCurrentUsers['text']} , গ্রহণের সময়: {$nothiMasterCurrentUsers['data']->issue_date->format("Y-m-d")})";
                                        $data[0]['dak_received_no'] = $receive;
                                        $data[0]['dakcreated'] = $dakNagoriksData['receive_date'];
                                        $data[0]['dak_subject'] = $dakNagoriksData['dak_subject'];

                                        $response = array(
                                            'status' => 'success',
                                            'data' => $data
                                        );
                                    } else {
                                        $response = array(
                                            'status' => 'error',
                                            'msg' => "দুঃখিত! তথ্য পাওয়া যায়নি"
                                        );
                                    }
                                } else {
                                    TableRegistry::remove("DakMovements");
                                    $dakMovements_table = TableRegistry::get("DakMovements");
                                    if (!empty($receive) || !empty($mobile) || !empty($date_from) || !empty($date_to)) {
                                        if (!empty($receive)) {
                                            $conditions['DakNagoriks.dak_received_no'] = h($receive);
                                        }

                                        if (!empty($date_from)) {
                                            $conditions['date(DakNagoriks.created) >='] = date("Y-m-d", strtotime($date_from));
                                        }

                                        if (!empty($date_to)) {
                                            $conditions['date(DakNagoriks.created) <='] = date("Y-m-d", strtotime($date_to));
                                        }

                                        $tracking = $dakMovements_table->getLastStatusofEachNaogirk($conditions);
                                        if (empty($tracking)) $tracking = [];
                                        $data = array_merge($data, $tracking);

                                        if (!empty($data)) {
                                            if ($data[0]['sequence'] > 1) {
                                                $data[0]['dak_actions'] = "আপনার আবেদনটি প্রক্রিয়াধীন আছে (বর্তমান ডেস্ক: {$data[0]['to_officer_designation_label']}, {$data[0]['to_office_unit_name']}, গ্রহণের সময়: " . $data[0]['movemodified'] . ")";
                                            }
                                            $response = array(
                                                'status' => 'success',
                                                'data' => $data
                                            );
                                        } else {
                                            $response = array(
                                                'status' => 'error',
                                                'msg' => "দুঃখিত! তথ্য পাওয়া যায়নি"
                                            );
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    // as nothijato dak can be back so we need to check whether nothijato back data is searched by user - start.
                    if (!empty($receive)) {
                        TableRegistry::remove('DakNagoriks');
                        $dakNagoriksData = TableRegistry::get('DakNagoriks')->find()->select(['dak_received_no'])->where(["FIND_IN_SET({$receive},DakNagoriks.previous_receipt_no)"])->order(['id desc'])->first();
                        if (empty($dakNagoriksData)) {
                            $dakNagoriksData = TableRegistry::get('DakNagoriks')->find()
                                ->where(['dak_received_no' => $receive])->first();
                        }
                        if (!empty($dakNagoriksData) && !empty($dakNagoriksData['dak_received_no'])) {
                            $receive = $dakNagoriksData['dak_received_no'];
                        }
                    }

                    TableRegistry::remove("DakMovements");
                    $dakMovements_table = TableRegistry::get("DakMovements");
                    if (!empty($receive) || !empty($mobile) || !empty($date_from) || !empty($date_to)) {
                        if (!empty($employee_office)) {
                            $conditions['DakNagoriks.receiving_office_id'] = ($employee_office['office_id']);
                        }
                        if (!empty($receive)) {
                            $conditions['DakNagoriks.dak_received_no'] = h($receive);
                        }
                        if (!empty($mobile)) {
                            $conditions['DakNagoriks.mobile_no'] = h($mobile);
                        }

                        if (!empty($date_from)) {
                            $conditions['date(DakNagoriks.created) >='] = date("Y-m-d", strtotime($date_from));
                        }

                        if (!empty($date_to)) {
                            $conditions['date(DakNagoriks.created) <='] = date("Y-m-d", strtotime($date_to));
                        }

                        $tracking = $dakMovements_table->getLastStatusofEachNaogirk($conditions);
                        $data = array_merge($data, $tracking);
                        if (!empty($data)) {
                            if ($data[0]['nagorikstatus'] == 'NothiVukto' ||
                                $data[0]['nagorikstatus'] == 'NothiJato') {
                                TableRegistry::remove('NothiDakPotroMaps');
                                $nothiDakPotroMaps = TableRegistry::get('NothiDakPotroMaps')
                                    ->getNothiInformationForDak($data[0]['dak_id'], 'Nagorik');

                                TableRegistry::remove('NothiMasterCurrentUsers');
                                $nothiMasterCurrentUsers = TableRegistry::get('NothiMasterCurrentUsers')
                                    ->getCurrentUserDetailsWithData($nothiDakPotroMaps['nothi_masters_id'],
                                        $nothiDakPotroMaps['nothi_part_no'],
                                        $data[0]['to_office_id']);

                                if (!empty($nothiMasterCurrentUsers)) {
                                    $isnisponno = $nothiMasterCurrentUsers['data']['is_finished'] === 1 ? "নিস্পত্তি করা হয়েছে" : "প্রক্রিয়াধীন আছে ";
                                    $data[0]['dak_actions'] = "আপনার আবেদনটি নথিতে {$isnisponno}";

                                    $response = array(
                                        'status' => 'success',
                                        'data' => $data
                                    );
                                } else {
                                    $response = array(
                                        'status' => 'error',
                                        'msg' => "দুঃখিত! তথ্য পাওয়া যায়নি"
                                    );
                                }
                            } else {
                                if ($data[0]['sequence'] > 1) {
                                    $data[0]['dak_actions'] = "আপনার আবেদনটি প্রক্রিয়াধীন আছে (বর্তমান ডেস্ক: {$data[0]['to_officer_designation_label']}, {$data[0]['to_office_unit_name']}, গ্রহণের সময়: " . $data[0]['movemodified'] . ")";
                                }
                                $response = array(
                                    'status' => 'success',
                                    'data' => $data
                                );
                            }
                        } else {
                            $response = array(
                                'status' => 'error',
                                'msg' => "দুঃখিত! তথ্য পাওয়া যায়নি"
                            );
                        }
                    }
                }


                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
        } catch (\Exception $ex) {
            $response = array(
                'status' => 'error',
                'msg' => "দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না",
                'reason' => ''
            );
            if ($this->request->is('post')) {
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            } else {
                $this->Flash->error($response['msg']);
            }
        }

        die;
    }

    public function trackNagorikDak()
    {

    }

    /* API of this Controller */

    public function apiGetCurrentStatus()
    {
        $this->layout = null;
        $employee_office = $this->getCurrentDakSection();

        $response = array(
            'status' => 'error',
            'msg' => "দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না"
        );

        if ($this->request->is('ajax', 'post')) {

            if (isset($this->request->data['api_key'])) {
                if (empty($this->request->data['api_key']) || $this->checkToken($this->request->data['api_key']) == FALSE) {
                    echo json_encode($response);
                    die;
                }
            }
            $dakMovements_table = TableRegistry::get("DakMovements");
            $conditions = '';
            $receive = !empty($this->request->data['dak_receive_no'])
                ? $this->request->data['dak_receive_no'] : 0;
            $mobile = !empty($this->request->data['mobile_no']) ? $this->request->data['mobile_no']
                : 0;
            $docketing_no = !empty($this->request->data['docketing_no']) ? $this->request->data['docketing_no']
                : 0;
            $date_from = !empty($this->request->data['date_from']) ? $this->request->data['date_from']
                : 0;
            $date_to = !empty($this->request->data['date_to']) ? $this->request->data['date_to']
                : 0;

            if (!empty($receive) || !empty($mobile) || !empty($docketing_no) || !empty($date_from)
                || !empty($date_to)) {

                if (!empty($employee_office)) {
                    $conditions['DakNagoriks.receiving_office_id'] = $employee_office['office_id'];
                }
                if (!empty($receive)) {
                    $conditions['DakNagoriks.dak_received_no'] = h($receive);
                }
                if (!empty($mobile)) {
                    $conditions['DakNagoriks.mobile_no'] = h($mobile);
                }

                if (!empty($date_from)) {
                    $conditions['date(DakNagoriks.created) >='] = date("Y-m-d", strtotime($date_from));
                }

                if (!empty($date_to)) {
                    $conditions['date(DakNagoriks.created) <='] = date("Y-m-d", strtotime($date_to));
                }

                if (!empty($docketing_no)) {
                    $conditions['DakNagoriks.docketing_no'] = h($docketing_no);
                }

                $tracking = $dakMovements_table->getLastStatusofEachNaogirk($conditions);

                if (!empty($tracking)) {
                    $response = array(
                        'status' => 'success',
                        'data' => $tracking
                    );
                } else {
                    $response = array(
                        'status' => 'error',
                        'msg' => "দুঃখিত! তথ্য পাওয়া যায়নি"
                    );
                }
            }
        }

        $this->response->type('json');
        $this->response->body(json_encode($response));
        return $this->response;
    }

    public function apiGetMovementDetails()
    {
        $this->layout = null;
        $employee_office = $this->getCurrentDakSection();

        $response = array(
            'status' => 'error',
            'msg' => "দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না"
        );

        if ($this->request->is('ajax', 'post')) {

            if (isset($this->request->data['api_key'])) {
                if (empty($this->request->data['api_key']) || $this->checkToken($this->request->data['api_key']) == FALSE) {
                    echo json_encode($response);
                    die;
                }
            }
            $dakMovements_table = TableRegistry::get("DakMovements");
            $conditions = [];
            $receive = !empty($this->request->data['dak_receive_no'])
                ? h($this->request->data['dak_receive_no']) : 0;
            $mobile = !empty($this->request->data['mobile_no']) ? h($this->request->data['mobile_no'])
                : 0;
            $docketing_no = !empty($this->request->data['docketing_no']) ? h($this->request->data['docketing_no'])
                : 0;
            $date_from = !empty($this->request->data['date_from']) ? h($this->request->data['date_from'])
                : 0;
            $date_to = !empty($this->request->data['date_to']) ? h($this->request->data['date_to'])
                : 0;

            if (!empty($receive) || !empty($mobile) || !empty($docketing_no) || !empty($date_from)
                || !empty($date_to)) {
                if (!empty($employee_office)) {
                    $conditions['DakNagoriks.receiving_office_id'] = $employee_office['office_id'];
                }
                if (!empty($receive)) {
                    $conditions['DakNagoriks.dak_received_no'] = trim($receive);
                }
                if (!empty($mobile)) {
                    $conditions['DakNagoriks.mobile_no'] = trim($mobile);
                }

                if (!empty($docketing_no)) {
                    $conditions['DakNagoriks.docketing_no'] = $receive;
                }

                if (!empty($date_from)) {
                    $conditions['DATE(DakNagoriks.created) >='] = $date_from;
                }

                if (!empty($date_to)) {
                    $conditions['DATE(DakNagoriks.created) <='] = $date_to;
                }

                $tracking = $dakMovements_table->getFullofEachNaogirk($conditions);

                if (!empty($tracking)) {
                    $response = array(
                        'status' => 'success',
                        'data' => $tracking
                    );
                } else {
                    $response = array(
                        'status' => 'error',
                        'msg' => "দুঃখিত! তথ্য পাওয়া যায়নি"
                    );
                }
            }

            echo json_encode($response);
        }

        die;
    }

    public function apiOnlineAbedon()
    {
        $this->layout = "ajax";

        $office_front_desk_table = TableRegistry::get('OfficeFrontDesk');

        $return = array("status" => 'error', 'msg' => 'Invalid request');

        if ($this->request->is('post')) {

            $apikey = $this->request->data['api_key'];

            if ($apikey == API_KEY && !empty($this->request->data)) {
                if (empty($apikey) || $this->checkToken($apikey) == FALSE) {
                    try {
                        $this->switchOffice($this->request->data['office_id'],
                            'receiverOffice');
                        $con = ConnectionManager::get('default');
                        $dak_nagorik_table = TableRegistry::get('DakNagoriks');
                        $con->begin();

                        $dak_nagorik = $dak_nagorik_table->newEntity();

                        $DakDaptoriksTable = TableRegistry::get('DakDaptoriks');

                        $dak_countdp = $DakDaptoriksTable->find()->where(['receiving_office_id' => $this->request->data['office_id']])->order(['cast(docketing_no as signed)' => 'desc'])->first();
                        $dak_countng = $dak_nagorik_table->find()->where(['receiving_office_id' => $this->request->data['office_id']])->order(['cast(docketing_no as signed)' => 'desc'])->first();
                        $dak_count = max([(!empty($dak_countdp['docketing_no']) ? $dak_countdp['docketing_no'] : 0), (!empty($dak_countng['docketing_no']) ? $dak_countng['docketing_no'] : 0)]) + 1;

                        $this->request->data['sender_name'] = $this->request->data['name_bng'];
                        $this->request->data['dak_type_id'] = 2;
                        $this->request->data['docketing_no'] = $dak_count;

                        $dak_nagorik->feedback_type = isset($this->request->data['feedback_type'])
                            ? $this->request->data['feedback_type'] : 3;

                        $employee_office = $office_front_desk_table->find()->where(['office_id' => $this->request->data['office_id']])->first();

                        if (empty($employee_office)) {
                            $return = array("status" => 'error', 'msg' => "দুঃখিত! অফিসের ফ্রন্ট ডেস্ক এর তথ্য পাওয়া যায়নি");
                            $con->rollback();
                        } else {

                            $this->request->data['uploader_designation_id'] = $employee_office['office_unit_organogram_id'];
                            $this->request->data['receiving_officer_id'] = $employee_office['officer_id'];
                            $this->request->data['receiving_officer_name'] = $employee_office['officer_name'];
                            $this->request->data['receiving_officer_designation_id']
                                = $employee_office['office_unit_organogram_id'];
                            $this->request->data['receiving_officer_designation_label']
                                = $employee_office['designation_label'];
                            $this->request->data['receiving_office_id'] = $employee_office['office_id'];
                            $this->request->data['receiving_office_unit_id'] = $employee_office['office_unit_id'];
                            $this->request->data['receiving_office_unit_name'] = $employee_office['office_unit_name'];

                            $this->request->data['dak_received_no'] = $this->generateDakReceivedNo($employee_office['office_id'],
                                1);
                            $this->request->data['forms_track_no'] = !empty($this->request->data['tracking_id']) ? $this->request->data['tracking_id'] : '';
                            $this->request->data['receive_date'] = date("Y-m-d H:i:s");
                            $this->request->data['application_origin'] = !empty($this->request->data['service_name']) ? $this->request->data['service_name'] : 'nothi';
                            $this->request->data['dak_status'] = DAK_CATEGORY_SENT;
                            $this->request->data['sender_name'] = $this->request->data['name_bng'];

                            $dak_nagorik = $dak_nagorik_table->patchEntity($dak_nagorik,
                                $this->request->data, ['validate' => 'add']);

                            $error = $dak_nagorik->errors();

                            $return = array("status" => 'success', 'data' => array(
                                'dak_received_no' => $dak_nagorik->dak_received_no),
                                'test' => $dak_nagorik);

                            if (empty($error)) {
                                $dak_nagorik->receive_date = date("Y-m-d H:i:s");
                                $dak_nagorik = $dak_nagorik_table->save($dak_nagorik);

                                $table_instance_da = TableRegistry::get('DakAttachments');
                                if (!empty($this->request->data['attachment'])) {

                                    $attachmentArray = explode(',', $this->request->data['attachment']);

                                    if (!empty($attachmentArray)) {
                                        foreach ($attachmentArray as $k => $originalpath) {
                                            if (!empty($originalpath)) {

                                                $name = explode('/', $originalpath);

                                                $path = 'Dak';
                                                $office = $employee_office['office_id'];

                                                if (!is_dir(FILE_FOLDER_DIR . $path . '/')) {
                                                    mkdir(FILE_FOLDER_DIR . $path . '/');
                                                    chmod(FILE_FOLDER_DIR . $path . '/',
                                                        0777);
                                                }

                                                //office
                                                if (!is_dir(FILE_FOLDER_DIR . $path . '/' . $office . '/')) {
                                                    mkdir(FILE_FOLDER_DIR . $path . '/' . $office . '/');
                                                    chmod(FILE_FOLDER_DIR . $path . '/' . $office . '/',
                                                        0777);
                                                }

                                                //year
                                                if (!is_dir(FILE_FOLDER_DIR . $path . '/' . $office . '/' . date('Y') . '/')) {
                                                    mkdir(FILE_FOLDER_DIR . $path . '/' . $office . '/' . date('Y') . '/');
                                                    chmod(FILE_FOLDER_DIR . $path . '/' . $office . '/' . date('Y') . '/',
                                                        0777);
                                                }

                                                //month
                                                if (!is_dir(FILE_FOLDER_DIR . $path . '/' . $office . '/' . date('Y') . '/' . date('m') . '/')) {
                                                    mkdir(FILE_FOLDER_DIR . $path . '/' . $office . '/' . date('Y') . '/' . date('m') . '/');
                                                    chmod(FILE_FOLDER_DIR . $path . '/' . $office . '/' . date('Y') . '/' . date('m') . '/',
                                                        0777);
                                                }

                                                //day
                                                if (!is_dir(FILE_FOLDER_DIR . $path . '/' . $office . '/' . date('Y') . '/' . date('m') . '/' . date('d') . '/')) {
                                                    mkdir(FILE_FOLDER_DIR . $path . '/' . $office . '/' . date('Y') . '/' . date('m') . '/' . date('d') . '/');
                                                    chmod(FILE_FOLDER_DIR . $path . '/' . $office . '/' . date('Y') . '/' . date('m') . '/' . date('d') . '/',
                                                        0777);
                                                }

                                                $file = $path . DS . $office . DS . date('Y') . DS . date('m') . DS . date('d') . DS . $name[count($name)
                                                    - 1];


                                                //File to save the contents to
                                                $fp = fopen(FILE_FOLDER_DIR . $file,
                                                    'w');

                                                $url = $originalpath;

                                                //Here is the file we are downloading, replace spaces with %20
                                                $ch = curl_init(urldecode($url));

                                                curl_setopt($ch, CURLOPT_TIMEOUT, 60);

                                                //give curl the file pointer so that it can write to it
                                                curl_setopt($ch, CURLOPT_FILE, $fp);
                                                curl_setopt($ch,
                                                    CURLOPT_FOLLOWLOCATION, true);

                                                $data = curl_exec($ch); //get curl response
                                                //done
                                                curl_close($ch);

                                                if ($data) {
                                                    $attachment_data = array();
                                                    $attachment_data['dak_type'] = DAK_NAGORIK;
                                                    $attachment_data['dak_id'] = $dak_nagorik->id;
                                                    $attachment_data['attachment_description']
                                                        = '';

                                                    $attachment_data['attachment_type']
                                                        = $this->getMimeType($file);

                                                    $file_path_arr = explode(FILE_FOLDER,
                                                        $file);

                                                    $attachment_data['file_dir'] = FILE_FOLDER;
                                                    $attachment_data['file_name'] = isset($file_path_arr[1]) ? $file_path_arr[1] : (isset($file_path_arr[0]) ? $file_path_arr[0] : '');
                                                    $explode = explode('?token=', $attachment_data['file_name']);
                                                    $attachment_data['file_name'] = $explode[0];

                                                    $dak_attachment = $table_instance_da->newEntity();
                                                    $dak_attachment = $table_instance_da->patchEntity($dak_attachment,
                                                        $attachment_data);
                                                    $table_instance_da->save($dak_attachment);
                                                }
                                            }
                                        }
                                    }
                                }

                                if (!empty($dak_nagorik->description)) {
                                    $attachment_data = array();
                                    $attachment_data['dak_type'] = DAK_NAGORIK;
                                    $attachment_data['dak_id'] = $dak_nagorik->id;
                                    $attachment_data['is_main'] = 1;
                                    $attachment_data['attachment_description'] = !empty($this->request->data['file_description'])
                                        ? $this->request->data['file_description']
                                        : '';
                                    $attachment_data['attachment_type'] = "text";
                                    $file_path_arr = '';
                                    $attachment_data['file_dir'] = $this->request->webroot;
                                    $attachment_data['file_name'] = '';
                                    $attachment_data['content_body'] = $dak_nagorik->description;

                                    $dak_attachment = $table_instance_da->newEntity();
                                    $dak_attachment = $table_instance_da->patchEntity($dak_attachment,
                                        $attachment_data);
                                    $table_instance_da->save($dak_attachment);
                                }

                                if (($returnResponse = $this->addNagorikOnlineAbedorn($dak_nagorik,
                                        $employee_office)) === true) {
                                    $this->request->data['dak_id'] = $dak_nagorik->id;
                                    $this->request->data['dak_type'] = DAK_NAGORIK;
                                    if ($this->addDakTracks($this->request->data)
                                        == false) {
                                        $return = array("status" => 'error', 'msg' => "দুঃখিত! সবগুলো তথ্য সঠিক ভাবে দেয়া হয়নি",
                                            'error' => $returnResponse);

                                        $con->rollback();
                                    } else {

                                        $con->commit();

                                        $return = array("status" => 'success', 'data' => array(
                                            'dak_received_no' => $dak_nagorik->dak_received_no),
                                            'msg' => "আবেদন প্রেরণ করা হয়েছে। আপনার গ্রহণ নম্বর হচ্ছে " . $this->request->data['dak_received_no'] . "। ভবিষ্যতে অনুসন্ধানের জন্য রেখে দিন");

                                        if (!$this->NotificationSet('inbox',
                                            array(1, "Online Nagorik Dak"),
                                            1, $employee_office)) {

                                        }

                                        if (!empty($this->request->data['email'])) {
                                            $email_options['email_type'] = EMAIL_DAK;
                                            $email_options['subject'] = "অনলাইন নাগরিক আবেদন";
                                            $email_options['office_id'] = $employee_office['office_id'];
                                            $email_options['office_name'] = $employee_office['office_name'];
                                            $email_message = "প্রিয় " . $this->request->data['name_bng'] . ",<br/><br/>আপনার আবেদন " . $employee_office['office_name'] . " এর নিকট পাঠানো হয়েছে। আপনার আবেদন নম্বর হলো " . enTobn(!empty($dak_nagorik->forms_track_no) ? $dak_nagorik->forms_track_no : $dak_nagorik->dak_received_no) . "<br/>ভবিষ্যৎ অনুসন্ধানের জন্য আবেদন নম্বররটি সংরক্ষণ করুন।<br/>ধন্যবাদ।";
                                            $this->SendEmailMessage($this->request->data['email'], $email_message, $email_options);
                                        }
                                        if (!empty($this->request->data['mobile_no'])) {
                                            $this->SendSmsMessage($this->request->data['mobile_no'],
                                                "Dear " . $this->request->data['name_eng'] . ",\nYour request is sent. Your receipt no is " . bnToen(!empty($dak_nagorik->forms_track_no) ? $dak_nagorik->forms_track_no : $dak_nagorik->dak_received_no) . "\nThank you.");
                                        }
                                    }
                                } else {
                                    $return = array("status" => 'error', 'msg' => "দুঃখিত! সবগুলো তথ্য সঠিক ভাবে দেয়া হয়নি",
                                        'error' => $returnResponse);

                                    $con->rollback();
                                }
                            } else {
                                $return = array("status" => 'error', 'msg' => "দুঃখিত! সবগুলো তথ্য সঠিক ভাবে দেয়া হয়নি",
                                    'error' => $error);

                                $con->rollback();
                            }
                        }
                    } catch (\Exception $exc) {
                        $return = array("status" => 'error', 'msg' => "দুঃখিত! সবগুলো তথ্য সঠিক ভাবে দেয়া হয়নি",
                            'error' => $exc->getMessage());

                        $con->rollback();
                    } catch (Exception $ex) {
                        $return = array("status" => 'error', 'msg' => "দুঃখিত! সবগুলো তথ্য সঠিক ভাবে দেয়া হয়নি",
                            'error' => $ex);

                        $con->rollback();
                    }
                } else {

                    $return = array("status" => 'error', 'msg' => 'দুঃখিত! অফিস বাছাই করা হয়নি');

                }
            } else {
                $return = array("status" => 'error', 'msg' => 'Invalid request');
            }
        }

        $this->response->body(json_encode($return));
        $this->response->type('application/json');
        return $this->response;
    }


    public function apiApplication()
    {
        $this->layout = "ajax";

        $office_front_desk_table = TableRegistry::get('OfficeFrontDesk');
        $employeOfficeTable = TableRegistry::get('EmployeeOffices');
        $officeUnitsTable = TableRegistry::get('OfficeUnits');
        $employeeRecordsTable = TableRegistry::get('EmployeeRecords');
        $officeTable = TableRegistry::get('Offices');

        $return = array("status" => 'error', 'message' => 'Invalid request');

        if ($this->request->is('post')) {

            $token = !empty($this->request->data['token']) ? $this->request->data['token'] : null;
            if (empty($token)) {
                $return = array("status" => 'error', 'message' => "দুঃখিত! টোকেন দেওয়া হয়নি");
                goto responseBack;
            }
            if ($this->checkAuthorization($token)) {
                $con = ConnectionManager::get('default');
                try {

                    if($this->request->data['api_client'] == 'BTRC'){
                        $this->request->data['data'] = BTRC_TEMPLATE;
                    }

                    $dataDescription = !empty($this->request->data['data']) ? $this->request->data['data'] : null;
                    if (empty($dataDescription)) {
                        throw new \Exception(__("Invalid request! No Data is found"));
                    }

                    $clientId = !empty($this->request->data['api_client']) ? $this->request->data['api_client'] : null;
                    if (empty($clientId)) {
                        throw new \Exception(__("Invalid request! No Client Information is found"));
                    }

                    $company_name = !empty($this->request->data['company_name']) ? $this->request->data['company_name'] : null;
                    if (empty($company_name) && $this->request->data['api_client'] == 'BTRC') {
                        throw new \Exception(__("Invalid request! No Company Information is found"));
                    }

                    $officeId = intval($this->request->data['office_id']);
                    if (empty($officeId)) {
                        throw new \Exception(__("Invalid request! No Office found"));
                    }

                    $this->switchOffice($officeId, 'receiverOffice');

                    TableRegistry::remove('DakNagoriks');
                    TableRegistry::remove('DakDaptoriks');

                    $dak_nagorik_table = TableRegistry::get('DakNagoriks');

                    $con->begin();

                    $dak_nagorik = $dak_nagorik_table->newEntity();

                    $DakDaptoriksTable = TableRegistry::get('DakDaptoriks');

                    $dak_countdp = $DakDaptoriksTable->find()->where(['receiving_office_id' => $officeId])->order(['cast(docketing_no as signed)' => 'desc'])->first();
                    $dak_countng = $dak_nagorik_table->find()->where(['receiving_office_id' => $officeId])->order(['cast(docketing_no as signed)' => 'desc'])->first();
                    $dak_count = max([(!empty($dak_countdp['docketing_no']) ? $dak_countdp['docketing_no'] : 0), (!empty($dak_countng['docketing_no']) ? $dak_countng['docketing_no'] : 0)]) + 1;

                    $this->request->data['sender_name'] = !empty($this->request->data['applicant_name']) ? h($this->request->data['applicant_name']) : '';
                    $this->request->data['name_bng'] = $this->request->data['sender_name'];
                    $this->request->data['dak_type_id'] = 2;
                    $this->request->data['docketing_no'] = $dak_count;
                    $this->request->data['application_origin'] = !empty($this->request->data['api_client']) ? $this->request->data['api_client'] : 'nothi';
                    $this->request->data['dak_subject'] = !empty($this->request->data['application_subject']) ? $this->request->data['application_subject'] : ' ';
                    $dak_nagorik->feedback_type = isset($this->request->data['feedback_type']) ? $this->request->data['feedback_type'] : 3;

                    $frontdeskOrganogram = !empty($this->request->data['fdesk']) ? intval($this->request->data['fdesk']) : 0;

                    if (empty($this->request->data['dak_subject'])) {
                        throw new \Exception(__("Invalid request! Dak Subject can not be empty"));
                    }

                    $metadata = $this->request->data;
                    $employee_office = [];
                    $officeInfo = $officeTable->get($officeId);
                    if (!empty($frontdeskOrganogram)) {
                        $employee_office_data = $employeOfficeTable->find()->where(['office_id' => $officeId,
                            'office_unit_organogram_id' => $frontdeskOrganogram, 'status' => 1])->first();

                        if (!empty($employee_office_data)) {
                            $unitdetails = $officeUnitsTable->get($employee_office_data['office_unit_id']);
                            $employeeInfo = $employeeRecordsTable->get($employee_office_data['employee_record_id']);
                            $employee_office = [
                                'officer_id' => $employee_office_data['employee_record_id'],
                                'office_id' => $employee_office_data['office_id'],
                                'office_unit_id' => $employee_office_data['office_unit_id'],
                                'office_unit_organogram_id' => $employee_office_data['office_unit_organogram_id'],

                                'office_name' => $officeInfo['office_name_bng'],
                                'officer_name' => $employeeInfo['name_bng'],
                                'office_unit_name' => $unitdetails['unit_name_bng'],
                                'designation_label' => $employee_office_data['designation'] . (!empty($employee_office_data['incharge_label']) ? (' (' . $employee_office_data['incharge_label'] . ')') : ''),
                            ];
                        }
                    } else {
                        $employee_office = $office_front_desk_table->find()->where(['office_id' => $officeId])->first();
                    }

                    if (empty($employee_office)) {
                        throw new \Exception("দুঃখিত! অফিসের ফ্রন্ট ডেস্ক এর তথ্য পাওয়া যায়নি");
                    } else {

                        $this->request->data['uploader_designation_id'] = $employee_office['office_unit_organogram_id'];
                        $this->request->data['receiving_officer_id'] = $employee_office['officer_id'];
                        $this->request->data['receiving_officer_name'] = $employee_office['officer_name'];
                        $this->request->data['receiving_officer_designation_id'] = $employee_office['office_unit_organogram_id'];
                        $this->request->data['receiving_officer_designation_label'] = $employee_office['designation_label'];
                        $this->request->data['receiving_office_id'] = $employee_office['office_id'];
                        $this->request->data['receiving_office_unit_id'] = $employee_office['office_unit_id'];
                        $this->request->data['receiving_office_unit_name'] = $employee_office['office_unit_name'];

                        $this->request->data['dak_received_no'] = $this->generateDakReceivedNo($employee_office['office_id'],
                            1);
                        $this->request->data['forms_track_no'] = !empty($this->request->data['tracking_id']) ? $this->request->data['tracking_id'] : '';
                        $this->request->data['receive_date'] = date("Y-m-d H:i:s");

                        $this->request->data['dak_status'] = DAK_CATEGORY_SENT;

                        $this->request->data['description'] = $this->generateHtmlBodyData((!empty($this->request->data['data']) ? $this->request->data['data'] : ''), $metadata);
                        if (empty($this->request->data['description'])) {
                            throw new \Exception(__("Invalid request! Dak Description can not be empty"));
                        }

                        if(!empty($metadata['sdesk'])){
                            $sdeskOfficeEmployeeData = $employeOfficeTable->find()->where(['office_id' => $officeId, 'office_unit_organogram_id' => $metadata['sdesk'], 'status' => 1])->first();
                            if(!empty($sdeskOfficeEmployeeData)) {
                                $sdeskUnitData = $officeUnitsTable->get($sdeskOfficeEmployeeData['office_unit_id']);
                                $sdeskEmployeeData = $employeeRecordsTable->get($sdeskOfficeEmployeeData['employee_record_id']);
                                $this->request->data['description'] = str_replace('{{sdesk_designation}}', $sdeskOfficeEmployeeData['designation'], $this->request->data['description']);
                                $this->request->data['description'] = str_replace('{{sdesk_office}}', $officeInfo['office_name_bng'], $this->request->data['description']);

                                if (!empty($sdeskEmployeeData)) {
                                    $this->request->data['description'] = str_replace('{{sdesk_name}}', $sdeskEmployeeData['name_bng'], $this->request->data['description']);
                                    $this->request->data['description'] = str_replace('{{sdesk_email}}', $sdeskEmployeeData['personal_email'], $this->request->data['description']);
                                } else {
                                    $this->request->data['description'] = str_replace('{{sdesk_name}}', '', $this->request->data['description']);
                                    $this->request->data['description'] = str_replace('{{sdesk_email}}', '', $this->request->data['description']);
                                }

                                if (!empty($sdeskUnitData)) {
                                    $this->request->data['description'] = str_replace('{{sdesk_unit}}', $sdeskUnitData['unit_name_bng'], $this->request->data['description']);
                                } else {
                                    $this->request->data['description'] = str_replace('{{sdesk_unit}}', '', $this->request->data['description']);
                                }
                            }else{
                                throw new \Exception("দুঃখিত! অনুমোদনকারীর ডেস্ক এর তথ্য পাওয়া যায়নি");
                            }
                        }
                        $dak_nagorik = $dak_nagorik_table->patchEntity($dak_nagorik, $this->request->data, ['validate' => 'add']);

                        $error = $dak_nagorik->errors();

                        if (empty($error)) {
                            $dak_nagorik->receive_date = date("Y-m-d H:i:s");
                            if (!empty($metadata['data'])) {
                                unset($metadata['data']);
                            }
                            if (!empty($metadata['token'])) {
                                unset($metadata['token']);
                            }
                            if (!empty($metadata['description'])) {
                                unset($metadata['description']);
                            }

                            $dak_nagorik->application_meta_data = json_encode($metadata);
                            $dak_nagorik = $dak_nagorik_table->save($dak_nagorik);

                            $table_instance_da = TableRegistry::get('DakAttachments');
                            if (!empty($this->request->data['attachments'])) {

                                $attachmentArray = is_array($this->request->data['attachments']) ? $this->request->data['attachments'] : json_decode($this->request->data['attachments'], true);

                                if (!empty($attachmentArray)) {
                                    foreach ($attachmentArray as $k => $originalpath) {
                                        if (!empty($originalpath)) {

                                            $name = explode('/', $originalpath['url']);

                                            $path = 'Dak';
                                            $office = $employee_office['office_id'];

                                            if (!is_dir(FILE_FOLDER_DIR . $path . '/')) {
                                                mkdir(FILE_FOLDER_DIR . $path . '/');
                                                chmod(FILE_FOLDER_DIR . $path . '/',
                                                    0777);
                                            }

                                            //office
                                            if (!is_dir(FILE_FOLDER_DIR . $path . '/' . $office . '/')) {
                                                mkdir(FILE_FOLDER_DIR . $path . '/' . $office . '/');
                                                chmod(FILE_FOLDER_DIR . $path . '/' . $office . '/',
                                                    0777);
                                            }

                                            //year
                                            if (!is_dir(FILE_FOLDER_DIR . $path . '/' . $office . '/' . date('Y') . '/')) {
                                                mkdir(FILE_FOLDER_DIR . $path . '/' . $office . '/' . date('Y') . '/');
                                                chmod(FILE_FOLDER_DIR . $path . '/' . $office . '/' . date('Y') . '/',
                                                    0777);
                                            }

                                            //month
                                            if (!is_dir(FILE_FOLDER_DIR . $path . '/' . $office . '/' . date('Y') . '/' . date('m') . '/')) {
                                                mkdir(FILE_FOLDER_DIR . $path . '/' . $office . '/' . date('Y') . '/' . date('m') . '/');
                                                chmod(FILE_FOLDER_DIR . $path . '/' . $office . '/' . date('Y') . '/' . date('m') . '/',
                                                    0777);
                                            }

                                            //day
                                            if (!is_dir(FILE_FOLDER_DIR . $path . '/' . $office . '/' . date('Y') . '/' . date('m') . '/' . date('d') . '/')) {
                                                mkdir(FILE_FOLDER_DIR . $path . '/' . $office . '/' . date('Y') . '/' . date('m') . '/' . date('d') . '/');
                                                chmod(FILE_FOLDER_DIR . $path . '/' . $office . '/' . date('Y') . '/' . date('m') . '/' . date('d') . '/',
                                                    0777);
                                            }

                                            $file = $path . DS . $office . DS . date('Y') . DS . date('m') . DS . date('d') . DS . $name[count($name)
                                                - 1];


                                            //File to save the contents to
                                            $fp = fopen(FILE_FOLDER_DIR . $file,
                                                'w');

                                            $url = $originalpath['url'];

                                            //Here is the file we are downloading, replace spaces with %20
                                            $ch = curl_init(urldecode($url));

                                            curl_setopt($ch, CURLOPT_TIMEOUT, 60);

                                            //give curl the file pointer so that it can write to it
                                            curl_setopt($ch, CURLOPT_FILE, $fp);
                                            curl_setopt($ch,
                                                CURLOPT_FOLLOWLOCATION, true);

                                            $data = curl_exec($ch); //get curl response
                                            //done
                                            curl_close($ch);

                                            if ($data) {
                                                $attachment_data = array();
                                                $attachment_data['dak_type'] = DAK_NAGORIK;
                                                $attachment_data['dak_id'] = $dak_nagorik->id;
                                                $attachment_data['attachment_description']
                                                    = '';

                                                $attachment_data['attachment_type']
                                                    = $this->getMimeType($file);

                                                $file_path_arr = explode(FILE_FOLDER,
                                                    $file);

                                                $attachment_data['file_dir'] = FILE_FOLDER;
                                                $attachment_data['file_name'] = isset($file_path_arr[1]) ? $file_path_arr[1] : (isset($file_path_arr[0]) ? $file_path_arr[0] : '');
                                                $explode = explode('?token=', $attachment_data['file_name']);
                                                $attachment_data['file_name'] = $explode[0];
                                                $attachment_data['user_file_name'] = !empty($originalpath['name']) ? $originalpath['name'] : $attachment_data['file_name'];

                                                $dak_attachment = $table_instance_da->newEntity();
                                                $dak_attachment = $table_instance_da->patchEntity($dak_attachment,
                                                    $attachment_data);
                                                $table_instance_da->save($dak_attachment);
                                            }
                                        }
                                    }
                                }
                            }

                            if (!empty($dak_nagorik->description)) {
                                $attachment_data = array();
                                $attachment_data['dak_type'] = DAK_NAGORIK;
                                $attachment_data['dak_id'] = $dak_nagorik->id;
                                $attachment_data['is_main'] = 1;
                                $attachment_data['attachment_description'] = !empty($this->request->data['file_description'])
                                    ? $this->request->data['file_description']
                                    : '';
                                $attachment_data['attachment_type'] = "text";
                                $attachment_data['file_dir'] = $this->request->webroot;
                                $attachment_data['file_name'] = '';
                                $attachment_data['content_body'] = $dak_nagorik->description;

                                $dak_attachment = $table_instance_da->newEntity();
                                $dak_attachment = $table_instance_da->patchEntity($dak_attachment,
                                    $attachment_data);
                                $table_instance_da->save($dak_attachment);
                            }

                            if (($returnResponse = $this->addNagorikOnlineAbedorn($dak_nagorik,
                                    $employee_office)) === true) {
                                $this->request->data['dak_id'] = $dak_nagorik->id;
                                $this->request->data['dak_type'] = DAK_NAGORIK;

                                $this->request->data['service_name'] = $clientId;
                                if ($this->addDakTracks($this->request->data, $metadata)
                                    == false) {
                                    throw new \Exception("দুঃখিত! সবগুলো তথ্য সঠিক ভাবে দেয়া হয়নি");
                                } else {
                                    $return = array("status" => 'success', 'data' =>
                                        [
                                            'dak_received_no' => $dak_nagorik->dak_received_no,
                                            'dak_id' => $dak_nagorik->id,
                                            'current_desk_id' => $dak_nagorik->receiving_officer_designation_id,
                                            'nothi_id' => !empty($metadata['nothi_id']) ? $metadata['nothi_id'] : 0,
                                            'note_id' => !empty($metadata['note_id']) ? $metadata['note_id'] : 0,
                                            'potro_id' => !empty($metadata['potro_id']) ? $metadata['potro_id'] : 0,
                                            'requested_data' => $metadata,
                                        ],
                                        'message' => "আবেদন গ্রহণ করা হয়েছে। আপনার গ্রহণ নম্বর হচ্ছে " . $this->request->data['dak_received_no'] . "। ভবিষ্যতে অনুসন্ধানের জন্য রেখে দিন");
                                    // need to save dak_id in metadata
                                    $metadata['dak_id'] = $dak_nagorik->id;
                                    $dak_nagorik_table->updateAll(['application_meta_data' => json_encode($metadata)],['id' => $dak_nagorik->id]);
                                    $con->commit();
                                    if (!$this->NotificationSet('inbox', array(1, "Online Nagorik Dak"),
                                        1, $employee_office)) {
                                    }

                                    if (!empty($this->request->data['email'])) {
                                        $email_options['email_type'] = EMAIL_DAK;
                                        $email_options['subject'] = "অনলাইন আবেদন";
                                        $email_options['office_id'] = $employee_office['office_id'];
                                        $email_options['office_name'] = $employee_office['office_name'];
                                        $email_message = "প্রিয় " . $this->request->data['name_bng'] . ",<br/><br/>আপনার আবেদন " . $employee_office['office_name'] . " এর নিকট পাঠানো হয়েছে। আপনার আবেদন নম্বর হলো " . enTobn(!empty($dak_nagorik->forms_track_no) ? $dak_nagorik->forms_track_no : $dak_nagorik->dak_received_no) . "<br/>ভবিষ্যৎ অনুসন্ধানের জন্য আবেদন নম্বররটি সংরক্ষণ করুন।<br/>ধন্যবাদ।";
                                        $this->SendEmailMessage($this->request->data['email'], $email_message, $email_options);
                                    }
                                    if (!empty($this->request->data['mobile_no'])) {
                                        $this->SendSmsMessage($this->request->data['mobile_no'],
                                            "Your request is sent. Your receipt no is " . bnToen(!empty($dak_nagorik->forms_track_no) ? $dak_nagorik->forms_track_no : $dak_nagorik->dak_received_no) . "\nThank you.");
                                    }
                                }
                            } else {
                                throw new \Exception("দুঃখিত! সবগুলো তথ্য সঠিক ভাবে দেয়া হয়নি");
                            }
                        } else {
                            throw new \Exception("দুঃখিত! সবগুলো তথ্য সঠিক ভাবে দেয়া হয়নি");
                        }
                    }
                } catch (\Exception $exception) {
                    $con->rollback();
                    $return = array("status" => 'error', 'message' => $exception->getMessage());
                } catch (Exception $ex) {
                    $return = array("status" => 'error', 'message' => "দুঃখিত! সবগুলো তথ্য সঠিক ভাবে দেয়া হয়নি",
                        'error' => $this->makeEncryptedData($ex));

                    $con->rollback();
                }
            } else {
                $return = array("status" => 'error', 'message' => 'দুঃখিত! আপনার অনুরোধ অনুনোমোদিত।');

            }
        }

        responseBack:
        $this->response->body(json_encode($return));
        $this->response->type('application/json');
        return $this->response;
    }

    /* API of this Controller */

    protected function generateHtmlBodyData($html, $meta)
    {

        $replaceArr = [
            'noc_reference_no',
            'model_count',
            'total_model_count',
            'license_no',
            'approval_date',
            'validation_date',
            'tbl_imei_xl',
        ];

        foreach ($replaceArr as $key => $value) {
            if ($value == 'validation_date') {
                if (!empty($meta['approval_date'])) {

                    $current = strtotime($meta['approval_date']);
                    $meta['validation_date'] = date("Y-m-d", strtotime("+6 month", strtotime(date("Y-m-d", $current))));;
                }
            }
            if (!empty($meta[$value])) {
                $html = str_replace('{{' . $value . '}}', $meta[$value], $html);
            }
        }

        $head = !empty($meta['tbl_imei_head']) ?($meta['tbl_imei_head']) : [];

        if (!empty($head) && !empty($meta['tbl_imei_data'])) {

            $tableBody = '';
            $body = $meta['tbl_imei_data'];
            if (!empty($body)) {
                foreach ($body as $key => $value) {
                    $jsondata = json_encode($value);
                    if (is_array($value)) {
                        $tableBody .= "<tr data-json='".($jsondata)."'>";
                        foreach ($head as $index => $headValue) {
                            $headKey = key($headValue);
                            if($headValue[$headKey]){
                                $tableBody .= "<td><span data-type='text' id='{$headKey}__{$value['id']}__{$key}' editable='true' class='canedit' >" . (!empty($value[$headKey]) ? $value[$headKey] : '-') . "</span></td>";
                            }
                            else{
                                $tableBody .= "<td>" . (!empty($value[$headKey]) ? $value[$headKey] : '') . "</td>";
                            }
                        }
                        $tableBody .= "</tr>";
                    }
                }
            }

            $html = str_replace('{{imeidata}}', $tableBody, $html);
        }

        return $html;
    }
}