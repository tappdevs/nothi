<?php

namespace App\Controller;

use App\Controller\ProjapotiController;
use App\Model\Table;
use App\Model\Table\EmployeeRecordsTable;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\Entity;
use App\Model\Entity\EmployeeRecords;
use Cake\ORM\TableRegistry;

class DakOnlineNagoriksController extends ProjapotiController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['nagorik_dak', 'checkBirthCertificate', 'upload']);
    }

    public function index()
    {
        $dak_nagoriks = TableRegistry::get('DakOnlineNagoriks');
        $query        = $dak_nagoriks->find('all');
        $this->set(compact('query'));
    }

    public function checkBirthCertificate()
    {
        $this->response->body(json_encode(1));
        $this->response->type('application/json');
        return $this->response;
    }

    public function nagorik_dak()
    {

        $table_dak_online_nagoriks = TableRegistry::get('DakOnlineNagoriks');
        $dak_online_nagoriks       = $table_dak_online_nagoriks->newEntity();

        if ($this->request->is('post')) {

            $dak_online_nagoriks = $table_dak_online_nagoriks->patchEntity($dak_online_nagoriks,
                $this->request->data);
            if ($online_dak          = $table_dak_online_nagoriks->save($dak_online_nagoriks)) {

                /* Save Dak Attachment */
                $table_instance_da                 = TableRegistry::get('DakOnlineAttachments');
                $this->request->data['attachment'] = explode(',',
                    $this->request->data['uploaded_attachments']);
                foreach ($this->request->data['attachment'] as $file) {
                    if (!empty($file)) {
                        $attachment_data                           = array();
                        $attachment_data['dak_type']               = $this->request->data['dak_type'];
                        $attachment_data['dak_id']                 = $online_dak->id;
                        $attachment_data['attachment_description'] = $this->request->data['file_description'];

                        $attachment_data['attachment_type'] = $this->getMimeType($file);

                        $file_path_arr                = explode($this->request->webroot,
                            $file);
                        $attachment_data['file_dir']  = $this->request->webroot;
                        $attachment_data['file_name'] = $file;

                        $dak_attachment = $table_instance_da->newEntity();
                        $dak_attachment = $table_instance_da->patchEntity($dak_attachment,
                            $attachment_data);
                        $table_instance_da->save($dak_attachment);
                    }
                }
                /* End Save Dak Attachment */

                $this->Flash->success(__('????? ??? ?????? ??? ?????? '));
                return $this->redirect(['action' => 'nagorik_dak']);
            }
        }
    }

    public function upload()
    {
        $this->layout = 'ajax';
        if (!empty($this->request->data['file'])) {
            $filename    = $this->request->data['file']['name'];
            $destination = WWW_ROOT.'/files/'.strtolower($filename);
            if ($this->request->data['file']['error'] == 1) {
                echo "File is too large!";
                return;
            }
            if (move_uploaded_file($this->request->data['file']['tmp_name'],
                    $destination)) {
                echo 1;
            } else echo "Can't upload file!";
        } else echo "Something went wrong!";
        die;
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $table_dak_nagoriks = TableRegistry::get('DakOnlineNagoriks');
        $dak_daptoriks      = $table_dak_nagoriks->get($id);
        if ($table_dak_nagoriks->delete($dak_daptoriks)) {
            return $this->redirect(['action' => 'index']);
        }
    }

    public function view($id = null)
    {
        $table_dak_nagoriks = TableRegistry::get('DakOnlineNagoriks');
        $this->loadModel('DakOnlineNagoriks');
        $dak_nagoriks       = $table_dak_nagoriks->find()->where(['id' => $id])->contain(['DakOnlineAttachments'])->toArray();
        $this->set(compact('dak_nagoriks'));
    }

    public function approve($id = null)
    {

        $current_office   = $session->read('selected_office_section');
        $logged_user_info = $session->read('logged_employee_record');

        $table_dak_online_nagoriks     = TableRegistry::get('DakOnlineNagoriks');
        $dak_online_nagoriks           = $table_dak_online_nagoriks->get($id);
        $dak_online_nagoriks->approval = 1;
        $table_dak_online_nagoriks->save($dak_online_nagoriks);

        unset($dak_online_nagoriks->approval);

        /* Transfer Intermedia Dak into Nagorik Dak Table */
        $dak_online_nagorik_data                              = array();
        $dak_online_nagorik_data['receive_date']              = $dak_online_nagoriks->receive_date;
        $dak_online_nagorik_data['org_organization_id']       = $dak_online_nagoriks->office_id;
        $dak_online_nagorik_data['dak_type_id']               = $dak_online_nagoriks->dak_type_id;
        $dak_online_nagorik_data['name_eng']                  = $dak_online_nagoriks->name_eng;
        $dak_online_nagorik_data['name_bng']                  = $dak_online_nagoriks->name_bng;
        $dak_online_nagorik_data['national_idendity_no']      = $dak_online_nagoriks->national_idendity_no;
        $dak_online_nagorik_data['passport']                  = $dak_online_nagoriks->passport;
        $dak_online_nagorik_data['birth_registration_number'] = $dak_online_nagoriks->birth_registration_number;
        $dak_online_nagorik_data['father_name']               = $dak_online_nagoriks->father_name;
        $dak_online_nagorik_data['mother_name']               = $dak_online_nagoriks->mother_name;
        $dak_online_nagorik_data['present_address']           = $dak_online_nagoriks->present_address;
        $dak_online_nagorik_data['parmanent_address']         = $dak_online_nagoriks->parmanent_address;
        $dak_online_nagorik_data['email']                     = $dak_online_nagoriks->email;
        $dak_online_nagorik_data['phone_no']                  = $dak_online_nagoriks->phone_no;
        $dak_online_nagorik_data['mobile_no']                 = $dak_online_nagoriks->mobile_no;
        $dak_online_nagorik_data['subject']                   = $dak_online_nagoriks->subject;
        $dak_online_nagorik_data['status']                    = $dak_online_nagoriks->status;
        $dak_online_nagorik_data['created_by']                = $dak_online_nagoriks->created_by;
        $dak_online_nagorik_data['modified_by']               = $dak_online_nagoriks->modified_by;


        $dak_online_nagorik_data['receiving_office_id']                 = $current_office["office_id"];
        $dak_online_nagorik_data['receiving_officer_id']                = $current_office["employee_record_id"];
        $dak_online_nagorik_data['receiving_officer_name']              = $logged_user_info["personal_info"]["name_bng"];
        $dak_online_nagorik_data['receiving_officer_designation_label'] = $logged_user_info["personal_info"]["current_office_designation_id"];


        $table_dak_nagoriks = TableRegistry::get('DakNagoriks');
        $dak_nagorik        = $table_dak_nagoriks->newEntity();
        $dak_nagorik        = $table_dak_nagoriks->patchEntity($dak_nagorik,
            $dak_online_nagorik_data);
        $table_dak_nagoriks->save($dak_nagorik);
        /* End Transfer Intermedia Dak into Nagorik Dak Table */


        /* Transfer Intermediate Dak Attachment */
        $table_instance_doa    = TableRegistry::get('DakOnlineAttachments');
        $table_instance_da     = TableRegistry::get('DakAttachments');
        $dak_online_attachment = $table_instance_doa->find('all')->where(['dak_id' => $id])->toArray();

        foreach ($dak_online_attachment as $file) {
            if (!empty($file['file_name'])) {
                $attachment_data                           = array();
                $attachment_data['dak_type']               = $file['dak_type'];
                $attachment_data['dak_id']                 = $file['dak_id'];
                $attachment_data['attachment_description'] = $file['file_description'];
                $attachment_data['file_dir']               = '';
                $attachment_data['attachment_type']        = $file['file_name_file']['type'];
                $attachment_data['file_name_file']         = $file['file_name_file'];

                // required for file uploader: file name of upload file
                $attachment_data['filename_prefix'] = $attachment_data['dak_type']."/".$attachment_data['dak_id']."/".time()."_".$attachment_data['file_name_file']['name'];

                $dak_attachment = $table_instance_da->newEntity();
                $dak_attachment = $table_instance_da->patchEntity($dak_attachment,
                    $attachment_data);
                $table_instance_da->save($dak_attachment);
            }
        }
        /* End Transfer Intermediate Dak Attachment */

        return $this->redirect(['action' => 'index']);
    }
}