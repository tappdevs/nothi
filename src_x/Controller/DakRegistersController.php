<?php

namespace App\Controller;

use App\Controller\ProjapotiController;
use App\Model\Table;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Psr\Log\InvalidArgumentException;
use Cake\I18n\I18n;
use Cake\I18n\Time;
use Cake\I18n\Number;

class DakRegistersController extends ProjapotiController {

    public function initialize() {
        parent::initialize();
        $this->loadComponent('Paginator');
    }

    /**
     * Sent Dak report
     */
    public function dakGrohonRegisterExcel($type='pdf',$startDate='',$endDate='',$dateRangeBn='', $unit_id = ''){
        $result = $this->dakGrohonRegisterContent($startDate, $endDate, $type, 20, $unit_id);

        $unitInformation='';
        $DakList = Array();
        $j=0;

        if (!empty($result['data'])) {

            foreach ($result['data'] as $value) {
                if( $unitInformation ==  $value['to_office_unit_name']){
                    $DakList[$j]['to_office_unit_name'] ='';
                }
                else {
                    $DakList[$j]['to_office_unit_name'] = $value['to_office_unit_name'];
                    $unitInformation = $value['to_office_unit_name'];
                }
                $DakList[$j]['dak_received_no'] = $value['dak_type'] ==DAK_DAPTORIK ? (enTobn($value['DakDaptoriks']['dak_received_no'])) : (enTobn($value['DakNagoriks']['dak_received_no']));
                $DakList[$j]['docketing_no'] = $value['dak_type'] == DAK_DAPTORIK ? enTobn($value['DakDaptoriks']['docketing_no']) : enTobn($value['DakNagoriks']['docketing_no']);
                $DakList[$j]['sender_sarok_no'] = $value['dak_type'] ==DAK_DAPTORIK ? $value['DakDaptoriks']['sender_sarok_no'] : '';

                $time = new Time($value['dak_type'] == DAK_DAPTORIK ? $value['daptorikCreated'] : $value['nagorikCreated']);
                $DakList[$j]['application_date'] = $time->i18nFormat(null, null, 'bn-BD');
                $DakList[$j]['dak_type'] = $value['dak_type'] == DAK_DAPTORIK ? 'দাপ্তরিক' : 'নাগরিক';
                $DakList[$j]['dak_subject'] = $value['dak_type'] == DAK_DAPTORIK ? $value['DakDaptoriks']['dak_subject'] : $value['DakNagoriks']['dak_subject'];
                $DakList[$j]['applicant_details'] = $value['dak_type'] == DAK_DAPTORIK ? ($value['from_officer_designation_label'] . ', ' . $value['from_office_unit_name'] . ', ' . $value['from_office_name']) : ( $value['from_officer_name'] . (!empty($value['from_officer_designation_label']) ? (', ' . $value['from_officer_designation_label']) : '') . (!empty($value['from_office_name']) ? (', ' . $value['from_office_name']) : '') );
                $DakList[$j]['previous_sender'] = $value['dak_type'] == DAK_DAPTORIK ? ($value['DakDaptoriks']['sender_officer_designation_label'] . ', ' . $value['DakDaptoriks']['sender_office_unit_name'] . ', ' . $value['DakDaptoriks']['sender_office_name']) : ($value['DakNagoriks']['sender_name']);


                if (!empty($value['mainPrapok'])) {
                    $DakList[$j]['mainPrapok']= $value['mainPrapok'][0]['to_officer_designation_label'] . ', ' . $value['mainPrapok'][0]['to_office_unit_name'] . ', ' . $value['mainPrapok'][0]['to_office_name'] ;

                } else { $DakList[$j]['mainPrapok']='';}

                $receiving_date = new Time($value['created']);
                $DakList[$j]['receiving_date']= $receiving_date->i18nFormat(null, null, 'bn-BD');

                $secIndex = ($value['dak_type'] == DAK_DAPTORIK ? $value['DakDaptoriks']['dak_security_level'] : $value['DakNagoriks']['dak_security_level']);
                $DakList[$j]['security_level_list']= isset($result['security_level_list'][$secIndex]) ? $result['security_level_list'][$secIndex] : '';

                $DakList[$j]['dak_priority']= isset($result['priority_list'][$value['dak_priority']]) ? $result['priority_list'][$value['dak_priority']] : '';
                $DakList[$j]['dak_actions']= $value['dak_actions'];

                $j++;

            }
        }
        if(empty($DakList)){
            $DakList= Array();
        }
        if($type == 'excel') {
            $this->printExcel($DakList, [
                ['key' => 'to_office_unit_name', 'title' => 'শাখা'],
                ['key' => 'dak_received_no', 'title' => 'গ্রহণ নম্বর'],
                ['key' => 'docketing_no', 'title' => 'ডকেট নং'],
                ['key' => 'sender_sarok_no', 'title' => 'স্মারক নম্বর'],
                ['key' => 'application_date', 'title' => 'আবেদনের তারিখ'],
                ['key' => 'dak_type', 'title' => 'ধরন'],
                ['key' => 'dak_subject', 'title' => 'বিষয়'],
                ['key' => 'applicant_details', 'title' => 'আবেদনকারী'],
                ['key' => 'previous_sender', 'title' => 'পূর্ববর্তী প্রেরক'],
                ['key' => 'mainPrapok', 'title' => 'মূল প্রাপক'],
                ['key' => 'receiving_date', 'title' => 'প্রাপ্তির তারিখ'],
                ['key' => 'security_level_list', 'title' => 'গোপনীয়তা'],
                ['key' => 'dak_priority', 'title' => 'অগ্রাধিকার'],
                ['key' => 'dak_actions', 'title' => 'সর্বশেষ অবস্থা'],
            ], [
                'name' => ((isset($result['office_name'])) ? $result['office_name'] . ' অফিসের ' : '') . 'ডাক গ্রহণ নিবন্ধন বহি',
                'title' => [
                    ((isset($result['office_name'])) ? $result['office_name'] . ' অফিসের ' : '') . 'ডাক গ্রহণ নিবন্ধন বহি',
                    $dateRangeBn,
                ]
            ]);
        } else {
            $this->printPdf($DakList, [
                ['key' => 'to_office_unit_name', 'title' => 'শাখা'],
                ['key' => 'dak_received_no', 'title' => 'গ্রহণ নম্বর'],
                ['key' => 'docketing_no', 'title' => 'ডকেট নং'],
                ['key' => 'sender_sarok_no', 'title' => 'স্মারক নম্বর'],
                ['key' => 'application_date', 'title' => 'আবেদনের তারিখ'],
                ['key' => 'dak_type', 'title' => 'ধরন'],
                ['key' => 'dak_subject', 'title' => 'বিষয়'],
                ['key' => 'applicant_details', 'title' => 'আবেদনকারী'],
                ['key' => 'previous_sender', 'title' => 'পূর্ববর্তী প্রেরক'],
                ['key' => 'mainPrapok', 'title' => 'মূল প্রাপক'],
                ['key' => 'receiving_date', 'title' => 'প্রাপ্তির তারিখ'],
                ['key' => 'security_level_list', 'title' => 'গোপনীয়তা'],
                ['key' => 'dak_priority', 'title' => 'অগ্রাধিকার'],
                ['key' => 'dak_actions', 'title' => 'সর্বশেষ অবস্থা'],
            ], [
                'name' => ((isset($result['office_name'])) ? $result['office_name'] . ' অফিসের ' : '') . 'ডাক গ্রহণ নিবন্ধন বহি',
                'title' => [
                    ((isset($result['office_name'])) ? $result['office_name'] . ' অফিসের ' : '') . 'ডাক গ্রহণ নিবন্ধন বহি',
                    $dateRangeBn,
                ]
            ]);
        }
        exit;
    }

    public function dakGrohonRegister() {

        $session = $this->request->session();
        $employee = $session->read('logged_employee_record');
        $this->set("office", $employee);

        $units_list = $this->getOwnUnitList();
        $this->set("units_list", $units_list);
    }

//    public function dakGrohonRegisterContent($startDate = "", $endDate = "",$isExport="") {
//         set_time_limit(0);
//        $this->layout = null;
//
//        $reportTable = TableRegistry::get('Reports');
//        $office = TableRegistry::get('Offices');
//        $dakMovementsTable = TableRegistry::get('DakMovements');
//
//
//        $startDate = !empty($startDate) ? h($startDate) : date("Y-m-d");
//        $endDate = !empty($endDate) ? h($endDate) : date("Y-m-d");
//        $condition = ' 1 ';
//
//        if (!empty($startDate) And ! empty($endDate)) {
//            if (!empty($condition))
//                $condition .= ' AND ';
//            $condition .= " date(DakMovements.created)  BETWEEN  '{$startDate}' AND '{$endDate}' ";
//        } else {
//            $startDate = date('Y-m-d');
//            $endDate = date('Y-m-d');
//            if (!empty($condition))
//                $condition .= ' AND ';
//            $condition .= " date(DakMovements.created)  BETWEEN  '{$startDate}' AND '{$endDate}' ";
//        }
//
//        $selected_office_section = $this->getCurrentDakSection();
//        $this->set(compact('selected_office_section'));
//
//        $officeUnitTable = TableRegistry::get("OfficeUnits");
//
//        $units = $officeUnitTable->getOfficeChildInfo($selected_office_section['office_unit_id']);
//        $unitsOwn = $officeUnitTable->getOwnChildInfo($selected_office_section['office_unit_id']);
//
//        $units = !empty($units[0]) ? $units[0] : '';
//        $unitsOwn = !empty($unitsOwn[0]) ? ($selected_office_section['office_unit_id'] . ',' . $unitsOwn[0]) : $selected_office_section['office_unit_id'];
//
//        $units = $units . (!empty($unitsOwn) ? ((!empty($units) ? ',' : '') . $unitsOwn) : '');
////        $condition .= " AND DakMovements.to_office_id IN ({$units}) ";
//        $condition .= " AND DakMovements.to_office_unit_id IN ({$units}) ";
////        $condition .= " AND DakMovements.to_officer_designation_id IN ({$units}) ";
//        $condition .= " AND DakMovements.operation_type IN ('Forward','Sent') ";
//        $priorities = json_decode(DAK_PRIORITY_TYPE);
//        $priority_list = array();
//
//        foreach ($priorities as $key => $value) {
//            $priority_list[$key] = $value;
//        }
//
//        $security_level_list = array();
//        $security_levels = json_decode(DAK_SECRECY_TYPE);
//
//        foreach ($security_levels as $key => $value) {
//            $security_level_list[$key] = $value;
//        }
//
//
//        if($isExport=='excel' || $isExport=='pdf'){
//            $dak_list = $reportTable->getDakRegisters($condition)->toArray();
//        } else {
//            $dak_list = $this->Paginator->paginate($reportTable->getDakRegisters($condition), ['limit' => 20]);
//        }
//
//        $preorder = '';
//        $data = [];
//        $i = 0;
//        if (!empty($dak_list)) {
//            foreach ($dak_list as $dak) {
//                if (empty($preorder)) {
//                    $preorder = $dak['to_office_unit_id'];
//                }
//                $preorder = $preorder . ',' . $dak['to_office_unit_id'];
//            }
//            if (!empty($preorder)) {
//                $order_unit = $officeUnitTable->getOrderwiseUnits($preorder);
//                foreach ($order_unit as $key => $val) {
//                    foreach ($dak_list as $dak) {
//                        if ($dak['to_office_unit_id'] == $key) {
//                            $data[$i]['daptorikCreated'] = $dak['daptorikCreated'];
//                            $data[$i]['nagorikCreated'] = $dak['nagorikCreated'];
//                            $data[$i]['id'] = $dak['id'];
//                            $data[$i]['dak_type'] = $dak['dak_type'];
//
//                            $data[$i]['from_office_name'] = $dak['from_office_name'];
//                            $data[$i]['from_office_unit_name'] = $dak['from_office_unit_name'];
//                            $data[$i]['from_officer_name'] = $dak['from_officer_name'];
//                            $data[$i]['from_officer_designation_label'] = $dak['from_officer_designation_label'];
//
//                            $data[$i]['dak_id'] = $dak['dak_id'];
//
//                            $data[$i]['to_office_name'] = $dak['to_office_name'];
//                            $data[$i]['to_office_unit_id'] = $dak['to_office_unit_id'];
//                            $data[$i]['to_office_unit_name'] = $dak['to_office_unit_name'];
//                            $data[$i]['to_officer_name'] = $dak['to_officer_name'];
//                            $data[$i]['to_officer_designation_label'] = $dak['to_officer_designation_label'];
//
//                            $data[$i]['operation_type'] = $dak['operation_type'];
//                            $data[$i]['sequence'] = $dak['sequence'];
//                            $data[$i]['attention_type'] = $dak['attention_type'];
//                            $data[$i]['dak_actions'] = $dak['dak_actions'];
//                            $data[$i]['created'] = $dak['created'];
//                            $data[$i]['is_summary_nothi'] = $dak['is_summary_nothi'];
//                            $data[$i]['dak_priority'] = $dak['dak_priority'];
//
//                            $data[$i]['DakDaptoriks'] = $dak['DakDaptoriks'];
//                            $data[$i]['DakNagoriks'] = $dak['DakNagoriks'];
//                            $data[$i]['mainPrapok'] = $dakMovementsTable->getDakMovesBy_sequence_dakId($dak['sequence'], $dak['dak_id'], $dak['dak_type'], 1);
//
//
//                            $i++;
//                        }
//                    }
//                }
//            }
//        }
//        $office_name = $office->find()->select(['office_name_bng', 'office_address'])->where(['id' => $selected_office_section['office_id']])->first();
//
//
//        if($isExport=='excel' || $isExport=='pdf'){
//            return $result= Array('data'=> $data,'office_name'=> $office_name['office_name_bng'],'security_level_list'=>$security_level_list,'priority_list'=>$priority_list);
//        }
//
//        $this->set('dak_list', $data);
//        $this->set('office_name', $office_name);
//        $this->set(compact('security_level_list'));
//        $this->set(compact('priority_list'));
//    }
    public function dakGrohonRegisterContent($startDate = "", $endDate = "",$isExport="",$per_page = 20 ,$unit_id = "") {
         set_time_limit(0);
        $this->layout = null;

        $reportTable = TableRegistry::get('Reports');
        $office = TableRegistry::get('Offices');
        $dakMovementsTable = TableRegistry::get('DakMovements');


        $startDate = !empty($startDate) ? h($startDate) : date("Y-m-d");
        $endDate = !empty($endDate) ? h($endDate) : date("Y-m-d");
        $condition =[];

        $selected_office_section = $this->getCurrentDakSection();
        $this->set(compact('selected_office_section'));

        $officeUnitTable = TableRegistry::get("OfficeUnits");

        $units = $officeUnitTable->getOfficeChildInfo($selected_office_section['office_unit_id']);
        $unitsOwn = $officeUnitTable->getOwnChildInfo($selected_office_section['office_unit_id']);

        $units = !empty($units[0]) ? $units[0] : '';
        $unitsOwn = !empty($unitsOwn[0]) ? ($selected_office_section['office_unit_id'] . ',' . $unitsOwn[0]) : $selected_office_section['office_unit_id'];

        $units = $units . (!empty($unitsOwn) ? ((!empty($units) ? ',' : '') . $unitsOwn) : '');

        $unit_id_array = explode(",",$units);
        if(!empty($unit_id) && in_array((int)$unit_id,$unit_id_array)){
            $unit_id_array = (int)$unit_id;
        }
        $priorities = json_decode(DAK_PRIORITY_TYPE);
        $priority_list = array();

        foreach ($priorities as $key => $value) {
            $priority_list[$key] = $value;
        }

        $security_level_list = array();
        $security_levels = json_decode(DAK_SECRECY_TYPE);

        foreach ($security_levels as $key => $value) {
            $security_level_list[$key] = $value;
        }

        array_push($condition,['DakMovements.to_office_unit_id IN' => $unit_id_array]);
        array_push($condition,['DakMovements.operation_type IN' =>['Forward','Sent']]);
        $order=['DakMovements.created DESC'];

        if($isExport=='excel' || $isExport=='pdf'){
            $dak_list = $reportTable->getDakRegisters($startDate,$endDate,$condition,$order)->toArray();
        } else {
            $dak_list = $this->Paginator->paginate($reportTable->getDakRegisters($startDate,$endDate,$condition,$order), ['limit' => $per_page]);
        }

        $preorder = '';
        $data = [];
        $i = 0;
        if (!empty($dak_list)) {
            foreach ($dak_list as $dak) {
                if (empty($preorder)) {
                    $preorder = $dak['to_office_unit_id'];
                }
                $preorder = $preorder . ',' . $dak['to_office_unit_id'];
            }
            if (!empty($preorder)) {
                $order_unit = $officeUnitTable->getOrderwiseUnits($preorder);
                foreach ($order_unit as $key => $val) {
                    foreach ($dak_list as $dak) {
                        if ($dak['to_office_unit_id'] == $key) {
                            $data[$i]['daptorikCreated'] = $dak['daptorikCreated'];
                            $data[$i]['nagorikCreated'] = $dak['nagorikCreated'];
                            $data[$i]['id'] = $dak['id'];
                            $data[$i]['dak_type'] = $dak['dak_type'];

                            $data[$i]['from_office_name'] = $dak['from_office_name'];
                            $data[$i]['from_office_unit_name'] = $dak['from_office_unit_name'];
                            $data[$i]['from_officer_name'] = $dak['from_officer_name'];
                            $data[$i]['from_officer_designation_label'] = $dak['from_officer_designation_label'];

                            $data[$i]['dak_id'] = $dak['dak_id'];

                            $data[$i]['to_office_name'] = $dak['to_office_name'];
                            $data[$i]['to_office_unit_id'] = $dak['to_office_unit_id'];
                            $data[$i]['to_office_unit_name'] = $dak['to_office_unit_name'];
                            $data[$i]['to_officer_name'] = $dak['to_officer_name'];
                            $data[$i]['to_officer_designation_label'] = $dak['to_officer_designation_label'];

                            $data[$i]['operation_type'] = $dak['operation_type'];
                            $data[$i]['sequence'] = $dak['sequence'];
                            $data[$i]['attention_type'] = $dak['attention_type'];
                            $data[$i]['dak_actions'] = $dak['dak_actions'];
                            $data[$i]['created'] = $dak['created'];
                            $data[$i]['is_summary_nothi'] = $dak['is_summary_nothi'];
                            $data[$i]['dak_priority'] = $dak['dak_priority'];

                            $data[$i]['DakDaptoriks'] = $dak['DakDaptoriks'];
                            $data[$i]['DakNagoriks'] = $dak['DakNagoriks'];
                            $data[$i]['mainPrapok'] = $dakMovementsTable->getDakMovesBy_sequence_dakId($dak['sequence'], $dak['dak_id'], $dak['dak_type'], 1);


                            $i++;
                        }
                    }
                }
            }
        }
        $office_name = $office->find()->select(['office_name_bng', 'office_address'])->where(['id' => $selected_office_section['office_id']])->first();


        if($isExport=='excel' || $isExport=='pdf'){
            return $result= Array('data'=> $data,'office_name'=> $office_name['office_name_bng'],'security_level_list'=>$security_level_list,'priority_list'=>$priority_list);
        }

        $this->set('dak_list', $data);
        $this->set('office_name', $office_name);
        $this->set(compact('security_level_list'));
        $this->set(compact('priority_list'));
    }

    /**
     * Sent Dak report
     */
    public function dakBiliRegisterExcel($type='pdf',$startDate='',$endDate='',$dateRangeBn='', $unit_id = ''){
        $result = $this->dakBiliRegisterContent($startDate, $endDate, $type,20 ,$unit_id);

        $unitInformation='';
        $DakList = Array();
        $j=0;

        if (!empty($result['data'])) {

            foreach ($result['data'] as $value) {
                if( $unitInformation ==  $value['from_office_unit_id']){
                    $DakList[$j]['from_office_unit_name'] ='';
                }
                else {
                    $DakList[$j]['from_office_unit_name'] = $value['from_office_unit_name'];
                    $unitInformation = $value['from_office_unit_id'];
                }
                $DakList[$j]['dak_received_no'] = $value['dak_type'] ==DAK_DAPTORIK ? (enTobn($value['DakDaptoriks']['dak_received_no'])) : (enTobn($value['DakNagoriks']['dak_received_no']));
                $DakList[$j]['docketing_no'] = $value['dak_type'] == DAK_DAPTORIK ? enTobn($value['DakDaptoriks']['docketing_no']) : enTobn($value['DakNagoriks']['docketing_no']);
                $DakList[$j]['sender_sarok_no'] = $value['dak_type'] ==DAK_DAPTORIK ? $value['DakDaptoriks']['sender_sarok_no'] : '';

                $time = new Time($value['dak_type'] == DAK_DAPTORIK ? $value['daptorikCreated'] : $value['nagorikCreated']);
                $DakList[$j]['application_date'] = $time->i18nFormat(null, null, 'bn-BD');
                $DakList[$j]['dak_type'] = $value['dak_type'] == DAK_DAPTORIK ? 'দাপ্তরিক' : 'নাগরিক';
                $DakList[$j]['dak_subject'] = $value['dak_type'] == DAK_DAPTORIK ? $value['DakDaptoriks']['dak_subject'] : $value['DakNagoriks']['dak_subject'];
                $DakList[$j]['applicant_details'] = $value['dak_type'] == DAK_DAPTORIK ? ($value['from_officer_designation_label'] . ', ' . $value['from_office_unit_name'] . ', ' . $value['from_office_name']) : ( $value['from_officer_name'] . (!empty($value['from_officer_designation_label']) ? (', ' . $value['from_officer_designation_label']) : '') . (!empty($value['from_office_name']) ? (', ' . $value['from_office_name']) : '') );
                $DakList[$j]['previous_sender'] = $value['dak_type'] == DAK_DAPTORIK ? ($value['DakDaptoriks']['sender_officer_designation_label'] . ', ' . $value['DakDaptoriks']['sender_office_unit_name'] . ', ' . $value['DakDaptoriks']['sender_office_name']) : ($value['DakNagoriks']['sender_name']);


                if (!empty($value['mainPrapok'])) {
                    $DakList[$j]['mainPrapok']= $value['mainPrapok'][0]['to_officer_designation_label'] . ', ' . $value['mainPrapok'][0]['to_office_unit_name'] . ', ' . $value['mainPrapok'][0]['to_office_name'] ;

                } else { $DakList[$j]['mainPrapok']='';}

                $receiving_date = new Time($value['created']);
                $DakList[$j]['receiving_date']= $receiving_date->i18nFormat(null, null, 'bn-BD');

                $secIndex = ($value['dak_type'] == DAK_DAPTORIK ? $value['DakDaptoriks']['dak_security_level'] : $value['DakNagoriks']['dak_security_level']);
                $DakList[$j]['security_level_list']= isset($result['security_level_list'][$secIndex]) ? $result['security_level_list'][$secIndex] : '';

                $DakList[$j]['dak_priority']= isset($result['priority_list'][$value['dak_priority']]) ? $result['priority_list'][$value['dak_priority']] : '';
                $DakList[$j]['dak_actions']= $value['dak_actions'];

                $j++;

            }
        }
        if(empty($DakList)){
            $DakList= Array();
        }
        if($type == 'excel') {
            $this->printExcel($DakList, [
                ['key' => 'from_office_unit_name', 'title' => 'শাখা'],
                ['key' => 'dak_received_no', 'title' => 'গ্রহণ নম্বর'],
                ['key' => 'docketing_no', 'title' => 'ডকেট নং'],
                ['key' => 'sender_sarok_no', 'title' => 'স্মারক নম্বর'],
                ['key' => 'application_date', 'title' => 'আবেদনের তারিখ'],
                ['key' => 'dak_type', 'title' => 'ধরন'],
                ['key' => 'dak_subject', 'title' => 'বিষয়'],
                ['key' => 'applicant_details', 'title' => 'আবেদনকারী'],
                ['key' => 'previous_sender', 'title' => 'পূর্ববর্তী প্রেরক'],
                ['key' => 'mainPrapok', 'title' => 'মূল প্রাপক'],
                ['key' => 'receiving_date', 'title' => 'প্রাপ্তির তারিখ'],
                ['key' => 'security_level_list', 'title' => 'গোপনীয়তা'],
                ['key' => 'dak_priority', 'title' => 'অগ্রাধিকার'],
                ['key' => 'dak_actions', 'title' => 'সর্বশেষ অবস্থা'],
            ], [
                'name' => ((isset($result['office_name'])) ? $result['office_name'] . ' অফিসের ' : '') . 'ডাক বিলি নিবন্ধন বহি',
                'title' => [
                    ((isset($result['office_name'])) ? $result['office_name'] . ' অফিসের ' : '') . 'ডাক বিলি নিবন্ধন বহি',
                    $dateRangeBn,
                ]
            ]);
        } else {
            $this->printPdf($DakList, [
                ['key' => 'from_office_unit_name', 'title' => 'শাখা'],
                ['key' => 'dak_received_no', 'title' => 'গ্রহণ নম্বর'],
                ['key' => 'docketing_no', 'title' => 'ডকেট নং'],
                ['key' => 'sender_sarok_no', 'title' => 'স্মারক নম্বর'],
                ['key' => 'application_date', 'title' => 'আবেদনের তারিখ'],
                ['key' => 'dak_type', 'title' => 'ধরন'],
                ['key' => 'dak_subject', 'title' => 'বিষয়'],
                ['key' => 'applicant_details', 'title' => 'আবেদনকারী'],
                ['key' => 'previous_sender', 'title' => 'পূর্ববর্তী প্রেরক'],
                ['key' => 'mainPrapok', 'title' => 'মূল প্রাপক'],
                ['key' => 'receiving_date', 'title' => 'প্রাপ্তির তারিখ'],
                ['key' => 'security_level_list', 'title' => 'গোপনীয়তা'],
                ['key' => 'dak_priority', 'title' => 'অগ্রাধিকার'],
                ['key' => 'dak_actions', 'title' => 'সর্বশেষ অবস্থা'],
            ], [
                'name' => ((isset($result['office_name'])) ? $result['office_name'] . ' অফিসের ' : '') . 'ডাক বিলি নিবন্ধন বহি',
                'title' => [
                    ((isset($result['office_name'])) ? $result['office_name'] . ' অফিসের ' : '') . 'ডাক বিলি নিবন্ধন বহি',
                    $dateRangeBn,
                ]
            ]);
        }
        exit;
    }

    public function dakBiliRegister() {
        $session = $this->request->session();
        $employee = $session->read('logged_employee_record');
        $this->set("office", $employee);

        $units_list = $this->getOwnUnitList();
        $this->set("units_list", $units_list);
    }

    public function dakBiliRegisterContent($startDate = "", $endDate = "",$isExport='',$per_page = 20 ,$unit_id = "") {
          set_time_limit(0);
        $this->layout = null;

        $reportTable = TableRegistry::get('Reports');
        $dakMovementsTable = TableRegistry::get('DakMovements');

        $startDate = !empty($startDate) ? h($startDate) : date("Y-m-d");
        $endDate = !empty($endDate) ? h($endDate) : date("Y-m-d");
        $condition = [];

        $selected_office_section = $this->getCurrentDakSection();
        $this->set(compact('selected_office_section'));

        $officeUnitTable = TableRegistry::get("OfficeUnits");

        $units = $officeUnitTable->getOfficeChildInfo($selected_office_section['office_unit_id']);
        $unitsOwn = $officeUnitTable->getOwnChildInfo($selected_office_section['office_unit_id']);

        $units = !empty($units[0]) ? $units[0] : '';
        $unitsOwn = !empty($unitsOwn[0]) ? ($selected_office_section['office_unit_id'] . ',' . $unitsOwn[0]) : $selected_office_section['office_unit_id'];

        $units = $units . (!empty($unitsOwn) ? ((!empty($units) ? ',' : '') . $unitsOwn) : '');

        $unit_id_array = explode(",",$units);
        if(!empty($unit_id) && in_array((int)$unit_id,$unit_id_array)){
            $unit_id_array = (int)$unit_id;
        }

        array_push($condition,['DakMovements.from_office_unit_id IN' => $unit_id_array]);
        array_push($condition,['DakMovements.operation_type IN' =>['Forward','Sent']]);

        $priorities = json_decode(DAK_PRIORITY_TYPE);
        $priority_list = array();

        foreach ($priorities as $key => $value) {
            $priority_list[$key] = $value;
        }

        $security_level_list = array();
        $security_levels = json_decode(DAK_SECRECY_TYPE);

        foreach ($security_levels as $key => $value) {
            $security_level_list[$key] = $value;
        }

        if($isExport=='excel' || $isExport=='pdf'){
            $dak_list = $reportTable->getDakRegisters($startDate,$endDate,$condition)->toArray();
        } else {
            $dak_list = $this->Paginator->paginate($reportTable->getDakRegisters($startDate,$endDate,$condition), ['limit' => $per_page]);
        }

        $preorder = '';
        $data = [];
        $i = 0;
        if (!empty($dak_list)) {
            foreach ($dak_list as $dak) {
                if (empty($preorder)) {
                    $preorder = $dak['from_office_unit_id'];
                }
                $preorder = $preorder . ',' . $dak['from_office_unit_id'];
            }
            if (!empty($preorder)) {
                $order_unit = $officeUnitTable->getOrderwiseUnits($preorder);
                foreach ($order_unit as $key => $val) {
                    foreach ($dak_list as $dak) {
                        if ($dak['from_office_unit_id'] == $key) {
                            $data[$i]['daptorikCreated'] = $dak['daptorikCreated'];
                            $data[$i]['nagorikCreated'] = $dak['nagorikCreated'];
                            $data[$i]['id'] = $dak['id'];
                            $data[$i]['dak_type'] = $dak['dak_type'];

                            $data[$i]['from_office_name'] = $dak['from_office_name'];
                            $data[$i]['from_office_unit_id'] = $dak['from_office_unit_id'];
                            $data[$i]['from_office_unit_name'] = $dak['from_office_unit_name'];
                            $data[$i]['from_officer_name'] = $dak['from_officer_name'];
                            $data[$i]['from_officer_designation_label'] = $dak['from_officer_designation_label'];

                            $data[$i]['dak_id'] = $dak['dak_id'];

                            $data[$i]['to_office_name'] = $dak['to_office_name'];
                            $data[$i]['to_office_unit_id'] = $dak['to_office_unit_id'];
                            $data[$i]['to_office_unit_name'] = $dak['to_office_unit_name'];
                            $data[$i]['to_officer_name'] = $dak['to_officer_name'];
                            $data[$i]['to_officer_designation_label'] = $dak['to_officer_designation_label'];

                            $data[$i]['operation_type'] = $dak['operation_type'];
                            $data[$i]['sequence'] = $dak['sequence'];
                            $data[$i]['attention_type'] = $dak['attention_type'];
                            $data[$i]['dak_actions'] = $dak['dak_actions'];
                            $data[$i]['created'] = $dak['created'];
                            $data[$i]['is_summary_nothi'] = $dak['is_summary_nothi'];
                            $data[$i]['dak_priority'] = $dak['dak_priority'];

                            $data[$i]['DakDaptoriks'] = $dak['DakDaptoriks'];
                            $data[$i]['DakNagoriks'] = $dak['DakNagoriks'];
                            $data[$i]['mainPrapok']= $dakMovementsTable->getDakMovesBy_sequence_dakId($dak['sequence'], $dak['dak_id'], $dak['dak_type'], 1);


                            $i++;
                        }
                    }
                }
            }
        }
        $office = TableRegistry::get('Offices');
        $office_name = $office->find()->select(['office_name_bng', 'office_address'])->where(['id' => $selected_office_section['office_id']])->first();

        if($isExport=='excel' || $isExport=='pdf'){
            return $result= Array('data'=> $data,'office_name'=> $office_name['office_name_bng'],'security_level_list'=>$security_level_list,'priority_list'=>$priority_list);
        }

        $this->set('dak_list', $data);
        $this->set('office_name', $office_name);
        $this->set(compact('security_level_list'));
        $this->set(compact('priority_list'));
    }
//    public function dakBiliRegisterContent($startDate = "", $endDate = "",$isExport='') {
//          set_time_limit(0);
//        $this->layout = null;
//
//        $reportTable = TableRegistry::get('Reports');
//        $dakMovementsTable = TableRegistry::get('DakMovements');
//
//        $startDate = !empty($startDate) ? h($startDate) : date("Y-m-d");
//        $endDate = !empty($endDate) ? h($endDate) : date("Y-m-d");
//        $condition = ' 1 ';
//
//        if (!empty($startDate) And ! empty($endDate)) {
//            if (!empty($condition))
//                $condition .= ' AND ';
//            $condition .= " date(DakMovements.created)  BETWEEN  '{$startDate}' AND '{$endDate}' ";
//        } else {
//            $startDate = date('Y-m-d');
//            $endDate = date('Y-m-d');
//            if (!empty($condition))
//                $condition .= ' AND ';
//            $condition .= " date(DakMovements.created)  BETWEEN  '{$startDate}' AND '{$endDate}' ";
//        }
//
//        $selected_office_section = $this->getCurrentDakSection();
//        $this->set(compact('selected_office_section'));
//
//        $officeUnitTable = TableRegistry::get("OfficeUnits");
//
//        $units = $officeUnitTable->getOfficeChildInfo($selected_office_section['office_unit_id']);
//        $unitsOwn = $officeUnitTable->getOwnChildInfo($selected_office_section['office_unit_id']);
//
//        $units = !empty($units[0]) ? $units[0] : '';
//        $unitsOwn = !empty($unitsOwn[0]) ? ($selected_office_section['office_unit_id'] . ',' . $unitsOwn[0]) : $selected_office_section['office_unit_id'];
//
//        $units = $units . (!empty($unitsOwn) ? ((!empty($units) ? ',' : '') . $unitsOwn) : '');
////        $condition .= " AND DakMovements.from_office_id = {$selected_office_section['office_id']} ";
//        $condition .= " AND DakMovements.from_office_unit_id IN ({$units}) ";
////        $condition .= " AND DakMovements.from_officer_designation_id = {$selected_office_section['office_unit_organogram_id']} ";
//        $condition .= " AND DakMovements.operation_type IN ('Forward','Sent') ";
//
//
//        $priorities = json_decode(DAK_PRIORITY_TYPE);
//        $priority_list = array();
//
//        foreach ($priorities as $key => $value) {
//            $priority_list[$key] = $value;
//        }
//
//        $security_level_list = array();
//        $security_levels = json_decode(DAK_SECRECY_TYPE);
//
//        foreach ($security_levels as $key => $value) {
//            $security_level_list[$key] = $value;
//        }
//
//        if($isExport=='excel' || $isExport=='pdf'){
//            $dak_list = $reportTable->getDakRegisters($condition)->toArray();
//        } else {
//            $dak_list = $this->Paginator->paginate($reportTable->getDakRegisters($condition), ['limit' => 20]);
//        }
//
//        $preorder = '';
//        $data = [];
//        $i = 0;
//        if (!empty($dak_list)) {
//            foreach ($dak_list as $dak) {
//                if (empty($preorder)) {
//                    $preorder = $dak['from_office_unit_id'];
//                }
//                $preorder = $preorder . ',' . $dak['from_office_unit_id'];
//            }
//            if (!empty($preorder)) {
//                $order_unit = $officeUnitTable->getOrderwiseUnits($preorder);
//                foreach ($order_unit as $key => $val) {
//                    foreach ($dak_list as $dak) {
//                        if ($dak['from_office_unit_id'] == $key) {
//                            $data[$i]['daptorikCreated'] = $dak['daptorikCreated'];
//                            $data[$i]['nagorikCreated'] = $dak['nagorikCreated'];
//                            $data[$i]['id'] = $dak['id'];
//                            $data[$i]['dak_type'] = $dak['dak_type'];
//
//                            $data[$i]['from_office_name'] = $dak['from_office_name'];
//                            $data[$i]['from_office_unit_id'] = $dak['from_office_unit_id'];
//                            $data[$i]['from_office_unit_name'] = $dak['from_office_unit_name'];
//                            $data[$i]['from_officer_name'] = $dak['from_officer_name'];
//                            $data[$i]['from_officer_designation_label'] = $dak['from_officer_designation_label'];
//
//                            $data[$i]['dak_id'] = $dak['dak_id'];
//
//                            $data[$i]['to_office_name'] = $dak['to_office_name'];
//                            $data[$i]['to_office_unit_id'] = $dak['to_office_unit_id'];
//                            $data[$i]['to_office_unit_name'] = $dak['to_office_unit_name'];
//                            $data[$i]['to_officer_name'] = $dak['to_officer_name'];
//                            $data[$i]['to_officer_designation_label'] = $dak['to_officer_designation_label'];
//
//                            $data[$i]['operation_type'] = $dak['operation_type'];
//                            $data[$i]['sequence'] = $dak['sequence'];
//                            $data[$i]['attention_type'] = $dak['attention_type'];
//                            $data[$i]['dak_actions'] = $dak['dak_actions'];
//                            $data[$i]['created'] = $dak['created'];
//                            $data[$i]['is_summary_nothi'] = $dak['is_summary_nothi'];
//                            $data[$i]['dak_priority'] = $dak['dak_priority'];
//
//                            $data[$i]['DakDaptoriks'] = $dak['DakDaptoriks'];
//                            $data[$i]['DakNagoriks'] = $dak['DakNagoriks'];
//                            $data[$i]['mainPrapok']= $dakMovementsTable->getDakMovesBy_sequence_dakId($dak['sequence'], $dak['dak_id'], $dak['dak_type'], 1);
//
//
//                            $i++;
//                        }
//                    }
//                }
//            }
//        }
//        $office = TableRegistry::get('Offices');
//        $office_name = $office->find()->select(['office_name_bng', 'office_address'])->where(['id' => $selected_office_section['office_id']])->first();
//
//        if($isExport=='excel' || $isExport=='pdf'){
//            return $result= Array('data'=> $data,'office_name'=> $office_name['office_name_bng'],'security_level_list'=>$security_level_list,'priority_list'=>$priority_list);
//        }
//
//        $this->set('dak_list', $data);
//        $this->set('office_name', $office_name);
//        $this->set(compact('security_level_list'));
//        $this->set(compact('priority_list'));
//    }

    /**
     * Sent Dak report
     */
    public function dakDiaryRegisterExcel($type='pdf',$startDate='',$endDate='',$dateRangeBn='' ,$unit_id = "") {
        $result = $this->dakDiaryRegisterContent($startDate, $endDate, $type, 20 ,$unit_id);
        $unitInformation='';
        $DakList = Array();
        $j=0;

        if (!empty($result['data'])) {

            foreach ($result['data'] as $dak) {
                if( $unitInformation ==  $dak['to_office_unit_name']){
                    $DakList[$j]['to_office_unit_name'] ='';
                }
                else {
                    $DakList[$j]['to_office_unit_name'] = $dak['to_office_unit_name'];
                    $unitInformation = $dak['to_office_unit_name'];
                }
                $created = new Time($dak['created']);
                $dak['created'] = $created->i18nFormat(null, null, 'bn-BD');
                if(!empty($dak['NothiParts']['nothi_no'])){
                    $nothi_move_created = new Time($dak['NothiPotros']['created']);
                    $dak['nothi_move_created'] = $nothi_move_created->i18nFormat(null, null, 'bn-BD'); }
                else { $dak['nothi_move_created']= "প্রযোজ্য নয়";}

                $DakList[$j]['sender_sarok_no'] = h($dak['sender_sarok_no']);
                $DakList[$j]['created'] = h($dak['created']);
                $DakList[$j]['sender_name'] = h($dak['sender_name']) . ', ' . h($dak['sender_officer_designation_label']) . __(!empty($dak['sender_office_unit_name']) ? (", " . $dak['sender_office_unit_name']) : '') . __(!empty($dak['sender_office_name']) ? (", " . $dak['sender_office_name']) : '' );
                $DakList[$j]['dak_subject'] =$dak['dak_subject'];
                $DakList[$j]['nothi_no'] =h(!empty($dak['NothiParts']['nothi_no']) ? $dak['NothiParts']['nothi_no'] : "প্রযোজ্য নয়");
                $DakList[$j]['nothi_move_created'] =  h($dak['nothi_move_created']);

                $j++;

            }
        }
        if(empty($DakList)){
            $DakList= Array();
        }
        if($type=='excel'){
            $this->printExcel($DakList, [
                ['key' => 'to_office_unit_name', 'title' => 'শাখা'],
                ['key' => 'sender_sarok_no', 'title' => 'পত্রের স্মারক নম্বর'],
                ['key' => 'created', 'title' => 'পত্রের তারিখ'],
                ['key' => 'sender_name', 'title' => 'কাহার নিকট হইতে প্রাপ্ত'],
                ['key' => 'dak_subject', 'title' => 'পত্রের সংক্ষিপ্ত বিষয়'],
                ['key' => 'nothi_no', 'title' => 'নথি নম্বর'],
                ['key' => 'nothi_move_created', 'title' => 'চূড়ান্ত ব্যবস্থা গ্রহণের তারিখ'],
            ],[
                'name'=>((isset($result['office_name']))?$result['office_name'].' অফিসের ':'') .'শাখা ডায়েরি নিবন্ধন বহি' ,
                'title' => [
                    ((isset($result['office_name']))?$result['office_name'].' অফিসের ':'') .'শাখা ডায়েরি নিবন্ধন বহি' ,
                    $dateRangeBn,
                ]
            ]);
        } else {
            $this->printPdf($DakList, [
                ['key' => 'to_office_unit_name', 'title' => 'শাখা'],
                ['key' => 'sender_sarok_no', 'title' => 'পত্রের স্মারক নম্বর'],
                ['key' => 'created', 'title' => 'পত্রের তারিখ'],
                ['key' => 'sender_name', 'title' => 'কাহার নিকট হইতে প্রাপ্ত'],
                ['key' => 'dak_subject', 'title' => 'পত্রের সংক্ষিপ্ত বিষয়'],
                ['key' => 'nothi_no', 'title' => 'নথি নম্বর'],
                ['key' => 'nothi_move_created', 'title' => 'চূড়ান্ত ব্যবস্থা গ্রহণের তারিখ'],
            ],[
                'name'=>((isset($result['office_name']))?$result['office_name'].' অফিসের ':'') .'শাখা ডায়েরি নিবন্ধন বহি' ,
                'title' => [
                    ((isset($result['office_name']))?$result['office_name'].' অফিসের ':'') .'শাখা ডায়েরি নিবন্ধন বহি' ,
                    $dateRangeBn,
                ]
            ]);
        }

        exit;
    }

    public function dakDiaryRegister() {
        $session = $this->request->session();
        $employee = $session->read('logged_employee_record');
        $this->set("office", $employee);

        $units_list = $this->getOwnUnitList();
        $this->set("units_list",$units_list);
    }

    public function dakDiaryRegisterContent($startDate = "", $endDate = "", $isExport="",$per_page = 20 ,$unit_id = "") {
          set_time_limit(0);
        $this->layout = null;

        $employee_office = $this->getCurrentDakSection();

        //  echo $employee_section_id ;die;

        $startDate = !empty($startDate)?h($startDate):date("Y-m-d");
        $endDate = !empty($endDate)?h($endDate):date("Y-m-d");

        $officeUnitTable = TableRegistry::get("OfficeUnits");

        $units = $officeUnitTable->getOfficeChildInfo($employee_office['office_unit_id']);
        $unitsOwn = $officeUnitTable->getOwnChildInfo($employee_office['office_unit_id']);

        $units = !empty($units[0]) ? $units[0] : '';
        $unitsOwn = !empty($unitsOwn[0]) ? ($employee_office['office_unit_id'] . ',' . $unitsOwn[0]) : $employee_office['office_unit_id'];

        $units = $units . (!empty($unitsOwn) ? ((!empty($units) ? ',' : '') . $unitsOwn) : '');

        $unit_id_array = explode(",",$units);
        if(!empty($unit_id) && in_array((int)$unit_id,$unit_id_array)){
            $unit_id_array = (int)$unit_id;
        }

        $conditions = ["to_office_unit_id IN" => $unit_id_array];
        $order = ['DakMovements.modified'];
        $distinct = ['DakMovements.id'];

        $reportTable = TableRegistry::get('Reports');

        if($isExport=='excel' || $isExport=='pdf'){
            $grohon_daks= $reportTable->getDakDiaryRegister($startDate,$endDate,$conditions,$order,$distinct)->toArray();
        } else {
            $grohon_daks=  $this->Paginator->paginate($reportTable->getDakDiaryRegister($startDate,$endDate,$conditions,$order,$distinct), ['limit' => $per_page]);
        }

        $office = TableRegistry::get('Offices');
        $office_name = $office->find()->select(['office_name_bng', 'office_address'])->where(['id' => $employee_office['office_id']])->first();

        $preorder = '';
        $data = [];
        $i = 0;
        if (!empty($grohon_daks)) {
            foreach ($grohon_daks as $dak) {
                if (empty($preorder)) {
                    $preorder = $dak['to_office_unit_id'];
                }
                $preorder = $preorder . ',' . $dak['to_office_unit_id'];
            }
            if (!empty($preorder)) {
                $order_unit = $officeUnitTable->getOrderwiseUnits($preorder);
            }
            if (!empty($order_unit)) {
                foreach ($order_unit as $key => $val) {
                    foreach ($grohon_daks as $dak) {
                        if ($dak['to_office_unit_id'] == $key) {

                            $data[$i]['dak_id'] = $dak['dak_id'];
                            $data[$i]['sender_sarok_no'] = $dak['sender_sarok_no'];
                            $data[$i]['created'] = $dak['created'];
                            $data[$i]['to_office_unit_id'] = $dak['to_office_unit_id'];
                            $data[$i]['to_office_unit_name'] = $dak['to_office_unit_name'];

                            $data[$i]['sender_name'] = h($dak['sender_name']);
                            $data[$i]['sender_office_unit_name'] = $dak['sender_office_unit_name'];
                            $data[$i]['sender_officer_designation_label'] = $dak['sender_officer_designation_label'];
                            $data[$i]['sender_office_name'] = $dak['sender_office_name'];

                            $data[$i]['dak_subject'] = h($dak['dak_subject']);
                            $data[$i]['NothiParts'] = $dak['NothiParts'];
                            $data[$i]['NothiPotros'] = $dak['NothiPotros'];
                            $data[$i]['DakMovements'] = $dak['DakMovements'];
                            $i++;
                        }
                    }
                }
            }
        }
        if($isExport=='excel' || $isExport=='pdf'){
            return $result= Array('data'=> $data,'office_name'=> $office_name['office_name_bng']);
        }
        $this->set('data', $data);
        $this->set('office_name', $office_name);
    }
//    public function dakDiaryRegisterContent($startDate = "", $endDate = "", $isExport="") {
//          set_time_limit(0);
//        $this->layout = null;
//        $condition = '1 ';
//
//        $employee_office = $this->getCurrentDakSection();
//
//        //  echo $employee_section_id ;die;
//        $table_instance_dm = TableRegistry::get("DakMovements");
//
//        $startDate = !empty($startDate)?h($startDate):'';
//        $endDate = !empty($endDate)?h($endDate):'';
//
//        if (!empty($startDate) And ! empty($endDate)) {
//            if (!empty($condition))
//                $condition .= ' AND ';
//            $condition .= " DakMovements.created  BETWEEN  '{$startDate} 00:00:00' AND '{$endDate}
//            23:59:59' ";
//        } else {
//            $startDate = date('Y-m-d');
//            $endDate = date('Y-m-d');
//            if (!empty($condition))
//                $condition .= ' AND ';
//            $condition .= " DakMovements.created  BETWEEN  '{$startDate} 00:00:00' AND '{$endDate}
//            23:59:59' ";
//        }
//
//        $officeUnitTable = TableRegistry::get("OfficeUnits");
//
//        $units = $officeUnitTable->getOfficeChildInfo($employee_office['office_unit_id']);
//        $unitsOwn = $officeUnitTable->getOwnChildInfo($employee_office['office_unit_id']);
//
//        $units = !empty($units[0]) ? $units[0] : '';
//        $unitsOwn = !empty($unitsOwn[0]) ? ($employee_office['office_unit_id'] . ',' . $unitsOwn[0]) : $employee_office['office_unit_id'];
//
//        $units = $units . (!empty($unitsOwn) ? ((!empty($units) ? ',' : '') . $unitsOwn) : '');
//        $query = $table_instance_dm->find()->select([
//            'dak_id',
//            "sender_sarok_no" => 'DakDaptoriks.sender_sarok_no', 'DakMovements.created',
//            'sender_name' => 'DakDaptoriks.sender_name',
//            'sender_office_unit_name' => 'DakDaptoriks.sender_office_unit_name',
//            'sender_officer_designation_label' => 'DakDaptoriks.sender_officer_designation_label',
//            'sender_office_name' => 'DakDaptoriks.sender_office_name',
//            'dak_subject' => 'DakDaptoriks.dak_subject',
//            'NothiParts.nothi_no',
//            'NothiPotros.nothi_master_id',
//            'NothiPotros.created',
//            'NothiPotros.sarok_no',
//            'to_office_unit_id' => 'DakMovements.to_office_unit_id',
//            'to_office_unit_name' => 'DakMovements.to_office_unit_name'
//            ])->where(["to_office_unit_id IN ({$units})"])
//              ->join([
//                'DakDaptoriks' => [
//                    'table' => 'dak_daptoriks',
//                    'type' => 'INNER',
//                    'conditions' =>
//                    'DakDaptoriks.id =DakMovements.dak_id  AND DakMovements.dak_type = "' . DAK_DAPTORIK . '"',
//                ],
//                'NothiPotros' => [
//                    'table' => 'nothi_potros',
//                    'type' => 'left',
//                    'conditions' => [
//                        'NothiPotros.dak_id' => new \Cake\Database\Expression\IdentifierExpression('DakMovements.dak_id'),
//                        'NothiPotros.dak_type' => DAK_DAPTORIK,
//                    ]
//                ],
//                'NothiParts' => [
//                    'table' => 'nothi_parts',
//                    'type' => 'LEFT',
//                    'conditions' => [
//                        'NothiParts.id = NothiPotros.nothi_part_no'
//                    ]
//                ],
//            ])->where([$condition])->order(['DakMovements.modified'])->distinct(['DakMovements.id']);
//
//        if($isExport=='excel' || $isExport=='pdf'){
//            $grohon_daks= $query->toArray();
//        } else {
//            $grohon_daks=  $this->Paginator->paginate($query, ['limit' => 20]);
//        }
//
//        $office = TableRegistry::get('Offices');
//        $office_name = $office->find()->select(['office_name_bng', 'office_address'])->where(['id' => $employee_office['office_id']])->first();
//
//        $preorder = '';
//        $data = [];
//        $i = 0;
//        if (!empty($grohon_daks)) {
//            foreach ($grohon_daks as $dak) {
//                if (empty($preorder)) {
//                    $preorder = $dak['to_office_unit_id'];
//                }
//                $preorder = $preorder . ',' . $dak['to_office_unit_id'];
//            }
//            if (!empty($preorder)) {
//                $order_unit = $officeUnitTable->getOrderwiseUnits($preorder);
//            }
//            if (!empty($order_unit)) {
//                foreach ($order_unit as $key => $val) {
//                    foreach ($grohon_daks as $dak) {
//                        if ($dak['to_office_unit_id'] == $key) {
//
//                            $data[$i]['dak_id'] = $dak['dak_id'];
//                            $data[$i]['sender_sarok_no'] = $dak['sender_sarok_no'];
//                            $data[$i]['created'] = $dak['created'];
//                            $data[$i]['to_office_unit_id'] = $dak['to_office_unit_id'];
//                            $data[$i]['to_office_unit_name'] = $dak['to_office_unit_name'];
//
//                            $data[$i]['sender_name'] = h($dak['sender_name']);
//                            $data[$i]['sender_office_unit_name'] = $dak['sender_office_unit_name'];
//                            $data[$i]['sender_officer_designation_label'] = $dak['sender_officer_designation_label'];
//                            $data[$i]['sender_office_name'] = $dak['sender_office_name'];
//
//                            $data[$i]['dak_subject'] = h($dak['dak_subject']);
//                            $data[$i]['NothiParts'] = $dak['NothiParts'];
//                            $data[$i]['NothiPotros'] = $dak['NothiPotros'];
//                            $data[$i]['DakMovements'] = $dak['DakMovements'];
//                            $i++;
//                        }
//                    }
//                }
//            }
//        }
//        if($isExport=='excel' || $isExport=='pdf'){
//            return $result= Array('data'=> $data,'office_name'=> $office_name['office_name_bng']);
//        }
//        $this->set('data', $data);
//        $this->set('office_name', $office_name);
//    }

    /**
     * Sent Dak report
     */
    public function dakMovementRegisterExcel($startDate='',$endDate='',$dateRangeBn='') {
        $result = $this->dakMovementRegisterContent($startDate, $endDate, 'Excel');
        $unitInformation='';
        $DakList = Array();
        $j=0;

        if (!empty($result['data'])) {

            foreach ($result['data'] as $dak) {
                if( $unitInformation ==  $dak['receiving_office_unit_name']){
                    $DakList[$j]['receiving_office_unit_name'] ='';
                }
                else {
                    $DakList[$j]['receiving_office_unit_name'] = $dak['receiving_office_unit_name'];
                    $unitInformation = $dak['receiving_office_unit_name'];
                }
                $created = new Time($dak['created']);
                $dak['created'] = $created->i18nFormat(null, null, 'bn-BD');


                $DakList[$j]['sender_sarok_no'] = h($dak['sender_sarok_no']);
                $DakList[$j]['created'] = h($dak['created']);
                $DakList[$j]['sender_name'] = h($dak['sender_name']) . __(!empty($dak['sender_office_unit_name']) ? (", " . $dak['sender_office_unit_name']) : '') . __(!empty($dak['sender_office_name']) ? (", " . $dak['sender_office_name']) : '' );
                $DakList[$j]['dak_subject'] =$dak['dak_subject'];
                $DakList[$j]['description'] =  h($dak['from_officer_name'] . $dak['from_office_name'] .
                    ' - ' . $dak['to_officer_name'] . ', ' . $dak['to_office_address']);

                $j++;

            }
        }
        if(empty($DakList)){
            $DakList= Array();
        }
        $this->printExcel($DakList, [
            ['key' => 'receiving_office_unit_name', 'title' => 'শাখা'],
            ['key' => 'sender_sarok_no', 'title' => 'পত্রের স্মারক নম্বর'],
            ['key' => 'created', 'title' => 'পত্রের তারিখ'],
            ['key' => 'sender_name', 'title' => 'কাহার নিকট হইতে প্রাপ্ত'],
            ['key' => 'dak_subject', 'title' => 'পত্রের সংক্ষিপ্ত বিষয়'],
            ['key' => 'description', 'title' => 'গতিবিধি বিবরণ'],
        ],[
            'name'=>((isset($result['office_name']))?$result['office_name'].' অফিসের ':'') .'গতিবিধি নিবন্ধন বহি' ,
            'title' => [
                ((isset($result['office_name']))?$result['office_name'].' অফিসের ':'') .'গতিবিধি নিবন্ধন বহি' ,
                $dateRangeBn,
            ]
        ]);
        exit;
    }

    public function dakMovementRegister() {
        $session = $this->request->session();
        $employee = $session->read('logged_employee_record');
        $this->set("office", $employee);
    }

    public function dakMovementRegisterContent($startDate = "", $endDate = "",$isExport="") {
          set_time_limit(0);
        $this->layout = null;
        $condition = '1 ';

        $employee_office = $this->getCurrentDakSection();
        $employee_section_id = $employee_office['office_unit_id'];
        $dak_mov_table = TableRegistry::get("DakMovements");
        $dak_daptorik_table = TableRegistry::get("DakDaptoriks");

        $startDate = !empty($startDate)?h($startDate):'';

        $endDate = !empty($endDate)?h($endDate):'';

        if (!empty($startDate) And ! empty($endDate)) {
            if (!empty($condition))
                $condition .= ' AND ';
            $condition .= " created  BETWEEN  '{$startDate} 00:00:00' AND '{$endDate}
            23:59:59' ";
        } else {
            $startDate = date('Y-m-d');
            $endDate = date('Y-m-d');
            if (!empty($condition))
                $condition .= ' AND ';
            $condition .= " created  BETWEEN  '{$startDate} 00:00:00' AND '{$endDate}
            23:59:59' ";
        }

        $officeUnitTable = TableRegistry::get("OfficeUnits");

        $units = $officeUnitTable->getOfficeChildInfo($employee_office['office_unit_id']);
        $unitsOwn = $officeUnitTable->getOwnChildInfo($employee_office['office_unit_id']);

        $units = !empty($units[0]) ? $units[0] : '';
        $unitsOwn = !empty($unitsOwn[0]) ? ($employee_office['office_unit_id'] . ',' . $unitsOwn[0]) : $employee_office['office_unit_id'];

        $units = $units . (!empty($unitsOwn) ? ((!empty($units) ? ',' : '') . $unitsOwn) : '');
        $condition .= " AND receiving_office_unit_id IN ({$units})";

        $query = $dak_daptorik_table->find()->select(['receiving_office_unit_name','receiving_office_unit_id', 'sender_sarok_no', 'created', 'sender_name', 'sender_office_unit_name', 'sender_office_name', 'dak_subject', 'id'])->where([$condition])->order(['modified DESC']);

        if($isExport=='excel' || $isExport=='pdf'){
            $grohon_daks =$query->toArray();
        } else {
            $grohon_daks= $this->Paginator->paginate($query, ['limit' => 20]);
        }

            $office = TableRegistry::get('Offices');
            $office_name = $office->find()->select(['office_name_bng', 'office_address'])->where(['id' => $employee_office['office_id']])->first();

            $preorder = '';
            $data = [];
            $i = 0;
            if (!empty($grohon_daks)) {
                foreach ($grohon_daks as $dak) {
                    if (empty($preorder)) {
                        $preorder = $dak['receiving_office_unit_id'];
                    }
                    $preorder = $preorder . ',' . $dak['receiving_office_unit_id'];
                }
                if (!empty($preorder)) {
                    $order_unit = $officeUnitTable->getOrderwiseUnits($preorder);
                }
                if (!empty($order_unit)) {
                    foreach ($order_unit as $key => $val) {
                        foreach ($grohon_daks as $dak) {
                            if ($dak['receiving_office_unit_id'] == $key) {

                                $data[$i]['id'] = $dak['id'];

                                $info = $dak_mov_table->find()->select(['from_office_name', 'from_officer_name', 'dak_id', 'to_office_unit_name', 'to_officer_name'])->where(["dak_id" => $dak['id'], 'dak_type' => DAK_DAPTORIK])->first();

                                $data[$i]['from_office_name'] = $info['from_office_name'];
                                $data[$i]['from_officer_name'] = $info['from_officer_name'];
                                $data[$i]['to_office_address'] = $info['to_office_unit_name'];
                                $data[$i]['to_officer_name'] = $info['to_officer_name'];

                                $data[$i]['receiving_office_unit_name'] = $dak['receiving_office_unit_name'];
                                $data[$i]['receiving_office_unit_id'] = $dak['receiving_office_unit_id'];
                                $data[$i]['sender_sarok_no'] = $dak['sender_sarok_no'];
                                $data[$i]['created'] = $dak['created'];
                                $data[$i]['sender_name'] = $dak['sender_name'];
                                $data[$i]['sender_office_unit_name'] = $dak['sender_office_unit_name'];
                                $data[$i]['sender_office_name'] = $dak['sender_office_name'];
                                $data[$i]['dak_subject'] = $dak['dak_subject'];
                                
                               
                                $i++;
                            }
                        }
                    }
                }
            }
        if($isExport=='excel' || $isExport=='pdf'){
            return $result= Array('data'=> $data,'office_name'=> $office_name['office_name_bng']);
        }
            $this->set('data', $data);
            $this->set('office_name', $office_name);

            
    }

    protected function getOwnUnitList(){
        $employee_office = $this->getCurrentDakSection();

        $officeUnitTable = TableRegistry::get("OfficeUnits");

        $units = $officeUnitTable->getOfficeChildInfo($employee_office['office_unit_id']);
        $unitsOwn = $officeUnitTable->getOwnChildInfo($employee_office['office_unit_id']);

        $units = !empty($units[0]) ? $units[0] : '';
        $unitsOwn = !empty($unitsOwn[0]) ? ($employee_office['office_unit_id'] . ',' . $unitsOwn[0]) : $employee_office['office_unit_id'];

        $units = $units . (!empty($unitsOwn) ? ((!empty($units) ? ',' : '') . $unitsOwn) : '');
        $unit_id_array = explode(",",$units);
        $units_list = $officeUnitTable->find('list',['keyField'=>'id','valueField'=>'unit_name_bng'])->where(['id IN'=>$unit_id_array])->toArray();
        if(count($unit_id_array) > 1){
            $units_list = [''=>'সকল শাখা']+$units_list;
        }

        return $units_list;
    }

}
