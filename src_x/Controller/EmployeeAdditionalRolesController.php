<?php
namespace App\Controller;

use App\Controller\ProjapotiController;
use App\Model\Table;
use App\Model\Table\EmployeeAdditionalRolesTable;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\Entity;
use App\Model\Entity\EmployeeAdditionalRoles;
use Cake\ORM\TableRegistry;

class EmployeeAdditionalRolesController extends ProjapotiController
{
//    public $paginate = [
//        'fields' => ['Districts.id'],
//        'limit' => 25,
//        'order' => [
//            'Districts.id' => 'asc'
//        ]
//    ];
//
//    public function initialize()
//    {
//        parent::initialize();
//        $this->loadComponent('Paginator');
//    }

    public function index()
    {
        $employee_additional_roles = TableRegistry::get('EmployeeAdditionalRoles');
        $query = $employee_additional_roles->find('all');
        $this->set(compact('query'));
    }

    public function add()
    {
        $this->loadModel('EmployeeRecords');
        $employee_records = $this->EmployeeRecords->find('all');
        $this->set(compact('employee_records'));

        $this->loadModel('OfficeRecords');
        $office_records = $this->OfficeRecords->find('all');
        $this->set(compact('office_records'));

        $this->loadModel('OfficeOrganograms');
        $office_organograms = $this->OfficeOrganograms->find('all');
        $this->set(compact('office_organograms'));

        $this->loadModel('UserRoles');
        $roles = $this->UserRoles->find('all');
        $this->set(compact('roles'));

        $employee_additional_roles = $this->EmployeeAdditionalRoles->newEntity();
        if ($this->request->is('post')) {
            $employee_additional_roles = $this->EmployeeAdditionalRoles->patchEntity($employee_additional_roles, $this->request->data);
            if ($this->EmployeeAdditionalRoles->save($employee_additional_roles)) {
                return $this->redirect(['action' => 'index']);
            }
        }
        $this->set('employee_additional_roles', $employee_additional_roles);
    }

    public function edit($id = null, $is_ajax = null)
    {
        if ($is_ajax == 'ajax') {
            $this->layout = null;
        }

        $this->loadModel('EmployeeRecords');
        $employee_records = $this->EmployeeRecords->find('all');
        $this->set(compact('employee_records'));

        $this->loadModel('OfficeRecords');
        $office_records = $this->OfficeRecords->find('all');
        $this->set(compact('office_records'));

        $this->loadModel('OfficeOrganograms');
        $office_organograms = $this->OfficeOrganograms->find('all');
        $this->set(compact('office_organograms'));

        $this->loadModel('UserRoles');
        $roles = $this->UserRoles->find('all');
        $this->set(compact('roles'));

        $employee_additional_role = $this->EmployeeAdditionalRoles->get($id);
        if ($this->request->is(['post', 'put'])) {
            $this->EmployeeAdditionalRoles->patchEntity($employee_additional_role, $this->request->data);
            if ($this->EmployeeAdditionalRoles->save($employee_additional_role)) {
                return $this->redirect(['action' => 'index']);
            }
        }
        $this->set('employee_additional_role', $employee_additional_role);
    }


    public function view($id = null)
    {
        $employee_additional_role = $this->EmployeeAdditionalRoles->get($id);
        $this->set(compact('employee_additional_role'));
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);

        $employee_additional_role = $this->EmployeeAdditionalRoles->get($id);
        if ($this->EmployeeAdditionalRoles->delete($employee_additional_role)) {
            return $this->redirect(['action' => 'index']);
        }
    }

    public function mapAdditionalRoles($is_ajax = null)
    {
        if ($is_ajax == 'ajax') {
            $this->layout = null;
        }

        if ($this->request->is(['post', 'put'])) {
            $office_unit_organogram_id = $this->request->data('main_id');
            $mapped_office_unit_organogram_id = $this->request->data('secondary_id');

            $employee_additional_roles = TableRegistry::get('EmployeeAdditionalRoles');
            $entity = $employee_additional_roles->newEntity();

            $entity->office_unit_organogram_id = $office_unit_organogram_id;
            $entity->mapped_office_unit_organogram_id = $mapped_office_unit_organogram_id;
            if ($employee_additional_roles->save($entity)) {
                $this->response->body(json_encode(1));
            } else {
                $this->response->body(json_encode(0));
            }

            $this->response->type('application/json');
            return $this->response;
        }
    }
}
