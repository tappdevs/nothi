<?php
namespace App\Controller;

use App\Controller\ProjapotiController;
use App\Model\Table;
use App\Model\Table\EmployeeBatchesTable;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

class EmployeeBatchesController extends ProjapotiController
{
    /* public $paginate = [
        'fields' => ['Districts.id'],
        'limit' => 25,
        'order' => [
            'Districts.id' => 'asc'
        ]
    ];

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Paginator');
    }
 */
    public function index()
    {
        $employee_batches = TableRegistry::get('EmployeeBatches');
        $query = $employee_batches->find('all');
        $this->set(compact('query'));
    }

    public function add()
    {
        $employee_batch = $this->EmployeeBatches->newEntity();
        if ($this->request->is('post')) {
            $employee_batch = $this->EmployeeBatches->patchEntity($employee_batch, $this->request->data);
            if ($this->EmployeeBatches->save($employee_batch)) {
                return $this->redirect(['action' => 'index']);
            }
        }
        $this->set('employee_batch', $employee_batch);
    }

    public function edit($id = null, $is_ajax = null)
    {
        if ($is_ajax == 'ajax') {
            $this->layout = null;
        }
        $employee_batch = $this->EmployeeBatches->get($id);
        if ($this->request->is(['post', 'put'])) {
            $this->EmployeeBatches->patchEntity($employee_batch, $this->request->data);
            if ($this->EmployeeBatches->save($employee_batch)) {
                return $this->redirect(['action' => 'index']);
            }
        }
        $this->set('employee_batch', $employee_batch);
    }

    public function view($id = null)
    {
        $employee_batch = $this->EmployeeBatches->get($id);
        $this->set(compact('employee_batch'));
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);

        $employee_batch = $this->EmployeeBatches->get($id);
        if ($this->EmployeeBatches->delete($employee_batch)) {
            return $this->redirect(['action' => 'index']);
        }
    }


}
