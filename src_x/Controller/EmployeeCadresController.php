<?php
namespace App\Controller;

use App\Controller\ProjapotiController;
use App\Model\Table;
use App\Model\Table\EmployeeCadresTable;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

class EmployeeCadresController extends ProjapotiController
{
    /* public $paginate = [
        'fields' => ['Districts.id'],
        'limit' => 25,
        'order' => [
            'Districts.id' => 'asc'
        ]
    ];

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Paginator');
    }
 */
    public function index()
    {
        $employee_cadres = TableRegistry::get('EmployeeCadres');
        $query = $employee_cadres->find('all');
        $this->set(compact('query'));
    }

    public function add()
    {
        $employee_cadre = $this->EmployeeCadres->newEntity();
        if ($this->request->is('post')) {
            $employee_cadre = $this->EmployeeCadres->patchEntity($employee_cadre, $this->request->data);
            if ($this->EmployeeCadres->save($employee_cadre)) {
                return $this->redirect(['action' => 'index']);
            }
        }
        $this->set('employee_cadre', $employee_cadre);
    }

    public function edit($id = null, $is_ajax = null)
    {
        if ($is_ajax == 'ajax') {
            $this->layout = null;
        }
        $employee_cadre = $this->EmployeeCadres->get($id);
        if ($this->request->is(['post', 'put'])) {
            $this->EmployeeCadres->patchEntity($employee_cadre, $this->request->data);
            if ($this->EmployeeCadres->save($employee_cadre)) {
                return $this->redirect(['action' => 'index']);
            }
        }
        $this->set('employee_cadre', $employee_cadre);
    }

    public function view($id = null)
    {
        $employee_cadre = $this->EmployeeCadres->get($id);
        $this->set(compact('employee_cadre'));
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);

        $employee_cadre = $this->EmployeeCadres->get($id);
        if ($this->EmployeeCadres->delete($employee_cadre)) {
            return $this->redirect(['action' => 'index']);
        }
    }


}
