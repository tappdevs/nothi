<?php

namespace App\Controller;

use App\Controller\ProjapotiController;
use App\Model\Table;
use App\Model\Table\EmployeeGradesTable;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\Entity;
use App\Model\Entity\EmployeeGrades;
use Cake\ORM\TableRegistry;

class EmployeeGradesController extends ProjapotiController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Paginator');
    }

    public function index()
    {
        $employee_grade = TableRegistry::get('EmployeeGrades');
        $query          = $employee_grade->find('all')->contain(['EmployeeCadres','EmployeeRanks']);
        try {
            $this->set('query', $this->paginate($query));
        } catch (NotFoundException $e) {
            $this->redirect(['action' => 'index']);
        }
    }

    public function add()
    {
        $employee_cadres =TableRegistry::get('EmployeeCadres')->find('all');
        $this->set(compact('employee_cadres'));

        $this->loadModel('EmployeeRanks');
        $employee_ranks = $this->EmployeeRanks->find('all');
        $this->set(compact('employee_ranks'));

        $employee_grades = $this->EmployeeGrades->newEntity();
        if ($this->request->is('post')) {
            $employee_grades = $this->EmployeeGrades->patchEntity($employee_grades,
                $this->request->data);
            if ($employee_grades->errors()) {
                $this->set('errors', $employee_grades->errors());
            } else {
                if ($this->EmployeeGrades->save($employee_grades)) {
                      $this->Flash->success('সংরক্ষিত হয়েছে।');
                    return $this->redirect(['action' => 'index']);
                }
            }
        }
        $this->set('employee_grades', $employee_grades);
    }

    public function edit($id = null, $is_ajax = null)
    {
        if ($is_ajax == 'ajax') {
            $this->layout = null;
        }

        $this->loadModel('EmployeeCadres');
        $employee_cadres = $this->EmployeeCadres->find('all');
        $this->set(compact('employee_cadres'));

        $this->loadModel('EmployeeRanks');
        $employee_ranks = $this->EmployeeRanks->find('all');
        $this->set(compact('employee_ranks'));

        $employee_grade = $this->EmployeeGrades->get($id);
        if ($this->request->is(['post', 'put'])) {
            $this->EmployeeGrades->patchEntity($employee_grade,
                $this->request->data);
            if ($this->EmployeeGrades->save($employee_grade)) {
                return $this->redirect(['action' => 'index']);
            }
        }
        $this->set('employee_grade', $employee_grade);
    }

    public function view($id = null)
    {
        $employee_grade = $this->EmployeeGrades->get($id);
        $this->set(compact('employee_grade'));
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);

        $employee_grade = $this->EmployeeGrades->get($id);
        if ($this->EmployeeGrades->delete($employee_grade)) {
            return $this->redirect(['action' => 'index']);
        }
    }
}