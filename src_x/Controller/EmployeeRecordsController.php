<?php

namespace App\Controller;

use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
use Cake\I18n\Time;

class EmployeeRecordsController extends ProjapotiController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['getOfficerInfoForSealSelection', 'getUserSignature', 'getUserProfilePhoto','userDetails','getOfficerInfo','postApiUserSignatureType','setDefaultSignature','getDigitalSignatureSettings','postApiGetEmployeeInfo','checkUserSoftToken']);

        $user = $this->Auth->user();
        if ($user['user_role_id'] > 2) {
            if (in_array($this->request->action,
                ['add', 'edit', 'delete', 'index', 'updateLogin'])) {
                $this->Flash->error(__('Unauthorized Access'));
                $this->redirect(['action' => 'dashboard', 'controller' => 'dashboard']);
            }
        }
    }

    public function index()
    {
        $this->redirect(['action' => 'employeeRecordList']);
        $employee_records = TableRegistry::get('EmployeeRecords');
        $query = $employee_records->find('all');
        $this->set(compact('query'));
    }

    public function add()
    {
        $table_instance_grade = TableRegistry::get('EmployeeGrades');
        $data_items = $table_instance_grade->find('list')->toArray();
        $this->set('serviceGrades', $data_items);

        $table_instance_rank = TableRegistry::get('EmployeeRanks');
        $employeeRanks = $table_instance_rank->find('list')->toArray();
        $this->set('serviceRanks', $employeeRanks);

        $table_instance_batch = TableRegistry::get('EmployeeBatches');
        $employeeBatches = $table_instance_batch->find('list')->toArray();
        $this->set('employeeBatches', $employeeBatches);

        $table_instance_cadre = TableRegistry::get('EmployeeCadres');
        $employeeCadres = $table_instance_cadre->find('list')->toArray();
        $this->set('employeeCadres', $employeeCadres);

        $table_instance_emp_records = TableRegistry::get('EmployeeRecords');

        $employee_record = $table_instance_emp_records->newEntity();

        if ($this->request->is('post')) {
            try {
                $conn = ConnectionManager::get('projapotiDb');
                $conn->begin();
                //$employee_record_id = $table_instance_emp_records->getLastId();

                $user_entity_table = TableRegistry::get('Users');

                $this->request->data['identity_no'] = isset($this->request->data['identity_no'])
                    ? trim($this->request->data['identity_no']) : '';
                $this->request->data['identity_no'] = bnToen($this->request->data['identity_no']);
                if ($this->request->data['identity_no'] != '' && strlen($this->request->data['identity_no']) > 11) {
                    throw new \Exception('দুঃখিত! পরিচিতি নম্বর ১১ ডিজিটের বেশি হতে পারবে না। দয়া করে পুনরায় চেষ্টা করুন।');
                }
                $this->request->data['is_cadre'] = $table_instance_emp_records->getCadreId($this->request->data);
                $this->request->data['date_of_birth'] = date("Y-m-d",strtotime($this->request->data['date_of_birth']));

                if ($this->request->data['is_cadre'] == 0) {
                    throw new \Exception(__('Sorry! You did not provide Identity No'));
                }
                if (!empty($this->request->data['date_of_birth'])) {
                    $datetime1 = new \DateTime($this->request->data['date_of_birth']);
                    $datetime2 = new \DateTime("now");
                    $check_DOB = date_diff($datetime1, $datetime2)->format('%y');
                    if (($check_DOB) < 20 || ($check_DOB) > 90) {
                        throw new \Exception(' জন্ম তারিখ সঠিক নয়। ');
                    }
                } else {
                    throw new \Exception(' জন্ম তারিখ সঠিক নয়।   ');
                }
                
                // duplicate check using name, fathers_name, mothers_name, date_of_birth
                $employee_record_check = $table_instance_emp_records->find()->where([
                    'name_bng' => $this->request->data['name_bng'],
                    'father_name_bng' => $this->request->data['father_name_bng'],
                    'mother_name_bng' => $this->request->data['mother_name_bng'],
                    'date_of_birth' => $this->request->data['date_of_birth'],
                ])->count();
                if ($employee_record_check != 0) {
                    throw new \Exception(' এই ব্যবহারকারী পূর্বে অন্তর্ভূক্ত হয়েছে। ');
                }

                if (!empty($this->request->data['nid'])) {
                    if(mb_strlen($this->request->data['nid']) != 10){
                        $check_NID_DOB = $this->checkNID($this->request->data['date_of_birth'],
                            $this->request->data['nid']);
                        if ($check_NID_DOB > 0) {
                            throw new \Exception(' জাতীয় পরিচয়পত্রের তথ্য   মিলছে না।  Error: ' . $check_NID_DOB);
                        }
                    }
                } else {
                    throw new \Exception(' জাতীয় পরিচয়পত্র নম্বর ১৭ সংখ্যার হতে হবে।   ');
                }

                $this->request->data['nid'] = bnToen($this->request->data['nid']);
                $username = $table_instance_emp_records->createUserId($this->request->data);

                if (empty($username)) {
                    throw new \Exception(__('Sorry! You did not provide Identity No'));
                }

                $this->request->data['username'] = $username;
                $this->request->data['name_eng'] = str_replace(',', '.',
                    $this->request->data['name_eng']);
                $this->request->data['name_bng'] = str_replace(',', '.',
                    $this->request->data['name_bng']);

                $this->request->data['name_eng'] = str_replace(';', '.',
                    $this->request->data['name_eng']);
                $this->request->data['name_bng'] = str_replace(';', '.',
                    $this->request->data['name_bng']);


                $userExist = $user_entity_table->find()->select(['username'])->where(['username' => $username])->first();

                if (!empty($userExist)) {
                    throw new \Exception('দুঃখিত! এই  ইউজার আইডি ব্যবহার হয়ে গেছে');
                }

                $userExist = $user_entity_table->find()->select(['user_alias'])->where(['user_alias' => $username])->first();

                if (!empty($userExist)) {
                    throw new \Exception('দুঃখিত! এই ইউজার নেম ব্যবহার হয়ে গেছে');
                }

                $employee_record = $table_instance_emp_records->patchEntity($employee_record, $this->request->data, ['validate' => 'add']);

                $employee_record['name_bng'] = str_replace(';', '.',
                    $employee_record['name_bng']);
                $employee_record['name_bng'] = str_replace(',', '.',
                    $employee_record['name_bng']);
                //$employee_record['id'] = $employee_record_id['id'] + 1;
                $employee_record['status'] = 1;
                $employee_record['created_by'] = $this->Auth->user('id');
                $employee_record['modified_by'] = $this->Auth->user('id');

                if (!empty($employee_record['date_of_birth'])) {
                    $employee_record['date_of_birth'] = date("Y-m-d",strtotime($employee_record['date_of_birth']));
                } else {
                    $employee_record['date_of_birth'] = date("Y-m-d",strtotime($this->request->data['date_of_birth']));
                }

                if ($record = $table_instance_emp_records->save($employee_record)) {

                    $user_entity = $user_entity_table->newEntity();
                    $user_entity->username = $username;
                    $user_entity->is_email_verified = 1;
                    $user_entity->email_verify_code = '';
                    $user_entity->ssn = '';
                    $user_entity->active = 1;
                    $user_entity->verification_date = date("Y-m-d");
                    $user_entity->last_password_change = date("Y-m-d");

                    $this->request->data['password'] = trim($this->request->data['password']);
                    $user_entity->password = (empty($this->request->data['password'])
                        ? DEFAULT_PASSWORD : $this->request->data['password']);

                    $user_type_entity = TableRegistry::get('UserRoles');
                    $user_type_id = $user_type_entity->find()->where(['name' => 'Employee'])->first()->id;

                    $user_entity->user_role_id = $user_type_id;
                    $user_entity->user_status = 1;
                    $user_entity->employee_record_id = $record->id;
                    $user_entity->user_alias = $username;
                    $user_entity->created_by = $this->Auth->user('id');
                    $user_entity->modified_by = $this->Auth->user('id');

                    $user_entity_table->save($user_entity);

                    $conn->commit();
                    $this->Flash->success(__('Successfully Done'));
                    return $this->redirect(['action' => 'add']);
                }
            } catch (\Exception $ex) {
                $this->set('entity', $this->request->data);
                $this->Flash->error(__('Something went wrong') . ' ' . $ex->getMessage());
                $conn->rollback();
            }
        }
        $this->set('entity', $employee_record);
    }

    public function edit($id = null, $is_ajax = null)
    {
        $auth = $this->Auth->user();

        if ($auth['user_role_id'] > 2) {
            $this->redirect(['controller' => 'dashboard', 'action' => 'dashboard']);
        }

        Time::setToStringFormat('yyyy-MM-dd');
        if ($is_ajax == 'ajax') {
            $this->layout = null;
        }

        $table_instance_grade = TableRegistry::get('EmployeeGrades');
        $data_items = $table_instance_grade->find('list')->toArray();
        $this->set('serviceGrades', $data_items);

        $table_instance_rank = TableRegistry::get('EmployeeRanks');
        $employeeRanks = $table_instance_rank->find('list')->toArray();
        $this->set('serviceRanks', $employeeRanks);

        $table_instance_batch = TableRegistry::get('EmployeeBatches');
        $employeeBatches = $table_instance_batch->find('list')->toArray();
        $this->set('employeeBatches', $employeeBatches);

        $table_instance_cadre = TableRegistry::get('EmployeeCadres');
        $employeeCadres = $table_instance_cadre->find('list')->toArray();
        $this->set('employeeCadres', $employeeCadres);

        $table_instance_emp_records = TableRegistry::get('EmployeeRecords');
        $employee_record = $table_instance_emp_records->get($id);

        if (!empty($employee_record) && $this->request->is(['post', 'put'])) {
            $this->request->data['name_eng'] = str_replace(',', '.',
                $this->request->data['name_eng']);
            $this->request->data['name_bng'] = str_replace(',', '.',
                $this->request->data['name_bng']);

            $this->request->data['name_eng'] = str_replace(';', '.',
                $this->request->data['name_eng']);
            $this->request->data['name_bng'] = str_replace(';', '.',
                $this->request->data['name_bng']);

            $employee_record = $table_instance_emp_records->patchEntity($employee_record,
                $this->request->data, ['validate' => 'edit']);
            $employee_record['name_bng'] = str_replace(';', '.',
                $employee_record['name_bng']);
            $employee_record['name_bng'] = str_replace(',', '.',
                $employee_record['name_bng']);
            $has_error = $employee_record->errors();
            if (empty($has_error)) {
                try {
                    $employee_record->date_of_birth = date("Y-m-d 00:00:00",
                        strtotime($this->request->data['date_of_birth']));

                    $employee_record->nid = bnToen($employee_record->nid);
                    if (mb_strlen($employee_record->nid) != 10) {
                        $check_NID_DOB = $this->checkNID($employee_record->date_of_birth, $employee_record->nid);
                        if ($check_NID_DOB > 0) {
                            throw new \Exception(' জাতীয় পরিচয়পত্রের তথ্য   মিলছে না।  Error: ' . $check_NID_DOB);
                        }
                    }
                    if ($table_instance_emp_records->save($employee_record)) {
                        return $this->redirect(['action' => 'employeeRecordList']);
                    }
                }catch (\Exception $ex){
                    $this->Flash->error($ex->getMessage());
                }
            }
        }
          if(!empty($employee_record->date_of_birth)){
//                $time = new Time($employee_record->date_of_birth);
        //        $employee_record->date_of_birth = $time->i18nFormat(null, null, 'en-US');
                $time = $employee_record->date_of_birth;
                $employee_record->date_of_birth = $time->format("Y-m-d");
          }
           if(!empty($employee_record->joining_date)){
//                $time = new Time($employee_record->joining_date);
        //        $employee_record->joining_date = $time->i18nFormat(null, null, 'en-US');
               $time = $employee_record->joining_date;
                $employee_record->joining_date = $time->format("Y-m-d");
           }

        $this->set('entity', $employee_record);
    }

    public function view($id = null)
    {
        $employee_record = $this->EmployeeRecords->get($id);
        $this->set(compact('employee_record'));
    }

    public function delete($id = null)
    {
        $auth = $this->Auth->user();

        if ($auth['user_role_id'] > 2) {
            $this->redirect(['controller' => 'dashboard', 'action' => 'dashboard']);
        }

        $this->request->allowMethod(['post', 'delete']);

        $employee_records = $this->EmployeeRecords->get($id);
        if ($this->EmployeeRecords->delete($employee_records)) {
            return $this->redirect(['action' => 'index']);
        }
    }

    public function loadLayersByMinistry()
    {
        $ministry_id = $this->request->data['office_ministry_id'];
        $this->loadModel('OfficeLayers');
        $layers = $this->OfficeLayers->find('all')->select(['id', 'layer_name_bng'])->where(['office_ministry_id' => $ministry_id,
            'active_status' => 1]);
        $listData = array();
        foreach ($layers as $layer) {
            $listData[$layer['id']] = $layer['layer_name_bng'];
        }

        $this->response->body(json_encode($listData));
        $this->response->type('application/json');
        return $this->response;
    }

    /**
     * Get Ministry wise office layers
     * Method Type: Ajax
     */
    public function loadOfficesByMinistryAndLayer()
    {
        $ministry_id = $this->request->data['office_ministry_id'];
        $layer_id = $this->request->data['office_layer_id'];
        $this->loadModel('Offices');
        $offices = $this->Offices->find('all')->select(['id', 'office_name_bng'])->where(['office_ministry_id' => $ministry_id,
            'office_layer_id' => $layer_id]);
        $listData = array();
        foreach ($offices as $office) {
            $listData[$office['id']] = $office['office_name_bng'];
        }

        $this->response->body(json_encode($listData));
        $this->response->type('application/json');
        return $this->response;
    }

    public function loadOfficesAndDesignationsByMinistryAndLayer()
    {
        $ministry_id = $this->request->data['office_ministry_id'];
        $layer_id = $this->request->data['office_layer_id'];

        $this->loadModel('Offices');
        $offices = $this->Offices->find('all')->select(['id', 'office_name_bng'])->where(['office_ministry_id' => $ministry_id,
            'office_layer_id' => $layer_id]);

        $listDataParentOffices = array();
        foreach ($offices as $office) {
            $listDataParentOffices[$office['id']] = $office['office_name_bng'];
        }

        $this->loadModel('OfficeOrganograms');
        $designations = $this->OfficeOrganograms->find('all')->select(['id',
            'designation_bng'])->where(['office_ministry_id' => $ministry_id,
            'office_layer_id' => $layer_id]);
        $listDataDesignations = array();
        foreach ($designations as $designation) {
            $listDataDesignations[$designation['id']] = $designation['designation_bng'];
        }

        $responseData = array(
            'offices' => $listDataParentOffices,
            'designations' => $listDataDesignations
        );

        $this->response->body(json_encode($responseData));
        $this->response->type('application/json');
        return $this->response;
    }

    /**
     * Employee Management
     * Show Current Assignment
     * Release From Current Assignment
     * Rebook into new assignment
     */
    public function employeeManagement()
    {
        $employee_offices = $this->getCurrentDakSection();

        $this->set('employee_offices', $employee_offices);
        $table_instance = TableRegistry::get('OfficeInchargeTypes');
        $data_items = $table_instance->find('all')->toArray();

        $data = array();
        foreach ($data_items as $item) {
            $data[$item['name_bng']] = $item['name_bng'];
        }

        $this->set('officeInchargeTypes', $data);
    }

    /**
     * Output: Employee Current Job Status
     */
    public function getEmployeeJobStatus()
    {
        $currentOffice = $this->getCurrentDakSection();

        $identity_no = $this->request->data['identity_no']; //User Login Id
        $n_id = $this->request->data['n_id']; //User Login Id

        $identity_no = bnToen($identity_no);

        if(!empty($identity_no)){
            $table_user = TableRegistry::get('Users');
            $user = $table_user->find()->where(['username' => $identity_no])->first();
        }
        else if(!empty($n_id)){
            $user = TableRegistry::get('EmployeeRecords')->getUserDatabyNID($n_id)->first();
            $user['employee_record_id'] = $user['id'];
        }

        /* get employee personal info */
        $table_instance_emp_records = TableRegistry::get('EmployeeRecords');
        $employee_info = $table_instance_emp_records->get($user['employee_record_id']);

        /* get employee job info */
        $table_instance_emp_offices = TableRegistry::get('EmployeeOffices');
        $employee_office_records = $table_instance_emp_offices->getEmployeeOfficeRecords($employee_info['id']);

        $data_item = array();
        $data_item['current_office_unit_organogram_id'] = $currentOffice['office_unit_organogram_id'];
        $data_item['own_office'] = !empty($currentOffice['office_id']) ? $currentOffice['office_id']
            : 0;
        $data_item['personal_info'] = $employee_info;
        $data_item['office_records'] = count($employee_office_records) > 0 ? $employee_office_records
            : array();

        $this->response->body(json_encode($data_item));
        $this->response->type('application/json');
        return $this->response;
    }

    /**
     * Output: Employee All Roles Histories
     */
    public function getEmployeeRoleHistories()
    {
        $identity_no = $this->request->data['identity_no']; //User Login Id

        $identity_no = bnToen($identity_no);
        $table_user = TableRegistry::get('Users');
        $user = $table_user->find()->where(['username' => $identity_no])->first();

        /* get employee personal info */
        $table_instance_emp_records = TableRegistry::get('EmployeeRecords');
        $employee_info = $table_instance_emp_records->get($user['employee_record_id']);

        /* get employee work history */
        $table_instance_emp_offices = TableRegistry::get('EmployeeOffices');
        $employee_office_records = $table_instance_emp_offices->getEmployeeRoleHistory($employee_info['id'],-1);

        /* Get user Statistics of Dak & Nothi */

        if (!empty($employee_office_records)) {
            foreach ($employee_office_records as $employee_office_record) {
                if ($employee_office_record['joining_date'] != null) {
                    $employee_office_record['joining_date'] = $employee_office_record['joining_date']->format("d/m/Y");
                }
                if ($employee_office_record['last_office_date'] != null) {
                    $employee_office_record['last_office_date'] = $employee_office_record['last_office_date']->format("d/m/Y");
                }
                $this->switchOffice($employee_office_record['office_id'],
                    'Office');
                $table_instance_dak_user = TableRegistry::get('DakUsers');
                $sum = 0;
                $sum += $table_instance_dak_user->getDakCountBy_employee_designation(DAK_CATEGORY_INBOX,
                    $employee_office_record['office_unit_organogram_id']);
                $sum += $table_instance_dak_user->getDakCountBy_employee_designation(DAK_CATEGORY_INBOX,
                    $employee_office_record['office_unit_organogram_id'],
                    DAK_NAGORIK);
                $dak_inbox = $sum;

                $sum = 0;
                $sum += $table_instance_dak_user->getDakCountOfOthers($employee_office_record['office_unit_organogram_id']);
                $total_dak = $sum;

                $tableNothiMovements = TableRegistry::get('NothiMasterMovements');
                $totalInboxNothi = $tableNothiMovements->getInboxCountBy_employee_designation($employee_office_record['office_unit_organogram_id']);
                $totalOutboxNothi = $tableNothiMovements->getOutboxCountBy_employee_designation($employee_office_record['office_unit_organogram_id']);
                if ($total_dak <= 0) {
                    $employee_office_record['dak_count'] = 0;
                } else {
                    $employee_office_record['dak_count'] = (int)(100 - (($dak_inbox
                                / ($dak_inbox + $total_dak)) * 100));
                }
                if ($totalOutboxNothi <= 0 || $totalInboxNothi <= 0) {
                    $employee_office_record['nothi_count'] = 0;
                } else {
                    $employee_office_record['nothi_count'] = (int)(100 - (($totalInboxNothi
                                / ($totalInboxNothi + $totalOutboxNothi)) * 100));
                }

                if ($employee_office_record['dak_count'] > 100) {
                    $employee_office_record['dak_count'] = 100;
                }
                if ($employee_office_record['nothi_count'] > 100) {
                    $employee_office_record['nothi_count'] = 100;
                }
                while ($employee_office_record['dak_count'] < 0) {
                    $employee_office_record['dak_count'] += 100;
                }
                while ($employee_office_record['nothi_count'] < 0) {
                    $employee_office_record['nothi_count'] += 100;
                }
            }
        }

        $data_item = array();
        $data_item['personal_info'] = $employee_info;
        $data_item['username'] = $identity_no;
        $data_item['office_records'] = count($employee_office_records) > 0 ? $employee_office_records
            : array();

        $this->response->body(json_encode($data_item));
        $this->response->type('application/json');
        return $this->response;
    }

    /**
     * Employee : new assignment
     */
    public function assignDesignation()
    {
        $request_data = $this->request->data;

        $table_instance = TableRegistry::get('EmployeeOffices');
        $assign = $table_instance->assignDesignation($request_data);

        if($assign){
            $this->response->body(json_encode(1));
        }
    else{
            $this->response->body(json_encode(0));
        }

        rtn:
        $this->response->type('application/json');
        return $this->response;
    }
    /**
     * Employee : new assignment
     */

    /**
     * Remove Assignment
     */
    public function removeAssignment()
    {
        $record_id = $this->request->data['id'];
        $table_instance = TableRegistry::get('EmployeeOffices');
//        $userTable = TableRegistry::get('Users');

        $this->response->body(json_encode(0));
        if (!empty($record_id)) {
            $unassign = $table_instance->unAssignDesignation($record_id,$this->request->data);
            $this->response->body(json_encode($unassign));
        }
        rtn:
        $this->response->type('application/json');
        return $this->response;
    }

    public function getOfficerInfo(){
        $designation_id = $this->request->data['designation_id'];

        $table_instance_eo = TableRegistry::get('EmployeeOffices');

        $designation_info = $table_instance_eo->getDesignationInfo($designation_id);

        $employee_info = array();

        if (!empty($designation_info)) {
            $table_instance_er = TableRegistry::get('EmployeeRecords');
            $employee_info = $table_instance_er->get($designation_info['employee_record_id']);
            $employee_english_info = $table_instance_eo->getEmployeeEnglishInfo($designation_info['employee_record_id'],$designation_info['office_id'],$designation_info['office_unit_id'],$designation_info['office_unit_organogram_id']);
            $employee_info['office_head'] = ($designation_info['office_head'] == 1?1:(empty($designation_info['show_unit']) ? 1 : 0));

            $employee_info['incharge_label'] = !empty($designation_info['incharge_label']) ? $designation_info['incharge_label'] : '';
            $employee_info['incharge_label_eng'] = !empty($designation_info['incharge_label']) ? $designation_info['incharge_label'] : '';
            $employee_info['office_name_eng'] = !empty($employee_english_info['office_name_eng']) ? $employee_english_info['office_name_eng'] : '';
            $employee_info['unit_name_eng'] = !empty($employee_english_info['unit_name_eng']) ? $employee_english_info['unit_name_eng'] : '';
            $employee_info['unit_name_bng'] = !empty($employee_english_info['unit_name_bng']) ? $employee_english_info['unit_name_bng'] : '';
            $employee_info['designation_name_eng'] = !empty($employee_english_info['designation_name_eng']) ? $employee_english_info['designation_name_eng'] : '';
        }
        $this->response->body(json_encode($employee_info));

        $this->response->type('application/json');

        return $this->response;
    }

    public function getOfficerInfoForSealSelection()
    {
        $this->layout = null;
        $table_instance_eo = TableRegistry::get('EmployeeOffices');

        $selected_designations = $this->request->data['selected_designations'];

        $employee_info_response_data = array();

        if (!empty($selected_designations)) {
            foreach ($selected_designations as $designation) {
                $employee_info = array();

                $designation_info = $table_instance_eo->getDesignationDetailsInfo($designation[0]);

                $employee_info['label'] = $designation_info['designation'];

                $employee_info['designation_id'] = $designation[0];

                $employee_record_id = 0;

                if (!empty($designation_info)) {
                    $employee_record_id = $designation_info['employee_record_id'];

                    $employee_info['employee_name'] = $designation_info['EmployeeRecords']['name_bng'];
                    $employee_info['employee_office_id'] = $designation_info['id'];
                    $employee_info['employee_record_id'] = $employee_record_id;
                }


                $employee_info_response_data[] = $employee_info;
            }
        }

        echo json_encode($employee_info_response_data);
        die;
    }

    public function employeeRecordList()
    {

        $auth = $this->Auth->user();

        if ($auth['user_role_id'] > 2) {
            $this->redirect(['controller' => 'dashboard', 'action' => 'dashboard']);
        }
    }

    public function employeeRecordListAjax()
    {
        $auth = $this->Auth->user();
        $this->set('auth',$auth);
        $this->layout = 'ajax';

        if ($auth['user_role_id'] > 2) {
            die("Invalid Request");
        }
        $table_instance_er = TableRegistry::get('EmployeeRecords');

        $conditions = [];
        if (!empty($this->request->data)) {
            if (!empty($this->request->data['office_id'])) {
                $conditions['EmployeeOffices.office_id'] = intval($this->request->data['office_id']);
            }
            if (!empty($this->request->data['name_bng'])) {
                $conditions['EmployeeRecords.name_bng LIKE'] = "%" . $this->request->data['name_bng'] . "%";
            }
            if (!empty($this->request->data['username'])) {
                $conditions['Users.username LIKE'] = "%" . $this->request->data['username'] . "%";
            }


            if (!empty($this->request->data['identity_no'])) {
                $this->request->data['identity_no'] = bnToen($this->request->data['identity_no']);
                $conditions['EmployeeOffices.identification_number LIKE'] = "%" . $this->request->data['identity_no'] . "%";
            }
            if (!empty($this->request->data['email'])) {
                $conditions['EmployeeRecords.personal_email LIKE'] = "%" . $this->request->data['email'] . "%";
            }
            if (!empty($this->request->data['mobile'])) {
                $conditions['EmployeeRecords.personal_mobile LIKE'] = "%" . $this->request->data['mobile'] . "%";
            }
        }

        if (empty($conditions)) {
            $conditions = 'EmployeeOffices.office_id is null';
        }

        $employees = $table_instance_er->getAllWithDetails($conditions);

        $this->set('employee_data', $this->paginate($employees));
    }

    public function employeeActive() {
        $users_table = TableRegistry::get('Users');
        $user = $users_table->get($this->request->data['user_id']);
        $user->active = 1;
        $users_table->save($user);
        $this->Flash->success(__('ব্যবহারকারীকে সচল করা হয়েছে।'));
        return $this->redirect($this->referer());
    }

    public function employeeRoleHistories()
    {

    }

    public function __changeDefaultRole()
    {
        $record_id = $this->request->data['id'];
        $table_instance = TableRegistry::get('EmployeeOffices');
        $entity = $table_instance->get($record_id);

        $this->response->body(json_encode(0));
        if (!empty($record_id)) {
            $employee_record_id = $entity->employee_record_id;

            $all_active_roles = $table_instance->find()->where(['employee_record_id' => $employee_record_id,
                'status' => '1']);
            foreach ($all_active_roles as $active_role) {
                $active_role->is_default_role = 0;
                if ($table_instance->save($active_role)) {

                }
            }

            $entity->is_default_role = 1;

            if ($table_instance->save($entity)) {
                $this->response->body(json_encode(1));
            }
        }
        $this->response->type('application/json');
        return $this->response;
    }

    /**
     * Email Activations List
     */
    public function __emailActivations()
    {
        $employee_email_activation_records = TableRegistry::get('UserActivationEmails');
        $query = $employee_email_activation_records->find('all')->order(['id' => 'DESC']);
        $this->set(compact('query'));
    }

    /**
     * SMS Activations List
     */
    public function __smsActivations()
    {
        $employee_sms_activation_records = TableRegistry::get('UserActivationSMS');
        $query = $employee_sms_activation_records->find('all')->order(['id' => 'DESC']);
        $this->set(compact('query'));
    }

    public function myProfile()
    {
        $employee_info = $this->getCurrentDakSection();
        $user = $this->Auth->user();
        $employee_own = $this->getLoggedEmployeeRecord();

        $this->set(compact('user'));
        $this->set(compact('employee_info'));
        $this->set(compact('employee_own'));
    }

    public function profileEdit($is_ajax = 'ajax')
    {
        $this->layout = null;

        $user = $this->Auth->user();

        $id = $user['employee_record_id'];

        $table_instance_emp_records = TableRegistry::get('EmployeeRecords');

        if (!empty($user['employee_record_id'])) {
            $employee_record = $table_instance_emp_records->get($id);
        } else {
            $employee_record = $table_instance_emp_records->newEntity();
        }

        if ($this->request->is(['post', 'put'])) {
            unset($this->request->data['id']);
            if (!empty($this->request->data['personal_mobile'])) {
                $this->request->data['personal_mobile'] = $this->BngToEng($this->request->data['personal_mobile']);
            }
            if(!empty($this->request->data['personal_email'])){
                $this->request->data['personal_email'] = trim(($this->request->data['personal_email']));
            }else{
                throw new \Exception(' ইমেইল দেয়া হয়নি');
            }

            try {
                $existing_nid = $employee_record->nid;
                $update_profile_data = $table_instance_emp_records->patchEntity($employee_record,
                    $this->request->data, ['validate' => 'edit']);

                $employee_record->date_of_birth = date("Y-m-d 00:00:00",
                    strtotime($this->request->data['date_of_birth']));

                if (mb_strlen($employee_record->nid) == 10) {
                    // 10 digit nid verification
                    $employee_record->nid = $existing_nid;
                } else {
                    if (!empty($employee_record->nid)) {
                        $check_NID_DOB = $this->checkNID($employee_record->date_of_birth,
                            $employee_record->nid);
                        if ($check_NID_DOB > 0) {
                            throw new \Exception(' জাতীয় পরিচয়পত্রের তথ্য   মিলছে না।  Error: ' . $check_NID_DOB);
                            // return $this->redirect(['action' => 'add']);
                        }
                    } else {
                        throw new \Exception(' জাতীয় পরিচয়পত্র নম্বর ১৭ সংখ্যার হতে হবে।   ');
                        // return $this->redirect(['action' => 'add']);
                    }
                    $employee_record->nid = bnToen($employee_record->nid);
                }
                if ($employee_record = $table_instance_emp_records->save($employee_record)) {
                    $usersTable = TableRegistry::get('Users');
                    $userInfo = $usersTable->get($user['id']);
                    $userInfo->employee_record_id = $employee_record->id;
                    $userInfo->user_alias = !empty($this->request->data['user_alias'])?$this->request->data['user_alias']:$userInfo->username;

                    if ($usersTable->checkAlias($userInfo)) {
                        $potrojariGroupsMigTable = TableRegistry::get('PotrojariGroupsUsers');
                        $potrojariGroupsMigTable->updatePotrojariGroupProfileDetails($employee_record->id,(!empty($this->request->data['name_eng'])?$this->request->data['name_eng']:''),$this->request->data['name_bng'],$this->request->data['personal_email']);
                        $usersTable->save($userInfo);
                        if($this->request->is('ajax')){
                            echo 1;
                            die;
                        }else{
                            return $this->redirect(['controller'=>'dashboard','action'=>'dashboard']);
                        }
                    }
                } else {
                    if($this->request->is('ajax')) {
                        $this->response->type('application/json');
                        if (is_array($update_profile_data->errors())) {
                            $error = ($update_profile_data->errors());
                            foreach ($error as $k => $v) {
                                $verro = array_values($v);
                                $this->response->body(json_encode(['error' => $verro[0]]));
                            }
                        } else {
                            $this->response->body(json_encode($update_profile_data->errors()));
                        }
                        return $this->response;
                    }else{
                        $this->Flash->error(__('আপনার প্রোফাইলে ইমেল তথ্য দেওয়া হয়নি। দয়া করে ইমেইল তথ্য দিন'));
                        return $this->redirect(['controller'=>'dashboard','action'=>'dashboard']);
                    }
                }
            } catch (\Exception $ex) {
                if($this->request->is('ajax')) {
                    $this->response->type('application/json');
                    $this->response->body(json_encode(['error' => $ex->getMessage()]));
                    return $this->response;
                }else{
                    $this->Flash->error($ex->getMessage());
                    return $this->redirect(['controller'=>'dashboard','action'=>'dashboard']);
                }
            }
        }
        $this->set('entity', $employee_record);
    }

    public function profileView()
    {
        $this->layout = null;
        $user = $this->Auth->user();

        if (!empty($user['employee_record_id'])) {
            $employee_record = $this->EmployeeRecords->get($user['employee_record_id']);
        } else {
            $employee_record = $this->EmployeeRecords->newEntity();
        }


        $this->set(compact('employee_record'));
    }

    public function passwordChange()
    {
        $this->layout = null;
        $user = $this->Auth->user();

        $id = $user['employee_record_id'];

        $changePassOnly = false;
        if (empty($user['employee_record_id'])) {
            $changePassOnly = true;
            $id = $user['id'];
        }

        $userTable = TableRegistry::get('Users');

        if ($changePassOnly) {

            $userInformation = $userTable->get($id);
            if (!empty($this->request->data) && $this->request->is('post')) {
                if (!empty($this->request->data['current_password']) && !empty($this->request->data['password'])
                    && $this->request->data['password'] == $this->request->data['cpassword']
                ) {
                    try {
                        $userInformation = $userTable->patchEntity($userInformation,
                            [
                                'current_password' => $this->request->data['current_password'],
                                'password' => $this->request->data['password'],
                                'cpassword' => $this->request->data['cpassword']
                            ], ['validate' => 'password']);

                        $errors = $userInformation->errors();
                        if (empty($errors)) {
                            if ($this->request->data['password'] == DEFAULT_PASSWORD) {
                                $this->Flash->error('পাসওয়ার্ড পরিবর্তন করা সম্ভব হচ্ছে না। এই পাসওয়ার্ড   গ্রহণযোগ্য  নয়');
                                return $this->redirect(['controller' => 'EmployeeRecords',
                                    'action' => 'passwordChange']);
                            }
                            $userInformation->force_password_change = 0;
                            $userInformation->last_password_change = date("Y-m-d H:i:s");
                            $userTable->save($userInformation);
                            $this->Auth->setUser($userInformation->toArray());
                            $this->Flash->success('পাসওয়ার্ড পরিবর্তন করা হয়েছে');
                            return $this->redirect(['controller' => 'EmployeeRecords',
                                'action' => 'myProfile', '?' => ['page' => 'profileppassword']]);
                        } else {
                            if (!empty($userInformation->errors()['password']['length'])) {
                                $this->Flash->error($userInformation->errors()['password']['length']);
                            } else if (!empty($userInformation->errors()['cpassword']['message'])) {
                                $this->Flash->error($userInformation->errors()['cpassword']['cpassword']);
                            } else if (!empty($userInformation->errors()['current_password']['custom'])) {
                                $this->Flash->error($userInformation->errors()['current_password']['custom']);
                            } else if (!empty($userInformation->errors()['password']['custom'])) {
                                $this->Flash->error($userInformation->errors()['password']['custom']);
                            } else {
                                $this->Flash->error('পাসওয়ার্ড পরিবর্তন করা সম্ভব হচ্ছে না');
                            }
                            return $this->redirect(['controller' => 'EmployeeRecords',
                                'action' => 'passwordChange']);
                        }
                    } catch (\Exception $ex) {

                        $this->Flash->error('পাসওয়ার্ড পরিবর্তন করা সম্ভব হচ্ছে না');
                        return $this->redirect(['controller' => 'EmployeeRecords',
                            'action' => 'passwordChange']);
                    }
                }
            }
        } else {
            $userInformation = $userTable->find()->where(['employee_record_id' => $id])->first();

            if (!empty($this->request->data) && $this->request->is('post')) {

                if (!empty($this->request->data['current_password']) && !empty($this->request->data['password'])
                    && $this->request->data['password'] == $this->request->data['cpassword']
                ) {
                    try {
                        $employeeinfo = $this->EmployeeRecords->get($id);

                        $userInformation = $userTable->patchEntity($userInformation,
                            [
                                'current_password' => $this->request->data['current_password'],
                                'password' => $this->request->data['password'],
                                'cpassword' => $this->request->data['cpassword']
                            ], ['validate' => 'password']);


                        $errors = $userInformation->errors();
                        if (empty($errors)) {
                            $isSent = 0;
                            if ($this->request->data['password'] == DEFAULT_PASSWORD) {
                                $this->Flash->error('পাসওয়ার্ড পরিবর্তন করা সম্ভব হচ্ছে না। এই পাসওয়ার্ড   গ্রহণযোগ্য  নয়');
                                return $this->redirect(['controller' => 'EmployeeRecords',
                                    'action' => 'passwordChange']);
                            }

                            $userInformation->force_password_change = 0;
                            $userInformation->last_password_change = date("Y-m-d H:i:s");
                            if ($userInformation = $userTable->save($userInformation)) {
                                $this->Auth->setUser($userInformation->toArray());
                                if (!empty($employeeinfo['personal_email'])) {
//                                    $this->SendEmailMessage($employeeinfo['personal_email'],
//                                        "আপনার পাসওয়ার্ড পরিবর্তন হয়েছে। <br/>ধন্যবাদ",
//                                        __("Password Change"));

                                    
                                    $employeeOffices = TableRegistry::get('EmployeeOffices');
                                    $offices = $employeeOffices->getEmployeeOfficeRecords($id);
                                    $email_options['email_type'] = EMAIL_USER;
                                    $email_options['subject'] = "Password Change";
                                    $email_options['office_id'] = $offices[0]['office_id'];
                                    $email_options['office_name'] = $offices[0]['office_name'];
                                    $email_message = "আপনার পাসওয়ার্ড পরিবর্তন হয়েছে। <br/>ধন্যবাদ";
                                    
                                    $this->SendEmailMessage($employeeinfo['personal_email'],$email_message, $email_options);
                                    
                                    $isSent = 1;
                                }
                                if (!empty($employeeinfo['personal_mobile'])) {
                                    $this->SendSmsMessage($employeeinfo['personal_mobile'],
                                        "Dear " . $employeeinfo['name_eng'] . "\n\nYour password is changed.\nThank you");

                                    $isSent = 1;
                                }
                                $this->Flash->success('পাসওয়ার্ড পরিবর্তন করা হয়েছে');
                                return $this->redirect(['controller' => 'EmployeeRecords',
                                    'action' => 'myProfile', '?' => ['page' => 'profileppassword']]);
                            } else {
                                $this->Flash->error('পাসওয়ার্ড পরিবর্তন করা সম্ভব হচ্ছে না');
                                return $this->redirect(['controller' => 'EmployeeRecords',
                                    'action' => 'passwordChange']);
                            }
                        } else {
                            if (!empty($userInformation->errors()['password']['length'])) {
                                $this->Flash->error($userInformation->errors()['password']['length']);
                            } else if (!empty($userInformation->errors()['cpassword']['message'])) {
                                $this->Flash->error($userInformation->errors()['cpassword']['cpassword']);
                            } else if (!empty($userInformation->errors()['current_password']['custom'])) {
                                $this->Flash->error($userInformation->errors()['current_password']['custom']);
                            } else if (!empty($userInformation->errors()['password']['custom'])) {
                                $this->Flash->error($userInformation->errors()['password']['custom']);
                            } else {
                                $this->Flash->error('পাসওয়ার্ড পরিবর্তন করা সম্ভব হচ্ছে না');
                            }
                            return $this->redirect(['controller' => 'EmployeeRecords',
                                'action' => 'passwordChange']);
                        }
                    } catch (\Exception $ex) {

                        $this->Flash->error('পাসওয়ার্ড পরিবর্তন করা সম্ভব হচ্ছে না');
                        return $this->redirect(['controller' => 'EmployeeRecords',
                            'action' => 'passwordChange']);
                    }
//                    return $this->redirect(['controller' => 'EmployeeRecords', 'action' => 'myProfile']);
                }
//               return $this->redirect(['controller' => 'EmployeeRecords', 'action' => 'myProfile']);
            }
        }
    }

    public function profileUpload()
    {
        $this->layout = null;
        $user = $this->Auth->user();
        $employee_record = '';
        if (!empty($user['employee_record_id'])) {
            $employee_record = $this->EmployeeRecords->get($user['employee_record_id']);
        }

        $this->set(compact('employee_record'));

        $this->set(compact('user'));

        if ($this->request->is("post")) {
            if (!empty($this->request->data['img_data'])) {
                $destination = FILE_FOLDER_DIR . 'Personal/profile/' . $user['username'] . '.png';
                $ifp = fopen($destination, "wb");
                $data = explode(',', $this->request->data['img_data']);

                if(!empty($data[0])){
                    $ext = explode(';',$data[0]);
                    if($ext[0] != 'data:image/png' && $ext[0] != 'data:image/jpeg' && $ext[0] != 'data:image/jpg'){
                        fclose($ifp);
                        $this->Flash->error("দুঃখিত! ছবি  পরিবর্তন করা সম্ভব হচ্ছে না");
                        echo 0;
                    }
                }else{
                    fclose($ifp);
                    $this->Flash->error("দুঃখিত! ছবি  পরিবর্তন করা সম্ভব হচ্ছে না");
                    echo 0;
                }

                fwrite($ifp, base64_decode($data[1]));
                fclose($ifp);
                $this->Flash->success("ছবি পরিবর্তন করা হয়েছে");
                echo 1;
            } else {
                $this->Flash->error("দুঃখিত! ছবি  পরিবর্তন করা সম্ভব হচ্ছে না");
                echo 0;
            }
        }
    }

    function updateUsername()
    {

    }

    public function emoloyeeByOffice()
    {
        $group = isset($this->request->data['group']) ? $this->request->data['group']
            : '';
        $officeid = $this->request->data['office_id'];
        $officerid = isset($this->request->data['officer_id']) ? $this->request->data['officer_id']
            : 0;

        $table_employee_office = TableRegistry::get('EmployeeOffices');
        $table_employee_records = TableRegistry::get('EmployeeRecords');
        $table_user = TableRegistry::get('Users');
        $table_office_unit = TableRegistry::get('OfficeUnits');

        $query = $table_employee_office->find()->select([
            'office_unit_id',
            'office_unit_organogram_id',
            'designation',
            'employee_record_id',
            "unit_name_bng" => 'OfficeUnits.unit_name_bng',
            "id" => 'EmployeeRecords.id',
            "personal_mobile" => 'EmployeeRecords.personal_mobile',
            "personal_email" => 'EmployeeRecords.personal_email',
            "name_bng" => 'EmployeeRecords.name_bng',
            "name_eng" => 'EmployeeRecords.name_eng',
            "identity_no" => 'EmployeeRecords.identity_no',
            "is_cadre" => 'EmployeeRecords.is_cadre',
            "username" => 'Users.username',
            "role_id" => "Users.id",
            "is_admin" => "OfficeUnitOrganograms.is_admin",
        ])->join([
            'OfficeUnitOrganograms' => [
                'table' => 'office_unit_organograms',
                'type' => 'left',
                'conditions' => 'EmployeeOffices.office_unit_organogram_id = OfficeUnitOrganograms.id'
            ],
            'OfficeUnits' => [
                'table' => 'office_units',
                'type' => 'left',
                'conditions' => 'EmployeeOffices.office_unit_id = OfficeUnits.id'
            ],
            'EmployeeRecords' => [
                'table' => 'employee_records',
                'type' => 'inner',
                'conditions' => 'EmployeeRecords.id = EmployeeOffices.employee_record_id'
            ],
            'Users' => [
                'table' => 'users',
                'type' => 'inner',
                'conditions' => 'Users.employee_record_id = EmployeeOffices.employee_record_id'
            ]
        ])->where(['EmployeeOffices.office_id' => $officeid, 'EmployeeOffices.status' => 1]);

        if (!empty($officerid)) {
            $query = $query->where(['EmployeeOffices.employee_record_id' => $officerid]);
        }

        if (!empty($group)) {
            $query = $query->group([$group]);
        }
        $results = $query->order(['EmployeeOffices.designation_level ASC, EmployeeOffices.designation_sequence ASC'])->toArray();

        echo json_encode($results);
        die;
    }
    public function emoloyeeDesignationByOffice()
    {
        $group = isset($this->request->data['group']) ? $this->request->data['group'] : '';
        $officeid = $this->request->data['office_id'];
        $officerid = isset($this->request->data['officer_id']) ? $this->request->data['officer_id'] : 0;

        $table_employee_office = TableRegistry::get('EmployeeOffices');
        $table_employee_records = TableRegistry::get('EmployeeRecords');
        $table_user = TableRegistry::get('Users');
        $table_office_unit = TableRegistry::get('OfficeUnits');

        $query = $table_employee_office->find()->select([
                'office_unit_id',
                'office_unit_organogram_id',
                'designation',
                'employee_record_id',
                "unit_name_bng" => 'OfficeUnits.unit_name_bng',
                "id" => 'EmployeeRecords.id',
                "personal_mobile" => 'EmployeeRecords.personal_mobile',
                "personal_email" => 'EmployeeRecords.personal_email',
                "name_bng" => 'EmployeeRecords.name_bng',
                "name_eng" => 'EmployeeRecords.name_eng',
                "identity_no" => 'EmployeeRecords.identity_no',
                "is_cadre" => 'EmployeeRecords.is_cadre',
                "username" => 'Users.username',
                "role_id" => "Users.id",
                "is_admin" => "OfficeUnitOrganograms.is_admin",
                "employee_office_id" => "EmployeeOffices.id",
            ])->join([
                'OfficeUnitOrganograms' => [
                    'table' => 'office_unit_organograms',
                    'type' => 'left',
                    'conditions' => 'EmployeeOffices.office_unit_organogram_id = OfficeUnitOrganograms.id'
                ],
                'OfficeUnits' => [
                    'table' => 'office_units',
                    'type' => 'left',
                    'conditions' => 'EmployeeOffices.office_unit_id = OfficeUnits.id'
                ],
                'EmployeeRecords' => [
                    'table' => 'employee_records',
                    'type' => 'inner',
                    'conditions' => 'EmployeeRecords.id = EmployeeOffices.employee_record_id'
                ],
                'Users' => [
                    'table' => 'users',
                    'type' => 'inner',
                    'conditions' => 'Users.employee_record_id = EmployeeOffices.employee_record_id'
                ]
            ])->where(['EmployeeOffices.office_id' => $officeid, 'EmployeeOffices.status' => 1]);

        if (!empty($officerid)) {
            $query = $query->where(['EmployeeOffices.employee_record_id' => $officerid]);
        }

        if (!empty($group)) {
            $query = $query->group([$group]);
        }
        $results = $query->order(['EmployeeOffices.employee_record_id ASC, EmployeeOffices.designation_level ASC, EmployeeOffices.designation_sequence ASC'])->toArray();

        echo json_encode($results);
        die;
    }

    public function signatureUpload()
    {
        $this->layout = null;
        $user = $this->Auth->user();
        $employee_record = $this->EmployeeRecords->get($user['employee_record_id']);

        $this->set(compact('employee_record'));
        $this->set(compact('user'));

        if ($this->request->is("post")) {
            if (!empty($this->request->data['img_data'])) {
                $ret = 1;
                if(
                    $this->request->data['img_data']=='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJYAAAAyCAYAAAC+jCIaAAAA8klEQVR4Xu3SMQ0AAAzDsJU/6aHI5wLoEXlnCgQFFny6VODAgiApAFaS1SlYDCQFwEqyOgWLgaQAWElWp2AxkBQAK8nqFCwGkgJgJVmdgsVAUgCsJKtTsBhICoCVZHUKFgNJAbCSrE7BYiApAFaS1SlYDCQFwEqyOgWLgaQAWElWp2AxkBQAK8nqFCwGkgJgJVmdgsVAUgCsJKtTsBhICoCVZHUKFgNJAbCSrE7BYiApAFaS1SlYDCQFwEqyOgWLgaQAWElWp2AxkBQAK8nqFCwGkgJgJVmdgsVAUgCsJKtTsBhICoCVZHUKFgNJAbCSrE4fJ1kAM+6iqU8AAAAASUVORK5CYII=' ||
                    $this->request->data['img_data'] == 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJYAAAAyCAYAAAC+jCIaAAAA+UlEQVR4Xu3SsQ0AIBADMdh/Zx6JGbjOGSDFyXvOzDIFPhfYYH0u6u4VAAuEpABYSVanYDGQFAAryeoULAaSAmAlWZ2CxUBSAKwkq1OwGEgKgJVkdQoWA0kBsJKsTsFiICkAVpLVKVgMJAXASrI6BYuBpABYSVanYDGQFAAryeoULAaSAmAlWZ2CxUBSAKwkq1OwGEgKgJVkdQoWA0kBsJKsTsFiICkAVpLVKVgMJAXASrI6BYuBpABYSVanYDGQFAAryeoULAaSAmAlWZ2CxUBSAKwkq1OwGEgKgJVkdQoWA0kBsJKsTsFiICkAVpLVKVgMJAXASrI6vYWHxzkKYSPgAAAAAElFTkSuQmCC'
                 ){
                    $this->Flash->error("দুঃখিত! স্বাক্ষর   পরিবর্তন করা সম্ভব হচ্ছে না। এরর কোডঃ ১");
                    $ret = 0;
                }else {
                    $main_file = $destination = FILE_FOLDER_DIR . 'Personal/signature/' . $user['username'] . time() . '.png';
                    $ifp = fopen($destination, "wb");
                    $data = explode(',', $this->request->data['img_data']);

                    if (!empty($data[0])) {
                        $ext = explode(';', $data[0]);
                        if ($ext[0] != 'data:image/png' && $ext[0] != 'data:image/jpeg' && $ext[0] != 'data:image/jpg') {
                            fclose($ifp);
                            $this->Flash->error("দুঃখিত! স্বাক্ষর পরিবর্তন করা সম্ভব হচ্ছে না। এরর কোডঃ ২");
                            $ret = 0;
                        }
                    } else {
                        fclose($ifp);
                        $this->Flash->error("দুঃখিত! স্বাক্ষর পরিবর্তন করা সম্ভব হচ্ছে না। এরর কোডঃ ৩");
                        $ret = 0;
                    }

                    if($ret === 1) {

                        fwrite($ifp, base64_decode($data[1]));
                        fclose($ifp);

                        $userSignaturesTable = TableRegistry::get('UserSignatures');
                        $previous = $userSignaturesTable->getSignature($user['username']);
                        $this->resizeFiles($destination, 80);
                        $data = file_get_contents($main_file);
                        $dataUri = base64_encode($data);

                        if (!$this->check_base64_image($dataUri)) {
                            $this->Flash->error("দুঃখিত! স্বাক্ষর পরিবর্তন করা সম্ভব হচ্ছে না। এরর কোডঃ ৪");
                            $ret = 0;
                        }else{
                            $entity = $userSignaturesTable->newEntity();
                            $entity->username = $user['username'];
                            $entity->signature_file = $destination;
                            $entity->previous_signature = $previous['signature_file'];
                            $entity->encode_sign = 'data:image/png;base64,'.$dataUri;
                            $userSignaturesTable->save($entity);
                            $this->Flash->success("স্বাক্ষর  পরিবর্তন করা হয়েছে");
                            $ret = 1;
                        }
                    }
                }
                echo $ret;
            } else {
                $this->Flash->error("দুঃখিত! স্বাক্ষর   পরিবর্তন করা সম্ভব হচ্ছে না। এরর কোডঃ ৫");
                $ret = 0;
                echo $ret;
            }
        }
    }

    public function userDetails($id,$unit_id = 0)
    {
        $employee_office_table = TableRegistry::get('EmployeeOffices');
        $employee_user_table = TableRegistry::get('Users');
            $employee_office_info = $employee_office_table->find()->select('office_unit_id')->where(['employee_record_id'=> $id,'status' => 1])->first();
        if(!empty($unit_id)){
            $office_unit_table = TableRegistry::get('office_units');
            $office_unit_info = $office_unit_table->get($unit_id);
        }
       else if(!empty($employee_office_info['office_unit_id'])){
            $office_unit_table = TableRegistry::get('office_units');
            $office_unit_info = $office_unit_table->get($employee_office_info['office_unit_id']);
        }
        $userinfo = $this->EmployeeRecords->get($id);
        $users = $employee_user_table->find()->where(['employee_record_id'=>$id])->first();

        if(!empty($office_unit_info['email'])){
            $userinfo['personal_email'] = $office_unit_info['email'];
        }else{
            $userinfo['personal_email'] = '...';
        }
        if(!empty($office_unit_info['phone'])){
            $userinfo['personal_mobile'] = enTobn($office_unit_info['phone']);
        }else{
            $userinfo['personal_mobile'] = '...';
        }
        if(!empty($office_unit_info['fax'])){
            $userinfo['fax'] = enTobn($office_unit_info['fax']);
        }else{
            $userinfo['fax'] = '...';
        }
        $userinfo['username'] = $users['username'];
        $userinfo['en_username'] = sGenerateToken(['file'=>$userinfo['username']],['exp'=>time() + 60*300]);

        echo json_encode($userinfo);

        die;
    }

    public function ownOfficeAdd()
    {
        $user = $this->getCurrentDakSection();

        $this->set('office_id', $user['office_id']);
        $table_instance_grade = TableRegistry::get('EmployeeGrades');
        $table_instance_rank = TableRegistry::get('EmployeeRanks');
        $table_instance_batch = TableRegistry::get('EmployeeBatches');
        $table_instance_cadre = TableRegistry::get('EmployeeCadres');
        $table_instance = TableRegistry::get('OfficeInchargeTypes');
        $table_instance_emp_records = TableRegistry::get('EmployeeRecords');
        $table_emp_office_records = TableRegistry::get('EmployeeOffices');
        $table_org_records = TableRegistry::get('OfficeUnitOrganograms');
        $user_entity_table = TableRegistry::get('Users');

        if ($this->request->is('get')) {
//            $data_items = $table_instance_grade->find('list')->toArray();
//            $this->set('serviceGrades', $data_items);
//
//            $employeeRanks = $table_instance_rank->find('list')->toArray();
//            $this->set('serviceRanks', $employeeRanks);
//
//            $employeeBatches = $table_instance_batch->find('list')->toArray();
//            $this->set('employeeBatches', $employeeBatches);
//
//            $employeeCadres = $table_instance_cadre->find('list')->toArray();
//            $this->set('employeeCadres', $employeeCadres);
            
        }
        $employee_record = $table_instance_emp_records->newEntity();
            


        if ($this->request->is('post')) {

            $conn = ConnectionManager::get('projapotiDb');
            try {
                $conn->begin();

                $this->request->data['identity_no'] = isset($this->request->data['identity_no'])
                    ? trim($this->request->data['identity_no']) : '';
                $this->request->data['identity_no'] = bnToen($this->request->data['identity_no']);
                if (!empty($this->request->data['identity_no']) && strlen($this->request->data['identity_no']) > 11) {
                    throw new \Exception('দুঃখিত! পরিচিতি নম্বর ১১ ডিজিটের বেশি হতে পারবে না। দয়া করে পুনরায় চেষ্টা করুন।');
                }
                $this->request->data['is_cadre'] = $table_instance_emp_records->getCadreId($this->request->data);

                if ($this->request->data['is_cadre'] == 0) {
                    throw new \Exception(__('Sorry! You did not provide Identity No'));
                }

                $username = $table_instance_emp_records->createUserId($this->request->data);

                if (empty($username)) {
                    throw new \Exception(__('Sorry! You did not provide Identity No'));
                }

                $this->request->data['username'] = $username;

                if (empty($this->request->data['office_unit_organogram_id'])) {
                    throw new \Exception('দুঃখিত! কর্মকর্তার অফিসের পদবি দেয়া হয়নি');
                }

                $this->request->data['name_eng'] = str_replace(',', '.',
                    $this->request->data['name_eng']);
                $this->request->data['name_bng'] = str_replace(',', '.',
                    $this->request->data['name_bng']);

                $this->request->data['name_eng'] = str_replace(';', '.',
                    $this->request->data['name_eng']);
                $this->request->data['name_bng'] = str_replace(';', '.',
                    $this->request->data['name_bng']);

                if (empty($this->request->data['name_bng'])) {
                    throw new \Exception(' দয়া করে নিজের নামটি বাংলায় দিন ');
                    //return $this->redirect(['action' => 'ownOfficeAdd']);
                }
                if (empty($this->request->data['personal_mobile']) || mb_strlen($this->request->data['personal_mobile']) != 11) {
                    throw new \Exception(' দয়া করে মোবাইল নাম্বারটি দিন ');
                    //return $this->redirect(['action' => 'ownOfficeAdd']);
                }
                $active = 0;
                if (!empty($this->request->data['nid'])) {

                    if (mb_strlen($this->request->data['nid']) != 17 && mb_strlen($this->request->data['nid']) != 10) {
                        throw new \Exception(' জাতীয় পরিচয়পত্র নম্বর ১০ অথবা ১৭ সংখ্যার হতে হবে।');
                    } else {
                        $employee_record_table = TableRegistry::get('EmployeeRecords');
                        // duplicate check using name, fathers_name, mothers_name, date_of_birth
                        $employee_record_check = $employee_record_table->find()->where([
                            'name_bng' => $this->request->data['name_bng'],
                            'father_name_bng' => $this->request->data['father_name_bng'],
                            'mother_name_bng' => $this->request->data['mother_name_bng'],
                            'date_of_birth' => $this->request->data['date_of_birth'],
                        ])->count();

                        if ($employee_record_check != 0) {
                            throw new \Exception(' এই ব্যবহারকারী পূর্বে অন্তর্ভূক্ত হয়েছে। ');
                        }

                        $employee_record_check = $employee_record_table->find()->where(['nid' => $this->request->data['nid']])->count();
                        if ($employee_record_check != 0) {
                            throw new \Exception(' জাতীয় পরিচয়পত্র নম্বর পূর্বে ব্যবহৃত হয়েছে। ');
                        } else {
                            if (mb_strlen($this->request->data['nid']) == 17) {
                                $check_NID_DOB = $this->checkNID($this->request->data['date_of_birth'], $this->request->data['nid']);
                                if ($check_NID_DOB > 0) {
                                    throw new \Exception(' জাতীয় পরিচয়পত্রের তথ্য মিলছে না। Error: ' . $check_NID_DOB);
                                }
                                $active = 1;
                            } elseif (mb_strlen($this->request->data['nid']) == 10) {
                                $active = 1;
                            }
                        }
                    }
                } else {
                    throw new \Exception(' জাতীয় পরিচয়পত্র নম্বর অবশ্যই দিতে হবে।   ');
                }

                $this->request->data['nid'] = bnToen($this->request->data['nid']);

                $userExist = $user_entity_table->find()->select(['username'])->where(['username' => $username])->orWhere(['user_alias' => $username])->first();

                if (!empty($userExist)) {
                    throw new \Exception('দুঃখিত! এই লগইন আইডি ব্যবহার হয়ে গেছে');
                }

                $this->request->data['date_of_birth'] = date("Y-m-d 00:00:00",strtotime($this->request->data['date_of_birth']));

                $employee_record = $table_instance_emp_records->patchEntity($employee_record,
                    $this->request->data,['validate'=>'add']);

                $employee_record['name_bng'] = str_replace(';', '.',
                    $employee_record['name_bng']);
                $employee_record['name_bng'] = str_replace(',', '.',
                    $employee_record['name_bng']);
                //$employee_record['id'] = $employee_record_id['id'] + 1;
                $employee_record['status'] = 1;
                $employee_record['created_by'] = $this->Auth->user('id');
                $employee_record['modified_by'] = $this->Auth->user('id');
                $employee_record['date_of_birth'] = date("Y-m-d 00:00:00",strtotime($this->request->data['date_of_birth']));

                $getError = $employee_record->errors();
                if (empty($getError)) {
                    $employee_record = $table_instance_emp_records->save($employee_record);
                    $user_entity = $user_entity_table->newEntity();
                    $user_entity->username = $username;
                    $user_entity->active = $active;
                    $user_entity->is_email_verified = 1;
                    $user_entity->email_verify_code = '';
                    $user_entity->ssn = '';

                    $this->request->data['password'] = trim($this->request->data['password']);

                    $user_entity->password = (empty($this->request->data['password'])
                        ? DEFAULT_PASSWORD : $this->request->data['password']);

                    $user_type_entity = TableRegistry::get('UserRoles');
                    $user_type_id = $user_type_entity->find()->where(['name' => 'Employee'])->first()->id;

                    $user_entity->user_role_id = $user_type_id;
                    $user_entity->user_status = 1;
                    $user_entity->employee_record_id = $employee_record->id;
                    $user_entity->user_alias = $username;
                    $user_entity->created_by = $this->Auth->user('id');
                    $user_entity->modified_by = $this->Auth->user('id');

                    $user_entity_table->save($user_entity);

                    $employeeOffices = $table_emp_office_records->newEntity();

                    $employeeOffices->employee_record_id = $employee_record->id;
                    $employeeOffices->identification_number = isset($this->request->data['identity_no'])
                        ? trim($this->request->data['identity_no']) : 0;
                    $employeeOffices->office_id = $user['office_id'];
                    $employeeOffices->office_unit_id = $this->request->data['office_unit_id'];
                    $employeeOffices->office_unit_organogram_id = $this->request->data['office_unit_organogram_id'];

                    $designation = $table_org_records->get($this->request->data['office_unit_organogram_id']);
                    $employeeOffices->designation = $designation['designation_bng'];
                    $employeeOffices->designation_level = $designation['designation_level'];
                    $employeeOffices->designation_sequence = $designation['designation_sequence'];
                    $employeeOffices->is_default_role = 1;
                    $employeeOffices->incharge_label = isset($this->request->data['incharge_label'])?$this->request->data['incharge_label']:'';
                    $employeeOffices->status = 1;
                    $employeeOffices['joining_date'] = !empty($this->request->data['joining_date'])?date("Y-m-d",strtotime($this->request->data['joining_date'])):date("Y-m-d");

                    $table_emp_office_records->save($employeeOffices);


                    $conn->commit();
                    $this->Flash->success('কর্মকর্তা তৈরি এবং পদবি দেয়া হয়েছে');
                    return $this->redirect(['controller'=>'OfficeEmployees', 'action' => 'index']);

                }
            } catch (\Exception $ex) {
                $this->set('entity', $this->request->data);
                //$this->Flash->error('দুঃখিত! কর্মকর্তার তথ্য সঠিকভাবে দেয়া হয়নি। <br>->' . $this->makeEncryptedData($ex->getMessage()));
                $this->Flash->error($ex->getMessage());
                $conn->rollback();
            }

        }
        $this->set('entity', $employee_record);
        $officeInchargeTypes = $table_instance->find('list',['keyField' => 'name_bng','valueField' => 'name_bng'])->toArray();
        $this->set(compact('officeInchargeTypes'));

    }

    public function getUserProfilePhoto($username = '', $encode = 0)
    {

        return $this->getProfile($username, $encode, 0);
    }

    public function getUserSignature($username = '', $encode = 0, $date = null)
    {

        return $this->getSignature($username, $encode, 0, false, $date);
    }

    public function resetPassword()
    {

        $this->layout = null;
        $user = $this->Auth->user();

        $id = $user['employee_record_id'];

        $changePassOnly = false;
        if (empty($user['employee_record_id'])) {
            $changePassOnly = true;
            $id = $user['id'];
        }

        $userTable = TableRegistry::get('Users');

        if ($changePassOnly) {

            $userInformation = $userTable->get($id);

            if (!empty($this->request->data) && $this->request->is('post')) {

                if (!empty($this->request->data['password'])) {
                    try {

                        $userInformation->password = ($this->request->data['password']);
                        $has_error = $userInformation->errors();
                        if (empty($has_error)) {
                            if ($this->request->data['password'] == DEFAULT_PASSWORD) {
                                $this->Flash->error('পাসওয়ার্ড পরিবর্তন করা সম্ভব হচ্ছে না। এই পাসওয়ার্ড   গ্রহণযোগ্য  নয়');
                                return $this->redirect(['controller' => 'EmployeeRecords',
                                    'action' => 'resetPassword']);
                            }
                            $userInformation->force_password_change = 0;
                            $userInformation->last_password_change = date("Y-m-d H:i:s");

                            if ($userInformation = $userTable->save($userInformation)) {
                                unset($userInformation->password);
                                $this->Auth->setUser($userInformation->toArray());
                                $this->Flash->success('পাসওয়ার্ড পরিবর্তন করা হয়েছে');
                                unset($userInformation->password);
                                $this->Auth->setUser($userInformation->toArray());
                                return $this->redirect(['controller' => 'dashboard',
                                    'action' => 'dashboard']);
                            } else {
                                $this->Flash->error('পাসওয়ার্ড পরিবর্তন করা সম্ভব হচ্ছে না');
                                return $this->redirect(['controller' => 'EmployeeRecords',
                                    'action' => 'resetPassword']);
                            }
                        } else {
                            if (!empty($userInformation->errors()['password']['length'])) {
                                $this->Flash->error($userInformation->errors()['password']['length']);
                            } else if (!empty($userInformation->errors()['cpassword']['message'])) {
                                $this->Flash->error($userInformation->errors()['cpassword']['cpassword']);
                            } else if (!empty($userInformation->errors()['current_password']['custom'])) {
                                $this->Flash->error($userInformation->errors()['current_password']['custom']);
                            } else {
                                $this->Flash->error('পাসওয়ার্ড পরিবর্তন করা সম্ভব হচ্ছে না');
                            }
                            return $this->redirect(['controller' => 'EmployeeRecords',
                                'action' => 'resetPassword']);
                        }
                    } catch (Exception $ex) {

                        $this->Flash->error('পাসওয়ার্ড পরিবর্তন করা সম্ভব হচ্ছে না');
                        return $this->redirect(['controller' => 'EmployeeRecords',
                            'action' => 'resetPassword']);
                    }
                }
            }
        } else {
            $userInformation = $userTable->find()->where(['employee_record_id' => $id])->first();
            if (!empty($this->request->data) && $this->request->is('post')) {

                if (!empty($this->request->data['password'])) {
                    try {
                        $employeeinfo = $this->EmployeeRecords->get($id);

                        $userInformation->password = ($this->request->data['password']);
                        $has_error = $userInformation->errors();
                        if (empty($has_error)) {
                            $isSent = 0;
                            if ($this->request->data['password'] == DEFAULT_PASSWORD) {
                                $this->Flash->error('পাসওয়ার্ড পরিবর্তন করা সম্ভব হচ্ছে না। এই পাসওয়ার্ড   গ্রহণযোগ্য  নয়');
                                return $this->redirect(['controller' => 'EmployeeRecords',
                                    'action' => 'resetPassword']);
                            }

                            $userInformation->force_password_change = 0;
                            $userInformation->last_password_change = date("Y-m-d H:i:s");
                            if ($userInformation = $userTable->save($userInformation)) {
                                unset($userInformation->password);
                                $this->Auth->setUser($userInformation->toArray());
                                if (!empty($employeeinfo['personal_email'])) {
//                                    $this->SendEmailMessage($employeeinfo['personal_email'],
//                                        "আপনার পাসওয়ার্ড পরিবর্তন হয়েছে। <br/>ধন্যবাদ",
//                                        __("Password Change"));
//
//                                    
                                    $employeeOffices = TableRegistry::get('EmployeeOffices');
                                    $offices = $employeeOffices->getEmployeeOfficeRecords($id);
                                    $email_options['email_type'] = EMAIL_USER;
                                    $email_options['subject'] = "Password Change";
                                    $email_options['office_id'] = $offices[0]['office_id'];
                                    $email_options['office_name'] = $offices[0]['office_name'];
                                    $email_message = "আপনার পাসওয়ার্ড পরিবর্তন হয়েছে। <br/>ধন্যবাদ";
                                    $this->SendEmailMessage($employeeinfo['personal_email'],$email_message, $email_options);
                                    $isSent = 1;
                                }
                                if (!empty($employeeinfo['personal_mobile'])) {
                                    $this->SendSmsMessage($employeeinfo['personal_mobile'],
                                        "Dear " . $employeeinfo['name_eng'] . "\n\nYour password is changed.\nThank you");

                                    $isSent = 1;
                                }
                                $this->Flash->success('পাসওয়ার্ড পরিবর্তন করা হয়েছে');
                                return $this->redirect(['controller' => 'EmployeeRecords',
                                    'action' => 'myProfile']);
                            } else {
                                $this->Flash->error('পাসওয়ার্ড পরিবর্তন করা সম্ভব হচ্ছে না');
                                return $this->redirect(['controller' => 'EmployeeRecords',
                                    'action' => 'resetPassword']);
                            }
                        } else {
                            if (!empty($userInformation->errors()['password']['length'])) {
                                $this->Flash->error($userInformation->errors()['password']['length']);
                            } else if (!empty($userInformation->errors()['cpassword']['message'])) {
                                $this->Flash->error($userInformation->errors()['cpassword']['cpassword']);
                            } else if (!empty($userInformation->errors()['current_password']['custom'])) {
                                $this->Flash->error($userInformation->errors()['current_password']['custom']);
                            } else {
                                $this->Flash->error('পাসওয়ার্ড পরিবর্তন করা সম্ভব হচ্ছে না');
                            }
                            return $this->redirect(['controller' => 'EmployeeRecords',
                                'action' => 'resetPassword']);
                        }
                    } catch (Exception $ex) {

                        $this->Flash->error('পাসওয়ার্ড পরিবর্তন করা সম্ভব হচ্ছে না');
                        return $this->redirect(['controller' => 'EmployeeRecords',
                            'action' => 'resetPassword']);
                    }
//                    return $this->redirect(['controller' => 'EmployeeRecords', 'action' => 'myProfile']);
                }
//               return $this->redirect(['controller' => 'EmployeeRecords', 'action' => 'myProfile']);
            }
        }
    }

    public function updateLogin()
    {
        set_time_limit(0);
        $users_table = TableRegistry::get('Users');

        $users = $users_table->find()->select(['id', 'username', 'user_alias'])->where(['user_role_id >' => 2])->toArray();

        foreach ($users as $key => $value) {
            $username = $value['username'];
            $prefix = $username[0];
            $rest = substr($username, 1);
            $rest_num = intval($rest);

            $new_username = $prefix . str_pad($rest_num, 11, '0', STR_PAD_LEFT);

            $users_table->updateAll(['username' => $new_username, 'user_alias' => $new_username],
                ['id' => $value['id']]);
            if (file_exists(FILE_FOLDER_DIR . 'Personal/signature/' . $username . '.png')) {
                $file = FILE_FOLDER_DIR . 'Personal/signature/' . $username . '.png';
                $newfile = FILE_FOLDER_DIR . 'Personal/signature/' . $new_username . '.png';

                if (!copy($file, $newfile)) {
                    echo "failed to copy signature : " . $new_username . "<br/>";
                } else {
                    unlink($file);
//echo 'copied';
                }
            }
            if (file_exists(FILE_FOLDER_DIR . 'Personal/profile/' . $username . '.png')) {
                $file = FILE_FOLDER_DIR . 'Personal/profile/' . $username . '.png';
                $newfile = FILE_FOLDER_DIR . 'Personal/profile/' . $new_username . '.png';

                if (!copy($file, $newfile)) {
                    echo "failed to copy profile : " . $new_username . "<br/>";
                } else {
                    unlink($file);
                }
            }
        }
        die;
    }

    public function updateNid()
    {
        $this->layout = null;
        $user = $this->Auth->user();

        $id = $user['employee_record_id'];
        if (empty($id)) {
            $this->Flash->error("দুঃখিত! প্রোফাইল তথ্য পাওয়া যায়নি");
            $this->set('entity', []);
        } else {
            $employee_record_table = TableRegistry::get('EmployeeRecords');

            $employee_record = $employee_record_table->get($id);
            $this->set('entity', $employee_record);

            if (!empty($this->request->data)) {

                $conn = ConnectionManager::get('projapotiDb');
                try {
                    $conn->begin();

                    $employee_record->nid = bnToen($this->request->data['nid']);

                    $employee_record->date_of_birth = date("Y-m-d 00:00:00",
                        strtotime($this->request->data['date_of_birth']));

                    if (!empty($employee_record->nid)) {
                        $check_NID_DOB = $this->checkNID($employee_record->date_of_birth,
                            $employee_record->nid);

                        if ($check_NID_DOB > 0) {
                            throw new \Exception(' জাতীয় পরিচয়পত্রের তথ্য   মিলছে না।  Error: ' . $check_NID_DOB);
                        }
                        if ($employee_record_table->find()->where(['nid' => $employee_record->nid, 'id <>' => $id])->count() > 0) {
                            throw new \Exception('জাতীয় পরিচয়পত্র নম্বর পূর্বে ব্যবহৃত হয়েছে ');
                        }
                    } else {
                        throw new \Exception(' জাতীয় পরিচয়পত্র নম্বর ১৭ সংখ্যার হতে হবে।   ');
                    }

//                     pr($employee_record);die;
                    if ($employee_record_table->save($employee_record)) {
                        $conn->commit();

                        return $this->redirect(['controller' => 'Dashboard', 'action' => 'dashboard']);
                    }
                } catch (\Exception $ex) {
                    $this->Flash->error('দুঃখিত! কর্মকর্তার তথ্য সঠিকভাবে দেয়া হয়নি। ' . $ex->getMessage());
                    $conn->rollback();
                }
            }
        }
    }

    public function checkDoubleEntry()
    {
        if ($this->request->is('post')) {
            $response = [
                'status' => 'error',
                'message' => 'Something went wrong'
            ];
            try{
                $cadre_id = $this->request->data['cadre'];
                $identity_no = isset($this->request->data['identity'])?bnToen($this->request->data['identity']):'';
                if(empty($identity_no) || empty($cadre_id)){
                    $response['message'] = 'পরিচিতি নাম্বার দেওয়া হয়নি';
                    goto rtn;
                }

                $usersTable = TableRegistry::get('Users');
                $is_ex = $usersTable->find()->select(['username', 'employee_record_id'])->where(['username' => ($cadre_id . str_pad(intval($identity_no),
                        11, '0', STR_PAD_LEFT))])->first();
                if (!empty($is_ex)) {
                    $employeeRecordsTable = TableRegistry::get('EmployeeRecords');
                    $user_info = $employeeRecordsTable->find()->select(['id', 'name_bng', 'father_name_bng', 'mother_name_bng', 'date_of_birth', 'identity_no'])->where(['EmployeeRecords.id' => $is_ex['employee_record_id']])->first();
                    $user_info['username'] = $is_ex['username'];
                    if(!empty($user_info['date_of_birth'])){
                        $user_info['date_of_birth'] = $user_info['date_of_birth']->format("Y-m-d");
                    }
                    $response = [
                        'status' => 'success',
                        'data' => $user_info
                    ];
                } else {
                    $response = [
                        'status' => 'success',
                        'data' => ''
                    ];
                }
            }catch (\Exception $ex){
                $response['message'] = __('Technical error happen');
                $response['reason'] = $this->makeEncryptedData($ex->getMessage());
            }
           rtn:
            $this->response->body(json_encode($response));
            $this->response->type('application/json');
            return $this->response;
        }
    }

    public function viewOfficeDashboard()
    {
        $tbl_VOD = TableRegistry::get('ViewOfficeDashboard');
        $selected_office_section = $this->getCurrentDakSection();
        if ($this->request->is('get')) {
            $tbl_EO = TableRegistry::get('EmployeeOffices');
            $data = $tbl_VOD->find()->select(['viewer_designations'])->where(['office_id' => $selected_office_section['office_id']])->first();
            $designation_array = [];
            if (!empty($data)) {
                $php_array = json_decode($data['viewer_designations']);
                if (!empty($php_array)) {
                    foreach ($php_array as $val) {
                        $designation_array[] = $val;
                    }
                }
            }
            $all_record = $tbl_EO->getEmployeeOfficeRecordsByOfficeId($selected_office_section['office_id']);
            $this->set(compact(['designation_array', 'all_record']));
        } else if ($this->request->is('post')) {
            $response = ['status' => 'error', 'msg' => 'somthing went wrong'];
            try {
                $designation_array = json_encode($this->request->data['designation']);
                $tbl_VOD->deleteAll(['office_id' => $selected_office_section['office_id']]);
                $VOD_entity = $tbl_VOD->newEntity();
                $VOD_entity->office_id = $selected_office_section['office_id'];
                $VOD_entity->viewer_designations = $designation_array;
                $VOD_entity->created_by = $this->Auth->user()['employee_record_id'];
                $tbl_VOD->save($VOD_entity);
                $response = ['status' => 'success', 'msg' => 'সংরক্ষিত হয়েছে'];
                $this->request->session()->delete('canSeeOfficeDB');
            } catch (\Exception $ex) {
                $response['msg'] = 'Can not Update. Error: ' . $ex->getMessage();
            }
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;

        }
    }
    public function englishDesignationUpdateList(){
        $current_dak_section = $this->getCurrentDakSection();

        $employee_offices_table = TableRegistry::get('EmployeeOffices');
        $office_employees= $this->paginate($employee_offices_table->getEmployeeOfficeRecordsWithOfficeUnitOrganogramByOfficeId($current_dak_section['office_id']));
        $this->set('datas', $office_employees);
    }
    public function englishDesignationUpdate(){
        if(!empty($this->request->data['eng_designation']) && !empty($this->request->data['office_unit_organogram_id'])){
            $employee_records_table = TableRegistry::get('OfficeUnitOrganograms');
            $employee_data = $employee_records_table->get($this->request->data['office_unit_organogram_id']);
            if(!empty($employee_data)){
                $employee_data->designation_eng = $this->request->data['eng_designation'];
                if($employee_records_table->save($employee_data)){
                    $this->Flash->set('পদবি সফলভাবে পরিবর্তন হয়েছে।', [
                        'element' => 'success'
                    ]);
                } else{
                    $this->Flash->set('দুঃখিত, আপনার আবেদন গ্রহণ করা সম্ভব হচ্ছে না. অনুগ্রহপূর্বক আবার চেষ্টা করুন', [
                        'element' => 'error'
                    ]); }
                }
            }
            else {
                $this->Flash->set('দুঃখিত, আপনার আবেদন গ্রহণ করা সম্ভব হচ্ছে না. অনুগ্রহপূর্বক আবার চেষ্টা করুন', [
                    'element' => 'error'
                ]); }
        $this->redirect($this->referer());
    }
    public function getDesignationHistoryInfo(){
        $response = [
            'status' => 'error',
            'msg' => 'Something wrong'
        ];
        $designation_id = $this->request->data['designation_id'];
        if(empty($designation_id)){
            $response['msg'] = 'empty designation';
            goto rtn; 
        }
        try{
            $lastWorkInfo = TableRegistry::get('EmployeeOffices')->getLastDesignationHistoryInfo($designation_id);
            $msg = ' সর্বশেষ যে কর্মকর্তা এই পদে ছিলঃ <br>';
             $msg = $msg . '<table class="table table-bordered"><thead><tr><th>নাম</th><th>যোগদানের তারিখ</th><th>অব্যাহতি তারিখ</th></tr></thead>';
            if(!empty($lastWorkInfo)){
               $msg     =$msg . '<tbody><tr><td>'
                   .(!empty($lastWorkInfo['EmployeeRecords']['name_bng'])?$lastWorkInfo['EmployeeRecords']['name_bng']:'').
                   '</td><td>'
                   .(!empty($lastWorkInfo['joining_date'])?$lastWorkInfo['joining_date']->i18nformat('Y-M-d'):'')
                   .'</td><td>'
                   .(!empty($lastWorkInfo['last_office_date'])?$lastWorkInfo['last_office_date']->i18nformat('Y-M-d'):'')
                   .'</td></tr></tbody>';
//                pr($msg);die;
            }else{
                 $msg .= '<tbody><tr><td colspan="3" class="danger text-center" >পূর্বে কোন ব্যক্তি এই পদে কর্মরত ছিল না।</td></tr></tbody>';
            }
             $msg .= '</table>';
             //$msg .= '</table><br><b> আপনি কি নিশ্চিত পদবিতে নির্বাচিত কর্মকর্তাকে দায়িত্ব অর্পণ করতে? </b>';
             if(!empty($msg)){
                $response = [
                'status' => 'success',
                'msg' => $msg
                ];
             }
            } catch (\Exception $ex) {
                $response['msg'] = $ex->getMessage();
            }
        
        rtn:
            $this->response->body(json_encode($response));
            $this->response->type('application/json');
            return $this->response;
    }

    public function testSignature($office_id = null, $autoUpdate = false){
        $tableEmployeeOffices = TableRegistry::get('EmployeeOffices');
        $tableUserSignatures = TableRegistry::get('UserSignatures');

        $users = $tableEmployeeOffices->find()
            ->select(['username'=>'Users.username','signature'=>'UserSignatures.encode_sign','signature_file'=>'UserSignatures.signature_file','signid'=>'UserSignatures.id'])
            ->join([
                'Users'=>[
                    'table'=>'users',
                    'type'=>'inner',
                    'conditions'=>'Users.employee_record_id = EmployeeOffices.employee_record_id'
                ],
                'UserSignatures'=>[
                    'table'=>'user_signatures',
                    'type'=>'inner',
                    'conditions'=>'Users.username = UserSignatures.username'
                ]
            ]);

        if(!empty($office_id)){
            $users = $users->where(['EmployeeOffices.office_id'=>$office_id]);
        }

        $users = $users->where(['EmployeeOffices.status'=>1])->order(['EmployeeOffices.employee_record_id'=>'asc'])->toArray();

        foreach($users as $key=>$sign){

            $signature = str_replace('data:image/png;base64,','',$sign['signature']);
            $checkvalid = $this->check_base64_image($signature);

            if($checkvalid){
                continue;
            }

            echo $sign['signid'] . '--' . $sign['username'] . '::' . "Not valid<br/>";

            if($autoUpdate) {
                $checkvalid2 = false;
                if (!empty($sign['signature_file'])) {
                    $data = file_get_contents($sign['signature_file']);
                    if (!empty($data)) {
                        $dataUri = base64_encode($data);
                        if (!empty($dataUri)) {
                            $checkvalid2 = $this->check_base64_image($dataUri);
                        }
                    }
                }
                if ($checkvalid2) {
                    try {
                        $tableUserSignatures->updateAll(['encode_sign' => ('data:image/png;base64,' . $dataUri)], ['id' => $sign['signid']]);
                        echo "::Updated Now";
                    } catch (\Exception $ex) {
                        echo $ex->getMessage();
                    }
                } else {
                    echo "::Cannot update";
                }
                echo "<br/>";
            }
        }


        die;
    }
    public function setDefaultSignature(){
           $response = [
            'status' => 'error',
            'msg' => 'Something wrong'
        ];
        $emp_id = !empty($this->request->data['id'])?$this->request->data['id']:0;
        $apikey = !empty($this->request->data['api_key'])?$this->request->data['api_key']:'';
//        $sign = !empty($this->request->data['sign'])?$this->request->data['sign']:0;
        $sign = !empty($this->request->data['tokenType'])?($this->request->data['tokenType'] == 'Soft' ?1:2):0;
        if(empty($emp_id)){
            $response['msg'] = 'কর্মকর্তার তথ্য পাওয়া যায়নি।';
            goto rtn;
        }
        if(empty($apikey)){
            if($this->Auth->user()['employee_record_id'] != $emp_id){
                $response['msg'] = 'আপনি অনুমতিপ্রাপ্ত নন।';
                goto rtn;
            }
        }else{
            if (empty($apikey) || $this->checkToken($apikey) == FALSE) {
                $response['msg'] = 'API key mismatch';
                goto rtn;
            }
            if($apikey != API_KEY){
                //verify api token
                $this->loadComponent('ApiRelated');
                $getApiTokenData = $this->ApiRelated->getMobileApiTokenData($apikey);
                //verify api token
            }
            if(!empty($getApiTokenData)){
                if($emp_id != $getApiTokenData['employee_record_id']){
                    $jsonArray['msg'] = 'Unauthorized request';
                    goto rtn;
                }
            }
        }
        try{

            //for dummy presentation file based certificate check Added. Request by: Hasan bhai on 3 Jan,2018.
            if(defined('Live') && Live != 1 && isset($this->request->data['type'])){
                if($this->request->data['type'] == 'file'){
                    $data = ['default_sign' => $sign];
                    TableRegistry::get('EmployeeRecords')->updateAll($data, ['id' => $emp_id]);
                    $response = [
                        'status' => 'success',
                        'msg' => 'স্বাক্ষর ধরন পরিবর্তন করা হয়েছে।'
                    ];
                }
            }
            $data = ['default_sign' => $sign];
            if($sign > 0){
                 $ca = !empty($this->request->data['ca'])?$this->request->data['ca']:'';
                 $certSerialNumber = !empty($this->request->data['certSerialNumber'])?$this->request->data['certSerialNumber']:'';
                 $certProofNo = !empty($this->request->data['ProofNo'])?$this->request->data['ProofNo']:0;
                 $certProofType = !empty($this->request->data['ProofType'])?$this->request->data['ProofType']:'';
                 $proofTypeArray= [1 => 'NID',2 => 'PASSPORT' ,3 => 'BCN', 4 => 'TIN'];
                if(empty($ca) || empty($certSerialNumber)|| empty($certProofNo)|| empty($certProofType)){
                      $response['msg'] = 'সিগনেচার এর সার্টিফিকেট নাম্বার অথবা কর্তৃপক্ষ তথ্য পাওয়া যায়নি।';
                        goto rtn;
                }else{
                    $data['cert_id'] = $certProofNo;
                    $data['cert_type'] = $proofTypeArray[$certProofType];
                    $data['cert_provider'] = $ca;
                    $data['cert_serial'] = $certSerialNumber;
                }
            }
            TableRegistry::get('EmployeeRecords')->updateAll($data, ['id' => $emp_id]);
             $response = [
                        'status' => 'success',
                        'msg' => 'স্বাক্ষর ধরন পরিবর্তন করা হয়েছে।'
                    ];
            $session = $this->request->session();
            $session_data = $session->read('selected_office_section');
            $session_data['default_sign'] = $sign;
            $session->write('selected_office_section',$session_data);
        } catch (\Exception $ex) {
            $response['msg'] = $ex->getMessage();
        }
        rtn:
            $this->response->body(json_encode($response));
            $this->response->type('application/json');
            return $this->response;
    }
    private function notifyUser($to_user = '',$cc_user = '',$subject = '',$from = 'নথি',$message = ''){
        if( !empty($message) && !empty($to_user)){
             sendMailsByCron($to_user, $cc_user, $subject, $from, $message);
        }
       return;
    }
    public function getDigitalSignatureSettings(){
//        $employee_office = $this->getCurrentDakSection();
        $this->digitalSigantureUrlChecker();

        $proofTypeArray= [1 => 'NID',2 => 'PASSPORT' ,3 => 'BCN', 4 => 'TIN'];
        if($this->request->is('post')){
            $response = ['status' => 'error' ,'msg' => 'something wrong'];
            $emp_id = $id = !empty($this->request->data['id'])?$this->request->data['id']:0;
            $ProofType = !empty($this->request->data['ProofType'])?$this->request->data['ProofType']:'';
            $ProofNo = !empty($this->request->data['ProofNo'])?$this->request->data['ProofNo']:0;
            $apikey = !empty($this->request->data['api_key'])?$this->request->data['api_key']:'';
            try{
                if(empty($apikey)){
                    if($this->Auth->user()['employee_record_id'] != $emp_id){
                        $response['msg'] = 'আপনি অনুমতিপ্রাপ্ত নন।';
                        goto rtn;
                    }
                }else{
                    if (empty($apikey) || $this->checkToken($apikey) == FALSE) {
                        $response['msg'] = 'API key mismatch';
                        goto rtn;
                    }
                    if($apikey != API_KEY){
                        //verify api token
                        $this->loadComponent('ApiRelated');
                        $getApiTokenData = $this->ApiRelated->getMobileApiTokenData($apikey);
                        //verify api token
                    }
                    if(!empty($getApiTokenData)){
                        if($emp_id != $getApiTokenData['employee_record_id']){
                            $jsonArray['msg'] = 'Unauthorized request';
                            goto rtn;
                        }
                    }
                }
                if(!empty($ProofType) && !empty($ProofNo)){
                    $getCertificate = TableRegistry::get('DigitalSignature')->getCertificate($proofTypeArray[$ProofType],$ProofNo);
                    if(!empty($getCertificate)){
                         $response = $getCertificate;
                         goto rtn;
                    }
                }else{
                    $response['msg'] = __('Required info not given');
                }
            } catch (\Exception $ex) {
                $response['msg'] = $ex->getMessage();
            }
            rtn:
                $this->response->type('json');
                $this->response->body(json_encode($response));
                return $this->response;
        }
    }
    public function postApiUserSignatureType(){
        $response = array(
            "status" => "error",
            "msg" => "Invalid Request"
        );
        if (!defined("PROJAPOTI_API") || PROJAPOTI_API == false) {
            goto rtn;
        }
        $apikey = (isset($this->request->data['api_key']) ? $this->request->data['api_key'] : (isset($this->request->query['api_key']) ? $this->request->query['api_key'] : ''));
        if (empty($apikey) || $this->checkToken($apikey) == FALSE) {
            $response['msg'] = 'API key mismatch';
            goto rtn;
        }
        $employee_record_id = isset($this->request->data['employee_record_id']) ? $this->request->data['employee_record_id'] : (isset($this->request->query['employee_record_id']) ? $this->request->query['employee_record_id']:'');
        if (empty($employee_record_id)) {
            $response['msg'] = 'Employee record missing';
            goto rtn;
        }
        try{
            if($apikey != API_KEY){
                //verify api token
                $this->loadComponent('ApiRelated');
                $getApiTokenData = $this->ApiRelated->getMobileApiTokenData($apikey);
                //verify api token
            }
            if(!empty($getApiTokenData)){
                if($employee_record_id != $getApiTokenData['employee_record_id']){
                    $jsonArray['msg'] = 'Unauthorized request';
                    goto rtn;
                }
            }

            $employeeRecordsTable = TableRegistry::get('EmployeeRecords');

            $employee_record = $employeeRecordsTable->get($employee_record_id);
            $response = array(
                "status" => "success",
                "data" => $employee_record,
                'api_key' => $apikey
            );

        }catch (\Exception $ex){
            $response['msg'] = 'টেকনিক্যাল ত্রুটি হয়েছে';
            $response['reason'] = $this->makeEncryptedData($ex->getMessage());
        }
        rtn:
            $this->layout ='online_dak';
            $this->set(compact('response'));

    }
    public function postApiGetEmployeeInfo(){
        $response = array(
            "status" => "error",
            "msg" => "Invalid Request"
        );
        if (!defined("PROJAPOTI_API") || PROJAPOTI_API == false) {
            goto rtn;
        }
        $apikey = (isset($this->request->data['api_key']) ? $this->request->data['api_key'] : (isset($this->request->query['api_key']) ? $this->request->query['api_key'] : ''));
        if (empty($apikey) || $this->checkToken($apikey) == FALSE) {
            $response['msg'] = 'API key mismatch';
            goto rtn;
        }
        $employee_record_id = isset($this->request->data['employee_record_id']) ? $this->request->data['employee_record_id'] : (isset($this->request->query['employee_record_id']) ? $this->request->query['employee_record_id']:'');
        if (empty($employee_record_id)) {
            $response['msg'] = 'Employee record missing';
            goto rtn;
        }
        try{
            if($apikey != API_KEY){
                //verify api token
                $this->loadComponent('ApiRelated');
                $getApiTokenData = $this->ApiRelated->getMobileApiTokenData($apikey);
                //verify api token
            }
            if(!empty($getApiTokenData)){
                if($employee_record_id != $getApiTokenData['employee_record_id']){
                    $jsonArray['msg'] = 'Unauthorized request';
                    goto rtn;
                }
            }

            $employeeRecordsTable = TableRegistry::get('EmployeeRecords');

            $employee_record = $employeeRecordsTable->get($employee_record_id);
            $response = array(
                "status" => "success",
                "data" => $employee_record,
            );

        }catch (\Exception $ex){
            $response['msg'] = 'টেকনিক্যাল ত্রুটি হয়েছে';
            $response['reason'] = $this->makeEncryptedData($ex->getMessage());
        }
        rtn:
        $this->response->type('json');
        $this->response->body(json_encode($response));
        return $this->response;

    }
    public function checkUserSoftToken(){
        $response = array(
            "status" => "error",
            "message" => "Invalid Request"
        );
        $tableDsUserInfo = TableRegistry::get('DsUserInfo');

        $employee_office = $this->getCurrentDakSection();

        if(empty($employee_office)){
            $apikey = isset($this->request->data['api_key']) ? $this->request->data['api_key'] : '';
            $employee_record_id = isset($this->request->data['employee_record_id']) ? $this->request->data['employee_record_id'] : '';
            if (empty($apikey) || $this->checkToken($apikey) == FALSE) {
                goto rtn;
            }
            if($apikey != API_KEY){
                //verify api token
                $this->loadComponent('ApiRelated');
                $getApiTokenData = $this->ApiRelated->getMobileApiTokenData($apikey);
                //verify api token
                if(!empty($getApiTokenData)){
                    if($employee_record_id != $getApiTokenData['employee_record_id']){
                        $response['message'] = 'Unauthorized request';
                        goto rtn;
                    }
                }
                $employee_office = $this->setCurrentDakSection($getApiTokenData['designations'][0]);
            }

        }

        if(!empty($employee_office['officer_id'])){
            $has_token = $tableDsUserInfo->getData(
                ['expired_at'],
                ['expired_at >=' => date('Y-m-d H:i:s'),'employee_record_id' => $employee_office['officer_id']])
            ->first();
            if(!empty($has_token)){
                //generate special formatted string so that system can understand this is not a real soft token rather need to fetch soft token of that user from database
                $start = rand(1,3);
                $end = rand(1,3);
                $sign_token = 'st-'.$start.$end.'-'.$this->mt_rand_str($start).'-'.$this->mt_rand_str($end);//highest 13 alphanumeric

//                $time_pasred = Time::parse($has_token['expired_at'])->i18nFormat(null, null, 'bn-BD');
                $left = $this->dateDiff($has_token['expired_at']->i18nFormat(null, null, 'En'),date('Y-m-d H:i:s'));

                $message = "আপনি পরবর্তী {$left} পর্যন্ত সফট টোকেন ব্যতীত ডিজিটাল সিগনেচার করতে অনুমতিপ্রাপ্ত।";
                $response = array(
                    "status" => "success",
                    "time" => $has_token['expired_at'],
                    "message" => $message,
                    "sign_token" => $sign_token,
                );
            }else{
               $response['message'] = 'expired';
            }
        }
        rtn:
        $this->response->type('json');
        $this->response->body(json_encode($response));
        return $this->response;
    }

    public function view_permission_settings() {
		$this->layout = null;
		$current_user = $this->getCurrentDakSection();
		$existing_permissions_table = TableRegistry::get('OtherOrganogramActivitiesSettings');
		$OfficeUnitOrganogramsTable = TableRegistry::get('OfficeUnitOrganograms');

		if ($this->request->is('post')) {
			if ($this->request->data('method') == 'delete') {
				$existing_permissions_table->updateAll([
					'unassigned_date' => date('Y-m-d H:i:s'),
					'status' => 0,
				], [
					'id' => $this->request->data('permission_id')
				]);
				$this->Flash->success('সফলভাবে মুছে ফেলা হয়েছে');
				return $this->redirect(['controller' => 'EmployeeRecords', 'action' => 'myProfile', '?' => ['page' => 'viewPermissionToOther']]);
			} else {
				$existing_permission = $existing_permissions_table->newEntity();
				$existing_permission->organogram_id = $current_user['office_unit_organogram_id'];
				$existing_permission->assigned_organogram_id = $this->request->data('assigned_organogram_id');
				$existing_permission->assigned_date = date('Y-m-d H:i:s');
				$existing_permission->permission_for = $this->request->data('permission_for');
				$existing_permission->status = 1;
				$existing_permissions_table->save($existing_permission);
				$this->Flash->success('সফলভাবে অনুমতি প্রদান করা হয়েছে');
				return $this->redirect(['controller' => 'EmployeeRecords', 'action' => 'myProfile', '?' => ['page' => 'viewPermissionToOther']]);
			}
		}
		$OfficeUnitOrganograms = $OfficeUnitOrganogramsTable->find()->select(['id', 'designation_bng'])->where(['office_id' => $current_user['office_id'], 'status' => 1])->order(['office_unit_id'])->toArray();

		$existing_permissions = $existing_permissions_table->find()->where(['organogram_id' => $current_user['office_unit_organogram_id'], 'status' => 1])->toArray();

		$this->set(compact('current_user', 'OfficeUnitOrganograms', 'existing_permissions'));

	}


	public function dsCertificateUpload(){

        $ds_proof_type = json_decode(DS_PROOF_TYPE, true);


            $post_fields = array(
                "data" =>
                    array(
                        "identityProfType" => $ds_proof_type[$this->request->data['identityProfType']], //string
                        "identityProofNo" => $this->request->data['identityProofNo'], //string
                        "caName" => $this->request->data['caName'], //string
                        "serialNumber" => $this->request->data['serialNumber'], //string
                        "certificateValidity" => $this->request->data['certificateValidity'], //int
                        "password" => $this->request->data['password'], //string
                        "cnfm_password" => $this->request->data['cnfm_password'], //string
                        "issueDate" => $this->request->data['issueDate'], // must be MM/dd/yyyy format
                        "certificateContent" => $this->request->data['certificateContent'] // Base64 converted pfx/p12 file
                    )
            );
            try {
                //share with curl
                $curl = curl_init();
                curl_setopt($curl, CURLOPT_URL, 'https://signer.nothi.gov.bd:8444/nothi/certificateinformation/savecrt');
                curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                curl_setopt($curl, CURLOPT_USERPWD, "admin:admin1234"); //Your credentials goes here
                curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                    'Accept: application/json',
                    'Content-Type: application/json'
                ));
                curl_setopt($curl, CURLOPT_POST, true);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($curl, CURLOPT_POSTFIELDS, (json_encode($post_fields)));
                $errno = curl_errno($curl);
                if (!empty($errno)) {
                    $error_message = curl_strerror($errno);
                    $error = $errno . ' : ' . $error_message;
                    $this->Flash->set('দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না। বিবরণ : ' . $error, ['element' => 'error']);
                    $this->redirect($this->referer());

                }
                $result = curl_exec($curl);
                curl_close($curl);
                //end


                $this->response->body($result);
                $this->response->type('application/json');
                return $this->response;


            } catch (\Exception $ex) {
                $this->response->body(json_encode($ex));
                $this->response->type('application/json');
                return $this->response;
            }
    }
}
