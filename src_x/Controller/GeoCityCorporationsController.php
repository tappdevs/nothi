<?php

namespace App\Controller;

use App\Controller\ProjapotiController;
use App\Model\Table;
use App\Model\Table\GeoCityCorporationsTable;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\Entity;
use App\Model\Entity\GeoCityCorporation;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

class GeoCityCorporationsController extends ProjapotiController
{
     public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Paginator');
    }
    public function index()
    {
        $geo_city_corporations = TableRegistry::get('GeoCityCorporations');
        $query                 = $geo_city_corporations->find('all')->contain(['GeoDivisions',
            'GeoDistricts']);
         try {
            $this->set('query', $this->paginate($query));
        } catch (NotFoundException $e) {
            $this->redirect(['action' => 'index']);
        }
    }

    public function add()
    {
        $this->loadModel('GeoDivisions');
        $geo_divisions = $this->GeoDivisions->find('all');
        $this->set(compact('geo_divisions'));

        $this->loadModel('GeoDistricts');
        $geo_districts = $this->GeoDistricts->find('all');
        $this->set(compact('geo_districts'));

        $geo_city_corporation = $this->GeoCityCorporations->newEntity();
        if ($this->request->is('post')) {

            $validator = new Validator();
            $validator->notEmpty('city_corporation_name_bng',
                'সিটি কর্‌পোরেশনের  নাম(বাংলা) দেওয়া হয় নি')->notEmpty('city_corporation_name_eng',
                'সিটি কর্‌পোরেশনের  নাম(ইংরেজি) দেওয়া হয় নি')->notEmpty('bbs_code',
                'সিটি কর্‌পোরেশনের কোড দেওয়া হয় নি')->notEmpty('status',
                'অবস্থা নির্বাচন করুন')->notEmpty('geo_district_id',
                'জেলা কোড দেওয়া হয় নি')->notEmpty('geo_division_id',
                'বিভাগ কোড দেওয়া হয় নি');
            $errors    = $validator->errors($this->request->data());
            if (empty($errors)) {
                $geo_city_corporation = $this->GeoCityCorporations->patchEntity($geo_city_corporation,
                    $this->request->data);
                  $other_info                                    = TableRegistry::get('GeoDistricts')->find()->select(['bbs_code',
                        'division_bbs_code'])->where([ 'id' => $geo_city_corporation->geo_district_id])->first();
                $geo_city_corporation->division_bbs_code     = $other_info['division_bbs_code'];
                $geo_city_corporation->district_bbs_code = $other_info['bbs_code'];
                if ($this->GeoCityCorporations->save($geo_city_corporation)) {
                    $this->Flash->success('সংরক্ষিত হয়েছে।');
                    return $this->redirect(['action' => 'index']);
                }
            } else {
                $this->set(compact('errors'));
            }
        }
        $this->set('user', $geo_city_corporation);
    }

    public function edit($id = null, $is_ajax = null)
    {
        if ($is_ajax == 'ajax') {
            $this->layout = null;
        }

         $geo_divisions = TableRegistry::get('GeoDivisions')->find('list',
                    ['keyField' => 'id', 'valueField' => 'division_name_bng'])->toArray();
            $this->set(compact('geo_divisions'));

            $geo_districts = TableRegistry::get('GeoDistricts')->find('list',
                    ['keyField' => 'id', 'valueField' => 'district_name_bng'])->toArray();
            $this->set(compact('geo_districts'));

        $geo_city_corporation = $this->GeoCityCorporations->get($id);
        if ($this->request->is(['post', 'put'])) {

            $validator = new Validator();
            $validator->notEmpty('city_corporation_name_bng',
                'সিটি কর্‌পোরেশনের  নাম(বাংলা) দেওয়া হয় নি')->notEmpty('city_corporation_name_eng',
                'সিটি কর্‌পোরেশনের  নাম(ইংরেজি) দেওয়া হয় নি')->notEmpty('bbs_code',
                'সিটি কর্‌পোরেশনের কোড দেওয়া হয় নি')->notEmpty('status',
                'অবস্থা নির্বাচন করুন')->notEmpty('geo_district_id',
                'জেলা আইডি দেওয়া হয় নি')->notEmpty('geo_division_id',
                'প্রশাসনিক বিভাগ দেওয়া হয় নি')->notEmpty('district_bbs_code',
                'জেলা কোড  দেওয়া হয় নি')->notEmpty('division_bbs_code',
                ' বিভাগ কোড দেওয়া হয় নি');
            $errors    = $validator->errors($this->request->data());
            if (empty($errors)) {
                $geo_city_corporation                          = $this->GeoCityCorporations->patchEntity($geo_city_corporation,
                    $this->request->data);
                pr($geo_city_corporation);die;
                if ($this->GeoCityCorporations->save($geo_city_corporation)) {
                    $this->Flash->success('সংরক্ষিত হয়েছে।');
                    return $this->redirect(['action' => 'index']);
                }
            } else {
                $this->set(compact('errors'));
            }
        }

        $this->set('geo_city_corporation', $geo_city_corporation);
    }

    public function view($id = null)
    {
        $geo_city_corporation = $this->GeoCityCorporations->get($id);
        $this->set(compact('geo_city_corporation'));
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);

        $geo_city_corporation = $this->GeoCityCorporations->get($id);
        if ($this->GeoCityCorporations->delete($geo_city_corporation)) {
            return $this->redirect(['action' => 'index']);
        }
    }
}