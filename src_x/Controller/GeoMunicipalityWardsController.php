<?php

namespace App\Controller;

use App\Controller\ProjapotiController;
use App\Model\Table;
use App\Model\Table\GeoMunicipalityWardsTable;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\Entity;
use App\Model\Entity\GeoMunicipalityWards;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

class GeoMunicipalityWardsController extends ProjapotiController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Paginator');
    }

    public function index()
    {
        $geo_municipality_wards = TableRegistry::get('GeoMunicipalityWards');
        $query                  = $geo_municipality_wards->find('all')->contain(['GeoDivisions','GeoMunicipalities','GeoDistricts','GeoUpazilas']);
          try{
        $this->set('query',$this->paginate($query));
        }catch (NotFoundException $e) {
            $this->redirect(['action'=>'index']);
        }
    }

    public function add()
    {
        $this->loadModel('GeoDivisions');
        $geo_divisions = $this->GeoDivisions->find('all');
        $this->set(compact('geo_divisions'));

        $this->loadModel('GeoDistricts');
        $geo_districts = $this->GeoDistricts->find('all');
        $this->set(compact('geo_districts'));

        $this->loadModel('GeoMunicipalities');
        $geo_municipalities = $this->GeoMunicipalities->find('all');
        $this->set(compact('geo_municipalities'));

        $this->loadModel('GeoUpazilas');
        $geo_upazilas = $this->GeoUpazilas->find('all');
        $this->set(compact('geo_upazilas'));

        $geo_municipality_wards = $this->GeoMunicipalityWards->newEntity();
        if ($this->request->is('post')) {

            $validator = new Validator();
            $validator->notEmpty('ward_name_bng',
                'পৌরসভার  নাম(বাংলা) দেওয়া হয় নি')->notEmpty('ward_name_eng',
                'পৌরসভার  নাম(ইংরেজি) দেওয়া হয় নি')->notEmpty('bbs_code',
                'পৌরসভার কোড দেওয়া হয় নি')->notEmpty('status',
                'অবস্থা নির্বাচন করুন')->notEmpty('geo_district_id',
                'জেলা নির্বাচন করুন')->notEmpty('geo_division_id',
                'বিভাগ নির্বাচন করুন')->notEmpty('geo_upazila_id',
                'উপজেলা নির্বাচন করুন')->notEmpty('geo_municipality_id',
                'পৌরসভা  নির্বাচন করুন ');
            $errors    = $validator->errors($this->request->data());
            if (empty($errors)) {
                $geo_municipality_wards                        = $this->GeoMunicipalityWards->patchEntity($geo_municipality_wards,
                    $this->request->data);
                $other_info                                    = TableRegistry::get('GeoMunicipalities')->find()->select(['bbs_code',
                        'upazila_bbs_code', 'district_bbs_code', 'division_bbs_code'])->where([ 'id' => $geo_municipality_wards->geo_municipality_id])->first();
                $geo_municipality_wards->district_bbs_code     = $other_info['district_bbs_code'];
                $geo_municipality_wards->division_bbs_code     = $other_info['division_bbs_code'];
                $geo_municipality_wards->upazila_bbs_code      = $other_info['upazila_bbs_code'];
                $geo_municipality_wards->municipality_bbs_code = $other_info['bbs_code'];
                if ($this->GeoMunicipalityWards->save($geo_municipality_wards)) {
                    $this->Flash->success('সংরক্ষিত হয়েছে।');
                    return $this->redirect(['action' => 'index']);
                }
            } else {
                $this->set(compact('errors'));
            }
        }
        $this->set('geo_municipality_wards', $geo_municipality_wards);
    }

    public function edit($id = null, $is_ajax = null)
    {
        if ($is_ajax == 'ajax') {
            $this->layout = null;
        }
            $geo_divisions = TableRegistry::get('GeoDivisions')->find('list',
                    ['keyField' => 'id', 'valueField' => 'division_name_bng'])->toArray();
            $this->set(compact('geo_divisions'));

            $geo_districts = TableRegistry::get('GeoDistricts')->find('list',
                    ['keyField' => 'id', 'valueField' => 'district_name_bng'])->toArray();
            $this->set(compact('geo_districts'));

            $geo_upazilas       = TableRegistry::get('GeoUpazilas')->find('list',
                    ['keyField' => 'id', 'valueField' => 'upazila_name_bng'])->toArray();
            $this->set(compact('geo_upazilas'));
            
            $geo_municipalities = TableRegistry::get('GeoMunicipalities')->find('list',
                    ['keyField' => 'id', 'valueField' => 'municipality_name_bng'])->toArray();
            $this->set(compact('geo_municipalities'));

        $geo_municipality_wards = $this->GeoMunicipalityWards->get($id);
        if ($this->request->is(['post', 'put'])) {

            $validator = new Validator();
            $validator->notEmpty('ward_name_bng',
                'ওয়ার্ডের   নাম(বাংলা) দেওয়া হয় নি')->notEmpty('ward_name_eng',
                'ওয়ার্ডের   নাম(ইংরেজি) দেওয়া হয় নি')->notEmpty('bbs_code',
                'ওয়ার্ডের কোড দেওয়া হয় নি')->notEmpty('status',
                'অবস্থা নির্বাচন করুন')->notEmpty('geo_district_id',
                'জেলা নির্বাচন করুন')->notEmpty('geo_division_id',
                'বিভাগ নির্বাচন করুন')->notEmpty('geo_upazila_id',
                'উপজেলা নির্বাচন করুন')->notEmpty('geo_municipality_id',
                'পৌরসভা  নির্বাচন করুন ')->notEmpty('municipality_bbs_code',
                ' পৌরসভা কোড নির্বাচন করুন ')->notEmpty('upazila_bbs_code',
                ' উপজেলা কোড  নির্বাচন করুন ')->notEmpty('district_bbs_code',
                '  জেলা কোড নির্বাচন করুন ');
            $errors    = $validator->errors($this->request->data());
            if (empty($errors)) {
                $this->GeoMunicipalityWards->patchEntity($geo_municipality_wards,
                    $this->request->data);
                if ($this->GeoMunicipalityWards->save($geo_municipality_wards)) {
                    $this->Flash->success('সংরক্ষিত হয়েছে।');
                    return $this->redirect(['action' => 'index']);
                }
            }else{
                $this->set(compact('errors'));
            }
        }
        $this->set('geo_municipality_wards', $geo_municipality_wards);
    }

    public function view($id = null)
    {
        $geo_municipality_wards = $this->GeoMunicipalityWards->get($id);
        $this->set(compact('geo_municipality_wards'));
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);

        $geo_municipality_wards = $this->GeoMunicipalityWards->get($id);
        if ($this->GeoMunicipalityWards->delete($geo_municipality_wards)) {
            return $this->redirect(['action' => 'index']);
        }
    }
}