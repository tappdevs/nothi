<?php

namespace App\Controller;

use App\Controller\ProjapotiController;
use App\Model\Table;
use App\Model\Table\GeoPostOfficesTable;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\Entity;
use App\Model\Entity\GeoPostOffices;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

class GeoPostOfficesController extends ProjapotiController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Paginator');
    }

    public function index()
    {
        $geo_post_office = TableRegistry::get('GeoPostOffices');
        $query           = $geo_post_office->find('all')->contain(['GeoDivisions',
            'GeoThanas', 'GeoDistricts', 'GeoUpazilas']);
      try {
            $this->set('query', $this->paginate($query));
        } catch (NotFoundException $e) {
            $this->redirect(['action' => 'index']);
        }
    }

    public function add()
    {
        $this->loadModel('GeoDivisions');
        $geo_divisions = $this->GeoDivisions->find('all');
        $this->set(compact('geo_divisions'));

        $this->loadModel('GeoDistricts');
        $geo_districts = $this->GeoDistricts->find('all');
        $this->set(compact('geo_districts'));

        $this->loadModel('GeoUpazilas');
        $geo_upazilas = $this->GeoUpazilas->find('all');
        $this->set(compact('geo_upazilas'));

       $GeoThanas = TableRegistry::get('GeoThanas')->find('list',
                ['keyField' => 'id', 'valueField' => 'thana_name_bng'])->toArray();
        $this->set(compact('GeoThanas'));

        $geo_post_office = $this->GeoPostOffices->newEntity();
        if ($this->request->is('post')) {

            $validator = new Validator();
            $validator->notEmpty('geo_division_id',
                'প্রশাসনিক বিভাগ  নির্বাচন করুন')->notEmpty('postoffice_name_bng',
                'পোষ্ট  নাম(বাংলা) দেওয়া হয় নি')->notEmpty('postoffice_name_eng',
                'পোষ্ট  নাম(ইংরেজি) দেওয়া হয় নি')->notEmpty('bbs_code',
                'উপজেলার কোড দেওয়া হয় নি')->notEmpty('status',
                'অবস্থা নির্বাচন করুন')->notEmpty('geo_district_id',
                'জেলা কোড দেওয়া হয় নি')->notEmpty('geo_upazila_id',
                'উপজেলা নির্বাচন করুন');
            $errors    = $validator->errors($this->request->data());
            if (empty($errors)) {
                $geo_post_office                    = $this->GeoPostOffices->patchEntity($geo_post_office,
                    $this->request->data);
                $geo_upazilla_table                 = TableRegistry::get('GeoUpazilas')->find()->select(['bbs_code',
                        'division_bbs_code', 'district_bbs_code'])->where([ 'id' => $geo_post_office->geo_upazila_id])->first();
                $geo_post_office->upazila_bbs_code  = $geo_upazilla_table->bbs_code;
                $geo_post_office->division_bbs_code = $geo_upazilla_table->division_bbs_code;
                $geo_post_office->district_bbs_code = $geo_upazilla_table->district_bbs_code;
                if ($this->GeoPostOffices->save($geo_post_office)) {
                    $this->Flash->success('সংরক্ষিত হয়েছে।');
                    return $this->redirect(['action' => 'index']);
                }
            } else {
                $this->set(compact('errors'));
            }
        }
        $this->set('geo_post_office', $geo_post_office);
    }

    public function edit($id = null, $is_ajax = null)
    {
        if ($is_ajax == 'ajax') {
            $this->layout = null;
        }

        $geo_divisions = TableRegistry::get('GeoDivisions')->find('list',
                ['keyField' => 'id', 'valueField' => 'division_name_bng'])->toArray();
        $this->set(compact('geo_divisions'));

        $geo_districts = TableRegistry::get('GeoDistricts')->find('list',
                ['keyField' => 'id', 'valueField' => 'district_name_bng'])->toArray();
        $this->set(compact('geo_districts'));

        $geo_upazilas = TableRegistry::get('GeoUpazilas')->find('list',
                ['keyField' => 'id', 'valueField' => 'upazila_name_bng'])->toArray();
        $this->set(compact('geo_upazilas'));

        $GeoThanas = TableRegistry::get('GeoThanas')->find('list',
                ['keyField' => 'id', 'valueField' => 'thana_name_bng'])->toArray();
        $this->set(compact('GeoThanas'));

        $geo_post_office = $this->GeoPostOffices->get($id);
        if ($this->request->is(['post', 'put'])) {

            $validator = new Validator();
            $validator->notEmpty('postoffice_name_bng',
                'পোষ্ট  নাম(বাংলা) দেওয়া হয় নি')->notEmpty('postoffice_name_eng',
                'পোষ্ট  নাম(ইংরেজি) দেওয়া হয় নি')->notEmpty('bbs_code',
                'উপজেলার কোড দেওয়া হয় নি')->notEmpty('status',
                'অবস্থা নির্বাচন করুন')->notEmpty('geo_district_id',
                'জেলা কোড দেওয়া হয় নি')->notEmpty('district_bbs_code',
                'জেলা কোড দেওয়া হয় নি');
            $errors    = $validator->errors($this->request->data());
            if (empty($errors)) {
                $this->GeoPostOffices->patchEntity($geo_post_office,
                    $this->request->data);
                if ($this->GeoPostOffices->save($geo_post_office)) {
                    return $this->redirect(['action' => 'index']);
                }
            } else {
                $this->set(compact('errors'));
            }
        }
        $this->set('geo_post_office', $geo_post_office);
    }

    public function view($id = null)
    {
        $geo_post_office = $this->GeoPostOffices->get($id);
        $this->set(compact('geo_post_office'));
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);

        $geo_post_office = $this->GeoPostOffices->get($id);
        if ($this->GeoPostOffices->delete($geo_post_office)) {
            return $this->redirect(['action' => 'index']);
        }
    }
}