<?php
namespace App\Controller;

use App\Controller\ProjapotiController;
use App\Model\Table;
use App\Model\Table\GeoThanasTable;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\Entity;
use App\Model\Entity\GeoThanas;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

class GeoThanasController extends ProjapotiController
{
//    public $paginate = [
//        'fields' => ['Districts.id'],
//        'limit' => 25,
//        'order' => [
//            'Districts.id' => 'asc'
//        ]
//    ];
//
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Paginator');
    }

    public function index()
    {
        $geo_thana = TableRegistry::get('GeoThanas');
        $query = $geo_thana->find('all')->contain(['GeoDivisions','GeoDistricts']);
           try{
        $this->set('query',$this->paginate($query));
        }catch (NotFoundException $e) {
            $this->redirect(['action'=>'index']);
        }
    }

    public function add()
    {
        $this->loadModel('GeoDivisions');
        $geo_divisions = $this->GeoDivisions->find('all');
        $this->set(compact('geo_divisions'));

        $this->loadModel('GeoDistricts');
        $geo_districts = $this->GeoDistricts->find('all');
        $this->set(compact('geo_districts'));

        $geo_thana = $this->GeoThanas->newEntity();
        if ($this->request->is('post')) {

                $validator = new Validator();
            $validator->notEmpty('geo_division_id', 'বিভাগ নির্বাচন করুন')->notEmpty('thana_name_bng',
                'থানার  নাম(বাংলা) দেওয়া হয় নি')->notEmpty('thana_name_eng',
                'থানার  নাম(ইংরেজি) দেওয়া হয় নি')->notEmpty('bbs_code',
                'জেলা কোড দেওয়া হয় নি')->notEmpty('status',
                'অবস্থা নির্বাচন করুন')->notEmpty('geo_district_id',
                'জেলা নির্বাচন করুন');
            $errors    = $validator->errors($this->request->data());
            if (empty($errors)) {
                $geo_thana = $this->GeoThanas->patchEntity($geo_thana,
                    $this->request->data);
                if ($this->GeoThanas->save($geo_thana)) {
                    $this->Flash->success('সংরক্ষিত হয়েছে।');
                    return $this->redirect(['action' => 'index']);
                }
            } else {
                $this->set(compact('errors'));
            }
        }
        $this->set('geo_thana', $geo_thana);
    }

    public function edit($id = null, $is_ajax = null)
    {
        if ($is_ajax == 'ajax') {
            $this->layout = null;
        }

         $geo_divisions = TableRegistry::get('GeoDivisions')->find('list',
                    ['keyField' => 'id', 'valueField' => 'division_name_bng'])->toArray();
            $this->set(compact('geo_divisions'));

            $geo_districts = TableRegistry::get('GeoDistricts')->find('list',
                    ['keyField' => 'id', 'valueField' => 'district_name_bng'])->toArray();
            $this->set(compact('geo_districts'));

        $geo_thana = $this->GeoThanas->get($id);
        if ($this->request->is(['post', 'put'])) {

            $validator = new Validator();
            $validator->notEmpty('geo_division_id', 'বিভাগ নির্বাচন করুন')->notEmpty('thana_name_bng',
                'থানার  নাম(বাংলা) দেওয়া হয় নি')->notEmpty('thana_name_eng',
                'থানার  নাম(ইংরেজি) দেওয়া হয় নি')->notEmpty('bbs_code',
                'জেলা কোড দেওয়া হয় নি')->notEmpty('status',
                'অবস্থা নির্বাচন করুন')->notEmpty('geo_district_id',
                'জেলা নির্বাচন করুন');
            $errors    = $validator->errors($this->request->data());
            if (empty($errors)) {
                  $this->GeoThanas->patchEntity($geo_thana, $this->request->data);
                if ($this->GeoThanas->save($geo_thana)) {
                    $this->Flash->success('সংরক্ষিত হয়েছে।');
                    return $this->redirect(['action' => 'index']);
                }
            } else {
                 $this->set(compact('errors'));
            }
        }
        $this->set('geo_thana', $geo_thana);
    }


    public function view($id = null)
    {
        $geo_thana = $this->GeoThanas->get($id);
        $this->set(compact('geo_thana'));
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);

        $geo_thana = $this->GeoThanas->get($id);
        if ($this->GeoThanas->delete($geo_thana)) {
            return $this->redirect(['action' => 'index']);
        }
    }
}
