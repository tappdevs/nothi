<?php
namespace App\Controller;

use App\Controller\ProjapotiController;
use App\Model\Table;
use App\Model\Table\GeoUpazilasTable;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\Entity;
use App\Model\Entity\GeoUpazilas;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

class GeoUpazilasController extends ProjapotiController
{
       public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Paginator');
    }
    public function index()
    {
//        $this->paginate = [
//            'contain' => ['GeoDistricts', 'GeoDivisions']
//        ];
//        $this->set('geoUpazilas', $this->paginate($this->GeoUpazilas));
//        $this->set('_serialize', ['geoUpazilas']);

        $geo_upazila = TableRegistry::get('GeoUpazilas');
        $query = $geo_upazila->find('all')->contain(['GeoDivisions','GeoDistricts']);
           try{
        $this->set('query',$this->paginate($query));
        }catch (NotFoundException $e) {
            $this->redirect(['action'=>'index']);
        }
    }

    public function add()
    {
        $this->loadModel('GeoDivisions');
        $geo_divisions = $this->GeoDivisions->find('all');
        $this->set(compact('geo_divisions'));

        $this->loadModel('GeoDistricts');
        $geo_districts = $this->GeoDistricts->find('all');
        $this->set(compact('geo_districts'));

        $geo_upazila = $this->GeoUpazilas->newEntity();
        if ($this->request->is('post')) {

             $validator = new Validator();
            $validator->notEmpty('geo_division_id', 'বিভাগ নির্বাচন করুন')->notEmpty('upazila_name_bng',
                'উপজেলার  নাম(বাংলা) দেওয়া হয় নি')->notEmpty('upazila_name_eng',
                'উপজেলার  নাম(ইংরেজি) দেওয়া হয় নি')->notEmpty('bbs_code',
                'উপজেলার কোড দেওয়া হয় নি')->notEmpty('status',
                'অবস্থা নির্বাচন করুন')->notEmpty('geo_district_id',
                'জেলা নির্বাচন করুন');
            $errors    = $validator->errors($this->request->data());
            if (empty($errors)) {
                $geo_upazila = $this->GeoUpazilas->patchEntity($geo_upazila,
                    $this->request->data);
                $other_info  = TableRegistry::get('GeoDistricts')->find()->select(['bbs_code',
                    'division_bbs_code'])->where([ 'id' => $geo_upazila->geo_district_id])->first();
                $geo_upazila->district_bbs_code = $other_info['bbs_code'];
                $geo_upazila->division_bbs_code = $other_info['division_bbs_code'];
                if ($this->GeoUpazilas->save($geo_upazila)) {
                    $this->Flash->success('সংরক্ষিত হয়েছে।');
                    return $this->redirect(['action' => 'index']);
                }
            } else {
                $this->set(compact('errors'));
            }
        }
        $this->set('geo_upazila', $geo_upazila);
    }

    public function edit($id = null, $is_ajax = null)
    {
        if ($is_ajax == 'ajax') {
            $this->layout = null;
        }

         $geo_divisions = TableRegistry::get('GeoDivisions')->find('list',
                    ['keyField' => 'id', 'valueField' => 'division_name_bng'])->toArray();
            $this->set(compact('geo_divisions'));

            $geo_districts = TableRegistry::get('GeoDistricts')->find('list',
                    ['keyField' => 'id', 'valueField' => 'district_name_bng'])->toArray();
            $this->set(compact('geo_districts'));

        $geo_upazila = $this->GeoUpazilas->get($id);
        if ($this->request->is(['post', 'put'])) {
            $validator = new Validator();
            $validator->notEmpty('district_bbs_code', 'জেলা কোড দেওয়া হয় নি')->notEmpty('upazila_name_bng',
                'উপজেলার  নাম(বাংলা) দেওয়া হয় নি')->notEmpty('upazila_name_eng',
                'উপজেলার  নাম(ইংরেজি) দেওয়া হয় নি')->notEmpty('bbs_code',
                'উপজেলার কোড দেওয়া হয় নি')->notEmpty('status',
                'অবস্থা নির্বাচন করুন')->notEmpty('geo_district_id',
                'জেলা কোড দেওয়া হয় নি');
            $errors    = $validator->errors($this->request->data());
            if (empty($errors)) {
                $geo_upazila = $this->GeoUpazilas->patchEntity($geo_upazila,
                    $this->request->data);
                if ($this->GeoUpazilas->save($geo_upazila)) {
                    $this->Flash->success('সংরক্ষিত হয়েছে।');
                    return $this->redirect(['action' => 'index']);
                }
            } else {
                $this->set(compact('errors'));
            }
        }
        $this->set('geo_upazila', $geo_upazila);
    }


    public function view($id = null)
    {
        $geo_upazila = $this->GeoUpazilas->get($id);
        $this->set(compact('geo_upazila'));
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);

        $geo_upazila = $this->GeoUpazilas->get($id);
        if ($this->GeoUpazilas->delete($geo_upazila)) {
            return $this->redirect(['action' => 'index']);
        }
    }

    public function getUpazillasByDistrict()
    {
        $id = $this->request->data['geo_district_id'];
        $districtTable = TableRegistry::get('GeoUpazilas');
        $data_array = $districtTable->find('list')->where(['geo_district_id' => $id])->order(['id'])->toArray();
        $this->response->body(json_encode($data_array));
        $this->response->type('application/json');
        return $this->response;
    }
}
