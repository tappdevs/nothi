<?php

namespace App\Controller;

use Cake\ORM\TableRegistry;


class GlobalMessagesController extends ProjapotiController {

	public $paginate = [
		'limit' => 25,
		'order' => [
			'Messages.created' => 'desc'
		]
	];

    public function initialize() {
        parent::initialize();
        $this->loadComponent('Paginator');
    }

//    public function index() {
//        $notification = TableRegistry::get('Notifications');
//        $query = $notification->find('all');
//        $this->set(compact('query'));
//    }

    public function global_message() {
		$layerLevels = jsonA(OFFICE_LAYER_TYPE);
		$options = [];
		if(!empty($layerLevels)){
			foreach($layerLevels as $key => $level){
				$options[$key] = __($level);
			}
		}

    	$messagesTable = TableRegistry::get('Messages');
		$messages = $messagesTable->find()->where(['is_deleted' => 0]);
		$this->set('messages', $this->paginate($messages));
		$this->set('options', $options);
	}

	public function create() {
		$messagesTable = TableRegistry::get('Messages');
		$message = $messagesTable->newEntity();
    	$message->title = $this->request->data('title');
    	$message->message = $this->request->data('message');
    	$message->message_by = $this->Auth->user()['id'];
    	$message->is_deleted = 0;
    	if (isset($this->request->data()['layer_id']) && $this->request->data()['layer_id'] > 0) {
			$message->message_for = 'layer';
			$message->related_id = $this->request->data()['layer_id'];
		} elseif (isset($this->request->data()['message_office_unit_organogram_id']) && $this->request->data()['message_office_unit_organogram_id'] > 0) {
			$message->message_for = 'organogram';
			$message->related_id = $this->request->data()['message_office_unit_organogram_id'];
		} elseif (isset($this->request->data()['message_office_unit_id']) && $this->request->data()['message_office_unit_id'] > 0) {
			$message->message_for = 'unit';
			$message->related_id = $this->request->data()['message_office_unit_id'];
		} elseif (isset($this->request->data()['message_office_id']) && $this->request->data()['message_office_id'] > 0) {
			$message->message_for = 'office';
			$message->related_id = $this->request->data()['message_office_id'];
		} elseif (isset($this->request->data()['message_office_origin_id']) && $this->request->data()['message_office_origin_id'] > 0) {
			$message->message_for = 'office_origin';
			$message->related_id = $this->request->data()['message_office_origin_id'];
		} elseif (isset($this->request->data()['message_office_layer_id']) && $this->request->data()['message_office_layer_id'] > 0) {
			$message->message_for = 'office_layer';
			$message->related_id = $this->request->data()['message_office_layer_id'];
		} elseif (isset($this->request->data()['message_office_ministry_id']) && $this->request->data()['message_office_ministry_id'] > 0) {
			$message->message_for = 'office_ministry';
			$message->related_id = $this->request->data()['message_office_ministry_id'];
		} elseif (isset($this->request->data()['message_for']) && $this->request->data()['message_for'] == 'all') {
			$message->message_for = $this->request->data()['message_for'];
    		$message->related_id = 0;
    	} else {
    		$this->Flash->error('সঠিক প্রাপক নির্বাচন করা হয় নাই। দয়া করে পুনরায় চেষ্টা করুন');
    		return $this->redirect('/global_messages');
		}
		if ($messagesTable->save($message)) {
			$this->Flash->success('সফলভাবে পাঠানো হয়েছে');
			return $this->redirect('/global_messages');
		} else {
			$this->Flash->error('সাময়িক সমস্যার জন্য দুঃখিত! পুনরায় চেষ্টা করুন');
			return $this->redirect('/global_messages');
		}
	}

	public function view_message() {
		$message_id = $this->request->data('message_id');
		$messageViewTable = TableRegistry::get('MessageViews');
		$session_data = $this->getCurrentDakSection();
		$messageView = $messageViewTable->find()->where(['message_id' => $message_id, 'organogram_id' => $session_data['office_unit_organogram_id']])->first();
		$this->response->body(json_encode(['status' => 'old', 'msg' => 'Successfully viewed']));
		if ($messageView) {
			$messageView->view_count = $messageView->view_count + 1;
			$this->response->body(json_encode(['status' => 'old', 'msg' => 'Successfully viewed']));
		} else {
			$messageView = $messageViewTable->newEntity();
			$messageView->message_id = $message_id;
			$messageView->is_view = 1;
			$messageView->organogram_id = $session_data['office_unit_organogram_id'];
			$messageView->view_count = 1;
			$this->response->body(json_encode(['status' => 'new', 'msg' => 'Successfully viewed']));
		}
		$messageViewTable->save($messageView);

		$this->response->type('application/json');
		return $this->response;
	}

	public function delete() {
    	$message_id = $this->request->data('message_id');
		$messagesTable = TableRegistry::get('Messages');
		$messages = $messagesTable->get($message_id);
		if ($messages) {
			$messages->is_deleted = 1;
			if($messagesTable->save($messages)) {
				$this->Flash->success('সফলভাবে মুছে ফেলা হয়েছে');
				return $this->redirect('/global_messages');
			} else {
				$this->Flash->error('এই মূহুতর্ে মুছে ফেলা সম্ভব হচ্ছে না, কিছুক্ষণ পর পুনরায় চেষ্টা করুন');
				return $this->redirect('/global_messages');
			}
		}
	}
}
