<?php

namespace App\Controller;

use App\Controller\DashboardController;
use App\Model\Table;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\Network\Email\Email;

class MonitorOfficesController extends DashboardController
{
     public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['monitorDashboardUpdate']);
    }
    public function monitorDashboardUpdate()
    {
        set_time_limit(0);
        ini_set('memory_limit', -1);
        $start_time        = strtotime(date('Y-m-d H:i:s'));
       
        $MonitorOfficesTable = TableRegistry::get('MonitorOffices');
        $officesTable        = TableRegistry::get('Offices');
        $all_offices         = $officesTable->find('list')->where(['active_status' => 1])->toArray();
        $today               = date('Y-m-d');
        $yesterday           = date('Y-m-d', strtotime($today.' -1 day'));

        $total = count($all_offices);
        $error = 0;
        $success = 0;
        $error_id = [];
        foreach ($all_offices as $office_id => $office_name) {
            $result              = [];
            $function            = '';
            $office_record_today = $MonitorOfficesTable->checkUpdate($office_id, $today);
            if ($office_record_today > 0) {
                $record          = $MonitorOfficesTable->getLastUpdateTime($office_id);
                $cur_time        = strtotime(date('Y-m-d H:i:s'));
                $record_time     = strtotime($record['modified']->format("Y-m-d H:i:s"));
                $time_difference = round(abs($record_time - $cur_time) / 60, 2);
                if ($time_difference > 60) {
                    $MonitorOfficesTable->deleteDataofOffices($office_id, $today);
                    $office_record_today = 0;
                }
            }
            if ($office_record_today < 1) {
                $office_record_yesterday = $MonitorOfficesTable->checkUpdate($office_id, $yesterday);
                if ($office_record_yesterday < 1) {
                    $MonitorOfficesTable->deleteDataofOffices($office_id);
                    $result   = $this->reportMonitorDashboardSummary($office_id);
                    $function = 'report';
                }
                else {
                    $result   = $this->updateMonitorDashboardSummary($office_id);
                    $function = 'update';
                }
            }
            else {
                   $success++;
            }
            if (!empty($result)) {
                
                if (!empty($function) && $function == 'report') {
                    $monitorOfficesEntity                       = $MonitorOfficesTable->newEntity();
                    $monitorOfficesEntity->office_id            = $office_id;
                    $monitorOfficesEntity->yesterdayinbox       = $result['totalyesterdayinbox'];
                    $monitorOfficesEntity->yesterdayoutbox      = $result['totalyesterdayoutbox'];
                    $monitorOfficesEntity->yesterdaynothijat    = $result['totalyesterdaynothijato'];
                    $monitorOfficesEntity->yesterdaynothivukto  = $result['totalyesterdaynothivukto'];
                    $monitorOfficesEntity->totalnisponnodak     = $result['totalnisponnodakall'];
                    $monitorOfficesEntity->totalonisponnodak    = $result['totalOnisponnodakall'];
                    $monitorOfficesEntity->yesterdayselfnote    = $result['totalyesterdaysrijitonote'];
                    $monitorOfficesEntity->yesterdaydaksohonote = $result['totalyesterdaydaksohonote'];
                    $monitorOfficesEntity->yesterdaypotrojari   = $result['totalyesterdaypotrojari'];
                    $monitorOfficesEntity->totalnisponnonote    = $result['totalnisponnonoteall'];
                    $monitorOfficesEntity->totalonisponnonote   = $result['totalOnisponnonoteall'];
                    $monitorOfficesEntity->totalnisponno        = $result['totalnisponnoall'];
                    $monitorOfficesEntity->totalonisponno       = $result['totalOnisponnoall'];
                    $monitorOfficesEntity->status               = $result['Status'];
                    $monitorOfficesEntity->updated              = date('Y-m-d');

                    if ($MonitorOfficesTable->save($monitorOfficesEntity)) {
                        $success++;
                    }
                }
                elseif (!empty($function) && $function == 'update') {
                    $office_record_yesterday                    = $MonitorOfficesTable->getOfficeData($office_id,
                            [$yesterday, $yesterday])->order(['date(modified) desc'])->first();
                    $monitorOfficesEntity                       = $MonitorOfficesTable->newEntity();
                    $monitorOfficesEntity->office_id            = $office_id;
                    $monitorOfficesEntity->yesterdayinbox       = $result['totalyesterdayinbox'];
                    $monitorOfficesEntity->yesterdayoutbox      = $result['totalyesterdayoutbox'];
                    $monitorOfficesEntity->yesterdaynothijat    = $result['totalyesterdaynothijato'];
                    $monitorOfficesEntity->yesterdaynothivukto  = $result['totalyesterdaynothivukto'];
                    $monitorOfficesEntity->totalnisponnodak     = $office_record_yesterday['totalnisponnodak']
                        + $result['totaltodaynisponnodak'];
                    $monitorOfficesEntity->totalonisponnodak    = $office_record_yesterday['totalonisponnodak']
                        + $result['totaltodayOnisponnodak'];
                    $monitorOfficesEntity->yesterdayselfnote    = $result['totalyesterdaysrijitonote'];
                    $monitorOfficesEntity->yesterdaydaksohonote = $result['totalyesterdaydaksohonote'];
                    $monitorOfficesEntity->yesterdaypotrojari   = $result['totalyesterdaypotrojari'];
                    $monitorOfficesEntity->totalnisponnonote    = $office_record_yesterday['totalnisponnonote']
                        + $result['totaltodaynisponnonote'];
                    $monitorOfficesEntity->totalonisponnonote   = $office_record_yesterday['totalonisponnonote']
                        + $result['totaltodayOnisponnonote'];
                    $monitorOfficesEntity->totalnisponno        = $office_record_yesterday['totalnisponno']
                        + $result['totaltodaynisponno'];
                    $monitorOfficesEntity->totalonisponno       = $office_record_yesterday['totalonisponno']
                        + $result['totaltodayOnisponno'];
                    $monitorOfficesEntity->status               = $result['Status'];
                    $monitorOfficesEntity->updated              = date('Y-m-d');
                    if ($MonitorOfficesTable->save($monitorOfficesEntity)) {
                       $success++;
                    }else{
                        $error++;
                        $error_id[] =$office_id;
                    }
                }
            }
        }

        $response = "Total: ".$total ." Success: ".$success . " Error: ".$error;
         $end_time     = strtotime(date('Y-m-d H:i:s'));
         $time_difference = round(abs($end_time - $start_time), 2);
         $response .= "<br> Time: " .$time_difference. "s";
         if(!empty($error_id)){
             $response .= '<br>Error for office_id: ';
             foreach ($error_id as $id){
                 $response .= $id." ";
             }
         }
         $this->sendCronMessage('tusharkaiser@gmail.com','eather.ahmed@gmail.com','Cron Report '.date('Y-m-d'),'Monitoring Dashboard',$response);
        die;
    }
    public function compareNisponnoNote(){
         set_time_limit(0);
        $MonitorOfficesTable = TableRegistry::get('MonitorOffices');
        $officesTable        = TableRegistry::get('Offices');
        $all_offices         = $officesTable->find('list')->where(['active_status' => 1])->toArray();
        if(!empty($all_offices)){
            foreach ($all_offices as $office_id => $office_name){
                $max_nisponno = $MonitorOfficesTable->find()->select(['totalnisponnonote','updated'])->where(['office_id' => $office_id])->order(['totalnisponnonote desc'])->first();
                $last_nisponno = $MonitorOfficesTable->find()->select(['totalnisponnonote','updated'])->where(['office_id' => $office_id])->order(['updated desc'])->first();
                if($last_nisponno['totalnisponnonote'] < $max_nisponno['totalnisponnonote']){
                    echo 'Office id: '.$office_id.' max totalnisponnonote: '.$max_nisponno['totalnisponnonote'].' updated: '.$max_nisponno['updated'].'Current: '.$last_nisponno['totalnisponnonote'].' updated: '.$last_nisponno['updated'].'<br>';
                }
            }
        }
        die;
    }

}