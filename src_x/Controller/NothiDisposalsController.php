<?php
namespace App\Controller;

use App\Controller\ProjapotiController;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\TableRegistry;
use Cake\I18n\Number;
use Psr\Log\InvalidArgumentException;

class NothiDisposalsController extends ProjapotiController
{
	public $paginate = [
        'fields' => ['nothi_disposals.id'],
        'limit' => 25,
        'order' => [
            'nothi_disposals.id' => 'asc'
        ]
    ];

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Paginator');
    }
	
	public function index()
    {
        $nothi_disposals = TableRegistry::get('NothiDisposals');
        $query = $nothi_disposals->find('all');
        $this->set(compact('query'));
    }

    public function add()
    {
        $nothi_disposals = $this->NothiDisposals->newEntity();
        if ($this->request->is('post')) {
            $nothi_disposals = $this->NothiDisposals->patchEntity($nothi_disposals, $this->request->data);
            if ($this->NothiDisposals->save($nothi_disposals)) {
                return $this->redirect(['action' => 'index']);
            }
        }
        $this->set('nothi_disposals', $nothi_disposals);
    }
	
	 public function edit($id = null, $is_ajax = null)
    {
        if ($is_ajax == 'ajax') {
            $this->layout = null;
        }
        $nothi_disposals = $this->NothiDisposals->get($id);
        if ($this->request->is(['post', 'put'])) {
            $this->NothiDisposals->patchEntity($nothi_disposals, $this->request->data);
            if ($this->NothiDisposals->save($nothi_disposals)) {
                return $this->redirect(['action' => 'index']);
            }
        }
        $this->set('nothi_disposals', $nothi_disposals);
    }
	
	
	
	
	
	}