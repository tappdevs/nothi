<?php

namespace App\Controller;

use App\Controller\ProjapotiController;
use Cake\Database\Schema\Table;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\TableRegistry;
use Cake\I18n\Number;
use Psr\Log\InvalidArgumentException;

class NothiNoteSheetsController extends ProjapotiController
{

    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['apiNoteOnuccedAdd', 'apiNotesheetPage', 'downloadNoteAttachment', 'postApiNotesheetPage', 'saveNote', 'apiNotesheetPrint', 'apiOnucched', 'apiDeleteNode','downloadNoteAttachments','downloadPotro','downloadNoteAttachmentrefs','downloadGuardFile','downloadDak','downloadPotrojariFile']);
    }

    public function apiNotesheetPage($id, $nothi_office = 0)
    {
        $this->layout = 'online_dak';

        $this->set('id', $id);

        $user_designation = isset($this->request->query['user_designation']) ? $this->request->query['user_designation'] : 0;

        $apikey = isset($this->request->query['api_key']) ? $this->request->query['api_key'] : '';

        if (empty($apikey) || $this->checkToken($apikey) == FALSE) {
            die("Invalid Request");
        }
        $is_new = isset($this->request->query['is_new']) ? $this->request->query['is_new']: 0;
        $height = !empty($this->request->query['height']) ? $this->request->query['height']: !empty($this->request->data['height'])?$this->request->data['height']:0;
        $width = !empty($this->request->query['width']) ? $this->request->query['width']: !empty($this->request->data['width'])?$this->request->data['width']:0;
        $this->set('is_new', $is_new);
        $this->set('height', $height);
        $this->set('width', $width);
        if (!defined("PROJAPOTI_API") || PROJAPOTI_API == false) {
            die("Invalid Request");
        }
        if (empty($user_designation)) {
            die("Invalid Request");
        }
        if (empty($id)) {

            die("Invalid Request");
        }
        if ($apikey != API_KEY) {
            //verify api token
            $this->loadComponent('ApiRelated');
            $getApiTokenData = $this->ApiRelated->getMobileApiTokenData($apikey);
            if (!empty($getApiTokenData)) {
                if (!in_array($user_designation, $getApiTokenData['designations'])) {
                    die("Unauthorized request");
                }
            }
            //verify api token
        }
        $this->set('apikey', $apikey);
        $this->set('user_designation', $user_designation);

        $EmployeeOfficesTable = TableRegistry::get('EmployeeOffices');
        $employee_office = $this->setCurrentDakSection($user_designation);
        //$employee_office = $EmployeeOfficesTable->find()->where(['office_unit_organogram_id' => $user_designation, 'status' => 1])->first();


        $otherNothi = false;
        if ($nothi_office != $employee_office['office_id']) {
            $otherNothi = true;
        } else {
            $nothi_office = $employee_office['office_id'];
        }
		$this->switchOffice($nothi_office, 'MainNothiOffice');

        $this->set('nothi_office', $nothi_office);
        $this->set(compact('otherNothi'));

        $this->set(compact('employee_office'));

        $nothiMasterPermissionsTable = TableRegistry::get("NothiMasterPermissions");
        TableRegistry::remove('NothiMasterCurrentUsers');
        $nothiMasterCurrentUsersTable = TableRegistry::get("NothiMasterCurrentUsers");

        $nothiMasters = array();
        $nothiMastersCurrentUser = array();
        TableRegistry::remove('NothiParts');
        $nothiPartsTable = TableRegistry::get('NothiParts');
        $nothiInformation = $nothiPartsTable->get($id);

        if (!empty($employee_office)) {
            $nothiMasters = $nothiMasterPermissionsTable->hasAccess($nothi_office, $employee_office['office_id'], $employee_office['office_unit_id'], $employee_office['office_unit_organogram_id'], $nothiInformation['nothi_masters_id'], $nothiInformation['id']);

            $nothiMastersCurrentUser = $nothiMasterCurrentUsersTable->find()->where(['nothi_part_no' => $id, 'nothi_master_id' => $nothiInformation['nothi_masters_id'], 'nothi_office' => $nothi_office])->first();
        }

        if (!empty($nothiMastersCurrentUser)) {
            $this->set('nisponno', $nothiMastersCurrentUser['is_finished']);
            if ($nothiMastersCurrentUser['office_unit_organogram_id'] == $employee_office['office_unit_organogram_id']) {
                $this->set('privilige_type', $nothiMasters['privilige_type']);
                $this->set('current_status', 1);
                $this->set('can_input_new_onucched', 1);
            } else {
                $this->set('privilige_type', 2);
                $this->set('current_status', $nothiMastersCurrentUser['view_status']);
                $this->set('can_input_new_onucched', 0);
            }
        } else {
            $this->set('nisponno', 0);
            $this->set('privilige_type', 2);
            $this->set('current_status', 0);
            $this->set('can_input_new_onucched', 0);
        }

        TableRegistry::remove('NothiNoteSheets');
        $nothiSheet = TableRegistry::get('NothiNoteSheets');

        $paginate = array('limit' => 1, 'model' => 'NothiNoteSheets');

        $query = $nothiSheet->find('all')->where(['nothi_part_no' => $id]);

        if ($query->count() != 0 && (empty($this->request->query['page']) || !isset($this->request->query['page']) || (isset($this->request->query['page']) && intval($this->request->query['page']) > $query->count()))) {
            $page = $query->count();
            $paginate = $paginate + array('page' => $page);
        }

        $this->paginate = $paginate;
        $query = $this->paginate($query);

        $nothi_note_sheets_id = 1;
        if (!empty($query)) {
            foreach ($query as $row) {
                $nothi_note_sheets_id = $row->sheet_no;
            }
        }

        TableRegistry::remove('NothiNotes');
        $notes = TableRegistry::get('NothiNotes');

        $notesquery = $notes->find()
            ->select(['NothiNotes.nothi_master_id', 'NothiNotes.nothi_part_no', 'nothi_part_no_en' => 'NothiParts.nothi_part_no', 'nothi_part_no_bn' => 'NothiParts.nothi_part_no_bn', 'NothiNotes.nothi_notesheet_id', 'NothiNotes.note_no', 'NothiNotes.id', 'NothiNotes.nothi_notesheet_id', 'NothiNotes.subject', 'NothiNotes.note_description', 'NothiNotes.note_status', 'NothiNotes.is_potrojari', 'NothiNotes.potrojari_status', 'NothiNotes.created', 'NothiNotes.modified', 'NothiNotes.digital_sign', 'NothiNotes.sign_info'])
            ->where(['NothiNotes.nothi_part_no' => $id])
            ->where(['OR' => [
                'NothiNotes.office_organogram_id' => $employee_office['office_unit_organogram_id'],
                'NothiNotes.note_status <>' => 'DRAFT',
            ]])->join([
                'NothiParts' => [
                    'table' => 'nothi_parts',
                    "conditions" => "NothiParts.id= NothiNotes.nothi_part_no",
                    "type" => "INNER"
                ]
            ]);


        TableRegistry::remove('NothiNoteAttachments');
        $noteAttachments = TableRegistry::get('NothiNoteAttachments');

        $noteAttachmentsquery = $noteAttachments->find()->where(['nothi_part_no' => $id])->toArray();

        $noteAttachmentsmap = array();
        if (!empty($noteAttachmentsquery)) {
            foreach ($noteAttachmentsquery as $key => $value) {
                $noteAttachmentsmap[$value['note_no']][] = array(
                    'user_file_name' => $value['user_file_name'],
                    'attachment_type' => str_replace("; charset=binary", '', $value['attachment_type']),
                    'file_name' => $value['file_name'],
                    'id' => $value['id'],
                    'digital_sign' => $value['digital_sign'],
                    'sign_info' => $value['sign_info'],
                );
            }
        }

		$nothiMasterMovementsTable = TableRegistry::get('NothiMasterMovements');
		$this->set('nothimovement', $nothiMasterMovementsTable->getTotalMove($id));
		$nothiPartsTable = TableRegistry::get("NothiParts");
		$nothiPartsInformation = $nothiRecord = $nothiPartsTable->getAll('NothiParts.id=' . $id)->first();
		$this->set('nothiInformation', $nothiPartsInformation);


        $this->set('noteAttachmentsmap', $noteAttachmentsmap);

        $this->set(compact('query', 'notesquery', 'nothi_note_sheets_id'));

        $this->set('signatures', $this->getSignatures($id));

        $employeeOfficeDesignation = array();
        $allPermittedUser = $nothiMasterPermissionsTable->getMasterNothiListbyMasterId($nothiInformation['nothi_masters_id'], $nothi_office);

        if (!empty($allPermittedUser)) {
            foreach ($allPermittedUser as $key => $value) {
                $employeeOfficeDesignation[$value['office_unit_organograms_id']] = array(
                    $value['designation_level'], 1
                );
            }
        }

//notelists
        $this->set('noteNos', $this->showNoteList($nothiInformation['nothi_masters_id'], $nothi_office, $employee_office));
        $this->set('employeeOfficeDesignation', $employeeOfficeDesignation);
    }

    public function postApiNotesheetPage($id, $nothi_office = 0)
    {
        $this->layout = 'online_dak';
        $this->view = 'post_api_notesheet_page';
        // For cheking if request comes from old android version, we set a message on view.
        $is_new = 1;
        $this->set(compact('is_new'));
        $this->set('id', $id);

        if (!empty($this->request->query)) {
            $this->request->data = $this->request->query;
        }
        $user_designation = isset($this->request->data['user_designation']) ? $this->request->data['user_designation'] : 0;

        $apikey = isset($this->request->data['api_key']) ? $this->request->data['api_key'] : '';
        if (empty($apikey) || $this->checkToken($apikey) == FALSE) {
            die("Invalid Request");
        }

        if (!defined("PROJAPOTI_API") || PROJAPOTI_API == false) {
            die("Invalid Request");
        }
        if (empty($user_designation)) {
            die("Invalid Request");
        }
        if (empty($id)) {

            die("Invalid Request");
        }
        if ($apikey != API_KEY) {
            //verify api token
            $this->loadComponent('ApiRelated');
            $getApiTokenData = $this->ApiRelated->getMobileApiTokenData($apikey);
            if (!empty($getApiTokenData)) {
                if (!in_array($user_designation, $getApiTokenData['designations'])) {
                    die("Unauthorized request");
                }
            }
            //verify api token
        }
        $this->set('api_key', $apikey);
        $this->set('user_designation', $user_designation);

        $EmployeeOfficesTable = TableRegistry::get('EmployeeOffices');
		$employee_office = $this->setCurrentDakSection($user_designation);
        //$employee_office = $EmployeeOfficesTable->find()->where(['office_unit_organogram_id' => $user_designation, 'status' => 1])->first();


        $otherNothi = false;
		if ($nothi_office != $employee_office['office_id']) {
			$otherNothi = true;
		} else {
			$nothi_office = $employee_office['office_id'];
		}
		$this->switchOffice($nothi_office, 'MainNothiOffice');

        $this->set('nothi_office', $nothi_office);
        $this->set(compact('otherNothi'));

        $this->set(compact('employee_office'));

        $nothiMasterPermissionsTable = TableRegistry::get("NothiMasterPermissions");
        TableRegistry::remove('NothiMasterCurrentUsers');
        $nothiMasterCurrentUsersTable = TableRegistry::get("NothiMasterCurrentUsers");

        $nothiMasters = array();
        $nothiMastersCurrentUser = array();
        TableRegistry::remove('NothiParts');
        $nothiPartsTable = TableRegistry::get('NothiParts');
        $nothiInformation = $nothiPartsTable->get($id);

        if (!empty($employee_office)) {
            $nothiMasters = $nothiMasterPermissionsTable->hasAccess($nothi_office, $employee_office['office_id'], $employee_office['office_unit_id'], $employee_office['office_unit_organogram_id'], $nothiInformation['nothi_masters_id'], $nothiInformation['id']);

            $nothiMastersCurrentUser = $nothiMasterCurrentUsersTable->find()->where(['nothi_part_no' => $id, 'nothi_master_id' => $nothiInformation['nothi_masters_id'], 'nothi_office' => $nothi_office])->first();
        }


        if (!empty($nothiMastersCurrentUser)) {
            $this->set('nisponno', $nothiMastersCurrentUser['is_finished']);
            if ($nothiMastersCurrentUser['office_unit_organogram_id'] == $employee_office['office_unit_organogram_id']) {
                $this->set('privilige_type', $nothiMasters['privilige_type']);
                $this->set('current_status', 1);
                $this->set('can_input_new_onucched', 1);
            } else {
                $this->set('privilige_type', 2);
                $this->set('current_status', $nothiMastersCurrentUser['view_status']);
                $this->set('can_input_new_onucched', 0);
            }
        } else {
            $this->set('nisponno', 0);
            $this->set('privilige_type', 2);
            $this->set('current_status', 0);
            $this->set('can_input_new_onucched', 0);
        }

        TableRegistry::remove('NothiNoteSheets');
        $nothiSheet = TableRegistry::get('NothiNoteSheets');

        $paginate = array('limit' => 1, 'model' => 'NothiNoteSheets');

        $query = $nothiSheet->find()->where(['nothi_part_no' => $id]);

        if ($query->count() != 0 && (empty($this->request->query['page']) || !isset($this->request->query['page']) || (isset($this->request->query['page']) && intval($this->request->query['page']) > $query->count()))) {
            $page = $query->count();
            $paginate = $paginate + array('page' => $page);
        }

        $this->paginate = $paginate;
        $query = $this->paginate($query);

        $nothi_note_sheets_id = 1;
        if (!empty($query)) {
            foreach ($query as $row) {
                $nothi_note_sheets_id = $row->sheet_no;
            }
        }

        TableRegistry::remove('NothiNotes');
        $notes = TableRegistry::get('NothiNotes');

        $notesquery = $notes->find()
            ->select(['NothiNotes.nothi_master_id', 'NothiNotes.nothi_part_no', 'nothi_part_no_en' => 'NothiParts.nothi_part_no', 'nothi_part_no_bn' => 'NothiParts.nothi_part_no_bn', 'NothiNotes.nothi_notesheet_id', 'NothiNotes.note_no', 'NothiNotes.id', 'NothiNotes.nothi_notesheet_id', 'NothiNotes.subject', 'NothiNotes.note_description', 'NothiNotes.note_status', 'NothiNotes.is_potrojari', 'NothiNotes.potrojari_status', 'NothiNotes.created', 'NothiNotes.modified', 'NothiNotes.digital_sign', 'NothiNotes.sign_info'])
            ->where(['NothiNotes.nothi_part_no' => $id])
            ->where(['OR' => [
                'NothiNotes.office_organogram_id' => $employee_office['office_unit_organogram_id'],
                'NothiNotes.note_status <>' => 'DRAFT',
            ]])->join([
                'NothiParts' => [
                    'table' => 'nothi_parts',
                    "conditions" => "NothiParts.id= NothiNotes.nothi_part_no",
                    "type" => "INNER"
                ]
            ]);

		$nothiMasterMovementsTable = TableRegistry::get('NothiMasterMovements');
		$this->set('nothimovement', $nothiMasterMovementsTable->getTotalMove($id));
		$nothiPartsTable = TableRegistry::get("NothiParts");
		$nothiPartsInformation = $nothiRecord = $nothiPartsTable->getAll('NothiParts.id=' . $id)->first();
		$this->set('nothiInformation', $nothiPartsInformation);


        TableRegistry::remove('NothiNoteAttachments');
        $noteAttachments = TableRegistry::get('NothiNoteAttachments');

//        $noteAttachmentsquery = $noteAttachments->find()->where(['nothi_part_no' => $id, 'nothi_notesheet_id' => $nothi_note_sheets_id])->toArray();
        $noteAttachmentsquery = $noteAttachments->find()->where(['nothi_part_no' => $id])->toArray();

        $noteAttachmentsmap = array();
        if (!empty($noteAttachmentsquery)) {
            foreach ($noteAttachmentsquery as $key => $value) {
                $noteAttachmentsmap[$value['note_no']][] = array(
                    'user_file_name' => $value['user_file_name'],
                    'attachment_type' => str_replace("; charset=binary", '', $value['attachment_type']),
                    'file_name' => $value['file_name'],
                    'id' => $value['id'],
                    'digital_sign' => $value['digital_sign'],
                    'sign_info' => $value['sign_info'],
                );
            }
        }


        $this->set('noteAttachmentsmap', $noteAttachmentsmap);

        $this->set(compact('query', 'notesquery', 'nothi_note_sheets_id'));

        $this->set('signatures', $this->getSignatures($id));

        $employeeOfficeDesignation = array();
        $allPermittedUser = $nothiMasterPermissionsTable->getMasterNothiListbyMasterId($nothiInformation['nothi_masters_id'], $nothi_office);

        if (!empty($allPermittedUser)) {
            foreach ($allPermittedUser as $key => $value) {
                $employeeOfficeDesignation[$value['office_unit_organograms_id']] = array(
                    $value['designation_level'], 1
                );
            }
        }

//notelists
        $this->set('noteNos', $this->showNoteList($nothiInformation['nothi_masters_id'], $nothi_office, $employee_office));

        $noteDecisionTable = TableRegistry::get('NothiDecisions');

        $noteDecision = $noteDecisionTable->find('list', ['keyField' => 'id', 'valueField' => 'decisions'])->where(['status' => 1])->toArray();
        $this->set('employeeOfficeDesignation', $employeeOfficeDesignation);


        $this->set(compact('noteDecision'));

        $potroAttachmentTable = TableRegistry::get('NothiPotroAttachments');
        TableRegistry::remove('NothiMasterPermissions');
        $nothiMastersPermissionTable = TableRegistry::get('NothiMasterPermissions');
        $permitted_nothi_part_list = $nothiMastersPermissionTable->NothiPermissionListForSingleNothi($nothi_office, $employee_office['office_id'], $employee_office['office_unit_id'], $employee_office['office_unit_organogram_id'], $nothiInformation['nothi_masters_id']);

        $potroAttachmentRecord = $potroAttachmentTable->getBibecchoPotroWithSubject($permitted_nothi_part_list);

        $this->set(compact('potroAttachmentRecord'));
    }

    public function notesheetPage($id, $nothi_office = 0)
    {
        set_time_limit('0');
        $this->layout = null;

        $this->set('id', $id);

        $employee_office = $this->getCurrentDakSection();

        $otherNothi = false;
        if ($nothi_office != 0 && $nothi_office != $employee_office['office_id']) {
            $otherNothi = true;

            $this->switchOffice($nothi_office, 'MainNothiOffice');
        } else {
            $nothi_office = $employee_office['office_id'];
        }

        $this->set('nothi_office', $nothi_office);
        $this->set(compact('otherNothi'));

        $this->set(compact('employee_office'));

        TableRegistry::remove("nothiMasterMovements");
        TableRegistry::remove("NothiParts");
        TableRegistry::remove("NothiNoteSheets");
        TableRegistry::remove("NothiPotroAttachments");
        TableRegistry::remove("NothiDecisions");
        TableRegistry::remove("NothiNoteAttachments");
        TableRegistry::remove("NothiNotes");

        $nothiMasterPermissionsTable = TableRegistry::get("NothiMasterPermissions");
        $nothiMasterMovementsTable = TableRegistry::get("nothiMasterMovements");
        TableRegistry::remove('NothiMasterCurrentUsers');
        $nothiMasterCurrentUsersTable = TableRegistry::get("NothiMasterCurrentUsers");

        $nothiMasters = array();
        $nothiMastersCurrentUser = array();
        $nothiPartsTable = TableRegistry::get('NothiParts');
        $nothiInformation = $nothiPartsTable->get($id);

        $archive_level = getNothiArchiveStatus($nothiInformation['nothi_masters_id'], $nothiInformation['office_id']);
        $this->set('archive_level', $archive_level);

        $nothimovement = $nothiMasterMovementsTable->getTotalMove($id);

        $this->set(compact('nothimovement'));
        $this->set(compact('nothiInformation'));

        if (!empty($employee_office)) {
            $nothiMasters = $nothiMasterPermissionsTable->hasAccess($nothi_office, $employee_office['office_id'], $employee_office['office_unit_id'], $employee_office['office_unit_organogram_id'], $nothiInformation['nothi_masters_id'], $nothiInformation['id']);

            $nothiMastersCurrentUser = $nothiMasterCurrentUsersTable->find()->where(['nothi_part_no' => $id, 'nothi_master_id' => $nothiInformation['nothi_masters_id'], 'nothi_office' => $nothi_office])->first();
        }

        if (!empty($nothiMastersCurrentUser)) {
            $this->set('nisponno', $nothiMastersCurrentUser['is_finished']);
            if ($nothiMastersCurrentUser['office_unit_organogram_id'] == $employee_office['office_unit_organogram_id']) {
                $this->set('privilige_type', $nothiMasters['privilige_type']);
                $this->set('current_status', 1);
            } else {
                $this->set('privilige_type', 2);
                $this->set('current_status', $nothiMastersCurrentUser['view_status']);
            }
        } else {
            $this->set('nisponno', 0);
            $this->set('privilige_type', 2);
            $this->set('current_status', 0);
        }

        $nothiSheet = TableRegistry::get('NothiNoteSheets');

        $paginate = array('limit' => 10, 'model' => 'NothiNoteSheets');

        $query = $nothiSheet->find('all')->where(['nothi_part_no' => $id]);

        if ($query->count() != 0 && (empty($this->request->query['page']) || !isset($this->request->query['page']) || (isset($this->request->query['page']) && intval($this->request->query['page']) > $query->count()))) {
            $page = $query->count();
            $paginate = $paginate + array('page' => $page);
        }

        $this->paginate = $paginate;
        $query = $this->paginate($query);

        $nothi_note_sheets_id = 1;
        if (!empty($query)) {
            foreach ($query as $row) {
                $nothi_note_sheets_id = $row->sheet_no;
            }
        }

        $notes = TableRegistry::get('NothiNotes');

        $notesquery = $notes->find()
            ->select(['NothiNotes.nothi_master_id', 'NothiNotes.nothi_part_no', 'nothi_part_no_en' => 'NothiParts.nothi_part_no', 'nothi_part_no_bn' => 'NothiParts.nothi_part_no_bn', 'NothiNotes.nothi_notesheet_id', 'NothiNotes.note_no', 'NothiNotes.id', 'NothiNotes.nothi_notesheet_id', 'NothiNotes.subject', 'NothiNotes.note_description', 'NothiNotes.note_status', 'NothiNotes.is_potrojari', 'NothiNotes.potrojari_status', 'NothiNotes.created', 'NothiNotes.modified'])
            ->where(['NothiNotes.nothi_part_no' => $id, 'NothiNotes.nothi_notesheet_id' => $nothi_note_sheets_id, '(NothiNotes.office_organogram_id = ' . $employee_office['office_unit_organogram_id'] . ' OR NothiNotes.note_status <> "DRAFT")'])->join([
                'NothiParts' => [
                    'table' => 'nothi_parts',
                    "conditions" => "NothiParts.id= NothiNotes.nothi_part_no",
                    "type" => "INNER"
                ]
            ]);

        $noteAttachments = TableRegistry::get('NothiNoteAttachments');

        $noteAttachmentsquery = $noteAttachments->find()->where(['nothi_part_no' => $id, 'nothi_notesheet_id' => $nothi_note_sheets_id])->toArray();

        $noteAttachmentsmap = array();
        if (!empty($noteAttachmentsquery)) {
            foreach ($noteAttachmentsquery as $key => $value) {
                $noteAttachmentsmap[$value['note_no']][] = array(
                    'user_file_name' => $value['user_file_name'],
                    'attachment_type' => str_replace("; charset=binary", '', $value['attachment_type']),
                    'file_name' => $value['file_name'],
                    'id' => $value['id'],
                    'digital_sign' => $value['digital_sign'],
                    'sign_info' => $value['sign_info'],
                );
            }
        }


        $this->set('noteAttachmentsmap', $noteAttachmentsmap);

        $this->set(compact('query', 'notesquery', 'nothi_note_sheets_id'));

        $this->set('signatures', $this->getSignatures($id));

        $employeeOfficeDesignation = array();
        $allPermittedUser = $nothiMasterPermissionsTable->getMasterNothiListbyMasterId($nothiInformation['nothi_masters_id'], $nothi_office);

        if (!empty($allPermittedUser)) {
            foreach ($allPermittedUser as $key => $value) {
                $employeeOfficeDesignation[$value['office_unit_organograms_id']] = array(
                    $value['designation_level'], 1
                );
            }
        }

//notelists
        $this->set('noteNos', $this->showNoteList($nothiInformation['nothi_masters_id'], $nothi_office, $employee_office));

        $noteDecisionTable = TableRegistry::get('NothiDecisions');

        $noteDecision = $noteDecisionTable->find('list', ['keyField' => 'id', 'valueField' => 'decisions'])->where(['status' => 1])->toArray();
        $this->set('employeeOfficeDesignation', $employeeOfficeDesignation);


        $this->set(compact('noteDecision'));
//        $potroAttachmentTable = TableRegistry::get('NothiPotroAttachments');

//        $permitted_nothi_list = $nothiMasterPermissionsTable->allNothiPermissionList($nothi_office, $employee_office['office_id'], $employee_office['office_unit_id'], $employee_office['office_unit_organogram_id']);


//        $potroAttachmentRecord = $potroAttachmentTable->find('list', ['keyField' => 'id', 'valueField' => 'nothi_potro_page_bn'])->where(['NothiPotroAttachments.nothi_master_id' => $nothiInformation['nothi_masters_id']])
//                        ->where(['NothiPotroAttachments.nothijato' => 0,'NothiPotroAttachments.status'=>1])
//                        ->where(['NothiPotroAttachments.nothi_part_no IN' => $permitted_nothi_list])
//                        ->order(['NothiPotroAttachments.id desc'])->toArray();
//
//        $this->set(compact('potroAttachmentRecord'));
    }

    public function notesheetPageNew($id, $nothi_office_id = 0, $ordering_type = 'ASC')
    {
        set_time_limit('0');
        $this->layout = null;
        $this->view = '/NothiMasters/notesShowPage';

        $get_current_dak_section = $this->getCurrentDakSection();
        $otherNothi = false;
        if ($nothi_office_id && $nothi_office_id != $get_current_dak_section['office_id']) {
            try {
                $otherNothi = true;
                $this->switchOffice($nothi_office_id, 'MainNothiOffice');
            } catch (\Exception $exception) {
                $this->Flash('Nothi office could not connected, please try again after sometime');
                $this->referer($this->referer());
            }
        } else {
            $nothi_office_id = $get_current_dak_section['office_id'];
        }

        TableRegistry::remove('NothiParts');
        $nothi_parts_table = TableRegistry::get('NothiParts');
        $nothi_part = $nothi_parts_table->get($id);

        $archive_level = getNothiArchiveStatus($nothi_part['nothi_masters_id'], $nothi_part['office_id']);
        $this->set('archive_level', $archive_level);

        $nothi_master_permissions_table = TableRegistry::get('NothiMasterPermissions');
        $nothi_master_permissions = $nothi_master_permissions_table->hasAccess($nothi_office_id, $get_current_dak_section['office_id'], $get_current_dak_section['office_unit_id'], $get_current_dak_section['office_unit_organogram_id'], $nothi_part['nothi_masters_id'], $nothi_part['id']);

        $nothi_master_current_users_table = TableRegistry::get('NothiMasterCurrentUsers');
        $nothi_master_current_users = $nothi_master_current_users_table->find()->where(['nothi_part_no' => $id, 'nothi_master_id' => $nothi_part['nothi_masters_id'], 'nothi_office' => $nothi_office_id])->first();

        if (!empty($nothi_master_current_users)) {
            $this->set('nisponno', $nothi_master_current_users['is_finished']);
            if ($nothi_master_current_users['office_unit_organogram_id'] == $get_current_dak_section['office_unit_organogram_id']) {
                $this->set('privilige_type', $nothi_master_permissions['privilige_type']);
                $this->set('current_status', 1);
            } else {
                $this->set('privilige_type', 2);
                $this->set('current_status', $nothi_master_current_users['view_status']);
            }
        } else {
            $this->set('nisponno', 0);
            $this->set('privilige_type', 2);
            $this->set('current_status', 0);
        }


        $nothi_notes_table = TableRegistry::get('NothiNotes');
        $limit = 10;
        //limitStart
        $nothi_notes = $nothi_notes_table->find()
            ->select(['NothiNotes.nothi_master_id', 'NothiNotes.nothi_part_no',
                'nothi_part_no_en' => 'NothiParts.nothi_part_no',
                'nothi_part_no_bn' => 'NothiParts.nothi_part_no_bn',
                'NothiNotes.nothi_notesheet_id', 'NothiNotes.note_no',
                'NothiNotes.id', 'NothiNotes.nothi_notesheet_id', 'NothiNotes.subject',
                'NothiNotes.note_description', 'NothiNotes.note_status', 'NothiNotes.is_potrojari',
                'NothiNotes.potrojari_status', 'NothiNotes.created',
                'NothiNotes.modified', 'NothiNotes.digital_sign', 'NothiNotes.sign_info'])
            ->where(['NothiNotes.nothi_part_no' => $id])
            ->where(["OR" => [
                'office_organogram_id' => $get_current_dak_section['office_unit_organogram_id'],
                'NothiNotes.note_status <>' => 'DRAFT',
            ]])->join([
                'NothiParts' => [
                    'table' => 'nothi_parts',
                    "conditions" => "NothiParts.id= NothiNotes.nothi_part_no",
                    "type" => "INNER"
                ]
            ]);
		$nothi_note_subject = $nothi_notes->order(['NothiNotes.id'=> 'ASC'])->first();

		$nothi_notes = $nothi_notes->order(['NothiNotes.id'=> $ordering_type])->limit($limit);

        $total_page = ceil($nothi_notes->count() / $limit);
        if (!isset($this->request->data['limitStart'])) {
            $this->request->data['limitStart'] = 0;
        }
        $requested_page_no = ceil($this->request->data['limitStart'] / $limit);
        if ($requested_page_no > $total_page) {
            $requested_page_no = $total_page;
        }
        $nothi_notes = $nothi_notes->page($requested_page_no + 1);
        if (count($nothi_notes->toArray()) == 0) {
            //echo "<p class='text-center text-danger'>এই পেজ এ কোন অনুচ্ছেদ দেয়া হয়নি</p>";
            die;
        }
        $this->set(compact('nothi_notes', 'otherNothi', 'nothi_note_subject'));

        $noteAttachments = TableRegistry::get('NothiNoteAttachments');
        $noteAttachmentsquery = $noteAttachments->find()->where(['nothi_part_no' => $id])->toArray();
        $noteAttachmentsmap = array();
        if (!empty($noteAttachmentsquery)) {
            foreach ($noteAttachmentsquery as $key => $value) {
                $noteAttachmentsmap[$value['note_no']][] = array(
                    'user_file_name' => $value['user_file_name'],
                    'attachment_type' => str_replace("; charset=binary", '', $value['attachment_type']),
                    'file_name' => $value['file_name'],
                    'id' => $value['id'],
                    'digital_sign' => $value['digital_sign'],
                    'sign_info' => $value['sign_info'],
                );
            }
        }

        $this->set('noteAttachmentsmap', $noteAttachmentsmap);
        $this->set('signatures', $this->getSignatures($id));
        $employeeOfficeDesignation = array();
        $allPermittedUser = $nothi_master_permissions_table->getMasterNothiListbyMasterId($nothi_part['nothi_masters_id'], $nothi_office_id);
        if (!empty($allPermittedUser)) {
            foreach ($allPermittedUser as $key => $value) {
                $employeeOfficeDesignation[$value['office_unit_organograms_id']] = array(
                    $value['designation_level'], 1
                );
            }
        }
        $noteDecisionTable = TableRegistry::get('NothiDecisions');
        $noteDecision = $noteDecisionTable->find('list', ['keyField' => 'id', 'valueField' => 'decisions'])->where(['status' => 1])->toArray();
        $this->set('employeeOfficeDesignation', $employeeOfficeDesignation);
        $this->set(compact('noteDecision', 'nothi_office_id'));
    }

    public function notesheetPageAll($id, $nothi_office = 0, $print = 0)
    {
        if ($print) {
            $this->layout = 'single';
        } else {
            $this->layout = null;
        }

        $employee_office = $this->getCurrentDakSection();
        $otherNothi = false;
        if ($nothi_office != 0 && $nothi_office != $employee_office['office_id']) {
            $otherNothi = true;

            $this->switchOffice($nothi_office, 'MainNothiOffice');
        } else {
            $nothi_office = $employee_office['office_id'];
        }

        $this->set('nothi_office', $nothi_office);
        $this->set('otherNothi', $otherNothi);

        TableRegistry::remove("nothiMasterMovements");
        TableRegistry::remove("NothiParts");
        TableRegistry::remove("NothiNoteSheets");
        TableRegistry::remove("NothiPotroAttachments");
        TableRegistry::remove("NothiDecisions");
        TableRegistry::remove("NothiNoteAttachments");
        TableRegistry::remove("NothiNotes");
        $nothiPartsTable = TableRegistry::get("NothiParts");
        $nothiMasterPermissionsTable = TableRegistry::get("NothiMasterPermissions");

        $nothiPartsInformation = $nothiPartsTable->getAll('NothiParts.id=' . $id)->first();

        $allNothiMasterList = array();
        $allNothiMasterList = $nothiMasterPermissionsTable->getMasterNothiPartList($nothi_office, $employee_office['office_id'], $employee_office['office_unit_id'], $employee_office['office_unit_organogram_id'], $nothiPartsInformation['nothi_masters_id']);

        $nothiPartId = array();

        if (!empty($allNothiMasterList)) {
            foreach ($allNothiMasterList as $key => $value) {
                $nothiPartId[] = $value['nothi_part_no'];
            }
        }
        $employeeOfficesTable = TableRegistry::get("EmployeeOffices");

        $nothiMasters = array();
        $nothiMastersCurrentUser = array();

        $notes = TableRegistry::get('NothiNotes');

        $query = $notes->find()
            ->select(['NothiNotes.nothi_master_id', 'NothiNotes.nothi_part_no', 'nothi_part_no_en' => 'NothiParts.nothi_part_no', 'nothi_part_no_bn' => 'NothiParts.nothi_part_no_bn', 'NothiNotes.nothi_notesheet_id', 'NothiNotes.note_no', 'NothiNotes.id', 'NothiNotes.nothi_notesheet_id', 'NothiNotes.subject', 'NothiNotes.note_description', 'NothiNotes.note_status', 'NothiNotes.is_potrojari', 'NothiNotes.potrojari_status', 'NothiNotes.created', 'NothiNotes.modified', 'NothiNotes.id', 'NothiNotes.digital_sign', 'NothiNotes.sign_info'])
            ->where(['NothiNotes.nothi_part_no IN' => $nothiPartId, '(NothiNotes.office_organogram_id = ' . $employee_office['office_unit_organogram_id'] . ' OR NothiNotes.note_status <> "DRAFT")'])->join([
                'NothiParts' => [
                    'table' => 'nothi_parts',
                    "conditions" => "NothiParts.id= NothiNotes.nothi_part_no",
                    "type" => "INNER"
                ]
            ]);
        $paginate = array('limit' => 20, 'model' => 'NothiNotes');

        $this->paginate = $paginate;
        $notesquery = $this->paginate($query);

        $this->set('notesquery', $notesquery);

        $noteAttachments = TableRegistry::get('NothiNoteAttachments');

        $noteAttachmentsquery = $noteAttachments->find()->where(['nothi_part_no IN' => $nothiPartId])->toArray();

        $noteAttachmentsmap = array();
        if (!empty($noteAttachmentsquery)) {
            foreach ($noteAttachmentsquery as $key => $value) {
                $noteAttachmentsmap[$value['note_no']][] = array(
                    'attachment_type' => str_replace("; charset=binary", '', $value['attachment_type']),
                    'file_name' => $value['file_name'],
                    'id' => $value['id'],
                    'digital_sign' => $value['digital_sign'],
                    'sign_info' => $value['sign_info'],
                );
            }
        }

        $this->set('noteAttachmentsmap', $noteAttachmentsmap);


        $this->set('signatures', $this->getSignaturesAll($nothiPartId, 0, $nothi_office));

        $employeeOfficeDesignation = array();
        $allPermittedUser = $nothiMasterPermissionsTable->getMasterNothiListbyMasterId($nothiPartsInformation['nothi_masters_id'], $nothi_office);

        if (!empty($allPermittedUser)) {
            foreach ($allPermittedUser as $key => $value) {

                $employeeOfficeDesignation[$value['office_unit_organograms_id']] = array(
                    $value['designation_level'], 1
                );
            }
        }
//notelists
        $this->set('noteNos', $this->showNoteList($nothiPartsInformation['nothi_masters_id'], $nothi_office, $employee_office));

        $this->set('employeeOfficeDesignation', $employeeOfficeDesignation);
        $this->set('nothi_office', $nothi_office);
    }

    public function notesheetPageAllNew($id, $nothi_office = 0, $print = 0)
    {
        if ($print) {
            $this->layout = 'single';
        } else {
            $this->layout = null;
        }
        $this->view = '/NothiMasters/notesShowPageAll';

        $employee_office = $this->getCurrentDakSection();
        $otherNothi = false;
        if ($nothi_office != 0 && $nothi_office != $employee_office['office_id']) {
            $otherNothi = true;

            $this->switchOffice($nothi_office, 'MainNothiOffice');
        } else {
            $nothi_office = $employee_office['office_id'];
        }

        $this->set('nothi_office', $nothi_office);
        $this->set('otherNothi', $otherNothi);

        TableRegistry::remove("nothiMasterMovements");
        TableRegistry::remove("NothiParts");
        TableRegistry::remove("NothiNoteSheets");
        TableRegistry::remove("NothiPotroAttachments");
        TableRegistry::remove("NothiDecisions");
        TableRegistry::remove("NothiNoteAttachments");
        TableRegistry::remove("NothiNotes");
        $nothiPartsTable = TableRegistry::get("NothiParts");
        $nothiMasterPermissionsTable = TableRegistry::get("NothiMasterPermissions");

        $nothiPartsInformation = $nothiPartsTable->getAll('NothiParts.id=' . $id)->first();

        $allNothiMasterList = array();
        $allNothiMasterList = $nothiMasterPermissionsTable->getMasterNothiPartList($nothi_office, $employee_office['office_id'], $employee_office['office_unit_id'], $employee_office['office_unit_organogram_id'], $nothiPartsInformation['nothi_masters_id']);

        $nothiPartId = array_map(function ($val) {
            return $val['nothi_part_no'];
        }, $allNothiMasterList);

        $nothi_notes_table = TableRegistry::get('NothiNotes');
        $limit = 10;
        //limitStart
		if(isset($_GET['permitted_by']) && $_GET['permitted_by'] != '') {
			$employee_office_office_unit_organogram_id = $_GET['permitted_by'];
		} else {
			$employee_office_office_unit_organogram_id = $employee_office['office_unit_organogram_id'];
		}
        $nothi_notes = $nothi_notes_table->find()
            ->select(['NothiNotes.nothi_master_id', 'NothiNotes.nothi_part_no', 'NothiNotes.id', 'NothiNotes.digital_sign', 'NothiNotes.sign_info',
                'nothi_part_no_en' => 'NothiParts.nothi_part_no', 'nothi_part_no_bn' => 'NothiParts.nothi_part_no_bn', 'NothiNotes.nothi_notesheet_id', 'NothiNotes.note_no', 'NothiNotes.id', 'NothiNotes.nothi_notesheet_id', 'NothiNotes.subject', 'NothiNotes.note_description', 'NothiNotes.note_status', 'NothiNotes.is_potrojari', 'NothiNotes.potrojari_status', 'NothiNotes.created', 'NothiNotes.modified'])
            ->where([
                'NothiNotes.nothi_part_no IN' => $nothiPartId
            ])
            ->where(["OR" => [
                'office_organogram_id' => $employee_office_office_unit_organogram_id,
                'NothiNotes.note_status <>' => 'DRAFT',
            ]])->join([
                'NothiParts' => [
                    'table' => 'nothi_parts',
                    "conditions" => "NothiParts.id= NothiNotes.nothi_part_no",
                    "type" => "INNER"
                ]
            ])->order(['NothiNotes.nothi_part_no'])->limit($limit);


        $total_page = ceil($nothi_notes->count() / $limit);
        if (!isset($this->request->data['limitStart'])) {
            $this->request->data['limitStart'] = 0;
        }
        $requested_page_no = ceil($this->request->data['limitStart'] / $limit);
        if ($requested_page_no > $total_page) {
            $requested_page_no = $total_page;
        }
        $nothi_notes = $nothi_notes->page($requested_page_no + 1);
        if (count($nothi_notes->toArray()) == 0) {
            die;
        }
        $this->set('notesquery', $nothi_notes);

        $noteAttachments = TableRegistry::get('NothiNoteAttachments');

        $noteAttachmentsquery = $noteAttachments->find()->where(['nothi_part_no IN' => $nothiPartId])->toArray();

        $noteAttachmentsmap = array();
        if (!empty($noteAttachmentsquery)) {
            foreach ($noteAttachmentsquery as $key => $value) {
                $noteAttachmentsmap[$value['note_no']][] = array(
                    'user_file_name' => $value['user_file_name'],
                    'attachment_type' => str_replace("; charset=binary", '', $value['attachment_type']),
                    'file_name' => $value['file_name'],
                    'id' => $value['id'],
                    'digital_sign' => $value['digital_sign'],
                    'sign_info' => $value['sign_info'],
                );
            }
        }

        $this->set('noteAttachmentsmap', $noteAttachmentsmap);

        $this->set('signatures', $this->getSignaturesAll($nothiPartId, 0, $nothi_office));

        $employeeOfficeDesignation = array();
        $allPermittedUser = $nothiMasterPermissionsTable->getMasterNothiListbyMasterId($nothiPartsInformation['nothi_masters_id'], $nothi_office);

        if (!empty($allPermittedUser)) {
            foreach ($allPermittedUser as $key => $value) {

                $employeeOfficeDesignation[$value['office_unit_organograms_id']] = array(
                    $value['designation_level'], 1
                );
            }
        }

        $this->set('noteNos', $this->showNoteList($nothiPartsInformation['nothi_masters_id'], $nothi_office, $employee_office));

        $this->set('employeeOfficeDesignation', $employeeOfficeDesignation);
        $this->set('nothi_office', $nothi_office);
    }

    public function notesheetSearch($nothi_office = 0)
    {
        $this->layout = 'ajax';
        $employee_office = $this->getCurrentDakSection();

        if ($nothi_office != 0 && $nothi_office != $employee_office['office_id']) {
            $this->switchOffice($nothi_office, 'MainNothiOffice');
        } else {
            $nothi_office = $employee_office['office_id'];
        }

        $this->set('nothi_office', $nothi_office);

        // code for advance search
        $nothi_master_id = intval($this->request->data['nothi_master_id']);
        $nothi_start = intval($this->request->data['note_start']);
        $nothi_end = intval($this->request->data['note_end']);

        $this->getSearchedNotes($employee_office, $nothi_master_id, $nothi_office, $nothi_start, $nothi_end);
    }

    public function apiNotesheetPrint($designation, $nothi_office, $nothi_master_id, $nothi_start, $nothi_end)
    {
        $query = $this->request->query;
        $this->view = 'notesheet_search';
        $this->layout = 'body';
        $employee_office = $this->setCurrentDakSection($designation);

        $this->switchOffice($nothi_office, 'MainNothiOffice');

        $this->set('query',$query);
        $this->getSearchedNotes($employee_office, $nothi_master_id, $nothi_office, $nothi_start, $nothi_end);
    }

    private function getSearchedNotes($employee_office, $nothi_master_id, $nothi_office, $nothi_start, $nothi_end)
    {
        $nothi_start = min($nothi_start, $nothi_end);
        $nothi_end = max($nothi_start, $nothi_end);
        TableRegistry::remove("NothiMasterPermissions");
        TableRegistry::remove("NothiNoteAttachments");
        TableRegistry::remove("NothiNotes");
        TableRegistry::remove("NothiMasterPermissions");

        $tableNothiMasterPermissions = TableRegistry::get("NothiMasterPermissions");
        $tableNothiNotes = TableRegistry::get('NothiNotes');
        $tableNothiNoteAttachments = TableRegistry::get('NothiNoteAttachments');

        $allNothiPart = $tableNothiMasterPermissions->find('list', ['keyField' => 'nothi_part_no', 'valueField' => 'nothi_part_no'])
            ->where(['office_unit_organograms_id' => $employee_office['office_unit_organogram_id'], 'nothi_masters_id' => $nothi_master_id, 'nothi_office' => $nothi_office])
            ->where(['nothi_part_no >=' => $nothi_start, 'nothi_part_no <=' => $nothi_end])->order(['nothi_part_no' => 'ASC'])->toArray();

        $notesquery = $tableNothiNotes->find()
            ->select(['NothiNotes.nothi_master_id',
                'NothiNotes.nothi_part_no', 'nothi_part_no_en' => 'NothiParts.nothi_part_no',
                'nothi_part_no_bn' => 'NothiParts.nothi_part_no_bn', 'NothiNotes.nothi_notesheet_id',
                'NothiNotes.note_no', 'NothiNotes.office_organogram_id', 'NothiNotes.is_potrojari', 'NothiNotes.id', 'NothiNotes.nothi_notesheet_id', 'NothiNotes.subject',
                'NothiNotes.note_description', 'NothiNotes.note_status', 'digital_sign', 'sign_info'])
            ->where(['NothiNotes.nothi_part_no IN' => array_values($allNothiPart)])
            ->join([
                'NothiParts' => [
                    'table' => 'nothi_parts',
                    "conditions" => "NothiParts.id= NothiNotes.nothi_part_no",
                    "type" => "INNER"
                ]
            ])->toArray();

        $noteAttachmentsquery = $tableNothiNoteAttachments->find()->where(['nothi_part_no IN' => array_values($allNothiPart)])->toArray();

        $noteAttachmentsmap = array();
        if (!empty($noteAttachmentsquery)) {
            foreach ($noteAttachmentsquery as $key => $value) {
                $noteAttachmentsmap[$value['note_no']][] = array(
                    'attachment_type' => str_replace("; charset=binary", '', $value['attachment_type']),
                    'file_name' => isset($value['user_file_name']) ? $value['user_file_name'] : $value['file_name'],
                    'id' => $value['id'],
                    'digital_sign' => $value['digital_sign'],
                    'sign_info' => $value['sign_info'],
                );
            }
        }

        $this->set('signatures', $this->getSignaturesAll($allNothiPart));
        $this->set('notesquery', $notesquery);
        $this->set('noteAttachmentsmap', $noteAttachmentsmap);
        $this->set('employee_office', $employee_office);
        $this->view = '/NothiNoteSheets/notesheet_search';
    }

    public function saveNote()
    {
        if ($this->request->is('ajax')) {
            try{
                $this->layout = null;
                $id = intval($this->request->data['id']);

                if (!empty($this->request->data['user_designation'])) {

                    $employee_office = $this->setCurrentDakSection($this->request->data['user_designation']);
                } else {
                    $employee_office = $this->getCurrentDakSection();
                }
//            echo 0;die;
                $nothi_office = intval($this->request->data['nothi_office']);
                $otherNothi = false;
                if ($nothi_office != 0 && $nothi_office != $employee_office['office_id']) {
                    $otherNothi = true;
                    $this->switchOffice($nothi_office, 'MainNothiOffice');
                } else {
                    $nothi_office = $employee_office['office_id'];
                }

                TableRegistry::remove('NothiParts');
                TableRegistry::remove('NothiNotes');
                TableRegistry::remove('NothiNoteAttachments');
                TableRegistry::remove('NothiPotroAttachments');
                TableRegistry::remove('NothiMasterCurrentUsers');
                $nothiPartsTable = TableRegistry::get('NothiParts');



                $nothiPartsInformation = $nothiPartsTable->get(intval($this->request->data['nothimasterid']));

                $nothimasterid = $nothiPartsInformation['nothi_masters_id'];
                $nothipartid = intval($this->request->data['nothimasterid']);
                $notesheetid = intval($this->request->data['notesheetid']);
                $subject = ($this->request->data['notesubject']);
                $notedecision = explode('/', $this->request->data['notedecision']);
                $body = base64_encode($this->request->data['body']);
                $attachments = isset($this->request->data['attachments']) ? $this->request->data['attachments'] : array();
                $attachmentnames = isset($this->request->data['user_file_name']) ? $this->request->data['user_file_name'] : array();

                $notesTable = TableRegistry::get('NothiNotes');
                $noteAttachmentsTable = TableRegistry::get('NothiNoteAttachments');
                $potroAttachmentTable = TableRegistry::get('NothiPotroAttachments');
                TableRegistry::remove('NothiMasterCurrentUsers');
                $nothiMasterCurrentUsersTable = TableRegistry::get('NothiMasterCurrentUsers');

                if(empty($id)){
                    // check if duplicate request came
                    $lastNoteInfo =  $notesTable->find()->where(['nothi_part_no'=>$nothipartid])->order(['id desc'])->first();
                    if(!empty($lastNoteInfo)){
                        // check if duplicate
                        if($lastNoteInfo['note_description'] == $body && $lastNoteInfo['office_organogram_id'] == $employee_office['office_unit_organogram_id']){
                            echo $lastNoteInfo['id'];
                            die;
                        }
                    }
                }

                $hasAccess = $nothiMasterCurrentUsersTable->getCurrentUserbyDesignation($nothipartid, $employee_office['office_unit_organogram_id'], $employee_office['office_unit_id'], $nothi_office);

                if (!empty($hasAccess)) {

                    $lastNote = $notesTable->find()->select(["note_no" => 'cast(note_no as signed)'])->where(['nothi_part_no' => $nothipartid])->order(['cast(note_no as signed) DESC'])->first();

                    if (empty($id)) {
                        $notesTableData = array();
                        $notesTableData['nothi_master_id'] = $nothimasterid;
                        $notesTableData['nothi_part_no'] = $nothipartid;
                        $notesTableData['nothi_notesheet_id'] = $notesheetid;

                        $notesTableData['office_id'] = $employee_office['office_id'];
                        $notesTableData['office_unit_id'] = $employee_office['office_unit_id'];
                        $notesTableData['office_organogram_id'] = $employee_office['office_unit_organogram_id'];
                        $notesTableData['employee_id'] = $employee_office['officer_id'];
                        $notesTableData['employee_designation'] = $employee_office['designation_label'];

                        $notesTableData['note_no'] = (empty($lastNote) ? 0 : ($lastNote['note_no'] + 1));
                        $notesTableData['note_description'] = ($body);
                        $notesTableData['description_bk'] = ($this->request->data['body']);
                        $notesTableData['subject'] = $subject;
                        $notesTableData['created_by'] = 1;
                        $notesTableData['modified_by'] = 1;
                        $notesTableData['created'] = date("Y-m-d H:i:s");

                        $notesTableEntity = $notesTable->newEntity();
                        $notesTableEntity = $notesTable->patchEntity($notesTableEntity, $notesTableData);
                        if ($notesTable->save($notesTableEntity)) {
                            if ($notedecision[0] > 0) {
                                $potroAttachmentTable->updateAll(['potrojari' => $notedecision[1], 'potrojari_status' => ''], ['id' => $notedecision[0]]);
                            }
                            $nothiMasterCurrentUsersTable->updateAll(['is_new' => 0], ['nothi_part_no' => $nothipartid, 'office_unit_organogram_id' => $employee_office['office_unit_organogram_id']]);
                            if (!empty($attachments)) {
                                foreach ($attachments as $key => $file) {
                                    if (!empty($file)) {
                                        $attachment_data = array();
                                        $attachment_data['nothi_master_id'] = $nothimasterid;
                                        $attachment_data['nothi_notesheet_id'] = $notesheetid;
                                        $attachment_data['nothi_part_no'] = $nothipartid;
                                        $attachment_data['note_no'] = $notesTableEntity->id;
                                        $attachment_data['attachment_type'] = $this->getMimeType($file);

                                        $attachment_data['file_dir'] = $this->request->webroot . 'content/';
                                        $filepath = explode('content/', $file);

                                        $attachment_data['file_name'] = $filepath[1];
                                        $explode = explode('?token=', $attachment_data['file_name']);
                                        $attachment_data['file_name'] = $explode[0];
                                        $attachment_data['user_file_name'] = !empty($attachmentnames[$key]) ? htmlspecialchars($attachmentnames[$key]) : '';
                                        $attachment_data['created_by'] = $this->Auth->user('id');
                                        $attachment_data['modified_by'] = $this->Auth->user('id');
                                        $attachment_data['created'] = date("Y-m-d H:i:s");

                                        $note_attachment = $noteAttachmentsTable->newEntity();
                                        $note_attachment = $noteAttachmentsTable->patchEntity($note_attachment, $attachment_data);
                                        $noteAttachmentsTable->save($note_attachment);
                                    }
                                }
                            }

                            echo $notesTableEntity->id;
                        } else
                            echo 0;
                    }
                    else {

                        $notes = $notesTable->get($id);
                        $notes->note_description = ($body);
                        $notes->description_bk = $this->request->data['body'];
                        $notes->subject = $subject;

                        if ($notesTable->save($notes)) {
                            if ($notedecision[0] > 0) {
                                $potroAttachmentTable->updateAll(['potrojari' => $notedecision[1]], ['id' => $notedecision[0]]);
                            }
                            $nothiMasterCurrentUsersTable->updateAll(['is_new' => 0], ['nothi_part_no' => $nothipartid, 'office_unit_organogram_id' => $employee_office['office_unit_organogram_id']]);
                            if (!empty($attachments)) {
                                foreach ($attachments as $key => $file) {
                                    if (!empty($file)) {
                                        $attachment_data = array();
                                        $attachment_data['nothi_master_id'] = $notes->nothi_master_id;
                                        $attachment_data['nothi_part_no'] = $notes->nothi_part_no;
                                        $attachment_data['note_no'] = $id;
                                        $attachment_data['nothi_notesheet_id'] = $notes->nothi_notesheet_id;
                                        $attachment_data['attachment_type'] = $this->getMimeType($file);

                                        $attachment_data['file_dir'] = $this->request->webroot . 'content/';
                                        $filepath = explode('content/', $file);
                                        $attachment_data['file_name'] = $filepath[1];
                                        $explode = explode('?token=', $attachment_data['file_name']);
                                        $attachment_data['file_name'] = $explode[0];
                                        $attachment_data['user_file_name'] = !empty($attachmentnames[$key]) ? htmlspecialchars($attachmentnames[$key]) : '';
                                        $attachment_data['created_by'] = $this->Auth->user('id');
                                        $attachment_data['modified_by'] = $this->Auth->user('id');
                                        $attachment_data['created'] = date("Y-m-d H:i:s");

                                        $note_attachment = $noteAttachmentsTable->newEntity();
                                        $note_attachment = $noteAttachmentsTable->patchEntity($note_attachment, $attachment_data);
                                        $noteAttachmentsTable->save($note_attachment);
                                    }
                                }
                            }
                            echo $id;
                        } else
                            echo 0;
                    }
                } else {
                    echo 0;
                }
            }catch (\Exception $ex){
                $this->Flash->error($ex->getMessage());
                echo 0;
            }
        }
        die;
    }

    public function deleteNote()
    {
        $this->layout = null;
        if ($this->request->is('ajax')) {
            $response = array("status" => 'error', 'msg' => "দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না");;

            $employee_office = $this->getCurrentDakSection();
            $nothimasterid = $this->request->data['nothimasterid'];
            $noteid = $this->request->data['noteid'];

            $nothi_office = intval($this->request->data['nothi_office']);
            $otherNothi = false;
            if ($nothi_office != 0 && $nothi_office != $employee_office['office_id']) {
                $otherNothi = true;

                $this->switchOffice($nothi_office, 'MainNothiOffice');
            } else {
                $nothi_office = $employee_office['office_id'];
            }

            TableRegistry::remove("nothiMasterMovements");
            TableRegistry::remove("NothiParts");
            TableRegistry::remove("NothiNoteSheets");
            TableRegistry::remove("NothiPotroAttachments");
            TableRegistry::remove("NothiDecisions");
            TableRegistry::remove("NothiNoteAttachments");
            TableRegistry::remove("NothiNotes");
            TableRegistry::remove("Potrojari");

            $nothiNoteSheetTable = TableRegistry::get('NothiNoteSheets');
            $nothiNoteTable = TableRegistry::get('NothiNotes');
            TableRegistry::remove('NothiMasterCurrentUsers');
            $nothiMasterCurrentUserTable = TableRegistry::get('NothiMasterCurrentUsers');


            $noteDetail = $nothiNoteTable->getNotesInfo($nothimasterid, $noteid);

            if (!empty($noteDetail['id'])) {
                // if potrojari Exist , we can not delete note
                $potrojariExist = TableRegistry::get('Potrojari')->countPotrojariByNotesId($noteDetail['id'], $nothimasterid);
                if (!empty($potrojariExist) && $potrojariExist > 0) {
                    $response['msg'] = ' পত্রজারি খসড়া তৈরি  করা হয়েছে এই অনুচ্ছেদ থেকে । এই অনুচ্ছেদ মুছে ফেলা সম্ভব নয়।';
                    goto rtn;
                }
            }

            if (!empty($noteDetail) && $noteDetail['nothi_part_no'] == $nothimasterid) {
                $isCurrentUser = $nothiMasterCurrentUserTable->getCurrentUserbyDesignation($nothimasterid, $employee_office['office_unit_organogram_id'], $employee_office['office_unit_id'], $nothi_office);

                if (!empty($isCurrentUser)) {
                    $checknotesheet = $nothiNoteTable->find()->where(['nothi_notesheet_id' => $noteDetail['nothi_notesheet_id']])->count();
                    $notesheetid = $noteDetail['nothi_notesheet_id'];
                    if ($nothiNoteTable->delete($noteDetail)) {

                        if ($checknotesheet === 1) {
                            $nothiNoteSheetTable->deleteAll(['sheet_no' => $notesheetid, 'nothi_part_no' => $nothimasterid]);
                        }
                        $checkNotes = $nothiNoteTable->find()->where(['nothi_part_no' => $nothimasterid])->count();

                        if ($checkNotes == 0) {
                            $nothiMasterCurrentUserTable->updateAll(['is_new' => 1], ['nothi_part_no' => $nothimasterid, 'office_unit_organogram_id' => $employee_office['office_unit_organogram_id']]);
                        } else {
							$nothi_notes_drafts = $nothiNoteTable->find()->where(['nothi_part_no' => $nothimasterid, 'note_status' => 'DRAFT'])->order(['note_no' => 'desc'])->toArray();
							$new_note_no = $nothiNoteTable->find()->where(['nothi_part_no' => $nothimasterid, 'note_no !=' => '-1'])->count();
							foreach ($nothi_notes_drafts as $nothi_notes_draft) {
								$nothiNoteTable->updateAll(['note_no' => --$new_note_no], ['id' => $nothi_notes_draft->id]);
							}
						}
                        $noteAttachmentsTable = TableRegistry::get('NothiNoteAttachments');
                        $attachmentData = $noteAttachmentsTable->find()->where(['nothi_part_no' => $nothimasterid, 'note_no' => $noteid]);
                        if (!empty($attachmentData)) {
                            foreach ($attachmentData as $key => $value) {

                                if ($noteAttachmentsTable->delete($value)) {
                                }
                            }
                        }
                        $response = array("status" => 'success', 'msg' => "অনুচ্ছেদটি মুছে ফেলা হয়েছে");
                        goto rtn;
                    }
                }
            }
            rtn:
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;

        }
    }

    public function addNoteSheet()
    {

        $this->layout = null;
        if ($this->request->is('ajax')) {

            $employee_office = $this->getCurrentDakSection();
            $nothi_office = intval($this->request->data['nothi_office']);
            $otherNothi = false;
            if ($nothi_office != 0 && $nothi_office != $employee_office['office_id']) {
                $otherNothi = true;

                $this->switchOffice($nothi_office, 'MainNothiOffice');
            } else {
                $nothi_office = $employee_office['office_id'];
            }
            TableRegistry::remove("nothiMasterMovements");
            TableRegistry::remove("NothiParts");
            TableRegistry::remove("NothiNoteSheets");
            TableRegistry::remove("NothiPotroAttachments");
            TableRegistry::remove("NothiDecisions");
            TableRegistry::remove("NothiNoteAttachments");
            TableRegistry::remove("NothiNotes");
            $noteSheetNo = 1;
            $noteSheetTable = TableRegistry::get('NothiNoteSheets');
            $notesTable = TableRegistry::get('NothiNotes');

            $nothiPartsTable = TableRegistry::get('NothiParts');

            $nothiPartsInformation = $nothiPartsTable->get(intval($this->request->data['nothimasterid']));

            $getExistingNothiInfo = array();

            $getExistingNothiInfo = $noteSheetTable->find()->select(['sheet_no'])->where(['nothi_part_no' => intval($this->request->data['nothimasterid'])])->order(['id DESC'])->first();
            $totalNotes = 1;

            if (!empty($getExistingNothiInfo)) {
                $totalNotes = $notesTable->getNotesbyNotesheet($getExistingNothiInfo['id']);

                if ($totalNotes > 0) {
                    $noteSheetNo = $getExistingNothiInfo['sheet_no'] + 1;
                }
            }

            if ($totalNotes > 0) {
                $exitsing = $noteSheetTable->find()->where(['sheet_no' => $noteSheetNo, 'nothi_part_no' => intval($this->request->data['nothimasterid'])])->first();
                if (count($exitsing) == 0) {
                    $noteSheetTableEntity = $noteSheetTable->newEntity();
                    $noteSheetData = array();
                    $noteSheetData['nothi_master_id'] = $nothiPartsInformation['nothi_masters_id'];
                    $noteSheetData['nothi_part_no'] = intval($this->request->data['nothimasterid']);
                    $noteSheetData['sheet_no'] = $noteSheetNo;
                    $noteSheetData['sheet_no_bn'] = Number::format($noteSheetNo);

                    $noteSheetData['created_by'] = $this->Auth->user('id');
                    $noteSheetData['created'] = date("Y-m-d H:i:s");
                    try {
                        $noteSheetTableEntity = $noteSheetTable->patchEntity($noteSheetTableEntity, $noteSheetData);

                        $noteSheetTable->save($noteSheetTableEntity);
                        echo $noteSheetNo;
                    } catch (\Exception $e) {
                        echo 0;
                    }
                } else {
                    echo $noteSheetNo;
                }
            } else {
                echo $noteSheetNo;
            }
        }
        die;
    }

    public function saveNoteFlag()
    {

        $this->layout = null;
        if ($this->request->is('ajax')) {
            $session = $this->request->session();
            $employee = $session->read('logged_employee_record');
            $employee_office = $this->getCurrentDakSection();

            $nothi_office = intval($this->request->data['nothi_office']);
            $otherNothi = false;
            if ($nothi_office != 0 && $nothi_office != $employee_office['office_id']) {
                $otherNothi = true;

                $this->switchOffice($nothi_office, 'MainNothiOffice');
            } else {
                $nothi_office = $employee_office['office_id'];
            }
            TableRegistry::remove("NoteFlags");

            $bookmarkedData = array();
            $bookmarkedData['nothi_master_id'] = intval($this->request->data['masternoteno']);
            $bookmarkedData['notesheet_id'] = intval($this->request->data['notesheetid']);
            $bookmarkedData['color'] = ($this->request->data['color']);
            $bookmarkedData['title'] = $this->request->data['title'];
            $bookmarkedData['page_url'] = $this->request->data['page_url'];
            $bookmarkedData['created'] = date("Y-m-d H:i:s");

            $bookmarkedData['office_id'] = $employee_office['office_id'];
            $bookmarkedData['office_unit_id'] = $employee_office['office_unit_id'];
            $bookmarkedData['office_organogram_id'] = $employee_office['office_unit_organogram_id'];
            $bookmarkedData['employee_id'] = $employee['personal_info']['id'];

            $noteFlagTable = TableRegistry::get('NoteFlags');

            $noteFlagRecord = $noteFlagTable->find()
                ->where([
                    'nothi_master_id' => $bookmarkedData['nothi_master_id'],
                    'employee_id' => $bookmarkedData['employee_id'],
                    'notesheet_id' => $bookmarkedData['notesheet_id']
                ])
                ->first();

            if (empty($noteFlagRecord)) {
                $noteFlagTableEntity = $noteFlagTable->newEntity();
                $noteFlagTableEntity = $noteFlagTable->patchEntity($noteFlagTableEntity, $bookmarkedData);
                $noteFlagTable->save($noteFlagTableEntity);
                echo 1;
            } else {
                $noteFlagTable->delete($noteFlagRecord);
                echo 0;
            }
        }
        die;
    }

    public function showNoteList($nothimasterid = 0, $nothi_office = 0, $employee_office = [])
    {

        if ($nothi_office != 0 && $nothi_office != $employee_office['office_id']) {

            $this->switchOffice($nothi_office, 'MainNothiOffice');
        } else {
            $nothi_office = $employee_office['office_id'];
        }
        TableRegistry::remove("NothiNotes");
        $noteTable = TableRegistry::get('NothiNotes');

        TableRegistry::remove('NothiMasterPermissions');
        $nothiMastersPermissionTable = TableRegistry::get('NothiMasterPermissions');
        $permitted_nothi_list = $nothiMastersPermissionTable->allNothiPermissionList($nothi_office, $employee_office['office_id'], $employee_office['office_unit_id'], $employee_office['office_unit_organogram_id']);


        $noteTablequery = $noteTable->find()
            ->select(['NothiNotes.id', 'NothiNotes.nothi_master_id', 'NothiNotes.nothi_part_no', 'nothi_part_no_en' => 'NothiParts.nothi_part_no', 'nothi_part_no_bn' => 'NothiParts.nothi_part_no_bn', 'NothiNotes.nothi_notesheet_id', 'NothiNotes.note_no'])
            ->where(['NothiNotes.nothi_master_id' => $nothimasterid, 'note_no >= ' => 0])
            ->join([
                'NothiParts' => [
                    'table' => 'nothi_parts',
                    "conditions" => "NothiParts.id= NothiNotes.nothi_part_no",
                    "type" => "INNER"
                ]
            ])
            ->where(['NothiNotes.nothi_part_no IN' => $permitted_nothi_list, '(NothiNotes.office_organogram_id = ' . $employee_office['office_unit_organogram_id'] . ' OR NothiNotes.note_status <> "DRAFT")'])
            ->order(['nothi_part_no_en asc,cast(NothiNotes.note_no as signed) asc'])->toArray();


        $returnData = array();


        if (!empty($noteTablequery)) {
            foreach ($noteTablequery as $key => $row) {
                $returnData[] = array(
                    'nothi_master_id' => $row['nothi_master_id'],
                    'nothi_part_no' => $row['nothi_part_no'],
                    'nothi_part_no_bn' => ($row['nothi_part_no_bn']),
                    'nothi_part_no_en' => ($row['nothi_part_no_en']),
                    'nothi_notesheet_id' => $row['nothi_notesheet_id'],
                    'note_no' => Number::format($row['note_no']),
                    'note_no_en' => $row['note_no'],
                    'id' => $row['id'],
                );
            }
        }

        return $returnData;
    }

    private function getSignatures($masterId, $nothiNoteId = 0)
    {

        TableRegistry::remove('NothiNoteSignatures');
        $noteSignatureTable = TableRegistry::get('NothiNoteSignatures');
        $employeeRecordsTable = TableRegistry::get('EmployeeRecords');
        $employeeOfficesTable = TableRegistry::get('EmployeeOffices');
        $userTable = TableRegistry::get('Users');

        $allSignatures = $noteSignatureTable->find()->where(['nothi_part_no' => $masterId]);

        if (!empty($nothiNoteId)) {
            $allSignatures = $allSignatures->where(['nothi_note_id' => $nothiNoteId]);
        }

        $allSignatures = $allSignatures->toArray();

        $returnData = array();
        if (!empty($allSignatures)) {
            foreach ($allSignatures as $key => $value) {
                $employeeRecords = $employeeRecordsTable->get($value['employee_id']);
//                $employeeinfo = $employeeOfficesTable->getOfficeDesignation($value['employee_id'], $value['office_organogram_id']);
                $userInfo = $userTable->find()->where(['employee_record_id' => $value['employee_id']])->first();
                $returnData[$value['nothi_note_id']][] = array(
                    'office_id' => $value['office_id'],
                    'office_unit_id' => $value['office_unit_id'],
                    'office_organogram_id' => $value['office_organogram_id'],
                    'employee_id' => $value['employee_id'],
//                    'employee_designation' => $employeeinfo['designation'] . (!empty($employeeinfo['incharge_label'])?(" (" . $employeeinfo['incharge_label'] . ')'):''),
                    'employee_designation' => $value['employee_designation'],
                    'note_decision' => $value['note_decision'],
                    'cross_signature' => $value['cross_signature'],
                    'signature_date' => $value['signature_date'],
                    'is_signature' => $value['is_signature'],
                    'cross_signature' => $value['cross_signature'],
                    'digital_sign' => $value['digital_sign'],
                    'signature' => $employeeRecords['d_sign'],
                    'name' => $employeeRecords['name_bng'],
                    'userInfo' => $userInfo['username'],
                );
            }
        }

        return $returnData;
    }

    private function getSignaturesAll($masterId, $nothiNoteId = 0)
    {

        TableRegistry::remove('NothiNoteSignatures');
        $noteSignatureTable = TableRegistry::get('NothiNoteSignatures');
        $employeeRecordsTable = TableRegistry::get('EmployeeRecords');
        $employeeOfficesTable = TableRegistry::get('EmployeeOffices');
        $userTable = TableRegistry::get('Users');

        $allSignatures = $noteSignatureTable->find()->where(['nothi_part_no IN' => $masterId]);

        if (!empty($nothiNoteId)) {
            $allSignatures = $allSignatures->where(['nothi_note_id' => $nothiNoteId]);
        }

        $allSignatures = $allSignatures->toArray();

        $returnData = array();
        if (!empty($allSignatures)) {
            foreach ($allSignatures as $key => $value) {
                $userInfo = $userTable->find()->where(['employee_record_id' => $value['employee_id']])->first();
//                $employeeinfo = $employeeOfficesTable->getOfficeDesignation($value['employee_id'], $value['office_organogram_id']);
                $employeeRecords = $employeeRecordsTable->get($value['employee_id']);
                $returnData[$value['nothi_part_no']][$value['nothi_note_id']][] = array(
                    'office_id' => $value['office_id'],
                    'office_unit_id' => $value['office_unit_id'],
                    'office_organogram_id' => $value['office_organogram_id'],
                    'employee_id' => $value['employee_id'],
                    'employee_designation' => $value['employee_designation'],
                    'note_decision' => $value['note_decision'],
                    'cross_signature' => $value['cross_signature'],
                    'signature_date' => $value['signature_date'],
                    'is_signature' => $value['is_signature'],
                    'digital_sign' => $value['digital_sign'],
                    'cross_signature' => $value['cross_signature'],
                    'signature' => $employeeRecords['d_sign'],
                    'name' => $employeeRecords['name_bng'],
                    'userInfo' => $userInfo['username'],
                );
            }
        }

        return $returnData;
    }

    public function savePotroFlag()
    {
        $this->layout = null;
        if ($this->request->is('ajax')) {

            $employee_office = $this->getCurrentDakSection();

            $nothi_office = intval($this->request->data['nothi_office']);
            $otherNothi = false;
            if ($nothi_office != 0 && $nothi_office != $employee_office['office_id']) {
                $otherNothi = true;

                $this->switchOffice($nothi_office, 'MainNothiOffice');
            } else {
                $nothi_office = $employee_office['office_id'];
            }

            TableRegistry::remove("NothiParts");
            TableRegistry::remove("PotroFlags");
            $nothiPartsTable = TableRegistry::get('NothiParts');
            $potroFlagTable = TableRegistry::get('PotroFlags');

            $nothiPartsInformation = $nothiPartsTable->get(intval($this->request->data['masternoteno']));

            $potroFlagRecord = $potroFlagTable->find()
                ->where([
                    'nothi_part_no' => $nothiPartsInformation['id'],
                    'potro_attachment_id <>' => intval($this->request->data['potro_no']),
                    'title' => $this->request->data['title']
                ])
                ->first();

            if (!empty($potroFlagRecord)) {
                echo 2;
                die;
            }

            $bookmarkedData = array();

            $bookmarkedData['nothi_master_id'] = $nothiPartsInformation['nothi_masters_id'];
            $bookmarkedData['nothi_part_no'] = $nothiPartsInformation['id'];
            $bookmarkedData['color'] = ($this->request->data['color']);
            $bookmarkedData['title'] = $this->request->data['title'];
            $bookmarkedData['potro_attachment_id'] = intval($this->request->data['potro_no']);
            $bookmarkedData['page_no'] = $this->BngToEng($this->request->data['page']);
            $bookmarkedData['created'] = date("Y-m-d H:i:s");
            $bookmarkedData['office_id'] = $employee_office['office_id'];
            $bookmarkedData['office_unit_id'] = $employee_office['office_unit_id'];
            $bookmarkedData['office_organogram_id'] = $employee_office['office_unit_organogram_id'];

            $potroFlagRecord = $potroFlagTable->find()
                ->where([
                    'nothi_master_id' => $bookmarkedData['nothi_master_id'],
                    'office_organogram_id' => $bookmarkedData['office_organogram_id'],
                    'potro_attachment_id' => $bookmarkedData['potro_attachment_id'],
                    'page_no' => $bookmarkedData['page_no']
                ])
                ->first();

            if (empty($potroFlagRecord)) {
                $potroFlagTableEntity = $potroFlagTable->newEntity();
                $potroFlagTableEntity = $potroFlagTable->patchEntity($potroFlagTableEntity, $bookmarkedData);
                $potroFlagTable->save($potroFlagTableEntity);
                echo 1;
            } else {
                $potroFlagTable->delete($potroFlagRecord);

                $potroFlagTableEntity = $potroFlagTable->newEntity();
                $potroFlagTableEntity = $potroFlagTable->patchEntity($potroFlagTableEntity, $bookmarkedData);
                $potroFlagTable->save($potroFlagTableEntity);
                echo 0;
            }
        }
        die;
    }

    public function deleteAttachment()
    {
        if ($this->request->is('ajax')) {

            $id = intval($this->request->data['id']);
            $nothimasterid = intval($this->request->data['nothimasterid']);

            $employee_office = $this->getCurrentDakSection();
            $nothi_office = intval($this->request->data['nothi_office']);
            $otherNothi = false;
            if ($nothi_office != 0 && $nothi_office != $employee_office['office_id']) {
                $otherNothi = true;

                $this->switchOffice($nothi_office, 'MainNothiOffice');
            } else {
                $nothi_office = $employee_office['office_id'];
            }

            TableRegistry::remove("NothiNoteAttachments");
            $noteAttachmentsTable = TableRegistry::get('NothiNoteAttachments');
            $attachmentData = $noteAttachmentsTable->get($id);

            if (!empty($attachmentData)) {
                TableRegistry::remove('NothiMasterCurrentUsers');
                $nothiMasterCurrentUserTable = TableRegistry::get('NothiMasterCurrentUsers');

                $isCurrentUser = $nothiMasterCurrentUserTable->getCurrentUserbyDesignation($nothimasterid, $employee_office['office_unit_organogram_id'], $employee_office['office_unit_id'], $nothi_office);

                if (!empty($isCurrentUser)) {

                    if ($noteAttachmentsTable->delete($attachmentData)) {
                        //unlink(FILE_FOLDER_DIR . urldecode($fileName));
                        echo json_encode(array("status" => 'success', 'msg' => "সংযুক্তি  মুছে ফেলা হয়েছে"));
                        die;
                    }
                }
            }
            echo json_encode(array("status" => 'error', 'msg' => "দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না"));
            die;
        }
    }

    public function downloadNoteAttachment($user_designation = 0)
    {
        $this->layout = null;

        $id = intval($this->request->query['id']);
        $nothipartid = intval($this->request->query['nothimaster']);

        if (empty($user_designation)) {
            $employee_office = $this->getCurrentDakSection();
        } else {
            $EmployeeOfficesTable = TableRegistry::get('EmployeeOffices');
            $employee_office = $EmployeeOfficesTable->find()->where(['office_unit_organogram_id' => $user_designation, 'status' => 1])->first();

        }

        $nothi_office = intval($this->request->query['nothi_office']);

        $otherNothi = false;
        if ($nothi_office != 0) {
            $otherNothi = true;

            $this->switchOffice($nothi_office, 'MainNothiOffice');
        } else {
            $nothi_office = $employee_office['office_id'];
        }
        TableRegistry::remove("NothiNoteAttachments");
        TableRegistry::remove("NothiParts");

        $noteAttachmentsTable = TableRegistry::get('NothiNoteAttachments');
        $notePartsTable = TableRegistry::get('NothiParts');

        $attachmentData = $noteAttachmentsTable->get($id);

        $nothi_parts_information = $notePartsTable->get($nothipartid);

        if (!empty($attachmentData)) {
            TableRegistry::remove('NothiMasterPermissions');
            $nothiMasterPermissionTable = TableRegistry::get('NothiMasterPermissions');

            $isAllowed = $nothiMasterPermissionTable->hasAccess($nothi_office, $employee_office['office_id'], $employee_office['office_unit_id'], $employee_office['office_unit_organogram_id'], $nothi_parts_information['nothi_masters_id'], $nothipartid);

            if (!empty($isAllowed) && !empty($attachmentData['file_name'])) {

                $filename = explode("/", $attachmentData['file_name']);


                $this->response->file(
                    (FILE_FOLDER_DIR . urldecode($attachmentData['file_name'])), array('download' => true, 'name' => urldecode($filename[count($filename) - 1]))
                );
                return $this->response;
            }
        }
        if (empty($user_designation)) {
            return $this->redirect(['action' => 'dashboard', 'controller' => 'dashboard']);
        }

    }

    public function apiOnucched($part_id, $nothi_office)
    {
        if (!empty($part_id) && !empty($nothi_office) && !empty($this->request->data['nothimasterid']) && !empty($this->request->data['notesheetid'])) {

            $this->layout = 'online_dak';
            $this->switchOffice($nothi_office, 'MainNothiOffice');
            if (!empty($this->request->data['note-id'])) {
                $id = intval($this->request->data['note-id']);
                $notesTable = TableRegistry::get('NothiNotes');
                $notes = $notesTable->get($id);
                $this->set('notes', $notes);
                $this->set('id', $id);
            }

            $user_designation = !empty($this->request->query['user_designation']) ? $this->request->query['user_designation'] : '';
            if (empty($user_designation)) {
                $user_designation = !empty($this->request->data['user_designation']) ? $this->request->data['user_designation'] : '';
            }

            $apikey = !empty($this->request->query['api_key']) ? $this->request->query['api_key'] : '';
            if (empty($apikey)) {
                $apikey = !empty($this->request->data['api_key']) ? $this->request->data['api_key'] : '';
            }

            if (empty($user_designation)) {
                die("Invalid Request");
                die;
            }

            if (empty($apikey) || $this->checkToken($apikey) == FALSE) {
                die("Invalid Request");
                die;
            }
            if (!defined("PROJAPOTI_API") || PROJAPOTI_API == false) {
                die("Invalid Request");
                die;
            }

            if ($apikey != API_KEY) {
                //verify api token
                $this->loadComponent('ApiRelated');
                $getApiTokenData = $this->ApiRelated->getMobileApiTokenData($apikey);
                if (!empty($getApiTokenData)) {
                    if (!in_array($user_designation, $getApiTokenData['designations'])) {
                        die("Unauthorized request");
                    }
                    if(!empty($getApiTokenData['device_type'])){
                        $this->set('device_type',$getApiTokenData['device_type']);
                    }else{
                        $this->set('device_type','android');
                    }
                }
                //verify api token
            }
            $this->set('apikey', $apikey);
            $this->set('user_designation', $user_designation);


            $nothiPartsTable = TableRegistry::get('NothiParts');
            $nothiInformation = $nothiPartsTable->get($part_id);

            $EmployeeOfficesTable = TableRegistry::get('EmployeeOffices');
            $employee_office = $EmployeeOfficesTable->find()->where(['office_unit_organogram_id' =>
                $user_designation, 'status' => 1])->first();

            $potroAttachmentTable = TableRegistry::get('NothiPotroAttachments');
            TableRegistry::remove('NothiMasterPermissions');
            $nothiMastersPermissionTable = TableRegistry::get('NothiMasterPermissions');
            $permitted_nothi_part_list = $nothiMastersPermissionTable->NothiPermissionListForSingleNothi($nothi_office, $employee_office['office_id'], $employee_office['office_unit_id'], $employee_office['office_unit_organogram_id'], $nothiInformation['nothi_masters_id']);

            $potroAttachmentRecord = $potroAttachmentTable->getBibecchoPotroWithSubject($permitted_nothi_part_list);

            $this->set(compact('potroAttachmentRecord'));

            $noteDecisionTable = TableRegistry::get('NothiDecisionEmployees');
            $noteDecision = $noteDecisionTable->GetNothiDecisionsByEmployee($employee_office['employee_record_id']);
            if (empty($noteDecision)) {
                $noteDecision = $noteDecisionTable->GetNothiDecisionsByEmployee(0);
            }

            $this->set(compact('noteDecision'));

            $this->set('part_id', $part_id);
            $this->set('nothi_office', $nothi_office);
            $this->set('nothimasterid', $this->request->data['nothimasterid']);
            $this->set('notesubject', $this->request->data['notesubject']);
            $this->set('notesheetid', $this->request->data['notesheetid']);
            $this->set('user_designation', $this->request->data['user_designation']);
            $this->set('height', $this->request->data['height']);
            $this->set('width', $this->request->data['width']);
            $this->set('api_key', $this->request->data['api_key']);

            $onucched_count = TableRegistry::get('NothiNotes')->checkEmptyNote($nothiInformation['nothi_master_id'],$part_id);
            $this->set(compact('onucched_count'));

        } else {
            die("Invalid Request");
        }
    }

    public function apiNoteOnuccedAdd($part_id, $nothi_office)
    {

        $user_designation = !empty($this->request->query['user_designation']) ? $this->request->query['user_designation'] : '';
        if (empty($user_designation)) {
            $user_designation = !empty($this->request->data['user_designation']) ? $this->request->data['user_designation'] : '';
        }

        $jsonArray = array(
            "status" => "error",
            "error_code" => 404,
            "message" => "Invalid Request"
        );

        if (empty($user_designation)) {
            echo json_encode($jsonArray);
            die;
        }
        if (empty($part_id)) {
            echo json_encode($jsonArray);
            die;
        }

        $result = array();

        try {

            $EmployeeOfficesTable = TableRegistry::get('EmployeeOffices');
            $employee_office = $EmployeeOfficesTable->find()->where(['office_unit_organogram_id' => $user_designation, 'status' => 1])->first();

            $this->switchOffice($nothi_office, 'MainNothiOffice');

            $nothiPartsTable = TableRegistry::get('NothiParts');

            $nothiPartsInformation = $nothiPartsTable->get($part_id);

            $nothimasterid = $nothiPartsInformation['nothi_masters_id'];
            $nothipartid = $part_id;
            $id = intval($this->request->data['id']);
            $notesheetid = intval($this->request->data['notesheetid']);
            $subject = !empty($this->request->data['notesubject']) ? $this->request->data['notesubject'] : '';
            $notedecision = '';
            $body = base64_encode($this->request->data['body']);
            $attachments = isset($this->request->data['attachments']) ? $this->request->data['attachments'] : array();
            $attachmentnames = isset($this->request->data['user_file_name']) ? $this->request->data['user_file_name'] : array();


            $notesTable = TableRegistry::get('NothiNotes');
            $noteAttachmentsTable = TableRegistry::get('NothiNoteAttachments');
            TableRegistry::remove('NothiMasterCurrentUsers');
            $nothiMasterCurrentUsersTable = TableRegistry::get('NothiMasterCurrentUsers');

            $hasAccess = $nothiMasterCurrentUsersTable->getCurrentUserbyDesignation($part_id, $employee_office['office_unit_organogram_id'], $employee_office['office_unit_id'], $nothi_office);

            if (empty($hasAccess)) {
                $jsonArray = array(
                    "status" => "error",
                    "error_code" => 404,
                    "message" => "দুঃখিত!অনুচ্ছেদ দেয়া সম্ভব হচ্ছে না"
                );
                $this->response->body(json_encode($jsonArray));
                $this->response->type('application/json');
                return $this->response;
            }
            $lastNote = $notesTable->find()->select(["note_no" => 'cast(note_no as signed)'])->where(['nothi_part_no' => $nothipartid])->order(['cast(note_no as signed) DESC'])->first();

            if (empty($id) || !isset($id)) {
                $notesTableData = array();
                $notesTableData['nothi_master_id'] = $nothimasterid;
                $notesTableData['nothi_part_no'] = $nothipartid;
                $notesTableData['nothi_notesheet_id'] = $notesheetid;

                $notesTableData['office_id'] = $employee_office['office_id'];
                $notesTableData['office_unit_id'] = $employee_office['office_unit_id'];
                $notesTableData['office_organogram_id'] = $employee_office['office_unit_organogram_id'];
                $notesTableData['employee_id'] = $employee_office['employee_record_id'];
                $notesTableData['employee_designation'] = $employee_office['designation_label'];

                $notesTableData['note_no'] = (empty($lastNote) ? 0 : ($lastNote['note_no'] + 1));
                $notesTableData['note_description'] = ($body);
                $notesTableData['description_bk'] = $this->request->data['body'];
                $notesTableData['subject'] = $subject;
                $notesTableData['created_by'] = $employee_office['employee_record_id'];
                $notesTableData['modified_by'] = $employee_office['employee_record_id'];
                $notesTableData['created'] = date("Y-m-d H:i:s");

                $notesTableEntity = $notesTable->newEntity();
                $notesTableEntity = $notesTable->patchEntity($notesTableEntity, $notesTableData);
                if ($notesTable->save($notesTableEntity)) {

                    $nothiMasterCurrentUsersTable->updateAll(['is_new' => 0], ['nothi_part_no' => $nothipartid, 'office_unit_organogram_id' => $employee_office['office_unit_organogram_id']]);
                    if (!empty($attachments)) {
                        foreach ($attachments as $key => $file) {
                            if (!empty($file)) {
                                $attachment_data = array();
                                $attachment_data['nothi_master_id'] = $nothimasterid;
                                $attachment_data['nothi_notesheet_id'] = $notesheetid;
                                $attachment_data['nothi_part_no'] = $nothipartid;
                                $attachment_data['note_no'] = $notesTableEntity->id;
                                $attachment_data['attachment_type'] = $this->getMimeType($file);

                                $attachment_data['file_dir'] = $this->request->webroot . 'content/';
                                $filepath = explode('content/', $file);
                                $attachment_data['file_name'] = $filepath[1];
                                $attachment_data['user_file_name'] = !empty($attachmentnames[$key]) ? htmlspecialchars($attachmentnames[$key]) : '';
                                $attachment_data['created_by'] = $employee_office['employee_record_id'];
                                $attachment_data['modified_by'] = $employee_office['employee_record_id'];
                                $attachment_data['created'] = date("Y-m-d H:i:s");

                                $note_attachment = $noteAttachmentsTable->newEntity();
                                $note_attachment = $noteAttachmentsTable->patchEntity($note_attachment, $attachment_data);
                                $noteAttachmentsTable->save($note_attachment);
                            }
                        }
                    }

                    $jsonArray = array(
                        "status" => "success"
                    );
                } else {
                    $jsonArray = array(
                        "status" => "error",
                        "msg" => 'দুঃখিত! অনুচ্ছেদ দেয়া সম্ভব হচ্ছে না'
                    );
                }
            } else {

                $notes = $notesTable->get($id);
                $notes->note_description = ($body);
                $notes['description_bk'] = $this->request->data['body'];
                $notes->subject = $subject;

                if ($notesTable->save($notes)) {
                    $nothiMasterCurrentUsersTable->updateAll(['is_new' => 0], ['nothi_part_no' => $id, 'office_unit_organogram_id' => $employee_office['office_unit_organogram_id']]);
                    if (!empty($attachments)) {
                        foreach ($attachments as $key => $file) {
                            if (!empty($file)) {
                                $attachment_data = array();
                                $attachment_data['nothi_master_id'] = $notes->nothi_master_id;
                                $attachment_data['nothi_part_no'] = $notes->nothi_part_no;
                                $attachment_data['note_no'] = $id;
                                $attachment_data['nothi_notesheet_id'] = $notes->nothi_notesheet_id;
                                $attachment_data['attachment_type'] = $this->getMimeType($file);

                                $attachment_data['file_dir'] = $this->request->webroot . 'content/';
                                $filepath = explode('content/', $file);
                                $attachment_data['file_name'] = $filepath[1];
                                $attachment_data['user_file_name'] = !empty($attachmentnames[$key]) ? htmlspecialchars($attachmentnames[$key]) : '';
                                $attachment_data['created_by'] = $this->Auth->user('id');
                                $attachment_data['modified_by'] = $this->Auth->user('id');
                                $attachment_data['created'] = date("Y-m-d H:i:s");

                                $note_attachment = $noteAttachmentsTable->newEntity();
                                $note_attachment = $noteAttachmentsTable->patchEntity($note_attachment, $attachment_data);
                                $noteAttachmentsTable->save($note_attachment);
                            }
                        }
                    }
                    $jsonArray = array(
                        "status" => "success"
                    );
                } else {
                    $jsonArray = array(
                        "status" => "error",
                        "msg" => 'দুঃখিত! অনুচ্ছেদ দেয়া সম্ভব হচ্ছে না'
                    );
                }
            }
        } catch (\Exception $ex) {
            $jsonArray = array(
                "status" => "error",
                "error_code" => 404,
                'error' => $ex->getMessage(),
                "message" => "দুঃখিত!অনুচ্ছেদ দেয়া সম্ভব হচ্ছে না",
                "msg" => "দুঃখিত!অনুচ্ছেদ দেয়া সম্ভব হচ্ছে না"
            );
        }

        $this->response->body(json_encode($jsonArray));
        $this->response->type('application/json');
        return $this->response;
    }

    public function downloadNoteAttachments($nothi_office = 0, $id = 0)
    {
        $employee_office = $this->getCurrentDakSection();
        if ($nothi_office != 0) {
            $this->switchOffice($nothi_office, 'MainNothiOffice');
        } else {
            $nothi_office = $employee_office['office_id'];
        }
        TableRegistry::remove('NothiNoteAttachments');
        $potroAttachmentTable = TableRegistry::get('NothiNoteAttachments');

        $potroAttachmentRecord = $potroAttachmentTable->find()->where(['id' => $id])->first();

        if (!empty($potroAttachmentRecord)) {
            $file = new \Cake\Filesystem\File(FILE_FOLDER_DIR . $potroAttachmentRecord['file_name']);
            if ($file->exists()) {
                $file_name = explode("/", $potroAttachmentRecord['file_name']);

                $this->response->file(FILE_FOLDER_DIR . $potroAttachmentRecord['file_name'], ['name' => (end($file_name)), 'download' => true]);
            }else{
                $this->Flash->error("দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না");
                return $this->redirect($this->request->referer());
            }
        }

        return $this->response;
    }

    public function downloadPotro($nothi_office = 0, $id = 0)
    {
        $employee_office = $this->getCurrentDakSection();
        if ($nothi_office != 0) {
            $this->switchOffice($nothi_office, 'MainNothiOffice');
        } else {
            $nothi_office = $employee_office['office_id'];
        }
        TableRegistry::remove('NothiPotroAttachments');
        $potroAttachmentTable = TableRegistry::get('NothiPotroAttachments');

        $potroAttachmentRecord = $potroAttachmentTable->find()->where(['id' => $id])->first();

        if (!empty($potroAttachmentRecord)) {
            $file = new \Cake\Filesystem\File(FILE_FOLDER_DIR . $potroAttachmentRecord['file_name']);
            if ($file->exists()) {
                $file_name = str_replace(" ", "_", $potroAttachmentRecord['sarok_no']);
                $name = explode(".", $potroAttachmentRecord['file_name']);

                $this->response->file(FILE_FOLDER_DIR . $potroAttachmentRecord['file_name'], ['name' => ($file_name . '.' . end($name)), 'download' => true]);
            }else{
                $this->Flash->error("দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না");
                return $this->redirect($this->request->referer());
            }
        }

        return $this->response;
    }

    public function downloadNoteAttachmentrefs($nothi_office = 0, $id = 0)
    {
        $employee_office = $this->getCurrentDakSection();
        if ($nothi_office != 0) {
            $this->switchOffice($nothi_office, 'MainNothiOffice');
        } else {
            $nothi_office = $employee_office['office_id'];
        }
        TableRegistry::remove('NothiNoteAttachmentRefs');
        $potroAttachmentTable = TableRegistry::get('NothiNoteAttachmentRefs');

        $potroAttachmentRecord = $potroAttachmentTable->find()->where(['id' => $id])->first();

        if (!empty($potroAttachmentRecord)) {
            $file = new \Cake\Filesystem\File(FILE_FOLDER_DIR . $potroAttachmentRecord['file_name']);
            if ($file->exists()) {
                $file_name = explode("/", $potroAttachmentRecord['file_name']);

                $this->response->file(FILE_FOLDER_DIR . $potroAttachmentRecord['file_name'], ['name' => (end($file_name)), 'download' => true]);
            }else{
                $this->Flash->error("দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না");
                return $this->redirect($this->request->referer());
            }
        }

        return $this->response;
    }

    public function downloadPotrojariFile($nothi_office = 0, $id = 0)
    {
        $employee_office = $this->getCurrentDakSection();
        if ($nothi_office != 0) {
            $this->switchOffice($nothi_office, 'MainNothiOffice');
        } else {
            $nothi_office = $employee_office['office_id'];
        }
        TableRegistry::remove('PotrojariAttachments');
        $potroAttachmentTable = TableRegistry::get('PotrojariAttachments');

        $potroAttachmentRecord = $potroAttachmentTable->find()->where(['id' => $id])->first();

        if (!empty($potroAttachmentRecord)) {
            $file = new \Cake\Filesystem\File(FILE_FOLDER_DIR . $potroAttachmentRecord['file_name']);
            if ($file->exists()) {
                $file_name = explode("/", $potroAttachmentRecord['file_name']);

                $this->response->file(FILE_FOLDER_DIR . $potroAttachmentRecord['file_name'], ['name' => (end($file_name)), 'download' => true]);
            }else{
                $this->Flash->error("দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না");
                return $this->redirect($this->request->referer());
            }
        }

        return $this->response;
    }

    public function downloadGuardFile($nothi_office = 0, $id = 0)
    {
        TableRegistry::remove('GuardFileAttachments');
        $potroAttachmentTable = TableRegistry::get('GuardFileAttachments');

        $potroAttachmentRecord = $potroAttachmentTable->find()->where(['id' => $id])->first();

        if (!empty($potroAttachmentRecord)) {
            $file = new \Cake\Filesystem\File(FILE_FOLDER_DIR . $potroAttachmentRecord['file_name']);
            if ($file->exists()) {
                $file_name = explode("/", $potroAttachmentRecord['file_name']);

                $this->response->file(FILE_FOLDER_DIR . $potroAttachmentRecord['file_name'], ['name' => (end($file_name)), 'download' => true]);
            }else{
                $this->Flash->error("দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না");
                return $this->redirect($this->request->referer());
            }
        }

        return $this->response;
    }

    public function downloadDak($nothi_office = 0, $id = 0)
    {
        $employee_office = $this->getCurrentDakSection();
        if ($nothi_office != 0) {
            $this->switchOffice($nothi_office, 'MainNothiOffice');
        } else {
            $nothi_office = $employee_office['office_id'];
        }
        TableRegistry::remove('DakAttachments');
        $potroAttachmentTable = TableRegistry::get('DakAttachments');

        $potroAttachmentRecord = $potroAttachmentTable->find()->where(['id' => $id])->first();

        if (!empty($potroAttachmentRecord)) {
            $file = new \Cake\Filesystem\File(FILE_FOLDER_DIR . $potroAttachmentRecord['file_name']);
            if ($file->exists()) {
                $file_name = explode("/", $potroAttachmentRecord['file_name']);

                $this->response->file(FILE_FOLDER_DIR . $potroAttachmentRecord['file_name'], ['name' => (end($file_name)), 'download' => true]);
            }else{
                $this->Flash->error("দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না");
                return $this->redirect($this->request->referer());
            }
        }

        return $this->response;
    }

    public function verifySoftTokenForOnucched()
    {
        $response = array("status" => 'error', 'msg' => "দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না");;
        $token = !empty($this->request->data['token']) ? $this->request->data['token'] : '';
        $part_no = !empty($this->request->data['part_no']) ? $this->request->data['part_no'] : 0;
        if (empty($part_no) || empty($token)) {
            goto rtn;
        }
        $employee_office = $this->getCurrentDakSection();
        $allNotes = TableRegistry::get('NothiNotes')->getNotes($part_no, $employee_office['officer_id'])->where(['digital_sign']);
        rtn:
        $this->response->type('application/json');
        $this->response->body(json_encode($response));
        return $this->response;
    }

    public function apiDeleteNode()
    {
        $this->layout = null;
        $user_designation = isset($this->request->data['user_designation']) ? $this->request->data['user_designation'] : 0;

        $apikey = isset($this->request->data['api_key']) ? $this->request->data['api_key'] : '';

        if (empty($apikey) || $this->checkToken($apikey) == FALSE) {
            die("Invalid Request");
        }

        if (!defined("PROJAPOTI_API") || PROJAPOTI_API == false) {
            die("Invalid Request");
        }
        if (empty($user_designation)) {
            die("Invalid Request");
        }

        if ($apikey != API_KEY) {
            //verify api token
            $this->loadComponent('ApiRelated');
            $getApiTokenData = $this->ApiRelated->getMobileApiTokenData($apikey);
            if (!empty($getApiTokenData)) {
                if (!in_array($user_designation, $getApiTokenData['designations'])) {
                    die("Unauthorized request");
                }
            }
            //verify api token
        }
        $this->set('apikey', $apikey);
        $this->set('user_designation', $user_designation);

        $response = array("status" => 'error', 'msg' => "দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না");;

        $employee_office = $this->setCurrentDakSection($user_designation);
        $nothimasterid = intval($this->request->data['nothimasterid']);
        $noteid = intval($this->request->data['noteid']);

        $nothi_office = intval($this->request->data['nothi_office']);
        $otherNothi = false;
        if ($nothi_office != 0 && $nothi_office != $employee_office['office_id']) {
            $otherNothi = true;
        } else {
            $nothi_office = $employee_office['office_id'];
        }

        $this->switchOffice($nothi_office, 'MainNothiOffice');

        TableRegistry::remove("nothiMasterMovements");
        TableRegistry::remove("NothiParts");
        TableRegistry::remove("NothiNoteSheets");
        TableRegistry::remove("NothiPotroAttachments");
        TableRegistry::remove("NothiDecisions");
        TableRegistry::remove("NothiNoteAttachments");
        TableRegistry::remove("NothiNotes");
        TableRegistry::remove("Potrojari");

        $nothiNoteSheetTable = TableRegistry::get('NothiNoteSheets');
        $nothiNoteTable = TableRegistry::get('NothiNotes');
        TableRegistry::remove('NothiMasterCurrentUsers');
        $nothiMasterCurrentUserTable = TableRegistry::get('NothiMasterCurrentUsers');


        $noteDetail = $nothiNoteTable->getNotesInfo($nothimasterid, $noteid);

        if (!empty($noteDetail['id'])) {
            // if potrojari Exist , we can not delete note
            $potrojariExist = TableRegistry::get('Potrojari')->countPotrojariByNotesId($noteDetail['id'], $nothimasterid);
            if (!empty($potrojariExist) && $potrojariExist > 0) {
                $response['msg'] = ' পত্রজারি খসড়া তৈরি  করা হয়েছে এই অনুচ্ছেদ থেকে । এই অনুচ্ছেদ মুছে ফেলা সম্ভব নয়।';
                goto rtn;
            }
        }

        if (!empty($noteDetail) && $noteDetail['nothi_part_no'] == $nothimasterid) {
            $isCurrentUser = $nothiMasterCurrentUserTable->getCurrentUserbyDesignation($nothimasterid, $employee_office['office_unit_organogram_id'], $employee_office['office_unit_id'], $nothi_office);

            if (!empty($isCurrentUser)) {
                $checknotesheet = $nothiNoteTable->find()->where(['nothi_notesheet_id' => $noteDetail['nothi_notesheet_id']])->count();
                $notesheetid = $noteDetail['nothi_notesheet_id'];
                if ($nothiNoteTable->delete($noteDetail)) {

                    if ($checknotesheet === 1) {
                        $nothiNoteSheetTable->deleteAll(['sheet_no' => $notesheetid, 'nothi_part_no' => $nothimasterid]);
                    }
                    $checkNotes = $nothiNoteTable->find()->where(['nothi_part_no' => $nothimasterid])->count();

                    if ($checkNotes == 0) {
                        $nothiMasterCurrentUserTable->updateAll(['is_new' => 1], ['nothi_part_no' => $nothimasterid, 'office_unit_organogram_id' => $employee_office['office_unit_organogram_id']]);
                    }
                    $noteAttachmentsTable = TableRegistry::get('NothiNoteAttachments');
                    $attachmentData = $noteAttachmentsTable->find()->where(['nothi_part_no' => $nothimasterid, 'note_no' => $noteid]);
                    if (!empty($attachmentData)) {
                        foreach ($attachmentData as $key => $value) {

                            if ($noteAttachmentsTable->delete($value)) {
                            }
                        }
                    }
                    $response = array("status" => 'success', 'msg' => "অনুচ্ছেদটি মুছে ফেলা হয়েছে");
                    goto rtn;
                }
            }
        }
        rtn:
        $this->response->type('application/json');
        $this->response->body(json_encode($response));
        return $this->response;

    }
}
