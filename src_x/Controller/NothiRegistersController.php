<?php

namespace App\Controller;

use App\Controller\ProjapotiController;
use App\Model\Table;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Psr\Log\InvalidArgumentException;
use Cake\I18n\I18n;
use Cake\I18n\Time;
use Cake\I18n\Number;

class NothiRegistersController extends ProjapotiController {

    /**
     * Nothi Grohon register
     */
    public function potoroJariRegister() {

        $session = $this->request->session();
        $employee = $session->read('logged_employee_record');
        $this->set("office", $employee);
    }

    public function potoroJariRegisterContent($startDate = "", $endDate = "") {
        $this->layout = null;
        $condition = '1 ';

        $employee_office = $this->getCurrentDakSection();
        $officeUnitTable = TableRegistry::get("OfficeUnits");
        $officeTable = TableRegistry::get("Offices");

        $units = $officeUnitTable->getOfficeChildInfo($employee_office['office_unit_id']);
        $unitsOwn = $officeUnitTable->getOwnChildInfo($employee_office['office_unit_id']);

        $units = !empty($units[0]) ? $units[0] : '';
        $unitsOwn = !empty($unitsOwn[0]) ? ($employee_office['office_unit_id'] . ',' . $unitsOwn[0]) : $employee_office['office_unit_id'];

        $units = $units . (!empty($unitsOwn) ? ((!empty($units) ? ',' : '') . $unitsOwn) : '');


        $condition .= " AND Potrojari.office_unit_id IN ({$units})";

        $startDate = $startDate;

        $endDate = $endDate;

        if (!empty($startDate) And ! empty($endDate)) {
            if (!empty($condition))
                $condition .= ' AND ';
            $condition .= " Potrojari.modified  BETWEEN  '{$startDate} 00:00:00' AND '{$endDate}
            23:59:59' ";
        } else {
            $startDate = date('Y-m-d');
            $endDate = date('Y-m-d');
            if (!empty($condition))
                $condition .= ' AND ';
            $condition .= " Potrojari.modified  BETWEEN  '{$startDate} 00:00:00' AND '{$endDate}
            23:59:59' ";
        }

        $report_table = TableRegistry::get('Reports');

        $reports = $report_table->getPotrojariRegister($condition);

        $html = "";
        $count = 0;

        if (!empty($reports)) {

            foreach ($reports as $potrojari) {
                $office_information = $officeTable->get($potrojari['PotrojariReceiver']['receiving_office_id']);

                $count++;
                $html .= '<tr>';
                $html .= '<td class="text-center">' . Number::format($count) . '</td>';
                $html .= '<td class="text-center">' . h($potrojari['NothiParts']['nothi_no']) . '</td>';
                $html .= '<td class="text-center">' . $potrojari['sarok_no'] . "<br/>" . Time::parse($potrojari['PotrojariReceiver']['modified']) . '</td>';
                $html .= '<td class="text-center">' . (empty($potrojari['PotrojariReceiver']['receiving_office_id']) ? ($potrojari['PotrojariReceiver']['receiving_office_name']) : ($office_information['office_name_bng'] . ', ' . h($office_information['office_address']))) . '</td>';
                $html .= '<td class="text-center">' . '' . ' </td>';
                $html .= '<td class="text-center">' . '' . '</td>';
                $html .= '<td class="text-center">' . (empty($potrojari['PotrojariReceiver']['receiving_office_id']) ? (!empty($potrojari['PotrojariReceiver']['receiving_officer_email']) ? 'ইমেইল' : 'অন্যান্য') : 'নথি ব্যবস্থাপনার মাধ্যমে') . '</td>';
                $html .= '<td class="text-center">' . '' . '</td>';
                $html .= '</tr>';
            }
        }
        echo $html;
        die();
    }

    /**
     * Nothi Grohon register
     */
    public function nothiMovementRegister() {

        $session = $this->request->session();
        $employee = $session->read('logged_employee_record');
        $this->set("office", $employee);
    }

    public function nothiMovementRegisterContent($startDate = "", $endDate = "") {
        $this->layout = null;

        die;
    }

    /**
     * Nothi Grohon register
     */
    public function nothiDiaryRegisterExcel($type='pdf',$startDate = "", $endDate = "",$dateRangeBn='',$unit_id=0) {
        $result = $this->nothiDiaryRegisterContent($startDate, $endDate, $type, 20, $unit_id);
        $unitInformation='';
        $DakList = Array();
        $j=0;

        if (!empty($result['data'])) {

            foreach ($result['data'] as $dak) {
                if( $unitInformation ==  $dak['office_units_name']){
                    $DakList[$j]['office_units_name'] ='';
                }
                else {
                    $DakList[$j]['office_units_name'] = h($dak['office_units_name']);
                    $unitInformation = $dak['office_units_name'];
                }
                $created = new Time($dak['nothi_created_date']);
                $dak['nothi_created_date'] = $created->i18nFormat(null, null, 'bn-BD');

                $DakList[$j]['nothi_no'] = h($dak['nothi_no']);
                $DakList[$j]['changes_history'] = h($dak['changes_history']);
                $DakList[$j]['subject'] = $dak['NothiTypes']['type_name'] . ' - ' . $dak['subject'];
                $DakList[$j]['nothi_created_date'] = $result['nothiClass'][$dak['nothi_class']] . ', ' . $dak['nothi_created_date'];
                $j++;

            }
        }
        if(empty($DakList)){
            $DakList= Array();
        }
        if($type == 'excel'){
            $this->printExcel($DakList, [
                ['key' => 'office_units_name', 'title' => 'শাখা'],
                ['key' => 'nothi_no', 'title' => 'নথির নম্বর'],
                ['key' => 'changes_history', 'title' => 'পূর্ববর্তী নথির নম্বরসমূহ'],
                ['key' => 'subject', 'title' => 'শিরোনাম/বিষয়'],
                ['key' => 'nothi_created_date', 'title' => 'শ্রেণিবিন্যাসসহ নথিভুক্ত করিবার তারিখ'],
            ],[
                'name'=>((isset($result['office_name']))?$result['office_name'].' অফিসের ':'') .'নথি নিবন্ধন বহি' ,
                'title' => [
                    ((isset($result['office_name']))?$result['office_name'].' অফিসের ':'') .'নথি নিবন্ধন বহি' ,
                    $dateRangeBn,
                ]
            ]);
        } else {
            $this->printPdf($DakList, [
                ['key' => 'office_units_name', 'title' => 'শাখা'],
                ['key' => 'nothi_no', 'title' => 'নথির নম্বর'],
                ['key' => 'changes_history', 'title' => 'পূর্ববর্তী নথির নম্বরসমূহ'],
                ['key' => 'subject', 'title' => 'শিরোনাম/বিষয়'],
                ['key' => 'nothi_created_date', 'title' => 'শ্রেণিবিন্যাসসহ নথিভুক্ত করিবার তারিখ'],
            ],[
                'name'=>((isset($result['office_name']))?$result['office_name'].' অফিসের ':'') .'নথি নিবন্ধন বহি' ,
                'title' => [
                    ((isset($result['office_name']))?$result['office_name'].' অফিসের ':'') .'নথি নিবন্ধন বহি' ,
                    $dateRangeBn,
                ]
            ]);
        }

        exit;
    }
    
    public function nothiDiaryRegister() {

        $session = $this->request->session();
        $employee = $session->read('logged_employee_record');
        $this->set("office", $employee);

        $units_list = $this->getOwnUnitList();
        $this->set("units_list",$units_list);
    }

    public function nothiDiaryRegisterContent($startDate = "", $endDate = "",$isExport="",$per_page = 20 ,$unit_id = "") {
        $this->layout = null;
        $condition = [];

        $nothiClass = json_decode(NOTHI_CLASS, true);

        $employee_office = $this->getCurrentDakSection();
        $officeUnitTable = TableRegistry::get("OfficeUnits");

        $units = $officeUnitTable->getOfficeChildInfo($employee_office['office_unit_id']);
        $unitsOwn = $officeUnitTable->getOwnChildInfo($employee_office['office_unit_id']);

        $units = !empty($units[0]) ? $units[0] : '';
        $unitsOwn = !empty($unitsOwn[0]) ? ($employee_office['office_unit_id'] . ',' . $unitsOwn[0]) : $employee_office['office_unit_id'];

        $units = $units . (!empty($unitsOwn) ? ((!empty($units) ? ',' : '') . $unitsOwn) : '');
        $unit_id_array = explode(",",$units);
        if(!empty($unit_id) && in_array((int)$unit_id,$unit_id_array)){
            $unit_id_array = (int)$unit_id;
        }
        array_push($condition,["NothiMasters.office_units_id IN" => $unit_id_array]);

        $startDate = !empty($startDate)?h($startDate):date('Y-m-d');
        $endDate = !empty($endDate)?h($endDate):date('Y-m-d');

        $report_table = TableRegistry::get('Reports');
        if($isExport=='excel' || $isExport=='pdf'){
            $reports =$report_table->getNothiDiary($startDate, $endDate,$condition);
        }
        else {
            $reports = $this->Paginator->paginate($report_table->getNothiDiary($startDate, $endDate,$condition), ['limit' => $per_page]);
        }

        $office = TableRegistry::get('Offices');
        $office_name = $office->find()->select(['office_name_bng', 'office_address'])->where(['id' => $employee_office['office_id']])->first();

        $preorder = '';
        $data = [];
        $i = 0;
        if (!empty($reports)) {
            foreach ($reports as $dak) {
                if (empty($preorder)) {
                    $preorder = $dak['office_units_id'];
                }
                $preorder = $preorder . ',' . $dak['office_units_id'];
            }
            if (!empty($preorder)) {
                $order_unit = $officeUnitTable->getOrderwiseUnits($preorder);
            }
            if (!empty($order_unit)) {
                foreach ($order_unit as $key => $val) {
                    foreach ($reports as $dak) {
                        if ($dak['office_units_id'] == $key) {
                            $nothi_changes_history = $report_table->getNothiPreviousChanges($dak['id']);

                            $data[$i]['id'] = $dak['id'];
                            $data[$i]['changes_history'] = $nothi_changes_history;
                            $data[$i]['office_units_id'] = $dak['office_units_id'];

                            $unit_name = $officeUnitTable->get($dak['office_units_id']);

                            $data[$i]['office_units_name'] = $unit_name['unit_name_bng'];
                            $data[$i]['nothi_no'] = $dak['nothi_no'];
                            $data[$i]['subject'] = $dak['subject'];
                            $data[$i]['nothi_types_id'] = $dak['nothi_types_id'];
                            $data[$i]['nothi_created_date'] = $dak['nothi_created_date'];
                            $data[$i]['nothi_class'] = $dak['nothi_class'];
                            $data[$i]['NothiTypes'] = $dak['NothiTypes'];

                            $i++;
                        }
                    }
                }
            }
        }
        if($isExport=='excel' || $isExport=='pdf') {
            return $result = Array('data' => $data, 'office_name' => $office_name['office_name_bng'],'nothiClass'=> $nothiClass);
        }

        $this->set('data', $data);
        $this->set('office_name', $office_name);
        $this->set('nothiClass', $nothiClass);
    }
//    public function nothiDiaryRegisterContent($startDate = "", $endDate = "",$isExport='', $unit_id=0) {
//        $this->layout = null;
//        $condition = '1 ';
//
//        $nothiClass = json_decode(NOTHI_CLASS, true);
//
//        $employee_office = $this->getCurrentDakSection();
//        $officeUnitTable = TableRegistry::get("OfficeUnits");
//
//        $units = $officeUnitTable->getOfficeChildInfo($employee_office['office_unit_id']);
//        $unitsOwn = $officeUnitTable->getOwnChildInfo($employee_office['office_unit_id']);
//
//        $units = !empty($units[0]) ? $units[0] : '';
//        $unitsOwn = !empty($unitsOwn[0]) ? ($employee_office['office_unit_id'] . ',' . $unitsOwn[0]) : $employee_office['office_unit_id'];
//
//        $units = $units . (!empty($unitsOwn) ? ((!empty($units) ? ',' : '') . $unitsOwn) : '');
//
//        $condition .= " AND NothiMasters.office_units_id IN ({$units}) ";
//
//        $startDate = !empty($startDate)?h($startDate):'';
//        $endDate = !empty($endDate)?h($endDate):'';
//
//        if (!empty($startDate) And ! empty($endDate)) {
//            if (!empty($condition))
//                $condition .= ' AND ';
//            $condition .= " NothiMasters.nothi_created_date  BETWEEN  '{$startDate} 00:00:00' AND '{$endDate}
//            23:59:59' ";
//        } else {
//            $startDate = date('Y-m-d');
//            $endDate = date('Y-m-d');
//            if (!empty($condition))
//                $condition .= ' AND ';
//            $condition .= " NothiMasters.nothi_created_date  BETWEEN  '{$startDate} 00:00:00' AND '{$endDate}
//            23:59:59' ";
//        }
//        if ($unit_id != 0) {
//            $condition .= " AND NothiMasters.office_units_id = {$unit_id}";
//        }
//
//        $report_table = TableRegistry::get('Reports');
//        if($isExport=='excel' || $isExport=='pdf'){
//            $reports =$report_table->getNothiDiary($condition);
//        }
//        else {
//            $reports = $this->Paginator->paginate($report_table->getNothiDiary($condition), ['limit' => 20]);
//        }
//
//        $office = TableRegistry::get('Offices');
//        $office_name = $office->find()->select(['office_name_bng', 'office_address'])->where(['id' => $employee_office['office_id']])->first();
//
//        $preorder = '';
//        $data = [];
//        $i = 0;
//        if (!empty($reports)) {
//            foreach ($reports as $dak) {
//                if (empty($preorder)) {
//                    $preorder = $dak['office_units_id'];
//                }
//                $preorder = $preorder . ',' . $dak['office_units_id'];
//            }
//            if (!empty($preorder)) {
//                $order_unit = $officeUnitTable->getOrderwiseUnits($preorder);
//            }
//            if (!empty($order_unit)) {
//                foreach ($order_unit as $key => $val) {
//                    foreach ($reports as $dak) {
//                        if ($dak['office_units_id'] == $key) {
//                            $nothi_changes_history = $report_table->getNothiPreviousChanges($dak['id']);
//
//                            $data[$i]['id'] = $dak['id'];
//                            $data[$i]['changes_history'] = $nothi_changes_history;
//                            $data[$i]['office_units_id'] = $dak['office_units_id'];
//
//                            $unit_name = $officeUnitTable->get($dak['office_units_id']);
//
//                            $data[$i]['office_units_name'] = $unit_name['unit_name_bng'];
//                            $data[$i]['nothi_no'] = $dak['nothi_no'];
//                            $data[$i]['subject'] = $dak['subject'];
//                            $data[$i]['nothi_types_id'] = $dak['nothi_types_id'];
//                            $data[$i]['nothi_created_date'] = $dak['nothi_created_date'];
//                            $data[$i]['nothi_class'] = $dak['nothi_class'];
//                            $data[$i]['NothiTypes'] = $dak['NothiTypes'];
//
//                            $i++;
//                        }
//                    }
//                }
//            }
//        }
//        if($isExport=='excel' || $isExport=='pdf') {
//            return $result = Array('data' => $data, 'office_name' => $office_name['office_name_bng'],'nothiClass'=> $nothiClass);
//        }
//
//        $this->set('data', $data);
//        $this->set('office_name', $office_name);
//        $this->set('nothiClass', $nothiClass);
//    }

    /**
     * Nothi Grohon register
     */
    public function nothiGrohonRegisterExcel($type='pdf',$startDate = "", $endDate = "",$dateRangeBn='',$unit_id=0) {
        $result = $this->nothiGrohonRegisterContent($startDate, $endDate, $type, 20, $unit_id);
        $unitInformation='';
        $DakList = Array();
        $j=0;

        if (!empty($result['data'])) {

            foreach ($result['data'] as $dak) {
                if( $unitInformation ==  $dak['to_office_unit_name']){
                    $DakList[$j]['from_office_unit_name_bng'] ='';
                }
                else {
                    $DakList[$j]['from_office_unit_name_bng'] = h($dak['to_office_unit_name']);
                    $unitInformation = $dak['to_office_unit_name'];
                }
                $created = new Time($dak['created']);
                $dak['created'] = $created->i18nFormat(null, null, 'bn-BD');

                $DakList[$j]['nothi_no'] = h($dak['NothiParts']['nothi_no']);
                $DakList[$j]['created'] = h($dak['created']);
                $DakList[$j]['from_office_unit_name'] = h($dak['from_office_unit_name']) . ', ' . h($dak['from_officer_designation_label']);
                $DakList[$j]['subject'] = h($dak['NothiParts']['subject']);
                $DakList[$j]['to_officer_name'] = h($dak['from_officer_name']) . ', ' . h($dak['from_officer_designation_label']);
                $j++;

            }
        }
        if(empty($DakList)){
            $DakList= Array();
        }
        if($type == 'excel'){
            $this->printExcel($DakList, [
                ['key' => 'from_office_unit_name_bng', 'title' => 'শাখা'],
                ['key' => 'nothi_no', 'title' => 'গৃহীত নথির নম্বর'],
                ['key' => 'created', 'title' => 'গ্রহণের তারিখ'],
                ['key' => 'from_office_unit_name', 'title' => 'অফিস/শাখার নাম'],
                ['key' => 'subject', 'title' => 'নথির বিষয়'],
                ['key' => 'to_officer_name', 'title' => 'পূর্ববর্তী প্রেরক'],
            ],[
                'name'=>((isset($result['office_name']))?$result['office_name'].' অফিসের ':'') .'নথি গ্রহণ নিবন্ধন বহি' ,
                'title' => [
                    ((isset($result['office_name']))?$result['office_name'].' অফিসের ':'') .'নথি গ্রহণ নিবন্ধন বহি' ,
                    $dateRangeBn,
                ]
            ]);
        } else {
            $this->printPdf($DakList, [
                ['key' => 'from_office_unit_name_bng', 'title' => 'শাখা'],
                ['key' => 'nothi_no', 'title' => 'গৃহীত নথির নম্বর'],
                ['key' => 'created', 'title' => 'গ্রহণের তারিখ'],
                ['key' => 'from_office_unit_name', 'title' => 'অফিস/শাখার নাম'],
                ['key' => 'subject', 'title' => 'নথির বিষয়'],
                ['key' => 'to_officer_name', 'title' => 'পূর্ববর্তী প্রেরক'],
            ],[
                'name'=>((isset($result['office_name']))?$result['office_name'].' অফিসের ':'') .'নথি গ্রহণ নিবন্ধন বহি' ,
                'title' => [
                    ((isset($result['office_name']))?$result['office_name'].' অফিসের ':'') .'নথি গ্রহণ নিবন্ধন বহি' ,
                    $dateRangeBn,
                ]
            ]);
        }

        exit;
    }

    public function nothiGrohonRegister() {
        $session = $this->request->session();
        $employee = $session->read('logged_employee_record');
        $this->set("office", $employee);

        $units_list = $this->getOwnUnitList();
        $this->set("units_list",$units_list);
    }

    public function nothiGrohonRegisterContent($startDate = "", $endDate = "",$isExport="",$per_page = 20 ,$unit_id = "") {
        $this->layout = null;
        $condition = [];


        $employee_office = $this->getCurrentDakSection();


        $startDate = !empty($startDate)?h($startDate):date('Y-m-d');
        $endDate = !empty($endDate)?h($endDate):date('Y-m-d');


        $report_table = TableRegistry::get('Reports');

        $officeUnitTable = TableRegistry::get("OfficeUnits");

        $units = $officeUnitTable->getOfficeChildInfo($employee_office['office_unit_id']);
        $unitsOwn = $officeUnitTable->getOwnChildInfo($employee_office['office_unit_id']);

        $units = !empty($units[0]) ? $units[0] : '';
        $unitsOwn = !empty($unitsOwn[0]) ? ($employee_office['office_unit_id'] . ',' . $unitsOwn[0]) : $employee_office['office_unit_id'];

        $units = $units . (!empty($unitsOwn) ? ((!empty($units) ? ',' : '') . $unitsOwn) : '');
        $unit_id_array = explode(",",$units);
        if(!empty($unit_id) && in_array((int)$unit_id,$unit_id_array)){
            $unit_id_array = (int)$unit_id;
        }
        array_push($condition,["NothiMasterMovements.to_office_unit_id IN"=> $unit_id_array]);

        if($isExport=='excel' || $isExport=='pdf'){
            $reports =$report_table->getAllReceivedNothi($startDate,$endDate,$condition);
        }
        else {
            $reports = $this->Paginator->paginate($report_table->getAllReceivedNothi($startDate,$endDate,$condition), ['limit' => $per_page]);
        }

        $office = TableRegistry::get('Offices');
        $office_name = $office->find()->select(['office_name_bng', 'office_address'])->where(['id' => $employee_office['office_id']])->first();

        $preorder = '';
        $data = [];
        $i = 0;
        if (!empty($reports)) {
            foreach ($reports as $dak) {
                if (empty($preorder)) {
                    $preorder = $dak['to_office_unit_id'];
                }
                $preorder = $preorder . ',' . $dak['to_office_unit_id'];
            }
            if (!empty($preorder)) {
                $order_unit = $officeUnitTable->getOrderwiseUnits($preorder);
            }
            if (!empty($order_unit)) {
                foreach ($order_unit as $key => $val) {
                    foreach ($reports as $dak) {
                        if ($dak['to_office_unit_id'] == $key) {

                            $data[$i]['id'] = $dak['id'];
                            $data[$i]['modified'] = $dak['modified'];
                            $data[$i]['from_officer_name'] = $dak['from_officer_name'];
                            $data[$i]['from_office_unit_id'] = $dak['from_office_unit_id'];
                            $data[$i]['from_office_unit_name'] = $dak['from_office_unit_name'];
                            $data[$i]['created'] = $dak['created'];
                            $data[$i]['to_office_unit_id'] = $dak['to_office_unit_id'];
                            $data[$i]['to_office_unit_name'] = $dak['to_office_unit_name'];
                            $data[$i]['to_officer_name'] = $dak['to_officer_name'];
                            $data[$i]['from_officer_designation_label'] = $dak['from_officer_designation_label'];
                            $data[$i]['to_officer_designation_label'] = $dak['to_officer_designation_label'];
                            $data[$i]['NothiParts']['nothi_part_no'] = $dak['NothiParts']['nothi_part_no'];
                            $data[$i]['NothiParts']['id'] = $dak['NothiParts']['id'];
                            $data[$i]['NothiParts']['subject'] = $dak['NothiParts']['subject'];
                            $data[$i]['NothiParts']['nothi_no'] = $dak['NothiParts']['nothi_no'];
                            $data[$i]['NothiParts']['created'] = $dak['NothiParts']['created'];
                            $data[$i]['NothiParts']['modified'] = $dak['NothiParts']['modified'];

                            $i++;
                        }
                    }
                }
            }
        }
        if($isExport=='excel' || $isExport=='pdf') {
            return $result = Array('data' => $data, 'office_name' => $office_name['office_name_bng']);
        }
        $this->set('data', $data);
        $this->set('office_name', $office_name);
    }
//    public function nothiGrohonRegisterContent($startDate = "", $endDate = "",$isExport='',$unit_id=0) {
//        $this->layout = null;
//        $condition = '1 ';
//
//
//        $employee_office = $this->getCurrentDakSection();
//
//
//        $startDate = !empty($startDate)?h($startDate):'';
//        $endDate = !empty($endDate)?h($endDate):'';
//
//        if (!empty($startDate) And ! empty($endDate)) {
//            if (!empty($condition))
//                $condition .= ' AND ';
//            $condition .= " DATE(NothiMasterMovements.created)  BETWEEN  '{$startDate}' AND '{$endDate}' ";
//        } else {
//            $startDate = date('Y-m-d');
//            $endDate = date('Y-m-d');
//            if (!empty($condition))
//                $condition .= ' AND ';
//            $condition .= " DATE(NothiMasterMovements.created)  BETWEEN  '{$startDate}' AND '{$endDate}' ";
//        }
//
//
//        $report_table = TableRegistry::get('Reports');
//
//        $officeUnitTable = TableRegistry::get("OfficeUnits");
//
//        $units = $officeUnitTable->getOfficeChildInfo($employee_office['office_unit_id']);
//        $unitsOwn = $officeUnitTable->getOwnChildInfo($employee_office['office_unit_id']);
//
//        $units = !empty($units[0]) ? $units[0] : '';
//        $unitsOwn = !empty($unitsOwn[0]) ? ($employee_office['office_unit_id'] . ',' . $unitsOwn[0]) : $employee_office['office_unit_id'];
//
//        $units = $units . (!empty($unitsOwn) ? ((!empty($units) ? ',' : '') . $unitsOwn) : '');
//        $condition .= " AND NothiMasterMovements.to_office_unit_id IN ({$units})";
//
//        if ($unit_id != 0) {
//            $condition .= " AND NothiMasterMovements.from_office_unit_id = {$unit_id}";
//        }
//
//        if($isExport=='excel' || $isExport=='pdf'){
//            $reports =$report_table->getAllReceivedNothi($condition);
//        }
//        else {
//            $reports = $this->Paginator->paginate($report_table->getAllReceivedNothi($condition), ['limit' => 20]);
//        }
//
//        $office = TableRegistry::get('Offices');
//        $office_name = $office->find()->select(['office_name_bng', 'office_address'])->where(['id' => $employee_office['office_id']])->first();
//
//        $preorder = '';
//        $data = [];
//        $i = 0;
//        if (!empty($reports)) {
//            foreach ($reports as $dak) {
//                if (empty($preorder)) {
//                    $preorder = $dak['to_office_unit_id'];
//                }
//                $preorder = $preorder . ',' . $dak['to_office_unit_id'];
//            }
//            if (!empty($preorder)) {
//                $order_unit = $officeUnitTable->getOrderwiseUnits($preorder);
//            }
//            if (!empty($order_unit)) {
//                foreach ($order_unit as $key => $val) {
//                    foreach ($reports as $dak) {
//                        if ($dak['to_office_unit_id'] == $key) {
//
//                            $data[$i]['id'] = $dak['id'];
//                            $data[$i]['modified'] = $dak['modified'];
//                            $data[$i]['from_officer_name'] = $dak['from_officer_name'];
//                            $data[$i]['from_office_unit_id'] = $dak['from_office_unit_id'];
//                            $data[$i]['from_office_unit_name'] = $dak['from_office_unit_name'];
//                            $data[$i]['created'] = $dak['created'];
//                            $data[$i]['to_office_unit_id'] = $dak['to_office_unit_id'];
//                            $data[$i]['to_office_unit_name'] = $dak['to_office_unit_name'];
//                            $data[$i]['to_officer_name'] = $dak['to_officer_name'];
//                            $data[$i]['from_officer_designation_label'] = $dak['from_officer_designation_label'];
//                            $data[$i]['to_officer_designation_label'] = $dak['to_officer_designation_label'];
//                            $data[$i]['NothiParts']['nothi_part_no'] = $dak['NothiParts']['nothi_part_no'];
//                            $data[$i]['NothiParts']['id'] = $dak['NothiParts']['id'];
//                            $data[$i]['NothiParts']['subject'] = $dak['NothiParts']['subject'];
//                            $data[$i]['NothiParts']['nothi_no'] = $dak['NothiParts']['nothi_no'];
//                            $data[$i]['NothiParts']['created'] = $dak['NothiParts']['created'];
//                            $data[$i]['NothiParts']['modified'] = $dak['NothiParts']['modified'];
//
//                            $i++;
//                        }
//                    }
//                }
//            }
//        }
//        if($isExport=='excel' || $isExport=='pdf') {
//            return $result = Array('data' => $data, 'office_name' => $office_name['office_name_bng']);
//        }
//        $this->set('data', $data);
//        $this->set('office_name', $office_name);
//    }

    /**
     * Nothi Preron register
     */

    public function nothiPreronRegisterExcel($type='pdf',$startDate = "", $endDate = "",$dateRangeBn='', $unit_id=0) {
        $result = $this->nothiPreronRegisterContent($startDate, $endDate, $type,20, $unit_id);
        $unitInformation='';
        $DakList = Array();
        $j=0;

        if (!empty($result['data'])) {

            foreach ($result['data'] as $dak) {
                if( $unitInformation ==  $dak['from_office_unit_name']){
                    $DakList[$j]['from_office_unit_name_bng'] ='';
                }
                else {
                    $DakList[$j]['from_office_unit_name_bng'] = h($dak['from_office_unit_name']);
                    $unitInformation = $dak['from_office_unit_name'];
                }
                $created = new Time($dak['NothiParts']['created']);
                $dak['created'] = $created->i18nFormat(null, null, 'bn-BD');

                $DakList[$j]['nothi_no'] = h($dak['NothiParts']['nothi_no']);
                $DakList[$j]['from_office_unit_name'] = h($dak['from_office_unit_name']);
                $DakList[$j]['subject'] = h($dak['NothiParts']['subject']);
                $DakList[$j]['from_officer_name'] = h($dak['from_officer_name']);
                $DakList[$j]['created'] = $dak['created'];
                $DakList[$j]['to_officer_name'] = h($dak['to_officer_name']) . ', ' . h($dak['to_officer_designation_label']);
                $j++;

            }
        }
        if(empty($DakList)){
            $DakList= Array();
        }
        if($type == 'excel'){
            $this->printExcel($DakList, [
                ['key' => 'from_office_unit_name_bng', 'title' => 'শাখা'],
                ['key' => 'nothi_no', 'title' => 'প্রেরিত নথির নং'],
                ['key' => 'from_office_unit_name', 'title' => 'অফিস/শাখার নাম'],
                ['key' => 'subject', 'title' => 'নথির বিষয়'],
                ['key' => 'from_officer_name', 'title' => 'পূর্ববর্তী প্রেরক '],
                ['key' => 'created', 'title' => 'প্রেরণের তারিখ'],
                ['key' => 'to_officer_name', 'title' => 'পরবর্তী প্রাপক'],
            ],[
                'name'=>((isset($result['office_name']))?$result['office_name'].' অফিসের ':'') .'নথি প্রেরণ নিবন্ধন বহি' ,
                'title' => [
                    ((isset($result['office_name']))?$result['office_name'].' অফিসের ':'') .'নথি প্রেরণ নিবন্ধন বহি' ,
                    $dateRangeBn,
                ]
            ]);
        } else {
            $this->printPdf($DakList, [
                ['key' => 'from_office_unit_name_bng', 'title' => 'শাখা'],
                ['key' => 'nothi_no', 'title' => 'প্রেরিত নথির নং'],
                ['key' => 'from_office_unit_name', 'title' => 'অফিস/শাখার নাম'],
                ['key' => 'subject', 'title' => 'নথির বিষয়'],
                ['key' => 'from_officer_name', 'title' => 'পূর্ববর্তী প্রেরক '],
                ['key' => 'created', 'title' => 'প্রেরণের তারিখ'],
                ['key' => 'to_officer_name', 'title' => 'পরবর্তী প্রাপক'],
            ],[
                'name'=>((isset($result['office_name']))?$result['office_name'].' অফিসের ':'') .'নথি প্রেরণ নিবন্ধন বহি' ,
                'title' => [
                    ((isset($result['office_name']))?$result['office_name'].' অফিসের ':'') .'নথি প্রেরণ নিবন্ধন বহি' ,
                    $dateRangeBn,
                ]
            ]);
        }

        exit;
    }

    public function nothiPreronRegister() {
        $session = $this->request->session();
        $employee = $session->read('logged_employee_record');
        $this->set("office", $employee);

        $units_list = $this->getOwnUnitList();
        $this->set("units_list",$units_list);
    }

    protected function getOwnUnitList(){
        $employee_office = $this->getCurrentDakSection();

        $officeUnitTable = TableRegistry::get("OfficeUnits");

        $units = $officeUnitTable->getOfficeChildInfo($employee_office['office_unit_id']);
        $unitsOwn = $officeUnitTable->getOwnChildInfo($employee_office['office_unit_id']);

        $units = !empty($units[0]) ? $units[0] : '';
        $unitsOwn = !empty($unitsOwn[0]) ? ($employee_office['office_unit_id'] . ',' . $unitsOwn[0]) : $employee_office['office_unit_id'];

        $units = $units . (!empty($unitsOwn) ? ((!empty($units) ? ',' : '') . $unitsOwn) : '');
        $unit_id_array = explode(",",$units);
        $units_list = $officeUnitTable->find('list',['keyField'=>'id','valueField'=>'unit_name_bng'])->where(['id IN'=>$unit_id_array])->toArray();
        if(count($unit_id_array) > 1){
            $units_list = [''=>'সকল শাখা']+$units_list;
        }

        return $units_list;
    }

    public function nothiPreronRegisterContent($startDate = "", $endDate = "", $isExport="", $per_page = 20 ,$unit_id = "") {
        $this->layout = null;
        $employee_office = $this->getCurrentDakSection();
        $condition=[];

        $startDate = !empty($startDate)?h($startDate):date('Y-m-d');
        $endDate = !empty($endDate)?h($endDate):date('Y-m-d');



        $report_table = TableRegistry::get('Reports');
        $officeUnitTable = TableRegistry::get("OfficeUnits");

        $units = $officeUnitTable->getOfficeChildInfo($employee_office['office_unit_id']);
        $unitsOwn = $officeUnitTable->getOwnChildInfo($employee_office['office_unit_id']);

        $units = !empty($units[0]) ? $units[0] : '';
        $unitsOwn = !empty($unitsOwn[0]) ? ($employee_office['office_unit_id'] . ',' . $unitsOwn[0]) : $employee_office['office_unit_id'];

        $units = $units . (!empty($unitsOwn) ? ((!empty($units) ? ',' : '') . $unitsOwn) : '');

        $unit_id_array = explode(",",$units);
        if(!empty($unit_id) && in_array((int)$unit_id,$unit_id_array)){
            $unit_id_array = (int)$unit_id;
        }
        array_push($condition,["NothiMasterMovements.from_office_unit_id IN" => $unit_id_array]);

        if($isExport=='excel' || $isExport=='pdf'){
            $reports = $report_table->getAllSentNothi($startDate,$endDate,'NothiMasterMovements.modified',$condition);
        } else {
            $reports = $this->Paginator->paginate($report_table->getAllSentNothi($startDate,$endDate,'NothiMasterMovements.modified',$condition), ['limit' => $per_page]);
        }

        $office = TableRegistry::get('Offices');
        $office_name = $office->find()->select(['office_name_bng', 'office_address'])->where(['id' => $employee_office['office_id']])->first();

        $preorder = '';
        $data = [];
        $i = 0;
        if (!empty($reports)) {
            foreach ($reports as $dak) {
                if (empty($preorder)) {
                    $preorder = $dak['from_office_unit_id'];
                }
                $preorder = $preorder . ',' . $dak['from_office_unit_id'];
            }
            if (!empty($preorder)) {
                $order_unit = $officeUnitTable->getOrderwiseUnits($preorder);
            }
            if (!empty($order_unit)) {
                foreach ($order_unit as $key => $val) {
                    foreach ($reports as $dak) {
                        if ($dak['from_office_unit_id'] == $key) {

                            $data[$i]['id'] = $dak['id'];
                            $data[$i]['modified'] = $dak['modified'];
                            $data[$i]['from_office_unit_id'] = $dak['from_office_unit_id'];
                            $data[$i]['from_office_unit_name'] = $dak['from_office_unit_name'];
                            $data[$i]['from_officer_name'] = $dak['from_officer_name'];
                            $data[$i]['created'] = $dak['created'];
                            $data[$i]['to_office_unit_id'] = $dak['to_office_unit_id'];
                            $data[$i]['from_office_name'] = $dak['from_office_name'];
                            $data[$i]['to_office_name'] = $dak['to_office_name'];
                            $data[$i]['to_officer_name'] = $dak['to_officer_name'];
                            $data[$i]['from_officer_designation_label'] = $dak['from_officer_designation_label'];
                            $data[$i]['to_officer_designation_label'] = $dak['to_officer_designation_label'];
                            $data[$i]['NothiParts']['id'] = $dak['NothiParts']['id'];
                            $data[$i]['NothiParts']['nothi_part_no'] = $dak['NothiParts']['nothi_part_no'];
                            $data[$i]['NothiParts']['subject'] = $dak['NothiParts']['subject'];
                            $data[$i]['NothiParts']['nothi_no'] = $dak['NothiParts']['nothi_no'];
                            $data[$i]['NothiParts']['created'] = $dak['NothiParts']['created'];
                            $data[$i]['NothiParts']['modified'] = $dak['NothiParts']['modified'];

                            $i++;
                        }
                    }
                }
            }
        }
        if($isExport=='excel' || $isExport=='pdf'){
            return $result= Array('data'=> $data,'office_name'=> $office_name['office_name_bng']);
        }

        $this->set('data', $data);
        $this->set('office_name', $office_name);
    }
//    public function nothiPreronRegisterContent($startDate = "", $endDate = "",$isExport='', $unit_id=0) {
//        $this->layout = null;
//        $condition = '1 ';
//
//        $employee_office = $this->getCurrentDakSection();
//
//        $startDate = !empty($startDate)?h($startDate):'';
//        $endDate = !empty($endDate)?h($endDate):'';
//
//        if (!empty($startDate) And ! empty($endDate)) {
//            if (!empty($condition))
//                $condition .= ' AND ';
//            $condition .= " DATE(NothiMasterMovements.modified)  BETWEEN  '{$startDate}' AND '{$endDate}' ";
//        } else {
//            $startDate = date('Y-m-d');
//            $endDate = date('Y-m-d');
//            if (!empty($condition))
//                $condition .= ' AND ';
//            $condition .= " DATE(NothiMasterMovements.modified)  BETWEEN  '{$startDate}' AND '{$endDate}' ";
//        }
//
//
//        $report_table = TableRegistry::get('Reports');
//        $officeUnitTable = TableRegistry::get("OfficeUnits");
//
//        $units = $officeUnitTable->getOfficeChildInfo($employee_office['office_unit_id']);
//        $unitsOwn = $officeUnitTable->getOwnChildInfo($employee_office['office_unit_id']);
//
//        $units = !empty($units[0]) ? $units[0] : '';
//        $unitsOwn = !empty($unitsOwn[0]) ? ($employee_office['office_unit_id'] . ',' . $unitsOwn[0]) : $employee_office['office_unit_id'];
//
//        $units = $units . (!empty($unitsOwn) ? ((!empty($units) ? ',' : '') . $unitsOwn) : '');
//        $condition .= " AND NothiMasterMovements.from_office_unit_id IN ({$units})";
//
//        if ($unit_id != 0) {
//            $condition .= " AND NothiMasterMovements.from_office_unit_id = {$unit_id}";
//        }
//
//        if($isExport=='excel' || $isExport=='pdf'){
//            $reports = $report_table->getAllSentNothi($condition);
//        } else {
//            $reports = $this->Paginator->paginate($report_table->getAllSentNothi($condition), ['limit' => 20]);
//        }
//
//        $office = TableRegistry::get('Offices');
//        $office_name = $office->find()->select(['office_name_bng', 'office_address'])->where(['id' => $employee_office['office_id']])->first();
//
//        $preorder = '';
//        $data = [];
//        $i = 0;
//        if (!empty($reports)) {
//            foreach ($reports as $dak) {
//                if (empty($preorder)) {
//                    $preorder = $dak['from_office_unit_id'];
//                }
//                $preorder = $preorder . ',' . $dak['from_office_unit_id'];
//            }
//            if (!empty($preorder)) {
//                $order_unit = $officeUnitTable->getOrderwiseUnits($preorder);
//            }
//            if (!empty($order_unit)) {
//                foreach ($order_unit as $key => $val) {
//                    foreach ($reports as $dak) {
//                        if ($dak['from_office_unit_id'] == $key) {
//
//                            $data[$i]['id'] = $dak['id'];
//                            $data[$i]['modified'] = $dak['modified'];
//                            $data[$i]['from_office_unit_id'] = $dak['from_office_unit_id'];
//                            $data[$i]['from_office_unit_name'] = $dak['from_office_unit_name'];
//                            $data[$i]['from_officer_name'] = $dak['from_officer_name'];
//                            $data[$i]['created'] = $dak['created'];
//                            $data[$i]['to_office_unit_id'] = $dak['to_office_unit_id'];
//                            $data[$i]['from_office_name'] = $dak['from_office_name'];
//                            $data[$i]['to_office_name'] = $dak['to_office_name'];
//                            $data[$i]['to_officer_name'] = $dak['to_officer_name'];
//                            $data[$i]['from_officer_designation_label'] = $dak['from_officer_designation_label'];
//                            $data[$i]['to_officer_designation_label'] = $dak['to_officer_designation_label'];
//                            $data[$i]['NothiParts']['id'] = $dak['NothiParts']['id'];
//                            $data[$i]['NothiParts']['nothi_part_no'] = $dak['NothiParts']['nothi_part_no'];
//                            $data[$i]['NothiParts']['subject'] = $dak['NothiParts']['subject'];
//                            $data[$i]['NothiParts']['nothi_no'] = $dak['NothiParts']['nothi_no'];
//                            $data[$i]['NothiParts']['created'] = $dak['NothiParts']['created'];
//                            $data[$i]['NothiParts']['modified'] = $dak['NothiParts']['modified'];
//
//                            $i++;
//                        }
//                    }
//                }
//            }
//        }
//        if($isExport=='excel' || $isExport=='pdf'){
//            return $result= Array('data'=> $data,'office_name'=> $office_name['office_name_bng']);
//        }
//
//        $this->set('data', $data);
//        $this->set('office_name', $office_name);
//    }

    /**
     * Hard copy register
     */
    public function hardCopyPreronRegister() {
        $session = $this->request->session();
        $employee = $session->read('logged_employee_record');
        $this->set("office", $employee);
    }

    public function hardCopyPreronRegisterContent($startDate = "", $endDate = "") {
        $this->layout = null;
        $condition = '1 ';

        $session = $this->request->session();
        $employee = $session->read('logged_employee_record');
        $employee_section_id = $employee['office_records'][0]->office_unit_id;
        $nothi_master_table = TableRegistry::get("nothi_masters");
        $dak_nothi_table = TableRegistry::get("nothi_potros");
        $dak_daptorik_table = TableRegistry::get("dak_daptoriks");
        $dak_grohon_table = TableRegistry::get("DakGrohonRegisters");

        $startDate = $startDate;

        $endDate = $endDate;

        if (!empty($startDate) And ! empty($endDate)) {
            if (!empty($condition))
                $condition .= ' AND ';
            $condition .= " NothiPotros.issue_date  BETWEEN  '{$startDate} 00:00:00' AND '{$endDate}
            23:59:59' ";
        } else {
            $startDate = date('Y-m-d');
            $endDate = date('Y-m-d');
            if (!empty($condition))
                $condition .= ' AND ';
            $condition .= " NothiPotros.issue_date  BETWEEN  '{$startDate} 00:00:00' AND '{$endDate}
            23:59:59' ";
        }


        $report_table = TableRegistry::get('Reports');

        $reports = $report_table->getAllSentHardcopyReport($condition)->toArray();
        $officeUnitTable = TableRegistry::get('OfficeUnits');

        $html = "";
        $count = 0;
        foreach ($reports as $nothi) {
            $unitInformation = $officeUnitTable->get($nothi['office_units_id']);
            $count++;
            $html .= '<tr>';
            $html .= '<td class="text-center">' . Number::format($count) . '</td>';
            $html .= '<td class="text-center">' . h($nothi['nothi_no']) . '</td>';
            $html .= '<td class="text-center">' . h($nothi['NothiPotros']['issue_date']) . '</td>';
            $html .= '<td class="text-center">' . h($unitInformation['unit_name_bng']) . '</td>';
            $html .= '<td class="text-center">' . h($nothi['subject']) . '</td>';
            $html .= '<td class="text-center">' . h($nothi['NothiFlowInfos']['next_owner']) . ' </td>';
            $html .= '<td class="text-center">' . h($nothi['NothiPotros']['sarok_no']) . ', ' . ($nothi['NothiPotros']['created']) . '</td>';
            $html .= '<td class="text-center">' . h($nothi['NothiPotros']['potro_media']) . '</td>';
            $html .= '</tr>';
        }
        echo $html;
        die();
    }

    public function nothiPotrojariRegister() {
        $session = $this->request->session();
        $employee = $session->read('logged_employee_record');
        $this->set("office", $employee);

        $units_list = $this->getOwnUnitList();
        $this->set("units_list", $units_list);
    }
    public function nothiPotrojariRegisterExcel($type='pdf',$startDate = "", $endDate = "",$dateRangeBn="", $unit_id="") {
        $result = $this->nothiPotrojariRegisterContent($startDate, $endDate, $type, 20, $unit_id);
        $unitInformation='';
        $DakList = Array();
        $j=0;
        if (!empty($result['data'])) {

            foreach ($result['data'] as $value) {
                if( $unitInformation ==  $value['office_unit_name']){
                    $DakList[$j]['office_unit_name'] ='';
                }
                else {
                    $DakList[$j]['office_unit_name'] = h($value['office_unit_name']);
                    $unitInformation = $value['office_unit_name'];
                }

                $created = new Time($value['potrojari_date']);
                $value['potrojari_date'] = $created->i18nFormat(null, null, 'bn-BD');

            $DakList[$j]['potro_subject'] = h($value['potro_subject']);
            $DakList[$j]['sender'] = h($value['officer_designation_label']) . ', ' . h($value['office_unit_name']) ;
            $DakList[$j]['sarok_no'] = h($value['sarok_no']) ;
            $DakList[$j]['potrojari_date'] = $value['potrojari_date'] ;
            $DakList[$j]['prapoks'] = !empty($result['prapoks'][$value['id']])?implode("\n", $result['prapoks'][$value['id']]):'' ;
            $DakList[$j]['onulipis'] = !empty($result['onulipis'][$value['id']])?implode("\n", $result['onulipis'][$value['id']]):'' ;
            $DakList[$j]['potro_type'] = $result['potroTypeInfo'][$value['potro_type']] ;
                $j++;
            }
        }
        if(empty($DakList)){
            $DakList= Array();
        }
        if($type == 'excel'){
            $this->printExcel($DakList, [
                ['key' => 'office_unit_name', 'title' => 'শাখা'],
                ['key' => 'potro_subject', 'title' => 'বিষয়'],
                ['key' => 'sender', 'title' => 'প্রেরক'],
                ['key' => 'sarok_no', 'title' => 'স্মারক নম্বর'],
                ['key' => 'potrojari_date', 'title' => 'প্রেরণের তারিখ'],
                ['key' => 'prapoks', 'title' => 'প্রাপকসমূহ'],
                ['key' => 'onulipis', 'title' => 'অনুলিপিসমূহ'],
                ['key' => 'potro_type', 'title' => 'পত্র ধরন'],
            ],[
                'name'=>((isset($result['office_name']))?$result['office_name'].' অফিসের ':'') .'পত্রজারি নিবন্ধন বহি' ,
                'title' => [
                    ((isset($result['office_name']))?$result['office_name'].' অফিসের ':'') .'পত্রজারি নিবন্ধন বহি',
                    $dateRangeBn,
                ]
            ]);
        } else {
            $this->printPdf($DakList, [
                ['key' => 'office_unit_name', 'title' => 'শাখা'],
                ['key' => 'potro_subject', 'title' => 'বিষয়'],
                ['key' => 'sender', 'title' => 'প্রেরক'],
                ['key' => 'sarok_no', 'title' => 'স্মারক নম্বর'],
                ['key' => 'potrojari_date', 'title' => 'প্রেরণের তারিখ'],
                ['key' => 'prapoks', 'title' => 'প্রাপকসমূহ'],
                ['key' => 'onulipis', 'title' => 'অনুলিপিসমূহ'],
                ['key' => 'potro_type', 'title' => 'পত্র ধরন'],
            ],[
                'name'=>((isset($result['office_name']))?$result['office_name'].' অফিসের ':'') .'পত্রজারি নিবন্ধন বহি' ,
                'title' => [
                    ((isset($result['office_name']))?$result['office_name'].' অফিসের ':'') .'পত্রজারি নিবন্ধন বহি',
                    $dateRangeBn,
                ]
            ]);
        }

        exit;
    }

    public function nothiPotrojariRegisterContent($startDate = "", $endDate = "",$isExport="",$per_page = 20 ,$unit_id = "") {
        $this->layout = null;
        $condition = [];

        $employee_office = $this->getCurrentDakSection();

        $officeUnitTable = TableRegistry::get("OfficeUnits");
      


        $startDate = !empty($startDate)?h($startDate):date('Y-m-d');
        $endDate = !empty($endDate)?h($endDate):date('Y-m-d');

        $units = $officeUnitTable->getOfficeChildInfo($employee_office['office_unit_id']);
        $unitsOwn = $officeUnitTable->getOwnChildInfo($employee_office['office_unit_id']);

        $units = !empty($units[0]) ? $units[0] : '';
        $unitsOwn = !empty($unitsOwn[0]) ? ($employee_office['office_unit_id'] . ',' . $unitsOwn[0]) : $employee_office['office_unit_id'];

        $units = $units . (!empty($unitsOwn) ? ((!empty($units) ? ',' : '') . $unitsOwn) : '');

        $unit_id_array = explode(",",$units);
        if(!empty($unit_id) && in_array((int)$unit_id,$unit_id_array)){
            $unit_id_array = (int)$unit_id;
        }
        array_push($condition,["potrojari.office_unit_id IN"=>$unit_id_array]);


        $report_table = TableRegistry::get('potrojari');
        TableRegistry::remove('NothiMasterPermissions');
        $nothiMastersPermissionTable = TableRegistry::get('NothiMasterPermissions');
        //Previously we use allNothiPermissionList method. but we need all potrojari of every unit.
        $permitted_nothi_list = $nothiMastersPermissionTable->UnitwiseNothiPermissionList($employee_office['office_id'], $employee_office['office_id'], $units, 1);
        $query= $report_table->find()
            ->join([
                'NothiParts' => [
                    'table' => 'nothi_parts',
                    "conditions" => ["potrojari.nothi_master_id = NothiParts.nothi_masters_id","potrojari.nothi_part_no = NothiParts.id"],
                    "type" => "INNER"
                ]
            ])
            ->where(function($query) use ($startDate,$endDate){
                return $query->between('date(potrojari.potrojari_date)',$startDate,$endDate);
            })
            ->where($condition)
            ->where(['NothiParts.id IN' => $permitted_nothi_list])
            ->where(['potro_status' => 'Sent'])
            ->order(['potrojari.potrojari_date desc']);

        if($isExport=='excel' || $isExport=='pdf'){
            $reports = $query->toArray();
        } else {
            $reports = $this->Paginator->paginate($query, ['limit' => $per_page]);
        }
        $office = TableRegistry::get('Offices');
        $office_name = $office->find()->select(['office_name_bng', 'office_address'])->where(['id' => $employee_office['office_id']])->first();

        $preorder = '';
        $data = [];
        $i = 0;
        $templete_ids = [];
        $potro_ids = [];
        if (!empty($reports)) {
            foreach ($reports as $dak) {
                if (empty($preorder)) {
                    $preorder = $dak['office_unit_id'];
                }
                $preorder = $preorder . ',' . $dak['office_unit_id'];
            }
            if (!empty($preorder)) {
                $order_unit = $officeUnitTable->getOrderwiseUnits($preorder);
            }
            if (!empty($order_unit)) {
                foreach ($order_unit as $key => $val) {
                    foreach ($reports as $dak) {
                        if ($dak['office_unit_id'] == $key) {
                            $templete_ids[]= $dak['potro_type'];
                            $potro_ids[]= $dak['id'];

                            $data[$i]['id'] = $dak['id'];
                            $data[$i]['nothi_master_id'] = $dak['nothi_master_id'];
                            $data[$i]['nothi_part_no'] = $dak['nothi_part_no'];
                            $data[$i]['potrojari_draft_office_id'] = $dak['potrojari_draft_office_id'];
                            $data[$i]['potrojari_draft_unit'] = $dak['potrojari_draft_unit'];
                            $data[$i]['nothi_notes_id'] = $dak['nothi_notes_id'];
                            $data[$i]['nothi_potro_id'] = $dak['nothi_potro_id'];
                            $data[$i]['potro_type'] = $dak['potro_type'];
                            $data[$i]['sarok_no'] = $dak['sarok_no'];
                            $data[$i]['dak_id'] = $dak['dak_id'];
                            $data[$i]['can_potrojari'] = $dak['can_potrojari'];
                            $data[$i]['office_id'] = $dak['office_id'];
                            $data[$i]['officer_id'] = $dak['officer_id'];
                            $data[$i]['office_name'] = $dak['office_name'];
                            $data[$i]['officer_name'] = $dak['officer_name'];
                            $data[$i]['office_unit_id'] = $dak['office_unit_id'];
                            $data[$i]['office_unit_name'] = $dak['office_unit_name'];
                            $data[$i]['officer_designation_id'] = $dak['officer_designation_id'];
                            $data[$i]['officer_designation_label'] = $dak['officer_designation_label'];
                            $data[$i]['sovapoti_office_id'] = $dak['sovapoti_office_id'];
                            $data[$i]['sovapoti_officer_id'] = $dak['sovapoti_officer_id'];
                            $data[$i]['sovapoti_officer_designation_id'] = $dak['sovapoti_officer_designation_id'];
                            $data[$i]['sovapoti_officer_designation_label'] = $dak['sovapoti_officer_designation_label'];
                            $data[$i]['sovapoti_officer_name'] = $dak['sovapoti_officer_name'];
                            $data[$i]['potrojari_date'] = $dak['potrojari_date'];
                            $data[$i]['potro_subject'] = $dak['potro_subject'];
                            $data[$i]['potro_security_level'] = $dak['potro_security_level'];
                            $data[$i]['potro_priority_level'] = $dak['potro_priority_level'];
                            $data[$i]['potro_cover'] = $dak['potro_cover'];
                            $data[$i]['potro_description'] = $dak['potro_description'];
                            
                            $i++;
                        }
                    }
                }
            }
        }
        $potrojari_onulipi_Table = TableRegistry::get("PotrojariOnulipi");
        $potrojari_receiver_Table = TableRegistry::get("PotrojariReceiver");
        $potrojariTemplateTable = TableRegistry::get("PotrojariTemplates");

        $potroTypeInfo = $potrojariTemplateTable->find('list', ['keyField' => 'id', 'valueField' => 'template_name'])->where(['id IN'=>array_unique($templete_ids)])->toArray();

        if($isExport=='excel' || $isExport=='pdf'){
            $prapoks = $potrojari_receiver_Table->find()->select(['potrojari_id','prapok' => 'IF(receiving_office_id>0,concat(receiving_officer_designation_label,", ",receiving_office_unit_name,", ",receiving_office_name, "(সিস্টেম)"),concat(receiving_officer_designation_label,", ",receiving_office_name," (মেইল) "))'])->where(['potrojari_id IN' => array_unique($potro_ids)])->toArray();

            $onulipis = $potrojari_onulipi_Table->find()->select(['potrojari_id','prapok' => 'IF(receiving_office_id>0,concat(receiving_officer_designation_label,", ",receiving_office_unit_name,", ",receiving_office_name, " (সিস্টেম) "),concat(receiving_officer_designation_label,", ",receiving_office_name," (মেইল) "))'])->where(['potrojari_id IN' => array_unique($potro_ids)])->toArray();

        } else {

            $prapoks = $potrojari_receiver_Table->find()->select(['potrojari_id', 'prapok' => 'IF(receiving_office_id>0,concat(receiving_officer_designation_label,", ",receiving_office_unit_name,", ",receiving_office_name, " <b>(সিস্টেম)</b> "),concat(receiving_officer_designation_label,", ",receiving_office_name," <b>(মেইল)</b> "))'])->where(['potrojari_id IN' => array_unique($potro_ids)])->toArray();

            $onulipis = $potrojari_onulipi_Table->find()->select(['potrojari_id', 'prapok' => 'IF(receiving_office_id>0,concat(receiving_officer_designation_label,", ",receiving_office_unit_name,", ",receiving_office_name, " <b>(সিস্টেম)</b> "),concat(receiving_officer_designation_label,", ",receiving_office_name," <b>(মেইল)</b> "))'])->where(['potrojari_id IN' => array_unique($potro_ids)])->toArray();
        }

        $formatted_prapoks= [];
        foreach ($prapoks as $prapok){
            $formatted_prapoks[$prapok['potrojari_id']][]= $prapok['prapok'];
        }
        $formatted_onulipis= [];
        foreach ($onulipis as $onulipi){
            $formatted_onulipis[$onulipi['potrojari_id']][]= $onulipi['prapok'];
        }

        $priority = json_decode(DAK_PRIORITY_TYPE, true);
        $security = json_decode(DAK_SECRECY_TYPE, true);

        if($isExport=='excel' || $isExport=='pdf'){
            return $result = Array('data' => $data, 'office_name' => $office_name['office_name_bng'],'potroTypeInfo'=>$potroTypeInfo,'prapoks'=>$formatted_prapoks,'onulipis'=>$formatted_onulipis,'priority'=>$priority,'security'=>$security);
        }

        $this->set('data', $data);
        $this->set('potroTypeInfo', $potroTypeInfo);
        $this->set('prapoks', $formatted_prapoks);
        $this->set('onulipis', $formatted_onulipis);
        $this->set('office_name', $office_name);
        $this->set('priority', $priority);
        $this->set('security', $security);

    }
//    public function nothiPotrojariRegisterContent($startDate = "", $endDate = "",$isExport="", $unit_id=0) {
//        $this->layout = null;
//        $condition = '1 ';
//
//        $employee_office = $this->getCurrentDakSection();
//
//        $officeUnitTable = TableRegistry::get("OfficeUnits");
//
//
//        if (empty($startDate) && empty($endDate)) {
//               $startDate = date('Y-m-d');
//            $endDate = date('Y-m-d');
//        }
//        $startDate = !empty($startDate)?h($startDate):'';
//        $endDate = !empty($endDate)?h($endDate):'';
//            if (!empty($condition)){
//                $condition .= ' AND ';
//            }
//            $condition .= " DATE(potrojari.potrojari_date)  BETWEEN  '{$startDate}' AND '{$endDate}' ";
//
//        $units = $officeUnitTable->getOfficeChildInfo($employee_office['office_unit_id']);
//        $unitsOwn = $officeUnitTable->getOwnChildInfo($employee_office['office_unit_id']);
//
//        $units = !empty($units[0]) ? $units[0] : '';
//        $unitsOwn = !empty($unitsOwn[0]) ? ($employee_office['office_unit_id'] . ',' . $unitsOwn[0]) : $employee_office['office_unit_id'];
//
//        $units = $units . (!empty($unitsOwn) ? ((!empty($units) ? ',' : '') . $unitsOwn) : '');
////
//
//        $condition .= " AND potrojari.office_unit_id IN ({$units}) ";
//        if ($unit_id != 0) {
//            $condition .= " AND potrojari.office_unit_id = {$unit_id}";
//        }
//
//
//        $report_table = TableRegistry::get('potrojari');
//        TableRegistry::remove('NothiMasterPermissions');
//        $nothiMastersPermissionTable = TableRegistry::get('NothiMasterPermissions');
//        //Previously we use allNothiPermissionList method. but we need all potrojari of every unit.
//        $permitted_nothi_list = $nothiMastersPermissionTable->UnitwiseNothiPermissionList($employee_office['office_id'], $employee_office['office_id'], $units, 1);
//        $query= $report_table->find()
//            ->join([
//                'NothiParts' => [
//                    'table' => 'nothi_parts',
//                    "conditions" => "potrojari.nothi_master_id = NothiParts.nothi_masters_id AND potrojari.nothi_part_no = NothiParts.id",
//                    "type" => "INNER"
//                ]
//            ])
//            ->where($condition)->where(['NothiParts.id IN' => $permitted_nothi_list])->andWhere(['potro_status' => 'Sent'])->order(['potrojari.potrojari_date desc']);
//
//        if($isExport=='excel' || $isExport=='pdf'){
//            $reports = $query->toArray();
//        } else {
//            $reports = $this->Paginator->paginate($query, ['limit' => 20]);
//        }
//        $office = TableRegistry::get('Offices');
//        $office_name = $office->find()->select(['office_name_bng', 'office_address'])->where(['id' => $employee_office['office_id']])->first();
//
//        $preorder = '';
//        $data = [];
//        $i = 0;
//        $templete_ids = [];
//        $potro_ids = [];
//        if (!empty($reports)) {
//            foreach ($reports as $dak) {
//                if (empty($preorder)) {
//                    $preorder = $dak['office_unit_id'];
//                }
//                $preorder = $preorder . ',' . $dak['office_unit_id'];
//            }
//            if (!empty($preorder)) {
//                $order_unit = $officeUnitTable->getOrderwiseUnits($preorder);
//            }
//            if (!empty($order_unit)) {
//                foreach ($order_unit as $key => $val) {
//                    foreach ($reports as $dak) {
//                        if ($dak['office_unit_id'] == $key) {
//                            $templete_ids[]= $dak['potro_type'];
//                            $potro_ids[]= $dak['id'];
//
//                            $data[$i]['id'] = $dak['id'];
//                            $data[$i]['nothi_master_id'] = $dak['nothi_master_id'];
//                            $data[$i]['nothi_part_no'] = $dak['nothi_part_no'];
//                            $data[$i]['potrojari_draft_office_id'] = $dak['potrojari_draft_office_id'];
//                            $data[$i]['potrojari_draft_unit'] = $dak['potrojari_draft_unit'];
//                            $data[$i]['nothi_notes_id'] = $dak['nothi_notes_id'];
//                            $data[$i]['nothi_potro_id'] = $dak['nothi_potro_id'];
//                            $data[$i]['potro_type'] = $dak['potro_type'];
//                            $data[$i]['sarok_no'] = $dak['sarok_no'];
//                            $data[$i]['dak_id'] = $dak['dak_id'];
//                            $data[$i]['can_potrojari'] = $dak['can_potrojari'];
//                            $data[$i]['office_id'] = $dak['office_id'];
//                            $data[$i]['officer_id'] = $dak['officer_id'];
//                            $data[$i]['office_name'] = $dak['office_name'];
//                            $data[$i]['officer_name'] = $dak['officer_name'];
//                            $data[$i]['office_unit_id'] = $dak['office_unit_id'];
//                            $data[$i]['office_unit_name'] = $dak['office_unit_name'];
//                            $data[$i]['officer_designation_id'] = $dak['officer_designation_id'];
//                            $data[$i]['officer_designation_label'] = $dak['officer_designation_label'];
//                            $data[$i]['sovapoti_office_id'] = $dak['sovapoti_office_id'];
//                            $data[$i]['sovapoti_officer_id'] = $dak['sovapoti_officer_id'];
//                            $data[$i]['sovapoti_officer_designation_id'] = $dak['sovapoti_officer_designation_id'];
//                            $data[$i]['sovapoti_officer_designation_label'] = $dak['sovapoti_officer_designation_label'];
//                            $data[$i]['sovapoti_officer_name'] = $dak['sovapoti_officer_name'];
//                            $data[$i]['potrojari_date'] = $dak['potrojari_date'];
//                            $data[$i]['potro_subject'] = $dak['potro_subject'];
//                            $data[$i]['potro_security_level'] = $dak['potro_security_level'];
//                            $data[$i]['potro_priority_level'] = $dak['potro_priority_level'];
//                            $data[$i]['potro_cover'] = $dak['potro_cover'];
//                            $data[$i]['potro_description'] = $dak['potro_description'];
//
//                            $i++;
//                        }
//                    }
//                }
//            }
//        }
//        $potrojari_onulipi_Table = TableRegistry::get("PotrojariOnulipi");
//        $potrojari_receiver_Table = TableRegistry::get("PotrojariReceiver");
//        $potrojariTemplateTable = TableRegistry::get("PotrojariTemplates");
//
//        $potroTypeInfo = $potrojariTemplateTable->find('list', ['keyField' => 'id', 'valueField' => 'template_name'])->where(['id IN'=>array_unique($templete_ids)])->toArray();
//
//        if($isExport=='excel' || $isExport=='pdf'){
//            $prapoks = $potrojari_receiver_Table->find()->select(['potrojari_id','prapok' => 'IF(receiving_office_id>0,concat(receiving_officer_designation_label,", ",receiving_office_unit_name,", ",receiving_office_name, "(সিস্টেম)"),concat(receiving_officer_designation_label,", ",receiving_office_name," (মেইল) "))'])->where(['potrojari_id IN' => array_unique($potro_ids)])->toArray();
//
//            $onulipis = $potrojari_onulipi_Table->find()->select(['potrojari_id','prapok' => 'IF(receiving_office_id>0,concat(receiving_officer_designation_label,", ",receiving_office_unit_name,", ",receiving_office_name, " (সিস্টেম) "),concat(receiving_officer_designation_label,", ",receiving_office_name," (মেইল) "))'])->where(['potrojari_id IN' => array_unique($potro_ids)])->toArray();
//
//        } else {
//
//            $prapoks = $potrojari_receiver_Table->find()->select(['potrojari_id', 'prapok' => 'IF(receiving_office_id>0,concat(receiving_officer_designation_label,", ",receiving_office_unit_name,", ",receiving_office_name, " <b>(সিস্টেম)</b> "),concat(receiving_officer_designation_label,", ",receiving_office_name," <b>(মেইল)</b> "))'])->where(['potrojari_id IN' => array_unique($potro_ids)])->toArray();
//
//            $onulipis = $potrojari_onulipi_Table->find()->select(['potrojari_id', 'prapok' => 'IF(receiving_office_id>0,concat(receiving_officer_designation_label,", ",receiving_office_unit_name,", ",receiving_office_name, " <b>(সিস্টেম)</b> "),concat(receiving_officer_designation_label,", ",receiving_office_name," <b>(মেইল)</b> "))'])->where(['potrojari_id IN' => array_unique($potro_ids)])->toArray();
//        }
//
//        $formatted_prapoks= [];
//        foreach ($prapoks as $prapok){
//            $formatted_prapoks[$prapok['potrojari_id']][]= $prapok['prapok'];
//        }
//        $formatted_onulipis= [];
//        foreach ($onulipis as $onulipi){
//            $formatted_onulipis[$onulipi['potrojari_id']][]= $onulipi['prapok'];
//        }
//
//        $priority = json_decode(DAK_PRIORITY_TYPE, true);
//        $security = json_decode(DAK_SECRECY_TYPE, true);
//
//        if($isExport=='excel' || $isExport=='pdf'){
//            return $result = Array('data' => $data, 'office_name' => $office_name['office_name_bng'],'potroTypeInfo'=>$potroTypeInfo,'prapoks'=>$formatted_prapoks,'onulipis'=>$formatted_onulipis,'priority'=>$priority,'security'=>$security);
//        }
//
//        $this->set('data', $data);
//        $this->set('potroTypeInfo', $potroTypeInfo);
//        $this->set('prapoks', $formatted_prapoks);
//        $this->set('onulipis', $formatted_onulipis);
//        $this->set('office_name', $office_name);
//        $this->set('priority', $priority);
//        $this->set('security', $security);
//
//    }
     public function binostojoggoNothiList() {
        $session = $this->request->session();
        $employee = $session->read('logged_employee_record');
        $this->set("office", $employee);
    }

    public function binostojoggoNothiListContent($startDate = "", $endDate = "") {

        $this->layout = null;
        $condition = [];
        $employee_office = $this->getCurrentDakSection();

        $nothiClass = json_decode(NOTHI_CLASS, true);
        $officeUnitTable = TableRegistry::get("OfficeUnits");

        $units = $officeUnitTable->getOfficeChildInfo($employee_office['office_unit_id']);
        $unitsOwn = $officeUnitTable->getOwnChildInfo($employee_office['office_unit_id']);

        $units = !empty($units[0]) ? $units[0] : '';
        $unitsOwn = !empty($unitsOwn[0]) ? ($employee_office['office_unit_id'] . ',' . $unitsOwn[0]) : $employee_office['office_unit_id'];

        $units = $units . (!empty($unitsOwn) ? ((!empty($units) ? ',' : '') . $unitsOwn) : '');

        array_push($condition,["NothiParts.office_units_id IN"=> explode(",",$units)]);
        array_push($condition,["NothiParts.nothi_class IN"=> [2,3]]);

        $startDate = !empty($startDate)?h($startDate):date('Y-m-d');
        $endDate = !empty($endDate)?h($endDate):date('Y-m-d');


        $report_table = TableRegistry::get('Reports');

        $reports = $this->Paginator->paginate($report_table->getAllSectionWiseNothi($startDate,$endDate,$condition),['limit' => 20]);

        $office = TableRegistry::get('Offices');
        $office_name = $office->find()->select(['office_name_bng', 'office_address'])->where(['id' => $employee_office['office_id']])->first();

        $preorder = '';
        $data = [];
        $i = 0;
        if (!empty($reports)) {
            foreach ($reports as $dak) {
                if (empty($preorder)) {
                    $preorder = $dak['office_units_id'];
                }
                $preorder = $preorder . ',' . $dak['office_units_id'];
            }
            if (!empty($preorder)) {
                $order_unit = $officeUnitTable->getOrderwiseUnits($preorder);
            }
            if (!empty($order_unit)) {
                foreach ($order_unit as $key => $val) {
                    foreach ($reports as $dak) {
                        if ($dak['office_units_id'] == $key) {
                            $data[$i]['nothi_created_date'] = $dak['nothi_created_date'];
                            $data[$i]['id'] = $dak['id'];
                            $data[$i]['nothi_class'] = $dak['nothi_class'];
                            $data[$i]['nothi_part_no'] = $dak['nothi_part_no'];
                            $data[$i]['subject'] = $dak['subject'];
                            $data[$i]['nothi_no'] = $dak['nothi_no'];
                            $data[$i]['created'] = $dak['created'];
                            $data[$i]['modified'] = $dak['modified'];
                            $data[$i]['nothi_types_id'] = $dak['nothi_types_id'];
                            $data[$i]['office_units_id'] = $dak['office_units_id'];
                            $data[$i]['office_units_organogram_id'] = $dak['office_units_organogram_id'];
                            $data[$i]['office_id'] = $dak['office_id'];
                            $data[$i]['NothiTypes'] = $dak['NothiTypes'];
                          
                            $i++;
                        }
                    }
                }
            }
        }
        $this->set('data', $data);
        $this->set('office_name', $office_name);
        $this->set('nothiClass', $nothiClass);
        
    }
//    public function binostojoggoNothiListContent($startDate = "", $endDate = "") {
//
//        $this->layout = null;
//        $condition = '1 ';
//        $employee_office = $this->getCurrentDakSection();
//
//        $nothiClass = json_decode(NOTHI_CLASS, true);
//        $officeUnitTable = TableRegistry::get("OfficeUnits");
//
//        $units = $officeUnitTable->getOfficeChildInfo($employee_office['office_unit_id']);
//        $unitsOwn = $officeUnitTable->getOwnChildInfo($employee_office['office_unit_id']);
//
//        $units = !empty($units[0]) ? $units[0] : '';
//        $unitsOwn = !empty($unitsOwn[0]) ? ($employee_office['office_unit_id'] . ',' . $unitsOwn[0]) : $employee_office['office_unit_id'];
//
//        $units = $units . (!empty($unitsOwn) ? ((!empty($units) ? ',' : '') . $unitsOwn) : '');
//
//        $condition .= " AND NothiParts.office_units_id IN ({$units})";
//        $startDate = $startDate;
//        $endDate = $endDate;
//
//        if (!empty($startDate) And ! empty($endDate)) {
//            if (!empty($condition))
//                $condition .= ' AND ';
//            $condition .= " NothiParts.nothi_created_date  BETWEEN  '{$startDate} 00:00:00' AND '{$endDate}
//            23:59:59' ";
//        } else {
//            $startDate = date('Y-m-d');
//            $endDate = date('Y-m-d');
//            if (!empty($condition))
//                $condition .= ' AND ';
//            $condition .= " NothiParts.nothi_created_date  BETWEEN  '{$startDate} 00:00:00' AND '{$endDate}
//            23:59:59' ";
//        }
//         $condition .= " AND (NothiParts.nothi_class=2 or NothiParts.nothi_class=3)";
//        $report_table = TableRegistry::get('Reports');
//
//        $reports = $this->Paginator->paginate($report_table->getAllSectionWiseNothi($condition),['limit' => 20]);
//
//        $office = TableRegistry::get('Offices');
//        $office_name = $office->find()->select(['office_name_bng', 'office_address'])->where(['id' => $employee_office['office_id']])->first();
//
//        $preorder = '';
//        $data = [];
//        $i = 0;
//        if (!empty($reports)) {
//            foreach ($reports as $dak) {
//                if (empty($preorder)) {
//                    $preorder = $dak['office_units_id'];
//                }
//                $preorder = $preorder . ',' . $dak['office_units_id'];
//            }
//            if (!empty($preorder)) {
//                $order_unit = $officeUnitTable->getOrderwiseUnits($preorder);
//            }
//            if (!empty($order_unit)) {
//                foreach ($order_unit as $key => $val) {
//                    foreach ($reports as $dak) {
//                        if ($dak['office_units_id'] == $key) {
//                            $data[$i]['nothi_created_date'] = $dak['nothi_created_date'];
//                            $data[$i]['id'] = $dak['id'];
//                            $data[$i]['nothi_class'] = $dak['nothi_class'];
//                            $data[$i]['nothi_part_no'] = $dak['nothi_part_no'];
//                            $data[$i]['subject'] = $dak['subject'];
//                            $data[$i]['nothi_no'] = $dak['nothi_no'];
//                            $data[$i]['created'] = $dak['created'];
//                            $data[$i]['modified'] = $dak['modified'];
//                            $data[$i]['nothi_types_id'] = $dak['nothi_types_id'];
//                            $data[$i]['office_units_id'] = $dak['office_units_id'];
//                            $data[$i]['office_units_organogram_id'] = $dak['office_units_organogram_id'];
//                            $data[$i]['office_id'] = $dak['office_id'];
//                            $data[$i]['NothiTypes'] = $dak['NothiTypes'];
//
//                            $i++;
//                        }
//                    }
//                }
//            }
//        }
//        $this->set('data', $data);
//        $this->set('office_name', $office_name);
//        $this->set('nothiClass', $nothiClass);
//
//    }

      public function jerList() {
        $session = $this->request->session();
        $employee = $session->read('logged_employee_record');
        $this->set("office", $employee);
    }

    public function jerListContent($startDate = "", $endDate = "") {
        $this->layout=null;
        $data = '';
        $this->set('data',$data);
    }
    
     public function oneMonthList() {
        $session = $this->request->session();
        $employee = $session->read('logged_employee_record');
        $this->set("office", $employee);
    }

    public function oneMonthListContent($startDate = "", $endDate = "") {
        $this->layout=null;
        $data = '';
        $this->set('data',$data);
    }
    
      public function nothiMovementList() {
        $session = $this->request->session();
        $employee = $session->read('logged_employee_record');
        $this->set("office", $employee);
    }

    public function nothiMovementListContent($startDate = "", $endDate = "") {
        $this->layout = null;
        $condition = [];
        $orCondition = [];

        $employee_office = $this->getCurrentDakSection();

        $officeUnitTable = TableRegistry::get("OfficeUnits");

        $startDate = !empty($startDate)?h($startDate):date('Y-m-d');
        $endDate = !empty($endDate)?h($endDate):date('Y-m-d');


        $units = $officeUnitTable->getOfficeChildInfo($employee_office['office_unit_id']);
        $unitsOwn = $officeUnitTable->getOwnChildInfo($employee_office['office_unit_id']);

        $units = !empty($units[0]) ? $units[0] : '';
        $unitsOwn = !empty($unitsOwn[0]) ? ($employee_office['office_unit_id'] . ',' . $unitsOwn[0]) : $employee_office['office_unit_id'];

        $units = $units . (!empty($unitsOwn) ? ((!empty($units) ? ',' : '') . $unitsOwn) : '');
//

        array_push($condition,["NothiMasterMovements.from_office_unit_id IN" =>explode(",",$units)]);
        array_push($orCondition,["NothiMasterMovements.to_office_unit_id IN"=>explode(",",$units)]);


        $report_table = TableRegistry::get('Reports');

        $reports = $this->Paginator->paginate($report_table->getAllSentNothi($startDate,$endDate,'NothiMasterMovements.created',$condition,$orCondition), ['limit' => 20]);



        $office = TableRegistry::get('Offices');
        $office_name = $office->find()->select(['office_name_bng', 'office_address'])->where(['id' => $employee_office['office_id']])->first();

        $preorder = '';
        $data = [];
        $i = 0;
        if (!empty($reports)) {
            foreach ($reports as $dak) {
                if (empty($preorder)) {
                    $preorder = $dak['from_office_unit_id'];
                }
                $preorder = $preorder . ',' . $dak['from_office_unit_id'];
            }
            if (!empty($preorder)) {
                $order_unit = $officeUnitTable->getOrderwiseUnits($preorder);
            }
            if (!empty($order_unit)) {
                foreach ($order_unit as $key => $val) {
                    foreach ($reports as $dak) {
                        if ($dak['from_office_unit_id'] == $key) {
                            $data[$i]['id'] = $dak['id'];
                            $data[$i]['modified'] = $dak['modified'];
                            $data[$i]['from_office_unit_id'] = $dak['from_office_unit_id'];
                            $data[$i]['from_office_unit_name'] = $dak['from_office_unit_name'];
                            $data[$i]['from_officer_name'] = $dak['from_officer_name'];
                            $data[$i]['created'] = $dak['created'];
                            $data[$i]['to_office_unit_id'] = $dak['to_office_unit_id'];
                            $data[$i]['to_office_unit_name'] = $dak['to_office_unit_name'];
                            $data[$i]['from_office_name'] = $dak['from_office_name'];
                            $data[$i]['to_office_name'] = $dak['to_office_name'];
                            $data[$i]['to_officer_name'] = $dak['to_officer_name'];
                            $data[$i]['from_officer_designation_label'] = $dak['from_officer_designation_label'];
                            $data[$i]['to_officer_designation_label'] = $dak['to_officer_designation_label'];
                            $data[$i]['NothiMasters'] = $dak['NothiMasters'];

                            $i++;
                        }
                    }
                }
            }
        }
//        pr($data);die;
        $this->set('data', $data);
        $this->set('office_name', $office_name);
    }
//    public function nothiMovementListContent($startDate = "", $endDate = "") {
//       $this->layout = null;
//        $condition = '1 ';
//
//        $employee_office = $this->getCurrentDakSection();
//
//        $officeUnitTable = TableRegistry::get("OfficeUnits");
//
//
//        $startDate = $startDate;
//
//        $endDate = $endDate;
//
//        if (!empty($startDate) And ! empty($endDate)) {
//            if (!empty($condition))
//                $condition .= ' AND ';
//            $condition .= " NothiMasterMovements.created  BETWEEN  '{$startDate} 00:00:00' AND '{$endDate}  23:59:59' ";
//        } else {
//            $startDate = date('Y-m-d');
//            $endDate = date('Y-m-d');
//            if (!empty($condition))
//                $condition .= ' AND ';
//            $condition .= " NothiMasterMovements.created  BETWEEN  '{$startDate} 00:00:00' AND '{$endDate}  23:59:59' ";
//        }
//
//        $units = $officeUnitTable->getOfficeChildInfo($employee_office['office_unit_id']);
//        $unitsOwn = $officeUnitTable->getOwnChildInfo($employee_office['office_unit_id']);
//
//        $units = !empty($units[0]) ? $units[0] : '';
//        $unitsOwn = !empty($unitsOwn[0]) ? ($employee_office['office_unit_id'] . ',' . $unitsOwn[0]) : $employee_office['office_unit_id'];
//
//        $units = $units . (!empty($unitsOwn) ? ((!empty($units) ? ',' : '') . $unitsOwn) : '');
////
//
//        $condition .= " AND (NothiMasterMovements.from_office_unit_id IN ({$units}) OR  NothiMasterMovements.to_office_unit_id IN ({$units}))";
//
//
//        $report_table = TableRegistry::get('Reports');
//
//        $reports = $this->Paginator->paginate($report_table->getAllSentNothi($condition), ['limit' => 20]);
//
//
//
//        $office = TableRegistry::get('Offices');
//        $office_name = $office->find()->select(['office_name_bng', 'office_address'])->where(['id' => $employee_office['office_id']])->first();
//
//        $preorder = '';
//        $data = [];
//        $i = 0;
//        if (!empty($reports)) {
//            foreach ($reports as $dak) {
//                if (empty($preorder)) {
//                    $preorder = $dak['from_office_unit_id'];
//                }
//                $preorder = $preorder . ',' . $dak['from_office_unit_id'];
//            }
//            if (!empty($preorder)) {
//                $order_unit = $officeUnitTable->getOrderwiseUnits($preorder);
//            }
//            if (!empty($order_unit)) {
//                foreach ($order_unit as $key => $val) {
//                    foreach ($reports as $dak) {
//                        if ($dak['from_office_unit_id'] == $key) {
//                            $data[$i]['id'] = $dak['id'];
//                            $data[$i]['modified'] = $dak['modified'];
//                            $data[$i]['from_office_unit_id'] = $dak['from_office_unit_id'];
//                            $data[$i]['from_office_unit_name'] = $dak['from_office_unit_name'];
//                            $data[$i]['from_officer_name'] = $dak['from_officer_name'];
//                            $data[$i]['created'] = $dak['created'];
//                            $data[$i]['to_office_unit_id'] = $dak['to_office_unit_id'];
//                            $data[$i]['to_office_unit_name'] = $dak['to_office_unit_name'];
//                            $data[$i]['from_office_name'] = $dak['from_office_name'];
//                            $data[$i]['to_office_name'] = $dak['to_office_name'];
//                            $data[$i]['to_officer_name'] = $dak['to_officer_name'];
//                            $data[$i]['from_officer_designation_label'] = $dak['from_officer_designation_label'];
//                            $data[$i]['to_officer_designation_label'] = $dak['to_officer_designation_label'];
//                            $data[$i]['NothiMasters'] = $dak['NothiMasters'];
//
//                            $i++;
//                        }
//                    }
//                }
//            }
//        }
////        pr($data);die;
//        $this->set('data', $data);
//        $this->set('office_name', $office_name);
//    }
     public function nothiPermissionAdd()
    {
        set_time_limit(0);
        TableRegistry::remove('NothiMasterPermissions');
        $nothiMasterPermissions = TableRegistry::get('NothiMasterPermissions');
        $employee_office        = $this->getCurrentDakSection();
        if ($this->request->is('get')) {
            $all_NothiMaster_info = $nothiMasterPermissions->find('list',
                    ['keyField' => 'id', 'valueField' => 'nothi_masters_id'])->where(['nothi_office' => $employee_office['office_id'], 'office_unit_organograms_id' => $employee_office['office_unit_organogram_id']])->toArray();
            $NothiMaster = [];
            if (!empty($all_NothiMaster_info)) {
                $NothiMastersTable = TableRegistry::get('NothiMasters');
                $NothiMaster = $NothiMastersTable->find('list', ['keyField' => 'id', 'valueField' => 'subject'])->where(['id IN' => array_values($all_NothiMaster_info)]);
                if (isset($this->request->query['subject'])) {
                    $NothiMaster = $NothiMaster->where(['subject like' => '%'.$this->request->query['subject'].'%'])->toArray();
                    $this->response->body(json_encode($NothiMaster));
                    $this->response->type('application/json');
                    return $this->response;
                } else {
                    $NothiMaster = $NothiMaster->toArray();
                }
            }
            $this->set(compact('NothiMaster', 'employee_office'));
        } else {
            try {
                $nothi_part_id_group_by_master_id = $this->request->data['ids'];
                $all_part_ids = "";
                foreach ($nothi_part_id_group_by_master_id as $master_id => $part_ids) {
                    $all_part_ids .= implode(',', $part_ids);
                    $all_part_ids .= ',';
                }
                $all_part_ids = explode(',', $all_part_ids);
                $Master_ids = array_keys($nothi_part_id_group_by_master_id);
                $permission_list = $this->request->data['permitted'];
                if (!empty($permission_list)) {
                    $all_NothiMaster_info = $nothiMasterPermissions->find('list',
                            ['keyField' => 'nothi_part_no', 'valueField' => 'nothi_masters_id'])->where(['office_unit_organograms_id' => $employee_office['office_unit_organogram_id'],
                            'nothi_office' => $employee_office['office_id'], 'office_id' => $employee_office['office_id'],
                            'office_unit_id' => $employee_office['office_unit_id'], 'privilige_type >' => 0,
                            'nothi_part_no IN' => array_values($all_part_ids)])->group(['nothi_part_no'])->toArray();
                    foreach ($permission_list as $value) {
                        $all_part_no = $nothiMasterPermissions->find('list',
                                ['keyField' => 'nothi_part_no', 'valueField' => 'nothi_masters_id'])->where([
                                'nothi_office' => $employee_office['office_id'], 'nothi_part_no IN' => array_keys($all_NothiMaster_info),'nothi_masters_id IN'=>  array_values($all_NothiMaster_info)])->where([
                                'office_unit_organograms_id' => $value['office_unit_organogram_id']
                            ])->distinct(['nothi_part_no'])->toArray();
                        $need_to_add = array_diff_key(($all_NothiMaster_info), ($all_part_no));
                        if (!empty($need_to_add)) {
                            foreach ($need_to_add as $part_no => $nothi_masters_id) {
                                $optionsRadios = array();

                                $nothiPriviligeRecord = $nothiMasterPermissions->newEntity();

                                $optionsRadios['nothi_masters_id']           = $nothi_masters_id;
                                $optionsRadios['nothi_part_no']              = $part_no;
                                $optionsRadios['office_id']                  = $value['office_id'];
                                $optionsRadios['office_unit_id']             = $value['office_unit_id'];
                                $optionsRadios['office_unit_organograms_id'] = $value['office_unit_organogram_id'];
                                $optionsRadios['designation_level']          = $this->BngToEng($value['designation_level']);
                                $optionsRadios['privilige_type']             = intval(1);
                                $optionsRadios['nothi_office']               = $employee_office['office_id'];
                                $optionsRadios['created']                    = date("Y-m-d H:i:s");
                                $optionsRadios['created_by']                 = $this->Auth->user('id');


                                $nothiPriviligeRecord = $nothiMasterPermissions->patchEntity($nothiPriviligeRecord,
                                    $optionsRadios,
                                    [
                                    'validate' => 'add'
                                ]);

                                $nothiMasterPermissions->save($nothiPriviligeRecord);
                            }
                        }
                    }
                    echo 'success';
                }
            } catch (\Exception $ex) {
                echo 'failed. msg: '.$ex->getMessage();
            }
            die;
        }
    }
    public function nothiPermissionAddNew() {
		$this->view = 'nothi_permission_add';
		TableRegistry::remove('NothiMasterPermissions');
		$nothiMasterPermissionsTable = TableRegistry::get('NothiMasterPermissions');

    	$master_data = $this->request->data('master_data');
		$permitted_users = $this->request->data('permitted');
    	foreach($master_data as $master_id => $part_nos) {
    		if ($part_nos == 'all') {
    			$all_part_no = TableRegistry::get('NothiParts')->find('list', ['keyField' => 'id', 'valueField' => 'id'])->where(['nothi_masters_id' => $master_id])->toArray();
			} else {
    			$all_part_no = $part_nos;
			}
			foreach ($all_part_no as $part_no) {
				foreach ($permitted_users as $key => $permitted_user) {
					$nothiMasterPermissions = $nothiMasterPermissionsTable->find()->where([
						'nothi_office' => $permitted_user['office_id'],
						'office_unit_organograms_id' => $permitted_user['office_unit_organogram_id'],
						'nothi_part_no' => $part_no,
						'nothi_masters_id' => $master_id,

					])->count();

					if ($nothiMasterPermissions == 0) {
						$optionsRadios = array();
						$nothiPriviligeRecord = $nothiMasterPermissionsTable->newEntity();
						$optionsRadios['nothi_masters_id']           = $master_id;
						$optionsRadios['nothi_part_no']              = $part_no;
						$optionsRadios['office_id']                  = $permitted_user['office_id'];
						$optionsRadios['office_unit_id']             = $permitted_user['office_unit_id'];
						$optionsRadios['office_unit_organograms_id'] = $permitted_user['office_unit_organogram_id'];
						$optionsRadios['designation_level']          = $this->BngToEng($permitted_user['designation_level']);
						$optionsRadios['privilige_type']             = intval(1);
						$optionsRadios['nothi_office']               = $permitted_user['office_id'];
						$optionsRadios['created']                    = date("Y-m-d H:i:s");
						$optionsRadios['created_by']                 = $this->Auth->user('id');

						$nothiPriviligeRecord = $nothiMasterPermissionsTable->patchEntity($nothiPriviligeRecord, $optionsRadios, ['validate' => 'add']);
						$nothiMasterPermissionsTable->save($nothiPriviligeRecord);
					}
				}
			}
		}
		$this->response->type('application/json');
		$this->response->body(json_encode(['success']));
		return $this->response;
	}
    public function showMasterPermissionList($master_id = 0, $withNoteSubject = false){
         $response = ['status' => 'error' , 'msg' => 'Something Wrong'];
         $employee_office        = $this->getCurrentDakSection();
         TableRegistry::remove('NothiMasterPermissions');
        $nothiMasterPermissionTable = TableRegistry::get('NothiMasterPermissions');
        $EmployeeOffices = TableRegistry::get('EmployeeOffices');
        $OfficeUnits = TableRegistry::get('OfficeUnits');
        if($this->request->is('get')){
            $all_NothiMaster_info = $nothiMasterPermissionTable->find('list',
                ['keyField' => 'id', 'valueField' => 'nothi_part_no'])->where(['office_unit_organograms_id' => $employee_office['office_unit_organogram_id'], 'nothi_masters_id' => $master_id])->toArray();
            if ($withNoteSubject) {
                $data= TableRegistry::get('NothiParts')->find()->contain(['NothiNotes'])->select(['NothiParts.id', 'NothiParts.nothi_part_no_bn', 'subject'=> 'NothiNotes.subject'])->where(['NothiNotes.note_no' => 0, 'NothiParts.nothi_masters_id' => $master_id, 'NothiParts.id IN' => $all_NothiMaster_info])->toArray();
            } else {
                $data= TableRegistry::get('NothiParts')->find('list',['keyField' => 'id', 'valueField' => 'nothi_part_no_bn'])->where(['nothi_masters_id' => $master_id])->toArray();
            }
             $response = ['status' => 'success' , 'data' => $data];
        }else{
            $part = $this->request->data['part'];
            $master_id = isset($this->request->data['master'])?$this->request->data['master']:0;
             $allPermittedPeople = $nothiMasterPermissionTable->getMasterNothiListbyPartId($part,$employee_office['office_id'],$master_id);
             //pr($allPermittedPeople);die;
             if(!empty($allPermittedPeople)){
                foreach ($allPermittedPeople as $value){
                    if(!isset($value['office_unit_organograms_id'])){
                        continue;
                    }
                    $userInfo = $EmployeeOffices->getEmployeeOfficeRecordsByOfficeId($value['office_id'],$value['office_unit_id'],$value['office_unit_organograms_id'], 2);
                    $unit_name = $OfficeUnits->getBanglaName($value['office_unit_id'])['unit_name_bng'];
                    if (count($userInfo) > 0) {
                        $name = '';
                        $name .= $userInfo[0]['status'] == 1 ? (isset($userInfo[0]['name_bng']) ? $userInfo[0]['name_bng'] . ', ' : '') : '(শুন্যপদ), ';
                        $name .= (isset($userInfo[0]['designation']) ? $userInfo[0]['designation'] . ', ' : '');
                        $name .= (isset($unit_name)?$unit_name:'');
                        $data[] = $name;
                    }
                }
            }
           $response = ['status' => 'success' , 'data' => $data];
        }

            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
    }
    public function showMasterPermissionListUsingOrganogram($master_id = 0, $withNoteSubject = false, $organogram_id = false){
         $response = ['status' => 'error' , 'msg' => 'Something Wrong'];
         $employee_office        = $this->getCurrentDakSection();
         TableRegistry::remove('NothiMasterPermissions');
        $nothiMasterPermissionTable = TableRegistry::get('NothiMasterPermissions');
        $EmployeeOffices = TableRegistry::get('EmployeeOffices');
        $OfficeUnits = TableRegistry::get('OfficeUnits');
        if($this->request->is('get')){
        	if ($organogram_id) {
				$all_NothiMaster_info = $nothiMasterPermissionTable->find('list',
					['keyField' => 'id', 'valueField' => 'nothi_part_no'])->where(['office_unit_organograms_id' => $organogram_id, 'nothi_masters_id' => $master_id])->toArray();
			} else {
				$all_NothiMaster_info = $nothiMasterPermissionTable->find('list',
					['keyField' => 'id', 'valueField' => 'nothi_part_no'])->where(['office_unit_organograms_id' => $employee_office['office_unit_organogram_id'], 'nothi_masters_id' => $master_id])->toArray();
			}
            if ($withNoteSubject) {
                $data= TableRegistry::get('NothiParts')->find()->contain(['NothiNotes'])->select(['NothiParts.id', 'NothiParts.nothi_part_no_bn', 'subject'=> 'NothiNotes.subject'])->where(['NothiNotes.note_no' => 0, 'NothiParts.nothi_masters_id' => $master_id, 'NothiParts.id IN' => $all_NothiMaster_info])->toArray();
            } else {
                $data= TableRegistry::get('NothiParts')->find('list',['keyField' => 'id', 'valueField' => 'nothi_part_no_bn'])->where(['nothi_masters_id' => $master_id])->toArray();
            }
             $response = ['status' => 'success' , 'data' => $data];
        }else{
            $part = $this->request->data['part'];
            $master_id = isset($this->request->data['master'])?$this->request->data['master']:0;
             $allPermittedPeople = $nothiMasterPermissionTable->getMasterNothiListbyPartId($part,$employee_office['office_id'],$master_id);
             //pr($allPermittedPeople);die;
             if(!empty($allPermittedPeople)){
                foreach ($allPermittedPeople as $value){
                    if(!isset($value['office_unit_organograms_id'])){
                        continue;
                    }
                    $userInfo = $EmployeeOffices->getEmployeeOfficeRecordsByOfficeId($value['office_id'],$value['office_unit_id'],$value['office_unit_organograms_id'], 2);
                    $unit_name = $OfficeUnits->getBanglaName($value['office_unit_id'])['unit_name_bng'];
                    if (count($userInfo) > 0) {
                        $name = '';
                        $name .= $userInfo[0]['status'] == 1 ? (isset($userInfo[0]['name_bng']) ? $userInfo[0]['name_bng'] . ', ' : '') : '(শুন্যপদ), ';
                        $name .= (isset($userInfo[0]['designation']) ? $userInfo[0]['designation'] . ', ' : '');
                        $name .= (isset($unit_name)?$unit_name:'');
                        $data[] = $name;
                    }
                }
            }
           $response = ['status' => 'success' , 'data' => $data];
        }

            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
    }
    public function nothiPermissionDelete(){
        $employee_office = $this->getCurrentDakSection();
        $this->set(compact('employee_office'));
        if($this->request->is('post')){
            $this->layout = 'ajax';
            $office_id = $this->request->data['office_id'];
            $unit_id = $this->request->data['office_unit_id'];
            $designation_id = $this->request->data['office_unit_organogram_id'];
            TableRegistry::remove('NothiMasterPermissions');
            $nothiMasterPermissions = TableRegistry::get('NothiMasterPermissions');
             $employee_office        = $this->getCurrentDakSection();
             $all_NothiMaster_info = $nothiMasterPermissions->find('list',
                    ['keyField' => 'id', 'valueField' => 'nothi_masters_id'])->where(['office_id'=> $office_id,'nothi_office' => $employee_office['office_id'],'office_unit_id' =>$unit_id,'office_unit_organograms_id' => $designation_id])->toArray();
            $NothiMaster          = [];
            $myPermissionList          = [];
            if (!empty($all_NothiMaster_info)) {
                $NothiMastersTable = TableRegistry::get('NothiMasters');
                $NothiMaster       = $NothiMastersTable->find('list',
                        ['keyField' => 'id', 'valueField' => 'subject'])->where(['id IN' => array_values($all_NothiMaster_info)])->toArray();
                $myPermissionList = $nothiMasterPermissions->find('list',
                    ['keyField' => 'id', 'valueField' => 'nothi_masters_id'])->where(['office_id'=> $employee_office['office_id'],'nothi_office' => $employee_office['office_id'],'office_unit_id' =>$employee_office['office_unit_id'],'office_unit_organograms_id' => $employee_office['office_unit_organogram_id']])->toArray();
            }
            $this->set(compact(['NothiMaster','myPermissionList']));
            $this->render('nothi_list_for_delete');
            
        }
		if (isset($this->request->query['subject'])) {
			$this->layout = 'ajax';
			$office_id = $this->request->query['office_id'];
			$unit_id = $this->request->query['office_unit_id'];
			$designation_id = $this->request->query['office_unit_organogram_id'];

			TableRegistry::remove('NothiMasterPermissions');
			$nothiMasterPermissions = TableRegistry::get('NothiMasterPermissions');
			$employee_office        = $this->getCurrentDakSection();
			$all_NothiMaster_info = $nothiMasterPermissions->find('list',
				['keyField' => 'id', 'valueField' => 'nothi_masters_id'])->where(['office_id'=> $office_id,'nothi_office' => $employee_office['office_id'],'office_unit_id' =>$unit_id,'office_unit_organograms_id' => $designation_id])->toArray();

			$NothiMaster          = [];
			$myPermissionList          = [];
			if (!empty($all_NothiMaster_info)) {
				$NothiMastersTable = TableRegistry::get('NothiMasters');

				$NothiMaster = $NothiMastersTable->find('list',
					['keyField' => 'id', 'valueField' => 'subject'])->where(['id IN' => array_values($all_NothiMaster_info)])->where(['subject like' => '%' . $this->request->query['subject'] . '%'])->toArray();
				$myPermissionList = $nothiMasterPermissions->find('list',
					['keyField' => 'id', 'valueField' => 'nothi_masters_id'])->where(['office_id' => $employee_office['office_id'], 'nothi_office' => $employee_office['office_id'], 'office_unit_id' => $employee_office['office_unit_id'], 'office_unit_organograms_id' => $employee_office['office_unit_organogram_id']])->toArray();
			}
			$this->set(compact(['NothiMaster','myPermissionList']));
			$this->render('nothi_list_for_delete');
		}
    }
    public function nothiPermissionUpdate()
    {
        if ($this->request->is('post')) {
            try {
                $office_id                = $this->request->data['office_id'];
                $unit_id                  = $this->request->data['office_unit_id'];
                $designation_id           = $this->request->data['office_unit_organogram_id'];
                $part_no                  = $this->request->data['masters'];
                $employee_office          = $this->getCurrentDakSection();
                TableRegistry::remove('NothiMasterPermissions');
                $nothiMasterPermissions   = TableRegistry::get('NothiMasterPermissions');
                TableRegistry::remove('NothiMasterCurrentUsers');
                $NothiMastersCurrentUsers = TableRegistry::get('NothiMasterCurrentUsers');
                $all_NothiMaster_info     = $nothiMasterPermissions->find('list',
                        ['keyField' => 'nothi_part_no', 'valueField' => 'nothi_masters_id'])->where(['office_unit_organograms_id' => $designation_id,
                        'nothi_office' => $employee_office['office_id'], 'office_id' => $office_id,
                        'office_unit_id' => $unit_id,
                        'nothi_part_no IN' => array_values($part_no)])->toArray();
                if (!empty($all_NothiMaster_info)) {
                    foreach ($all_NothiMaster_info as $part => $master) {
                        $is_current_user = $NothiMastersCurrentUsers->find()->where(['nothi_master_id' => $master,
                                'nothi_part_no' => $part, 'office_id' => $office_id, 'office_unit_id' => $unit_id,
                                'office_unit_organogram_id' => $designation_id, 'nothi_office' => $employee_office['office_id']])->count();
                        if ($is_current_user == 0) {
                            $nothiMasterPermissions->deleteAll(['nothi_part_no' => $part, 'nothi_masters_id' => $master,
                                'office_unit_organograms_id' => $designation_id,
                                'nothi_office' => $employee_office['office_id'], 'office_id' => $office_id,
                                'office_unit_id' => $unit_id]);
                        }
                    }
                }
                echo 'success';
            } catch (\Exception $ex) {
                echo 'failed. '.$ex->getMessage();
            }
            die;
        }
    }
}

