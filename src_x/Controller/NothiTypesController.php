<?php

namespace App\Controller;

use App\Controller\ProjapotiController;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\TableRegistry;
use Cake\I18n\Number;
use Cake\I18n\Time;
use Psr\Log\InvalidArgumentException;

class NothiTypesController extends ProjapotiController
{
    /*
     * Nothi Types
     */

    public function nothiTypesAdd()
    {
        $nothiTypes_table = TableRegistry::get("NothiTypes");
        $nothiTypes_records = $nothiTypes_table->newEntity();

        $session = $this->request->session();

        $employee_office = $this->getCurrentDakSection();

        if ($this->request->is('post')) {

            if (!empty($this->request->data)) {
                if (!empty($this->request->data['type_code'])) {

                $this->request->data['type_code'] = str_replace('*', '', $this->request->data['type_code']);
                $this->request->data['type_code'] = (strlen($this->request->data['type_code']) < 2 ? str_pad($this->request->data['type_code'], 2, 0, STR_PAD_LEFT) : $this->request->data['type_code']);

                $this->request->data['type_code'] = enTobn($this->request->data['type_code']);
                $nothiTypes_records = $nothiTypes_table->patchEntity($nothiTypes_records, $this->request->data, ['validate' => 'add']);
                $nothiTypes_records->created = date("Y-m-d");
                $errors = $nothiTypes_records->errors();
                if (!$errors) {
                    try {
                        if (strlen($nothiTypes_records->type_code) == 1) {
                            throw new \Exception('দুঃখিত! কোড ২ ডিজিট হতে হবে ');
                        }
                        $nothiTypes_records->created_by = $this->Auth->user('id');
                        $nothiTypes_records->modified_by = $this->Auth->user('id');
                        $nothiTypes_table->save($nothiTypes_records);
                        $this->Flash->success('ধরন সংরক্ষিত হয়েছে।');
                        $this->redirect(['action' => 'nothiTypes']);
                    } catch (InvalidArgumentException $e) {
                        $this->Flash->error('ধরন সংরক্ষিত করা সম্ভব হচ্ছে না।');
                    } catch (\Exception $ex) {
                        $this->Flash->error($ex->getMessage());
                    }
                } else {
                    $this->Flash->error('ধরন সংরক্ষিত করা সম্ভব হচ্ছে না।');
                }
            } else {
                    $this->Flash->error('ধরন সংরক্ষিত করা সম্ভব হচ্ছে না।');
                }
            }
        }

        $this->set('office_id', $employee_office['office_id']);
        $this->set('office_unit_id', $employee_office['office_unit_id']);
        $this->set(compact('nothiTypes_records'));
    }

    public function nothiTypesEdit($id)
    {
        $nothiTypes_table = TableRegistry::get("NothiTypes");

        if (empty($id)) {
            $this->Flash->error('অনুরোধ গ্রহণযোগ্য নয়।');
            $this->redirect(['action' => 'nothiTypes']);
        }

        $session = $this->request->session();


        $employee_office = $this->getCurrentDakSection();

        $nothiTypes_records = $nothiTypes_table->get($id);
        $type_code = $nothiTypes_records['type_code'];
        $nothiTypes_records['type_code'] = bnToen($nothiTypes_records['type_code']);
        $nothi_master_table = TableRegistry::get('NothiMasters');
        $count = $nothi_master_table->find()->where(['nothi_types_id' => $id])->count();

        if ($nothiTypes_records['office_id'] != $employee_office['office_id'] || $nothiTypes_records['office_unit_id']
            != $employee_office['office_unit_id']
        ) {
            $this->Flash->error('অনুরোধ গ্রহণযোগ্য নয়।');
            $this->redirect(['action' => 'nothiTypes']);
        }
        if ($this->request->is('post')) {
            if (!empty($this->request->data)) {
                if (!empty($this->request->data['type_code'])) {

                $this->request->data['type_code'] = str_replace('*', '', $this->request->data['type_code']);
                $this->request->data['type_code'] = (strlen($this->request->data['type_code']) < 2 ? str_pad($this->request->data['type_code'], 2, 0, STR_PAD_LEFT) : $this->request->data['type_code']);

                $this->request->data['type_code'] = enTobn($this->request->data['type_code']);
                $nothiTypes_records = $nothiTypes_table->patchEntity($nothiTypes_records,
                    $this->request->data, ['validate' => 'edit']);
                $nothiTypes_records->created = date("Y-m-d");
                $errors = $nothiTypes_records->errors();

                if (!$errors) {
                    try {
                        if (intval($count) > 0) {
                            $nothiTypes_records->type_code = $type_code;
                        }
                        if (strlen($nothiTypes_records->type_code) == 1) {
                            throw new \Exception('দুঃখিত! কোড ২ ডিজিট হতে হবে ');
                        }
                        $nothiTypes_records->type_code = $this->EngToBng($nothiTypes_records->type_code);
                        $nothiTypes_table->save($nothiTypes_records);
                        $this->Flash->success('ধরন সংরক্ষিত হয়েছে।');
                        $this->redirect(['action' => 'nothiTypes']);
                    } catch (InvalidArgumentException $e) {
                        $this->Flash->error('ধরন সংরক্ষিত করা সম্ভব হচ্ছে না।');
                    } catch (\Exception $ex) {
                        $this->Flash->error($ex->getMessage());
                    }
                } else {
                    $this->Flash->error('ধরন সংরক্ষিত করা সম্ভব হচ্ছে না।');
                }
            } else {
                    $this->Flash->error('ধরন সংরক্ষিত করা সম্ভব হচ্ছে না।');
                }
            }
        }

        $this->set('office_id', $employee_office['office_id']);
        $this->set('office_unit_id', $employee_office['office_unit_id']);
        $this->set('nothiTypes_records', $nothiTypes_records);
        $this->set('count', $count);
    }

    public function nothiTypesDelete($id)
    {
        $nothiTypes_table = TableRegistry::get("NothiTypes");
        $nothiMasters_table = TableRegistry::get("NothiMasters");
        $employee_office = $this->getCurrentDakSection();
        if (empty($id)) {
            $this->Flash->error('অনুরোধ গ্রহণযোগ্য নয়।');
            $this->redirect(['action' => 'nothiTypes']);
        }
        $nothiTypes_records = $nothiTypes_table->get($id);

        if (empty($nothiTypes_records)) {
            $this->Flash->error('অনুরোধ গ্রহণযোগ্য নয়।');
            $this->redirect(['action' => 'nothiTypes']);
        }

        if ($nothiMasters_table->hasThisType($id)) {
            $this->Flash->error('অনুরোধ গ্রহণযোগ্য নয়।');
            $this->redirect(['action' => 'nothiTypes']);
        } else {
            if ($nothiTypes_records['office_id'] != $employee_office['office_id'] || $nothiTypes_records['office_unit_id']
                != $employee_office['office_unit_id']
            ) {
                $this->Flash->error('অনুরোধ গ্রহণযোগ্য নয়।');
                $this->redirect(['action' => 'nothiTypes']);
            }
            try {
                $nothiTypes_table->delete($nothiTypes_records);
                $this->Flash->success('ধরনটি মুছে ফেলা হয়েছে।');
            } catch (InvalidArgumentException $e) {
                $this->Flash->error('অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না।');
            }
        }

        $this->redirect(['action' => 'nothiTypes']);
    }

    public function nothiTypes()
    {
		if (isset($_COOKIE['NothiTypeList_PageLimit'])) {
			$len = $_COOKIE['NothiTypeList_PageLimit'];
		} else {
			$len = 50;
			setcookie('NothiTypeList_PageLimit', $len, time() + (86400 * 30), '/');
		}
		$this->set(compact('len'));
    }

    public function nothiTypesList()
    {
        $this->layout = null;
        $nothiTypes_table = TableRegistry::get("NothiTypes");
        if (isset($_COOKIE['NothiTypeList_PageLimit'])) {
            $len = $_COOKIE['NothiTypeList_PageLimit'];
        } else {
            $len = 50;
            setcookie('NothiTypeList_PageLimit', $len, time() + (86400 * 30), '/');
        }
        if (isset($this->request->data['length'])) {
            $len = $this->request->data['length'];
            setcookie('NothiTypeList_PageLimit', $len, time() + (86400 * 30), '/');
        }

        $start = isset($this->request->data['start']) ? intval($this->request->data['start']) : 0;
        //$len = isset($this->request->data['length']) ? intval($this->request->data['length']) : 500;
        $page = ($start / $len) + 1;

        $session = $this->request->session();

        $employee_office = $this->getCurrentDakSection();

        if ($this->request->is('ajax')) {
            $totalRec = $nothiTypes_table->getAllTypes(0, 100, $employee_office)->toArray();
            $nothityperecord = $nothiTypes_table->getAllTypes($page, $len, $employee_office)->toArray();

            $data = [];
            if (!empty($nothityperecord)) {
                $i = 0;
                foreach ($nothityperecord as $key => $record) {
                    $i++;
                    $si = (($page - 1) * $len) + $i;
                    if (is_numeric($record['type_code'])) {
                        $record['type_code'] = Number::format($record['type_code']);
                    }
                    $data[] = array(
                        "<div class='text-center'>" . Number::format($si) . "</div>",
                        h($record['type_name']),
                        "<div class='text-center'>" . __($record['type_code']) . "</div>",
                        //"<div class='text-center'>" . Number::format($record['type_last_number']) . "</div>",
                        "<div class='text-center'>" . Number::format($record['totalTypes']) . "</div>",
//                        __("{$record['created']}"),
                        "<div class='text-center btn-group btn-group-round'>" . '<a href="nothiTypesEdit/' . $record['id'] . '" class="btn btn-primary btn-sm" title="সম্পাদন করুন"  ><span class="glyphicon glyphicon-pencil"></span></a>' . (($record['totalTypes'] == 0) ?'&nbsp;<button link="nothiTypesDelete/' . $record['id'] . '" class="btn btn-primary btn-sm red" title="মুছে ফেলুন" onclick =deleteType("'.$record['id'].'")  ><span class="glyphicon glyphicon-remove"></span></button>': '' ).
                        "</div>"
                    );
                }
            }

            $json_data = array(
                "draw" => intval($this->request->data['draw']),
                "recordsTotal" => count($totalRec),
                "recordsFiltered" => count($totalRec),
                "data" => $data,
                "len" => $_COOKIE['NothiTypeList_PageLimit']
            );
            $this->response->body(json_encode($json_data));
            $this->response->type('application/json');
            return $this->response;
        } else {
            $this->response->body(json_encode(0));
            $this->response->type('application/json');
            return $this->response;
        }
    }

    public function superAdminOption()
    {
        $user = $this->Auth->user();
        if (empty($user['user_role_id']) || $user['user_role_id'] > 2) {
            $this->redirect(['controller' => 'users', 'action' => 'login']);
        }
    }

    public function officeNothiTypes($office_id = 0, $unit_id = 0)
    {
        $user = $this->Auth->user();
        if (empty($user['user_role_id']) || $user['user_role_id'] > 2) {
            $this->redirect(['controller' => 'users', 'action' => 'login']);
        }
        if (empty($office_id)) {
            $this->Flash->error('অফিস বাছাই করা হয় নি');
            $this->redirect(['controller' => 'NothiTypes', 'action' => 'superAdminOption']);
        }
        if (empty($unit_id)) {
            $this->Flash->error('শাখা বাছাই করা হয় নি');
            $this->redirect(['controller' => 'NothiTypes', 'action' => 'superAdminOption']);
        }
        $this->layout = null;

        $start = isset($this->request->data['start']) ? intval($this->request->data['start'])
            : 0;
        $len = isset($this->request->data['length']) ? intval($this->request->data['length'])
            : 500;
        $page = ($start / $len) + 1;
        $employee_office['office_id'] = $office_id;
        $employee_office['office_unit_id'] = $unit_id;

        if ($this->request->is('ajax')) {
            try {
                $this->switchOffice($employee_office['office_id'], 'OfficeDB');
                $nothiTypes_table = TableRegistry::get("NothiTypes");
                $totalRec = $nothiTypes_table->getAllTypes(0, 100, $employee_office)->toArray();
                $nothityperecord = $nothiTypes_table->getAllTypes($page, $len, $employee_office)->toArray();
                $i = 0;
                $data = [];
                foreach ($nothityperecord as $key => $record) {
                    $i++;
                    $si = (($page - 1) * $len) + $i;
                    if (is_numeric($record['type_code'])) {
                        $record['type_code'] = Number::format($record['type_code']);
                    }
                    $data[] = array(
                        "<div class='text-center'>" . Number::format($si) . "</div>",
                        $record['type_name'],
                        "<div class='text-center'>" . $record['type_code'] . "</div>",
                        "<div class='text-center'>" . Number::format($record['type_last_number']) . "</div>",
                        "<div class='text-center'>" . Number::format($record['totalTypes']) . "</div>",
//                        __("{$record['created']}"),
                        "<div class='text-center'>" . '<a href="superAdminNothiTypesEdit/' . $record['id'] . '/' . $office_id . '/' . $unit_id . '" class="btn btn-primary btn-sm"  ><span class="glyphicon glyphicon-pencil"></span></a>' .
                        "</div>"
                    );
                }
                $json_data = array(
                    "draw" => intval($this->request->data['draw']),
                    "recordsTotal" => count($totalRec),
                    "recordsFiltered" => count($totalRec),
                    "data" => $data
                );
                $this->response->body(json_encode($json_data));
                $this->response->type('application/json');
                return $this->response;
            } catch (\Exception $ex) {
                $data = [];
                $this->response->body(json_encode($data));
                $this->response->type('application/json');
                return $this->response;
            }
        } else {
            $this->response->body(json_encode(0));
            $this->response->type('application/json');
            return $this->response;
        }
    }

    /*
     * End Nothi Types
     */

    public function superAdminNothiTypesEdit($id, $office_id, $unit_id)
    {

        if (empty($id) || empty($office_id) || empty($unit_id)) {
            $this->Flash->error('অনুরোধ গ্রহণযোগ্য নয়।');
            $this->redirect(['action' => 'superAdminOption']);
        }

        $user = $this->Auth->user();
        if (empty($user['user_role_id']) || $user['user_role_id'] > 2) {
            $this->Flash->error('অনুরোধ গ্রহণযোগ্য নয়।');
            $this->redirect(['controller' => 'users', 'action' => 'login']);
        }

        try {
            $this->switchOffice($office_id, 'OfficeDB');
            $nothiTypes_table = TableRegistry::get("NothiTypes");
            $nothiTypes_records = $nothiTypes_table->get($id);
            $type_code = $nothiTypes_records['type_code'];
            $potrojari_table = TableRegistry::get('Potrojari');
            $count = $potrojari_table->getPotrojaribyNothiType($id)->count();

            if ($this->request->is('post')) {
                if (!empty($this->request->data)) {
                    $nothiTypes_records = $nothiTypes_table->patchEntity($nothiTypes_records,
                        $this->request->data, ['validate' => 'edit']);
                    $nothiTypes_records->created = date("Y-m-d");
                    $errors = $nothiTypes_records->errors();

                    if (!$errors) {
                        $new_type_code = $type_code;
                        try {
                            if (intval($count) > 0) {
                                $nothiTypes_records->type_code = $type_code;
                            } else {
                                $new_type_code = $nothiTypes_records->type_code;
                            }
                            $nothiTypes_records->type_last_number = $this->BngToEng($this->request->data['type_last_number']);
                            if ($nothiTypes_table->save($nothiTypes_records)) {
                                $nothi_master_table = TableRegistry::get('NothiMasters');
                                $nothi_nos = $nothi_master_table->find()->select(['nothi_no'])->where(['nothi_types_id' => $id])->toArray();
                                if (!empty($nothi_nos)) {
                                    foreach ($nothi_nos as $nothi_no) {
//                                    $no = substr($nothi_no,14,2);
                                        $new_nothi_no = mb_substr($nothi_no['nothi_no'], 0, 15) . $new_type_code . mb_substr($nothi_no['nothi_no'],
                                                17);
                                        $nothi_master_table->updateAll(['nothi_no' => $new_nothi_no],
                                            ['nothi_no' => $nothi_no['nothi_no'], 'office_id' => $office_id,
                                                'office_units_id' => $unit_id]);
                                    }
                                }
                                $nothi_parts_table = TableRegistry::get('NothiParts');
                                $nothi_nos = $nothi_parts_table->find()->select(['nothi_no'])->where(['nothi_types_id' => $id])->toArray();
                                if (!empty($nothi_nos)) {
                                    foreach ($nothi_nos as $nothi_no) {
//                                    $no = substr($nothi_no,14,2);
                                        $new_nothi_no = mb_substr($nothi_no['nothi_no'], 0, 15) . $new_type_code . mb_substr($nothi_no['nothi_no'],
                                                17);
                                        $nothi_parts_table->updateAll(['nothi_no' => $new_nothi_no],
                                            ['nothi_no' => $nothi_no['nothi_no'], 'office_id' => $office_id,
                                                'office_units_id' => $unit_id]);
                                    }
                                }
                                $this->Flash->success('ধরন সংরক্ষিত হয়েছে।');
                                $this->redirect(['action' => 'superAdminOption']);
                            }
                        } catch (\InvalidArgumentException $e) {
                            $this->Flash->error('ধরন সংরক্ষিত করা সম্ভব হচ্ছে না।');
                        }
                    } else {
                        $this->Flash->error('ধরন সংরক্ষিত করা সম্ভব হচ্ছে না।');
                    }
                }
            }
            $this->set('office_id', $office_id);
            $this->set('office_unit_id', $unit_id);
            $this->set('nothiTypes_records', $nothiTypes_records);
            $this->set('count', $count);
            $this->set('for', 'admin');
            $this->view = 'nothi_types_edit';
        } catch (Exception $ex) {

        }
    }
    public function getNothiTypeByUnit($unit_id){
        $tbl_nothi_types = TableRegistry::get('NothiTypes');
        if($unit_id = 'all'){
            $nothi_types = $tbl_nothi_types->find('list')->toArray();
        } else {
            $nothi_types = $tbl_nothi_types->find('list')->where(['office_unit_id'=>(int)$unit_id])->toArray();
        }

        $this->response->type('application/json');
        $this->response->body(json_encode($nothi_types));
        return $this->response;

    }

    public function SuperAdminGetNothiTypeByUnit($unit_id,$office_id){
        $role_id= $this->Auth->user('user_role_id');
        if($role_id==1 || $role_id==2) {
            $this->switchOffice($office_id, 'OfficeDB');

            $tbl_nothi_types = TableRegistry::get('NothiTypes');
            $nothi_types = $tbl_nothi_types->find('list')->where(['office_unit_id' => $unit_id])->toArray();

            $this->response->type('application/json');
            $this->response->body(json_encode($nothi_types));
            return $this->response;
        }

    }

    public function getNothiDataChangeHistory($id,$is_admin=0,$office_id=0){
        if($is_admin==1 && !empty($office_id)){
            $role_id= $this->Auth->user('user_role_id');
            if($role_id==1 || $role_id==2){
                $this->switchOffice($office_id,'OfficeDB');
            } else {
                $this->response->type('application/json');
                $this->response->body(json_encode(['status'=>'error','h_data'=>'Error Occured']));
                return $this->response;
            }
        }
        $id = base64_decode($id);
        $nothi_data_change_history_table = TableRegistry::get('NothiDataChangeHistory');
        $nothi_data_change_history = $nothi_data_change_history_table->find()->where(['nothi_master_id'=>$id])->order(['id desc'])->toArray();
        if(!empty($nothi_data_change_history)){
            $data=[];
            foreach ($nothi_data_change_history as $item){
                $time = new Time($item['created']);
                $data[]= ['time' => $time->i18nFormat(null, null, 'bn-BD')]+unserialize($item['nothi_data']);
            }
        $this->response->type('application/json');
        $this->response->body(json_encode(['status'=>'success','h_data'=>$data]));
        return $this->response;}
        else {
            $this->response->type('application/json');
            $this->response->body(json_encode(['status'=>'error','h_data'=>'No data found']));
            return $this->response;
        }
    }

    public function nothiNameUpdates()
    {
        $selected_office_section = $this->getCurrentDakSection();
        $OfficeUnitsTable = TableRegistry::get('OfficeUnits');
        if (empty($selected_office_section)) {
            return $this->redirect('/');
        } else {
            $ownChildInfo = $OfficeUnitsTable->getOfficeUnitsList($selected_office_section['office_id']);
        }
        $this->set(compact('selected_office_section'));
        $tbl_nothi_master = TableRegistry::get('NothiMasters');
        $tbl_nothi_parts = TableRegistry::get('NothiParts');
        $tbl_nothi_types = TableRegistry::get('NothiTypes');
        $this->set(compact('ownChildInfo'));

        if(!empty($this->request->query('unit_id'))){
            if($this->request->query('unit_id') == 'all'){

                $nothi_types = $tbl_nothi_types->find('list')->where(['office_id'=>$selected_office_section['office_id']])->toArray();
                $data = $tbl_nothi_master->find()->select(['id', 'office_units_id', 'nothi_no', 'subject','nothi_class','nothi_types_id', 'nothi_created_date' => 'DATE(nothi_created_date)'])->where(['office_id'=>$selected_office_section['office_id'], '(is_archived is null or is_archived = 0)']);
            } else {
                $unit_id = !empty($this->request->query('unit_id'))?(int)$this->request->query('unit_id'):'';

                $nothi_types = $tbl_nothi_types->find('list')->where(['office_unit_id'=>$unit_id,'office_id'=>$selected_office_section['office_id']])->toArray();
                $data = $tbl_nothi_master->find()->select(['id', 'office_units_id', 'nothi_no', 'subject','nothi_class','nothi_types_id', 'nothi_created_date' => 'DATE(nothi_created_date)'])->where(['office_units_id' => $unit_id,'office_id'=>$selected_office_section['office_id'], '(is_archived is null or is_archived = 0)']);
            }

            if (!empty($this->request->query('nothi_subject'))) {
                $data = $data->where(['subject like' => '%'.h($this->request->query('nothi_subject')).'%']);
            }
            if (!empty($this->request->query('nothi_no'))) {
                $data = $data->where(['nothi_no' => h($this->request->query('nothi_no'))]);
            }
            $data = $data->toArray();

            $i=0;
            $datas=[];
            foreach ($data as $data) {
                if (empty($nothi_types[$data['nothi_types_id']])) {
                    continue;
                }
                if (!empty($ownChildInfo[$data['office_units_id']])){
                    $data['office_units_name'] = $ownChildInfo[$data['office_units_id']];
            	}
                $is_nothi_expire = getNothiArchiveStatus($data->id, $selected_office_section['office_id']);
                $datas[$i]= $data;
                $datas[$i]['nothi_types_name']= $nothi_types[$data['nothi_types_id']];
                $datas[$i]['expire_level']= $is_nothi_expire['level'];
                $i++;
            }
            $this->response->type('application/json');
            $this->response->body(json_encode($datas));
            return $this->response;
        }

        if ($this->request->is('post')) {
            $id = base64_decode($this->request->data['id']);
            $nothi_data = $tbl_nothi_master->get($id);
            if (!empty($nothi_data)) {
            $response = ['status' => 'error', 'msg' => 'সকল তথ্য দেওয়া হয় নি '];
            if (empty($this->request->data['id']) || empty($this->request->data['class_id']) || empty($this->request->data['type_id']) || empty($this->request->data['unit_it'])) {

            } else {
                if (!empty($this->request->data['nothi_no']) && (mb_strlen($this->request->data['nothi_no'])!=5)) {
                    if (mb_strlen($this->request->data['nothi_no']) != 24) {
                        $this->response->type('application/json');
                        $this->response->body(json_encode(['status' => 'error', 'msg' => 'দুঃখিত! নথি নম্বর সঠিক নয়। নথি নম্বর ১৮ ডিজিটের হতে হবে।']));
                        return $this->response;
                    }
                    $unique_nothi_check = $tbl_nothi_master->find()->where(['nothi_no' => enTobn($this->request->data['nothi_no'])])->first();
                    if (!empty($unique_nothi_check)) {
                        $this->response->type('application/json');
                        $this->response->body(json_encode(['status' => 'error', 'msg' => 'নথি নম্বরটি পূর্বে ব্যবহৃত হয়েছে।']));
                        return $this->response;
                    }
                }

                $nothi_data_change_history_table = TableRegistry::get('NothiDataChangeHistory');

				$is_nothi_expire = getNothiArchiveStatus($nothi_data->id, $nothi_data->office_id);
//				if ($is_nothi_expire['level'] == 2) {
//					$class_id = $nothi_data['nothi_class'];
//				} else {
					$class_id = $this->request->data['class_id'];
//				}

                try {
                    $subject = $this->request->data['name'];

                    $types_id = $this->request->data['type_id'];
                    $office_units_id = $this->request->data['unit_it'];
                    $prev_type_name = $this->request->data['prev_type_name'];
                    $prev_unit_name = $this->request->data['prev_unit_name'];

                    $nothi_history = $nothi_data_change_history_table->newEntity();
                    $nothi_history->nothi_master_id = $nothi_data->id;
                    $nothi_history->nothi_data= serialize(['office_units_id'=>$nothi_data->office_units_id,'nothi_types_id'=>$nothi_data->nothi_types_id,'nothi_no'=>$nothi_data->nothi_no,'subject'=>$nothi_data->subject,'nothi_class'=>$nothi_data->nothi_class,'type_name'=>$prev_type_name,'unit_name'=>$prev_unit_name]);
                    $nothi_history->employee_id = $selected_office_section['officer_id'];
                    $nothi_history->office_unit_organogram_id = $selected_office_section['office_unit_organogram_id'];
                    $nothi_history->created_by = $this->Auth->user('id');
                    $nothi_history->modified_by = $this->Auth->user('id');
                    $nothi_data_change_history_table->save($nothi_history);

                    if (!empty($this->request->data['nothi_no']) && (mb_strlen($this->request->data['nothi_no'])!=5)) {

                        $nothi_no = enTobn($this->request->data['nothi_no']);

                        $tbl_nothi_master->updateAll(['subject' => $subject, 'nothi_types_id' => $types_id, 'nothi_class' => $class_id, 'office_units_id' => $office_units_id, 'nothi_no' => $nothi_no, 'modified_by' => $this->Auth->user()['id']], ['id' => $id]);

                        $tbl_nothi_parts->updateAll(['subject' => $subject, 'nothi_types_id' => $types_id, 'nothi_class' => $class_id, 'office_units_id' => $office_units_id, 'nothi_no' => $nothi_no], ['nothi_masters_id' => $id]);

                    } else {

                        $tbl_nothi_master->updateAll(['subject' => $subject, 'nothi_types_id' => $types_id, 'nothi_class' => $class_id, 'office_units_id' => $office_units_id, 'modified_by' => $this->Auth->user()['id']], ['id' => $id]);

                        $tbl_nothi_parts->updateAll(['subject' => $subject, 'nothi_types_id' => $types_id, 'nothi_class' => $class_id, 'office_units_id' => $office_units_id], ['nothi_masters_id' => $id]);
                    }

                    $response = ['status' => 'success', 'msg' => 'তথ্য পরিবর্তন করা হয়েছে।'];
                } catch (\Exception $ex) {
                    $response['msg'] = 'টেকনিক্যাল ত্রুটি হয়েছে।';
                    $response['reason'] = $this->makeEncryptedData($ex->getMessage());
                }
            }
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        } else {
                $response = ['status' => 'error', 'msg' => 'দুঃখিত! নথি খুজে পাওয়া যায় নি।'];
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
        }
    }
    public function SuperAdminNothiDetailUpdates()
    {
        if(!empty($this->request->query('unit_id')) && !empty($this->request->query('office_id'))){

            $this->switchOffice($this->request->query('office_id'),'OfficeDB');

            $tbl_nothi_types = TableRegistry::get('NothiTypes');
            $tbl_nothi_master = TableRegistry::get('NothiMasters');
            $unit_id = !empty($this->request->query('unit_id'))?$this->request->query('unit_id'):'';

            $nothi_types = $tbl_nothi_types->find('list')->where(['office_unit_id'=>$unit_id])->toArray();
            $query_data = $tbl_nothi_master->find()->select(['id', 'nothi_no', 'subject','nothi_class','nothi_types_id', 'nothi_created_date' => 'DATE(nothi_created_date)'])->where(['office_units_id' => $unit_id])->toArray();
            $i=0;
            $datas=[];
            foreach ($query_data as $data) {
                if(empty($nothi_types[$data['nothi_types_id']])){
                    continue;
                }
                $datas[$i] = $data;
                $datas[$i]['nothi_types_name']= $nothi_types[$data['nothi_types_id']];
                $i++;
            }
            $result['nothies']=$datas;
            $result['types']=$nothi_types;
            $this->response->type('application/json');
            $this->response->body(json_encode($result));
            return $this->response;

        }

        if ($this->request->is('post')) {

            $this->switchOffice($this->request->data['office_id'],'OfficeDB');
            $tbl_nothi_master = TableRegistry::get('NothiMasters');
            $tbl_nothi_parts = TableRegistry::get('NothiParts');
            $id = base64_decode($this->request->data['id']);
            $nothi_data = $tbl_nothi_master->get($id);
            if (!empty($nothi_data)) {
                $response = ['status' => 'error', 'msg' => 'সকল তথ্য দেওয়া হয় নি '];
                if (empty($this->request->data['id']) || empty($this->request->data['class_id']) || empty($this->request->data['type_id']) || empty($this->request->data['unit_it'])) {

                } else {
                    if (!empty($this->request->data['nothi_no'])) {
                        if (mb_strlen($this->request->data['nothi_no']) != 24) {
                            $this->response->type('application/json');
                            $this->response->body(json_encode(['status' => 'error', 'msg' => 'দুঃখিত! নথি নম্বর সঠিক নয়। নথি নম্বর ১৮ ডিজিটের হতে হবে।']));
                            return $this->response;
                        }
                        $unique_nothi_check = $tbl_nothi_master->find()->where(['nothi_no' => enTobn($this->request->data['nothi_no'])])->first();
                        if (!empty($unique_nothi_check)) {
                            $this->response->type('application/json');
                            $this->response->body(json_encode(['status' => 'error', 'msg' => 'নথি নম্বরটি পূর্বে ব্যবহৃত হয়েছে।']));
                            return $this->response;
                        }
                    }

                    $nothi_data_change_history_table = TableRegistry::get('NothiDataChangeHistory');

                    try {
                        $subject = $this->request->data['name'];
                        $is_nothi_expire = getNothiArchiveStatus($nothi_data->id, $nothi_data->office_id);
                        if ($is_nothi_expire['level'] == 2) {
                            $class_id = $nothi_data['nothi_class'];
                        } else {
                            $class_id = $this->request->data['class_id'];
                        }
                        $types_id = $this->request->data['type_id'];
                        $office_units_id = $this->request->data['unit_it'];
                        $prev_type_name = $this->request->data['prev_type_name'];
                        $prev_unit_name = $this->request->data['prev_unit_name'];

                        $nothi_history = $nothi_data_change_history_table->newEntity();
                        $nothi_history->nothi_master_id = $nothi_data->id;
                        $nothi_history->nothi_data= serialize(['office_units_id'=>$nothi_data->office_units_id,'nothi_types_id'=>$nothi_data->nothi_types_id,'nothi_no'=>$nothi_data->nothi_no,'subject'=>$nothi_data->subject,'nothi_class'=>$nothi_data->nothi_class,'type_name'=>$prev_type_name,'unit_name'=>$prev_unit_name]);
                        $nothi_history->employee_id = 0;
                        $nothi_history->office_unit_organogram_id = 0;
                        $nothi_history->created_by = $this->Auth->user('id');
                        $nothi_history->modified_by = $this->Auth->user('id');
                        $nothi_data_change_history_table->save($nothi_history);



                        if (!empty($this->request->data['nothi_no'])) {

                            $nothi_no = enTobn($this->request->data['nothi_no']);

                            $tbl_nothi_master->updateAll(['subject' => $subject, 'nothi_types_id' => $types_id, 'nothi_class' => $class_id, 'office_units_id' => $office_units_id, 'nothi_no' => $nothi_no, 'modified_by' => $this->Auth->user()['employee_record_id']], ['id' => $id]);

                            $tbl_nothi_parts->updateAll(['subject' => $subject, 'nothi_types_id' => $types_id, 'nothi_class' => $class_id, 'office_units_id' => $office_units_id, 'nothi_no' => $nothi_no], ['nothi_masters_id' => $id]);

                        } else {

                            $tbl_nothi_master->updateAll(['subject' => $subject, 'nothi_types_id' => $types_id, 'nothi_class' => $class_id, 'office_units_id' => $office_units_id, 'modified_by' => $this->Auth->user()['employee_record_id']], ['id' => $id]);

                            $tbl_nothi_parts->updateAll(['subject' => $subject, 'nothi_types_id' => $types_id, 'nothi_class' => $class_id, 'office_units_id' => $office_units_id], ['nothi_masters_id' => $id]);
                        }

                        $response = ['status' => 'success', 'msg' => 'তথ্য পরিবর্তন করা হয়েছে।'];
                    } catch (\Exception $ex) {
                        $response['msg'] = 'টেকনিক্যাল ত্রুটি হয়েছে';
                        $response['reason'] = makeEncryptedData($ex->getMessage());
                    }
                }
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            } else {
                $response = ['status' => 'error', 'msg' => 'দুঃখিত! নথি খুজে পাওয়া যায় নি।'];
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
        }
    }
}