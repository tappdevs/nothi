<?php

namespace App\Controller;

use App\Controller\ProjapotiController;
use App\Model\Table;
use App\Model\Table\NotificationSettingsTable;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

class NotificationEmployeeSettingsController extends ProjapotiController {

    public $paginate = [
        'fields' => ['NotificationSettings.id'],
        'limit' => 25,
        'order' => [
            'NotificationSettings.id' => 'asc'
        ]
    ];

    public function initialize() {
        parent::initialize();
        $this->loadComponent('Paginator');
    }

    public function index() {
        $notification = TableRegistry::get('NotificationSettings');
        $query = $notification->find('all');
        $this->set(compact('query'));
    }

    public function add() {

        $table_entry_ns = TableRegistry::get('NotificationSettings');
        $session = $this->request->session();
        $selected_office_section = $session->read('selected_office_section');
        $settings_type = "";
        if (!isset($selected_office_section['office_id']))
        {
            $selected_office_section['office_id'] = 0;
            $selected_office_section['officer_id'] = 0;
            $settings_type = "superadmin";
        }
        else if (isset($selected_office_section['officer_id']))
        {
            $settings_type = "employee";
        }

        $notification = "";
        if ($this->request->is('post')) {

            $table_entry_ns->deleteAll(['employee_id' => $selected_office_section['officer_id'], 'office_id' => $selected_office_section['office_id']]);

            $email = "";
            $sms = "";
            $system = "";
            $data = array();

            for ($i = 0; $i < count($this->request->data['event_id']); $i++) {
                $email = "";
                $sms = "";
                $system = "";
                if (isset($this->request->data['email'][$i]) && ($this->request->data['email'][$i] == 1))
                    $email = "email";
                else if (!isset($this->request->data['email'][$i]))
                    $email = "";

                if (isset($this->request->data['sms'][$i]) && ($this->request->data['sms'][$i] == 1))
                    $sms = "sms";
                else if (!isset($this->request->data['sms'][$i]))
                    $sms = "";

                if (isset($this->request->data['system'][$i]) && ($this->request->data['system'][$i] == 1))
                    $system = "system";
                else if (!isset($this->request->data['system'][$i]))
                    $system = "";

                $this->request->data['media'] = $email . "," . $sms . "," . $system;
                $this->request->data['media'] = str_replace(",,", ",", $this->request->data['media']);
                $this->request->data['media'] = trim($this->request->data['media'], ",");

                
                $data["settings_type"] = $settings_type;
                $data["event_id"] = $this->request->data['event_id'][$i];
                $data["media"] = $this->request->data['media'];
                $data["employee_id"] = $selected_office_section['officer_id'];
                $data["office_id"] = $selected_office_section['office_id'];

                $notification = $table_entry_ns->newEntity();
                $notification = $table_entry_ns->patchEntity($notification, $data);
                $table_entry_ns->save($notification);
            }

            return $this->redirect(['action' => 'add']);
        }

        $table_instance_ne = TableRegistry::get('NotificationEvents');
        $events = $table_instance_ne->find('all')->toArray();

        $table_instance_ns = TableRegistry::get('NotificationSettings');
        $event_settings = $table_instance_ns->find()->select(['event_id', 'media'])
                        ->where(['office_id' => $selected_office_section['office_id'], 'employee_id' => $selected_office_section['officer_id'], 'settings_type' => $settings_type])->toArray();

        $setting_list = array();
        foreach ($event_settings as $setting) {
            $setting_list[] = array('event_id' => $setting["event_id"], 'media' => $setting["media"]);
        }


        $event_list = array();
        foreach ($events as $event) {
            $media = "";
            for ($i = 0; $i < count($setting_list); $i++) {
                if ($event["id"] == $setting_list[$i]["event_id"]) {
                    $media = $setting_list[$i]["media"];
                    break;
                }
            }
            $event_list[] = array('id' => $event["id"], 'event_name_bng' => $event["event_name_bng"], 'media' => $media);
        }

        $this->set('event_list', $event_list);
        $this->set('notification', $notification);
    }

    public function edit($id = null, $is_ajax = null) {
        if ($is_ajax == 'ajax') {
            $this->layout = null;
        }

        $table_entry_ns = TableRegistry::get('NotificationSettings');
        $notification = $table_entry_ns->get($id);
        if ($this->request->is(['post', 'put'])) {
            $table_entry_ns->patchEntity($notification, $this->request->data);
            if ($table_entry_ns->save($notification)) {
                return $this->redirect(['action' => 'index']);
            }
        }
        $this->set('notification', $notification);
    }

    public function view($id = null) {
        $table_entry_ns = TableRegistry::get('NotificationSettings');
        $notification = $table_entry_ns->get($id);
        $this->set(compact('notification'));
    }

    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);

        $table_entry_ns = TableRegistry::get('NotificationSettings');
        $notification = $table_entry_ns->get($id);
        if ($this->NotificationSettings->delete($notification)) {
            return $this->redirect(['action' => 'index']);
        }
    }

}
