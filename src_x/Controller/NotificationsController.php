<?php

namespace App\Controller;

use App\Controller\ProjapotiController;
use App\Model\Table;
use App\Model\Table\NotificationsTable;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;


class NotificationsController extends ProjapotiController {

    public $paginate = [
        'fields' => ['Notifications.id'],
        'limit' => 25,
        'order' => [
            'Notifications.id' => 'asc'
        ]
    ];

    public function initialize() {
        parent::initialize();
        $this->loadComponent('Paginator');
    }

    public function index() {

        $notification = TableRegistry::get('Notifications');
        $query = $notification->find('all');
        $this->set(compact('query'));
    }

    public function add() {
        $notification = $this->Notifications->newEntity();
        if ($this->request->is('post')) {
            $notification = $this->Notifications->patchEntity($notification, $this->request->data);
            if ($this->Notifications->save($notification)) {
                return $this->redirect(['action' => 'add']);
            }
        }

        $table_instance_ne = TableRegistry::get('NotificationEvents');
        $events = $table_instance_ne->find('all');

        $event_list = array();
        foreach ($events as $event) {
            $event_list[] = array('id' => $event["id"], 'event_name_bng' => $event["event_name_bng"]);
        }
        $this->set('event_list', $event_list);

        $this->set('notification', $notification);
    }

    public function edit($id = null, $is_ajax = null) {
        if ($is_ajax == 'ajax') {
            $this->layout = null;
        }
        $notification = $this->Notifications->get($id);
        if ($this->request->is(['post', 'put'])) {
            $this->Notifications->patchEntity($notification, $this->request->data);
            if ($this->Notifications->save($notification)) {
                return $this->redirect(['action' => 'index']);
            }
        }
        $this->set('notification', $notification);
    }

    public function view($id = null) {
        $notification = $this->Notifications->get($id);
        $this->set(compact('notification'));
    }

    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);

        $notification = $this->Notifications->get($id);
        if ($this->Notifications->delete($notification)) {
            return $this->redirect(['action' => 'index']);
        }
    }

    public function dak_upload_notification($media = array()) {
        
    }

    public function dak_nothivukti_notification($media = array()) {
        
    }

    public function dak_nishpotti_notification($media = array()) {
        
    }

}
