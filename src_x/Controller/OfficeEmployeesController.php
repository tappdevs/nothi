<?php

namespace App\Controller;

use Cake\Network\Exception\NotFoundException;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use App\Controller\ProjapotiController;
use App\Model\Table;
use App\Model\Table\OfficeEmployeesTable;
use App\Model\Table\OfficeUnitsTable;
use Cake\Datasource\ConnectionManager;
use Cake\Cache\Cache;

class OfficeEmployeesController extends ProjapotiController
{
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->loadComponent('Paginator');
    }

    public function index()
    {
        $employee = $this->getCurrentDakSection();

        $officeid = $employee['office_id'];

        $table_employee_office = TableRegistry::get('EmployeeOffices');

        $query = $table_employee_office->find()->select([
            'EmployeeOffices.designation_level',
            'EmployeeOffices.designation_sequence',
            'EmployeeOffices.office_unit_id',
            'EmployeeOffices.office_unit_organogram_id',
            'EmployeeOffices.designation',
            'EmployeeOffices.joining_date',
            'EmployeeOffices.employee_record_id',
            "unit_name_bng" => 'OfficeUnits.unit_name_bng',
            "id" => 'EmployeeRecords.id',
            "personal_mobile" => 'EmployeeRecords.personal_mobile',
            "personal_email" => 'EmployeeRecords.personal_email',
            "name_bng" => 'EmployeeRecords.name_bng',
            "name_eng" => 'EmployeeRecords.name_eng',
            "identity_no" => 'EmployeeRecords.identity_no',
            "is_cadre" => 'EmployeeRecords.is_cadre',
            "username" => 'Users.username',
            "active" => 'Users.active'
        ])->join([
            'OfficeUnits' => [
                'table' => 'office_units',
                'type' => 'left',
                'conditions' => 'EmployeeOffices.office_unit_id = OfficeUnits.id'
            ],

            'EmployeeRecords' => [
                'table' => 'employee_records',
                'type' => 'left',
                'conditions' => 'EmployeeRecords.id = EmployeeOffices.employee_record_id'
            ],
            'Users' => [
                'table' => 'users',
                'type' => 'inner',
                'conditions' => 'EmployeeRecords.id = Users.employee_record_id'
            ]
        ])->where(['EmployeeOffices.office_id' => $officeid, 'EmployeeOffices.status' => 1])->order(['EmployeeOffices.designation_level' => 'ASC', 'EmployeeOffices.designation_sequence' => 'ASC']);

        $limit = 20;
        $page = 1;

        if (!empty($this->request->query['page'])) {
            $page = intval($this->request->query['page']);
        }

        if (!empty($this->request->data)) {
            if (!empty($this->request->data['username'])) {
                $query->where(['username LIKE' => '%' . $this->request->data['username'] . '%']);
            }
            if (!empty($this->request->data['name_bng'])) {
                $query->where(['name_bng LIKE' => '%' . $this->request->data['name_bng'] . '%']);
            }
            if (!empty($this->request->data['personal_mobile'])) {
                $query->where(['personal_mobile LIKE' => '%' . $this->request->data['personal_mobile'] . '%']);
            }
            if (!empty($this->request->data['national_id'])) {
                $query->where(['EmployeeRecords.nid LIKE' => '%' . $this->request->data['national_id'] . '%']);
            }
        }

        if (!empty($this->request->query('print'))) {
            $this->printExcel($query->toArray(), [
                ['key' => 'si', 'title' => 'ক্রম'],
                ['key' => 'name_bng', 'title' => 'নাম'],
                ['key' => 'designation', 'title' => 'পদবি'],
                ['key' => 'unit_name_bng', 'title' => 'সেকশন'],
                ['key' => 'personal_mobile', 'title' => 'মোবাইল'],
                ['key' => 'personal_email', 'title' => 'ইমেইল'],
                ['key' => 'username', 'title' => 'ইউজারনেম']
            ], ['name' => ((isset($employee['office_name'])) ? $employee['office_name'] . ' অফিসের ' : '') . 'দপ্তর কর্মকর্তার তালিকা']);
            exit;
        }
        $results = $this->paginate($query, ['limit' => $limit]);
        $protikolpo_settings_table = TableRegistry::get('ProtikolpoSettings');

        foreach($results as $result) {
                $protikolpo_data = $protikolpo_settings_table->find()->where(['designation_id' => $result['office_unit_organogram_id'], 'active_status' => 1])->first();

                if (!empty($protikolpo_data) && ($protikolpo_data['employee_record_id'] !=$result['employee_record_id'])) {
                    $result['main_employee_name'] = $protikolpo_data['employee_name'];
                    $result['protikolpo'] = ' (<b>প্রতিকল্প</b>)';
                }
        }

        $this->set(compact('results', 'limit', 'page'));
    }

    public function officeUnitNothiCodes()
    {

        $employee = $this->getCurrentDakSection();
        $officeUnits = TableRegistry::get('OfficeUnits');

        $results = $officeUnits->find()->select(['id', 'unit_name_bng', 'unit_nothi_code', 'sarok_no_start','unit_level'])->where(['office_id' => $employee['office_id']])->order(['unit_level asc, parent_unit_id asc'])->where(['active_status' => 1])->toArray();


        $this->set(compact('results'));
    }

    public function editOfficeUnitNothiCode($id = null)
    {
        $users = $this->getCurrentDakSection();

        $officeId = $users['office_id'];

        $officeUnitTable = TableRegistry::get('OfficeUnits');
        $officeUnitCategoryTable = TableRegistry::get('OfficeUnitCategories');

        $this->set('unitCategory', $officeUnitCategoryTable->find('list', ['keyField' => 'category_name_bng', 'valueField' => 'category_name_bng'])->toArray());

        $unit_id = $id;

        $entity = $officeUnitTable->get($unit_id);
        //if office mismatch no need save info
        if($officeId != $entity->office_id){
            $this->Flash->error("আপনি অনুমতিপ্রাপ্ত নয়।");
            $this->redirect('/dashboard');
        }
        //if office mismatch no need save info
        $officeUnitList = $officeUnitTable->find('list', ['keyField' => 'id', 'valueField' => 'unit_name_bng'])->where(['office_id' => $officeId, 'id <> ' => $unit_id])->toArray();

        $this->set('office_units', $officeUnitList);

        $this->set('entity', $entity);
    }

    public function designationLebel()
    {
        $employeeOffice = $this->getCurrentDakSection();

        $tabledesignation = TableRegistry::get('OfficeUnitOrganograms');

        $allOrganogram = $tabledesignation->getAll(['OfficeUnitOrganograms.office_id' => $employeeOffice['office_id']],
            ['OfficeUnitOrganograms.id', 'OfficeUnitOrganograms.designation_bng', 'OfficeUnitOrganograms.designation_level', 'OfficeUnitOrganograms.designation_sequence', 'OfficeUnits.unit_name_bng'])
            ->order([' designation_level ASC, designation_sequence ASC'])->join([
            'OfficeUnits' => [
                'table' => 'office_units',
                'type' => 'inner',
                'conditions' => 'OfficeUnits.id = OfficeUnitOrganograms.office_unit_id AND OfficeUnits.office_id = OfficeUnitOrganograms.office_id'
            ]
        ])->toArray();

        $this->set(compact('allOrganogram'));
    }

    public function designationDetails()
    {
        $employeeOffice = $this->getCurrentDakSection();

        $tabledesignation = TableRegistry::get('OfficeUnitOrganograms');

        $allOrganogram = $tabledesignation->getAll(['OfficeUnitOrganograms.office_id' => $employeeOffice['office_id']], ['OfficeUnitOrganograms.id', 'OfficeUnitOrganograms.designation_bng', 'OfficeUnitOrganograms.designation_level', 'OfficeUnitOrganograms.designation_sequence', 'OfficeUnits.unit_name_bng', 'OfficeUnitOrganograms.designation_description'])->order([' designation_level ASC, designation_sequence ASC'])->join([
            'OfficeUnits' => [
                'table' => 'office_units',
                'type' => 'inner',
                'conditions' => 'OfficeUnits.id = OfficeUnitOrganograms.office_unit_id AND OfficeUnits.office_id = OfficeUnitOrganograms.office_id'
            ]
        ])->toArray();

        $this->set(compact('allOrganogram'));
    }

    public function officeEmployeesDesignationUpdate()
    {
        $employeeOffice = $this->getCurrentDakSection();

        $tabledesignation = TableRegistry::get('OfficeUnitOrganograms');
        $tableemployeedesignation = TableRegistry::get('EmployeeOffices');

        TableRegistry::remove('NothiMasterPermissions');
        TableRegistry::remove('OfficeUnitSeals');
        TableRegistry::remove('OfficeDesignationSeals');
        $tableNothiMasterPermissions = TableRegistry::get('NothiMasterPermissions');
        $tableOfficeUnitSeals = TableRegistry::get('OfficeUnitSeals');
        $tableOfficeDesignationSeals = TableRegistry::get('OfficeDesignationSeals');

        if (!empty($this->request->data)) {

            $id = $this->request->data['id'];

            $designation = $tabledesignation->get($id);

            try {
                $designation->designation_description = !empty($this->request->data['designation_description']) ? $this->request->data['designation_description'] : $designation['designation_description'];
                $designation->designation_level= !empty($this->request->data['designation_level']) ? $this->request->data['designation_level'] : 999;
                $designation->designation_sequence = !empty($this->request->data['designation_sequence']) ? $this->request->data['designation_sequence'] : $designation['designation_sequence'];
                $designation->designation_level = bnToen($designation->designation_level);
                $designation->designation_sequence = bnToen($designation->designation_sequence);

                $tabledesignation->save($designation);

                $tableemployeedesignation->updateAll(['designation_level' => $designation->designation_level, 'designation_sequence' => $designation->designation_sequence], ['office_id' => $employeeOffice['office_id'], 'office_unit_organogram_id' => $id]);
                $tableNothiMasterPermissions->updateAll(
                    [
                        'designation_level' => $designation->designation_level
                    ],
                    [
                        'nothi_office' => $employeeOffice['office_id'],
                        'office_unit_organograms_id' => $id
                    ]);

                $tableOfficeUnitSeals->updateAll(
                    [
                        'designation_level' => $designation->designation_level,
                        'designation_seq' => $designation->designation_sequence,
                    ],
                    [
                        'office_id' => $employeeOffice['office_id'],
                        'office_unit_organogram_id' => $id
                    ]);

                $tableOfficeDesignationSeals->updateAll(
                    [
                        'designation_level' => $designation->designation_level,
                        'designation_seq' => $designation->designation_sequence,
                    ],
                    [
                        'office_id' => $employeeOffice['office_id'],
                        'office_unit_organogram_id' => $id
                    ]);

                echo 1;
            } catch (Exception $ex) {

                echo $this->makeEncryptedData($ex->getMessage());
            }
        }


        die;
    }



    public function designationNameUpdate()
    {
        $user = $this->Auth->user();
        if (empty($user) || $user['user_role_id'] > 2) {
            return $this->redirect(['controller' => 'Users', 'action' => 'login']);
        }
    }
    public function designationNameUpdateOfficeWise()
    {
        $office_id = $this->getCurrentDakSection()['office_id'];
        $office_unit_organogram_id = $this->getCurrentDakSection()['office_unit_organogram_id'];
        $offices = TableRegistry::get('Offices')->get($office_id);
        if($offices['unit_organogram_edit_option'] == '1' || $offices['unit_organogram_edit_option'] == '3') {
            $user = $this->Auth->user();
            $employee_offices = TableRegistry::get('OfficeUnitOrganograms')->find()->where(['id' => $office_unit_organogram_id, 'is_admin' => 1])->first();
            if ($employee_offices) {
                if (empty($user) || $user['user_role_id'] == 3) {
                    $page = 1;
                    $this->request->query['office_id'] = $office_id;
                    if ($this->request->is('get')) {
                        $this->layout = null;
                        if (isset($this->request->query['page'])) {
                            if (($this->request->query['page'] == 1)) {
                                $page = 1;
                            } else {
                                $page = (intval($this->request->query['page'])) * 10 + $page;
                            }

                        }

                        if (isset($this->request->query['reset'])) {
                            $this->layout = null;
                        }

                        if (isset($this->request->query['search'])) {
                            $this->set('search_text', $this->request->query['search']);
                            $this->layout = null;
                            $designation_info = TableRegistry::get('OfficeUnitOrganograms')->allDesignationName($this->request->query);
                        } else {
                            $designation_info = TableRegistry::get('OfficeUnitOrganograms')->allDesignationName($this->request->query);
                        }
                        if (isset($this->request->query['unit_id'])) {
                            $this->set('unit_id', $this->request->query['unit_id']);
                            $this->set('office_id', $this->request->query['office_id']);
                            $this->layout = null;
                            $designation_info = TableRegistry::get('OfficeUnitOrganograms')->allDesignationName($this->request->query);
                        }

                        if ($this->request->is('ajax')) {
                            $allOrganogram = $this->paginate($designation_info);
                            $i = $page;
                            foreach ($allOrganogram as $rows):
                                $office_name = TableRegistry::get('Offices')->getBanglaName($rows['office']);
                                $unit_name   = TableRegistry::get('OfficeUnits')->getBanglaName($rows['unit']);
                                ?>
                                <tr >
                                    <td class="text-center"><?php echo enTobn($i++) ?></td>
                                    <td class="text-center " id = "<?= "designation_bn".$rows['id'] ?>">
                                        <input type="hidden" id="designation_bn_<?=$rows['id']?>" value="<?= $rows['d_bng'] ?>">
                                        <?php echo $rows['d_bng'] . (!empty($rows['EmployeeRecords']['name_bng'])?(' (' . $rows['EmployeeRecords']['name_bng'] . ')'):''); ?>
                                    </td>
                                    <td class="text-center" id = "<?= "designation_en".$rows['id'] ?>">
                                        <input type="hidden" id="designation_en_<?=$rows['id']?>" value="<?= $rows['d_eng'] ?>">
                                        <?php echo $rows['d_eng'] . (!empty($rows['EmployeeRecords']['name_eng'])?(' (' . $rows['EmployeeRecords']['name_eng'] . ')'):''); ?>
                                    </td>
                                    <td class="text-center" ><?php echo $unit_name['unit_name_bng']; ?></td>
                                    <td class="text-center" ><?php echo $office_name['office_name_bng']; ?></td>
                                    <td class="text-center">
                                        <a class="btn   btn-primary" data-toggle="modal" data-target="#yourModal" onclick="update(<?= $rows['id'] ?>);"><?= __('Edit') ?></a>
                                    </td>
                                </tr>
                            <?php
                            endforeach;
                            die;
                        }
                        $this->set('allOrganogram', $this->paginate($designation_info));
                        $this->set(compact('page'));
                    }


                } else {
                    return $this->redirect(['controller' => 'Users', 'action' => 'login']);
                }
            } else {
                $this->Flash->error('আপনি অনুমতিপ্রাপ্ত নন।');
                return $this->redirect('/');
            }
        } else {
            $this->Flash->error('আপনি অনুমতিপ্রাপ্ত নন।');
            return $this->redirect('/');
        }
    }

    public function designationNameSearch()
    {
        $user = $this->Auth->user();
        if (empty($user) || $user['user_role_id'] > 2) {
            return $this->redirect(['controller' => 'Users', 'action' => 'login']);
        }
        if ($user['user_role_id'] <= 2) {
            $page = 1;
            if ($this->request->is('get')) {
                $this->layout = null;
                if (isset($this->request->query['page'])) {
                    if (($this->request->query['page'] == 1)) {
                        $page = 1;
                    } else {
                        $page = (intval($this->request->query['page'])) * 10 + $page;
                    }

                }

                if (isset($this->request->query['reset'])) {
                    $this->layout = null;
                }
                if (isset($this->request->query['search'])) {
                    $this->set('search_text', $this->request->query['search']);
                    $this->layout = null;
                    $designation_info = TableRegistry::get('OfficeUnitOrganograms')->allDesignationName($this->request->query);
                } else {
                    $designation_info = TableRegistry::get('OfficeUnitOrganograms')->allDesignationName($this->request->query);
                }
                if (isset($this->request->query['unit_id'])) {
                    $this->set('unit_id', $this->request->query['unit_id']);
                    $this->set('office_id', $this->request->query['office_id']);
                    $this->layout = null;
                    $designation_info = TableRegistry::get('OfficeUnitOrganograms')->allDesignationName($this->request->query);
                }

                $this->set('allOrganogram', $this->paginate($designation_info));
                $this->set(compact('page'));
            }

        }
    }

    public function designationNameSaveOfficeWise()
    {
        $user = $this->Auth->user();
        if ($user['user_role_id'] == 3) {
            $data = [
                'msg' => 'সকল তথ্য সঠিক ভাবে পূরণ করুন',
                'status' => 'error'
            ];
            if ($this->request->is('post')) {
                $employee_office = $this->getCurrentDakSection();
                $this->layout = null;
                $bng = !empty($this->request->data['bn_text']) ? $this->request->data['bn_text'] : '';
                $eng = !empty($this->request->data['en_text']) ? $this->request->data['en_text'] : '';
                $id = !empty($this->request->data['id']) ? $this->request->data['id'] : '';
                if (empty($bng) || empty($id)) {
                    $this->response->type('Application/json');
                    $this->response->body(json_encode($data));
                    return $this->response;
                }
                $designation_table = TableRegistry::get('OfficeUnitOrganograms');
                $organogram_origin_table = TableRegistry::get('OfficeOriginUnitOrganograms');
                $office_layer_table = TableRegistry::get('OfficeLayers');
                $offices_table = TableRegistry::get('Offices');
                $tableEmployeeOffice = TableRegistry::get('EmployeeOffices');
                $designation_info = $designation_table->get($id);
                if (empty($designation_info)) {
                    $data['msg'] = ' তথ্য পাওয়া যাই নি ';
                    $this->response->type('Application/json');
                    $this->response->body(json_encode($data));
                    return $this->response;
                }
                if ($user['user_role_id'] > 2) {
                    $employee_info = $this->getCurrentDakSection();
                } else {
                    $employee_info['office_id'] = $employee_info['office_unit_id'] = $employee_info['office_unit_organogram_id'] = 0;
                }
                $office_info = $offices_table->get($designation_info['office_id']);

                if (empty($office_info)) {
                    $data['msg'] = ' তথ্য পাওয়া যাই নি ';
                    $this->response->type('Application/json');
                    $this->response->body(json_encode($data));
                    return $this->response;
                }


                // use ($designation_info, $id, $eng, $bng,$employee_info,$user)
                $update_data['id'] = $id;
                $update_data['eng'] = $eng;
                $update_data['bng'] = $bng;

                $conn = ConnectionManager::get('default');
                $f = true;
                $f = $conn->transactional(function ($conn) use ($designation_info, $update_data, $employee_info, $user, $designation_table) {
                    $designationUpdateHistoryTable = TableRegistry::get('designationUpdateHistory');
                    $designationUpdateHistoryEntity = $designationUpdateHistoryTable->newEntity();
                    $designationUpdateHistoryEntity->designation_id = $update_data['id'];
                    $designationUpdateHistoryEntity->office_id = $designation_info['office_id'];
                    $designationUpdateHistoryEntity->office_unit_id = $designation_info['office_unit_id'];
                    $designationUpdateHistoryEntity->superior_unit_id = $designation_info['superior_unit_id'];
                    $designationUpdateHistoryEntity->superior_designation_id = $designation_info['superior_designation_id'];
                    $designationUpdateHistoryEntity->ref_origin_unit_org_id = $designation_info['ref_origin_unit_org_id'];
                    $designationUpdateHistoryEntity->old_designation_eng = $designation_info['designation_eng'];
                    $designationUpdateHistoryEntity->old_designation_bng = $designation_info['designation_bng'];
                    $designationUpdateHistoryEntity->designation_eng = $update_data['eng'];
                    $designationUpdateHistoryEntity->designation_bng = $update_data['bng'];
                    $designationUpdateHistoryEntity->employee_office_id = $employee_info['office_id'];
                    $designationUpdateHistoryEntity->employee_unit_id = $employee_info['office_unit_id'];
                    $designationUpdateHistoryEntity->employee_designation_id = $employee_info['office_unit_organogram_id'];
                    $designationUpdateHistoryEntity->created_by = $user['id'];
                    $designationUpdateHistoryEntity->modified_by = $user['id'];

                    if (!($designationUpdateHistoryTable->save($designationUpdateHistoryEntity))) {
                        return false;
                    } else {
                        $projapotiGroupsMigTable = TableRegistry::get('PotrojariGroupsUsers');
                        $designation_info->designation_eng = $update_data['eng'];
                        $designation_info->designation_bng = $update_data['bng'];
                        if (!($designation_table->save($designation_info) && $projapotiGroupsMigTable->updatePotrojariGroupDesignationDetails($update_data['id'], $update_data['eng'], $update_data['bng']))) {

                            return false;
                        }
                    }
                    return true;
                });

                $office_layer_info = $office_layer_table->get($office_info['office_layer_id']);

                if(!empty($office_layer_info) && $office_layer_info['layer_level']<=3
                    && $office_layer_info['layer_level'] != 0){
                    $organogram_origin_table->updateAll(['designation_bng'=>$bng,
                        'designation_eng'=>$eng],
                        ['id'=>$designation_info['ref_origin_unit_org_id']]);
                }

                if ($f == false) {
                    $data['msg'] = ' তথ্য সংরক্ষণ সম্ভব হচ্ছে না। কিছুক্ষণ পর আবার চেষ্টা করুন ';
                } else {

                    $isExists = $tableEmployeeOffice->find()->where([
                        'office_unit_organogram_id'=>$designation_info['id'],
                        'status'=> 1,
                    ])->first();
                    if(!empty($isExists)) {
                        $unassign = $tableEmployeeOffice->unAssignDesignation($isExists['id']);

                        if ($unassign) {

                            $assign = $tableEmployeeOffice->assignDesignation(
                                [
                                    'employee_record_id' => $isExists['employee_record_id'],
                                    'identification_number' => $isExists['identification_number'],
                                    'office_id' => $designation_info['office_id'],
                                    'office_unit_id' => $designation_info['office_unit_id'],
                                    'office_unit_organogram_id' => $designation_info['id'],
                                    'designation' => $designation_info['designation_bng'],
                                    'incharge_label' => $isExists['incharge_label'],
                                    'joining_date' => date("Y-m-d H:i:s"),
                                ]
                            );

                            if ($assign) {
                                if ($employee_office['officer_id'] == $isExists['employee_record_id']) {
                                    $data = [
                                        'msg' => "সংরক্ষিত হয়েছে",
                                        'status' => 'success',
                                        'redirect' => true
                                    ];
                                } else {
                                    $data = [
                                        'msg' => "সংরক্ষিত হয়েছে। ব্যবহারকারি লগআউট করে লগিন করলে পরিবর্তন দেখতে পারবে",
                                        'status' => 'success', 'redirect' => false
                                    ];
                                }
                            }
                        }
                    }else{
                        $data = [
                            'msg' => "সংরক্ষিত হয়েছে",
                            'status' => 'success',
                            'redirect' => false
                        ];
                    }
                }
                $this->response->type('application/json');
                $this->response->body(json_encode($data));
                return $this->response;
            }
        }
    }

    public function designationNameSave()
    {
        $user = $this->Auth->user();
        if (empty($user) || $user['user_role_id'] > 2) {
            return $this->redirect(['controller' => 'Users', 'action' => 'login']);
        }
        if ($user['user_role_id'] <= 2) {
            $data = [
                'msg' => 'সকল তথ্য সঠিক ভাবে পূরণ করুন',
                'status' => 'error'
            ];
            if ($this->request->is('post')) {
                $this->layout = null;
                $bng = !empty($this->request->data['bn_text']) ? $this->request->data['bn_text'] : '';
                $eng = !empty($this->request->data['en_text']) ? $this->request->data['en_text'] : '';
                $id = !empty($this->request->data['id']) ? intval($this->request->data['id']) : 0;
                if (empty($bng) || empty($id)) {
                    $this->response->type('Application/json');
                    $this->response->body(json_encode($data));
                    return $this->response;
                }
                $designation_table = TableRegistry::get('OfficeUnitOrganograms');
                $organogram_origin_table = TableRegistry::get('OfficeOriginUnitOrganograms');
                $office_layer_table = TableRegistry::get('OfficeLayers');
                $offices_table = TableRegistry::get('Offices');
                $designation_info = $designation_table->get($id);
                if (empty($designation_info)) {
                    $data['msg'] = ' তথ্য পাওয়া যাই নি ';
                    $this->response->type('Application/json');
                    $this->response->body(json_encode($data));
                    return $this->response;
                }
                if ($user['user_role_id'] > 2) {
                    $employee_info = $this->getCurrentDakSection();
                } else {
                    $employee_info['office_id'] = $employee_info['office_unit_id'] = $employee_info['office_unit_organogram_id'] = 0;
                }
                $office_info = $offices_table->get($designation_info['office_id']);

                if (empty($office_info)) {
                    $data['msg'] = ' তথ্য পাওয়া যাই নি ';
                    $this->response->type('Application/json');
                    $this->response->body(json_encode($data));
                    return $this->response;
                }


                // use ($designation_info, $id, $eng, $bng,$employee_info,$user)
                $update_data['id'] = $id;
                $update_data['eng'] = $eng;
                $update_data['bng'] = $bng;

                $conn = ConnectionManager::get('default');
                $f = true;
                $f = $conn->transactional(function ($conn) use ($designation_info, $update_data, $employee_info, $user, $designation_table) {
                    $designationUpdateHistoryTable = TableRegistry::get('designationUpdateHistory');
                    $designationUpdateHistoryEntity = $designationUpdateHistoryTable->newEntity();
                    $designationUpdateHistoryEntity->designation_id = $update_data['id'];
                    $designationUpdateHistoryEntity->office_id = $designation_info['office_id'];
                    $designationUpdateHistoryEntity->office_unit_id = $designation_info['office_unit_id'];
                    $designationUpdateHistoryEntity->superior_unit_id = $designation_info['superior_unit_id'];
                    $designationUpdateHistoryEntity->superior_designation_id = $designation_info['superior_designation_id'];
                    $designationUpdateHistoryEntity->ref_origin_unit_org_id = $designation_info['ref_origin_unit_org_id'];
                    $designationUpdateHistoryEntity->old_designation_eng = $designation_info['designation_eng'];
                    $designationUpdateHistoryEntity->old_designation_bng = $designation_info['designation_bng'];
                    $designationUpdateHistoryEntity->designation_eng = $update_data['eng'];
                    $designationUpdateHistoryEntity->designation_bng = $update_data['bng'];
                    $designationUpdateHistoryEntity->employee_office_id = $employee_info['office_id'];
                    $designationUpdateHistoryEntity->employee_unit_id = $employee_info['office_unit_id'];
                    $designationUpdateHistoryEntity->employee_designation_id = $employee_info['office_unit_organogram_id'];
                    $designationUpdateHistoryEntity->created_by = $user['id'];
                    $designationUpdateHistoryEntity->modified_by = $user['id'];

                    if (!($designationUpdateHistoryTable->save($designationUpdateHistoryEntity))) {
                        return false;
                    } else {
                        $projapotiGroupsMigTable = TableRegistry::get('PotrojariGroupsUsers');
                        $designation_info->designation_eng = $update_data['eng'];
                        $designation_info->designation_bng = $update_data['bng'];
                        if (!($designation_table->save($designation_info) && $projapotiGroupsMigTable->updatePotrojariGroupDesignationDetails($update_data['id'], $update_data['eng'], $update_data['bng']))) {

                            return false;
                        }
                    }
                    return true;
                });

                $office_layer_info = $office_layer_table->get($office_info['office_layer_id']);

                if(!empty($office_layer_info) && $office_layer_info['layer_level']<=3
                    && $office_layer_info['layer_level'] != 0){
                    $organogram_origin_table->updateAll(['designation_bng'=>$bng,
                        'designation_eng'=>$eng],
                        ['id'=>$designation_info['ref_origin_unit_org_id']]);
                }

                //NEW CODE
                $this->switchOffice($designation_info['office_id'], 'MainNothiOffice');
                $tableOfficeUnitSeals = TableRegistry::get('OfficeUnitSeals');
                $tableOfficeDesignationSeals = TableRegistry::get('OfficeDesignationSeals');
                $tableOfficeUnitSeals->updateAll(
                    [
                        'designation_name_bng' => $bng,
                        'designation_name_eng' => $eng,
                    ],
                    [
                        'office_id' => $designation_info['office_id'],
                        'office_unit_organogram_id' => $id
                    ]);

                $tableOfficeDesignationSeals->updateAll(
                    [
                        'designation_name_bng' => $bng,
                        'designation_name_eng' => $eng,
                    ],
                    [
                        'office_id' => $designation_info['office_id'],
                        'office_unit_organogram_id' => $id
                    ]);

                $tableEmployeeOffice = TableRegistry::get('EmployeeOffices');
                $isExists = $tableEmployeeOffice->find()->where([
                    'office_unit_organogram_id'=>$designation_info['id'],
                    'status'=> 1,
                ])->first();
                if(!empty($isExists)) {
                    $unassign = $tableEmployeeOffice->unAssignDesignation($isExists['id']);

                    if ($unassign) {

                        $assign = $tableEmployeeOffice->assignDesignation(
                            [
                                'employee_record_id' => $isExists['employee_record_id'],
                                'identification_number' => $isExists['identification_number'],
                                'office_id' => $designation_info['office_id'],
                                'office_unit_id' => $designation_info['office_unit_id'],
                                'office_unit_organogram_id' => $designation_info['id'],
                                'designation' => $designation_info['designation_bng'],
                                'incharge_label' => $isExists['incharge_label'],
                                'joining_date' => date("Y-m-d H:i:s"),
                            ]
                        );
                    }
                    //NEW CODE END
                    if ($f == false) {
                        $data['msg'] = ' তথ্য সংরক্ষণ সম্ভব হচ্ছে না। কিছুক্ষণ পর আবার চেষ্টা করুন ';
                    } else {
                        $data = [
                            'msg' => "সংরক্ষিত হয়েছে",
                            'status' => 'success'
                        ];
                    }
                }else{
                    $data = [
                        'msg' => "সংরক্ষিত হয়েছে",
                        'status' => 'success'
                    ];
                }
                $this->response->type('application/json');
                $this->response->body(json_encode($data));
                return $this->response;
            }
        }
    }

    public function editUnitsInfo()
    {
        $user = $this->Auth->user();
        if (empty($user) || $user['user_role_id'] > 2) {
            return $this->redirect(['controller' => 'Users', 'action' => 'login']);
        }
    }
    public function editUnitsInfoOfficeWise()
    {
        $office_id = $this->getCurrentDakSection()['office_id'];
        $office_unit_organogram_id = $this->getCurrentDakSection()['office_unit_organogram_id'];
        $offices = TableRegistry::get('Offices')->get($office_id);
        if($offices['unit_organogram_edit_option'] == '1' || $offices['unit_organogram_edit_option'] == '2') {
            $user = $this->Auth->user();
            $employee_offices = TableRegistry::get('OfficeUnitOrganograms')->find()->where(['id' => $office_unit_organogram_id, 'is_admin' => 1])->first();
            if ($employee_offices) {
                if (empty($user) || $user['user_role_id'] == 3) {
                    $OfficeUnitsData = TableRegistry::get('OfficeUnits')->getUnitsInfoByOfficeId($office_id)->select(['id', 'unit_name_bng', 'unit_name_eng'])->toArray();
                    $this->set(compact('OfficeUnitsData'));
                    $this->set(compact('office_id'));
                } else {
                    $this->Flash->error('This option only for admin user');
                    return $this->redirect('/');
                }
            } else {
                $this->Flash->error('This option only for office admin');
                return $this->redirect('/');
            }
        } else {
            $this->Flash->error('আপনি অনুমতিপ্রাপ্ত নন');
            return $this->redirect('/');
        }
    }

    public function getUnitInfoForEdit()
    {
        $ministry_id = isset($this->request->query['ministry_id']) ? $this->request->query['ministry_id'] : '';
        $layer_id = isset($this->request->query['layer_id']) ? $this->request->query['layer_id'] : '';
        $origin_id = isset($this->request->query['origin_id']) ? $this->request->query['origin_id'] : '';
        $office_id = isset($this->request->query['office_id']) ? $this->request->query['office_id'] : '';

        if (empty($office_id)) {
            echo 'Office id is not given';
            die;
        }
        try {
            $this->layout = 'ajax';
            $OfficeUnitsData = TableRegistry::get('OfficeUnits')->getUnitsInfoByOfficeId($office_id)->select(['id', 'unit_name_bng', 'unit_name_eng'])->toArray();
            $this->set(compact('OfficeUnitsData'));
        } catch (\Exception $ex) {
            echo $ex->getMessage();
            die;
        }
    }

    public function unitInfoUpdate()
    {
        $user = $this->Auth->user();
        //if (empty($user) || $user['user_role_id'] > 2) {
        //    die('Invalid request');
        //}
        if ($user['user_role_id'] <= 20) {
            $data = [
                'msg' => 'সকল তথ্য সঠিক ভাবে পূরণ করুন',
                'status' => 'error'
            ];
            if ($this->request->is('post')) {
                $this->layout = null;
                $requestData = $this->request->data;
                $bng = !empty($requestData['bn_text']) ? $requestData['bn_text'] : '';
                $eng = !empty($requestData['en_text']) ? $requestData['en_text'] : '';
                $id = !empty($requestData['id']) ? $requestData['id'] : '';
                if (empty($bng) || empty($id)) {
                    $this->response->type('Application/json');
                    $this->response->body(json_encode($data));
                    return $this->response;
                }
                try {
                    $unit_table = TableRegistry::get('OfficeUnits');
                    $unit_origin_table = TableRegistry::get('OfficeOriginUnits');
                    $office_layer_table = TableRegistry::get('OfficeLayers');
                    $offices_table = TableRegistry::get('Offices');
                    $unit_info = $unit_table->get($id);
                    if (empty($unit_info)) {
                        $data['msg'] = ' তথ্য পাওয়া যাই নি ';
                        $this->response->type('Application/json');
                        $this->response->body(json_encode($data));
                        return $this->response;
                    }
                    if ($user['user_role_id'] > 2) {
                        $employee_info = $this->getCurrentDakSection();
                    } else {
                        $employee_info['office_id'] = $employee_info['office_unit_id']
                            = $employee_info['office_unit_organogram_id'] = 0;
                    }

                    $office_info = $offices_table->get($unit_info['office_id']);

                    if (empty($office_info)) {
                        $data['msg'] = ' তথ্য পাওয়া যাই নি ';
                        $this->response->type('Application/json');
                        $this->response->body(json_encode($data));
                        return $this->response;
                    }

                    $update_data['id'] = $id;
                    $update_data['eng'] = $eng;
                    $update_data['bng'] = $bng;

                    $conn = ConnectionManager::get('default');
                    $f = true;
                    $f = $conn->transactional(function ($conn) use ($unit_info, $update_data, $employee_info, $user, $unit_table) {
                        $unitUpdateHistoryTable = TableRegistry::get('UnitUpdateHistory');
                        $unitUpdateHistoryEntity = $unitUpdateHistoryTable->newEntity();
                        $unitUpdateHistoryEntity->office_id = $unit_info['office_id'];
                        $unitUpdateHistoryEntity->office_unit_id = $update_data['id'];
                        $unitUpdateHistoryEntity->office_origin_unit_id = $unit_info['office_origin_unit_id'];
                        $unitUpdateHistoryEntity->parent_unit_id = $unit_info['parent_unit_id'];
                        $unitUpdateHistoryEntity->old_unit_eng = $unit_info['unit_name_eng'];
                        $unitUpdateHistoryEntity->old_unit_bng = $unit_info['unit_name_bng'];
                        $unitUpdateHistoryEntity->unit_eng = $update_data['eng'];
                        $unitUpdateHistoryEntity->unit_bng = $update_data['bng'];
                        $unitUpdateHistoryEntity->employee_office_id = $employee_info['office_id'];
                        $unitUpdateHistoryEntity->employee_unit_id = $employee_info['office_unit_id'];
                        $unitUpdateHistoryEntity->employee_designation_id = $employee_info['office_unit_organogram_id'];
                        $unitUpdateHistoryEntity->created_by = $user['id'];
                        $unitUpdateHistoryEntity->modified_by = $user['id'];

                        if (!($unitUpdateHistoryTable->save($unitUpdateHistoryEntity))) {
                            return false;
                        } else {
                            $potrojariGroupsMigTable = TableRegistry::get('PotrojariGroupsUsers');
                            $unit_info->unit_name_eng = $update_data['eng'];
                            $unit_info->unit_name_bng = $update_data['bng'];
                            if (!($unit_table->save($unit_info) && $potrojariGroupsMigTable->updatePotrojariGroupUnitDetails($update_data['id'], $update_data['eng'], $update_data['bng']))) {
                                return false;
                            }
                        }
                        return true;
                    });

                    $office_layer_info = $office_layer_table->get($office_info['office_layer_id']);

                    if(!empty($office_layer_info) && $office_layer_info['layer_level']<=3 && $office_layer_info['layer_level'] != 0){
                        $unit_origin_table->updateAll(['unit_name_bng'=>$bng,'unit_name_eng'=>$eng],['id'=>$unit_info['office_origin_unit_id']]);
                    }

                    if ($f == false) {
                        $data['msg'] = ' তথ্য সংরক্ষণ সম্ভব হচ্ছে না। কিছুক্ষণ পর আবার চেষ্টা করুন ';
                    } else {
                        $data = [
                            'msg' => "সংরক্ষিত হয়েছে",
                            'status' => 'success'
                        ];
                    }
                } catch (\Exception $ex) {
                    $data['msg'] = ' তথ্য সংরক্ষণ সম্ভব হচ্ছে না। কিছুক্ষণ পর আবার চেষ্টা করুন ';
                    $data['reason'] = $this->makeEncryptedData($ex->getMessage());
                }

                $this->response->type('application/json');
                $this->response->body(json_encode($data));
                return $this->response;
            }
        }
    }
    public function employeeSignatureSetting(){
         $employee_office = $this->getCurrentDakSection();
          if(empty($employee_office)){
               return $this->redirect(['controller' => 'User','action' => 'login']);
            }
        if($this->request->is('get')){
            $data = TableRegistry::get('EmployeeOffices')->getEmployeeOfficeRecordsByOfficeId($employee_office['office_id']);
            $this->set(compact('data'));
        }else{
           $response = ['status' => 'error', 'msg' => 'Something happen'];
            $id = !empty($this->request->data['id'])?$this->request->data['id']:0;
            $soft_signature = !empty($this->request->data['soft_signature'])?$this->request->data['soft_signature']:0;
            $hard_signature = !empty($this->request->data['hard_signature'])?$this->request->data['hard_signature']:0;
            if(empty($id)){
                $response['msg'] = 'কোন তথ্য পাওয়া যায়নি';;
                goto rtn;
            }
            try{
                if(empty($soft_signature) && empty($hard_signature)){
                    TableRegistry::get('EmployeeRecords')->updateAll(['soft_signature' => $soft_signature,'hard_signature' =>$hard_signature,'default_sign' =>0], ['id' => $id]);
                }else{
                    TableRegistry::get('EmployeeRecords')->updateAll(['soft_signature' => $soft_signature,'hard_signature' =>$hard_signature], ['id' => $id]);
                }
                $response = ['status' => 'success', 'msg' => 'সংরক্ষণ করা হয়েছে। '];
            } catch (\Exception $ex) {
                $response['msg'] = $ex->getMessage();
            }
            rtn:
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        }
    }
}
