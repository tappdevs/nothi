<?php
namespace App\Controller;

use App\Controller\ProjapotiController;
use App\Model\Table;
use App\Model\Table\OfficeHierarchyTemplatesTable;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\Entity;
use App\Model\Entity\OfficeHierarchyTemplate;
use Cake\ORM\TableRegistry;

class OfficeHierarchyTemplatesController extends ProjapotiController
{
    public function index()
    {
        $office_organization_structure = TableRegistry::get('OfficeHierarchyTemplates');
        $query = $office_organization_structure->find('all');
        $this->set(compact('query'));
    }

    public function add()
    {
        $parent_id = isset($this->request->query['parent_id']) ? $this->request->query['parent_id'] : "";
        $this->layout = null;
        $office_hie_table = TableRegistry::get('OfficeHierarchyTemplates');
        $office_hie_entity = $office_hie_table->newEntity();
        if ($this->request->is('post')) {
            parse_str($this->request->data['formdata'], $this->request->data);
            $office_hie_entity = $office_hie_table->patchEntity($office_hie_entity, $this->request->data);
            if ($office_hie_table->save($office_hie_entity)) {
                $this->response->body(json_encode(1));
                $this->response->type('application/json');
                return $this->response;
            }
        }
        $this->set('parent', $parent_id);
        $this->set('office_hierarchy_template', $office_hie_entity);
    }

    public function edit($id = null)
    {
        $office_organization_structure = $this->OfficeHierarchyTemplate->get($id);
        if ($this->request->is(['post', 'put'])) {
            $this->OfficeHierarchyTemplate->patchEntity($office_organization_structure, $this->request->data);
            if ($this->OfficeHierarchyTemplate->save($office_organization_structure)) {
                return $this->redirect(['action' => 'index']);
            }
        }
        $this->set('office_organization_structure', $office_organization_structure);
    }

    public function view($id = null)
    {
        $office_organization_structure = $this->OfficeHierarchyTemplate->get($id);
        $this->set(compact('office_organization_structure'));
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);

        $office_organization_structure = $this->OfficeHierarchyTemplate->get($id);
        if ($this->OfficeHierarchyTemplate->delete($office_organization_structure)) {
            return $this->redirect(['action' => 'index']);
        }
    }

    public function globalOfficeHierarchy()
    {

    }

    public function loadGlobalOfficeHierarchy($parent = 0)
    {
        $request_data = $this->request->query['id'];
        $request_type = $this->request->query['type'];
        $request_arr = explode("_", $request_data);
        $type = "";
        foreach ($request_arr as $arr) {
            if (intval($arr) > 0) {
                continue;
            }
            $type .= $arr . "_";
        }
        $parent = $request_arr[count($request_arr) - 1];

        $globalOfficeHierarchyTable = TableRegistry::get('OfficeHierarchyTemplates');
        $globalOfficeHierarchy = $globalOfficeHierarchyTable->find()->select(['id', 'title_eng', 'title_bng', 'structure_type'])->where(['parent' => $parent])->order(['sequence'])->toArray();
        $data = array();

        if ($parent == '#') {
            $parent = 0;
        }
        /* Load Existing Data From Database*/
        foreach ($globalOfficeHierarchy as $div) {
            $row = array();
            $row["id"] = "node_" . $parent . "_" . $div['id'];
            $row["data-id"] = $div['id'];
            //$row["text"] = '<a href="javascript:;" data-parent-node="'.$request_data.'" data-id="'.$div['id'].'" data-parent="'.$parent.'" onclick="GeoTree.gotoEdit(this)">'.$div['title_bng'].'['.$div['title_eng'].']</a>';
            $row["text"] = '<a href="javascript:;" data-parent-node="' . $request_data . '" data-id="' . $div['id'] . '" data-parent="' . $parent . '" onclick="GeoTree.gotoEdit(this)">' . $div['title_bng'] . '</a>';
            $row["icon"] = "icon icon-arrow-right";
            $row["children"] = true;
            $row["type"] = "root";
            $data[] = $row;
        }

        if ($request_type !== 'checkbox') {
            /* Add node for new entry */
            $row = array();
            $row["id"] = "node_" . $parent . "_0";
            $row["data-id"] = 0;
            $row["text"] = '<a href="javascript:;" data-parent-node="' . $request_data . '" data-parent="' . $parent . '" data-id="0"  onclick="TreeView.newNode(this)"><i class="icon-plus"></i></a>';
            $row["icon"] = "icon icon-arrow-right";
            $row["children"] = false;
            $row["type"] = "root";
            $data[] = $row;
        }

        $this->response->body(json_encode($data));
        $this->response->type('application/json');
        return $this->response;
    }


}
