<?php

namespace App\Controller;

use Cake\Database\Exception;
use Cake\Datasource\ConnectionManager;
use Cake\Datasource\Exception\MissingDatasourceConfigException;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Event\Event;
use Cake\Network\Exception\ForbiddenException;
use Cake\ORM\TableRegistry;
use Cake\View\CellTrait;
//use App\Utility\Migration;
use Cake\Cache\Cache;

class OfficeManagementController extends ProjapotiController
{

    use CellTrait;

    /*
     * Start: Actual Office Management 
     */

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['loadOriginOffices', 'getOfficeDesignationsAndIdentificationNoByOfficeId', 'autocompleteOfficeDesignation', 'loadOfficeUnits', 'loadOfficeUnitOrganograms','loadOfficeUnitOrganogramsWithName']);
    }

    public function officeManagement()
    {

    }

    public function loadParentOfficesByMinistry()
    {
        $office_ministry_id = $this->request->data['office_ministry_id'];
        $table_instance = TableRegistry::get('Offices');

        $data_items = $table_instance->getOfficesByMinistry($office_ministry_id);

        $this->response->body(json_encode($data_items));
        $this->response->type('application/json');
        return $this->response;
    }

    public function saveOffice()
    {
        $table_instance = TableRegistry::get('Offices');
        $new_entity = $table_instance->newEntity();
        $this->layout = null;
        $response = ['status' => 0, 'message' => 'something wrong'];

        parse_str($this->request->data['formdata'], $this->request->data);

        if (empty($this->request->data['id'])) {
            unset($this->request->data['id']);
        }

        $table_instance_origin = TableRegistry::get('OfficeOrigins');

        $origin_data = $table_instance_origin->get($this->request->data['office_origin_id']);
        $this->request->data['office_ministry_id'] = $origin_data['office_ministry_id'];
        $this->request->data['office_layer_id'] = $origin_data['office_layer_id'];

        if (!isset($this->request->data['id'])) {
            $check_offices = $table_instance->find()->where([
                'office_origin_id' => $this->request->data['office_origin_id'],
                'office_name_bng' => $this->request->data['office_name_bng'],
                'geo_division_id' => $this->request->data['geo_division_id'],
                'geo_district_id' => $this->request->data['geo_district_id'],
                'geo_upazila_id' => $this->request->data['geo_upazila_id'],
                'geo_upazila_id' => $this->request->data['geo_upazila_id'],
            ])->count();
            if ($check_offices > 0) {
                $response['message'] = "এই অফিস ইতোমধ্যে যোগ করা হয়েছে।";
                $this->response->body(json_encode($response));
                goto rtn;
            }
        }

        $this->request->data['reference_code'] = bnToen($this->request->data['reference_code']);
        $this->request->data['digital_nothi_code'] = isset($this->request->data['digital_nothi_code']) ? bnToen($this->request->data['digital_nothi_code']) : '';
        $digital_nothi_code = $this->request->data['digital_nothi_code'];

        if (mb_strlen($digital_nothi_code) != 10) {
            $response['message'] = "ডিজিটাল নথি কোড ৮ ডিজিটের হতে হবে ";
            $this->response->body(json_encode($response));
            goto rtn;
        }

        $entity_with_data = $table_instance->patchEntity($new_entity, $this->request->data);

        if (empty($entity_with_data->office_name_bng) || empty($entity_with_data->office_name_eng) || empty($entity_with_data->geo_division_id) || empty($entity_with_data->geo_division_id)) {
            $response['message'] = ' সব তথ্য সঠিকভাবে দেওয়া হয়নি। ';
            $this->response->body(json_encode($response));
        } else {
            if ($table_instance->save($entity_with_data)) {
                $response['status'] = 1;
                $this->response->body(json_encode($response));
            } else {
                $this->response->body(json_encode($response));
            }
        }
        rtn:
        $this->response->type('application/json');
        return $this->response;
    }

    public function deleteOffice()
    {
        $table_instance = TableRegistry::get('Offices');
        $this->layout = null;
        parse_str($this->request->data['formdata'], $this->request->data);

        if (!empty($this->request->data['id'])) {
            $entity_with_data = $table_instance->get($this->request->data['id']);

            if (!empty($entity_with_data)) {
                $officeUnitTable = TableRegistry::get('OfficeUnits');
                $isLinked = $officeUnitTable->getInfoByOfficeId($this->request->data['id']);
                $isParent = $table_instance->find()->where(['parent_office_id' => $this->request->data['id']])->count();
                if (!$isLinked && !$isParent) {
                    $entity_with_data->active_status = 0;

                    if ($table_instance->save($entity_with_data)) {
                        $this->response->body(json_encode(1));
                    } else {
                        $this->response->body(json_encode(0));
                    }
                } else {
                    $this->response->body(json_encode(0));
                }
            }
        }
        $this->response->type('application/json');
        return $this->response;
    }

    public function deleteOfficeUnits()
    {
        $this->layout = null;
        $office_unit_id = $this->request->data['office_unit_ids'];
        $response = ['status' => 0, 'msg' => 'Something wrong'];
        if (empty($office_unit_id)) {
            $response['msg'] = 'Empty Unit Data';
            goto rtn;
        }

        $table_instance = TableRegistry::get('OfficeUnits');
        $table_de_instance = TableRegistry::get('OfficeUnitOrganograms');
        $employeeOfficesTable = TableRegistry::get('EmployeeOffices');
        $checkoffice = true;

        try {
            $this->switchOffice($this->request->data['office_id'], 'switching_office', 1);
            TableRegistry::remove('DakMovements');
            TableRegistry::remove('NothiMasterMovements');
            TableRegistry::remove('Potrojari');
            $table_dakmove = TableRegistry::get('DakMovements');
            $table_nothimove = TableRegistry::get('NothiMasterMovements');
            $table_potrojari = TableRegistry::get('Potrojari');
        } catch (\Exception $ex) {
            $checkoffice = false;
        } catch (Exception $e) {
            $checkoffice = false;
        }

        $failed = 0;
        $success = 0;
        foreach ($office_unit_id as $key => $ofcunitid) {
            $unit_id_array = explode("_", $ofcunitid);
            $unit_id = $unit_id_array[count($unit_id_array) - 1];
            if ($unit_id[count($unit_id) - 1] == 0) continue;

            $hasExist = $employeeOfficesTable->getCountOfEmployeeOfOfficeUnites(0, $unit_id);
            if (empty($hasExist)) {
                if ($checkoffice) {
                    $hasDak = $table_dakmove->find()->where(["from_office_unit_id" => $unit_id])->orWhere(["to_office_unit_id" => $unit_id])->count();
                    if ($hasDak == 0) {
                        $hasNothi = $table_nothimove->find()->where(["from_office_unit_id" => $unit_id])->orWhere(["to_office_unit_id" => $unit_id])->count();
                        if ($hasNothi == 0) {
                            $hasPotrojari = $table_potrojari->find()->where(["office_unit_id" => $unit_id])->orWhere(["approval_office_unit_id" => $unit_id])->count();
                            if ($hasPotrojari == 0) {
                                $table_data = $table_instance->get($unit_id);
                                $table_instance->delete($table_data);
                                $table_de_instance->deleteAll(['office_unit_id' => $unit_id]);
                                $success++;
                            } else {
                                $failed++;
                            }
                        } else {
                            $failed++;
                        }
                    } else {
                        $failed++;
                    }
                } else {
                    $table_data = $table_instance->get($unit_id);
                    $table_instance->delete($table_data);
                    $table_de_instance->deleteAll(['office_unit_id' => $unit_id]);
                    $success++;
                }
            } else {
                $failed++;
            }
        }
        if ($failed > 0) {
            $response['msg'] = ' কিছু কার্যক্রম বা কর্মকর্তা থাকায় শাখা বাতিল করা সম্ভব নয়। (মোট রিকোয়েস্টঃ ' . ($success + $failed) . ', বাতিল করা সম্ভব হয়েছেঃ ' . $success . ')';
        } else {
            $response = ['status' => 1, 'msg' => ' সবগুলো শাখা বাতিল সম্ভব হয়েছে। '];
        }

        rtn:
        $this->response->body(json_encode($response));
        $this->response->type('application/json');
        return $this->response;
    }

    public function deleteOfficeOrganogram()
    {
        $this->layout = null;
        $office_unit_org_ids = $this->request->data['office_unit_org_ids'];
        $response = ['status' => 0, 'msg' => 'Something wrong'];
        if (empty($office_unit_org_ids)) {
            $response['msg'] = 'Organogram ID not given';
            goto rtn;
        }

        $table_instance = TableRegistry::get('OfficeUnitOrganograms');
        $table_employee_office = TableRegistry::get('EmployeeOffices');

        $checkoffice = true;

        try {
            $this->switchOffice($this->request->data['office_id'], 'switching_office', 1);
            TableRegistry::remove('DakMovements');
            TableRegistry::remove('NothiMasterMovements');
            TableRegistry::remove('Potrojari');
            $table_dakmove = TableRegistry::get('DakUsers');
            $table_nothimove = TableRegistry::get('NothiMasterCurrentUsers');
            $table_potrojari = TableRegistry::get('Potrojari');
        } catch (\Exception $ex) {
            $checkoffice = false;
        } catch (Exception $e) {
            $checkoffice = false;
        }

        $failed = 0;
        $success = 0;
        foreach ($office_unit_org_ids as $key => $unitOrganogramId) {
            $hasEmployee = $table_employee_office->getDesignationInfo($unitOrganogramId, 1);
            if (!empty($hasEmployee)) {
                $failed++;
                continue;
            }
            if ($checkoffice) {
                $hasDak = $table_dakmove->find()->where(["to_officer_designation_id" => $unitOrganogramId, 'dak_category' => 'Inbox'])->count();
//                if ($hasDak > 0) {
//                    continue;
//                }
                $hasNothi = $table_nothimove->find()->where(["office_unit_organogram_id" => $unitOrganogramId])->count();
//                if ($hasNothi > 0) {
//                    continue;
//                }
                $hasPotrojari = $table_potrojari->find()->Where(["OR" =>
                    ["officer_designation_id" => $unitOrganogramId],
                    ["approval_officer_designation_id" => $unitOrganogramId],
                    ["sovapoti_officer_designation_id" => $unitOrganogramId]
                ])->where(['potro_status' => 'Draft'])->count();
            } else {
                $hasDak = 0;
                $hasNothi = 0;
                $hasPotrojari = 0;
            }
            if (empty($hasEmployee) && !($hasDak | $hasNothi | $hasPotrojari)) {
                $table_instance->updateAll(['status'=>0],['id' => $unitOrganogramId]);
                $success++;
            } else {
                $failed++;
                continue;
            }
        }
        if ($failed > 0) {
            $response['msg'] = ' কিছু কার্যক্রম বা কর্মকর্তা থাকায় পদবি বাতিল করা সম্ভব নয়। (মোট রিকোয়েস্টঃ ' . ($success + $failed) . ', বাতিল করা সম্ভব হয়েছেঃ ' . $success . ')';
        } else {
            $response = ['status' => 1, 'msg' => ' সবগুলো পদবি বাতিল সম্ভব হয়েছে। '];
        }
        rtn:
        $this->response->body(json_encode($response));
        $this->response->type('application/json');
        return $this->response;
    }

    public function editOffice($id)
    {
        $this->layout = null;

        $table_instance = TableRegistry::get('Offices');
        $entity = $table_instance->get($id);
        $entity->digital_nothi_code = enTobn($entity->digital_nothi_code);
        $entity->reference_code = enTobn($entity->reference_code);

        if (!empty($entity)) {
            $this->response->body(json_encode($entity));
        } else {
            $this->response->body(json_encode(0));
        }

        $this->response->type('application/json');
        return $this->response;
    }

    /**
     * Start : Office Unit Management
     */
    public function officeUnitManagement()
    {
        $offices = TableRegistry::get('Offices');
        $employee_offices = $this->getCurrentDakSection();

        $officeInformation = array();
        if (!empty($employee_offices)) {
            $officeInformation = $offices->get($employee_offices['office_id']);
            $employee_offices['office_origin_id'] = $officeInformation['office_origin_id'];
        }

        $this->set('employee_offices', $employee_offices);
    }

    public function loadOriginOffices()
    {
        $office_origin_id = $this->request->data['office_origin_id'];
        $table_instance = TableRegistry::get('Offices');

        $data_items = array();
        if (($data_items = Cache::read('origin_office_' . $office_origin_id, 'memcached')) === false) {
            $data_items = $table_instance->getOfficesByOrigin($office_origin_id);

            Cache::write('origin_office_' . $office_origin_id, $data_items, 'memcached');
        }
        $this->response->body(json_encode($data_items));
        $this->response->type('application/json');
        return $this->response;
    }

    public function transferOfficeUnits()
    {
        $origin_unit_node_ids = $this->request->data['origin_unit_ids'];
        $office_id = $this->request->data['office_id'];
        $origin_unit_ids = array();

        foreach ($origin_unit_node_ids as $node_id) {
            $splitted_node_id = explode("_", $node_id);
            $unit_id = $splitted_node_id[count($splitted_node_id) - 1];

            $origin_unit_ids[] = $unit_id;
        }

        $table_instance_origin_unit = TableRegistry::get('OfficeOriginUnits');
        $table_instance_office_unit = TableRegistry::get('OfficeUnits');
        $table_instance_office = TableRegistry::get('Offices');

        $officeinfo = $table_instance_office->get($office_id);

        $ministry_id = $officeinfo['office_ministry_id'];
        $office_layer_id = $officeinfo['office_layer_id'];

        foreach ($origin_unit_ids as $id) {

            $isExisting = $table_instance_office_unit->find()->where(['office_origin_unit_id' => $id, 'office_id' => $office_id, 'active_status' => 1])->first();

            if ($id == 0 && !empty($isExisting)) {
                continue;
            }

            $origin_unit = $table_instance_origin_unit->find()->where(['id' => $id])->first();

            if (empty($origin_unit)) continue;

            $allParents = $table_instance_origin_unit->getOfficeFullInfo($id);

            $parents = explode(',', $allParents[0]);

            asort($parents);

            if ($this->checkParentUnit($parents, $office_id, $ministry_id, $office_layer_id) == false) {
                $this->response->body(json_encode(0));
                $this->response->type('application/json');
                return $this->response;
            }

            if ($this->saveUnit($id, $office_id, $ministry_id, $office_layer_id) == false) {
                $this->response->body(json_encode(0));
                $this->response->type('application/json');
                return $this->response;
            }

        }

        $this->response->body(json_encode(1));
        $this->response->type('application/json');
        return $this->response;
    }

    private function checkParentUnit($parents = array(), $office_id, $ministry_id, $office_layer_id)
    {

        $table_instance_office_unit = TableRegistry::get('OfficeUnits');

        if (!empty($parents)) {

            foreach ($parents as $key => $value) {

                if ($value == 0) continue;
                $isExisting = $table_instance_office_unit->find()->where(['office_origin_unit_id' => $value, 'office_id' => $office_id, 'active_status' => 1])->first();

                if (!empty($isExisting)) {
                    continue;
                }

                if ($this->saveUnit($value, $office_id, $ministry_id, $office_layer_id) == false) {
                    return false;
                }

            }
        }

        return true;
    }

    private function saveUnit($originId, $office_id, $ministry_id, $office_layer_id)
    {
        $table_instance_origin_unit = TableRegistry::get('OfficeOriginUnits');
        $table_instance_office_unit = TableRegistry::get('OfficeUnits');

        try {
            if (!empty($originId)) {

                $office_unit_data = $table_instance_origin_unit->get($originId);

                $isExisting = $table_instance_office_unit->find()->where(['office_origin_unit_id' => $originId, 'office_id' => $office_id, 'active_status' => 1])->first();

                if (!empty($isExisting)) {
                    return true;
                }

                $parentUnit = $table_instance_office_unit->find()->where(['office_origin_unit_id' => $office_unit_data['parent_unit_id'], 'office_id' => $office_id, 'active_status' => 1])->hydrate(false)->first();

                $office_unit = $table_instance_office_unit->newEntity();
                $office_unit->office_id = $office_id;
                $office_unit->office_origin_unit_id = $originId;
                $office_unit->office_ministry_id = $ministry_id;
                $office_unit->office_layer_id = $office_layer_id;
                $office_unit->unit_name_bng = $office_unit_data['unit_name_bng'];
                $office_unit->unit_name_eng = $office_unit_data['unit_name_eng'];
                $office_unit->office_unit_category = $office_unit_data['office_unit_category'];
                $office_unit->parent_origin_unit_id = $office_unit_data['parent_unit_id'];
                $office_unit->unit_level = $office_unit_data['unit_level'];
                $office_unit->parent_unit_id = empty($parentUnit) ? 0 : $parentUnit['id'];
                $office_unit->created_by = $this->Auth->user('id');
                $office_unit->modified_by = $this->Auth->user('id');

                $table_instance_office_unit->save($office_unit);

            }
        } catch (\Exception $ex) {
            echo $ex->getMessage();
            return false;
        }

        return true;

    }

    private function _getSuperiorDesignationId($final_data, $superior_origin_unit_desig_id)
    {
        $superior_data = array();
        foreach ($final_data as $struct) {
            if ($superior_origin_unit_desig_id == $struct->ref_sup_origin_unit_desig_id) {
                $superior_data['designation'] = $struct->id;
                $superior_data['unit'] = $struct->office_unit_id;
                break;
            }
        }
        return $superior_data;
    }

    /**
     * End : Office Unit Management
     */

    /**
     * Start : Office Unit Organogram Management
     */
    public function officeUnitOrganogramManagement()
    {
        $employee_offices = $this->getCurrentDakSection();
        if (!empty($employee_offices['office_id'])) {
            $table_offices = TableRegistry::get('Offices');
            $office_info = $table_offices->get($employee_offices['office_id']);
            $office_info_count = $table_offices->find()->where(['office_origin_id' => $office_info['office_origin_id']])->count();
        }

        $this->set('office_info_count', !empty($office_info_count) ? $office_info_count : 0);
        $this->set('employee_offices', $employee_offices);
    }

    public function transferOrganogram()
    {
        $org_ids = $this->request->data['origin_unit_org_ids'];
        $office_id = $this->request->data['office_id'];

        $table_instance_origin_unit = TableRegistry::get('OfficeOriginUnitOrganograms');
        $table_instance_office_unit = TableRegistry::get('OfficeUnitOrganograms');

        /* Remove Previous Entry */
        //$table_instance_office_unit->deleteAll(['office_id' => $office_id]);

        $final_data = array();
        $err_msg = '';
        foreach ($org_ids as $row) {
            $row_arr = explode(":", $row);
            $office_unit_part = $row_arr[0];
            $origin_org_part = $row_arr[1];

            $office_unit_id = explode("_", $office_unit_part)[1];
            $org_id = explode("_", $origin_org_part)[1];

            $origin_unit_org = $table_instance_origin_unit->get($org_id);

            unset($origin_unit_org['id']);
            unset($origin_unit_org['created']);
            unset($origin_unit_org['modified']);
            unset($origin_unit_org['created_by']);
            unset($origin_unit_org['modified_by']);
            unset($origin_unit_org['office_origin_id']);

            $previous_org_id_exists = $table_instance_office_unit->find()->select(['id', 'ref_origin_unit_org_id', 'designation_bng', 'status'])->where(['office_unit_id' => $office_unit_id, 'ref_origin_unit_org_id' => $org_id])->first();
            if (empty($previous_org_id_exists)) {

                $office_org = $table_instance_office_unit->newEntity();

                $office_org->office_id = $office_id;
                $office_org->office_unit_id = $office_unit_id;
                $office_org->superior_unit_id = 0;
                $office_org->superior_designation_id = 0;
                $office_org->ref_sup_origin_unit_id = $origin_unit_org['superior_unit_id'];
                $office_org->ref_sup_origin_unit_desig_id = $origin_unit_org['superior_designation_id'];
                $office_org->ref_origin_unit_org_id = $org_id;
                $office_org->designation_bng = $origin_unit_org['designation_bng'];
                $office_org->designation_eng = $origin_unit_org['designation_eng'];
                $office_org->designation_level = $origin_unit_org['designation_level'];
                $office_org->designation_sequence = $origin_unit_org['designation_sequence'];
                $office_org->status = 1;
                $office_org->short_name_eng = $origin_unit_org['designation_eng'];
                $office_org->short_name_bng = $origin_unit_org['designation_bng'];
                $office_org->created_by = $this->Auth->user('id');
                $office_org->modified_by = $this->Auth->user('id');

                $final_data[] = $table_instance_office_unit->save($office_org);
            } else {
                if ($previous_org_id_exists->status == 0) {
                    $office_org = $table_instance_office_unit->patchEntity($table_instance_office_unit->newEntity(), $previous_org_id_exists->toArray());

                    $office_org->office_id = $office_id;
                    $office_org->office_unit_id = $office_unit_id;
                    $office_org->superior_unit_id = 0;
                    $office_org->superior_designation_id = 0;
                    $office_org->ref_sup_origin_unit_id = $origin_unit_org['superior_unit_id'];
                    $office_org->ref_sup_origin_unit_desig_id = $origin_unit_org['superior_designation_id'];
                    $office_org->ref_origin_unit_org_id = $org_id;
                    $office_org->designation_bng = $origin_unit_org['designation_bng'];
                    $office_org->designation_eng = $origin_unit_org['designation_eng'];
                    $office_org->designation_level = $origin_unit_org['designation_level'];
                    $office_org->designation_sequence = $origin_unit_org['designation_sequence'];
                    $office_org->status = 1;
                    $office_org->short_name_eng = $origin_unit_org['designation_eng'];
                    $office_org->short_name_bng = $origin_unit_org['designation_bng'];
                    $office_org->modified_by = $this->Auth->user('id');

                    $final_data[] = $table_instance_office_unit->save($office_org);
                } else {
                    $err_msg = $err_msg . $previous_org_id_exists['designation_bng'] . ', ';
                    $this->response->body(json_encode(array('status' => 'error', 'msg' => $err_msg . ' পদবি একই শাখায় পুর্বে ব্যবহার করা হয়েছে।')));
                }
            }
        }

        foreach ($final_data as $office_designation) {
            $superior_data = $this->_getSuperiorDesignationId($final_data, $office_designation['ref_sup_origin_unit_desig_id']);

            $designation = $table_instance_office_unit->get($office_designation['id']);
            $designation->superior_unit_id = $superior_data['unit'];
            $designation->superior_designation_id = $superior_data['designation'];
            $table_instance_office_unit->save($designation);
        }
        if (empty($err_msg)) {
            $this->response->body(json_encode(array('status' => 'success')));
        }
        $this->response->type('application/json');
        return $this->response;
    }

    public function getOriginUnitOrgByOfficeId()
    {
        $office_id = $this->request->data['office_id'];
        $this->layout = null;

        if (empty($office_id)) {
            $this->response->body(json_encode(array()));
            $this->response->type('application/json');
            return $this->response;
        }

        $table_instance_origin_unit = TableRegistry::get('OfficeUnits');
        $table_instance_origin_unit_org = TableRegistry::get('OfficeOriginUnitOrganograms');
        $office_units = $table_instance_origin_unit->find()->where(['office_id' => $office_id, 'active_status' => 1])->order(['unit_level asc, parent_unit_id asc'])->toArray();

        $data_items = array();

        foreach ($office_units as $office_unit) {

            $rowtree = array();
            $parent_id = $rowtree['id'] = "officeunitorg_" . $office_id . '_' . $office_unit['id'];
            $rowtree['parent'] = '#';
            $rowtree['text'] = $office_unit['unit_name_bng'];
            $rowtree["icon"] = "icon icon-control-end";
            $rowtree["type"] = "root";
            $rowtree["children"] = true;
            $data_items[] = $rowtree;

            $org = $table_instance_origin_unit_org->find()->where(['office_origin_unit_id' => $office_unit['office_origin_unit_id'], 'status' => 1])->order(['designation_level asc'])->toArray();
            if (!empty($org)) {
                foreach ($org as $k => $v) {
                    $rowtree = array();
                    $rowtree['id'] = "unitorg_" . $office_id . "_" . $v['id'];
                    $rowtree['parent'] = $parent_id;
                    $rowtree['text'] = '<a class=\"org_checkbox\" data-unit-id=\"' . $office_unit['id'] . '\" id=\"' . $v['id'] . '\">&nbsp;&nbsp;' . $v['designation_bng'] . ' </a>';
                    $rowtree["icon"] = "icon icon-control-end";
                    $rowtree["children"] = true;
                    $data_items[] = $rowtree;
                }
            }

        }

        $this->response->body(json_encode(array($data_items)));
        $this->response->type('application/json');
        return $this->response;
    }

    public function getOfficeDesignationsDataByOfficeId()
    {
        $office_id = $this->request->data['office_id'];
        if (!empty($office_id)) {
            $office_info = TableRegistry::get('Offices')->getBanglaName($office_id);
        }

        $table_instance_unit = TableRegistry::get('OfficeUnits');
        $table_instance_emp_offices = TableRegistry::get('EmployeeOffices');
        $table_instance_emp_records = TableRegistry::get('EmployeeRecords');
        $office_units = $table_instance_unit->find()
            ->where(['OfficeUnits.office_id' => $office_id, 'OfficeUnits.active_status' => 1])
            ->order(['OfficeUnits.unit_level asc', 'OfficeUnits.id asc'])
            ->contain([
                'Designations'
            ])->hydrate(false)->toArray();


        /* Get all active designations for selected office */
        $active_designations = array();
        $active_designation_rows = $table_instance_emp_offices->find()
            ->select(['office_unit_organogram_id',
                'identification_number', "office_id",
                'office_unit_id', 'employee_record_id',
                'EmployeeOffices.id', 'EmployeeRecords.name_bng'])->join([
                'EmployeeRecords' => [
                    'table' => 'employee_records',
                    'type' => 'inner',
                    'conditions' => 'EmployeeRecords.id = EmployeeOffices.employee_record_id'
                ]
            ])->where(['EmployeeOffices.office_id' => $office_id, 'EmployeeOffices.status' => 1])->order(['designation_level ASC, designation_sequence ASC']);

        if (!empty($active_designation_rows)) {
            foreach ($active_designation_rows as $active) {
                $active_designations[$active->office_unit_organogram_id] = $active->office_unit_organogram_id;
                $employee_record_name[$active->office_unit_organogram_id] = $active['EmployeeRecords']['name_bng'];
            }
        }

        $unit_org_ids = array();
        foreach ($office_units as $key => &$row) {
            $data = $row;
            unset($data['designations']);
            if (!empty($row['designations'])) {
                foreach ($row['designations'] as $index => &$value) {
                    if (in_array($value['id'], $active_designations)) {
                        $value['active_status'] = 0;
                        $value['employee'] = $employee_record_name[$value['id']];
                    } else {
                        $value['active_status'] = 1;
                        $emp_data = $table_instance_emp_records->getEmployeeInfoByDesignation($value['id'], 0);
                        $value['employee'] = !empty($emp_data['name_bng']) ? h($emp_data['name_bng']) : '';
                    }
                }
            }
            $unit_org_ids[] = [
                'office_unit' => $data,
                'office' => $office_info,
                'designations' => $row['designations']
            ];
        }

        $this->response->body(json_encode($unit_org_ids));
        $this->response->type('application/json');
        return $this->response;
    }

    public function getOfficeDesignationsByOfficeId()
    {
        $employeeOffice = $this->getCurrentDakSection();
        $office_id = !empty($employeeOffice['office_id']) ? $employeeOffice['office_id'] : $this->request->data['office_id'];
        $needName = !empty($this->request->data['need_name']) ? $this->request->data['need_name'] : 0;
        $this->layout = null;

        if (empty($office_id)) {
            $this->response->body(json_encode(array()));
            $this->response->type('application/json');
            return $this->response;
        }


        $data_items = array();

        $table_instance_unit = TableRegistry::get('OfficeUnits');
        $table_instance_unit_org = TableRegistry::get('OfficeUnitOrganograms');
        $table_instance_emp_offices = TableRegistry::get('EmployeeOffices');
        $office_units = $table_instance_unit->find()->where(['office_id' => $office_id, "active_status" => 1])->order(['unit_level asc, parent_unit_id asc'])->toArray();

        /* Get all active designations for selected office */
        $active_designations = array();
        $active_designation_details = array();
        $active_designation_rows = $table_instance_emp_offices->find()
            ->select(['EmployeeOffices.office_unit_organogram_id', "name_bng" => 'EmployeeRecords.name_bng'])
            ->where(['EmployeeOffices.office_id' => $office_id, 'EmployeeOffices.status' => 1])
            ->join([
                'EmployeeRecords' => [
                    'table' => 'employee_records',
                    'type' => 'INNER',
                    'conditions' => "EmployeeRecords.id=EmployeeOffices.employee_record_id AND EmployeeOffices.status = 1"
                ]
            ])
            ->order(['EmployeeOffices.designation_level asc'])->toArray();

        foreach ($active_designation_rows as $row => $active) {
            $active_designations[] = $active['office_unit_organogram_id'];
            $active_designation_details[$active['office_unit_organogram_id']] = $active['name_bng'];
        }

        $unit_org_ids = array();

        foreach ($office_units as $office_unit) {

            $rowtree = array();
            $parent_id = $rowtree['id'] = "officeunit_" . $office_id . '_' . $office_unit['id'];
            $rowtree['parent'] = '#';
            $rowtree['text'] = $office_unit['unit_name_bng'];
            $rowtree["icon"] = "icon icon-control-end";
            $rowtree["type"] = "root";
            $rowtree["children"] = true;
//            $rowtree["state"] = '"opened":true';
//            $rowtree["a_attr"] = '';
            $data_items[] = $rowtree;

            $org = $table_instance_unit_org->find()->where(['office_unit_id' => $office_unit['id'], "status" => 1])->order(['designation_level asc'])->toArray();
            if (!empty($org)) {
                foreach ($org as $k => $v) {
                    $rowtree = array();
//                    if (in_array($v['id'], $active_designations)) {
//                        $rowtree["state"] = '"disabled":true';
//                        $rowtree["a_attr"] = '"class":"jstree-checked"';
//                    } else {
//                        $rowtree["a_attr"] = '"onclick":"EmployeeAssignment.selectOrganogram(this)"';
//                        $rowtree["state"] = '"disabled":false';
//                    }
                    $rowtree['id'] = "org_" . $office_id . "_" . $v['id'];
                    $rowtree['parent'] = $parent_id;
                    $rowtree['text'] = '<a class=\"org_checkbox\" data-unit-id=\"' . $office_unit['id'] . '\" id=\"' . $v['id'] . '\">&nbsp;&nbsp;' . $v['designation_bng'] . ($needName && isset($active_designation_details [$v['id']]) ? (' (' . $active_designation_details [$v['id']] . ')') : '') . '</a>';
                    $rowtree["icon"] = "icon icon-control-end";
                    $rowtree["children"] = true;
                    $data_items[] = $rowtree;
                }
            }
        }

        $this->response->body(json_encode(array($data_items)));
        $this->response->type('application/json');
        return $this->response;
    }

    /**
     * End : Office Unit Organogram Management
     */
    public function getOfficeInfo()
    {
        $office_id = $this->request->data['office_id'];
        $table_instance = TableRegistry::get('Offices');
        $office = $table_instance->get($office_id);

        $this->response->body(json_encode($office));
        $this->response->type('application/json');
        return $this->response;
    }

    public function loadOfficeUnits()
    {
        $office_id = $this->request->data['office_id'];
        $table_instance = TableRegistry::get('OfficeUnits');

        $select = '<option value=0>--বাছাই করুন--</option>';
        $office_units = $table_instance->getOfficeUnitsList($office_id);
        if (!empty($office_units)):
            foreach ($office_units as $key => $value) {
                $select .= '<option value=' . $key . '>' . $value . '</option>';
            }
        endif;

        echo $select;
        die;
    }

    public function officeUnitsList()
    {
        $office_id = $this->request->data['office_id'];
        $table_instance = TableRegistry::get('OfficeUnits');

        $office_units = $table_instance->getOfficeUnitsList($office_id);
        $this->response->type('json');
        $this->response->body(json_encode($office_units));
        return $this->response;
    }

    public function loadOfficeUnitOrganograms()
    {
        $office_unit_id = $this->request->data['office_unit_id'];
        $table_instance = TableRegistry::get('OfficeUnitOrganograms');

        $office_unit_orgs = array();
        if (($office_unit_orgs = Cache::read('origin_office_unit_org_' . $office_unit_id, 'memcached')) === false) {
            $office_unit_orgs = $table_instance->getUnitOrgByOfficeUnitId($office_unit_id);

            Cache::write('origin_office_unit_org_' . $office_unit_id, $office_unit_orgs, 'memcached');
        }

        $this->response->body(json_encode($office_unit_orgs));
        $this->response->type('application/json');
        return $this->response;
    }

    public function loadOfficeUnitOrganogramsWithName()
    {
        $office_unit_id = $this->request->data['office_unit_id'];
        $table_instance = TableRegistry::get('OfficeUnitOrganograms');

        $office_unit_orgs_with_name = array();
//        if (($office_unit_orgs_with_name = Cache::read('origin_office_unit_org_name' . $office_unit_id, 'memcached')) === false) {
        $office_unit_orgs = $table_instance->getUnitOrgByOfficeUnitId($office_unit_id);
        $employee_office_table = TableRegistry::get('EmployeeOffices');
        $indx = 0;
        foreach ($office_unit_orgs as $organogram_id => $organogram_name) {
            $employee_office = $employee_office_table->find()->contain(['EmployeeRecords'])->where(['EmployeeOffices.office_unit_organogram_id' => $organogram_id, 'EmployeeOffices.status' => 1])->first();
            if ($employee_office) {
                $designation = $organogram_name;
                $name = $employee_office->employee_record->name_bng;
            } else {
                $designation = $organogram_name;
                $name = '(শুন্যপদ)';
            }
            $office_unit_orgs_with_name[$indx]['designation'] = $designation;
            $office_unit_orgs_with_name[$indx]['name'] = $name;
            $office_unit_orgs_with_name[$indx]['id'] = $organogram_id;
            $indx++;
        }
//            Cache::write('origin_office_unit_org_name' . $office_unit_id, $office_unit_orgs_with_name, 'memcached');
//        }

        $this->response->body(json_encode($office_unit_orgs_with_name));
        $this->response->type('application/json');
        return $this->response;
    }

    public function loadUnUsedOfficeUnitOrganograms()
    {
        $office_unit_id = $this->request->data['office_unit_id'];
        $table_instance = TableRegistry::get('OfficeUnitOrganograms');
        $user = $this->getCurrentDakSection();

        $office_unit_orgs = array();
//        if (($office_unit_orgs = Cache::read('origin_office_unit_org_used_'.$office_unit_id, 'memcached')) === false) {
        $office_unit_orgs = $table_instance->getUnusedUnitOrgByOfficeUnitId($office_unit_id, $user['office_id']);

//            Cache::write('origin_office_unit_org_used_'.$office_unit_id, $office_unit_orgs, 'memcached');
//        }


        $this->response->body(json_encode($office_unit_orgs));
        $this->response->type('application/json');
        return $this->response;
    }

    public function autocompleteOfficeName()
    {
        $user = $this->Auth->user();
        $EmployeeOfficesTable = TableRegistry::get('EmployeeOffices');

        $search_key = $this->request->query['search_key'];
        $office_origin_id = $this->request->query['office_origin_id'];

        $table_instance = TableRegistry::get('OfficeUnitOrganograms');
        $table_instance_unit = TableRegistry::get('OfficeUnits');
        $table_instance_office = TableRegistry::get('Offices');
        $office_id = isset($this->request->query['office_id']) ? $this->request->query['office_id'] : "";
        if (!empty($office_id)) {
            if (!empty($user)) {
                $employee_offices = $EmployeeOfficesTable->find()->where(['employee_record_id' => $user['employee_record_id'], 'status' => 1])->toArray();
            } else {
                $employee_offices = [];
            }

            $self_designations = array();
            foreach ($employee_offices as $self) {
                $self_designations[] = $self['office_unit_organogram_id'];
            }
            //Previous Like Operator for searching
//            where('(OfficeUnitOrganograms.designation_bng like "%%%' . $search_key . '%%%" OR EmployeeRecords.name_bng LIKE "%%%' . $search_key . '%%%") AND OfficeUnitOrganograms.office_id =' .$office_id )->where(['OfficeUnitOrganograms.id <>' => $self_designations])->where(['EmployeeOffices.status'=>1]);
            //Previous Like Operator for searching
            $organograms = $table_instance->find()->select([
                'OfficeUnitOrganograms.designation_bng',
                'OfficeUnitOrganograms.designation_eng',
                'OfficeUnitOrganograms.id',
                'OfficeUnitOrganograms.office_id',
                'EmployeeRecords.name_bng',
                'EmployeeRecords.name_eng',
                'EmployeeOffices.office_head',
                'EmployeeOffices.employee_record_id',
                'EmployeeOffices.incharge_label',
                'EmployeeOffices.show_unit',
                'OfficeUnits.id',
                'unit_name_bng' => 'OfficeUnits.unit_name_bng',
                'OfficeUnits.unit_name_eng',
                'Offices.id',
                'office_name_bng' => 'Offices.office_name_bng',
                'Offices.office_name_eng',
                'Offices.office_address',
            ])->where([
                "or" => [
                    'EmployeeRecords.name_bng LIKE' => "$search_key%",
                    'Offices.office_name_bng LIKE' => "$search_key%",
                    'OfficeUnitOrganograms.designation_bng LIKE' => "$search_key%",
                    'CONCAT(designation_bng,", ",office_name_bng) LIKE' => "%{$search_key}%",
                    'CONCAT(designation_bng,", ",unit_name_bng,", ",office_name_bng) LIKE' => "%{$search_key}%",
                ]
            ])->where(['OfficeUnitOrganograms.office_id' => $office_id])
                ->where(['OfficeUnitOrganograms.id <>' => $self_designations])->where(['EmployeeOffices.status' => 1])
                ->join([
                    'Offices' => [
                        'table' => 'offices',
                        'type' => 'inner',
                        'conditions' => 'Offices.id = OfficeUnitOrganograms.office_id'
                    ],
                    'OfficeUnits' => [
                        'table' => 'office_units',
                        'type' => 'inner',
                        'conditions' => 'OfficeUnits.id = OfficeUnitOrganograms.office_unit_id AND OfficeUnits.office_id = OfficeUnitOrganograms.office_id'
                    ],
                    'EmployeeOffices' => [
                        'table' => 'employee_offices',
                        'type' => 'inner',
                        'conditions' => 'EmployeeOffices.office_unit_organogram_id = OfficeUnitOrganograms.id AND EmployeeOffices.office_id = OfficeUnitOrganograms.office_id AND EmployeeOffices.office_id  = ' . $office_id
                    ],
                    'EmployeeRecords' => [
                        'table' => 'employee_records',
                        'type' => 'inner',
                        'conditions' => 'EmployeeOffices.employee_record_id = EmployeeRecords.id'
                    ],
                ])
                ->order(['OfficeUnitOrganograms.designation_level ASC'])->limit(500)->toArray();
        } else {

            $offices = $table_instance_office->find()->where([
                'Offices.office_name_bng LIKE' => "$search_key%",
                'Offices.office_origin_id ' => "$office_origin_id",
            ]);
            if (isset($this->request->query['geo_division_id']) && $this->request->query['geo_division_id'] > 0) {
                $offices = $offices->where(['Offices.geo_division_id' => $this->request->query['geo_division_id']]);
            }
            if (isset($this->request->query['geo_district_id']) && $this->request->query['geo_district_id'] > 0) {
                $offices = $offices->where(['Offices.geo_district_id' => $this->request->query['geo_district_id']]);
            }
            if (isset($this->request->query['geo_upazila_id']) && $this->request->query['geo_upazila_id'] > 0) {
                $offices = $offices->where(['Offices.geo_upazila_id' => $this->request->query['geo_upazila_id']]);
            }
            if (isset($this->request->query['geo_union_id']) && $this->request->query['geo_union_id'] > 0) {
                $offices = $offices->where(['Offices.geo_union_id' => $this->request->query['geo_union_id']]);
            }
            $offices = $offices->order(['Offices.office_name_bng ASC'])->limit(50)->toArray();
        }
        $data_list = array();

        if (!empty($offices)) {
            foreach ($offices as $k => &$org) {
                $row = array();
                $row['id'] = $org['id'];
                $row['office_name'] = $org['office_name_bng'];
                $row['office_name_eng'] = $org['office_name_eng'];
                $row['value'] = $org['office_name_bng']; // . ', ' . $row['office_name_eng'];
                $data_list[] = $row;
            }
        }

        if (count($data_list) == 0) {
            $row = array();
            $row['id'] = 0;
            $row['value'] = "[" . $row['id'] . "]" . "--কোনো তথ্য খুঁজে পাওয়া যায় নি--";
            $data_list[] = $row;
        }
        $this->response->body(json_encode($data_list));
        return $this->response;
    }

    public function autocompleteOfficeDesignation()
    {
        $user = $this->Auth->user();
        $EmployeeOfficesTable = TableRegistry::get('EmployeeOffices');

        $search_key = $this->request->query['search_key'];

        $table_instance = TableRegistry::get('OfficeUnitOrganograms');
        $table_instance_unit = TableRegistry::get('OfficeUnits');
        $table_instance_office = TableRegistry::get('Offices');
        $office_id = isset($this->request->query['office_id']) ? $this->request->query['office_id'] : "";
        if (!empty($office_id)) {
            if (!empty($user)) {
                $employee_offices = $EmployeeOfficesTable->find()->where(['employee_record_id' => $user['employee_record_id'], 'status' => 1])->toArray();
            } else {
                $employee_offices = [];
            }

            $self_designations = array();
            foreach ($employee_offices as $self) {
                $self_designations[] = $self['office_unit_organogram_id'];
            }
            //Previous Like Operator for searching
//            where('(OfficeUnitOrganograms.designation_bng like "%%%' . $search_key . '%%%" OR EmployeeRecords.name_bng LIKE "%%%' . $search_key . '%%%") AND OfficeUnitOrganograms.office_id =' .$office_id )->where(['OfficeUnitOrganograms.id <>' => $self_designations])->where(['EmployeeOffices.status'=>1]);
            //Previous Like Operator for searching
            $organograms = $table_instance->find()->select([
                'OfficeUnitOrganograms.designation_bng',
                'OfficeUnitOrganograms.designation_eng',
                'OfficeUnitOrganograms.id',
                'OfficeUnitOrganograms.office_id',
                'EmployeeRecords.name_bng',
                'EmployeeRecords.name_eng',
                'EmployeeRecords.personal_mobile',
                'EmployeeOffices.office_head',
                'EmployeeOffices.employee_record_id',
                'EmployeeOffices.incharge_label',
                'EmployeeOffices.show_unit',
                'OfficeUnits.id',
                'unit_name_bng' => 'OfficeUnits.unit_name_bng',
                'OfficeUnits.unit_name_eng',
                'Offices.id',
                'office_name_bng' => 'Offices.office_name_bng',
                'Offices.office_name_eng',
                'Offices.office_address',
            ])->where([
                "or" => [
                    'EmployeeRecords.name_bng LIKE' => "$search_key%",
                    'Offices.office_name_bng LIKE' => "$search_key%",
                    'OfficeUnitOrganograms.designation_bng LIKE' => "$search_key%",
                    'CONCAT(designation_bng,", ",office_name_bng) LIKE' => "%{$search_key}%",
                    'CONCAT(designation_bng,", ",unit_name_bng,", ",office_name_bng) LIKE' => "%{$search_key}%",
                ]
            ])->where(['OfficeUnitOrganograms.office_id' => $office_id])
                ->where(['OfficeUnitOrganograms.id <>' => $self_designations])->where(['EmployeeOffices.status' => 1])
                ->join([
                    'Offices' => [
                        'table' => 'offices',
                        'type' => 'inner',
                        'conditions' => 'Offices.id = OfficeUnitOrganograms.office_id'
                    ],
                    'OfficeUnits' => [
                        'table' => 'office_units',
                        'type' => 'inner',
                        'conditions' => 'OfficeUnits.id = OfficeUnitOrganograms.office_unit_id AND OfficeUnits.office_id = OfficeUnitOrganograms.office_id'
                    ],
                    'EmployeeOffices' => [
                        'table' => 'employee_offices',
                        'type' => 'inner',
                        'conditions' => 'EmployeeOffices.office_unit_organogram_id = OfficeUnitOrganograms.id AND EmployeeOffices.office_id = OfficeUnitOrganograms.office_id AND EmployeeOffices.office_id  = ' . $office_id
                    ],
                    'EmployeeRecords' => [
                        'table' => 'employee_records',
                        'type' => 'inner',
                        'conditions' => 'EmployeeOffices.employee_record_id = EmployeeRecords.id'
                    ],
                ])
                ->order(['OfficeUnitOrganograms.designation_level ASC'])->limit(500)->toArray();
        } else {

            $organograms = $table_instance->find()->select([
                'OfficeUnitOrganograms.designation_bng',
                'OfficeUnitOrganograms.designation_eng',
                'OfficeUnitOrganograms.id',
                'OfficeUnitOrganograms.office_id',
                'EmployeeRecords.name_bng',
                'EmployeeRecords.name_eng',
                'EmployeeRecords.personal_mobile',
                'EmployeeOffices.employee_record_id',
                'EmployeeOffices.office_head',
                'EmployeeOffices.incharge_label',
                'EmployeeOffices.show_unit',
                'OfficeUnits.id',
                'unit_name_bng' => 'OfficeUnits.unit_name_bng',
                'OfficeUnits.unit_name_eng',
                'Offices.id',
                'office_name_bng' => 'Offices.office_name_bng',
                'Offices.office_name_eng',
                'Offices.office_address',
            ])
                ->where([
                    "or" => [
                        'EmployeeRecords.name_bng LIKE' => "$search_key%",
                        'Offices.office_name_bng LIKE' => "$search_key%",
                        'OfficeUnitOrganograms.designation_bng LIKE' => "$search_key%",
                        'CONCAT(designation_bng,", ",office_name_bng) LIKE' => "%{$search_key}%",
                        'CONCAT(designation_bng,", ",unit_name_bng,", ",office_name_bng) LIKE' => "%{$search_key}%",
                    ]
                ])
                ->where(['EmployeeOffices.status' => 1])
                ->join([
                    'Offices' => [
                        'table' => 'offices',
                        'type' => 'inner',
                        'conditions' => 'Offices.id = OfficeUnitOrganograms.office_id'
                    ],
                    'OfficeUnits' => [
                        'table' => 'office_units',
                        'type' => 'inner',
                        'conditions' => 'OfficeUnits.id = OfficeUnitOrganograms.office_unit_id AND OfficeUnits.office_id = OfficeUnitOrganograms.office_id'
                    ],
                    'EmployeeOffices' => [
                        'table' => 'employee_offices',
                        'type' => 'inner',
                        'conditions' => 'EmployeeOffices.office_unit_organogram_id = OfficeUnitOrganograms.id AND EmployeeOffices.office_id = OfficeUnitOrganograms.office_id'
                    ],
                    'EmployeeRecords' => [
                        'table' => 'employee_records',
                        'type' => 'inner',
                        'conditions' => 'EmployeeOffices.employee_record_id = EmployeeRecords.id'
                    ],
                ])
                ->order(['OfficeUnitOrganograms.designation_level ASC'])
                ->limit(500)->toArray();
        }
        $data_list = array();

        if (!empty($organograms)) {

            foreach ($organograms as $k => &$org) {
                $row = array();

                $designation_info = designationInfo($org['id']);
				$row['image'] = $designation_info['user_photo'];

                $row['id'] = $org['id'];
                $row['office_head'] = ($org['EmployeeOffices']['office_head'] == 1 ? 1 : (empty($org['EmployeeOffices']['show_unit']) ? 1 : 0));
                $row['address'] = $org['Offices']['office_address'];
                $row['office_name'] = $org['office_name_bng'];
                $row['office_name_eng'] = $org['Offices']['office_name_eng'];
                $row['personal_mobile'] = $org['EmployeeRecords']['personal_mobile'];
                $row['officer_name'] = $org['EmployeeRecords']['name_bng'];
                $row['officer_name_eng'] = $row['name_eng'] = $org['EmployeeRecords']['name_eng'];
                $row['officer_id'] = $org['EmployeeOffices']['employee_record_id'];
                $row['office_id'] = $org['Offices']['id'];
                $row['office_unit_name'] = $org['unit_name_bng'];
                $row['office_unit_name_eng'] = $org['OfficeUnits']['unit_name_eng'];
                $row['office_unit_id'] = $org['OfficeUnits']['id'];
                $row['designation_label'] = $org['designation_bng'] . (!empty($org['EmployeeOffices']['incharge_label']) && $org['Offices']['id'] != 8 ? " ({$org['EmployeeOffices']['incharge_label']})" : '');
                $row['designation_name_eng'] = $org['designation_eng'] . (!empty($org['EmployeeOffices']['incharge_label']) && $org['Offices']['id'] != 8 ? " ({$org['EmployeeOffices']['incharge_label']})" : '');
                $row['value'] = $org['EmployeeRecords']['name_bng'] . ', ' . $row['designation_label'] . ',' . $org['unit_name_bng'] . ',' . $org['office_name_bng'];
                $data_list[] = $row;
            }
        }

        if (count($data_list) == 0) {
            $row = array();
            $row['id'] = 0;
            $row['value'] = "[" . $row['id'] . "]" . "--কোনো তথ্য খুঁজে পাওয়া যায় নি--";
            $data_list[] = $row;
        }
        $this->response->body(json_encode($data_list));
        return $this->response;
    }

    public function getOfficeDesignationsAndIdentificationNoByOfficeId()
    {
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        $office_id = $this->request->data['office_id'];

        $table_instance_unit = TableRegistry::get('OfficeUnits');
        $table_instance_emp_offices = TableRegistry::get('EmployeeOffices');
        $office_units = $table_instance_unit->find()
            ->where(['OfficeUnits.office_id' => $office_id, 'OfficeUnits.active_status' => 1])
            ->order(['OfficeUnits.unit_level asc', 'OfficeUnits.id asc'])
            ->contain([
                'Designations'
            ])->hydrate(false)->toArray();

        /* Get all active designations for selected office */
        $active_designations = array();
        $identification_number = array();
        $employee_record_id = array();
        $active_designation_rows = $table_instance_emp_offices->find()
            ->select(['office_unit_organogram_id',
                'identification_number', "office_id",
                'office_unit_id', 'employee_record_id',
                'EmployeeOffices.id', 'EmployeeRecords.name_bng'])->join([
                'EmployeeRecords' => [
                    'table' => 'employee_records',
                    'type' => 'inner',
                    'conditions' => 'EmployeeRecords.id = EmployeeOffices.employee_record_id'
                ]
            ])->where(['EmployeeOffices.office_id' => $office_id, 'EmployeeOffices.status' => 1])->order(['designation_level ASC, designation_sequence ASC']);

        if (!empty($active_designation_rows)) {
            foreach ($active_designation_rows as $active) {
                $active_designations[$active->office_unit_organogram_id] = $active->office_unit_organogram_id;
                $identification_number[$active->office_unit_organogram_id] = $active->identification_number;
                $employee_record_id[$active->office_unit_organogram_id] = $active->employee_record_id;
                $employee_office_id[$active->office_unit_organogram_id] = $active->id;
                $employee_unit_id[$active->office_unit_organogram_id] = $active->office_unit_id;
                $employee_record_name[$active->office_unit_organogram_id] = $active['EmployeeRecords']['name_bng'];
            }
        }

        $unit_org_ids = array();
        foreach ($office_units as $key => &$row) {
            $data = $row;
            unset($data['designations']);
            if (!empty($row['designations'])) {
                foreach ($row['designations'] as $index => &$value) {
                    if (in_array($value['id'], $active_designations)) {
                        $value['active_status'] = 0;
                        $value['identification_number'] = $identification_number[$value['id']];
                        $value['employee_record_id'] = $employee_record_id[$value['id']];
                        $value['employee_office_id'] = $employee_office_id[$value['id']];
                        $value['employee_unit_id'] = $employee_unit_id[$value['id']];
                        $value['office_unit_organogram_id'] = $active_designations[$value['id']];
                        $value['office_employee_name'] = $employee_record_name[$value['id']];
                    } else {
                        $value['active_status'] = 1;
                    }
                }
            }
            $unit_org_ids[] = [
                'office_unit' => $data,
                'designations' => $row['designations']
            ];
        }

        $this->response->body(json_encode($unit_org_ids));
        $this->response->type('application/json');
        return $this->response;
    }

    /**
     * End : Office Unit Organogram Management
     */
    public function loadOfficeSeal()
    {
        $emplyeeOfficeTable = TableRegistry::get('EmployeeOffices');
        $this->layout = null;
        $params = $this->getCurrentDakSection();
        if (!empty($params)) {
            $table_instance_seal = TableRegistry::get('OfficeDesignationSeals');
            $seal_designations = $table_instance_seal->getSealArray($params);
            $innerHtml = '';

            $si = 0;
            foreach ($seal_designations as $designation) {
                $employee = $emplyeeOfficeTable->getDesignationDetailsInfo($designation->office_unit_organogram_id);
                $innerHtml .= '<tr><td style="width: 30px;text-align: center;">&nbsp;&nbsp;<input type="hidden" name="office-employee" data-office-id="' . $designation->office_id . '"  data-office-employee-name="' . $employee['EmployeeRecords']['name_bng'] . '" data-office-employee-id="' . $employee['employee_record_id'] . '" data-office-unit-id="' . $designation->office_unit_id . '" data-office-unit-organogram-id="' . $designation->office_unit_organogram_id . '" data-designation-name="' . $designation->designation_name_bng . '" data-unit-name="' . $designation->unit_name_bng . '"></td><td><span title="' . $employee['designation_description'] . '">&nbsp;&nbsp;' . $employee['EmployeeRecords']['name_bng'] . ', ' . $designation->designation_name_bng . ', ' . $designation->unit_name_bng . '</span></td></tr>';
            }
            $html = '<div class="row"><table class="table table-bordered"><tbody>' . $innerHtml . '</tbody></table></div>';
        }
        $this->set('post', $html);
    }

    /*toDo :: auto tree with office id of the user*/

    public function loadOfficeUnitOrganogramsTree($parent = 0)
    {
        /* Request Parameters */
        $node_id = $this->request->query['id'];
        $office_id = $this->request->query['office_id'];

        $node_arr = explode(":", $node_id);
        $parent_node = $node_arr[0];
        $root = isset($node_arr[1]) ? $node_arr[1] : "0";
        if ($root == '#') {
            $root = 0;
        }
        $parent_node_arr = explode("_", $parent_node);
        $node_type = $parent_node_arr[0];
        $child_node_prefix = str_replace(':', '_', $node_id);
        $child_node_prefix = str_replace('_unit', '', $child_node_prefix);
        $child_node_prefix = str_replace('_designation', '', $child_node_prefix);

        $data = array();

        /* Check is unit or designation */
        if ($node_type == 'unit') {
            if ($root == 'unit') {
                $child_node_arr = explode("_", $child_node_prefix);
                $parent_unit_id = $child_node_arr[count($child_node_arr) - 1];

                $table_instance = TableRegistry::get('OfficeUnits');
                $data_item_units = $table_instance->find()->select(['id', 'unit_name_bng', 'office_id'])->where(['office_id' => $office_id, 'parent_unit_id' => $parent_unit_id, 'active_status' => 1])->toArray();

                /* Load Existing Data Units From Database */
                foreach ($data_item_units as $unit) {
                    $row = array();
                    $row["id"] = $child_node_prefix . ":" . $unit['id'];
                    $row["data-id"] = $unit['id'];
                    $row["text"] = $unit['unit_name_bng'];
                    $row["icon"] = "icon icon-home";
                    $row["children"] = true;
                    $row["type"] = "root";
                    $data[] = $row;
                }
            } else {
                $row_designation = array();
                $row_designation["id"] = str_replace('unit', 'designation', $child_node_prefix) . ":designation";
                $row_designation["data-id"] = "designation";
                $row_designation["text"] = 'Office Stuffs';
                $row_designation["icon"] = "icon icon-user";
                $row_designation["children"] = true;
                $row_designation["type"] = "root";
                $data[] = $row_designation;

                $row_unit = array();
                $row_unit["id"] = $child_node_prefix . ":unit";
                $row_unit["data-id"] = "unit";
                $row_unit["text"] = 'Doptor/Section';
                $row_unit["icon"] = "icon icon-home";
                $row_unit["children"] = true;
                $row_unit["type"] = "root";
                $data[] = $row_unit;
            }
        } else if ($node_type == 'designation') {
            if ($root == 'designation') {
                $child_node_arr = explode("_", $child_node_prefix);
                $origin_unit_id = $child_node_arr[count($child_node_arr) - 1];

                $table_instance_origin_unit_org = TableRegistry::get('OfficeUnitOrganograms');
                $data_item_designations = $table_instance_origin_unit_org->find()->select(['id', 'designation_bng', 'office_unit_id'])->where(['office_unit_id' => $origin_unit_id, 'status' => 1])->order(['designation_level ASC'])->toArray();

                /* Load Existing Designation From Database */
                foreach ($data_item_designations as $designation) {
                    $row = array();
                    $row["id"] = $child_node_prefix . ":" . $designation['id'];
                    $row["data-id"] = $designation['id'];
                    $row["text"] = '<a href="javascript:;" data-node-id="' . $row["id"] . '" data-id="' . $row["data-id"] . '" >' . $designation['designation_bng'] . '</a>';
                    $row["icon"] = "icon icon-home";
                    $row["children"] = true;
                    $row["type"] = "root";
                    $data[] = $row;
                }
            }
        } else {
            /* Query Data : Unit */
            $table_instance = TableRegistry::get('OfficeUnits');
            $data_item_units = $table_instance->find()->select(['id', 'unit_name_bng', 'office_id'])->where(['office_id' => $office_id, 'parent_unit_id' => $root, 'active_status' => 1])->toArray();

            /* Load Existing Data From Database */
            foreach ($data_item_units as $unit) {
                $row = array();
                $row["id"] = "unit_" . $root . ":" . $unit['id'];
                $row["data-id"] = $unit['id'];
                $row["text"] = '<a href="javascript:;" data-node-id="' . $row["id"] . '" data-id="' . $row["data-id"] . '" >' . $unit['unit_name_bng'] . '</a>';
                $row["icon"] = "icon icon-home";
                $row["children"] = true;
                $row["type"] = "root";
                $data[] = $row;
            }
        }

        $this->response->body(json_encode($data));
        $this->response->type('application/json');
        return $this->response;
    }

    public function loadEmployeeUnitOrganogramsTree($parent = 0)
    {

        $session = $this->request->session();
        $employee = $session->read('logged_employee_record');

        $officeId = 0;
        $officeUnitId = 0;
        $officeUnitOrganogramId = 0;

        if (!empty($employee['office_records'])) {
            $officeId = $employee['office_records'][0]['office_id'];
            $officeUnitId = $employee['office_records'][0]['office_unit_id'];
            $officeUnitOrganogramId = $employee['office_records'][0]['office_unit_organogram_id'];
        }

        /* Request Parameters */
        $node_id = $this->request->query['id'];
        $office_id = $this->request->query['office_id'];
        $icon = $this->request->query['icon'];

        $node_arr = explode(":", $node_id);
        $parent_node = $node_arr[0];
        $root = isset($node_arr[1]) ? $node_arr[1] : "0";
        if ($root == '#') {
            $root = 0;
        }
        $parent_node_arr = explode("_", $parent_node);
        $node_type = $parent_node_arr[0];
        $child_node_prefix = str_replace(':', '_', $node_id);
        $child_node_prefix = str_replace('_unit', '', $child_node_prefix);
        $child_node_prefix = str_replace('_designation', '', $child_node_prefix);

        $data = array();

        /* Check is unit or designation */
        if ($node_type == 'unit') {
            if ($root == 'unit') {
                $child_node_arr = explode("_", $child_node_prefix);
                $parent_unit_id = $child_node_arr[count($child_node_arr) - 1];

                $table_instance = TableRegistry::get('OfficeUnits');
                $data_item_units = $table_instance->find()->select(['id', 'unit_name_bng', 'office_id'])->where(['office_id' => $office_id, 'parent_unit_id' => $parent_unit_id, 'active_status' => 1])->toArray();

                /* Load Existing Data Units From Database */
                if (!empty($data_item_units)) {
                    foreach ($data_item_units as $unit) {
                        $row = array();
                        $row["id"] = $child_node_prefix . ":" . $unit['id'];
                        $row["data-id"] = $unit['id'];
                        $row["text"] = $unit['unit_name_bng'];
                        $row["icon"] = "icon icon-home";
                        $row["children"] = true;
                        $row["type"] = "root";
                        $data[] = $row;
                    }
                }
            } else {
                $row_designation = array();
                $row_designation["id"] = str_replace('unit', 'designation', $child_node_prefix) . ":designation";
                $row_designation["data-id"] = "designation";
                $row_designation["text"] = 'Office Stuffs';
                $row_designation["icon"] = "icon icon-user";
                $row_designation["children"] = true;
                $row_designation["type"] = "root";
                $data[] = $row_designation;

                $row_unit = array();
                $row_unit["id"] = $child_node_prefix . ":unit";
                $row_unit["data-id"] = "unit";
                $row_unit["text"] = 'Doptor/Section';
                $row_unit["icon"] = "icon icon-home";
                $row_unit["children"] = true;
                $row_unit["type"] = "root";
                $data[] = $row_unit;
            }
        } else if ($node_type == 'designation') {
            if ($root == 'designation') {
                $child_node_arr = explode("_", $child_node_prefix);
                $unit_id = $child_node_arr[count($child_node_arr) - 1];

                $table_instance_origin_unit_org = TableRegistry::get('OfficeUnitOrganograms');
                $table_instance_employee_office = TableRegistry::get('EmployeeOffices');
                $table_instance_employee_record = TableRegistry::get('EmployeeRecords');
                $data_item_designations = $table_instance_origin_unit_org->find()->select(['id', 'designation_bng', 'office_unit_id'])->where(['office_unit_id' => $unit_id, 'status' => 1])->order(['designation_level ASC'])->toArray();

                /* Load Existing Designation From Database */
                if (!empty($data_item_designations)) {
                    foreach ($data_item_designations as $designation) {

                        $employees = $table_instance_employee_office->find()->select(['employee_record_id'])->where(['office_unit_id' => $unit_id, 'office_id' => $office_id, 'office_unit_organogram_id' => $designation['id'], 'status' => 1])->toArray();
                        if (!empty($employees)) {
                            foreach ($employees as $k => $employee) {
                                try {
                                    $employeeInfo = $table_instance_employee_record->get($employee['employee_record_id']);
                                } catch (RecordNotFoundException $ex) {
                                    continue;
                                }

                                $row = array();
                                $row["id"] = $child_node_prefix . ":" . $designation['id'] . '_' . $employeeInfo['id'];
                                $row["data-id"] = $designation['id'];
                                $row["text"] = '<a class="employee_info" href="javascript:;" data-employee-record-id=' . $employee['employee_record_id'] . ' data-office-id=' . $office_id . ' data-office-unit-id=' . $unit_id . ' data-node-id="' . $row["id"] . '" data-office-unit-organogram-id=' . $designation['id'] . ' data-id="' . $row["data-id"] . '" >' . (isset($employeeInfo['name_bng']) ? $employeeInfo['name_bng'] : '') . ' (' . $designation['designation_bng'] . ')</a>' . ($icon == 1 ? '<div class="radio-list">
												<label class="radio-inline">
												<div class="radio" ><span ' . ($unit_id != $officeUnitId ? 'class="checked"' : '') . '><input type="radio" class="optionsRadios" name="optionsRadios' . $designation['id'] . '" data-org-id = ' . $designation['id'] . ' data-office-id=' . $office_id . ' data-office-unit-id=' . $unit_id . '  value="0" ' . ($unit_id != $officeUnitId ? 'checked="checked"' : '') . '></span></div> <img class="icons noPermission"></img> </label>
												<label class="radio-inline">
												<div class="radio" ><span class=""><input class="optionsRadios" type="radio" name="optionsRadios' . $designation['id'] . '" data-org-id = ' . $designation['id'] . ' data-office-id=' . $office_id . ' data-office-unit-id=' . $unit_id . '  value="1" ></span></div> <img class="icons allPermission"></img> </label>
												<label class="radio-inline">
												<div class="radio" ><span ' . ($unit_id == $officeUnitId ? 'class="checked"' : '') . '><input type="radio" class="optionsRadios" name="optionsRadios' . $designation['id'] . '" data-org-id = ' . $designation['id'] . ' data-office-id=' . $office_id . ' data-office-unit-id=' . $unit_id . '  value="2" ' . ($unit_id == $officeUnitId ? 'checked="checked"' : '') . '></span></div> <img class="icons canSee"></img> </label>
											</div>' : '');
                                $row["icon"] = "icon icon-home";
                                $row["children"] = true;
                                $row["type"] = "root";
                                $data[] = $row;
                            }

                        }
                    }
                }
            }
        } else {
            /* Query Data : Unit */
            $table_instance = TableRegistry::get('OfficeUnits');
            $data_item_units = $table_instance->find()->select(['id', 'unit_name_bng', 'office_id'])->where(['office_id' => $office_id, 'parent_unit_id' => $root, 'active_status' => 1])->toArray();

            /* Load Existing Data From Database */
            if (!empty($data_item_units)) {
                foreach ($data_item_units as $unit) {
                    $row = array();
                    $row["id"] = "unit_" . $root . ":" . $unit['id'];
                    $row["data-id"] = $unit['id'];
                    $row["text"] = $unit['unit_name_bng'];
                    $row["icon"] = "icon icon-home";
                    $row["children"] = true;
                    $row["type"] = "root";
                    $data[] = $row;
                }
            }
        }

        $this->response->body(json_encode($data));
        $this->response->type('application/json');
        return $this->response;
    }

    public function officeEmployeeTree()
    {

        $session = $this->request->session();
        $employee = $session->read('logged_employee_record');

        $officeId = 0;
        if (!empty($employee['office_records'])) {
            $officeId = $employee['office_records'][0]['office_id'];
        }

        $this->set(compact('officeId'));
    }

    public function officeOrganogramTree()
    {

        $session = $this->request->session();
        $employee = $session->read('logged_employee_record');

        $officeId = 0;
        if (!empty($employee['office_records'])) {
            $officeId = $employee['office_records'][0]['office_id'];
        }

        $this->set(compact('officeId'));
    }


    public function officeSeal($type = 0)
    {
        $this->set(compact('type'));
        $this->layout = null;

        $this->set('currentOffice', $this->getCurrentDakSection());
    }

    public function submitDesignationInSeal()
    {
        $employee_office = $this->getCurrentDakSection();
        $request_data = $this->request->data;

        $table_instance_designation_seal = TableRegistry::get('OfficeDesignationSeals');
        $table_instance_office_unit = TableRegistry::get('OfficeUnits');
        $table_instance_office_unit_organogram = TableRegistry::get('OfficeUnitOrganograms');
        $table_instance_employee_offices = TableRegistry::get('EmployeeOffices');

        $table_instance_designation_seal->deleteAll([
            'office_id' => $request_data['office_id'],
            'seal_owner_designation_id' => $employee_office['office_unit_organogram_id']
        ]);

        for ($i = 0; $i < count($request_data['designation_bng']); $i++) {
            $findDesignationSeq = $table_instance_employee_offices->getDesignationSeq($employee_office['office_id'], $request_data['office_unit_id'][$i], $request_data['office_unit_organogram_id'][$i]);
            $entity = $table_instance_designation_seal->newEntity();
            $entity->office_id = $employee_office['office_id'];
            $entity->seal_owner_unit_id = $employee_office['office_unit_id'];
            $entity->seal_owner_designation_id = $employee_office['office_unit_organogram_id'];
            $entity->office_unit_id = $request_data['office_unit_id'][$i];
            $entity->office_unit_organogram_id = $request_data['office_unit_organogram_id'][$i];
            $entity->designation_level = !empty($findDesignationSeq['designation_level']) ? $findDesignationSeq['designation_level'] : 0;
            $entity->designation_seq = !empty($findDesignationSeq['designation_sequence']) ? $findDesignationSeq['designation_sequence'] : 0;

            $office_unit = $table_instance_office_unit->get($request_data['office_unit_id'][$i]);
            $entity->unit_name_bng = $office_unit['unit_name_bng'];
            $entity->unit_name_eng = $office_unit['unit_name_eng'];

            $office_unit_organogram = $table_instance_office_unit_organogram->get($request_data['office_unit_organogram_id'][$i]);
            $entity->designation_name_eng = $office_unit_organogram['designation_eng'];
            $entity->designation_name_bng = $request_data['designation_bng'][$i];

            $entity->created_by = $this->Auth->user('id');
            $entity->modified_by = $this->Auth->user('id');

            if ($table_instance_designation_seal->save($entity)) {
//                $this->response->body(json_encode(1));
            } else {
//                $this->response->body(json_encode(0));
            }
        }
        $this->response->body(json_encode(1));
        $this->response->type('application/json');
        return $this->response;
    }


    public function reloadOfficeSeal($office_id, $office_unit_id, $sealOf = 0, $sealType = 0)
    {
        $this->layout = null;
        $selected_office_section['office_id'] = $office_id;
        $selected_office_section['office_unit_id'] = $office_unit_id;
        $this->set(compact('selected_office_section', 'sealOf', 'sealType'));
    }

    public function officeSealDelete()
    {

        $employee_offices = $this->getCurrentDakSection();
        $this->layout = null;
        $data = array("status" => 'error', 'msg' => "দুঃখিত! সিল মুছে ফেলা সম্ভব হচ্ছে না");

        if ($this->request->is('ajax')) {
            $table_instance = TableRegistry::get('OfficeDesignationSeals');
            $seals = $table_instance->getDesignationSealArray($employee_offices);
            if ($this->request->is('post')) {
                $id = $this->request->data['id'];
                if (empty($seals)) {
                    $table_instance_unit = TableRegistry::get('OfficeUnitSeals');
                    $sealinfo = $table_instance_unit->find()->where(['seal_owner_unit_id' => $employee_offices['office_unit_id']])->toArray();
                    if (!empty($sealinfo)) {
                        foreach ($sealinfo as $key => $value) {
                            if ($value['id'] == $id) {
                                continue;
                            }
                            $entity = $table_instance->newEntity();
                            $entity->designation_name_bng = $value['designation_name_bng'];
                            $entity->designation_name_eng = $value['designation_name_eng'];
                            $entity->office_id = $value['office_id'];
                            $entity->office_unit_organogram_id = $value['office_unit_organogram_id'];
                            $entity->office_unit_id = $value['office_unit_id'];
                            $entity->unit_name_bng = $value['unit_name_bng'];
                            $entity->unit_name_eng = $value['unit_name_eng'];
                            $entity->seal_owner_unit_id = $value['seal_owner_unit_id'];
                            $entity->seal_owner_designation_id = $employee_offices['office_unit_organogram_id'];
                            $entity->designation_seq = $value['designation_seq'];
                            $entity->designation_level = $value['designation_level'];
                            $entity->created_by = $this->Auth->user('id');
                            $entity->modified_by = $this->Auth->user('id');
                            $table_instance->save($entity);
                        }
                    }
                } else {
                    if ($table_instance->deleteAll(['id' => $id])) {
                        $data = array("status" => 'success');
                    }
                }
            }
        }

        $this->response->body(json_encode($data));
        $this->response->type('application/json');
        return $this->response;
    }


    public function updateOwnOffice($office_id = 0)
    {

//        $employee_info = $this->getLoggedEmployeeRecord();

        $users = $this->getCurrentDakSection();

        $auth_user = $this->Auth->user();
        if ($this->request->is('get')) {
            if (empty($users['office_id']) && $auth_user['user_role_id'] <= 2 && empty($office_id)) {
                $this->set('redirect_url','officeManagement/updateOwnOffice/');
                $this->view = 'update_office_info';
                return;
            }
        }

        $this->set(compact('auth_user'));

        if ($auth_user['user_role_id'] <= 2 && !empty($office_id)) {
            $officeId = $office_id;
        } else {
            $officeId = $users['office_id'];
        }


        $officeTable = TableRegistry::get('Offices');

        $officeInformation = $officeTable->get($officeId);

        if (!empty($this->request->data)) {
            $this->request->data['digital_nothi_code'] = enTobn($this->request->data['digital_nothi_code']);
            $digital_nothi_code = isset($this->request->data['digital_nothi_code']) ? $this->request->data['digital_nothi_code'] : '';
            if (mb_strlen($digital_nothi_code) != 10) {
                $this->Flash->error("ডিজিটাল নথি কোড ৮ ডিজিটের হতে হবে ");
                return $this->redirect(['controller' => 'OfficeManagement', 'action' => 'updateOwnOffice']);
            }

            $officeInformation = $officeTable->patchEntity($officeInformation, $this->request->data);

            if (empty($officeInformation->office_name_bng)) {
                $this->Flash->error("দুঃখিত! অফিসের নাম দেওয়া প্রয়োজন");
            } else if (empty($officeInformation->office_name_eng)) {
                $this->Flash->error("দুঃখিত! অফিসের নাম দেওয়া প্রয়োজন");
            } else if (empty($officeInformation->geo_division_id)) {
                $this->Flash->error("দুঃখিত! বিভাগ দেওয়া হয়নি।");
            } else {

                if ($officeTable->save($officeInformation)) {
                    $this->Flash->success("অফিস তথ্য সংশোধিত হয়েছে");
                } else {
                    $this->Flash->error("দুঃখিত! প্রয়োজনীয় তথ্য দেয়া হয়নি");
                }
            }
            if (empty($office_id)) {
                $this->redirect(['controller' => 'OfficeManagement', 'action' => 'updateOwnOffice']);
            } else {
                $this->redirect(['controller' => 'OfficeManagement', 'action' => 'updateOwnOffice', $office_id]);
            }

        }
        $this->set('entity', $officeInformation);

    }

    public function ownOfficeUnit()
    {
        $users = $this->getCurrentDakSection();

        $officeId = $users['office_id'];

        $this->set('office_id', $officeId);

    }

    public function showOfficeUnit()
    {

        $this->layout = null;
        $users = $this->getCurrentDakSection();

        $officeId = $users['office_id'];

        $officeUnitTable = TableRegistry::get('OfficeUnits');
        $officeUnitCategoryTable = TableRegistry::get('OfficeUnitCategories');

        $this->set('unitCategory', $officeUnitCategoryTable->find('list', ['keyField' => 'category_name_bng', 'valueField' => 'category_name_bng'])->toArray());


        if ($this->request->is('ajax')) {
            if (!empty($this->request->data)) {
                $unit_id = $this->request->data['unit_id'];

                $entity = $officeUnitTable->get($unit_id);
                $officeUnitList = $officeUnitTable->find('list', ['keyField' => 'id', 'valueField' => 'unit_name_bng'])->where(['office_id' => $officeId, 'id <> ' => $unit_id])->toArray();

                $this->set('office_units', $officeUnitList);

                $this->set('entity', $entity);

            }
        }
    }

    public function editOfficeUnit()
    {

        $this->layout = null;
        $users = $this->getCurrentDakSection();

        $officeId = $users['office_id'];

        $officeUnitTable = TableRegistry::get('OfficeUnits');

        $response =[
            'status' => 'error',
            'message' => __('Sorry no data found')
        ];
        if (!empty($this->request->data)) {

            $entity = $officeUnitTable->get($this->request->data['id']);

            //if office mismatch no need save info
            if($officeId != $entity->office_id){
                $response['message'] = __('Unauthorized Access');
                goto rtn ;
            }
            //if office mismatch no need save info

            $entity->office_id = $officeId;
            // unit name can not be changed from this menu.
            unset($this->request->data['unit_name_bng']);

            if (!empty($this->request->data['sarok_no_start'])) {
                $this->request->data['sarok_no_start'] = $this->BngToEng($this->request->data['sarok_no_start']);
            }
            if (!empty($this->request->data['unit_nothi_code'])) {
                $this->request->data['unit_nothi_code'] = $this->BngToEng($this->request->data['unit_nothi_code']);
            }
            if (!empty($this->request->data['phone'])) {
                $this->request->data['phone'] = $this->BngToEng($this->request->data['phone']);
            }
            if (!empty($this->request->data['fax'])) {
                $this->request->data['fax'] = $this->BngToEng($this->request->data['fax']);
            }

            $entity = $officeUnitTable->patchEntity($entity, $this->request->data);

            try {
                $officeUnitTable->save($entity);
                $response =  array(
                    'status' => 'success', 'msg' => 'অফিস শাখা সংশোধন করা হয়েছে'
                );
                Cache::delete('getOfficeUnitsList_' . $officeId, 'memcached');
            } catch (Exception $ex) {
               $response = array(
                   'status' => 'error', 'msg' => 'অফিস শাখা সংশোধন করা সম্ভব হচ্ছে না','reason' => makeEncryptedData($ex->getMessage())
               );
            }
        }
        rtn:
        if ($this->request->is('ajax')) {
            $this->response->type('json');
            $this->response->body(json_encode($response));
            return $this->response;
        }else{
            $this->redirect(['controller' => 'officeEmployees','action' => 'officeUnitNothiCodes']);
        }
    }


    public function officeAdmin()
    {
        $user = $this->Auth->user();

        if ($user['user_role_id'] > 2) {
            $this->redirect(['controller' => 'dashboard', 'action' => 'dashboard']);
        }


    }

    public function AssignOfficeAdmin()
    {
        $userOfficeTable = TableRegistry::get('EmployeeOffices');
        if ($this->request->is('ajax')) {
            if (!empty($this->request->data)) {
                $office_id = intval($this->request->data['office_id']);
                $requested_user_id = intval($this->request->data['requested_user_id']);

                $userOffice = $userOfficeTable->get($requested_user_id);
                if (empty($userOffice)) {
                    echo 'Employee not found';
                    die;
                } elseif ($userOffice->status == 0) {
                    echo 'Employee is not active';
                    die;
                } else {
                    //OfficeUnitOrganograms
                    $OfficeUnitOrganogramsTable = TableRegistry::get('OfficeUnitOrganograms');
                    $OfficeUnitOrganograms = $OfficeUnitOrganogramsTable->get($userOffice->office_unit_organogram_id);

                    $userTable = TableRegistry::get('Users');
                    $userInformation = $userTable->find()->where(['employee_record_id' => $userOffice->employee_record_id])->first();
                    $userInformation->is_admin = 0;
                    $userTable->save($userInformation);

                    $OfficeUnitOrganogramsTable->updateAll(['is_admin' => 0], ['office_id' => $office_id]);
                    $OfficeUnitOrganogramsTable->updateAll(['is_admin' => 1], ['office_id' => $office_id, 'id' => $userOffice->office_unit_organogram_id]);
                    echo "অফিস এডমিন হিসেবে দায়িত্ব দেয়া হয়েছে";
                    die;
                }
                die;

//                $userTable = TableRegistry::get('Users');
//                $userInformation = $userTable->find()->where(['id' => $id])->first();
//                if (!empty($userInformation)) {
//
//                    if ($userTable->updateAll(['is_admin' => $idrole], ['id' => $id])) {
//                        echo 1;
//                        die;
//                    } else {
//                        echo 0;
//                        die;
//                    }
//                }

            }
        }
        echo -1;
        die;
    }


    public function officeDomainSetup()
    {
        $office_domain_table = TableRegistry::get('OfficeDomains');

        $entity = $office_domain_table->newEntity();

        if (!empty($this->request->data) && $this->request->data['office_id']) {
            try {
                $checkExist = $office_domain_table->getDomainbyOffice($this->request->data['office_id']);
                $checkPrefix = $office_domain_table->findByDomainPrefix($this->request->data['domain_prefix'])->where(['office_id <>' => $this->request->data['office_id']])->first();
                if (!empty($checkPrefix)) {
                    throw new \Exception('অফিস প্রিফিক্স ব্যবহার করা হয়েছে');
                }

                if (!empty($checkExist)) {
                    $office_domain_table->deleteAll(['office_id' => $this->request->data['office_id']]);
                }

                $defaultDataSource = \Cake\Datasource\ConnectionManager::get('projapotiDb');
                $default = $defaultDataSource->config();

                $data['office_id'] = intval($this->request->data['office_id']);
                $data['office_db'] = 'projapoti_' . intval($this->request->data['office_id']);
                $data['domain_url'] = !empty($this->request->data['domain_url']) ? trim($this->request->data['domain_url']) : '';
                $data['domain_prefix'] = !empty($this->request->data['domain_prefix']) ? trim($this->request->data['domain_prefix']) : '';
                $data['domain_host'] = !empty($this->request->data['domain_host']) ? trim($this->request->data['domain_host']) : $default['host'];
                $data['domain_username'] = !empty($this->request->data['domain_username']) ? trim($this->request->data['domain_username']) : $default['username'];
                $data['domain_password'] = !empty($this->request->data['domain_password']) ? $this->request->data['domain_password'] : $default['password'];

                $entity = $office_domain_table->patchEntity($entity, $data);

                $errors = $entity->errors();
                if (empty($errors)) {
                    $office_domain_table->save($entity);
                    //$migration = new Migration($entity);
                    $migration = true;

                    if ($migration) {
                        $this->Flash->success("অফিস ডোমেইন সংরক্ষণ করা হয়েছে। অফিস ডাটাবেজ নামঃ " . $entity->office_db);
                    } else {
                        $this->Flash->success("অফিস ডোমেইন সংরক্ষণ করা হয়েছে। অফিস ডাটাবেজ নামঃ " . $entity->office_db . 'but cannot be created');
                    }
                    $this->redirect(['controller' => 'officeManagement', 'action' => 'officeDomainSetup']);

                }
            } catch (Exception $ex) {
                $this->Flash->error($ex);
            } catch (\Exception $e) {
                $this->Flash->error($e->getMessage());
            }
        }

        $this->set('entity', $entity);
    }

    public function getOfficeDomain()
    {

        if ($this->request->is('ajax', 'post')) {
            if (!empty($this->request->data)) {
                $office_domain_table = TableRegistry::get('OfficeDomains');
                $office_id = ($this->request->data['office_id']);

                $checkExist = $office_domain_table->getDomainbyOffice($office_id);

                if (!empty($checkExist)) {
                    $checkExist['status'] = 'success';
                    echo json_encode($checkExist);
                    die;
                }
            }
        }

        echo json_encode(array('status' => 'error'));

        die;
    }

    public function getAllDesignationByOfficeOrUnitID($office_id = 0, $unit_id = 0, $status = 0)
    {
        $query = TableRegistry::get('EmployeeOffices')->find();
        if (!empty($office_id)) {
            $query = $query->where(['office_id' => $office_id]);
        }
        if (!empty($unit_id)) {
            $query = $query->where(['office_unit_id' => $unit_id]);
        }
        if (!empty($status)) {
            $query = $query->where(['status' => $status]);
        }
        $allDesignations = $query->group(['office_unit_organogram_id'])->order(['designation_level ASC', 'designation_sequence ASC'])->toArray();
        $this->response->type('application/json');
        $this->response->body(json_encode($allDesignations));
        return $this->response;
    }
}
