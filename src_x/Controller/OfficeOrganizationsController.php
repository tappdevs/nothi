<?php
namespace App\Controller;

use App\Controller\ProjapotiController;
use App\Model\Table;
use App\Model\Table\OfficeOrganizationsTable;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\Entity;
use App\Model\Entity\OfficeOrganizations;
use Cake\ORM\TableRegistry;

class OfficeOrganizationsController extends ProjapotiController
{
//    public $paginate = [
//        'fields' => ['Districts.id'],
//        'limit' => 25,
//        'order' => [
//            'Districts.id' => 'asc'
//        ]
//    ];
//
//    public function initialize()
//    {
//        parent::initialize();
//        $this->loadComponent('Paginator');
//    }

    public function index()
    {
        $office_organizations = TableRegistry::get('OfficeOrganizations');
        $query = $office_organizations->find('all');
        $this->set(compact('query'));
    }

    public function add()
    {

        $office_organizations = $this->OfficeOrganizations->newEntity();
        if ($this->request->is('post')) {
            $office_organizations = $this->OfficeOrganizations->patchEntity($office_organizations, $this->request->data);
            if ($this->OfficeOrganizations->save($office_organizations)) {
                return $this->redirect(['action' => 'index']);
            }
        }
        $this->set('office_organizations', $office_organizations);

        $office_hierarchy_table = TableRegistry::get('OfficeHierarchyTemplates');
        $office_hierarchys = $office_hierarchy_table->find()->select(['id', 'title_eng', 'title_bng'])->toArray();
        $office_hierarchy_list = array();
        foreach ($office_hierarchys as $oh) {
            $office_hierarchy_list[$oh['id']] = $oh['title_bng'] . "[" . $oh['title_eng'] . "]";
        }

        $this->set('office_hierarchy_list', $office_hierarchy_list);
    }

    public function edit($id = null)
    {
        $office_organizations = $this->OfficeOrganizations->get($id);
        if ($this->request->is(['post', 'put'])) {
            $this->OfficeOrganizations->patchEntity($office_organizations, $this->request->data);
            if ($this->OfficeOrganizations->save($office_organizations)) {
                return $this->redirect(['action' => 'index']);
            }
        }
        $this->set('office_organizations', $office_organizations);
    }


    public function view($id = null)
    {
        $office_organizations = $this->OfficeOrganizations->get($id);
        $this->set(compact('office_organizations'));
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);

        $office_organizations = $this->OfficeOrganizations->get($id);
        if ($this->OfficeOrganizations->delete($office_organizations)) {
            return $this->redirect(['action' => 'index']);
        }
    }


    public function newOffice()
    {
        $office_hierarchy_table = TableRegistry::get('OfficeHierarchyTemplates');
        $office_hierarchys = $office_hierarchy_table->find()->select(['id', 'title_eng', 'title_bng'])->toArray();
        $office_hierarchy_list = array();
        foreach ($office_hierarchys as $oh) {
            $office_hierarchy_list[$oh['id']] = $oh['title_bng'] . "[" . $oh['title_eng'] . "]";
        }

        $this->set('office_hierarchy_list', $office_hierarchy_list);
    }

}
