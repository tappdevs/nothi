<?php
namespace App\Controller;

use App\Controller\ProjapotiController;
use App\Model\Table;
use App\Model\Table\OfficeOrganogramsTable;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\Entity;
use App\Model\Entity\OfficeOrganograms;
use Cake\ORM\TableRegistry;

class OfficeOrganogramsController extends ProjapotiController
{
//    public $paginate = [
//        'fields' => ['Districts.id'],
//        'limit' => 25,
//        'order' => [
//            'Districts.id' => 'asc'
//        ]
//    ];
//
//    public function initialize()
//    {
//        parent::initialize();
//        $this->loadComponent('Paginator');
//    }

    public function index()
    {
        $office_oraganograms = TableRegistry::get('OfficeOrganograms');
        $query = $office_oraganograms->find('all');
        $this->set(compact('query'));
    }

    public function add()
    {
        $this->loadModel('OfficeOrganizations');
        $office_organizations = $this->OfficeOrganizations->find('all');
        $this->set(compact('office_organizations'));

        $office_oraganograms = $this->OfficeOrganograms->newEntity();
        if ($this->request->is('post')) {
            $office_oraganograms = $this->OfficeOrganograms->patchEntity($office_oraganograms, $this->request->data);
            if ($this->OfficeOrganograms->save($office_oraganograms)) {
                return $this->redirect(['action' => 'index']);
            }
        }
        $this->set('office_oraganograms', $office_oraganograms);
    }

    public function edit($id = null)
    {
        $office_oraganograms = $this->OfficeOrganograms->get($id);
        if ($this->request->is(['post', 'put'])) {
            $this->OfficeOrganograms->patchEntity($office_oraganograms, $this->request->data);
            if ($this->OfficeOrganograms->save($office_oraganograms)) {
                return $this->redirect(['action' => 'index']);
            }
        }
        $this->set('office_oraganograms', $office_oraganograms);
    }

    public function view($id = null)
    {
        $office_oraganograms = $this->OfficeOrganograms->get($id);
        $this->set(compact('office_oraganograms'));
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);

        $office_oraganograms = $this->OfficeOrganograms->get($id);
        if ($this->OfficeOrganograms->delete($office_oraganograms)) {
            return $this->redirect(['action' => 'index']);
        }
    }

    /**
     * Office Organogram using office organization id
     * Tree View
     * Request Params: office organization id
     */
    public function loadOfficeOrganogramData()
    {
        $node_id = $this->request->query['id'];
        $tree_type = $this->request->query['type'];
        $org_id = $this->request->query['office_organization_id'];

        if ($node_id == '#') {
            $node_id = 'node_0_0';
        }

        $request_arr = explode("_", $node_id);
        foreach ($request_arr as $arr) {
            if (intval($arr) > 0) {
                continue;
            }
        }
        $parent = $request_arr[count($request_arr) - 1];

        $OfcOrgTable = TableRegistry::get('OfficeOrganograms');
        $OfcOrgData = $OfcOrgTable->find()->select(['id', 'title_eng', 'title_bng'])->where(['parent' => $parent, 'office_organization_id' => $org_id])->order(['sequence'])->toArray();

        $data = array();

        /* Load Existing Data From Database*/
        foreach ($OfcOrgData as $div) {
            $row = array();
            $row["id"] = "node_" . $parent . "_" . $div['id'];
            $row["data-id"] = $div['id'];
            $row["text"] = '<a href="javascript:;" data-parent-node="' . $node_id . '" data-id="' . $div['id'] . '" data-parent="' . $parent . '" onclick="GeoTree.gotoEdit(this)">' . $div['title_bng'] . '</a>';
            $row["icon"] = "icon icon-arrow-right";
            $row["children"] = true;
            $row["type"] = "root";
            $data[] = $row;
        }

        if ($tree_type != 'checkbox') {
            /* Add node for new entry */
            $row = array();
            $row["id"] = "node_" . $parent . "_0";
            $row["data-id"] = 0;
            $row["text"] = '<a href="javascript:;" data-parent-node="' . $node_id . '" data-parent="' . $parent . '" data-id="0"  onclick="TreeView.newNode(this)"><i class="icon-plus"></i></a>';
            $row["icon"] = "icon icon-arrow-right";
            $row["children"] = false;
            $row["type"] = "root";
            $data[] = $row;
        }

        $this->response->body(json_encode($data));
        $this->response->type('application/json');
        return $this->response;
    }
}
