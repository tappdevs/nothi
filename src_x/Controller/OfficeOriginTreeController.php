<?php

namespace App\Controller;

use App\Controller\ProjapotiController;
use App\Model\Table;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\Log\Log;

class OfficeOriginTreeController extends ProjapotiController
{
    /*
     * |-----Tree Start Node-----|
     */

    public function loadRootNode()
    {
        $row = array();
        $row["id"] = "node_0_0_0"; //layer-ministry-id-superior-layer-id
        $row["text"] = '';
        $row["icon"] = "fa fa-tree";
        $row["children"] = true;
        $row["type"] = "root";
        $data[] = $row;

        $this->response->body(json_encode($data));
        $this->response->type('application/json');
        return $this->response;
    }

    /*
     * |-----END-----|
     */

    /*
     * |-----Office Layer Tree-----|
     */

    public function loadOfficeLayers()
    {
        $ministry_id = $this->request->query['office_ministry_id'];
        if (empty($ministry_id)) {
            $this->response->body(json_encode(array()));
            $this->response->type('application/json');
            return $this->response;
        }

        $node_id = $this->request->query['id'];
        $tree_type = $this->request->query['type']; // data tree, selection tree

        $parent_layer_arr = explode('_', $node_id);
        $parent_layer_id = $parent_layer_arr[count($parent_layer_arr) - 1];

        $table_instance = TableRegistry::get('OfficeLayers');
        $data_items = $table_instance->find()->select(['id', 'layer_name_bng'])->where(['office_ministry_id' => $ministry_id, 'parent_layer_id' => $parent_layer_id, 'active_status' => 1])->order(['layer_sequence'])->toArray();

        $data = array();

        /* Load Existing Data From Database */
        foreach ($data_items as $div) {
            $id = 'layer_' . $ministry_id . '_' . $parent_layer_id . '_' . $div['id'];
            $row = array();
            $row["id"] = $id;
            $row["text"] = '<a href="javascript:;" data-id="' . $row["id"] . '" onclick="LayerSetup.gotoEdit(this)">' . $div['layer_name_bng'] . '</a>';
            $row["icon"] = "icon icon-control-end";
            $row["children"] = true;
            $row["type"] = "root";
            $data[] = $row;
        }

        $this->response->body(json_encode($data));
        $this->response->type('application/json');
        return $this->response;
    }

    /*
     * Load Office Origin Tree 
     * Ministry Wise
     */

    public function loadOfficeOriginTree()
    {
        $ministry_id = $this->request->query['office_ministry_id'];
        if (empty($ministry_id)) {
            $this->response->body(json_encode(array()));
            $this->response->type('application/json');
            return $this->response;
        }

        $node_id = $this->request->query['id'];
        $tree_type = $this->request->query['type']; // data tree, selection tree

        $parent_origin_arr = explode('_', $node_id);
        $parent_origin_id = $parent_origin_arr[count($parent_origin_arr) - 1];

        $table_instance = TableRegistry::get('OfficeOrigins');
        $data_items = $table_instance->find()->select(['id', 'office_name_bng'])->where(['office_ministry_id' => $ministry_id, 'parent_office_id' => $parent_origin_id])->order(['office_sequence'])->toArray();

        $data = array();

        /* Load Existing Data From Database */
        foreach ($data_items as $div) {
            $id = 'origin_' . $ministry_id . '_' . $parent_origin_id . '_' . $div['id'];
            $row = array();
            $row["id"] = $id;
            $row["text"] = '<a href="javascript:;" data-id="' . $row["id"] . '" >' . $div['office_name_bng'] . '</a>';
            $row["icon"] = "icon icon-control-end";
            $row["children"] = true;
            $row["type"] = "root";
            $data[] = $row;
        }

        $this->response->body(json_encode($data));
        $this->response->type('application/json');
        return $this->response;
    }

    //new
    public function loadOfficeTree()
    {
        $this->layout = null;
//        $ministry_id = $this->request->query['office_ministry_id'] = 6;
        $ministry_id = 5;

        $data_items = array();
        $table_ministries = TableRegistry::get('OfficeMinistries');


        $office_tree = $table_ministries->getFullTree($ministry_id);

        $officeLayersArray = array();
        if (!empty($office_tree)) {
            foreach ($office_tree as $key => $value) {
                if (empty($value['OfficeLayers']['id']))
                    continue;
//                if($value['OfficeLayers']['parent_layer_id']==0){
                $officeLayersArray[$value['OfficeLayers']['id']] = $value['OfficeLayers'];
//                }else{
//                    $officeLayersArray["child"][$value['OfficeLayers']['parent_layer_id']][$value['OfficeLayers']['id']] = $value['OfficeLayers'];
//                }
            }
        }

        $officeOriginsArray = array();
        if (!empty($office_tree)) {
            foreach ($office_tree as $key => $value) {
                if (empty($value['OfficeOrigins']['id']))
                    continue;
//                if($value['OfficeOrigins']['parent_office_id']==0)
                $officeOriginsArray[$value['OfficeOrigins']['id']] = $value['OfficeOrigins'];
//                else
//                    $officeOriginsArray["child"][$value['OfficeOrigins']['parent_office_id']][$value['OfficeOrigins']['id']]  = $value['OfficeOrigins'];
            }
        }

        $officeOriginUnitsArray = array();
        if (!empty($office_tree)) {
            foreach ($office_tree as $key => $value) {
                if (empty($value['OfficeOriginUnits']['id']))
                    continue;
//                if($value['OfficeOriginUnits']['parent_unit_id']==0)
                $officeOriginUnitsArray[$value['OfficeOriginUnits']['id']] = $value['OfficeOriginUnits'];
//                else
//                    $officeOriginUnitsArray["child"][$value['OfficeOriginUnits']['parent_unit_id']][$value['OfficeOriginUnits']['id']]  = $value['OfficeOriginUnits'];
            }
        }

        $officeOriginUnitOrganogramsArray = array();
        if (!empty($office_tree)) {
            foreach ($office_tree as $key => $value) {
                if (empty($value['OfficeOriginUnitOrganograms']['id']))
                    continue;
//                if($value['OfficeOriginUnitOrganograms']['superior_designation_id']==0)
                $officeOriginUnitOrganogramsArray[$value['OfficeOriginUnitOrganograms']['id']] = $value['OfficeOriginUnitOrganograms'];
//                else
//                    $officeOriginUnitsArray["child"][$value['OfficeOriginUnitOrganograms']['superior_designation_id']][$value['OfficeOriginUnitOrganograms']['id']]  = $value['OfficeOriginUnitOrganograms'];
            }
        }


        $final_tree = array();

        $finalLayerTree = array();
        $finalOriginTree = array();
        $finalOriginUnitTree = array();
        $finalOriginUnitOrganogramTree = array();

        if (!empty($officeOriginUnitOrganogramsArray)) {
            foreach ($officeOriginUnitOrganogramsArray as $originUnitOrgranogramKey => $originUnitOrganogramValue) {
                $finalOriginUnitOrganogramTree[$originUnitOrganogramValue['id']]["office_origin_unit_id"] = $originUnitOrganogramValue['office_origin_unit_id'];
                $finalOriginUnitOrganogramTree[$originUnitOrganogramValue['id']]["name"] = $originUnitOrganogramValue['designation_eng'];

                $finalOriginUnitTree[$originUnitOrganogramValue['office_origin_unit_id']]['children'][] = $finalOriginUnitOrganogramTree[$originUnitOrganogramValue['id']];
            }
        }

        if (!empty($officeOriginUnitsArray)) {
            foreach ($officeOriginUnitsArray as $originUnitKey => $originUnitValue) {
                $finalOriginUnitTree[$originUnitValue['id']]["office_origin_id"] = $originUnitValue['office_origin_id'];
                $finalOriginUnitTree[$originUnitValue['id']]["name"] = $originUnitValue['unit_name_eng'];

                $finalOriginTree[$originUnitValue['office_origin_id']]['children'][] = $finalOriginUnitTree[$originUnitValue['id']];
            }
        }


        foreach ($officeOriginsArray as $originKey => $originValue) {
            $finalOriginTree[$originValue['id']]["office_layer_id"] = $originValue['office_layer_id'];
            $finalOriginTree[$originValue['id']]["name"] = $originValue['office_name_eng'];

            $finalLayerTree[$originValue['office_layer_id']]['children'][] = $finalOriginTree[$originValue['id']];
        }

        foreach ($officeLayersArray as $layerKey => $layerValue) {
            $finalLayerTree[$layerValue['id']]["name"] = $layerValue['layer_name_eng'];
        }


        $office_tree_main = array(
            "name" => $office_tree[0]['name_eng'],
            "children" => array_values($finalLayerTree)
        );

        echo json_encode($office_tree_main);
        die;
    }

    public function loadTree()
    {
        $this->render('/OfficeManagement/office_full_tree');
    }

    private function __findLayerChild($layerArray, $parentId)
    {

        if ($parentId == 0)
            return;
        $res = array();
        foreach ($layerArray as $key => $val) {
            if ($val['parent_layer_id'] == $parentId) {
                $res[] = array(
                    "name" => $val['layer_name_eng'],
                    "children" => $this->__findLayerChild($layerArray, $val['id'])
                );
            }
        }

        return $res;
    }

    private function __findOriginChild($originArray, $parentId)
    {

        if ($parentId == 0)
            return;
        $res = array();
        foreach ($originArray as $key => $val) {
            if ($val['parent_office_id'] == $parentId) {
                $res[] = array(
                    "name" => $val['office_name_eng'],
                    "children" => $this->__findOriginChild($originArray, $val['id'])
                );
            }
        }

        return $res;
    }

    private function __findOriginUnitChild($officeOriginUnitsArray, $parentId)
    {

        if ($parentId == 0)
            return;
        $res = array();
        foreach ($officeOriginUnitsArray as $key => $val) {
            if ($val['parent_unit_id'] == $parentId) {
                $res[] = array(
                    "name" => $val['unit_name_eng'],
                    "children" => $this->__findOriginUnitChild($officeOriginUnitsArray, $val['id'])
                );
            }
        }

        return $res;
    }

    private function __findOriginUnitOrgranogramChild($officeOriginUnitOrganogramsArray, $parentId)
    {

        if ($parentId == 0)
            return;
        $res = array();
        foreach ($officeOriginUnitOrganogramsArray as $key => $val) {
            if ($val['superior_designation_id'] == $parentId) {
                $res[] = array(
                    "name" => $val['designation_eng'],
                    "children" => $this->__findOriginUnitOrgranogramChild($officeOriginUnitOrganogramsArray, $val['id'])
                );
            }
        }

        return $res;
    }


    public function loadOfficeTreeDataNew()
    {
        $this->layout = null;
        $ministry_id = $this->request->data['office_ministry_id'];
        if (empty($ministry_id)) {
            $this->response->body(json_encode(array()));
            $this->response->type('application/json');
            return $this->response;
        }

        $node_id = $this->request->data['parent_office_id'][0];

        $parent_origin_arr = explode('_', $node_id);
        $parent_office_id = $parent_origin_arr[count($parent_origin_arr) - 1];

        $data_items = array();
        $table_instance_office = TableRegistry::get('Offices');
        $all_offices = $table_instance_office->find()
            ->select(['Offices.id', 'Offices.office_name_bng', 'Offices.parent_office_id']);

        if(!empty($parent_office_id)){
            $all_offices->where(['Offices.office_ministry_id' => $ministry_id, 'Offices.office_origin_id' => $parent_office_id, 'Offices.active_status' => 1]);
        }else{
            $all_offices->where(['Offices.office_ministry_id' => $ministry_id, 'Offices.active_status' => 1]);
        }

        $all_offices->order(['Offices.parent_office_id'])->toArray();

        $id = 'office_' . $ministry_id . '_0';
        $row = array();
        $row["id"] = $id;
        $row["text"] = '<a href=\"javascript:;\" data-id=\"' . $ministry_id . '\"><i class=\"fa fa-tree\"></i></a>';
        $row["icon"] = "icon icon-control-end";
        $row["children"] = true;
        $row["type"] = "root";
        $row["parent"] = '#';

        $parents = [];

        $data_items[] = $row;
        if (!empty($all_offices)) {
            foreach ($all_offices as $office) {
                $parent_off = $table_instance_office->get($office['parent_office_id']);
                $id = 'office_' . $ministry_id . '_' . $office['id'];
                $row = array();
                $row["id"] = $id;
                $row["text"] = '<a href=\"javascript:;\" data-id=\"' . $row["id"] . '\" onclick=\"OfficeManagement.enableEditMode(this)\">' . $office['office_name_bng'] . '</a>';
                $row["icon"] = "icon icon-control-end";
                $row["children"] = true;
                $row["type"] = "root";
                if(empty($parent_off)){
                    $row["parent"] = 'office_' . $ministry_id . '_0' ;
                }else{
                    $row["parent"] = 'office_' . $ministry_id . '_' . $office['parent_office_id'];

                    if(!in_array($office['parent_office_id'], $parents)){
                        $parents[] = $office['parent_office_id'];
                        $parent_off_info = $table_instance_office->get($office['parent_office_id']);
                        if(empty($parent_off_info)){
                            $id = 'office_' . $ministry_id . '_0';
                            $rowP = array();
                            $rowP["id"] = $id;
                            $rowP["text"] = '<a href=\"javascript:;\" data-id=\"' . $rowP["id"] . '\" onclick=\"OfficeManagement.enableEditMode(this)\">' . '...' . '</a>';
                            $rowP["icon"] = "icon icon-control-end";
                            $rowP["children"] = true;
                            $rowP["type"] = "root";
                            $rowP["parent"] = 'office_' . $ministry_id . '_0' ;
                            $data_items[] = $rowP;
                        }else{
                            $id = 'office_' . $ministry_id . '_' . $parent_off_info['id'];
                            $rowP = array();
                            $rowP["id"] = $id;
                            $rowP["text"] = '<a href=\"javascript:;\" data-id=\"' . $rowP["id"] . '\" onclick=\"OfficeManagement.enableEditMode(this)\">' . $parent_off_info['office_name_bng'] . '</a>';
                            $rowP["icon"] = "icon icon-control-end";
                            $rowP["children"] = true;
                            $rowP["type"] = "root";
                            $rowP["parent"] = 'office_' . $ministry_id . '_0' ;
                            $data_items[] = $rowP;
                        }

                    }

                }

                $data_items[] = $row;
            }
        }


        $this->set('tree_nodes', $data_items);
        $this->render('/OfficeManagement/office_tree');
    }

    public function loadOfficeTreeData()
    {
        $this->layout = null;
        $ministry_id = $this->request->data['office_ministry_id'];
        if (empty($ministry_id)) {
            $this->response->body(json_encode(array()));
            $this->response->type('application/json');
            return $this->response;
        }

        $parent_office_id = $this->request->data['parent_office_id'];

        $data_items = array();
        $table_instance_office = TableRegistry::get('Offices');
        $all_offices = $table_instance_office->find()
            ->select(['Offices.id', 'Offices.office_name_bng', 'Offices.parent_office_id'])
            ->where(['Offices.office_ministry_id' => $ministry_id, 'Offices.parent_office_id <= ' => $parent_office_id, 'Offices.active_status' => 1])
            ->order(['Offices.parent_office_id'])
            ->toArray();

        $id = 'office_' . $ministry_id . '_0';
        $row = array();
        $row["id"] = $id;
        $row["text"] = '<a href=\"javascript:;\" data-id=\"' . $row["id"] . '\"><i class=\"fa fa-tree\"></i></a>';
        $row["icon"] = "icon icon-control-end";
        $row["children"] = true;
        $row["type"] = "root";
        $row["parent"] = '#';
        $data_items[] = $row;
        if (!empty($all_offices)) {
            foreach ($all_offices as $office) {
                $id = 'office_' . $ministry_id . '_' . $office['id'];
                $row = array();
                $row["id"] = $id;
                $row["text"] = '<a href=\"javascript:;\" data-id=\"' . $row["id"] . '\" onclick=\"OfficeManagement.enableEditMode(this)\">' . $office['office_name_bng'] . '</a>';
                $row["icon"] = "icon icon-control-end";
                $row["children"] = true;
                $row["type"] = "root";
                    $row["parent"] = 'office_' . $ministry_id . '_' . $office['parent_office_id'];
                $data_items[] = $row;
            }
        }

        $this->set('tree_nodes', $data_items);
        $this->render('/OfficeManagement/office_tree');
    }

    public function populateFullOriginUnitTree()
    {
        $this->layout = null;
        $office_origin_id = $this->request->data['office_origin_id'];
        if (empty($office_origin_id)) {
            $this->response->body(json_encode(array()));
            $this->response->type('application/json');
            return $this->response;
        }

        $data_items = array();
        $table_instance_office_unit = TableRegistry::get('OfficeOriginUnits');
        $datam = $table_instance_office_unit->find()
            ->select(['id', 'unit_name_bng', 'parent_unit_id'])
            ->where(['office_origin_id' => $office_origin_id])
            ->where(['active_status'=>1])
            ->order(['unit_level asc, parent_unit_id asc'])
            ->toArray();


        $id = 'officeunit_' . $office_origin_id . '_0';
        $row = array();
        $row["id"] = $id;
        $row["text"] = '<a href=\"javascript:;\" data-id=\"' . $row["id"] . '\"><i class=\"fa fa-tree\"></i></a>';
        $row["icon"] = "icon icon-control-end";
        $row["children"] = true;
        $row["type"] = "root";
        $row["parent"] = '#';
        $data_items[] = $row;

        $ids = [];
        $ids[] = 0;
        $parents = [];

        if(!empty($datam)) {
            foreach ($datam as $officeUnit) {
                $ids[] = $officeUnit['id'];
            }

            foreach ($datam as $officeUnit) {
                $id = 'officeunit_' . $office_origin_id . '_' . $officeUnit['id'];
                $row = array();
                $row["id"] = $id;
                $row["text"] = '<a href=\"javascript:;\" data-id=\"' . $row["id"] . '\" onclick=\"OfficeManagement.enableEditMode(this)\">' . $officeUnit['unit_name_bng'] . '</a>';
                $row["icon"] = "icon icon-control-end";
                $row["type"] = "root";
                $row["children"] = true;

                if(in_array($officeUnit['parent_unit_id'],$ids)){
                    $row["parent"] = 'officeunit_' . $office_origin_id . '_' . $officeUnit['parent_unit_id'];
                }else{
                    $row["parent"] = 'officeunit_' . $office_origin_id . '_0';
                }
                $data_items[] = $row;
            }
        }

        $this->response->body(json_encode(array($data_items)));
        $this->response->type('application/json');
        return $this->response;
    }

    public function populateFullOfficeUnitTree()
    {
        $this->layout = null;
        $office_id = $this->request->data['office_id'];
        if (empty($office_id)) {
            $this->response->body(json_encode(array()));
            $this->response->type('application/json');
            return $this->response;
        }

        $data_items = array();
        $table_instance_office_unit = TableRegistry::get('OfficeUnits');
        $datam = $table_instance_office_unit->find()
            ->select(['id', 'unit_name_bng', 'parent_unit_id'])
            ->where(['office_id' => $office_id, 'active_status' => 1])
            ->order(['unit_level asc, parent_unit_id asc'])
            ->toArray();

        $id = 'office_' . $office_id . '_0';
        $row = array();
        $row["id"] = $id;
        $row["text"] = '<a href=\"javascript:;\" data-id=\"' . $row["id"] . '\"><i class=\"fa fa-tree\"></i></a>';
        $row["icon"] = "icon icon-control-end";
        $row["children"] = true;
        $row["type"] = "root";
        $row["parent"] = '#';
        $data_items[] = $row;

        foreach ($datam as $officeUnit) {
            if(!empty($officeUnit['parent_unit_id'])){
             $parent_data =   $table_instance_office_unit->find()->where(['id' => $officeUnit['parent_unit_id']])->first();
             if(empty($parent_data)){
                 $officeUnit['parent_unit_id'] = 0;
             }
            }
            $id = 'office_' . $office_id . '_' . $officeUnit['id'];
            $row = array();
            $row["id"] = $id;
            $row["text"] = '<a href=\"javascript:;\" data-id=\"' . $row["id"] . '\" onclick=\"OfficeManagement.enableEditMode(this)\">' . $officeUnit['unit_name_bng'] . '</a>';
            $row["icon"] = "icon icon-control-end";
            $row["children"] = true;
            $row["type"] = "root";
            $row["parent"] = 'office_' . $office_id . '_' . $officeUnit['parent_unit_id'];
            $data_items[] = $row;
        }

        $this->response->body(json_encode(array($data_items)));
        $this->response->type('application/json');
        return $this->response;
    }

    public function populateFullOfficeUnitOrgTree()
    {
        $this->layout = null;
        $office_origin_id = $this->request->data['office_origin_id'];
        if (empty($office_origin_id)) {
            $this->response->body(json_encode(array()));
            $this->response->type('application/json');
            return $this->response;
        }


        $this->response->body(json_encode(array()));
        $this->response->type('application/json');
        return $this->response;
    }

    private function _getOfficeParentId($id)
    {
        $table_instance = TableRegistry::get('Offices');
        $response_data = $table_instance->get($id);
        return $response_data->parent_office_id;
    }

    private function _buildTree(array $elements, $parentId)
    {
        $branch = array();
        foreach ($elements as $element) {
            if ($element['parent_office_id'] == $parentId) {
                $children = $this->_buildTree($elements, $element['id']);

                if ($children) {
                    $element['children'] = $children;
                }
                $branch[] = $element;
            }
        }

        return $branch;
    }

}
