<?php

namespace App\Controller;

use App\Controller\ProjapotiController;
use App\Model\Table;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\Log\Log;
use Cake\Cache\Cache;

class OfficeSettingsController extends ProjapotiController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['loadLayersByMinistry', 'loadOfficesByMinistryAndLayer','officeSummary']);
    }

    /**
     * List of office ministries
     */
    public function ministries()
    {
        $this->loadModel('OfficeMinistries');
        $listData = $this->OfficeMinistries->find('all')->where(['active_status' => 1]);
        $this->set('officeMinistries', $listData);
    }

    /**
     * Create new Ministry/Division
     */
    public function addMinistry()
    {
        $this->loadModel('OfficeMinistries');
        $entity = $this->OfficeMinistries->newEntity();
        if ($this->request->is('post')) {
            $entityWithData = $this->OfficeMinistries->patchEntity($entity,
                $this->request->data, ['validate' => 'add']);
            $errors         = $entityWithData->errors();
            if (!$errors) {
                if ($this->OfficeMinistries->save($entityWithData)) {
                    $this->Flash->success(__('Your data has been saved.'));
                    return $this->redirect(['action' => 'ministries']);
                }
            }
            $this->Flash->error(__('Unable to add your data.'));
        }
        $this->set('entity', $entity);
    }

    /**
     * Update data of Ministry/Division
     */
    public function editMinistry($id)
    {
        $table_instance = TableRegistry::get('OfficeMinistries');
        $entity         = $table_instance->get($id);

        if ($this->request->is(['post', 'put'])) {
            $entityWithData = $table_instance->patchEntity($entity,
                $this->request->data, ['validate' => 'update']);
            $errors         = $entityWithData->errors();
            if (!$errors) {
                if ($table_instance->save($entityWithData)) {
                    $this->Flash->success(__('Your data has been updated.'));
                    return $this->redirect(['action' => 'ministries']);
                }
            }
            $this->Flash->error(__('Unable to update your data.'));
        }
        $this->set('entity', $entity);
    }

    /**
     * List of office layers
     */
    public function officeLayers()
    {
        $this->loadModel('OfficeLayers');
        $listData = $this->OfficeLayers->find('all')->where(['OfficeLayers.active_status' => 1])->contain(['OfficeMinistries']);
        $this->set('officeLayers', $listData);
    }
    /*
     * |----- Manage ministry wise layers
     */

    public function layerSetup()
    {
        $table_instance_ministry = TableRegistry::get('OfficeMinistries');
        $this->set('officeMinistries',
            $table_instance_ministry->find('list')->where(['active_status' => 1]));

        $table_instance_layer = TableRegistry::get('OfficeMinistries');
        $entity               = $table_instance_layer->newEntity();
        $this->set('entity', $entity);
    }

    /**
     * Create new Ministry/Division
     */
    public function addLayer()
    {
        $table_instance = TableRegistry::get('OfficeLayers');
        $new_entity     = $table_instance->newEntity();
        $this->layout   = null;

        parse_str($this->request->data['formdata'], $this->request->data);

        if (empty($this->request->data['id'])) {
            unset($this->request->data['id']);
        }

        $entity_with_data = $table_instance->patchEntity($new_entity,
            $this->request->data);

        $errors = $entity_with_data->errors();

        if ($table_instance->save($entity_with_data)) {
            $this->response->body(json_encode(1));
        } else {
            $this->response->body(json_encode(0));
        }

        $this->response->type('application/json');
        return $this->response;
    }

    public function deleteLayer($id)
    {
        if (empty($id)) {
            $this->response->body(json_encode(0));
            $this->response->type('application/json');
            return $this->response;
        }
        $table_instance   = TableRegistry::get('OfficeLayers');
        $entity_with_data = $table_instance->get($id);
        $this->layout     = null;

        if (!empty($entity_with_data)) {
            $entity_with_data->active_status = 0;
            if ($table_instance->save($entity_with_data)) {
                $this->response->body(json_encode(1));
            } else {
                $this->response->body(json_encode(0));
            }
        } else {
            $this->response->body(json_encode(0));
        }

        $this->response->type('application/json');
        return $this->response;
    }

    /**
     * Get layer information by id
     * Json Response
     */
    public function getLayerById($id)
    {
        $table_instance = TableRegistry::get('OfficeLayers');
        $entity         = $table_instance->get($id);
        $this->response->body(json_encode($entity));
        $this->response->type('application/json');
        return $this->response;
    }

    /**
     * List of offices
     */
    public function officeOrigins()
    {
        $table_instance = TableRegistry::get('OfficeOrigins');
        $offices        = $table_instance->find('all')->where(['active_status' => 1]);

        $listData = array();
        foreach ($offices as $office) {
            $row                    = array();
            $row['id']              = $office['id'];
            $row['office_name_bng'] = $office['office_name_bng'];
            $row['office_name_eng'] = $office['office_name_eng'];
            $row['office_level']    = $office['office_level'];
            $row['office_sequence'] = $office['office_sequence'];

            /* Get Ministry Name form office ministry id */
            $ministry                        = TableRegistry::get('OfficeMinistries');
            $ministry                        = $ministry->find()->where(['id' => $office['office_ministry_id'],
                    'active_status' => 1])->first();
            $row['office_ministry_name_bng'] = $ministry['name_bng'];

            /* Get Layer Name form office layer id */
            $layer                        = TableRegistry::get('OfficeLayers');
            $layer                        = $layer->find()->where(['id' => $office['office_layer_id']])->first();
            $row['office_layer_name_bng'] = $layer['layer_name_bng'];

            /* Get Parent Name form office id */
            $parentOffice = $table_instance->find()->where(['id' => $office['office_parent_id']])->first();
            if (!empty($parentOffice)) {
                $row['parent'] = $parentOffice['office_name_bng'];
            } else {
                $row['parent'] = "";
            }
            $listData[] = $row;
        }

        $this->set('officeOrigins', $listData);
    }

    /**
     * Create new office
     */
    public function addOfficeOrigin()
    {
        $table_instance = TableRegistry::get('OfficeOrigins');
        $entity         = $table_instance->newEntity();
        if ($this->request->is('post')) {
            $entityWithData = $table_instance->patchEntity($entity,
                $this->request->data, ['validate' => 'add']);
            $errors         = $entityWithData->errors();

            if (!$errors) {
                if ($table_instance->save($entityWithData)) {
                    $this->Flash->success(__('Your data has been saved.'));
                    return $this->redirect(['action' => 'officeOrigins']);
                }
            }
            $this->Flash->error(__('Unable to add your data.'));
        }

        $table_ministry = TableRegistry::get('OfficeMinistries');
        $this->set('officeMinistries',
            $table_ministry->find('list')->where(['active_status' => 1]));
        $this->set('entity', $entity);
    }

    /**
     * Update Existing office
     */
    public function editOfficeOrigin($id)
    {
        $table_instance = TableRegistry::get('OfficeOrigins');
        $entity         = $table_instance->get($id);

        if ($this->request->is(['post', 'put'])) {
            $entityWithData = $table_instance->patchEntity($entity,
                $this->request->data, ['validate' => 'update']);
            $errors         = $entityWithData->errors();
            if (!$errors) {
                if ($table_instance->save($entityWithData)) {
                    $this->Flash->success(__('Your data has been updated.'));
                    return $this->redirect(['action' => 'officeOrigins']);
                }
            }
            $this->Flash->error(__('Unable to update your data.'));
        }

        $table_ministry = TableRegistry::get('OfficeMinistries');
        $this->set('officeMinistries',
            $table_ministry->find('list')->where(['active_status' => 1]));
        $this->set('entity', $entity);
    }

    public function deleteMinistry($id)
    {
        $officeTable    = TableRegistry::get('Offices');
        $table_instance = TableRegistry::get('OfficeMinistries');
        $entity         = $table_instance->get($id);
        if (!empty($entity)) {
            $isLinked = $officeTable->getMinistryInfo($id);
            if (!$isLinked) {
                $entity->active_status = 0;
                if ($table_instance->save($entity)) {
                    $this->Flash->success(__('The ministry has been deleted.'));
                } else {
                    $this->Flash->error(__('Unable to delete the ministry.'));
                }
            } else {
                $this->Flash->error(__('The ministry is linked with office origin units.'));
            }
        } else {
            $this->Flash->error(__('Invalid request!'));
        }
        return $this->redirect(['action' => 'ministries']);
    }

    public function officeOrigindelete($id)
    {
        $officeOriginUnits = TableRegistry::get('OfficeOriginUnits');
        $table_instance    = TableRegistry::get('OfficeOrigins');
        $entity            = $table_instance->get($id);
        if (!empty($entity)) {
            $isLinked = $officeOriginUnits->getOriginInfo($id);
            $isParent = $table_instance->find()->where(['parent_office_id' => $id,
                    'active_status' => 1])->count();
            if (!$isLinked && !$isParent) {
                //$entity->active_status = 0;

                //if ($table_instance->save($entity)) {
                if ($table_instance->delete($entity)) {
                    $this->Flash->success(__('The office origin has been deleted.'));
                } else {
                    $this->Flash->error(__('Unable to delete the office origin.'));
                }
            } else {
                $this->Flash->error(__('The office origin is linked with office origin units.'));
            }
        } else {
            $this->Flash->error(__('Invalid request!'));
        }
        return $this->redirect(['action' => 'officeOrigins']);
    }

    /**
     * Get Ministry wise office layers
     * Method Type: Ajax
     */
    public function loadLayersByMinistry()
    {
        $ministry_id = $this->request->data['office_ministry_id'];
        $this->loadModel('OfficeLayers');
        $listData    = array();
        if (($listData    = Cache::read('layer_by_ministry_'.$ministry_id,
                'memcached')) === false) {

            $layers   = $this->OfficeLayers->find('all')->select(['id', 'layer_name_bng'])->where(['office_ministry_id' => $ministry_id,
                'OfficeLayers.active_status' => 1]);
            $listData = array();
            foreach ($layers as $layer) {
                $listData[$layer['id']] = $layer['layer_name_bng'];
            }

            Cache::write('layer_by_ministry_'.$ministry_id, $listData,
                'memcached');
        }

        $this->response->body(json_encode($listData));
        $this->response->type('application/json');
        return $this->response;
    }

    /**
     * Get Ministry wise office layers
     * Method Type: Ajax
     */
    public function loadOfficesByMinistryAndLayer()
    {
        $ministry_id = $this->request->data['office_ministry_id'];
        $layer_id    = $this->request->data['office_layer_id'];

        $listData = array();

        if (($listData = Cache::read('layer_by_ministry_layer_'.$layer_id,
                'memcached')) === false) {

            $this->loadModel('OfficeOrigins');
            $offices  = $this->OfficeOrigins->find('all')->select(['id', 'office_name_bng'])->where(['office_ministry_id' => $ministry_id,
                'office_layer_id' => $layer_id, 'active_status' => 1]);
            $listData = array();
            foreach ($offices as $office) {
                $listData[$office['id']] = $office['office_name_bng'];
            }

            Cache::write('layer_by_ministry_layer_'.$layer_id, $listData,
                'memcached');
        }
        $this->response->body(json_encode($listData));
        $this->response->type('application/json');
        return $this->response;
    }
    public function loadAllOfficesByMinistryAndLayer()
    {
        $ministry_id = $this->request->data['office_ministry_id'];
        $layer_id    = $this->request->data['office_layer_id'];

        $this->loadModel('Offices');
        $offices  = $this->Offices->find('all')->select(['id', 'office_name_bng', 'unit_organogram_edit_option'])->where(['office_ministry_id' => $ministry_id,
            'office_layer_id' => $layer_id, 'active_status' => 1]);
        $listData = array();
        foreach ($offices as $office) {
            $listData[$office['id']]['name'] = $office['office_name_bng'];
            $listData[$office['id']]['setting'] = $office['unit_organogram_edit_option'];
        }
        $this->response->body(json_encode($listData));
        $this->response->type('application/json');
        return $this->response;
    }

    public function loadParentOfficesAndLayersByMinistry()
    {

        $ministry_id           = $this->request->data['office_ministry_id'];
        $table_instance_origin = TableRegistry::get('OfficeOrigins');
        $responseData          = array();

        if (($responseData = Cache::read('parent_office_and_layer_by_ministry_'.$ministry_id,
                'memcached')) === false) {

            $offices               = $table_instance_origin->find('all')->select(['id',
                    'office_name_bng'])->where(['office_ministry_id' => $ministry_id,
                'active_status' => 1]);
            $listDataParentOffices = array();
            foreach ($offices as $office) {
                $listDataParentOffices[$office['id']] = $office['office_name_bng'];
            }

            $table_instance_layer = TableRegistry::get('OfficeLayers');
            $layers               = $table_instance_layer->find('all')->select(['id',
                    'layer_name_bng'])->where(['office_ministry_id' => $ministry_id,
                'OfficeLayers.active_status' => 1]);
            $listDataLayers       = array();
            foreach ($layers as $layer) {
                $listDataLayers[$layer['id']] = $layer['layer_name_bng'];
            }

            $responseData = array(
                'parents' => $listDataParentOffices,
                'layers' => $listDataLayers
            );
            Cache::write('parent_office_and_layer_by_ministry_'.$ministry_id,
                $responseData, 'memcached');
        }

        $this->response->body(json_encode($responseData));
        $this->response->type('application/json');
        return $this->response;
    }

    /**
     * Office Autocomplete
     */
    public function loadofficeAutocompleteData()
    {
        $result_data = array();
        $search_text = $this->request->query['search_key'];
        $this->loadModel('OfficeRecords');
        $office_data = $this->OfficeRecords->loadOfficeAutocompleteData($search_text);
        foreach ($office_data as $office_data) {
            $result_data[] = array(
                "id" => $office_data['id'],
                "value" => $office_data['office_name_bng'],
                "desc" => $office_data['office_address'],
                "tokens" => $office_data['id']
            );
        }
        $this->response->body(json_encode($result_data));
        $this->response->type('application/json');
        return $this->response;
    }

    public function officeList()
    {
        $this->layout = 'dashboard';
        $office_domains = TableRegistry::get("OfficeDomains")->find()->select([
            'office_id','office_db','domain_host',
            'office_name' => 'Offices.office_name_bng',
            'ministry_name' => 'OfficeMinistries.name_bng',
            'layer_name' => 'OfficeLayers.layer_name_bng',
            'origin_name' => 'OfficeOrigins.office_name_bng',
        ])
            ->join([
                        "Offices" => [
                            'table' => 'offices', 'type' => 'inner',
                            'conditions' => ['OfficeDomains.office_id =Offices.id']
                        ],
                        "OfficeOrigins" => [
                            'table' => 'office_origins', 'type' => 'inner',
                            'conditions' => ['OfficeOrigins.id =Offices.office_origin_id']
                        ],
                        "OfficeLayers" => [
                            'table' => 'office_layers', 'type' => 'inner',
                            'conditions' => ['OfficeLayers.id =OfficeOrigins.office_layer_id']
                        ],
                        "OfficeMinistries" => [
                            'table' => 'office_ministries', 'type' => 'inner',
                            'conditions' => ['OfficeMinistries.id =OfficeLayers.office_ministry_id']
                        ],
                    ]);

        $getAllOffice = $office_domains->where(['status' => 1])->toArray();
        $this->set('offices', $getAllOffice);
    }

    public function officeSummary($start_date = "", $end_date = "")
    {
        if(empty($start_date)){
            $start_date = date("Y-m-d");
        }
        if(empty($end_date)){
            $end_date = date("Y-m-d");
        }

        $this->set(compact('start_date','end_date'));
        $this->layout = 'dashboard';
        $office_domains = TableRegistry::get("OfficeDomains");

        $getAllOffice = $office_domains->find()->where(['status' => 1])->toArray();

        $this->set('offices', $getAllOffice);
    }
    public function getOfficeAddress($office_id = 0){
         $response = ['status' => 'error' ,'data' => ''];
         $office_id = !empty($office_id)?$office_id:(isset($this->request->data['office_id'])?$this->request->data['office_id']:0);
        if(empty($office_id)){
           $response['msg'] = 'no office info found';
           goto rtn ;
        }
        try{
             $office_info = TableRegistry::get('Offices')->getOfficeAddress($office_id);
             if(!empty($office_info['office_address'])){
                 $response = ['status' => 'success' ,'data' => $office_info['office_address']];
                 goto rtn ;
             }
        } catch (\Exception $ex) {
            $response['msg'] = $ex->getMessage();
        }
         rtn:
             $this->response->type('application/json');
             $this->response->body(json_encode($response));
             return $this->response;
    }
    public function getOfficeDigitalNothiCode($office_id = 0){
         $response = ['status' => 'error' ,'data' => ''];
         $office_id = !empty($office_id)?$office_id:(isset($this->request->data['office_id'])?$this->request->data['office_id']:0);
        if(empty($office_id)){
           $response['msg'] = 'no office info found';
           goto rtn ;
        }
        try{
             $office_info = TableRegistry::get('Offices')->getAll(['id'=>$office_id],['digital_nothi_code'])->first();
             if(!empty($office_info['digital_nothi_code'])){
                 Cache::delete('get_office_digital_nothi_code_'.$office_id,'memcached');
                 $response = ['status' => 'success' ,'data' => $office_info['digital_nothi_code']];
                 goto rtn ;
             }
        } catch (\Exception $ex) {
            $response['msg'] = 'টেকনিক্যাল ত্রুটি হয়েছে।';
            $response['reason'] = $this->makeEncryptedData($ex->getMessage());
        }
         rtn:
             $this->response->type('application/json');
             $this->response->body(json_encode($response));
             return $this->response;
    }
}