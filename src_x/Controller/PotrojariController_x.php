<?php

namespace App\Controller;

use App\Controller\ProjapotiController;
use App\Model\Table;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\I18n\Number;
use Cake\Datasource\ConnectionManager;
use Cake\Network\Email\Email;
use \BanglaDate\BanglaDate;
use Cake\Routing\Route\Route;
use Cake\Routing\Router;
use CakePdf\Pdf\CakePdf;
use DateTime;
use DOMDocument;
use mikehaertl\wkhtmlto\Pdf;
use Cake\I18n\Time;
use Cake\Cache\Cache;
use Cake\Utility\String;

class PotrojariController extends ProjapotiController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->loadComponent('Paginator');
        $this->Auth->allow(['potrojariPreview', 'apiSendDraftCronPotrojari', 'apiApprovePotrojari','updatePotrojariStatus','getError','apiPotroDraft','getBangladate','apiPotrojariDraftUpdate','apiSendSummaryDraftPotrojari','potrojariDynamicHeader','potrojariDynamicFooter','apiPotroDraftView']);
    }

    public function index(){
        $this->layout = 'online_dak';
    }

    public function decided_daklist()
    {
        $dak_daptoriks = TableRegistry::get('DakDaptoriks');
        $query = $dak_daptoriks->find('all');
        $this->set(compact('query'));
    }

    public function potrojari_template_list($employee_offices = null)
    {
             // define offices2ShowTemplateModals
            $offices2ShowTemplateModals =  $this->offices2ShowTemplateModals();
            if(empty($employee_offices)) {
                $employee_offices = $this->getCurrentDakSection();
            }
//           if (($query = Cache::read('potrojari_templates', 'memcached')) === false) {
                $potrojari_templates = TableRegistry::get('PotrojariTemplates');
                if(!empty($offices2ShowTemplateModals) && !empty($employee_offices)){
                    if(in_array($employee_offices['office_id'], $offices2ShowTemplateModals)){
                         $query = $potrojari_templates->find('list', ['keyField' => 'template_id', 'valueField' => 'template_name'])->where(['status' => 1])->toArray();
                    }else{
                            $query = $potrojari_templates->find('list', ['keyField' => 'template_id', 'valueField' => 'template_name'])->where(['status' => 1,'id NOT IN' => [21,27]])->toArray();
                    }
                }

//                Cache::write('potrojari_templates', $query, 'memcached');
//            }
        return $query;
    }

    public function get_potrojari_template()
    {

        $id = $this->request->data['template_id'];
        $potrojari_templates = TableRegistry::get('PotrojariTemplates');
        $query = $potrojari_templates->get($id);

        $this->response->body(json_encode($query));
        $this->response->type('application/json');
        return $this->response;
    }

    public function makeDraft($nothiMasterId, $potroid = 0, $type = 'potro', $attached_id = 0,$potrojari_id = 0)
    {
        /*
         * Flow
         * make draft
         * potrojari cell
         * office search cell
         */
        $this->layout = null;

        $this->set(compact('nothiMasterId', 'potroid', 'type','attached_id'));

        $employee_office = $this->getCurrentDakSection();
        if(!empty($attached_id)){
            $potrojari_templates = TableRegistry::get('PotrojariTemplates');
            $template_list = $potrojari_templates->find('list',
                ['keyField' => 'template_id', 'valueField' => 'template_name'])->where(['status' => 4])->toArray();
            $this->set('is_endorse',1);
            $this->set('potrojari_language',TableRegistry::get('Potrojari')->find()->select(['potrojari_language'])->where(['id' => $potrojari_id])->first()['potrojari_language']);
        }else {
            $template_list = $this->potrojari_template_list();
        }

        $officeUnitsTable = TableRegistry::get('OfficeUnits');

        $nothiPartsTable = TableRegistry::get('NothiParts');
        $potrosTable = TableRegistry::get('NothiPotros');
        $potroInfoTable = TableRegistry::get('NothiPotroAttachments');
        $potrojariTable = TableRegistry::get('Potrojari');
        $nothiparts = $nothiPartsTable->get($nothiMasterId);
        $table = TableRegistry::get('PotrojariHeaderSettings');
        try {
            $heading = $table->findByOfficeId($employee_office['office_id'])->firstOrfail();
        } catch (\Exception $ex) {
            $heading = $table->newEntity();
            $heading->office_id = $employee_office['office_id'];
            $heading->office_unit_id = $employee_office['office_unit_id'];
            $heading->office_unit_organogram_id = $employee_office['office_unit_organogram_id'];
            $heading->employee_record_id = $employee_office['officer_id'];
            $heading->potrojari_head = json_encode([
                'head_title' => 'গণপ্রজাতন্ত্রী বাংলাদেশ সরকার',
                'head_ministry' => $employee_office['ministry_records'],
                'head_office' => $employee_office['office_name'],
                'head_unit' => '...',
                'head_other' => $employee_office['office_web']
            ]);
            $heading->potrojari_head_eng = json_encode([
                'head_title' => "Government of the People's Republic of Bangladesh",
                'head_ministry' => $employee_office['ministry_name_eng'],
                'head_office' => $employee_office['office_name_eng'],
                'head_unit' => '...',
                'head_other' => $employee_office['office_web']
            ]);
            $table->save($heading);
        }
        $this->set(compact('heading'));
        if(!empty($heading->potrojari_head)){
            $this->set('potrojari_head',$heading->potrojari_head);
        }

        $ownUnitInfo = $unitInfo = $officeUnitsTable->get($employee_office['office_unit_id']);

        $totalPotrojari = $potrojariTable->find()->where(['office_id' => $employee_office['office_id'],
            'YEAR(potrojari_date)' => date('Y'), 'potrojari_draft_office_id' => $employee_office['office_id'],
            'potrojari_draft_unit' => $employee_office['office_unit_id']])->count();

        $totalPotrojari = $ownUnitInfo['sarok_no_start'] + $totalPotrojari;

        $this->set('totalPotrojari', Number::format(($totalPotrojari + 1),['pattern'=>'######']));

        $this->set('nothiMasterInfo', $nothiparts);
        $this->set('potroInfo', array());
        $this->set('potroSubject', "");

        TableRegistry::remove('NothiMasterPermissions');
        $nothiMastersPermissionTable = TableRegistry::get('NothiMasterPermissions');
        $permitted_nothi_list = $nothiMastersPermissionTable->allNothiPermissionList($employee_office['office_id'],
            $employee_office['office_id'], $employee_office['office_unit_id'],
            $employee_office['office_unit_organogram_id'], 1);

        $potroAttachmentRecord = $potroInfoTable->find('list',
            ['keyField' => 'id', 'valueField' => [
                "nothi_potro_page_bn", 'subject'
            ]])
            ->select(['subject' => 'NothiPotros.subject', 'NothiPotroAttachments.id',
                "nothi_potro_page_bn" => 'NothiPotroAttachments.nothi_potro_page_bn'])
            ->join([
                'NothiPotros' => [
                    'table' => 'nothi_potros',
                    'type' => 'INNER',
                    'conditions' => 'NothiPotros.id = NothiPotroAttachments.nothi_potro_id'
                ]
            ])
            ->where(['NothiPotroAttachments.nothi_master_id' => $nothiparts['nothi_masters_id']])
            ->where(['NothiPotroAttachments.nothijato' => 0, 'NothiPotroAttachments.status' => 1,
                ])
            ->where(['NothiPotroAttachments.nothi_part_no IN' => $permitted_nothi_list])
            ->order(['NothiPotroAttachments.id asc'])->toArray();
        // Potrojari body also supported in potrojari attachemnt
        //'NothiPotroAttachments.attachment_type <> ' => 'text'-- ommited

        $this->set(compact('potroAttachmentRecord'));

        if (!empty($potroid) && $type == 'potro') {
            $potroInfo = $potroInfoTable->get($potroid);
            $this->set('potroInfo', $potroInfo);
            $potroSubject = $potrosTable->get($potroInfo['nothi_potro_id']);
            $this->set('potroSubject', $potroSubject['subject']);
        }

        $guardFileCategories_table = TableRegistry::get("GuardFileCategories");
        $guarfilesubjects = $guardFileCategories_table->getTypes(0, $employee_office);
        $this->set(compact('guarfilesubjects'));

        $this->set('template_list', $template_list);
        $this->set('office_id', $employee_office['office_id']);
        $this->set('employee_office', $employee_office);

        $time = new Time(date('Y-m-d'));
        $this->set('signature_date_bangla', $time->i18nFormat('d-M-Y', null, 'bn-BD'));
        // offline potro draft key
        // naming convention : potro_designation-id_office-id_nothi-part_no_formOnucched/formPotro_reference-onucched-id/reference-potro-id_potro-type_potro-language
// Example : potro_817_11_100_note_200_1_bn

        $this->set('offline_potro_draft_key','potro_'.$employee_office['office_unit_organogram_id'].'_'.$employee_office['office_id'].'_'.$nothiMasterId.'_'.$type.'_'.$potroid);
    }

    public function sendPotrojari($nothiMasterId, $potroid = 0, $type = 'potro')
    {
        $this->layout = null;

        if ($this->request->is('ajax')) {
            if (!empty($this->request->data)) {
                if (empty($nothiMasterId)) {
                    echo json_encode(array("status" => 'error', 'msg' => __("দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না")));
                    die;
                }
                $emplyee_office = $this->getCurrentDakSection();
                if(!empty($emplyee_office) && $emplyee_office['default_sign'] > 0){
                    $this->loadComponent('DigitalSignatureRelated');
                    $res = $this->DigitalSignatureRelated->verifyDigitalSignature($emplyee_office['default_sign'],$this->request->data['soft_token']);
                    if($res['status'] =='error'){
                         $this->response->body(json_encode(array('status' => 'error',
                             'msg' => 'দুঃখিত! ডিজিটাল সিগনেচার করা সম্ভব হয়নি। কারণঃ ')));
                         $this->response->type('application/json');
                         return $this->response;
                    }
                }

                $potrojari = $this->add($nothiMasterId, $potroid, $type);

                if (!empty($potrojari)) {

                    $status = $this->sendDraftPotro($potrojari, $nothiMasterId);
                    echo json_encode(array("status" => 'success', 'msg' => __('পত্রজারি প্রক্রিয়াধীন')));
                } else
                    echo json_encode(array("status" => 'error', 'msg' => __('পত্রজারি করা সম্ভব হচ্ছেনা')));
            }
        }

        die;
    }

    public function sendDraftPotrojari($nothiMasterId, $referenceId,
                                       $type = 'potro')
    {
        $this->layout = null;

        if ($this->request->is('post', 'ajax')) {
            if (empty($nothiMasterId)) {
                echo json_encode(array("status" => 'error', 'msg' => __("দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না")));
                die;
            }

            $potrojariTable = TableRegistry::get("Potrojari");

            $potrojari = $potrojariTable->find()->where([
                'id' => $referenceId
            ])->first();


            if (!empty($potrojari)) {
                $status = $this->sendDraftPotro($potrojari, $nothiMasterId);
                echo json_encode(array("status" => 'success', 'msg' => __('পত্রজারি প্রক্রিয়াধীন')));
            } else {
                echo json_encode(array("status" => 'error', 'msg' => __('পত্রজারি করা সম্ভব হচ্ছেনা')));
            }
        }
        die;
    }

    public function sendDraftCronPotrojari($nothiMasterId, $referenceId,
                                           $type = 'potro')
    {
        $this->layout = null;

        if ($this->request->is('post', 'ajax')) {
            if (empty($nothiMasterId)) {
                echo json_encode(array("status" => 'error', 'msg' => __("দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না")));
                die;
            }

            $employee_office = $this->getCurrentDakSection();
            $nothi_office = !empty($this->request->data['nothi_office']) ? $this->request->data['nothi_office'] : 0;
            $redirect = !empty($this->request->data['redirect']) ? $this->request->data['redirect'] : 0;
            if ($nothi_office != $employee_office['office_id']) {
            } else {
                $nothi_office = $employee_office['office_id'];
            }

            $this->switchOffice($nothi_office, "apiOffice");

            TableRegistry::remove("Potrojari");
            $potrojariTable = TableRegistry::get("Potrojari");

            $potrojari = $potrojariTable->find()->where([
                'id' => $referenceId,'potro_status'=>'Draft'
            ])->first();
            // if potro type = 30 then need to make pj attachment
            if($potrojari['potro_type'] == 30){
               $updateCs = $this->updateCsDuringPotrojari($referenceId,$this->makeEncryptedData(Date('Y-m-d')));
               if(!empty($updateCs['status']) && $updateCs['status'] =='success'){
                   //replaced cover with description . so need to call again
                   TableRegistry::remove("Potrojari");
                   $potrojariTable = TableRegistry::get("Potrojari");
                   $potrojari = $potrojariTable->find()->where(['id' => $referenceId,'potro_status'=>'Draft'])->first();
               }else{
                   $this->response->body(json_encode(array('status' => 'error', 'msg' =>isset($updateCs['message'])? $updateCs['message'] : 'দুঃখিত! প্রতিবেদনটি সংযুক্তি আকারে তৈরি করা সম্ভব হয়নি। ')));
                   $this->response->type('application/json');
                   return $this->response;
               }
            }
            // just check whether signature is correct or not
            // skip if soft token -1
            if(!empty($employee_office) && $employee_office['default_sign'] > 0 && isset($this->request->data['soft_token']) && $this->request->data['soft_token'] != -1){
             $this->loadComponent('DigitalSignatureRelated');
              $res = $this->DigitalSignatureRelated->signPotrojari($referenceId,$this->request->data['soft_token']);
                if($res['status'] =='error'){
                     $this->response->body(json_encode(array('status' => 'error',
                         'msg' => 'দুঃখিত! ডিজিটাল সিগনেচার করা সম্ভব হয়নি। কারণঃ '.$res['msg'])));
                     $this->response->type('application/json');
                     return $this->response;
                }
            }

            if (!empty($potrojari)) {
                $status = $this->sendDraftCronPotro($potrojari, $nothiMasterId, false,$employee_office);
                echo json_encode(array("status" => 'success', 'msg' => __('পত্রজারি প্রক্রিয়াধীন')));
            } else {
                echo json_encode(array("status" => 'error', 'msg' => __('পত্রজারি করা সম্ভব হচ্ছেনা')));
            }
             if($redirect == 1){
            return $this->redirect(['_name' => 'noteDetail',$nothiMasterId,$nothi_office]);
        }
        }
        die;
    }

    public function apiSendDraftCronPotrojari($nothiMasterId, $referenceId,
                                              $type = 'potro')
    {
        $this->layout = null;

        if (!empty($this->request->data)) {
            if (empty($nothiMasterId)) {
                echo json_encode(array("status" => 'error', 'msg' => __("দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না")));
                die;
            }

            $user_designation = !empty($this->request->data['user_designation']) ? $this->request->data['user_designation'] : 0;

            $jsonArray = array(
                "status" => "error",
                "error_code" => 404,
                "message" => "Invalid Request"
            );

            if (empty($user_designation)) {
                echo json_encode($jsonArray);
                die;
            }
            if (empty($nothiMasterId)) {
                echo json_encode($jsonArray);
                die;
            }
            if (empty($referenceId)) {
                echo json_encode($jsonArray);
                die;
            }

            $employee_office = $this->setCurrentDakSection($user_designation);

            $this->set(compact('employee_office'));

            $nothi_office = !empty($this->request->data['nothi_office']) ? $this->request->data['nothi_office'] : 0;
            $otherNothi = false;
            if ($nothi_office != $employee_office['office_id']) {
                $otherNothi = true;
            } else {
                $nothi_office = $employee_office['office_id'];
            }

            $this->switchOffice($nothi_office, "apiOffice");

            TableRegistry::remove("Potrojari");;
            $potrojariTable = TableRegistry::get("Potrojari");

            $potrojari = $potrojariTable->find()->where([
                'id' => $referenceId
            ])->first();
            // skip if soft token -1
            if(!empty($employee_office) && $employee_office['default_sign'] > 0 && isset($this->request->data['soft_token']) && $this->request->data['soft_token'] != -1){
                $this->loadComponent('DigitalSignatureRelated');
                $res = $this->DigitalSignatureRelated->signPotrojari($referenceId,$this->request->data['soft_token'],$employee_office['default_sign'],$employee_office);
                if($res['status'] =='error'){
                    $this->response->body(json_encode(array('status' => 'error',
                                                            'msg' => 'দুঃখিত! ডিজিটাল সিগনেচার করা সম্ভব হয়নি। কারণঃ '.$res['msg'])));
                    $this->response->type('application/json');
                    return $this->response;
                }
            }

            if (!empty($potrojari)) {
                $status = $this->sendDraftCronPotro($potrojari, $nothiMasterId, false, $employee_office);
                echo json_encode(array("status" => 'success', 'msg' => __('পত্রজারি প্রক্রিয়াধীন')));
            } else {
                echo json_encode(array("status" => 'error', 'msg' => __('পত্রজারি করা সম্ভব হচ্ছেনা')));
            }
        }
        die;
    }

    public function deleteDraftPotrojari($nothiMasterId, $referenceId,
                                         $type = 'potro',$redirect = 0)
    {
        $response = [
            'status' => 'error',
            'msg' => 'Something went wrong'
        ];
        $this->layout = null;

        if (empty($nothiMasterId)) {
            echo json_encode(array("status" => 'error', 'msg' => __("দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না")));
            die;
        }
        $employee_office = $this->getCurrentDakSection();
        // first check if logged user is nothi part current user
        $nothiMasterCurrentUsersTable = TableRegistry::get("NothiMasterCurrentUsers");
        $checkCurrentUser = $nothiMasterCurrentUsersTable->getCurrentUserbyDesignation($nothiMasterId,$employee_office['office_unit_organogram_id'],$employee_office['office_unit_id'],$employee_office['office_id']);
        if(empty($checkCurrentUser)){
            $response['msg'] = __('Unauthorized Access');
            goto rtn;
        }

        $nothiPartsTable = TableRegistry::get('NothiParts');
        $nothiPartsInformation = $nothiPartsTable->get($nothiMasterId);
        $conn = ConnectionManager::get('default');

        $potrojariTable = TableRegistry::get("Potrojari");

        $potrojari = $potrojariTable->find()->where(['id' => $referenceId, 'potro_status' => 'Draft'])->first();
        if (!empty($potrojari)) {
            try {
                $conn->begin();

                $potrojariVersionTable = TableRegistry::get('PotrojariVersions');
                $tablePotrojariReceiver = TableRegistry::get('PotrojariReceiver');
                $tablePotrojariOnulipi = TableRegistry::get('PotrojariOnulipi');
                $tablePotrojariAttachment = TableRegistry::get('PotrojariAttachments');

                $potrojariVersionTable->deleteAll(['potrojari_id' => $potrojari->id]);
                $tablePotrojariReceiver->deleteAll(['potrojari_id' => $potrojari->id]);
                $tablePotrojariOnulipi->deleteAll(['potrojari_id' => $potrojari->id]);
                $tablePotrojariAttachment->deleteAll(['potrojari_id' => $potrojari->id]);

                $potrojariTable->delete($potrojari);

                if (!empty($potrojari->nothi_potro_id)) {

                    $potroAttachmentTable = TableRegistry::get('NothiPotroAttachments');

                    $potrinfo = $potroAttachmentTable->get($potrojari->nothi_potro_id);

                    $potrinfo->potrojari = 0;
                    $potrinfo->potrojari_id = 0;
                    $potrinfo->potrojari_status = '';
                    $potroAttachmentTable->save($potrinfo);
                } else if(!empty($potrojari->nothi_notes_id)) {
                    $notesTable = TableRegistry::get('NothiNotes');
                    $noteInfo = $notesTable->get($potrojari->nothi_notes_id);

                    $noteInfo->potrojari = 0;
                    $noteInfo->potrojari_id = 0;
                    $noteInfo->potrojari_status = '';
                    $notesTable->save($noteInfo);
                }

                $nothiPartsInformation->is_active = 1;
                $nothiPartsTable->save($nothiPartsInformation);
                $conn->commit();
                if($redirect == true){
                       $this->Flash->set('ড্রাফট মুছে ফেলা হয়েছে');
                }
                $response = [
                    "status" => 'success', 'msg' => __('ড্রাফট মুছে ফেলা হয়েছে')
                ];
            } catch (\Exception $ex) {
                $conn->rollback();
                  if($redirect == true){
                       $this->Flash->set('দুঃখিত! ড্রাফট মুছে ফেলা সম্ভব হচ্ছে না');
                  }
                  $response['msg'] = __('দুঃখিত! ড্রাফট মুছে ফেলা সম্ভব হচ্ছে না');
                  $response['reason'] = $this->makeEncryptedData($ex->getMessage());
            }
        } else {
            $conn->rollback();
             if($redirect == true){
                $this->Flash->set('দুঃখিত! ড্রাফট মুছে ফেলা সম্ভব হচ্ছে না');
            }
            $response['msg'] = __('দুঃখিত! ড্রাফট মুছে ফেলা সম্ভব হচ্ছে না');
            $response['reason'] = $this->makeEncryptedData('Empty Data');
        }
        if($redirect == true || $this->request->is('get')){
           return $this->redirect(['_name' =>'noteDetail',$nothiMasterId]);
        }
        rtn:
        $this->response->type('json');
        $this->response->body(json_encode($response));
        return $this->response;
    }

    public function saveDraft($nothiMasterId, $potroid = 0, $type = 'potro')
    {
        $this->digitalSigantureUrlChecker();
        $this->layout = null;

        if ($this->request->is('ajax')) {
            $noteTable = TableRegistry::get('NothiNotes');
            $nothiMasterTable = TableRegistry::get('NothiMaster');

            if (!empty($this->request->data)) {
                if (empty($nothiMasterId)) {
                    echo json_encode(array("status" => 'error', 'msg' => __("দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না")));
                    die;
                }

                $potrojari = $this->add($nothiMasterId, $potroid, $type);

                if (!empty($potrojari)) {
                    if ($type == 'note') {
                        try {
                            $noteinfo = $noteTable->get($potroid);
                            $noteinfo->potrojari = 1;
                            $noteinfo->potrojari_id = $potrojari->id;
                            $noteinfo->potrojari_status = DAK_CATEGORY_DRAFT;

                            if ($noteTable->save($noteinfo)) {

                                echo json_encode(array("status" => 'success', 'msg' => __('পত্রজারি ড্রাফট করা হয়েছে')));
                            } else {
                                echo json_encode(array("status" => 'error', 'msg' => __('ড্রাফ্‌ট করা সম্ভব হচ্ছে না')));
                            }
                        } catch (\Exception $ex) {

                            echo json_encode(array("status" => 'error', 'msg' => __('ড্রাফ্‌ট করা সম্ভব হচ্ছে না')));
                        }
                    } else {
                        try {
                            $potroAttachmentTable = TableRegistry::get('NothiPotroAttachments');

                            $potrinfo = $potroAttachmentTable->get($potroid);
                            $potrinfo->potrojari = 1;
                            $potrinfo->potrojari_status = DAK_CATEGORY_DRAFT;

                            if ($potroAttachmentTable->save($potrinfo)) {

                                echo json_encode(array("status" => 'success', 'msg' => __('পত্রজারি ড্রাফট করা হয়েছে')));
                            } else {
                                echo json_encode(array("status" => 'error', 'msg' => __('ড্রাফ্‌ট করা সম্ভব হচ্ছে না')));
                            }
                        } catch (\Exception $ex) {

                            echo json_encode(array("status" => 'error', 'msg' => __('ড্রাফ্‌ট করা সম্ভব হচ্ছে না')));
                        }
                    }
                }
            }
        }

        die;
    }

    public function PotrojariDraftUpdate($id, $nothi_office = 0)
    {

        $this->digitalSigantureUrlChecker();
        $this->layout = null;

        if ($this->request->is('ajax')) {
            if (!empty($this->request->data)) {
                $employee_office = $this->getCurrentDakSection();
                $otherNothi = false;
                if ($nothi_office != 0 && $nothi_office != $employee_office['office_id']) {
                    $otherNothi = true;
                    $this->switchOffice($nothi_office, 'MainNothiOffice');
                } else {
                    $nothi_office = $employee_office['office_id'];
                }
                TableRegistry::remove('Potrojari');
                $tablePotroJari = TableRegistry::get('Potrojari');
                $potrojari = $tablePotroJari->get($id);
                /*
                 * Checking sender/onumodonkari/sovapotir sakkhor
                 *
                 */

                if (!empty($potrojari['officer_id'])) {
                    $usersTable = TableRegistry::get('Users');
                    $users = $usersTable->find()->where(['employee_record_id' => $potrojari['officer_id']])->first();
                    $username = $users['username'];
                    $options['token'] = sGenerateToken(['file'=>$username],['exp'=>time() + 60*300]);

                    $data = $this->getSignature($username, 1, 0, true,null,$options);

                    if ($data == false) {
                        echo json_encode(array('status' => 'error',
                            'msg' => 'দুঃখিত! প্রেরক/অনুমোদনকারী এর স্বাক্ষর পাওয়া যায়নি। অনুগ্রহ করে সিস্টেমে স্বাক্ষর আপলোড করে খসড়া পুনরায় সংরক্ষণ করুন। ধন্যবাদ।'));
                        die;
                    }
                }
                if (!empty($potrojari['approval_officer_id'])) {

                    $users = $usersTable->find()->where(['employee_record_id' => $potrojari['approval_officer_id']])->first();
                    $username = $users['username'];
                    $options['token'] = sGenerateToken(['file'=>$username],['exp'=>time() + 60*300]);

                    $data = $this->getSignature($username, 1, 0, true,null,$options);

                    if ($data == false) {
                        echo json_encode(array('status' => 'error',
                            'msg' => 'দুঃখিত! প্রেরক/অনুমোদনকারী এর স্বাক্ষর পাওয়া যায়নি। অনুগ্রহ করে সিস্টেমে স্বাক্ষর আপলোড করে খসড়া পুনরায় সংরক্ষণ করুন। ধন্যবাদ।'));
                        die;
                    }
                }
                if ($potrojari['sovapoti_officer_id'] > 0) {
                    $users = $usersTable->find()->where(['employee_record_id' => $potrojari['sovapoti_officer_id']])->first();
                    $username = $users['username'];
                    $options['token'] = sGenerateToken(['file'=>$username],['exp'=>time() + 60*300]);

                    $data = $this->getSignature($username, 1, 0, true,null,$options);

                    if ($data == false) {
                        echo json_encode(array('status' => 'error',
                            'msg' => 'দুঃখিত! সভাপতি এর স্বাক্ষর পাওয়া যায়নি। অনুগ্রহ করে সিস্টেমে স্বাক্ষর আপলোড করে খসড়া পুনরায় সংরক্ষণ করুন। ধন্যবাদ।'));
                        die;
                    }
                }

                /*
                 * End
                 */
                $data = $this->updatePotrojari($potrojari, $id, $nothi_office, $employee_office);

                if ($data['status']=='success') {
                    echo json_encode(array('status' => 'success'));
                } else {
                    echo json_encode(array('status' => 'error', 'msg' => 'দুঃখিত! খসড়া পত্র সংশোধন করা সম্ভব হচ্ছে না। '.(isset($data['msg'])?$data['msg']:''),'reason'=>''));
                }
            }
        }

        die;
    }

    public function SummaryNothiDraftUpdate($id, $nothi_office = 0)
    {

        $this->layout = null;

        if ($this->request->is('ajax')) {
            if (!empty($this->request->data)) {
                $employee_office = $this->getCurrentDakSection();
                $otherNothi = false;
                if ($nothi_office != 0 && $nothi_office != $employee_office['office_id']) {
                    $otherNothi = true;
                    $this->switchOffice($nothi_office, 'MainNothiOffice');
                } else {
                    $nothi_office = $employee_office['office_id'];
                }
                TableRegistry::remove('Potrojari');
                $tablePotroJari = TableRegistry::get('Potrojari');
                $potrojari = $tablePotroJari->get($id);
                /*
                 * Checking sender/onumodonkari/sovapotir sakkhor
                 *
                 */

                if (!empty($potrojari['officer_id'])) {
                    $usersTable = TableRegistry::get('Users');
                    $users = $usersTable->find()->where(['employee_record_id' => $potrojari['officer_id']])->first();
                    $username = $users['username'];
                    $options['token'] = sGenerateToken(['file'=>$username],['exp'=>time() + 60*300]);

                    $data = $this->getSignature($username, 1, 0, true,null,$options);

                    if ($data == false) {
                        echo json_encode(array('status' => 'error',
                            'msg' => 'দুঃখিত! প্রেরক/অনুমোদনকারী এর স্বাক্ষর পাওয়া যায়নি। অনুগ্রহ করে সিস্টেমে স্বাক্ষর আপলোড করে খসড়া পুনরায় সংরক্ষণ করুন। ধন্যবাদ।'));
                        die;
                    }
                }
                if (!empty($potrojari['approval_officer_id'])) {

                    $users = $usersTable->find()->where(['employee_record_id' => $potrojari['approval_officer_id']])->first();
                    $username = $users['username'];
                    $options['token'] = sGenerateToken(['file'=>$username],['exp'=>time() + 60*300]);

                    $data = $this->getSignature($username, 1, 0, true,null,$options);

                    if ($data == false) {
                        echo json_encode(array('status' => 'error',
                            'msg' => 'দুঃখিত! প্রেরক/অনুমোদনকারী এর স্বাক্ষর পাওয়া যায়নি। অনুগ্রহ করে সিস্টেমে স্বাক্ষর আপলোড করে খসড়া পুনরায় সংরক্ষণ করুন। ধন্যবাদ।'));
                        die;
                    }
                }
                if ($potrojari['sovapoti_officer_id'] > 0) {
                    $users = $usersTable->find()->where(['employee_record_id' => $potrojari['sovapoti_officer_id']])->first();
                    $username = $users['username'];
                    $options['token'] = sGenerateToken(['file'=>$username],['exp'=>time() + 60*300]);

                    $data = $this->getSignature($username, 1, 0, true,null,$options);

                    if ($data == false) {
                        echo json_encode(array('status' => 'error',
                            'msg' => 'দুঃখিত! সভাপতি এর স্বাক্ষর পাওয়া যায়নি। অনুগ্রহ করে সিস্টেমে স্বাক্ষর আপলোড করে খসড়া পুনরায় সংরক্ষণ করুন। ধন্যবাদ।'));
                        die;
                    }
                }

                /*
                 * End
                 */
                $data = $this->updateSummaryNothi($potrojari, $id, $nothi_office);

                if ($data===true) {
                    echo json_encode(array('status' => 'success'));
                } else {
                    echo json_encode(array('status' => 'error', 'msg' => 'দুঃখিত! খসড়া পত্র সংশোধন করা সম্ভব হচ্ছে না','reason'=>''));
                }
            }
        }

        die;
    }

    private function add($nothiMasterId = 0, $potroid = 0, $type = 'potro')
    {

        $table_instance_office = TableRegistry::get('Offices');
        $table_instance_unit = TableRegistry::get('OfficeUnits');

        $tablePotroJari = TableRegistry::get('Potrojari');
        $tablePotroAttachment = TableRegistry::get('NothiPotroAttachments');

        $tablePotrojariReceiver = TableRegistry::get('PotrojariReceiver');
        $tablePotrojariOnulipi = TableRegistry::get('PotrojariOnulipi');
        $tablePotroJariAttachment = TableRegistry::get('PotrojariAttachments');
        $nothiPartsTable = TableRegistry::get('NothiParts');
        $tablePotrojariGroup = TableRegistry::get('PotrojariGroups');
        $nothiPartInformation = $nothiPartsTable->get($nothiMasterId);

        $current_office_selection = $this->getCurrentDakSection();

        if ($this->request->is(['post'])) {
            $conn = ConnectionManager::get('default');

            try {
                $conn->begin();
                $potrojari = $tablePotroJari->newEntity();

                $potrojari->nothi_master_id = $nothiPartInformation['nothi_masters_id'];
                $potrojari->nothi_part_no = $nothiMasterId;
                $potrojari->nothi_notes_id = ($type == 'potro' ? 0 : $potroid);
                $potrojari->nothi_potro_id = ($type == 'note' ? 0 : $potroid);
                $potrojari->is_summary_nothi = 0;
                $potrojari->potrojari_draft_office_id = $current_office_selection['office_id'];
                $potrojari->potrojari_draft_unit = $current_office_selection['office_unit_id'];
                $potrojari->attached_potro_id = isset($this->request->data['attached_potro_id'])?$this->request->data['attached_potro_id']:0;

                $attached_html = '';
                if(!empty($potrojari->attached_potro_id)){
                    $attached_potro = $tablePotroAttachment->get($potrojari->attached_potro_id);

                    if(substr($attached_potro['attachment_type'],0,5)=='image') {
                        $src = Router::url(['_name'=>'getContent','?'=>[
                            'file'=> $attached_potro['file_name'],
                            'token'=>sGenerateToken(['file'=> $attached_potro['file_name']],['exp'=>time() + 60*24*365*10])
                        ]],true);

                        $attached_html = "<div class='row'><div class='col-md-12' style='text-align: center;'><img src='{$src}' alt='{$attached_potro['sarok_no']}'/> </div></div>";
                    } elseif($attached_potro['attachment_type'] == 'application/pdf'){
                        $pathinfo = pathinfo($attached_potro['file_name']);

                        $output_image_name = $pathinfo['dirname'].'/'.$pathinfo['filename'].'_0.png';
                        $output_image_path = FILE_FOLDER . $output_image_name;

                        if (file_exists(FILE_FOLDER_DIR . $output_image_name)) {
                            $src = $output_image_path;
                        } else {
                            echo json_encode(array('status' => 'error',
                                'msg' => 'দুঃখিত! পত্রজারি করা সম্ভব হচ্ছেনা। অনুগ্রহ করে কিছুক্ষণ পর আবার চেষ্টা করুন।'));
                            return false;
                        }

                        //$return_image = getImageFromPdf($attached_potro['file_name']);
                        //if ($return_image['status'] == 'success') {
                        //    $src = $return_image['image_path'];
                        //} else {
                        //    echo json_encode(array('status' => 'error',
                        //        'msg' => 'দুঃখিত! পত্রজারি করা সম্ভব হচ্ছেনা। অনুগ্রহ করে কিছুক্ষণ পর আবার চেষ্টা করুন।'));
                        //    return false;
                        //}

                        $explode = explode(FILE_FOLDER,$src);
                        $url = Router::url(['_name'=>'getContent','?'=>[
                            'file'=>$explode[1],
                            'token'=>sGenerateToken(['file'=>$explode[1]],['exp'=>time() + 60*24*365*10])
                        ]],true);

                        $attached_html = "<div class='row'><div class='col-md-12' style='text-align: center;'><img src='{$url}' alt='{$attached_potro['sarok_no']}' class='img-responsive'/> </div></div>";

                        if (!empty($this->request->data['prapto_potro'])) {
                            array_push($this->request->data['prapto_potro'], $this->request->data['attached_potro_id']);
                        } else {
                            $this->request->data['prapto_potro'] = [0=>$this->request->data['attached_potro_id']];
                        }

                    }elseif($attached_potro['attachment_type'] == 'text'){
                        $attached_html = trim($attached_potro->content_body);
                    }

                }

                $tableHeader = TableRegistry::get('PotrojariHeaderSettings');
                $headerSettings = [];
                $settings = [];
                $heading = $tableHeader->findByOfficeId($current_office_selection['office_id'])->first();
                if(!empty($heading)){
                    $settings = jsonA($heading['potrojari_head']);
                }
                if(empty($settings['banner_position'])){
                    $headerSettings['banner_position'] = 'left';
                }else{
                    $headerSettings['banner_position'] = $settings['banner_position'];
                }
                if(empty($settings['banner_width'])){
                    $headerSettings['banner_width'] = 'auto';
                }else{
                    $headerSettings['banner_width'] = $settings['banner_width'];
                }
                if(empty($this->request->data['meta_data'])){

                    $this->request->data['meta_data'] = json_encode([
                        'potro_header'=>0,
                        'potro_header_banner'=>base64_encode($this->potrojariDynamicHeader($current_office_selection['office_id'],true))
                    ]+$headerSettings);
                }else{
                    $header = jsonA($this->request->data['meta_data']);

                        $header['potro_header_banner'] = base64_encode($this->potrojariDynamicHeader($current_office_selection['office_id'],true));

                    $this->request->data['meta_data'] = json_encode($header+$headerSettings);
                }

                $potrojari->attached_potro = $attached_html;
                $potrojari->potrojari_language = !empty($this->request->data['potrojari_language'])?$this->request->data['potrojari_language']:'bn';
                $potrojari->potro_type = $this->request->data['potro_type'];
                $potrojari->sarok_no = $this->request->data['sender_sarok_no'];
                $potrojari->meta_data = !empty($this->request->data['meta_data'])?$this->request->data['meta_data']:'';
                $potrojari->potrojari_date = date("Y-m-d H:i:s");
                $potrojari->potro_subject = trim($this->request->data['dak_subject']);
                $potrojari->potro_security_level = $this->request->data['potro_security_level'];
                $potrojari->potro_priority_level = $this->request->data['potro_priority_level'];
                $potrojari->potro_status = DAK_CATEGORY_DRAFT;
                $potrojari->created = date("Y-m-d H:i:s");
                $potrojari->modified = date("Y-m-d H:i:s");
                $potrojari->created_by = $this->Auth->user("id");
                $potrojari->modified_by = $this->Auth->user("id");

                // onumodonkari
                $potrojari->office_id = !empty($this->request->data['sender_office_id_final'])
                    ? $this->request->data['sender_office_id_final'] : 0;

                $officeInfo = $table_instance_office->get($potrojari->office_id);
                $potrojari->officer_designation_id = !empty($this->request->data['sender_officer_designation_id_final'])
                    ? $this->request->data['sender_officer_designation_id_final']
                    : 0;
                $rec_office_unit = $table_instance_unit->getOfficeUnitBy_designationId($potrojari->officer_designation_id);
                $potrojari->officer_designation_label = !empty($this->request->data['sender_officer_designation_label_final'])
                    ? trim($this->request->data['sender_officer_designation_label_final'])
                    : '';
                $potrojari->office_name = ($potrojari->potrojari_language == 'eng' && $officeInfo['office_name_eng'])?trim($officeInfo['office_name_eng']):trim($officeInfo['office_name_bng']);
                $potrojari->officer_id = !empty($this->request->data['sender_officer_id_final'])
                    ? $this->request->data['sender_officer_id_final'] : 0;
                $potrojari->office_unit_id = $rec_office_unit['id'];
                $potrojari->office_unit_name = ($potrojari->potrojari_language == 'eng' && $rec_office_unit['unit_name_eng'])?trim($rec_office_unit['unit_name_eng']):trim($rec_office_unit['unit_name_bng']);
                $potrojari->officer_name = !empty($this->request->data['sender_officer_name_final'])
                    ? trim($this->request->data['sender_officer_name_final']) : '';
                $potrojari->officer_visibleName = !empty($this->request->data['sender_officer_visibleName'])
                    ? trim($this->request->data['sender_officer_visibleName']) : '';
                $potrojari->officer_visibleDesignation = !empty($this->request->data['sender_officer_visibleDesignation'])
                    ? trim($this->request->data['sender_officer_visibleDesignation']) : '';

                //prerok

                $potrojari->approval_office_id = !empty($this->request->data['approval_office_id_final'])
                    ? $this->request->data['approval_office_id_final'] : 0;

                $officeInfo = $table_instance_office->get($potrojari->approval_office_id);
                $potrojari->approval_officer_designation_id = !empty($this->request->data['approval_officer_designation_id_final'])
                    ? $this->request->data['approval_officer_designation_id_final']
                    : 0;

                $rec_office_unit = $table_instance_unit->getOfficeUnitBy_designationId($potrojari->approval_officer_designation_id);
                $potrojari->approval_officer_designation_label = !empty($this->request->data['approval_officer_designation_label_final'])
                    ? trim($this->request->data['approval_officer_designation_label_final'])
                    : '';

                $potrojari->approval_office_name = isset($this->request->data['approval_office_name_final'])?$this->request->data['approval_office_name_final']:(  ($potrojari->potrojari_language == 'eng')?$officeInfo['office_name_eng'] :$officeInfo['office_name_bng']);
                $potrojari->approval_officer_id = !empty($this->request->data['approval_officer_id_final'])
                    ? $this->request->data['approval_officer_id_final'] : 0;

                $potrojari->approval_office_unit_id = $rec_office_unit['id'];
                $potrojari->approval_office_unit_name = !empty($this->request->data['approval_office_unit_name_final'])?trim($this->request->data['approval_office_unit_name_final']):( ($potrojari->potrojari_language == 'eng' && $rec_office_unit['unit_name_eng'])?trim($rec_office_unit['unit_name_eng']):trim($rec_office_unit['unit_name_bng']));
                $potrojari->approval_officer_name = !empty($this->request->data['approval_officer_name_final'])
                    ? trim($this->request->data['approval_officer_name_final']) : '';
                $potrojari->approval_visibleName = !empty($this->request->data['approval_visibleName'])
                    ? trim($this->request->data['approval_visibleName']) : '';
                $potrojari->approval_visibleDesignation = !empty($this->request->data['approval_visibleDesignation'])
                    ? trim($this->request->data['approval_visibleDesignation']) : '';

                $this->request->data['contentbody'] = preg_replace('/(((width)+\s*[:]+\s*)+&?([\d]+)&?(pt)+)/', 'width: $4%;', $this->request->data['contentbody']);
                $potrojari->potro_description = trim($this->request->data['contentbody']);
                //if potro type CS template then potro cover need to be saved
                if($potrojari->potro_type == 30 ){
                    $potrojari->potro_cover = isset($this->request->data['content_cover'])?trim($this->request->data['content_cover']):'';
                }

                //sovapoti
                if ($potrojari->potro_type == 17) {
                    $potrojari->can_potrojari = 0; //($current_office_selection['office_unit_organogram_id']  == $potrojari->sovapoti_officer_designation_id ) ? 1 : 0;

                    $potrojari->sovapoti_office_id = !empty($this->request->data['sovapoti_office_id_final'])
                        ? ($this->request->data['sovapoti_office_id_final'])
                        : 0;

                    $potrojari->sovapoti_officer_id = (!empty($this->request->data['sovapoti_officer_id_final'])
                        ? ($this->request->data['sovapoti_officer_id_final'])
                        : 0);

                    $potrojari->sovapoti_officer_designation_id = (!empty($this->request->data['sovapoti_officer_designation_id_final'])
                        ? ($this->request->data['sovapoti_officer_designation_id_final'])
                        : 0);

                    $potrojari->sovapoti_officer_designation_label = (!empty($this->request->data['sovapoti_officer_designation_label_final'])
                        ? trim($this->request->data['sovapoti_officer_designation_label_final'])
                        : 0);

                    $potrojari->sovapoti_officer_name = (!empty($this->request->data['sovapoti_officer_name_final'])
                        ? trim($this->request->data['sovapoti_officer_name_final'])
                        : 0);
                    $potrojari->sovapoti_visibleName = !empty($this->request->data['sovapoti_visibleName'])
                        ? trim($this->request->data['sovapoti_visibleName']) : '';
                    $potrojari->sovapoti_visibleDesignation = !empty($this->request->data['sovapoti_visibleDesignation'])
                        ? trim($this->request->data['sovapoti_visibleDesignation']) : '';
                } else {
                    $potrojari->can_potrojari = 0;
                }
                                    // attension
                    $potrojari->attension_officer_designation_label = (!empty($this->request->data['attension_officer_designation_label_final'])
                        ? trim($this->request->data['attension_officer_designation_label_final'])
                        : 0);
                    $potrojari->attension_officer_designation_id = (!empty($this->request->data['attension_officer_designation_id_final'])
                        ? ($this->request->data['attension_officer_designation_id_final'])
                        : 0);
                    $potrojari->attension_office_unit_name = (!empty($this->request->data['attension_office_unit_name_final'])
                        ? trim($this->request->data['attension_office_unit_name_final'])
                        : '');
                    $potrojari->attension_office_unit_id = (!empty($this->request->data['attension_office_unit_id_final'])
                        ? ($this->request->data['attension_office_unit_id_final'])
                        : 0);
                    $potrojari->attension_office_name = (!empty($this->request->data['attension_office_name_final'])
                        ? trim($this->request->data['attension_office_name_final'])
                        : '');
                    $potrojari->attension_office_id = (!empty($this->request->data['attension_office_id_final'])
                        ? ($this->request->data['attension_office_id_final'])
                        : 0);
                    $potrojari->attension_officer_name = (!empty($this->request->data['attension_officer_name_final'])
                        ? trim($this->request->data['attension_officer_name_final'])
                        : '');
                    $potrojari->attension_officer_id = (!empty($this->request->data['attension_officer_id_final'])
                        ? ($this->request->data['attension_officer_id_final'])
                        : 0);

                /*
                 * Checking sender/onumodonkari/sovapotir sakkhor
                 *
                 */

                if (!empty($potrojari['officer_id'])) {
                    $usersTable = TableRegistry::get('Users');
                    $users = $usersTable->find()->where(['employee_record_id' => $potrojari['officer_id']])->first();
                    $username = $users['username'];
                    $options['token'] = sGenerateToken(['file'=>$username],['exp'=>time()+60*300]);

                    $data = $this->getSignature($username, 1, 0, true,null,$options);

                    if ($data == false) {
                        echo json_encode(array('status' => 'error',
                            'msg' => 'দুঃখিত! প্রেরক/অনুমোদনকারী এর স্বাক্ষর পাওয়া যায়নি। অনুগ্রহ করে সিস্টেমে স্বাক্ষর আপলোড করে খসড়া পুনরায় সংরক্ষণ করুন। ধন্যবাদ।'));
                        return false;
                    }
                }

                if (empty($potrojari['approval_officer_id'])) {
                    $users = $usersTable->find()->where(['employee_record_id' => $potrojari['approval_officer_id']])->first();
                    $username = $users['username'];
                    $options['token'] = sGenerateToken(['file'=>$username],['exp'=>time()+60*300]);


                    $data = $this->getSignature($username, 1, 0, true,null,$options);

                    if ($data == false) {
                        echo json_encode(array('status' => 'error',
                            'msg' => 'দুঃখিত! প্রেরক/অনুমোদনকারী এর স্বাক্ষর পাওয়া যায়নি। অনুগ্রহ করে সিস্টেমে স্বাক্ষর আপলোড করে খসড়া পুনরায় সংরক্ষণ করুন। ধন্যবাদ।'));
                        return false;
                    }
                }

                if ($potrojari['sovapoti_officer_id'] > 0) {
                    $users = $usersTable->find()->where(['employee_record_id' => $potrojari['sovapoti_officer_id']])->first();
                    $username = $users['username'];
                    $options['token'] = sGenerateToken(['file'=>$username],['exp'=>time()+60*300]);

                    $data = $this->getSignature($username, 1, 0, true,null,$options);

                    if ($data == false) {
                        echo json_encode(array('status' => 'error',
                            'msg' => 'দুঃখিত! সভাপতি এর স্বাক্ষর পাওয়া যায়নি। অনুগ্রহ করে সিস্টেমে স্বাক্ষর আপলোড করে খসড়া পুনরায় সংরক্ষণ করুন। ধন্যবাদ।'));
                        return false;
                    }
                }

                /*
                 * End
                 */

                $potrojari = $tablePotroJari->save($potrojari);
                /*
                 * Check if this potojari happening as internal or external
                 */
                $potrojari_internal = 0;

                $receiverGroupId = (!empty($this->request->data['receiver_group_id_final'])
                    ? (explode(';',
                        $this->request->data['receiver_group_id_final'])) : array());
                $receiverGroupDesignation = (!empty($this->request->data['receiver_group_designation_final'])
                    ? (explode(';',
                        $this->request->data['receiver_group_designation_final']))
                    : array());
                $receiverGroupMember = (!empty($this->request->data['receiver_group_member_final'])
                    ? (explode(';',
                        $this->request->data['receiver_group_member_final'])) : array());

                $receiverOfficeId = (!empty($this->request->data['receiver_office_id_final'])
                    ? (explode(';',
                        $this->request->data['receiver_office_id_final'])) : array());
                $receiverOfficeName = (!empty($this->request->data['receiver_office_name_final'])
                    ? (explode(';',
                        $this->request->data['receiver_office_name_final']))
                    : array());
                $receiverOfficerOrgId = (!empty($this->request->data['receiver_officer_designation_id_final'])
                    ? (explode(";",
                        $this->request->data['receiver_officer_designation_id_final']))
                    : array());
                $receiverOfficerUnitId = (!empty($this->request->data['receiver_office_unit_id_final'])
                    ? (explode(';',
                        $this->request->data['receiver_office_unit_id_final']))
                    : array());
                $receiverOfficeerUnitName = (!empty($this->request->data['receiver_office_unit_name_final'])
                    ? (explode(';',
                        $this->request->data['receiver_office_unit_name_final']))
                    : array());
                $receiverOfficerOrgLabel = (!empty($this->request->data['receiver_officer_designation_label_final'])
                    ? (explode(";",
                        $this->request->data['receiver_officer_designation_label_final']))
                    : array());
                $receiverOfficerId = (!empty($this->request->data['receiver_officer_id_final'])
                    ? (explode(";",
                        $this->request->data['receiver_officer_id_final'])) : array());
                $receiverOfficerName = (!empty($this->request->data['receiver_officer_name_final'])
                    ? (explode(";",
                        $this->request->data['receiver_officer_name_final']))
                    : array());
                $receiverOfficerEmail = (!empty($this->request->data['receiver_officer_email_final'])
                    ? (explode(";",
                        $this->request->data['receiver_officer_email_final']))
                    : array());
                $receiverOfficerHead = (!empty($this->request->data['receiver_office_head_final'])
                    ? (explode(";",
                        $this->request->data['receiver_office_head_final']))
                    : array());
                $receiverOfficerVisibleName = (!empty($this->request->data['receiver_visibleName_final'])
                    ? (explode(";", $this->request->data['receiver_visibleName_final']))
                    : array());
                $receiver_officer_mobile_final = (!empty($this->request->data['receiver_officer_mobile_final'])
                    ? (explode(";", $this->request->data['receiver_officer_mobile_final']))
                    : array());
                $receiver_sms_message_final = (!empty($this->request->data['receiver_sms_message_final'])
                    ? (explode(";", $this->request->data['receiver_sms_message_final']))
                    : array());
                $receiver_options_final = (!empty($this->request->data['receiver_options_final'])
                    ? (explode(";", $this->request->data['receiver_options_final']))
                    : array());

                if (!empty($receiverOfficerOrgLabel)) {
                    foreach ($receiverOfficerOrgLabel as $receiverKey => $receiverValue) {

                        if (empty($receiverValue)) continue;
                        $potrojariReceiver = $tablePotrojariReceiver->newEntity();

                        $potrojariReceiver->nothi_master_id = $potrojari->nothi_master_id;
                        $potrojariReceiver->nothi_part_no = $potrojari->nothi_part_no;
                        $potrojariReceiver->nothi_notes_id = $potrojari->nothi_notes_id;
                        $potrojariReceiver->nothi_potro_id = $potrojari->nothi_potro_id;
                        $potrojariReceiver->potro_type = $potrojari->potro_type;

                        $potrojariReceiver->potrojari_id = $potrojari->id;
                        $potrojariReceiver->potro_status = $potrojari->potro_status;
                        $potrojariReceiver->modified = $potrojari->modified;
                        $potrojariReceiver->created = $potrojari->modified;
                        $potrojariReceiver->modified_by = $potrojari->modified_by;
                        $potrojariReceiver->created_by = $potrojari->created_by;


                        $officeInfo = (!empty($receiverOfficeId[$receiverKey]) ? $table_instance_office->get($receiverOfficeId[$receiverKey])
                            : 0);

                        $potrojariReceiver->receiving_office_name =(!empty($receiverOfficeName[$receiverKey])?$receiverOfficeName[$receiverKey]: !empty($officeInfo)
                            ? (($potrojari->potrojari_language == 'eng' && $officeInfo['office_name_eng'])?trim($officeInfo['office_name_eng'] ): trim($officeInfo['office_name_bng'])) : (!empty($receiverOfficeName[$receiverKey])
                                ? $receiverOfficeName[$receiverKey] : ''));

                        $potrojariReceiver->receiving_office_id = isset($receiverOfficeId[$receiverKey])
                            ? $receiverOfficeId[$receiverKey] : 0;
                        $potrojariReceiver->receiving_office_unit_id = isset($receiverOfficerUnitId[$receiverKey])
                            ? $receiverOfficerUnitId[$receiverKey] : 0;
                        $potrojariReceiver->receiving_office_unit_name = isset($receiverOfficeerUnitName[$receiverKey])
                            ? trim($receiverOfficeerUnitName[$receiverKey]) : '';
                        $potrojariReceiver->receiving_officer_id = isset($receiverOfficerId[$receiverKey])
                            ? $receiverOfficerId[$receiverKey] : 0;
                        $potrojariReceiver->receiving_officer_designation_id = isset($receiverOfficerOrgId[$receiverKey])
                            ? $receiverOfficerOrgId[$receiverKey] : 0;
                        $potrojariReceiver->receiving_officer_designation_label = isset($receiverValue)
                            ? trim($receiverValue) : '';
                        $potrojariReceiver->receiving_officer_name = isset($receiverOfficerName[$receiverKey])
                            ? trim($receiverOfficerName[$receiverKey]) : '';
                        $potrojariReceiver->receiving_officer_email = isset($receiverOfficerEmail[$receiverKey])
                            ? trim($receiverOfficerEmail[$receiverKey]) : '';

                        $potrojariReceiver->receiving_office_head = isset($receiverOfficerHead[$receiverKey])
                            ? $receiverOfficerHead[$receiverKey] : 0;

                        $potrojariReceiver->visibleName = isset($receiverOfficerVisibleName[$receiverKey])
                            ? trim($receiverOfficerVisibleName[$receiverKey]) : '';

                        $potrojariReceiver->officer_mobile = isset($receiver_officer_mobile_final[$receiverKey])
                                ? $receiver_officer_mobile_final[$receiverKey] : '';
                        if(empty($potrojariReceiver->receiving_officer_designation_id)){
                            $potrojariReceiver->sms_message = !empty($receiver_sms_message_final[$receiverKey])
                                ? $receiver_sms_message_final[$receiverKey] : (!empty($receiverOfficerEmail[$receiverKey])?'আপনার পত্রটি ইমেইলে প্রেরণ করা হয়েছে':'আপনার পত্রটি ডাকে প্রেরণ করা হয়েছে');
                        }
                        // if user have any options
                          $potrojariReceiver->options = isset($receiver_options_final[$receiverKey])
                            ? $receiver_options_final[$receiverKey] : '';

                        if($potrojari_internal == 0 && $potrojariReceiver->receiving_officer_designation_id != 0){
                            $potrojari_internal = 1;
                        }
                        $tablePotrojariReceiver->save($potrojariReceiver);

                    }
                }

                if (!empty($receiverGroupId)) {
                    foreach ($receiverGroupId as $groupKey => $groupValue) {
                        if (empty($groupValue)) continue;
                        $groupDetails = $tablePotrojariGroup->find()->where(['id'=>$groupValue])->contain(['PotrojariGroupsUsers'])->first();
                        $groupvalueArray = $groupDetails['potrojari_groups_users'];
                        if (!empty($groupvalueArray)) {
                            $selectedDesignation = explode('~',
                                $receiverGroupDesignation[$groupKey]);
                            $selectedMember = $receiverGroupMember[$groupKey];

                            foreach ($groupvalueArray as $groupJsonKey => $groupJsonValue) {
                                $design = !empty($groupJsonValue['office_unit_organogram_id'])
                                    ? $groupJsonValue['office_unit_organogram_id']
                                    : (!empty($groupJsonValue['officer_email'])
                                        ? $groupJsonValue['officer_email'] : 0);
                                if (empty($design)) {
                                    continue;
                                } else {
                                    if (in_array($design, $selectedDesignation)) {
                                        //saving eng data if potrojari language is english
                                        if($potrojari->potrojari_language == 'eng'){
                                            $groupJsonValue['office_name'] = !empty($groupJsonValue['office_name_eng'])?trim($groupJsonValue['office_name_eng']):trim($groupJsonValue['office_name_bng']) ;
                                            $groupJsonValue['office_unit_name'] = !empty($groupJsonValue['office_unit_name_eng'])?trim($groupJsonValue['office_unit_name_eng']):trim($groupJsonValue['office_unit_name_bng']);
                                            $groupJsonValue['office_unit_organogram_name'] = !empty($groupJsonValue['office_unit_organogram_name_eng'])?trim($groupJsonValue['office_unit_organogram_name_eng']):trim($groupJsonValue['office_unit_organogram_name_bng']);
                                            $groupJsonValue['employee_name'] = !empty($groupJsonValue['employee_name_eng'])?trim($groupJsonValue['employee_name_eng']):trim($groupJsonValue['employee_name_bng']);
                                        }else{
                                            $groupJsonValue['office_name'] = trim($groupJsonValue['office_name_bng']) ;
                                            $groupJsonValue['office_unit_name'] = trim($groupJsonValue['office_unit_name_bng']);
                                            $groupJsonValue['office_unit_organogram_name'] = trim($groupJsonValue['office_unit_organogram_name_bng']);
                                            $groupJsonValue['employee_name'] = trim($groupJsonValue['employee_name_bng']);
                                        }
                                        //saving eng data if potrojari language is english

                                        $potrojariReceiver = $tablePotrojariReceiver->newEntity();

                                        $potrojariReceiver->nothi_master_id = $potrojari->nothi_master_id;
                                        $potrojariReceiver->nothi_part_no = $potrojari->nothi_part_no;
                                        $potrojariReceiver->nothi_notes_id = $potrojari->nothi_notes_id;
                                        $potrojariReceiver->nothi_potro_id = $potrojari->nothi_potro_id;
                                        $potrojariReceiver->potro_type = $potrojari->potro_type;
                                        $potrojariReceiver->potrojari_id = $potrojari->id;
                                        $potrojariReceiver->potro_status = $potrojari->potro_status;
                                        $potrojariReceiver->modified = $potrojari->modified;
                                        $potrojariReceiver->created = $potrojari->modified;
                                        $potrojariReceiver->modified_by = $potrojari->modified_by;
                                        $potrojariReceiver->created_by = $potrojari->created_by;

                                        $potrojariReceiver->receiving_office_name
                                            = !empty($groupJsonValue['office_name'])
                                            ? trim($groupJsonValue['office_name'])
                                            : '';

                                        $potrojariReceiver->group_id = $groupDetails['id'];

                                        $potrojariReceiver->group_name = trim($groupDetails['group_name']);
                                        $potrojariReceiver->group_member = $selectedMember;

                                        $potrojariReceiver->receiving_office_id = !empty($groupJsonValue['office_id'])
                                            ? $groupJsonValue['office_id'] : 0;

                                        $potrojariReceiver->receiving_office_unit_id
                                            = !empty($groupJsonValue['office_unit_id'])
                                            ? $groupJsonValue['office_unit_id']
                                            : 0;

                                        $potrojariReceiver->receiving_office_unit_name
                                            = !empty($groupJsonValue['office_unit_name'])
                                            ? trim($groupJsonValue['office_unit_name'])
                                            : '';

                                        $potrojariReceiver->receiving_officer_id
                                            = !empty($groupJsonValue['employee_id'])
                                            ? $groupJsonValue['employee_id']
                                            : 0;

                                        $potrojariReceiver->receiving_officer_designation_id
                                            = !empty($groupJsonValue['office_unit_organogram_id'])
                                            ? $groupJsonValue['office_unit_organogram_id']
                                            : '';

                                        $potrojariReceiver->receiving_officer_designation_label
                                            = !empty($groupJsonValue['office_unit_organogram_name'])
                                            ? trim($groupJsonValue['office_unit_organogram_name'])
                                            : '';

                                        $potrojariReceiver->receiving_officer_name
                                            = !empty($groupJsonValue['employee_name'])
                                            ? trim($groupJsonValue['employee_name'])
                                            : '';

                                        $potrojariReceiver->receiving_officer_email
                                            = !empty($groupJsonValue['officer_email'])
                                            ? trim($groupJsonValue['officer_email'])
                                            : '';
                                        if($potrojari_internal == 0 && $potrojariReceiver->receiving_officer_designation_id != 0){
                                            $potrojari_internal = 1;
                                        }

                                        $tablePotrojariReceiver->save($potrojariReceiver);
                                    }
                                }
                            }
                        }
                    }
                }

                $onulipiGroupId = (!empty($this->request->data['onulipi_group_id_final'])
                    ? (explode(';',
                        $this->request->data['onulipi_group_id_final'])) : array());

                $onulipiGroupDesignation = (!empty($this->request->data['onulipi_group_designation_final'])
                    ? (explode(';',
                        $this->request->data['onulipi_group_designation_final']))
                    : array());
                $onulipiGroupMember = (!empty($this->request->data['onulipi_group_member_final'])
                    ? (explode(';',
                        $this->request->data['onulipi_group_member_final'])) : array());

                $onulipiOfficeId = (!empty($this->request->data['onulipi_office_id_final'])
                    ? (explode(';',
                        $this->request->data['onulipi_office_id_final'])) : array());
                $onulipiOfficeName = (!empty($this->request->data['onulipi_office_name_final'])
                    ? (explode(';',
                        $this->request->data['onulipi_office_name_final'])) : array());
                $onulipiOfficerOrgId = (!empty($this->request->data['onulipi_officer_designation_id_final'])
                    ? (explode(";",
                        $this->request->data['onulipi_officer_designation_id_final']))
                    : array());
                $onulipiOfficerUnitId = (!empty($this->request->data['onulipi_office_unit_id_final'])
                    ? (explode(';',
                        $this->request->data['onulipi_office_unit_id_final']))
                    : array());
                $onulipiOfficeerUnitName = (!empty($this->request->data['onulipi_office_unit_name_final'])
                    ? (explode(';',
                        $this->request->data['onulipi_office_unit_name_final']))
                    : array());
                $onulipiOfficerOrgLabel = (!empty($this->request->data['onulipi_officer_designation_label_final'])
                    ? (explode(";",
                        $this->request->data['onulipi_officer_designation_label_final']))
                    : array());
                $onulipiOfficerId = (!empty($this->request->data['onulipi_officer_id_final'])
                    ? (explode(";",
                        $this->request->data['onulipi_officer_id_final'])) : array());
                $onulipiOfficerName = (!empty($this->request->data['onulipi_officer_name_final'])
                    ? (explode(";",
                        $this->request->data['onulipi_officer_name_final']))
                    : array());
                $onulipiOfficerEmail = (!empty($this->request->data['onulipi_officer_email_final'])
                    ? (explode(";",
                        $this->request->data['onulipi_officer_email_final']))
                    : array());
                $onulipiOfficerHead = (!empty($this->request->data['onulipi_office_head_final'])
                    ? (explode(";",
                        $this->request->data['onulipi_office_head_final']))
                    : array());
                $onulipiOfficerVisibleName = (!empty($this->request->data['onulipi_visibleName_final'])
                    ? (explode(";", $this->request->data['onulipi_visibleName_final']))
                    : array());
                $onulipi_officer_mobile_final = (!empty($this->request->data['onulipi_officer_mobile_final'])
                    ? (explode(";", $this->request->data['onulipi_officer_mobile_final']))
                    : array());
                $onulipi_sms_message_final = (!empty($this->request->data['onulipi_sms_message_final'])
                    ? (explode(";", $this->request->data['onulipi_sms_message_final']))
                    : array());
                $onulipi_options_final = (!empty($this->request->data['onulipi_options_final'])
                    ? (explode(";", $this->request->data['onulipi_options_final']))
                    : array());


                if (!empty($onulipiOfficerOrgLabel)) {
                    foreach ($onulipiOfficerOrgLabel as $onulipiKey => $onulipiValue) {

                        if (empty($onulipiValue)) continue;
                        $projapotiOnulipi = $tablePotrojariOnulipi->newEntity();

                        $projapotiOnulipi->potrojari_id = $potrojari->id;
                        $projapotiOnulipi->nothi_master_id = $potrojari->nothi_master_id;
                        $projapotiOnulipi->nothi_part_no = $potrojari->nothi_part_no;
                        $projapotiOnulipi->nothi_notes_id = $potrojari->nothi_notes_id;
                        $projapotiOnulipi->nothi_potro_id = $potrojari->nothi_potro_id;
                        $projapotiOnulipi->potro_type = $potrojari->potro_type;
                        $projapotiOnulipi->potro_status = $potrojari->potro_status;
                        $projapotiOnulipi->modified = $potrojari->modified;
                        $projapotiOnulipi->created = $potrojari->modified;
                        $projapotiOnulipi->modified_by = $potrojari->modified_by;
                        $projapotiOnulipi->created_by = $potrojari->created_by;

                        $officeInfo = (!empty($onulipiOfficeId[$onulipiKey])
                            ? $table_instance_office->get($onulipiOfficeId[$onulipiKey])
                            : 0);

                        $projapotiOnulipi->receiving_office_name = (!empty($onulipiOfficeName[$onulipiKey])
                                ? $onulipiOfficeName[$onulipiKey] : (($potrojari->potrojari_language == 'eng' && $officeInfo['office_name_eng'])?$officeInfo['office_name_eng']:$officeInfo['office_name_bng']));
                        $projapotiOnulipi->receiving_office_id = isset($onulipiOfficeId[$onulipiKey])
                            ? $onulipiOfficeId[$onulipiKey] : 0;
                        $projapotiOnulipi->receiving_office_unit_id = isset($onulipiOfficerUnitId[$onulipiKey])
                            ? $onulipiOfficerUnitId[$onulipiKey] : 0;
                        $projapotiOnulipi->receiving_office_unit_name = isset($onulipiOfficeerUnitName[$onulipiKey])
                            ? $onulipiOfficeerUnitName[$onulipiKey] : '';
                        $projapotiOnulipi->receiving_officer_id = isset($onulipiOfficerId[$onulipiKey])
                            ? $onulipiOfficerId[$onulipiKey] : 0;
                        $projapotiOnulipi->receiving_officer_designation_id = isset($onulipiOfficerOrgId[$onulipiKey])
                            ? $onulipiOfficerOrgId[$onulipiKey] : 0;
                        $projapotiOnulipi->receiving_officer_designation_label = isset($onulipiValue)
                            ? $onulipiValue : '';
                        $projapotiOnulipi->receiving_officer_name = isset($onulipiOfficerName[$onulipiKey])
                            ? $onulipiOfficerName[$onulipiKey] : '';
                        $projapotiOnulipi->receiving_officer_email = isset($onulipiOfficerEmail[$onulipiKey])
                            ? $onulipiOfficerEmail[$onulipiKey] : '';
                        $projapotiOnulipi->receiving_office_head = isset($onulipiOfficerHead[$onulipiKey])
                            ? $onulipiOfficerHead[$onulipiKey] : 0;
                        $projapotiOnulipi->visibleName = isset($onulipiOfficerVisibleName[$onulipiKey])
                            ? $onulipiOfficerVisibleName[$onulipiKey] : '';

                               $projapotiOnulipi->officer_mobile = isset($onulipi_officer_mobile_final[$onulipiKey])
                            ? $onulipi_officer_mobile_final[$onulipiKey] : '';
                    if(empty($projapotiOnulipi->receiving_officer_designation_id)){
                        $projapotiOnulipi->sms_message = !empty($onulipi_sms_message_final[$onulipiKey])
                            ? $onulipi_sms_message_final[$onulipiKey] : (!empty($onulipiOfficerEmail[$onulipiKey])?'আপনার পত্রটি ইমেইলে প্রেরণ করা হয়েছে':'আপনার পত্রটি ডাকে প্রেরণ করা হয়েছে');
                        }
                        // if user have any options
                          $projapotiOnulipi->options = isset($onulipi_options_final[$onulipiKey])
                            ? $onulipi_options_final[$onulipiKey] : '';

                        if($potrojari_internal == 0 && $projapotiOnulipi->receiving_officer_designation_id != 0){
                            $potrojari_internal = 1;
                        }

                        $tablePotrojariOnulipi->save($projapotiOnulipi);
                    }
                }

                if (!empty($onulipiGroupId)) {
                    foreach ($onulipiGroupId as $groupKey => $groupValue) {
                        if (empty($groupValue)) continue;
                         $groupDetails = $tablePotrojariGroup->find()->where(['id'=>$groupValue])->contain(['PotrojariGroupsUsers'])->first();
                        $groupvalueArray = $groupDetails['potrojari_groups_users'];
                        if (!empty($groupvalueArray)) {
                            $selectedDesignation = explode('~',
                                $onulipiGroupDesignation[$groupKey]);
                            $selectedMember = $onulipiGroupMember[$groupKey];
                            foreach ($groupvalueArray as $groupJsonKey => $groupJsonValue) {
                                $design = !empty($groupJsonValue['office_unit_organogram_id'])
                                    ? $groupJsonValue['office_unit_organogram_id']
                                    : (!empty($groupJsonValue['officer_email'])
                                        ? $groupJsonValue['officer_email'] : 0);
                                if (empty($design)) {
                                    continue;
                                } else {
                                    if (in_array($design, $selectedDesignation)) {
                                                                              //saving eng data if potrojari language is english
                                        if($potrojari->potrojari_language == 'eng'){
                                            $groupJsonValue['office_name'] = !empty($groupJsonValue['office_name_eng'])?trim($groupJsonValue['office_name_eng']):trim($groupJsonValue['office_name_bng']) ;
                                            $groupJsonValue['office_unit_name'] = !empty($groupJsonValue['office_unit_name_eng'])?trim($groupJsonValue['office_unit_name_eng']):trim($groupJsonValue['office_unit_name_bng']);
                                            $groupJsonValue['office_unit_organogram_name'] = !empty($groupJsonValue['office_unit_organogram_name_eng'])?trim($groupJsonValue['office_unit_organogram_name_eng']):trim($groupJsonValue['office_unit_organogram_name_bng']);
                                            $groupJsonValue['employee_name'] = !empty($groupJsonValue['employee_name_eng'])?trim($groupJsonValue['employee_name_eng']):trim($groupJsonValue['employee_name_bng']);
                                        }else{
                                            $groupJsonValue['office_name'] = trim($groupJsonValue['office_name_bng']) ;
                                            $groupJsonValue['office_unit_name'] = trim($groupJsonValue['office_unit_name_bng']);
                                            $groupJsonValue['office_unit_organogram_name'] = trim($groupJsonValue['office_unit_organogram_name_bng']);
                                            $groupJsonValue['employee_name'] = trim($groupJsonValue['employee_name_bng']);
                                        }
                                        //saving eng data if potrojari language is english

                                        $potrojariReceiver = $tablePotrojariOnulipi->newEntity();

                                        $potrojariReceiver->nothi_master_id = $potrojari->nothi_master_id;
                                        $potrojariReceiver->nothi_part_no = $potrojari->nothi_part_no;
                                        $potrojariReceiver->nothi_notes_id = $potrojari->nothi_notes_id;
                                        $potrojariReceiver->nothi_potro_id = $potrojari->nothi_potro_id;
                                        $potrojariReceiver->potro_type = $potrojari->potro_type;
                                        $potrojariReceiver->potrojari_id = $potrojari->id;
                                        $potrojariReceiver->potro_status = $potrojari->potro_status;
                                        $potrojariReceiver->modified = $potrojari->modified;
                                        $potrojariReceiver->created = $potrojari->modified;
                                        $potrojariReceiver->modified_by = $potrojari->modified_by;
                                        $potrojariReceiver->created_by = $potrojari->created_by;

                                        $potrojariReceiver->receiving_office_name
                                            = !empty($groupJsonValue['office_name'])
                                            ? trim($groupJsonValue['office_name'])
                                            : '';

                                        $potrojariReceiver->group_id = $groupDetails['id'];

                                        $potrojariReceiver->group_name = trim($groupDetails['group_name']);
                                        $potrojariReceiver->group_member = $selectedMember;

                                        $potrojariReceiver->receiving_office_id = !empty($groupJsonValue['office_id'])
                                            ? $groupJsonValue['office_id'] : 0;

                                        $potrojariReceiver->receiving_office_unit_id
                                            = !empty($groupJsonValue['office_unit_id'])
                                            ? $groupJsonValue['office_unit_id']
                                            : 0;

                                        $potrojariReceiver->receiving_office_unit_name
                                            = !empty($groupJsonValue['office_unit_name'])
                                            ? trim($groupJsonValue['office_unit_name'])
                                            : '';

                                        $potrojariReceiver->receiving_officer_id
                                            = !empty($groupJsonValue['employee_id'])
                                            ? $groupJsonValue['employee_id']
                                            : 0;

                                        $potrojariReceiver->receiving_officer_designation_id
                                            = !empty($groupJsonValue['office_unit_organogram_id'])
                                            ? $groupJsonValue['office_unit_organogram_id']
                                            : 0;

                                        $potrojariReceiver->receiving_officer_designation_label
                                            = !empty($groupJsonValue['office_unit_organogram_name'])
                                            ? trim($groupJsonValue['office_unit_organogram_name'])
                                            : '';

                                        $potrojariReceiver->receiving_officer_name
                                            = !empty($groupJsonValue['employee_name'])
                                            ? trim($groupJsonValue['employee_name'])
                                            : '';

                                        $potrojariReceiver->receiving_officer_email
                                            = !empty($groupJsonValue['officer_email'])
                                            ? trim($groupJsonValue['officer_email'])
                                            : '';
                                         if($potrojari_internal == 0 && $potrojariReceiver->receiving_officer_designation_id != 0){
                                            $potrojari_internal = 1;
                                        }

                                        $tablePotrojariOnulipi->save($potrojariReceiver);
                                    }
                                }
                            }
                        }
                    }
                }

                $this->request->data['attachment'] = explode(',',
                    $this->request->data['uploaded_attachments']);

                if (!empty($this->request->data['prapto_potro'])) {
                    foreach ($this->request->data['prapto_potro'] as $praptopotrokey => $praptopotro) {
                        if (!empty($praptopotro)) {

                            $file = $tablePotroAttachment->find()->where(['id' => $praptopotro])->first()->toArray();
                            $attachment_data = array();
                            $attachment_data = $file;
                            $attachment_data['potrojari_type'] = "Portojari";
                            $attachment_data['potrojari_id'] = $potrojari->id;
                            $attachment_data['is_inline'] = 1;
                            $attachment_data['potro_id'] = $praptopotro;

                            unset($attachment_data['id']);
                            $potrojari_attachment = $tablePotroJariAttachment->newEntity();
                            $potrojari_attachment = $tablePotroJariAttachment->patchEntity($potrojari_attachment,
                                $attachment_data);
                            $potrojari_attachment->modified_by = $potrojari->modified_by;
                            $potrojari_attachment->created_by = $potrojari->created_by;
                            $tablePotroJariAttachment->save($potrojari_attachment);
                            // Need to create Potrojari Pdf
                            if($attachment_data['attachment_type'] == 'text'){
                                $signHide = 0;
                                $folderPath = 'Nothi/'.$potrojari->office_id.'/';
                                $filename = "potro_" . $praptopotro . '_' . $nothiMasterId;
                                $command = ROOT."/bin/cake cronjob getPdfByNothiPotroId {$praptopotro} {$potrojari->office_id} {$signHide} '$folderPath' '$filename'> /dev/null 2>&1 &";

                                exec($command);
                            }

                        }
                    }

                }
                $attachment_names = (isset($this->request->data['uploaded_attachments_names']))?explode(',',$this->request->data['uploaded_attachments_names']):[];
                $attachment_rm_type = (isset($this->request->data['uploaded_attachments_rm_type']))?explode(',',$this->request->data['uploaded_attachments_rm_type']):[];
                if (!empty($this->request->data['attachment'])) {
                    foreach ($this->request->data['attachment'] as $k=> $file) {
                        if (!empty($file)) {
                            $attachment_data = array();
                            $attachment_data['potrojari_type'] = "Portojari";
                            $attachment_data['potrojari_id'] = $potrojari->id;
                            $attachment_data['attachment_description'] = $this->request->data['file_description'];
                            $attachment_data['user_file_name'] = (!empty($attachment_names[$k])?$attachment_names[$k]:'');

                            $attachment_data['attachment_type'] = $this->getMimeType($file);

                            $filepath = explode(FILE_FOLDER,$file);
                            $attachment_data['file_dir'] = FILE_FOLDER;
                            $attachment_data['file_name'] = $filepath[1];
                            $explode = explode('?token=',$attachment_data['file_name']);
                            $attachment_data['file_name'] = $explode[0];

                            $potrojari_attachment = $tablePotroJariAttachment->newEntity();
                            $potrojari_attachment = $tablePotroJariAttachment->patchEntity($potrojari_attachment,
                                $attachment_data);
                            $potrojari_attachment->modified_by = $potrojari->modified_by;
                            $potrojari_attachment->created_by = $potrojari->created_by;

                            if(!empty($attachment_rm_type[$k])){
                                $potrojari_attachment->options = json_encode(['rm_attachment_type' => $attachment_rm_type[$k]]);
                            }
                            $tablePotroJariAttachment->save($potrojari_attachment);
                        }
                    }
                }

                if (!empty($this->request->data['contentbody'])) {
                    $attachment_data = array();
                    $attachment_data['potrojari_type'] = "Portojari";
                    $attachment_data['potrojari_id'] = $potrojari->id;
                    $attachment_data['attachment_description'] = '';
                    $attachment_data['attachment_type'] = 'text';
                    $attachment_data['file_dir'] = '';
                    $attachment_data['file_name'] = '';
                    if($potrojari->potro_type == 30){
                        $attachment_data['content_body'] = (!empty($attached_html)?($attached_html . "<br/>"):'') .(!empty($potrojari->potro_cover)?$potrojari->potro_cover:'');
                        $attachment_data['content_cover'] =  '';
                    }else{
                        $attachment_data['content_cover'] = (!empty($potrojari->potro_cover)?$potrojari->potro_cover:'');
                        $attachment_data['content_body'] = (!empty($attached_html)?($attached_html . "<br/>"):'') . ($potrojari->potro_description);
                    }

                    $attachment_data['meta_data'] = $potrojari->meta_data;

                    $potrojari_attachment = $tablePotroJariAttachment->newEntity();
                    $potrojari_attachment = $tablePotroJariAttachment->patchEntity($potrojari_attachment,
                        $attachment_data);
                    $potrojari_attachment->modified_by = $potrojari->modified_by;
                    $potrojari_attachment->created_by = $potrojari->created_by;
                    $tablePotroJariAttachment->save($potrojari_attachment);
                }

                $nothiPartInformation->is_active = 0;

                $nothiPartsTable->save($nothiPartInformation);

                $conn->commit();
                if (!$this->NotificationSet('draft', array("1", "Potro"), 2,
                    $current_office_selection, [], $potrojari->potro_subject)
                ) {

                }
                if(!empty($potrojari_internal) && $potrojari_internal == 1){
                    $potrojari->potrojari_internal = 1;
                    $tablePotroJari->updateAll(['potrojari_internal' => 1], ['id' => $potrojari->id]);
                }else if(isset($potrojari_internal) && $potrojari_internal == 0){
                    $potrojari->potrojari_internal = 0;
                    $tablePotroJari->updateAll(['potrojari_internal' => 0], ['id' => $potrojari->id]);
                }
                return $potrojari;
            } catch (\Exception $ex) {

                $conn->rollback();
                echo json_encode(array("status" => 'error', 'msg' => __('পত্রজারি করা সম্ভব হচ্ছেনা'),'reason' => $ex->getMessage(),'trace'=> ''));
                return false;
            }
        }
        return true;
    }

    private function updatePotrojari($potrojari, $id, $nothi_office = 0,$employee_office,$api = 0)
    {
        $response = ['status' => 'error','msg' => ''];
        $current_office_selection = $employee_office;

        $otherNothi = false;
        if ($nothi_office != 0 && $nothi_office != $employee_office['office_id']) {
            $otherNothi = true;

            $this->switchOffice($nothi_office, 'MainNothiOffice');
        } else {
            $nothi_office = $employee_office['office_id'];
        }
        $this->set('nothi_office', $nothi_office);
        $table_instance_office = TableRegistry::get('Offices');
        $table_instance_unit = TableRegistry::get('OfficeUnits');

        TableRegistry::remove('Potrojari');
        TableRegistry::remove('PotrojariReceiver');
        TableRegistry::remove('PotrojariOnulipi');
        TableRegistry::remove('PotrojariAttachments');
        TableRegistry::remove('NothiPotroAttachments');
        TableRegistry::remove('PotrojariGroups');
        $tablePotroJari = TableRegistry::get('Potrojari');
        $tablePotrojariReceiver = TableRegistry::get('PotrojariReceiver');
        $tablePotrojariOnulipi = TableRegistry::get('PotrojariOnulipi');
        $tablePotroJariAttachment = TableRegistry::get('PotrojariAttachments');
        $tableNothiPotroAttachment = TableRegistry::get('NothiPotroAttachments');
        $tablePotrojariGroup = TableRegistry::get('PotrojariGroups');

        if ($this->request->is(['post', 'put'])) {
            //check digital signature
            // skip if soft token -1
                if(!empty($current_office_selection) && $current_office_selection['default_sign'] > 0 && intval($this->request->data['can_potrojari']) > 0  && isset($this->request->data['soft_token']) && $this->request->data['soft_token'] != -1){
                    $this->loadComponent('DigitalSignatureRelated');
                    $res = $this->DigitalSignatureRelated->signPotrojari($id,$this->request->data['soft_token'],$current_office_selection['default_sign'],$current_office_selection);
                    if($res['status'] =='error'){
                       return $res;
                    }
                }
            $conn = ConnectionManager::get('default');

            try {
                $conn->begin();

                if (!$potrojari->is_summary_nothi) {
                    $tablePotrojariReceiver->deleteAll(['potrojari_id' => $id]);
                }
                $tablePotrojariOnulipi->deleteAll(['potrojari_id' => $id]);
                $tablePotroJariAttachment->deleteAll(['potrojari_id' => $id]);

                $potrojari->sarok_no = !empty($this->request->data['sender_sarok_no']) ? $this->request->data['sender_sarok_no'] : '';


                $tableHeader = TableRegistry::get('PotrojariHeaderSettings');
                $headerSettings = [];
                $settings = [];
                $heading = $tableHeader->findByOfficeId($current_office_selection['office_id'])->first();
                if(!empty($heading)){
                    $settings = jsonA($heading['potrojari_head']);
                }
                if(empty($settings['banner_position'])){
                    $headerSettings['banner_position'] = 'left';
                }else{
                    $headerSettings['banner_position'] = $settings['banner_position'];
                }
                if(empty($settings['banner_width'])){
                    $headerSettings['banner_width'] = 'auto';
                }else{
                    $headerSettings['banner_width'] = $settings['banner_width'];
                }
                if(empty($this->request->data['meta_data'])){

                    $this->request->data['meta_data'] = json_encode([
                            'potro_header'=>0,
                            'potro_header_banner'=>base64_encode($this->potrojariDynamicHeader($current_office_selection['office_id'],true))
                        ]+$headerSettings);
                }else{
                    $header = jsonA($this->request->data['meta_data']);
                        $header['potro_header_banner'] = base64_encode($this->potrojariDynamicHeader($current_office_selection['office_id'],true));

                    $this->request->data['meta_data'] = json_encode($header+$headerSettings);
                }


                $potrojari->meta_data = !empty($this->request->data['meta_data'])?$this->request->data['meta_data']:'';
                $potrojari->potrojari_date = date("Y-m-d H:i:s");
                $potrojari->potro_subject = trim($this->request->data['dak_subject']);
                $potrojari->potro_security_level = isset($this->request->data['potro_security_level'])
                    ? $this->request->data['potro_security_level'] : 0;
                $potrojari->potro_priority_level = isset($this->request->data['potro_priority_level'])
                    ? $this->request->data['potro_priority_level'] : 0;

                $potrojari->modified = date("Y-m-d H:i:s");

                if ($potrojari->is_summary_nothi) {
                    $potrojari->potro_cover = $this->request->data['contentbody'];
                    $potrojari->potro_description = $this->request->data['contentbody2'];
                } else {
                    //if potro type CS template then potro cover need to be saved
                    if($potrojari->potro_type == 30 ){
                        $potrojari->potro_cover = isset($this->request->data['content_cover'])?trim($this->request->data['content_cover']):'';
                    }
                        $potrojari->potro_description = $this->request->data['contentbody'];
                     // onumodonkari
                    $potrojari->office_id = !empty($this->request->data['sender_office_id_final'])
                        ? $this->request->data['sender_office_id_final'] : 0;
                    $officeInfo = $table_instance_office->get($potrojari->office_id);
                    $potrojari->officer_designation_id = !empty($this->request->data['sender_officer_designation_id_final'])
                        ? $this->request->data['sender_officer_designation_id_final']
                        : 0;
                    $rec_office_unit = $table_instance_unit->getOfficeUnitBy_designationId($potrojari->officer_designation_id);
                    $potrojari->officer_designation_label = !empty($this->request->data['sender_officer_designation_label_final'])
                        ? trim($this->request->data['sender_officer_designation_label_final'])
                        : '';
                    $potrojari->office_name = trim($officeInfo['office_name_bng']);
                    $potrojari->officer_id = !empty($this->request->data['sender_officer_id_final'])
                        ? $this->request->data['sender_officer_id_final'] : 0;
                    $potrojari->office_unit_id = $rec_office_unit['id'];
                    $potrojari->office_unit_name = trim($rec_office_unit['unit_name_bng']);
                    $potrojari->officer_name = !empty($this->request->data['sender_officer_name_final'])
                        ? trim($this->request->data['sender_officer_name_final']) : '';
                    $potrojari->officer_visibleName = !empty($this->request->data['sender_officer_visibleName'])
                    ? trim($this->request->data['sender_officer_visibleName']) : '';
                    $potrojari->officer_visibleDesignation = !empty($this->request->data['sender_officer_visibleDesignation'])
                    ? trim($this->request->data['sender_officer_visibleDesignation']) : '';

                     //prerok
                    $potrojari->approval_office_id = !empty($this->request->data['approval_office_id_final'])
                        ? $this->request->data['approval_office_id_final'] : 0;

                    $officeInfo = $table_instance_office->get($potrojari->approval_office_id);
                    $potrojari->approval_officer_designation_id = !empty($this->request->data['approval_officer_designation_id_final'])
                        ? $this->request->data['approval_officer_designation_id_final']
                        : 0;

                    $rec_office_unit = $table_instance_unit->getOfficeUnitBy_designationId($potrojari->approval_officer_designation_id);
                    $potrojari->approval_officer_designation_label = !empty($this->request->data['approval_officer_designation_label_final'])
                        ? trim($this->request->data['approval_officer_designation_label_final'])
                        : '';

                    $potrojari->approval_office_name = trim($officeInfo['office_name_bng']);
                    $potrojari->approval_officer_id = !empty($this->request->data['approval_officer_id_final'])
                        ? $this->request->data['approval_officer_id_final'] : 0;

                    $potrojari->approval_office_unit_id = $rec_office_unit['id'];
                    $potrojari->approval_office_unit_name = trim($rec_office_unit['unit_name_bng']);
                    $potrojari->approval_officer_name = !empty($this->request->data['approval_officer_name_final'])
                        ? trim($this->request->data['approval_officer_name_final'])
                        : '';
                    $potrojari->approval_visibleName = !empty($this->request->data['approval_visibleName'])
                    ? trim($this->request->data['approval_visibleName']) : '';
                $potrojari->approval_visibleDesignation = !empty($this->request->data['approval_visibleDesignation'])
                    ? trim($this->request->data['approval_visibleDesignation']) : '';

                    if ($potrojari->potro_type == 17) {
//                        $potrojari->can_potrojari = 0;//($current_office_selection['office_unit_organogram_id']  == $potrojari->sovapoti_officer_designation_id ) ? 1 : 0;
                        $potrojari->can_potrojari = ($current_office_selection['office_unit_organogram_id']
                            == $potrojari->sovapoti_officer_designation_id) ? intval($this->request->data['can_potrojari'])
                            : $potrojari->can_potrojari;
                    } else {
                        $potrojari->can_potrojari = ($current_office_selection['office_unit_organogram_id']
                            == $potrojari->officer_designation_id) ? intval($this->request->data['can_potrojari'])
                            : 0;
                    }
                }

                //sovapoti
                if ($potrojari->potro_type == 17) {
                    $potrojari->sovapoti_office_id = !empty($this->request->data['sovapoti_office_id_final'])
                        ? ($this->request->data['sovapoti_office_id_final'])
                        : 0;

                    $potrojari->sovapoti_officer_id = (!empty($this->request->data['sovapoti_officer_id_final'])
                        ? ($this->request->data['sovapoti_officer_id_final'])
                        : 0);

                    $potrojari->sovapoti_officer_designation_id = (!empty($this->request->data['sovapoti_officer_designation_id_final'])
                        ? ($this->request->data['sovapoti_officer_designation_id_final'])
                        : 0);

                    $potrojari->sovapoti_officer_designation_label = (!empty($this->request->data['sovapoti_officer_designation_label_final'])
                        ? trim($this->request->data['sovapoti_officer_designation_label_final'])
                        : 0);

                    $potrojari->sovapoti_officer_name = (!empty($this->request->data['sovapoti_officer_name_final'])
                        ? trim($this->request->data['sovapoti_officer_name_final'])
                        : 0);
                    $potrojari->sovapoti_visibleName = !empty($this->request->data['sovapoti_visibleName'])
                        ? trim($this->request->data['sovapoti_visibleName']) : '';
                    $potrojari->sovapoti_visibleDesignation = !empty($this->request->data['sovapoti_visibleDesignation'])
                        ? trim($this->request->data['sovapoti_visibleDesignation']) : '';

                }
                                    // attension
                    $potrojari->attension_officer_designation_label = (!empty($this->request->data['attension_officer_designation_label_final'])
                        ? trim($this->request->data['attension_officer_designation_label_final'])
                        : 0);
                    $potrojari->attension_officer_designation_id = (!empty($this->request->data['attension_officer_designation_id_final'])
                        ? ($this->request->data['attension_officer_designation_id_final'])
                        : 0);
                    $potrojari->attension_office_unit_name = (!empty($this->request->data['attension_office_unit_name_final'])
                        ? trim($this->request->data['attension_office_unit_name_final'])
                        : '');
                    $potrojari->attension_office_unit_id = (!empty($this->request->data['attension_office_unit_id_final'])
                        ? ($this->request->data['attension_office_unit_id_final'])
                        : 0);
                    $potrojari->attension_office_name = (!empty($this->request->data['attension_office_name_final'])
                        ? trim($this->request->data['attension_office_name_final'])
                        : '');
                    $potrojari->attension_office_id = (!empty($this->request->data['attension_office_id_final'])
                        ? ($this->request->data['attension_office_id_final'])
                        : 0);
                    $potrojari->attension_officer_name = (!empty($this->request->data['attension_officer_name_final'])
                        ? trim($this->request->data['attension_officer_name_final'])
                        : '');
                    $potrojari->attension_officer_id = (!empty($this->request->data['attension_officer_id_final'])
                        ? ($this->request->data['attension_officer_id_final'])
                        : 0);
                $potrojari = $tablePotroJari->save($potrojari);

                if (!$potrojari->is_summary_nothi) {
                    $receiverGroupId = (!empty($this->request->data['receiver_group_id_final'])
                        ? (explode(';',
                            $this->request->data['receiver_group_id_final']))
                        : array());
                    $receiverGroupDesignation = (!empty($this->request->data['receiver_group_designation_final'])
                        ? (explode(';',
                            $this->request->data['receiver_group_designation_final']))
                        : array());
                    $receiverGroupMember = (!empty($this->request->data['receiver_group_member_final'])
                        ? (explode(';',
                            $this->request->data['receiver_group_member_final']))
                        : array());

                    $receiverOfficeId = (!empty($this->request->data['receiver_office_id_final'])
                        ? (explode(';',
                            $this->request->data['receiver_office_id_final']))
                        : array());
                    $receiverOfficeName = (!empty($this->request->data['receiver_office_name_final'])
                        ? (explode(';',
                            $this->request->data['receiver_office_name_final']))
                        : array());

                    $receiverOfficerOrgId = (!empty($this->request->data['receiver_officer_designation_id_final'])
                        ? (explode(";", $this->request->data['receiver_officer_designation_id_final']))
                        : array());
                    $receiverOfficerUnitId = (!empty($this->request->data['receiver_office_unit_id_final'])
                        ? (explode(';',
                            $this->request->data['receiver_office_unit_id_final']))
                        : array());
                    $receiverOfficeerUnitName = (!empty($this->request->data['receiver_office_unit_name_final'])
                        ? (explode(';',
                            $this->request->data['receiver_office_unit_name_final']))
                        : array());
                    $receiverOfficerOrgLabel = (!empty($this->request->data['receiver_officer_designation_label_final'])
                        ? (explode(";",
                            $this->request->data['receiver_officer_designation_label_final']))
                        : array());
                    $receiverOfficerId = (!empty($this->request->data['receiver_officer_id_final'])
                        ? (explode(";",
                            $this->request->data['receiver_officer_id_final']))
                        : array());
                    $receiverOfficerName = (!empty($this->request->data['receiver_officer_name_final'])
                        ? (explode(";",
                            $this->request->data['receiver_officer_name_final']))
                        : array());
                    $receiverOfficerEmail = (!empty($this->request->data['receiver_officer_email_final'])
                        ? (explode(";",
                            $this->request->data['receiver_officer_email_final']))
                        : array());
                    $receiverOfficerHead = (!empty($this->request->data['receiver_office_head_final'])
                        ? (explode(";",
                            $this->request->data['receiver_office_head_final']))
                        : array());
                    $receiverOfficerVisibleName = (!empty($this->request->data['receiver_visibleName_final'])
                        ? (explode(";", $this->request->data['receiver_visibleName_final']))
                        : array());
                      $receiver_officer_mobile_final = (!empty($this->request->data['receiver_officer_mobile_final'])
                    ? (explode(";", $this->request->data['receiver_officer_mobile_final']))
                    : array());
                $receiver_sms_message_final = (!empty($this->request->data['receiver_sms_message_final'])
                    ? (explode(";", $this->request->data['receiver_sms_message_final']))
                    : array());
                $receiver_options_final = (!empty($this->request->data['receiver_options_final'])
                    ? (explode(";", $this->request->data['receiver_options_final']))
                    : array());
                     /*
                 * Check if this potojari happening as internal or external
                 */
                $potrojari_internal = 0;
                    if (!empty($receiverOfficerOrgLabel)) {
                        foreach ($receiverOfficerOrgLabel as $receiverKey => $receiverValue) {

                            if (empty($receiverValue)) continue;
                            $potrojariReceiver = $tablePotrojariReceiver->newEntity();

                            $potrojariReceiver->potrojari_id = $potrojari->id;
                            $potrojariReceiver->nothi_master_id = $potrojari->nothi_master_id;
                            $potrojariReceiver->nothi_part_no = $potrojari->nothi_part_no;
                            $potrojariReceiver->nothi_notes_id = $potrojari->nothi_notes_id;
                            $potrojariReceiver->nothi_potro_id = $potrojari->nothi_potro_id;
                            $potrojariReceiver->potro_type = $potrojari->potro_type;
                            $potrojariReceiver->potro_status = $potrojari->potro_status;
                            $potrojariReceiver->modified = $potrojari->modified;
                            $potrojariReceiver->created = $potrojari->modified;
                            $potrojariReceiver->created_by = $potrojari->created_by;
                            $potrojariReceiver->modified_by = $potrojari->modified_by;


                            $officeInfo = (!empty($receiverOfficeId[$receiverKey])
                                ? $table_instance_office->get($receiverOfficeId[$receiverKey])
                                : 0);

                            $potrojariReceiver->receiving_office_name = !empty($officeInfo)
                                ? $officeInfo['office_name_bng'] : (!empty($receiverOfficeName[$receiverKey])
                                    ? $receiverOfficeName[$receiverKey] : '');

                            $potrojariReceiver->receiving_office_id = isset($receiverOfficeId[$receiverKey])
                                ? $receiverOfficeId[$receiverKey] : 0;
                            $potrojariReceiver->receiving_office_unit_id = isset($receiverOfficerUnitId[$receiverKey])
                                ? $receiverOfficerUnitId[$receiverKey] : 0;
                            $potrojariReceiver->receiving_office_unit_name = isset($receiverOfficeerUnitName[$receiverKey])
                                ? $receiverOfficeerUnitName[$receiverKey] : '';
                            $potrojariReceiver->receiving_officer_id = isset($receiverOfficerId[$receiverKey])
                                ? $receiverOfficerId[$receiverKey] : 0;
                            $potrojariReceiver->receiving_officer_designation_id
                                = isset($receiverOfficerOrgId[$receiverKey]) ? $receiverOfficerOrgId[$receiverKey]
                                : 0;
                            $potrojariReceiver->receiving_officer_designation_label
                                = isset($receiverValue) ? $receiverValue : '';
                            $potrojariReceiver->receiving_officer_name = isset($receiverOfficerName[$receiverKey])
                                ? $receiverOfficerName[$receiverKey] : '';
                            $potrojariReceiver->receiving_officer_email = isset($receiverOfficerEmail[$receiverKey])
                                ? $receiverOfficerEmail[$receiverKey] : '';
                            $potrojariReceiver->receiving_office_head = isset($receiverOfficerHead[$receiverKey])
                                ? $receiverOfficerHead[$receiverKey] : 0;
                            $potrojariReceiver->visibleName = isset($receiverOfficerVisibleName[$receiverKey])
                                ? $receiverOfficerVisibleName[$receiverKey] : '';

                            $potrojariReceiver->officer_mobile = isset($receiver_officer_mobile_final[$receiverKey])
                            ? $receiver_officer_mobile_final[$receiverKey] : '';
                            if(empty($potrojariReceiver->receiving_officer_designation_id)){
                            $potrojariReceiver->sms_message = !empty($receiver_sms_message_final[$receiverKey])
                                ? $receiver_sms_message_final[$receiverKey] : (!empty($receiverOfficerEmail[$receiverKey])?'আপনার পত্রটি ইমেইলে প্রেরণ করা হয়েছে':'আপনার পত্রটি ডাকে প্রেরণ করা হয়েছে');
                             }
                              // if user have any options
                          $potrojariReceiver->options = isset($receiver_options_final[$receiverKey])
                            ? $receiver_options_final[$receiverKey] : '';

                            if($potrojari_internal == 0 && $potrojariReceiver->receiving_officer_designation_id != 0){
                            $potrojari_internal = 1;
                        }
                            $tablePotrojariReceiver->save($potrojariReceiver);
                        }
                    }

                    if (!empty($receiverGroupId)) {
                        foreach ($receiverGroupId as $groupKey => $groupValue) {
                            if (empty($groupValue)) {
                                continue;
                            }
                             $groupDetails = $tablePotrojariGroup->find()->where(['id'=>$groupValue])->contain(['PotrojariGroupsUsers'])->first();
                        $groupvalueArray = $groupDetails['potrojari_groups_users'];
                            if (!empty($groupvalueArray)) {
                                $selectedDesignation = explode('~',
                                    $receiverGroupDesignation[$groupKey]);
                                $selectedMember = $receiverGroupMember[$groupKey];
                                foreach ($groupvalueArray as $groupJsonKey => $groupJsonValue) {
                                    $design = !empty($groupJsonValue['office_unit_organogram_id'])
                                        ? $groupJsonValue['office_unit_organogram_id']
                                        : (!empty($groupJsonValue['officer_email'])
                                            ? $groupJsonValue['officer_email']
                                            : 0);
                                    if (empty($design)) {
                                        continue;
                                    } else {
                                        if (in_array($design,
                                            $selectedDesignation)) {
                                                                                    //saving eng data if potrojari language is english
                                        if($potrojari->potrojari_language == 'eng'){
                                            $groupJsonValue['office_name'] = !empty($groupJsonValue['office_name_eng'])?trim($groupJsonValue['office_name_eng']):trim($groupJsonValue['office_name_bng']) ;
                                            $groupJsonValue['office_unit_name'] = !empty($groupJsonValue['office_unit_name_eng'])?trim($groupJsonValue['office_unit_name_eng']):trim($groupJsonValue['office_unit_name_bng']);
                                            $groupJsonValue['office_unit_organogram_name'] = !empty($groupJsonValue['office_unit_organogram_name_eng'])?trim($groupJsonValue['office_unit_organogram_name_eng']):trim($groupJsonValue['office_unit_organogram_name_bng']);
                                            $groupJsonValue['employee_name'] = !empty($groupJsonValue['employee_name_eng'])?trim($groupJsonValue['employee_name_eng']):trim($groupJsonValue['employee_name_bng']);
                                        }else{
                                            $groupJsonValue['office_name'] = trim($groupJsonValue['office_name_bng']);
                                            $groupJsonValue['office_unit_name'] = trim($groupJsonValue['office_unit_name_bng']);
                                            $groupJsonValue['office_unit_organogram_name'] = trim($groupJsonValue['office_unit_organogram_name_bng']);
                                            $groupJsonValue['employee_name'] = trim($groupJsonValue['employee_name_bng']);
                                        }
                                        //saving eng data if potrojari language is english

                                            $potrojariReceiver = $tablePotrojariReceiver->newEntity();

                                            $potrojariReceiver->potrojari_id = $potrojari->id;
                                            $potrojariReceiver->nothi_master_id = $potrojari->nothi_master_id;
                                            $potrojariReceiver->nothi_part_no = $potrojari->nothi_part_no;
                                            $potrojariReceiver->nothi_notes_id = $potrojari->nothi_notes_id;
                                            $potrojariReceiver->nothi_potro_id = $potrojari->nothi_potro_id;
                                            $potrojariReceiver->potro_type = $potrojari->potro_type;
                                            $potrojariReceiver->potro_status = $potrojari->potro_status;
                                            $potrojariReceiver->modified = $potrojari->modified;
                                            $potrojariReceiver->created = $potrojari->modified;
                                            $potrojariReceiver->created_by = $potrojari->created_by;
                                            $potrojariReceiver->modified_by = $potrojari->modified_by;

                                            $potrojariReceiver->receiving_office_name
                                                = !empty($groupJsonValue['office_name'])
                                                ? trim($groupJsonValue['office_name'])
                                                : '';

                                            $potrojariReceiver->group_id = $groupDetails['id'];

                                            $potrojariReceiver->group_name = trim($groupDetails['group_name']);
                                            $potrojariReceiver->group_member = $selectedMember;

                                            $potrojariReceiver->receiving_office_id
                                                = !empty($groupJsonValue['office_id'])
                                                ? $groupJsonValue['office_id']
                                                : 0;

                                            $potrojariReceiver->receiving_office_unit_id
                                                = !empty($groupJsonValue['office_unit_id'])
                                                ? $groupJsonValue['office_unit_id']
                                                : 0;

                                            $potrojariReceiver->receiving_office_unit_name
                                                = !empty($groupJsonValue['office_unit_name'])
                                                ? trim($groupJsonValue['office_unit_name'])
                                                : '';

                                            $potrojariReceiver->receiving_officer_id
                                                = !empty($groupJsonValue['employee_id'])
                                                ? $groupJsonValue['employee_id']
                                                : 0;

                                            $potrojariReceiver->receiving_officer_designation_id
                                                = !empty($groupJsonValue['office_unit_organogram_id'])
                                                ? $groupJsonValue['office_unit_organogram_id']
                                                : 0;

                                            $potrojariReceiver->receiving_officer_designation_label
                                                = !empty($groupJsonValue['office_unit_organogram_name'])
                                                ? trim($groupJsonValue['office_unit_organogram_name'])
                                                : '';

                                            $potrojariReceiver->receiving_officer_name
                                                = !empty($groupJsonValue['employee_name'])
                                                ? trim($groupJsonValue['employee_name'])
                                                : '';

                                            $potrojariReceiver->receiving_officer_email
                                                = !empty($groupJsonValue['officer_email'])
                                                ? trim($groupJsonValue['officer_email'])
                                                :'';
                                            if($potrojari_internal == 0 && $potrojariReceiver->receiving_officer_designation_id != 0){
                                                $potrojari_internal = 1;
                                            }

                                            $tablePotrojariReceiver->save($potrojariReceiver);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    $onulipiGroupId = (!empty($this->request->data['onulipi_group_id_final'])
                        ? (explode(';',
                            $this->request->data['onulipi_group_id_final']))
                        : array());
                    $onulipiGroupDesignation = (!empty($this->request->data['onulipi_group_designation_final'])
                        ? (explode(';',
                            $this->request->data['onulipi_group_designation_final']))
                        : array());
                    $onulipiGroupMember = (!empty($this->request->data['onulipi_group_member_final'])
                        ? (explode(';',
                            $this->request->data['onulipi_group_member_final']))
                        : array());

                    $onulipiOfficeId = (!empty($this->request->data['onulipi_office_id_final'])
                        ? (explode(';',
                            $this->request->data['onulipi_office_id_final']))
                        : array());
                    $onulipiOfficeName = (!empty($this->request->data['onulipi_office_name_final'])
                        ? (explode(';',
                            $this->request->data['onulipi_office_name_final']))
                        : array());
                    $onulipiOfficerOrgId = (!empty($this->request->data['onulipi_officer_designation_id_final'])
                        ? (explode(";",
                            $this->request->data['onulipi_officer_designation_id_final']))
                        : array());
                    $onulipiOfficerUnitId = (!empty($this->request->data['onulipi_office_unit_id_final'])
                        ? (explode(';',
                            $this->request->data['onulipi_office_unit_id_final']))
                        : array());
                    $onulipiOfficeerUnitName = (!empty($this->request->data['onulipi_office_unit_name_final'])
                        ? (explode(';',
                            $this->request->data['onulipi_office_unit_name_final']))
                        : array());
                    $onulipiOfficerOrgLabel = (!empty($this->request->data['onulipi_officer_designation_label_final'])
                        ? (explode(";",
                            $this->request->data['onulipi_officer_designation_label_final']))
                        : array());
                    $onulipiOfficerId = (!empty($this->request->data['onulipi_officer_id_final'])
                        ? (explode(";",
                            $this->request->data['onulipi_officer_id_final']))
                        : array());
                    $onulipiOfficerName = (!empty($this->request->data['onulipi_officer_name_final'])
                        ? (explode(";",
                            $this->request->data['onulipi_officer_name_final']))
                        : array());
                    $onulipiOfficerEmail = (!empty($this->request->data['onulipi_officer_email_final'])
                        ? (explode(";",
                            $this->request->data['onulipi_officer_email_final']))
                        : array());
                    $onulipiOfficerHead = (!empty($this->request->data['onulipi_office_head_final'])
                        ? (explode(";",
                            $this->request->data['onulipi_office_head_final']))
                        : array());
                    $onulipiOfficerVisibleName = (!empty($this->request->data['onulipi_visibleName_final'])
                        ? (explode(";", $this->request->data['onulipi_visibleName_final']))
                        : array());
                    $onulipi_officer_mobile_final = (!empty($this->request->data['onulipi_officer_mobile_final'])
                    ? (explode(";", $this->request->data['onulipi_officer_mobile_final']))
                    : array());
                    $onulipi_sms_message_final = (!empty($this->request->data['onulipi_sms_message_final'])
                        ? (explode(";", $this->request->data['onulipi_sms_message_final']))
                        : array());
                     $onulipi_options_final = (!empty($this->request->data['onulipi_options_final'])
                    ? (explode(";", $this->request->data['onulipi_options_final']))
                    : array());

                    if (!empty($onulipiOfficerOrgLabel)) {
                        foreach ($onulipiOfficerOrgLabel as $onulipiKey => $onulipiValue) {

                            if (empty($onulipiValue)) continue;
                            $projapotiOnulipi = $tablePotrojariOnulipi->newEntity();

                            $projapotiOnulipi->potrojari_id = $potrojari->id;
                            $projapotiOnulipi->nothi_master_id = $potrojari->nothi_master_id;
                            $projapotiOnulipi->nothi_part_no = $potrojari->nothi_part_no;
                            $projapotiOnulipi->nothi_notes_id = $potrojari->nothi_notes_id;
                            $projapotiOnulipi->nothi_potro_id = $potrojari->nothi_potro_id;
                            $projapotiOnulipi->potro_type = $potrojari->potro_type;

                           $projapotiOnulipi->potro_status = $potrojari->potro_status;
                            $projapotiOnulipi->modified = $potrojari->modified;
                            $projapotiOnulipi->created = $potrojari->modified;
                            $projapotiOnulipi->created_by = $potrojari->created_by;
                            $projapotiOnulipi->modified_by = $potrojari->modified_by;


                            $officeInfo = (!empty($onulipiOfficeId[$onulipiKey])
                                ? $table_instance_office->get($onulipiOfficeId[$onulipiKey])
                                : 0);

                            $projapotiOnulipi->receiving_office_name = !empty($officeInfo)
                                ? $officeInfo['office_name_bng'] : (!empty($onulipiOfficeName[$onulipiKey])
                                    ? trim($onulipiOfficeName[$onulipiKey]) : '');
                            $projapotiOnulipi->receiving_office_id = isset($onulipiOfficeId[$onulipiKey])
                                ? $onulipiOfficeId[$onulipiKey] : 0;
                            $projapotiOnulipi->receiving_office_unit_id = isset($onulipiOfficerUnitId[$onulipiKey])
                                ? $onulipiOfficerUnitId[$onulipiKey] : 0;
                            $projapotiOnulipi->receiving_office_unit_name = isset($onulipiOfficeerUnitName[$onulipiKey])
                                ? trim($onulipiOfficeerUnitName[$onulipiKey]) : '';
                            $projapotiOnulipi->receiving_officer_id = isset($onulipiOfficerId[$onulipiKey])
                                ? $onulipiOfficerId[$onulipiKey] : 0;
                            $projapotiOnulipi->receiving_officer_designation_id = isset($onulipiOfficerOrgId[$onulipiKey])
                                ? $onulipiOfficerOrgId[$onulipiKey] : 0;
                            $projapotiOnulipi->receiving_officer_designation_label
                                = isset($onulipiValue) ? trim($onulipiValue) : '';
                            $projapotiOnulipi->receiving_officer_name = isset($onulipiOfficerName[$onulipiKey])
                                ? trim($onulipiOfficerName[$onulipiKey]) : '';
                            $projapotiOnulipi->receiving_officer_email = isset($onulipiOfficerEmail[$onulipiKey])
                                ? trim($onulipiOfficerEmail[$onulipiKey]) : '';
                            $projapotiOnulipi->receiving_office_head = isset($onulipiOfficerHead[$onulipiKey])
                                ? $onulipiOfficerHead[$onulipiKey] : 0;
                            $projapotiOnulipi->visibleName = isset($onulipiOfficerVisibleName[$onulipiKey])
                                ? trim($onulipiOfficerVisibleName[$onulipiKey]) : '';

                                $projapotiOnulipi->officer_mobile = isset($onulipi_officer_mobile_final[$onulipiKey])
                                ? $onulipi_officer_mobile_final[$onulipiKey] : '';
                            if(empty($projapotiOnulipi->receiving_officer_designation_id)){
                                $projapotiOnulipi->sms_message = !empty($onulipi_sms_message_final[$onulipiKey])
                                ? $onulipi_sms_message_final[$onulipiKey] : (!empty($onulipiOfficerEmail[$onulipiKey])?'আপনার পত্রটি ইমেইলে প্রেরণ করা হয়েছে':'আপনার পত্রটি ডাকে প্রেরণ করা হয়েছে');
                            }
                             // if user have any options
                          $projapotiOnulipi->options = isset($onulipi_options_final[$onulipiKey])
                            ? $onulipi_options_final[$onulipiKey] : '';

                             if($potrojari_internal == 0 && $projapotiOnulipi->receiving_officer_designation_id != 0){
                            $potrojari_internal = 1;
                            }

                            $tablePotrojariOnulipi->save($projapotiOnulipi);
                        }
                    }

                    $selectedDesignation = [];
                    if (!empty($onulipiGroupId)) {
                        foreach ($onulipiGroupId as $groupKey => $groupValue) {
                            if (empty($groupValue)) continue;
                               $groupDetails = $tablePotrojariGroup->find()->where(['id'=>$groupValue])->contain(['PotrojariGroupsUsers'])->first();
                        $groupvalueArray = $groupDetails['potrojari_groups_users'];
                            if (!empty($groupvalueArray)) {
                                $selectedDesignation = explode('~',
                                    $onulipiGroupDesignation[$groupKey]);
                                $selectedMember = $onulipiGroupMember[$groupKey];

                                foreach ($groupvalueArray as $groupJsonKey => $groupJsonValue) {
                                    $design = !empty($groupJsonValue['office_unit_organogram_id'])
                                        ? $groupJsonValue['office_unit_organogram_id']
                                        : (!empty($groupJsonValue['officer_email'])
                                            ? $groupJsonValue['officer_email']
                                            : 0);
                                    if (empty($design)) {
                                        continue;
                                    } else {
                                        if (in_array($design,
                                            $selectedDesignation)) {
                                                   //saving eng data if potrojari language is english
                                        if($potrojari->potrojari_language == 'eng'){
                                            $groupJsonValue['office_name'] = !empty($groupJsonValue['office_name_eng'])?trim($groupJsonValue['office_name_eng']):trim($groupJsonValue['office_name_bng']) ;
                                            $groupJsonValue['office_unit_name'] = !empty($groupJsonValue['office_unit_name_eng'])?trim($groupJsonValue['office_unit_name_eng']):trim($groupJsonValue['office_unit_name_bng']);
                                            $groupJsonValue['office_unit_organogram_name'] = !empty($groupJsonValue['office_unit_organogram_name_eng'])?trim($groupJsonValue['office_unit_organogram_name_eng']):trim($groupJsonValue['office_unit_organogram_name_bng']);
                                            $groupJsonValue['employee_name'] = !empty($groupJsonValue['employee_name_eng'])?trim($groupJsonValue['employee_name_eng']):trim($groupJsonValue['employee_name_bng']);
                                        }else{
                                            $groupJsonValue['office_name'] = trim($groupJsonValue['office_name_bng']) ;
                                            $groupJsonValue['office_unit_name'] = trim($groupJsonValue['office_unit_name_bng']);
                                            $groupJsonValue['office_unit_organogram_name'] = trim($groupJsonValue['office_unit_organogram_name_bng']);
                                            $groupJsonValue['employee_name'] = trim($groupJsonValue['employee_name_bng']);
                                        }
                                        //saving eng data if potrojari language is english

                                            $potrojariReceiver = $tablePotrojariOnulipi->newEntity();

                                            $potrojariReceiver->potrojari_id = $potrojari->id;
                                            $potrojariReceiver->nothi_master_id = $potrojari->nothi_master_id;
                                            $potrojariReceiver->nothi_part_no = $potrojari->nothi_part_no;
                                            $potrojariReceiver->nothi_notes_id = $potrojari->nothi_notes_id;
                                            $potrojariReceiver->nothi_potro_id = $potrojari->nothi_potro_id;
                                            $potrojariReceiver->potro_type = $potrojari->potro_type;
                                            $potrojariReceiver->potro_status = $potrojari->potro_status;
                                            $potrojariReceiver->modified = $potrojari->modified;
                                            $potrojariReceiver->created = $potrojari->modified;
                                            $potrojariReceiver->created_by = $potrojari->created_by;
                                            $potrojariReceiver->modified_by = $potrojari->modified_by;

                                            $potrojariReceiver->receiving_office_name
                                                = !empty($groupJsonValue['office_name'])
                                                ? trim($groupJsonValue['office_name'])
                                                : '';

                                            $potrojariReceiver->group_id = $groupDetails['id'];

                                            $potrojariReceiver->group_name = trim($groupDetails['group_name']);
                                            $potrojariReceiver->group_member = $selectedMember;

                                            $potrojariReceiver->receiving_office_id
                                                = !empty($groupJsonValue['office_id'])
                                                ? $groupJsonValue['office_id']
                                                : 0;

                                            $potrojariReceiver->receiving_office_unit_id
                                                = !empty($groupJsonValue['office_unit_id'])
                                                ? $groupJsonValue['office_unit_id']
                                                : 0;

                                            $potrojariReceiver->receiving_office_unit_name
                                                = !empty($groupJsonValue['office_unit_name'])
                                                ? trim($groupJsonValue['office_unit_name'])
                                                : '';

                                            $potrojariReceiver->receiving_officer_id
                                                = !empty($groupJsonValue['employee_id'])
                                                ? $groupJsonValue['employee_id']
                                                : 0;

                                            $potrojariReceiver->receiving_officer_designation_id
                                                = !empty($groupJsonValue['office_unit_organogram_id'])
                                                ? $groupJsonValue['office_unit_organogram_id']
                                                : 0;

                                            $potrojariReceiver->receiving_officer_designation_label
                                                = !empty($groupJsonValue['office_unit_organogram_name'])
                                                ? trim($groupJsonValue['office_unit_organogram_name'])
                                                : '';

                                            $potrojariReceiver->receiving_officer_name
                                                = !empty($groupJsonValue['employee_name'])
                                                ? trim($groupJsonValue['employee_name'])
                                                : '';

                                            $potrojariReceiver->receiving_officer_email
                                                = !empty($groupJsonValue['officer_email']) && $groupJsonValue['officer_email']!='undefined'
                                                ? trim($groupJsonValue['officer_email'])
                                                : '';
                                             if($potrojari_internal == 0 && $potrojariReceiver->receiving_officer_designation_id != 0){
                                            $potrojari_internal = 1;
                                            }

                                            $tablePotrojariOnulipi->save($potrojariReceiver);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                $this->request->data['attachment'] = explode(',',
                    $this->request->data['uploaded_attachments']);

                if (!empty($this->request->data['prapto_potro'])) {
                    foreach ($this->request->data['prapto_potro'] as $praptopotrokey => $praptopotro) {
                        if (!empty($praptopotro)) {
                            $file = $tableNothiPotroAttachment->find()->where(['id' => $praptopotro])->first()->toArray();
                            $attachment_data = array();
                            $attachment_data = $file;
                            $attachment_data['potrojari_type'] = "Portojari";
                            $attachment_data['potrojari_id'] = $potrojari->id;

                            $attachment_data['is_inline'] = 1;
                            $attachment_data['potro_id'] = $praptopotro;

                            $potrojari_attachment = $tablePotroJariAttachment->newEntity();
                            $potrojari_attachment = $tablePotroJariAttachment->patchEntity($potrojari_attachment,
                                $attachment_data);
                            $potrojari_attachment->created_by = $potrojari->created_by;
                            $potrojari_attachment->modified_by = $potrojari->modified_by;
                            $tablePotroJariAttachment->save($potrojari_attachment);
                             // Need to create Potrojari Pdf
                            if($attachment_data['attachment_type'] == 'text'){
                                $signHide = 0;
                                $folderPath = 'Nothi/'.$potrojari->office_id.'/';
                                $filename = "potro_" . $praptopotro . '_' . $potrojari->nothi_part_no;
                                $command = ROOT."/bin/cake cronjob getPdfByNothiPotroId {$praptopotro} {$potrojari->office_id} {$signHide} '$folderPath' '$filename' > /dev/null 2>&1 &";

                                exec($command);
//                                return $command;
                            }
                        }
                    }
                }
                $attachment_names = explode(',',$this->request->data['uploaded_attachments_names']);
                $attachment_rm_type = (isset($this->request->data['uploaded_attachments_rm_type']))?explode(',',$this->request->data['uploaded_attachments_rm_type']):[];
                if (!empty($this->request->data['attachment'])) {
                    foreach ($this->request->data['attachment'] as $k => $file) {
                        if (!empty($file)) {
                            $attachment_data = array();
                            $attachment_data['potrojari_type'] = "Portojari";
                            $attachment_data['potrojari_id'] = $potrojari->id;
                            $attachment_data['attachment_description'] = $this->request->data['file_description'];
                            $attachment_data['user_file_name'] = (!empty($attachment_names[$k])?$attachment_names[$k]:'');

                            $attachment_data['attachment_type'] = $this->getMimeType($file);
                            if ($potrojari->is_summary_nothi) {
                                $attachment_data['is_summary_nothi'] = 1;
                            }

                            $filepath = explode(FILE_FOLDER,
                                $file);
                            $attachment_data['file_dir'] = FILE_FOLDER;
                            $attachment_data['file_name'] = $filepath[1];
                            $explode = explode('?token=',$attachment_data['file_name']);
                            $attachment_data['file_name'] = $explode[0];

                            $potrojari_attachment = $tablePotroJariAttachment->newEntity();
                            $potrojari_attachment = $tablePotroJariAttachment->patchEntity($potrojari_attachment,
                                $attachment_data);
                            $potrojari_attachment->created_by = $potrojari->created_by;
                            $potrojari_attachment->modified_by = $potrojari->modified_by;
                            if(!empty($attachment_rm_type[$k])){
                                $potrojari_attachment->options = json_encode(['rm_attachment_type' => $attachment_rm_type[$k]]);
                            }
                            $tablePotroJariAttachment->save($potrojari_attachment);
                        }
                    }
                }

                if (!empty($this->request->data['inline_attachment'])) {
                    $attachments = explode(',',
                        $this->request->data['inline_attachment']);
                    foreach ($attachments as $ke => $val) {
                        $file = $tableNothiPotroAttachment->get($val);
                        if (!empty($file)) {
                            $attachment_data = array();
                            $attachment_data['potrojari_type'] = "Portojari";
                            $attachment_data['potrojari_id'] = $potrojari->id;
                            $attachment_data['attachment_description'] = $file['file_description'];

                            $attachment_data['attachment_type'] = $file['attachment_type'];
                            $attachment_data['is_summary_nothi'] = 1;
                            $attachment_data['is_inline'] = 1;
                            $attachment_data['meta_data'] = $file['meta_data'];

                            $attachment_data['file_dir'] = $this->request->webroot;
                            $attachment_data['file_name'] = $file['file_name'];
                            $attachment_data['content_body'] = $file['content_body'];
                            $attachment_data['content_cover'] = $file['content_body'];

                            $potrojari_attachment = $tablePotroJariAttachment->newEntity();
                            $potrojari_attachment = $tablePotroJariAttachment->patchEntity($potrojari_attachment,
                                $attachment_data);
                            $potrojari_attachment->created_by = $potrojari->created_by;
                            $tablePotroJariAttachment->save($potrojari_attachment);
                        }
                    }
                }


                if (!empty($this->request->data['contentbody'])) {
                    $attachment_data = array();
                    $attachment_data['potrojari_type'] = "Portojari";
                    $attachment_data['potrojari_id'] = $potrojari->id;
                    $attachment_data['attachment_description'] = '';
                    $attachment_data['attachment_type'] = 'text';
                    $attachment_data['file_dir'] = '';
                    $attachment_data['file_name'] = '';
                    $attachment_data['meta_data'] = $potrojari->meta_data;
                    if ($potrojari->is_summary_nothi == 0) {
                        if($potrojari->potro_type == 30){
                            $attachment_data['content_cover'] =  '';
                            $attachment_data['content_body'] = (!empty($potrojari->attached_potro)?($potrojari->attached_potro . "<br/>"):'') .(!empty($potrojari->potro_cover)?$potrojari->potro_cover:'');
                        }else{
                            $attachment_data['content_body'] = (!empty($potrojari->attached_potro)?($potrojari->attached_potro . "<br/>"):'') . ($potrojari->potro_description);
                            $attachment_data['content_cover'] = (!empty($potrojari->potro_cover)?$potrojari->potro_cover:'');
                        }

                    } else {
                        $attachment_data['is_summary_nothi'] = 1;
                        $attachment_data['content_body'] = $this->request->data['contentbody2'];
                        $attachment_data['content_cover'] = $this->request->data['contentbody'];
                    }

                    $potrojari_attachment = $tablePotroJariAttachment->newEntity();
                    $potrojari_attachment = $tablePotroJariAttachment->patchEntity($potrojari_attachment,
                        $attachment_data);
                    $potrojari_attachment->created_by = $potrojari->created_by;
                    $potrojari_attachment->modified_by = $potrojari->modified_by;
                    $tablePotroJariAttachment->save($potrojari_attachment);
                }

                $conn->commit();
                //check digital signature
                if(!empty($current_office_selection) && $current_office_selection['default_sign'] > 0 && intval($this->request->data['can_potrojari']) > 0 && isset($this->request->data['soft_token']) && $this->request->data['soft_token'] != -1){
                    $this->loadComponent('DigitalSignatureRelated');
                    $res = $this->DigitalSignatureRelated->signPotrojari($id,$this->request->data['soft_token'],$current_office_selection['default_sign'],$current_office_selection);
                    if($res['status'] =='error'){
                        return $res;
                    }
                }
                if (!$this->NotificationSet('draftedit', array("1"), 2,
                    $current_office_selection, '', $potrojari->potro_subject)
                ) {

                }
                 if(!empty($potrojari_internal) && $potrojari_internal == 1){
                    $potrojari->potrojari_internal = 1;
                    $tablePotroJari->updateAll(['potrojari_internal' => 1], ['id' => $potrojari->id]);
                }else if(isset($potrojari_internal) && $potrojari_internal == 0){
                     $potrojari->potrojari_internal = 0;
                     $tablePotroJari->updateAll(['potrojari_internal' => 0], ['id' => $potrojari->id]);
                 }
                return ['status' => 'success'];
            } catch (\Exception $ex) {

                $conn->rollback();
                //echo json_encode(array("status" => 'error', 'msg' => __('খসড়া সংশোধন করা সম্ভব হচ্ছেনা')));
                $response['msg'] = 'খসড়া সংশোধন করা সম্ভব হচ্ছেনা';
                $response['reason'] = $this->makeEncryptedData($ex->getMessage());
                return $response;
            }
        }
        return $response;
    }

    private function updateSummaryNothi($potrojari, $id, $nothi_office = 0)
    {

        $employee_office = $current_office_selection = $this->getCurrentDakSection();
        $otherNothi = false;
        if ($nothi_office != 0 && $nothi_office != $employee_office['office_id']) {
            $otherNothi = true;

            $this->switchOffice($nothi_office, 'MainNothiOffice');
        } else {
            $nothi_office = $employee_office['office_id'];
        }
        $this->set('nothi_office', $nothi_office);
        $table_instance_office = TableRegistry::get('Offices');
        $table_instance_unit = TableRegistry::get('OfficeUnits');

        TableRegistry::remove('Potrojari');
        TableRegistry::remove('PotrojariReceiver');
        TableRegistry::remove('PotrojariOnulipi');
        TableRegistry::remove('PotrojariAttachments');
        TableRegistry::remove('NothiPotroAttachments');
        TableRegistry::remove('PotrojariGroups');
        $tablePotroJari = TableRegistry::get('Potrojari');
        $tablePotrojariReceiver = TableRegistry::get('PotrojariReceiver');
        $tablePotrojariOnulipi = TableRegistry::get('PotrojariOnulipi');
        $tablePotroJariAttachment = TableRegistry::get('PotrojariAttachments');
        $tableNothiPotroAttachment = TableRegistry::get('NothiPotroAttachments');
        $tablePotrojariGroup = TableRegistry::get('PotrojariGroups');

        if ($this->request->is(['post', 'put'])) {
            $conn = ConnectionManager::get('default');

            try {
                $conn->begin();

                if (!$potrojari->is_summary_nothi) {
                    $tablePotrojariReceiver->deleteAll(['potrojari_id' => $id]);
                }
                $tablePotrojariOnulipi->deleteAll(['potrojari_id' => $id]);
                $tablePotroJariAttachment->deleteAll(['potrojari_id' => $id]);

                $potrojari->sarok_no = !empty($this->request->data['sender_sarok_no']) ? $this->request->data['sender_sarok_no'] : '';
                $potrojari->meta_data = !empty($this->request->data['meta_data'])?$this->request->data['meta_data']:'';
                $potrojari->potrojari_date = date("Y-m-d H:i:s");
                $potrojari->potro_subject = $this->request->data['dak_subject'];
                $potrojari->potro_security_level = isset($this->request->data['potro_security_level'])
                    ? $this->request->data['potro_security_level'] : 0;
                $potrojari->potro_priority_level = isset($this->request->data['potro_priority_level'])
                    ? $this->request->data['potro_priority_level'] : 0;

                $potrojari->modified = date("Y-m-d H:i:s");

                if ($potrojari->is_summary_nothi) {
                    $potrojari->potro_cover = $this->request->data['contentbody'];
                    $potrojari->potro_description = $this->request->data['contentbody2'];
                } else {
                    $potrojari->potro_description = $this->request->data['contentbody'];
                    $potrojari->office_id = !empty($this->request->data['sender_office_id_final'])
                        ? $this->request->data['sender_office_id_final'] : 0;
                    $officeInfo = $table_instance_office->get($potrojari->office_id);
                    $potrojari->officer_designation_id = !empty($this->request->data['sender_officer_designation_id_final'])
                        ? $this->request->data['sender_officer_designation_id_final']
                        : 0;
                    $rec_office_unit = $table_instance_unit->getOfficeUnitBy_designationId($potrojari->officer_designation_id);
                    $potrojari->officer_designation_label = !empty($this->request->data['sender_officer_designation_label_final'])
                        ? $this->request->data['sender_officer_designation_label_final']
                        : '';
                    $potrojari->office_name = $officeInfo['office_name_bng'];
                    $potrojari->officer_id = !empty($this->request->data['sender_officer_id_final'])
                        ? $this->request->data['sender_officer_id_final'] : 0;
                    $potrojari->office_unit_id = $rec_office_unit['id'];
                    $potrojari->office_unit_name = $rec_office_unit['unit_name_bng'];
                    $potrojari->officer_name = !empty($this->request->data['sender_officer_name_final'])
                        ? $this->request->data['sender_officer_name_final'] : '';

                    $potrojari->approval_office_id = !empty($this->request->data['approval_office_id_final'])
                        ? $this->request->data['approval_office_id_final'] : 0;

                    $officeInfo = $table_instance_office->get($potrojari->approval_office_id);
                    $potrojari->approval_officer_designation_id = !empty($this->request->data['approval_officer_designation_id_final'])
                        ? $this->request->data['approval_officer_designation_id_final']
                        : 0;

                    $rec_office_unit = $table_instance_unit->getOfficeUnitBy_designationId($potrojari->approval_officer_designation_id);
                    $potrojari->approval_officer_designation_label = !empty($this->request->data['approval_officer_designation_label_final'])
                        ? $this->request->data['approval_officer_designation_label_final']
                        : '';

                    $potrojari->approval_office_name = $officeInfo['office_name_bng'];
                    $potrojari->approval_officer_id = !empty($this->request->data['approval_officer_id_final'])
                        ? $this->request->data['approval_officer_id_final'] : 0;

                    $potrojari->approval_office_unit_id = $rec_office_unit['id'];
                    $potrojari->approval_office_unit_name = $rec_office_unit['unit_name_bng'];
                    $potrojari->approval_officer_name = !empty($this->request->data['approval_officer_name_final'])
                        ? $this->request->data['approval_officer_name_final']
                        : '';

                    if ($potrojari->potro_type == 17) {
//                        $potrojari->can_potrojari = 0;//($current_office_selection['office_unit_organogram_id']  == $potrojari->sovapoti_officer_designation_id ) ? 1 : 0;
                        $potrojari->can_potrojari = ($current_office_selection['office_unit_organogram_id']
                            == $potrojari->sovapoti_officer_designation_id) ? intval($this->request->data['can_potrojari'])
                            : $potrojari->can_potrojari;
                    } else {
                        $potrojari->can_potrojari = ($current_office_selection['office_unit_organogram_id']
                            == $potrojari->officer_designation_id) ? intval($this->request->data['can_potrojari'])
                            : $potrojari->can_potrojari;
                    }
                }

                //sovapoti
                if ($potrojari->potro_type == 17) {
                    $potrojari->sovapoti_office_id = !empty($this->request->data['sovapoti_office_id_final'])
                        ? ($this->request->data['sovapoti_office_id_final'])
                        : 0;

                    $potrojari->sovapoti_officer_id = (!empty($this->request->data['sovapoti_officer_id_final'])
                        ? ($this->request->data['sovapoti_officer_id_final'])
                        : 0);

                    $potrojari->sovapoti_officer_designation_id = (!empty($this->request->data['sovapoti_officer_designation_id_final'])
                        ? ($this->request->data['sovapoti_officer_designation_id_final'])
                        : 0);

                    $potrojari->sovapoti_officer_designation_label = (!empty($this->request->data['sovapoti_officer_designation_label_final'])
                        ? ($this->request->data['sovapoti_officer_designation_label_final'])
                        : 0);

                    $potrojari->sovapoti_officer_name = (!empty($this->request->data['sovapoti_officer_name_final'])
                        ? ($this->request->data['sovapoti_officer_name_final'])
                        : 0);
                }
                                    // attension
                    $potrojari->attension_officer_designation_label = (!empty($this->request->data['attension_officer_designation_label_final'])
                        ? ($this->request->data['attension_officer_designation_label_final'])
                        : 0);
                    $potrojari->attension_officer_designation_id = (!empty($this->request->data['attension_officer_designation_id_final'])
                        ? ($this->request->data['attension_officer_designation_id_final'])
                        : 0);
                    $potrojari->attension_office_unit_name = (!empty($this->request->data['attension_office_unit_name_final'])
                        ? ($this->request->data['attension_office_unit_name_final'])
                        : '');
                    $potrojari->attension_office_unit_id = (!empty($this->request->data['attension_office_unit_id_final'])
                        ? ($this->request->data['attension_office_unit_id_final'])
                        : 0);
                    $potrojari->attension_office_name = (!empty($this->request->data['attension_office_name_final'])
                        ? ($this->request->data['attension_office_name_final'])
                        : '');
                    $potrojari->attension_office_id = (!empty($this->request->data['attension_office_id_final'])
                        ? ($this->request->data['attension_office_id_final'])
                        : 0);
                    $potrojari->attension_officer_name = (!empty($this->request->data['attension_officer_name_final'])
                        ? ($this->request->data['attension_officer_name_final'])
                        : '');
                    $potrojari->attension_officer_id = (!empty($this->request->data['attension_officer_id_final'])
                        ? ($this->request->data['attension_officer_id_final'])
                        : 0);
                $potrojari = $tablePotroJari->save($potrojari);

                if (!$potrojari->is_summary_nothi) {
                    $receiverGroupId = (!empty($this->request->data['receiver_group_id_final'])
                        ? (explode(';',
                            $this->request->data['receiver_group_id_final']))
                        : array());
                    $receiverGroupDesignation = (!empty($this->request->data['receiver_group_designation_final'])
                        ? (explode(';',
                            $this->request->data['receiver_group_designation_final']))
                        : array());
                    $receiverGroupMember = (!empty($this->request->data['receiver_group_member_final'])
                        ? (explode(';',
                            $this->request->data['receiver_group_member_final']))
                        : array());

                    $receiverOfficeId = (!empty($this->request->data['receiver_office_id_final'])
                        ? (explode(';',
                            $this->request->data['receiver_office_id_final']))
                        : array());
                    $receiverOfficeName = (!empty($this->request->data['receiver_office_name_final'])
                        ? (explode(';',
                            $this->request->data['receiver_office_name_final']))
                        : array());
                    $receiverOfficerOrgId = (!empty($this->request->data['receiver_officer_designation_id_final'])
                        ? (explode(";",
                            $this->request->data['receiver_officer_designation_id_final']))
                        : array());
                    $receiverOfficerUnitId = (!empty($this->request->data['receiver_office_unit_id_final'])
                        ? (explode(';',
                            $this->request->data['receiver_office_unit_id_final']))
                        : array());
                    $receiverOfficeerUnitName = (!empty($this->request->data['receiver_office_unit_name_final'])
                        ? (explode(';',
                            $this->request->data['receiver_office_unit_name_final']))
                        : array());
                    $receiverOfficerOrgLabel = (!empty($this->request->data['receiver_officer_designation_label_final'])
                        ? (explode(";",
                            $this->request->data['receiver_officer_designation_label_final']))
                        : array());
                    $receiverOfficerId = (!empty($this->request->data['receiver_officer_id_final'])
                        ? (explode(";",
                            $this->request->data['receiver_officer_id_final']))
                        : array());
                    $receiverOfficerName = (!empty($this->request->data['receiver_officer_name_final'])
                        ? (explode(";",
                            $this->request->data['receiver_officer_name_final']))
                        : array());
                    $receiverOfficerEmail = (!empty($this->request->data['receiver_officer_email_final'])
                        ? (explode(";",
                            $this->request->data['receiver_officer_email_final']))
                        : array());

                     /*
                 * Check if this potojari happening as internal or external
                 */
                $potrojari_internal = 0;
                    if (!empty($receiverOfficerOrgLabel)) {
                        foreach ($receiverOfficerOrgLabel as $receiverKey => $receiverValue) {

                            if (empty($receiverValue)) continue;
                            $potrojariReceiver = $tablePotrojariReceiver->newEntity();

                            $potrojariReceiver->potrojari_id = $potrojari->id;
                            $potrojariReceiver->nothi_master_id = $potrojari->nothi_master_id;
                            $potrojariReceiver->nothi_part_no = $potrojari->nothi_part_no;
                            $potrojariReceiver->nothi_notes_id = $potrojari->nothi_notes_id;
                            $potrojariReceiver->nothi_potro_id = $potrojari->nothi_potro_id;
                            $potrojariReceiver->potro_type = $potrojari->potro_type;
                            $potrojariReceiver->potro_status = $potrojari->potro_status;
                            $potrojariReceiver->modified = $potrojari->modified;
                            $potrojariReceiver->created = $potrojari->modified;
                            $potrojariReceiver->created_by = $potrojari->created_by;
                            $potrojariReceiver->modified_by = $potrojari->modified_by;


                            $officeInfo = (!empty($receiverOfficeId[$receiverKey])
                                ? $table_instance_office->get($receiverOfficeId[$receiverKey])
                                : 0);

                            $potrojariReceiver->receiving_office_name = !empty($officeInfo)
                                ? $officeInfo['office_name_bng'] : (!empty($receiverOfficeName[$receiverKey])
                                    ? $receiverOfficeName[$receiverKey] : '');

                            $potrojariReceiver->receiving_office_id = isset($receiverOfficeId[$receiverKey])
                                ? $receiverOfficeId[$receiverKey] : 0;
                            $potrojariReceiver->receiving_office_unit_id = isset($receiverOfficerUnitId[$receiverKey])
                                ? $receiverOfficerUnitId[$receiverKey] : 0;
                            $potrojariReceiver->receiving_office_unit_name = isset($receiverOfficeerUnitName[$receiverKey])
                                ? $receiverOfficeerUnitName[$receiverKey] : '';
                            $potrojariReceiver->receiving_officer_id = isset($receiverOfficerId[$receiverKey])
                                ? $receiverOfficerId[$receiverKey] : 0;
                            $potrojariReceiver->receiving_officer_designation_id
                                = isset($receiverOfficerOrgId[$receiverKey]) ? $receiverOfficerOrgId[$receiverKey]
                                : 0;
                            $potrojariReceiver->receiving_officer_designation_label
                                = isset($receiverValue) ? $receiverValue : '';
                            $potrojariReceiver->receiving_officer_name = isset($receiverOfficerName[$receiverKey])
                                ? $receiverOfficerName[$receiverKey] : '';
                            $potrojariReceiver->receiving_officer_email = isset($receiverOfficerEmail[$receiverKey])
                                ? $receiverOfficerEmail[$receiverKey] : '';

                            if($potrojari_internal == 0 && $potrojariReceiver->receiving_officer_designation_id != 0){
                            $potrojari_internal = 1;
                        }
                            $tablePotrojariReceiver->save($potrojariReceiver);
                        }
                    }

                    if (!empty($receiverGroupId)) {
                        foreach ($receiverGroupId as $groupKey => $groupValue) {
                            if (empty($groupValue)) {
                                continue;
                            }
                            $groupDetails = $tablePotrojariGroup->get($groupValue);
                            $groupvalueArray = json_decode($groupDetails['group_value'],
                                true);
                            if (!empty($groupvalueArray)) {
                                $selectedDesignation = explode('~',
                                    $receiverGroupDesignation[$groupKey]);
                                $selectedMember = $receiverGroupMember[$groupKey];
                                foreach ($groupvalueArray as $groupJsonKey => $groupJsonValue) {
                                    $design = !empty($groupJsonValue['office_unit_organogram_id'])
                                        ? $groupJsonValue['office_unit_organogram_id']
                                        : (!empty($groupJsonValue['officer_email'])
                                            ? $groupJsonValue['officer_email']
                                            : 0);
                                    if (empty($design)) {
                                        continue;
                                    } else {
                                        if (in_array($design,
                                            $selectedDesignation)) {
                                            $potrojariReceiver = $tablePotrojariReceiver->newEntity();

                                            $potrojariReceiver->potrojari_id = $potrojari->id;
                                            $potrojariReceiver->nothi_master_id = $potrojari->nothi_master_id;
                                            $potrojariReceiver->nothi_part_no = $potrojari->nothi_part_no;
                                            $potrojariReceiver->nothi_notes_id = $potrojari->nothi_notes_id;
                                            $potrojariReceiver->nothi_potro_id = $potrojari->nothi_potro_id;
                                            $potrojariReceiver->potro_type = $potrojari->potro_type;
                                            $potrojariReceiver->potro_status = $potrojari->potro_status;
                                            $potrojariReceiver->modified = $potrojari->modified;
                                            $potrojariReceiver->created = $potrojari->modified;
                                            $potrojariReceiver->created_by = $potrojari->created_by;
                                            $potrojariReceiver->modified_by = $potrojari->modified_by;

                                            $potrojariReceiver->receiving_office_name
                                                = !empty($groupJsonValue['office_name'])
                                                ? $groupJsonValue['office_name']
                                                : '';

                                            $potrojariReceiver->group_id = $groupDetails['id'];

                                            $potrojariReceiver->group_name = $groupDetails['group_name'];
                                            $potrojariReceiver->group_member = $selectedMember;

                                            $potrojariReceiver->receiving_office_id
                                                = !empty($groupJsonValue['office_id'])
                                                ? $groupJsonValue['office_id']
                                                : 0;

                                            $potrojariReceiver->receiving_office_unit_id
                                                = !empty($groupJsonValue['office_unit_id'])
                                                ? $groupJsonValue['office_unit_id']
                                                : 0;

                                            $potrojariReceiver->receiving_office_unit_name
                                                = !empty($groupJsonValue['office_unit_name'])
                                                ? $groupJsonValue['office_unit_name']
                                                : '';

                                            $potrojariReceiver->receiving_officer_id
                                                = !empty($groupJsonValue['employee_id'])
                                                ? $groupJsonValue['employee_id']
                                                : 0;

                                            $potrojariReceiver->receiving_officer_designation_id
                                                = !empty($groupJsonValue['office_unit_organogram_id'])
                                                ? $groupJsonValue['office_unit_organogram_id']
                                                : 0;

                                            $potrojariReceiver->receiving_officer_designation_label
                                                = !empty($groupJsonValue['office_unit_organogram_name'])
                                                ? $groupJsonValue['office_unit_organogram_name']
                                                : '';

                                            $potrojariReceiver->receiving_officer_name
                                                = !empty($groupJsonValue['employee_name'])
                                                ? $groupJsonValue['employee_name']
                                                : '';

                                            $potrojariReceiver->receiving_officer_email
                                                = !empty($groupJsonValue['officer_email'])
                                                ? $groupJsonValue['officer_email']
                                                :'';
                                            if($potrojari_internal == 0 && $potrojariReceiver->receiving_officer_designation_id != 0){
                                                $potrojari_internal = 1;
                                            }

                                            $tablePotrojariReceiver->save($potrojariReceiver);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    $onulipiGroupId = (!empty($this->request->data['onulipi_group_id_final'])
                        ? (explode(';',
                            $this->request->data['onulipi_group_id_final']))
                        : array());
                    $onulipiGroupDesignation = (!empty($this->request->data['onulipi_group_designation_final'])
                        ? (explode(';',
                            $this->request->data['onulipi_group_designation_final']))
                        : array());
                    $onulipiGroupMember = (!empty($this->request->data['onulipi_group_member_final'])
                        ? (explode(';',
                            $this->request->data['onulipi_group_member_final']))
                        : array());

                    $onulipiOfficeId = (!empty($this->request->data['onulipi_office_id_final'])
                        ? (explode(';',
                            $this->request->data['onulipi_office_id_final']))
                        : array());
                    $onulipiOfficeName = (!empty($this->request->data['onulipi_office_name_final'])
                        ? (explode(';',
                            $this->request->data['onulipi_office_name_final']))
                        : array());
                    $onulipiOfficerOrgId = (!empty($this->request->data['onulipi_officer_designation_id_final'])
                        ? (explode(";",
                            $this->request->data['onulipi_officer_designation_id_final']))
                        : array());
                    $onulipiOfficerUnitId = (!empty($this->request->data['onulipi_office_unit_id_final'])
                        ? (explode(';',
                            $this->request->data['onulipi_office_unit_id_final']))
                        : array());
                    $onulipiOfficeerUnitName = (!empty($this->request->data['onulipi_office_unit_name_final'])
                        ? (explode(';',
                            $this->request->data['onulipi_office_unit_name_final']))
                        : array());
                    $onulipiOfficerOrgLabel = (!empty($this->request->data['onulipi_officer_designation_label_final'])
                        ? (explode(";",
                            $this->request->data['onulipi_officer_designation_label_final']))
                        : array());
                    $onulipiOfficerId = (!empty($this->request->data['onulipi_officer_id_final'])
                        ? (explode(";",
                            $this->request->data['onulipi_officer_id_final']))
                        : array());
                    $onulipiOfficerName = (!empty($this->request->data['onulipi_officer_name_final'])
                        ? (explode(";",
                            $this->request->data['onulipi_officer_name_final']))
                        : array());
                    $onulipiOfficerEmail = (!empty($this->request->data['onulipi_officer_email_final'])
                        ? (explode(";",
                            $this->request->data['onulipi_officer_email_final']))
                        : array());

                    if (!empty($onulipiOfficerOrgLabel)) {
                        foreach ($onulipiOfficerOrgLabel as $onulipiKey => $onulipiValue) {

                            if (empty($onulipiValue)) continue;
                            $projapotiOnulipi = $tablePotrojariOnulipi->newEntity();

                            $projapotiOnulipi->potrojari_id = $potrojari->id;
                            $projapotiOnulipi->nothi_master_id = $potrojari->nothi_master_id;
                            $projapotiOnulipi->nothi_part_no = $potrojari->nothi_part_no;
                            $projapotiOnulipi->nothi_notes_id = $potrojari->nothi_notes_id;
                            $projapotiOnulipi->nothi_potro_id = $potrojari->nothi_potro_id;
                            $projapotiOnulipi->potro_type = $potrojari->potro_type;
                            $projapotiOnulipi->potro_status = $potrojari->potro_status;
                            $projapotiOnulipi->modified = $potrojari->modified;
                            $projapotiOnulipi->created = $potrojari->modified;
                            $projapotiOnulipi->created_by = $potrojari->created_by;
                            $projapotiOnulipi->modified_by = $potrojari->modified_by;


                            $officeInfo = (!empty($onulipiOfficeId[$onulipiKey])
                                ? $table_instance_office->get($onulipiOfficeId[$onulipiKey])
                                : 0);

                            $projapotiOnulipi->receiving_office_name = !empty($officeInfo)
                                ? $officeInfo['office_name_bng'] : (!empty($onulipiOfficeName[$onulipiKey])
                                    ? $onulipiOfficeName[$onulipiKey] : '');
                            $projapotiOnulipi->receiving_office_id = isset($onulipiOfficeId[$onulipiKey])
                                ? $onulipiOfficeId[$onulipiKey] : 0;
                            $projapotiOnulipi->receiving_office_unit_id = isset($onulipiOfficerUnitId[$onulipiKey])
                                ? $onulipiOfficerUnitId[$onulipiKey] : 0;
                            $projapotiOnulipi->receiving_office_unit_name = isset($onulipiOfficeerUnitName[$onulipiKey])
                                ? $onulipiOfficeerUnitName[$onulipiKey] : '';
                            $projapotiOnulipi->receiving_officer_id = isset($onulipiOfficerId[$onulipiKey])
                                ? $onulipiOfficerId[$onulipiKey] : 0;
                            $projapotiOnulipi->receiving_officer_designation_id = isset($onulipiOfficerOrgId[$onulipiKey])
                                ? $onulipiOfficerOrgId[$onulipiKey] : 0;
                            $projapotiOnulipi->receiving_officer_designation_label
                                = isset($onulipiValue) ? $onulipiValue : '';
                            $projapotiOnulipi->receiving_officer_name = isset($onulipiOfficerName[$onulipiKey])
                                ? $onulipiOfficerName[$onulipiKey] : '';
                            $projapotiOnulipi->receiving_officer_email = isset($onulipiOfficerEmail[$onulipiKey])
                                ? $onulipiOfficerEmail[$onulipiKey] : '';
                             if($potrojari_internal == 0 && $projapotiOnulipi->receiving_officer_designation_id != 0){
                            $potrojari_internal = 1;
                            }

                            $tablePotrojariOnulipi->save($projapotiOnulipi);
                        }
                    }

                    $selectedDesignation = [];
                    if (!empty($onulipiGroupId)) {
                        foreach ($onulipiGroupId as $groupKey => $groupValue) {
                            if (empty($groupValue)) continue;
                            $groupDetails = $tablePotrojariGroup->get($groupValue);
                            $groupvalueArray = json_decode($groupDetails['group_value'],
                                true);
                            if (!empty($groupvalueArray)) {
                                $selectedDesignation = explode('~',
                                    $onulipiGroupDesignation[$groupKey]);
                                $selectedMember = $onulipiGroupMember[$groupKey];
                                foreach ($groupvalueArray as $groupJsonKey => $groupJsonValue) {
                                    $design = !empty($groupJsonValue['office_unit_organogram_id'])
                                        ? $groupJsonValue['office_unit_organogram_id']
                                        : (!empty($groupJsonValue['officer_email'])
                                            ? $groupJsonValue['officer_email']
                                            : 0);
                                    if (empty($design)) {
                                        continue;
                                    } else {
                                        if (in_array($design,
                                            $selectedDesignation)) {
                                            $potrojariReceiver = $tablePotrojariOnulipi->newEntity();

                                            $potrojariReceiver->potrojari_id = $potrojari->id;
                                            $potrojariReceiver->nothi_master_id = $potrojari->nothi_master_id;
                                            $potrojariReceiver->nothi_part_no = $potrojari->nothi_part_no;
                                            $potrojariReceiver->nothi_notes_id = $potrojari->nothi_notes_id;
                                            $potrojariReceiver->nothi_potro_id = $potrojari->nothi_potro_id;
                                            $potrojariReceiver->potro_type = $potrojari->potro_type;
                                            $potrojariReceiver->potro_status = $potrojari->potro_status;
                                            $potrojariReceiver->modified = $potrojari->modified;
                                            $potrojariReceiver->created = $potrojari->modified;
                                            $potrojariReceiver->created_by = $potrojari->created_by;
                                            $potrojariReceiver->modified_by = $potrojari->modified_by;

                                            $potrojariReceiver->receiving_office_name
                                                = !empty($groupJsonValue['office_name'])
                                                ? $groupJsonValue['office_name']
                                                : '';

                                            $potrojariReceiver->group_id = $groupDetails['id'];

                                            $potrojariReceiver->group_name = $groupDetails['group_name'];
                                            $potrojariReceiver->group_member = $selectedMember;

                                            $potrojariReceiver->receiving_office_id
                                                = !empty($groupJsonValue['office_id'])
                                                ? $groupJsonValue['office_id']
                                                : 0;

                                            $potrojariReceiver->receiving_office_unit_id
                                                = !empty($groupJsonValue['office_unit_id'])
                                                ? $groupJsonValue['office_unit_id']
                                                : 0;

                                            $potrojariReceiver->receiving_office_unit_name
                                                = !empty($groupJsonValue['office_unit_name'])
                                                ? $groupJsonValue['office_unit_name']
                                                : '';

                                            $potrojariReceiver->receiving_officer_id
                                                = !empty($groupJsonValue['employee_id'])
                                                ? $groupJsonValue['employee_id']
                                                : 0;

                                            $potrojariReceiver->receiving_officer_designation_id
                                                = !empty($groupJsonValue['office_unit_organogram_id'])
                                                ? $groupJsonValue['office_unit_organogram_id']
                                                : 0;

                                            $potrojariReceiver->receiving_officer_designation_label
                                                = !empty($groupJsonValue['office_unit_organogram_name'])
                                                ? $groupJsonValue['office_unit_organogram_name']
                                                : '';

                                            $potrojariReceiver->receiving_officer_name
                                                = !empty($groupJsonValue['employee_name'])
                                                ? $groupJsonValue['employee_name']
                                                : '';

                                            $potrojariReceiver->receiving_officer_email
                                                = !empty($groupJsonValue['officer_email']) && $groupJsonValue['officer_email']!='undefined'
                                                ? $groupJsonValue['officer_email']
                                                : '';
                                             if($potrojari_internal == 0 && $potrojariReceiver->receiving_officer_designation_id != 0){
                                            $potrojari_internal = 1;
                                            }

                                            $tablePotrojariOnulipi->save($potrojariReceiver);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                $this->request->data['attachment'] = explode(',',
                    $this->request->data['uploaded_attachments']);

                if (!empty($this->request->data['prapto_potro'])) {
                    foreach ($this->request->data['prapto_potro'] as $praptopotrokey => $praptopotro) {
                        if (!empty($praptopotro)) {
                            $file = $tableNothiPotroAttachment->find()->where(['id' => $praptopotro])->first()->toArray();
                            $attachment_data = array();
                            $attachment_data = $file;
                            $attachment_data['potrojari_type'] = "Portojari";
                            $attachment_data['potrojari_id'] = $potrojari->id;

                            $attachment_data['is_inline'] = 1;
                            $attachment_data['potro_id'] = $praptopotro;

                            $potrojari_attachment = $tablePotroJariAttachment->newEntity();
                            $potrojari_attachment = $tablePotroJariAttachment->patchEntity($potrojari_attachment,
                                $attachment_data);
                            $potrojari_attachment->created_by = $potrojari->created_by;
                            $potrojari_attachment->modified_by = $potrojari->modified_by;
                            $tablePotroJariAttachment->save($potrojari_attachment);
                        }
                    }
                }

                if (!empty($this->request->data['attachment'])) {
                    foreach ($this->request->data['attachment'] as $file) {
                        if (!empty($file)) {
                            $file_details =  explode(';',$file,2);
                            $attachment_data = array();
                            $attachment_data['potrojari_type'] = "Portojari";
                            $attachment_data['potrojari_id'] = $potrojari->id;
                            $attachment_data['attachment_description'] = $this->request->data['file_description'];
                            $attachment_data['attachment_name'] = !empty($file_details[1]) ? $file_details[1] : null;

                            $attachment_data['attachment_type'] = $this->getMimeType($file_details[0]);
                            if ($potrojari->is_summary_nothi) {
                                $attachment_data['is_summary_nothi'] = 1;
                            }

                            $filepath = explode( 'content/',
                                $file_details[0]);
                            $attachment_data['file_dir'] = $this->request->webroot . 'content/';
                            $attachment_data['file_name'] = $filepath[1];
                            $explode = explode('?token=',$attachment_data['file_name']);
                            $attachment_data['file_name'] = $explode[0];

                            $potrojari_attachment = $tablePotroJariAttachment->newEntity();
                            $potrojari_attachment = $tablePotroJariAttachment->patchEntity($potrojari_attachment,
                                $attachment_data);
                            $potrojari_attachment->created_by = $potrojari->created_by;
                            $potrojari_attachment->modified_by = $potrojari->modified_by;
                            $tablePotroJariAttachment->save($potrojari_attachment);
                        }
                    }
                }

                if (!empty($this->request->data['inline_attachment'])) {
                    $attachments = explode(',',
                        $this->request->data['inline_attachment']);
                    foreach ($attachments as $ke => $val) {
                        $file = $tableNothiPotroAttachment->get($val);
                        if (!empty($file)) {
                            $attachment_data = array();
                            $attachment_data['potrojari_type'] = "Portojari";
                            $attachment_data['potrojari_id'] = $potrojari->id;
                            $attachment_data['attachment_description'] = $file['file_description'];

                            $attachment_data['attachment_type'] = $file['attachment_type'];
                            $attachment_data['is_summary_nothi'] = 1;
                            $attachment_data['is_inline'] = 1;
                            $attachment_data['meta_data'] = $file['meta_data'];

                            $attachment_data['file_dir'] = $this->request->webroot;
                            $attachment_data['file_name'] = $file['file_name'];
                            $attachment_data['content_body'] = $file['content_body'];
                            $attachment_data['content_cover'] = $file['content_body'];

                            $potrojari_attachment = $tablePotroJariAttachment->newEntity();
                            $potrojari_attachment = $tablePotroJariAttachment->patchEntity($potrojari_attachment,
                                $attachment_data);
                            $potrojari_attachment->created_by = $potrojari->created_by;
                            $tablePotroJariAttachment->save($potrojari_attachment);
                        }
                    }
                }


                if (!empty($this->request->data['contentbody'])) {
                    $attachment_data = array();
                    $attachment_data['potrojari_type'] = "Portojari";
                    $attachment_data['potrojari_id'] = $potrojari->id;
                    $attachment_data['attachment_description'] = '';
                    $attachment_data['attachment_type'] = 'text';
                    $attachment_data['file_dir'] = '';
                    $attachment_data['file_name'] = '';
                    $attachment_data['meta_data'] = $potrojari->meta_data;
                    if ($potrojari->is_summary_nothi == 0) {
                        $attachment_data['content_body'] = (!empty($potrojari->attached_potro)?($potrojari->attached_potro . "<br/>"):'') . $potrojari->potro_description;
                    } else {
                        $attachment_data['is_summary_nothi'] = 1;
                        $attachment_data['content_body'] = $this->request->data['contentbody2'];
                        $attachment_data['content_cover'] = $this->request->data['contentbody'];
                    }

                    $potrojari_attachment = $tablePotroJariAttachment->newEntity();
                    $potrojari_attachment = $tablePotroJariAttachment->patchEntity($potrojari_attachment,
                        $attachment_data);
                    $potrojari_attachment->created_by = $potrojari->created_by;
                    $potrojari_attachment->modified_by = $potrojari->modified_by;
                    $tablePotroJariAttachment->save($potrojari_attachment);
                }

                $conn->commit();

                if (!$this->NotificationSet('draftedit', array("1"), 2,
                    $current_office_selection, '', $potrojari->potro_subject)
                ) {

                }
                 if(!empty($potrojari_internal) && $potrojari_internal == 1){
                    $potrojari->potrojari_internal = 1;
                    $tablePotroJari->updateAll(['potrojari_internal' => 1], ['id' => $potrojari->id]);
                }else if(isset($potrojari_internal) && $potrojari_internal == 0){
                     $potrojari->potrojari_internal = 0;
                     $tablePotroJari->updateAll(['potrojari_internal' => 0], ['id' => $potrojari->id]);
                 }
                return true;
            } catch (\Exception $ex) {

                $conn->rollback();
                //echo json_encode(array("status" => 'error', 'msg' => __('খসড়া সংশোধন করা সম্ভব হচ্ছেনা')));
                return $ex->getMessage();
            }
        }
        return false;
    }

    private function sendDraftPotro($potrojari, $nothiMasterId,
                                    $isSummary = false, $current_office_selection = [])
    {

        $username = $this->Auth->user('username');
        $options['token'] = sGenerateToken(['file'=>$username],['exp'=>time() + 60*300]);

        $data = $this->getSignature($username, 1, 0, true,null,$options);

        if ($data == false) {
            $this->response->body(json_encode(array('status' => 'error',
                'msg' => 'দুঃখিত! আপনার স্বাক্ষর পাওয়া যায়নি। অনুগ্রহ করে সিস্টেমে স্বাক্ষর আপলোড করুন। ধন্যবাদ।')));
            $this->response->type('application/json');
            return $this->response;
        }
        $conn = ConnectionManager::get('default');
        $conn->begin();
        $nothi_part_no = 0;
           /*
             * Check If there is notes or not
             */
            $hasOnucched = TableRegistry::get('NothiNotes')->checkEmptyNote(0,$nothiMasterId);
            if($hasOnucched  == 0){
                echo json_encode(array("status" => 'error', 'msg' => __('কোন অনুচ্ছেদ না দিয়ে পত্রজারি করা সম্ভব নয়। দয়া করে নোটে অনুচ্ছেদ দিন।')));
                die;
            }
            /*
             * Count total moves for potroajri nisponno
             */
            $totalMovement = TableRegistry::get('NothiMasterMovements')->countTotalMove($nothiMasterId);

            //send
        if ($this->sendReceiver($potrojari, $nothiMasterId, $isSummary)) {
            //sendDak
        } else {
            $conn->rollback();
            echo json_encode(array("status" => 'error', 'msg' => __('পত্রজারি করা সম্ভব হচ্ছেনা')));
            die;
        }
        if ($this->sendOnulipi($potrojari, $nothiMasterId, $isSummary)) {

        } else {
            $conn->rollback();
            echo json_encode(array("status" => 'error', 'msg' => __('পত্রজারি  করা সম্ভব হচ্ছেনা')));
            die;
        }

        try {

            TableRegistry::remove('Potrojari');
            TableRegistry::remove('NothiParts');
            TableRegistry::remove('NothiNotes');
            TableRegistry::remove('NothiPotros');
            $table_instance_potrojari = TableRegistry::get('Potrojari');
            if(empty($current_office_selection)) {
                $current_office_selection = $this->getCurrentDakSection();
            }

            $potrojari->potro_status = DAK_CATEGORY_SENT;
            $potrojari->potrojari_date = date("Y-m-d H:i:s");
            $table_instance_potrojari->save($potrojari);

            $nothiPartsTable = TableRegistry::get('NothiParts');
            $nothiPartInformation = $nothiPartsTable->get($nothiMasterId);

            $nothiPartInformation->is_active = 1;

            $nothiPartsTable->save($nothiPartInformation);

            $dakid = 0;
            $daktype = 0;
            $notesTable = TableRegistry::get('NothiNotes');
            $notesTable->updateAll(['note_status' => 'INBOX'], ['nothi_part_no' => $nothiMasterId, 'office_organogram_id' => $current_office_selection['office_unit_organogram_id']]);
            if (!empty($potrojari->nothi_potro_id)) {
                $potroAttachmentTable = TableRegistry::get('NothiPotroAttachments');

                $potrinfo = $potroAttachmentTable->get($potrojari->nothi_potro_id);

                $potrinfo->potrojari = 1;
                $potrinfo->potrojari_status = DAK_CATEGORY_SENT;
                $potroAttachmentTable->save($potrinfo);

                $tableNothiPotro = TableRegistry::get('NothiPotros');

                $potroInformation = $tableNothiPotro->get($potrinfo->nothi_potro_id);

                if (!empty($potroInformation)) {
                    $dakid = $potroInformation['dak_id'];
                    $daktype = $potroInformation['dak_type'];
                    $nothi_part_no = $potroInformation['nothi_part_no'];
                }
                /*
                 * Create an entry in Nisponno Records (Potrojari From Dak)
                 */
                if($totalMovement > 1){
                      TableRegistry::get('NisponnoRecords')->saveData( $potrojari['nothi_master_id'],$potrojari['nothi_part_no'],'potrojari',
                    $potrojari['nothi_notes_id'],$potrojari['id'],$potrojari['office_id'],
                    (!empty($potrojari['approval_office_id'])?$potrojari['approval_office_id']:$potrojari['office_id']),
                    (!empty($potrojari['approval_office_unit_id'])?$potrojari['approval_office_unit_id']:$potrojari['office_unit_id']),
                    (!empty($potrojari['approval_officer_designation_id'])?$potrojari['approval_officer_designation_id']:$potrojari['officer_designation_id']),
                    $potrojari['officer_id']);
                }
            } elseif (!empty($potrojari->nothi_notes_id)) {
                $noteInfo = $notesTable->get($potrojari->nothi_notes_id);

                $noteInfo->potrojari = 1;
                $noteInfo->potrojari_status = DAK_CATEGORY_SENT;
                $notesTable->save($noteInfo);

                /*
                 * Create an entry in Nisponno Records (Potrojari from Notes)
                 */
                if ($potrojari->nothi_notes_id == -1) {
                    $potrojari->nothi_notes_id = 0;
                }
                  if($totalMovement > 1){
                     TableRegistry::get('NisponnoRecords')->saveData( $potrojari['nothi_master_id'],$potrojari['nothi_part_no'],'potrojari',$potrojari['nothi_notes_id'],$potrojari['id'],$potrojari['office_id'],$potrojari['approval_office_id'],$potrojari['approval_office_unit_id'],$potrojari['approval_officer_designation_id'],$potrojari['officer_id']);
                }
            }else{
                  if($totalMovement > 1){
                      TableRegistry::get('NisponnoRecords')->saveData( $potrojari['nothi_master_id'],$potrojari['nothi_part_no'],'potrojari',$potrojari['nothi_notes_id'],$potrojari['id'],$potrojari['office_id'],$potrojari['approval_office_id'],$potrojari['approval_office_unit_id'],$potrojari['approval_officer_designation_id'],$potrojari['officer_id']);
                 }
            }

            if ($this->nothiVuktoKoron($potrojari,$dakid,$daktype)) {

               // $potrojariVersionTable = TableRegistry::get('PotrojariVersions');

               // $potrojariVersionTable->deleteAll(['potrojari_id' => $potrojari->id]);

                $lastNote = $notesTable->find()->select(['id'])->where(['nothi_part_no' => $nothiMasterId,
                    'note_no <>' => -1])->order(['id DESC'])->first();

                if (!empty($lastNote)) {
                    $nothiNoteSignaturesTable = TableRegistry::get('NothiNoteSignatures');
                    $nothiNoteSignaturesTable->updateAll(['is_signature' => 1],
                        ['nothi_part_no' => $nothiMasterId, 'nothi_note_id' => $lastNote['id']]);
                }


                $conn->commit();



                if ($potrojari->is_summary_nothi) {
                    if (!$this->NotificationSet('potrojari',
                        array("1", "Khosra Sar Sonkhep"), 2,
                        $current_office_selection, [],
                        $potrojari->potro_subject)
                    ) {

                    }
                } else {
                    if (!$this->NotificationSet('potrojari',
                        array("1", "Khosra Potro"), 2,
                        $current_office_selection, [],
                        $potrojari->potro_subject)
                    ) {

                    }
                }
                if (!empty($nothi_part_no)) {
                    TableRegistry::get('NothiDakPotroMaps')->updateNisponnoField($nothi_part_no);
                }
                if (!empty($nothiMasterId)) {
                    TableRegistry::get('NothiMasterCurrentUsers')->updateIsFinishField($nothiMasterId,
                        $current_office_selection['office_id'],true);
                }
                $this->setSignatureInOnucched($potrojari['nothi_part_no'],['officer_id' =>isset($current_office_selection['officer_id'])?$current_office_selection['officer_id']:(isset($current_office_selection['employee_record_id'])? $current_office_selection['employee_record_id']:$potrojari['officer_id']),'office_id' =>isset($current_office_selection['office_id'])?$current_office_selection['office_id']: $potrojari['office_id'],'office_unit_id' => isset($current_office_selection['office_unit_id'])?$current_office_selection['office_unit_id']:$potrojari['office_unit_id'],'designation_label' =>isset($current_office_selection['designation'])?$current_office_selection['designation']: $potrojari['approval_officer_designation_label'],'incharge_label' =>isset($current_office_selection['incharge_label'])?$current_office_selection['incharge_label']:( isset($potrojari['approval_officer_incharge_label'])?$potrojari['approval_officer_incharge_label']:''),'office_unit_name'=>isset($current_office_selection['office_unit_name'])?$current_office_selection['office_unit_name']:$potrojari['approval_office_unit_name'],'office_unit_organogram_id' =>isset($current_office_selection['office_unit_organogram_id'])?$current_office_selection['office_unit_organogram_id']: $potrojari['approval_officer_designation_id'],'default_sign' =>isset($current_office_selection['default_sign'])?$current_office_selection['default_sign']: 0  ,'cert_id' => isset($current_office_selection['cert_id'])?$current_office_selection['cert_id']: 0,'cert_type' => isset($current_office_selection['cert_type'])?$current_office_selection['cert_type']: '','cert_provider' => isset($current_office_selection['cert_provider'])?$current_office_selection['cert_provider']: '','cert_serial' => isset($current_office_selection['cert_serial'])?$current_office_selection['cert_serial']: ''],$potrojari['office_id']);

                echo json_encode(array("status" => 'success', 'msg' => __('পত্রজারি প্রক্রিয়াধীন')));
                die;
            } else {

                $conn->rollback();
                echo json_encode(array("status" => 'error', 'msg' => __('পত্রজারি  নথিতে উপস্থাপন করা সম্ভব হচ্ছেনা')));
                die;
            }
        } catch (\Exception $e) {
            $conn->rollback();

            echo json_encode(array("status" => 'error', 'msg' => __('পত্রজারি প্রেরণ  করা সম্ভব হচ্ছেনা')));
            die;
        } catch (\InvalidArgumentException $e) {
            $conn->rollback();
            echo json_encode(array("status" => 'error', 'msg' => __('পত্রজারি প্রেরণ  করা সম্ভব হচ্ছেনা')));
            die;
        }
        $conn->commit();
        return true;
    }

    private function sendDraftCronPotro($potrojari, $nothiMasterId,
                                        $isSummary = false, $employee_office = [])
    {

        TableRegistry::remove('Potrojari');
        $table_instance_potrojari = TableRegistry::get('Potrojari');
        TableRegistry::remove('PotrojariReceiver');
        $table_instance_potrojariRec = TableRegistry::get('PotrojariReceiver');
        TableRegistry::remove('PotrojariOnulipi');
        $table_instance_potrojariOnu = TableRegistry::get('PotrojariOnulipi');

        TableRegistry::remove('NothiMasterCurrentUsers');
        $nothiMasterCurrentUsersTable = TableRegistry::get("NothiMasterCurrentUsers");

        $nothiMastersCurrentUser = $nothiMasterCurrentUsersTable->find()->where(
            ['nothi_part_no' => $nothiMasterId,
            'office_unit_organogram_id' => $employee_office['office_unit_organogram_id'],
            'nothi_office' => $potrojari['office_id']])->first();

        if(empty($nothiMastersCurrentUser)){
            echo json_encode(array("status" => 'error', 'msg' => __('পত্রজারি প্রেরণ  করা সম্ভব হচ্ছে না')));
            die;
        }

        if (!empty($potrojari['officer_id'])) {
            $usersTable = TableRegistry::get('Users');
            $users = $usersTable->find()->where(['employee_record_id' => $potrojari['officer_id']])->first();
            $username = $users['username'];
            $options['token'] = sGenerateToken(['file'=>$username],['exp'=>time() + 60*300]);

            $data = $this->getSignature($username, 1, 0, true,null,$options);

            if ($data == false) {
                if ($isSummary) {
                    $table_instance_potrojari->deleteAll(['id' => $potrojari['id']]);
                    $table_instance_potrojariRec->deleteAll(['potrojari_id' => $potrojari['id']]);
                    $table_instance_potrojariOnu->deleteAll(['potrojari_id' => $potrojari['id']]);
                }
                echo json_encode(array('status' => 'error',
                    'msg' => 'দুঃখিত! প্রেরক/অনুমোদনকারী এর স্বাক্ষর পাওয়া যায়নি। অনুগ্রহ করে সিস্টেমে স্বাক্ষর আপলোড করে খসড়া পুনরায় সংরক্ষণ করুন। ধন্যবাদ।'));
                die;
            }
        }

        if (!empty($potrojari['approval_officer_id'])) {
            $users = $usersTable->find()->where(['employee_record_id' => $potrojari['approval_officer_id']])->first();
            $username = $users['username'];
            $options['token'] = sGenerateToken(['file'=>$username],['exp'=>time() + 60*300]);

            $data = $this->getSignature($username, 1, 0, true,null,$options);

            if ($data == false) {
                if ($isSummary) {
                    $table_instance_potrojari->deleteAll(['id' => $potrojari['id']]);
                    $table_instance_potrojariRec->deleteAll(['potrojari_id' => $potrojari['id']]);
                    $table_instance_potrojariOnu->deleteAll(['potrojari_id' => $potrojari['id']]);
                }
                echo json_encode(array('status' => 'error',
                    'msg' => 'দুঃখিত! প্রেরক/অনুমোদনকারী এর স্বাক্ষর পাওয়া যায়নি। অনুগ্রহ করে সিস্টেমে স্বাক্ষর আপলোড করে খসড়া পুনরায় সংরক্ষণ করুন। ধন্যবাদ।'));
                die;
            }
        }
        if ($potrojari['sovapoti_officer_id'] > 0) {
            $users = $usersTable->find()->where(['employee_record_id' => $potrojari['sovapoti_officer_id']])->first();
            $username = $users['username'];
            $options['token'] = sGenerateToken(['file'=>$username],['exp'=>time() + 60*300]);

            $data = $this->getSignature($username, 1, 0, true,null,$options);

            if ($data == false) {
                echo json_encode(array('status' => 'error',
                    'msg' => 'দুঃখিত! সভাপতি এর স্বাক্ষর পাওয়া যায়নি। অনুগ্রহ করে সিস্টেমে স্বাক্ষর আপলোড করে খসড়া পুনরায় সংরক্ষণ করুন। ধন্যবাদ।'));
                die;
            }
        }

        $conn = ConnectionManager::get('default');
        $conn->begin();
        $nothi_part_no = 0;

        try {
            TableRegistry::remove('NothiParts');
            TableRegistry::remove('NothiNotes');
            TableRegistry::remove('NothiPotroAttachments');
            TableRegistry::remove('NothiPotros');
            TableRegistry::remove('PotrojariVersions');
            TableRegistry::remove('NothiNoteSignatures');
            TableRegistry::remove('NothiDakPotroMaps');
            TableRegistry::remove('NothiMasterMovements');

            $potrojari->potro_status = DAK_CATEGORY_SENT;
            $potrojari->potrojari_date = date("Y-m-d H:i:s");

            $table_instance_potrojari->save($potrojari);

            if($table_instance_potrojariRec->saveAttensionasReceiver($potrojari)){

            }

            $nothiPartsTable = TableRegistry::get('NothiParts');
            $nothiPartInformation = $nothiPartsTable->get($nothiMasterId);

            $nothiPartInformation->is_active = 1;

            $nothiPartsTable->save($nothiPartInformation);

            $dakid = 0;
            $daktype = DAK_DAPTORIK;
            $notesTable = TableRegistry::get('NothiNotes');
            /*
             * Check If there is notes or not
             */
            $hasOnucched = $notesTable->checkEmptyNote(0,$nothiMasterId);
            if($hasOnucched  == 0){
                echo json_encode(array("status" => 'error', 'msg' => __('কোন অনুচ্ছেদ না দিয়ে পত্রজারি করা সম্ভব নয়। দয়া করে নোটে অনুচ্ছেদ দিন।')));
                die;
            }
            /*
             * Count total moves for potroajri nisponno
             */
            $totalMovement = TableRegistry::get('NothiMasterMovements')->countTotalMove($nothiMasterId);
            if (!empty($potrojari->nothi_potro_id)) {
                $potroAttachmentTable = TableRegistry::get('NothiPotroAttachments');

                $potrinfo = $potroAttachmentTable->find()->where(['id' => $potrojari->nothi_potro_id])->first();
                if (!empty($potrinfo)) {
                    $potrinfo->potrojari = 1;
                    $potrinfo->potrojari_status = DAK_CATEGORY_SENT;
                    $potroAttachmentTable->save($potrinfo);
                }


                $tableNothiPotro = TableRegistry::get('NothiPotros');

                $potroInformation = $tableNothiPotro->get($potrinfo->nothi_potro_id);

                if (!empty($potroInformation)) {
                    $dakid = $potroInformation['dak_id'];
                    $daktype = $potroInformation['dak_type'];
                }
                /*
                 * Create an entry in Nisponno Records (Potrojari From Dak)
                 */
                if($totalMovement > 0){
                      TableRegistry::get('NisponnoRecords')->saveData( $potrojari['nothi_master_id'],$potrojari['nothi_part_no'],'potrojari',
                    $potrojari['nothi_notes_id'],$potrojari['id'],$potrojari['office_id'],
                    (!empty($potrojari['approval_office_id'])?$potrojari['approval_office_id']:$potrojari['office_id']),
                    (!empty($potrojari['approval_office_unit_id'])?$potrojari['approval_office_unit_id']:$potrojari['office_unit_id']),
                    (!empty($potrojari['approval_officer_designation_id'])?$potrojari['approval_officer_designation_id']:$potrojari['officer_designation_id']),
                    $potrojari['officer_id']);
                }


            } elseif (!empty($potrojari->nothi_notes_id)) {
                $noteInfo = $notesTable->find()->where(['id' => $potrojari->nothi_notes_id])->first();
                if (!empty($noteInfo)) {
                    $noteInfo->potrojari = 1;
                    $noteInfo->potrojari_status = DAK_CATEGORY_SENT;
                    $notesTable->save($noteInfo);
                }

                /*
                 * Create an entry in Nisponno Records (Potrojari from Notes)
                 */
                if ($potrojari->nothi_notes_id == -1) {
                    $potrojari->nothi_notes_id = 0;
                }
                if($totalMovement > 0){
                     TableRegistry::get('NisponnoRecords')->saveData( $potrojari['nothi_master_id'],$potrojari['nothi_part_no'],'potrojari',$potrojari['nothi_notes_id'],$potrojari['id'],$potrojari['office_id'],$potrojari['approval_office_id'],$potrojari['approval_office_unit_id'],$potrojari['approval_officer_designation_id'],$potrojari['officer_id']);
                }

            }else{
                 if($totalMovement > 0){
                      TableRegistry::get('NisponnoRecords')->saveData( $potrojari['nothi_master_id'],$potrojari['nothi_part_no'],'potrojari',$potrojari['nothi_notes_id'],$potrojari['id'],$potrojari['office_id'],$potrojari['approval_office_id'],$potrojari['approval_office_unit_id'],$potrojari['approval_officer_designation_id'],$potrojari['officer_id']);
                 }


            }

            if ($this->nothiVuktoKoron($potrojari, $dakid, $daktype,$employee_office)) {

                $lastNote = $notesTable->find()->select(['id'])->where(['nothi_part_no' => $nothiMasterId,
                    'note_no <>' => -1])->order(['id DESC'])->first();

                if (!empty($lastNote)) {
                    $nothiNoteSignaturesTable = TableRegistry::get('NothiNoteSignatures');

                    $checkissign = $nothiNoteSignaturesTable->find()->select(['id','is_signature'])
                        ->where(['nothi_part_no' => $nothiMasterId, 'nothi_note_id' => $lastNote['id']])
                        ->order(['id desc'])->first();
                    if(!empty($checkissign) && $checkissign['is_signature'] == 0) {
                        $nothiNoteSignaturesTable->updateAll(['is_signature' => 1],
                            ['id'=>$checkissign['id']]);
                    }
                }

                $current_office_selection = $this->getCurrentDakSection();

                $domain = \Cake\Routing\Router::url(['controller' => "potrojari",
                    'action' => "potrojariPreview", $potrojari->office_id, $potrojari->id],
                    true);
                $commandReceiver = ROOT . "/bin/cake cronjob potrojariReceiverSent {$potrojari->office_id} {$potrojari->id} '$domain' 1> /dev/null 2>&1 &";

                exec($commandReceiver);

                $commandOnulipi = ROOT . "/bin/cake cronjob potrojariOnulipiSent {$potrojari->office_id} {$potrojari->id} '$domain' 1> /dev/null 2>&1 &";

                exec($commandOnulipi);

                $conn->commit();
                //update is_finished field for current user table. there can be multiple office permitted
                if (!empty($nothiMasterId)) {
                    TableRegistry::get('NothiDakPotroMaps')->updateNisponnoField($nothiMasterId);
                    // Need to check which Office has Permission
                    TableRegistry::remove('NothiMasterPermissions');
                    $permission_offices = TableRegistry::get('NothiMasterPermissions')->permittedOfficesId($nothiMasterId, $potrojari->office_id);
                    $multiple_office_has_permission = false;
                    if (!empty($permission_offices)) {
                        if (count($permission_offices) == 1 && current($permission_offices) == $current_office_selection['office_id']) {
                            $multiple_office_has_permission = true;
                        }
                        foreach ($permission_offices as $per_k => $per_v) {
                            if ($multiple_office_has_permission == false) {
                                $is_connected =  $this->switchOfficeWithStatus($per_v, 'MainNothiOffice',-1,true);
                                if(!$is_connected || (!empty($is_connected['status']) && $is_connected['status'] == 'error')) {
                                    continue;
                                }
                                $checkDBConnection = $this->checkCurrentOfficeDBConnection();
                                if(empty($checkDBConnection) || (!empty($checkDBConnection['status']) && $checkDBConnection['status'] == 'error')){
                                    continue;
                                }
                            }
                            TableRegistry::remove('NothiMasterCurrentUsers');
                            TableRegistry::get('NothiMasterCurrentUsers')->updateIsFinishField($nothiMasterId,
                                $potrojari->office_id, true);
                        }
                        // back to own office
                        if ($multiple_office_has_permission == false) {
                            $this->switchOffice($potrojari->office_id, 'MainNothiOffice');
                        }
                    }
                }
                if ($potrojari->is_summary_nothi) {
                    if (!$this->NotificationSet('potrojari',
                        array("1", "Khosra Sar Sonkhep"), 2,
                        $current_office_selection, [],
                        $potrojari->potro_subject)
                    ) {

                    }
                } else {
                    if (!$this->NotificationSet('potrojari',
                        array("1", "Khosra Potro"), 2,
                        $current_office_selection, [],
                        $potrojari->potro_subject)
                    ) {

                    }
                }
                  $this->setSignatureInOnucched($potrojari['nothi_part_no'],['officer_id' =>isset($current_office_selection['officer_id'])?$current_office_selection['officer_id']:(isset($current_office_selection['employee_record_id'])? $current_office_selection['employee_record_id']:$potrojari['officer_id']),'office_id' =>isset($current_office_selection['office_id'])?$current_office_selection['office_id']: $potrojari['office_id'],'office_unit_id' => isset($current_office_selection['office_unit_id'])?$current_office_selection['office_unit_id']:$potrojari['office_unit_id'],'designation_label' =>isset($current_office_selection['designation'])?$current_office_selection['designation']: $potrojari['approval_officer_designation_label'],'incharge_label' =>isset($current_office_selection['incharge_label'])?$current_office_selection['incharge_label']:( isset($potrojari['approval_officer_incharge_label'])?$potrojari['approval_officer_incharge_label']:''),'office_unit_name'=>isset($current_office_selection['office_unit_name'])?$current_office_selection['office_unit_name']:$potrojari['approval_office_unit_name'],'office_unit_organogram_id' =>isset($current_office_selection['office_unit_organogram_id'])?$current_office_selection['office_unit_organogram_id']: $potrojari['approval_officer_designation_id'],'default_sign' => isset($current_office_selection['default_sign'])?$current_office_selection['default_sign']:0 ,'cert_id' => isset($current_office_selection['cert_id'])?$current_office_selection['cert_id']: 0,'cert_type' => isset($current_office_selection['cert_type'])?$current_office_selection['cert_type']: '','cert_provider' => isset($current_office_selection['cert_provider'])?$current_office_selection['cert_provider']: '','cert_serial' => isset($current_office_selection['cert_serial'])?$current_office_selection['cert_serial']: ''],$potrojari['office_id']);
                /**
                 * Update Notes status Field
                 */
                TableRegistry::get('NothiNotes')->updateAll(['note_status' => 'INBOX'],
                    ['nothi_part_no' => $potrojari['nothi_part_no'], 'office_organogram_id' => $current_office_selection['office_unit_organogram_id']]);
                echo json_encode(array("status" => 'success', 'msg' => __('পত্রজারি প্রক্রিয়াধীন'),
                    'potrojari_id' => $potrojari->id));
                die;
            } else {
                $conn->rollback();
                echo json_encode(array("status" => 'error', 'msg' => __('পত্রজারি নথিতে উপস্থাপন করা সম্ভব হচ্ছেনা')));
                die;
            }
        } catch (\Exception $e) {
            $conn->rollback();

            echo json_encode(array("status" => 'error', 'msg' => __('পত্রজারি প্রেরণ  করা সম্ভব হচ্ছেনা'),'msgdetails'=> $this->makeEncryptedData($e->getMessage())));
            die;
        } catch (\InvalidArgumentException $ex) {
            $conn->rollback();
            echo json_encode(array("status" => 'error', 'msg' => __('পত্রজারি প্রেরণ  করা সম্ভব হচ্ছেনা'),'msgdetails'=> $this->makeEncryptedData($ex->getMessage())));
            die;
        }
        $conn->commit();
        return true;
    }
    private function updateCsDuringPotrojari($cs_id = '',$token = ''){
        $response = [
            'status' => 'error',
            'message' => 'Something went wrong (Error: CS)'
        ];
        try{
            if(!empty($cs_id) && !empty($token)){
                $token = $this->getDecryptedData($token);
                if(!empty($token) && $token == Date('Y-m-d')){
                    $tablePJ = TableRegistry::get('Potrojari');
                    $tablePjAttachments = TableRegistry::get('PotrojariAttachments');
                    $cs_info = $tablePJ->get($cs_id);
                    if(!empty($cs_info['potro_cover'])){
                        // time to make PDF from potro description
                        //check folder path and get path info
                        $folder_path = $this->makeDirectory('Nothi',!empty($cs_info['approval_office_id'])?$cs_info['approval_office_id']:$cs_info['office_id']);
                        $folder_path = str_replace(FILE_FOLDER_DIR,'',$folder_path);
                        $file_name = 'cs-'.time().'-'.$cs_id;
                        $this->request->data['orientation'] = 'landscape';
                        $file_response = $this->createPdf($file_name,$cs_info['potro_description'],false, $folder_path,false);
                        $file_response = jsonA($file_response);
                       if(!empty($file_response['status']) && $file_response['status'] == 'success'){
                           //get All attachments of this potrojari
                           $allPjAttachments = $tablePjAttachments->getData([],['potrojari_id' => $cs_id])->toArray();
                           // need to save CS body in pj attachments table
                          $is_saved = $tablePjAttachments->setData([
                               'potrojari_type' => 'Portojari',
                               'potrojari_id' => $cs_id,
                               'attachment_description' => '',
                               'user_file_name' => 'প্রতিবেদন-পত্র'.(!empty($cs_info['sarok_no'])?(' ('.$cs_info['sarok_no'].')'):''),
                               'attachment_type' => 'application/pdf',
                               'file_dir' => FILE_FOLDER,
                               'file_name' =>$folder_path.base64_encode($file_name).'.pdf',
                               'created_by' => $cs_info['created_by'],
                               'modified_by' => $cs_info['modified_by'],
                           ]);
                          if($is_saved){
                              // now all previous attachments will be deleted and added again . Details: #1857
                              if(!empty($allPjAttachments)){
                                  foreach ($allPjAttachments as $attachment){
                                      if($tablePjAttachments->deleteAll(['id' => $attachment['id']])){
                                          unset($attachment['id']);
                                          $attachment2save = $tablePjAttachments->newEntity($attachment->toArray());
                                          $attachment2save->isNew(true);
                                          $tablePjAttachments->save($attachment2save,['checkExisting' => false]);
                                      }
                                  }
                              }
                              // now time to replace protibedon potro with forwarding potro as protibedon is in attachment section
                              $cs_info['potro_description'] = $cs_info['potro_cover'];
                              $cs_info['potro_cover'] = '';
                              $tablePJ->save($cs_info);
                              $response = [
                                  'status' => 'success',
                                  'message' => 'updated successfully'
                              ];
                          }else{
                              $response['message'] = '';
                          }


                       }else{
                           $response['message'] = 'দুঃখিত! প্রতিবেদনটি সংযুক্তি আকারে তৈরি করা সম্ভব হয়নি। ';
                       }
                    }else{
                        $response = [
                            'status' => 'success',
                            'message' => 'updated successfully'
                        ];
                    }
                }else{
                    $response['message'] = __('Required info missing');
                }
            }else{
                $response['message'] = __('Required info not given');
            }
        }catch (\Exception $ex){
            $response['message'] = $ex->getMessage();
        }
        rtn:
        return $response;
    }

    private function sendReceiver($potrojari, $nothiMasterId = null)
    {

        set_time_limit(0);
        ini_set('memory_limit', -1);
        $potrojariSent = array();
        $selected_office_section = $this->getCurrentDakSection();

        try {

            $table_instance_pa = TableRegistry::get('PotrojariAttachments');
            $table_instance_potrojari_receiver = TableRegistry::get('PotrojariReceiver');
            if($table_instance_potrojari_receiver->saveAttensionasReceiver($potrojari)){

            }
            $receiverData = $table_instance_potrojari_receiver->find()->where(['potrojari_id' => $potrojari->id])->toArray();
            $attachments = $table_instance_pa->find()->where(['potrojari_id' => $potrojari->id,
                'attachment_type <> ' => 'text'])->toArray();

            if (!empty($receiverData)) {
                foreach ($receiverData as $receiverKey => $receiver) {

                    if (empty($receiver['receiving_office_id'])) {

                        if (!empty($receiver['receiving_officer_email'])  && $receiver['receiving_officer_email'] !='undefined') {
                            try {
                                $margin = !empty($potrojari['meta_data'])?json_decode($potrojari['meta_data'],true):[];
                                $margin['margin_top'] = !empty($margin['margin_top'])?doubleval($margin['margin_top']):1;
                                $margin['margin_right'] = !empty($margin['margin_right'])?doubleval($margin['margin_right']):0.75;
                                $margin['margin_left'] = !empty($margin['margin_left'])?doubleval($margin['margin_left']):0.75;
                                $margin['margin_bottom'] = !empty($margin['margin_bottom'])?doubleval($margin['margin_bottom']):.075;
                                $margin['orientation'] = !empty($margin['orientation'])?$margin['orientation']:'portrait';

                                $options = array(
                                    'no-outline', // option without argument
                                    'disable-external-links',
                                    'disable-internal-links',
                                    'disable-forms',
                                    'footer-center' => '[page]',
                                    'orientation' => $margin['orientation'],
                                    'margin-top'    => bnToen($margin['margin_top'])*20,
                                    'margin-right'  => bnToen($margin['margin_right'])*20,
                                    'margin-bottom' => bnToen($margin['margin_bottom'])*20,
                                    'margin-left'   => bnToen($margin['margin_left'])*20,
                                    'encoding' => 'UTF-8', // option with argument
                                    'binary' => ROOT . DS . 'vendor' . DS . 'profburial' . DS . 'wkhtmltopdf-binaries-centos6' . DS . 'bin' . DS . PDFBINARY,
                                    'user-style-sheet' => WWW_ROOT . DS . 'assets' . DS . 'global' . DS . 'plugins' . DS . 'bootstrap' . DS . 'css' . DS . 'bootstrap.min.css',
                                );

                                $pdf = new Pdf($options);

                                $pdfbody = $potrojari->attached_potro . "<br/>" . $potrojari->potro_description;

                                $pdfbody = str_replace($this->request->webroot . 'img/',
                                    WWW_ROOT . 'img/', $pdfbody);
                                $pdfbody = str_replace('lang="AR-SA"', 'lang="BN"', $pdfbody);
                                 $pdfbody = str_replace('dir="RTL"', '', $pdfbody);
                                   if(!defined('Live') || Live == 0 ){
                                    $background_image = 'background-image: url("'.WWW_ROOT. 'img/test-background.png'.'");background-repeat: no-repeat;background-position:center;';
                                }else{
                                     $background_image = '';
                                }

                                $pdf->addPage("<html><head><script src='https://code.jquery.com/jquery-1.11.3.js'></script><style>body{
                font-family: nikosh,SolaimanLipi, Garamond,'Open Sans', sans-serif !important;
                font-size: 12pt!important;
                {$background_image}
            }
            .bangladate{
                border-bottom: 1px solid #000;
            }
            #sovapoti_signature img, #sender_signature img, #sender_signature2 img, #sender_signature3 img, #sender_signature4 img, #sender_signature5 img{
                height: 50px;
            }  
             table {
      table-layout: fixed;
      width: 100%!importan;
      white-space: nowrap;
    }
    table td , table th{
      white-space: pre-wrap;
      overflow: hidden;
      text-overflow: ellipsis;
      word-break: break-word;
    }
    
    table th{
      word-break: normal;
    }
    </style></head><body><script>
            
        $(function(){
            $('#sharok_no').parent('div').css('white-space','nowrap');
            $('#sharok_no2').parent('div').css('white-space','nowrap');
        })
</script>{$pdfbody}</body>   </html>");

                                $t = time();

                                //module
                                $path = 'Nothi';
                                $office = $selected_office_section['office_id'];
                                $pdf_path = $this->makeDirectory($path, $office);
                                chmod($pdf_path, 777);
                                if ($pdf->saveAs($pdf_path . 'main_potro_' . $t . '.pdf')) {
                                    $recEmail = explode(',',
                                        $receiver['receiving_officer_email']);
                                    if (!empty($recEmail)) {
                                        foreach ($recEmail as $k => $mail) {
                                            $email = new Email('default');

                                            $email->emailFormat('html')->from(['nothi@nothi.org.bd' => $potrojari->officer_name])
                                                ->to($mail)
                                                ->subject($potrojari->potro_subject);


                                            if (!empty($attachments)) {
                                                foreach ($attachments as $k => $file) {
                                                    if (!empty($file)) {
                                                        $filname = explode('/',
                                                            $file['file_name']);
                                                        $filepath = FILE_FOLDER_DIR . $file['file_name'];

                                                        $email->addAttachments(array(
                                                            urldecode($filname[count($filname)
                                                            - 1]) => ['file' => urldecode($filepath)]));
                                                    }
                                                }
                                            }

                                            $email->addAttachments(array('main_potro.pdf' => ['file' => $pdf_path . 'main_potro_' . $t . '.pdf']));

                                            if ($email->sendWithoutException("আপনার একটি পত্র এসেছে। মূল পত্র সহ সকল সংযুক্তি ইমেইলের সংযুক্তিতে দেয়া হলো। <br/><br/>ধন্যবাদ।")) {
                                                $receiver['potro_status'] = 'Sent';
                                            }
                                        }
                                    }
                                    $receiver['is_sent'] = 1;
                                    $receiver['potro_status'] = 'Sent';
                                    $receiver['run_task'] = 1;
                                    $receiver['modified'] = date("Y-m-d H:i:s");
                                } else {
                                    $receiver['is_sent'] = 0;
                                    $receiver['run_task'] = 1;
                                    $receiver['potro_status'] = 'Failed:' . $pdf->getError();
                                    $receiver['modified'] = date("Y-m-d H:i:s");
                                }
                                $potrojariSent[] = $receiver;
                            } catch (\Exception $ex) {
                                $receiver['is_sent'] = 0;
                                $receiver['potro_status'] = 'Failed:' . $ex->getMessage();
                                $receiver['modified'] = date("Y-m-d H:i:s");
                                $potrojariSent[] = $receiver;
                            }
                        } else {
                            $receiver['is_sent'] = 1;
                            $receiver['potro_status'] = 'Sent';
                            $receiver['modified'] = date("Y-m-d H:i:s");
                            $potrojariSent[] = $receiver;
                        }
                        continue;
                    }

                    if ($receiver['receiving_office_id'] == 0) continue;

                    $this->switchOffice($receiver['receiving_office_id'],
                        'recieverOffice');

                    TableRegistry::remove('DakDaptoriks');
                    TableRegistry::remove('DakNagoriks');
                    $table_instance_dd = TableRegistry::get('DakDaptoriks');
                    $table_instance_dn = TableRegistry::get('DakNagoriks');

                    //dak daptorik

                    $dak_daptoriks = $table_instance_dd->newEntity();
                    $dak_daptoriks->sender_sarok_no = $potrojari->sarok_no;
                    $dak_daptoriks->sender_office_id = $potrojari->office_id;
                    $dak_daptoriks->sender_officer_id = $potrojari->officer_id;
                    $dak_daptoriks->sender_office_name = $potrojari->office_name;
                    $dak_daptoriks->sender_office_unit_id = $potrojari->office_unit_id;
                    $dak_daptoriks->sender_office_unit_name = $potrojari->office_unit_name;
                    $dak_daptoriks->sender_officer_designation_id = $potrojari->officer_designation_id;
                    $dak_daptoriks->sender_officer_designation_label = $potrojari->officer_designation_label;
                    $dak_daptoriks->sender_name = $potrojari->officer_name;

                    $dak_daptoriks->sending_date = $potrojari->potrojari_date;
                    $dak_daptoriks->dak_sending_media = 1;
                    $dak_daptoriks->dak_received_no = $this->generateDakReceivedNo($dak_daptoriks->sender_office_id);
                    $dak_daptoriks->receiving_date = date("Y-m-d H:i:s");
                    $dak_daptoriks->uploader_designation_id = $selected_office_section['office_unit_organogram_id'];
                    $dak_daptoriks->dak_subject = $potrojari->potro_subject;
                    $dak_daptoriks->dak_security_level = $potrojari->potro_security_level;
                    $dak_daptoriks->dak_priority_level = $potrojari->potro_priority_level;
                    $dak_daptoriks->dak_description = $potrojari->attached_potro . "<br/>" . $potrojari->potro_description;
                    $dak_daptoriks->meta_data = $potrojari->meta_data;
                    $dak_daptoriks->is_summary_nothi = $potrojari->is_summary_nothi;
                    $dak_daptoriks->dak_cover = !empty($potrojari->potro_cover)
                        ? $potrojari->potro_cover : '';


                    $dak_daptoriks->receiving_office_id = $receiver['receiving_office_id'];
                    $dak_daptoriks->receiving_officer_designation_id = $receiver['receiving_officer_designation_id'];
                    $dak_daptoriks->receiving_office_unit_id = $receiver['receiving_office_unit_id'];
                    $dak_daptoriks->receiving_office_unit_name = !empty($receiver['receiving_office_unit_name'])?$receiver['receiving_office_unit_name']:'';
                    $dak_daptoriks->receiving_officer_id = $receiver['receiving_officer_id'];
                    $dak_daptoriks->receiving_officer_designation_label = !empty($receiver['receiving_officer_designation_label'])?$receiver['receiving_officer_designation_label']:'';
                    $dak_daptoriks->receiving_officer_name = !empty($receiver['receiving_officer_name'])?$receiver['receiving_officer_name']:'';

                    $dak_countdp = $table_instance_dd->find()->where(['receiving_office_id' => $receiver['receiving_office_id']])->order(['cast(docketing_no as signed)' => 'desc'])->first();
                    $dak_countng = $table_instance_dn->find()->where(['receiving_office_id' => $receiver['receiving_office_id']])->order(['cast(docketing_no as signed)' => 'desc'])->first();
                    $dak_count = max([(!empty($dak_countdp['docketing_no']) ? $dak_countdp['docketing_no'] : 0), (!empty($dak_countng['docketing_no']) ? $dak_countng['docketing_no'] : 0)]) + 1;


                    $dak_daptoriks->docketing_no = $dak_count;
                    $dak_daptoriks->created_by = 0;
                    $dak_daptoriks->modified_by = 0;
                    $dak_daptoriks->dak_status = DAK_CATEGORY_INBOX;
                    $dak = $table_instance_dd->save($dak_daptoriks);

                    //other
                    TableRegistry::remove('DakUsers');
                    $table_instance_du = TableRegistry::get('DakUsers');

                    $drafterOther = $table_instance_du->newEntity();
                    $drafterOther->dak_type = DAK_DAPTORIK;
                    $drafterOther->dak_id = $dak->id;
                    $drafterOther->to_office_id = $dak_daptoriks->receiving_office_id;
                    $drafterOther->to_office_name = $potrojari->office_name;
                    $drafterOther->to_office_unit_id = $dak_daptoriks->receiving_office_unit_id;
                    $drafterOther->to_office_unit_name = $dak_daptoriks->receiving_office_unit_name;
                    $drafterOther->to_office_address = '';
                    $drafterOther->to_officer_id = $dak_daptoriks->receiving_officer_id;
                    $drafterOther->to_officer_name = $dak_daptoriks->receiving_officer_name;
                    $drafterOther->to_officer_designation_id = $dak_daptoriks->receiving_officer_designation_id;
                    $drafterOther->to_officer_designation_label = $dak_daptoriks->receiving_officer_designation_label;
                    $drafterOther->dak_view_status = DAK_VIEW_STATUS_NEW;
                    $drafterOther->dak_priority = 0;
                    $drafterOther->is_summary_nothi = $potrojari->is_summary_nothi;
                    $drafterOther->attention_type = 1;
                    $drafterOther->dak_category = DAK_CATEGORY_INBOX;
                    $drafterOther->created_by = 0;
                    $drafterOther->modified_by = 0;

                    $drafterOther = $table_instance_du->save($drafterOther);

                    TableRegistry::remove('DakUserActions');
                    $table_instance_dua = TableRegistry::get('DakUserActions');
                    $user_action = $table_instance_dua->newEntity();
                    $user_action->dak_id = $dak->id;
                    $user_action->dak_user_id = $drafterOther->id;
                    $user_action->dak_action = DAK_CATEGORY_INBOX;
                    $user_action->is_summary_nothi = $potrojari->is_summary_nothi;
                    $user_action->created_by = 0;
                    $user_action->modified_by = 0;
                    $table_instance_dua->save($user_action);

                    TableRegistry::remove('DakAttachments');
                    $table_instance_da = TableRegistry::get('DakAttachments');

                    if (!empty($potrojari->potro_description)) {
                        $attachment_data = array();
                        $attachment_data['dak_type'] = "Daptorik";
                        $attachment_data['dak_id'] = $dak->id;
                        $attachment_data['attachment_description'] = '';
                        $attachment_data['is_main'] = 1;

                        $attachment_data['attachment_type'] = 'text';
                        $attachment_data['content_body'] =  (!empty($potrojari->attached_potro)?($potrojari->attached_potro . "<br/>"):'') . $potrojari->potro_description;
                        $attachment_data['meta_data'] = $potrojari->meta_data;
                        if ($potrojari->is_summary_nothi) {
                            $attachment_data['content_cover'] = $potrojari->potro_cover;
                            $attachment_data['is_summary_nothi'] = 1;
                        }

                        $attachment_data['file_dir'] = '';
                        $attachment_data['file_name'] = '';

                        $dak_attachment = $table_instance_da->newEntity();
                        $dak_attachment = $table_instance_da->patchEntity($dak_attachment,
                            $attachment_data);
                        $dak_attachment->created_by = 0;
                        $dak_attachment->modified_by = 0;
                        $table_instance_da->save($dak_attachment);
                    }


                    if (!empty($attachments)) {
                        foreach ($attachments as $k => $file) {
                            if (!empty($file)) {
                                $attachment_data = array();
                                $attachment_data['dak_type'] = "Daptorik";
                                $attachment_data['dak_id'] = $dak->id;
                                $attachment_data['attachment_description'] = '';

                                $attachment_data['attachment_type'] = $file['attachment_type'];
                                $attachment_data['file_dir'] = $file['file_dir'];
                                $attachment_data['file_name'] = $file['file_name'];
                                if ($potrojari->is_summary_nothi) {
                                    $attachment_data['is_summary_nothi'] = 1;
                                }

                                $dak_attachment = $table_instance_da->newEntity();
                                $dak_attachment = $table_instance_da->patchEntity($dak_attachment,
                                    $attachment_data);
                                $dak_attachment->created_by = 0;
                                $dak_attachment->modified_by = 0;
                                $table_instance_da->save($dak_attachment);
                            }
                        }
                    }

                    /* Update Dak Movement Table */
                    TableRegistry::remove('DakMovements');
                    $DakMovementsTable = TableRegistry::get('DakMovements');

                    /* Sender to Draft User Movement */
                    $second_move = $DakMovementsTable->newEntity();
                    $second_move->dak_type = DAK_DAPTORIK;
                    $second_move->dak_id = $dak->id;
                    $second_move->from_office_id = $dak['sender_office_id'];
                    $second_move->from_office_name = $dak['sender_office_name'];
                    $second_move->from_office_unit_id = $dak['sender_office_unit_id'];
                    $second_move->from_office_unit_name = $dak['sender_office_unit_name'];
                    $second_move->from_office_address = '';
                    $second_move->from_officer_id = $dak['sender_officer_id'];
                    $second_move->from_officer_name = $dak['sender_name'];
                    $second_move->from_officer_designation_id = $dak['sender_officer_designation_id'];
                    $second_move->from_officer_designation_label = $dak['sender_officer_designation_label'];


                    $second_move->to_office_id = $drafterOther['to_office_id'];
                    $second_move->to_office_name = $drafterOther['to_office_name'];
                    $second_move->to_office_unit_id = $drafterOther['to_office_unit_id'];
                    $second_move->to_office_unit_name = $drafterOther['to_office_unit_name'];
                    $second_move->to_office_address = $drafterOther['to_office_address'];
                    $second_move->to_officer_id = $drafterOther['to_officer_id'];
                    $second_move->to_officer_name = $drafterOther['to_officer_name'];
                    $second_move->to_officer_designation_id = $drafterOther['to_officer_designation_id'];
                    $second_move->to_officer_designation_label = $drafterOther['to_officer_designation_label'];
                    $second_move->attention_type = 1;
                    $second_move->docketing_no = $dak['docketing_no'];
                    $second_move->from_sarok_no = "";
                    $second_move->to_sarok_no = "";
                    $second_move->operation_type = DAK_CATEGORY_FORWARD;
                    $second_move->sequence = 2;
                    $second_move->dak_actions = '';
                    $second_move->is_summary_nothi = $potrojari->is_summary_nothi;
                    $second_move->dak_priority = $dak['dak_priority_level'];
                    $second_move->created_by = 0;
                    $second_move->modified_by = 0;

                    $DakMovementsTable->save($second_move);

                    $receiver['dak_id'] = $dak->id;
                    $receiver['potro_status'] = 'Sent';
                    $receiver['modified'] = date("Y-m-d H:i:s");
                    $receiver['is_sent'] = 1;
                    $potrojariSent[] = $receiver;

                    $toUser['office_id'] = $receiver['receiving_office_id'];
                    $toUser['officer_id'] = $receiver['receiving_officer_id'];

                    if ($potrojari->is_summary_nothi && $receiver['is_sent']) {

                        $tableSummaryNothiUser = TableRegistry::get('SummaryNothiUsers');
                        $tableSummaryNothiUser->updateAll([
                            'dak_id' => $dak->id,
                        ],
                            [
                                'potrojari_id' => $potrojari->id,
                                'current_office_id' => $receiver['receiving_office_id'],
                                'nothi_office' => $potrojari->office_id,
                            ]);
                        $tableSummaryNothiUser->updateAll([
                            'is_sent' => 1
                        ],
                            [
                                'potrojari_id' => $potrojari->id,
                                'current_office_id' => $potrojari->office_id,
                                'nothi_office' => $potrojari->office_id,
                            ]);
                    }


                    if ($potrojari->is_summary_nothi) {
                        if (!$this->NotificationSet('inbox',
                            array("1", "Sar Sonkhep sent as Potrojari"), 1,
                            $selected_office_section, $toUser,
                            $potrojari->potro_subject)
                        ) {

                        }
                    } else {
                        $priority = json_decode(DAK_PRIORITY_TYPE, true);
                        $security = json_decode(DAK_SECRECY_TYPE, true);
                        $status = (isset($priority[$dak['dak_priority_level']])
                            && $dak['dak_priority_level'] > 0 ? ('"' . $priority[$dak['dak_priority_level']] . '" ')
                                : '') . (isset($security[$dak['dak_security_level']])
                            && $dak['dak_security_level'] > 0 ? ('"' . $security[$dak['dak_security_level']] . '" ')
                                : '');

                        if (!$this->NotificationSet('inbox',
                            array("1", ("Daptorik Dak sent as Potrojari")),
                            1, $selected_office_section, $toUser,
                            $potrojari->potro_subject)
                        ) {

                        }
                    }
                }
            }
        } catch (\Exception $e) {

            $receiver['is_sent'] = 0;
            $receiver['potro_status'] = 'Failed:' . $e->getMessage();
            $receiver['modified'] = date("Y-m-d H:i:s");
            $potrojariSent[] = $receiver;
        }

        $this->switchOffice($selected_office_section['office_id'], 'OwnOffice');
        TableRegistry::remove('PotrojariReceiver');
        $table_instance_potrojari_receiver = TableRegistry::get('PotrojariReceiver');

        if (!empty($potrojariSent)) {
            foreach ($potrojariSent as $key => $value) {

                $value['run_task'] = 1;
                $table_instance_potrojari_receiver->save($value);
            }
        }

        return true;
    }

    private function sendOnulipi($potrojari, $nothiMasterId)
    {
        set_time_limit(0);
        ini_set('memory_limit', -1);

        $potrojariSent = array();
        $selected_office_section = $this->getCurrentDakSection();

        try {

            $table_instance_pa = TableRegistry::get('PotrojariAttachments');
            $table_instance_potrojari_receiver = TableRegistry::get('PotrojariOnulipi');

            $receiverData = $table_instance_potrojari_receiver->find()->where(['potrojari_id' => $potrojari->id])->toArray();
            $attachments = $table_instance_pa->find()->where(['potrojari_id' => $potrojari->id,
                'attachment_type <> ' => 'text'])->toArray();

            if (!empty($receiverData)) {
                foreach ($receiverData as $receiverKey => $receiver) {

                    if (empty($receiver['receiving_office_id'])) {

                        if (!empty($receiver['receiving_officer_email'])  && $receiver['receiving_officer_email'] !='undefined') {
                            try {
                                $margin = !empty($potrojari['meta_data'])?json_decode($potrojari['meta_data'],true):[];
                                $margin['margin_top'] = !empty($margin['margin_top'])?doubleval($margin['margin_top']):1;
                                $margin['margin_right'] = !empty($margin['margin_right'])?doubleval($margin['margin_right']):0.75;
                                $margin['margin_bottom'] = !empty($margin['margin_bottom'])?doubleval($margin['margin_bottom']):.075;
                                $margin['margin_left'] = !empty($margin['margin_left'])?doubleval($margin['margin_left']):.075;
                                $margin['orientation'] = !empty($margin['orientation'])?$margin['orientation']:'portrait';

                                $options = array(
                                    'no-outline', // option without argument
                                    'disable-external-links',
                                    'disable-internal-links',
                                    'disable-forms',
                                    'footer-center' => '[page]',
                                    'orientation' => $margin['orientation'],
                                    'margin-top'    => bnToen($margin['margin_top'])*20,
                                    'margin-right'  => bnToen($margin['margin_right'])*20,
                                    'margin-bottom' => bnToen($margin['margin_bottom'])*20,
                                    'margin-left'   => bnToen($margin['margin_left'])*20,
                                    'encoding' => 'UTF-8', // option with argument
                                    'binary' => ROOT . DS . 'vendor' . DS . 'profburial' . DS . 'wkhtmltopdf-binaries-centos6' . DS . 'bin' . DS . PDFBINARY,
                                    'user-style-sheet' => WWW_ROOT . DS . 'assets' . DS . 'global' . DS . 'plugins' . DS . 'bootstrap' . DS . 'css' . DS . 'bootstrap.min.css',
                                );

                                $pdf = new Pdf($options);

                                $pdfbody = $potrojari->attached_potro . "<br/>" . $potrojari->potro_description;

                                $pdfbody = str_replace($this->request->webroot . 'img/',
                                    WWW_ROOT . 'img/', $pdfbody);
                                        $pdfbody = str_replace('lang="AR-SA"', 'lang="BN"', $pdfbody);
                                    $pdfbody = str_replace('dir="RTL"', '', $pdfbody);
                                      if(!defined('Live') || Live == 0 ){
                                        $background_image = 'background-image: url("'.WWW_ROOT. 'img/test-background.png'.'");background-repeat: no-repeat;background-position:center;';
                                    }else{
                                         $background_image = '';
                                    }


                                $pdf->addPage("<html><head><script src='https://code.jquery.com/jquery-1.11.3.js'></script><style>body{
                font-family: nikosh,SolaimanLipi, Garamond,'Open Sans', sans-serif !important;
                font-size: 12pt!important;
                {$background_image}
            }
            .bangladate{
                border-bottom: 1px solid #000;
            }
            #sovapoti_signature img, #sender_signature img, #sender_signature2 img, #sender_signature3 img, #sender_signature4 img, #sender_signature5 img{
                height: 50px;
            } 
            table {
      table-layout: fixed;
      width: 100%!importan;
      white-space: nowrap;
    }
    table td , table th{
      white-space: pre-wrap;
      overflow: hidden;
      text-overflow: ellipsis;
      word-break: break-word;
    }
    
    table th{
      word-break: normal;
    }
    </style></head><body><script>
        $(function(){
            $('#sharok_no').parent('div').css('white-space','nowrap');
            $('#sharok_no2').parent('div').css('white-space','nowrap');
        })
</script>{$pdfbody}</body>   </html>");

                                $t = time();

                                //module
                                $path = 'Nothi';
                                $office = $selected_office_section['office_id'];
                                $pdf_path = $this->makeDirectory($path, $office);
                                chmod($pdf_path, 777);
                                if ($pdf->saveAs($pdf_path . 'main_potro_' . $t . '.pdf')) {
                                    $recEmail = explode(',',
                                        $receiver['receiving_officer_email']);
                                    if (!empty($recEmail)) {
                                        foreach ($recEmail as $k => $mail) {
                                            $email = new Email('default');

                                            $email->emailFormat('html')->from(['nothi@nothi.org.bd' => $potrojari->officer_name])
                                                ->to($mail)
                                                ->subject($potrojari->potro_subject);

                                            if (!empty($attachments)) {
                                                foreach ($attachments as $k => $file) {
                                                    if (!empty($file)) {
                                                        $filname = explode('/',
                                                            $file['file_name']);
                                                        $filepath = FILE_FOLDER_DIR . $file['file_name'];

                                                        $email->addAttachments(array(
                                                            urldecode($filname[count($filname)
                                                            - 1]) => ['file' => urldecode($filepath)]));
                                                    }
                                                }
                                            }

                                            $email->addAttachments(array('main_potro.pdf' => ['file' => $pdf_path . 'main_potro_' . $t . '.pdf']));

                                            if ($email->sendWithoutException("আপনার একটি পত্র এসেছে। মূল পত্র সহ সকল সংযুক্তি ইমেইলের সংযুক্তিতে দেয়া হলো। <br/><br/>ধন্যবাদ।")) {
                                                $receiver['potro_status'] = 'Sent';
                                            }
                                        }
                                    }

                                    $receiver['modified'] = date("Y-m-d H:i:s");
                                    $receiver['is_sent'] = 1;
                                    $receiver['run_task'] = 1;
                                    $receiver['potro_status'] = 'Sent';
                                } else {
                                    $receiver['is_sent'] = 0;
                                    $receiver['run_task'] = 1;
                                    $receiver['potro_status'] = 'Failed:' . $pdf->getError();
                                    $receiver['modified'] = date("Y-m-d H:i:s");
                                }
                                $potrojariSent[] = $receiver;
                            } catch (\Exception $ex) {
                                $receiver['is_sent'] = 0;
                                $receiver['potro_status'] = 'Failed:' . $ex->getMessage();
                                $receiver['modified'] = date("Y-m-d H:i:s");
                                $potrojariSent[] = $receiver;
                            }
                        } else {
                            $receiver['modified'] = date("Y-m-d H:i:s");
                            $receiver['is_sent'] = 1;
                            $receiver['potro_status'] = 'Sent';
                            $potrojariSent[] = $receiver;
                        }
                        continue;
                    }

                    if ($receiver['receiving_office_id'] == 0) continue;
                    $this->switchOffice($receiver['receiving_office_id'],
                        'recieverOffice');

                    TableRegistry::remove('DakDaptoriks');
                    TableRegistry::remove('DakNagoriks');
                    $table_instance_dd = TableRegistry::get('DakDaptoriks');
                    $table_instance_dn = TableRegistry::get('DakNagoriks');

                    //dak daptorik
                    $dak_daptoriks = $table_instance_dd->newEntity();
                    $dak_daptoriks->sender_sarok_no = $potrojari->sarok_no;
                    $dak_daptoriks->sender_office_id = $potrojari->office_id;
                    $dak_daptoriks->sender_officer_id = $potrojari->officer_id;
                    $dak_daptoriks->sender_office_name = $potrojari->office_name;
                    $dak_daptoriks->sender_office_unit_id = $potrojari->office_unit_id;
                    $dak_daptoriks->sender_office_unit_name = $potrojari->office_unit_name;
                    $dak_daptoriks->sender_officer_designation_id = $potrojari->officer_designation_id;
                    $dak_daptoriks->sender_officer_designation_label = $potrojari->officer_designation_label;
                    $dak_daptoriks->sender_name = $potrojari->officer_name;
                    $dak_daptoriks->meta_data = $potrojari->meta_data;

                    $dak_daptoriks->sending_date = $potrojari->potrojari_date;
                    $dak_daptoriks->dak_sending_media = 1;
                    $dak_daptoriks->dak_received_no = $this->generateDakReceivedNo($dak_daptoriks->sender_office_id);
                    $dak_daptoriks->receiving_date = date("Y-m-d H:i:s");
                    $dak_daptoriks->uploader_designation_id = $selected_office_section['office_unit_organogram_id'];
                    $dak_daptoriks->dak_subject = $potrojari->potro_subject;
                    $dak_daptoriks->dak_security_level = $potrojari->potro_security_level;
                    $dak_daptoriks->dak_priority_level = $potrojari->potro_priority_level;
                    $dak_daptoriks->dak_description = $potrojari->attached_potro . "<br/>" . $potrojari->potro_description;
                    $dak_daptoriks->is_summary_nothi = $potrojari->is_summary_nothi;
                    $dak_daptoriks->dak_cover = !empty($potrojari->potro_cover)
                        ? $potrojari->potro_cover : '';


                    $dak_daptoriks->receiving_office_id = $receiver['receiving_office_id'];
                    $dak_daptoriks->receiving_officer_designation_id = $receiver['receiving_officer_designation_id'];
                    $dak_daptoriks->receiving_office_unit_id = $receiver['receiving_office_unit_id'];
                    $dak_daptoriks->receiving_office_unit_name = !empty($receiver['receiving_office_unit_name'])?$receiver['receiving_office_unit_name']:'';
                    $dak_daptoriks->receiving_officer_id = $receiver['receiving_officer_id'];
                    $dak_daptoriks->receiving_officer_designation_label = !empty($receiver['receiving_officer_designation_label'])?$receiver['receiving_officer_designation_label']:'';
                    $dak_daptoriks->receiving_officer_name = !empty($receiver['receiving_officer_name'])?$receiver['receiving_officer_name']:'';

                    $dak_countdp = $table_instance_dd->find()->where(['receiving_office_id' => $receiver['receiving_office_id']])->order(['cast(docketing_no as signed)' => 'desc'])->first();
                    $dak_countng = $table_instance_dn->find()->where(['receiving_office_id' => $receiver['receiving_office_id']])->order(['cast(docketing_no as signed)' => 'desc'])->first();
                    $dak_count = max([(!empty($dak_countdp['docketing_no']) ? $dak_countdp['docketing_no'] : 0), (!empty($dak_countng['docketing_no']) ? $dak_countng['docketing_no'] : 0)]) + 1;

                    $dak_daptoriks->docketing_no = $dak_count;
                    $dak_daptoriks->dak_status = DAK_CATEGORY_INBOX;
                    $dak = $table_instance_dd->save($dak_daptoriks);

                    //other
                    TableRegistry::remove('DakUsers');
                    $table_instance_du = TableRegistry::get('DakUsers');

                    $drafterOther = $table_instance_du->newEntity();
                    $drafterOther->dak_type = DAK_DAPTORIK;
                    $drafterOther->dak_id = $dak->id;
                    $drafterOther->to_office_id = $dak_daptoriks->receiving_office_id;
                    $drafterOther->to_office_name = $potrojari->office_name;
                    $drafterOther->to_office_unit_id = $dak_daptoriks->receiving_office_unit_id;
                    $drafterOther->to_office_unit_name = $dak_daptoriks->receiving_office_unit_name;
                    $drafterOther->to_office_address = '';
                    $drafterOther->to_officer_id = $dak_daptoriks->receiving_officer_id;
                    $drafterOther->to_officer_name = $dak_daptoriks->receiving_officer_name;
                    $drafterOther->to_officer_designation_id = $dak_daptoriks->receiving_officer_designation_id;
                    $drafterOther->to_officer_designation_label = $dak_daptoriks->receiving_officer_designation_label;
                    $drafterOther->dak_view_status = DAK_VIEW_STATUS_NEW;
                    $drafterOther->dak_priority = 0;
                    $drafterOther->is_summary_nothi = $potrojari->is_summary_nothi;
                    $drafterOther->attention_type = 1;
                    $drafterOther->dak_category = DAK_CATEGORY_INBOX;

                    $drafterOther = $table_instance_du->save($drafterOther);

                    TableRegistry::remove('DakUserActions');
                    $table_instance_dua = TableRegistry::get('DakUserActions');
                    $user_action = $table_instance_dua->newEntity();
                    $user_action->dak_id = $dak->id;
                    $user_action->dak_user_id = $drafterOther->id;
                    $user_action->dak_action = DAK_CATEGORY_INBOX;
                    $user_action->is_summary_nothi = $potrojari->is_summary_nothi;
                    $table_instance_dua->save($user_action);

                    TableRegistry::remove('DakAttachments');
                    $table_instance_da = TableRegistry::get('DakAttachments');

                    if (!empty($potrojari->potro_description)) {
                        $attachment_data = array();
                        $attachment_data['dak_type'] = "Daptorik";
                        $attachment_data['dak_id'] = $dak->id;
                        $attachment_data['attachment_description'] = '';
                        $attachment_data['is_main'] = 1;

                        $attachment_data['attachment_type'] = 'text';
                        $attachment_data['meta_data'] = $potrojari->meta_data;
                        $attachment_data['content_body'] =  (!empty($potrojari->attached_potro)?($potrojari->attached_potro . "<br/>"):'') . $potrojari->potro_description;
                        if ($potrojari->is_summary_nothi) {
                            $attachment_data['content_cover'] = $potrojari->potro_cover;
                            $attachment_data['is_summary_nothi'] = 1;
                        }

                        $attachment_data['file_dir'] = '';
                        $attachment_data['file_name'] = '';

                        $dak_attachment = $table_instance_da->newEntity();
                        $dak_attachment = $table_instance_da->patchEntity($dak_attachment,
                            $attachment_data);
                        $table_instance_da->save($dak_attachment);
                    }


                    if (!empty($attachments)) {
                        foreach ($attachments as $k => $file) {
                            if (!empty($file)) {
                                $attachment_data = array();
                                $attachment_data['dak_type'] = "Daptorik";
                                $attachment_data['dak_id'] = $dak->id;
                                $attachment_data['attachment_description'] = '';

                                $attachment_data['attachment_type'] = $file['attachment_type'];
                                $attachment_data['file_dir'] = $file['file_dir'];
                                $attachment_data['file_name'] = $file['file_name'];
                                if ($potrojari->is_summary_nothi) {
                                    $attachment_data['is_summary_nothi'] = 1;
                                }

                                $dak_attachment = $table_instance_da->newEntity();
                                $dak_attachment = $table_instance_da->patchEntity($dak_attachment,
                                    $attachment_data);
                                $table_instance_da->save($dak_attachment);
                            }
                        }
                    }

                    /* Update Dak Movement Table */
                    TableRegistry::remove('DakMovements');
                    $DakMovementsTable = TableRegistry::get('DakMovements');

                    /* Sender to Draft User Movement */
                    $second_move = $DakMovementsTable->newEntity();
                    $second_move->dak_type = DAK_DAPTORIK;
                    $second_move->dak_id = $dak->id;
                    $second_move->from_office_id = $dak['sender_office_id'];
                    $second_move->from_office_name = $dak['sender_office_name'];
                    $second_move->from_office_unit_id = $dak['sender_office_unit_id'];
                    $second_move->from_office_unit_name = $dak['sender_office_unit_name'];
                    $second_move->from_office_address = '';
                    $second_move->from_officer_id = $dak['sender_officer_id'];
                    $second_move->from_officer_name = $dak['sender_name'];
                    $second_move->from_officer_designation_id = $dak['sender_officer_designation_id'];
                    $second_move->from_officer_designation_label = $dak['sender_officer_designation_label'];


                    $second_move->to_office_id = $drafterOther['to_office_id'];
                    $second_move->to_office_name = $drafterOther['to_office_name'];
                    $second_move->to_office_unit_id = $drafterOther['to_office_unit_id'];
                    $second_move->to_office_unit_name = $drafterOther['to_office_unit_name'];
                    $second_move->to_office_address = $drafterOther['to_office_address'];
                    $second_move->to_officer_id = $drafterOther['to_officer_id'];
                    $second_move->to_officer_name = $drafterOther['to_officer_name'];
                    $second_move->to_officer_designation_id = $drafterOther['to_officer_designation_id'];
                    $second_move->to_officer_designation_label = $drafterOther['to_officer_designation_label'];
                    $second_move->attention_type = 1;
                    $second_move->docketing_no = $dak['docketing_no'];
                    $second_move->from_sarok_no = "";
                    $second_move->to_sarok_no = "";
                    $second_move->operation_type = DAK_CATEGORY_FORWARD;
                    $second_move->sequence = 2;
                    $second_move->dak_actions = '';
                    $second_move->is_summary_nothi = $potrojari->is_summary_nothi;
                    $second_move->dak_priority = $dak['dak_priority_level'];

                    $DakMovementsTable->save($second_move);

                    $receiver['dak_id'] = $dak->id;
                    $receiver['potro_status'] = 'Sent';
                    $receiver['is_sent'] = 1;
                    $receiver['modified'] = date("Y-m-d H:i:s");
                    $potrojariSent[] = $receiver;

                    $toUser['office_id'] = $receiver['receiving_office_id'];
                    $toUser['officer_id'] = $receiver['receiving_officer_id'];

                    if ($potrojari->is_summary_nothi) {
                        if (!$this->NotificationSet('inbox',
                            array("1", "Sar Sonkhep sent as Potrojari"), 1,
                            $selected_office_section, $toUser,
                            $potrojari->potro_subject)
                        ) {

                        }
                    } else {
                        $priority = json_decode(DAK_PRIORITY_TYPE, true);
                        $security = json_decode(DAK_SECRECY_TYPE, true);
                        $status = (isset($priority[$dak['dak_priority_level']])
                            && $dak['dak_priority_level'] > 0 ? ('"' . $priority[$dak['dak_priority_level']] . '" ')
                                : '') . (isset($security[$dak['dak_security_level']])
                            && $dak['dak_security_level'] > 0 ? ('"' . $security[$dak['dak_security_level']] . '" ')
                                : '');

                        if (!$this->NotificationSet('inbox',
                            array("1", ("Daptorik Dak sent as Potrojari")),
                            1, $selected_office_section, $toUser,
                            $potrojari->potro_subject)
                        ) {

                        }
                    }
                }
            }
        } catch (\Exception $e) {
            $receiver['is_sent'] = 0;
            $receiver['potro_status'] = 'Failed:' . $e->getMessage();
            $receiver['modified'] = date("Y-m-d H:i:s");
            $potrojariSent[] = $receiver;
        }

        $this->switchOffice($selected_office_section['office_id'], 'OwnOffice');
        TableRegistry::remove('PotrojariOnulipi');
        $table_instance_potrojari_receiver = TableRegistry::get('PotrojariOnulipi');

        if (!empty($potrojariSent)) {
            foreach ($potrojariSent as $key => $value) {
                $value['run_task'] = 1;
                $table_instance_potrojari_receiver->save($value);
            }
        }
        return true;
    }

    private function nothiVuktoKoron($potrojari, $dakId = null,$dakType = 'Daptorik',$selected_office_section = [])
    {

        if (empty($selected_office_section)) {
            $selected_office_section = $this->getCurrentDakSection();
        }

        if ($potrojari['potrojari_draft_office_id'] == $selected_office_section['office_id']) {
            $this->switchOffice($selected_office_section['office_id'], 'OwnOffice');
        }

        TableRegistry::remove('NothiPotros');
        TableRegistry::remove('NothiNotes');
        TableRegistry::remove('DakDaptoriks');
        TableRegistry::remove('PotrojariAttachments');
        TableRegistry::remove('NothiDakPotroMaps');
        TableRegistry::remove('NothiPotroAttachments');

        $nothiPotrosTable = TableRegistry::get("NothiPotros");
        $dakAttachmentsTable = TableRegistry::get("PotrojariAttachments");
        $nothiPotroAttachmentsTable = TableRegistry::get("NothiPotroAttachments");
        $potrojariTemplatesTable = TableRegistry::get("PotrojariTemplates");

        $con = ConnectionManager::get('default');
        try {
            $con->begin();
            $potro_type = $potrojariTemplatesTable->get($potrojari->potro_type);
            $nothi_potro_page = $nothiPotroAttachmentsTable->find()->select(['nothi_potro_page'])->where(['nothi_master_id' => $potrojari->nothi_master_id])->order(['nothi_potro_page DESC'])->first();

            $dakRecord = array();
//            if (!empty($dakId)) {
//                $dakRecord = $dakDaptoriksTable->get($dakId);
//            }

            $attachments = $dakAttachmentsTable->find()->where(['potrojari_id' => $potrojari->id, 'attachment_type <>' => 'text'])->order(['id asc'])->toArray();

            $attachmenttexts = $dakAttachmentsTable->find()->where(['potrojari_id' => $potrojari->id, 'attachment_type' => 'text'])->toArray();
            $nothiPotroRecord = $nothiPotrosTable->newEntity();

            $nothiPotroRecord->nothi_master_id = $potrojari->nothi_master_id;
            $nothiPotroRecord->nothi_part_no = $potrojari->nothi_part_no;
            $nothiPotroRecord->dak_id = 0;
            $nothiPotroRecord->potro_media = isset($dakRecord['dak_sending_media']) ? $dakRecord['dak_sending_media'] : 'ডাকযোগে';
            $nothiPotroRecord->potro_pages = count($attachments) + 1;
            $nothiPotroRecord->subject = isset($dakRecord['dak_subject']) ? $dakRecord['dak_subject'] : $potrojari->potro_subject;
            $nothiPotroRecord->sarok_no = isset($dakRecord['sender_sarok_no']) ? $dakRecord['sender_sarok_no'] : $potrojari->sarok_no;
            $nothiPotroRecord->issue_date = date("Y-m-d H:i:s");
            $nothiPotroRecord->due_date = date("Y-m-d H:i:s");
            $nothiPotroRecord->created = date("Y-m-d H:i:s");
            $nothiPotroRecord->created_by = !empty($selected_office_section['officer_id']) ? $selected_office_section['officer_id'] : $selected_office_section['employee_record_id'];
            $nothiPotroRecord->modified_by = !empty($selected_office_section['officer_id']) ? $selected_office_section['officer_id'] : $selected_office_section['employee_record_id'];

            $nothiPotroRecord = $nothiPotrosTable->save($nothiPotroRecord);

            $nothipotro = $nothiPotroAttachmentsTable->find()->where(['NothiPotroAttachments.id'=>$potrojari->nothi_potro_id])
                ->contain(['NothiPotros'])->first();

            if(!empty($nothipotro['application_meta_data'])){
                $potro = $nothipotro['nothi_potro'];
                $potro_meta = jsonA($potro['application_meta_data']);

                if (!empty($potro_meta['feedback_url'])) {
                    $feedbackdata = [
                        'api_key' => !empty($potro_meta['feedback_api_key']) ? $potro_meta['feedback_api_key'] : '',
                        'aid' => $potro_meta['tracking_id'],
                        'decision' => 4,
                        'decision_note' => "পত্রজারি করা হয়েছে",
                        'dak_id' => $potro_meta['dak_id'],
                        'nothi_id' => $potro_meta['nothi_master_id'],
                        'note_id' => $potro_meta['nothi_part_no'],
                        'potro_id' => $potro_meta['id'],
                        'current_desk_id' => $selected_office_section['office_unit_organogram_id']
                    ];

                    $this->notifyFeedback($potro_meta['feedback_url'], $feedbackdata);
                }
            }
            $comment = "পত্রজারি করা হয়েছে। স্মারক নং: ". $potrojari->sarok_no;
            $this->addDakmovement($dakId,$dakType,$comment);
//            $nothiDakPotroMapsRecord = $nothiDakPotroMapsTable->newEntity();
//            $nothiDakPotroMapsRecord->dak_id = $nothiPotroRecord->dak_id;
//            $nothiDakPotroMapsRecord->nothi_masters_id = $nothiPotroRecord->nothi_master_id;
//            $nothiDakPotroMapsRecord->nothi_part_no = $nothiPotroRecord->nothi_part_no;
//            $nothiDakPotroMapsRecord->nothi_potro_id = $nothiPotroRecord->id;
//
//            $nothiDakPotroMapsTable->save($nothiDakPotroMapsRecord);

            $potroPage = !empty($nothi_potro_page->nothi_potro_page) ? ($nothi_potro_page->nothi_potro_page)
                : 0;
            if (!empty($attachments)) {

                foreach ($attachments as $attachmentKey => $attachmentValue) {
                    $potroPage++;
                    $nextPotroPagebn = Number::format($potroPage);
                    $nothiPotroAttachmentsRecord = $nothiPotroAttachmentsTable->newEntity();

                    $nothiPotroAttachmentsRecord->potrojari_id = $potrojari->id;
                    $nothiPotroAttachmentsRecord->nothi_master_id = $potrojari->nothi_master_id;
                    $nothiPotroAttachmentsRecord->nothi_part_no = $potrojari->nothi_part_no;
                    $nothiPotroAttachmentsRecord->nothi_potro_id = $nothiPotroRecord->id;
                    $nothiPotroAttachmentsRecord->sarok_no = $nothiPotroRecord->sarok_no;
                    $nothiPotroAttachmentsRecord->created_by = !empty($selected_office_section['officer_id']) ? $selected_office_section['officer_id'] : $selected_office_section['employee_record_id'];;
                    $nothiPotroAttachmentsRecord->modified_by = !empty($selected_office_section['officer_id']) ? $selected_office_section['officer_id'] : $selected_office_section['employee_record_id'];;
                    $nothiPotroAttachmentsRecord->created = date("Y-m-d H:i:s");
                    $nothiPotroAttachmentsRecord->modified = date("Y-m-d H:i:s");
                    $nothiPotroAttachmentsRecord->nothi_potro_page = $potroPage;
                    $nothiPotroAttachmentsRecord->nothi_potro_page_bn = $nextPotroPagebn;
                    $nothiPotroAttachmentsRecord->attachment_type = $attachmentValue['attachment_type'];
                    $nothiPotroAttachmentsRecord->content_body = $attachmentValue['content_body'];
                    $nothiPotroAttachmentsRecord->potro_cover = isset($attachmentValue['content_cover'])
                        ? $attachmentValue['content_cover'] : '';
                    $nothiPotroAttachmentsRecord->is_summary_nothi = (isset($potrojari->is_summary_nothi)
                        ? $potrojari->is_summary_nothi : 0);
                    $nothiPotroAttachmentsRecord->attachment_description = $attachmentValue['attachment_description'];
                    $nothiPotroAttachmentsRecord->file_name = $attachmentValue['file_name'];
                    $nothiPotroAttachmentsRecord->user_file_name = $attachmentValue['user_file_name'];
                    $nothiPotroAttachmentsRecord->file_dir = $attachmentValue['file_dir'];

                    $nothiPotroAttachmentsTable->save($nothiPotroAttachmentsRecord);
                }
            }

            if (!empty($attachmenttexts)) {
                foreach ($attachmenttexts as $attachmentKey => $attachmentValue) {
                    $potroPage++;
                    $nextPotroPagebn = Number::format($potroPage);
                    $nothiPotroAttachmentsRecord = $nothiPotroAttachmentsTable->newEntity();

                    $nothiPotroAttachmentsRecord->potrojari_id = $potrojari->id;
                    $nothiPotroAttachmentsRecord->nothi_master_id = $potrojari->nothi_master_id;
                    $nothiPotroAttachmentsRecord->nothi_part_no = $potrojari->nothi_part_no;
                    $nothiPotroAttachmentsRecord->nothi_potro_id = $nothiPotroRecord->id;
                    $nothiPotroAttachmentsRecord->sarok_no = $nothiPotroRecord->sarok_no;
                    $nothiPotroAttachmentsRecord->created_by = !empty($selected_office_section['officer_id']) ? $selected_office_section['officer_id'] : $selected_office_section['employee_record_id'];;
                    $nothiPotroAttachmentsRecord->modified_by = !empty($selected_office_section['officer_id']) ? $selected_office_section['officer_id'] : $selected_office_section['employee_record_id'];;
                    $nothiPotroAttachmentsRecord->created = date("Y-m-d H:i:s");
                    $nothiPotroAttachmentsRecord->modified = date("Y-m-d H:i:s");
                    $nothiPotroAttachmentsRecord->nothi_potro_page = $potroPage;
                    $nothiPotroAttachmentsRecord->nothi_potro_page_bn = $nextPotroPagebn;
                    $nothiPotroAttachmentsRecord->attachment_type = $attachmentValue['attachment_type'];
                    $nothiPotroAttachmentsRecord->content_body = $attachmentValue['content_body'];
                    $nothiPotroAttachmentsRecord->meta_data = $attachmentValue['meta_data'];
                    $nothiPotroAttachmentsRecord->potro_cover = isset($attachmentValue['content_cover']) ? $attachmentValue['content_cover'] : '';
                    $nothiPotroAttachmentsRecord->is_summary_nothi = (isset($potrojari->is_summary_nothi) ? $potrojari->is_summary_nothi : 0);
                    $nothiPotroAttachmentsRecord->attachment_description = $attachmentValue['attachment_description'];
                    $nothiPotroAttachmentsRecord->file_name = $attachmentValue['file_name'];
                    $nothiPotroAttachmentsRecord->file_dir = $attachmentValue['file_dir'];

                    $nothiPotroAttachmentsTable->save($nothiPotroAttachmentsRecord);
                }
            }

            $notesTable = TableRegistry::get('NothiNotes');
            $nothinotes = $notesTable->newEntity();
            $lastNoteinfo = $notesTable->find()->where(['nothi_master_id' => $potrojari->nothi_master_id,
                'nothi_part_no' => $potrojari->nothi_part_no])->order(['note_no desc'])->first();

            $nothinotes->nothi_master_id = $potrojari->nothi_master_id;
            $nothinotes->nothi_part_no = $potrojari->nothi_part_no;
            $nothinotes->nothi_notesheet_id = !empty($lastNoteinfo) ? $lastNoteinfo['nothi_notesheet_id']
                : 1;
            $nothinotes->office_id = $potrojari->office_id;
            $nothinotes->office_unit_id = $potrojari->office_unit_id;
            $nothinotes->office_organogram_id = $potrojari->officer_designation_id;
            $nothinotes->employee_id = $potrojari->officer_id;
            $nothinotes->employee_designation = $potrojari->officer_designation_label;
            $nothinotes->note_no = -1;
            $nothinotes->subject = '';
            $time = new \Cake\I18n\Time($potrojari->potrojari_date);
            $potrojari->potrojari_date = $time->i18nFormat('YYYY-MM-dd',
                null, 'bn-BD');
            $nothinotes->note_description = base64_encode("{$potro_type->template_name} নম্বর " . $potrojari->sarok_no . ', প্রেরণের তারিখ: ' . $potrojari->potrojari_date);
            $nothinotes->note_status = "INBOX";
            $nothinotes->is_potrojari = 1;

            $notesTable->save($nothinotes);
            $con->commit();

            return true;
        } catch (\Exception $e) {
            $con->rollback();
            return false;
        }

        return true;
    }

    public function getNothiPotros($id = null, $is_ajax = null)
    {
        $this->layout = null;

        $nothi_potros_table = TableRegistry::get("NothiPotros");
        $nothi_potros = $nothi_potros_table->find("all")->where(["nothi_master_id" => $id]);

        $this->response->body(json_encode($nothi_potros));
        $this->response->type('application/json');
        return $this->response;
    }

    public function update_draft($id = null, $is_ajax = null)
    {

        $updated_data = $this->request->data;
        if ($is_ajax == 'ajax') {
            $this->layout = null;
        }
        $table_potrojari_draft = TableRegistry::get('PotrojariDraftVersions');
        $potrojari_draft = $table_potrojari_draft->get($updated_data["draft_id"]);
        if ($this->request->is(['post', 'put'])) {
            $table_potrojari_draft->patchEntity($potrojari_draft,
                $this->request->data);
            if ($table_potrojari_draft->save($potrojari_draft)) {
                return $this->redirect(['action' => 'index']);
            }
        }

        $this->set('potrojari_draft', $potrojari_draft);

        $this->response->body(json_encode(1));
        $this->response->type('application/json');
        return $this->response;
    }

    public function upload()
    {
        $this->layout = 'ajax';
        if (!empty($this->request->data['file'])) {
            $filename = $this->request->data['file']['name'];
            $destination = WWW_ROOT . '/files/' . strtolower($filename);
            if ($this->request->data['file']['error'] == 1) {
                echo "File is too large!";
                return;
            }
            if (move_uploaded_file($this->request->data['file']['tmp_name'],
                $destination)) {
                echo 1;
            } else echo "Can't upload file!";
        } else echo "Something went wrong!";
        die;
    }

    public function forwardDraft($id = 0)
    {
        $this->layout = null;
        $table_potrojari_draft = TableRegistry::get('PotrojariDraftVersions');
        $potrojari_draft = $table_potrojari_draft->get($id);

        $this->set('potrojari_draft', $potrojari_draft);

        $session = $this->request->session();
        $selected_office_section = $session->read('selected_office_section');

        $this->set('selected_office_section', $selected_office_section);
    }

    public function getPotrojariMovementSequence($potrojari_draft_id)
    {
        $table_instance_pm = TableRegistry::get('PotrojariDraftMovements');
        $pm = $table_instance_pm->find()->select(['sequence'])->where(['potrojari_draft_id' => $potrojari_draft_id])->order(['created desc'])->first();
        return (intval($pm['sequence']) + 1);
    }

    public function generateDakReceivedNo($office_id)
    {

        TableRegistry::remove('DakDaptoriks');
        $table_instance_dd = TableRegistry::get('DakDaptoriks');
        $dak_count = $table_instance_dd->find()->where(['receiving_office_id' => $office_id, 'date(created)' => date('Y-m-d')])->count();

        startagain:
        $dak_count++;
        $year = date('y');
        $month = date('m');
        $day = date('d');
        $daktype = 2;
        $offline = 1;

        $received_no = "";

        $received_no = $daktype . $offline . str_pad($office_id, 5, "0", STR_PAD_LEFT) . $year . $month . $day . str_pad($dak_count, 3, "0", STR_PAD_LEFT);

        $exists = $table_instance_dd->find()->where(['dak_received_no' => $received_no])->count();
        if($exists){
            goto startagain;
        }
        return $received_no;
    }

    public function generateSarokNo()
    {
        $office_id = $this->request->data['office_id'];
        $dak_daptoriks = TableRegistry::get('DakDaptoriks');
        $dak_daptoriks_count = $dak_daptoriks->getDakCount($office_id);

        $this->response->body(json_encode(($dak_daptoriks_count + 1)));
        $this->response->type('application/json');
        return $this->response;
    }

    public function getExistingDakUser($dak_id = 0, $dak_officer_unit_id,
                                       $dak_office_organogram_id,
                                       $dakType = DAK_DAPTORIK)
    {
        $DakUsersTable = TableRegistry::get('DakUsers');
        $data = $DakUsersTable->find()->where(['dak_id' => $dak_id, 'to_office_unit_id' => $dak_officer_unit_id,
            'to_officer_designation_id' => $dak_office_organogram_id, 'dak_type' => $dakType])->first();
        return $data;
    }

    public function getBangladate()
    {
        if ($this->request->is('ajax')) {
            if (!empty($this->request->data)) {
                $date = !empty($this->request->data['date']) ? $this->request->data['date'] : date("Y-m-d");
                $lang = !empty($this->request->data['lang']) ? $this->request->data['lang']: 'bn';
                 if(!empty($lang) && $lang=='eng'){
                     $bn = new \Date(strtotime($date));
                    $banglaDate = $bn->get_date();
                    echo json_encode((array('date' => $banglaDate[0], 'month' => $banglaDate[1],
                        'year' => $banglaDate[2])));
                 }
                else{
                $bn = new BanglaDate(strtotime($date));
                $banglaDate = $bn->get_date();
                echo json_encode((array('date' => $banglaDate[0], 'month' => $banglaDate[1],
                    'year' => $banglaDate[2])));
                }
            }
        }
        die;
    }

//summarynothi

    public function sendSummaryDraftPotrojari($nothiMasterId, $referenceId)
    {
        $this->layout = null;

        if ($this->request->is('post', 'ajax', 'put')) {

            if (empty($nothiMasterId)) {
                echo json_encode(array("status" => 'error', 'msg' => __("দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না")));
                die;
            }

            $potrojariTable = TableRegistry::get("Potrojari");

            $potrojari = $potrojariTable->find()->where(['nothi_part_no' => $nothiMasterId, 'id' => $referenceId, 'potro_status' => 'SummaryDraft'])->first();
            $employee_office = $this->getCurrentDakSection();

            if (!empty($potrojari)) {
                $status = $this->sendDraftCronPotro($potrojari, $nothiMasterId,true,$employee_office);
            } else {
                echo json_encode(array("status" => 'error', 'msg' => __('সার-সংক্ষেপ জারি করা সম্ভব হচ্ছেনা')));
            }
        }

        die;
    }

    public function sendPotrojaryByMinistry($nothipartid, $nothipotroid)
    {

        $potrojari = $this->potrojaryByMinistry($nothipartid, $nothipotroid);

        if (!empty($potrojari)) {
            $current_office_selection = $this->getCurrentDakSection();
            $status = $this->sendDraftCronPotro($potrojari, $nothipartid, true, $current_office_selection);

            if (!$this->NotificationSet('potrojari',
                array("1", "Khosra Sar Sonkhep"), 2,
                $current_office_selection, [], $potrojari->potro_subject)
            ) {

            }
        }
        die;
    }

    private function potrojaryByMinistry($nothimasterid, $nothipotroid)
    {

        $employee_office = $this->getCurrentDakSection();
        $tableOffices = TableRegistry::get('Offices');
        $tableEmployeeOffices = TableRegistry::get('EmployeeOffices');
        $tableOfficeUnits = TableRegistry::get('OfficeUnits');
        $tableSummaryNothiUser = TableRegistry::get('SummaryNothiUsers');

        TableRegistry::remove('Potrojari');
        TableRegistry::remove('PotrojariReceiver');
        TableRegistry::remove('DakDaptoriks');
        TableRegistry::remove('PotrojariAttachments');
        TableRegistry::remove('NothiPotroAttachments');
        TableRegistry::remove('NothiPotros');
        TableRegistry::remove('NothiParts');


        $tablePotroJari = TableRegistry::get('Potrojari');
        $tablePotrojariReceiver = TableRegistry::get('PotrojariReceiver');
        $tableDakDaptorik = TableRegistry::get('DakDaptoriks');
        $tablePotroJariAttachment = TableRegistry::get('PotrojariAttachments');
        $tableNothiPotroAttachment = TableRegistry::get('NothiPotroAttachments');
        $nothiPotroTable = TableRegistry::get('NothiPotros');
        $nothiPartTable = TableRegistry::get('NothiParts');


        $nothiPartInformation = $nothiPartTable->get($nothimasterid);
        $nothiMasters = array();
        $nothiMastersCurrentUser = array();

        $nothiPotros = $nothiPotroTable->find()->where(['nothi_part_no' => $nothimasterid, 'id' => $nothipotroid])->first();

        if (empty($nothiPotros)) {
            echo json_encode(array("status" => 'error', 'msg' => __('খসড়া প্রেরণ করা সম্ভব হচ্ছেনা')));
            return false;
        }

        $mainpotro = $tableNothiPotroAttachment->find()->where(['nothi_part_no' => $nothimasterid,
            'nothi_potro_id' => $nothipotroid, 'attachment_type' => 'text'])->order(['id desc'])->first();

        //check for next receiver

        $getData = $tableSummaryNothiUser->find()->where(['nothi_part_no' => $nothimasterid,
            'potro_id' => $nothipotroid,
            'current_office_id' => $employee_office['office_id'], 'is_sent' => 0])->first();

        $potrojariReceiverInfo = $tableDakDaptorik->get($nothiPotros['dak_id']);
        if (!empty($getData)) {
            $nextSequence = $tableSummaryNothiUser->getNextOffice($getData['sequence_number']);
            if (!empty($nextSequence)) {
                $employeeInformation = $tableEmployeeOffices->getDesignationDetailsInfo($nextSequence['designation_id']);
                $unitinfo = $tableOfficeUnits->get($employeeInformation['office_unit_id']);
                $potrojariReceiverInfo['sender_office_name'];
                $potrojariReceiverInfo['sender_office_id'] = $nextSequence['current_office_id'];
                $potrojariReceiverInfo['sender_office_unit_id'] = $employeeInformation['office_unit_id'];
                $potrojariReceiverInfo['sender_office_unit_name'] = $unitinfo['unit_name_bng'];
                $potrojariReceiverInfo['sender_officer_id'] = $employeeInformation['employee_record_id'];
                $potrojariReceiverInfo['sender_officer_designation_id'] = $employeeInformation['office_unit_organogram_id'];
                $potrojariReceiverInfo['sender_officer_designation_label'] = $employeeInformation['designation_label'];
                $potrojariReceiverInfo['sender_name'] = $employeeInformation['EmployeeRecords']['name_bng'];
            }
        }

        //end
        if ($this->request->is(['post', 'ajax'])) {
            $conn = ConnectionManager::get('default');

            try {
                $conn->begin();

                $potrojari = $tablePotroJari->newEntity();
                $potrojari->sarok_no = $mainpotro['sarok_no'];
                $potrojari->nothi_master_id = $nothiPartInformation['nothi_masters_id'];
                $potrojari->nothi_part_no = $nothimasterid;
                $potrojari->nothi_notes_id = 0;
                $potrojari->nothi_potro_id = $nothipotroid;
                $potrojari->is_summary_nothi = 1;
                $potrojari->potro_type = 14;
                $potrojari->potrojari_date = date("Y-m-d H:i:s");
                $potrojari->potro_status = 'Sent';
                $potrojari->potro_subject = $potrojariReceiverInfo['dak_subject'];
                $potrojari->potro_security_level = isset($potrojariReceiverInfo['dak_security_level'])
                    ? $potrojariReceiverInfo['dak_security_level'] : 0;
                $potrojari->potro_priority_level = isset($potrojariReceiverInfo['dak_priority_level'])
                    ? $potrojariReceiverInfo['dak_priority_level'] : 0;

                $potrojari->modified = date("Y-m-d H:i:s");
                $potrojari->potro_cover = $mainpotro['potro_cover'];
                $potrojari->meta_data = !empty($mainpotro['meta_data']) ? $mainpotro['meta_data'] : $potrojariReceiverInfo['meta_data'];
                $potrojari->potro_description = !empty($mainpotro['content_body']) ? $mainpotro['content_body'] : $potrojariReceiverInfo['dak_description'];

                $recOffice = $tableOffices->get($potrojariReceiverInfo['receiving_office_id']);

                $potrojari->office_id = $potrojariReceiverInfo['receiving_office_id'];
                $potrojari->office_name = $recOffice['office_name_bng'];
                $potrojari->officer_designation_id = $potrojariReceiverInfo['receiving_officer_designation_id'];

                $potrojari->officer_designation_label = $potrojariReceiverInfo['receiving_officer_designation_label'];
                $potrojari->officer_id = $potrojariReceiverInfo['receiving_officer_id'];

                $potrojari->office_unit_id = $potrojariReceiverInfo['receiving_office_unit_id'];

                $potrojari->office_unit_name = $potrojariReceiverInfo['receiving_office_unit_name'];

                $potrojari->officer_name = $potrojariReceiverInfo['receiving_officer_name'];

                $potrojari->created_by = $this->Auth->user('id');
                $potrojari->modified_by = $this->Auth->user('id');
                $potrojari->created = date("Y-m-d H:i:s");
                $potrojari->modified = date("Y-m-d H:i:s");
                $potrojari = $tablePotroJari->save($potrojari);

                $potrojariReceiver = $tablePotrojariReceiver->newEntity();

                $potrojariReceiver->potrojari_id = $potrojari->id;
                $potrojariReceiver->nothi_master_id = $potrojari->nothi_master_id;
                $potrojariReceiver->nothi_part_no = $potrojari->nothi_part_no;
                $potrojariReceiver->nothi_notes_id = $potrojari->nothi_notes_id;
                $potrojariReceiver->nothi_potro_id = $potrojari->nothi_potro_id;
                $potrojariReceiver->potro_type = $potrojari->potro_type;
                $potrojariReceiver->potro_description = $potrojari->potro_description;
                $potrojariReceiver->potro_status = $potrojari->potro_status;
                $potrojariReceiver->modified = $potrojari->modified;
                $potrojariReceiver->created = $potrojari->modified;

                $potrojariReceiver->receiving_office_name = $potrojariReceiverInfo['sender_office_name'];
                $potrojariReceiver->receiving_office_id = $potrojariReceiverInfo['sender_office_id'];
                $potrojariReceiver->receiving_office_unit_id = $potrojariReceiverInfo['sender_office_unit_id'];

                $potrojariReceiver->receiving_office_unit_name = $potrojariReceiverInfo['sender_office_unit_name'];
                $potrojariReceiver->receiving_officer_id = $potrojariReceiverInfo['sender_officer_id'];
                $potrojariReceiver->receiving_officer_designation_id = $potrojariReceiverInfo['sender_officer_designation_id'];
                $potrojariReceiver->receiving_officer_designation_label = $potrojariReceiverInfo['sender_officer_designation_label'];
                $potrojariReceiver->receiving_officer_name = !empty($potrojariReceiverInfo['sender_name'])?$potrojariReceiverInfo['sender_name']:'';

                $potrojariReceiver->created_by = $this->Auth->user('id');
                $potrojariReceiver->modified_by = $this->Auth->user('id');
                $potrojariReceiver->created = date("Y-m-d H:i:s");
                $potrojariReceiver->modified = date("Y-m-d H:i:s");
                $tablePotrojariReceiver->save($potrojariReceiver);

                $attachments = $tablePotroJariAttachment->find()->where(['potrojari_id' => $potrojari->id,
                    'attachment_type <> ' => 'text'])->toArray();

                if (!empty($attachments)) {
                    foreach ($attachments as $k => $file) {
                        if (!empty($file)) {
                            $attachment_data = array();

                            $attachment_data['potrojari_type'] = "Portojari";
                            $attachment_data['potrojari_id'] = $potrojari->id;
                            $attachment_data['attachment_description'] = '';
                            $attachment_data['attachment_type'] = $file['attachment_type'];
                            $attachment_data['file_dir'] = $file['file_dir'];
                            $attachment_data['file_name'] = $file['file_name'];
                            $attachment_data['is_summary_nothi'] = $potrojari->is_summary_nothi;

                            $dak_attachment = $tablePotroJariAttachment->newEntity();
                            $dak_attachment = $tablePotroJariAttachment->patchEntity($dak_attachment,
                                $attachment_data);
                            $dak_attachment->created_by = $this->Auth->user('id');
                            $dak_attachment->modified_by = $this->Auth->user('id');
                            $dak_attachment->created = date("Y-m-d H:i:s");
                            $dak_attachment->modified = date("Y-m-d H:i:s");
                            $tablePotroJariAttachment->save($dak_attachment);
                        }
                    }
                }

                if (!empty($potrojari->potro_description)) {
                    $attachment_data = array();
                    $attachment_data['potrojari_type'] = "Portojari";
                    $attachment_data['potrojari_id'] = $potrojari->id;
                    $attachment_data['attachment_description'] = '';

                    $attachment_data['file_dir'] = '';
                    $attachment_data['file_name'] = '';
                    $attachment_data['is_summary_nothi'] = $potrojari->is_summary_nothi;

                    $attachment_data['attachment_type'] = 'text';
                    $attachment_data['meta_data'] = $potrojari->meta_data;
                    $attachment_data['content_body'] = $potrojari->potro_description;
                    $attachment_data['content_cover'] = $potrojari->potro_cover;

                    $dak_attachment = $tablePotroJariAttachment->newEntity();
                    $dak_attachment = $tablePotroJariAttachment->patchEntity($dak_attachment,
                        $attachment_data);
                    $dak_attachment->created_by = $this->Auth->user('id');
                    $dak_attachment->modified_by = $this->Auth->user('id');
                    $dak_attachment->created = date("Y-m-d H:i:s");
                    $dak_attachment->modified = date("Y-m-d H:i:s");
                    $tablePotroJariAttachment->save($dak_attachment);
                }

                if (!empty($getData)) {
                    $tableSummaryNothiUser->updateAll(['potrojari_id' => $potrojari['id']], ['id' => $getData['id']]);
                }
                $conn->commit();

                return $potrojari;
            } catch (\Exception $ex) {

                $conn->rollback();
                echo json_encode(array("status" => 'error', 'msg' => __('খসড়া প্রেরণ করা সম্ভব হচ্ছেনা')));
                return false;
            }
        }
        return false;
    }

//potrojari
    public function makeRmDraft($nothimasterid, $potroid=0, $type = "potro",$attached_id=0,$potrojari_id = 0,$nothi_office = 0)

    {
        //$this->view = 'potro_draft_offline';
        $employee_office = $this->getCurrentDakSection();

        $otherNothi = false;
        if ($nothi_office != 0 && $nothi_office != $employee_office['office_id']) {
            $otherNothi = true;

            $this->switchOffice($nothi_office, 'MainNothiOffice');
        } else {
            $nothi_office = $employee_office['office_id'];
        }


        $this->set('nothi_office', $nothi_office);
        $this->set('nothiMasterId', $nothimasterid);
        $this->set(compact( 'potroid', 'type','attached_id'));
        $this->set(compact('otherNothi'));

        $guardFileCategories_table = TableRegistry::get("GuardFileCategories");
        $guarfilesubjects = $guardFileCategories_table->getTypes(0, $employee_office);
        $this->set(compact('guarfilesubjects'));

//        TableRegistry::remove('NothiMasterPermissions');
//        TableRegistry::remove('NothiMasterCurrentUsers');
        $nothiMasterPermissionsTable = TableRegistry::get("NothiMasterPermissions");
        $nothiMasterCurrentUsersTable = TableRegistry::get("NothiMasterCurrentUsers");

        $officeUnitsTable = TableRegistry::get('OfficeUnits');
        $potrosTable = TableRegistry::get('NothiPotros');
        $potroInfoTable = TableRegistry::get('NothiPotroAttachments');
        $potrojariTable = TableRegistry::get("Potrojari");
        $nothiPartsTable = TableRegistry::get('NothiParts');
        $potrojariTemplatesTable = TableRegistry::get('PotrojariTemplates');
        $table = TableRegistry::get('PotrojariHeaderSettings');
        try {
            $heading = $table->findByOfficeId($employee_office['office_id'])->firstOrfail();
        } catch (\Exception $ex) {
            $heading = $table->newEntity();
            $heading->office_id = $employee_office['office_id'];
            $heading->office_unit_id = $employee_office['office_unit_id'];
            $heading->office_unit_organogram_id = $employee_office['office_unit_organogram_id'];
            $heading->employee_record_id = $employee_office['officer_id'];
            $heading->potrojari_head = json_encode([
                'head_title' => 'গণপ্রজাতন্ত্রী বাংলাদেশ সরকার ',
                'head_ministry' => $employee_office['ministry_records'],
                'head_office' => $employee_office['office_name'],
                'head_unit' => '...',
                'head_other' => $employee_office['office_web']
            ]);
            $heading->potrojari_head_eng = json_encode([
                'head_title' => "Government of the People\'s Republic of Bangladesh",
//                'head_title' => "Government of the Peoples Republic of Bangladesh",
                'head_ministry' => $employee_office['ministry_name_eng'],
                'head_office' => $employee_office['office_name_eng'],
                'head_unit' => '...',
                'head_other' => $employee_office['office_web']
            ]);

            $table->save($heading);
        }
        $this->set(compact('heading'));

        $nothiPartInformation = $nothiPartsTable->get($nothimasterid);

        $ownUnitInfo = $unitInfo = $officeUnitsTable->get($employee_office['office_unit_id']);

        if(!empty($potrojari_id)){
               $totalPotrojari = $potrojariTable->find()->where(['office_id' => $employee_office['office_id'],
            'YEAR(potrojari_date)' => date('Y'), 'potrojari_draft_office_id' => $employee_office['office_id'],
            'potrojari_draft_unit' => $employee_office['office_unit_id'], 'id <> ' => $potrojari_id])->count();
        }else{
               $totalPotrojari = $potrojariTable->find()->where(['office_id' => $employee_office['office_id'],
            'YEAR(potrojari_date)' => date('Y'), 'potrojari_draft_office_id' => $employee_office['office_id'],
            'potrojari_draft_unit' => $employee_office['office_unit_id']])->count();
        }


        $totalPotrojari = $ownUnitInfo['sarok_no_start'] + $totalPotrojari;

        $this->set('totalPotrojari', Number::format(($totalPotrojari + 1)));

        $nothiMasters = array();
        $nothiMastersCurrentUser = array();

        if (!empty($employee_office['office_id'])) {

            $nothiMasters = $nothiMasterPermissionsTable->hasAccess($nothi_office,
                $employee_office['office_id'],
                $employee_office['office_unit_id'],
                $employee_office['office_unit_organogram_id'],
                $nothiPartInformation['nothi_masters_id'],
                $nothiPartInformation['id']);

            $nothiMastersCurrentUser = $nothiMasterCurrentUsersTable->find()->where(['nothi_part_no' => $nothimasterid,
                'office_id' => $employee_office['office_id'], 'office_unit_id' => $employee_office['office_unit_id'],
                'office_unit_organogram_id' => $employee_office['office_unit_organogram_id'],
                'nothi_office' => $nothi_office])->first();

            if (empty($nothiMasters) || $nothiMasters['privilige_type'] == 0) {
                $this->Flash->error("দুঃখিত! কোনো তথ্য পাওয়া যায়নি");
                $this->redirect(['controller' => 'Dashboard', 'action' => 'dashboard']);
            }

            if (!empty($nothiMastersCurrentUser)) {
                $this->set('privilige_type', $nothiMasters['privilige_type']);
            } else {
                $this->set('privilige_type', 2);
            }
//            $template_list = $this->potrojari_template_list();
            $template_list = [];

            $nothiMasterPermissionsTable = TableRegistry::get("NothiMasterPermissions");
            $permitted_nothi_list = $nothiMasterPermissionsTable->allNothiPermissionList($nothi_office,
                $employee_office['office_id'],
                $employee_office['office_unit_id'],
                $employee_office['office_unit_organogram_id']);

            $potroAttachmentRecord = $potroInfoTable->find('list',
                ['keyField' => 'id', 'valueField' => [
                    "nothi_potro_page_bn", 'subject'
                ]])
                ->select(['subject' => 'NothiPotros.subject', 'NothiPotroAttachments.id',
                    "nothi_potro_page_bn" => 'NothiPotroAttachments.nothi_potro_page_bn'])
                ->join([
                    'NothiPotros' => [
                        'table' => 'nothi_potros',
                        'type' => 'INNER',
                        'conditions' => 'NothiPotros.id = NothiPotroAttachments.nothi_potro_id'
                    ]
                ])->where(['NothiPotroAttachments.nothi_master_id' => $nothiMasters['nothi_masters_id']])
                ->where(['NothiPotroAttachments.nothijato' => 0, 'NothiPotroAttachments.status' => 1,
                ])
                ->where(['NothiPotroAttachments.nothi_part_no IN' => $permitted_nothi_list])
                ->order(['NothiPotroAttachments.nothi_potro_page asc'])->toArray();
//             Potrojari body should be visible as attachment
//            'NothiPotroAttachments.attachment_type <> ' => 'text' -- ommited

            if(!empty($potrojari_id)) {
                $contentInformation = $potrojariTable->getInfo(['id' => $potrojari_id])->first();
                if(!empty($contentInformation) && $contentInformation['potro_status'] == 'Sent'){
                    $this->Flash->error("দুঃখিত! কোনো তথ্য পাওয়া যায়নি");
                    $this->redirect('/nothiDetail/'.$contentInformation['nothi_part_no']);
                }
                $this->set('is_new_potrojari',0);
            } else {
                $contentInformation = $potrojariTable->newEntity();
                $routine_letter = $potrojariTemplatesTable->getTemplateByIdAndVersion(21,'bn');
                $contentInformation->potro_description = $routine_letter['html_content'];
                 $this->set('is_new_potrojari',1);
//                pr($routine_letter); die;
            }

            if (!empty($contentInformation['officer_designation_id'])) {
                if ($contentInformation['can_potrojari'] == 1 && $contentInformation['officer_designation_id']
                    != $employee_office['office_unit_organogram_id']
                ) {
                    $this->set('privilige_type', 2);
                }
            }
            $this->set('potroInfo', array());
            $this->set('potroSubject', "");

            if (!empty($contentInformation['nothi_potro_id']) && $type == 'potro') {
                $potroInfo = $potroInfoTable->get($contentInformation['nothi_potro_id']);
                $this->set('potroInfo', $potroInfo);
                $potroSubject = $potrosTable->get($potroInfo['nothi_potro_id']);
                $this->set('potroSubject', $potroSubject['subject']);
            }
            $this->set('template_list', $template_list);
            $this->set('office_id', $employee_office['office_id']);
            $this->set('employee_office', $employee_office);

            try {
                $nothiRecord = $nothiPartsTable->getAll(['NothiParts.id'=>$nothimasterid])->first();

                $officeUnitTable = TableRegistry::get('OfficeUnits');
                $officeunit = $officeUnitTable->get($nothiRecord['office_units_id']);
                $officeUnitsName = $officeunit['unit_name_bng'];

                $this->set('officeUnitsName', $officeUnitsName);

                $attachments = array();
                if (!empty($contentInformation)) {
                    $contentInformation['dak_subject'] = isset($contentInformation['potro_subject'])
                        ? $contentInformation['potro_subject'] : '';
                    $contentInformation['sender_sarok_no'] = isset($contentInformation['sarok_no'])
                        ? $contentInformation['sarok_no'] : '';
                    $contentInformation['sender_office_id_final'] = isset($contentInformation['office_id'])
                        ? $contentInformation['office_id'] : 0;
                    $contentInformation['sender_officer_designation_id_final'] = isset($contentInformation['officer_designation_id'])
                        ? $contentInformation['officer_designation_id'] : 0;
                    $contentInformation['sender_officer_designation_label_final']
                        = isset($contentInformation['officer_designation_label'])
                        ? $contentInformation['officer_designation_label'] : '';
                    $contentInformation['sender_officer_id_final'] = isset($contentInformation['officer_id'])
                        ? $contentInformation['officer_id'] : 0;
                    $contentInformation['sender_officer_name_final'] = isset($contentInformation['officer_name'])
                        ? $contentInformation['officer_name'] : '';

                    $contentInformation['sovapoti_office_id_final'] = isset($contentInformation['sovapoti_office_id'])
                        ? $contentInformation['sovapoti_office_id'] : 0;
                    $contentInformation['sovapoti_officer_designation_id_final']
                        = isset($contentInformation['sovapoti_officer_designation_id'])
                        ? $contentInformation['sovapoti_officer_designation_id']
                        : 0;
                    $contentInformation['sovapoti_officer_designation_label_final']
                        = isset($contentInformation['sovapoti_officer_designation_label'])
                        ? $contentInformation['sovapoti_officer_designation_label']
                        : '';
                    $contentInformation['sovapoti_officer_id_final'] = isset($contentInformation['sovapoti_officer_id'])
                        ? $contentInformation['sovapoti_officer_id'] : 0;
                    $contentInformation['sovapoti_officer_name_final'] = isset($contentInformation['sovapoti_officer_name'])
                        ? $contentInformation['sovapoti_officer_name'] : '';

                    //approval
                    $contentInformation['approval_office_id_final'] = isset($contentInformation['approval_office_id'])
                        ? $contentInformation['approval_office_id'] : 0;

                    $contentInformation['approval_officer_id_final'] = isset($contentInformation['approval_officer_id'])
                        ? $contentInformation['approval_officer_id'] : 0;

                    $contentInformation['approval_office_unit_id_final'] = isset($contentInformation['approval_office_unit_id'])
                        ? $contentInformation['approval_office_unit_id'] : 0;

                    $contentInformation['approval_officer_designation_id_final']
                        = isset($contentInformation['approval_officer_designation_id'])
                        ? $contentInformation['approval_officer_designation_id']
                        : 0;

                    $contentInformation['approval_officer_designation_label_final']
                        = isset($contentInformation['approval_officer_designation_label'])
                        ? $contentInformation['approval_officer_designation_label']
                        : '';

                    $contentInformation['approval_officer_name_final'] = isset($contentInformation['approval_officer_name'])
                        ? $contentInformation['approval_officer_name'] : '';

                    $contentInformation['approval_office_name_final'] = isset($contentInformation['approval_office_name'])
                        ? $contentInformation['approval_office_name'] : '';

                    $contentInformation['approval_office_unit_name_final'] = isset($contentInformation['approval_office_unit_name'])
                        ? $contentInformation['approval_office_unit_name'] : '';


                    $this->set('draftVersion', $contentInformation);
                    $this->set('nothiMasterInfo', $nothiRecord);
                    $this->set('nothiRecord', $nothiRecord);
                    $this->set('nothimasterid', $nothimasterid);
                    $this->set('referenceid',
                        $contentInformation['nothi_potro_id']);
                    $this->set('referencetype', $type);

                    $potrojari_attachments = TableRegistry::get('PotrojariAttachments');

                    $attachments = $potrojari_attachments->find()->where(['potrojari_id' => $contentInformation->id,
                        'is_inline' => 0])->toArray();

                    $inlineattachments = $potrojari_attachments->find('list',
                        ['keyField' => 'id', 'valueField' => 'potro_id'])->where(['potrojari_id' => $contentInformation->id,
                        'is_inline' => 1])->toArray();
                }

                $this->set('potroAttachmentRecord', $potroAttachmentRecord);
                $this->set('inlineattachments', $inlineattachments);
                $this->set('attachments', $attachments);

                $allOtherDraftsofThisPotro = array();
                if ($contentInformation['nothi_potro_id'] != 0) {
                    $allOtherDraftsofThisPotro = $potrojariTable->find()->select([
                        'Potrojari.id',
                        'Potrojari.nothi_master_id',
                        'Potrojari.nothi_part_no',
                        'Potrojari.potrojari_date',
                        'Potrojari.potro_subject',
                        'Potrojari.potro_type',
                    ])->where(['nothi_potro_id' => $contentInformation['nothi_potro_id'],
                        'potro_status' => 'Draft'])->toArray();

                    if (!empty($allOtherDraftsofThisPotro)) {
                        $potrojariTemplateTable = TableRegistry::get('PotrojariTemplates');
                        foreach ($allOtherDraftsofThisPotro as $key => &$value) {

                            $templatename = $potrojariTemplateTable->get($value['potro_type']);
                            $value['template_name'] = $templatename['template_name'];
                        }
                    }
                }

                $this->set('allOtherDrafts', $allOtherDraftsofThisPotro);
                $time = new Time(date('Y-m-d'));
                $this->set('signature_date_bangla', $time->i18nFormat('d-M-Y', null, 'bn-BD'));
                $token = $this->generateToken([
                'office_id'=> $employee_office['office_id'],
                'office_unit_organogram_id'=>$employee_office['office_unit_organogram_id'],
                'parent_id'=> $potrojari_id,
                'module'=>'Potrojari',
                'module_type'=> 'Nothi',
            ],['exp'=>time() + 3600*3]);
            $this->set('temp_token',$token);
            if(!empty($potrojari_id)){
                // potro offline: potrojari_potrojari-id_part-no -- for edit potrojari
                $this->set('offline_potro_draft_key','potrojari_'.$potrojari_id.'_'.$contentInformation['nothi_part_no']);
            }else{
                // potro offline: potrojari_potrojari-id_part-no -- for make draft
                $this->set('offline_potro_draft_key','potro_'.$employee_office['office_unit_organogram_id'].'_'.$employee_office['office_id'].'_'.$nothimasterid.'_'.$type.'_'.$potroid);
            }

            } catch (\Exception $ex) {

                $this->Flash->error("দুঃখিত! কোনো তথ্য পাওয়া যায়নি");
                $this->redirect(['controller' => 'Dashboard', 'action' => 'dashboard']);
            }
        }
    }

    public function potroDraft($nothimasterid, $potrojari_id, $type = "potro",
                               $nothi_office = 0)
    {

        //$this->view = 'potro_draft_offline';
        $employee_office = $this->getCurrentDakSection();

        $otherNothi = false;
        if ($nothi_office != 0 && $nothi_office != $employee_office['office_id']) {
            $otherNothi = true;

            $this->switchOffice($nothi_office, 'MainNothiOffice');
        } else {
            $nothi_office = $employee_office['office_id'];
        }


        $this->set('nothi_office', $nothi_office);
        $this->set(compact('otherNothi'));

        $guardFileCategories_table = TableRegistry::get("GuardFileCategories");
        $guarfilesubjects = $guardFileCategories_table->getTypes(0, $employee_office);
        $this->set(compact('guarfilesubjects'));

        TableRegistry::remove('NothiMasterPermissions');
         TableRegistry::remove('NothiMasterCurrentUsers');
        $nothiMasterPermissionsTable = TableRegistry::get("NothiMasterPermissions");
        $nothiMasterCurrentUsersTable = TableRegistry::get("NothiMasterCurrentUsers");

        $officeUnitsTable = TableRegistry::get('OfficeUnits');
        $potrosTable = TableRegistry::get('NothiPotros');
        $potroInfoTable = TableRegistry::get('NothiPotroAttachments');
        $potrojariTable = TableRegistry::get("Potrojari");
        $nothiPartsTable = TableRegistry::get('NothiParts');
        $table = TableRegistry::get('PotrojariHeaderSettings');
        try {
            $heading = $table->findByOfficeId($employee_office['office_id'])->firstOrfail();
        } catch (\Exception $ex) {
            $heading = $table->newEntity();
            $heading->office_id = $employee_office['office_id'];
            $heading->office_unit_id = $employee_office['office_unit_id'];
            $heading->office_unit_organogram_id = $employee_office['office_unit_organogram_id'];
            $heading->employee_record_id = $employee_office['officer_id'];
            $heading->potrojari_head = json_encode([
                'head_title' => 'গণপ্রজাতন্ত্রী বাংলাদেশ সরকার ',
                'head_ministry' => $employee_office['ministry_records'],
                'head_office' => $employee_office['office_name'],
                'head_unit' => '...',
                'head_other' => $employee_office['office_web']
            ]);
            $heading->potrojari_head_eng = json_encode([
                'head_title' => "Government of the People\'s Republic of Bangladesh",
//                'head_title' => "Government of the Peoples Republic of Bangladesh",
                'head_ministry' => $employee_office['ministry_name_eng'],
                'head_office' => $employee_office['office_name_eng'],
                'head_unit' => '...',
                'head_other' => $employee_office['office_web']
            ]);

            $table->save($heading);
        }
        $this->set(compact('heading'));

        $nothiPartInformation = $nothiPartsTable->get($nothimasterid);

        $ownUnitInfo = $unitInfo = $officeUnitsTable->get($employee_office['office_unit_id']);

        $totalPotrojari = $potrojariTable->find()->where(['office_id' => $employee_office['office_id'],
            'YEAR(potrojari_date)' => date('Y'), 'potrojari_draft_office_id' => $employee_office['office_id'],
            'potrojari_draft_unit' => $employee_office['office_unit_id'], 'id <> ' => $potrojari_id])->count();

        $totalPotrojari = $ownUnitInfo['sarok_no_start'] + $totalPotrojari;

        $this->set('totalPotrojari', Number::format(($totalPotrojari + 1)));

        $nothiMasters = array();
        $nothiMastersCurrentUser = array();

        if (!empty($employee_office['office_id'])) {

            $nothiMasters = $nothiMasterPermissionsTable->hasAccess($nothi_office,
                $employee_office['office_id'],
                $employee_office['office_unit_id'],
                $employee_office['office_unit_organogram_id'],
                $nothiPartInformation['nothi_masters_id'],
                $nothiPartInformation['id']);

            $nothiMastersCurrentUser = $nothiMasterCurrentUsersTable->find()->where(['nothi_part_no' => $nothimasterid,
                'office_id' => $employee_office['office_id'], 'office_unit_id' => $employee_office['office_unit_id'],
                'office_unit_organogram_id' => $employee_office['office_unit_organogram_id'],
                'nothi_office' => $nothi_office])->first();

            if (empty($nothiMasters) || $nothiMasters['privilige_type'] == 0) {
                $this->Flash->error("দুঃখিত! কোনো তথ্য পাওয়া যায়নি");
                $this->redirect(['controller' => 'Dashboard', 'action' => 'dashboard']);
            }

            if (!empty($nothiMastersCurrentUser)) {
                $this->set('privilige_type', $nothiMasters['privilige_type']);
            } else {
                $this->set('privilige_type', 2);
            }
            $template_list = $this->potrojari_template_list();

            $nothiMasterPermissionsTable = TableRegistry::get("NothiMasterPermissions");
            $permitted_nothi_list = $nothiMasterPermissionsTable->allNothiPermissionList($nothi_office,
                $employee_office['office_id'],
                $employee_office['office_unit_id'],
                $employee_office['office_unit_organogram_id']);

            $potroAttachmentRecord = $potroInfoTable->find('list',
                ['keyField' => 'id', 'valueField' => [
                    "nothi_potro_page_bn", 'subject'
                ]])
                ->select(['subject' => 'NothiPotros.subject', 'NothiPotroAttachments.id',
                    "nothi_potro_page_bn" => 'NothiPotroAttachments.nothi_potro_page_bn'])
                ->join([
                    'NothiPotros' => [
                        'table' => 'nothi_potros',
                        'type' => 'INNER',
                        'conditions' => 'NothiPotros.id = NothiPotroAttachments.nothi_potro_id'
                    ]
                ])->where(['NothiPotroAttachments.nothi_master_id' => $nothiMasters['nothi_masters_id']])
                ->where(['NothiPotroAttachments.nothijato' => 0, 'NothiPotroAttachments.status' => 1,
                    ])
                ->where(['NothiPotroAttachments.nothi_part_no IN' => $permitted_nothi_list])
                ->order(['NothiPotroAttachments.nothi_potro_page asc'])->toArray();
            // Potrojari body should be visible as attachment
//            'NothiPotroAttachments.attachment_type <> ' => 'text' -- ommited

            $contentInformation = $potrojariTable->getInfo(['id' => $potrojari_id])->first();
            // check if jarikrito potro came to edit
            if ($contentInformation['potro_status'] == 'Sent') {
                $this->Flash->error("দুঃখিত! কোনো তথ্য পাওয়া যায়নি");
                $this->redirect('/nothiDetail/'.$contentInformation['nothi_part_no']);
            }
            //check if RM CS or formal letter
            if ($contentInformation['potro_type'] == 21) {
              return  $this->redirect(\Cake\Routing\Router::url(['_name' => 'editRm',$nothimasterid, 0,$type,0,$potrojari_id, $nothi_office],true));
            }
            else if ($contentInformation['potro_type'] == 27) {
                return  $this->redirect(\Cake\Routing\Router::url(['_name' => 'edit-formal-letter',$nothimasterid, 0,$type,0,$potrojari_id, $nothi_office],true));
            }
            else if ($contentInformation['potro_type'] == 30) {
                return  $this->redirect(\Cake\Routing\Router::url(['_name' => 'edit-cs-letter',$nothimasterid, 0,$type,0,$potrojari_id, $nothi_office],true));
            }
            if (!empty($contentInformation['officer_designation_id'])) {
                if($contentInformation['can_potrojari'] == 1){
                    if($contentInformation['potro_type']==17){

                        if($contentInformation['sovapoti_officer_designation_id'] != $employee_office['office_unit_organogram_id']){
                            $this->set('privilige_type', 2);
                        }
                    }elseif($contentInformation['officer_designation_id'] != $employee_office['office_unit_organogram_id']){
                        $this->set('privilige_type', 2);
                    }
                }
            }
            $this->set('potroInfo', array());
            $this->set('potroSubject', "");

            if (!empty($contentInformation['nothi_potro_id']) && $type == 'potro') {
                $potroInfo = $potroInfoTable->get($contentInformation['nothi_potro_id']);
                $this->set('potroInfo', $potroInfo);
                $potroSubject = $potrosTable->get($potroInfo['nothi_potro_id']);
                $this->set('potroSubject', $potroSubject['subject']);
            }
            $this->set('template_list', $template_list);
            $this->set('office_id', $employee_office['office_id']);
            $this->set('employee_office', $employee_office);
            $token = $this->generateToken([
                'office_id'=> $employee_office['office_id'],
                'office_unit_organogram_id'=>$employee_office['office_unit_organogram_id'],
                'parent_id'=> $potrojari_id,
                'module'=>'Potrojari',
                'module_type'=> 'Nothi',
            ],['exp'=>time() + 3600*3]);
            $this->set('temp_token',$token);

            try {
                $nothiRecord = $nothiPartsTable->getAll(['NothiParts.id' => $nothimasterid])->first();

                $officeUnitTable = TableRegistry::get('OfficeUnits');
                $officeunit = $officeUnitTable->get($nothiRecord['office_units_id']);
                $officeUnitsName = $officeunit['unit_name_bng'];

                $this->set('officeUnitsName', $officeUnitsName);

                $attachments = array();
                if (!empty($contentInformation)) {
                    $contentInformation['dak_subject'] = isset($contentInformation['potro_subject'])
                        ? $contentInformation['potro_subject'] : '';
                    $contentInformation['sender_sarok_no'] = isset($contentInformation['sarok_no'])
                        ? $contentInformation['sarok_no'] : '';
                    $contentInformation['sender_office_id_final'] = isset($contentInformation['office_id'])
                        ? $contentInformation['office_id'] : 0;
                    $contentInformation['sender_officer_designation_id_final'] = isset($contentInformation['officer_designation_id'])
                        ? $contentInformation['officer_designation_id'] : 0;
                    $contentInformation['sender_officer_designation_label_final']
                        = isset($contentInformation['officer_designation_label'])
                        ? $contentInformation['officer_designation_label'] : '';
                    $contentInformation['sender_officer_id_final'] = isset($contentInformation['officer_id'])
                        ? $contentInformation['officer_id'] : 0;
                    $contentInformation['sender_officer_name_final'] = isset($contentInformation['officer_name'])
                        ? $contentInformation['officer_name'] : '';

                    $contentInformation['sovapoti_office_id_final'] = isset($contentInformation['sovapoti_office_id'])
                        ? $contentInformation['sovapoti_office_id'] : 0;
                    $contentInformation['sovapoti_officer_designation_id_final']
                        = isset($contentInformation['sovapoti_officer_designation_id'])
                        ? $contentInformation['sovapoti_officer_designation_id']
                        : 0;
                    $contentInformation['sovapoti_officer_designation_label_final']
                        = isset($contentInformation['sovapoti_officer_designation_label'])
                        ? $contentInformation['sovapoti_officer_designation_label']
                        : '';
                    $contentInformation['sovapoti_officer_id_final'] = isset($contentInformation['sovapoti_officer_id'])
                        ? $contentInformation['sovapoti_officer_id'] : 0;
                    $contentInformation['sovapoti_officer_name_final'] = isset($contentInformation['sovapoti_officer_name'])
                        ? $contentInformation['sovapoti_officer_name'] : '';

                    //approval
                    $contentInformation['approval_office_id_final'] = isset($contentInformation['approval_office_id'])
                        ? $contentInformation['approval_office_id'] : 0;

                    $contentInformation['approval_officer_id_final'] = isset($contentInformation['approval_officer_id'])
                        ? $contentInformation['approval_officer_id'] : 0;

                    $contentInformation['approval_office_unit_id_final'] = isset($contentInformation['approval_office_unit_id'])
                        ? $contentInformation['approval_office_unit_id'] : 0;

                    $contentInformation['approval_officer_designation_id_final']
                        = isset($contentInformation['approval_officer_designation_id'])
                        ? $contentInformation['approval_officer_designation_id']
                        : 0;

                    $contentInformation['approval_officer_designation_label_final']
                        = isset($contentInformation['approval_officer_designation_label'])
                        ? $contentInformation['approval_officer_designation_label']
                        : '';

                    $contentInformation['approval_officer_name_final'] = isset($contentInformation['approval_officer_name'])
                        ? $contentInformation['approval_officer_name'] : '';

                    $contentInformation['approval_office_name_final'] = isset($contentInformation['approval_office_name'])
                        ? $contentInformation['approval_office_name'] : '';

                    $contentInformation['approval_office_unit_name_final'] = isset($contentInformation['approval_office_unit_name'])
                        ? $contentInformation['approval_office_unit_name'] : '';

                    $this->set('draftVersion', $contentInformation);
                    $this->set('nothiMasterInfo', $nothiRecord);
                    $this->set('nothiRecord', $nothiRecord);
                    $this->set('nothimasterid', $nothimasterid);
                    $this->set('referenceid',
                        $contentInformation['nothi_potro_id']);
                    $this->set('referencetype', $type);

                    $potrojari_attachments = TableRegistry::get('PotrojariAttachments');

                    $attachments = $potrojari_attachments->find()->where(['potrojari_id' => $contentInformation->id,
                        'is_inline' => 0])->toArray();

                    $inlineattachments = $potrojari_attachments->find('list',
                        ['keyField' => 'id', 'valueField' => 'potro_id'])->where(['potrojari_id' => $contentInformation->id,
                        'is_inline' => 1])->toArray();
                }

                $this->set('potroAttachmentRecord', $potroAttachmentRecord);
                $this->set('inlineattachments', $inlineattachments);
                $this->set('attachments', $attachments);

                $allOtherDraftsofThisPotro = array();
                if ($contentInformation['nothi_potro_id'] != 0) {
                    $allOtherDraftsofThisPotro = $potrojariTable->find()->select([
                        'Potrojari.id',
                        'Potrojari.nothi_master_id',
                        'Potrojari.nothi_part_no',
                        'Potrojari.potrojari_date',
                        'Potrojari.potro_subject',
                        'Potrojari.potro_type',
                    ])->where(['nothi_potro_id' => $contentInformation['nothi_potro_id'],
                        'potro_status' => 'Draft'])->toArray();

                    if (!empty($allOtherDraftsofThisPotro)) {
                        $potrojariTemplateTable = TableRegistry::get('PotrojariTemplates');
                        foreach ($allOtherDraftsofThisPotro as $key => &$value) {

                            $templatename = $potrojariTemplateTable->get($value['potro_type']);
                            $value['template_name'] = $templatename['template_name'];
                        }
                    }
                }

                $this->set('allOtherDrafts', $allOtherDraftsofThisPotro);
                $time = new Time(date('Y-m-d'));
                $this->set('signature_date_bangla', $time->i18nFormat('d-M-Y', null, 'bn-BD'));

                if ($contentInformation['potro_type'] == 13) {
                    $tablePotrojariTemplate = TableRegistry::get('PotrojariTemplates');
                    $template = $tablePotrojariTemplate->get($contentInformation['potro_type']);
                    $this->set('templates', $template);
                }
                // potro offline: potrojari_potrojari-id_part-no
                $this->set('offline_potro_draft_key','potrojari_'.$potrojari_id.'_'.$contentInformation['nothi_part_no']);
            } catch (Exception $ex) {

                $this->Flash->error("দুঃখিত! কোনো তথ্য পাওয়া যায়নি");
                $this->redirect(['controller' => 'Dashboard', 'action' => 'dashboard']);
            }
        }
    }

    public function SendPotrojariEdit($potrojari_id,$nothi_office, $type = "potro")
    {
//        Only for Super Admin to edit jarikrito potro
        if($this->Auth->user('user_role_id') > 2){
            $this->Flash->error("দুঃখিত! আপনি অনুমতিপ্রাপ্ত নন।");
            $this->redirect(['controller' => 'Dashboard', 'action' => 'dashboard']);
        }

        $employee_office['office_unit_id']= 0;
        $employee_office['office_id']= $nothi_office;


            $otherNothi = true;

        $this->switchOffice($nothi_office, 'MainNothiOffice');

        $potrojariTable = TableRegistry::get("Potrojari");
        $contentInformation = $potrojariTable->get($potrojari_id);
        $nothimasterid = $contentInformation['nothi_part_no'];
        $nothiMasters['nothi_masters_id']= $contentInformation['nothi_master_id'];


        $this->set('part_id', $contentInformation['nothi_part_no']);
        $this->set('nothi_office', $nothi_office);
        $this->set(compact('otherNothi'));

        $guardFileCategories_table = TableRegistry::get("GuardFileCategories");
        $guarfilesubjects = $guardFileCategories_table->getTypes(0, $employee_office);
        $this->set(compact('guarfilesubjects'));

        $guardFileApiDatasTable = TableRegistry::get("GuardFileApiDatas");
        $guard_file_api_types = $guardFileApiDatasTable->find('list', ['keyField' => 'type', 'valueField' => 'type'])->distinct(['type'])->toArray();
        $this->set('guard_file_api_types', $guard_file_api_types);

        TableRegistry::remove('NothiMasterPermissions');
        TableRegistry::remove('NothiMasterCurrentUsers');


        $officeUnitsTable = TableRegistry::get('OfficeUnits');
        $potrosTable = TableRegistry::get('NothiPotros');
        $potroInfoTable = TableRegistry::get('NothiPotroAttachments');
        $potrojariTable = TableRegistry::get("Potrojari");
        $nothiPartsTable = TableRegistry::get('NothiParts');
        $table = TableRegistry::get('PotrojariHeaderSettings');


        $heading = $table->findByOfficeId($employee_office['office_id'])->firstOrfail();

        $this->set(compact('heading'));

        $ownUnitInfo = $unitInfo = $officeUnitsTable->get($employee_office['office_unit_id']);

        $totalPotrojari = $potrojariTable->find()->where(['office_id' => $employee_office['office_id'],
            'YEAR(potrojari_date)' => date('Y'), 'potrojari_draft_office_id' => $employee_office['office_id'],
            'potrojari_draft_unit' => $employee_office['office_unit_id'], 'id <> ' => $potrojari_id])->count();

        $totalPotrojari = $ownUnitInfo['sarok_no_start'] + $totalPotrojari;

        $this->set('totalPotrojari', Number::format(($totalPotrojari + 1)));

        if (!empty($employee_office['office_id'])) {

                $this->set('privilige_type', 1);

            $template_list = $this->potrojari_template_list();


            $potroAttachmentRecord = $potroInfoTable->find('list',
                ['keyField' => 'id', 'valueField' => [
                    "nothi_potro_page_bn", 'subject'
                ]])
                ->select(['subject' => 'NothiPotros.subject', 'NothiPotroAttachments.id',
                    "nothi_potro_page_bn" => 'NothiPotroAttachments.nothi_potro_page_bn'])
                ->join([
                    'NothiPotros' => [
                        'table' => 'nothi_potros',
                        'type' => 'INNER',
                        'conditions' => 'NothiPotros.id = NothiPotroAttachments.nothi_potro_id'
                    ]
                ])->where(['NothiPotroAttachments.nothi_master_id' => $nothiMasters['nothi_masters_id']])
                ->where(['NothiPotroAttachments.nothijato' => 0, 'NothiPotroAttachments.status' => 1,
                    'NothiPotroAttachments.attachment_type <> ' => 'text'])
                ->order(['NothiPotroAttachments.nothi_potro_page asc'])->toArray();

            $this->set('potroInfo', array());
            $this->set('potroSubject', "");

            if (!empty($contentInformation['nothi_potro_id']) && $type == 'potro') {
                $potroInfo = $potroInfoTable->get($contentInformation['nothi_potro_id']);
                $this->set('potroInfo', $potroInfo);
                $potroSubject = $potrosTable->get($potroInfo['nothi_potro_id']);
                $this->set('potroSubject', $potroSubject['subject']);
            }
            $this->set('template_list', $template_list);
            $this->set('office_id', $employee_office['office_id']);
            $this->set('employee_office', $employee_office);

            $token = $this->generateToken([
                'office_id'=> $employee_office['office_id'],
                'office_unit_organogram_id'=>$employee_office['office_unit_organogram_id'],
                'parent_id'=> $potrojari_id,
                'module'=>'Potrojari'
            ],['exp'=>time() + 3600*3]);
            $this->set('temp_token',$token);

            try {
                $nothiRecord = $nothiPartsTable->getAll(['NothiParts.id' => $nothimasterid])->first();

                $officeUnitTable = TableRegistry::get('OfficeUnits');
                $officeunit = $officeUnitTable->get($nothiRecord['office_units_id']);
                $officeUnitsName = $officeunit['unit_name_bng'];

                $this->set('officeUnitsName', $officeUnitsName);

                $attachments = array();
                if (!empty($contentInformation)) {
                    $contentInformation['dak_subject'] = isset($contentInformation['potro_subject'])
                        ? $contentInformation['potro_subject'] : '';
                    $contentInformation['sender_sarok_no'] = isset($contentInformation['sarok_no'])
                        ? $contentInformation['sarok_no'] : '';
                    $contentInformation['sender_office_id_final'] = isset($contentInformation['office_id'])
                        ? $contentInformation['office_id'] : 0;
                    $contentInformation['sender_officer_designation_id_final'] = isset($contentInformation['officer_designation_id'])
                        ? $contentInformation['officer_designation_id'] : 0;
                    $contentInformation['sender_officer_designation_label_final']
                        = isset($contentInformation['officer_designation_label'])
                        ? $contentInformation['officer_designation_label'] : '';
                    $contentInformation['sender_officer_id_final'] = isset($contentInformation['officer_id'])
                        ? $contentInformation['officer_id'] : 0;
                    $contentInformation['sender_officer_name_final'] = isset($contentInformation['officer_name'])
                        ? $contentInformation['officer_name'] : '';

                    $contentInformation['sovapoti_office_id_final'] = isset($contentInformation['sovapoti_office_id'])
                        ? $contentInformation['sovapoti_office_id'] : 0;
                    $contentInformation['sovapoti_officer_designation_id_final']
                        = isset($contentInformation['sovapoti_officer_designation_id'])
                        ? $contentInformation['sovapoti_officer_designation_id']
                        : 0;
                    $contentInformation['sovapoti_officer_designation_label_final']
                        = isset($contentInformation['sovapoti_officer_designation_label'])
                        ? $contentInformation['sovapoti_officer_designation_label']
                        : '';
                    $contentInformation['sovapoti_officer_id_final'] = isset($contentInformation['sovapoti_officer_id'])
                        ? $contentInformation['sovapoti_officer_id'] : 0;
                    $contentInformation['sovapoti_officer_name_final'] = isset($contentInformation['sovapoti_officer_name'])
                        ? $contentInformation['sovapoti_officer_name'] : '';

                    //approval
                    $contentInformation['approval_office_id_final'] = isset($contentInformation['approval_office_id'])
                        ? $contentInformation['approval_office_id'] : 0;

                    $contentInformation['approval_officer_id_final'] = isset($contentInformation['approval_officer_id'])
                        ? $contentInformation['approval_officer_id'] : 0;

                    $contentInformation['approval_office_unit_id_final'] = isset($contentInformation['approval_office_unit_id'])
                        ? $contentInformation['approval_office_unit_id'] : 0;

                    $contentInformation['approval_officer_designation_id_final']
                        = isset($contentInformation['approval_officer_designation_id'])
                        ? $contentInformation['approval_officer_designation_id']
                        : 0;

                    $contentInformation['approval_officer_designation_label_final']
                        = isset($contentInformation['approval_officer_designation_label'])
                        ? $contentInformation['approval_officer_designation_label']
                        : '';

                    $contentInformation['approval_officer_name_final'] = isset($contentInformation['approval_officer_name'])
                        ? $contentInformation['approval_officer_name'] : '';

                    $contentInformation['approval_office_name_final'] = isset($contentInformation['approval_office_name'])
                        ? $contentInformation['approval_office_name'] : '';

                    $contentInformation['approval_office_unit_name_final'] = isset($contentInformation['approval_office_unit_name'])
                        ? $contentInformation['approval_office_unit_name'] : '';


                    $this->set('draftVersion', $contentInformation);
                    $this->set('nothiMasterInfo', $nothiRecord);
                    $this->set('nothiRecord', $nothiRecord);
                    $this->set('nothimasterid', $nothimasterid);
                    $this->set('referenceid',
                        $contentInformation['nothi_potro_id']);
                    $this->set('referencetype', $type);

                    $potrojari_attachments = TableRegistry::get('PotrojariAttachments');

                    $attachments = $potrojari_attachments->find()->where(['potrojari_id' => $contentInformation->id,
                        'is_inline' => 0])->toArray();

                    $inlineattachments = $potrojari_attachments->find('list',
                        ['keyField' => 'id', 'valueField' => 'potro_id'])->where(['potrojari_id' => $contentInformation->id,
                        'is_inline' => 1])->toArray();
                }

                $this->set('potroAttachmentRecord', $potroAttachmentRecord);
                $this->set('inlineattachments', $inlineattachments);
                $this->set('attachments', $attachments);

                $allOtherDraftsofThisPotro = array();
                if ($contentInformation->nothi_potro_id != 0) {
                    $allOtherDraftsofThisPotro = $potrojariTable->find()->select([
                        'Potrojari.id',
                        'Potrojari.nothi_master_id',
                        'Potrojari.nothi_part_no',
                        'Potrojari.potrojari_date',
                        'Potrojari.potro_subject',
                        'Potrojari.potro_type',
                    ])->where(['nothi_potro_id' => $contentInformation->nothi_potro_id,
                        'potro_status' => 'Draft'])->toArray();

                    if (!empty($allOtherDraftsofThisPotro)) {
                        $potrojariTemplateTable = TableRegistry::get('PotrojariTemplates');
                        foreach ($allOtherDraftsofThisPotro as $key => &$value) {

                            $templatename = $potrojariTemplateTable->get($value['potro_type']);
                            $value['template_name'] = $templatename['template_name'];
                        }
                    }
                }

                $this->set('allOtherDrafts', $allOtherDraftsofThisPotro);
                $time = new Time(date('Y-m-d'));
                $this->set('signature_date_bangla', $time->i18nFormat('d-M-Y', null, 'bn-BD'));

                if ($contentInformation->potro_type == 13) {
                    $tablePotrojariTemplate = TableRegistry::get('PotrojariTemplates');
                    $template = $tablePotrojariTemplate->get($contentInformation->potro_type);
                    $this->set('templates', $template);
                }
            } catch (Exception $ex) {

                $this->Flash->error("দুঃখিত! কোনো তথ্য পাওয়া যায়নি");
                $this->redirect(['controller' => 'Dashboard', 'action' => 'dashboard']);
            }
        }
    }

    public function showChangeLog()
    {
        $this->layout = null;
        $allDrafts = array();
        if ($this->request->is('ajax')) {
            if (!empty($this->request->data)) {
                $nothi_office = !empty($this->request->data['nothi_office']) ? intval($this->request->data['nothi_office'])
                    : 0;
                $employee_office = $this->getCurrentDakSection();
                $otherNothi = false;
                if ($nothi_office != 0 && $nothi_office != $employee_office['office_id']) {
                    $otherNothi = true;

                    $this->switchOffice($nothi_office, 'MainNothiOffice');
                } else {
                    $nothi_office = $employee_office['office_id'];
                }

                $this->set('nothi_office', $nothi_office);
                $this->set(compact('otherNothi'));

                $potrojariVersionTable = TableRegistry::get('PotrojariVersions');

                $potrojariid = $this->request->data['potrojari_id'];
                $nothimasterid = $this->request->data['nothi_master_id'];
                try {
                    $allDrafts = $potrojariVersionTable->find()->where(['potrojari_id' => $potrojariid,
                        'nothi_part_no' => $nothimasterid])->toArray();
                } catch (Exception $ex) {

                }
            }
        }
        $this->set('allDrafts', $allDrafts);
    }

    public function testEmail($mail)
    {
        $email = new Email('default');

        $email->emailFormat('html')->from(['nothi@nothi.org.bd' => 'test'])
            ->to($mail)
            ->subject('test');

        try {
            if ($email->send("আপনার একটি পত্র এসেছে। মূল পত্র সহ সকল সংযুক্তি ইমেইলের সংযুক্তিতে দেয়া হলো। <br/><br/>ধন্যবাদ।")) {

            }
        } catch (\Exception $ex) {
            echo $ex->getMessage();
        }

        die;
    }

    public function potrojariNisponnoRecordUpdate()
    {
        $this->layout = 'dashboard';
        $office_domains = TableRegistry::get('OfficeDomains');
        $all_offices = $office_domains->find()->where(['status' => 1])->toArray();
        $this->set(compact('all_offices'));
//            $this->view   = 'performance_report_offices_from_beginning';
    }

    public function noteNisponnoRecordUpdate()
    {
        $this->layout = 'dashboard';
        $office_domains = TableRegistry::get('OfficeDomains');
        $all_offices = $office_domains->find()->where(['status' => 1])->toArray();
        $this->set(compact('all_offices'));
        $this->set('note', 1);
        $this->view = 'potrojari_nisponno_record_update';
    }

    public function potrojariNisponnoRecord()
    {
        set_time_limit(0);

//        $NisponnoRecordsTable->hasData(1,1);
//        pr($NisponnoRecordsTable);die;
        Time::setToStringFormat('yyyy-MM-dd');
        $data = [
            'status' => 'error',
            'msg' => 'No Office ID'
        ];
        if ($this->request->is('post') && !empty($this->request->data['office_id'])) {
            try {
                $office_id = $this->request->data['office_id'];
                $this->switchOffice($office_id, 'DashboardOffice');
                $PotrojariTable = TableRegistry::get('Potrojari');
                $NisponnoRecordsTable = TableRegistry::get('NisponnoRecords');
                $all_potrojaris = $PotrojariTable->find()->select(['nothi_master_id',
                    'nothi_part_no',
                    'nothi_notes_id',
                    'officer_id', 'office_id', 'office_unit_id', 'officer_designation_id',
                    'potrojari_date' => 'date(potrojari_date)'])->where(['potro_status' => 'sent'])->toArray();
                $cnt = 0;
                $total = 0;
                $already = 0;
                foreach ($all_potrojaris as $potrojari) {
                    $time = new Time($potrojari['potrojari_date']);
                    $potrojari['potrojari_date'] = $time->i18nFormat(null, null,
                        'en-US');
                    $total++;
                    $has_data = $NisponnoRecordsTable->hasData($potrojari['nothi_master_id'],
                        $potrojari['nothi_part_no'],
                        $potrojari['nothi_notes_id'], $potrojari['office_id']);
                    if ($has_data == 0) {
                        $nisponnoEntity = $NisponnoRecordsTable->newEntity();
                        $nisponnoEntity->nothi_master_id = $potrojari['nothi_master_id'];
                        $nisponnoEntity->nothi_part_no = $potrojari['nothi_part_no'];
                        $nisponnoEntity->type = 'potrojari';
                        $nisponnoEntity->nothi_onucched_id = $potrojari['nothi_notes_id'];
                        $nisponnoEntity->nothi_office_id = $potrojari['office_id'];
                        $nisponnoEntity->office_id = $potrojari['office_id'];
                        $nisponnoEntity->unit_id = $potrojari['office_unit_id'];
                        $nisponnoEntity->designation_id = $potrojari['officer_designation_id'];
                        $nisponnoEntity->employee_id = $potrojari['officer_id'];
                        $nisponnoEntity->operation_date = $potrojari['potrojari_date'];
                        if ($NisponnoRecordsTable->save($nisponnoEntity)) {
                            $cnt++;
                        }
                    } else {
                        $already++;
                    }
                }
                $data = [
                    'status' => 'success',
                    'total' => $total,
                    'saved' => $cnt,
                    'already' => $already
                ];
            } catch (\Exception $ex) {
                $data = [
                    'status' => 'error',
                    'msg' => $ex->getMessage()
                ];
            }
        }
        $this->response->body(json_encode($data));
        $this->response->type('application/json');
        return $this->response;
    }

    public function noteNisponnoRecord()
    {
        set_time_limit(0);

//        $NisponnoRecordsTable->hasData(1,1);
//        pr($NisponnoRecordsTable);die;
        Time::setToStringFormat('yyyy-MM-dd');
        $data = [
            'status' => 'error',
            'msg' => 'No Office ID'
        ];
        if ($this->request->is('post') && !empty($this->request->data['office_id'])) {
            try {
                $office_id = $this->request->data['office_id'];
                $this->switchOffice($office_id, 'DashboardOffice');
                $NothiNotesTable = TableRegistry::get('NothiNotes');
                TableRegistry::remove('NothiMasterCurrentUsers');
                $nothiMasterCurrentUsers = TableRegistry::get('NothiMasterCurrentUsers');
                $NisponnoRecordsTable = TableRegistry::get('NisponnoRecords');

                $all_finished = $nothiMasterCurrentUsers->find()->select(['nothi_master_id',
                    'nothi_part_no',
                    'employee_id', 'office_id', 'office_unit_id', 'office_unit_organogram_id',
                    'nothi_office',
                    'modified'])->where(['is_finished' => 1, 'nothi_office' => $office_id])->toArray();

                $cnt = 0;
                $total = 0;
                $already = 0;
                foreach ($all_finished as $potrojari) {
                    $time = new Time($potrojari['modified']);
                    $potrojari['modified'] = $time->i18nFormat(null, null,
                        'en-US');
                    $last_note = $NothiNotesTable->getLastNote($potrojari['nothi_part_no']);
                    $last_note['note_no'] = !empty($last_note['note_no']) ? $last_note['note_no']
                        : 0;

                    $total++;
                    $has_data = $NisponnoRecordsTable->hasData($potrojari['nothi_master_id'],
                        $potrojari['nothi_part_no'], $last_note['note_no'],
                        $potrojari['office_id']);
                    if ($has_data == 0) {

                        $nisponnoEntity = $NisponnoRecordsTable->newEntity();
                        $nisponnoEntity->nothi_master_id = $potrojari['nothi_master_id'];
                        $nisponnoEntity->nothi_part_no = $potrojari['nothi_part_no'];
                        $nisponnoEntity->type = 'note';
                        $nisponnoEntity->nothi_onucched_id = ($last_note['note_no']
                            != 0 || $last_note['note_no'] != -1) ? $last_note['note_no']
                            : null;
                        $nisponnoEntity->nothi_office_id = $potrojari['nothi_office'];
                        $nisponnoEntity->office_id = $potrojari['office_id'];
                        $nisponnoEntity->unit_id = $potrojari['office_unit_id'];
                        $nisponnoEntity->designation_id = $potrojari['office_unit_organogram_id'];
                        $nisponnoEntity->employee_id = $potrojari['employee_id'];
                        $nisponnoEntity->operation_date = $potrojari['modified'];
                        if ($NisponnoRecordsTable->save($nisponnoEntity)) {
                            $cnt++;
                        }
                    } else {
                        $already++;
                    }
                }
                $data = [
                    'status' => 'success',
                    'total' => $total,
                    'saved' => $cnt,
                    'already' => $already
                ];
            } catch (\Exception $ex) {
                $data = [
                    'status' => 'error',
                    'msg' => 'Technical error occured',
                    'reason' => makeEncryptedData($ex->getMessage()),
                ];
            }
        }
        $this->response->body(json_encode($data));
        $this->response->type('application/json');
        return $this->response;
    }

    public function potrojariDakTracking($office_id = 0, $potrojari_id = 0, $nothi_part_no = 0){
        $employee_office = $current_office_selection = $this->getCurrentDakSection();
        if ($office_id != 0 && $office_id != $employee_office['office_id']) {
            $this->switchOffice($office_id, 'MainNothiOffice');
        } else {
            $office_id = $employee_office['office_id'];
        }

        TableRegistry::remove('PotrojariReceiver');
        TableRegistry::remove('PotrojariOnulipi');
        TableRegistry::remove('Potrojari');

        $receiverTable = TableRegistry::get('PotrojariReceiver');
        $onulipiTable = TableRegistry::get('PotrojariOnulipi');
        $potrojariTable = TableRegistry::get('Potrojari');

        $potrojari = $potrojariTable->find()->where(['id' => $potrojari_id, 'office_id' => $office_id])->first();
        if (empty($potrojari)) {
            $this->Flash->error("দুঃখিত! পত্র পাওয়া যায়নি");
            $this->redirect(['action' => 'dashboard', 'controller' => 'Dashboard']);
            return;
        }
        $receiverList = $receiverTable->find()->where(['potrojari_id' => $potrojari_id])->toArray();
        $onulipiList = $onulipiTable->find()->where(['potrojari_id' => $potrojari_id])->toArray();

        $this->set(compact('receiverList', 'onulipiList', 'potrojari_id',
            'office_id', 'potrojari', 'nothi_part_no'));
    }

    public function getDakTracking(){

        $office_id = $this->request->data['office_id'];
        $dak_id = $this->request->data['dak_id'];

        if(!empty($office_id) && !empty($dak_id)){
            $office_id = getDecryptedData($office_id);
            $dak_id = getDecryptedData($dak_id);

            $employee_office = $current_office_selection = $this->getCurrentDakSection();
            if ($office_id != 0 && $office_id != $employee_office['office_id']) {
                $this->switchOffice($office_id, 'MainNothiOffice');
            } else {
                $office_id = $employee_office['office_id'];
            }

            TableRegistry::remove("DakMovements");
            $dakMovements_table = TableRegistry::get("DakMovements");

            if(!empty($dak_id)){
                $tracking_data = $dakMovements_table->potrojariDakTracking($dak_id);
                $this->response->type('application/json');
                if($tracking_data['status']=='error'){$this->response->body(json_encode($tracking_data['msg']));}
                else {$this->response->body(json_encode(enTobn($tracking_data['data'][0]['dak_actions'])));}
                return $this->response;

            }
        }

    }

    public function pendingList($office_id = 0, $potrojari_id = 0, $nothi_part_no = 0)
    {
        $employee_office = $current_office_selection = $this->getCurrentDakSection();
        if ($office_id != 0 && $office_id != $employee_office['office_id']) {
            $this->switchOffice($office_id, 'MainNothiOffice');
        } else {
            $office_id = $employee_office['office_id'];
        }
        TableRegistry::remove('PotrojariReceiver');
        TableRegistry::remove('PotrojariOnulipi');
        TableRegistry::remove('Potrojari');
        $receiverTable = TableRegistry::get('PotrojariReceiver');
        $onulipiTable = TableRegistry::get('PotrojariOnulipi');
        $potrojariTable = TableRegistry::get('Potrojari');

        $potrojari = $potrojariTable->find()->where(['id' => $potrojari_id, 'office_id' => $office_id])->first();
        if (empty($potrojari)) {
            $this->Flash->error("দুঃখিত! পত্র পাওয়া যায়নি");
            $this->redirect(['action' => 'dashboard', 'controller' => 'Dashboard']);
            return;
        }
        $receiverList = $receiverTable->find()->where(['potrojari_id' => $potrojari_id])->toArray();
        $onulipiList = $onulipiTable->find()->where(['potrojari_id' => $potrojari_id])->toArray();

        $this->set(compact('receiverList', 'onulipiList', 'potrojari_id',
            'office_id', 'potrojari', 'nothi_part_no'));
    }

    public function retrySent($office_id, $potrojari_id, $hard = 0, $multiple=false)
    {
        $employee_office = $current_office_selection = $this->getCurrentDakSection();
        if(empty($employee_office['office_id'])){
            $employee_office['office_id']=-1;
        }
        if ($office_id != 0 && $office_id != $employee_office['office_id']) {
            $this->switchOffice($office_id, 'MainNothiOffice');
        } else {
            $office_id = $employee_office['office_id'];
        }
        TableRegistry::remove('PotrojariReceiver');
        TableRegistry::remove('PotrojariOnulipi');
        $receiverTable = TableRegistry::get('PotrojariReceiver');
        $onulipiTable = TableRegistry::get('PotrojariOnulipi');

        if($hard){
            $receiverTable->updateAll(['run_task' => 0, 'task_reposponse' => '', 'potro_status' => '','is_sent'=>0,'retry_count' => 0], ['receiving_officer_id' => 0, 'potrojari_id' => $potrojari_id]);
            $onulipiTable->updateAll(['run_task' => 0, 'task_reposponse' => '', 'potro_status' => '','is_sent'=>0,'retry_count' => 0], ['receiving_officer_id' => 0, 'potrojari_id' => $potrojari_id]);
        }else{
            $receiverTable->updateAll(['run_task' => 0, 'task_reposponse' => '', 'potro_status' => '','retry_count' => 0], ['is_sent' => 0, 'potrojari_id' => $potrojari_id]);
            $onulipiTable->updateAll(['run_task' => 0, 'task_reposponse' => '', 'potro_status' => '','retry_count' => 0], ['is_sent' => 0, 'potrojari_id' => $potrojari_id]);
        }
        $force = 1;
//        ConnectionManager::drop('MainNothiOffice');
        ConnectionManager::drop('default');
        $domain = \Cake\Routing\Router::url(['controller' => "potrojari",
            'action' => "potrojariPreview", $office_id, $potrojari_id], true);

        $commandReceiver = ROOT . "/bin/cake cronjob potrojariReceiverSent {$office_id} {$potrojari_id} '{$domain}' {$force}> /dev/null 2>&1 &";

        exec($commandReceiver,$out,$t);

        $commandOnulipi = ROOT . "/bin/cake cronjob potrojariOnulipiSent {$office_id} {$potrojari_id} '{$domain}' {$force}> /dev/null 2>&1 &";

        exec($commandOnulipi,$out2,$t2);
        if($multiple==true){
            return true;
        }
        echo json_encode(['status' => 'success','r' => $this->makeEncryptedData($commandReceiver),'o' => $this->makeEncryptedData($commandOnulipi)]);
        die;
    }

	public function retrySentAll($office_id, $office_unit_organogram_id, $hard = 0, $multiple=false)
	{
		$employee_office = $current_office_selection = $this->getCurrentDakSection();
		if(empty($employee_office['office_id'])){
			$employee_office['office_id']=-1;
		}
		if ($office_id != 0 && $office_id != $employee_office['office_id']) {
			$this->switchOffice($office_id, 'MainNothiOffice');
		} else {
			$office_id = $employee_office['office_id'];
		}
		TableRegistry::remove('Potrojari');
		TableRegistry::remove('PotrojariReceiver');
		TableRegistry::remove('PotrojariOnulipi');
		$potrojariTable = TableRegistry::get('Potrojari');
		$receiverTable = TableRegistry::get('PotrojariReceiver');
		$onulipiTable = TableRegistry::get('PotrojariOnulipi');
		$nothiMasterCurrentUsers = TableRegistry::get('NothiMasterCurrentUsers');
        $allCurrentNothi = $nothiMasterCurrentUsers->getAllNothiPartNo($office_id, 0, $office_unit_organogram_id,[],1)->where(['nothi_office' => $office_id])->toArray();

		$potrojari_pending_receiver = $receiverTable->find('list', ['valueField' => 'Potrojari.id'])->join([
            "Potrojari" => [
                'table' => 'potrojari', 'type' => 'inner',
                'conditions' => ['PotrojariReceiver.potrojari_id =Potrojari.id']
            ]
        ])->where(['PotrojariReceiver.is_sent' => 0, 'Potrojari.nothi_part_no IN' => $allCurrentNothi, 'Potrojari.potro_status <>' => 'Draft'])->toArray();

		$potrojari_pending_onulipi = $onulipiTable->find('list', ['valueField' => 'Potrojari.id'])->join([
            "Potrojari" => [
                'table' => 'potrojari', 'type' => 'inner',
                'conditions' => ['PotrojariOnulipi.potrojari_id =Potrojari.id']
            ]
        ])->where(['PotrojariOnulipi.is_sent' => 0, 'Potrojari.nothi_part_no IN' => $allCurrentNothi, 'Potrojari.potro_status <>' => 'Draft'])->toArray();


			$receiverTable->updateAll(['run_task' => 0, 'task_reposponse' => '', 'potro_status' => '','retry_count' => 0], ['is_sent' => 0, 'potrojari_id IN' => $potrojari_pending_receiver]);
			$onulipiTable->updateAll(['run_task' => 0, 'task_reposponse' => '', 'potro_status' => '','retry_count' => 0], ['is_sent' => 0, 'potrojari_id IN' => $potrojari_pending_onulipi]);
		//        ConnectionManager::drop('MainNothiOffice');
//		ConnectionManager::drop('default');

		$commandReceiver = ROOT . "/bin/cake cronjob potrojariReceiverSentAll {$office_id} {$office_unit_organogram_id} 1> /dev/null 2>&1 &";

		exec($commandReceiver,$out,$t);

		$commandOnulipi = ROOT . "/bin/cake cronjob potrojariOnulipiSentAll {$office_id} {$office_unit_organogram_id} 1> /dev/null 2>&1 &";

		exec($commandOnulipi,$out2,$t2);
		if($multiple==true){
			return true;
		}
		echo json_encode(['status' => 'success','r' => $this->makeEncryptedData($commandReceiver),'o' => $this->makeEncryptedData($commandOnulipi)]);
		die;
	}
    public function resendLayerwisePotrojari(){
        if($this->Auth->user('user_role_id')<=2 &&  (!empty($this->request->data['office_id'])) && (!empty($this->request->data['potrojari_ids'])) ){
            $data = ['status' => 'success'];
            try {
                foreach ($this->request->data['potrojari_ids'] as $potrojari_id) {
                    $this->retrySent($this->request->data['office_id'], $potrojari_id, 1, true);
                }
            } catch (\Exception $ex) {
                $data = ['status' => 'success', 'error details'=>$ex];
            }
            $this->response->type('application/json');
            $this->response->body(json_encode($data));
            return $this->response;
        }
        die;
    }

    public function ajaxApprovePotrojari($nothi_office)
    {
        $this->digitalSigantureUrlChecker();
        $employee_office = $this->getCurrentDakSection();
        $otherNothi = false;

        if ($nothi_office > 0 && $nothi_office != $employee_office['office_id']) {

            $otherNothi = true;
            $this->switchOffice($nothi_office, 'MainNothiOffice');
        } else {
            $nothi_office = $employee_office['office_id'];
        }

        if (!empty($this->request->data)) {
            TableRegistry::remove('Potrojari');
            $tablePotrojari = TableRegistry::get('Potrojari');
            $potrojari_id = $this->request->data['potrojari_id'];
            $status = intval($this->request->data['status']);
            // just check whether signature is correct or not
            // skip if soft token -1
            if(!empty($employee_office) && $employee_office['default_sign'] > 0 && $status == 1  && isset($this->request->data['soft_token']) && $this->request->data['soft_token'] != -1){
             $this->loadComponent('DigitalSignatureRelated');
//              $res = $this->DigitalSignature->verifyDigitalSignature(1,$this->request->data['soft_token']);
              $res = $this->DigitalSignatureRelated->signPotrojari($potrojari_id,$this->request->data['soft_token']);
                if($res['status'] =='error'){
                     $this->response->body(json_encode(array('status' => 'error',
                         'msg' => 'দুঃখিত! ডিজিটাল সিগনেচার করা সম্ভব হয়নি। কারণঃ '.$res['msg'])));
                     $this->response->type('application/json');
                     return $this->response;
                }
            }

            $potrojari = $tablePotrojari->get($potrojari_id);

            $now = new Time(date('Y-m-d'));

            if($potrojari->potrojari_language == 'eng'){
                $updateDate = $now->format('d-M-Y');
            }else {
                $updateDate = $now->i18nFormat('d-M-Y');
            }

            if($potrojari->potro_type == 30){
                $regex = '/<span id="sender_signature_date" style="([^"]+)">(.+?)<\/span>/s';
                $result = preg_replace( $regex,
                    '<span id="sender_signature_date" style="$1">'.$updateDate.'</span>',
                    $potrojari->potro_cover
                );

                $regex2 = '/<span id="sender_signature2_date" style="([^"]+)">(.+?)<\/span>/s';
                $result2 = preg_replace( $regex2,
                    '<span id="sender_signature2_date" style="$1">'.$updateDate.'</span>',
                    $result
                );

                $potrojari->potro_cover = $result2;
            }else{
                $regex = '/<span id="sender_signature_date" style="([^"]+)">(.+?)<\/span>/s';
                $result = preg_replace( $regex,
                    '<span id="sender_signature_date" style="$1">'.$updateDate.'</span>',
                    $potrojari->potro_description
                );

                $regex2 = '/<span id="sender_signature2_date" style="([^"]+)">(.+?)<\/span>/s';
                $result2 = preg_replace( $regex2,
                    '<span id="sender_signature2_date" style="$1">'.$updateDate.'</span>',
                    $result
                );

                $potrojari->potro_description = $result2;
            }


            if ($potrojari['officer_designation_id'] == $employee_office['office_unit_organogram_id']
                && $potrojari['potro_type'] != 17
            ) {
                $potrojari->can_potrojari = intval($status);
            } else if ($potrojari['sovapoti_officer_designation_id'] == $employee_office['office_unit_organogram_id']
                && $potrojari['potro_type'] == 17
            ) {
                $potrojari->can_potrojari = intval($status);
            }

            $tablePotrojari->save($potrojari);

            $potrojari_attachment_table_intance = TableRegistry::get('PotrojariAttachments');

            if($potrojari->potro_type == 30){
                $potrojari_attachment_table_intance->updateAll(['content_body'=>((!empty($potrojari->attached_potro)?($potrojari->attached_potro . '<br/>'):'') .$potrojari->potro_cover)],[
                    'potrojari_id'=>$potrojari->id,'is_inline' => 0,'attachment_type' => 'text'
                ]);
            }else{
                $potrojari_attachment_table_intance->updateAll(['content_body'=>((!empty($potrojari->attached_potro)?($potrojari->attached_potro . '<br/>'):'') .$potrojari->potro_description)],[
                    'potrojari_id'=>$potrojari->id,'is_inline' => 0,'attachment_type' => 'text'
                ]);
            }

        }

        $this->response->body(json_encode(['status' => 'success']));
        $this->response->type('json');
        return $this->response;
    }

    public function apiApprovePotrojari($nothi_office)
    {
        $user_designation = !empty($this->request->data['user_designation']) ? $this->request->data['user_designation'] : 0;

        $jsonArray = array(
            "status" => "error",
            "error_code" => 404,
            "message" => "Invalid Request"
        );

        if (empty($user_designation)) {
            echo json_encode($jsonArray);
            die;
        }

        if (empty($nothi_office)) {
            echo json_encode($jsonArray);
            die;
        }

        $employee_office = $this->setCurrentDakSection($user_designation);

        $this->switchOffice($nothi_office, 'MainNothiOffice');
        TableRegistry::remove('Potrojari');
        $tablePotrojari = TableRegistry::get('Potrojari');

        if (!empty($this->request->data)) {
            $potrojari_id = $this->request->data['potrojari_id'];
            $status = intval($this->request->data['status']);

            // skip if soft token -1
            if(!empty($employee_office) && $employee_office['default_sign'] > 0 && $status == 1  && isset($this->request->data['soft_token']) && $this->request->data['soft_token'] != -1){
                $this->loadComponent('DigitalSignatureRelated');
//              $res = $this->DigitalSignature->verifyDigitalSignature(1,$this->request->data['soft_token']);
                $res = $this->DigitalSignatureRelated->signPotrojari($potrojari_id,$this->request->data['soft_token'],$employee_office['default_sign'],$employee_office);
                if($res['status'] =='error'){
                    $this->response->body(json_encode(array('status' => 'error',
                                                            'msg' => 'দুঃখিত! ডিজিটাল সিগনেচার করা সম্ভব হয়নি। কারণঃ '.$res['msg'])));
                    $this->response->type('application/json');
                    return $this->response;
                }
            }

            $potrojari = $tablePotrojari->get($potrojari_id);

            $now = new Time(date('Y-m-d'));

            if($potrojari->potrojari_language == 'eng'){
                $updateDate = $now->format('d-M-Y');
            }else {
                $updateDate = $now->i18nFormat('d-M-Y');
            }

            if($potrojari->potro_type == 30){
                $regex = '/<span id="sender_signature_date" style="([^"]+)">(.+?)<\/span>/s';
                $result = preg_replace( $regex,
                    '<span id="sender_signature_date" style="$1">'.$updateDate.'</span>',
                    $potrojari->potro_cover
                );

                $regex2 = '/<span id="sender_signature2_date" style="([^"]+)">(.+?)<\/span>/s';
                $result2 = preg_replace( $regex2,
                    '<span id="sender_signature2_date" style="$1">'.$updateDate.'</span>',
                    $result
                );

                $potrojari->potro_cover = $result2;
            }else{
                $regex = '/<span id="sender_signature_date" style="([^"]+)">(.+?)<\/span>/s';
                $result = preg_replace( $regex,
                    '<span id="sender_signature_date" style="$1">'.$updateDate.'</span>',
                    $potrojari->potro_description
                );

                $regex2 = '/<span id="sender_signature2_date" style="([^"]+)">(.+?)<\/span>/s';
                $result2 = preg_replace( $regex2,
                    '<span id="sender_signature2_date" style="$1">'.$updateDate.'</span>',
                    $result
                );

                $potrojari->potro_description = $result2;
            }

            if (!empty($potrojari)) {
                if ($potrojari['officer_designation_id'] == $employee_office['office_unit_organogram_id']
                    && $potrojari['potro_type'] != 17
                ) {
                    $potrojari->can_potrojari = intval($status);
                } else if ($potrojari['sovapoti_officer_designation_id'] == $employee_office['office_unit_organogram_id']
                    && $potrojari['potro_type'] == 17
                ) {
                    $potrojari->can_potrojari = intval($status);
                }
                $tablePotrojari->save($potrojari);

                $potrojari_attachment_table_intance = TableRegistry::get('PotrojariAttachments');

                if($potrojari->potro_type == 30){
                    $potrojari_attachment_table_intance->updateAll(['content_body'=>((!empty($potrojari->attached_potro)?($potrojari->attached_potro . '<br/>'):'') .$potrojari->potro_cover)],[
                        'potrojari_id'=>$potrojari->id,'is_inline' => 0,'attachment_type' => 'text'
                    ]);
                }else{
                    $potrojari_attachment_table_intance->updateAll(['content_body'=>((!empty($potrojari->attached_potro)?($potrojari->attached_potro . '<br/>'):'') .$potrojari->potro_description)],[
                        'potrojari_id'=>$potrojari->id,'is_inline' => 0,'attachment_type' => 'text'
                    ]);
                }

            }
        }

        $this->response->body(json_encode(['status' => 'success']));
        $this->response->type('json');
        return $this->response;
    }

    public function potrojariPreview($office_id, $portrojari_id)
    {
        $this->layout = 'single';
        $this->switchOffice($office_id, 'MainNothiOffice');

        TableRegistry::remove('Potrojari');
        $potrojariTable = TableRegistry::get('Potrojari');
        $potrojari = $potrojariTable->get($portrojari_id);
        $this->set('potrojari', $potrojari);
    }

    public function headerSetting()
    {
        $employee_office = $this->getCurrentDakSection();
        $this->set(compact('employee_office'));
    }

    public function setPotrojariHeader()
    {
        $employee_office = $this->getCurrentDakSection();
        $table = TableRegistry::get('PotrojariHeaderSettings');
        try {
            $heading = $table->findByOfficeId($employee_office['office_id'])->firstOrfail();
        } catch (\Exception $ex) {
            $heading = $table->newEntity();
        }

        if (!empty($this->request->data)) {
            $heading->office_id = $employee_office['office_id'];
            $heading->office_unit_id = $employee_office['office_unit_id'];
            $heading->office_unit_organogram_id = $employee_office['office_unit_organogram_id'];
            $heading->employee_record_id = $employee_office['officer_id'];
            $heading->write_unit = !empty($this->request->data['write_unit']) ? 1 : 0;
            $json = jsonA($heading->potrojari_head);
            $heading->potrojari_head = json_encode([
                'remove_header_logo' => !empty($json['remove_header_logo']) ? 1: 0,
                'remove_header_left_slogan' => !empty($json['remove_header_left_slogan']) ? 1: 0,
                'remove_header_right_slogan' => !empty($json['remove_header_right_slogan']) ? 1: 0,
                'remove_header_head_1' => !empty($json['remove_header_head_1']) ? 1: 0,
                'remove_header_head_2' => !empty($json['remove_header_head_2']) ? 1: 0,
                'remove_header_head_3' => !empty($json['remove_header_head_3']) ? 1: 0,
                'remove_header_unit' => !empty($json['remove_header_unit']) ? 1: 0,
                'remove_header_head_4' => !empty($json['remove_header_head_4']) ? 1: 0,
                'remove_header_head_5' => !empty($json['remove_header_head_5']) ? 1: 0,
                'banner_position' => !empty($json['banner_position']) ? $json['banner_position']: 'left',
                'banner_width' => !empty($json['banner_width']) ? $json['banner_position']: 'auto',
                'head_title' => !empty($this->request->data['head_title']) ? $this->request->data['head_title'] : '...',
                'head_ministry' => !empty($this->request->data['head_ministry']) ? $this->request->data['head_ministry'] : '...',
                'head_office' => !empty($this->request->data['head_office']) ? $this->request->data['head_office'] : '...',
                'head_unit' => !empty($this->request->data['head_unit']) ? $this->request->data['head_unit'] : '...',
                'head_other' => !empty($this->request->data['head_other']) ? $this->request->data['head_other'] : '...',
                'head_logo_type' => !empty($this->request->data['head_logo_type']) ? $this->request->data['head_logo_type'] : '',
//                'head_logo' => !empty($this->request->data['head_logo_input']) ? $this->request->data['head_logo_input'] : '',
                'head_text' => !empty($this->request->data['head_text']) ? addcslashes($this->request->data['head_text'],'"') : '',
                'head_left_type' => !empty($this->request->data['head_left_type']) ? $this->request->data['head_left_type'] : '',
//                'head_left_logo' => !empty($this->request->data['head_left_logo_input']) ? $this->request->data['head_left_logo_input'] : '',
                'head_left_slogan' => !empty($this->request->data['head_left_slogan']) ? addcslashes($this->request->data['head_left_slogan'],'"') : '',
                'head_right_type' => !empty($this->request->data['head_right_type']) ? addcslashes($this->request->data['head_right_type'],'"') : '',
//                'head_right_logo' => !empty($this->request->data['head_right_logo_input']) ? addcslashes($this->request->data['head_right_logo_input'],'"') : '',
                'head_right_slogan' => !empty($this->request->data['head_right_slogan']) ? addcslashes($this->request->data['head_right_slogan'],'"') : '',
                'head_office_address' => !empty($this->request->data['head_office_address']) ? addcslashes($this->request->data['head_office_address'],'"') : '...'
            ]);


            $heading->potrojari_head_eng = json_encode([
                'remove_header_logo' => !empty($this->request->data['remove_header_logo']) ? 1: 0,
                'remove_header_left_slogan' => !empty($this->request->data['remove_header_left_slogan']) ? 1: 0,
                'remove_header_right_slogan' => !empty($this->request->data['remove_header_right_slogan']) ? 1: 0,
                'remove_header_head_1' => !empty($this->request->data['remove_header_head_1']) ? 1: 0,
                'remove_header_head_2' => !empty($this->request->data['remove_header_head_2']) ? 1: 0,
                'remove_header_head_3' => !empty($this->request->data['remove_header_head_3']) ? 1: 0,
                'remove_header_unit' => !empty($this->request->data['remove_header_unit']) ? 1: 0,
                'remove_header_head_4' => !empty($this->request->data['remove_header_head_4']) ? 1: 0,
                'remove_header_head_5' => !empty($this->request->data['remove_header_head_5']) ? 1: 0,
                'banner_position' => !empty($this->request->data['banner_position']) ? $this->request->data['banner_position']: 'left',
                'banner_width' => !empty($this->request->data['banner_width']) ? $this->request->data['banner_position']: 'auto',
                'head_title' => !empty($this->request->data['head_title_eng']) ? $this->request->data['head_title_eng'] : '...',
                'head_ministry' => !empty($this->request->data['head_ministry_eng']) ? $this->request->data['head_ministry_eng'] : '...',
                'head_office' => !empty($this->request->data['head_office_eng']) ? $this->request->data['head_office_eng'] : '...',
                'head_unit' => !empty($this->request->data['head_unit_eng']) ? $this->request->data['head_unit_eng'] : '...',
                'head_other' => !empty($this->request->data['head_other_eng']) ? $this->request->data['head_other_eng'] : '...',
                'head_logo_type' => !empty($this->request->data['head_logo_type']) ? $this->request->data['head_logo_type'] : '',
//                'head_logo' => !empty($this->request->data['head_logo_input']) ? $this->request->data['head_logo_input'] : '',
                'head_text' => !empty($this->request->data['head_text_eng']) ? addcslashes($this->request->data['head_text_eng'],'"') : '',
                'head_left_type' => !empty($this->request->data['head_left_type']) ? $this->request->data['head_left_type'] : '',
//                'head_left_logo' => !empty($this->request->data['head_left_logo_input']) ? $this->request->data['head_left_logo_input'] : '',
                'head_left_slogan' => !empty($this->request->data['head_left_slogan_eng']) ? addcslashes($this->request->data['head_left_slogan_eng'],'"') : '',
                'head_right_type' => !empty($this->request->data['head_right_type']) ? addcslashes($this->request->data['head_right_type'],'"') : '',
//                'head_right_logo' => !empty($this->request->data['head_right_logo_input']) ? addcslashes($this->request->data['head_right_logo_input'],'"') : '',
                'head_right_slogan' => !empty($this->request->data['head_right_slogan_eng']) ? addcslashes($this->request->data['head_right_slogan_eng'],'"') : '',
                'head_office_address' => !empty($this->request->data['head_office_address_eng']) ? addcslashes($this->request->data['head_office_address_eng'],'"') : '...'
            ]);

            $heading->potrojari_head_img = json_encode([
                'head_logo' => !empty($this->request->data['head_logo_input']) ? $this->request->data['head_logo_input'] : '',
                'head_left_logo' => !empty($this->request->data['head_left_logo_input']) ? $this->request->data['head_left_logo_input'] : '',
                'head_right_logo' => !empty($this->request->data['head_right_logo_input']) ? addcslashes($this->request->data['head_right_logo_input'],'"') : ''
                ]);

            try {
                $table->save($heading);

                $this->Flash->success($employee_office['office_name'] . " অফিসের জন্য পত্রজারি হেডার সেটিং করা হয়েছে। ");
            } catch (\Exception $ex) {
                $this->Flash->error($ex->getMessage());
            }
        }

        $this->set(compact('heading', 'employee_office', 'unit_id'));
    }


    public function getPendingPotrojari()
    {
		$employee_office = $this->getCurrentDakSection();
		$office_id = $employee_office['office_id'];

		TableRegistry::remove('PotrojariReceiver');
		TableRegistry::remove('PotrojariOnulipi');
		TableRegistry::remove('NothiMasterCurrentUsers');
		$receiverTable = TableRegistry::get('PotrojariReceiver');
		$onulipiTable = TableRegistry::get('PotrojariOnulipi');
		$nothiMasterCurrentUsers = TableRegistry::get('NothiMasterCurrentUsers');

        $allCurrentNothi = $nothiMasterCurrentUsers->getAllNothiPartNo($office_id, 0, $employee_office['office_unit_organogram_id'],[],1)->where(['nothi_office' => $office_id])->toArray();

		$total['receiver'] = $receiverTable->find()->join([
			"Potrojari" => [
				'table' => 'potrojari', 'type' => 'inner',
				'conditions' => ['PotrojariReceiver.potrojari_id =Potrojari.id']
			]
		])->where(['PotrojariReceiver.is_sent' => 0, 'Potrojari.nothi_part_no IN' => $allCurrentNothi, 'Potrojari.potro_status <>' => 'Draft'])->count();

		$total['onulipi'] = $onulipiTable->find()->join([
			"Potrojari" => [
				'table' => 'potrojari', 'type' => 'inner',
				'conditions' => ['PotrojariOnulipi.potrojari_id =Potrojari.id']
			]
		])->where(['PotrojariOnulipi.is_sent' => 0, 'Potrojari.nothi_part_no IN' => $allCurrentNothi, 'Potrojari.potro_status <>' => 'Draft'])->count();

		cache::delete('total_potrojari_pending_'.$employee_office['office_unit_organogram_id'],'memcached');
		$this->set(compact('office_id', 'total', 'employee_office'));
    }
    public function pendingPotrojariGet() {
		$employee_office = $this->getCurrentDakSection();
		$office_id = $employee_office['office_id'];
		TableRegistry::remove('PotrojariReceiver');
		TableRegistry::remove('PotrojariOnulipi');
		$receiverTable = TableRegistry::get('PotrojariReceiver');
		$onulipiTable = TableRegistry::get('PotrojariOnulipi');
		TableRegistry::remove('NothiMasterCurrentUsers');
		$nothiMasterCurrentUsers = TableRegistry::get('NothiMasterCurrentUsers');

		$allCurrentNothi = $nothiMasterCurrentUsers->getAllNothiPartNo($office_id, 0, $employee_office['office_unit_organogram_id'])->where(['nothi_office' => $office_id])->select(['nothi_part_no'])->hydrate(false)->find('list', ['valueField' => 'nothi_part_no'])->toArray();

		$limit = 50;
		$page = $this->request->query['page'];
		if ($this->request->query['data_for'] == 'receiver') {
			$potrojariList = $receiverTable->find()->join([
				"Potrojari" => [
					'table' => 'potrojari', 'type' => 'inner',
					'conditions' => ['PotrojariReceiver.potrojari_id =Potrojari.id']
				]
			])->where(['PotrojariReceiver.is_sent' => 0, 'Potrojari.nothi_part_no IN' => $allCurrentNothi, 'Potrojari.potro_status <>' => 'Draft']);
			$potrojariList = $this->Paginator->paginate($potrojariList, ['limit' => $limit]);
		} elseif ($this->request->query['data_for'] == 'onulipi') {
			$potrojariList = $onulipiTable->find()->join([
				"Potrojari" => [
					'table' => 'potrojari', 'type' => 'inner',
					'conditions' => ['PotrojariOnulipi.potrojari_id =Potrojari.id']
				]
			])->where(['PotrojariOnulipi.is_sent' => 0, 'Potrojari.nothi_part_no IN' => $allCurrentNothi, 'Potrojari.potro_status <>' => 'Draft']);
			$potrojariList = $this->Paginator->paginate($potrojariList, ['limit' => $limit]);
		}

		cache::delete('total_potrojari_pending_'.$employee_office['office_unit_organogram_id'],'memcached');
		$this->set(compact('potrojariList', 'office_id', 'page', 'limit'));
		$this->layout = null;
		$this->view = 'pending_potrojari_get';

    }

    public function updatePotrojariStatus()
    {
        if (!empty($this->request->data)) {
            try {
                $nothi_office = intval($this->request->data['nothi_office']);
                $receiverTable = $this->request->data['type'];
                $id = intval($this->request->data['id']);
                $status = intval($this->request->data['status']);
                $error = ($this->request->data['error']);
                if (!empty($nothi_office) && !empty($id) && !empty($receiverTable)) {
                    $this->switchOffice($nothi_office, 'MainNothiOffice');

                    TableRegistry::remove($receiverTable);
                    $table = TableRegistry::get($receiverTable);
                    $data = $table->get($id);
                    $data->potro_status = $status == 1 ? 'Sent' : 'Not Sent';
                    $data->is_sent = $status == 1 ? 1 : 0;
                    $data->task_reposponse = $status == 1 ? '' : $error;
                    if($table->save($data)){
                        echo "done";
                    }
                    //send SMS
                    if ($data->is_sent == 1 && $data->potro_status == 'Sent' && !empty($data->officer_mobile) && !empty($data->sms_message)) {
                        // can be multiple receiver
                        $recSMS = explode('--', $data->officer_mobile);
                        if(!empty($recSMS)){
                            foreach ($recSMS as $receiverSMS){
                                $this->SendSmsMessage($receiverSMS, $data->sms_message);
                            }
                        }
                    }
                    //send SMS
                }
            } catch (\Exception $ex) {
                echo $ex->getMessage();
            }
        }
        die;
    }

    public function printPreview($id, $nothi_office = 0, $nothi_part_no = 0){

        $employee_office = $this->getCurrentDakSection();
        if ($nothi_office != 0 && $nothi_office != $employee_office['office_id']) {
            $this->switchOffice($nothi_office, 'MainNothiOffice');
        } else {
            $nothi_office = $employee_office['office_id'];
        }

        $this->set(compact("id",'nothi_part_no','nothi_office'));
    }

    public function getPdfById($id,$nothi_office = 0,$signHide = true,$folder_path=''){
        $this->view = 'potrojari_preview_print';
        $this->layout = 'single';
        $employee_office = $this->getCurrentDakSection();
        if ($nothi_office != 0 && $nothi_office != $employee_office['office_id']) {
            $this->switchOffice($nothi_office, 'MainNothiOffice');
        } else {
            $nothi_office = $employee_office['office_id'];
        }

        $potrojariTable = TableRegistry::get('Potrojari');
        $potrojariData = $potrojariTable->get($id);

        // for cs only active tab data will show
        if($potrojariData->potro_type == 30){
            if(isset($this->request->query['show'])){
                if($this->request->query['show'] == 'cover'){
                    $pdfbody = (!empty($potrojariData->attached_potro)?('<div class="showimageforce">'. html_entity_decode($potrojariData->attached_potro) . '</div><br/>'):'') . (!empty($potrojariData->potro_cover)?($potrojariData->potro_cover . "<br/>"):'');
                }else{
                    $pdfbody = $potrojariData->potro_description;
                }
            }else{
                $pdfbody = (!empty($potrojariData->attached_potro)?('<div class="showimageforce">'. html_entity_decode($potrojariData->attached_potro) . '</div><br/>'):'') . (!empty($potrojariData->potro_cover)?($potrojariData->potro_cover . "<br/>"):'').$potrojariData->potro_description;
            }

        }else{
            $pdfbody = (!empty($potrojariData->attached_potro)?('<div class="showimageforce">'. html_entity_decode($potrojariData->attached_potro) . '</div><br/>'):'') . (!empty($potrojariData->potro_cover)?($potrojariData->potro_cover . "<br/>"):'').$potrojariData->potro_description;
        }

        $isCloned = false;
        if(empty($this->request->data['meta'])) {
            $meta = !empty($potrojariData->meta_data) ? json_decode($potrojariData->meta_data, true) : [];
            if (isset($meta['margin_top'])) {
                $this->request->data['margin_top'] = $meta['margin_top'];
            }
            if (isset($meta['margin_right'])) {
                $this->request->data['margin_right'] = $meta['margin_right'];
            }
            if (isset($meta['margin_bottom'])) {
                $this->request->data['margin_bottom'] = $meta['margin_bottom'];
            }
            if (isset($meta['margin_left'])) {
                $this->request->data['margin_left'] = $meta['margin_left'];
            }
            if (!empty($meta['orientation'])) {
                $this->request->data['orientation'] = $meta['orientation'];
            }
            if (isset($meta['potro_header']) && $meta['potro_header'] == 0 && !empty($meta['potro_header_banner'])) {
                $this->request->data['potro_header_banner'] = $meta['potro_header_banner'];
            }
            if (isset($meta['potro_header']) && $meta['potro_header'] == 0 && !empty($meta['banner_position'])) {
                $this->request->data['banner_position'] = $meta['banner_position'];
            }
            if (isset($meta['potro_header']) && $meta['potro_header'] == 0 && !empty($meta['banner_width'])) {
                $this->request->data['banner_width'] = $meta['banner_width'];
            }

        }
        if($potrojariData->potro_type == 20 || $potrojariData->potro_type == 27 || $potrojariData->potro_type == 21){
            $regex = '/<span class="[a-z]* potro_security"(.+?)>(.+?)<\/span>/s';
            $pdfbody = preg_replace( $regex, '', $pdfbody);
            $security_level = json_decode(DAK_SECRECY_TYPE,true);
            $this->request->data['pdf_footer_title'] = (!empty($potrojariData->potro_security_level) && !empty($security_level[$potrojariData->potro_security_level]))?$security_level[$potrojariData->potro_security_level]:'';
            $this->request->data['pdf_header_title'] = (!empty($potrojariData->potro_security_level) && !empty($security_level[$potrojariData->potro_security_level]))?$security_level[$potrojariData->potro_security_level]:'';
            $this->request->data['show_pdf_page_number'] = 1;
        }
        if ($potrojariData->cloned_potrojari_id) {
            $isCloned = true;
        }
        return $this->createPdf("potrojari_" . $id . '_' . $nothi_office,$pdfbody,$signHide,$folder_path, $isCloned);
    }


    public function getPdfByPotro($id,$nothi_office = 0,$signHide = false){
        $this->view = 'potrojari_preview_print';
        $this->layout = 'single';
        $employee_office = $this->getCurrentDakSection();
        if ($nothi_office != 0 && $nothi_office != $employee_office['office_id']) {
            $this->switchOffice($nothi_office, 'MainNothiOffice');
        } else {
            $nothi_office = $employee_office['office_id'];
        }
        $isCloned = false;
        $potroTable = TableRegistry::get('NothiPotroAttachments');
        $potroData = $potroTable->get($id);

        $pdfbody =  (!empty($potroData->potro_cover)?($potroData->potro_cover . "<br/>"):'').$potroData->content_body;

        if(empty($this->request->data['meta'])) {
            $meta = !empty($potroData['meta_data']) ? json_decode($potroData['meta_data'], true) : [];

            if (isset($meta['margin_top'])) {
                $this->request->data['margin_top'] = $meta['margin_top'];
            }
            if (isset($meta['margin_right'])) {
                $this->request->data['margin_right'] = $meta['margin_right'];
            }
            if (isset($meta['margin_bottom'])) {
                $this->request->data['margin_bottom'] = $meta['margin_bottom'];
            }
            if (isset($meta['margin_left'])) {
                $this->request->data['margin_left'] = $meta['margin_left'];
            }
            if (!empty($meta['orientation'])) {
                $this->request->data['orientation'] = $meta['orientation'];
            }
            if (isset($meta['potro_header']) && $meta['potro_header'] == 0 && !empty($meta['potro_header_banner'])) {
                $this->request->data['potro_header_banner'] = $meta['potro_header_banner'];
            }
            if (isset($meta['potro_header']) && $meta['potro_header'] == 0 && !empty($meta['banner_position'])) {
                $this->request->data['banner_position'] = $meta['banner_position'];
            }
            if (isset($meta['potro_header']) && $meta['potro_header'] == 0 && !empty($meta['banner_width'])) {
                $this->request->data['banner_width'] = $meta['banner_width'];
            }
        }
        $potrojariData = TableRegistry::get('Potrojari')->get($potroData['potrojari_id']);

        if(!empty($potrojariData)) {
            if($potrojariData->potro_type == 20 || $potrojariData->potro_type == 27 || $potrojariData->potro_type == 21){
                $regex = '/<span class="[a-z]* potro_security"(.+?)>(.+?)<\/span>/s';
                $pdfbody = preg_replace( $regex, '', $pdfbody);
                $security_level = json_decode(DAK_SECRECY_TYPE,true);
                $this->request->data['pdf_footer_title'] = (!empty($potrojariData->potro_security_level) && !empty($security_level[$potrojariData->potro_security_level]))?$security_level[$potrojariData->potro_security_level]:'';
                $this->request->data['pdf_header_title'] = (!empty($potrojariData->potro_security_level) && !empty($security_level[$potrojariData->potro_security_level]))?$security_level[$potrojariData->potro_security_level]:'';
                $this->request->data['show_pdf_page_number'] = 1;
            }
            if ($potrojariData['cloned_potrojari_id']) {
                $isCloned = true;
            }
        }
        return $this->createPdf('potro_' . $id . '_' . $nothi_office,$pdfbody, $signHide, '', $isCloned);
    }

    public function getPdfByDak($nothi_office = 0,$id=0,$dak_type = DAK_DAPTORIK,$signHide = false){
        $this->view = 'potrojari_preview_print';
        $this->layout = 'single';
        $employee_office = $this->getCurrentDakSection();
        if ($nothi_office != 0 && $nothi_office != $employee_office['office_id']) {
            $this->switchOffice($nothi_office, 'MainNothiOffice');
        } else {
            $nothi_office = $employee_office['office_id'];
        }

        TableRegistry::remove('DakDaptoriks');
        TableRegistry::remove('DakNagoriks');
        $potroTable = TableRegistry::get('DakDaptoriks');
        if($dak_type!=DAK_DAPTORIK){
            $potroTable = TableRegistry::get('DakNagoriks');
        }

        $potroData = $potroTable->get($id);

        $pdfbody = (isset($potroData->dak_cover)?($potroData->dak_cover . "<br/>"):'').(isset($potroData->dak_description)?$potroData->dak_description:$potroData->description);

        if(empty($this->request->data['meta'])) {
            $meta = !empty($potroData['meta_data']) ? json_decode($potroData['meta_data'], true) : [];

            if (isset($meta['margin_top'])) {
                $this->request->data['margin_top'] = $meta['margin_top'];
            }
            if (isset($meta['margin_right'])) {
                $this->request->data['margin_right'] = $meta['margin_right'];
            }
            if (isset($meta['margin_bottom'])) {
                $this->request->data['margin_bottom'] = $meta['margin_bottom'];
            }
            if (isset($meta['margin_left'])) {
                $this->request->data['margin_left'] = $meta['margin_left'];
            }
            if (!empty($meta['orientation'])) {
                $this->request->data['orientation'] = $meta['orientation'];
            }
            if (isset($meta['potro_header']) && $meta['potro_header'] == 0 && !empty($meta['potro_header_banner'])) {
                $this->request->data['potro_header_banner'] = $meta['potro_header_banner'];
            }
            if (isset($meta['potro_header']) && $meta['potro_header'] == 0 && !empty($meta['banner_position'])) {
                $this->request->data['banner_position'] = $meta['banner_position'];
            }
            if (isset($meta['potro_header']) && $meta['potro_header'] == 0 && !empty($meta['banner_width'])) {
                $this->request->data['banner_width'] = $meta['banner_width'];
            }
        }
        return $this->createPdf('dak_' . $dak_type . '_' . $id . '_' . $nothi_office,$pdfbody, $signHide);
    }

    public function getPdfByBody($signHide = true){

        return $this->createPdf($this->request->data['name'],$this->request->data['body'],$signHide);
    }


    protected function createPdf($filename, $body, $removeSign = true,$folder_path='', $isCloned = false){
        $employee_office = $this->getCurrentDakSection();
        if(!isset($this->request->data['margin_top'])){
            $this->request->data['margin_top'] = 1;
        }if(!isset($this->request->data['margin_right'])){
            $this->request->data['margin_right'] = .75;
        }if(!isset($this->request->data['margin_bottom'])){
            $this->request->data['margin_bottom'] = .75;
        }if(!isset($this->request->data['margin_left'])){
            $this->request->data['margin_left'] = .75;
        }

        $filename = base64_encode($filename);
        if(file_exists(FILE_FOLDER_DIR .$folder_path. $filename .'.pdf')) {
            unlink(FILE_FOLDER_DIR .$folder_path. $filename . '.pdf');
        }

        $response = ['status'=>'error','msg'=>'Cannot build the preview'];

        $optionsMerge = [];
        if(defined("POTROJARI_UPDATE") && POTROJARI_UPDATE){
            if((isset($this->request->data['potro_header']) && ($this->request->data['potro_header']==0)) || isset($this->request->data['pdf_header_title']) ) {

                if(defined("TESTING_SERVER") && TESTING_SERVER){
                    if (!empty($this->request->data['potro_header_banner'])) {
                        $optionsMerge['header-html'] = Router::url(['controller' => 'Potrojari', 'action' => 'potrojariDynamicHeader', $employee_office['office_id'],'?'=>
                            [
                                'imagetoken'=>urlencode($this->request->data['potro_header_banner']),
                                'banner_position'=>!empty($this->request->data['banner_position'])?$this->request->data['banner_position']:'left',
                                'banner_width'=>!empty($this->request->data['banner_width'])?$this->request->data['banner_width']:'auto',
                                'pdf_header_title'=>!empty($this->request->data['pdf_header_title'])?$this->request->data['pdf_header_title']:'---',
                            ],'_ssl'=>(defined("Live") && Live)?true:false], true);
                    }else{
                        $optionsMerge['header-html'] = Router::url(['controller' => 'Potrojari', 'action' => 'potrojariDynamicHeader', $employee_office['office_id'],'?'=>[
                            'banner_position'=>!empty($this->request->data['banner_position'])?$this->request->data['banner_position']:'left',
                            'banner_width'=>!empty($this->request->data['banner_width'])?$this->request->data['banner_width']:'auto',
                            'pdf_header_title'=>!empty($this->request->data['pdf_header_title'])?$this->request->data['pdf_header_title']:'---',
                        ],'_ssl'=>(defined("Live") && Live)?true:false], true);
                    }

                }else{
                    $s = (defined("Live") && Live)?'s':'';
                    if (!empty($this->request->data['potro_header_banner'])) {
                        $optionsMerge['header-html'] = "http://localhost/potrojariDynamicHeader/{$employee_office['office_id']}?imagetoken=".urlencode($this->request->data['potro_header_banner']) . "&banner_position=".(!empty($this->request->data['banner_position'])?$this->request->data['banner_position']:'left')."&banner_width=".(!empty($this->request->data['banner_width'])?$this->request->data['banner_width']:'auto')."&pdf_header_title=".(!empty($this->request->data['pdf_header_title'])?urlencode($this->request->data['pdf_header_title']):'---');
                    }else{
                        $optionsMerge['header-html'] = "http://localhost/potrojariDynamicHeader/{$employee_office['office_id']}?banner_position=".(!empty($this->request->data['banner_position'])?$this->request->data['banner_position']:'left')."&banner_width=".(!empty($this->request->data['banner_width'])?$this->request->data['banner_width']:'auto')."&pdf_header_title=".(!empty($this->request->data['pdf_header_title'])?urlencode($this->request->data['pdf_header_title']):'---');
                    }

                }

                $optionsMerge['javascript-delay'] = 1000;
                $optionsMerge['footer-center'] = '';
            }
        }

        if(defined("TESTING_SERVER") && TESTING_SERVER) {
            $optionsMerge['footer-html'] = Router::url(['controller' => 'Potrojari', 'action' => 'potrojariDynamicFooter', $isCloned,
            '?' => [
                'pdf_footer_title'=>!empty($this->request->data['pdf_footer_title'])?$this->request->data['pdf_footer_title']:'---',
                'show_pdf_page_number'=>(isset($this->request->data['show_pdf_page_number'])?$this->request->data['show_pdf_page_number']:1),
                ]
            ], true);
        }else {
            $s = (defined("Live") && Live)?'s':'';
            $optionsMerge['footer-html'] = "http://localhost/potrojariDynamicFooter/".$isCloned.'?pdf_footer_title='.((!empty($this->request->data['pdf_footer_title']))?urlencode($this->request->data['pdf_footer_title']):'---').'&show_pdf_page_number='.(isset($this->request->data['show_pdf_page_number'])?$this->request->data['show_pdf_page_number']:1);
        }

        $request = new \Cake\Network\Request();
        $options = $optionsMerge + array(
            'no-outline', // option without argument
            'disable-external-links',
            'disable-internal-links',
            'disable-forms',
//            'footer-center' => '[page]',
            'orientation' => !empty($this->request->data['orientation'])?$this->request->data['orientation']:'portrait',
            'margin-top'    => bnToen($this->request->data['margin_top'])*20,
            'margin-right'  => 0,
            'margin-bottom' => bnToen($this->request->data['margin_bottom'])*20,
            'margin-left'   => 0,
            'encoding' => 'UTF-8', // option with argument
            'binary' => ROOT . DS . 'vendor' . DS . 'profburial' . DS . 'wkhtmltopdf-binaries-centos6' . DS . 'bin' . DS . PDFBINARY,
            'user-style-sheet' => WWW_ROOT . DS . 'assets' . DS . 'global' . DS . 'plugins' . DS . 'bootstrap' . DS . 'css' . DS . 'bootstrap.min.css',
        );

        $pdf = new Pdf($options);

        $domain_name = str_replace('content/', '', FILE_FOLDER);
        $domain_name = str_replace('https://', '', $domain_name);
        $domain_name = str_replace('http://', '', $domain_name);
//        $body = str_replace($request->webroot . 'img/', WWW_ROOT . 'img/', $body);
        $body = str_replace( $domain_name. 'img/', WWW_ROOT . 'img/', $body);
        $body = str_replace('lang="AR-SA"', 'lang="BN"', $body);
        $body = str_replace('dir="RTL"', '', $body);
        //$body = preg_replace('/(((width)+\s*[:]+\s*)+&?([\d]+)&?(pt)+)/', 'width: $4%;', $body);

        /*
         * In live  & training server need to replace domain with localhost.
         */
        if(defined("TESTING_SERVER") && !TESTING_SERVER) {
            $pattern = "/(http|https)\:\/\/([a-zA-Z0-9-.]*)\.?nothi\.gov\.bd\/(content|getContent)/";
            $body = preg_replace($pattern,'http://localhost/$3',$body);
//            $pattern = "/(http|https)\:\/\/([a-zA-Z0-9-.]*)\.?nothi\.gov\.bd\/(content|getContent)";
//
//            preg_match_all($pattern,$body,$matches);
//            if(!empty($matches)){
//                foreach ($matches[0] as $match){
//                    $body = str_replace($match,'http://localhost',$body);
//                }
//            }
        }

          if(!Live ){
            $background_image = 'background-image: url("'.WWW_ROOT. 'img/test-background.png'.'");background-repeat: no-repeat;background-position:center;';
        }else{
             $background_image = '';
        }

        $pdf->addPage("<html>
    <head>
        <meta charset='UTF-8' />
        <script src='https://code.jquery.com/jquery-1.11.3.js'></script>
        <link href='".$this->request->webroot."assets/global/plugins/bootstrap/css/bootstrap.min.css' rel='stylesheet' type='text/css'/>
        <link href='".$this->request->webroot."assets/global/plugins/uniform/css/uniform.default.css' rel='stylesheet' type='text/css'/>
        <link href='".$this->request->webroot."assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css' rel='stylesheet' type='text/css'/>
        <style type='text/css'>
            *{
                font-family: Nikosh, SolaimanLipi, 'Open Sans', sans-serif !important;
            }
            body{
                font-size: 12pt;
                padding-left: " . bnToen($this->request->data['margin_left']) . "in;
                padding-right: " . bnToen($this->request->data['margin_right']) . "in;
                {$background_image}
            }
            thead { display: table-row-group}
            tfoot { display: table-row-group}
            tr { page-break-inside: avoid}
            th { min-height: 1.1cm; }
            .col-12,.col-6,.col-3{
                padding-right:15px;
                padding-left:15px;
            }
            .col-12{
                flex: 0 0 100%;
                max-width: 100%;
            }
            .col-6{
                flex: 0 0 50%;
                max-width: 50%;
            }
            .col-3{
                flex: 0 0 25%;
                max-width: 25%;
            }
            .bangladate{
                border-bottom: 1px solid #000;
            }
            #sovapoti_signature img, #sender_signature img, #sender_signature2 img, #sender_signature3 img, #sender_signature4 img, #sender_signature5 img{
                height: 50px;
            }

            #pencil{
                visibility: hidden;
            }
            .text-center{
                text-align: center!important;
            }

            #subject {
                word-break: normal;
                word-wrap: break-word;
            }
            .class2 thead tr th, .class2 tbody tr td {
                border: 1px solid #0c0c0c;
            }

            .hide{
                display:none;
            }

            #upomontri_receiver_signature img,
            #sochib_signature img,
            #second_receiver_signature img,
            #third_receiver_signature img,
            #fourth_receiver_signature img,
            #fifth_receiver_signature img {
                height:50px;
            }
            ".($removeSign? ("
            img[alt=signature] {
                visibility: hidden;
            }
            .showimageforce img{
                visibility: visible!important;
            }

            #sender_signature,#sender_signature2,#sender_signature3,#sovapoti_signature{
                border: 1px solid #000;
                padding:2px;
            }
            #sender_signature img,#sender_signature2 img,#sender_signature3 img,#sovapoti_signature img{
                visibility: hidden;
            }
            .showimageforce #sovapoti_signature, .showimageforce #sender_signature, .showimageforce #sender_signature2, .showimageforce #sender_signature3,
            .showimageforce #sovapoti_signature_date, .showimageforce #sender_signature_date, .showimageforce #sender_signature2_date, .showimageforce #sender_signature3_date {
                visibility: visible!important;
                border: 0px solid #000;
                padding:0px;
            }
           
            "):"")."
        </style>
    </head>
    <body >
    <style  type='text/css'>
        thead { display: table-row-group}
        tfoot { display: table-row-group}
        tr { page-break-inside: avoid}
        th { min-height: 1.1cm; }
        .text-center{
            text-align: center!important;
        }
    
    .editable {
        border: none !important;
        word-break: normal;
        word-wrap:break-word
    }

    #note {
        overflow: hidden;
        word-break: normal;
        word-wrap: break-word;
    }

    .bangladate {
        border-bottom: 1px solid #000 !important;
    }
   
    table {
      table-layout: fixed;
      width: 100%!importan;
      white-space: nowrap;
    }
    table td , table th{
      white-space: pre-wrap;
      /*overflow: hidden;*/
      /*text-overflow: ellipsis;*/
      word-break: normal;
      word-wrap: break-word;
    }
    
    table th{
      word-break: normal;
    }
      
    table tr td
    {
        width:fit-content;
        word-break:normal;
        /*white-space: nowrap;*/
        padding-left:2px;
    }

    table tr td:first-child
    {
        min-width:65px !important;
        padding-left:2px;
    }
    .imei_table_area{
        overflow: hidden!important;
    }
            
    </style>
    <script>
        $(function(){
            $('#sharok_no').parent('div').css({'white-space':'nowrap!important'});
            $('#sharok_no2').parent('div').css('white-space','nowrap!important');
        })
</script>
        {$body}
    </body>
</html>");

        $pdf->ignoreWarnings = true;
        if($pdf->saveAs(FILE_FOLDER_DIR .$folder_path. $filename  .'.pdf')){
            $response = ['status'=>'success','src'=>$this->request->webroot . 'getContent?file=' .$folder_path.  $filename .'.pdf&token='.$this->generateToken(['file'=>$folder_path.  $filename .'.pdf'],['exp'=>time() + 60*300]),'file_name'=>$filename.'.pdf'];
        }else{
            $response = ['status'=>'error','msg'=>$pdf->getError()];
        }
        rtn:
        $this->response->type('application/json');
        $this->response->body(json_encode($response));
        return $this->response;
    }

    public function potrojariClone($potrojari_id,$nothi_master_id = 0,$nothi_part_no = 0,$nothi_office = 0){
       $potrojari_table_instance = TableRegistry::get('Potrojari');
       if(!empty($potrojari_id)) {
           $potrojari_info = $potrojari_table_instance->get($potrojari_id);

           if(!empty($potrojari_info)){
              $employee_office = $this->getCurrentDakSection();

              $nothiPartsTable = TableRegistry::get('NothiParts');
              $potrojariTable = TableRegistry::get('Potrojari');
              $officeUnitsTable = TableRegistry::get('OfficeUnits');
              $employeeOfficeTable = TableRegistry::get('EmployeeOffices');

               $nothiInformation = $nothiPartsTable->get($potrojari_info['nothi_part_no']);
               if($nothi_part_no !=0){
                   $nothiInformation = $nothiPartsTable->get($nothi_part_no);
                   if(!empty($nothiInformation)){
                       $clone_nothi_no= $nothiInformation['nothi_no'];
                   }
                  else {
                      $this->Flash->set('দুঃখিত, আপনার আবেদন গ্রহন করা সম্ভব হচ্ছে না. অনুগ্রহপূর্বক আবার চেষ্টা করুন', [
                          'element' => 'error'
                      ]);
                      $this->redirect($this->referer());
                  }
               } else{

                   if(!empty($nothiInformation)){
                       $clone_nothi_no= $nothiInformation['nothi_no'];
                   }
                   else {
                       $this->Flash->set('দুঃখিত, আপনার আবেদন গ্রহন করা সম্ভব হচ্ছে না. অনুগ্রহপূর্বক আবার চেষ্টা করুন', [
                           'element' => 'error'
                       ]);
                       $this->redirect($this->referer());
                   }
               }
               $ownUnitInfo = $unitInfo = $officeUnitsTable->get($employee_office['office_unit_id']);

               $totalPotrojari = $potrojariTable->find()->where(['office_id' => $employee_office['office_id'],
                   'YEAR(potrojari_date)' => date('Y'), 'potrojari_draft_office_id' => $employee_office['office_id'],
                   'potrojari_draft_unit' => $employee_office['office_unit_id']])->count();

               $totalPotrojari = $ownUnitInfo['sarok_no_start'] + $totalPotrojari;

               $totalPotrojariNew = Number::format(($totalPotrojari + 1),['pattern'=>'######']);

               $clone_sarok_no = ($potrojari_info['potrojari_language']=='eng')? $this->BngToEng($clone_nothi_no.'.'.$totalPotrojariNew) : ($this->EngToBng($clone_nothi_no.'.'.$totalPotrojariNew));

               $old_sarok_no = $potrojari_info['sarok_no'];
               $potrojari_info['potro_description'] = str_replace($old_sarok_no,$clone_sarok_no, $potrojari_info['potro_description']);

               $potrojari_clone =  $potrojari_table_instance->newEntity();
                if(!empty($nothi_part_no)){
                  $lastNoteID= TableRegistry::get('NothiNotes')->getLastNotetId($nothi_part_no,$potrojari_info['office_id'])['id'];
               }

                $potrojari_clone['nothi_master_id'] =                 $nothiInformation['nothi_masters_id'];
                $potrojari_clone['nothi_part_no'] =                     (!empty($nothi_part_no))?$nothi_part_no :$potrojari_info['nothi_part_no'];
                $potrojari_clone['potrojari_draft_office_id'] =         $potrojari_info['potrojari_draft_office_id'];
                $potrojari_clone['potrojari_draft_unit'] =              $potrojari_info['potrojari_draft_unit'];
                $potrojari_clone['nothi_potro_id'] =                     ((!empty($nothi_part_no) && $nothi_part_no == $potrojari_info['nothi_part_no']) || empty($nothi_part_no))?  $potrojari_info['nothi_potro_id'] : 0;
                $potrojari_clone['nothi_notes_id'] =                    ((!empty($nothi_part_no) && $nothi_part_no == $potrojari_info['nothi_part_no']) || empty($nothi_part_no))?  $potrojari_info['nothi_notes_id'] : (!empty($lastNoteID)?$lastNoteID:0);

                $potrojari_clone['potro_type'] =                        $potrojari_info['potro_type'];
                $potrojari_clone['sarok_no'] =                          $clone_sarok_no;

               if($potrojari_info['potro_type'] == 30){
                   $meta_datas = !empty($potrojari_info['meta_data'])?jsonA($potrojari_info['meta_data']):[];
                   if(!empty($meta_datas['has_signature'])){
                       unset($meta_datas['has_signature']);
                   }
                   $potrojari_clone['meta_data'] =                        json_encode($meta_datas);
               }else{
                   $potrojari_clone['meta_data'] =                         $potrojari_info['meta_data'];
               }

                $potrojari_clone['dak_id'] =                            $potrojari_info['dak_id'];
                $potrojari_clone['can_potrojari'] =                     0;
                $potrojari_clone['office_id'] =                         $potrojari_info['office_id'];

                $potrojari_clone['office_name'] =              $potrojari_info['office_name'];

                 // get officer data
                $officer_data =(!empty($potrojari_info['officer_designation_id']))?$employeeOfficeTable->getDesignationDetailsInfo($potrojari_info['officer_designation_id']):[];
                $potrojari_clone['officer_name'] =                        (!empty($officer_data['EmployeeRecords']['name_bng']))?trim($officer_data['EmployeeRecords']['name_bng']):trim($potrojari_info['officer_name']);

               $potrojari_clone['officer_id'] =                        (!empty($officer_data['employee_record_id']))?($officer_data['employee_record_id']):$potrojari_info['officer_id'];//as new user can be assigned in designation

                $potrojari_clone['office_unit_id'] =                    $potrojari_info['office_unit_id'];
                $potrojari_clone['office_unit_name'] =                  $potrojari_info['office_unit_name'];
                $potrojari_clone['officer_designation_id'] =            $potrojari_info['officer_designation_id'];

                $potrojari_clone['officer_designation_label'] =          (!empty($officer_data['designation_label']))?trim($officer_data['designation_label']):trim($potrojari_info['officer_designation_label']);

                //if new user can be in this designation, so previous user visible info is not needed.
               if($potrojari_clone['officer_id'] == $potrojari_info['officer_id']){
//                   $potrojari_clone['officer_visibleName'] =          (!empty($potrojari_info['officer_visibleName']))?trim($potrojari_info['officer_visibleName']):'';
//                   $potrojari_clone['officer_visibleDesignation'] =          (!empty($potrojari_info['officer_visibleDesignation']))?trim($potrojari_info['officer_visibleDesignation']):'';
               }


                $potrojari_clone['approval_office_id'] =                $potrojari_info['approval_office_id'];

                $potrojari_clone['approval_office_unit_id'] =           $potrojari_info['approval_office_unit_id'];
                $potrojari_clone['approval_officer_designation_id'] =   $potrojari_info['approval_officer_designation_id'];
                $potrojari_clone['approval_office_name'] =              $potrojari_info['approval_office_name'];

                // get approval data
                $approval_data =(!empty($potrojari_info['approval_officer_designation_id']))?$employeeOfficeTable->getDesignationDetailsInfo($potrojari_info['approval_officer_designation_id']):[];
                $potrojari_clone['approval_officer_name'] =                        (!empty($approval_data['EmployeeRecords']['name_bng']))?trim($approval_data['EmployeeRecords']['name_bng']):trim($potrojari_info['approval_officer_name']);
               $potrojari_clone['approval_officer_id'] =                (!empty($approval_data['employee_record_id']))?($approval_data['employee_record_id']):$potrojari_info['approval_officer_id'];//as new user can be assigned in designation

                $potrojari_clone['approval_office_unit_name'] =         $potrojari_info['approval_office_unit_name'];

                $potrojari_clone['approval_officer_designation_label'] =(!empty($approval_data['designation_label']))?trim($approval_data['designation_label']):trim($potrojari_info['approval_officer_designation_label']);

               //if new user can be in this designation, so previous user visible info is not needed.
               if($potrojari_clone['approval_officer_id'] == $potrojari_info['approval_officer_id']){
//                   $potrojari_clone['approval_visibleName'] =          (!empty($potrojari_info['approval_visibleName']))?trim($potrojari_info['approval_visibleName']):'';
//                   $potrojari_clone['approval_visibleDesignation'] =          (!empty($potrojari_info['approval_visibleDesignation']))?trim($potrojari_info['approval_visibleDesignation']):'';
               }


                $potrojari_clone['sovapoti_office_id'] =                $potrojari_info['sovapoti_office_id'];

                $potrojari_clone['sovapoti_officer_designation_id'] =   $potrojari_info['sovapoti_officer_designation_id'];

                // get sovapoti data
                $sovapoti_data =(!empty($potrojari_info['sovapoti_officer_designation_id']))?$employeeOfficeTable->getDesignationDetailsInfo($potrojari_info['sovapoti_officer_designation_id']):[];
                $potrojari_clone['sovapoti_officer_designation_label'] =(!empty($sovapoti_data['designation_label']))?trim($sovapoti_data['designation_label']):trim($potrojari_info['sovapoti_officer_designation_label']);
                $potrojari_clone['sovapoti_officer_name'] =              (!empty($sovapoti_data['EmployeeRecords']['name_bng']))?trim($sovapoti_data['EmployeeRecords']['name_bng']):trim($potrojari_info['sovapoti_officer_name']);
               $potrojari_clone['sovapoti_officer_id'] =               (!empty($sovapoti_data['employee_record_id']))?($sovapoti_data['employee_record_id']):($potrojari_info['sovapoti_officer_id']);

                $potrojari_clone['potrojari_date'] =                    $potrojari_info['potrojari_date'];
                $potrojari_clone['potro_subject'] =                     trim($potrojari_info['potro_subject']);
                $potrojari_clone['potro_security_level'] =              $potrojari_info['potro_security_level'];
                $potrojari_clone['potro_priority_level'] =              $potrojari_info['potro_priority_level'];

                if($potrojari_info['potro_type'] == 30){
                    $potrojari_clone['potro_cover'] =                       $potrojari_info['potro_description'];
                    // get initialize cs body html
                    $getcsBody = TableRegistry::get('PotrojariTemplates')->getCsTemplateByIdAndVersion();
                    $potrojari_clone['potro_description'] = isset($getcsBody['cs_body']['html_content'])?$getcsBody['cs_body']['html_content']:'';
                }else{
                    $potrojari_clone['potro_cover'] =                       $potrojari_info['potro_cover'];
                    $potrojari_clone['potro_description'] =                 trim($potrojari_info['potro_description']);
                }

                $potrojari_clone['receiving_office_id'] =               $potrojari_info['receiving_office_id'];
                $potrojari_clone['receiving_office_unit_id'] =          $potrojari_info['receiving_office_unit_id'];
                $potrojari_clone['receiving_office_unit_name'] =        trim($potrojari_info['receiving_office_unit_name']);
                $potrojari_clone['receiving_officer_id'] =              $potrojari_info['receiving_officer_id'];
                $potrojari_clone['receiving_officer_designation_id'] =  $potrojari_info['receiving_officer_designation_id'];
                $potrojari_clone['receiving_officer_designation_label']=trim($potrojari_info['receiving_officer_designation_label']);
                $potrojari_clone['receiving_officer_name'] =            trim($potrojari_info['receiving_officer_name']);
                $potrojari_clone['potro_status'] =                      'Draft';
                $potrojari_clone['receiver_sent'] =                     0;
                $potrojari_clone['onulipi_sent'] =                      0;
                $potrojari_clone['is_summary_nothi'] =                  $potrojari_info['is_summary_nothi'];
                $potrojari_clone['created_by'] =                        $potrojari_info['created_by'];
                $potrojari_clone['modified_by'] =                       $potrojari_info['modified_by'];
                $potrojari_clone['potrojari_internal'] =                       $potrojari_info['potrojari_internal'];
                $potrojari_clone['potrojari_language'] =                       $potrojari_info['potrojari_language'];
                   // attension
                $potrojari_clone->attension_officer_designation_label = (!empty( $potrojari_info['attension_officer_designation_label'])
                    ? trim( $potrojari_info['attension_officer_designation_label'])
                    : 0);
                $potrojari_clone->attension_officer_designation_id = (!empty( $potrojari_info['attension_officer_designation_id'])
                    ? ( $potrojari_info['attension_officer_designation_id'])
                    : 0);
                $potrojari_clone->attension_office_unit_name = (!empty( $potrojari_info['attension_office_unit_name'])
                    ? trim( $potrojari_info['attension_office_unit_name'])
                    : '');
                $potrojari_clone->attension_office_unit_id = (!empty( $potrojari_info['attension_office_unit_id'])
                    ? ( $potrojari_info['attension_office_unit_id'])
                    : 0);
                $potrojari_clone->attension_office_name = (!empty( $potrojari_info['attension_office_name'])
                    ? trim( $potrojari_info['attension_office_name'])
                    : '');
                $potrojari_clone->attension_office_id = (!empty( $potrojari_info['attension_office_id'])
                    ? ( $potrojari_info['attension_office_id'])
                    : 0);
                $potrojari_clone->attension_officer_name = (!empty( $potrojari_info['attension_officer_name'])
                    ? trim( $potrojari_info['attension_officer_name'])
                    : '');
                $potrojari_clone->attension_officer_id = (!empty( $potrojari_info['attension_officer_id'])
                    ? ( $potrojari_info['attension_officer_id'])
                    : 0);

               $potrojari_clone['cloned_potrojari_id'] = $potrojari_id;
               $potrojari_table_instance->save($potrojari_clone);
               $clone_id = $potrojari_clone->id;

               $potrojari_attachment_table_intance = TableRegistry::get('PotrojariAttachments');
               $attachments_info = $potrojari_attachment_table_intance->find()->where(['potrojari_id'=>$potrojari_id])->toArray();
               if(!empty($attachments_info)) {
                   foreach ($attachments_info as $attachment_info) {
                       $attachment_clone = $potrojari_attachment_table_intance->newEntity();

                       $attachment_clone['potrojari_type'] = $attachment_info['potrojari_type'];
                       $attachment_clone['potrojari_id'] = $clone_id;
                       $attachment_clone['attachment_type'] = $attachment_info['attachment_type'];
                       $attachment_clone['attachment_description'] = $attachment_info['attachment_description'];
                       $attachment_clone['content_cover'] = $attachment_info['content_cover'];
                       $attachment_clone['content_body'] = $potrojari_info['potro_description'];
                       $attachment_clone['meta_data'] = $attachment_info['meta_data'];
                       $attachment_clone['is_summary_nothi'] = $attachment_info['is_summary_nothi'];
                       $attachment_clone['is_inline'] = $attachment_info['is_inline'];
                       $attachment_clone['potro_id'] = $attachment_info['potro_id'];
                       $attachment_clone['file_name'] = $attachment_info['file_name'];
                       $attachment_clone['file_dir'] = $attachment_info['file_dir'];
                       $attachment_clone['created_by'] = $attachment_info['created_by'];
                       $attachment_clone['modified_by'] = $attachment_info['modified_by'];
                       $attachment_clone['user_file_name'] = $attachment_info['user_file_name'];
                       $attachment_clone['options'] = $attachment_info['options'];

                       $potrojari_attachment_table_intance->save($attachment_clone);
                   }
               }
               /*
                * Checking Is potrojari Draft is form internal offices
                */
               $potrojari_internal = isset($potrojari_clone['potrojari_internal'])?$potrojari_clone['potrojari_internal']:0;

               $potrojari_receiver_table_instance = TableRegistry::get('PotrojariReceiver');
               $receivers_info = $potrojari_receiver_table_instance->find()->where(['potrojari_id'=>$potrojari_id])->toArray();
               if(!empty($receivers_info)){
                   foreach ($receivers_info as $receiver_info){
                       $receiver_clone = $potrojari_receiver_table_instance->newEntity();

                    $receiver_clone['potrojari_id'] =               $clone_id;
                    $receiver_clone['nothi_master_id'] =           $potrojari_clone['nothi_master_id'];
                    $receiver_clone['nothi_part_no'] =             $potrojari_clone['nothi_part_no'];
                    $receiver_clone['nothi_notes_id'] =               ((!empty($nothi_part_no) && $nothi_part_no == $receiver_info['nothi_part_no']) || empty($nothi_part_no))?  $receiver_info['nothi_notes_id'] : 0;
                    $receiver_clone['nothi_potro_id'] =               ((!empty($nothi_part_no) && $nothi_part_no == $receiver_info['nothi_part_no']) || empty($nothi_part_no))?  $receiver_info['nothi_notes_id'] : 0;
                    $receiver_clone['potro_type'] =                 $receiver_info['potro_type'];

                    $receiver_clone['group_id'] =                   $receiver_info['group_id'];
                    $receiver_clone['group_name'] =                 $receiver_info['group_name'];
                    $receiver_clone['group_member'] =                 $receiver_info['group_member'];
                    $receiver_clone['receiving_office_id'] =        $receiver_info['receiving_office_id'];
                    $receiver_clone['receiving_office_name'] =      $receiver_info['receiving_office_name'];
                    $receiver_clone['receiving_office_unit_id'] =   $receiver_info['receiving_office_unit_id'];
                    $receiver_clone['receiving_office_unit_name'] = $receiver_info['receiving_office_unit_name'];
                    $receiver_clone['receiving_officer_id'] =       $receiver_info['receiving_officer_id'];
                    $receiver_clone['receiving_officer_designation_id'] =$receiver_info['receiving_officer_designation_id'];
                    $receiver_clone['receiving_officer_designation_label'] =$receiver_info['receiving_officer_designation_label'];
                    $receiver_clone['receiving_officer_name'] =     $receiver_info['receiving_officer_name'];
                    $receiver_clone['receiving_officer_email'] =    $receiver_info['receiving_officer_email'];
                    $receiver_clone['receiver_office_head'] =    $receiver_info['receiver_office_head'];
                    $receiver_clone['visibleName'] =    $receiver_info['visibleName'];
                    $receiver_clone['officer_mobile'] =    $receiver_info['officer_mobile'];
                    $receiver_clone['sms_message'] =    $receiver_info['sms_message'];
                    $receiver_clone['options'] =    $receiver_info['options'];
                    $receiver_clone['run_task'] =                   0;
                    $receiver_clone['retry_count'] =                0;
                    $receiver_clone['task_reposponse'] =            '';
                    $receiver_clone['potro_status'] =               'Draft';
                    $receiver_clone['is_sent'] =                    0;
                    $receiver_clone['created_by'] =                 $receiver_info['created_by'];
                    $receiver_clone['modified_by'] =                $receiver_info['modified_by'];
                    $receiver_clone['visibleName'] =                $receiver_info['visibleName'];
                    $receiver_clone['officer_mobile'] =                $receiver_info['officer_mobile'];
                    $receiver_clone['sms_message'] =                $receiver_info['sms_message'];

                    if($potrojari_internal == 0 && $receiver_clone['receiving_officer_designation_id'] != 0){
                            $potrojari_internal = 1;
                        }
                    $potrojari_receiver_table_instance->save($receiver_clone);
                   }
               }

               $potrojari_onulipi_table_instance = TableRegistry::get('PotrojariOnulipi');
               $onulipi_infos = $potrojari_onulipi_table_instance->find()->where(['potrojari_id'=>$potrojari_id])->toArray();
               if(!empty($onulipi_infos)){
                   foreach ($onulipi_infos as $receiver_info){
                       $receiver_clone = $potrojari_onulipi_table_instance->newEntity();

                       $receiver_info['potro_description'] = $potrojari_info['potro_description'];

                       $receiver_clone['potrojari_id'] =               $clone_id;
                       $receiver_clone['nothi_master_id'] =          $potrojari_clone['nothi_master_id'];
                       $receiver_clone['nothi_part_no'] =             $potrojari_clone['nothi_part_no'];
                       $receiver_clone['nothi_notes_id'] =               ((!empty($nothi_part_no) && $nothi_part_no == $receiver_info['nothi_part_no']) || empty($nothi_part_no))?  $receiver_info['nothi_notes_id'] : 0;
                       $receiver_clone['nothi_potro_id'] =               ((!empty($nothi_part_no) && $nothi_part_no == $receiver_info['nothi_part_no']) || empty($nothi_part_no))?  $receiver_info['nothi_potro_id'] : 0;
                       $receiver_clone['potro_type'] =                 $receiver_info['potro_type'];

                       $receiver_clone['group_id'] =                   $receiver_info['group_id'];
                       $receiver_clone['group_name'] =                 $receiver_info['group_name'];
                       $receiver_clone['group_member'] =                 $receiver_info['group_member'];
                       $receiver_clone['receiving_office_id'] =        $receiver_info['receiving_office_id'];
                       $receiver_clone['receiving_office_name'] =      $receiver_info['receiving_office_name'];
                       $receiver_clone['receiving_office_unit_id'] =   $receiver_info['receiving_office_unit_id'];
                       $receiver_clone['receiving_office_unit_name'] = $receiver_info['receiving_office_unit_name'];
                       $receiver_clone['receiving_officer_id'] =       $receiver_info['receiving_officer_id'];
                       $receiver_clone['receiving_officer_designation_id'] =$receiver_info['receiving_officer_designation_id'];
                       $receiver_clone['receiving_officer_designation_label'] =$receiver_info['receiving_officer_designation_label'];
                       $receiver_clone['receiving_officer_name'] =     $receiver_info['receiving_officer_name'];
                       $receiver_clone['receiving_officer_email'] =    $receiver_info['receiving_officer_email'];
                        $receiver_clone['visibleName'] =    $receiver_info['visibleName'];
                        $receiver_clone['officer_mobile'] =    $receiver_info['officer_mobile'];
                        $receiver_clone['sms_message'] =    $receiver_info['sms_message'];
                        $receiver_clone['options'] =    $receiver_info['options'];
                       $receiver_clone['run_task'] =                   0;
                       $receiver_clone['retry_count'] =                0;
                       $receiver_clone['task_reposponse'] =            '';
                       $receiver_clone['potro_status'] =               'Draft';
                       $receiver_clone['is_sent'] =                    0;
                       $receiver_clone['created_by'] =                 $receiver_info['created_by'];
                       $receiver_clone['modified_by'] =                $receiver_info['modified_by'];
                        $receiver_clone['visibleName'] =                $receiver_info['visibleName'];
                        $receiver_clone['officer_mobile'] =                $receiver_info['officer_mobile'];
                        $receiver_clone['sms_message'] =                $receiver_info['sms_message'];

                       if($potrojari_internal == 0 && $receiver_clone['receiving_officer_designation_id'] != 0){
                            $potrojari_internal = 1;
                        }
                       $potrojari_onulipi_table_instance->save($receiver_clone);
                   }
               }

           }
           else {
           $this->Flash->set('দুঃখিত, আপনার আবেদন গ্রহন করা সম্ভব হচ্ছে না. অনুগ্রহপূর্বক আবার চেষ্টা করুন', [
               'element' => 'error'
           ]); }
       }
       else {
           $this->Flash->set('দুঃখিত, আপনার আবেদন গ্রহন করা সম্ভব হচ্ছে না. অনুগ্রহপূর্বক আবার চেষ্টা করুন', [
               'element' => 'error'
           ]); }

        $this->Flash->set('পত্রজারি ক্লোন সফলভাবে সম্পন্ন হয়েছে।', [
            'element' => 'success'
        ]);
            if(!empty($potrojari_internal) && $potrojari_internal == 1){
                    $potrojari_table_instance->updateAll(['potrojari_internal' => 1], ['id' => $clone_id]);
            }
            if(!empty($nothi_office)){
                if($potrojari_info['potro_type'] == 21){
                     $this->redirect(['_name' =>'editRm',$nothi_part_no,0,'potro',0,$clone_id,$nothi_office]);
                }
                else if($potrojari_info['potro_type'] == 27){
                     $this->redirect(['_name' =>'edit-formal-letter',$nothi_part_no,0,'potro',0,$clone_id,$nothi_office]);
                }
                 $this->redirect(['controller' => 'potrojari' ,'action' => 'potroDraft',$nothi_part_no,$clone_id,'potro',$nothi_office]);

            }
            if(empty($nothi_part_no)){
                 $nothi_part_no =$potrojari_info['nothi_part_no'];
            }
             if($potrojari_info['potro_type'] == 21){
                     $this->redirect(['controller' => 'potrojari' ,'action' => 'editRm',$nothi_part_no,0,'potro',0,$clone_id,$nothi_office]);
            }
            else if($potrojari_info['potro_type'] == 27){
                     $this->redirect(['controller' => 'potrojari' ,'action' => 'editFormalDraft',$nothi_part_no,0,'potro',0,$clone_id,$nothi_office]);
            }
            $this->redirect(['controller' => 'potrojari' ,'action' => 'potroDraft',$nothi_part_no,$clone_id,'potro']);
    }
    public function makeCronRequestforPotrojari(){
        if($this->request->is('ajax')){
            $response = ['status' => 0,'msg' => 'Something Wrong'];
            try{
                $office_id =  isset($this->request->data['office_id'])?$this->request->data['office_id']:0;
                $potrojari_id =  isset($this->request->data['potrojari_id'])?$this->request->data['potrojari_id']:0;
                $param =  isset($this->request->data['param'])?$this->request->data['param']:'Receiver';
                if(empty($office_id)){
                    $response['msg'] = 'Office Id missing';
                    goto rtn;
                }
                if(empty($potrojari_id)){
                    $response['msg'] = 'Potrojari Id missing';
                    goto rtn;
                }
                // Updating retry status
                 $this->switchOffice($office_id, 'MainNothiOffice');
                 if($param == 'Receiver'){
                    TableRegistry::remove('PotrojariReceiver');
                    $receiverTable = TableRegistry::get('PotrojariReceiver');
                    $hasNotsentData = $receiverTable->find()->where(['is_sent' => 0, 'potrojari_id' => $potrojari_id])->count();
                     if ($hasNotsentData > 0) {
                        $receiverTable->updateAll(['run_task' => 0, 'task_reposponse' => '', 'potro_status' => ''], ['is_sent' => 0, 'potrojari_id' => $potrojari_id]);
                    }
                 }
                 else if($param == 'Onulipi'){
                    TableRegistry::remove('PotrojariOnulipi');
                    $onulipiTable = TableRegistry::get('PotrojariOnulipi');
                    $hasNotsentData = $onulipiTable->find()->where(['is_sent' => 0, 'potrojari_id' => $potrojari_id])->count();
                    if ($hasNotsentData > 0) {
                        $onulipiTable->updateAll(['run_task' => 0, 'task_reposponse' => '', 'potro_status' => ''], ['is_sent' => 0, 'potrojari_id' => $potrojari_id]);
                    }
                 }

                  // Updating retry status
                  // Making New Request
                $domain = \Cake\Routing\Router::url(['controller' => "potrojari",
                    'action' => "potrojariPreview", $office_id, $potrojari_id],
                    true);
                $command = ROOT . "/bin/cake cronjob potrojari{$param}Sent {$office_id} {$potrojari_id} '$domain'> /dev/null 2>&1 &";
                exec($command);
                $response = ['status' => 1,'msg' => '<i class="fa fa-check"></i> Request sent','cmd' => makeEncryptedData($command)];
            } catch (\Exception $ex) {
                $response['msg'] = 'Technical error occured';
                $response['reason'] = makeEncryptedData($ex->getMessage());
            }
          rtn:
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
        }
    }

    public function potrojariList(){
        $potrojariTempate = TableRegistry::get('PotrojariTemplates');
        $potrojari = $potrojariTempate->find()->hydrate(false)->where(['status'=>1])->toArray();
        $this->set(compact('potrojari'));
    }

    public function potrojariEdit($id){
        $potrojariTempate = TableRegistry::get('PotrojariTemplates');
        $template = $potrojariTempate->get($id);

        $this->set(compact('template'));
    }


    public function getError($office_id = 0){
        $total = 0;
        try{
            $this->switchOffice($office_id, "Office");
            TableRegistry::remove('PotrojariReceiver');
            $receiverTable = TableRegistry::get('PotrojariReceiver');
            TableRegistry::remove('PotrojariOnulipi');
            $onulipiTable = TableRegistry::get('PotrojariOnulipi');

            $hasNotsentRData = $receiverTable->find()->where(['is_sent' => 0, 'office_id' => $office_id,'potro_status <>'=>'Draft', 'date(created) >=' => date('Y-m-d', strtotime("-2 days"))])->count();
            $hasNotsentOData = $onulipiTable->find()->where(['is_sent' => 0,'office_id' => $office_id,'potro_status <>'=>'Draft', 'date(created) >=' => date('Y-m-d', strtotime("-2 days"))])->count();
            $total = ($hasNotsentRData+$hasNotsentOData);
        }catch(\Exception $ex){

        }

        echo json_encode(['status'=>($total==0?"success":'error'),'count'=>$total]);
        die;
    }

    public function portalPublish(){
        if ($this->request->is('post')) {
//        if ($this->request->is('post') && Live == 1) {
            if(!empty($this->request->data['potrojari_id']) && !empty($this->request->data['archive_date']) && !empty($this->request->data['type'])) {
                $potrojariTable = TableRegistry::get('Potrojari');
                $data = $potrojariTable->get($this->request->data['potrojari_id']);
                if (!empty($data)) {
                    $officesTable = TableRegistry::get('Offices');
                    $employee_office = $this->getCurrentDakSection();
                    $nothi_office = $employee_office['office_id'];

                    $officeinfo = $officesTable->get($nothi_office);
                    $OfficeLayersTable = TableRegistry::get('OfficeLayers');
                    $office_layer_info = $OfficeLayersTable->get($officeinfo['office_layer_id']);
                    $layer_level = $office_layer_info['layer_level'];

                    $pdf_response = $this->getPdfById($this->request->data['potrojari_id'], $nothi_office, 0, 'Portal/');
                    $pdf_response = json_decode($pdf_response, true);
                    $pdf_name = '';
                    if (!empty($pdf_response['status']) && $pdf_response['status'] == 'success') {
                        $pdf_path = explode('content/', $pdf_response['src'], 2);
                        if(empty($pdf_path[1])){
                            $pdf_path = explode('file=', $pdf_response['src'], 2);
                        }
                        $pdf_name = isset($pdf_path[1])?$pdf_path[1]:'';
                        if(!empty($pdf_name)){
                            $pos_of_query = strpos($pdf_name, '?');
                            if($pos_of_query === false) {
                                $pos_of_first_and = strpos($pdf_name, '&');
                                if ($pos_of_first_and !== false) {
                                    $pdf_name = substr_replace($pdf_name, '?', $pos_of_first_and, strlen('?'));
                                }
                            }
                        }

//                        if(!empty($pdf_name)){
//                            if (function_exists('curlRequest')) {
//                                $ch = curl_init(FILE_FOLDER . $pdf_name);
//                                curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
//                                curl_setopt($ch,CURLOPT_TIMEOUT,10);
//                                $output = curl_exec($ch);
//                                $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
//                                curl_close($ch);
//                                if($httpcode != 200){
//                                    $this->Flash->set('দুঃখিত! আপনার অনুরোধটি সম্পন্ন করা সম্ভব হচ্ছে না। অনুগহপুর্বক আবার চেস্টা করুন। কোডঃ FG3', ['element' => 'error']);
//                                    return $this->redirect($this->referer());
//                                }
//                            }
//                        }
//                        $domain_name = str_replace('content/', '', FILE_FOLDER);
//                        $pdf_path = $domain_name .$pdf_response['src'];
//                        $pdf_name = str_replace('//', '/', $pdf_path);
                    } else {
                        $this->Flash->set('দুঃখিত! আপনার অনুরোধটি সম্পন্ন করা সম্ভব হচ্ছে না। অনুগহপুর্বক আবার চেস্টা করুন। কোডঃ FG1', ['element' => 'error']);
                        return $this->redirect($this->referer());
                    }
                    $data['potro_subject'] = $this->request->data['potrojari_subject'];
                    if(empty($data['potro_subject'])){
                        $potrojariTempleteTable = TableRegistry::get('PotrojariTemplates');
                        $potro_type_name = $potrojariTempleteTable->find()->where(['template_id' => $data['potro_type'],'version' => $data['potrojari_language']])->first()['template_name'];
                        $data['potro_subject'] = $potro_type_name.(!empty($data['sarok_no'])?(' - '.$data['sarok_no']):'');
                    }
                    if(empty($pdf_name)){
                        $this->Flash->set('দুঃখিত! আপনার অনুরোধটি সম্পন্ন করা সম্ভব হচ্ছে না। অনুগহপুর্বক আবার চেস্টা করুন। কোডঃ FG2', ['element' => 'error']);
                        return $this->redirect($this->referer());
                    }

                    $portalPublishesTable = TableRegistry::get('PortalPublishes');
                    $new_publish = $portalPublishesTable->newEntity();

                    $this->request->data['domain'] = str_replace('https://', '', $this->request->data['domain']);
                    $this->request->data['domain'] = str_replace('http://', '', $this->request->data['domain']);
                    $new_publish->potrojari_id = $this->request->data['potrojari_id'];
                    $new_publish->publish_type = $this->request->data['type'];
                    $new_publish->nothi_master_id = $data['nothi_master_id'];
                    $new_publish->nothi_office = $nothi_office;
                    $new_publish->nothi_part_id = $data['nothi_part_no'];
                    $new_publish->subject = $data['potro_subject'];
                    $new_publish->description = $this->request->data['description'];
                    $new_publish->domain = $this->request->data['domain'];
                    $new_publish->archive_date = $this->request->data['archive_date'];
                    $new_publish->potrojari_date = $data['potrojari_date'];
                    $new_publish->publish_type_category = !empty($this->request->data['category'])?$this->request->data['category']:'';
                    $new_publish->file_path = FILE_FOLDER. $pdf_name;
                    $new_publish->unique_id = String::uuid();
                    $new_publish->created_by = $this->Auth->user('id');
                    $new_publish->modified_by = $this->Auth->user('id');
                    if($saved_entity = $portalPublishesTable->save($new_publish)) {
                        if($new_publish->publish_type == 'notification_circular'){
                            $attachment_type_1 = 'pdf';
                            $attachment_type_2 = 'doc';
                            $categoty = '';
                        } else if($new_publish->publish_type == 'moedu_office_order'){
                            $categoty = $new_publish->publish_type_category;
                            $attachment_type_1 = 'field_pdf';
                            $attachment_type_2 = 'attachments_2';

                        }
                        else{
                            $attachment_type_1 = 'attachments';
                            $attachment_type_2 = 'attachments_2';
                        }
                        $potrojari_attachment_table = TableRegistry::get('PotrojariAttachments');
                        $attachments = $potrojari_attachment_table->find()->where(['potrojari_id'=>$new_publish->potrojari_id])->toArray();

                        $attachments_array = array();
                        array_push($attachments_array,['link' => $layer_level>3?str_replace('https://','http://',$new_publish->file_path):$new_publish->file_path,'caption_bn'=>'মূল পত্র','caption_en'=>'Main Letter']);
                        foreach ($attachments as $attachment){
                            if(!empty($attachment['file_name'])){
                                array_push($attachments_array, ['link' => ($layer_level>3?str_replace('https://','http://',FILE_FOLDER):FILE_FOLDER).$attachment['file_name'].'?token='.sGenerateToken(['file'=>$attachment['file_name']],['exp'=>time() + 60*300]),'caption_bn'=>!empty($attachment['user_file_name'])?'সংযুক্তি( '.$attachment['user_file_name'].' )':'সংযুক্তি','caption_en'=>!empty($attachment['user_file_name'])?'Attachment( '.$attachment['user_file_name'].' )':'Attachment']);
                            }
                        }

                        if($layer_level>=1 && $layer_level<=3){
                            $post_fields = [
                                'title_bn' => $new_publish->subject,
                                'title_en' => $new_publish->subject,
                                'body_bn' => $new_publish->description,
                                'body_en' => $new_publish->description,
                                'pubdate' => date("Y-m-d"),
                                'publish_date' => date("Y-m-d"),
                                'archivedate' => date("Y-m-d", strtotime($new_publish->archive_date)),
                                $attachment_type_1 => $attachments_array,
                                $attachment_type_2 => $attachments_array
                            ];


                            try {
                                //share with curl
                                $ch = curl_init();
                                curl_setopt($ch, CURLOPT_URL, 'https://'.$new_publish->domain.'/api/v1/content-type/'.$new_publish->publish_type.'?api_key=7NCnGnrYAAjrz1yScbZL');
                                curl_setopt($ch, CURLOPT_POST, true);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_fields));
                                $errno = curl_errno($ch);
                                if(!empty($errno)) {
                                    $error_message = curl_strerror($errno);
                                    $error = $errno.' : '.$error_message;
                                    $this->Flash->set('দুঃখিত! পোর্টালের সাথে যোগাযোগ করা সম্ভব হচ্ছে না। বিবরণ : ' . $error, ['element' => 'error']);
                                    $this->redirect($this->referer());

                                }
                                $result = curl_exec($ch);
                                curl_close($ch);
                                //end
                                $result = json_decode($result,true);

                                if(isset($result['_meta']['status']) && $result['_meta']['status']=='SUCCESS') {
                                    if(!empty($result['records']['id'])){
                                        $portalPublishesTable->updateAll(['portal_returned_id'=>$result['records']['id']],['id'=>$saved_entity->id]);
                                    }
                                    $this->Flash->set('পত্রটি সফলভাবে পোর্টালে প্রকাশিত হয়েছে।', ['element' => 'success']);
                                } else {
                                    $this->Flash->set('দুঃখিত! পোর্টালের সাথে যোগাযোগ করা সম্ভব হচ্ছে না। বিবরণ :SE2', ['element' => 'error']);
                                }
                                $this->redirect($this->referer());

                            } catch (\Exception $ex) {
                                $this->Flash->set('দুঃখিত! পোর্টালের সাথে যোগাযোগ করা সম্ভব হচ্ছে না। বিবরণ : ' . $ex, ['element' => 'error']);
                                $this->redirect($this->referer());
                            }
                        } else {
                            $post_fields = [
                                'api_key' => 'aPmnN3n8Qb',
                                'title_bn' => $new_publish->subject,
                                'title_en' => $new_publish->subject,
                                'body_bn' => $new_publish->description,
                                'body_en' => $new_publish->description,
                                'pubdate' => date("Y-m-d"),
                                'publish_date' => date("Y-m-d"),
                                'content_type' => $new_publish->publish_type,
                                'unique_id' => $new_publish->unique_id,
                                'archive_date' => date("Y-m-d", strtotime($new_publish->archive_date)),
                                $attachment_type_1 => $attachments_array
                            ];

                            try {
                                //share with curl
                                $ch = curl_init();
                                curl_setopt($ch, CURLOPT_URL, ('http://'.$new_publish->domain.'/npfadmin/public/api/CreateContentType'));
                                curl_setopt($ch, CURLOPT_POST, true);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_fields));
                                $errno = curl_errno($ch);
                                if(!empty($errno)) {
                                    $error_message = curl_strerror($errno);
                                    $error = $errno.' : '.$error_message;
                                    $this->Flash->set('দুঃখিত! পোর্টালের সাথে যোগাযোগ করা সম্ভব হচ্ছে না। বিবরণ : ' . $error, ['element' => 'error']);
                                    $this->redirect($this->referer());

                                }
                                $result = curl_exec($ch);
                                curl_close($ch);
                                //end
                                $result = json_decode($result,true);

                                if(isset($result['id']) && !empty($result['id'])) {
                                        $portalPublishesTable->updateAll(['portal_returned_id'=>$result['id']],['id'=>$saved_entity->id]);
                                    $this->Flash->set('পত্রটি সফলভাবে পোর্টালে প্রকাশিত হয়েছে।', ['element' => 'success']);
                                    $this->redirect($this->referer());
                                } else {
                                    $this->Flash->set('দুঃখিত! পোর্টালের সাথে যোগাযোগ করা সম্ভব হচ্ছে না। বিবরণ : ES2' , ['element' => 'error']);
                                    $this->redirect($this->referer());
                                }


                            } catch (\Exception $ex) {
                                $this->Flash->set('দুঃখিত! পোর্টালের সাথে যোগাযোগ করা সম্ভব হচ্ছে না। বিবরণ : ' . $ex, ['element' => 'error']);
                                $this->redirect($this->referer());
                            }
                        }

                    } else {
                        $this->Flash->set('দুঃখিত! আপনার অনুরোধটি সম্পন্ন করা সম্ভব হচ্ছে না। অনুগহপুর্বক আবার চেস্টা করুন। কোডঃ FS2', ['element' => 'error']);
                        $this->redirect($this->referer());
                    }
                }
            } else {
                $this->Flash->set('দুঃখিত! আপনার অনুরোধটি সম্পন্ন করা সম্ভব হচ্ছে না। অনুগহপুর্বক আবার চেস্টা করুন। কোডঃ ER3', ['element' => 'error']);
                $this->redirect($this->referer());
            }
        } else {
            $this->Flash->set('দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না।', ['element' => 'error']);
            $this->redirect($this->referer());
        }
    }

public function apiPotroDraftView($nothi_part_id, $potrojari_id, $type = "potro", $nothi_office){
    $this->layout = 'online_dak';
    $api_key = !empty($this->request->data['api_key']) ? $this->request->data['api_key'] :
        (!empty($this->request->query['api_key']) ? $this->request->query['api_key']:'');
    $jsonArray = array(
        "status" => "error",
        "error_code" => 404,
        "message" => "Invalid Request"
    );
    if (empty($api_key) || $this->checkToken($api_key) == FALSE) {
        echo json_encode($jsonArray);
        die;
    }
    $noteViewData = !empty($this->request->data['noteViewData']) ? $this->request->data['noteViewData'] :'';
    $user_designation = !empty($this->request->data['user_designation']) ? $this->request->data['user_designation'] :0;

    if($api_key != API_KEY){
        //verify api token
        $this->loadComponent('ApiRelated');
        $getApiTokenData = $this->ApiRelated->getMobileApiTokenData($api_key);
        if(!empty($getApiTokenData)){
            if(!in_array($user_designation,$getApiTokenData['designations'])){
                $jsonArray['message'] = 'Unauthorized request';
                echo json_encode($jsonArray);die;
            }
        }
        //verify api token
    }

    $height = !empty($this->request->query['height']) ? $this->request->query['height']: $this->request->data['height'];
    $width = !empty($this->request->query['width']) ? $this->request->query['width']: $this->request->data['width'];

    $this->set('height', $height);
    $this->set('width', $width);
    $this->set(compact('api_key'));
    $this->set(compact('nothi_part_id'));
    $this->set(compact('potrojari_id'));
    $this->set(compact('type'));
    $this->set(compact('nothi_office'));
    $this->set(compact('noteViewData'));
    $this->set(compact('user_designation'));
}

    public function apiPotroDraft($nothimasterid, $potrojari_id, $type = "potro", $nothi_office = 0,$body_update=false)
    {

        $user_designation = !empty($this->request->data['user_designation']) ? $this->request->data['user_designation'] :
            (!empty($this->request->query['user_designation']) ? $this->request->query['user_designation'] : 0);

        $apikey = !empty($this->request->data['api_key']) ? $this->request->data['api_key'] :
            (!empty($this->request->query['api_key']) ? $this->request->query['api_key']:'');
        $this->set(compact('apikey'));

        $jsonArray = array(
            "status" => "error",
            "error_code" => 404,
            "message" => "Invalid Request"
        );
        if (empty($apikey) || $this->checkToken($apikey) == FALSE) {
            echo json_encode($jsonArray);
            die;
        }

        if (empty($user_designation)) {
            $jsonArray['message'] ='user designation missing';
            goto rtn;
        }

        if($apikey != API_KEY){
            //verify api token
            $this->loadComponent('ApiRelated');
            $getApiTokenData = $this->ApiRelated->getMobileApiTokenData($apikey);
            if(!empty($getApiTokenData)){
                if(!in_array($user_designation,$getApiTokenData['designations'])){
                    $jsonArray['message'] = 'Unauthorized request';
                    echo json_encode($jsonArray);die;
                }
                if(!empty($getApiTokenData['device_type'])){
                    $this->set('device_type',$getApiTokenData['device_type']);
                }else{
                    $this->set('device_type','android');
                }
            }
            //verify api token
        }

        if (empty($nothi_office)) {
            $jsonArray['message'] ='nothi office missing';
            goto rtn;
        }

        $this->layout = 'online_dak';

        $employee_office = $this->setCurrentDakSection($user_designation);

        $otherNothi = false;
        if ($nothi_office != 0 && $nothi_office != $employee_office['office_id']) {
            $otherNothi = true;
        } else {
            $nothi_office = $employee_office['office_id'];
        }
        $this->switchOffice($nothi_office, 'MainNothiOffice');

        $height = !empty($this->request->query['height']) ? $this->request->query['height']: $this->request->data['height'];
        $width = !empty($this->request->query['width']) ? $this->request->query['width']: $this->request->data['width'];

        $this->set('height', $height);
        $this->set('width', $width);
        $this->set('nothi_office', $nothi_office);
        $this->set('user_designation', $user_designation);
        $this->set(compact('otherNothi'));
        $noteViewData = !empty($this->request->data['noteViewData'])?$this->request->data['noteViewData']:'';
        if($body_update== true && !empty($noteViewData)){
            $this->set(compact('noteViewData'));
        } else {
            $this->set('noteViewData', '');
        }

        TableRegistry::remove('NothiMasterPermissions');
        TableRegistry::remove('NothiMasterCurrentUsers');
        $nothiMasterPermissionsTable = TableRegistry::get("NothiMasterPermissions");
        $nothiMasterCurrentUsersTable = TableRegistry::get("NothiMasterCurrentUsers");

        $officeUnitsTable = TableRegistry::get('OfficeUnits');
        $potrosTable = TableRegistry::get('NothiPotros');
        $potroInfoTable = TableRegistry::get('NothiPotroAttachments');
        $potrojariTable = TableRegistry::get("Potrojari");
        $nothiPartsTable = TableRegistry::get('NothiParts');
        $table = TableRegistry::get('PotrojariHeaderSettings');
        $heading = $table->findByOfficeId($employee_office['office_id'])->firstOrfail();

        $this->set(compact('heading'));

        $nothiPartInformation = $nothiPartsTable->get($nothimasterid);

        $ownUnitInfo = $unitInfo = $officeUnitsTable->get($employee_office['office_unit_id']);

        $totalPotrojari = $potrojariTable->find()->where(['office_id' => $employee_office['office_id'],
            'YEAR(potrojari_date)' => date('Y'), 'potrojari_draft_office_id' => $employee_office['office_id'],
            'potrojari_draft_unit' => $employee_office['office_unit_id'], 'id <> ' => $potrojari_id])->count();

        $totalPotrojari = $ownUnitInfo['sarok_no_start'] + $totalPotrojari;

        $this->set('totalPotrojari', Number::format(($totalPotrojari + 1)));

        $nothiMasters = array();
        $nothiMastersCurrentUser = array();

        if (!empty($employee_office['office_id'])) {

            $nothiMasters = $nothiMasterPermissionsTable->hasAccess($nothi_office,
                $employee_office['office_id'],
                $employee_office['office_unit_id'],
                $employee_office['office_unit_organogram_id'],
                $nothiPartInformation['nothi_masters_id'],
                $nothiPartInformation['id']);

            $nothiMastersCurrentUser = $nothiMasterCurrentUsersTable->find()->where(['nothi_part_no' => $nothimasterid,
                'office_id' => $employee_office['office_id'], 'office_unit_id' => $employee_office['office_unit_id'],
                'office_unit_organogram_id' => $employee_office['office_unit_organogram_id'],
                'nothi_office' => $nothi_office])->first();

            if (empty($nothiMasters) || $nothiMasters['privilige_type'] == 0) {
                 $jsonArray['message'] ="দুঃখিত! কোনো তথ্য পাওয়া যায়নি";
                goto rtn;
            }

            if (!empty($nothiMastersCurrentUser)) {
                $this->set('privilige_type', $nothiMasters['privilige_type']);
            } else {
                $this->set('privilige_type', 2);
            }
            $template_list = $this->potrojari_template_list($employee_office);

            $nothiMasterPermissionsTable = TableRegistry::get("NothiMasterPermissions");
            $permitted_nothi_list = $nothiMasterPermissionsTable->allNothiPermissionList($nothi_office,
                $employee_office['office_id'],
                $employee_office['office_unit_id'],
                $employee_office['office_unit_organogram_id']);

            $potroAttachmentRecord = $potroInfoTable->find('list',
                ['keyField' => 'id', 'valueField' => [
                    "nothi_potro_page_bn", 'subject'
                ]])
                ->select(['subject' => 'NothiPotros.subject', 'NothiPotroAttachments.id',
                    "nothi_potro_page_bn" => 'NothiPotroAttachments.nothi_potro_page_bn'])
                ->join([
                    'NothiPotros' => [
                        'table' => 'nothi_potros',
                        'type' => 'INNER',
                        'conditions' => 'NothiPotros.id = NothiPotroAttachments.nothi_potro_id'
                    ]
                ])->where(['NothiPotroAttachments.nothi_master_id' => $nothiMasters['nothi_masters_id']])
                ->where(['NothiPotroAttachments.nothijato' => 0, 'NothiPotroAttachments.status' => 1,
                    'NothiPotroAttachments.attachment_type <> ' => 'text'])
                ->where(['NothiPotroAttachments.nothi_part_no IN' => $permitted_nothi_list])
                ->order(['NothiPotroAttachments.nothi_potro_page asc'])->toArray();

            $contentInformation = $potrojariTable->getInfo(['id' => $potrojari_id])->first();

            if (!empty($contentInformation['officer_designation_id'])) {
                if ($contentInformation['can_potrojari'] == 1 && $contentInformation['officer_designation_id']
                    != $employee_office['office_unit_organogram_id']
                ) {
                    $this->set('privilige_type', 2);
                }
            }
            $this->set('potroInfo', array());
            $this->set('potroSubject', "");

            if (!empty($contentInformation['nothi_potro_id']) && $type == 'potro') {
                $potroInfo = $potroInfoTable->get($contentInformation['nothi_potro_id']);
                $this->set('potroInfo', $potroInfo);
                $potroSubject = $potrosTable->get($potroInfo['nothi_potro_id']);
                $this->set('potroSubject', $potroSubject['subject']);
            }
            $this->set('template_list', $template_list);
            $this->set('potrojari_id', $potrojari_id);
            $this->set('office_id', $employee_office['office_id']);
            $this->set('employee_office', $employee_office);
            $token = $this->generateToken([
                'office_id'=> $employee_office['office_id'],
                'office_unit_organogram_id'=>$employee_office['office_unit_organogram_id'],
                'parent_id'=> $potrojari_id,
                'module'=>'Potrojari',
                'module_type'=> 'Nothi',
            ],['exp'=>time() + 3600*3]);
            $this->set('temp_token',$token);

            try {
                $nothiRecord = $nothiPartsTable->getAll(['NothiParts.id' => $nothimasterid])->first();

                $officeUnitTable = TableRegistry::get('OfficeUnits');
                $officeunit = $officeUnitTable->get($nothiRecord['office_units_id']);
                $officeUnitsName = $officeunit['unit_name_bng'];

                $this->set('officeUnitsName', $officeUnitsName);

                $attachments = array();
                if (!empty($contentInformation)) {
                    $contentInformation['dak_subject'] = isset($contentInformation['potro_subject'])
                        ? $contentInformation['potro_subject'] : '';
                    $contentInformation['sender_sarok_no'] = isset($contentInformation['sarok_no'])
                        ? $contentInformation['sarok_no'] : '';
                    $contentInformation['sender_office_id_final'] = isset($contentInformation['office_id'])
                        ? $contentInformation['office_id'] : 0;
                    $contentInformation['sender_officer_designation_id_final'] = isset($contentInformation['officer_designation_id'])
                        ? $contentInformation['officer_designation_id'] : 0;
                    $contentInformation['sender_officer_designation_label_final']
                        = isset($contentInformation['officer_designation_label'])
                        ? $contentInformation['officer_designation_label'] : '';
                    $contentInformation['sender_officer_id_final'] = isset($contentInformation['officer_id'])
                        ? $contentInformation['officer_id'] : 0;
                    $contentInformation['sender_officer_name_final'] = isset($contentInformation['officer_name'])
                        ? $contentInformation['officer_name'] : '';

                    $contentInformation['sovapoti_office_id_final'] = isset($contentInformation['sovapoti_office_id'])
                        ? $contentInformation['sovapoti_office_id'] : 0;
                    $contentInformation['sovapoti_officer_designation_id_final']
                        = isset($contentInformation['sovapoti_officer_designation_id'])
                        ? $contentInformation['sovapoti_officer_designation_id']
                        : 0;
                    $contentInformation['sovapoti_officer_designation_label_final']
                        = isset($contentInformation['sovapoti_officer_designation_label'])
                        ? $contentInformation['sovapoti_officer_designation_label']
                        : '';
                    $contentInformation['sovapoti_officer_id_final'] = isset($contentInformation['sovapoti_officer_id'])
                        ? $contentInformation['sovapoti_officer_id'] : 0;
                    $contentInformation['sovapoti_officer_name_final'] = isset($contentInformation['sovapoti_officer_name'])
                        ? $contentInformation['sovapoti_officer_name'] : '';

                    //approval
                    $contentInformation['approval_office_id_final'] = isset($contentInformation['approval_office_id'])
                        ? $contentInformation['approval_office_id'] : 0;

                    $contentInformation['approval_officer_id_final'] = isset($contentInformation['approval_officer_id'])
                        ? $contentInformation['approval_officer_id'] : 0;

                    $contentInformation['approval_office_unit_id_final'] = isset($contentInformation['approval_office_unit_id'])
                        ? $contentInformation['approval_office_unit_id'] : 0;

                    $contentInformation['approval_officer_designation_id_final']
                        = isset($contentInformation['approval_officer_designation_id'])
                        ? $contentInformation['approval_officer_designation_id']
                        : 0;

                    $contentInformation['approval_officer_designation_label_final']
                        = isset($contentInformation['approval_officer_designation_label'])
                        ? $contentInformation['approval_officer_designation_label']
                        : '';

                    $contentInformation['approval_officer_name_final'] = isset($contentInformation['approval_officer_name'])
                        ? $contentInformation['approval_officer_name'] : '';

                    $contentInformation['approval_office_name_final'] = isset($contentInformation['approval_office_name'])
                        ? $contentInformation['approval_office_name'] : '';

                    $contentInformation['approval_office_unit_name_final'] = isset($contentInformation['approval_office_unit_name'])
                        ? $contentInformation['approval_office_unit_name'] : '';


                    $this->set('draftVersion', $contentInformation);
                    $this->set('nothiMasterInfo', $nothiRecord);
                    $this->set('nothiRecord', $nothiRecord);
                    $this->set('nothimasterid', $nothimasterid);
                    $this->set('referenceid',
                        $contentInformation['nothi_potro_id']);
                    $this->set('referencetype', $type);

                    $potrojari_attachments = TableRegistry::get('PotrojariAttachments');

                    $attachments = $potrojari_attachments->find()->where(['potrojari_id' => $contentInformation->id,
                        'is_inline' => 0])->toArray();

                    $inlineattachments = $potrojari_attachments->find('list',
                        ['keyField' => 'id', 'valueField' => 'potro_id'])->where(['potrojari_id' => $contentInformation->id,
                        'is_inline' => 1])->toArray();
                }

                $this->set('potroAttachmentRecord', $potroAttachmentRecord);
                $this->set('inlineattachments', $inlineattachments);
                $this->set('attachments', $attachments);

                $allOtherDraftsofThisPotro = array();
                if ($contentInformation->nothi_potro_id != 0) {
                    $allOtherDraftsofThisPotro = $potrojariTable->find()->select([
                        'Potrojari.id',
                        'Potrojari.nothi_master_id',
                        'Potrojari.nothi_part_no',
                        'Potrojari.potrojari_date',
                        'Potrojari.potro_subject',
                        'Potrojari.potro_type',
                    ])->where(['nothi_potro_id' => $contentInformation->nothi_potro_id,
                        'potro_status' => 'Draft'])->toArray();

                    if (!empty($allOtherDraftsofThisPotro)) {
                        $potrojariTemplateTable = TableRegistry::get('PotrojariTemplates');
                        foreach ($allOtherDraftsofThisPotro as $key => &$value) {

                            $templatename = $potrojariTemplateTable->get($value['potro_type']);
                            $value['template_name'] = $templatename['template_name'];
                        }
                    }
                }

                $this->set('allOtherDrafts', $allOtherDraftsofThisPotro);
                $time = new Time(date('Y-m-d'));
                $this->set('signature_date_bangla', $time->i18nFormat('d-M-Y', null, 'bn-BD'));

                if ($contentInformation->potro_type == 13) {
                    $tablePotrojariTemplate = TableRegistry::get('PotrojariTemplates');
                    $template = $tablePotrojariTemplate->get($contentInformation->potro_type);
                    $this->set('templates', $template);
                }
            } catch (Exception $ex) {
                  $jsonArray['message'] ="দুঃখিত! কোনো তথ্য পাওয়া যায়নি";
                goto rtn;
            }
             $jsonArray = array(
            "status" => "success",
            "message" => "validation succesful"
        );
        }
         rtn:
             if($jsonArray['status'] == 'error'){
                  $this->response->type('application/json');
                    $this->response->body(json_encode($jsonArray));
                   return $this->response;
             }
    }

    public function apiPotrojariDraftUpdate($id, $nothi_office = 0, $user_designation)
    {
        $this->digitalSigantureUrlChecker();
        $jsonArray = array(
            "status" => "error",
            "error_code" => 404,
            "message" => "Invalid Request"
        );

        if (empty($user_designation)) {
            echo json_encode($jsonArray);
            die;
        }

        if (empty($nothi_office)) {
            echo json_encode($jsonArray);
            die;
        }

        $employee_office = $this->setCurrentDakSection($user_designation);

        $otherNothi = false;
        if ($nothi_office != 0 && $nothi_office != $employee_office['office_id']) {
            $otherNothi = true;
        } else {
            $nothi_office = $employee_office['office_id'];
        }
        $this->switchOffice($nothi_office, 'MainNothiOffice');

        $this->layout = null;

        if ($this->request->is('ajax')) {
            if (!empty($this->request->data)) {

                TableRegistry::remove('Potrojari');
                $tablePotroJari = TableRegistry::get('Potrojari');
                $potrojari = $tablePotroJari->get($id);
                /*
                 * Checking sender/onumodonkari/sovapotir sakkhor
                 *
                 */

                if (!empty($potrojari['officer_id'])) {
                    $usersTable = TableRegistry::get('Users');
                    $users = $usersTable->find()->where(['employee_record_id' => $potrojari['officer_id']])->first();
                    $username = $users['username'];
                    $options['token'] = sGenerateToken(['file'=>$username],['exp'=>time() + 60*300]);

                    $data = $this->getSignature($username, 1, 0, true,null,$options);

                    if ($data == false) {
                        echo json_encode(array('status' => 'error',
                            'msg' => 'দুঃখিত! প্রেরক/অনুমোদনকারী এর স্বাক্ষর পাওয়া যায়নি। অনুগ্রহ করে সিস্টেমে স্বাক্ষর আপলোড করে খসড়া পুনরায় সংরক্ষণ করুন। ধন্যবাদ।'));
                        die;
                    }
                }
                if (!empty($potrojari['approval_officer_id'])) {

                    $users = $usersTable->find()->where(['employee_record_id' => $potrojari['approval_officer_id']])->first();
                    $username = $users['username'];
                    $options['token'] = sGenerateToken(['file'=>$username],['exp'=>time() + 60*300]);

                    $data = $this->getSignature($username, 1, 0, true,null,$options);

                    if ($data == false) {
                        echo json_encode(array('status' => 'error',
                            'msg' => 'দুঃখিত! প্রেরক/অনুমোদনকারী এর স্বাক্ষর পাওয়া যায়নি। অনুগ্রহ করে সিস্টেমে স্বাক্ষর আপলোড করে খসড়া পুনরায় সংরক্ষণ করুন। ধন্যবাদ।'));
                        die;
                    }
                }
                if ($potrojari['sovapoti_officer_id'] > 0) {
                    $users = $usersTable->find()->where(['employee_record_id' => $potrojari['sovapoti_officer_id']])->first();
                    $username = $users['username'];
                    $options['token'] = sGenerateToken(['file'=>$username],['exp'=>time() + 60*300]);

                    $data = $this->getSignature($username, 1, 0, true,null,$options);

                    if ($data == false) {
                        echo json_encode(array('status' => 'error',
                            'msg' => 'দুঃখিত! সভাপতি এর স্বাক্ষর পাওয়া যায়নি। অনুগ্রহ করে সিস্টেমে স্বাক্ষর আপলোড করে খসড়া পুনরায় সংরক্ষণ করুন। ধন্যবাদ।'));
                        die;
                    }
                }

                /*
                 * End
                 */
                $data = $this->updatePotrojari($potrojari, $id, $nothi_office, $employee_office,1);

                if ($data['status']=='success') {
                    echo json_encode(array('status' => 'success'));
                } else {
                    echo json_encode(array('status' => 'error', 'msg' => 'দুঃখিত! খসড়া পত্র সংশোধন করা সম্ভব হচ্ছে না। '.(isset($data['msg'])?$data['msg']:''),'reason'=>''));
                }
            }
        }

        die;
    }
    public function getDesignationListPotrojariHeader($office_id = 0){
      $employeeOfficesTable =  TableRegistry::get('EmployeeOffices');
        if($this->request->is('get')){
            $user = $this->Auth->user();
            if(!empty($user) && $user['user_role_id'] <= 2){
                if(empty($office_id)){
                    $this->set('redirect_url','potrojari/getDesignationListPotrojariHeader/');
                    $this->view = '/OfficeManagement/update_office_info';
                    return;
                }
                $officeid = $office_id;
            }else{
                $employee_info = $this->getCurrentDakSection();
                if(empty($employee_info)){
                    return $this->redirect(['controller' => 'users','action' =>'login']);
                }
                $officeid = $employee_info['office_id'];
            }
            $allOrganogram = $employeeOfficesTable->getEmployeeOfficeRecordsByOfficeId($officeid);
            $this->set(compact('allOrganogram'));
        }else if($this->request->is('post')){
            $jsonArray = ['msg' =>'something wrong','status' => 'error'];
            $data = $this->request->data['data'];
            $show_unit_zero = [];
            $show_unit_one = [];
            if(!empty($data)){
                foreach($data as $date_key => $data_val){
                    if(isset($data_val['desigantion_id']) && isset($data_val['show_unit'])){
                        if($data_val['show_unit'] == 1){
                            $show_unit_one[]=  $data_val['desigantion_id'];
                        }else{
                            $show_unit_zero[]=  $data_val['desigantion_id'];
                        }
                    }
                }
                try{
                    $potrojariGroupUsers =  TableRegistry::get('PotrojariGroupsUsers');
                    if(!empty($show_unit_one)){
                         $employeeOfficesTable->updateAll(['show_unit' =>1],['office_unit_organogram_id IN' => $show_unit_one,'status' => 1]);
                         $potrojariGroupUsers->updateAll(['office_head' =>1],['office_unit_organogram_id IN' => $show_unit_one]);
                    }
                    if(!empty($show_unit_zero)){
                           $employeeOfficesTable->updateAll(['show_unit' =>0],['office_unit_organogram_id IN' => $show_unit_zero,'status' => 1]);
                           $potrojariGroupUsers->updateAll(['office_head' =>0],['office_unit_organogram_id IN' => $show_unit_zero]);
                    }
                    $jsonArray = ['msg' =>'সকল পদবির জন্য  সংরক্ষণ সম্ভব হয়েছে','status' => 'success'];
                } catch (\Exception $ex) {
                    $jsonArray = ['msg' =>'সকল পদবির জন্য  সংরক্ষণ সম্ভব হয়নি','status' => 'error','reason' => $this->makeEncryptedData($ex->getMessage())];
                }
            }
             rtn:
            $this->response->type('application/json');
            $this->response->body(json_encode($jsonArray));
             return $this->response;
        }

    }

    /**
     * @param type $part_id
     * @param type $user
     *  user need to have
     * id - employee Record id, office_id, office_unit_id,office_unit_organogram_id,
     *  designation_label, incharge_label,office_unit_name
     */
    public function setSignatureInOnucched($part_id,$user,$nothi_office = 0){
        try{
        TableRegistry::remove('NothiNotes');
        TableRegistry::remove('NothiNoteSignatures');

        $NothiNotesTable = TableRegistry::get('NothiNotes');
        $nothiNoteSignaturesTable = TableRegistry::get('NothiNoteSignatures');
            /*
            * Time to check digital signature
            */
           if($user['default_sign']>0 && !empty($this->request->data['soft_token'])){
               $allNotesToSign = $NothiNotesTable->find('list',['keyField'=>'id','valueField'=>'note_description'])->where(['note_status' => 'DRAFT','nothi_part_no' => $part_id, 'office_organogram_id' => $user['office_unit_organogram_id']])->toArray();
               $this->loadComponent('DigitalSignatureRelated');
              $res = $this->DigitalSignatureRelated->signOnucched($allNotesToSign,$this->request->data['soft_token'],$part_id,$user['default_sign'],$user,$nothi_office);
              if($res['status'] =='error'){
                   $this->response->body(json_encode(array('status' => 'error',
                       'msg' => 'দুঃখিত! ডিজিটাল সিগনেচার করা সম্ভব হয়নি। কারণঃ '.$res['msg'])));
                   $this->response->type('application/json');
                   return $this->response;
              }else{
                  $note_ids = $res['note_ids'];

              }
           }
       $lastNote = $NothiNotesTable->find()->where(['nothi_part_no' => $part_id, 'note_no >=' => 0])->order(['id DESC'])->first();
         //get last signature of that note for is_signature = 1and cross_signature = 0
            $lastSignatureFor_is_Signature_1 = $nothiNoteSignaturesTable->getLastSignature($user['office_unit_organogram_id'],
                    $part_id, $lastNote['id'])->where(['is_signature' => 1,'cross_signature' =>0])->order(['id desc'])->first();
            // signature already exist no need to add another one.
            if(!empty($lastSignatureFor_is_Signature_1)){
                $nothiNoteSignaturesTable->updateAll(['signature_date'=>date("Y-m-d H:i:s")],['id'=>$lastSignatureFor_is_Signature_1['id']]);
               return false;
            }
          //get last signature for this user where 'is_signature'=>0
        $lastSignature = $nothiNoteSignaturesTable->getByEmployeeId($user['office_unit_organogram_id'],
            $part_id)->first();
          //save own signature
        if (empty($lastSignature)) {
            // No signature = 0 For this onucched. Create a new one.
            $nothiNoteSignatures = $nothiNoteSignaturesTable->newEntity();
            $nothiNoteSignatures->nothi_master_id = $lastNote['nothi_master_id'];
            $nothiNoteSignatures->nothi_part_no = $part_id;
            $nothiNoteSignatures->office_id = $user['office_id'];
            $nothiNoteSignatures->employee_id = $user['officer_id'];
            $nothiNoteSignatures->office_unit_id = $user['office_unit_id'];
            $nothiNoteSignatures->office_organogram_id = $user['office_unit_organogram_id'];
            $nothiNoteSignatures->employee_designation = $user['designation_label'] . (!empty($user['incharge_label'])?(" (" . $user['incharge_label'] . ')'):''). (!empty($user['office_unit_name'])?(", " . $user['office_unit_name']):'');
            $nothiNoteSignatures->can_delete = 1;

            $nothiNoteSignatures->nothi_note_id = !empty($lastNote['id'])
                ? intval($lastNote['id']) : 0;
            $nothiNoteSignatures->note_decision = '';
            $nothiNoteSignatures->cross_signature = 0;
            $nothiNoteSignatures->is_signature = 1;
            $nothiNoteSignatures->signature_date = date("Y-m-d H:i:s");
            $nothiNoteSignatures->created = date("Y-m-d H:i:s");
            $nothiNoteSignatures->created_by = (isset($this->Auth->user))?$this->Auth->user('id'):$user['officer_id'];
            $nothiNoteSignatures->modified_by =  (isset($this->Auth->user))?$this->Auth->user('id'):$user['officer_id'];
            $nothiNoteSignatures->digital_sign =  (isset($user['default_sign']))?$user['default_sign']:0;

            $nothiNoteSignaturesTable->save($nothiNoteSignatures);
        } else {
            // Has a signature =0
            if ($lastSignature['nothi_note_id'] == $lastNote['id']) {
                // Last signature should be from current user
                if ($lastSignature->office_organogram_id == $user['office_unit_organogram_id']) {
                    $lastSignature->office_id = $user['office_id'];
                    $lastSignature->employee_id = $user['officer_id'];
                    $lastSignature->office_unit_id = $user['office_unit_id'];
                    $lastSignature->office_organogram_id = $user['office_unit_organogram_id'];
                    $lastSignature->employee_designation = $user['designation_label'] . (!empty($user['incharge_label'])?(" (" . $user['incharge_label'] . ')'):''). (!empty($user['office_unit_name'])?(", " . $user['office_unit_name']):'');
                    $lastSignature->is_signature = 1;
                    $lastSignature->can_delete = 1;
                    $lastSignature->signature_date = date("Y-m-d H:i:s");
                    $lastSignature->created = date("Y-m-d H:i:s");
                    $lastSignature->created_by = (isset($this->Auth->user))?$this->Auth->user('id'):$user['officer_id'];
                    $lastSignature->modified_by =(isset($this->Auth->user))?$this->Auth->user('id'):$user['officer_id'];
                    $lastSignature->digital_sign =  (isset($user['default_sign']))?$user['default_sign']:0;

                    $nothiNoteSignaturesTable->save($lastSignature);
                }
            } else {
                //User has signature = 0 but not in last onucched
                if ($lastSignature->office_organogram_id == $user['office_unit_organogram_id']) {
                    $lastSignature->office_id = $user['office_id'];
                    $lastSignature->employee_id = $user['officer_id'];
                    $lastSignature->office_unit_id = $user['office_unit_id'];
                    $lastSignature->office_organogram_id = $user['office_unit_organogram_id'];
                    $lastSignature->employee_designation = $user['designation_label'] . (!empty($user['incharge_label'])?(" (" . $user['incharge_label'] . ')'):''). (!empty($user['office_unit_name'])?(", " . $user['office_unit_name']):'');
                    $lastSignature->is_signature = 1;
                    $lastSignature->can_delete = 1;
                    $lastSignature->cross_signature = 1;
                    $lastSignature->signature_date = date("Y-m-d H:i:s");
                    $lastSignature->created = date("Y-m-d H:i:s");
                    $lastSignature->created_by = (isset($this->Auth->user))?$this->Auth->user('id'):$user['officer_id'];
                    $lastSignature->modified_by = (isset($this->Auth->user))?$this->Auth->user('id'):$user['officer_id'];
                    $lastSignature->digital_sign =  (isset($user['default_sign']))?$user['default_sign']:0;

                    $nothiNoteSignaturesTable->save($lastSignature);
                    $nothiNoteSignatures = $nothiNoteSignaturesTable->newEntity();
                    $nothiNoteSignatures->nothi_master_id = $lastNote['nothi_master_id'];
                    $nothiNoteSignatures->nothi_part_no = $part_id;
                    $nothiNoteSignatures->office_id = $user['office_id'];
                    $nothiNoteSignatures->employee_id = $user['officer_id'];
                    $nothiNoteSignatures->office_unit_id = $user['office_unit_id'];
                    $nothiNoteSignatures->office_organogram_id
                        = $user['office_unit_organogram_id'];
                    $nothiNoteSignatures->employee_designation
                        = $user['designation_label'] . (!empty($user['incharge_label'])?(" (" . $user['incharge_label'] . ')'):''). (!empty($user['office_unit_name'])?(", " . $user['office_unit_name']):'');

                    $nothiNoteSignatures->nothi_note_id = !empty($lastNote['id'])
                        ? intval($lastNote['id']) : 0;
                    $nothiNoteSignatures->note_decision = '';
                    $nothiNoteSignatures->cross_signature = 0;
                    $nothiNoteSignatures->is_signature = 1;
                    $nothiNoteSignatures->can_delete = 1;
                    $nothiNoteSignatures->signature_date = date("Y-m-d H:i:s");
                    $nothiNoteSignatures->created = date("Y-m-d H:i:s");
                    $nothiNoteSignatures->created_by = (isset($this->Auth->user))?$this->Auth->user('id'):$user['officer_id'];
                    $nothiNoteSignatures->modified_by = (isset($this->Auth->user))?$this->Auth->user('id'):$user['officer_id'];
                    $nothiNoteSignatures->digital_sign =  (isset($user['default_sign']))?$user['default_sign']:0;

                    $nothiNoteSignaturesTable->save($nothiNoteSignatures);
                }
            }
        }
            // only for digital signature -- as signature put in final step so update of signature should be done after that
            if(!empty($note_ids)){
                $nothiNoteSignaturesTable->updateAll(['digital_sign' => 1], ['digital_sign' => 0,'nothi_note_id IN' => $note_ids,'office_organogram_id' => $user['office_unit_organogram_id'],'cross_signature' => 0,'nothi_part_no' => $part_id]);
            }
            // only for digital signature
        //                            }
        //end
         return  ['status' => 'success' ];
        } catch (\Exception $ex) {
            return  ['status' => 'error','msg' => 'Technical error occured' ,'reason' => $this->makeEncryptedData($ex->getMessage())];
        }
    }

    public function apiSendSummaryDraftPotrojari($nothiMasterId,$nothi_office, $referenceId)
    {
        $this->layout = null;
        $user_designation = $this->request->data['user_designation'];
        $jsonArray = array(
            "status" => "error",
            "error_code" => 404,
            "message" => "Invalid Request"
        );

        if (empty($user_designation)) {
            echo json_encode($jsonArray);
            die;
        }

        $apikey = isset($this->request->data['api_key']) ? $this->request->data['api_key'] : '';
        if (empty($apikey) || $this->checkToken($apikey) == FALSE) {
            echo json_encode($jsonArray);
            die;
        }

        $this->switchOffice($nothi_office, 'MainNothiOffice');
        $employee_office = $this->setCurrentDakSection($user_designation);

        if ($this->request->is('post', 'ajax', 'put')) {

            if (empty($nothiMasterId)) {
                echo json_encode(array("status" => 'error', 'msg' => __("দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না")));
                die;
            }

            $potrojariTable = TableRegistry::get("Potrojari");

            $potrojari = $potrojariTable->find()->where(['nothi_part_no' => $nothiMasterId, 'id' => $referenceId, 'potro_status' => 'SummaryDraft'])->first();

            if (!empty($potrojari)) {
                $status = $this->sendDraftCronPotro($potrojari, $nothiMasterId,true, $employee_office);
            } else {
                echo json_encode(array("status" => 'error', 'msg' => __('সার-সংক্ষেপ জারি করা সম্ভব হচ্ছেনা')));
            }
        }

        die;
    }
    //potrojari
    public function customPotroEdit($nothimasterid, $potrojari_id, $type = "potro", $nothi_office = 0)
    {

            $employee_office = $this->getCurrentDakSection();

            $otherNothi = false;
            if ($nothi_office != 0 && $nothi_office != $employee_office['office_id']) {
                $otherNothi = true;

                $this->switchOffice($nothi_office, 'MainNothiOffice');
            }
            else {
                $nothi_office = $employee_office['office_id'];
            }


            $this->set('nothi_office', $nothi_office);
            $this->set(compact('otherNothi'));


            TableRegistry::remove('NothiMasterPermissions');
            TableRegistry::remove('NothiMasterCurrentUsers');
            $nothiMasterPermissionsTable  = TableRegistry::get("NothiMasterPermissions");
            $nothiMasterCurrentUsersTable = TableRegistry::get("NothiMasterCurrentUsers");

            $officeUnitsTable = TableRegistry::get('OfficeUnits');
            $potrosTable      = TableRegistry::get('NothiPotros');
            $potroInfoTable   = TableRegistry::get('NothiPotroAttachments');
            $potrojariTable   = TableRegistry::get("Potrojari");
            $nothiPartsTable  = TableRegistry::get('NothiParts');

            $nothiPartInformation = $nothiPartsTable->get($nothimasterid);
            $this->set(compact('nothiPartInformation'));

            $ownUnitInfo = $unitInfo    = $officeUnitsTable->get($employee_office['office_unit_id']);

            $nothiMasters            = array();
            $nothiMastersCurrentUser = array();

            if (!empty($employee_office['office_id'])) {

                $nothiMasters = $nothiMasterPermissionsTable->hasAccess($nothi_office,
                    $employee_office['office_id'], $employee_office['office_unit_id'],
                    $employee_office['office_unit_organogram_id'],
                    $nothiPartInformation['nothi_masters_id'], $nothiPartInformation['id']);

                $nothiMastersCurrentUser = $nothiMasterCurrentUsersTable->find()->where(['nothi_part_no' => $nothimasterid,
                        'office_id' => $employee_office['office_id'], 'office_unit_id' => $employee_office['office_unit_id'],
                        'office_unit_organogram_id' => $employee_office['office_unit_organogram_id'],
                        'nothi_office' => $nothi_office])->first();

                if (empty($nothiMasters) || $nothiMasters['privilige_type'] == 0) {
                    $this->Flash->error("দুঃখিত! কোনো তথ্য পাওয়া যায়নি");
                    $this->redirect(['controller' => 'Dashboard', 'action' => 'dashboard']);
                }

                if (!empty($nothiMastersCurrentUser)) {
                    $this->set('privilige_type', 1);
                }
                else {
                    $this->set('privilige_type', 2);
                }
                $template_list = $this->potrojari_template_list();


                $contentInformation = $potrojariTable->get($potrojari_id);

                if (!empty($contentInformation['officer_designation_id'])) {
                    if ($contentInformation['can_potrojari'] == 1 && $contentInformation['approval_officer_designation_id']
                        != $employee_office['office_unit_organogram_id']
                    ) {
                        $this->set('privilige_type', 2);
                    }else{
                        if ($this->request->is('post')) {
                            $response = ['status' => 'error' , 'msg' => ' পত্রজারির তারিখ সংশোধন করা সম্ভব হয়নি। '];
                            $body = $this->request->data['body'];
                            $sharok_no = $this->request->data['sharok_no'];
                            if(!empty($body) ){
                               $status = $this->updateBody($potrojari_id, $body, $sharok_no);
                               if($status == true){
                                     $response = ['status' => 'success' , 'msg' => 'পত্রজারির তারিখ সংশোধন হয়েছে '];
                               }
                            }
                            $this->response->type('Application/json');
                            $this->response->body(json_encode($response));
                            return $this->response;
                        }

                    }
                }
                $this->set(compact('contentInformation'));
            }

    }

    private function updateBody($potrojari_id, $body, $sharok_no = null)
    {

        if (empty($body)) {
            return false;
        }
        if (empty($potrojari_id)) {
            return false;
        }
        try{
            TableRegistry::remove("Potrojari");
            $potrojariTable = TableRegistry::get("Potrojari");
            TableRegistry::remove('PotrojariAttachments');
            $potrojariAttachmentsTable = TableRegistry::get("PotrojariAttachments");

            $potrojari = $potrojariTable->get($potrojari_id);
            if ($sharok_no != null || $sharok_no != '') {
                $potrojari->sarok_no = $sharok_no;
            }
            if($potrojari->potro_type == 30){
                $potrojari->potro_cover = $body;
                $potrojariTable->save($potrojari);
                $potrojari->attached_potro = trim($potrojari->attached_potro);
                $potrojariAttachmentsTable->updateAll(['content_body' => ((!empty($potrojari->attached_potro)?($potrojari->attached_potro . '<br/>'):'') . $body)], ['potrojari_id' => $potrojari_id,'is_inline' => 0,'attachment_type' => 'text']);
            }else{
                $potrojari->potro_description = $body;
                $potrojariTable->save($potrojari);
                $potrojari->attached_potro = trim($potrojari->attached_potro);
                $potrojariAttachmentsTable->updateAll(['content_body' => ((!empty($potrojari->attached_potro)?($potrojari->attached_potro . '<br/>'):'') . $body)], ['potrojari_id' => $potrojari_id,'is_inline' => 0,'attachment_type' => 'text']);
            }

            return true;
        } catch (\Exception $ex) {
            return false;
        }

    }

    public function onucchedPdfForPrint($nothi_office){

        $employee_office = $this->getCurrentDakSection();
        if(!isset($this->request->data['margin_top'])){
            $this->request->data['margin_top'] = 1;
        }if(!isset($this->request->data['margin_right'])){
            $this->request->data['margin_right'] = .75;
        }if(!isset($this->request->data['margin_bottom'])){
            $this->request->data['margin_bottom'] = .75;
        }if(!isset($this->request->data['margin_left'])){
            $this->request->data['margin_left'] = .75;
        }

        $filename = $employee_office['office_unit_organogram_id']  .'_note';

        if(file_exists(FILE_FOLDER_DIR . $filename .'.pdf')) {
            unlink(FILE_FOLDER_DIR . $filename . '.pdf');
        }

        if(defined("TESTING_SERVER") && TESTING_SERVER) {
            $url = Router::url(['_name'=>'nothiAdvancePrint',$employee_office['office_unit_organogram_id'],$nothi_office,$this->request->data['nothi_master_id'],$this->request->data['note_start'],$this->request->data['note_end']],true);
        }else{
            $url = "http://localhost/nothiAdvancePrint/{$employee_office['office_unit_organogram_id']}/{$nothi_office}/{$this->request->data['nothi_master_id']}/{$this->request->data['note_start']}/{$this->request->data['note_end']}";
        }
        $optionsMerge = [];

        if(defined("POTROJARI_UPDATE") && POTROJARI_UPDATE){
            if($this->request->data['potro_header']==0) {
                if(defined("TESTING_SERVER") && TESTING_SERVER) {
                    if (!empty($this->request->data['potro_header_banner'])) {
                        $optionsMerge['javascript-delay'] = 1000;
                        $optionsMerge['header-html'] = Router::url(['controller' => 'Potrojari', 'action' => 'potrojariDynamicHeader', $nothi_office, '?' => ['imagetoken' => urlencode($this->request->data['potro_header_banner'])], '_ssl' => (defined("Live") && Live) ? true : false], true);
                    }else{
                        $optionsMerge['javascript-delay'] = 1000;
                        $optionsMerge['header-html'] = Router::url(['controller' => 'Potrojari', 'action' => 'potrojariDynamicHeader', $nothi_office, '_ssl' => (defined("Live") && Live) ? true : false], true);
                    }
                }else{
                    $s = (defined("Live") && Live)?'s':'';
                    if (!empty($margin['potro_header_banner'])) {
                        $optionsMerge['javascript-delay'] = 1000;
                        $optionsMerge['header-html'] = "http://localhost/potrojariDynamicHeader/{$nothi_office}?imagetoken=".urlencode($this->request->data['potro_header_banner']);
                    }else{
                        $optionsMerge['javascript-delay'] = 1000;
                        $optionsMerge['header-html'] = "http://localhost/potrojariDynamicHeader/{$nothi_office}";
                    }
                }
                $optionsMerge['footer-center'] = '';
            }
        }

        if(defined("TESTING_SERVER") && TESTING_SERVER) {
            $optionsMerge['footer-html'] = Router::url(['controller' => 'Potrojari', 'action' => 'potrojariDynamicFooter'], true);
        }else {
            $s = (defined("Live") && Live)?'s':'';
            $optionsMerge['footer-html'] = "http://localhost/potrojariDynamicFooter";
        }
        $optionsMerge['footer-center'] = '';

        $options = $optionsMerge + array(
            'no-outline', // option without argument
            'disable-external-links',
            'disable-internal-links',
            'disable-forms',
            'footer-center' => '[page]',
            'orientation' => !empty($this->request->data['orientation'])?$this->request->data['orientation']:'portrait',
            'margin-top'    => bnToen($this->request->data['margin_top'])*20,
            'margin-right'  => 0,
            'margin-bottom' => bnToen($this->request->data['margin_bottom'])*20,
            'margin-left'   => 0,
            'encoding' => 'UTF-8', // option with argument
            'binary' => ROOT . DS . 'vendor' . DS . 'profburial' . DS . 'wkhtmltopdf-binaries-centos6' . DS . 'bin' . DS . PDFBINARY,
            'user-style-sheet' => WWW_ROOT . DS . 'assets' . DS . 'global' . DS . 'plugins' . DS . 'bootstrap' . DS . 'css' . DS . 'bootstrap.min.css',
        );

        $pdf = new Pdf($options);

        $pdf->addPage($url . '?margin-left='.(bnToen($this->request->data['margin_left'])).'&margin-right='. (bnToen($this->request->data['margin_right'])));

        $pdf->ignoreWarnings = true;
        if($pdf->saveAs(FILE_FOLDER_DIR . $filename  .'.pdf')){
            $response = ['status'=>'success','filename'=>$this->request->webroot . 'getContent?file=' .  $filename . '.pdf&token='.$this->generateToken(['file'=>$filename.'.pdf'],['exp'=>time() + 60*300])];
        }else{
            $response = ['status'=>'error','msg'=>$pdf->getError()];
        }

        $this->response->type('application/json');
        $this->response->body(json_encode($response));
        return $this->response;
    }

    public function downloadPotro($nothi_office = 0, $id = 0)
    {
        $employee_office = $this->getCurrentDakSection();
        if ($nothi_office != 0) {
            $this->switchOffice($nothi_office, 'MainNothiOffice');
        } else {
            $nothi_office = $employee_office['office_id'];
        }
        TableRegistry::remove('PotrojariAttachments');
        $potroAttachmentTable = TableRegistry::get('PotrojariAttachments');

        $potroAttachmentRecord = $potroAttachmentTable->find()
            ->where(['id' => $id,'attachment_type <>'=>'text'])
            ->first();

        if (!empty($potroAttachmentRecord)) {
            TableRegistry::remove('NothiMasterPermissions');
            $nothiMasterPermissionTable = TableRegistry::get('NothiMasterPermissions');
            $potrojari_table = TableRegistry::get('Potrojari');
            $potrojari_info = $potrojari_table->get($potroAttachmentRecord['potrojari_id']);

            $isAllowed = $nothiMasterPermissionTable->hasAccess($nothi_office, $employee_office['office_id'], $employee_office['office_unit_id'], $employee_office['office_unit_organogram_id'], $potrojari_info['nothi_master_id'], $potrojari_info['nothi_part_no']);
            $file = new \Cake\Filesystem\File(FILE_FOLDER_DIR.$potroAttachmentRecord['file_name']);
            if(!empty($isAllowed && $file->exists())){
                    $name = explode("/", $potroAttachmentRecord['file_name']);
                    $this->response->file(FILE_FOLDER_DIR.$potroAttachmentRecord['file_name'],['name'=> end($name),'download'=> end($name)]);
            }else{
                $this->Flash->error('ফাইল পাওয়া যায়নি');
                return $this->redirect($this->referer());
            }
        } else {
            $this->Flash->error('ফাইল পাওয়া যায়নি');
            return $this->redirect($this->referer());
        }

        return $this->response;
    }

    public function setPotrojariDynamicHeader()
    {
        $employee_office = $this->getCurrentDakSection();
        $table = TableRegistry::get('PotrojariHeaderSettings');
        try {
            $heading = $table->findByOfficeId($employee_office['office_id'])->firstOrfail();
        } catch (\Exception $ex) {
            $heading = $table->newEntity();
        }

        if (!empty($this->request->data)) {
            try {
                $destination = FILE_FOLDER_DIR . 'Notice/header_' . $employee_office['office_id'] . time() . '.png';

                if($this->request->data['head_logo']['size']>1040000){
                    throw new \Exception("ফাইলের সাইজ সর্বোচ্চ ১মেগাবাইট হবে");
                }
                if(!empty($this->request->data['head_logo']['tmp_name']) && empty($this->request->data['head_logo']['error']) && in_array($this->request->data['head_logo']['type'],['image/jpeg','image/jpg','image/png'])){
                    if(move_uploaded_file($this->request->data['head_logo']['tmp_name'],$destination)){
                        $this->resizeFilesHeight($destination, 60, 680);
                        $heading->office_id = $employee_office['office_id'];
                        $heading->office_unit_id = $employee_office['office_unit_id'];
                        $heading->office_unit_organogram_id = $employee_office['office_unit_organogram_id'];
                        $heading->employee_record_id = $employee_office['officer_id'];

                        $heading->header_photo = 'Notice/header_' . $employee_office['office_id'] . time() . '.png';

                        if(empty($heading)){
                            $heading->potrojari_head = json_encode([
                                'remove_header_logo' => !empty($this->request->data['remove_header_logo']) ? 1: 0,
                                'remove_header_left_slogan' => !empty($this->request->data['remove_header_left_slogan']) ? 1: 0,
                                'remove_header_right_slogan' => !empty($this->request->data['remove_header_right_slogan']) ? 1: 0,
                                'remove_header_head_1' => !empty($this->request->data['remove_header_head_1']) ? 1: 0,
                                'remove_header_head_2' => !empty($this->request->data['remove_header_head_2']) ? 1: 0,
                                'remove_header_head_3' => !empty($this->request->data['remove_header_head_3']) ? 1: 0,
                                'remove_header_unit' => !empty($this->request->data['remove_header_unit']) ? 1: 0,
                                'remove_header_head_4' => !empty($this->request->data['remove_header_head_4']) ? 1: 0,
                                'remove_header_head_5' => !empty($this->request->data['remove_header_head_5']) ? 1: 0,

                                'banner_position' => !empty($this->request->data['banner_position']) ? $this->request->data['banner_position']: 'left',
                                'banner_width' => !empty($this->request->data['banner_width']) ? $this->request->data['banner_width']: 'auto',

                                'head_title' => !empty($this->request->data['head_title']) ? $this->request->data['head_title'] : '...',
                                'head_ministry' => !empty($this->request->data['head_ministry']) ? $this->request->data['head_ministry'] : '...',
                                'head_office' => !empty($this->request->data['head_office']) ? $this->request->data['head_office'] : '...',
                                'head_unit' => !empty($this->request->data['head_unit']) ? $this->request->data['head_unit'] : '...',
                                'head_other' => !empty($this->request->data['head_other']) ? $this->request->data['head_other'] : '...',
                                'head_logo_type' => !empty($this->request->data['head_logo_type']) ? $this->request->data['head_logo_type'] : '',
//                                'head_logo' => !empty($this->request->data['head_logo_input']) ? $this->request->data['head_logo_input'] : '',
                                'head_text' => !empty($this->request->data['head_text']) ? $this->request->data['head_text'] : '',
                                'head_left_type' => !empty($this->request->data['head_left_type']) ? $this->request->data['head_left_type'] : '',
                                'head_left_slogan' => !empty($this->request->data['head_left_slogan']) ? addcslashes($this->request->data['head_left_slogan'], '"') : '',
                                'head_right_type' => !empty($this->request->data['head_right_type']) ? addcslashes($this->request->data['head_right_type'],'"') : '',
                                'head_right_slogan' => !empty($this->request->data['head_right_slogan']) ? addcslashes($this->request->data['head_right_slogan'], '"') : '',
                                'head_office_address' => !empty($this->request->data['head_office_address']) ? addcslashes($this->request->data['head_office_address'], '"') : '...'
                            ]);


                            $heading->potrojari_head_eng = json_encode([
                                'remove_header_logo' => !empty($this->request->data['remove_header_logo']) ? 1: 0,
                                'remove_header_left_slogan' => !empty($this->request->data['remove_header_left_slogan']) ? 1: 0,
                                'remove_header_right_slogan' => !empty($this->request->data['remove_header_right_slogan']) ? 1: 0,
                                'remove_header_head_1' => !empty($this->request->data['remove_header_head_1']) ? 1: 0,
                                'remove_header_head_2' => !empty($this->request->data['remove_header_head_2']) ? 1: 0,
                                'remove_header_head_3' => !empty($this->request->data['remove_header_head_3']) ? 1: 0,
                                'remove_header_unit' => !empty($this->request->data['remove_header_unit']) ? 1: 0,
                                'remove_header_head_4' => !empty($this->request->data['remove_header_head_4']) ? 1: 0,
                                'remove_header_head_5' => !empty($this->request->data['remove_header_head_5']) ? 1: 0,

                                'banner_position' => !empty($this->request->data['banner_position']) ? $this->request->data['banner_position']: 'left',
                                'banner_width' => !empty($this->request->data['banner_width']) ? $this->request->data['banner_width']: 'auto',

                                'head_title' => !empty($this->request->data['head_title_eng']) ? $this->request->data['head_title_eng'] : '...',
                                'head_ministry' => !empty($this->request->data['head_ministry_eng']) ? $this->request->data['head_ministry_eng'] : '...',
                                'head_office' => !empty($this->request->data['head_office_eng']) ? $this->request->data['head_office_eng'] : '...',
                                'head_unit' => !empty($this->request->data['head_unit_eng']) ? $this->request->data['head_unit_eng'] : '...',
                                'head_other' => !empty($this->request->data['head_other_eng']) ? $this->request->data['head_other_eng'] : '...',
                                'head_logo_type' => !empty($this->request->data['head_logo_type']) ? $this->request->data['head_logo_type'] : '',
                                'head_text' => !empty($this->request->data['head_text_eng']) ? $this->request->data['head_text_eng'] : '',
                                'head_left_type' => !empty($this->request->data['head_left_type']) ? $this->request->data['head_left_type'] : '',
                                'head_left_slogan' => !empty($this->request->data['head_left_slogan_eng']) ? addcslashes($this->request->data['head_left_slogan_eng'], '"') : '',
                                'head_right_type' => !empty($this->request->data['head_right_type']) ? addcslashes($this->request->data['head_right_type'],'"') : '',
                                'head_right_slogan' => !empty($this->request->data['head_right_slogan_eng']) ? addcslashes($this->request->data['head_right_slogan_eng'], '"') : '',
                                'head_office_address' => !empty($this->request->data['head_office_address_eng']) ? addcslashes($this->request->data['head_office_address_eng'], '"') : '...'
                            ]);
                            $heading->potrojari_head_img = json_encode([
                                'head_logo' => !empty($this->request->data['head_logo_input']) ? $this->request->data['head_logo_input'] : '',
                                'head_left_logo' => !empty($this->request->data['head_left_logo_input']) ? $this->request->data['head_left_logo_input'] : '',
                                'head_right_logo' => !empty($this->request->data['head_right_logo_input']) ? addcslashes($this->request->data['head_right_logo_input'],'"') : ''
                            ]);
                        }
                        else{
                            $json = jsonA($heading->potrojari_head);
                            $json['remove_header_logo'] = !empty($this->request->data['remove_header_logo']) ? 1 : 0;
                            $json['remove_header_left_slogan'] = !empty($this->request->data['remove_header_left_slogan']) ?1 : 0;
                            $json['remove_header_right_slogan'] = !empty($this->request->data['remove_header_right_slogan']) ?1 : 0;
                            $json['remove_header_head_1'] = !empty($this->request->data['remove_header_head_1']) ?1 : 0;
                            $json['remove_header_head_2'] = !empty($this->request->data['remove_header_head_2']) ?1 : 0;
                            $json['remove_header_head_3'] = !empty($this->request->data['remove_header_head_3']) ?1 : 0;
                            $json['remove_header_unit'] = !empty($this->request->data['remove_header_unit']) ?1 : 0;
                            $json['remove_header_head_4'] = !empty($this->request->data['remove_header_head_4']) ?1 : 0;
                            $json['remove_header_head_5'] = !empty($this->request->data['remove_header_head_5']) ?1 : 0;

                            $json['banner_position'] =  !empty($this->request->data['banner_position']) ? $this->request->data['banner_position']: 'left';
                            $json['banner_width'] = !empty($this->request->data['banner_width']) ? $this->request->data['banner_width']: 'auto';

                            $heading->potrojari_head = json_encode($json);
                        }
                        $table->save($heading);

                        $this->Flash->success("পত্রজারি হেডার ব্যানার পরিবর্তন করা হয়েছে");
                    }else{
                        throw new \Exception("File cannot be uploaded");
                    }
                }else if(empty($this->request->data['head_logo']['tmp_name'])){

                    if(!empty($this->request->data['remove_image'])) {
                        $heading->header_photo = null;
                        $this->Flash->success("পত্রজারি হেডার ব্যানার মুছে ফেলা হয়েছে");
                    }else{
                        $this->Flash->success("পত্রজারি হেডার পরিবর্তন করা হয়েছে");
                    }
                    $json = [];
                    if(!empty($heading->potrojari_head)){
                        $json = jsonA($heading->potrojari_head);
                    }

                    $json['remove_header_logo'] = !empty($this->request->data['remove_header_logo']) ? 1 : 0;
                    $json['remove_header_left_slogan'] = !empty($this->request->data['remove_header_left_slogan']) ?1 : 0;
                    $json['remove_header_right_slogan'] = !empty($this->request->data['remove_header_right_slogan']) ?1 : 0;
                    $json['remove_header_head_1'] = !empty($this->request->data['remove_header_head_1']) ?1 : 0;
                    $json['remove_header_head_2'] = !empty($this->request->data['remove_header_head_2']) ?1 : 0;
                    $json['remove_header_head_3'] = !empty($this->request->data['remove_header_head_3']) ?1 : 0;
                    $json['remove_header_unit'] = !empty($this->request->data['remove_header_unit']) ?1 : 0;
                    $json['remove_header_head_4'] = !empty($this->request->data['remove_header_head_4']) ?1 : 0;
                    $json['remove_header_head_5'] = !empty($this->request->data['remove_header_head_5']) ?1 : 0;

                    $json['banner_position'] =  !empty($this->request->data['banner_position']) ? $this->request->data['banner_position']: 'left';
                    $json['banner_width'] = !empty($this->request->data['banner_width']) ? $this->request->data['banner_width']: 'auto';

                    $heading->potrojari_head = json_encode($json);

                    $table->save($heading);

                }
                else{
                    throw new \Exception("File cannot be uploaded");
                }
            } catch (\Exception $ex) {
                $this->Flash->error($ex->getMessage());
            }
        }

        $this->redirect(['action'=>'setPotrojariHeader']);
    }


    public function potrojariDynamicHeader($office_id = null, $getHeader = false){
        $this->layout = '';
        $employee_office = $this->getCurrentDakSection();
        $table = TableRegistry::get('PotrojariHeaderSettings');

        $heading = [];
        if(!empty($this->request->query['imagetoken'])){
            $encrypted = urldecode($this->request->query['imagetoken']);
        }
        if(!empty($this->request->query['banner_position'])){
            $heading['banner_position'] = $this->request->query['banner_position'];
        }
        if(!empty($this->request->query['banner_width'])){
            $heading['banner_width'] = $this->request->query['banner_width'];
        }
        if(!empty($encrypted)){
            $decrypted = base64_decode($encrypted);
            $heading['header_photo'] = $decrypted;
        }else {
            if (!empty($employee_office)) {
                $headingData = $table->findByOfficeId($employee_office['office_id'])->first();
            } else {
                $headingData = $table->findByOfficeId($office_id)->first();
            }

            $heading = jsonA($headingData['potrojari_head']);
            $heading['header_photo'] = $headingData['header_photo'];
        }


        $header = '';
        if(!empty($heading['header_photo'])){
            if($getHeader){
                return $heading['header_photo'];
            }
            $header = $heading['header_photo'];
            $path = '';
            if(defined("TESTING_SERVER") && TESTING_SERVER) {
                $path = $this->request->webroot;
            }else {
                $s = (defined("Live") && Live)?'s':'';
                $path = "http://localhost/";
            }
            $this->set('header', $path . 'getContent/?file='.$header.'&token=' . sGenerateToken([
                'file'=>$header
                ],['exp'=>time() + 60*300]));
        }else {
            $header = null;
            $this->set('header', null);
        }
        $title  = '';
        $style= !empty($header)?'padding-top:62px!important;text-align:center':'text-align:center';
        if(!empty($this->request->query['pdf_header_title']) && $this->request->query['pdf_header_title'] != '---'){
            // check if url encoded
            if(strpos($this->request->query['pdf_header_title'],'%') !== false){
                $this->request->query['pdf_header_title'] = urldecode($this->request->query['pdf_header_title']);
            }
            $title = '<p style="'.$style.'">' . $this->request->query['pdf_header_title'] . '</p>';
        }
        else if(!empty($this->request->data['pdf_header_title']) && $this->request->data['pdf_header_title'] != '---'){
            // check if url encoded
            if(strpos($this->request->data['pdf_header_title'],'%') !== false){
                $this->request->data['pdf_header_title'] = urldecode($this->request->data['pdf_header_title']);
            }
            $title = '<p style="'.$style.'">' . $this->request->data['pdf_header_title'] . '</p>';
        }
        $this->set('title',$title);
        $this->set('heading',$heading);
    }

    public function potrojariDynamicFooter($isCloned = false){
        $this->layout = '';

        $title  = '';
        if(!empty($this->request->query['title'])){
            // check if url encoded
            if(strpos($this->request->query['title'],'%') !== false){
                $this->request->query['title'] = urldecode($this->request->query['title']);
            }
            $title = '<p style="margin-bottom: -5px!important;">' . $this->request->query['title'] . '</p>';
        }
        if(!empty($this->request->query['pdf_footer_title']) && $this->request->query['pdf_footer_title'] != '---'){
            // check if url encoded
            if(strpos($this->request->query['pdf_header_title'],'%') !== false){
                $this->request->query['pdf_header_title'] = urldecode($this->request->query['pdf_header_title']);
            }
            $title = '<p style="margin-bottom: -5px!important;">' . $this->request->query['pdf_footer_title'] . '</p>';
        }
        else if(!empty($this->request->data['pdf_footer_title']) && $this->request->data['pdf_footer_title'] != '---'){
            // check if url encoded
            if(strpos($this->request->data['pdf_header_title'],'%') !== false){
                $this->request->data['pdf_header_title'] = urldecode($this->request->data['pdf_header_title']);
            }
            $title = '<p style="margin-bottom: -5px!important;">' . $this->request->data['pdf_footer_title'] . '</p>';
        }
        $show_pdf_page_number  = 1;
        if(isset($this->request->query['show_pdf_page_number'])){
            $show_pdf_page_number = $this->request->query['show_pdf_page_number'];
        }
        else if(isset($this->request->data['show_pdf_page_number'])){
            $show_pdf_page_number = $this->request->data['show_pdf_page_number'];
        }
        $this->set(compact('show_pdf_page_number'));
        $this->set('isCloned',$isCloned);
        $this->set('title',$title);
    }
    public function convertImageFromPDF() {
        $file_name = $this->request->data['file_name'];
        $image_return = getImageFromPdf($file_name);
        $this->response->body(json_encode($image_return));
        $this->response->type('application/json');
        return $this->response;
    }
    public function makeFormalDraft($nothimasterid, $potroid=0, $type = "potro",$attached_id=0,$potrojari_id = 0,$nothi_office = 0){
        $this->view = 'make_rm_draft';
        $employee_office = $this->getCurrentDakSection();

        $otherNothi = false;
        if ($nothi_office != 0 && $nothi_office != $employee_office['office_id']) {
            $otherNothi = true;

            $this->switchOffice($nothi_office, 'MainNothiOffice');
        } else {
            $nothi_office = $employee_office['office_id'];
        }


        $this->set('nothi_office', $nothi_office);
        $this->set('nothiMasterId', $nothimasterid);//actully it is nothi_part_id
        $this->set(compact( 'potroid', 'type','attached_id'));
        $this->set(compact('otherNothi'));

        $guardFileCategories_table = TableRegistry::get("GuardFileCategories");
        $guarfilesubjects = $guardFileCategories_table->getTypes(0, $employee_office);
        $this->set(compact('guarfilesubjects'));

//        TableRegistry::remove('NothiMasterPermissions');
//        TableRegistry::remove('NothiMasterCurrentUsers');
        $nothiMasterPermissionsTable = TableRegistry::get("NothiMasterPermissions");
        $nothiMasterCurrentUsersTable = TableRegistry::get("NothiMasterCurrentUsers");

        $officeUnitsTable = TableRegistry::get('OfficeUnits');
        $potrosTable = TableRegistry::get('NothiPotros');
        $potroInfoTable = TableRegistry::get('NothiPotroAttachments');
        $potrojariTable = TableRegistry::get("Potrojari");
        $nothiPartsTable = TableRegistry::get('NothiParts');
        $potrojariTemplatesTable = TableRegistry::get('PotrojariTemplates');
        $table = TableRegistry::get('PotrojariHeaderSettings');
        try {
            $heading = $table->findByOfficeId($employee_office['office_id'])->firstOrfail();
        } catch (\Exception $ex) {
            $heading = $table->newEntity();
            $heading->office_id = $employee_office['office_id'];
            $heading->office_unit_id = $employee_office['office_unit_id'];
            $heading->office_unit_organogram_id = $employee_office['office_unit_organogram_id'];
            $heading->employee_record_id = $employee_office['officer_id'];
            $heading->potrojari_head = json_encode([
                'head_title' => 'গণপ্রজাতন্ত্রী বাংলাদেশ সরকার ',
                'head_ministry' => $employee_office['ministry_records'],
                'head_office' => $employee_office['office_name'],
                'head_unit' => '...',
                'head_other' => $employee_office['office_web']
            ]);
            $heading->potrojari_head_eng = json_encode([
                'head_title' => "Government of the People\'s Republic of Bangladesh",
                //                'head_title' => "Government of the Peoples Republic of Bangladesh",
                'head_ministry' => $employee_office['ministry_name_eng'],
                'head_office' => $employee_office['office_name_eng'],
                'head_unit' => '...',
                'head_other' => $employee_office['office_web']
            ]);

            $table->save($heading);
        }
        $this->set(compact('heading'));

        $nothiPartInformation = $nothiPartsTable->get($nothimasterid);

        $ownUnitInfo = $unitInfo = $officeUnitsTable->get($employee_office['office_unit_id']);

        if(!empty($potrojari_id)){
            $totalPotrojari = $potrojariTable->find()->where(['office_id' => $employee_office['office_id'],
                                                              'YEAR(potrojari_date)' => date('Y'), 'potrojari_draft_office_id' => $employee_office['office_id'],
                                                              'potrojari_draft_unit' => $employee_office['office_unit_id'], 'id <> ' => $potrojari_id])->count();
        }else{
            $totalPotrojari = $potrojariTable->find()->where(['office_id' => $employee_office['office_id'],
                                                              'YEAR(potrojari_date)' => date('Y'), 'potrojari_draft_office_id' => $employee_office['office_id'],
                                                              'potrojari_draft_unit' => $employee_office['office_unit_id']])->count();
        }


        $totalPotrojari = $ownUnitInfo['sarok_no_start'] + $totalPotrojari;

        $this->set('totalPotrojari', Number::format(($totalPotrojari + 1)));

        $nothiMasters = array();
        $nothiMastersCurrentUser = array();

        if (!empty($employee_office['office_id'])) {

            $nothiMasters = $nothiMasterPermissionsTable->hasAccess($nothi_office,
                $employee_office['office_id'],
                $employee_office['office_unit_id'],
                $employee_office['office_unit_organogram_id'],
                $nothiPartInformation['nothi_masters_id'],
                $nothiPartInformation['id']);

            $nothiMastersCurrentUser = $nothiMasterCurrentUsersTable->find()->where(['nothi_part_no' => $nothimasterid,
                                                                                     'office_id' => $employee_office['office_id'], 'office_unit_id' => $employee_office['office_unit_id'],
                                                                                     'office_unit_organogram_id' => $employee_office['office_unit_organogram_id'],
                                                                                     'nothi_office' => $nothi_office])->first();

            if (empty($nothiMasters) || $nothiMasters['privilige_type'] == 0) {
                $this->Flash->error("দুঃখিত! কোনো তথ্য পাওয়া যায়নি");
                $this->redirect(['controller' => 'Dashboard', 'action' => 'dashboard']);
            }

            if (!empty($nothiMastersCurrentUser)) {
                $this->set('privilige_type', $nothiMasters['privilige_type']);
            } else {
                $this->set('privilige_type', 2);
            }
//            $template_list = $this->potrojari_template_list();
            $template_list = [];

            $nothiMasterPermissionsTable = TableRegistry::get("NothiMasterPermissions");
            $permitted_nothi_list = $nothiMasterPermissionsTable->allNothiPermissionList($nothi_office,
                $employee_office['office_id'],
                $employee_office['office_unit_id'],
                $employee_office['office_unit_organogram_id']);

            $potroAttachmentRecord = $potroInfoTable->find('list',
                ['keyField' => 'id', 'valueField' => [
                    "nothi_potro_page_bn", 'subject'
                ]])
                                                    ->select(['subject' => 'NothiPotros.subject', 'NothiPotroAttachments.id',
                                                              "nothi_potro_page_bn" => 'NothiPotroAttachments.nothi_potro_page_bn'])
                                                    ->join([
                                                        'NothiPotros' => [
                                                            'table' => 'nothi_potros',
                                                            'type' => 'INNER',
                                                            'conditions' => 'NothiPotros.id = NothiPotroAttachments.nothi_potro_id'
                                                        ]
                                                    ])->where(['NothiPotroAttachments.nothi_master_id' => $nothiMasters['nothi_masters_id']])
                                                    ->where(['NothiPotroAttachments.nothijato' => 0, 'NothiPotroAttachments.status' => 1,
                                                    ])
                                                    ->where(['NothiPotroAttachments.nothi_part_no IN' => $permitted_nothi_list])
                                                    ->order(['NothiPotroAttachments.nothi_potro_page asc'])->toArray();
//             Potrojari body should be visible as attachment
//            'NothiPotroAttachments.attachment_type <> ' => 'text' -- ommited

            if(!empty($potrojari_id)) {
                $contentInformation = $potrojariTable->getInfo(['id' => $potrojari_id])->first();
                if(!empty($contentInformation) && $contentInformation['potro_status'] == 'Sent'){
                    $this->Flash->error("দুঃখিত! কোনো তথ্য পাওয়া যায়নি");
                    $this->redirect('/nothiDetail/'.$contentInformation['nothi_part_no']);
                }
                $this->set('is_new_potrojari',0);
            } else {
                $contentInformation = $potrojariTable->newEntity();
                $formal_letter = $potrojariTemplatesTable->getTemplateByIdAndVersion(27,'bn');//formal letter
                $contentInformation->potro_description = $formal_letter['html_content'];
                $this->set('is_new_potrojari',1);

//                pr($routine_letter); die;
            }
            $this->set('potro_type',27);

            if (!empty($contentInformation['officer_designation_id'])) {
                if ($contentInformation['can_potrojari'] == 1 && $contentInformation['officer_designation_id']
                    != $employee_office['office_unit_organogram_id']
                ) {
                    $this->set('privilige_type', 2);
                }
            }
            $this->set('potroInfo', array());
            $this->set('potroSubject', "");

            if (!empty($contentInformation['nothi_potro_id']) && $type == 'potro') {
                $potroInfo = $potroInfoTable->get($contentInformation['nothi_potro_id']);
                $this->set('potroInfo', $potroInfo);
                $potroSubject = $potrosTable->get($potroInfo['nothi_potro_id']);
                $this->set('potroSubject', $potroSubject['subject']);
            }
            $this->set('template_list', $template_list);
            $this->set('office_id', $employee_office['office_id']);
            $this->set('employee_office', $employee_office);

            try {
                $nothiRecord = $nothiPartsTable->getAll(['NothiParts.id' => $nothimasterid])->first();

                $officeUnitTable = TableRegistry::get('OfficeUnits');
                $officeunit = $officeUnitTable->get($nothiRecord['office_units_id']);
                $officeUnitsName = $officeunit['unit_name_bng'];

                $this->set('officeUnitsName', $officeUnitsName);

                $attachments = array();
                if (!empty($contentInformation)) {
                    $contentInformation['dak_subject'] = isset($contentInformation['potro_subject'])
                        ? $contentInformation['potro_subject'] : '';
                    $contentInformation['sender_sarok_no'] = isset($contentInformation['sarok_no'])
                        ? $contentInformation['sarok_no'] : '';
                    $contentInformation['sender_office_id_final'] = isset($contentInformation['office_id'])
                        ? $contentInformation['office_id'] : 0;
                    $contentInformation['sender_officer_designation_id_final'] = isset($contentInformation['officer_designation_id'])
                        ? $contentInformation['officer_designation_id'] : 0;
                    $contentInformation['sender_officer_designation_label_final']
                        = isset($contentInformation['officer_designation_label'])
                        ? $contentInformation['officer_designation_label'] : '';
                    $contentInformation['sender_officer_id_final'] = isset($contentInformation['officer_id'])
                        ? $contentInformation['officer_id'] : 0;
                    $contentInformation['sender_officer_name_final'] = isset($contentInformation['officer_name'])
                        ? $contentInformation['officer_name'] : '';

                    $contentInformation['sovapoti_office_id_final'] = isset($contentInformation['sovapoti_office_id'])
                        ? $contentInformation['sovapoti_office_id'] : 0;
                    $contentInformation['sovapoti_officer_designation_id_final']
                        = isset($contentInformation['sovapoti_officer_designation_id'])
                        ? $contentInformation['sovapoti_officer_designation_id']
                        : 0;
                    $contentInformation['sovapoti_officer_designation_label_final']
                        = isset($contentInformation['sovapoti_officer_designation_label'])
                        ? $contentInformation['sovapoti_officer_designation_label']
                        : '';
                    $contentInformation['sovapoti_officer_id_final'] = isset($contentInformation['sovapoti_officer_id'])
                        ? $contentInformation['sovapoti_officer_id'] : 0;
                    $contentInformation['sovapoti_officer_name_final'] = isset($contentInformation['sovapoti_officer_name'])
                        ? $contentInformation['sovapoti_officer_name'] : '';

                    //approval
                    $contentInformation['approval_office_id_final'] = isset($contentInformation['approval_office_id'])
                        ? $contentInformation['approval_office_id'] : 0;

                    $contentInformation['approval_officer_id_final'] = isset($contentInformation['approval_officer_id'])
                        ? $contentInformation['approval_officer_id'] : 0;

                    $contentInformation['approval_office_unit_id_final'] = isset($contentInformation['approval_office_unit_id'])
                        ? $contentInformation['approval_office_unit_id'] : 0;

                    $contentInformation['approval_officer_designation_id_final']
                        = isset($contentInformation['approval_officer_designation_id'])
                        ? $contentInformation['approval_officer_designation_id']
                        : 0;

                    $contentInformation['approval_officer_designation_label_final']
                        = isset($contentInformation['approval_officer_designation_label'])
                        ? $contentInformation['approval_officer_designation_label']
                        : '';

                    $contentInformation['approval_officer_name_final'] = isset($contentInformation['approval_officer_name'])
                        ? $contentInformation['approval_officer_name'] : '';

                    $contentInformation['approval_office_name_final'] = isset($contentInformation['approval_office_name'])
                        ? $contentInformation['approval_office_name'] : '';

                    $contentInformation['approval_office_unit_name_final'] = isset($contentInformation['approval_office_unit_name'])
                        ? $contentInformation['approval_office_unit_name'] : '';


                    $this->set('draftVersion', $contentInformation);
                    $this->set('nothiMasterInfo', $nothiRecord);
                    $this->set('nothiRecord', $nothiRecord);
                    $this->set('nothimasterid', $nothimasterid);
                    $this->set('referenceid',
                        $contentInformation['nothi_potro_id']);
                    $this->set('referencetype', $type);

                    $potrojari_attachments = TableRegistry::get('PotrojariAttachments');

                    $attachments = $potrojari_attachments->find()->where(['potrojari_id' => $contentInformation->id,
                                                                          'is_inline' => 0])->toArray();

                    $inlineattachments = $potrojari_attachments->find('list',
                        ['keyField' => 'id', 'valueField' => 'potro_id'])->where(['potrojari_id' => $contentInformation->id,
                                                                                  'is_inline' => 1])->toArray();
                }

                $this->set('potroAttachmentRecord', $potroAttachmentRecord);
                $this->set('inlineattachments', $inlineattachments);
                $this->set('attachments', $attachments);

                $allOtherDraftsofThisPotro = array();
                if ($contentInformation->nothi_potro_id != 0) {
                    $allOtherDraftsofThisPotro = $potrojariTable->find()->select([
                        'Potrojari.id',
                        'Potrojari.nothi_master_id',
                        'Potrojari.nothi_part_no',
                        'Potrojari.potrojari_date',
                        'Potrojari.potro_subject',
                        'Potrojari.potro_type',
                    ])->where(['nothi_potro_id' => $contentInformation->nothi_potro_id,
                               'potro_status' => 'Draft'])->toArray();

                    if (!empty($allOtherDraftsofThisPotro)) {
                        $potrojariTemplateTable = TableRegistry::get('PotrojariTemplates');
                        foreach ($allOtherDraftsofThisPotro as $key => &$value) {

                            $templatename = $potrojariTemplateTable->get($value['potro_type']);
                            $value['template_name'] = $templatename['template_name'];
                        }
                    }
                }

                $this->set('allOtherDrafts', $allOtherDraftsofThisPotro);
                $time = new Time(date('Y-m-d'));
                $this->set('signature_date_bangla', $time->i18nFormat('d-M-Y', null, 'bn-BD'));
                $token = $this->generateToken([
                    'office_id'=> $employee_office['office_id'],
                    'office_unit_organogram_id'=>$employee_office['office_unit_organogram_id'],
                    'parent_id'=> $potrojari_id,
                    'module'=>'Potrojari',
                    'module_type'=> 'Nothi',
                ],['exp'=>time() + 3600*3]);
                $this->set('temp_token',$token);
                if(!empty($potrojari_id)){
                    // potro offline: potrojari_potrojari-id_part-no -- for edit potrojari
                    $this->set('offline_potro_draft_key','potrojari_'.$potrojari_id.'_'.$contentInformation['nothi_part_no']);
                }else{
                    // potro offline: potrojari_potrojari-id_part-no -- for make draft
                    $this->set('offline_potro_draft_key','potro_'.$employee_office['office_unit_organogram_id'].'_'.$employee_office['office_id'].'_'.$nothimasterid.'_'.$type.'_'.$potroid);
                }

            } catch (\Exception $ex) {

                $this->Flash->error("দুঃখিত! কোনো তথ্য পাওয়া যায়নি");
                $this->redirect(['controller' => 'Dashboard', 'action' => 'dashboard']);
            }
        }
    }

    protected function addDakmovement($dak_id,$dak_type,$comment){

        $DakMovementsTable = TableRegistry::get('DakMovements');
        $lastMove = $DakMovementsTable->find()->where(['dak_id'=>$dak_id,'dak_type'=>$dak_type,'operation_type'=>'NothiVukto'])->order(['id'])->first();
        if(!empty($lastMove)) {
            $first_move = $DakMovementsTable->newEntity();
            $first_move->dak_type = $lastMove['dak_type'];
            $first_move->dak_id = $lastMove['dak_id'];
            $first_move->dak_dagorik_type = $lastMove['dak_dagorik_type'];
            $first_move->from_office_id = $lastMove['from_office_id'];
            $first_move->from_office_name = $lastMove['from_office_name'];
            $first_move->from_office_unit_id = $lastMove['from_office_unit_id'];
            $first_move->from_office_unit_name = $lastMove['from_office_unit_name'];
            $first_move->from_office_address = $lastMove['from_office_address'];
            $first_move->from_officer_id = $lastMove['from_officer_id'];
            $first_move->from_officer_name = $lastMove['from_officer_name'];
            $first_move->from_officer_designation_id = $lastMove['from_officer_designation_id'];
            $first_move->from_officer_designation_label = $lastMove['from_officer_designation_label'];
            $first_move->to_office_id = $lastMove['to_office_id'];
            $first_move->to_office_name = $lastMove['to_office_name'];
            $first_move->to_office_unit_id = $lastMove['to_office_unit_id'];
            $first_move->to_office_unit_name = $lastMove['to_office_unit_name'];
            $first_move->to_office_address = $lastMove['to_office_address'];
            $first_move->to_officer_id = $lastMove['to_officer_id'];
            $first_move->to_officer_name = $lastMove['to_officer_name'];
            $first_move->to_officer_designation_id = $lastMove['to_officer_designation_id'];
            $first_move->to_officer_designation_label = $lastMove['to_officer_designation_label'];
            $first_move->attention_type = $lastMove['attention_type'];
            $first_move->docketing_no = $lastMove['docketing_no'];
            $first_move->from_sarok_no = $lastMove['from_sarok_no'];
            $first_move->to_sarok_no = $lastMove['to_sarok_no'];
            $first_move->operation_type = $lastMove['operation_type'];
            $first_move->sequence = $lastMove['sequence'] + 1;
            $first_move->dak_actions = $comment;
            $first_move->dak_priority = $lastMove['dak_priority'];
            $first_move->created_by = $this->Auth->user('id');
            $first_move->modified_by = $this->Auth->user('id');

            $DakMovementsTable->save($first_move);
        }
    }
    public function makeCsDraft($nothimasterid, $potroid=0, $type = "potro",$attached_id=0,$potrojari_id = 0,$nothi_office = 0)

    {
        //$this->view = 'potro_draft_offline';
        $employee_office = $this->getCurrentDakSection();

        $otherNothi = false;
        if ($nothi_office != 0 && $nothi_office != $employee_office['office_id']) {
            $otherNothi = true;

            $this->switchOffice($nothi_office, 'MainNothiOffice');
        } else {
            $nothi_office = $employee_office['office_id'];
        }


        $this->set('nothi_office', $nothi_office);
        $this->set('nothiMasterId', $nothimasterid);
        $this->set(compact( 'potroid', 'type','attached_id','employee_office'));
        $this->set(compact('otherNothi'));

        $guardFileCategories_table = TableRegistry::get("GuardFileCategories");
        $guarfilesubjects = $guardFileCategories_table->getTypes(0, $employee_office);
        $this->set(compact('guarfilesubjects'));

//        TableRegistry::remove('NothiMasterPermissions');
//        TableRegistry::remove('NothiMasterCurrentUsers');
        $nothiMasterPermissionsTable = TableRegistry::get("NothiMasterPermissions");
        $nothiMasterCurrentUsersTable = TableRegistry::get("NothiMasterCurrentUsers");

        $officeUnitsTable = TableRegistry::get('OfficeUnits');
        $potrosTable = TableRegistry::get('NothiPotros');
        $potroInfoTable = TableRegistry::get('NothiPotroAttachments');
        $potrojariTable = TableRegistry::get("Potrojari");
        $nothiPartsTable = TableRegistry::get('NothiParts');
        $potrojariTemplatesTable = TableRegistry::get('PotrojariTemplates');
        $table = TableRegistry::get('PotrojariHeaderSettings');
        try {
            $heading = $table->findByOfficeId($employee_office['office_id'])->firstOrfail();
        } catch (\Exception $ex) {
            $heading = $table->newEntity();
            $heading->office_id = $employee_office['office_id'];
            $heading->office_unit_id = $employee_office['office_unit_id'];
            $heading->office_unit_organogram_id = $employee_office['office_unit_organogram_id'];
            $heading->employee_record_id = $employee_office['officer_id'];
            $heading->potrojari_head = json_encode([
                'head_title' => 'গণপ্রজাতন্ত্রী বাংলাদেশ সরকার ',
                'head_ministry' => $employee_office['ministry_records'],
                'head_office' => $employee_office['office_name'],
                'head_unit' => '...',
                'head_other' => $employee_office['office_web']
            ]);
            $heading->potrojari_head_eng = json_encode([
                'head_title' => "Government of the People\'s Republic of Bangladesh",
                //                'head_title' => "Government of the Peoples Republic of Bangladesh",
                'head_ministry' => $employee_office['ministry_name_eng'],
                'head_office' => $employee_office['office_name_eng'],
                'head_unit' => '...',
                'head_other' => $employee_office['office_web']
            ]);

            $table->save($heading);
        }
        $this->set(compact('heading'));

        $nothiPartInformation = $nothiPartsTable->get($nothimasterid);

        $ownUnitInfo = $unitInfo = $officeUnitsTable->get($employee_office['office_unit_id']);

        if(!empty($potrojari_id)){
            $totalPotrojari = $potrojariTable->find()->where(['office_id' => $employee_office['office_id'],
                                                              'YEAR(potrojari_date)' => date('Y'), 'potrojari_draft_office_id' => $employee_office['office_id'],
                                                              'potrojari_draft_unit' => $employee_office['office_unit_id'], 'id <> ' => $potrojari_id])->count();
        }else{
            $totalPotrojari = $potrojariTable->find()->where(['office_id' => $employee_office['office_id'],
                                                              'YEAR(potrojari_date)' => date('Y'), 'potrojari_draft_office_id' => $employee_office['office_id'],
                                                              'potrojari_draft_unit' => $employee_office['office_unit_id']])->count();
        }


        $totalPotrojari = $ownUnitInfo['sarok_no_start'] + $totalPotrojari;

        $this->set('totalPotrojari', Number::format(($totalPotrojari + 1)));

        $nothiMasters = array();
        $nothiMastersCurrentUser = array();

        if (!empty($employee_office['office_id'])) {

            $nothiMasters = $nothiMasterPermissionsTable->hasAccess($nothi_office,
                $employee_office['office_id'],
                $employee_office['office_unit_id'],
                $employee_office['office_unit_organogram_id'],
                $nothiPartInformation['nothi_masters_id'],
                $nothiPartInformation['id']);

            $nothiMastersCurrentUser = $nothiMasterCurrentUsersTable->find()->where(['nothi_part_no' => $nothimasterid,
                                                                                     'office_id' => $employee_office['office_id'], 'office_unit_id' => $employee_office['office_unit_id'],
                                                                                     'office_unit_organogram_id' => $employee_office['office_unit_organogram_id'],
                                                                                     'nothi_office' => $nothi_office])->first();
            if (empty($nothiMasters) || $nothiMasters['privilige_type'] == 0) {
                $this->Flash->error("দুঃখিত! কোনো তথ্য পাওয়া যায়নি");
                $this->redirect(['controller' => 'Dashboard', 'action' => 'dashboard']);
            }

            if (!empty($nothiMastersCurrentUser)) {
                $this->set('privilige_type', $nothiMasters['privilige_type']);
            } else {
                $this->set('privilige_type', 2);
            }
//            $template_list = $this->potrojari_template_list();
            $template_list = [];

            $nothiMasterPermissionsTable = TableRegistry::get("NothiMasterPermissions");
            $permitted_nothi_list = $nothiMasterPermissionsTable->allNothiPermissionList($nothi_office,
                $employee_office['office_id'],
                $employee_office['office_unit_id'],
                $employee_office['office_unit_organogram_id']);

            $potroAttachmentRecord = $potroInfoTable->find('list',
                ['keyField' => 'id', 'valueField' => [
                    "nothi_potro_page_bn", 'subject'
                ]])
                                                    ->select(['subject' => 'NothiPotros.subject', 'NothiPotroAttachments.id',
                                                              "nothi_potro_page_bn" => 'NothiPotroAttachments.nothi_potro_page_bn'])
                                                    ->join([
                                                        'NothiPotros' => [
                                                            'table' => 'nothi_potros',
                                                            'type' => 'INNER',
                                                            'conditions' => 'NothiPotros.id = NothiPotroAttachments.nothi_potro_id'
                                                        ]
                                                    ])->where(['NothiPotroAttachments.nothi_master_id' => $nothiMasters['nothi_masters_id']])
                                                    ->where(['NothiPotroAttachments.nothijato' => 0, 'NothiPotroAttachments.status' => 1,
                                                    ])
                                                    ->where(['NothiPotroAttachments.nothi_part_no IN' => $permitted_nothi_list])
                                                    ->order(['NothiPotroAttachments.nothi_potro_page asc'])->toArray();
//             Potrojari body should be visible as attachment
//            'NothiPotroAttachments.attachment_type <> ' => 'text' -- ommited

            if(!empty($potrojari_id)) {
                $contentInformation = $potrojariTable->getInfo(['id' => $potrojari_id])->first();
                if(!empty($contentInformation) && $contentInformation['potro_status'] == 'Sent'){
                    $this->Flash->error("দুঃখিত! কোনো তথ্য পাওয়া যায়নি");
                    $this->redirect('/nothiDetail/'.$contentInformation['nothi_part_no']);
                }
                $this->set('is_new_potrojari',0);
            } else {
                $contentInformation = $potrojariTable->newEntity();
                $cs_letter = $potrojariTemplatesTable->getCsTemplateByIdAndVersion('bn');
                if(!empty($cs_letter)){
                    $contentInformation->potro_cover = $cs_letter['cs_cover']['html_content'];// forwarding potro
                    $contentInformation->potro_description = $cs_letter['cs_body']['html_content']; // mul potro
                }
                $this->set('is_new_potrojari',1);
//                pr($routine_letter); die;
            }
//            pr($contentInformation);die;
            if (!empty($contentInformation['officer_designation_id'])) {
                if ($contentInformation['can_potrojari'] == 1 && $contentInformation['officer_designation_id']
                    != $employee_office['office_unit_organogram_id']
                ) {
                    $this->set('privilige_type', 2);
                }
            }
            $this->set('potroInfo', array());
            $this->set('potroSubject', "");

            if (!empty($contentInformation['nothi_potro_id']) && $type == 'potro') {
                $potroInfo = $potroInfoTable->get($contentInformation['nothi_potro_id']);
                $this->set('potroInfo', $potroInfo);
                $potroSubject = $potrosTable->get($potroInfo['nothi_potro_id']);
                $this->set('potroSubject', $potroSubject['subject']);
            }
            $this->set('template_list', $template_list);
            $this->set('office_id', $employee_office['office_id']);
            $this->set('employee_office', $employee_office);

            try {
                $nothiRecord = $nothiPartsTable->getAll(['NothiParts.id' => $nothimasterid])->first();

                $officeUnitTable = TableRegistry::get('OfficeUnits');
                $officeunit = $officeUnitTable->get($nothiRecord['office_units_id']);
                $officeUnitsName = $officeunit['unit_name_bng'];

                $this->set('officeUnitsName', $officeUnitsName);

                $attachments = array();
                if (!empty($contentInformation)) {
                    $contentInformation['dak_subject'] = isset($contentInformation['potro_subject'])
                        ? $contentInformation['potro_subject'] : '';
                    $contentInformation['sender_sarok_no'] = isset($contentInformation['sarok_no'])
                        ? $contentInformation['sarok_no'] : '';
                    $contentInformation['sender_office_id_final'] = isset($contentInformation['office_id'])
                        ? $contentInformation['office_id'] : 0;
                    $contentInformation['sender_officer_designation_id_final'] = isset($contentInformation['officer_designation_id'])
                        ? $contentInformation['officer_designation_id'] : 0;
                    $contentInformation['sender_officer_designation_label_final']
                        = isset($contentInformation['officer_designation_label'])
                        ? $contentInformation['officer_designation_label'] : '';
                    $contentInformation['sender_officer_id_final'] = isset($contentInformation['officer_id'])
                        ? $contentInformation['officer_id'] : 0;
                    $contentInformation['sender_officer_name_final'] = isset($contentInformation['officer_name'])
                        ? $contentInformation['officer_name'] : '';

                    $contentInformation['sovapoti_office_id_final'] = isset($contentInformation['sovapoti_office_id'])
                        ? $contentInformation['sovapoti_office_id'] : 0;
                    $contentInformation['sovapoti_officer_designation_id_final']
                        = isset($contentInformation['sovapoti_officer_designation_id'])
                        ? $contentInformation['sovapoti_officer_designation_id']
                        : 0;
                    $contentInformation['sovapoti_officer_designation_label_final']
                        = isset($contentInformation['sovapoti_officer_designation_label'])
                        ? $contentInformation['sovapoti_officer_designation_label']
                        : '';
                    $contentInformation['sovapoti_officer_id_final'] = isset($contentInformation['sovapoti_officer_id'])
                        ? $contentInformation['sovapoti_officer_id'] : 0;
                    $contentInformation['sovapoti_officer_name_final'] = isset($contentInformation['sovapoti_officer_name'])
                        ? $contentInformation['sovapoti_officer_name'] : '';

                    //approval
                    $contentInformation['approval_office_id_final'] = isset($contentInformation['approval_office_id'])
                        ? $contentInformation['approval_office_id'] : 0;

                    $contentInformation['approval_officer_id_final'] = isset($contentInformation['approval_officer_id'])
                        ? $contentInformation['approval_officer_id'] : 0;

                    $contentInformation['approval_office_unit_id_final'] = isset($contentInformation['approval_office_unit_id'])
                        ? $contentInformation['approval_office_unit_id'] : 0;

                    $contentInformation['approval_officer_designation_id_final']
                        = isset($contentInformation['approval_officer_designation_id'])
                        ? $contentInformation['approval_officer_designation_id']
                        : 0;

                    $contentInformation['approval_officer_designation_label_final']
                        = isset($contentInformation['approval_officer_designation_label'])
                        ? $contentInformation['approval_officer_designation_label']
                        : '';

                    $contentInformation['approval_officer_name_final'] = isset($contentInformation['approval_officer_name'])
                        ? $contentInformation['approval_officer_name'] : '';

                    $contentInformation['approval_office_name_final'] = isset($contentInformation['approval_office_name'])
                        ? $contentInformation['approval_office_name'] : '';

                    $contentInformation['approval_office_unit_name_final'] = isset($contentInformation['approval_office_unit_name'])
                        ? $contentInformation['approval_office_unit_name'] : '';
                    if(empty($contentInformation['potrojari_language'])){
                        $contentInformation['potrojari_language'] = 'bn';
                    }
                    if(empty($contentInformation['potro_type'])){
                        $contentInformation['potro_type'] = 30;
                    }

                    $this->set('draftVersion', $contentInformation);
                    $this->set('nothiMasterInfo', $nothiRecord);
                    $this->set('nothiRecord', $nothiRecord);
                    $this->set('nothimasterid', $nothimasterid);
                    $this->set('referenceid',
                        $contentInformation['nothi_potro_id']);
                    $this->set('referencetype', $type);

                    $potrojari_attachments = TableRegistry::get('PotrojariAttachments');

                    $attachments = $potrojari_attachments->find()->where(['potrojari_id' => $contentInformation->id,
                                                                          'is_inline' => 0])->toArray();

                    $inlineattachments = $potrojari_attachments->find('list',
                        ['keyField' => 'id', 'valueField' => 'potro_id'])->where(['potrojari_id' => $contentInformation->id,
                                                                                  'is_inline' => 1])->toArray();
                }

                $this->set('potroAttachmentRecord', $potroAttachmentRecord);
                $this->set('inlineattachments', $inlineattachments);
                $this->set('attachments', $attachments);

                $allOtherDraftsofThisPotro = array();
                if ($contentInformation['nothi_potro_id'] != 0) {
                    $allOtherDraftsofThisPotro = $potrojariTable->find()->select([
                        'Potrojari.id',
                        'Potrojari.nothi_master_id',
                        'Potrojari.nothi_part_no',
                        'Potrojari.potrojari_date',
                        'Potrojari.potro_subject',
                        'Potrojari.potro_type',
                    ])->where(['nothi_potro_id' => $contentInformation['nothi_potro_id'],
                               'potro_status' => 'Draft'])->toArray();

                    if (!empty($allOtherDraftsofThisPotro)) {
                        $potrojariTemplateTable = TableRegistry::get('PotrojariTemplates');
                        foreach ($allOtherDraftsofThisPotro as $key => &$value) {

                            $templatename = $potrojariTemplateTable->get($value['potro_type']);
                            $value['template_name'] = $templatename['template_name'];
                        }
                    }
                }

                $this->set('allOtherDrafts', $allOtherDraftsofThisPotro);
                $time = new Time(date('Y-m-d'));
                $this->set('signature_date_bangla', $time->i18nFormat('d-M-Y', null, 'bn-BD'));
                $token = $this->generateToken([
                    'office_id'=> $employee_office['office_id'],
                    'office_unit_organogram_id'=>$employee_office['office_unit_organogram_id'],
                    'parent_id'=> $potrojari_id,
                    'module'=>'Potrojari',
                    'module_type'=> 'Nothi',
                ],['exp'=>time() + 3600*3]);
                $this->set('temp_token',$token);
                if(!empty($potrojari_id)){
                    // potro offline: potrojari_potrojari-id_part-no -- for edit potrojari
                    $this->set('offline_potro_draft_key','potrojari_'.$potrojari_id.'_'.$contentInformation['nothi_part_no']);
                }else{
                    // potro offline: potrojari_potrojari-id_part-no -- for make draft
                    $this->set('offline_potro_draft_key','potro_'.$employee_office['office_unit_organogram_id'].'_'.$employee_office['office_id'].'_'.$nothimasterid.'_'.$type.'_'.$potroid);
                }

            } catch (\Exception $ex) {

                $this->Flash->error("দুঃখিত! কোনো তথ্য পাওয়া যায়নি");
                $this->redirect(['controller' => 'Dashboard', 'action' => 'dashboard']);
            }
//            pr($contentInformation);die;
        }
    }
    public function updateCsBody(){
        $response = [
            'status' => 'error',
            'message' => 'Something went wrong'
        ];
        $cs_id = isset($this->request->data['cs_id'])?$this->request->data['cs_id']:'';
        $cs_body = isset($this->request->data['cs_body'])?$this->request->data['cs_body']:'';
        $is_signature = isset($this->request->data['is_signature'])?$this->request->data['is_signature']:0;
        $employee_office = $this->getCurrentDakSection();

        if(empty($employee_office)){
            $api_key = isset($this->request->data['api_key'])?$this->request->data['api_key']:0;
            if(empty($api_key)){
                $response['message'] = __('Unauthorized Access');
                goto rtn;
            }
        }
        if(empty($cs_id) || empty($cs_body)){
            $response['message'] = __('Required info not given');
            goto rtn;
        }
        try{
            $tablePJ = TableRegistry::get('Potrojari');
            $csData = $tablePJ->get($cs_id);
            if(!empty($csData)){
                $nothi_part_no = $csData->nothi_part_no;
                if(!empty($nothi_part_no)){
                    $current_user_info = TableRegistry::get('NothiMasterCurrentUsers')->getCurrentUserofNote($csData->nothi_master_id,$nothi_part_no)->select(['office_unit_organogram_id','nothi_office'])->first();
                    if(empty($current_user_info)){
                        $response['message'] = __('Required info missing');
                        goto rtn;
                    }
                    else if($current_user_info['office_unit_organogram_id'] != $employee_office['office_unit_organogram_id']){
                        $response['message'] = __('Unauthorized Access');
                        goto rtn;
                    }
                    $csData->potro_description = $cs_body;
                    if(isset($is_signature) && $is_signature == 1){
                        $meta_data = jsonA($csData->meta_data);
                        $meta_data['has_signature']  = $employee_office['office_unit_organogram_id'];
                        $csData->meta_data = json_encode($meta_data);
                    }
                    $tablePJ->save($csData);
                    $response = [
                        'status' => 'success',
                        'message' => __('Data successfully saved'),
                    ];
                }
            }
        }catch (\Exception $ex){
            $response['message'] = __('Technical error happen');
            goto rtn;
        }
        //check if user is note's current user.
        rtn:
        $this->response->type('application/json');
        $this->response->body(json_encode($response));
        return $this->response;
    }

    public function show_potrojari($potrojari_id) {
		$employee_office = $this->getCurrentDakSection();

		if (empty($employee_office['office_id'])) {
			if (!empty($this->request->query['user_designation'])) {
				$this->request->query['user_designation'] = intval($this->request->query['user_designation']);
				$employee_office = $this->setCurrentDakSection($this->request->query['user_designation']);
			} elseif (!empty($this->request->data['user_designation'])) {
				$this->request->data['user_designation'] = intval($this->request->data['user_designation']);
				$employee_office = $this->setCurrentDakSection($this->request->data['user_designation']);
			} else {
				//die("Invalid request");
			}
		}
//		$otherNothi = false;
//		if (($attachment_office_id != 0 && $attachment_office_id != $employee_office['office_id']) || ($attachment_office_id != 0 && $data_ref == 'api')) {
//			$otherNothi = true;
//
//			$this->switchOffice($attachment_office_id, 'MainNothiOffice');
//		} else {
			$nothi_office = $employee_office['office_id'];
//		}

		$this->view = 'file_popup';
		$this->layout = null;
		TableRegistry::remove('Potrojari');
		$table = TableRegistry::get('Potrojari');
		$data = $table->find()->where(['id' => $potrojari_id])->first();

		if (!empty($data['file_name'])) {
			$token = $this->generateToken([
				'office_unit_organogram_id' => $employee_office['office_unit_organogram_id'],
				'file' => $data['file_name'],
			], ['exp' => time() + 60 * 300]);
			$this->set('token', $token);
		}

		$this->set('data', $data);
		//$this->set('isMobile', $isMobile);
		$this->set('nothi_office',$nothi_office);
		//$this->set('name', 'downloadPotrojariAttachmentRef');
	}

    public function edit3rdPartyPotro($nothi_office, $id){

        $employee_office = $this->getCurrentDakSection();

        $otherNothi = false;
        if ($nothi_office != 0 && $nothi_office != $employee_office['office_id']) {
            $otherNothi = true;

            $this->switchOffice($nothi_office, 'MainNothiOffice');
        } else {
            $nothi_office = $employee_office['office_id'];
        }

        $this->set('nothi_office',$nothi_office);
        $this->set('id',$id);
        $this->set('employee_office',$employee_office);
        $this->set('otherNothi',$otherNothi);

        $tableNothiPotroAttachments = TableRegistry::get('NothiPotroAttachments');
        $nothiMasterCurrentUsersTable = TableRegistry::get("NothiMasterCurrentUsers");
        $potroInfo = $tableNothiPotroAttachments->find()->contain(['NothiPotros'])->where(['NothiPotroAttachments.id'=>$id])->first();

        $nothiMastersCurrentUser = $nothiMasterCurrentUsersTable->find()->where(['nothi_part_no' => $potroInfo['nothi_part_no'],
            'office_id' => $employee_office['office_id'], 'office_unit_id' => $employee_office['office_unit_id'],
            'office_unit_organogram_id' => $employee_office['office_unit_organogram_id'],
            'nothi_office' => $nothi_office])->first();

        if (!empty($potroInfo['nothi_potro']['application_origin']) && $potroInfo['nothi_potro']['application_origin'] != 'nothi') {
            $decision_meta = jsonA($potroInfo['nothi_potro']['application_meta_data']);
            $signdesk = !empty($decision_meta['sdesk']) ? ($decision_meta['sdesk']) : 0;
            $action_decisions = !empty($decision_meta['action_decisions']) ? ($decision_meta['action_decisions']) : [];
            $decision_answer = isset($decision_meta['decision_answer']) ? $decision_meta['decision_answer'] : -1;

            if($this->request->is('get')){
                // attach QR code
                $makeQRdata = [];
                if(isset($decision_meta['company_name'])){
                    $makeQRdata['Company-Name'] = $decision_meta['company_name'];
                }
                if(isset($decision_meta['tracking_id'])){
                    $makeQRdata['Tracking-ID'] = $decision_meta['tracking_id'];
                }
                if(!empty($makeQRdata)){
                    $makeQRdata['Approval-Time'] = Date('Y-m-d H:i:s');
                }
                $qrCodeInfo = $this->makeQRCode($makeQRdata,['format' => 'base64Image']);
                if(!empty($qrCodeInfo['status']) && $qrCodeInfo['status'] == 'success' && !empty($qrCodeInfo['base64Image'])){
                    $this->set('qr_code',$qrCodeInfo['base64Image']);
                }
            }

            if(empty($action_decisions['editable']) || (!in_array($decision_answer,$action_decisions['editable']) && !in_array($decision_answer,$action_decisions['completable']))){
                $this->Flash->error("দুঃখিত! কোনো তথ্য পাওয়া যায়নি");
                $this->redirect(['controller' => 'Dashboard', 'action' => 'dashboard']);
            }

            $this->set('action_decisions',$action_decisions);
            $this->set('decision_answer',$decision_answer);
        }else{
            $this->Flash->error("দুঃখিত! কোনো তথ্য পাওয়া যায়নি");
            $this->redirect(['controller' => 'Dashboard', 'action' => 'dashboard']);
        }
        if (empty($nothiMastersCurrentUser)) {
            $this->Flash->error("দুঃখিত! কোনো তথ্য পাওয়া যায়নি");
            $this->redirect(['controller' => 'Dashboard', 'action' => 'dashboard']);
        }
        $employeeOfficesTable = TableRegistry::get('EmployeeOffices');
        $usersTable = TableRegistry::get('Users');
        $signDeskData =$employeeOfficesTable->find()->where(['status'=>1,'office_unit_organogram_id'=>$signdesk])->first();

        if(empty($signDeskData)){
            $this->Flash->error("দুঃখিত! অনুমোদনকারীর তথ্য সঠিক নয়");
            $this->redirect($this->request->referer());
        }

        if($this->request->is('post')){

            $jsonResponse = ['status'=>'error','message'=>'দুঃখিত! ভুল তথ্য প্রেরণ করা হয়েছে'];
            if(!empty($this->request->data['content_body'])){
                $potroInfo['content_body'] = $this->request->data['content_body'];

                if(!empty($this->request->data['imeidata'])) {
                    if(is_array($this->request->data['imeidata'])){
                        $imeidata = [];
                        foreach($this->request->data['imeidata'] as $key=>$value){
                            $imeidata[] = jsonA($value);
                        }

                        $decision_meta['tbl_imei_data'] = $imeidata;

                        $tableNothiPotroAttachments->NothiPotros->updateAll(
                            ['application_meta_data'=>json_encode($decision_meta),'potro_content'=>$potroInfo['content_body']],['id'=>$potroInfo['nothi_potro']['id']]
                        );
                    }
                }

                $tableNothiPotroAttachments->save($potroInfo);

                $jsonResponse = ['status'=>'success','message' => __('Data successfully added')];
            }

            $this->response->body(json_encode($jsonResponse));
            $this->response->type('json');
            return $this->response;
        }

        $this->set('privilige_type', 1);
        $this->set('draftVersion', $potroInfo);

        $nothiPartsTable = TableRegistry::get("NothiParts");
        $nothiRecord = $nothiPartsTable->getAll(['NothiParts.id' => $potroInfo['nothi_part_no']])->first();

        $this->set('nothiRecord', $nothiRecord);

        $officeUnitTable = TableRegistry::get('OfficeUnits');
        $officeunit = $officeUnitTable->get($nothiRecord['office_units_id']);
        $officeUnitsName = $officeunit['unit_name_bng'];

        $this->set('officeUnitsName', $officeUnitsName);

        $userData = $usersTable->find()->where(['employee_record_id'=>$signDeskData['employee_record_id']])->hydrate(false)->first();

        $userData['en_username'] = sGenerateToken(['file'=>$userData['username']],['exp'=>time() + 60*300]);

        $this->set('user_info',$userData);
        $this->set('nothi_part_no',$potroInfo['nothi_part_no']);
        $this->set('potro_id',$potroInfo['nothi_potro']['id']);

    }

    public function potrojariPrintPreview($id){

      $potrojariTable = TableRegistry::get('Potrojari');
      $potrojariData = $potrojariTable->get($id);
      $this->layout = 'potrojari-print-preview/potrojari_'.$potrojariData->potro_type;
      $this->set("potrojariData",$potrojariData);

    }
}