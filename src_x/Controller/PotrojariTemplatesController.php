<?php

namespace App\Controller;

use App\Controller\ProjapotiController;
use App\Model\Table;
use App\Model\Table\PotrojariTemplatesTable;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

class PotrojariTemplatesController extends ProjapotiController
{

    public function getTemplatesByVersion($version='bn')
    {
            try {
                $offices2ShowTemplateModals =  $this->offices2ShowTemplateModals();
                $employee_offices = $this->getCurrentDakSection();
                if(!empty($offices2ShowTemplateModals) && !empty($employee_offices)){
                    if(in_array($employee_offices['office_id'], $offices2ShowTemplateModals)){
                         $template = $this->PotrojariTemplates->find('list',['keyField' =>'template_id','valueField' =>'template_name'])->where(['status' => 1,'version' => $version])->toArray();
                    }else{
                          $template = $this->PotrojariTemplates->find('list',['keyField' =>'template_id','valueField' =>'template_name'])->where(['status' => 1,'version' => $version,'id <>' => 21])->toArray();
                    }
                }
              
            } catch (\Exception $ex) {
                $template = [];
            }
            $this->response->type('application/json');
            $this->response->body(json_encode($template));
            return $this->response;
    }
    public function getTemplate($id = null, $is_ajax = null,$version='bn')
    {
        $this->layout = null;
        $template     = array();
        if (!empty($id)) {
            try {
                if($id == 19){
                    $version = 'bn';
                }

                if($id == 30){
                    $template = $this->PotrojariTemplates->find()->where(['id IN'=>[28,29]])->toArray();
                }
                else{
                    $template = $this->PotrojariTemplates->getTemplateByIdAndVersion($id,$version);
                }
            } catch (\Cake\Datasource\Exception\RecordNotFoundException $e) {

            }
            catch (\Exception $ex) {

            }
        }

        echo json_encode($template);
        die();
    }

}
