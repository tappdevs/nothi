<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\I18n\I18n;
use Cake\Network\Email\Email;
use Cake\Datasource\ConnectionManager;
use Cake\I18n\Number;
use Cake\I18n\Time;
use Exception;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;
use Cake\Cache\Cache;
// JWT Auth For API
use \Firebase\JWT\JWT;
use mikehaertl\wkhtmlto\Pdf;
use Cake\Routing\Router;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class ProjapotiController extends AppController
{
    public $components = array('Cookie');

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * @return void
     */
    //use ProjapotiAuditTrait;
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Flash');
        $this->loadComponent('ControllerList');
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Paginator');
        $this->loadComponent('Auth',
            [
                'loginRedirect' => [
                    'controller' => 'dashboard',
                    'action' => 'dashboard'
                ],
                'logoutRedirect' => [
                    'controller' => 'Users',
                    'action' => 'login'
                ]
            ]);
    }

    public function afterFilter(Event $event)
    {
        if ($this->request->is('post') && !empty($this->request->data['data_ref']) || !empty($this->request->query['data_ref'])) {
            //for api request no need to run below code
            return;
        }
        if (defined("Live") && Live == 1) {
            $user = $this->Auth->user();

            if (!empty($user)) {
                $employee_record_table = TableRegistry::get('EmployeeRecords');

                if (!empty($user['user_role_id']) && $user['user_role_id'] > 2 && $user['force_password_change'] == 1) {
                    if (defined("PROJAPOTI_API") && PROJAPOTI_API == true) {

                        $this->response->type('application/json');
                        $this->response->body(json_encode(['status' => 'error', 'message' => 'Please change your default password!',
                            'msg' => 'Please change your default password!']));
                        return $this->response;
                    } else {
                        if ($this->request->action != 'resetPassword') {
                            return $this->redirect(['action' => 'resetPassword', 'controller' => 'EmployeeRecords']);
                        }
                    }
                }

                if ($this->request->is('ajax') || (!empty($user['user_role_id']) && $user['user_role_id'] > 2 && $user['force_password_change'] == 1)) {

                } else {

                    $tildate = new Time("2016-12-01");
                    $today = new Time();

                    if (!empty($user['user_role_id']) && $user['user_role_id'] > 2 && $today->gte($tildate)) {

                        $employee_record = $employee_record_table->get($user['employee_record_id']);

                        if (empty($employee_record['date_of_birth']) || $this->checkNID($employee_record['date_of_birth']->format("Y-m-d"), $employee_record['nid'])) {
                            if (defined("PROJAPOTI_API") && PROJAPOTI_API == true) {
                                if (!empty($employee_record['date_of_birth']) && !empty($employee_record['nid']) && strlen($employee_record['nid']) == 10) {
                                    //10 Digit NID escape
                                } else {
                                    $this->response->type('application/json');
                                    $this->response->body(json_encode(['status' => 'error', 'message' => 'Please set Valid NID from your profile edit page!',
                                        'msg' => 'Please set Valid NID from your profile edit page!']));
                                    return $this->response;
                                }

                            } else {
//                                if (!empty($employee_record['date_of_birth']) && !empty($employee_record['nid']) && strlen($employee_record['nid']) == 10) {
//                                    //10 Digit NID escape
//                                } else {
//                                    if ($this->request->action != 'updateNid') {
//                                        return $this->redirect(['action' => 'updateNid', 'controller' => 'EmployeeRecords']);
//                                    }
//                                }
                            }
                        } else if (empty($employee_record['personal_email']) && $this->request->action != 'profileEdit') {
                            $this->Flash->error(__('আপনার প্রোফাইলে ইমেল তথ্য দেওয়া হয়নি। দয়া করে ইমেইল তথ্য দিন'));
                            return $this->redirect(['action' => 'profileEdit', 'controller' => 'EmployeeRecords']);
                        }
                    }

                    if((!defined("PROJAPOTI_API") || PROJAPOTI_API == false) && !empty($user['user_role_id']) && $user['user_role_id'] > 2) {
                        $this->checkProtikolpo($user['employee_record_id']);
                    }
                }
            }
        }else{
            $user = $this->Auth->user();

            if(!empty($user) && (!defined("PROJAPOTI_API") || PROJAPOTI_API == false) && !empty($user['user_role_id']) && $user['user_role_id'] > 2) {
                $this->checkProtikolpo($user['employee_record_id']);
            }
        }
    }

    public function beforeRender(Event $event)
    {
        if (!defined("CDN_PATH") && defined("CDN") && CDN == 1) {
            define("CDN_PATH", 'http://cdn1.nothi.gov.bd/webroot/');
        } else if (!defined("CDN_PATH")) {
            define("CDN_PATH", $this->request->webroot);
        }
        if (!defined("js_css_version")) {
            define("js_css_version", "v2" );
        }
        if (!defined("DAK_PRIORITY_TYPE_ENG")) {
            define("DAK_PRIORITY_TYPE_ENG", json_encode(array('0' => ' ', '2' => 'Emergency', '3' => 'Immediately', '4' => 'Highest Priority', 5 => 'Instructions', 6 => 'Seek Attention')));

        }
        if (!defined("DAK_SECRECY_TYPE_ENG")) {
            define("DAK_SECRECY_TYPE_ENG", json_encode(array('0' => ' ', '2' => 'Very Confidential', '3' => 'Special Confidential', '4' => 'Confidential', '5' => 'Limited')));
        }

		$global_messages['read'] = [];
		$global_messages['unread'] = [];
		$designation = $this->getCurrentDakSection();
		if($designation) {
			$global_messages = $this->get_global_messages($designation);
		}
		$this->set('global_messages', $global_messages);
    }

    public function beforeFilter(Event $event)
    {
        $session = $this->request->session();
        $lang = $session->read('lang_code');
        if (!empty($lang) && $lang == "en") {
            I18n::locale('en_US');
        } else {
            I18n::locale('bn_BD');
        }
//        $auth_user = !empty($this->Auth->user())?$this->Auth->user():[];
        if (($this->request->is('post') && isset($this->request->data['data_ref']) && $this->request->data['data_ref'] == 'api') || (isset($this->request->query['data_ref']) && $this->request->query['data_ref'] == 'api') ){
            //for api request no need to run below code
            return;
        }
// Some get request does not need to calculate pending Dak & Nothi. Ajax request also does not related to Pending Dak & Nothi

        $dont_calculate_pending_dak_nothi = $session->read('request_dont_need_calculation_of_pending');
        if (empty($dont_calculate_pending_dak_nothi)) {
            // all controller  and action name should be in lower case.
            $dont_calculate_pending_dak_nothi = [
                'users/logout',
                'users/login',
                'nothimasters/index'
            ];
            $session->write('request_dont_need_calculation_of_pending', $dont_calculate_pending_dak_nothi);
        }
        $actionUrl = $this->request->params ['controller'] . '/' . $this->request->params ['action'];
        if (in_array(strtolower($actionUrl), $dont_calculate_pending_dak_nothi)) {
            return;
        }

        $totalNothi = 0;
        $totalDak = 0;
        $this->_pickLayout();

        // From login time same user is Auth user. better not to call again and again.
        $designation = $this->getCurrentDakSection();
        if (count($session->read('loggedUser')) < 2) {
            $loggedUser = $this->Auth->user();
            $loggedUser['is_admin'] =!empty($designation['is_admin'])?($designation['is_admin']):0;
            $user = $loggedUser;
            $this->set('loggedUser', $loggedUser);
            $session->write('loggedUser', $loggedUser);
        } else {
            $loggedUser = $session->read('loggedUser');
            $loggedUser['is_admin'] = !empty($designation['is_admin'])?($designation['is_admin']):0;
            $user = $loggedUser;
            $this->set('loggedUser', $loggedUser);
        }
//        echo 'here';die;
        if (!empty($user) && !$this->request->is('ajax')) {
            $this->_setupAccessControl($event);
        }

        $current_office_id = isset($designation['office_id']) ? $designation['office_id'] : 0;

        if (!empty($user) && !empty($user['user_role_id']) && $user['user_role_id']
            > 2
        ) {

            $table_instance_emp_offices = TableRegistry::get('EmployeeOffices');

            $employee_office_records = $table_instance_emp_offices->getEmployeeOfficeRecords($user['employee_record_id']);

            if (!empty($employee_office_records)) {


                $dataSourceObject = ConnectionManager::get('default');
                $current_db = $dataSourceObject->config()['database'];

                // If current DB is own office DB, then no need to switchOffice
                if (!empty($this->request->data['selected_office_id'])) {
                    $this->switchOffice($this->request->data['selected_office_id'], 'myOffice');
                    $current_office_id = $this->request->data['selected_office_id'];
                } else if (!empty($designation)) {
                    if ($current_db != 'projapoti_' . $designation['office_id']) {
                        $this->switchOffice($designation['office_id'], 'myOffice');
                    }
                } else {
                    if ($current_db != 'projapoti_' . $employee_office_records[0]['office_id']) {
                        $this->switchOffice($employee_office_records[0]['office_id'], 'myOffice');
                        $current_office_id = $employee_office_records[0]['office_id'];
                    }
                }
                if (defined('REDIRECT') && REDIRECT == false) {

                } else {
                    if ((defined("PROJAPOTI_API") && PROJAPOTI_API == true) || $this->request->is('ajax')) {

                    }
                    else {
                        $officeDomainTable = TableRegistry::get('OfficeDomains');
                        $officeDomain = $officeDomainTable->find()->where(['office_id' => $designation['office_id']])->first();
                        // setting subDomain
                        $subdomain = $this->checkSubdomain();
                        if (!empty($officeDomain)) {

                            if ($subdomain == -1 || $subdomain != $officeDomain['domain_prefix']) {

                                $this->request->env('HTTP_HOST',
                                    $officeDomain['domain_prefix'] . '.' . $this->extract_domain($_SERVER['HTTP_HOST']));
                                // query params if any
                                $url = $officeDomain['domain_url'] . '/' . $this->request->url;
                                if(!empty($this->request->query)){
                                    $url .='?';
                                    $f = 0;
                                    foreach($this->request->query as $param => $val){
                                        if($f == 1){
                                            $url .= '&';
                                        }
                                        $url .= $param.'='.$val;
                                        $f = 1;
                                    }
                                }

                                $this->redirect($url);
                            }
                        }
                    }
                }

                // Calculate Pending Dak and Nothi (ajax request does not need it)
                if (!$this->request->is('ajax')) {
                    $allunits = array();
                    if (!empty($employee_office_records)) {
                        foreach ($employee_office_records as $key => $value) {
                            if (!empty($value['office_unit_id'])) {
                                $allunits['unit'][] = $value['office_unit_id'];
                            }
                            if (!empty($value['office_unit_organogram_id'])) {
                                $allunits['org'][] = $value['office_unit_organogram_id'];
                            }
                            if (!empty($value['office_id'])) {
                                $allunits['ofc'][] = $value['office_id'];
                            }
                        }
                    }
                    TableRegistry::remove('NothiMasterCurrentUsers');
                    TableRegistry::remove('NothiNotes');
                    TableRegistry::remove('DakUsers');
                    $nothiCurrentUser = TableRegistry::get('NothiMasterCurrentUsers');
                    $table_instance_dak_user = TableRegistry::get('DakUsers');
                    $totalNothi = $nothiCurrentUser->getTotalNothiCount($allunits['org'],$current_office_id);
                    $totalDak = $table_instance_dak_user->getTotalDakCountBy_employee_designation(DAK_CATEGORY_INBOX,
                        $allunits['org']);
                }


                if (!empty($session) && !empty($designation)) {

                } elseif (!empty($user['employee_record_id'])) {
                    $table_instance_emp_records = TableRegistry::get('EmployeeRecords');

                    $employee_info = $table_instance_emp_records->get($user['employee_record_id']);

                    $table_instance_emp_offices = TableRegistry::get('EmployeeOffices');

                    $employee_office_records = $table_instance_emp_offices->getEmployeeOfficeRecords($employee_info->id);

                    $data_item = array();
                    $data_item['personal_info'] = $employee_info;
                    $data_item['office_records'] = count($employee_office_records)
                    > 0 ? $employee_office_records : array();

                    $table_instance_offices = TableRegistry::get('Offices');
                    $table_instance_ministris = TableRegistry::get('OfficeMinistries');
                    $officeInformation = array();
                    $ministryInformation = array();
                    if (!empty($employee_office_records[0]['office_id'])) {
                        $officeInformation = $table_instance_offices->get($employee_office_records[0]['office_id']);

                        if (!empty($officeInformation)) {
                            $ministryInformation = $table_instance_ministris->get($officeInformation['office_ministry_id']);
                        }
                    }

                    $data_item['ministry_records'] = count($ministryInformation)
                    > 0 ? $ministryInformation : array();

                    $session = $this->request->session();
                    if (!$session->check('logged_employee_record')) {
                        $session->write('logged_employee_record', $data_item);
                    }

                    if (!empty($employee_office_records[0])) {

                        $office_section = array();
                        $selected_office = $employee_office_records[0];
                        $office_section['office_id'] = $selected_office['office_id'];
                        $office_section['office_unit_id'] = $selected_office['office_unit_id'];
                        $office_section['office_unit_organogram_id'] = $selected_office['office_unit_organogram_id'];
                        $office_section['officer_id'] = $employee_office_records[0]['employee_record_id'];
                        $office_section['incharge_label'] = $employee_office_records[0]['incharge_label'];

                        $OfficeUnitOrganogramsTable = TableRegistry::get('OfficeUnitOrganograms');
                        $owninfo = $OfficeUnitOrganogramsTable->get($employee_office_records[0]['office_unit_organogram_id']);
                        $office_section['is_admin'] = $owninfo['is_admin'];
                        //$office_section['is_admin'] = $employee_office_records[0]['is_admin'];

                        $table_instance_office = TableRegistry::get('Offices');
                        $office = $table_instance_office->get($office_section['office_id']);
                        $office_section['office_name'] = $office['office_name_bng'];
                        $office_section['office_name_eng'] = $office['office_name_eng'];
                        $office_section['office_address'] = $office['office_address'];

                        $designation_info = TableRegistry::get('OfficeUnitOrganograms')->getAll(['id' => $selected_office['office_unit_organogram_id']], ['designation_eng'])->first();
                        $office_section['designation_label'] = $office_section['designation'] = $selected_office['designation'];
                        $office_section['designation_eng'] = $designation_info['designation_eng'];

                        $office_section['officer_name'] = $employee_info['name_bng'];
                        $office_section['officer_name_eng'] = $employee_info['name_eng'];

                        $office_section['ministry_id'] = $office['office_ministry_id'];

                        $ministryInformation = $table_instance_ministris->get($office['office_ministry_id']);
                        $office_section['ministry_records'] = $office_section['ministry_name_bng'] = count($ministryInformation)
                        > 0 ? $ministryInformation['name_bng'] : '';
                        $office_section['ministry_name_eng'] = count($ministryInformation)
                        > 0 ? $ministryInformation['name_eng'] : '';

                        $office_section['office_phone'] = $office['office_phone'];
                        $office_section['office_fax'] = $office['office_fax'];
                        $office_section['office_email'] = $office['office_email'];
                        $office_section['office_web'] = $office['office_web'];

                        $table_instance_office_unit = TableRegistry::get('OfficeUnits');
                        $office_unit = $table_instance_office_unit->get($office_section['office_unit_id']);
                        $office_section['office_unit_name'] = $office_unit['unit_name_bng'];
                        $office_section['office_unit_name_eng'] = $office_unit['unit_name_eng'];
                        $office_section['show_unit'] = $selected_office['show_unit'];

                         //Digital sign
                        $office_section['default_sign'] = intval($employee_info['default_sign']);
                        $office_section['cert_id'] = $employee_info['cert_id'];
                        $office_section['cert_type'] = $employee_info['cert_type'];
                        $office_section['cert_provider'] = $employee_info['cert_provider'];
                        $office_section['cert_serial'] = $employee_info['cert_serial'];

                        $office_section['user_id'] = isset($user['id'])?$user['id']:0;
                        $office_section['username'] = isset($user['username'])?$user['username']:'';

                        $designation = $office_section;
                        $session->write('selected_office_section', $office_section);

                        $loggedUser = $session->read('loggedUser');
                        $loggedUser['is_admin'] = $office_section['is_admin'];
                        $this->set('loggedUser', $loggedUser);
                    }
                }
            }
        }
        //potrojari pending -- only in Nothi module nothi list function
        $selected_module_id = $session->read('module_id');
        if($selected_module_id == 4 && $loggedUser['user_role_id'] > 2 && $this->request->params ['action'] == 'dashboard' ){
            TableRegistry::remove('Potrojari');
            $totalPotrojariPending = TableRegistry::get('Potrojari')->getDesignationWisePotrojariPending($designation['office_id'],$designation['office_unit_id'],$designation['office_unit_organogram_id']);
            $this->set(compact('totalPotrojariPending'));
            $url = $this->request->webroot.'pendingPotrojari';
            if($totalPotrojariPending > 0){
                $this->Flash->set('<a href="'.$url.'"> প্রিয় ব্যবহারকারী, আপনার '.enTobn($totalPotrojariPending). 'টি পত্র - জারি সফল হয়নি। বিস্তারিত জানার জন্য ক্লিক করুন।</a>');
            }
        }
        $this->set('totalNothi', $totalNothi);
        $this->set('totalDak', $totalDak);
        $this->set('Year', Number::format(date('Y'), ['pattern' => '#']));
    }

    public function get_global_messages($employee) {
		$parameter['office'] = $employee['office_id'];
		$parameter['unit'] = $employee['office_unit_id'];
		$parameter['organogram'] = $employee['office_unit_organogram_id'];
		$office = TableRegistry::get('Offices')->get($employee['office_id']);
		$office_layer = TableRegistry::get('OfficeLayers')->get($office['office_layer_id']);
		$parameter['layer'] = $office_layer['layer_level'];
		$parameter['office_origin'] = $office['office_origin_id'];
		$parameter['office_layer'] = $office['office_layer_id'];
		$parameter['office_ministry'] = $office['office_ministry_id'];

		$messageTable = TableRegistry::get('Messages');
		$messages = $messageTable->find()->where(function($query) use ($parameter) {
			$conditions[] = $query->and_(['message_for' => 'office', 'related_id' => $parameter['office']]);
			$conditions[] = $query->and_(['message_for' => 'unit', 'related_id' => $parameter['unit']]);
			$conditions[] = $query->and_(['message_for' => 'organogram', 'related_id' => $parameter['organogram']]);
			$conditions[] = $query->and_(['message_for' => 'layer', 'related_id' => $parameter['layer']]);
			$conditions[] = $query->and_(['message_for' => 'office_origin', 'related_id' => $parameter['office_origin']]);
			$conditions[] = $query->and_(['message_for' => 'office_layer', 'related_id' => $parameter['office_layer']]);
			$conditions[] = $query->and_(['message_for' => 'office_ministry', 'related_id' => $parameter['office_ministry']]);
			$conditions[] = $query->and_(['message_for' => 'all']);
			return $query->or_($conditions);
		});
		$global_messages['read'] = [];
		$global_messages['unread'] = [];
		$g_messages = $messages->where(['is_deleted' => 0])->limit(20)->order(['created' => 'desc'])->toArray();
		foreach($g_messages as $key => $global_message) {
			$messageViewsTable = TableRegistry::get('MessageViews');
			$messageView = $messageViewsTable->find()->where(['message_id' => $global_message['id'], 'organogram_id' => $parameter['organogram']]);
			if (count($messageView->toArray()) > 0) {
				$global_messages['read'][] = $global_message;
			} else {
				$global_messages['unread'][] = $global_message;
			}

		}
		return $global_messages;
	}

    /**
     *
     * @param array $data_to_encode to encode in token
     * @param array $options optional for token generation
     * @param string sercret_key optional for token generation
     * @param string algorithm optional for token generation
     */
    protected function generateToken($data_to_encode, $options = [], $secretKey = '', $algorithm = 'HS256')
    {
        if(empty($secretKey)){
            $secretKey = SECRET_KEY;
        }
        if(isset($data_to_encode['file'])){
            $data_to_encode['file_access_key'] = $this->makeEncryptedData($data_to_encode['file']);
        }
        $domain_name = str_replace('content/', '', FILE_FOLDER);
        $issuedAt = time();
        $tokenId = base64_encode($issuedAt);
        $notBefore = $issuedAt;
        $expire = $notBefore + 3600 * 24 * 1; // Adding 1 day expiration
        $serverName = $domain_name; /// set your domain name

        /*
         * Create the token as an array
         */
        $data = $options + [
                'iat' => $issuedAt,         // Issued at: time when the token was generated
                'jti' => $tokenId,          // Json Token Id: an unique identifier for the token
                'iss' => $serverName,       // Issuer
                'nbf' => $notBefore,        // Not before
                'exp' => $expire,           // Expire
                'data' => $data_to_encode
            ];

        /// Here we will transform this array into JWT:
        $token = JWT::encode(
            $data, //Data to be encoded in the JWT
            $secretKey, // The signing key
            $algorithm
        );

        return $token;
    }

    protected function getTokenData($token, $secretKey = '', $algorithm = 'HS256')
    {
        try {
            if(empty($secretKey)){
                $secretKey = SECRET_KEY;
            }
            if (defined("TOKEN_CHECK") && TOKEN_CHECK) {
                return json_decode(json_encode(JWT::decode($token, $secretKey, array($algorithm))), true);
            } else {
                return API_KEY;
            }
        } catch (\Exception $ex) {
            return API_KEY;
        }

    }

    protected function checkToken($token)
    {
        if (empty($token))
            return false;
        if (!defined("TOKEN_CHECK") || TOKEN_CHECK == 0) {
            return API_KEY == $token;
        }
//         if($token == API_KEY){
//             return true;
//         }
        $token_data = $this->getTokenData($token);
        if (empty($token_data))
            return false;
        if (empty($token_data['data']))
            return false;
        //expiry time check
        if (empty($token_data['exp']) || $token_data['exp'] < time())
            return false;
        // issuer time check
        if (empty($token_data['nbf']) || $token_data['nbf'] > time())
            return false;
        //issuer domain check
        $domain_name = str_replace('content/', '', FILE_FOLDER);
        if(empty($token_data['iss'])|| $token_data['iss'] !=$domain_name)
            return false;

        return TRUE;
    }

    public function switchOffice($office_id, $officeName, $status = -1, $returnerror = false)
    {

        $officeDomainTable = TableRegistry::get('OfficeDomains');
        $officeDomain = $officeDomainTable->find()->where(['office_id' => $office_id]);
        if ($status != -1) {
            $officeDomain = $officeDomain->where(['status' => $status]);
        }
        $officeDomain = $officeDomain->first();

        if (empty($officeDomain)) {
            if($returnerror){
                return false;
            }
            throw new Exception("অফিস ডাটাবেজ পাওয়া যায় নি! সাপোর্ট টিমের সাথে যোগাযোগ করুন। অফিস আইডিঃ ".$office_id);
        }

        ConnectionManager::drop($officeName);
        ConnectionManager::drop('default');
        ConnectionManager::config($officeName,
            [
                'className' => 'Cake\Database\Connection',
                'driver' => 'Cake\Database\Driver\Mysql',
                'persistent' => false,
                'host' => $officeDomain['domain_host'],
                'username' => $officeDomain['domain_username'],
                'password' => $officeDomain['domain_password'],
                'database' => $officeDomain['office_db'],
                'encoding' => 'utf8',
                'cacheMetadata' => false,
            ]
        );

        ConnectionManager::alias($officeName, 'default');
        if($returnerror){
            return true;
        }
    }

    private function _setupAccessControl(Event $event)
    {
        $user = $this->Auth->user();

        UsermgmtInIt($this);
        $session = $this->request->session();
        $employee_office = $session->read('selected_office_section');
        $is_admin = isset($employee_office['is_admin'])?$employee_office['is_admin']:0;
        $controller = $this->request->params ['controller'];
        $action = $this->request->params ['action'];
        $actionUrl = $controller . '/' . $action;

        $permissionFree = array(
            'officesettings/loadlayersbyministry',
            'officesettings/loadofficesbyministryandlayer',
            'officemanagement/loadoriginoffices',
            'dakonlinenagoriks/nagorik_dak',
            'dakonlinenagoriks/checknid',
            'dakonlinenagoriks/checkbirthcertificate',
            'dakonlinenagoriks/upload',
            'users/login',
            'users/apiLogin',
            'users/logout',
            'userroleactions/access_denied',
            'dashboard/dashboard',
            'users/forgetpassword',
            'users/activation',
            'users/choosepassword',
        );

        if (!in_array(strtolower($actionUrl), $permissionFree) && ($action != 'access_denied')) {
            $userGroupModel = TableRegistry::get('UserRoles');
            if (isset($user['user_role_id']) && !$userGroupModel->isUserGroupAccess($controller,
                    $action, $user['user_role_id'],$is_admin)
            ) {
                return $this->redirect(['controller' => 'userRoleActions','action' => 'access_denied','?' => ['request_url' => urlencode($controller.'-'.$action)]]);
            }
        }
    }

    function mt_rand_str($l, $c = '1234567890ABCDEFGHIJ1234567890ABCDEFGHIJ')
    {
        $s = '';$cl = strlen($c) - 1;
        for ($i = 0; $i < $l; ++$i){
            $s .= $c [mt_rand(0, $cl)];
        };
        return $s;
    }

    private function _pickLayout()
    {
        if ($this->request->url == 'login' || ($this->request->params ['controller']
                == 'Users' && $this->request->params ['action'] == 'login')
        ) {
            $this->layout = "login";
        } else if ($this->request->url == 'forgetpassword' || ($this->request->params ['controller']
                == 'Users' && $this->request->params ['action'] == 'forgetpassword')
        ) {
            $this->layout = "forgetpassword";
        } else if ($this->request->url == 'activation' || ($this->request->params ['controller']
                == 'Users' && $this->request->params ['action'] == 'activation')
        ) {
            $this->layout = "activation";
        } else if ($this->request->url == 'choosepassword' || ($this->request->params ['controller']
                == 'Users' && $this->request->params ['action'] == 'choosepassword')
        ) {
            $this->layout = "choosepassword";
        } else if ($this->request->params ['controller'] == 'DakOnlineNagoriks' && $this->request->params ['action']
            == 'nagorik_dak'
        ) {
            $this->layout = "online_dak";
        } else {
            $this->layout = "default";
        }
    }

    public function _getControllers()
    {
        $files = scandir('../src/Controller/');
        $results = [];
        $ignoreList = [
            '.',
            '..',
            'Component',
            'AppController.php',
        ];
        array_push($results, "All");
        foreach ($files as $file) {
            if (!in_array($file, $ignoreList)) {
                $controller = explode('.', $file)[0];
                array_push($results, $controller);
            }
        }

        return $results;
    }

    public function getMimeType($file)
    {
        $explode = explode('?token=',$file);
        $file = $explode[0];

        return get_file_type($file);
    }

    public function getCurrentDakSection()
    {
        $session = $this->request->session();
        return $session->read('selected_office_section');
    }

    public function getLoggedEmployeeRecord()
    {
        $session = $this->request->session();
        return $session->read('logged_employee_record');
    }

    public function get_full_url()
    {
        $https = !empty($_SERVER['HTTPS']) && strcasecmp($_SERVER['HTTPS'], 'on')
            === 0 ||
            !empty($_SERVER['HTTP_X_FORWARDED_PROTO']) &&
            strcasecmp($_SERVER['HTTP_X_FORWARDED_PROTO'], 'https') === 0;
        return
            ($https ? 'https://' : 'http://') .
            (!empty($_SERVER['REMOTE_USER']) ? $_SERVER['REMOTE_USER'] . '@' : '') .
            (isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : ($_SERVER['SERVER_NAME'] .
                ($https && $_SERVER['SERVER_PORT'] === 443 ||
                $_SERVER['SERVER_PORT'] === 80 ? '' : ':' . $_SERVER['SERVER_PORT']))) .
            substr($_SERVER['SCRIPT_NAME'], 0,
                strrpos($_SERVER['SCRIPT_NAME'], '/'));
    }

    public function EngToBng($value = 0)
    {
        $bn_digits = array('০', '১', '২', '৩', '৪', '৫', '৬', '৭', '৮', '৯');
        return $output = str_replace(range(0, 9), $bn_digits, $value);
    }

    public function BngToEng($value = 0)
    {
        $bn_digits = array('০', '১', '২', '৩', '৪', '৫', '৬', '৭', '৮', '৯');
        return $output = str_replace($bn_digits, range(0, 9), $value);
    }

    public function NotificationSet($type, $options, $notificationtype,
                                    $employee = array(), $toUser = array(),
                                    $subject = '', $msg = '', $replace_msg = 0)
    {
        if (NOTIFICATION_OFF) {
            return true;
        }
        if($notificationtype == 2 && $type == 'forward'){
            $type = 'sent';
        }
        $notificationTable = TableRegistry::get('NotificationMessages');
        return $notificationTable->saveNotification($type, $options,
            $notificationtype, $employee, $toUser, $subject, $msg,
            $replace_msg);
    }

//    protected function SendEmailMessage($emailadd, $message, $subject = '',
//                                        $from = 'অফিস ব্যবস্থাপনা নোটিফিকেশন',
//                                        $fromName = '', $format = 'html',
//                                        $layout = 'default',
//                                        $template = 'notification')
//    {
//        if (NOTIFICATION_OFF) {
//            return true;
//        }
//
//        $email = new Email('default');
//
//        try {
//            $email->emailFormat($format)->from(['nothi@nothi.org.bd' => $from])
//                ->to(trim($emailadd))
//                ->subject($subject)
//                ->viewVars([
////                    'sender'=>$sender,
////                    'subject'=>"",
//                    'notification' => $message,
//                    'notificationTime' => \Cake\I18n\Time::parse(date("Y-m-d H:i:s"))
//                ])
//                ->template($template, $layout)
//                ->sendWithoutException();
//        } catch (Exception $ex) {
//
//            return false;
//        }
//
//        return true;
//    }

    protected function SendEmailMessage($email, $message, $options = [])
    {
        if (NOTIFICATION_OFF) {
            return true;
        }
        //Prepare Variables
        $data['to_email'] = $email;
        $data['to_name'] = !empty($options['to_name']) ? $options['to_name'] : "";
        $data['from_email'] = !empty($options['from_email']) ? $options['from_email'] : "nothi@nothi.org.bd";
        $data['from_name'] = !empty($options['from_name']) ? $options['from_name'] : "অফিস ব্যবস্থাপনা নোটিফিকেশান";;
        $data['format'] = !empty($options['format']) ? $options['format'] : "html";
        $data['template'] = !empty($options['template']) ? $options['template'] : "notification";
        $data['subject'] = !empty($options['subject']) ? $options['subject'] : "";
        $data['body_subject'] = !empty($options['subject']) ? $options['subject'] : "";
        $data['email_body'] = $message;
        $data['layout'] = !empty($options['layout']) ? $options['layout'] : "default";
        $data['viewVars'] = !empty($options['viewVars']) ? $options['viewVars'] : [];
        $data['priority'] = !empty($options['priority']) ? $options['priority'] : 5;
        //
        $data['office_id'] = "";
        $data['office_name'] = "";
        $data['response_url'] = "";
        $data['email_trace_id'] = "";

        //
        $data['service_id'] = !empty($options['email_type']) ? $options['email_type'] : EMAIL_USER;
        try {
            if (defined("EMAILER_ACTIVE") && EMAILER_ACTIVE == 1) {
                $nothi_email = new \App\Model\Entity\NothiEmail();
                $nothi_email->addEmailQueue($data);
            } else {
                $emailer = new Email('default');
                $emailer->emailFormat($data['format'])->from([$data['from_email'] => $data['from_name']])
                    ->to(trim($email))
                    ->subject($data['subject'])
                    ->viewVars($data['viewVars'])
                    ->template($data['template'], $data['layout'])
                    ->sendWithoutException();
            }
            return true;
        } catch (Exception $e) {
            return false;
        }
        return true;
    }

    protected function SendSmsMessage($mobile, $message)
    {

        if (NOTIFICATION_OFF) {
            return true;
        }

        $ms = trim($mobile);
        $txt = $message;
//        $txt = urlencode($txt);

        $ms = bnToen($ms); //convert bangla to english

        if (substr($ms, 0, 3) != '+88') {
            $ms = '+88' . $ms;
        }

        try {
            $SmsRequestTable = TableRegistry::get('SmsRequest');
            $SmsRequestTable->saveData($ms, $txt);
        } catch (\Exception $ex) {
            return false;
        }

        return true;
    }

    public function makeDirectory($path = 'Dak', $office = 0)
    {

        if (!is_dir(FILE_FOLDER_DIR . $path . '/')) {
            mkdir(FILE_FOLDER_DIR . $path . '/', 777, true);
            $content = "<?php die('Digital Bangladesh'); ?>";
            $fp = fopen(FILE_FOLDER_DIR . $path . "/index.php", "wb");
            fwrite($fp, $content);
            fclose($fp);
        }

        //office
        if (!is_dir(FILE_FOLDER_DIR . $path . '/' . $office . '/')) {
            mkdir(FILE_FOLDER_DIR . $path . '/' . $office . '/', 777, true);
            $content = "<?php die('Digital Bangladesh'); ?>";
            $fp = fopen(FILE_FOLDER_DIR . $path . '/' . $office . "/index.php",
                "wb");
            fwrite($fp, $content);
            fclose($fp);
        }

        //year
        if (!is_dir(FILE_FOLDER_DIR . $path . '/' . $office . '/' . date('Y') . '/')) {
            mkdir(FILE_FOLDER_DIR . $path . '/' . $office . '/' . date('Y') . '/', 777,
                true);
            $content = "<?php die('Digital Bangladesh'); ?>";
            $fp = fopen(FILE_FOLDER_DIR . $path . '/' . $office . '/' . date("Y") . "/index.php",
                "wb");
            fwrite($fp, $content);
            fclose($fp);
        }

        //month
        if (!is_dir(FILE_FOLDER_DIR . $path . '/' . $office . '/' . date('Y') . '/' . date('m') . '/')) {
            mkdir(FILE_FOLDER_DIR . $path . '/' . $office . '/' . date('Y') . '/' . date('m') . '/',
                777, true);
            $content = "<?php die('Digital Bangladesh'); ?>";
            $fp = fopen(FILE_FOLDER_DIR . $path . '/' . $office . '/' . date('Y') . '/' . date('m') . "/index.php",
                "wb");
            fwrite($fp, $content);
            fclose($fp);
        }

        //day
        if (!is_dir(FILE_FOLDER_DIR . $path . '/' . $office . '/' . date('Y') . '/' . date('m') . '/' . date('d') . '/')) {
            mkdir(FILE_FOLDER_DIR . $path . '/' . $office . '/' . date('Y') . '/' . date('m') . '/' . date('d') . '/',
                777, true);
            $content = "<?php die('Digital Bangladesh'); ?>";
            $fp = fopen(FILE_FOLDER_DIR . $path . '/' . $office . '/' . date('Y') . '/' . date('m') . '/' . date('d') . "/index.php",
                "wb");
            fwrite($fp, $content);
            fclose($fp);
        }

        return FILE_FOLDER_DIR . $path . '/' . $office . '/' . date('Y') . '/' . date('m') . '/' . date('d') . '/';
    }

    public function checkSubdomain()
    {

        $subdomain = $this->extract_subdomains($_SERVER['HTTP_HOST']);

        if (!empty($subdomain)) {
            return $subdomain;
        }

        return -1;
    }

    protected function extract_domain($domain)
    {
        if (preg_match("/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i",
            $domain, $matches)) {
            return $matches['domain'];
        } else {
            return $domain;
        }
    }

    protected function extract_subdomains($domain)
    {
        $subdomains = $domain;
        $domain = $this->extract_domain($subdomains);

        $subdomains = rtrim(strstr($subdomains, $domain, true), '.');

        return $subdomains;
    }

    protected function checkUndone()
    {

        $return = array();
        $user = $this->getCurrentDakSection();

        $potrojariTable = TableRegistry::get('Potrojari');
        $nothipartTable = TableRegistry::get('NothiParts');
        $nothiMasterCurrentUsersTable = TableRegistry::get('NothiMasterCurrentUsers');

        $nothiPartNos = $nothiMasterCurrentUsersTable->find('list',
            ['keyField' => 'id', 'valueField' => 'nothi_part_no'])->where([
            'NothiMasterCurrentUsers.office_id' => $user['office_id'],
            'NothiMasterCurrentUsers.office_unit_id' => $user['office_unit_id'],
            'NothiMasterCurrentUsers.office_unit_organogram_id' => $user['office_unit_organogram_id'],
            'NothiMasterCurrentUsers.is_archive' => 0
        ])->toArray();

        $draftPotro = $potrojariTable->find()->where(['Potrojari.potro_status' => 'Draft',
            'Potrojari.nothi_part_no IN' => $nothiPartNos, 'Potrojari.can_potrojari' => 1,
            'Potrojari.is_summary_nothi' => 0])->count();

        $draftSummary = $potrojariTable->find()->where(['Potrojari.potro_status' => 'Draft',
            'Potrojari.nothi_part_no IN' => $draftPotro, 'Potrojari.can_potrojari' => 1,
            'Potrojari.is_summary_nothi' => 1])->count();


        $pendingDaktable = TableRegistry::get('Reports');
        $return['PendingDak'] = $pendingDaktable->dakinbox($user['office_id'],
            $user['office_unit_id'], $user['office_unit_organogram_id'],
            'DATEDIFF(NOW(),date(created))>=3');

        $return['DraftPotro'] = 0;
        $return['DraftSummary'] = 0;

        if ($draftPotro > 0) {
            $return['DraftPotro'] = ($draftPotro);
        }
        if ($draftSummary > 0) {
            $return['DraftSummary'] = ($draftSummary);
        }

        return $return;
    }

    public function get_user_photo($username, $encode = 0) {
		$exist = (FILE_FOLDER_DIR . 'Personal/profile/' . $username . '.png');
		if (file_exists($exist)) {
			$file = FILE_FOLDER_DIR . 'Personal/profile/' . $username . '.png';
			$file_url = \Cake\Routing\Router::url('/content/Personal/profile/' . $username . '.png');
		} else {
			$file = ROOT . '/webroot/assets/admin/pages/media/profile/avatar.png';
			$file_url = \Cake\Routing\Router::url('/assets/admin/pages/media/profile/avatar.png');
		}

		if ($encode) {
			$type = pathinfo($file, PATHINFO_EXTENSION);
			$data = file_get_contents($file);
			$base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
			return $base64;
		} else {
			return $file_url;
		}
	}

    protected function getProfile($username, $encode = 0, $top_bar = 1)
    {
        $exist = (FILE_FOLDER_DIR . 'Personal/profile/' . $username . '.png');
        if (file_exists($exist)) {
            $file = FILE_FOLDER_DIR . 'Personal/profile/' . $username . '.png';
        } else {
            $file = ROOT . '/webroot/assets/admin/pages/media/profile/avatar.png';
        }

        if ($encode) {
            $type = pathinfo($file, PATHINFO_EXTENSION);
            $data = file_get_contents($file);
            $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
            if ($top_bar) {
                return $base64;
            } else {
                echo $base64;
                exit;
            }
        } else {
            $this->response->file($file);
            return $this->response;
        }
    }

    protected function validateToken($data,$username){
        $tokenDecode = $this->getTokenDetail($data);
        if (!empty($tokenDecode['data']['file_access_key'])) {
            $decrypt_data = $this->getDecryptedData($tokenDecode['data']['file_access_key']);
            if($username == $decrypt_data){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    protected function getSignature($username, $encode = 1, $needreturn = 0, $validate = false, $date = null,$option=[])
    {
        if(!empty($this->request->query('token'))) {
            $return = $this->validateToken($this->request->query('token'),$username);
            if(!$return){
                die('Invalid request. Err: 01');
            }
        } else if(!empty($option['token'])){
            $return = $this->validateToken($option['token'],$username);
            if(!($return)){
                die('Invalid request. Err: 02');
            }
        } else die('Invalid request. Err: 03');

        try {

            $userSignaturesTable = TableRegistry::get('UserSignatures');

            $signaturePath = $userSignaturesTable->getSignature($username, $date);
            if ($encode) {
                $base64 = !empty($signaturePath['encode_sign']) ? $signaturePath['encode_sign'] : '';
                if (empty($base64)) {
                    //check previous file
                    $file = FILE_FOLDER_DIR . 'Personal/signature/' . $username . '.png';
                    if (file_exists($file)) {
                        if ($validate) {
                            return true;
                        }
                        $type = pathinfo($file, PATHINFO_EXTENSION);
                        $data = file_get_contents($file);
                        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
                    } else {
                        if ($validate) {
                            return false;
                        }
                        $file = FILE_FOLDER_DIR . 'Personal/signature/no-signature.png';
                        $type = pathinfo($file, PATHINFO_EXTENSION);
                        $data = file_get_contents($file);
                        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
                    }
                } else {
                    if ($validate) {
                        return true;
                    }
                }

                if ($needreturn) {
                    return $base64;
                } else {
                    echo $base64;
                    exit;
                }
            } else {
                $exist = !empty($signaturePath['signature_file']) ? $signaturePath['signature_file'] : (FILE_FOLDER_DIR . 'Personal/signature/' . $username . '.png');
                if (file_exists($exist)) {
                    $file = $exist;
                    if ($validate) {
                        return true;
                    }
                } else {
                    $exist = FILE_FOLDER_DIR . 'Personal/signature/' . $username . '.png';
                    if (file_exists($exist)) {
                        $file = $exist;
                        if ($validate) {
                            return true;
                        }
                    } else {
                        if ($validate) {
                            return false;
                        }
                        $file = FILE_FOLDER_DIR . 'Personal/signature/no-signature.png';
                    }
                }
                $this->response->file($file);
                return $this->response;
            }
        } catch (Exception $ex) {
            if ($validate) {
                return false;
            }
            if ($needreturn) {
                return '';
            } else {
                echo '';
                exit;
            }
        } catch (FileNotFoundException $exception) {
            if ($validate) {
                return false;
            }
            if ($needreturn) {
                return '';
            } else {
                echo '';
                exit;
            }
        }
    }

    /*
     * @params: $employee_office, [employee_record_id, office_id, $office_unit_id, $office_unit_organogram_id, office_name, designation]
     * @params: $browswer = [], [platform, name, userAgent, version]
     *
     */
    protected function saveLoginHistory($employee_office, $browser = [],$by_cron = 1)
    {
        try {
            $browser = !empty($browser) ? $browser : getBrowser();
            if (empty($browser['is_mobile'])) {
                $auth_user = $this->Auth->user();
            } else {
                $auth_user = [];
            }
            if($by_cron){
                $time = rand(1,1000).time().'-'.rand(1,1000);
                Cache::write('saveLoginHistory_emp_' . $time, $employee_office, 'memcached');
                Cache::write('saveLoginHistory_browser_' . $time, $browser, 'memcached');
                Cache::write('saveLoginHistory_auth_' . $time, $auth_user, 'memcached');
                // send to cron to save user login histroy
                $command = ROOT . "/bin/cake API saveLoginHistory {$time}> /dev/null 2>&1 &";
//                print_r($command);die;
                exec($command);
                return true;
                // send to cron to save user login histroy
            }
            if (!empty($employee_office)) {
                $Officestbl = TableRegistry::get('Offices');

                $table_login_history = TableRegistry::get('UserLoginHistory');
                $EmployeeRecordsTable = TableRegistry::get('EmployeeRecords');


                if (!empty($browser['ip'])) {
                    $ip = $browser['ip'];
                } else {
                   $ip = getIP();
                }
                if (!empty($browser['is_mobile']) && $browser['is_mobile'] == 1 && !empty($browser['user_id']) && !empty($browser['emp_rcrd_id'])) {
                    // Comes From API
                    $auth_user['employee_record_id'] = $browser['emp_rcrd_id'];
                    $auth_user['id'] = $browser['user_id'];
                } else {
                    $auth_user = $this->Auth->user();
                }

//                pr($auth_user);die;
                $Employee_personal_info = $EmployeeRecordsTable->getEmployeeInfoByRecordId($auth_user['employee_record_id']);
                if (!empty($employee_office)) {
                    foreach ($employee_office as $key => $value) {
                        $loginentity = $table_login_history->newEntity();
                        $allInfo = $Officestbl->find()->select(['office_ministry_id', 'office_origin_id', 'office_layer_id'])->where(['id' => $value['office_id']])->first();

                        $loginentity->client_ip = $ip;
                        $loginentity->employee_record_id = $value['employee_record_id'];
                        $loginentity->office_id = $value['office_id'];
                        $loginentity->office_unit_id = $value['office_unit_id'];
                        $loginentity->office_unit_organogram = $value['office_unit_organogram_id'];
                        $loginentity->office_name = $value['office_name'];
                        $loginentity->unit_name = $value['unit_name'];
                        $loginentity->organogram_name = $value['designation'];
                        $loginentity->login_time = date("Y-m-d H:i:s");
                        $loginentity->device_name = $browser['platform'];
                        $loginentity->browser_name = $browser['name'];
                        $loginentity->is_mobile = isset($browser['is_mobile']) ? $browser['is_mobile'] : 0;

                        $loginentity->user_id = $auth_user['id'];
                        $loginentity->employee_name = isset($Employee_personal_info['name_bng']) ? $Employee_personal_info['name_bng'] : '';
                        $loginentity->employee_email = isset($Employee_personal_info['personal_email']) ? $Employee_personal_info['personal_email'] : NULL;
                        $loginentity->mobile_number = isset($Employee_personal_info['personal_mobile']) ? $Employee_personal_info['personal_mobile'] : NULL;
                        // Check if it is comes from API
                        if (!empty($browser['token']))
                            $loginentity->token = $browser['token'];
                        if (!empty($browser['device_type']))
                            $loginentity->device_type = $browser['device_type'];
                        else
                            $loginentity->device_type = substr($browser['name'] . ' - ' . $browser['version'] . ' - ' . $browser['platform'], 0, 49);
                        if (!empty($browser['device_id']))
                            $loginentity->device_id = $browser['device_id'];
                        else
                            $loginentity->device_id = $ip;
                        // Check if it is comes from API

                        $loginentity->ministry_id = $allInfo['office_ministry_id'];
                        $loginentity->origin_id = $allInfo['office_origin_id'];
                        $loginentity->layer_id = $allInfo['office_layer_id'];

                        $loginentity->network_information = json_encode(array(
                            "ClientIp" => $loginentity->client_ip,
                            "UserAgent" => $browser['userAgent'],
                            "DeviceId" => isset($browser['device_id']) ? $browser['device_id'] : (isset($value['fingerprint']) ? $value['fingerprint'] : ''),
                            "NetworkName" => isset($browser['network_name']) ? $browser['network_name'] : '',
                            "Browser" => $browser['name'],
                            "Version" => $browser['version'],
                            "Device" => $browser['platform'],
                            "fingerprint" => isset($value['fingerprint']) ? $value['fingerprint'] : '',
                            "Time" => date("Y-m-d H:i:s"),
                        ));

                        $table_login_history->save($loginentity);
                    }
                }
            }
            return true;
        } catch (\Exception $ex) {
            return $ex->getMessage();
        }
        return false;
    }

    public function generatePassword()
    {
        $n = '123456789';
        $c = '!@#$%^&*()-=+_~';
        $sm = 'abcdefghijklmnopqrstuvwxyz';
        $password = '';
        for ($s = '', $cl = strlen($n) - 1, $i = 0; $i < 2; $s .= $n [mt_rand(0, $cl)], ++$i)
            ;
        $password .= $s;
        for ($s = '', $cl = strlen($c) - 1, $i = 0; $i < 1; $s .= $c [mt_rand(0, $cl)], ++$i)
            ;
        $password .= $s;
        for ($s = '', $cl = strlen($sm) - 1, $i = 0; $i < 3; $s .= $sm [mt_rand(0, $cl)], ++$i)
            ;
        $password .= $s;
        return $password;
    }

    protected function saveMobileLoginHistory($employee_office, $browser = [])
    {
        $browser['is_mobile'] = 1;
        return $this->saveLoginHistory($employee_office, $browser);
    }

    protected function updateLoginHistory($employee_id)
    {
        set_time_limit('0');
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        try {
//            $table_instance_emp_offices = TableRegistry::get('EmployeeOffices');
//
//            $employee_office_records = $table_instance_emp_offices->getEmployeeOfficeRecords($employee_id);
            $table_login_history = TableRegistry::get('UserLoginHistory');
            $table_login_history->updateLogoutbyEmployeeId($employee_id, $ip);

//            if (!empty($employee_office_records)) {
//                foreach ($employee_office_records as $key => $value) {
//                    $table_login_history->updateLogout($value['office_unit_organogram_id'],
//                        $ip);
//                }
//            }

            return true;
        } catch (\Exception $ex) {
            return $ex->getMessage();
        }
        return false;
    }

    public function dateDiff($date, $mydate = '')
    {

        if (empty($mydate)) {
            $mydate = date("Y-m-d H:i:s");
        }
        $theDiff = "";
        $datetime1 = date_create($date);
        $datetime2 = date_create($mydate);
        $interval = date_diff($datetime1, $datetime2);
        $min = $interval->format('%i');
        $sec = $interval->format('%s');
        $hour = $interval->format('%h');
        $mon = $interval->format('%m');
        $day = $interval->format('%d');
        $year = $interval->format('%y');
        if ($interval->format('%i%h%d%m%y') == "00000") {
            return Number::format($sec) . " সেকেন্ড";
        } else if ($interval->format('%h%d%m%y') == "0000") {
            return Number::format($min) . " মিনিট";
        } else if ($interval->format('%d%m%y') == "000") {
            return Number::format($hour) . " ঘণ্টা";
        } else if ($interval->format('%m%y') == "00") {
            return Number::format($day) . " দিন";
        } else if ($interval->format('%y') == "0") {
            return Number::format($mon) . " মাস";
        } else {
            return Number::format($year) . " বছর";
        }
    }

    public function sendCronMessage($to, $cc, $subject = '',
                                    $from = 'নথি', $message,
                                    $format = 'html', $layout = 'default',
                                    $template = 'notification', $body_subject = '')
    {
        $command = ROOT . "/bin/cake cronjob sendCronMessage {$to} {$cc} {$subject} {$from} {$message} {$format} {$layout} {$template} {$body_subject}> /dev/null 2>&1 &";
        exec($command);

        return true;
        $email = new Email('default');

        try {
            $email->emailFormat($format)->from(['nothi@nothi.org.bd' => $from])
                ->to($to)
                ->addCc($cc)
                ->subject($subject)
                ->viewVars([
                    'notification' => $message,
                    'notificationTime' => \Cake\I18n\Time::parse(date("Y-m-d H:i:s")),
                    'subject' => $subject,
                    'body_subject' => !empty($body_subject) ? $body_subject : $subject,
                ])
                ->template($template, $layout)
                ->send();
        } catch (\Exception $ex) {

            return $ex->getMessage();
        }

        return true;
    }

    protected function checkNID($birthDate = '', $NID = '')
    {
        $NID = bnToen($NID);
        if (!empty($birthDate) && !empty($NID)) {
            $NID_first_four_char = mb_substr($NID, 0, 4);
            $birthDate_first_four_char = bnToen(mb_substr($birthDate, 0, 4));
            if ($NID_first_four_char == $birthDate_first_four_char) {
                $district_code = mb_substr($NID, 4, 2);
                $upozila_thana_code = mb_substr($NID, 7, 2);
                $upozila_data = TableRegistry::get('GeoUpazilas')->find()->select(['district_bbs_code'])->where(['bbs_code' => $upozila_thana_code])->toArray();
                $thana_data = TableRegistry::get('GeoThanas')->find()->select(['district_bbs_code'])->where(['bbs_code' => $upozila_thana_code])->toArray();
                $upozila_thana_data = array_merge($upozila_data, $thana_data);
                if (empty($upozila_thana_data)) {
                    return 1;
                } else {
                    $f = 0;
                    foreach ($upozila_thana_data as $upozila_thana_val) {
                        if ($district_code == $upozila_thana_val['district_bbs_code']) {
                            $f = 1;
                            break;
                        }
                    }
                    if ($f == 0) {
//                      Upozila or Thanas district_bbs_code ID mismatch with District code given in NID
                        return 2;
                    }
                }
            } else {
//                 BirthDate and NID first Four Digit mismatch
                return 3;
            }
            $RMO = [1, 2, 3, 4, 5, 9];
//                pr(mb_substr($NID, 6, 1));
            if (!in_array(mb_substr($NID, 6, 1), $RMO)) {
//               RMO is not 1 to 5 and 9.
//                die;
                return 4;
            }
        } else {
//            NID or Birth Date empty
            return 5;
        }

//        echo " ok";
        return 0;
    }

    public function notifyAppUser($username = '', $employee_record_id = '', $data_array = [], $notification_array = [])
    {
        if (NOTIFICATION_OFF) {
            return true;
        }
        if (empty($username) && empty($employee_record_id)) {
            return true;
        }
        if (!empty($username)) {
            $fcm_data = TableRegistry::get('ApiTokens')->checkByUserName($username);
        } else if (!empty($employee_record_id)) {
            $fcm_data = TableRegistry::get('ApiTokens')->checkByEmployeeRecordId($employee_record_id);
        }
        if (empty($fcm_data)) return true;


        $registrationIds = $fcm_data['fcm_token'];

// prep the bundle
        $data_body = isset($data_array['body']) ? $data_array['body'] : '';
        $data_title = isset($data_array['title']) ? $data_array['title'] : '';
        $msg = array
        (
            'body' => $data_body,
            'title' => $data_body,
        );
        $notify_body = isset($notification_array['body']) ? $notification_array['body'] : '';
        $notify_title = isset($notification_array['title']) ? $notification_array['title'] : '';
        $notify = array
        (
            'body' => $notify_body,
            'title' => $notify_title,
            'vibrate' => 1,
            'sound' => 1
        );
        $fields = array
        (
            'to' => $registrationIds,
            'data' => $msg,
            'notification' => $notify
        );

        $headers = array
        (
            'Authorization: key=AAAA4Bwf3WA:APA91bEnsNx36kB1GdUmj1RtHMgihbIKgpcVs2A23WNp54-BXn12dHkGR_jSf3oIKaAOFyRkKBJtADyIy81wKdQewISpUW3JujOThLWUjVJ9QNNCBl_6n7aim-qrUFYAyFytQQin654M',
            'Content-Type: application/json'
        );
        try {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            $result = curl_exec($ch);
            curl_close($ch);
            $re_array = json_decode($result, true);
            if ($re_array['success'] > 0) {
                return true;
            }
        } catch (\Exception $ex) {
            return false;
        }
        return true;
    }

    protected function printPdf($data = [], $header = [], $pdf_options = []){

        $thead = '';
        $tbody = '';
        if(!empty($header)){

            $thead.= '<thead><tr>';
                foreach ($header as $key => $headTitle){
                    $thead.= '<th>'.$headTitle["title"].'</th>';
                }
            $thead.= '</tr></thead>';
        }

        if(!empty($data)){
            $tbody .= '<tbody>';
            foreach($data as $row => $value){
                $tbody .= '<tr>';
                    foreach ($header as $col => $headTitle) {
                        $tbody.= '<td>'.$value[$headTitle["key"]].'</td>';
                    }
                $tbody .= '</tr>';

            }
            $tbody .= '</tbody>';
        }

        $titles='';
        if(!empty($pdf_options['title'])){
            foreach ($pdf_options['title'] as $title)
            $titles.= '<h3>'.$title.'</h3>';
        }

//        if(!Live ){
//            $background_image = 'background-image: url("'.WWW_ROOT. 'img/test-background.png'.'");background-repeat: no-repeat;background-position:center;';
//        }else{
            $background_image = '';
//        }
        $html ='<!DOCTYPE html>
<html>
<head>
    <style>
        body{
                
              '.$background_image.'
            }
            
            *{
                font-family: Nikosh, SolaimanLipi, "Open Sans", sans-serif !important;
            }
            
        
        tfoot { display: table-row-group}
        tr { page-break-inside: avoid}
        th { min-height: 1.1cm; }
    
        h3{
            text-align: center;
        }
        
        table {
            border-collapse: collapse;
            width: 100%;
            font-size: 11px !important;
        }

        table td{
            border: 1px solid #ddd;
            padding: 2px 5px;
        }

        table tr:nth-child(even){background-color: #f2f2f2;}
        table th {
            border: 1px solid #ddd;
            padding-top: 2px;
            padding-bottom: 2px;
            text-align: left;
            background-color: lightgrey;
            color: Black;
        }
    </style>
</head>
<body>
'.$titles.'
<table>
    '.$thead.'
    '.$tbody.'

</table>

</body>
</html>';

        if(defined("TESTING_SERVER") && TESTING_SERVER) {
            $optionsMerge['footer-html'] = Router::url(['controller' => 'Potrojari', 'action' => 'potrojariDynamicFooter'], true);
        }else {
            $s = (defined("Live") && Live)?'s':'';
            $optionsMerge['footer-html'] = "http://localhost/potrojariDynamicFooter";
        }

        $options = $optionsMerge +  array(
                'no-outline', // option without argument
                'disable-external-links',
                'disable-internal-links',
                'disable-forms',
                'orientation' => !empty($pdf_options['orientation'])?$pdf_options['orientation']:'landscape', //portrait or landscape
                'encoding' => 'UTF-8', // option with argument
                'binary' => ROOT . DS . 'vendor' . DS . 'profburial' . DS . 'wkhtmltopdf-binaries-centos6' . DS . 'bin' . DS . PDFBINARY,
                'user-style-sheet' => WWW_ROOT . DS . 'assets' . DS . 'global' . DS . 'plugins' . DS . 'bootstrap' . DS . 'css' . DS . 'bootstrap.min.css',
                'margin-top'=>10,
                'margin-bottom'=>10,
//            'footer-left'=>isset($pdf_options['title'])?$pdf_options['title']:'রিপোর্ট',
            );
        $pdf = new Pdf($options);
        $pdf->addPage($html);
        $pdf->ignoreWarnings = true;


// Save the PDF


        if(isset($pdf_options['response_type']) && $pdf_options['response_type'] == 'save_pdf') {
            echo 'this con';
            if (!$pdf->saveAs(isset($pdf_options['name'])?$pdf_options['name'].'.pdf':'report.pdf')) {
                $error = $pdf->getError();
                echo $pdf->getError();
                // ... handle error here
            }
        }


// ... or send to client for inline display
        if(isset($pdf_options['response_type']) && $pdf_options['response_type']=='inline_display') {
            if (!$pdf->send()) {
                $error = $pdf->getError();
                // ... handle error here
            }
        }

// ... or you can get the raw pdf as a string
        if(isset($pdf_options['response_type']) && $pdf_options['response_type']=='pdf_string') {
            $content = $pdf->toString();
        }

// ... or send to client as file download
        if(!isset($pdf_options['response_type']) || $pdf_options['response_type']=='download') {
            if (!$pdf->send(isset($pdf_options['name'])?$pdf_options['name'].'.pdf':'report.pdf')) {
                $error = $pdf->getError();
//             ... handle error here
            }
        }
    }

    protected function printExcel($data = [], $header = [], $options = [])
    {
        try {
            set_time_limit('0');
            ini_set('memory_limit', '-1');
            require ROOT . DS . 'vendor' . DS . 'phpoffice' . DS . 'phpexcel' . DS . 'Classes' . DS . 'PHPExcel.php';
            require ROOT . DS . 'vendor' . DS . 'phpoffice' . DS . 'phpexcel' . DS . 'Classes' . DS . 'PHPExcel' . DS . 'IOFactory.php';

            $book = new \PHPExcel();
            $title = isset($options['name']) ? $options['name'] : 'excel';
            $book->getActiveSheet()->setTitle('Sheet 1');
            $sheet = $book->getActiveSheet();

            $style = array(
                'font' => array('bold' => true, 'size' => 12),
                'alignment' => array('horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,),
            );
            $cur_index = 1;
            if (!empty($header)) {
                $t_header = count($header);
//                if(!empty($options['name'])){
//
//                    $sheet->setCellValueByColumnAndRow(0,$cur_index, $options['name']);
//                    $sheet->mergeCellsByColumnAndRow(0,$cur_index,$t_header,$cur_index);
//                    $sheet->getStyleByColumnAndRow(0, $cur_index)->applyFromArray($style);
//                    $cur_index++;
//                }
                if (!empty($options['title'])) {
                    foreach ($options['title'] as $titles) {
                        $sheet->setCellValueByColumnAndRow(0, $cur_index, $titles);
                        $sheet->mergeCellsByColumnAndRow(0, $cur_index, $t_header, $cur_index);
                        $sheet->getStyleByColumnAndRow(0, $cur_index)->applyFromArray($style);
                        $cur_index++;
                    }

                }
                foreach ($header as $key => $headTitle) {
                    $sheet->setCellValueByColumnAndRow($key, $cur_index, $headTitle['title']);
                }
                $cur_index++;
            }

            if (!empty($data)) {
                foreach ($data as $row => $value) {
                    foreach ($header as $col => $headTitle) {
                        $sheet->setCellValueByColumnAndRow($col, $cur_index, ($headTitle['key'] == 'si' ? Number::format($row + 1) : (isset($value[$headTitle['key']]) ? $value[$headTitle['key']] : '')));

                    }
                    $cur_index++;
                }
            }
            //footer settings
            if (!empty($options['footer'])) {
                foreach ($options['footer'] as $footers) {
                    foreach ($footers as $col => $footer) {
                        $sheet->setCellValueByColumnAndRow($col, $cur_index, $footer);
                        $sheet->getStyleByColumnAndRow($col, $cur_index)->applyFromArray($style);
                    }
                    $cur_index++;
                }
            }


            header("Content-Type: application/vnd.ms-excel");
            header("Content-Disposition: attachment; filename=\".$title.xls\"");
            header("Cache-Control: max-age=0");
            $writer = \PHPExcel_IOFactory::createWriter($book, 'Excel2007');

            if (ob_get_contents()) ob_end_clean();
            //ob_end_clean();

            $writer->save('php://output');
            exit;
        } catch (\Exception $ex) {

        }
    }

    public function setCurrentDakSection($designation = null)
    {
        $table = TableRegistry::get('EmployeeOffices');

        return $table->setCurrentSection($designation);
    }

    function cleanInput($input)
    {

        $search = array(
            '@<script[^>]*?>.*?</script>@si',   // Strip out javascript
            '@<[\/\!]*?[^<>]*?>@si',            // Strip out HTML tags
            '@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly
            '@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments
        );
        $output = preg_replace($search, '', $input);
        return $output;
    }

    public function offices2ShowTemplateModals()
    {
        $rmModalOption = jsonA(COAST_GUARD_TEMPLATE);
        return $rmModalOption;
    }

    protected function getTokenDetail($token, $secretKey = '', $algorithm = 'HS256')
    {
        if(empty($secretKey)){
            $secretKey = SECRET_KEY;
        }
        try {
            return json_decode(json_encode(JWT::decode($token, $secretKey, array($algorithm))), true);
        } catch (\Exception $ex) {
            return $ex->getMessage();
        }

    }

    protected function makeEncryptedData($string= '',$options=[]){
       return makeEncryptedData($string,$options);
    }

    protected function getDecryptedData($encrypt_string= '',$options = []){
        return getDecryptedData($encrypt_string,$options);
    }

    function check_base64_image($data) {

        $img = imagecreatefromstring(base64_decode($data));

        if (!$img) {
            return false;
        }
        $size = getimagesizefromstring(base64_decode($data));

        if (!$size || $size[0] == 0 || $size[1] == 0 || !$size['mime']) {
            return false;
        }

        return true;
    }

    public function resizeFiles($destination, $maxHeight = 80, $maxWidth = 180)
    {
        list($width, $height, $type, $attr) = getimagesize($destination);

        if (($maxWidth > 0 && $width > $maxWidth) || $height > $maxHeight) {
            $target_filename = $destination;
            $fn = $destination;
            $size = getimagesize($fn);
            $ratio = $size[0] / $size[1]; // width/height
            if ($ratio > 1) {
                if($maxWidth){$width = $maxWidth;}
                $height = $maxHeight / $ratio;
            } else {
                if($maxWidth){$width = $maxWidth * $ratio;}
                $height = $maxHeight;
            }
            $src = imagecreatefromstring(file_get_contents($fn));
            $dst = imagecreatetruecolor($width, $height);
            imagecopyresampled($dst, $src, 0, 0, 0, 0, $width, $height,
                $size[0], $size[1]);
            imagedestroy($src);
            imagepng($dst, $target_filename); // adjust format as needed
            imagedestroy($dst);
        }
    }

    public function resizeFilesHeight($destination, $maxHeight = 80, $maxWidth = 180){
        list($width, $height, $type, $attr) = getimagesize($destination);

        if (($maxWidth > 0 && $width > $maxWidth) || $height > $maxHeight) {
            $target_filename = $destination;
            $fn = $destination;
            $size = getimagesize($fn);
            //$ratio = $size[0] / $size[1]; // width/height

            if($size[1]>$maxHeight) {
                $height = $maxHeight;
            }

            $src = imagecreatefromstring(file_get_contents($fn));
            $dst = imagecreatetruecolor($width, $height);
            imagecopyresampled($dst, $src, 0, 0, 0, 0, $width, $height,
                $size[0], $size[1]);
            imagedestroy($src);
            imagepng($dst, $target_filename); // adjust format as needed
            imagedestroy($dst);
        }
    }


    public function checkProtikolpo($employee_record_id) {
        $protikolpo_settings_table = TableRegistry::get('protikolpo_settings');
        $protikolpo_settings = $protikolpo_settings_table->find()->where(['employee_record_id' => $employee_record_id, 'active_status' => 1])->count();
        if ($protikolpo_settings > 0) {
            $employee_offices_table = TableRegistry::get('employee_offices');
            $employee_offices = $employee_offices_table->find()->where(['employee_record_id' => $employee_record_id, 'status' => 1])->count();
            if ($this->request->params['controller'] == 'Users' && $this->request->params['action'] == 'protikolpoRevert') {
                return true;
            }
            if ($this->request->params['controller'] == 'ProtikolpoSettings' && $this->request->params['action'] == 'updateEndDate') {
                return true;
            }
            if ($this->request->params['controller'] == 'ProtikolpoSettings' && $this->request->params['action'] == 'stopProtikolpo') {
                return true;
            }
            if ($employee_offices > 0) {
                $this->Flash->error(__('আপনি আপনার পদে ফেরত আসতে চাইলে প্রতিকল্প ব্যবস্থাপনা বাতিল করুন।'));
                return $this->redirect(['action' => 'protikolpoRevert', 'controller' => 'Users']);
            } else {
                $this->Flash->error(__('আপনি আপনার পদে ফেরত আসতে চাইলে প্রতিকল্প ব্যবস্থাপনা বাতিল করুন। অন্যথায় লগআউট করুন।'));
                return $this->redirect(['action' => 'protikolpoRevert', 'controller' => 'Users']);
            }
        }
    }

    public function notifyFeedback($url = null, $data = null,$wait = 1)
    {

        try{
            if (empty($url) || empty($data)) {
                throw new \Exception(__("Invalid request"));
            }
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            if(!empty($wait)){
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            }else{
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, false);
            }
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            $data = json_encode($data);

            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            $response = curl_exec($ch);
            curl_close($ch);
            $result = [
                'status' => 'success',
                'response' => $response,
            ];
            file_put_contents(ROOT. '/logs/feedback.json',$data);
        }catch(\Exception $ex){
            $result = [
                'status' => 'error',
                'message' => $ex->getMessage(),
            ];
        }


        return $result;
    }
    protected function switchOfficeWithStatus($office_id, $officeName, $status = -1, $returnerror = false)
    {
        $response = [
            'status' => 'error',
            'message' => 'Something went wrong'
        ];
        try{
            if($this->switchOffice($office_id,$officeName,$status,$returnerror) === true){
                $response = [
                    'status' => 'success',
                    'message' => 'Database: '.$office_id.' connected successfully',
                ];
            }

        }catch (Exception $ex){
            $response = [
                'reason' => $this->makeEncryptedData($ex),
                'message' => __('Technical error happen'),
            ];
        }
        rtn:
        return $response;

    }
    protected function checkCurrentOfficeDBConnection($tableName = 'NothiMasterCurrentUsers')
    {
        $response = [
            'status' => 'error',
            'message' => 'Something went wrong'
        ];
        try{
            $conn = ConnectionManager::get('default');
            $conn->connect();
            TableRegistry::remove($tableName);
            TableRegistry::get($tableName);
            $response = [
                'status' => 'success',
                'message' => 'Database connected successfully',
            ];
        }catch (\Exception $ex){
            $response = [
                'status' => 'error',
                'reason' => $this->makeEncryptedData($ex->getMessage()),
                'message' => __('Technical error happen'),
            ];
        }
        rtn:
        return $response;
    }

    protected function checkAuthorization($token){
        $ip[] = $this->request->clientIp();
        $ip[] = getIP();
        $table_nothi_access_token = TableRegistry::get('ApiAccessToken');
        $checkToken = $table_nothi_access_token->checkToken($token, $ip);
        if (isset($checkToken['status']) && $checkToken['status'] === true) {
            return true;
        }
        return false;
    }
    protected function digitalSigantureUrlChecker(){
        header('Access-Control-Allow-Origin: *');
        $domain_name = str_replace('content/', '', FILE_FOLDER);
        if(strpos($domain_name,$this->request->host()) === FALSE){
            die('Invalid Request. Domain mismatch.Host - '.$this->request->host());
        }
    }
    protected function makeQRCode($data = '',$options = ['format' => 'all']){
        $this->loadComponent('QRGenerator');
        return $this->QRGenerator->generateQRcode($data,$options);
    }
    protected function getMainDomainUrl(){
       return str_replace('content/', '', FILE_FOLDER);
    }
    protected function NotifiyFeedbackAuthTokenGeneration($url,$data = ''){
        $response = [
            'status' => 'error',
            'message' => 'Something went wrong'
        ];
        try{
           if(!empty($url)){
               $auth_token_response = curlRequest($url,$data);
               if(!empty($auth_token_response)){
                   $auth_token_response = jsonA($auth_token_response);
                   if(!empty($auth_token_response['status_code']) && ($auth_token_response['status_code'])== 200 && !empty($auth_token_response['token'])){
                       $response = [
                           'status' => 'success',
                           'token' => $auth_token_response['token']
                       ];
                   }
               }
           }
        }catch (Exception $ex){
            $response = [
                'reason' => $this->makeEncryptedData($ex),
                'message' => __('Technical error happen'),
            ];
        }
        rtn:
        return $response;
    }
}