<?php

namespace App\Controller;

use App\Controller\ProjapotiController;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
use Cake\I18n\Time;

class ProtikolpoSettingsController extends ProjapotiController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $employee_offices = $this->getCurrentDakSection();
        if ($employee_offices['is_admin'] != 1) {
            $this->Auth->deny();
        }
    }

    public function setProtikolpoSettings()
    {
        $employee_offices = $this->getCurrentDakSection();
        if ($employee_offices['is_admin'] == 1) {
            $ProtikolpoSettingsTable = TableRegistry::get('ProtikolpoSettings');
            if ($this->request->is('post') && !empty($this->request->data['data'])) {
                $user = $this->Auth->user();
                if (!empty($this->request->data['data'])) {
                    $ProtikolpoSettingsTable->deleteAll(['office_id' => $employee_offices['office_id']]);
                    foreach ($this->request->data['data'] as $row) {
                        $protikolpo_setting = $ProtikolpoSettingsTable->newEntity();
                        $protikolpo_setting->office_id = $row['office_id'];
                        $protikolpo_setting->unit_id = $row['unit_id'];
                        $protikolpo_setting->designation_id = $row['designation_id'];
                        $protikolpo_setting->employee_record_id = 0;
                        $protikolpossetting = [];
                        if($row['protikolpos']['protikolpo_1']['designation_id']>0) {
                            $protikolpossetting['protikolpo_1']['office_id'] = $row['protikolpos']['protikolpo_1']['office_id'];
                            $protikolpossetting['protikolpo_1']['employee_record_id'] = $row['protikolpos']['protikolpo_1']['employee_record_id'];
                            $protikolpossetting['protikolpo_1']['office_unit_id'] = $row['protikolpos']['protikolpo_1']['office_unit_id'];
                            $protikolpossetting['protikolpo_1']['designation_id'] = $row['protikolpos']['protikolpo_1']['designation_id'];
                            $protikolpossetting['protikolpo_1']['other_officer_info'] = $row['protikolpos']['protikolpo_1']['office_id'] != $employee_offices['office_id'] ? $row['protikolpos']['protikolpo_1']['other_officer_info'] : '';
                        }
                        if($row['protikolpos']['protikolpo_2']['designation_id']>0) {
                            $protikolpossetting['protikolpo_2']['office_id'] = $row['protikolpos']['protikolpo_2']['office_id'];
                            $protikolpossetting['protikolpo_2']['employee_record_id'] = $row['protikolpos']['protikolpo_2']['employee_record_id'];
                            $protikolpossetting['protikolpo_2']['office_unit_id'] = $row['protikolpos']['protikolpo_2']['office_unit_id'];
                            $protikolpossetting['protikolpo_2']['designation_id'] = $row['protikolpos']['protikolpo_2']['designation_id'];
                            $protikolpossetting['protikolpo_2']['other_officer_info'] = $row['protikolpos']['protikolpo_2']['office_id'] != $employee_offices['office_id'] ? $row['protikolpos']['protikolpo_2']['other_officer_info'] : '';
                        }
                        $protikolpo_setting->protikolpos = json_encode($protikolpossetting);
                        $protikolpo_setting->active_status = 0;
                        $protikolpo_setting->is_show_acting = 0;
                        $protikolpo_setting->selected_protikolpo = 0;
                        $protikolpo_setting->created_by = $user['id'];
                        $protikolpo_setting->modified_by = $user['id'];
                        $ProtikolpoSettingsTable->save($protikolpo_setting);

                    }
                }
                $this->response->body(json_encode([
                    'status' => 'success'
                ]));
                $this->response->type('json');
                return $this->response;

            }
            $previous_settings = $ProtikolpoSettingsTable->find()
                ->where(['office_id' => $employee_offices['office_id'],'active_status'=>0])
                ->toArray();
            $formatted_previous_settings = array();
            if(!empty($previous_settings)) {
                foreach ($previous_settings as $previous_setting) {
                    $protikolpos = json_decode($previous_setting['protikolpos'], true);
                    if(!empty($protikolpos['protikolpo_1']))$formatted_previous_settings[$previous_setting['designation_id']]['protikolpo_1_designation_id'] = $protikolpos['protikolpo_1']['designation_id'];
                    if(!empty($protikolpos['protikolpo_1']['other_officer_info'])){
                        $formatted_previous_settings[$previous_setting['designation_id']]['protikolpo_1_other_designation_info']=
                            array('text'=>$protikolpos['protikolpo_1']['other_officer_info'],'value'=>$protikolpos['protikolpo_1']['designation_id'],'data-employee-record-id'=>$protikolpos['protikolpo_1']['employee_record_id'],'data-office-unit-id'=>$protikolpos['protikolpo_1']['office_unit_id'],'data-office-unit-organogram-id'=>$protikolpos['protikolpo_1']['designation_id'],'data-office-id'=>$protikolpos['protikolpo_1']['office_id'], 'data-other-office'=>'true');
                    }

                    if(!empty($protikolpos['protikolpo_2']))$formatted_previous_settings[$previous_setting['designation_id']]['protikolpo_2_designation_id'] = $protikolpos['protikolpo_2']['designation_id'];
                    if(!empty($protikolpos['protikolpo_2']['other_officer_info'])){
                        $formatted_previous_settings[$previous_setting['designation_id']]['protikolpo_2_other_designation_info']=
                            array('text'=>$protikolpos['protikolpo_2']['other_officer_info'],'value'=>$protikolpos['protikolpo_2']['designation_id'],'data-employee-record-id'=>$protikolpos['protikolpo_2']['employee_record_id'],'data-office-unit-id'=>$protikolpos['protikolpo_2']['office_unit_id'],'data-office-unit-organogram-id'=>$protikolpos['protikolpo_2']['designation_id'],'data-office-id'=>$protikolpos['protikolpo_2']['office_id']);
                    }
                }
            }
            $employee_offices_table = TableRegistry::get('EmployeeOffices');
            $allOrganogram = $employee_offices_table->getEmployeeOfficeRecordsWithOfficeUnitAndDesignationOfficeId($employee_offices['office_id'])->hydrate(false)->toArray();
            $row_options = array();

            $this->set('formatted_previous_settings', $formatted_previous_settings);
            $this->set('row_options', $row_options);
            $this->set(compact('allOrganogram'));
        } else {
            $this->Flash->error("দুঃখিত! আপনি অনুমতিপ্রাপ্ত নন। ");
            $this->redirect(['controller' => 'Dashboard', 'action' => 'dashboard']);
        }
    }

    private function getDesignationByOfficerId($officer_id){
            $employee_offices_table = TableRegistry::get('employee_offices');
            return $employee_offices_table->find('list',['keyField'=>'office_unit_organogram_id','valueField'=>'office_unit_organogram_id'])->where(['employee_record_id'=>$officer_id,'status'=>1])->toArray();
    }

    public function getProtikolpoSettings()
    {
        if ($this->request->is('ajax')) {
            $this->layout = null;
        }
        $employee_offices = $this->getCurrentDakSection();
        $ProtikolpoSettingsTable = TableRegistry::get('ProtikolpoSettings');
        $employee_offices_table = TableRegistry::get('EmployeeOffices');

        $all_designations = $employee_offices_table->find()->where(['employee_record_id' => $employee_offices['officer_id'], 'status' => 1])
            ->order(['incharge_label ASC, designation_level ASC, designation_sequence ASC'])->toArray();

        $protikolpo_settings = [];

        foreach ($all_designations as $row => $designation){
            $protikolpo_setting = $ProtikolpoSettingsTable->find()
                ->where(['designation_id' => $designation['office_unit_organogram_id'],'active_status'=>0])->first();

            if($protikolpo_setting){
                $protikolpos = json_decode($protikolpo_setting['protikolpos'], true);
                $designations1 = !empty($protikolpos['protikolpo_1'])?$protikolpos['protikolpo_1']['designation_id']:0;
                $designations2 = !empty($protikolpos['protikolpo_2'])?$protikolpos['protikolpo_2']['designation_id']:0;
                
                if($protikolpo_setting['selected_protikolpo']==1){
                    $protikolpo_info =$employee_offices_table->getDesignationDetailsInfo($designations1);
                } else if($protikolpo_setting['selected_protikolpo']==2){
                    $protikolpo_info =$employee_offices_table->getDesignationDetailsInfo($designations2);
                }
                if (isset($protikolpo_info)) {
                    $protikolpo_setting['protikolpo_info'] = $protikolpo_info['EmployeeRecords']['name_bng'] . ', ' . $protikolpo_info['designation_label'];
                }
                if(!empty($designations1)) {
                    $designationArray[] = $designations1;
                }
                if(!empty($designations2)) {
                    $designationArray[] = $designations2;
                }
                $protikolpo_setting['protikolpo_1']= !empty($protikolpos['protikolpo_1'])?$protikolpos['protikolpo_1']:[];
                $protikolpo_setting['protikolpo_2']=  !empty($protikolpos['protikolpo_2'])?$protikolpos['protikolpo_2']:[];
            }else{
                $protikolpo_setting['protikolpo_1']= [];
                $protikolpo_setting['protikolpo_2']=  [];
            }
            $protikolpo_setting['designation_id']= $designation['office_unit_organogram_id'];
            $protikolpo_setting['designation_name_bng'] = $designation['designation'];

            $protikolpo_settings[] = $protikolpo_setting;
        }

        $this->set('protikolpo_settings',$protikolpo_settings);
    }

    public function setLeaveDate()
    {
        if ($this->request->is('post') && !empty($this->request->data)) {
            $ProtikolpoSettingsTable = TableRegistry::get('ProtikolpoSettings');
            $EmployeeOfficesTable = TableRegistry::get('EmployeeOffices');
            $result = array();
            $employee_office = $this->getCurrentDakSection();
            $designation_ids = $this->getDesignationByOfficerId($employee_office['officer_id']);
            $this->request->data['start_date'] = date('Y-m-d',strtotime($this->request->data['start_date']));
            $this->request->data['end_date'] = date('Y-m-d',strtotime($this->request->data['end_date']));
            if($this->request->data['start_date'] > $this->request->data['end_date']){
                $this->response->body(json_encode(['status'=>'error','data'=>'invalid date range']));
                $this->response->type('json');
                return $this->response;
            }

            foreach ($designation_ids as $key=>$designation_id) {
                $protikolpo_info = $ProtikolpoSettingsTable->find()
                    ->where([
                        'designation_id' => $designation_id,'active_status'=>0])
                    ->first();
                if(empty($protikolpo_info)){
                    $result[$designation_id]['protikolpo_1']=3;
                    continue;
                }
                $protikolpos = json_decode($protikolpo_info['protikolpos'],true);
                if(empty($protikolpos['protikolpo_1']['designation_id']) || $protikolpos['protikolpo_1']['designation_id'] == $employee_office['office_unit_organogram_id']){
                    $result[$designation_id]['protikolpo_1']=0;
                } else {
                    $available = $ProtikolpoSettingsTable->find()->where(['designation_id' => $protikolpos['protikolpo_1']['designation_id'],'DATE(start_date) <= "' . $this->request->data['start_date'] . '" AND DATE(start_date) >="' . $this->request->data['end_date'] .'"'])->orWhere(['designation_id' => $protikolpos['protikolpo_1']['designation_id'], 'DATE(end_date) >= "' . $this->request->data['start_date'] . '" AND DATE(end_date) <= "' . $this->request->data['end_date'].'"'])->first();

                    if (!empty($available)) {
                        $result[$designation_id]['protikolpo_1']=0;
                    } else {

                        $ProtikolpoSettingsTable->updateAll(
                            [
                                'employee_record_id'=>$employee_office['officer_id'],
                                'employee_name'=>$employee_office['officer_name'],
                                'start_date' => $this->request->data['start_date'],
                                'end_date' => $this->request->data['end_date'],
                                'is_show_acting' => $this->request->data['show_active'],
                                'selected_protikolpo' => 1,
                                'active_status' => 0
                            ],
                            [
                                'designation_id' => $designation_id
                            ]
                        );
                        $protikolpo_1_info = $EmployeeOfficesTable->getDesignationDetailsInfo($protikolpos['protikolpo_1']['designation_id']);
                        $result[$designation_id]['protikolpo_1_info']=$protikolpo_1_info['EmployeeRecords']['name_bng'].', '.$protikolpo_1_info['designation_label'];
                        $result[$designation_id]['protikolpo_1']=1;

                    }
                }
                if($result[$designation_id]['protikolpo_1']==0){
                    if(empty($protikolpos['protikolpo_2']['designation_id']) || $protikolpos['protikolpo_2']['designation_id'] == $employee_office['office_unit_organogram_id']){
                        $result[$designation_id]['protikolpo_2'] = 0;
                    } else {
                        $available = $ProtikolpoSettingsTable->find()
                            ->where(['designation_id' => $protikolpos['protikolpo_2']['designation_id'],
                                'start_date BETWEEN ' . $this->request->data['start_date'] . ' AND ' .
                                $this->request->data['end_date']])
                            ->orWhere(['designation_id' => $protikolpos['protikolpo_2']['designation_id'],
                                'end_date BETWEEN ' . $this->request->data['start_date'] . ' AND ' . $this->request->data['end_date']])->first();

                        if (!empty($available)) {
                            $result[$designation_id]['protikolpo_2']=0;
                        } else {

                            $ProtikolpoSettingsTable->updateAll(
                                [
                                    'employee_record_id'=>$employee_office['officer_id'],
                                    'employee_name'=>$employee_office['officer_name'],
                                    'start_date' => $this->request->data['start_date'],
                                    'end_date' => $this->request->data['end_date'],
                                    'selected_protikolpo' => 2,
                                    'active_status' => 0
                                ],
                                [
                                    'designation_id' => $designation_id
                                ]
                            );
                            $protikolpo_2_info = $EmployeeOfficesTable->getDesignationDetailsInfo($protikolpos['protikolpo_2']['designation_id']);
                            $result[$designation_id]['protikolpo_2_info']=$protikolpo_2_info['EmployeeRecords']['name_bng'].', '.$protikolpo_2_info['designation_label'];
                            $result[$designation_id]['protikolpo_2']=1;

                        }
                    }
                }


            }
            $this->response->body(json_encode(['status'=>'success','data'=>$result]));
            $this->response->type('json');
            return $this->response;
        }
        die;
    }

    public function protikolpoStatus(){
        $employee_offices = $this->getCurrentDakSection();
        if ($employee_offices['is_admin'] == 1) {
            $tableOfficeUnitOrganograms = TableRegistry::get('OfficeUnitOrganograms');
            $tableProtikolpoSettings = TableRegistry::get('ProtikolpoSettings');
            $tableProtikolpoLogs = TableRegistry::get('ProtikolpoLog');

            $protikolpo = $tableProtikolpoSettings->find()
                ->select([
                    'id',
                    'start_date'=>'DATE(start_date)',
                    'end_date'=>'DATE(end_date)',
                    'office_id',
                    'unit_id',
                    'designation_id',
                    'employee_record_id',
                    'employee_name',
                    'protikolpos',
                    'selected_protikolpo',
                    'start_date',
                    'end_date',
                    'active_status'
                ])
                ->where([
                'office_id' => $employee_offices['office_id']
            ])->hydrate(false)->toArray();

            foreach ($protikolpo as &$eachProtikolpo){
                $total = $tableProtikolpoLogs->find()->where(['protikolpo_id'=>$eachProtikolpo['id']])->count();
                $eachProtikolpo['total'] = $total;
            }

            $allOrgranogram = $tableOfficeUnitOrganograms->find('list',['keyField'=>'id','valueField'=>'details'])
                ->select([
                    'id',
                    'details' =>"CONCAT(designation_bng, ', ', OfficeUnits.unit_name_bng)"
                ])
                ->contain(['OfficeUnits'])
                ->where(['OfficeUnitOrganograms.office_id' => $employee_offices['office_id']])
                ->toArray();


            if(!empty($protikolpo)){
                foreach($protikolpo as $key=>&$value){
                    $value['designation'] = $allOrgranogram[$value['designation_id']];

                    if($value['selected_protikolpo']>0){
                        $protikolpos = jsonA($value['protikolpos'])['protikolpo_'.$value['selected_protikolpo']];

                        if(empty($allOrgranogram[$protikolpos['designation_id']])){
                            $otherOrgranogram = $tableOfficeUnitOrganograms->find()
                                ->select([
                                    'id',
                                    'details' =>"CONCAT(designation_bng, ', ', OfficeUnits.unit_name_bng)"
                                ])
                                ->contain(['OfficeUnits'])
                                ->where(['OfficeUnitOrganograms.id' => $protikolpos['designation_id']])
                                ->first();

                            $value['selected_protikolpo'] = $otherOrgranogram['details'];
                        }else{
                            $value['selected_protikolpo'] = $allOrgranogram[$protikolpos['designation_id']];
                        }
                    }
                }
            }

            $this->set(compact('protikolpo', 'employee_offices'));

        } else {
            $this->Flash->error("দুঃখিত! আপনি অনুমতিপ্রাপ্ত নন।");
            $this->redirect(['controller' => 'Dashboard', 'action' => 'dashboard']);
        }
    }

    public function protikolpoEditStatus($force = 0){
        $response=[
            'status'=>'error','message'=>'Invalid request'
        ];

        $employee_offices = $this->getCurrentDakSection();

        if ($employee_offices['is_admin'] == 1 && !empty($this->request->data['id'])) {
            $tableProtikolpoSettings = TableRegistry::get('ProtikolpoSettings');

            $protikolpo = $tableProtikolpoSettings->get($this->request->data['id']);
            unset($this->request->data['id']);
            if(!empty($this->request->data['start_date'])){
                $this->request->data['start_date'] = date('Y-m-d',strtotime($this->request->data['start_date']));
            } else {
                $this->request->data['start_date'] = $protikolpo['start_date']->i18nFormat('Y-M-dd', null, 'en-US');
            }
            if(!empty($this->request->data['end_date'])){
                $this->request->data['end_date'] = date('Y-m-d',strtotime($this->request->data['end_date']));
            } else {
                $this->request->data['end_date'] = $protikolpo['end_date']->i18nFormat('Y-M-dd', null, 'en-US');
            }
            if(!empty($this->request->data['start_date']) && !empty($this->request->data['end_date'])){
                if ($force == 0) {
                    if($this->request->data['start_date'] > $this->request->data['end_date']){
                        $response=[
                            'status'=>'error','message'=>'দুঃখিত! সঠিক তারিখ সীমা দেওয়া হয়নি।'
                        ];
                        $this->response->body(json_encode($response));
                        $this->response->statusCode(200);
                        $this->response->type('json');
                        return $this->response;
                    }
                }
            }
            $protikolpo = $tableProtikolpoSettings->patchEntity($protikolpo,$this->request->data);
            if($tableProtikolpoSettings->save($protikolpo)){

                $employeeRecord = TableRegistry::get('EmployeeRecords');
                $employeeInformation = $employeeRecord->get($protikolpo->employee_record_id);
                $this->SendSmsMessage($employeeInformation->personal_mobile,
                    "প্রিয় " . $employeeInformation->name_bng . PHP_EOL .'আপনার প্রতিকল্পের তারিখ পরিবর্তন করা হয়েছে।' . PHP_EOL. "ধন্যবাদ");
                $response=[
                    'status'=>'success'
                ];
            }else{
                $response=[
                    'status'=>'error','message'=>'দুঃখিত! কার্যক্রম সম্পন্ন করা সম্ভব হচ্ছে না'
                ];
            }
        }

        $this->response->body(json_encode($response));
        $this->response->statusCode(200);
        $this->response->type('json');
        return $this->response;
    }

    public function protikolpoCancelStatus(){
        $response=[
            'status'=>'error','message'=>'Invalid request'
        ];
		$id = (int)$this->request->data['id'];

		$edit_response = $this->protikolpoEditStatus(1);
		if (json_decode($edit_response->body())->status == 'success') {
            exec(ROOT . "/bin/cake API apiProtikolpoTransferByEnd false null ".$id." > /dev/null 2>&1 &",  $out, $t);
            $response=[
                'status' => 'success',
                'message' => 'প্রতিকল্প বাতিল কার্যক্রম চলছে...'
            ];
        } else {
            $response=[
                'status'=>'error','message'=>'দুঃখিত! কার্যক্রম সম্পন্ন করা সম্ভব হচ্ছে না'
            ];
        }
        

        // $employee_offices = $this->getCurrentDakSection();

        // if ($employee_offices['is_admin'] == 1 && !empty($this->request->data['id'])) {
        //     $tableProtikolpoSettings = TableRegistry::get('ProtikolpoSettings');
        //     $tableProtikolpoLog = TableRegistry::get('ProtikolpoLog');
        //     $tableEmployeeOffices = TableRegistry::get('EmployeeOffices');

        //     $protikolpo = $tableProtikolpoSettings->get($this->request->data['id']);
        //     $isExists = $tableEmployeeOffices->find()->where(['office_id' => $protikolpo['office_id'],
        //         'office_unit_id' => $protikolpo['unit_id'], 'office_unit_organogram_id' => $protikolpo['designation_id'],
        //         'EmployeeOffices.status' => 1,'protikolpo_status'=>1])->order(['id desc'])->first();
        //     unset($this->request->data['id']);
        //     $protikolpo->employee_record_id = 0;
        //     $protikolpo->employee_name = '';
        //     $protikolpo->selected_protikolpo = 0;
        //     $protikolpo->start_date = null;
        //     $protikolpo->end_date = null;
        //     $protikolpo->active_status = 0;
        //     if($tableProtikolpoSettings->save($protikolpo)){
        //         if(!empty($isExists)) {
        //             $protikolpoLogEntity = $tableProtikolpoLog->find()->where(['protikolpo_id' => $protikolpo['id']])->order(['id desc'])->first();
        //             if(!empty($protikolpoLogEntity)) {
        //                 $tableProtikolpoLog->updateAll([
        //                     'protikolpo_end_date' => date("Y-m-d H:i:s"),
        //                     'protikolpo_status' => 0,
        //                     'protikolpo_ended_by' => $employee_offices['officer_id'],
        //                 ], ['id' => $protikolpoLogEntity['id']]);
        //             }
        //         }
        //         $response=[
        //             'status'=>'success'
        //         ];
        //     }else{
        //         $response=[
        //             'status'=>'error','message'=>'দুঃখিত! কার্যক্রম সম্পন্ন করা সম্ভব হচ্ছে না'
        //         ];
        //     }
        // }

        $this->response->body(json_encode($response));
        $this->response->statusCode(200);
        $this->response->type('json');
        return $this->response;
    }

    public function protikolpoStartStatus(){
        $response=[
            'status'=>'error','message'=>'Invalid request'
        ];

        $employee_offices = $this->getCurrentDakSection();

        if ($employee_offices['is_admin'] == 1 && !empty($this->request->data['id'])) {
            $tableProtikolpoSettings = TableRegistry::get('ProtikolpoSettings');

            $protikolpo = $tableProtikolpoSettings->get($this->request->data['id']);

            if($protikolpo) {
                exec(ROOT . "/bin/cake API apiProtikolpoTransferByStart", $out, $t);

                $response = [
                    'status' => 'success','data'=>$out
                ];
            }

        }

        $this->response->body(json_encode($response));
        $this->response->statusCode(200);
        $this->response->type('json');
        return $this->response;
    }

    public function viewLog($id){

        $employee_offices = $this->getCurrentDakSection();

        $protikolpo = [];
        if ($employee_offices['is_admin'] == 1 && !empty($id)) {
            $tableProtikolpoLogs = TableRegistry::get('ProtikolpoLog');

            $protikolpo = $tableProtikolpoLogs->findByProtikolpoId($id)->toArray();
        }


        $this->set('protikolpo',$protikolpo);
    }

    public function updateEndDate() {
        $employee_record_id = $this->request->data['employee_record_id'];
        $end_date = $this->request->data['end_date'];
        $protikolpoSettingsTable = TableRegistry::get('ProtikolpoSettings');
        $protikolpoSettings = $protikolpoSettingsTable->find()->where(['employee_record_id' => $employee_record_id])->count();
        if ($protikolpoSettings > 0) {
            $protikolpoSettingsTable->updateAll(['end_date' => date('Y-m-d', strtotime($end_date))],['employee_record_id' => $employee_record_id]);
            $response = [
                'status' => 'success',
                'msg' => 'সফলভাবে হালনাগাদ করা হয়েছে'
            ];
        } else {
            $response = [
                'status' => 'error',
                'msg' => 'প্রতিকল্প ব্যবস্থাপনা সেটিং পাওয়া যায় নি'
            ];
        }
        $this->response->type('json');
        $this->response->body(json_encode($response));
        return $this->response;
    }
    public function stopProtikolpo() {
        $employee_record_id = $this->request->data['employee_record_id'];
        exec(ROOT . "/bin/cake API apiProtikolpoTransferByEnd 1 $employee_record_id",$output);

        $this->response->type('json');
        $this->response->body(json_encode(['status'=>'success', 'msg' => $output]));
        return $this->response;
    }
}