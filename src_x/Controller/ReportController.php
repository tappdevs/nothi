<?php

namespace App\Controller;

use App\Controller\ProjapotiController;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;
use Cake\Cache\Cache;

class ReportController extends ProjapotiController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
         $this->Auth->allow(['checkMigrationStatusContent','checkMigrationStatus']);
    }

    public function generateMonitoringDashboardMonthlyReport(){
        if($this->request->is('get')){
            
        }else{
            $response = [
                'status' =>'error',
                'msg' => 'Something went wrong'
            ];
            $start = isset($this->request->data['start'])?$this->request->data['start']:(isset($this->request->data['date'])?$this->request->data['date']:'');
             $end = isset($this->request->data['end'])?$this->request->data['end']:(isset($this->request->data['date'])?$this->request->data['date']:'');
           
            if(!empty($start) && !empty($end)){
                 $this->layout = 'ajax';
                $cronLogTable = TableRegistry::get('CronLog');
               $allData = $cronLogTable->getData(0,['start' => $start,'end' => $end],'Monitor Dashboard')->first();
   //            pr($allData);die;
               if(!empty($allData)){
                   $json_data = json_decode($allData['details']);
                   $data = [];
                   $data['operation_date'] = $allData['operation_date'];
                   $data['total_offices'] = $allData['total_request'];
                   $data['total_errors'] = $allData['failed'];
                   $data['error_details'] = substr($json_data,stripos($json_data,'<ul>'));
                   $response = [
                        'status' =>'success',
                        'data' => $data
                    ];
               }else{
                   $response['msg'] = 'No record found for '.$start;
               }
            }
            rtn:
                 $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
        }
       
    }
    public function officeWisePotrojariPending(){
        $auth = $this->Auth->user();
        if ($auth['user_role_id'] > 2) {
            $this->Flash->error('Unauthorized Access');
            return $this->redirect(['controller' => 'Users', 'action' => 'login']);
        }
        
    }
    public function generatePotrojariReceiverPending(){
        if($this->request->is('ajax')){
                set_time_limit('0');
                ini_set('memory_limit', '-1');
                $time[0] = isset($this->request->data['start_date'])?$this->request->data['start_date']:date('Y-m-d', strtotime("-2 days"));
                $time[1] =isset($this->request->data['end_date'])?$this->request->data['end_date']:date('Y-m-d');
                $office_id =  isset($this->request->data['office_id'])?$this->request->data['office_id']:0;
                $response = ['status' => 0,'msg' => 'Something Wrong'];
                try {
                    $this->switchOffice($office_id, 'DashboardOffices');
                    TableRegistry::remove('PotrojariReceiver');TableRegistry::remove('PotrojariOnulipi');TableRegistry::remove('Potrojari');
                    $p_receiver = TableRegistry::get('PotrojariReceiver');
                    $failed_Potrojari_receiver = $p_receiver->find()->select(['PotrojariReceiver.id','potro_subject' =>'Potrojari.potro_subject','PotrojariReceiver.receiving_officer_name','PotrojariReceiver.receiving_officer_email','PotrojariReceiver.receiving_officer_designation_label','PotrojariReceiver.receiving_office_unit_name','PotrojariReceiver.receiving_office_name','PotrojariReceiver.group_name','PotrojariReceiver.task_reposponse','PotrojariReceiver.created','PotrojariReceiver.potrojari_id','office_id'=> 'Potrojari.office_id','potrojari_date'=>'Potrojari.potrojari_date'])->where(['is_sent'=>0])->join([
                            "Potrojari" => [
                                'table' => 'potrojari', 'type' => 'inner',
                                'conditions' => ['Potrojari.id =PotrojariReceiver.potrojari_id']
                            ]
                        ])->where(['Potrojari.potro_status' => 'Sent']);
                    if(!empty($time[0])){
                        $failed_Potrojari_receiver = $failed_Potrojari_receiver->where(['date(PotrojariReceiver.created) >=' => $time[0]]);
                    }
                    if(!empty($time[1])){
                        $failed_Potrojari_receiver = $failed_Potrojari_receiver->where(['date(PotrojariReceiver.created) <=' => $time[1]]);
                    }
                    $response = ['status' => 1,'data' => $this->paginate($failed_Potrojari_receiver)];
                } catch (\Exception $ex) {
                    $response = ['status' => 0,'msg' => $ex->getMessage()];
                }
                $this->set(compact('time'));
                $this->set(compact('office_id'));
                $this->layout = 'ajax';
                $this->set(compact('response'));
                $this->set('from','Receiver');
                $this->set('header',' প্রাপক ');
                $this->render('office_wise_potrojari_pending_content');
        }
    }
    public function generatePotrojariOnulipiPending(){
          if($this->request->is('ajax')){
                set_time_limit('0');
                ini_set('memory_limit', '-1');
                $time[0] = isset($this->request->data['start_date'])?$this->request->data['start_date']:date('Y-m-d', strtotime("-2 days"));
                $time[1] =isset($this->request->data['end_date'])?$this->request->data['end_date']:date('Y-m-d');
                $office_id =  isset($this->request->data['office_id'])?$this->request->data['office_id']:0;
                $response = ['status' => 0,'msg' => 'Something Wrong'];
                try {
                    $this->switchOffice($office_id, 'DashboardOffices');
                    TableRegistry::remove('PotrojariReceiver');TableRegistry::remove('PotrojariOnulipi');TableRegistry::remove('Potrojari');
                    $p_onulipi = TableRegistry::get('PotrojariOnulipi');
                    $failed_Potrojari_onulipi = $p_onulipi->find()->select(['PotrojariOnulipi.id','potro_subject' =>  'Potrojari.potro_subject','PotrojariOnulipi.receiving_officer_name','PotrojariOnulipi.receiving_officer_email','PotrojariOnulipi.receiving_officer_designation_label','PotrojariOnulipi.receiving_office_unit_name','PotrojariOnulipi.receiving_office_name','PotrojariOnulipi.group_name','PotrojariOnulipi.task_reposponse','PotrojariOnulipi.created','PotrojariOnulipi.potrojari_id','office_id' =>'Potrojari.office_id','potrojari_date'=>'Potrojari.potrojari_date'])->where(['PotrojariOnulipi.is_sent'=>0])->join([
                            "Potrojari" => [
                                'table' => 'potrojari', 'type' => 'inner',
                                'conditions' => ['Potrojari.id =PotrojariOnulipi.potrojari_id']
                            ]
                        ])->where(['Potrojari.potro_status' => 'Sent']);
                    if(!empty($time[0])){
                        $failed_Potrojari_onulipi = $failed_Potrojari_onulipi->where(['date(PotrojariOnulipi.created) >=' => $time[0]]);
                    }
                    if(!empty($time[1])){
                        $failed_Potrojari_onulipi = $failed_Potrojari_onulipi->where(['date(PotrojariOnulipi.created) <=' => $time[1]]);
                    }
                    $response = ['status' => 1,'data' => $this->paginate($failed_Potrojari_onulipi)];
                } catch (\Exception $ex) {
                    $response = ['status' => 0,'msg' => $ex->getMessage()];
                }
                $this->set(compact('time'));
                $this->set(compact('office_id'));
                $this->layout = 'ajax';
                $this->set(compact('response'));
                $this->set('from','Onulipi');
                $this->set('header',' অনুলিপি ');
                $this->render('office_wise_potrojari_pending_content');
        }
    }
    public function officeIdWisePotrojariPending(){
          $auth = $this->Auth->user();
        if ($auth['user_role_id'] > 2) {
            $this->Flash->error('Unauthorized Access');
            return $this->redirect(['controller' => 'Users', 'action' => 'login']);
        }
    }
    public function checkMigrationStatus(){
         $office_domains        = TableRegistry::get('OfficeDomains');
        $all_offices           = $office_domains->getOfficesData();
        $this->set(compact('all_offices'));
        $this->layout = 'dashboard';
    }
    public function checkMigrationStatusContent(){
        set_time_limit('0');
        ini_set('memory_limit', '-1');
        $office_id = $this->request->data['office_id'];
          $data = ['status' => 0,'msg' => 'Something Wrong'];
            //            $result = $table_reports->officeSummary($officeid);
            try {
                $this->switchOffice($office_id, 'DashboardOffices');
                TableRegistry::remove('nothi_master_current_users_mig');TableRegistry::remove('nothi_master_permissions_mig');
                $current_own_office             = TableRegistry::get('NothiMasterCurrentUsers')->find()->where(['office_id' => $office_id,'nothi_office' => $office_id])->count();
                $current_other_office             = TableRegistry::get('NothiMasterCurrentUsers')->find()->where(['office_id' => $office_id,'nothi_office <>' => $office_id])->count();
                $current_migrated_own_office             = TableRegistry::get('nothi_master_current_users_mig')->find()->where(['office_id' => $office_id,'nothi_office' => $office_id])->count();
                $current_migrated_other_office             = TableRegistry::get('nothi_master_current_users_mig')->find()->where(['office_id' => $office_id,'nothi_office <>' => $office_id])->count();
                
                $premission_migrated_own_office              = TableRegistry::get('nothi_master_permissions_mig')->find()->where(['office_id' => $office_id,'nothi_office' => $office_id])->count();
                $premission_migrated_other_office              = TableRegistry::get('nothi_master_permissions_mig')->find()->where(['office_id' => $office_id,'nothi_office <>' => $office_id])->count();
                $premission_own_office              = TableRegistry::get('NothiMasterPermissions')->find()->where(['office_id' => $office_id,'nothi_office' => $office_id])->count();
                $premission_other_office              = TableRegistry::get('NothiMasterPermissions')->find()->where(['office_id' => $office_id,'nothi_office <>' => $office_id])->count();
                $data = [
                    'current_own_office' => $current_own_office,
                    'current_other_office' => $current_other_office,
                    'current_migrated_own_office' => $current_migrated_own_office,
                    'current_migrated_other_office' => $current_migrated_other_office,
                    'premission_migrated_own_office' => $premission_migrated_own_office,
                    'premission_migrated_other_office' => $premission_migrated_other_office,
                    'premission_own_office' =>$premission_own_office,
                    'premission_other_office' =>$premission_other_office,
                    'status' => 1
                ];
            } catch (\Exception $ex) {
                $data = ['status' => 0,'msg' => $ex->getMessage()];
            }

          $this->response->type('application/json');
          $this->response->body(json_encode($data));
          return $this->response;
    }
    public function nothiDebugger(){
        if($this->request->is('post')){
            $response = [
                'status' => 'error',
                'msg' => 'দুঃখিত কোন তথ্য পাওয়া যায়নি।'
            ];
            $msg = isset($this->request->data['msg'])?$this->request->data['msg']:'';
        if(empty($msg)){
                goto rtn;
            }
            try{
                $data = $this->getDecryptedData($msg);
                if(!empty($data)){
                    $response = [
                        'status' => 'success',
                        'data' => $data
                    ];
                }
            }catch (\Exception $ex) {
                $response['reason'] = makeEncryptedData($ex->getMessage());
            }
            rtn:
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        }
    }

    public function wingWiseReport($unit_id = 0) {
        $current_user = $this->getCurrentDakSection();
        $office_unit_table = TableRegistry::get('OfficeUnits');
        $office_unit = $office_unit_table->find('list', ['keyField'=>'id', 'valueField'=>'id'])->where(['office_id'=>$current_user['office_id']]);
        if ($unit_id == 0) {
            $office_unit = $office_unit->where(['parent_unit_id' => 0])->toArray();
        } else {
            $office_unit = $office_unit->where(['parent_unit_id' => $unit_id])->toArray();
        }
        $this->set('unit_ids', implode(',', $office_unit));
    }
    public function wingWiseReportDetail($startDate = '', $endDate = '') {
        set_time_limit(0);
        $this->layout = null;
        $reportsTable = TableRegistry::get('Reports');
        $OfficeUnitsTable = TableRegistry::get('OfficeUnits');
        $performanceOfficesTable = TableRegistry::get('PerformanceOffices');
        $performanceUnitsTable = TableRegistry::get('PerformanceUnits');
        $user_login_history_table = TableRegistry::get('UserLoginHistory');
        $employee_offices_table = TableRegistry::get('EmployeeOffices');
        $startDate = !empty($startDate) ? $startDate : date("Y-m-d");
        $endDate = !empty($endDate) ? $endDate : date("Y-m-d");

        $office_info = [];
        $data = [];
        if (!empty($this->request->data)) {
            $unit_Id = intval($this->request->data['office_unit_id']);

            $ownChildInfo = [];
            if ($unit_Id == 0) {
                $selected_office_section = $this->getCurrentDakSection();

//                $selfunit     = $OfficeUnitsTable->find('list',
//                        ['keyField' => 'id', 'valueField' => 'unit_name_bng'])->where(['id' => $selected_office_section['office_unit_id']])->toArray();
                if (empty($selected_office_section)) {
                    $ownChildInfo = $OfficeUnitsTable->find('list')->where(['active_status' => 1])->order(['unit_level asc, parent_unit_id asc',
                        'id desc'])->toArray();
                } else {
                    $ownChildInfo = $OfficeUnitsTable->getOfficeUnitsList($selected_office_section['office_id']);
                }

            }
            else {
                $selfunit = $OfficeUnitsTable->find()->select(['id',
                    'unit_name_bng'])->where(['id' => $unit_Id])->first();
                $ownChildInfo[$selfunit['id']] = $selfunit['unit_name_bng'];
            }
            $result = [];
            if (!empty($ownChildInfo)) {
                foreach ($ownChildInfo as $unit_Id => $unitname) {
                    try {
                        $today = date('Y-m-d');
                        $yesterday = date('Y-m-d', strtotime($today . ' -1 day'));
                        $office_info = $OfficeUnitsTable->getAll(['id' => $unit_Id], ['office_id'])->first();
                        $condition = [
                            'totalInbox' => 'SUM(inbox)', 'totalOutbox' => 'SUM(outbox)', 'totalNothijato' => 'SUM(nothijat)',
                            'totalNothivukto' => 'SUM(nothivukto)', 'totalNisponnoDak' => 'SUM(nisponnodak)',
                            'totalSouddog' => 'SUM(selfnote)',
                            'totalDaksohoNote' => 'SUM(daksohonote)', 'totalNisponnoPotrojari' => 'SUM(nisponnopotrojari)',
                            'totalNisponnoNote' => 'SUM(nisponnonote)',
                            'totalNisponno' => 'SUM(nisponno)', 'totalONisponno' => 'SUM(onisponno)',
                            'totalID' => 'count(id)'
                        ];
                        $data = $performanceUnitsTable->getUnitData($unit_Id,
                            [$startDate, $endDate])->select($condition)->group(['unit_id'])->toArray();
                        if (!empty($data[0]['totalID']) && $data[0]['totalID'] > 0) {
//                                $totalEmployee = $employee_offices_table->getAllEmployeeRecordID($office_info['office_id'],
//                                    $unit_Id, 0);
                            /* Total Login */
//                                $TotalLogin = $user_login_history_table->countTotalLogin($office_info['office_id'],
//                                    $unit_Id, 0, [$startDate, $endDate]);
                            $data[0]['totalUser'] = $employee_offices_table->getCountOfEmployeeOfOfficeUnites($office_info['office_id'], $unit_Id);
                            $lastonisponno = $performanceUnitsTable->getLastOnisponno($unit_Id,
                                [$startDate, $endDate]);
                            $data[0]['totalONisponnoDak'] = $lastonisponno['onisponnodak'];
                            $data[0]['totalONisponnoNote'] = $lastonisponno['onisponnonote'];
//                                $data[0]['totalLogin'] = $TotalLogin->count();
                        } else {
                            $this->switchOffice($office_info['office_id'], 'OfficeDB');
                            $data = $reportsTable->performanceReport($office_info['office_id'],
                                $unit_Id, 0,
                                ['date_start' => $startDate, 'date_end' => $endDate]);
                        }


                        $data[0]['lastUpdate'] =$performanceUnitsTable->getLastUpdateTime($unit_Id)['record_date'];
                        $data[0]['lastUpdate'] =(!empty($data[0]['lastUpdate']))? $data[0]['lastUpdate']->format('Y-m-d'):'';
                        $data[0]['totalSouddog'] = (($data[0]['totalSouddog'] < 0) ? 0: $data[0]['totalSouddog']);
                        $result[$unit_Id] = $data[0];
                        $result[$unit_Id]['name'] = $unitname;
                    } catch (\Exception $ex) {
                        continue;
                    }
                }
            }
        }
        $this->response->type('application/json');
        $this->response->body(json_encode($result[$unit_Id]));
        return $this->response;

        $this->set(compact('result'));
    }
}