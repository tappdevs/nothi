<?php

namespace App\Controller;

use App\Controller\ProjapotiController;
use App\Model\SSO\AppLoginRequest;
use App\Model\SSO\AppLoginResponse;
use App\Model\SSO\AppLogoutRequest;
use App\Model\SSO\InterAppLoginRequest;
use App\Model\SSO\InterAppLoginResponse;
use App\Model\SSO\SSOValues;
use App\Model\SSO\SSOToken;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;
use Cake\Utility\String;
use Cake\Auth\DefaultPasswordHasher;
use Cake\I18n\Time;
use Cake\Cache\Cache;

class SSOController extends ProjapotiController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        // Allow users to register and logout.
        // You should not add the "login" action to allow list. Doing so would
        // cause problems with normal functioning of AuthComponent.
        $this->Auth->allow(['login', 'applogin', 'interapplogin','generateOISFToken']);
    }

    public function login()
    {
        if ($this->request->is('get')) {
            $landingPageUrl = isset($this->request->query["landing_page_uri"]) ? $this->request->query["landing_page_uri"] : '';
            $appLoginRequest = new AppLoginRequest();
            $appLoginRequest->setLandingPageUrl($landingPageUrl);
            $requestUrl = $appLoginRequest->buildRequest();
            $nonce = $appLoginRequest->getReqNonce();

            $session = $this->request->session();
            $session->write('nonce', $nonce);

//            Cache::write('nonce', $nonce, 'memcached');

            return $this->redirect($requestUrl);
        }
        if ($this->request->is('post') && isset($this->request->data['token'])) {
            $token = $this->request->data['token'];
            $this->request->data['username'] = ' ';
            $this->request->data['password'] = ' ';
            try {
                $session = $this->request->session();
                $nonce = $session->read('nonce');

                $appLoginResponse = new AppLoginResponse();
                $response = $appLoginResponse->parseResponse($token, $nonce);

                $session->write('username', $response->getUserName());
                $session->delete("nonce");

                return $this->redirect($response->getLandingPageUrl());
            } catch (\Exception $ex) {
            }
        }
    }

    public function applogin()
    {
        try {
            $token = !empty($this->request->data['token']) ? $this->request->data['token'] : '';
            $this->request->data['username'] = '';
            $this->request->data['password'] = '';
            $session = $this->request->session();
            $nonce = $session->read('nonce');
//            if(empty($nonce)){
//                $nonce = Cache::read('nonce', 'memcached');
//            }
            $appLoginResponse = new AppLoginResponse();
            $response = $appLoginResponse->parseResponse($token, $nonce);
            if (!empty($response->getUserName())) {
                $username = $response->getUserName();
                $session->write('username', $username);
                $session->delete("nonce");
                // User successfully authenticated
                // Please provide him session
                // $response contains all the necessary user information.
                $userTable = TableRegistry::get('Users');
                $userData = $userTable->find()->where(['username' => $username])->orWhere(['user_alias' => $username])->first();
                if (!empty($userData)) {
                    $user = $userTable->get($userData['id']);
                    $user->force_password_change = 1;
                    unset($user->password);
                    $this->Auth->setUser($user->toArray());
                    $employee_info = TableRegistry::get('EmployeeOffices')->find()->select(['office_unit_organogram_id'])->where(['employee_record_id' => $user['employee_record_id']])->first();
                    if (!empty($employee_info)) {
                        $session->write('selected_office_section', $this->setCurrentDakSection($employee_info['office_unit_organogram_id']));
                        $modules = jsonA(MODULES);
                        $session->write('modules', $modules);
                        if ($user['user_role_id'] > 2) {
                            $session->write('module_id', 5);
                        } else {
                            $session->write('module_id', 1);
                        }
                        if ($user['user_role_id'] > 2) {
                            $table_instance_emp_offices = TableRegistry::get('EmployeeOffices');
                            if (($employee_office_records = Cache::read('emp_offc_rcds_' . $user['employee_record_id'], 'memcached')) === false) {
                                $employee_office_records = $table_instance_emp_offices->getEmployeeOfficeRecords($user['employee_record_id']);
                                Cache::write('emp_offc_rcds_' . $user['employee_record_id'], $employee_office_records, 'memcached');
                            }

                            if (!empty($this->request->data['fingerprint'])) {
                                foreach ($employee_office_records as $key => $value) {
                                    $value['fingerprint'] = $this->request->data['fingerprint'];
                                }
                            }

                            $this->saveLoginHistory($employee_office_records);
                        }
                    }
                    return $this->redirect($response->getLandingPageUrl());
                } else {
                    $ssoRequest = new AppLoginRequest();
                    die('Empty DATA');
                    return $this->redirect($ssoRequest->buildRequest());
                }
            }
        } catch (\Exception $ex) {
            $this->Flash->error($ex->getMessage());
            print_r($ex->getMessage());
            die;
            // $ssoRequest = new AppLoginRequest();
            // return $this->redirect($ssoRequest->buildRequest());
        }

    }

    public function iALoginReqInfoAjax()
    {
        $toAppName = $this->request->data['toAppName'];
        $landingPageUrl = $this->request->data['landingPageUrl']; //null
        $username = $this->Auth->user()["username"];

        $obj = new InterAppLoginRequest();
        echo($obj->getIALoginReqInfoAjax($username, $toAppName, $landingPageUrl));
        exit();
    }

    public function interapplogin()
    {
        $token = !empty($this->request->data['token']) ? $this->request->data['token'] : '';
        if (isset($token)) {
            try {
                $iaLoginResponse = new InterAppLoginResponse();
                $response = $iaLoginResponse->parseResponse($token);
                if (!empty($response->getUserName())) {
                    $username = $response->getUserName();
                    $userTable = TableRegistry::get('Users');
                    $userData = $userTable->find()->where(['username' => $username])->orWhere(['user_alias' => $username])->first();
                    if (!empty($userData)) {
                        $user = $userTable->get($userData['id']);
                        $user->force_password_change = 1;
                        unset($user->password);
                        $this->Auth->setUser($user->toArray());
                        if (!empty($response->getLandingPageUrl())) {
                            if ($response->getLandingPageUrl() == 'dashboard') {
                                return $this->redirect(['controller' => 'dashboard', 'action' => 'dashboard']);
                            } else {

                                return $this->redirect($response->getLandingPageUrl());
                            }

                        } else {
                            return $this->redirect(['controller' => 'dashboard', 'action' => 'dashboard']);
                        }

                    } else {
                        $ssoRequest = new AppLoginRequest();
                        return $this->redirect($ssoRequest->buildRequest());
                    }
                }
            } catch (\Exception $ex) {
                var_dump($ex->getMessage());
                exit(0);
            }
        }
    }

    public function logout()
    {
        $session = $this->request->session();
        $employee_office = $this->getCurrentDakSection();
        if (!empty($employee_office)) {
            $this->updateLoginHistory($employee_office['officer_id']);
        }
        $session->destroy();
        $appLogoutRequest = new AppLogoutRequest();
        $requestUrl = $appLogoutRequest->buildRequest();
        return $this->redirect($requestUrl);
    }

    public function apps()

    {

        $session = $this->request->session();

        $employee_office = $this->getCurrentDakSection();

        if (!empty($employee_office)) {

            $designationId = $employee_office['office_unit_organogram_id'];

        }

        $getToken = $this->generateOISFToken();
        // pr($getToken);die;
        if(!empty($getToken['status']) && $getToken['status'] == 'success'){
            $token = $getToken['token']; // should be a token
        }else{
            $token = "ancsuyfaudhsfvhvt2rt8285fg2"; // should be a token
            die('token not generated');
        }
        // $format = "http://esb.beta.doptor.gov.bd:8280/identity/designation/". 103150 . "/apps";
        // $url = "http://esb.beta.doptor.gov.bd:8280/identity/designation/". $designationId . "/apps";
        $url = "http://esb.beta.doptor.gov.bd:8280/identity/designation/". 103150 . "/apps";


        // $url = sprintf($format, $designationId);

        $curl = curl_init();

        curl_setopt_array($curl, array(

            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => "",
            CURLOPT_HTTPHEADER => array(
                "Authorization: Bearer {$token}",
                "cache-control: no-cache"
            ),

        ));


        $result = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        }

        echo $result;

        die;

    }


    public function testAppPermission()

    {

        echo '<pre>';

        try {

            $session = $this->request->session();

            $employee_office = $this->getCurrentDakSection();

            if (!empty($employee_office)) {

                $designationId = $employee_office['office_unit_organogram_id'];

            }

            $getToken = $this->generateOISFToken();
            if(!empty($getToken['status']) && $getToken['status'] == 'success'){
                $token = $getToken['token']; // should be a token
            }else{
                $token = "ancsuyfaudhsfvhvt2rt8285fg2"; // should be a token
            }


            $format = "http://doptor.gov.bd:8090/identity/designation/" . $designationId . "/apps";

            $url = sprintf($format, $designationId);


            $curl = curl_init();

            curl_setopt_array($curl, array(

                CURLOPT_URL => $url,

                CURLOPT_RETURNTRANSFER => 1,

                CURLOPT_HTTPHEADER, array(

                    "Authorization: Bearer " . $token

                ),

            ));


            $result = curl_exec($curl);

            curl_close($curl);

            echo $result;

            die;

        } catch (\Exception $ex) {

            print_r($ex->getMessage);

        }


    }
    private function generateOISFToken(){
        $response = array("status" => 'error', 'message' => 'Invalid request');
        try{
            $ssoValues = new SSOValues();
            $url = "http://esb.beta.doptor.gov.bd:8280/token/create";
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => "",
                CURLOPT_HTTPHEADER => array(
                    "Authorization: Secret {$ssoValues->getSharedSecret()}",
                    "cache-control: no-cache"
                ),
            ));

            $result = curl_exec($curl);
            $result = jsonA($result);

            curl_close($curl);

            if(!empty($result['token'])){
                $response = array("status" => 'success', 'token' => $result['token']);
            }
        }catch (\Exception $ex){
            $response['message'] = $ex->getMessage();
        }
        return $response;
    }

}
