<?php

namespace App\Controller;

use App\Controller\ProjapotiController;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\TableRegistry;
use Cake\I18n\Number;
use Cake\Datasource\ConnectionManager;
use Cake\I18n\Time;

class SummaryNothiController extends ProjapotiController
{
    /*
     * Nothi Masters
     */

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow([
            'apiSummaryPotroShow',
            'apiDraftApprovedByMinistry',
            'apiDraftUpdateByMinistry',
            'apiGetPosition',
            'apiDraftApproved',
            'apiApprovalCheck',
            'apiApprovalUsers',
            'apiApprovalStatus',
        ]);
    }

    public function CreateSarNothiDraft($nothimasterid)
    {
        $this->layout = null;
        $employee_office = $this->getCurrentDakSection();
        $nothiMasterPermissionsTable = TableRegistry::get("NothiMasterPermissions");
        $nothiMasterCurrentUsersTable = TableRegistry::get("NothiMasterCurrentUsers");
        $nothiMastersTable = TableRegistry::get('NothiMasters');
        $officesTable = TableRegistry::get('Offices');
        $employeeOfficesTable = TableRegistry::get('EmployeeOffices');
        $employeeRecordsTable = TableRegistry::get('EmployeeRecords');
        $ministriestable = TableRegistry::get('OfficeMinistries');
        $nothiPotroAttachmentsTable = TableRegistry::get('NothiPotroAttachments');

        $getSochibInformation = array();
        $getProtiMontriInformation = array();
        $getUpoMontriInformation = array();
        $getMontriInformation = array();
        $getPresidentInformation = array();

        $getSochibRecord = array();
        $getProtiMontriRecord = array();
        $getMontriRecord = array();
        $getPresidentRecord = array();

        $officeid = $employee_office['office_id'];

        $this->set(compact('officeid'));

        $officeInformation = $officesTable->get($officeid);
        $getMinistryName = $ministriestable->find()->where(['id' => $officeInformation['office_ministry_id']])->first();

        $this->set('ministryName', $getMinistryName);

        if ($officeid == 53) {
            $getSochibofficeinformation = $employeeOfficesTable->find()->select(['designation',
                'employee_record_id', 'office_unit_organogram_id', 'office_unit_id',
                'office_id'])->where(['office_id' => $officeid, 'summary_nothi_post_type' => 6,
                'status' => 1])->first();
            $getSochibRecord = $employeeRecordsTable->find()->select(['name_bng', 'username' => 'Users.username'])->join([
                'Users' => [
                    'table' => 'users', 'type' => 'INNER', 'conditions' => "Users.employee_record_id = EmployeeRecords.id"
                ]
            ])->where(['EmployeeRecords.id' => $getSochibofficeinformation['employee_record_id']])->first();
        } else {
            $getSochibofficeinformation = $employeeOfficesTable->find()->select(['designation',
                'employee_record_id', 'office_unit_organogram_id', 'office_unit_id',
                'office_id'])->where(['office_id' => $officeid, 'summary_nothi_post_type' => 1,
                'status' => 1])->first();
            $getSochibRecord = $employeeRecordsTable->find()->select(['name_bng', 'username' => 'Users.username'])->join([
                'Users' => [
                    'table' => 'users', 'type' => 'INNER', 'conditions' => "Users.employee_record_id = EmployeeRecords.id"
                ]
            ])->where(['EmployeeRecords.id' => $getSochibofficeinformation['employee_record_id']])->first();
        }


        $getProtiMontriofficeinformation = $employeeOfficesTable->find()->select(['designation',
            'employee_record_id', 'office_unit_organogram_id', 'office_unit_id',
            'office_id'])->where(['office_id' => $officeid, 'summary_nothi_post_type' => 2,
            'status' => 1])->first();
        if (!empty($getProtiMontriofficeinformation)) {
            $getProtiMontriRecord = $employeeRecordsTable->find()->select(['name_bng', 'username' => 'Users.username'])->join([
                'Users' => [
                    'table' => 'users', 'type' => 'INNER', 'conditions' => "Users.employee_record_id = EmployeeRecords.id"
                ]
            ])->where(['EmployeeRecords.id' => $getProtiMontriofficeinformation['employee_record_id']])->first();
        }

        $getUpoMontriofficeinformation = $employeeOfficesTable->find()->select(['designation',
            'employee_record_id', 'office_unit_organogram_id', 'office_unit_id',
            'office_id'])->where(['office_id' => $officeid, 'summary_nothi_post_type' => 7,
            'status' => 1])->first();
        if (!empty($getUpoMontriofficeinformation)) {
            $getUpoMontriRecord = $employeeRecordsTable->find()->select(['name_bng', 'username' => 'Users.username'])->join([
                'Users' => [
                    'table' => 'users', 'type' => 'INNER', 'conditions' => "Users.employee_record_id = EmployeeRecords.id"
                ]
            ])->where(['EmployeeRecords.id' => $getUpoMontriofficeinformation['employee_record_id']])->first();
        }

        $getMontriofficeinformation = $employeeOfficesTable->find()->select(['designation',
            'employee_record_id', 'office_unit_organogram_id', 'office_unit_id',
            'office_id'])->where(['office_id' => $officeid, 'summary_nothi_post_type' => 3,
            'status' => 1])->first();

        if (!empty($getMontriofficeinformation)) {
            $getMontriRecord = $employeeRecordsTable->find()->select(['name_bng', 'username' => 'Users.username'])->join([
                'Users' => [
                    'table' => 'users', 'type' => 'INNER', 'conditions' => "Users.employee_record_id = EmployeeRecords.id"
                ]
            ])->where(['EmployeeRecords.id' => $getMontriofficeinformation['employee_record_id']])->first();
        }


        $getPrimofficeinformation = $employeeOfficesTable->find()->select(['designation',
            'employee_record_id', 'office_unit_organogram_id', 'office_unit_id',
            'office_id'])->where(['summary_nothi_post_type' => 4,
            'status' => 1])->first();

        if (!empty($getPrimofficeinformation)) {
            $getPrimRecord = $employeeRecordsTable->find()->select(['name_bng', 'username' => 'Users.username'])->join([
                'Users' => [
                    'table' => 'users', 'type' => 'INNER', 'conditions' => "Users.employee_record_id = EmployeeRecords.id"
                ]
            ])->where(['EmployeeRecords.id' => $getPrimofficeinformation['employee_record_id']])->first();
        }

        $getPresidentofficeinformation = $employeeOfficesTable->find()->select(['designation',
            'employee_record_id', 'office_unit_organogram_id', 'office_unit_id',
            'office_id'])->where(['summary_nothi_post_type' => 5,
            'status' => 1])->first();

        if (!empty($getPresidentofficeinformation)) {
            $getPresidentRecord = $employeeRecordsTable->find()->select(['name_bng', 'username' => 'Users.username'])->join([
                'Users' => [
                    'table' => 'users', 'type' => 'INNER', 'conditions' => "Users.employee_record_id = EmployeeRecords.id"
                ]
            ])->where(['EmployeeRecords.id' => $getPresidentofficeinformation['employee_record_id']])->first();
        }

        $getSochibInformation['name_bng'] = !empty($getSochibRecord['name_bng'])
            ? $getSochibRecord['name_bng'] : '';
        $getSochibInformation['designation'] = !empty($getSochibofficeinformation['designation'])
            ? $getSochibofficeinformation['designation'] : '';
        $getSochibInformation['username'] = !empty($getSochibRecord['username'])
            ? $getSochibRecord['username'] : '';
        $getSochibInformation['en_username'] = !empty($getSochibRecord['username'])
            ? sGenerateToken(['file'=>$getSochibRecord['username']],['exp'=>time() + 60*300]):'';

        if (!empty($getSochibInformation['username'])) {
            $options['token'] = $getSochibInformation['en_username'];

            $data = $this->getSignature($getSochibInformation['username'], 1, 0, true,null,$options);

            if ($data == false) {
                echo "<p class='text-center text-danger'>দুঃখিত! সচিব এর স্বাক্ষর পাওয়া যায়নি।</p>";
                die;
            }
        }

        $getProtiMontriInformation['name_bng'] = !empty($getProtiMontriRecord['name_bng'])
            ? $getProtiMontriRecord['name_bng'] : '';
        $getProtiMontriInformation['designation'] = !empty($getProtiMontriofficeinformation['designation'])
            ? $getProtiMontriofficeinformation['designation'] : '';
        $getProtiMontriInformation['username'] = !empty($getProtiMontriRecord['username'])
            ? $getProtiMontriRecord['username'] : '';
        $getProtiMontriInformation['en_username'] = !empty($getProtiMontriRecord['username'])
            ?  sGenerateToken(['file'=>$getProtiMontriRecord['username']],['exp'=>time() + 60*300]):'';

        if (!empty($getProtiMontriInformation['username'])) {
            $options['token'] = $getProtiMontriInformation['en_username'];

            $data = $this->getSignature($getProtiMontriInformation['username'], 1, 0, true,null,$options);

            if ($data == false) {
                echo "<p class='text-center text-danger'>দুঃখিত! প্রতিমন্ত্রী এর স্বাক্ষর পাওয়া যায়নি।</p>";
                die;
            }
        }

        $getUpoMontriInformation['name_bng'] = !empty($getUpoMontriRecord['name_bng'])
            ? $getUpoMontriRecord['name_bng'] : '';
        $getUpoMontriInformation['designation'] = !empty($getUpoMontriofficeinformation['designation'])
            ? $getUpoMontriofficeinformation['designation'] : '';
        $getUpoMontriInformation['username'] = !empty($getUpoMontriRecord['username'])
            ? $getUpoMontriRecord['username'] : '';
        $getUpoMontriInformation['en_username'] = !empty($getUpoMontriRecord['username'])
            ? sGenerateToken(['file'=>$getUpoMontriInformation['username']],['exp'=>time() + 60*300]):'';

        if (!empty($getUpoMontriInformation['username'])) {
            $options['token'] = $getUpoMontriInformation['en_username'];

            $data = $this->getSignature($getUpoMontriInformation['username'], 1, 0, true,null,$options);

            if ($data == false) {

                echo "<p class='text-center text-danger'>দুঃখিত! উপমন্ত্রী এর স্বাক্ষর পাওয়া যায়নি।</p>";
                die;
            }
        }

        $getMontriInformation['name_bng'] = !empty($getMontriRecord['name_bng'])
            ? $getMontriRecord['name_bng'] : '';
        $getMontriInformation['designation'] = !empty($getMontriofficeinformation['designation'])
            ? $getMontriofficeinformation['designation'] : '';
        $getMontriInformation['username'] = !empty($getMontriRecord['username'])
            ? $getMontriRecord['username'] : '';
        $getMontriInformation['en_username'] = !empty($getMontriRecord['username'])
            ? sGenerateToken(['file'=>$getMontriInformation['username']],['exp'=>time() + 60*300]):'';

        if (!empty($getMontriInformation['username'])) {
            $options['token'] = $getMontriInformation['en_username'];

            $data = $this->getSignature($getMontriInformation['username'], 1, 0, true,null,$options);

            if ($data == false) {
                echo "<p class='text-center text-danger'>দুঃখিত! মন্ত্রী এর স্বাক্ষর পাওয়া যায়নি।</p>";
                die;
            }
        }

        $getPrimInformation['name_bng'] = !empty($getPrimRecord['name_bng'])
            ? $getPrimRecord['name_bng'] : '';
        $getPrimInformation['designation'] = !empty($getPrimofficeinformation['designation'])
            ? $getPrimofficeinformation['designation'] : '';
        $getPrimInformation['username'] = !empty($getPrimRecord['username'])
            ? $getPrimRecord['username'] : '';
        $getPrimInformation['en_username'] = !empty($getPrimRecord['username'])
            ? sGenerateToken(['file'=>$getPrimInformation['username']],['exp'=>time() + 60*300]):'';

        if (!empty($getPrimInformation['username']) && $employee_office['office_id'] == 53) {
            $options['token'] = $getPrimInformation['en_username'];

            $data = $this->getSignature($getPrimInformation['username'], 1, 0, true,null,$options);

            if ($data == false) {
                echo "<p class='text-center text-danger'>দুঃখিত! মাননীয় প্রধানমন্ত্রী এর স্বাক্ষর পাওয়া যায়নি।</p>";
                die;
            }
        }

        $getPresidentInformation['name_bng'] = !empty($getPresidentRecord['name_bng'])
            ? $getPresidentRecord['name_bng'] : '';
        $getPresidentInformation['designation'] = !empty($getPresidentofficeinformation['designation'])
            ? $getPresidentofficeinformation['designation'] : '';
        $getPresidentInformation['username'] = !empty($getPresidentofficeinformation['username'])
            ? $getPresidentofficeinformation['username'] : '';
        $getPresidentInformation['en_username'] = !empty($getPresidentofficeinformation['username'])
            ? sGenerateToken(['file'=>$getPresidentInformation['username']],['exp'=>time() + 60*300]):'';

        if (!empty($getPresidentInformation['username'])) {
            $options['token'] = $getPresidentInformation['en_username'];

            $data = $this->getSignature($getPresidentInformation['username'], 1, 0, true,null,$options);

            if ($data == false) {
                echo "<p class='text-center text-danger'>দুঃখিত! মহামান্য রাষ্ট্রপতি এর স্বাক্ষর পাওয়া যায়নি।</p>";
                die;
            }
        }

        $nothiMasters = array();
        $nothiMastersCurrentUser = array();

        $nothiPartsTable = TableRegistry::get('NothiParts');

        $nothiPartsInformation = $nothiPartsTable->get($nothimasterid);

        if (!empty($employee_office['office_id'])) {

            $nothiMasters = $nothiMasterPermissionsTable->hasAccess($employee_office['office_id'],
                $employee_office['office_id'],
                $employee_office['office_unit_id'],
                $employee_office['office_unit_organogram_id'],
                $nothiPartsInformation['nothi_masters_id'], $nothimasterid);
            $nothiMastersCurrentUser = $nothiMasterCurrentUsersTable->find()->where(['nothi_part_no' => $nothimasterid,
                'office_id' => $employee_office['office_id'], 'office_unit_id' => $employee_office['office_unit_id'],
                'office_unit_organogram_id' => $employee_office['office_unit_organogram_id'],
                'nothi_office' => $employee_office['office_id']])->first();

            if (empty($nothiMasters) || $nothiMasters['privilige_type'] == 0) {
                $this->Flash->error("দুঃখিত! কোনো তথ্য পাওয়া যায়নি");
                $this->redirect(['controller' => 'Dashboard', 'action' => 'dashboard']);
            }

            if (!empty($nothiMastersCurrentUser)) {

                $this->set('privilige_type', $nothiMasters['privilige_type']);

                $nothiRecord = $nothiPartsTable->getAll('NothiParts.id=' . $nothimasterid)->first();

                $officeUnitTable = TableRegistry::get('OfficeUnits');
                $officeunit = $officeUnitTable->get($nothiRecord['office_units_id']);
                $officeUnitsName = $officeunit['unit_name_bng'];

                $this->set('officeUnitsName', $officeUnitsName);

                $template_list = $this->potrojari_template_list(2);
                $this->set(compact('template_list'));
                $coverTemplate = $this->potrojari_template_list(3);
                $this->set(compact('coverTemplate'));
                $this->set('nothiMasterInfo', $nothiRecord);
                $this->set(compact('nothimasterid'));

                $this->set('getSochibInformation', $getSochibInformation);
                $this->set('getProtiMontriInformation', $getProtiMontriInformation);
                $this->set('getUpoMontriInformation', $getUpoMontriInformation);
                $this->set('getMontriInformation', $getMontriInformation);
                $this->set('getPrimInformation', $getPrimInformation);
                $this->set('getPresidentInformation', $getPresidentInformation);
            }
            $permitted_nothi_list = $nothiMasterPermissionsTable->allNothiPermissionList($employee_office['office_id'],
                $employee_office['office_id'],
                $employee_office['office_unit_id'],
                $employee_office['office_unit_organogram_id'], 1);
            $potroInfoTable = TableRegistry::get('NothiPotroAttachments');

            $potroAttachmentRecord = $potroInfoTable->find('list',
                ['keyField' => 'id', 'valueField' => [
                    "nothi_potro_page_bn", 'subject'
                ]])
                ->select(['subject' => 'NothiPotros.subject', 'NothiPotroAttachments.id',
                    "nothi_potro_page_bn" => 'NothiPotroAttachments.nothi_potro_page_bn'])
                ->join([
                    'NothiPotros' => [
                        'table' => 'nothi_potros',
                        'type' => 'INNER',
                        'conditions' => 'NothiPotros.id = NothiPotroAttachments.nothi_potro_id'
                    ]
                ])
                ->where(['NothiPotroAttachments.nothi_master_id' => $nothiRecord['nothi_masters_id']])
                ->where(['NothiPotroAttachments.nothijato' => 0, 'NothiPotroAttachments.status' => 1])
                ->where(['NothiPotroAttachments.nothi_part_no IN' => $permitted_nothi_list])
                ->order(['NothiPotroAttachments.id asc'])->toArray();

            $this->set(compact('potroAttachmentRecord', $potroAttachmentRecord));
        }
    }

    public function EditSarNothiDraft($nothimasterid, $sarSummaryid = 0,
                                      $nothi_office = 0)
    {
        $employee_office = $this->getCurrentDakSection();
        $otherNothi = false;
        if ($nothi_office != 0 && $nothi_office != $employee_office['office_id']) {
            $otherNothi = true;

            $this->switchOffice($nothi_office, 'MainNothiOffice');
        } else {
            $nothi_office = $employee_office['office_id'];
        }

        $this->set('nothi_office', $nothi_office);
        $this->set(compact('otherNothi'));

        TableRegistry::remove('NothiParts');
        TableRegistry::remove('NothiMasters');
        TableRegistry::remove('Potrojari');
        TableRegistry::remove('PotrojariAttachments');
        TableRegistry::remove('NothiPotroAttachments');

        $nothiMasterPermissionsTable = TableRegistry::get("NothiMasterPermissions");
        $nothiMasterCurrentUsersTable = TableRegistry::get("NothiMasterCurrentUsers");
        $potrojariTable = TableRegistry::get('Potrojari');
        $potrojari_attachments = TableRegistry::get('PotrojariAttachments');
        $nothiPotroAttachmentsTable = TableRegistry::get('NothiPotroAttachments');

        $nothiMasters = array();
        $nothiMastersCurrentUser = array();

        if (empty($sarSummaryid)) {
            $this->redirect(['controller' => 'NothiMasters', 'action' => 'index']);
        }

        $tableSummaryNothiUser = TableRegistry::get('SummaryNothiUsers');

        $allUsers = $tableSummaryNothiUser->find()->where(['nothi_part_no' => $nothimasterid, 'nothi_office' => $nothi_office,
            'potrojari_id' => $sarSummaryid, 'can_approve' => 1])->hydrate(false)->toArray();

        $this->set('can_approve', 0);
        $this->set('is_approve', 0);

        if (!empty($allUsers)) {
            foreach ($allUsers as $key => $value) {
                if ($value['designation_id'] == $employee_office['office_unit_organogram_id']) {
                    $this->set('can_approve', $value['can_approve']);
                    $this->set('is_approve', $value['is_approve']);
                    break;
                }
            }
        }

        if (!empty($employee_office['office_id'])) {
            $summaryDraft = $potrojariTable->get($sarSummaryid);

            $this->set('summaryDraft', $summaryDraft);

            $nothiPartsTable = TableRegistry::get('NothiParts');
            $nothiPartsInformation = $nothiPartsTable->get($nothimasterid);

            $nothiMasters = $nothiMasterPermissionsTable->hasAccess($employee_office['office_id'],
                $employee_office['office_id'],
                $employee_office['office_unit_id'],
                $employee_office['office_unit_organogram_id'],
                $nothiPartsInformation['nothi_masters_id'],
                $nothiPartsInformation['id']);
            $nothiMastersCurrentUser = $nothiMasterCurrentUsersTable->find()->where(['nothi_part_no' => $nothimasterid,
                'office_id' => $employee_office['office_id'], 'office_unit_id' => $employee_office['office_unit_id'],
                'office_unit_organogram_id' => $employee_office['office_unit_organogram_id'],
                'nothi_office' => $employee_office['office_id']])->first();

            if (empty($nothiMasters) || $nothiMasters['privilige_type'] == 0) {
                $this->Flash->error("দুঃখিত! কোনো তথ্য পাওয়া যায়নি");
                $this->redirect(['controller' => 'Dashboard', 'action' => 'dashboard']);
            }

            $this->set('privilige_type', 0);

            $nothiRecord = $nothiPartsTable->getAll('NothiParts.id=' . $nothimasterid)->first();

            $officeUnitTable = TableRegistry::get('OfficeUnits');
            $officeunit = $officeUnitTable->get($nothiRecord['office_units_id']);
            $officeUnitsName = $officeunit['unit_name_bng'];

            $this->set('officeUnitsName', $officeUnitsName);
            $this->set('nothiRecord', $nothiRecord);

            $template_list = $this->potrojari_template_list(2);
            $this->set(compact('template_list'));
            $coverTemplate = $this->potrojari_template_list(3);
            $this->set(compact('coverTemplate'));
            $this->set('nothiMasterInfo', $nothiRecord);
            $this->set(compact('nothimasterid'));

            $token = $this->generateToken([
                'office_id'=> $employee_office['office_id'],
                'office_unit_organogram_id'=>$employee_office['office_unit_organogram_id'],
                'parent_id'=> $sarSummaryid,
                'module'=>'Potrojari'
            ],['exp'=>time() + 3600*3]);
            $this->set('temp_token',$token);

            if (!empty($nothiMastersCurrentUser)) {
                $this->set('privilige_type', $nothiMasters['privilige_type']);
            }
            $attachments = $potrojari_attachments->find()->where(['potrojari_id' => $sarSummaryid,
                'is_inline' => 0])->toArray();

            $inlineattachments = $potrojari_attachments->find('list',
                ['keyField' => 'id', 'valueField' => 'potro_id'])->where(['potrojari_id' => $sarSummaryid,
                'is_inline' => 1])->toArray();

            $this->set('inlineattachments', $inlineattachments);
            $this->set('attachments', $attachments);
            $permitted_nothi_list = $nothiMasterPermissionsTable->allNothiPermissionList($employee_office['office_id'],
                $employee_office['office_id'],
                $employee_office['office_unit_id'],
                $employee_office['office_unit_organogram_id'], 1);
            $potroInfoTable = TableRegistry::get('NothiPotroAttachments');
            $potroAttachmentRecord = $potroInfoTable->find('list',
                ['keyField' => 'id', 'valueField' => [
                    "nothi_potro_page_bn", 'subject'
                ]])
                ->select(['subject' => 'NothiPotros.subject', 'NothiPotroAttachments.id',
                    "nothi_potro_page_bn" => 'NothiPotroAttachments.nothi_potro_page_bn'])
                ->join([
                    'NothiPotros' => [
                        'table' => 'nothi_potros',
                        'type' => 'INNER',
                        'conditions' => 'NothiPotros.id = NothiPotroAttachments.nothi_potro_id'
                    ]
                ])
                ->where(['NothiPotroAttachments.nothi_master_id' => $nothiRecord['nothi_masters_id']])
                ->where(['NothiPotroAttachments.nothijato' => 0, 'NothiPotroAttachments.status' => 1])
                ->where(['NothiPotroAttachments.nothi_part_no IN' => $permitted_nothi_list])
                ->order(['NothiPotroAttachments.id asc'])->toArray();

            $this->set(compact('potroAttachmentRecord', $potroAttachmentRecord));
        }
    }

    public function summaryPotroShow($nothimasterid, $nothipotroid = 0,
                                     $nothi_office = 0)
    {
        $employee_office = $this->getCurrentDakSection();
        $otherNothi = false;
        if ($nothi_office != 0 && $nothi_office != $employee_office['office_id']) {
            $otherNothi = true;

            $this->switchOffice($nothi_office, 'MainNothiOffice');
        } else {
            $nothi_office = $employee_office['office_id'];
        }

        $this->set('nothi_office', $nothi_office);
        TableRegistry::remove('NothiNotes');
        TableRegistry::remove('NothiParts');
        TableRegistry::remove('NothiPotroAttachments');
        $nothiMasterPermissionsTable = TableRegistry::get("NothiMasterPermissions");
        $nothiMasterCurrentUsersTable = TableRegistry::get("NothiMasterCurrentUsers");
        $nothiNotesTable = TableRegistry::get('NothiNotes');
        $employeeofficetable = TableRegistry::get('EmployeeOffices');
        $nothiPartsTable = TableRegistry::get('NothiParts');

        $nothiPartsInformation = $nothiPartsTable->get($nothimasterid);

        $nothiMasters = array();
        $nothiMastersCurrentUser = array();

        if (empty($nothipotroid)) {
            $this->redirect(['controller' => 'NothiMasters', 'action' => 'index']);
        }

        $nothiMasters = $nothiMasterPermissionsTable->hasAccess($employee_office['office_id'],
            $employee_office['office_id'], $employee_office['office_unit_id'],
            $employee_office['office_unit_organogram_id'],
            $nothiPartsInformation['nothi_masters_id'], $nothimasterid);

        $nothiMastersCurrentUser = $nothiMasterCurrentUsersTable->find()->where(['nothi_part_no' => $nothimasterid,
            'office_id' => $employee_office['office_id'], 'office_unit_id' => $employee_office['office_unit_id'],
            'office_unit_organogram_id' => $employee_office['office_unit_organogram_id'],
            'nothi_office' => $employee_office['office_id']])->first();

        if (empty($nothiMasters) || $nothiMasters['privilige_type'] == 0) {
            $this->Flash->error("দুঃখিত! কোনো তথ্য পাওয়া যায়নি");
            $this->redirect(['controller' => 'Dashboard', 'action' => 'dashboard']);
        }

        $this->set('privilige_type', 0);
        $this->set('can_approve', 0);

        TableRegistry::remove('SummaryNothiUsers');
        $tableSummaryNothiUser = TableRegistry::get('SummaryNothiUsers');

        $allUsers = $tableSummaryNothiUser->find()->where(['nothi_part_no' => $nothimasterid, 'current_office_id' => $employee_office['office_id'],
            'potro_id' => $nothipotroid,
            'designation_id' => $employee_office['office_unit_organogram_id']])->hydrate(false)->first();

        $this->set('position_number', '');
        if (!empty($allUsers)) {
            $this->set('position_number', $allUsers['position_number']);
            $this->set('approved', $allUsers['is_approve']);
            $this->set('can_approve', $allUsers['is_sent'] == 0 ? $allUsers['can_approve'] : 0);
        }


        $nothiRecord = $nothiPartsTable->getAll('NothiParts.id=' . $nothimasterid)->first();

        $officeUnitTable = TableRegistry::get('OfficeUnits');
        $officeunit = $officeUnitTable->get($nothiRecord['office_units_id']);
        $officeUnitsName = $officeunit['unit_name_bng'];

        $this->set('officeUnitsName', $officeUnitsName);

        $this->set('nothiRecord', $nothiRecord);

        $template_list = $this->potrojari_template_list(2);
        $this->set(compact('template_list'));
        $coverTemplate = $this->potrojari_template_list(3);
        $this->set(compact('coverTemplate'));
        $this->set('nothiMasterInfo', $nothiRecord);
        $this->set(compact('nothimasterid'));
        if (!empty($nothiMastersCurrentUser)) {
            $this->set('privilige_type', 1);
        }

        $nothipotro_attachments = TableRegistry::get('NothiPotroAttachments');

        $attachments = $nothipotro_attachments->find()->where(['nothi_part_no' => $nothimasterid,
            'nothi_potro_id' => $nothipotroid, 'attachment_type <>' => 'text'])->toArray();

        $this->set('attachments', $attachments);

        $mainpotro = $nothipotro_attachments->find()->where(['nothi_part_no' => $nothimasterid,
            'nothi_potro_id' => $nothipotroid, 'attachment_type' => 'text'])->order(['id desc'])->first();


        $this->set('mainpotro', $mainpotro);

        $notesheedid = $nothiNotesTable->getLastNoteSheetId($nothimasterid,
            $nothi_office);

        $this->set(compact('nothimasterid', 'nothipotroid', 'notesheedid'));
//        }
    }

    public function saveSummaryNothi($nothiMasterId)
    {
        set_time_limit(0);
        $this->layout = null;

        if ($this->request->is('ajax')) {
            if (!empty($this->request->data)) {
                if (empty($nothiMasterId)) {

                    echo json_encode(array("status" => 'error', 'msg' => __("দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না")));
                    die;
                }

                $potrojari = $this->summaryadd($nothiMasterId);

                if (!empty($potrojari)) {
                    echo json_encode(array("status" => 'success', 'msg' => __(' ড্রাফট করা হয়েছে')));
                }
            }
        }

        die;
    }

    private function summaryadd($nothiMasterId = 0)
    {

        $employee_office = $this->getCurrentDakSection();
        $table_instance_office = TableRegistry::get('Offices');
        $table_instance_unit = TableRegistry::get('OfficeUnits');
        $employeeOfficesTable = TableRegistry::get('EmployeeOffices');
        $employeeRecordsTable = TableRegistry::get('EmployeeRecords');
        $tablePotroJari = TableRegistry::get('Potrojari');

        $tablePotrojariReceiver = TableRegistry::get('PotrojariReceiver');
        $tablePotroJariAttachment = TableRegistry::get('PotrojariAttachments');
        $tableNothiPotroAttachment = TableRegistry::get('NothiPotroAttachments');

        $nothiPartsTable = TableRegistry::get('NothiParts');
        $nothiPartsInformation = $nothiPartsTable->get($nothiMasterId);

        $officeid = $employee_office['office_id'];

        if ($officeid == 53) {
            $getSochibofficeinformation = $employeeOfficesTable->find()->select(['designation',
                'employee_record_id', 'office_unit_organogram_id', 'office_unit_id',
                'office_id'])->where(['office_id' => $officeid, 'summary_nothi_post_type' => 6,
                'status' => 1])->first();
            $getSochibRecord = $employeeRecordsTable->find()->select(['name_bng'])->where(['id' => $getSochibofficeinformation['employee_record_id']])->first();
        } else {
            $getSochibofficeinformation = $employeeOfficesTable->find()->select(['designation',
                'employee_record_id', 'office_unit_organogram_id', 'office_unit_id',
                'office_id'])->where(['office_id' => $officeid, 'summary_nothi_post_type' => 1,
                'status' => 1])->first();
            $getSochibRecord = $employeeRecordsTable->find()->select(['name_bng'])->where(['id' => $getSochibofficeinformation['employee_record_id']])->first();
        }

        if (empty($getSochibRecord)) {
            echo json_encode(array("status" => 'error', 'msg' => __('দুঃখিত! সচিব মহোদয়ের তথ্য পাওয়া যায়নি')));
            return false;
        }

        $receiver = $employeeOfficesTable->find()->select(['designation',
            'employee_record_id', 'office_unit_organogram_id', 'office_unit_id',
            'office_id'])->where(['summary_nothi_post_type' => 8, 'status' => 1])->first();

        if (empty($receiver)) {
            $receiver = $employeeOfficesTable->find()->select(['designation',
                'employee_record_id', 'office_unit_organogram_id', 'office_unit_id',
                'office_id'])->where(['summary_nothi_post_type' => 6, 'status' => 1])->first();
        }
        $getReceiverInfo = array();


        if (empty($receiver)) {
            echo json_encode(array("status" => 'error', 'msg' => __('দুঃখিত! গ্রহণকারী তথ্য পাওয়া যায়নি')));
            return false;
        }

        if (!empty($receiver)) {
            $getReceiverInfo = $employeeRecordsTable->find()->select(['name_bng'])->where(['id' => $receiver['employee_record_id']])->first();
        }


        if ($this->request->is(['post'])) {
            $conn = ConnectionManager::get('default');

            try {
                $conn->begin();
                $potrojari = $tablePotroJari->newEntity();

                $potrojari->nothi_master_id = $nothiPartsInformation['nothi_masters_id'];
                $potrojari->nothi_part_no = $nothiPartsInformation['id'];
                $potrojari->nothi_notes_id = 0;
                $potrojari->nothi_potro_id = 0;
                $potrojari->potro_type = $this->request->data['potro_type'];
                $potrojari->sarok_no = $this->request->data['sender_sarok_no'];
                $potrojari->potrojari_date = date("Y-m-d H:i:s");
                $potrojari->potro_subject = $this->request->data['dak_subject'];
                $potrojari->potro_security_level = 0;
                $potrojari->potro_priority_level = 0;
                $potrojari->potro_status = 'SummaryDraft';
                $potrojari->potro_cover = $this->request->data['contentbody'];
                $potrojari->potro_description = $this->request->data['contentbody2'];
                $potrojari->is_summary_nothi = 1;
                $potrojari->created = date("Y-m-d H:i:s");
                $potrojari->modified = date("Y-m-d H:i:s");

                $potrojari->office_id = !empty($getSochibofficeinformation['office_id'])
                    ? $getSochibofficeinformation['office_id'] : 0;
                if (!empty($potrojari->office_id)) {
                    $officeInfo = $table_instance_office->get($potrojari->office_id);
                    $potrojari->officer_designation_id = !empty($getSochibofficeinformation['office_unit_organogram_id'])
                        ? $getSochibofficeinformation['office_unit_organogram_id']
                        : 0;
                    $rec_office_unit = $table_instance_unit->getOfficeUnitBy_designationId($potrojari->officer_designation_id);
                    $potrojari->officer_designation_label = !empty($getSochibofficeinformation['designation'])
                        ? $getSochibofficeinformation['designation'] : '';
                    $potrojari->office_name = $officeInfo['office_name_bng'];
                    $potrojari->officer_id = !empty($getSochibofficeinformation['employee_record_id'])
                        ? $getSochibofficeinformation['employee_record_id'] : 0;
                    $potrojari->office_unit_id = $rec_office_unit['id'];
                    $potrojari->office_unit_name = $rec_office_unit['unit_name_bng'];
                    $potrojari->officer_name = !empty($getSochibRecord['name_bng'])
                        ? $getSochibRecord['name_bng'] : '';
                }

                $potrojari->created_by = $this->Auth->user('id');
                $potrojari->modified_by = $this->Auth->user('id');
                $potrojari = $tablePotroJari->save($potrojari);

                $potrojariReceiver = $tablePotrojariReceiver->newEntity();

                $potrojariReceiver->potrojari_id = $potrojari->id;
                $potrojariReceiver->nothi_master_id = $potrojari->nothi_master_id;
                $potrojariReceiver->nothi_part_no = $potrojari->nothi_part_no;
                $potrojariReceiver->nothi_notes_id = $potrojari->nothi_notes_id;
                $potrojariReceiver->nothi_potro_id = $potrojari->nothi_potro_id;
                $potrojariReceiver->potro_type = $potrojari->potro_type;
                $potrojariReceiver->sarok_no = $potrojari->sarok_no;

                $potrojariReceiver->office_id = $potrojari->office_id;
                $potrojariReceiver->office_name = $potrojari->office_name;
                $potrojariReceiver->officer_id = $potrojari->officer_id;
                $potrojariReceiver->officer_name = $potrojari->officer_name;
                $potrojariReceiver->office_unit_id = $potrojari->office_unit_id;
                $potrojariReceiver->office_unit_name = $potrojari->office_unit_name;
                $potrojariReceiver->officer_designation_id = $potrojari->officer_designation_id;
                $potrojariReceiver->officer_designation_label = $potrojari->officer_designation_label;

                $potrojariReceiver->potrojari_date = $potrojari->potrojari_date;
                $potrojariReceiver->potro_subject = $potrojari->potro_subject;
                $potrojariReceiver->potro_security_level = $potrojari->potro_security_level;
                $potrojariReceiver->potro_priority_level = $potrojari->potro_priority_level;
                $potrojariReceiver->potro_description = $potrojari->potro_description;
                $potrojariReceiver->potro_cover = $potrojari->potro_cover;
                $potrojariReceiver->potro_status = $potrojari->potro_status;
                $potrojariReceiver->modified = $potrojari->modified;
                $potrojariReceiver->created = $potrojari->modified;
                $potrojariReceiver->created_by = $potrojari->created_by;
                $potrojariReceiver->modified_by = $potrojari->modified_by;

                $potrojariReceiver->receiving_office_id = !empty($receiver['office_id'])
                    ? $receiver['office_id'] : 0;
                if (!empty($potrojariReceiver->receiving_office_id)) {
                    $officeInfo = $table_instance_office->get($potrojariReceiver->receiving_office_id);
                    $potrojariReceiver->receiving_officer_designation_id = !empty($receiver['office_unit_organogram_id'])
                        ? $receiver['office_unit_organogram_id'] : 0;
                    $rec_office_unit = $table_instance_unit->getOfficeUnitBy_designationId($potrojariReceiver->receiving_officer_designation_id);
                    $potrojariReceiver->receiving_officer_designation_label = !empty($receiver['designation'])
                        ? $receiver['designation'] : '';
                    $potrojariReceiver->receiving_office_name = $officeInfo['office_name_bng'];
                    $potrojariReceiver->receiving_officer_id = !empty($receiver['employee_record_id'])
                        ? $receiver['employee_record_id'] : 0;
                    $potrojariReceiver->receiving_office_unit_id = $rec_office_unit['id'];
                    $potrojariReceiver->receiving_office_unit_name = $rec_office_unit['unit_name_bng'];
                    $potrojariReceiver->receiving_officer_name = !empty($getReceiverInfo['name_bng'])
                        ? $getReceiverInfo['name_bng'] : '';
                }

                $tablePotrojariReceiver->save($potrojariReceiver);

                $this->request->data['attachment'] = explode(',',
                    $this->request->data['uploaded_attachments']);

                if (!empty($this->request->data['prapto_potro'])) {
                    foreach ($this->request->data['prapto_potro'] as $praptopotrokey => $praptopotro) {
                        if (!empty($praptopotro)) {

                            $file = $tableNothiPotroAttachment->find()->where(['id' => $praptopotro])->first()->toArray();
                            $attachment_data = $file;
                            $attachment_data['potrojari_type'] = "Portojari";
                            $attachment_data['potrojari_id'] = $potrojari->id;
                            $attachment_data['is_inline'] = 1;
                            $attachment_data['potro_id'] = $praptopotro;
                            $attachment_data['is_summary_nothi'] = 1;

                            unset($attachment_data['id']);
                            $potrojari_attachment = $tablePotroJariAttachment->newEntity();
                            $potrojari_attachment = $tablePotroJariAttachment->patchEntity($potrojari_attachment,
                                $attachment_data);
                            $potrojari_attachment->created_by = $potrojari->created_by;
                            $potrojari_attachment->modified_by = $potrojari->modified_by;
                            $tablePotroJariAttachment->save($potrojari_attachment);
                        }
                    }
                }

                if (!empty($this->request->data['attachment'])) {
                    foreach ($this->request->data['attachment'] as $file) {
                        if (!empty($file)) {
                            $file_details = explode(';', $file, 2);
                            $attachment_data = array();
                            $attachment_data['potrojari_type'] = "Portojari";
                            $attachment_data['potrojari_id'] = $potrojari->id;
                            $attachment_data['attachment_description'] = $this->request->data['file_description'];
                            $attachment_data['attachment_name'] = !empty($file_details[1]) ? $file_details[1] : null;


                            $attachment_data['attachment_type'] = $this->getMimeType($file_details[0]);
                            $attachment_data['is_summary_nothi'] = 1;
                            $file_path_arr = explode(FILE_FOLDER, $file_details[0]);

                            $attachment_data['file_dir'] = $this->request->webroot . 'content/';
                            $attachment_data['file_name'] = $file_path_arr[1];
                            $explode = explode('?token=',$attachment_data['file_name']);
                            $attachment_data['file_name'] = $explode[0];
                            $potrojari_attachment = $tablePotroJariAttachment->newEntity();
                            $potrojari_attachment = $tablePotroJariAttachment->patchEntity($potrojari_attachment,
                                $attachment_data);
                            $potrojari_attachment->created_by = $potrojari->created_by;
                            $potrojari_attachment->modified_by = $potrojari->modified_by;
                            $tablePotroJariAttachment->save($potrojari_attachment);
                        }
                    }
                }

                if (!empty($this->request->data['contentbody'])) {
                    $attachment_data = array();
                    $attachment_data['potrojari_type'] = "Portojari";
                    $attachment_data['potrojari_id'] = $potrojari->id;
                    $attachment_data['attachment_description'] = '';
                    $attachment_data['attachment_type'] = 'text';
                    $attachment_data['file_dir'] = '';
                    $attachment_data['file_name'] = '';
                    $attachment_data['is_summary_nothi'] = 1;
                    $attachment_data['content_body'] = $this->request->data['contentbody2'];
                    $attachment_data['content_cover'] = $this->request->data['contentbody'];

                    $potrojari_attachment = $tablePotroJariAttachment->newEntity();
                    $potrojari_attachment = $tablePotroJariAttachment->patchEntity($potrojari_attachment,
                        $attachment_data);
                    $potrojari_attachment->created_by = $potrojari->created_by;
                    $potrojari_attachment->modified_by = $potrojari->modified_by;
                    $tablePotroJariAttachment->save($potrojari_attachment);
                }

                $nothiPartsInformation->is_active = 0;

                $nothiPartsTable->save($nothiPartsInformation);

                $conn->commit();

                $tableSummaryNothiUser = TableRegistry::get('SummaryNothiUsers');

                $tableSummaryNothiUser->deleteAll(['nothi_part_no' => $nothiMasterId,
                    'potrojari_id' => $potrojari->id, 'nothi_office' => $officeid]);

//sachib
                $summarynothientity = $tableSummaryNothiUser->newEntity();
                $summarynothientity->nothi_office = $officeid;
                $summarynothientity->tracking_id = bnToen($potrojari->sarok_no);
                $summarynothientity->current_office_id = $officeid;
                $summarynothientity->nothi_master_id = $nothiPartsInformation['nothi_masters_id'];
                $summarynothientity->nothi_part_no = $nothiMasterId;
                $summarynothientity->potrojari_id = $potrojari->id;
                $summarynothientity->designation_id = $potrojari->officer_designation_id;
                $summarynothientity->employee_record_id = $potrojari->officer_id;
                $summarynothientity->can_approve = 1;
                $summarynothientity->is_approve = 0;
                $summarynothientity->sequence_number = 1;
                $summarynothientity->position_number = 'sochib_signature';
                $summarynothientity->created = date("Y-m-d H:i:s");
                $summarynothientity->created_by = $potrojari->created_by;
                $summarynothientity->modified_by = $potrojari->modified_by;
                $tableSummaryNothiUser->save($summarynothientity);


                //upomontri
                $getUpoMontriofficeinformation = $employeeOfficesTable->find()->select(['designation',
                    'employee_record_id', 'office_unit_organogram_id', 'office_unit_id',
                    'office_id'])->where(['office_id' => $officeid, 'summary_nothi_post_type' => 7,
                    'status' => 1])->first();

                if (!empty($getUpoMontriofficeinformation)) {
                    $summarynothientity = $tableSummaryNothiUser->newEntity();
                    $summarynothientity->nothi_office = $officeid;
                    $summarynothientity->tracking_id = bnToen($potrojari->sarok_no);
                    $summarynothientity->current_office_id = $officeid;
                    $summarynothientity->nothi_master_id = $nothiPartsInformation['nothi_masters_id'];
                    $summarynothientity->nothi_part_no = $nothiMasterId;
                    $summarynothientity->potrojari_id = $potrojari->id;
                    $summarynothientity->designation_id = $getUpoMontriofficeinformation->office_unit_organogram_id;
                    $summarynothientity->employee_record_id = $getUpoMontriofficeinformation->employee_record_id;
                    $summarynothientity->can_approve = 1;
                    $summarynothientity->is_approve = 0;
                    $summarynothientity->sequence_number = 2;
                    $summarynothientity->position_number = 'upomontri_receiver_signature';
                    $summarynothientity->created = date("Y-m-d H:i:s");
                    $summarynothientity->created_by = $potrojari->created_by;
                    $summarynothientity->modified_by = $potrojari->modified_by;
                    $tableSummaryNothiUser->save($summarynothientity);
                }


                $getProtiMontriofficeinformation = $employeeOfficesTable->find()->select(['designation',
                    'employee_record_id', 'office_unit_organogram_id', 'office_unit_id',
                    'office_id'])->where(['office_id' => $officeid, 'summary_nothi_post_type' => 2,
                    'status' => 1])->first();

                if (!empty($getProtiMontriofficeinformation)) {
//proti montri
                    $summarynothientity = $tableSummaryNothiUser->newEntity();
                    $summarynothientity->nothi_office = $officeid;
                    $summarynothientity->tracking_id = bnToen($potrojari->sarok_no);
                    $summarynothientity->current_office_id = $officeid;
                    $summarynothientity->nothi_master_id = $nothiPartsInformation['nothi_masters_id'];
                    $summarynothientity->nothi_part_no = $nothiMasterId;
                    $summarynothientity->potrojari_id = $potrojari->id;
                    $summarynothientity->designation_id = $getProtiMontriofficeinformation->office_unit_organogram_id;
                    $summarynothientity->employee_record_id = $getProtiMontriofficeinformation->employee_record_id;
                    $summarynothientity->can_approve = 1;
                    $summarynothientity->is_approve = 0;
                    $summarynothientity->sequence_number = 3;
                    $summarynothientity->position_number = 'second_receiver_signature';
                    $summarynothientity->created = date("Y-m-d H:i:s");
                    $summarynothientity->created_by = $potrojari->created_by;
                    $summarynothientity->modified_by = $potrojari->modified_by;
                    $tableSummaryNothiUser->save($summarynothientity);
                }


                $getMontriofficeinformation = $employeeOfficesTable->find()->select(['designation',
                    'employee_record_id', 'office_unit_organogram_id', 'office_unit_id',
                    'office_id'])->where(['office_id' => $officeid, 'summary_nothi_post_type' => 3,
                    'status' => 1])->first();

                if (!empty($getMontriofficeinformation)) {
// montri
                    $summarynothientity = $tableSummaryNothiUser->newEntity();
                    $summarynothientity->nothi_office = $officeid;
                    $summarynothientity->tracking_id = bnToen($potrojari->sarok_no);
                    $summarynothientity->current_office_id = $officeid;
                    $summarynothientity->nothi_master_id = $nothiPartsInformation['nothi_masters_id'];
                    $summarynothientity->nothi_part_no = $nothiMasterId;
                    $summarynothientity->potrojari_id = $potrojari->id;
                    $summarynothientity->designation_id = $getMontriofficeinformation->office_unit_organogram_id;
                    $summarynothientity->employee_record_id = $getMontriofficeinformation->employee_record_id;
                    $summarynothientity->can_approve = 1;
                    $summarynothientity->is_approve = 0;
                    $summarynothientity->sequence_number = 4;
                    $summarynothientity->position_number = empty($getProtiMontriofficeinformation)
                        ? 'second_receiver_signature' : 'third_receiver_signature';
                    $summarynothientity->created = date("Y-m-d H:i:s");
                    $summarynothientity->created_by = $potrojari->created_by;
                    $summarynothientity->modified_by = $potrojari->modified_by;
                    $tableSummaryNothiUser->save($summarynothientity);
                }


                $Mainreceiver = $employeeOfficesTable->find()->select(['designation',
                    'employee_record_id', 'office_unit_organogram_id', 'office_unit_id',
                    'office_id'])->where(['summary_nothi_post_type' => 4, 'status' => 1])->first();

// main
                if (!empty($Mainreceiver)) {
                    $summarynothientity = $tableSummaryNothiUser->newEntity();
                    $summarynothientity->nothi_office = $officeid;
                    $summarynothientity->tracking_id = bnToen($potrojari->sarok_no);
                    $summarynothientity->current_office_id = $Mainreceiver->office_id;
                    $summarynothientity->nothi_master_id = ($officeid == $Mainreceiver->office_id ? $nothiPartsInformation['nothi_masters_id'] : 0);
                    $summarynothientity->nothi_part_no = ($officeid == $Mainreceiver->office_id ? $nothiMasterId : 0);
                    $summarynothientity->potrojari_id = $potrojari->id;
                    $summarynothientity->designation_id = $Mainreceiver->office_unit_organogram_id;
                    $summarynothientity->employee_record_id = $Mainreceiver->employee_record_id;
                    $summarynothientity->can_approve = 1;
                    $summarynothientity->is_approve = 0;
                    $summarynothientity->sequence_number = 5;
                    $summarynothientity->position_number = empty($getProtiMontriofficeinformation)
                        ? (empty($getMontriofficeinformation) ? 'second_receiver_signature'
                            : 'third_receiver_signature') : (empty($getMontriofficeinformation)
                            ? 'third_receiver_signature' : 'fourth_receiver_signature');
                    $summarynothientity->created = date("Y-m-d H:i:s");
                    $summarynothientity->created_by = $potrojari->created_by;
                    $summarynothientity->modified_by = $potrojari->modified_by;
                    $tableSummaryNothiUser->save($summarynothientity);
                }


                if ($potrojari->potro_type == 16) {

                    $Mainreceiver = $employeeOfficesTable->find()->select(['designation',
                        'employee_record_id', 'office_unit_organogram_id', 'office_unit_id',
                        'office_id'])->where(['summary_nothi_post_type' => 5,
                        'status' => 1])->first();

                    if (!empty($Mainreceiver)) {
// main
                        $summarynothientity = $tableSummaryNothiUser->newEntity();
                        $summarynothientity->nothi_office = $officeid;
                        $summarynothientity->tracking_id = bnToen($potrojari->sarok_no);
                        $summarynothientity->current_office_id = $Mainreceiver->office_id;
                        $summarynothientity->nothi_master_id = ($officeid == $Mainreceiver->office_id ? $nothiPartsInformation['nothi_masters_id'] : 0);
                        $summarynothientity->nothi_part_no = ($officeid == $Mainreceiver->office_id ? $nothiMasterId : 0);
                        $summarynothientity->potrojari_id = $potrojari->id;
                        $summarynothientity->designation_id = $Mainreceiver->office_unit_organogram_id;
                        $summarynothientity->employee_record_id = $Mainreceiver->employee_record_id;
                        $summarynothientity->can_approve = 1;
                        $summarynothientity->is_approve = 0;
                        $summarynothientity->sequence_number = 6;
                        $summarynothientity->position_number = empty($getProtiMontriofficeinformation)
                            ? (empty($getMontriofficeinformation) ? 'third_receiver_signature'
                                : 'fourth_receiver_signature') : (empty($getMontriofficeinformation)
                                ? 'fourth_receiver_signature' : 'fifth_receiver_signature');
                        $summarynothientity->created = date("Y-m-d H:i:s");
                        $summarynothientity->created_by = $potrojari->created_by;
                        $summarynothientity->modified_by = $potrojari->modified_by;
                        $tableSummaryNothiUser->save($summarynothientity);
                    } else {
                        $conn->rollback();
                        echo json_encode(array("status" => 'error', 'msg' => __('সার-সংক্ষেপ সংরক্ষণ করা সম্ভব হচ্ছেনা')));
                        return false;
                    }
                }

                return $potrojari;
            } catch (\Exception $ex) {

                $conn->rollback();
                echo json_encode(array("status" => 'error', 'msg' => __('সার-সংক্ষেপ সংরক্ষণ করা সম্ভব হচ্ছেনা'), 'error' => $ex->getMessage()));
                return false;
            }
        }
        return true;
    }

    public function summaryNothiUser()
    {

        $selectedOffice = $this->getCurrentDakSection();
        $offices = TableRegistry::get('Offices')->get($selectedOffice['office_id']);
        $office_layers = TableRegistry::get('OfficeLayers')->get($offices['office_layer_id']);
        if ($office_layers['layer_level'] != 1) {
            $this->Flash->error('আপনি অনুমতিপ্রাপ্ত নয়');
            return $this->redirect('/');
        }

        $officeid = $selectedOffice['office_id'];
        if ($officeid == 53) {
            $this->view = 'pmo_summary_nothi_user';
        }

        $table_employee_office = TableRegistry::get('EmployeeOffices');
        $table_employee_records = TableRegistry::get('EmployeeRecords');

        $results = $table_employee_office->find()->select([
            'EmployeeOffices.id',
            'EmployeeOffices.office_unit_id',
            'EmployeeOffices.office_unit_organogram_id',
            'EmployeeOffices.designation',
            'EmployeeOffices.joining_date',
            'EmployeeOffices.employee_record_id',
            'EmployeeOffices.summary_nothi_post_type',
            "unit_name_bng" => 'OfficeUnits.unit_name_bng',
            "personal_mobile" => 'EmployeeRecords.personal_mobile',
            "personal_email" => 'EmployeeRecords.personal_email',
            "name_bng" => 'EmployeeRecords.name_bng',
            "name_eng" => 'EmployeeRecords.name_eng',
            "identity_no" => 'EmployeeRecords.identity_no',
            "is_cadre" => 'EmployeeRecords.is_cadre',
            "username" => 'Users.username'
        ])->join([

            'OfficeUnits' => [
                'table' => 'office_units',
                'type' => 'left',
                'conditions' => 'EmployeeOffices.office_unit_id = OfficeUnits.id'
            ],
            'EmployeeRecords' => [
                'table' => 'employee_records',
                'type' => 'left',
                'conditions' => 'EmployeeRecords.id = EmployeeOffices.employee_record_id'
            ],
            'Users' => [
                'table' => 'users',
                'type' => 'inner',
                'conditions' => 'EmployeeRecords.id = Users.employee_record_id'
            ]
        ])->where(['EmployeeOffices.office_id' => $officeid, 'EmployeeOffices.status' => 1])->order(['EmployeeOffices.designation_level ASC, EmployeeOffices.designation_sequence ASC']);


        $this->set(compact('results'));
    }


    public function deleteSummaryDraftPotrojari($nothiMasterId, $referenceId)
    {
        $this->layout = null;

        if (empty($nothiMasterId)) {
            echo json_encode(array("status" => 'error', 'msg' => __("দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না")));
            die;
        }

        $conn = ConnectionManager::get('default');

        TableRegistry::remove('Potrojari');
        TableRegistry::remove('PotrojariVersions');
        TableRegistry::remove('PotrojariReceiver');
        TableRegistry::remove('PotrojariAttachments');
        $potrojariTable = TableRegistry::get("Potrojari");

        $employee_office = $this->getCurrentDakSection();

        $potrojari = $potrojariTable->find()->where([
            'nothi_part_no' => $nothiMasterId,
            'id' => $referenceId,
            'potro_status' => 'SummaryDraft',
        ])->first();

        if (!empty($potrojari)) {
            try {
                $conn->begin();
                $potrojariVersionTable = TableRegistry::get('PotrojariVersions');
                $tablePotrojariReceiver = TableRegistry::get('PotrojariReceiver');
                $tablePotrojariAttachments = TableRegistry::get('PotrojariAttachments');
                $tableSummaryNothiUsers = TableRegistry::get('SummaryNothiUsers');

                $potrojariVersionTable->deleteAll(['potrojari_id' => $potrojari->id]);
                $tablePotrojariReceiver->deleteAll(['potrojari_id' => $potrojari->id]);
                $tablePotrojariAttachments->deleteAll(['potrojari_id' => $potrojari->id]);

                $tableSummaryNothiUsers->deleteAll(['potrojari_id' => $potrojari->id, 'nothi_office' => $employee_office['office_id'],]);

                $potrojariTable->delete($potrojari);

                $conn->commit();
                echo json_encode(array("status" => 'success', 'msg' => __('ড্রাফট মুছে ফেলা হয়েছে')));
            } catch (Exception $ex) {
                $conn->rollback();
                echo json_encode(array("status" => 'error', 'msg' => __('দুঃখিত! ড্রাফট মুছে ফেলা সম্ভব হচ্ছে না')));
            }
        } else {
            $conn->rollback();
            echo json_encode(array("status" => 'error', 'msg' => __('দুঃখিত! ড্রাফট মুছে ফেলা সম্ভব হচ্ছে না')));
        }
        die;
    }


    private function potrojari_template_list($status = 1)
    {

        $potrojari_templates = TableRegistry::get('PotrojariTemplates');
        $query = $potrojari_templates->find('list',
            ['keyField' => 'id', 'valueField' => 'template_name'])->select(["id",
            "template_name"])->where(['status' => $status])->toArray();

        return $query;
    }


    public function draftApprovedByMinistry($approve = 0, $nothi_office = 0)
    {
        $employee_office = $this->getCurrentDakSection();
        $otherNothi = false;
        if ($nothi_office != 0 && $nothi_office != $employee_office['office_id']) {
            $otherNothi = true;

            $this->switchOffice($nothi_office, 'MainNothiOffice');
        } else {
            $nothi_office = $employee_office['office_id'];
        }
        $this->layout = null;

        if ($this->request->is('post', 'ajax', 'put')) {

            $tableSummaryNothiUser = TableRegistry::get('SummaryNothiUsers');

            $allUsers = $tableSummaryNothiUser->find()->where([
                'nothi_part_no' => $this->request->data['nothi_part_no'],
                'potro_id' => $this->request->data['potro_id'], 'current_office_id' => $employee_office['office_id'],
                'designation_id' => $employee_office['office_unit_organogram_id'], 'is_sent' => 0])->hydrate(false)->first();

            $userInfo = array();
            if (!empty($allUsers)) {
                $usersTable = TableRegistry::get("Users");
                $userData = $usersTable->find()->where(['employee_record_id' => $employee_office['officer_id']])->first();

                $options['token']= sGenerateToken(['file'=>$userData['username']],['exp'=>time() + 60*300]);
                $exists = $this->getSignature($userData['username'],
                    1, 1, true,null,$options);

                if ($approve && !$exists) {
                    echo json_encode(array("status" => 'error', 'msg' => __("দুঃখিত! স্বাক্ষর পাওয়া যায় নি")));
                    die;
                }

                $options['token']= sGenerateToken(['file'=>$userData['username']],['exp'=>time() + 60*300]);
                $userInfo['signature'] = $this->getSignature($userData['username'],
                    1, 1,false,null,$options);
            }

            if (!empty($userInfo)) {
                $userInfo['will_sign'] = $approve;
                $userInfo['position'] = $allUsers['position_number'];

                echo json_encode(array("status" => 'success', 'data' => $userInfo));
            } else {
                echo json_encode(array("status" => 'error', 'msg' => __("দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না")));
            }
        }

        die;
    }

    public function DraftUpdateByMinistry($nothimasterid, $nothipotroid = 0,
                                          $nothi_office = 0)
    {
        $employee_office = $this->getCurrentDakSection();
        $otherNothi = false;
        if ($nothi_office != 0 && $nothi_office != $employee_office['office_id']) {
            $otherNothi = true;

            $this->switchOffice($nothi_office, 'MainNothiOffice');
        } else {
            $nothi_office = $employee_office['office_id'];
        }
        $this->set('nothi_office', $nothi_office);
        $nothiMasterPermissionsTable = TableRegistry::get("NothiMasterPermissions");

        $nothiMasters = array();
        $nothiMastersCurrentUser = array();

        $return = array("status" => 'error', 'msg' => "দুঃখিত! কোনো তথ্য পাওয়া যায়নি");

        if (empty($nothipotroid)) {
            echo json_encode($return);
            die;
        }

        TableRegistry::remove('NothiNotes');
        TableRegistry::remove('NothiParts');
        TableRegistry::remove('NothiPotroAttachments');
        $nothiPartsTable = TableRegistry::get('NothiParts');
        $usersTable = TableRegistry::get('Users');

        $nothiPartsInformation = $nothiPartsTable->get($nothimasterid);

        $nothiMasters = $nothiMasterPermissionsTable->hasAccess($employee_office['office_id'],
            $employee_office['office_id'], $employee_office['office_unit_id'],
            $employee_office['office_unit_organogram_id'],
            $nothiPartsInformation['nothi_masters_id'], $nothimasterid);
        if (empty($nothiMasters) || $nothiMasters['privilige_type'] == 0) {
            echo json_encode($return);
            die;
        }
        if ($this->request->is('ajax')) {
            if (!empty($this->request->data)) {

                try {
//                    $userData = $usersTable->find()->where(['employee_record_id' => $employee_office['officer_id']])->first();
//                    $exists = $this->getSignature(getTokenValue(4) . $userData['username'] . getTokenValue(8),
//                        1, 1, true);
//
//                    if(!$exists){
//                        echo json_encode(array("status" => 'error', 'msg' => __("দুঃখিত! স্বাক্ষর পাওয়া যায় নি")));
//                        die;
//                    }

                    $potrojari_attachments = TableRegistry::get('NothiPotroAttachments');
                    $mainpotro = $potrojari_attachments->find()->where(['nothi_part_no' => $nothimasterid,
                        'nothi_potro_id' => $nothipotroid, 'attachment_type' => 'text'])->order(['id desc'])->first();
                    if (!empty($mainpotro)) {
                        $mainpotro->content_body = $this->request->data['contentbody2'];
                        $mainpotro->is_approved = 1;
                        $potrojari_attachments->save($mainpotro);
                        $potrojari_attachments->updateAll(['is_approved' => 1],
                            ['nothi_part_no' => $nothimasterid, 'nothi_potro_id' => $nothipotroid,
                                'attachment_type <>' => 'text']);

                        $tableSummaryNothiUser = TableRegistry::get('SummaryNothiUsers');

                        $allUsers = $tableSummaryNothiUser->find()->where([
                            'nothi_part_no' => $nothimasterid,
                            'potro_id' => $nothipotroid, 'current_office_id' => $employee_office['office_id'],
                            'designation_id' => $employee_office['office_unit_organogram_id'],
                            'is_sent' => 0])->hydrate(false)->first();

                        if (!empty($allUsers)) {
                            $tableSummaryNothiUser->updateAll(['is_approve' => 1], ['id' => $allUsers['id']]);
                        }

                        $return = array("status" => 'success', 'msg' => 'সার-সংক্ষেপ অনুমোদিত হয়েছে');

                        echo json_encode($return);
                        die;
                    } else {
                        echo json_encode($return);
                        die;
                    }
                } catch (Exception $ex) {
                    echo json_encode($return);
                    die;
                }
            }
        }
        die;
    }

    public function draftApproved($nothiMasterId, $referenceId, $approve = 0,
                                  $nothi_office = 0)
    {
        $this->layout = null;
        $employee_office = $this->getCurrentDakSection();
        $otherNothi = false;
        if ($nothi_office != 0 && $nothi_office != $employee_office['office_id']) {
            $otherNothi = true;

            $this->switchOffice($nothi_office, 'MainNothiOffice');
        } else {
            $nothi_office = $employee_office['office_id'];
        }
        if ($this->request->is('post', 'ajax', 'put')) {

            if (empty($nothiMasterId)) {
                echo json_encode(array("status" => 'error', 'msg' => __("দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না")));
                die;
            }

            TableRegistry::remove("Potrojari");
            $potrojariTable = TableRegistry::get("Potrojari");
            $usersTable = TableRegistry::get("Users");

            $potrojari = $potrojariTable->find()->where([
                'nothi_part_no' => $nothiMasterId,
                'id' => $referenceId,
                'potro_status' => 'SummaryDraft',
            ])->first();


            if (!empty($potrojari)) {
                $userInfo = array();

                $userData = $usersTable->find()->where(['employee_record_id' => $employee_office['officer_id']])->first();

                $options['token']= sGenerateToken(['file'=>$userData['username']],['exp'=>time() + 60*300]);
                $exists = $this->getSignature($userData['username'],
                    1, 1, true,null,$options);

                if ($approve && !$exists) {
                    echo json_encode(array("status" => 'error', 'msg' => __("দুঃখিত! স্বাক্ষর পাওয়া যায় নি")));
                    die;
                }

                $tableSummaryNothiUser = TableRegistry::get('SummaryNothiUsers');
                $getData = $tableSummaryNothiUser->find()->where([
                    'potrojari_id' => $referenceId,
                    'nothi_part_no' => $nothiMasterId,
                    'current_office_id' => $employee_office['office_id'],
                    'designation_id' => $employee_office['office_unit_organogram_id'],
                    'can_approve' => 1])->first();

                if (!empty($getData)) {
                    try {

                        if (!empty($this->request->data['description'])) {
                            $potrojari->potro_description = $this->request->data['description'];
                            if (!$this->updateSummaryBody($potrojari->id, $potrojari->potro_description)) {
                                throw new \Exception("দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না");
                            }
                        }
                        $getData->is_approve = $approve;
                        $tableSummaryNothiUser->save($getData);
                        $userInfo['username'] = $userData['username'];
                        $userInfo['position'] = $getData['position_number'];
                        $userInfo['will_sign'] = $approve;
                        echo json_encode(array("status" => 'success', 'data' => $userInfo));
                    } catch (\Exception $ex) {
                        echo json_encode(array("status" => 'error', 'msg' => __("দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না"),'details'=>$this->makeEncryptedData($ex->getMessage())));
                    }
                } else {
                    echo json_encode(array("status" => 'error', 'msg' => __("দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না")));
                }
            } else {
                echo json_encode(array("status" => 'error', 'msg' => __("দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না")));
            }
        }

        die;
    }


    public function assignUserforSummary()
    {
        $officeEmployeeTable = TableRegistry::get('EmployeeOffices');

        $grahokPro = $grahokPmo = $montri = $sochib = $protimontri = $upomontri = $president = $pmmontri = $mukkhosochib = 0;
        if (!empty($this->request->data)) {
            $montri = !empty($this->request->data['montri']) ? intval($this->request->data['montri']) : 0;
            $sochib = !empty($this->request->data['sochib']) ? intval($this->request->data['sochib']) : 0;
            $protimontri = !empty($this->request->data['protimontri']) ? intval($this->request->data['protimontri']) : 0;
            $upomontri = !empty($this->request->data['upomontri']) ? intval($this->request->data['upomontri']) : 0;
            $president = !empty($this->request->data['president']) ? intval($this->request->data['president']) : 0;
            $pmmontri = !empty($this->request->data['pmontri']) ? intval($this->request->data['pmontri']) : 0;
            $mukkhosochib = !empty($this->request->data['mukkhosochib']) ? intval($this->request->data['mukkhosochib']) : 0;
            $grahokPmo = !empty($this->request->data['grahokPmo']) ? intval($this->request->data['grahokPmo']) : 0;
            $grahokPro = !empty($this->request->data['grahokPro']) ? intval($this->request->data['grahokPro']) : 0;
        }

        $employeeoffices = $this->getCurrentDakSection();

        try {
            $conn = ConnectionManager::get('default');

            $conn->begin();

            if (($president <= 0)) {
                $officeEmployeeTable->updateAll(['summary_nothi_post_type' => 0],
                    ['office_id' => $employeeoffices['office_id'], 'summary_nothi_post_type' => 5]);
            } else {
                $getPresidentData = $officeEmployeeTable->get($president);

                if (!empty($getPresidentData)) {
                    $officeEmployeeTable->updateAll(['summary_nothi_post_type' => 0],
                        ['office_id' => $employeeoffices['office_id'], 'summary_nothi_post_type' => 5]);

                    $getPresidentData->summary_nothi_post_type = 5;
                    $officeEmployeeTable->save($getPresidentData);
                }
            }

            if ($pmmontri <= 0) {
                $officeEmployeeTable->updateAll(['summary_nothi_post_type' => 0],
                    ['office_id' => $employeeoffices['office_id'], 'summary_nothi_post_type' => 4]);
            } else {
                $getPMontriData = $officeEmployeeTable->get($pmmontri);

                if (!empty($getPMontriData)) {

                    $getPMontriData->summary_nothi_post_type = 4;
                    $officeEmployeeTable->save($getPMontriData);
                }
            }

            if ($mukkhosochib <= 0) {
                $officeEmployeeTable->updateAll(['summary_nothi_post_type' => 0],
                    ['office_id' => $employeeoffices['office_id'], 'summary_nothi_post_type' => 6]);
            } else {
                $getMukkhosochibData = $officeEmployeeTable->get($mukkhosochib);

                if (!empty($getMukkhosochibData)) {
                    $officeEmployeeTable->updateAll(['summary_nothi_post_type' => 0],
                        ['office_id' => $employeeoffices['office_id'], 'summary_nothi_post_type' => 6]);

                    $getMukkhosochibData->summary_nothi_post_type = 6;
                    $officeEmployeeTable->save($getMukkhosochibData);
                }
            }


            if ($grahokPmo <= 0 || $grahokPmo == $mukkhosochib) {
                $officeEmployeeTable->updateAll(['summary_nothi_post_type' => 0],
                    ['office_id' => $employeeoffices['office_id'], 'summary_nothi_post_type' => 8]);
            } else {
                $getGrahokPmoData = $officeEmployeeTable->get($grahokPmo);

                if (!empty($getGrahokPmoData)) {
                    $officeEmployeeTable->updateAll(['summary_nothi_post_type' => 0],
                        ['office_id' => $employeeoffices['office_id'], 'summary_nothi_post_type' => 8]);

                    $getGrahokPmoData->summary_nothi_post_type = 8;
                    $officeEmployeeTable->save($getGrahokPmoData);
                }
            }


            if ($grahokPro <= 0 || $grahokPro == $president) {
                $officeEmployeeTable->updateAll(['summary_nothi_post_type' => 0],
                    ['office_id' => $employeeoffices['office_id'], 'summary_nothi_post_type' => 9]);
            } else {
                $getGrahokProData = $officeEmployeeTable->get($grahokPro);

                if (!empty($getGrahokProData)) {
                    $officeEmployeeTable->updateAll(['summary_nothi_post_type' => 0],
                        ['office_id' => $employeeoffices['office_id'], 'summary_nothi_post_type' => 9]);

                    $getGrahokProData->summary_nothi_post_type = 9;
                    $officeEmployeeTable->save($getGrahokProData);
                }
            }


            if ($montri <= 0) {
                $officeEmployeeTable->updateAll(['summary_nothi_post_type' => 0],
                    ['office_id' => $employeeoffices['office_id'], 'summary_nothi_post_type' => 3]);
            } else {
                $getMontriData = $officeEmployeeTable->get($montri);

                if (!empty($getMontriData)) {
                    $officeEmployeeTable->updateAll(['summary_nothi_post_type' => 0],
                        ['office_id' => $employeeoffices['office_id'], 'summary_nothi_post_type' => 3]);

                    $getMontriData->summary_nothi_post_type = 3;
                    $officeEmployeeTable->save($getMontriData);
                }
            }

            if ($upomontri <= 0) {
                $officeEmployeeTable->updateAll(['summary_nothi_post_type' => 0],
                    ['office_id' => $employeeoffices['office_id'], 'summary_nothi_post_type' => 7]);
            } else {
                $getUpoMontriData = $officeEmployeeTable->get($upomontri);

                if (!empty($getUpoMontriData)) {
                    $officeEmployeeTable->updateAll(['summary_nothi_post_type' => 0],
                        ['office_id' => $employeeoffices['office_id'], 'summary_nothi_post_type' => 7]);

                    $getUpoMontriData->summary_nothi_post_type = 7;
                    $officeEmployeeTable->save($getUpoMontriData);
                }
            }

            if ($protimontri <= 0) {
                $officeEmployeeTable->updateAll(['summary_nothi_post_type' => 0],
                    ['office_id' => $employeeoffices['office_id'], 'summary_nothi_post_type' => 2]);
            } else {
                $getProtiMontriData = $officeEmployeeTable->get($protimontri);

                if (!empty($getProtiMontriData)) {
                    $officeEmployeeTable->updateAll(['summary_nothi_post_type' => 0],
                        ['office_id' => $employeeoffices['office_id'], 'summary_nothi_post_type' => 2]);

                    $getProtiMontriData->summary_nothi_post_type = 2;
                    $officeEmployeeTable->save($getProtiMontriData);
                }
            }

            if ($sochib <= 0) {
                $officeEmployeeTable->updateAll(['summary_nothi_post_type' => 0],
                    ['office_id' => $employeeoffices['office_id'], 'summary_nothi_post_type' => 1]);
            } else {
                $getSochibData = $officeEmployeeTable->get($sochib);

                if (!empty($getSochibData)) {
                    $officeEmployeeTable->updateAll(['summary_nothi_post_type' => 0],
                        ['office_id' => $employeeoffices['office_id'], 'summary_nothi_post_type' => 1]);

                    $getSochibData->summary_nothi_post_type = 1;
                    $officeEmployeeTable->save($getSochibData);
                }
            }

            echo 1;

            $conn->commit();
        } catch (Exception $e) {
            echo 0;
            $conn->rollback();
        }

        die;
    }

    public function approvalCheck($nothi_part = 0, $potrojari_id = 0, $nothi_office = 0)
    {
        $this->layout = null;
        $employee_office = $this->getCurrentDakSection();
        $otherNothi = false;
        if ($nothi_office != 0 && $nothi_office != $employee_office['office_id']) {
            $otherNothi = true;
            $this->switchOffice($nothi_office, 'MainNothiOffice');
        } else {
            $nothi_office = $employee_office['office_id'];
        }

        if ($this->request->is('post', 'ajax', 'put')) {

            if (empty($nothi_part) || empty($potrojari_id)) {
                echo json_encode(array("status" => 'error', 'msg' => __("দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না")));
                die;
            }

            TableRegistry::remove("Potrojari");
            $potrojariTable = TableRegistry::get("Potrojari");


            $potrojari = $potrojariTable->find()->where([
                'nothi_part_no' => $nothi_part,
                'id' => $potrojari_id,
                'potro_status' => 'SummaryDraft',
            ])->first();


            if (!empty($potrojari)) {

                $tableSummaryNothiUser = TableRegistry::get('SummaryNothiUsers');
                $getData = $tableSummaryNothiUser->find()->where(['potrojari_id' => $potrojari_id,
                    'nothi_office' => $employee_office['office_id'],
                    'nothi_part_no' => $nothi_part,
                    'designation_id <>' => $employee_office['office_unit_organogram_id'],
                    'is_approve' => 0])->count();

                echo json_encode(array("status" => 'success', 'count' => $getData));

            } else {
                echo json_encode(array("status" => 'error', 'msg' => __("দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না")));
            }
        } else {
            echo json_encode(array("status" => 'error', 'msg' => __("দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না")));
        }

        die;
    }

    public function approvalUsers($nothi_part = 0, $potrojari_id = 0, $nothi_office = 0)
    {
        $this->layout = null;
        $employee_office = $this->getCurrentDakSection();
        $otherNothi = false;
        if ($nothi_office != 0 && $nothi_office != $employee_office['office_id']) {
            $otherNothi = true;
            $this->switchOffice($nothi_office, 'MainNothiOffice');
        } else {
            $nothi_office = $employee_office['office_id'];
        }

        if ($this->request->is('post', 'ajax', 'put')) {

            if (empty($nothi_part) || empty($potrojari_id)) {
                echo json_encode(array("status" => 'error', 'msg' => __("দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না")));
                die;
            }

            TableRegistry::remove("Potrojari");
            $potrojariTable = TableRegistry::get("Potrojari");


            $potrojari = $potrojariTable->find()->where([
                'nothi_part_no' => $nothi_part,
                'id' => $potrojari_id,
                'potro_status' => 'SummaryDraft',
            ])->first();


            if (!empty($potrojari)) {

                $tableSummaryNothiUser = TableRegistry::get('SummaryNothiUsers');
                $tableOfficeUnitOrganograms = TableRegistry::get('OfficeUnitOrganograms');
                $tableEmployeeRecords = TableRegistry::get('EmployeeRecords');
                $getData = $tableSummaryNothiUser->find()->where(['potrojari_id' => $potrojari_id,
                    'nothi_office' => $employee_office['office_id'],
                    'nothi_part_no' => $nothi_part,
                    'designation_id <>' => $employee_office['office_unit_organogram_id'],
                    'is_approve' => 0])->toArray();

                if (!empty($getData)) {
                    foreach ($getData as $key => &$value) {
                        $value['designation_name'] = $tableOfficeUnitOrganograms->getBanglaName($value['designation_id'])['designation_bng'];
                        $value['employee_name'] = $tableEmployeeRecords->getEmployeeInfoByRecordId($value['employee_record_id'])['name_bng'];
                    }
                }

                echo json_encode(array("status" => 'success', 'data' => $getData, 'potrojari' => $potrojari));

            } else {
                echo json_encode(array("status" => 'error', 'msg' => __("দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না")));
            }
        } else {
            echo json_encode(array("status" => 'error', 'msg' => __("দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না")));
        }

        die;
    }

    public function approvalStatus($nothi_part = 0, $potrojari_id = 0, $nothi_office = 0)
    {
        $this->layout = null;
        $employee_office = $this->getCurrentDakSection();
        $otherNothi = false;
        if ($nothi_office != 0 && $nothi_office != $employee_office['office_id']) {
            $otherNothi = true;
            $this->switchOffice($nothi_office, 'MainNothiOffice');
        } else {
            $nothi_office = $employee_office['office_id'];
        }

        if ($this->request->is('post', 'ajax')) {

            if (empty($nothi_part) || empty($potrojari_id)) {
                echo json_encode(array("status" => 'error', 'msg' => __("দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না")));
                die;
            }

            TableRegistry::remove("Potrojari");
            $potrojariTable = TableRegistry::get("Potrojari");
            TableRegistry::remove('PotrojariAttachments');
            $potrojariAttachmentsTable = TableRegistry::get("PotrojariAttachments");
            TableRegistry::remove('PotrojariReceiver');
            $potrojariPotrojariReceiver = TableRegistry::get("PotrojariReceiver");

            $tableSummaryNothiUser = TableRegistry::get('SummaryNothiUsers');

            $checkExits = $tableSummaryNothiUser->find()->where(['nothi_office' => $employee_office['office_id'],
                'potrojari_id' => $potrojari_id, 'nothi_part_no' => $nothi_part, 'designation_id' => $employee_office['office_unit_organogram_id'], 'is_approve' => 1])->count();

            $potrojari = $potrojariTable->get($potrojari_id);

            if (!empty($checkExits) && !empty($potrojari) && $potrojari->nothi_part_no == $nothi_part && $potrojari->potro_status == 'SummaryDraft') {

                $data = $this->request->data;

                if (!empty($data['description'])) {
                    if ($this->updateSummaryBody($potrojari_id, $data['description'])) {
                        if (!empty($data['data'])) {
                            foreach ($data['data'] as $key => $value) {
                                if ($value['status'] == 1) {
                                    $tableSummaryNothiUser->updateAll(['is_approve' => 1],
                                        ['nothi_office' => $employee_office['office_id'],
                                        'potrojari_id' => $potrojari_id, 'nothi_part_no' => $nothi_part,
                                        'designation_id <>' => $employee_office['office_unit_organogram_id'],
                                        'is_approve' => 0, 'position_number' => $value['position']]);
                                }
                            }
                        }
                        $totalNotApproved = $tableSummaryNothiUser->find()
                            ->where(['nothi_office' => $employee_office['office_id'],
                                'potrojari_id' => $potrojari_id,
                                'nothi_part_no' => $nothi_part,
                                'is_approve' => 0])->count();


                        echo json_encode(array("status" => 'success','canpotrojari'=>$totalNotApproved));
                    }
                } else {
                    echo json_encode(array("status" => 'error', 'msg' => __("দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না")));
                }
            } else {
                echo json_encode(array("status" => 'error', 'msg' => __("দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না")));
            }
        } else {
            echo json_encode(array("status" => 'error', 'msg' => __("দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না")));
        }

        die;
    }

    public function getPosition($nothi_part = 0, $potrojari_id = 0)
    {

        $this->layout = null;
        $employee_office = $this->getCurrentDakSection();

        if ($this->request->is('post', 'ajax')) {

            if (empty($nothi_part) || empty($potrojari_id)) {
                echo json_encode(array("status" => 'error', 'msg' => __("দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না")));
                die;
            }

            $tableSummaryNothiUser = TableRegistry::get('SummaryNothiUsers');

            $checkExits = $tableSummaryNothiUser->find()->where(['nothi_office' => $employee_office['office_id'],
                'potrojari_id' => $potrojari_id, 'nothi_part_no' => $nothi_part, 'designation_id' => $employee_office['office_unit_organogram_id']])->first();

            if (!empty($checkExits)) {
                echo json_encode(array("status" => 'success', 'data' => $checkExits));
            } else {
                echo json_encode(array("status" => 'error', 'msg' => __("দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না")));
            }
        } else {
            echo json_encode(array("status" => 'error', 'msg' => __("দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না")));
        }
        die;
    }


    private function updateSummaryBody($potrojari_id, $body)
    {

        if (empty($body)) {
            return false;
        }
        if (empty($potrojari_id)) {
            return false;
        }
        TableRegistry::remove("Potrojari");
        $potrojariTable = TableRegistry::get("Potrojari");
        TableRegistry::remove('PotrojariAttachments');
        $potrojariAttachmentsTable = TableRegistry::get("PotrojariAttachments");
        TableRegistry::remove('PotrojariReceiver');
        $potrojariPotrojariReceiver = TableRegistry::get("PotrojariReceiver");

        $potrojari = $potrojariTable->get($potrojari_id);
        $potrojari->potro_description = $body;
        $potrojariTable->save($potrojari);
        $potrojariAttachmentsTable->updateAll(['content_body' => $body], ['potrojari_id' => $potrojari_id]);

        return true;
    }

    public function summaryNothiSarokTracking()
    {
    }

    public function getTracking()
    {
        if ($this->request->is('post', 'ajax')) {
            if (!empty($this->request->data['sarok_no'])) {
                $this->layout = null;
                $summaryNothiUsersTable = TableRegistry::get('SummaryNothiUsers');
                $data = $summaryNothiUsersTable->getTracking(h($this->request->data['sarok_no']));
                $this->set('data', $data);
            }
        } else {
            die;
        }
    }

    public function apiSummaryPotroShow($nothimasterid, $nothipotroid = 0, $nothi_office = 0)
    {
        $this->layout = 'online_dak';
        $user_designation = !empty($this->request->query['user_designation'])?$this->request->query['user_designation']:0;
        $deviceType = !empty($this->request->query['deviceType'])?$this->request->query['deviceType']:'Android';
        if($deviceType == 'IOS'){
            $this->view = 'api_summary_potro_show_ios';
        }
        $jsonArray = array(
            "status" => "error",
            "error_code" => 404,
            "message" => "Invalid Request"
        );

        $apikey = isset($this->request->query['api_key']) ? $this->request->query['api_key']
            : '';
        if (empty($apikey) || $this->checkToken($apikey) == FALSE) {
            echo json_encode($jsonArray);
            die;
        }

        if (!defined("PROJAPOTI_API") || PROJAPOTI_API == false) {
            echo json_encode($jsonArray);
            die;
        }
        if (empty($user_designation)) {
            echo json_encode($jsonArray);
            die;
        }

        if (empty($nothimasterid)) {
            echo json_encode($jsonArray);
            die;
        }
        if (empty($nothipotroid)) {
            echo json_encode($jsonArray);
            die;
        }


        $employee_office = $this->setCurrentDakSection($user_designation);

        $this->switchOffice($nothi_office, 'MainNothiOffice');

        $this->set('nothi_office', $nothi_office);
        $this->set('employee_office', $employee_office);
        TableRegistry::remove('NothiNotes');
        TableRegistry::remove('NothiParts');
        TableRegistry::remove('NothiPotroAttachments');
        $nothiMasterPermissionsTable = TableRegistry::get("NothiMasterPermissions");
        $nothiMasterCurrentUsersTable = TableRegistry::get("NothiMasterCurrentUsers");
        $nothiNotesTable = TableRegistry::get('NothiNotes');
        $nothiPartsTable = TableRegistry::get('NothiParts');

        $nothiPartsInformation = $nothiPartsTable->get($nothimasterid);

        $nothiMasters = array();
        $nothiMastersCurrentUser = array();

        if (empty($nothipotroid)) {
            $this->redirect(['controller' => 'NothiMasters', 'action' => 'index']);
        }

        $nothiMasters = $nothiMasterPermissionsTable->hasAccess($employee_office['office_id'],
            $employee_office['office_id'], $employee_office['office_unit_id'],
            $employee_office['office_unit_organogram_id'],
            $nothiPartsInformation['nothi_masters_id'], $nothimasterid);

        $nothiMastersCurrentUser = $nothiMasterCurrentUsersTable->find()->where(['nothi_part_no' => $nothimasterid,
            'office_id' => $employee_office['office_id'], 'office_unit_id' => $employee_office['office_unit_id'],
            'office_unit_organogram_id' => $employee_office['office_unit_organogram_id'],
            'nothi_office' => $employee_office['office_id']])->first();

        if (empty($nothiMasters) || $nothiMasters['privilige_type'] == 0) {
            $this->Flash->error("দুঃখিত! কোনো তথ্য পাওয়া যায়নি");
            $this->redirect(['controller' => 'Dashboard', 'action' => 'dashboard']);
        }

        $this->set('privilige_type', 0);
        $this->set('can_approve', 0);

        TableRegistry::remove('SummaryNothiUsers');
        $tableSummaryNothiUser = TableRegistry::get('SummaryNothiUsers');

        $allUsers = $tableSummaryNothiUser->find()->where(['nothi_part_no' => $nothimasterid, 'current_office_id' => $employee_office['office_id'],
            'potro_id' => $nothipotroid,
            'designation_id' => $employee_office['office_unit_organogram_id']])->hydrate(false)->first();

        $this->set('position_number', '');
        if (!empty($allUsers)) {
            $this->set('position_number', $allUsers['position_number']);
            $this->set('approved', $allUsers['is_approve']);
            $this->set('can_approve', $allUsers['is_sent'] == 0 ? $allUsers['can_approve'] : 0);
        }


        $nothiRecord = $nothiPartsTable->getAll('NothiParts.id=' . $nothimasterid)->first();

        $officeUnitTable = TableRegistry::get('OfficeUnits');
        $officeunit = $officeUnitTable->get($nothiRecord['office_units_id']);
        $officeUnitsName = $officeunit['unit_name_bng'];

        $this->set('officeUnitsName', $officeUnitsName);

        $this->set('nothiRecord', $nothiRecord);

        $template_list = $this->potrojari_template_list(2);
        $this->set(compact('template_list'));
        $coverTemplate = $this->potrojari_template_list(3);
        $this->set(compact('coverTemplate'));
        $this->set('nothiMasterInfo', $nothiRecord);
        $this->set(compact('nothimasterid'));
        if (!empty($nothiMastersCurrentUser)) {
            $this->set('privilige_type', 1);
        }

        $nothipotro_attachments = TableRegistry::get('NothiPotroAttachments');

        $attachments = $nothipotro_attachments->find()->where(['nothi_part_no' => $nothimasterid,
            'nothi_potro_id' => $nothipotroid, 'attachment_type <>' => 'text','is_summary_nothi'=>1])->toArray();

        $this->set('attachments', $attachments);

        $mainpotro = $nothipotro_attachments->find()->where(['nothi_part_no' => $nothimasterid,
            'nothi_potro_id' => $nothipotroid, 'attachment_type' => 'text','is_summary_nothi'=>1])->order(['id desc'])->first();

        if(empty($mainpotro)){
            echo json_encode($jsonArray);
            die;
        }

        $this->set('mainpotro', $mainpotro);

        $notesheedid = $nothiNotesTable->getLastNoteSheetId($nothimasterid,
            $nothi_office);

        $this->set(compact('nothimasterid', 'nothipotroid', 'notesheedid'));
    }

    public function apiDraftApprovedByMinistry($approve = 0, $nothi_office = 0)
    {
        $user_designation = $this->request->data['user_designation'];
        $jsonArray = array(
            "status" => "error",
            "error_code" => 404,
            "message" => "Invalid Request"
        );

        if (empty($user_designation)) {
            echo json_encode($jsonArray);
            die;
        }


        $employee_office = $this->setCurrentDakSection($user_designation);
        $this->switchOffice($nothi_office, 'MainNothiOffice');
        $this->layout = null;

        if ($this->request->is('post', 'ajax', 'put')) {

            $tableSummaryNothiUser = TableRegistry::get('SummaryNothiUsers');

            $allUsers = $tableSummaryNothiUser->find()->where([
                'nothi_part_no' => $this->request->data['nothi_part_no'],
                'potro_id' => $this->request->data['potro_id'], 'current_office_id' => $employee_office['office_id'],
                'designation_id' => $employee_office['office_unit_organogram_id'], 'is_sent' => 0])->hydrate(false)->first();

            $userInfo = array();
            if (!empty($allUsers)) {
                $usersTable = TableRegistry::get("Users");
                $userData = $usersTable->find()->where(['employee_record_id' => $employee_office['officer_id']])->first();

                $options['token']= sGenerateToken(['file'=>$userData['username']],['exp'=>time() + 60*300]);
                $exists = $this->getSignature($userData['username'],
                    1, 1,true,null,$options);

                if ($approve && !$exists) {
                    echo json_encode(array("status" => 'error', 'msg' => __("দুঃখিত! স্বাক্ষর পাওয়া যায় নি")));
                    die;
                }

                $options['token']= sGenerateToken(['file'=>$userData['username']],['exp'=>time() + 60*300]);
                $userInfo['signature'] = $this->getSignature($userData['username'],
                    1, 1,false,null,$options);
            }

            if (!empty($userInfo)) {
                $userInfo['will_sign'] = $approve;
                $userInfo['position'] = $allUsers['position_number'];

                echo json_encode(array("status" => 'success', 'data' => $userInfo));
            } else {
                echo json_encode(array("status" => 'error', 'msg' => __("দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না")));
            }
        }

        die;
    }

    public function apiDraftUpdateByMinistry($nothimasterid, $nothipotroid = 0,
                                          $nothi_office = 0)
    {
        $user_designation = $this->request->data['user_designation'];
        $jsonArray = array(
            "status" => "error",
            "error_code" => 404,
            "message" => "Invalid Request"
        );

        if (empty($user_designation)) {
            echo json_encode($jsonArray);
            die;
        }


        $employee_office = $this->setCurrentDakSection($user_designation);
        $this->switchOffice($nothi_office, 'MainNothiOffice');
        $this->layout = null;

        $this->set('nothi_office', $nothi_office);
        $nothiMasterPermissionsTable = TableRegistry::get("NothiMasterPermissions");

        $nothiMasters = array();
        $nothiMastersCurrentUser = array();

        $return = array("status" => 'error', 'msg' => "দুঃখিত! কোনো তথ্য পাওয়া যায়নি");

        if (empty($nothipotroid)) {
            echo json_encode($return);
            die;
        }

        TableRegistry::remove('NothiNotes');
        TableRegistry::remove('NothiParts');
        TableRegistry::remove('NothiPotroAttachments');
        $nothiPartsTable = TableRegistry::get('NothiParts');

        $nothiPartsInformation = $nothiPartsTable->get($nothimasterid);

        $nothiMasters = $nothiMasterPermissionsTable->hasAccess($employee_office['office_id'],
            $employee_office['office_id'], $employee_office['office_unit_id'],
            $employee_office['office_unit_organogram_id'],
            $nothiPartsInformation['nothi_masters_id'], $nothimasterid);
        if (empty($nothiMasters) || $nothiMasters['privilige_type'] == 0) {
            echo json_encode($return);
            die;
        }
        if ($this->request->is('ajax')) {
            if (!empty($this->request->data)) {

                try {

                    $potrojari_attachments = TableRegistry::get('NothiPotroAttachments');
                    $mainpotro = $potrojari_attachments->find()->where(['nothi_part_no' => $nothimasterid,
                        'nothi_potro_id' => $nothipotroid, 'attachment_type' => 'text'])->order(['id desc'])->first();
                    if (!empty($mainpotro)) {
                        $mainpotro->content_body = $this->request->data['contentbody2'];
                        $mainpotro->is_approved = 1;
                        $potrojari_attachments->save($mainpotro);
                        $potrojari_attachments->updateAll(['is_approved' => 1],
                            ['nothi_part_no' => $nothimasterid, 'nothi_potro_id' => $nothipotroid,
                                'attachment_type <>' => 'text']);

                        $tableSummaryNothiUser = TableRegistry::get('SummaryNothiUsers');

                        $allUsers = $tableSummaryNothiUser->find()->where([
                            'nothi_part_no' => $nothimasterid,
                            'potro_id' => $nothipotroid, 'current_office_id' => $employee_office['office_id'],
                            'designation_id' => $employee_office['office_unit_organogram_id'],
                            'is_sent' => 0])->hydrate(false)->first();

                        if (!empty($allUsers)) {
                            $tableSummaryNothiUser->updateAll(['is_approve' => 1], ['id' => $allUsers['id']]);
                        }

                        $return = array("status" => 'success', 'msg' => 'সার-সংক্ষেপ অনুমোদিত হয়েছে');

                        echo json_encode($return);
                        die;
                    } else {
                        echo json_encode($return);
                        die;
                    }
                } catch (Exception $ex) {
                    echo json_encode($return);
                    die;
                }
            }
        }
        die;
    }

    public function apiGetPosition($nothi_part = 0,$nothi_office, $potrojari_id = 0)
    {

        $user_designation = $this->request->data['user_designation'];
        $jsonArray = array(
            "status" => "error",
            "error_code" => 404,
            "message" => "Invalid Request"
        );

        if (empty($user_designation)) {
            echo json_encode($jsonArray);
            die;
        }
        $apikey = isset($this->request->data['api_key']) ? $this->request->data['api_key']
            : '';
        if (empty($apikey) || $this->checkToken($apikey) == FALSE) {
            echo json_encode($jsonArray);
            die;
        }

        $employee_office = $this->setCurrentDakSection($user_designation);
        $this->switchOffice($nothi_office, 'MainNothiOffice');
        $this->layout = null;

        if ($this->request->is('post', 'ajax')) {

            if (empty($nothi_part) || empty($potrojari_id)) {
                echo json_encode(array("status" => 'error', 'msg' => __("দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না")));
                die;
            }

            $tableSummaryNothiUser = TableRegistry::get('SummaryNothiUsers');

            $checkExits = $tableSummaryNothiUser->find()->where(['nothi_office' => $employee_office['office_id'],
                'potrojari_id' => $potrojari_id, 'nothi_part_no' => $nothi_part, 'designation_id' => $employee_office['office_unit_organogram_id']])->first();

            if (!empty($checkExits)) {
                echo json_encode(array("status" => 'success', 'data' => $checkExits));
            } else {
                echo json_encode(array("status" => 'error', 'msg' => __("দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না")));
            }
        } else {
            echo json_encode(array("status" => 'error', 'msg' => __("দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না")));
        }
        die;
    }

    public function apiDraftApproved($nothiMasterId, $referenceId, $approve = 0, $nothi_office = 0)
    {
        $user_designation = $this->request->data['user_designation'];
        $jsonArray = array(
            "status" => "error",
            "error_code" => 404,
            "message" => "Invalid Request"
        );

        if (empty($user_designation)) {
            echo json_encode($jsonArray);
            die;
        }
        $apikey = isset($this->request->data['api_key']) ? $this->request->data['api_key'] : '';
        if (empty($apikey) || $this->checkToken($apikey) == FALSE) {
            echo json_encode($jsonArray);
            die;
        }

        $employee_office = $this->setCurrentDakSection($user_designation);
        $this->switchOffice($nothi_office, 'MainNothiOffice');
        $this->layout = null;

        if ($this->request->is('post', 'ajax', 'put')) {

            if (empty($nothiMasterId)) {
                echo json_encode(array("status" => 'error', 'msg' => __("দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না")));
                die;
            }

            TableRegistry::remove("Potrojari");
            $potrojariTable = TableRegistry::get("Potrojari");
            $usersTable = TableRegistry::get("Users");

            $potrojari = $potrojariTable->find()->where([
                'nothi_part_no' => $nothiMasterId,
                'id' => $referenceId,
                'potro_status' => 'SummaryDraft',
            ])->first();


            if (!empty($potrojari)) {
                $userInfo = array();

                $userData = $usersTable->find()->where(['employee_record_id' => $employee_office['officer_id']])->first();

                $options['token']= sGenerateToken(['file'=>$userData['username']],['exp'=>time() + 60*300]);
                $exists = $this->getSignature($userData['username'],
                    1, 1, true,null,$options);

                if ($approve && !$exists) {
                    echo json_encode(array("status" => 'error', 'msg' => __("দুঃখিত! স্বাক্ষর পাওয়া যায় নি")));
                    die;
                }

                $tableSummaryNothiUser = TableRegistry::get('SummaryNothiUsers');
                $getData = $tableSummaryNothiUser->find()->where([
                    'potrojari_id' => $referenceId,
                    'nothi_part_no' => $nothiMasterId,
                    'current_office_id' => $employee_office['office_id'],
                    'designation_id' => $employee_office['office_unit_organogram_id'],
                    'can_approve' => 1])->first();

                if (!empty($getData)) {
                    try {

                        if (!empty($this->request->data['description'])) {
                            $potrojari->potro_description = $this->request->data['description'];
                            if (!$this->updateSummaryBody($potrojari->id, $potrojari->potro_description)) {
                                throw new \Exception("দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না");
                            }
                        }
                        $getData->is_approve = $approve;
                        $tableSummaryNothiUser->save($getData);
                        $userInfo['username'] = $userData['username'];
                        $userInfo['position'] = $getData['position_number'];
                        $userInfo['will_sign'] = $approve;
                        echo json_encode(array("status" => 'success', 'data' => $userInfo));
                    } catch (\Exception $ex) {
                        echo json_encode(array("status" => 'error', 'msg' => __("দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না")));
                    }
                } else {
                    echo json_encode(array("status" => 'error', 'msg' => __("দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না")));
                }
            } else {
                echo json_encode(array("status" => 'error', 'msg' => __("দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না")));
            }
        }

        die;
    }

    public function apiApprovalCheck($nothi_part = 0, $potrojari_id = 0, $nothi_office = 0)
    {
        $user_designation = $this->request->data['user_designation'];
        $jsonArray = array(
            "status" => "error",
            "error_code" => 404,
            "message" => "Invalid Request"
        );

        if (empty($user_designation)) {
            echo json_encode($jsonArray);
            die;
        }
        $apikey = isset($this->request->data['api_key']) ? $this->request->data['api_key'] : '';
        if (empty($apikey) || $this->checkToken($apikey) == FALSE) {
            echo json_encode($jsonArray);
            die;
        }

        $employee_office = $this->setCurrentDakSection($user_designation);
        $this->switchOffice($nothi_office, 'MainNothiOffice');

        $this->layout = null;

        if ($this->request->is('post', 'ajax', 'put')) {

            if (empty($nothi_part) || empty($potrojari_id)) {
                echo json_encode(array("status" => 'error', 'msg' => __("দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না")));
                die;
            }

            TableRegistry::remove("Potrojari");
            $potrojariTable = TableRegistry::get("Potrojari");


            $potrojari = $potrojariTable->find()->where([
                'nothi_part_no' => $nothi_part,
                'id' => $potrojari_id,
                'potro_status' => 'SummaryDraft',
            ])->first();


            if (!empty($potrojari)) {

                $tableSummaryNothiUser = TableRegistry::get('SummaryNothiUsers');
                $getData = $tableSummaryNothiUser->find()->where(['potrojari_id' => $potrojari_id,
                    'nothi_office' => $employee_office['office_id'],
                    'nothi_part_no' => $nothi_part,
                    'designation_id <>' => $employee_office['office_unit_organogram_id'],
                    'is_approve' => 0])->count();

                echo json_encode(array("status" => 'success', 'count' => $getData));

            } else {
                echo json_encode(array("status" => 'error', 'msg' => __("দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না")));
            }
        } else {
            echo json_encode(array("status" => 'error', 'msg' => __("দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না")));
        }

        die;
    }

    public function apiApprovalUsers($nothi_part = 0, $potrojari_id = 0, $nothi_office = 0)
    {
        $user_designation = $this->request->data['user_designation'];
        $jsonArray = array(
            "status" => "error",
            "error_code" => 404,
            "message" => "Invalid Request"
        );

        if (empty($user_designation)) {
            echo json_encode($jsonArray);
            die;
        }
        $apikey = isset($this->request->data['api_key']) ? $this->request->data['api_key'] : '';
        if (empty($apikey) || $this->checkToken($apikey) == FALSE) {
            echo json_encode($jsonArray);
            die;
        }

        $employee_office = $this->setCurrentDakSection($user_designation);
        $this->switchOffice($nothi_office, 'MainNothiOffice');
        $this->layout = null;

        if ($this->request->is('post', 'ajax', 'put')) {

            if (empty($nothi_part) || empty($potrojari_id)) {
                echo json_encode(array("status" => 'error', 'msg' => __("দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না")));
                die;
            }

            TableRegistry::remove("Potrojari");
            $potrojariTable = TableRegistry::get("Potrojari");


            $potrojari = $potrojariTable->find()->where([
                'nothi_part_no' => $nothi_part,
                'id' => $potrojari_id,
                'potro_status' => 'SummaryDraft',
            ])->first();


            if (!empty($potrojari)) {

                $tableSummaryNothiUser = TableRegistry::get('SummaryNothiUsers');
                $tableOfficeUnitOrganograms = TableRegistry::get('OfficeUnitOrganograms');
                $tableEmployeeRecords = TableRegistry::get('EmployeeRecords');
                $getData = $tableSummaryNothiUser->find()->where(['potrojari_id' => $potrojari_id,
                    'nothi_office' => $employee_office['office_id'],
                    'nothi_part_no' => $nothi_part,
                    'designation_id <>' => $employee_office['office_unit_organogram_id'],
                    'is_approve' => 0])->toArray();

                if (!empty($getData)) {
                    foreach ($getData as $key => &$value) {
                        $value['designation_name'] = $tableOfficeUnitOrganograms->getBanglaName($value['designation_id'])['designation_bng'];
                        $value['employee_name'] = $tableEmployeeRecords->getEmployeeInfoByRecordId($value['employee_record_id'])['name_bng'];
                    }
                }

                echo json_encode(array("status" => 'success', 'data' => $getData, 'potrojari' => $potrojari));

            } else {
                echo json_encode(array("status" => 'error', 'msg' => __("দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না")));
            }
        } else {
            echo json_encode(array("status" => 'error', 'msg' => __("দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না")));
        }

        die;
    }

    public function apiApprovalStatus($nothi_part = 0, $potrojari_id = 0, $nothi_office = 0)
    {
        $this->layout = null;
        $user_designation = $this->request->data['user_designation'];
        $jsonArray = array(
            "status" => "error",
            "error_code" => 404,
            "message" => "Invalid Request"
        );

        if (empty($user_designation)) {
            echo json_encode($jsonArray);
            die;
        }
        $apikey = isset($this->request->data['api_key']) ? $this->request->data['api_key'] : '';
        if (empty($apikey) || $this->checkToken($apikey) == FALSE) {
            echo json_encode($jsonArray);
            die;
        }

        $employee_office = $this->setCurrentDakSection($user_designation);
        $this->switchOffice($nothi_office, 'MainNothiOffice');

        if ($this->request->is('post', 'ajax')) {

            if (empty($nothi_part) || empty($potrojari_id)) {
                echo json_encode(array("status" => 'error', 'msg' => __("দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না")));
                die;
            }

            TableRegistry::remove("Potrojari");
            $potrojariTable = TableRegistry::get("Potrojari");
            $tableSummaryNothiUser = TableRegistry::get('SummaryNothiUsers');

            $checkExits = $tableSummaryNothiUser->find()->where(['nothi_office' => $employee_office['office_id'],
                'potrojari_id' => $potrojari_id, 'nothi_part_no' => $nothi_part,
                'designation_id' => $employee_office['office_unit_organogram_id'], 'is_approve' => 1])->count();

            $potrojari = $potrojariTable->get($potrojari_id);

            if (!empty($checkExits) && !empty($potrojari) && $potrojari->nothi_part_no == $nothi_part && $potrojari->potro_status == 'SummaryDraft') {

                $data = $this->request->data;

                if (!empty($data['description'])) {
                    if ($this->updateSummaryBody($potrojari_id, $data['description'])) {
                        if (!empty($data['data'])) {
                            foreach ($data['data'] as $key => $value) {
                                if ($value['status'] == 1) {
                                    $tableSummaryNothiUser->updateAll(['is_approve' => 1], ['nothi_office' => $employee_office['office_id'],
                                        'potrojari_id' => $potrojari_id, 'nothi_part_no' => $nothi_part,
                                        'designation_id <>' => $employee_office['office_unit_organogram_id'],
                                        'is_approve' => 0, 'position_number' => $value['position']]);
                                }
                            }
                        }
                        $totalNotApproved = $tableSummaryNothiUser->find()
                            ->where(['nothi_office' => $employee_office['office_id'],
                                'potrojari_id' => $potrojari_id,
                                'nothi_part_no' => $nothi_part,
                                'is_approve' => 0])->count();

                        echo json_encode(array("status" => 'success','canpotrojari'=>$totalNotApproved));
                    }
                } else {
                    echo json_encode(array("status" => 'error', 'msg' => __("দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না")));
                }
            } else {
                echo json_encode(array("status" => 'error', 'msg' => __("দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না")));
            }
        } else {
            echo json_encode(array("status" => 'error', 'msg' => __("দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না")));
        }

        die;
    }


}