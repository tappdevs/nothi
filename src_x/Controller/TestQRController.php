<?php
    namespace App\Controller;

    use Cake\Core\Controller;

    class TestQRController extends AppController
    {
        public function index()
        {
            $this->loadComponent('QRGenerator');
            $qr_data = $this->QRGenerator->qrcode("hello world");
            print_r($qr_data);
            echo "<hr>";
            echo "<img src=".$qr_data['file_path'].">";
            die;
        }
    }

?>