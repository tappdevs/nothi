<?php

namespace App\Controller;

use App\Controller\ProjapotiController;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\TableRegistry;

class UserRolesController extends ProjapotiController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
    }

    public function index()
    {
        $query = $this->UserRoles->find('all');
        $this->set('userTypes', $query);
    }

    public function add()
    {
        $office_origin_unit_organograms_tbl = TableRegistry::get('OfficeOriginUnitOrganograms');
        $origin_organogram = $office_origin_unit_organograms_tbl->find()->select(['designation_eng', 'id'])->group('designation_eng')->toArray();
        $origin_organogram_array = array();

        if (!empty($origin_organogram)) {
            foreach ($origin_organogram as $k => $val) {
                $origin_organogram_array[] = $val['designation_eng'];
            }
        }
        $this->set('origin_organogram_array', json_encode($origin_organogram_array));

        $userType = $this->UserRoles->newEntity();
        if ($this->request->is('post')) {
            $userType = $this->UserRoles->patchEntity($userType, $this->request->data);
            if ($this->UserRoles->save($userType)) {
                $this->Flash->success(__('The user role has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Unable to add the user role.'));
        }
        $this->set('userType', $userType);
    }

    public function edit($id = null)
    {
        $office_origin_unit_organograms_tbl = TableRegistry::get('OfficeOriginUnitOrganograms');
        $origin_organogram = $office_origin_unit_organograms_tbl->find()->select(['designation_eng', 'id'])->group('designation_eng')->toArray();
        $origin_organogram_array = array();

        if (!empty($origin_organogram)) {
            foreach ($origin_organogram as $k => $val) {
                $origin_organogram_array[] = $val['designation_eng'];
            }
        }
        $this->set('origin_organogram_array', json_encode($origin_organogram_array));

        $userType = $this->UserRoles->get($id);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $userType = $this->UserRoles->patchEntity($userType, $this->request->data);
            if ($this->UserRoles->save($userType)) {
                $this->Flash->success('The user role has been updated.');
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error('Unable to update user role. Please, try again.');
        }
        $this->set('userType', $userType);
    }

    public function delete($id)
    {
        $userTable = TableRegistry::get('Users');
        $userType = $this->UserRoles->get($id);
        if (!empty($userType)) {
            $isLinked = $userTable->getUserInfobyRole($id);
            if (!$isLinked) {
                $result = $this->UserRoles->delete($userType);
                if ($result == 1) {
                    $this->Flash->success(__('The user role has been deleted.'));
                } else {
                    $this->Flash->error(__('Unable to delete the user role.'));
                }
            } else {
                $this->Flash->error(__('The user role is linked with one or more user.'));
            }
        } else {
            $this->Flash->error(__('Invalid request!'));
        }
        return $this->redirect(['action' => 'index']);
    }

}
