<?php

namespace App\Controller;

use App\Controller\ProjapotiController;
use App\Model\Table\OfficesTable;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;
use Cake\Utility\String;
use Cake\Auth\DefaultPasswordHasher;
use Cake\I18n\Time;
use Cake\Cache\Cache;

class UsersController extends ProjapotiController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        // Allow users to register and logout.
        // You should not add the "login" action to allow list. Doing so would
        // cause problems with normal functioning of AuthComponent.
        $this->Auth->allow(['login', 'APILogin', 'logout', 'forgetpassword', 'APISignature',
            'APIgetDesignantions', 'clearAllCache', 'postAPILogin', 'postAPIgetDesignantions',
            'apiLoginHistorySave', 'resetPassword', 'renewPassword', 'accountVerify','verifyDevice','postApiLoginHistorySave','postAPISetFirebaseToken','postApiNotifyUser','sslLogin']);

        $user = $this->Auth->user();
        if ($user['user_role_id'] > 2) {
            if (in_array($this->request->action,
                ['add', 'edit', 'delete', 'index', 'activation', 'view'])) {
                $this->Flash->error(__('Unauthorized Access'));
                $this->redirect(['action' => 'dashboard', 'controller' => 'dashboard']);
            }
        }

        if (!empty($user)) {
            if ($this->request->action == 'login') {
                $this->Flash->error(__('Unauthorized Access'));
                $this->redirect(['action' => 'dashboard', 'controller' => 'dashboard']);
            }
        }
    }

    public function login()
    {
    	$video_tutorial_links = [
			['id' => '_gU_2BGa28w', 'name' => 'নথি ১ - নথি সিস্টেমে লগইন ও পরিচিতি', 'url' => 'https://youtube.com/embed/_gU_2BGa28w'],
			['id' => 'rxoS55PAQBI', 'name' => 'নথি ২ - প্রোফাইল এডিট করা', 'url' => 'https://youtube.com/embed/rxoS55PAQBI'],
			['id' => 'EMz_fHYCom4', 'name' => 'নথি ৩ - সিস্টেমে ডাক আপলোড করা', 'url' => 'https://youtube.com/embed/EMz_fHYCom4'],
			['id' => 'xfC3QkoSsbs', 'name' => 'নথি ৪ - আগত ডাক দেখা ও সিদ্ধান্ত দেয়া', 'url' => 'https://youtube.com/embed/xfC3QkoSsbs'],
			['id' => 'jMlRuCDsoOY', 'name' => 'নথি ৫ - আগত ডাক নথিতে উপস্থাপন', 'url' => 'https://youtube.com/embed/jMlRuCDsoOY'],
			['id' => 'IBaNz6HEL-4', 'name' => 'নথি ৬ - আগত নথি দেখা ও সিদ্ধান্ত দেয়া', 'url' => 'https://youtube.com/embed/IBaNz6HEL-4'],
			['id' => 'edDnoU7EouA', 'name' => 'নথি ৭ - খসড়াপত্র তৈরি ও প্রেরণ', 'url' => 'https://youtube.com/embed/edDnoU7EouA'],
			['id' => 'CstqbJ_-aGg', 'name' => 'নথি ৮ - খসড়াপত্র অনুমোদন ও পত্রজারির জন্য প্রেরণ', 'url' => 'https://youtube.com/embed/CstqbJ_-aGg'],
			['id' => 'hZiZDcR6_sQ', 'name' => 'নথি ৯ - নথি সিস্টেমে পত্রজারি ও প্রেরণ', 'url' => 'https://youtube.com/embed/hZiZDcR6_sQ'],
			['id' => 'CI--x1gfOqw', 'name' => 'নথি ১০ - নোট নিষ্পত্তি', 'url' => 'https://youtube.com/embed/CI--x1gfOqw'],
			['id' => 's1R-dzq9PHE', 'name' => 'নথি ১১.১ - অফিস এডমিন (কর্মকর্তা-কর্মচারীর তালিকা দেখা, ইউজার আইডি তৈরি, রিলিজ, এসাইন)', 'url' => 'https://youtube.com/embed/s1R-dzq9PHE'],
			['id' => 'j5druwosldw', 'name' => 'নথি ১১.২ - অফিস এডমিন (ড্যাশবোর্ড দেখার অনুমতি, কর্মকর্তাদের ইংরেজি পদবী সংশোধন, অফিস প্রধান বাছাই, অফিস ফ্রন্ট ডেস্ক নির্বাচন, অফিস তথ্য সংশোধন)', 'url' => 'https://youtube.com/embed/j5druwosldw'],
			['id' => 'wz0wt2VWk3M', 'name' => 'নথি ১১.৩ - অফিস এডমিন (অফিস কাঠামো ট্রান্সফার, শাখা তথ্য সংশোধন,পদবীর স্তর ও ক্রম ির্বাচন, রিপোর্ট সংগ্রহ)', 'url' => 'https://youtube.com/embed/wz0wt2VWk3M'],
			['id' => 'GIkoC9Qz0Ps', 'name' => 'নথি ১১.৪ - অফিস এডমিন (প্রতিকল্প সেট করা)', 'url' => 'https://youtube.com/embed/GIkoC9Qz0Ps'],
			['id' => 'Q5fqmlsVfto', 'name' => 'নথি ১১.৫ - অফিস এডমিন (নথির তথ্য ও পত্রজারি হেডিং সংশোধন)', 'url' => 'https://youtube.com/embed/Q5fqmlsVfto'],
		];
    	$this->set(compact('video_tutorial_links'));
        // maintenance
        if(date('Y-m-d') == '2019-01-23' && date('G') <= 20 && date('G') >= 18 && Live == 1){
             $this->layout = 'login_new';
            $this->view = 'maintenance';
            return;
        } else {
			$this->layout = 'login';
		}
        if(defined('NewLogin') && NewLogin == 1){
            $this->layout = 'login_new';
            $this->view = 'login_new';
        }
        elseif(defined('NewLogin') && NewLogin == 2){
            $this->view = 'login_new2';
            $this->layout = 'login_new2';
        }
        // maintenance
         if ($this->request->is('get')){
             if(defined('SSO_LOGIN') && SSO_LOGIN && empty($this->request->query['service'])){
                 return $this->redirect('/sso-login');
             }
            //SSL request check
            if (defined("Live") && Live == 1) {
                if(!(!empty($_SERVER['HTTP_X_SSL_CIPHER']) || env('HTTPS') || $this->request->is('ssl') == 1 || $this->request->is('ssl') == 'on' || $this->request->is('ssl') == true)){
                  return  $this->redirect(['controller' => 'users', 'action' => 'sslLogin']);
                }
            }
             $user = TableRegistry::get('Users')->newEntity();
             $this->set(compact('user'));
         }

        $office_messages = Cache::read('login_page_settings', 'loginSettingsCache');

        $this->set('office_messages',$office_messages);
        if ($this->request->is('post')) {

             if(!empty($this->request->data['username'])) {
                 $this->request->data['username'] = trim($this->request->data['username']);


                 $bn_digits = array('০', '১', '২', '৩', '৪', '৫', '৬', '৭', '৮', '৯');
                 $output = str_replace($bn_digits, range(0, 9),
                     $this->request->data['username']);

                 $this->request->data['username'] = $output;
             }

            try {
                $user = [];
                if (!empty($this->request->data['alias'])) {
                    $user = $this->Users->find()->where(['user_alias' => $this->request->data['alias'], 'active'=>1])->hydrate(false)->first();
                }
                if (!empty($user)) {
                    $this->request->data['username'] = $user['username'];
                }
                $user = $this->Auth->identify();
                if($user && !$user['active']) {
                    $this->Flash->error(__('দুঃখিত! আপনার আইডিটি এই মুহূর্তে নিষ্ক্রিয় আছে, আইডিটি সক্রিয় করার জন্য অফিস এডমিন অথবা সাপোর্ট এ যোগাযোগ করুন।'));
                    return $this->redirect($this->referer());
                }

                if ($user) {

                    $this->Auth->setUser($user);
                    if ($this->request->data['password'] == DEFAULT_PASSWORD) {

                        $uservalue = $this->Users->get($user['id']);
                        $uservalue->force_password_change = 1;
                        $uservalue = $this->Users->save($uservalue);

                        unset($uservalue->password);
                        $this->Auth->setUser($uservalue->toArray());
                    }

//                    $moduleInfos = TableRegistry::get('ModuleInfos');
//                    $modules = $moduleInfos->getModuleList();
                    $modules = jsonA(MODULES);

                    $session = $this->request->session();
                    $session->write('modules', $modules);

//                    $menus = TableRegistry::get('ModuleMenus');
//                    if ($user['user_role_id'] > 2) {
//                        $menus = $menus->setupModuleMenu(5);
//                    } else {
//                        $menus = $menus->setupModuleMenu(1);
//                    }
//                    $session->write('menu_items', $menus);

                    if ($user['user_role_id'] > 2) {
                        $session->write('module_id', 5);
                    } else {
                        $session->write('module_id', 1);
                    }
                    

                    if ($user['user_role_id'] > 2) {
                        $table_instance_emp_offices = TableRegistry::get('EmployeeOffices');
                        if (($employee_office_records = Cache::read('emp_offc_rcds_' . $user['employee_record_id'], 'memcached')) === false) {
                            $employee_office_records = $table_instance_emp_offices->getEmployeeOfficeRecords($user['employee_record_id']);
                            Cache::write('emp_offc_rcds_' . $user['employee_record_id'], $employee_office_records, 'memcached');
                        }

                        if (!empty($this->request->data['fingerprint'])) {
                            foreach ($employee_office_records as $key => $value) {
                                $value['fingerprint'] = $this->request->data['fingerprint'];
                            }
                        }

                        $this->saveLoginHistory($employee_office_records);
                    }

                    if (defined('REDIRECT') && REDIRECT == false) {

                    } else {
                        if ($user['user_role_id'] > 2) {

                            $table_instance_emp_offices = TableRegistry::get('EmployeeOffices');

                            $employee_office_records = $table_instance_emp_offices->getEmployeeOfficeRecords($user['employee_record_id']);
                            if (!empty($employee_office_records)) {

                                $officeDomainTable = TableRegistry::get('OfficeDomains');
                                $officeDomain = $officeDomainTable->find()->where(['office_id' => $employee_office_records[0]['office_id']])->first();

                                if (!empty($officeDomain) && $subdomain = $this->checkSubdomain()) {

                                    if ($subdomain == -1 || $subdomain != $officeDomain['domain_prefix']) {

                                        $this->redirect($officeDomain['domain_url']);
                                    } else {

                                    }
                                }
                            }
                        }
                    }

                    return $this->redirect('/');
                }
                $this->Flash->error(__('দুঃখিত! লগইন আইডি অথবা পাসওয়ার্ড সঠিক নয়'));
            } catch (\Exception $ex) {
                $this->Flash->error($ex->getMessage());
            }
        }
        if ($this->Auth->user()) {
            return $this->redirect('/');
        }
    }

    public function logout()
    {
        if(defined('SSO_LOGIN') && SSO_LOGIN){
            return $this->redirect('/sso-logout');
        }
        $session = $this->request->session();
        $employee_office = $this->getCurrentDakSection();
        if(!empty($employee_office)){
            $this->updateLoginHistory($employee_office['officer_id']);
        }
        $session->destroy();
         if(Live == 1){
            $this->Auth->config('logoutRedirect',"https://nothi.gov.bd/login");
        }
        return $this->redirect($this->Auth->logout());
    }
    public function sslLogin()
    {
        if ($this->Auth->user()) {
            return $this->redirect($this->Auth->redirectUrl());
        }
        $this->layout='ssl_login';
    }

    public function index()
    {

        $user = $this->Auth->user();

        if ($user['user_role_id'] > 2) {
            $this->redirect(['controller' => 'dashboard', 'action' => 'dashboard']);
        }
        $query = $this->Users->find('all')->where(['Users.user_role_id <>' => 3])->contain(['UserRoles']);
        $this->set('users', $this->paginate($query));
    }

    public function view($id)
    {
        $user = $this->Auth->user();

        if ($user['user_role_id'] > 2) {
            $this->redirect(['controller' => 'dashboard', 'action' => 'dashboard']);
        }
        if (!$id) {
            throw new NotFoundException(__('Invalid user'));
        }

        $user = $this->Users->get($id);
        $this->set(compact('user'));
    }

    public function add()
    {
        $user = $this->Auth->user();

        if ($user['user_role_id'] > 2) {
            $this->redirect(['controller' => 'dashboard', 'action' => 'dashboard']);
        }
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            $user->user_alias = $user->username;
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(['action' => 'add']);
            }
            $this->Flash->error(__('Unable to add the user.'));
        }
        $this->set('user', $user);
        $UTTable = TableRegistry::get('UserRoles');
        $userTypes = $UTTable->find('list')->select(['id, name'])->toArray();
        $this->set('userTypes', $userTypes);
    }

    public function edit($id = null)
    {
        $user = $this->Auth->user();

        if ($user['user_role_id'] > 2) {
            $this->redirect(['controller' => 'dashboard', 'action' => 'dashboard']);
        }
        $user = $this->Users->get($id);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success('The user has been updated.');
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error('Unable to update user. Please, try again.');
        }
        $this->set('user', $user);
        $UTTable = TableRegistry::get('UserRoles');
        $userTypes = $UTTable->find('list')->select(['id, name'])->toArray();
        $this->set('userTypes', $userTypes);
    }

    public function delete($id)
    {
        $user = $this->Auth->user();

        if ($user['user_role_id'] > 2) {
            $this->redirect(['controller' => 'dashboard', 'action' => 'dashboard']);
        }
        $user = $this->Users->get($id);
        $result = $this->Users->delete($user);
        if ($result == 1) {
            $this->Flash->success(__('The user has been deleted.'));
            return $this->redirect(['action' => 'index']);
        }
        $this->Flash->error(__('Unable to delete the user.'));
    }

    public function forgetPassword()
    {
        $employeeRecordsTable = TableRegistry::get('EmployeeRecords');
        if ($this->request->is('post', 'ajax') && !empty($this->request->data)) {

            $loginId = trim($this->request->data['username']);

            $bn_digits = array('০', '১', '২', '৩', '৪', '৫', '৬', '৭', '৮', '৯');
            $loginId = str_replace($bn_digits, range(0, 9), $loginId);

            if (!empty($loginId)) {
                $users = $this->Users->find()->where(['username' => $loginId])->first();

                if (!empty($users)) {
                    try {
                        $employeeinfo = $employeeRecordsTable->get($users['employee_record_id']);

                        if (!empty($employeeinfo)) {
                            if ($employeeinfo['personal_email'] != $this->request->data['email']) {
                                echo json_encode(array('status' => 'error', 'msg' => 'দুঃখিত! আপনি ভুল তথ্য দিয়ে চেষ্টা করছেন।'));
                                die;
                            }
                            $users->email_verify_code = String::uuid();
                            $users->is_email_verified = 0;
                            $users->verification_date = date("Y-m-d");

                            $emailvarificationcode = sGenerateToken([
                                'token'=>$users->email_verify_code
                            ],['exp'=>(time() + (3600*24*3))]);

                            if(defined('Live') && Live == 1){
                                $url = Router::url(['action' => 'resetPassword', 'controller' => 'Users', '?' => ['token' => $emailvarificationcode, 'username' => $users->username],'_ssl' => true], true);
                            }else{
                                $url = Router::url(['action' => 'resetPassword', 'controller' => 'Users', '?' => ['token' => $emailvarificationcode, 'username' => $users->username]], true);
                            }

                            $isSent = 0;

                            if (!empty($employeeinfo['personal_email'])) {
                                sendMailsByCron($employeeinfo['personal_email'], '', 'Password reset', '', "জনাব " . $employeeinfo['name_bng'] . "<br/><br/>আপনার পাসওয়ার্ড পরিবর্তনের জন্য <a href=\"{$url}\" >এইখানে </a> ক্লিক করুন।<br/><br/>ধন্যবাদ।");
                                $isSent = 1;
                            }

                            if ($isSent) {
                                $this->Users->save($users);

                                echo json_encode(array('status' => 'success', 'msg' => 'পাসওয়ার্ড রিসেট লিঙ্ক ইমেইল (ইনবক্স অথবা স্প্যাম/যাঙ্ক) পাঠানো হয়েছে।'));
                            } else {
                                echo json_encode(array('status' => 'error', 'msg' => 'পাসওয়ার্ড রিসেট লিঙ্ক পাঠানো সম্ভব হচ্ছে না। অনুগ্রহ করে এডমিন এর সাথে যোগাযোগ করুন'));
                            }
                        } else {
                            echo json_encode(array('status' => 'error', 'msg' => 'পাসওয়ার্ড রিসেট লিঙ্ক পাঠানো সম্ভব হচ্ছে না। অনুগ্রহ করে এডমিন এর সাথে যোগাযোগ করুন'));
                        }
                    } catch (Exception $ex) {
                        echo json_encode(array('status' => 'error', 'msg' => 'দুঃখিত! লগইন আইডি সঠিক নয়'));
                    } catch (\Exception $e) {
                        echo json_encode(array('status' => 'error', 'msg' => 'দুঃখিত! লগইন আইডি সঠিক নয়'));
                    }
                } else {
                    echo json_encode(array('status' => 'error', 'msg' => 'দুঃখিত! লগইন আইডি সঠিক নয়'));
                }
            }
        }

        die;
    }

    public function activation()
    {

        $user = $this->Auth->user();

        if ($user['user_role_id'] > 2) {
            $this->redirect(['controller' => 'dashboard', 'action' => 'dashboard']);
        }

        if ($this->request->is('post')) {

            /* Lookup user by activation code supplied */
            $activation_code = $this->request->data['activation_code'];

            $user_act_emails = TableRegistry::get('UserActivationEmails');
            $user_act_entity = $user_act_emails->find()->where(['activation_code' => $activation_code])->first();

            $user_act_sms = TableRegistry::get('UserActivationSMS');
            $user_act_entity2 = $user_act_sms->find()->where(['activation_code' => $activation_code])->first();

            if ((empty($user_act_entity->id)) && (empty($user_act_entity2->id))) {
                $this->Flash->error(__('Invalid activation code, please try again'));
            } else {
                $current_time = time();
                if (!(empty($user_act_entity->id))) {
                    // email
                    if ($current_time > $user_act_entity->expiry) {
                        $this->Flash->error(__('Activation code you tried already expired'));
                        return;
                    } else {
                        $user_act_entity->is_activated = 1;
                        $user_act_emails->save($user_act_entity);
                        $user_id = $user_act_entity->user_id;
                    }
                } else if (!(empty($user_act_entity2->id))) {
                    // sms
                    if ($current_time > $user_act_entity2->expiry) {
                        $this->Flash->error(__('Activation code you tried already expired'));
                        return;
                    } else {
                        $user_act_entity2->is_activated = 1;
                        $user_act_sms->save($user_act_entity2);
                        $user_id = $user_act_entity2->user_id;
                    }
                }
                // Pass the user_id to map with password owner for change password
                // do something with the $user_id :'(';
                // send an activation
                $session = $this->request->session();
                $session->write('user_id', $user_id);

                return $this->redirect(['action' => 'choosepassword']);
            }
        }
    }

    public function choosepassword($id = null)
    {

        if ($this->request->is(['patch', 'post', 'put'])) {
            //$user_x = base64_decode($this->request->query["__h"]);
            //die($this->request->data['id']);
            $user = $this->Users->get($this->request->data['id']);
            $user = $this->Users->patchEntity($user, $this->request->data, ['validate' => 'password']);
            if ($this->Users->save($user)) {
                $this->Flash->success('Password has been changed.');
                return $this->redirect(['action' => 'login']);
            }
            $this->Flash->error('Unable to update user. Please, try again.');
        }
    }

    public function changepassword($id = null)
    {
        $user = $this->Users->get($id);

        $users = $this->Auth->user();

        if ($users['user_role_id'] > 2 && $user['username'] != $users['username']) {

            $this->redirect(['controller' => 'dashboard', 'action' => 'dashboard']);
        }

        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->data, ['validate' => 'password']);
            if ($this->Users->save($user)) {
                $this->Flash->success('Password has been changed.');
                return $this->redirect(['action' => 'logout']);
            }
            $this->Flash->error('Unable to update user. Please, try again.');
        }
        $this->set('user', $user);
    }

    public function renewPassword()
    {
        try {
            if (empty($this->request->data['id'])) {
                throw new \Exception('দুঃখিত! ভুল ব্যবহারকারী পুনরায় চেষ্টা করুন।');
            } elseif (empty($this->request->data['token'])) {
                throw new \Exception('দুঃখিত! ভুল টোকেন। পুনরায় চেষ্টা করুন।');
            }

            $user = $this->Users->get($this->request->data['id']);

            if (empty($user)) {
                throw new \Exception('দুঃখিত! ভুল ব্যবহারকারী পুনরায় চেষ্টা করুন।');
            } else {

                $dateVerication = new Time($user['verification_date']);
                $dateToday = new Time();
                $daydiff = $dateVerication->diffInDays($dateToday);

                if ($daydiff > 3) {
                    throw new \Exception('দুঃখিত! টোকেন ৩ দিন এর বেশি পুরানো। পুনরায় চেষ্টা করুন।');
                }

                $check = $this->checkToken($this->request->data['token']);
                if(!$check)
                {
                    throw new \Exception('দুঃখিত! ভুল টোকেন। পুনরায় চেষ্টা করুন।');
                }
                $decrypt = getTokenData($this->request->data['token']);

                $token = ($decrypt['data']['token'] == $user['email_verify_code'] ? true : false);
                if (!$token) {
                    throw new \Exception('দুঃখিত! ভুল টোকেন। পুনরায় চেষ্টা করুন।');
                }

                if ($this->request->data['password'] !== $this->request->data['cpassword']) {
                    throw new \Exception('দুঃখিত! পাসওয়ার্ড ভুল হয়েছে।');
                }
                $user->password = $this->request->data['password'];
                $user->email_verify_code = '';
                $user->is_email_verified = 1;
                $user->verification_date = date("Y-m-d");
                $success = $this->Users->passwordValidation($this->request->data['password']);
                if ($success === true && $this->Users->save($user)) {
                    $employeeRecordsTable = TableRegistry::get('EmployeeRecords');
                    $employeeinfo = $employeeRecordsTable->get($user['employee_record_id']);

                    $this->SendEmailMessage($employeeinfo['personal_email'],
                        "জনাব " . $employeeinfo['name_bng'] . "<br/><br/>পাসওয়ার্ড পরিবর্তন করা হয়েছে' <br/><br/>ধন্যবাদ।",
                        'New password');
                    $this->Flash->success('পাসওয়ার্ড পরিবর্তন করা হয়েছে');
                }else{
                    if($success === 0){
                        throw  new \Exception ("পাসওয়ার্ড নূন্যতম ৬ অক্ষরের হতে হবে।");
                    }
                    if($success === 1){
                        throw  new \Exception ("অন্তত একটি A-Z অথবা a-z থাকতে হবে। ");
                    }
                }
            }
        } catch (\Exception $ex) {
            $this->Flash->error($ex->getMessage());
        }
        return $this->redirect($this->Auth->redirectUrl());
    }

    public function resetPassword()
    {
        $this->layout = 'dashboard';
        try {
            if (empty($this->request->query['username'])) {
                throw new \Exception('Invalid Username. Please, try again.');
            } elseif (empty($this->request->query['token'])) {
                throw new \Exception('দুঃখিত! ভুল টোকেন। পুনরায় চেষ্টা করুন।');
            }

            $user = $this->Users->findByUsername($this->request->query['username'])->first();
            $token = '';
            if (empty($user)) {
                throw new \Exception('দুঃখিত! ভুল টোকেন। পুনরায় চেষ্টা করুন।');
            } else {

                $dateVerication = new Time($user['verification_date']);
                $dateToday = new Time();
                $daydiff = $dateVerication->diffInDays($dateToday);

                if ($daydiff > 3) {
                    throw new \Exception('দুঃখিত! টোকেন ৩ দিন এর বেশি পুরানো। পুনরায় চেষ্টা করুন।');
                }
                $check = $this->checkToken($this->request->query['token']);
                if(!$check)
                {
                    throw new \Exception('দুঃখিত! ভুল টোকেন। পুনরায় চেষ্টা করুন।');
                }
                $decrypt = getTokenData($this->request->query['token']);

                $token = ($decrypt['data']['token'] == $user['email_verify_code'] ? true : false);
                if (!$token) {
                    throw new \Exception('দুঃখিত! ভুল টোকেন। পুনরায় চেষ্টা করুন।');
                }
            }
            $this->set('token', $this->request->query['token']);
            $this->set('user', $user);
        } catch (\Exception $ex) {

            $this->Flash->error($ex->getMessage());
            return $this->redirect($this->Auth->redirectUrl());
        }
    }


    public function loginAudit()
    {

        $user = $this->Auth->user();
        $office_id = 0;
        $office_name = '';
        if ($user['user_role_id'] > 2) {
            $employee_office = $this->getCurrentDakSection();
            $office_id = $employee_office['office_id'];
            $office_name = $employee_office['office_name'];
        }

        $this->set(compact('user', 'office_name', 'office_id'));
    }

    public function loginAuditAjax()
    {
        $loginHistoryTable = TableRegistry::get('UserLoginHistory');

        $user = $this->Auth->user();

        $page = 1;
        $limit = 50;
        $office_id = 0;
        $office_unit_id = 0;
        $office_unit_organogram_id = 0;
        $start_time = $end_time = '';

        if ($user['user_role_id'] > 2) {
            $is_superAmdin = false;
            $employee_office = $this->getCurrentDakSection();
            $office_id = $employee_office['office_id'];
        } else {
            $is_superAmdin = true;
        }
        $this->set(compact('is_superAmdin'));
        $this->layout = 'blank';
        if ($this->request->is('ajax')) {

            if (!empty($this->request->query)) {

                if (!empty($this->request->query['page'])) {
                    $page = intval($this->request->query['page']);
                }

                if (!empty($this->request->query['office_id'])) {
                    $office_id = intval($this->request->query['office_id']);
                }

                if ($user['user_role_id'] > 2) {
                    $office_id = $employee_office['office_id'];
                }

                if (!empty($this->request->query['office_unit_id'])) {
                    $office_unit_id = intval($this->request->query['office_unit_id']);
                }

                if (!empty($this->request->query['office_unit_organogram_id'])) {
                    $office_unit_organogram_id = intval($this->request->query['office_unit_organogram_id']);
                }

                if (!empty($this->request->query['start_time'])) {
                    $start_time = ($this->request->query['start_time']);
                }

                if (!empty($this->request->query['end_time'])) {
                    $end_time = ($this->request->query['end_time']);
                }
            }
        }

        $loginHistory = $this->Paginator->paginate($loginHistoryTable->getLogingHistory($office_id,
            $office_unit_id, $office_unit_organogram_id,
            [$start_time, $end_time]), ['limit' => $limit]);

        $this->set(compact('loginHistory', 'page', 'limit'));
    }

    public function clearAllCache()
    {
        \Cake\Cache\Cache::clear(false);
        \Cake\Cache\Cache::clear(false, 'memcached');

        die("All cache are cleared");
    }

    public function migrateUserLoginHistrory()
    {
        set_time_limit(0);
        ini_set('memory_limit', -1);
        $table = TableRegistry::get('UserLoginHistory');
        $OfficesTable = TableRegistry::get('Offices');
        $allData = $table->find()->select(['office_id'])->group(['office_id'])->toArray();
        if (!empty($allData)) {
            foreach ($allData as $val) {
                $allInfo = $OfficesTable->find()->select(['office_ministry_id', 'office_origin_id', 'office_layer_id'])->where(['id' => $val['office_id']])->first();
                $table->updateAll(['ministry_id' => $allInfo['office_ministry_id'], 'origin_id' => $allInfo['office_origin_id'], 'layer_id' => $allInfo['office_layer_id']], ['office_id' => $val['office_id']]);
            }
        }
        echo 'Done';
        die;
    }
    public function adminPasswordChange($employee_record = 0){

        if (empty($employee_record)) {
        $this->response->type('application/json');
        $this->response->body(json_encode(array('status' => 'error', 'msg' => 'ব্যাবহারকারীর কোন তথ্য পাওয়া যায়নি।')));
        return $this->response;
        }
        $user = $this->Auth->user();
        if (empty($user) || $user['user_role_id'] > 2) {
            $this->response->type('application/json');
             $this->response->body(json_encode(array('status' => 'error', 'msg' => ' আপনি এই কার্যক্রম করার জন্য অনুমতিপ্রাপ্ত নয়')));
            return $this->response;
        }
        $response = array('status' => 'error', 'msg' => ' Something went wrong ');
        if ($this->request->is('post')) {
            try {
                $employeeRecordsTable = TableRegistry::get('EmployeeRecords');
                $UsersTable           = TableRegistry::get('Users');
                $employeeinfo         = $employeeRecordsTable->get($employee_record);
                $user          = $UsersTable->find()->where(['employee_record_id' => $employee_record])->first();
                if (!empty($user)) {
                    $new_pass = $this->generatePassword();
                    $user->password = $new_pass;
                    if ($UsersTable->save($user)) {
                        $response = array('status' => 'success', 'msg' => 'পাসওয়ার্ড পরিবর্তন হয়েছে। নতুন পাসওয়ার্ড হচ্ছে: <b>'.$new_pass.'</b>');
                        if (!empty($employeeinfo['personal_email'])) {
                            $this->SendEmailMessage($employeeinfo['personal_email'],
                                "জনাব ".$employeeinfo['name_bng']."<br/><br/> সুপার এডমিন কর্তৃক আপনার পাসওয়ার্ড পরিবর্তন হয়েছে। নতুন পাসওয়ার্ড হচ্ছে: <b>".$new_pass." </b><br/><br/>ধন্যবাদ।",
                                'Password reset');
                            $response = array('status' => 'success', 'msg' =>$employeeinfo['name_bng']. ' এর পাসওয়ার্ড পরিবর্তন হয়েছে। নতুন পাসওয়ার্ড হচ্ছে: <b>'.$new_pass.'</b>');
                        }
                    }
                    else {
                        $response = array('status' => 'error', 'msg' => 'পাসওয়ার্ড  পরিবর্তন সম্ভব হচ্ছে না। অনুগ্রহ করে কিছুক্ষণ পর আবার চেষ্টা করুন।');
                    }
                }
                 else {
                        $response =  array('status' => 'error', 'msg' => 'ব্যাবহারকারীর কোন তথ্য পাওয়া যায়নি।');
                    }
            } catch (\Exception $ex) {
                $response = array('status' => 'error', 'msg' => 'পাসওয়ার্ড  পরিবর্তন সম্ভব হচ্ছে না। এররঃ '.$ex->getMessage());
            }
        }
            $this->response->type('application/json');
             $this->response->body(json_encode($response));
            return $this->response;
    }
    /* API of this Controller  */

    public function APILogin()
    {
        $jsonArray = array(
            "status" => "error",
            "error_code" => 404,
            "message" => "Invalid Request"
//            "message" => " নথি অ্যাপ্লিকেশন উন্নতিকরণের কাজ চলছে। সাময়িক অসুবিধার জন্য আমরা দুঃখিত।"
        );
         // maintenance
        if(date('Y-m-d') == '2019-01-23' && date('G') <= 20 && date('G') >= 18  && Live == 1){
           $jsonArray['message'] =  " নথি অ্যাপ্লিকেশন উন্নতিকরণের কাজ চলছে। সাময়িক অসুবিধার জন্য আমরা দুঃখিত।";
            echo json_encode($jsonArray);
            die;
        }
         // maintenance
        if (!defined("PROJAPOTI_API") || PROJAPOTI_API == false) {
            goto rtn;
        }

        $apikey = isset($this->request->query['api_key']) ? $this->request->query['api_key'] : '';

        if (empty($apikey) || $apikey != API_KEY) {
            goto rtn;
        }

        if ($this->request->is('post')) {

            $this->request->data['alias'] =!empty($this->request->data['alias'])
                ? trim($this->request->data['alias']) : (!empty($this->request->data['username'])
                    ? $this->request->data['username'] : '');
            $this->request->data['username'] = !empty($this->request->data['userid'])
                ? trim($this->request->data['userid']) : (!empty($this->request->data['username'])
                    ? $this->request->data['username'] : '');
            $this->request->data['device_id'] = !empty($this->request->data['device_id'])
                ? trim($this->request->data['device_id']) : '';
            $version = !empty($this->request->data['version']) ? $this->request->data['version'] : 0;
            $device_type = !empty($this->request->data['device_type']) ? $this->request->data['device_type'] : '';


            $bn_digits = array('০', '১', '২', '৩', '৪', '৫', '৬', '৭', '৮', '৯');
            $this->request->data['username'] = str_replace($bn_digits, range(0, 9),
                $this->request->data['username']);


            // check device version
            if(!empty($device_type)){
                if($device_type == 'android'){
                    if($version < floatval(ANDROID_APP_VERSION)){
                        $jsonArray['message'] = 'দুঃখিত! আপনি অ্যাপ-এর পুরনো ভার্সন ব্যবহার করছেন। দয়া করে Google Playstore থেকে আপডেট করে নিন।';
                        $jsonArray['old_app'] = 1;
                        goto rtn;
                    }
                }
                else if($device_type == 'ios'){
                    if($version < floatval(IOS_APP_VERSION)){
                        $jsonArray['message'] = 'দুঃখিত! আপনি অ্যাপ-এর পুরনো ভার্সন ব্যবহার করছেন। দয়া করে App Store থেকে আপডেট করে নিন।';
                        $jsonArray['old_app'] = 1;
                        goto rtn;
                    }
                }
            }


            $user = $this->Auth->identify();
            // if user login through alias
            if (empty($user) && !empty($this->request->data['alias'])) {
                $user = $this->Users->find()->where(['user_alias' => $this->request->data['alias'],'active'=>1])->hydrate(false)->first();
                if (!empty($user)) {
                    $this->request->data['username'] = $user['username'];
                }
            }

            if (empty($user)) {
                $jsonArray['message'] = __('দুঃখিত! লগইন আইডি অথবা পাসওয়ার্ড সঠিক নয়');
                $jsonArray['error_code'] = 403;
                goto rtn;
            }

            if(!$user['active']) {
                $jsonArray['message'] = __('দুঃখিত! আপনার আইডিটি এই মুহূর্তে নিষ্ক্রিয় আছে, আইডিটি সক্রিয় করার জন্য অফিস এডমিন অথবা সাপোর্ট এ যোগাযোগ করুন।');
                $jsonArray['error_code'] = 403;
                goto rtn;
            }

            $user = $this->Auth->identify();

            $employee_records = [];

            if ($user){
                $this->Auth->setUser($user);
                if ($this->request->data['password'] == DEFAULT_PASSWORD && defined('Live') && Live == 1) {

                    $uservalue = $this->Users->get($user['id']);
                    $uservalue->force_password_change = 1;
                    $this->Users->save($uservalue);

                    $jsonArray['message'] = 'দুঃখিত! দয়া করে ওয়েব থেকে আপনার পাসওয়ার্ড পরিবর্তন করুন।';
                    goto rtn;
                }

                $designation_ids = [];
                if ($user['user_role_id'] > 2) {

                    $table_instance_emp_offices = TableRegistry::get('EmployeeOffices');
                    $table_instance_emp_records = TableRegistry::get('EmployeeRecords');

                    $employee_records = $table_instance_emp_records->find()->select([
                        'name_bng', 'personal_mobile', 'personal_email', 'id','default_sign'
                    ])->where(['id' => $user['employee_record_id']])->first();

                      if (($employee_office_records = Cache::read('emp_offc_rcds_' . $user['employee_record_id'], 'memcached')) === false) {
                            $employee_office_records = $table_instance_emp_offices->getEmployeeOfficeRecords($user['employee_record_id']);
                            Cache::write('emp_offc_rcds_' . $user['employee_record_id'], $employee_office_records, 'memcached');
                        }
                    if (!empty($employee_office_records)) {
                        foreach ($employee_office_records as $eor) {
                            if (!empty($eor['office_unit_organogram_id'])) {
                                $designation_ids[] = $this->makeEncryptedData($eor['office_unit_organogram_id']);
                            }
                        }
                    }
                    // Generating Token for API
                    $token = $this->generateToken(['un' => $this->makeEncryptedData($this->request->data['username']),'di' =>$this->request->data['device_id'],'dt' => $this->request->data['device_type'],'er' => $this->makeEncryptedData($employee_records['id']),'as' => $designation_ids]);
                    if (!empty($employee_office_records)) {

                            // Save Login History for API hit
                            $browser['user_id'] = $user['id'];
                            $browser['emp_rcrd_id'] = $user['employee_record_id'];
                            $browser['device_type'] = $this->request->data['device_type'];
                            $browser['device_id'] = $this->request->data['device_id'];
                            $browser['token'] = $token;
                            $browser['platform'] = !empty($this->request->data['platform'])
                                ? $this->request->data['platform'] : '';
                            $browser['name'] = !empty($this->request->data['os_name'])
                                ? $this->request->data['os_name'] : '';
                            $browser['userAgent'] = !empty($this->request->data['user_agent'])
                                ? $this->request->data['user_agent'] : '';
                            $browser['version'] = !empty($this->request->data['os_version'])
                                ? $this->request->data['os_version'] : '';
                            $browser['ip'] = !empty($this->request->data['ip']) ? $this->request->data['ip']
                                : '';
                            $browser['network_name'] = !empty($this->request->data['network_name'])
                                ? $this->request->data['network_name'] : '';
                            $browser['device_id'] = !empty($this->request->data['device_id'])
                                ? $this->request->data['device_id'] : '';
                            $this->saveMobileLoginHistory($employee_office_records,$browser);
                        //  $this->saveLoginHistory($employee_office_records);
                    }
                }
                $user['name_bng'] = $employee_records['name_bng'];
                $user['personal_mobile'] = $employee_records['personal_mobile'];
                $user['personal_email'] = $employee_records['personal_email'];
                $user['default_sign'] = $employee_records['default_sign'];

               $user = $this->unsetUserInfo($user);//unset sensitive data
                $jsonArray = array(
                    "status" => "success",
                    "data" => array(
                        "user" => $user,
                        "token"=>isset($token)?$token:'',
                    )
                );
            } else {
                $jsonArray = array(
                    "status" => "error",
                    "error_code" => 404,
                    "message" => 'দুঃখিত! লগইন আইডি অথবা পাসওয়ার্ড সঠিক নয়'
                );
            }
        }
        rtn:
        $this->response->type('application/json');
        $this->response->body(json_encode($jsonArray));
        return $this->response;
    }

    public function postAPILogin()
    {
        $jsonArray = array(
            "status" => "error",
            "error_code" => 404,
            "message" => "Invalid Request"
//            "message" => " নথি অs্যাপ্লিকেশন উন্নতিকরণের কাজ চলছে। সাময়িক অসুবিধার জন্য আমরা দুঃখিত।"
        );
           // maintenance
        if(date('Y-m-d') == '2019-01-23' && date('G') <= 20 && date('G') >= 18  && Live == 1){
            $jsonArray['message'] =  " নথি অ্যাপ্লিকেশন উন্নতিকরণের কাজ চলছে। সাময়িক অসুবিধার জন্য আমরা দুঃখিত।";
            echo json_encode($jsonArray);
            die;
        }
         // maintenance
        if (!defined("PROJAPOTI_API") || PROJAPOTI_API == false) {
            goto rtn;
        }


        if ($this->request->is('post')) {

            $this->request->data['username'] =!empty($this->request->data['userid'])
                ? trim($this->request->data['userid']) : (!empty($this->request->data['username'])
                    ? $this->request->data['username'] : '');
            $this->request->data['device_id'] = !empty($this->request->data['device_id']) ? trim($this->request->data['device_id']) : '';
            $this->request->data['device_type'] = !empty($this->request->data['device_type']) ? trim($this->request->data['device_type']) : '';

            $bn_digits = array('০', '১', '২', '৩', '৪', '৫', '৬', '৭', '৮', '৯');
            $this->request->data['username']  = str_replace($bn_digits, range(0, 9), $this->request->data['username']);

            $version = !empty($this->request->data['version']) ? $this->request->data['version'] : 0;
            $device_type = !empty($this->request->data['device_type']) ? $this->request->data['device_type'] : '';

            // check device version
            if(!empty($device_type)){
                if($device_type == 'android'){
                    if($version < floatval(ANDROID_APP_VERSION)){
                        $jsonArray['message'] = 'দুঃখিত! আপনি অ্যাপ-এর পুরনো ভার্সন ব্যবহার করছেন। দয়া করে Google Playstore থেকে আপডেট করে নিন।';
                        $jsonArray['old_app'] = 1;
                        goto rtn;
                    }
                }
                else if($device_type == 'ios'){
                    if($version < floatval(IOS_APP_VERSION)){
                        $jsonArray['message'] = 'দুঃখিত! আপনি অ্যাপ-এর পুরনো ভার্সন ব্যবহার করছেন। দয়া করে App Store থেকে আপডেট করে নিন।';
                        $jsonArray['old_app'] = 1;
                        goto rtn;
                    }
                }
            }

            $user = $this->Auth->identify();
            if (empty($user) && !empty($this->request->data['alias'])) {
                $user = $this->Users->find()->where(['user_alias' => $this->request->data['alias'], 'active'=>1])->hydrate(false)->first();
                if (!empty($user)) {
                    $this->request->data['username'] = $user['username'];
                }
            }
            
            if (empty($user)) {
                $jsonArray['message'] = __('দুঃখিত! লগইন আইডি অথবা পাসওয়ার্ড সঠিক নয়');
                $jsonArray['error_code'] = 403;
                goto rtn;
            }

            if(!$user['active']) {
                $jsonArray['message'] = __('দুঃখিত! আপনার আইডিটি এই মুহূর্তে নিষ্ক্রিয় আছে, আইডিটি সক্রিয় করার জন্য অফিস এডমিন অথবা সাপোর্ট এ যোগাযোগ করুন।');
                $jsonArray['error_code'] = 403;
                goto rtn;
            }



            $employee_records = [];
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);

                if ($this->request->data['password'] == DEFAULT_PASSWORD  && defined('Live') && Live == 1) {

                    $uservalue = $this->Users->get($user['id']);
                    $uservalue->force_password_change = 1;
                    $this->Users->save($uservalue);

                    $jsonArray['message'] = 'দুঃখিত! দয়া করে ওয়েব থেকে আপনার পাসওয়ার্ড পরিবর্তন করুন।';
                    goto rtn;
                }
                $designation_ids = [];
                if ($user['user_role_id'] > 2) {

                    $table_instance_emp_offices = TableRegistry::get('EmployeeOffices');
                    $table_instance_emp_records = TableRegistry::get('EmployeeRecords');

                    $employee_records = $table_instance_emp_records->find()->select([
                        'name_bng', 'personal_mobile', 'personal_email', 'id','default_sign'
                    ])->where(['id' => $user['employee_record_id']])->first();

                    $employee_office_records = $table_instance_emp_offices->getEmployeeOfficeRecords($user['employee_record_id']);
                   if(!empty($employee_office_records)){
                        foreach($employee_office_records as $eor){
                            if(!empty($eor['office_unit_organogram_id'])){
                                $designation_ids[] = $this->makeEncryptedData($eor['office_unit_organogram_id']);
                            }
                        }
                }
                    // Generating Token for API
                    $token = $this->generateToken(['un' => $this->makeEncryptedData($this->request->data['username']),'di' =>$this->request->data['device_id'],'dt' => $this->request->data['device_type'],'er' => $this->makeEncryptedData($employee_records['id']),'as' => $designation_ids]);
                    if (!empty($employee_office_records)) {
                        // Save Login History for API hit
                        $browser['user_id'] = $user['id'];
                        $browser['emp_rcrd_id'] = $user['employee_record_id'];
                        $browser['device_type'] = $this->request->data['device_type'];
                        $browser['device_id'] = $this->request->data['device_id'];
                        $browser['token'] = $token;
                        $browser['platform'] = !empty($this->request->data['platform'])
                            ? $this->request->data['platform'] : '';
                        $browser['name'] = !empty($this->request->data['os_name'])
                            ? $this->request->data['os_name'] : '';
                        $browser['userAgent'] = !empty($this->request->data['user_agent'])
                            ? $this->request->data['user_agent'] : '';
                        $browser['version'] = !empty($this->request->data['os_version'])
                            ? $this->request->data['os_version'] : '';
                        $browser['ip'] = !empty($this->request->data['ip']) ? $this->request->data['ip']
                            : '';
                        $browser['network_name'] = !empty($this->request->data['network_name'])
                            ? $this->request->data['network_name'] : '';
                        $browser['device_id'] = !empty($this->request->data['device_id'])
                            ? $this->request->data['device_id'] : '';
                        $this->saveMobileLoginHistory($employee_office_records,$browser);
                    }
                }
                $user['name_bng'] = $employee_records['name_bng'];
                $user['personal_mobile'] = $employee_records['personal_mobile'];
                $user['personal_email'] = $employee_records['personal_email'];
                $user['default_sign'] = $employee_records['default_sign'];
                $user = $this->unsetUserInfo($user);//unset sensitive data
                $jsonArray = array(
                    "status" => "success",
                    "data" => array(
                        "user" => $user,
                        'token' => isset($token)?$token:"",
                    )
                );
            } else
                $jsonArray = array(
                    "status" => "error",
                    "error_code" => 404,
                    "message" => 'দুঃখিত! লগইন আইডি অথবা পাসওয়ার্ড সঠিক নয়'
                );
        }

        rtn:
        $this->response->type('application/json');
        $this->response->body(json_encode($jsonArray));
        return $this->response;
    }

    public function APISignature()
    {
        $jsonArray = array(
            "status" => "error",
            "error_code" => 404,
            "message" => "Invalid Request"
        );
        if (!defined("PROJAPOTI_API") || PROJAPOTI_API == false) {
            echo json_encode($jsonArray);
            die;
        }

        $apikey = isset($this->request->query['api_key']) ? $this->request->query['api_key']
            : '';

        if (empty($apikey) || $this->checkToken($apikey) == FALSE) {
            echo json_encode($jsonArray);
            die;
        }

        $userId = !empty($this->request->query['user_id']) ? intval($this->request->query['user_id'])
            : 0;

        $userTable = TableRegistry::get('Users');

        if (!empty($userId)) {
            $userInfo = $userTable->get($userId);

            if (!empty($userInfo)) {
                $options['token']= sGenerateToken(['file'=>$userInfo->username],['exp'=> time() + 60*300]);
                $jsonArray = array(
                    "status" => "success",
                    "data" => $this->getSignature($userInfo->username,
                        1, 1,false,null,$options),
                );
            }
        }

        echo json_encode($jsonArray);
        die;
    }

    public function APIgetDesignantions()
    {
        $jsonArray = array(
            "status" => "error",
            "error_code" => 404,
            "message" => "Invalid Request"
        );
        if (!defined("PROJAPOTI_API") || PROJAPOTI_API == false) {
            echo json_encode($jsonArray);
            die;
        }

        $apikey = isset($this->request->query['api_key']) ? $this->request->query['api_key']
            : '';


        if (empty($apikey) || $this->checkToken($apikey) == FALSE) {
            echo json_encode($jsonArray);
            die;
        }
        if($apikey != API_KEY){
            //verify api token
            $this->loadComponent('ApiRelated');
            $getApiTokenData = $this->ApiRelated->getMobileApiTokenData($apikey);
            //verify api token
        }

        $user_id = isset($this->request->query['user_id']) ? intval($this->request->query['user_id'])
            : 0;

        if ($this->request->is('get')) {

            if (!empty($user_id)) {
                $userstable = TableRegistry::get('Users');

                $user = $userstable->find()->where(['id' => $user_id])->first();

                if(!empty($getApiTokenData)){
                    if($user['username'] != $getApiTokenData['user_name'] || $user['employee_record_id'] != $getApiTokenData['employee_record_id']){
                        $jsonArray['message'] = 'Unauthorized request';
                        echo json_encode($jsonArray);die;
                    }
                }

                $employeeOffice = TableRegistry::get('EmployeeOffices');

                $organogramId = $employeeOffice->getEmployeeOfficeRecords($user['employee_record_id'],1);
                if(!empty($organogramId)){
                    foreach($organogramId as &$org){
                        $org = $this->unsetEmployeeInfo($org);//hide sensitive data
                    }
                    
                }

                if (!empty($organogramId)) {
                    $jsonArray = array(
                        "status" => "success",
                        "data" => $organogramId,
                    );
                } else {
                    $jsonArray = array(
                        "status" => "error",
                        "data" => 'দুঃখিত কোন তথ্য নেই',
                    );
                }
            }
        }
        echo json_encode($jsonArray);
        die;
    }

    public function postAPIgetDesignantions()
    {

        $jsonArray = array(
            "status" => "error",
            "error_code" => 404,
            "message" => "Invalid Request"
        );
        if (!defined("PROJAPOTI_API") || PROJAPOTI_API == false) {
            echo json_encode($jsonArray);
            die;
        }

        $apikey = isset($this->request->data['api_key']) ? $this->request->data['api_key']
            : '';

        //API checking
        if (empty($apikey) || $this->checkToken($apikey) == FALSE ) {
            echo json_encode($jsonArray);
            die;
        }
        if($apikey != API_KEY){
            //verify api token
            $this->loadComponent('ApiRelated');
            $getApiTokenData = $this->ApiRelated->getMobileApiTokenData($apikey);
            //verify api token
        }
        $user_id = isset($this->request->data['user_id']) ? intval($this->request->data['user_id'])
            : 0;

        if ($this->request->is('post')) {

            if (!empty($user_id)) {
                $userstable = TableRegistry::get('Users');

                $user = $userstable->find()->where(['id' => $user_id])->first();
                if(!empty($getApiTokenData)){
                    if($user['username'] != $getApiTokenData['user_name'] || $user['employee_record_id'] != $getApiTokenData['employee_record_id']){
                        $jsonArray['message'] = 'Unauthorized request';
                        echo json_encode($jsonArray);die;
                    }
                }

                $employeeOffice = TableRegistry::get('EmployeeOffices');

                $organogramId = $employeeOffice->getEmployeeOfficeRecords($user['employee_record_id'],1);
                if(!empty($organogramId)){
                    foreach($organogramId as &$org){
                        $org = $this->unsetEmployeeInfo($org);//hide sensitive data
                    }

                }

                if (!empty($organogramId)) {
                    $jsonArray = array(
                        "status" => "success",
                        "data" => $organogramId,
                    );
                } else {
                    $jsonArray = array(
                        "status" => "error",
                        "data" => 'দুঃখিত কোন তথ্য নেই',
                    );
                }
            }
        }
        echo json_encode($jsonArray);
        die;
    }

    public function apiLoginHistorySave()
    {
        $jsonArray = array(
            "status" => "error",
            "error_code" => 404,
            "message" => "Invalid Request"
        );
        if (!defined("PROJAPOTI_API") || PROJAPOTI_API == false) {
            echo json_encode($jsonArray);
            die;
        }

        $apikey = isset($this->request->query['api_key']) ? $this->request->query['api_key']
            : '';


        if (empty($apikey) || $this->checkToken($apikey) == FALSE) {
            echo json_encode($jsonArray);
            die;
        }

        $result = [];
        if ($this->request->is('get') || $this->request->is('post')) {
            $user_id = isset($this->request->data['user_id']) ? intval($this->request->data['user_id']) : 0;
            if (!empty($user_id)) {
                $userstable = TableRegistry::get('Users');

                $user = $userstable->find()->where(['id' => $user_id])->first();
                if($apikey != API_KEY){
                    //verify api token
                    $this->loadComponent('ApiRelated');
                    $getApiTokenData = $this->ApiRelated->getMobileApiTokenData($apikey);
                    //verify api token
                    if(!empty($getApiTokenData)){
                        if($user['username'] != $getApiTokenData['user_name'] || $user['employee_record_id'] != $getApiTokenData['employee_record_id']){
                            $jsonArray['message'] = 'Unauthorized request';
                            echo json_encode($jsonArray);die;
                        }
                    }
                }
                $employeeOffice = TableRegistry::get('EmployeeOffices');

                $employee_office = $employeeOffice->getEmployeeOfficeRecords($user['employee_record_id']);

                $browser = [];

                $browser['platform'] = !empty($this->request->data['platform'])
                    ? $this->request->data['platform'] : '';
                $browser['name'] = !empty($this->request->data['os_name'])
                    ? $this->request->data['os_name'] : '';
                $browser['userAgent'] = !empty($this->request->data['user_agent'])
                    ? $this->request->data['user_agent'] : '';
                $browser['version'] = !empty($this->request->data['os_version'])
                    ? $this->request->data['os_version'] : '';
                $browser['ip'] = !empty($this->request->data['ip']) ? $this->request->data['ip']
                    : '';
                $browser['network_name'] = !empty($this->request->data['network_name'])
                    ? $this->request->data['network_name'] : '';
                $browser['device_id'] = !empty($this->request->data['device_id'])
                    ? $this->request->data['device_id'] : '';
                $browser['user_id'] = $user_id;
                $browser['emp_rcrd_id'] = $user['employee_record_id'];
                if (!empty($browser)) {
                    $result = $this->saveMobileLoginHistory($employee_office, $browser);

                    if (!empty($result)) {
                        $jsonArray = array(
                            "status" => "success",
                            "data" => [],
                        );
                    } else {
                        $jsonArray = array(
                            "status" => "error",
                            "data" => $result === false ? 'দুঃখিত! অনুরোধ গ্রহণ করা সম্ভব হচ্ছে না। '
                                : $result,
                        );
                    }
                } else {
                    $jsonArray = array(
                        "status" => "error",
                        "data" => $result === false ? 'দুঃখিত! অনুরোধ গ্রহণ করা সম্ভব হচ্ছে না। '
                            : $result,
                    );
                }
            }
        }
        echo json_encode($jsonArray);
        die;
    }
    public function postApiLoginHistorySave()
    {
        $jsonArray = array(
            "status" => "error",
            "error_code" => 404,
            "message" => "Invalid Request"
        );
        if (!defined("PROJAPOTI_API") || PROJAPOTI_API == false) {
            echo json_encode($jsonArray);
            die;
        }

        $apikey = isset($this->request->data['api_key']) ? $this->request->data['api_key']
            : '';
        if (empty($apikey) || $this->checkToken($apikey) == FALSE) {
            echo json_encode($jsonArray);
            die;
        }

        $result = [];
        if ($this->request->is('post')) {
            $user_id = isset($this->request->data['user_id']) ? intval($this->request->data['user_id']) : 0;
            if (!empty($user_id)) {
                $userstable = TableRegistry::get('Users');

                $user = $userstable->find()->where(['id' => $user_id])->first();
                if($apikey != API_KEY){
                    //verify api token
                    $this->loadComponent('ApiRelated');
                    $getApiTokenData = $this->ApiRelated->getMobileApiTokenData($apikey);
                    //verify api token
                    if(!empty($getApiTokenData)){
                        if($user['username'] != $getApiTokenData['user_name'] || $user['employee_record_id'] != $getApiTokenData['employee_record_id']){
                            $jsonArray['message'] = 'Unauthorized request';
                            echo json_encode($jsonArray);die;
                        }
                    }
                }
                $employeeOffice = TableRegistry::get('EmployeeOffices');

                $employee_office = $employeeOffice->getEmployeeOfficeRecords($user['employee_record_id']);

                $browser = [];

                $browser['platform'] = !empty($this->request->data['platform'])
                    ? $this->request->data['platform'] : '';
                $browser['name'] = !empty($this->request->data['os_name'])
                    ? $this->request->data['os_name'] : '';
                $browser['userAgent'] = !empty($this->request->data['user_agent'])
                    ? $this->request->data['user_agent'] : '';
                $browser['version'] = !empty($this->request->data['os_version'])
                    ? $this->request->data['os_version'] : '';
                $browser['ip'] = !empty($this->request->data['ip']) ? $this->request->data['ip']
                    : '';
                $browser['network_name'] = !empty($this->request->data['network_name'])
                    ? $this->request->data['network_name'] : '';
                $browser['device_id'] = !empty($this->request->data['device_id'])
                    ? $this->request->data['device_id'] : '';
                $browser['user_id'] = $user_id;
                $browser['emp_rcrd_id'] = $user['employee_record_id'];
                if (!empty($browser)) {
                    $result = $this->saveMobileLoginHistory($employee_office, $browser);

                    if (!empty($result)) {
                        $jsonArray = array(
                            "status" => "success",
                            "data" => [],
                        );
                    } else {
                        $jsonArray = array(
                            "status" => "error",
                            "data" => $result === false ? 'দুঃখিত! অনুরোধ গ্রহণ করা সম্ভব হচ্ছে না। '
                                : $result,
                        );
                    }
                } else {
                    $jsonArray = array(
                        "status" => "error",
                        "data" => $result === false ? 'দুঃখিত! অনুরোধ গ্রহণ করা সম্ভব হচ্ছে না। '
                            : $result,
                    );
                }
            }
        }
        echo json_encode($jsonArray);
        die;
    }

    public function accountVerify(){
        $jsonArray = array(
            "status" => "error",
            "error_code" => 404,
            "message" => "Invalid Request"
        );
        if (!defined("PROJAPOTI_API") || PROJAPOTI_API == false) {
            echo json_encode($jsonArray);
            die;
        }

        $apikey = isset($this->request->data['api_key']) ? $this->request->data['api_key']: '';
        $device_id = isset($this->request->data['device_id']) ? $this->request->data['device_id']: '';

         if ( empty($device_id) ) {
            echo json_encode($jsonArray);
            die;
        }


//        if ( empty($apikey) || $this->checkToken($apikey) == FALSE
//        ) {
//            echo json_encode($jsonArray);
//            die;
//        }

        if(!empty($this->request->data)) {
            $user = $this->Auth->identify();
            if (!empty($user)) {
                $table = TableRegistry::get('UserDeviceRegistration');
                $response = $table->registerDevice(['user' => $user, 'device_id' => $device_id,'os' => $this->request->data['device_type']]);
                if ($response === true) {
                    $jsonArray = array(
                        "status" => "success"
                    );
                } else {
                    $jsonArray = array(
                        "status" => "invalid",
                        "message" => $response
                    );
                }
            }else{
                $jsonArray = array(
                    "status" => "error",
                    "message" => 'Invalid request'
                );
            }
        }

        $this->response->type('json');
        $this->response->body(json_encode($jsonArray));
        return $this->response;
    }

    public function verifyDevice(){
        $jsonArray = array(
            "status" => "error",
            "error_code" => 404,
            "message" => "Invalid Request"
        );
        if (!defined("PROJAPOTI_API") || PROJAPOTI_API == false) {
            echo json_encode($jsonArray);
            die;
        }

        $apikey = isset($this->request->data['api_key']) ? $this->request->data['api_key']: '';


        if (empty($this->request->data['device_id'])) {
            echo json_encode($jsonArray);
            die;
        }

        if(!empty($this->request->data)) {
            $user = $this->Auth->identify();
            if (!empty($user)) {
                $table = TableRegistry::get('UserDeviceRegistration');
                $response = $table->activationDevice($user['id'], $this->request->data['device_id'], $this->request->data['activation_code']);
                if ($response === true) {
                    $jsonArray = array(
                        "status" => "success"
                    );
                } else {
                    $jsonArray = array(
                        "status" => "invalid",
                        "message" => $response
                    );
                }
            }else{
                $jsonArray = array(
                    "status" => "error",
                    "message" => 'Invalid username or password'
                );
            }
        }

        $this->response->type('json');
        $this->response->body(json_encode($jsonArray));
        return $this->response;
    }
    public function postAPISetFirebaseToken(){
          $jsonArray = array(
            "status" => "error",
            "error_code" => 404,
            "message" => "Invalid Request"
        );
        if (!defined("PROJAPOTI_API") || PROJAPOTI_API == false) {
             goto rtn;
        }
        $apikey = isset($this->request->data['api_key']) ? $this->request->data['api_key']: '';
          if ( empty($apikey) || $this->checkToken($apikey) == FALSE ) {
             goto rtn;
        }
           $username = isset($this->request->data['username']) ? $this->request->data['username']: '';
           $fcm_token = isset($this->request->data['fcm_token']) ? $this->request->data['fcm_token']: '';
           if(empty($username) || empty($fcm_token) || strlen($username) < 8){
               goto rtn;
           }
        if($apikey != API_KEY){
            //verify api token
            $this->loadComponent('ApiRelated');
            $getApiTokenData = $this->ApiRelated->getMobileApiTokenData($apikey);
            //verify api token
            if(!empty($getApiTokenData)){
                if($username != $getApiTokenData['user_name']){
                    $jsonArray['message'] = 'Unauthorized request';
                    echo json_encode($jsonArray);die;
                }
            }
        }
           $apiTokensTable =  TableRegistry::get('ApiTokens');
           // Check if exist by username
           $is_exist = $apiTokensTable->checkByUserName($username);
           if(empty($is_exist)){
               // create a new entry
            $getEmployeeRecord= TableRegistry::get('Users')->getEmployeeRecordIdByUsername($username)->first();
           if(empty($getEmployeeRecord['employee_record_id'])){
               $jsonArray['message'] = 'Employee Record does not exist';
               goto rtn;
           }
               $apiTokenEntity = $apiTokensTable->newEntity();
               $apiTokenEntity->fcm_token = $fcm_token;
               $apiTokenEntity->username = $username;
               $apiTokenEntity->employee_record_id = $getEmployeeRecord['employee_record_id'];
               if($apiTokensTable->save($apiTokenEntity)){
                     $jsonArray = array(
                        "status" => "success",
                        "message" => "FCM code added"
                         );
                      goto rtn;
               }
           }
           else{
               // update previous one
               if(empty($is_exist->employee_record_id)){
                    $getEmployeeRecord= TableRegistry::get('Users')->getEmployeeRecordIdByUsername($username)->first();
                    if(empty($getEmployeeRecord['employee_record_id'])){
                        $jsonArray['message'] = 'Employee Record does not exist';
                        goto rtn;
                    }
                    $is_exist->employee_record_id = $getEmployeeRecord['employee_record_id'];
               }
               $is_exist->fcm_token = $fcm_token;
               if($apiTokensTable->save($is_exist)){
                     $jsonArray = array(
                        "status" => "success",
                        "message" => "FCM code updated"
                         );
                      goto rtn;
               }
           }
           rtn:
                 echo json_encode($jsonArray);
                die;
        
    }
    public function  postApiNotifyUser(){
        $jsonArray = array(
            "status" => "error",
            "error_code" => 404,
            "message" => "Invalid Request"
        );
        if (!defined("PROJAPOTI_API") || PROJAPOTI_API == false) {
             goto rtn;
        }
        $apikey = isset($this->request->data['api_key']) ? $this->request->data['api_key']: '';
          if ( empty($apikey) || $this->checkToken($apikey) == FALSE ) {
             goto rtn;
        }
           $username = isset($this->request->data['username']) ? $this->request->data['username']: '';
           $employee_id = isset($this->request->data['employee_id']) ? $this->request->data['employee_id']: '';
           $message = isset($this->request->data['message']) ? $this->request->data['message']: '';
           $title = isset($this->request->data['title']) ? $this->request->data['title']: '';
           if(empty($message) || empty($title) || (!empty($username) && strlen($username) < 8)){
               goto rtn;
           }
           if(empty($username) && empty($employee_id)){
               goto rtn;
           }
        if($apikey != API_KEY){
            //verify api token
            $this->loadComponent('ApiRelated');
            $getApiTokenData = $this->ApiRelated->getMobileApiTokenData($apikey);
            //verify api token
            if(!empty($getApiTokenData)){
                if($username != $getApiTokenData['user_name']){
                    $jsonArray['message'] = 'Unauthorized request';
                    echo json_encode($jsonArray);die;
                }
            }
        }
           $send_notification = $this->notifyAppUser($username,$employee_id,['title' => $title,'body' => $message],['title' => $title,'body' => $message]);
           if($send_notification == true){
                  $jsonArray = array(
                    "status" => "success",
                    "message" => "Notification Sent Successfully"
                );
           }else{
                 $jsonArray = array(
                    "status" => "error",
                    "message" => "Notification Sent failed. Error: ".$send_notification
                );
           }
           rtn:
                 echo json_encode($jsonArray);
                die;
    }
    /* API of this Controller */

    public function loginNew()
    {
        $this->layout='login_new';
        if ($this->request->is('get')){
            $user = TableRegistry::get('Users')->newEntity();
            $this->set(compact(['user']));
        }
        $office_messages = Cache::read('login_page_settings', 'memcached');

        $this->set('office_messages',$office_messages);
        if ($this->request->is('post')) {

            $this->request->data['username'] = trim($this->request->data['username']);

            $bn_digits = array('০', '১', '২', '৩', '৪', '৫', '৬', '৭', '৮', '৯');
            $output = str_replace($bn_digits, range(0, 9),
                $this->request->data['username']);

            $this->request->data['username'] = $output;

            try {
                $user = [];
                if (!empty($this->request->data['alias'])) {
                    $user = $this->Users->find()->where(['user_alias' => $this->request->data['alias']])->hydrate(false)->first();
                }
                if (!empty($user)) {
                    $this->request->data['username'] = $user['username'];
                }
                $user = $this->Auth->identify();

                if ($user) {
                    $this->Auth->setUser($user);

                    if ($this->request->data['password'] == DEFAULT_PASSWORD) {

                        $uservalue = $this->Users->get($user['id']);
                        $uservalue->force_password_change = 1;
                        $uservalue = $this->Users->save($uservalue);

                        unset($uservalue->password);
                        $this->Auth->setUser($uservalue->toArray());
                    }

                    $moduleInfos = TableRegistry::get('ModuleInfos');
                    $modules = $moduleInfos->getModuleList();

                    $session = $this->request->session();
                    $session->write('modules', $modules);

                    $session = $this->request->session();
                    if ($user['user_role_id'] > 2) {
                        $session->write('module_id', 5);
                    } else {
                        $session->write('module_id', 1);
                    }

                    if ($user['user_role_id'] > 2) {
                        $table_instance_emp_offices = TableRegistry::get('EmployeeOffices');

                        if (($employee_office_records = Cache::read('emp_offc_rcds_' . $user['employee_record_id'], 'memcached')) === false) {
                            $employee_office_records = $table_instance_emp_offices->getEmployeeOfficeRecords($user['employee_record_id']);
                            Cache::write('emp_offc_rcds_' . $user['employee_record_id'], $employee_office_records, 'memcached');
                        }

                        if (!empty($this->request->data['fingerprint'])) {
                            foreach ($employee_office_records as $key => $value) {
                                $value['fingerprint'] = $this->request->data['fingerprint'];
                            }
                        }

                        $this->saveLoginHistory($employee_office_records);
                    }

                    if (defined('REDIRECT') && REDIRECT == false) {

                    } else {
                        if ($user['user_role_id'] > 2) {

                            $table_instance_emp_offices = TableRegistry::get('EmployeeOffices');
                             if (($employee_office_records = Cache::read('emp_offc_rcds_' . $user['employee_record_id'], 'memcached')) === false) {
                            $employee_office_records = $table_instance_emp_offices->getEmployeeOfficeRecords($user['employee_record_id']);
                            Cache::write('emp_offc_rcds_' . $user['employee_record_id'], $employee_office_records, 'memcached');
                        }

                            if (!empty($employee_office_records)) {

                                $officeDomainTable = TableRegistry::get('OfficeDomains');
                                $officeDomain = $officeDomainTable->find()->where(['office_id' => $employee_office_records[0]['office_id']])->first();

                                if (!empty($officeDomain) && $subdomain = $this->checkSubdomain()) {

                                    if ($subdomain == -1 || $subdomain != $officeDomain['domain_prefix']) {

                                        $this->redirect($officeDomain['domain_url']);
                                    } else {

                                    }
                                }
                            }
                        }
                    }

                    return $this->redirect($this->Auth->redirectUrl());
                }
                $this->Flash->error(__('দুঃখিত! লগইন আইডি অথবা পাসওয়ার্ড সঠিক নয়'));
            } catch (\Exception $ex) {
                $this->Flash->error($ex->getMessage());
            }
        }
        if ($this->Auth->user()) {
            return $this->redirect($this->Auth->redirectUrl());
        }
    }

    public function healthStatus(){
        $this->layout = 'dashboard';
        $tableHealthCheckup =TableRegistry::get('OfficeHealthStatus');
        $current_office = $this->getCurrentDakSection()['office_id'];
        $healthStatus = $tableHealthCheckup->getHealthStatus($current_office);

        if($healthStatus['error']==0){
            $this->redirect(['action'=>'dashboard','controller'=>'dashboard']);
        }
        $this->set('healthStatus',$healthStatus);
    }
     public function officeWiseLoginAudit()
    {

        $user = $this->Auth->user();
        if ($user['user_role_id'] > 2) {
                return $this->redirect(['controller' => 'Users', 'action' =>'login']);
        }
    }

    public function officeWiseLoginAuditAjax()
    {
        $loginHistoryTable = TableRegistry::get('UserLoginHistory');

        $user = $this->Auth->user();

        $page = 1;
        $limit = 50;
        $office_id = 0;
        $office_unit_id = 0;
        $office_unit_organogram_id = 0;
        $start_time = $end_time = '';

            $is_superAmdin = true;
        $this->set(compact('is_superAmdin'));
        $this->layout = 'blank';
        if ($this->request->is('ajax')) {

            if (!empty($this->request->query)) {

                if (!empty($this->request->query['page'])) {
                    $page = intval($this->request->query['page']);
                }

                if (!empty($this->request->query['office_id'])) {
                    $office_id = intval($this->request->query['office_id']);
                }


                if (!empty($this->request->query['office_unit_id'])) {
                    $office_unit_id = intval($this->request->query['office_unit_id']);
                }

                if (!empty($this->request->query['office_unit_organogram_id'])) {
                    $office_unit_organogram_id = intval($this->request->query['office_unit_organogram_id']);
                }

                if (!empty($this->request->query['start_time'])) {
                    $start_time = ($this->request->query['start_time']);
                }

                if (!empty($this->request->query['end_time'])) {
                    $end_time = ($this->request->query['end_time']);
                }
            }
        }
        $query = $loginHistoryTable->loginByDateRange($office_id,
            $office_unit_id, $office_unit_organogram_id,
            [$start_time, $end_time])->select(['office_unit_organogram','client_ip'])->group(['office_unit_organogram','client_ip']);
        $loginHistoryObject = $this->Paginator->paginate($query, ['limit' => $limit]);

        $designationCollection = [];
        $IPCollection = [];
        $loginHistory = [];
       $loginHistoryArray = $loginHistoryObject->toArray();
        if(!empty($loginHistoryArray)){
            foreach( $loginHistoryArray as  $value){
                $designationCollection[] = $value['office_unit_organogram'];
                $IPCollection[] = $value['client_ip'];
            }
            $loginHistory = $loginHistoryTable->getLoginDataByDesignationAndIP($designationCollection,$IPCollection,[$start_time,$end_time])->select(['employee_name','office_name','unit_name','organogram_name','network_information','client_ip','device_name','browser_name','login_time','logout_time','countIP' => 'count(client_ip)','office_unit_organogram'])->group(['office_unit_organogram,client_ip']);
        }

        $this->set(compact('loginHistory', 'page', 'limit','start_time','end_time'));
    }
        public function officeWiseLoginAuditDetails()
    {
            $this->layout = 'ajax';
        $loginHistoryTable = TableRegistry::get('UserLoginHistory');
        $response = ['status' => 'error', 'msg' => 'Something wrong'];

        if (!empty($this->request->query['designation'])) {
            $designation = intval($this->request->query['designation']);
        }else{
            $rsponse['msg'] = 'Designation missing';
            goto rtn;
        }
        if (!empty($this->request->query['ip'])) {
            $ip = ($this->request->query['ip']);
        }else{
            $rsponse['msg'] = 'IP missing';
            goto rtn;
        }
        if (!empty($this->request->query['start'])) {
            $start = ($this->request->query['start']);
        }else{
            $rsponse['msg'] = 'Start Date missing';
            goto rtn;
        }
        if (!empty($this->request->query['end'])) {
            $end =$this->request->query['end'];
        }else{
            $rsponse['msg'] = 'End Date missing';
            goto rtn;
        }
        if (!empty($this->request->query['info'])) {
            $info =$this->request->query['info'];
        }else{
            $info = '';
        }

          $loginHistory = $loginHistoryTable->getLoginDataByDesignationAndIP($designation,$ip,[$start,$end])->select(['employee_name','office_name','unit_name','organogram_name','network_information','client_ip','device_name','browser_name','login_time','logout_time'])->toArray();
          $response = ['status' => 'success', 'data' => $loginHistory];
        rtn:
        $this->set(compact('response','info','start','end','ip'));

    }
    private function unsetUserInfo($user = []){
        if(empty($user)){
            return ;
        }
        //unset user info which is not needed
        if(isset($user['password'])){
            unset($user['password']);
        } if(isset($user['hash_change_password'])){
            unset($user['hash_change_password']);
        } if(isset($user['email_verify_code'])){
            unset($user['email_verify_code']);
        } if(isset($user['verification_date'])){
            unset($user['verification_date']);
        }if(isset($user['ssn'])){
            unset($user['ssn']);
        }if(isset($user['force_password_change'])){
            unset($user['force_password_change']);
        }if(isset($user['last_password_change'])){
            unset($user['last_password_change']);
        }if(isset($user['modified'])){
            unset($user['modified']);
        }if(isset($user['created_by'])){
            unset($user['created_by']);
        }if(isset($user['modified_by'])){
            unset($user['modified_by']);
        }if(isset($user['modified_by'])){
            unset($user['modified_by']);
        }if(isset($user['is_email_verified'])){
            unset($user['is_email_verified']);
        }if(isset($user['verification_date'])){
            unset($user['verification_date']);
        }
        //unset user info which is not needed
        return $user;
    }
    private function unsetEmployeeInfo($user = []){
        if(empty($user)){
            return ;
        }
        //unset employee info which is not needed
        if(isset($user['id'])){
            unset($user['id']);
        } if(isset($user['identification_number'])){
            unset($user['identification_number']);
        } if(isset($user['is_default_role'])){
            unset($user['is_default_role']);
        } if(isset($user['summary_nothi_post_type'])){
            unset($user['summary_nothi_post_type']);
        }if(isset($user['main_role_id'])){
            unset($user['main_role_id']);
        }if(isset($user['joining_date'])){
            unset($user['joining_date']);
        }if(isset($user['last_office_date'])){
            unset($user['last_office_date']);
        }if(isset($user['status_change_date'])){
            unset($user['status_change_date']);
        }if(isset($user['modified'])){
            unset($user['modified']);
        }if(isset($user['created_by'])){
            unset($user['created_by']);
        }if(isset($user['modified_by'])){
            unset($user['modified_by']);
        }if(isset($user['modified_by'])){
            unset($user['modified_by']);
        }
        //unset employee info which is not needed
        return $user;
    }

    public function protikolpoRevert() {
        $is_logout_button = true; // force show setting window
        $user = $this->Auth->user();
        $employee_record_id = $user['employee_record_id'];
        $employee_offices_table = TableRegistry::get('employee_offices');
        $employee_offices = $employee_offices_table->find()->where(['employee_record_id' => $employee_record_id, 'status' => 1])->toArray();
        if (count($employee_offices) == 0) {
            $is_logout_button = true;
        }

        $protikolpo_settings_table = TableRegistry::get('protikolpo_settings');
        $protikolpo_settings = $protikolpo_settings_table->find()->where(['employee_record_id' => $employee_record_id, 'active_status' => 1])->toArray();
        $protikolpo_list = [];
        $employee_record_user = TableRegistry::get('EmployeeRecords')->get($employee_record_id);
        $protikolpo_list['user_name'] = $employee_record_user->name_bng;
        $protikolpo_list['employee_record_id'] = $employee_record_user->id;
        foreach ($protikolpo_settings as $key => $protikolpo_setting) {
            $protikolpos = json_decode($protikolpo_setting['protikolpos'], true);
            if ($protikolpo_setting['selected_protikolpo'] > 0) {
                $selected_protikolpo = $protikolpos['protikolpo_'.$protikolpo_setting['selected_protikolpo']];
            }
            //pr($selected_protikolpo);die;
            $protikolpo_list['data'][$key]['start_date'] = $protikolpo_setting['start_date'];
            $protikolpo_list['data'][$key]['end_date'] = $protikolpo_setting['end_date'];

                $office = TableRegistry::get('Offices')->get($selected_protikolpo['office_id']);
                $office_unit = TableRegistry::get('OfficeUnits')->get($selected_protikolpo['office_unit_id']);
                $office_unit_ogranogram = TableRegistry::get('OfficeUnitOrganograms')->get($selected_protikolpo['designation_id']);
                $employee_record = TableRegistry::get('EmployeeRecords')->get($selected_protikolpo['employee_record_id']);
                $protikolpo_list['data'][$key]['protikolpo'] = [
                    'office_id' => $selected_protikolpo['office_id'],
                    'employee_record_id' => $selected_protikolpo['employee_record_id'],
                    'office_unit_id' => $selected_protikolpo['office_unit_id'],
                    'office_unit_organogram_id' => $selected_protikolpo['designation_id'],

                    'employee_name' => $employee_record->name_bng,
                    'office_unit_organogram_name' => $office_unit_ogranogram->designation_bng,
                    'office_unit_name' => $office_unit->unit_name_bng,
                    'office_name' => $office->office_name_bng,
                ];

        }
        //pr($protikolpo_list['data']);die;
        $this->set(compact('is_logout_button', 'protikolpo_list'));
    }
}