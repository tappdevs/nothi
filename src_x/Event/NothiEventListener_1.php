<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PostListener
 *
 * @author a2i
 */

namespace App\Event;

use App\Model\Table;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\Log\Log;
use Cake\Event\EventListenerInterface;

//use App\Controller\Component\EmailNotificationComponent;

class NothiEventListener implements EventListenerInterface {

    public function implementedEvents() {
        return array(
            'Dak.Nagorik.Upload' => 'notifyAbroad',
            'Dak.Nagorik.Send' => 'notifyAbroad',
            'Dak.Daptorik.Upload' => 'notifyAbroad',
            'Dak.Daptorik.Send' => 'notifyAbroad'
        );
    }

    public function notifyAbroad($event = array()) {
	
	/*
		echo "<pre>";
		print_r($event);
		echo "</pre>";
		die('ddd');
	*/
        $media = array();
        $to_officer_ids = array();
        $to_office_ids = array();
        $event_id = 0;
        $event_type = "";
        $template_id = 0;
        $from_officer_id = 0;
        $from_office_id = 0;

        $from_officer_id = $event->data['from_officer_id'];
        $to_officer_ids = $event->data['to_officer_ids'];
        $from_office_id = $event->data['from_office_id'];
        $to_office_ids = $event->data['to_office_ids'];
        $event_id = $event->data['event_id'];
        $event_type = $event->data['event_type'];
        $template_id = $event->data['template_id'];
		
		/*
					echo "<pre>";
			print_r($event);			
			die('xx');
		*/
		

        $media = explode(",", $event->data['media']);
        for ($i = 0; $i < count($media); $i++) {
            if ($media[$i] == "email") {
                $this->sendEmail($event_id, $event_type, $template_id, $from_officer_id, $to_officer_ids, $from_office_id, $to_office_ids);
            } else if ($media[$i] == "sms") {
                $this->sendSMS($event_id, $event_type, $template_id, $from_officer_id, $to_officer_ids, $from_office_id, $to_office_ids);
            } else if ($media[$i] == "system") {
                $this->sendSystemAlert($event_id, $event_type, $template_id, $from_officer_id, $to_officer_ids, $from_office_id, $to_office_ids);
            }
        }
    }

    public function sendEmail($event_id = 0, $event_type = "", $template_id = 0, $from_officer_id = 0, $to_officer_ids = array(), $from_office_id = 0, $to_office_ids = array()) {
		
        $table_entity_ntm = TableRegistry::get('NotificationTemplateMessages');
        $template_message = $table_entity_ntm->find()->select(['title_bng', 'template_message_bng'])->where(['template_id' => $template_id, 'media' => 'email'])->toArray();

        $title = $template_message[0]['title_bng'];
        $message = $template_message[0]['template_message_bng'];

        $table_entity_nm = TableRegistry::get('NotificationMessages');

        for ($i = 0; $i < count($to_officer_ids); $i++) {

            $NotificationMessageAction = $table_entity_nm->newEntity();
            $NotificationMessageAction->event_id = $event_id;
            $NotificationMessageAction->event_type = $event_type;
            $NotificationMessageAction->title_bng = $title;
            $NotificationMessageAction->message_bng = $message;
            $NotificationMessageAction->from_user_id = $from_officer_id;
            $NotificationMessageAction->to_user_id = $to_officer_ids[$i];
            $NotificationMessageAction->from_office_id = $from_office_id;
            $NotificationMessageAction->to_office_id = $to_office_ids[$i];
            $NotificationMessageAction->media = 'email';
			

            $table_entity_nm->save($NotificationMessageAction);

        }
		
    }

    public function sendSMS($event_id = 0, $event_type = "", $template_id = 0, $from_officer_id = 0, $to_officer_ids = array(), $from_office_id = 0, $to_office_ids = array()) {

        $table_entity_ntm = TableRegistry::get('NotificationTemplateMessages');
        $template_message = $table_entity_ntm->find()->select(['title_bng', 'template_message_bng'])->where(['template_id' => $template_id, 'media' => 'sms'])->toArray();

//        echo $template_id;
//        echo "<pre>";
//        print_r($template_message);
//        echo "</pre>";
//        die('dd');

        if (count($template_message) > 0) {
            $title = $template_message[0]['title_bng'];
            $message = $template_message[0]['template_message_bng'];

            $table_entity_nm = TableRegistry::get('NotificationMessages');

            for ($i = 0; $i < count($to_officer_ids); $i++) {

                $NotificationMessageAction = $table_entity_nm->newEntity();
                $NotificationMessageAction->event_id = $event_id;
                $NotificationMessageAction->event_type = $event_type;
                $NotificationMessageAction->title_bng = $title;
                $NotificationMessageAction->message_bng = $message;
                $NotificationMessageAction->from_user_id = $from_officer_id;
                $NotificationMessageAction->to_user_id = $to_officer_ids[$i];
                $NotificationMessageAction->from_office_id = $from_office_id;
                $NotificationMessageAction->to_office_id = $to_office_ids[$i];
                $NotificationMessageAction->media = 'sms';

                $table_entity_nm->save($NotificationMessageAction);
            }
        }
    }

    public function sendSystemAlert($event_id = 0, $event_type = "", $template_id = 0, $from_officer_id = 0, $to_officer_ids = array(), $from_office_id = 0, $to_office_ids = array()) {

        $table_entity_ntm = TableRegistry::get('NotificationTemplateMessages');
        $template_message = $table_entity_ntm->find()->select(['title_bng', 'template_message_bng'])->where(['template_id' => $template_id, 'media' => 'system'])->toArray();

        if (count($template_message) > 0) {
            $title = $template_message[0]['title_bng'];
            $message = $template_message[0]['template_message_bng'];

            $table_entity_nm = TableRegistry::get('NotificationMessages');

            for ($i = 0; $i < count($to_officer_ids); $i++) {

                $NotificationMessageAction = $table_entity_nm->newEntity();
                $NotificationMessageAction->event_id = $event_id;
                $NotificationMessageAction->event_type = $event_type;
                $NotificationMessageAction->title_bng = $title;
                $NotificationMessageAction->message_bng = $message;
                $NotificationMessageAction->from_user_id = $from_officer_id;
                $NotificationMessageAction->to_user_id = $to_officer_ids[$i];
                $NotificationMessageAction->from_office_id = $from_office_id;
                $NotificationMessageAction->to_office_id = $to_office_ids[$i];
                $NotificationMessageAction->media = 'system';

                $table_entity_nm->save($NotificationMessageAction);
            }
        }
    }

}

// Attach the UserStatistic object to the Order's event manager
