<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PostListener
 *
 * @author a2i
 */
namespace App\Event;

use Cake\Log\Log;
use Cake\Event\EventListenerInterface;

class UserDeviceRegistrationListener implements EventListenerInterface{

    public function implementedEvents() {
        die;
        return array(
            'Model.UserDeviceRegistration.afterSave' => 'activateRequest',
        );
    }

    public function activateRequest($event,  $entity, $options) {
        pr($event);
        pr($entity);
        pr($options);
        Log::error("test");
        die;
    }
}