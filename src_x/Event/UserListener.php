<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PostListener
 *
 * @author a2i
 */

namespace App\Event;

use Cake\Log\Log;
use Cake\Event\EventListenerInterface;

class UserListener implements EventListenerInterface {

    public function implementedEvents() {
        return array(
            'Model.User.created' => 'updateUserLog',
        );
    }

    public function updateUserLog($event = array(), $entity = array(), $options = array()) {

        echo "<pre>";
        print_r($options);
        echo "</pre>";

        echo "<pre>";
        print_r($entity);
        echo "</pre>";
        die();
        Log::write('debug', 'Something did not work');
    }

}

// Attach the UserStatistic object to the Order's event manager
