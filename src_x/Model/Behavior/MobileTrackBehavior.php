<?php

namespace App\Model\Behavior;

use Cake\Controller\Controller;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\Network\Request;
use Cake\ORM\Behavior;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Cache\Cache;

/**
 * MobileTrack behavior
 */
class MobileTrackBehavior extends Behavior
{

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    public function beforeSave(\Cake\Event\Event $event, \Cake\Datasource\EntityInterface $entity)
    {
        $tokendata = [];
        if (empty($tokendata)) {
            $BrowserInfo = getBrowser();
            $BrowserData= (!empty($BrowserInfo['name'])?($BrowserInfo['name'].'-'):''). (!empty($BrowserInfo['version'])?($BrowserInfo['version'].' - '):'').(!empty($BrowserInfo['platform'])?($BrowserInfo['platform'].'-'):'');
            $tokendata['device_type'] = (!empty($BrowserData)) ?substr($BrowserData, 0,49): '';
            $tokendata['device_id'] = getIP();
         }
        $entity->set('token', !empty($tokendata['id']) ? $tokendata['id'] : '');
        $entity->set('device_type', !empty($tokendata) ? $tokendata['device_type'] : 'web');
        $entity->set('device_id', !empty($tokendata) ? $tokendata['device_id'] : '');
    }
}
