<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use \Cake\ORM\TableRegistry;

class EmployeeRecords extends Entity
{
    protected $_virtual = ['user_alias'];
    protected function _getUserAlias()
    {
        $usersTable = TableRegistry::get('Users');

        if(!empty($this->_properties['id'])){
            $alias = $usersTable->find()->select(['user_alias','username'])->where(['employee_record_id'=>$this->_properties['id']])->first();

            if(!empty($alias)){
                return $alias['user_alias'];
            }else{
                return $alias['username'];
            }
        }

        return '';
    }
}
