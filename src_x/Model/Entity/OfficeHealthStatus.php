<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * OfficeHealthStatus Entity.
 */
class OfficeHealthStatus extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'office_id' => true,
        'has_error' => true,
        'health_report' => true,
        'check_date' => true,
        'last_check' => true,
        'created_by' => true,
        'modified_by' => true,
        'office' => true,
    ];
}
