<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * OfficeUnitSeal Entity.
 */
class OfficeUnitSeal extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'designation_name_bng' => true,
        'designation_name_eng' => true,
        'office_id' => true,
        'office_unit_organogram_id' => true,
        'office_unit_id' => true,
        'unit_name_bng' => true,
        'unit_name_eng' => true,
        'seal_owner_unit_id' => true,
        'designation_seq' => true,
        'designation_level' => true,
        'created_by' => true,
        'modified_by' => true,
        'office' => true,
        'office_unit_organogram' => true,
        'office_unit' => true,
        'seal_owner_unit' => true,
    ];
}
