<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
/**
 * Potrojari Entity.
 */
class Potrojari extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'nothi_master_id' => true,
        'nothi_part_no' => true,
        'potrojari_draft_office_id' => true,
        'potrojari_draft_unit' => true,
        'nothi_notes_id' => true,
        'nothi_potro_id' => true,
        'potro_type' => true,
        'sarok_no' => true,
        'dak_id' => true,
        'can_potrojari' => true,
        'office_id' => true,
        'officer_id' => true,
        'office_name' => true,
        'officer_name' => true,
        'office_unit_id' => true,
        'office_unit_name' => true,
        'officer_designation_id' => true,
        'officer_designation_label' => true,
        'approval_office_id' => true,
        'approval_officer_id' => true,
        'approval_office_unit_id' => true,
        'approval_officer_designation_id' => true,
        'approval_office_name' => true,
        'approval_officer_name' => true,
        'approval_office_unit_name' => true,
        'approval_officer_designation_label' => true,
        'sovapoti_office_id' => true,
        'sovapoti_officer_id' => true,
        'sovapoti_officer_designation_id' => true,
        'sovapoti_officer_designation_label' => true,
        'sovapoti_officer_name' => true,
        'potrojari_date' => true,
        'potro_subject' => true,
        'potro_security_level' => true,
        'potro_priority_level' => true,
        'potro_cover' => true,
        'potro_description' => true,
        'receiving_office_id' => true,
        'receiving_office_unit_id' => true,
        'receiving_office_unit_name' => true,
        'receiving_officer_id' => true,
        'receiving_officer_designation_id' => true,
        'receiving_officer_designation_label' => true,
        'receiving_officer_name' => true,
        'potro_status' => true,
        'receiver_sent' => true,
        'onulipi_sent' => true,
        'is_summary_nothi' => true,
        'created_by' => true,
        'modified_by' => true,
    ];

    protected $_virtual = [
//        'total_error'
    ];

//    protected function _getTotalError(){
//        $receiverTable = TableRegistry::get('PotrojariReceiver');
//        $onulipiTable = TableRegistry::get('PotrojariOnulipi');
//
//        $totalError = 0;
//        if(isset($this->_properties['id'])) {
//            $totalError = $receiverTable->SentStatus($this->_properties['id'], [], 0)->count();
//            $totalError += $onulipiTable->SentStatus($this->_properties['id'], [], 0)->count();
//        }
//        return $totalError;
//    }
}
