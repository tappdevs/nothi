<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PotrojariHeaderSetting Entity.
 */
class PotrojariHeaderSetting extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'employee_record_id' => true,
        'office_id' => true,
        'office_unit_id' => true,
        'office_unit_organogram_id' => true,
        'potrojari_head' => true,
        'created_by' => true,
        'modified_by' => true,
        'employee_record' => true,
        'office' => true,
        'office_unit' => true,
        'office_unit_organogram' => true,
        'write_unit' => true,
    ];
}
