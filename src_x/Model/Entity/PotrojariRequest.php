<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PotrojariRequest Entity.
 */
class PotrojariRequest extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'command' => true,
        'office_id' => true,
        'potrojari_id' => true,
        'try' => true,
        'status' => true,
    ];
}
