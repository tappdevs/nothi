<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ProtikolpoLog Entity.
 */
class ProtikolpoLog extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'protikolpo_id' => true,
        'protikolpo_start_date' => true,
        'protikolpo_end_date' => true,
        'protikolpo_ended_by' => true,
        'employee_office_id_from_name' => true,
        'employee_office_id_to_name' => true,
        'protikolpo_status' => true,
        'protikolpo' => true,
    ];
}
