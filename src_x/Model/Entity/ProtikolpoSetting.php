<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ProtikolpoSetting Entity.
 */
class ProtikolpoSetting extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'office_id' => true,
        'unit_id' => true,
        'designation_id' => true,
        'employee_record_id' => true,
        'protikolpos' => true,
        'selected_protikolpo' => true,
        'start_date' => true,
        'end_date' => true,
        'active_status' => true,
        'created_by' => true,
        'modified_by' => true,
        'office' => true,
        'unit' => true,
        'designation' => true,
        'employee_record' => true,
    ];
}
