<?php
namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;


class ApiUsersTable extends ProjapotiTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $conn = ConnectionManager::get('ApiAuthDB');
        $this->connection($conn);
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
    }
    
    
    public function getAccess($data = []){
        
        return $this->find()->where(['username'=>$data['username'],'password'=>$data['password'],'project_ip IN'=>$data['ip']])->first();
    }
    
    
}