<?php

namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;

class ArchiveNothiMasterPermissionsTable extends ProjapotiTable
{
    public function initialize(array $config)
    {
        /*$conn = ConnectionManager::get('NothiAccessDb');
        $this->connection($conn);*/
        parent::initialize($config);

        $this->addBehavior('Timestamp');
        $this->addBehavior('MobileTrack');
    }

    public function validationAdd($validator)
    {
        $validator
            ->notEmpty('nothi_masters_id', "এই তথ্য ফাকা রাখা যাবে না")
            ->notEmpty('office_unit_organograms_id', "এই তথ্য ফাকা রাখা যাবে না");

        return $validator;
    }

    public function validationEdit($validator)
    {
        return $validator;
    }

    public function hasThisType($typeId)
    {
        return $this->find()->select(['id'])->where(['nothi_types_id' => $typeId])->count();
    }

    public function getMasterNothiList($nothi_office, $officeId, $officeUnitId, $officeUnitOrgranogramsId, $nothi_master_id = false, $nothi_part_no = false)
    {
        $query = $this->find()->select(['nothi_masters_id'])->where(['office_id' => $officeId, 'office_unit_id' => $officeUnitId, 'office_unit_organograms_id' => $officeUnitOrgranogramsId, 'nothi_office' => $nothi_office]);

        if (!empty($nothi_office)) {
            $query->where(['nothi_office' => $nothi_office]);
        }
        if ($nothi_master_id) {
            $query->where(['nothi_masters_id' => $nothi_master_id]);
        }
        if ($nothi_part_no) {
            $query->where(['nothi_part_no' => $nothi_part_no]);
        }

        return $query->toArray();
    }

    public function getMasterNothiPartList($nothi_office, $officeId, $officeUnitId, $officeUnitOrgranogramsId, $nothimaster_id = 0)
    {
        $query = $this->find()->where(['office_id' => $officeId, 'office_unit_id' => $officeUnitId, 'office_unit_organograms_id' => $officeUnitOrgranogramsId, 'privilige_type >' => 0, 'nothi_office' => $nothi_office]);
        if (!empty($nothimaster_id)) {
            $query->where(['nothi_masters_id' => $nothimaster_id]);
        }

        return $query->toArray();
    }

    public function getMasterNothiListbyMasterId($nothiMasterId, $nothi_office = 0)
    {
        return $this->find()->select(['nothi_masters_id', 'office_id', 'designation_level', 'office_unit_id', 'office_unit_organograms_id'])->where(['nothi_masters_id' => $nothiMasterId, 'nothi_office' => $nothi_office])->toArray();
    }

    public function getMasterNothiListbyPartId($NothiPartId = 0, $nothi_office = 0, $nothiMasterId = 0)
    {

        if (!empty($nothiMasterId)) {
            return $this->find()->select(['nothi_masters_id', 'nothi_part_no', 'office_id', 'designation_level', 'office_unit_id', 'office_unit_organograms_id'])->where(['nothi_part_no' => $NothiPartId, 'nothi_office' => $nothi_office, 'nothi_masters_id' => $nothiMasterId])->distinct(['office_unit_organograms_id'])->toArray();
        } else {
            return $this->find()->select(['nothi_masters_id', 'nothi_part_no', 'office_id', 'designation_level', 'office_unit_id', 'office_unit_organograms_id'])->where(['nothi_part_no' => $NothiPartId, 'nothi_office' => $nothi_office])->distinct(['office_unit_organograms_id'])->toArray();
        }
    }

    public function hasAccess($nothi_office, $officeId, $officeUnitId, $officeUnitOrgranogramsId, $nothiMasterId, $nothiPartNo = 0)
    {
        $query = $this->find()->where(['office_id' => $officeId, 'office_unit_id' => $officeUnitId, 'office_unit_organograms_id' => $officeUnitOrgranogramsId, 'nothi_masters_id' => $nothiMasterId, 'nothi_office' => $nothi_office]);

        if (!empty($nothiPartNo)) {
            $query = $query->where(['nothi_part_no' => $nothiPartNo]);
        }

        return $query->first();
    }

    public function hasAccessList($nothi_office, $officeId, $officeUnitId, $officeUnitOrgranogramsId, $nothiMasterId, $nothiPartNo = 0)
    {

        $query = $this->find()->where(['office_id' => $officeId, 'office_unit_id' => $officeUnitId, 'office_unit_organograms_id' => $officeUnitOrgranogramsId, 'nothi_masters_id' => $nothiMasterId, 'nothi_office' => $nothi_office]);

        if (!empty($nothiPartNo)) {
            $query = $query->where(['nothi_part_no' => $nothiPartNo]);
        }

        return $query->first();
    }

    public function hasAccessPartList($officeId, $officeUnitId, $officeUnitOrgranogramsId, $nothiPartNo = 0, $nothi_office_id = 0, $nothi_master_id = 0)
    {

        $query = $this->find()->where(['office_id' => $officeId, 'office_unit_id' => $officeUnitId, 'office_unit_organograms_id' => $officeUnitOrgranogramsId, 'nothi_part_no' => $nothiPartNo]);

        if (!empty($nothi_office_id)) {
            $query->where(['nothi_office' => $nothi_office_id]);
        }
        if (!empty($nothi_master_id)) {
            $query->where(['nothi_masters_id' => $nothi_master_id]);
        }

        return $query->first();
    }

    public function allNothiPermissionList($nothi_office, $officeId, $officeUnitId, $officeUnitOrgranogramsId, $visited = -1)
    {

        $query = $this->find('list', ['keyField' => 'id', 'valueField' => 'nothi_part_no'])->where(['office_id' => $officeId, 'office_unit_id' => $officeUnitId, 'office_unit_organograms_id' => $officeUnitOrgranogramsId, 'nothi_office' => $nothi_office]);

        if ($visited != -1) {
            $query->where(['visited' => $visited]);
        }

        return $query->toArray();
    }

    public function get_other_offices($own_office_id, $master_id, $nothi_office)
    {
        return $this->find()->where(['office_id <>' => $own_office_id, 'nothi_part_no' => $master_id, 'nothi_office' => $nothi_office])->distinct(['office_unit_organograms_id'])->toArray();
    }

    public function UnitwiseNothiPermissionList($nothi_office, $officeId, $officeUnitId, $visited = -1)
    {

        $query = $this->find('list', ['keyField' => 'id', 'valueField' => 'nothi_part_no'])->where(['office_id' => $officeId, "office_unit_id IN ({$officeUnitId})", 'nothi_office' => $nothi_office]);

//        if($visited!=-1){
//            $query->where(['visited'=>$visited]);
//        }

        return $query->toArray();
    }

    public function getNothiPermissionsList($designation_id = 0)
    {

        $query = $this->find('list', ['keyField' => 'id', 'valueField' => 'nothi_part_no'])->where(['office_unit_organograms_id' => $designation_id]);

        return $query->toArray();
    }

    public function permittedOfficesId($part_id, $nothi_office)
    {
        return $this->find('list', ['keyField' => 'office_id', 'valueField' => 'office_id'])->where(['nothi_part_no' => $part_id, 'nothi_office' => $nothi_office])->toArray();
    }

    public function getPermissionListForNothiPart($officeId = 0, $officeUnitId = 0, $officeUnitOrgranogramsId = 0, $nothiPartNo = 0, $nothi_office_id = 0, $nothi_master_id = 0)
    {
        $query = $this->find();

        if (!empty($officeId)) {
            $query->where(['office_id' => $officeId]);
        }
        if (!empty($officeUnitId)) {
            $query->where(['office_unit_id' => $officeUnitId]);
        }
        if (!empty($officeUnitOrgranogramsId)) {
            $query->where(['office_unit_organograms_id' => $officeUnitOrgranogramsId]);
        }
        if (!empty($nothiPartNo)) {
            $query->where(['nothi_part_no' => $nothiPartNo]);
        }
        if (!empty($nothi_office_id)) {
            $query->where(['nothi_office' => $nothi_office_id]);
        }
        if (!empty($nothi_master_id)) {
            $query->where(['nothi_masters_id' => $nothi_master_id]);
        }
        return $query;
    }

    public function permittedOfficesIdByNothiMasterID($nothi_master_id, $nothi_office)
    {
        return $this->find('list', ['keyField' => 'office_id', 'valueField' => 'office_id'])->where(['nothi_masters_id' => $nothi_master_id, 'nothi_office' => $nothi_office])->toArray();
    }

    public function getPermittedNothis($conditions = [], $list = false)
    {
        $query = $this;
        if($list){
            $query->find('list',$list);
        }else{
            $query->find();
        }

        if(!empty($conditions)){
            $query->where($conditions);
        }

        return $query;
    }

}