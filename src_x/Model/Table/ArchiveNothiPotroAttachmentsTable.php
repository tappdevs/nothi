<?php

namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;

class ArchiveNothiPotroAttachmentsTable extends ProjapotiTable
{

    public function initialize(array $config)
    {
        $this->primaryKey('id');
        $this->belongsTo('Potrojari', [
            'foreignKey' => 'potrojari_id',
            'joinType' => 'LEFT'
        ]);

        $this->belongsTo('ArchiveNothiPotros', [
            'foreignKey' => 'nothi_potro_id',
            'joinType' => 'INNER',
        ]);

        $this->belongsTo('PotrojariData', [
            'className'=>'Potrojari',
            'foreignKey' => 'potrojari_id',
            'conditions' => 'ArchiveNothiPotroAttachments.potrojari_id = PotrojariData.id and ArchiveNothiPotroAttachments.nothi_master_id = PotrojariData.nothi_master_id and ArchiveNothiPotroAttachments.nothi_part_no = PotrojariData.nothi_part_no',
            'joinType' => 'left',
        ]);
    }

    public function getPotro($condition = '', $order = 'id desc', $toArray = 1)
    {

        $queryString = $this->find();

        if (!empty($condition)) {
            $queryString = $queryString->where([$condition]);
        }

        if (!empty($order)) {
            $queryString = $queryString->order([$order]);
        }

        if ($toArray) {
            $queryString = $queryString->toArray();
        }

        return $queryString;
    }

    public function getOnlyMainPotro($nothimasterid, $limit)
    {
        $query = "SELECT 
            m1.*
FROM nothi_potro_attachments m1 LEFT JOIN nothi_potro_attachments m2
 ON (m1.nothi_potro_id = m2.nothi_potro_id AND m1.id < m2.id)
WHERE m2.id IS NULL AND m1.nothi_master_id = {$nothimasterid}
ORDER BY `m1`.`nothi_potro_id` DESC limit {$limit}";

        $conn = ConnectionManager::get('default');
        $stmt = $conn->query($query);
        $stmt->execute();
        return $stmt->fetchAll('assoc');
    }

    public function getAllPotro($nothi_potro_id)
    {

        $data  = $this->find()->where(['nothi_potro_id' => $nothi_potro_id, 'attachment_type <>' => 'text','status'=>1])->order(['id asc'])->toArray();
        $data2 = $this->find()->where(['nothi_potro_id' => $nothi_potro_id, 'attachment_type' => 'text','status'=>1])->order(['id desc'])->toArray();

        return $main_data = array_merge($data2, $data);
    }
}