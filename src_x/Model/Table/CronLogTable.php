<?php

namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\Datasource\ConnectionManager;
use Cake\Controller\Component;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

class CronLogTable extends ProjapotiTable
{

    public function initialize(array $config)
    {
        //parent::initialize($config);
        $conn = ConnectionManager::get('NothiReportsDb');
        $this->connection($conn);
        $this->table('cron_log');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
    }
    public function saveData($cron_name , $op_date, $start_time, $end_time , $total, $success , $failed , $details ){
        $entity = $this->newEntity();
        $entity->cron_name = $cron_name;
        $entity->operation_date = $op_date;
        $entity->start_time = $start_time;
        $entity->end_time = $end_time;
        $entity->total_request = $total;
        $entity->failed = $failed;
        $entity->success = $success;
        $entity->details = json_encode($details);
        if($this->save($entity)){
            return 1;
        }
        return 0;
    }
    /**
     *
     * @param int $id
     * @param date $date
     * @param string $name
     * @return object Query Object will return
     */
    public function getData($id= 0,$date =[],$name = ''){
        $query = $this->find();
        if(!empty($id)){
            $query->where(['id' => $id]);
        }
        if(!empty($date)){
            if(!empty($date['start'])){
                 $query->where(['DATE(operation_date) >=' => $date['start']]);
            }
            if(!empty($date['end'])){
                 $query->where(['DATE(operation_date) <=' => $date['end']]);
            }
        }
        if(!empty($name)){
            $query->where(['cron_name LIKE' => "%{$name}%"]);
        }
        return $query;
    }


}