<?php

namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\Datasource\ConnectionManager;
use Cake\Controller\Component;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

class CronRequestTable extends ProjapotiTable
{

    public function initialize(array $config)
    {
        //parent::initialize($config);
        $conn = ConnectionManager::get('NothiReportsDb');
        $this->connection($conn);
        $this->table('cron_request');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
    }
    public function saveData($command){
        //first check if command is saved
        if(!empty($command)){
           $hasData = $this->find()->where(['command' => $command])->count();
           if($hasData > 0){
               return 1;
           }
        }
        $entity = $this->newEntity();
        $entity->command = $command;
        if($this->save($entity)){
            return 1;
        }
        return 0;
    }
    /**
     *
     * @param int $id
     * @param date $date
     * @param string $name
     * @return object Query Object will return
     */
    public function getData($limit = 0){
        $query = $this->find();
        if(!empty($limit)){
            $query->limit($limit);
        }
        return $query;
    }


}