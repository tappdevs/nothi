<?php
namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

class DakAttachmentCommentTable extends ProjapotiTable
{
    public function initialize(array $config)
    {
        $this->primaryKey('id');
    }

    function getLastByAttachment($attachmentId){
        return $this->find()->where(['attachment_id'=>$attachmentId])->order(['id desc'])->first();
    }

    function getAllCommentsbyAttachment($dak_id, $dak_type = "Daptorik"){
        return $this->find()->where(['dak_id'=>$dak_id, 'dak_type'=>$dak_type])->order(['id asc'])->toArray();
    }
}