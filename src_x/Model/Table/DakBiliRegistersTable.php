<?php
namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;


class DakBiliRegistersTable extends ProjapotiTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->primaryKey('id');
        $this->displayField('receiving_office_unit_id');
        $this->displayField('dak_sarok_no');
        $this->displayField('sending_date');
        $this->displayField('sender_name');
        $this->displayField('sender_office_unit_name');
        $this->displayField('sender_officer_designation_label');
        $this->displayField('sender_office_name');
        $this->displayField('dak_subject');
        $this->addBehavior('Timestamp');
    }
}