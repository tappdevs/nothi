<?php

namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

class DakDaptoriksTable extends ProjapotiTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
    }

    public function validationAdd($validator)
    {
        $validator
            ->notEmpty('sender_office_name', "প্রেরকের কার্যালয়ের নাম দিন")
            ->notEmpty('dak_subject', "ডাকের বিষয় দিন")
            ->add('sender_sarok_no', 'isUnique', [
                'rule' => function($data, $provider){
                    if(empty($data) || $data=='প্রযোজ্য নহে') return true;
                   
                if(!empty($provider['data']['id'])){
                    if($this->find()->where(['sender_sarok_no'=>$data, 'id <>' => $provider['data']['id']])->count()>0)
                            return 'স্মারক নম্বর পূর্বে ব্যবহৃত হয়েছে';
                }elseif($this->find()->where(['sender_sarok_no'=>$data])->count()>0)
                        return 'স্মারক নম্বর পূর্বে ব্যবহৃত হয়েছে';
                    
                    return true;
                },
                'message' => __('স্মারক নম্বর পূর্বে ব্যবহৃত হয়েছে')
            ])
            ->notEmpty('receiving_office_id', "প্রাপকের কার্যালয় বাছাই করুন")->notEmpty('receiving_officer_id', "প্রাপক বাছাই করুন");

        return $validator;
    }

    public function getDakCount($office_id)
    {
        return $this->find()->where(['sender_office_id' => $office_id])->count();
    }

    public function getDakBy_Status_Designation_Employee($status, $designation_id, $employee_record_id)
    {
        return $this->find()->where(['dak_status' => $status, 'created_by' => $employee_record_id, 'uploader_designation_id' => $designation_id]);
    }

    public function getDakCountBy_Status_Designation_Employee($status, $designation_id, $employee_record_id)
    {
        return $this->find()->where(['dak_status' => $status, 'created_by' => $employee_record_id, 'uploader_designation_id' => $designation_id])->count();
    }

    public function getDakBy_Status_Designation($status, $designation_id)
    {
        return $this->find()->where(['dak_status' => $status, 'uploader_designation_id' => $designation_id]);
    }

    public function getDakCountBy_Status_Designation($status, $designation_id)
    {
        return $this->find()->where(['dak_status' => $status, 'uploader_designation_id' => $designation_id])->count();
    }
    public function getDaptoriksDaks($ids){
        if(empty($ids))
            return;
         return $this->find()->select(['id','dak_subject', 'Utsho' =>'CONCAT(sender_name,",",sender_officer_designation_label,",",sender_office_unit_name,",",sender_office_name)','receive_date'=>'modified'])->where(['id IN'=>$ids]);
    }
}
