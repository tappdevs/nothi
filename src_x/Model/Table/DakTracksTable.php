<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Network\Email\Email;
use Cake\Datasource\ConnectionManager;
use Cake\Controller\Component;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

class DakTracksTable extends Table {
    
    public function initialize(array $config) {
        $con=  ConnectionManager::get('projapotiDb');
        $this->connection($con);
        $this->table('dak_tracks');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
    }
}