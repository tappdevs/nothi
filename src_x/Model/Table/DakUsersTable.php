<?php

namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;

class DakUsersTable extends ProjapotiTable {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        $conn = ConnectionManager::get('default');
        $this->connection($conn);
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->addBehavior('MobileTrack');
    }

    public function getDakCountBy_status_employee_designation($status, $designation_id, $dak_type = "Daptorik") {
        return $this->find()->where(['dak_view_status' => $status, 'to_officer_designation_id IN ' => $designation_id,
                    'dak_category' => DAK_CATEGORY_INBOX, 'dak_type' => $dak_type, 'is_archive' => 0])->count();
    }

    public function getDakCountBy_employee_designation($status = DAK_CATEGORY_INBOX, $designation_id, $dak_type = "Daptorik") {
        return $this->find()->where(['to_officer_designation_id IN ' => $designation_id, 'dak_category' => $status,
                    'dak_type' => $dak_type, 'is_archive' => 0])->count();
    }

    public function getTotalDakCountBy_employee_designation($status = DAK_CATEGORY_INBOX, $designation_id, $attention = -1){
        $query = $this->find()->where(['to_officer_designation_id IN ' => $designation_id, 'dak_category' => $status, 'is_archive' => 0]);
        if($attention != -1){
            $query = $query->where(['attention_type'=>$attention]);
        }

        return $query->count();
    }

    public function getDakCountBy_employee_designationWithDate($status = DAK_CATEGORY_INBOX, $designation_id, $dak_type = "Daptorik") {
        return $this->find()->where(['to_officer_designation_id IN ' => $designation_id, 'dak_category' => $status,
                    'dak_type' => $dak_type, 'is_archive' => 0]);
    }

    public function getAllUsers($dakid, $dak_type = 'Daptorik') {

        return $this->find()->where(['dak_id' => $dakid, 'dak_type' => $dak_type])->toArray();
    }

    public function getTaggedUsers($dakid, $designation_id, $dak_type = 'Daptorik') {

        return $this->find()->where(['dak_id' => $dakid, 'dak_type' => $dak_type, 'to_officer_designation_id <>' => $designation_id, "dak_category" => 'Inbox', 'attention_type' => 1])->toArray();
    }

    /**
     *
     * @param int $designation_id
     * @return int count of dak other than inbox
     */
    public function getDakCountOfOthers($designation_id) {
        return $this->find()->where(['to_officer_designation_id IN ' => $designation_id,
                    'dak_category <>' => DAK_CATEGORY_INBOX])->count();
    }

    public function getNothivuktoDakList($office_id = 0, $unit_id = 0, $time = []) {
        $query = $this->find();

        if (!empty($office_id)) {
            $query = $query->where(['to_office_id' => $office_id]);
        }
        if (!empty($unit_id)) {
            $query = $query->where(['to_office_unit_id' => $unit_id]);
        }
        if (!empty($time[0])) {
            $query = $query->where(['DATE(modified) >=' => $time[0]]);
        }
        if (!empty($time[1])) {
            $query = $query->where(['DATE(modified) <=' => $time[1]]);
        }
        return $query->where(['dak_category' => 'NothiVukto'])->toArray();
    }

    public function getOnisponnoDak($office_id = 0, $unit_id = 0, $designation_id = 0, $date = '') {
        $query = $this->find()->where(['dak_category' => 'Inbox', 'attention_type' => 1, 'is_archive' => 0]);

        if (!empty($office_id)) {
            $query->where(['to_office_id' => $office_id]);
        }

        if (!empty($unit_id)) {
            $query->where(['to_office_unit_id' => $unit_id]);
        }

        if (!empty($designation_id)) {
            $query->where(['to_officer_designation_id' => $designation_id]);
        }

        if (!empty($date)) {
            $query->where(['DATE(created) <=' => $date]);
        }

        if (!empty($office_id) && (empty($unit_id) && empty($designation_id))) {
            $query->group(['dak_id', 'dak_type']);
        }
        return $query->count();
    }

    public function getNothijatDakUser($dak_id, $dak_type) {
        return $this->find()->where(['dak_id'=>$dak_id, 'dak_type'=>$dak_type, 'dak_category'=>DAK_CATEGORY_NOTHIJATO])->hydrate(false)->first();
    }
     public function getOnisponnoDakByMultipleDesignations($designation_id = [], $date = '') {
        $query = $this->find()->where(['dak_category' => 'Inbox', 'attention_type' => 1, 'is_archive' => 0]);

        if (!empty($designation_id)) {
            $query->where(['to_officer_designation_id IN' => $designation_id]);
        }

        if (!empty($date)) {
            $query->where(['DATE(created) <=' => $date]);
        }

        $query->group(['dak_id', 'dak_type']);
        return $query->count();
    }

    public function getData($conditions = []) {

        return $this->find()->where($conditions);
    }
}
