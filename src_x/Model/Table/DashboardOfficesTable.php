<?php

namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\Datasource\ConnectionManager;
use Cake\Controller\Component;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

class DashboardOfficesTable extends ProjapotiTable
{

    public function initialize(array $config)
    {
        //parent::initialize($config);
        $conn = ConnectionManager::get('NothiReportsDb');
        $this->connection($conn);
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
    }
    public function saveData($data = ''){
        if(empty($data))
            return false;
        $DashboardOfficesEntity = $this->newEntity();
        $DashboardOfficesEntity->office_id = $data['office_id'];
        $DashboardOfficesEntity->operation_date = $data['operation_date'];
        $DashboardOfficesEntity->dak_inbox =$data['dak_inbox'];
        $DashboardOfficesEntity->dak_outbox = $data['dak_outbox'];
        $DashboardOfficesEntity->nothivukto = $data['nothivukto'];
        $DashboardOfficesEntity->nothijat = $data['nothijat'];
        $DashboardOfficesEntity->dak_nisponno = $data['dak_nisponno'];
        $DashboardOfficesEntity->dak_onisponno = $data['dak_onisponno'];
        $DashboardOfficesEntity->self_note = $data['self_note'];
        $DashboardOfficesEntity->dak_note = $data['dak_note'];
        $DashboardOfficesEntity->potrojari_nisponno = $data['potrojari_nisponno'];
        $DashboardOfficesEntity->note_nisponno = $data['note_nisponno'];
        $DashboardOfficesEntity->onisponno_note = $data['onisponno_note'];
        $DashboardOfficesEntity->potrojari = $data['potrojari'];

        return $this->save($DashboardOfficesEntity);
    }
     public function checkUpdate($office_id = 0, $date = '')
    {
        if (!empty($date) && !empty($office_id)) {
            return $this->find()->where(['operation_date' => $date, 'office_id' => $office_id])->count();
        }
    }
     public function getLastUpdateDate($office_id = 0)
    {
        if (!empty($office_id)) {
            return $this->find()->select(['operation_date'])->where(['office_id' => $office_id])->order(['operation_date desc'])->first();
        }
    }
    public function getData($office_id = 0){
        if(empty($office_id))
            return;
               $condition = [
            'totalInbox' => 'dak_inbox', 'totalOutbox' => 'dak_outbox', 'totalNothijato' => 'nothijat',
            'totalNothivukto' => 'nothivukto', 'totalNisponnoDak' => 'dak_nisponno','totalOnisponnodakall' => 'dak_onisponno',
                   
            'totalSouddog' => 'self_note','totalDaksohoNote' => 'dak_note',
            'totalNisponnoNote' => 'note_nisponno','totalNisponnoPotrojari' =>'potrojari_nisponno',
            'totalONisponno' => 'onisponno_note',

           'totalyesterdayinbox'=>'yesterday_inbox','totalyesterdaynothivukto' => 'yesterday_nothivukto','totalyesterdaynothijato' => 'yesterday_nothijat',
            'totalyesterdaypotrojari' => 'yesterday_potrojari','totalyesterdayOnisponnodak' => 'yesterday_onisponno_dak',

            'totalyesterdaydaksohonote' => 'yesterday_dak_note', 'totalyesterdaysrijitonote' => 'yesterday_self_note','totalyesterdaynisponnonote' => 'yesterday_niponno_note','totalyesterdaynisponnopotrojari' => 'yesterday_nisponno_potrojari', 'totalyesterdayOnisponnonote'=> 'yesterday_onisponno_note',

            'totalID' => 'count(id)', 'totalPotrojari' => 'potrojari'
        ];
        return  $this->find()->select($condition)->where(['office_id' => $office_id])->first();
    }

}