<?php

namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;

class DashboardTable extends ProjapotiTable
{

    public function oldNothiDashboardCount($office_id = 0, $unit_id = 0, $designation_id = 0,
                                        $time = [])
    {
        set_time_limit(0);
        TableRegistry::remove('NothiMasterCurrentUsers');
        TableRegistry::remove('NisponnoRecords');
        TableRegistry::remove('NothiDakPotroMaps');
        TableRegistry::remove('NothiParts');
        TableRegistry::remove('NothiNotes');
        $NothiMasterCurrentUserTable = TableRegistry::get('NothiMasterCurrentUsers');
        $NisponnoRecordsTable        = TableRegistry::get('NisponnoRecords');
        $NothiDakPotroMapsTable      = TableRegistry::get('NothiDakPotroMaps');
        $NothiPartsTable      = TableRegistry::get('NothiParts');
        $NothiNotesTable      = TableRegistry::get('NothiNotes');

        $getAllParts        = $NothiMasterCurrentUserTable->getAllNothiPartNo($office_id, $unit_id,
                $designation_id, $time,1)->toArray();
//        $dak_note           = $NothiDakPotroMapsTable->checkIsDakSrijitoNote('',$time);
        $dak_note           = $NothiPartsTable->dakSrijitoNoteCount($office_id, $unit_id,
                $designation_id, $time);
        $all_nisponno = $NisponnoRecordsTable->getNisponnoCount($office_id,$unit_id,$designation_id,$time);
        $nisponno_potrojari = $all_nisponno['potrojari'];
        $nisponno_note      = $all_nisponno['note'];
        $onisponno          = count($getAllParts);
        $self_note  = $NothiPartsTable->selfSrijitoNoteCount($office_id,$unit_id,$designation_id,$time);
        $allDatabyPotrojari = $NisponnoRecordsTable->getLastNisponnoByPotrojari($office_id,$unit_id,$designation_id,$time);
        $allDatabyNote = $NisponnoRecordsTable->getLastNisponnoByNote($office_id,$unit_id,$designation_id,$time);
        if(!empty($allDatabyPotrojari)){
            foreach($allDatabyPotrojari as $P_No => $OP_Date){
                $has_onucched = $NothiNotesTable->findNoteAfterNisponno($P_No,$OP_Date);
                if($has_onucched > 0){
                    $from_dak = $NothiDakPotroMapsTable->checkPartIsFromDak($P_No,$OP_Date);
                    if($from_dak > 0){
                        $dak_note++;
                    }else{
                        $self_note++;
                    }
                }
            }
        }
        if(!empty($allDatabyNote)){
            foreach($allDatabyNote as $P_No => $OP_Date){
                $has_onucched = $NothiNotesTable->findNoteAfterNisponno($P_No,$OP_Date);
                if($has_onucched > 0){
                    $from_dak = $NothiDakPotroMapsTable->checkPartIsFromDak($P_No,$OP_Date);
                    if($from_dak > 0){
                        $dak_note++;
                    }else{
                        $self_note++;
                    }
                }
            }
        }

        $data = [
            'selfNote' => $self_note,
            'dakNote' => $dak_note,
            'potrojariNisponno' => $nisponno_potrojari,
            'noteNisponno' => $nisponno_note,
            'onisponno' => $onisponno,
//            'total' => $total,
//            'noNotes' => $has,
        ];
//        if(!empty($office_id)){
//
//        }
        return $data;
    }

    public function OfficeDashboardUpdate($officeid = 0)
    {
        set_time_limit(0);
        ini_set('memory_limit', '-1');

        TableRegistry::remove('DakMovements');
        $DakMovementsTable = TableRegistry::get('DakMovements');
        TableRegistry::remove('Potrojari');
        $potrojariTable    = TableRegistry::get('Potrojari');
        TableRegistry::remove('DakUsers');
        $DakUsersTable     = TableRegistry::get('DakUsers');
        $today             = date('Y-m-d');
        $yesterday         = date('Y-m-d', strtotime($today.' -1 day'));


        /*         * Inbox Total* */
        $totalprevinbox = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0, 'Inbox',
            [ '', $yesterday]);

        /*         * Inbox Total* */

        /*         * Outbox Total* */
        $totalprevoutbox = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0, 'Outbox',
            [ '', $yesterday]);

        /*         * Outbox Total* */

        /*         * Nothivukto * */
        $totalprevnothivukto = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0,
            'NothiVukto', [ '', $yesterday]);

        /*         * Nothivukto * */

        /*         * Nothijato * */
        $totalprevnothijato = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0, 'NothiJato',
            [ '', $yesterday]);

        /*         * Nothijato * */

        /*         * Potrojari Nisponno* */
        $totalprepotrojari = $potrojariTable->allPotrojariCount($officeid, 0, 0, [ '', $yesterday]);

        /*         * Potrojari Nisponno* */

        /*         * Result* */

        $returnSummary['totalinboxall'] = $totalprevinbox;

        $returnSummary['totaloutboxall'] = $totalprevoutbox;

        $returnSummary['totalnothivuktoall'] = $totalprevnothivukto;

        $returnSummary['totalnothijatoall'] = $totalprevnothijato;

        $returnSummary['totalpotrojariall'] = $totalprepotrojari;

        /*         * Nisponno Dak* */

        $returnSummary['totalnisponnodakall'] = $returnSummary['totalnothijatoall'] + $returnSummary['totalnothivuktoall'];

        /*         * Nisponno Dak* */

        /*         * Onisponno Dak* */
        $returnSummary['totalOnisponnodakall'] = $DakUsersTable->getOnisponnoDak($officeid, 0, 0,
            $yesterday);

        /*         * Onisponno Dak* */



        /* --- Dak Part End --- */

        $temp = $this->nothiDashboardCount($officeid, 0, 0, [ '', $yesterday]);


        /*         * Self Given Note* */
        $returnSummary['totalsrijitonoteall'] = $temp['selfNote'];

        /*         * Self Given Note* */

        /*         * Dak Soho Note* */
        $returnSummary['totaldaksohonoteall'] = $temp['dakNote'];

        /*         * Dak Soho Note* */

        /*         * Nisponno by Note* */
        $returnSummary['totalnisponnonoteall'] = $temp['noteNisponno'];

        /*         * Nisponno by Note* */

        /*         * Nisponno by Potrojari* */
        $returnSummary['totalnisponnopotrojariall'] = $temp['potrojariNisponno'];

        /*         * Nisponno by Potrojari* */

        /*         * Onisponno Note* */
        $returnSummary['totalOnisponnonoteall'] = $temp['onisponno'];

        /*         * Onisponno Note* */

        return $returnSummary;
    }

    public function OtherDashboard($officeid = 0, $unitid = 0, $designationid = 0)
    {
        set_time_limit(0);
        ini_set('memory_limit', '-1');

        TableRegistry::remove('DakMovements');
        $DakMovementsTable = TableRegistry::get('DakMovements');
        TableRegistry::remove('Potrojari');
        $potrojariTable    = TableRegistry::get('Potrojari');
        TableRegistry::remove('DakUsers');
        $DakUsersTable     = TableRegistry::get('DakUsers');
        $today             = date('Y-m-d');
        $yesterday         = date('Y-m-d', strtotime($today.' -1 day'));



        /*         * Inbox Total* */
        $totalprevinbox = $DakMovementsTable->countAllDakbyType($officeid, $unitid, $designationid,
            'Inbox', [ '', $yesterday]);

        /*         * Inbox Total* */

        /*         * Outbox Total* */
        $totalprevoutbox = $DakMovementsTable->countAllDakbyType($officeid, $unitid, $designationid,
            'Outbox', [ '', $yesterday]);

        /*         * Outbox Total* */

        /*         * Nothivukto * */
        $totalprevnothivukto = $DakMovementsTable->countAllDakbyType($officeid, $unitid,
            $designationid, 'NothiVukto', [ '', $yesterday]);

        /*         * Nothivukto * */

        /*         * Nothijato * */
        $totalprevnothijato = $DakMovementsTable->countAllDakbyType($officeid, $unitid,
            $designationid, 'NothiJato', [ '', $yesterday]);
        /*         * Nothijato * */

        /*         * Total Potrojari* */
        $totalprepotrojari = $potrojariTable->allPotrojariCount($officeid, $unitid, $designationid,
            [ '', $yesterday]);

        /*         * Potrojari* */

        /*         * Result* */
        $returnSummary['totalinboxall'] = $totalprevinbox;

        $returnSummary['totaloutboxall'] = $totalprevoutbox;

        $returnSummary['totalnothivuktoall'] = $totalprevnothivukto;

        $returnSummary['totalnothijatoall'] = $totalprevnothijato;

        $returnSummary['totalpotrojariall'] = $totalprepotrojari;

        $returnSummary['totalnisponnodakall']  = $returnSummary['totaloutboxall'] + $returnSummary['totalnothijatoall']
            + $returnSummary['totalnothivuktoall'] + $DakMovementsTable->countOnulipiDak($officeid,
                $unitid, $designationid);
        $returnSummary['totalOnisponnodakall'] = $DakUsersTable->getOnisponnoDak($officeid, $unitid,
            $designationid);

        /* --- Dak Part End --- */

        $temp = $this->nothiDashboardCount($officeid, $unitid, $designationid, [ '', $yesterday]);



        /*         * Self Given Note* */
        $returnSummary['totalsrijitonoteall'] = $temp['selfNote'];

        /*         * Self Given Note* */

        /*         * Dak Soho Note* */
        $returnSummary['totaldaksohonoteall'] = $temp['dakNote'];

        /*         * Dak Soho Note* */

        /*         * Nisponno by Note* */
        $returnSummary['totalnisponnonoteall'] = $temp['noteNisponno'];

        /*         * Nisponno by Note* */

        /*         * Nisponno by Potrojari* */
        $returnSummary['totalnisponnopotrojariall'] = $temp['potrojariNisponno'];

        /*         * Nisponno by Potrojari* */

        /*         * Onisponno Note* */
        $returnSummary['totalOnisponnonoteall'] = $temp['onisponno'];

        /*         * Onisponno Note* */


        return $returnSummary;
    }

    public function OfficeDashboardUpdateDak($officeid = 0,$date = '')
    {
        set_time_limit(0);
        ini_set('memory_limit', '-1');

        TableRegistry::remove('DakMovements');
        $DakMovementsTable = TableRegistry::get('DakMovements');
        TableRegistry::remove('Potrojari');
        $potrojariTable    = TableRegistry::get('Potrojari');
        TableRegistry::remove('DakUsers');
        $DakUsersTable     = TableRegistry::get('DakUsers');
        $today             = date('Y-m-d');
        $yesterday         = date('Y-m-d', strtotime($today.' -1 day'));


        /*         * Inbox Total* */
        $totalprevinbox = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0, 'Inbox',
            [ $date, $yesterday]);

        /*         * Inbox Total* */

        /*         * Outbox Total* */
        $totalprevoutbox = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0, 'Outbox',
            [ $date, $yesterday]);

        /*         * Outbox Total* */

        /*         * Nothivukto * */
        $totalprevnothivukto = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0,
            'NothiVukto', [ $date, $yesterday]);

        /*         * Nothivukto * */

        /*         * Nothijato * */
        $totalprevnothijato = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0, 'NothiJato',
            [ $date, $yesterday]);

        /*         * Nothijato * */

        /*         *Total Potrojari * */
        $totalprepotrojari = TableRegistry::get('NisponnoRecords')->allPotrojariCount($officeid, 0, 0, [ $date, $yesterday]);

        /*         *Total Potrojari* */

        /*         * Result* */

        $returnSummary['totalinboxall'] = $totalprevinbox;

        $returnSummary['totaloutboxall'] = $totalprevoutbox;

        $returnSummary['totalnothivuktoall'] = $totalprevnothivukto;

        $returnSummary['totalnothijatoall'] = $totalprevnothijato;

        $returnSummary['totalpotrojariall'] = $totalprepotrojari;

        /*         * Nisponno Dak* */

        $returnSummary['totalnisponnodakall'] = $returnSummary['totalnothijatoall'] + $returnSummary['totalnothivuktoall'];

        /*         * Nisponno Dak* */

        /*         * Onisponno Dak* */
        $returnSummary['totalOnisponnodakall'] = $DakUsersTable->getOnisponnoDak($officeid, 0, 0,
            $yesterday);

        /*         * Onisponno Dak* */



        /* --- Dak Part End --- */



        return $returnSummary;
    }

    public function OfficeDashboardUpdateNothi($officeid = 0,$date = '')
    {
        set_time_limit(0);
        ini_set('memory_limit', '-1');

        $today     = date('Y-m-d');
        $yesterday = date('Y-m-d', strtotime($today.' -1 day'));

        $temp = $this->nothiDashboardCount($officeid, 0, 0, [ $date, $yesterday]);


        /*         * Self Given Note* */
        $returnSummary['totalsrijitonoteall'] = $temp['selfNote'];

        /*         * Self Given Note* */

        /*         * Dak Soho Note* */
        $returnSummary['totaldaksohonoteall'] = $temp['dakNote'];

        /*         * Dak Soho Note* */

        /*         * Nisponno by Note* */
        $returnSummary['totalnisponnonoteall'] = $temp['noteNisponno'];

        /*         * Nisponno by Note* */

        /*         * Nisponno by Potrojari* */
        $returnSummary['totalnisponnopotrojariall'] = $temp['potrojariNisponno'];

        /*         * Nisponno by Potrojari* */

        /*         * Onisponno Note* */
        $returnSummary['totalOnisponnonoteall'] = $temp['onisponno'];

        /*         * Onisponno Note* */

        return $returnSummary;
    }

    public function officeSummary($officeid = 0)
    {
        set_time_limit(0);
        ini_set('memory_limit', '-1');
//        $dataSourceObject = ConnectionManager::get('default');
//       echo  $dataSourceObject->config()['database'];

        $DashboardOfficesTable = TableRegistry::get('DashboardOffices');
        $performanceOfficesTable = TableRegistry::get('PerformanceOffices');
        TableRegistry::remove('DakUsers');
        TableRegistry::remove('DakMovements');
        TableRegistry::remove('Potrojari');
        $DakUsersTable = TableRegistry::get('DakUsers');

        $result = $DashboardOfficesTable->getData($officeid);
//        print_r($result);die;

        $DakMovementsTable        = TableRegistry::get('DakMovements');
        $potrojariTable           = TableRegistry::get('Potrojari');
        $today                    = date('Y-m-d');
        $yesterday                = date('Y-m-d', strtotime($today.' -1 day'));

        $condition = [
                                'totalInbox' => 'SUM(inbox)', 'totalOutbox' => 'SUM(outbox)', 'totalNothijato' => 'SUM(nothijat)',
                                'totalNothivukto' => 'SUM(nothivukto)', 'totalNisponnoDak' => 'SUM(nisponnodak)',
                                'totalSouddog' => 'SUM(selfnote)','OnisponnoDak'=>'SUM(onisponnodak)',
                                'totalDaksohoNote' => 'SUM(daksohonote)', 'totalNisponnoPotrojari' => 'SUM(nisponnopotrojari)',
                                'totalNisponnoNote' => 'SUM(nisponnonote)','potrojari' => 'SUM(potrojari)',
                                'totalNisponno' => 'SUM(nisponno)', 'totalONisponno' => 'SUM(onisponno)','yesterdayOnisponnoNote'=>'SUM(onisponnonote)',
                                'totalID' => 'count(id)'
                            ];
        $result_yesterday = $performanceOfficesTable->getOfficeData($officeid,[$yesterday, $yesterday])->select($condition)->group(['office_id'])->first();
        if(!empty($result_yesterday)){
            $result['totalyesterdayinbox'] = $result_yesterday['totalInbox'];
            $result['totalyesterdaynothivukto'] = $result_yesterday['totalNothivukto'];
            $result['totalyesterdaynothijato'] = $result_yesterday['totalNothijato'];
            $result['totalyesterdaypotrojari'] = $result_yesterday['potrojari'];
            $result['totalyesterdaynisponnodak'] = $result_yesterday['totalNisponnoDak'];
            $result['totalyesterdayOnisponnodak'] = $result_yesterday['OnisponnoDak'];
            $result['totalyesterdaydaksohonote'] = $result_yesterday['totalDaksohoNote'];
            $result['totalyesterdaysrijitonote'] = $result_yesterday['totalSouddog'];
            $result['totalyesterdaynisponnonote'] = $result_yesterday['totalNisponnoNote'];
            $result['totalyesterdaynisponnopotrojari'] = $result_yesterday['totalNisponnoPotrojari'];
            $result['totalyesterdayOnisponnonote'] = $result_yesterday['yesterdayOnisponnoNote'];
        }

        /* Total Login */
//        $employee_records = $employee_offices_table->getAllEmployeeRecordID($officeid);
//        $TotalLogin       = $user_login_history_table->countLogin($employee_records);
//        $yesterdayLogin   = $user_login_history_table->countLoginYesterday($employee_records);
//        $employee_records =0;
        $TotalLogin       =0;
        $yesterdayLogin   = 0;
        /* Total Login */

        /*         * Inbox Total* */
        $totaltodayinbox = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0, 'Inbox',
            [$today, $today]);

        if (isset($result['totalyesterdayinbox'])) {
            $totalyesterdayinbox = $result['totalyesterdayinbox'];
        }else{
             $totalyesterdayinbox = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0, 'Inbox',
            [$yesterday, $yesterday]);
        }
       

        if ($result['totalID'] > 0) {
            $totalprevinbox = $result['totalInbox'] + $totaltodayinbox;
        }
        else {
            $totalprevinbox = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0, 'Inbox');
        }

        /*         * Inbox Total* */

        /*         * Outbox Total* */
        $totaltodayoutbox     = 0;
        $totalyesterdayoutbox = 0;
        $totalprevoutbox      = 0;

        /*         * Outbox Total* */

        /*         * Nothivukto * */
        $totaltodaynothivukto = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0,
            'NothiVukto', [$today, $today]);

       if (isset($result['totalyesterdaynothivukto'])) {
            $totalyesterdaynothivukto = $result['totalyesterdaynothivukto'];
        }else{
             $totalyesterdaynothivukto = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0,
            'NothiVukto', [$yesterday, $yesterday]);
        }

        if ($result['totalID'] > 0) {
            $totalprevnothivukto = $result['totalNothivukto'] + $totaltodaynothivukto;
        }
        else {
            $totalprevnothivukto = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0,
                'NothiVukto');
        }

        /*         * Nothivukto * */

        /*         * Nothijato * */
        $totaltodaynothijato     = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0,
            'NothiJato', [$today, $today]);
         if (isset($result['totalyesterdaynothijato'])) {
            $totalyesterdaynothijato = $result['totalyesterdaynothijato'];
        }else{
             $totalyesterdaynothijato = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0,
            'NothiJato', [$yesterday, $yesterday]);
        }
       
        if ($result['totalID'] > 0) {
            $totalprevnothijato = $result['totalNothijato'] + $totaltodaynothijato;
        }
        else {
            $totalprevnothijato = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0,
                'NothiJato');
        }

        /*         * Nothijato * */

        /*         * Total Potrojari* */
        $totaltodaypotrojari     = $potrojariTable->allPotrojariCount($officeid, 0, 0,
            [$today, $today]);
         if (isset($result['totalyesterdaypotrojari'])) {
            $totalyesterdaypotrojari = $result['totalyesterdaypotrojari'];
        }else{
             $totalyesterdaypotrojari = $potrojariTable->allPotrojariCount($officeid, 0, 0,
            [$yesterday, $yesterday]);
        }
        
        if ($result['totalID'] > 0) {
            $totalprepotrojari = $result['totalPotrojari'] + $totaltodaypotrojari;
        }
        else {
            $totalprepotrojari = $potrojariTable->allPotrojariCount($officeid, 0, 0);
        }

        /*         * Total Potrojari* */

        /** Nothi Note Part */
        $temp_today = $this->nothiDashboardCount($officeid, 0, 0, [ $today, $today]);
//        print_r($temp_today);die;

        $temp_yesterday = $this->nothiDashboardCount($officeid, 0, 0, [ $yesterday, $yesterday]);

        $returnSummary['ajax_request'] = false;
        if ($result['totalID'] <= 0) {
            $returnSummary['ajax_request'] = true;
        }


        /*         * Result* */
        //User Count
        $returnSummary['totallogin']     = $TotalLogin;
        $returnSummary['yesterdayLogin'] = $yesterdayLogin;
        // Inbox Count
        $returnSummary['totaltodayinbox']     = $totaltodayinbox;
        $returnSummary['totalyesterdayinbox'] = $totalyesterdayinbox;
        $returnSummary['totalinboxall']       = $totalprevinbox;
          // Outbox Count
        $returnSummary['totaltodayoutbox']     = $totaltodayoutbox;
        $returnSummary['totalyesterdayoutbox'] = $totalyesterdayoutbox;
        $returnSummary['totaloutboxall']       = $totalprevoutbox;
           // Nothivukto Count
        $returnSummary['totaltodaynothivukto']     = $totaltodaynothivukto;
        $returnSummary['totalyesterdaynothivukto'] = $totalyesterdaynothivukto;
        $returnSummary['totalnothivuktoall']       = $totalprevnothivukto;
           // Nothijato Count
        $returnSummary['totaltodaynothijato']     = $totaltodaynothijato;
        $returnSummary['totalyesterdaynothijato'] = $totalyesterdaynothijato;
        $returnSummary['totalnothijatoall']       = $totalprevnothijato;
           //  Nisponno Dak Count
        $returnSummary['totaltodaynisponnodak']     = $returnSummary['totaltodaynothijato'] + $returnSummary['totaltodaynothivukto'];
        $returnSummary['totalyesterdaynisponnodak'] = $returnSummary['totalyesterdaynothijato'] + $returnSummary['totalyesterdaynothivukto'];
        if ($result['totalID'] > 0) {
            $returnSummary['totalnisponnodakall'] = $result['totalNisponnoDak'] + $returnSummary['totaltodaynisponnodak'];
        }
        else {
            $returnSummary['totalnisponnodakall'] = $returnSummary['totalnothijatoall'] + $returnSummary['totalnothivuktoall'];
        }
           // Onisponno Dak Count
        $returnSummary['totaltodayOnisponnodak'] = $DakUsersTable->getOnisponnoDak($officeid, 0, 0,
            $today);

//        $returnSummary['totalyesterdayOnisponnodak'] = $DakUsersTable->getOnisponnoDak($officeid, 0,
//            0, $yesterday);
        if ($result['totalID'] > 0) {
             $returnSummary['totalyesterdayOnisponnodak'] = $result['totalyesterdayOnisponnodak'];
        }else{
            $returnSummary['totalyesterdayOnisponnodak'] = $DakUsersTable->getOnisponnoDak($officeid, 0,
            0, $yesterday);
        }
//         $returnSummary['totalyesterdayOnisponnodak'] = $returnSummary['totaltodayOnisponnodak'] - ($returnSummary['totaltodayinbox']
//            - $returnSummary['totaltodaynisponnodak']);
//        if ($result['totalID'] > 0) {
//            $returnSummary['totalOnisponnodakall'] = $result['totalOnisponnodakall'] + ($returnSummary['totaltodayinbox']
//                - $returnSummary['totaltodaynisponnodak']);
            $returnSummary['totalOnisponnodakall'] =$returnSummary['totaltodayOnisponnodak'];
//        }
//        else {
//            $returnSummary['totalOnisponnodakall'] = TableRegistry::get('DakUsers')->getOnisponnoDak($officeid,
//                0, 0);
//        }
            // potrojari Count
        $returnSummary['totaltodaypotrojari']     = $totaltodaypotrojari;
        $returnSummary['totalyesterdaypotrojari'] = $totalyesterdaypotrojari;
        $returnSummary['totalpotrojariall']       = $totalprepotrojari;


      // Dak Soho Note Can not be greater than Nothivukto Dak
        $returnSummary['totaltodaydaksohonote']     =  $temp_today['dakNote'];
        if (isset($result['totalyesterdaydaksohonote'])) {
            $returnSummary['totalyesterdaydaksohonote'] = $result['totalyesterdaydaksohonote'];
        }else{
             $returnSummary['totalyesterdaydaksohonote'] = $temp_yesterday['dakNote'];
        }
       
        if ($result['totalID'] > 0) {
            $returnSummary['totaldaksohonoteall'] = $result['totalDaksohoNote'] + $returnSummary['totaltodaydaksohonote'];
            $returnSummary['totaldaksohonoteall'] = ($totalprevnothivukto < $returnSummary['totaldaksohonoteall'] ? $totalprevnothivukto : $returnSummary['totaldaksohonoteall']);
        }
          //Potrojari Nisponno Note can not be greater than Potrojari
         $returnSummary['totaltodaynisponnopotrojari']     =($totaltodaypotrojari < $temp_today['potrojariNisponno'] ?$totaltodaypotrojari :  $temp_today['potrojariNisponno']);
        if (isset($result['totalyesterdaynisponnopotrojari'])) {
            $returnSummary['totalyesterdaynisponnopotrojari'] = $result['totalyesterdaynisponnopotrojari'];
        }else{
              $returnSummary['totalyesterdaynisponnopotrojari'] = /*$temp_yesterday['potrojariNisponno'];*/($totalyesterdaypotrojari < $temp_yesterday['potrojariNisponno'] ?$totalyesterdaypotrojari : $temp_yesterday['potrojariNisponno']);
        }
      
        if ($result['totalID'] > 0) {
            $returnSummary['totalnisponnopotrojariall'] = $result['totalNisponnoPotrojari'] + $returnSummary['totaltodaynisponnopotrojari'];
            $returnSummary['totalnisponnopotrojariall'] = ($totalprepotrojari <  $returnSummary['totalnisponnopotrojariall'] ? $totalprepotrojari : $returnSummary['totalnisponnopotrojariall']);
        }
        // Nisponno By Note Count
        $returnSummary['totaltodaynisponnonote']     = $temp_today['noteNisponno'];
          if (isset($result['totalyesterdaynisponnonote'])) {
            $returnSummary['totalyesterdaynisponnonote'] = $result['totalyesterdaynisponnonote'];
        }else{
             $returnSummary['totalyesterdaynisponnonote'] = $temp_yesterday['noteNisponno'];
        }
        if ($result['totalID'] > 0) {
            $returnSummary['totalnisponnonoteall'] = $result['totalNisponnoNote'] + $returnSummary['totaltodaynisponnonote'];
        }


        // Onisponno Note Count
        $returnSummary['totaltodayOnisponnonote']     = $this->getOnisponnoNoteCount($officeid,
            0, 0, [$today, $today]);
         if (isset($result['totalyesterdayOnisponnonote'])) {
            $returnSummary['totalyesterdayOnisponnonote'] = $result['totalyesterdayOnisponnonote'];
        }else{
              $returnSummary['totalyesterdayOnisponnonote'] = $this->getOnisponnoNoteCount($officeid,
            0, 0, [$yesterday, $yesterday]);
        }
       
//        if ($result['totalID'] > 0) {
//            print_r($result['totalONisponno']);            print_r($temp_today['selfNote'] + $temp_today['dakNote']);die;
//            $returnSummary['totalOnisponnonoteall']   = $result['totalONisponno'] + $temp_today['selfNote']
//                + $temp_today['dakNote'] - $temp_today['noteNisponno'] - $temp_today['potrojariNisponno'];
             $returnSummary['totalOnisponnonoteall'] =$returnSummary['totaltodayOnisponnonote'] ;
//        }else{
//            $returnSummary['totaltodayOnisponnonote']     = $this->getOnisponnoNoteCount($officeid,
//            0, 0, [$today, $today]);
//        }

//        $returnSummary['totalyesterdayOnisponnodak'] = $returnSummary['totaltodayOnisponnodak'] - ($returnSummary['totaltodayinbox']
//            - $returnSummary['totaltodaynisponnodak']);

        /*         * Self Given Note* */
        $returnSummary['totaltodaysrijitonote']     = $temp_today['selfNote'] ;
         if (isset($result['totalyesterdaysrijitonote'])) {
             $returnSummary['totalyesterdaysrijitonote'] = $result['totalyesterdaysrijitonote'];
        }else{
             $returnSummary['totalyesterdaysrijitonote'] = $temp_yesterday['selfNote'];
        }
       
        if($returnSummary['totaltodaysrijitonote'] < 0){
            $returnSummary['totaltodaysrijitonote'] = 0;
        }
        if($returnSummary['totalyesterdaysrijitonote'] < 0){
            $returnSummary['totalyesterdaysrijitonote'] = 0;
        }
      

        /*         * Self Given Note* */

       
//        $returnSummary['totalyesterdayOnisponnonote'] = $returnSummary['totaltodayOnisponnonote'] - ( $returnSummary['totaltodaydaksohonote']
//            + $returnSummary['totaltodaysrijitonote'] - $returnSummary['totaltodaynisponnonote'] - $returnSummary['totaltodaynisponnopotrojari']);
        if($returnSummary['totalOnisponnodakall'] < 0){
            $returnSummary['totalOnisponnodakall'] = 0;
        }
          if ($result['totalID'] > 0) {
            $returnSummary['totalsrijitonoteall'] =  $returnSummary['totalnisponnonoteall'] +  $returnSummary['totalnisponnopotrojariall'] +  $returnSummary['totalOnisponnonoteall'] -  ($totalprevnothivukto < $returnSummary['totaldaksohonoteall'] ?$totalprevnothivukto :  $returnSummary['totaldaksohonoteall']);
            if($returnSummary['totalsrijitonoteall'] < 0){
                $returnSummary['totalsrijitonoteall'] = 0;
        }
        }

        $returnSummary['totalinboxall']  = $returnSummary['totalOnisponnodakall'] + $returnSummary['totalnisponnodakall'];
//        $returnSummary['totaloutboxall'] = $returnSummary['totalinboxall'];

        if ($returnSummary['totalyesterdayOnisponnodak'] < 0) {
            $returnSummary['totalyesterdayOnisponnodak'] = 0;
        }
        if ($returnSummary['totalyesterdayOnisponnonote'] < 0) {
            $returnSummary['totalyesterdayOnisponnonote'] = 0;
        }

        return $returnSummary;
    }

    public function unitSummary($officeid = 0, $unit_id = 0)
    {
        set_time_limit(0);
        ini_set('memory_limit', '-1');

          TableRegistry::remove('DakUsers');
          TableRegistry::remove('DakMovements');
          TableRegistry::remove('Potrojari');
        $DashboardUnitsTable = TableRegistry::get('DashboardUnits');

        $result = $DashboardUnitsTable->getData($unit_id);
        $performanceUnitsTable = TableRegistry::get('PerformanceUnits');
        $DakMovementsTable        = TableRegistry::get('DakMovements');
        $potrojariTable           = TableRegistry::get('Potrojari');
        $today                    = date('Y-m-d');
        $yesterday                = date('Y-m-d', strtotime($today.' -1 day'));
        $condition = [
                                'totalInbox' => 'SUM(inbox)', 'totalOutbox' => 'SUM(outbox)', 'totalNothijato' => 'SUM(nothijat)',
                                'totalNothivukto' => 'SUM(nothivukto)', 'totalNisponnoDak' => 'SUM(nisponnodak)',
                                'totalSouddog' => 'SUM(selfnote)','OnisponnoDak'=>'SUM(onisponnodak)',
                                'totalDaksohoNote' => 'SUM(daksohonote)', 'totalNisponnoPotrojari' => 'SUM(nisponnopotrojari)',
                                'totalNisponnoNote' => 'SUM(nisponnonote)','potrojari' => 'SUM(potrojari)',
                                'totalNisponno' => 'SUM(nisponno)', 'totalONisponno' => 'SUM(onisponno)','yesterdayOnisponnoNote'=>'SUM(onisponnonote)',
                                'totalID' => 'count(id)'
                            ];
        $result_yesterday = $performanceUnitsTable->getUnitData($unit_id,[$yesterday, $yesterday])->select($condition)->group(['unit_id'])->first();
        if(!empty($result_yesterday)){
            $result['totalyesterdayinbox'] = $result_yesterday['totalInbox'];
            $result['totalyesterdaynothivukto'] = $result_yesterday['totalNothivukto'];
            $result['totalyesterdaynothijato'] = $result_yesterday['totalNothijato'];
            $result['totalyesterdaypotrojari'] = $result_yesterday['potrojari'];
            $result['totalyesterdaynisponnodak'] = $result_yesterday['totalNisponnoDak'];
            $result['totalyesterdayOnisponnodak'] = $result_yesterday['OnisponnoDak'];
            $result['totalyesterdaydaksohonote'] = $result_yesterday['totalDaksohoNote'];
            $result['totalyesterdaysrijitonote'] = $result_yesterday['totalSouddog'];
            $result['totalyesterdaynisponnonote'] = $result_yesterday['totalNisponnoNote'];
            $result['totalyesterdaynisponnopotrojari'] = $result_yesterday['totalNisponnoPotrojari'];
            $result['totalyesterdayOnisponnonote'] = $result_yesterday['yesterdayOnisponnoNote'];
        }
//        print_r($result);die;

        

        /* Total Login */
//        $employee_records = $employee_offices_table->getAllEmployeeRecordID($officeid, $unit_id);
//        $TotalLogin       = $user_login_history_table->countLogin($employee_records);
//        $yesterdayLogin   = $user_login_history_table->countLoginYesterday($employee_records);
        $TotalLogin       = 0;
        $yesterdayLogin   = 0;
        /* Total Login */

        /*         * Inbox Total* */
        $totaltodayinbox = $DakMovementsTable->countAllDakbyType($officeid, $unit_id, 0, 'Inbox',
            [$today, $today]);

        $totalyesterdayinbox = $DakMovementsTable->countAllDakbyType($officeid, $unit_id, 0,
            'Inbox', [$yesterday, $yesterday]);

        if ($result['totalID'] > 0) {
            $totalprevinbox = $result['totalInbox'] + $totaltodayinbox;
        }
        else {
            $totalprevinbox = $DakMovementsTable->countAllDakbyType($officeid, $unit_id, 0, 'Inbox');
        }

        /*         * Inbox Total* */

        /*         * Outbox Total* */
        $totaltodayoutbox     = $DakMovementsTable->countAllDakbyType($officeid, $unit_id, 0,
            'Outbox', [$today, $today]);
        $totalyesterdayoutbox = $DakMovementsTable->countAllDakbyType($officeid, $unit_id, 0,
            'Outbox', [$yesterday, $yesterday]);
        if ($result['totalID'] > 0) {
            $totalprevoutbox = $result['totalOutbox'] + $totaltodayoutbox;
        }
        else {
            $totalprevoutbox = $DakMovementsTable->countAllDakbyType($officeid, $unit_id, 0,
                'Outbox');
        }


        /*         * Outbox Total* */

        /*         * Nothivukto * */
        $totaltodaynothivukto = $DakMovementsTable->countAllDakbyType($officeid, $unit_id, 0,
            'NothiVukto', [$today, $today]);

        $totalyesterdaynothivukto = $DakMovementsTable->countAllDakbyType($officeid, $unit_id, 0,
            'NothiVukto', [$yesterday, $yesterday]);

        if ($result['totalID'] > 0) {
            $totalprevnothivukto = $result['totalNothivukto'] + $totaltodaynothivukto;
        }
        else {
            $totalprevnothivukto = $DakMovementsTable->countAllDakbyType($officeid, $unit_id, 0,
                'NothiVukto');
        }

        /*         * Nothivukto * */

        /*         * Nothijato * */
        $totaltodaynothijato     = $DakMovementsTable->countAllDakbyType($officeid, $unit_id, 0,
            'NothiJato', [$today, $today]);
        $totalyesterdaynothijato = $DakMovementsTable->countAllDakbyType($officeid, $unit_id, 0,
            'NothiJato', [$yesterday, $yesterday]);
        if ($result['totalID'] > 0) {
            $totalprevnothijato = $result['totalNothijato'] + $totaltodaynothijato;
        }
        else {
            $totalprevnothijato = $DakMovementsTable->countAllDakbyType($officeid, $unit_id, 0,
                'NothiJato');
        }

        /*         * Nothijato * */

        /*         * Total Potrojari* */
        $totaltodaypotrojari     = $potrojariTable->allPotrojariCount($officeid, $unit_id, 0,
            [$today, $today]);
        $totalyesterdaypotrojari = $potrojariTable->allPotrojariCount($officeid, $unit_id, 0,
            [$yesterday, $yesterday]);
        if ($result['totalID'] > 0) {
            $totalprepotrojari = $result['totalPotrojari'] + $totaltodaypotrojari;
        }
        else {
            $totalprepotrojari = $potrojariTable->allPotrojariCount($officeid, $unit_id, 0);
        }

        /*         * Total Potrojari* */

        /** Nothi Note Part */
        $temp_today = $this->nothiDashboardCount($officeid, $unit_id, 0, [ $today, $today]);
//        print_r($temp_today);die;

        $temp_yesterday = $this->nothiDashboardCount($officeid, $unit_id, 0,
            [ $yesterday, $yesterday]);

        $returnSummary['ajax_request'] = false;
        if ($result['totalID'] <= 0) {
            $returnSummary['ajax_request'] = true;
        }


        /*         * Result* */

        $returnSummary['totallogin']     = $TotalLogin;
        $returnSummary['yesterdayLogin'] = $yesterdayLogin;
        // Inbox
        $returnSummary['totaltodayinbox']     = $totaltodayinbox;
        $returnSummary['totalyesterdayinbox'] = $totalyesterdayinbox;
        $returnSummary['totalinboxall']       = $totalprevinbox;
        // Outbox
        $returnSummary['totaltodayoutbox']     = $totaltodayoutbox;
        $returnSummary['totalyesterdayoutbox'] = $totalyesterdayoutbox;
        $returnSummary['totaloutboxall']       = $totalprevoutbox;
        // Nothivukto
        $returnSummary['totaltodaynothivukto']     = $totaltodaynothivukto;
        $returnSummary['totalyesterdaynothivukto'] = $totalyesterdaynothivukto;
        $returnSummary['totalnothivuktoall']       = $totalprevnothivukto;
        // Nothijato
        $returnSummary['totaltodaynothijato']     = $totaltodaynothijato;
        $returnSummary['totalyesterdaynothijato'] = $totalyesterdaynothijato;
        $returnSummary['totalnothijatoall']       = $totalprevnothijato;

        /*         * Nisponno Dak* */

        $returnSummary['totaltodaynisponnodak']     = $returnSummary['totaltodayoutbox'] + $returnSummary['totaltodaynothijato']
            + $returnSummary['totaltodaynothivukto'] + $DakMovementsTable->countOnulipiDak($officeid,
                $unit_id, 0, [ $today, $today]);
        $returnSummary['totalyesterdaynisponnodak'] = $returnSummary['totalyesterdayoutbox'] + $returnSummary['totalyesterdaynothijato']
            + $returnSummary['totalyesterdaynothivukto'] + $DakMovementsTable->countOnulipiDak($officeid,
                $unit_id, 0, [ $yesterday, $yesterday]);
        if ($result['totalID'] > 0) {
            $returnSummary['totalnisponnodakall'] = $result['totalNisponnoDak'] + $returnSummary['totaltodaynisponnodak'];
        }
        else {
            $returnSummary['totalnisponnodakall'] = $returnSummary['totaloutboxall'] + $returnSummary['totalnothijatoall']
                + $returnSummary['totalnothivuktoall'] + $DakMovementsTable->countOnulipiDak($officeid,
                    $unit_id, 0);
        }

        /*         * Nisponno Dak* */
        // Onisponno Dak
        $returnSummary['totaltodayOnisponnodak'] = TableRegistry::get('DakUsers')->getOnisponnoDak($officeid,
            $unit_id, 0, $today);

          if ($result['totalID'] > 0) {
            $returnSummary['totalyesterdayOnisponnodak'] = $result['totalyesterdayOnisponnodak'];
        }else{
              $returnSummary['totalyesterdayOnisponnodak'] = TableRegistry::get('DakUsers')->getOnisponnoDak($officeid,
            $unit_id, 0, $yesterday);
        }
//       $returnSummary['totalyesterdayOnisponnodak']  = $returnSummary['totaltodayOnisponnodak'] - ($returnSummary['totaltodayinbox']
//            - $returnSummary['totaltodaynisponnodak']);
//        if ($result['totalID'] > 0) {
//            $returnSummary['totalOnisponnodakall'] = $result['totalOnisponnodakall'];
            $returnSummary['totalOnisponnodakall'] = $returnSummary['totaltodayOnisponnodak'] ;
//        }
//        else {
//            $returnSummary['totalOnisponnodakall'] = TableRegistry::get('DakUsers')->getOnisponnoDak($officeid,
//                $unit_id, 0);
//        }
            //Potrojari
        $returnSummary['totaltodaypotrojari']     = $totaltodaypotrojari;
        $returnSummary['totalyesterdaypotrojari'] = $totalyesterdaypotrojari;
        $returnSummary['totalpotrojariall']       = $totalprepotrojari;


        // Dak Soho note can not be greater than Nothivukto Dak
        $returnSummary['totaltodaydaksohonote']     = $temp_today['dakNote'];
        $returnSummary['totalyesterdaydaksohonote'] = $temp_yesterday['dakNote'];
        if ($result['totalID'] > 0) {
            $returnSummary['totaldaksohonoteall'] = $result['totalDaksohoNote'] + $returnSummary['totaltodaydaksohonote'];
            $returnSummary['totaldaksohonoteall'] = $returnSummary['totaldaksohonoteall'];
        }

        //Nisponno Note
        $returnSummary['totaltodaynisponnonote']     = $temp_today['noteNisponno'];
        $returnSummary['totalyesterdaynisponnonote'] = $temp_yesterday['noteNisponno'];
        if ($result['totalID'] > 0) {
            $returnSummary['totalnisponnonoteall'] = $result['totalNisponnoNote'] + $returnSummary['totaltodaynisponnonote'];
        }
        // Nisponno Potrojari can not be greater than potrojari
        $returnSummary['totaltodaynisponnopotrojari']     =($totaltodaypotrojari < $temp_today['potrojariNisponno'] ?$totaltodaypotrojari :  $temp_today['potrojariNisponno']);
        $returnSummary['totalyesterdaynisponnopotrojari'] =/*$temp_yesterday['potrojariNisponno'];*/($totalyesterdaypotrojari < $temp_yesterday['potrojariNisponno'] ?$totalyesterdaypotrojari :  $temp_yesterday['potrojariNisponno']);
        if ($result['totalID'] > 0) {
            $returnSummary['totalnisponnopotrojariall'] = $result['totalNisponnoPotrojari'] + $returnSummary['totaltodaynisponnopotrojari'];
            $returnSummary['totalnisponnopotrojariall'] = ($totalprepotrojari < $returnSummary['totalnisponnopotrojariall']  ?$totalprepotrojari :  $returnSummary['totalnisponnopotrojariall'] );
        }


        $returnSummary['totaltodayOnisponnonote']     = $this->getOnisponnoNoteCount($officeid,
            $unit_id, 0, [$today, $today]);

         if ($result['totalID'] > 0) {
            $returnSummary['totalyesterdayOnisponnonote'] = $result['totalyesterdayOnisponnonote'];
        }else{
              $returnSummary['totalyesterdayOnisponnonote'] = $this->getOnisponnoNoteCount($officeid,
            $unit_id, 0, [$yesterday, $yesterday]);
        }
//        if ($result['totalID'] > 0) {
//            print_r($result['totalONisponno']);            print_r($temp_today['selfNote'] + $temp_today['dakNote']);die;
//            $returnSummary['totalOnisponnonoteall']   = $result['totalONisponno'] + $temp_today['selfNote']
//                + $temp_today['dakNote'] - $temp_today['noteNisponno'] - $temp_today['potrojariNisponno'];
             $returnSummary['totalOnisponnonoteall'] = $returnSummary['totaltodayOnisponnonote'];
//        }else{
//            $returnSummary['totaltodayOnisponnonote']     = $this->getOnisponnoNoteCount($officeid,
//            $unit_id, 0, [$today, $today]);
//        }
                                     /*         * Self Given Note* */
        $returnSummary['totaltodaysrijitonote']     = $temp_today['selfNote'] ;
        $returnSummary['totalyesterdaysrijitonote'] = $temp_yesterday['selfNote'];
                    if($returnSummary['totaltodaysrijitonote'] < 0){
                $returnSummary['totaltodaysrijitonote'] = 0;
            }
                        if($returnSummary['totalyesterdaysrijitonote'] < 0){
                $returnSummary['totalyesterdaysrijitonote'] = 0;
            }
   
        /*         * Self Given Note* */
        
//        $returnSummary['totalyesterdayOnisponnonote'] = $returnSummary['totaltodayOnisponnonote'] - ( $returnSummary['totaltodaydaksohonote']
//            + $returnSummary['totaltodaysrijitonote'] - $returnSummary['totaltodaynisponnonote'] - $returnSummary['totaltodaynisponnopotrojari']);

        $returnSummary['totalinboxall'] = $returnSummary['totalOnisponnodakall'] + $returnSummary['totalnisponnodakall'];

//         $returnSummary['totaltodayOnisponno']     = $returnSummary['totaltodayOnisponnodak'] + $returnSummary['totaltodayOnisponnonote'];
//        $returnSummary['totalyesterdayOnisponno'] = $returnSummary['totalyesterdayOnisponnodak'] + $returnSummary['totalyesterdayOnisponnonote'];
//        $returnSummary['totalOnisponnoall']       = $returnSummary['totalOnisponnodakall'] + $returnSummary['totalOnisponnonoteall'];
             if ($result['totalID'] > 0) {
            $returnSummary['totalsrijitonoteall'] =  $returnSummary['totalnisponnonoteall'] +  $returnSummary['totalnisponnopotrojariall'] +  $returnSummary['totalOnisponnonoteall'] -  ($totalprevnothivukto < $returnSummary['totaldaksohonoteall'] ?$totalprevnothivukto :  $returnSummary['totaldaksohonoteall']);
            if( $returnSummary['totalsrijitonoteall'] < 0){
                $returnSummary['totalsrijitonoteall'] = 0;
            }
        }
          if($returnSummary['totalOnisponnodakall'] < 0){
            $returnSummary['totalOnisponnodakall'] = 0;
        }


        if ($returnSummary['totaltodayinbox'] < $returnSummary['totaltodayoutbox']) {
            $returnSummary['totaltodayoutbox'] = $returnSummary['totaltodayinbox'];
        }
        if ($returnSummary['totalyesterdayinbox'] < $returnSummary['totalyesterdayoutbox']) {
            $returnSummary['totalyesterdayoutbox'] = $returnSummary['totalyesterdayinbox'];
        }
        if ($returnSummary['totalinboxall'] < $returnSummary['totaloutboxall']) {
            $returnSummary['totaloutboxall'] = $returnSummary['totalinboxall'];
        }
        if ($returnSummary['totalyesterdayOnisponnodak'] < 0) {
            $returnSummary['totalyesterdayOnisponnodak'] = 0;
        }
        if ($returnSummary['totalyesterdayOnisponnonote'] < 0) {
            $returnSummary['totalyesterdayOnisponnonote'] = 0;
        }

        return $returnSummary;
    }

    public function designationSummary($officeid = 0, $unit_id = 0, $designation_id = 0)
    {
        set_time_limit(0);
        ini_set('memory_limit', '-1');

        TableRegistry::remove('DashboardDesignations');
          TableRegistry::remove('DakUsers');
          TableRegistry::remove('Potrojari');
        $DashboardDesignationsTable = TableRegistry::get('DashboardDesignations');

        $result = $DashboardDesignationsTable->getData($designation_id);

        $DakMovementsTable        = TableRegistry::get('DakMovements');
        $potrojariTable           = TableRegistry::get('Potrojari');
        $today                    = date('Y-m-d');
        $yesterday                = date('Y-m-d', strtotime($today.' -1 day'));

        /* Total Login */
//        $employee_records = $employee_offices_table->getAllEmployeeRecordID($officeid, $unit_id,
//            $designation_id);
//        $TotalLogin       = $user_login_history_table->countLogin($employee_records);
//        $yesterdayLogin   = $user_login_history_table->countLoginYesterday($employee_records);
        $TotalLogin       = 0;
        $yesterdayLogin   = 0;
        /* Total Login */

        /*         * Inbox Total* */
        $totaltodayinbox = $DakMovementsTable->countAllDakbyType($officeid, $unit_id,
            $designation_id, 'Inbox', [$today, $today]);

        $totalyesterdayinbox = $DakMovementsTable->countAllDakbyType($officeid, $unit_id,
            $designation_id, 'Inbox', [$yesterday, $yesterday]);

        if ($result['totalID'] > 0) {
            $totalprevinbox = $result['totalInbox'] + $totaltodayinbox;
        }
        else {
            $totalprevinbox = $DakMovementsTable->countAllDakbyType($officeid, $unit_id,
                $designation_id, 'Inbox');
        }

        /*         * Inbox Total* */

        /*         * Outbox Total* */
        $totaltodayoutbox     = $DakMovementsTable->countAllDakbyType($officeid, $unit_id,
            $designation_id, 'Outbox', [$today, $today]);
        $totalyesterdayoutbox = $DakMovementsTable->countAllDakbyType($officeid, $unit_id,
            $designation_id, 'Outbox', [$yesterday, $yesterday]);
        if ($result['totalID'] > 0) {
            $totalprevoutbox = $result['totalOutbox'] + $totaltodayoutbox;
        }
        else {
            $totalprevoutbox = $DakMovementsTable->countAllDakbyType($officeid, $unit_id,
                $designation_id, 'Outbox');
        }


        /*         * Outbox Total* */

        /*         * Nothivukto * */
        $totaltodaynothivukto = $DakMovementsTable->countAllDakbyType($officeid, $unit_id,
            $designation_id, 'NothiVukto', [$today, $today]);

        $totalyesterdaynothivukto = $DakMovementsTable->countAllDakbyType($officeid, $unit_id,
            $designation_id, 'NothiVukto', [$yesterday, $yesterday]);

        if ($result['totalID'] > 0) {
            $totalprevnothivukto = $result['totalNothivukto'] + $totaltodaynothivukto;
        }
        else {
            $totalprevnothivukto = $DakMovementsTable->countAllDakbyType($officeid, $unit_id,
                $designation_id, 'NothiVukto');
        }

        /*         * Nothivukto * */

        /*         * Nothijato * */
        $totaltodaynothijato     = $DakMovementsTable->countAllDakbyType($officeid, $unit_id,
            $designation_id, 'NothiJato', [$today, $today]);
        $totalyesterdaynothijato = $DakMovementsTable->countAllDakbyType($officeid, $unit_id,
            $designation_id, 'NothiJato', [$yesterday, $yesterday]);
        if ($result['totalID'] > 0) {
            $totalprevnothijato = $result['totalNothijato'] + $totaltodaynothijato;
        }
        else {
            $totalprevnothijato = $DakMovementsTable->countAllDakbyType($officeid, $unit_id,
                $designation_id, 'NothiJato');
        }

        /*         * Nothijato * */

        /*         * Total Potrojari* */
        $totaltodaypotrojari     = $potrojariTable->allPotrojariCount($officeid, $unit_id,
            $designation_id, [$today, $today]);
        $totalyesterdaypotrojari = $potrojariTable->allPotrojariCount($officeid, $unit_id,
            $designation_id, [$yesterday, $yesterday]);
        if ($result['totalID'] > 0) {
            $totalprepotrojari = $result['totalPotrojari'] + $totaltodaypotrojari;
        }
        else {
            $totalprepotrojari = $potrojariTable->allPotrojariCount($officeid, $unit_id,
                $designation_id);
        }

        /*         * Total Potrojari* */

        /** Nothi Note Part */
        $temp_today = $this->nothiDashboardCount($officeid, $unit_id, $designation_id,
            [ $today, $today]);
//        print_r($temp_today);die;

        $temp_yesterday = $this->nothiDashboardCount($officeid, $unit_id, $designation_id,
            [ $yesterday, $yesterday]);

        $returnSummary['ajax_request'] = false;
        if ($result['totalID'] <= 0) {
            $returnSummary['ajax_request'] = true;
        }


        /*         * Result* */

        $returnSummary['totallogin']     = $TotalLogin;
        $returnSummary['yesterdayLogin'] = $yesterdayLogin;
        // Inbox
        $returnSummary['totaltodayinbox']     = $totaltodayinbox;
        $returnSummary['totalyesterdayinbox'] = $totalyesterdayinbox;
        $returnSummary['totalinboxall']       = $totalprevinbox;
        // Outbox
        $returnSummary['totaltodayoutbox']     = $totaltodayoutbox;
        $returnSummary['totalyesterdayoutbox'] = $totalyesterdayoutbox;
        $returnSummary['totaloutboxall']       = $totalprevoutbox;
        // Nothivukto
        $returnSummary['totaltodaynothivukto']     = $totaltodaynothivukto;
        $returnSummary['totalyesterdaynothivukto'] = $totalyesterdaynothivukto;
        $returnSummary['totalnothivuktoall']       = $totalprevnothivukto;
        // NothiJato
        $returnSummary['totaltodaynothijato']     = $totaltodaynothijato;
        $returnSummary['totalyesterdaynothijato'] = $totalyesterdaynothijato;
        $returnSummary['totalnothijatoall']       = $totalprevnothijato;
        // Nisponno Dak
        $returnSummary['totaltodaynisponnodak']     = $returnSummary['totaltodayoutbox'] + $returnSummary['totaltodaynothijato']
            + $returnSummary['totaltodaynothivukto'] + $DakMovementsTable->countOnulipiDak($officeid,
                $unit_id, $designation_id, [ $today, $today]);
        $returnSummary['totalyesterdaynisponnodak'] = $returnSummary['totalyesterdayoutbox'] + $returnSummary['totalyesterdaynothijato']
            + $returnSummary['totalyesterdaynothivukto'] + $DakMovementsTable->countOnulipiDak($officeid,
                $unit_id, $designation_id, [ $yesterday, $yesterday]);
        if ($result['totalID'] > 0) {
            $returnSummary['totalnisponnodakall'] = $result['totalNisponnoDak'] + $returnSummary['totaltodaynisponnodak'];
        }
        else {
            $returnSummary['totalnisponnodakall'] = $returnSummary['totaloutboxall'] + $returnSummary['totalnothijatoall']
                + $returnSummary['totalnothivuktoall'] + $DakMovementsTable->countOnulipiDak($officeid,
                    $unit_id, $designation_id);
        }

        // Onisponno Dak
        $returnSummary['totaltodayOnisponnodak'] = TableRegistry::get('DakUsers')->getOnisponnoDak($officeid,
            $unit_id, $designation_id, $today);

         if ($result['totalID'] > 0) {
            $returnSummary['totalyesterdayOnisponnodak'] = $result['totalyesterdayOnisponnodak'];
        }else{
              $returnSummary['totalyesterdayOnisponnodak'] = TableRegistry::get('DakUsers')->getOnisponnoDak($officeid,
            $unit_id, $designation_id, $yesterday);
        }

//          $returnSummary['totalyesterdayOnisponnodak']  = $returnSummary['totaltodayOnisponnodak'] - $returnSummary['totaltodayinbox']
//            + $returnSummary['totaltodaynisponnodak'];
//        if ($result['totalID'] > 0) {
//            $returnSummary['totalOnisponnodakall'] = $result['totalOnisponnodakall'] +( $returnSummary['totaltodayinbox'] - $returnSummary['totaltodaynisponnodak'] );
            $returnSummary['totalOnisponnodakall'] = $returnSummary['totaltodayOnisponnodak'];
//        }
//        else {
//            $returnSummary['totalOnisponnodakall'] = TableRegistry::get('DakUsers')->getOnisponnoDak($officeid,
//                $unit_id, $designation_id);
//        }
            // Potrojari
        $returnSummary['totaltodaypotrojari']     = $totaltodaypotrojari;
        $returnSummary['totalyesterdaypotrojari'] = $totalyesterdaypotrojari;
        $returnSummary['totalpotrojariall']       = $totalprepotrojari;
        // Dak Soho Note can not exceed Nothivukto
        $returnSummary['totaltodaydaksohonote']     = $temp_today['dakNote'];
        $returnSummary['totalyesterdaydaksohonote'] =  $temp_yesterday['dakNote'];
        if ($result['totalID'] > 0) {
            $returnSummary['totaldaksohonoteall'] = $result['totalDaksohoNote'] + $returnSummary['totaltodaydaksohonote'];
             $returnSummary['totaldaksohonoteall'] =($totalprevnothivukto < $returnSummary['totaldaksohonoteall'] ?$totalprevnothivukto :  $returnSummary['totaldaksohonoteall']);
        }

        //  Nisponno By Note
        $returnSummary['totaltodaynisponnonote']     = $temp_today['noteNisponno'];
        $returnSummary['totalyesterdaynisponnonote'] = $temp_yesterday['noteNisponno'];
        if ($result['totalID'] > 0) {
            $returnSummary['totalnisponnonoteall'] = $result['totalNisponnoNote'] + $returnSummary['totaltodaynisponnonote'];
        }
        
        // Nisponno Potrojari can not be greater than potrojari
        $returnSummary['totaltodaynisponnopotrojari']     = ($totaltodaypotrojari < $temp_today['potrojariNisponno'] ?$totaltodaypotrojari :  $temp_today['potrojariNisponno']);
        $returnSummary['totalyesterdaynisponnopotrojari'] = /*$temp_yesterday['potrojariNisponno'];*/ ($totalyesterdaypotrojari < $temp_yesterday['potrojariNisponno'] ?$totalyesterdaypotrojari :  $temp_yesterday['potrojariNisponno']);
        if ($result['totalID'] > 0) {
            $returnSummary['totalnisponnopotrojariall'] = $result['totalNisponnoPotrojari'] + $returnSummary['totaltodaynisponnopotrojari'];
            $returnSummary['totalnisponnopotrojariall'] = ($totalprepotrojari < $returnSummary['totalnisponnopotrojariall']  ?$totalprepotrojari :  $returnSummary['totalnisponnopotrojariall'] );
        }
        

//        $returnSummary['totaltodayOnisponnonote']     = $this->getOnisponnoNoteCount($officeid,
//            $unit_id, $designation_id, [$today, $today]);
         if ($result['totalID'] > 0) {
            $returnSummary['totalyesterdayOnisponnonote'] = $result['totalyesterdayOnisponnonote'];
        }else{
             $returnSummary['totalyesterdayOnisponnonote'] = $this->getOnisponnoNoteCount($officeid,
            $unit_id, $designation_id, [$yesterday, $yesterday]);
        }
        
            $returnSummary['totaltodayOnisponnonote']     = $this->getOnisponnoNoteCount($officeid,
            $unit_id, $designation_id, [$today, $today]);
//        if ($result['totalID'] > 0) {
//            print_r($result['totalONisponno']);            print_r($temp_today['selfNote'] + $temp_today['dakNote']);die;
//            $returnSummary['totalOnisponnonoteall']   = $result['totalONisponno'] + $temp_today['selfNote']
//                + $temp_today['dakNote'] - $temp_today['noteNisponno'] - $temp_today['potrojariNisponno'];
             $returnSummary['totalOnisponnonoteall'] = $returnSummary['totaltodayOnisponnonote'] ;
//        }else{
//
//        }
                                     /*         * Self Given Note* */
        $returnSummary['totaltodaysrijitonote']     = $temp_today['selfNote'] ;
        $returnSummary['totalyesterdaysrijitonote'] = $temp_yesterday['selfNote'];
         if( $returnSummary['totaltodaysrijitonote'] < 0){
                $returnSummary['totaltodaysrijitonote'] = 0;
            }
             if( $returnSummary['totalyesterdaysrijitonote'] < 0){
                $returnSummary['totalyesterdaysrijitonote'] = 0;
            }
      
//        $returnSummary['totalyesterdayOnisponnonote'] = $returnSummary['totaltodayOnisponnonote'] - ( $returnSummary['totaltodaydaksohonote']
//            + $returnSummary['totaltodaysrijitonote'] - $returnSummary['totaltodaynisponnonote'] - $returnSummary['totalyesterdaynisponnopotrojari']);

          if($returnSummary['totalOnisponnodakall'] < 0){
            $returnSummary['totalOnisponnodakall'] = 0;
        }


        if ($result['totalID'] > 0) {
            $returnSummary['totalsrijitonoteall'] =  $returnSummary['totalnisponnonoteall'] +  $returnSummary['totalnisponnopotrojariall'] +  $returnSummary['totalOnisponnonoteall'] -  ($totalprevnothivukto < $returnSummary['totaldaksohonoteall'] ?$totalprevnothivukto :  $returnSummary['totaldaksohonoteall']);
            if( $returnSummary['totalsrijitonoteall'] < 0){
                $returnSummary['totalsrijitonoteall'] = 0;
            }
        }
        /*         * Self Given Note* */
        $returnSummary['totalinboxall'] = $returnSummary['totalOnisponnodakall'] + $returnSummary['totalnisponnodakall'];

//         $returnSummary['totaltodayOnisponno']     = $returnSummary['totaltodayOnisponnodak'] + $returnSummary['totaltodayOnisponnonote'];
//        $returnSummary['totalyesterdayOnisponno'] = $returnSummary['totalyesterdayOnisponnodak'] + $returnSummary['totalyesterdayOnisponnonote'];
//        $returnSummary['totalOnisponnoall']       = $returnSummary['totalOnisponnodakall'] + $returnSummary['totalOnisponnonoteall'];

        $returnSummary['totalinboxall'] = $returnSummary['totalOnisponnodakall'] + $returnSummary['totalnisponnodakall'];

        if ($returnSummary['totaltodayinbox'] < $returnSummary['totaltodayoutbox']) {
            $returnSummary['totaltodayoutbox'] = $returnSummary['totaltodayinbox'];
        }
        if ($returnSummary['totalyesterdayinbox'] < $returnSummary['totalyesterdayoutbox']) {
            $returnSummary['totalyesterdayoutbox'] = $returnSummary['totalyesterdayinbox'];
        }
        if ($returnSummary['totalinboxall'] < $returnSummary['totaloutboxall']) {
            $returnSummary['totaloutboxall'] = $returnSummary['totalinboxall'];
        }
        if ($returnSummary['totalyesterdayOnisponnodak'] < 0) {
            $returnSummary['totalyesterdayOnisponnodak'] = 0;
        }
        if ($returnSummary['totalyesterdayOnisponnonote'] < 0) {
            $returnSummary['totalyesterdayOnisponnonote'] = 0;
        }

        return $returnSummary;
    }

    public function getOnisponnoNoteCount($office_id = 0, $unit_id = 0, $designation_id = 0,
                                          $time = [])
    {
        TableRegistry::remove('NothiNotes');
        $nothiNotesTable              = TableRegistry::get('NothiNotes');
        TableRegistry::remove('NothiMasterCurrentUsers');
        $nothiMasterCurrentUsersTable = TableRegistry::get('NothiMasterCurrentUsers');
        if (!empty($time[0])) {
            $time[0] = '';
        }

        $data = $nothiMasterCurrentUsersTable->find('list',
                ['keyField' => 'nothi_part_no', 'valueField' => 'nothi_part_no'])->where(['is_finished' => 0,'is_archive' => 0]);

        if (!empty($designation_id)) {
            $data = $data->where(['office_unit_organogram_id' => $designation_id]);
        }
        if (!empty($unit_id)) {
            $data = $data->where(['office_unit_id' => $unit_id]);
        }
        if (!empty($office_id)) {
            $data = $data->where(['office_id' => $office_id]);
        }

        if (!empty($time[1])) {
            $data = $data->where(['DATE(modified) <=' => $time[1]]);
        }

        $allNothi = $data->toArray();
//        return $data->count();

        if (!empty($allNothi)) {
            return $nothiNotesTable->find()->where(['nothi_part_no IN' => $allNothi])->group(['nothi_part_no'])->count();
        }

        return 0;
    }
    public function getOnisponnoNoteCountByMultipleDesignations( $designation_id = [],
                                          $time = [])
    {
        TableRegistry::remove('NothiNotes');
        $nothiNotesTable              = TableRegistry::get('NothiNotes');
        TableRegistry::remove('NothiMasterCurrentUsers');
        $nothiMasterCurrentUsersTable = TableRegistry::get('NothiMasterCurrentUsers');
        if (!empty($time[0])) {
            $time[0] = '';
        }

        $data = $nothiMasterCurrentUsersTable->find('list',
                ['keyField' => 'nothi_part_no', 'valueField' => 'nothi_part_no'])->where(['is_finished' => 0,'is_archive' => 0]);

        if (!empty($designation_id)) {
            $data = $data->where(['office_unit_organogram_id IN' => $designation_id]);
        }

        if (!empty($time[1])) {
            $data = $data->where(['DATE(modified) <=' => $time[1]]);
        }

        $allNothi = $data->toArray();
//        return $data->count();

        if (!empty($allNothi)) {
            return $nothiNotesTable->find()->where(['nothi_part_no IN' => $allNothi])->group(['nothi_part_no'])->count();
        }

        return 0;
    }

    public function getNisponnoNoteCount($office_id = 0, $unit_id = 0, $designation_id = 0,
                                         $time = [])
    {
        $nothiNotesTable              = TableRegistry::get('NothiNotes');
        TableRegistry::remove('NothiMasterCurrentUsers');
        $nothiMasterCurrentUsersTable = TableRegistry::get('NothiMasterCurrentUsers');
        if (!empty($time[0])) {
            $time[0] = '';
        }

        $data = $nothiMasterCurrentUsersTable->find('list',
                ['keyField' => 'nothi_part_no', 'valueField' => 'nothi_part_no'])->where(['is_finished' => 1]);

        if (!empty($designation_id)) {
            $data = $data->where(['office_unit_organogram_id' => $designation_id]);
        }
        if (!empty($unit_id)) {
            $data = $data->where(['office_unit_id' => $unit_id]);
        }
        if (!empty($office_id)) {
            $data = $data->where(['office_id' => $office_id]);
        }

        if (!empty($time[1])) {
            $data = $data->where(['DATE(created) <=' => $time[1]]);
        }

        $allNothi = $data->toArray();
//        return $data->count();

        if (!empty($allNothi)) {
            return $nothiNotesTable->find()->where(['nothi_part_no IN' => $allNothi])->group(['nothi_part_no'])->count();
        }

        return 0;
    }

    public function otherDashboardUpdateDak($officeid = 0, $unit_id = 0, $designation_id = 0,$date = '')
    {
        set_time_limit(0);
        ini_set('memory_limit', '-1');

        TableRegistry::remove('DakMovements');
        $DakMovementsTable = TableRegistry::get('DakMovements');
        TableRegistry::remove('Potrojari');
        $potrojariTable    = TableRegistry::get('Potrojari');
        TableRegistry::remove('DakUsers');
        $DakUsersTable     = TableRegistry::get('DakUsers');
        $today             = date('Y-m-d');
        $yesterday         = date('Y-m-d', strtotime($today.' -1 day'));


        /*         * Inbox Total* */
        $totalprevinbox = $DakMovementsTable->countAllDakbyType($officeid, $unit_id,
            $designation_id, 'Inbox', [ $date, $yesterday]);

        /*         * Inbox Total* */

        /*         * Outbox Total* */
        $totalprevoutbox = $DakMovementsTable->countAllDakbyType($officeid, $unit_id,
            $designation_id, 'Outbox', [ $date, $yesterday]);

        /*         * Outbox Total* */

        /*         * Nothivukto * */
        $totalprevnothivukto = $DakMovementsTable->countAllDakbyType($officeid, $unit_id,
            $designation_id, 'NothiVukto', [ $date, $yesterday]);

        /*         * Nothivukto * */

        /*         * Nothijato * */
        $totalprevnothijato = $DakMovementsTable->countAllDakbyType($officeid, $unit_id,
            $designation_id, 'NothiJato', [ $date, $yesterday]);

        /*         * Nothijato * */

        /*         * Potrojari Nisponno* */
        $totalprepotrojari = TableRegistry::get('NisponnoRecords')->allPotrojariCount($officeid, $unit_id,
            $designation_id, [ $date, $yesterday]);

        /*         * Potrojari Nisponno* */

        /*         * Result* */

        $returnSummary['totalinboxall'] = $totalprevinbox;

        $returnSummary['totaloutboxall'] = $totalprevoutbox;

        $returnSummary['totalnothivuktoall'] = $totalprevnothivukto;

        $returnSummary['totalnothijatoall'] = $totalprevnothijato;

        $returnSummary['totalpotrojariall'] = $totalprepotrojari;

        /*         * Nisponno Dak* */

        $returnSummary['totalnisponnodakall'] = $returnSummary['totaloutboxall'] + $returnSummary['totalnothijatoall']
            + $returnSummary['totalnothivuktoall'] + $DakMovementsTable->countOnulipiDak($officeid,
                $unit_id, 0, [ $date, $yesterday]);

        /*         * Nisponno Dak* */

        /*         * Onisponno Dak* */
        $returnSummary['totalOnisponnodakall'] = $DakUsersTable->getOnisponnoDak($officeid,
            $unit_id, $designation_id, $yesterday);

        /*         * Onisponno Dak* */



        /* --- Dak Part End --- */



        return $returnSummary;
    }

    public function otherDashboardUpdateNothi($officeid = 0, $unit_id = 0, $designation_id = 0,$date = '')
    {
        set_time_limit(0);
        ini_set('memory_limit', '-1');

        $today     = date('Y-m-d');
        $yesterday = date('Y-m-d', strtotime($today.' -1 day'));

        $temp = $this->nothiDashboardCount($officeid, $unit_id, $designation_id, [ $date, $yesterday]);


        /*         * Self Given Note* */
        $returnSummary['totalsrijitonoteall'] = $temp['selfNote'];

        /*         * Self Given Note* */

        /*         * Dak Soho Note* */
        $returnSummary['totaldaksohonoteall'] = $temp['dakNote'];

        /*         * Dak Soho Note* */

        /*         * Nisponno by Note* */
        $returnSummary['totalnisponnonoteall'] = $temp['noteNisponno'];

        /*         * Nisponno by Note* */

        /*         * Nisponno by Potrojari* */
        $returnSummary['totalnisponnopotrojariall'] = $temp['potrojariNisponno'];

        /*         * Nisponno by Potrojari* */

        /*         * Onisponno Note* */
        $returnSummary['totalOnisponnonoteall'] = $temp['onisponno'];

        /*         * Onisponno Note* */

        return $returnSummary;
    }
     public function nothiDashboardCount($office_id = 0, $unit_id = 0, $designation_id = 0,
                                        $time = [])
    {
        set_time_limit(0);
        TableRegistry::remove('NothiMasterCurrentUsers');
        TableRegistry::remove('NisponnoRecords');
        TableRegistry::remove('NoteInitialize');
        TableRegistry::remove('NothiParts');
//        $NothiMasterCurrentUserTable = TableRegistry::get('NothiMasterCurrentUsers');
        $NisponnoRecordsTable        = TableRegistry::get('NisponnoRecords');
        $NoteInitializeTable      = TableRegistry::get('NoteInitialize');

//        $getAllParts        = $NothiMasterCurrentUserTable->getAllNothiPartNo($office_id, $unit_id,
//                $designation_id, $time,1)->toArray();
        $onisponno        =$this->getOnisponnoNoteCount($office_id, $unit_id,$designation_id, $time);
        $self_note  = $NoteInitializeTable->selfSrijitoNote($office_id,$unit_id,$designation_id,$time)->count();
        $dak_note           = $NoteInitializeTable->dakSrijitoNote($office_id, $unit_id,$designation_id, $time)->count();
//        die;
//        if(!empty($getAllParts)){
//              $self_note  = $NoteInitializeTable->selfSrijitoNoteByNoteId($getAllParts,$time)->count();
//              $dak_note           = $NoteInitializeTable->dakSrijitoNoteByNoteId($getAllParts, $time)->count();
//        }else{
//             $self_note   = $dak_note = 0;
//        }
        
        $all_nisponno = $NisponnoRecordsTable->getNisponnoCount($office_id,$unit_id,$designation_id,$time);
        $nisponno_potrojari = $all_nisponno['potrojari'];
        $nisponno_note      = $all_nisponno['note'];
       
        $data = [
            'selfNote' => $self_note,
            'dakNote' => $dak_note,
            'potrojariNisponno' => $nisponno_potrojari,
            'noteNisponno' => $nisponno_note,
            'onisponno' => $onisponno,
//            'total' => $total,
//            'noNotes' => $has,
        ];
//        if(!empty($office_id)){
//
//        }
        return $data;
    }
}