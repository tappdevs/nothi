<?php
namespace App\Model\Table;

use Cake\Datasource\ConnectionManager;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\Validation\Validator;


class DsUserInfoTable extends Table
{

    public function initialize(array $config)
    {
        $conn = ConnectionManager::get('NothiAccessDb');
        $this->connection($conn);
        $this->table('ds_user_info');
        $this->displayField('soft_token');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->addBehavior('MobileTrack');
    }
    public function getData($select = [], $conditions = [], $order = []){
        $query = $this->find();
        if(!empty($select)){
            $query->select($select);
        }
        if(!empty($conditions)){
            $query->where($conditions);
        }
        if(!empty($order)){
            $query->order($order);
        }
        return $query;
    }
    public function setData($data){
        if(empty($data) || empty($data['soft_token'])){
            return false;
        }
        try{
            $entity = $this->newEntity();
            $entity->username = $data['username'];
            $entity->employee_record_id = $data['employee_record_id'];
            $entity->soft_token = base64_encode($data['soft_token']);
            $entity->expired_at = $data['expired_at'];
            $this->save($entity);
            return true;
        }catch (\Exception $ex){

        }
        return false;
    }

}
