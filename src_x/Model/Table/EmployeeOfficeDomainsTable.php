<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use App\Model\Table\ProjapotiTable;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;
/**
 * Office Model
 */
class EmployeeOfficeDomainsTable extends ProjapotiTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $conn = ConnectionManager::get('projapotiDb');
        $this->connection($conn);

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->requirePresence('id', 'update')
            ->notEmpty('id');

        $validator
            ->requirePresence('office_id')
            ->notEmpty('office_id');

        $validator
            ->requirePresence('office_domains_id')
            ->notEmpty('office_domains_id');
        
        $validator
            ->requirePresence('employee_record_id')
            ->notEmpty('employee_record_id');

        $validator
            ->allowEmpty('modified_by');

        return $validator;
    }

    public function getOfficeDomainbyEmployee($employee_id = 0){
        
        return $this->find()->where(['employee_record_id'=>$employee_id])->toArray();
    }

    public function getOfficeDomainbyOfficeId($office_id = 0){
        
        return $this->find()->where(['office_id'=>$office_id])->toArray();
    }

    public function getOfficeDomainbyDomainId($domain_id = 0){
        
        return $this->find()->where(['office_domains_id'=>$domain_id])->toArray();
    }

}
