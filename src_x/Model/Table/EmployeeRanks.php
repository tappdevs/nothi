<?php
namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;


class EmployeeRanksTable extends ProjapotiTable
{
    public function initialize(array $config)
    {
        $this->table('employee_ranks');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->belongsTo('OfficeMinistries', [
            'foreignKey' => 'office_ministry_id'
            /* ,
            'joinType' => 'INNER' */
        ]);

    }
}