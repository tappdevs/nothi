<?php

namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;

class EmployeeRecordsTable extends ProjapotiTable
{

    public $prefix = [
        'cadre' => 1,
        'noncadre' => 2,
        'cadrenoid' => 3,
        'publicrepr' => 7,
        'existcadre' => 5
    ];

    public function initialize(array $config)
    {
        parent::initialize($config);
        $conn = ConnectionManager::get('projapotiDb');
        $this->connection($conn);

        $this->entityClass('EmployeeRecords');

        $this->hasOne('Users', [
            'conditions' => 'Users.employee_record_id = EmployeeRecords.id'
        ]);
        $this->displayField('identity_no');
        $this->addBehavior('Timestamp');
    }

    public function validationAdd($validator)
    {
        $validator
//            ->notEmpty('name_eng', " দয়া করে নিজের নামটি ইংরেজিতে দিন ")
            ->notEmpty('name_bng', " দয়া করে নিজের নামটি বাংলায় দিন ")
            ->notEmpty('nid', " দয়া করে নিজের জাতীয় পরিচয়পত্র নম্বর  দিন ")
            ->notEmpty('personal_mobile', "দয়া করে মোবাইল নাম্বারটি দিন")
            ->add('nid', 'isUnique',
                [
                    'rule' => function ($data, $provider) {

                        if ($this->find()->where(['nid' => $data])->count() > 0) {
                            throw new \Exception("জাতীয় পরিচয়পত্র নম্বর পূর্বে ব্যবহৃত হয়েছে");
                        }
                        return true;
                    },
                    'message' => __('জাতীয় পরিচয়পত্র নম্বর পূর্বে ব্যবহৃত হয়েছে')
                ]);

        return $validator;
    }

    public function validationEdit($validator)
    {
        $validator
            ->notEmpty('nid', " দয়া করে নিজের জাতীয় পরিচয়পত্র নম্বর  দিন ")
            ->notEmpty('name_bng', " দয়া করে নিজের নামটি বাংলায় দিন।  ")
            ->notEmpty('personal_mobile', "দয়া করে মোবাইল নাম্বারটি দিন। ")
            ->add('nid', 'isUnique',
                [
                    'rule' => function ($data, $provider) {

                        if ($this->find()->where(['nid' => $data, 'id <>' => $provider['data']['id']])->count() > 0) {

                            throw new \Exception("জাতীয় পরিচয়পত্র নম্বর পূর্বে ব্যবহৃত হয়েছে");
                        }
                        return true;
                    },
                    'message' => __('জাতীয় পরিচয়পত্র নম্বর পূর্বে ব্যবহৃত হয়েছে')
                ]);

        return $validator;
    }

    public function getPersonalInfoByIdentityNo($identity_no)
    {
        return $this->find()->select(['id', 'name_bng', 'personal_email', 'personal_mobile'])->where(['identity_no' => $identity_no])->first();
    }

        public function getEmployeeInfoByRecordId($id)
    {
        return $this->find()->select(['id', 'name_bng', 'personal_email', 'personal_mobile','default_sign'])->where(['EmployeeRecords.id' => $id])->first();
    }

    public function getAll($conditions = '', $select = '', $type = 'all',
                           $options = [])
    {

        $queryString = $this->find($type, $options);

        if (!empty($select)) {
            $queryString = $queryString->select($select);
        }

        if (!empty($conditions)) {
            $queryString = $queryString->where($conditions);
        }

        return $queryString;
    }

    public function getAllWithDetails($conditions = null)
    {

        $query = $this->find()->select([
            'username' => 'Users.username',
            'user_id' => 'Users.id',
            'active' => 'Users.active',
            'name_bng',
            'id' => 'EmployeeRecords.id',
            'name_eng',
            'date_of_birth',
            'blood_group',
            'personal_email',
            'personal_mobile',
            'is_cadre',
            'identity_no',
            'incharge_label' => 'EmployeeOffices.incharge_label',
            'status' => 'EmployeeOffices.status',
            'office_name_bng' => 'Offices.office_name_bng',
            'unit_name_bng' => 'OfficeUnits.unit_name_bng',
            'designation_bng' => 'EmployeeOffices.designation',
        ]);

        $query->join([
            'Users' => [
                'table' => 'users',
                'type' => 'inner',
                'conditions' => 'Users.employee_record_id = EmployeeRecords.id'
            ],
            'EmployeeOffices' => [
                'table' => 'employee_offices',
                'type' => 'left',
                'conditions' => 'EmployeeOffices.employee_record_id = EmployeeRecords.id'
            ],
            'Offices' => [
                'table' => 'offices',
                'type' => 'left',
                'conditions' => 'Offices.id = EmployeeOffices.office_id AND Offices.active_status = 1'
            ],
            'OfficeUnits' => [
                'table' => 'office_units',
                'type' => 'left',
                'conditions' => 'EmployeeOffices.office_unit_id = OfficeUnits.id AND OfficeUnits.active_status = 1'
            ]
        ]);

        if (!empty($conditions)) {
            $query->where($conditions);
        }

        $query->order(['EmployeeRecords.id desc']);

        return $query;
    }

    public function getEmployeeInfoByDesignation($designation_id = 0,$status = 1)
    {
        if (!empty($designation_id)) {
            $EmployeeOfficesTable = TableRegistry::get('EmployeeOffices');
            $designationSummary = $EmployeeOfficesTable->getDesignationInfo($designation_id,$status);
            if (!empty($designationSummary['employee_record_id'])) {
                return $this->find()->select(['name_bng'])->where(['id' => $designationSummary['employee_record_id']])->first();
            }
            return;
        }
    }


    public function getCadreId($data)
    {

        $usersTable = TableRegistry::get('Users');
        $id = !empty($data['id']) ? $data['id'] : 0;

        if ($data['is_cadre'] == 1) {
            if (empty($data['no_service_id'])) {
                return $this->prefix['cadrenoid'];
            } else {
                if (!empty($data['identity_no'])) {
                    $is_ex = $usersTable->find()->select(['username'])->where(['username' => ($data['is_cadre'] . str_pad(intval($data['identity_no']), 11, '0', STR_PAD_LEFT))]);

                    if (!empty($id)) {
                        $is_ex->where(['employee_record_id <>' => $id]);
                    }

                    if ($is_ex->count() > 0) {
                        return $this->prefix['existcadre'];
                    } else {
                        return $this->prefix['cadre'];
                    }
                } else {
                    return 0;
                }
            }
        } elseif ($data['is_cadre'] == 2) {
            return $this->prefix['noncadre'];
        } elseif ($data['is_cadre'] == 7) {
            return $this->prefix['publicrepr'];
        }

        return 0;
    }

    public function createUserId($data)
    {

        $cadre = $data['is_cadre'];
        $usersTable = TableRegistry::get('Users');

        $array = array_values($this->prefix);
        if (!in_array($cadre, $array)) {
            return '';
        }

        if ($cadre === 1 || $cadre === 5) {
            $data['identity_no'] = bnToen($data['identity_no']);
            return $cadre . str_pad(intval($data['identity_no']), 11, '0', STR_PAD_LEFT);
        } elseif ($cadre === 2 || $cadre === 3 || $cadre === 7) {
            $last_user = $usersTable->find()->select(['username'])->where('username Like ("' . $cadre . '%") AND CHAR_LENGTH(username)=12')->order(['cast(username as signed) desc'])->first();

            if (!empty($last_user)) {
                return $cadre . str_pad(intval(substr($last_user['username'], 1)) + 1, 11, '0', STR_PAD_LEFT);
            } else {
                return $cadre . str_pad(1, 11, '0', STR_PAD_LEFT);
            }
        }

        return '';
    }

    public function getUserDatabyNID($n_id)
    {
        return $this->find()->where(['nid' => $n_id]);

    }

    public function getLastId(){
        return $this->find()->select(['id'])->order(['id'=>'desc'])->first();
    }
}