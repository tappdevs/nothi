<?php
namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;


class ErrorLogTable extends ProjapotiTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $conn = ConnectionManager::get('projapotiDb');
        $this->connection($conn);
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
    }
    
    public function getError($office_id = 0 , $status  = -1){
        $query = $this->find()->where(['office_id'=>$office_id]);
        
        if($status != -1){
            $query->where(['error_status'=>$status]);
        }
        
        return $query->toArray();
    }
}