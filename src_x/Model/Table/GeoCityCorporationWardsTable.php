<?php

namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;

class GeoCityCorporationWardsTable extends ProjapotiTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $conn = ConnectionManager::get('projapotiDb');
        $this->connection($conn);
        $this->table('geo_city_corporation_wards');
        $this->displayField('ward_name_bng');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->belongsTo('GeoDistricts',
            [
            'foreignKey' => 'geo_district_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('GeoDivisions',
            [
            'foreignKey' => 'geo_division_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('GeoCityCorporations',
            [
            'foreignKey' => 'geo_city_corporation_id',
            'joinType' => 'INNER'
        ]);
    }
}