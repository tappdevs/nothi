<?php

namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;

class GeoPostOfficesTable extends ProjapotiTable
{

    public function initialize(array $config)
    {
        $conn = ConnectionManager::get('projapotiDb');
        $this->connection($conn);
        $this->table('geo_post_offices');
        $this->displayField('postoffice_name_bng');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->belongsTo('GeoDistricts',
            [
            'foreignKey' => 'geo_district_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('GeoDivisions',
            [
            'foreignKey' => 'geo_division_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('GeoThanas',
            [
            'foreignKey' => '	geo_thana_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('GeoUpazilas',
            [
            'foreignKey' => 'geo_upazila_id',
            'joinType' => 'INNER'
        ]);
    }
}