<?php

namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;

class GuardFilesTable extends ProjapotiTable {

    public function initialize(array $config) {
        $conn = ConnectionManager::get('NothiAccessDb');
        $this->connection($conn);
        
        $this->displayField('name_bng');
        $this->primaryKey('id');
        
    }
    
    
    public function validationAdd($validator) {
        $validator
            ->notEmpty('name_bng', "এই তথ্য ফাকা রাখা যাবে না")
            ->notEmpty('guard_file_category_id', "এই তথ্য ফাকা রাখা যাবে না")
            ->add('name_bng', 'isUnique', [
                'rule' => function($data, $provider) {

                    if ($this->find()->where(['name_bng' => $data, 'guard_file_category_id' => $provider['data']['guard_file_category_id'], 'office_id' => $provider['data']['office_id']])->count() > 0){
                        return "এটি পূর্বে ব্যবহৃত হয়েছে";
                    }
                    return true;
                },
            'message' => __('এটি পূর্বে ব্যবহৃত হয়েছে')
        ]);

        return $validator;
    }

    public function validationEdit($validator) {
        $validator
            ->notEmpty('name_bng', "এই তথ্য ফাকা রাখা যাবে না")
            ->notEmpty('guard_file_category_id', "এই তথ্য ফাকা রাখা যাবে না")
            ->add('name_bng', 'isUnique', [
                'rule' => function($data, $provider) {
                    if ($this->find()->where(['name_bng' => $data, 'id <>' => $provider['data']['id'], 'guard_file_category_id' => $provider['data']['guard_file_category_id'], 'office_id' => $provider['data']['office_id']])->count() > 0)
                        return "এটি পূর্বে ব্যবহৃত হয়েছে";
                    return true;
                },
            'message' => __('এটি পূর্বে ব্যবহৃত হয়েছে')
        ]);

        return $validator;
    }
    
    
    public function getAll($page = 0, $len = 100, $employee = null) {
        $queryString = $this->find()->select([
            "category_name"=>"GuardFileCategories.name_bng",
            "name_bng",
            "id",
            "file_number",
            "office_name",
            "office_unit_name",
            "office_unit_organogram_id",
        ])
                ->join([
                    'GuardFileCategories'=>[
                        'table'=>'guard_file_categories',
                        'type'=>'inner',
                        'conditions'=>"GuardFileCategories.id = GuardFiles.guard_file_category_id"
                    ]
                ])
                //->where(["GuardFiles.office_id" => $employee['office_id'], "GuardFiles.office_unit_id" => $employee['office_unit_id']])
                ->where(["GuardFiles.office_id" => $employee['office_id']])
                ->order("GuardFiles.guard_file_category_id ASC");


        if (!empty($page)) {
            $queryString = $queryString->page($page, $len);
        }

        return $queryString;
    }
    
    public function getFileNumber($office_id, $unit_id, $cat_id){
        
        return $this->find()->select(['file_number'])->where(['office_id'=>$office_id,'guard_file_category_id'=>$cat_id])->order(['file_number desc'])->first();
    }
    
    public function getByUnit($id, $employee){
        
        return $this->find()->select([
            "category_name"=>"GuardFileCategories.name_bng",
            "name_bng",
            "id",
            "file_number",
            "office_name",
            "office_id",
            "office_unit_id",
            "office_unit_name",
        ])
        ->join([
            'GuardFileCategories'=>[
                'table'=>'guard_file_categories',
                'type'=>'inner',
                'conditions'=>"GuardFileCategories.id = GuardFiles.guard_file_category_id"
            ]
        ])
        ->where(["GuardFiles.office_id" => $employee['office_id'], "GuardFiles.office_unit_id" => $employee['office_unit_id'],'GuardFiles.id'=>$id])
        ->order("GuardFiles.guard_file_category_id ASC")->first();

    }


}
