<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Network\Email\Email;
use Cake\Datasource\ConnectionManager;
use Cake\Controller\Component;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;
class LoginNoticeAttachmentsTable extends Table {
    
    public function initialize(array $config) {
        //parent::initialize($config);
        $conn = ConnectionManager::get('NothiAccessDb');
        $this->connection($conn);
        $this->table('login_notice_attachments');
        $this->primaryKey('id');
    }
}
