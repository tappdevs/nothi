<?php

namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\Datasource\ConnectionManager;
use Cake\Controller\Component;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

class MonitorOfficesTable extends ProjapotiTable
{

    public function initialize(array $config)
    {
        //parent::initialize($config);
        $conn = ConnectionManager::get('NothiReportsDb');
        $this->connection($conn);
        $this->table('monitor_offices');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
    }

    public function checkUpdate($office_id = 0, $date = '')
    {
        if (!empty($date) && !empty($office_id)) {
            return $this->find()->where(['updated' => $date, 'office_id' => $office_id])->count();
        }
    }
    /**
     *
     * @param int $office_id
     * @param DateTime $time
     * @return object  CollectionBetweenTwoDates
     */
    public function getOfficeData($office_id = 0, $time = [])
    {
        if (!empty($office_id)) {
            $query = $this->find()->where(['office_id' => $office_id]);
            if (!empty($time)) {
                if (!empty($time[0])) {
                    $query = $query->where(['updated >=' => $time[0]]);
                }
                if (!empty($time[1])) {
                    $query = $query->where(['updated <=' => $time[1]]);
                }
            }
            return $query;
        }
        return;
    }
    public function getLastUpdateTime($office_id = 0){
        $query =  $this->find()->select(['modified'])->order(['modified desc']);
        if(!empty($office_id)){
            $query = $query->where(['office_id' => $office_id]);
        }
    return$query->first();
    }
    public function deleteDataofOffices($office_id = 0, $date =''){
        if(!empty($office_id)){
            $condition = [
                'office_id' => $office_id,
            ];
              if(!empty($date)){
                     $condition = [
                'office_id' => $office_id,
                'updated' => $date
            ];
            }
            $query = $this->deleteAll([ $condition ]);
            return $query;
        }
       return 0;
    }
}