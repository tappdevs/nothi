<?php

namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;
use Cake\Routing\Router;

class NothiDataChangeHistoryTable extends ProjapotiTable
{

    public function initialize(array $config)
    {
        $this->displayField('nothi_data');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
    }
    public function saveRecord($data){
        $response = ['status' => 'error', 'message' => 'Something went wrong'];
        try{
            $selected_office_section = $this->getCurrentDakSection();
            $session = Router::getRequest()->session();
            $loggedUser = $session->read('loggedUser');
            $data['employee_id'] = isset($selected_office_section['officer_id'])?$selected_office_section['officer_id']:0;
            $data['office_unit_organogram_id'] = isset($selected_office_section['office_unit_organogram_id'])?$selected_office_section['office_unit_organogram_id']:0;
            $data['created_by'] = $loggedUser['id'];
            $data['modified_by'] = $loggedUser['id'];
            $entity = $this->newEntity();
            $data_to_save =  $this->patchEntity($entity,$data);
            if($this->save($data_to_save)){
                $response = ['status' => 'success', 'message' => ''];
            }else{
                $response['message'] = 'দুঃখিত! নথি পরিবর্তন তথ্য সংরক্ষণ সম্ভব হয়নি।';
            }
        }catch (\Exception $ex){
            $response['message'] ='টেকনিক্যাল ত্রুটি হয়েছে।';
            $response['reason'] =makeEncryptedData($ex->getMessage());
        }
        return $response;
    }

}