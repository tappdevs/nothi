<?php
namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;

class NothiDecisionEmployeesTable extends ProjapotiTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $conn = ConnectionManager::get('NothiAccessDb');
        $this->connection($conn);
        $this->primaryKey('id');
        $this->displayField('nothi_decision_id');
        $this->addBehavior('Timestamp');
    }
    public function GetNothiDecisionsByEmployee($employee_id){
        return $this->find('list', ['keyField' => 'id', 'valueField' => 'decisions'])->select(['id'=>'NothiDecisions.id','decisions'=>'NothiDecisions.decisions'])
            ->join([
                'NothiDecisions'=>[
                    'table'=>'nothi_decisions_mig',
                    'type'=>'inner',
                    'conditions' => [
                        'NothiDecisionEmployees.nothi_decision_id= NothiDecisions.id'
                    ]
                ]
            ])
            ->where(['NothiDecisionEmployees.employee_id'=>$employee_id])
            ->where(['NothiDecisions.status'=>1])
            ->order(['NothiDecisionEmployees.id' => 'DESC'])
            ->toArray();
    }
}