<?php
namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;

class NothiDecisionsTable extends ProjapotiTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $conn = ConnectionManager::get('projapotiDb');
        $this->connection($conn);
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
    }
         public function GetNothiDecisions($org_id = 0)
    {
        $query = $this->find()->select(['id','decisions','organogram_id'])->where(['status'=>1]);
        
        if(!empty($org_id)){
            $query->where(['organogram_id'=>$org_id])->orWhere(['organogram_id'=>0]);
        }
        
        return $query->order(['id asc']);
    }
}