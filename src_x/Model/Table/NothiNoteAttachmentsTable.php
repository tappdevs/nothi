<?php
namespace App\Model\Table;

use App\Model\Entity\NothiNoteAttachment;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * NothiNoteAttachments Model
 */
class NothiNoteAttachmentsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('nothi_note_attachments');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->belongsTo('NothiMasters',
            [
            'foreignKey' => 'nothi_master_id',
            'joinType' => 'INNER'
        ]);
//        $this->belongsTo('NothiNotesheets', [
//            'foreignKey' => 'nothi_notesheet_id',
//            'joinType' => 'INNER'
//        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nothi_part_no', 'create')
            ->notEmpty('nothi_part_no');

        $validator
            ->requirePresence('note_no', 'create')
            ->notEmpty('note_no');

        $validator
            ->requirePresence('attachment_type', 'create')
            ->notEmpty('attachment_type');

        $validator
            ->requirePresence('file_name', 'create')
            ->notEmpty('file_name');

        $validator
            ->allowEmpty('user_file_name');

        $validator
            ->requirePresence('file_dir', 'create')
            ->notEmpty('file_dir');

        $validator
            ->allowEmpty('digital_sign');

        $validator
            ->allowEmpty('sign_info');

        $validator
            ->add('created_by', 'valid', ['rule' => 'numeric'])
            ->requirePresence('created_by', 'create')
            ->notEmpty('created_by');

        $validator
            ->add('modified_by', 'valid', ['rule' => 'numeric'])
            ->requirePresence('modified_by', 'create')
            ->notEmpty('modified_by');

        $validator
            ->allowEmpty('token');

        $validator
            ->allowEmpty('device_type');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['nothi_master_id'], 'NothiMasters'));
//        $rules->add($rules->existsIn(['nothi_notesheet_id'], 'NothiNotesheets'));

        return $rules;
    }

    public function getAllAttachments($part_no = 0, $note_no = [], $select = [])
    {
        $query = $this->find();
        if (!empty($select)) {
            $query->select($select);
        }
        if (!empty($part_no)) {
            $query->where(['nothi_part_no' => $part_no]);
        }
        if (!empty($note_no)) {
            if (is_array($note_no)) {
                $query->where(['note_no IN' => $note_no]);
            }
            else {
                $query->where(['note_no' => $note_no]);
            }
        }
        return $query;
    }
}