<?php
namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

class NothiNotePermissionsTable extends ProjapotiTable
{

    public function validationAdd($validator)
    {
        $validator
            ->notEmpty('nothi_masters_id', "এই তথ্য ফাকা রাখা যাবে না")
            ->notEmpty('nothi_note_id', "এই তথ্য ফাকা রাখা যাবে না");

        return $validator;
    }

    public function validationEdit($validator)
    {

        return $validator;
    }
}