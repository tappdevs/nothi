<?php
namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

class NothiPotrosTable extends ProjapotiTable
{
    public function initialize(array $config)
    {
        parent::initialize($config);
    }

    public function validationAdd($validator)
    {
        $validator
            ->notEmpty('nothi_master_id', "এই তথ্য ফাকা রাখা যাবে না")
            ->notEmpty('sarok_no', "এই তথ্য ফাকা রাখা যাবে না");

        return $validator;
    }

    public function getInfo($condition = []){

        $queryString = $this->find();

        if(empty($condition)){
            $queryString = $queryString->where([$condition]);
        }


        return $queryString;
    }
    public function getNothijatoDakIdandTypebyNothiMaster($nothiMasterId){
      return  $this->find()->select(['dak_id','dak_type'])->where(['nothi_master_id' => $nothiMasterId,'dak_id <>' => 0,'dak_type is not NULL' ]);
    }

    public function rollbackFromNothijatoToDak($dak_id, $dak_type, $employee_office, $dak_receipt_no, $user){
        $response = [];

        if($dak_type == 'Daptorik'){
             $table_instance_dd   = TableRegistry::get('DakDaptoriks');
        }
        else if($dak_type == 'Nagorik'){
             $table_instance_dd   = TableRegistry::get('DakNagoriks');
        }

        //old_dak_info
        $dak_entity = $table_instance_dd->get($dak_id);

        if(!isset($dak_entity->is_rollback_to_dak) || $dak_entity->is_rollback_to_dak == 1){
            return ['status'=>'error', 'msg'=> "ডাকটি ইতিপুর্বে ফেরত আনা হয়েছে"];
        }

        //new_dak_info
        $new_dak_entity = $table_instance_dd->newEntity();
        // Dak Daptoriks or Nagoriks will get a new entry
       if($dak_type == 'Daptorik'){
           $new_dak_entity->sender_sarok_no = $dak_entity->sender_sarok_no;
           $dataset = $this->makeDaptorikTableDataset($dak_entity,$employee_office,$dak_receipt_no,$user);
       }

        if($dak_type == 'Nagorik'){
             $dataset = $this->makeNagorikTableDataset($dak_entity,$employee_office,$dak_receipt_no,$user);
        }
        // some common colunms of daptoriks and nagoriks - start
        $dataset['docketing_no']      = 0;
        $dataset['dak_subject']        = $dak_entity['dak_subject'];
         $dataset['dak_received_no']   = $dak_receipt_no;
         $dataset['dak_security_level'] = $dak_entity['dak_security_level'];
        $dataset['dak_priority_level'] = $dak_entity['dak_priority_level'];

        $dataset['receiving_office_id']                 = $employee_office['office_id'];
        $dataset['receiving_office_unit_id']            = $employee_office['office_unit_id'];
        $dataset['receiving_office_unit_name']          = $employee_office['office_unit_name'];
        $dataset['receiving_officer_id']                = $employee_office['officer_id'];
            $dataset['receiving_officer_designation_id']    = $employee_office['office_unit_organogram_id'];
        $dataset['receiving_officer_designation_label'] = $employee_office['designation_label'];
        $dataset['receiving_officer_name']              = $employee_office['officer_name'];

        $dataset['dak_status']       = DAK_CATEGORY_DRAFT;
        $dataset['created_by']              = $user['id'];
        $dataset['modified_by']             = $user['id'];
        $dataset['uploader_designation_id'] = $employee_office['office_unit_organogram_id'];

         $dataset['previous_receipt_no']   = !empty($dak_entity['previous_receipt_no']) ? $dak_entity['previous_receipt_no'].",".$dak_entity['dak_received_no']
                : $dak_entity['dak_received_no'];
        $dataset['previous_docketing_no'] = !empty($dak_entity['previous_docketing_no']) ? $dak_entity['previous_docketing_no'].",".$dak_entity['docketing_no']
                : $dak_entity['docketing_no'];

        // some common colunms of daptoriks and nagoriks - end


        $dak_daptoriks = $table_instance_dd->patchEntity($new_dak_entity, $dataset);
        if($error = $dak_daptoriks->errors()){
            return $response = ['status'=>'error', 'error'=>$error];
        }

        $conn = \Cake\Datasource\ConnectionManager::get('default');
        $conn->begin();
        try{
            $dak = $table_instance_dd->save($dak_daptoriks);
            // Save done for daptoriks or nagoriks

            $table_instance_da = TableRegistry::get('DakAttachments');
            $dak_attachments = $table_instance_da->loadAllAttachmentByDakId($dak_entity['id'],$dak_type);
            $attachments = [];
            foreach($dak_attachments as $da){
                $new_da['file_name'] = $da['file_name'];
                $new_da['file_custom_name'] = $da['file_custom_name'];
                $new_da['attachment_type'] = $da['attachment_type'];
                $new_da['user_file_name'] = $da['user_file_name'];
                $new_da['attachment_description'] = $da['attachment_description'];
                $new_da['dak_id'] = $dak['id'];
                $new_da['dak_type'] = $dak_type;
                $new_da['file_dir'] = $da['file_dir'];
                $new_da['content_cover'] = $da['content_cover'];
                $new_da['content_body'] = $da['content_body'];
                $new_da['meta_data'] = $da['meta_data'];
                $new_da['is_summary_nothi'] = $da['is_summary_nothi'];
                $new_da['is_main'] = $da['is_main'];

                $attachments[] = $new_da;

            }
            $attachments = $table_instance_da->newEntities($attachments);

            foreach ($attachments as $da) {
                $table_instance_da->save($da);
                // Save done for dak attachments
            }

            $table_instance_du  = TableRegistry::get('DakUsers');
            $table_instance_dua = TableRegistry::get('DakUserActions');

            $drafter                                = $table_instance_du->newEntity();
            $drafter->dak_type                      = $dak_type;
            $drafter->dak_id                        = $dak->id;

            $drafter->to_office_id                  = $employee_office['office_id'];
            $drafter->to_office_name                = $employee_office['office_name'];
            $drafter->to_office_unit_id             = $employee_office['office_unit_id'];
            $drafter->to_office_unit_name           = $employee_office['office_unit_name'];
            $drafter->to_office_address             = $employee_office['office_address'];
            $drafter->to_officer_id                 = $employee_office['officer_id'];
            $drafter->to_officer_name               = $employee_office['officer_name'];
            $drafter->to_officer_designation_id     = $employee_office['office_unit_organogram_id'];
            $drafter->to_officer_designation_label  = $employee_office['designation_label'];


            $drafter->dak_view_status               = DAK_VIEW_STATUS_NEW;
            $drafter->dak_priority                  = 0;
            $drafter->attention_type                = 1;
            $drafter->dak_category                  = DAK_CATEGORY_INBOX;
            $drafter->created_by                    = $user['id'];
            $drafter->modified_by                   = $user['id'];

            $table_instance_du->save($drafter);
            // save done for dak user

            $user_action              = $table_instance_dua->newEntity();
            $user_action->dak_id      = $dak->id;
            $user_action->dak_user_id = $drafter->id;
            $user_action->dak_action  = DAK_CATEGORY_DRAFT;
            $user_action->created_by  = $user['id'];
            $table_instance_dua->save($user_action);
            // save done for dak user action

            // lastly set current dak rollback to 1
            $dak_entity->is_rollback_to_dak = 1;
            $table_instance_dd->save($dak_entity);

            $conn->commit();
            return ['status'=>'success', 'dak'=>$dak,'msg' => ' ডাকটি ফেরত আনা হয়েছে।'];
        }
        catch(\Exception $e){
            $conn->rollback();
            return ['status'=>'error', 'msg'=>$e->getMessage()];
        }

        return ['status'=>'error', 'msg'=>"ডাক ফেরত আনা সম্ভব হচ্ছে না"];

    }
     private function makeNagorikTableDataset($dak_entity,$employee_office,$dak_receipt_no,$user){
                $dataset['dak_type_id']                        = $dak_entity->dak_type_id;
                $dataset['description']                        = $dak_entity->description;
                $dataset['name_eng']                        = $dak_entity->name_eng;
                $dataset['name_bng']                        = $dak_entity->name_bng;
                $dataset['sender_name']                        = $dak_entity->sender_name;
                $dataset['national_idendity_no']                        = $dak_entity->national_idendity_no;
                $dataset['birth_registration_number']                        = $dak_entity->birth_registration_number;
                $dataset['passport']                        = $dak_entity->passport;
                $dataset['father_name']                        = $dak_entity->father_name;
                $dataset['mother_name']                        = $dak_entity->mother_name;
                $dataset['address']                        = $dak_entity->address;
                $dataset['parmanent_address']                        = $dak_entity->parmanent_address;
                $dataset['email']                        = $dak_entity->email;
                $dataset['phone_no']                        = $dak_entity->phone_no;
                $dataset['mobile_no']                        = $dak_entity->mobile_no;
                $dataset['gender']                        = $dak_entity->gender;
                $dataset['nationality']                        = $dak_entity->nationality;
                $dataset['religion']                        = $dak_entity->religion;
                $dataset['feedback_type']                        = $dak_entity->feedback_type;
                // Nagorik receive_date have to be same as previous
                $dataset['receive_date']                        = $dak_entity->receive_date;
                $dataset['nothi_master_id']                        = $dak_entity->nothi_master_id;
                $dataset['description']                        = $dak_entity->description;
                $dataset['application_meta_data']                        = $dak_entity->application_meta_data;

                return $dataset;
     }
    private function makeDaptorikTableDataset($dak_entity,$employee_office,$dak_receipt_no,$user)
    {
        

        $dataset['sender_office_id']                 = $employee_office['office_id'];
        $dataset['sender_officer_id']                = $employee_office['officer_id'];
        $dataset['sender_office_name']               = $employee_office['office_name'];
        $dataset['sender_office_unit_id']            = $employee_office['office_unit_id'];
        $dataset['sender_office_unit_name']          = $employee_office['office_unit_name'];
        $dataset['sender_officer_designation_id']    = $employee_office['office_unit_organogram_id'];
        $dataset['sender_officer_designation_label'] = $employee_office['designation_label'];
        $dataset['sending_date']                     = date("Y-m-d H:i:s");
        $dataset['sender_name']                      = $employee_office['officer_name'];
        $dataset['sender_address']                   = $employee_office['office_address'];
        $dataset['sender_email']                   = $dak_entity['sender_email'];
        $dataset['sender_phone']                   = $dak_entity['sender_phone'];
        $dataset['sender_mobile']                   = $dak_entity['sender_mobile'];

        $dataset['dak_sending_media'] = $dak_entity['dak_sending_media'];
       
      

        $dataset['receiving_date']     = date("Y-m-d H:i:s");
       
        
        $dataset['dak_cover']          = $dak_entity['dak_cover'];
        $dataset['dak_description']    = $dak_entity['dak_description'];
        $dataset['meta_data']           = $dak_entity['meta_data'];

        $dataset['is_summary_nothi'] = $dak_entity['is_summary_nothi'];
        
        return $dataset;
    }
}