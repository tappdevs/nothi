<?php

namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\Datasource\ConnectionManager;
use Cake\Controller\Component;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

class NothiUpdateTable extends ProjapotiTable
{

    public function initialize(array $config)
    {
        //parent::initialize($config);
        $conn = ConnectionManager::get('projapotiDb');
        $this->connection($conn);
        $this->table('nothi_updates');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
    }

}
