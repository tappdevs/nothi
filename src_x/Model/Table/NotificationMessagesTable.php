<?php

namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Network\Email\Email;

class NotificationMessagesTable extends ProjapotiTable
{
    private $notificationType = array(
        1 => 'Dak',
        2 => 'Nothi'
    );
    private $notificationMessage = array(
        'upload' => '%s টি %s উত্তোলন করা হয়েছে',
        'edit' => '%s টি %s সংশোধন করা হয়েছে',
        'sent' => '%s টি %s পাঠানো হয়েছে',
        'forward' => '%s টি %s পরবর্তী কার্যক্রমের জন্য পাঠানো হয়েছে',
        'nothivukto' => '%s টি %s ডাক নথিতে উপস্থাপন করা হয়েছে',
        'nothijat' => '%s টি %s ডাক নথিজাত করা হয়েছে',
        'nothiforward' => '%s টি  নথি এসেছে',
        'inbox' => '%s টি %s এসেছে',
        'draft' => '%s খসড়া তৈরি করা হয়েছে',
        'draftedit' => '%s টি খসড়া সংশোধন করা হয়েছে',
        'waiting' => '%s টি %s %s দিন ধরে সিদ্ধান্তের জন্য অপেক্ষা করছে',
        'potrojari' => '%s টি %s পত্রজারি করা হয়েছে',
    );
    private $notificationEnMessage = array(
        'upload' => '%s %s is uploaded successfully.',
        'edit' => '%s %s edited successfully.',
        'sent' => '%s %s sent successfully.',
        'forward' => '%s %s is forwarded successfully.',
        'nothivukto' => '%s %s Dak is put into Nothi successfully.',
        'nothijat' => '%s %s Dak is Nothijat successfully.',
        'nothiforward' => 'You have %s new Nothi ',
        'inbox' => 'You have %s new %s.',
        'draft' => '%s draft is saved successfully.',
        'draftedit' => '%s draft is updated successfully.',
        'waiting' => '%s %s are waiting for %s days.',
        'potrojari' => '%s %s is sent as Potrojari successfully.',
    );

    public function saveNotification($type, $options, $notificationtype, $employee = array(),
                                     $toUser = array(), $subject = '', $msg = '', $replace_msg = 0)
    {

        if (empty($toUser)) {
            $toUser = $employee;
        }
        if ($notificationtype == 'ডাক') {
            $notificationtype = 'Dak';
        }
        if ($notificationtype == 'নথি') {
            $notificationtype = 'Nothi';
        }

        $notificationEventsTable = TableRegistry::get('notificationEvents');

        $notificationevent = $notificationEventsTable->find()->select(['id'])->where(['notification_type' => $this->notificationType[$notificationtype] . "." . $type])->first();

        if (!empty($notificationevent)) {

            $notificationSettingsTable = TableRegistry::get('NotificationSettings');
            /*             * First Superman confirm to send user email and sms* */
            $SupermangetSettings = $notificationSettingsTable->find()->select(['event_id',
                'system',
                'email', 'sms', 'mobile_app'])->where(['employee_id' => 0,
                'office_id' => $toUser['office_id'], 'event_id' => $notificationevent['id']])->first();
            /*             * * */

            $getSettings = [];
            if (!empty($SupermangetSettings)) {
                $getSettings = $notificationSettingsTable->find()->select(['event_id',
                    'system',
                    'email', 'sms', 'mobile_app'])->where(['employee_id' => $toUser['officer_id'],
                    'office_id' => $toUser['office_id'], 'event_id' => $notificationevent['id']])->first();

                if (!empty($getSettings)) {
                    if ($SupermangetSettings['system'] == 0) {
                        $getSettings['system'] = 0;
                    }
                    if ($SupermangetSettings['email'] == 0) {
                        $getSettings['email'] = 0;
                    }
                    if ($SupermangetSettings['sms'] == 0) {
                        $getSettings['sms'] = 0;
                    }
                }

            }
        }

        if (!empty($getSettings)) {
            $BnOptions = [];
            if (!empty($options)) {
                foreach ($options as $option) {
                    if (is_numeric($option)) {
                        $BnOptions[] = enTobn($option);
                    } else {
                        $BnOptions[] = $option;
                    }
                }
            }
            $messageBng = sprintf($this->notificationMessage[$type], $BnOptions[0],
                isset($BnOptions[1]) ? __($BnOptions[1]) : '',
                isset($BnOptions[2]) ? __($BnOptions[2]) : '');

            $messageEng = sprintf($this->notificationEnMessage[$type], $options[0],
                isset($options[1]) ? $options[1] : '', isset($options[2]) ? $options[2] : '');
            $notificationt_entity = $this->newEntity();

            $notificationt_entity->event_id = $getSettings['event_id'];
            $notificationt_entity->event_type = $this->notificationType[$notificationtype];
            $notificationt_entity->message_bng = $messageBng;
            $notificationt_entity->message_eng = $messageEng;
            $notificationt_entity->from_user_id = isset($employee['officer_id']) ? $employee['officer_id']
                : 0;
            $notificationt_entity->from_office_id = isset($employee['office_id']) ? $employee['office_id']
                : 0;
            $notificationt_entity->to_user_id = $toUser['officer_id'];
            $notificationt_entity->to_office_id = $toUser['office_id'];
            $notificationt_entity->system = $getSettings['system'];
            $notificationt_entity->email = $getSettings['email'];
            $notificationt_entity->sms = $getSettings['sms'];
            $notificationt_entity->mobile_app = $getSettings['mobile_app'];
            $notificationt_entity->created = date("Y-m-d H:i:s");

            try {
                $this->save($notificationt_entity);
                $title = $this->notificationType[$notificationtype];

                if (!empty($toUser['officer_id'])) {
                    $employeeRecordTable = TableRegistry::get('EmployeeRecords');
                    $employeeRecord = $employeeRecordTable->find()->select(['name_bng', 'name_eng',
                        'personal_email', 'personal_mobile', 'alternative_mobile'])->where(['id' => $toUser['officer_id']])->first();

                    if ($getSettings['email'] == 1 && !empty($employeeRecord['personal_email'])) {

                        $this->dispatchCall((strtolower($this->notificationType[$notificationtype]) . ucfirst($type)),
                            $messageBng, $title, $employee, $employeeRecord, $subject, $msg,
                            $replace_msg);
                    }

                    if ($getSettings['sms'] == 1 && !empty($employeeRecord['personal_mobile'])) {
                        if ($this->sendNotificationSMS($messageEng, $employeeRecord)) {

                        }
                    }
                }
            } catch (\Exception $ex) {
                return false;
            }
        }
        return true;
    }

    private function dispatchCall($event, $notification, $title, $touser, $employee_record,
                                  $subject = '', $msg = '', $replace_msg = 0)
    {

        return $this->{$event}($notification, $title, $employee_record, $touser, $subject, $msg,
            $replace_msg);
    }

    private function dakUpload($notification, $title, $touser, $employee_record, $subject = ' ',
                               $msg = '', $replace_msg = 0)
    {
        
        if(empty($touser['personal_email'])){
            return false;
        }
        $notification_params = new \App\Model\Custom\NothiEmailer();
        $notification_params->notification = $notification;
        $notification_params->title = $title;
        $notification_params->to_email = $touser['personal_email'];
        $notification_params->to_name = isset($employee_record['officer_name']) ? $employee_record['officer_name'] : "";
        $notification_params->subject = $subject;
        $notification_params->employee_record = $employee_record;
        
        return $this->sendNotificationMail($notification_params);
    }

    private function dakSent($notification, $title, $employee, $touser, $subject = ' ', $msg = '',
                             $replace_msg = 0)
    {

        //done
        if(empty($touser['personal_email'])){
            return false;
        }
        $notification_params = new \App\Model\Custom\NothiEmailer();
        $notification_params->notification = $notification;
        $notification_params->title = $title;
        $notification_params->to_email = $touser['personal_email'];
        $notification_params->to_name = isset($employee['officer_name']) ? $employee['officer_name'] : "";
        $notification_params->subject = $subject;
        $notification_params->employee_record = $employee;
        
        return $this->sendNotificationMail($notification_params);
    }

    private function dakForward($notification, $title, $touser, $employee_record, $subject = "")
    {
        if(empty($touser['personal_email'])){
            return false;
        }
        $notification_params = new \App\Model\Custom\NothiEmailer();
        $notification_params->notification = $notification;
        $notification_params->title = $title;
        $notification_params->to_email = $touser['personal_email'];
        $notification_params->to_name = isset($employee_record['officer_name']) ? $employee_record['officer_name'] : "";
        $notification_params->subject = $subject;
        $notification_params->employee_record = $employee_record;
        
        return $this->sendNotificationMail($notification_params);
    }

    private function dakNothivukto($notification, $title, $touser, $employee_record, $subject = ' ',
                                   $msg = '', $replace_msg = 0)
    {
        if(empty($touser['personal_email'])){
            return false;
        }
        $notification_params = new \App\Model\Custom\NothiEmailer();
        $notification_params->notification = $notification;
        $notification_params->title = $title;
        $notification_params->to_email = $touser['personal_email'];
        $notification_params->to_name = isset($employee_record['officer_name']) ? $employee_record['officer_name'] : "";
        $notification_params->subject = $subject;
        $notification_params->employee_record = $employee_record;
        return $this->sendNotificationMail($notification_params);
    }

    private function dakNothijat($notification, $title, $touser, $employee_record, $subject = ' ',
                                 $msg = '', $replace_msg = 0)
    {
        //done
        if(empty($touser['personal_email'])){
            return false;
        }
        $notification_params = new \App\Model\Custom\NothiEmailer();
        $notification_params->notification = $notification;
        $notification_params->title = $title;
        $notification_params->to_email = $touser['personal_email'];
        $notification_params->to_name = isset($employee_record['officer_name']) ? $employee_record['officer_name'] : "";
        $notification_params->subject = $subject;
        $notification_params->employee_record = $employee_record;
        
        return $this->sendNotificationMail($notification_params);
    }

    private function dakWaiting($notification, $title, $touser, $employee_record, $subject = ' ',
                                $msg = '', $replace_msg = 0)
    {

    }

    private function dakInbox($notification, $title, $touser, $employee_record, $subject = ' ',
                              $msg = '', $replace_msg = 0)
    {

        //done
        if(empty($touser['personal_email'])){
            return false;
        }
        $name = (isset($employee_record['officer_name']) ? ($employee_record['officer_name'] . ', ') : '') . (isset($employee_record['designation_label'])
                ? ($employee_record['designation_label'] . ', ') : '') . (isset($employee_record['office_unit_name'])
                ? ($employee_record['office_unit_name'] . ', ') : '') . (isset($employee_record['office_name'])
                ? ($employee_record['office_name']) : '');

        $notification_params = new \App\Model\Custom\NothiEmailer();
        $notification_params->notification = $notification;
        $notification_params->title = $title;
        $notification_params->to_email = $touser['personal_email'];
        $notification_params->to_name = $name;
        $notification_params->subject = $subject;
        $notification_params->employee_record = $employee_record;
        
        return $this->sendNotificationMail($notification_params);
    }

    private function nothiInbox($notification, $title, $touser, $employee_record, $subject = ' ',
                                $msg = '', $replace_msg = 0)
    {

       $name = (isset($employee_record['officer_name']) ? ($employee_record['officer_name'] . ', ') : '') . (isset($employee_record['designation_label'])
                ? ($employee_record['designation_label'] . ', ') : '') . (isset($employee_record['office_unit_name'])
                ? ($employee_record['office_unit_name'] . ', ') : '') . (isset($employee_record['office_name'])
                ? ($employee_record['office_name']) : '');

        if(empty($touser['personal_email'])){
            return false;
        }
        $notification_params = new \App\Model\Custom\NothiEmailer();
        $notification_params->notification = $notification;
        $notification_params->title = $title;
        $notification_params->to_email = $touser['personal_email'];
        $notification_params->to_name = $name;
        $notification_params->subject = $subject;
        $notification_params->employee_record = $employee_record;
        
        //done
        return $this->sendNotificationMail($notification_params);
    }

    private function nothiSent($notification, $title, $touser, $employee_record, $subject = ' ',
                               $msg = '', $replace_msg = 0)
    {

        //done
        if(empty($touser['personal_email'])){
            return false;
        }
        $notification_params = new \App\Model\Custom\NothiEmailer();
        $notification_params->notification = $notification;
        $notification_params->title = $title;
        $notification_params->to_email = $touser['personal_email'];
        $notification_params->to_name = isset($employee_record['officer_name']) ? $employee_record['officer_name'] : "";
        $notification_params->subject = $subject;
        $notification_params->employee_record = $employee_record;
        
        return $this->sendNotificationMail($notification_params);
    }

    private function nothiDraft($notification, $title, $touser, $employee_record, $subject = ' ',
                                $msg = '', $replace_msg = 0)
    {
        if(empty($touser['personal_email'])){
            return false;
        }
        $notification_params = new \App\Model\Custom\NothiEmailer();
        $notification_params->notification = $notification;
        $notification_params->title = $title;
        $notification_params->to_email = $touser['personal_email'];
        $notification_params->to_name = isset($employee_record['officer_name']) ? $employee_record['officer_name'] : "";
        $notification_params->subject = $subject;
        $notification_params->employee_record = $employee_record;
        
        return $this->sendNotificationMail($notification_params);
    }

    private function nothiPotrojari($notification, $title, $employee_record, $touser,
                                    $subject = ' ', $msg = '', $replace_msg = 0)
    {

        if(empty($touser['personal_email'])){
            return false;
        }
        $notification_params = new \App\Model\Custom\NothiEmailer();
        $notification_params->notification = $notification;
        $notification_params->title = $title;
        $notification_params->to_email = $touser['personal_email'];
        $notification_params->to_name = isset($employee_record['officer_name']) ? $employee_record['officer_name'] : "";
        $notification_params->subject = $subject;
        $notification_params->employee_record = $employee_record;
        
        return $this->sendNotificationMail($notification_params);
        
    }

    private function sendNotificationMail($notification_params)
    {
        if (NOTIFICATION_OFF) {
            return true;
        }
        //Prepare Variables
        $notification_params = get_object_vars($notification_params);
        $data['to_email'] = $notification_params['to_email'];
        $data['to_name'] = !empty($notification_params['to_name']) ? $notification_params['to_name']: "";
        $data['from_email'] = !empty($notification_params['from_email']) ? $notification_params['from_email']: "nothi@nothi.org.bd";
        $data['from_name'] = !empty($notification_params['from_name']) ? $notification_params['from_name']: "নথি";;
        $data['format'] = !empty($notification_params['format']) ? $notification_params['format']: "html";
        $data['template'] = !empty($notification_params['template']) ? $notification_params['template']: "notification";
        $data['subject'] = $notification_params['title'] == 'Dak' ? 'ডাক' : ($notification_params['title'] == 'Nothi' ? 'নথি' : $notification_params['title']);
        $data['email_body'] = !empty($data['email_body']) ? $data['email_body'] : "";
        $data['layout'] = !empty($notification_params['layout']) ? $notification_params['layout']: "default";
        $data['viewVars'] = ['sender' => $data['to_email'],
                                'subject' =>$notification_params['title'] == 'Dak' ? 'ডাক' : ($notification_params['title'] == 'Nothi' ? 'নথি' : $notification_params['title']),
                                'body_subject' =>$data['subject'],
                                'notification' => $notification_params['notification'],
                                'notificationTime' => enTobn(date("Y-m-d H:i:s"))
                            ];
        //
        $data['office_id'] = $notification_params['employee_record']['office_id'];
        $data['office_name'] = isset($notification_params['employee_record']['office_name']) ? $notification_params['employee_record']['office_name']: "";
        $data['response_url'] = "";
        $data['email_trace_id'] = "";
        //
        $data['service_id'] = ($notification_params['title'] == 'Dak' ? EMAIL_DAK : ($notification_params['title'] == 'Nothi' ? EMAIL_NOTHI : $notification_params['title']));
        try{
            if(defined("EMAILER_ACTIVE") && EMAILER_ACTIVE==1) {
                $nothi_email = new \App\Model\Entity\NothiEmail();
                $nothi_email->addEmailQueue($data);
            }else{
                $email = new Email('default');
                $email->emailFormat($data['format'])->from([$data['from_email'] => $data['from_name']])
                    ->to(trim($data['to_email']))
                    ->subject($data['subject'])
                    ->viewVars($data['viewVars'])
                    ->template($data['template'], $data['layout'])
                    ->sendWithoutException();
            }

            return true;
        }catch(Exception $e){
            return false;
        }
        return true;
    }
    
    private function sendNotificationSMS($notification, $employee_record)
    {
        if (NOTIFICATION_OFF) {
            return true;
        }
        if (empty($employee_record['personal_mobile']) || empty($employee_record['name_eng'])) {
            return true;
        }
       
        $ms  = bnToen($employee_record['personal_mobile']);
        $txt = "Dear ".$employee_record['name_eng'].",\n\r".$notification."\n\rThank you.";
       
        if (substr($ms, 0, 3) != '+88') {
            $ms = '+88'.$ms;
        }
        try{
            $SmsRequestTable = TableRegistry::get('SmsRequest');
            $SmsRequestTable->saveData($ms,$txt);
        } catch (\Exception $ex) {
        }
       
        return true;
    }

    public function getNewNotification($employee_record_id, $type = 'mobile_app')
    {
        $totalNotification = $this->find()->select([
            'NotificationMessages.event_type',
            'NotificationMessages.event_id'
        ])->where(['is_notified' => 0, 'to_user_id' => $employee_record_id, $type => 1])->toArray();

        $this->updateAll(['is_notified' => 1],
            ['is_notified' => 0, 'to_user_id' => $employee_record_id, $type => 1]);
        return $totalNotification;
    }
}