<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use App\Model\Table\ProjapotiTable;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;
/**
 * Office Model
 */
class OfficeDomainsTable extends ProjapotiTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $conn = ConnectionManager::get('projapotiDb');
        $this->connection($conn);
        $this->entityClass('OfficeDomain');
        $this->displayField('domain_url');
        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->requirePresence('id', 'update')
            ->notEmpty('id');

        $validator
            ->requirePresence('domain_url')
            ->notEmpty('domain_url');

        $validator
            ->requirePresence('domain_prefix')
            ->notEmpty('domain_prefix');

        $validator
            ->allowEmpty('modified_by');

        return $validator;
    }
    public function getOfficesData($office = 0){
      $query =  $this->find('list', ['keyField' => 'Offices.id', 'valueField' => 'office_name_bng'])->select(['Offices.id','office_name_bng' => 'concat(Offices.office_name_bng," (",Offices.office_name_eng,")")'])->join([
            "Offices" => [
                'table' => 'offices', 'type' => 'inner',
                'conditions' => ['Offices.id =OfficeDomains.office_id']
            ]
        ])->where(['OfficeDomains.status' => 1]);

      if(!empty($office)){
          $query = $query->where(['OfficeDomains.office_id'=>$office]);
      }

      return $query->toArray();
    }

    public function getDomainbyOffice($office_id = 0){
        
        return $this->find()->where(['office_id'=>$office_id])->first();
    }

    public function getPortalDomainbyOffice($office_id = 0){

        $office_domain_info= $this->find()->where(['office_id'=>$office_id])->first();
        return str_replace('.nothi','',$office_domain_info['domain_url']);

    }
    public function getNothiReplacedPortalDomainbyOffice($office_id = 0){

        $office_domain_info= $this->find()->where(['office_id'=>$office_id])->first();
        return str_replace('.nothi','.portal',$office_domain_info['domain_url']);

    }

    public function getPortalDomainByFieldOffice($office_id = 0){

        $office_domain_info= $this->find()->where(['office_id'=>$office_id])->first();
        return str_replace('.nothi','',$office_domain_info['domain_url']);

    }

    public function getApiPortalDomainbyOffice($office_id = 0){

        $office_domain_info= $this->find()->where(['office_id'=>$office_id])->first();
        $office_domain = str_replace('.nothi','.portal',$office_domain_info['domain_url']);
        $office_domain = str_replace('http://', '', $office_domain);
        $office_domain = str_replace('https://', '', $office_domain);
        return $office_domain;

    }
    public function getUniqueDomainHost(){
       return $this->find('list',['keyField' => 'id','valueField' => 'domain_host'])->distinct(['domain_host'])->toArray();
    }
    public function getHostWiseOfficeID($host){
         return $this->find('list',['valueField' => 'office_id'])->where(['domain_host' => $host])->toArray();
    }
}
