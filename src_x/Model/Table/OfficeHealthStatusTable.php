<?php
namespace App\Model\Table;

use App\Model\Entity\OfficeHealthStatus;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * OfficeHealthStatus Model
 */
class OfficeHealthStatusTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $conn = ConnectionManager::get('projapotiDb');
        $this->connection($conn);
        $this->table('office_health_status');
        $this->displayField('has_error');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->belongsTo('Offices', [
            'foreignKey' => 'office_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');
            
        $validator
            ->add('has_error', 'valid', ['rule' => 'numeric'])
            ->requirePresence('has_error', 'create')
            ->notEmpty('has_error');
            
        $validator
            ->allowEmpty('health_report');
            
        $validator
            ->add('check_date', 'valid', ['rule' => 'date'])
            ->requirePresence('check_date', 'create')
            ->notEmpty('check_date');
            
        $validator
            ->add('last_check', 'valid', ['rule' => 'datetime'])
            ->allowEmpty('last_check');
            
        $validator
            ->allowEmpty('created_by');
            
        $validator
            ->allowEmpty('modified_by');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['office_id'], 'Offices'));
        return $rules;
    }

    public function getByOffice($office_id = 0, $date = null){
        $data = $this->find()->where(['office_id'=>$office_id]);
        if(!empty($date)){
            $data  = $data->where(['check_date'=>$date]);
        }
        return $data->first();
    }

    public function getHealthStatus($office_id, $user = []){

        $data = $this->getByOffice($office_id, date("Y-m-d"));

        if(empty($data)){
            $data = curlRequest(DB_CHECK_URL .  $office_id, $user);
            $json = jsonA($data);
            $entity = $this->newEntity();
            $entity->office_id = $office_id;
            $entity->has_error = !empty($json)?$json['error']:0;
            $entity->health_report = $data;
            $entity->check_date = date("Y-m-d");
            $entity->last_check = date("Y-m-d H:i:s");
            $entity->created = date("Y-m-d H:i:s");
            $entity->modified = date("Y-m-d H:i:s");
            $this->save($entity);
            return $json;
        }else{
            if($data['has_error']){
                $data = curlRequest(DB_CHECK_URL .  $office_id , $user);
                $json = jsonA($data);
                if(!empty($json) && $json['error'] == 0){
                    $this->updateAll(['has_error'=>0,'health_report'=>'','last_check'=>date("Y-m-d H:i:s")],['office_id'=>$office_id]);
                }
                return $json;
            }else{

            }
        }

        return ['status'=>200,'error'=>0,'report'=>''];
    }


}
