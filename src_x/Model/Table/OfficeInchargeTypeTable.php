<?php
namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

/**
 * OfficeMinistry Model
 */
class OfficeInchargeTypesTable extends ProjapotiTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->displayField('name_bng');
        $this->addBehavior('Timestamp');
    }

}
