<?php

namespace App\Model\Table;

use App\Model\Entity\DakType;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use App\Model\Table\ProjapotiTable;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;
use Cake\Cache\Cache;

/**
 * OfficeLayer Model
 */
class OfficeLayersTable extends ProjapotiTable
{

    public $layerLavel = [];
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $conn = ConnectionManager::get('projapotiDb');
        $this->connection($conn);

        $this->table('office_layers');
        $this->displayField('layer_name_bng');
        $this->addBehavior('Timestamp');
        $this->belongsTo('OfficeMinistries',
            [
            'foreignKey' => 'office_ministry_id',
            'joinType' => 'INNER',
            'className' => 'OfficeMinistries',
        ]);
        $this->setData();
    }
    public function setData(){
        $this->layerLavel = array_flip(jsonA(OFFICE_LAYER_TYPE));
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->requirePresence('id', 'update')
            ->notEmpty('id');

        $validator
            ->requirePresence('layer_name_bng')
            ->notEmpty('name');

        $validator
            ->requirePresence('layer_name_eng')
            ->notEmpty('name');

        $validator
            ->allowEmpty('modified_by');

        return $validator;
    }

    public function get($primaryKey, $options = array())
    {

        if (!empty($options)) {
            parent::get($primaryKey, $options);
        } else {
            return $this->find()->where(['id' => $primaryKey, 'OfficeLayers.active_status' => 1])->first();
        }
    }

    public function getAll($conditions = '', $select = '')
    {

        $queryString = $this->find();

        if (!empty($select)) {
            $queryString = $queryString->select($select);
        }

        if (!empty($conditions)) {
            $queryString = $queryString->where($conditions);
        }

        return $queryString->where(['OfficeLayers.active_status' => 1]);
    }

    public function getBanglaName($id)
    {
        return $this->find()->select(['layer_name_bng'])->where(['id' => $id])->first();
    }

    public function getLayerwiseOfficeEmployeeCount($officelayertype = '',$ministry_id = 0)
    {

        $office_ministry_table = \Cake\ORM\TableRegistry::get('OfficeMinistries');
        $officelist            = $office_ministry_table->find('list',
                ['keyField' => 'Offices.id', 'valueField' => 'Offices.office_name_bng'])->join([
            'OfficeLayers' => [
                'table' => 'office_layers',
                'type' => 'inner',
                'conditions' => "OfficeLayers.office_ministry_id = OfficeMinistries.id"
            ],
            'OfficeOrigins' => [
                'table' => 'office_origins',
                'type' => 'inner',
                'conditions' => "OfficeLayers.id = OfficeOrigins.office_layer_id"
            ],
            "Offices" => [
                'table' => 'offices',
                'type' => 'inner',
                'conditions' => 'OfficeOrigins.id = Offices.office_origin_id'
            ]
        ]);

        if(!empty($officelayertype)){
            if ($officelayertype == 'Other') {
                $condition  = 'OfficeLayers.layer_level > 6 OR  OfficeLayers.layer_level < 1';
                $officelist = $officelist->where([ $condition]);
            } else {
                $officelist = $officelist->where(['OfficeLayers.layer_level' => $this->layerLavel[$officelayertype]]);
            }
        }
        if(!empty($ministry_id)){
            $officelist = $officelist->where(['OfficeMinistries.id' => $ministry_id]);
        }

        $data = $officelist->select(['Offices.id', 'Offices.office_name_bng'])->distinct(['Offices.id'])->where(['Offices.active_status' => 1])->toArray();

        return $data;
    }

    public function allLayers($ministry_id){
        return $this->find('list',['keyField'=>'id','valueField'=>'layer_name_bng'])->where(['office_ministry_id' => $ministry_id])->toArray();
    }
    public function getLayerLevel($layer_id){
        return $this->find()->where(['id' => $layer_id])->first();
    }
}