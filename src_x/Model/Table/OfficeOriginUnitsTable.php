<?php
namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use  Cake\Datasource\ConnectionManager;

class OfficeOriginUnitsTable extends ProjapotiTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $conn = ConnectionManager::get('projapotiDb');
        $this->connection($conn);

        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->displayField('unit_name_bng');
    }


    public function getOriginInfo($origin = 0)
    {

        if (!empty($origin)) {
            return $this->find()->where(['office_origin_id' => $origin, 'active_status' => 1])->count();
        }

        return 0;
    }
    
    public function getOfficeFullInfo($officeunitid)
    {
        $conn = ConnectionManager::get('projapotiDb');
        $stmt = $conn->query ( 'SELECT  group_concat(@id :=
        (
        SELECT  parent_unit_id
        FROM    office_origin_units
        WHERE   id = @id
        )) AS office_units
FROM    (
        SELECT  @id := ' . $officeunitid . '
        ) vars
STRAIGHT_JOIN
        office_origin_units
WHERE   @id IS NOT NULL' );

        $stmt->execute ();

        return $stmt->fetch ();
    }
  public function getBanglaName($id)
    {
        return $this->find()->select(['unit_name_bng'])->where(['id'=>$id])->first();
    }

}