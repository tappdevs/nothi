<?php
namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;


class OfficeRecordsTable extends ProjapotiTable
{

    /**
     * Autocompelte office data
     * @param unknown $search_key
     */
    public function loadOfficeAutocompleteData($search_key)
    {
        return $this->find('all')
            ->select(['id', 'office_name_bng', 'office_address'])
            ->where(['office_name_bng LIKE' => '%' . $search_key . '%']);
    }
}