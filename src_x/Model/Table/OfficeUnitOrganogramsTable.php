<?php

namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;

class OfficeUnitOrganogramsTable extends ProjapotiTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $conn = ConnectionManager::get('projapotiDb');
        $this->connection($conn);
        $this->primaryKey('id');
        $this->displayField('designation_bng');
        $this->addBehavior('Timestamp');

        $this->belongsTo('OfficeUnits', [
            'foreignKey' => 'office_unit_id',
            'joinType' => 'INNER',
            'sort'=>'unit_level ASC'
        ]);
    }

    public function getUnitOrgByOfficeId($origin_unit_id)
    {
        return $this->find()->where(['office_origin_unit_id' => $origin_unit_id,
            'status' => 1])->order(['designation_level ASC'])->toArray();
    }

    public function getUnitOrgByOfficeUnitId($office_unit_id, $status = 1)
    {
        $query = $this->find('list')->where(['office_unit_id' => $office_unit_id]);

        if ($status != -1) {
            $query->where(['status' => $status]);
        }

        return $query->order(['designation_level ASC'])->toArray();
    }

    public function getUnusedUnitOrgByOfficeUnitId($office_unit_id, $office_id)
    {
        return $this->find('list')->join([
            'EmployeeOffices' => [
                'table' => 'employee_offices',
                'type' => 'LEFT',
                'conditions' => 'EmployeeOffices.office_unit_organogram_id = OfficeUnitOrganograms.id AND EmployeeOffices.office_id = OfficeUnitOrganograms.office_id AND EmployeeOffices.status = 1'
            ]
        ])->where(['OfficeUnitOrganograms.office_id' => $office_id, 'OfficeUnitOrganograms.office_unit_id' => $office_unit_id,
            'OfficeUnitOrganograms.status' => 1])->where('EmployeeOffices.id IS NULL')->order(['OfficeUnitOrganograms.designation_level ASC'])->toArray();
    }

    public function getUnitOrganogramInfo($id)
    {
        if (!empty($id)) {
            return $this->find()->where(['ref_origin_unit_org_id' => $id, 'status' => 1])->toArray();
        }

        return 0;
    }

    public function getAll($conditions = null, $select = null)
    {

        $queryString = $this->find()->where(['OfficeUnitOrganograms.status' => 1]);

        if (!empty($select)) {
            $queryString = $queryString->select($select);
        }

        if (!empty($conditions)) {
            $queryString = $queryString->where($conditions);
        }
        return $queryString;
    }

    public function getDesignationsByUnitId($unitId = 0)
    {
        if (empty($unitId)) return;

        return $this->find('list')->join([
            "EmployeeOffices" => [
                'table' => 'employee_offices', 'type' => 'inner',
                'conditions' => ['EmployeeOffices.office_unit_organogram_id = OfficeUnitOrganograms.id', 'EmployeeOffices.office_unit_id = OfficeUnitOrganograms.office_unit_id', 'EmployeeOffices.office_id = OfficeUnitOrganograms.office_id']
            ]
        ])->where(['OfficeUnitOrganograms.office_unit_id' => $unitId, 'EmployeeOffices.status' => 1])->order(['OfficeUnitOrganograms.designation_level ASC','OfficeUnitOrganograms.designation_sequence ASC'])->toArray();
    }

    public function getAllDesignationsByUnitId($unitId = 0)
    {
        if (empty($unitId)) return;

        return $this->find('list')->join([
            "EmployeeOffices" => [
                'table' => 'employee_offices', 'type' => 'inner',
                'conditions' => ['EmployeeOffices.office_unit_organogram_id = OfficeUnitOrganograms.id', 'EmployeeOffices.office_unit_id = OfficeUnitOrganograms.office_unit_id', 'EmployeeOffices.office_id = OfficeUnitOrganograms.office_id']
            ]
        ])->where(['OfficeUnitOrganograms.office_unit_id' => $unitId])->group(['EmployeeOffices.office_unit_organogram_id'])->order(['OfficeUnitOrganograms.designation_level ASC','OfficeUnitOrganograms.designation_sequence ASC'])->toArray();
    }

    /*
     * @return Object of Designation Name English and Bangla
     */
    public function allDesignationName($data = [])
    {
        $query = $this->find()->select(['OfficeUnitOrganograms.id', 'd_eng' => 'OfficeUnitOrganograms.designation_eng',
            'd_bng' => 'OfficeUnitOrganograms.designation_bng','office'=>'OfficeUnitOrganograms.office_id',
            'unit'=>'OfficeUnitOrganograms.office_unit_id','EmployeeRecords.name_bng','EmployeeRecords.name_eng']);

        if(!empty($data['search'])){
            $data['search'] = h($data['search']);
            $query= $query->where(["(OfficeUnitOrganograms.designation_eng like '%{$data['search']}%' OR OfficeUnitOrganograms.designation_bng like '%{$data['search']}%')"]);
        }
        if(!empty($data['office_id'])){
            $query= $query->where(['OfficeUnitOrganograms.office_id' => $data['office_id']]);
        }

        if(!empty($data['unit_id'])){
            $query = $query->where(['OfficeUnitOrganograms.office_unit_id' => $data['unit_id']]);
        }

        $query->join([
            'EmployeeOffices'=>[
                'type'=>'LEFT',
                'table'=>'employee_offices',
                'conditions' => "EmployeeOffices.office_unit_organogram_id = OfficeUnitOrganograms.id AND EmployeeOffices.status = 1"
            ],
            'EmployeeRecords'=>[
                'type'=>'LEFT',
                'table'=>'employee_records',
                'conditions' => "EmployeeOffices.employee_record_id = EmployeeRecords.id"
            ]
        ]);

        return $query->order(['EmployeeOffices.designation_level ASC,EmployeeOffices.designation_sequence ASC']);
    }

    public function getDesignationByOfficeId($office_id = 0)
    {
        if (empty($office_id))
            return;
        return $this->find('list')->where(['office_id' => $office_id]);
    }

    public function getBanglaName($id)
    {
        return $this->find()->select(['designation_bng'])->where(['id' => $id])->first();
    }

    public function getEnglishName($id)
    {
        return $this->find()->select(['designation_eng'])->where(['id' => $id])->first();
    }

    public function getDesignationUnitName($id)
    {
        return $this->find()->select(['designation_bng',"unit_name_bng"=>'OfficeUnits.unit_name_bng'])->contain(['OfficeUnits'])->where(['OfficeUnitOrganograms.id' => $id])->first();
    }
    /**
     *
     * @param type $designation_ids
     * @param type $select
     * @param type $list
     * @param type $list_fields -> ['keyField' => '','valueField' => '']
     * @return type
     */
    public function getAllDesignationsByDesignationIds($designation_ids = [],$select = [],$list = 0,$list_fields = [])
    {
        if (empty($designation_ids)) return [];
        if(!empty($list)){
             if(!empty($list_fields)){
                 $query = $this->find('list',$list_fields);
             }else{
                 $query = $this->find('list');
             }
        }else{
             $query = $this->find();
        }
        if(!empty($select)){
            $query->select($select);
        }
        return $query->where(['OfficeUnitOrganograms.id IN' => $designation_ids]);
    }

	public function getAllOrganogramsByUnitId($unitId = 0)
	{
		if (empty($unitId)) return;

		/*$query = $this->find()->select(['OfficeUnitOrganograms.id', 'd_eng' => 'OfficeUnitOrganograms.designation_eng',
			'd_bng' => 'OfficeUnitOrganograms.designation_bng','office'=>'OfficeUnitOrganograms.office_id',
			'unit'=>'OfficeUnitOrganograms.office_unit_id','OfficeUnits.unit_name_bng']);*/
		//$query = $query->where(['OfficeUnitOrganograms.office_unit_id' => $unitId, 'OfficeUnitOrganograms.status' => 1]);

		$query = $this->find()->select(['id']);
		$query = $query->where(['office_unit_id' => $unitId, 'status' => 1]);

		/*$query->join([
			'OfficeUnits'=>[
				'type'=>'LEFT',
				'table'=>'office_units',
				'conditions' => "OfficeUnitOrganograms.office_unit_id = OfficeUnits.id"
			]
		]);*/

		return $query;
	}
	public function employee_office($id) {
		$employee_offices = TableRegistry::get('EmployeeOffices')->find()->where(['office_unit_organogram_id' => $id])->order(['status' => 'desc'])->first();
		if (count($employee_offices) > 0) {
			return $employee_offices->toArray();
		} else {
			return false;
		}
	}
	public function employee_record($id) {
		$employee_records = TableRegistry::get('EmployeeRecords')->get($id);
		if (count($employee_records) > 0) {
			return $employee_records->toArray();
		} else {
			return false;
		}
	}
}