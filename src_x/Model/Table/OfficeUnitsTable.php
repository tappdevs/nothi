<?php
namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;
use Cake\Cache\Cache;

class OfficeUnitsTable extends ProjapotiTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $conn = ConnectionManager::get('projapotiDb');
        $this->connection($conn);
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->displayField('unit_name_bng');

        $this->hasMany('Designations', [
            'className'=>'OfficeUnitOrganograms',
            'foreignKey' => 'office_unit_id',
            'joinType' => 'INNER',
            'sort' => 'designation_level asc',
            'conditions'=>'Designations.status = 1'
        ]);

        $this->hasMany('OfficeUnitOrganograms', [
            'foreignKey' => 'office_unit_id',
            'joinType' => 'INNER',
            'sort' => 'designation_level asc',
            'conditions'=>'OfficeUnitOrganograms.status = 1'
        ]);
    }
    
    public function getNameWithOffice($unit_id){
        
        return $this->find()->select(['unit_name_bng',"office_name_bng"=>'Offices.office_name_bng'])->where(['OfficeUnits.id'=>$unit_id])->join([
            'Offices'=>[
                'table'=>'offices',
                'conditions'=>'Offices.id = OfficeUnits.office_id'
            ]
        ])->first();
    }


    public function getOfficeUnitBy_designationId($designation_id)
    {
        $table_instance_ouo = TableRegistry::get('OfficeUnitOrganograms');
        $OfficeUnitOrganograms = $table_instance_ouo->find()->select(['office_unit_id'])->where(['id' => $designation_id])->first();
        $unit_id = $OfficeUnitOrganograms['office_unit_id'];

        return $this->find()->select(['id', 'unit_name_bng'])->where(['id' => $unit_id])->first();
    }

    public function getOfficeUnitsList($office_id, $id = 0)
    {
        if(!empty($id)){
            return $this->find('list')->where(['office_id' => $office_id, 'active_status' => 1,'id'=>$id])->toArray();
        }
          if (($units = Cache::read('getOfficeUnitsList_' . $office_id, 'memcached')) === false) {
                $units = $this->find('list')->where(['office_id' => $office_id, 'active_status' => 1])->order(['unit_level asc, parent_unit_id asc'])->toArray();
                Cache::write('getOfficeUnitsList_' . $office_id, $units, 'memcached');
            }
            return $units;
    }
    public function getOfficeUnitsinfo($office_id, $id = 0)
    {
        if(!empty($id)){
            return $this->find()->select(['id','unit_name_bng','unit_name_eng'])->where(['office_id' => $office_id, 'active_status' => 1,'id'=>$id])->toArray();
        }
        return $this->find()->select(['id','unit_name_bng','unit_name_eng'])->where(['office_id' => $office_id, 'active_status' => 1])->order(['unit_level asc, parent_unit_id asc','id desc'])->toArray();
    }

    public function getInfoByOfficeId($office_id)
    {
        if (!empty($office_id) && $this->find()->where(['office_id' => $office_id, 'active_status' => 1])->count()) {
            return 1;
        }

        return 0;
    }

    public function getInfoByOfficeUnitId($id)
    {
        if (!empty($id)) {
            return $this->find()->where(['office_origin_unit_id' => $id, 'active_status' => 1])->order(['unit_level asc, parent_unit_id asc'])->toArray();
        }

        return 0;
    }

    public function getOfficeFullInfo($officeunitid)
    {
        $conn = ConnectionManager::get('projapotiDb');
        $stmt = $conn->query ( 'SELECT  group_concat(@id :=
        (
        SELECT  parent_unit_id
        FROM    office_units
        WHERE   id = @id
        )) AS office_units
FROM    (
        SELECT  @id := ' . $officeunitid . '
        ) vars
STRAIGHT_JOIN
        office_units
WHERE   @id IS NOT NULL' );

        $stmt->execute ();

        return $stmt->fetchAll ();
    }
    
    public function getOfficeChildInfo($officeunitid)
    {
        $conn = ConnectionManager::get('projapotiDb');
        $stmt = $conn->query ( 'SELECT group_concat(p.id) FROM office_units c INNER JOIN office_units p ON c.id = p.parent_unit_id WHERE c.parent_unit_id = ' . $officeunitid . ' order by c.unit_level, c.parent_unit_id asc, c.id asc' );

        $stmt->execute ();

        return $stmt->fetch ();
    }
    
    public function getOwnChildInfo($officeunitid)
    {
        $conn = ConnectionManager::get('projapotiDb');
        $stmt = $conn->query ( 'SELECT group_concat(c.id) FROM office_units c WHERE c.parent_unit_id = ' . $officeunitid . ' order by c.unit_level, c.parent_unit_id asc, c.id asc' );

        $stmt->execute ();

        return $stmt->fetch ();
    }
    
    
    public function getAll($conditions = '', $select=''){
        
        $queryString = $this->find()->where(['active_status' => 1]);
        
        if(!empty($select)){
            $queryString = $queryString->select($select);
        }
        
        if(!empty($conditions)){
            $queryString = $queryString->where($conditions);
        }
        $queryString->order(['unit_level asc, parent_unit_id asc, id asc']);
        return $queryString;
    }
    
    public function getOrderwiseUnits($unit_ids)
    {
//        pr($unit_ids);
        return $this->getAll(["id IN ({$unit_ids})"],['id'])->order(['unit_level asc, parent_unit_id asc, id asc'])->find('list')->hydrate(false)->toArray();
    }

    public function getOwnChildIdandName($officeunitid)
    {
        return $this->find('list',['keyField' => 'id','valuField' => 'unit_name_bng'])->where(['parent_unit_id' => $officeunitid])->order(['unit_level', 'parent_unit_id','id' ])->toArray();
    }


    public function getUnitWiseDesignation($office_id, $unit_id = 0){

        $query = $this->find()->contain(['OfficeUnitOrganograms'])->where(['OfficeUnits.office_id'=>$office_id,'OfficeUnits.active_status'=>1]);

        if(!empty($unit_id)){
            $query->where(['OfficeUnits.id'=>$unit_id]);
        }

        $query->order(['unit_level asc, parent_unit_id asc, id asc']);
        return $query;
    }
  public function getBanglaName($id)
    {
        return $this->find()->select(['unit_name_bng'])->where(['id'=>$id])->first();
    }
  public function getEnglishName($id)
    {
        return $this->find()->select(['unit_name_eng', 'unit_name_bng'])->where(['id'=>$id])->first();
    }
    public function getUnitsInfoByOfficeId($office_id, $status = 0)
    {
        if(!empty($status)){
            $query= $this->find()->where(['office_id' => $office_id, 'active_status' => $status]);
        }else{
            $query = $this->find()->where(['office_id' => $office_id]);
        }
        return $query->order(['unit_level asc, parent_unit_id asc']);
       
    }
}   

