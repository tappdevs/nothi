<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;
/**
 * Office Model
 */
class OfficesTable extends ProjapotiTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $conn = ConnectionManager::get('projapotiDb');
        $this->connection($conn);

        $this->displayField('office_name_bng');
        $this->addBehavior('Timestamp');

        $this->hasOne('OfficeDomains', [
            'foreignKey' => false,
            'type' => 'leftJoin',
            'conditions' => 'Offices.id = OfficeDomains.office_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->requirePresence('id', 'update')
            ->notEmpty('id')
            ->requirePresence('office_name_bng')
            ->notEmpty('name')
            ->notEmpty('digital_nothi_code')
            ->requirePresence('office_name_eng')
            ->requirePresence('digital_nothi_code')
//            ->add('digital_nothi_code', [
//                'length' => [
//                    'rule' => ['minLength', 10],
//                    'message' => 'ডিজিটাল নথি কোড ৮ ডিজিটের হতে হবে',
//                ]
//            ])
            ->allowEmpty('modified_by');

        return $validator;
    }

    public function getOfficesByMinistry($ministry_id)
    {
        return $this->find('list')->where(['office_ministry_id' => $ministry_id, 'active_status' => 1])->toArray();
    }

    public function getOfficesByOrigin($origin_id)
    {
        return $this->find('list')->where(['office_origin_id' => $origin_id, 'active_status' => 1])->toArray();
    }

    public function getOfficesByid($id)
    {
        return $this->find('list')->where(['id' => $id, 'active_status' => 1])->toArray();
    }

    public function getMinistryInfo($ministy_id)
    {
        if (!empty($ministy_id) && $this->find()->where(['office_ministry_id' => $ministy_id, 'active_status' => 1])->count()) {
            return 1;
        }

        return 0;
    }
    
     
    public function getAll($conditions = '', $select=''){
        
        $queryString = $this->find();
        
        if(!empty($select)){
            $queryString = $queryString->select($select);
        }
        
        if(!empty($conditions)){
            $queryString = $queryString->where($conditions);
        }
                
        return $queryString;
    }
    
    public function getOfficeFullInfo($officeid)
    {
        $conn = ConnectionManager::get('projapotiDb');
        $stmt = $conn->query ( 'SELECT  group_concat(@id :=
        (
        SELECT  parent_office_id
        FROM    offices
        WHERE   id = @id AND parent_office_id > 0
        )) AS offices
FROM    (
        SELECT  @id := ' . $officeid . '
        ) vars
STRAIGHT_JOIN
        offices
WHERE   @id IS NOT NULL' );

        $stmt->execute ();

        return $stmt->fetch();
    }
    
    public function getChildOffices($office_id=0)
    {
        return $this->find()->where(['parent_office_id'=>$office_id,'active_status' => 1])->toArray();
    }

    public function getBanglaName($id)
    {
        return $this->find()->select(['office_name_bng'])->where(['id'=>$id])->first();
    }
    public function getEnglishName($id)
    {
        return $this->find()->select(['office_name_eng'])->where(['id'=>$id])->first();
    }
    public function getOfficeName($id)
    {
        return $this->find()->select(['office_name_eng','office_name_bng'])->where(['id'=>$id])->first();
    }

    public function getIdandNameofChildOffices($office_id=0)
    {
        return $this->find('list')->where(['parent_office_id'=>$office_id,'active_status' => 1])->toArray();
    }
    public function getOfficeListByDomain($domain_name = '')
    {
        return $this->find('list')->where(['office_name_bng LIKE'=>"%".$domain_name."%",'active_status' => 1])->orWhere(['office_name_eng LIKE'=>"%".$domain_name."%",'active_status' => 1])->order(['parent_office_id asc,office_name_bng asc'])->toArray();
    }
     public function getOfficeAddress($id)
    {
        return $this->find()->select(['office_address'])->where(['id'=>$id])->first();
    }
    public function getCustomLayerInfo($custom_layer_id)
    {
        return $this->find()->where(['custom_layer_id'=>$custom_layer_id]);
    }

    public function getOfficesByDistrict($request)
    {
        $district_id = $request['district_id'];
        $active_status_id = $request['active_status_id'];
        $mapping_status_id = $request['mapping_status_id'];
        $office_name = $request['office_name']; 

        // $officeDomainTable = TableRegistry::get('OfficeDomains');
        $officeList = $this->find()->contain(['OfficeDomains']);

        if(!empty($district_id))
        {
            $officeList->where(['geo_district_id'=>$district_id]);
        }
        if(!empty($active_status_id) && $active_status_id==1 )
        {
            $officeList->where(['active_status'=>$active_status_id]);
        }elseif(!empty($active_status_id) && $active_status_id==2 )
        {
            $officeList->where(['active_status'=>0]);
        }
        if(!empty($office_name))
        {
            $officeList->where(['office_name_bng like '=>'%'.$office_name.'%']);
        }
        if(!empty($mapping_status_id) && $mapping_status_id == 1)
        {
            $officeList->where(['OfficeDomains.office_id > '=> 0]);
        }
        if(!empty($mapping_status_id) && $mapping_status_id == 2)
        {
            $officeList->where([' OfficeDomains.office_id is '=> null ]);
        }
        
        return $officeList;
    }
}
