<?php

namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\I18n\Number;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

class OtherOrganogramActivitiesSettingsTable extends ProjapotiTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
    }
}
