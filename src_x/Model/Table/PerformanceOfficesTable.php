<?php

namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\Datasource\ConnectionManager;
use Cake\Controller\Component;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

class PerformanceOfficesTable extends ProjapotiTable
{

    public function initialize(array $config)
    {
        //parent::initialize($config);
        $conn = ConnectionManager::get('NothiReportsDb');
        $this->connection($conn);
        $this->table('performance_offices');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
    }

    public function checkUpdate($office_id = 0, $date = '')
    {
        if (!empty($date) && !empty($office_id)) {
            return $this->find()->where(['record_date' => $date, 'office_id' => $office_id])->count();
        }
    }
    public function checkNothiUpdate($office_id = 0, $date = '')
    {
        if (!empty($date) && !empty($office_id)) {
            return $this->find()->where(['record_date' => $date, 'office_id' => $office_id,'updated' => 0])->count();
        }
    }
    /**
     *
     * @param int $office_id
     * @param DateTime $time
     * @return object  CollectionBetweenTwoDates
     */
    public function getOfficeData($office_id = 0, $time = [])
    {
        if (!empty($office_id)) {
            $query = $this->find()->where(['office_id' => $office_id]);
            if (!empty($time)) {
               if (!empty($time[0])) {
                    $query = $query->where(['record_date >=' => $time[0]]);
                }
                if (!empty($time[1])) {
                    $query = $query->where(['record_date <=' => $time[1]]);
                }
            }
//            pr($query);die;
            return $query;
        }
        return;
    }
    public function getLastUpdateTime($office_id = 0){
        $query =  $this->find()->select(['record_date'])->order(['record_date desc']);
        if(!empty($office_id)){
            $query = $query->where(['office_id' => $office_id]);
        }
    return $query->first();
    }
    public function getLastOnisponno($office_id = 0,$date = ''){
        $query =  $this->find()->select(['onisponnodak' , 'onisponnonote', 'unassignedPendingDak' => 'unassigned_pending_dak','unassignedPendingNote' => 'unassigned_pending_note','unassignedDesignation' => 'unassigned_designation']);
        if(!empty($office_id)){
            $query = $query->where(['office_id' => $office_id]);
        }
        if(!empty( $date[0])){
            $query = $query->where(['record_date >=' => $date[0]]);
        }
        if(!empty( $date[1])){
            $query = $query->where(['record_date <=' => $date[1]]);
        }
//         pr($query);
    return $query->order(['record_date desc'])->first();
    }
    public function deleteDataofOffices($office_id = 0, $date =''){
        if(!empty($office_id)){
            $condition = [
                'office_id' => $office_id,
            ];
              if(!empty($date)){
                     $condition = [
                'office_id' => $office_id,
                'record_date' => $date
            ];
            }
            $query = $this->deleteAll([ $condition ]);
            return $query;
        }
       return 0;
    }
    public function  getOddabodhiDataofOffices($officeid =0)
  {
        set_time_limit(0);
        ini_set('memory_limit', '-1');
//        $dataSourceObject = ConnectionManager::get('default');
//       echo  $dataSourceObject->config()['database'];

        $DashboardOfficesTable = TableRegistry::get('DashboardOffices');
        TableRegistry::remove('DakUsers');
        $DakUsersTable = TableRegistry::get('DakUsers');

        $result = $DashboardOfficesTable->getData($officeid);
//        print_r($result);die;

        $DakMovementsTable        = TableRegistry::get('DakMovements');
        $today                    = date('Y-m-d');


        /*         * Inbox Total* */
        $totaltodayinbox = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0, 'Inbox',
            [$today, $today]);


        if ($result['totalID'] > 0) {
            $totalprevinbox = $result['totalInbox'] + $totaltodayinbox;
        }
        else {
            $totalprevinbox = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0, 'Inbox');
        }

        /*         * Inbox Total* */

          /*         * Outbox Total* */
        $totaltodayoutbox     = $DakMovementsTable->countAllDakbyType($officeid, 0, 0,
            'Outbox', [$today, $today]);
        if ($result['totalID'] > 0) {
            $totalprevoutbox = $result['totalOutbox'] + $totaltodayoutbox;
        }
        else {
            $totalprevoutbox = $DakMovementsTable->countAllDakbyType($officeid, 0, 0,
                'Outbox');
        }


        /*         * Outbox Total* */


        /*         * Nothivukto * */
        $totaltodaynothivukto = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0,
            'NothiVukto', [$today, $today]);

        if ($result['totalID'] > 0) {
            $totalprevnothivukto = $result['totalNothivukto'] + $totaltodaynothivukto;
        }
        else {
            $totalprevnothivukto = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0,
                'NothiVukto');
        }

        /*         * Nothivukto * */

        /*         * Nothijato * */
        $totaltodaynothijato     = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0,
            'NothiJato', [$today, $today]);
        if ($result['totalID'] > 0) {
            $totalprevnothijato = $result['totalNothijato'] + $totaltodaynothijato;
        }
        else {
            $totalprevnothijato = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0,
                'NothiJato');
        }

        /*         * Nothijato * */


        /** Nothi Note Part */
        $temp_today = TableRegistry::get('Dashboard')->nothiDashboardCount($officeid, 0, 0, [ $today, $today]);
//        print_r($temp_today);die;

        /*         * Self Given Note* */
        $returnSummary['totaltodaysrijitonote']     = $temp_today['selfNote'];
        if ($result['totalID'] > 0) {
            $returnSummary['totalSouddog'] = $result['totalSouddog'] + $returnSummary['totaltodaysrijitonote'];
        }

        /*         * Self Given Note* */

        /*         * Result* */


            $returnSummary['totalInbox']       = $totalprevinbox;

        $returnSummary['totalOutbox']       = $totalprevoutbox;


        $returnSummary['totalNothivukto']       = $totalprevnothivukto;

        $returnSummary['totalNothijato']       = $totalprevnothijato;

        $returnSummary['totaltodaynisponnodak']     = $totaltodaynothijato + $totaltodaynothivukto;
        if ($result['totalID'] > 0) {
            $returnSummary['totalNisponnoDak'] = $result['totalNisponnoDak'] + $returnSummary['totaltodaynisponnodak'];
        }
        else {
            $returnSummary['totalNisponnoDak'] = $returnSummary['totalNothijato'] + $returnSummary['totalNothivukto'];
        }


        if ($result['totalID'] > 0) {
            $returnSummary['totalONisponnoDak'] = $result['totalOnisponnodakall'] + ($totaltodayinbox
                - $returnSummary['totaltodaynisponnodak']);
        }
        else {
            $returnSummary['totalONisponnoDak'] = TableRegistry::get('DakUsers')->getOnisponnoDak($officeid,
                0, 0);
        }


        $returnSummary['totaltodaydaksohonote']     = $temp_today['dakNote'];
        if ($result['totalID'] > 0) {
            $returnSummary['totalDaksohoNote'] = $result['totalDaksohoNote'] + $returnSummary['totaltodaydaksohonote'];
        }


        $returnSummary['totaltodaynisponnonote']     = $temp_today['noteNisponno'];
        if ($result['totalID'] > 0) {
            $returnSummary['totalNisponnoNote'] = $result['totalNisponnoNote'] + $returnSummary['totaltodaynisponnonote'];
        }

        $returnSummary['totaltodaynisponnopotrojari']     = $temp_today['potrojariNisponno'];
        if ($result['totalID'] > 0) {
            $returnSummary['totalNisponnoPotrojari'] = $result['totalNisponnoPotrojari'] + $returnSummary['totaltodaynisponnopotrojari'];
        }


//        $returnSummary['totaltodayOnisponnonote']     = $this->getOnisponnoNoteCount($officeid,
//            0, 0, [$today, $today]);
//        $returnSummary['totalyesterdayOnisponnonote'] = $this->getOnisponnoNoteCount($officeid,
//            0, 0, [$yesterday, $yesterday]);
        if ($result['totalID'] > 0) {
//            print_r($result['totalONisponno']);            print_r($temp_today['selfNote'] + $temp_today['dakNote']);die;
            $returnSummary['totalONisponnoNote']   = $result['totalONisponno'] + $temp_today['selfNote']
                + $temp_today['dakNote'] - $temp_today['noteNisponno'] - $temp_today['potrojariNisponno'];
        }else{
            $returnSummary['totalONisponnoNote']     = TableRegistry::get('Dashboard')->getOnisponnoNoteCount($officeid,
            0, 0, [$today, $today]);
        }


        if($returnSummary['totalONisponnoDak'] < 0){
            $returnSummary['totalONisponnoDak'] = 0;
        }

        $returnSummary['totalInbox']  = $returnSummary['totalONisponnoDak'] + $returnSummary['totalNisponnoDak'];
      

        if ($returnSummary['totalONisponnoNote'] < 0) {
            $returnSummary['totalONisponnoNote'] = 0;
        }
        if ($returnSummary['totalOutbox'] > $returnSummary['totalInbox']) {
            $returnSummary['totalOutbox'] = $returnSummary['totalInbox'];
        }

        return $returnSummary;
    }

      public function  getOddabodhiDataofOther($officeid = 0, $unit_id = 0, $designation_id = 0)
  {
        set_time_limit(0);
        ini_set('memory_limit', '-1');
//        $dataSourceObject = ConnectionManager::get('default');
//       echo  $dataSourceObject->config()['database'];

        $DashboardUnitsTable = TableRegistry::get('DashboardUnits');
        $DashboardDesignationsTable = TableRegistry::get('DashboardDesignations');
        TableRegistry::remove('DakUsers');
        $DakUsersTable = TableRegistry::get('DakUsers');

        if(empty($designation_id)){
             $result = $DashboardUnitsTable->getData($unit_id);
        }else{
              $result = $DashboardDesignationsTable->getData($designation_id);
        }
       
//        print_r($result);die;

        $DakMovementsTable        = TableRegistry::get('DakMovements');
        $today                    = date('Y-m-d');


        /*         * Inbox Total* */
        $totaltodayinbox =$DakMovementsTable->countAllDakbyType($officeid, $unit_id,
            $designation_id, 'Inbox', [$today, $today]);


        if ($result['totalID'] > 0) {
             $totalprevinbox = $result['totalInbox'] + $totaltodayinbox;
        }
        else {
             $totalprevinbox = $DakMovementsTable->countAllDakbyType($officeid, $unit_id,
                $designation_id, 'Inbox');
        }

        /*         * Inbox Total* */

          /*         * Outbox Total* */
         $totaltodayoutbox     = $DakMovementsTable->countAllDakbyType($officeid, $unit_id,
            $designation_id, 'Outbox', [$today, $today]);
       if ($result['totalID'] > 0) {
            $totalprevoutbox = $result['totalOutbox'] + $totaltodayoutbox;
        }
        else {
            $totalprevoutbox = $DakMovementsTable->countAllDakbyType($officeid, $unit_id,
                $designation_id, 'Outbox');
        }


        /*         * Outbox Total* */


        /*         * Nothivukto * */
      $totaltodaynothivukto = $DakMovementsTable->countAllDakbyType($officeid, $unit_id,
            $designation_id, 'NothiVukto', [$today, $today]);

        if ($result['totalID'] > 0) {
            $totalprevnothivukto = $result['totalNothivukto'] + $totaltodaynothivukto;
        }
        else {
            $totalprevnothivukto = $DakMovementsTable->countAllDakbyType($officeid, $unit_id,
                $designation_id, 'NothiVukto');
        }

        /*         * Nothivukto * */

        /*         * Nothijato * */
          $totaltodaynothijato     = $DakMovementsTable->countAllDakbyType($officeid, $unit_id,
            $designation_id, 'NothiJato', [$today, $today]);
          if ($result['totalID'] > 0) {
            $totalprevnothijato = $result['totalNothijato'] + $totaltodaynothijato;
        }
        else {
            $totalprevnothijato = $DakMovementsTable->countAllDakbyType($officeid, $unit_id,
                $designation_id, 'NothiJato');
        }

        /*         * Nothijato * */


        /** Nothi Note Part */
         $temp_today = TableRegistry::get('Dashboard')->nothiDashboardCount($officeid, $unit_id, $designation_id,
            [ $today, $today]);
//        print_r($temp_today);die;

        /*         * Self Given Note* */
        $returnSummary['totaltodaysrijitonote']     = $temp_today['selfNote'];
        if ($result['totalID'] > 0) {
            $returnSummary['totalSouddog'] = $result['totalSouddog'] + $returnSummary['totaltodaysrijitonote'];
        }

        /*         * Self Given Note* */

        /*         * Result* */


            $returnSummary['totalInbox']       = $totalprevinbox;

        $returnSummary['totalOutbox']       = $totalprevoutbox;


        $returnSummary['totalNothivukto']       = $totalprevnothivukto;

        $returnSummary['totalNothijato']       = $totalprevnothijato;

        $returnSummary['totaltodaynisponnodak']     = $totaltodayoutbox + $totaltodaynothijato + $totaltodaynothivukto+ $DakMovementsTable->countOnulipiDak($officeid,
                $unit_id, $designation_id, [ $today, $today]);
        if ($result['totalID'] > 0) {
            $returnSummary['totalNisponnoDak'] = $result['totalNisponnoDak'] + $returnSummary['totaltodaynisponnodak'];
        }
        else {
            $returnSummary['totalNisponnoDak'] = $returnSummary['totalOutbox'] + $returnSummary['totalNothijato'] + $returnSummary['totalNothivukto']+ $DakMovementsTable->countOnulipiDak($officeid,  $unit_id, $designation_id);
        }


        if ($result['totalID'] > 0) {
            $returnSummary['totalONisponnoDak'] = $result['totalOnisponnodakall'] + ($totaltodayinbox
                - $returnSummary['totaltodaynisponnodak']);
        }
        else {
            $returnSummary['totalONisponnoDak'] = $DakUsersTable->getOnisponnoDak($officeid,
                $unit_id, $designation_id);
        }


        $returnSummary['totaltodaydaksohonote']     = $temp_today['dakNote'];
        if ($result['totalID'] > 0) {
            $returnSummary['totalDaksohoNote'] = $result['totalDaksohoNote'] + $returnSummary['totaltodaydaksohonote'];
        }


        $returnSummary['totaltodaynisponnonote']     = $temp_today['noteNisponno'];
        if ($result['totalID'] > 0) {
            $returnSummary['totalNisponnoNote'] = $result['totalNisponnoNote'] + $returnSummary['totaltodaynisponnonote'];
        }

        $returnSummary['totaltodaynisponnopotrojari']     = $temp_today['potrojariNisponno'];
        if ($result['totalID'] > 0) {
            $returnSummary['totalNisponnoPotrojari'] = $result['totalNisponnoPotrojari'] + $returnSummary['totaltodaynisponnopotrojari'];
        }


//        $returnSummary['totaltodayOnisponnonote']     = $this->getOnisponnoNoteCount($officeid,
//            0, 0, [$today, $today]);
//        $returnSummary['totalyesterdayOnisponnonote'] = $this->getOnisponnoNoteCount($officeid,
//            0, 0, [$yesterday, $yesterday]);
        if ($result['totalID'] > 0) {
//            print_r($result['totalONisponno']);            print_r($temp_today['selfNote'] + $temp_today['dakNote']);die;
            $returnSummary['totalONisponnoNote']   = $result['totalONisponno'] + $temp_today['selfNote']
                + $temp_today['dakNote'] - $temp_today['noteNisponno'] - $temp_today['potrojariNisponno'];
        }else{
            $returnSummary['totalONisponnoNote']     = TableRegistry::get('Dashboard')->getOnisponnoNoteCount($officeid,
            0, 0, [$today, $today]);
        }


        if($returnSummary['totalONisponnoDak'] < 0){
            $returnSummary['totalONisponnoDak'] = 0;
        }

        $returnSummary['totalInbox']  = $returnSummary['totalONisponnoDak'] + $returnSummary['totalNisponnoDak'];


        if ($returnSummary['totalONisponnoNote'] < 0) {
            $returnSummary['totalONisponnoNote'] = 0;
        }
        if ($returnSummary['totalOutbox'] > $returnSummary['totalInbox']) {
            $returnSummary['totalOutbox'] = $returnSummary['totalInbox'];
        }

        return $returnSummary;
    }
    public function getOfficeOnisponno(){
//        $returnSummary['totalONisponnoNote']   = $result['totalONisponno'] + $temp_today['selfNote']
//                + $temp_today['dakNote'] - $temp_today['noteNisponno'] - $temp_today['potrojariNisponno'];
    }
}