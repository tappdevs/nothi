<?php

namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;

class PotrojariAttachmentsTable extends ProjapotiTable
{

    public function initialize(array $config)
    {
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->addBehavior('MobileTrack');
    }
    public function getAllAttachments($potro_id,$conditions = [],$select = []){
        if(empty($potro_id)){
            return;
        }
        $query = $this->find()->where(['potrojari_id' => $potro_id]);
        if(!empty($select)){
            $query->select($select);
        }
        if(!empty($conditions)){
            $query->where($conditions);
        }
        return $query ; 
    }
    public function getData($select = [], $conditions = [], $order = []){
        $query = $this->find();
        if(!empty($select)){
            $query->select($select);
        }
        if(!empty($conditions)){
            $query->where($conditions);
        }
        if(!empty($order)){
            $query->order($order);
        }
        return $query;
    }
    public function setData($data){
        if(empty($data)){
            return false;
        }
        try{
            $entity = $this->patchEntity($this->newEntity(),$data);
            $this->save($entity);
            return true;
        }catch (\Exception $ex){

        }
        return false;
    }
}