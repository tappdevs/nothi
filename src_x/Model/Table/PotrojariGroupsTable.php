<?php

namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;

class PotrojariGroupsTable extends ProjapotiTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $conn = ConnectionManager::get('NothiAccessDb');
        $this->connection($conn);
        $this->primaryKey('id');
        $this->displayField('group_name');
        $this->addBehavior('Timestamp');
        $this->table('potrojari_groups');

        $this->hasMany('PotrojariGroupsUsers', [
            'foreignKey' => 'group_id',
        ]);
    }

    public function getGroups($office_id = 0, $unit_id = 0, $org_id = 0,
                              $employee_id = 0, $privacy = '',
                              $order = ['modified desc'], $list = false)
    {
        if ($list) {
            $query = $this->find('list');
        } else {
            $query = $this->find();
        }

        if (!empty($office_id)) {
            $query->where(['creator_office_id' => $office_id])->orWhere(['creator_office_id' => 0]);
        }
        if (!empty($unit_id)) {
            $query->where(['creator_unit_id' => $unit_id])->orWhere(['creator_unit_id' => 0]);
        }
        if (!empty($org_id)) {
            $query->where(['creator_office_unit_organogram_id' => $org_id])->orWhere(['creator_office_unit_organogram_id' => 0]);
        }
        if (!empty($employee_id)) {
            $query->where(['creator_employee_id' => $employee_id])->orWhere(['creator_employee_id' => 0]);
        }
        if (!empty($privacy)) {
            $query->where(['privacy_type' => $privacy]);
        }

        return $query->order($order);
    }

    public function getGroupList($conditions = [], $list = 1){
        if ($list == 1) {
            $query = $this->find('list')->contain(['PotrojariGroupsUsers']);
        } else {
            $query = $this->find()->contain(['PotrojariGroupsUsers']);
        }

        if (!empty($conditions)) {
            $query->where($conditions);
        }

        return $query->hydrate(false);
    }
}