<?php

namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;

class PotrojariGroupsUsersTable extends ProjapotiTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $conn = ConnectionManager::get('NothiAccessDb');
        $this->connection($conn);
        $this->primaryKey('id');
        $this->table('potrojari_groups_users');
        $this->addBehavior('Timestamp');
        $this->belongsTo('PotrojariGroups', [
            'foreignKey' => 'group_id',
            'joinType' => 'INNER'
        ]);
    }

    public function updatePotrojariGroupOfficeHead($new_office_unit_organogram_id, $prev_office_unit_organogram_id = '')
    {
        if (!empty($prev_office_unit_organogram_id)) {
            $this->updateAll(['office_head' => 0], ['office_unit_organogram_id' => $prev_office_unit_organogram_id]);
        }
        $this->updateAll(['office_head' => 1], ['office_unit_organogram_id' => $new_office_unit_organogram_id]);
        return true;
    }

    public function updatePotrojariGroupEmployeeDetails($employee_record_id, $office_unit_organogram_id)
    {
        $employeeRecordsTable = TableRegistry::get('EmployeeRecords');
        try {
            $employee_record_info = $employeeRecordsTable->get($employee_record_id);
            if (!empty($employee_record_info)) {
                $this->updateAll(['employee_id' => $employee_record_id,
                    'employee_name_eng' => $employee_record_info['name_eng'],
                    'employee_name_bng' => $employee_record_info['name_bng'],
                    'officer_email' => $employee_record_info['personal_email'],
                    'officer_mobile' => $employee_record_info['personal_mobile'],
                ], ['office_unit_organogram_id' => $office_unit_organogram_id]);
            }
            return ['status' => 1, 'data' => $employee_record_info];
        } catch (\Exception $ex) {
            return ['status' => 0, 'msg' => $ex->getMessage()];
        }

    }

    public function unsetPotrojariGroupEmployeeDetails($office_unit_organogram_id)
    {

        if ($office_unit_organogram_id <= 0) {
            return ['status' => 0, 'msg' => 'Invalid request'];
        }
        try {
            $this->updateAll([
                'employee_id' => 0,
                'employee_name_eng' => "(No designation)",
                'employee_name_bng' => "(শূন্য পদ)",
                'officer_email' => '',
                'officer_mobile' => '',
            ],
                ['office_unit_organogram_id' => $office_unit_organogram_id]);

            return ['status' => 1];
        } catch (\Exception $ex) {
            return ['status' => 0, 'msg' => $ex->getMessage()];
        }

    }

    public function updatePotrojariGroupUnitDetails($office_unit_id, $office_unit_name_eng, $office_unit_name_bng)
    {
        $this->updateAll(['office_unit_name_eng' => $office_unit_name_eng, 'office_unit_name_bng' => $office_unit_name_bng], ['office_unit_id' => $office_unit_id]);
        return true;
    }

    public function updatePotrojariGroupDesignationDetails($office_unit_organogram_id, $office_unit_organogram_name_eng, $office_unit_organogram_name_bng)
    {
        $this->updateAll(['office_unit_organogram_name_eng' => $office_unit_organogram_name_eng, 'office_unit_organogram_name_bng' => $office_unit_organogram_name_bng], ['office_unit_organogram_id' => $office_unit_organogram_id]);
        return true;
    }

    public function updatePotrojariGroupProfileDetails($employee_record_id, $employee_name_eng, $employee_name_bng, $officer_email = '')
    {
        $this->updateAll(['employee_name_eng' => $employee_name_eng, 'employee_name_bng' => $employee_name_bng, 'officer_email' => $officer_email], ['employee_id' => $employee_record_id]);
        return true;
    }

    public function getUserList($groups_id = [],$json = 0,$conditions = []){
        $query = $this->find();
        if(!empty($groups_id)){
            $query->where(['group_id IN' => $groups_id]);
        }
        if(!empty($conditions)){
            $query->where($conditions);
        }

        $query = $query->order(['group_id'])->toArray();
        if($json == 0){
            return $query;
        } else {
            $result = [];
            $ind = 0;
            if (!empty($query)) {
                foreach ($query as $group_val) {
                    $json_data = [
                        'employee_id' => $group_val['employee_id'],
                        'employee_name' => $group_val['employee_name'],
                        'office_id' => $group_val['office_id'],
                        'office_name' => $group_val['office_name_bng'],
                        'office_name_bng' => $group_val['office_name_bng'],
                        'office_name_eng' => $group_val['office_name_eng'],
                        'office_unit_id' => $group_val['office_unit_id'],
                        'office_unit_name' => $group_val['office_unit_name_bng'],
                        'office_unit_name_eng' => $group_val['office_unit_name_eng'],
                        'office_unit_name_bng' => $group_val['office_unit_name_bng'],
                        'office_unit_organogram_id' => $group_val['office_unit_organogram_id'],
                        'office_unit_organogram_name' => $group_val['office_unit_organogram_name_bng'],
                        'officer_email' => $group_val['officer_email'],
                        'officer_mobile' => $group_val['officer_mobile'],
                        'office_head' => $group_val['office_head'],
                    ];
                    $result[$group_val['group_id']]['id'] = $group_val['id'];
                    $result[$group_val['group_id']]['group_name'] = $group_val['group_name'];
                    $result[$group_val['group_id']]['group_value'][] = $json_data;
                }
                return $result;
            }
        }
    }
    public function getAll($select = [], $conditions = [], $order = []){
        $query = $this->find();
        if(!empty($select)){
            $query->select($select);
        }
        if(!empty($conditions)){
            $query->where($conditions);
        }
        if(!empty($order)){
            $query->order($order);
        }
        return $query;
    }
}