<?php
namespace App\Model\Table;

use App\Model\Entity\PotrojariOnulipi;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PotrojariOnulipi Model
 */
class PotrojariOnulipiTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('potrojari_onulipi');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->belongsTo('Potrojari', [
            'foreignKey' => 'potrojari_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('NothiMasters', [
            'foreignKey' => 'nothi_master_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('NothiNotes', [
            'foreignKey' => 'nothi_notes_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('NothiPotros', [
            'foreignKey' => 'nothi_potro_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('Offices', [
            'foreignKey' => 'office_id',
            'joinType' => 'INNER'
        ]);
        
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');
            
        $validator
            ->requirePresence('nothi_part_no', 'create')
            ->notEmpty('nothi_part_no');
            
        $validator
            ->add('potro_type', 'valid', ['rule' => 'numeric'])
            ->requirePresence('potro_type', 'create')
            ->notEmpty('potro_type');
            
        $validator
            ->requirePresence('sarok_no', 'create')
            ->notEmpty('sarok_no');
            
        $validator
            ->requirePresence('office_name', 'create')
            ->notEmpty('office_name');
            
        $validator
            ->allowEmpty('officer_name');
            
        $validator
            ->requirePresence('office_unit_name', 'create')
            ->notEmpty('office_unit_name');
            
        $validator
            ->allowEmpty('officer_designation_label');
            
        $validator
            ->add('potrojari_date', 'valid', ['rule' => 'datetime'])
            ->requirePresence('potrojari_date', 'create')
            ->notEmpty('potrojari_date');
            
        $validator
            ->requirePresence('potro_subject', 'create')
            ->notEmpty('potro_subject');
            
        $validator
            ->requirePresence('potro_security_level', 'create')
            ->notEmpty('potro_security_level');
            
        $validator
            ->requirePresence('potro_priority_level', 'create')
            ->notEmpty('potro_priority_level');
            
        $validator
            ->allowEmpty('potro_description');
            
        $validator
            ->allowEmpty('receiving_office_name');
            
        $validator
            ->allowEmpty('receiving_office_unit_name');
            
        $validator
            ->allowEmpty('receiving_officer_designation_label');
            
        $validator
            ->allowEmpty('receiving_officer_name');
            
        $validator
            ->allowEmpty('receiving_officer_email');
            
        $validator
            ->add('run_task', 'valid', ['rule' => 'numeric'])
            ->requirePresence('run_task', 'create')
            ->notEmpty('run_task');
            
        $validator
            ->add('retry_count', 'valid', ['rule' => 'numeric'])
            ->requirePresence('retry_count', 'create')
            ->notEmpty('retry_count');
            
        $validator
            ->allowEmpty('task_reposponse');
            
        $validator
            ->requirePresence('potro_status', 'create')
            ->notEmpty('potro_status');
            
        $validator
            ->add('is_sent', 'valid', ['rule' => 'numeric'])
            ->requirePresence('is_sent', 'create')
            ->notEmpty('is_sent');
            
        $validator
            ->add('created_by', 'valid', ['rule' => 'numeric'])
            ->requirePresence('created_by', 'create')
            ->notEmpty('created_by');
            
        $validator
            ->add('modified_by', 'valid', ['rule' => 'numeric'])
            ->requirePresence('modified_by', 'create')
            ->notEmpty('modified_by');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function SentStatus ($potrojari_id, $conditions = [], $status = -1){

        $query = $this->find()->where(['potrojari_id'=>$potrojari_id]);

        if($status!=-1){
            $query->where(['is_sent'=>$status]);
        }

        if(!empty($conditions)){
            $query->where($conditions);
        }

        return $query;
    }
     public function allPotrojariIDArray($potrojari_ids = [], $potrojari_ids_not_in = [])
    {
        return $this->find('list',['keyField' =>'potrojari_id','valueField'=>'potrojari_id'])->where(['receiving_officer_designation_id >' => 0, 'potrojari_id IN' => $potrojari_ids, 'potrojari_id NOT IN' => $potrojari_ids_not_in])->toArray();
    }
}
