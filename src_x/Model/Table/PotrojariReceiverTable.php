<?php
namespace App\Model\Table;

use App\Model\Entity\PotrojariReceiver;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

/**
 * PotrojariReceiver Model
 */
class PotrojariReceiverTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('potrojari_receiver');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->belongsTo('Potrojari', [
            'foreignKey' => 'potrojari_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('NothiMasters', [
            'foreignKey' => 'nothi_master_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('NothiNotes', [
            'foreignKey' => 'nothi_notes_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('NothiPotros', [
            'foreignKey' => 'nothi_potro_id',
            'joinType' => 'INNER'
        ]);
       
        $this->belongsTo('Offices', [
            'foreignKey' => 'office_id',
            'joinType' => 'INNER'
        ]);
       
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');
            
        $validator
            ->requirePresence('nothi_part_no', 'create')
            ->notEmpty('nothi_part_no');
            
        $validator
            ->add('potro_type', 'valid', ['rule' => 'numeric'])
            ->requirePresence('potro_type', 'create')
            ->notEmpty('potro_type');
            
        $validator
            ->requirePresence('sarok_no', 'create')
            ->notEmpty('sarok_no');
            
        $validator
            ->requirePresence('office_name', 'create')
            ->notEmpty('office_name');
            
        $validator
            ->allowEmpty('officer_name');
            
        $validator
            ->requirePresence('office_unit_name', 'create')
            ->notEmpty('office_unit_name');
            
        $validator
            ->allowEmpty('officer_designation_label');
            
        $validator
            ->add('potrojari_date', 'valid', ['rule' => 'datetime'])
            ->requirePresence('potrojari_date', 'create')
            ->notEmpty('potrojari_date');
            
        $validator
            ->requirePresence('potro_subject', 'create')
            ->notEmpty('potro_subject');
            
        $validator
            ->requirePresence('potro_security_level', 'create')
            ->notEmpty('potro_security_level');
            
        $validator
            ->requirePresence('potro_priority_level', 'create')
            ->notEmpty('potro_priority_level');
            
        $validator
            ->allowEmpty('potro_description');
            
        $validator
            ->allowEmpty('receiving_office_name');
            
        $validator
            ->allowEmpty('receiving_office_unit_name');
            
        $validator
            ->allowEmpty('receiving_officer_designation_label');
            
        $validator
            ->allowEmpty('receiving_officer_name');
            
        $validator
            ->allowEmpty('receiving_officer_email');
            
        $validator
            ->add('run_task', 'valid', ['rule' => 'numeric'])
            ->requirePresence('run_task', 'create')
            ->notEmpty('run_task');
            
        $validator
            ->add('retry_count', 'valid', ['rule' => 'numeric'])
            ->requirePresence('retry_count', 'create')
            ->notEmpty('retry_count');
            
        $validator
            ->allowEmpty('task_reposponse');
            
        $validator
            ->requirePresence('potro_status', 'create')
            ->notEmpty('potro_status');
            
        $validator
            ->add('is_sent', 'valid', ['rule' => 'numeric'])
            ->requirePresence('is_sent', 'create')
            ->notEmpty('is_sent');
            
        $validator
            ->add('created_by', 'valid', ['rule' => 'numeric'])
            ->requirePresence('created_by', 'create')
            ->notEmpty('created_by');
            
        $validator
            ->add('modified_by', 'valid', ['rule' => 'numeric'])
            ->requirePresence('modified_by', 'create')
            ->notEmpty('modified_by');

        return $validator;
    }

    public function SentStatus ($potrojari_id, $conditions = [], $status = -1){

        $query = $this->find()->where(['potrojari_id'=>$potrojari_id]);

        if($status!=-1){
            $query->where(['is_sent'=>$status]);
        }

        if(!empty($conditions)){
            $query->where($conditions);
        }

        return $query;
    }
    public function allPotrojariIDArray($potrojari_ids = [])
    {
        return $this->find('list',['keyField' =>'potrojari_id','valueField'=>'potrojari_id'])->where(['receiving_officer_designation_id >' => 0, 'potrojari_id IN' => $potrojari_ids])->toArray();
    }

    public function saveAttensionasReceiver($potrojari){
        try {
            if(empty($potrojari['attension_officer_designation_id'])){
                return true;
            }
            $hasAlready = $this->find()->where([
                'receiving_officer_designation_id'=>$potrojari['attension_officer_designation_id'],
                'potrojari_id'=>$potrojari['id'],
                'nothi_part_no'=>$potrojari['nothi_part_no'],
            ])->count();

            $tableOnulipi = TableRegistry::get('PotrojariOnulipi');
            $hasAlready2 = $tableOnulipi->find()->where([
                'receiving_officer_designation_id'=>$potrojari['attension_officer_designation_id'],
                'potrojari_id'=>$potrojari['id'],
                'nothi_part_no'=>$potrojari['nothi_part_no'],
            ])->count();

            if($hasAlready || $hasAlready2){
                return true;
            }

            $entity = $this->newEntity();

            $entity->potrojari_id = $potrojari['id'];
            $entity->nothi_master_id = $potrojari['nothi_master_id'];
            $entity->nothi_part_no = $potrojari['nothi_part_no'];
            $entity->nothi_notes_id = $potrojari['nothi_notes_id'];
            $entity->nothi_potro_id = $potrojari['nothi_potro_id'];
            $entity->potro_type = $potrojari['potro_type'];

            $entity->dak_id = $potrojari['dak_id'];
            $entity->sarok_no = $potrojari['sarok_no'];

            $entity->office_id = $potrojari['office_id'];
            $entity->officer_id = $potrojari['officer_id'];
            $entity->office_name = $potrojari['office_name'];
            $entity->officer_name = $potrojari['officer_name'];
            $entity->office_unit_id = $potrojari['office_unit_id'];
            $entity->office_unit_name = $potrojari['office_unit_name'];
            $entity->officer_designation_id = $potrojari['officer_designation_id'];
            $entity->officer_designation_label = $potrojari['officer_designation_label'];

            $entity->potrojari_date = $potrojari['potrojari_date'];
            $entity->potro_subject = $potrojari['potro_subject'];
            $entity->potro_security_level = $potrojari['potro_security_level'];
            $entity->potro_priority_level = $potrojari['potro_priority_level'];
            $entity->potro_description = $potrojari['potro_description'];
            
            $entity->receiving_office_id = $potrojari['attension_office_id'];
            $entity->receiving_office_name = $potrojari['attension_office_name'];
            $entity->receiving_officer_id = $potrojari['attension_officer_id'];
            $entity->receiving_office_unit_id = $potrojari['attension_office_unit_id'];
            $entity->receiving_office_unit_name = $potrojari['attension_office_unit_name'];
            $entity->receiving_officer_designation_id = $potrojari['attension_officer_designation_id'];
            $entity->receiving_officer_designation_label = $potrojari['attension_officer_designation_label'];
            $entity->receiving_officer_name = $potrojari['attension_officer_name'];
            $entity->visibleName = $entity->receiving_officer_designation_label . ', ' . $potrojari['attension_office_unit_name'] . ', ' . $potrojari['attension_office_name'];

            $entity->potro_status = $potrojari['potro_status'];
            $entity->is_sent = 0;
            $entity->created_by = 0;
            $entity->modified_by = 0;

            if($this->save($entity)){
                return true;
            }
        }catch (\Exception $ex){

        }

        return false;
    }
}
