<?php
namespace App\Model\Table;

use App\Model\Entity\PotrojariRequest;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;
/**
 * PotrojariRequest Model
 */
class PotrojariRequestTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $conn = ConnectionManager::get('projapotiDb');
        $this->connection($conn);
        $this->table('potrojari_request');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');
            
        $validator
            ->requirePresence('command', 'create')
            ->notEmpty('command');
            
        $validator
            ->add('try', 'valid', ['rule' => 'numeric'])
            ->requirePresence('try', 'create')
            ->notEmpty('try');
            
        $validator
            ->add('status', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        return $rules;
    }
    public function saveData($command,$office_id,$potrojari_id,$try,$status){
        // first check if this data exist or not
        $hasData = $this->find()->where(['command' => $command,'office_id' => $office_id,'potrojari_id' => $potrojari_id])->first();
        if(!empty($hasData) && isset($hasData['try'])){
            $this->updateAll(['try' => intval($hasData['try'])+1], ['command' => $command,'office_id' => $office_id,'potrojari_id' => $potrojari_id]);
            return true;
        }
        else if(empty($hasData)){
             $entity = $this->newEntity();
            $entity->command	 = $command;
            $entity->office_id = $office_id;
            $entity->potrojari_id = $potrojari_id;
            $entity->try = $try;
            $entity->status = $status;
             if($this->save($entity)){
                return true;
            }
        }
        return false;
    }
}
