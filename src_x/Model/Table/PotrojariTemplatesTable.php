<?php
namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;

class PotrojariTemplatesTable extends ProjapotiTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $conn = ConnectionManager::get('projapotiDb');
        $this->connection($conn);
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
    }
    public function getTemplateByIdAndVersion($template_id =0,$version ='bn'){
        return $this->find()->where(['template_id' => $template_id,'version' => $version])->first();
    }
    public function getCsTemplateByIdAndVersion($version ='bn'){
        $content=[];
        $content['cs_cover'] = $this->find()->where(['template_id' => 28,'version' => $version])->first();
        $content['cs_body'] = $this->find()->where(['template_id' => 29,'version' => $version])->first();
        return $content;
    }
}