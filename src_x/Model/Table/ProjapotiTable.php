<?php

namespace App\Model\Table;

use Cake\Network\Session;
use Cake\ORM\Table;

class ProjapotiTable extends Table
{
    public function getCurrentDakSection()
    {
        $session = new Session();
        return $session->read('selected_office_section');
    }
}