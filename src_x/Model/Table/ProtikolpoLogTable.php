<?php
namespace App\Model\Table;

use App\Model\Entity\ProtikolpoLog;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ProtikolpoLog Model
 */
class ProtikolpoLogTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('protikolpo_log');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->belongsTo('ProtikolpoSettings', [
            'foreignKey' => 'protikolpo_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');
            
        $validator
            ->add('protikolpo_start_date', 'valid', ['rule' => 'datetime'])
            ->requirePresence('protikolpo_start_date', 'create')
            ->notEmpty('protikolpo_start_date');
            
        $validator
            ->add('protikolpo_end_date', 'valid', ['rule' => 'datetime'])
            ->allowEmpty('protikolpo_end_date');
            
        $validator
            ->add('protikolpo_ended_by', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('protikolpo_ended_by');
            
        $validator
            ->allowEmpty('employee_office_id_from_name');
            
        $validator
            ->allowEmpty('employee_office_id_to_name');
            
        $validator
            ->add('protikolpo_status', 'valid', ['rule' => 'numeric'])
            ->requirePresence('protikolpo_status', 'create')
            ->notEmpty('protikolpo_status');

        return $validator;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'NothiAccessDb';
    }
}
