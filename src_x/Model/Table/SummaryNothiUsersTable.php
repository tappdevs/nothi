<?php
namespace App\Model\Table;

use App\Model\Entity\SummaryNothiUser;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use App\Controller\ProjapotiController;

/**
 * SummaryNothiUsers Model
 */
class SummaryNothiUsersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $conn = ConnectionManager::get('projapotiDb');
        $this->connection($conn);
        $this->table('summary_nothi_users');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');

        $this->belongsTo('EmployeeRecords', [
            'foreignKey' => 'employee_record_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');
            
        $validator
            ->add('nothi_office', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('nothi_office');
            
        $validator
            ->allowEmpty('nothi_part_no');
            
        $validator
            ->requirePresence('position_number', 'create')
            ->notEmpty('position_number');
            
        $validator
            ->add('can_approve', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('can_approve');
            
        $validator
            ->add('is_approve', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('is_approve');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['employee_record_id'], 'EmployeeRecords'));
        return $rules;
    }

    public function getData($conditions){

        return $this->find()->where($conditions);
    }

    public function getNextOffice($current_sequence){

        return $this->find()->where(['sequence_number >' => $current_sequence])->order(['sequence_number asc'])->first();
    }
    public function getTracking($tracking_no){
       $results= $this->find()->where(['tracking_id LIKE'=>$tracking_no])->order(['id desc'])->toArray();
       if(!empty($results)) {

           $status = [];
           $flag = 0;
           $office_ids = [];
           $nothi_master_ids = [];
           $designation_ids = [];
           $employee_record_ids = [];
           $i = 0;
           foreach ($results as $result) {
               if ($result['potro_id'] > 0) {
                   $status[$i]['current_office_id'] = $result['current_office_id'];
                   $status[$i]['nothi_master_id'] = $result['nothi_master_id'];
                   $status[$i]['designation_id'] = $result['designation_id'];
                   $status[$i]['employee_record_id'] = $result['employee_record_id'];
                   $status[$i]['is_approve'] = $result['is_approve'];
                   $flag < 3 ? $flag = 3 : $flag = $flag;
                   $office_ids[] = $result['current_office_id'];
                   $nothi_master_ids[$i]['office_id'] = $result['current_office_id'];
                   $nothi_master_ids[$i]['master_id'] = $result['nothi_master_id'];
                   $designation_ids[] = $result['designation_id'];
                   $employee_record_ids[] = $result['employee_record_id'];
                   $i++;
               } else if ($result['dak_id'] > 0) {
                   $status[$i]['current_office_id'] = $result['current_office_id'];
                   $status[$i]['nothi_master_id'] = $result['nothi_master_id'];
                   $status[$i]['designation_id'] = $result['designation_id'];
                   $status[$i]['employee_record_id'] = $result['employee_record_id'];
                   $status[$i]['is_approve'] = $result['is_approve'];
                   $office_ids[] = $result['current_office_id'];
                   $nothi_master_ids[$i]['office_id'] = $result['current_office_id'];
                   $nothi_master_ids[$i]['master_id'] = $result['nothi_master_id'];
                   $designation_ids[] = $result['designation_id'];
                   $employee_record_ids[] = $result['employee_record_id'];
                   $flag < 3 ?$flag = 2 : $flag = $flag;
                   $i++;
               } else {
                   $status[$i]['current_office_id'] = $result['current_office_id'];
                   $status[$i]['nothi_master_id'] = $result['nothi_master_id'];
                   $status[$i]['designation_id'] = $result['designation_id'];
                   $status[$i]['employee_record_id'] = $result['employee_record_id'];
                   $status[$i]['is_approve'] = $result['is_approve'];
                   $office_ids[] = $result['current_office_id'];
                   $nothi_master_ids[$i]['office_id'] = $result['current_office_id'];
                   $nothi_master_ids[$i]['master_id'] = $result['nothi_master_id'];
                   $designation_ids[] = $result['designation_id'];
                   $employee_record_ids[] = $result['employee_record_id'];
                   $flag > 1 ? $flag = $flag : $flag= 1;
                   $i++;
               }
           }
           $officeTable = TableRegistry::get('Offices');

           $employeeRecordsTable = TableRegistry::get('EmployeeRecords');
           $officeUnitOrganogramsTable = TableRegistry::get('OfficeUnitOrganograms');

           $office_ids = array_unique($office_ids);
           $employee_record_ids = array_unique($employee_record_ids);
           $designation_ids = array_unique($designation_ids);

           $office_infos = $officeTable->find()->where(['id IN' => $office_ids])->toArray();
           $employee_record_infos = $employeeRecordsTable->find()->where(['id IN' => $employee_record_ids])->toArray();
           $designation_infos = $officeUnitOrganogramsTable->find()->where(['id IN' => $designation_ids])->toArray();

           $formatted_office_infos = [];
           foreach ($office_infos as $office_info) {
               $formatted_office_infos[$office_info['id']]['office_name_bng'] = $office_info['office_name_bng'];
           }
           $formatted_employee_record_infos = [];
           foreach ($employee_record_infos as $employee_record_info) {
               $formatted_employee_record_infos[$employee_record_info['id']]['name_bng'] = $employee_record_info['name_bng'];
           }
           $formatted_designation_infos = [];
           foreach ($designation_infos as $designation_info) {
               $formatted_designation_infos[$designation_info['id']]['designation_bng'] = $designation_info['designation_bng'];
           }

           $keys    = array_map(function ($j) { return $j['office_id']; }, $nothi_master_ids);
           $deduped = array_combine($keys, $nothi_master_ids);
           $nothi_master_ids  = array_values($deduped);

           $projapoti_controller = new ProjapotiController();
           $current_dak_section_data =$projapoti_controller->getCurrentDakSection();

           $own_office_id = $current_dak_section_data['office_id'];
           $nothi_master_info_formatted =[];
           foreach($nothi_master_ids as $nothi_master_id){
               $projapoti_controller->switchOffice($nothi_master_id['office_id'],'OfficeDB');
               TableRegistry::remove('NothiMasters');
               $nothiMastersTable = TableRegistry::get('NothiMasters');
               if(!empty($nothi_master_id['master_id'])) {
                   $nothi_master_infos = $nothiMastersTable->find()->where(['id' => $nothi_master_id['master_id']])->first();
                   $nothi_master_info_formatted[$nothi_master_id['office_id']][$nothi_master_id['master_id']]['nothi_no'] = $nothi_master_infos['nothi_no'];
                   $nothi_master_info_formatted[$nothi_master_id['office_id']][$nothi_master_id['master_id']]['subject'] = $nothi_master_infos['subject'];
               }
           }
           if(!empty($own_office_id)) {
               $projapoti_controller->switchOffice($own_office_id, 'OfficeDB');
           }

           $data['search']='success';
           $data['formatted_office_infos'] = $formatted_office_infos;
           $data['nothi_master_info_formatted'] = $nothi_master_info_formatted;
           $data['formatted_employee_record_infos'] = $formatted_employee_record_infos;
           $data['formatted_designation_infos'] = $formatted_designation_infos;
           $data['status'] = $status;
           $data['flag'] = $flag;
           return $data;
       }
       else {
           $data= array();
           $data['search']='fail';
           return $data;

       }
    }
}
