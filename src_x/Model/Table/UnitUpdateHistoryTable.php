<?php

namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;

class UnitUpdateHistoryTable extends ProjapotiTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $conn = ConnectionManager::get('projapotiDb');
        $this->connection($conn);
        $this->table('unit_update_history');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');

        $this->belongsTo('OfficeUnits', [
            'foreignKey' => 'unit_id',
            'joinType' => 'INNER',
        ]);
    }
}