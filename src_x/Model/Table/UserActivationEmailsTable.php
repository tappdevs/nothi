<?php
namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;

/**
 * Articles Model
 */
class UserActivationEmailsTable extends ProjapotiTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $conn = ConnectionManager::get('projapotiDb');
        $this->connection($conn);

        $this->table('user_activation_emails');
        /* $this->primaryKey('id');
        $this->displayField('username');
        $this->addBehavior('Timestamp');
        $this->belongsTo('UserRoles');
        $this->addBehavior('Cake3Upload.Upload', [
                'fields' => [
                    'photo' => [
                        'path' => 'upload/Users/:y/:m/:md5'
                    ]
                ]
            ]
        ); */
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    /* public function validationDefault(Validator $validator)
    {
        return $validator
            ->notEmpty('username', 'A username is required')
            ->notEmpty('password', 'A password is required')
            ->notEmpty('role', 'A role is required')
            ;
    } */

}
