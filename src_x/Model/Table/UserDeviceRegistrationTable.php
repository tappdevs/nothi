<?php
namespace App\Model\Table;

use App\Model\Entity\UserDeviceRegistration;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Datasource\ConnectionManager;
use Cake\I18n\Time;
use Cake\Network\Email\Email;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Utility\String;
use Cake\Validation\Validator;


/**
 * UserDeviceRegistration Model
 */
class UserDeviceRegistrationTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $con = ConnectionManager::get('projapotiDb');
        $this->connection($con);
        $this->table('user_device_registration');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->add('activate', 'valid', ['rule' => 'numeric'])
            ->requirePresence('activate', 'create')
            ->notEmpty('activate');

        $validator
            ->allowEmpty('activation_code');

        $validator
            ->add('activate_request', 'valid', ['rule' => 'date'])
            ->allowEmpty('activate_request');

        $validator
            ->add('activate_date', 'valid', ['rule' => 'date'])
            ->allowEmpty('activate_date');

        $validator
            ->add('expire_date', 'valid', ['rule' => 'date'])
            ->allowEmpty('expire_date');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        return $rules;
    }

    public function verifyDevice($user_id, $device_id, $code)
    {

        $data = $this->find()->where(['user_id' => $user_id, 'device_id' => $device_id, 'date(expire_date) >=' => date("Y-m-d")])->order(['id desc'])->first();

        if (!empty($data)) {
            $hasher = new DefaultPasswordHasher();
            if ($hasher->check($code, $data['activation_code'])) {
                return $data;
            }
        }

        return false;
    }

    public function activationDevice($user_id, $device_id, $code)
    {

        $data = $this->verifyDevice($user_id, $device_id, $code);

        if ($data !== false) {
            $this->updateAll(['activate' => 1, 'activate_date' => date("Y-m-d")], ['id' => $data['id']]);
            return true;
        }
        return "Invalid code";
    }

    public function registerDevice($data)
    {
        if (!empty($data['user']) && !empty($data['device_id'])) {
            $this->deleteAll(['user_id' => $data['user']['id'], 'device_id' => $data['device_id']]);
            $tableProfiles = TableRegistry::get('EmployeeRecords');
            try {
                $entity = $this->newEntity();
                $activation_code = getTokenValue(8);
                $hasher = new DefaultPasswordHasher();
                $timer = new Time();
                $entity->user_id = $data['user']['id'];
                $entity->device_id = $data['device_id'];
                $entity->activation_code = $hasher->hash($activation_code);
                $entity->activate_request = date("Y-m-d");
                $entity->expire_date = $timer->addDays(3);
                $this->save($entity);
                $user = $data['user'];
                if (!empty($user)) {
                    $profile = $tableProfiles->get($user['employee_record_id']);
                    if (!empty($profile) && !empty($profile['personal_email'])) {
                        $email = new Email('default');
                        $reg_message = 'নথি মোবাইল এ্যাপ-এ আপনাকে স্বাগতম।<br><br>
আপনার'. (isset($data['os'])?' '.h($data['os']).' ' :'') .'ডিভাইস '.(isset($data['device_id'])?' ('.h($data['device_id']).') ' :'').' থেকে নথি এ্যাপ ব্যবহারের জন্য রেজিষ্ট্রেশন প্রক্রিয়া সম্পন্ন করুন।
<br><br>Registration Code: <b>'.h($activation_code).' </b><br><br>

এই কোড-টি আপনার মোবাইলে নথি এ্যাপ-এর Registration Page থেকে প্রেরণ করুন। উল্লেখ্য এই প্রক্রিয়াটি প্রতিটা ডিভাইসের জন্য শুধুমাত্র একবারই করতে হবে।<br><br>

ধন্যবাদ,<br>
নথি টিম';
                        $email->emailFormat('html')->to(h($profile['personal_email']))->from('nothi@nothi.org.bd')->subject("Device registration")->send($reg_message);
                        return true;
                    } else {
                        throw new \Exception("Invalid email");
                    }
                } else {
                    throw new \Exception("Invalid user information");
                }
            } catch (\Exception $ex) {
                return $ex->getMessage();
            } catch (Exception $x) {
                return $x;
            }
        } else {
            return "Invalid request";
        }
    }
}
