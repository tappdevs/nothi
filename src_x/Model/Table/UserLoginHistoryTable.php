<?php

namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;
use Cake\Cache\Cache;
class UserLoginHistoryTable extends ProjapotiTable
{

    public function initialize(array $config) 
    {
        $conn = ConnectionManager::get('projapotiDb');
        $this->connection($conn);
        
        $this->displayField('name_bng');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
    }
    
    public function readLogin($office_unit_organogram = 0, $ip = null)
    {
        $query = $this->find();
        
        if(!empty($ip)){
            $query->where(['client_ip'=>$ip]);
        }
        
        if(!empty($employee_id)){
            $query->where(['office_unit_organogram'=>$office_unit_organogram]);
        }
        
        return $query->hydrate(false);  
    }
    
    public function updateLogout($office_unit_organogram = 0, $ip = null)
    {
        $browser = getBrowser();
        $condition = [];
        
        if(!empty($office_unit_organogram)){
            $condition['office_unit_organogram']=$office_unit_organogram;
        }
        
        if(!empty($ip)){
            $condition['client_ip']=$ip;
        }
        
        $condition['device_name']=$browser['platform'];
        $condition['browser_name']=$browser['name'];
        
        if($this->updateAll(['logout_time'=>date("Y-m-d H:i:s")], [$condition + ['logout_time IS NULL']])){
            return true;
        }
        
        return false;
    }
    public function updateLogoutbyEmployeeId($emp_id = 0, $ip = null)
    {
        $condition = [];

        if(!empty($emp_id)){
            $condition['employee_record_id']=$emp_id;
        }

        if(!empty($ip)){
            $condition['client_ip']=$ip;
        }

        if($this->updateAll(['logout_time'=>date("Y-m-d H:i:s")], [$condition + ['logout_time IS NULL']])){
            return true;
        }

        return false;
    }
    
    public function countLogin($employee_records)
    {
        return $this->find()->where(['employee_record_id IN'    => $employee_records,'date(login_time)' => date('Y-m-d')])->distinct(['employee_record_id'])->count();
    }

    public function countTotalLogin($office_id = 0 , $unit_id = 0, $organogram_id = 0 , $time = [] )
    {
        $query =  $this->find()->select(['office_unit_organogram']);


        if(!empty($office_id)){
            $query->where(['UserLoginHistory.office_id'=>$office_id]);
        }

        if(!empty($unit_id)){
            $query->where(['UserLoginHistory.office_unit_id'=>$unit_id]);
        }

        if(!empty($organogram_id)){
            $query->where(['UserLoginHistory.office_unit_organogram'=>$organogram_id]);
        }

        if(!empty($time)){
            $query->where(['date(login_time) >='=>$time[0]]);
            if(!empty($time[1])){
                $query->where(['date(login_time) <='=>$time[1]]);
            }
        }
        $query->group(['employee_record_id,date(login_time)']);

        return $query;
    }

     public function countLoginYesterday($employee_records)
    {
        return $this->find()->where(['employee_record_id IN'    => $employee_records ,'date(login_time) = DATE(SUBDATE(NOW(),1))'])->distinct(['employee_record_id'])->count();
    }
    
    public function getLogingHistory($office_id = 0, $unit_id = 0, $organogram_id = 0,$time = [], $order = [])
    {
        $query = $this->find()->select([
            'client_ip',
            'device_name',
            'browser_name',
            'login_time',
            'logout_time',
            'network_information',
            "designation"=>'organogram_name',
            "office_name"=>'office_name',
            "unit_name"=>'unit_name',
            'employee_record_id',
            'employee_name'
        ]);
        
        if(!empty($office_id)){
            $query->where(['UserLoginHistory.office_id'=>$office_id]);
        }
        
        if(!empty($unit_id)){
            $query->where(['UserLoginHistory.office_unit_id'=>$unit_id]);
        }
        
        if(!empty($organogram_id)){
            $query->where(['UserLoginHistory.office_unit_organogram'=>$organogram_id]);
        }
        
        if(!empty($time)){
            $query->where(['date(login_time) >='=>$time[0]]);
            if(!empty($time[1])){
                $query->where(['date(login_time) <='=>$time[1]]);
            }
        }
        $query->group(['date(login_time)','UserLoginHistory.office_unit_organogram']);
//        if(!empty($order)){
//            $query->order($order);
//        }
        return $query;
//        return $query->join([
//            'EmployeeOffices' => [
//                'table'         =>  'employee_offices',
//                'type'          =>  'inner',
//                'conditions'    =>  'EmployeeOffices.office_unit_organogram_id = UserLoginHistory.office_unit_organogram'
//            ]
//        ])->where(['EmployeeOffices.status'=>1])->order(['login_time ASC']);
    }
    /**
     *
     * @param int $ministry_id
     * @param int $layer_id
     * @param int $origin_id
     * @param int $office_id
     * @param int $unit_id
     * @param int $designation_id
     * @param array $time
     * @return object Build a query for login Count
     */
    public function getLoginCount ( $ministry_id = 0 , $layer_id = 0, $origin_id = 0,$office_id = 0, $unit_id = 0, $organogram_id = 0,$time = []){
        $query = $this->find();
        if(!empty($ministry_id)){
            $query->where(['ministry_id' => $ministry_id]);
        }
        if(!empty($layer_id)){
            $query->where(['layer_id' => $layer_id]);
        }
        if(!empty($origin_id)){
            $query->where(['origin_id' => $origin_id]);
        }
        if(!empty($office_id)){
            $query->where(['UserLoginHistory.office_id'=>$office_id]);
        }

        if(!empty($unit_id)){
            $query->where(['UserLoginHistory.office_unit_id'=>$unit_id]);
        }

        if(!empty($organogram_id)){
            $query->where(['UserLoginHistory.office_unit_organogram'=>$organogram_id]);
        }

        if(!empty($time)){
            $query->where(['date(login_time) >='=>$time[0]]);
            if(!empty($time[1])){
                $query->where(['date(login_time) <='=>$time[1]]);
            }
        }
       return $query->group(['UserLoginHistory.office_unit_organogram'])->order(['login_time desc']);
        
    }
     public function loginByDateRange($office_id = 0 , $unit_id = 0, $organogram_id = 0 , $time = [] )
    {
        $query =  $this->find();


        if(!empty($office_id)){
            $query->where(['UserLoginHistory.office_id'=>$office_id]);
        }

        if(!empty($unit_id)){
            $query->where(['UserLoginHistory.office_unit_id'=>$unit_id]);
        }

        if(!empty($organogram_id)){
            $query->where(['UserLoginHistory.office_unit_organogram'=>$organogram_id]);
        }

        if(!empty($time)){
            $query->where(['date(login_time) >='=>$time[0]]);
            if(!empty($time[1])){
                $query->where(['date(login_time) <='=>$time[1]]);
            }
        }

        return $query;
    }
    public function getLoginDataByDesignationAndIP($designation = [], $ip = [],$time= []){
        if(empty($designation) && empty($ip)){
            return;
        }
       $query = $this->find()->where(['office_unit_organogram IN' => $designation,'client_ip IN' => $ip]);

        if(!empty($time)){
            $query->where(['date(login_time) >='=>$time[0]]);
            if(!empty($time[1])){
                $query->where(['date(login_time) <='=>$time[1]]);
            }
        }
        return $query;
    }
    public function findbyTokendata($token,$device_id = '',$device_type = ''){
//         if (($query = Cache::read('token_' . $token, 'memcached')) === false) {
                $query =  $this->find()->where(['token'=>$token]);
                if(!empty($device_id))
                {
                    $query->where(['device_id' => $device_id]);
                }
                if(!empty($device_type))
                {
                    $query->where(['device_type' => $device_type]);
                }
               $query =  $query->first();
//                Cache::write('token_' . $token, $query, 'memcached');
//            }
            return $query;
    }
    public function checkbyTokendata($token,$device_id = '',$device_type = ''){
        $query =  $this->find()->where(['token'=>$token]);
        if(!empty($device_id))
        {
            $query->where(['device_id' => $device_id]);
        }
        if(!empty($device_type))
        {
            $query->where(['device_type' => $device_type]);
        }
        return $query->count();
    }
    public function getLoggedOfficesId($time = []){
        $query = $this->find('list',['valueField' => 'office_id']);
        if(!empty($time[0])){
            $query->where(['DATE(login_time) >=' => $time[0]]);
        }
        if(!empty($time[1])){
            $query->where(['DATE(login_time) <=' => $time[1]]);
        }
        return $query->group(['office_id'])->toArray();
    }
}
