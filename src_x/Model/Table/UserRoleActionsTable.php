<?php
namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\Validation\Validator;
use  Cake\Datasource\ConnectionManager;

/**
 * Articles Model
 */
class UserRoleActionsTable extends ProjapotiTable

{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $conn = ConnectionManager::get('projapotiDb');
        $this->connection($conn);

        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->table('user_role_actions');
        $this->belongsTo('UserRoles');
    }
}
