<?php
namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use Cake\Cache\Cache;
use  Cake\Datasource\ConnectionManager;

/**
 * Articles Model
 */
class UserRolesTable extends ProjapotiTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $conn = ConnectionManager::get('projapotiDb');
        $this->connection($conn);

        $this->table('user_roles');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
    }

    public function getUserTypes()
    {
        $userTypes = $this->find();
        $uts = array();
        foreach ($userTypes as $ut) {
            if ($ut->id) {
                $uts[$ut->id] = $ut->name;
            }
        }

        return $uts;
    }

    public function getUserTypeIdsAndNames()
    {
        $userTypes = $this->find();
        $uts = array();
        foreach ($userTypes as $ut) {
            $row = array();
            $row['id'] = $ut->id;
            $row['name'] = $ut->name;

            $uts[] = $row;
        }

        return $uts;
    }

    /**
     * Used to check permissions of group
     *
     * @access public
     * @param string $controller controller name
     * @param string $action action name
     * @param integer $userGroupID group id
     * @return boolean
     */
    public function isUserGroupAccess($controller, $action, $userGroupID,$is_admin =0)
    {
        if (!PERMISSIONS) {
            return true;
        }
        $access = str_replace(' ', '', ucwords(str_replace('_', ' ', $controller))) . '/' . $action;
        return $this->getPermissions($userGroupID,$is_admin,strtolower($access));

    }

    /**
     * Used to check permissions of guest group
     *
     * @access public
     * @param string $controller controller name
     * @param string $action action name
     * @return boolean
     */
    public function isGuestAccess($controller, $action)
    {
        if (PERMISSIONS) {
            return $this->isUserGroupAccess($controller, $action, GUEST_GROUP_ID);
        } else {
            return true;
        }
    }

    /**
     * Used to check permissions from cache or database of a group
     *
     * @access public
     * @param integer $userGroupID group id
     * @param $is_admin boolean define admin or not
     * @param $access string define the url user requested
     * @return bool
     */
    public function getPermissions($userGroupID = 0,$is_admin = 0,$access = '')
    {
        if(($userGroupID == 0 || $userGroupID == 3) && empty($is_admin)){
            // for normal/guest user these url not accessible
            $can_not_access = getOfficeAdminAccess()+getSuperAdminAccess()+getRobinLayerAccess();
            if(in_array($access,$can_not_access)){
                return false;
            }else{
                return true;
            }
        }
        else if($userGroupID == 2){
            $can_access = getRobinLayerAccess();
            if(in_array($access,$can_access)){
                return true;
            }else{
                return false;
            }
        }
        else if(!empty($is_admin)){
            //office admin
            $can_access = getOfficeAdminAccess();
            $can_not_access = getSuperAdminAccess()+getRobinLayerAccess();
            if(in_array($access,$can_not_access)){
                if(in_array($access,$can_access)){
                    return true;
                }
                return false;
            }else{
                return true;
            }
        }
        else{
            return true;
        }
    }
}
