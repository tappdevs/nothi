<?php
namespace App\Model\Table;

use App\Model\Table\ProjapotiTable;
use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;
use  Cake\Datasource\ConnectionManager;
/**
 * Articles Model
 */
class UsersTable extends ProjapotiTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $conn = ConnectionManager::get('projapotiDb');
        $this->connection($conn);
        
        $this->table('users');
        $this->primaryKey('id');
        $this->displayField('username');
        $this->addBehavior('Timestamp');
        $this->belongsTo('UserRoles');
        $this->addBehavior('Cake3Upload.Upload', [
                'fields' => [
                    'photo' => [
                        'path' => 'upload/Users/:y/:m/:md5'
                    ]
                ]
            ]
        );
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        return $validator
            ->notEmpty('username', 'A username is required')
            ->notEmpty('password', 'A password is required')
            ->notEmpty('role', 'A role is required')
            ->add('user_alias', [
                'length' => [
                    'rule' => ['lengthBetween', 4, 50],
                    'message' => ' লগইন নেম   টি কমপক্ষে  ৪ ক্যারেক্টার হতে হবে ',
                ]
            ])
            ->add('user_alias', 'isUnique',
                [
                'rule' => function($data, $provider) {

                if(!empty($provider['data']['id'])){
                    if ($this->find()->where(['user_alias' => $data, 'id <>'=>$provider['data']['id']])->orWhere(['username' => $data, 'id <>'=>$provider['data']['id']])->count() > 0){
                        throw new \Exception("দুঃখিত! লগইন নেম ব্যবহার করা হয়েছে");
                    }
                }else{
                    if ($this->find()->where(['user_alias' => $data])->orWhere(['username' => $data])->count() > 0){
                        throw new \Exception("দুঃখিত! লগইন নেম ব্যবহার করা হয়েছে");
                    }
                }

                    return true;
                },
                    'message' => __("দুঃখিত! লগইন নেম ব্যবহার করা হয়েছে")
            ]);
//            ->add('user_alias', 'notNumeric',
//                [
//                'rule' => function($data) {
//
//                    if(is_numeric($data)){
//                        throw new \Exception("দুঃখিত! লগিন নেম শুধুমাত্র নিউমেরিক হতে পারবে না। ");
//                    }
//
//                    return true;
//                },
//                    'message' => __("দুঃখিত! লগিন নেম শুধুমাত্র নিউমেরিক হতে পারবে না। ")
//            ]);
    }

    public function getUserInfobyRole($roleId = "")
    {
        if (!empty($roleId)) {
            return $this->find()->where(["user_role_id" => $roleId])->count();
        }
        return 0;
    }
    
    public function returnHash($password){
        return (new DefaultPasswordHasher)->hash($password);
    }

    public function checkPassword($password, $oldPassword){
        return (new DefaultPasswordHasher)->check($password, $oldPassword);
    }
    
    public function validationPassword(Validator $validator )
    {

        $validator
            ->add('current_password','custom',[
                'rule'=>  function($value, $context){
                    $user = $this->get($context['data']['id']);
                 
                    if ($user) {
                        
                        if ((new DefaultPasswordHasher)->check($value, $user->password)) {
                            
                           
                            return true;
                        }
                    }
                  
                    return false;
                },
                'message'=>' বর্তমান পাসওয়ার্ড    মিলছে না। আবার চেষ্টা করুন।  ',
            ])
            ->notEmpty('current_password')
            ->add('password', [
                'length' => [
                    'rule' => ['lengthBetween', 4, 80],
                    'message' => ' পাসওয়ার্ড  টি কমপক্ষে  ৪ ক্যারেক্টার হতে হবে ',
                ]
            ])
            ->add('cpassword',[
                'match'=>[
                    'rule'=> ['compareWith','password'],
                    'message'=>' পাসওয়ার্ড   মিলছে না। আবার চেষ্টা করুন। ',
                ]
            ])->add('password','custom',[
                'rule'=>  function($value, $context){
                    if($this->passwordValidation($value)===0){
                        return false;
                    }else if ($this->passwordValidation($value)===1){
                        return false;
                    }
                    return true;
                },
                'message'=>'পাসওয়ার্ড নূন্যতম ৬ অক্ষরের হতে হবে। অন্তত একটি A-Z অথবা a-z থাকতে হবে। ',
            ])
            ->notEmpty('password');
        

        return $validator;
    }

    public function checkAlias($data){

        if($data['user_alias'] != $data['username'] && strlen($data['user_alias']) == 12 && is_numeric($data['user_alias'])){
            throw new \Exception("দুঃখিত! ১২ ডিজিটের নম্বর হিসেবে ইউজার নেম দেয়া যাবে না। ");
        }
        if(!empty($data['id'])){
            if ($this->find()->where(['user_alias' => $data['user_alias'], 'id <>'=>$data['id']])->orWhere(['username' => $data['user_alias'], 'id <>'=>$data['id']])->count() > 0){
                throw new \Exception("দুঃখিত! লগইন নেম ব্যবহার করা হয়েছে");
            }
        }else{
            if ($this->find()->where(['user_alias' => $data['user_alias']])->orWhere(['username' => $data['user_alias']])->count() > 0){
                throw new \Exception("দুঃখিত! লগইন নেম ব্যবহার করা হয়েছে");
            }
        }

        return true;
    }

    public function getEmployeeRecordIdByUsername($username = '')
    {
        if (empty($username))
            return;
        return $this->find()->select(['employee_record_id'])->where(['username' => $username]);
    }
    /*
     * @response bool - 0 and 1 error and true means validation successful.
     * I know weird logic but I foud it like that
     * For validation success check always use === true
     */
    public function passwordValidation($password){
        //check the length
        if(strlen($password)<6){
            return 0;
        }

        //check all a-z
        if(!preg_match('/[a-zA-Z]/', $password)){
            return 1;
        }

        return true;
    }
    public function getData($select = [], $conditions = [], $order = []){
        $query = $this->find();
        if(!empty($select)){
            $query->select($select);
        }
        if(!empty($conditions)){
            $query->where($conditions);
        }
        if(!empty($order)){
            $query->order($order);
        }
        return $query;
    }
}
