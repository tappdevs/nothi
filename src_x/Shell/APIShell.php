<?php

namespace App\Shell;

use Cake\Console\Shell;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use App\Controller\ProjapotiController;
use Cake\Datasource\ConnectionManager;
use Cake\I18n\I18n;
use Exception;
use Cake\Cache\Cache;

/**
 * Report shell command.
 */
class APIShell extends ProjapotiShell
{

    /**
     * main() method.
     *
     * @return bool|int Success or error code.
     */
    public function main()
    {

    }

    private function commands($office_id, $browser, $auth_user)
    {
        $command = "bin/cake API saveLoginHistory {$office_id} {$browser} {$auth_user}> /dev/null 2>&1 &";
    }

    public function saveLoginHistory($time = '')
    {
        try {
            if (!empty($time)) {
//                $this->out($time);
                $employee_office = Cache::read('saveLoginHistory_emp_' . $time, 'memcached');
                $browser = Cache::read('saveLoginHistory_browser_' . $time, 'memcached');
                $auth_user = Cache::read('saveLoginHistory_auth_' . $time, 'memcached');
                if (!empty($employee_office)) {
                    Cache::delete('saveLoginHistory_emp_' . $time, 'memcached');
                    Cache::delete('saveLoginHistory_browser_' . $time, 'memcached');
                    Cache::delete('saveLoginHistory_auth_' . $time, 'memcached');
                } else {
                    $this->out('No data');
                    return;
                }
            }
            $browser = !empty($browser) ? $browser : getBrowser();
            if (!empty($employee_office)) {
                $Officestbl = TableRegistry::get('Offices');

                $table_login_history = TableRegistry::get('UserLoginHistory');
                $EmployeeRecordsTable = TableRegistry::get('EmployeeRecords');


                if (!empty($browser['ip'])) {
                    $ip = $browser['ip'];
                } else {
                   $ip = getIP();
                }
                if (!empty($browser['is_mobile']) && $browser['is_mobile'] == 1 && !empty($browser['user_id']) && !empty($browser['emp_rcrd_id'])) {
                    // Comes From API
                    $auth_user['employee_record_id'] = $browser['emp_rcrd_id'];
                    $auth_user['id'] = $browser['user_id'];
                } else {
                    $auth_user = !empty($auth_user) ? $auth_user : [];
                }
                if (empty($auth_user)) {
                    $this->out('empty auth');
                    return;
                }
                $Employee_personal_info = $EmployeeRecordsTable->getEmployeeInfoByRecordId($auth_user['employee_record_id']);
                $allOrganogramIds = [];
                $allOfficeIds = [];
                if (!empty($employee_office)) {
                    foreach ($employee_office as $key => $value) {
                        $loginentity = $table_login_history->newEntity();
                        $allInfo = $Officestbl->find()->select(['office_ministry_id', 'office_origin_id', 'office_layer_id'])->where(['id' => $value['office_id']])->first();
                        $allOfficeIds[] = $value['office_id'];
                        if(isset($value['office_unit_organogram_id'])){
                            $allOrganogramIds[] = $value['office_unit_organogram_id'];
                        }

                        $loginentity->client_ip = $ip;
                        $loginentity->employee_record_id = $value['employee_record_id'];
                        $loginentity->office_id = $value['office_id'];
                        $loginentity->office_unit_id = $value['office_unit_id'];
                        $loginentity->office_unit_organogram = $value['office_unit_organogram_id'];
                        $loginentity->office_name = $value['office_name'];
                        $loginentity->unit_name = $value['unit_name'];
                        $loginentity->organogram_name = $value['designation'];
                        $loginentity->login_time = date("Y-m-d H:i:s");
                        $loginentity->device_name = $browser['platform'];
                        $loginentity->browser_name = $browser['name'];
                        $loginentity->is_mobile = isset($browser['is_mobile']) ? $browser['is_mobile'] : 0;

                        $loginentity->user_id = $auth_user['id'];
                        $loginentity->employee_name = isset($Employee_personal_info['name_bng']) ? $Employee_personal_info['name_bng'] : '';
                        $loginentity->employee_email = isset($Employee_personal_info['personal_email']) ? $Employee_personal_info['personal_email'] : NULL;
                        $loginentity->mobile_number = isset($Employee_personal_info['personal_mobile']) ? $Employee_personal_info['personal_mobile'] : NULL;
                        // Check if it is comes from API
                        if (!empty($browser['token']))
                            $loginentity->token = $browser['token'];
                        if (!empty($browser['device_type']))
                            $loginentity->device_type = $browser['device_type'];
                        else
                            $loginentity->device_type = substr($browser['name'] . ' - ' . $browser['version'] . ' - ' . $browser['platform'], 0, 49);
                        if (!empty($browser['device_id']))
                            $loginentity->device_id = $browser['device_id'];
                        else
                            $loginentity->device_id = $ip;
                        // Check if it is comes from API

                        $loginentity->ministry_id = $allInfo['office_ministry_id'];
                        $loginentity->origin_id = $allInfo['office_origin_id'];
                        $loginentity->layer_id = $allInfo['office_layer_id'];

                        $loginentity->network_information = json_encode(array(
                            "ClientIp" => $loginentity->client_ip,
                            "UserAgent" => $browser['userAgent'],
                            "DeviceId" => isset($browser['device_id']) ? $browser['device_id'] : (isset($value['fingerprint']) ? $value['fingerprint'] : ''),
                            "NetworkName" => isset($browser['network_name']) ? $browser['network_name'] : '',
                            "Browser" => $browser['name'],
                            "Version" => $browser['version'],
                            "Device" => $browser['platform'],
                            "fingerprint" => isset($value['fingerprint']) ? $value['fingerprint'] : '',
                            "Time" => date("Y-m-d H:i:s"),
                        ));

                        $table_login_history->save($loginentity);
                        //retry potrojari pending
//                        $commandReceiver = ROOT . "/bin/cake cronjob potrojariReceiverSentAll {$value['office_id']} {$value['office_unit_organogram_id']} 1> /dev/null 2>&1 &";
//
//                        exec($commandReceiver);
//
//                        $commandOnulipi = ROOT . "/bin/cake cronjob potrojariOnulipiSentAll {$value['office_id']} {$value['office_unit_organogram_id']}  1> /dev/null 2>&1 &";
//
//                        exec($commandOnulipi);
                    }
                }
            }
            if(!empty($allOrganogramIds)){
               $isAdmin = TableRegistry::get('OfficeUnitOrganograms')->getAll(['id IN' => $allOrganogramIds,'is_admin' => 1])->count();
               if($isAdmin > 0){
                   if(!empty($allOfficeIds)){
                       foreach ($allOfficeIds as $office_id){
                           $run_next_request = 0;
                           $command = ROOT . "/bin/cake cronjob updatePerformanceTables {$office_id} {$run_next_request}> /dev/null 2>&1 &";
                           exec($command);
                           sleep(60);
                       }
                   }
               }
            }
//            $this->out('Done');
            return true;
        } catch (\Exception $ex) {
            $this->out($ex->getMessage());
            return $ex->getMessage();
        }
        return false;
    }

    public function apiProtikolpoTransferByStart()
    {

        TableRegistry::remove('ProtikolpoSettings');
        TableRegistry::remove('ProtikolpoLog');
        $tableProtikolpoSettings = TableRegistry::get('ProtikolpoSettings');
        $tableProtikolpoLog = TableRegistry::get('ProtikolpoLog');
        $tableEmployeeOffices = TableRegistry::get('EmployeeOffices');
        $tableOfficeUnitOrganograms = TableRegistry::get('OfficeUnitOrganograms');
        $tableEmployeeRecords = TableRegistry::get('EmployeeRecords');

        $nonActiveProtikolpos = $tableProtikolpoSettings
            ->find()
            ->select([
                'id',
                'office_id',
                'unit_id',
                'designation_id',
                'employee_record_id',
                'protikolpos',
                'selected_protikolpo',
                'start_date',
                'end_date',
                'is_show_acting',
            ])
            ->hydrate(false)
            ->where(['active_status' => 0, 'selected_protikolpo >' => 0, 'DATE(start_date) <=' => Date("Y-m-d")])
            ->toArray();

        if (!empty($nonActiveProtikolpos)) {
            foreach ($nonActiveProtikolpos as $key => $value) {
                //check employee office table
                $isExists = $tableEmployeeOffices->getEmployeeInfo($value['office_id'], $value['unit_id'], $value['designation_id']);

                if (!empty($isExists)) {
                    $employeeInfo = jsonA($value['protikolpos']);
                    $selectedInfo = !empty($employeeInfo['protikolpo_'.$value['selected_protikolpo']]) ? $employeeInfo['protikolpo_'.$value['selected_protikolpo']] : [];

                    if (!empty($selectedInfo)) {
                        if ($value['is_show_acting'] == 1) {
                            $isExists['incharge_label'] = 'ভারপ্রাপ্ত';
                        }
                        $isExistsNew = $tableEmployeeOffices->getEmployeeInfo($selectedInfo['office_id'],
                            $selectedInfo['office_unit_id'], $selectedInfo['designation_id']);
                        if(!$isExistsNew){
                            continue;
                        }
                        //unassign from employee office table
                        $unassign = $tableEmployeeOffices->unAssignDesignation($isExists['id']);

                        if ($unassign) {
                            //assign into employee office table
                            $employeeAssignedInfo = $tableEmployeeRecords->get(
                                $selectedInfo['employee_record_id']
                            );
                            $designationInfo = $tableOfficeUnitOrganograms->get(
                                $value['designation_id']
                            );

                            $assign = $tableEmployeeOffices->assignDesignation(
                                [
                                    'employee_record_id' => $employeeAssignedInfo['id'],
                                    'identification_number' => $employeeAssignedInfo['identity_no'],
                                    'office_id' => $value['office_id'],
                                    'office_unit_id' => $value['unit_id'],
                                    'office_unit_organogram_id' => $value['designation_id'],
                                    'designation' => $designationInfo['designation_bng'],
                                    'incharge_label' => $isExists['incharge_label'],
                                    'joining_date' => date("Y-m-d H:i:s"),
                                    'protikolpo_status' => 1,
                                ]
                            );
                            if($assign) {
                                //update active status 1 in protikolpo settings
                                $tableProtikolpoSettings->updateAll(['active_status'=>1],['id'=>$value['id']]);

                                $employeeunAssignedInfo = $tableEmployeeRecords->get(
                                    $value['employee_record_id']
                                );

                                $protikolpoLogEntity = $tableProtikolpoLog->newEntity();
                                $protikolpoLogEntity->protikolpo_id = $value['id'];
                                $protikolpoLogEntity->protikolpo_start_date = date("Y-m-d H:i:s");
                                $protikolpoLogEntity->employee_office_id_from_name = $employeeunAssignedInfo['name_bng'];
                                $protikolpoLogEntity->employee_office_id_to_name = $employeeAssignedInfo['name_bng'];
                                $protikolpoLogEntity->protikolpo_status = 1;
                                $tableProtikolpoLog->save($protikolpoLogEntity);
                            }
                        }
                    }

                }
                if(!empty($employeeAssignedInfo['name_bng'])) {
                    //done and go for next
//                    $this->out("Transfer : " . $value['designation_id'] . ' -> ' . $employeeAssignedInfo['name_bng']);
                }
            }
        }

        $this->out("Transfer End");
        $this->apiProtikolpoTransferByEnd();
    }

    public function apiProtikolpoTransferByEnd($force = false, $employee_id = 0, $protikolpo_setting_id = 0)
    {
        TableRegistry::remove('ProtikolpoSettings');
        TableRegistry::remove('ProtikolpoLog');
        $tableProtikolpoSettings = TableRegistry::get('ProtikolpoSettings');
        $tableProtikolpoLog = TableRegistry::get('ProtikolpoLog');
        $tableEmployeeOffices = TableRegistry::get('EmployeeOffices');
        $tableOfficeUnitOrganograms = TableRegistry::get('OfficeUnitOrganograms');
        $tableEmployeeRecords = TableRegistry::get('EmployeeRecords');

        $conditions = [];
        $conditions['selected_protikolpo >'] = 0;
        if ($protikolpo_setting_id == 0) {
            if($force == false){
                $conditions['DATE(end_date)'] = Date("Y-m-d",strtotime("-1 day"));
            }
        }
        if($protikolpo_setting_id != 0){
            $conditions['id'] = $protikolpo_setting_id;
        }
        if($employee_id != 0){
            $conditions['employee_record_id'] = $employee_id;
        }

        $nonActiveProtikolpos = $tableProtikolpoSettings
            ->find()
            ->select([
                'id',
                'office_id',
                'unit_id',
                'designation_id',
                'employee_record_id',
                'protikolpos',
                'selected_protikolpo',
                'start_date',
                'end_date',
                'is_show_acting'
            ])
            ->hydrate(false)
            ->where(['active_status' => 1])
            ->where($conditions)
            ->toArray();

        if (!empty($nonActiveProtikolpos)) {
            foreach ($nonActiveProtikolpos as $key => $value) {
                //check employee office table
                $isExists = $tableEmployeeOffices
                    ->getEmployeeInfo($value['office_id'], $value['unit_id'], $value['designation_id']);

                if (!empty($isExists)) {
//                    $this->out('Start stopping protikolpo ');
                    $employeeInfo = jsonA($value['protikolpos']);
                    $selectedInfo = !empty($employeeInfo['protikolpo_'.$value['selected_protikolpo']]) ? $employeeInfo['protikolpo_'.$value['selected_protikolpo']] : [];

                    if (!empty($selectedInfo)
                        && $selectedInfo['employee_record_id'] == $isExists['employee_record_id']) {
                        //unassign from employee office table
                        $unassign = $tableEmployeeOffices->unAssignDesignation($isExists['id']);

                        if ($unassign) {
                            //assign into employee office table
                            $employeeAssignedInfo = $tableEmployeeRecords->get(
                                $value['employee_record_id']
                            );
                            $designationInfo = $tableOfficeUnitOrganograms->get(
                                $value['designation_id']
                            );


                            $previous = $tableEmployeeOffices
                                ->find()->where(['office_id' => $value['office_id'], 'office_unit_organogram_id' => $value['designation_id'],
                                    'EmployeeOffices.status' => 0,'EmployeeOffices.employee_record_id'=>$employeeAssignedInfo['id']])->order(['id'=>'DESC'])->first();

                            $assign = $tableEmployeeOffices->assignDesignation(
                                [
                                    'employee_record_id' => $employeeAssignedInfo['id'],
                                    'identification_number' => $employeeAssignedInfo['identity_no'],
                                    'office_id' => $value['office_id'],
                                    'office_unit_id' => $value['unit_id'],
                                    'office_unit_organogram_id' => $value['designation_id'],
                                    'designation' => $designationInfo['designation_bng'],
                                    'incharge_label' => !empty($previous)?$previous['incharge_label']:($value['is_show_acting']==1?$isExists['incharge_label']:''),
                                    'joining_date' => date("Y-m-d H:i:s"),
                                    'protikolpo_status' => 0,
                                ]
                            );
                            if($assign) {
                                //update active status 1 in protikolpo settings
                                $tableProtikolpoSettings->updateAll(['active_status'=>0,
                                    'employee_record_id'=>0,
                                    'employee_name'=>'',
                                    'start_date'=>null,
                                    'end_date'=>null,
                                    'selected_protikolpo'=>0,
                                    'is_show_acting'=>0,
                                    ],['id'=>$value['id']]);

                                $protikolpoLogEntity = $tableProtikolpoLog->find()->where(['protikolpo_id'=>$value['id']])->order(['id desc'])->first();

                                if(!empty($protikolpoLogEntity)) {
//                                    $employeeunAssignedInfo = $tableEmployeeRecords->get(
//                                        $isExists['employee_record_id']
//                                    );

                                    $tableProtikolpoLog->updateAll([
                                        'protikolpo_end_date' => date("Y-m-d H:i:s"),
                                        'protikolpo_status' => 0,
                                    ], ['id' => $protikolpoLogEntity['id']]);
                                }
                            }
                        }
                    }else{
                        $tableProtikolpoSettings->updateAll(['active_status'=>0,
                            'employee_record_id'=>0,
                            'employee_name'=>'',
                            'start_date'=>null,
                            'end_date'=>null,
                            'selected_protikolpo'=>0,
                        ],['id'=>$value['id']]);
                    }
                }else{
                    $tableProtikolpoSettings->updateAll(['active_status'=>0,
                        'employee_record_id'=>0,
                        'employee_name'=>'',
                        'start_date'=>null,
                        'end_date'=>null,
                        'selected_protikolpo'=>0,
                    ],['id'=>$value['id']]);
                }
                if(!empty($employeeAssignedInfo['name_bng'])) {
                    //done and go for next
//                    $this->out("Transfer Back: " . $value['designation_id'] . ' <- ' . $employeeAssignedInfo['name_bng']);
                }
            }
        }
//        if($protikolpo_setting_id == 0) {
//            $dbAccess = ConnectionManager::get('NothiAccessDb');
//            $dbAccess->execute("TRUNCATE sessions");
//        }

//        $this->out("Transfer Back End");
    }
}