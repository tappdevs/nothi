<?php

namespace App\Shell;

use App\Controller\ArchiveController;
use Cake\Console\Shell;
use Cake\ORM\TableRegistry;
use App\Controller\ProjapotiController;
use Cake\Datasource\ConnectionManager;
use Cake\I18n\I18n;
use Cake\Network\Email\Email;
use Cake\I18n\Number;
use Cake\I18n\Time;
use Cake\Routing\Route\Route;
use Cake\Routing\Router;
use Exception;
use mikehaertl\wkhtmlto\Pdf;
use Cake\Cache\Cache;

/**
 * Report shell command.
 */
class ArchivingShell extends Shell
{

    /**
     * main() method.
     *
     * @return bool|int Success or error code.
     */
    private function switchOffice($office_id, $officeName)
    {
        $officeDomainTable = TableRegistry::get('OfficeDomains');
        $officeDomain = $officeDomainTable->find()->where(['office_id' => $office_id])->first();

        if (empty($officeDomain)) {
            throw new Exception("অফিস ডাটাবেজ পাওয়া যায় নি! সাপোর্ট টিমের সাথে যোগাযোগ করুন");
        }

        ConnectionManager::drop('default');
        ConnectionManager::drop($officeName);
        ConnectionManager::config($officeName,
            [
                'className' => 'Cake\Database\Connection',
                'driver' => 'Cake\Database\Driver\Mysql',
                'persistent' => false,
                'host' => $officeDomain['domain_host'],
                'username' => $officeDomain['domain_username'],
                'password' => $officeDomain['domain_password'],
                'database' => $officeDomain['office_db'],
                'encoding' => 'utf8',
                'cacheMetadata' => true,
            ]
        );

        ConnectionManager::alias($officeName, 'default');
    }

    private function commands($office_id, $potrojari_id) {
        //$command = "bin/cake report potrojariReceiverSent {$office_id} {$potrojari_id}> /dev/null 2>&1 &";
    }

    public function archiveAllNothi() {
        $nothi_archive_requests_table = TableRegistry::get('NothiArchiveRequests');
        $nothi_archive_requests_data = $nothi_archive_requests_table->find()->where(['status' => 1]);
        if ($nothi_archive_requests_data->count() > 0) {
            foreach ($nothi_archive_requests_data as $key => $nothi_archive_data) {
                $nothi_archive_data['status'] = 2;
                $nothi_archive_requests_table->save($nothi_archive_data);

                $return_status = $this->archive_data($nothi_archive_data->office_id, $nothi_archive_data->nothi_master_id);

                if ($return_status) {
                    $nothi_archive_data['status'] = 3;
                    $nothi_archive_requests_table->save($nothi_archive_data);

                    $this->out('Archive Done');
                } else {
                    $nothi_archive_data['status'] = 4;
                    $nothi_archive_requests_table->save($nothi_archive_data);

                    $this->out('Archive Failed');
                }
            }
        } else {
            $this->out('No data found for archive');
        }
    }

    public function archive_data($office_id, $nothi_master_id) {
        try {
            $this->switchOffice($office_id, 'OfficeDb');
            $connection = ConnectionManager::get('default');
            $table_list['nothi'] = [
                'note_initialize' => 'nothi_masters_id',
                'nothi_dak_potro_maps' => 'nothi_masters_id',
                'nothi_data_change_history' => 'nothi_master_id',
                'nothi_masters_dak_map' => 'nothi_masters_id',
                'nothi_master_current_users' => 'nothi_master_id',
                'nothi_master_movements' => 'nothi_master_id',
                'nothi_master_permissions' => 'nothi_masters_id',
                'nothi_notes' => 'nothi_master_id',
                'nothi_note_attachments' => 'nothi_master_id',
                'nothi_note_attachment_refs' => 'nothi_master_id',
                'nothi_note_permissions' => 'nothi_masters_id',
                'nothi_note_sheets' => 'nothi_master_id',
                'nothi_note_signatures' => 'nothi_master_id',
                'nothi_parts' => 'nothi_masters_id',
                'nothi_potros' => 'nothi_master_id',
                'nothi_potro_attachments' => 'nothi_master_id',
                'potro_flags' => 'nothi_master_id',
            ];
			$table_list['dak'] = [
				'dak_attachments' => 'dak_id',
				'dak_daptoriks' => 'id',
				'dak_movements' => 'dak_id',
				'dak_nagoriks' => 'id',
				'dak_users' => 'dak_id',
				'dak_user_actions' => 'dak_id',
			];
            $nothi_masters_initiate = TableRegistry::get("NothiMasters")->exists(['id' => $nothi_master_id]);
            if ($nothi_masters_initiate) {
				$nothi_dak_porto_maps = TableRegistry::get('nothi_dak_potro_maps')->find()->where(['nothi_masters_id' => $nothi_master_id])->toArray();

				### Nothi archive start... ##
                $table_name = 'nothi_masters';
                $queries[] = "CREATE TABLE IF NOT EXISTS `archive_$table_name` LIKE `$table_name`;";
                $queries[] = "INSERT `archive_$table_name` SELECT * FROM `$table_name`  WHERE id = $nothi_master_id;";
                $queries[] = "DELETE FROM `$table_name`  WHERE id = $nothi_master_id;";
                foreach ($table_list['nothi'] as $table_name => $key) {
                    $queries[] = "CREATE TABLE IF NOT EXISTS `archive_$table_name` LIKE `$table_name`;";
                    $queries[] = "INSERT `archive_$table_name` SELECT * FROM `$table_name`  WHERE $key = $nothi_master_id;";
                    $queries[] = "DELETE FROM `$table_name`  WHERE $key = $nothi_master_id;";
                }
				### Nothi archive end ##

				### Dak archive start... ##
				$dak_ids = [];
				if ($nothi_dak_porto_maps) {
                	foreach ($nothi_dak_porto_maps as $dak_porto_map) {
						$dak_ids[$dak_porto_map['dak_type']][] = $dak_porto_map['dak_id'];
					}
				}
				foreach ($table_list['dak'] as $table_name => $key) {
					$queries[] = "CREATE TABLE IF NOT EXISTS `archive_$table_name` LIKE `$table_name`;";
					foreach ($dak_ids as $dak_type => $dak_id_list) {
						foreach($dak_id_list as $dak_id) {
							if ($dak_id > 0) {
								if ($table_name == 'dak_daptoriks' || $table_name == 'dak_nagoriks') {
									if (($table_name == 'dak_daptoriks' && $dak_type == 'Daptorik') || ($table_name == 'dak_nagoriks' && $dak_type == 'Nagorik')) {
										$queries[] = "INSERT `archive_$table_name` SELECT * FROM `$table_name`  WHERE id = $dak_id;";
										$queries[] = "DELETE FROM `$table_name`  WHERE id = $dak_id;";
									}
								} else {
									$queries[] = "INSERT `archive_$table_name` SELECT * FROM `$table_name`  WHERE dak_type = '" . $dak_type . "' and dak_id = $dak_id;";
									$queries[] = "DELETE FROM `$table_name`  WHERE dak_type = '" . $dak_type . "' and dak_id = $dak_id;";
								}

							}
						}
					}
				}
				### Dak archive end ##
                $connection->transactional(function ($connection) use ($queries) {
                    foreach ($queries as $query) {
                        $connection->execute($query);
                    }
                });

                return 1;
            }
        } catch (\Exception $exception) {
            $connection->rollback();
            $this->out($exception->getMessage());
            return 0;
        }

    }
}