<?php

namespace App\Shell;

use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
use Cake\Network\Email\Email;
use Cake\I18n\Time;
use Cake\Routing\Router;
use mikehaertl\wkhtmlto\Pdf;
use Cake\Cache\Cache;

/**
 * Cronjob shell command.
 */
class CronjobShell extends ProjapotiShell
{
    var $retryRec = [];
    var $retryOnulipi = [];
    var $ignoreRec = [];
    var $ignoreOnu = [];

    /**
     * main() method.
     *
     * @return bool|int Success or error code.
     */
    public function main()
    {

    }

    private function commands($office_id, $potrojari_id)
    {
        $command = "bin/cake cronjob potrojariReceiverSent {$office_id} {$potrojari_id}> /dev/null 2>&1 &";
    }

    public function potrojariReceiverSent($office_id, $potrojari_id = 0, $domain = '', $force = 0)
    {
        set_time_limit(0);

        $this->switchOffice($office_id, 'cronOffice');
        TableRegistry::remove('Potrojari');
        TableRegistry::remove('PotrojariReceiver');
        TableRegistry::remove('PotrojariAttachments');
        $potrojariTable = TableRegistry::get('Potrojari');
        $potrojariRecieverTable = TableRegistry::get('PotrojariReceiver');
        $table_instance_pa = TableRegistry::get('PotrojariAttachments');
        $potrojari = $potrojariTable->get($potrojari_id);

        $query = $potrojariRecieverTable->find();
        if (!empty($potrojari_id)) {
            $query->where(['potrojari_id' => $potrojari_id]);
        }
        if(!empty($force)){
            $query->where(['retry_count <=' => 2,'is_sent' => 0]);
        }else{
            $query->where(['run_task' => 0, 'retry_count <=' => 2, 'is_sent' => 0]);
        }

        $receivers = $query->toArray();

        // if all receiver got potrojari item , we will notify by potrojari  receiver_sent column.
        if (empty($receivers)) {
            $potrojariTable->updateAll(['receiver_sent' => 1], ['id' => $potrojari_id]);
            ConnectionManager::drop('default');
            ConnectionManager::drop('cronOffice');
            $this->out('receiver_sent = 1');
            return true;
        }
        // if potrojari happen before yesterday then we need to deny that request
        $weekBefore = date('Y-m-d H:i:s', strtotime("-7 days"));
        if ($force == 0 && $potrojari['potrojari_date']->format("Y-m-d H:i:s") < $weekBefore) {
            $potrojariSent = [
                'potro_status' => 'Sent',
                'is_sent' => 1,
                'run_task' => 1,
                'retry_count' => intval($receivers[0]['retry_count']) + 1,
                'modified' => date("Y-m-d H:i:s"),
                'task_reposponse' => 'Failed'
            ];
            $potrojariRecieverTable->updateAll($potrojariSent, ['potrojari_id' => $potrojari_id, 'is_sent' => 0]);
            $this->out('before a week');
            return true;
        }
        // if potrojari happen before yesterday then we need to deny that request

        $attachments = $table_instance_pa->find()->where(['potrojari_id' => $potrojari->id])->toArray();
        // Potrojari Attachment can be text ,'attachment_type <> ' => 'text' -- omitted
        foreach ($receivers as $k => $receiver) {
            try {
                if (empty($receiver['receiving_office_id'])) {

                    if (!empty($receiver['receiving_officer_email']) && $receiver['receiving_officer_email']
                        != 'undefined') {
                        try {
                            $meta = $potrojari->meta_data;
                            $margin = !empty($meta) ? json_decode($meta, true) : [];
                            $margin['margin_top'] = isset($margin['margin_top']) ? doubleval($margin['margin_top'])
                                : 1;
                            $margin['margin_right'] = isset($margin['margin_right'])
                                ? doubleval($margin['margin_right']) : 0.75;
                            $margin['margin_bottom'] = isset($margin['margin_bottom'])
                                ? doubleval($margin['margin_bottom']) : .075;
                            $margin['margin_left'] = isset($margin['margin_left'])
                                ? doubleval($margin['margin_left']) : 0.75;
                            $margin['orientation'] = !empty($margin['orientation'])
                                ? ($margin['orientation']) : 'portrait';

                            $margin['banner_width'] = !empty($margin['banner_width'])
                                ? ($margin['banner_width']) : 'auto';
                            $margin['banner_position'] = !empty($margin['banner_position'])
                                ? ($margin['banner_position']) : 'left';
                            // check if potro has pdf header or footer
                            if($potrojari->potro_type == 20 || $potrojari->potro_type == 27 || $potrojari->potro_type == 21){
                                $regex = '/<span class="[a-z]* potro_security"(.+?)>(.+?)<\/span>/s';
                                $potrojari->potro_description = preg_replace( $regex, '', $potrojari->potro_description);
                                $security_level = json_decode(DAK_SECRECY_TYPE,true);
                                $margin['pdf_footer_title'] = (!empty($potrojari->potro_security_level) && !empty($security_level[$potrojari->potro_security_level]))?$security_level[$potrojari->potro_security_level]:'';
                                $margin['pdf_header_title'] = (!empty($potrojari->potro_security_level) && !empty($security_level[$potrojari->potro_security_level]))?$security_level[$potrojari->potro_security_level]:'';
                                $margin['show_pdf_page_number'] = 1;
                            }

                            $receiver['receiving_officer_email'] = str_replace(' ', ',',
                                $receiver['receiving_officer_email']);
                            $receiver['receiving_officer_email'] = str_replace('/', ',',
                                $receiver['receiving_officer_email']);


                            $optionsMerge = [];

                            $maindomain = explode('potrojari/', $domain);

                            if (defined("POTROJARI_UPDATE") && POTROJARI_UPDATE) {
                                if (empty($margin['potro_header'])) {
                                    if (defined("TESTING_SERVER") && TESTING_SERVER) {

                                        if (!empty($margin['potro_header_banner'])) {
                                            $optionsMerge['javascript-delay'] = 1000;
                                            $optionsMerge['header-html'] = "{$maindomain[0]}potrojariDynamicHeader/{$potrojari['office_id']}?imagetoken=" . urlencode($margin['potro_header_banner']) . "&banner_position=" . $margin['banner_position'] . "&banner_width=" . $margin['banner_width']."&pdf_header_title=".(!empty($margin['pdf_header_title'])?urlencode($margin['pdf_header_title']):'---');
                                        } else {
                                            $optionsMerge['header-html'] = "{$maindomain[0]}potrojariDynamicHeader/{$potrojari['office_id']}?banner_position=" . $margin['banner_position'] . "&banner_width=" . $margin['banner_width']."&pdf_header_title=".(!empty($margin['pdf_header_title'])?urlencode($margin['pdf_header_title']):'---');
                                        }
                                    } else {
                                        $s = (defined("Live") && Live) ? 's' : '';
                                        if (!empty($margin['potro_header_banner'])) {
                                            $optionsMerge['javascript-delay'] = 1000;
                                            $optionsMerge['header-html'] = "http://localhost/potrojariDynamicHeader/{$potrojari['office_id']}?imagetoken=" . urlencode($margin['potro_header_banner']) . "&banner_position=" . $margin['banner_position'] . "&banner_width=" . $margin['banner_width']."&pdf_header_title=".(!empty($margin['pdf_header_title'])?urlencode($margin['pdf_header_title']):'---');
                                        } else {
                                            $optionsMerge['header-html'] = "http://localhost/potrojariDynamicHeader/{$potrojari['office_id']}?banner_position=" . $margin['banner_position'] . "&banner_width=" . $margin['banner_width']."&pdf_header_title=".(!empty($margin['pdf_header_title'])?urlencode($margin['pdf_header_title']):'---');
                                        }
                                    }
                                }
                            }
                            if (defined("TESTING_SERVER") && TESTING_SERVER) {
                                $optionsMerge['footer-html'] = "{$maindomain[0]}potrojariDynamicFooter".'?pdf_footer_title='.((!empty($margin['pdf_footer_title']))?urlencode($margin['pdf_footer_title']):'---').'&show_pdf_page_number='.(isset($margin['show_pdf_page_number'])?$margin['show_pdf_page_number']:1);
                            }
                            else {
                                $s = (defined("Live") && Live) ? 's' : '';
                                $optionsMerge['footer-html'] = "http://localhost/potrojariDynamicFooter".'?pdf_footer_title='.((!empty($margin['pdf_footer_title']))?urlencode($margin['pdf_footer_title']):'---').'&show_pdf_page_number='.(isset($margin['show_pdf_page_number'])?$margin['show_pdf_page_number']:1);
                            }
                            $optionsMerge['footer-center'] = '';
                            $options = $optionsMerge + array(
                                    'no-outline', // option without argument
                                    'disable-external-links',
                                    'disable-internal-links',
                                    'disable-forms',
                                    'footer-center' => '[page]',
                                    'orientation' => $margin['orientation'],
                                    'margin-top' => bnToen($margin['margin_top']) * 20,
                                    'margin-right' => bnToen($margin['margin_right']) * 20,
                                    'margin-bottom' => bnToen($margin['margin_bottom']) * 20,
                                    'margin-left' => bnToen($margin['margin_left']) * 20,
                                    'encoding' => 'UTF-8', // option with argument
                                    'binary' => ROOT . DS . 'vendor' . DS . 'profburial' . DS . 'wkhtmltopdf-binaries-centos6' . DS . 'bin' . DS . (defined("PDFBINARY")
                                            ? PDFBINARY : 'wkhtmltopdf-amd64-centos6'),
                                    'user-style-sheet' => WWW_ROOT . DS . 'assets' . DS . 'global' . DS . 'plugins' . DS . 'bootstrap' . DS . 'css' . DS . 'bootstrap.min.css',
                                );

                            $pdf = new Pdf($options);

                            $pdfbody = (!empty($potrojari->attached_potro) ? $potrojari->attached_potro . '<br>' : '') . $potrojari->potro_description;

                            $request = new \Cake\Network\Request();
                            $pdfbody = str_replace($request->webroot . 'img/', WWW_ROOT . 'img/',
                                $pdfbody);
                            $pdfbody = str_replace('lang="AR-SA"', 'lang="BN"', $pdfbody);
                            $pdfbody = str_replace('dir="RTL"', '', $pdfbody);
                            if (!defined('Live') || Live == 0) {
                                $background_image = 'background-image: url("' . WWW_ROOT . 'img/test-background.png' . '");background-repeat: no-repeat;background-position:center;';
                            } else {
                                $background_image = '';
                            }

                            $pdf->addPage("<html><head><script src='https://code.jquery.com/jquery-1.11.3.js'></script><style>body{
                font-family: nikosh,SolaimanLipi, Garamond,'Open Sans', sans-serif !important;
                font-size: 12pt!important;
                {$background_image}
            }
            thead { display: table-row-group}
    tfoot { display: table-row-group}
    tr { page-break-inside: avoid}
    thead th { height: 1.1cm; }
    .text-center{
        text-align: center!important;
    }
            .bangladate{
                border-bottom: 1px solid #000;
            }
            #sovapoti_signature img, #sender_signature img, #sender_signature2 img, #sender_signature3 img, #sender_signature4 img, #sender_signature5 img{
                height: 50px;
            }
            #upomontri_receiver_signature img,
    #sochib_signature img,
    #second_receiver_signature img,
    #third_receiver_signature img,
    #fourth_receiver_signature img,
    #fifth_receiver_signature img{
        height:50px;
    }
    table {
      table-layout: fixed;
      width: 100%!importan;
      white-space: nowrap;
    }
    table td , table th{
      white-space: pre-wrap;
      /*overflow: hidden;*/
      /*text-overflow: ellipsis;*/
      word-break: normal;
      word-wrap: break-word;
    }
    
    table th{
      word-break: normal;
    }
    .imei_table_area{
        overflow: hidden!important;
    }
    </style>
    </head>
    <body>
    <script>
        $(function(){
            $('#sharok_no').parent('div').css('white-space','nowrap');
            $('#sharok_no2').parent('div').css('white-space','nowrap');
        })
</script>
{$pdfbody}
</body>
</html>");

                            //module
                            $path = 'Nothi';
                            $office = $office_id;

                            $pdf_path = $this->makeDirectory($path, $office);
                            //chmod($pdf_path, 777);

                            $canpdf = false;
                            $filename = $pdf_path . 'main_potro_' . $office_id . '_' . $potrojari_id . '.pdf';
                            // check pdf has been save or not
                            if (file_exists($filename)) {
                                $canpdf = true;
                            } else {
                                $pdf->saveAs($filename);

                                if (file_exists($filename)) {
                                    $canpdf = true;
                                }
                            }
                            if ($canpdf) {
//                                $this->out('PDF created');
                                $recEmail = explode(',', $receiver['receiving_officer_email']);
                                if (!empty($recEmail)) {
                                    if ((defined("EMAILER_ACTIVE") && EMAILER_ACTIVE == 1)) {
                                        $recEmail = array_map('trim', $recEmail);
                                        $email_attachments = [];
                                        if (!empty($attachments)) {
                                            foreach ($attachments as $k => $file) {
                                                if (!empty($file)) {
                                                    if ($file['attachment_type'] == 'text' && $file['is_inline'] != 0) {
                                                        continue;
                                                        $signHide = 0;
                                                        $folderPath = 'Nothi/' . $office_id . '/';
                                                        $custom_filename = "potro_" . $potrojari_id . '_' . $potrojari->nothi_part_no . '_' . $k;
                                                        $pdf_status = $this->createPdf($custom_filename, $file['attachment_description'], $signHide, $folderPath, $margin, $office_id);
                                                        if ($pdf_status['status'] == 'success' && !empty($pdf_status['src'])) {
                                                            if (file_exists(FILE_FOLDER_DIR . $folderPath . $custom_filename . '.pdf')) {
                                                                $custom_name = (!empty($file['user_file_name']) ? $file['user_file_name'] . '_' : '') . $custom_filename;
                                                                $email_attachments[urldecode($custom_name)] = ['file' => (urldecode($pdf_status['src']))];
                                                            } else {
                                                                $potrojariSent = [
                                                                    'is_sent' => 0,
                                                                    'dak_id' => 0,
                                                                    'potro_status' => 'Not Sent',
                                                                    'task_reposponse' => 'Please resend.PDF not created.',
                                                                    'run_task' => 1,
                                                                    'retry_count' => intval($receiver['retry_count']) + 1,
                                                                    'modified' => date("Y-m-d H:i:s"),
                                                                    'created_by' => 0
                                                                ];
                                                                $potrojariRecieverTable->updateAll($potrojariSent, ['id' => $receiver['id']]);
                                                                //Can not save PDF . so need to add/edit potrojari cron request - start
                                                                $potrojariRequestTable = TableRegistry::get('PotrojariRequest');
                                                                $commandReceiver = ROOT . "/bin/cake cronjob potrojariReceiverSent {$office_id} {$potrojari_id} '$domain'";
                                                                $potrojariRequestTable->saveData($commandReceiver, $office_id,
                                                                    $potrojari_id, 0, 0);
                                                                //Can not save PDF . so need to add/edit potrojari cron request - end
                                                                continue;
                                                            }
                                                        }
                                                    } else {
                                                        $filname = explode('/',
                                                            $file['file_name']);
                                                        $filepath = FILE_FOLDER_DIR . $file['file_name'];
                                                        $custom_name = (!empty($file['user_file_name']) ? $file['user_file_name'] . '_' : '') . $filname[count($filname) - 1];
                                                        $email_attachments[urldecode($custom_name)] = ['file' => (urldecode($filepath))];
                                                    }

                                                }
                                            }
                                        }

                                        $email_attachments['main_potro.pdf'] = ['file' => ($filename)];
                                        foreach ($recEmail as $k => $mail) {
                                            $nothi_emailer = new \App\Model\Custom\NothiEmailer();
                                            $nothi_emailer->to_email = $mail;
                                            $nothi_emailer->to_name = $receiver['receiving_officer_name'] . ' ' . $receiver['receiving_officer_designation_label'];
                                            $nothi_emailer->from_des = $potrojari['officer_name'] . ', ' . $potrojari['officer_designation_label'] . ', ' . $potrojari ['office_name'];
                                            $nothi_emailer->subject = $potrojari->potro_subject;
                                            $nothi_emailer->email_body = "আপনার একটি পত্র এসেছে। মূল পত্র সহ সকল সংযুক্তি ইমেইলের সংযুক্তিতে দেয়া হলো। <br/><br/>ধন্যবাদ।";
                                            $nothi_emailer->office_id = 0;
                                            $nothi_emailer->office_name = "";
                                            $nothi_emailer->attachments = $email_attachments;
                                            $nothi_emailer->type = 'PotrojariReceiver';
                                            $nothi_emailer->email_trace_id = $receiver['id'];
                                            $nothi_emailer->nothi_office = $office_id;

                                            $emailer_response = $this->sendPotroJariEmail(get_object_vars($nothi_emailer));
                                            if ($emailer_response == true) {
                                                if (empty($receiver['potro_status'])) {
                                                    $receiver['potro_status'] = 'পত্রটি মেইল সার্ভার এর কাছে প্রেরণ করা হয়েছে। অপেক্ষা করুন';
                                                }
                                            } else {
                                                $receiver['potro_status'] = $emailer_response;
                                            }
                                        }
                                    } else {
                                        //if we don't use emailer app
                                        foreach ($recEmail as $k => $mail) {
                                            $email = new Email('default');

                                            $email->emailFormat('html')->from(['nothi@nothi.org.bd' => $potrojari->officer_name])
                                                ->to(trim($mail))
                                                ->subject($potrojari->potro_subject);

                                            if (!empty($attachments)) {
                                                foreach ($attachments as $k => $file) {
                                                    if (!empty($file)) {
                                                        if ($file['attachment_type'] == 'text' && $file['is_inline'] != 0) {
                                                            continue;
                                                            $signHide = 0;
                                                            $folderPath = 'Nothi/' . $office_id . '/';
                                                            $custom_filename = "potro_" . $potrojari_id . '_' . $potrojari->nothi_part_no;
                                                            $pdf_status = $this->createPdf($custom_filename, $file['attachment_description'], $signHide, $folderPath, $margin, $office_id);
                                                            if ($pdf_status['status'] == 'success' && !empty($pdf_status['src'])) {
                                                                if (file_exists(FILE_FOLDER_DIR . $folderPath . $custom_filename . '.pdf')) {
                                                                    $custom_name = (!empty($file['user_file_name']) ? $file['user_file_name'] . '_' : '') . $custom_filename;
                                                                    $email_attachments[urldecode($custom_name)] = ['file' => (urldecode($pdf_status['src']))];
                                                                } else {
                                                                    $potrojariSent = [
                                                                        'is_sent' => 0,
                                                                        'dak_id' => 0,
                                                                        'potro_status' => 'Not Sent',
                                                                        'task_reposponse' => 'Please resend.PDF not created',
                                                                        'run_task' => 1,
                                                                        'retry_count' => intval($receiver['retry_count']) + 1,
                                                                        'modified' => date("Y-m-d H:i:s"),
                                                                        'created_by' => 0
                                                                    ];
                                                                    $potrojariRecieverTable->updateAll($potrojariSent, ['id' => $receiver['id']]);
                                                                    $this->out('before yesterday');
                                                                    //Can not save PDF . so need to add/edit potrojari cron request - start
                                                                    $potrojariRequestTable = TableRegistry::get('PotrojariRequest');
                                                                    $commandReceiver = ROOT . "/bin/cake cronjob potrojariReceiverSent {$office_id} {$potrojari_id} '$domain'";
                                                                    $potrojariRequestTable->saveData($commandReceiver, $office_id,
                                                                        $potrojari_id, 0, 0);
                                                                    //Can not save PDF . so need to add/edit potrojari cron request - end
                                                                    continue;
                                                                }
                                                            }
                                                        } else {
                                                            $filname = explode('/',
                                                                $file['file_name']);
                                                            $filepath = FILE_FOLDER_DIR . $file['file_name'];
                                                            $custom_name = (!empty($file['user_file_name']) ? $file['user_file_name'] . '_' : '') . $filname[count($filname) - 1];
                                                            $email->addAttachments(array(
                                                                urldecode($custom_name) => ['file' => urldecode($filepath)]));
                                                        }

                                                    }
                                                }
                                            }
                                            $email->addAttachments(array('main_potro.pdf' => ['file' => $filename]));
                                            if ($email->sendWithoutException("আপনার একটি পত্র এসেছে। মূল পত্র সহ সকল সংযুক্তি ইমেইলের সংযুক্তিতে দেয়া হলো। <br/><br/>ধন্যবাদ।")) {
                                                $receiver['potro_status'] = 'Sent';
                                            }
                                        }
                                    }
                                }

                                $potrojariSent = [
                                    'dak_id' => 0,
                                    'potro_status' => ((defined("EMAILER_ACTIVE") && EMAILER_ACTIVE
                                        == 1 && defined('Live') && Live == 1)) ? 'পত্রটি মেইল সার্ভার এর কাছে প্রেরণ করা হয়েছে। অপেক্ষা করুন।'
                                        : 'Sent',
                                    'task_reposponse' => $receiver['potro_status'],
                                    'is_sent' => ((defined("EMAILER_ACTIVE") && EMAILER_ACTIVE == 1 && defined('Live') && Live == 1)
                                    ) ? 1 : 1,
                                    'run_task' => 1,
                                    'retry_count' => intval($receiver['retry_count']) + 1,
                                    'modified' => date("Y-m-d H:i:s"),
                                ];
                                //send SMS -- default emailer
                                if ($potrojariSent['is_sent'] == 1 && $potrojariSent['potro_status'] == 'Sent' && !empty($receiver['officer_mobile']) && !empty($receiver['sms_message'])) {
                                    // can be multiple receiver
                                    $recSMS = explode('--', $receiver['officer_mobile']);
                                    if (!empty($recSMS)) {
                                        foreach ($recSMS as $receiverSMS) {
                                            $this->saveSMSRequest($receiverSMS, $receiver['sms_message']);
                                        }
                                    }
                                }
                                //send SMS
//                                $this->out('sent');
                            } else {
                                $this->out('Can not create PDF.Filepath: ' . $filename);
                                $this->retryRec[$receiver['id']] = isset($this->retryRec[$receiver['id']])
                                    ? ($this->retryRec[$receiver['id']] + 1) : 1;
                                $potrojariSent = [
                                    'is_sent' => 0,
                                    'dak_id' => 0,
                                    'potro_status' => 'Not Sent',
                                    'task_reposponse' => "Failed! Please Resend.PDF not created",
                                    'run_task' => 1,
                                    'retry_count' => intval($receiver['retry_count']) + 1,
                                    'modified' => date("Y-m-d H:i:s"),
                                    'created_by' => 0
                                ];

//                                $email = new Email('default');
//                                $email->emailFormat('html')->from(['nothi@nothi.org.bd'])
//                                    ->to(trim('eather@tappware.com'))
//                                    ->subject("Folder permission missing");
//                                $email->sendWithoutException("Permission for " . $pdf_path . " is missing. Error: " . $pdf->getError());
//                                $this->out('PDF problem');
                                $potrojariRecieverTable->updateAll($potrojariSent, ['id' => $receiver['id']]);
                                //Can not save PDF . so need to add/edit potrojari cron request - start
                                $potrojariRequestTable = TableRegistry::get('PotrojariRequest');
                                $commandReceiver = ROOT . "/bin/cake cronjob potrojariReceiverSent {$office_id} {$potrojari_id} '$domain'";
                                $potrojariRequestTable->saveData($commandReceiver, $office_id,
                                    $potrojari_id, 0, 0);
                                //Can not save PDF . so need to add/edit potrojari cron request - end
                                continue;
                            }
                        } catch (\Exception $ex) {
                            $this->out('Exception occured. ' . $ex->getMessage());
                            $this->retryRec[$receiver['id']] = isset($this->retryRec[$receiver['id']])
                                ? ($this->retryRec[$receiver['id']] + 1) : 1;
                            $potrojariSent = [
                                'is_sent' => 0,
                                'dak_id' => 0,
                                'potro_status' => 'Not Sent',
                                'task_reposponse' => $ex->getMessage(),
                                'run_task' => 1,
                                'retry_count' => intval($receiver['retry_count']) + 1,
                                'modified' => date("Y-m-d H:i:s"),
                                'created_by' => 0
                            ];
                            $potrojariRecieverTable->updateAll($potrojariSent, ['id' => $receiver['id']]);
                            //Exception Occured . so need to add/edit potrojari cron request - start
                            $potrojariRequestTable = TableRegistry::get('PotrojariRequest');
                            $commandReceiver = ROOT . "/bin/cake cronjob potrojariReceiverSent {$office_id} {$potrojari_id} '$domain'";
                            $potrojariRequestTable->saveData($commandReceiver, $office_id,
                                $potrojari_id, 0, 0);
                            //Exception Occured . so need to add/edit potrojari cron request - end
                            continue;
                        }
                    } else {
                        // does not have any mail id to send
                        $potrojariSent = [
                            'dak_id' => 0,
                            'potro_status' => 'Not Sent',
                            'task_reposponse' => 'Undefined Email',
                            'is_sent' => 1,
                            'run_task' => 1,
                            'retry_count' => intval($receiver['retry_count']) + 1,
                            'modified' => date("Y-m-d H:i:s"),
                            'created_by' => 0
                        ];
                        $this->out('wrong email');
                        //send SMS -- no mail but can have a sms
                        if (!empty($receiver['officer_mobile']) && !empty($receiver['sms_message'])) {
                            // can be multiple receiver
                            $recSMS = explode('--', $receiver['officer_mobile']);
                            if (!empty($recSMS)) {
                                foreach ($recSMS as $receiverSMS) {
                                    $this->saveSMSRequest($receiverSMS, $receiver['sms_message']);
                                }
                            }
                        }
                        //send SMS
                    }
                    $potrojariRecieverTable->updateAll($potrojariSent, ['id' => $receiver['id']]);

                    continue;
                }

                $this->switchOffice($receiver['receiving_office_id'], 'recieverOffice');
                TableRegistry::remove('DakDaptoriks');
                TableRegistry::remove('DakNagoriks');
                $table_instance_dd = TableRegistry::get('DakDaptoriks');
                $table_instance_dn = TableRegistry::get('DakNagoriks');

                //dak daptorik
                $dak_daptoriks = $table_instance_dd->newEntity();
                $dak_daptoriks->sender_sarok_no = $potrojari->sarok_no;
                $dak_daptoriks->sender_office_id = $potrojari->office_id;
                $dak_daptoriks->sender_officer_id = $potrojari->officer_id;
                $dak_daptoriks->sender_office_name = $potrojari->office_name;
                $dak_daptoriks->sender_office_unit_id = $potrojari->office_unit_id;
                $dak_daptoriks->sender_office_unit_name = $potrojari->office_unit_name;
                $dak_daptoriks->sender_officer_designation_id = $potrojari->officer_designation_id;
                $dak_daptoriks->sender_officer_designation_label = $potrojari->officer_designation_label;
                $dak_daptoriks->sender_name = $potrojari->officer_name;

                $dak_daptoriks->sending_date = $potrojari->potrojari_date;
                $dak_daptoriks->dak_sending_media = 1;
                $dak_daptoriks->dak_received_no = $this->generateDakReceivedNo($dak_daptoriks->sender_office_id);
                $dak_daptoriks->receiving_date = date("Y-m-d H:i:s");
                $dak_daptoriks->uploader_designation_id = $potrojari->officer_designation_id;
                $dak_daptoriks->dak_subject = $potrojari->potro_subject;
                $dak_daptoriks->dak_security_level = $potrojari->potro_security_level;
                $dak_daptoriks->dak_priority_level = $potrojari->potro_priority_level;
                $dak_daptoriks->dak_description = (!empty($potrojari->attached_potro) ? $potrojari->attached_potro . '<br>' : '') . $potrojari->potro_description;
                $dak_daptoriks->is_summary_nothi = $potrojari->is_summary_nothi;
                $dak_daptoriks->meta_data = $potrojari->meta_data;
                $dak_daptoriks->dak_cover = !empty($potrojari->potro_cover) ? $potrojari->potro_cover
                    : '';


                $dak_daptoriks->receiving_office_id = $receiver['receiving_office_id'];
                $dak_daptoriks->receiving_officer_designation_id = $receiver['receiving_officer_designation_id'];
                $dak_daptoriks->receiving_office_unit_id = $receiver['receiving_office_unit_id'];
                $dak_daptoriks->receiving_office_unit_name = !empty($receiver['receiving_office_unit_name'])
                    ? $receiver['receiving_office_unit_name'] : '';
                $dak_daptoriks->receiving_officer_id = $receiver['receiving_officer_id'];
                $dak_daptoriks->receiving_officer_designation_label = !empty($receiver['receiving_officer_designation_label'])
                    ? $receiver['receiving_officer_designation_label'] : '';
                $dak_daptoriks->receiving_officer_name = !empty($receiver['receiving_officer_name'])
                    ? $receiver['receiving_officer_name'] : '';

                $dak_countdp = $table_instance_dd->find()->where(['receiving_office_id' => $receiver['receiving_office_id']])->order(['cast(docketing_no as signed)' => 'desc'])->first();
                $dak_countng = $table_instance_dn->find()->where(['receiving_office_id' => $receiver['receiving_office_id']])->order(['cast(docketing_no as signed)' => 'desc'])->first();
                $dak_count = max([(!empty($dak_countdp['docketing_no']) ? $dak_countdp['docketing_no'] : 0), (!empty($dak_countng['docketing_no']) ? $dak_countng['docketing_no'] : 0)]) + 1;

                $dak_daptoriks->docketing_no = $dak_count;
                $dak_daptoriks->dak_status = DAK_CATEGORY_INBOX;
                $dak_daptoriks->receiving_date = date("Y-m-d H:i:s");
                $dak_daptoriks->sending_date = date("Y-m-d H:i:s");
                $dak_daptoriks->created_by = 0;
                $dak_daptoriks->modified_by = 0;
                $dak = $table_instance_dd->save($dak_daptoriks);

                //other
                TableRegistry::remove('DakUsers');
                $table_instance_du = TableRegistry::get('DakUsers');

                $drafterOther = $table_instance_du->newEntity();
                $drafterOther->dak_type = DAK_DAPTORIK;
                $drafterOther->dak_id = $dak->id;
                $drafterOther->to_office_id = $dak_daptoriks->receiving_office_id;
                $drafterOther->to_office_name = $potrojari->office_name;
                $drafterOther->to_office_unit_id = $dak_daptoriks->receiving_office_unit_id;
                $drafterOther->to_office_unit_name = $dak_daptoriks->receiving_office_unit_name;
                $drafterOther->to_office_address = '';
                $drafterOther->to_officer_id = $dak_daptoriks->receiving_officer_id;
                $drafterOther->to_officer_name = $dak_daptoriks->receiving_officer_name;
                $drafterOther->to_officer_designation_id = $dak_daptoriks->receiving_officer_designation_id;
                $drafterOther->to_officer_designation_label = $dak_daptoriks->receiving_officer_designation_label;
                $drafterOther->dak_view_status = DAK_VIEW_STATUS_NEW;
                $drafterOther->dak_priority = 0;
                $drafterOther->is_summary_nothi = $potrojari->is_summary_nothi;
                $drafterOther->attention_type = 1;
                $drafterOther->dak_category = DAK_CATEGORY_INBOX;
                $drafterOther->created_by = 0;
                $drafterOther->modified_by = 0;

                $drafterOther = $table_instance_du->save($drafterOther);

                TableRegistry::remove('DakUserActions');
                $table_instance_dua = TableRegistry::get('DakUserActions');
                $user_action = $table_instance_dua->newEntity();
                $user_action->dak_id = $dak->id;
                $user_action->dak_user_id = $drafterOther->id;
                $user_action->dak_action = DAK_CATEGORY_INBOX;
                $user_action->is_summary_nothi = $potrojari->is_summary_nothi;
                $user_action->created_by = 0;
                $user_action->modified_by = 0;
                $table_instance_dua->save($user_action);

                TableRegistry::remove('DakAttachments');
                $table_instance_da = TableRegistry::get('DakAttachments');

                //first attachment then main potro
                if (!empty($attachments)) {
                    foreach ($attachments as $k => $file) {
                        if (!empty($file)) {
                            if ($file['attachment_type'] == 'text' && $file['is_inline'] == 0) {
                                continue;
                            }
                            $attachment_data = array();
                            $attachment_data['dak_type'] = "Daptorik";
                            $attachment_data['dak_id'] = $dak->id;
                            $attachment_data['attachment_description'] = '';

                            $attachment_data['attachment_type'] = $file['attachment_type'];
                            $attachment_data['file_dir'] = $file['file_dir'];
                            $attachment_data['file_name'] = $file['file_name'];
                            $attachment_data['user_file_name'] = $file['user_file_name'];
                            if ($potrojari->is_summary_nothi) {
                                $attachment_data['is_summary_nothi'] = 1;
                            }
                            if ($file['attachment_type'] == 'text') {
                                $attachment_data['attachment_type'] = 'text';
                                $attachment_data['content_body'] = $file['content_body'];
                            }
                            $dak_attachment = $table_instance_da->newEntity();
                            $dak_attachment = $table_instance_da->patchEntity($dak_attachment,
                                $attachment_data);
                            $dak_attachment->created_by = 0;
                            $dak_attachment->modified_by = 0;
                            $table_instance_da->save($dak_attachment);
                        }
                    }
                }
                //first attachment then main potro
                if (!empty($potrojari->potro_description)) {
                    $attachment_data = array();
                    $attachment_data['dak_type'] = "Daptorik";
                    $attachment_data['dak_id'] = $dak->id;
                    $attachment_data['attachment_description'] = '';
                    $attachment_data['is_main'] = 1;

                    $attachment_data['attachment_type'] = 'text';
                    $attachment_data['meta_data'] = $potrojari->meta_data;
                    $attachment_data['content_body'] = (!empty($potrojari->attached_potro) ? $potrojari->attached_potro . '<br>' : '') . $potrojari->potro_description;
                    if ($potrojari->is_summary_nothi) {
                        $attachment_data['content_cover'] = $potrojari->potro_cover;
                        $attachment_data['is_summary_nothi'] = 1;
                    }

                    $attachment_data['file_dir'] = '';
                    $attachment_data['file_name'] = '';

                    $dak_attachment = $table_instance_da->newEntity();
                    $dak_attachment = $table_instance_da->patchEntity($dak_attachment,
                        $attachment_data);
                    $dak_attachment->created_by = 0;
                    $dak_attachment->modified_by = 0;
                    $table_instance_da->save($dak_attachment);
                }


                /* Update Dak Movement Table */
                TableRegistry::remove('DakMovements');
                $DakMovementsTable = TableRegistry::get('DakMovements');

                /* Sender to Draft User Movement */
                $second_move = $DakMovementsTable->newEntity();
                $second_move->dak_type = DAK_DAPTORIK;
                $second_move->dak_id = $dak->id;
                $second_move->from_office_id = $dak['sender_office_id'];
                $second_move->from_office_name = $dak['sender_office_name'];
                $second_move->from_office_unit_id = $dak['sender_office_unit_id'];
                $second_move->from_office_unit_name = $dak['sender_office_unit_name'];
                $second_move->from_office_address = '';
                $second_move->from_officer_id = $dak['sender_officer_id'];
                $second_move->from_officer_name = $dak['sender_name'];
                $second_move->from_officer_designation_id = $dak['sender_officer_designation_id'];
                $second_move->from_officer_designation_label = $dak['sender_officer_designation_label'];


                $second_move->to_office_id = $drafterOther['to_office_id'];
                $second_move->to_office_name = $drafterOther['to_office_name'];
                $second_move->to_office_unit_id = $drafterOther['to_office_unit_id'];
                $second_move->to_office_unit_name = $drafterOther['to_office_unit_name'];
                $second_move->to_office_address = $drafterOther['to_office_address'];
                $second_move->to_officer_id = $drafterOther['to_officer_id'];
                $second_move->to_officer_name = $drafterOther['to_officer_name'];
                $second_move->to_officer_designation_id = $drafterOther['to_officer_designation_id'];
                $second_move->to_officer_designation_label = $drafterOther['to_officer_designation_label'];
                $second_move->attention_type = 1;
                $second_move->docketing_no = $dak['docketing_no'];
                $second_move->from_sarok_no = "";
                $second_move->to_sarok_no = "";
                $second_move->operation_type = DAK_CATEGORY_FORWARD;
                $second_move->sequence = 2;
                $second_move->dak_actions = '';
                $second_move->is_summary_nothi = $potrojari->is_summary_nothi;
                $second_move->dak_priority = $dak['dak_priority_level'];
                $second_move->from_potrojari = 1;
                $second_move->created_by = 0;
                $second_move->modified_by = 0;
                $DakMovementsTable->save($second_move);

                $potrojariSent = [
                    'dak_id' => $dak->id,
                    'potro_status' => 'Sent',
                    'is_sent' => 1,
                    'run_task' => 1,
                    'retry_count' => intval($receiver['retry_count']) + 1,
                    'modified' => date("Y-m-d H:i:s"),
                ];

                $selected_office_section = [
                    'office_id' => $office_id,
                    'office_unit_id' => $potrojari->office_unit_id,
                    'office_unit_organogram_id' => $potrojari->officer_designation_id,
                    'incharge_label' => '',
                    'office_name' => $potrojari->office_name,
                    'office_address' => '',
                    'designation_label' => $potrojari->officer_designation_label,
                    'officer_name' => $potrojari->officer_name,
                    'ministry_records' => '',
                    'office_phone' => '',
                    'office_fax' => '',
                    'office_email' => '',
                    'office_web' => '',
                    'office_unit_name' => $potrojari->office_unit_name,
                ];

                $toUser['office_id'] = $receiver['receiving_office_id'];
                $toUser['officer_id'] = $receiver['receiving_officer_id'];
                if ($potrojari->is_summary_nothi) {
                    if (!$this->NotificationSet('inbox',
                        array("1", "Sar Sonkhep sent as Potrojari"), 1,
                        $selected_office_section, $toUser, $potrojari->potro_subject)
                    ) {

                    }
                } else {
                    $priority = json_decode(DAK_PRIORITY_TYPE, true);
                    $security = json_decode(DAK_SECRECY_TYPE, true);
                    $status = (isset($priority[$dak['dak_priority_level']]) && $dak['dak_priority_level']
                        > 0 ? ('"' . $priority[$dak['dak_priority_level']] . '" ') : '') . (isset($security[$dak['dak_security_level']])
                        && $dak['dak_security_level'] > 0 ? ('"' . $security[$dak['dak_security_level']] . '" ')
                            : '');

                    if (!$this->NotificationSet('inbox',
                        array("1", ("Daptorik Dak sent as Potrojari")), 1,
                        $selected_office_section, $toUser, $potrojari->potro_subject)
                    ) {

                    }
                }

                $potrojariRecieverTable->updateAll($potrojariSent, ['id' => $receiver['id']]);


                if ($potrojari->is_summary_nothi && $potrojariSent['is_sent']) {

                    $tableSummaryNothiUser = TableRegistry::get('SummaryNothiUsers');
                    $tableSummaryNothiUser->updateAll([
                        'dak_id' => $dak->id
                    ],
                        [
                            'potrojari_id' => $potrojari->id,
                            'current_office_id' => $receiver['receiving_office_id'],
                            'nothi_office' => $potrojari->office_id,
                        ]);
                    $tableSummaryNothiUser->updateAll([
                        'is_sent' => 1
                    ],
                        [
                            'potrojari_id' => $potrojari->id,
                            'current_office_id' => $potrojari->office_id,
                            'nothi_office' => $potrojari->office_id,
                        ]);
                }
            } catch (\Exception $ex) {
                if (!empty($receiver)) {
                    $this->retryRec[$receiver['id']] = isset($this->retryRec[$receiver['id']]) ? ($this->retryRec[$receiver['id']]
                        + 1) : 1;
                    $potrojariSent = ['dak_id' => 0,
                        'potro_status' => 'Not Sent',
                        'task_reposponse' => $ex->getMessage(),
                        'run_task' => 1,
                        'is_sent' => 0,
                        'retry_count' => intval($receiver['retry_count']) + 1,
                        'modified' => date("Y-m-d H:i:s"),
                        'created_by' => 0];
                    $potrojariRecieverTable->updateAll($potrojariSent, ['id' => $receiver['id']]);
                    $this->out($ex->getMessage());
                    //Exception Occured . so need to add/edit potrojari cron request - start
                    $potrojariRequestTable = TableRegistry::get('PotrojariRequest');
                    $commandReceiver = ROOT . "/bin/cake cronjob potrojariReceiverSent {$office_id} {$potrojari_id} '$domain'";
                    $potrojariRequestTable->saveData($commandReceiver, $office_id, $potrojari_id, 0, 0);
                    //Exception Occured . so need to add/edit potrojari cron request - end
                }
            }
        }

        endline:
        ConnectionManager::drop('default');
        ConnectionManager::drop('recieverOffice');
    }

    public function potrojariOnulipiSent($office_id, $potrojari_id = 0, $domain = '', $force = 0)
    {

        set_time_limit(0);
        $potrojariSent = [];
        $this->switchOffice($office_id, 'cronOffice');
        TableRegistry::remove('Potrojari');
        TableRegistry::remove('PotrojariOnulipi');
        TableRegistry::remove('PotrojariAttachments');
        $potrojariTable = TableRegistry::get('Potrojari');
        $potrojariRecieverTable = TableRegistry::get('PotrojariOnulipi');
        $table_instance_pa = TableRegistry::get('PotrojariAttachments');
        $potrojari = $potrojariTable->get($potrojari_id);
        $query = $potrojariRecieverTable->find();
        if (!empty($potrojari_id)) {
            $query->where(['potrojari_id' => $potrojari_id]);
        }
        if(!empty($force)){
            $query->where(['retry_count <=' => 2,'is_sent' => 0]);
        }else{
            $query->where(['run_task' => 0, 'retry_count <=' => 2, 'is_sent' => 0]);
        }
        $receivers = $query->toArray();

        if (empty($receivers)) {
            $potrojariTable->updateAll(['onulipi_sent' => 1], ['id' => $potrojari_id]);
            ConnectionManager::drop('default');
            ConnectionManager::drop('cronOffice');
            $this->out('receiver_sent = 1');
            return true;
        }
        // if potrojari happen before yesterday then we need to deny that request
        $weekBefore = date('Y-m-d H:i:s', strtotime("-7 days"));

        if ($force == 0 && $potrojari['potrojari_date']->format("Y-m-d H:i:s") < $weekBefore) {
            $potrojariSent = [
                'potro_status' => 'Sent',
                'is_sent' => 1,
                'run_task' => 1,
                'retry_count' => intval($receivers[0]['retry_count']) + 1,
                'modified' => date("Y-m-d H:i:s"),
                'task_reposponse' => 'Failed. Before period.',
                'created_by' => 0
            ];
            $potrojariRecieverTable->updateAll($potrojariSent, ['potrojari_id' => $potrojari_id, 'is_sent' => 0]);
            $this->out('before a week');
            return true;
        }

        $attachments = $table_instance_pa->find()->where(['potrojari_id' => $potrojari->id])->toArray();
        //text omitted
        foreach ($receivers as $k => $receiver) {
            try {
                if (empty($receiver['receiving_office_id'])) {

                    if (!empty($receiver['receiving_officer_email']) && $receiver['receiving_officer_email']
                        != 'undefined') {
                        try {
                            $meta = $potrojari->meta_data;
                            $margin = !empty($meta) ? json_decode($meta, true) : [];
                            $margin['margin_top'] = isset($margin['margin_top']) ? doubleval($margin['margin_top']) : 1;
                            $margin['margin_right'] = isset($margin['margin_right']) ? doubleval($margin['margin_right']) : 0.75;
                            $margin['margin_bottom'] = isset($margin['margin_bottom']) ? doubleval($margin['margin_bottom']) : .075;
                            $margin['margin_left'] = isset($margin['margin_left']) ? doubleval($margin['margin_left']) : 0.75;
                            $margin['orientation'] = !empty($margin['orientation']) ? ($margin['orientation']) : 'portrait';
                            $receiver['receiving_officer_email'] = str_replace(' ', ',', $receiver['receiving_officer_email']);
                            $receiver['receiving_officer_email'] = str_replace('/', ',', $receiver['receiving_officer_email']);
                            $margin['banner_width'] = !empty($margin['banner_width']) ? ($margin['banner_width']) : 'auto';
                            $margin['banner_position'] = !empty($margin['banner_position']) ? ($margin['banner_position']) : 'left';
                            // check if potro has pdf header or footer
                            if($potrojari->potro_type == 20 || $potrojari->potro_type == 27 || $potrojari->potro_type == 21){
                                $regex = '/<span class="[a-z]* potro_security"(.+?)>(.+?)<\/span>/s';
                                $potrojari->potro_description = preg_replace( $regex, '', $potrojari->potro_description);
                                $security_level = json_decode(DAK_SECRECY_TYPE,true);
                                $margin['pdf_footer_title'] = (!empty($potrojari->potro_security_level) && !empty($security_level[$potrojari->potro_security_level]))?$security_level[$potrojari->potro_security_level]:'';
                                $margin['pdf_header_title'] = (!empty($potrojari->potro_security_level) && !empty($security_level[$potrojari->potro_security_level]))?$security_level[$potrojari->potro_security_level]:'';
                                $margin['show_pdf_page_number'] = 1;
                            }

                            $receiver['receiving_officer_email'] = str_replace(' ', ',',
                                $receiver['receiving_officer_email']);
                            $receiver['receiving_officer_email'] = str_replace('/', ',',
                                $receiver['receiving_officer_email']);

                            $optionsMerge = [];
                            $maindomain = explode('potrojari/', $domain);

                            if (defined("POTROJARI_UPDATE") && POTROJARI_UPDATE) {
                                if ($margin['potro_header'] == 0) {
                                    if (defined("TESTING_SERVER") && TESTING_SERVER) {

                                        if (!empty($margin['potro_header_banner'])) {
                                            $optionsMerge['javascript-delay'] = 1000;
                                            $optionsMerge['header-html'] = "{$maindomain[0]}potrojariDynamicHeader/{$potrojari['office_id']}?imagetoken=" . urlencode($margin['potro_header_banner']) . "&banner_position=" . $margin['banner_position'] . "&banner_width=" . $margin['banner_width']."&pdf_header_title=".(!empty($margin['pdf_header_title'])?urlencode($margin['pdf_header_title']):'---');
                                        } else {
                                            $optionsMerge['header-html'] = "{$maindomain[0]}potrojariDynamicHeader/{$potrojari['office_id']}?banner_position=" . $margin['banner_position'] . "&banner_width=" . $margin['banner_width']."&pdf_header_title=".(!empty($margin['pdf_header_title'])?urlencode($margin['pdf_header_title']):'---');
                                        }
                                    } else {
                                        $s = (defined("Live") && Live) ? 's' : '';
                                        if (!empty($margin['potro_header_banner'])) {
                                            $optionsMerge['javascript-delay'] = 1000;
                                            $optionsMerge['header-html'] = "http://localhost/potrojariDynamicHeader/{$potrojari['office_id']}?imagetoken=" . urlencode($margin['potro_header_banner']) . "&banner_position=" . $margin['banner_position'] . "&banner_width=" . $margin['banner_width']."&pdf_header_title=".(!empty($margin['pdf_header_title'])?urlencode($margin['pdf_header_title']):'---');
                                        } else {
                                            $optionsMerge['header-html'] = "http://localhost/potrojariDynamicHeader/{$potrojari['office_id']}?banner_position=" . $margin['banner_position'] . "&banner_width=" . $margin['banner_width']."&pdf_header_title=".(!empty($margin['pdf_header_title'])?urlencode($margin['pdf_header_title']):'---');
                                        }
                                    }
                                }
                            }

                            if (defined("TESTING_SERVER") && TESTING_SERVER) {
                                $optionsMerge['footer-html'] = "{$maindomain[0]}potrojariDynamicFooter".'?pdf_footer_title='.((!empty($margin['pdf_footer_title']))?urlencode($margin['pdf_footer_title']):'---').'&show_pdf_page_number='.(isset($margin['show_pdf_page_number'])?$margin['show_pdf_page_number']:1);
                            }
                            else {
                                $s = (defined("Live") && Live) ? 's' : '';
                                $optionsMerge['footer-html'] = "http://localhost/potrojariDynamicFooter".'?pdf_footer_title='.((!empty($margin['pdf_footer_title']))?urlencode($margin['pdf_footer_title']):'---').'&show_pdf_page_number='.(isset($margin['show_pdf_page_number'])?$margin['show_pdf_page_number']:1);
                            }
                            $optionsMerge['footer-center'] = '';
                            $options = $optionsMerge + array(
                                    'no-outline', // option without argument
                                    'disable-external-links',
                                    'disable-internal-links',
                                    'disable-forms',
                                    'footer-center' => '[page]',
                                    'orientation' => $margin['orientation'],
                                    'margin-top' => bnToen($margin['margin_top']) * 20,
                                    'margin-right' => bnToen($margin['margin_right']) * 20,
                                    'margin-bottom' => bnToen($margin['margin_bottom']) * 20,
                                    'margin-left' => bnToen($margin['margin_left']) * 20,
                                    'encoding' => 'UTF-8', // option with argument
                                    'binary' => ROOT . DS . 'vendor' . DS . 'profburial' . DS . 'wkhtmltopdf-binaries-centos6' . DS . 'bin' . DS . (defined("PDFBINARY")
                                            ? PDFBINARY : 'wkhtmltopdf-amd64-centos6'),
                                    'user-style-sheet' => WWW_ROOT . DS . 'assets' . DS . 'global' . DS . 'plugins' . DS . 'bootstrap' . DS . 'css' . DS . 'bootstrap.min.css',
                                );

                            $pdf = new Pdf($options);

                            $pdfbody = (!empty($potrojari->attached_potro) ? $potrojari->attached_potro . '<br>' : '') . $potrojari->potro_description;

                            $request = new \Cake\Network\Request();

                            $pdfbody = str_replace($request->webroot . 'img/', WWW_ROOT . 'img/',
                                $pdfbody);
                            $pdfbody = str_replace('lang="AR-SA"', 'lang="BN"', $pdfbody);
                            $pdfbody = str_replace('dir="RTL"', '', $pdfbody);
                            if (!defined('Live') || Live == 0) {
                                $background_image = 'background-image: url("' . WWW_ROOT . 'img/test-background.png' . '");background-repeat: no-repeat;background-position:center;';
                            } else {
                                $background_image = '';
                            }

                            $pdf->addPage("<html><head><script src='https://code.jquery.com/jquery-1.11.3.js'></script><style>body{
                font-family: nikosh,SolaimanLipi, Garamond,'Open Sans', sans-serif !important;
                font-size: 12pt!important;
                {$background_image}
            }
            thead { display: table-row-group}
    tfoot { display: table-row-group}
    tr { page-break-inside: avoid}
    thead th { height: 1.1cm; }
    .text-center{
        text-align: center!important;
    }
            .bangladate{
                border-bottom: 1px solid #000;
            }
            #sovapoti_signature img, #sender_signature img, #sender_signature2 img, #sender_signature3 img, #sender_signature4 img, #sender_signature5 img{
                height: 50px;
            }
            #upomontri_receiver_signature img,
    #sochib_signature img,
    #second_receiver_signature img,
    #third_receiver_signature img,
    #fourth_receiver_signature img,
    #fifth_receiver_signature img{
        height:50px;
    }
    table {
      table-layout: fixed;
      width: 100%!importan;
      white-space: nowrap;
    }
    table td , table th{
      white-space: pre-wrap;
      /*overflow: hidden;*/
      /*text-overflow: ellipsis;*/
      word-break: normal;
      word-wrap: break-word;
    }
    
    table th{
      word-break: normal;
    }
    .imei_table_area{
        overflow: hidden!important;
    }
    </style></head><body><script>
        $(function(){
            $('#sharok_no').parent('div').css('white-space','nowrap');
            $('#sharok_no2').parent('div').css('white-space','nowrap');
        })
</script>{$pdfbody}</body>   </html>");

                            //module
                            $path = 'Nothi';
                            $office = $office_id;

                            $pdf_path = $this->makeDirectory($path, $office);
                            //chmod($pdf_path, 777);

                            $canpdf = false;
                            $filename = $pdf_path . 'main_potro_' . $office_id . '_' . $potrojari_id . '.pdf';
                            if (file_exists($filename)) {
                                $canpdf = true;
                            } else {
                                $pdf->saveAs($filename);
                                if (file_exists($filename)) {
                                    $canpdf = true;
                                }
                            }
                            if ($canpdf) {
                                $recEmail = explode(',', $receiver['receiving_officer_email']);
                                if (!empty($recEmail)) {
                                    if ((defined("EMAILER_ACTIVE") && EMAILER_ACTIVE == 1)) {
                                        $recEmail = array_map('trim', $recEmail);
                                        $email_attachments = [];
                                        if (!empty($attachments)) {
                                            foreach ($attachments as $k => $file) {
                                                if (!empty($file)) {
                                                    if ($file['attachment_type'] == 'text' && $file['is_inline'] != 0) {
                                                        continue;
                                                        $signHide = 0;
                                                        $folderPath = 'Nothi/' . $office_id . '/';
                                                        $custom_filename = "potro_" . $potrojari_id . '_' . $potrojari->nothi_part_no . '_' . $k;
                                                        $pdf_status = $this->createPdf($custom_filename, $file['attachment_description'], $signHide, $folderPath, $margin, $office_id);
                                                        if ($pdf_status['status'] == 'success' && !empty($pdf_status['src'])) {
                                                            if (file_exists(FILE_FOLDER_DIR . $folderPath . $custom_filename . '.pdf')) {
                                                                $custom_name = (!empty($file['user_file_name']) ? $file['user_file_name'] . '_' : '') . $custom_filename;
                                                                $email_attachments[urldecode($custom_name)] = ['file' => (urldecode($pdf_status['src']))];
                                                            } else {
                                                                $potrojariSent = [
                                                                    'is_sent' => 0,
                                                                    'dak_id' => 0,
                                                                    'potro_status' => 'Not Sent',
                                                                    'task_reposponse' => 'Please resend.PDF not created.',
                                                                    'run_task' => 1,
                                                                    'retry_count' => intval($receiver['retry_count']) + 1,
                                                                    'modified' => date("Y-m-d H:i:s"),
                                                                    'created_by' => 0
                                                                ];
                                                                $potrojariRecieverTable->updateAll($potrojariSent, ['id' => $receiver['id']]);
                                                                $this->out('PDF not created');
                                                                //Can not save PDF . so need to add/edit potrojari cron request - start
                                                                $potrojariRequestTable = TableRegistry::get('PotrojariRequest');
                                                                $commandReceiver = ROOT . "/bin/cake cronjob potrojariOnulipiSent {$office_id} {$potrojari_id} '$domain'";
                                                                $potrojariRequestTable->saveData($commandReceiver, $office_id,
                                                                    $potrojari_id, 0, 0);
                                                                //Can not save PDF . so need to add/edit potrojari cron request - end
                                                                continue;
                                                            }
                                                        }
                                                    } else {
                                                        $filname = explode('/',
                                                            $file['file_name']);
                                                        $filepath = FILE_FOLDER_DIR . $file['file_name'];
                                                        $custom_name = (!empty($file['user_file_name']) ? $file['user_file_name'] . '_' : '') . $filname[count($filname) - 1];
                                                        $email_attachments[urldecode($custom_name)] = ['file' => (urldecode($filepath))];
                                                    }

                                                }
                                            }
                                        }

                                        $email_attachments['main_potro.pdf'] = ['file' => ($filename)];
                                        foreach ($recEmail as $k => $mail) {
                                            $nothi_emailer = new \App\Model\Custom\NothiEmailer();
                                            $nothi_emailer->to_email = $mail;
                                            $nothi_emailer->to_name = $receiver['receiving_officer_name'] . ' ' . $receiver['receiving_officer_designation_label'];
                                            $nothi_emailer->from_des = $potrojari['officer_name'] . ', ' . $potrojari['officer_designation_label'] . ', ' . $potrojari['office_name'];
                                            $nothi_emailer->subject = $potrojari->potro_subject;
                                            $nothi_emailer->email_body = "আপনার একটি পত্র এসেছে। মূল পত্র সহ সকল সংযুক্তি ইমেইলের সংযুক্তিতে দেয়া হলো। <br/><br/>ধন্যবাদ।";
                                            $nothi_emailer->office_id = 0;
                                            $nothi_emailer->office_name = "";
                                            $nothi_emailer->attachments = $email_attachments;
                                            $nothi_emailer->type = 'PotrojariOnulipi';
                                            $nothi_emailer->email_trace_id = $receiver['id'];
                                            $nothi_emailer->nothi_office = $office_id;

                                            $emailer_response = $this->sendPotroJariEmail(get_object_vars($nothi_emailer));
                                            if ($emailer_response == true) {
                                                if (empty($receiver['potro_status'])) {
                                                    $receiver['potro_status'] = 'পত্রটি মেইল সার্ভার এর কাছে প্রেরণ করা হয়েছে। অপেক্ষা করুন';
                                                }
                                            } else {
                                                $receiver['potro_status'] = $emailer_response;
                                            }
                                        }
                                    } else {
                                        //default emailer
                                        foreach ($recEmail as $k => $mail) {
                                            $email = new Email('default');

                                            $email->emailFormat('html')->from(['nothi@nothi.org.bd' => $potrojari->officer_name])
                                                ->to(trim($mail))
                                                ->subject($potrojari->potro_subject);


                                            if (!empty($attachments)) {
                                                foreach ($attachments as $k => $file) {
                                                    if (!empty($file)) {
                                                        if ($file['attachment_type'] == 'text' && $file['is_inline'] != 0) {
                                                            $signHide = 0;
                                                            $folderPath = 'Nothi/' . $office_id . '/';
                                                            $custom_filename = "potro_" . $potrojari_id . '_' . $potrojari->nothi_part_no;
                                                            $pdf_status = $this->createPdf($custom_filename, $file['attachment_description'], $signHide, $folderPath, $margin, $office_id);
                                                            if ($pdf_status['status'] == 'success' && !empty($pdf_status['src'])) {
                                                                if (file_exists(FILE_FOLDER_DIR . $folderPath . $custom_filename . '.pdf')) {
                                                                    $custom_name = (!empty($file['user_file_name']) ? $file['user_file_name'] . '_' : '') . $custom_filename;
                                                                    $email_attachments[urldecode($custom_name)] = ['file' => (urldecode($pdf_status['src']))];
                                                                } else {
                                                                    $potrojariSent = [
                                                                        'is_sent' => 0,
                                                                        'dak_id' => 0,
                                                                        'potro_status' => 'Not Sent',
                                                                        'task_reposponse' => 'Please resend.PDF not created',
                                                                        'run_task' => 1,
                                                                        'retry_count' => intval($receiver['retry_count']) + 1,
                                                                        'modified' => date("Y-m-d H:i:s"),
                                                                        'created_by' => 0
                                                                    ];
                                                                    $potrojariRecieverTable->updateAll($potrojariSent, ['id' => $receiver['id']]);
                                                                    $this->out('PDF problem');
                                                                    //Can not save PDF . so need to add/edit potrojari cron request - start
                                                                    $potrojariRequestTable = TableRegistry::get('PotrojariRequest');
                                                                    $commandReceiver = ROOT . "/bin/cake cronjob potrojariOnulipiSent {$office_id} {$potrojari_id} '$domain'";
                                                                    $potrojariRequestTable->saveData($commandReceiver, $office_id,
                                                                        $potrojari_id, 0, 0);
                                                                    //Can not save PDF . so need to add/edit potrojari cron request - end
                                                                    continue;
                                                                }
                                                            }
                                                        } else {
                                                            $filname = explode('/', $file['file_name']);
                                                            $filepath = FILE_FOLDER_DIR . $file['file_name'];
                                                            $custom_name = (!empty($file['user_file_name']) ? $file['user_file_name'] . '_' : '') . $filname[count($filname) - 1];
                                                            $email->addAttachments(array(
                                                                urldecode($custom_name) => ['file' => urldecode($filepath)]));
                                                        }

                                                    }
                                                }
                                            }

                                            $email->addAttachments(array('main_potro.pdf' => ['file' => $filename]));

                                            if ($email->sendWithoutException("আপনার একটি পত্র এসেছে। মূল পত্র সহ সকল সংযুক্তি ইমেইলের সংযুক্তিতে দেয়া হলো। <br/><br/>ধন্যবাদ।")) {
                                                $receiver['potro_status'] = 'Sent';
                                            }
                                        }
                                    }
                                }
                                $potrojariSent = [
                                    'dak_id' => 0,
                                    'potro_status' => ((defined("EMAILER_ACTIVE") && EMAILER_ACTIVE == 1 && defined('Live') && Live == 1)
                                    ) ? 'পত্রটি মেইল সার্ভার এর কাছে প্রেরণ করা হয়েছে। অপেক্ষা করুন।' : 'Sent',
                                    'task_reposponse' => $receiver['potro_status'],
                                    'is_sent' => ((defined("EMAILER_ACTIVE") && EMAILER_ACTIVE == 1 && defined('Live') && Live == 1)
                                    ) ? 1 : 1,
                                    'run_task' => 1,
                                    'retry_count' => intval($receiver['retry_count']) + 1,
                                    'modified' => date("Y-m-d H:i:s"),
                                    'created_by' => 0
                                ];
                                //send SMS -- default emailer
                                if ($potrojariSent['is_sent'] == 1 && $potrojariSent['potro_status'] == 'Sent' && !empty($receiver['officer_mobile']) && !empty($receiver['sms_message'])) {
                                    // can be multiple receiver
                                    $recSMS = explode('--', $receiver['officer_mobile']);
                                    if (!empty($recSMS)) {
                                        foreach ($recSMS as $receiverSMS) {
                                            $this->saveSMSRequest($receiverSMS, $receiver['sms_message']);
                                        }
                                    }
                                }
                                //send SMS
//                                $this->out('sent');
                            } else {
                                $potrojariSent = [
                                    'is_sent' => 0,
                                    'dak_id' => 0,
                                    'potro_status' => 'Not Sent',
                                    'task_reposponse' => "Failed! Please Resend.PDF not created",
                                    'run_task' => 1,
                                    'retry_count' => intval($receiver['retry_count']) + 1,
                                    'modified' => date("Y-m-d H:i:s"),
                                    'created_by' => 0
                                ];
                                $this->out('not sent.pdf problem');
                                $potrojariRecieverTable->updateAll($potrojariSent, ['id' => $receiver['id']]);
//                                $email         = new Email('default');
//                                $email->emailFormat('html')->from(['nothi@nothi.org.bd'])
//                                    ->to(trim('eather@tappware.com'))
//                                    ->subject("Folder permission missing");
//                                $email->sendWithoutException("Permission for ".$pdf_path." is missing. Error: ".$pdf->getError());

                                //Cannot Create PDF . so need to add/edit potrojari cron request - start
                                $potrojariRequestTable = TableRegistry::get('PotrojariRequest');
                                $commandOnulipi = ROOT . "/bin/cake cronjob potrojariOnulipiSent {$office_id} {$potrojari_id} '$domain'";
                                $potrojariRequestTable->saveData($commandOnulipi, $office_id, $potrojari_id, 0, 0);
                                //Cannot Create PDF . so need to add/edit potrojari cron request - end
                                continue;
                            }
                        } catch (\Exception $ex) {
                            $this->retryOnulipi[$receiver['id']] = isset($this->retryOnulipi[$receiver['id']])
                                ? ($this->retryOnulipi[$receiver['id']] + 1) : 1;
                            $potrojariSent = [
                                'is_sent' => 0,
                                'dak_id' => 0,
                                'potro_status' => 'Not Sent',
                                'task_reposponse' => $ex->getMessage(),
                                'run_task' => 1,
                                'retry_count' => intval($receiver['retry_count']) + 1,
                                'modified' => date("Y-m-d H:i:s"),
                                'created_by' => 0
                            ];
                            $this->out($ex->getMessage());
                            $potrojariRecieverTable->updateAll($potrojariSent, ['id' => $receiver['id']]);
                            //Exception occured . so need to add/edit potrojari cron request - start
                            $potrojariRequestTable = TableRegistry::get('PotrojariRequest');
                            $commandOnulipi = ROOT . "/bin/cake cronjob potrojariOnulipiSent {$office_id} {$potrojari_id} '$domain'";
                            $potrojariRequestTable->saveData($commandOnulipi, $office_id,
                                $potrojari_id, 0, 0);
                            //Exception  occured . so need to add/edit potrojari cron request - end
                            continue;
                        }
                    } else {
                        $potrojariSent = [
                            'dak_id' => 0,
                            'potro_status' => 'Not Sent',
                            'is_sent' => 1,
                            'task_reposponse' => 'Undefined Email',
                            'run_task' => 1,
                            'retry_count' => intval($receiver['retry_count']) + 1,
                            'modified' => date("Y-m-d H:i:s"),
                            'created_by' => 0
                        ];
                        $this->out('wrong email');
                        $potrojariRecieverTable->updateAll($potrojariSent, ['id' => $receiver['id']]);
                        //send SMS -- no email but can be a mobile number to send
                        if (!empty($receiver['officer_mobile']) && !empty($receiver['sms_message'])) {
                            // can be multiple receiver
                            $recSMS = explode('--', $receiver['officer_mobile']);
                            if (!empty($recSMS)) {
                                foreach ($recSMS as $receiverSMS) {
                                    $this->saveSMSRequest($receiverSMS, $receiver['sms_message']);
                                }
                            }
                        }
                        //send SMS
                    }

                    continue;
                }

                $this->switchOffice($receiver['receiving_office_id'], 'recieverOffice');
                TableRegistry::remove('DakDaptoriks');
                TableRegistry::remove('DakNagoriks');
                $table_instance_dd = TableRegistry::get('DakDaptoriks');
                $table_instance_dn = TableRegistry::get('DakNagoriks');

                //dak daptorik
                $dak_daptoriks = $table_instance_dd->newEntity();
                $dak_daptoriks->sender_sarok_no = $potrojari->sarok_no;
                $dak_daptoriks->sender_office_id = $potrojari->office_id;
                $dak_daptoriks->sender_officer_id = $potrojari->officer_id;
                $dak_daptoriks->sender_office_name = $potrojari->office_name;
                $dak_daptoriks->sender_office_unit_id = $potrojari->office_unit_id;
                $dak_daptoriks->sender_office_unit_name = $potrojari->office_unit_name;
                $dak_daptoriks->sender_officer_designation_id = $potrojari->officer_designation_id;
                $dak_daptoriks->sender_officer_designation_label = $potrojari->officer_designation_label;
                $dak_daptoriks->sender_name = $potrojari->officer_name;

                $dak_daptoriks->sending_date = $potrojari->potrojari_date;
                $dak_daptoriks->dak_sending_media = 1;
                $dak_daptoriks->dak_received_no = $this->generateDakReceivedNo($dak_daptoriks->sender_office_id);
                $dak_daptoriks->receiving_date = date("Y-m-d H:i:s");
                $dak_daptoriks->uploader_designation_id = $potrojari->officer_designation_id;
                $dak_daptoriks->dak_subject = $potrojari->potro_subject;
                $dak_daptoriks->dak_security_level = $potrojari->potro_security_level;
                $dak_daptoriks->dak_priority_level = $potrojari->potro_priority_level;
                $dak_daptoriks->dak_description = (!empty($potrojari->attached_potro) ? $potrojari->attached_potro . '<br>' : '') . $potrojari->potro_description;
                $dak_daptoriks->is_summary_nothi = $potrojari->is_summary_nothi;
                $dak_daptoriks->meta_data = $potrojari->meta_data;
                $dak_daptoriks->dak_cover = !empty($potrojari->potro_cover) ? $potrojari->potro_cover
                    : '';


                $dak_daptoriks->receiving_office_id = $receiver['receiving_office_id'];
                $dak_daptoriks->receiving_officer_designation_id = $receiver['receiving_officer_designation_id'];
                $dak_daptoriks->receiving_office_unit_id = $receiver['receiving_office_unit_id'];
                $dak_daptoriks->receiving_office_unit_name = !empty($receiver['receiving_office_unit_name'])
                    ? $receiver['receiving_office_unit_name'] : '';
                $dak_daptoriks->receiving_officer_id = $receiver['receiving_officer_id'];
                $dak_daptoriks->receiving_officer_designation_label = !empty($receiver['receiving_officer_designation_label'])
                    ? $receiver['receiving_officer_designation_label'] : '';
                $dak_daptoriks->receiving_officer_name = !empty($receiver['receiving_officer_name'])
                    ? $receiver['receiving_officer_name'] : '';

                $dak_countdp = $table_instance_dd->find()->where(['receiving_office_id' => $receiver['receiving_office_id']])->order(['cast(docketing_no as signed)' => 'desc'])->first();
                $dak_countng = $table_instance_dn->find()->where(['receiving_office_id' => $receiver['receiving_office_id']])->order(['cast(docketing_no as signed)' => 'desc'])->first();
                $dak_count = max([(!empty($dak_countdp['docketing_no']) ? $dak_countdp['docketing_no']
                        : 0), (!empty($dak_countng['docketing_no']) ? $dak_countng['docketing_no']
                        : 0)]) + 1;


                $dak_daptoriks->docketing_no = $dak_count;
                $dak_daptoriks->dak_status = DAK_CATEGORY_INBOX;
                $dak_daptoriks->receiving_date = date("Y-m-d H:i:s");
                $dak_daptoriks->sending_date = date("Y-m-d H:i:s");
                $dak_daptoriks->created_by = 0;
                $dak_daptoriks->modified_by = 0;
                $dak = $table_instance_dd->save($dak_daptoriks);

                //other
                TableRegistry::remove('DakUsers');
                $table_instance_du = TableRegistry::get('DakUsers');

                $drafterOther = $table_instance_du->newEntity();
                $drafterOther->dak_type = DAK_DAPTORIK;
                $drafterOther->dak_id = $dak->id;
                $drafterOther->to_office_id = $dak_daptoriks->receiving_office_id;
                $drafterOther->to_office_name = $potrojari->office_name;
                $drafterOther->to_office_unit_id = $dak_daptoriks->receiving_office_unit_id;
                $drafterOther->to_office_unit_name = $dak_daptoriks->receiving_office_unit_name;
                $drafterOther->to_office_address = '';
                $drafterOther->to_officer_id = $dak_daptoriks->receiving_officer_id;
                $drafterOther->to_officer_name = $dak_daptoriks->receiving_officer_name;
                $drafterOther->to_officer_designation_id = $dak_daptoriks->receiving_officer_designation_id;
                $drafterOther->to_officer_designation_label = $dak_daptoriks->receiving_officer_designation_label;
                $drafterOther->dak_view_status = DAK_VIEW_STATUS_NEW;
                $drafterOther->dak_priority = 0;
                $drafterOther->is_summary_nothi = $potrojari->is_summary_nothi;
                $drafterOther->attention_type = 1;
                $drafterOther->dak_category = DAK_CATEGORY_INBOX;
                $drafterOther->created_by = 0;
                $drafterOther->modified_by = 0;

                $drafterOther = $table_instance_du->save($drafterOther);

                TableRegistry::remove('DakUserActions');
                $table_instance_dua = TableRegistry::get('DakUserActions');
                $user_action = $table_instance_dua->newEntity();
                $user_action->dak_id = $dak->id;
                $user_action->dak_user_id = $drafterOther->id;
                $user_action->dak_action = DAK_CATEGORY_INBOX;
                $user_action->is_summary_nothi = $potrojari->is_summary_nothi;
                $user_action->created_by = 0;
                $user_action->modified_by = 0;
                $table_instance_dua->save($user_action);

                TableRegistry::remove('DakAttachments');
                $table_instance_da = TableRegistry::get('DakAttachments');
                //first attachment then main potro
                if (!empty($attachments)) {
                    foreach ($attachments as $k => $file) {
                        if (!empty($file)) {
                            if ($file['attachment_type'] == 'text' && $file['is_inline'] == 0) {
                                continue;
                            }
                            $attachment_data = array();
                            $attachment_data['dak_type'] = "Daptorik";
                            $attachment_data['dak_id'] = $dak->id;
                            $attachment_data['attachment_description'] = '';
                            if ($file['attachment_type'] == 'text') {
                                $attachment_data['content_body'] = $file['content_body'];
                            }
                            $attachment_data['attachment_type'] = $file['attachment_type'];
                            $attachment_data['file_dir'] = $file['file_dir'];
                            $attachment_data['file_name'] = $file['file_name'];
                            $attachment_data['user_file_name'] = $file['user_file_name'];
                            if ($potrojari->is_summary_nothi) {
                                $attachment_data['is_summary_nothi'] = 1;
                            }

                            $dak_attachment = $table_instance_da->newEntity();
                            $dak_attachment = $table_instance_da->patchEntity($dak_attachment,
                                $attachment_data);
                            $dak_attachment->created_by = 0;
                            $dak_attachment->modified_by = 0;
                            $table_instance_da->save($dak_attachment);
                        }
                    }
                }
                //first attachment then main potro
                if (!empty($potrojari->potro_description)) {
                    $attachment_data = array();
                    $attachment_data['dak_type'] = "Daptorik";
                    $attachment_data['dak_id'] = $dak->id;
                    $attachment_data['attachment_description'] = '';
                    $attachment_data['is_main'] = 1;

                    $attachment_data['attachment_type'] = 'text';
                    $attachment_data['meta_data'] = $potrojari->meta_data;
                    $attachment_data['content_body'] = (!empty($potrojari->attached_potro) ? $potrojari->attached_potro . '<br>' : '') . $potrojari->potro_description;
                    if ($potrojari->is_summary_nothi) {
                        $attachment_data['content_cover'] = $potrojari->potro_cover;
                        $attachment_data['is_summary_nothi'] = 1;
                    }

                    $attachment_data['file_dir'] = '';
                    $attachment_data['file_name'] = '';

                    $dak_attachment = $table_instance_da->newEntity();
                    $dak_attachment = $table_instance_da->patchEntity($dak_attachment,
                        $attachment_data);
                    $dak_attachment->created_by = 0;
                    $dak_attachment->modified_by = 0;
                    $table_instance_da->save($dak_attachment);
                }


                /* Update Dak Movement Table */
                TableRegistry::remove('DakMovements');
                $DakMovementsTable = TableRegistry::get('DakMovements');

                /* Sender to Draft User Movement */
                $second_move = $DakMovementsTable->newEntity();
                $second_move->dak_type = DAK_DAPTORIK;
                $second_move->dak_id = $dak->id;
                $second_move->from_office_id = $dak['sender_office_id'];
                $second_move->from_office_name = $dak['sender_office_name'];
                $second_move->from_office_unit_id = $dak['sender_office_unit_id'];
                $second_move->from_office_unit_name = $dak['sender_office_unit_name'];
                $second_move->from_office_address = '';
                $second_move->from_officer_id = $dak['sender_officer_id'];
                $second_move->from_officer_name = $dak['sender_name'];
                $second_move->from_officer_designation_id = $dak['sender_officer_designation_id'];
                $second_move->from_officer_designation_label = $dak['sender_officer_designation_label'];


                $second_move->to_office_id = $drafterOther['to_office_id'];
                $second_move->to_office_name = $drafterOther['to_office_name'];
                $second_move->to_office_unit_id = $drafterOther['to_office_unit_id'];
                $second_move->to_office_unit_name = $drafterOther['to_office_unit_name'];
                $second_move->to_office_address = $drafterOther['to_office_address'];
                $second_move->to_officer_id = $drafterOther['to_officer_id'];
                $second_move->to_officer_name = $drafterOther['to_officer_name'];
                $second_move->to_officer_designation_id = $drafterOther['to_officer_designation_id'];
                $second_move->to_officer_designation_label = $drafterOther['to_officer_designation_label'];
                $second_move->attention_type = 1;
                $second_move->docketing_no = $dak['docketing_no'];
                $second_move->from_sarok_no = "";
                $second_move->to_sarok_no = "";
                $second_move->operation_type = DAK_CATEGORY_FORWARD;
                $second_move->sequence = 2;
                $second_move->dak_actions = '';
                $second_move->is_summary_nothi = $potrojari->is_summary_nothi;
                $second_move->dak_priority = $dak['dak_priority_level'];
                $second_move->from_potrojari = 1;
                $second_move->created_by = 0;
                $second_move->modified_by = 0;
                $DakMovementsTable->save($second_move);

                $potrojariSent = [
                    'dak_id' => $dak->id,
                    'potro_status' => 'Sent',
                    'is_sent' => 1,
                    'run_task' => 1,
                    'retry_count' => intval($receiver['retry_count']) + 1,
                    'modified' => date("Y-m-d H:i:s"),
                ];

                $selected_office_section = [
                    'office_id' => $office_id,
                    'office_unit_id' => $potrojari->office_unit_id,
                    'office_unit_organogram_id' => $potrojari->officer_designation_id,
                    'incharge_label' => '',
                    'office_name' => $potrojari->office_name,
                    'office_address' => '',
                    'designation_label' => $potrojari->officer_designation_label,
                    'officer_name' => $potrojari->officer_name,
                    'ministry_records' => '',
                    'office_phone' => '',
                    'office_fax' => '',
                    'office_email' => '',
                    'office_web' => '',
                    'office_unit_name' => $potrojari->office_unit_name,
                ];

                $toUser['office_id'] = $receiver['receiving_office_id'];
                $toUser['officer_id'] = $receiver['receiving_officer_id'];
                if ($potrojari->is_summary_nothi) {
                    if (!$this->NotificationSet('inbox',
                        array("1", "Sar Sonkhep sent as Potrojari"), 1,
                        $selected_office_section, $toUser, $potrojari->potro_subject)
                    ) {

                    }
                } else {
                    $priority = json_decode(DAK_PRIORITY_TYPE, true);
                    $security = json_decode(DAK_SECRECY_TYPE, true);
                    $status = (isset($priority[$dak['dak_priority_level']]) && $dak['dak_priority_level']
                        > 0 ? ('"' . $priority[$dak['dak_priority_level']] . '" ') : '') . (isset($security[$dak['dak_security_level']])
                        && $dak['dak_security_level'] > 0 ? ('"' . $security[$dak['dak_security_level']] . '" ')
                            : '');

                    if (!$this->NotificationSet('inbox',
                        array("1", ("Daptorik Dak sent as Potrojari")), 1,
                        $selected_office_section, $toUser, $potrojari->potro_subject)
                    ) {

                    }
                }

                $potrojariRecieverTable->updateAll($potrojariSent, ['id' => $receiver['id']]);
            } catch (\Exception $ex) {

                if (!empty($receiver)) {
                    $this->retryOnulipi[$receiver['id']] = isset($this->retryOnulipi[$receiver['id']])
                        ? ($this->retryOnulipi[$receiver['id']] + 1) : 1;

                    $potrojariSent = [
                        'dak_id' => 0,
                        'is_sent' => 0,
                        'potro_status' => 'Not Sent',
                        'task_reposponse' => $ex->getMessage(),
                        'run_task' => 1,
                        'retry_count' => intval($receiver['retry_count']) + 1,
                        'modified' => date("Y-m-d H:i:s"),
                        'created_by' => 0
                    ];
                    $potrojariRecieverTable->updateAll($potrojariSent, ['id' => $receiver['id']]);
                    //Exception occured . so need to add/edit potrojari cron request - start
                    $potrojariRequestTable = TableRegistry::get('PotrojariRequest');
                    $commandOnulipi = ROOT . "/bin/cake cronjob potrojariOnulipiSent {$office_id} {$potrojari_id} '$domain'";
                    $potrojariRequestTable->saveData($commandOnulipi, $office_id, $potrojari_id, 0,
                        0);
                    $this->out($ex->getMessage());
                    //Exception  occured . so need to add/edit potrojari cron request - end
                }
            }
        }

        endline:
        ConnectionManager::drop('default');
        ConnectionManager::drop('recieverOffice');
    }

    public function potrojariReceiverSentAll($office_id, $office_unit_organogram_id, $domain = '', $force = 0)
    {
        set_time_limit(0);
        $this->switchOffice($office_id, 'cronOffice');

        $receiverTable = TableRegistry::get('PotrojariReceiver');
        $nothiMasterCurrentUsers = TableRegistry::get('NothiMasterCurrentUsers');
        $allCurrentNothi = $nothiMasterCurrentUsers->getAllNothiPartNo($office_id, 0, $office_unit_organogram_id,[],1)->where(['nothi_office' => $office_id])->toArray();
        $potrojariLists = $receiverTable->find()->join([
            "Potrojari" => [
                'table' => 'potrojari', 'type' => 'inner',
                'conditions' => ['PotrojariReceiver.potrojari_id =Potrojari.id']
            ]
        ])->where(['PotrojariReceiver.is_sent' => 0, 'Potrojari.nothi_part_no IN' => $allCurrentNothi, 'Potrojari.potro_status <>' => 'Draft'])->distinct(['Potrojari.id'])->toArray();

        foreach ($potrojariLists as $key => $potrojariList) {
            $potrojari_id = $potrojariList['potrojari_id'];
//            $this->out($potrojari_id);
            $domain = \Cake\Routing\Router::url(['controller' => "potrojari",
                'action' => "potrojariPreview", $office_id, $potrojari_id], true);
            $this->potrojariReceiverSent($office_id, $potrojari_id, $domain, 1);
        }
        endline:
        ConnectionManager::drop('default');
        ConnectionManager::drop('recieverOffice');
    }

    public function potrojariOnulipiSentAll($office_id, $office_unit_organogram_id, $domain = '', $force = 0)
    {

        set_time_limit(0);
//        $potrojariSent = [];
        $this->switchOffice($office_id, 'cronOffice');

        $onulipiTable = TableRegistry::get('PotrojariOnulipi');
        $nothiMasterCurrentUsers = TableRegistry::get('NothiMasterCurrentUsers');
        $allCurrentNothi = $nothiMasterCurrentUsers->getAllNothiPartNo($office_id, 0, $office_unit_organogram_id,[],1)->where(['nothi_office' => $office_id])->toArray();
        $potrojariLists = $onulipiTable->find()->join([
            "Potrojari" => [
                'table' => 'potrojari', 'type' => 'inner',
                'conditions' => ['PotrojariOnulipi.potrojari_id =Potrojari.id']
            ]
        ])->where(['PotrojariOnulipi.is_sent' => 0, 'Potrojari.nothi_part_no IN' => $allCurrentNothi, 'Potrojari.potro_status <>' => 'Draft'])->distinct(['Potrojari.id'])->toArray();

        foreach ($potrojariLists as $key => $potrojariList) {
            $potrojari_id = $potrojariList['potrojari_id'];
//            $this->out($potrojari_id);
            $domain = \Cake\Routing\Router::url(['controller' => "potrojari",
                'action' => "potrojariPreview", $office_id, $potrojari_id], true);
            $this->potrojariOnulipiSent($office_id, $potrojari_id, $domain, 1);
        }
        endline:
        ConnectionManager::drop('default');
        ConnectionManager::drop('recieverOffice');
    }

    private function generateDakReceivedNo($office_id)
    {

        TableRegistry::remove('DakDaptoriks');
        $table_instance_dd = TableRegistry::get('DakDaptoriks');
        $dak_count = $table_instance_dd->find()->where(['receiving_office_id' => $office_id, 'date(created)' => date('Y-m-d')])->count();

        startagain:
        $dak_count++;
        $year = date('y');
        $month = date('m');
        $day = date('d');
        $daktype = 2;
        $offline = 1;

        $received_no = "";

        $received_no = $daktype . $offline . str_pad($office_id, 5, "0", STR_PAD_LEFT) . $year . $month . $day . str_pad($dak_count,
                3, "0", STR_PAD_LEFT);

        $exists = $table_instance_dd->find()->where(['dak_received_no' => $received_no])->count();
        if ($exists) {
            goto startagain;
        }
        return $received_no;
    }


    private function cronForOfficeDashboard()
    {
        ini_set('memory_limit', -1);
        set_time_limit(0);
        $start_time = strtotime(date('Y-m-d H:i:s'));
        TableRegistry::remove('OfficeDomains');
        $office_domains = TableRegistry::get('OfficeDomains');
        $all_offices = $office_domains->getOfficesData();
        $data = [];
        $total = 0;
        $save = 0;
        $yesterday = date('Y-m-d', strtotime(date('Y-m-d') . ' -1 day'));
//            pr($all_offices);
        TableRegistry::remove('Dashboard');
        $DashboardTable = TableRegistry::get('Dashboard');
        TableRegistry::remove('DashboardOffices');
        $dashboardOfficesTable = TableRegistry::get('DashboardOffices');
        $performance_officesTable = TableRegistry::get('PerformanceOffices');
        foreach ($all_offices as $office_id => $office_name) {
            if (empty($office_id)) {
                continue;
            }
            //re-initialize again to skip MySQL server gone away error
            TableRegistry::remove('Dashboard');
            $DashboardTable = TableRegistry::get('Dashboard');
            TableRegistry::remove('DashboardOffices');
            $dashboardOfficesTable = TableRegistry::get('DashboardOffices');
            $performance_officesTable = TableRegistry::get('PerformanceOffices');
            //re-initialize again to skip MySQL server gone away error

            $total++;
            $f = $dashboardOfficesTable->checkUpdate($office_id, date('Y-m-d'));
            if ($f > 0) {
                continue;
            }
            try {
                $this->switchOffice($office_id, 'DashboardOffice');
                $last_update = $dashboardOfficesTable->getLastUpdateDate($office_id);
                if (!empty($last_update['operation_date'])) {
                    $time = new Time($last_update['operation_date']);
                    $last_update['operation_date'] = $time->i18nFormat(null, null, 'en-US');
                }
                $dt = !empty($last_update['operation_date']) ? date('Y-m-d',
                    strtotime($last_update['operation_date'])) : '';

                // Collect From Designation Table
                $condition = [
                    'totalinboxall' => 'SUM(inbox)', 'totaloutboxall' => 'SUM(outbox)', 'totalnothijatoall' => 'SUM(nothijat)',
                    'totalnothivuktoall' => 'SUM(nothivukto)', 'totalnisponnodakall' => 'SUM(nisponnodak)',
                    'totalsrijitonoteall' => 'SUM(selfnote)', 'totalOnisponnodakall' => 'SUM(onisponnodak)',
                    'totaldaksohonoteall' => 'SUM(daksohonote)', 'totalnisponnopotrojariall' => 'SUM(nisponnopotrojari)',
                    'totalnisponnonoteall' => 'SUM(nisponnonote)', 'totalpotrojariall' => 'SUM(potrojari)',
                    'totalOnisponnonoteall' => 'SUM(onisponnonote)',
                    'totalID' => 'count(id)'
                ];
                if (!empty($dt)) {
                    $hasDataInPerformanceOffices = $performance_officesTable->getOfficeData($office_id, [$dt, $yesterday])->select($condition)->group(['office_id'])->first();
                }
                if (!empty($hasDataInPerformanceOffices['totalID']) && $hasDataInPerformanceOffices['totalID'] > 0) {
                    // get yesterday data
                    $performanceOffice_yesterday = $performance_officesTable->getOfficeData($office_id, [$yesterday, $yesterday])->select($condition)->group(['office_id'])->first();
                    if (empty($performanceOffice_yesterday)) {
                        $dak_yesterday = $DashboardTable->OfficeDashboardUpdateDak($office_id, $yesterday);
                        $nothi_yesterday = $DashboardTable->OfficeDashboardUpdateNothi($office_id,
                            $yesterday);
                    }

                    $dak['totalinboxall'] = $hasDataInPerformanceOffices['totalinboxall'];
                    $dak['totaloutboxall'] = $hasDataInPerformanceOffices['totaloutboxall'];
                    $dak['totalnothivuktoall'] = $hasDataInPerformanceOffices['totalnothivuktoall'];
                    $dak['totalnothijatoall'] = $hasDataInPerformanceOffices['totalnothijatoall'];
                    $dak['totalnisponnodakall'] = $hasDataInPerformanceOffices['totalnisponnodakall'];
                    $dak['totalOnisponnodakall'] = (!empty($performanceOffice_yesterday)) ? $performanceOffice_yesterday['totalOnisponnodakall'] : $dak_yesterday['totalOnisponnodakall'];
                    $nothi['totalsrijitonoteall'] = $hasDataInPerformanceOffices['totalsrijitonoteall'];
                    $nothi['totaldaksohonoteall'] = $hasDataInPerformanceOffices['totaldaksohonoteall'];
                    $nothi['totalnisponnonoteall'] = $hasDataInPerformanceOffices['totalnisponnonoteall'];
                    $nothi['totalnisponnopotrojariall'] = $hasDataInPerformanceOffices['totalnisponnopotrojariall'];
                    $nothi['totalOnisponnonoteall'] = (!empty($performanceOffice_yesterday)) ? $performanceOffice_yesterday['totalOnisponnonoteall'] : $nothi_yesterday['totalOnisponnonoteall'];
                    $dak['totalpotrojariall'] = $hasDataInPerformanceOffices['totalpotrojariall'];

                    if (!empty($performanceOffice_yesterday)) {
                        $dak_yesterday['totalinboxall'] = $performanceOffice_yesterday['totalinboxall'];
                        $dak_yesterday['totaloutboxall'] = $performanceOffice_yesterday['totaloutboxall'];
                        $dak_yesterday['totalnothivuktoall'] = $performanceOffice_yesterday['totalnothivuktoall'];
                        $dak_yesterday['totalnothijatoall'] = $performanceOffice_yesterday['totalnothijatoall'];
                        $dak_yesterday['totalnisponnodakall'] = $performanceOffice_yesterday['totalnisponnodakall'];
                        $dak_yesterday['totalOnisponnodakall'] = $performanceOffice_yesterday['totalOnisponnodakall'];
                        $nothi_yesterday['totalsrijitonoteall'] = $performanceOffice_yesterday['totalsrijitonoteall'];
                        $nothi_yesterday['totaldaksohonoteall'] = $performanceOffice_yesterday['totaldaksohonoteall'];
                        $nothi_yesterday['totalnisponnonoteall'] = $performanceOffice_yesterday['totalnisponnonoteall'];
                        $nothi_yesterday['totalnisponnopotrojariall'] = $performanceOffice_yesterday['totalnisponnopotrojariall'];
                        $nothi_yesterday['totalOnisponnonoteall'] = $performanceOffice_yesterday['totalOnisponnonoteall'];
                        $dak_yesterday['totalpotrojariall'] = $performanceOffice_yesterday['totalpotrojariall'];
                    }
                } // Collect From Designation Table
                else {

                    $dak = $DashboardTable->OfficeDashboardUpdateDak($office_id, $dt);
                    $dak_yesterday = $DashboardTable->OfficeDashboardUpdateDak($office_id, $yesterday);
                    $nothi = $DashboardTable->OfficeDashboardUpdateNothi($office_id, $dt);
                    $nothi_yesterday = $DashboardTable->OfficeDashboardUpdateNothi($office_id,
                        $yesterday);
                }

                if (!empty($dt)) {
                    $save_type = 'partial';
                } else {
                    $save_type = 'full';
                }

                if (empty($dak) || empty($nothi) || !isset($dak['totalinboxall']) || !isset($nothi['totalsrijitonoteall'])) {
                    $data[] = [
                        'office_id' => $office_id,
                        'office_name' => $office_name,
                        'msg' => 'Empty Data',
                    ];
                    continue;
                }
                if ($save_type == 'full') {
                    if ($dak['totalnothivuktoall'] < $nothi['totaldaksohonoteall']) {
                        $nothi['totaldaksohonoteall'] = $dak['totalnothivuktoall'];
                    }
                    if ($dak['totalpotrojariall'] < $nothi['totalnisponnopotrojariall']) {
                        $nothi['totalnisponnopotrojariall'] = $dak['totalpotrojariall'];
                    }
                    // yesterday
                    if ($dak_yesterday['totalpotrojariall'] < $nothi_yesterday['totalnisponnopotrojariall']) {
                        $nothi_yesterday['totalnisponnopotrojariall'] = $dak_yesterday['totalpotrojariall'];
                    }
                    if ($dak_yesterday['totalnothivuktoall'] < $nothi_yesterday['totaldaksohonoteall']) {
                        $nothi_yesterday['totaldaksohonoteall'] = $dak_yesterday['totalnothivuktoall'];
                    }
                    $nothi_yesterday['totalOnisponnonoteall'] = $nothi['totalOnisponnonoteall'];
                    // yesterday
                    $nothi['totalsrijitonoteall'] = ($nothi['totalnisponnopotrojariall']
                        + $nothi['totalnisponnonoteall'] + $nothi['totalOnisponnonoteall'] - $nothi['totaldaksohonoteall']);
                    if ($nothi['totalsrijitonoteall'] < 0) {
                        $nothi['totalsrijitonoteall'] = 0;
                    }
                    if ($nothi_yesterday['totalsrijitonoteall'] < 0) {
                        $nothi_yesterday['totalsrijitonoteall'] = 0;
                    }
                    $save_data = [
                        'office_id' => $office_id, 'operation_date' => date('Y-m-d'),
                        'dak_inbox' => $dak['totalinboxall'], 'dak_outbox' => $dak['totaloutboxall'],
                        'nothivukto' => $dak['totalnothivuktoall'],
                        'nothijat' => $dak['totalnothijatoall'], 'dak_nisponno' => $dak['totalnisponnodakall'],
                        'dak_onisponno' => $dak['totalOnisponnodakall'],
                        'self_note' => $nothi['totalsrijitonoteall'], 'dak_note' => $nothi['totaldaksohonoteall'],
                        'note_nisponno' => $nothi['totalnisponnonoteall'],
                        'potrojari_nisponno' => $nothi['totalnisponnopotrojariall'],
                        'onisponno_note' => $nothi['totalOnisponnonoteall'],
                        'potrojari' => $dak['totalpotrojariall'],
                        //yesterday
                        'yesterday_inbox' => $dak_yesterday['totalinboxall'], 'yesterday_outbox' => $dak_yesterday['totaloutboxall'],
                        'yesterday_nothivukto' => $dak_yesterday['totalnothivuktoall'],
                        'yesterday_nothijat' => $dak_yesterday['totalnothijatoall'], 'yesterday_nisponno_dak' => $dak_yesterday['totalnisponnodakall'],
                        'yesterday_onisponno_dak' => $dak_yesterday['totalOnisponnodakall'],
                        'yesterday_self_note' => $nothi_yesterday['totalsrijitonoteall'], 'yesterday_dak_note' => $nothi_yesterday['totaldaksohonoteall'],
                        'yesterday_niponno_note' => $nothi_yesterday['totalnisponnonoteall'],
                        'yesterday_nisponno_potrojari' => $nothi_yesterday['totalnisponnopotrojariall'],
                        'yesterday_onisponno_note' => $nothi_yesterday['totalOnisponnonoteall'],
                        'yesterday_potrojari' => $dak_yesterday['totalpotrojariall'],
                    ];
                } else {
                    $last_data = $dashboardOfficesTable->getData($office_id);
                    if ($dak['totalpotrojariall'] + $last_data['totalPotrojari'] < $nothi['totalnisponnopotrojariall']
                        + $last_data['totalNisponnoPotrojari']
                    ) {
                        $nothi['totalnisponnopotrojariall'] = $dak['totalpotrojariall'] + $last_data['totalPotrojari'];
                    } else {
                        $nothi['totalnisponnopotrojariall'] = $nothi['totalnisponnopotrojariall'] + $last_data['totalNisponnoPotrojari'];
                    }
                    if ($dak['totalnothivuktoall'] + $last_data['totalNothivukto'] < $nothi['totaldaksohonoteall']
                        + $last_data['totalDaksohoNote']
                    ) {
                        $nothi['totaldaksohonoteall'] = $dak['totalnothivuktoall'] + $last_data['totalNothivukto'];
                    } else {
                        $nothi['totaldaksohonoteall'] = $nothi['totaldaksohonoteall'] + $last_data['totalDaksohoNote'];
                    }
                    // yesterday
                    if ($dak_yesterday['totalpotrojariall'] < $nothi_yesterday['totalnisponnopotrojariall']) {
                        $nothi_yesterday['totalnisponnopotrojariall'] = $dak_yesterday['totalpotrojariall'];
                    }
                    if ($dak_yesterday['totalnothivuktoall'] < $nothi_yesterday['totaldaksohonoteall']) {
                        $nothi_yesterday['totaldaksohonoteall'] = $dak_yesterday['totalnothivuktoall'];
                    }
                    // yesterday
                    $nothi['totalOnisponnonoteall'] = $DashboardTable->getOnisponnoNoteCount($office_id,
                        0, 0);
                    $nothi['totalsrijitonoteall'] = ($nothi['totalnisponnopotrojariall']
                        + $nothi['totalnisponnonoteall'] + $last_data['totalNisponnoNote'] + $nothi['totalOnisponnonoteall']
                        - $nothi['totaldaksohonoteall']);
                    //yesterday
                    $nothi_yesterday['totalOnisponnonoteall'] = $nothi['totalOnisponnonoteall'];
                    //yesterday
                    $save_data = [
                        'office_id' => $office_id, 'operation_date' => date('Y-m-d'),
                        'dak_inbox' => $dak['totalinboxall'] + $last_data['totalInbox'],
                        'dak_outbox' => $dak['totaloutboxall'] + $last_data['totalOutbox'],
                        'nothivukto' => $dak['totalnothivuktoall'] + $last_data['totalNothivukto'],
                        'nothijat' => $dak['totalnothijatoall'] + $last_data['totalNothijato'],
                        'dak_nisponno' => $dak['totalnisponnodakall'] + $last_data['totalNisponnoDak'],
                        'dak_onisponno' => $dak['totalOnisponnodakall'],
                        'self_note' => $nothi['totalsrijitonoteall'],
                        'dak_note' => $nothi['totaldaksohonoteall'],
                        'note_nisponno' => $nothi['totalnisponnonoteall'] + $last_data['totalNisponnoNote'],
                        'potrojari_nisponno' => $nothi['totalnisponnopotrojariall'],
                        'onisponno_note' => $nothi['totalOnisponnonoteall'],
                        'potrojari' => $dak['totalpotrojariall'] + $last_data['totalPotrojari'],
                        //yesterday
                        'yesterday_inbox' => $dak_yesterday['totalinboxall'], 'yesterday_outbox' => $dak_yesterday['totaloutboxall'],
                        'yesterday_nothivukto' => $dak_yesterday['totalnothivuktoall'],
                        'yesterday_nothijat' => $dak_yesterday['totalnothijatoall'], 'yesterday_nisponno_dak' => $dak_yesterday['totalnisponnodakall'],
                        'yesterday_onisponno_dak' => $dak_yesterday['totalOnisponnodakall'],
                        'yesterday_self_note' => $nothi_yesterday['totalsrijitonoteall'], 'yesterday_dak_note' => $nothi_yesterday['totaldaksohonoteall'],
                        'yesterday_niponno_note' => $nothi_yesterday['totalnisponnonoteall'],
                        'yesterday_nisponno_potrojari' => $nothi_yesterday['totalnisponnopotrojariall'],
                        'yesterday_onisponno_note' => $nothi_yesterday['totalOnisponnonoteall'],
                        'yesterday_potrojari' => $dak_yesterday['totalpotrojariall'],
                    ];
                }

                $dashboardOfficesTable->deleteAll(['office_id' => $office_id]);
                $DashboardOfficesEntity = $dashboardOfficesTable->newEntity();
                $DashboardOfficesEntity->office_id = $save_data['office_id'];
                $DashboardOfficesEntity->operation_date = $save_data['operation_date'];
                $DashboardOfficesEntity->dak_inbox = $save_data['dak_inbox'];
                $DashboardOfficesEntity->dak_outbox = $save_data['dak_outbox'];
                $DashboardOfficesEntity->nothivukto = $save_data['nothivukto'];
                $DashboardOfficesEntity->nothijat = $save_data['nothijat'];
                $DashboardOfficesEntity->dak_nisponno = $save_data['dak_nisponno'];
                $DashboardOfficesEntity->dak_onisponno = $save_data['dak_onisponno'];
                $DashboardOfficesEntity->self_note = $save_data['self_note'];
                $DashboardOfficesEntity->dak_note = $save_data['dak_note'];
                $DashboardOfficesEntity->potrojari_nisponno = $save_data['potrojari_nisponno'];
                $DashboardOfficesEntity->note_nisponno = $save_data['note_nisponno'];
                $DashboardOfficesEntity->onisponno_note = $save_data['onisponno_note'];
                $DashboardOfficesEntity->potrojari = $save_data['potrojari'];
                //yesterday
                $DashboardOfficesEntity->yesterday_inbox = $save_data['yesterday_inbox'];
                $DashboardOfficesEntity->yesterday_outbox = $save_data['yesterday_outbox'];
                $DashboardOfficesEntity->yesterday_nothivukto = $save_data['yesterday_nothivukto'];
                $DashboardOfficesEntity->yesterday_nothijat = $save_data['yesterday_nothijat'];
                $DashboardOfficesEntity->yesterday_nisponno_dak = $save_data['yesterday_nisponno_dak'];
                $DashboardOfficesEntity->yesterday_onisponno_dak = $save_data['yesterday_onisponno_dak'];
                $DashboardOfficesEntity->yesterday_self_note = $save_data['yesterday_self_note'];
                $DashboardOfficesEntity->yesterday_dak_note = $save_data['yesterday_dak_note'];
                $DashboardOfficesEntity->yesterday_niponno_note = $save_data['yesterday_niponno_note'];
                $DashboardOfficesEntity->yesterday_nisponno_potrojari = $save_data['yesterday_nisponno_potrojari'];
                $DashboardOfficesEntity->yesterday_potrojari = $save_data['yesterday_potrojari'];
                $DashboardOfficesEntity->yesterday_onisponno_note = $save_data['yesterday_onisponno_note'];

                $f = $dashboardOfficesTable->save($DashboardOfficesEntity);
                if ($f == FALSE) {
                    $data[] = [
                        'office_id' => $office_id,
                        'office_name' => $office_name,
                        'msg' => 'Can not update'
                    ];
                    continue;
                } else {
                    $save++;
                }
//                if ($save >= 500) {
//                    break;
//                }
            } catch (\Exception $ex) {
                $data[] = [
                    'office_id' => $office_id,
                    'office_name' => $office_name,
                    'msg' => $ex->getMessage()
                ];
                continue;
            }
        }
        if (!empty($data)) {
            $response = "Total office run/check during this process: " . $total;
            $end_time = strtotime(date('Y-m-d H:i:s'));
            $time_difference = round(abs(($end_time - $start_time) / 3600), 2);
            $response .= "<br>Duration of this process: " . $time_difference . " Hour";
            if (!empty($data)) {
                $response .= '<br>Error occurred - ' . count($data) . '<br> Error details:<br> <ul>';
                foreach ($data as $id) {
                    $response .= "<li>Office ID: " . $id['office_id'] . " Name: " . $id['office_name'] . " Error: " . $id['msg'] . "</li><br>";
                }
                $response .= "</ul> ";
            } else {
                $response .= "<br>Report generation process run successfully. No error occurred";
            }
            if (NOTIFICATION_OFF == 0) {
//                $this->sendCronMessage('tusharkaiser@gmail.com', [], 'Cron Report ' . date('Y-m-d'),
//                    'Office Dashboard Report', $response);
            }

            $this->saveCronLog("[Report] Office Dashboard [Date - " . date('Y-m-d') . "]", date('Y-m-d'), $start_time, $end_time, $total,
                $total - count($data), count($data), $response);
        }
    }

    private function cronForUnitDashboard()
    {
        ini_set('memory_limit', -1);
        set_time_limit(0);
        $yesterday = date('Y-m-d', strtotime(date('Y-m-d') . ' -1 day'));
        $start_time = strtotime(date('Y-m-d H:i:s'));
        TableRegistry::remove('EmployeeOffices');
        $employee_offices = TableRegistry::get('EmployeeOffices');
        $all_units = $employee_offices->getAllUnitID()->toArray();
        $data = [];
        $total = 0;
        $save = 0;
//            pr($all_offices);
        TableRegistry::remove('Dashboard');
        $DashboardTable = TableRegistry::get('Dashboard');
        TableRegistry::remove('DashboardUnits');
        $dashboardUnitsTable = TableRegistry::get('DashboardUnits');
        TableRegistry::get('OfficeUnits');
        $OfficeUnits = TableRegistry::get('OfficeUnits');
        $performance_unitTable = TableRegistry::get('PerformanceUnits');
        foreach ($all_units as $unit_id => $unit_name) {
            if (empty($unit_id)) {
                continue;
            }
            //re-initialize again to skip MySQl Server gone away error
            TableRegistry::remove('Dashboard');
            $DashboardTable = TableRegistry::get('Dashboard');
            TableRegistry::remove('DashboardUnits');
            $dashboardUnitsTable = TableRegistry::get('DashboardUnits');
            TableRegistry::get('OfficeUnits');
            $OfficeUnits = TableRegistry::get('OfficeUnits');
            $performance_unitTable = TableRegistry::get('PerformanceUnits');
            //re-initialize again to skip MySQl Server gone away error
            $total++;
            $f = $dashboardUnitsTable->checkUpdate($unit_id, date('Y-m-d'));
            if ($f > 0) {
                continue;
            }
            $unit_info = $OfficeUnits->get($unit_id);
            $office_id = $unit_info['office_id'];
            try {
                $last_update = $dashboardUnitsTable->getLastUpdateDate($unit_id);
                if (!empty($last_update['operation_date'])) {
                    $time = new Time($last_update['operation_date']);
                    $last_update['operation_date'] = $time->i18nFormat(null, null, 'en-US');
                }
                $dt = !empty($last_update['operation_date']) ? date('Y-m-d',
                    strtotime($last_update['operation_date'])) : '';

                // Collect From Performance Unit Table
                $condition = [
                    'totalinboxall' => 'SUM(inbox)', 'totaloutboxall' => 'SUM(outbox)', 'totalnothijatoall' => 'SUM(nothijat)',
                    'totalnothivuktoall' => 'SUM(nothivukto)', 'totalnisponnodakall' => 'SUM(nisponnodak)',
                    'totalsrijitonoteall' => 'SUM(selfnote)', 'totalOnisponnodakall' => 'SUM(onisponnodak)',
                    'totaldaksohonoteall' => 'SUM(daksohonote)', 'totalnisponnopotrojariall' => 'SUM(nisponnopotrojari)',
                    'totalnisponnonoteall' => 'SUM(nisponnonote)', 'totalpotrojariall' => 'SUM(potrojari)',
                    'totalOnisponnonoteall' => 'SUM(onisponnonote)',
                    'totalID' => 'count(id)'
                ];
                if (!empty($dt)) {
                    $hasDataInPerformanceUnit = $performance_unitTable->getUnitData($unit_id, [$dt, $yesterday])->select($condition)->group(['unit_id'])->first();
                }
                if (!empty($hasDataInPerformanceUnit['totalID']) && $hasDataInPerformanceUnit['totalID'] > 0) {
                    // get yesterday data
                    $performanceUnit_yesterday = $performance_unitTable->getUnitData($unit_id, [$yesterday, $yesterday])->select($condition)->group(['unit_id'])->first();
                    if (empty($performanceUnit_yesterday)) {
                        $this->switchOffice($office_id, 'DashboardOffice');
                        $dak_yesterday = $DashboardTable->OtherDashboardUpdateDak($office_id, $unit_id, 0,
                            $yesterday);
                        $nothi_yesterday = $DashboardTable->OtherDashboardUpdateNothi($office_id, $unit_id,
                            0, $yesterday);
                    }

                    $dak['totalinboxall'] = $hasDataInPerformanceUnit['totalinboxall'];
                    $dak['totaloutboxall'] = $hasDataInPerformanceUnit['totaloutboxall'];
                    $dak['totalnothivuktoall'] = $hasDataInPerformanceUnit['totalnothivuktoall'];
                    $dak['totalnothijatoall'] = $hasDataInPerformanceUnit['totalnothijatoall'];
                    $dak['totalnisponnodakall'] = $hasDataInPerformanceUnit['totalnisponnodakall'];
                    $dak['totalOnisponnodakall'] = (!empty($performanceUnit_yesterday)) ? $performanceUnit_yesterday['totalOnisponnodakall'] : $dak_yesterday['totalOnisponnodakall'];
                    $nothi['totalsrijitonoteall'] = $hasDataInPerformanceUnit['totalsrijitonoteall'];
                    $nothi['totaldaksohonoteall'] = $hasDataInPerformanceUnit['totaldaksohonoteall'];
                    $nothi['totalnisponnonoteall'] = $hasDataInPerformanceUnit['totalnisponnonoteall'];
                    $nothi['totalnisponnopotrojariall'] = $hasDataInPerformanceUnit['totalnisponnopotrojariall'];
                    $nothi['totalOnisponnonoteall'] = (!empty($performanceUnit_yesterday)) ? $performanceUnit_yesterday['totalOnisponnonoteall'] : $nothi_yesterday['totalOnisponnonoteall'];
                    $dak['totalpotrojariall'] = $hasDataInPerformanceUnit['totalpotrojariall'];

                    if (!empty($performanceUnit_yesterday)) {
                        $dak_yesterday['totalinboxall'] = $performanceUnit_yesterday['totalinboxall'];
                        $dak_yesterday['totaloutboxall'] = $performanceUnit_yesterday['totaloutboxall'];
                        $dak_yesterday['totalnothivuktoall'] = $performanceUnit_yesterday['totalnothivuktoall'];
                        $dak_yesterday['totalnothijatoall'] = $performanceUnit_yesterday['totalnothijatoall'];
                        $dak_yesterday['totalnisponnodakall'] = $performanceUnit_yesterday['totalnisponnodakall'];
                        $dak_yesterday['totalOnisponnodakall'] = $performanceUnit_yesterday['totalOnisponnodakall'];
                        $nothi_yesterday['totalsrijitonoteall'] = $performanceUnit_yesterday['totalsrijitonoteall'];
                        $nothi_yesterday['totaldaksohonoteall'] = $performanceUnit_yesterday['totaldaksohonoteall'];
                        $nothi_yesterday['totalnisponnonoteall'] = $performanceUnit_yesterday['totalnisponnonoteall'];
                        $nothi_yesterday['totalnisponnopotrojariall'] = $performanceUnit_yesterday['totalnisponnopotrojariall'];
                        $nothi_yesterday['totalOnisponnonoteall'] = $performanceUnit_yesterday['totalOnisponnonoteall'];
                        $dak_yesterday['totalpotrojariall'] = $performanceUnit_yesterday['totalpotrojariall'];
                    }
                } // Collect From Designation Table
                else {
                    $this->switchOffice($office_id, 'DashboardOffice');
                    $dak = $DashboardTable->OtherDashboardUpdateDak($office_id, $unit_id, 0,
                        $dt);
                    $dak_yesterday = $DashboardTable->OtherDashboardUpdateDak($office_id, $unit_id, 0,
                        $yesterday);
                    $nothi = $DashboardTable->OtherDashboardUpdateNothi($office_id, $unit_id,
                        0, $dt);
                    $nothi_yesterday = $DashboardTable->OtherDashboardUpdateNothi($office_id, $unit_id,
                        0, $yesterday);
                }

                if (!empty($dt)) {
                    $save_type = 'partial';
                } else {
                    $save_type = 'full';
                }

                if (empty($dak) || empty($nothi) || !isset($dak['totalinboxall']) || !isset($nothi['totalsrijitonoteall'])) {
                    $data[] = [
                        'office_id' => $office_id,
                        'unit_id' => $unit_id,
                        'unit_name' => $unit_name,
                        'msg' => 'Empty Data',
                    ];
                    continue;
                }
                if ($save_type == 'full') {
                    if ($dak['totalnothivuktoall'] < $nothi['totaldaksohonoteall']) {
                        $nothi['totaldaksohonoteall'] = $dak['totalnothivuktoall'];
                    }
                    // yesterday
                    if ($dak_yesterday['totalpotrojariall'] < $nothi_yesterday['totalnisponnopotrojariall']) {
                        $nothi_yesterday['totalnisponnopotrojariall'] = $dak_yesterday['totalpotrojariall'];
                    }
                    if ($dak_yesterday['totalnothivuktoall'] < $nothi_yesterday['totaldaksohonoteall']) {
                        $nothi_yesterday['totaldaksohonoteall'] = $dak_yesterday['totalnothivuktoall'];
                    }
                    $nothi_yesterday['totalOnisponnonoteall'] = $nothi['totalOnisponnonoteall'];
                    // yesterday
                    $nothi['totalsrijitonoteall'] = ($nothi['totalnisponnopotrojariall']
                        + $nothi['totalnisponnonoteall'] + $nothi['totalOnisponnonoteall'] - $nothi['totaldaksohonoteall']);
                    if ($nothi['totalsrijitonoteall'] < 0) {
                        $nothi['totalsrijitonoteall'] = 0;
                    }
                    if ($nothi_yesterday['totalsrijitonoteall'] < 0) {
                        $nothi_yesterday['totalsrijitonoteall'] = 0;
                    }
                    $save_data = [
                        'unit_id' => $unit_id, 'operation_date' => date('Y-m-d'),
                        'dak_inbox' => $dak['totalinboxall'], 'dak_outbox' => $dak['totaloutboxall'],
                        'nothivukto' => $dak['totalnothivuktoall'],
                        'nothijat' => $dak['totalnothijatoall'], 'dak_nisponno' => $dak['totalnisponnodakall'],
                        'dak_onisponno' => $dak['totalOnisponnodakall'],
                        'self_note' => $nothi['totalsrijitonoteall'], 'dak_note' => $nothi['totaldaksohonoteall'],
                        'note_nisponno' => $nothi['totalnisponnonoteall'],
                        'potrojari_nisponno' => $nothi['totalnisponnopotrojariall'],
                        'onisponno_note' => $nothi['totalOnisponnonoteall'],
                        'potrojari' => $dak['totalpotrojariall'],
                        //yesterday
                        'yesterday_inbox' => $dak_yesterday['totalinboxall'], 'yesterday_outbox' => $dak_yesterday['totaloutboxall'],
                        'yesterday_nothivukto' => $dak_yesterday['totalnothivuktoall'],
                        'yesterday_nothijat' => $dak_yesterday['totalnothijatoall'], 'yesterday_nisponno_dak' => $dak_yesterday['totalnisponnodakall'],
                        'yesterday_onisponno_dak' => $dak_yesterday['totalOnisponnodakall'],
                        'yesterday_self_note' => $nothi_yesterday['totalsrijitonoteall'], 'yesterday_dak_note' => $nothi_yesterday['totaldaksohonoteall'],
                        'yesterday_niponno_note' => $nothi_yesterday['totalnisponnonoteall'],
                        'yesterday_nisponno_potrojari' => $nothi_yesterday['totalnisponnopotrojariall'],
                        'yesterday_onisponno_note' => $nothi_yesterday['totalOnisponnonoteall'],
                        'yesterday_potrojari' => $dak_yesterday['totalpotrojariall'],
                    ];
                } else {
                    $last_data = $dashboardUnitsTable->getData($unit_id);
                    if ($dak['totalpotrojariall'] + $last_data['totalPotrojari'] < $nothi['totalnisponnopotrojariall']
                        + $last_data['totalNisponnoPotrojari']
                    ) {
                        $nothi['totalnisponnopotrojariall'] = $dak['totalpotrojariall'] + $last_data['totalPotrojari'];
                    } else {
                        $nothi['totalnisponnopotrojariall'] = $nothi['totalnisponnopotrojariall'] + $last_data['totalNisponnoPotrojari'];
                    }
                    if ($dak['totalnothivuktoall'] + $last_data['totalNothivukto'] < $nothi['totaldaksohonoteall']
                        + $last_data['totalDaksohoNote']
                    ) {
                        $nothi['totaldaksohonoteall'] = $dak['totalnothivuktoall'] + $last_data['totalNothivukto'];
                    } else {
                        $nothi['totaldaksohonoteall'] = $nothi['totaldaksohonoteall'] + $last_data['totalDaksohoNote'];
                    }
                    // yesterday
                    if ($dak_yesterday['totalpotrojariall'] < $nothi_yesterday['totalnisponnopotrojariall']) {
                        $nothi_yesterday['totalnisponnopotrojariall'] = $dak_yesterday['totalpotrojariall'];
                    }
                    if ($dak_yesterday['totalnothivuktoall'] < $nothi_yesterday['totaldaksohonoteall']) {
                        $nothi_yesterday['totaldaksohonoteall'] = $dak_yesterday['totalnothivuktoall'];
                    }
                    // yesterday
                    $nothi['totalOnisponnonoteall'] = $DashboardTable->getOnisponnoNoteCount($office_id,
                        $unit_id, 0, ['', date('Y-m-d', strtotime(date('Y-m-d') . ' -1 day'))]);
                    //yesterday
                    $nothi_yesterday['totalsrijitonoteall'] = ($nothi_yesterday['totalnisponnopotrojariall']
                        + $nothi_yesterday['totalnisponnonoteall'] + $nothi_yesterday['totalOnisponnonoteall']
                        - $nothi_yesterday['totaldaksohonoteall']);
                    $nothi_yesterday['totalOnisponnonoteall'] = $nothi['totalOnisponnonoteall'];
                    //yesterday
                    $save_data = [
                        'unit_id' => $unit_id, 'operation_date' => date('Y-m-d'),
                        'dak_inbox' => $dak['totalinboxall'] + $last_data['totalInbox'],
                        'dak_outbox' => $dak['totaloutboxall'] + $last_data['totalOutbox'],
                        'nothivukto' => $dak['totalnothivuktoall'] + $last_data['totalNothivukto'],
                        'nothijat' => $dak['totalnothijatoall'] + $last_data['totalNothijato'],
                        'dak_nisponno' => $dak['totalnisponnodakall'] + $last_data['totalNisponnoDak'],
                        'dak_onisponno' => $dak['totalOnisponnodakall'],
                        'self_note' => $nothi['totalsrijitonoteall'],
                        'dak_note' => $nothi['totaldaksohonoteall'],
                        'note_nisponno' => $nothi['totalnisponnonoteall'] + $last_data['totalNisponnoNote'],
                        'potrojari_nisponno' => $nothi['totalnisponnopotrojariall'],
                        'onisponno_note' => $nothi['totalOnisponnonoteall'],
                        'potrojari' => $dak['totalpotrojariall'] + $last_data['totalPotrojari'],
                        //yesterday
                        'yesterday_inbox' => $dak_yesterday['totalinboxall'], 'yesterday_outbox' => $dak_yesterday['totaloutboxall'],
                        'yesterday_nothivukto' => $dak_yesterday['totalnothivuktoall'],
                        'yesterday_nothijat' => $dak_yesterday['totalnothijatoall'], 'yesterday_nisponno_dak' => $dak_yesterday['totalnisponnodakall'],
                        'yesterday_onisponno_dak' => $dak_yesterday['totalOnisponnodakall'],
                        'yesterday_self_note' => $nothi_yesterday['totalsrijitonoteall'], 'yesterday_dak_note' => $nothi_yesterday['totaldaksohonoteall'],
                        'yesterday_niponno_note' => $nothi_yesterday['totalnisponnonoteall'],
                        'yesterday_nisponno_potrojari' => $nothi_yesterday['totalnisponnopotrojariall'],
                        'yesterday_onisponno_note' => $nothi_yesterday['totalOnisponnonoteall'],
                        'yesterday_potrojari' => $dak_yesterday['totalpotrojariall'],
                    ];
                }
                $dashboardUnitsTable->deleteAll(['unit_id' => $unit_id]);
                $DashboardUnitsEntity = $dashboardUnitsTable->newEntity();
                $DashboardUnitsEntity->unit_id = $save_data['unit_id'];
                $DashboardUnitsEntity->operation_date = $save_data['operation_date'];
                $DashboardUnitsEntity->dak_inbox = $save_data['dak_inbox'];
                $DashboardUnitsEntity->dak_outbox = $save_data['dak_outbox'];
                $DashboardUnitsEntity->nothivukto = $save_data['nothivukto'];
                $DashboardUnitsEntity->nothijat = $save_data['nothijat'];
                $DashboardUnitsEntity->dak_nisponno = $save_data['dak_nisponno'];
                $DashboardUnitsEntity->dak_onisponno = $save_data['dak_onisponno'];
                $DashboardUnitsEntity->self_note = $save_data['self_note'];
                $DashboardUnitsEntity->dak_note = $save_data['dak_note'];
                $DashboardUnitsEntity->potrojari_nisponno = $save_data['potrojari_nisponno'];
                $DashboardUnitsEntity->note_nisponno = $save_data['note_nisponno'];
                $DashboardUnitsEntity->onisponno_note = $save_data['onisponno_note'];
                $DashboardUnitsEntity->potrojari = $save_data['potrojari'];
                //yesterday
                $DashboardUnitsEntity->yesterday_inbox = $save_data['yesterday_inbox'];
                $DashboardUnitsEntity->yesterday_outbox = $save_data['yesterday_outbox'];
                $DashboardUnitsEntity->yesterday_nothivukto = $save_data['yesterday_nothivukto'];
                $DashboardUnitsEntity->yesterday_nothijat = $save_data['yesterday_nothijat'];
                $DashboardUnitsEntity->yesterday_nisponno_dak = $save_data['yesterday_nisponno_dak'];
                $DashboardUnitsEntity->yesterday_onisponno_dak = $save_data['yesterday_onisponno_dak'];
                $DashboardUnitsEntity->yesterday_self_note = $save_data['yesterday_self_note'];
                $DashboardUnitsEntity->yesterday_dak_note = $save_data['yesterday_dak_note'];
                $DashboardUnitsEntity->yesterday_niponno_note = $save_data['yesterday_niponno_note'];
                $DashboardUnitsEntity->yesterday_nisponno_potrojari = $save_data['yesterday_nisponno_potrojari'];
                $DashboardUnitsEntity->yesterday_potrojari = $save_data['yesterday_potrojari'];
                $DashboardUnitsEntity->yesterday_onisponno_note = $save_data['yesterday_onisponno_note'];

                $f = $dashboardUnitsTable->save($DashboardUnitsEntity);
                if ($f == FALSE) {
                    $data[] = [
                        'office_id' => $office_id,
                        'unit_id' => $unit_id,
                        'unit_name' => $unit_name,
                        'msg' => 'Can not update'
                    ];
                    continue;
                } else {
                    $save++;
                }
//                if ($save >= 3000) {
//                    break;
//                }
            } catch (\Exception $ex) {
                $data[] = [
                    'office_id' => $office_id,
                    'unit_id' => $unit_id,
                    'unit_name' => $unit_name,
                    'msg' => $ex->getMessage()
                ];
                continue;
            }
        }
        if (!empty($data)) {
            $response = "Total unit run/check during this process: " . $total;
            $end_time = strtotime(date('Y-m-d H:i:s'));
            $time_difference = round(abs(($end_time - $start_time) / 3600), 2);
            $response .= "<br>Duration of this process: " . $time_difference . " hour";
            if (!empty($data)) {
                $response .= '<br>Error occurred - ' . count($data) . '.<br> Error details:<br> <ul>';
                foreach ($data as $id) {
                    $response .= "<li>Office ID: " . $id['office_id'] . " Unit ID: " . $id['unit_id'] . " Name: " . $id['unit_name'] . " Error: " . $id['msg'] . "</li><br>";
                }
                $response .= "</ul> ";
            } else {
                $response .= "<br>Report generation process run successfully. No error occurred";
            }
            if (NOTIFICATION_OFF == 0) {
//                $res = $this->sendCronMessage('tusharkaiser@gmail.com', [],
//                    'Cron Report ' . date('Y-m-d'), 'Unit Dashboard Report', $response);
            }
            $this->saveCronLog("[Report] Unit Dashboard [Date - " . date('Y-m-d') . "]", date('Y-m-d'), $start_time, $end_time, $total,
                $total - count($data), count($data), $response);
        }

//                 saveData
    }

    private function cronForDesignationDashboard()
    {
        ini_set('memory_limit', -1);
        set_time_limit(0);
        $yesterday = date('Y-m-d', strtotime(date('Y-m-d') . ' -1 day'));
        $start_time = strtotime(date('Y-m-d H:i:s'));
        TableRegistry::remove('EmployeeOffices');
        $employeeOfficesTable = TableRegistry::get('EmployeeOffices');
        $all_designations = $employeeOfficesTable->getAllDesignationID()->toArray();
        $data = [];
        $total = 0;
        $save = 0;
//            pr($all_offices);
        $DashboardTable = TableRegistry::get('Dashboard');
        TableRegistry::remove('DashboardDesignations');
        $dashboardDesignationsTable = TableRegistry::get('DashboardDesignations');
        TableRegistry::remove('OfficeUnitOrganograms');
        $designationTable = TableRegistry::get('OfficeUnitOrganograms');
        $performance_designationTable = TableRegistry::get('PerformanceDesignations');
        foreach ($all_designations as $designation_id => $designation_name) {
            if (empty($designation_id)) {
                continue;
            }
            //re-initialize again to skip MySQl server gone away error
            TableRegistry::remove('Dashboard');
            $DashboardTable = TableRegistry::get('Dashboard');
            TableRegistry::remove('DashboardDesignations');
            $dashboardDesignationsTable = TableRegistry::get('DashboardDesignations');
            TableRegistry::remove('OfficeUnitOrganograms');
            $designationTable = TableRegistry::get('OfficeUnitOrganograms');
            $performance_designationTable = TableRegistry::get('PerformanceDesignations');
            //re-initialize again to skip MySQl server gone away error
            $total++;
            $f = $dashboardDesignationsTable->checkUpdate($designation_id, date('Y-m-d'));
            if ($f > 0) {
                continue;
            }
            $designation_info = $designationTable->get($designation_id);
            $office_id = $designation_info['office_id'];
            $unit_id = $designation_info['office_unit_id'];
            try {
                $last_update = $dashboardDesignationsTable->getLastUpdateDate($designation_id);
                //  date('Y-m-d',  strtotime("+1 day",strtotime($last_update['operation_date']) ) )
                if (!empty($last_update['operation_date'])) {
                    $time = new Time($last_update['operation_date']);
                    $last_update['operation_date'] = $time->i18nFormat(null, null, 'en-US');
                }
                $dt = !empty($last_update['operation_date']) ? date('Y-m-d',
                    strtotime($last_update['operation_date'])) : '';
                // Collect From Designation Table
                $condition = [
                    'totalinboxall' => 'SUM(inbox)', 'totaloutboxall' => 'SUM(outbox)', 'totalnothijatoall' => 'SUM(nothijat)',
                    'totalnothivuktoall' => 'SUM(nothivukto)', 'totalnisponnodakall' => 'SUM(nisponnodak)',
                    'totalsrijitonoteall' => 'SUM(selfnote)', 'totalOnisponnodakall' => 'SUM(onisponnodak)',
                    'totaldaksohonoteall' => 'SUM(daksohonote)', 'totalnisponnopotrojariall' => 'SUM(nisponnopotrojari)',
                    'totalnisponnonoteall' => 'SUM(nisponnonote)', 'totalpotrojariall' => 'SUM(potrojari)',
                    'totalOnisponnonoteall' => 'SUM(onisponnonote)',
                    'totalID' => 'count(id)'
                ];
                if (!empty($dt)) {
                    $hasDataInPerformanceDesignation = $performance_designationTable->getDesignationData($designation_id, [$dt, $yesterday])->select($condition)->group(['designation_id'])->first();
                }
                if (!empty($hasDataInPerformanceDesignation['totalID']) && $hasDataInPerformanceDesignation['totalID'] > 0) {
                    // get yesterday data
                    $performanceDesignation_yesterday = $performance_designationTable->getDesignationData($designation_id, [$yesterday, $yesterday])->select($condition)->group(['designation_id'])->first();
                    if (empty($performanceDesignation_yesterday)) {
                        $this->switchOffice($office_id, 'DashboardOffice');
                        $dak_yesterday = $DashboardTable->OtherDashboardUpdateDak($office_id, $unit_id,
                            $designation_id, $yesterday);
                        $nothi_yesterday = $DashboardTable->OtherDashboardUpdateNothi($office_id, $unit_id,
                            $designation_id, $yesterday);
                    }

                    $dak['totalinboxall'] = $hasDataInPerformanceDesignation['totalinboxall'];
                    $dak['totaloutboxall'] = $hasDataInPerformanceDesignation['totaloutboxall'];
                    $dak['totalnothivuktoall'] = $hasDataInPerformanceDesignation['totalnothivuktoall'];
                    $dak['totalnothijatoall'] = $hasDataInPerformanceDesignation['totalnothijatoall'];
                    $dak['totalnisponnodakall'] = $hasDataInPerformanceDesignation['totalnisponnodakall'];
                    $dak['totalOnisponnodakall'] = (!empty($performanceDesignation_yesterday)) ? $performanceDesignation_yesterday['totalOnisponnodakall'] : $dak_yesterday['totalOnisponnodakall'];
                    $nothi['totalsrijitonoteall'] = $hasDataInPerformanceDesignation['totalsrijitonoteall'];
                    $nothi['totaldaksohonoteall'] = $hasDataInPerformanceDesignation['totaldaksohonoteall'];
                    $nothi['totalnisponnonoteall'] = $hasDataInPerformanceDesignation['totalnisponnonoteall'];
                    $nothi['totalnisponnopotrojariall'] = $hasDataInPerformanceDesignation['totalnisponnopotrojariall'];
                    $nothi['totalOnisponnonoteall'] = (!empty($performanceDesignation_yesterday)) ? $performanceDesignation_yesterday['totalOnisponnonoteall'] : $nothi_yesterday['totalOnisponnonoteall'];
                    $dak['totalpotrojariall'] = $hasDataInPerformanceDesignation['totalpotrojariall'];

                    if (!empty($performanceDesignation_yesterday)) {
                        $dak_yesterday['totalinboxall'] = $performanceDesignation_yesterday['totalinboxall'];
                        $dak_yesterday['totaloutboxall'] = $performanceDesignation_yesterday['totaloutboxall'];
                        $dak_yesterday['totalnothivuktoall'] = $performanceDesignation_yesterday['totalnothivuktoall'];
                        $dak_yesterday['totalnothijatoall'] = $performanceDesignation_yesterday['totalnothijatoall'];
                        $dak_yesterday['totalnisponnodakall'] = $performanceDesignation_yesterday['totalnisponnodakall'];
                        $dak_yesterday['totalOnisponnodakall'] = $performanceDesignation_yesterday['totalOnisponnodakall'];
                        $nothi_yesterday['totalsrijitonoteall'] = $performanceDesignation_yesterday['totalsrijitonoteall'];
                        $nothi_yesterday['totaldaksohonoteall'] = $performanceDesignation_yesterday['totaldaksohonoteall'];
                        $nothi_yesterday['totalnisponnonoteall'] = $performanceDesignation_yesterday['totalnisponnonoteall'];
                        $nothi_yesterday['totalnisponnopotrojariall'] = $performanceDesignation_yesterday['totalnisponnopotrojariall'];
                        $nothi_yesterday['totalOnisponnonoteall'] = $performanceDesignation_yesterday['totalOnisponnonoteall'];
                        $dak_yesterday['totalpotrojariall'] = $performanceDesignation_yesterday['totalpotrojariall'];
                    }
                } // Collect From Designation Table
                else {
                    $this->switchOffice($office_id, 'DashboardOffice');
                    $dak = $DashboardTable->OtherDashboardUpdateDak($office_id, $unit_id,
                        $designation_id, $dt);
                    $dak_yesterday = $DashboardTable->OtherDashboardUpdateDak($office_id, $unit_id,
                        $designation_id, $yesterday);
                    $nothi = $DashboardTable->OtherDashboardUpdateNothi($office_id, $unit_id,
                        $designation_id, $dt);
                    $nothi_yesterday = $DashboardTable->OtherDashboardUpdateNothi($office_id, $unit_id,
                        $designation_id, $yesterday);
                }
                if (!empty($dt)) {
                    $save_type = 'partial';
                } else {
                    $save_type = 'full';
                }

                if (empty($dak) || empty($nothi) || !isset($dak['totalinboxall']) || !isset($nothi['totalsrijitonoteall'])) {
                    $data[] = [
                        'office_id' => $office_id,
                        'unit_id' => $unit_id,
                        'designation_id' => $designation_id,
                        'designation_name' => $designation_name,
                        'msg' => 'Empty Data',
                    ];
                    continue;
                }
                if ($save_type == 'full') {
                    if ($dak['totalnothivuktoall'] < $nothi['totaldaksohonoteall']) {
                        $nothi['totaldaksohonoteall'] = $dak['totalnothivuktoall'];
                    }
                    // yesterday
                    if ($dak_yesterday['totalpotrojariall'] < $nothi_yesterday['totalnisponnopotrojariall']) {
                        $nothi_yesterday['totalnisponnopotrojariall'] = $dak_yesterday['totalpotrojariall'];
                    }
                    if ($dak_yesterday['totalnothivuktoall'] < $nothi_yesterday['totaldaksohonoteall']) {
                        $nothi_yesterday['totaldaksohonoteall'] = $dak_yesterday['totalnothivuktoall'];
                    }
                    $nothi_yesterday['totalOnisponnonoteall'] = $nothi['totalOnisponnonoteall'];
                    // yesterday
                    $nothi['totalsrijitonoteall'] = ($nothi['totalnisponnopotrojariall']
                        + $nothi['totalnisponnonoteall'] + $nothi['totalOnisponnonoteall'] - $nothi['totaldaksohonoteall']);
                    if ($nothi['totalsrijitonoteall'] < 0) {
                        $nothi['totalsrijitonoteall'] = 0;
                    }
                    if ($nothi_yesterday['totalsrijitonoteall'] < 0) {
                        $nothi_yesterday['totalsrijitonoteall'] = 0;
                    }
                    $save_data = [
                        'designation_id' => $designation_id, 'operation_date' => date('Y-m-d'),
                        'dak_inbox' => $dak['totalinboxall'], 'dak_outbox' => $dak['totaloutboxall'],
                        'nothivukto' => $dak['totalnothivuktoall'],
                        'nothijat' => $dak['totalnothijatoall'], 'dak_nisponno' => $dak['totalnisponnodakall'],
                        'dak_onisponno' => $dak['totalOnisponnodakall'],
                        'self_note' => $nothi['totalsrijitonoteall'], 'dak_note' => $nothi['totaldaksohonoteall'],
                        'note_nisponno' => $nothi['totalnisponnonoteall'],
                        'potrojari_nisponno' => $nothi['totalnisponnopotrojariall'],
                        'onisponno_note' => $nothi['totalOnisponnonoteall'],
                        'potrojari' => $dak['totalpotrojariall'],
                        //yesterday
                        'yesterday_inbox' => $dak_yesterday['totalinboxall'], 'yesterday_outbox' => $dak_yesterday['totaloutboxall'],
                        'yesterday_nothivukto' => $dak_yesterday['totalnothivuktoall'],
                        'yesterday_nothijat' => $dak_yesterday['totalnothijatoall'], 'yesterday_nisponno_dak' => $dak_yesterday['totalnisponnodakall'],
                        'yesterday_onisponno_dak' => $dak_yesterday['totalOnisponnodakall'],
                        'yesterday_self_note' => $nothi_yesterday['totalsrijitonoteall'], 'yesterday_dak_note' => $nothi_yesterday['totaldaksohonoteall'],
                        'yesterday_niponno_note' => $nothi_yesterday['totalnisponnonoteall'],
                        'yesterday_nisponno_potrojari' => $nothi_yesterday['totalnisponnopotrojariall'],
                        'yesterday_onisponno_note' => $nothi_yesterday['totalOnisponnonoteall'],
                        'yesterday_potrojari' => $dak_yesterday['totalpotrojariall'],
                    ];
                } else {
                    $last_data = $dashboardDesignationsTable->getData($designation_id);
                    if ($dak['totalpotrojariall'] + $last_data['totalPotrojari'] < $nothi['totalnisponnopotrojariall']
                        + $last_data['totalNisponnoPotrojari']
                    ) {
                        $nothi['totalnisponnopotrojariall'] = $dak['totalpotrojariall'] + $last_data['totalPotrojari'];
                    } else {
                        $nothi['totalnisponnopotrojariall'] = $nothi['totalnisponnopotrojariall'] + $last_data['totalNisponnoPotrojari'];
                    }
                    if ($dak['totalnothivuktoall'] + $last_data['totalNothivukto'] < $nothi['totaldaksohonoteall']
                        + $last_data['totalDaksohoNote']
                    ) {
                        $nothi['totaldaksohonoteall'] = $dak['totalnothivuktoall'] + $last_data['totalNothivukto'];
                    } else {
                        $nothi['totaldaksohonoteall'] = $nothi['totaldaksohonoteall'] + $last_data['totalDaksohoNote'];
                    }
                    // yesterday
                    if ($dak_yesterday['totalpotrojariall'] < $nothi_yesterday['totalnisponnopotrojariall']) {
                        $nothi_yesterday['totalnisponnopotrojariall'] = $dak_yesterday['totalpotrojariall'];
                    }
                    if ($dak_yesterday['totalnothivuktoall'] < $nothi_yesterday['totaldaksohonoteall']) {
                        $nothi_yesterday['totaldaksohonoteall'] = $dak_yesterday['totalnothivuktoall'];
                    }
                    // yesterday
                    $nothi['totalOnisponnonoteall'] = $DashboardTable->getOnisponnoNoteCount($office_id,
                        $unit_id, $designation_id,
                        ['', date('Y-m-d', strtotime(date('Y-m-d') . ' -1 day'))]);
                    $nothi['totalsrijitonoteall'] = ($nothi['totalnisponnopotrojariall']
                        + $nothi['totalnisponnonoteall'] + $last_data['totalNisponnoNote'] + $nothi['totalOnisponnonoteall']
                        - $nothi['totaldaksohonoteall']);
                    //yesterday
                    $nothi_yesterday['totalOnisponnonoteall'] = $nothi['totalOnisponnonoteall'];
                    //yesterday
                    $save_data = [
                        'designation_id' => $designation_id, 'operation_date' => date('Y-m-d'),
                        'dak_inbox' => $dak['totalinboxall'] + $last_data['totalInbox'],
                        'dak_outbox' => $dak['totaloutboxall'] + $last_data['totalOutbox'],
                        'nothivukto' => $dak['totalnothivuktoall'] + $last_data['totalNothivukto'],
                        'nothijat' => $dak['totalnothijatoall'] + $last_data['totalNothijato'],
                        'dak_nisponno' => $dak['totalnisponnodakall'] + $last_data['totalNisponnoDak'],
                        'dak_onisponno' => $dak['totalOnisponnodakall'],
                        'self_note' => $nothi['totalsrijitonoteall'],
                        'dak_note' => $nothi['totaldaksohonoteall'],
                        'note_nisponno' => $nothi['totalnisponnonoteall'] + $last_data['totalNisponnoNote'],
                        'potrojari_nisponno' => $nothi['totalnisponnopotrojariall'],
                        'onisponno_note' => $nothi['totalOnisponnonoteall'],
                        'potrojari' => $dak['totalpotrojariall'] + $last_data['totalPotrojari'],
                        //yesterday
                        'yesterday_inbox' => $dak_yesterday['totalinboxall'], 'yesterday_outbox' => $dak_yesterday['totaloutboxall'],
                        'yesterday_nothivukto' => $dak_yesterday['totalnothivuktoall'],
                        'yesterday_nothijat' => $dak_yesterday['totalnothijatoall'], 'yesterday_nisponno_dak' => $dak_yesterday['totalnisponnodakall'],
                        'yesterday_onisponno_dak' => $dak_yesterday['totalOnisponnodakall'],
                        'yesterday_self_note' => $nothi_yesterday['totalsrijitonoteall'], 'yesterday_dak_note' => $nothi_yesterday['totaldaksohonoteall'],
                        'yesterday_niponno_note' => $nothi_yesterday['totalnisponnonoteall'],
                        'yesterday_nisponno_potrojari' => $nothi_yesterday['totalnisponnopotrojariall'],
                        'yesterday_onisponno_note' => $nothi_yesterday['totalOnisponnonoteall'],
                        'yesterday_potrojari' => $dak_yesterday['totalpotrojariall'],
                    ];
                }
                $dashboardDesignationsTable->deleteAll(['designation_id' => $designation_id]);
                $DashboardDesignationsEntity = $dashboardDesignationsTable->newEntity();
                $DashboardDesignationsEntity->designation_id = $designation_id;
                $DashboardDesignationsEntity->operation_date = $save_data['operation_date'];
                $DashboardDesignationsEntity->dak_inbox = $save_data['dak_inbox'];
                $DashboardDesignationsEntity->dak_outbox = $save_data['dak_outbox'];
                $DashboardDesignationsEntity->nothivukto = $save_data['nothivukto'];
                $DashboardDesignationsEntity->nothijat = $save_data['nothijat'];
                $DashboardDesignationsEntity->dak_nisponno = $save_data['dak_nisponno'];
                $DashboardDesignationsEntity->dak_onisponno = $save_data['dak_onisponno'];
                $DashboardDesignationsEntity->self_note = $save_data['self_note'];
                $DashboardDesignationsEntity->dak_note = $save_data['dak_note'];
                $DashboardDesignationsEntity->potrojari_nisponno = $save_data['potrojari_nisponno'];
                $DashboardDesignationsEntity->note_nisponno = $save_data['note_nisponno'];
                $DashboardDesignationsEntity->onisponno_note = $save_data['onisponno_note'];
                $DashboardDesignationsEntity->potrojari = $save_data['potrojari'];
                //yesterday
                $DashboardDesignationsEntity->yesterday_inbox = $save_data['yesterday_inbox'];
                $DashboardDesignationsEntity->yesterday_outbox = $save_data['yesterday_outbox'];
                $DashboardDesignationsEntity->yesterday_nothivukto = $save_data['yesterday_nothivukto'];
                $DashboardDesignationsEntity->yesterday_nothijat = $save_data['yesterday_nothijat'];
                $DashboardDesignationsEntity->yesterday_nisponno_dak = $save_data['yesterday_nisponno_dak'];
                $DashboardDesignationsEntity->yesterday_onisponno_dak = $save_data['yesterday_onisponno_dak'];
                $DashboardDesignationsEntity->yesterday_self_note = $save_data['yesterday_self_note'];
                $DashboardDesignationsEntity->yesterday_dak_note = $save_data['yesterday_dak_note'];
                $DashboardDesignationsEntity->yesterday_niponno_note = $save_data['yesterday_niponno_note'];
                $DashboardDesignationsEntity->yesterday_nisponno_potrojari = $save_data['yesterday_nisponno_potrojari'];
                $DashboardDesignationsEntity->yesterday_potrojari = $save_data['yesterday_potrojari'];
                $DashboardDesignationsEntity->yesterday_onisponno_note = $save_data['yesterday_onisponno_note'];

                $f = $dashboardDesignationsTable->save($DashboardDesignationsEntity);
                if ($f == FALSE) {
                    $data[] = [
                        'office_id' => $office_id,
                        'unit_id' => $unit_id,
                        'designation_id' => $designation_id,
                        'designation_name' => $designation_name,
                        'msg' => 'Can not update'
                    ];
                    continue;
                } else {
                    $save++;
                }
//                if ($save >= 10000) {
//                    break;
//                }
            } catch (\Exception $ex) {
                $data[] = [
                    'office_id' => $office_id,
                    'unit_id' => $unit_id,
                    'designation_id' => $designation_id,
                    'designation_name' => $designation_name,
                    'msg' => $ex->getMessage()
                ];
                continue;
            }
        }
        if (!empty($data)) {
            $response = "Total designation run/check during this process: " . $total;
            $end_time = strtotime(date('Y-m-d H:i:s'));
            $time_difference = round(abs(($end_time - $start_time) / 3600), 2);
            $response .= "<br>Duration of this process: " . $time_difference . " hour";
            if (!empty($data)) {
                $response .= '<br>Error occurred - ' . count($data) . '.<br> Error details:<br> <ul>';
                foreach ($data as $id) {
                    $response .= "<li>Office ID: " . $id['office_id'] . " Unit ID: " . $id['unit_id'] . " Designation ID: " . $id['designation_id'] . " Name: " . $id['designation_name'] . " Error: " . $id['msg'] . "</li><br>";
                }
                $response .= "</ul> ";
            } else {
                $response .= "<br>Report generation process run successfully. No error occurred";
            }
            if (NOTIFICATION_OFF == 0) {
//                $this->sendCronMessage('tusharkaiser@gmail.com', [], 'Cron Report ' . date('Y-m-d'),
//                    'Designation Dashboard Report', $response);
            }
            $this->saveCronLog("[Report] Designation dashboard [Date - " . date('Y-m-d') . "]", date('Y-m-d'), $start_time, $end_time,
                $total, $total - count($data), count($data), $response);
//                 saveData
        }
    }
    public function monitorIndividualOfficeUpdate($office_id = 0){
        set_time_limit(0);
        ini_set('memory_limit', -1);

        if(empty($office_id)){
            return;
        }

        $MonitorOfficesTable = TableRegistry::get('MonitorOffices');
        $officesTable = TableRegistry::get('Offices');

        $all_offices = $officesTable->find('list')->where(['active_status' => 1,'id' => $office_id])->toArray();
        $today = date('Y-m-d');
        $yesterday = date('Y-m-d', strtotime($today . ' -1 day'));

        foreach ($all_offices as $office_id => $office_val) {
            try {
                if (!empty($office_id)) {
                    $result = '';
                    $function = '';
                    $office_record_today = $MonitorOfficesTable->checkUpdate($office_id, $today);
                    if ($office_record_today > 0) {
                        $record = $MonitorOfficesTable->getLastUpdateTime($office_id);
                        $cur_time = strtotime(date('Y-m-d H:i:s'));
                        $record_time = strtotime($record['modified']->format("Y-m-d H:i:s"));
                        $time_difference = round(abs($record_time - $cur_time) / 60, 2);
                        if ($time_difference > 180) {
                            $query = $MonitorOfficesTable->deleteDataofOffices($office_id,
                                $today);
                            $office_record_today = 0;
                        } else {
//                            $this->out('already Updated');
                            return;
                        }
                    }
                    if ($office_record_today < 1) {
                        $office_record_yesterday = $MonitorOfficesTable->checkUpdate($office_id,
                            $yesterday);
                        if ($office_record_yesterday < 1) {
                            $MonitorOfficesTable->deleteDataofOffices($office_id);
                            $result = $this->reportMonitorDashboardSummary($office_id);
                            $function = 'report';
                        } else {
                            $result = $this->updateMonitorDashboardSummary($office_id);
                            $function = 'update';
                        }
                    }
                    if (!empty($result)) {
                        if ($result['Status'] == 0) {
                            $this->out('Error: '. $result['msg']);
                        }
                        if (!empty($function) && $function == 'report') {
                            if ($result['totalyesterdaysrijitonote'] < 0) {
                                $result['totalyesterdaysrijitonote'] = 0;
                            }
                            $monitorOfficesEntity = $MonitorOfficesTable->newEntity();
                            $monitorOfficesEntity->office_id = $office_id;
                            $monitorOfficesEntity->yesterdayinbox = isset($result['totalyesterdayinbox'])?$result['totalyesterdayinbox']:0;
                            $monitorOfficesEntity->yesterdayoutbox = isset($result['totalyesterdayoutbox'])?$result['totalyesterdayoutbox']:0;
                            $monitorOfficesEntity->yesterdaynothijat = isset($result['totalyesterdaynothijato'])?$result['totalyesterdaynothijato']:0;
                            $monitorOfficesEntity->yesterdaynothivukto = isset($result['totalyesterdaynothivukto'])?$result['totalyesterdaynothivukto']:0;
                            $monitorOfficesEntity->totalnisponnodak = isset($result['totalnisponnodakall'])?$result['totalnisponnodakall']:0;
                            $monitorOfficesEntity->totalonisponnodak = isset($result['totalOnisponnodakall'])?$result['totalOnisponnodakall']:0;
                            $monitorOfficesEntity->yesterdayselfnote = isset($result['totalyesterdaysrijitonote'])?$result['totalyesterdaysrijitonote']:0;
                            $monitorOfficesEntity->yesterdaydaksohonote = isset($result['totalyesterdaydaksohonote'])?$result['totalyesterdaydaksohonote']:0;
                            $monitorOfficesEntity->yesterdaypotrojari = isset($result['totalyesterdaypotrojari'])?$result['totalyesterdaypotrojari']:0;
                            $monitorOfficesEntity->totalnisponnonote = isset($result['totalnisponnonoteall'])?$result['totalnisponnonoteall']:0;
                            $monitorOfficesEntity->totalonisponnonote = isset($result['totalOnisponnonoteall'])?$result['totalOnisponnonoteall']:0;
                            $monitorOfficesEntity->totalnisponno = isset($result['totalnisponnoall'])?$result['totalnisponnoall']:0;
                            $monitorOfficesEntity->totalonisponno = isset($result['totalOnisponnoall'])?$result['totalOnisponnoall']:0;
                            $monitorOfficesEntity->status = $result['Status'];
                            $monitorOfficesEntity->updated = date('Y-m-d');

                            if ($MonitorOfficesTable->save($monitorOfficesEntity)) {
//                                $this->out('Full');
                            }
                        } elseif (!empty($function) && $function == 'update') {
                            $office_record_yesterday = $MonitorOfficesTable->getOfficeData($office_id,
                                [$yesterday, $yesterday])->order(['date(modified) desc'])->first();
                            if ($result['totalyesterdaysrijitonote'] < 0) {
                                $result['totalyesterdaysrijitonote'] = 0;
                            }
                            $monitorOfficesEntity = $MonitorOfficesTable->newEntity();
                            $monitorOfficesEntity->office_id = $office_id;
                            $monitorOfficesEntity->yesterdayinbox = isset($result['totalyesterdayinbox'])?$result['totalyesterdayinbox']:0;
                            $monitorOfficesEntity->yesterdayoutbox = isset($result['totalyesterdayoutbox'])?$result['totalyesterdayoutbox']:0;
                            $monitorOfficesEntity->yesterdaynothijat = isset($result['totalyesterdaynothijato'])?$result['totalyesterdaynothijato']:0;
                            $monitorOfficesEntity->yesterdaynothivukto = isset($result['totalyesterdaynothivukto'])?$result['totalyesterdaynothivukto']:0;
                            $monitorOfficesEntity->totalnisponnodak = /* $office_record_yesterday['totalnisponnodak']
                                  + */
                                $result['totaltodaynisponnodak'];
                            $monitorOfficesEntity->totalonisponnodak = isset($result['totaltodayOnisponnodak'])?$result['totaltodayOnisponnodak']:0;

                            $monitorOfficesEntity->yesterdayselfnote = $result['totalyesterdaysrijitonote']
                                + ($result['totalyesterdaynothivukto'] < $result['totalyesterdaydaksohonote']
                                    ? $result['totalyesterdaydaksohonote'] - $result['totalyesterdaynothivukto']
                                    : 0);
                            $monitorOfficesEntity->yesterdaydaksohonote = ($result['totalyesterdaynothivukto']
                            < $result['totalyesterdaydaksohonote'] ? $result['totalyesterdaynothivukto']
                                : $result['totalyesterdaydaksohonote']);
                            $monitorOfficesEntity->yesterdaypotrojari = isset($result['totalyesterdaypotrojari'])?$result['totalyesterdaypotrojari']:0;
                            $monitorOfficesEntity->totalnisponnonote = /* $office_record_yesterday['totalnisponnonote']  + */
                                isset($result['totaltodaynisponnonote'])?$result['totaltodaynisponnonote']:0;
                            $monitorOfficesEntity->totalonisponnonote = isset($result['totaltodayOnisponnonote'])?$result['totaltodayOnisponnonote']:0;
                            $monitorOfficesEntity->totalnisponno = /* $office_record_yesterday['totalnisponno'] + */
                                isset($result['totaltodaynisponno'])?$result['totaltodaynisponno']:0;
                            $monitorOfficesEntity->totalonisponno = $result['totaltodayOnisponnonote'] +
                                $result['totaltodayOnisponnodak'];
                            $monitorOfficesEntity->status = $result['Status'];
                            $monitorOfficesEntity->updated = date('Y-m-d');
                            if ($MonitorOfficesTable->save($monitorOfficesEntity)) {
//                                $this->out('Partial');
                            }
                        }
                    }
                } else {
//                $data['error']++;
//                $data['error_ids'] .=' '.$office_id;
                }
            } catch (\Exception $ex) {
                $this->out('Error: '.$ex->getMessage());
            }
        }
    }
    public function monitorCronWithPagination($offset_page = 1, $limit = 1000){
        set_time_limit(0);
        ini_set('memory_limit', -1);

        $MonitorOfficesTable = TableRegistry::get('MonitorOffices');
        $officesTable = TableRegistry::get('Offices');

        $all_offices = $officesTable->find('list')->where(['active_status' => 1])->page($offset_page,$limit)->toArray();
        $today = date('Y-m-d');
        $yesterday = date('Y-m-d', strtotime($today . ' -1 day'));
        $function = '';
        $start_time = strtotime(date('Y-m-d H:i:s'));
        $total = 0;
        $data = [
            'partial' => 0,
            'error' => 0,
            'already' => 0,
            'full' => 0,
            'error_ids' => ''
        ];


        foreach ($all_offices as $office_id => $office_val) {
            $total++;
            try {
                if (!empty($office_id)) {
                    $result = '';
                    $function = '';
                    $office_record_today = $MonitorOfficesTable->checkUpdate($office_id, $today);
                    if ($office_record_today > 0) {
                        $record = $MonitorOfficesTable->getLastUpdateTime($office_id);
                        $cur_time = strtotime(date('Y-m-d H:i:s'));
                        $record_time = strtotime($record['modified']->format("Y-m-d H:i:s"));
                        $time_difference = round(abs($record_time - $cur_time) / 60, 2);
                        if ($time_difference > 360) {
                            $query = $MonitorOfficesTable->deleteDataofOffices($office_id,
                                $today);
                            $office_record_today = 0;
                        } else {
                            $data['already']++;
                        }
                    }
                    if ($office_record_today < 1) {
                        $office_record_yesterday = $MonitorOfficesTable->checkUpdate($office_id,
                            $yesterday);
                        if ($office_record_yesterday < 1) {
                            $MonitorOfficesTable->deleteDataofOffices($office_id);
                            $result = $this->reportMonitorDashboardSummary($office_id);
                            $function = 'report';
                        } else {
                            $result = $this->updateMonitorDashboardSummary($office_id);
                            $function = 'update';
                        }
                    }
                    if (!empty($result)) {
                        if ($result['Status'] == 0) {
                            $data['error']++;
                            $data['error_ids'] .= '<li> ' . $office_id . ' (' . $office_val . ')' . (!empty($result['msg'])
                                    ? ' Error: ' . $result['msg'] : '') . '</li>';
                        }
                        if (!empty($function) && $function == 'report') {
                            if ($result['totalyesterdaysrijitonote'] < 0) {
                                $result['totalyesterdaysrijitonote'] = 0;
                            }
                            $monitorOfficesEntity = $MonitorOfficesTable->newEntity();
                            $monitorOfficesEntity->office_id = $office_id;
                            $monitorOfficesEntity->yesterdayinbox = $result['totalyesterdayinbox'];
                            $monitorOfficesEntity->yesterdayoutbox = $result['totalyesterdayoutbox'];
                            $monitorOfficesEntity->yesterdaynothijat = $result['totalyesterdaynothijato'];
                            $monitorOfficesEntity->yesterdaynothivukto = $result['totalyesterdaynothivukto'];
                            $monitorOfficesEntity->totalnisponnodak = $result['totalnisponnodakall'];
                            $monitorOfficesEntity->totalonisponnodak = $result['totalOnisponnodakall'];
                            $monitorOfficesEntity->yesterdayselfnote = $result['totalyesterdaysrijitonote'];
                            $monitorOfficesEntity->yesterdaydaksohonote = $result['totalyesterdaydaksohonote'];
                            $monitorOfficesEntity->yesterdaypotrojari = $result['totalyesterdaypotrojari'];
                            $monitorOfficesEntity->totalnisponnonote = $result['totalnisponnonoteall'];
                            $monitorOfficesEntity->totalonisponnonote = $result['totalOnisponnonoteall'];
                            $monitorOfficesEntity->totalnisponno = $result['totalnisponnoall'];
                            $monitorOfficesEntity->totalonisponno = $result['totalOnisponnoall'];
                            $monitorOfficesEntity->status = $result['Status'];
                            $monitorOfficesEntity->updated = date('Y-m-d');

                            if ($MonitorOfficesTable->save($monitorOfficesEntity)) {
                                $data['full']++;
                            }
                        } elseif (!empty($function) && $function == 'update') {
                            $office_record_yesterday = $MonitorOfficesTable->getOfficeData($office_id,
                                [$yesterday, $yesterday])->order(['date(modified) desc'])->first();
                            if ($result['totalyesterdaysrijitonote'] < 0) {
                                $result['totalyesterdaysrijitonote'] = 0;
                            }
                            $monitorOfficesEntity = $MonitorOfficesTable->newEntity();
                            $monitorOfficesEntity->office_id = $office_id;
                            $monitorOfficesEntity->yesterdayinbox = $result['totalyesterdayinbox'];
                            $monitorOfficesEntity->yesterdayoutbox = $result['totalyesterdayoutbox'];
                            $monitorOfficesEntity->yesterdaynothijat = $result['totalyesterdaynothijato'];
                            $monitorOfficesEntity->yesterdaynothivukto = $result['totalyesterdaynothivukto'];
                            $monitorOfficesEntity->totalnisponnodak = /* $office_record_yesterday['totalnisponnodak']
                                  + */
                                $result['totaltodaynisponnodak'];
                            $monitorOfficesEntity->totalonisponnodak = $result['totaltodayOnisponnodak'];

                            $monitorOfficesEntity->yesterdayselfnote = $result['totalyesterdaysrijitonote']
                                + ($result['totalyesterdaynothivukto'] < $result['totalyesterdaydaksohonote']
                                    ? $result['totalyesterdaydaksohonote'] - $result['totalyesterdaynothivukto']
                                    : 0);
                            $monitorOfficesEntity->yesterdaydaksohonote = ($result['totalyesterdaynothivukto']
                            < $result['totalyesterdaydaksohonote'] ? $result['totalyesterdaynothivukto']
                                : $result['totalyesterdaydaksohonote']);
                            $monitorOfficesEntity->yesterdaypotrojari = $result['totalyesterdaypotrojari'];
                            $monitorOfficesEntity->totalnisponnonote = /* $office_record_yesterday['totalnisponnonote']  + */
                                $result['totaltodaynisponnonote'];
                            $monitorOfficesEntity->totalonisponnonote = $result['totaltodayOnisponnonote'];
                            $monitorOfficesEntity->totalnisponno = /* $office_record_yesterday['totalnisponno'] + */
                                $result['totaltodaynisponno'];
                            $monitorOfficesEntity->totalonisponno = $result['totaltodayOnisponnonote'] +
                                $result['totaltodayOnisponnodak'];
                            $monitorOfficesEntity->status = $result['Status'];
                            $monitorOfficesEntity->updated = date('Y-m-d');
                            if ($MonitorOfficesTable->save($monitorOfficesEntity)) {
                                $data['partial']++;
                            }
                        }
                    }
                } else {
//                $data['error']++;
//                $data['error_ids'] .=' '.$office_id;
                }
            } catch (\Exception $ex) {
                $data['error']++;
                $data['error_ids'] .= '<li> ' . $office_id . ' (' . $office_val . ') Error: ' . $ex->getMessage() . '</li>';
            }

        }

        $response = "<br>Total office run/check during this process: " . $total;
        $end_time = strtotime(date('Y-m-d H:i:s'));
        $time_difference = round(abs(($end_time - $start_time) / 3600), 2);
        $response .= "<br>Duration of this process: " . $time_difference . " hour";
        if (!empty($data['error'])) {
            $response .= '<br>Error occurred: ' . ($data['error']) . '.<br> Error details:<br>';
            $response .= "<ul> " . $data['error_ids'] . "</ul>";
        } else {
            $response .= "<br><br><b>Report generation process run successfully. No error occurred</b><br>";
        }
        $start_limit = (($offset_page - 1)*$limit) + 1;
        $end_limit = $start_limit + $limit;
        if (NOTIFICATION_OFF == 0) {
            $params = ['to_email' => 'mhasan.a2i@gmail.com', 'to_cc' => 'nothi.team@gmail.com', 'to_bcc' => 'tusharkaiser@gmail.com', 'subject' => "[Report] Monitor Dashboard [Date - " . date('Y-m-d') . "][Limit: " . $start_limit ." - ". $end_limit . "]", 'email_body' => "Dear Concern,<br> Monitor dashboard  report [Date - " . date('Y-m-d') . "] is ready. Detail of this process has been given below.<br>" . $response . "<br>Thanks,<br>Nothi Team"];
            $this->sendEmailByEmailer($params);
        }
        $this->saveCronLog("[Report] Monitor Dashboard [Date - " . date('Y-m-d') . "][Limit: " . ($start_limit) ." - ". $end_limit . "]", date('Y-m-d'), $start_time, $end_time, $total,
            $total - $data['error'], $data['error'], $response);
        $this->runCronRequest();
    }
    private function monitorCron()
    {
        $officesTable = TableRegistry::get('Offices');
        $all_offices_count = $officesTable->find('list')->where(['active_status' => 1])->count();
        $tbl_CronRequest = TableRegistry::get('CronRequest');
        $limit = 1000;
        if($all_offices_count > 0){
            $number_of_times_need_2_run = ceil($all_offices_count / $limit);
            if(!empty($number_of_times_need_2_run)){
                for ($index =1;$index <= $number_of_times_need_2_run;$index++){
                    sleep(60);
                    $command = ROOT . "/bin/cake cronjob monitorCronWithPagination {$index} {$limit} > /dev/null 2>&1 &";
                    $tbl_CronRequest->saveData($command);
                }
            }
        }
//        $this->out('Done'.(isset($all_offices_count)?(' Total Request: '.$all_offices_count):''));
        $this->runCronRequest();
    }

    public function decideCronRequest()
    {
        if (Live != 1) {
            return;
        }
        if (NOTIFICATION_OFF == 0) {
            $getCountOfCronRequest = TableRegistry::get('CronRequest')->find()->count();
            if ($getCountOfCronRequest > 0) {
                $this->sendCronMessage('tusharkaiser@gmail.com', [], 'Cron Request Status ' . date('Y-m-d'),
                    'Cron Request Status', 'Generated at: ' . date('Y-m-d H:i:s') . '. Cron request remain: ' . $getCountOfCronRequest);
            }
        }
        $time = (date('G'));
        $this->runCronRequest();

        if (intval($time) >= 0 && intval($time) < 1) {
            $this->clearAllCache();
            TableRegistry::get('CronRequest')->deleteAll(['1 = 1']); 
            $this->runThisCommand(ROOT . "/bin/cake cronjob saveUserLogin > /dev/null 2>&1 &");
            $this->runThisCommand(ROOT . "/bin/cake cronjob OfficeWisePerformanceTablesUpdate > /dev/null 2>&1 &");
        } else if (intval($time) >= 1 && intval($time) < 2) {
            $this->runThisCommand(ROOT . "/bin/cake API apiProtikolpoTransferByStart > /dev/null 2>&1 &");
        } else if (intval($time) >= 5 && intval($time) < 6) {
            $this->runThisCommand(ROOT . "/bin/cake API apiProtikolpoTransferByStart > /dev/null 2>&1 &");
        } else if (intval($time) >= 6 && intval($time) < 7) {
            $this->performanceDesignationUpdate();
            $this->cronForDesignationDashboard();
        } else if (intval($time) >= 7 && intval($time) < 8) {
            $this->runThisCommand(ROOT . "/bin/cake notification getActiveDomains");
        } else if (intval($time) >= 8 && intval($time) < 9) {
            $this->runThisCommand(ROOT . "/bin/cake notification mailExcelDomainReport");
            $this->runThisCommand(ROOT . "/bin/cake archiving archiveAllNothi");
            // report checker for 16 and 1
            if(date('d') == '16'){
                $this->runThisCommand(ROOT . "/bin/cake report findMissingReportsOfOffices '" . date('Y-m-d', strtotime('first day of this month')) . "' '" . date('Y-m-d', strtotime(date('Y-m-d') . ' -1 day')) . "' > /dev/null 2>&1 &");
            }else if(date('d') == '01' || date('d') == '1' ){
                $this->runThisCommand(ROOT . "/bin/cake report findMissingReportsOfOffices '" . date('Y-m-d', strtotime('first day of last month')) . "' '" . date('Y-m-d', strtotime(date('Y-m-d') . ' -1 day')) . "' > /dev/null 2>&1 &");
            }else{
                $this->runThisCommand(ROOT . "/bin/cake report findMissingReportsOfOffices '" . date('Y-m-d', strtotime('first day of this month')) . "' '" . date('Y-m-d', strtotime(date('Y-m-d') . ' -1 day')) . "' > /dev/null 2>&1 &");
            }
        }
        else if (intval($time) >= 13 && intval($time) < 14) {
            $this->performanceUnitUpdate();
            $this->cronForUnitDashboard();
        }
        else if (intval($time) >= 19 && intval($time) < 20) {
            $this->performanceOfficeUpdate();
            sleep(10);
            $this->cronForOfficeDashboard();
        }
        else if (intval($time) >= 20 && intval($time) < 21) {
            $this->requestUrl('https://nothi.gov.bd/cron/runReportUpdateChecker');
            $this->runThisCommand(ROOT . "/bin/cake report checkReportMismatch '" . date('Y-m-d', strtotime('first day of this month')) . "' '" . date('Y-m-d', strtotime('last day of this month')) . "' > /dev/null 2>&1 &");
        }
        else if (intval($time) >= 21 && intval($time) < 22) {
            $this->runThisCommand(ROOT . "/bin/cake notification OfficeWisePendingPotrojari > /dev/null 2>&1 &");
            sleep(10);
            $this->monitorCron();
        }
        $week_day = date('N');
        if (!empty($week_day) && $week_day == 5 && intval($time) >= 8 && intval($time) < 9) {
            $this->requestUrl('http://103.48.17.139/nothidbhealth/healthCheck');
        }

        if (!empty($week_day) && $week_day == 7 && intval($time) >= 10 && intval($time) < 11) {
            $this->runThisCommand(ROOT . "/bin/cake notification saturdayPendingDakNothiNotification");
        }
    }

    private function reportMonitorDashboardSummary($officeid)
    {
        set_time_limit(0);
        try {
            $this->switchOffice($officeid, 'DashboardOffice');

            TableRegistry::remove('DakMovements');
            TableRegistry::remove('Potrojari');
            TableRegistry::remove('NothiParts');
            TableRegistry::remove('employee_offices');
            TableRegistry::remove('UserLoginHistory');
            TableRegistry::remove('NothiMasterCurrentUsers');
            TableRegistry::remove('NothiDakPotroMaps');
            TableRegistry::remove('Dashboard');
            TableRegistry::remove('DakUsers');

            $dashboardTable = TableRegistry::get('Dashboard');
            $DakUsersTable = TableRegistry::get('DakUsers');
            $DakMovementsTable = TableRegistry::get('DakMovements');
            $potrojariTable = TableRegistry::get('Potrojari');
            $today = date('Y-m-d');
            $yesterday = date('Y-m-d', strtotime($today . ' -1 day'));


            /*             * Inbox Total* */

            $totalyesterdayinbox = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0,
                'Inbox', [$yesterday, $yesterday]);

            $totalprevinbox = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0, 'Inbox');
            /*             * Inbox Total* */

            /*             * Outbox Total* */

            $totalyesterdayoutbox = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0,
                'Outbox', [$yesterday, $yesterday]);

            $totalprevoutbox = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0, 'Outbox');
            /*             * Outbox Total* */

            /*             * Nothivukto * */

            $totalyesterdaynothivukto = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0,
                'NothiVukto', [$yesterday, $yesterday]);

            $totalprevnothivukto = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0,
                'NothiVukto');
            /*             * Nothivukto * */

            /*             * Nothijato * */

            $totalyesterdaynothijato = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0,
                'NothiJato', [$yesterday, $yesterday]);
            $totalprevnothijato = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0,
                'NothiJato');
            /*             * Nothijato * */

            /*             * Nothi Part Here * */
            $nothi_data_yesterday = TableRegistry::get('Dashboard')->nothiDashboardCount($officeid,
                0, 0, [$yesterday, $yesterday]);

            /*             * Potrojari* */

            $totalyesterdaypotrojari = TableRegistry::get('NisponnoRecords')->allPotrojariCount($officeid, 0, 0,
                [$yesterday, $yesterday]);
            /*             * Potrojari* */


            /*             * Self Given Note* */

            $returnSummary['totalyesterdaysrijitonote'] = $nothi_data_yesterday['selfNote'];

            /*             * Self Given Note* */

            /*             * Result* */


            $returnSummary['totalyesterdayinbox'] = $totalyesterdayinbox;
            $returnSummary['totalinboxall'] = $totalprevinbox;


            $returnSummary['totalyesterdayoutbox'] = $totalyesterdayoutbox;
            $returnSummary['totaloutboxall'] = $totalprevoutbox;


            $returnSummary['totalyesterdaynothivukto'] = $totalyesterdaynothivukto;
            $returnSummary['totalnothivuktoall'] = $totalprevnothivukto;


            $returnSummary['totalyesterdaynothijato'] = $totalyesterdaynothijato;
            $returnSummary['totalnothijatoall'] = $totalprevnothijato;


            $returnSummary['totalyesterdaypotrojari'] = $totalyesterdaypotrojari;


            $returnSummary['totalyesterdaydaksohonote'] = $nothi_data_yesterday['dakNote'];


            $returnSummary['totalnisponnodakall'] = $returnSummary['totalnothijatoall'] + $returnSummary['totalnothivuktoall'] /* + $DakMovementsTable->countOnulipiDak($officeid,
              0, 0) */
            ;

            $prevNothiData = $dashboardTable->nothiDashboardCount($officeid,
                0, 0);
            $returnSummary['totalnisponnonoteall'] = $prevNothiData['potrojariNisponno'] + $prevNothiData['noteNisponno'];


            $returnSummary['totalOnisponnodakall'] = $DakUsersTable->getOnisponnoDak($officeid, 0, 0);


            $returnSummary['totalOnisponnonoteall'] = $prevNothiData['onisponno'];

            if ($returnSummary['totalOnisponnodakall'] < 0) {
                $returnSummary['totalOnisponnodakall'] = 0;
            }
            if ($returnSummary['totalnisponnonoteall'] < 0) {
                $returnSummary['totalnisponnonoteall'] = 0;
            }

            $returnSummary['totalOnisponnoall'] = $returnSummary['totalOnisponnodakall'] + $returnSummary['totalOnisponnonoteall'];

            $returnSummary['totalnisponnoall'] = $returnSummary['totalnisponnodakall'] + $returnSummary['totalnisponnonoteall'];

            $returnSummary['Status'] = 1;
        } catch (\Exception $ex) {
            $returnSummary['totalyesterdayinbox'] = 0;
            $returnSummary['totalinboxall'] = 0;


            $returnSummary['totalyesterdayoutbox'] = 0;
            $returnSummary['totaloutboxall'] = 0;


            $returnSummary['totalyesterdaynothivukto'] = 0;
            $returnSummary['totalnothivuktoall'] = 0;


            $returnSummary['totalyesterdaynothijato'] = 0;
            $returnSummary['totalnothijatoall'] = 0;

            $returnSummary['totalnisponnodakall'] = 0;


            $returnSummary['totalOnisponnodakall'] = 0;

            $returnSummary['totalyesterdaypotrojari'] = 0;

            $returnSummary['totalyesterdaydaksohonote'] = 0;

            $returnSummary['totalyesterdaysrijitonote'] = 0;

            $returnSummary['totalnisponnonoteall'] = 0;

            $returnSummary['totalOnisponnonoteall'] = 0;

            $returnSummary['totalOnisponnoall'] = 0;

            $returnSummary['totalnisponnoall'] = 0;

            $returnSummary['Status'] = 0;
            $returnSummary['msg'] = $ex->getMessage();
        }
        return $returnSummary;
    }

    private function updateMonitorDashboardSummary($officeid)
    {
        set_time_limit(0);
        $performance_officesTable = TableRegistry::get('PerformanceOffices');
        try {
            $this->switchOffice($officeid, 'DashboardOffice');
            $today = date('Y-m-d');
            $yesterday = date('Y-m-d', strtotime($today . ' -1 day'));
            // Collect From Designation Table
            $condition = [
                'totalyesterdayinbox' => 'SUM(inbox)', 'totalyesterdayoutbox' => 'SUM(outbox)', 'totalyesterdaynothijato' => 'SUM(nothijat)',
                'totalyesterdaynothivukto' => 'SUM(nothivukto)',
                'totalyesterdaysrijitonote' => 'SUM(selfnote)',
                'totalyesterdaydaksohonote' => 'SUM(daksohonote)',
                'totalyesterdaypotrojari' => 'SUM(potrojari)',
                'totalID' => 'count(id)'
            ];
            $performanceOffice_yesterday = $performance_officesTable->getOfficeData($officeid, [$yesterday, $yesterday])->select($condition)->group(['office_id'])->first();
            if (!empty($performanceOffice_yesterday)) {
//                $this->out($officeid . ' From Performance Office.');
                /*                 * Result* */


                $returnSummary['totalyesterdayinbox'] = $performanceOffice_yesterday['totalyesterdayinbox'];


                $returnSummary['totalyesterdayoutbox'] = $performanceOffice_yesterday['totalyesterdayoutbox'];


                $returnSummary['totalyesterdaynothivukto'] = $performanceOffice_yesterday['totalyesterdaynothivukto'];


                $returnSummary['totalyesterdaynothijato'] = $performanceOffice_yesterday['totalyesterdaynothijato'];


                $returnSummary['totalyesterdaypotrojari'] = $performanceOffice_yesterday['totalyesterdaypotrojari'];


                $returnSummary['totalyesterdaydaksohonote'] = $performanceOffice_yesterday['totalyesterdaydaksohonote'];

                $returnSummary['totalyesterdaysrijitonote'] = $performanceOffice_yesterday['totalyesterdaysrijitonote'];


                $result = TableRegistry::get('DashboardOffices')->getData($officeid);

                /*                 * Full Nisponno Dak* */
                $returnSummary['totaltodaynisponnodak'] = /* $returnSummary['totaltodaynothijato'] + $returnSummary['totaltodaynothivukto'] /* + $DakMovementsTable->countOnulipiDak($officeid,
                      0, 0, [$today, $today])  + */
                    $result['totalNisponnoDak'];


                /*                 * Full Nisponno Note* */
                $returnSummary['totaltodaynisponnonote'] = $result['totalNisponnoPotrojari'] + $result['totalNisponnoNote'];


                /*                 * Full Onisponno Dak* */
                TableRegistry::remove('DakUsers');
                $returnSummary['totaltodayOnisponnodak'] = TableRegistry::get('DakUsers')->getOnisponnoDak($officeid,
                    0, 0);

                /*                 * Full Onisponno Note* */
                $returnSummary['totaltodayOnisponnonote'] = $result['totalONisponno'] /* + $temp_today['selfNote']
                  + $temp_today['dakNote'] - $temp_today['noteNisponno'] - $temp_today['potrojariNisponno'] */
                ;

                if ($returnSummary['totaltodayOnisponnodak'] < 0) {
                    $returnSummary['totaltodayOnisponnodak'] = 0;
                }
                if ($returnSummary['totaltodaynisponnonote'] < 0) {
                    $returnSummary['totaltodaynisponnonote'] = 0;
                }

                $returnSummary['totaltodayOnisponno'] = $returnSummary['totaltodayOnisponnodak'] + $returnSummary['totaltodayOnisponnonote'];

                $returnSummary['totaltodaynisponno'] = $returnSummary['totaltodaynisponnodak'] + $returnSummary['totaltodaynisponnonote'];

                $returnSummary['Status'] = 1;
            } else {

                TableRegistry::remove('DakMovements');
                TableRegistry::remove('Potrojari');
                TableRegistry::remove('NothiParts');
                TableRegistry::remove('employee_offices');
                TableRegistry::remove('UserLoginHistory');
                TableRegistry::remove('NothiMasterCurrentUsers');
                TableRegistry::remove('NothiDakPotroMaps');

                $dashboardTable = TableRegistry::get('Dashboard');
                $DakUsersTable = TableRegistry::get('DakUsers');
                $DakMovementsTable = TableRegistry::get('DakMovements');
                $potrojariTable = TableRegistry::get('Potrojari');
                $nothiPartTable = TableRegistry::get('NothiParts');
                $NothiMasterCurrentUsersTable = TableRegistry::get('NothiMasterCurrentUsers');
                $NothiDakPotroMapsTable = TableRegistry::get('NothiDakPotroMaps');


                /*                 * Inbox Total* */

//                $totaltodayinbox     = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0,
//                    'Inbox', [$today, $today]);
                $totaltodayinbox = 0;
                $totalyesterdayinbox = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0,
                    'Inbox', [$yesterday, $yesterday]);

                /*                 * Inbox Total* */

                /*                 * Outbox Total* */

//                $totaltodayoutbox     = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0,
//                    'Outbox', [$today, $today]);
                $totaltodayoutbox = 0;
                $totalyesterdayoutbox = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0,
                    'Outbox', [$yesterday, $yesterday]);

                /*                 * Outbox Total* */

                /*                 * Nothivukto * */

//                $totaltodaynothivukto     = $DakMovementsTable->countAllDakforOffice($officeid, 0,
//                    0, 'NothiVukto', [$today, $today]);
                $totaltodaynothivukto = 0;
                $totalyesterdaynothivukto = $DakMovementsTable->countAllDakforOffice($officeid, 0,
                    0, 'NothiVukto', [$yesterday, $yesterday]);

                /*                 * Nothivukto * */

                /*                 * Nothijato * */

//                $totaltodaynothijato     = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0,
//                    'NothiJato', [$today, $today]);
                $totaltodaynothijato = 0;
                $totalyesterdaynothijato = $DakMovementsTable->countAllDakforOffice($officeid, 0, 0,
                    'NothiJato', [$yesterday, $yesterday]);
                /*                 * Nothijato * */

                /*                 * Potrojari* */

                $totalyesterdaypotrojari = TableRegistry::get('NisponnoRecords')->allPotrojariCount($officeid,
                    0, 0, [$yesterday, $yesterday]);
                /*                 * Potrojari* */

                /*                 * Nothi Part Here * */
                $nothi_data_yesterday = TableRegistry::get('Dashboard')->nothiDashboardCount($officeid,
                    0, 0, [$yesterday, $yesterday]);

                /*                 * Self Given Note* */

                $returnSummary['totalyesterdaysrijitonote'] = $nothi_data_yesterday['selfNote'];

                /*                 * Self Given Note* */

                /*                 * Result* */


                $returnSummary['totaltodayinbox'] = $totaltodayinbox;
                $returnSummary['totalyesterdayinbox'] = $totalyesterdayinbox;


                $returnSummary['totaltodayoutbox'] = $totaltodayoutbox;
                $returnSummary['totalyesterdayoutbox'] = $totalyesterdayoutbox;


                $returnSummary['totaltodaynothivukto'] = $totaltodaynothivukto;
                $returnSummary['totalyesterdaynothivukto'] = $totalyesterdaynothivukto;


                $returnSummary['totaltodaynothijato'] = $totaltodaynothijato;
                $returnSummary['totalyesterdaynothijato'] = $totalyesterdaynothijato;


                $returnSummary['totalyesterdaypotrojari'] = $totalyesterdaypotrojari;


                $returnSummary['totalyesterdaydaksohonote'] = $nothi_data_yesterday['dakNote'];

                $result = TableRegistry::get('DashboardOffices')->getData($officeid);

                /*                 * Full Nisponno Dak* */
                $returnSummary['totaltodaynisponnodak'] = /* $returnSummary['totaltodaynothijato'] + $returnSummary['totaltodaynothivukto'] /* + $DakMovementsTable->countOnulipiDak($officeid,
                      0, 0, [$today, $today])  + */
                    $result['totalNisponnoDak'];


//                $temp_today = TableRegistry::get('Dashboard')->nothiDashboardCount($officeid, 0, 0,
//                    [$today, $today]);

                /*                 * Full Nisponno Note* */
                $returnSummary['totaltodaynisponnonote'] = $result['totalNisponnoPotrojari'] + $result['totalNisponnoNote'];


                /*                 * Full Onisponno Dak* */
                TableRegistry::remove('DakUsers');
                $returnSummary['totaltodayOnisponnodak'] = TableRegistry::get('DakUsers')->getOnisponnoDak($officeid,
                    0, 0);

                /*                 * Full Onisponno Note* */
                $returnSummary['totaltodayOnisponnonote'] = $result['totalONisponno'] /* + $temp_today['selfNote']
                  + $temp_today['dakNote'] - $temp_today['noteNisponno'] - $temp_today['potrojariNisponno'] */
                ;

                if ($returnSummary['totaltodayOnisponnodak'] < 0) {
                    $returnSummary['totaltodayOnisponnodak'] = 0;
                }
                if ($returnSummary['totaltodaynisponnonote'] < 0) {
                    $returnSummary['totaltodaynisponnonote'] = 0;
                }

                $returnSummary['totaltodayOnisponno'] = $returnSummary['totaltodayOnisponnodak'] + $returnSummary['totaltodayOnisponnonote'];

                $returnSummary['totaltodaynisponno'] = $returnSummary['totaltodaynisponnodak'] + $returnSummary['totaltodaynisponnonote'];

                $returnSummary['Status'] = 1;
            }
        } catch (\Exception $ex) {

            /*             * Result* */

            //need to last nisponno note,dak count otherwise previous count omitted.
            TableRegistry::remove('DashboardOffices');
            $result = TableRegistry::get('DashboardOffices')->getData($officeid);

            $returnSummary['totaltodayinbox'] = 0;
            $returnSummary['totalyesterdayinbox'] = 0;


            $returnSummary['totaltodayoutbox'] = 0;
            $returnSummary['totalyesterdayoutbox'] = 0;


            $returnSummary['totaltodaynothivukto'] = 0;
            $returnSummary['totalyesterdaynothivukto'] = 0;


            $returnSummary['totaltodaynothijato'] = 0;
            $returnSummary['totalyesterdaynothijato'] = 0;

            $returnSummary['totalyesterdaysrijitonote'] = 0;

            $returnSummary['totalyesterdaypotrojari'] = 0;


            $returnSummary['totalyesterdaydaksohonote'] = 0;


            $returnSummary['totaltodaynisponnodak'] = isset($result['totalNisponnoDak']) ? $result['totalNisponnoDak'] : 0;


            $returnSummary['totaltodaynisponnonote'] = (isset($result['totalNisponnoPotrojari']) || isset($result['totalNisponnoNote'])) ? ($result['totalNisponnoPotrojari'] + $result['totalNisponnoNote']) : 0;


            $returnSummary['totaltodayOnisponnodak'] = 0;

            $returnSummary['totaltodayOnisponnonote'] = isset($result['totalONisponno']) ? $result['totalONisponno'] : 0;


            $returnSummary['totaltodayOnisponno'] = 0;
            $returnSummary['totaltodaynisponno'] = 0;

            $returnSummary['Status'] = 0;
            $returnSummary['msg'] = $ex->getMessage();
        }
        return $returnSummary;
    }

    public function performanceOfficeUpdate($date_start = '')
    {
        ini_set('memory_limit', -1);
        set_time_limit(0);
        $start_time = strtotime(date('Y-m-d H:i:s'));

        $officesTable = TableRegistry::get('Offices');
        $office_domains = TableRegistry::get('OfficeDomains');
        $all_offices = $office_domains->getOfficesData();

        $performanceOfficeTable = TableRegistry::get('PerformanceOffices');
        $reportsTable = TableRegistry::get('Reports');
        $MinistriesTable = TableRegistry::get('OfficeMinistries');
        $LayersTable = TableRegistry::get('OfficeLayers');
        $OriginsTable = TableRegistry::get('OfficeOrigins');

        $total = 0;
        $error = 0;
        $success = 0;
        $error_id = [];


        foreach ($all_offices as $office_id => $office_name) {

            $period = [];
            $temp_error = 0;
            $office_record = $performanceOfficeTable->getLastUpdateTime($office_id);

            if (!empty($date_start)) {
                $begin = new \DateTime($date_start);
            } else if (!empty($office_record)) {
                $begin = ($office_record['record_date']);
            } else {
                $begin = new \DateTime(date('Y-m-d', strtotime(date('Y-m-d') . ' -1 week')));
            }
            $end = new \DateTime(date('Y-m-d'));

            for ($i = $begin; $begin < $end; $i->modify('+1 day')) {
                $period [] = $i->format("Y-m-d");
            }
            if (!empty($period)) {
                try {
                    //reinitialize agian to skip MySql erver has gone away error
                    TableRegistry::remove('Offices');
                    TableRegistry::remove('OfficeDomains');
                    TableRegistry::remove('PerformanceOffices');
                    TableRegistry::remove('Reports');
                    TableRegistry::remove('OfficeMinistries');
                    TableRegistry::remove('OfficeLayers');
                    TableRegistry::remove('OfficeOrigins');
                    TableRegistry::remove('PerformanceDesignations');
                    TableRegistry::remove('EmployeeOffices');

                    $officesTable = TableRegistry::get('Offices');
                    $office_domains = TableRegistry::get('OfficeDomains');

                    $performanceOfficeTable = TableRegistry::get('PerformanceOffices');
                    $reportsTable = TableRegistry::get('Reports');
                    $MinistriesTable = TableRegistry::get('OfficeMinistries');
                    $LayersTable = TableRegistry::get('OfficeLayers');
                    $OriginsTable = TableRegistry::get('OfficeOrigins');

                    //reinitialize agian to skip MySql erver has gone away error

                    $this->switchOffice($office_id, 'OfficeDB');
                    foreach ($period as $dt) {
                        $has_record = $performanceOfficeTable->find()->where(['record_date' => $dt, 'office_id' => $office_id])->count();
                        if ($has_record > 0) {
                            $total++;
                            $success++;
                            continue;
                        }
                        $total++;
                        $date = $dt;

                        if (!($office_related_data = Cache::read('office_related_data_' . $office_id))) {
                            $office_related_data = $officesTable->getAll(['id' => $office_id],
                                ['office_ministry_id', 'office_layer_id', 'office_origin_id', 'office_name_bng'])->first();
                            Cache::write('office_related_data_' . $office_id, $office_related_data);
                        }

                        // Collect From Performance Unit Table
                        $from_designation_report = true;
                        $all_unit = TableRegistry::get('EmployeeOffices')->getAllUnitByOfficeID($office_id)->group(['office_unit_id'])->toArray();
                        $condition = [
                            'totalInbox' => 'SUM(inbox)', 'totalOutbox' => 'SUM(outbox)', 'totalNothijato' => 'SUM(nothijat)',
                            'totalNothivukto' => 'SUM(nothivukto)', 'totalNisponnoDak' => 'SUM(nisponnodak)', 'totalpotrojariall' => 'SUM(potrojari)',
                            'totalSouddog' => 'SUM(selfnote)', 'OnisponnoDak' => 'SUM(onisponnodak)', 'OnisponnoNote' => 'SUM(onisponnonote)',
                            'totalDaksohoNote' => 'SUM(daksohonote)', 'totalNisponnoPotrojari' => 'SUM(nisponnopotrojari)',
                            'totalNisponnoNote' => 'SUM(nisponnonote)',
                            'totalNisponno' => 'SUM(nisponno)', 'totalONisponno' => 'SUM(onisponno)',
                        ];
                        if (!empty($all_unit)) {
//                          $hasDataInPerformanceUnit = TableRegistry::get('PerformanceUnits')->getAllUnitData($all_unit,[$date,$date])->select($condition)->group(['unit_id'])->toArray();
                            $hasDataInPerformanceUnit = TableRegistry::get('PerformanceDesignations')->getOfficeData([$office_id], [$date, $date])->select($condition)->group(['office_id'])->toArray();
                            if (!empty($hasDataInPerformanceUnit)) {
                                $returnSummary['totalInbox'] = 0;
                                $returnSummary['totalOutbox'] = 0;
                                $returnSummary['totalNothijato'] = 0;
                                $returnSummary['totalNothivukto'] = 0;
                                $returnSummary['totalNisponnoDak'] = 0;
                                $returnSummary['totalONisponnoDak'] = 0;
                                $returnSummary['totalPotrojari'] = 0;
                                $returnSummary['totalSouddog'] = 0;
                                $returnSummary['totalDaksohoNote'] = 0;
                                $returnSummary['totalNisponnoNote'] = 0;
                                $returnSummary['totalNisponnoPotrojari'] = 0;
                                $returnSummary['totalONisponnoNote'] = 0;
                                $returnSummary['totalNisponno'] = 0;
                                $returnSummary['totalONisponno'] = 0;
                                foreach ($hasDataInPerformanceUnit as $val_form_designation) {
                                    $returnSummary['totalPotrojari'] += $val_form_designation['totalpotrojariall'];
                                    $returnSummary['totalONisponnoDak'] += $val_form_designation['OnisponnoDak'];
                                    $returnSummary['totalONisponnoNote'] += $val_form_designation['OnisponnoNote'];
                                    $returnSummary['totalSouddog'] += $val_form_designation['totalSouddog'];
                                    $returnSummary['totalDaksohoNote'] += $val_form_designation['totalDaksohoNote'];
                                    $returnSummary['totalNisponnoNote'] += $val_form_designation['totalNisponnoNote'];
                                    $returnSummary['totalNisponnoPotrojari'] += $val_form_designation['totalNisponnoPotrojari'];
                                    $returnSummary['totalNisponno'] += $val_form_designation['totalNisponno'];
                                    $returnSummary['totalONisponno'] += $val_form_designation['totalONisponno'];
                                }
                                TableRegistry::remove('DakMovements');
                                $dakMovmentsTable = TableRegistry::get('DakMovements');
                                $returnSummary['totalInbox'] = $dakMovmentsTable->countAllDakforOffice($office_id, 0,
                                    0, 'Inbox', [$date, $date]);
                                $returnSummary['totalOutbox'] = $dakMovmentsTable->countAllDakforOffice($office_id, 0,
                                    0, 'Outbox', [$date, $date]);
                                $returnSummary['totalNothijato'] = $dakMovmentsTable->countAllDakforOffice($office_id, 0,
                                    0, 'Nothijato', [$date, $date]);
                                $returnSummary['totalNothivukto'] = $dakMovmentsTable->countAllDakforOffice($office_id, 0,
                                    0, 'Nothivukto', [$date, $date]);
//                               $returnSummary['totalONisponnoDak'] = TableRegistry::get('DakUsers')->getOnisponnoDak($office_id);
                                $returnSummary['totalNisponnoDak'] = $returnSummary['totalNothijato'] + $returnSummary['totalNothivukto'];
//                              $returnSummary['totalONisponnoNote'] = TableRegistry::get('Dashboard')->getOnisponnoNoteCount($office_id,0,0, [$date,$date]);
                                $returnSummary['internal_potrojari'] = TableRegistry::get('NisponnoRecords')->generatePotrojariNisponnoInternalExternalCount($office_id, 0, 0, [$date, $date]);
                                $returnSummary['external_potrojari'] = $returnSummary['totalNisponnoPotrojari'] - $returnSummary['internal_potrojari'];
                                $unassignedEmployees = TableRegistry::get('EmployeeOffices')->getUnassingedEmolyeesDesignation($office_id);
                                $returnSummary['unassignedDesignation'] = count($unassignedEmployees);
                                if ($returnSummary['unassignedDesignation'] == 0) {
                                    $returnSummary['unassignedPendingDak'] = $returnSummary['unassignedPendingNote'] = 0;
                                } else {
                                    $returnSummary['unassignedPendingDak'] = TableRegistry::get('DakUsers')->getOnisponnoDakByMultipleDesignations($unassignedEmployees, $date);
                                    $returnSummary['unassignedPendingNote'] = TableRegistry::get('Dashboard')->getOnisponnoNoteCountByMultipleDesignations($unassignedEmployees, [$date, $date]);
                                }
                            } else {
                                $from_designation_report = false;
                            }
                        } else {
                            $from_designation_report = false;
                        }

                        if ($from_designation_report == false) {
                            $returnSummary = $reportsTable->newPerformanceReport($office_id,
                                0, 0, ['date_start' => $date, 'date_end' => $date]);
                        }
                        $performance_offices_entity = $performanceOfficeTable->newEntity();

                        $performance_offices_entity->inbox = $returnSummary['totalInbox'];
                        $performance_offices_entity->outbox = $returnSummary['totalOutbox'];
                        $performance_offices_entity->nothijat = $returnSummary['totalNothijato'];
                        $performance_offices_entity->nothivukto = $returnSummary['totalNothivukto'];
                        $performance_offices_entity->nisponnodak = $returnSummary['totalNisponnoDak'];
                        $performance_offices_entity->onisponnodak = $returnSummary['totalONisponnoDak'];

                        if ($returnSummary['totalSouddog'] < 0) {
                            $returnSummary['totalSouddog'] = 0;
                        }
                        $performance_offices_entity->potrojari = $returnSummary['totalPotrojari'];
                        $performance_offices_entity->selfnote = $returnSummary['totalSouddog'];
                        $performance_offices_entity->daksohonote = ($returnSummary['totalNothivukto']
                        < $returnSummary['totalDaksohoNote'] ? $returnSummary['totalNothivukto']
                            : $returnSummary['totalDaksohoNote']);
                        $performance_offices_entity->nisponnopotrojari = $returnSummary['totalNisponnoPotrojari'] = ($returnSummary['totalPotrojari']
                        < $returnSummary['totalNisponnoPotrojari'] ? $returnSummary['totalPotrojari']
                            : $returnSummary['totalNisponnoPotrojari']);
                        $performance_offices_entity->nisponnonote = $returnSummary['totalNisponnoNote'];
                        $performance_offices_entity->onisponnonote = $returnSummary['totalONisponnoNote'];

                        $performance_offices_entity->nisponno = $returnSummary['totalNisponno'];
                        $performance_offices_entity->onisponno = $returnSummary['totalONisponno'];

//                        $internal_potrojari = TableRegistry::get('NisponnoRecords')->generatePotrojariNisponnoInternalExternalCount($office_id,0,0,[$date,$date]);
                        $internal_potrojari = $returnSummary['internal_potrojari'];
//                        $external_potrojari = $returnSummary['totalNisponnoPotrojari'] - $internal_potrojari;
                        $external_potrojari = $returnSummary['external_potrojari'];
                        $performance_offices_entity->potrojari_nisponno_internal = ($external_potrojari < 0) ? $returnSummary['totalNisponnoPotrojari'] : $internal_potrojari;
                        $performance_offices_entity->potrojari_nisponno_external = ($external_potrojari < 0) ? 0 : $external_potrojari;

                        $performance_offices_entity->unassigned_designation = $returnSummary['unassignedDesignation'];
                        $performance_offices_entity->unassigned_pending_dak = $returnSummary['unassignedPendingDak'];
                        $performance_offices_entity->unassigned_pending_note = $returnSummary['unassignedPendingNote'];

                        $performance_offices_entity->office_id = $office_id;
                        $performance_offices_entity->ministry_id = $office_related_data['office_ministry_id'];
                        $performance_offices_entity->layer_id = $office_related_data['office_layer_id'];
                        $performance_offices_entity->origin_id = $office_related_data['office_origin_id'];
                        $performance_offices_entity->office_name = $office_name;

                        if (!($ministry_info = Cache::read('ministry_info_' . $office_related_data['office_ministry_id']))) {
                            $ministry_info = $MinistriesTable->getBanglaName($office_related_data['office_ministry_id']);
                            Cache::write('ministry_info_' . $office_related_data['office_ministry_id'], $ministry_info);
                        }

                        $performance_offices_entity->ministry_name = $ministry_info['name_bng'];

                        if (!($layer_info = Cache::read('layer_info_' . $office_related_data['office_layer_id']))) {
                            $layer_info = $LayersTable->getBanglaName($office_related_data['office_layer_id']);
                            Cache::write('layer_info_' . $office_related_data['office_layer_id'], $layer_info);
                        }

                        $performance_offices_entity->layer_name = $layer_info['layer_name_bng'];

                        if (!($origin_info = Cache::read('origin_info_' . $office_related_data['office_origin_id']))) {
                            $origin_info = $OriginsTable->getBanglaName($office_related_data['office_origin_id']);
                            Cache::write('origin_info_' . $office_related_data['office_origin_id'], $origin_info);
                        }

                        $performance_offices_entity->origin_name = $origin_info['office_name_bng'];

                        $performance_offices_entity->status = 1;
                        $performance_offices_entity->record_date = $date;
//                            pr($performance_offices_entity);die;

                        if ($performanceOfficeTable->save($performance_offices_entity)) {
                            $success++;
                        } else {
                            $error++;
                            if ($temp_error == 0) {
                                $error_id [] = "Office Id:" . $office_id . ", Office Name:" . $office_name . "  <br> Error:  Could not save";
                                $temp_error = 1;
                            }
                        }
                    }
                } catch (\Exception $ex) {
                    $error++;
                    if ($temp_error == 0) {
                        $error_id [] = "Office Id:" . $office_id . ", Office Name:" . $office_name . "  <br> Error: " . $ex->getMessage();
                        $temp_error = 1;
                    }
                }
            }
        }

        $response = "<br>Total office run/check during this process: " . $total;
        $end_time = strtotime(date('Y-m-d H:i:s'));
        $time_difference = round(abs(($end_time - $start_time) / 60), 2);
        $response .= "<br>Duration of this process: " . $time_difference . " minutes";
        if (!empty($error_id)) {
            $response .= '<br>Error occurred: ' . $error . '.<br> Error details:<br> <ul>';
            foreach ($error_id as $id) {
                $response .= "<li>" . $id . "</li><br>";
            }
            $response .= "</ul> ";
        } else {
            $response .= "<br><br><b>Report generation process run successfully. No error occurred</b><br>";
        }
        if (NOTIFICATION_OFF == 0) {
            $params = ['to_email' => 'mhasan.a2i@gmail.com', 'to_cc' => 'nothi.team@gmail.com', 'to_bcc' => 'tusharkaiser@gmail.com', 'subject' => "[Report] Office-wise performance [Date - " . date('Y-m-d') . "]", 'email_body' => "Dear Concern,<br> Office-wise performance report [Date - " . date('Y-m-d') . "] is ready. Detail of this process has been given below.<br>" . $response . "<br>Thanks,<br>Nothi Team"];
            $this->sendEmailByEmailer($params);
        }

        $this->saveCronLog("[Report] Office-wise performance [Date - " . date('Y-m-d') . "]", date('Y-m-d'), $start_time, $end_time,
            $total, $total - $error, $error, $response);
    }

    public function performanceUnitUpdate($date_start = '')
    {
        set_time_limit(0);
        ini_set('memory_limit', -1);
        $start_time = strtotime(date('Y-m-d H:i:s'));

        $unitsTable = TableRegistry::get('OfficeUnits');
        $employee_offices = TableRegistry::get('EmployeeOffices');
        $all_units = $employee_offices->getAllUnitID()->toArray();
        $performanceUnitTable = TableRegistry::get('PerformanceUnits');
        $reportsTable = TableRegistry::get('Reports');
        $MinistriesTable = TableRegistry::get('OfficeMinistries');
        $LayersTable = TableRegistry::get('OfficeLayers');
        $OfficesTable = TableRegistry::get('Offices');
        $UnitOriginsTable = TableRegistry::get('OfficeOriginUnits');
        $performance_designationTable = TableRegistry::get('PerformanceDesignations');

        $total = 0;
        $error = 0;
        $success = 0;
        $error_id = [];

        foreach ($all_units as $unit_id => $unit_name) {
            $temp_error = 0;
            $period = [];
            $unit_record = $performanceUnitTable->getLastUpdateTime($unit_id);
            if (!empty($date_start)) {
                $begin = new \DateTime($date_start);
            } else if (!empty($unit_record)) {
                $begin = ($unit_record['record_date']);
            } else {
                $begin = new \DateTime(date('Y-m-d', strtotime(date('Y-m-d') . ' -1 week')));
            }
            $end = new \DateTime(date('Y-m-d'));

            for ($i = $begin; $begin < $end; $i->modify('+1 day')) {
                $period [] = $i->format("Y-m-d");
            }
            if (!empty($period)) {
                try {
                    //reinitialize again to skip MySql server has gone away
                    TableRegistry::remove('OfficeUnits');
                    TableRegistry::remove('EmployeeOffices');
                    TableRegistry::remove('PerformanceUnits');
                    TableRegistry::remove('Reports');
                    TableRegistry::remove('OfficeMinistries');
                    TableRegistry::remove('OfficeLayers');
                    TableRegistry::remove('Offices');
                    TableRegistry::remove('OfficeOriginUnits');
                    TableRegistry::remove('PerformanceDesignations');

                    $unitsTable = TableRegistry::get('OfficeUnits');
                    $employee_offices = TableRegistry::get('EmployeeOffices');
                    $performanceUnitTable = TableRegistry::get('PerformanceUnits');
                    $reportsTable = TableRegistry::get('Reports');
                    $MinistriesTable = TableRegistry::get('OfficeMinistries');
                    $LayersTable = TableRegistry::get('OfficeLayers');
                    $OfficesTable = TableRegistry::get('Offices');
                    $UnitOriginsTable = TableRegistry::get('OfficeOriginUnits');
                    $performance_designationTable = TableRegistry::get('PerformanceDesignations');
                    //reinitialize again to skip MySql server has gone away

                    $unit_related_data = $unitsTable->getAll(['id' => $unit_id],
                        ['office_ministry_id', 'office_layer_id', 'office_origin_unit_id', 'office_id'])->first();
                    $this->switchOffice($unit_related_data['office_id'], 'OfficeDB');
                    foreach ($period as $dt) {
                        $has_record = $performanceUnitTable->find()->where(['record_date' => $dt, 'unit_id' => $unit_id])->count();
                        if ($has_record > 0) {
                            continue;
                        }
                        $total++;
                        $date = $dt;
//                        pr($unit_related_data['office_id']);die;
                        // Collect From Designation Table
                        $from_designation_report = true;
                        $all_designation = $employee_offices->getAllDesignationByOfficeOrUnitID($unit_related_data['office_id'],
                            $unit_id)->distinct(['EmployeeOffices.office_unit_organogram_id'])->toArray();
                        $condition = [
                            'totalInbox' => 'SUM(inbox)', 'totalOutbox' => 'SUM(outbox)', 'totalNothijato' => 'SUM(nothijat)',
                            'totalNothivukto' => 'SUM(nothivukto)', 'totalNisponnoDak' => 'SUM(nisponnodak)', 'totalpotrojariall' => 'SUM(potrojari)',
                            'totalSouddog' => 'SUM(selfnote)',
                            'totalDaksohoNote' => 'SUM(daksohonote)', 'totalNisponnoPotrojari' => 'SUM(nisponnopotrojari)',
                            'totalNisponnoNote' => 'SUM(nisponnonote)',
                            'totalNisponno' => 'SUM(nisponno)', 'totalONisponno' => 'SUM(onisponno)',
                            'totalID' => 'count(id)', 'designation_id'
                        ];
                        if (!empty($all_designation)) {
                            $hasDataInPerformanceDesignation = $performance_designationTable->getDesignationData($all_designation, [$date, $date])->select($condition)->group(['designation_id'])->toArray();
                            if (!empty($hasDataInPerformanceDesignation)) {
                                $returnSummary['totalInbox'] = 0;
                                $returnSummary['totalOutbox'] = 0;
                                $returnSummary['totalNothijato'] = 0;
                                $returnSummary['totalNothivukto'] = 0;
                                $returnSummary['totalNisponnoDak'] = 0;
                                $returnSummary['totalONisponnoDak'] = 0;
                                $returnSummary['totalPotrojari'] = 0;
                                $returnSummary['totalSouddog'] = 0;
                                $returnSummary['totalDaksohoNote'] = 0;
                                $returnSummary['totalNisponnoNote'] = 0;
                                $returnSummary['totalNisponnoPotrojari'] = 0;
                                $returnSummary['totalONisponnoNote'] = 0;
                                $returnSummary['totalNisponno'] = 0;
                                $returnSummary['totalONisponno'] = 0;
                                foreach ($hasDataInPerformanceDesignation as $val_form_designation) {
                                    $returnSummary['totalInbox'] += $val_form_designation['totalInbox'];
                                    $returnSummary['totalOutbox'] += $val_form_designation['totalOutbox'];
                                    $returnSummary['totalNothijato'] += $val_form_designation['totalNothijato'];
                                    $returnSummary['totalNothivukto'] += $val_form_designation['totalNothivukto'];
                                    $returnSummary['totalNisponnoDak'] += $val_form_designation['totalNisponnoDak'];
                                    $returnSummary['totalONisponnoDak'] += TableRegistry::get('DakUsers')->getOnisponnoDak($unit_related_data['office_id'], $unit_id);
                                    $returnSummary['totalPotrojari'] += $val_form_designation['totalpotrojariall'];
                                    $returnSummary['totalSouddog'] += $val_form_designation['totalSouddog'];
                                    $returnSummary['totalDaksohoNote'] += $val_form_designation['totalDaksohoNote'];
                                    $returnSummary['totalNisponnoNote'] += $val_form_designation['totalNisponnoNote'];
                                    $returnSummary['totalNisponnoPotrojari'] += $val_form_designation['totalNisponnoPotrojari'];
                                    $returnSummary['totalONisponnoNote'] += TableRegistry::get('NothiMasterCurrentUsers')->getAllNothiPartNo($unit_related_data['office_id'], $unit_id, 0, [$date, $date])->count();
                                    $returnSummary['totalNisponno'] += $val_form_designation['totalNisponno'];
                                    $returnSummary['totalONisponno'] += $val_form_designation['totalONisponno'];
                                }
                            } else {
                                $from_designation_report = false;
                            }
                        } else {
                            $from_designation_report = false;
                        }

                        if ($from_designation_report == false) {
                            $returnSummary = $reportsTable->newPerformanceReport($unit_related_data['office_id'],
                                $unit_id, 0, ['date_start' => $date, 'date_end' => $date]);
                        }

                        $performance_units_entity = $performanceUnitTable->newEntity();

                        $performance_units_entity->inbox = $returnSummary['totalInbox'];
                        $performance_units_entity->outbox = $returnSummary['totalOutbox'];
                        $performance_units_entity->nothijat = $returnSummary['totalNothijato'];
                        $performance_units_entity->nothivukto = $returnSummary['totalNothivukto'];
                        $performance_units_entity->nisponnodak = $returnSummary['totalNisponnoDak'];
                        $performance_units_entity->onisponnodak = $returnSummary['totalONisponnoDak'];

                        $performance_units_entity->potrojari = $returnSummary['totalPotrojari'];
                        if ($returnSummary['totalSouddog'] < 0) {
                            $returnSummary['totalSouddog'] = 0;
                        }
                        $performance_units_entity->selfnote = $returnSummary['totalSouddog'];
                        $performance_units_entity->daksohonote = ($returnSummary['totalNothivukto']
                        < $returnSummary['totalDaksohoNote'] ? $returnSummary['totalNothivukto']
                            : $returnSummary['totalDaksohoNote']);
                        $performance_units_entity->nisponnopotrojari = ($returnSummary['totalPotrojari']
                        < $returnSummary['totalNisponnoPotrojari'] ? $returnSummary['totalPotrojari']
                            : $returnSummary['totalNisponnoPotrojari']);
                        $performance_units_entity->nisponnonote = $returnSummary['totalNisponnoNote'];
                        $performance_units_entity->onisponnonote = $returnSummary['totalONisponnoNote'];

                        $performance_units_entity->nisponno = $returnSummary['totalNisponno'];
                        $performance_units_entity->onisponno = $returnSummary['totalONisponno'];

                        $performance_units_entity->office_id = $unit_related_data['office_id'];
                        $performance_units_entity->unit_id = $unit_id;
                        $performance_units_entity->ministry_id = $unit_related_data['office_ministry_id'];
                        $performance_units_entity->layer_id = $unit_related_data['office_layer_id'];
                        $performance_units_entity->origin_id = $unit_related_data['office_origin_unit_id'];
                        $performance_units_entity->unit_name = $unit_name;

                        if (!($office_info = Cache::read('office_info_' . $unit_related_data['office_id']))) {
                            $office_info = $OfficesTable->getBanglaName($unit_related_data['office_id']);
                            Cache::write('office_info_' . $unit_related_data['office_id'], $office_info);
                        }

                        $performance_units_entity->office_name = $office_info['office_name_bng'];

                        if (!($ministry_info = Cache::read('ministry_info_' . $unit_related_data['office_ministry_id']))) {
                            $ministry_info = $MinistriesTable->getBanglaName($unit_related_data['office_ministry_id']);
                            Cache::write('ministry_info_' . $unit_related_data['office_ministry_id'], $ministry_info);
                        }

                        $performance_units_entity->ministry_name = $ministry_info['name_bng'];

                        if (!($layer_info = Cache::read('layer_info_' . $unit_related_data['office_layer_id']))) {
                            $layer_info = $LayersTable->getBanglaName($unit_related_data['office_layer_id']);
                            Cache::write('layer_info_' . $unit_related_data['office_layer_id'], $layer_info);
                        }

                        $performance_units_entity->layer_name = $layer_info['layer_name_bng'];

                        if (!($origin_info = Cache::read('origin_info_' . $unit_related_data['office_origin_unit_id']))) {
                            $origin_info = $UnitOriginsTable->getBanglaName($unit_related_data['office_origin_unit_id']);
                            Cache::write('origin_info_' . $unit_related_data['office_origin_unit_id'], $origin_info);
                        }

                        $performance_units_entity->origin_name = $origin_info['unit_name_bng'];

                        $performance_units_entity->status = 1;
                        $performance_units_entity->record_date = $date;

                        if ($performanceUnitTable->save($performance_units_entity)) {
                            $success++;
                        } else {
                            $error++;
                            if ($temp_error == 0) {
                                $error_id [] = "Unit Id:" . $unit_id . ", Unit Name:" . $unit_name . ", Office Id:" . $unit_related_data['office_id'] . ", Office name:" . $office_info['office_name_bng'] . "<br>Error: Could not Save";
                                $temp_error = 1;
                            }
                        }
                    }
                    $dataSourceObject = ConnectionManager::get('default');
                    $dataSourceObject->disconnect();
                    ConnectionManager::drop('default');
                } catch (\Exception $ex) {
                    $error++;
                    if ($temp_error == 0) {
                        $error_id [] = "Unit Id:" . $unit_id . ", Unit Name:" . $unit_name . ", Office Id:" . $unit_related_data['office_id'] . "<br> Error: " . $ex->getMessage();
                        $temp_error = 1;
                    }
                }
            }
        }

        $response = "<br>Total unit run/check during this process: " . count($all_units);
        $end_time = strtotime(date('Y-m-d H:i:s'));
        $time_difference = round(abs(($end_time - $start_time) / 60), 2);
        $response .= "<br>Duration of this process: " . $time_difference . " mins";
        if (!empty($error_id)) {
            $response .= '<br>Error occurred: ' . $error . '.<br> Error details:<br> <ul>';
            foreach ($error_id as $id) {
                $response .= "<li>" . $id . " </li><br> ";
            }
            $response .= "</ul>";
        } else {
            $response .= "<br><br><b>Report generation process run successfully. No error occurred</b><br>";
        }
        if (NOTIFICATION_OFF == 0) {
            $params = ['to_email' => 'mhasan.a2i@gmail.com', 'to_cc' => 'nothi.team@gmail.com', 'to_bcc' => 'tusharkaiser@gmail.com', 'subject' => "[Report] Unit-wise performance [Date - " . date('Y-m-d') . "]", 'email_body' => "Dear Concern,<br> Unit-wise performance report [Date - " . date('Y-m-d') . "] is ready. Detail of this process has been given below.<br>" . $response . "<br>Thanks,<br>Nothi Team"];
            $this->sendEmailByEmailer($params);
        }

        $this->saveCronLog("[Report] Unit-wise performance [Date - " . date('Y-m-d') . "]", date('Y-m-d'), $start_time, $end_time,
            $total, $total - $error, $error, $response);
    }

    public function performanceDesignationUpdate($date_start = '')
    {
        set_time_limit(0);
        ini_set('memory_limit', -1);
        $start_time = strtotime(date('Y-m-d H:i:s'));

        $employeeOfficesTable = TableRegistry::get('EmployeeOffices');
        $all_designations = $employeeOfficesTable->getAllDesignationID()->toArray();


        $total = 0;
        $error = 0;
        $success = 0;
        $error_id = [];


        foreach ($all_designations as $designation_id => $designation_name) {
            //again initialize table for reconnecting to DB (timeout occur for long process)
            TableRegistry::remove('EmployeeOffices');
            TableRegistry::remove('PerformanceDesignations');
            TableRegistry::remove('Reports');
            TableRegistry::remove('OfficeMinistries');
            TableRegistry::remove('OfficeLayers');
            TableRegistry::remove('Offices');
            TableRegistry::remove('OfficeUnits');
            TableRegistry::remove('OfficeOrigins');

            $employeeOfficesTable = TableRegistry::get('EmployeeOffices');
            $performanceDesignationTable = TableRegistry::get('PerformanceDesignations');
            $reportsTable = TableRegistry::get('Reports');
            $MinistriesTable = TableRegistry::get('OfficeMinistries');
            $LayersTable = TableRegistry::get('OfficeLayers');
            $OfficesTable = TableRegistry::get('Offices');
            $UnitsTable = TableRegistry::get('OfficeUnits');
            $OriginsTable = TableRegistry::get('OfficeOrigins');
            //again initialize table for reconnecting to DB

            if (empty($designation_id)) continue;
            $temp_error = 0;
            $period = [];
            $designation_record = $performanceDesignationTable->getLastUpdateTime($designation_id);
            if (!empty($date_start)) {
                $begin = new \DateTime($date_start);
            } else if (!empty($designation_record)) {
                $begin = ($designation_record['record_date']);
            } else {
                $begin = new \DateTime(date('Y-m-d', strtotime(date('Y-m-d') . ' -1 week')));
            }
            $end = new \DateTime(date('Y-m-d'));
            for ($i = $begin; $begin < $end; $i->modify('+1 day')) {
                $period [] = $i->format("Y-m-d");
            }
            if (!empty($period)) {
                $designation_related_data = $employeeOfficesTable->getDesignationInfo($designation_id);
                try {
                    $this->switchOffice($designation_related_data['office_id'], 'OfficeDB');
                    foreach ($period as $dt) {
                        $has_record = $performanceDesignationTable->checkUpdate($designation_id, $dt);
                        if ($has_record > 0) {
                            continue;
                        }
                        $total++;
                        $date = $dt;

//                        pr($designation_related_data);die;

                        $returnSummary = $reportsTable->newPerformanceReport($designation_related_data['office_id'],
                            $designation_related_data['office_unit_id'], $designation_id,
                            ['date_start' => $date, 'date_end' => $date]);
                        $performance_designatons_entity = $performanceDesignationTable->newEntity();

                        $performance_designatons_entity->inbox = $returnSummary['totalInbox'];
                        $performance_designatons_entity->outbox = $returnSummary['totalOutbox'];
                        $performance_designatons_entity->nothijat = $returnSummary['totalNothijato'];
                        $performance_designatons_entity->nothivukto = $returnSummary['totalNothivukto'];
                        $performance_designatons_entity->nisponnodak = $returnSummary['totalNisponnoDak'];
                        $performance_designatons_entity->onisponnodak = $returnSummary['totalONisponnoDak'];

                        if ($returnSummary['totalSouddog'] < 0) {
                            $returnSummary['totalSouddog'] = 0;
                        }
                        $performance_designatons_entity->selfnote = $returnSummary['totalSouddog'];
                        $performance_designatons_entity->daksohonote = ($returnSummary['totalNothivukto']
                        < $returnSummary['totalDaksohoNote'] ? $returnSummary['totalNothivukto']
                            : $returnSummary['totalDaksohoNote']);
                        $performance_designatons_entity->potrojari = $returnSummary['totalPotrojari'];
                        $performance_designatons_entity->nisponnopotrojari = ($returnSummary['totalPotrojari']
                        < $returnSummary['totalNisponnoPotrojari'] ? $returnSummary['totalPotrojari']
                            : $returnSummary['totalNisponnoPotrojari']);
                        $performance_designatons_entity->nisponnonote = $returnSummary['totalNisponnoNote'];
                        $performance_designatons_entity->onisponnonote = $returnSummary['totalONisponnoNote'];

                        $performance_designatons_entity->nisponno = $returnSummary['totalNisponno'];
                        $performance_designatons_entity->onisponno = $returnSummary['totalONisponno'];

                        if (!($office_related_data = Cache::read('office_related_data_' . $designation_related_data['office_id']))) {
                            $office_related_data = $OfficesTable->getAll(['id' => $designation_related_data['office_id']],
                                ['office_ministry_id', 'office_layer_id', 'office_origin_id',
                                    'office_name_bng'])->first();
                            Cache::write('office_related_data_' . $designation_related_data['office_id'], $office_related_data);
                        }

                        $performance_designatons_entity->office_id = $designation_related_data['office_id'];
                        $performance_designatons_entity->ministry_id = $office_related_data['office_ministry_id'];
                        $performance_designatons_entity->layer_id = $office_related_data['office_layer_id'];
                        $performance_designatons_entity->origin_id = $office_related_data['office_origin_id'];
                        $performance_designatons_entity->office_name = $office_related_data['office_name_bng'];

                        if (!($unit_info = Cache::read('unit_info_' . $designation_related_data['office_unit_id']))) {
                            $unit_info = $UnitsTable->getBanglaName($designation_related_data['office_unit_id']);
                            Cache::write('unit_info_' . $designation_related_data['office_unit_id'], $unit_info);
                        }
                        $performance_designatons_entity->unit_name = $unit_info['unit_name_bng'];
                        $performance_designatons_entity->unit_id = $designation_related_data['office_unit_id'];
                        $performance_designatons_entity->designation_id = $designation_id;
                        $performance_designatons_entity->designation_name = $designation_name;

                        if (!($ministry_info = Cache::read('ministry_info_' . $office_related_data['office_ministry_id']))) {
                            $ministry_info = $MinistriesTable->getBanglaName($office_related_data['office_ministry_id']);
                            Cache::write('ministry_info_' . $office_related_data['office_ministry_id'], $ministry_info);
                        }

                        $performance_designatons_entity->ministry_name = $ministry_info['name_bng'];

                        if (!($layer_info = Cache::read('layer_info_' . $office_related_data['office_layer_id']))) {
                            $layer_info = $LayersTable->getBanglaName($office_related_data['office_layer_id']);
                            Cache::write('layer_info_' . $office_related_data['office_layer_id'], $layer_info);
                        }

                        $performance_designatons_entity->layer_name = $layer_info['layer_name_bng'];

                        if (!($origin_info = Cache::read('origin_info_' . $office_related_data['office_origin_id']))) {
                            $origin_info = $OriginsTable->getBanglaName($office_related_data['office_origin_id']);
                            Cache::write('origin_info_' . $office_related_data['office_origin_id'], $origin_info);
                        }

                        $performance_designatons_entity->origin_name = $origin_info['office_name_bng'];

                        $performance_designatons_entity->status = 1;
                        $performance_designatons_entity->record_date = $date;

                        if ($performanceDesignationTable->save($performance_designatons_entity)) {
                            $success++;
                        } else {
                            if ($temp_error == 0) {
                                $temp_error = 1;
                                $error_id [] = "Designation Id:" . $designation_id . ", Designation Name:" . $designation_name . ", Office Id:" . $designation_related_data['office_id'] . " Office Name: " . $office_related_data['office_name_bng'] . "<br> Error: Could not save";
                            }
                            $error++;
                        }
                    }
                    $dataSourceObject = ConnectionManager::get('default');
                    $dataSourceObject->disconnect();
                    ConnectionManager::drop('default');
                } catch (\Exception $ex) {
                    $error++;
                    if ($temp_error == 0) {
                        $temp_error = 1;
                        $error_id [] = "Designation Id:" . $designation_id . ", Designation Name:" . $designation_name . ", Office Id:" . $designation_related_data['office_id'] . "<br> Error: " . $ex->getMessage();
                    }
                }
            }
//            $this->out('Designation Done: ' . $designation_id);
        }

        $response = "<b>Total designation run/check during this process: " . count($all_designations)."<b>";
        $end_time = strtotime(date('Y-m-d H:i:s'));
        $time_difference = round(abs(($end_time - $start_time) / 3600), 2);
        $response .= "<br>Duration of this process: " . $time_difference . " hour";
        if (!empty($error_id)) {
            $response .= '<br>Error occurred: ' . $error . '.<br> Error details:<br> <ul>';
            foreach ($error_id as $id) {
                $response .= "<li>" . $id . " </li><br> ";
            }
            $response .= "</ul> ";
        } else {
            $response .= "<br><br><b>Report generation process run successfully. No error occurred</b><br>";
        }
        if (NOTIFICATION_OFF == 0) {
            $params = ['to_email' => 'mhasan.a2i@gmail.com', 'to_cc' => 'nothi.team@gmail.com', 'to_bcc' => 'tusharkaiser@gmail.com', 'subject' => "[Report] Designation-wise performance [Date - " . date('Y-m-d') . "]", 'email_body' => "Dear Concern,<br> Designation-wise performance report [Date - " . date('Y-m-d') . "] is ready. Detail of this process has been given below.<br>" . $response . "<br>Thanks,<br>Nothi Team"];
            $this->sendEmailByEmailer($params);
        }
        $this->saveCronLog("[Report] Designation-wise performance [Date - " . date('Y-m-d') . "]", date('Y-m-d'), $start_time,
            $end_time, $total, $total - $error, $error, $response);
    }

    private function clearAllCache()
    {
        try {
            \Cake\Cache\Cache::clear(false);
            \Cake\Cache\Cache::clear(false, 'memcached');
        } catch (\Exception $ex) {
            return;
        }
    }


    public function sendPotroJariEmail($notification_params)
    {
        if (!empty($notification_params['to_email']) && !($this->emailValidate($notification_params['to_email']))) {
            return 'Invalid Email ( ' . $notification_params['to_email'] . ' )';
        }
        $data['to_email'] = $notification_params['to_email'];
        $data['to_cc'] = !empty($notification_params['to_cc']) ? $notification_params['to_cc'] : "";
        $data['to_name'] = !empty($notification_params['to_name']) ? $notification_params['to_name'] : "";
        $data['from_email'] = !empty($notification_params['from_email']) ? $notification_params['from_email'] : "nothi@nothi.org.bd";
        $data['from_name'] = !empty($notification_params['from_name']) ? $notification_params['from_name'] : "নথি ";
        $data['from_des'] = !empty($notification_params['from_des']) ? $notification_params['from_des'] : "";
        $data['format'] = !empty($notification_params['format']) ? $notification_params['format'] : "html";
        $data['template'] = !empty($notification_params['template']) ? $notification_params['template'] : "notification";
        $data['subject'] = $notification_params['subject'];
        $data['email_body'] = !empty($notification_params['email_body']) ? $notification_params['email_body'] : "";
        $data['layout'] = !empty($notification_params['layout']) ? $notification_params['layout'] : "default";
        $data['viewVars'] = !empty($notification_params['viewVars']) ? $notification_params['viewVars'] : [];
        $data['attachments'] = !empty($notification_params['attachments']) ? $notification_params['attachments'] : [];
        $data['type'] = !empty($notification_params['type']) ? $notification_params['type'] : [];
        $data['email_trace_id'] = !empty($notification_params['email_trace_id']) ? $notification_params['email_trace_id'] : [];
        $data['nothi_office'] = !empty($notification_params['nothi_office']) ? $notification_params['nothi_office'] : [];
        $data['office_id'] = !empty($notification_params['office_id']) ? $notification_params['office_id'] : 0;
        $data['office_name'] = !empty($notification_params['office_name']) ? $notification_params['office_name'] : "";
        $data['response_url'] = Router::url(['_name' => 'potrojariStatus']);
        //
        $data['service_id'] = EMAIL_POTRO;
        $nothi_email = new \App\Model\Entity\NothiEmail();
        try {
            $nothi_email->addEmailQueue($data);
            return true;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
        return 'Error occured';
    }

    public function sendCronMessage($to, $cc, $subject = '', $from = 'Cron Jobs', $message, $format = 'html', $layout = 'default', $template = 'notification', $body_subject = '')
    {
        $email = new Email('default');

        try {
            $email->emailFormat($format)->from(['nothi@nothi.org.bd' => $from])
                ->to($to)
                ->addCc($cc)
                ->subject($subject)
                ->viewVars([
                    'notification' => $message,
                    'notificationTime' => \Cake\I18n\Time::parse(date("Y-m-d H:i:s")),
                    'subject' => $subject,
                    'body_subject' => !empty($body_subject) ? $body_subject : $subject,
                ])
                ->template($template, $layout)
                ->send();
        } catch (\Exception $ex) {

            return $ex->getMessage();
        }

        return true;


    }

    public function updateIndividualOffice($office_id = 0)
    {
        if (empty($office_id)) {
            $this->out('No Office ID given');
            return;
        }
        $yesterday = date('Y-m-d', strtotime(date('Y-m-d') . ' -1 day'));
        TableRegistry::remove('Dashboard');
        $DashboardTable = TableRegistry::get('Dashboard');
        TableRegistry::remove('DashboardOffices');
        $performance_officesTable = TableRegistry::get('PerformanceOffices');
        $dashboardOfficesTable = TableRegistry::get('DashboardOffices');

        $office_name = TableRegistry::get('Offices')->getBanglaName($office_id)['office_name_bng'];
        $f = $dashboardOfficesTable->checkUpdate($office_id, date('Y-m-d'));
        if ($f > 0) {
//            $this->out('Already Updated');
            return;
        }
        try {
            $last_update = $dashboardOfficesTable->getLastUpdateDate($office_id);
            if (!empty($last_update['operation_date'])) {
                $time = new Time($last_update['operation_date']);
                $last_update['operation_date'] = $time->i18nFormat(null, null, 'en-US');
            }
            $dt = !empty($last_update['operation_date']) ? date('Y-m-d',
                strtotime($last_update['operation_date'])) : '';
            $this->switchOffice($office_id, 'DashboardOffice');

            // Collect From Designation Table
            $condition = [
                'totalinboxall' => 'SUM(inbox)', 'totaloutboxall' => 'SUM(outbox)', 'totalnothijatoall' => 'SUM(nothijat)',
                'totalnothivuktoall' => 'SUM(nothivukto)', 'totalnisponnodakall' => 'SUM(nisponnodak)',
                'totalsrijitonoteall' => 'SUM(selfnote)', 'totalOnisponnodakall' => 'SUM(onisponnodak)',
                'totaldaksohonoteall' => 'SUM(daksohonote)', 'totalnisponnopotrojariall' => 'SUM(nisponnopotrojari)',
                'totalnisponnonoteall' => 'SUM(nisponnonote)', 'totalpotrojariall' => 'SUM(potrojari)',
                'totalOnisponnonoteall' => 'SUM(onisponnonote)',
                'totalID' => 'count(id)'
            ];
            if (!empty($dt)) {
                $hasDataInPerformanceOffices = $performance_officesTable->getOfficeData($office_id, [$dt, $yesterday])->select($condition)->group(['office_id'])->first();
            }
            if (!empty($hasDataInPerformanceOffices['totalID']) && $hasDataInPerformanceOffices['totalID'] > 0) {
                // get yesterday data
                $performanceOffice_yesterday = $performance_officesTable->getOfficeData($office_id, [$yesterday, $yesterday])->select($condition)->group(['office_id'])->first();
                if (empty($performanceOffice_yesterday)) {
                    $dak_yesterday = $DashboardTable->OfficeDashboardUpdateDak($office_id, $yesterday);
                    $nothi_yesterday = $DashboardTable->OfficeDashboardUpdateNothi($office_id,
                        $yesterday);
                }

                $dak['totalinboxall'] = $hasDataInPerformanceOffices['totalinboxall'];
                $dak['totaloutboxall'] = $hasDataInPerformanceOffices['totaloutboxall'];
                $dak['totalnothivuktoall'] = $hasDataInPerformanceOffices['totalnothivuktoall'];
                $dak['totalnothijatoall'] = $hasDataInPerformanceOffices['totalnothijatoall'];
                $dak['totalnisponnodakall'] = $hasDataInPerformanceOffices['totalnisponnodakall'];
                $dak['totalOnisponnodakall'] = (!empty($performanceOffice_yesterday)) ? $performanceOffice_yesterday['totalOnisponnodakall'] : $dak_yesterday['totalOnisponnodakall'];
                $nothi['totalsrijitonoteall'] = $hasDataInPerformanceOffices['totalsrijitonoteall'];
                $nothi['totaldaksohonoteall'] = $hasDataInPerformanceOffices['totaldaksohonoteall'];
                $nothi['totalnisponnonoteall'] = $hasDataInPerformanceOffices['totalnisponnonoteall'];
                $nothi['totalnisponnopotrojariall'] = $hasDataInPerformanceOffices['totalnisponnopotrojariall'];
                $nothi['totalOnisponnonoteall'] = (!empty($performanceOffice_yesterday)) ? $performanceOffice_yesterday['totalOnisponnonoteall'] : $nothi_yesterday['totalOnisponnonoteall'];
                $dak['totalpotrojariall'] = $hasDataInPerformanceOffices['totalpotrojariall'];

                if (!empty($performanceOffice_yesterday)) {
                    $dak_yesterday['totalinboxall'] = $performanceOffice_yesterday['totalinboxall'];
                    $dak_yesterday['totaloutboxall'] = $performanceOffice_yesterday['totaloutboxall'];
                    $dak_yesterday['totalnothivuktoall'] = $performanceOffice_yesterday['totalnothivuktoall'];
                    $dak_yesterday['totalnothijatoall'] = $performanceOffice_yesterday['totalnothijatoall'];
                    $dak_yesterday['totalnisponnodakall'] = $performanceOffice_yesterday['totalnisponnodakall'];
                    $dak_yesterday['totalOnisponnodakall'] = $performanceOffice_yesterday['totalOnisponnodakall'];
                    $nothi_yesterday['totalsrijitonoteall'] = $performanceOffice_yesterday['totalsrijitonoteall'];
                    $nothi_yesterday['totaldaksohonoteall'] = $performanceOffice_yesterday['totaldaksohonoteall'];
                    $nothi_yesterday['totalnisponnonoteall'] = $performanceOffice_yesterday['totalnisponnonoteall'];
                    $nothi_yesterday['totalnisponnopotrojariall'] = $performanceOffice_yesterday['totalnisponnopotrojariall'];
                    $nothi_yesterday['totalOnisponnonoteall'] = $performanceOffice_yesterday['totalOnisponnonoteall'];
                    $dak_yesterday['totalpotrojariall'] = $performanceOffice_yesterday['totalpotrojariall'];
                }
            } // Collect From Designation Table
            else {
                $dak = $DashboardTable->OfficeDashboardUpdateDak($office_id, $dt);
                $dak_yesterday = $DashboardTable->OfficeDashboardUpdateDak($office_id, $yesterday);
                $nothi = $DashboardTable->OfficeDashboardUpdateNothi($office_id, $dt);
                $nothi_yesterday = $DashboardTable->OfficeDashboardUpdateNothi($office_id, $yesterday);
            }
            if (!empty($dt)) {
                $save_type = 'partial';
            } else {
                $save_type = 'full';
            }

            if (empty($dak) || empty($nothi) || !isset($dak['totalinboxall']) || !isset($nothi['totalsrijitonoteall'])) {
                $data = [
                    'office_id' => $office_id,
                    'office_name' => $office_name,
                    'msg' => 'Empty Data',
                ];
//                $this->out($data);
                return;
            }
            if ($save_type == 'full') {
                if ($dak['totalnothivuktoall'] < $nothi['totaldaksohonoteall']) {
                    $nothi['totaldaksohonoteall'] = $dak['totalnothivuktoall'];
                }
                if ($dak['totalpotrojariall'] < $nothi['totalnisponnopotrojariall']) {
                    $nothi['totalnisponnopotrojariall'] = $dak['totalpotrojariall'];
                }
                // yesterday
                if ($dak_yesterday['totalpotrojariall'] < $nothi_yesterday['totalnisponnopotrojariall']) {
                    $nothi_yesterday['totalnisponnopotrojariall'] = $dak_yesterday['totalpotrojariall'];
                }
                if ($dak_yesterday['totalnothivuktoall'] < $nothi_yesterday['totaldaksohonoteall']) {
                    $nothi_yesterday['totaldaksohonoteall'] = $dak_yesterday['totalnothivuktoall'];
                }
                $nothi_yesterday['totalsrijitonoteall'] = ($nothi_yesterday['totalnisponnopotrojariall']
                    + $nothi_yesterday['totalnisponnonoteall'] + $nothi_yesterday['totalOnisponnonoteall']
                    - $nothi_yesterday['totaldaksohonoteall']);
                $nothi_yesterday['totalOnisponnonoteall'] = $nothi['totalOnisponnonoteall'];
                // yesterday
                $nothi['totalsrijitonoteall'] = ($nothi['totalnisponnopotrojariall'] + $nothi['totalnisponnonoteall']
                    + $nothi['totalOnisponnonoteall'] - $nothi['totaldaksohonoteall']);
                $save_data = [
                    'office_id' => $office_id, 'operation_date' => date('Y-m-d'),
                    'dak_inbox' => $dak['totalinboxall'], 'dak_outbox' => $dak['totaloutboxall'],
                    'nothivukto' => $dak['totalnothivuktoall'],
                    'nothijat' => $dak['totalnothijatoall'], 'dak_nisponno' => $dak['totalnisponnodakall'],
                    'dak_onisponno' => $dak['totalOnisponnodakall'],
                    'self_note' => $nothi['totalsrijitonoteall'], 'dak_note' => $nothi['totaldaksohonoteall'],
                    'note_nisponno' => $nothi['totalnisponnonoteall'],
                    'potrojari_nisponno' => $nothi['totalnisponnopotrojariall'],
                    'onisponno_note' => $nothi['totalOnisponnonoteall'],
                    'potrojari' => $dak['totalpotrojariall'],
                    //yesterday
                    'yesterday_inbox' => $dak_yesterday['totalinboxall'], 'yesterday_outbox' => $dak_yesterday['totaloutboxall'],
                    'yesterday_nothivukto' => $dak_yesterday['totalnothivuktoall'],
                    'yesterday_nothijat' => $dak_yesterday['totalnothijatoall'], 'yesterday_nisponno_dak' => $dak_yesterday['totalnisponnodakall'],
                    'yesterday_onisponno_dak' => $dak_yesterday['totalOnisponnodakall'],
                    'yesterday_self_note' => $nothi_yesterday['totalsrijitonoteall'], 'yesterday_dak_note' => $nothi_yesterday['totaldaksohonoteall'],
                    'yesterday_niponno_note' => $nothi_yesterday['totalnisponnonoteall'],
                    'yesterday_nisponno_potrojari' => $nothi_yesterday['totalnisponnopotrojariall'],
                    'yesterday_onisponno_note' => $nothi_yesterday['totalOnisponnonoteall'],
                    'yesterday_potrojari' => $dak_yesterday['totalpotrojariall'],
                ];
            } else {
                $last_data = $dashboardOfficesTable->getData($office_id);
                if ($dak['totalpotrojariall'] + $last_data['totalPotrojari'] < $nothi['totalnisponnopotrojariall']
                    + $last_data['totalNisponnoPotrojari']
                ) {
                    $nothi['totalnisponnopotrojariall'] = $dak['totalpotrojariall'] + $last_data['totalPotrojari'];
                } else {
                    $nothi['totalnisponnopotrojariall'] = $nothi['totalnisponnopotrojariall'] + $last_data['totalNisponnoPotrojari'];
                }
                if ($dak['totalnothivuktoall'] + $last_data['totalNothivukto'] < $nothi['totaldaksohonoteall']
                    + $last_data['totalDaksohoNote']
                ) {
                    $nothi['totaldaksohonoteall'] = $dak['totalnothivuktoall'] + $last_data['totalNothivukto'];
                } else {
                    $nothi['totaldaksohonoteall'] = $nothi['totaldaksohonoteall'] + $last_data['totalDaksohoNote'];
                }
                // yesterday
                if ($dak_yesterday['totalpotrojariall'] < $nothi_yesterday['totalnisponnopotrojariall']) {
                    $nothi_yesterday['totalnisponnopotrojariall'] = $dak_yesterday['totalpotrojariall'];
                }
                if ($dak_yesterday['totalnothivuktoall'] < $nothi_yesterday['totaldaksohonoteall']) {
                    $nothi_yesterday['totaldaksohonoteall'] = $dak_yesterday['totalnothivuktoall'];
                }
                // yesterday
                $nothi['totalOnisponnonoteall'] = $DashboardTable->getOnisponnoNoteCount($office_id,
                    0, 0, ['', date('Y-m-d', strtotime(date('Y-m-d') . ' -1 day'))]);
                $nothi['totalsrijitonoteall'] = ($nothi['totalnisponnopotrojariall'] + $nothi['totalnisponnonoteall']
                    + $last_data['totalNisponnoNote'] + $nothi['totalOnisponnonoteall'] - $nothi['totaldaksohonoteall']);
                //yesterday
                $nothi_yesterday['totalsrijitonoteall'] = ($nothi_yesterday['totalnisponnopotrojariall']
                    + $nothi_yesterday['totalnisponnonoteall'] + $nothi_yesterday['totalOnisponnonoteall']
                    - $nothi_yesterday['totaldaksohonoteall']);
                $nothi_yesterday['totalOnisponnonoteall'] = $nothi['totalOnisponnonoteall'];
                //yesterday
                $save_data = [
                    'office_id' => $office_id, 'operation_date' => date('Y-m-d'),
                    'dak_inbox' => $dak['totalinboxall'] + $last_data['totalInbox'],
                    'dak_outbox' => $dak['totaloutboxall'] + $last_data['totalOutbox'],
                    'nothivukto' => $dak['totalnothivuktoall'] + $last_data['totalNothivukto'],
                    'nothijat' => $dak['totalnothijatoall'] + $last_data['totalNothijato'],
                    'dak_nisponno' => $dak['totalnisponnodakall'] + $last_data['totalNisponnoDak'],
                    'dak_onisponno' => $dak['totalOnisponnodakall'],
                    'self_note' => $nothi['totalsrijitonoteall'] + $last_data['totalSouddog'],
                    'dak_note' => $nothi['totaldaksohonoteall'] + $last_data['totalDaksohoNote'],
                    'note_nisponno' => $nothi['totalnisponnonoteall'] + $last_data['totalNisponnoNote'],
                    'potrojari_nisponno' => $nothi['totalnisponnopotrojariall'] + $last_data['totalNisponnoPotrojari'],
                    'onisponno_note' => $nothi['totalOnisponnonoteall'],
                    'potrojari' => $dak['totalpotrojariall'] + $last_data['totalPotrojari'],
                    //yesterday
                    'yesterday_inbox' => $dak_yesterday['totalinboxall'], 'yesterday_outbox' => $dak_yesterday['totaloutboxall'],
                    'yesterday_nothivukto' => $dak_yesterday['totalnothivuktoall'],
                    'yesterday_nothijat' => $dak_yesterday['totalnothijatoall'], 'yesterday_nisponno_dak' => $dak_yesterday['totalnisponnodakall'],
                    'yesterday_onisponno_dak' => $dak_yesterday['totalOnisponnodakall'],
                    'yesterday_self_note' => $nothi_yesterday['totalsrijitonoteall'], 'yesterday_dak_note' => $nothi_yesterday['totaldaksohonoteall'],
                    'yesterday_niponno_note' => $nothi_yesterday['totalnisponnonoteall'],
                    'yesterday_nisponno_potrojari' => $nothi_yesterday['totalnisponnopotrojariall'],
                    'yesterday_onisponno_note' => $nothi_yesterday['totalOnisponnonoteall'],
                    'yesterday_potrojari' => $dak_yesterday['totalpotrojariall'],
                ];
            }

            $dashboardOfficesTable->deleteAll(['office_id' => $office_id]);
            $DashboardOfficesEntity = $dashboardOfficesTable->newEntity();
            $DashboardOfficesEntity->office_id = $save_data['office_id'];
            $DashboardOfficesEntity->operation_date = $save_data['operation_date'];
            $DashboardOfficesEntity->dak_inbox = $save_data['dak_inbox'];
            $DashboardOfficesEntity->dak_outbox = $save_data['dak_outbox'];
            $DashboardOfficesEntity->nothivukto = $save_data['nothivukto'];
            $DashboardOfficesEntity->nothijat = $save_data['nothijat'];
            $DashboardOfficesEntity->dak_nisponno = $save_data['dak_nisponno'];
            $DashboardOfficesEntity->dak_onisponno = $save_data['dak_onisponno'];
            $DashboardOfficesEntity->self_note = $save_data['self_note'];
            $DashboardOfficesEntity->dak_note = $save_data['dak_note'];
            $DashboardOfficesEntity->potrojari_nisponno = $save_data['potrojari_nisponno'];
            $DashboardOfficesEntity->note_nisponno = $save_data['note_nisponno'];
            $DashboardOfficesEntity->onisponno_note = $save_data['onisponno_note'];
            $DashboardOfficesEntity->potrojari = $save_data['potrojari'];
            //yesterday
            $DashboardOfficesEntity->yesterday_inbox = $save_data['yesterday_inbox'];
            $DashboardOfficesEntity->yesterday_outbox = $save_data['yesterday_outbox'];
            $DashboardOfficesEntity->yesterday_nothivukto = $save_data['yesterday_nothivukto'];
            $DashboardOfficesEntity->yesterday_nothijat = $save_data['yesterday_nothijat'];
            $DashboardOfficesEntity->yesterday_nisponno_dak = $save_data['yesterday_nisponno_dak'];
            $DashboardOfficesEntity->yesterday_onisponno_dak = $save_data['yesterday_onisponno_dak'];
            $DashboardOfficesEntity->yesterday_self_note = $save_data['yesterday_self_note'];
            $DashboardOfficesEntity->yesterday_dak_note = $save_data['yesterday_dak_note'];
            $DashboardOfficesEntity->yesterday_niponno_note = $save_data['yesterday_niponno_note'];
            $DashboardOfficesEntity->yesterday_nisponno_potrojari = $save_data['yesterday_nisponno_potrojari'];
            $DashboardOfficesEntity->yesterday_potrojari = $save_data['yesterday_potrojari'];
            $DashboardOfficesEntity->yesterday_onisponno_note = $save_data['yesterday_onisponno_note'];

            $f = $dashboardOfficesTable->save($DashboardOfficesEntity);
            if ($f == FALSE) {
                $data = [
                    'office_id' => $office_id,
                    'office_name' => $office_name,
                    'msg' => 'Can not update'
                ];
//                $this->out($data);
                return;
            }
        } catch (\Exception $ex) {
            $data = [
                'office_id' => $office_id,
                'office_name' => $office_name,
                'msg' => $ex->getMessage()
            ];
            $this->out($ex->getMessage());
            return;
        }
    }

    public function updateIndividualUnit($unit_id = 0)
    {
        TableRegistry::remove('Dashboard');
        $DashboardTable = TableRegistry::get('Dashboard');
        TableRegistry::remove('DashboardUnits');
        $dashboardUnitsTable = TableRegistry::get('DashboardUnits');
        TableRegistry::get('OfficeUnits');
        $OfficeUnits = TableRegistry::get('OfficeUnits');
        $performance_unitTable = TableRegistry::get('PerformanceUnits');
        $yesterday = date('Y-m-d', strtotime(date('Y-m-d') . ' -1 day'));
        $f = $dashboardUnitsTable->checkUpdate($unit_id, date('Y-m-d'));
        if ($f > 0) {
//            $this->out('Already Updated');
            return;
        }
        $unit_info = $OfficeUnits->get($unit_id);
        $unit_name = $unit_info['unit_name_bng'];
        $office_id = $unit_info['office_id'];
        try {
            $last_update = $dashboardUnitsTable->getLastUpdateDate($unit_id);
            if (!empty($last_update['operation_date'])) {
                $time = new Time($last_update['operation_date']);
                $last_update['operation_date'] = $time->i18nFormat(null, null, 'en-US');
            }
            $dt = !empty($last_update['operation_date']) ? date('Y-m-d',
                strtotime($last_update['operation_date'])) : '';

            $this->switchOffice($office_id, 'DashboardOffice');
            // Collect From Performance Unit Table
            $condition = [
                'totalinboxall' => 'SUM(inbox)', 'totaloutboxall' => 'SUM(outbox)', 'totalnothijatoall' => 'SUM(nothijat)',
                'totalnothivuktoall' => 'SUM(nothivukto)', 'totalnisponnodakall' => 'SUM(nisponnodak)',
                'totalsrijitonoteall' => 'SUM(selfnote)', 'totalOnisponnodakall' => 'SUM(onisponnodak)',
                'totaldaksohonoteall' => 'SUM(daksohonote)', 'totalnisponnopotrojariall' => 'SUM(nisponnopotrojari)',
                'totalnisponnonoteall' => 'SUM(nisponnonote)', 'totalpotrojariall' => 'SUM(potrojari)',
                'totalOnisponnonoteall' => 'SUM(onisponnonote)',
                'totalID' => 'count(id)'
            ];
            if (!empty($dt)) {
                $hasDataInPerformanceUnit = $performance_unitTable->getUnitData($unit_id, [$dt, $yesterday])->select($condition)->group(['unit_id'])->first();
            }
            if (!empty($hasDataInPerformanceUnit['totalID']) && $hasDataInPerformanceUnit['totalID'] > 0) {
                // get yesterday data
                $performanceUnit_yesterday = $performance_unitTable->getUnitData($unit_id, [$yesterday, $yesterday])->select($condition)->group(['unit_id'])->first();
                if (empty($performanceUnit_yesterday)) {
                    $dak_yesterday = $DashboardTable->OtherDashboardUpdateDak($office_id, $unit_id, 0,
                        $yesterday);
                    $nothi_yesterday = $DashboardTable->OtherDashboardUpdateNothi($office_id, $unit_id,
                        0, $yesterday);
                }

                $dak['totalinboxall'] = $hasDataInPerformanceUnit['totalinboxall'];
                $dak['totaloutboxall'] = $hasDataInPerformanceUnit['totaloutboxall'];
                $dak['totalnothivuktoall'] = $hasDataInPerformanceUnit['totalnothivuktoall'];
                $dak['totalnothijatoall'] = $hasDataInPerformanceUnit['totalnothijatoall'];
                $dak['totalnisponnodakall'] = $hasDataInPerformanceUnit['totalnisponnodakall'];
                $dak['totalOnisponnodakall'] = (!empty($performanceUnit_yesterday)) ? $performanceUnit_yesterday['totalOnisponnodakall'] : $dak_yesterday['totalOnisponnodakall'];
                $nothi['totalsrijitonoteall'] = $hasDataInPerformanceUnit['totalsrijitonoteall'];
                $nothi['totaldaksohonoteall'] = $hasDataInPerformanceUnit['totaldaksohonoteall'];
                $nothi['totalnisponnonoteall'] = $hasDataInPerformanceUnit['totalnisponnonoteall'];
                $nothi['totalnisponnopotrojariall'] = $hasDataInPerformanceUnit['totalnisponnopotrojariall'];
                $nothi['totalOnisponnonoteall'] = (!empty($performanceUnit_yesterday)) ? $performanceUnit_yesterday['totalOnisponnonoteall'] : $nothi_yesterday['totalOnisponnonoteall'];
                $dak['totalpotrojariall'] = $hasDataInPerformanceUnit['totalpotrojariall'];

                if (!empty($performanceUnit_yesterday)) {
                    $dak_yesterday['totalinboxall'] = $performanceUnit_yesterday['totalinboxall'];
                    $dak_yesterday['totaloutboxall'] = $performanceUnit_yesterday['totaloutboxall'];
                    $dak_yesterday['totalnothivuktoall'] = $performanceUnit_yesterday['totalnothivuktoall'];
                    $dak_yesterday['totalnothijatoall'] = $performanceUnit_yesterday['totalnothijatoall'];
                    $dak_yesterday['totalnisponnodakall'] = $performanceUnit_yesterday['totalnisponnodakall'];
                    $dak_yesterday['totalOnisponnodakall'] = $performanceUnit_yesterday['totalOnisponnodakall'];
                    $nothi_yesterday['totalsrijitonoteall'] = $performanceUnit_yesterday['totalsrijitonoteall'];
                    $nothi_yesterday['totaldaksohonoteall'] = $performanceUnit_yesterday['totaldaksohonoteall'];
                    $nothi_yesterday['totalnisponnonoteall'] = $performanceUnit_yesterday['totalnisponnonoteall'];
                    $nothi_yesterday['totalnisponnopotrojariall'] = $performanceUnit_yesterday['totalnisponnopotrojariall'];
                    $nothi_yesterday['totalOnisponnonoteall'] = $performanceUnit_yesterday['totalOnisponnonoteall'];
                    $dak_yesterday['totalpotrojariall'] = $performanceUnit_yesterday['totalpotrojariall'];
                }
            } // Collect From Designation Table
            else {
                $dak = $DashboardTable->OtherDashboardUpdateDak($office_id, $unit_id, 0,
                    $dt);
                $dak_yesterday = $DashboardTable->OtherDashboardUpdateDak($office_id, $unit_id, 0,
                    $yesterday);
                $nothi = $DashboardTable->OtherDashboardUpdateNothi($office_id, $unit_id,
                    0, $dt);
                $nothi_yesterday = $DashboardTable->OtherDashboardUpdateNothi($office_id, $unit_id,
                    0, $yesterday);
            }

            if (!empty($dt)) {
                $save_type = 'partial';
            } else {
                $save_type = 'full';
            }

            if (empty($dak) || empty($nothi) || !isset($dak['totalinboxall']) || !isset($nothi['totalsrijitonoteall'])) {
                $data = [
                    'office_id' => $office_id,
                    'unit_id' => $unit_id,
                    'unit_name' => $unit_name,
                    'msg' => 'Empty Data',
                ];
//                $this->out($data);
                return;
            }
            if ($save_type == 'full') {
                if ($dak['totalnothivuktoall'] < $nothi['totaldaksohonoteall']) {
                    $nothi['totaldaksohonoteall'] = $dak['totalnothivuktoall'];
                }
                // yesterday
                if ($dak_yesterday['totalpotrojariall'] < $nothi_yesterday['totalnisponnopotrojariall']) {
                    $nothi_yesterday['totalnisponnopotrojariall'] = $dak_yesterday['totalpotrojariall'];
                }
                if ($dak_yesterday['totalnothivuktoall'] < $nothi_yesterday['totaldaksohonoteall']) {
                    $nothi_yesterday['totaldaksohonoteall'] = $dak_yesterday['totalnothivuktoall'];
                }
                $nothi_yesterday['totalOnisponnonoteall'] = $nothi['totalOnisponnonoteall'];
                // yesterday
                $nothi['totalsrijitonoteall'] = ($nothi['totalnisponnopotrojariall']
                    + $nothi['totalnisponnonoteall'] + $nothi['totalOnisponnonoteall'] - $nothi['totaldaksohonoteall']);
                if ($nothi['totalsrijitonoteall'] < 0) {
                    $nothi['totalsrijitonoteall'] = 0;
                }
                if ($nothi_yesterday['totalsrijitonoteall'] < 0) {
                    $nothi_yesterday['totalsrijitonoteall'] = 0;
                }
                $save_data = [
                    'unit_id' => $unit_id, 'operation_date' => date('Y-m-d'),
                    'dak_inbox' => $dak['totalinboxall'], 'dak_outbox' => $dak['totaloutboxall'],
                    'nothivukto' => $dak['totalnothivuktoall'],
                    'nothijat' => $dak['totalnothijatoall'], 'dak_nisponno' => $dak['totalnisponnodakall'],
                    'dak_onisponno' => $dak['totalOnisponnodakall'],
                    'self_note' => $nothi['totalsrijitonoteall'], 'dak_note' => $nothi['totaldaksohonoteall'],
                    'note_nisponno' => $nothi['totalnisponnonoteall'],
                    'potrojari_nisponno' => $nothi['totalnisponnopotrojariall'],
                    'onisponno_note' => $nothi['totalOnisponnonoteall'],
                    'potrojari' => $dak['totalpotrojariall'],
                    //yesterday
                    'yesterday_inbox' => $dak_yesterday['totalinboxall'], 'yesterday_outbox' => $dak_yesterday['totaloutboxall'],
                    'yesterday_nothivukto' => $dak_yesterday['totalnothivuktoall'],
                    'yesterday_nothijat' => $dak_yesterday['totalnothijatoall'], 'yesterday_nisponno_dak' => $dak_yesterday['totalnisponnodakall'],
                    'yesterday_onisponno_dak' => $dak_yesterday['totalOnisponnodakall'],
                    'yesterday_self_note' => $nothi_yesterday['totalsrijitonoteall'], 'yesterday_dak_note' => $nothi_yesterday['totaldaksohonoteall'],
                    'yesterday_niponno_note' => $nothi_yesterday['totalnisponnonoteall'],
                    'yesterday_nisponno_potrojari' => $nothi_yesterday['totalnisponnopotrojariall'],
                    'yesterday_onisponno_note' => $nothi_yesterday['totalOnisponnonoteall'],
                    'yesterday_potrojari' => $dak_yesterday['totalpotrojariall'],
                ];
            } else {
                $last_data = $dashboardUnitsTable->getData($unit_id);
                if ($dak['totalpotrojariall'] + $last_data['totalPotrojari'] < $nothi['totalnisponnopotrojariall']
                    + $last_data['totalNisponnoPotrojari']
                ) {
                    $nothi['totalnisponnopotrojariall'] = $dak['totalpotrojariall'] + $last_data['totalPotrojari'];
                } else {
                    $nothi['totalnisponnopotrojariall'] = $nothi['totalnisponnopotrojariall'] + $last_data['totalNisponnoPotrojari'];
                }
                if ($dak['totalnothivuktoall'] + $last_data['totalNothivukto'] < $nothi['totaldaksohonoteall']
                    + $last_data['totalDaksohoNote']
                ) {
                    $nothi['totaldaksohonoteall'] = $dak['totalnothivuktoall'] + $last_data['totalNothivukto'];
                } else {
                    $nothi['totaldaksohonoteall'] = $nothi['totaldaksohonoteall'] + $last_data['totalDaksohoNote'];
                }
                // yesterday
                if ($dak_yesterday['totalpotrojariall'] < $nothi_yesterday['totalnisponnopotrojariall']) {
                    $nothi_yesterday['totalnisponnopotrojariall'] = $dak_yesterday['totalpotrojariall'];
                }
                if ($dak_yesterday['totalnothivuktoall'] < $nothi_yesterday['totaldaksohonoteall']) {
                    $nothi_yesterday['totaldaksohonoteall'] = $dak_yesterday['totalnothivuktoall'];
                }
                // yesterday
                $nothi['totalOnisponnonoteall'] = $DashboardTable->getOnisponnoNoteCount($office_id,
                    $unit_id, 0, ['', date('Y-m-d', strtotime(date('Y-m-d') . ' -1 day'))]);
                //yesterday
                $nothi_yesterday['totalsrijitonoteall'] = ($nothi_yesterday['totalnisponnopotrojariall']
                    + $nothi_yesterday['totalnisponnonoteall'] + $nothi_yesterday['totalOnisponnonoteall']
                    - $nothi_yesterday['totaldaksohonoteall']);
                $nothi_yesterday['totalOnisponnonoteall'] = $nothi['totalOnisponnonoteall'];
                //yesterday
                $save_data = [
                    'unit_id' => $unit_id, 'operation_date' => date('Y-m-d'),
                    'dak_inbox' => $dak['totalinboxall'] + $last_data['totalInbox'],
                    'dak_outbox' => $dak['totaloutboxall'] + $last_data['totalOutbox'],
                    'nothivukto' => $dak['totalnothivuktoall'] + $last_data['totalNothivukto'],
                    'nothijat' => $dak['totalnothijatoall'] + $last_data['totalNothijato'],
                    'dak_nisponno' => $dak['totalnisponnodakall'] + $last_data['totalNisponnoDak'],
                    'dak_onisponno' => $dak['totalOnisponnodakall'],
                    'self_note' => $nothi['totalsrijitonoteall'],
                    'dak_note' => $nothi['totaldaksohonoteall'],
                    'note_nisponno' => $nothi['totalnisponnonoteall'] + $last_data['totalNisponnoNote'],
                    'potrojari_nisponno' => $nothi['totalnisponnopotrojariall'],
                    'onisponno_note' => $nothi['totalOnisponnonoteall'],
                    'potrojari' => $dak['totalpotrojariall'] + $last_data['totalPotrojari'],
                    //yesterday
                    'yesterday_inbox' => $dak_yesterday['totalinboxall'], 'yesterday_outbox' => $dak_yesterday['totaloutboxall'],
                    'yesterday_nothivukto' => $dak_yesterday['totalnothivuktoall'],
                    'yesterday_nothijat' => $dak_yesterday['totalnothijatoall'], 'yesterday_nisponno_dak' => $dak_yesterday['totalnisponnodakall'],
                    'yesterday_onisponno_dak' => $dak_yesterday['totalOnisponnodakall'],
                    'yesterday_self_note' => $nothi_yesterday['totalsrijitonoteall'], 'yesterday_dak_note' => $nothi_yesterday['totaldaksohonoteall'],
                    'yesterday_niponno_note' => $nothi_yesterday['totalnisponnonoteall'],
                    'yesterday_nisponno_potrojari' => $nothi_yesterday['totalnisponnopotrojariall'],
                    'yesterday_onisponno_note' => $nothi_yesterday['totalOnisponnonoteall'],
                    'yesterday_potrojari' => $dak_yesterday['totalpotrojariall'],
                ];
            }
            $dashboardUnitsTable->deleteAll(['unit_id' => $unit_id]);
            $DashboardUnitsEntity = $dashboardUnitsTable->newEntity();
            $DashboardUnitsEntity->unit_id = $save_data['unit_id'];
            $DashboardUnitsEntity->operation_date = $save_data['operation_date'];
            $DashboardUnitsEntity->dak_inbox = $save_data['dak_inbox'];
            $DashboardUnitsEntity->dak_outbox = $save_data['dak_outbox'];
            $DashboardUnitsEntity->nothivukto = $save_data['nothivukto'];
            $DashboardUnitsEntity->nothijat = $save_data['nothijat'];
            $DashboardUnitsEntity->dak_nisponno = $save_data['dak_nisponno'];
            $DashboardUnitsEntity->dak_onisponno = $save_data['dak_onisponno'];
            $DashboardUnitsEntity->self_note = $save_data['self_note'];
            $DashboardUnitsEntity->dak_note = $save_data['dak_note'];
            $DashboardUnitsEntity->potrojari_nisponno = $save_data['potrojari_nisponno'];
            $DashboardUnitsEntity->note_nisponno = $save_data['note_nisponno'];
            $DashboardUnitsEntity->onisponno_note = $save_data['onisponno_note'];
            $DashboardUnitsEntity->potrojari = $save_data['potrojari'];
            //yesterday
            $DashboardUnitsEntity->yesterday_inbox = $save_data['yesterday_inbox'];
            $DashboardUnitsEntity->yesterday_outbox = $save_data['yesterday_outbox'];
            $DashboardUnitsEntity->yesterday_nothivukto = $save_data['yesterday_nothivukto'];
            $DashboardUnitsEntity->yesterday_nothijat = $save_data['yesterday_nothijat'];
            $DashboardUnitsEntity->yesterday_nisponno_dak = $save_data['yesterday_nisponno_dak'];
            $DashboardUnitsEntity->yesterday_onisponno_dak = $save_data['yesterday_onisponno_dak'];
            $DashboardUnitsEntity->yesterday_self_note = $save_data['yesterday_self_note'];
            $DashboardUnitsEntity->yesterday_dak_note = $save_data['yesterday_dak_note'];
            $DashboardUnitsEntity->yesterday_niponno_note = $save_data['yesterday_niponno_note'];
            $DashboardUnitsEntity->yesterday_nisponno_potrojari = $save_data['yesterday_nisponno_potrojari'];
            $DashboardUnitsEntity->yesterday_potrojari = $save_data['yesterday_potrojari'];
            $DashboardUnitsEntity->yesterday_onisponno_note = $save_data['yesterday_onisponno_note'];

            $f = $dashboardUnitsTable->save($DashboardUnitsEntity);
            if ($f == FALSE) {
                $data[] = [
                    'office_id' => $office_id,
                    'unit_id' => $unit_id,
                    'unit_name' => $unit_name,
                    'msg' => 'Can not update'
                ];
                $this->out($unit_id . ' Can not update');
                return;
            } else {
            }
//                if ($save >= 3000) {
//                    break;
//                }
        } catch (\Exception $ex) {
            $data[] = [
                'office_id' => $office_id,
                'unit_id' => $unit_id,
                'unit_name' => $unit_name,
                'msg' => $ex->getMessage()
            ];
            $this->out($ex->getMessage());
            return;
        }
    }

    public function updateIndividualDesignation($designation_id = 0)
    {
        $DashboardTable = TableRegistry::get('Dashboard');
        TableRegistry::remove('DashboardDesignations');
        $dashboardDesignationsTable = TableRegistry::get('DashboardDesignations');
        TableRegistry::get('OfficeUnitOrganograms');
        $designationTable = TableRegistry::get('OfficeUnitOrganograms');
        $performance_designationTable = TableRegistry::get('PerformanceDesignations');
        $yesterday = date('Y-m-d', strtotime(date('Y-m-d') . ' -1 day'));
        $f = $dashboardDesignationsTable->checkUpdate($designation_id, date('Y-m-d'));
        if ($f > 0) {
//            $this->out('Already Updated');
            return;
        }
        $designation_info = $designationTable->get($designation_id);
        $designation_name = $designation_info['designation_eng'];
        $office_id = $designation_info['office_id'];
        $unit_id = $designation_info['office_unit_id'];
        try {
            $last_update = $dashboardDesignationsTable->getLastUpdateDate($designation_id);
            //  date('Y-m-d',  strtotime("+1 day",strtotime($last_update['operation_date']) ) )
            if (!empty($last_update['operation_date'])) {
                $time = new Time($last_update['operation_date']);
                $last_update['operation_date'] = $time->i18nFormat(null, null, 'en-US');
            }
            $dt = !empty($last_update['operation_date']) ? date('Y-m-d',
                strtotime($last_update['operation_date'])) : '';
            $this->switchOffice($office_id, 'DashboardOffice');
            // Collect From Designation Table
            $condition = [
                'totalinboxall' => 'SUM(inbox)', 'totaloutboxall' => 'SUM(outbox)', 'totalnothijatoall' => 'SUM(nothijat)',
                'totalnothivuktoall' => 'SUM(nothivukto)', 'totalnisponnodakall' => 'SUM(nisponnodak)',
                'totalsrijitonoteall' => 'SUM(selfnote)', 'totalOnisponnodakall' => 'SUM(onisponnodak)',
                'totaldaksohonoteall' => 'SUM(daksohonote)', 'totalnisponnopotrojariall' => 'SUM(nisponnopotrojari)',
                'totalnisponnonoteall' => 'SUM(nisponnonote)', 'totalpotrojariall' => 'SUM(potrojari)',
                'totalOnisponnonoteall' => 'SUM(onisponnonote)',
                'totalID' => 'count(id)'
            ];
            if (!empty($dt)) {
                $hasDataInPerformanceDesignation = $performance_designationTable->getDesignationData($designation_id, [$dt, $yesterday])->select($condition)->group(['designation_id'])->first();
            }
            if (!empty($hasDataInPerformanceDesignation['totalID']) && $hasDataInPerformanceDesignation['totalID'] > 0) {
                // get yesterday data
                $performanceDesignation_yesterday = $performance_designationTable->getDesignationData($designation_id, [$yesterday, $yesterday])->select($condition)->group(['designation_id'])->first();
                if (empty($performanceDesignation_yesterday)) {
                    $this->switchOffice($office_id, 'DashboardOffice');
                    $dak_yesterday = $DashboardTable->OtherDashboardUpdateDak($office_id, $unit_id,
                        $designation_id, $yesterday);
                    $nothi_yesterday = $DashboardTable->OtherDashboardUpdateNothi($office_id, $unit_id,
                        $designation_id, $yesterday);
                }

                $dak['totalinboxall'] = $hasDataInPerformanceDesignation['totalinboxall'];
                $dak['totaloutboxall'] = $hasDataInPerformanceDesignation['totaloutboxall'];
                $dak['totalnothivuktoall'] = $hasDataInPerformanceDesignation['totalnothivuktoall'];
                $dak['totalnothijatoall'] = $hasDataInPerformanceDesignation['totalnothijatoall'];
                $dak['totalnisponnodakall'] = $hasDataInPerformanceDesignation['totalnisponnodakall'];
                $dak['totalOnisponnodakall'] = (!empty($performanceDesignation_yesterday)) ? $performanceDesignation_yesterday['totalOnisponnodakall'] : $dak_yesterday['totalOnisponnodakall'];
                $nothi['totalsrijitonoteall'] = $hasDataInPerformanceDesignation['totalsrijitonoteall'];
                $nothi['totaldaksohonoteall'] = $hasDataInPerformanceDesignation['totaldaksohonoteall'];
                $nothi['totalnisponnonoteall'] = $hasDataInPerformanceDesignation['totalnisponnonoteall'];
                $nothi['totalnisponnopotrojariall'] = $hasDataInPerformanceDesignation['totalnisponnopotrojariall'];
                $nothi['totalOnisponnonoteall'] = (!empty($performanceDesignation_yesterday)) ? $performanceDesignation_yesterday['totalOnisponnonoteall'] : $nothi_yesterday['totalOnisponnonoteall'];
                $dak['totalpotrojariall'] = $hasDataInPerformanceDesignation['totalpotrojariall'];

                if (!empty($performanceDesignation_yesterday)) {
                    $dak_yesterday['totalinboxall'] = $performanceDesignation_yesterday['totalinboxall'];
                    $dak_yesterday['totaloutboxall'] = $performanceDesignation_yesterday['totaloutboxall'];
                    $dak_yesterday['totalnothivuktoall'] = $performanceDesignation_yesterday['totalnothivuktoall'];
                    $dak_yesterday['totalnothijatoall'] = $performanceDesignation_yesterday['totalnothijatoall'];
                    $dak_yesterday['totalnisponnodakall'] = $performanceDesignation_yesterday['totalnisponnodakall'];
                    $dak_yesterday['totalOnisponnodakall'] = $performanceDesignation_yesterday['totalOnisponnodakall'];
                    $nothi_yesterday['totalsrijitonoteall'] = $performanceDesignation_yesterday['totalsrijitonoteall'];
                    $nothi_yesterday['totaldaksohonoteall'] = $performanceDesignation_yesterday['totaldaksohonoteall'];
                    $nothi_yesterday['totalnisponnonoteall'] = $performanceDesignation_yesterday['totalnisponnonoteall'];
                    $nothi_yesterday['totalnisponnopotrojariall'] = $performanceDesignation_yesterday['totalnisponnopotrojariall'];
                    $nothi_yesterday['totalOnisponnonoteall'] = $performanceDesignation_yesterday['totalOnisponnonoteall'];
                    $dak_yesterday['totalpotrojariall'] = $performanceDesignation_yesterday['totalpotrojariall'];
                }
            } // Collect From Designation Table
            else {
                $dak = $DashboardTable->OtherDashboardUpdateDak($office_id, $unit_id,
                    $designation_id, $dt);
                $dak_yesterday = $DashboardTable->OtherDashboardUpdateDak($office_id, $unit_id,
                    $designation_id, $yesterday);
                $nothi = $DashboardTable->OtherDashboardUpdateNothi($office_id, $unit_id,
                    $designation_id, $dt);
                $nothi_yesterday = $DashboardTable->OtherDashboardUpdateNothi($office_id, $unit_id,
                    $designation_id, $yesterday);
            }

            if (!empty($dt)) {
                $save_type = 'partial';
            } else {
                $save_type = 'full';
            }

            if (empty($dak) || empty($nothi) || !isset($dak['totalinboxall']) || !isset($nothi['totalsrijitonoteall'])) {
                $data[] = [
                    'office_id' => $office_id,
                    'unit_id' => $unit_id,
                    'designation_id' => $designation_id,
                    'designation_name' => $designation_name,
                    'msg' => 'Empty Data',
                ];
                $this->out($designation_id . 'empty data');
                return;
            }
            if ($save_type == 'full') {
                if ($dak['totalnothivuktoall'] < $nothi['totaldaksohonoteall']) {
                    $nothi['totaldaksohonoteall'] = $dak['totalnothivuktoall'];
                }
                // yesterday
                if ($dak_yesterday['totalpotrojariall'] < $nothi_yesterday['totalnisponnopotrojariall']) {
                    $nothi_yesterday['totalnisponnopotrojariall'] = $dak_yesterday['totalpotrojariall'];
                }
                if ($dak_yesterday['totalnothivuktoall'] < $nothi_yesterday['totaldaksohonoteall']) {
                    $nothi_yesterday['totaldaksohonoteall'] = $dak_yesterday['totalnothivuktoall'];
                }
                $nothi_yesterday['totalOnisponnonoteall'] = $nothi['totalOnisponnonoteall'];
                // yesterday
                $nothi['totalsrijitonoteall'] = ($nothi['totalnisponnopotrojariall']
                    + $nothi['totalnisponnonoteall'] + $nothi['totalOnisponnonoteall'] - $nothi['totaldaksohonoteall']);
                if ($nothi['totalsrijitonoteall'] < 0) {
                    $nothi['totalsrijitonoteall'] = 0;
                }
                if ($nothi_yesterday['totalsrijitonoteall'] < 0) {
                    $nothi_yesterday['totalsrijitonoteall'] = 0;
                }
                $save_data = [
                    'designation_id' => $designation_id, 'operation_date' => date('Y-m-d'),
                    'dak_inbox' => $dak['totalinboxall'], 'dak_outbox' => $dak['totaloutboxall'],
                    'nothivukto' => $dak['totalnothivuktoall'],
                    'nothijat' => $dak['totalnothijatoall'], 'dak_nisponno' => $dak['totalnisponnodakall'],
                    'dak_onisponno' => $dak['totalOnisponnodakall'],
                    'self_note' => $nothi['totalsrijitonoteall'], 'dak_note' => $nothi['totaldaksohonoteall'],
                    'note_nisponno' => $nothi['totalnisponnonoteall'],
                    'potrojari_nisponno' => $nothi['totalnisponnopotrojariall'],
                    'onisponno_note' => $nothi['totalOnisponnonoteall'],
                    'potrojari' => $dak['totalpotrojariall'],
                    //yesterday
                    'yesterday_inbox' => $dak_yesterday['totalinboxall'], 'yesterday_outbox' => $dak_yesterday['totaloutboxall'],
                    'yesterday_nothivukto' => $dak_yesterday['totalnothivuktoall'],
                    'yesterday_nothijat' => $dak_yesterday['totalnothijatoall'], 'yesterday_nisponno_dak' => $dak_yesterday['totalnisponnodakall'],
                    'yesterday_onisponno_dak' => $dak_yesterday['totalOnisponnodakall'],
                    'yesterday_self_note' => $nothi_yesterday['totalsrijitonoteall'], 'yesterday_dak_note' => $nothi_yesterday['totaldaksohonoteall'],
                    'yesterday_niponno_note' => $nothi_yesterday['totalnisponnonoteall'],
                    'yesterday_nisponno_potrojari' => $nothi_yesterday['totalnisponnopotrojariall'],
                    'yesterday_onisponno_note' => $nothi_yesterday['totalOnisponnonoteall'],
                    'yesterday_potrojari' => $dak_yesterday['totalpotrojariall'],
                ];
            } else {
                $last_data = $dashboardDesignationsTable->getData($designation_id);
                if ($dak['totalpotrojariall'] + $last_data['totalPotrojari'] < $nothi['totalnisponnopotrojariall']
                    + $last_data['totalNisponnoPotrojari']
                ) {
                    $nothi['totalnisponnopotrojariall'] = $dak['totalpotrojariall'] + $last_data['totalPotrojari'];
                } else {
                    $nothi['totalnisponnopotrojariall'] = $nothi['totalnisponnopotrojariall'] + $last_data['totalNisponnoPotrojari'];
                }
                if ($dak['totalnothivuktoall'] + $last_data['totalNothivukto'] < $nothi['totaldaksohonoteall']
                    + $last_data['totalDaksohoNote']
                ) {
                    $nothi['totaldaksohonoteall'] = $dak['totalnothivuktoall'] + $last_data['totalNothivukto'];
                } else {
                    $nothi['totaldaksohonoteall'] = $nothi['totaldaksohonoteall'] + $last_data['totalDaksohoNote'];
                }
                // yesterday
                if ($dak_yesterday['totalpotrojariall'] < $nothi_yesterday['totalnisponnopotrojariall']) {
                    $nothi_yesterday['totalnisponnopotrojariall'] = $dak_yesterday['totalpotrojariall'];
                }
                if ($dak_yesterday['totalnothivuktoall'] < $nothi_yesterday['totaldaksohonoteall']) {
                    $nothi_yesterday['totaldaksohonoteall'] = $dak_yesterday['totalnothivuktoall'];
                }
                // yesterday
                $nothi['totalOnisponnonoteall'] = $DashboardTable->getOnisponnoNoteCount($office_id,
                    $unit_id, $designation_id,
                    ['', date('Y-m-d', strtotime(date('Y-m-d') . ' -1 day'))]);
                $nothi['totalsrijitonoteall'] = ($nothi['totalnisponnopotrojariall']
                    + $nothi['totalnisponnonoteall'] + $last_data['totalNisponnoNote'] + $nothi['totalOnisponnonoteall']
                    - $nothi['totaldaksohonoteall']);
                //yesterday
                $nothi_yesterday['totalOnisponnonoteall'] = $nothi['totalOnisponnonoteall'];
                //yesterday
                $save_data = [
                    'designation_id' => $designation_id, 'operation_date' => date('Y-m-d'),
                    'dak_inbox' => $dak['totalinboxall'] + $last_data['totalInbox'],
                    'dak_outbox' => $dak['totaloutboxall'] + $last_data['totalOutbox'],
                    'nothivukto' => $dak['totalnothivuktoall'] + $last_data['totalNothivukto'],
                    'nothijat' => $dak['totalnothijatoall'] + $last_data['totalNothijato'],
                    'dak_nisponno' => $dak['totalnisponnodakall'] + $last_data['totalNisponnoDak'],
                    'dak_onisponno' => $dak['totalOnisponnodakall'],
                    'self_note' => $nothi['totalsrijitonoteall'] + $last_data['totalSouddog'],
                    'dak_note' => $nothi['totaldaksohonoteall'] + $last_data['totalDaksohoNote'],
                    'note_nisponno' => $nothi['totalnisponnonoteall'] + $last_data['totalNisponnoNote'],
                    'potrojari_nisponno' => $nothi['totalnisponnopotrojariall'] + $last_data['totalNisponnoPotrojari'],
                    'onisponno_note' => $DashboardTable->getOnisponnoNoteCount($office_id, $unit_id,
                        $designation_id, ['', date('Y-m-d', strtotime(date('Y-m-d') . ' -1 day'))]),
                    'potrojari' => $dak['totalpotrojariall'] + $last_data['totalPotrojari'],
                    //yesterday
                    'yesterday_inbox' => $dak_yesterday['totalinboxall'], 'yesterday_outbox' => $dak_yesterday['totaloutboxall'],
                    'yesterday_nothivukto' => $dak_yesterday['totalnothivuktoall'],
                    'yesterday_nothijat' => $dak_yesterday['totalnothijatoall'], 'yesterday_nisponno_dak' => $dak_yesterday['totalnisponnodakall'],
                    'yesterday_onisponno_dak' => $dak_yesterday['totalOnisponnodakall'],
                    'yesterday_self_note' => $nothi_yesterday['totalsrijitonoteall'], 'yesterday_dak_note' => $nothi_yesterday['totaldaksohonoteall'],
                    'yesterday_niponno_note' => $nothi_yesterday['totalnisponnonoteall'],
                    'yesterday_nisponno_potrojari' => $nothi_yesterday['totalnisponnopotrojariall'],
                    'yesterday_onisponno_note' => $nothi_yesterday['totalOnisponnonoteall'],
                    'yesterday_potrojari' => $dak_yesterday['totalpotrojariall'],
                ];
            }
            $dashboardDesignationsTable->deleteAll(['designation_id' => $designation_id]);
            $DashboardDesignationsEntity = $dashboardDesignationsTable->newEntity();
            $DashboardDesignationsEntity->designation_id = $designation_id;
            $DashboardDesignationsEntity->operation_date = $save_data['operation_date'];
            $DashboardDesignationsEntity->dak_inbox = $save_data['dak_inbox'];
            $DashboardDesignationsEntity->dak_outbox = $save_data['dak_outbox'];
            $DashboardDesignationsEntity->nothivukto = $save_data['nothivukto'];
            $DashboardDesignationsEntity->nothijat = $save_data['nothijat'];
            $DashboardDesignationsEntity->dak_nisponno = $save_data['dak_nisponno'];
            $DashboardDesignationsEntity->dak_onisponno = $save_data['dak_onisponno'];
            $DashboardDesignationsEntity->self_note = $save_data['self_note'];
            $DashboardDesignationsEntity->dak_note = $save_data['dak_note'];
            $DashboardDesignationsEntity->potrojari_nisponno = $save_data['potrojari_nisponno'];
            $DashboardDesignationsEntity->note_nisponno = $save_data['note_nisponno'];
            $DashboardDesignationsEntity->onisponno_note = $save_data['onisponno_note'];
            $DashboardDesignationsEntity->potrojari = $save_data['potrojari'];
            //yesterday
            $DashboardDesignationsEntity->yesterday_inbox = $save_data['yesterday_inbox'];
            $DashboardDesignationsEntity->yesterday_outbox = $save_data['yesterday_outbox'];
            $DashboardDesignationsEntity->yesterday_nothivukto = $save_data['yesterday_nothivukto'];
            $DashboardDesignationsEntity->yesterday_nothijat = $save_data['yesterday_nothijat'];
            $DashboardDesignationsEntity->yesterday_nisponno_dak = $save_data['yesterday_nisponno_dak'];
            $DashboardDesignationsEntity->yesterday_onisponno_dak = $save_data['yesterday_onisponno_dak'];
            $DashboardDesignationsEntity->yesterday_self_note = $save_data['yesterday_self_note'];
            $DashboardDesignationsEntity->yesterday_dak_note = $save_data['yesterday_dak_note'];
            $DashboardDesignationsEntity->yesterday_niponno_note = $save_data['yesterday_niponno_note'];
            $DashboardDesignationsEntity->yesterday_nisponno_potrojari = $save_data['yesterday_nisponno_potrojari'];
            $DashboardDesignationsEntity->yesterday_potrojari = $save_data['yesterday_potrojari'];
            $DashboardDesignationsEntity->yesterday_onisponno_note = $save_data['yesterday_onisponno_note'];

            $f = $dashboardDesignationsTable->save($DashboardDesignationsEntity);
            if ($f == FALSE) {
                $data[] = [
                    'office_id' => $office_id,
                    'unit_id' => $unit_id,
                    'designation_id' => $designation_id,
                    'designation_name' => $designation_name,
                    'msg' => 'Can not update'
                ];
                $this->out($designation_id . ' Can not update');
                return;
            }
        } catch (\Exception $ex) {
            $data[] = [
                'office_id' => $office_id,
                'unit_id' => $unit_id,
                'designation_id' => $designation_id,
                'designation_name' => $designation_name,
                'msg' => $ex->getMessage()
            ];
            $this->out($ex->getMessage());
            return;
        }
    }

    public function MonitoringDashboard()
    {
        $this->monitorCron();
    }

    public function saveUserLoginHistory()
    {
        $this->saveUserLogin();
    }


    public function saveUserLogin()
    {
        $start_time = strtotime(date('Y-m-d H:i:s'));
        $total = 0;
        $success = 0;
        $error = 0;
        $tmp = '';
        $unitsTable = TableRegistry::get('OfficeUnits');
        $officesTable = TableRegistry::get('Offices');
        $EmployeeOfficesTable = TableRegistry::get('EmployeeOffices');
        $UserLoginHistoryTable = TableRegistry::get('UserLoginHistory');
        $UnitWiseUserLoginTable = TableRegistry::get('UnitWiseUserLogin');
        $yesterday = date('Y-m-d', strtotime(date('Y-m-d') . ' -1 day'));
        $allUnits = $unitsTable->find('list',
            ['keyField' => 'id', 'valueField' => 'office_id'])->where(['active_status' => 1])->toArray();

        if (!empty($allUnits)) {
            foreach ($allUnits as $k => $office_id) {
                try {
                    $total++;
                    $office_info = $officesTable->find()->select(['office_ministry_id', 'office_layer_id',
                        'office_origin_id'])->where(['id' => $office_id, 'active_status' => 1])->first();
                    if (!empty($office_info)) {
                        $total_user = $EmployeeOfficesTable->getCountOfEmployeeOfOfficeUnites($office_id,
                            $k);
                        $yesterday_login = $UserLoginHistoryTable->getLoginCount(0,
                            0, 0, $office_id, $k, 0, [$yesterday, $yesterday])->count();
                        $UnitWiseUserLoginTable->deleteDataofUnits($k);
                        $UnitWiseUserLoginEntity = $UnitWiseUserLoginTable->newEntity();
                        $UnitWiseUserLoginEntity->ministry_id = $office_info['office_ministry_id'];
                        $UnitWiseUserLoginEntity->layer_id = $office_info['office_layer_id'];
                        $UnitWiseUserLoginEntity->origin_id = $office_info['office_origin_id'];
                        $UnitWiseUserLoginEntity->office_id = $office_id;
                        $UnitWiseUserLoginEntity->unit_id = $k;
                        $UnitWiseUserLoginEntity->total_user = $total_user;
                        $UnitWiseUserLoginEntity->yesterday_login = $yesterday_login;
                        $UnitWiseUserLoginTable->save($UnitWiseUserLoginEntity);
                        $success++;
                    } else {
                        $error++;
                        $tmp .= "<li>Office ID: " . $office_id . " Unit ID: " . $k . " Error: No Office info in Offices Table</li><br>";
                    }
                } catch (\Exception $ex) {
                    $error++;
                    $tmp .= "<li>Office ID: " . $office_id . " Unit ID: " . $k . " Error: " . $ex->getMessage() . "</li><br>";
                }
            }
        }

        $response = "Total: " . $total;
        $end_time = strtotime(date('Y-m-d H:i:s'));
        $time_difference = round(abs(($end_time - $start_time) / 60), 2);
        $response .= "<br> Time: " . $time_difference . " mins";
        if (!empty($error)) {
            $response .= '<br>Error list (' . $error . ') : <ul>';
            $response .= $tmp;
            $response .= "</ul> ";
        } else {
            $response .= "No Error Occured";
        }
        $this->saveCronLog('Unit wise Login Details', date('Y-m-d'), $start_time, $end_time, $total,
            $success, $error, $response);
    }

    public function officesUpdate()
    {
        $this->cronForOfficeDashboard();
    }

    public function unitsUpdate()
    {
        $this->cronForUnitDashboard();
    }

    public function designationsUpdate()
    {
        $this->cronForDesignationDashboard();
    }

    public function updatePotrojaricount()
    {
        $office_domains = TableRegistry::get('OfficeDomains');
        $all_offices = $office_domains->getOfficesData();
        TableRegistry::remove('DashboardOffices');
        $dashboardOfficesTable = TableRegistry::get('DashboardOffices');
        foreach ($all_offices as $office_id => $office_name) {
            try {
                $last_update = $dashboardOfficesTable->getLastUpdateDate($office_id);
                if (!empty($last_update['operation_date'])) {
                    $time = new Time($last_update['operation_date']);
                    $last_update['operation_date'] = $time->i18nFormat(null, null, 'en-US');
                    $dt = !empty($last_update['operation_date']) ? date('Y-m-d',
                        strtotime($last_update['operation_date'])) : '';
                    if (!empty($dt)) {
                        $this->switchOffice($office_id, 'DashboardOffice');
                        TableRegistry::remove('Potrojari');
                        $potrojariCount = TableRegistry::get('Potrojari')->allPotrojariCount($office_id,
                            0, 0, ['', $dt]);
                        $last_data = $dashboardOfficesTable->find()->select(['id'])->where(['office_id' => $office_id])->order(['operation_date DESC'])->first();
                        $data_office = $dashboardOfficesTable->get($last_data['id']);
                        $data_office->potrojari = $potrojariCount;
                        $dashboardOfficesTable->save($data_office);
                    }
                }
            } catch (\Exception $ex) {
                $this->out($ex->getMessage());
            }
        }
    }


    public function deleteMultipleRows($start_date = '', $end_date = '')
    {
        $office_domains = TableRegistry::get('OfficeDomains');
        $all_offices = $office_domains->getOfficesData();
        if (!empty($start_date)) {
            $begin = new \DateTime($start_date);
        } else {
            $begin = new \DateTime('2017-12-01');
        }
        if (!empty($end_date)) {
            $end = new \DateTime($end_date);
        } else {
            $end = new \DateTime(date('Y-m-d'));
        }

        for ($i = $begin; $begin <= $end; $i->modify('+1 day')) {
            $period [] = $i->format("Y-m-d");
        }
        $perfromanceOfficesTable = TableRegistry::get('PerformanceOffices');
        if (!empty($all_offices)) {
            foreach ($all_offices as $office_id => $office_name) {
                if (!empty($period)) {
                    foreach ($period as $day) {
                        $data_count = $perfromanceOfficesTable->find()->where(['record_date' => $day, 'office_id' => $office_id])->count();
                        if ($data_count > 1) {
                            $last_id = $perfromanceOfficesTable->find()->select(['id'])->where(['record_date' => $day, 'office_id' => $office_id])->first();
                            $perfromanceOfficesTable->deleteAll(['record_date' => $day, 'office_id' => $office_id, 'id <>' => $last_id['id']]);
                        }
                    }
                }
            }
        }
        $employee_offices = TableRegistry::get('EmployeeOffices');
        $all_units = $employee_offices->getAllUnitID()->toArray();
        $performanceUnitTable = TableRegistry::get('PerformanceUnits');
        foreach ($all_units as $unit_id => $unit_name) {
            if (!empty($period)) {
                foreach ($period as $day) {
                    $data_count = $performanceUnitTable->find()->where(['record_date' => $day, 'unit_id' => $unit_id])->count();
                    if ($data_count > 1) {
                        $last_id = $performanceUnitTable->find()->select(['id'])->where(['record_date' => $day, 'unit_id' => $unit_id])->first();
                        $performanceUnitTable->deleteAll(['record_date' => $day, 'unit_id' => $unit_id, 'id <>' => $last_id['id']]);
                    }
                }
            }
        }
        $employeeOfficesTable = TableRegistry::get('EmployeeOffices');
        $all_designations = $employeeOfficesTable->getAllDesignationID()->toArray();

        $performanceDesignationTable = TableRegistry::get('PerformanceDesignations');
        foreach ($all_designations as $designation_id => $designation_name) {
            if (!empty($period)) {
                foreach ($period as $day) {
                    $data_count = $performanceDesignationTable->find()->where(['record_date' => $day, 'designation_id' => $designation_id])->count();
                    if ($data_count > 1) {
                        $last_id = $performanceDesignationTable->find()->select(['id'])->where(['record_date' => $day, 'designation_id' => $designation_id])->first();
                        $performanceDesignationTable->deleteAll(['record_date' => $day, 'designation_id' => $designation_id, 'id <>' => $last_id['id']]);
                    }
                }
            }
        }
    }


    public function sendSMSNotification()
    {
        $SmsRequestTable = TableRegistry::get('SmsRequest');
        $SmsRequestData = $SmsRequestTable->find()->where(['try'=>0])->toArray();
        $url = [];
        if (!empty($SmsRequestData)) {
            foreach ($SmsRequestData as $data) {
                if (empty($data['cell_number']) || empty($data['message']) || strlen($data['cell_number'])<=10 ) {
                    $SmsRequestTable->updateAll(['try'=>1,'status'=>2],['id' => $data['id']]);
                    continue;
                }
                $data['message'] = urlencode($data['message']);
                $url[] = "https://bulksms.teletalk.com.bd/link_sms_send.php?op=SMS&user=nothi_user&pass=R6uW9vRs&mobile={$data['cell_number']}&charset=UTF-8&sms={$data['message']}";
                $SmsRequestTable->updateAll(['try'=>1,'status'=>1],['id' => $data['id']]);
            }
        }

        if(!empty($url)){
            foreach($url as $key=>$msg){
                try {
                    $curl_handle = curl_init();
                    curl_setopt($curl_handle, CURLOPT_URL, $msg);
                    curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
                    curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, false);
                    curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Nothi');
                    $response = curl_exec($curl_handle);
                    curl_close($curl_handle);
                } catch (\Exception $ex) {
                    $this->out($ex->getMessage());
                    continue;
                }
            }
        }
    }

    public function sendPotrojariRequest()
    {
        $allPotrojariRequestData = TableRegistry::get('PotrojariRequest')->find()->where(['date(created)' => date('Y-m-d'), 'status' => 0])->toArray();
        if (!empty($allPotrojariRequestData)) {
            foreach ($allPotrojariRequestData as $data) {
                if (!empty($data['office_id']) && !empty($data['potrojari_id']) && !empty($data['command'])) {
                    $this->retryPotrojariRequest($data['office_id'], $data['potrojari_id'], $data['command']);
                }
            }
        }
    }

    private function retryPotrojariRequest($office_id, $potrojari_id, $command)
    {
        try {
            $this->switchOffice($office_id, 'MainNothiOffice');
            // check for which table this request generated
            if (strpos($command, 'potrojariReceiverSent') != false) {
                TableRegistry::remove('PotrojariReceiver');
                $receiverTable = TableRegistry::get('PotrojariReceiver');
                $hasNotsentData = $receiverTable->find()->where(['is_sent' => 0, 'potrojari_id' => $potrojari_id, 'task_reposponse <>' => 'Pending...'])->count();
                if ($hasNotsentData > 0) {
                    $receiverTable->updateAll(['run_task' => 0, 'task_reposponse' => '', 'potro_status' => '', 'retry_count' => 0], ['is_sent' => 0, 'potrojari_id' => $potrojari_id]);
                } else {
//                    $this->out('here');
                    TableRegistry::get('PotrojariRequest')->updateAll(['status' => 1], ['command' => $command, 'office_id' => $office_id, 'potrojari_id' => $potrojari_id]);
                    return true;
                }
            } else {
                TableRegistry::remove('PotrojariOnulipi');
                $onulipiTable = TableRegistry::get('PotrojariOnulipi');
                $hasNotsentData = $onulipiTable->find()->where(['is_sent' => 0, 'potrojari_id' => $potrojari_id,
                    'task_reposponse <>' => 'Pending...'])->count();
                if ($hasNotsentData > 0) {
                    $onulipiTable->updateAll(['run_task' => 0, 'task_reposponse' => '', 'potro_status' => '', 'retry_count' => 0], ['is_sent' => 0, 'potrojari_id' => $potrojari_id]);
                } else {
//                    $this->out('there');
                    TableRegistry::get('PotrojariRequest')->updateAll(['status' => 1], ['command' => $command, 'office_id' => $office_id, 'potrojari_id' => $potrojari_id]);
                    return true;
                }
            }
            if (defined('Live') && Live == 1) {
                $command = str_replace("/nothi_new", "", $command);
            }
            exec($command, $return, $output);
            echo json_encode(['status' => 'success', 'ret' => $return, 'out' => $output, 'com' => $command]);
            echo "\n";
        } catch (\Exception $ex) {
            $this->out($ex->getMessage());
        }
    }


    public function migrateNothiMasterPermissions()
    {
        set_time_limit(0);
        ini_set('memory_limit', -1);
        $this->entryOwnOfficeNothiPermission();
        //sleep(1);
        //$this->entryOtherOfficeNothiPermission();
        die;
    }

    public function migrateNothiMasterCurrentUsers()
    {
        set_time_limit(0);
        ini_set('memory_limit', -1);
        $this->entryOwnOfficeNothiCurrentUser();
        sleep(1);
        $this->entryOtherOfficeNothiCurrentUser();
        die;
    }

    private function entryOwnOfficeNothiPermission()
    {
        set_time_limit(0);
        ini_set('memory_limit', -1);
        $tableOfficeDomain = TableRegistry::get('OfficeDomains');
        $tableNothiMasterPermission = TableRegistry::get('NothiMasterPermissions');
        $con = ConnectionManager::get('default');
        $officeDomains = $tableOfficeDomain->find()->where(['OfficeDomains.status' => 1])->toArray();
        $error = [];
        foreach ($officeDomains as $key => $value) {
            try {
                $this->switchOffice($value['office_id'], "currentOffice");
                $data = $tableNothiMasterPermission->find()->where(['nothi_office' => $value['office_id'], 'mig_status' => 0])->toArray();

                if (!empty($data)) {
                    TableRegistry::remove('nothi_master_permissions_mig');
                    $tableofficeNothiMasterPermission = TableRegistry::get('nothi_master_permissions_mig');
                    $tableofficeNothiMasterPermission->deleteAll(['nothi_office' => $value['office_id']]);
                    $con->begin();
                    foreach ($data as $dataKey => $dataValue) {
                        $entity = $tableofficeNothiMasterPermission->newEntity();
                        $entity->nothi_masters_id = $dataValue['nothi_masters_id'];
                        $entity->nothi_part_no = $dataValue['nothi_part_no'];
                        $entity->nothi_office = $dataValue['nothi_office'];
                        $entity->office_id = $dataValue['office_id'];
                        $entity->office_unit_id = $dataValue['office_unit_id'];
                        $entity->office_unit_organograms_id = $dataValue['office_unit_organograms_id'];
                        $entity->designation_level = $dataValue['designation_level'];
                        $entity->privilige_type = $dataValue['privilige_type'];
                        $entity->visited = $dataValue['visited'];
                        $entity->created = empty($dataValue['created']) ? date("Y-m-d H:i:s") : $dataValue['created'];
                        $entity->created_by = isset($dataValue['created_by']) ? $dataValue['created_by'] : 0;
                        $entity->modified = isset($dataValue['modified']) ? $dataValue['modified'] : date("Y-m-d H:i:s");
                        $entity->modified_by = isset($dataValue['modified_by']) ? $dataValue['modified_by'] : 0;
                        $tableofficeNothiMasterPermission->save($entity);
                        $tableNothiMasterPermission->updateAll(['mig_status' => 1], ['id' => $dataValue['id']]);
                    }
                    $con->commit();
                }
            } catch (\Exception $ex) {
                $error[$value['office_id']][] = $ex->getMessage();
                $con->rollback();
                continue;
            }
        }
        if (!empty($error)) {
            foreach ($error as $e_k => $e_v) {
                if (is_array($e_v)) {
                    foreach ($e_v as $e_v_k => $e_v_v) {
                        $this->out('Office Id: ' . $e_k . ' msg: ' . $e_v_v);
                    }
                }
            }
        }

//        pr($error);
    }

    public function createCronRequestForMigrationOfPermission()
    {
        set_time_limit(0);
        ini_set('memory_limit', -1);
        $tableOfficeDomain = TableRegistry::get('OfficeDomains');
        $tableCronRequest = TableRegistry::get('CronRequest');
        $officeDomains = $tableOfficeDomain->find()->where(['OfficeDomains.status' => 1])->toArray();
        $indx = 0;
        foreach ($officeDomains as $key => $value) {
            $command = ROOT . "/bin/cake cronjob migrateIndividualOwnOfficeNothiPermission {$value['office_id']}> /dev/null 2>&1 &";
            $tableCronRequest->saveData($command);
            $indx++;
            if ($indx % 300 == 0) {
                $this->runCronRequest();
            }
        }
        $this->entryOtherOfficeNothiPermission();

    }

    public function migrateIndividualOwnOfficeNothiPermission($office_id)
    {
        set_time_limit(0);
        ini_set('memory_limit', -1);
        $tableNothiMasterPermission = TableRegistry::get('NothiMasterPermissions');
        $con = ConnectionManager::get('default');
        $error = [];
        try {
            $this->switchOffice($office_id, "currentOffice");
            $data = $tableNothiMasterPermission->find()->where(['nothi_office' => $office_id, 'mig_status' => 0])->toArray();

            if (!empty($data)) {
                TableRegistry::remove('nothi_master_permissions_mig');
                $tableofficeNothiMasterPermission = TableRegistry::get('nothi_master_permissions_mig');
                $tableofficeNothiMasterPermission->deleteAll(['nothi_office' => $office_id]);
                $con->begin();
                foreach ($data as $dataKey => $dataValue) {
                    $entity = $tableofficeNothiMasterPermission->newEntity();
                    $entity->nothi_masters_id = $dataValue['nothi_masters_id'];
                    $entity->nothi_part_no = $dataValue['nothi_part_no'];
                    $entity->nothi_office = $dataValue['nothi_office'];
                    $entity->office_id = $dataValue['office_id'];
                    $entity->office_unit_id = $dataValue['office_unit_id'];
                    $entity->office_unit_organograms_id = $dataValue['office_unit_organograms_id'];
                    $entity->designation_level = $dataValue['designation_level'];
                    $entity->privilige_type = $dataValue['privilige_type'];
                    $entity->visited = $dataValue['visited'];
                    $entity->created = empty($dataValue['created']) ? date("Y-m-d H:i:s") : $dataValue['created'];
                    $entity->created_by = isset($dataValue['created_by']) ? $dataValue['created_by'] : 0;
                    $entity->modified = isset($dataValue['modified']) ? $dataValue['modified'] : date("Y-m-d H:i:s");
                    $entity->modified_by = isset($dataValue['modified_by']) ? $dataValue['modified_by'] : 0;
                    $tableofficeNothiMasterPermission->save($entity);
                    $tableNothiMasterPermission->updateAll(['mig_status' => 1], ['id' => $dataValue['id']]);
                }
                $con->commit();
            }
        } catch (\Exception $ex) {
            $error[$office_id][] = $ex->getMessage();
            $con->rollback();
        }
        if (!empty($error)) {
            foreach ($error as $e_k => $e_v) {
                if (is_array($e_v)) {
                    foreach ($e_v as $e_v_k => $e_v_v) {
                        $this->out('Office Id: ' . $e_k . ' msg: ' . $e_v_v);
                    }
                }
            }
        }
        $this->runCronRequest();
//        pr($error);
    }

    private function entryOtherOfficeNothiPermission()
    {
        set_time_limit(0);
        ini_set('memory_limit', -1);
        $tableNothiMasterPermission = TableRegistry::get('NothiMasterPermissions');
        $getAllOffices = $tableNothiMasterPermission->find()->select(['nothi_office', 'office_id', 'nothi_part_no'])
            ->where(['nothi_office <> office_id', 'nothi_office >' => 0])->group(['nothi_office', 'office_id', 'nothi_part_no'])->toArray();

        $con = ConnectionManager::get('default');
        $nothi = [];
        $error = [];
        if (!empty($getAllOffices)) {
            foreach ($getAllOffices as $key => $value) {
                try {
                    $data = $tableNothiMasterPermission->find()->select()
                        ->where(['nothi_office' => $value['nothi_office'], 'nothi_part_no' => $value['nothi_part_no']])->hydrate(false)->toArray();
                    if (!empty($data)) {
                        $nothi[$value['office_id']][$value['nothi_office']][$value['nothi_part_no']] = $data;
                    }
                } catch (\Exception $ex) {
                    $error[$value['nothi_office']][] = $ex->getMessage();
                    $con->rollback();
                    continue;
                }
            }
        }

        foreach ($nothi as $key => $value) {
            try {
                $this->switchOffice($key, "currentOffice");
                TableRegistry::remove('nothi_master_permissions_mig');
                $tableofficeNothiMasterPermission = TableRegistry::get('nothi_master_permissions_mig');
                foreach ($value as $nothi_office => $part_array) {
                    $con->begin();
                    foreach ($part_array as $part_no => $part_item) {
                        $tableofficeNothiMasterPermission->deleteAll(['office_id' => $key, 'nothi_office' => $nothi_office, 'nothi_part_no' => $part_no]);
                        foreach ($part_item as $dataKey => $dataValue) {
                            $entity = $tableofficeNothiMasterPermission->newEntity();
                            $entity->nothi_masters_id = $dataValue['nothi_masters_id'];
                            $entity->nothi_part_no = $dataValue['nothi_part_no'];
                            $entity->nothi_office = $dataValue['nothi_office'];
                            $entity->office_id = $dataValue['office_id'];
                            $entity->office_unit_id = $dataValue['office_unit_id'];
                            $entity->office_unit_organograms_id = $dataValue['office_unit_organograms_id'];
                            $entity->designation_level = $dataValue['designation_level'];
                            $entity->privilige_type = $dataValue['privilige_type'];
                            $entity->visited = $dataValue['visited'];
                            $entity->created = empty($dataValue['created']) ? date("Y-m-d H:i:s") : $dataValue['created'];
                            $entity->created_by = isset($dataValue['created_by']) ? $dataValue['created_by'] : 0;
                            $entity->modified = isset($dataValue['modified']) ? $dataValue['modified'] : date("Y-m-d H:i:s");
                            $entity->modified_by = isset($dataValue['modified_by']) ? $dataValue['modified_by'] : 0;
                            $tableofficeNothiMasterPermission->save($entity);
                        }
                    }
                    $con->commit();
                }
            } catch (\Exception $ex) {
                $this->out('Other Office Permission (' . $key . '): ' . $ex->getMessage());
                continue;
            }
        }
    }

    private function entryOwnOfficeNothiCurrentUser()
    {
        set_time_limit(0);
        ini_set('memory_limit', -1);
        $tableOfficeDomain = TableRegistry::get('OfficeDomains');
        $tableNothiMasterPermission = TableRegistry::get('NothiMasterCurrentUsers');
        $con = ConnectionManager::get('default');
        $officeDomains = $tableOfficeDomain->find()->where(['OfficeDomains.status' => 1])->toArray();
        $error = [];
        foreach ($officeDomains as $key => $value) {
            try {
                $this->switchOffice($value['office_id'], "currentOffice");
                $data = $tableNothiMasterPermission->find()->where(['nothi_office' => $value['office_id'], 'mig_status' => 0])->toArray();

                if (!empty($data)) {
                    TableRegistry::remove('nothi_master_current_users_mig');
                    $tableofficeNothiMasterPermission = TableRegistry::get('nothi_master_current_users_mig');
                    $tableofficeNothiMasterPermission->deleteAll(['nothi_office' => $value['office_id']]);
                    $con->begin();
                    foreach ($data as $dataKey => $dataValue) {
                        $entity = $tableofficeNothiMasterPermission->newEntity();
                        $entity->nothi_master_id = $dataValue['nothi_master_id'];
                        $entity->nothi_part_no = $dataValue['nothi_part_no'];
                        $entity->nothi_office = $dataValue['nothi_office'];
                        $entity->office_id = $dataValue['office_id'];
                        $entity->office_unit_id = $dataValue['office_unit_id'];
                        $entity->office_unit_organogram_id = $dataValue['office_unit_organogram_id'];
                        $entity->is_archive = $dataValue['is_archive'];
                        $entity->is_finished = $dataValue['is_finished'];
                        $entity->employee_id = $dataValue['employee_id'];
                        $entity->view_status = $dataValue['view_status'];
                        $entity->issue_date = $dataValue['issue_date'];
                        $entity->forward_date = $dataValue['forward_date'];
                        $entity->is_new = $dataValue['is_new'];
                        $entity->priority = !empty($dataValue['priority']) ? $dataValue['priority'] : 0;
                        $entity->created = empty($dataValue['created']) ? date("Y-m-d H:i:s") : $dataValue['created'];
                        $entity->created_by = isset($dataValue['created_by']) ? $dataValue['created_by'] : 0;
                        $entity->modified = isset($dataValue['modified']) ? $dataValue['modified'] : date("Y-m-d H:i:s");
                        $entity->modified_by = isset($dataValue['modified_by']) ? $dataValue['modified_by'] : 0;
                        $tableofficeNothiMasterPermission->save($entity);

                        $tableNothiMasterPermission->updateAll(['mig_status' => 1], ['id' => $dataValue['id']]);
                    }
                    $con->commit();
                }
            } catch (\Exception $ex) {
                $error[$value['office_id']][] = $ex->getMessage();
                $con->rollback();
                continue;
            }
        }
        if (!empty($error)) {
            foreach ($error as $e_k => $e_v) {
                if (is_array($e_v)) {
                    foreach ($e_v as $e_v_k => $e_v_v) {
                        $this->out('Office Id: ' . $e_k . ' msg: ' . $e_v_v);
                    }
                }
            }
        }
    }

    public function entryOtherOfficeNothiCurrentUser()
    {
        set_time_limit(0);
        ini_set('memory_limit', -1);
        $tableNothiMasterPermission = TableRegistry::get('NothiMasterCurrentUsers');
        $getAllOffices = $tableNothiMasterPermission->find()->select(['nothi_office', 'office_id', 'nothi_part_no'])
            ->where(['nothi_office <> office_id', 'nothi_office >' => 0])->group(['nothi_office', 'office_id', 'nothi_part_no'])->toArray();

        $con = ConnectionManager::get('default');
        $nothi = [];
        $error = [];
        if (!empty($getAllOffices)) {
            foreach ($getAllOffices as $key => $value) {
                try {
                    $data = $tableNothiMasterPermission->find()->select()
                        ->where(['nothi_office' => $value['nothi_office'], 'nothi_part_no' => $value['nothi_part_no']])->hydrate(false)->toArray();
                    if (!empty($data)) {
                        $nothi[$value['office_id']][$value['nothi_office']][$value['nothi_part_no']] = $data;
                    }
                } catch (\Exception $ex) {
                    $error[$value['nothi_office']][] = $ex->getMessage();
                    $con->rollback();
                    continue;
                }
            }
        }

        foreach ($nothi as $key => $value) {
            try {
                $this->switchOffice($key, "currentOffice");
                TableRegistry::remove('nothi_master_current_users_mig');
                $tableofficeNothiMasterPermission = TableRegistry::get('nothi_master_current_users_mig');
                foreach ($value as $nothi_office => $part_array) {
                    $con->begin();
                    foreach ($part_array as $part_no => $part_item) {
                        $tableofficeNothiMasterPermission->deleteAll(['office_id' => $key, 'nothi_office' => $nothi_office, 'nothi_part_no' => $part_no]);
                        foreach ($part_item as $dataKey => $dataValue) {
                            $entity = $tableofficeNothiMasterPermission->newEntity();
                            $entity->nothi_master_id = $dataValue['nothi_master_id'];
                            $entity->nothi_part_no = $dataValue['nothi_part_no'];
                            $entity->nothi_office = $dataValue['nothi_office'];
                            $entity->office_id = $dataValue['office_id'];
                            $entity->office_unit_id = $dataValue['office_unit_id'];
                            $entity->office_unit_organogram_id = $dataValue['office_unit_organogram_id'];
                            $entity->is_archive = $dataValue['is_archive'];
                            $entity->is_finished = $dataValue['is_finished'];
                            $entity->employee_id = $dataValue['employee_id'];
                            $entity->view_status = $dataValue['view_status'];
                            $entity->issue_date = $dataValue['issue_date'];
                            $entity->forward_date = $dataValue['forward_date'];
                            $entity->is_new = $dataValue['is_new'];
                            $entity->priority = !empty($dataValue['priority']) ? $dataValue['priority'] : 0;
                            $entity->created = empty($dataValue['created']) ? date("Y-m-d H:i:s") : $dataValue['created'];
                            $entity->created_by = isset($dataValue['created_by']) ? $dataValue['created_by'] : 0;
                            $entity->modified = isset($dataValue['modified']) ? $dataValue['modified'] : date("Y-m-d H:i:s");
                            $entity->modified_by = isset($dataValue['modified_by']) ? $dataValue['modified_by'] : 0;
                            $tableofficeNothiMasterPermission->save($entity);
                        }
                    }
                    $con->commit();
                }
            } catch (\Exception $ex) {
                $this->out('Other Office Current User (' . $key . '): ' . $ex->getMessage());
                continue;
            }
        }
    }

    public static function emailValidate($email)
    {
        return (boolean)filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    public function performaceOfficeNisponnoPotrojariInternalExternalUpdate($date_start = '')
    {
        ini_set('memory_limit', -1);
        set_time_limit(0);

        $office_domains = TableRegistry::get('OfficeDomains');
        $all_offices = $office_domains->getOfficesData();

        $performanceOfficeTable = TableRegistry::get('PerformanceOffices');


        foreach ($all_offices as $office_id => $office_name) {

            $period = [];
            $office_record = $performanceOfficeTable->getLastUpdateTime($office_id);
            if (empty($office_record)) {
                continue;
            }
            if (!empty($date_start)) {
                $begin = new \DateTime($date_start);
            } else {
                $begin = new \DateTime(date('Y-m-d', strtotime(date('Y-m-d') . ' -1 day')));
            }
            if (!empty($office_record)) {
                $end = ($office_record['record_date']);
            }

            for ($i = $begin; $begin <= $end; $i->modify('+1 day')) {
                $period [] = $i->format("Y-m-d");
            }
            if (!empty($period)) {
                try {
                    $this->switchOffice($office_id, 'OfficeDB');
                    TableRegistry::remove('Potrojari');
                    TableRegistry::remove('PotrojariReceiver');
                    TableRegistry::remove('PotrojariOnulipi');
                    foreach ($period as $dt) {
                        $perfoarmanceOfficeData = $performanceOfficeTable->getOfficeData($office_id, [$dt, $dt])->first();
                        if (empty($perfoarmanceOfficeData)) {
                            continue;
                        }
                        $nisponnoByPotrojari = $perfoarmanceOfficeData['nisponnopotrojari'];
                        if ($nisponnoByPotrojari == 0) {
                            continue;
                        }
                        $potrojari_internal = TableRegistry::get('Potrojari')->allInternalPotrojariCountForMigration($office_id, 0, 0, [$dt, $dt]);
                        if ($potrojari_internal > $nisponnoByPotrojari) {
                            $potrojari_internal = $nisponnoByPotrojari;
                        }
                        $potrojari_external = $nisponnoByPotrojari - $potrojari_internal;
                        $performanceOfficeTable->updateAll(['potrojari_nisponno_internal' => $potrojari_internal, 'potrojari_nisponno_external' => $potrojari_external], ['office_id' => $office_id, 'record_date' => $dt]);
                    }
                    $this->out($office_id . ' Done.');
                } catch (\Exception $ex) {
                    $this->out($ex->getMessage());
                }
            }
        }
    }

    public function individualPerformanceDesignationUpdate($designation_id = 0, $date_start = '')
    {
//        set_time_limit(0);
//        ini_set('memory_limit', -1);

        $employeeOfficesTable = TableRegistry::get('EmployeeOffices');

        $performanceDesignationTable = TableRegistry::get('PerformanceDesignations');
        $reportsTable = TableRegistry::get('Reports');
        $MinistriesTable = TableRegistry::get('OfficeMinistries');
        $LayersTable = TableRegistry::get('OfficeLayers');
        $OfficesTable = TableRegistry::get('Offices');
        $UnitsTable = TableRegistry::get('OfficeUnits');
        $OriginsTable = TableRegistry::get('OfficeOrigins');


        if (empty($designation_id)) {
            $this->out('empty designation');
            return;
        }

        $designation_record = $performanceDesignationTable->getLastUpdateTime($designation_id);
        if (!empty($date_start)) {
            $begin = new \DateTime($date_start);
        } else if (!empty($designation_record)) {
            $begin = ($designation_record['record_date']);
        } else {
            $begin = new \DateTime(date('Y-m-d', strtotime(date('Y-m-d') . ' -1 week')));
        }
        $end = new \DateTime(date('Y-m-d'));
        for ($i = $begin; $begin < $end; $i->modify('+1 day')) {
            $period [] = $i->format("Y-m-d");
        }
        if (!empty($period)) {
            $designation_related_data = $employeeOfficesTable->getDesignationInfo($designation_id);
            try {
                $this->switchOffice($designation_related_data['office_id'], 'OfficeDB');
                foreach ($period as $dt) {
                    $has_record = $performanceDesignationTable->checkUpdate($designation_id, $dt);
                    if ($has_record > 0) {
                        continue;
                    }
                    $date = $dt;
//                        pr($designation_related_data);die;

                    $returnSummary = $reportsTable->newPerformanceReport($designation_related_data['office_id'],
                        $designation_related_data['office_unit_id'], $designation_id,
                        ['date_start' => $date, 'date_end' => $date]);
                    $performance_designatons_entity = $performanceDesignationTable->newEntity();

                    $performance_designatons_entity->inbox = $returnSummary['totalInbox'];
                    $performance_designatons_entity->outbox = $returnSummary['totalOutbox'];
                    $performance_designatons_entity->nothijat = $returnSummary['totalNothijato'];
                    $performance_designatons_entity->nothivukto = $returnSummary['totalNothivukto'];
                    $performance_designatons_entity->nisponnodak = $returnSummary['totalNisponnoDak'];
                    $performance_designatons_entity->onisponnodak = $returnSummary['totalONisponnoDak'];

                    if ($returnSummary['totalSouddog'] < 0) {
                        $returnSummary['totalSouddog'] = 0;
                    }
                    $performance_designatons_entity->selfnote = $returnSummary['totalSouddog'];
                    $performance_designatons_entity->daksohonote = ($returnSummary['totalNothivukto']
                    < $returnSummary['totalDaksohoNote'] ? $returnSummary['totalNothivukto']
                        : $returnSummary['totalDaksohoNote']);
                    $performance_designatons_entity->potrojari = $returnSummary['totalPotrojari'];
                    $performance_designatons_entity->nisponnopotrojari = ($returnSummary['totalPotrojari']
                    < $returnSummary['totalNisponnoPotrojari'] ? $returnSummary['totalPotrojari']
                        : $returnSummary['totalNisponnoPotrojari']);
                    $performance_designatons_entity->nisponnonote = $returnSummary['totalNisponnoNote'];
                    $performance_designatons_entity->onisponnonote = $returnSummary['totalONisponnoNote'];

                    $performance_designatons_entity->nisponno = $returnSummary['totalNisponno'];
                    $performance_designatons_entity->onisponno = $returnSummary['totalONisponno'];

                    if (!($office_related_data = Cache::read('office_related_data_' . $designation_related_data['office_id']))) {
                        $office_related_data = $OfficesTable->getAll(['id' => $designation_related_data['office_id']],
                            ['office_ministry_id', 'office_layer_id', 'office_origin_id',
                                'office_name_bng'])->first();
                        Cache::write('office_related_data_' . $designation_related_data['office_id'], $office_related_data);
                    }

                    $performance_designatons_entity->office_id = $designation_related_data['office_id'];
                    $performance_designatons_entity->ministry_id = $office_related_data['office_ministry_id'];
                    $performance_designatons_entity->layer_id = $office_related_data['office_layer_id'];
                    $performance_designatons_entity->origin_id = $office_related_data['office_origin_id'];
                    $performance_designatons_entity->office_name = $office_related_data['office_name_bng'];

                    if (!($unit_info = Cache::read('unit_info_' . $designation_related_data['office_unit_id']))) {
                        $unit_info = $UnitsTable->getBanglaName($designation_related_data['office_unit_id']);
                        Cache::write('unit_info_' . $designation_related_data['office_unit_id'], $unit_info);
                    }
                    $performance_designatons_entity->unit_name = $unit_info['unit_name_bng'];
                    $performance_designatons_entity->unit_id = $designation_related_data['office_unit_id'];
                    $performance_designatons_entity->designation_id = $designation_id;
                    $performance_designatons_entity->designation_name = $designation_related_data['designation'];

                    if (!($ministry_info = Cache::read('ministry_info_' . $office_related_data['office_ministry_id']))) {
                        $ministry_info = $MinistriesTable->getBanglaName($office_related_data['office_ministry_id']);
                        Cache::write('ministry_info_' . $office_related_data['office_ministry_id'], $ministry_info);
                    }

                    $performance_designatons_entity->ministry_name = $ministry_info['name_bng'];

                    if (!($layer_info = Cache::read('layer_info_' . $office_related_data['office_layer_id']))) {
                        $layer_info = $LayersTable->getBanglaName($office_related_data['office_layer_id']);
                        Cache::write('layer_info_' . $office_related_data['office_layer_id'], $layer_info);
                    }

                    $performance_designatons_entity->layer_name = $layer_info['layer_name_bng'];

                    if (!($origin_info = Cache::read('origin_info_' . $office_related_data['office_origin_id']))) {
                        $origin_info = $OriginsTable->getBanglaName($office_related_data['office_origin_id']);
                        Cache::write('origin_info_' . $office_related_data['office_origin_id'], $origin_info);
                    }

                    $performance_designatons_entity->origin_name = $origin_info['office_name_bng'];

                    $performance_designatons_entity->status = 1;
                    $performance_designatons_entity->record_date = $date;

                    $performanceDesignationTable->save($performance_designatons_entity);
                }
                $dataSourceObject = ConnectionManager::get('default');
                $dataSourceObject->disconnect();
                ConnectionManager::drop('default');
            } catch (\Exception $ex) {
                $this->out("Error({$designation_id}): " . $ex->getMessage());
            }
        }
        $this->out('Designation Done: ' . $designation_id);
    }

    public function saveDesignationRequestForCron()
    {
        if (Live == 1) {
            $employeeOfficesTable = TableRegistry::get('EmployeeOffices');
            $cronRequestTable = TableRegistry::get('CronRequest');
            $all_designations = $employeeOfficesTable->getAllDesignationID()->toArray();
            foreach ($all_designations as $id => $name) {
                $command = ROOT . "/bin/cake cronjob individualPerformanceDesignationUpdate {$id}> /dev/null 2>&1 &";
                $cronRequestTable->saveData($command);
            }
        }

    }

    public function deleteRequestForCron()
    {
        if (Live == 1) {
//            $this->CronRequest->query('TRUNCATE TABLE cron_request;');
            TableRegistry::get('CronRequest')->deleteAll(['1 = 1']);
        }

    }

    public function saveOfficeRequestForCron()
    {
        if (Live == 1) {
            $cronRequestTable = TableRegistry::get('CronRequest');
            $office_domains = TableRegistry::get('OfficeDomains');
            $all_offices = $office_domains->getOfficesData();
            foreach ($all_offices as $id => $name) {
                $command = ROOT . "/bin/cake cronjob performaceIndividualOfficeUpdate {$id}> /dev/null 2>&1 &";
                $cronRequestTable->saveData($command);
            }
        }

    }



    public function runThisCommand($command = '')
    {
        try {
            $command = str_replace("/nothi_new", "", $command);
            exec($command);
        } catch (\Exception $ex) {
            $this->out($ex->getMessage());
        }

    }

    public function performaceIndividualOfficeUpdate($office_id = 0, $date_start = '')
    {
        ini_set('memory_limit', -1);
        set_time_limit(0);
        if (empty($office_id)) {
            return;
        }

        $officesTable = TableRegistry::get('Offices');

        $performanceOfficeTable = TableRegistry::get('PerformanceOffices');
        $reportsTable = TableRegistry::get('Reports');
        $MinistriesTable = TableRegistry::get('OfficeMinistries');
        $LayersTable = TableRegistry::get('OfficeLayers');
        $OriginsTable = TableRegistry::get('OfficeOrigins');
        $performance_designationTable = TableRegistry::get('PerformanceDesignations');
        $tbl_CronRequest = TableRegistry::get('CronRequest');

        $office_record = $performanceOfficeTable->getLastUpdateTime($office_id);

        if (!empty($date_start)) {
            $begin = new \DateTime($date_start);
        } else if (!empty($office_record)) {
            $begin = ($office_record['record_date']);
        } else {
            $begin = new \DateTime(date('Y-m-d', strtotime(date('Y-m-d') . ' -1 week')));
        }
        $end = new \DateTime(date('Y-m-d'));
        $last_week_date = new \DateTime(date('Y-m-d', strtotime(date('Y-m-d') . ' -1 week')));
        if(!empty($begin) && !empty($last_week_date) && $begin < $last_week_date){
            $begin =  $last_week_date;
        }
        for ($i = $begin; $begin < $end; $i->modify('+1 day')) {
            $period [] = $i->format("Y-m-d");
        }
        if (!empty($period)) {
            try {
                $this->switchOffice($office_id, 'OfficeDB');
                foreach ($period as $dt) {
                    $has_record = $performanceOfficeTable->find()->where(['record_date' => $dt, 'office_id' => $office_id])->count();
                    if ($has_record > 0) {
                        continue;
                    }
                    $date = $dt;

                    if (!($office_related_data = Cache::read('office_related_data_' . $office_id))) {
                        $office_related_data = $officesTable->getAll(['id' => $office_id],
                            ['office_ministry_id', 'office_layer_id', 'office_origin_id', 'office_name_bng'])->first();
                        Cache::write('office_related_data_' . $office_id, $office_related_data);
                    }
                    // Collect From Performance Unit Table
                    $from_designation_report = true;
                    $all_designation = TableRegistry::get('EmployeeOffices')->getAllDesignationByOfficeOrUnitID($office_id,0,1)->distinct(['EmployeeOffices.office_unit_organogram_id'])->toArray();
                    $condition = [
                        'totalInbox' => 'SUM(inbox)', 'totalOutbox' => 'SUM(outbox)', 'totalNothijato' => 'SUM(nothijat)',
                        'totalNothivukto' => 'SUM(nothivukto)', 'totalNisponnoDak' => 'SUM(nisponnodak)', 'totalpotrojariall' => 'SUM(potrojari)',
                        'totalSouddog' => 'SUM(selfnote)', 'OnisponnoDak' => 'SUM(onisponnodak)', 'OnisponnoNote' => 'SUM(onisponnonote)',
                        'totalDaksohoNote' => 'SUM(daksohonote)', 'totalNisponnoPotrojari' => 'SUM(nisponnopotrojari)',
                        'totalNisponnoNote' => 'SUM(nisponnonote)',
                        'totalNisponno' => 'SUM(nisponno)', 'totalONisponno' => 'SUM(onisponno)',
                    ];
                    if (!empty($all_designation)) {
//                          $hasDataInPerformanceUnit = TableRegistry::get('PerformanceUnits')->getAllUnitData($all_unit,[$date,$date])->select($condition)->group(['unit_id'])->toArray();
                        // first check all designation has records
                        $designation_ids = array_values($all_designation);
                        $count_designations = count($designation_ids);
                        $all_designation_has_report = $performance_designationTable->getDesignationData($designation_ids,[$date, $date])->count();
                        if($all_designation_has_report < $count_designations){
                            $this->out('Office '.$office_id.' All designation report not recorded. Found: '.$all_designation_has_report.' Required: '.$count_designations.'For Date:' .$date);
                            // generate again office wise performance
                            $command = ROOT . "/bin/cake cronjob updatePerformanceTables {$office_id} > /dev/null 2>&1 &";
                            $tbl_CronRequest->saveData($command);
                            return;
                        }

                        $hasDataInPerformanceUnit = TableRegistry::get('PerformanceDesignations')->getOfficeData([$office_id], [$date, $date])->select($condition)->group(['office_id'])->toArray();
                        if (!empty($hasDataInPerformanceUnit)) {
                            $returnSummary['totalInbox'] = 0;
                            $returnSummary['totalOutbox'] = 0;
                            $returnSummary['totalNothijato'] = 0;
                            $returnSummary['totalNothivukto'] = 0;
                            $returnSummary['totalNisponnoDak'] = 0;
                            $returnSummary['totalONisponnoDak'] = 0;
                            $returnSummary['totalPotrojari'] = 0;
                            $returnSummary['totalSouddog'] = 0;
                            $returnSummary['totalDaksohoNote'] = 0;
                            $returnSummary['totalNisponnoNote'] = 0;
                            $returnSummary['totalNisponnoPotrojari'] = 0;
                            $returnSummary['totalONisponnoNote'] = 0;
                            $returnSummary['totalNisponno'] = 0;
                            $returnSummary['totalONisponno'] = 0;
                            foreach ($hasDataInPerformanceUnit as $val_form_designation) {
                                $returnSummary['totalONisponnoDak'] += $val_form_designation['OnisponnoDak'];
                                $returnSummary['totalONisponnoNote'] += $val_form_designation['OnisponnoNote'];
                                $returnSummary['totalPotrojari'] += $val_form_designation['totalpotrojariall'];
                                $returnSummary['totalSouddog'] += $val_form_designation['totalSouddog'];
                                $returnSummary['totalDaksohoNote'] += $val_form_designation['totalDaksohoNote'];
                                $returnSummary['totalNisponnoNote'] += $val_form_designation['totalNisponnoNote'];
                                $returnSummary['totalNisponnoPotrojari'] += $val_form_designation['totalNisponnoPotrojari'];
                                $returnSummary['totalNisponno'] += $val_form_designation['totalNisponno'];
                                $returnSummary['totalONisponno'] += $val_form_designation['totalONisponno'];
                            }
                            TableRegistry::remove('DakMovements');
                            $dakMovmentsTable = TableRegistry::get('DakMovements');
                            $returnSummary['totalInbox'] = $dakMovmentsTable->countAllDakforOffice($office_id, 0,
                                0, 'Inbox', [$date, $date]);
                            $returnSummary['totalOutbox'] = $dakMovmentsTable->countAllDakforOffice($office_id, 0,
                                0, 'Outbox', [$date, $date]);
                            $returnSummary['totalNothijato'] = $dakMovmentsTable->countAllDakforOffice($office_id, 0,
                                0, 'Nothijato', [$date, $date]);
                            $returnSummary['totalNothivukto'] = $dakMovmentsTable->countAllDakforOffice($office_id, 0,
                                0, 'Nothivukto', [$date, $date]);
//                               $returnSummary['totalONisponnoDak'] = TableRegistry::get('DakUsers')->getOnisponnoDak($office_id);
                            $returnSummary['totalNisponnoDak'] = $returnSummary['totalNothijato'] + $returnSummary['totalNothivukto'];
//                              $returnSummary['totalONisponnoNote'] = TableRegistry::get('Dashboard')->getOnisponnoNoteCount($office_id,0,0, [$date,$date]);
                            $returnSummary['internal_potrojari'] = TableRegistry::get('NisponnoRecords')->generatePotrojariNisponnoInternalExternalCount($office_id, 0, 0, [$date, $date]);
                            $returnSummary['external_potrojari'] = $returnSummary['totalNisponnoPotrojari'] - $returnSummary['internal_potrojari'];
                            $unassignedEmployees = TableRegistry::get('EmployeeOffices')->getUnassingedEmolyeesDesignation($office_id);
                            $returnSummary['unassignedDesignation'] = count($unassignedEmployees);
                            if ($returnSummary['unassignedDesignation'] == 0) {
                                $returnSummary['unassignedPendingDak'] = $returnSummary['unassignedPendingNote'] = 0;
                            } else {
                                $returnSummary['unassignedPendingDak'] = TableRegistry::get('DakUsers')->getOnisponnoDakByMultipleDesignations($unassignedEmployees, $date);
                                $returnSummary['unassignedPendingNote'] = TableRegistry::get('Dashboard')->getOnisponnoNoteCountByMultipleDesignations($unassignedEmployees, [$date, $date]);
                            }
                        } else {
                            $from_designation_report = false;
                        }
                    } else {
                        $from_designation_report = false;
                    }

                    if ($from_designation_report == false) {
                        $returnSummary = $reportsTable->newPerformanceReport($office_id,
                            0, 0, ['date_start' => $date, 'date_end' => $date]);
                    }

                    $performance_offices_entity = $performanceOfficeTable->newEntity();

                    $performance_offices_entity->inbox = $returnSummary['totalInbox'];
                    $performance_offices_entity->outbox = $returnSummary['totalOutbox'];
                    $performance_offices_entity->nothijat = $returnSummary['totalNothijato'];
                    $performance_offices_entity->nothivukto = $returnSummary['totalNothivukto'];
                    $performance_offices_entity->nisponnodak = $returnSummary['totalNisponnoDak'];
                    $performance_offices_entity->onisponnodak = $returnSummary['totalONisponnoDak'];

                    if ($returnSummary['totalSouddog'] < 0) {
                        $returnSummary['totalSouddog'] = 0;
                    }
                    $performance_offices_entity->potrojari = $returnSummary['totalPotrojari'];
                    $performance_offices_entity->selfnote = $returnSummary['totalSouddog'];
                    $performance_offices_entity->daksohonote = $returnSummary['totalDaksohoNote'];
                    $performance_offices_entity->nisponnopotrojari = $returnSummary['totalNisponnoPotrojari'];
                    $performance_offices_entity->nisponnonote = $returnSummary['totalNisponnoNote'];
                    $performance_offices_entity->onisponnonote = $returnSummary['totalONisponnoNote'];

                    $performance_offices_entity->nisponno = $returnSummary['totalNisponno'];
                    $performance_offices_entity->onisponno = $returnSummary['totalONisponno'];

//                        $internal_potrojari = TableRegistry::get('NisponnoRecords')->generatePotrojariNisponnoInternalExternalCount($office_id,0,0,[$date,$date]);
//                        $external_potrojari = $returnSummary['totalNisponnoPotrojari'] - $internal_potrojari;
                    $internal_potrojari = $returnSummary['internal_potrojari'];
//                        $external_potrojari = $returnSummary['totalNisponnoPotrojari'] - $internal_potrojari;
                    $external_potrojari = $returnSummary['external_potrojari'];
                    $performance_offices_entity->potrojari_nisponno_internal = ($external_potrojari < 0) ? $returnSummary['totalNisponnoPotrojari'] : $internal_potrojari;
                    $performance_offices_entity->potrojari_nisponno_external = ($external_potrojari < 0) ? 0 : $external_potrojari;

                    $performance_offices_entity->unassigned_designation = $returnSummary['unassignedDesignation'];
                    $performance_offices_entity->unassigned_pending_dak = $returnSummary['unassignedPendingDak'];
                    $performance_offices_entity->unassigned_pending_note = $returnSummary['unassignedPendingNote'];
                    $performance_offices_entity->potrojari_nisponno_internal = ($external_potrojari < 0) ? $returnSummary['totalNisponnoPotrojari'] : $internal_potrojari;
                    $performance_offices_entity->potrojari_nisponno_external = ($external_potrojari < 0) ? 0 : $external_potrojari;

                    $performance_offices_entity->office_id = $office_id;
                    $performance_offices_entity->ministry_id = $office_related_data['office_ministry_id'];
                    $performance_offices_entity->layer_id = $office_related_data['office_layer_id'];
                    $performance_offices_entity->origin_id = $office_related_data['office_origin_id'];
                    $performance_offices_entity->office_name = $office_related_data['office_name_bng'];

                    if (!($ministry_info = Cache::read('ministry_info_' . $office_related_data['office_ministry_id']))) {
                        $ministry_info = $MinistriesTable->getBanglaName($office_related_data['office_ministry_id']);
                        Cache::write('ministry_info_' . $office_related_data['office_ministry_id'], $ministry_info);
                    }

                    $performance_offices_entity->ministry_name = $ministry_info['name_bng'];

                    if (!($layer_info = Cache::read('layer_info_' . $office_related_data['office_layer_id']))) {
                        $layer_info = $LayersTable->getBanglaName($office_related_data['office_layer_id']);
                        Cache::write('layer_info_' . $office_related_data['office_layer_id'], $layer_info);
                    }

                    $performance_offices_entity->layer_name = $layer_info['layer_name_bng'];

                    if (!($origin_info = Cache::read('origin_info_' . $office_related_data['office_origin_id']))) {
                        $origin_info = $OriginsTable->getBanglaName($office_related_data['office_origin_id']);
                        Cache::write('origin_info_' . $office_related_data['office_origin_id'], $origin_info);
                    }

                    $performance_offices_entity->origin_name = $origin_info['office_name_bng'];

                    $performance_offices_entity->status = 1;
                    $performance_offices_entity->record_date = $date;
//                            pr($performance_offices_entity);die;

                    $performanceOfficeTable->save($performance_offices_entity);
                }
                $this->monitorIndividualOfficeUpdate($office_id);
                $this->deleteMultipleRowsOfOffice($office_id, !empty($period[0]) ? $period[0] : '');

                $dataSourceObject = ConnectionManager::get('default');
                $dataSourceObject->disconnect();
                ConnectionManager::drop('default');

            } catch (\Exception $ex) {
                $this->out($ex->getMessage());
            }
        }
    }

    public function individualPerformanceUnitUpdate($unit_id = 0, $date_start = '')
    {
        set_time_limit(0);
        ini_set('memory_limit', -1);
        if (empty($unit_id))
            return;

        $unitsTable = TableRegistry::get('OfficeUnits');
        $employee_offices = TableRegistry::get('EmployeeOffices');
        $performanceUnitTable = TableRegistry::get('PerformanceUnits');
        $reportsTable = TableRegistry::get('Reports');
        $MinistriesTable = TableRegistry::get('OfficeMinistries');
        $LayersTable = TableRegistry::get('OfficeLayers');
        $OfficesTable = TableRegistry::get('Offices');
        $UnitOriginsTable = TableRegistry::get('OfficeOriginUnits');
        $performance_designationTable = TableRegistry::get('PerformanceDesignations');


        $unit_record = $performanceUnitTable->getLastUpdateTime($unit_id);
        if (!empty($date_start)) {
            $begin = new \DateTime($date_start);
        } else if (!empty($unit_record)) {
            $begin = ($unit_record['record_date']);
        } else {
            $begin = new \DateTime(date('Y-m-d', strtotime(date('Y-m-d') . ' -1 week')));
        }
        $end = new \DateTime(date('Y-m-d'));

        for ($i = $begin; $begin < $end; $i->modify('+1 day')) {
            $period [] = $i->format("Y-m-d");
        }
        if (!empty($period)) {
            try {
                $unit_related_data = $unitsTable->getAll(['id' => $unit_id],
                    ['office_ministry_id', 'office_layer_id', 'office_origin_unit_id', 'office_id', 'unit_name_bng'])->first();
                $this->switchOffice($unit_related_data['office_id'], 'OfficeDB');
                foreach ($period as $dt) {
                    $has_record = $performanceUnitTable->find()->where(['record_date' => $dt, 'unit_id' => $unit_id])->count();
                    if ($has_record > 0) {
                        continue;
                    }
                    $date = $dt;
//                        pr($unit_related_data['office_id']);die;
                    // Collect From Designation Table
                    $from_designation_report = true;
                    $all_designation = $employee_offices->getAllDesignationByOfficeOrUnitID($unit_related_data['office_id'],
                        $unit_id)->distinct(['EmployeeOffices.office_unit_organogram_id'])->toArray();
                    $condition = [
                        'totalInbox' => 'SUM(inbox)', 'totalOutbox' => 'SUM(outbox)', 'totalNothijato' => 'SUM(nothijat)',
                        'totalNothivukto' => 'SUM(nothivukto)', 'totalNisponnoDak' => 'SUM(nisponnodak)', 'totalpotrojariall' => 'SUM(potrojari)',
                        'totalSouddog' => 'SUM(selfnote)',
                        'totalDaksohoNote' => 'SUM(daksohonote)', 'totalNisponnoPotrojari' => 'SUM(nisponnopotrojari)',
                        'totalNisponnoNote' => 'SUM(nisponnonote)',
                        'totalNisponno' => 'SUM(nisponno)', 'totalONisponno' => 'SUM(onisponno)',
                        'totalID' => 'count(id)', 'designation_id'
                    ];
                    if (!empty($all_designation)) {
                        $hasDataInPerformanceDesignation = $performance_designationTable->getDesignationData($all_designation, [$date, $date])->select($condition)->group(['designation_id'])->toArray();
                        if (!empty($hasDataInPerformanceDesignation)) {
                            $returnSummary['totalInbox'] = 0;
                            $returnSummary['totalOutbox'] = 0;
                            $returnSummary['totalNothijato'] = 0;
                            $returnSummary['totalNothivukto'] = 0;
                            $returnSummary['totalNisponnoDak'] = 0;
                            $returnSummary['totalONisponnoDak'] = 0;
                            $returnSummary['totalPotrojari'] = 0;
                            $returnSummary['totalSouddog'] = 0;
                            $returnSummary['totalDaksohoNote'] = 0;
                            $returnSummary['totalNisponnoNote'] = 0;
                            $returnSummary['totalNisponnoPotrojari'] = 0;
                            $returnSummary['totalONisponnoNote'] = 0;
                            $returnSummary['totalNisponno'] = 0;
                            $returnSummary['totalONisponno'] = 0;
                            foreach ($hasDataInPerformanceDesignation as $val_form_designation) {
                                $returnSummary['totalInbox'] += $val_form_designation['totalInbox'];
                                $returnSummary['totalOutbox'] += $val_form_designation['totalOutbox'];
                                $returnSummary['totalNothijato'] += $val_form_designation['totalNothijato'];
                                $returnSummary['totalNothivukto'] += $val_form_designation['totalNothivukto'];
                                $returnSummary['totalNisponnoDak'] += $val_form_designation['totalNisponnoDak'];
                                $returnSummary['totalONisponnoDak'] += TableRegistry::get('DakUsers')->getOnisponnoDak($unit_related_data['office_id'], $unit_id);
                                $returnSummary['totalPotrojari'] += $val_form_designation['totalpotrojariall'];
                                $returnSummary['totalSouddog'] += $val_form_designation['totalSouddog'];
                                $returnSummary['totalDaksohoNote'] += $val_form_designation['totalDaksohoNote'];
                                $returnSummary['totalNisponnoNote'] += $val_form_designation['totalNisponnoNote'];
                                $returnSummary['totalNisponnoPotrojari'] += $val_form_designation['totalNisponnoPotrojari'];
                                $returnSummary['totalONisponnoNote'] += TableRegistry::get('NothiMasterCurrentUsers')->getAllNothiPartNo($unit_related_data['office_id'], $unit_id, 0, [$date, $date])->count();
                                $returnSummary['totalNisponno'] += $val_form_designation['totalNisponno'];
                                $returnSummary['totalONisponno'] += $val_form_designation['totalONisponno'];
                            }
                        } else {
                            $from_designation_report = false;
                        }
                    } else {
                        $from_designation_report = false;
                    }

                    if ($from_designation_report == false) {
                        $returnSummary = $reportsTable->newPerformanceReport($unit_related_data['office_id'],
                            $unit_id, 0, ['date_start' => $date, 'date_end' => $date]);
                    }

                    $performance_units_entity = $performanceUnitTable->newEntity();

                    $performance_units_entity->inbox = $returnSummary['totalInbox'];
                    $performance_units_entity->outbox = $returnSummary['totalOutbox'];
                    $performance_units_entity->nothijat = $returnSummary['totalNothijato'];
                    $performance_units_entity->nothivukto = $returnSummary['totalNothivukto'];
                    $performance_units_entity->nisponnodak = $returnSummary['totalNisponnoDak'];
                    $performance_units_entity->onisponnodak = $returnSummary['totalONisponnoDak'];

                    $performance_units_entity->potrojari = $returnSummary['totalPotrojari'];
                    if ($returnSummary['totalSouddog'] < 0) {
                        $returnSummary['totalSouddog'] = 0;
                    }
                    $performance_units_entity->selfnote = $returnSummary['totalSouddog'];
                    $performance_units_entity->daksohonote = ($returnSummary['totalNothivukto']
                    < $returnSummary['totalDaksohoNote'] ? $returnSummary['totalNothivukto']
                        : $returnSummary['totalDaksohoNote']);
                    $performance_units_entity->nisponnopotrojari = ($returnSummary['totalPotrojari']
                    < $returnSummary['totalNisponnoPotrojari'] ? $returnSummary['totalPotrojari']
                        : $returnSummary['totalNisponnoPotrojari']);
                    $performance_units_entity->nisponnonote = $returnSummary['totalNisponnoNote'];
                    $performance_units_entity->onisponnonote = $returnSummary['totalONisponnoNote'];

                    $performance_units_entity->nisponno = $returnSummary['totalNisponno'];
                    $performance_units_entity->onisponno = $returnSummary['totalONisponno'];

                    $performance_units_entity->office_id = $unit_related_data['office_id'];
                    $performance_units_entity->unit_id = $unit_id;
                    $performance_units_entity->ministry_id = $unit_related_data['office_ministry_id'];
                    $performance_units_entity->layer_id = $unit_related_data['office_layer_id'];
                    $performance_units_entity->origin_id = $unit_related_data['office_origin_unit_id'];
                    $performance_units_entity->unit_name = $unit_related_data['unit_name_bng'];

                    if (!($office_info = Cache::read('office_info_' . $unit_related_data['office_id']))) {
                        $office_info = $OfficesTable->getBanglaName($unit_related_data['office_id']);
                        Cache::write('office_info_' . $unit_related_data['office_id'], $office_info);
                    }

                    $performance_units_entity->office_name = $office_info['office_name_bng'];

                    if (!($ministry_info = Cache::read('ministry_info_' . $unit_related_data['office_ministry_id']))) {
                        $ministry_info = $MinistriesTable->getBanglaName($unit_related_data['office_ministry_id']);
                        Cache::write('ministry_info_' . $unit_related_data['office_ministry_id'], $ministry_info);
                    }

                    $performance_units_entity->ministry_name = $ministry_info['name_bng'];

                    if (!($layer_info = Cache::read('layer_info_' . $unit_related_data['office_layer_id']))) {
                        $layer_info = $LayersTable->getBanglaName($unit_related_data['office_layer_id']);
                        Cache::write('layer_info_' . $unit_related_data['office_layer_id'], $layer_info);
                    }

                    $performance_units_entity->layer_name = $layer_info['layer_name_bng'];

                    if (!($origin_info = Cache::read('origin_info_' . $unit_related_data['office_origin_unit_id']))) {
                        $origin_info = $UnitOriginsTable->getBanglaName($unit_related_data['office_origin_unit_id']);
                        Cache::write('origin_info_' . $unit_related_data['office_origin_unit_id'], $origin_info);
                    }

                    $performance_units_entity->origin_name = $origin_info['unit_name_bng'];

                    $performance_units_entity->status = 1;
                    $performance_units_entity->record_date = $date;

                    $performanceUnitTable->save($performance_units_entity);
                }
                $dataSourceObject = ConnectionManager::get('default');
                $dataSourceObject->disconnect();
                ConnectionManager::drop('default');
            } catch (\Exception $ex) {
                $this->out($ex->getMessage());
            }
        }
    }

    public function performanceDesignationUpdateByOfficeID($office_id, $date_start = '')
    {
        set_time_limit(0);
        ini_set('memory_limit', -1);
        $this->switchOffice($office_id, 'OfficeDB');


        $employeeOfficesTable = TableRegistry::get('EmployeeOffices');
        $all_designations = $employeeOfficesTable->getAllDesignationID()->where(['EmployeeOffices.office_id' => $office_id])->toArray();

        $performanceDesignationTable = TableRegistry::get('PerformanceDesignations');
        $reportsTable = TableRegistry::get('Reports');
        $MinistriesTable = TableRegistry::get('OfficeMinistries');
        $LayersTable = TableRegistry::get('OfficeLayers');
        $OfficesTable = TableRegistry::get('Offices');
        $UnitsTable = TableRegistry::get('OfficeUnits');
        $OriginsTable = TableRegistry::get('OfficeOrigins');


        foreach ($all_designations as $designation_id => $designation_name) {
            if (empty($designation_id)) continue;
            $period = [];
            $designation_record = $performanceDesignationTable->getLastUpdateTime($designation_id);
            if (!empty($date_start)) {
                $begin = new \DateTime($date_start);
            } else if (!empty($designation_record)) {
                $begin = ($designation_record['record_date']);
            } else {
                $begin = new \DateTime(date('Y-m-d', strtotime(date('Y-m-d') . ' -1 week')));
            }
            $end = new \DateTime(date('Y-m-d'));
            for ($i = $begin; $begin < $end; $i->modify('+1 day')) {
                $period [] = $i->format("Y-m-d");
            }
            if (!empty($period)) {
                $designation_related_data = $employeeOfficesTable->getDesignationInfo($designation_id);
                try {
                    foreach ($period as $dt) {
                        $has_record = $performanceDesignationTable->checkUpdate($designation_id, $dt);
                        if ($has_record > 0) {
                            continue;
                        }
                        $date = $dt;

//                        pr($designation_related_data);die;

                        $returnSummary = $reportsTable->newPerformanceReport($designation_related_data['office_id'],
                            $designation_related_data['office_unit_id'], $designation_id,
                            ['date_start' => $date, 'date_end' => $date]);
                        $performance_designatons_entity = $performanceDesignationTable->newEntity();

                        $performance_designatons_entity->inbox = $returnSummary['totalInbox'];
                        $performance_designatons_entity->outbox = $returnSummary['totalOutbox'];
                        $performance_designatons_entity->nothijat = $returnSummary['totalNothijato'];
                        $performance_designatons_entity->nothivukto = $returnSummary['totalNothivukto'];
                        $performance_designatons_entity->nisponnodak = $returnSummary['totalNisponnoDak'];
                        $performance_designatons_entity->onisponnodak = $returnSummary['totalONisponnoDak'];

                        if ($returnSummary['totalSouddog'] < 0) {
                            $returnSummary['totalSouddog'] = 0;
                        }
                        $performance_designatons_entity->selfnote = $returnSummary['totalSouddog'];
                        $performance_designatons_entity->daksohonote = $returnSummary['totalDaksohoNote'];
                        $performance_designatons_entity->potrojari = $returnSummary['totalPotrojari'];
                        $performance_designatons_entity->nisponnopotrojari = $returnSummary['totalNisponnoPotrojari'];
                        $performance_designatons_entity->nisponnonote = $returnSummary['totalNisponnoNote'];
                        $performance_designatons_entity->onisponnonote = $returnSummary['totalONisponnoNote'];

                        $performance_designatons_entity->nisponno = $returnSummary['totalNisponno'];
                        $performance_designatons_entity->onisponno = $returnSummary['totalONisponno'];

                        if (!($office_related_data = Cache::read('office_related_data_' . $designation_related_data['office_id']))) {
                            $office_related_data = $OfficesTable->getAll(['id' => $designation_related_data['office_id']],
                                ['office_ministry_id', 'office_layer_id', 'office_origin_id',
                                    'office_name_bng'])->first();
                            Cache::write('office_related_data_' . $designation_related_data['office_id'],
                                $office_related_data);
                        }

                        $performance_designatons_entity->office_id = $designation_related_data['office_id'];
                        $performance_designatons_entity->ministry_id = $office_related_data['office_ministry_id'];
                        $performance_designatons_entity->layer_id = $office_related_data['office_layer_id'];
                        $performance_designatons_entity->origin_id = $office_related_data['office_origin_id'];
                        $performance_designatons_entity->office_name = $office_related_data['office_name_bng'];

                        if (!($unit_info = Cache::read('unit_info_' . $designation_related_data['office_unit_id']))) {
                            $unit_info = $UnitsTable->getBanglaName($designation_related_data['office_unit_id']);
                            Cache::write('unit_info_' . $designation_related_data['office_unit_id'],
                                $unit_info);
                        }
                        $performance_designatons_entity->unit_name = $unit_info['unit_name_bng'];
                        $performance_designatons_entity->unit_id = $designation_related_data['office_unit_id'];
                        $performance_designatons_entity->designation_id = $designation_id;
                        $performance_designatons_entity->designation_name = $designation_name;

                        if (!($ministry_info = Cache::read('ministry_info_' . $office_related_data['office_ministry_id']))) {
                            $ministry_info = $MinistriesTable->getBanglaName($office_related_data['office_ministry_id']);
                            Cache::write('ministry_info_' . $office_related_data['office_ministry_id'],
                                $ministry_info);
                        }

                        $performance_designatons_entity->ministry_name = $ministry_info['name_bng'];

                        if (!($layer_info = Cache::read('layer_info_' . $office_related_data['office_layer_id']))) {
                            $layer_info = $LayersTable->getBanglaName($office_related_data['office_layer_id']);
                            Cache::write('layer_info_' . $office_related_data['office_layer_id'],
                                $layer_info);
                        }

                        $performance_designatons_entity->layer_name = $layer_info['layer_name_bng'];

                        if (!($origin_info = Cache::read('origin_info_' . $office_related_data['office_origin_id']))) {
                            $origin_info = $OriginsTable->getBanglaName($office_related_data['office_origin_id']);
                            Cache::write('origin_info_' . $office_related_data['office_origin_id'],
                                $origin_info);
                        }

                        $performance_designatons_entity->origin_name = $origin_info['office_name_bng'];

                        $performance_designatons_entity->status = 1;
                        $performance_designatons_entity->record_date = $date;

                        $performanceDesignationTable->save($performance_designatons_entity);
                    }
                    $this->updateIndividualDesignation($designation_id);
                    $this->deleteMultipleRowsOfDesignations($designation_id, !empty($period[0]) ? $period[0] : '');
                } catch (\Exception $ex) {
                    $this->out($ex->getMessage().(isset($designation_id)?(' For Designation: '.$designation_id):''));
                }
            }
        }
        $this->out('Designation Done For Office: ' . $office_id);
//          $this->runCronRequest();
    }

    public function performanceUnitUpdateByOfficeID($office_id, $date_start = '')
    {
        set_time_limit(0);
        ini_set('memory_limit', -1);
        $this->switchOffice($office_id, 'OfficeDB');


        $unitsTable = TableRegistry::get('OfficeUnits');
        $employee_offices = TableRegistry::get('EmployeeOffices');
        $all_units = $employee_offices->getAllUnitID()->where(['EmployeeOffices.office_id' => $office_id])->toArray();
        $performanceUnitTable = TableRegistry::get('PerformanceUnits');
        $reportsTable = TableRegistry::get('Reports');
        $MinistriesTable = TableRegistry::get('OfficeMinistries');
        $LayersTable = TableRegistry::get('OfficeLayers');
        $OfficesTable = TableRegistry::get('Offices');
        $UnitOriginsTable = TableRegistry::get('OfficeOriginUnits');
        $performance_designationTable = TableRegistry::get('PerformanceDesignations');
        TableRegistry::remove('DakUsers');
        TableRegistry::remove('NothiMasterCurrentUsers');
        $tbl_CronRequest = TableRegistry::get('CronRequest');


        foreach ($all_units as $unit_id => $unit_name) {
            $period = [];
            $unit_record = $performanceUnitTable->getLastUpdateTime($unit_id);
            if (!empty($date_start)) {
                $begin = new \DateTime($date_start);
            } else if (!empty($unit_record)) {
                $begin = ($unit_record['record_date']);
            } else {
                $begin = new \DateTime(date('Y-m-d', strtotime(date('Y-m-d') . ' -1 week')));
            }
            $end = new \DateTime(date('Y-m-d'));
            $last_week_date = new \DateTime(date('Y-m-d', strtotime(date('Y-m-d') . ' -1 week')));
            if(!empty($begin) && !empty($last_week_date) && $begin < $last_week_date){
                 $begin =  $last_week_date;
            }
            for ($i = $begin; $begin < $end; $i->modify('+1 day')) {
                $period [] = $i->format("Y-m-d");
            }
            if (!empty($period)) {
                try {
                    $unit_related_data = $unitsTable->getAll(['id' => $unit_id],
                        ['office_ministry_id', 'office_layer_id', 'office_origin_unit_id', 'office_id'])->first();
                    foreach ($period as $dt) {
                        $has_record = $performanceUnitTable->find()->where(['record_date' => $dt, 'unit_id' => $unit_id])->count();
                        if ($has_record > 0) {
                            continue;
                        }
                        $date = $dt;
//                        pr($unit_related_data['office_id']);die;
                        // Collect From Designation Table
                        $from_designation_report = true;
                        $all_designation = $employee_offices->getAllDesignationByOfficeOrUnitID($unit_related_data['office_id'],
                            $unit_id,1)->distinct(['EmployeeOffices.office_unit_organogram_id'])->toArray();
                        $condition = [
                            'totalInbox' => 'SUM(inbox)', 'totalOutbox' => 'SUM(outbox)', 'totalNothijato' => 'SUM(nothijat)',
                            'totalNothivukto' => 'SUM(nothivukto)', 'totalNisponnoDak' => 'SUM(nisponnodak)', 'totalpotrojariall' => 'SUM(potrojari)',
                            'totalSouddog' => 'SUM(selfnote)', 'totalONisponnoDak' => 'SUM(onisponnodak)',
                            'totalDaksohoNote' => 'SUM(daksohonote)', 'totalNisponnoPotrojari' => 'SUM(nisponnopotrojari)',
                            'totalNisponnoNote' => 'SUM(nisponnonote)', 'totalONisponnoNote' => 'SUM(onisponnonote)',
                            'totalNisponno' => 'SUM(nisponno)', 'totalONisponno' => 'SUM(onisponno)',
                            'totalID' => 'count(id)', 'designation_id'
                        ];
                        if (!empty($all_designation)) {
                            // first check all designation has records
                            $designation_ids = array_values($all_designation);
                            $count_designations = count($designation_ids);
                            $all_designation_has_report = $performance_designationTable->getDesignationData($designation_ids,[$date, $date])->count();
                            if($all_designation_has_report < $count_designations){
                                $this->out('Unit '.$unit_id.'All designation report not recorded. Found: '.$all_designation_has_report.' Required: '.$count_designations.'For Date:' .$date);
                                // generate again office wise performance
                                $command = ROOT . "/bin/cake cronjob updatePerformanceTables {$office_id} > /dev/null 2>&1 &";
                                $tbl_CronRequest->saveData($command);
                                return;
                            }
                            $hasDataInPerformanceDesignation = $performance_designationTable->getUnitData([$unit_id], [$date, $date])->select($condition)->group(['unit_id'])->toArray();
                            if (!empty($hasDataInPerformanceDesignation)) {
                                $returnSummary['totalInbox'] = 0;
                                $returnSummary['totalOutbox'] = 0;
                                $returnSummary['totalNothijato'] = 0;
                                $returnSummary['totalNothivukto'] = 0;
                                $returnSummary['totalNisponnoDak'] = 0;
                                $returnSummary['totalONisponnoDak'] = 0;
                                $returnSummary['totalPotrojari'] = 0;
                                $returnSummary['totalSouddog'] = 0;
                                $returnSummary['totalDaksohoNote'] = 0;
                                $returnSummary['totalNisponnoNote'] = 0;
                                $returnSummary['totalNisponnoPotrojari'] = 0;
                                $returnSummary['totalONisponnoNote'] = 0;
                                $returnSummary['totalNisponno'] = 0;
                                $returnSummary['totalONisponno'] = 0;
                                foreach ($hasDataInPerformanceDesignation as $val_form_designation) {
                                    $returnSummary['totalInbox'] += $val_form_designation['totalInbox'];
                                    $returnSummary['totalOutbox'] += $val_form_designation['totalOutbox'];
                                    $returnSummary['totalNothijato'] += $val_form_designation['totalNothijato'];
                                    $returnSummary['totalNothivukto'] += $val_form_designation['totalNothivukto'];
                                    $returnSummary['totalNisponnoDak'] += $val_form_designation['totalNisponnoDak'];
//                              $returnSummary['totalONisponnoDak'] += TableRegistry::get('DakUsers')->getOnisponnoDak($unit_related_data['office_id'], $unit_id);
                                    $returnSummary['totalONisponnoDak'] += $val_form_designation['totalONisponnoDak'];
                                    $returnSummary['totalPotrojari'] += $val_form_designation['totalpotrojariall'];
                                    $returnSummary['totalSouddog'] += $val_form_designation['totalSouddog'];
                                    $returnSummary['totalDaksohoNote'] += $val_form_designation['totalDaksohoNote'];
                                    $returnSummary['totalNisponnoNote'] += $val_form_designation['totalNisponnoNote'];
                                    $returnSummary['totalNisponnoPotrojari'] += $val_form_designation['totalNisponnoPotrojari'];
//                              $returnSummary['totalONisponnoNote'] += TableRegistry::get('NothiMasterCurrentUsers')->getAllNothiPartNo($unit_related_data['office_id'], $unit_id,0, [$date,$date])->count();
                                    $returnSummary['totalONisponnoNote'] += $val_form_designation['totalONisponnoNote'];
                                    $returnSummary['totalNisponno'] += $val_form_designation['totalNisponno'];
                                    $returnSummary['totalONisponno'] += $val_form_designation['totalONisponno'];
                                }
                            } else {
                                $from_designation_report = false;
                            }
                        } else {
                            $from_designation_report = false;
                        }

                        if ($from_designation_report == false) {
                            $returnSummary = $reportsTable->newPerformanceReport($unit_related_data['office_id'],
                                $unit_id, 0, ['date_start' => $date, 'date_end' => $date]);
                        }

                        $performance_units_entity = $performanceUnitTable->newEntity();

                        $performance_units_entity->inbox = $returnSummary['totalInbox'];
                        $performance_units_entity->outbox = $returnSummary['totalOutbox'];
                        $performance_units_entity->nothijat = $returnSummary['totalNothijato'];
                        $performance_units_entity->nothivukto = $returnSummary['totalNothivukto'];
                        $performance_units_entity->nisponnodak = $returnSummary['totalNisponnoDak'];
                        $performance_units_entity->onisponnodak = $returnSummary['totalONisponnoDak'];

                        $performance_units_entity->potrojari = $returnSummary['totalPotrojari'];
                        if ($returnSummary['totalSouddog'] < 0) {
                            $returnSummary['totalSouddog'] = 0;
                        }
                        $performance_units_entity->selfnote = $returnSummary['totalSouddog'];
                        $performance_units_entity->daksohonote = $returnSummary['totalDaksohoNote'];
                        $performance_units_entity->nisponnopotrojari = $returnSummary['totalNisponnoPotrojari'];
                        $performance_units_entity->nisponnonote = $returnSummary['totalNisponnoNote'];
                        $performance_units_entity->onisponnonote = $returnSummary['totalONisponnoNote'];

                        $performance_units_entity->nisponno = $returnSummary['totalNisponno'];
                        $performance_units_entity->onisponno = $returnSummary['totalONisponno'];

                        $performance_units_entity->office_id = $unit_related_data['office_id'];
                        $performance_units_entity->unit_id = $unit_id;
                        $performance_units_entity->ministry_id = $unit_related_data['office_ministry_id'];
                        $performance_units_entity->layer_id = $unit_related_data['office_layer_id'];
                        $performance_units_entity->origin_id = $unit_related_data['office_origin_unit_id'];
                        $performance_units_entity->unit_name = $unit_name;

                        if (!($office_info = Cache::read('office_info_' . $unit_related_data['office_id']))) {
                            $office_info = $OfficesTable->getBanglaName($unit_related_data['office_id']);
                            Cache::write('office_info_' . $unit_related_data['office_id'], $office_info);
                        }

                        $performance_units_entity->office_name = $office_info['office_name_bng'];

                        if (!($ministry_info = Cache::read('ministry_info_' . $unit_related_data['office_ministry_id']))) {
                            $ministry_info = $MinistriesTable->getBanglaName($unit_related_data['office_ministry_id']);
                            Cache::write('ministry_info_' . $unit_related_data['office_ministry_id'], $ministry_info);
                        }

                        $performance_units_entity->ministry_name = $ministry_info['name_bng'];

                        if (!($layer_info = Cache::read('layer_info_' . $unit_related_data['office_layer_id']))) {
                            $layer_info = $LayersTable->getBanglaName($unit_related_data['office_layer_id']);
                            Cache::write('layer_info_' . $unit_related_data['office_layer_id'], $layer_info);
                        }

                        $performance_units_entity->layer_name = $layer_info['layer_name_bng'];

                        if (!($origin_info = Cache::read('origin_info_' . $unit_related_data['office_origin_unit_id']))) {
                            $origin_info = $UnitOriginsTable->getBanglaName($unit_related_data['office_origin_unit_id']);
                            Cache::write('origin_info_' . $unit_related_data['office_origin_unit_id'], $origin_info);
                        }

                        $performance_units_entity->origin_name = $origin_info['unit_name_bng'];

                        $performance_units_entity->status = 1;
                        $performance_units_entity->record_date = $date;

                        $performanceUnitTable->save($performance_units_entity);
                    }
                    $this->updateIndividualUnit($unit_id);
                    $this->deleteMultipleRowsOfUnits($unit_id, !empty($period[0]) ? $period[0] : '');
                } catch (\Exception $ex) {
                    // exception happend so need to run again.
                    $command = ROOT . "/bin/cake cronjob updatePerformanceTables {$office_id} > /dev/null 2>&1 &";
                    $tbl_CronRequest->saveData($command);
                    $this->out($ex->getMessage().(isset($unit_id)?(' For Unit: '.$unit_id):''));
                }
            }
        }
        $this->out('Unit Done For Office: ' . $office_id);
//         $this->runCronRequest();
    }

    public function OfficeWisePerformanceTablesUpdate()
    {
        ini_set('memory_limit', -1);
        set_time_limit(0);

        $tbl_CronRequest = TableRegistry::get('CronRequest');
        $offcieDomainsTable = TableRegistry::get('OfficeDomains');
        $domain_array = $offcieDomainsTable->getUniqueDomainHost();
        $max_length = 0;
        $host = [];
        $offices_ids = [];
        if (!empty($domain_array)) {
            foreach ($domain_array as $DA) {
                $allOfficeIds = $offcieDomainsTable->getHostWiseOfficeID($DA);
                $toalOffices = count($allOfficeIds);
                if ($max_length < $toalOffices) {
                    $max_length = $toalOffices;
                }
                $host[$DA] = array_values($allOfficeIds);
            }
            for ($i = 0; $i < $max_length; $i++) {
                foreach ($domain_array as $DA) {
                    if (isset($host[$DA][$i])) {
                        $offices_ids[] = $host[$DA][$i];
                    }
                }
            }
        }
        if (!empty($offices_ids)) {
            foreach ($offices_ids as $id) {
                $command = ROOT . "/bin/cake cronjob updatePerformanceTables {$id} > /dev/null 2>&1 &";
                $tbl_CronRequest->saveData($command);
            }
        }
        $command = ROOT . "/bin/cake projapoti requestUrl 'https://nothi.gov.bd/cron/runReportUpdateChecker'> /dev/null 2>&1 &";
        $tbl_CronRequest->saveData($command);
        if (!empty($domain_array)) {
            $run_request = count($domain_array) * 6;
            for ($indx = 1; $indx <= $run_request; $indx++) {
                $this->runCronRequest();
            }
        }
    }

    public function performaceOfficePotrojariUpdate($date_start = '')
    {
        ini_set('memory_limit', -1);
        set_time_limit(0);

        $office_domains = TableRegistry::get('OfficeDomains');
        $all_offices = $office_domains->getOfficesData();

        $performanceOfficeTable = TableRegistry::get('PerformanceOffices');


        foreach ($all_offices as $office_id => $office_name) {

            $period = [];
            $office_record = $performanceOfficeTable->getLastUpdateTime($office_id);
            if (empty($office_record)) {
                continue;
            }
            if (!empty($date_start)) {
                $begin = new \DateTime($date_start);
            } else {
                $begin = new \DateTime(date('Y-m-d', strtotime(date('Y-m-d') . ' -1 week')));
            }
            if (!empty($office_record)) {
                $end = ($office_record['record_date']);
            } else {
                $end = new \DateTime(date('Y-m-d'));
            }

            for ($i = $begin; $begin <= $end; $i->modify('+1 day')) {
                $period [] = $i->format("Y-m-d");
            }
            if (!empty($period)) {
                try {
                    $this->switchOffice($office_id, 'OfficeDB');
                    TableRegistry::remove('Potrojari');
                    foreach ($period as $dt) {
                        $perfoarmanceOfficeData = $performanceOfficeTable->getOfficeData($office_id, [$dt, $dt])->first();
                        if (empty($perfoarmanceOfficeData)) {
                            continue;
                        }
                        $potrojari = TableRegistry::get('Potrojari')->allPotrojariCount($office_id, 0, 0, [$dt, $dt]);
                        $performanceOfficeTable->updateAll(['potrojari' => $potrojari], ['office_id' => $office_id, 'record_date' => $dt]);
                    }
                    $this->out($office_id . ' Done.');
                } catch (\Exception $ex) {
                    $this->out($ex->getMessage());
                }
            }
        }
    }

    public function potrojariGroupMigration()
    {
        ini_set('memory_limit', -1);
        set_time_limit(0);

        $potrojariGroupsTable = TableRegistry::get('PotrojariGroups');
        $potrojariGroupsMigTable = TableRegistry::get('PotrojariGroupsUsers');
        $employeeOfficesTable = TableRegistry::get('EmployeeOffices');
        $officesTable = TableRegistry::get('Offices');
        $officeUnitsTable = TableRegistry::get('OfficeUnits');
        $officeUnitOrganogramsTable = TableRegistry::get('OfficeUnitOrganograms');
        $employeeRecordsTable = TableRegistry::get('EmployeeRecords');

        try {
            $potrojari_groups_datas = $potrojariGroupsTable->find()->all();
            foreach ($potrojari_groups_datas as $potrojari_groups_data) {

                $users = json_decode($potrojari_groups_data['group_value'], true);
                foreach ($users as $user) {
                    if (!empty($user['office_id'])) {

                        $employee_offices_info = $employeeOfficesTable->find()->select(['office_head'])->where(['office_unit_organogram_id' => $user['office_unit_organogram_id'], 'status' => 1])->first();

                        if (empty($employee_offices_info)) {

                            $employee_offices_info = $employeeOfficesTable->find()->select(['office_head'])->where(['office_unit_organogram_id' => $user['office_unit_organogram_id']])->order(['id' => 'DESC'])->first();

                        }
                    }

                    $potrojari_groups_mig = $potrojariGroupsMigTable->newEntity();

                    if (!empty($user['office_id'])) {
                        $office_info = $officesTable->get($user['office_id']);
                        $potrojari_groups_mig->office_id = $user['office_id'];
                        $potrojari_groups_mig->office_name_eng = $office_info['office_name_eng'];
                        $potrojari_groups_mig->office_name_bng = $office_info['office_name_bng'];
                    } else {
                        $potrojari_groups_mig->office_id = $user['office_id'];
                        $potrojari_groups_mig->office_name_eng = '';
                        $potrojari_groups_mig->office_name_bng = $user['office_name'];
                    }

                    if (!empty($user['office_unit_id'])) {
                        $office_unit_info = $officeUnitsTable->get($user['office_unit_id']);
                        $potrojari_groups_mig->office_unit_id = $user['office_unit_id'];
                        $potrojari_groups_mig->office_unit_name_eng = $office_unit_info['unit_name_eng'];
                        $potrojari_groups_mig->office_unit_name_bng = $office_unit_info['unit_name_bng'];

                    } else {
                        $potrojari_groups_mig->office_unit_id = $user['office_unit_id'];
                        $potrojari_groups_mig->office_unit_name_eng = '';
                        $potrojari_groups_mig->office_unit_name_bng = $user['office_unit_name'];
                    }

                    if (!empty($user['office_unit_organogram_id'])) {
                        $office_unit_organogram_info = $officeUnitOrganogramsTable->get($user['office_unit_organogram_id']);
                        $potrojari_groups_mig->office_unit_organogram_id = $user['office_unit_organogram_id'];
                        $potrojari_groups_mig->office_unit_organogram_name_eng = $office_unit_organogram_info['designation_eng'];
                        $potrojari_groups_mig->office_unit_organogram_name_bng = $office_unit_organogram_info['designation_bng'];
                    } else {
                        $potrojari_groups_mig->office_unit_organogram_id = $user['office_unit_organogram_id'];
                        $potrojari_groups_mig->office_unit_organogram_name_eng = '';
                        $potrojari_groups_mig->office_unit_organogram_name_bng = $user['office_unit_organogram_name'];
                    }

                    if (!empty($user['employee_id'])) {
                        $employee_record_info = $employeeRecordsTable->get($user['employee_id']);
                        $potrojari_groups_mig->employee_id = $user['employee_id'];
                        $potrojari_groups_mig->employee_name_eng = $employee_record_info['name_eng'];
                        $potrojari_groups_mig->employee_name_bng = $employee_record_info['name_bng'];
                    } else {
                        $potrojari_groups_mig->employee_id = $user['employee_id'];
                        $potrojari_groups_mig->employee_name_eng = '';
                        $potrojari_groups_mig->employee_name_bng = $user['employee_name'];
                    }

                    $potrojari_groups_mig->group_id = $potrojari_groups_data['id'];
                    $potrojari_groups_mig->group_name = $potrojari_groups_data['group_name'];
                    $potrojari_groups_mig->privacy_type = $potrojari_groups_data['privacy_type'];
                    $potrojari_groups_mig->creator_employee_id = $potrojari_groups_data['creator_employee_id'];
                    $potrojari_groups_mig->creator_office_unit_organogram_id = $potrojari_groups_data['creator_office_unit_organogram_id'];
                    $potrojari_groups_mig->creator_unit_id = $potrojari_groups_data['creator_unit_id'];
                    $potrojari_groups_mig->creator_office_id = $potrojari_groups_data['creator_office_id'];
                    $potrojari_groups_mig->created = $potrojari_groups_data['created'];
                    $potrojari_groups_mig->modified = $potrojari_groups_data['modified'];
                    $potrojari_groups_mig->officer_email = !empty($user['officer_email']) ? $user['officer_email'] : '';
                    if (!empty($user['office_id'])) {
                        $potrojari_groups_mig->office_head = $employee_offices_info['office_head'];
                    }
                    $potrojariGroupsMigTable->save($potrojari_groups_mig);
                }
            }
        } catch (\Exception $ex) {
            $this->out($ex->getMessage());
        }
    }

    public function setCountOfUsersINPotrojariGroups()
    {
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        $potrojariGroups = TableRegistry::get('potrojariGroups');
        $potrojari_groups_datas = $potrojariGroups->find()->all();
        foreach ($potrojari_groups_datas as $potrojari_groups_data) {
            $users = json_decode($potrojari_groups_data['group_value'], true);
            $potrojariGroups->updateAll(['total_users' => count($users)], ['id' => $potrojari_groups_data['id']]);
        }
    }

    public function updatePerformanceTables($office_id,$run_other = 1)
    {
        try {
            $this->performanceDesignationUpdateByOfficeID($office_id);
            $this->performanceUnitUpdateByOfficeID($office_id);
            $this->performaceIndividualOfficeUpdate($office_id);
//        $this->runThisCommand(ROOT."/bin/cake report reportMismatch '".date('Y-m-d')."' '".date('Y-m-d')."' > /dev/null 2>&1 &");
            if(!empty($run_other)){
                $this->runCronRequest();
            }
        } catch (\Exception $ex) {
            $this->out($ex->getMessage());
            if(!empty($run_other)){
                $this->runCronRequest();
            }
        }
    }

    public function updatePerformanceTableOfLoggedOffices()
    {
        $today = date('Y-m-d');
        $yesterday = date('Y-m-d', strtotime($today . ' -1 day'));
        $allOficesId = TableRegistry::get('UserLoginHistory')->getLoggedOfficesId([$yesterday, $today]);
        if (!empty($allOficesId)) {
            foreach ($allOficesId as $office_id) {
                try {
//                    $this->out('Office: ' . $office_id . '\n strated at: ' . date('Y-m-d H:i:s'));
                    $this->performanceDesignationUpdateByOfficeID($office_id);
                    $this->performanceUnitUpdateByOfficeID($office_id);
                    $this->performaceIndividualOfficeUpdate($office_id);
//                    $this->out('ended at: ' . date('Y-m-d H:i:s'));
                } catch (\Exception $ex) {
                    $this->out($ex->getMessage());
                    continue;
                }
            }
        }
    }

    public function guardFileAllDataApi()
    {

        try {
            //share with curl
            $this->out('started');
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'http://portal.gov.bd/npfadmin/public/api/guardFiles?ministry=1&name=all&api_key=aPmnN3n8Qb');
            curl_setopt($ch, CURLOPT_POST, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $errno = curl_errno($ch);
            if (!empty($errno)) {
                $error_message = curl_strerror($errno);
                $error = $errno . ' : ' . $error_message;
                $this->out($error->getMessage());

            } else {
                $result = json_decode(curl_exec($ch), true);
                $this->out($result['status']);
                if ($result['status'] == 'SUCCESS' && !empty($result['data'])) {
                    $guard_file_api_data_table = TableRegistry::get('GuardFileApiDatas');
                    $guard_file_api_data_table->deleteAll(['1']);
                    foreach ($result['data'] as $data) {
                        $row = $guard_file_api_data_table->newEntity();
                        $row->layer_id = $data['layer_id'];
                        $row->type = $data['type'];
                        $row->subdomain = $data['subdomain'];
                        $row->name = $data['name'];
                        $row->link = $data['link'];
                        $guard_file_api_data_table->save($row);
                    }
                    $this->out('done');
                }
            }
            curl_close($ch);
            //end

        } catch (\Exception $ex) {
            $this->out($ex->getMessage());
        }
    }

    public function migrateDakSohoNoteCount($dt = '')
    {
        $office_domains = TableRegistry::get('OfficeDomains');
        $all_offices = $office_domains->getOfficesData();
        $performanceOfficesTable = TableRegistry::get('PerformanceOffices');
        $employee_offices = TableRegistry::get('EmployeeOffices');
        $performanceUnitTable = TableRegistry::get('PerformanceUnits');
        $performanceDesignationTable = TableRegistry::get('PerformanceDesignations');
        foreach ($all_offices as $office_id => $office_name) {
            try {
                if (!empty($dt)) {
                    if (empty($office_id)) {
                        continue;
                    }
//                    $this->out('Started: ' . $office_id);
                    $this->switchOffice($office_id, 'DashboardOffice');
                    TableRegistry::remove('NothiParts');
                    TableRegistry::remove('NothiDakPotroMaps');
//                        $dakSohonote = TableRegistry::get('NothiParts')->dakSrijitoNoteCount($office_id,0,0, [$dt,$dt]);
                    $last_data = $performanceOfficesTable->getOfficeData($office_id, [$dt, $dt])->first();
                    if (!empty($last_data)) {
                        $performanceOfficesTable->updateAll(['daksohonote' => intval($last_data['nothivukto'] * 0.4)], ['record_date' => $dt, 'office_id' => $office_id]);
                    }

                    $all_units = $employee_offices->getAllUnitID()->where(['EmployeeOffices.office_id' => $office_id])->toArray();
                    if (!empty($all_units)) {
                        foreach ($all_units as $unit_id => $unit_name) {
                            if (empty($unit_id)) {
                                continue;
                            }
//                                 $dakSohonote = TableRegistry::get('NothiParts')->dakSrijitoNoteCount($office_id,$unit_id,0, [$dt,$dt]);
                            $last_data = $performanceUnitTable->getUnitData($unit_id, [$dt, $dt])->first();
                            $performanceUnitTable->updateAll(['daksohonote' => intval($last_data['nothivukto'] * 0.4)], ['record_date' => $dt, 'office_id' => $office_id, 'unit_id' => $unit_id]);
                        }
                    }
                    $all_designations = $employee_offices->getAllDesignationID()->where(['EmployeeOffices.office_id' => $office_id])->toArray();
                    if (!empty($all_designations)) {
                        foreach ($all_designations as $designation_id => $designation_name) {
//                                    $dakSohonote = TableRegistry::get('NothiParts')->dakSrijitoNoteCount($office_id,$unit_id,$designation_id, [$dt,$dt]);
                            if (empty($designation_id)) {
                                continue;
                            }
                            $last_data = $performanceDesignationTable->getDesignationData($designation_id, [$dt, $dt])->first();
                            $performanceDesignationTable->updateAll(['daksohonote' =>
                                intval($last_data['nothivukto'] * 0.4)], ['record_date' => $dt, 'office_id' => $office_id, 'designation_id' => $designation_id]);
                        }
                    }
                }
            } catch (\Exception $ex) {
                $this->out($ex->getMessage());
                continue;
            }
        }
    }

    public function deleteMultipleRowsOfOffice($office_id = 0, $start_date = '', $end_date = '')
    {
        if (empty($office_id)) {
            return;
        }
        $begin = !empty($start_date) ? new \DateTime($start_date) : new \DateTime(date('Y-m-d', strtotime(date('Y-m-d') . ' -1 week')));
        $end = !empty($start_date) ? new \DateTime($end_date) : new \DateTime(date('Y-m-d'));

        for ($i = $begin; $begin <= $end; $i->modify('+1 day')) {
            $period [] = $i->format("Y-m-d");
        }
        $perfromanceOfficesTable = TableRegistry::get('PerformanceOffices');
        if (!empty($period)) {
            foreach ($period as $day) {
                $data_count = $perfromanceOfficesTable->find()->where(['record_date' => $day, 'office_id' => $office_id])->count();
                if ($data_count > 1) {
                    $last_id = $perfromanceOfficesTable->find()->select(['id'])->where(['record_date' => $day, 'office_id' => $office_id])->first();
                    $perfromanceOfficesTable->deleteAll(['record_date' => $day, 'office_id' => $office_id, 'id <>' => $last_id['id']]);
                }
            }
        }

    }

    public function deleteMultipleRowsOfUnits($unit_id = 0, $start_date = '', $end_date = '')
    {
        if (empty($unit_id)) {
            return;
        }
        $begin = !empty($start_date) ? new \DateTime($start_date) : new \DateTime(date('Y-m-d', strtotime(date('Y-m-d') . ' -1 week')));
        $end = !empty($start_date) ? new \DateTime($end_date) : new \DateTime(date('Y-m-d'));

        for ($i = $begin; $begin <= $end; $i->modify('+1 day')) {
            $period [] = $i->format("Y-m-d");
        }
        $performanceUnitTable = TableRegistry::get('PerformanceUnits');
        if (!empty($period)) {
            foreach ($period as $day) {
                $data_count = $performanceUnitTable->find()->where(['record_date' => $day, 'unit_id' => $unit_id])->count();
                if ($data_count > 1) {
                    $last_id = $performanceUnitTable->find()->select(['id'])->where(['record_date' => $day, 'unit_id' => $unit_id])->first();
                    $performanceUnitTable->deleteAll(['record_date' => $day, 'unit_id' => $unit_id, 'id <>' => $last_id['id']]);
                }
            }
        }
    }

    public function deleteMultipleRowsOfDesignations($designation_id = 0, $start_date = '', $end_date = '')
    {
        if (empty($designation_id)) {
            return;
        }
        $begin = !empty($start_date) ? new \DateTime($start_date) : new \DateTime(date('Y-m-d', strtotime(date('Y-m-d') . ' -1 week')));
        $end = !empty($start_date) ? new \DateTime($end_date) : new \DateTime(date('Y-m-d'));

        for ($i = $begin; $begin <= $end; $i->modify('+1 day')) {
            $period [] = $i->format("Y-m-d");
        }

        $performanceDesignationTable = TableRegistry::get('PerformanceDesignations');
        if (!empty($period)) {
            foreach ($period as $day) {
                $data_count = $performanceDesignationTable->find()->where(['record_date' => $day, 'designation_id' => $designation_id])->count();
                if ($data_count > 1) {
                    $last_id = $performanceDesignationTable->find()->select(['id'])->where(['record_date' => $day, 'designation_id' => $designation_id])->first();
                    $performanceDesignationTable->deleteAll(['record_date' => $day, 'designation_id' => $designation_id, 'id <>' => $last_id['id']]);
                }
            }
        }
    }

    private function createPdf($filename, $body, $removeSign = 1, $folder_path = '', $magin_info = [], $office_id = 0, $maindomain = null)
    {
        if (empty($maindomain)) {
            $maindomain = FILE_FOLDER;
        }

        if (!isset($magin_info['margin_top'])) {
            $magin_info['margin_top'] = 1;
        }
        if (!isset($magin_info['margin_right'])) {
            $magin_info['margin_right'] = .75;
        }
        if (!isset($magin_info['margin_bottom'])) {
            $magin_info['margin_bottom'] = .75;
        }
        if (!isset($magin_info['margin_left'])) {
            $magin_info['margin_left'] = .75;
        }
        if (!isset($magin_info['pdf_header_title'])) {
            $magin_info['pdf_header_title'] = '---';
        }
        if (!isset($magin_info['pdf_footer_title'])) {
            $magin_info['pdf_footer_title'] = '---';
        }
        if (!isset($magin_info['show_pdf_page_number'])) {
            $magin_info['show_pdf_page_number'] = 1;
        }

        $filename = base64_encode($filename);
        $request = new \Cake\Network\Request();
        if (file_exists(FILE_FOLDER_DIR . $folder_path . $filename . '.pdf')) {
            // already created
            $response = ['status' => 'success', 'src' => $request->webroot . 'content/' . $folder_path . $filename . '.pdf'];
            goto rtn;
        }

        $response = ['status' => 'error', 'msg' => 'Cannot build the preview'];


        $optionsMerge = [];
        if (defined("POTROJARI_UPDATE") && POTROJARI_UPDATE) {
            if (isset($magin_info['potro_header']) && ($magin_info['potro_header'] == 0)) {

                if (defined("TESTING_SERVER") && TESTING_SERVER) {
                    if (!empty($magin_info['potro_header_banner'])) {
                        $optionsMerge['javascript-delay'] = 1000;
                        $optionsMerge['header-html'] = "{$maindomain}potrojariDynamicHeader/{$office_id}?imagetoken=" . urlencode($magin_info['potro_header_banner']) . "&banner_position=" . $magin_info['banner_position'] . "&banner_width=" . $magin_info['banner_width']."&pdf_header_title=".urlencode($magin_info['pdf_header_title']);
                    } else {
                        $optionsMerge['header-html'] = "{$maindomain}potrojariDynamicHeader/{$office_id}?banner_position=" . $magin_info['banner_position'] . "&banner_width=" . $magin_info['banner_width']."&pdf_header_title=".urlencode($magin_info['pdf_header_title']);
                    }
                } else {
                    $s = (defined("Live") && Live) ? 's' : '';
                    if (!empty($magin_info['potro_header_banner'])) {
                        $optionsMerge['header-html'] = "http://localhost/potrojariDynamicHeader/{$office_id}?imagetoken=" . urlencode($magin_info['potro_header_banner']) . "&banner_position=" . (!empty($magin_info['banner_position']) ? $magin_info['banner_position'] : 'left') . "&banner_width=" . (!empty($magin_info['banner_width']) ? $magin_info['banner_width'] : 'auto')."&pdf_header_title=".urlencode($magin_info['pdf_header_title']);
                    } else {
                        $optionsMerge['header-html'] = "http://localhost/potrojariDynamicHeader/{$office_id}?banner_position=" . (!empty($magin_info['banner_position']) ? $magin_info['banner_position'] : 'left') . "&banner_width=" . (!empty($magin_info['banner_width']) ? $magin_info['banner_width'] : 'auto')."&pdf_header_title=".urlencode($magin_info['pdf_header_title']);
                    }
                }

                $optionsMerge['javascript-delay'] = 1000;
                $optionsMerge['footer-center'] = '';
            }
        }
        if (defined("TESTING_SERVER") && TESTING_SERVER) {
            $optionsMerge['footer-html'] = "{$maindomain}potrojariDynamicFooter"."?pdf_footer_title=".urlencode($magin_info['pdf_footer_title'])."&show_pdf_page_number=".($magin_info['show_pdf_page_number']);
        } else {
            $s = (defined("Live") && Live) ? 's' : '';
            $optionsMerge['footer-html'] = "http://localhost/potrojariDynamicFooter"."?pdf_footer_title=".urlencode($magin_info['pdf_footer_title'])."&show_pdf_page_number=".($magin_info['show_pdf_page_number']);
        }
        $optionsMerge['footer-center'] = '';
        $request = new \Cake\Network\Request();
        $options = $optionsMerge + array(
                'no-outline', // option without argument
                'disable-external-links',
                'disable-internal-links',
                'disable-forms',
//                'footer-center' => '[page]',
                'orientation' => !empty($magin_info['orientation']) ? $magin_info['orientation'] : 'portrait',
                'margin-top' => bnToen($magin_info['margin_top']) * 20,
                'margin-right' => 0,
                'margin-bottom' => bnToen($magin_info['margin_bottom']) * 20,
                'margin-left' => 0,
                'encoding' => 'UTF-8', // option with argument
                'binary' => ROOT . DS . 'vendor' . DS . 'profburial' . DS . 'wkhtmltopdf-binaries-centos6' . DS . 'bin' . DS . (defined("PDFBINARY") ? PDFBINARY : 'wkhtmltopdf-amd64-centos6'),
                'user-style-sheet' => WWW_ROOT . DS . 'assets' . DS . 'global' . DS . 'plugins' . DS . 'bootstrap' . DS . 'css' . DS . 'bootstrap.min.css',
            );

        $pdf = new Pdf($options);

        $body = str_replace($request->webroot . 'img/', WWW_ROOT . 'img/', $body);
        $body = str_replace('lang="AR-SA"', 'lang="BN"', $body);
        $body = str_replace('dir="RTL"', '', $body);

        /*
         * In live  & training server need to replace domain with localhost.
         */
        if (defined("TESTING_SERVER") && !TESTING_SERVER) {
            $pattern = "/(http|https)\:\/\/([a-zA-Z0-9-.]*)\.?nothi\.gov\.bd\/(content|getContent)/";
            $body = preg_replace($pattern,'http://localhost/$3',$body);
//            $pattern = "/(http|https)\:\/\/([a-zA-Z0-9-.]*)\.?nothi\.gov\.bd/";
//
//            preg_match_all($pattern, $body, $matches);
//            if (!empty($matches)) {
//                foreach ($matches[0] as $match) {
//                    $body = str_replace($match, 'http://localhost', $body);
//                }
//            }
        }
        if (!defined('Live') || Live == 0) {
            $background_image = 'background-image: url("' . WWW_ROOT . 'img/test-background.png' . '");background-repeat: no-repeat;background-position:center;';
        } else {
            $background_image = '';
        }

        $pdf->addPage("<html>
    <head>
        <meta charset='UTF-8' />
        <script src='https://code.jquery.com/jquery-1.11.3.js'></script>
        <link href='" . $this->request->webroot . "assets/global/plugins/bootstrap/css/bootstrap.min.css' rel='stylesheet' type='text/css'/>
        <link href='" . $this->request->webroot . "assets/global/plugins/uniform/css/uniform.default.css' rel='stylesheet' type='text/css'/>
        <link href='" . $this->request->webroot . "assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css' rel='stylesheet' type='text/css'/>
        
        <style type='text/css'>
           *{
                font-family: Nikosh, SolaimanLipi, 'Open Sans', sans-serif !important;
            }
            body{
                font-size: 12pt;
                padding-left: " . bnToen($this->request->data['margin_left']) . "in;
                padding-right: " . bnToen($this->request->data['margin_right']) . "in;
                {$background_image}
            }
            
            .col-12,.col-6,.col-3{
                padding-right:15px;
                padding-left:15px;
            }
            .col-12{
                flex: 0 0 100%;
                max-width: 100%;
            }
            .col-6{
                flex: 0 0 50%;
                max-width: 50%;
            }
            
            .col-3{
                flex: 0 0 25%;
                max-width: 25%;
            }
            .bangladate{
                border-bottom: 1px solid #000;
            }
            #sovapoti_signature img, #sender_signature img, #sender_signature2 img, #sender_signature3 img, #sender_signature4 img, #sender_signature5 img{
                height: 50px;
            }

            #pencil{
                visibility: hidden;
            }
            thead { display: table-row-group}
            tfoot { display: table-row-group}
            tr { page-break-inside: avoid}
            thead th { height: 1.1cm; }
            .text-center{
                text-align: center!important;
            }

            #subject {
                word-break: break-word;
                word-wrap: break-word;
            }
            .class2 thead tr th, .class2 tbody tr td {
                border: 1px solid #0c0c0c;
            }

            .hide{
                display:none;
            }

            #upomontri_receiver_signature img,
            #sochib_signature img,
            #second_receiver_signature img,
            #third_receiver_signature img,
            #fourth_receiver_signature img,
            #fifth_receiver_signature img {
                height:50px;
            }
            " . ($removeSign ? ("
            img[alt=signature] {
                visibility: hidden;
            }
            .showimageforce img{
                visibility: visible!important;
            }

            #sender_signature,#sender_signature2,#sender_signature3,#sovapoti_signature{
                border: 1px solid #000;
                padding:2px;
            }
            #sender_signature img,#sender_signature2 img,#sender_signature3 img,#sovapoti_signature img{
                visibility: hidden;
            }
            .showimageforce #sovapoti_signature, .showimageforce #sender_signature, .showimageforce #sender_signature2, .showimageforce #sender_signature3,
            .showimageforce #sovapoti_signature_date, .showimageforce #sender_signature_date, .showimageforce #sender_signature2_date, .showimageforce #sender_signature3_date {
                visibility: visible!important;
                border: 0px solid #000;
                padding:0px;
            }
           
            ") : "") . "
        </style>
    </head>
    <body >
    <style  type='text/css'>
        thead { display: table-row-group}
        tfoot { display: table-row-group}
        tr { page-break-inside: avoid}
        th { min-height: 1.1cm; }
        .text-center{
            text-align: center!important;
        }
    
    .editable {
        border: none !important;
        word-break: normal;
        word-wrap:break-word
    }

    #note {
        overflow: hidden;
        word-break: normal;
        word-wrap: break-word;
    }

    .bangladate {
        border-bottom: 1px solid #000 !important;
    }
      
    table {
      table-layout: fixed;
      width: 100%!importan;
      white-space: nowrap;
    }
    table td , table th{
      white-space: pre-wrap;
      /*overflow: hidden;*/
      /*text-overflow: ellipsis;*/
      word-break: normal;
      word-wrap: break-word;
    }
    
    table th{
      word-break: normal;
    }
    .imei_table_area{
        overflow: hidden!important;
    }
    </style>
    <script>
        $(function(){
            $('#sharok_no').parent('div').css('white-space','nowrap');
            $('#sharok_no2').parent('div').css('white-space','nowrap');
            
        })
</script>
        {$body}
    </body>
</html>");

        $pdf->ignoreWarnings = true;
        if ($pdf->saveAs(FILE_FOLDER_DIR . $folder_path . $filename . '.pdf')) {
            $response = ['status' => 'success', 'src' => $request->webroot . 'content/' . $folder_path . $filename . '.pdf?v=' . time()];
        } else {
            $response = ['status' => 'error', 'msg' => $pdf->getError()];
        }
        rtn:
        return $response;
    }


    public function getPdfByNothiPotroId($id, $nothi_office = 0, $signHide = 1, $folder_path = '', $filename = '', $office_id = 0)
    {
        try {
            if ($nothi_office != 0 && $nothi_office != $office_id) {
                $this->switchOffice($nothi_office, 'MainNothiOffice');
            } else {
                $nothi_office = $office_id;
            }

            $nothiPotroAttachmentsTable = TableRegistry::get('NothiPotroAttachments');
            $nothiPotroTable = TableRegistry::get('NothiPotros');
            $nothiPotroAttachmentsData = $nothiPotroAttachmentsTable->get($id);
            $nothiPotrosData = $nothiPotroTable->get($nothiPotroAttachmentsData['nothi_potro_id']);

            $pdfbody = (!empty($nothiPotroAttachmentsData->potro_cover) ? ($nothiPotroAttachmentsData->potro_cover . "<br/>") : '') . $nothiPotroAttachmentsData->content_body;

//        if(empty($this->request->data['meta'])) {
            $meta = !empty($nothiPotroAttachmentsData['meta_data']) ? json_decode($nothiPotroAttachmentsData['meta_data'], true) : [];
//            if (isset($meta['margin_top'])) {
//                $this->request->data['margin_top'] = $meta['margin_top'];
//            }
//            if (isset($meta['margin_right'])) {
//                $this->request->data['margin_right'] = $meta['margin_right'];
//            }
//            if (isset($meta['margin_bottom'])) {
//                $this->request->data['margin_bottom'] = $meta['margin_bottom'];
//            }
//            if (isset($meta['margin_left'])) {
//                $this->request->data['margin_left'] = $meta['margin_left'];
//            }
//            if (!empty($meta['orientation'])) {
//                $this->request->data['orientation'] = $meta['orientation'];
//            }
//        }
            if (!empty($filename)) {
                $filename = "potro_" . $id . '_' . $nothi_office;
            }

            $pdf_create_status = $this->createPdf($filename, $pdfbody, $signHide, $folder_path, $meta, $nothi_office);
            if ($pdf_create_status['status'] == 'success' && !empty($pdf_create_status['src']) && !empty($id)) {
                if (file_exists(FILE_FOLDER_DIR . $folder_path . base64_encode($filename) . '.pdf')) {
                    $rows = TableRegistry::get('PotrojariAttachments')
                        ->updateAll(['file_name' => $folder_path . base64_encode($filename) . '.pdf',
                            'file_dir' => FILE_FOLDER_DIR, 'attachment_type' => 'application/pdf', 'user_file_name' => $nothiPotrosData['subject']], ['potro_id' => $id]);
//                    $this->out($rows);
                    return true;
                } else {
//                    $this->out($pdf_create_status['src']);
                }
            } else {
//                $this->out($pdf_create_status['msg']);
            }
        } catch (\Exception $ex) {
            $this->out($ex->getMessage());
        }
//        $this->out('false');
        return false;
    }

    public function runAllReportCrons()
    {
        try {
            $response = 'Report Run.';
            $this->performanceDesignationUpdate();
            $response .= 'performanceDesignationUpdate done.';
            $this->cronForDesignationDashboard();
            $response .= 'cronForDesignationDashboard done.';
            $this->performanceUnitUpdate();
            $response .= 'performanceUnitUpdate done.';
            $this->cronForUnitDashboard();
            $response .= 'cronForUnitDashboard done.';
            $this->performanceOfficeUpdate();
            $response .= 'performanceOfficeUpdate done.';
            $this->cronForOfficeDashboard();
            $response .= 'cronForOfficeDashboard done.';
            $this->monitorCron();
            $response .= 'monitorCron done.';
        } catch (\Exception $ex) {
            $this->sendCronMessage('tusharkaiser@gmail.com', [], 'Cron Report ' . date('Y-m-d'),
                'Run All Cron Report', 'Error: ' . $ex->getMessage().'<br>Response: '.$response);
            $this->out($ex->getMessage());
        }
    }

    /**
     *
     * @param int $cell_number
     * @param string $message
     * @return boolean
     */
    public function saveSMSRequest($cell_number = '', $message = '')
    {
        if (empty($cell_number) || empty($message)) {
            return;
        }
        $ms = bnToen($cell_number);
        if (substr($ms, 0, 3) != '+88') {
            $ms = '+88' . $ms;
        }
        try {
            $SmsRequestTable = TableRegistry::get('SmsRequest');
            $SmsRequestTable->saveData($ms, $message);
        } catch (\Exception $ex) {
            return;
        }
    }


    public function error_log_mail()
    {
        $this->layout = 'ajax';
        $errorlogTable = TableRegistry::get('ErrorLog');
        $error_log = $errorlogTable->find()->where(['error_status' => 0]);
        $error_count = $error_log->count();
        $title = 'ইরর বার্তা';
        $subject = 'নথির ইররসমূহ (মোট ইরর ' . enTobn($error_count) . 'টি)';
        $content = '';
        if ($error_count > 0) {
            $content = '<table width="100%"><tr><th style="border: solid 1px silver;padding:2px;">অফিস আইডি</th><th style="border: solid 1px silver;padding:2px;">রিকোয়েষ্ট ইউআরএল</th><th style="border: solid 1px silver;padding:2px;">ইরর শিরোনাম</th><th style="border: solid 1px silver;padding:2px;">ইররের সময়</th></tr>';
        }
        foreach ($error_log as $key => $error) {
            $content .= '<tr><td style="border: solid 1px silver;padding:2px;">' . $error->office_id . '</td><td style="border: solid 1px silver;padding:2px;">' . $error->request_url . '</td><td style="border: solid 1px silver;padding:2px;">' . $error->error_title . '</td><td style="border: solid 1px silver;padding:2px;">' . $error->created . '</td></tr>';

            $errorlogTable->updateAll(['error_status' => 1], ['id' => $error->id]);
        }
        if ($error_count > 0) {
            $content .= '</table>';
        }
        $this->set(compact('title', 'subject', 'content'));
        $this->view = '/email/html/layout';

        if (ERRORLOG_MAIL) { // if option is active
            if ($error_count > 0) {
                $email = new Email('default');
                $email->emailFormat('html')->from(['nothi@nothi.org.bd'])
                    ->to('jbhasan@gmail.com')
                    ->subject($subject)
                    ->viewVars([
                        'title' => $title,
                        'subject' => $subject,
                        'content' => $content
                    ])
                    ->template('layout')
                    ->sendWithoutException();

//                $this->out("ইমেইল পাঠানো হয়েছে");
                return false;
            } else {
                $this->out("কোনো ইরর পাওয়া যায় নি");
                return false;
            }
        }
    }
}
