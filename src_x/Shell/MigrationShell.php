<?php

namespace App\Shell;

use Cake\Console\Shell;
use Cake\ORM\TableRegistry;
use App\Controller\ProjapotiController;
use Cake\Datasource\ConnectionManager;
use Cake\I18n\I18n;
use Cake\Network\Email\Email;
use Cake\I18n\Number;
use Cake\I18n\Time;
use Cake\Routing\Route\Route;
use Cake\Routing\Router;
use Exception;
use mikehaertl\wkhtmlto\Pdf;
use Cake\Cache\Cache;

/**
 * Report shell command.
 */
class MigrationShell extends ProjapotiShell
{

    /**
     * main() method.
     *
     * @return bool|int Success or error code.
     */
    public function main()
    {

    }

    private function commands($office_id, $potrojari_id)
    {
        $command = "bin/cake report potrojariReceiverSent {$office_id} {$potrojari_id}> /dev/null 2>&1 &";
    }
    public function migrateDakActions(){
        $tableDakActions = TableRegistry::get('DakActions');
        $tableDakActionsMig = TableRegistry::get('DakActionsMig');
        $tableDakActionEmployees = TableRegistry::get('DakActionEmployees');
        $tableEmployeeOffices = TableRegistry::get('EmployeeOffices');
        $tableUsers = TableRegistry::get('Users');
        //get all data for projapoti DB
        try{
                $allDakActions = $tableDakActions->find()->toArray();
                $tableDakActionsMig->deleteAll(['1 = 1']);
                $tableDakActionEmployees->deleteAll(['1 = 1']);
                $total = 0;$success = 0;
                if(!empty($allDakActions)){
                    foreach($allDakActions as $d_action){
                        $total++;
                        //prepare save in dak action mig
                        if(!empty($d_action['organogram_id'])){
                          $emp=  $tableEmployeeOffices->find()->select(['employee_record_id'])->where(['office_unit_organogram_id' => $d_action['organogram_id']])->order(['created desc'])->first();
                          if(empty($emp)){
                              $emp['employee_record_id'] = 0;
                              $user['id'] = 0;
                          }else{
                              $user = $tableUsers->find()->select(['id'])->where(['employee_record_id' => $emp['employee_record_id']])->first();
                          }
                        }else{
                               $emp['employee_record_id'] = 0;
                                $user['id'] = 0;
                        }
                       $tableDakActionsMigEntity = $tableDakActionsMig->newEntity();
                       $tableDakActionsMigEntity->dak_action_name  = $d_action['dak_action_name'];
                       $tableDakActionsMigEntity->status  = $d_action['status'];
                       $tableDakActionsMigEntity->creator  = $emp['employee_record_id'];
                       $tableDakActionsMigEntity->created_by  = $user['id'];
                       $tableDakActionsMigEntity->modified_by  = $user['id'];

                       $tableDakActionEmployeesEntity = $tableDakActionEmployees->newEntity();
                       $tableDakActionEmployeesEntity->employee_id = $emp['employee_record_id'];
                       $tableDakActionEmployeesEntity->created_by = $user['id'];
                       $tableDakActionEmployeesEntity->modified_by = $user['id'];
                       
                       $dak_action_record = $tableDakActionsMig->save($tableDakActionsMigEntity);
                       if($dak_action_record){
                            $tableDakActionEmployeesEntity->dak_action_id = $dak_action_record->id;
                            if($tableDakActionEmployees->save($tableDakActionEmployeesEntity)){
                                 $success++;
                            }
                       }
                    }
                   $this->out('Total: '.$total.' Saved: '.$success);
        }
        } catch (\Exception $ex) {
            $this->out('Error:' . $ex->getMessage());
        }

    }
    public function migrateNothiActions(){
        $tableNothiDecisions = TableRegistry::get('NothiDecisions');
        $tableNothiDecisionsMig = TableRegistry::get('NothiDecisionsMig');
        $tableNothiDecisionEmployees = TableRegistry::get('NothiDecisionEmployees');
        $tableEmployeeOffices = TableRegistry::get('EmployeeOffices');
        $tableUsers = TableRegistry::get('Users');
        //get all data for projapoti DB
        try{
                $allNothiDecisions = $tableNothiDecisions->find()->toArray();
                $tableNothiDecisionsMig->deleteAll(['1 = 1']);
                $tableNothiDecisionEmployees->deleteAll(['1 = 1']);
                $total = 0;$success = 0;
                if(!empty($allNothiDecisions)){
                    foreach($allNothiDecisions as $n_decision){
                        $total++;
                        //prepare save in dak action mig
                        if(!empty($n_decision['organogram_id'])){
                          $emp=  $tableEmployeeOffices->find()->select(['employee_record_id'])->where(['office_unit_organogram_id' => $n_decision['organogram_id']])->order(['created desc'])->first();
                          if(empty($emp)){
                              $emp['employee_record_id'] = 0;
                              $user['id'] = 0;
                          }else{
                              $user = $tableUsers->find()->select(['id'])->where(['employee_record_id' => $emp['employee_record_id']])->first();
                          }
                        }else{
                               $emp['employee_record_id'] = 0;
                                $user['id'] = 0;
                        }
                       $tableNothiDecisionsMigEntity = $tableNothiDecisionsMig->newEntity();
                       $tableNothiDecisionsMigEntity->decisions  = $n_decision['decisions'];
                       $tableNothiDecisionsMigEntity->status  = $n_decision['status'];
                       $tableNothiDecisionsMigEntity->creator  = $emp['employee_record_id'];
                       $tableNothiDecisionsMigEntity->created_by  = $user['id'];
                       $tableNothiDecisionsMigEntity->modified_by  = $user['id'];

                       $tableNothiDecisionEmployeesEntity = $tableNothiDecisionEmployees->newEntity();
                       $tableNothiDecisionEmployeesEntity->employee_id = $emp['employee_record_id'];
                       $tableNothiDecisionEmployeesEntity->created_by = $user['id'];
                       $tableNothiDecisionEmployeesEntity->modified_by = $user['id'];

                       $nothi_decision_record = $tableNothiDecisionsMig->save($tableNothiDecisionsMigEntity);
                       if($nothi_decision_record){
                            $tableNothiDecisionEmployeesEntity->nothi_decision_id = $nothi_decision_record->id;
                            if($tableNothiDecisionEmployees->save($tableNothiDecisionEmployeesEntity)){
                                 $success++;
                            }
                       }
                    }
                   $this->out('Total: '.$total.' Saved: '.$success);
        }
        } catch (\Exception $ex) {
            $this->out('Error:' . $ex->getMessage());
        }

    }
    public function potrojariGroupUsersNameUpdate(){
        ini_set('memory_limit', -1);
        set_time_limit(0);
        $potrojariGroupsMigTable= TableRegistry::get('PotrojariGroupsUsers');
        $employeeRecordsTable = TableRegistry::get('EmployeeRecords');

        try{
            $office_unit_organogram_ids = $potrojariGroupsMigTable->find('list', [
                'keyField' => 'office_unit_organogram_id',
                'valueField' => 'office_unit_organogram_id'
            ])->distinct(['office_unit_organogram_id'])->where(['office_unit_organogram_id <>'=>0])->toArray();

            $employee_records= $employeeRecordsTable->find()->select(['EmployeeRecords.id','EmployeeRecords.name_eng','EmployeeRecords.name_bng','EmployeeRecords.personal_email','office_unit_organogram_id'=>'EmployeeOffices.office_unit_organogram_id'])
                ->join([
                    'EmployeeOffices' => [
                        'table' => 'employee_offices',
                        "conditions" => "EmployeeOffices.employee_record_id= EmployeeRecords.id",
                        "type" => "INNER"
                    ]
                ])
                ->where(['EmployeeOffices.office_unit_organogram_id IN'=>$office_unit_organogram_ids])
                ->where(['EmployeeOffices.status'=>1])
                ->toArray();
            $total_record = 0;
            $updated=0;
            $not_updated=0;
            foreach ($employee_records as $employee_record){
                $total_record++;

                if($potrojariGroupsMigTable->updateAll(['employee_name_eng'=>$employee_record['name_eng'], 'employee_name_bng'=>$employee_record['name_bng'],'officer_email'=>$employee_record['personal_email']],['office_unit_organogram_id'=>$employee_record['office_unit_organogram_id']])){
                    $updated++;
                } else {
                    $not_updated++;
                }
            }
            $this->out(['Total Users: '.$total_record.', Updated: '.$updated.', Not Updated: '.$not_updated]);
        }catch(\Exception $ex){
            $this->out($ex->getMessage());
        }
    }
    public function NothiChangeHistoryJsonToSerialiseMigration(){
        ini_set('memory_limit', -1);
        set_time_limit(0);
        $office_domains = TableRegistry::get('OfficeDomains');
        $all_offices = $office_domains->getOfficesData();
        $total = 0;
        foreach ($all_offices as $office_id => $office_name) {
            if (empty($office_id)) {
                continue;
            }
            try {
                $this->switchOffice($office_id, 'OfficeDB');
                TableRegistry::remove('NothiDataChangeHistory');
                $nothi_data_change_history_table = TableRegistry::get('NothiDataChangeHistory');
                $all_data = $nothi_data_change_history_table->find()->toArray();
                if (empty($all_data)) {
                    continue;
                }

                $total++;
                $i = 0;
                foreach ($all_data as $data) {
                    $nothi_data = serialize(json_decode($data['nothi_data'], true));
                    $nothi_data_change_history_table->updateAll(['nothi_data' => $nothi_data], ['id' => $data['id']]);
                    $i++;
                }
                $this->out(['Office ' . $office_id . ', Total ' . $i . ' rows updated.']);
            } catch (\Exception $ex) {
                $this->out(['Office: ' . $office_id . ' Office name: ' . $office_name . ' msg: ' . $ex->getMessage()]);
                continue;
            }
        }
        $this->out(['Total '.$total.' office updated.']);
    }
    private function switchOffice($office_id, $officeName)
    {
        $officeDomainTable = TableRegistry::get('OfficeDomains');
        $officeDomain = $officeDomainTable->find()->where(['office_id' => $office_id])->first();

        if (empty($officeDomain)) {
            throw new Exception("অফিস ডাটাবেজ পাওয়া যায় নি! সাপোর্ট টিমের সাথে যোগাযোগ করুন");
        }

        ConnectionManager::drop('default');
        ConnectionManager::drop($officeName);
        ConnectionManager::config($officeName,
            [
                'className' => 'Cake\Database\Connection',
                'driver' => 'Cake\Database\Driver\Mysql',
                'persistent' => false,
                'host' => $officeDomain['domain_host'],
                'username' => $officeDomain['domain_username'],
                'password' => $officeDomain['domain_password'],
                'database' => $officeDomain['office_db'],
                'encoding' => 'utf8',
                'cacheMetadata' => true,
            ]
        );

        ConnectionManager::alias($officeName, 'default');
    }
    public function addCustomLayerID(){
        $officeTbl = TableRegistry::get('Offices');
        $offices =$officeTbl->find()->select(['id','office_layer_id'])->toArray();
        if(!empty($offices)){
            foreach($offices as $val){
                if(empty($val['office_layer_id'])){
                    $officeTbl->updateAll(['custom_layer_id' => 6],['id' => $val['id']]);
                }else{
                    $officeLayerInfo = TableRegistry::get('OfficeLayers')->getLayerLevel($val['office_layer_id']);
                    if(!empty($officeLayerInfo) && $officeLayerInfo['layer_level']>0 && $officeLayerInfo['layer_level']<6){
                        $officeTbl->updateAll(['custom_layer_id' => $officeLayerInfo['layer_level']],['id' => $val['id']]);
                    }else{
                        $officeTbl->updateAll(['custom_layer_id' => 6],['id' => $val['id']]);
                    }

                }
            }
        }
    }

    public function updatePotrojariGroupUsersOfficeHeadByEmployeeOfficesShowUnit(){
        ini_set('memory_limit', -1);
        set_time_limit(0);

        try {
            $potrojari_groups_users_table = TableRegistry::get('PotrojariGroupsUsers');
            $employee_offices_table =  TableRegistry::get('EmployeeOffices');
            $system_user = 0;
            $updated_user = 0;
            $updated_office_head = 0;
            $showed_unit = 0;
            $not_showed_unit = 0;

            $all_users = $potrojari_groups_users_table->find('all')->distinct(['office_unit_organogram_id'])->hydrate(false)->toArray();
            $total_user = count($all_users);
            foreach ($all_users as $user){
                if(!empty($user['office_unit_organogram_id'])){
                    ++$system_user;
                    $employee_offices_user = $employee_offices_table->find()->where(['office_unit_organogram_id'=>$user['office_unit_organogram_id'],'status'=>1])->first();

                    if($employee_offices_user['office_head']==1){
                        $potrojari_groups_users_table->updateAll(['office_head' => 1], ['office_unit_organogram_id' => $user['office_unit_organogram_id']]);
                        ++$updated_user;
                        ++$updated_office_head;
                    } else {
                        if($employee_offices_user['show_unit']==1){
                            $potrojari_groups_users_table->updateAll(['office_head' => 0], ['office_unit_organogram_id' => $user['office_unit_organogram_id']]);
                            ++$updated_user;
                            ++$showed_unit;
                        } else if($employee_offices_user['show_unit']==0){
                            $potrojari_groups_users_table->updateAll(['office_head' => 1], ['office_unit_organogram_id' => $user['office_unit_organogram_id']]);
                            ++$updated_user;
                            ++$not_showed_unit;
                        }
                    }
                }

            }

        } catch (\Exception $ex) {
            $this->out($ex->getMessage());
        }
        $this->out('all user: '.$total_user.', system user: '.$system_user.', updated user: '.$updated_user.', updated office head: '.$updated_office_head.', unit showed: '.$showed_unit.', unit not showed: '.$not_showed_unit);

    }

    public function OfficeWisePerformanceUnitTablesMigrationUpdate($date = '2018-12-31')
    {
        ini_set('memory_limit', -1);
        set_time_limit(0);

        $tbl_CronRequest = TableRegistry::get('CronRequest');
        $offcieDomainsTable = TableRegistry::get('OfficeDomains');
        $domain_array = $offcieDomainsTable->getUniqueDomainHost();
        $max_length = 0;
        $host = [];
        $offices_ids = [];
        if (!empty($domain_array)) {
            foreach ($domain_array as $DA) {
                $allOfficeIds = $offcieDomainsTable->getHostWiseOfficeID($DA);
                $toalOffices = count($allOfficeIds);
                if ($max_length < $toalOffices) {
                    $max_length = $toalOffices;
                }
                $host[$DA] = array_values($allOfficeIds);
            }
            for ($i = 0; $i < $max_length; $i++) {
                foreach ($domain_array as $DA) {
                    if (isset($host[$DA][$i])) {
                        $offices_ids[] = $host[$DA][$i];
                    }
                }
            }
        }
        if (!empty($offices_ids)) {
            foreach ($offices_ids as $id) {
                $command = ROOT . "/bin/cake migration performanceUnitUpdateByOfficeIDMigration {$id} {$date}> /dev/null 2>&1 &";
                $tbl_CronRequest->saveData($command);
//                $this->performanceUnitUpdateByOfficeIDMigration($id,$date);
            }
        }
        if (!empty($domain_array)) {
            $run_request = count($domain_array) * 2;
            for ($indx = 1; $indx <= $run_request; $indx++) {
                $command = ROOT . "/bin/cake cron runCronRequest";
                exec($command);
                sleep(1);
            }
        }
    }

    public function performanceUnitUpdateByOfficeIDMigration($office_id, $date_start = '')
    {
        set_time_limit(0);
        ini_set('memory_limit', -1);
        $this->switchOffice($office_id, 'OfficeDB');


        $unitsTable = TableRegistry::get('OfficeUnits');
        $employee_offices = TableRegistry::get('EmployeeOffices');
        $all_units = $employee_offices->getAllUnitID()->where(['EmployeeOffices.office_id' => $office_id])->toArray();
        $performanceUnitTable = TableRegistry::get('PerformanceUnits');
        $reportsTable = TableRegistry::get('Reports');
        $MinistriesTable = TableRegistry::get('OfficeMinistries');
        $LayersTable = TableRegistry::get('OfficeLayers');
        $OfficesTable = TableRegistry::get('Offices');
        $UnitOriginsTable = TableRegistry::get('OfficeOriginUnits');
        $performance_designationTable = TableRegistry::get('PerformanceDesignations');
        TableRegistry::remove('DakUsers');
        TableRegistry::remove('NothiMasterCurrentUsers');


        foreach ($all_units as $unit_id => $unit_name) {
            $period = [];
            $unit_record = $performanceUnitTable->getLastUpdateTime($unit_id);
            if (!empty($date_start)) {
                $begin = new \DateTime($date_start);
            } else if (!empty($unit_record)) {
                $begin = ($unit_record['record_date']);
            } else {
                $begin = new \DateTime(date('Y-m-d', strtotime(date('Y-m-d') . ' -1 week')));
            }
            if (!empty($date_start)) {
                $end = new \DateTime($date_start);
            }else{
                $end = new \DateTime(date('Y-m-d'));
            }

            for ($i = $begin; $begin <= $end; $i->modify('+1 day')) {
                $period [] = $i->format("Y-m-d");
            }
            if (!empty($period)) {
                try {
                    $unit_related_data = $unitsTable->getAll(['id' => $unit_id],
                        ['office_ministry_id', 'office_layer_id', 'office_origin_unit_id', 'office_id'])->first();
                    foreach ($period as $dt) {
                        $has_record = $performanceUnitTable->find()->where(['record_date' => $dt, 'unit_id' => $unit_id])->count();
                        if ($has_record == 0) {
                            $this->out('No record found.Unit id: '.$unit_id.' For: '.$dt);
                            continue;
                        }
                        $date = $dt;
//                        pr($unit_related_data['office_id']);die;
                        // Collect From Designation Table
                        $from_designation_report = true;
                        $all_designation = $employee_offices->getAllDesignationByOfficeOrUnitID($unit_related_data['office_id'],
                            $unit_id)->toArray();
                        $condition = [
                            'totalInbox' => 'SUM(inbox)', 'totalOutbox' => 'SUM(outbox)', 'totalNothijato' => 'SUM(nothijat)',
                            'totalNothivukto' => 'SUM(nothivukto)', 'totalNisponnoDak' => 'SUM(nisponnodak)', 'totalpotrojariall' => 'SUM(potrojari)',
                            'totalSouddog' => 'SUM(selfnote)', 'totalONisponnoDak' => 'SUM(onisponnodak)',
                            'totalDaksohoNote' => 'SUM(daksohonote)', 'totalNisponnoPotrojari' => 'SUM(nisponnopotrojari)',
                            'totalNisponnoNote' => 'SUM(nisponnonote)', 'totalONisponnoNote' => 'SUM(onisponnonote)',
                            'totalNisponno' => 'SUM(nisponno)', 'totalONisponno' => 'SUM(onisponno)',
                            'totalID' => 'count(id)', 'designation_id'
                        ];
                        if (!empty($all_designation)) {
                            $hasDataInPerformanceDesignation = $performance_designationTable->getUnitData([$unit_id], [$date, $date])->select($condition)->group(['unit_id'])->toArray();
                            if (!empty($hasDataInPerformanceDesignation)) {
                                $returnSummary['totalInbox'] = 0;
                                $returnSummary['totalOutbox'] = 0;
                                $returnSummary['totalNothijato'] = 0;
                                $returnSummary['totalNothivukto'] = 0;
                                $returnSummary['totalNisponnoDak'] = 0;
                                $returnSummary['totalONisponnoDak'] = 0;
                                $returnSummary['totalPotrojari'] = 0;
                                $returnSummary['totalSouddog'] = 0;
                                $returnSummary['totalDaksohoNote'] = 0;
                                $returnSummary['totalNisponnoNote'] = 0;
                                $returnSummary['totalNisponnoPotrojari'] = 0;
                                $returnSummary['totalONisponnoNote'] = 0;
                                $returnSummary['totalNisponno'] = 0;
                                $returnSummary['totalONisponno'] = 0;
                                foreach ($hasDataInPerformanceDesignation as $val_form_designation) {
                                    $returnSummary['totalInbox'] += $val_form_designation['totalInbox'];
                                    $returnSummary['totalOutbox'] += $val_form_designation['totalOutbox'];
                                    $returnSummary['totalNothijato'] += $val_form_designation['totalNothijato'];
                                    $returnSummary['totalNothivukto'] += $val_form_designation['totalNothivukto'];
                                    $returnSummary['totalNisponnoDak'] += $val_form_designation['totalNisponnoDak'];
//                              $returnSummary['totalONisponnoDak'] += TableRegistry::get('DakUsers')->getOnisponnoDak($unit_related_data['office_id'], $unit_id);
                                    $returnSummary['totalONisponnoDak'] += $val_form_designation['totalONisponnoDak'];
                                    $returnSummary['totalPotrojari'] += $val_form_designation['totalpotrojariall'];
                                    $returnSummary['totalSouddog'] += $val_form_designation['totalSouddog'];
                                    $returnSummary['totalDaksohoNote'] += $val_form_designation['totalDaksohoNote'];
                                    $returnSummary['totalNisponnoNote'] += $val_form_designation['totalNisponnoNote'];
                                    $returnSummary['totalNisponnoPotrojari'] += $val_form_designation['totalNisponnoPotrojari'];
//                              $returnSummary['totalONisponnoNote'] += TableRegistry::get('NothiMasterCurrentUsers')->getAllNothiPartNo($unit_related_data['office_id'], $unit_id,0, [$date,$date])->count();
                                    $returnSummary['totalONisponnoNote'] += $val_form_designation['totalONisponnoNote'];
                                    $returnSummary['totalNisponno'] += $val_form_designation['totalNisponno'];
                                    $returnSummary['totalONisponno'] += $val_form_designation['totalONisponno'];
                                }
                            } else {
                                $from_designation_report = false;
                            }
                        } else {
                            $from_designation_report = false;
                        }

                        if ($from_designation_report == false) {
                            $returnSummary = $reportsTable->newPerformanceReport($unit_related_data['office_id'],
                                $unit_id, 0, ['date_start' => $date, 'date_end' => $date]);
                        }

                        //get id of performanceUnit table record
                        $performanceUnitTableInfo = $performanceUnitTable->find()->select(['id'])->where(['record_date' => $dt, 'unit_id' => $unit_id])->first();
                        if(empty($performanceUnitTableInfo['id'])){
                            $this->out('No record found. For: '.$dt.' Unit id: '.$unit_id);
                            continue;
                        }

                        $performance_units_entity = $performanceUnitTable->get($performanceUnitTableInfo['id']);

//                        $performance_units_entity->inbox = $returnSummary['totalInbox'];
//                        $performance_units_entity->outbox = $returnSummary['totalOutbox'];
//                        $performance_units_entity->nothijat = $returnSummary['totalNothijato'];
//                        $performance_units_entity->nothivukto = $returnSummary['totalNothivukto'];
//                        $performance_units_entity->nisponnodak = $returnSummary['totalNisponnoDak'];
                        $performance_units_entity->onisponnodak = $returnSummary['totalONisponnoDak'];

//                        $performance_units_entity->potrojari = $returnSummary['totalPotrojari'];
                        if ($returnSummary['totalSouddog'] < 0) {
                            $returnSummary['totalSouddog'] = 0;
                        }
//                        $performance_units_entity->selfnote = $returnSummary['totalSouddog'];
//                        $performance_units_entity->daksohonote = $returnSummary['totalDaksohoNote'];
//                        $performance_units_entity->nisponnopotrojari = $returnSummary['totalNisponnoPotrojari'];
//                        $performance_units_entity->nisponnonote = $returnSummary['totalNisponnoNote'];
                        $performance_units_entity->onisponnonote = $returnSummary['totalONisponnoNote'];

//                        $performance_units_entity->nisponno = $returnSummary['totalNisponno'];
                        $performance_units_entity->onisponno = $returnSummary['totalONisponno'];

//                        $performance_units_entity->office_id = $unit_related_data['office_id'];
//                        $performance_units_entity->unit_id = $unit_id;
//                        $performance_units_entity->ministry_id = $unit_related_data['office_ministry_id'];
//                        $performance_units_entity->layer_id = $unit_related_data['office_layer_id'];
//                        $performance_units_entity->origin_id = $unit_related_data['office_origin_unit_id'];
//                        $performance_units_entity->unit_name = $unit_name;

//                        if (!($office_info = Cache::read('office_info_' . $unit_related_data['office_id']))) {
//                            $office_info = $OfficesTable->getBanglaName($unit_related_data['office_id']);
//                            Cache::write('office_info_' . $unit_related_data['office_id'], $office_info);
//                        }
//
//                        $performance_units_entity->office_name = $office_info['office_name_bng'];

//                        if (!($ministry_info = Cache::read('ministry_info_' . $unit_related_data['office_ministry_id']))) {
//                            $ministry_info = $MinistriesTable->getBanglaName($unit_related_data['office_ministry_id']);
//                            Cache::write('ministry_info_' . $unit_related_data['office_ministry_id'], $ministry_info);
//                        }
//
//                        $performance_units_entity->ministry_name = $ministry_info['name_bng'];

//                        if (!($layer_info = Cache::read('layer_info_' . $unit_related_data['office_layer_id']))) {
//                            $layer_info = $LayersTable->getBanglaName($unit_related_data['office_layer_id']);
//                            Cache::write('layer_info_' . $unit_related_data['office_layer_id'], $layer_info);
//                        }
//
//                        $performance_units_entity->layer_name = $layer_info['layer_name_bng'];
//
//                        if (!($origin_info = Cache::read('origin_info_' . $unit_related_data['office_origin_unit_id']))) {
//                            $origin_info = $UnitOriginsTable->getBanglaName($unit_related_data['office_origin_unit_id']);
//                            Cache::write('origin_info_' . $unit_related_data['office_origin_unit_id'], $origin_info);
//                        }
//
//                        $performance_units_entity->origin_name = $origin_info['unit_name_bng'];

//                        $performance_units_entity->status = 1;
//                        $performance_units_entity->record_date = $date;

                        $performanceUnitTable->save($performance_units_entity);
                    }
//                    $this->updateIndividualUnit($unit_id);
//                    $this->deleteMultipleRowsOfUnits($unit_id, !empty($period[0]) ? $period[0] : '');
                } catch (\Exception $ex) {
                    $this->out($ex->getMessage());
                }
            }
        }
        $this->out('Unit Migration Done For Office: ' . $office_id);
        $command = ROOT . "/bin/cake cron runCronRequest";
        exec($command);
    }
}