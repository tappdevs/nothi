<?php

namespace App\Shell;

use Cake\Console\Shell;
use Cake\ORM\TableRegistry;
use App\Controller\ProjapotiController;
use Cake\Datasource\ConnectionManager;
use Cake\I18n\I18n;
use Cake\Network\Email\Email;
use Cake\I18n\Number;
use Cake\I18n\Time;
use Cake\Routing\Route\Route;
use Cake\Routing\Router;
use Exception;
use mikehaertl\wkhtmlto\Pdf;
use Cake\Cache\Cache;
use Cake\Database\Expression\QueryExpression;

/**
 * Report shell command.
 */
class NotificationShell extends ProjapotiShell
{
    public function OfficeWisePendingPotrojari($start = '',$end = ''){
        ini_set('memory_limit', -1);
        set_time_limit(0);
        $start_time = strtotime(date('Y-m-d H:i:s'));

        $officesTable = TableRegistry::get('Offices');
        $office_domains = TableRegistry::get('OfficeDomains');
        $all_offices = $office_domains->getOfficesData();
        if(empty($start)){
            $start = date('Y-m-d', strtotime(date('Y-m-d') . ' -1 week'));
        }
        if(empty($end)){
            $end = date('Y-m-d');
        }
        $html ='';
        $html .= '<table style="border-collapse: collapse;border: 1px solid grey;" cellpadding="10">
                    <thead>
                        <tr>
                            <th style="border: 1px solid grey;"> ক্রম </th>
                            <th style="border: 1px solid grey;"> অফিস </th>
                            <th style="border: 1px solid grey;"> পত্রজারি প্রাপক অপেক্ষমাণ </th>
                            <th style="border: 1px solid grey;"> পত্রজারি অনুলিপি অপেক্ষমাণ </th>
                        </tr>
                    </thead>
                    <tbody>';
        $indx = 0;
        $total_falied_receiver = 0;
        $total_falied_onulipi = 0;
        $failed_html ='';
        $critical = 0;
        foreach ($all_offices as $office_id => $office_name) {
            try {
                $this->switchOffice($office_id, 'DashboardOffices');
                TableRegistry::remove('PotrojariReceiver');TableRegistry::remove('PotrojariOnulipi');TableRegistry::remove('Potrojari');
                $p_receiver = TableRegistry::get('PotrojariReceiver');
                $failed_Potrojari_receiver = $p_receiver->find()->where(['PotrojariReceiver.is_sent'=>0])->join([
                    "Potrojari" => [
                        'table' => 'potrojari', 'type' => 'inner',
                        'conditions' => ['Potrojari.id =PotrojariReceiver.potrojari_id']
                    ]
                ])->where(['Potrojari.potro_status' => 'Sent']);
                if(!empty($start)){
                    $failed_Potrojari_receiver = $failed_Potrojari_receiver->where(['date(PotrojariReceiver.created) >=' => $start]);
                }
                if(!empty($end)){
                    $failed_Potrojari_receiver = $failed_Potrojari_receiver->where(['date(PotrojariReceiver.created) <=' => $end]);
                }
               $failed_receiver = $failed_Potrojari_receiver->count();
                $p_onulipi = TableRegistry::get('PotrojariOnulipi');
                $failed_Potrojari_onulipi = $p_onulipi->find()->where(['PotrojariOnulipi.is_sent'=>0])->join([
                    "Potrojari" => [
                        'table' => 'potrojari', 'type' => 'inner',
                        'conditions' => ['Potrojari.id =PotrojariOnulipi.potrojari_id']
                    ]
                ])->where(['Potrojari.potro_status' => 'Sent']);
                if(!empty($start)){
                    $failed_Potrojari_onulipi = $failed_Potrojari_onulipi->where(['date(PotrojariOnulipi.created) >=' => $start]);
                }
                if(!empty($end)){
                    $failed_Potrojari_onulipi = $failed_Potrojari_onulipi->where(['date(PotrojariOnulipi.created) <=' => $end]);
                }
               $failed_onulipi = $failed_Potrojari_onulipi->count();
                if($failed_onulipi == 0 && $failed_receiver == 0){
                    continue;
                }
                $total_falied_receiver += $failed_receiver;
                $total_falied_onulipi += $failed_onulipi;
                $style = '';
                if($failed_receiver >= 100 || $failed_onulipi >= 100 ){
                    $critical++;
                    $style ='style="color:red!important"';
                }
                $html .= "<tr {$style}>".'
                            <td style="border: 1px solid grey;text-align: right;">'.enTobn(++$indx).'</td>
                            <td style="border: 1px solid grey;">'.$office_name .' ('.$office_id.')'.'</td>
                            <td style="border: 1px solid grey;text-align: right;">'.enTobn($failed_receiver).'</td>
                            <td style="border: 1px solid grey;text-align: right;">'.entobn($failed_onulipi).'</td>
                        </tr>';
            }catch (\Exception $ex){
                $failed_html .= "<tr {$style}>".'
                            <td style="border: 1px solid grey;">'.$office_name .' ('.$office_id.')'.'</td>
                            <td style="border: 1px solid grey;text-align: right; color:red">'.$ex->getMessage().'</td>
                        </tr>';
                $this->out($ex->getMessage());
                continue;
            }

        }
        $html .= '<tr>
                    <td style="border: 1px solid grey;text-align: right;" colspan="2">মোটঃ '.enTobn($indx).'</td>
                    <td style="border: 1px solid grey;text-align: right;">'.enTobn($total_falied_receiver).'</td>
                    <td style="border: 1px solid grey;text-align: right;">'.entobn($total_falied_onulipi).'</td>
                </tr>';
        $html .= '</tbody></table>';
        $response = "প্রিয় কর্তৃপক্ষ,<br> অফিসভিত্তিক অপেক্ষমাণ পত্রজারি রিপোর্ট [তারিখঃ ".entobn(date('Y-m-d'))."] তৈরি প্রক্রিয়া সম্পন্ন হয়েছে। নিম্নে বিস্তারিত তথ্য পেশ করা হলঃ <br>পত্রজারি অপেক্ষমাণ এমন অফিসের সংখ্যাঃ " . $indx;
        $end_time = strtotime(date('Y-m-d H:i:s'));
        $time_difference = round(abs(($end_time - $start_time) / 60), 2);
        $response .= "<br> প্রতিবেদন তৈরি করতে সময় লেগেছেঃ " . $time_difference . " মিনিট";
        $response .= "<br> {$html}";

        if($failed_html != ''){
            $response .= "<br> প্রতিবেদন তৈরি করতে যেয়ে যেসব অফিসে ত্রুটি হয়েছে তার বর্ণনা নিম্নরূপঃ <br> ";
            $response .= '<table style="border-collapse: collapse;border: 1px solid grey;" cellpadding="10">
                    <thead>
                        <tr>
                            <th style="border: 1px solid grey;"> অফিস </th>
                            <th style="border: 1px solid grey;"> ত্রুটি </th>
                        </tr>
                    </thead>
                    <tbody>'.$failed_html.'</tbody></table>';
        }
        if (NOTIFICATION_OFF == 0) {
            if(empty($critical)){
                $params = ['to_email' => 'mhasan.a2i@gmail.com','to_cc' => 'tushar@tappware.com','subject' => "[NNS] Office-wise pending potrojari Report [Date - ".date('Y-m-d')."]",'email_body'=> $response];
            }else{
                $params = ['to_email' => 'mhasan.a2i@gmail.com','to_cc' => 'tushar@tappware.com','subject' => "[NNS] Office-wise pending potrojari Report [Date - ".date('Y-m-d')."] (Critical)",'email_body'=> $response];
            }
            $this->sendEmailByEmailer($params);


        }
        $this->saveCronLog("[NNS] Office-wise pending potrojari Report [Date - ".date('Y-m-d')."]", date('Y-m-d'), $start_time, $end_time,
            $indx, $indx, 0, $response);
    }
    // GIT#1169 Subdomain checking & email report with attachment. Here 2 method is needed for this process [checkAllDomain, url_test]
    // These methods write here for testing. Please move these methods to Shell controller for cron job
    public function mailExcelDomainReport() {
        try {
            set_time_limit('0');
            ini_set('memory_limit', '-1');
            require ROOT . DS . 'vendor' . DS . 'phpoffice' . DS . 'phpexcel' . DS . 'Classes' . DS . 'PHPExcel.php';
            require ROOT . DS . 'vendor' . DS . 'phpoffice' . DS . 'phpexcel' . DS . 'Classes' . DS . 'PHPExcel' . DS . 'IOFactory.php';

            $book = new \PHPExcel();
            $text = 'Office Domain Checking Report';
            $book->getActiveSheet()->setTitle('Office Domain Checking');
            $sheet = $book->getActiveSheet();

            $style = array(
                'font' => array('bold' => true),
                'alignment' => array('horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,),
            );
            $sheet->setCellValueByColumnAndRow(0, 1, $text); // Sets cell 'a1' to value 'ID
            $sheet->mergeCells('A1:E1');

            $sheet->setCellValueByColumnAndRow(0, 2, 'Serial'); // Sets cell 'a2' to value 'ID
            $sheet->setCellValueByColumnAndRow(1, 2, 'Domain URL'); // Sets cell 'a2' to value 'ID
            $sheet->setCellValueByColumnAndRow(2, 2, 'Status'); // Sets cell 'b2' to value 'ID
            $sheet->setCellValueByColumnAndRow(3, 2, 'Total Time'); // Sets cell 'c2' to value 'ID
            $sheet->setCellValueByColumnAndRow(4, 2, 'Connect Time'); // Sets cell 'e2' to value 'ID

            $indx = 3;
            $nothi_report_db = ConnectionManager::get('NothiReportsDb');
            $domain_reports_table = TableRegistry::get('domain_reports', ['connection' => $nothi_report_db]);
            $activeOffices = $domain_reports_table->find('all')->toArray();
            $error_list = [];
            foreach ($activeOffices as $key => $response) {
                $response['total_time'] = number_format($response['total_time'], 3);
                $response['connect_time'] = number_format($response['connect_time'], 3);
                if ($response['status'] == 'error') {
                    $error_list[] = $response;
                } else {
                    if ($response['total_time'] > 10) {
                        $response['status'] = 'slow';
                        $error_list[] = $response;
                    }
                }
                $data_tmp = array(
                    $key+1,
                    $response['url'],
                    $response['status'],
                    $response['total_time'],
                    $response['connect_time'],
                );
                for ($col = 0; $col < 5; $col++) {
                    $sheet->setCellValueByColumnAndRow($col, $indx, $data_tmp[$col]);
                }
                $indx++;
            }

            for ($col = 0; $col < 5; $col++) {
                $sheet->getStyleByColumnAndRow($col, 1)->applyFromArray(array(
                    'font' => array('bold' => true, 'size' => 12),
                    'alignment' => array('horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,),
                ));
                $sheet->getStyleByColumnAndRow($col, 2)->applyFromArray($style);
                $sheet->getColumnDimensionByColumn($col)->setAutoSize(true);
            }
            //header("Content-Type: application/vnd.ms-excel");
            //header("Content-Disposition: attachment; filename=\".$text.xls\"");
            //header("Cache-Control: max-age=0");
            $writer = \PHPExcel_IOFactory::createWriter($book, 'Excel2007');

            // $writer->setIncludeCharts(true);
            if (ob_get_contents()) ob_end_clean();
            //$writer->save('php://output');
            $writer->save(FILE_FOLDER_DIR.'office_domain_checking.xlsx');

            //check excel exist
            if(!file_exists(FILE_FOLDER_DIR.'office_domain_checking.xlsx')){
//                $this->out('File not exist: '.FILE_FOLDER_DIR.'office_domain_checking.xlsx');
            }

            //$email = new Email('default');
            $message = 'Dear Concern,<br> Office Domain Checking Report - '.date('Y-m-d').' is ready.For details, please find the attachment below.<br>  <span style="background-color:purple;color:white;padding: 10px 20px;border-radius: 0 5px 5px 0;margin: 10px 0;float:left;solid 2px #460046;border-left: none;text-transform: uppercase;">Total Error: <b>'.count($error_list).'</b></span><br><br>
<table class="tableMail" style="width:100%">
    <thead>
    <tr>
        <th style="border:solid 1px silver">#</th>
        <th style="border:solid 1px silver">Domain</th>
        <th style="border:solid 1px silver">Status</th>
        <th style="border:solid 1px silver">Total Time</th>
        <th style="border:solid 1px silver">Connect Time</th>
    </tr>
    </thead>
    <tbody>';
            if (count($error_list) > 0) {
                foreach ($error_list as $key => $value) {
                    $message .= '<tr><td style="border:solid 1px silver">' . ($key + 1) . '</td><td style="border:solid 1px silver">' . $value['url'] . '</td><td style="border:solid 1px silver">' . ucfirst($value['status']) . '</td><td style="border:solid 1px silver">' . $value['total_time'] . ' sec</td><td style="border:solid 1px silver">' . $value['connect_time'] . ' sec</td></tr>';
                }
            } else {
                $message .= '<tr><td style="border:solid 1px silver" colspan="5">No Error Found</td></tr>';
            }

            $message .='</tbody>
</table>

<br><br>
Thanks,<br>
Nothi Team';
//            $email->emailFormat('html')->to('jbhasan@gmail.com')->from('nothi@nothi.org.bd')->subject("Office Domain Checking Report - ".date('Y-m-d'))->attachments(['OfficeDomainCheckingReport.xlsx' => FILE_FOLDER_DIR.'office_domain_checking.xlsx'])->send($message);
            if (NOTIFICATION_OFF == 0) {
                $params = ['to_email' => 'mhasan.a2i@gmail.com','to_cc' => 'tushar@tappware.com','to_bcc'=> 'tushar@tappware.com','subject' => "[NNS] Office Domain Report [Date - ".date('Y-m-d')."]",'email_body'=> $message,'attachments' => ['office_domain_checking.xlsx' => ['file' => FILE_FOLDER_DIR.'office_domain_checking.xlsx'] ] ];
                $this->sendEmailByEmailer($params);
            }
            exit;
        } catch (\Exception $ex) {
            $this->out($ex->getMessage());
        }
    }
    public function checkAllDomain($quantity_per_request, $number_of_request) {
        $activeOffices = TableRegistry::get('OfficeDomains')->find()->where(['status' => 1])->limit($quantity_per_request)->page($number_of_request);
        $error_list = [];
        $nothi_report_db = ConnectionManager::get('NothiReportsDb');
        $domain_reports_table = TableRegistry::get('domain_reports', ['connection' => $nothi_report_db]);
        foreach ($activeOffices as $key => $office) {
            $response = $this->url_test(trim($office->domain_url, '/'));
            if ($response['status'] == 'error') {
                $error_list[] = $response;
            } else {
                if ($response['total_time'] > 10) {
                    $response['status'] = 'slow';
                    $error_list[] = $response;
                }
            }
            $domain_reports = $domain_reports_table->newEntity();
            $domain_reports->office_id = $office->office_id;
            $domain_reports->url = $response['url'];
            $domain_reports->status = $response['status'];
            $domain_reports->total_time = $response['total_time'];
            $domain_reports->connect_time = $response['connect_time'];
            $domain_reports->created_at = date('Y-m-d H:i:s');
            $domain_reports_table->save($domain_reports);
        }
    }
    public function url_test( $url ) {

        $new_url = explode('//', $url);
        $new_url = end($new_url);
        $ch = curl_init();
        curl_setopt ( $ch, CURLOPT_URL, $new_url );
        curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 1 );
        curl_exec($ch);

        //$timeout = 10;
        //curl_setopt ( $ch, CURLOPT_TIMEOUT, $timeout );
        //$http_respond = trim( strip_tags( $http_respond ) );
        $curl_info = curl_getinfo( $ch );
        if ( $curl_info['primary_ip'] != '' ) {
            $response = [
                'status' => 'success',
                'url' => $curl_info['url'],
                'http_code' => $curl_info['http_code'],
                'header_size' => $curl_info['header_size'],
                'ssl_verify_result' => $curl_info['ssl_verify_result'],
                'total_time' => $curl_info['total_time'],
                'connect_time' => $curl_info['connect_time'],
                'primary_ip' => $curl_info['primary_ip'],
                'primary_port' => $curl_info['primary_port'],
            ];
        } else {
            $response = [
                'status' => 'error',
                'url' => $curl_info['url'],
                'http_code' => $curl_info['http_code'],
                'header_size' => $curl_info['header_size'],
                'ssl_verify_result' => $curl_info['ssl_verify_result'],
                'total_time' => $curl_info['total_time'],
                'connect_time' => $curl_info['connect_time'],
                'primary_ip' => $curl_info['primary_ip'],
                'primary_port' => $curl_info['primary_port'],
            ];
        }
        curl_close( $ch );
        return $response;
    }
    public function getActiveDomains() {
//        $this->out('Process started');
        $nothi_report_db = ConnectionManager::get('NothiReportsDb');
        $nothi_report_db->execute(sprintf('TRUNCATE TABLE domain_reports'));

        $active_domains = TableRegistry::get('OfficeDomains')->find()->where(['status' => 1])->count();
        $quantity_per_request = 500;
        for ($page=1; $page<=ceil($active_domains/$quantity_per_request); $page++) {
//            $this->out('CRON: '.$page.' is running');
            //$this->out('CRON: '.ROOT . "/bin/cake API checkAllDomain $quantity_per_request $page > /dev/null 2>&1 &");
            exec(ROOT . "/bin/cake notification checkAllDomain $quantity_per_request $page > /dev/null 2>&1 &");
        }
//        $this->out('Successfully Generated CRON(s)');
    }

    public function checkNothiMailerAliveOrNot() {
//        $this->out('Process started');
        if (!EMAIL_QUEUE_URL) {
            $EMAIL_QUEUE_URL = 'http://103.48.17.140/nothiemailer/api/email_queues.json';
        } else {
            $EMAIL_QUEUE_URL = EMAIL_QUEUE_URL;
        }
        $response = $this->url_test(trim($EMAIL_QUEUE_URL, '/'));
        if ($response['status'] == 'error') {
            $message = 'Dear Concern,<br><b style="color:red;">Mailer server is not running properly</b>. Last checking time: '.date("Y-m-d H:i:s").'<br>';
            $message .='<br><br>
Thanks,<br>
Nothi Team';

            $email = new Email('default');
            $email->emailFormat('html')->to('mhasan.a2i@gmail.com')->cc('tushar@tappware.com')->bcc('tushar@tappware.com')->from('nothi@nothi.org.bd')->subject("Mailer Checking [Date - ".date('Y-m-d')."]")->send($message);
        } else {
            $message = 'Dear Concern,<br>Mailer server is running properly. Last checking time: '.date("Y-m-d H:i:s").'<br>';
            $message .='<br><br>
Thanks,<br>
Nothi Team';
            if (NOTIFICATION_OFF == 0) {
                $params = ['to_email' => 'mhasan.a2i@gmail.com','to_cc' => 'tushar@tappware.com','to_bcc'=> 'tushar@tappware.com','subject' => "[NNS] Mailer Checking [Date - ".date('Y-m-d')."]",'email_body'=> $message,'attachments' => ['office_domain_checking.xlsx' => ['file' => FILE_FOLDER_DIR.'office_domain_checking.xlsx'] ] ];
                $this->sendEmailByEmailer($params);
            }
        }
//        $this->out('Successfully Generated CRON(s)');
    }

    public function saturdayPendingDakNothiNotification(){
        ini_set('memory_limit', -1);
        set_time_limit(0);
        $total_sms=0;

        $employee_offices_table = TableRegistry::get("EmployeeOffices");
        $employee_records_table = TableRegistry::get("EmployeeRecords");
        $all_active_employee = $employee_offices_table->find()->select(['employee_record_id'])->distinct(['employee_record_id'])->where(['status'=>1])->hydrate(false)->limit(1000)->toArray();

        foreach ($all_active_employee as $each_active_employee){
            $employee_mobile_number = $employee_records_table->get($each_active_employee['employee_record_id'])['personal_mobile'];
            if(empty($employee_mobile_number)){
                continue;
            }

            $employee_office_records = $employee_offices_table->find()->select(['office_id','office_unit_organogram_id'])->where(['status'=>1,'employee_record_id'=>$each_active_employee['employee_record_id']])->hydrate(false)->toArray();

            $office_wise_designations = array();
            foreach ($employee_office_records as $employee_office_record) {
                $office_wise_designations[$employee_office_record['office_id']][] = $employee_office_record['office_unit_organogram_id'];
            }

            $totalNothi =0;
            $totalDak =0;
            if(!empty($office_wise_designations)){

                foreach ($office_wise_designations as $office=>$designations) {
                    try {
                        $this->switchOffice($office, 'OfficeDb');
                        TableRegistry::remove('NothiMasterCurrentUsers');
                        TableRegistry::remove('DakUsers');
                        $nothiCurrentUser = TableRegistry::get('NothiMasterCurrentUsers');
                        $table_instance_dak_user = TableRegistry::get('DakUsers');

                        $totalOfficeWiseNothi = $nothiCurrentUser->getTotalNothiCount($designations, $office);
                        $totalOfficeWiseDak = $table_instance_dak_user->getTotalDakCountBy_employee_designation(DAK_CATEGORY_INBOX, $designations,1);
                        $totalNothi = $totalNothi + (int)$totalOfficeWiseNothi;
                        $totalDak = $totalDak + (int)$totalOfficeWiseDak;

                    } catch (\Exception $ex) {
                        continue;
                    }
                }
            }

            if(!empty($totalDak) || !empty($totalNothi)) {
                if (!empty($totalDak) && !empty($totalNothi)) {
                    $sms=
'প্রিয় ই-নথি ব্যবহারকারী,
ই-নথিতে আপনার ' . enTobn($totalDak) . 'টি ডাক এবং ' . enTobn($totalNothi) . 'টি নথি অনিষ্পন্ন রয়েছে।
ধন্যবাদ।';
                } else if (!empty($totalDak) && empty($totalNothi)) {
                    $sms=
'প্রিয় ই-নথি ব্যবহারকারী,
ই-নথিতে আপনার ' . enTobn($totalDak) . 'টি ডাক অনিষ্পন্ন রয়েছে।
ধন্যবাদ।';
                } else {
                    $sms =
'প্রিয় ই-নথি ব্যবহারকারী,
ই-নথিতে আপনার ' . enTobn($totalNothi) . 'টি নথি অনিষ্পন্ন রয়েছে।
ধন্যবাদ।';
                }
                $this->saveSMSRequest($employee_mobile_number, $sms);
                $total_sms++;
            }


        }
//        $this->out('Total sms send: '.$total_sms);
    }
    public function reportUpdateChecker(){
        $date = date('Y-m-d', strtotime(date('Y-m-d') . ' -1 day'));
        $errors= 0;
        $total = 0;
        $html = '';
        $html .= '<table style="border-collapse: collapse;border: 1px solid grey;" cellpadding="10">
                    <thead>
                        <tr>
                            <th style="border: 1px solid grey;"> ক্রম </th>
                            <th style="border: 1px solid grey;"> অফিস </th>
                            <th style="border: 1px solid grey;"> সর্বশেষ হালনাগাদ </th>
                        </tr>
                    </thead>
                    <tbody>';
        $style ='';
        try{
            $start_time = strtotime(date('Y-m-d H:i:s'));
            $office_domains = TableRegistry::get('OfficeDomains');
            $all_offices = $office_domains->getOfficesData();
            $total = count($all_offices);
            $cronRequestTable = TableRegistry::get('CronRequest');

            foreach ($all_offices as $office_id => $office_name) {
                $performanceOfficeTable = TableRegistry::get('PerformanceOffices');
                $has_record = $performanceOfficeTable->find()->where(['record_date' => $date, 'office_id' => $office_id])->count();
                if($has_record > 0){
                    continue;
                }
                $errors++;

                $last_record_info = $performanceOfficeTable->getLastUpdateTime($office_id);
                if(!empty($last_record_info['record_date'])){
                    $last_record_date = $last_record_info['record_date'];
                }
                if(empty($last_record_date)){
                    $style ='style="color:red!important"';
                }
                $html .= "<tr {$style}>".'
                            <td style="border: 1px solid grey;text-align: right;">'.enTobn($errors).'</td>
                            <td style="border: 1px solid grey;">'.$office_name .' ('.$office_id.')'.'</td>
                            <td style="border: 1px solid grey;text-align: right;">'.(!empty($last_record_date)?enTobn($last_record_date):'হালনাগাদ হয় নি').'</td>
                        </tr>';
//                $cmd = ROOT . "/bin/cake cronjob updatePerformanceTables {$office_id}> /dev/null 2>&1 &";
//                $cronRequestTable->saveData($cmd);
                $time = 30;
                $cmd = ROOT . "/bin/cake notification runPendingOfficesReport {$office_id} {$time}> /dev/null 2>&1 &";
                $cronRequestTable->saveData($cmd);
            }
        }catch (\Exception $ex){
            $html .= "<tr {$style}>".'
                            <td style="border: 1px solid grey;text-align: right;color:red!important" colspan="3">'.'টেকনিক্যাল ত্রুটিঃ '.$ex->getMessage().'</td>
                        </tr>';
        }
        if(empty($errors)){
            $html .= "<tr {$style}>".'
                            <td style="border: 1px solid grey;text-align: center;" colspan="3">সকল অফিস হালনাগাদ হয়েছে </td>
                        </tr>';
        }
        $html .= '</tbody></table>';
        $response = "প্রিয় কর্তৃপক্ষ,<br> অফিসভিত্তিক রিপোর্ট হালনাগাদ তদন্ত প্রতিবেদন [তারিখঃ ".entobn(date('Y-m-d'))."] তৈরি প্রক্রিয়া সম্পন্ন হয়েছে। নিম্নে বিস্তারিত তথ্য পেশ করা হলঃ <br>হালনাগাদ হয়নি এমন অফিসের সংখ্যাঃ " . $errors;
        $end_time = strtotime(date('Y-m-d H:i:s'));
        $time_difference = round(abs(($end_time - $start_time) / 60), 2);
        $response .= "<br> প্রতিবেদন তৈরি করতে সময় লেগেছেঃ " . $time_difference . " মিনিট";
        $response .= "<br> {$html}";
        $params = ['to_email' => 'mhasan.a2i@gmail.com','to_cc' => 'tushar@tappware.com','subject' => "[NNS] Offices Report Update Checker [Date - ".date('Y-m-d')."] (Error: {$errors})",'email_body'=> $response];
        $this->sendEmailByEmailer($params);
        $this->saveCronLog("[NNS] Offices Report Update Checker [Date - ".date('Y-m-d')."]", date('Y-m-d'), $start_time, $end_time,
            $total, $total, $errors, $response);

        if($errors > 0){
            $run_cron_limit = $errors/100;
            if(empty($run_cron_limit)){
                $run_cron_limit = 1;
                for($i = 0;$i <= $run_cron_limit;$i++){
                    $this->runCronRequest();
                }
            }
        }
    }
    public function runPendingOfficesReport($id = 0,$time = 5){
//        var str = '';
//        $('table tr td:nth-child(2)').each(function(){
//            str += $(this).text()+',';
//        });
//        console.log(str);
        if(!empty($id)){
            try{
                $url = '/cron/runOfficeRelatedTasks';
                $officeDomainTbl = TableRegistry::get('OfficeDomains');
                $getOfficeInfo = $officeDomainTbl->getDomainbyOffice($id);
               if(!empty($getOfficeInfo)){
                   if($getOfficeInfo['domain_url']){
                       $url_2_hit = $getOfficeInfo['domain_url'].$url.'/'.$id;
//                       $this->out($url_2_hit);
                       $this->requestUrl($url_2_hit);
                       sleep($time);
                       $this->runCronRequest();
                   }
               }
            }catch (\Exception $ex){
                $this->out($ex->getMessage());
                $this->runCronRequest();
            }
        }
    }
}