<?php

namespace App\Shell;

use App\Controller\Component\TransferData;
use Cake\Console\Shell;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use App\Controller\ProjapotiController;
use Cake\Datasource\ConnectionManager;
use Cake\I18n\I18n;
use Exception;
use Cake\Cache\Cache;

/**
 * Report shell command.
 */
class TransferDataShell extends ProjapotiShell
{
	use TransferData;

    /**
     * main() method.
     *
     * @return bool|int Success or error code.
     */
    public function main()
    {

    }

    private function commands($office_id, $browser, $auth_user)
    {
        $command = "bin/cake API saveLoginHistory {$office_id} {$browser} {$auth_user}> /dev/null 2>&1 &";
    }

	public function countUserActivitiesDetailsCount($designation_id = false, $is_internal = false, $office_id)
	{
		$this->switchOffice($office_id, 'OfficeDb');
		if (!$designation_id) {
			$designation_id = $this->request->data['designation_id'];
		}
		$organogram_info = TableRegistry::get('OfficeUnitOrganograms')->get($designation_id);
		$this->switchOffice($organogram_info['office_id'], 'officeDb');
		$response = [];
		$response['dak_inbox'] = TableRegistry::get('DakUsers')->find()->where(['to_officer_designation_id' => $designation_id, 'is_archive' => 0, 'dak_category' => 'Inbox'])->count();
		$response['nothi_inbox'] = TableRegistry::get('NothiMasterCurrentUsers')->find()->where(['office_unit_organogram_id' => $designation_id, 'is_archive' => 0])->count();
		$response['nothi_sent'] = TableRegistry::get('NothiMasterMovements')->find()->where(['from_officer_designation_id' => $designation_id])->count();
		$response['nothi_other_sent'] = TableRegistry::get('OtherOfficeNothiMasterMovements')->find()->where(['from_officer_designation_id' => $designation_id])->count();

		if ($is_internal) {
			return $response;
		} else {
			$this->response->body(json_encode($response));
			$this->response->type('json');
			return $this->response;
		}
	}

    public function officeTransfer($index, $auth_user) {
		if (!empty($index)) {
			$request_data = Cache::read('office_transfer_request_data' . $index, 'memcached');
			if (!empty($request_data)) {
			    //please remove if success
//				Cache::delete('office_transfer_request_data' . $index, 'memcached');
			} else {
				$this->out('No data');
				return;
			}

			$this->out('Process for '.$request_data['selectedDesignation']);

			$officeToTransfer = intval($request_data['destinationOffice']);
			$selectedDesignations = (array)$request_data['selectedDesignation'];
			$from_unit_ids = (array)$request_data['from_unit'];
			$table_instance_unit_org = TableRegistry::get('OfficeUnitOrganograms');
			$table_instance_unit = TableRegistry::get('OfficeUnits');
			$table_instance_unit_origin = TableRegistry::get('OfficeOriginUnits');
			$table_instance_office = TableRegistry::get('Offices');

			$this->authUserId = $auth_user;
			$this->transferUnit = $from_unit_ids;
			$this->transferFromOffice = $request_data['office_id'];
			$this->employeeOffice = ['office_id' => $officeToTransfer];
			$this->will_move = false;

			$selectedDetailDesignations = $table_instance_unit_org->getAll(['id IN' => $selectedDesignations])->toArray();
			if ($selectedDetailDesignations) {
				$this->officeInformation = $table_instance_office->get($officeToTransfer);
				$connection = ConnectionManager::get('projapotiDb');
				try {
					$this->out('Process started....');
					$connection->begin();
					$units = [];
					#pr($selectedDesignations);
					#pr($selectedDetailDesignations);
					#pr($officeToTransfer);

					foreach ($selectedDetailDesignations as $key => &$value) {
						if ($value['office_id'] == $officeToTransfer) {
							//cannot move to same unit again
							continue;
						}
						if (!in_array($value['office_unit_id'], $units)) {
							$units[] = $value['office_unit_id'];
						}
						$this->unitInformation = $table_instance_unit->get($value['office_unit_id']);
						$existOrigin = $table_instance_unit_origin->find()->where(['office_ministry_id' => $this->officeInformation['office_ministry_id'], 'office_origin_id' => $this->officeInformation['office_origin_id'], 'active_status' => 1, 'unit_name_bng' => $this->unitInformation['unit_name_bng']])->first();
						if (empty($existOrigin)) {
							$exist = $this->createOriginUnit();
							$this->out('Unit origin created');
						} else {
							$exist = $table_instance_unit->find()->where(['office_id' => $this->officeInformation['id'], 'office_origin_unit_id' => $existOrigin['id'], 'active_status' => 1, 'unit_name_bng' => $this->unitInformation['unit_name_bng']])->first();
							if (empty($exist)) {
								$exist = $this->createUnit($existOrigin);
								$this->out('Unit created');
							}
						}
						$value['previous_office_unit_id'] = $value['office_unit_id'];
						$value['office_unit_id'] = $exist['id'];
					}
					$connection->commit();
					$connection->begin();
					$this->unitInformation = null;

					if (!empty($units)) {
						foreach ($selectedDetailDesignations as $key => $value) {
							if ($value['office_id'] == $officeToTransfer) {
								//cannot move to same unit again
								continue;
							}
							$this->unitInformation = $table_instance_unit->get($value['office_unit_id']);
							$this->selectedDesignation = $value;
							$this->selectedUnit = $value['office_unit_id'];
							$this->out('Process ongoing...');
							$success = $this->initiateTransfer();

							//finally change status

							if (!$success) {
								throw new \Exception('Sorry! Office segregation is not process at this moment');
								break;
							}
						}
					}
					$connection->commit();
					$this->out('Successfully office segregate');
				} catch (\Exception $ex) {
					$connection->rollback();
					$this->out($ex->getMessage());
				}
			} else {
				$this->out('This organogram not found as active');
			}
		}
	}
}