<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
	        আর্কাইভের জন্য অপেক্ষমাণ নথিসমূহ
        </div>
    </div>
    <div class="portlet-body">
        <?php if (!empty($return_data)): $i=0; ?>
            <div class="table-scrollable" style="border:none !important;">
                <ul class="nav nav-tabs" style="margin-bottom:0 !important;">
                    <?php foreach ($return_data as $nothi_class => $data_list): $i++; ?>
                        <li class="<?= $i==1?'active':'' ?>"><a data-toggle="tab" href="#home_<?=$nothi_class?>"><?=nothi_class_name($nothi_class)?> শ্রেণির নথিসমূহ</a></li>
                    <?php endforeach; ?>
                </ul>

                <div class="tab-content">
                    <?php $i=0; foreach ($return_data as $nothi_class => $data_list): $i++; ?>

                        <div id="home_<?=$nothi_class?>" class="tab-pane fade <?= $i==1?'in active':'' ?>">
                            <table class="table table-bordered table-hover">
                                <thead>
                                <tr class="text-center heading">
                                    <th class="text-center">ক্রম</th>
                                    <th class="text-center">নথি নং</th>
                                    <th class="text-center">নথির বিষয়</th>
                                    <th class="text-center"><?= __('Actions') ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($data_list as $key => $data): ?>
                                    <tr class="text-center">
                                        <td><?=enTobn($key+1)?></td>
                                        <td><?=$data['nothi_no']?></td>
                                        <td><?=$data['subject']?></td>
                                        <td>
                                            <?= $this->Form->create(null, ['type' => 'post', 'class' => 'form-horizontal', 'url' => ['action'=>'doArchive']]); ?>
                                            <?= $this->Form->input('nothi_master_id', ['label' => false, 'class' => 'form-control input-circle', 'type' => 'hidden', 'value' => $data['id']]); ?>
                                            <?= $this->Form->submit(__('আর্কাইভ করুন'),array('class' => 'btn btn-md green round-corner-5')); ?>
                                            <?= $this->Form->end() ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>

                    <?php endforeach; ?>
                </div>
            </div>
            <?php else: ?>
                <div class="alert alert-danger">আর্কাইভের জন্য অপেক্ষমাণ কোন নথি নাই।</div>
        <?php endif; ?>
    </div>
</div>
