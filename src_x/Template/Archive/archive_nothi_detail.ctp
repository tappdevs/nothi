<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>projapoti-nothi/css/styles.css"/>
<link href="<?php echo CDN_PATH; ?>projapoti-nothi/css/ekko-lightbox.css" rel="stylesheet">
<style>
    .btn-forward-nothi, .btn-sar-nothi-draft, .btn-othi-list {
        padding: 3px 5px !important;

    }

    .portlet-body .notesheetview img {
        width: 100% !important;
        height: 50px !important;
    }

    .btn-icon-only {

    }

    .portlet.box.green > .portlet-title > .actions > a.btn {
        background-color: #fff !important;
        color: green !important;;
    }

    .portlet.box.green > .portlet-title > .actions > a.btn > i {

        color: green !important;;
    }

    #potrodraft_list #sovapoti_signature, #potrodraft_list #sender_signature, #potrodraft_list #sender_signature2, #potrodraft_list #sender_signature3 {
        visibility: hidden;
    }

    #potrodraft_list #sovapoti_signature_date, #potrodraft_list #sender_signature_date, #potrodraft_list #sender_signature2_date, #potrodraft_list #sender_signature3_date {
        visibility: hidden;
    }
</style>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="portlet box green-seagreen full-height-content full-height-content-scrollable">
            <div class="portlet-title">
                <div class="caption">
                    <?php
                    if (!empty($note_priority) && $note_priority == 1) {
                        echo '<i  class="glyphicon glyphicon glyphicon-star" ' . ($note_priority == 1 ? 'style="color:red;"' : '') . ' data-title="' . $note_priority_text . '"  title="' . $note_priority_text . '" > </i>';
                    }
                    ?>
                    <?php echo "শাখা: " . $nothiUnit . "; নথি নম্বর: " . $nothiRecord['nothi_no'] . '; বিষয়: ' . $nothiRecord['subject']; ?>
                </div>
                <div class="actions">
                    <a title=" নথি তালিকায় ফেরত যান" class=" btn   btn-primary "
                       href="<?php echo $this->request->webroot; ?>">
                        <i class="fs1 a2i_gn_details1"></i> নথিসমূহ
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="full-height-content-body">
                    <?php if (!empty($allNothiParts) && count($allNothiParts) >= 1) { ?>
                        <div class="row ">
                            <div class="col-md-6 ">
                                <div class="portlet collapsable  box green" style="margin-bottom: 0;">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            নোটাংশ
                                        </div>

                                        <div class="actions">
                                            <a href="javascript:;"
                                               class="btn btn-default btn-sm btn-maximize btn-minmax" title="">
                                                <i class="glyphicon glyphicon-chevron-right"></i>
                                            </a>

                                        </div>
                                    </div>
                                    <div class="portlet-body" id="onucchedBody">
                                        <?php
                                        $bookMarkLi = '';
                                        $currentLi = array();
                                        ?>

                                        <style>
                                            .nothiGroupView.dropdown-menu {
                                                width: 50px !important;
                                                min-width: 55px;
                                                box-shadow: none;
                                            }

                                            @media print {
                                                a[href]:after {
                                                    content: none !important;
                                                }

                                                .notesheetview {
                                                    line-height: 1.4;
                                                    margin: 0px !important;
                                                    padding: 0px !important;
                                                }

                                                .notesheetview .noteContent {
                                                    padding: 2px !important;
                                                }

                                                @page {
                                                    margin-top: 1in;
                                                    margin-left: 1.2in;
                                                    margin-bottom: .75in;
                                                    margin-right: .75in;
                                                }

                                                body {
                                                    margin-top: 1in;
                                                    margin-left: 1.2in;
                                                    margin-bottom: .75in;
                                                    margin-right: .75in;
                                                }

                                                .notesheetview p {
                                                    margin: 0px 0px 5px !important;
                                                }

                                                .notesheetview .btn-block {
                                                    margin-bottom: 2px !important;
                                                }

                                                .row > div {
                                                    padding: 0px !important;
                                                }
                                            }
                                        </style>
                                        <div class="portlet ">
                                            <div class="portlet-title" style="border-bottom: 1px solid #eee;">
                                                <div class="row">
                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                        <ul class="pager">
                                                            <li style="cursor: pointer;" id="noteLeftMenuToggle"
                                                                title="সব অনুচ্ছেদ দেখুন"
                                                                data-original-title="সব অনুচ্ছেদ দেখুন">
                                                                <button class="btn btn-sm "><i class="fs0 a2i_gn_view1" style="color:green;"></i></button>
                                                            </li>
                                                            <li>
                                                                <button data-target="#printAdvanceModal" data-toggle="modal"
                                                                   class="btn btn-sm green"><i
                                                                            class="fs1 efile-print"></i> কাস্টম প্রিন্ট</button>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="portlet-body ">
                                                <div class="row">

                                                    <div class="col-lg-0 col-md-0 col-sm-0 col-xs-0 noteLeftMenu" style="display: none;">
                                                        <?php
                                                        $lastListedNote = 0;

                                                        if (!empty($noteNos)) {
                                                            echo '<div class="scroller" style="height: 700px;" data-always-visible="1" data-rail-visible1="1">';
                                                            echo '<div class="btn-group-vertical">';
                                                            $noteGroupArray = array();

                                                            foreach ($noteNos as $key => $value) {

                                                                $noteGroupArray[$value['nothi_part_no_en']][] = '<li>
				<a href=' . $value['id'] . 'nothi_office=' . $nothi_office . ' title="অনুচ্ছেদ" class="nothiOnucched showforPopup btn btn-default "  nothiMasterId = ' . $value['nothi_notesheet_id'] . '  nothisheetsId = ' . $value['nothi_notesheet_id'] . ' notenoen=' . $value['note_no_en'] . ' nothi_part=' . $value['nothi_part_no'] . ' noteno=' . $value['note_no'] . '>' . $value['nothi_part_no_bn'] . '.' . $value['note_no'] . '</a>
			</li>';
                                                            }

                                                            foreach ($noteGroupArray as $ky => $v) {

                                                                echo '<div class="btn-group">';
                                                                echo '<button id="btnGroupVerticalDrop' . $ky . '" type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> ' . $this->Number->format($ky) . ' <i class="glyphicon glyphicon-chevron-down"></i> </button>';
                                                                echo '<ul class="nothiGroupView dropdown-menu" role="menu" aria-labelledby="btnGroupVerticalDrop' . $ky . '">';
                                                                foreach ($v as $groupNoteKey => $groupNoteValue) {
                                                                    echo $groupNoteValue;
                                                                }
                                                                echo '</ul> </div>';
                                                            }

                                                            echo "</div></div>";
                                                        }
                                                        ?>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noteContentSide">
                                                        <div class="nothiDetailsPage ">
                                                            <div class="notesheetview" id="notesShow_new">

                                                            </div>
                                                            <div id="noteShowLoading"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="portlet-footer" style="border-top: 1px solid #eee;">
                                                <div class="row">

                                                    <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-2 col-lg-10 col-md-10 col-sm-10 text-right">

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="portlet-body" id="khosrapotrobody" style="display: none;">

                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 ">
                                <div class="portlet collapsable  box green" style="margin-bottom:0;">
                                    <div class="portlet-title">

                                        <div class="actions pull-left">
                                            <a href="javascript:;"
                                               class="btn btn-default btn-sm btn-maximize btn-minmax" title="">
                                                <i class="glyphicon glyphicon-chevron-left"></i>
                                            </a>
                                        </div>
                                        <div class="caption pull-right">
                                            পত্রাংশ
                                        </div>

                                    </div>
                                    <div class="portlet-body" id="potroShow">

                                    </div>

                                </div>
                            </div>

                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade modal-purple" id="responsiveOnuccedModal" data-backdrop="static" tabindex="-1" role="dialog" style="z-index:99999" aria-hidden="true">
    <div class="modal-dialog modal-full">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade modal-purple" id="printAdvanceModal">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal"
                        aria-hidden="true">×
                </button>
                <h4 class="modal-title" style="float: left;">
                    কাস্টম প্রিন্ট করুন</h4>
            </div>
            <div class="modal-body">

                <div class="row">
                    <div class="col-md-12 text-left">
                        <?= $this->Form->create('advanceSearchForm', ['id' => 'advanceSearchForm', 'class' => 'form']) ?>
                        <input type="hidden"
                               id="nothi_master_id"
                               name="nothi_master_id"
                               value="<?= $nothiRecord['nothi_masters_id'] ?>"/>
                        <div class="row">
                            <div class="col-md-3 col-sm-3 col-xs-3 col-lg-3">
                                <label class="control-label">হইতে</label><br/>
                                <select name="note_start" class="form-control">
                                    <?php
                                    if (!empty($allNothiParts)) {
                                        foreach ($allNothiParts as $key => $value) {
                                            ?>
                                            <option value="<?= $value['id'] ?>">
                                                নোট নম্বর: <?= $value['nothi_part_no_bn'] ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                                <br/>
                                <label class="control-label">পর্যন্ত</label><br/>
                                <select name="note_end" class="form-control">
                                    <?php
                                    if (!empty($allNothiParts)) {
                                        foreach ($allNothiParts as $key => $value) {
                                            ?>
                                            <option value="<?= $value['id'] ?>">
                                                নোট নম্বর: <?= $value['nothi_part_no_bn'] ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-3 col-lg-3">
                                <?= $this->Form->input('margin_top', ['class' => 'form-control input-sm ', 'label' => 'উপর মার্জিন ', 'default' => '1', 'type' => 'number', 'precision' => 2, 'step' => .25, 'min' => 0]) ?>
                                <?= $this->Form->input('margin_bottom', ['class' => 'form-control input-sm', 'label' => 'নিচ মার্জিন ', 'default' => '.75', 'type' => 'number', 'precision' => 2, 'step' => .25, 'min' => 0]) ?>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-3 col-lg-3">
                                <?= $this->Form->input('margin_right', ['class' => 'form-control input-sm', 'label' => 'ডান মার্জিন ', 'default' => '.75', 'type' => 'number', 'precision' => 2, 'step' => .25, 'min' => 0]) ?>
                                <?= $this->Form->input('margin_left', ['class' => 'form-control input-sm', 'label' => 'বাম মার্জিন ', 'default' => '1', 'type' => 'number', 'precision' => 2, 'step' => .25, 'min' => 0]) ?>
                            </div>

                            <div class="col-md-3 col-sm-3 col-xs-3 col-lg-3">
                                <?= $this->Form->input('orientation', ['class' => 'form-control input-sm', 'label' => 'ধরন', 'options' => ['portrait' => 'Portrait', 'landscape' => 'Landscape']]) ?>
                                <br/><input value="প্রিভিউ"
                                            type="submit"
                                            class="btn btn-primary btn-sm btn-pdf"
                                            type="button"/>
                            </div>
                        </div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <embed class="showPreview" src="" style=" width:100%; height: 600px;"
                               type="application/pdf"></embed>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal"
                        class="btn  btn-danger">বন্ধ করুন
                </button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade modal-purple" id="searchAdvanceModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal"
                        aria-hidden="true">×
                </button>
                <h4 class="modal-title" style="float: left;">
                    কাস্টম অনুসন্ধান করুন</h4>
            </div>
            <div class="modal-body">

                <div class="row">
                    <div class="col-md-12 text-left">
                        <?= $this->Form->create('showAdvanceSearchForm', ['id' => 'showAdvanceSearchForm', 'class' => 'form-horizontal']) ?>
                        <input type="hidden"
                               id="nothi_master_id"
                               name="nothi_master_id"
                               value="<?= $nothiRecord['nothi_masters_id'] ?>"/>
                        <div class="row">
                            <div class="col-md-3 col-sm-3 col-xs-3 col-lg-3">
                                <label class="control-label">হইতে</label><br/>
                                <select name="note_start" class="form-control">
                                    <?php
                                    if (!empty($allNothiParts)) {
                                        foreach ($allNothiParts as $key => $value) {
                                            ?>
                                            <option value="<?= $value['id'] ?>">
                                                নোট নম্বর: <?= $value['nothi_part_no_bn'] ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-3 col-lg-3">
                                <label class="control-label">পর্যন্ত</label><br/>
                                <select name="note_end" class="form-control">
                                    <?php
                                    if (!empty($allNothiParts)) {
                                        foreach ($allNothiParts as $key => $value) {
                                            ?>
                                            <option value="<?= $value['id'] ?>">
                                                নোট নম্বর: <?= $value['nothi_part_no_bn'] ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="col-md-3 col-sm-3 col-xs-3 col-lg-3">
                                <label class="control-label" style="height:24px;"></label><br/>
                                <input value="প্রিভিউ" type="submit" class="btn btn-primary btn-sm" type="button"/>
                            </div>
                        </div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="showNotesPreview"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal"
                        class="btn  btn-danger">বন্ধ করুন
                </button>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-markdown/lib/markdown.js" type="text/javascript"></script>
<script src="<?php echo CDN_PATH; ?>projapoti-nothi/js/nothi_master_movements.js"></script>
<script src="<?php echo CDN_PATH; ?>projapoti-nothi/js/ekko-lightbox.js"></script>
<script>
    $('body').addClass('page-quick-sidebar-over-content page-full-width');
    jQuery.ajaxSetup({
        cache: true
    });
    $(document).on('click', '#notesShow .pagination a', function (e) {
        e.preventDefault();
        if ($(this).parent('li').hasClass('disabled') == false)
            noteShow(this.href);

        return false;
    });


    $(document).ready(function () {

        Metronic.init();
        NothiMasterMovement.init('nothing');
        $('li').tooltip({'placement':'bottom'});
        $('#notesShow').html('<img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp; লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');
        $('#potroShow').html('<img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp; লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');

        //noteShow('<?php echo $this->request->webroot; ?>NothiNoteSheets/notesheetPageAll/<?php echo $nothiRecord['id'] ?>/<?= $nothi_office ?>');

        var noteSheetPageUrl = '<?php echo $this->request->webroot; ?>archiveNotesheetPageAll/<?php echo $nothiRecord['id'] ?>/<?php echo $nothi_office ?>';
        noteShow(noteSheetPageUrl, 0);

        $('#notesShow_new').scroll(function () {
            if ($("#noteShowLoading").css('display') == 'none') {
                if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight - 200) {
                    var limitStart = $("#notesShow_new > div").length;
                    noteShow(noteSheetPageUrl, limitStart);
                }
            }
        });

        if ($("#noteShowLoading").length != 0) {
            var minHeight = $("#notesShow_new").height();
            $("#notesShow_new").css('height', 'auto');
            if ($("#notesShow_new").parent().height() > $("#notesShow_new").height()) {
                var limitStart = $("#notesShow_new > div").length;
                noteShow(noteSheetPageUrl, parseInt(limitStart) + 1);
            }
            if ($("#notesShow_new").height() < minHeight) {
                $("#notesShow_new").css('height', minHeight);
            }
        }

        potroPageShow('<?php echo $this->request->webroot; ?>archivePotroPageAll/<?php echo $nothiRecord['id'] ?>/<?= $nothi_office ?>');

        $('[title]').tooltip({'placement':'bottom'});
        $('li').tooltip({'placement':'bottom'});

        $(document).on('click', '.btn-minmax', function () {
            $('.collapsable').show();
            $('.collapsable').parent('div').removeClass('col-md-12').addClass('col-md-6');
            $('.btn-minmax').addClass('btn-maximize').removeClass('btn-minimized');

        });

        $(document).on('click', '.btn-minmax', function () {
            $('.collapsable').show();
            $('.collapsable').parent('div').removeClass('col-md-12').addClass('col-md-6');
            $('.btn-minmax').addClass('btn-maximize').removeClass('btn-minimized');
            //$(this).find('i').toggleClass('fa-chevron-right').toggleClass('fa-chevron-left');
        });

        $(document).on('click', '.btn-minimized', function () {
            $(this).find('i').toggleClass('fa-chevron-right').toggleClass('fa-chevron-left');
        });

        $(document).on('click', '.btn-maximize', function () {
            var ind = $('.btn-maximize').index(this);

            if (ind == 1) {
                ind = 0;
            } else {
                ind = 1;
            }
            $('.collapsable').eq(ind).hide();
            $('.collapsable').parent('div').removeClass('col-md-6').addClass('col-md-12');
            $('.btn-minmax').removeClass('btn-maximize').addClass('btn-minimized');
            $(this).find('i').toggleClass('fa-chevron-right').toggleClass('fa-chevron-left');

        });

    });


    function getUrlVars(href) {
        var vars = [], hash;
        var hashes = href.slice(href.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    }


    function noteShowOLD(href) {
        if (href == '' || typeof (href) == 'undefined')
            return;
        $('#notesShow').html('<img src="<?php echo $this->request->webroot; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp;লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');
        PROJAPOTI.ajaxSubmitAsyncDataCallbackWithoutAbrot(href, {}, 'html', function (response) {
            $('#notesShow').html(response);
//            Metronic.initSlimScroll('.scroller');
            //$('[data-title-orginal]').tooltip({placement: 'bottom'});
            //$('[title]').tooltip({placement: 'bottom'});
            //$('li').tooltip({placement: 'bottom'});

            $.each($('.signatures'), function (i, v) {
                var imgurl = $(v).data('id');
                var token = $(v).data('token_id');
                var signDate = $(v).data('signdate');
                PROJAPOTI.ajaxSubmitAsyncDataCallbackWithoutAbrot(js_wb_root + 'getSignature/' + imgurl + '/1/' + signDate + '?token='+token, {}, 'html', function (signature) {
                    $(v).attr('src', signature);
                });

            });
            heightlimit();
        });
    }

    function noteShow(href, limitStart) {
        if (href == '' || typeof (href) == 'undefined') {
            return;
        }
        $('#noteShowLoading').show();
        $('#noteShowLoading').html('<img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp;লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');
        //$('#notesShow_new').html();

        //alert('before request');
        PROJAPOTI.ajaxSubmitAsyncDataCallbackWithoutAbrot(href, {'limitStart': limitStart}, 'html', function (response) {
            if (isEmpty(response)) {
                $('#noteShowLoading').remove();
            } else {
                $('#noteShowLoading').hide();
            }
            if (limitStart == 0) {
                //
            }
            $('#noteShowLoading').hide();
            if (limitStart == '' || typeof (limitStart) == 'undefined') {
                $('#notesShow_new').html(response);
            } else {
                $('#notesShow_new').append(response);
            }

            //Metronic.initSlimScroll('.scroller');
            //$('[data-title-orginal]').tooltip({placement: 'bottom'});
            //$('[title]').tooltip({placement: 'bottom'});
            //$('li').tooltip({placement: 'bottom'});

            $.each($('.signatures'), function (i, v) {
                var imgurl = $(v).data('id');
                var token = $(v).data('token_id');
                var signDate = $(v).data('signdate');
                PROJAPOTI.ajaxSubmitAsyncDataCallbackWithoutAbrot(js_wb_root + 'getSignature/' + imgurl + '/1/' + signDate + '?token='+token, {}, 'html', function (signature) {
                    $(v).attr('src', signature);
                });
            });
        });
        $(document).ajaxStop(function () {
            checkEmptyOnucched();
        });
        heightlimit();
    }

    function checkEmptyOnucched() {
        if ($('.note_show_details').length == 0) {
            $('.notesubjectdiv').show().removeClass('hidden');
            $('#responsiveNoteEdit').show();
            $('#addNewNote').hide();
            $('.btn-closeNote').hide();
        }
    }

    $(document).on('click', '#noteLeftMenuToggle', function () {
        $('.noteLeftMenu').toggle().toggleClass('col-lg-2 col-md-2 col-sm-2 col-xs-2');

        if ($('.noteLeftMenu').css('display') == 'none') {
            $('.noteContentSide').addClass('col-lg-12 col-md-12 col-sm-12 col-xs-12')
            $('.noteContentSide').removeClass('col-lg-10 col-md-10 col-sm-10 col-xs-10')

        } else {
            $('.noteContentSide').addClass('col-lg-10 col-md-10 col-sm-10 col-xs-10')
            $('.noteContentSide').removeClass('col-lg-12 col-md-12 col-sm-12 col-xs-12')

        }
    })
    $(document).on('click', '#potroLeftMenuToggle', function () {
        $('.potroLeftMenu').toggle().toggleClass('col-lg-2 col-md-2 col-sm-2 col-xs-2');

        if ($('.potroLeftMenu').css('display') == 'none') {
            $('.potroContentSide').addClass('col-lg-12 col-md-12 col-sm-12 col-xs-12').css({
                'min-height': '40px',
                'overflow-y': 'auto',
                'overflow-x': 'hidden'
            });
            $('.potroContentSide').removeClass('col-lg-10 col-md-10 col-sm-10 col-xs-10')

        } else {
            $('.potroContentSide').addClass('col-lg-10 col-md-10 col-sm-10 col-xs-10').css({
                'min-height': '40px',
                'overflow-y': 'auto',
                'overflow-x': 'hidden'
            });
            $('.potroContentSide').removeClass('col-lg-12 col-md-12 col-sm-12 col-xs-12')

        }
    });


    function potroPageShow(href, pageno) {
        $('#potroShow').html('<img src="<?php echo $this->request->webroot; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp;লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');

        PROJAPOTI.ajaxSubmitAsyncDataCallbackWithoutAbrot(href, {}, 'html', function (response) {
            $('#potroShow').html(response);

//            Metronic.initSlimScroll('.scroller');
            if (typeof (pageno) != 'undefined') {
                goToPage(pageno);
                getpotropage(pageno);
            }
            //$('[data-title-orginal]').tooltip({placement: 'bottom'});
            //$('[title]').tooltip({placement: 'bottom'});
            //$('li').tooltip({placement: 'bottom'});
            heightlimit();
        });
    }

    function getpotropage(potroPage) {
        $('.potroNo').val(potroPage);
    }

    function refPotro(potrono) {
        if (typeof (potrono) != 'undefined') {
            goToPage(potrono);
            getpotropage(potrono);
        }
    }


    function getPopUpPotro(href, title) {
        $('#responsiveOnuccedModal').find('.modal-title').text('');
        $('#responsiveOnuccedModal').find('.modal-body').html('');
        $.ajax({
            url: '<?php echo $this->request->webroot; ?>NothiMasters/showPopUp/' + href+'?nothi_part='+'<?= $part_id ?>'+'&token='+'<?= sGenerateToken(['file'=>$part_id],['exp'=>time() + 60 * 300]) ?>',
            dataType: 'html',
            data: {'nothi_office':<?php echo $nothi_office ?>},
            type: 'post',
            success: function (response) {
                $('#responsiveOnuccedModal').modal('show');
                $('#responsiveOnuccedModal').find('.modal-title').text(title);

                $('#responsiveOnuccedModal').find('.modal-body').html(response);
            }
        });
    }


    $(document).on('click', '.showforPopup', function (e) {
        e.preventDefault();
        var title = $(this).attr('title').length > 0 ? $(this).attr('title') : $(this).attr('data-original-title');
        getPopUpPotro($(this).attr('href'), title);
    })


    $(document).on('click', '.btn-notesheet-print', function () {
        $(document).find('.notesheetview').printThis({
            importCSS: true,
            debug: false,
            importStyle: true,
            printContainer: true,
            pageTitle: "",
            removeInline: false,
            header: null
        });
    });


    $(document).ready(function ($) {
        // delegate calls to data-toggle="lightbox"

        $(document).delegate('*[data-toggle="lightbox"]:not([data-gallery="navigateTo"])', 'click', function (event) {
            event.preventDefault();
            return $(this).ekkoLightbox({
                onShown: function () {

                },
                onNavigate: function (direction, itemIndex) {

                }
            });
        });
    });

    <?php if ($otherNothi == false): ?>
    $('.newPartCreate').on('click', function () {

        toastr.options = {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-bottom-right"
        };

        var nothimasterid = <?php echo $nothiRecord['nothi_masters_id']; ?>;

        if (nothimasterid > 0) {
        } else {
            return false;
        }

        var selectedPriviliges = {};

        bootbox.dialog({
            message: "আপনি কি নতুন নোট তৈরি করতে চান?",
            title: "নতুন নোট",
            buttons: {
                success: {
                    label: "হ্যাঁ",
                    className: "green",
                    callback: function () {
                        Metronic.blockUI({
                            target: '.page-container',
                            boxed: true
                        });
                        $('.nothiCreateButton').attr('disabled', 'disabled');
                        $.ajax({
                            url: '<?php echo $this->Url->build(['controller' => 'NothiMasters',
                                'action' => 'add'])
                                ?>',
                            data: {formPart: {nothi_master_id: nothimasterid}, priviliges: selectedPriviliges},
                            type: "POST",
                            dataType: 'JSON',
                            success: function (response) {
                                $('div.error-message').remove();
                                if (response.status == 'error') {
                                    Metronic.unblockUI('.page-container');
                                    $('.nothiCreateButton').removeAttr('disabled');
                                    toastr.error(response.msg);

                                    if (typeof (response.data) != 'undefined') {
                                        var errors = response.data;
                                        $.each(errors, function (i, value) {
                                            $.each(value, function (key, val) {
                                                if (i != 'office_units_organogram_id') {
                                                    $('[name=' + i + ']').focus();
                                                    $('[name=' + i + ']').after('<div class="error-message">' + val + '</div>');
                                                }
                                            });
                                        });
                                    }
                                } else if (response.status == 'success') {
                                    window.location.href = js_wb_root + 'noteDetail/' + response.id;
                                }
                            },
                            error: function (status, xresponse) {

                            }
                        });
                    }
                },
                danger: {
                    label: "না",
                    className: "red",
                    callback: function () {
                        Metronic.unblockUI('.page-container');
                    }
                }
            }
        });
        return false;
    });
    <?php endif; ?>

    function heightlimit() {
        var resBreakpointMd = Metronic.getResponsiveBreakpoint('md');
        var handle100HeightContent = function () {

            var target = $('.full-height-content');
            var height;

            height = Metronic.getViewPort().height -
                $('.page-header').outerHeight(true) -
                $('.page-footer').outerHeight(true) -
                $('.page-title').outerHeight(true) -
                $('.page-bar').outerHeight(true);

            if (target.hasClass('portlet')) {
                var portletBody = target.find('.portlet-body');

                if (Metronic.getViewPort().width < resBreakpointMd) {
                    Metronic.destroySlimScroll(portletBody.find('.full-height-content-body')); // destroy slimscroll
                    return;
                }

                height = height -
                    target.find('.portlet-title').outerHeight(true) -
                    parseInt(target.find('.portlet-body').css('padding-top')) -
                    parseInt(target.find('.portlet-body').css('padding-bottom')) - 2;

                if (target.hasClass("full-height-content-scrollable")) {
                    height = height - 15;

                    $('#notesShow_new').css('overflow', 'auto');
                    $('#notesShow_new').css('height', height - 100);

                    $('#potroShow').css('overflow', 'auto');
                    $('#potroShow').css('height', height - 18);

                    //portletBody.find('.full-height-content-body').css('height', height);
                    // Metronic.initSlimScroll(portletBody.find('.full-height-content-body'));
                } else {
                    portletBody.css('min-height', height);
                }
            } else {
                if (Metronic.getViewPort().width < resBreakpointMd) {
                    Metronic.destroySlimScroll(target.find('.full-height-content-body')); // destroy slimscroll
                    return;
                }

                if (target.hasClass("full-height-content-scrollable")) {
                    height = height - 15;
                    target.find('.full-height-content-body').css('height', height);
                    // Metronic.initSlimScroll(target.find('.full-height-content-body'));
                } else {
                    target.css('min-height', height);
                }
            }
            $('.notecount').css({"max-height": height});
            $('.notecount').css({"overflow": 'auto'});
            $('.slimScrollBar').css({'top': '40px'});
        };
        handle100HeightContent();
        Metronic.addResizeHandler(handle100HeightContent);
        var maxhe = Math.max($('.noteContentSide').height() + 80, $('.potroContentSide').height() + 80);
        //$('#notesShow').height(maxhe);
        //$('#potroShow').height(maxhe);
    }

    $(document).off('click', '.showAttachmentOfContent').on('click', '.showAttachmentOfContent', function () {
        $('#responsiveOnuccedModal').modal('show');
        var file = $(this).text();
        $('#responsiveOnuccedModal').find('.modal-title').html(file);

        var url = $(this).data('url');
        var attachment_type = $(this).data('type');

        if (typeof(url) != 'undefined') {

            if (attachment_type == 'text') {
                $('#responsiveOnuccedModal').find('.modal-body').html('<div style="background-color: #fff; max-width:950px; min-height:815px; max-height: 815px; margin:0 auto; page-break-inside: auto;">"' + file + '<br/>" + file + "</div>"');
            } else if ((attachment_type.substring(0, 15)) == 'application/vnd' || (attachment_type.substring(0, 15)) == 'application/ms' || (attachment_type.substring(0, 15)) == 'application/pdf') {
                $('#responsiveOnuccedModal').find('.modal-body').html('<iframe style=" width:100%; height: 700px;" src="https://docs.google.com/gview?url=' + url + '&embedded=true" style="width:600px; height:500px;" frameborder="0"></iframe>');

            } else if ((attachment_type.substring(0, 5)) == 'image') {
                $('#responsiveOnuccedModal').find('.modal-body').html('<embed src="' + url + '" style=" width:100%; height: 700px;" type="' + attachment_type + '"></embed>');
            } else {
                $('#responsiveOnuccedModal').find('.modal-body').html('দুঃখিত! ডাটা পাওয়া যায়নি');
            }
        }

    });

    $("form#advanceSearchForm").submit(function (event) {
        Metronic.blockUI({target: '#advanceSearchForm'});
        event.preventDefault();
        $('.showPreview').attr('src', '');
        PROJAPOTI.ajaxSubmitAsyncDataCallbackWithoutAbrot('<?= $this->Url->build(['_name' => 'onucchedPdf', $nothi_office]); ?>',
            $(this).serializeArray(), 'json', function (response) {
                if (response.status == 'success') {
                    $('.showPreview').attr('src', response.filename);
                } else {
                    toastr.error(response.msg);
                }
                Metronic.unblockUI('#advanceSearchForm');
            }
        );
    });
    $("form#showAdvanceSearchForm").submit(function (event) {
        Metronic.blockUI({target: '#showAdvanceSearchForm'});
        event.preventDefault();
        $('.showNotesPreview').html('');
        PROJAPOTI.ajaxSubmitAsyncDataCallbackWithoutAbrot('<?= $this->Url->build(['_name' => 'nothiAdvanceSearch', $nothi_office]); ?>',
            $(this).serializeArray(), 'html', function (response) {
                console.log(response);
                $('.showNotesPreview').html(response);
                Metronic.unblockUI('#showAdvanceSearchForm');
            }
        );
    });
</script>