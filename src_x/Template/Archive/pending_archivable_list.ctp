<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
	        আর্কাইভের জন্য অপেক্ষমাণ অসম্পন্ন নথিসমূহ
        </div>
    </div>
    <div class="portlet-body">
        <?php if (!empty($return_data)): $i=0; ?>
            <div class="table-scrollable" style="border:none !important;">
                <ul class="nav nav-tabs" style="margin-bottom:0 !important;">
                    <?php foreach ($return_data as $nothi_class => $data_list): $i++; ?>
                        <li class="<?= $i==1?'active':'' ?>"><a data-toggle="tab" href="#home_<?=$nothi_class?>"><?=nothi_class_name($nothi_class)?> শ্রেণির নথিসমূহ</a></li>
                    <?php endforeach; ?>
                </ul>

                <div class="tab-content">
                    <?php $i=0; foreach ($return_data as $nothi_class => $data_list): $i++; ?>

                        <div id="home_<?=$nothi_class?>" class="tab-pane fade <?= $i==1?'in active':'' ?>">
                            <table class="table table-bordered table-hover">
                                <thead>
                                <tr class="text-center heading">
                                    <th class="text-center">ক্রম</th>
                                    <th class="text-center">নথি নং</th>
                                    <th class="text-center">নথির বিষয়</th>
                                    <th class="text-center">অনিষ্পন্ন নোটের সংখ্যা</th>
                                    <th class="text-center"><?= __('Actions') ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($data_list as $key => $data): ?>
                                    <tr class="text-center">
                                        <td><?=enTobn($key+1)?></td>
                                        <td><?=$data['nothi_no']?></td>
                                        <td class="text-left"><?=$data['subject']?></td>
                                        <td><?=enTobn($data['pending_note'])?></td>
                                        <td>
                                            <button class="btn btn-md green" onclick="showModalData(<?=$data['id']?>, '<?=$data['subject']?>')">অসম্পন্নসমূহ দেখুন</button>
                                            <div id="viewModal" class="modal fade modal-purple" role="dialog">
                                                <div class="modal-dialog modal-full text-left">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" data-dismiss="modal">&times;</button>
                                                            <h4 class="modal-title" id="pending_nothi_subject"></h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div id="pending_notes_detail"><img src="<?= $this->request->webroot; ?>assets/global/img/loading-spinner-grey.gif"></div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-md red" data-dismiss="modal">বন্ধ করুন</button>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>

                    <?php endforeach; ?>
                </div>
            </div>
            <?php else: ?>
                <div class="alert alert-danger">আর্কাইভের জন্য অপেক্ষমাণ অসম্পন্ন কোন নথি নাই।</div>
        <?php endif; ?>
    </div>
</div>
<script>
    function showModalData(master_id, nothi_subject) {
        $("#viewModal").modal('show');
        $("#pending_nothi_subject").html('নথির বিষয়ঃ '+nothi_subject);
        $("#pending_notes_detail").html('<img src="<?= $this->request->webroot; ?>assets/global/img/loading-spinner-grey.gif">');
        $.ajax({
            type: "POST",
            url: '<?= $this->request->webroot; ?>Archive/show_pending_note',
            data: {
                'master_id': master_id
            },
            success: function(result){
                //console.log(result);
                $("#pending_notes_detail").html(result);
            },
            //dataType: "script"
        });
    }
</script>
