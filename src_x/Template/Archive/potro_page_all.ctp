<style>

    .portlet-body img {
        max-width: 100%;
        height: auto;
    }

    div.attachmentsRecord {
        display: none;
    }

    div.attachmentsRecord.active {
        display: block;
    }

    div.DraftattachmentsRecord {
        display: none;
    }

    div.DraftattachmentsRecord.active {
        display: block;
    }

    div.NothijatoattachmentsRecord {
        display: none;
    }

    div.NothijatoattachmentsRecord.active {
        display: block;
    }

    div.summaryDraftattachmentsRecord {
        display: none;
    }

    div.summaryDraftattachmentsRecord.active {
        display: block;
    }

    .pager li > a, .pager li > span {
        display: inline-block;
        padding: 5px 8px;
        background-color: #fff;
        border: 1px solid #ddd;
        border-radius: 0;
    }

    .potroview {
        margin: 0 auto;
    }

    div.potroAccordianPaginationBody {
        display: none;
    }

    div.potroAccordianPaginationBody.active {
        display: block;
    }

    a {
        cursor: pointer;
    }

    .editable {
        border: none;
        word-break: break-word;
    }

    #potrodraft_list #sovapoti_signature, #potrodraft_list #sender_signature, #potrodraft_list #sender_signature2, #potrodraft_list #sender_signature3 {
        visibility: hidden;
    }

    #potrodraft_list #sovapoti_signature_date, #potrodraft_list #sender_signature_date, #potrodraft_list #sender_signature2_date, #potrodraft_list #sender_signature3_date {
        visibility: hidden;
    }

    .showimageforce #sovapoti_signature, .showimageforce #sender_signature, .showimageforce #sender_signature2, .showimageforce #sender_signature3,
    .showimageforce #sovapoti_signature_date, .showimageforce #sender_signature_date, .showimageforce #sender_signature2_date, .showimageforce #sender_signature3_date {
        visibility: visible !important;
    }

    #sarnothidraft_list img[alt=signature] {
        visibility: hidden;
    }

    .showImage img {
        visibility: visible !important;
    }

    div#note {
        margin-left: 0px !important;
        overflow-wrap: break-word;
        word-wrap: break-word;

        /* Adds a hyphen where the word breaks */
        -webkit-hyphens: auto;
        -ms-hyphens: auto;
        -moz-hyphens: auto;
        hyphens: auto;
    }

    table {
        width: 100% !important;
    }

    .bangladate {
        border-bottom: 1px solid #000;
    }
</style>
<?php
$bookMarkLi = '';
$currentLi = array();

?>
<?= $this->Form->hidden('master_id', ['value' => $nothimastersid, 'id' => 'master_id']) ?>
<?= $this->Form->hidden('nothi_office', ['value' => $nothi_office, 'id' => 'nothi_office']) ?>
<div class="portlet light">
    <div class="portlet-title" style="border-bottom: 1px solid #eee;">
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2">
                <ul class="pager">
                    <li style="cursor: pointer;" id="potroLeftMenuToggle" title="সব পতাকা দেখুন"
                        data-original-title="সব পতাকা দেখুন">
                        <i class="fs0 a2i_gn_view1" style="color:green;"></i>
                    </li>
                </ul>
            </div>
            <div class="col-lg-10 col-md-10 col-sm-10">
                <div class="row">
                    <div class="col-md-9">
                        <?= $this->Form->input('potroSearch', ['class' => 'form-control input-md potroSearch', 'label' => false, 'placeholder' => 'পত্রের বিষয় অথবা স্মারক নম্বর দিয়ে খুজুন']) ?>
                    </div>
                    <div class="col-md-3">
                        <?= $this->Form->button('<i class="fs1 efile-search3"></i>', ['class' => 'btn btn-primary btn-md potroSearchButton', 'type' => 'button', 'style' => 'padding:7px 10px;margin:0px!important;']) ?>
                        <?= $this->Form->button('<i class="fs1 a2i_gn_reset2"></i>', ['class' => 'btn btn-danger btn-md potroSearchResetButton', 'type' => 'button', 'style' => 'padding:7px 10px;margin:0px!important;']) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="portlet-body">
        <div class="row divcenter">
            <div class="col-lg-0 col-md-0 col-sm-0 col-xs-0 potroLeftMenu" style="display: none;;">
                <?php
                if (!empty($potroFlags)) {
                    echo '<div class="scroller" style="height: 700px;" data-always-visible="1" data-rail-visible1="1">';

                    foreach ($potroFlags as $key => $value) {
                        echo "<div title='পত্র নম্বর: {$value['potro_no_bn']}' style='font-size:8pt;width: 100%; padding: 1px 1px; border-radius: 0;word-wrap: break-word; white-space: pre-line;'  href='{$value['attachment_id']}/potaka/{$value['nothi_master_id']}/{$value['page']}' class='potroFlagButton showforPopup btn btn-default btn-{$value['class']}' btn_potro_no=" . $value['potro_no'] . ">{$value['title']}</div>";
                    }
                    echo '</div>';
                }
                ?>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 potroContentSide">
                <div class="potroDetailsPage " data-always-visible="1"
                     data-rail-visible1="1">
                    <div class="tabbable-custom nav-justified">
                        <ul class="nav nav-tabs nav-justified potroview_tab">
                            <?php if (!empty($draftSummary)
                                || !empty($draftPotro)): ?>
                                <li class="dropdown active">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:;"
                                       aria-expanded="true">খসড়া&nbsp;<b class="caret"></b></a>
                                    <ul class="dropdown-menu" role="menu">

                                        <?php if (!empty($draftSummary)): ?>
                                            <li class="active">
                                                <a href="#sarnothidraft_list" data-toggle="tab">
                                                    খসড়া সার-সংক্ষেপ
                                                </a>
                                            </li>
                                        <?php endif; ?>
                                        <?php if (!empty($draftPotro)): ?>
                                            <li>
                                                <a href="#potrodraft_list" data-toggle="tab">
                                                    খসড়া পত্র
                                                </a>
                                            </li>
                                        <?php endif; ?>
                                    </ul>
                                </li>
                            <?php endif; ?>
                            <?php if (!empty($summaryAttachmentRecord)): ?>
                                <li class="<?php echo(empty($draftSummary) && empty($draftPotro) ? 'active' : '') ?>">
                                    <a href="#summaryfinal_list" data-toggle="tab">
                                        সার-সংক্ষেপ
                                    </a>
                                </li>
                            <?php endif; ?>
                            <li class="loadsokolpotro <?php echo(empty($draftSummary) && empty($summaryAttachmentRecord) ? (!empty($draftPotro) ? '' : 'active') : '') ?>">
                                <a href="#potroview_list" data-toggle="tab">
                                    সকল পত্র
                                </a>
                            </li>
                            <?php // if (!empty($potroNothijatoAttachmentRecord)): ?>
                            <li class="sokolnothijatopotro">
                                <a href="#nothijato_list" data-toggle="tab">
                                    নথিজাত পত্র
                                </a>
                            </li>
                            <?php // endif; ?>
                        </ul>
                        <div class="tab-content">
                            <?php if (!empty($draftSummary)): ?>
                                <div class="tab-pane active" id="sarnothidraft_list">
                                    <div class="portlet light  ">
                                        <div class="portlet-body" style="overflow-y: auto;overflow-x:  hidden;">
                                            <div class="row divcenter">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="potroview">

                                                        <div style="background-color: #a94442; padding: 5px; color: #fff;">
                                                            <div class="pull-left"
                                                                 style="padding-top: 0px;    white-space: nowrap;">
                                                                স্মারক নম্বর:
                                                                &nbsp;&nbsp;<?php echo $draftSummary['sarok_no']; ?>
                                                            </div>
                                                            <div class="pull-right text-right">
                                                                <div class="btn-group">
                                                                    <button type="button"
                                                                            class="btn btn-success btn-xs dropdown-toggle"
                                                                            data-toggle="dropdown"
                                                                            aria-expanded="false">
                                                                        <i class="fa fa-ellipsis-horizontal"></i>
                                                                        প্রিন্ট <i class="fa fa-angle-down"></i>
                                                                    </button>
                                                                    <ul class="dropdown-menu pull-right">
                                                                        <li>
                                                                            <a title="প্রিন্ট করুন"
                                                                               data-id="<?= $row['id'] ?>"
                                                                               href="javascript:void(0)"
                                                                               class="btn-printsummarydraft"><i
                                                                                        class="fs1 a2i_gn_print2"></i>
                                                                                প্রিন্ট প্রিভিউ</a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                                <a title="ড্রাফট পরিবর্তন দেখুন"
                                                                   nothi_master_id="<?php echo $draftSummary['nothi_part_no'] ?>"
                                                                   potrojari="<?php echo $draftSummary['id'] ?>"
                                                                   nothi_office="<?= $nothi_office ?>"
                                                                   class="btn    btn-primary btn-sm btn-changelog">
                                                                    <i class="fs1 a2i_gn_history3"></i>
                                                                </a>
                                                                <a data-title-orginal="খসড়া সার-সংক্ষেপ দেখুন"
                                                                   title="খসড়া সার-সংক্ষেপ দেখুন"
                                                                   href="<?php echo $this->request->webroot ?>SummaryNothi/EditSarNothiDraft/<?php echo $draftSummary['nothi_part_no'] . '/' . $draftSummary['id'] ?>/<?= $nothi_office ?>"
                                                                   class="btn    btn-xs green"> <i
                                                                            class="fs1 a2i_nt_potrojari3"></i> </a>

                                                            </div>
                                                            <div style="clear: both;"></div>
                                                        </div>
                                                        <div class="summaryContentBody">
                                                            <div id="template-body"
                                                                 style="background-color: #fff; max-width:950px; min-height:815px; height: auto; margin:0 auto;">
                                                                <?php echo $draftSummary->potro_cover; ?>
                                                            </div>

                                                            <div id="template-body2"
                                                                 style="background-color: #fff; max-width:950px; min-height:815px; height: auto; margin:0 auto;">
                                                                <?php echo $draftSummary->potro_description; ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            <?php endif; ?>
                            <?php if (!empty($draftPotro)): ?>
                                <div class="tab-pane <?php echo(!empty($draftSummary) ? '' : 'active') ?>"
                                     id="potrodraft_list">
                                    <div class="portlet light ">
                                        <div class="portlet-title">
                                            <div class="row">
                                                <div class="col-lg-7 col-md-7 col-sm-7 text-left">
                                                    <nav>
                                                        <ul class="pager">
                                                            <li><a href="javascript:void(0);"
                                                                   class="DraftimagePrev btn btn-sm default disabled"><</a>
                                                            </li>
                                                            <li><a href="javascript:void(0);"
                                                                   class="DraftimageNext btn btn-sm default">></a>
                                                            </li>
                                                        </ul>
                                                    </nav>
                                                </div>
                                                <div class="col-lg-5 col-md-5 col-sm-5 text-right">
                                                    <p> মোট: <b><?php echo $this->Number->format(count($draftPotro)); ?>
                                                            টি</b></p>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="portlet-body" style="overflow-y: auto;overflow-x:  hidden;">
                                            <div class="row divcenter">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="potroview">
                                                        <?php
                                                        if (!empty($draftPotro)) {
                                                            $i = 0;
                                                            foreach ($draftPotro as $row) {
                                                                echo '<div id_en="' . $row['nothi_potro_page'] . '"  id_bn="' . $row['nothi_potro_page_bn'] . '" class="DraftattachmentsRecord ' . ($i == 0 ? 'active first' : ($i == (count($draftPotro) - 1) ? 'last' : '')) . '">';
                                                                ?>
                                                                <div style="background-color: #a94442; padding: 5px; color: #fff;">
                                                                    <div class="pull-left"
                                                                         style="padding-top: 0px;    white-space: nowrap;">

                                                                        <?php echo mb_substr($row['potro_subject'], 0, 50) . (mb_strlen($row['potro_subject']) > 50 ? '...' : ''); ?>
                                                                    </div>
                                                                    <div class="pull-right text-right">
                                                                        <div class="btn-group">
                                                                            <button type="button"
                                                                                    class="btn btn-success btn-xs dropdown-toggle"
                                                                                    data-toggle="dropdown"
                                                                                    aria-expanded="false">
                                                                                <i class="fa fa-ellipsis-horizontal"></i>
                                                                                প্রিন্ট <i class="fa fa-angle-down"></i>
                                                                            </button>
                                                                            <ul class="dropdown-menu pull-right">
                                                                                <li>
                                                                                    <a title="প্রিন্ট করুন"
                                                                                       data-id="<?= $row['id'] ?>"
                                                                                       data-potro-type ="<?=$row['potro_type']?>"
                                                                                       href="javascript:void(0)"
                                                                                       class="btn-printdraft"><i
                                                                                                class="fs1 a2i_gn_print2"></i>
                                                                                        প্রিন্ট প্রিভিউ</a>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                        <a title="ড্রাফট পরিবর্তন দেখুন"
                                                                           nothi_master_id="<?php echo $row['nothi_part_no'] ?>"
                                                                           potrojari="<?php echo $row['id'] ?>"
                                                                           nothi_office="<?= $nothi_office ?>"
                                                                           class="btn    btn-primary btn-sm btn-changelog">
                                                                            <i class="fs1 a2i_gn_history3"></i>
                                                                        </a>
                                                                        <?php
                                                                        //RM url different
                                                                        if(($row['potro_type'] == 21)){
                                                                            $url = $this->Url->build(['_name' => 'editRm',$row['nothi_part_no'],0,($row['id'] == 0) ?'note':'potro',0,$row['id'] ]);
                                                                        }
                                                                        // formal
                                                                        else if(($row['potro_type'] == 27)){
                                                                            $url = $this->Url->build(['_name' => 'edit-formal-letter',$row['nothi_part_no'],0,($row['id'] == 0) ?'note':'potro',0,$row['id'] ]);
                                                                        }
                                                                        // CS
                                                                        else if(($row['potro_type'] == 30)){
                                                                            $url = $this->Url->build(['_name' => 'edit-cs-letter',$row['nothi_part_no'],0,($row['id'] == 0) ?'note':'potro',0,$row['id'] ]);
                                                                        }
                                                                        else{
                                                                            $url = $this->Url->build(['controller' => 'Potrojari','action' => 'potroDraft',$row['nothi_part_no'],$row['id'] ,($row['id'] == 0) ?'note':'potro',$nothi_office ]);
                                                                        }
                                                                        ?>
                                                                        <a data-title-orginal="খসড়া পত্র দেখুন"
                                                                           title="খসড়া পত্র দেখুন"
                                                                           href="<?php echo $url ?>"
                                                                           class="btn    btn-xs green"> <i
                                                                                    class="fs1 a2i_nt_potrojari3"></i>
                                                                        </a>

                                                                    </div>
                                                                    <div style="clear: both;"></div>
                                                                </div>
                                                                <?php
                                                                $header = jsonA($row->meta_data);
                                                                if(isset($row['potro_type']) && $row['potro_type'] == 30){
                                                                    echo '<ul class="nav nav-tabs nav-justified">
                                                                            <li class="active">
                                                                                <a href="#csCover" data-toggle="tab" aria-expanded="true">ফরওয়ার্ডিং পত্র</a>
                                                                            </li>
                                                                            <li class="">
                                                                                <a href="#csBody" data-toggle="tab" aria-expanded="true">মূল পত্র (<input type="checkbox" class ="cs-body-approve" data-cs="'.$row['id'].'" /> '.__('Approval').' )</a>
                                                                            </li>
                                                                        </ul>
                                                                        <div class="tab-content">
                                                                            <div class="tab-pane active" id="csCover">'.$row['potro_cover'];
                                                                    echo '</div><div class="tab-pane" id="csBody">';
                                                                    echo '<div id="template-body" potrojari_language="'.h($row['potrojari_language']).'" style="background-color: #fff; max-width:950px; min-height:815px; height: auto; margin:0 auto;">' . (!empty($header['potro_header_banner'])?('<div class="potrojari_header_banner" style="text-align: '.($header['banner_position']).'!important;"><img src="'.$this->request->webroot . 'getContent?file='. base64_decode($header['potro_header_banner']).'&token=' . sGenerateToken(['file'=>base64_decode($header['potro_header_banner'])],['exp'=>time() + 60*300]) .'" style="width:'.($header['banner_width']).';height: 60px;max-width: 100%;"/></div>'):'') .'<div class="showimageforce">'. html_entity_decode($row['attached_potro']) . '</div><br/>' . html_entity_decode($row['potro_description']) . "</div>";
                                                                    echo '</div></div>';
                                                                }else{
                                                                    echo '<div id="template-body" potrojari_language="'.h($row['potrojari_language']).'" style="background-color: #fff; max-width:950px; min-height:815px; height: auto; margin:0 auto;">' . (!empty($header['potro_header_banner'])?('<div class="potrojari_header_banner" style="text-align: '.($header['banner_position']).'!important;"><img src="'.$this->request->webroot . 'getContent?file='. base64_decode($header['potro_header_banner']).'&token=' . sGenerateToken(['file'=>base64_decode($header['potro_header_banner'])],['exp'=>time() + 60*300]) .'" style="width:'.($header['banner_width']).';height: 60px;max-width: 100%;"/></div>'):'') .'<div class="showimageforce">'. html_entity_decode($row['attached_potro']) . '</div><br/>' . html_entity_decode($row['potro_description']) . "</div>";
                                                                }
                                                                ?>
                                                                <div class="table">
                                                                    <table role="presentation"
                                                                           class="table table-striped clearfix">
                                                                        <tbody class="files">
                                                                        <?php

                                                                        if (isset($draftPotroAttachments[$row['id']]) && count($draftPotroAttachments[$row['id']]) > 0) {
                                                                            foreach ($draftPotroAttachments[$row['id']] as $ke => $single_data) {
                                                                                if ($single_data['attachment_type'] == 'text' || $single_data['attachment_type'] == 'text/html') {
                                                                                    continue;
                                                                                }

                                                                                $fileName = explode('/', $single_data['file_name']);
                                                                                $attachmentHeaders = get_file_type($single_data['file_name']);
                                                                                $value = array(
                                                                                    'name' => urldecode($fileName[count($fileName) - 1]),
                                                                                    'thumbnailUrl' => $this->request->webroot . 'content/' . $single_data['file_name'],
                                                                                    'url' => $this->request->webroot . 'content/' . $single_data['file_name'],

                                                                                );


                                                                                echo '<tr class="template-download fade in">

<td>
<p class="name">
' . (isset($value['name']) && (substr($single_data['attachment_type'], 0, 5) == 'image' || $single_data['attachment_type'] == 'application/pdf') ?
                                                                                        '<a href="' . $single_data['id'] . '/showPotroAttachment/' . $row['nothi_part_no'] . '" 
title="' . $value['name'] . '" class="showforPopup">' . urldecode($value['name']) . '</a>' : (isset($value['name']) ? urldecode($value['name']) : '')) . '
</p>
</td>
<td>
' . (($single_data['attachment_type'] != 'text' && $single_data['attachment_type'] != 'text/html') ? $this->Html->link('<i class="fs1 fa fa-download"></i> &nbsp;', ['controller' => 'Potrojari',
                                                                                        'action' => 'downloadPotro',
                                                                                        $nothi_office,
                                                                                        $single_data['id']], ['escape' => false]) : '') . '
</td>
</tr>';
                                                                            }
                                                                        }
                                                                        ?>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                                <?php
                                                                echo "</div>";
                                                                $i++;
                                                            }
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <?php if (!empty($summaryAttachmentRecord)): ?>
                                <div class="tab-pane <?php echo((empty($draftSummary) && empty($draftPotro)) ? 'active' : '') ?>"
                                     id="summaryfinal_list">
                                    <div class="portlet light ">
                                        <div class="portlet-title">
                                            <div class="row">
                                                <div class="col-lg-7 col-md-7 col-sm-7 text-left">
                                                    <nav>
                                                        <ul class="pager">
                                                            <li><a href="javascript:void(0);"
                                                                   class="summaryDraftimagePrev btn btn-sm default disabled"><</a>
                                                            </li>
                                                            <li><a href="javascript:void(0);"
                                                                   class="summaryDraftimageNext btn btn-sm default">></a>
                                                            </li>
                                                        </ul>
                                                    </nav>
                                                </div>
                                                <div class="col-lg-5 col-md-5 col-sm-5 text-right">
                                                    <p> মোট: <b><?php
                                                            $totalSum = !empty($summaryAttachmentRecord) ? count($summaryAttachmentRecord) : 0;
                                                            echo $this->Number->format($totalSum);
                                                            ?>টি</b></p>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="portlet-body" style="overflow-y: auto;overflow-x:  hidden;">
                                            <div class="row divcenter">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="potroview">
                                                        <?php
                                                        if (!empty($summaryAttachmentRecord)) {
                                                            $i = 0;
                                                            foreach ($summaryAttachmentRecord as $row) {
                                                                echo '<div id_en="' . $row['nothi_potro_page'] . '"  id_bn="' . $row['nothi_potro_page_bn'] . '" class="summaryDraftattachmentsRecord ' . ($i == 0 ? 'active first' : ($i == (count($draftPotro) - 1) ? 'last' : '')) . '">';
                                                                ?>
                                                                <div style="background-color: #a94442; padding: 5px; color: #fff;">
                                                                    <div class="pull-left"
                                                                         style="padding-top: 0px;    white-space: nowrap;">
                                                                        স্মারক নম্বর:
                                                                        &nbsp;&nbsp;<?php echo $row['sarok_no']; ?>
                                                                    </div>
                                                                    <div class="pull-right text-right">
                                                                        <div class="btn-group">
                                                                            <button type="button"
                                                                                    class="btn btn-success btn-xs dropdown-toggle"
                                                                                    data-toggle="dropdown"
                                                                                    aria-expanded="false">
                                                                                <i class="fa fa-ellipsis-horizontal"></i>
                                                                                প্রিন্ট <i class="fa fa-angle-down"></i>
                                                                            </button>
                                                                            <ul class="dropdown-menu pull-right">
                                                                                <li>
                                                                                    <a title="প্রিন্ট করুন"
                                                                                       data-id="<?= $row['id'] ?>"
                                                                                       href="javascript:void(0)"
                                                                                       class="btn-summaryprintdraft"><i
                                                                                                class="fs1 a2i_gn_print2"></i>
                                                                                        প্রিন্ট প্রিভিউ</a>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                        <a data-title-orginal="সার-সংক্ষেপ দেখুন"
                                                                           title="সার-সংক্ষেপ দেখুন"
                                                                           href="<?php echo $this->request->webroot ?>SummaryNothi/summaryPotroShow/<?php echo $row['nothi_part_no'] . '/' . $row['nothi_potro_id'] ?> /<?= $nothi_office ?>"
                                                                           class="btn    btn-xs green"> <i
                                                                                    class="fs1 a2i_nt_potrojari3"></i>
                                                                        </a>

                                                                    </div>

                                                                    <div style="clear: both;"></div>
                                                                </div>
                                                                <?php
                                                                if ($row['attachment_type'] == 'text') {

                                                                    echo '<div style="background-color: #fff; max-width:950px; min-height:815px;  margin:0 auto; page-break-inside: auto;"><div style="background-color: #fff; max-width:950px; min-height:815px; ">' . html_entity_decode($row['potro_cover']) . "</div>" . html_entity_decode($row['content_body']) . "</div>";
                                                                } elseif (substr($row['attachment_type'], 0, 5) != 'image') {
                                                                    if (substr($row['attachment_type'], 0, strlen('application/vnd')) == 'application/vnd' || substr($row['attachment_type'], 0, strlen('application/ms')) == 'application/ms') {
                                                                        $url = urlencode(FILE_FOLDER . $row['file_name'] . '?token=' . sGenerateToken(['file' => $row['file_name']], ['exp' => time() + 60 * 300]));
                                                                        echo '<iframe style=" width:100%; height: 700px;" src="https://docs.google.com/gview?url=' . $url . '&embedded=true" style="width:600px; height:500px;" frameborder="0"></iframe>';
                                                                    } else {
//              echo '<iframe style=" width:100%; height: 700px;" src="https://docs.google.com/gview?url=' . $this->request->webroot . 'content/'.$row['file_name'] . '&embedded=true" style="width:600px; height:500px;" frameborder="0"></iframe>';
                                                                        echo '<embed src="' . $this->request->webroot . 'getContent?file=' . $row['file_name'] . '&token=' . sGenerateToken(['file' => $row['file_name']], ['exp' => time() + 60 * 300]) . '" style=" width:100%; height: 700px;" type="' . $row['attachment_type'] . '"></embed>';
                                                                    }
                                                                } else {
                                                                    echo '<div class="text-center"><a href="' . $this->request->webroot . 'content/' . $row['file_name'] . '?token=' . sGenerateToken(['file' => $row['file_name']], ['exp' => time() + 60 * 300]) . '" onclick="return false;" data-title="পত্র নম্বর:  ' . $row['nothi_potro_page_bn'] . '" data-footer="" ><img class="zoomimg img-responsive"  src="' . $this->request->webroot . 'content/' . $row['file_name'] . '?token=' . sGenerateToken(['file' => $row['file_name']], ['exp' => time() + 60 * 300]) . '" alt="ছবি পাওয়া যায়নি"></a></div>';
                                                                }
                                                                echo "</div>";
                                                                $i++;
                                                            }
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <div class="tab-pane <?php echo(empty($draftSummary) && empty($summaryAttachmentRecord) ? (!empty($draftPotro) ? '' : 'active') : '') ?>"
                                 id="potroview_list">

                                <div class="portlet light  ">
                                    <div class="portlet-title">
                                        <div class="row">
                                            <div class="col-lg-7 col-md-7 col-sm-7 text-left">
                                                <nav>
                                                    <ul class="pager">
                                                        <li><a href="javascript:void(0);"
                                                               class="imagePrev btn btn-sm default disabled"><</a>
                                                        </li>
                                                        <li>
                                                            <input value="<?php echo $this->Number->format(!empty($potroAttachmentRecord) ? $potroAttachmentRecord[0]['nothi_potro_page'] : 0) ?>"
                                                                   type="text"
                                                                   class="imageNo pagination-panel-input form-control input-mini input-inline input-sm potroNo"
                                                                   maxlength="20"
                                                                   style="text-align:center; margin: 0 5px;">
                                                        </li>
                                                        <li><a href="javascript:void(0);"
                                                               class="imageNext btn btn-sm default">></a>
                                                        </li>
                                                        <?php if (!empty($total)): ?>

                                                        <?php endif; ?>
                                                    </ul>
                                                </nav>
                                            </div>
                                            <div class="col-lg-5 col-md-5 col-sm-5 text-right">
                                                <p> মোট: <b><?php echo $this->Number->format($total); ?>টি</b></p>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="portlet-body" style="overflow-y: auto;overflow-x:  hidden;">
                                        <div class="row divcenter">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="potroview">
                                                    <?php
                                                    if (!empty($potroAttachmentRecord)) {
                                                        $i = 0;
                                                        foreach ($potroAttachmentRecord as $row) {
                                                            echo '<div id_ar="' . $row['id'] . '" id_en="' . $row['nothi_potro_page'] . '"  id_bn="' . $row['nothi_potro_page_bn'] . '" class="attachmentsRecord ' . ($i == 0 ? 'active first' : ($i == (count($potroAttachmentRecord) - 1) ? 'last' : '')) . '">';
                                                            ?>
                                                            <div style="background-color: #a94442; padding: 5px; color: #fff;">
                                                                <div class="pull-left"
                                                                     style="padding-top: 0px;    white-space: nowrap;">
                                                                    <?php echo mb_substr($row['nothi_potro']['subject'], 0, 50) . (mb_strlen($row['nothi_potro']['subject']) > 50 ? '...' : ''); ?>
                                                                    <?php echo (!empty($row['nothi_potro']['application_origin']) && $row['nothi_potro']['application_origin'] != 'nothi'?(" (".strtoupper($row['nothi_potro']['application_origin'])[0].")"):''); ?>
                                                                </div>
                                                                <div class="pull-right text-right">
                                                                    <?php
                                                                    if ($row['is_summary_nothi'] == 0 && $row['attachment_type'] == 'text' && !empty($row['potrojari_id']) && empty($row['potrojari_data']['attached_potro_id']) && $otherNothi == false) {
                                                                        ?>
                                                                        <button type="button"
                                                                                class="btn   btn-sm btn-warning"
                                                                                onclick="selectNote(<?= $row['potrojari_id'] ?>)"
                                                                                data-toggle="tooltip"
                                                                                title="পত্রজারি ক্লোন করুন">
                                                                            <span class="glyphicon glyphicon-copy"></span>
                                                                        </button>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                    <?php if ($row['attachment_type'] == 'text' || substr($row['attachment_type'], 0, 5) == 'image' || $row['attachment_type'] != 'application/pdf') { ?>
                                                                        <a data-id=<?= $row['id'] ?> href="javascript:void(0)"
                                                                           class="btn    green btn-sm btn-print"><i
                                                                                    class="fs1 a2i_gn_print2"></i></a>
                                                                    <?php } ?>
                                                                    <?php if ($row['attachment_type'] != 'text' && substr($row['attachment_type'], 0, 5) != 'image') { ?>

                                                                        <a title="ডাউনলোড করুন" href="<?=
                                                                        $this->Url->build(['controller' => 'NothiNoteSheets',
                                                                            'action' => 'downloadPotro',
                                                                            $nothi_office,
                                                                            $row['id']])
                                                                        ?>" class="btn   blue btn-xs"><i
                                                                                    class="fs1 fa fa-download"></i></a>
                                                                    <?php } ?>

                                                                </div>
                                                                <div style="clear: both;"></div>
                                                            </div>
                                                            <?php
                                                            $row['attachment_type'] = str_replace("; charset=binary", "", $row['attachment_type']);

                                                            if ($row['attachment_type'] == 'text') {

                                                                if ($row['is_summary_nothi'] == 1) {
                                                                    echo '<div style="background-color: #fff; max-width:950px; min-height:815px;  margin:0 auto; page-break-inside: auto;">' . html_entity_decode($row['potro_cover']) . "<br/>" . html_entity_decode($row['content_body']) . "</div>";
                                                                } else {
                                                                    $header = jsonA($row['meta_data']);
                                                                    echo '<div style="background-color: #fff; max-width:950px; min-height:815px;  margin:0 auto; page-break-inside: auto;">' . (!empty($header['potro_header_banner']) ? ('<div style="text-align: ' . ($header['banner_position']) . '!important;"><img src="' . $this->request->webroot . 'getContent?file=' . base64_decode($header['potro_header_banner']) . '&token=' . sGenerateToken(['file' => base64_decode($header['potro_header_banner'])], ['exp' => time() + 60 * 300]) . '" style="width:' . ($header['banner_width']) . ';height: 60px;max-width: 100%;"/></div>') : '') . html_entity_decode($row['content_body']) . "</div>";
                                                                }
                                                            } elseif (substr($row['attachment_type'], 0, 5) != 'image') {
                                                                if (substr($row['attachment_type'], 0, strlen('application/vnd')) == 'application/vnd' || substr($row['attachment_type'], 0, strlen('application/ms')) == 'application/ms') {
                                                                    $url = urlencode(FILE_FOLDER . $row['file_name'] . '?token=' . sGenerateToken(['file' => $row['file_name']], ['exp' => time() + 60 * 300]));
                                                                    echo '<iframe style=" width:100%; height: 700px;" src="https://docs.google.com/gview?url=' . $url . '&embedded=true" style="width:600px; height:500px;" frameborder="0"></iframe>';
                                                                } else {
                                                                    echo '<embed src="' . $this->request->webroot . 'getContent?file=' . $row['file_name'] . '&token=' . sGenerateToken(['file' => $row['file_name']], ['exp' => time() + 60 * 300]) . '" style=" width:100%; height: 700px;" type="' . $row['attachment_type'] . '"></embed>';
                                                                }
                                                            } else {
                                                                echo '<div class="text-center"><a href="' . $this->request->webroot . 'content/' . $row['file_name'] . '?token=' . sGenerateToken(['file' => $row['file_name']], ['exp' => time() + 60 * 300]) . '" onclick="return false;" data-title="পত্র নম্বর:  ' . $row['nothi_potro_page_bn'] . '" data-footer="" ><img class="zoomimg img-responsive"  src="' . $this->request->webroot . 'content/' . $row['file_name'] . '?token=' . sGenerateToken(['file' => $row['file_name']], ['exp' => time() + 60 * 300]) . '" alt="ছবি পাওয়া যায়নি"></a></div>';
                                                            }
                                                            echo "</div>";
                                                            $i++;
                                                        }
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php // if (!empty($potroNothijatoAttachmentRecord)): ?>
                            <div class="tab-pane " id="nothijato_list">
                                <div class="tab-pane fade notloaded selectedpotrogroup"
                                     id="nothijatopotroview_accordian">
                                    <div class="portlet light ">
                                        <div class="portlet-body">
                                            <div class="row divcenter">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="potroview">
                                                        <div class="portlet-body" style="min-height: 200px;">
                                                            <div class="nothijatopotroview_div">

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php // endif; ?>
                            <div class="tab-pane fade notloaded selectedpotrogroup" id="potroview_accordian">
                                <div class="portlet light">
                                    <div class="portlet-body">
                                        <div class="row divcenter">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="potroview">
                                                    <div class="portlet-body">
                                                        <div class="panel-group accordion  potroview_div">
                                                        </div>
                                                        <button class="btn    btn-sm purple btn-choose-potro"
                                                                style="display: none;"><span
                                                                    class="glyphicon glyphicon-new-window"></span>
                                                            একসাথে দেখুন
                                                        </button>
                                                    </div>
                                                </div>
                                                <div class="selectedpotroview" style="display: none;">
                                                    <div class="portlet-body">
                                                        <button class="btn    btn-sm btn-primary back_btn-choose-potro">
                                                            <span class="glyphicon glyphicon-share-alt"></span> ফেরত যান
                                                        </button>
                                                        <br/>

                                                        <div class="panel-group accordion scrollable potroselected_view">
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<div id="responsiveChangeLog" class="modal fade" tabindex="-1" aria-hidden="true" data-backdrop="static"
     data-keyboard="false">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">খসড়া পত্রের পরিবর্তনসমূহ</h4>
            </div>
            <div class="modal-body" style="background-color: #828282;">
                <div class="scroller" style="height:100%; max-height: 500px;" data-always-visible="1"
                     data-rail-visible1="1">

                </div>
            </div>
        </div>
    </div>
</div>

<div id="responsivepotalaLog" class="modal fade" tabindex="-1" aria-hidden="true" data-backdrop="static"
     data-keyboard="false">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">সংলাগ</h4>
            </div>
            <div class="modal-body" style="background-color: #828282;">
                <div class="scroller" style="height:100%; max-height: 500px;" data-always-visible="1"
                     data-rail-visible1="1">

                </div>
            </div>
        </div>
    </div>
</div>

<?= $this->element('preview_ele'); ?>
<script src="<?php echo $this->request->webroot; ?>projapoti-nothi/js/master_file_related.js?v=<?= time() ?>"
        type="text/javascript"></script>

<?php echo $this->Html->script('assets/global/scripts/printThis.js'); ?>
<?php if ((count($draftPotro) || count($draftSummary))): ?>

    <script>

		$(document).on('click', '.btn-changelog', function () {
			$('#responsiveChangeLog').modal('show');
			$('#responsiveChangeLog').find('.scroller').html('<img src="' + js_wb_root + 'assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp;লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');

			var potrojari_id = $(this).attr('potrojari');
			var nothi_master_id = $(this).attr('nothi_master_id');
			$.ajax({
				url: "<?= $this->Url->build(['controller' => 'Potrojari', 'action' => 'showChangeLog']) ?>",
				data: {
					nothi_master_id: nothi_master_id,
					potrojari_id: potrojari_id,
					nothi_office:<?php echo $nothi_office ?>},
				method: 'post',
				dataType: 'html',
				cache: false,
				success: function (response) {
					$('#responsiveChangeLog').find('.scroller').html(response);
				},
				error: function (err, status, rspn) {
					$('#responsiveChangeLog').find('.scroller').html('');
				}
			});
		});
    </script>
<?php endif; ?>
<script src="<?php echo CDN_PATH; ?>assets/admin/pages/scripts/portlet-draggable.js"></script>
<script>
	var numberArray = {
		1: '১',
		2: '২',
		3: '৩',
		4: '৪',
		5: '৫',
		6: '৬',
		7: '৭',
		8: '৮',
		9: '৯',
		0: '০'
	};

	//summary nothi
	var summarydraftpotroPageCount = 1;
	$(document).on('click', '.summaryDraftimageNext', function () {

		var firstndex = ($('.summaryDraftattachmentsRecord').first().index() == -1 ? 0 : $('.summaryDraftattachmentsRecord').first().index());
		var nowIndex = $('.summaryDraftattachmentsRecord.active').index();
		var lastIndex = $('.summaryDraftattachmentsRecord').last().index() == -1 ? 0 : $('.summaryDraftattachmentsRecord').last().index();

		if (nowIndex >= firstndex) {
			$('.summaryDraftimagePrev').removeClass('disabled');
		}

		if (lastIndex == 0 || nowIndex >= (<?php echo !empty($summaryAttachmentRecord) ? (count($summaryAttachmentRecord) - 1) : 0 ?>)) {
			$(this).addClass('disabled');
			return;
		}

		if ($('.summaryDraftattachmentsRecord').eq(nowIndex).hasClass('last') == false) {
			nowIndex++;
			$('.summaryDraftattachmentsRecord').removeClass('active');
			$('.summaryDraftattachmentsRecord').eq(nowIndex).addClass('active');
		}
	});

	$(document).on('click', '.summaryDraftimagePrev', function () {
		var firstndex = ($('.summaryDraftattachmentsRecord').first().index() == -1 ? 0 : $('.summaryDraftattachmentsRecord').first().index());
		var nowIndex = $('.summaryDraftattachmentsRecord.active').index();
		var lastIndex = $('.summaryDraftattachmentsRecord').last().index() == -1 ? 0 : $('.summaryDraftattachmentsRecord').last().index();

		if (nowIndex < lastIndex) {
			$('.summaryDraftimageNext').removeClass('disabled');
		}

		if (nowIndex <= 0) {
			$('.summaryDraftimageNext').removeClass('disabled');
			$(this).addClass('disabled');
			return;
		}


		if ($('.summaryDraftattachmentsRecord').eq(nowIndex).hasClass('first') == false) {
			$('.summaryDraftattachmentsRecord').removeClass('active');
			$('.summaryDraftattachmentsRecord').eq(nowIndex - 1).addClass('active');
		} else {
			$('.summaryDraftimagePrev').addClass('disabled');
			return;
		}

	});
	//draft
	var draftpotroPageCount = 1;
	$(document).on('click', '.DraftimageNext', function () {

		var firstndex = ($('.DraftattachmentsRecord').first().index() == -1 ? 0 : $('.DraftattachmentsRecord').first().index());
		var nowIndex = $('.DraftattachmentsRecord.active').index();
		var lastIndex = $('.DraftattachmentsRecord').last().index() == -1 ? 0 : $('.DraftattachmentsRecord').last().index();

		if (nowIndex >= firstndex) {
			$('.DraftimagePrev').removeClass('disabled');
		}

		if (lastIndex == 0 || nowIndex >= (<?php echo count($draftPotro) - 1 ?>)) {
			$(this).addClass('disabled');
			return;
		}

		if ($('.DraftattachmentsRecord').eq(nowIndex).hasClass('last') == false) {
			nowIndex++;
			$('.DraftattachmentsRecord').removeClass('active');
			$('.DraftattachmentsRecord').eq(nowIndex).addClass('active');
		}
	});

	$(document).on('click', '.DraftimagePrev', function () {
		var firstndex = ($('.DraftattachmentsRecord').first().index() == -1 ? 0 : $('.DraftattachmentsRecord').first().index());
		var nowIndex = $('.DraftattachmentsRecord.active').index();
		var lastIndex = $('.DraftattachmentsRecord').last().index() == -1 ? 0 : $('.DraftattachmentsRecord').last().index();

		if (nowIndex < lastIndex) {
			$('.DraftimageNext').removeClass('disabled');
		}

		if (nowIndex <= 0) {
			$('.DraftimageNext').removeClass('disabled');
			$(this).addClass('disabled');
			return;
		}


		if ($('.DraftattachmentsRecord').eq(nowIndex).hasClass('first') == false) {
			$('.DraftattachmentsRecord').removeClass('active');
			$('.DraftattachmentsRecord').eq(nowIndex - 1).addClass('active');
		} else {
			$('.DraftimagePrev').addClass('disabled');
			return;
		}

	});

	//nothijato
	var draftpotroPageCount = 1;
	$(document).on('click', '.NothijatoimageNext', function () {
		var firstndex = ($('.NothijatoattachmentsRecord').first().index() == -1 ? 0 : $('.NothijatoattachmentsRecord').first().index());
		var nowIndex = $('.NothijatoattachmentsRecord.active').index();
		var lastIndex = $('.NothijatoattachmentsRecord').last().index() == -1 ? 0 : $('.NothijatoattachmentsRecord').last().index();

		if (nowIndex >= firstndex) {
			$('.NothijatoimagePrev').removeClass('disabled');
		}

		if (lastIndex == 0 || nowIndex >= (<?php echo count($potroNothijatoAttachmentRecord) - 1 ?>)) {
			$(this).addClass('disabled');
			return;
		}

		if ($('.NothijatoattachmentsRecord').eq(nowIndex).hasClass('last') == false) {
			nowIndex++;
			$('.NothijatoattachmentsRecord').removeClass('active');
			$('.NothijatoattachmentsRecord').eq(nowIndex).addClass('active');
		}
	});

	$(document).on('click', '.NothijatoimagePrev', function () {
		var firstndex = ($('.NothijatoattachmentsRecord').first().index() == -1 ? 0 : $('.NothijatoattachmentsRecord').first().index());
		var nowIndex = $('.NothijatoattachmentsRecord.active').index();
		var lastIndex = $('.NothijatoattachmentsRecord').last().index() == -1 ? 0 : $('.NothijatoattachmentsRecord').last().index();

		if (nowIndex < lastIndex) {
			$('.NothijatoimageNext').removeClass('disabled');
		}

		if (nowIndex <= 0) {
			$('.NothijatoimageNext').removeClass('disabled');
			$(this).addClass('disabled');
			return;
		}

		if ($('.NothijatoattachmentsRecord').eq(nowIndex).hasClass('first') == false) {
			$('.NothijatoattachmentsRecord').removeClass('active');
			$('.NothijatoattachmentsRecord').eq(nowIndex - 1).addClass('active');
		} else {
			$('.NothijatoimagePrev').addClass('disabled');
			return;
		}

	});

	//potro

	var potroPageCount = 1;
	$(document).on('click', '.imageNext', function () {

		var firstndex = ($('.attachmentsRecord').first().index() == -1 ? 0 : $('.attachmentsRecord').first().index());
		var nowIndex = $('.attachmentsRecord.active').index();
		var lastIndex = $('.attachmentsRecord').last().index() == -1 ? 0 : $('.attachmentsRecord').last().index();

		if (nowIndex >= firstndex) {
			$('.imagePrev').removeClass('disabled');
		}

		if (lastIndex == 0 || nowIndex >= (<?php echo $total - 1 ?>)) {
			$(this).addClass('disabled');
			return;
		}

		if ($('.attachmentsRecord').eq(nowIndex).hasClass('last') == false) {
			nowIndex++;
			$('.attachmentsRecord').removeClass('active');
			$('.attachmentsRecord').eq(nowIndex).addClass('active');
			var potroPage = $('.attachmentsRecord').eq(nowIndex).attr('id_bn');
			getpotropage(potroPage);

		} else {
			$('.attachmentsRecord').removeClass('active');
			$('.attachmentsRecord').removeClass('last');
			potroPageCount++;
			$.ajax({
				url: '<?php echo $this->request->webroot ?>archive/nextAllPotroPage/<?php echo $part_id . '/'; ?>' + potroPageCount,
				dataType: 'JSON', type: 'post',
				data: {nothi_office:<?php echo $nothi_office ?>},
				success: function (response) {

					if (response.html == '') {
						$('.imageNext').addClass('disabled');
					} else {
						$('#potroview_list .potroview').append(response.html);
						getpotropage(response.potrono);
						$('.imageNext').removeClass('disabled');
					}

				}

			});
		}
	});

	$(document).on('click', '.imagePrev', function () {
		var firstndex = ($('.attachmentsRecord').first().index() == -1 ? 0 : $('.attachmentsRecord').first().index());
		var nowIndex = $('.attachmentsRecord.active').index();
		var lastIndex = $('.attachmentsRecord').last().index() == -1 ? 0 : $('.attachmentsRecord').last().index();

		if (nowIndex < lastIndex) {
			$('.imageNext').removeClass('disabled');
		}

		if (nowIndex <= 0) {
			$('.imageNext').removeClass('disabled');
			$(this).addClass('disabled');
			return;
		}


		if ($('.attachmentsRecord').eq(nowIndex).hasClass('first') == false) {
			$('.attachmentsRecord').removeClass('active');
			$('.attachmentsRecord').eq(nowIndex - 1).addClass('active');
			var potroPage = $('.attachmentsRecord').eq(nowIndex - 1).attr('id_bn');
			getpotropage(potroPage);

		} else {
			$('.imagePrev').addClass('disabled');
			return;
		}

	});

	$(document).on('keypress', '.potroNo', function (key) {
		if (key.keyCode == 13) {
			goToPage($(this).val());
		}
	});

	$(document).on('click', '.potroview_tab li', function () {

		var thisIndex = $(this).index();
		var isNotLoaded = $('.potroDetailsPage .tab-pane').eq(thisIndex).hasClass('notloaded');

		if (isNotLoaded) {
			var idIs = $('.potroDetailsPage .tab-pane').eq(thisIndex).attr('id');


			if (idIs == 'potroview_list') {
				$('#potroview_list .potroview').html('<img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp;লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');
				goToPage();
			} else {
				$('#potroview_accordian .potroview .potroview_div').html('<img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp;লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');
				potroAccordianView();
				$('.potroDetailsPage .tab-pane').eq(thisIndex).removeClass('notloaded');
			}
		}

	});

	$(document).on('click', '.potroview_div a.collapsed', function () {

		var hasPreviousExpand = $(this).hasClass('expand-already');

		if (hasPreviousExpand == false) {
			potroforAccordian($(this).attr('id'), $(this).attr('src'));
			$(this).addClass('expand-already');
		}
	});

	$(document).on('click', '.potro_choose', function () {
		if ($('.potro_choose:checked').length > 0) {
			$('.btn-choose-potro').show();
		} else {
			$('.btn-choose-potro').hide();
		}
	});

	$(document).on('click', '.btn-choose-potro', function () {
		var hasChoosen = $('.potro_choose:checked').length;

		if (hasChoosen != 0) {
			var selected = "<div class='row' id='sortable_portlets'>";

			$.each($('.potro_choose:checked'), function (index) {
				var datahtml = $(this).next('.img').length == 1 ? $(this).next('.img').html() : ($(this).next('iframe').length == 1 ? $(this).next('iframe').html() : ($(this).next('embed').length == 1 ? $(this).next('embed').html() : $(this).next('div').html()));
				selected += "<div class='col-md-6  column sortable'><div class='portlet portlet-sortable box green-haze'><div class='portlet-title'><div class='caption'>" + $(this).attr('potro_page_no') + "</div></div><div class='portlet-body'>" + datahtml + '</div></div><div class="portlet portlet-sortable-empty"></div></div>';
			});
			selected += "</div>";

			gotoSelectedPotors(selected);
		}
	});

	$(document).on('click', '.potroAccordianPaginator .nextImageAccordian', function () {

		var accordianId = $(this).parents('ul').attr('id');

		var firstndex = $('.potroAccordianPaginationBody.potro_details_accordian_div_' + accordianId).first().index();
		var nowIndex = $('.potroAccordianPaginationBody.potro_details_accordian_div_' + accordianId + '.active').index();
		var lastIndex = $('.potroAccordianPaginationBody.potro_details_accordian_div_' + accordianId).last().index();

		if (nowIndex < lastIndex) {
			nowIndex++;
			$('.potroAccordianPaginator .prevImageAccordian').removeAttr('disabled');
		} else {
			$(this).attr('disabled', 'disabled');
		}

		$('.potroAccordianPaginationBody.potro_details_accordian_div_' + accordianId).removeClass('active');
		$('.potroAccordianPaginationBody.potro_details_accordian_div_' + accordianId).eq(nowIndex).addClass('active');
	});

	$(document).on('click', '.potroAccordianPaginator .prevImageAccordian', function () {

		var accordianId = $(this).parents('ul').attr('id');

		var firstndex = $('.potroAccordianPaginationBody.potro_details_accordian_div_' + accordianId).first().index();
		var nowIndex = $('.potroAccordianPaginationBody.potro_details_accordian_div_' + accordianId + '.active').index();
		var lastIndex = $('.potroAccordianPaginationBody.potro_details_accordian_div_' + accordianId).last().index();

		if (nowIndex > 0) {
			$('.potroAccordianPaginator .nextImageAccordian').removeAttr('disabled');
			nowIndex--;
		} else {
			$(this).attr('disabled', 'disabled');
		}
		$('.potroAccordianPaginationBody.potro_details_accordian_div_' + accordianId).removeClass('active');
		$('.potroAccordianPaginationBody.potro_details_accordian_div_' + accordianId).eq(nowIndex).addClass('active');
	});

	$(document).on('click', '.back_btn-choose-potro', function () {
		$('#potroview_accordian .selectedpotroview .potroselected_view').html('');
		$('.potroview').show();
		$('.selectedpotroview').hide();
	});

	$('.sokolnothijatopotro').click(function () {
		if ($(this).hasClass('loaded') == false) {
			Metronic.blockUI({
				target: '#nothijato_list',
				boxed: true,
				message: 'অপেক্ষা করুন'
			});
			$.ajax({
				url: '<?= $this->Url->build(['controller' => 'NothiMasters', 'action' => 'loadNothijatoPotrowithPagination', $nothimastersid, $nothi_office])?>',
				method: 'post',
				cache: true,
				dataType: 'html',
				success: function (response) {
					$('.sokolnothijatopotro').addClass('loaded');
					$('#nothijatopotroview_accordian').removeClass('fade');
//                    $('#nothijato_list').html(response);
					$('.nothijatopotroview_div').html(response);
					Metronic.unblockUI('#nothijato_list');
				},
				error: function (xhr, status, errorThrown) {
					$('.sokolnothijatopotro').removeClass('loaded');
					Metronic.unblockUI('#nothijato_list');
				}
			});
		}
	});

	function potroShow(href) {
		$('.potroview').html('<img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp;লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');
		$.ajax({
			url: href,
			success: function (response) {
				$('.potroview').html(response);
				Metronic.initSlimScroll('.scroller');

				if (typeof (pageno) != 'undefined') {
					goToPage(pageno);
					getpotropage(pageno);
				}

				$('[data-title-orginal]').tooltip({'placement':'bottom'});

				$('li').tooltip({'placement':'bottom'});

			}
		})

		$('[data-title-orginal]').tooltip({'placement':'bottom'});

		$('li').tooltip({'placement':'bottom'});

	}

	function checkBookmark() {
		var isBooked = false;
		$.each($('.potroFlagButton'), function (i, v) {
			if (
				$('.attachmentsRecord.active').attr('id_en') == $(this).attr('btn_potro_no') ||
				$('.attachmentsRecord.active').attr('id_bn') == $(this).attr('btn_potro_no')
			) {
				isBooked = true;
			}
		});

		if (isBooked)
			$('.bookmarkImgPotro').removeClass('fa-bookmark-o').addClass('fa-bookmark');
		else
			$('.bookmarkImgPotro').addClass('fa-bookmark-o').removeClass('fa-bookmark');
	}

	function getpotropage(potroPage) {
		$('.potroNo').val(potroPage);
//        checkBookmark();
	}

	function goToPage(pageNo) {
		var hasDivbn = $('.attachmentsRecord ').index($('[id_bn=' + pageNo + ']'));
		var hasDiv = $('.attachmentsRecord ').index($('[id_en=' + pageNo + ']'));

		if (hasDivbn != -1 || hasDiv != -1) {
			$('.attachmentsRecord').removeClass('active');
			if (hasDivbn != -1) {
				$('.attachmentsRecord').eq(hasDivbn).addClass('active');
			} else if (hasDiv != -1) {
				$('.attachmentsRecord').eq(hasDiv).addClass('active');
			}
		}
//        checkBookmark();
	}

	function potroAccordianView() {

		$.ajax({
			url: '<?php echo $this->request->webroot ?>nothiMasters/potroListAll/<?php echo $part_id; ?>/<?= $nothi_office ?>',
			dataType: 'JSON',
			success: function (response) {
				$('#potroview_accordian .potroview .potroview_div').html(response.html);

				Metronic.initSlimScroll('.scroller');
				$('[data-title-orginal]').tooltip({'placement':'bottom'});

				$('li').tooltip({'placement':'bottom'});
			}
		});

	}

	function potroforAccordian(id, src) {

		if (id != 0 || id != '') {

			$(' #' + src + ' .panel-body').html('<img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp;লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');
			$.ajax({
				url: '<?php echo $this->request->webroot ?>nothiMasters/attachmentsForPotroAll/<?php echo $part_id; ?>/' + id + '/' +<?=$nothi_office ?>,
				dataType: 'JSON',
				success: function (response) {
					$('#' + src + ' .panel-body').html(response.html);
					if (response.html == '') {

					}
					$('[data-title-orginal]').tooltip({'placement':'bottom'});

					$('li').tooltip({'placement':'bottom'});
				}
			});
		}
	}

	function gotoSelectedPotors(selected) {

		if (selected.length != 0) {

			$('.selectedpotrogroup .potroview').hide();
			$('.selectedpotroview').show();

			$('#potroview_accordian .selectedpotroview .potroselected_view').html('<img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp;লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');

			$('#potroview_accordian .selectedpotroview .potroselected_view').html(selected);

			PortletDraggable.init();

		}
	}

	$(document).on('click', '.btn-printsummarydraft', function () {
		var id = $(this).data('id')
		showPdf('সার-সংক্ষেপ প্রিন্ট প্রিভিউ', '<?= $this->Url->build(['controller' => 'Potrojari', 'action' => 'getPdfById']) ?>/' + id + '/<?= $nothi_office ?>');
	});

	$(document).on('click', '.btn-summaryprintdraft', function () {
		var id = $(this).data('id')
		showPdf('সার-সংক্ষেপ প্রিন্ট প্রিভিউ', '<?= $this->Url->build(['controller' => 'Potrojari', 'action' => 'getPdfByPotro']) ?>/' + id + '/<?= $nothi_office ?>');
	});

	$(document).on('click', '.btn-printdraft', function () {
            var id = $(this).data('id');
            var potro_type = !isEmpty($(this).data('potro-type'))?$(this).data('potro-type'):0;
            console.log(potro_type);
            if(potro_type == 30){
                if($('.potroview').find('.DraftattachmentsRecord.active').find('.tab-pane.active').hasClass("csCover")){
                    showPdf('পত্র প্রিন্ট প্রিভিউ', '<?= $this->Url->build(['controller' => 'Potrojari', 'action' => 'getPdfById']) ?>/' + id + '/<?= $nothi_office ?>'+'?show=cover');
                }else{
                    showPdf('পত্র প্রিন্ট প্রিভিউ', '<?= $this->Url->build(['controller' => 'Potrojari', 'action' => 'getPdfById']) ?>/' + id + '/<?= $nothi_office ?>'+'?show=body');
                }
            }else{
                showPdf('পত্র প্রিন্ট প্রিভিউ', '<?= $this->Url->build(['controller' => 'Potrojari', 'action' => 'getPdfById']) ?>/' + id + '/<?= $nothi_office ?>');
            }
            $(".btn-pdf-margin").click();
	});

	$(document).on('click', '.btn-printpopPotro', function () {
		var node = $(this).closest('div.active').find('.templateWrapper')
		showPrintView(node);
	});
	$(document).on('click', '.btn-print', function () {
		var id = $(this).data('id')
		showPdf('পত্র প্রিন্ট প্রিভিউ', '<?= $this->Url->build(['controller' => 'Potrojari', 'action' => 'getPdfByPotro']) ?>/' + id + '/<?= $nothi_office ?>')
	});

	$('.potroSearchButton').click(function () {
		$('.potroview_tab li').removeClass('active');
		$('.loadsokolpotro').removeClass('loaded').addClass('active');
		$('.loadsokolpotro a').text('অনুসন্ধানকৃত পত্র');
		$('.tab-pane').removeClass('active');
		$('#potroview_list').addClass('active');
		$('.loadsokolpotro').trigger('click');
		setTimeout(function () {
			searchPotro(1);
		}, 500);
	})
	$('.potroSearchResetButton').click(function () {
		$('.potroSearch').val('');
		$('.potroview_tab li').removeClass('active');
		$('.loadsokolpotro').removeClass('loaded').addClass('active');
		$('.tab-pane').removeClass('active');
		$('#potroview_list').addClass('active');
		$('.loadsokolpotro a').text('সকল পত্র');
		$('.loadsokolpotro').trigger('click');
		setTimeout(function () {
			searchPotro(0);
		}, 500);
	})

	function searchPotro(type) {
		$.ajax({
			url: '<?= $this->Url->build(['controller' => 'archive', 'action' => 'nextAllPotroPage', $part_id, 1,]) ?>/' + type,
			dataType: 'JSON', type: 'post',
			data: {nothi_office:<?php echo $nothi_office ?>, q: $('.potroSearch').val()},
			success: function (response) {

				if (response.html == '') {
					$('.imageNext').addClass('disabled');
					$('.imagePrev').addClass('disabled');
					$('.potroNo').addClass('disabled').val(0);
					$('#potroview_list .potroview').html('<p class="text-danger text-center">পত্র খুজে পাওয়া যায়নি।</p>');
				} else {
					$('#potroview_list .potroview').html(response.html);
					getpotropage(response.potrono);
					$('.imageNext').removeClass('disabled');
				}

			}

		});
	}

</script>
