<style>
    .chart {
        width		: 100%;
        height		: 350px;
        font-size	: 11px;
    }
</style>

<div class="portlet light ">
    <div class="portlet-title">
        <div class="caption">
            নথি গতিবিধি
        </div>
    </div>
    <div class="portlet-body " id="datewiseowndesknothi"  style="height: auto; min-height: 350px">
    </div>
</div>

<!-- BEGIN PAGE LEVEL PLUGINS -->

<script src="<?php echo CDN_PATH ?>assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
<script src="<?php echo CDN_PATH ?>assets/global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
<script src="<?php echo CDN_PATH ?>assets/global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
<script src="<?php echo CDN_PATH ?>assets/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<script>
    var datewiseowndesknothi = function() {
        var chart = AmCharts.makeChart("datewiseowndesknothi", {
            "type": "serial",
            "theme": "light",

            "fontFamily": 'Open Sans',
            "color":    '#888888',

            "legend": {
                "equalWidths": false,
                "useGraphSettings": true,
                "valueAlign": "left",
                "valueWidth": 120
            },
            "valueAxes": [{
                "gridAlpha": 0,
                "axisAlpha": 0,
                "position": "left"
            }],
            "graphs": [{
                "id": "g1",
                "balloonText": "<div style='margin:10px; text-align:left'><span style='font-size:11px'>মোট প্রেরিত: [[prerito_nothi_bn]]</span></div>",
                "bullet": "round",
                "bulletBorderAlpha": 1,
                "useLineColorForBulletBorder": true,
                "bulletColor": "#FFFFFF",
                "dashLengthField": "dashLength",
                "legendValueText": "[[value]]",
                "title": "প্রেরিত",
                "fillAlphas": 0,
                "valueField": "prerito_nothi"
            }, {
                "balloonText": "<div style='margin:10px; text-align:left'><span style='font-size:11px'>মোট আগত: [[grihito_nothi_bn]]</span></div>",
                "bullet": "square",
                "bulletBorderAlpha": 1,
                "bulletBorderThickness": 1,
                "dashLengthField": "dashLength",
                "legendValueText": "[[value]]",
                "title": "আগত",
                "fillAlphas": 0,
                "valueField": "grihito_nothi"
            }],
            "chartScrollbar": {
                "graph": "g1",
                "oppositeAxis":false,
                "offset":20,
                "scrollbarHeight": 40,
                "backgroundAlpha": 0,
                "selectedBackgroundAlpha": 0.1,
                "selectedBackgroundColor": "#8888",
                "graphFillAlpha": 0,
                "graphLineAlpha": 0.5,
                "selectedGraphFillAlpha": 0,
                "selectedGraphLineAlpha": 1,
                "autoGridCount":true,
                "color":"#AAAAAA"
            },
            "chartCursor": {
                "categoryBalloonDateFormat": "DD",
                "cursorAlpha": 0.1,
                "cursorColor": "#000000",
                "fullWidth": true,
                "valueBalloonsEnabled": true,
                "zoomable": true
            }, "valueScrollbar":{
                "oppositeAxis":false,
                "offset":50,
                "scrollbarHeight":10
            },
            "dataDateFormat": "YYYY-MM-DD",
            "categoryField": "date",
            "categoryAxis": {
                "dateFormats": [{
                    "period": "DD",
                    "format": "DD"
                }, {
                    "period": "WW",
                    "format": "MMM DD"
                }, {
                    "period": "MM",
                    "format": "MMM"
                }, {
                    "period": "YYYY",
                    "format": "YYYY"
                }],
                "parseDates": true,
                "autoGridCount": true,
                "axisColor": "#555555",
                "gridAlpha": 0.1,
                "gridColor": "#FFFFFF"
            },
            "pathToImages": "http://cdn.amcharts.com/lib/3/images/",
            "exportConfig": {
                "menuBottom": "20px",
                "menuRight": "22px",
                "menuItems": [{
                    "icon": Metronic.getGlobalPluginsPath() + "amcharts/amcharts/images/export.png",
                    "format": 'png'
                }]
            },

            "dataProvider": <?php echo $resultArray ?>
        });

    }


    $(function () {
        datewiseowndesknothi();
    });
</script>