<style>
    .chart {
        width		: 100%;
        height		: 350px;
        font-size	: 11px;
    }
</style>

<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            ডাক গতিবিধি
        </div>
    </div>
    <div class="portlet-body " id="datewiseowndeskdak"  style="height: auto; min-height: 350px">
    </div>
</div>

<!-- BEGIN PAGE LEVEL PLUGINS -->

<script src="<?php echo CDN_PATH ?>assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
<script src="<?php echo CDN_PATH ?>assets/global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
<script src="<?php echo CDN_PATH ?>assets/global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
<script src="<?php echo CDN_PATH ?>assets/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<script>
    var datewiseowndeskdak = function() {
        var chart = AmCharts.makeChart("datewiseowndeskdak", {
            "type": "serial",
            "theme": "light",
            "fontFamily": 'Nikosh',
            "color":    '#888888',

            "legend": {
                "equalWidths": false,
                "useGraphSettings": true,
                "valueAlign": "left",
                "valueWidth": 80
            },
            "valueAxes": [{
                "gridAlpha": 0,
                "axisAlpha": 0,
                "position": "left"
            }],
            "graphs": [{
                "id": "g1",
                "balloonText": "<div style='margin:10px; text-align:left'><span style='font-size:11px'>নাগরিক ডাক: [[prerito_nagorik_dak]]</span><br><span style='font-size:11px'>দাপ্তরিক ডাক: [[prerito_daptorik_dak]]</span></div>",
                "bullet": "round",
                "bulletBorderAlpha": 1,
                "useLineColorForBulletBorder": true,
                "bulletColor": "#FFFFFF",
                "dashLengthField": "dashLength",
                "legendValueText": "[[value]]",
                "title": "প্রেরিত ",
                "fillAlphas": 0,
                "valueField": "prerito_dak"
            }, {
                "balloonText": "<div style='margin:10px; text-align:left'><span style='font-size:11px'>নাগরিক ডাক: [[grihito_nagorik_dak]]</span><br><span style='font-size:11px'>দাপ্তরিক ডাক: [[grihito_daptorik_dak]]</span></div>",
                "bullet": "square",
                "bulletBorderAlpha": 1,
                "bulletBorderThickness": 1,
                "dashLengthField": "dashLength",
                "legendValueText": "[[value]]",
                "title": "আগত",
                "fillAlphas": 0,
                "valueField": "grihito_dak"
            }, {
                "balloonText": "<div style='margin:10px; text-align:left'><span style='font-size:11px'>নাগরিক ডাক: [[nothi_vukto_nagorik_dak]]</span><br><span  style='font-size:11px'>দাপ্তরিক ডাক: [[nothi_vukto_daptorik_dak]]</span></div>",
                "bullet": "round",
                "bulletBorderAlpha": 1,
                "bulletBorderThickness": 1,
                "useLineColorForBulletBorder": true,
                "bulletColor": "#FFFFFF",
                "dashLengthField": "dashLength",
                "legendValueText": "[[value]]",
                "title": "নথিতে উপস্থাপিত",
                "fillAlphas": 0,
                "valueField": "nothi_vukto"
            }],
            "chartScrollbar": {
                "graph": "g1",
                "oppositeAxis":false,
                "offset":20,
                "scrollbarHeight": 40,
                "backgroundAlpha": 0,
                "selectedBackgroundAlpha": 0.1,
                "selectedBackgroundColor": "#8888",
                "graphFillAlpha": 0,
                "graphLineAlpha": 0.5,
                "selectedGraphFillAlpha": 0,
                "selectedGraphLineAlpha": 1,
                "autoGridCount":true,
                "color":"#AAAAAA"
            },
            "chartCursor": {
                "categoryBalloonDateFormat": "DD",
                "cursorAlpha": 0.1,
                "cursorColor": "#000000",
                "fullWidth": true,
                "valueBalloonsEnabled": true,
                "zoomable": true
            }, "valueScrollbar":{
                "oppositeAxis":false,
                "offset":50,
                "scrollbarHeight":10
            },
            "dataDateFormat": "YYYY-MM-DD",
            "categoryField": "date",
            "categoryAxis": {
                "dateFormats": [{
                    "period": "DD",
                    "format": "DD"
                }, {
                    "period": "WW",
                    "format": "MMM DD"
                }, {
                    "period": "MM",
                    "format": "MMM"
                }, {
                    "period": "YYYY",
                    "format": "YYYY"
                }],
                "parseDates": true,
                "autoGridCount": true,
                "axisColor": "#555555",
                "gridAlpha": 0.1,
                "gridColor": "#FFFFFF"
            },
            "pathToImages": "http://cdn.amcharts.com/lib/3/images/",
            "exportConfig": {
                "menuBottom": "20px",
                "menuRight": "22px",
                "menuItems": [{
                    "icon": Metronic.getGlobalPluginsPath() + "amcharts/amcharts/images/export.png",
                    "format": 'png'
                }]
            },

            "dataProvider": <?php echo $resultArray ?>
        });

        $('#chart_2').closest('.portlet').find('.fullscreen').click(function() {
            chart.invalidateSize();
        });

    }


    $(function () {
        datewiseowndeskdak();
    });
</script>