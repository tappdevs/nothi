<style>
    .chart {
        width		: 100%;
        height		: 250px;
        font-size	: 11px;
    }					
</style>

<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            ডাক সারমর্ম
        </div>
    </div>
    <div class="portlet-body " id="owndeskdak"  style="height: auto; min-height: 250px">           
    </div>
</div>

<!-- BEGIN PAGE LEVEL PLUGINS -->

<script src="<?php echo CDN_PATH ?>assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
<script src="<?php echo CDN_PATH ?>assets/global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
<script src="<?php echo CDN_PATH ?>assets/global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
<script src="<?php echo CDN_PATH ?>assets/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<script>
    var initOwnDeskDakSummary = function () {
        var chart = AmCharts.makeChart("owndeskdak", {
            "type": "pie",
            "theme": "light",
            "fontFamily": 'Open Sans',
            "fontSize": "12px",
            "marginBottom": 2,
            "marginTop": 2,
            "labelRadius": 5,
            "color": '#000',
            "dataProvider": <?php echo $resultArray; ?>,
            "valueField": "value",
            "titleField": "label",
            "balloonText": "[[title]]<br><span style='font-size:13px'><b>[[ban]]</b></span>",
            "labelText": "[[title]] : [[total]]"
        });
    }


    $(function () {
        initOwnDeskDakSummary();
    });
</script>