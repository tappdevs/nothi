<div class="row">

    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        
        <div class="dashboard-stat green-haze">
                <div class="visual">
                        <i class="fa fa-envelope"></i>
                </div>
                <div class="details">
                        <div class="number" <?php if($api==0){ ?> onclick="TOB_BAR_MANAGER.loadModuleWiseMenu('5')" <?php } ?>>
                                 <?php echo Cake\I18n\Number::format($totalInboxDak) ?> টি 
                        </div>
                        <div class="desc" >
                            <small>আগত</small>&nbsp;ডাক
                        </div>
                </div>
             <?php if($api==0){ ?><a class="more" href="javascript:;" onclick="TOB_BAR_MANAGER.loadModuleWiseMenu('5')">
            বিস্তারিত <i class="m-icon-swapright m-icon-white"></i>
            </a> <?php } ?>
        </div>
        
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        
        <div class="dashboard-stat green-haze">
                <div class="visual">
                        <i class="fa fa-envelope"></i>
                </div>
                <div class="details">
                        <div class="number" <?php if($api==0){ ?> onclick="TOB_BAR_MANAGER.loadModuleWiseMenu('5')" <?php } ?>>
                                 <?php echo Cake\I18n\Number::format($totalOutboxDak) ?> টি 
                        </div>
                        <div class="desc" >
                            <small>প্রেরিত , নথিতে উপস্থাপিত, নথিজাত</small>&nbsp;ডাক
                        </div>
                </div>
            <?php if($api==0){ ?> <a class="more" href="javascript:;" onclick="TOB_BAR_MANAGER.loadModuleWiseMenu('5')">
            বিস্তারিত <i class="m-icon-swapright m-icon-white"></i>
            </a> <?php } ?>
        </div>
        
    </div>

    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        
        <div class="dashboard-stat purple-plum">
                <div class="visual">
                        <i class="fa fa-book"></i>
                </div>
                <div class="details">
                        <div class="number" <?php if($api==0){ ?> onclick="TOB_BAR_MANAGER.loadModuleWiseMenu('4')" <?php } ?>>
                                 <?php echo Cake\I18n\Number::format($totalInboxNothi) ?> টি 
                        </div>
                        <div class="desc" >
                            <small>আগত</small>&nbsp;নথি
                        </div>
                </div>
             <?php if($api==0){ ?><a class="more" href="javascript:;" onclick="TOB_BAR_MANAGER.loadModuleWiseMenu('4')" >
            বিস্তারিত <i class="m-icon-swapright m-icon-white"></i>
            </a><?php } ?>
        </div>
        
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat purple-plum">
                <div class="visual">
                        <i class="fa fa-book"></i>
                </div>
                <div class="details">
                        <div class="number" <?php if($api==0){ ?> onclick="TOB_BAR_MANAGER.loadModuleWiseMenu('4')" <?php } ?>>
                                 <?php echo Cake\I18n\Number::format($totalOutboxNothi) ?> টি 
                        </div>
                        <div class="desc" >
                            <small>প্রেরিত</small>&nbsp;নথি
                        </div>
                </div>
             <?php if($api==0){ ?><a class="more" href="javascript:;" onclick="TOB_BAR_MANAGER.loadModuleWiseMenu('4')">
            বিস্তারিত <i class="m-icon-swapright m-icon-white"></i>
            </a> <?php } ?>
        </div>
    </div>
    
</div>
