<style>
    .chart {
        width		: 100%;
        height		: 350px;
        font-size	: 11px;
    }
</style>

<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            প্রেরিত ডাকের বর্তমান অবস্থা
        </div>
    </div>
    <div class="portlet-body " id="datewisesentdesk"  style="height: auto; min-height: 350px">
    </div>
</div>

<!-- BEGIN PAGE LEVEL PLUGINS -->

<script src="<?php echo CDN_PATH ?>assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
<script src="<?php echo CDN_PATH ?>assets/global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
<script src="<?php echo CDN_PATH ?>assets/global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
<script src="<?php echo CDN_PATH ?>assets/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<script>
    var datewisesentdesk = function() {
        var chart = AmCharts.makeChart("datewisesentdesk", {
            "type": "serial",
            "theme": "light",

            "fontFamily": 'Open Sans',
            "color":    '#888888',

            "legend": {
                "equalWidths": true,
                "useGraphSettings": true,
                "valueAlign": "left",
                "valueWidth": 120
            },
            "valueAxes": [{
                "gridAlpha": 0,
                "axisAlpha": 0,
                "position": "left"
            }],
            "graphs": [{
                "id": "g1",
                "balloonText": "<div style='margin:10px; text-align:left'><span style='font-size:11px'>মোট প্রেরিত: [[received_bn]] টি</span></div>",
                "bullet": "round",
                "bulletBorderAlpha": 1,
                "useLineColorForBulletBorder": true,
                "bulletColor": "#FFFFFF",
                "dashLengthField": "dashLength",
                "legendValueText": "[[received_bn]]",
                "title": "প্রেরণ করা হয়েছিলো ",
                "fillAlphas": 0,
                "valueField": "received"
            }, {
                "balloonText": "<div style='margin:10px; text-align:left'><span style='font-size:11px'>কার্যক্রম নিয়েছেন: [[out_bn]] টি</span></div>",
                "bullet": "square",
                "bulletBorderAlpha": 1,
                "bulletBorderThickness": 1,
                "dashLengthField": "dashLength",
                "legendValueText": "[[out_bn]]",
                "title": "কার্যক্রম নিয়েছেন",
                "fillAlphas": 0,
                "valueField": "out"
            }],
            "chartCursor": {
                "categoryBalloonDateFormat": "DD",
                "cursorAlpha": 0.1,
                "cursorColor": "#000000",
                "fullWidth": true,
                "valueBalloonsEnabled": true,                
                "zoomable": true
            }, 
            "categoryField": "designation_full",
            "categoryAxis": {
                "autoGridCount": true,
                "axisColor": "#555555",
                "gridAlpha": 0.1,
                "gridColor": "#FFFFFF",
                "autoWrap" : true
            },
            "exportConfig": {
                "menuBottom": "20px",
                "menuRight": "22px",
                "menuItems": [{
                    "icon": Metronic.getGlobalPluginsPath() + "amcharts/amcharts/images/export.png",
                    "format": 'png'
                }]
            },

            "dataProvider": <?php echo $resultArray ?>
        });


    }


    $(function () {
        datewisesentdesk();
    });
</script>