<div class="row dak-recepiant-seal " style="width:100%;">
    <div class="col-md-8 col-lg-8 col-sm-8">
        <?php echo $to_cell = $this->cell('OfficeSeal', ['params' => $seal_section]) ?>
    </div>
    <div class="col-md-4 col-lg-4 col-sm-4">
        <table class="table table-stripped table-bordered">
            <tbody>
                <tr>
                    <?php if ($decisionbox == 1) { ?>
                        <td class="form-group">
                            <div class="col-md-12 col-sm-12 col-lg-12">
                                <div class="row">
                                    <div class="pull-left">
                                        <label class="control-label font-lg font-purple">&nbsp; <?php echo __(SHIDDHANTO_DIN) ?></label>
                                    </div>
                                    <div class="pull-right">
                                        <button class="btn btn-xs btn-primary round-corner-5" onclick="NewDakAction();" > নতুন সিদ্ধান্ত&nbsp;</button>
                                    </div>
                                </div>
                                <hr style="margin-top: 2px;margin-bottom:2px;" class="bg-blue-madison">
                                <div class="row">
                                    <div class="col-md-2 col-sm-2 col-lg-2 text-center" >
                                        <input class="dak_actions_radio " value="1" type="radio" name="dak_actions_radio" data-title="নথিতে উপস্থাপন করুন" >
                                    </div>
                                    <div class="col-md-10 col-sm-10 col-lg-10 " >
                                        <label class=""> নথিতে উপস্থাপন করুন</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2 col-sm-2 col-lg-2 text-center " >
                                        <input class="dak_actions_radio" value="2" type="radio"  name="dak_actions_radio" data-title="নথিজাত করুন">
                                    </div>
                                    <div class="col-md-10 col-sm-10 col-lg-10 " >
                                        <label class=""> নথিজাত করুন</label>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-2 col-sm-2 col-lg-2 text-center" >
                                        <input class="dak_actions_radio" value="0" type="radio"  name="dak_actions_radio" data-title="ডিফল্ট সিদ্ধান্তসমূহ">
                                    </div>
                                    <div class="col-md-10 col-sm-10 col-lg-10 " >
                                        ডিফল্ট সিদ্ধান্তসমূহ&nbsp;<i class="fa fa-pencil-square-o" aria-hidden="true" onclick="clickModal();" ></i>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-lg-12 text-center">
                                <br>
                                <div class="input-group">
                                    <input type="text" id="dak-actions"  name="dak_actions"  class="form-control dakactiontext" value="<?php echo $decision; ?>" placeholder="সিদ্ধান্ত লিখুন">
                                </div>

                            </div>
                            <!--<div class="col-md-3 col-sm-3 col-lg-3">-->

                            <!--</div>-->

                        </td>
                    <?php } ?>

                </tr>
                <tr>
                    <td>
                        <label class="control-label"> <?php echo __("অগ্রাধিকার") ?> </label>
                        <?php
                        $dak_priority_levels = json_decode(DAK_PRIORITY_TYPE, true);
                        $dak_priority_levels[0] = 'অগ্রাধিকার বাছাই করুন';
                        echo $this->Form->input('dak_priority_level',
                            array('type' => 'select', 'label' => false, 'class' => 'form-control', 'options' => $dak_priority_levels,
                            'value' => (isset($priority) ? $priority : 0)));
                        ?>
                    </td>
                </tr>
                <tr>
                    <td >
	                    <div class="btn-group btn-group-round">
                        <?php if ($type == 'single') { ?>
                            <button class="btn btn-sm yellow forwardSingle" title="ডাক ফরোয়ার্ড করুন"><i class="a2i_nt_cholomandak2"></i>ফরোয়ার্ড করুন
                            </button>
                        <?php
                        }
                        else {
                            ?>
                            <button class="btn btn-sm yellow" id="forwardAllSelected" title="ডাক ফরোয়ার্ড করুন"><i class="a2i_nt_cholomandak2"></i>
                                ফরোয়ার্ড করুন
                            </button>
						<?php } ?>
	                    </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>

    <div class="col-md-12 col-lg-12">
        <form id="decision_form">
            <input type="hidden" name="to_officer_id" value="">
            <input type="hidden" name="to_officer_name" value="">
            <input type="hidden" name="to_officer_level" value="">
        </form>
    </div>
</div>

<script>
    $(document).find('[title]').tooltip({'placement':'bottom'});
    $(document).find('.checker').removeAttr('class')
</script>