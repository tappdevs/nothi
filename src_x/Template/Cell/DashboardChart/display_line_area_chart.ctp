<div id="chart_<?=$chart_no?>" class="chart" style="height: 400px;">
</div>
<?php
	$data = array();
	$dataF = array();
	foreach ($daptorik_items as $daptorik_item) {
		//echo $daptorik_item->created . " - " . $daptorik_item->count . "<br />";
		$dataX["date"] = $daptorik_item->created;
		$dataX["countDD"] = $daptorik_item->count;
		$dataX["countNG"] = 0;
		$dataX["total"] = $daptorik_item->count + 0;
		$data[] = $dataX;
	}
	
	/* foreach ($data as $dt) {
		echo $dt["date"] . " - " . $dt["countDD"] . " - " . $dt['countNG'] . "<br />";
	} */
	
	foreach ($nagorik_items as $nagorik_item) {
		$was_updated = false;
		foreach ($data as &$dt) {
			//echo $nagorik_item->created . " - " . $dt["date"] . "<br />";
			if ($nagorik_item->created == $dt["date"]) {
				$dt["countNG"] = $nagorik_item->count;
				$dt["total"] += $nagorik_item->count;
				$was_updated = true;
				break;
			}
		}
		if (!$was_updated) {
			$dataY["date"] = $nagorik_item->created;
			$dataY["countDD"] = 0;
			$dataY["countNG"] = $nagorik_item->count;
			$dataY["total"] = 0 + $nagorik_item->count;
			$data[] = $dataY;
		}
	}
	/* pr($data);
	
	foreach ($data as $x) {
		echo $x["date"] . " - " . $x["countDD"] . " - " . $x['countNG'] . "<br />";
	} */
	
?>

<script type="text/javascript">
	var chart<?=$chart_no?> = AmCharts.makeChart("chart_<?=$chart_no?>", {
		"type": "serial",
		"theme": "light",

		"fontFamily": 'Open Sans',
		"color":    '#888888',

		"legend": {
			"equalWidths": false,
			"useGraphSettings": true,
			"valueAlign": "left",
			"valueWidth": 120
		},
		"dataProvider": <?php echo json_encode($data); ?>,
		"valueAxes": [{
			"id": "distanceAxis",
			"axisAlpha": 0,
			"gridAlpha": 0,
			"position": "left",
			"title": "সর্বমোট আগত ডাক"
		}, {
			"id": "latitudeAxis",
			"axisAlpha": 0,
			"gridAlpha": 0,
			"labelsEnabled": false,
			"position": "right"
		}, {
			"id": "durationAxis",
			"axisAlpha": 0,
			"gridAlpha": 0,
			"inside": true,
			"position": "right",
			"labelsEnabled": false
		}],
		"graphs": [{
			"alphaField": "alpha",
			"balloonText": "মোট আগত ডাক: [[value]]",
			"dashLengthField": "dashLength",
			"fillAlphas": 0.7,
			"legendPeriodValueText": "মোট আগত ডাক: [[value.sum]]",
			"legendValueText": "মোট আগত ডাক[[value]]",
			"title": "সর্বমোট আগত ডাক",
			"type": "column",
			"valueField": "total",
			"valueAxis": "distanceAxis"
		}, {
			"balloonText": "দাপ্তরিক ডাক: [[value]]",
			"bullet": "round",
			"bulletBorderAlpha": 1,
			"useLineColorForBulletBorder": true,
			"bulletColor": "#FFFFFF",
			"bulletSizeField": "townSize",
			"labelPosition": "right",
			"legendValueText": "[[value]]",
			"title": "আগত দাপ্তরিক ডাক",
			"fillAlphas": 0,
			"valueField": "countDD",
			"valueAxis": "latitudeAxis"
		}, {
			"balloonText": "নাগরিক ডাক: [[value]]",
			"bullet": "square",
			"bulletBorderAlpha": 1,
			"bulletBorderThickness": 1,
			"legendValueText": "[[value]]",
			"title": "আগত নাগরিক ডাক",
			"fillAlphas": 0,
			"valueField": "countNG",
			"valueAxis": "durationAxis"
		}],
		"chartCursor": {
			"categoryBalloonDateFormat": "DD",
			"cursorAlpha": 0.1,
			"cursorColor": "#000000",
			"fullWidth": true,
			"valueBalloonsEnabled": false,
			"zoomable": false
		},
		"dataDateFormat": "YYYY-MM-DD",
		"categoryField": "date",
		"categoryAxis": {
			"dateFormats": [{
				"period": "DD",
				"format": "DD"
			}, {
				"period": "WW",
				"format": "MMM DD"
			}, {
				"period": "MM",
				"format": "MMM"
			}, {
				"period": "YYYY",
				"format": "YYYY"
			}],
			"parseDates": true,
			"autoGridCount": false,
			"axisColor": "#555555",
			"gridAlpha": 0.1,
			"gridColor": "#FFFFFF",
			"gridCount": 50
		},
		"exportConfig": {
			"menuBottom": "20px",
			"menuRight": "22px",
			"menuItems": [{
				"icon": js_wb_root + "webroot/assets/global/plugins/amcharts/amcharts/images/export.png",
				"format": 'png'
			}]
		}
	});
	
	$('#chart_<?=$chart_no?>').closest('.portlet').find('.fullscreen').click(function() {
		chart<?=$chart_no?>.invalidateSize();
	});
	
	$(document).ready(function(e){
		/* chart<?=$chart_no?>.addListener("rendered", function(e){
			$("#chart_<?=$chart_no?>").parent().hide();
		});
		chart<?=$chart_no?>.invalidateSize(); */
	});
	
</script>