<div class="row">
	<div class="col-md-4">
		<label> ব্যবধান বাছাই করুন </label>
	</div>
	<div class="col-md-8">
		<select id="number_of_days" class="form-control">
			<option value="0">-- বাছাই করুন  --</option>
			<option value="7">পূর্ববর্তী ৭ দিন</option>
			<option value="15">পূর্ববর্তী ১৫ দিন </option>
			<option value="30">পূর্ববর্তী ৩০ দিন </option>
			<option value="31">১ মাসের অধিক</option>
		</select>
	</div>
</div>
<div id="chart_<?=$chart_no?>" class="chart" style="height: 400px;">
</div>

<?php
	/*
		 [Shakhar naam] = "";
		 [Shakhar pending item] = "";
	*/
?>

<script type="text/javascript">
	var chart<?=$chart_no?> = AmCharts.makeChart("chart_<?=$chart_no?>", {
		"type": "radar",
		"theme": "light",

		"fontFamily": 'Open Sans',
		
		"color":    '#888',

		"dataProvider": [{
			"country": "Czech Republic",
			"litres": 156.9
		}, {
			"country": "Ireland",
			"litres": 131.1
		}, {
			"country": "Germany",
			"litres": 115.8
		}, {
			"country": "Australia",
			"litres": 109.9
		}],
		"valueAxes": [{
			"axisTitleOffset": 20,
			"minimum": 0,
			"axisAlpha": 0.15
		}],
		"startDuration": 2,
		"graphs": [{
			"balloonText": "[[value]] litres of beer per year",
			"bullet": "round",
			"valueField": "litres"
		}],
		"categoryField": "country",
		"exportConfig": {
			"menuTop": "10px",
			"menuRight": "10px",
			"menuItems": [{
				"icon": '/lib/3/images/export.png',
				"format": 'png'
			}]
		}
	});
	
	$('#chart_<?=$chart_no?>').closest('.portlet').find('.fullscreen').click(function() {
		chart<?=$chart_no?>.invalidateSize();
	});
	
	$(document).ready(function(e){	
		chart<?=$chart_no?>.addListener("rendered", function(e){
			$("#chart_<?=$chart_no?>").parent().hide();
		});
	});
	
</script>