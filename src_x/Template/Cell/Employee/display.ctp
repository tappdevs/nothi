<div class="row">
    <div class="col-md-12">
        <div class="portlet green-meadow box">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-cogs"></i>Office Employees</div>
                <div class="tools">
                    <a href="javascript:" class="collapse"></a>
                    <a href="#portlet-config" data-toggle="modal" class="config"></a>
                    <a href="javascript:;" class="reload"></a>
                    <a href="javascript:;" class="remove"></a>
                </div>
            </div>
            <div class="portlet-body form">
                <form class="form-horizontal">
                    <div class="form-body">
                        <div class="form-group">
                            <ul id="employee_list" style="list-style: none;">
                                <?php foreach ($office_employees as $key => $emp) { ?>
                                    <li style="padding:5px;"><input type="radio" name="office_employee"
                                                                    value="<?= $key ?>">&nbsp;&nbsp;<i
                                            class="fs1 a2i_gn_user1"></i><?= $emp ?></li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

