<div class="row">
    <div class="col-md-12">
        <p class="help-block text-primary text-center">
            *** <?= __("Search Employee by employee login ID and NID") ?> ***
        </p>

        <form class="alert alert-success alert-borderless" action="#">
            <div class="input-group">
                <div class="row">
                <div class="input-cont col-sm-5">
                    <?=
                    $this->Form->input('employee_identity_no',
                        ['list' => 'employee_identity_no_list', 'label' => __('Login ID'),
                        'class' => 'form-control', 'type' => 'text', 'placeholder' => 'লগইন আইডি বাছাই করুন']);
                    ?>
                    <datalist id="employee_identity_no_list">
                        <?php foreach ($employee_identity_nos as $identity_no) { ?>
                            <option value="<?php echo $identity_no ?>"></option>
<?php } ?>
                    </datalist>
                </div>

                <div class="col-sm-5">
                    <?php
                    echo $this->Form->input('national_id',
                        array(
                            'label' => __('NID'),
                            'class' => 'form-control', 'type' => 'text',
                            'placeholder'=>'জাতীয় পরিচয়পত্র'
                        ));
                    ?>
                </div>
                    <span class="input-group-btn">
                    <button class="btn green-haze" type="button"
                            id="btn_employee_search" style="margin-top: 25px">
                        <?=__("Search")?> &nbsp; <i class="m-icon-swapright m-icon-white"></i>
                    </button>
                </span>
                </div>
                
            </div>
        </form>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PORTLET -->
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption caption-md">
                    <i class="icon-bar-chart theme-font hide"></i>
                    <span
                        class="caption-subject font-blue-madison"><?= __("Personal")." ". __("Information")?></span>
                </div>
            </div>
            <div class="portlet-body">
                <p class="bold theme-font" id="p_name"></p>
<?php echo $this->Form->hidden('employee_record_id',
    array('id' => 'employee-record-id'));
?>

                <p class="bold theme-font" id="p_email"></p>

                <p class="bold theme-font" id="p_mobile"></p>

            </div>
        </div>
        <!-- END PORTLET -->
    </div>
    <div class="col-md-12">
        <!-- BEGIN PORTLET -->
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption caption-md">
                    <i class="icon-bar-chart theme-font hide"></i>
                    <span
                        class="caption-subject font-blue-madison"><?= __("Current")." ". __("Designations")?> </span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-scrollable table-scrollable-borderless">
                    <table class="table table-hover table-light">
                        <thead>
                            <tr class="uppercase">
                                <th>
                                    <?= __("Office") ?>
                                </th>
                                <th>
                                     <?= __("Designations") ?>
                                </th>
                                <th>
                                     <?= __("Actions") ?>
                                </th>

                            </tr>
                        </thead>
                        <tbody id="designation_table">

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- END PORTLET -->
    </div>
</div>
<div class="modal fade modal-purple" id="office_admin_selection_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="closed" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">অফিস এডমিন বাছাই করুন</h4>
            </div>
            <div class="modal-body">
                <div class="table-scrollable">
                    <table class="table table-striped table-bordered table-hover" id="sample_1">
                        <thead>
                        <tr>

                            <th class=""></th>
                            <th class="">ক্রমিক নং </th>
                            <th class=""><?php echo __('Name') ?></th>
                            <th class="">পদবি</th>
                            <th class="">শাখা</th>
                            <th class=""><?=  __('Username') ?></th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <input class="btn btn-md btn-primary" type="button" id="assignOfficeAdminAndReleaseCurrentUser" value="<?php echo __("Assign") ?>" />
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?=CDN_PATH?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script>
    function reloadPicker(){
        $('.date-picker').datepicker({
        //    rtl: Metronic.isRTL(),
            orientation: "left",
            autoclose: true,
            maxDate : 'now'
        });
    }
</script>
<script type="text/javascript">
    var addDatePicker = '<input class="date_release_date form-control form-control-inline input-sm date-picker" size="16" type="text" value="" placeholder="শেষ কার্য দিবস" data-date-end-date="0d">';
    var EmployeeManagement = {
        getEmployeeJobStatus: function () {
            $("#designation_table").html("");
            var identity_no = $("#employee-identity-no").val();
             var n_id =$("#national-id").val();
            PROJAPOTI.ajaxSubmitAsyncDataCallbackWithoutAbrot(js_wb_root + 'employeeRecords/getEmployeeJobStatus',
                    {'identity_no': identity_no,'n_id' : n_id}, 'json',
                    function (response) {
                        var personal_info = response.personal_info;
                        var office_records = response.office_records;
                        $("#p_name").text(personal_info.name_bng);
                        $("#p_email").text(personal_info.personal_email);
                        $("#p_mobile").text(personal_info.personal_mobile);
                        $("#employee-record-id").val(personal_info.id);

                        $.each(office_records, function (i) {
                            var record = office_records[i];
                            var str = '';
                            str += '<tr>';
                            str += '<td>';
                            str += '<span class="bold theme-font-color" data-office-id="'+record.office_id+'">' + record.office_name + '</span>';
                            str += '</td>';
                            str += '<td>';
                            str += '<span class="bold theme-font-color" data-organogram-id="'+record.office_unit_organogram_id+'">' + record.designation + (record.incharge_label != ''? (" (" + record.incharge_label + ")") : "") + ', ' + record.unit_name + '</span>';
                            str += '</td>';
                            if(parseInt(response.own_office) !== 0 && parseInt(response.own_office) !== parseInt(record.office_id)){
                                str += "<td></td>";
                            }else{
                                str += '<td><div>';
                                str += '<div class="pull-left">';
                                str += addDatePicker+'</div>&nbsp;&nbsp;';
                                if (response.current_office_unit_organogram_id == record.office_unit_organogram_id) {
                                    str += '<a id="admin_designation_anchor" data-record-id="'+record.id+'" data-office-id="'+record.office_id+'" class="btn btn-sm btn-danger" onclick="EmployeeManagement.removeAssignmentWithReleaseAdmin(this,' + record.id + ',' + record.office_id + ')"><i class=" fs1 a2i_gn_delete2"></i></a>';
                                } else {
                                    str += '<a id="" class="btn btn-sm btn-danger" onclick="EmployeeManagement.removeAssignment(this,' + record.id + ')"><i class=" fs1 a2i_gn_delete2"></i></a>';
                                }
                                str += '</div></td>';
                            }
                            str += '</tr>';
                            $("#designation_table").append(str);
                            reloadPicker();
                        });

                    });
        },
        removeAssignmentWithReleaseAdmin: function(that, record_id, office_id) {
            //office_admin_selection_modal
            if($(that).closest('div').find('.date_release_date').val()=='' || typeof($(that).closest('div').find('.date_release_date').val())=='undefined'){
                $(that).closest('div').find('.date_release_date').focus();
                toastr.error("শেষ কার্য দিবস দেওয়া হয়নি।");
                return;
            }
            PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + 'EmployeeRecords/emoloyeeDesignationByOffice',
                {'office_id': office_id, 'group': 'EmployeeOffices.office_unit_organogram_id'}, 'json',
                function (response) {
                    // Add response in Modal body
                    $('#office_admin_selection_modal').find('#sample_1').find('tbody').html('');
                    $.each(response, function(i, data){
                        $('#office_admin_selection_modal').find('#sample_1').find('tbody').append("<tr><td>"+ '<input ' +(data.is_admin==1?"checked=checked":'') + ' type="radio" name="office_admin" data-user-id="'+data.employee_office_id+'" class="office_admin" />' + "</td><td>"+BnFromEng(i+1)+"</td><td>"+data.name_bng+"</td><td>"+data.designation+"</td><td>"+data.unit_name_bng+"</td><td>"+data.username+"</td></tr>");
                    });
                    // Display Modal
                    $('#office_admin_selection_modal').modal('show');
                }
            );
        },
        removeAssignment: function (that, record_id) {
            var organogram_id = $(that).closest('tr').find('td').eq(1).find('span').attr('data-organogram-id');
            var office_id = $(that).closest('tr').find('td').eq(0).find('span').attr('data-office-id');
            $.ajax({
                url: js_wb_root + 'countUserActivities',
                type: 'POST',
                dataType: 'json',
                data: {designation_id: organogram_id},
                success: function (response) {
                    if ((response.dak + response.nothi) > 0) {
                        bootbox.dialog({
                            message: "<span style='color:red;font-weight:bold;'>আপনি কি এই পদবির সকল কার্যক্রম স্থানান্তর করে বর্তমান কর্মকর্তাকে অব্যাহতি করতে ইচ্ছুক?</span><br/><input type='radio' name='is_transfer' value='1' required onclick=$(this).closest('.modal-content').find('.modal-footer').find('.green').removeClass('hidden') /> হ্যাঁ <br/><input type='radio' name='is_transfer' value='0' required onclick=$(this).closest('.modal-content').find('.modal-footer').find('.green').removeClass('hidden') /> না",
                            title: "কার্যক্রম স্থানান্তর",
                            buttons: {
                                success: {
                                    label: "<i class='fa fa-forward'></i> পরবর্তী ধাপ",
                                    className: "green hidden",
                                    callback: function () {
                                        var is_yes = 0
	                                    if ($('[name=is_transfer]').is(':checked')) {
                                            is_yes = $('[name=is_transfer]:checked').val();
	                                    }

	                                    if (is_yes == 1) {
                                            var modalId = 'OrganogramSelectModal';
                                            var header = "যে পদবিতে সকল কার্যক্রম স্থানান্তর করতে চান তা নির্বাচন করুন";
                                            var content = '<div class="panel panel-primary">' +
                                                '<div class="panel-heading" style="color: #fff;">' +
                                                '<div class="row">' +
                                                '<div class="col-md-3 office_name">Office Employee List</div>' +
                                                '<div class="col-md-9 pull-right" style="display:none;">' +
                                                '<div class="input-group-round form-inline" style="text-align:left;">' +
                                                '<select name="new_organogram_id" placeholder="Hello" class="form-control text-left" style="width:60%;"></select>' +
                                                '<button class="btn btn-warning btn-md" data-office-id="'+office_id+'" onclick="deleteOrganogram(this, ' + organogram_id + ', \'' + $(that).closest('div').find('.date_release_date').val() + '\' , ' + record_id + ')" style="margin-left:-5px;">স্থানান্তর করুন</button>' +
                                                '</div>' +
                                                '</div>' +
                                                '</div>' +
                                                '</div>' +
                                                '<div class="panel-body" style="overflow:auto; height:calc(100vh - 180px);">' +
                                                '<div class="table-responsive">' +
                                                '<table class="table table-bordered table-striped table-hover">' +
                                                '<thead>' +
                                                '<tr>' +
                                                '<th>#</th>' +
                                                '<th>কর্মকর্তার নাম</th>' +
                                                '<th>পদের নাম</th>' +
                                                '<th>শাখার নাম</th>' +
                                                '<th>কার্যক্রম</th>' +
                                                '</tr>' +
                                                '</thead>' +
                                                '<tbody></tbody>' +
                                                '</table>' +
                                                '</div>' +
                                                '</div>' +
                                                '</div>';
                                            var submitBtn = '';
                                            doModal(modalId, header, content, submitBtn, 'height-auto');
                                            populateOrganogramByOfficeId(office_id, 'OrganogramSelectModal', organogram_id);
                                        } else {
                                            bootbox.dialog({
                                                message: "আপনি কি নিশ্চিতভাবে কর্মকর্তাকে অব্যাহতি করতে ইচ্ছুক?",
                                                title: "কর্মকর্তাকে অব্যাহতি",
                                                buttons: {
                                                    success: {
                                                        label: "হ্যাঁ",
                                                        className: "green",
                                                        callback: function () {
                                                            if($(that).closest('div').find('.date_release_date').val()=='' || typeof($(that).closest('div').find('.date_release_date').val())=='undefined'){
                                                                $(that).closest('div').find('.date_release_date').focus();
                                                                toastr.error("শেষ কার্য দিবস দেওয়া হয়নি।");
                                                                return;
                                                            }
                                                            PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + 'employeeRecords/removeAssignment',
                                                                {'id': record_id, 'last_release_date': $(that).closest('div').find('.date_release_date').val()}, 'json',
                                                                function (response) {
                                                                    if(response == 1){
                                                                        toastr.success("কর্মকর্তাকে অব্যাহতি দেওয়া হয়েছে");
                                                                    }else{
                                                                        toastr.error("কর্মকর্তাকে অব্যাহতি দেওয়া সম্ভব হয়নি।");
                                                                    }
                                                                    EmployeeManagement.getEmployeeJobStatus();
                                                                    EmployeeAssignment.getUnitOrganogramByOfficeId($("#office-id").val());
                                                                });
                                                        }
                                                    },
                                                    danger: {
                                                        label: "না",
                                                        className: "red",
                                                        callback: function () {
                                                        }
                                                    }
                                                }
                                            });
                                            changeBootBoxStyle();
	                                    }
                                    }
                                }
                            }
                        });
                    } else {
                        bootbox.dialog({
                            message: "আপনি কি নিশ্চিতভাবে কর্মকর্তাকে অব্যাহতি করতে ইচ্ছুক?",
                            title: "কর্মকর্তাকে অব্যাহতি",
                            buttons: {
                                success: {
                                    label: "হ্যাঁ",
                                    className: "green",
                                    callback: function () {
                                        if($(that).closest('div').find('.date_release_date').val()=='' || typeof($(that).closest('div').find('.date_release_date').val())=='undefined'){
                                            $(that).closest('div').find('.date_release_date').focus();
                                            toastr.error("শেষ কার্য দিবস দেওয়া হয়নি।");
                                            return;
                                        }
                                        PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + 'employeeRecords/removeAssignment',
                                            {'id': record_id, 'last_release_date': $(that).closest('div').find('.date_release_date').val()}, 'json',
                                            function (response) {
                                                if(response == 1){
                                                    toastr.success("কর্মকর্তাকে অব্যাহতি দেওয়া হয়েছে");
                                                }else{
                                                    toastr.error("কর্মকর্তাকে অব্যাহতি দেওয়া সম্ভব হয়নি।");
                                                }
                                                EmployeeManagement.getEmployeeJobStatus();
                                                EmployeeAssignment.getUnitOrganogramByOfficeId($("#office-id").val());
                                            });
                                    }
                                },
                                danger: {
                                    label: "না",
                                    className: "red",
                                    callback: function () {
                                    }
                                }
                            }
                        });
                    }
                }
            });
        }
    };

    function populateOrganogramByOfficeId(office_id, elementId, organogram_id) {
        $("#"+elementId).find(".panel").closest('.row').removeClass('hidden');
        $("#"+elementId).find(".panel").find('.panel-heading .office_name').html($("#office-id option:selected").text() + ' <i class="fa fa-cog fa-spin"></i> লোড হচ্ছে...');
        $("#"+elementId).find(".panel").find('.panel-body').hide();

        PROJAPOTI.ajaxSubmitAsyncDataCallbackWithoutAbrot(js_wb_root + 'showOfficeEmployeeList', {office_id: office_id}, 'json',
            function (response) {
                $("#"+elementId).find(".panel").find('tbody').html("");
                $("select[name=new_organogram_id]").parent().parent().hide();
                if (response.length != 0) {
                    $("select[name=new_organogram_id]").html("");
                    $("select[name=new_organogram_id]").append($('<option>', {
                        value: "",
                        text: "-- বাছাই করুন --"
                    }));
                    $.each(response, function (key, value) {
                        $("select[name=new_organogram_id]").append($('<option>', {
                            value: value.organogram_id,
                            text: value.employee_name + ', ' + value.designation_name + ', ' + value.unit_name
                        }));
                    });
                    $("select[name=new_organogram_id]").select2();
                    $("select[name=new_organogram_id]").parent().parent().show();

                    $('select[name=new_organogram_id] option[disabled]').attr('disabled', false);
                    $('select[name=new_organogram_id] option[value='+organogram_id+']').attr('disabled', true);

                    $("#"+elementId).find(".panel").find('.panel-heading .office_name').html('');
                } else {
                    $("#"+elementId).find(".panel").find('.panel-heading .office_name').html('কোনো তথ্য পাওয়া যায় নি');
                }
            }
        );
    }

    function deleteOrganogram(element, organogramId, date_release_date, record_id) {
        var btn = element;
        var officeId = $(btn).data('office-id');
        bootbox.dialog({
            message: "আপনি কি নিশ্চিত?",
            title: "কার্যক্রম স্থানান্তর",
            buttons: {
                success: {
                    label: "হ্যাঁ",
                    className: "green",
                    callback: function () {
                        if ($('select[name=new_organogram_id] option:selected').val() == 0) {
                            toastr.error("কোনো কর্মকর্তা বাছাই করা হয় নাই");
                            return false;
                        }
                        $(btn).html('<i class="fa fa-cog fa-spin"></i> পদবি স্থানান্তর হচ্ছে...');
                        $(btn).addClass('disabled');
                        $.ajax({
                            url: js_wb_root + 'designationDataTransfer',
                            type: 'GET',
                            dataType: 'json',
                            data: {
                                selectedDesignation: organogramId,
                                newDesignation: $('select[name=new_organogram_id] option:selected').val(),
                                office_id: officeId,
                            },
                            success: function (response) {
                                if (response.status == 'success') {
                                    PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + 'employeeRecords/removeAssignment',
                                        {'id': record_id, 'last_release_date': date_release_date}, 'json',
                                        function (response) {
                                            if(response == 1){
                                                toastr.success("কর্মকর্তাকে অব্যাহতি দেওয়া হয়েছে");
                                            }else{
                                                toastr.error("কর্মকর্তাকে অব্যাহতি দেওয়া সম্ভব হয়নি।");
                                            }
                                            EmployeeManagement.getEmployeeJobStatus();
                                            EmployeeAssignment.getUnitOrganogramByOfficeId($("#office-id").val());

                                            $(btn).closest('.modal').remove();
                                        });
                                } else {
                                    toastr.error('কার্যক্রম স্থানান্তর সম্ভব হচ্ছে না')
                                }
                            }
                        });
                    }
                },
                danger: {
                    label: "না",
                    className: "red",
                    callback: function () {
                    }
                }
            }
        });
    }

    $(function () {
        $("#btn_employee_search").bind('click', function () {
            var username = getusername();
            var n_id =$("#national-id").val();
            if ((username == '' || typeof (username) == 'undefined') && (n_id == '' || typeof(n_id) == 'undefined') ) {
                toastr.error("লগইন আইডি অথবা জাতীয় পরিচয়পত্র সঠিক নয়");
                return;
            }
            EmployeeManagement.getEmployeeJobStatus();
        });
        $("#employee-identity-no").keypress(function (e) {
            if (e.which == 13 || e.keyCode == 13) {
                e.preventDefault();
                var username = getusername();
                if (username == '' || typeof (username) == 'undefined') {
                    toastr.error("লগইন আইডি সঠিক নয়");
                    return;
                }
                EmployeeManagement.getEmployeeJobStatus(username);
            }
        });
        function getusername()
        {
            var identity_no = $("#employee-identity-no").val();
            if (identity_no == '') {
                return;
            }
            var len = identity_no.length;
            if (len > 0) {
                if (identity_no[0] == 0) {
                    return;
                }
            }
            if (len < 12) {
                var fake_id = identity_no;
                identity_no = '';
                identity_no += fake_id[0];
                var i = 0;
                var j = 0;
                var k = 0;
                for (i = 1; i <= 12 - len; i++) {
                    identity_no += '0';
                }

                for (j = i, k = 1; j + 1 <= 12; j++, k++) {
                    identity_no += fake_id[k];
                }
            }
            $("#employee-identity-no").val(identity_no);
            return identity_no;
        }

        $(document).on('click','#assignOfficeAdminAndReleaseCurrentUser',function(){
            var that = $("#admin_designation_anchor");
            var record_id = $("#admin_designation_anchor").attr('data-record-id');
            var done = 0;
            if($('.office_admin').length>0){
                bootbox.dialog({
                    message: "আপনি কি নিশ্চিতভাবে কর্মকর্তাকে অব্যাহতি করতে ইচ্ছুক?",
                    title: "কর্মকর্তাকে অব্যাহতি",
                    buttons: {
                        success: {
                            label: "হ্যাঁ",
                            className: "green",
                            callback: function () {
                                if($(that).closest('div').find('.date_release_date').val()=='' || typeof($(that).closest('div').find('.date_release_date').val())=='undefined'){
                                    $(that).closest('div').find('.date_release_date').focus();
                                    toastr.error("শেষ কার্য দিবস দেওয়া হয়নি।");
                                    return;
                                }
                                Metronic.blockUI({
                                    target: '#office_admin_selection_modal table',
                                    boxed: true
                                });
                                var requested_user_id = $('.office_admin:checked').attr('data-user-id');
                                if (requested_user_id == record_id) {
                                    Metronic.unblockUI('#office_admin_selection_modal table');
                                    toastr.error("আপনি ইতোমধ্যে এ্যাডমিন হিসেবে আছেন। দয়া করে অন্য ব্যবহারকারী নির্বাচন করুন।");
                                    return;
                                }
                                $.ajax({
                                    url: '<?php echo $this->Url->build(['controller'=>'OfficeManagement','action'=>'AssignOfficeAdmin']) ?>',
                                    data: {'requested_user_id':requested_user_id, 'office_id':$('#office-id').val()},
                                    cache: false,
                                    type: 'post',
                                    success: function(msg){
                                        toastr.success(msg);
                                        PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + 'employeeRecords/removeAssignment',
                                            {'id': record_id, 'last_release_date': $(that).closest('div').find('.date_release_date').val()}, 'json',
                                            function (response) {
                                                if(response == 1){
                                                    toastr.success("কর্মকর্তাকে অব্যাহতি দেওয়া হয়েছে");
                                                }else{
                                                    toastr.error("কর্মকর্তাকে অব্যাহতি দেওয়া সম্ভব হয়নি।");
                                                }
                                            }
                                        );
                                        window.location.replace(js_wb_root + 'logout');
                                    }
                                });
                            }
                        },
                        danger: {
                            label: "না",
                            className: "red",
                            callback: function () {
                            }
                        }
                    }
                });
            }
        });
    });
</script>