<div class="row">
    <div class="col-md-12">
        <p class="help-block text-danger  text-center">
            *** <?= __("Search Employee by employee login ID") ?> ***
        <hr>
        </p>

        <form class="alert alert-success alert-borderless">
            <div class="input-group">
                <div class="input-cont right">
                    <?= $this->Form->input('employee_identity_no',
                        ['list' => 'employee_identity_no_list', 'label' => false,
                        'class' => 'form-control', 'type' => 'text', 'placeholder' => '']); ?>
                    <datalist id="employee_identity_no_list">
                        <?php foreach ($employee_identity_nos as $identity_no) { ?>
                            <option value="<?php echo $identity_no ?>"></option>
<?php } ?>
                    </datalist>
                </div>
                <span class="input-group-btn">
                    <button class="btn green-haze" type="button" id="btn_employee_search">
<?= __("Search") ?> &nbsp; <i class="m-icon-swapright m-icon-white"></i>
                    </button>
                </span>

        </form>
    </div>
</div>
</div>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PORTLET -->
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption caption-md">
                    <i class="icon-bar-chart theme-font hide"></i>
                    <span
                        class="caption-subject font-blue-madison bold uppercase"><?= __("Personal")." ". __("Information") ?></span>
                </div>
            </div>
            <div class="portlet-body" >
                <div class="row" id="loding"></div>
                <div class="row">
                    <div class="col-md-3">
                        <ul class="list-unstyled profile-nav">
                            <li id="p_img">

                            </li>
                        </ul>
                    </div>
                    <div class="col-md-9">
                        <div class="row">
                            <div class="col-md-12 profile-info">
<?php echo $this->Form->hidden('employee_record_id',
    array('id' => 'employee-record-id')); ?>
                                <h1 id="p_name">&nbsp;</h1>
                                <ul class="list-inline">
                                    <li id="p_email">

                                    </li>
                                </ul>
                                <ul class="list-inline">
                                    <li id="p_mobile">

                                    </li>
                                </ul>
                            </div>
                            <!--end col-md-12-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PORTLET -->
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <ul class="timeline">

        </ul>
    </div>
</div>
<script type="text/javascript" src="<?= CDN_PATH ?>assets\global\plugins\jquery-easypiechart\jquery.easypiechart.min.js"></script>
<script type="text/javascript">

    var timeline_colors = ["timeline-red", "timeline-purple", "timeline-yellow", "timeline-blue", "timeline-grey"];
    var timeline_current = "timeline-green";
    var EmployeeManagement = {
        encodeImage: function (imgurl) {

            $.ajax({
                url: js_wb_root + 'EmployeeRecords/getUserProfilePhoto/' + imgurl + '/1',
                cache: true,
                type: 'post',
                async: false,
                success: function (img) {
                    $('#p_img').html('<img src="' + img + '" alt="profile" class="img-responsive"  />');
                }
            });
        },
        getEmployeeJobStatus: function (identity_no) {
            $("ul.timeline").html("");
            $("#p_email").html("");
            $("#p_mobile").html("");
            $("#p_name").html("");
            $("#loding").html("<img src='<?php echo CDN_PATH ?>assets/global/img/loading-spinner-grey.gif' alt='' class='img-responsive' />");
            PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + 'employeeRecords/getEmployeeRoleHistories',
                    {'identity_no': identity_no}, 'json',
                    function (response) {
                        $("#loding").html("");
                        var personal_info = response.personal_info;
                        var office_records = response.office_records;
                        $("#p_name").text(personal_info.name_bng);
                        $("#p_email").html('<i class="fa fa-envelope"></i> ' + personal_info.personal_email);
                        $("#p_mobile").html('<i class="fa fa-phone"></i> ' + personal_info.personal_mobile);
                        $("#employee-record-id").val(personal_info.id);

                        var total_records = office_records.length;

                        $.each(office_records, function (i) {
                            var font_size = "13px";
                            var record = office_records[i];
                            var str = '';

                            var bottom_node = "";
                            if (total_records == i + 1) {
                                bottom_node = "timeline-noline";
                            }

                            /* var jd = new Date(record.joining_date).toDateString();
                             var lod = new Date(record.last_office_date).toDateString(); */

                            // For multi-colored items
                            timeline_color_index = i;
                            if (i > 4) {
                                timeline_color_index %= i;
                            }
                            timeline_color = timeline_colors[timeline_color_index];

                            // For 2-colored items, green = current and yellow = old
                            //timeline_color = 5;
                            var lod = record.last_office_date;
                            if (record.last_office_date == null) {
                                lod = "বর্তমান";
                                timeline_color = timeline_current;
                            }
                            if (record.joining_date == null) {
                                record.joining_date = "জয়েন করার তারিখ পাওয়া যায় নি"
                            }

                            str += '<li class="' + timeline_color + ' ' + bottom_node + '">';
                            str += '<div class="timeline-time">';
                            str += '<span class="date">';
                            str += record.joining_date;
                            str += '</span>';

                            if (record.status == "1") {
                                str += '<span class="time">&nbsp; - &nbsp;';
                            } else {
                                str += '<span class="time" style="font-size:' + font_size + '">&nbsp; - &nbsp;';
                            }
                            str += lod;
                            str += '</span>';
                            str += '</div>';
                            str += '<div class="timeline-icon">';
                            //str+= '<i class="fa fa-trophy"></i>';
                            str += '</div>';
                            str += '<div class="timeline-body">';
                            str += '<h2>' + record.designation + ' (' + record.unit_name + ') - ' + record.office_name + '</h2>';
                            str += '<div class="timeline-content hide">';
                            //str+= 'The activity board goes here';
                            str += '<div class="row">';
                            str += '<div class="col-md-12 col-sm-12">';
                            str += '<div class="portlet light bordered">';
                            str += '<div class="portlet-title">';
                            str += '<div class="caption">';
                            str += '<i class="icon-cursor font-purple-intense"></i>';
                            str += '<span class="caption-subject font-purple-intense ">General Stats</span>';
                            str += '</div>';
                            str += '</div>';
                            str += '<div class="portlet-body">';
                            str += '<div class="row">';
                            str += '<div class="col-md-6">';
                            str += '<div class="easy-pie-chart">';
                            str += '<div data-percent="' + record.dak_count + '" class="number daks">';
                            str += '<span>';
                            str += record.dak_count + '%';
                            str += '</span>';
                            str += '<canvas height="75" width="75"></canvas></div>';
                            str += '<a href="#" class="title">';
                            str += '<i class="icon-paper-plane"></i> ডাক ';
                            str += '</a>';
                            str += '</div>';
                            str += '</div>';
                            str += '<div class="margin-bottom-10 visible-sm">';
                            str += '</div>';
//                            str += '<div class="col-md-4">';
//                            str += '<div class="easy-pie-chart">';
//                            str += '<div data-percent="' + record.nagoriks + '" class="number nothis">';
//                            str += '<span>';
//                            str += record.nagoriks + '%</span>';
//                            str += '<canvas height="75" width="75"></canvas></div>';
//                            str += '<a href="#" class="title">';
//                            str += '<i class="icon-paper-plane"></i>নাগরিক  ডাক';
//                            str += '</a>';
//                            str += '</div>';
//                            str += '</div>';
                            str += '<div class="margin-bottom-10 visible-sm">';
                            str += '</div>';
                            str += '<div class="col-md-6">';
                            str += '<div class="easy-pie-chart">';
                            str += '<div data-percent="' + record.nothi_count + '" class="number others">';
                            str += '<span>';
                            str += record.nothi_count + '%</span>';
                            str += '<canvas height="75" width="75"></canvas></div>';
                            str += '<a href="#" class="title">';
                            str += '<i class="icon-notebook"></i>নথি';
                            str += '</a>';
                            str += '</div>';
                            str += '</div>';
                            str += '</div>';
                            str += '</div>';
                            str += '</div>';
                            str += '</div>';
                            str += '</div>';
                            str += '</div>';
                            str += '</div>';
                            str += '</li>';

                            $("ul.timeline").append(str);

                            // Get the 3 percetile values from server/db and pass it to the size params below

                            $('.easy-pie-chart .number.daks:last').easyPieChart({
                                animate: 1000,
                                size: 75,
                                lineWidth: 3,
                                barColor: Metronic.getBrandColor('yellow')
                            });

                            $('.easy-pie-chart .number.nothis:last').easyPieChart({
                                animate: 1000,
                                size: 75,
                                lineWidth: 3,
                                barColor: Metronic.getBrandColor('green')
                            });

                            $('.easy-pie-chart .number.others:last').easyPieChart({
                                animate: 1000,
                                size: 75,
                                lineWidth: 3,
                                barColor: Metronic.getBrandColor('red')
                            });
                        });

                    });
        },
        removeAssignment: function (record_id) {
            var r = confirm("Are you sure to release employee from this designation ?");
            if (r == true) {
                PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + 'employeeRecords/removeAssignment',
                        {'id': record_id, 'last_release_date': $("#last_release_date_" + record_id).val()}, 'json',
                        function (response) {
                            EmployeeManagement.getEmployeeJobStatus();
                        });
            } else {

            }
        }
    };
    $(function () {
        $("#btn_employee_search").bind('click', function (evt) {
            evt.preventDefault();
            $("#p_img").html("");
            var username = getusername();
            if(username=='' || typeof(username)=='undefined'){
                toastr.error("লগইন আইডি সঠিক নয়");
                return;
            }
            EmployeeManagement.getEmployeeJobStatus(username);
            EmployeeManagement.encodeImage(username, function (src) {
                $('#p_img').html('<img src="' + src + '" alt="signature" width="100" />');
            });
        });
    });
    $("#employee-identity-no").keypress(function (e) {
        if (e.which == 13 || e.keyCode == 13) {
            e.preventDefault();
            $("#p_img").html("");
            var username = getusername();
             if(username=='' || typeof(username)=='undefined'){
                 toastr.error("লগইন আইডি সঠিক নয়");
                return;
            }
            EmployeeManagement.getEmployeeJobStatus(username);
            EmployeeManagement.encodeImage(username);
        }
    });
    function getusername()
    {
        var identity_no = $("#employee-identity-no").val();
        if (identity_no == '') {
            return;
        }
        var len = identity_no.length;
        if (len > 0) {
            if (identity_no[0] == 0) {
                return;
            }
        }
        if (len < 12) {
            var fake_id = identity_no;
            identity_no = '';
            identity_no += fake_id[0];
            var i = 0;
            var j = 0;
            var k = 0;
            for (i = 1; i <= 12 - len; i++) {
                identity_no += '0';
            }

            for (j = i, k = 1; j + 1 <= 12; j++, k++) {
                identity_no += fake_id[k];
            }
        }
        $("#employee-identity-no").val(identity_no);
        return identity_no;
    }
</script>
