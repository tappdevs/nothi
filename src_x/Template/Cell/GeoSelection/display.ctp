<div class="row">
    <div class="col-md-3 form-group form-horizontal">
        <label class="control-label">বিভাগ </label>
        <?php echo $this->Form->input('geo_division_id', array('empty' => '--Select--', 'options' => $data_items, 'type' => 'select', 'label' => false, 'class' => 'form-control', 'placeholder' => 'বিভাগ ','default'=>$divitionid)); ?>
    </div>
    <div class="col-md-3 form-group form-horizontal">
        <label class="control-label">জেলা </label>
        <?php echo $this->Form->input('geo_district_id', array('empty' => '--Select--', 'type' => 'select', 'label' => false, 'class' => 'form-control', 'placeholder' => 'জেলা  ')); ?>
    </div>
    <div class="col-md-3 form-group form-horizontal">
        <label class="control-label">উপজেলা </label>
        <?php echo $this->Form->input('geo_upazila_id', array('empty' => '--Select--', 'type' => 'select', 'label' => false, 'class' => 'form-control', 'placeholder' => 'উপজেলা')); ?>
    </div>
    <div class="col-md-3 form-group form-horizontal">
        <label class="control-label">ইউনিয়ন</label>
        <?php echo $this->Form->input('geo_union_id', array('empty' => '--Select--', 'type' => 'select', 'label' => false, 'class' => 'form-control', 'placeholder' => 'ইউনিয়ন')); ?>
    </div>
</div>
<script src="<?php echo CDN_PATH; ?>assets/admin/layout4/scripts/geo_setup.js" type="text/javascript"></script>

<script type="text/javascript">
    $(function () {
        $("#geo-division-id").bind('change', function () {
            var geo_division_id = $(this).val();
            GeoSetup.loadDistricts(geo_division_id,<?php echo $districtid ?>);
        });

        $("#geo-district-id").bind('change', function () {
            var geo_district_id = $(this).val();
            GeoSetup.loadUpazilas(geo_district_id,<?php echo $upazilla ?>);
        });

        $("#geo-upazila-id").bind('change', function () {
            var geo_upazila_id = $(this).val();
            GeoSetup.loadUnions(geo_upazila_id,<?php echo $union ?>);
        });
        
        $("#geo-division-id").trigger('change');
    });
</script>