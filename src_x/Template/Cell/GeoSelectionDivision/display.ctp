<div class="row">
    <div class="col-md-12 form-group form-horizontal">
        <label class="control-label  font-lg">বিভাগ </label>
        <?php echo $this->Form->input('geo_division_id', array('empty' => '--Select--', 'options' => $data_items, 'type' => 'select', 'label' => false, 'class' => 'form-control', 'placeholder' => 'দপ্তর / অধিদপ্তর ')); ?>
    </div>
</div>