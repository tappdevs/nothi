<div class="row permissiondiv">
    <div class="col-md-12">
        <div class="portlet box red">
            <div class="portlet-title" style="padding: 5px;">
                <div class="caption">
                    অফিস বাছাই করুন
                </div>
                <div class="tools">
                    <a style="color: #fff;" data-original-title="" href="javascript:;" onclick="$('.display-hide').toggle(); $(this).find('i').toggleClass('a2i_gn_add1').toggleClass('a2i_gn_close2')">
                        <i class="fs1 a2i_gn_add1"></i>
                    </a>
                </div>
            </div>
            <div style="display: none;" class="portlet-body display-hide">
                <?php echo $this->cell('OfficeUnitOrganogramByMinistry', ['',$id,$nothi_office,$api]); ?>
            </div>
        </div>
    </div>
</div>
