<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>

<script>
    $('#modal_title_uposthapon_nothijaato').html('ডাক নথিতে <?php echo !empty($nothijato)?"নথিজাত":"পেশ/উপস্থাপন" ?>&nbsp;&nbsp;<a dak_subject="<?php echo!empty($dak_subject) ? h($dak_subject) : '' ?>" class="newNothiCreate btn btn-sm green" data-nothijato="<?php echo isset($nothijato)?$nothijato:0 ?>" href="javascript: void(0);" style="font-size: 12px; vertical-align: bottom;"><i class="fs0 a2i_gn_add1"></i> নতুন নথি তৈরি করুন</a>');
</script>
<!--<h4>যে সব নথিতে <?php echo !empty($nothijato)?'নথিজাত':'পেশ/উপস্থাপন' ?>  করা যাবে &nbsp;&nbsp;<a dak_subject="<?php echo!empty($dak_subject) ? $dak_subject : '' ?>" class="newNothiCreate btn btn-sm green" data-nothijato="<?php echo isset($nothijato)?$nothijato:0 ?>" href="javascript: void(0);" style="font-size: 12px; vertical-align: bottom;"><i class="fs0 a2i_gn_add1"></i> নতুন নথি তৈরি করুন</a></h4>
<hr/>-->
<style>
    .selectPartNo{
        cursor: pointer;
    }
    .nothipermittedlistSummery {
        cursor: pointer;
    }
    .nothiprmittedlstSummery{
        width: 100%!important;   
    }
</style>
<div >
    <div class="row">
        <div class="col-md-8 col-sm-8 col-lg-8 ">
            <?php if (!empty($nothiPermissionList)) {
                ?>
                <table class="table table-striped table-hover nothiprmittedlstSummery">
                    <thead >
                        <tr role="row" class="heading">
                            <th class="text-center" style="width: 8%!important;">
                                <?php echo __("ক্রম") ?>
                            </th>
                            <th class="text-center" style="width: 20%!important;">
                                <?php echo __("শাখা") ?>
                            </th>

                            <th class="text-center" style="width: 22%!important;">
                                <?php echo __("নথি নম্বর") ?>
                            </th>
                            <th class="text-center" style="width: 30%!important;">
                                <?php echo __("বিষয়") ?>
                            </th>
                        </tr>
                    </thead>
                    <tbody >
                        <?php
                        if (!empty($nothiPermissionList)) {
                           
                            foreach ($nothiPermissionList as $key => $row) {
                                $officeUnitTable = \Cake\ORM\TableRegistry::get('OfficeUnits');
                                $unitInformation = $officeUnitTable->get($row['office_units_id']);
                                ?>
                                <tr  style="cursor: pointer;" class="nothipermittedlistSummery">
                                    <td class="text-center" nothi_id="<?php echo $row['id'] ?>">
                                        <?php echo $this->Number->format(++$key); ?>
                                    </td>
                                    <td >
                                        <?php echo h($unitInformation['unit_name_bng']) ?>
                                    </td>

                                    <td>
                                        <?php echo h($row['nothi_no']) ?>
                                    </td>
                                    <td >
                                        <?php echo h($row['subject']) ?>
                                    </td>

                                </tr>
                                <?php
                            }
                        }
                        ?>
                    </tbody>
                </table>
                <?php
            }
            ?>
        </div>
        <?php  if(empty($nothijato)){ ?>
        <div class="col-md-4 col-sm-4 col-lg-4 part_nothi_list" style="height:calc(100vh - 275px)">
            <p class="selectedNothiName text-center text-success ">নথি বাছাই করুন</p>
            <table class="table table-striped table-bordered table-hover" id="nothi_list_datatable">
                <thead>
                    <tr>
                        <th>
                            <input type="text" class="form-control" id="nothisubject" name="nothisubject" placeholder="নোট বাছাই করুন"/>
                        </th>
                    </tr>
                </thead>
                <tbody >
                    
                </tbody>
            </table>
        </div>
        <?php }?>
    </div>
</div>
<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>

<script>
    $(document).off('click', '.selectPartNo');
    $(document).on('click', '.selectPartNo', function () {

        $(this).toggleClass('active');
        $(this).siblings().removeClass('active');
    });

    $(document).off('click', '.nothipermittedlistSummery');
    $(document).on('click', '.nothipermittedlistSummery', function () {
        $(this).toggleClass('active');
        $(this).siblings().removeClass('active');
        if($('#nothijato_input').val() == 1){
            var master_id = $(this).find('td').eq(0).attr('nothi_id');
            $('#NothiVuktoKoronForm').find('input[type=submit]').attr('disabled', 'disabled');
             $("#selected_nothi_part_id").val(0);
             $("#selected_nothi_part_id").val(0);
            $.ajax({
               url: '<?php echo $this->Url->build(['controller'=>'NothiMasters','action'=>'getFirstPart']) ?>/' + master_id + '/' + $('#nothijato_input').eq(0).val(),
               cache: true,
               method: 'post',
               success: function(response){
                   if(!response){
                       toastr.error('দুঃখিত! নথিটি আবার বাছাই করুন ।');
                       $(this).toggleClass('active');
                   }else{
                       $("#selected_nothi_part_id").val(response);
                       $("#selected_nothi_master_id").val(master_id);
                   }
                   $('#NothiVuktoKoronForm').find('input[type=submit]').removeAttr('disabled');
               },
              error: function(){
                   toastr.error('দুঃখিত! নথিটি আবার বাছাই করুন ।');
                   $('#NothiVuktoKoronForm').find('input[type=submit]').removeAttr('disabled');
              }
            });
            return;
         }
        $("#nothi_list_datatable").dataTable().fnDestroy();
        $('#nothi_list_datatable').find('tbody').html('');
        $('.selectedNothiName').text('নথি বাছাই করুন');
        
        
        if ($(this).hasClass('active')) {
             $('.selectedNothiName').text("নথি নম্বর: " + $(this).find('td').eq(3).text());
            $.ajax({
               url: '<?php echo $this->Url->build(['controller'=>'NothiMasters','action'=>'getPart']) ?>/' + $(this).find('td').eq(0).attr('nothi_id') + '/' + $('#nothijato_input').eq(0).val(),
               method: 'post',
               success: function(response){
                   $('#nothi_list_datatable').find('tbody').html(response);  
                  
                   initTable1();
               }
            });
        }
    });

    if ($(".nothipermittedlistSummery").length == 1) {
        $(".nothipermittedlistSummery").trigger("click");
    }

    var initTable1 = function () {
       
      
        var table = $('#nothi_list_datatable');

        var oTable = table.dataTable({
             destroy: true,
            "bLengthChange": false,
            "scrollY":        "500px",
            "scrollCollapse": true,
            loadingMessage: 'লোড করা হচ্ছে...',
            "language": {
                "emptyTable": "কোনো তথ্য নেই",
                "info": "<span class='seperator'>|</span>মোট _TOTAL_ টি তথ্য পাওয়া গেছে",
                "infoEmpty": "কোনো তথ্য নেই",
                "infoFiltered": "<span class='seperator'>|</span>মোট _TOTAL_ টি তথ্য পাওয়া গেছে",
                "search": "খুঁজুন  ",
                "zeroRecords": "কোনো তথ্য নেই"
            },
            
            "serverSide": false,
            "paging": false,
            "ordering": false,
            "info": false,
            "bStateSave": false,
            // set the initial value
            "pageLength": -1,
            "dom": "<'table-scrollable't>"
        });
        
        $(document).on('keyup change','#nothisubject', function(){
            oTable.api().search($(this).val()).draw();
        });
 
    }

    var initTable2 = function () {
       
      
        var table = $('.nothiprmittedlstSummery');

        var oTable = table.dataTable({
          
            "dom": "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",
            "scrollY": "300",
            "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

            "lengthMenu": [
                [10, 20, 50, 100, 500],
                ["১০", "২০", "৫০", "১০০", "৫০০"] // change per page values here
            ],
            "pageLength": 20, // default record count per page
           
            loadingMessage: 'লোড করা হচ্ছে...',
            "language": { // language settings
                    // metronic spesific
                    "metronicGroupActions": "_TOTAL_ রেকর্ড নির্বাচন করা হয়েছে:  ",
                    "metronicAjaxRequestGeneralError": "অনুরোধ সম্মন্ন করা সম্ভব হচ্ছে না।",

                    // data tables spesific
                    "lengthMenu": "<span class='seperator'></span>দেখুন _MENU_ ধরন",
                    "info": "মোট _TOTAL_ টি পাওয়া গেছে",
                    "infoEmpty": "",
                    "sSearch": "খুঁজুন: ",
                    "emptyTable": "কোনো রেকর্ড  নেই",
                    "zeroRecords": "কোনো রেকর্ড  নেই",
                    "paginate": {
                        "previous": "প্রথম",
                        "next": "পরবর্তী",
                        "last": "শেষের",
                        "first": "প্রথম",
                        "page": "পাতা",
                        "pageOf": "এর"
                    }
                },
            "serverSide": false,
            "ordering": false,
            "infoFiltered": "( খোঁজা হয়েছে _MAX_ টি তথ্য থেকে)",
        });
        
    }
    initTable2();
</script>