<div class="row permissiondiv">
	<div class="col-md-6">
		<div class="portlet box green">
			<div class="portlet-title"><h3 style="font-weight: bold;margin-top:7px;">নির্বাচিত পদসমূহ</h3></div>
			<div class="portlet-body">
				<div class="tabbable-custom ">
					<ul class="nav nav-tabs ">
						<li class="active">
							<a href="#ownOfficeSelected" data-toggle="tab" aria-expanded="false">নিজ অফিসের পদসমূহ (<span id="ownOfficeTotalSelected">০</span>)</a>
						</li>
						<li class="">
							<a href="#otherOfficeSelected" data-toggle="tab" aria-expanded="true">অন্য অফিসের পদসমূহ (<span id="otherOfficeTotalSelected">০</span>)</a>
						</li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="ownOfficeSelected">
							<table class="table table-bordered table-hover table-striped">
								<thead>
								<tr>
									<th>#</th>
									<th>পদবি</th>
									<th>ক্রম</th>
								</tr>
								</thead>
								<tbody></tbody>
							</table>
						</div>
						<div class="tab-pane" id="otherOfficeSelected">
							<table class="table table-bordered table-hover table-striped">
								<thead>
								<tr>
									<th>#</th>
									<th>পদবি</th>
									<th>ক্রম</th>
								</tr>
								</thead>
								<tbody></tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="portlet box green">
			<div class="portlet-title"><h3 style="font-weight: bold;margin-top:7px;">পদবি নির্বাচন করুন</h3></div>
			<div class="portlet-body">
				<div class="tabbable-custom ">
					<ul class="nav nav-tabs ">
						<li class="active">
							<a href="#ownOfficeSelection" data-toggle="tab" aria-expanded="false">নিজ অফিসের পদসমূহ</a>
						</li>
						<li class="">
							<a href="#otherOfficeSelection" data-toggle="tab" aria-expanded="true">অন্য অফিসের পদসমূহ</a>
						</li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="ownOfficeSelection">
							<div class="table-container">
								<div class="panel-group accordion">
									<?= $data; ?>
								</div>
							</div>
						</div>
						<div class="tab-pane" id="otherOfficeSelection">
							<div class="permissiondiv">
								<?php echo $this->cell('OfficeUnitOrganogramByMinistry', ['',0,$nothi_office]); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<script>
    $(document).ready(function() {
        $("a[href=#ownOfficeSelection]").on('click', function() {
            $("a[href=#ownOfficeSelected]").tab('show');
        });
        $("a[href=#ownOfficeSelected]").on('click', function() {
            $("a[href=#ownOfficeSelection]").tab('show');
        });

        $("a[href=#otherOfficeSelection]").on('click', function() {
            $("a[href=#otherOfficeSelected]").tab('show');
        });
        $("a[href=#otherOfficeSelected]").on('click', function() {
            $("a[href=#otherOfficeSelection]").tab('show');
        });
    });
    listown();
    listownother();
    function listown() {
        $('#ownOfficeSelected').find('tbody').html('');
        $.each($('.permissiondiv').find('.optionsRadios:checked'), function (i, v) {
            var ok = '';
            var id = escapeHtml($(v).data('employee-id'));
            var name = escapeHtml($(v).data('employee-name'));
            var of_id = escapeHtml($(v).data('employee-office-id'));
            var unit_id = escapeHtml($(v).data('office-unit-id'));
            var org_id = escapeHtml($(v).data('office-unit-organogram-id'));
            var designation = escapeHtml($(v).data('designation-name'));
            var unit_name = escapeHtml($(v).data('unit-name'));
            $.each($('#ownOfficeSelected').find('tbody>tr'), function (ind, val) {
                if ($(val).find('.own_office_permission').data('office-unit-organogram-id') == org_id) {
                    designation_level = escapeHtml($(val).find('#designation_level').val());
                }
            });
            var designation_level = escapeHtml($(v).data('designation-level'));

            var perm_value = parseInt($(v).val());
            ok += '<tr><td style="width: 30px;text-align: center;">&nbsp;&nbsp;';
            ok += '<input type="hidden" class="own_office_permission" name="office-employee" data-office-id="' + of_id + '"' +
                'data-office-employee-name="' + name + '" data-office-employee-id="' + id + '"' +
                ' data-office-unit-id="' + unit_id + '" data-office-unit-organogram-id="' + org_id + '" ' +
                'data-designation-name="' + designation + '" data-unit-name="' + unit_name + '" data-designation-level ="' + designation_level + '">' +
                '</td><td>' + name + ', ' + designation + ', ' + unit_name + '</td>' +
                '<td><input id="designation_level" type="text" value="' + designation_level + '" style="width:50px;"></td></tr>';

            $('#ownOfficeSelected').find('tbody').append(ok);
            $('#ownOfficeTotalSelected').html( enTobn($('#ownOfficeSelected').find('tbody>tr').length) );
        });
    }
    function listownother() {
        $('#otherOfficeSelected').find('tbody').html('');
        $.ajax({
            type: 'POST',
            url: "<?php echo $this->Url->build(['controller' => 'NothiMasters', 'action' => 'other_user']) ?>",
            data: {"master_id": <?= $id ?>,"nothi_office": <?= $nothi_office ?>},
            success: function (data) {
                $('#otherOfficeSelected').find('tbody').html(data);
                $('#otherOfficeTotalSelected').html( enTobn($('#otherOfficeSelected').find('tbody>tr').length) );
            }
        });
    }
    function add_other_offices_by_selection(ok) {
        var heading = '<div class="row">'+
            '<div class="col-md-10" style="text-align: center;">'+
            'অনুমতিপ্রাপ্ত'+
            '</div>'+
            '<div class="col-md-2" style="text-align: center;">'+
            'ক্রম'+
            '</div>'+
            '</div>';
        $.each($('.otherPermissionList').find('.optionsothers:checked'), function (i, v) {

            var id = $(v).data('employee-id');
            var name = $(v).data('employee-name');
            var of_id = $(v).data('employee-office-id');
            var unit_id = $(v).data('office-unit-id');
            var org_id = $(v).data('office-unit-organogram-id');
            var designation = $(v).data('designation-name');
            var unit_name = $(v).data('unit-name');
            var designation_level = $(v).data('designation-level');
            var perm_value = parseInt($(v).val());
            var incharge_label = $(v).data('employee-incharge-label');
            ok += '<tr><td style="width: 30px;text-align: center;">&nbsp;&nbsp;';
            ok += '<input type="hidden" class="other_office_permission" name="office-employee" data-office-id="' + of_id + '"' +
                    'data-office-employee-name="' + name + '" data-office-employee-id="' + id + '"' +
                    ' data-office-unit-id="' + unit_id + '" data-office-unit-organogram-id="' + org_id + '" ' +' data-incharge-label="' + incharge_label + '" ' +
                    'data-designation-name="' + designation + '" data-unit-name="' + unit_name + '" data-designation-level ="'+designation_level+'">' +
                    '</td><td>&nbsp;&nbsp;' + name + ', ' + designation + ', ' + unit_name + '</td><td><input id="designation_level" type="text" value="'+designation_level+'"></td></tr></tr>';

        });

        $('#addotherpermission').append(ok);
        if($('#addotherpermission').html() != '') {
            $("#addotherpermission_heading").html(heading);
        }
    }
    function removeDuplicateRows($table) {
        function getVisibleRowText($row) {
            return $row.find('td:visible').text().toLowerCase();
        }

        $table.find('tr').each(function (index, row) {
            var $row = $(row);

            $row.nextAll('tr').each(function (index, next) {
                var $next = $(next);
                if (getVisibleRowText($next) == getVisibleRowText($row))
                    $next.remove();
            });
        });
    }
    function list_all(e_id)
    {
        $.each($('.otherPermissionList').find('.optionsothers'), function (i, v) {
            if ($(v).data('office-unit-organogram-id') == e_id) {
                if ($(v).is(':checked')) {
                    if ($('#otherOfficeSelected').find('tbody').find('.other_office_permission[data-office-unit-organogram-id='+e_id+']').length > 0) {
                        toastr.error('ইতোমধ্যে নির্বাচন করা হয়েছে');
                    } else {
                        var ok = '';
                        var id = $(v).data('employee-id');
                        var name = $(v).data('employee-name');
                        var of_id = $(v).data('employee-office-id');
                        var unit_id = $(v).data('office-unit-id');
                        var org_id = $(v).data('office-unit-organogram-id');
                        var designation = $(v).data('designation-name');
                        var unit_name = $(v).data('unit-name');
                        var designation_level = $(v).data('designation-level');
                        var perm_value = parseInt($(v).val());
                        var incharge_label = $(v).data('employee-incharge-label');
                        ok += '<tr><td style="width: 30px;text-align: center;">&nbsp;&nbsp;';
                        ok += '<input type="hidden" class="other_office_permission" name="office-employee" data-office-id="' + of_id + '"' +
                            'data-office-employee-name="' + name + '" data-office-employee-id="' + id + '"' +
                            ' data-office-unit-id="' + unit_id + '" data-office-unit-organogram-id="' + org_id + '" ' + ' data-incharge-label="' + incharge_label + '" ' +
                            'data-designation-name="' + designation + '" data-unit-name="' + unit_name + '" data-designation-level ="' + designation_level + '">' +
                            '</td><td>&nbsp;&nbsp;' + name + ', ' + designation + ', ' + unit_name + '</td><td style="min-width:20px"><input id="designation_level" type="text" style="width:40px" value="' + designation_level + '"></td></tr></tr>';
                        $('#otherOfficeSelected').find('tbody').append(ok);
                    }
                } else {
                     $.each($('#otherOfficeSelected').find('tbody').find('.other_office_permission'), function () {
                         if($(this).data('office-unit-organogram-id')==e_id){$(this).closest('tr').remove();}
                     });
                }
            }
            $('#otherOfficeTotalSelected').html( enTobn($('#otherOfficeSelected').find('tbody>tr').length) );
        });
    }

</script>