<style>


</style>

<?php if (!empty($nothiList)) {
    ?>
    <h4>অন্যান্য নথি বাছাই করুন</h4>
    <hr/>
    <?php
    foreach ($nothiList as $key => $row) {
        $officeUnitTable = \Cake\ORM\TableRegistry::get('OfficeUnits');
        $unitInformation = $officeUnitTable->get($row['office_units_id']);
        ?>
        <div class="row">
            <div class="col-md-12  form-group">
                <input
                    name="nothiNo[]" <?php echo (isset($nothiMastersCurrentList[$row['id']]) && $nothiMastersCurrentList[$row['id']] == $employee_id) ? "" : 'disabled=disabled'; ?>
                    id="nothi_no_<?php echo $row->id; ?>" type="radio" class="form-control nothiCheckbox"
                    value="<?php echo $row->id; ?>"/>
                <label
                    for="nothi_no_<?php echo $row->id; ?>" <?php echo (isset($nothiMastersCurrentList[$row['id']]) && $nothiMastersCurrentList[$row['id']] == $employee_id) ? "class='label label-warning'" : ''; ?>>শাখা: <?php echo $unitInformation['unit_name_bng'] ?>
                    ধরন:  <?php echo $row['NothiTypes']['type_name'] ?>, নথি নম্বর: <?php echo $row['nothi_no'] ?>,
                    বিষয়: <?php echo $row['subject'] ?></label>
            </div>
        </div>
    <?php
    }
}
?>
