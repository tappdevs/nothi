<div class="row">
    <div class="col-md-12">
        <div class="portlet green-meadow box">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-cogs"></i>Search From Office Hierarchy</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                    <a href="#portlet-config" data-toggle="modal" class="config"></a>
                    <a href="javascript:;" class="reload"></a>
                    <a href="javascript:" class="remove"></a>
                </div>
            </div>
            <div class="portlet-body form">
                <?= $this->Form->create("", ['class' => 'form-horizontal']); ?>
                <div class="form-body">
                    <div class="form-group">
                        <div class="col-md-6">
                            <?php echo $this->Form->input('office_hierarchy', array('list' => 'office_hierarchy_list', 'label' => false, 'class' => 'form-control')); ?>
                            <?php echo $this->Form->hidden('office_hierarchy_id', array('id' => 'office-hierarchy-id', 'label' => false, 'class' => 'form-control')); ?>
                        </div>
                        <div class="col-md-6">
                            <button type="button" class="btn btn-danger office_hierarchy_btn">Load Office</button>
                        </div>

                        <datalist id="office_hierarchy_list">
                            <?php foreach ($office_hierarchy_list as $ohl) { ?>
                                <option value="<?= $ohl; ?>"></option>
                            <?php } ?>
                        </datalist>
                    </div>
                </div>
                <?= $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>
