<style>
    .radio{
        margin-left: 20px;
    }
</style>
<div class="row">
    <div class="col-md-6 form-group">
        <label class="control-label"> গ্রুপ বাছাই করুন </label>
        <?php
        $type_prefix = explode("_", $prefix);
        $type_prefix = $type_prefix[0]."_group";
        ?>

        <div class="input select">
            <select data-prefix="<?= $prefix ?>" name="<?= $prefix.'group_id' ?>" class="form-control select group_id_choose" placeholder="গ্রুপ বাছাই করুন" id="<?= $prefix ?>group_id" >
                <option value="">--বাছাই করুন--</option>
                <?php if (!empty($valueList)): ?>
                    <?php foreach ($valueList as $key => $value): ?>
                        <?= "<option value={$value['id']} data-list='".h($value['list'])."'>{$value['value']}</option>" ?>
                    <?php endforeach; ?>
                <?php endif; ?>

            </select>
        </div>
         <div class="form-group">
             <hr>
                <p class="bold"> পত্রজারিতে যে নাম দেখাবে </p>
                <label class="radio"><input type="radio" name="<?=$prefix?>grpNameradio" id="<?=$prefix?>CombmindGroupName" checked="checked">  গ্রুপের নাম </label>
                <div class="<?=$prefix?>grpTmpNameDiv" style="margin-left: 40px; display: none">
                    <label class="radio"><input type="radio" name="<?=$prefix?>TmpNameShowradio" id="<?=$prefix?>customGrpName" checked="checked">
                    <input type="text" name="<?=$prefix?>grpTmpName" id="<?=$prefix?>grpTmpName"></label>
                    <label class="radio"><input type="radio" name="<?=$prefix?>TmpNameShowradio" id="<?=$prefix?>DropDownGrpName"><span id="<?=$prefix?>SpanGrpName"> Group Name</span></label>
                </div>

             <label class="radio"><input type="radio" name="<?=$prefix?>grpNameradio" id="<?=$prefix?>SeperateUserNameForGroup">  নির্বাচিত ব্যবহারকারীদের নাম </label>
        </div>
    </div>
    <div class="col-md-6 form-group">
        <div class="">
            <p class="bold"> ব্যবহারকারী </p>
                <label class="radio"><input type="radio" name="<?=$prefix?>grpradio" onclick="selectAllUser('<?=$prefix?>')" disabled>  সকলকে বাছাই করুন </label>
                <label class="radio"><input type="radio" name="<?=$prefix?>grpradio" onclick="deselectAllUser('<?=$prefix?>')" disabled>  সকলকে বাদ দিন </label>
        </div>
        <label class="control-label bold"> ব্যবহারকারী  বিবরণ </label>
        <div class="<?= $prefix ?>group_list full-height-content full-height-content-scrollable" style="height:200px;">
            <ul class="list-group">

            </ul>
        </div>

    </div>
</div>

<script src="<?php echo CDN_PATH; ?>assets/admin/layout4/scripts/projapoti_autocomplete.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).off('change', '.group_id_choose').on('change', '.group_id_choose', function () {
        var value =$(this).find('option:selected').val();
        var prefix  = $(this).data('prefix');
        if(value == '' || value == null || value == 'undefined'){
             $('[name='+prefix+'grpradio]').attr('disabled',true);
             $('[name='+prefix+'grpradio]').prop('checked',false);
            $('.' +prefix  + 'group_list').find('.list-group').html('');
            $('.group_id_choose').select2('val','');
            getTmpGroupName(prefix);
             return false;
        }
        setGroupInfo($(this).find('option:selected').data('list'), prefix);
        $( "[name="+prefix+"grpradio]").first().prop('checked',true);
        $('[name='+prefix+'grpradio]').removeAttr('disabled');
        $("#"+prefix+"SpanGrpName").text($(this).find('option:selected').text());
        getTmpGroupName(prefix);
    });

    function setGroupInfo(list, prefix) {
        $('.' + prefix + 'group_list').find('.list-group').html('');
        try{
             $.each(list, function (i, v) {
                if (v.office_unit_organogram_id == 0) {
                $('.' + prefix + 'group_list').find('.list-group').append("<li class='list-group-item' ><input type='checkbox' checked=checked class='" + prefix + "group_checkbox' data-office-name-bng='" + v.office_name_bng + "' data-office-name-eng='" + v.office_name_eng + "' data-office-id='" + v.office_id + "' data-office-employee-name-bng='" + (v.employee_name_bng!='(শূন্য পদ)'?v.employee_name_bng:v.office_unit_organogram_name_bng) + "'  data-office-employee-name-eng ='" + (v.employee_name_eng!='(No designation)'?v.employee_name_eng:v.office_unit_organogram_name_eng) + "' data-office-employee-id='" + v.employee_id + "' data-office-unit-id='" + v.office_unit_id + "'  data-office-unit-organogram-id='" + v.office_unit_organogram_id + "' data-designation-name-bng='" + v.office_unit_organogram_name_bng +"' data-designation-name-eng='" + v.office_unit_organogram_name_eng + "'  data-unit-name-eng='" + v.office_unit_name_eng + "'  data-unit-name-bng='" + v.office_unit_name_bng + "' data-officer-email='" + v.officer_email + "' data-office-head='" + v.office_head + "' /> &nbsp;" + (v.employee_name_bng!='(শূন্য পদ)'?(v.employee_name_bng + ", "):'')+ (isEmpty(v.office_unit_organogram_name_bng)?'':(v.office_unit_organogram_name_bng+", "))+(isEmpty(v.office_name_bng)?'':(v.office_name_bng))+(isEmpty(v.officer_email)?'':("("+v.officer_email+")"))+ "</li>");
            } else {
                $('.' + prefix + 'group_list').find('.list-group').append("<li class='list-group-item'><input type='checkbox' checked=checked class='" + prefix + "group_checkbox'  data-office-name-bng='" + v.office_name_bng + "' data-office-name-eng='" + v.office_name_eng + "' data-office-id='" + v.office_id + "' data-office-employee-name-bng='" + v.employee_name_bng + "'  data-office-employee-name-eng ='" + v.employee_name_eng + "' data-office-employee-id='" + v.employee_id + "' data-office-unit-id='" + v.office_unit_id + "'  data-office-unit-organogram-id='" + v.office_unit_organogram_id +  "' data-designation-name-bng='" + v.office_unit_organogram_name_bng +"' data-designation-name-eng='" + v.office_unit_organogram_name_eng + "'  data-unit-name-eng='" + v.office_unit_name_eng + "'  data-unit-name-bng='" + v.office_unit_name_bng + "' data-office-head='" + v.office_head + "' /> &nbsp;"+v.employee_name_bng+", " + v.office_unit_organogram_name_bng + ", " + v.office_unit_name_bng + ", " + v.office_name_bng + "</li>");
                    }
                });
            } catch (e) {
                console.log(e);
            }
        }
    function selectAllUser(prefix){
     $('.' + prefix + 'group_checkbox').prop('checked',true);
        getTmpGroupName(prefix);
    }
    function deselectAllUser(prefix){
       $('.' + prefix + 'group_checkbox').prop('checked',false);
       if(prefix=='receiver_'){
        toastr.error('আপনি কোনো প্রাপক সিলেক্ট করেন নি');
           getTmpGroupName(prefix);
        return false;
       }else{
           toastr.error('আপনি কোনো অনুলিপি সিলেক্ট করেন নি');
           getTmpGroupName(prefix);
           return false;
       }
        getTmpGroupName(prefix);
    }
    $(document).off('click', '#<?=$prefix?>SeperateUserNameForGroup').on('click', '#<?=$prefix?>SeperateUserNameForGroup', function () {
        if(this.checked) {
            $(".<?=$prefix?>grpTmpNameDiv").hide();
        }
    });

    $(document).off('click', '#<?=$prefix?>CombmindGroupName').on('click', '#<?=$prefix?>CombmindGroupName', function () {
        getTmpGroupName('<?=$prefix?>');
    });

    $(document).off('change', '.<?=$prefix?>group_checkbox').on('change', '.<?=$prefix?>group_checkbox', function () {
        getTmpGroupName('<?=$prefix?>');
    });

    function getTmpGroupName(prefix) {
        var group_select_val = $("#"+ prefix +"group_id option:selected").val();
        if (($('#'+ prefix +'CombmindGroupName').is(':checked')) && !isEmpty(group_select_val)) {

            var designations_values = [];
            $.each($('.'+ prefix +'group_checkbox'), function (i, v) {
                if (v.checked == true) {
                    var designation = $.trim($(this).data('designation-name-bng'));
                    designations_values.push(designation);
                }
            });

            var unique_designation = jQuery.unique(designations_values);
            if (unique_designation.length == 1) {

                var offices_values = [];
                $.each($('.'+ prefix +'group_checkbox'), function (i, v) {
                    if (v.checked == true) {
                        var office = $(this).data('office-name-bng');

                        var place_name = $.trim(office.split(",").pop());
                        offices_values.push(place_name);
                    }
                });
                var unique_offices = jQuery.unique(offices_values);

                var new_val = unique_designation[0] +", "+ unique_offices.join("/") ;
                $("#"+ prefix +"grpTmpName").val(new_val);

            } else {
                var temp_text = $("#"+ prefix +"group_id option:selected").text();
                $("#"+ prefix +"grpTmpName").val(temp_text);
            }
            $("."+ prefix +"grpTmpNameDiv").show();
            $("#"+ prefix +"grpTmpName").focus();

        } else {
            $("."+ prefix +"grpTmpNameDiv").hide();
        }
    }
</script>
