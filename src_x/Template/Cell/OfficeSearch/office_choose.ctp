<div class="row">
    <div class="col-md-12">
        <?= $officeSelectionCell = $this->cell('OfficeSelection', ['entity' => '', 'prefix' => $prefix]) ?>
        <div class="row">
            <div class="col-md-4 form-group form-horizontal">
                <label class="control-label"> কার্যালয় </label>
                <?php echo $this->Form->input($prefix . 'office_id', array('empty' => '--বাছাই করুন--', 'type' => 'select', 'label' => false, 'class' => 'form-control input-sm')); ?>
            </div>
            <div class="col-md-4 form-group form-horizontal">
                <label class="control-label"> দপ্তর/শাখা </label>
                <?php echo $this->Form->input($prefix . 'office_unit_id', array('empty' => '--বাছাই করুন--', 'type' => 'select', 'label' => false, 'class' => 'form-control input-sm')); ?>
            </div>
            <div class="col-md-4 form-group form-horizontal">
                <label class="control-label"> পদ </label>
                <?php echo $this->Form->input($prefix . 'office_unit_organogram_id', array('empty' => '--বাছাই করুন--', 'type' => 'select', 'label' => false, 'class' => 'form-control input-sm')); ?>
                <?php echo $this->Form->hidden($prefix . 'office_unit_organogram', array('id' => $prefix . 'office_unit_organogram')); ?>
            </div>
            <div class="col-md-4 form-group form-horizontal">
                <label class="control-label"> অফিসার  </label>
                <?php echo $this->Form->input($prefix . 'officer_name', array('label' => false, 'class' => 'form-control input-sm', 'readonly' => true)); ?>
                <?php echo $this->Form->hidden($prefix . 'officer_id', array('id' => $prefix . 'officer_id')); ?>
                <?php echo $this->Form->hidden($prefix . 'office_head', array('id' => $prefix . 'office_head')); ?>
                <?php echo $this->Form->hidden($prefix . 'incharge_label', array('id' => $prefix . 'incharge_label')); ?>
                <?php echo $this->Form->hidden($prefix . 'office_name_eng', array('id' => $prefix . 'office_name_eng')); ?>
                <?php echo $this->Form->hidden($prefix . 'unit_name_eng', array('id' => $prefix . 'unit_name_eng')); ?>
                <?php echo $this->Form->hidden($prefix . 'designation_name_eng', array('id' => $prefix . 'designation_name_eng')); ?>
                <?php echo $this->Form->hidden($prefix . 'name_eng', array('id' => $prefix . 'name_eng')); ?>
                <?php echo $this->Form->hidden($prefix . 'incharge_label_eng', array('id' => $prefix . 'incharge_label_eng')); ?>
                <?php echo $this->Form->hidden($prefix . 'personal_mobile', array('id' => $prefix . 'personal_mobile')); ?>
            </div>
            <?php
            if($is_api==0): ?>
                <?php if($prefix == 'sovapoti_' || $prefix == 'receiver_' || $prefix == 'onulipi_' || $prefix == 'attension' ): ?>
                <div class="col-md-4 form-group form-horizontal">
                    <br/>
                    <label class="control-label"> নিজ অফিস </label>
                    <?php echo $this->Form->checkbox($prefix . 'own_office',['id'=>$prefix . 'own_office']); ?>
                </div>
                <?php endif; ?>
            <?php endif; ?>
        </div>
    </div>
</div>

    <script src="<?php echo CDN_PATH; ?>assets/admin/layout4/scripts/projapoti_ajax.js?v=<?= js_css_version ?>" type="text/javascript"></script>
<script>

    var is_api = '<?= isset($is_api)?($is_api):0 ?>';
    is_api = parseInt(is_api);
    // $(window).load(function() {
        (function($) {
        var prefix = '<?php echo $prefix;?>';
        var prefixmain = '<?php echo $prefix;?>';
        var office_id = '<?php echo isset($office_id)?$office_id:0;?>';

        prefix = prefix.split("_");
        prefix = prefix[0] + '-';

		<?php if($openOwnOffice && $office_id>0): ?>
            $('#<?= $prefix ?>own_office').click();

            $("#" + prefix + "office-id").val(office_id);
            $("#" + prefix + "office-ministry-id").closest('.row').hide();

            PROJAPOTI.projapoti_dropdown_map("#" + prefix + "office-id", <?php echo $officeArray; ?>, '', office_id);
            setTimeout(function(){
				$("#" + prefix + "office-id").change();
				$("#" + prefix + "office-unit-id").html('');
            },500)

        <?php endif; ?>

        $("#" + prefix + "office-ministry-id").bind('change', function () {
            OfficeOrgSelectionCell.loadMinistryWiseLayers(prefix);
        });
        $("#" + prefix + "office-layer-id").bind('change', function () {
            OfficeOrgSelectionCell.loadMinistryAndLayerWiseOfficeOrigin(prefix);
        });
        $("#" + prefix + "office-origin-id").bind('change', function () {
            OfficeOrgSelectionCell.loadOriginOffices(prefix);
        });
        $("#" + prefix + "office-id").bind('change', function () {
            OfficeOrgSelectionCell.loadOfficeUnits(prefix);
        });
        $("#" + prefix + "office-unit-id").bind('change', function () {
            OfficeOrgSelectionCell.loadOfficeUnitDesignations(prefix);
        });
        $("#" + prefix + "office-unit-organogram-id").bind('change', function () {
            OfficeOrgSelectionCell.getOfficerInfo($(this).val(), prefix, prefixmain);
        });
        
        if(office_id>0 && (prefixmain == 'sender_' || prefixmain == 'approval_')){
            $("#" + prefix + "office-id").val(office_id);
            $("#" + prefix + "office-ministry-id").closest('.row').hide();
            
            PROJAPOTI.projapoti_dropdown_map("#" + prefix + "office-id", <?php echo $officeArray; ?>, '',office_id);
            $("#" + prefix + "office-unit-id").html('');
            PROJAPOTI.ajaxSubmitDataCallback('<?php echo $this->request->webroot ?>' + 'officeManagement/loadOfficeUnits',
                {'office_id': office_id}, 'html',
                function (response) {
                    $("#" + prefix + "office-unit-id").html(response);
                });
        }

        $("#<?= $prefix ?>own_office").click(function(){
            // $(".receiver_list").hide();
            if(this.checked){
                if(office_id > 0){
                    $("#" + prefix + "office-id").val(office_id);
                    $("#" + prefix + "office-ministry-id").closest('.row').hide();

                    PROJAPOTI.projapoti_dropdown_map("#" + prefix + "office-id", <?php echo $officeArray; ?>, '',office_id);
                    $("#" + prefix + "office-id").change();
                    $("#" + prefix + "office-unit-id").html('');
//                    PROJAPOTI.ajaxSubmitDataCallback('<?php //echo $this->request->webroot ?>//' + 'officeManagement/loadOfficeUnits',
//                        {'office_id': office_id}, 'html',
//                        function (response) {
//                            $("#" + prefix + "office-unit-id").html(response);
//                        });
                }
            }else{
                $("#" + prefix + "office-id").val(0);
                $("#" + prefix + "office-ministry-id").closest('.row').show();
                $("#" + prefix + "office-office-id").html('');
                $("#" + prefix + "office-unit-id").html('');
            }
        })

        //$(document).off('click',"#<?//= $prefix ?>//own_office").on('click',"#<?//= $prefix ?>//own_office",function(){
        //    if(this.checked){
        //        if(office_id>0){
        //            $("#" + prefix + "office-id").val(office_id);
        //            $("#" + prefix + "office-ministry-id").closest('.row').hide();
        //
        //            PROJAPOTI.projapoti_dropdown_map("#" + prefix + "office-id", <?php //echo $officeArray; ?>//, '',office_id);
        //            $("#" + prefix + "office-unit-id").html('');
        //            PROJAPOTI.ajaxSubmitDataCallback('<?php //echo $this->request->webroot ?>//' + 'officeManagement/loadOfficeUnits',
        //                {'office_id': office_id}, 'html',
        //                function (response) {
        //                    $("#" + prefix + "office-unit-id").html(response);
        //            });
        //        }
        //    }else{
        //        $("#" + prefix + "office-id").val(0);
        //        $("#" + prefix + "office-ministry-id").closest('.row').show();
        //        $("#" + prefix + "office-office-id").html('');
        //        $("#" + prefix + "office-unit-id").html('');
        //    }
        //});


    // });
    }(jQuery));

</script>
<script type="text/javascript">
    var OfficeOrgSelectionCell = {

        loadMinistryWiseLayers: function (prefix) {
            $("#" + prefix + "office-layer-id").find('option').remove().end().append('<option value=""> লোড হচ্ছে... </option>').val('');
            if(isEmpty(is_api)){
                $("#" + prefix + "office-layer-id").select2();
            }
            PROJAPOTI.ajaxSubmitDataCallback('<?php echo $this->request->webroot ?>' + 'officeSettings/loadLayersByMinistry',
                {'office_ministry_id': $("#" + prefix + "office-ministry-id").val()}, 'json',
                function (response) {
                    PROJAPOTI.projapoti_dropdown_map("#" + prefix + "office-layer-id", response, "--বাছাই করুন--");
                    if(isEmpty(is_api)){
                        $("#" + prefix + "office-layer-id").select2();
                    }
                });
        },
        loadMinistryAndLayerWiseOfficeOrigin: function (prefix) {
            $("#" + prefix + "office-origin-id").find('option').remove().end().append('<option value=""> লোড হচ্ছে... </option>').val('');
            if(isEmpty(is_api)){
                $("#" + prefix + "office-origin-id").select2();
            }
            PROJAPOTI.ajaxSubmitDataCallback('<?php echo $this->request->webroot ?>' + 'officeSettings/loadOfficesByMinistryAndLayer',
                {
                    'office_ministry_id': $("#" + prefix + "office-ministry-id").val(),
                    'office_layer_id': $("#" + prefix + "office-layer-id").val()
                }, 'json',
                function (response) {
                    PROJAPOTI.projapoti_dropdown_map("#" + prefix + "office-origin-id", response, "--বাছাই করুন--");
                    if(isEmpty(is_api)){
                        $("#" + prefix + "office-origin-id").select2();
                    }
                });
        },
        loadOriginOffices: function (prefix) {
            $("#" + prefix + "office-id").find('option').remove().end().append('<option value=""> লোড হচ্ছে... </option>').val('');
            if(isEmpty(is_api)){
                $("#" + prefix + "office-id").select2();
            }
            PROJAPOTI.ajaxSubmitDataCallback('<?php echo $this->request->webroot ?>' + 'officeManagement/loadOriginOffices',
                {'office_origin_id': $("#" + prefix + "office-origin-id").val()}, 'json',
                function (response) {
                    PROJAPOTI.projapoti_dropdown_map("#" + prefix + "office-id", response, "--বাছাই করুন--");
                    if(isEmpty(is_api)){
                        $("#" + prefix + "office-id").select2();
                    }
                });
        },
        loadOfficeUnits: function (prefix) {
            $("#" + prefix + "office-unit-id").find('option').remove().end().append('<option value=""> লোড হচ্ছে... </option>').val('');
            if(isEmpty(is_api)){
                $("#" + prefix + "office-unit-id").select2();
            }
            PROJAPOTI.ajaxSubmitDataCallback('<?php echo $this->request->webroot ?>' + 'officeManagement/loadOfficeUnits',
                {'office_id': $("#" + prefix + "office-id").val()}, 'html',
                function (response) {
                    $("#" + prefix + "office-unit-id").html(response);
                    if(isEmpty(is_api)){
                        $("#" + prefix + "office-unit-id").select2();
                    }
                });
        },
        loadOfficeUnitDesignations: function (prefix) {
	        $("#" + prefix + "office-unit-organogram-id").find('option').remove().end().append('<option value=""> লোড হচ্ছে... </option>').val('');
            var url = '<?php echo $this->request->webroot ?>' + 'officeManagement/loadOfficeUnitOrganograms';
	        if(isEmpty(is_api)){
                $("#" + prefix + "office-unit-organogram-id").select2();
                url = '<?php echo $this->request->webroot ?>' + 'officeManagement/loadOfficeUnitOrganogramsWithName';
            }
            PROJAPOTI.ajaxSubmitDataCallback(url,
                {'office_unit_id': $("#" + prefix + "office-unit-id").val()}, 'json',
                function (response) {
                    if(isEmpty(is_api)){
                        PROJAPOTI.projapoti_dropdown_map_with_title("#" + prefix + "office-unit-organogram-id", response, "--বাছাই করুন--",'',{'key' : 'id','value' : 'designation','title' : 'name'  });
                    }else{
                        PROJAPOTI.projapoti_dropdown_map("#" + prefix + "office-unit-organogram-id", response, "--বাছাই করুন--");
                    }
//                    PROJAPOTI.projapoti_dropdown_map("#" + prefix + "office-unit-organogram-id", response, "--বাছাই করুন--");
//                    $("#" + prefix + "office-unit-organogram-id").select2();

                });

        },
        getOfficerInfo: function (designation_id, prefix, prefixmain) {
            PROJAPOTI.ajaxSubmitDataCallback('<?php echo $this->request->webroot ?>' + 'employeeRecords/getOfficerInfo',
                {'designation_id': designation_id}, 'json',
                function (response) {
                    $("input[name=" + prefixmain + "officer_id]").val(response.id);
                    $("input[name=" + prefixmain + "officer_name]").val(response.name_bng);
                    $("input[name=" + prefixmain + "office_head]").val(response.office_head);
                    $("input[name=" + prefixmain + "incharge_label]").val(response.incharge_label);
                    $("input[name=" + prefixmain + "office_name_eng]").val(response.office_name_eng);
                    $("input[name=" + prefixmain + "unit_name_eng]").val(response.unit_name_eng);
                    $("input[name=" + prefixmain + "designation_name_eng]").val(response.designation_name_eng);
                    $("input[name=" + prefixmain + "name_eng").val(response.name_eng);
                    $("input[name=" + prefixmain + "incharge_label_eng").val(response.incharge_label_eng);
                    $("input[name=" + prefixmain + "personal_mobile").val(response.personal_mobile);
                }
            );
        }
    };
</script>


