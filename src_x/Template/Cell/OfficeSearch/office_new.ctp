<div class="row ">
    <div class="col-md-4 col-lg-4 col-sm-6 form-group">
        <label class="control-label"> নাম </label>
        <?php echo $this->Form->input($prefix . 'officer_name', array('label' => false, 'class' => 'form-control input-sm', 'value' => (isset($response_params[$prefix . 'office_name'])) ? $response_params[$prefix . 'office_name'] : '')); ?>
    </div>
    <div class="col-md-4 col-lg-4 col-sm-6 form-group">
        <label class="control-label"> পদ <span class="required"> * </span> </label>
        <?php echo $this->Form->input($prefix . 'officer_designation_label', array('label' => false, 'class' => 'form-control input-sm', 'value' => (isset($response_params[$prefix . 'officer_designation_label'])) ? $response_params[$prefix . 'officer_designation_label'] : '')); ?>
    </div>
    <div class="col-md-4 col-lg-4 col-sm-6 form-group">
        <label class="control-label"> দপ্তর/শাখা </label>
        <?php echo $this->Form->input($prefix . 'office_unit_name', array('label' => false, 'class' => 'form-control input-sm', 'value' => (isset($response_params[$prefix . 'office_unit_name'])) ? $response_params[$prefix . 'office_unit_name'] : '')); ?>
    </div>
    <div class="col-md-4 col-lg-4 col-sm-6 form-group">
        <label class="control-label"> কার্যালয়/ঠিকানা <span class="required"> * </span></label>
        <?php echo $this->Form->input($prefix . 'office_name', array('label' => false, 'class' => 'form-control input-sm', 'value' => (isset($response_params[$prefix . 'office_name'])) ? $response_params[$prefix . 'office_name'] : '')); ?>
    </div>
    <div class="col-md-4 col-lg-4 col-sm-6 form-group">
        <label class="control-label"> ইমেইল </label>
        <?php echo $this->Form->input($prefix . 'officer_email', array('label' => false, 'class' => 'form-control input-sm', 'value' => (isset($response_params[$prefix . 'officer_email'])) ? $response_params[$prefix . 'officer_email'] : '')); ?>
    </div>
    <?php if($prefix != 'sender_' && $prefix != 'approval_' && $prefix!= 'sovapoti_'  && $prefix != 'attension_'): ?>
    <div class="col-md-12 col-lg-12 col-sm-12">
        <div class="font-lg" style="border-bottom: 1px solid #CACACA;color:red">* পত্রজারির এসএমএস পাঠাতে চাইলে </div>
        <div class="col-md-6 col-lg-6 col-sm-6 form-group">
            <label class="control-label"> মোবাইল নাম্বার</label>
            <?php echo $this->Form->input($prefix . 'officer_mobile', array('label' => false, 'class' => 'form-control input-sm', 'value' => (isset($response_params[$prefix . 'officer_mobile'])) ? $response_params[$prefix . 'officer_mobile'] : '','placeholder' =>'015xxxxxxxx,015xxxxxxxx','maxLength' => '100'  )); ?>
            <span class="help-block font-red">একাধিক মোবাইল নাম্বার হলে, নাম্বারসমূহের মধ্যে কমা দেওয়া আবশ্যক (সর্বোচ্চ ৫টি নাম্বার)</span>
        </div>
        <div class="col-md-6 col-lg-6 col-sm-6 form-group">
            <label class="control-label"> এস এম এস বার্তা </label>
            <?php echo $this->Form->input($prefix . 'sms_message', array('label' => false, 'class' => 'form-control input-sm', 'value' => (isset($response_params[$prefix . 'sms_message' ])) ? $response_params[$prefix . 'sms_message'] : '','placeholder' =>'সর্বোচ্চ ১০০ অক্ষর' ,'maxLength' => 100, 'type' => 'textarea' )); ?>
            <span class="help-block font-red">বার্তাটি লিখুন। </span>
        </div>
    </div>

    <?php endif;?>
</div>