<?php
$session = $this->request->session();
$prefix = 'officeselection'.$prefix;
$prefixMain = $prefix;
?>
<div class="row">
    <div class="col-md-12">
        <?= $officeSelectionCell = $this->cell('OfficeSelection', ['entity' => '', 'prefix' => $prefix]) ?>
        <div class="row">
            <div class="col-md-4 form-group form-horizontal">
                <label class="control-label"> কার্যালয় </label>
                <?php echo $this->Form->input($prefix . 'office_id', array('empty' => '--বাছাই করুন--', 'type' => 'select', 'label' => false, 'class' => 'form-control input-sm')); ?>
            </div>
            <div class="col-md-4 form-group form-horizontal">
                <label class="control-label"> অফিসার  </label>
                <?php echo $this->Form->input($prefix . 'officer_name', array('label' => false, 'class' => 'form-control input-sm', 'readonly' => true)); ?>
                <?php echo $this->Form->hidden($prefix . 'officer_id', array('id' => $prefix . 'officer_id')); ?>
                <?php echo $this->Form->hidden($prefix . 'office_head', array('id' => $prefix . 'office_head')); ?>
                <?php echo $this->Form->hidden($prefix . 'incharge_label', array('id' => $prefix . 'incharge_label')); ?>

                <?php echo $this->Form->hidden($prefix . 'office_name_eng', array('id' => $prefix . 'office_name_eng')); ?>
                <?php echo $this->Form->hidden($prefix . 'office_name_bng', array('id' => $prefix . 'office_name_bng')); ?>
                <?php echo $this->Form->hidden($prefix . 'office_address', array('id' => $prefix . 'office_address')); ?>
                <?php echo $this->Form->hidden($prefix . 'office_id', array('id' => $prefix . 'office_id')); ?>

                <?php echo $this->Form->hidden($prefix . 'unit_name_eng', array('id' => $prefix . 'unit_name_eng')); ?>
                <?php echo $this->Form->hidden($prefix . 'unit_name_bng', array('id' => $prefix . 'unit_name_bng')); ?>
                <?php echo $this->Form->hidden($prefix . 'unit_id', array('id' => $prefix . 'unit_id')); ?>
                
                <?php echo $this->Form->hidden($prefix . 'designation_name_eng', array('id' => $prefix . 'designation_name_eng')); ?>
                <?php echo $this->Form->hidden($prefix . 'designation_name_bng', array('id' => $prefix . 'designation_name_bng')); ?>
                <?php echo $this->Form->hidden($prefix . 'designation_id', array('id' => $prefix . 'designation_id')); ?>

                <?php echo $this->Form->hidden($prefix . 'name_eng', array('id' => $prefix . 'name_eng')); ?>
                <?php echo $this->Form->hidden($prefix . 'name_bng', array('id' => $prefix . 'name_bng')); ?>
                <?php echo $this->Form->hidden($prefix . 'incharge_label_eng', array('id' => $prefix . 'incharge_label_eng')); ?>
                
                <?php echo $this->Form->hidden($prefix . 'personal_mobile', array('id' => $prefix . 'personal_mobile')); ?>
                
            </div>
            <?php if($prefix == 'sovapoti_'): ?>
            <div class="col-md-4 form-group form-horizontal">
                <br/>
                <label class="control-label"> নিজ অফিস </label>
                <?php echo $this->Form->checkbox($prefix . 'own_office',['id'=>$prefix . 'own_office']); ?>
            </div>
            <?php endif; ?>
        </div>
    </div>
</div>
<div class="font-red text-center"> **পত্রটি নির্বাচিত অফিসটির ফ্রন্ট-ডেস্কে যাবে। </div>

    <script src="<?php echo CDN_PATH; ?>assets/admin/layout4/scripts/projapoti_ajax.js?v=<?= js_css_version ?>" type="text/javascript"></script>
<script>

    (function($) {
        var prefix = '<?php echo $prefix;?>';
        var prefixmain = '<?php echo $prefixMain;?>';
        var office_id = '<?php echo isset($office_id)?$office_id:0;?>';

        prefix = prefix.split("_");
        prefix = prefix[0] + '-';
        $("#" + prefix + "office-ministry-id").bind('change', function () {
            OfficeSelectionCell.loadMinistryWiseLayers(prefix);
        });
        $("#" + prefix + "office-layer-id").bind('change', function () {
            OfficeSelectionCell.loadMinistryAndLayerWiseOfficeOrigin(prefix);
        });
        $("#" + prefix + "office-origin-id").bind('change', function () {
            OfficeSelectionCell.loadOriginOffices(prefix);
        });
        $("#" + prefix + "office-id").bind('change', function () {
            OfficeSelectionCell.loadOfficeFrontDesk(prefix,prefixmain);
        });

        if(office_id>0 && (prefixmain == 'sender_' || prefixmain == 'approval_')){
            $("#" + prefix + "office-id").val(office_id);
            $("#" + prefix + "office-ministry-id").closest('.row').hide();

            PROJAPOTI.projapoti_dropdown_map("#" + prefix + "office-id", <?php echo $officeArray; ?>, '',office_id);
            $("#" + prefix + "office-unit-id").html('');
            PROJAPOTI.ajaxSubmitDataCallback('<?php echo $this->request->webroot ?>' + 'officeManagement/loadOfficeUnits',
                {'office_id': office_id}, 'html',
                function (response) {
                    $("#" + prefix + "office-unit-id").html(response);
                });
        }

        $(document).off('click',"#<?= $prefix ?>own_office").on('click',"#"+prefix+"own_office",function(){
            alert('ok');
            if(this.checked){
                if(office_id>0){
                    $("#" + prefix + "office-id").val(office_id);
                    $("#" + prefix + "office-ministry-id").closest('.row').hide();

                    PROJAPOTI.projapoti_dropdown_map("#" + prefix + "office-id", <?php echo $officeArray; ?>, '',office_id);
                    $("#" + prefix + "office-unit-id").html('');
                    PROJAPOTI.ajaxSubmitDataCallback('<?php echo $this->request->webroot ?>' + 'officeManagement/loadOfficeUnits',
                        {'office_id': office_id}, 'html',
                        function (response) {
                            $("#" + prefix + "office-unit-id").html(response);
                    });
                }
            }else{
                $("#" + prefix + "office-id").val(0);
                $("#" + prefix + "office-ministry-id").closest('.row').show();
                $("#" + prefix + "office-office-id").html('');
                $("#" + prefix + "office-unit-id").html('');
            }
        });

    }(jQuery));

</script>
<script type="text/javascript">
    var OfficeSelectionCell = {

        loadMinistryWiseLayers: function (prefix) {
            PROJAPOTI.ajaxSubmitDataCallback('<?php echo $this->request->webroot ?>' + 'officeSettings/loadLayersByMinistry',
                {'office_ministry_id': $("#" + prefix + "office-ministry-id").val()}, 'json',
                function (response) {
                    PROJAPOTI.projapoti_dropdown_map("#" + prefix + "office-layer-id", response, "--বাছাই করুন--");
                });
        },
        loadMinistryAndLayerWiseOfficeOrigin: function (prefix) {
            PROJAPOTI.ajaxSubmitDataCallback('<?php echo $this->request->webroot ?>' + 'officeSettings/loadOfficesByMinistryAndLayer',
                {
                    'office_ministry_id': $("#" + prefix + "office-ministry-id").val(),
                    'office_layer_id': $("#" + prefix + "office-layer-id").val()
                }, 'json',
                function (response) {
                    PROJAPOTI.projapoti_dropdown_map("#" + prefix + "office-origin-id", response, "--বাছাই করুন--");
                });
        },
        loadOriginOffices: function (prefix) {
            PROJAPOTI.ajaxSubmitDataCallback('<?php echo $this->request->webroot ?>' + 'officeManagement/loadOriginOffices',
                {'office_origin_id': $("#" + prefix + "office-origin-id").val()}, 'json',
                function (response) {
                    PROJAPOTI.projapoti_dropdown_map("#" + prefix + "office-id", response, "--বাছাই করুন--");
                });
        },
        loadOfficeUnits: function (prefix) {
            PROJAPOTI.ajaxSubmitDataCallback('<?php echo $this->request->webroot ?>' + 'officeManagement/loadOfficeUnits',
                {'office_id': $("#" + prefix + "office-id").val()}, 'html',
                function (response) {
                    $("#" + prefix + "office-unit-id").html(response);
                });
        },
        loadOfficeUnitDesignations: function (prefix) {
            PROJAPOTI.ajaxSubmitDataCallback('<?php echo $this->request->webroot ?>' + 'officeManagement/loadOfficeUnitOrganogramsWithName',
                {'office_unit_id': $("#" + prefix + "office-unit-id").val()}, 'json',
                function (response) {
//                    PROJAPOTI.projapoti_dropdown_map("#" + prefix + "office-unit-organogram-id", response, "--বাছাই করুন--");
                    PROJAPOTI.projapoti_dropdown_map_with_title("#" + prefix + "office-unit-organogram-id", response, "--বাছাই করুন--",'',{'key' : 'id','value' : 'designation','title' : 'name'  });

                });

        },
        getOfficerInfo: function (designation_id, prefix, prefixmain) {
            PROJAPOTI.ajaxSubmitDataCallback('<?php echo $this->request->webroot ?>' + 'employeeRecords/getOfficerInfo',
                {'designation_id': designation_id}, 'json',
                function (response) {
                    $("input[name=" + prefixmain + "officer_id]").val(response.id);
                    $("input[name=" + prefixmain + "officer_name]").val(response.name_bng);
                    $("input[name=" + prefixmain + "office_head]").val(response.office_head);
                    $("input[name=" + prefixmain + "incharge_label]").val(response.incharge_label);
                    $("input[name=" + prefixmain + "office_name_eng]").val(response.office_name_eng);
                    $("input[name=" + prefixmain + "unit_name_eng]").val(response.unit_name_eng);
                    $("input[name=" + prefixmain + "designation_name_eng]").val(response.designation_name_eng);
                    $("input[name=" + prefixmain + "name_eng").val(response.name_eng);
                    $("input[name=" + prefixmain + "incharge_label_eng").val(response.incharge_label_eng);
                    $("input[name=" + prefixmain + "personal_mobile").val(response.personal_mobile);
                }
            );
        },
        loadOfficeFrontDesk: function (prefix,prefixmain) {
            if(isEmpty($("#" + prefix + "office-id").val())){
                return;
            }
            PROJAPOTI.ajaxSubmitDataCallback('<?php echo $this->request->webroot ?>' + 'ApiManagement/apiGetOfficeFronDesk',
                {'office_id': $("#" + prefix + "office-id").val()}, 'json',
                function (response) {
                    if(response.status == 'success' && !isEmpty(response.data.office_unit_organogram_id)){
                        var data = response.data;
                           $("input[name=" + prefixmain + "office_address").val(data.office_address);
                           $("input[name=" + prefixmain + "office_name_bng").val(data.office_name);
                           $("input[name=" + prefixmain + "office_id").val(data.office_id);
                           $("input[name=" + prefixmain + "unit_name_bng").val(data.office_unit_name);
                           $("input[name=" + prefixmain + "unit_id").val(data.office_unit_id);
                           $("input[name=" + prefixmain + "designation_id").val(data.office_unit_organogram_id);
                           $("input[name=" + prefixmain + "designation_name_bng").val(data.designation_label);
                        OfficeSelectionCell.getOfficerInfo(response.data.office_unit_organogram_id,prefix,prefixmain);
                    }else{
                        toastr.error(' অফিসের ফ্রন্ট-ডেস্ক খুঁজে পাওয়া যায়নি। দয়া করে নির্দিষ্ট অফিসার বাছায় করার জন্য  "অফিসার বাছাই করুন" ট্যাবের সাহায্য নিন। '
                                );
                    }
                });
        },
    };
</script>


