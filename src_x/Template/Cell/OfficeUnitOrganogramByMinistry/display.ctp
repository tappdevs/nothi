<!--<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>-->

<div class="row">
    <div class="col-md-12">
        <?= $officeSelectionCell = $this->cell('OfficeSelectionByMinistry', ['entity' => '', 'prefix' => $prefix]) ?>
        <div class="row">
            <div class="col-md-4 form-group form-horizontal">
                <label class="control-label"> কার্যালয় </label>
                <?php echo $this->Form->input($prefix . 'office_id', array('empty' => '--বাছাই করুন--', 'type' => 'select', 'label' => false, 'class' => 'form-control')); ?>
            </div>
            <div class="col-md-4 form-group form-horizontal">
                <label class="control-label"> দপ্তর/শাখা </label>
                <?php echo $this->Form->input($prefix . 'cell_office_unit_id', array('empty' => '--বাছাই করুন--', 'type' => 'select', 'label' => false, 'class' => 'form-control')); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 otherPermissionList panel-group accordion">
                
            </div>
        </div>

    </div>
</div>
<!--<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>-->


<script>
    var prefix = '<?php echo $prefix;?>';

    $("#" + prefix + "office-ministry-id").bind('change', function () {
        OfficeOrgSelectionCell.loadMinistryWiseLayers(prefix);
    });
    $("#" + prefix + "office-layer-id").bind('change', function () {
        OfficeOrgSelectionCell.loadMinistryAndLayerWiseOfficeOrigin(prefix);
    });
    $("#" + prefix + "office-origin-id").bind('change', function () {
        OfficeOrgSelectionCell.loadOriginOffices(prefix);
    });
    $("#" + prefix + "office-id").bind('change', function () {
        OfficeOrgSelectionCell.loadOfficeUnits(prefix);
    });
    $("#" + prefix + "cell-office-unit-id").bind('change', function () {
        OfficeOrgSelectionCell.loadPermissionList(prefix);
    });

</script>
<script type="text/javascript">
    var OfficeOrgSelectionCell = {

        loadMinistryWiseLayers: function () {
            PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + 'officeSettings/loadLayersByMinistry',
                {'office_ministry_id': $("#" + prefix + "office-ministry-id").val()}, 'json',
                function (response) {
                    PROJAPOTI.projapoti_dropdown_map("#" + prefix + "office-layer-id", response, "--বাছাই করুন--");
                });
        },
        loadMinistryAndLayerWiseOfficeOrigin: function () {
            PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + 'officeSettings/loadOfficesByMinistryAndLayer',
                {
                    'office_ministry_id': $("#" + prefix + "office-ministry-id").val(),
                    'office_layer_id': $("#" + prefix + "office-layer-id").val()
                }, 'json',
                function (response) {
                    PROJAPOTI.projapoti_dropdown_map("#" + prefix + "office-origin-id", response, "--বাছাই করুন--");
                });
        },
        loadOriginOffices: function () {
            PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + 'officeManagement/loadOriginOffices',
                {'office_origin_id': $("#" + prefix + "office-origin-id").val()}, 'json',
                function (response) {
                    PROJAPOTI.projapoti_dropdown_map("#" + prefix + "office-id", response, "--বাছাই করুন--");
                });
        },
        loadOfficeUnits: function () {
            PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + 'officeManagement/loadOfficeUnits',
                {'office_id': $("#" + prefix + "office-id").val()}, 'html',
                function (response) {
                    $("#" + prefix + "cell-office-unit-id").html(response);
//                    PROJAPOTI.projapoti_dropdown_map("#" + prefix + "cell-office-unit-id", response, "--বাছাই করুন--");
                    PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + 'nothiMasters/getOtherOfficePermissionList',
                        {'office_id': $("#" + prefix + "office-id").val(),'nothi_id':<?php echo $id ?>,'nothi_office':<?= $nothi_office?>,'api' : '<?= $api ?>'}, 'html',
                        function (response) {
                            $('.otherPermissionList').html(response);
                            $.each($('.otherPermissionList').find('.optionsothers'), function (i, v) {
                                if ($('#otherOfficeSelected').find('tbody').find('.other_office_permission[data-office-unit-organogram-id='+$(v).data('office-unit-organogram-id')+']').length >0) {
                                    $(v).attr('checked', true);
                                }
                            })
                        });
                });



        },
        loadPermissionList: function () {
            PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + 'nothiMasters/getOtherOfficePermissionList',
                {'office_id': $("#" + prefix + "office-id").val(),'office_unit_id':  $("#" + prefix + "cell-office-unit-id").val(),'nothi_id':<?php echo $id ?>,'nothi_office':<?= $nothi_office?>,'api' : '<?= $api ?>'}, 'html',
                function (response) {
                    $('.otherPermissionList').html(response);
                    $.each($('.otherPermissionList').find('.optionsothers'), function (i, v) {
                        if ($('#otherOfficeSelected').find('tbody').find('.other_office_permission[data-office-unit-organogram-id='+$(v).data('office-unit-organogram-id')+']').length >0) {
                            $(v).attr('checked', true);
                        }
                    })
//                    initTable2();
//                    $('.dataTables_filter').css('float','left');
                });
        }
    };
    $(document).ready(function() {
        $("select").select2();
  });
</script>


