<div class="portlet light">
    <div class="portlet-window-body">
        <input type="hidden" id="office-id" value="<?php echo $office_id ?>"/>

        <div class="row" id="tree_view_div">
            <div class="col-md-6">
                <div class="portlet light">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class=""></i>Office Organogram Tree
                        </div>
                    </div>
                    <div class="portlet-window-body">
                        <div id="content_tree"></div>
                    </div>
                </div>
            </div>
            <div id="content_tree_expand_view" class="col-md-6"></div>
        </div>
    </div>
</div>

<!-- Start : Office Setup JS -->

<script type="text/javascript">
    $(function () {
        OfficeUnitOrgView.init();
    });
</script>
<script type="text/javascript">
    var OfficeUnitOrgView = {
        selected_node: "",
        parent_id: 0,
        parent_node: "",
        selected_ministry_id: 0,
        selected_layer_id: 0,
        selected_office_id: 0,

        unitTreeView: function () {
            OfficeUnitOrgView.selected_office_id = $("#office-id").val();

            $('#content_tree').jstree("refresh");
            $('#content_tree').jstree({
                "core": {
                    "themes": {
                        "variant": "large"
                    },
                    'data': {
                        'url': function (node) {
                            return node.id === '#' ?
                                'loadOfficeUnitOrganogramsTree' : 'loadOfficeUnitOrganogramsTree';
                        },
                        'data': function (node) {
                            return {'id': node.id, 'type': "123", 'office_id': $("#office-id").val()};
                        }
                    }
                }
            });
        },

        init: function () {
            OfficeUnitOrgView.unitTreeView();
        }
    };

</script>

<!-- End : Office Setup JS -->