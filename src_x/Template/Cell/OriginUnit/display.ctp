<div class="form-group">
    <label class="col-sm-5 control-label">Superior Unit (Origin)</label>

    <div class="col-sm-7">
        <?php
        echo $this->Form->hidden('office_origin_superior_unit_id',
            array('id' => 'office-origin-superior-unit-id',
                'value' => isset($data_entity->office_origin_unit_id) ? $data_entity->office_origin_unit_id : ""));
        echo $this->Form->input('office_origin_unit', array('list' => 'office_origin_units',
            'label' => false,
            'type' => 'text',
            'class' => 'form-control input-sm'
        ));
        ?>
        <datalist id="office_origin_units">
            <?php foreach ($officeOriginUnits as $unit) {
                echo '<option value="' . $unit . '"></option>';
            } ?>
        </datalist>
    </div>
</div>

<script type="text/javascript">
    var office_origin_unit_data = <?php echo json_encode($officeOriginUnits);?>;
    var datalist = document.getElementById("office_origin_units");
    var input = document.getElementById("office-origin-unit");
    var has_data = false;

    for (var i in office_origin_unit_data) {
        if (office_origin_unit_data[i] == input.value) {
            has_data = true;
            document.getElementById("office-origin-superior-unit-id").value = i;
            loadOriginUnitStuffs(document.getElementById("office-origin-superior-unit-id").value);
            break;
        }
    }

    if (document.getElementById("office-origin-superior-unit-id").value != "") {
        document.getElementById("office-origin-unit").value = office_origin_unit_data[document.getElementById("office-origin-superior-unit-id").value];
    }

    input.addEventListener("blur",
        function (event) {
            for (var i in office_origin_unit_data) {
                if (office_origin_unit_data[i] == input.value) {
                    has_data = true;
                    document.getElementById("office-origin-superior-unit-id").value = i;
                    loadOriginUnitStuffs(document.getElementById("office-origin-superior-unit-id").value);
                    break;
                }
            }
            if (has_data == false) {
                document.getElementById("office-origin-superior-unit-id").value = "";
                document.getElementById("office-origin-unit").value = "";
                alert("Invalid Superior Unit.");
            }
        },
        false);

    function loadOriginUnitStuffs(origin_unit_id) {
        PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + 'officeOrigin/loadOriginUnitStuffs',
            {'origin_unit_id': origin_unit_id}, 'json',
            function (response) {
                PROJAPOTI.projapoti_dropdown_map("#superior-designation-id", response, "--বাছাই করুন--");
            });
    }
    $(function () {
        loadOriginUnitStuffs(document.getElementById("office-origin-superior-unit-id").value);
    });
</script>
