<style>
    .tt-menu{
        min-height: 300rem;
        overflow-y: auto;
        background: red;
    }
</style>
<div class="row">
    <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 sender_list mega-menu-dropdown mega-menu-full">
                <div class="row form-group">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <a href="javascript:;" title="স্বাক্ষরকারী" class="btn btn-primary potrojariOptions"
                           data-toggle="collapse" aria-expanded="true" data-target="#sender_tab"
                           style="min-width: 100%;">
                            অনুমোদনকারী<i class="glyphicon glyphicon-chevron-down pull-right"></i>
                        </a>
                        <div class="collapse"
                             style="width: 700px;z-index: 2;position: absolute;background-color: white;box-shadow: rgb(204, 204, 204) 1px 6px 18px 7px;"
                             id="sender_tab">
                            <?php
                            echo $this->cell('OfficeSearch::customRm',
                                ['prefix' => 'sender_',isset($office_id)?$office_id:0,isset($potro_type)?$potro_type:21]);
                            ?>
                        </div>

                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">

                        <ul class="list-inline sidebar-tags senderul" id="sender_ul">
                            <?php
                            if (!empty($potrojari) && !empty($potrojari->officer_designation_id)) {
                                ?>
                                <li>
                                    <a class="sender_users" href="javascript:;" data-toggle="tooltip"
                                       title="<?php echo $potrojari->officer_designation_label ?>"
                                       data-title="<?php echo $potrojari->officer_designation_label ?>"
                                       designation="<?php echo $potrojari->officer_designation_label ?>"
                                       designation_id="<?php echo $potrojari->officer_designation_id ?>"
                                       unit_name="<?php echo $potrojari->office_unit_name ?>"
                                       unit_id="<?php echo $potrojari->office_unit_id ?>"
                                       ofc_name="<?php echo $potrojari->office_name ?>"
                                       ofc_id="<?php echo $potrojari->office_id ?>"
                                       officer_id="<?php echo $potrojari->officer_id ?>"
                                       data-original-title="<?php echo $potrojari->officer_designation_label ?>"
                                       visibleName ="<?php echo $potrojari->officer_visibleName ?>"
                                       visibleDesignation ="<?php echo $potrojari->officer_visibleDesignation ?>"
                                       >
                                        <?php
                                        if(!empty($potrojari->officer_name) && $potrojari->officer_name != 'undefined') {
                                            echo h($potrojari->officer_name);
                                            if(!empty($potrojari->office_unit_name) && $potrojari->office_unit_name != 'undefined') {
                                                echo ', '.$potrojari->office_unit_name;
                                            }
                                            if(!empty($potrojari->office_name) && $potrojari->office_name != 'undefined') {
                                                echo ', '.$potrojari->office_name;
                                            }
                                        } else {
                                            echo $potrojari->officer_designation_label;
                                        }
                                        ?>
                                        <i class="fs1 a2i_gn_close2 remove_list"></i></a>
                                </li>
                                <?php
                            }
                            ?>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 approval_list mega-menu-dropdown mega-menu-full">
                <div class="row form-group">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <a href="javascript:;" class="btn btn-success potrojariOptions" data-toggle="collapse"
                           aria-expanded="true" data-target="#approval_tab" style="min-width: 100%;">
                            প্রেরক <i class="glyphicon glyphicon-chevron-down pull-right"></i>
                        </a>
                        <div class="collapse"
                             style="width: 700px;z-index: 2;position: absolute;background-color: white;box-shadow: rgb(204, 204, 204) 1px 6px 18px 7px;"
                             id="approval_tab">
                            <?php
                            echo $this->cell('OfficeSearch::customRm',
                                ['prefix' => 'approval_',isset($office_id)?$office_id:0,isset($potro_type)?$potro_type:21]);
                            ?>
                        </div>

                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">

                        <ul class="list-inline sidebar-tags approvalul" id="approval_ul">
                            <?php
                            if (!empty($potrojari) && !empty($potrojari->approval_officer_designation_id)) {
                                ?>
                                <li>
                                    <a class="approval_users" href="javascript:;" data-toggle="tooltip"
                                       title="<?php echo $potrojari->approval_officer_designation_label ?>"
                                       data-title="<?php echo $potrojari->approval_officer_designation_label ?>"
                                       designation="<?php echo $potrojari->approval_officer_designation_label ?>"
                                       designation_id="<?php echo $potrojari->approval_officer_designation_id ?>"
                                       unit_name="<?php echo $potrojari->approval_office_unit_name ?>"
                                       unit_id="<?php echo $potrojari->approval_office_unit_id ?>"
                                       ofc_name="<?php echo $potrojari->approval_office_name ?>"
                                       ofc_id="<?php echo $potrojari->approval_office_id ?>"
                                       officer_id="<?php echo $potrojari->approval_officer_id ?>"
                                       data-original-title="<?php echo $potrojari->approval_officer_designation_label ?>"
                                        visibleName ="<?php echo $potrojari->approval_visibleName ?>"
                                       visibleDesignation ="<?php echo $potrojari->approval_visibleDesignation ?>"
                                       >
                                        <?php
                                        if(!empty($potrojari->approval_officer_name) && $potrojari->approval_officer_name != 'undefined') {
                                            echo $potrojari->approval_officer_name;
                                            if(!empty($potrojari->approval_office_unit_name) && $potrojari->approval_office_unit_name != 'undefined') {
                                                echo ', '.$potrojari->approval_office_unit_name;
                                            }
                                            if(!empty($potrojari->approval_office_name) && $potrojari->approval_office_name != 'undefined') {
                                                echo ', '.$potrojari->approval_office_name;
                                            }
                                        } else {
                                            echo $potrojari->approval_officer_designation_label;
                                        }
                                        ?>
                                        <i class="fs1 a2i_gn_close2 remove_list"></i></a>
                                </li>
                                <?php
                            }
                            ?>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 receiver_list mega-menu-dropdown mega-menu-full">
                <div class="row form-group">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <a href="javascript:;" class="btn green potrojariOptions" data-toggle="collapse"
                           aria-expanded="true" data-target="#receiver_tab" style="min-width: 100%;">
                            প্রাপক <i class="glyphicon glyphicon-chevron-down pull-right"></i>
                        </a>
                        <div class="collapse"
                             style="width: 700px;z-index: 2;position: absolute;background-color: white;box-shadow: rgb(204, 204, 204) 1px 6px 18px 7px;"
                             id="receiver_tab">
                            <?php
                            echo $this->cell('OfficeSearch::customRm',
                                ['prefix' => 'receiver_',isset($office_id)?$office_id:0,isset($potro_type)?$potro_type:21]);
                            ?>
                        </div>

                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                        <ul class="list-inline sidebar-tags receiverul" id="receiver_ul">
                            <?php
                            if (!empty($receivers)) {
                                ?>

                                <?php
                                $groupid = 0;
                                foreach ($receivers as $key => $rec):
                                    if ($rec['group_id'] > 0) {
                                        if ($groupid == $rec['group_id'])
                                            continue;
                                        $groupid = $rec['group_id'];
                                        ?>
                                        <li>
                                            <a class="receiver_users" href="javascript:;"
                                               group_id="<?= $rec->group_id ?>" group_name="<?= $rec->group_name ?>"
                                               group_member="<?= intval($rec->group_member) ?>"
                                               data-toggle="tooltip" title="<?php echo $rec->group_name ?>"
                                               data-title="<?php echo $rec->group_name ?>" officer_email=""
                                               designation="" designation_id="" unit_name="" unit_id="" ofc_name=""
                                               ofc_id="" officer_id=""
                                               data-original-title="<?php echo $rec->group_name ?>"
                                               group_designation="<?php
                                               echo implode('~',
                                                   $receivergroups[$rec['group_id']])
                                               ?>"><?php echo $rec->group_name ?> <i
                                                        class="fs1 a2i_gn_close2 remove_list"></i></a>
                                        </li>
                                        <?php
                                    } else {
                                        $style2apply = '';
                                        if(!empty($rec->options)){
                                            $receiver_options = json_decode($rec->options,true);
                                        }else{
                                            $receiver_options['is_attention'] = 0 ;//attention
                                            $receiver_options['user_type'] = 2 ;// internal or external
                                            $receiver_options['office_selection'] = 0 ;//from direct office or user specific option
                                            $receiver_options['office_address'] = '' ;//user office address
                                        }
                                        if($receiver_options['user_type'] == 2){
                                            $style2apply .= 'color:#009688';
                                        }else{
                                            $style2apply .= 'color:#2196f3';
                                        }
                                        ?>
                                        <li>
                                            <a class="receiver_users" href="javascript:;" data-toggle="tooltip" style="<?=$style2apply?>"
                                               title="<?php echo $rec->receiving_officer_designation_label ?>"
                                               data-title="<?php echo $rec->receiving_officer_designation_label ?>"
                                               officer_email="<?php echo $rec->receiving_officer_email != 'undefined' ? $rec->receiving_officer_email : '' ?>"
                                               designation="<?php echo $rec->receiving_officer_designation_label ?>"
                                               designation_id="<?php echo $rec->receiving_officer_designation_id ?>"
                                               unit_name="<?php echo $rec->receiving_office_unit_name ?>"
                                               unit_id="<?php echo $rec->receiving_office_unit_id ?>"
                                               ofc_name="<?php echo $rec->receiving_office_name ?>"
                                               ofc_id="<?php echo $rec->receiving_office_id ?>"
                                               officer_id="<?php echo $rec->receiving_officer_id ?>"
                                               office_head="<?php echo $rec->receiving_office_head ?>"
                                               group_member="<?= intval($rec->group_member) ?>"
                                               data-original-title="<?php echo $rec->receiving_officer_designation_label ?>"
                                               visibleName ="<?php echo $rec->visibleName ?>"
                                               officer_name="<?php echo(!empty($rec->receiving_officer_name) && $rec->receiving_officer_name != 'undefined' ? $rec->receiving_officer_name : '') ?>"
                                            personal_mobile="<?php echo(!empty($rec->officer_mobile) && $rec->officer_mobile != 'undefined' ? $rec->officer_mobile : '') ?>"
                                            officer_mobile="<?php echo(!empty($rec->officer_mobile) && $rec->officer_mobile != 'undefined' ? $rec->officer_mobile : '') ?>"
                                               sms_message="<?php echo(!empty($rec->sms_message) && $rec->sms_message != 'undefined' ? $rec->sms_message : '') ?>"
                                               is_attention="<?php echo(!empty($receiver_options['is_attention']) && $receiver_options['is_attention'] != 'undefined' ? $receiver_options['is_attention'] : '') ?>"
                                               user_type="<?php echo(!empty($receiver_options['user_type']) && $receiver_options['user_type'] != 'undefined' ? $receiver_options['user_type'] : '') ?>"
                                               office_selection="<?php echo(!empty($receiver_options['office_selection']) && $receiver_options['office_selection'] != 'undefined' ? $receiver_options['office_selection'] : '') ?>"
                                               office_address="<?php echo(!empty($receiver_options['office_address']) && $receiver_options['office_address'] != 'undefined' ? $receiver_options['office_address'] : '') ?>"
                                               >
                                                <?php
                                                if(!empty($rec->receiving_officer_name) && $rec->receiving_officer_name != 'undefined') {
                                                    echo $rec->receiving_officer_name;
                                                    if(!empty($rec->receiving_office_unit_name) && $rec->receiving_office_unit_name != 'undefined') {
                                                        echo ', '.$rec->receiving_office_unit_name;
                                                    }
                                                    if(!empty($rec->receiving_office_name) && $rec->receiving_office_name != 'undefined') {
                                                        echo ', '.$rec->receiving_office_name;
                                                    }
                                                } else {
                                                    echo $rec->receiving_officer_designation_label;
                                                }
                                                ?>
                                                <i class="fs1 a2i_gn_close2 remove_list"></i></a>
                                        </li>
                                        <?php
                                    }
                                endforeach;
                                ?>

                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
            <!--attention-->
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 attension_list mega-menu-dropdown mega-menu-full attentionbox">
                <div class="row form-group">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <a href="javascript:;" class="btn btn-danger potrojariOptions" data-toggle="collapse"
                           aria-expanded="true" data-target="#attension_tab" style="min-width: 100%;">
                            দৃষ্টি আকর্ষণ
                            <i class="glyphicon glyphicon-chevron-down pull-right"></i>
                        </a>
                        <div class="collapse"
                             style="width: 700px;z-index: 2;position: absolute;background-color: white;box-shadow: rgb(204, 204, 204) 1px 6px 18px 7px;"
                             id="attension_tab">
                            <?php
                            echo $this->cell('OfficeSearch::customRm', ['prefix' => 'attension_',isset($office_id)?$office_id:0,isset($potro_type)?$potro_type:21]);
                            ?>
                        </div>

                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">

                        <ul class="list-inline sidebar-tags attensionul" id="attension_ul">
                            <?php
                            if (!empty($potrojari) && !empty($potrojari->attension_officer_designation_id)) {
                                ?>
                                <li>
                                    <a class="attension_users" href="javascript:;" data-toggle="tooltip"
                                       title="<?php echo $potrojari->attension_officer_designation_label ?>"
                                       data-title="<?php echo $potrojari->attension_officer_designation_label ?>"
                                       designation="<?php echo $potrojari->attension_officer_designation_label ?>"
                                       designation_id="<?php echo $potrojari->attension_officer_designation_id ?>"
                                       unit_name="<?php echo $potrojari->attension_office_unit_name ?>"
                                       unit_id="<?php echo $potrojari->attension_office_unit_id ?>"
                                       ofc_name="<?php echo $potrojari->attension_office_name ?>"
                                       ofc_id="<?php echo $potrojari->attension_office_id ?>"
                                       officer_id="<?php echo $potrojari->attension_officer_id ?>"
                                       data-original-title="<?php echo $potrojari->attension_officer_designation_label ?>">
                                        <?php
                                        if(!empty($potrojari->attension_officer_name) && $potrojari->attension_officer_name != 'undefined') {
                                            echo $potrojari->attension_officer_name;
                                            if(!empty($potrojari->attension_office_unit_name) && $potrojari->attension_office_unit_name != 'undefined') {
                                                echo ', '.$potrojari->attension_office_unit_name;
                                            }
                                            if(!empty($potrojari->attension_office_name) && $potrojari->attension_office_name != 'undefined') {
                                                echo ', '.$potrojari->attension_office_name;
                                            }
                                        } else {
                                            echo $potrojari->attension_officer_designation_label;
                                        }
                                        ?>
                                        <i class="fs1 a2i_gn_close2 remove_list"></i></a>
                                </li>
                                <?php
                            }
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
            <!--attention-->
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 onulipi_list mega-menu-dropdown mega-menu-full">
                <div class="row form-group">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <a href="javascript:;" class="btn purple potrojariOptions" data-toggle="collapse"
                           aria-expanded="true" data-target="#onulipi_tab" style="min-width: 100%;">
                            অনুলিপি <i class="glyphicon glyphicon-chevron-down pull-right"></i>
                        </a>
                        <div class="collapse"
                             style="width: 700px;z-index: 2;position: absolute;background-color: white;box-shadow: rgb(204, 204, 204) 1px 6px 18px 7px;"
                             id="onulipi_tab">
                            <?php
                            echo $this->cell('OfficeSearch::customRm',
                                ['prefix' => 'onulipi_',isset($office_id)?$office_id:0,isset($potro_type)?$potro_type:21]);
                            ?>
                        </div>

                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                        <ul class="list-inline sidebar-tags onulipiul" id="onulipi_ul">
                            <?php if (!empty($onulipis)) { ?>

                                <?php
                                $onulipigroupid = 0;
                                foreach ($onulipis as $key => $rec):
                                    if ($rec['group_id'] > 0) {
                                        if ($onulipigroupid == $rec['group_id'])
                                            continue;
                                        $onulipigroupid = $rec['group_id'];
                                        ?>
                                        <li>
                                            <a class="onulipi_users" href="javascript:;"
                                               group_id="<?= $rec->group_id ?>" group_name="<?= $rec->group_name ?>"
                                               group_member="<?= intval($rec->group_member) ?>"
                                               data-toggle="tooltip" title="<?php echo $rec->group_name ?>"
                                               data-title="<?php echo $rec->group_name ?>" officer_email=""
                                               designation="" designation_id="" unit_name="" unit_id="" ofc_name=""
                                               ofc_id="" officer_id=""
                                               data-original-title="<?php echo $rec->group_name ?>"
                                               group_designation="<?php
                                               echo implode('~',
                                                   $onulipigroups[$rec['group_id']])
                                               ?>"><?php echo $rec->group_name ?> <i
                                                        class="fs1 a2i_gn_close2 remove_list"></i></a>
                                        </li>
                                        <?php
                                    } else {
                                        $style2apply = '';
                                         if(!empty($rec->options)){
                                            $receiver_options = json_decode($rec->options,true);
                                        }else{
                                            $receiver_options['is_attention'] = 0 ;//attention
                                            $receiver_options['user_type'] = 2 ;// internal or external
                                            $receiver_options['office_selection'] = 0 ;//from direct office or user specific option
                                            $receiver_options['office_address'] = '' ;//office address
                                        }
                                        if($receiver_options['user_type'] == 2){
                                             $style2apply .= 'color:#009688';
                                        }else{
                                             $style2apply .= 'color:#2196f3';
                                        }
                                        ?>
                                        <li>
                                            <a class="onulipi_users" href="javascript:;" data-toggle="tooltip" style="<?=$style2apply?>"
                                               title="<?php echo $rec->receiving_officer_designation_label ?>"
                                               data-title="<?php echo $rec->receiving_officer_designation_label ?>"
                                               officer_email="<?php echo $rec->receiving_officer_email != 'undefined' ? $rec->receiving_officer_email : '' ?>"
                                               designation="<?php echo $rec->receiving_officer_designation_label ?>"
                                               designation_id="<?php echo $rec->receiving_officer_designation_id ?>"
                                               unit_name="<?php echo $rec->receiving_office_unit_name ?>"
                                               unit_id="<?php echo $rec->receiving_office_unit_id ?>"
                                               ofc_name="<?php echo $rec->receiving_office_name ?>"
                                               ofc_id="<?php echo $rec->receiving_office_id ?>"
                                               officer_id="<?php echo $rec->receiving_officer_id ?>"
                                               office_head="<?php echo $rec->receiving_office_head ?>"
                                               group_member="<?= intval($rec->group_member) ?>"
                                               data-original-title="<?php echo $rec->receiving_officer_designation_label ?>"
                                               visibleName ="<?php echo $rec->visibleName ?>"
                                            personal_mobile="<?php echo(!empty($rec->officer_mobile) && $rec->officer_mobile != 'undefined' ? $rec->officer_mobile : '') ?>"
                                            officer_name="<?php echo(!empty($rec->receiving_officer_name) && $rec->receiving_officer_name != 'undefined' ? $rec->receiving_officer_name : '') ?>"
                                                officer_mobile="<?php echo(!empty($rec->officer_mobile) && $rec->officer_mobile != 'undefined' ? $rec->officer_mobile : '') ?>"
                                               sms_message="<?php echo(!empty($rec->sms_message) && $rec->sms_message != 'undefined' ? $rec->sms_message : '') ?>"
                                                  is_attention="<?php echo(!empty($receiver_options['is_attention']) && $receiver_options['is_attention'] != 'undefined' ? $receiver_options['is_attention'] : '') ?>"
                                               user_type="<?php echo(!empty($receiver_options['user_type']) && $receiver_options['user_type'] != 'undefined' ? $receiver_options['user_type'] : '') ?>"
                                               office_selection="<?php echo(!empty($receiver_options['office_selection']) && $receiver_options['office_selection'] != 'undefined' ? $receiver_options['office_selection'] : '') ?>"
                                               office_address="<?php echo(!empty($receiver_options['office_address']) && $receiver_options['office_address'] != 'undefined' ? $receiver_options['office_address'] : '') ?>"
                                               >

                                                <?php
                                                if(!empty($rec->receiving_officer_name) && $rec->receiving_officer_name != 'undefined') {
                                                    echo $rec->receiving_officer_name;
                                                    if(!empty($rec->receiving_office_unit_name) && $rec->receiving_office_unit_name != 'undefined') {
                                                        echo ', '.$rec->receiving_office_unit_name;
                                                    }
                                                    if(!empty($rec->receiving_office_name) && $rec->receiving_office_name != 'undefined') {
                                                        echo ', '.$rec->receiving_office_name;
                                                    }
                                                } else {
                                                    echo $rec->receiving_officer_designation_label;
                                                }
                                                ?>
                                                <i class="fs1 a2i_gn_close2 remove_list"></i></a>
                                        </li>
                                        <?php
                                    }
                                endforeach;
                                ?>

                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 sovapoti_list mega-menu-dropdown mega-menu-full sovabox"
                 style="display: none;">
                <div class="row form-group">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <a href="javascript:;" class="btn red potrojariOptions" data-toggle="collapse"
                           aria-expanded="true" data-target="#sovapoti_tab" style="min-width: 100%;">
                            সভাপতি <i class="glyphicon glyphicon-chevron-down pull-right"></i>
                        </a>
                        <div class="collapse" style="width: 700px;box-shadow: rgb(204, 204, 204) 1px 6px 18px 7px;"
                             id="sovapoti_tab">
                            <?php
                            echo $this->cell('OfficeSearch::customRm',
                                ['prefix' => 'sovapoti_',isset($office_id)?$office_id:0,isset($potro_type)?$potro_type:21]);
                            ?>
                        </div>

                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                        <ul class="list-inline sidebar-tags sovapotisomuho">
                            <?php
                            if (!empty($potrojari->sovapoti_officer_designation_label)) {
                                ?>
                                <li>
                                    <a class="sovapoti_users" href="javascript:;" data-toggle="tooltip"
                                       title="<?php echo $potrojari->sovapoti_officer_designation_label ?>"
                                       data-title="<?php echo $potrojari->sovapoti_officer_designation_label ?>"
                                       designation="<?php echo $potrojari->sovapoti_officer_designation_label ?>"
                                       designation_id="<?php echo $potrojari->sovapoti_officer_designation_id ?>"
                                       unit_name="<?php echo $potrojari->sovapoti_office_unit_name ?>"
                                       unit_id="<?php echo $potrojari->sovapoti_office_unit_id ?>"
                                       ofc_name="<?php echo $potrojari->sovapoti_office_name ?>"
                                       ofc_id="<?php echo $potrojari->sovapoti_office_id ?>"
                                       officer_id="<?php echo $potrojari->sovapoti_officer_id ?>"
                                       data-original-title="<?php echo $potrojari->sovapoti_officer_designation_label ?>">
                                        <?php
                                        if(!empty($potrojari->sovapoti_officer_name) && $potrojari->sovapoti_officer_name != 'undefined') {
                                            echo $potrojari->sovapoti_officer_name;
                                            if(!empty($potrojari->sovapoti_office_unit_name) && $potrojari->sovapoti_office_unit_name != 'undefined') {
                                                echo ', '.$potrojari->sovapoti_office_unit_name;
                                            }
                                            if(!empty($potrojari->sovapoti_office_name) && $potrojari->sovapoti_office_name != 'undefined') {
                                                echo ', '.$potrojari->sovapoti_office_name;
                                            }
                                        } else {
                                            echo $potrojari->sovapoti_officer_designation_label;
                                        }
                                        ?>
                                        <i class="fs1 a2i_gn_close2 remove_list"></i></a>
                                </li>
                                <?php
                            }
                            ?>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <?php if (empty($potrojari)) { ?>
                    <div class="row">
                        <div class="col-lg-4 col-md-3 col-sm-3 col-xs-6">
                            পত্রের ধরন 
                        </div>
                        <div class="col-lg-8 col-md-9 col-sm-9 col-xs-6">
                            <?php
                            //                            (!empty($is_endorse) && $is_endorse == 1)?pr(19):pr(0);die;
                            echo $this->Form->input('potro_type',
                                ['class' => 'form-control select', 'type' => 'select',
                                    'empty' => 'পত্রের ধরন বাছাই করুন',
                                    'options' => $template, 'label' => false, 'default' => (!empty($potrojari)
                                    ? $potrojari->potro_type : ((!empty($is_endorse) && $is_endorse == 1) ? 19 : 0))])
                            ?>
                        </div>
                    </div>
                <?php } ?>
                <div class="row">
                    <div class="col-lg-4 col-md-3 col-sm-3 col-xs-6">
                        অগ্রাধিকার 
                    </div>
                    <div class="col-lg-8 col-md-9 col-sm-9 col-xs-6">
                        <?php
                        $dak_priority_levels = json_decode(DAK_PRIORITY_TYPE,
                            true);
                        echo $this->Form->input('potro_priority_level',
                            ['class' => 'form-control select', 'type' => 'select',
                                'options' => $dak_priority_levels,
                                'label' => false, 'default' => (!empty($potrojari) ? $potrojari->potro_priority_level
                                : 0)])
                        ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4 col-md-3 col-sm-3 col-xs-6">
                        গোপনীয়তা
                    </div>
                    <div class="col-lg-8 col-md-9 col-sm-9 col-xs-6">
                        <?php
                        $dak_security_levels = json_decode(DAK_SECRECY_TYPE,
                            true);
                        echo $this->Form->input('potro_security_level',
                            ['class' => 'form-control select', 'type' => 'select',
                                'options' => $dak_security_levels, 'label' => false,
                                'default' => (!empty($potrojari) ? $potrojari->potro_security_level
                                    : 0)])
                        ?>
                    </div>
                </div>
                <div class="row khalipotro" style="display:none;">
                    <div class="col-lg-4 col-md-3 col-sm-3 col-xs-6">
                        স্মারক নম্বর
                    </div>
                    <div class="col-lg-8 col-md-9 col-sm-9 col-xs-6">
                        <?php
                        echo $this->Form->input('blank_sarok',
                            ['class' => 'form-control ', 'type' => 'text', 'label' => false,
                                'default' => (!empty($potrojari) ? $potrojari->sarok_no
                                    : '')])
                        ?>
                    </div>
                </div>
                <div class="row khalipotro" style="display:none;">
                    <div class="col-lg-4 col-md-3 col-sm-3 col-xs-6">
                        বিষয়
                    </div>
                    <div class="col-lg-8 col-md-9 col-sm-9 col-xs-6">
                        <?php
                        echo $this->Form->input('blank_subject',
                            ['class' => 'form-control ', 'type' => 'text', 'label' => false,
                                'default' => (!empty($potrojari) ? $potrojari->potro_subject
                                    : '')])
                        ?>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<a title="প্রিন্ট প্রিভিউ" href="javascript:void(0);"
   class="  btn btn-xs pull-right btn-primary btn-printpreview">
    <i class="fa fa-binoculars"></i> প্রিন্ট প্রিভিউ
</a>


<div class="row" style="clear:both;">
    <div class="col-md-12">
        <div id="template-body" style="max-width: 950px;min-height: 815px;
         height: auto;
         margin: 20px auto;
         padding: 20px;
         display: block;
         background-color: rgb(255, 255, 255);
         border: 1px solid #E6E6E6; <?php
        echo !empty($potrojari) ? "" : "display:none;"
        ?>">
            <?php
            if (!empty($potrojari) && $potrojari->potro_type == 13) {

                echo !empty($templates) ? $templates['html_content'] : '';
            } else {
                echo !empty($potrojari) ? $potrojari->potro_description : '';
            }
            ?>
        </div>
        <?php
        if (!empty($potrojari) && $potrojari->potro_type == 13) {

            echo "<div id='tmpbody' style='display:none'>" . $potrojari->potro_description . '</div>';
        }
        ?>
    </div>
    <?php
    echo $this->Form->hidden('uploaded_attachments',
        ['id' => 'uploaded_attachments']);
    echo $this->Form->hidden('uploaded_attachments_names',
        ['id' => 'uploaded_attachments_names']);
    echo $this->Form->hidden('uploaded_attachments_rm_type',
        ['id' => 'uploaded_attachments_rm_type']);
    ?>
    <?php echo $this->Form->hidden('file_description', ['id' => 'file_description'])
    ?>
    <textarea id="contentbody" name="contentbody" style="display: none"></textarea>
</div>

<script src="<?php echo CDN_PATH; ?>projapoti-nothi/js/rm_file_update.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript"
        src="<?php echo CDN_PATH; ?>assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
<script>

    $(function(){

        var OfficeDataAdapter = new Bloodhound({
            datumTokenizer: function (d) {
                return d.tokens;
            },
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: getBaseUrl() + 'officeManagement/autocompleteOfficeDesignation?search_key=%QUERY&office_id=<?php echo $office_id; ?>'
        });
        OfficeDataAdapter.initialize();

        var OfficeDataAdapterRc = new Bloodhound({
            datumTokenizer: function (d) {
                return d.tokens;
            },
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: getBaseUrl() + 'officeManagement/autocompleteOfficeDesignation?search_key=%QUERY'
        });
        OfficeDataAdapterRc.initialize();

        var GroupDataAdapter = new Bloodhound({
            datumTokenizer: function (d) {
                return d.tokens;
            },
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: getBaseUrl() + 'PotrojariGroups/ajaxGetGroupList?search_key=%QUERY'
        });
        GroupDataAdapter.initialize();

        $('.typeahead_receiver_group')
            .typeahead({
                    hint: (Metronic.isRTL() ? false : true),
                    highlight: true,
                    minLength: 2
                }, {
                    name: 'datypeahead_receiver_group',
                    displayKey: 'value',
                    source: GroupDataAdapter.ttAdapter(),
                    hint: (Metronic.isRTL() ? false : true),
                    templates: {
                        suggestion: Handlebars.compile([
                            '<div class="media" onClick="setGroupInfo({{id}},{{list}},\'receiver_\')">',
                            '<div class="media-body" >',
                            '<h4 style="font-size:12px;" class="media-heading" >{{value}}</h4>',
                            '</div>',
                            '</div>',
                        ].join(''))
                    }
                }
            )
            .on('typeahead:opened', ongroupOpened)
            .on('typeahead:selected', ongroupAutocompleted)
            .on('typeahead:autocompleted', ongroupSelected);

        $('.typeahead_onulipi_group')
            .typeahead({
                    hint: (Metronic.isRTL() ? false : true),
                    highlight: true,
                    minLength: 2
                }, {
                    name: 'datypeahead_onulipi_group',
                    displayKey: 'value',
                    source: GroupDataAdapter.ttAdapter(),
                    hint: (Metronic.isRTL() ? false : true),
                    templates: {
                        suggestion: Handlebars.compile([
                            '<div class="media"  onClick="setGroupInfo({{id}},{{list}},\'onulipi_\')">',
                            '<div class="media-body">',
                            '<h4 style="font-size:12px;" class="media-heading"  >{{value}}</h4>',
                            '</div>',
                            '</div>',
                        ].join(''))
                    }
                }
            )
            .on('typeahead:opened', ongroupOpened)
            .on('typeahead:selected', ongroupAutocompleted)
            .on('typeahead:autocompleted', ongroupSelected);

        function ongroupOpened($e, datum) {

        }

        function ongroupSelected($e, datum) {

        }

        function ongroupAutocompleted($e, datum) {
        }

        $('.typeahead_sender')
            .typeahead({
                    hint: (Metronic.isRTL() ? false : true),
                    highlight: true,
                    minLength: 2
                }, {
                    name: 'datypeahead_sender',
                    displayKey: 'value',
                    source: OfficeDataAdapter.ttAdapter(),
                    hint: (Metronic.isRTL() ? false : true),
                    templates: {
                        suggestion: Handlebars.compile([
                            '<div class="media">',
                            '<div class="media-body">',
                            '<h4 style="font-size:12px;" class="media-heading">{{value}}</h4>',
                            '</div>',
                            '</div>',
                        ].join(''))
                    }
                }
            )
            .on('typeahead:opened', onOpened)
            .on('typeahead:selected', onAutocompleted)
            .on('typeahead:autocompleted', onSelected);

        $('.typeahead_approval')
            .typeahead({
                    hint: (Metronic.isRTL() ? false : true),
                    highlight: true,
                    minLength: 2
                }, {
                    name: 'datypeahead_approval',
                    displayKey: 'value',
                    source: OfficeDataAdapter.ttAdapter(),
                    hint: (Metronic.isRTL() ? false : true),
                    templates: {
                        suggestion: Handlebars.compile([
                            '<div class="media">',
                            '<div class="media-body">',
                            '<h4 style="font-size:12px;" class="media-heading">{{value}}</h4>',
                            '</div>',
                            '</div>',
                        ].join(''))
                    }
                }
            )
            .on('typeahead:opened', onOpened)
            .on('typeahead:selected', onAutocompleted)
            .on('typeahead:autocompleted', onSelected);

        $('.typeahead_receiver')
            .typeahead({
                    hint: (Metronic.isRTL() ? false : true),
                    highlight: true,
                    minLength: 2
                }, {
                    name: 'datypeahead_receiver',
                    displayKey: 'value',
                    source: OfficeDataAdapterRc.ttAdapter(),
                    templates: {
                        suggestion: Handlebars.compile([
                            '<div class="media">',
                            '<div class="media-body">',
                            '<h4 style="font-size:12px;" class="media-heading">{{value}}</h4>',
                            '</div>',
                            '</div>',
                        ].join(''))
                    }
                }
            )
            .on('typeahead:opened', onOpened)
            .on('typeahead:selected', onAutocompleted)
            .on('typeahead:autocompleted', onSelected);

        $('.typeahead_onulipi')
            .typeahead({
                    hint: (Metronic.isRTL() ? false : true),
                    highlight: true,
                    minLength: 2
                }, {
                    name: 'datypeahead_onulipi',
                    displayKey: 'value',
                    source: OfficeDataAdapterRc.ttAdapter(),
                    hint: (Metronic.isRTL() ? false : true),
                    templates: {
                        suggestion: Handlebars.compile([
                            '<div class="media">',
                            '<div class="media-body">',
                            '<h4 style="font-size:12px;" class="media-heading">{{value}}</h4>',
                            '</div>',
                            '</div>',
                        ].join(''))
                    }
                }
            )
            .on('typeahead:opened', onOpened)
            .on('typeahead:selected', onAutocompleted)
            .on('typeahead:autocompleted', onSelected);

        $('.typeahead_sovapoti')
            .typeahead({
                    hint: (Metronic.isRTL() ? false : true),
                    highlight: true,
                    minLength: 2
                }, {
                    name: 'datypeahead_sovapoti',
                    displayKey: 'value',
                    source: OfficeDataAdapterRc.ttAdapter(),
                    hint: (Metronic.isRTL() ? false : true),
                    templates: {
                        suggestion: Handlebars.compile([
                            '<div class="media">',
                            '<div class="media-body">',
                            '<h4 style="font-size:12px;" class="media-heading">{{value}}</h4>',
                            '</div>',
                            '</div>',
                        ].join(''))
                    }
                }
            )
            .on('typeahead:opened', onOpened)
            .on('typeahead:selected', onAutocompleted)
            .on('typeahead:autocompleted', onSelected);

        $('.typeahead_attension')
            .typeahead({
                    hint: (Metronic.isRTL() ? false : true),
                    highlight: true,
                    minLength: 2
                }, {
                    name: 'datypeahead_attension',
                    displayKey: 'value',
                    source: OfficeDataAdapterRc.ttAdapter(),
                    hint: (Metronic.isRTL() ? false : true),
                    templates: {
                        suggestion: Handlebars.compile([
                            '<div class="media">',
                            '<div class="media-body">',
                            '<h4 style="font-size:12px;" class="media-heading">{{value}}</h4>',
                            '</div>',
                            '</div>',
                        ].join(''))
                    }
                }
            )
            .on('typeahead:opened', onOpened)
            .on('typeahead:selected', onAutocompleted)
            .on('typeahead:autocompleted', onSelected);

        function onOpened($e, datum) {

        }

        function onSelected($e, datum) {

        }

        function onAutocompleted($e, datum) {
            DakOffice.setAutoDataIntoHiddenFields(datum, $e.currentTarget.id);
        }

        var list_rec = document.getElementById("receiver_ul");
        Sortable.create(list_rec, {
            animation: 150,
            onUpdate: function (evt) {
                setInformation();
            }
        });

        var list_onu = document.getElementById("onulipi_ul");
        Sortable.create(list_onu, {
            animation: 150,
            onUpdate: function (evt) {
                setInformation();
            }
        });

    })
</script>
