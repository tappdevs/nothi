<?php
$session = $this->request->session();
?>
<!--  -->
<div class="row">
    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-cogs font-green-sharp"></i>
                    <span class="caption-subject font-green-sharp bold uppercase"><?= __("Map employee") ?></span>
                    <span class="caption-helper"><?= __(" " . $prefix) ?>...</span>
                </div>
            </div>
            <div class="portlet-body">
                <?= $officeSelectionCell = $this->cell('OfficeSelection', ['entity' => '', 'prefix' => $prefix]) ?>
                <div class="form-group form-horizontal">
                    <label class="control-label"> কার্যালয় </label>
                    <?php echo $this->Form->input($prefix . 'office_id', array('empty' => '--বাছাই করুন--', 'type' => 'select', 'label' => false, 'class' => 'form-control')); ?>
                </div>
                <div class="form-group form-horizontal">
                    <label class="control-label"> দপ্তর/শাখা </label>
                    <?php echo $this->Form->input($prefix . 'office_unit_id', array('empty' => '--বাছাই করুন--', 'type' => 'select', 'label' => false, 'class' => 'form-control')); ?>
                </div>
                <div class="form-group form-horizontal">
                    <label class="control-label"> পদ </label>
                    <?php echo $this->Form->input($prefix . 'office_unit_organogram_id', array('empty' => '--বাছাই করুন--', 'type' => 'select', 'label' => false, 'class' => 'form-control')); ?>
                    <?php echo $this->Form->hidden($prefix . 'office_unit_organogram', array('id' => $prefix . 'office_unit_organogram')); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<!--  -->



