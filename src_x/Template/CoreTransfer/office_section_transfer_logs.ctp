<link href="<?=$this->request->webroot?>wizard/css/material-bootstrap-wizard.css" rel="stylesheet" />
<link href="<?=$this->request->webroot?>wizard/css/wizard.css" rel="stylesheet" />

<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            অন্যান্য অফিসে শাখা স্থানান্তর লগ
        </div>

    </div>
    <div class="portlet-body">
		<?php if (count($office_segregation_logs) > 0): ?>
		    <table class="table table-bordered">
			    <thead>
			    <tr class="heading">
				    <th class="text-center"> ক্রমিক </th>
				    <th colspan="2" class="text-center"> পূর্বের তথ্য </th>
				    <th colspan="2" class="text-center"> বর্তমান তথ্য </th>
				    <th colspan="2" class="text-center"> পূর্বের ডাক ও নথিসমূহ </th>
				    <th colspan="2" class="text-center"> বর্তমান ডাক ও নথিসমূহ </th>
				    <th class="text-center"> প্রক্রিয়া শুরুর সময়</th>
				    <th class="text-center"> প্রক্রিয়া শেষের সময় </th>
			    </tr>
			    </thead>
			    <tbody>
				<?php
				$count = 0;
				foreach ($office_segregation_logs as $key => $office_segregation_log) {
					$count++;
					?>
				    <tr>
					    <td rowspan="6" class="text-center"><?= $this->Number->format($count) ?> </td>

					    <td>মৌলিক অফিস আইডি</td><td><?= $office_segregation_log['prev_office_origin_id'] ?></td>
					    <td>মৌলিক অফিস আইডি</td><td><?= $office_segregation_log['office_origin_id'] ?></td>

					    <td>আগত ডাক</td><td><?= $office_segregation_log['prev_dak_inbox_count'] ?></td>
					    <td>আগত ডাক</td><td><?= $office_segregation_log['dak_inbox_count'] ?></td>

					    <td rowspan="6" class="text-center"><?= Cake\I18n\Time::parse($office_segregation_log['created']) ?></td>
					    <td rowspan="6" class="text-center"><?= Cake\I18n\Time::parse($office_segregation_log['modified']) ?></td>
				    </tr>

					<tr>
						<td>মৌলিক শাখা আইডি</td><td><?= $office_segregation_log['prev_unit_origin_id'] ?></td>
						<td>মৌলিক শাখা আইডি</td><td><?= $office_segregation_log['unit_origin_id'] ?></td>
						<td>আগত নথি</td><td><?= $office_segregation_log['prev_nothi_inbox_count'] ?></td>
						<td>আগত নথি</td><td><?= $office_segregation_log['nothi_inbox_count'] ?></td>
					</tr>
					<tr>
						<td>মৌলিক পদবি আইডি</td><td><?= $office_segregation_log['prev_organogram_origin_id'] ?></td>
						<td>মৌলিক পদবি আইডি</td><td><?= $office_segregation_log['organogram_origin_id'] ?></td>
						<td>প্রেরিত নথি</td><td><?= $office_segregation_log['prev_nothi_sent_count'] ?></td>
						<td>প্রেরিত নথি</td><td><?= $office_segregation_log['nothi_sent_count'] ?></td>
					</tr>
					<tr>
						<td>অফিস আইডি</td><td><?= $office_segregation_log['prev_office_id'] ?></td>
						<td>অফিস আইডি</td><td><?= $office_segregation_log['office_id'] ?></td>
						<td>অন্য অফিস প্রেরিত নথি</td><td><?= $office_segregation_log['prev_nothi_other_sent_count'] ?></td>
						<td>অন্য অফিস প্রেরিত নথি</td><td><?= $office_segregation_log['nothi_other_sent_count'] ?></td>
					</tr>
					<tr>
						<td>শাখা আইডি</td><td><?= $office_segregation_log['prev_unit_id'] ?></td>
						<td>শাখা আইডি</td><td><?= $office_segregation_log['unit_id'] ?></td>
						<td>পত্র </td><td><?= $office_segregation_log['prev_potro_count'] ?></td>
						<td>পত্র </td><td><?= $office_segregation_log['potro_count'] ?></td>
					</tr>
					<tr>
						<td>পদবি আইডি</td><td><?= $office_segregation_log['prev_organogram_id'] ?></td>
						<td>পদবি আইডি</td><td><?= $office_segregation_log['organogram_id'] ?></td>
						<td></td><td></td>
						<td></td><td></td>
					</tr>
					<?php
				}
				?>
			    </tbody>
		    </table>
			<?=customPagination($this->Paginator)?>
		<?php else: ?>
		    <h3>কোনো তথ্য পাওয়া যায় নি</h3>
		<?php endif; ?>
    </div>
</div>

<script>
    $('body').addClass('page-sidebar-closed');
    $('.page-sidebar-menu').addClass('page-sidebar-menu-closed');
</script>