<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
            Office Archive Update
        </div>
    </div>
    <div class="portlet-body">
        <div class="text-center">
            <div class="easy-pie-chart">
                <div class="number transactions" data-percent="0">
                    <span>0</span>%
                </div>
                <label class="badge badge-info totalCount">Total: <?= count($all_offices); ?></label>&nbsp;&nbsp;<label class="badge badge-success doneCount">Completed: 0</label>&nbsp;&nbsp;<label class="badge badge-danger errorCount">Errors: 0</label>
            </div>
        </div>
        <div class="table-scrollable">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr class="text-center heading">
                        <th class="text-center"><?= __('Sequence') ?> </th>
                        <th class="text-center"><?= __('Number') ?> </th>
                        <th class="text-center"><?= __('Name') ?></th>
                        <th class="text-center"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (!empty($all_offices)):
                        $i = 0;
                        foreach ($all_offices as $key => $val):
                            $i++;
                            ?>
                            <tr class="text-center">
                                <td> <?= $this->Number->format($i) ?> </td>
                                <td> <?= $this->Number->format($val['office_id']) ?> </td>
                                <td> <?= $val['office_name'] ?>  </td>
                                <td class="offices" id="<?= $val['office_id'] ?>">  </td>
                            </tr>
                            <?php
                        endforeach;
                    endif;
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script src="<?= CDN_PATH ?>assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script>
<script>
    offices_ids = [];
    error = 0;

    $(document).ready(function () {
        if (jQuery().easyPieChart) {
            $('.easy-pie-chart .number.transactions').easyPieChart({
                animate: 1000,
                size: 75,
                lineWidth: 3,
                barColor: Metronic.getBrandColor('green')
            });
        }
        $.each($('.offices'),function (index,val) {
           offices_ids.push(val.id);
        });
       updateOffices(offices_ids[0],0);
    });
function updateOffices(office_id,count){
    if(count >= offices_ids.length){
        return;
    }
     $('#'+office_id).html('<span><i class="fa fa-spin fa-refresh" aria-hidden="true"></i>    </span>');
          $.ajax({
        type: 'POST',
        url: "<?php echo $this->Url->build(['controller' => 'Cron', 'action' => 'checkArchive']) ?>",
        data: {"office_id": office_id},
        success: function (data) {
            if(data.status == 'success'){
                 $('.doneCount').text("Completed: " + (count+1));
                   $('#'+office_id).html('<span><i class="fa fa-check" aria-hidden="true"></i>   <b> ' + data.msg + ' </b></span>');
            $('.easy-pie-chart .number').each(function () {
                var newValue = Math.floor(100 * ((count+1) / offices_ids.length));
                $(this).data('easyPieChart').update(newValue);
                $('span', this).text(newValue);
            });
            updateOffices(offices_ids[count+1],count+1);
            }
          else{
               $('.errorCount').text("Errors: " + (++error));
               $('#'+office_id).html('<span><i class="fa fa-times" aria-hidden="true"></i>   <b> ' + data.msg + ' </b></span>');
                $('.doneCount').text("Completed: " + (count+1));

                $('.easy-pie-chart .number').each(function () {
                    var newValue = Math.floor(100 * ((count+1) / offices_ids.length));
                    $(this).data('easyPieChart').update(newValue);
                    $('span', this).text(newValue);
                });
                 updateOffices(offices_ids[count+1],count+1);
          }
        },
    });

}
</script>

