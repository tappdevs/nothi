<div class="portlet light">
    <div class="portlet-title hidden-print">
        <div class="caption"><i class="fs1 a2i_gn_details1"></i> ক্রন লগ রিপোর্ট বিস্তারিত বিবরণ</div>
    </div>
    <div class="portlet-body">
        <h4 class="text-center"><u> <?php echo (!empty($allData['cron_name'])? $allData['cron_name']:''). (!empty($allData['operation_date'])? '( '.$allData['operation_date']->format('Y-m-d')  .' )' :'') ?> </u></h4>
        <?php
        if(!empty($allData['details'])){
            echo json_decode($allData['details']);
        }else{
            echo ' কোন তথ্য পাওয়া যায়নি। ';
        }
        ?>
    </div>
</div>
