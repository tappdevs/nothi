<div id="table">
            <div class="portlet light">
                <div class="portlet-body inbox-content">
                    <div class="table-scrollable ">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr class="heading">
                                    <th class="text-center" >ক্রন </th>
                                    <th class="text-center" >তারিখ </th>
                                    <th class="text-center" >সময়কাল </th>
                                    <th class="text-center" >সর্বমোট </th>
                                    <th class="text-center" >সফল হয়েছে </th>
                                    <th class="text-center" >ব্যর্থ হয়েছে </th>
                                    <th class="text-center" > </th>

                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (!empty($allData)) {
                                    foreach ($allData as $v) {
                                        ?>
                                        <tr>
                                            <td class="text-center"><?= $v['cron_name'] ?></td>
                                            <td class="text-center"><?=$v['operation_date']->format('Y-m-d') ?></td>
                                            <td class="text-center"><?= date('H:i',$v['start_time']) .'-'. date('H:i',$v['end_time']) ?></td>
                                            <td class="text-center"><?= $v['total_request'] ?></td>
                                            <td class="text-center"><?= $v['success'] ?></td>
                                            <td class="text-center"><?= $v['failed'] ?></td>
                                            <td class="text-center"><a href="cronLogDetails/<?= $v['id'] ?>" target="_blank" class="btn btn-link btn-primary">বিস্তারিত</a></td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>


                    </div>
                </div>
            </div>

            <!--          <div class="row">
                        <div class="col-md-12">
                            <a class="btn pull-right btn-sm blue hidden-print margin-bottom-5" onclick="javascript:window.print();">
            <?php // echo __("Print") ?> <i class="fs1 a2i_gn_print2"></i> </a>
                        </div>
                    </div>-->
        </div>
<div class="actions text-center">
    <ul class="pagination pagination-sm">
        <?php
        echo $this->Paginator->first(__('প্রথম', true), array('class' => 'number-first'));
        echo $this->Paginator->prev('<<', array('tag' => 'li', 'escape' => false), '<a href="#" class="btn btn-sm blue">&laquo;</a>', array('class' => 'prev disabled btn btn-sm blue', 'tag' => 'li', 'escape' => false));
        echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a', 'reverse' => FALSE));
        echo $this->Paginator->next('>>', array('tag' => 'li', 'escape' => false), '<a href="#" class="btn btn-sm blue ">&raquo;</a>', array('class' => 'next disabled', 'tag' => 'li', 'escape' => false));
        echo $this->Paginator->last(__('শেষ', true), array('class' => 'number-last'));
        ?>
    </ul>

</div>