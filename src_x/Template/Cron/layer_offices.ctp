<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
            স্থর ভিত্তিক অফিস তালিকা
        </div>
    </div>
    <div class="portlet-body">
        <div class="row">
            <div class="col-md-4 form-group form-horizontal">
                <?php
                echo $this->Form->input('office_origin_id',
                    array('empty' => '--বাছাই করুন--', 'type' => 'select', 'label' => false, 'class' => 'form-control',
                    'options' => $options));
                ?>
                <?php
                echo $this->Form->create('', ['class' => 'searchForm'])
                ?>
                <?php echo $this->Form->end() ?>
            </div>

        </div>
        <br>
        <br>
        <div class="row" id="showlist">

        </div>
        <div class="row">
            <div class="table-container ">
                <table class="table table-bordered table-hover tableadvance">
                    <thead>
                        <tr class="heading">
                            <th class="text-center" >অফিসের আইডি </th>
                            <th class="text-center" > অফিসের নাম </th>
                            <th class="text-center" > কার্যক্রম </th>
                        </tr>
                    </thead>
                    <tbody id ="addData">

                    </tbody>
                </table>
            </div>

            <!--Tools end -->

        </div>
        <br/>
        <div class="inbox-content">

        </div>
    </div>
</div>
<script src="<?php echo CDN_PATH; ?>daptorik_preview/js/report_date_range.js"></script>
<script src="<?php echo CDN_PATH; ?>js/reports/offices_reports.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/floatthead/2.0.3/jquery.floatThead.min.js"></script>
<script>
        jQuery(document).ready(function () {
              $('.table').floatThead({
	position: 'absolute',
                    top: jQuery("div.navbar-fixed-top").height()
            });
            $('.table').floatThead('reflow');
        });

    jQuery(document).ready(function () {
        $(this).find('body').addClass('page-sidebar-closed');
        $(this).find('.page-sidebar-menu').addClass('page-sidebar-menu-closed');

        $("#office-origin-id").bind('change', function () {
            if(!($(this).val())==''){
            getOfficesId($(this).val());
            }
            $('#addData').html('');

        });
        function getOfficesId(office_layer_id) {
            $('#showlist').html('<img src="<?= CDN_PATH ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp; লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');
            $.ajax({
                type: 'POST',
                url: "<?php echo $this->Url->build(['controller' => 'cron', 'action' => 'getLayerwiseOffices']) ?>",
                dataType: 'json',
                data: {"office_layer_type": office_layer_id},
                success: function (data) {
                    if(!(isEmpty(data))){

                    $.each(data, function (i, v) {
                       var edit_url = "<?php echo $this->Url->build(['controller' => 'cron', 'action' => 'layerOfficesLayerEdit']) ?>"+'/'+i;
                        toAdd = '<tr>' +
                            '<td class="text-center" >' + BntoEng(i) + '</td> ' +
                            '<td class="text-center"> <b>' + v + '</b></td>' +
                            '<td class="text-center"><a href="'+edit_url+'" class="btn btn-xs btn-primary">সম্পাদনা</a></td>' +
                            '</tr>';
                        $('#addData').append(toAdd);

                    });
                    $('#showlist').html('');
                    return false;
                } else {
                    toAdd = '<tr>' +
                    '<td class="text-center" colspan="3"><span style="color: red"> দুঃখিত! কোন ডাটা পাওয়া যায় নি। </span></td> ' +
                    '</tr>';
            $('#addData').append(toAdd);
            $('#showlist').html('');
        }
                }
            });
        }
    });

    function BntoEng(input) {
        var numbers = {
            0: '০',
            1: '১',
            2: '২',
            3: '৩',
            4: '৪',
            5: '৫',
            6: '৬',
            7: '৭',
            8: '৮',
            9: '৯'
        };

        var output = '';
        if (parseInt(input) != 0) {
            while (parseInt(input) != 0) {
                var n = parseInt(input % 10);
                if (!Number.isInteger(n))
                    break;
                input = input / 10;
                output += (numbers[n]);
            }
        } else
        {
            output = '০';
        }
        for (var i = output.length - 1, o = ''; i >= 0; i--)
        {
            o += output[i];
        }
        return o;

    }
</script>
