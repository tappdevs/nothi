<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
           Dashboard Office Update (<?= date('Y-m-d H')?>)
        </div>
    </div>
    <div class="portlet-body">
        <div class="text-center">
            <div class="easy-pie-chart">
                <div class="number transactions" data-percent="0">
                    <span>0</span>%
                </div>
                <label class="badge badge-info totalCount">Total: <?= count($all_offices); ?></label>&nbsp;&nbsp;<label class="badge badge-success doneCount">Completed: 0</label>&nbsp;&nbsp;<label class="badge badge-danger errorCount">Errors: 0</label>
            </div>
        </div>
        <div class="table-scrollable">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr class="text-center heading">
                        <th class="text-center"><?= __('Sequence') ?> </th>
                        <th class="text-center"><?= __('Number') ?> </th>
                        <th class="text-center"><?= __('Name') ?></th>
                        <th class="text-center"><?= __('Dak') ?></th>
                        <th class="text-center"><?= __('Nothi') ?></th>
                        <th class="text-center"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (!empty($all_offices)):
                        $i = 0;
                        foreach ($all_offices as $key => $val):
                            $i++;
                            ?>
                            <tr class="text-center">
                                <td> <?= $this->Number->format($i) ?> </td>
                                <td> <?= $this->Number->format($val['office_id']) ?> </td>
                                <td> <?= $val['office_name'] ?>  </td>
                                <td class="" id="Dak<?= $val['office_id'] ?>">  </td>
                                <td class="" id="Nothi<?= $val['office_id'] ?>">   </td>
                                <td class="offices" id="<?= $val['office_id'] ?>">  </td>
                            </tr>
                            <?php
                        endforeach;
                    endif;
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script src="<?= CDN_PATH ?>assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script>
<script>
    offices_ids = [];
    error = 0;

    $(document).ready(function () {
        if (jQuery().easyPieChart) {
            $('.easy-pie-chart .number.transactions').easyPieChart({
                animate: 1000,
                size: 75,
                lineWidth: 3,
                barColor: Metronic.getBrandColor('green')
            });
        }
        $.each($('.offices'),function (index,val) {
           offices_ids.push(val.id);
        });
       updateOffices(offices_ids[0],0);
    });
function updateOffices(office_id,count){
    if(count >= offices_ids.length){
        return;
    }
          $.ajax({
        type: 'POST',
        url: "<?php echo $this->Url->build(['controller' => 'Cron', 'action' => 'officeDashboardCronUpdate']) ?>",
        data: {"office_id": office_id,'type': 'check'},
        success: function (data) {
            if(data.status == 'success'){
                getDakPart(office_id,count,data.last_time);
//            updateOffices(offices_ids[count+1],count+1);
            }
          else{
               $('#'+office_id).html('<span><i class="fa fa-check" aria-hidden="true"></i>   <b> ' + data.msg + ' </b></span>');
                $('.doneCount').text("Completed: " + (count+1));

                $('.easy-pie-chart .number').each(function () {
                    var newValue = Math.floor(100 * ((count+1) / offices_ids.length));
                    $(this).data('easyPieChart').update(newValue);
                    $('span', this).text(newValue);
                });
                 updateOffices(offices_ids[count+1],count+1);
          }
        },
    });
  
}
function getDakPart(office_id,count,last_time){
     $('#Dak'+office_id).html('<span><i class="fa fa-spin fa-refresh" aria-hidden="true"></i>    </span>');
      $.ajax({
        type: 'POST',
        url: "<?php echo $this->Url->build(['controller' => 'Cron', 'action' => 'officeDashboardCronUpdate']) ?>",
        data: {"office_id": office_id,'type': 'dak', "last_time" : last_time},
        success: function (data) {
          getNothiPart(office_id,count,data,last_time);
           $('#Dak'+office_id).html('<span><i class="fa fa-check" aria-hidden="true"></i> </span>');
        },
        error :  function () {
//          getNothiPart(office_id,count,'');
             $('#Dak'+office_id).html('<span><i class="fa fa-times" aria-hidden="true"></i></span>');
               updateOffices(offices_ids[count+1],count+1);
        }
    });
}
function getNothiPart(office_id,count,dak_data,last_time){
     $('#Nothi'+office_id).html('<span><i class="fa fa-spin fa-refresh" aria-hidden="true"></i>    </span>');
      $.ajax({
        type: 'POST',
        url: "<?php echo $this->Url->build(['controller' => 'Cron', 'action' => 'officeDashboardCronUpdate']) ?>",
        data: {"office_id": office_id,'type': 'nothi','last_time': last_time},
        success: function (data) {
            var role = 'full';
            if(last_time == ''){
                role = 'full';
            }else{
                role = 'partial';
            }
             saveData(office_id,count,dak_data,data,role);
//                updateOffices(offices_ids[count+1],count+1);
              $('#Nothi'+office_id).html('<span><i class="fa fa-check" aria-hidden="true"></i> </span>');
        },
        error :  function () {
//             saveData(office_id,count,dak_data,'');
                updateOffices(offices_ids[count+1],count+1);
                $('#Nothi'+office_id).html('<span><i class="fa fa-times" aria-hidden="true"></i> </span>');
            }
    });
}
function saveData(office_id,count,dak_data,nothi_data,role){
      $('#'+office_id).html('<span><i class="fa fa-spin fa-refresh" aria-hidden="true"></i>  Processing  </span>');
      $.ajax({
        type: 'POST',
        url: "<?php echo $this->Url->build(['controller' => 'Cron', 'action' => 'officeDashboardCronUpdate']) ?>",
        data: {"office_id": office_id,'type': 'save',"dak" : dak_data, "nothi" : nothi_data,'role' : role},
        success: function (data) {
               $('#'+office_id).html();
            if(data.status=='success'){
            $('#'+office_id).html('<span><i class="fa fa-check" aria-hidden="true"></i>  Done <b>( ' + data.msg + ' )</b></span>');
            }
            else{
                $('.errorCount').text("Errors: " + (++error));
                 $('#'+office_id).html('<span><i class="fa fa-times" aria-hidden="true"></i>  Error <b>( ' + data.msg + ' )</b> </span>');
            }
         updateOffices(offices_ids[count+1],count+1);
         $('.doneCount').text("Completed: " + (count+1));

            $('.easy-pie-chart .number').each(function () {
                var newValue = Math.floor(100 * ((count+1) / offices_ids.length));
                $(this).data('easyPieChart').update(newValue);
                $('span', this).text(newValue);
            });
        },
        error :  function () {
             $('.errorCount').text("Errors: " + (++error));
             $('#'+office_id).html();
             $('#'+office_id).html('<span><i class="fa fa-times" aria-hidden="true"></i>  Error  </span>');
              updateOffices(offices_ids[count+1],count+1);
              $('.doneCount').text("Completed: " + (count+1));

            $('.easy-pie-chart .number').each(function () {
                var newValue = Math.floor(100 * ((count+1) / offices_ids.length));
                $(this).data('easyPieChart').update(newValue);
                $('span', this).text(newValue);
            });
            }
    });
}
</script>

