<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
            রিপোর্ট মডুলে অনুমতিপ্রাপ্তদের তালিকা
        </div>
    </div>
    <div class="portlet-body">
        <div class="row">
            <div class="col-md-6 col-sm-6">
            <?php
               echo  $this->Form->create();
                 echo $this->Form->input('username',
                        array(
                        'label' => false,
                        'class' => 'form-control input-md',
                        'placeholder' => 'ব্যবহারকারীর ইউজার আইডি',
                        'required' => true,
                        'maxLength' => 12,
                    ));
                 ?>
            </div>
                 <div class="col-md-6 col-sm-6 float-left">
                <?php
                 echo $this->Form->submit(__('Submit'),array('class' => 'btn btn-md green'));
                 echo $this->Form->end();
            ?>
                 </div>
        </div>
        <br>
        <table class="table table-bordered table-hover tableadvance">
            <thead>
                <tr class="heading">
                    <th class="text-center"> ক্রম </th>
                    <th class="text-center">  ইউজার আইডি</th>
                    <th class="text-center"> তৈরি হয়েছে </th>
                    <th class="text-center"> <?= __('Actions') ?> </th>
                </tr>
            </thead>
            <tbody >
                <?php
                if (!empty($all_permitted_user)) {
                    $index = 0;
                    foreach ($all_permitted_user as $p_key => $p_val) {
                        ?>
                        <tr>
                            <td class="text-center"><?= enTobn(++$index) ?> </td>
                            <td class="text-center"><?= enTobn($p_val['username']) ?> </td>
                            <td class="text-right"><?= $p_val['created'] ?> </td>
                            <td class="text-right">
                                <button type="button" class="btn-link" onclick="delIT('<?=$p_val['id']?>','<?= $p_val['username'] ?>')">
                                    <i class="fa fa-times"></i>&nbsp;<?= __('Delete') ?>
                                </button>
                            </td>
                        </tr>
                        <?php
                    }
                }
                else {
                    ?>
                    <tr><td colspan="4"> কোন তথ্য পাওয়া যায়নি। </td></tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
    </div>
</div>
<script>
function delIT(id,username){
      Metronic.blockUI({
            target: '.page-container',
            boxed: true,
            message: 'অপেক্ষা করুন'
        });
    bootbox.dialog({
                    message: "ব্যবহারকারীঃ " +username +" এর  রিপোর্ট মডুল থেকে অনুমতি বাতিল করতে চান?",
                    title: " অনুমোদন বাতিল ",
                    buttons: {
                        success: {
                            label: "হ্যাঁ",
                            className: "green",
                            callback: function () {
                                deldt(id,username);
                                 
                            }
                        },
                        danger: {
                            label: "না",
                            className: "red",
                            callback: function () {
                                Metronic.unblockUI('.page-container');
                            }
                        }
                    }
                });

}
function deldt(id,username){
        PROJAPOTI.ajaxSubmitDataCallbackError('<?php echo $this->Url->build(['controller' => 'cron',
    'action' => 'deletepermittedReportModuleUser'])
?>', {'id' : id, 'username' : username}, 'json', function (response) {
                                    if(response.status == 'error'){
                                        totastr.error(response.msg);
                                        Metronic.unblockUI('.page-container');
                                    }
                                    else
                                    {
                                        toastr.success(response.msg);
                                        Metronic.unblockUI('.page-container');
                                        window.location.reload();
                                    }
                            }, function (err) {
                            Metronic.unblockUI('.page-container');
                        });
}
</script>
