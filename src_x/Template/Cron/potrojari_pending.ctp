<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
            স্তর ভিত্তিক অফিস তালিকা
        </div>
    </div>
    <div class="portlet-body">
        <div class="row">
            <div class="col-md-4 form-group form-horizontal">
                <?php
                echo $this->Form->hidden('start_date', ['class' => 'startdate']);
                echo $this->Form->hidden('end_date', ['class' => 'enddate']);
                echo $this->Form->input('office_origin_id',
                    array('empty' => '--বাছাই করুন--', 'type' => 'select', 'label' => false, 'class' => 'form-control',
                        'options' => $options));
                ?>
                <?php
                echo $this->Form->create('', ['class' => 'searchForm'])
                ?>
                <?php echo $this->Form->end() ?>
            </div>
                <!--Tools end -->

                <!--  Date Range Begain -->
                <div class="col-md-8 col-sm-4 pull-right margin-top-20" >
                    <div class="hidden-print page-toolbar pull-right portlet-title">
                        <div id="dashboard-report-range" class=" tooltips btn btn-sm btn-default"
                             data-container="body"
                             data-placement="bottom" data-original-title="তারিখ নির্বাচন করুন">
                            <i class="icon-calendar"></i>&nbsp; <span
                                    class="thin uppercase visible-lg-inline-block">তারিখ নির্বাচন করুন</span>&nbsp;
                            <i class="glyphicon glyphicon-chevron-down"></i>
                        </div>
                    </div>
                </div>
                <!--  Date Range End  -->


        </div>
        <br>
        <br>
        <div class="row" id="showlist">

        </div>
        <div class="row">
            <div class="table-container ">
                <table class="table table-bordered table-hover tableadvance">
                    <thead>
                        <tr class="heading">
                            <th class="text-center" >অফিসের আইডি </th>
                            <th class="text-center" > অফিসের নাম </th>
                            <th class="text-center" > অপেক্ষমাণ পত্রজারি প্রাপক সংখ্যা</th>
                            <th class="text-center" > অপেক্ষমাণ পত্রজারি অনুলিপি সংখ্যা</th>
                            <th  class="text-center"> <?= __('Actions') ?> </th>
                        </tr>
                    </thead>
                    <tbody id ="addData">

                    </tbody>
                </table>
            </div>

            <!--Tools end -->

        </div>
        <br/>
        <div class="inbox-content">

        </div>
    </div>
</div>
<script src="<?php echo CDN_PATH; ?>daptorik_preview/js/report_date_range.js"></script>
<script src="<?php echo CDN_PATH; ?>js/reports/offices_reports_for_pending_potrojari.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/floatthead/2.0.3/jquery.floatThead.min.js"></script>
<script>
        jQuery(document).ready(function () {
              $('.table').floatThead({
	position: 'absolute',
                    top: jQuery("div.navbar-fixed-top").height()
            });
            $('.table').floatThead('reflow');
        });

    jQuery(document).ready(function () {
        $(this).find('body').addClass('page-sidebar-closed');
        $(this).find('.page-sidebar-menu').addClass('page-sidebar-menu-closed');
        DateRange.init();
        DateRange.initDashboardDaterange();

        $("#office-origin-id").bind('change', function () {
            if(!($(this).val())==''){
//            getOfficesId($(this).val());
                SortingDate.init($('.startdate').val(), $('.enddate').val());
            }
            $('#addData').html('');

        });
    });

    function getOfficesId(office_layer_id,start_date,end_date) {
            $('#showlist').html('<img src="<?= CDN_PATH ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp; লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');
            $.ajax({
                type: 'POST',
                url: "<?php echo $this->Url->build(['controller' => 'cron', 'action' => 'getLayerwiseOffices']) ?>",
                dataType: 'json',
                data: {"office_layer_type": office_layer_id},
                success: function (data) {
                    if(!(isEmpty(data))){

                    $.each(data, function (i, v) {
                        $('#showlist').html('<img src="<?= CDN_PATH ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp;'+v+' লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');
                        $.ajax({
                            type: 'POST',
                            url: "<?php echo $this->Url->build(['controller' => 'cron', 'action' => 'getLayerwiseOfficesPendingPotrojari']) ?>",
                            dataType: 'json',
                            data: {"office_id": i,'start_date' : start_date,'end_date' : end_date},
                            success: function (res) {
                                if(res.receiver > 0 || res.onulipi  > 0){
                                    var button = '<button type="button"  class="btn btn-primary btn-sm  p_button" onclick="resendPotrojari('+i+',('+(res.potrojari_id)+'))"><i class="fa fa-repeat"></i>  <?= __('Resend') ?> </button>';
                                } else {
                                    var button ='';
                                }
                                toAdd = '<tr>' +
                                    '<td class="text-center" >' + enTobn(i) + '</td> ' +
                                    '<td class="text-center"> <b>' + v + '</b></td>' +
                                    '<td class="text-center">'+enTobn(res.receiver)+'</td>' +
                                    '<td class="text-center">'+enTobn(res.onulipi)+'</td>' +
                                    '<td class="text-center">'+button+'</td>' +
                                    '</tr>';
                                $('#addData').append(toAdd);
                                $('#showlist').html('');
                            }
                        });


                    });
                } else {
                        toAdd = '<tr>' +
                            '<td class="text-center" colspan="5"><span style="color: red"> দুঃখিত! কোন ডাটা পাওয়া যায় নি। </span></td> ' +
                            '</tr>';
                        $('#addData').append(toAdd);
                        $('#showlist').html('');
                    }

                }
            });
        }
        function resendPotrojari(office_id,potrojari_ids) {
            Metronic.blockUI({
                target: 'body',
                message: 'অপেক্ষা করুন'
            });
            $.ajax({
                type: 'POST',
                url: "<?php echo $this->Url->build(['controller' => 'Potrojari', 'action' => 'resendLayerwisePotrojari']) ?>",
                dataType: 'json',
                data: {"office_id": office_id,"potrojari_ids" : potrojari_ids},
                success: function (res) {
                    if(res.status=='success'){
                        toastr.success('পত্রজারি পুনরায় প্রেরন প্রক্রিয়াটি শুরু হয়েছে।');
                    } else{
                        toastr.error('পত্রজারি পুনরায় প্রেরন করা সম্ভব হচ্ছে না।');
                    }
                    Metronic.unblockUI('body');
                }
            });
        }

</script>
