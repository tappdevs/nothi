<table class="table table-bordered table-hover tableadvance">
    <thead>
        <tr class="heading">
            <th colspan="9" class="text-center">পত্রজারি </th>
        </tr>
        <tr class="heading">
            <th class="text-center" >  ধরন </th>
            <th class="text-center" >  বিষয় </th>
            <th class="text-center" >  প্রেরক </th>
            <th class="text-center" >  অনুমোদনকারী </th>
            <th class="text-center" >  প্রাপক </th>
            <th class="text-center" >  অনুলিপি </th>
            <th class="text-center" >  খসড়া তৈরি হয়েছে </th>
            <th class="text-center" >  সময় </th>
            <th class="text-center" >  পত্র প্রিভিউ </th>
        </tr>
    </thead>
    <tbody >
        <?php
        $nothi_parts_no = [];
        if ($data['status'] == 1) {
            if(!empty($data['data'])){
                foreach ($data['data'] as $val){
            ?>
            <tr>
                        <?php
                        if(isset($val['nothi_part_no']) && !in_array($val['nothi_part_no'],$nothi_parts_no)){
                            $nothi_parts_no[]= $val['nothi_part_no'];
                        }
                        $s_name = '';$s_unit = '';$s_designation = '';
                        if(!empty($val['approval_officer_name']) && $val['approval_officer_name'] !='0'){
                            $s_name =$val['approval_officer_name'].', ';
                        }
                        if(!empty($val['approval_officer_designation_label']) && $val['approval_officer_designation_label'] !='0'){
                            $s_designation =$val['approval_officer_designation_label'].', ';
                        }
                        if(!empty($val['approval_office_unit_name']) && $val['approval_office_unit_name'] !='0'){
                            $s_unit =$val['approval_office_unit_name'];
                        }
                        $a_name = '';$a_unit = '';$a_designation = '';
                        if(!empty($val['officer_name']) && $val['officer_name'] !='0'){
                            $a_name =$val['officer_name'].', ';
                        }
                        if(!empty($val['officer_designation_label']) && $val['officer_designation_label'] !='0'){
                            $a_designation =$val['officer_designation_label'].', ';
                        }
                        if(!empty($val['office_unit_name']) && $val['office_unit_name'] !='0'){
                            $a_unit =$val['office_unit_name'];
                        }
                      
                            ?>
                <td class="text-center"><?= $data['p_templates'][$val['potro_type']] ?> </td>
                <td class="text-center"><?= $val['potro_subject'].( isset($val['nothi_part_no'])?('<br>(নোট আইডিঃ '.$val['nothi_part_no'].')'):''); ?> </td>
                <td class="text-center"><?=$s_name.$s_designation.$s_unit?> </td>
                <td class="text-center"><?=$a_name.$a_designation.$a_unit .(($a_designation == $s_designation)?'<b>( প্রেরক-অনুমোদনকারী এক )</b>':'')?> </td>
                <td class="text-center">
                    <?php
                      $ind = 0;
                        if(!empty($val['potrojari_receiver'])){

//                            echo '<ul class="list-group">';
                            foreach($val['potrojari_receiver'] as $pr_val){
//                                echo '<li class="list-group-item">';
                                $f = 0;
                                    if($ind > 0){
                                        echo '<br>';
                                    }
                                    if(!empty($pr_val['receiving_officer_name']) || !empty($pr_val['receiving_officer_designation_label']) || !empty($pr_val['receiving_office_unit_name'])){
                                        echo entobn($ind+1).'. ';
                                    }
                                     
                                    if(!empty($pr_val['receiving_officer_name']) && $pr_val['receiving_officer_name'] !='0'){
                                       echo $pr_val['receiving_officer_name'];
                                       $f = 1;
                                   }
                                   if(!empty($pr_val['receiving_officer_designation_label']) && $pr_val['receiving_officer_designation_label'] !='0'){
                                       if($f == 1){
                                           echo ', ';
                                       }
                                      echo $pr_val['receiving_officer_designation_label'];
                                      $f = 1;
                                   }
                                   if(!empty($pr_val['receiving_office_unit_name']) && $pr_val['receiving_office_unit_name'] !='0'){
                                         if($f == 1){
                                           echo ', ';
                                       }
                                       echo $pr_val['receiving_office_unit_name'];
                                   }
                                    if(empty($pr_val['receiving_officer_designation_id'])){
                                        echo '<br><b>(বহিঃস্থ'
                                            .(!empty($pr_val['receiving_officer_email'])?(', ইমেইলঃ '.$pr_val['receiving_officer_email']):'')
                                            .(!empty($pr_val['officer_mobile'])?(', মোবাইলঃ '.str_replace('--',' ',$pr_val['officer_mobile'])):'')
                                            .')</b>';
                                    }else{
                                        echo ' <b>(সিস্টেম)</b>';
                                    }
                                   $ind++ ;
//                                   echo '</li>';
                            }
//                            echo '</ul>';
                        }
                    ?>
                </td>
                <td class="text-center">
                    <?php
                      $ind = 0;
                        if(!empty($val['potrojari_onulipi'])){
//                            echo '<ul class="list-group">';
                            foreach($val['potrojari_onulipi'] as $pr_val){
//                                echo '<li class="list-group-item">';
                                $f = 0;
                                    if($ind > 0){
                                        echo '<br>';
                                    }
                                     if(!empty($pr_val['receiving_officer_name']) || !empty($pr_val['receiving_office_unit_name']) || !empty($pr_val['receiving_officer_designation_label'])){
                                        echo entobn($ind+1).'. ';
                                    }
                                    if(!empty($pr_val['receiving_officer_name']) && $pr_val['receiving_officer_name'] !='0'){
                                       echo $pr_val['receiving_officer_name'];
                                       $f = 1;
                                   }
                                   if(!empty($pr_val['receiving_officer_designation_label']) && $pr_val['receiving_officer_designation_label'] !='0'){
                                        if($f == 1){
                                           echo ', ';
                                       }
                                      echo $pr_val['receiving_officer_designation_label'];
                                      $f = 1;
                                   }
                                   if(!empty($pr_val['receiving_office_unit_name']) && $pr_val['receiving_office_unit_name'] !='0'){
                                        if($f == 1){
                                           echo ', ';
                                       }
                                       echo $pr_val['receiving_office_unit_name'];
                                   }
                                   if(empty($pr_val['receiving_officer_designation_id'])){
                                       echo '<br><b>(বহিঃস্থ'
                                           .(!empty($pr_val['receiving_officer_email'])?(', ইমেইলঃ '.$pr_val['receiving_officer_email']):'')
                                           .(!empty($pr_val['officer_mobile'])?(', মোবাইলঃ '.str_replace('--',' ',$pr_val['officer_mobile'])):'')
                                           .')</b>';
                                   }else{
                                       echo ' <b>(সিস্টেম)</b>';
                                   }
                                   $ind++ ;
//                                echo '</li>';
                            }
//                            echo '</ul>';
                        }
                    ?>
                </td>
                <td class="text-center"><?= $val['created'] ?> </td>
                <td class="text-center"><?= $val['potrojari_date'] ?> </td>
                <td class="text-center" >
                    <button type="button" class="btn btn-sm btn-primary" onclick="viewDetails('<?= $val['approval_office_id'] ?>','<?=$val['id']?>')"> প্রিভিউ দেখুন </button>
                </td>
            </tr>
            <?php
                }
        }
        }
        else {
            ?>
            <tr><td colspan="8"> কোন তথ্য পাওয়া যায়নি। </td></tr>
            <?php
        }
        ?>
    </tbody>
</table>
<div class="actions text-center">
    <ul class="pagination pagination-sm">
        <?php
        echo $this->Paginator->first(__('প্রথম', true), array('class' => 'number-first'));
        echo $this->Paginator->prev('<<', array('tag' => 'li', 'escape' => false), '<a href="#" class="btn btn-sm blue">&laquo;</a>', array('class' => 'prev disabled btn btn-sm blue', 'tag' => 'li', 'escape' => false));
        echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a', 'reverse' => FALSE));
        echo $this->Paginator->next('>>', array('tag' => 'li', 'escape' => false), '<a href="#" class="btn btn-sm blue ">&raquo;</a>', array('class' => 'next disabled', 'tag' => 'li', 'escape' => false));
        echo $this->Paginator->last(__('শেষ', true), array('class' => 'number-last'));
        ?>
    </ul>
    <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">পত্রজারির পত্র প্রিভিউ</h4>
                                   
                    </div>
                    <div class="modal-body">
                        <div class="row" id="preview" style="height: 400px;overflow: auto;">
                            <embed class="showPreview" src="" style="width:100%; min-height: 600px;" type="application/pdf"></embed>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><?= __('Close') ?></button>
                    </div>
                </div>

            </div>
        </div>
</div>
<style>
        @media print {
        a[href]:after {
            content: none !important;
        }
        #preview {
            line-height: 1.4;
            margin: 0px!important;
            padding:2px!important;
        }

        @page {
            margin-top: 1in;
            margin-left: 1.2in;
            margin-bottom: .75in;
            margin-right: .75in;
        }

        body {
            margin-top: 1in;
            margin-left: 1.2in;
            margin-bottom: .75in;
            margin-right: .75in;
        }

        #preview p {
            margin: 0px 0px 5px !important;
        }

        #preview .btn-block {
            margin-bottom: 2px !important;
        }

        .row > div{
            padding: 0px !important;
        }
    }
</style>
<script src="<?=$this->request->webroot?>assets/global/scripts/printThis.js"></script>
<script>
    $(document).ready(function(){
        var $nothi_parts_no = <?= json_encode($nothi_parts_no); ?>;
        if(!isEmpty($nothi_parts_no)){
            console.log($nothi_parts_no);
        }
            $('.btn-print').click(function () {
        $('#preview').printThis({
            importCSS: true,
            debug: false,
            importStyle: true,
            printContainer: false,
            pageTitle: "",
            removeInline: false,
            header: null
        });
    });
    });

</script>