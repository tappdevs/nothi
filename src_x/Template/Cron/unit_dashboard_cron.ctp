<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
             Unit Dashboard Update ((<?= date('Y-m-d H')?>)
        </div>
    </div>
    <div class="portlet-body">
        <div class="text-center">
            <div class="easy-pie-chart">
                <div class="number transactions" data-percent="0">
                    <span>0</span>%
                </div>
                <label class="badge badge-info totalCount">Total: <?= count($all_units); ?></label>&nbsp;&nbsp;<label class="badge badge-success doneCount">Completed: 0</label>&nbsp;&nbsp;<label class="badge badge-danger errorCount">Errors: 0</label>
            </div>
        </div>
        <div class="table-scrollable">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr class="text-center heading">
                         <th class="text-center"><?= __('Sequence') ?> </th>
                        <th class="text-center"><?= __('Number') ?> </th>
                        <th class="text-center"><?= __('Name') ?></th>
                        <th class="text-center"><?= __('Dak') ?></th>
                        <th class="text-center"><?= __('Nothi') ?></th>
                        <th class="text-center"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (!empty($all_units)):
                        $i = 0;
                        foreach ($all_units as $key => $val):
                            $i++;
                            ?>
                            <tr class="text-center">
                                <td> <?= $this->Number->format($i) ?> </td>
                                <td> <?= $this->Number->format($key) ?> </td>
                                <td> <?= $val ?> </td>
                                <td class="" id="Dak<?= $key ?>">  </td>
                                <td class="" id="Nothi<?= $key ?>">   </td>
                                <td class="units" id="<?= $key ?>">  </td>
                            </tr>
                            <?php
                        endforeach;
                    endif;
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script src="<?= CDN_PATH ?>assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script>
<script>
    units_ids = [];
    error = 0;

    $(document).ready(function () {
        if (jQuery().easyPieChart) {
            $('.easy-pie-chart .number.transactions').easyPieChart({
                animate: 1000,
                size: 75,
                lineWidth: 3,
                barColor: Metronic.getBrandColor('green')
            });
        }


        $.each($('.units'),function (index,val) {
           units_ids.push(val.id);
        });
       updateUnits(units_ids[0],0);
    });
function updateUnits(unit_id,count){
    if(count >= units_ids.length){
        return;
    }
    $.ajax({
        type: 'POST',
        url: "<?php echo $this->Url->build(['controller' => 'Cron', 'action' => 'unitDashboardCronUpdate']) ?>",
        data: {"unit_id": unit_id,'type': 'check'},
        success: function (data) {
            if(data.status == 'success'){
                getDakPart(unit_id,count,data.last_time);
            }
          else{
               $('#'+unit_id).html('<span><i class="fa fa-check" aria-hidden="true"></i>   <b> ' + data.msg + ' </b></span>');
                $('.doneCount').text("Completed: " + (count+1));

                $('.easy-pie-chart .number').each(function () {
                    var newValue = Math.floor(100 * ((count+1) / units_ids.length));
                    $(this).data('easyPieChart').update(newValue);
                    $('span', this).text(newValue);
                });
                 updateUnits(units_ids[count+1],count+1);
          }
        },
    });
}
function getDakPart(unit_id,count,last_time){
     $('#Dak'+unit_id).html('<span><i class="fa fa-spin fa-refresh" aria-hidden="true"></i>    </span>');
      $.ajax({
        type: 'POST',
        url: "<?php echo $this->Url->build(['controller' => 'Cron', 'action' => 'unitDashboardCronUpdate']) ?>",
        data: {"unit_id": unit_id,'type': 'dak','last_time': last_time},
        success: function (data) {
          getNothiPart(unit_id,count,data,last_time);
           $('#Dak'+unit_id).html('<span><i class="fa fa-check" aria-hidden="true"></i> </span>');
        },
        error :  function () {
//          getNothiPart(office_id,count,'');
             $('#Dak'+unit_id).html('<span><i class="fa fa-times" aria-hidden="true"></i></span>');
               updateUnits(units_ids[count+1],count+1);
        }
    });
}
function getNothiPart(unit_id,count,dak_data,last_time){
     $('#Nothi'+unit_id).html('<span><i class="fa fa-spin fa-refresh" aria-hidden="true"></i>    </span>');
      $.ajax({
        type: 'POST',
        url: "<?php echo $this->Url->build(['controller' => 'Cron', 'action' => 'unitDashboardCronUpdate']) ?>",
        data: {"unit_id": unit_id,'type': 'nothi', 'last_time' : last_time},
        success: function (data) {
               var role = 'full';
            if(last_time == ''){
                role = 'full';
            }else{
                role = 'partial';
            }
             saveData(unit_id,count,dak_data,data,role);
              $('#Nothi'+unit_id).html('<span><i class="fa fa-check" aria-hidden="true"></i> </span>');
        },
        error :  function () {
//             saveData(office_id,count,dak_data,'');
                updateUnits(units_ids[count+1],count+1);
                $('#Nothi'+unit_id).html('<span><i class="fa fa-times" aria-hidden="true"></i> </span>');
            }
    });
}
function saveData(unit_id,count,dak_data,nothi_data,role){
      $('#'+unit_id).html('<span><i class="fa fa-spin fa-refresh" aria-hidden="true"></i>  Processing  </span>');
      $.ajax({
        type: 'POST',
        url: "<?php echo $this->Url->build(['controller' => 'Cron', 'action' => 'unitDashboardCronUpdate']) ?>",
        data: {"unit_id": unit_id,'type': 'save',"dak" : dak_data, "nothi" : nothi_data,'role' : role},
        success: function (data) {
               $('#'+unit_id).html();
            if(data.status=='success'){
            $('#'+unit_id).html('<span><i class="fa fa-check" aria-hidden="true"></i>  Done <b>( ' + data.msg + ' )</b></span>');
            }
            else{
                $('.errorCount').text("Errors: " + (++error));
                 $('#'+unit_id).html('<span><i class="fa fa-times" aria-hidden="true"></i>  Error <b>( ' + data.msg + ' )</b> </span>');
            }
         updateUnits(units_ids[count+1],count+1);
         $('.doneCount').text("Completed: " + (count+1));

            $('.easy-pie-chart .number').each(function () {
                var newValue = Math.floor(100 * ((count+1) / units_ids.length));
                $(this).data('easyPieChart').update(newValue);
                $('span', this).text(newValue);
            });
        },
        error :  function () {
             $('.errorCount').text("Errors: " + (++error));
             $('#'+unit_id).html();
             $('#'+unit_id).html('<span><i class="fa fa-times" aria-hidden="true"></i>  Error  </span>');
              updateUnits(units_ids[count+1],count+1);
              $('.doneCount').text("Completed: " + (count+1));

            $('.easy-pie-chart .number').each(function () {
                var newValue = Math.floor(100 * ((count+1) / units_ids.length));
                $(this).data('easyPieChart').update(newValue);
                $('span', this).text(newValue);
            });
            }
    });
}
</script>

