<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
        শাখা পরিবর্তন ট্র্যাকিং
        </div>
    </div>
    <div class="portlet-body">
        <div class="row">
            <?= $this->Cell('OfficeSelection'); ?>
            <div class="row">
                <div class="col-md-4 col-sm-8 form-group form-horizontal">
                    <label class="control-label"> <?php echo __("Office") ?> </label>
                    <?php
                    echo $this->Form->hidden('start_date', ['class' => 'startdate']);
                    echo $this->Form->hidden('end_date', ['class' => 'enddate']);
                    echo $this->Form->input('office_id',
                        array(
                        'label' => false,
                        'class' => 'form-control',
                        'empty' => '----'
                    ));
                    ?>
                </div>

             <!-- part two -->
 
            <div class="col-sm-4 form-group form-horizontal">
                <?php
                echo $this->Form->input('office_unit_id', array(
                    'label' => __('Unit'),
                    'class' => 'form-control',
                    'empty' => '----'
                ));
                ?>
            </div>
            <!-- <div class="col-sm-3 form-group form-horizontal">
                <?php
                /*** echo $this->Form->input('office_unit_organogram_id', array(
                    'label' => __('Organogra'),
                    'class' => 'form-control',
                    'empty' => '--বাছাই করুন--'
                ));*****/
                ?>

            </div> -->
            <div class="col-sm-1 form-group form-horizontal">
                 
                    <?php
                    echo $this->Form->button(__("Search"), array(
                        'label' =>false,
                        'class' => 'btn btn-success',
                        'type' => 'button',
                        'id' => 'btnSearch',
                        'style'=>'margin-top:25px'
                    ));
                    ?>
 
            </div>

<!-- end part two -->
            <!--Tools end -->

            <!--  Date Range Begain -->
            <div class="col-md-8 col-sm-4 pull-right margin-top-20" >
                <div class="hidden-print page-toolbar pull-right portlet-title">
                    <div id="dashboard-report-range" class=" tooltips btn btn-sm btn-default"
                         data-container="body"
                         data-placement="bottom" data-original-title="তারিখ নির্বাচন করুন">
                        <i class="icon-calendar"></i>&nbsp; <span
                            class="thin uppercase visible-lg-inline-block">তারিখ নির্বাচন করুন</span>&nbsp;
                        <i class="glyphicon glyphicon-chevron-down"></i>
                    </div>
                </div>
            </div>
            <!--  Date Range End  -->

            </div>
        </div>
        <div class="row" id="showlist">

        </div>
        <div class="row" >
            <div class="table-container " id ="addData">
            </div>



        </div>
        <br/>
    </div>
</div>
<script src="<?php echo CDN_PATH; ?>daptorik_preview/js/report_date_range.js"></script>
<!-- <script src="<?php //echo CDN_PATH; ?>js/reports/designation_tracking.js" type="text/javascript"></script> -->
<script>

    jQuery(document).ready(function () {
        
        $(this).find('body').addClass('page-sidebar-closed');
        $(this).find('.page-sidebar-menu').addClass('page-sidebar-menu-closed');
        DateRange.init();
        DateRange.initDashboardDaterange();
        var EmployeeLoginHistory = {
            loadMinistryWiseLayers: function () {
                OfficeSetup.loadLayers($("#office-ministry-id").val(), function () {

                });
            },
            loadMinistryAndLayerWiseOfficeOrigin: function () {
                OfficeSetup.loadOffices($("#office-ministry-id").val(), $("#office-layer-id").val(), function () {

                });
            },
            loadOriginOffices: function () {
                OfficeSetup.loadOriginOffices($("#office-origin-id").val(), function () {

                });
            },
            loadOfficeUnits: function () {
                OfficeSetup.loadOfficeUnits($("#office-id").val(), function () {

                });
            },
            loadOfficeUnitDesignations: function () {
            PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + 'officeManagement/loadOfficeUnitOrganogramsWithName',
                    {'office_unit_id': $("#office-unit-id").val()}, 'json',
                    function (response) {
                        // PROJAPOTI.projapoti_dropdown_map("#office-unit-organogram-id", response, "--বাছাই করুন--");
                        //PROJAPOTI.projapoti_dropdown_map_with_title("#office-unit-organogram-id", response, "--বাছাই করুন--",'',{'key' : 'id','value' : 'designation','title' : 'name'  });
                    });
            },
        };
        $("#office-ministry-id").bind('change', function () {
            EmployeeLoginHistory.loadMinistryWiseLayers();
        });
        $("#office-layer-id").bind('change', function () {
            EmployeeLoginHistory.loadMinistryAndLayerWiseOfficeOrigin();
        });
        $("#office-origin-id").bind('change', function () {
            EmployeeLoginHistory.loadOriginOffices();
        });
        $("#office-id").bind('change', function () {
            EmployeeLoginHistory.loadOfficeUnits();
        });
        $("#office-unit-id").bind('change', function () {
            EmployeeLoginHistory.loadOfficeUnitDesignations();
        });
        $("#btnSearch").bind('click', function () {
            SortingDate.init($('.startdate').val(), $('.enddate').val());
            // loadHistory(office_id,office_unit_id,start_date,end_date);
        });
    });
    function  loadHistory(office_id,office_unit_id,start_date,end_date) {
        Metronic.blockUI({target: '.portlet-body', boxed: true});
        $('#showlist').html('');
        $.ajax({
            type: 'POST',
            url: "<?php echo $this->Url->build(['controller' => 'Cron', 'action' => 'unitTracking']) ?>",
            data: {'office_id': office_id,'office_unit_id': office_unit_id,'start_date' : start_date,'end_date' : end_date},
            success: function (data) {
                Metronic.unblockUI('.portlet-body');
                $('#showlist').html(data);
            },
            error: function () {
                Metronic.unblockUI('.portlet-body');
            }
        });
    }
    function BntoEng(input) {
        var numbers = {
            0: '০',
            1: '১',
            2: '২',
            3: '৩',
            4: '৪',
            5: '৫',
            6: '৬',
            7: '৭',
            8: '৮',
            9: '৯'
        };

        var output = '';
        if (parseInt(input) != 0) {
            while (parseInt(input) != 0) {
                var n = parseInt(input % 10);
                if (!Number.isInteger(n))
                    break;
                input = input / 10;
                output += (numbers[n]);
            }
        } else
        {
            output = '০';
        }
        for (var i = output.length - 1, o = ''; i >= 0; i--)
        {
            o += output[i];
        }
        return o;

    }



    var SortingDate = function () {

                var content = $('.inbox-content');

                var loadDraft = function (el, startDate, endDate) {

                    $('.startdate').val(startDate);
                    $('.enddate').val(endDate);
                    var office_id = $('#office-id').val();
                    var office_unit_id = $('#office-unit-id').val();
                    //var office_designation_id = $('#office-unit-organogram-id').val();

                    loadHistory(office_id,office_unit_id,startDate,endDate);
                    var toggleButton = function (el) {
                        if (typeof el == 'undefined') {
                            return;
                        }
                        if (el.attr("disabled")) {
                            el.attr("disabled", false);
                        } else {
                            el.attr("disabled", true);
                        }
                    };
                };

                return {
                    //main function to initiate the module
                    init: function (startDate, endDate) {
                        $('#showlist').html('');
                            if(startDate == '' || typeof(startDate) == 'undefined' ){
                                startDate =   $('.startdate').val();
                            }
                            if(endDate == '' || typeof(endDate) == 'undefined' ){
                                endDate =   $('.enddate').val();
                            }
                        
                            loadDraft($(this), startDate, endDate);
                        
                    }

                };

}();

</script>

