<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            মন্ত্রণালয় ভিত্তিক কাস্টম স্তর ম্যাপিং
        </div>
        <div class="actions">
            <a href="<?php echo $this->Url->build(['controller' => 'CustomOfficeLayers', 'action' => 'index']) ?>" class="btn btn-sm btn-warning"><i class="fa fa-plus"></i>  কাস্টম স্তর যুক্তকরণ</a>
            <a href="<?php echo $this->Url->build(['controller' => 'CustomOfficeLayers', 'action' => 'mapInfos']) ?>" class="btn btn-sm btn-danger"><i class="fa fa-wrench"></i>  কাস্টম স্তর - অফিস ম্যাপিং</a>
        </div>
    </div>
    <div class="portlet-body">
        <div class="row">
            <div class="col-md-2">
                <label class="pull-right form-label-stripped">মন্ত্রণালয় </label>
            </div>
            <div class="col-md-4 form-group form-horizontal">
                <?php
                echo $this->Form->input('ministry_id',
                    array('empty' => '--বাছাই করুন--', 'type' => 'select', 'label' => false, 'class' => 'form-control',
                          'options' => $officeMinistries));
                ?>
            </div>
        </div>
        <br>
        <br>
        <div class="row" id="showlist">

        </div>
        <div class="row">
            <div class="table-container">
                <table class="table table-bordered table-hover tableadvance">
                    <thead>
                    <tr class="heading">
                        <th class="text-center" > ক্রম </th>
                        <th class="text-center" >মুল স্তরের নাম</th>
                        <th class="text-center" > কাস্টম স্তর </th>
                        <th class="text-center" > কার্যক্রম </th>
                    </tr>
                    </thead>
                    <tbody id ="addData">

                    </tbody>
                </table>
            </div>

            <!--Tools end -->

        </div>
        <br/>
        <div class="modal fade modal-purple height-auto" data-backdrop="static" id="yourModal" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">কাস্টম স্তর সংশোধন</h4>
                    </div>
                    <div class="modal-body">
                        <form class="form-group">
                            <div class="row">
                                <div class="col-md-4 col-sm-4">
                                    <label class="pull-right form-label-stripped"> মুল স্তর</label>
                                </div>
                                <div class="col-md-8 col-sm-8 form-group form-horizontal">
                                    <input type="text" class="form-control" id="update_text" readonly>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 col-md-4">
                                    <label class="pull-right form-label-stripped"> কাস্টম স্তর</label>
                                </div>
                                <div class="col-md-8 col-md-8 form-group form-horizontal">
                                    <?php
                                    echo $this->Form->input('update_custom_layer_id',
                                        array('empty' => '--বাছাই করুন--', 'type' => 'select', 'label' => false, 'class' => 'form-control',
                                              'options' => $all_custom_layers,'id' => 'update_custom_layer_id'));
                                    ?>
                                </div>
                            </div>

                            <input type="hidden" class="form-control" id="update_layer_id">

                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" onclick="update_action();">সংরক্ষণ</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">বন্ধ করুন</button>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<script>
    var all_custom_layers =<?=json_encode($all_custom_layers)?>;
    console.log(all_custom_layers);
    $(document).ready(function(){
        $("#ministry-id").trigger('change');
    });
    $("#ministry-id").bind('change', function () {
        if(!isEmpty($(this).val())){
            getLayersInfo($(this).val());
        }
        $('#addData').html('');
    });
    function getLayersInfo(ministry_id){
        $('#showlist').html('<img src="<?= CDN_PATH ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp; লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');
        $.ajax({
            type: 'POST',
            url: "<?php echo $this->Url->build(['controller' => 'CustomOfficeLayers', 'action' => 'ministryWiseMap']) ?>",
            dataType: 'json',
            data: {"ministry_id": ministry_id},
            success: function (data) {
                if(!(isEmpty(data)) && data.status == 'success' && !(isEmpty(data.data))){
                    $.each(data.data, function (i, v) {
                        var edit_url = "editCustomLayer("+v.id+",'"+v.layer_name_bng+"',"+v.custom_layer_id+")";
                        toAdd = '<tr>' +
                            '<td class="text-center" >' + BnFromEng(i+1) + '</td> ' +
                            '<td class="text-center" >' + v.layer_name_bng + '</td> ' +
                            '<td class="text-center"> <b>' + (isEmpty(v.custom_layer_id)?'':all_custom_layers[v.custom_layer_id]) + '</b></td>' +
                            '<td class="text-center"><button onclick="'+edit_url+'" class="btn btn-xs btn-primary">সম্পাদনা</button></td>' +
                            '</tr>';
                        $('#addData').append(toAdd);
//
                    });
                    $('#showlist').html('');
                    return false;
                } else {
                    toAdd = '<tr>' +
                        '<td class="text-center" colspan="4"><span style="color: red"> দুঃখিত! কোন তথ্য পাওয়া যায় নি। </span></td> ' +
                        '</tr>';
                    $('#addData').append(toAdd);
                    $('#showlist').html('');
                }
            }
        });
    }
    function editCustomLayer(id,name){
        $("#yourModal").modal('toggle');
        $("#update_text").val(name);
        $("#update_layer_id").val(id);
    }
    function update_action(){
        var custom_layer_id = $("#update_custom_layer_id").val();
        var layer_id = $("#update_layer_id").val();
        var edit_url = "<?php echo $this->Url->build(['controller' => 'CustomOfficeLayers', 'action' => 'customLayerEdit']) ?>";
        PROJAPOTI.ajaxSubmitDataCallback(edit_url,{layer_id:layer_id,custom_layer_id:custom_layer_id},'json',function(res){
            if(res.status == 'success'){
                toastr.success(res.msg);
                $("#yourModal").modal('toggle');
                $("#ministry-id").trigger('change');
            }else{
                toastr.error(res.msg);
            }
        });
    }
</script>