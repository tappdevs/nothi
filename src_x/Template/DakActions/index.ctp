<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            ডাক সিদ্ধান্ত
        </div>
        <div class="actions">
            <a href="<?=$dak_action_refer_url ?>" class="btn btn-sm btn-danger"><i class="fa fa-arrow-left"></i> <?= __('Back to previous URL') ?> </a>
        </div>
    </div>
    <div class="portlet-body">
        <div>
            <div class="pull-right" style="padding-bottom: 10px;">
            <button type="button" class="btn btn-success round-corner-5" onclick="save_status();">সংরক্ষণ</button>
            <br>
            </div>
        </div>
        <div class="table-scrollable">
            <table class="table table-hover table-bordered table-condensed" id="datatable_table">
                <thead>
                    <tr>
                        <th style="width:10%;" class="text-center" >
                            নম্বর
                        </th>
                        <th class="text-center heading" style="width:65%;">
                            সিদ্ধান্তসমূহ
                        </th>
                        <th style="width:10%;" class="text-center" >
                            তালিকাভুক্ত করণ <input type="checkbox" id="selectAll" title="সকল সিদ্ধান্ত বাছাই করুন"  />
                        </th>
                        <th class="text-center">
                            <a class="btn green btn-sm round-corner-5" style="text-decoration: none;padding-left: 10px;" data-toggle="modal" data-target="#myModal"><i class="fs1 a2i_gn_add1"></i>&nbsp;&nbsp;নতুন</a>
                        </th>
                    </tr>
                </thead>
                <tbody>

                    <?php
                     $i = 0;
                     if(!empty($data)){
                    foreach ($data as $key => $val) {
                       $i++;
                        ?>
                        <tr>
                            <td class="text-center"><?= $this->Number->format($i); ?></td>
                            <td id="update_<?= $val['id'] ?>" class="" style="font-size: 14px!important;"><?= h($val['dak_action_name']); ?></td>
                            <td class="text-center"><input type="checkbox" class="status" value="<?= $val['id'] ?>" <?= isset($status_data[$val['id']])? 'checked':'' ?> ></td>
                            <td class="text-center">
	                            <div class="btn-group btn-group-round">
                                <?php
                                if ($val['creator'] == $employee['officer_id']) {
                                    ?>
                                    <a class="btn btn-sm btn-success" style="margin:0" data-toggle="modal" title="সম্পাদনা" data-target="#yourModal" onclick="update(<?= $val['id'] ?>);"><i class="fa fa-edit"></i></a>&nbsp;&nbsp;&nbsp;
                                    <a class="btn btn-sm btn-danger" style="margin:0" data-toggle="modal" title="মুছে ফেলুন" data-target="#deleteModal" onclick="del(<?= $val['id'] ?>);"><i class="fa fa-times"></i></a>
                                    <?php
                                }
                                ?>
	                            </div>
                            </td>
                        </tr>
                        <?php
                    }
                     }
                    ?>

                </tbody>
            </table>

        </div>
        <?php if(!empty($data)){ ?>
        <div class="" style="padding-bottom: 10px;">
            <button type="button" class="btn btn-success pull-right round-corner-5" onclick="save_status();">সংরক্ষণ</button>
        </div><br>
            <?php
        }
        ?>
        
        <div class="modal fade modal-purple height-auto" data-backdrop="static" id="myModal" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">নতুন ডাক সিদ্ধান্ত</h4>
                    </div>
                    <div class="modal-body">
                        <form class="form-group">
                            <input type="text" class="form-control" id="save_text" placeholder="ডাক সিদ্ধান্ত লিখুন">
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" onclick="save_action(this);">সংরক্ষণ</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">বন্ধ করুন</button>
                    </div>
                </div>

            </div>
        </div>
        <div class="modal fade modal-purple height-auto" data-backdrop="static" id="yourModal" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">ডাক সিদ্ধান্ত সংশোধন</h4>
                    </div>
                    <div class="modal-body">
                        <form class="form-group">
                            <input type="text" class="form-control" id="update_text">
                            <input type="hidden" class="form-control" id="update_id">
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" onclick="update_action(this);">সংরক্ষণ</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">বন্ধ করুন</button>
                    </div>
                </div>

            </div>
        </div>
        <div class="modal fade modal-purple height-auto" data-backdrop="static" id="deleteModal" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"> ডাক সিদ্ধান্ত বাতিল</h4>
                    </div>
                    <div class="modal-body">
                        <form class="form-group">
                            <label>আপনি কি সিদ্ধান্তটি মুছে ফেলতে চান?</label>
                            <input type="hidden" class="form-control" id="delete_id">
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" onclick="delete_action(this);">মুছে ফেলুন</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">বন্ধ করুন</button>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function update(id)
    {
        var text = $('#update_' + id + '').text();
        $('#update_text').val(text);
        $('#update_id').val(id);
    }
    function del(id)
    {
        $('#delete_id').val(id);
    }
    function save_action(element)
    {
        $(element).addClass('disabled');
        $(element).html('<i class="fa fa-spin fa-cog"></i> সংরক্ষণ হচ্ছে...');
        var text = $('#save_text').val();
        $.ajax({
            type: 'POST',
            url: "<?php echo $this->Url->build(['controller' => 'DakActions', 'action' => 'add']) ?>",
            data: {"text": text},
            success: function (data) {
                window.location.reload();
            }
        });
    }
    function update_action(element)
    {
        $(element).addClass('disabled');
        $(element).html('<i class="fa fa-spin fa-cog"></i> সংরক্ষণ হচ্ছে...');
        var text = $('#update_text').val();
        var id = $('#update_id').val();
//       console.log(text+':'+id);
        $.ajax({
            type: 'POST',
            url: "<?php echo $this->Url->build(['controller' => 'DakActions', 'action' => 'update']) ?>",
            data: {"text": text, "id": id},
            success: function (data) {
                window.location.reload();
            }
        });
    }
    function save_status() {
        var status = new Array();
        $('.status:checked').each(function(){
            status.push($(this).val());
        });
        if(isEmpty(status)){
            toastr.error("দুঃখিত! কোন সিদ্ধান্ত নির্বাচন হয়নি");
            return false;
        }

        bootbox.dialog({
            message: "আপনি কি সিদ্ধান্তসমূহ সংরক্ষণ করতে ইচ্ছুক?",
            title: "সিদ্ধান্ত সংরক্ষণ ",
            buttons: {
                success: {
                    label: "হ্যাঁ",
                    className: "green",
                    callback: function () {
                        $.ajax({
                            type: 'POST',
                            url: "<?php echo $this->Url->build(['controller' => 'DakActions', 'action' => 'updateStatus']) ?>",
                            data: {"ids": status},
                            success: function (data) {
                                window.location.href = '<?= $dak_action_refer_url ?>';
                            }
                        });
                    }
                },
                danger: {
                    label: "না",
                    className: "red",
                    callback: function () {
                    }
                }
            }
        });


    }
    function delete_action(element)
    {
        $(element).addClass('disabled');
        $(element).html('<i class="fa fa-spin fa-cog"></i> মুছে ফেলা হচ্ছে...');
        var id = $('#delete_id').val();
//       console.log(id);
        $.ajax({
            type: 'POST',
            url: "<?=$this->Url->build(['controller' => 'DakActions', 'action' => 'delete']) ?>",
            data: {"id": id},
            success: function (data) {
                window.location.reload();
            }
        });
    }
    $(document).on('click', '#selectAll', function () {
        if (this.checked == true) {
            $('.status').each(function () {
                if (this.checked == false) {
                    this.click();
                }
            });
        } else {
            $('.status').each(function () {
                if (this.checked == true) {
                    this.click();
                }
            });
        }
    });
</script>
