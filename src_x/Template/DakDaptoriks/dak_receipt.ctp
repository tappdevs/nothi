<div class="modal-header">
    <button type="button" data-dismiss="modal" aria-hidden="true">&times</button>
    <div class="modal-title">দাপ্তরিক আবেদনের রশিদ</div>
</div>
<div class="modal-body">
    <div  id="myDiv" style="margin:0 auto;font-size: 1em;">
        
            <h5 class="text-center"><?php echo $receipt_data['received_office_name'] ?></h5>
            
                <h6 class="text-center">দাপ্তরিক আবেদনের রশিদ</h6>
                <style>
                    #myDiv .table>tbody>tr>td, #myDiv .table>tbody>tr>th, #myDiv .table>tfoot>tr>td, #myDiv .table>tfoot>tr>th, #myDiv .table>thead>tr>td, #myDiv .table>thead>tr>th{
                        padding: 5px;
                        font-size:.8em;
                        color:black;
                    }
                </style>
            
                <table class="table table-light" style=" width: 100%">
                    <tbody>
                    <tr>
                        <td align="left" colspan="1" style="width:65px;">আবেদন নম্বর</td>

                        <td align="left">: &nbsp;<?php echo $receipt_data['dak_receipt_no'] ?></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="1">বিষয়</td>

                        <td align="left">: &nbsp;<?php printf("%s",$receipt_data['dak_subject']) ?></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="1">প্রেরকের নাম</td>

                        <td align="left">: &nbsp;<?php echo (!empty($receipt_data['sender_name'])?(h($receipt_data['sender_name']) . ', '):'') . $receipt_data['sender_officer_designation_label'] ?></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="1">দপ্তরের নাম</td>

                        <td align="left">: &nbsp;<?php echo $receipt_data['sender_office_name'] ?></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="1">তারিখ ও সময়</td>

                        <td align="left">: &nbsp;<?php echo $receipt_data['received_date'] ?></td>
                    </tr>
                    </tbody>
                </table>
            
     
    </div>
    <div class="modal-footer">
        <button type="button" class="btn blue" onclick="PrintElem('123')">
            <i class="fs1 a2i_gn_print2"></i> প্রিন্ট
        </button>
    </div>
</div>