
<style>

    .inbox .inbox-header h1 {
        margin-bottom: 0px;
    }

    .checkbox input[type=checkbox], .checkbox-inline input[type=checkbox], .radio input[type=radio], .radio-inline input[type=radio] {
        margin-left: -10px !important;
    }

    .inbox .inbox-nav li.active b {
        display: none !important;
    }

    .tooltip-inner {
        white-space: pre-line;
    }

    h3 {
        margin-top: 0px;
        margin-bottom: 0px;
    }
</style>
<?//= pr($dak_users);die;?>
<div class="portlet light">
    <div class="portlet-body">
        <div class="row inbox">
            <div class="col-md-12">
                <div class="inbox-content">
                    <div class="portlet-body">
                        <div class="inbox-header inbox-view-header">
                            <div class="col-md-8">
                                <ul class="inbox-nav nav nav-pills">
                                    <li class="">
                                        <a class="btn btn-sm btn-primary round-corner-5" data-toggle="tooltip" data-title="ডাক উত্তোলন" title="ডাক উত্তোলন" href="<?php echo $this->request->webroot ?>dakDaptoriks/uploadDak"> <i class="fs1 a2i_nt_dakmanagement4"></i> ডাক আপলোড</a>
                                    </li>
                                    <li class="draft">
                                        <a data-toggle="tooltip" data-title="খসড়া" title="খসড়া" class="btn btn-sm green round-corner-5"  href="<?php echo $this->request->webroot ?>dakDaptoriks/dakInbox/draft"> <i class="fs1 a2i_nt_dakdraft4"></i> খসড়া ডাক
                                        </a>
                                    </li>
                                    <li class="my-draft">
                                        <a data-toggle="tooltip" data-title="প্রেরিত ডাক" title="প্রেরিত ডাক" class="btn btn-sm btn-danger round-corner-5" href="<?php echo $this->request->webroot ?>dakDaptoriks/dakInbox/my-draft"> <i class="fs1 a2i_nt_cholomandak1"></i> প্রেরিত ডাক </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-4">
                                <h3 class="text-right">
                                    দাপ্তরিক <?php echo __(DAK_GROHON) ?>
                                </h3>
                            </div>
                        </div>
                        <hr/>

                       <?php echo $this->element('DakDaptoriks/upload_dak_form');  ?>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<?php
echo $this->element('OfficeSealModal/office_seal_modal');
?>

<!-- Start: Common dak setup js -->
<script src="<?php echo CDN_PATH; ?>assets/admin/layout4/scripts/dak_setup.js"
type="text/javascript"></script>
<script type="text/javascript">

    $(function () {
        DakSetup.init();
    });
</script>
<!-- End: Common dak setup js -->
<style>
    div.disabled {
        pointer-events: none;
        opacity: 0.2;
    }
</style>

<script>

    $(function () {
        FormFileUpload.init();

        if($('#sender_officer_id').val() == 0 || $('#sender_officer_id').val() == ''){
             $('.new_office_info').show();
        }
        
        if ($('#sender-officer-name').val() != 0 && $('#sender-officer-name').val() != '') {
            var org = '<?php echo (isset($dak_daptoriks['sender_officer_designation_label'])?h($dak_daptoriks['sender_officer_designation_label']):'')?>';
            var unitname = '<?php echo (isset($dak_daptoriks['sender_office_unit_name'])?h($dak_daptoriks['sender_office_unit_name']):'')?>';
            var officename = '<?php echo (isset($dak_daptoriks['sender_office_name'])?h($dak_daptoriks['sender_office_name']):'')?>';

            $('#sender_autocomplete_office_id').val((org != ''?(org + ', '):'') + (unitname != ''?( unitname + ', '):'') + (officename != ''?(officename):''));


            var dakuser = <?php echo !empty($dak_users)?json_encode($dak_users): json_encode(array());  ?>;
               $.each($('input[name=office-employee_to]'), function (i, v) {
                if ( !isEmpty($(this).data('mainOfficeUnitOrganogramId')) && $(this).data('mainOfficeUnitOrganogramId') == <?php echo !empty($dak_daptoriks['receiving_officer_designation_id'])?$dak_daptoriks['receiving_officer_designation_id']:0; ?>) {
                    v.click();
                }
            });

            $.each(dakuser, function (i, v) {
                if(i == 0){
                }
                else if (v.attention_type == 0 && !isEmpty(v.to_officer_designation_id)) {
                    $('.office_employee_checkbox#emp_' + v.to_officer_designation_id).trigger('click');
                }
            });
            setTimeout(function(){
                $('.office_employee_to:checked').trigger('click');
            },100);
        }
        $("#autocomplete_office_id").bind('blur', function () {
            DAK_FORM.generateSarokNo($(this).val());
        });

        var OfficeDataAdapter = new Bloodhound({
            datumTokenizer: function (d) {
                return d.tokens;
            },
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: getBaseUrl() + 'officeManagement/autocompleteOfficeDesignation?search_key=%QUERY'
        });
        OfficeDataAdapter.initialize();

        $('.typeahead_sender')
                .typeahead(null, {
                    name: 'datypeahead_sender',
                    displayKey: 'value',
                    source: OfficeDataAdapter.ttAdapter(),
                    hint: (Metronic.isRTL() ? false : true),
                    templates: {
                        suggestion: Handlebars.compile([
                            '<div class="media">',
                            '<div class="media-body">',
                            '<h4 style="font-size:15px;" class="media-heading">{{value}}</h4>',
                            '</div>',
                            '</div>',
                        ].join(''))
                    }
                }
                )
                .on('typeahead:opened', onOpened)
                .on('typeahead:selected', onAutocompleted)
                .on('typeahead:autocompleted', onSelected);

        $('.typeahead_receiver')
                .typeahead(null, {
                    name: 'datypeahead_receiver',
                    displayKey: 'value',
                    source: OfficeDataAdapter.ttAdapter(),
                    hint: (Metronic.isRTL() ? false : true),
                    templates: {
                        suggestion: Handlebars.compile([
                            '<div class="media">',
                            '<div class="media-body">',
                            '<h4 style="font-size:15px;" class="media-heading">{{value}}</h4>',
                            '</div>',
                            '</div>',
                        ].join(''))
                    }
                }
                )
                .on('typeahead:opened', onOpened)
                .on('typeahead:selected', onAutocompleted)
                .on('typeahead:autocompleted', onSelected);

        function onOpened($e) {
            //console.log('opened');
        }

        function onAutocompleted($e, datum) {
            DakOffice.setAutoDataIntoHiddenFields(datum, $e.currentTarget.id);
        }

        function onSelected($e, datum) {
            //console.log('selected');
        }

    });

//    $(document).off('click', '#save-seal');
    $(document).on('click', '.btn-close-seal-modal', function () {
        $('.addSeal').find('.scroller').html('');
        $('.sealDiv').html('<img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"><span>&nbsp;&nbsp;লোড করা হচ্ছে...</span>');
        $('.sealDiv').load('<?php echo $this->Url->build(['controller'=>'officeManagement','action'=>'reloadOfficeSeal',$selected_office_section['office_id'],$selected_office_section['office_unit_id']]); ?>');
    });

$(document).off('click','.deleteSeal');
    $(document).on('click','.deleteSeal', function () {
        if(confirm("আপনি কি নিশ্চিত মুছে ফেলতে চান")){
            var sealId = $(this).data('id');
            $.ajax({
                url: '<?php echo $this->request->webroot ?>officeManagement/officeSealDelete',
                data: {id:sealId},
                type: "POST",
                dataType: 'json',
                success: function (res) {
                    if(res.status=="success")
                    {
                        $('.sealDiv').load('<?php echo $this->Url->build(['controller'=>'officeManagement','action'=>'reloadOfficeSeal',$selected_office_section['office_id'],$selected_office_section['office_unit_id']]); ?>');
                    }
                    else toastr.error(res.msg);
                }
            });
        }

    });

    var DAK_FORM = {
        attached_files: [],
        generateSarokNo: function (office_id) {
            PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + '/dakDaptoriks/generateSarokNo',
                    {'office_id': office_id}, 'json',
                    function (response) {
                        $("#sender-sarok-no").val(response);
                    });
        },
        submitForm: function () {
            $("#uploadDakForm").attr('action', '<?php echo $this->request->webroot ?>dak_daptoriks/editDak/<?php echo $dak_daptoriks['id'] ?>');

            DAK_FORM.attached_files = [];
            DAK_FORM.attached_files_names = [];
            DAK_FORM.attached_files_is_main = [];
            var is_main_selected = false;
            $('.template-download').each(function () {
                // var link_td = $(this).find('td:eq(2)');
                // var href = $(link_td).find('p.name > a').attr('href');
                var href = $(this).find('a').attr('hrefed');
                DAK_FORM.attached_files.push(href);
            });
            $('.template-download .potro-attachment-input').each(function () {
                var name = $(this).val();
                if(isEmpty(name)){
                    name = $(this).attr('title');
                }
                DAK_FORM.attached_files_names.push(name);
            });
            $("#uploaded_attachments_names").val(DAK_FORM.attached_files_names);

            $("#uploaded_attachments").val(DAK_FORM.attached_files);
            $('.template-download .main-dak').each(function () {
                var is_main = $(this).val();
                if(is_main == 1){
                    is_main_selected = true;
                }
                DAK_FORM.attached_files_is_main.push(is_main);
            });
            $("#uploaded_attachments_is_main").val(DAK_FORM.attached_files_is_main);
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "positionClass": "toast-bottom-right"
            };
            if ($("#uploaded_attachments").val().length == 0) {
                toastr.error("দু:খিত! ডাক সংযুক্তি দেয়া হয়নি");
                return;
            }

            if (is_main_selected == false) {
                toastr.error("দুঃখিত! মূল ডাক নির্বাচন করা হয়নি");
                return false;
            }
            
            if($('input[name=to_officer_id]').val()=='' || $('input[name=to_officer_id]').val()==0){
                toastr.error("দু:খিত! প্রাপক বাছাই করা হয়নি");return;
            }
            
            if($('input[name=to_officer_level]').val()=='' || $('input[name=to_officer_level]').val()==0){
                toastr.error("দু:খিত! মূল প্রাপক বাছাই করা হয়নি");return;
            }
            
            if($('input[name=dak_subject]').val()=='' || $('input[name=dak_subject]').val()==0){
                $('input[name=dak_subject]').focus();
                toastr.error("দু:খিত! ডাকের বিষয় দিন");return;
            }
            
            if($('input[name=sender_officer_id]').val()=='' || $('input[name=sender_officer_id]').val()==0){
                if($('input[name=sender_office_name_new]').val()==0 || $('input[name=sender_office_name_new]').val()==''){
                    $(this).focus();
                    toastr.error("দু:খিত! প্রেরকের কার্যালয় দেয়া হয়নি");return;
                }else if($('input[name=sender_officer_designation_label_new]').val()==0 || $('input[name=sender_officer_designation_label_new]').val()==''){
                    $(this).focus();
                    toastr.error("দু:খিত! প্রেরকের পদবি দেয়া হয়নি");return;
                }
            }
            
            if($('input[name=dak_subject]').val()=='' || $('input[name=dak_subject]').val()==0){
                $('input[name=dak_subject]').focus();
                toastr.error("দু:খিত! ডাকের বিষয় দিন");return;
            }
            
            if($('input[name=sender_sarok_no]').val() != ''){
                $.ajax({
                    url: js_wb_root + 'dakDaptoriks/checkSarokDuplicate',
                    data: {'sarok_no': $('input[name=sender_sarok_no]').val(), 'id': <?php echo $dak_daptoriks['id'] ?>},
                    method: 'post',
                    dataType: 'JSON',
                    success: function(response){
                        if(response.status == 'error'){
                            $(this).focus();
                            toastr.error(response.msg);return;
                        }else{
                            $("#file_description").val($("#file_description_upload").val());
                            $("#uploadDakForm").next().find('button').attr('disabled', 'disabled');
                            $("#uploadDakForm").submit();
                        }
                    }
                });
            }else{
                $("#file_description").val($("#file_description_upload").val());
                $("#uploadDakForm").next().find('button').attr('disabled', 'disabled');
                $("#uploadDakForm").submit();
            }
            
        },
        submitAndSendForm: function () {

            $("#uploadDakForm").attr('action', '<?php echo $this->request->webroot ?>dak_daptoriks/editDakSend/<?php echo $dak_daptoriks['id'] ?>');

            DAK_FORM.attached_files = [];
            DAK_FORM.attached_files_names = [];
            DAK_FORM.attached_files_is_main = [];
            var is_main_selected = false;
            $('.template-download').each(function () {
                // var link_td = $(this).find('td:eq(2)');
                // var href = $(link_td).find('p.name > a').attr('href');
                var href = $(this).find('a').attr('hrefed');
                DAK_FORM.attached_files.push(href);
            });

            $('.template-download .potro-attachment-input').each(function () {
                var name = $(this).val();
                if(isEmpty(name)){
                    name = $(this).attr('title');
                }
                DAK_FORM.attached_files_names.push(name);
            });
            $("#uploaded_attachments_names").val(DAK_FORM.attached_files_names);
            $("#uploaded_attachments").val(DAK_FORM.attached_files);
            $('.template-download .main-dak').each(function () {
                var is_main = $(this).val();
                if(is_main == 1){
                    is_main_selected = true;
                }
                DAK_FORM.attached_files_is_main.push(is_main);
            });
            $("#uploaded_attachments_is_main").val(DAK_FORM.attached_files_is_main);

            toastr.options = {
                "closeButton": true,
                "debug": false,
                "positionClass": "toast-bottom-right"
            };
            if ($("#uploaded_attachments").val().length == 0) {
                toastr.error("দু:খিত! ডাক সংযুক্তি দেয়া হয়নি");
                return;
            }

            if (is_main_selected == false) {
                toastr.error("দুঃখিত! মূল ডাক নির্বাচন করা হয়নি");
                return false;
            }
            
            if($('input[name=to_officer_id]').val()=='' || $('input[name=to_officer_id]').val()==0){
                toastr.error("দু:খিত! প্রাপক বাছাই করা হয়নি");return;
            }
            
            if($('input[name=to_officer_level]').val()=='' || $('input[name=to_officer_level]').val()==0){
                toastr.error("দু:খিত! মূল প্রাপক বাছাই করা হয়নি");return;
            }
            
            if($('input[name=dak_subject]').val()=='' || $('input[name=dak_subject]').val()==0){
                $('input[name=dak_subject]').focus();
                toastr.error("দু:খিত! ডাকের বিষয় দিন");return;
            }
            
            if($('input[name=sender_officer_id]').val()=='' || $('input[name=sender_officer_id]').val()==0){
                if($('input[name=sender_office_name_new]').val()==0 || $('input[name=sender_office_name_new]').val()==''){
                    $(this).focus();
                    toastr.error("দু:খিত! প্রেরকের কার্যালয় দেয়া করা হয়নি");return;
                }else if($('input[name=sender_officer_designation_label_new]').val()==0 || $('input[name=sender_officer_designation_label_new]').val()==''){
                    $(this).focus();
                    toastr.error("দু:খিত! প্রেরকের পদবি দেয়া করা হয়নি");return;
                }
            }
            
            if($('input[name=dak_subject]').val()=='' || $('input[name=dak_subject]').val()==0){
                $('input[name=dak_subject]').focus();
                toastr.error("দু:খিত! ডাকের বিষয় দিন");return;
            }
            
            if($('input[name=sender_sarok_no]').val() != ''){
                $.ajax({
                    url: js_wb_root + 'dakDaptoriks/checkSarokDuplicate',
                    data: {'sarok_no': $('input[name=sender_sarok_no]').val(), 'id': <?php echo $dak_daptoriks['id'] ?>},
                    method: 'post',
                    dataType: 'JSON',
                    success: function(response){
                        if(response.status == 'error'){
                            $(this).focus();
                            toastr.error(response.msg);return;
                        }else{
                            $("#file_description").val($("#file_description_upload").val());
                            $("#uploadDakForm").next().find('button').attr('disabled', 'disabled');
                            $("#uploadDakForm").submit();
                        }
                    }
                });
            }else{
                $("#file_description").val($("#file_description_upload").val());
                $("#uploadDakForm").next().find('button').attr('disabled', 'disabled');
                $("#uploadDakForm").submit();
            }
        }
    };

    function mainDak(element) {
        $('.template-download .radio>span').removeClass('checked');
        $('.template-download .potro-attachment-input').each(function () {
            if($(this).is('[readonly]') == true){
                $(this).val("").attr("readonly", false);
            }

        });
        $(".template-download .main-dak").val(0);
        $(".template-download").css('background', '#fff');
        $(element).val(1);
        $(element).closest('.template-download').css('background', '#eef5f2');
        $(element).closest('.template-download').find('.potro-attachment-input').val("মূল ডাক").attr("readonly", true);
    }
</script>

<!-- End: JavaScript -->
<!-- The blueimp Gallery widget -->
<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
    <div class="slides">
    </div>
    <h3 class="title"></h3>
    <a class="prev">
        ÔøΩ </a>
    <a class="next">
        ÔøΩ </a>
    <a class="close white">
    </a>
    <a class="play-pause">
    </a>
    <ol class="indicator">
    </ol>
</div>
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<script id="template-upload" type="text/x-tmpl">
    {% for (var i=0, file; file=o.files[i]; i++) { %}
    <div class="col-md-2 template-upload">
        <div class="pull-right">
            <button class="btn cancel btn-link">
			    <i class="fa fa-ban" style="color:red;"></i>
		    </button>
        </div>
        <div style="padding: 10px 20px;width:151px;height:131px;">
            <span class="preview"></span>
        </div>
        <div>
            <p class="size">প্রক্রিয়াকরন চলছে...</p>
		    <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
		    <div class="progress-bar progress-bar-success" style="width:0%;"></div>
        </div>
        <div>
            <p class="name">{%=file.name%}</p>
            <strong class="error label label-danger"></strong>
        </div>
    <div>
    {% } %}
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
    {% for (var i=0, file; file=o.files[i]; i++) { %}
    <div class="col-md-2 template-download" style="border: silver solid 1px;padding: 10px;margin-left:5px;">
        <div class="pull-right">
            <button class="btn delete btn-sm btn-link" title="ফাইলটি মুছে ফেলুন" style="color:red;" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
			    <i class="fs1 a2i_gn_close2"></i>
		    </button>
        </div>
        <div>
            <label>
            <input title="মূল ডাক" class="main-dak radio" style="float:left" type="radio" name="main_dak" value="0" onclick="mainDak(this)">&nbsp;মূল ডাক
            </label>
        </div>
        <div style="padding: 10px 20px;width:151px;height:131px;">
            {% if (file.thumbnailUrl) { %}
		        <a hrefed="<?= FILE_FOLDER ?>{%=file.url%}" title="{%=file.name%}" downloaded="{%=file.name%}" onclick="doModal('imgViewer', 'ইমেজ ভিউয়ার', '<img src=<?= FILE_FOLDER ?>{%=file.url%} style=\'height:auto;width:100%\' />')">
		            <img src="<?= FILE_FOLDER ?>{%=file.thumbnailUrl%}" style="width:100%">
	            </a>
		    {% } else { %}
		        {% if (file.type == 'application/pdf') { %}
		            <a hrefed="<?= FILE_FOLDER ?>{%=file.url%}" onclick="doModal('pdfViewer', 'পিডিএভ ভিউয়ার', '<embed src=<?= FILE_FOLDER ?>{%=file.url%} style=\'height:calc(100vh - 207px);width:100%\' type={%=file.type%}></embed>')" title="{%=file.name%}" downloaded="{%=file.name%}">
			            <i class="fa fa-file-pdf-o" style="font-size: 111px;margin-top: 50px;"></i>
		            </a>
		        {% } %}
		    {% } %}
        </div>
        <div>
            <span class="size">{%=o.formatFileSize(file.size)%}</span>
        </div>
        <div>
            <input type="text" title="{%=file.name%}" class="form-control potro-attachment-input" image="{%=file.url%}" file-type="{%=file.type%}" placeholder="সংযুক্তির নাম">
        </div>
    </div>
    {% } %}
</script>

