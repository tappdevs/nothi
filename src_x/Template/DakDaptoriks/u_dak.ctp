<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fs1 a2i_ld_prostabpotro2"></i>দাপ্তরিক <?php echo __(DAK_GROHON) ?>
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"></a> <a
                href="#portlet-config" data-toggle="modal" class="config"></a> <a
                href="javascript:;" class="reload"></a> <a href="javascript:"
                                                           class="remove"></a>
        </div>
    </div>
    <div class="portlet-body form">
        <!--Start: Common Fields required only for create action -->
        <?php echo $this->Form->create('', ['type' => 'file']);
        echo $this->Form->hidden('dak_type', array('label' => false, 'class' => 'form-control', 'value' => DAK_DAPTORIK));
        echo $this->Form->hidden('dak_status', array('label' => false, 'class' => 'form-control', 'value' => 1));
        // Dak Status-> 0:draft, 1:Save, 2:Send and others.....
        ?>
        <!--End: Common Fields required only for create action -->

        <!--Start: Remaining Form elements -->
        <div class="form-body">
            <h3 class="form-section">
                <i class="fa fa-paper-plane-o"></i>&nbsp;প্রেরক
            </h3>
            <?php echo $sender_cell = $this->cell('DakOffice', ['values' => array(), 'prefix' => 'sender_']); ?>
            <h3 class="form-section">
                <i class="fa fa-inbox"></i>&nbsp;প্রাপক
            </h3>
            <?php echo $receiver_cell = $this->cell('DakOffice', ['values' => array(), 'prefix' => 'receiver_']); ?>
            <h3 class="form-section">
                <i class="fa fa-file-text-o"></i>&nbsp;ডাকের বিবরন
            </h3>

            <div class="form-horizontal">
                <div class="row">
                    <div class=" col-md-6">
                        <div class="">
                            <label class="control-label"> স্মারক নম্বর </label>
                            <?php echo $this->Form->input('sender_sarok_no', array('label' => false, 'class' => 'form-control')); ?>
                        </div>
                    </div>
                    <div class=" col-md-2">
                        <div class="">
                            <label class="control-label"> তারিখ </label>
                            <?php echo $this->Form->input('sending_date', array('type' => 'text', 'label' => false, 'class' => 'form-control', 'placeholder' => 'YYYY-mm-dd', 'value' => date('Y-m-d H:i:s'))); ?>
                        </div>
                    </div>
                    <div class=" col-md-4">
                        <div class="">
                            <label class="control-label"> মিডিয়া </label>
                            <?php echo $this->Form->input('dak_sending_media', array('type' => 'text', 'empty' => '--', 'label' => false, 'class' => 'form-control')); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-horizontal">
                <div class="row">
                    <div class="col-md-12">
                        <label class="control-label"> বিষয় </label>
                        <?php echo $this->Form->input('dak_subject', array('type' => 'text', 'label' => false, 'class' => 'form-control')); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <label class="control-label"> বিবরণ </label>
                        <?php echo $this->Form->input('dak_description', array('label' => false, 'type' => 'textarea', 'class' => 'form-control')); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-5">
                        <label class="control-label"> গোপনীয়তা </label>
                        <?php
                        $dak_security_levels = json_decode(DAK_SECRECY_TYPE, true);
                        $dak_security_levels = array_combine($dak_security_levels, $dak_security_levels);
                        echo $this->Form->input('dak_security_level', array('type' => 'select', 'empty' => '--', 'label' => false, 'class' => 'form-control', 'options' => $dak_security_levels));
                        ?>
                    </div>
                    <div class="col-md-5">
                        <label class="control-label"> অগ্রাধিকার </label>
                        <?php
                        $dak_priority_levels = json_decode(DAK_PRIORITY_TYPE, true);
                        $dak_priority_levels = array_combine($dak_priority_levels, $dak_priority_levels);
                        echo $this->Form->input('dak_priority_level', array('type' => 'select', 'empty' => '--', 'label' => false, 'class' => 'form-control', 'options' => $dak_priority_levels));
                        ?>
                    </div>
                    <div class="col-md-2">
                        <label class="control-label"> গ্রহণের তারিখ </label>
                        <?php echo $this->Form->input('receiving_date', array('label' => false, 'type' => 'text', 'class' => 'form-control', 'placeholder' => 'YYYY-mm-dd', 'value' => date('Y-m-d H:i:s'))); ?>
                    </div>
                </div>
            </div>
            <br>

            <div class="form-horizontal">
                <div class="row">
                    <div class="col-md-12">
                        <?php echo $this->Form->hidden('docketing_no', array('label' => false, 'class' => 'form-control')); ?>
                    </div>
                </div>
            </div>

            <h3 class="form-section">
                <i class="fa fa-upload"></i>&nbsp; সংযুক্তি
            </h3>
            <?php if (isset($dak_attachments) && count($dak_attachments) > 0) { ?>
                <table class="table table-bordered att_tbl" id="att_tbl1">
                    <tbody>
                    <?php
                    $i = 1;
                    foreach ($dak_attachments as $single_data):
                        ?>
                        <tr>
                            <td width="80%">
                                <div id="<?php echo 'data_' . $single_data['id']; ?>"><a class=""
                                                                                         href="<?php echo $this->request->webroot . $single_data['file_name']; ?>"><?php echo $single_data['file_name']; ?></a>
                                </div>
                            </td>
                            <td width="20%" class="text-center">
				            	<span id="file_enable_div_<?php echo $single_data['id'];?>">
				               		<a target="_blank"
                                       href="<?php echo $this->request->webroot . $single_data['file_name']; ?>"><i
                                            class="fs1 a2i_gn_view1"></i></a>
				                	<a href="javascript:;" class="trash_attachment"
                                       data-attachment-id="<?php echo $single_data['id'];?>"><i class="fs1 a2i_gn_delete2"></i></a>
				                </span>
				                <span class="file_disable_div" id="file_disable_div_<?php echo $single_data['id'];?>">
				                	<a href="javascript:;" class="reload_trash_attachment"
                                       data-attachment-id="<?php echo $single_data['id'];?>"><i class="fa fa-edit"></i></a>
				                </span>
                            </td>
                        </tr>
                        <?php
                        $i++;
                    endforeach;
                    ?>
                    </tbody>
                </table>
            <?php } ?>
            <div class="row">
                <div class="col-md-9">
                    <?= $this->Form->button('', ['type' => 'button', 'class' => 'btn green fs1 a2i_gn_add1 projapoti_add_more_file']) ?>
                </div>
            </div>
            <hr/>
            <div id="projapoti_more_file_view"></div>

            <div id="file_view_1">
                <div class="form-group form-horizontal">
                    <div class="form-group">
                        <div class="col-md-1">
                            <span id="file_si_1" class="file_si badge badge-success">1.</span>
                        </div>
                        <div class="col-md-7">
                            <?php echo $this->Form->input('file_name.1', array('name' => 'attachment[1][file_name_file]', 'div' => false, 'label' => false, 'class' => 'btn default', 'type' => 'File')); ?>
                        </div>
                        <div class="col-md-2">
                            <a class="close fileinput-exists" data-file-index="1"
                               data-dismiss-view="file_view_1"
                               onclick="DakSetup.removeFileView(this)" href="javascript:;"> </a>
                        </div>
                    </div>
                </div>
                <div class="form-group form-horizontal">
                    <div class="form-group">
                        <div class="col-md-10">
                            <?php echo $this->Form->input('file_description.1', array('name' => 'attachment[1][file_description]', 'label' => false, 'class' => 'form-control', 'placeholder' => 'File Description')); ?>
                        </div>
                    </div>
                </div>
                <?php echo $this->Form->hidden('file_dir.1', array('name' => 'attachment[1][file_dir]')); ?>
            </div>
        </div>

        <!--End: Remaining Form elements -->
        <!--Start: Form Buttons -->
        <div class="form-actions">
            <div class="row">
                <div class="col-md-6">
                    <?= $this->Form->button(__('সেভ'), ['class' => 'btn btn-primary']) ?>
                </div>
            </div>
        </div>
        <!--End: Form Buttons -->
        <?php echo $this->Form->end(); ?>
    </div>
</div>

<!-- Start: JavaScript -->
<!--START SCRIPT FOR THE TINYEDITOR-->
<script src="<?php echo CDN_PATH; ?>/js/tinymce/tinymce.min.js"></script>
<!--END SCRIPT FOR THE TINYEDITOR-->

<!-- Start: Common dak setup js -->
<script src="<?php echo CDN_PATH; ?>assets/admin/layout4/scripts/dak_setup.js"
        type="text/javascript"></script>
<script type="text/javascript">
    $(function () {
        DakSetup.init();
    });
</script>
<!-- End: Common dak setup js -->
<style>
    div.disabled {
        pointer-events: none;
        opacity: 0.2;
    }
</style>

<script>
    $(function () {
        $("#autocomplete_office_id").bind('blur', function () {
            DAK_FORM.generateSarokNo($(this).val());
        });

        var OfficeDataAdapter = new Bloodhound({
            datumTokenizer: function (d) {
                return d.tokens;
            },
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: getBaseUrl() + 'officeManagement/autocompleteOfficeDesignation?search_key=%QUERY'
        });
        OfficeDataAdapter.initialize();

        $('.typeahead_sender')
            .typeahead(null, {
                name: 'datypeahead_sender',
                displayKey: 'value',
                source: OfficeDataAdapter.ttAdapter(),
                hint: (Metronic.isRTL() ? false : true),
                templates: {
                    suggestion: Handlebars.compile([
                        '<div class="media">',
                        '<div class="media-body">',
                        '<h4 style="font-size:15px;" class="media-heading">{{value}}</h4>',
                        '</div>',
                        '</div>',
                    ].join(''))
                }
            }
        )
            .on('typeahead:opened', onOpened)
            .on('typeahead:selected', onAutocompleted)
            .on('typeahead:autocompleted', onSelected);

        $('.typeahead_receiver')
            .typeahead(null, {
                name: 'datypeahead_receiver',
                displayKey: 'value',
                source: OfficeDataAdapter.ttAdapter(),
                hint: (Metronic.isRTL() ? false : true),
                templates: {
                    suggestion: Handlebars.compile([
                        '<div class="media">',
                        '<div class="media-body">',
                        '<h4 style="font-size:15px;" class="media-heading">{{value}}</h4>',
                        '</div>',
                        '</div>',
                    ].join(''))
                }
            }
        )
            .on('typeahead:opened', onOpened)
            .on('typeahead:selected', onAutocompleted)
            .on('typeahead:autocompleted', onSelected);

        function onOpened($e) {
            //console.log('opened');
        }

        function onAutocompleted($e, datum) {
            DakOffice.setAutoDataIntoHiddenFields(datum, $e.currentTarget.id);
        }

        function onSelected($e, datum) {
            //console.log('selected');
        }
    });
</script>
<script type="text/javascript">
    var DAK_FORM = {
        generateSarokNo: function (office_id) {
            PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + '/dakDaptoriks/generateSarokNo',
                {'office_id': office_id}, 'json',
                function (response) {
                    $("#sender-sarok-no").val(response);
                });
        }
    };
</script>
<script type="text/javascript">
    tinymce.init({  //script for the tiny editor
        selector: "textarea",
        statusbar: false,
        menubar: false,
        plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste"
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | fontselect",
        font_formats:"Nikosh='Nikosh','Open Sans', sans-serif;Arial=arial;Helvetica=helvetica,Sans-serif=sans-serif;Courier New=courier new;Courier=courier,Monospace=monospace;Comic Sans MS=comic sans ms;Times New Roman=times new roman,times;Kalpurush=kalpurush;Siyamrupali=Siyamrupali;SolaimanLipi=SolaimanLipi;",
        table_default_border: "1",
        table_default_attributes: {
                                width: "30%",
                                cellpadding: "2",
                                cellspacing: "0",
                                border: "1"
        },
       table_default_styles: {
            border: "1px solid #000;"
        },
        paste_retain_style_properties: "all",
    });
</script>
<!-- End: JavaScript -->