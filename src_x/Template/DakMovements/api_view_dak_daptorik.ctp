<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2.css"/>
<style>
    .pager {
        margin-top: 0px;
    }

    .inbox .inbox-header h1 {
        margin-bottom: 0px;
    }

    .checkbox input[type=checkbox], .checkbox-inline input[type=checkbox], .radio input[type=radio], .radio-inline input[type=radio] {
        margin-left: -10px !important;
    }
    .editable{
        border: none;
        word-break:break-word;
    }

</style>
<?php
    if(empty($is_new) || $is_new == 0){
        ?>
    <div class="alert alert-danger font-lg text-center">
        <i class="fa fa-info-circle " aria-hidden="true"></i>
      প্রিয় ব্যবহারকারী, আপনি পুরনো এন্ড্রয়েড অ্যাপ ব্যবহার করছেন। দয়া করে নতুন এন্ড্রয়েড অ্যাপ ইন্সটল করুন। এন্ড্রয়েড অ্যাপ ইন্সটল করতে
      <a href=" https://play.google.com/store/apps/details?id=com.tappware.nothipro" target="_blank">
          <img src="<?= $this->request->webroot?>img/app_image.png" alt="" style="hight:40px;width:40px;">
        </a>
      ছবিতে ক্লিক করুন অথবা Google Play Store থেকে অ্যাপটির নাম (<b>eNothi System</b>) লিখেও  খুঁজে নিতে পারেন। ধন্যবাদ।
    </div>
<?php
    }
?>
<div class="inbox-header inbox-view-info" style="border-bottom: solid 1px #9A12B3 !important;">
    <h3  style="word-break:break-word;">
        বিষয়:
        <?php echo $dak_details['dak_subject']; ?>
    </h3>

</div>
<div class="well inbox-view-info" style="vertical-align: middle;margin-bottom: 5px;padding-left: 5px; padding-right: 5px;">
    <div class="row">
        <div class="col-md-2 col-sm-2 col-xs-3 ">
            <?php echo "ডকেটিং নম্বর:  "; ?>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-4 ">
            <?php echo enTobn($dak_details['docketing_no']); ?>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-5 text-right">
            <?php
            echo (isset($dak_attachments) && count($dak_attachments) > 0) ? '<i class="glyphicon glyphicon-paperclip"> </i>&nbsp;'
                    : '';
            echo $move_date;
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 col-sm-2 col-xs-3 ">
            <?php echo "স্মারক নম্বর:  "; ?>
        </div>
        <div class="col-md-10 col-sm-10 col-xs-9 ">
            <?php echo $dak_details['sender_sarok_no']; ?>
        </div>
    </div>
</div>
<div class="inbox-view-info">
    <div class="row">
        <div class="col-md-1 col-sm-2 col-xs-2">
            <span class="bold"><?php echo UTSHO.': '; ?></span>
        </div>
        <div class="col-md-10 col-sm-10 col-xs-10">
            <span class=""><?php
                echo (!empty($dak_details['sender_name']) ? ($dak_details['sender_name'])
                        : '').(!empty($dak_details['sender_officer_designation_label'])
                        ? ((!empty($dak_details['sender_name']) ? ', ' : '').$dak_details['sender_officer_designation_label'].", ")
                        : ' ').$dak_details['sender_office_name'];
                ?></span>
        </div>
    </div>
    <div class="row">
        <div class="col-md-1 col-sm-2 col-xs-2">
            <span class="bold">  <?= PREROK.": " ?></span>
        </div>
        <div class="col-md-10 col-sm-10 col-xs-10">
            <i class="icon-send"></i>
            <span class=""><?php echo $sender_office; ?></span>
        </div>
    </div>

    <?php
    if (isset($dak_action)) {
        echo '<div class="row">
                        <div class="col-md-1 col-sm-2  col-xs-2">
                          <span class="bold"> সিদ্ধান্ত: </span>
                       </div>
                        <div class="col-md-10 col-sm-10 col-xs-10">
                            <i class="icon-send"></i>
                            <span class="">'.$dak_action.'</span>
                        </div>
                    </div>';
    }
    ?>
</div>


<?php
$attachmentGroup      = array();
$attachmentGroupimage = array();
if (isset($dak_attachments) && count($dak_attachments) > 0) {
    $i = 1;
    foreach ($dak_attachments as $single_data) {
        $file_name                      = $single_data['file_name'];
        $file_name                      = explode("/", $file_name);
        $file_name                      = $file_name[count($file_name) - 1];
        if ($single_data['attachment_type'] == 'text') continue;
        $single_data['attachment_type'] = get_file_type($single_data['file_name']);
        $f_type                         = explode('/',
            $single_data['attachment_type']);
        $file_name_arr[0]               = urldecode($file_name);
        $file_name_arr[1]               = $f_type[1];

        $single_data['file_name'] = $single_data['file_name'];

        if (isset($file_name_arr[1])) {
            if (!empty($single_data['file_name'])) {
                if (strtolower($file_name_arr[1]) == 'jpg' || strtolower($file_name_arr[1])
                    == 'jpeg' || strtolower($file_name_arr[1]) == 'png') {
                    $attachmentGroupimage['image'][] = array("id" => $single_data['id'],
                        "file_type" => $single_data['attachment_type'], "file_name" => $single_data['file_name'],
                        "name" => $file_name_arr[0], 'id' => $single_data['id']);
                } elseif (strtolower($file_name_arr[1]) == 'mp3' || strtolower($file_name_arr[1])
                    == 'mpeg') {
                    $attachmentGroup['mp3'][] = array("file_type" => $single_data['attachment_type'],
                        "file_name" => $single_data['file_name'], "name" => $file_name_arr[0],
                        'id' => $single_data['id']);
                } elseif (strtolower($file_name_arr[1]) == 'wmv' || strtolower($file_name_arr[1])
                    == 'mp4' || strtolower($file_name_arr[1]) == 'mkv' || strtolower($file_name_arr[1])
                    == 'avi' || strtolower($file_name_arr[1]) == '3gp' || strtolower($file_name_arr[1])
                    == 'mpg') {
                    $attachmentGroup['video'][] = array("file_type" => $single_data['attachment_type'],
                        "file_name" => $single_data['file_name'], "name" => $file_name_arr[0],
                        'id' => $single_data['id']);
                } elseif (strtolower($file_name_arr[1]) == 'pdf') {
                    $attachmentGroup['pdf'][] = array("file_type" => $single_data['attachment_type'],
                        "file_name" => $single_data['file_name'], "name" => $file_name_arr[0],
                        'id' => $single_data['id']);
                } else
                        $attachmentGroup["other"][] = array("file_type" => $single_data['attachment_type'],
                        "file_name" => $single_data['file_name'], "name" => $file_name_arr[0],
                        'id' => $single_data['id']);
            }
        } else {
            if (!empty($single_data['file_name']))
                    $attachmentGroup["other"][] = array("file_type" => $single_data['attachment_type'],
                    "file_name" => $single_data['file_name'], 'id' => $single_data['id']);
        }
    }
}
$attachmentGroup = $attachmentGroupimage + $attachmentGroup;
?>
<div class="inbox-view-detail">
    <div class="inbox-view">
        <?php
        if (!empty($dak_details['description'])):
            ?>
            <div class="divcenter" style ="overflow: auto;">
                <div class="col-md-12 col-lg-12 col-sm-12">
                    <span class="bold">বিবরণ :</span>
                    <div class='dakBodyPotro' style="background-color: #fff; min-height: 8in;min-width: 8in; margin:0 auto 20px; page-break-inside: auto; padding: 10px; border: 1px solid #aaa;">
                        <div id="docatingNot" style="position: relative; top: 0px; left:30px; font-weight: bold; font-size:12px; color: #733E3E;">
                            <?php echo "ডকেটিং নম্বর:  ".enTobn($dak_details['docketing_no']); ?></div>
                        <?php if (!empty($dak_details['dak_cover'])): ?>
                            <div style="height: 815px; border-bottom: 2px solid #eee; margin-bottom: 10px;" >
                                <?php echo $dak_details['dak_cover']; ?>
                            </div>
                        <?php endif; ?>
                        <?php echo $dak_details['description']; ?>
                    </div>
                </div>
            </div>
            <br/>
        <?php endif; ?>

        <?php
        if (!empty($dak_details['dak_description'])):
            ?>
            <div class="divcenter" style ="overflow: auto;">
                <div class="col-md-12 col-lg-12 col-sm-12">
                    <span class="bold">বিবরণ :</span> 
                    <div class='dakBodyPotro' style="background-color: #fff; min-width: 8in; min-height: 8in; margin:0 auto 20px; page-break-inside: auto; padding: 10px; border: 1px solid #aaa;">
                        <div id="docatingNot" style="position: relative; top: 0px; left:30px; font-weight: bold; font-size:12px; color: #733E3E;">
                            <?php echo "ডকেটিং নম্বর:  ".enTobn($dak_details['docketing_no']); ?></div>
                        <?php if (!empty($dak_details['dak_cover'])){ ?>
                            <div style="height: 815px; border-bottom: 2px solid #eee; margin-bottom: 10px;" >
                                <?php echo $dak_details['dak_cover']; ?>
                            </div>
                        <?php }else{
                            if(!empty($dak_details['meta_data'])) {
                                $header = jsonA($dak_details['meta_data']);
                                echo(!empty($header['potro_header_banner']) ? ('<div style="text-align: ' . ($header['banner_position']) . '!important;"><img src="' . $this->request->webroot . 'getContent?file=' . base64_decode($header['potro_header_banner']) . '&token=' . sGenerateToken(['file' => base64_decode($header['potro_header_banner'])], ['exp' => time() + 60 * 300]) . '" style="width:' . ($header['banner_width']) . ';height: 60px;max-width: 100%;"/></div>') : '');
                            }
                        } ?>
                        <?php echo $dak_details['dak_description']; ?>
                    </div>
                </div>
            </div>
            <br/>
        <?php endif; ?>

        <div class="row">
            <div class='col-md-12 col-lg-12 col-sm-12'>
                <?php
                
                if (isset($attachmentGroup) && count($attachmentGroup) > 0) {
                    ?>
                    <div class="portlet box green" style="border-top: 1px solid #4bc75e;">
                        <div class="portlet-body">
                            <?php
                            if (!empty($attachmentGroup)) {
                                $attachmentTab = '';
                                $attachmentTab.= "<div class='table-scrollable'><table class='table table-bordered table-stripped'><tbody>";
                                $i             = 0;
                                $newKey        = 1;
                                foreach ($attachmentGroup as $key => $value) {
                                    foreach ($value as $kval => $val) {
                                        $icon = '';
                                        if ($key == 'pdf') {
                                            $icon = '<a class="link" href="'. $this->Url->build(["controller" => "dakMovements", "action" => "popupPotro", "?" => ["dak_id" => $dak_details['id'],'dak_type'=>$type, "attachment_id" => $val['id'], 'user_designation' => $designationId] + $query]) . '"> <i style="font-size:16pt!important;" class="fa fa-file-pdf-o" aria-hidden="true"></i></a>';
                                        } else if ($key == 'image') {
                                             $icon = '<a class="link" href="'. $this->Url->build(["controller" => "dakMovements", "action" => "popupPotro", "?" => ["dak_id" => $dak_details['id'],'dak_type'=>$type, "attachment_id" => $val['id'], 'user_designation' => $designationId] + $query]) . '"> <i style="font-size:16pt!important;" class="fa fa-file-image-o" aria-hidden="true"></i></a>';
                                        } else {
                                            $icon = '<a class="link" href="'. $this->Url->build(["controller" => "dakMovements", "action" => "popupPotro", "?" => ["dak_id" => $dak_details['id'],'dak_type'=>$type, "attachment_id" => $val['id'], 'user_designation' => $designationId] + $query]) . '"> <i style="font-size:16pt!important;" class="fa fa-file-text" aria-hidden="true"></i></a>';
                                        }
                                        $pop_up = '';
                                        if (!empty($val['name'])) {
                                            $pop_up = "<a class='showDetailAttach' href='{$this->Url->build(["controller" => "dakMovements",
                                                    "action" => "popupPotro",
                                                    "?" => ["dak_id" => $dak_details['id'],'dak_type'=>$type,
                                                        "attachment_id" => $val['id'],
                                                        'user_designation' => $designationId] + $query])}' title='{$val['name']}'>{$val['name']}</a>";
//                                            $pop_up = "{$val['name']}";
                                        }
                                        $attachmentTab .= "<tr ><td style='font-size:12pt!important; text-align:center;'>".(Cake\I18n\Number::format($newKey++))."</td><td  style='vertical-align:middle'>".(!empty($val['file_type']) ? $icon : "")."</td><td  style='font-size:10pt!important; word-break:break-word;'>".$pop_up ."</td></tr>";
                                    }

                                    $i++;
                                }
                                $attachmentTab .= '</tbody></table></div>';
                                echo $attachmentTab;
                            }
                            ?>
                        </div>
                    </div>
                    <?php
                } else {
                    ?>
                    <label class="label label-danger">কোন সংযুক্তি নেই</label>
                <?php } ?>
<!--                     <div class="form-group form-actions margin-top-10">
                        <div class="alert alert-info">
                            <i class="fa fa-info-circle"></i> &nbsp;পরবর্তী কার্যক্রমের জন্য উপরের <b><?= isset($attention_type)?(!empty($attention_type)? 'প্রেরণ' : 'আর্কাইভ' ) : 'প্রেরণ অথবা আর্কাইভ'?></b> বাটন ব্যবহার করুন। ধন্যবাদ।
                        </div>
                    </div>-->
            </div>
        </div>
    </div>

</div>
<!-- *********** -->
<div class="modal fade bs-modal-lg" id="responsiveOnuccedModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">

        <div class="modal-content">
            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<script type="text/javascript">
    $(document).off('click', '.songlapbutton').on('click', '.songlapbutton', function () {
        $('#responsivepotalaLog').modal('show');
        $('#responsivepotalaLog').find('.scroller').html('<img src="' + js_wb_root + 'assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp;লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');

        var id = $(this).attr('id');
        var url = $(this).data('url');
        var type = $(this).data('type');
        var officeid = parseInt($(this).data('nothi-office'));

        if(typeof(id)!='undefined'){
            $.ajax({
            url: '<?php
                echo $this->Url->build(['controller' => 'NothiMasters',
                    'action' => 'showSonglap'])
                ?>/' + id + '/' + officeid,
            method: 'post',
            dataType: 'json',
            cache: false,
            success: function (response) {

                var attachmenttype = response.attachment_type;
                if (response.attachment_type == 'text') {
                    $('#responsivepotalaLog').find('.scroller').html('<div style="background-color: #fff; max-width:950px; min-height:815px; margin:0 auto 20px; page-break-inside: auto; margin:0 auto 20px; padding-bottom: 20px;">' + response.potro_cover + "<br/>" + response.content_body + "</div>");
                } else if ((attachmenttype.substring(0, 15)) == 'application/vnd' || (attachmenttype.substring(0, 15)) == 'application/ms') {
                    $('#responsivepotalaLog').find('.scroller').html('<iframe style=" width:100%; height: 700px;" src="https://docs.google.com/gview?url=' + encodeURIComponent('<?php echo FILE_FOLDER ?>' + response.file_name) + '&embedded=true" style="width:600px; height:500px;" frameborder="0"></iframe>');

                } else if ((attachmenttype.substring(0, 5)) != 'image') {
                    $('#responsivepotalaLog').find('.scroller').html('<embed src="' + '<?php echo $this->request->webroot . 'content/' ?>' + response.file_name + '" style=" width:100%; height: 700px;" type="' + response.attachment_type + '"></embed>');
                } else {
                    $('#responsivepotalaLog').find('.scroller').html('<div class="text-center"><a href="' + '<?php echo $this->request->webroot . 'content/' ?>' + response.file_name + '" data-gallery="multiimages"  data-toggle="lightbox" data-title="পত্র নম্বর:  ' + response.nothi_potro_page_bn + '" data-footer="" ><img class="zoomimg img-responsive"  src="' + '<?php echo $this->request->webroot . 'content/' ?>' + response.file_name + '" alt="ছবি পাওয়া যায়নি"></a></div>');
                }
            },
            error: function (err, status, rspn) {
                $('#responsivepotalaLog').find('.scroller').html('');
            }
        })
        }
        else if(typeof(url)!='undefined'){
            if ((type.substring(0, 15)) == 'application/vnd' || (type.substring(0, 15)) == 'application/ms') {
                    $('#responsivepotalaLog').find('.scroller').html('<iframe style=" width:100%; height: 700px;" src="https://docs.google.com/gview?url=' + url + '&embedded=true" style="width:600px; height:500px;" frameborder="0"></iframe>');

                } else if ((type.substring(0, 5)) != 'image') {
                    $('#responsivepotalaLog').find('.scroller').html('<embed src="' + url + '" style=" width:100%; height: 700px;" type="' + type + '"></embed>');
                } else {
                    $('#responsivepotalaLog').find('.scroller').html('<div class="text-center"><a href="' + url + '" data-gallery="multiimages"  data-toggle="lightbox" data-title="সংলাগ" data-footer="" ><img class="zoomimg img-responsive"  src="' + url + '" alt="সংলাগ পাওয়া যায়নি"></a></div>');
                }
        }
    });
//
//    $('.showDetailAttach').click(function(ev){
//        ev.preventDefault();
//    })
//
    $(document).on('click', '.showforPopup', function (e) {
        e.preventDefault();
        var title = $(this).attr('title').length > 0 ? $(this).attr('title') : $(this).attr('data-original-title');
        var href = $(this).attr('href');
        var href = js_wb_root+'NothiMasters/showPopUp/'+$.trim(href)+'?nothi_part=0&token='+'<?= sGenerateToken(['file' => 0], ['exp' => time() + 60 * 300]) ?>';
        getPopUpPotro(href, title);
    });

    function getPopUpPotro(href, title) {
        $('#responsiveOnuccedModal').find('.modal-title').text('');
        $('#responsiveOnuccedModal').find('.modal-body').html('');
        PROJAPOTI.ajaxSubmitDataCallback(href, {}, 'html', function (response) {
            $('#responsiveOnuccedModal').modal('show');
            $('#responsiveOnuccedModal').find('.modal-title').text(title);

            $('#responsiveOnuccedModal').find('.modal-body').html(response);
        });

    }
</script>