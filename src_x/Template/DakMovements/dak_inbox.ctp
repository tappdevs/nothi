<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-datepicker/css/datepicker.css"/>
<link href="<?php echo CDN_PATH; ?>daptorik_preview/css/custom.css" rel="stylesheet" type="text/css"/>
<style>
    .inbox .inbox-nav li.active b {
        display: none !important;
    }

    .tooltip-inner {
        white-space: pre-line;
    }
    .list-group-item:hover{
        background-color: #E5E5E5;
    }
    .blue{
        background: ##4B8DF8!important;
    }
    .purple{
        background: ##8E44AD!important;
    }
</style>
<div class="portlet light" style="padding: 0px 0px 0px 0px;">
    <div class="portlet-body">
        <div class="row inbox">

            <div class="col-md-12">

                <div class="inbox-loading" style="display: none;"><img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp;লোড হচ্ছে। একটু অপেক্ষা করুন... </span></div>
                <div class="inbox-content">

                </div>
                <div class="inbox-details" style="display: none;">

                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade modal-purple bs-modal-lg" id="responsiveModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">

        <div class="modal-content">
            <div class="modal-header">

                <button type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div id="nothiVuktoKoron-modal" class="modal fade modal-purple" tabindex="-1" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-full">
        <div class="modal-content nothiCreateContent" style="display: none; ">
            <div class="modal-header">
                <div class="modal-title">
                    <button type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <div class="modal-title">নথি তৈরি করুন</div>
                </div>
            </div>
            <div class="modal-body">
                <div class="portlet-body form">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="scroller" style="max-height:500px;height: 100%">

                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <input type="button" style="display: none;" data-nothijato="<?php echo isset($nothijato) ? $nothijato : 0 ?>" class="btn btn-danger bntNothiListShow" value="ফেরত যান"/>
            </div>
        </div>

        <div class="modal-content nothiVuktoContent">
            <div class="modal-header">
                <div class="modal-title">
                    <button type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <div class="modal-title" id="modal_title_uposthapon_nothijaato"></div>
                </div>
            </div>
            <?= $this->Form->create('', ['id' => 'NothiVuktoKoronForm']); ?>
            <?= $this->Form->hidden('selected_id', ['id' => 'selected_dak_id']); ?>
            <?= $this->Form->hidden('nothijato', ['id' => 'nothijato_input']); ?>
            <?= $this->Form->hidden('nothi_master_id',
                ['id' => 'selected_nothi_master_id']); ?>
            <?= $this->Form->hidden('nothi_part_no', ['id' => 'selected_nothi_part_id']); ?>
            <?= $this->Form->hidden('dak_type',
                ['id' => 'selected_dak_type', 'value' => "Daptorik"]); ?>
            <div class="modal-body">

            </div>

            <div class="modal-footer">
                <input type="submit" class="btn btn-primary " value="<?php echo __(SAVE) ?>"/>
                <button id="closenothiVuktoKoron-modal" type="button" class="btn btn-danger" data-dismiss="modal">বন্ধ করুন</button>
            </div>
            <?= $this->Form->end(); ?>
        </div>
    </div>
</div>
<div id="sourceNothiVuktoKoronSingle-modal" class="modal fade modal-purple" tabindex="-1" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-full">
        <div class="modal-content sourceNothiVuktoKoronSingleContent">
            <div class="modal-header">
                <div class="modal-title">
                    <button type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <div class="modal-title">উৎস নথি তৈরি করুন</div>
                </div>
            </div>
            <?= $this->Form->create('', ['id' => 'sourceNothiVuktoKoronSingleForm']); ?>
            <?= $this->Form->hidden('selected_id', ['id' => 'selected_dak_id']); ?>
            <?= $this->Form->hidden('nothijato', ['id' => 'nothijato_input']); ?>
            <?= $this->Form->hidden('nothi_master_id', ['id' => 'selected_nothi_master_id']); ?>
            <?= $this->Form->hidden('nothi_part_no', ['id' => 'selected_nothi_part_id']); ?>
            <?= $this->Form->hidden('dak_type', ['id' => 'selected_dak_type', 'value' => "Daptorik"]); ?>
            <div class="modal-body">

            </div>

            <div class="modal-footer">
                <input type="submit" class="btn btn-primary " value="<?php echo __(SAVE) ?>"/>
                <button id="closesourceNothiVuktoKoronSingle-modal" type="button" class="btn btn-danger" data-dismiss="modal">বন্ধ করুন</button>
            </div>
            <?= $this->Form->end(); ?>
        </div>
    </div>
</div>

<div id="dakDecisionModal" class="modal fade modal-purple" role="dialog" data-backdrop="static">
    <div class="modal-dialog modal-full">

        <!-- Modal content-->
        <div class="modal-content" >
            <div class="modal-header">
                <button type="button" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">সিদ্ধান্তসমূহ <span class="font-sm"> (সিদ্ধান্ত নির্বাচন করতে নির্দিষ্ট সিদ্ধান্তর পাশে চেকবক্স নির্বাচন/চেক করুন ) </span> </h4>
            </div>
            <div class="modal-body">
                <!--<table class="table table-hover table-bordered">-->
                <table class="table table-bordered loadDakAction" style="cursor:pointer;">
<?php foreach ($dak_actions as $dak_action) { ?>
                     <tr>
                        <td >
                            <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="radio" name="Radios" onclick="putthis(<?= $dak_action['id'] ?>);"  >
                                    </label>
                            </div>
                        </td>
                         <td class="text-left" id="action<?= $dak_action['id'] ?>"><?= $dak_action['dak_action_name'] ?></td>
                     </tr>
<?php } ?>
                </table>
                <!--</table>-->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm pull-left" onclick="loadNewDakAction()" ><i class="fa fa-refresh" aria-hidden="true"></i> &nbsp;  নতুন সিদ্ধান্ত লোড করুন</button>
                <button id="closeDakDecision" type="button" class="btn btn-danger" data-dismiss="modal"><?= __('Close') ?></button>
            </div>
        </div>

    </div>
</div>
<div id="NewDakActionModal" class="modal fade modal-purple height-auto" role="dialog" data-backdrop="static">
    <div class="modal-dialog" >
        <!-- Modal content-->
        <div class="modal-content" >
            <div class="modal-header">
                <button type="button" class="closed" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">নতুন সিদ্ধান্ত যুক্ত করুন </h4>
            </div>
            <div class="modal-body" >
                <div class=" form-inline">
                    <div class="form-group">
<?= $this->Form->input('add_dak_decision',
    ['label' => false, 'class' => 'form-control input-xlarge dakactiontext', 'placeholder' => ' সিদ্ধান্ত লিখুন']) ?>
                    </div>
                    <div class="form-group">
                        <button id="addNewDakDecision" type="button" class="btn btn-primary"> সিদ্ধান্ত সংরক্ষণ করুন</button>
                    </div>
                </div>
                <div class="row" id="ExtraText">

                </div>
            </div>
            <div class="modal-footer">
                <button id="closeDakAction" type="button" class="btn btn-danger" data-dismiss="modal"><?= __('Close') ?></button>
            </div>
        </div>

    </div>
</div>
<div class="modal fade bs-modal-lg" id="responsiveOnuccedModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">

        <div class="modal-content">
            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<?= $this->element('preview_ele'); ?>
<?php
echo $this->element('OfficeSealModal/office_seal_modal');
?>
<button style="background-color: white; color: white;" id="openmodal" type="button" class="btn btn-default   btn-xs" data-toggle="modal" data-target="#dakDecisionModal" hidden>সিদ্ধান্ত</button >
<button style="background-color: white; color: white;" id="NewDakAction" type="button" class="btn btn-default   btn-xs" data-toggle="modal" data-target="#NewDakActionModal" hidden>সিদ্ধান্ত</button >
<script>
    $('#openmodal').hide();
    $('#NewDakAction').hide();
</script>
<script type="text/javascript"
src="<?php echo $this->request->webroot; ?>daptorik_preview/js/inbox_dak_movement.js?v=<?= js_css_version ?>"></script>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript"
src="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript"
src="<?php echo $this->request->webroot; ?>assets/global/plugins/datatables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo $this->request->webroot; ?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script src="<?php echo $this->request->webroot; ?>assets/global/scripts/datatable.js"></script>
<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script>
    $('#openmodal').hide();
//    $(function () {

    InboxDakMovement.init('<?php echo $dak_inbox_group; ?>');

    var OfficeDataAdapter = new Bloodhound({
        datumTokenizer: function (d) {
            return d.tokens;
        },
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: getBaseUrl() + 'officeManagement/autocompleteOfficeDesignation?search_key=%QUERY'
    });
    OfficeDataAdapter.initialize();

//    });

//    jQuery(document).ready(function () {
    $('#openmodal').hide();
    $(".form_date").datepicker({
        autoclose: true,
        isRTL: Metronic.isRTL(),
        format: "yyyy-mm-dd",
        pickerPosition: (Metronic.isRTL() ? "bottom-right" : "bottom-left")
    });
//    });


    $(document).on('click', '.dak_actions_radio', function () {
		$('.dak_actions_radio').closest('span').removeClass('checked')
		$(this).parent('span').addClass('checked')
        if ($(this).checked == true || this.checked) {

            if ($(this).val() != 0) {
                $(this).closest('.form-group').find('#dak-actions').val($(this).data('title'));
                $(this).closest('.form-group').find('#dak-actions').attr('readonly', 'readonly');
            } else {
                $(this).closest('.form-group').find('#dak-actions').val('');
                $(this).closest('.form-group').find('#dak-actions').val('');
                $(this).closest('.form-group').find('#dak-actions').removeAttr('readonly');
            }
        }
    });
    function putthis(id) {
        $('.dakactiontext').val('');
        $('.dakactiontext').val($("#action" + id).text());
        $('#closeDakDecision').click();

    }
    function clickModal() {
        $('#openmodal').click();
    }
    function NewDakAction() {
        $('#NewDakAction').click();
    }
    $("#addNewDakDecision").click(function () {
        var text = $("#add-dak-decision").val();
        $("#ExtraText").html(' <img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"><span>&nbsp;&nbsp;সংরক্ষণ করা হচ্ছে...  </span>');
          
        $.ajax({
            type: 'POST',
            url: "<?php echo $this->Url->build(['controller' => 'DakActions', 'action' => 'add']) ?>",
            data: {"text": text,"dak" : true},
            success: function (data) {
                $("#add-dak-decision").val('');
                $('#ExtraText').html('');
                $('.dakactiontext').val(text);
                toastr.success(data);
                 $('#closeDakAction').click();
            }
        });
    });
    function loadNewDakAction(){
     $(".loadDakAction").html('  <li class="list-group-item" ><div class=""> <img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"><span>&nbsp;&nbsp;লোড হচ্ছে...  </span></div></li>');
         $.ajax({
            type: 'POST',
            url: "<?php echo $this->Url->build(['controller' => 'DakActions', 'action' => 'GetDakActions']) ?>",
            data: {},
            success: function (data) {
                $('.loadDakAction').html(data.data);
            }
        });
    }

    $(function () {
        $('.date-picker').datepicker({
            rtl: Metronic.isRTL(),
            orientation: "left",
            autoclose: true,
            endDate: '<?php echo date("Y-m-d") ?>'
        });
    });

    $(document).off('click', '.showAttachmentOfContent').on('click', '.showAttachmentOfContent', function () {
        $('#responsiveOnuccedModal').modal('show');
        var file = $(this).text();
        $('#responsiveOnuccedModal').find('.modal-title').html(file);

        var url = $(this).data('url');
        var attachment_type = $(this).data('type');

        if(typeof(url)!='undefined'){

            if (attachment_type == 'text') {
                $('#responsiveOnuccedModal').find('.modal-body').html('<div style="background-color: #fff; max-width:950px; min-height:815px; max-height: 815px; margin:0 auto; page-break-inside: auto;">"'+file+'<br/>" + file + "</div>"');
            } else if ((attachment_type.substring(0, 15)) == 'application/vnd' || (attachment_type.substring(0, 15)) == 'application/ms'  || (attachment_type.substring(0, 15)) == 'application/pdf') {
                $('#responsiveOnuccedModal').find('.modal-body').html('<iframe style=" width:100%; height: 700px;" src="https://docs.google.com/gview?url='  + url + '&embedded=true" style="width:600px; height:500px;" frameborder="0"></iframe>');

            } else if ((attachment_type.substring(0, 5)) == 'image') {
                $('#responsiveOnuccedModal').find('.modal-body').html('<embed src="'+ url + '" style=" width:100%; height: 700px;" type="' + attachment_type + '"></embed>');
            } else {
                $('#responsiveOnuccedModal').find('.modal-body').html('দুঃখিত! ডাটা পাওয়া যায়নি');
            }
        }

    });
</script>