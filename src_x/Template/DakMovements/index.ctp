<div class="portlet light">
    <div class="portlet-title">
        <div class="caption"><i class="fs1 a2i_gn_details1"></i>Dak Nagorik List</div>
        <div class="tools">
            <a href="javascript:" class="collapse"></a>
            <a href="#portlet-config" data-toggle="modal" class="config"></a>
            <a href="javascript:" class="reload"></a>
            <a href="javascript:" class="remove"></a>
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                        <? //$this->Html->link('Add New Dak Nagorik', ['action' => 'add'], ['class' => 'btn btn-default']) ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="btn-group pull-right">
                        <button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i
                                class="glyphicon glyphicon-chevron-down"></i></button>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="#">Print </a></li>
                            <li><a href="#">Save as PDF </a></li>
                            <li><a href="#">Export to Excel </a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th class="text-center"><?= $this->Paginator->sort('AAAA') ?></th>
                <th class="text-center"><?= $this->Paginator->sort('AAAA') ?></th>
                <th class="text-center"><?= $this->Paginator->sort('AAAA') ?></th>
                <th class="text-center"><?= $this->Paginator->sort('AAAA') ?></th>
                <th class="actions text-center"><?= __('Actions') ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($query as $rows): ?>
                <tr>
                    <td class="text-center"><?php echo $rows['name_eng']; ?></td>
                    <td class="text-center"><?php echo $rows['mobile_no']; ?></td>
                    <td class="text-center"><?php echo $rows['email']; ?></td>
                    <td class="text-center"><?php echo $rows['remarks']; ?></td>
                    <td class="actions text-center">
                        <div class="btn-group">
                            <button class="btn dropdown-toggle fa fa-cog" data-toggle="dropdown"><i
                                    class="glyphicon glyphicon-chevron-down"></i></button>
                            <ul class="dropdown-menu pull-right">
                                <li>
                                    <?= $this->Html->link(__('Preview Dak'), ['action' => 'previewDak', $rows['id']], ['class' => 'btn btn-xs text-warning']) ?>
                                </li>
                                <li>
                                    <?= $this->Html->link(__('Edit Dak'), ['action' => 'editDak', $rows['id']], ['class' => 'btn btn-xs text-warning']) ?>
                                </li>
                                <li>
                                    <?= $this->Html->link(__('Send Dak'), ['action' => 'sendDak', $rows['id']], ['class' => 'btn btn-xs text-warning']) ?>
                                </li>
                            </ul>
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

<script
    src="<?php echo CDN_PATH; ?>assets/admin/layout4/scripts/dak_setup.js"
    type="text/javascript"></script>
<script type="text/javascript">
    $(function () {
        DakSetup.init();
    });
</script>
