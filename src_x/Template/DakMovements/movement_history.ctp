
<div class="general-item-list">
    <?php if(!empty($tracking_data)): ?>
    <div class="item-body" style="padding-bottom:10px;color: grey;"><?php echo '<b> বর্তমান অবস্থা: </b>' .__($tracking_data) ?></div>
    <?php endif; ?>
    <?php
    if (!empty($move_data)):
        foreach ($move_data as $dak_move) {
            ?>
            <div class="item">
                <div class="item-head" style="padding:10px;">
                    <div class="item-details">
                        <div class="label label-success">
                            <i class="fa fa-user"></i>
                        </div>
                        &nbsp;&nbsp;
                        <a class="item-name"
                           href="jabascript:;"><?php echo __(PREROK).": ". h($dak_move['sender']) ?></a>
                        <span class="item-label"><?php echo $dak_move['move_date_time'] ?></span>
                    </div>
                    <span class="item-status">
                        <?php
                        $dak_priority_color = "badge-success";
                        if ($dak_move['priority'] == 1) {
                            $dak_priority_color = "badge-warning";
                        }
                        if ($dak_move['priority'] == 2) {
                            $dak_priority_color = "badge-danger";
                        }
                        ?>
                        <span class="badge badge-empty badge-success"></span>
                    </span>
                </div>
                <div class="item-body" style="padding-bottom:10px;color: grey;"><?php echo '<b> সিদ্ধান্ত: </b>' .__($dak_move['value']) ?></div>
        <?php foreach ($dak_move['receiver'] as $dak_receiver) { ?>
                
                    <div class="item-details">
                        <a class="bold" href="jabascript:;"
                           style="color:grey"><?php echo h($dak_receiver); ?></a>
                    </div>
        <?php } ?>
            </div>
            <?php
        }
    endif;
    ?>
</div>
