<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2.css"/>
<style>
    .pager {
        margin-top: 0px;
    }

    .inbox .inbox-header h3 {
        margin-bottom: 0px;
    }

    .checkbox input[type=checkbox], .checkbox-inline input[type=checkbox], .radio input[type=radio], .radio-inline input[type=radio] {
        margin-left: -10px !important;
    }

    .editable {
        border: none;
        word-break: break-word;
    }

    .blue{
        background: #4B8DF8!important;
        color: #ffffff!important;
    }
    .purple{
        background: #8E44AD!important;
        color: #ffffff!important;
    }

    .purple > i,.blue > i {
        color: #ffffff!important;
    }
</style>

<div class="inbox-header inbox-view-header" style="margin-bottom: -10px!important;margin-top: -10px!important;">
    <div class="pull-left ">
        <button class="btn  btn-primary btn-sm inbox-discard-btn"><i
                    class="glyphicon glyphicon-arrow-left"></i> আগত ডাকসমূহ
        </button>
    </div>
    <div class="pull-right">
        <nav>
            <ul class="pager">
                <li class="">
                    <?php echo __("দাপ্তরিক ডাক"); ?> &nbsp; &nbsp; <b><?php echo $this->Number->format($si); ?></b> /
                    <b><?php echo $this->Number->format($totalRec); ?></b>
                </li>
                <?php
                if($si == 1){
                    $style_2="hidden";
                } else {
                    $style_2="show";
                }
                ?>
                <li><a href="#" data-si="<?php echo $si - 1; ?>" data-dak-type='Daptorik'
                       class=" showPaginateDetailsDakInbox btn btn-primary" style="background-color: #3071a9; visibility: <?= $style_2 ?> "> < </a>
                </li>
                <?php
                if($si == $totalRec){
                    $style_1="hidden";
                } else {
                    $style_1="show";
                }
                ?>
                <li><a href="#" data-si="<?php echo $si + 1; ?>" data-dak-type='Daptorik'
                       class=" showPaginateDetailsDakInbox btn btn-primary" style="background-color: #3071a9; visibility: <?= $style_1 ?> ">></a>
                </li>
            </ul>
        </nav>
    </div>
</div>
<div class="inbox-header inbox-view-info">
    <h3 class="">
        <div class="row">
            <div class="col-md-1 col-sm-2">
                বিষয়:
            </div>
            <div class="col-md-8 col-sm-8" style="word-break:break-word;">
                <?php echo h($dak_details['dak_subject']); ?>
            </div>
            <div class="col-md-3 col-sm-2 text-right">
                <?php
                if ($rollback === 1 && !($canEdit)) {
                    echo '<button class="btn btn-danger btn-sm btn-rollback-to-dak" data-toggle="tooltip" data-title="ডাক ফেরত আনুন" title="ডাক ফেরত আনুন"  data-placement="bottom"><i  class="glyphicon glyphicon-retweet"> </i></button>';
                } else if ($rollback === 0) {
                    echo '<span class="font-sm text-danger">এই ডাকটি ফেরত আনা হয়েছে</span>';
                }
                echo '<button class="btn btn-default btn-sm"  data-title="' . trim($dak_security_txt) . '"  title="' . trim($dak_security_txt) . '" ><i class="glyphicon glyphicon glyphicon-lock" ' . (h($dak_details['dak_security_level'])
                    < 2 ? '' : ($dak_details['dak_security_level'] == 2 ? 'style="color:#FFCD32;"' : (h($dak_details['dak_security_level'])
                    == 3 ? 'style="color:red;"' : ($dak_details['dak_security_level'] == 4 ? 'style="color:#881C1C;"'
                        : 'style="color:orange;"')))) . '> </i></button>';
                echo '<button class="btn btn-default btn-sm" data-title="' . trim($dak_priority_txt) . '"  title="' . trim($dak_priority_txt) . '" ><i  class="glyphicon glyphicon glyphicon-star" ' . ($dak_priority
                    < 2 ? '' : ($dak_priority == 2 ? 'style="color:#FFCD32;"' : ($dak_priority == 3 ? 'style="color:green;"'
                        : 'style="color:red;"'))) . '> </i></button>';
                ?>
            </div>
        </div>
    </h3>
</div>
<div class="well inbox-view-info"
     style="vertical-align: middle;margin-bottom: 5px;padding-left: 5px; padding-right: 5px;">
    <div class="row">
        <div class="col-md-3 col-sm-3 ">
            <?php echo "ডকেটিং নম্বর:  " . enTobn($dak_details['docketing_no']); ?>
        </div>
        <div class="col-md-4 col-sm-4 ">
            <?php echo "স্মারক নম্বর:  " . h($dak_details['sender_sarok_no']); ?>
        </div>
        <div class="col-md-3 col-sm-3 ">
            <?php
            echo (isset($dak_attachments) && count($dak_attachments) > 0) ? '<i class="glyphicon glyphicon-paperclip"> </i>'
                : '';
            echo '&nbsp;&nbsp;' . h($move_date);
            ?>
        </div>
        <div class="col-md-2 col-sm-2 text-right">
	        <div class="btn-group btn-group-round">
	            <?php
	            if (isset($dak_user[0]['to_officer_designation_id'])) {
				if ($dak_user[0]['to_officer_designation_id'] == $employee_rec['office_unit_organogram_id']) {
	            if ($canEdit) { ?>
	                <?php if($dak_details['is_summary_nothi'] == 1): ?>
	                <a class="btn green btn-sm sourceNothiVuktoKoronSingle" data-dak-type="Daptorik" title="উৎস নথিতে উপস্থাপন"
	                        data-title="উৎস নথিতে উপস্থাপন" data-id="<?php echo $dak_details['id'] ?>" data-sarok-no="<?= bnToen($dak_details['sender_sarok_no']) ?>"><i
	                            class="fs1 efile-nothi3"> </i></a>
	                <?php endif; ?>

	                <a class="btn blue btn-sm nothiVuktoKoronSingle" data-dak-type="Daptorik" title="নথিতে উপস্থাপন"
	                        data-title="নথিতে উপস্থাপন" data-id="<?php echo $dak_details['id'] ?>"><i
	                            class="fs1 a2i_gn_nothi2"> </i></a>
	                <a class="btn purple btn-sm nothiJatoKoronSingle" data-dak-type="Daptorik" title="নথিজাত"
	                        data-title="নথিজাত" data-id="<?php echo $dak_details['id'] ?>"><i
	                            class="fs1 a2i_gn_nothi3"> </i></a>
	            <?php } else  {
	                if(!empty($archive) && $archive == 'archive'){} else {
	                	?>
	                <a class="btn btn-sm deleteThisDak" data-dak-type="Daptorik" title="আর্কাইভ করুন" data-title="আর্কাইভ করুন" style="background: #dbb233;color: white;"
	                        data-placement="top" type="button" data-messageid='<?php echo $dak_details['id'] ?>'>
	                <i class=" efile-official1" aria-hidden="true"></i>
	                </a>
	            <?php }}}} ?>
	        </div>
        </div>
    </div>
</div>
<div class="inbox-view-info font-lg">
    <div class="row">
        <div class="col-md-1 col-sm-2 text-right">
            <span class="bold"><?php echo UTSHO . ': '; ?></span>
        </div>
        <div class="col-md-10 col-sm-10">
            <span class=""><?php
                echo (!empty($dak_details['sender_name']) ? (h($dak_details['sender_name']) . ", ") : '') . (!empty($dak_details['sender_officer_designation_label'])
                        ? (h($dak_details['sender_officer_designation_label']) . ", ") : ' ') . h($dak_details['sender_office_name']);
                ?></span>
        </div>
    </div>
    <div class="row">
        <div class="col-md-1 col-sm-2 text-right">
            <span class="bold">  <?= PREROK . ": " ?></span>
        </div>
        <div class="col-md-10 col-sm-10">
            <span class=""><?php echo h($sender_office); ?></span>
        </div>
    </div>

    <?php
    if (isset($dak_action)) {
        echo '<div class="row">
                        <div class="col-md-1 col-sm-2 text-right ">
                          <span class="bold"> সিদ্ধান্ত: </span>
                       </div>
                        <div class="col-md-10 col-sm-10">
                            <span class="">' . ($dak_action) . '</span>
                        </div>
                    </div>';
    }
    ?>
</div>
<?php
    echo $this->element('dak_view',['dak_attachments'=>$dak_attachments,'dak'=>$dak_details]);
?>
<br/>
<?php if ($canEdit) { ?>
    <!--<hr>-->
    <div class="row dak_action_div">

        <div class="col-md-4 col-lg-4 form-group">
            <div class="portlet light bordered">
                <div class="portlet-title ">
                    <div class="caption">
                        <label class="control-label font-lg bold font-purple"> <?php echo __("Dak Action") ?> </label>
                    </div>
                    <div class="tools">
                        <button class="btn btn-xs btn-primary  " onclick="NewDakAction();"> নতুন
                            সিদ্ধান্ত&nbsp;
                        </button>
                    </div>
                </div>
                <div class="portlet-body">
                    <?php
                    $dak_priority_levels = json_decode(DAK_PRIORITY_TYPE, true);
                    $dak_priority_levels[0] = 'অগ্রাধিকার বাছাই করুন';
                    echo $this->Form->input('dak_priority_level',
                        array('type' => 'select', 'label' => false, 'class' => 'form-control',
                            'options' => $dak_priority_levels));
                    ?>
                    <br>
                    <!--</div>-->
                    <div class="row">
                        <div class="col-md-2 col-sm-2 col-lg-2 text-center">
                            <input class="dak_actions_radio " value="1" type="radio" name="dak_actions_radio"
                                   data-title="নথিতে উপস্থাপন করুন">
                        </div>
                        <div class="col-md-10 col-sm-10 col-lg-10 ">
                            <label class=""> নথিতে উপস্থাপন করুন</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2 col-sm-2 col-lg-2 text-center ">
                            <input class="dak_actions_radio" value="2" type="radio" name="dak_actions_radio"
                                   data-title="নথিজাত করুন">
                        </div>
                        <div class="col-md-10 col-sm-10 col-lg-10 ">
                            <label class=""> নথিজাত করুন</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2 col-sm-2 col-lg-2 text-center ">
                            <input class="dak_actions_radio" value="0" type="radio" name="dak_actions_radio"
                                   data-title="ডিফল্ট সিদ্ধান্তসমূহ">
                        </div>
                        <div class="col-md-10 col-sm-10 col-lg-10">
                            <!--<span class="input-group-addon bg-purple " onclick="clickModal();" data-toggle="tooltip" title="ডিফল্ট সিদ্ধান্তসমূহ" style="cursor:pointer">-->
                            ডিফল্ট সিদ্ধান্তসমূহ&nbsp;<i class="fa fa-pencil-square-o" aria-hidden="true"
                                                         onclick="clickModal();"></i>
                            <!--</span>-->
                        </div>

                    </div>
                    <br>
                    <div class="from-group">
                        <?php
                        echo $this->Form->input('dak_actions',
                            array('label' => false, 'class' => 'form-control dakactiontext',
                                'default' => isset($dak_action) ? h($dak_action) : '','placeholder'=>'সিদ্ধান্ত লিখুন'));
                        ?>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8 col-sm-8">
            <div class="portlet light bordered">
                <div class="portlet-title ">
                    <div class="caption">
                        <label class="control-label font-lg bold font-purple"> <?php echo __("কাকে পাঠাবেন ?") ?> </label>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-12 col-lg-12 sealDiv">
                            <?php
                            echo $this->cell('DakSender', ['params' => $employee_rec]);
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <hr>


    <div class="row">
        <div class="col-md-12 col-lg-12">
            <button class="btn btn-primary blue forwardSingleBtn" dak_type="Daptorik"
                    dak_id="<?php echo $dak_details['id'] ?>"><i
                        class="a2i_nt_cholomandak2"></i>&nbsp; <?php echo __("প্রেরণ"); ?></button>
            <span class="loading_show"></span>
        </div>
    </div>
    <br/>
<?php } ?>
<div style="border-top: 4px solid #f5f6fa;margin-top:10px">
    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">

            </div>
            <div class="actions">
                <input type="button" class="btn btn-primary btn-sm a2i_nt_cholomandak2"
                       onclick="getMovementHistory(<?php echo $dak_details['id'] ?>, 'Daptorik')"
                       value="<?= __(DAK_GOTIBIDHI_NIBONDHON_BOHI) . " " . __("View") ?>"/>
            </div>
            <div class="sub-actions" style="    float: right; display: inline-block; padding: 6px 0 14px 0; display: none">
                <input type="button" class="btn btn-danger btn-sm"
                       onclick="hideMovementHistory()"
                       value="<?= __(DAK_GOTIBIDHI_NIBONDHON_BOHI) . " " .__("Cancel") ?>"/>
            </div>

        </div>
        <div class="portlet-body showHistory">

        </div>
    </div>
</div>

<div id="responsivepotalaLog" class="modal fade" tabindex="-1" aria-hidden="true" data-backdrop="static"
     data-keyboard="false">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">সংলাগ</h4>
            </div>
            <div class="modal-body" style="background-color: #828282;">
                <div class="scroller" style="height:100%; max-height: 500px;" data-always-visible="1"
                     data-rail-visible1="1">

                </div>
            </div>
        </div>
    </div>
</div>
<?php
if (!empty($dak_id)) {
    echo $this->Form->hidden('dak_id', ['value' => $dak_id, 'id' => 'dak_id']);
}
if (!empty($dak_type)) {
    echo $this->Form->hidden('dak_type', ['value' => $dak_type, 'id' => 'dak_type']);
}
?>
<script type="text/javascript">

    $(document).off('click', '.btn-close-seal-modal');
    $(document).on('click', '.btn-close-seal-modal', function () {
        $('.addSeal').find('.scroller').html('');
        $('.sealDiv').html('<img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"><span>&nbsp;&nbsp;লোড করা হচ্ছে...</span>');
        $('.sealDiv').load('<?php
            echo $this->Url->build(['controller' => 'officeManagement',
                'action' => 'reloadOfficeSeal', $employee_rec['office_id'], $employee_rec['office_unit_id']]);
            ?>');
    });

    $(document).off('click', '.deleteSeal');
    $(document).on('click', '.deleteSeal', function () {
        var sealId = $(this).data('id');
        bootbox.dialog({
            message: "আপনি কি নিশ্চিত মুছে ফেলতে চান?",
            title: "ডাক সিল মুছে ফেলুন",
            buttons: {
                success: {
                    label: "হ্যাঁ",
                    className: "green",
                    callback: function () {
                        $.ajax({
                            url: '<?php echo $this->request->webroot ?>officeManagement/officeSealDelete',
                            data: {id: sealId},
                            type: "POST",
                            dataType: 'json',
                            success: function (res) {
                                if (res.status == "success") {
                                    $('.sealDiv').load('<?php
										echo $this->Url->build(['controller' => 'officeManagement',
											'action' => 'reloadOfficeSeal', $employee_rec['office_id'], $employee_rec['office_unit_id']]);
										?>');
                                } else
                                    toastr.error(res.msg);
                            }
                        });
                    }
                },
                danger: {
                    label: "না",
                    className: "red",
                    callback: function () {
                    }
                }
            }
        });
    });

    $(document).off('click', '.songlapbutton').on('click', '.songlapbutton', function () {
        $('#responsivepotalaLog').modal('show');
        $('#responsivepotalaLog').find('.scroller').html('<img src="' + js_wb_root + 'assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp;লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');

        var id = $(this).attr('id');
        var url = $(this).data('url');
        var type = $(this).data('type');
        var officeid = parseInt($(this).data('nothi-office'));

        if (typeof(id) != 'undefined') {
            $.ajax({
                url: '<?php echo $this->Url->build(['controller' => 'NothiMasters', 'action' => 'showSonglap']) ?>/' + id + '/' + officeid,
                method: 'post',
                dataType: 'json',
                cache: false,
                success: function (response) {

                    var attachmenttype = response.attachment_type;
                    if (response.attachment_type == 'text') {
                        $('#responsivepotalaLog').find('.scroller').html('<div style="background-color: #fff; max-width:950px; min-height:815px; margin:0 auto; page-break-inside: auto;">' + response.potro_cover + "<br/>" + response.content_body + "</div>");
                    } else if ((attachmenttype.substring(0, 15)) == 'application/vnd' || (attachmenttype.substring(0, 15)) == 'application/ms') {
                        $('#responsivepotalaLog').find('.scroller').html('<iframe style=" width:100%; height: 700px;" src="https://docs.google.com/gview?url=' + encodeURIComponent('<?php echo FILE_FOLDER ?>' + response.file_name) + '&embedded=true" style="width:600px; height:500px;" frameborder="0"></iframe>');

                    } else if ((attachmenttype.substring(0, 5)) != 'image') {
                        $('#responsivepotalaLog').find('.scroller').html('<embed src="' + '<?php echo $this->request->webroot . 'content/' ?>' + response.file_name + '" style=" width:100%; height: 700px;" type="' + response.attachment_type + '"></embed>');
                    } else {
                        $('#responsivepotalaLog').find('.scroller').html('<div class="text-center"><a href="' + '<?php echo $this->request->webroot . 'content/' ?>' + response.file_name + '" data-gallery="multiimages"  data-toggle="lightbox" data-title="পত্র নম্বর:  ' + response.nothi_potro_page_bn + '" data-footer="" ><img class="zoomimg img-responsive"  src="' + '<?php echo $this->request->webroot . 'content/' ?>' + response.file_name + '" alt="ছবি পাওয়া যায়নি"></a></div>');
                    }
                },
                error: function (err, status, rspn) {
                    $('#responsivepotalaLog').find('.scroller').html('');
                }
            })
        }
        else if (typeof(url) != 'undefined') {
            if ((type.substring(0, 15)) == 'application/vnd' || (type.substring(0, 15)) == 'application/ms') {
                $('#responsivepotalaLog').find('.scroller').html('<iframe style=" width:100%; height: 700px;" src="https://docs.google.com/gview?url=' + url + '&embedded=true" style="width:600px; height:500px;" frameborder="0"></iframe>');

            } else if ((type.substring(0, 5)) != 'image') {
                $('#responsivepotalaLog').find('.scroller').html('<embed src="' + url + '" style=" width:100%; height: 700px;" type="' + type + '"></embed>');
            } else {
                $('#responsivepotalaLog').find('.scroller').html('<div class="text-center"><a href="' + url + '" data-gallery="multiimages"  data-toggle="lightbox" data-title="সংলাগ" data-footer="" ><img class="zoomimg img-responsive"  src="' + url + '" alt="সংলাগ পাওয়া যায়নি"></a></div>');
            }
        }
    });


    $('[title]').tooltip({'container':'body', });
    $('[data-title=""]').tooltip('destroy');

    Metronic.initSlimScroll('.scroller');

    $(document).ajaxStop(function() {
        $.uniform.restore(".office_employee_to");
        $.uniform.restore(".office_employee_checkbox");
    })
</script>
<?php
if (!($canEdit)):
    ?>
    <script type="text/javascript"
            src="<?= CDN_PATH ?>projapoti-nothi/js/archive_related.js?v=<?= js_css_version ?>"></script>
    <?php
endif;
?>

