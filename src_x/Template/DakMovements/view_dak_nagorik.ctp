<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2.css"/>

<style>
    .pager {
        margin-top: 0px;
    }

    .inbox .inbox-header h3 {
        margin-bottom: 0px;
    }

    .checkbox input[type=checkbox], .checkbox-inline input[type=checkbox], .radio input[type=radio], .radio-inline input[type=radio] {
        margin-left: -10px !important;
    }
    .blue{
        background: #4B8DF8!important;
        color: #ffffff!important;
    }
    .purple{
        background: #8E44AD!important;
        color: #ffffff!important;
    }

    .purple > i,.blue > i {
        color: #ffffff!important;
    }
</style>
<?php
$dakace = false;
if (!empty($dak_details['description'])){
$dakace = true;
}
?>
<div class="inbox-header inbox-view-header" style="margin-bottom: -10px!important;margin-top: -10px!important;">
    <div class="pull-left ">
        <button class="btn  btn-primary btn-sm inbox-discard-btn"><i
                    class="glyphicon glyphicon-arrow-left"></i> আগত ডাকসমূহ
        </button>
    </div>
    <div class="pull-right">
        <nav>
            <ul class="pager">
                <li>
                    <?php echo __("নাগরিক  ডাক"); ?> &nbsp; &nbsp; <b><?php echo $this->Number->format($si); ?></b> /
                    <b><?php echo $this->Number->format($totalRec); ?></b>
                </li>
                <?php
                if($si == 1){
                    $style_2="hidden";
                } else {
                    $style_2="show";
                }
                ?>
                <li><a href="#" data-si="<?php echo $si - 1; ?>" data-dak-type='Nagorik'
                       class=" showPaginateDetailsDakInbox btn btn-primary" style="background-color: #3071a9; visibility: <?= $style_2 ?> "><</a>
                </li>
                <?php
                if($si == $totalRec){
                    $style_1="hidden";
                } else {
                    $style_1="show";
                }
                ?>
                <li><a href="#" data-si="<?php echo $si + 1; ?>" data-dak-type='Nagorik'
                       class=" showPaginateDetailsDakInbox btn btn-primary" style="background-color: #3071a9; visibility: <?= $style_1 ?> "> > </a>
                </li>
            </ul>
        </nav>
    </div>
</div>
<div class="inbox-header inbox-view-info">
    <h3 class="">
        <div class="row">
            <div class="col-md-1 col-sm-2" style="word-break:break-word;">
                বিষয়:
            </div>
            <div class="col-md-8 col-sm-8">
                <?php echo h($dak_details['dak_subject']); ?>
            </div>
            <div class="col-md-3 col-sm-2 text-right">
                <?php
                if ($rollback === 1 && !($canEdit)) {
                    echo '<button class="btn btn-danger btn-sm btn-rollback-to-dak" data-toggle="tooltip" data-title="ডাকটি নিজ ডেস্কে ফেরত" title="ডাকটি নিজ ডেস্কে ফেরত"  data-placement="bottom"><i  class="glyphicon glyphicon-retweet"> </i></button>';
                } else if ($rollback === 0) {
                    echo '<span class="font-sm text-danger">এই ডাকটি ফেরত আনা হয়েছে</span>';
                }
                if($dak_details['application_origin']=='bsap'){
                    echo '<button class="btn btn-default btn-sm" title="'.$dak_details['application_origin'].'">'.strtoupper($dak_details['application_origin'])[0].'</button>';
                }
                echo '<button class="btn btn-default btn-sm"  data-title="' . $dak_security_txt . '"   title="' . $dak_security_txt . '" ><i  class="glyphicon glyphicon glyphicon-lock" ' . ($dak_details['dak_security_level']
                    < 2 ? '' : ($dak_details['dak_security_level'] == 2 ? 'style="color:#FFCD32;"' : ($dak_details['dak_security_level']
                    == 3 ? 'style="color:red;"' : ($dak_details['dak_security_level'] == 4 ? 'style="color:#881C1C;"'
                        : 'style="color:orange;"')))) . '> </i></button>';
                echo '<button class="btn btn-default btn-sm" data-title="' . $dak_priority_txt . '"  title="' . $dak_priority_txt . '"><i class="glyphicon glyphicon glyphicon-star" ' . ($dak_priority
                    < 2 ? '' : ($dak_priority == 2 ? 'style="color:#FFCD32;"' : ($dak_priority == 3 ? 'style="color:green;"'
                        : 'style="color:red;"'))) . '> </i></button>';
                ?>
            </div>
        </div>
    </h3>
</div>
<div class="well inbox-view-info"
     style="vertical-align: middle;margin-bottom: 5px;padding-left: 5px; padding-right: 5px;font-size:14px!important;">
    <div class="row">
        <div class="col-md-3 col-sm-3 ">
            <?php
            echo (isset($dak_attachments) && count($dak_attachments) > 0) ? '<i class="glyphicon glyphicon-paperclip"> </i>'
                : '';
            echo '&nbsp;&nbsp;' . h($move_date);
            ?>
        </div>
        <div class="col-md-3 col-sm-3 ">
            <?php echo "ডকেটিং নম্বর:  " . enTobn($dak_details['docketing_no']); ?>
        </div>
        <div class="col-md-4 col-sm-4 ">
            <?php echo "স্মারক নম্বর:  " . h($dak_details['sender_sarok_no']); ?>
        </div>

        <div class="col-md-2 col-sm-2 text-right">
            <?php if (isset($canEdit) && $canEdit) { ?>
                <button data-meta='<?= !empty($meta) && !empty($meta['note_id']) ? $meta['note_id'] : 0  ?>' class="btn btn-default blue btn-sm nothiVuktoKoronSingle" data-dak-type="Nagorik" title="নথিতে উপস্থাপন"
                        data-title="নথিতে উপস্থাপন" data-id="<?php echo $dak_details['id'] ?>"><i
                            class="fs1 a2i_gn_nothi2"> </i></button>
                <?php
                    if(!empty($meta) && !empty($meta['note_id'])){}else{
                ?>
                <button class="btn btn-default purple btn-sm nothiJatoKoronSingle" data-dak-type="Nagorik" title="নথিজাত"
                        data-title="নথিজাত" data-id="<?php echo $dak_details['id'] ?>"><i
                            class="fs1 a2i_gn_nothi3"> </i></button>
                <?php } ?>
            <?php } else {
            if(!empty($archive) && $archive == 'archive'){} else { ?>
                <button class="btn btn-sm deleteThisDak" data-dak-type="Nagorik" title="আর্কাইভ করুন" style="background: #dbb233;color: white;"
                        data-toggle="tooltip" data-placement="top" type="button" data-messageid='<?php echo $dak_details['id'] ?>'>
                    <i class=" efile-official1" aria-hidden="true"></i>
                </button>
            <?php }} ?>
        </div>
    </div>
</div>

<div class="inbox-view-info">
    <div class="row">
        <div class="col-md-1 col-sm-2 text-right">
            <span class="bold"><?php echo UTSHO . ': '; ?></span>
        </div>
        <div class="col-md-10 col-sm-10">
            <span class=""><?php
                echo h($dak_details['name_bng'])
                ?></span>
        </div>
    </div>
    <div class="row">
        <div class="col-md-1 col-sm-2 text-right">
            <span class="bold"><?php echo PREROK . ": "; ?></span>
        </div>
        <div class="col-md-10 col-sm-10">
            <span class=""><?php
                echo h($sender_office);
                ?></span>
        </div>
    </div>
    <?php
    if (isset($dak_action)) {
        echo '<div class="row">
                        <div class="col-md-1 col-sm-2 text-right ">
                          <span class="bold"> সিদ্ধান্ত: </span>
                       </div>
                        <div class="col-md-10 col-sm-10">
                            <span class="">' . h($dak_action) . '</span>
                        </div>
                    </div>';
    }
    ?>
</div>
<?php
echo $this->element('dak_view',['dak_attachments'=>$dak_attachments,'dak'=>$dak_details]);
?>
<br/>
<?php if (isset($canEdit) && $canEdit) { ?>
    <div class="row dak_action_div">
        <div class="col-md-4 col-lg-4 form-group">
            <div class="portlet light bordered">
                <div class="portlet-title ">
                    <div class="caption">
                        <label class="control-label font-lg bold font-purple"> <?php echo __("Dak Action") ?> </label>
                    </div>
                    <div class="tools">
                        <button class="btn btn-xs btn-primary  " onclick="NewDakAction();"> নতুন
                            সিদ্ধান্ত&nbsp;
                        </button>
                    </div>
                </div>
                <div class="portlet-body">
                    <?php
                    $dak_priority_levels = json_decode(DAK_PRIORITY_TYPE, true);
                    $dak_priority_levels[0] = 'অগ্রাধিকার বাছাই করুন';
                    echo $this->Form->input('dak_priority_level',
                        array('type' => 'select', 'label' => false, 'class' => 'form-control',
                            'options' => $dak_priority_levels));
                    ?>
                    <br>
                    <div class="row">
                        <div class="col-md-2 col-sm-2 col-lg-2 text-center">
                            <input class="dak_actions_radio " value="1" type="radio" name="dak_actions_radio"
                                   data-title="নথিতে উপস্থাপন করুন">
                        </div>
                        <div class="col-md-10 col-sm-10 col-lg-10 ">
                            <label class=""> নথিতে উপস্থাপন করুন</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2 col-sm-2 col-lg-2 text-center ">
                            <input class="dak_actions_radio" value="2" type="radio" name="dak_actions_radio"
                                   data-title="নথিজাত করুন">
                        </div>
                        <div class="col-md-10 col-sm-10 col-lg-10 ">
                            <label class=""> নথিজাত করুন</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2 col-sm-2 col-lg-2 text-center ">
                            <input class="dak_actions_radio" value="0" type="radio" name="dak_actions_radio"
                                   data-title="ডিফল্ট সিদ্ধান্তসমূহ">
                        </div>
                        <div class="col-md-10 col-sm-10 col-lg-10">
                            <!--<span class="input-group-addon bg-purple " onclick="clickModal();" data-toggle="tooltip" title="ডিফল্ট সিদ্ধান্তসমূহ" style="cursor:pointer">-->
                            <label class=""> ডিফল্ট সিদ্ধান্তসমূহ&nbsp;</label> <i class="fa fa-pencil-square-o"
                                                                                   aria-hidden="true"
                                                                                   onclick="clickModal();"></i>
                            <!--</span>-->
                        </div>

                    </div>
                    <br>
                    <div class="from-group">
                        <?php
                        echo $this->Form->input('dak_actions',
                            array('label' => false, 'class' => 'form-control dakactiontext',
                                'default' => isset($dak_action) ? $dak_action : '','placeholder'=>'সিদ্ধান্ত লিখুন'));
                        ?>

                    </div>
                </div>
            </div>

        </div>
        <div class="col-md-8 col-lg-8">
            <div class="portlet light bordered">
                <div class="portlet-title ">
                    <div class="caption">
                        <label class="control-label font-lg bold font-purple"> <?php echo __("কাকে পাঠাবেন ?") ?> </label>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-12 col-lg-12 sealDiv">

                            <?php
                            echo $this->cell('DakSender',
                                ['params' => $selected_office_section]);
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr>


    <div class="row">
        <div class="col-md-12 col-lg-12">
            <button title="<?php echo __("প্রেরণ"); ?>" class="btn btn-primary blue forwardSingleBtn" dak_type="Nagorik"
                    dak_id="<?php echo $dak_details['id'] ?>"><i
                        class="a2i_nt_cholomandak2"></i>&nbsp; <?php echo __("প্রেরণ"); ?></button><span class="loading_show"></span>
        </div>
    </div>
    <br/>
<?php } ?>
</div>
</div>
<!-- *********** -->
<div style="border-top: 4px solid #f5f6fa;margin-top:10px">
    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="a2i_nt_cholomandak2"></i><?php echo __(DAK_GOTIBIDHI_NIBONDHON_BOHI) ?>
            </div>
            <div class="actions">
                <input type="button" class="btn btn-primary btn-sm"
                       onclick="getMovementHistory(<?php echo $dak_details['id'] ?>, 'Nagorik')"
                       value="<?= __("View") ?>"/>
            </div>
            <div class="sub-actions" style="    float: right; display: inline-block; padding: 6px 0 14px 0; display: none">
                <input type="button" class="btn btn-danger btn-sm"
                       onclick="hideMovementHistory()"
                       value="<?= __("Cancel") ?>"/>
            </div>
        </div>
        <div class="portlet-body showHistory">

        </div>
    </div>
</div>

<?php
if (!empty($dak_id)) {
    echo $this->Form->hidden('dak_id', ['value' => $dak_id, 'id' => 'dak_id']);
}
if (!empty($dak_type)) {
    echo $this->Form->hidden('dak_type', ['value' => $dak_type, 'id' => 'dak_type']);
}
?>
<script type="text/javascript">

    $(document).off('click', '.btn-close-seal-modal');
    $(document).on('click', '.btn-close-seal-modal', function () {
        $('.addSeal').find('.scroller').html('');
        $('.sealDiv').html('<img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"><span>&nbsp;&nbsp;লোড করা হচ্ছে... </span>');
        $('.sealDiv').load('<?php
            echo $this->Url->build(['controller' => 'officeManagement',
                'action' => 'reloadOfficeSeal', $selected_office_section['office_id'], $selected_office_section['office_unit_id']]);
            ?>');
    });

    $(document).off('click', '.deleteSeal');
    $(document).on('click', '.deleteSeal', function () {
        if (confirm("আপনি কি নিশ্চিত মুছে ফেলতে চান")) {

            var sealId = $(this).data('id');

            $.ajax({
                url: '<?php echo $this->request->webroot ?>officeManagement/officeSealDelete',
                data: {id: sealId},
                type: "POST",
                dataType: 'json',
                success: function (res) {
                    if (res.status == "success") {
                        $('.sealDiv').load('<?php
                            echo $this->Url->build(['controller' => 'officeManagement',
                                'action' => 'reloadOfficeSeal', $selected_office_section['office_id'], $selected_office_section['office_unit_id']]);
                            ?>');
                    } else
                        toastr.error(res.msg);
                }
            });
        }

    });

    $('[data-toggle="tooltip"]').tooltip();

    Metronic.initSlimScroll('.scroller');
</script>
<?php
if ((!isset($canEdit) || !$canEdit)):
    ?>
    <script type="text/javascript"
            src="<?= CDN_PATH ?>projapoti-nothi/js/archive_related.js?v=<?= js_css_version ?>"></script>
    <?php
endif;
?>

