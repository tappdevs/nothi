<style>
    .pager {
        margin-top: 0px;
    }

    .inbox .inbox-header h1 {
        margin-bottom: 0px;
    }
</style>
<div class="inbox-header inbox-view-header" style="margin-bottom: -10px!important;margin-top: -10px!important;">
    <div class="pull-left ">
        <button class="btn btn-default  btn-sm nothijato-discard-btn font-purple">
            <i class="glyphicon glyphicon-arrow-left"></i> নথিজাত ডাকসমূহ
        </button>
    </div>
    <div class="pull-right">
        <nav>
            <ul class="pager">
                <li>
                    <?php echo __("নাগরিক  ডাক"); ?> &nbsp; &nbsp; <b><?php echo $this->Number->format($si); ?></b> /
                    <b><?php echo $this->Number->format($totalRec); ?></b>
                </li>
                <li><a href="#" data-si="<?php echo $si - 1; ?>" data-dak-type='Nagorik'
                       class=" showPaginateDetailsNothiJatoDak" <?php echo($si == 1 ? "style='visibility: hidden'" : "style='visibility: show'"); ?>><</a>
                </li>
                <li><a href="#" data-si="<?php echo $si + 1; ?>" data-dak-type='Nagorik'
                       class=" showPaginateDetailsNothiJatoDak" <?php echo($si == $totalRec ? "style='visibility: hidden'" : "style='visibility: show'"); ?>>></a>
                </li>
            </ul>
        </nav>
    </div>
</div>
<div class="inbox-header inbox-view-info">
    <h3 class="">
        <div class="row">
            <div class="col-md-1 col-sm-2">
                বিষয়:
            </div>
            <div class="col-md-8 col-sm-7" style="word-break:break-word;">
                <?php echo h($dak_daptoriks['dak_subject']) ?>
            </div>
            <div class="col-md-3 col-sm-3 text-right">
                <?php
                if($dak_daptoriks['application_origin']=='bsap'){
                    echo '<button class="btn btn-default btn-sm" title="'.$dak_daptoriks['application_origin'].'">'.strtoupper($dak_daptoriks['application_origin'])[0].'</button>';
                }
                echo '<button class="btn btn-default btn-sm"><i title="' . h($dak_security_txt) . '" class="glyphicon glyphicon glyphicon-lock" ' . ($dak_daptoriks['dak_security_level'] < 2 ? '' : ($dak_daptoriks['dak_security_level'] == 2 ? 'style="color:#FFCD32;"' : ($dak_daptoriks['dak_security_level'] == 3 ? 'style="color:red;"' : ($dak_daptoriks['dak_security_level'] == 4 ? 'style="color:#881C1C;"' : 'style="color:orange;"')))) . '> </i></button>';
                echo '<button class="btn btn-default btn-sm"><i title="' . h($dak_priority_txt) . '" class="glyphicon glyphicon glyphicon-star" ' . ($dak_priority < 2 ? '' : ($dak_priority == 2 ? 'style="color:#FFCD32;"' : ($dak_priority == 3 ? 'style="color:green;"' : 'style="color:red;"'))) . '> </i></button>';
                if (!empty($dak_daptoriks['is_rollback_to_dak']) && $dak_daptoriks['is_rollback_to_dak'] == 1) {
                    echo '<p class="font-sm text-danger">এই ডাকটি ফেরত আনা হয়েছে</p>';
                } else if ($is_revert_enabled) {
                    echo '<button class="btn btn-danger btn-sm btn-rollback-to-dak" data-toggle="tooltip" data-title="ডাক ফেরত আনুন" title="ডাক ফেরত আনুন"  data-placement="bottom"><i  class="glyphicon glyphicon-retweet"> </i></button>';
                }
                ?>
            </div>
        </div>
    </h3>
</div>
<div class="well inbox-view-info"
     style="vertical-align: middle;margin-bottom: 5px;padding-left: 5px; padding-right: 5px;">
    <div class="row">
        <div class="col-md-6 col-sm-6 ">
            <?php echo (isset($dak_attachments) && count($dak_attachments) > 0) ? '<i class="glyphicon glyphicon-paperclip"> </i>' : '';
            echo '&nbsp;&nbsp;' . h($move_date); ?>
        </div>
        <div class="col-md-6 col-sm-6">
            <?php echo "ডকেটিং নম্বর:  " . enTobn($dak_daptoriks['docketing_no']) ?>
        </div>
    </div>
</div>
<div class="inbox-view-info">
    <div class="row">
        <div class="col-md-2 col-sm-2 text-right">
            <i class="icon-send"></i>
            <span class="bold"><?php echo UTSHO . ': '; ?></span>
        </div>
        <div class="col-md-10 col-sm-10">
            <span class=""><?php echo
                h($dak_daptoriks['name_bng']);
                ?></span>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 col-sm-2 text-right">
            <span class="bold">  <?= PRAPOK . ": " ?></span>
        </div>
        <div class="col-md-10 col-sm-10">
            <i class="icon-send"></i>
            <span class=""><?php echo h($sender_office); ?></span>
        </div>
    </div>
</div>

<?php
echo $this->element('dak_view',['dak_attachments'=>$dak_attachments,'dak'=>$dak_daptoriks]);
?>
<?php
echo $this->Form->hidden('dak_id', ['value' => $dak_id, 'id' => 'dak_id']);
echo $this->Form->hidden('dak_type', ['value' => $dak_type, 'id' => 'dak_type']);
?>
<!-- *********** -->
<div style="border-top: 4px solid #f5f6fa;margin-top:10px">
    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="a2i_nt_cholomandak2"></i><?php echo __(DAK_GOTIBIDHI_NIBONDHON_BOHI) ?>
            </div>
            <div class="actions">
                <input type="button" class="btn btn-primary btn-sm"
                       onclick="getMovementHistory(<?php echo $dak_daptoriks['id'] ?>,'Nagorik')"
                       value="<?= __("View") ?>"/>
            </div>
            <div class="sub-actions" style="    float: right; display: inline-block; padding: 6px 0 14px 0; display: none">
                <input type="button" class="btn btn-danger btn-sm"
                       onclick="hideMovementHistory()"
                       value="<?= __("Cancel") ?>"/>
            </div>
        </div>
        <div class="portlet-body showHistory">

        </div>
    </div>

</div>

<script type="text/javascript"
        src="<?= CDN_PATH ?>projapoti-nothi/js/nothijato_related.js?v=<?= js_css_version ?>"></script>
<script type="text/javascript">

    Metronic.initSlimScroll('.scroller');
    $('[data-title]').tooltip({'placement':'bottom'});

</script>