<style>
    .inbox .inbox-nav li.active b {
        display: none !important;
    }

    .tooltip-inner {
        white-space: pre-line;
    }
</style>
<div class="portlet light">
    <div class="portlet-body">
        <div class="row inbox">

            <div class="col-md-12">

                <div class="inbox-loading" style="display: none;"><img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp;লোড হচ্ছে। একটু অপেক্ষা করুন... </span></div>
                <div class="inbox-content">

                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo CDN_PATH; ?>daptorik_preview/js/draft_nagorik_dak_management.js" type="text/javascript"></script>
<script>
    $(function () {

        DraftDakMovement.init();

        var OfficeDataAdapter = new Bloodhound({
            datumTokenizer: function (d) {
                return d.tokens;
            },
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: getBaseUrl() + 'officeManagement/autocompleteOfficeDesignation?search_key=%QUERY'
        });
        OfficeDataAdapter.initialize();

    });
</script>