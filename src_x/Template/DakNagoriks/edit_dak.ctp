<style>

    .inbox .inbox-header h1 {
        margin-bottom: 0px;
    }

    .checkbox input[type=checkbox], .checkbox-inline input[type=checkbox], .radio input[type=radio], .radio-inline input[type=radio] {
        margin-left: -10px !important;
    }

    .inbox .inbox-header h1 {
        margin-bottom: 0px;
    }

    .checkbox input[type=checkbox], .checkbox-inline input[type=checkbox], .radio input[type=radio], .radio-inline input[type=radio] {
        margin-left: -10px !important;
    }

    .inbox .inbox-nav li.active b {
        display: none !important;
    }

    .tooltip-inner {
        white-space: pre-line;
    }

    h3 {
        margin-top: 0px;
        margin-bottom: 0px;
    }
</style>

<div class="portlet light">
    <div class="portlet-body">
        <div class="row inbox">
            <div class="col-md-12">
                <div class="inbox-content">
                    <div class="portlet-body">
                        <div class="inbox-header inbox-view-header">
                            <div class="col-md-12 col-sm-12 col-sm-12">
                                <h3 class="text-center">
                                    নাগরিক <?php echo __(DAK_GROHON) ?>
                                </h3>
                            </div>
                        </div>
                        <hr/>

                        <div class="portlet-body form">
                            <!--Start: Common Fields required only for create action -->
                            <?php
                            echo $this->Form->create($dak_nagoriks, ['type' => 'file', 'id' => 'uploadDakForm', 'autocomplete' => 'off']);
                            echo $this->Form->hidden('dak_type', array('label' => false, 'class' => 'form-control', 'value' => DAK_NAGORIK));
                            echo $this->Form->hidden('dak_status', array('label' => false, 'class' => 'form-control', 'value' => 1));
                            ?>
                            <!--End: Common Fields required only for create action -->

                            <!--Start: Remaining Form elements -->
                            <div class="form-body">


                                <h3 class="form-section dak-heading">
                                    <?php echo __(DAK_BIBORONI) ?>
                                </h3>
                                <div class="alert alert-danger display-hide">
                                    <button class="close" data-close="alert"></button>
                                    You have some form errors. Please check below.
                                </div>
                                <div class="alert alert-success display-hide">
                                    <button class="close" data-close="alert"></button>
                                    Your form validation is successful!
                                </div>
                                <div class="form-horizontal">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-4">ন্যাশনাল আইডি</label>

                                                <div class="col-md-8">
                                                    <?php echo $this->Form->input('national_idendity_no', array('label' => false, 'class' => 'form-control', 'data-ng-model' => 'tab.nid','value' =>  !empty($dak_nagoriks['national_idendity_no'])?  $dak_nagoriks['national_idendity_no'] : 'N/A','onclick'=>'$(this).select()')); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-4">জন্ম সনদ</label>

                                                <div class="col-md-8">
                                                    <?php echo $this->Form->input('birth_registration_number', array('label' => false, 'class' => 'form-control', 'data-ng-model' => 'tab.birth_certificate','value' => !empty($dak_nagoriks['birth_registration_number'])?  $dak_nagoriks['birth_registration_number'] : 'N/A','onclick'=>'$(this).select()')); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-4">পাসপোর্ট</label>

                                                <div class="col-md-8">
                                                    <?php echo $this->Form->input('passport', array('label' => false, 'class' => 'form-control', 'data-ng-model' => 'tab.passport','value' => !empty($dak_nagoriks['passport'])?  $dak_nagoriks['passport'] : 'N/A','onclick'=>'$(this).select()')); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">নাম (বাংলা) <span class="required"> * </span></label>

                                                <div class="col-md-8">
                                                    <?php echo $this->Form->input('name_bng', array('label' => false, 'class' => 'form-control', 'data-ng-model' => 'tab.name_bangla', 'data-required' => 1)); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">নাম (ইংরেজি) </label>

                                                <div class="col-md-8">
                                                    <?php echo $this->Form->input('name_eng', array('label' => false, 'class' => 'form-control', 'data-ng-model' => 'tab.name_english')); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-4">পিতার/স্বামীর নাম (বাংলা / ইংরেজি) </label>

                                                <div class="col-md-8">
                                                    <?php echo $this->Form->input('father_name', array('label' => false, 'class' => 'form-control')); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-4">মাতার নাম (বাংলা / ইংরেজি) </label>

                                                <div class="col-md-8">
                                                    <?php echo $this->Form->input('mother_name', array('label' => false, 'class' => 'form-control')); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-4">বর্তমান ঠিকানা <span
                                                            class="required"> * </span></label>

                                                <div class="col-md-8">
                                                    <?php echo $this->Form->input('address', array('label' => false, 'type' => 'textarea', 'rows' => '5', 'cols' => '55', 'class' => 'form-control', 'data-ng-model' => 'tab.address', 'data-required' => 1)); ?>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-4">স্থায়ী ঠিকানা <span
                                                            class="required"> * </span></label>

                                                <div class="col-md-8">
                                                    <input type="checkbox" id="sameaspresent" /> বর্তমান ঠিকানা
                                                    <?php echo $this->Form->input('parmanent_address', array('label' => false, 'type' => 'textarea', 'rows' => '5', 'cols' => '55', 'class' => 'form-control', 'data-ng-model' => 'tab.parmanent_address', 'data-required' => 1)); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-4">ই-মেইল</label>

                                                <div class="col-md-8">
                                                    <?php echo $this->Form->input('email', array('label' => false, 'class' => 'form-control', 'data-ng-model' => 'tab.email', 'type' => 'email', 'data-required' => 1)); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-4">মোবাইল নম্বর
                                                    <!--<span class="required"> * </span>-->
                                                </label>

                                                <div class="col-md-8">
                                                    <?php echo $this->Form->input('mobile_no', array('label' => false, 'class' => 'form-control', 'data-ng-model' => 'tab.mobile_no', 'data-required' => 1)); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label col-md-6">জাতীয়তা</label>

                                                <div class="col-md-6">
                                                    <?php
                                                    echo $this->Form->input('nationality', array('label' => false, 'class' => 'form-control', 'options' => array(
                                                        "bangladeshi" => "বাংলাদেশী",
                                                        "Other" => 'অন্যান্য'
                                                    )));
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label col-md-4">লিঙ্গ</label>

                                                <div class="col-md-8">
                                                    <?php echo $this->Form->input('gender', array('label' => false, 'class' => 'form-control', 'options' => array("Male" => 'পুরুষ', "Female" => 'মহিলা', "Other" => 'অন্যান্য'))); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label col-md-4">ধর্ম</label>

                                                <div class="col-md-8">
                                                    <?php
                                                    echo $this->Form->input('religion', array('label' => false, 'class' => 'form-control', 'options' => array(
                                                        "islam" => "ইসলাম",
                                                        "sonaton" => "হিন্দু",
                                                        "christian" => "খ্রিষ্টান",
                                                        "buddhism" => "বৌদ্ধ",
                                                        "Other" => 'অন্যান্য'
                                                    )));
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-2">বিষয় <span class="required"> * </span></label>

                                                <div class="col-md-10">
                                                    <?php echo $this->Form->input('dak_subject', array('label' => false, 'class' => 'form-control', 'data-ng-model' => 'tab.subject', 'data-required' => 1)); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-2"><?php echo __(GOPONIOTA) ?> </label>

                                                <div class="col-md-4">
                                                    <?php
                                                    $dak_security_levels = json_decode(DAK_SECRECY_TYPE, true);

                                                    echo $this->Form->input('dak_security_level', array('type' => 'select', 'label' => false, 'class' => 'form-control', 'options' => $dak_security_levels));
                                                    ?>
                                                </div>

                                                <label class="control-label col-md-2"><?php echo __(OGRADHIKAR) ?> </label>

                                                <div class="col-md-4">
                                                    <?php
                                                    $dak_priority_levels = json_decode(DAK_PRIORITY_TYPE, true);

                                                    echo $this->Form->input('dak_priority_level', array('type' => 'select', 'label' => false, 'class' => 'form-control', 'options' => $dak_priority_levels));
                                                    ?>
                                                    </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>


                                <h3 class="form-section">
                                    <?php echo __(PRAPOK) ?>
                                </h3>

                                <div class="row">
                                    <div class="col-md-12 col-lg-12 sealDiv">
                                        <?php echo $this->cell('DakSender', ['params' => $selected_office_section]); ?>
                                    </div>
                                </div>


                                <h3 class="form-section">
                                    <?php echo __(SHONGJUKTI) ?>&nbsp;<span class="text-danger">*</span>
                                </h3>
                                <?php echo $this->Form->hidden('uploaded_attachments', ['id' => 'uploaded_attachments']) ?>
                                <?php echo $this->Form->hidden('file_description', ['id' => 'file_description']) ?>
                                <?php echo $this->Form->hidden('uploaded_attachments_names', ['id' => 'uploaded_attachments_names']) ?>
                                <?php echo $this->Form->hidden('uploaded_attachments_is_main', ['id' => 'uploaded_attachments_is_main']) ?>
                                <!--End: Form Buttons -->
                                <?php echo $this->Form->end(); ?>
                                <!-- BEGIN PAGE CONTENT-->
                                <form id="fileupload" action="<?= $this->Url->build(['_name'=>'tempUpload']) ?>"
                                      method="POST" enctype="multipart/form-data">

                                    <input type="hidden" name="module_type" value="Dak" />
                                    <input type="hidden" name="module" value="Dak" />

                                    <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
                                    <div class="col-lg-6 form-group hidden">
                                        <input type="text" name="file_description_upload" id="file_description_upload" class="form-control " placeholder="সংযুক্তির বিবরণ">
                                    </div>

                                    <div class="row fileupload-buttonbar">
                                        <div class="col-lg-12">
                                            <!-- The fileinput-button span is used to style the file input field as button -->
                                            <span class="btn green fileinput-button round-corner-5">
                                                <i class="fs1 a2i_gn_add1"></i>
                                                <span>
                                                    ফাইল যুক্ত করুন </span>
                                                <input type="file" name="files[]" multiple="" accept="image/*,.pdf">
                                            </span>
                                            <button type="button" class="btn red delete hidden">
                                                <i class="fs1 a2i_gn_delete2"></i>
                                                <span>
                                                    সব মুছে ফেলুন </span>
                                            </button>
                                            <input type="checkbox" class="toggle hidden">
                                            <!-- The global file processing state -->
                                            <span class="fileupload-process">
                                            </span>
                                        </div>
                                        <!-- The global progress information -->
                                        <div class="col-lg-12 fileupload-progress fade">
                                            <!-- The global progress bar -->
                                            <div class="progress progress-striped active" role="progressbar"
                                                 aria-valuemin="0" aria-valuemax="100">
                                                <div class="progress-bar progress-bar-success" style="width:0%;">
                                                </div>
                                            </div>
                                            <!-- The extended global progress information -->
                                            <div class="progress-extended">
                                                &nbsp;
                                            </div>
                                        </div>

                                    </div>
                                    <!-- The table listing the files available for upload/download -->
                                    <div role="presentation" class="table table-striped clearfix">
                                        <div class="files">
                                        <?php
                                        if (isset($dak_attachments) && count($dak_attachments) > 0) {
                                            foreach ($dak_attachments as $single_data) {
                                                $fileName = explode('/', $single_data['file_name']);
                                                if($single_data['attachment_type']=='text')continue;
                                                $attachmentHeaders = get_file_type($single_data['file_name']);

                                                $value = array(
                                                    'user_file_name' => $single_data['user_file_name'],
                                                    'name' => urldecode($fileName[count($fileName) - 1]),
                                                    'thumbnailUrl' => (substr($attachmentHeaders,0,5) == 'image'?
                                                        (FILE_FOLDER .$single_data['file_name'].'?token='.sGenerateToken(['file'=>$single_data['file_name']],['exp'=>time() + 60*300])) : null),
                                                    'size' => '-',
                                                    'type' => $attachmentHeaders,
                                                    'url' => FILE_FOLDER . $single_data['file_name'].'?token='.sGenerateToken(['file'=>$single_data['file_name']],['exp'=>time() + 60*300]),
                                                    'deleteUrl' => $this->Url->build(['_name'=>'secureDelete','?'=>[
                                                        'id'=>$single_data['id'],
                                                        'token'=>$temp_token
                                                    ]]),
                                                    'deleteType' => "GET",
                                                );
												?>
	                                            <div class="col-md-2 template-download" style="border: silver solid 1px;padding: 10px;margin-left:5px; <?=($single_data['is_main']==1) ?'background:#eef5f2;' : ''?>">
		                                            <div class="pull-right">
			                                            <button class="btn delete btn-sm btn-link" title="ফাইলটি মুছে ফেলুন" style="color:red;" data-type="<?=$value['deleteType']?>" data-url="<?=$value['deleteUrl']?>">
				                                            <i class="fs1 a2i_gn_close2"></i>
			                                            </button>
		                                            </div>
		                                            <div>
			                                            <label>
				                                            <input title="মূল ডাক" class="main-dak radio" style="float:left" type="radio" name="main_dak" value="<?=$single_data['is_main']?>" onclick="mainDak(this)" <?=($single_data['is_main']==1)?'checked':''?>>&nbsp;মূল ডাক
			                                            </label>
		                                            </div>
		                                            <div style="padding: 10px 20px;width:151px;height:131px;">
														<?php if(!empty($value["thumbnailUrl"])): ?>
				                                            <a hrefed="<?=$value['url']?>" title="<?=$value['name']?>" downloaded="<?=$value['name']?>" onclick="doModal('imgViewer', 'ইমেজ ভিউয়ার', '<img src=<?=$value['url']?> style=\'height:auto;width:100% \' />')">
					                                            <img src="<?=$value["thumbnailUrl"]?>" style="width:100%">
				                                            </a>
														<?php else: ?>
															<?php //if($value['type'] == 'application/pdf'): ?>
				                                            <a hrefed="<?=$value['url']?>" onclick="doModal('pdfViewer', 'পিডিএভ ভিউয়ার', '<embed src=<?=$value['url']?> style=\'height:calc(100vh - 207px);width:100% \' type=<?=$value['type']?>></embed>')" title="<?=$value['name']?>" downloaded="<?=$value['name']?>">
					                                            <i class="fa fa-file-pdf-o" style="font-size: 111px;margin-top: 50px;"></i>
				                                            </a>
															<?php //endif; ?>
														<?php endif; ?>
		                                            </div>
		                                            <div>
			                                            <span class="size"><?= (isset($value['size'])) ? $value['size'] : 0 ?></span>
		                                            </div>
		                                            <div>
			                                            <input type="text" title="<?=$value['name']?>" <?=($single_data['is_main']==1)?'value="মূল ডাক" readonly="readonly"':'value="'.$value['user_file_name'].'"'?> class="form-control potro-attachment-input" image="<?=$value['url']?>" file-type="<?=$value['type']?>" placeholder="সংযুক্তির নাম">
		                                            </div>
	                                            </div>
												<?php
                                            }
                                        }
                                        ?>
                                        </div>
                                    </div>
                                </form>
                            </div>

                            <!--End: Remaining Form elements -->
                            <!--Start: Form Buttons -->
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-9">
                                        <?= $this->Form->button(__(SAVE), ['class' => 'btn btn-primary round-corner-5', 'onclick' => 'DAK_FORM.submitForm()']) ?>
                                        <?= $this->Form->button(__(SAVE . EBONG . SEND), ['class' => 'btn purple round-corner-5', 'onclick' => 'DAK_FORM.submitAndSendForm()']) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
echo $this->element('OfficeSealModal/office_seal_modal');
?>

<!-- Start: Common dak setup js -->
<script src="<?php echo CDN_PATH; ?>assets/admin/layout4/scripts/dak_setup.js"
        type="text/javascript"></script>

<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>assets/admin/pages/css/invoice.css"/>
<!-- END PAGE LEVEL STYLES -->

<script src="<?php echo CDN_PATH; ?>assets/global/scripts/printThis.js"></script>
<div class="modal fade modal-purple" id="receiptprint" role="basic" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
                <div class="modal-title">আবেদনের রশিদ</div>
            </div>
            <div class="modal-body">
                <img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span> &nbsp;&nbsp;লোড করা হচ্ছে... </span>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(function () {
        DakSetup.init();
    });
</script>
<!-- End: Common dak setup js -->
<style>
    div.disabled {
        pointer-events: none;
        opacity: 0.2;
    }
</style>

<script>


    $(function () {
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-bottom-right"
        };
        FormFileUpload.init();
        $("#autocomplete_office_id").bind('blur', function () {
            DAK_FORM.generateSarokNo($(this).val());
        });

        if ($('#sender-officer-name').val() != 0 && $('#sender-officer-name').val() != '') {
            var org = '<?php echo (isset($dak_nagoriks['sender_officer_designation_label'])?$dak_nagoriks['sender_officer_designation_label']:'')?>';
            var unitname = '<?php echo (isset($dak_nagoriks['sender_office_unit_name'])?$dak_nagoriks['sender_office_unit_name']:'')?>';
            var officename = '<?php echo (isset($dak_nagoriks['sender_office_name'])?$dak_nagoriks['sender_office_name']:'')?>';

            $('#sender_autocomplete_office_id').val((org != ''?(org + ', '):'') + (unitname != ''?( unitname + ', '):'') + (officename != ''?(officename):''));

            var dakuser = <?php echo !empty($dak_users) ? json_encode($dak_users) : json_encode(array());  ?>;
        $.each($('input[name=office-employee_to]'), function (i, v) {
            if ( !isEmpty($(this).data('mainOfficeUnitOrganogramId')) && $(this).data('mainOfficeUnitOrganogramId') == <?php echo !empty($dak_nagoriks['receiving_officer_designation_id'])?$dak_nagoriks['receiving_officer_designation_id']:0; ?>) {
                v.click();
            }
        });

        $.each(dakuser, function (i, v) {
            if(i == 0){
            }
            else if (v.attention_type == 0 && !isEmpty(v.to_officer_designation_id)) {
                $('.office_employee_checkbox#emp_' + v.to_officer_designation_id).trigger('click');
            }
        });
        setTimeout(function(){
            $('.office_employee_to:checked').trigger('click');
        },100);
    }

        var OfficeDataAdapter = new Bloodhound({
            datumTokenizer: function (d) {
                return d.tokens;
            },
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: getBaseUrl() + 'officeManagement/autocompleteOfficeDesignation?search_key=%QUERY'
        });
        OfficeDataAdapter.initialize();

        $('.typeahead_sender')
            .typeahead(null, {
                    name: 'datypeahead_sender',
                    displayKey: 'value',
                    source: OfficeDataAdapter.ttAdapter(),
                    hint: (Metronic.isRTL() ? false : true),
                    templates: {
                        suggestion: Handlebars.compile([
                            '<div class="media">',
                            '<div class="media-body">',
                            '<h4 class="media-heading">{{value}}</h4>',
                            '</div>',
                            '</div>',
                        ].join(''))
                    }
                }
            )
            .on('typeahead:opened', onOpened)
            .on('typeahead:selected', onAutocompleted)
            .on('typeahead:autocompleted', onSelected);

        $('.typeahead_receiver')
            .typeahead(null, {
                    name: 'datypeahead_receiver',
                    displayKey: 'value',
                    source: OfficeDataAdapter.ttAdapter(),
                    hint: (Metronic.isRTL() ? false : true),
                    templates: {
                        suggestion: Handlebars.compile([
                            '<div class="media">',
                            '<div class="media-body">',
                            '<h4 class="media-heading">{{value}}</h4>',
                            '</div>',
                            '</div>',
                        ].join(''))
                    }
                }
            )
            .on('typeahead:opened', onOpened)
            .on('typeahead:selected', onAutocompleted)
            .on('typeahead:autocompleted', onSelected);

        function onOpened($e) {
            //console.log('opened');
        }

        function onAutocompleted($e, datum) {
            DakOffice.setAutoDataIntoHiddenFields(datum, $e.currentTarget.id);
        }

        function onSelected($e, datum) {
            //console.log('selected');
        }

        <?php if (!empty($sentdak)): ?>

        var link = '<?php echo $this->Url->build(['controller' => 'dakNagoriks', 'action' => 'dakReceipt', (!empty($sentdak) ? $sentdak : 0)]); ?>';
        $("#receiptprint").modal('show').find(".modal-content").load(link);
        <?php endif; ?>

    });

    function PrintElem(elem) {
        $("#myDiv").printThis();
    }
    //    $(document).off('click','#save-seal');
    $(document).on('click', '.btn-close-seal-modal', function () {
        $('.addSeal').find('.scroller').html('');
        $('.sealDiv').html('<img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"><span>&nbsp;&nbsp;লোড করা হচ্ছে...</span>');
        $('.sealDiv').load('<?php echo $this->Url->build(['controller' => 'officeManagement', 'action' => 'reloadOfficeSeal', $selected_office_section['office_id'], $selected_office_section['office_unit_id']]); ?>');
    });

    //  $(document).off('click','.deleteSeal');
    $(document).on('click', '.deleteSeal', function () {
        var that = this;
        if (confirm("আপনি কি নিশ্চিত মুছে ফেলতে চান")) {
            var sealId = $(that).data('id');
            $.ajax({
                url: '<?php echo $this->request->webroot ?>officeManagement/officeSealDelete',
                data: {id: sealId},
                type: "POST",
                dataType: 'json',
                success: function (res) {
                    if (res.status == "success")
                    {
                        $('.sealDiv').load('<?php echo $this->Url->build(['controller' => 'officeManagement', 'action' => 'reloadOfficeSeal', $selected_office_section['office_id'], $selected_office_section['office_unit_id']]); ?>');
                    } else
                        toastr.error(res.msg);
                }
            });
        }

    });

    var DAK_FORM = {
        attached_files: [],
		attached_files_names: [],
        attached_files_is_main: [],
        is_main_selected: false,
        generateSarokNo: function (office_id) {
            PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + '/DakNagoriks/generateSarokNo',
                {'office_id': office_id}, 'json',
                function (response) {
                    $("#sender-sarok-no").val(response);
                });
        },
        submitForm: function () {
            $("#uploadDakForm").attr('action', '<?php echo $this->request->webroot ?>dakNagoriks/editDak/<?php echo $dak_id ?>');

            DAK_FORM.attached_files = [];
			DAK_FORM.attached_files_names = [];
            $('.template-download').each(function () {
                // var link_td = $(this).find('td:eq(2)');
                // var href = $(link_td).find('p.name > a').attr('href');
                var href = $(this).find('a').attr('hrefed');
                DAK_FORM.attached_files.push(href);
            });
            $("#uploaded_attachments").val(DAK_FORM.attached_files);

			$('.template-download .potro-attachment-input').each(function () {
				var name = $(this).val();
				if(isEmpty(name)){
					name = $(this).attr('title');
				}
				DAK_FORM.attached_files_names.push(name);
			});
			$("#uploaded_attachments_names").val(DAK_FORM.attached_files_names);
            $('.template-download .main-dak').each(function () {
                var is_main = $(this).val();
                if(is_main == 1){
                    is_main_selected = true;
                }
                DAK_FORM.attached_files_is_main.push(is_main);
            });
            $("#uploaded_attachments_is_main").val(DAK_FORM.attached_files_is_main);

            toastr.options = {
                "closeButton": true,
                "debug": false,
                "positionClass": "toast-bottom-right"
            };
            if ($("#uploaded_attachments").val().length == 0) {
                toastr.error("দুঃখিত! ডাক সংযুক্তি দেয়া হয়নি");
                return false;
            }

            if (is_main_selected == false) {
                toastr.error("দুঃখিত! মূল ডাক নির্বাচন করা হয়নি");
                return false;
            }

            if ($('input[name=to_officer_id]').val() == '' || $('input[name=to_officer_id]').val() == 0) {
                toastr.error("দুঃখিত! প্রাপক বাছাই করা হয়নি");
                return false;
            }

            if ($('input[name=to_officer_level]').val() == '' || $('input[name=to_officer_level]').val() == 0) {

                toastr.error("দুঃখিত! মূল প্রাপক বাছাই করা হয়নি");
                return false;
            }

            if ($('input[name=dak_subject]').val() == '' || $('input[name=dak_subject]').val() == 0) {
                $('input[name=dak_subject]').focus();
                toastr.error("দুঃখিত! ডাকের বিষয় দিন");
                return false;
            }

            if ($('input[name=name_bng]').val() == '' || $('input[name=name_bng]').val() == 0) {
                $('input[name=name_bng]').focus();
                toastr.error("দুঃখিত! প্রাপকের নাম দিন");
                return false;
            }


            if ($('input[name=name_eng]').val() == '' || $('input[name=name_eng]').val() == 0) {
                $('input[name=name_eng]').val($('input[name=name_bng]').val());
//                toastr.error("দুঃখিত! আপনার নাম  দেয়া হয়নি");
//                return false;
            }

//            if ($('input[name=father_name]').val() == '' || $('input[name=father_name]').val() == 0) {
//                $('input[name=father_name]').focus();
//                toastr.error("দুঃখিত! পিতার নাম দিন");
//                return false;
//            }
//
//
//            if ($('input[name=mother_name]').val() == '' || $('input[name=mother_name]').val() == 0) {
//                $('input[name=mother_name]').focus();
//                toastr.error("দুঃখিত! মাতার নাম দিন");
//                return false;
//            }


            if ($('textarea[name=present_address]').val() == '' || $('textarea[name=present_address]').val() == 0) {
                $('textarea[name=present_address]').focus();
                toastr.error("দুঃখিত! বর্তমান ঠিকানা দিন");
                return false;
            }

            if ($('textarea[name=parmanent_address]').val() == '' || $('textarea[name=parmanent_address]').val() == 0) {
                $('textarea[name=parmanent_address]').focus();
                toastr.error("দুঃখিত! স্থায়ী ঠিকানা দিন");
                return false;
            }

            var regex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
            if(!(isEmpty($('input[name=email]').val())) && (regex.test($('input[name=email]').val()) == false) ){
                toastr.error("দুঃখিত! ইমেইল সঠিক নয়");
                return false;
            }


//            if ($('input[name=mobile_no]').val() == '' || $('input[name=mobile_no]').val() == 0) {
//                $('input[name=mobile_no]').focus();
//                toastr.error("দুঃখিত! মোবাইল নম্বর দিন");
//                return false;
//            }

            $("#uploadDakForm").next().find('button').attr('disabled', 'disabled');

            $("#file_description").val($("#file_description_upload").val());
            $("#uploadDakForm").submit();
        },
        submitAndSendForm: function () {

            $("#uploadDakForm").attr('action', '<?php echo $this->request->webroot ?>dakNagoriks/editDakSend/<?php echo $dak_id ?>');

            DAK_FORM.attached_files = [];
            DAK_FORM.attached_files_names = [];
            DAK_FORM.attached_files_is_main = [];
            var is_main_selected = false;
            $('.template-download').each(function () {
                // var link_td = $(this).find('td:eq(2)');
                // var href = $(link_td).find('p.name > a').attr('href');
                var href = $(this).find('a').attr('hrefed');
                DAK_FORM.attached_files.push(href);
            });
            $("#uploaded_attachments").val(DAK_FORM.attached_files);
            $('.template-download .potro-attachment-input').each(function () {
                var name = $(this).val();
                if(isEmpty(name)){
                    name = $(this).attr('title');
                }
                DAK_FORM.attached_files_names.push(name);
            });
            $("#uploaded_attachments_names").val(DAK_FORM.attached_files_names);
            $('.template-download .main-dak').each(function () {
                var is_main = $(this).val();
                if(is_main == 1){
                    is_main_selected = true;
                }
                DAK_FORM.attached_files_is_main.push(is_main);
            });
            $("#uploaded_attachments_is_main").val(DAK_FORM.attached_files_is_main);

            toastr.options = {
                "closeButton": true,
                "debug": false,
                "positionClass": "toast-bottom-right"
            };
            if ($("#uploaded_attachments").val().length == 0) {
                toastr.error("দুঃখিত! ডাক সংযুক্তি দেয়া হয়নি");
                return false;
            }

            if (is_main_selected == false) {
                toastr.error("দুঃখিত! মূল ডাক নির্বাচন করা হয়নি");
                return false;
            }

            if ($('input[name=to_officer_id]').val() == '' || $('input[name=to_officer_id]').val() == 0) {
                toastr.error("দুঃখিত! প্রাপক বাছাই করা হয়নি");
                return false;
            }

            if ($('input[name=to_officer_level]').val() == '' || $('input[name=to_officer_level]').val() == 0) {

                toastr.error("দুঃখিত! মূল প্রাপক বাছাই করা হয়নি");
                return false;
            }

            if ($('input[name=dak_subject]').val() == '' || $('input[name=dak_subject]').val() == 0) {
                $('input[name=dak_subject]').focus();
                toastr.error("দুঃখিত! ডাকের বিষয় দিন");
                return false;
            }

            if ($('input[name=name_bng]').val() == '' || $('input[name=name_bng]').val() == 0) {
                $('input[name=name_bng]').focus();
                toastr.error("দুঃখিত! প্রাপকের নাম দিন");
                return false;
            }

            if ($('input[name=name_eng]').val() == '' || $('input[name=name_eng]').val() == 0) {
                $('input[name=name_eng]').val($('input[name=name_bng]').val());
//                toastr.error("দুঃখিত! আপনার নাম  দেয়া হয়নি");
//                return false;
            }



//            if ($('input[name=father_name]').val() == '' || $('input[name=father_name]').val() == 0) {
//                $('input[name=father_name]').focus();
//                toastr.error("দুঃখিত! পিতার নাম দিন");
//                return false;
//            }
//
//
//            if ($('input[name=mother_name]').val() == '' || $('input[name=mother_name]').val() == 0) {
//                $('input[name=mother_name]').focus();
//                toastr.error("দুঃখিত! মাতার নাম দিন");
//                return false;
//            }


            if ($('textarea[name=present_address]').val() == '' || $('textarea[name=present_address]').val() == 0) {
                $('textarea[name=present_address]').focus();
                toastr.error("দুঃখিত! বর্তমান ঠিকানা দিন");
                return false;
            }


            if ($('textarea[name=parmanent_address]').val() == '' || $('textarea[name=parmanent_address]').val() == 0) {
                $('textarea[name=parmanent_address]').focus();
                toastr.error("দুঃখিত! স্থায়ী ঠিকানা দিন");
                return false;
            }

            var regex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
            if(!(isEmpty($('input[name=email]').val())) && (regex.test($('input[name=email]').val()) == false) ){
                toastr.error("দুঃখিত! ইমেইল সঠিক নয়");
                return false;
            }

//            if ($('input[name=mobile_no]').val() == '' || $('input[name=mobile_no]').val() == 0) {
//                $('input[name=mobile_no]').focus();
//                toastr.error("দুঃখিত! মোবাইল নম্বর দিন");
//                return false;
//            }

            $("#uploadDakForm").next().find('button').attr('disabled', 'disabled');
            $("#file_description").val($("#file_description_upload").val());
            $("#uploadDakForm").submit();
        }
    };

</script>


<script type="text/javascript">
    $(function () {
        DakNagoriks.init();
    });

    $(document).on('click', '#sameaspresent', function () {
        if (this.checked == true) {
            $('#parmanent-address').val($('#address').val()).attr('readonly', 'readonly');
        } else {
            $('#parmanent-address').val('').removeAttr('readonly');
        }
    })
</script>
<script type="text/javascript">

    var DakNagoriks = {
        national_idendity_no: 0,
        birth_registration_number: 0,
        checkNid: function () {
            DakNagoriks.national_idendity_no = $("#national-idendity-no").val();
            $("#nid-check-sign").html("<span class='glyphicon glyphicon-ok-circle'></span>");

        },
        checkBirthCertificate: function () {
            DakNagoriks.birth_registration_number = $("#birth_registration_number").val();
            $("#birth-cirtificate-check-sign").html("Checking...");

            PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + 'DakNagoriks/checkBirthCertificate', {'birth_registration_number': DakNagoriks.birth_registration_number}, 'json', function (response) {

                if (response == 1) {
                    $("#birth-cirtificate-check-sign").html("<span class='glyphicon glyphicon-ok-circle'></span>");
                } else {
                    $("#birth-cirtificate-check-sign").html("<span class='glyphicon glyphicon-remove-circle'></span>");
                }
            });

        },
        init: function () {
            $("#national-idendity-no").bind('blur', function () {
//                DakNagoriks.checkNid();
            });

            $("#birth-registration-number").bind('blur', function () {
                DakNagoriks.checkBirthCertificate();
            });
        }
    };

    $('[type="email"]').change(function () {
        if (this.value != '') {
            var regex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
            if (regex.test(this.value) == false) {
                toastr.error("দুঃখিত! ইমেইল সঠিক নয়");
                $(this).focus();
            }
        }
    });

    function mainDak(element) {
        $('.template-download .radio>span').removeClass('checked');
        $('.template-download .potro-attachment-input').each(function () {
            if($(this).is('[readonly]') == true){
                $(this).val("").attr("readonly", false);
            }

        });
        $(".template-download .main-dak").val(0);
        $(".template-download").css('background', '#fff');
        $(element).val(1);
        $(element).closest('.template-download').css('background', '#eef5f2');
        $(element).closest('.template-download').find('.potro-attachment-input').val("মূল ডাক").attr("readonly", true);
    }

</script>

<!-- End: JavaScript -->
<!-- The blueimp Gallery widget -->
<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
    <div class="slides">
    </div>
    <h3 class="title"></h3>
    <a class="prev">
        ÔøΩ </a>
    <a class="next">
        ÔøΩ </a>
    <a class="close white">
    </a>
    <a class="play-pause">
    </a>
    <ol class="indicator">
    </ol>
</div>
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<script id="template-upload" type="text/x-tmpl">
    {% for (var i=0, file; file=o.files[i]; i++) { %}
    <div class="col-md-2 template-upload">
        <div class="pull-right">
            <button class="btn cancel btn-link">
			    <i class="fa fa-ban" style="color:red;"></i>
		    </button>
        </div>
        <div style="padding: 10px 20px;width:151px;height:131px;">
            <span class="preview"></span>
        </div>
        <div>
            <p class="size">প্রক্রিয়াকরন চলছে...</p>
		    <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
		    <div class="progress-bar progress-bar-success" style="width:0%;"></div>
        </div>
        <div>
            <p class="name">{%=file.name%}</p>
            <strong class="error label label-danger"></strong>
        </div>
    <div>
    {% } %}
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
    {% for (var i=0, file; file=o.files[i]; i++) { %}
    <div class="col-md-2 template-download" style="border: silver solid 1px;padding: 10px;margin-left:5px;">
        <div class="pull-right">
            <button class="btn delete btn-sm btn-link" title="ফাইলটি মুছে ফেলুন" style="color:red;" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
			    <i class="fs1 a2i_gn_close2"></i>
		    </button>
        </div>
        <div>
            <label>
            <input title="মূল ডাক" class="main-dak radio" style="float:left" type="radio" name="main_dak" value="0" onclick="mainDak(this)">&nbsp;মূল ডাক
            </label>
        </div>
        <div style="padding: 10px 20px;width:151px;height:131px;">
            {% if (file.thumbnailUrl) { %}
		        <a hrefed="<?= FILE_FOLDER ?>{%=file.url%}" title="{%=file.name%}" downloaded="{%=file.name%}" onclick="doModal('imgViewer', 'ইমেজ ভিউয়ার', '<img src=<?= FILE_FOLDER ?>{%=file.url%} style=\'height:auto;width:100%\' />')">
		            <img src="<?= FILE_FOLDER ?>{%=file.thumbnailUrl%}" style="width:100%">
	            </a>
		    {% } else { %}
		        {% if (file.type == 'application/pdf') { %}
		            <a hrefed="<?= FILE_FOLDER ?>{%=file.url%}" onclick="doModal('pdfViewer', 'পিডিএভ ভিউয়ার', '<embed src=<?= FILE_FOLDER ?>{%=file.url%} style=\'height:calc(100vh - 207px);width:100%\' type={%=file.type%}></embed>')" title="{%=file.name%}" downloaded="{%=file.name%}">
			            <i class="fa fa-file-pdf-o" style="font-size: 111px;margin-top: 50px;"></i>
		            </a>
		        {% } %}
		    {% } %}
        </div>
        <div>
            <span class="size">{%=o.formatFileSize(file.size)%}</span>
        </div>
        <div>
            <input type="text" title="{%=file.name%}" class="form-control potro-attachment-input" image="{%=file.url%}" file-type="{%=file.type%}" placeholder="সংযুক্তির নাম">
        </div>
    </div>
    {% } %}
</script>


