<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css"
      href="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css"
      href="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css"
      href="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<link rel="stylesheet" type="text/css"
      href="<?php echo CDN_PATH; ?>assets/admin/pages/css/invoice.css"/>
<!-- END PAGE LEVEL STYLES -->

<script
    src="<?php echo CDN_PATH; ?>assets/global/scripts/printThis.js"></script>
<style>
    tfoot {
        padding: 2px;
        background-color: rgba(241, 241, 241, 0.73);
    }

    .inbox-header button {
        display: inline-block;
    }

    h3 {
        margin-top: 0px;
        margin-bottom: 0px;
    }
</style>

<div class="portlet-body">
    <div class="inbox-header inbox-view-header">
        <div class="col-md-8">
            <ul class="inbox-nav nav nav-pills">
                <li class="">
                    <a class="btn btn-sm blue" data-toggle="tooltip" data-title="ডাক উত্তোলন" title="ডাক উত্তোলন"
                       href="<?php echo $this->request->webroot ?>DakNagoriks/uploadDak"> <i
                            class="fs1 a2i_nt_dakmanagement4"></i> <?php echo __(DAK_GROHON) ?> </a>
                </li>
                <li class="draft">
                    <a data-toggle="tooltip" data-title="খসড়া" title="খসড়া" class="btn btn-sm green"
                       href="javascript:;"> <i class="fs1 a2i_nt_dakdraft4"></i> খসড়া ডাক </a>
                </li>
                <li class="my-draft">
                    <a data-toggle="tooltip" data-title="প্রেরিত ডাক" title="আপলোডকৃত প্রেরিত ডাক"
                       class="btn btn-sm purple" href="javascript:;"> <i
                            class="fs1 a2i_nt_cholomandak1"></i>আপলোডকৃত প্রেরিত ডাক </a>
                </li>
            </ul>
        </div>
        <div class="col-md-4 text-center">
            <h3> <?php if ($dak_inbox_group == "draft") { ?>খসড়া ডাকসমূহ  <?php } else { ?>আপলোডকৃত প্রেরিত ডাকসমূহ  <?php } ?></h3>
        </div>
    </div>
    <hr/>

    <div class="table-container data-table-dak">
        <table class="table table-striped table-bordered table-hover" id="datatable_dak">
            <thead>
            <tr role="row" class="filter">
                <?php if ($dak_inbox_group == "draft") { ?>
                    <td colspan=3><input type="text" class="form-control form-filter input-sm" placeholder="প্রেরক" name="receiving_officer_name" id="filter_input_2"></td>
                <?php } else { ?>
                    <td colspan=3><input type="text" class="form-control form-filter input-sm" placeholder="প্রেরক" name="receiving_officer_name" id="filter_input_2"></td>
                <?php } ?>
                <td colspan=2><input type="text" class="form-control form-filter input-sm" placeholder="বিষয়" name="dak_subject" id="filter_input_1"></td>
                <td colspan=1>
                    <?php
                    echo $this->Form->input('dak_priority_level', ['options'=>json_decode(DAK_PRIORITY_TYPE), 'type'=>'select','class'=>'form-control  form-filter input-sm ', 'label'=>false, 'empty'=>'অগ্রাধিকার বাছাই করুন'])
                    ?>
                </td>
                <td colspan=1>
                    <?php
                    echo $this->Form->input('dak_security_level', ['options'=>json_decode(DAK_SECRECY_TYPE), 'type'=>'select','class'=>'form-control  form-filter input-sm ', 'label'=>false, 'empty'=>'গোপনীয়তা বাছাই করুন'])
                    ?>
                </td>
                <td colspan="3">
                    <button class="btn btn-sm yellow filter-submit margin-bottom" id="filter_submit_btn" name="filter_submit_btn" title="<?php echo SEARCH; ?>">
                        <i class="fs1 a2i_gn_search1"></i>
                    </button>
                    <button class="btn btn-sm red filter-cancel" title="<?php echo RESET; ?>">
                        <i class="fs1 a2i_gn_reset2"></i>
                    </button>
                    <?php if ($dak_inbox_group == "draft") { ?>
                    <button class="btn btn-sm blue button-forward-all" title="নির্বাচিত ডাক প্রেরণ করুন">
                            <i class="a2i_nt_cholomandak2"></i>
                        </button>
                    <?php } ?>
                </td>
            </tr>
            <tr role="row" class="heading">
                <?php if ($dak_inbox_group == "draft") { ?>
                    <th width="5%">
                        <input type="checkbox" id="selectAllDraft" />
                    </th>
                    <th width="5%">

                    </th>
                    <th width="5%">

                    </th>
                <?php } else { ?>
                    <th width="5%">
                        <!--<input type="checkbox" class="group-checkable">-->
                    </th>
                <?php } ?>
                <th style='width: 10%'>তারিখ</th>
                <th style='width: 15%'>প্রেরক</th>
                <th style='width: 15%'>প্রাপক</th>
                <th style='width: 20%'>বিষয়</th>
                <th style='width: 10%'>ধরন</th>
                <th style='width: 8%'>সংযুক্তি</th>
                <th style='width: 7%'>প্রিন্ট</th>

            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<!-- Store checked dak ids in a field for draft sending -->
<?php echo $this->Form->hidden('selected_dak_ids', ['id' => 'selected_dak_ids']) ?>
</div>
<div class="modal fade modal-purple height-auto" id="ajax" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
                <div class="modal-title">আবেদনের রশিদ</div>
            </div>
            <div class="modal-body">
                <img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif"
                     alt="" class="loading"> <span> &nbsp;&nbsp;লোড করা হচ্ছে... </span>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">

    function PrintElem(elem) {
        $("#myDiv").printThis();
    }

</script>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript"
        src="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript"
        src="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript"
        src="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script
    src="<?php echo CDN_PATH; ?>assets/global/scripts/datatable.js"></script>
<script
    src="<?php echo CDN_PATH; ?>daptorik_preview/js/tbl_dak_list_ajax.js"></script>

<script
    src="<?php echo CDN_PATH; ?>assets/admin/pages/scripts/table-advanced.js"></script>

<script type="text/javascript">

    $.extend(true, $.fn.dataTable.defaults, {
        "ordering": false
    });

    TableAjax.init('<?php echo $dak_inbox_group ?>', 'DakNagoriks/daklist/');

    $('.forward-message').on('click', function () {
        $(this).closest('div').find('.dak_sender_cell_list').toggle();
    });

    $('[data-toggle="tooltip"]').tooltip({'placement':'bottom'});
    $('[title]').tooltip({'placement':'bottom'});

    $(document).on('click','#selectAllDraft',function(){
        if(this.checked==true){
            $('.dak_draft_list_checkbox_to_select').each(function(){
                if(this.checked == false){
                    this.click();
                }
            });
        }else{
            $('.dak_draft_list_checkbox_to_select').each(function(){
                if(this.checked == true){
                    this.click();
                }
            });
        }
    });


</script>