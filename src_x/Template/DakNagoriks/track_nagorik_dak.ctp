<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-datepicker/css/datepicker.css"/>

<div class="row">
<div class="col-md-12 col-sm-12 col-md-offset-0 col-sm-offset-12">
        <div class="panel panel-success">
            <div class="panel-heading">
                <div class="panel-title">
                    <?php echo __("নাগরিক ডাক ট্র্যাকিং") ?>
                </div>
            </div>
            <div class="panel-body">
                <div class="row form-group">
                    <div class="col-md-4 col-xs-12">
                        <?php
                        echo $this->Form->input('dak_received_no', ['label' => "আবেদন গ্রহণ নম্বর", "class" => 'form-control', 'value' => (isset($referenceCode) ? $referenceCode : '')])
                        ?>
                    </div>

                    <div class="col-md-4 col-xs-12">
                        <?php
                        echo $this->Form->input('mobile_no', ['label' => "মোবাইল নম্বর", "class" => 'form-control'])
                        ?>
                    </div>
                    <div class="col-md-4 ">
                        <label class="control-label ">পাঠানোর তারিখ</label>
                        <div class="input-group input-large date-picker input-daterange" data-date="<?php echo date('Y-m-d') ?>" data-date-format="yyyy-mm-dd">
                            <input type="text" class="form-control" name="from" id="search_date_from">
                            <span class="input-group-addon"> হইতে </span>
                            <input type="text" class="form-control" name="to" id="search_date_to">
                        </div>

                    </div>
                </div>

                <div class="row form-actions">
                    <div class="col-md-6 text-right">
                        <a class="btn green btn-sm trackingformSearch">

                            <i class="fs1 a2i_gn_search1"></i> &nbsp; 
                            <?php echo __(SEARCH) ?>
                        </a>
                    </div>
                    <div class="col-md-6 text-left">
                        <a class="btn red btn-sm" onclick="reset_it()" >

                            <i class="fa fa-eraser" aria-hidden="true"></i> &nbsp; 
                            <?php echo __('রিসেট') ?>
                        </a>
                    </div>
                </div>


                <div class="row form-group responsetable"  style="display: none;">
                    <div class="col-md-12 ">
                        <hr/>
                        <table class="table table-bordered table-stripped ">
                            <thead>
                                <tr>
                                    <th style="width: 15%" class="text-center  font-sm">গ্রহণ নম্বর</th>
                                    <th style="width: 15%" class="text-center  font-sm">তারিখ</th>
                                    <th style="width: 25%" class="text-center  font-sm">বিষয়</th>
                                    <th style="width: 25%" class="text-center font-sm">বর্তমান টেবিল</th>
                                    <th style="width: 20%" class="text-center font-sm">সর্বশেষ মন্তব্য</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>                
            </div>
        </div>
    </div>
</div>

<script src="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-toastr/toastr.min.js"></script>
<script src="<?php echo CDN_PATH; ?>assets/admin/pages/scripts/ui-toastr.js"></script>
<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>


<script>
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "positionClass": "toast-bottom-right"
    };
    
     $(function(){
       $('.date-picker').datepicker({
                rtl: Metronic.isRTL(),
                orientation: "left",
                autoclose: true
            });
   })
   
      function reset_it(){
         $('#dak-received-no').val(''); 
         $('#mobile-no').val(''); 
         $('#search_date_from').val('');
         $('#search_date_to').val('');
     }
    
    $('.trackingformSearch').on('click',function(){
       var receive_no = $.trim($('#dak-received-no').val());
       var mobile_no = $.trim($('#mobile-no').val());
       
        var date_from = $.trim($('#search_date_from').val());
        var date_to = $.trim($('#search_date_to').val());

        if (receive_no != '' || mobile_no != '' || date_from!='' || date_to!='') {
       
           $('.responsetable').show();
           $('.responsetable').find('tbody').html('<tr><td class="text-center bold-text" colspan=5><i class="fa fa-spinner fa-spin"></i></td></tr>');
           
           $.ajax({
               url: '<?php echo $this->Url->build(['controller'=>'DakNagoriks','action'=>'getTracking']) ?>',
               type: 'post',
               data: {receive_no: receive_no, mobile_no: mobile_no,date_from:date_from,date_to:date_to },
               dataType: 'json',
               success: function (response){
                   $('.responsetable').find('tbody').html('');
                   if(response.status == 'success'){
                       $.each(response.data, function(i, v){
                           var montobbo = v.dak_actions;
                           $('.responsetable').find('tbody').append("<tr><td>" + v.dak_received_no + "</td><td>" + dateConvert(v.dakcreated) + "</td><td>" + escapeHtml(v.dak_subject) + "</td><td>" + escapeHtml(v.to_officer_designation_label) + "," + escapeHtml(v.to_office_unit_name) +  "</td><td>" + montobbo + "</td></tr>");
                       });
                   }else{
                     $('.responsetable').find('tbody').append("<tr><td colspan=5 class='text-center text-danger'>" + response.msg + "</td></tr>");
                   }
               },
               error: function(){
                   $('.responsetable').find('tbody').html('');
                   toastr.error("দুঃখিত! অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না");
               }
           });
       }
        
    });
    
    function dateConvert(value){
        var bDate = new Array("০", "১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯");
        var bMonth = new Array("জানুয়ারি", "ফেব্রুয়ারি", "মার্চ", "এপ্রিল", "মে", "জুন", "জুলাই", "আগস্ট", "সেপ্টেম্বর", "অক্টোবর", "নভেম্বর", "ডিসেম্বর");
      
        if (value!='' && value!=null) {
            var dt = new Date(value);
            var dtb = dt.getDate();
            var dtb1 = "", dtb2 = "";
            
            
            if (dtb >= 10) {
                dtb1 = Math.floor(dtb / 10);
                dtb2 = dtb % 10;
                dtb = bDate[dtb1] + "" + bDate[dtb2];
            } else {
                dtb = bDate[0] + "" + bDate[dtb];
            }

            var mnb;
            var mn = dt.getMonth();
            mnb = bMonth[mn];

            var yrb = "", yr1;
            var yr = dt.getFullYear();

            for (var i = 0; i < 3; i++) {
                yr1 = yr % 10;
                yrb = bDate[yr1] + yrb;
                yr = Math.floor(yr / 10);
            }

            yrb = bDate[yr] + "" + yrb;

//            return (typeof(dtb)!='undefined' && dtb!='undefined')?(dtb + " " + mnb + ", " + yrb + " খ্রিষ্টাব্দ"):dt.getDate();
            return (typeof(dtb)!='undefined' && dtb!='undefined')?(dtb + " " + mnb + ", " + yrb ):dt.getDate();
        }else{
            return value;
        }
    }
</script>
