<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption"><i class="fs1 a2i_gn_edit2"></i>New Nagorik Dak</div>
        <div class="tools">
            <a href="javascript:" class="collapse"></a>
            <a href="#portlet-config" data-toggle="modal" class="config"></a>
            <a href="javascript:" class="reload"></a>
            <a href="javascript:;" class="remove"></a>
        </div>
    </div>
    <div class="portlet-body">
        <h3 class="form-section">আবেদনকারীর তথ্য</h3>

        <div class="row">
            <div class="col-md-8">
                <div class="form-group form-horizontal">
                    <label class="control-label col-md-5"><b>নাম (ইংরেজিতে)</b></label>
                    <label class="control-label col-md-4 pull-left"><b><?= h($dak_nagoriks->name_eng) ?></b></label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <div class="form-group form-horizontal">
                    <label class="control-label col-md-5 "><b>নাম (বাংলায়)</b></label>
                    <label class="control-label col-md-4"><b><?= h($dak_nagoriks->name_bng) ?></b></label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <div class="form-group form-horizontal">
                    <label class="control-label col-md-5"><b>জাতীয় পরিচয়পত্র</b></label>
                    <label class="control-label col-md-4"><b><?= h($dak_nagoriks->national_idendity_no) ?></b></label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <div class="form-group form-horizontal">
                    <label class="control-label col-md-5"><b>ডাক গ্রহণের তারিখ</b></label>
                    <label class="control-label col-md-4"><b><?= h($dak_nagoriks->receive_date) ?></b></label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <div class="form-group form-horizontal">
                    <label class="control-label col-md-5"><b>পিতার নাম</b></label>
                    <label class="control-label col-md-4"><b><?= h($dak_nagoriks->father_name) ?></b></label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <div class="form-group form-horizontal">
                    <label class="control-label col-md-5"><b>মাতার নাম</b></label>
                    <label class="control-label col-md-4"><b><?= h($dak_nagoriks->mother_name) ?></b></label>
                </div>
            </div>
        </div>

        <h3 class="form-section">প্রাপকের তথ্য</h3>

        <div class="row">
            <div class="col-md-8">
                <div class="form-group form-horizontal">
                    <label class="control-label col-md-5"><b>প্রাপক</b></label>
                    <label class="control-label col-md-4"><b><?= h($dak_nagoriks->receiver) ?></b></label>
                </div>
            </div>
        </div>
        <br>

        <div class="row">
            <div class="col-md-8">
                <div class="form-group form-horizontal">
                    <label class="control-label col-md-5"><b>দৃষ্টি আকর্ষণ</b></label>
                    <label class="control-label col-md-4"><b><?= h($dak_nagoriks->attention) ?></b></label>
                </div>
            </div>
        </div>
        <br>

        <h3 class="form-section">ডাক বিবরণী</h3>

        <div class="row">
            <div class="col-md-8">
                <div class="form-group form-horizontal">
                    <label class="control-label col-md-5"><b>বিষয়</b></label>
                    <label class="control-label col-md-4"><b><?= h($dak_nagoriks->subject) ?></b></label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <div class="form-group form-horizontal">
                    <label class="control-label col-md-5"><b>ধরন</b></label>
                    <label class="control-label col-md-4"><b><?= h($dak_nagoriks->category_type_id) ?></b></label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <div class="form-group form-horizontal">
                    <label class="control-label col-md-5"><b>ডাক প্রাপ্তির মাধ্যম</b></label>
                    <label class="control-label col-md-4"><b><?= h($dak_nagoriks->media) ?></b></label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <div class="form-group form-horizontal">
                    <label class="control-label col-md-5"><b>বিস্তারিত বিবরণ</b></label>
                    <label class="control-label col-md-7"><b><?= ($dak_nagoriks->dak_description) ?></b></label>
                </div>
            </div>
        </div>
        <br>

        <h3 class="form-section">মন্তব্য</h3>

        <div class="row">
            <div class="col-md-8">
                <div class="form-group form-horizontal">
                    <label class="control-label col-md-5"><b>মন্তব্য</b></label>
                    <label class="control-label col-md-7"><b><?= ($dak_nagoriks->dak_description) ?></b></label>
                </div>
            </div>
        </div>
    </div>
</div>
</div>


<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
        <td>
            <span class="preview"></span>
        </td>
        <td>
            <p class="name">{%=file.name%}</p>
            <strong class="error text-danger label label-danger"></strong>
        </td>
        <td>
            <p class="size">প্রক্রিয়াকরন চলছে...</p>
            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
            <div class="progress-bar progress-bar-success" style="width:0%;"></div>
            </div>
        </td>
        <td>
            {% if (!i && !o.options.autoUpload) { %}
                <button class="btn blue start" disabled>
                    <i class="fa fa-upload"></i>
                    <span>আপলোড করুন</span>
                </button>
            {% } %}
            {% if (!i) { %}
                <button class="btn red cancel">
                    <i class="fa fa-ban"></i>
                    <span>বাতিল করুন</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}


</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
        {% for (var i=0, file; file=o.files[i]; i++) { %}
            <tr class="template-download fade">
                <td>
                    <span class="preview">
                        {% if (file.thumbnailUrl) { %}
                            <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
                        {% } %}
                    </span>
                </td>
                <td>
                    <p class="name">
                        {% if (file.url) { %}
                            <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
                        {% } else { %}
                            <span>{%=file.name%}</span>
                        {% } %}
                    </p>
                    {% if (file.error) { %}
                        <div><span class="label label-danger"> ত্রুটি: </span> {%=file.error%}</div>
                    {% } %}
                </td>
                <td>
                    <span class="size">{%=o.formatFileSize(file.size)%}</span>
                </td>
                <td>
                    {% if (file.deleteUrl) { %}
                        <button class="btn red delete btn-sm" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                            <i class="fs1 a2i_gn_delete2"></i>
                            <span> মুছে ফেলুন </span>
                        </button>
                        <input type="checkbox" name="delete" value="1" class="toggle">
                    {% } else { %}
                        <button class="btn yellow cancel btn-sm">
                            <i class="fa fa-ban"></i>
                            <span> বাতিল করুন </span>
                        </button>
                    {% } %}
                </td>
            </tr>
        {% } %}


</script>


<script type="text/javascript">
    jQuery(document).ready(function () {
        FormFileUpload.init();
    });
</script>
<script type="text/javascript">
    tinymce.init({  //script for the tiny editor
        selector: "textarea",
        statusbar: false,
        menubar: false,
        plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste"
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | fontselect",
        font_formats:"Nikosh='Nikosh','Open Sans', sans-serif;Arial=arial;Helvetica=helvetica,Sans-serif=sans-serif;Courier New=courier new;Courier=courier,Monospace=monospace;Comic Sans MS=comic sans ms;Times New Roman=times new roman,times;Kalpurush=kalpurush;Siyamrupali=Siyamrupali;SolaimanLipi=SolaimanLipi;",
        table_default_border: "1",
        table_default_attributes: {
            width: "30%",
            cellpadding: "2",
            cellspacing: "0",
            border: "1"
        },
        table_default_styles: {
            border: "1px solid #000;"
        },
        paste_retain_style_properties: "all",
    });
</script>