<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption"><i class="fs1 a2i_gn_edit2"></i>New Online Nagorik Dak</div>
        <div class="tools">
            <a href="javascript:;" class="collapse"></a>
            <a href="#portlet-config" data-toggle="modal" class="config"></a>
            <a href="javascript:;" class="reload"></a>
            <a href="javascript:;" class="remove"></a>
        </div>
    </div>
    <div class="portlet-body">

        <?php
        /*
        echo "<pre>";
        print_r($dak_nagoriks[0]);
        echo "</pre>";
         * 
         */
        ?>

        <h3 class="form-section">আবেদনকারীর তথ্য</h3>

        <div class="row">
            <div class="col-md-8">
                <div class="form-group form-horizontal">
                    <label class="control-label col-md-5"><b>নাম (ইংরেজিতে)</b></label>
                    <label class="control-label col-md-4 pull-left"><b><?= h($dak_nagoriks[0]->name_eng) ?></b></label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <div class="form-group form-horizontal">
                    <label class="control-label col-md-5 "><b>নাম (বাংলায়)</b></label>
                    <label class="control-label col-md-4"><b><?= h($dak_nagoriks[0]->name_bng) ?></b></label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <div class="form-group form-horizontal">
                    <label class="control-label col-md-5"><b>জাতীয় পরিচয়পত্র</b></label>
                    <label
                        class="control-label col-md-4"><b><?= h($dak_nagoriks[0]->national_idendity_no) ?></b></label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <div class="form-group form-horizontal">
                    <label class="control-label col-md-5"><b>ডাক গ্রহণের তারিখ</b></label>
                    <label class="control-label col-md-4"><b><?= h($dak_nagoriks[0]->created) ?></b></label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <div class="form-group form-horizontal">
                    <label class="control-label col-md-5"><b>পিতার নাম</b></label>
                    <label class="control-label col-md-4"><b><?= h($dak_nagoriks[0]->father_name) ?></b></label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <div class="form-group form-horizontal">
                    <label class="control-label col-md-5"><b>মাতার নাম</b></label>
                    <label class="control-label col-md-4"><b><?= h($dak_nagoriks[0]->mother_name) ?></b></label>
                </div>
            </div>
        </div>

        <h3 class="form-section">ডাক বিবরণী</h3>

        <div class="row">
            <div class="col-md-8">
                <div class="form-group form-horizontal">
                    <label class="control-label col-md-5"><b>বিষয়</b></label>
                    <label class="control-label col-md-4"><b><?= h($dak_nagoriks[0]->subject) ?></b></label>
                </div>
            </div>
        </div>

        <hr>
        <div class="inbox-attached">
            <?php
            if (isset($dak_nagoriks[0]->dak_online_attachments) && count($dak_nagoriks[0]->dak_online_attachments) > 0) {
                $i = 1;
                foreach ($dak_nagoriks[0]->dak_online_attachments as $single_data) {

                    $file_name = $single_data['file_name'];
                    $file_name = explode("/", $file_name);
                    $file_name = $file_name[count($file_name) - 1];
                    $file_name_arr = explode(".", $file_name);
                    ?>
                    <div class="row">
                        <div class="col-md-12"><b><h4>সংযুক্তি: <?php echo $i; ?></h4></b></div>
                        <hr>
                        <div class="col-md-12"><?php echo $single_data['attachment_description']; ?></div>
                        <div class="col-md-12">
                            <?php
                            if (isset($file_name_arr[1]) && ($file_name_arr[1] == 'pdf' || $file_name_arr[1] == 'jpg')) {
                                echo '<embed src="' . $single_data['file_name'] . '" width="100%" height="1040" type="application/pdf"></embed>';
                            }
                            //echo  $single_data['file_name'];
                            ?>
                        </div>
                    </div>
                    <?php
                    $i++;
                }
                ?>
            <?php } ?>
        </div>

    </div>
</div>
</div>


<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<script id="template-upload" type="text/x-tmpl">
    {% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
    <td>
    <span class="preview"></span>
    </td>
    <td>
    <p class="name">{%=file.name%}</p>
    <strong class="error text-danger label label-danger"></strong>
    </td>
    <td>
    <p class="size">প্রক্রিয়াকরন চলছে...</p>
    <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
    <div class="progress-bar progress-bar-success" style="width:0%;"></div>
    </div>
    </td>
    <td>
    {% if (!i && !o.options.autoUpload) { %}
    <button class="btn blue start" disabled>
    <i class="fa fa-upload"></i>
    <span>আপলোড করুন</span>
    </button>
    {% } %}
    {% if (!i) { %}
    <button class="btn red cancel">
    <i class="fa fa-ban"></i>
    <span>বাতিল করুন</span>
    </button>
    {% } %}
    </td>
    </tr>
    {% } %}

</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
    {% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade">
    <td>
    <span class="preview">
    {% if (file.thumbnailUrl) { %}
    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
    {% } %}
    </span>
    </td>
    <td>
    <p class="name">
    {% if (file.url) { %}
    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
    {% } else { %}
    <span>{%=file.name%}</span>
    {% } %}
    </p>
    {% if (file.error) { %}
    <div><span class="label label-danger">ত্রুটি: </span> {%=file.error%}</div>
    {% } %}
    </td>
    <td>
    <span class="size">{%=o.formatFileSize(file.size)%}</span>
    </td>
    <td>
    {% if (file.deleteUrl) { %}
    <button class="btn red delete btn-sm" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
    <i class="fs1 a2i_gn_delete2"></i>
    <span>মুছে ফেলুন</span>
    </button>
    <input type="checkbox" name="delete" value="1" class="toggle">
    {% } else { %}
    <button class="btn yellow cancel btn-sm">
    <i class="fa fa-ban"></i>
    <span>বাতিল করুন</span>
    </button>
    {% } %}
    </td>
    </tr>
    {% } %}

</script>


<script type="text/javascript">
    jQuery(document).ready(function () {
        FormFileUpload.init();
    });
</script>
<script type="text/javascript">
    tinymce.init({//script for the tiny editor
        selector: "textarea",
        statusbar: false,
        menubar: false,
        plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste"
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | fontselect",
          font_formats:"Nikosh='Nikosh','Open Sans', sans-serif;Arial=arial;Helvetica=helvetica,Sans-serif=sans-serif;Courier New=courier new;Courier=courier,Monospace=monospace;Comic Sans MS=comic sans ms;Times New Roman=times new roman,times;Kalpurush=kalpurush;Siyamrupali=Siyamrupali;SolaimanLipi=SolaimanLipi;",
        table_default_border: "1",
        table_default_attributes: {
            width: "30%",
            cellpadding: "2",
            cellspacing: "0",
            border: "1"
        },
        table_default_styles: {
            border: "1px solid #000;"
        },
        paste_retain_style_properties: "all",
    });
</script>