<?php
$prev = "";
$count = 0;
if (!empty($data)) {
    ?>
    <div class="table-container  table-scrollable">
        <table class="table table-bordered table-hover">
            <?php
            foreach ($data as $key => $value) {
                $unitInformation = h($value['receiving_office_unit_name']);
                $unitId = h($value['receiving_office_unit_id']);
                if ($prev == $unitId) {
                    
                } else {
                    $prev = $unitId;
                    if ($count) {
                        ?>
                        <tr><td class="text-center" colspan="6"> </td></tr> 

                        <?php
                        $count = 0;
                    }
                    ?>
                    <tr class="heading">
                        <th class="text-center" colspan="6"><?= $unitInformation . '<br>' . h($office_name['office_name_bng']) ?></th>
                    </tr>
                    <tr class="heading">
                        <th class="text-center" rowspan="2" width="5%">ক্রমিক সংখ্যা</th>
                        <th class="text-center" colspan="2">প্রাপ্ত পত্রের</th>
                        <th class="text-center" rowspan="2" width="10%">কাহার নিকট হইতে প্রাপ্ত</th>
                        <th class="text-center" rowspan="2" width="25%">পত্রের সংক্ষিপ্ত বিষয়</th>
                        <th class="text-center" rowspan="2">গতিবিধি বিবরণ</th>
                    </tr>
                    <tr class="heading">
                        <th class="text-center" width="15%">স্মারক নম্বর</th>
                        <th class="text-center" width="10%">তারিখ</th>
                    </tr> 
                    <?php
                }
                $count++;
                ?>
                <tr>
                    <td class="text-center"><?= $this->Number->format($count) ?></td>
                    <td class="text-center"><?= h($value['sender_sarok_no']) ?></td>
                    <td class="text-center"><?= Cake\I18n\Time::parse($value['created']) ?></td>
                    <td class="text-center"><?= h($value['sender_name']) . __(!empty($value['sender_office_unit_name']) ? (", " . h($value['sender_office_unit_name'])) : '') . __(!empty($value['sender_office_name']) ? (", " . h($value['sender_office_name'])) : '' ) ?></td>
                    <td class="text-center"><?= h($value['dak_subject']) ?></td>
                    <td class="text-center"><?=
                        h($value['from_officer_name']) . h($value['from_office_name']) .
                        ' - ' . h($value['to_officer_name']) . ', ' . h($value['to_office_address'])
                        ?></td>
                </tr>
                <?php
            }
            ?>
        </table>
    </div>
    <?php
} else {
    ?>
    <div class="table-container  table-scrollable">
        <table class="table table-bordered table-striped">
            <tr class="heading">
                <th class="text-center" rowspan="2" width="5%">ক্রমিক সংখ্যা</th>
                <th class="text-center" colspan="2">প্রাপ্ত পত্রের</th>
                <th class="text-center" rowspan="2" width="10%">কাহার নিকট হইতে প্রাপ্ত</th>
                <th class="text-center" rowspan="2" width="25%">পত্রের সংক্ষিপ্ত বিষয়</th>
                <th class="text-center" rowspan="2">গতিবিধি বিবরণ</th>
            </tr>
            <tr class="heading">
                <th class="text-center" width="15%">স্মারক নম্বর</th>
                <th class="text-center" width="10%">তারিখ</th>
            </tr>
            <tr><td class="text-center" colspan="6" style="color:red;"> দুঃখিত কোন তথ্য পাওয়া যায় নি।  </td></tr> 
        </table>
    </div>
    <?php
}
?>
<div class="actions text-center">
    <ul class="pagination pagination-sm">
        <?php
        echo $this->Paginator->first(__('প্রথম', true), array('class' => 'number-first'));
        echo $this->Paginator->prev('<<', array('tag' => 'li', 'escape' => false), '<a href="#" class="btn btn-sm blue">&laquo;</a>', array('class' => 'prev disabled btn btn-sm blue', 'tag' => 'li', 'escape' => false));
        echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a', 'reverse' => FALSE));
        echo $this->Paginator->next('>>', array('tag' => 'li', 'escape' => false), '<a href="#" class="btn btn-sm blue ">&raquo;</a>', array('class' => 'next disabled', 'tag' => 'li', 'escape' => false));
        echo $this->Paginator->last(__('শেষ', true), array('class' => 'number-last'));
        ?>
    </ul>

</div>













