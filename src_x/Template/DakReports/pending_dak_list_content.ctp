<?php
$prev = "";
$count = 0;
if (!empty($data)) {
    ?>
    <div class= "table-scrollable">
        <table class="table table-bordered table-hover">
            <thead>
                <tr class="heading">
                <th class="text-center" style="width: 5%;">ক্রমিক সংখ্যা</th>
                <th class="text-center" style="width: 5%;">ডকেট নং</th>
                <th class="text-center" style="width: 5%;">গ্রহণ নম্বর</th>
                <th class="text-center" style="width: 10%;">স্মারক নম্বর</th>
                <th class="text-center" style="width: 10%;">আবেদনের তারিখ</th>
                <th class="text-center" style="width: 15%;">বিষয়</th>
                <th class="text-center" style="width: 10%;">আবেদনকারী </th>
                <th class="text-center" style="width: 10%;">মূল প্রাপক </th>
                <th class="text-center" style="width: 10%;">প্রেরণের তারিখ </th>
                <th class="text-center" style="width: 5%;">গোপনীয়তা </th>
                <th class="text-center" style="width: 5%;">অগ্রাধিকার </th>
                <th class="text-center" style="width: 10%;">সর্বশেষ অবস্থা  </th>
                <th class="text-center" style="width: 5%;">পেন্ডিং সময়  </th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($data as $dak) {
                $unitInformation = h($dak['to_office_unit_name']);
                $unitId = h($dak['to_office_unit_id']);
                if ($prev != $unitId) {
                    $prev = $unitId;
                    $count = 0;
                    ?>
                    <tr>
                        <th class="text-center" colspan="12"><?= $unitInformation . '<br>' . h($office_name['office_name_bng']) ?></th>
                    </tr>

                    <?php
                }
                $count++;
                $created = new Cake\I18n\Time($dak['dakcreted']);
                $dak['dakcreted'] = $created->i18nFormat(null, null, 'bn-BD');

                $dak['movecreated']  =  $modified = new Cake\I18n\Time($dak['movecreated']);
                $dak['movecreated'] = $modified->i18nFormat(null, null, 'bn-BD');


                $pending = new Cake\I18n\Time($dak['pending_time']);

                $dak['pending_time'] = $pending->i18nFormat(null, null, 'bn-BD');

                $pending = \App\Controller\ProjapotiController::dateDiff(  $created->i18nFormat(null, null, 'En'), $pending->i18nFormat(null, null, 'En'));
                ?>
                <tr>
                    <td class="text-center"><?= $this->Number->format($count) ?></td>
                    <td class="text-center"><?= h($dak['dak_received_no']) ?></td>
                    <td class="text-center"><?= h(enTobn($dak['docketing_no'])) ?></td>
                    <td class="text-center"><?= h($dak['sender_sarok_no']) ?></td>
                    <td class="text-center"><?= __($dak['dakcreted']) ?></td>
                    <td class="text-center"><?= h($dak['dak_subject']) ?></td>
                    <td class="text-center"><?= h($dak['sender_name']) . ', ' . h($dak['sender_officer_designation_label']) . __(!empty($dak['sender_office_unit_name']) ? (", " . h($dak['sender_office_unit_name'])) : '') . __(!empty($dak['sender_office_name']) ? (", " . h($dak['sender_office_name'])) : '' ) ?></td>
                    <td class="text-center"><?= h($dak['receiving_officer_name']) . ', ' . h($dak['receiving_officer_designation_label']) . __(!empty($dak['receiving_office_unit_name']) ? (", " . h($dak['receiving_office_unit_name'])) : '') ?></td>
                    <td class="text-center"><?= __($dak['movecreated']) ?></td>
                    <td class="text-center"><?= (isset($dak['dak_security_level']) ? h($dak['dak_security_level']) : '') ?></td>
                    <td class="text-center"><?= (isset($dak['dak_priority']) ? h($dak['dak_priority']) : '') ?></td>
                    <td class="text-center"><?= h($dak['last_status_officer_name']) . ', ' . h($dak['last_status_officer_designation_label']) . __(!empty($dak['last_status_office_unit_name']) ? (", " . h($dak['last_status_office_unit_name'])) : '') ?></td>
                    <td class="text-center"><?= h($pending) ?></td>
                </tr>
                <?php
            }
            ?>
            </tbody>
        </table> </div>
    <?php
}
if (empty($data)) {
    ?>
    <div class= "table-scrollable">
        <table class="table table-bordered table-striped table-hover">
            <thead>

            <tr class="heading">
                <th class="text-center" style="width: 5%;">ক্রমিক সংখ্যা</th>
                <th class="text-center" style="width: 5%;">গ্রহণ নম্বর</th>
                <th class="text-center" style="width: 5%;">ডকেট নং</th>
                <th class="text-center" style="width: 10%;">স্মারক নম্বর</th>
                <th class="text-center" style="width: 10%;">আবেদনের তারিখ</th>
                <th class="text-center" style="width: 15%;">বিষয়</th>
                <th class="text-center" style="width: 10%;">আবেদনকারী </th>
                <!--<th class="text-center">পূর্ববর্তী প্রেরক </th>-->
                <th class="text-center" style="width: 10%;">মূল প্রাপক </th>
                <th class="text-center" style="width: 10%;">প্রাপ্তির তারিখ </th>
                <th class="text-center" style="width: 5%;">গোপনীয়তা </th>
                <th class="text-center" style="width: 5%;">অগ্রাধিকার </th>
                <th class="text-center" style="width: 10%;">সর্বশেষ অবস্থা  </th>
                <th class="text-center" style="width: 5%;">পেন্ডিং সময়  </th>
            </tr>
            </thead>
            <tbody>

            <tr><td class="text-center" colspan="13" style="color:red;"> দুঃখিত কোন তথ্য পাওয়া যায় নি।  </td></tr>
            </tbody> </table> </div>
    <?php
}
?>
<div class="actions text-center">
    <?= customPagination($this->Paginator) ?>

</div>
<script>
    jQuery(document).ready(function () {
        $('.table').floatThead({
            position: 'absolute',
            top: jQuery("div.navbar-fixed-top").height()
        });
        $('.table').floatThead('reflow');
    });
</script>