
<?php
if ($tableType == 'Office') {
    $th = ' অফিস: '.$tableHeader;
}
else if ($tableType == 'Unit') {
    $th = ' শাখা: '.$tableHeader;
}
else if ($tableType == 'Designation') {
    $th = ' কর্মকর্তার পদবি ';
}
else {
    $th = '';
}
?>
<div class="portlet box purple">
    <div class="portlet-title">
        <div class="caption"> <?= $tableHeader ?> </div>
        <div class="tools">
            <a href="javascript:;" class="collapse" data-original-title="" title=""></a>
            <a href="javascript:;" class="fullscreen" data-original-title="" title=""></a>
        </div>
    </div>
    <div class="portlet-body">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            আজকের অনুপস্থিত ব্যবহারকারী
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-scrollable">
                            <table class="table table-hover table-bordered">
                                <thead>
                                    <tr class="heading">
                                        <th class="text-center"> <?= $th ?> </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (!empty($data['today'])) {
//                                pr($data); die;
                                        foreach ($data['today'] as $key => $val) {
                                            ?>
                                            <tr class="text-center">
                                                <?php
                                                $employee_info = \Cake\ORM\TableRegistry::get('EmployeeRecords')->getEmployeeInfoByRecordId($val['employee_record_id']);
                                            if ($tableType == 'Unit' || $tableType == 'Designation') {
                                                echo "  <td><b> ". h($employee_info['name_bng']).", ".$val['designation']."</b></td>";
                                            }
                                            else {
                                                 $unit_info = \Cake\ORM\TableRegistry::get('OfficeUnits')->getBanglaName($val['office_unit_id']);
                                                echo "  <td><b> ".h($employee_info['name_bng']).", ".$val['designation'].", ".$unit_info['unit_name_bng']."</b></td>";
                                            }
                                                ?>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    else {
                                        echo "<tr> <td colspan=1 class='danger font-md  text-center'> দুঃখিত কোন তথ্য পাওয়া যায়নি। </tr>";
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            গতকালের অনুপস্থিত ব্যবহারকারী
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-scrollable">
                            <table class="table table-hover table-bordered">
                                <thead>
                                    <tr class="heading">
                                        <th class="text-center"> <?= $th ?> </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (!empty($data['yesterday'])) {
//                                pr($data); die;
                                        foreach ($data['yesterday'] as $key => $val) {
                                            ?>
                                            <tr class="text-center">
                                                <?php
                                                $employee_info = \Cake\ORM\TableRegistry::get('EmployeeRecords')->getEmployeeInfoByRecordId($val['employee_record_id']);
                                            if ($tableType == 'Unit' || $tableType == 'Designation') {
                                                echo "  <td><b> ".h($employee_info['name_bng']).", ".$val['designation']."</b></td>";
                                            }
                                            else {
                                                 $unit_info = \Cake\ORM\TableRegistry::get('OfficeUnits')->getBanglaName($val['office_unit_id']);
                                                echo "  <td><b> ".h($employee_info['name_bng']).", ".$val['designation'].", ".$unit_info['unit_name_bng']."</b></td>";
                                            }
                                                ?>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    else {
                                        echo "<tr> <td colspan=1 class='danger font-md text-center'> দুঃখিত কোন তথ্য পাওয়া যায়নি। </tr>";
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>