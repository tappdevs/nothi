<?php
$session         = $this->request->session();
$modules         = $session->read('modules');
$selected_module = $session->read('module_id');
?>

<!-- BEGIN PAGE HEAD -->
<div class="page-head">

    <div class="note note-success note-shadow">
        <h3><?= $modules[$selected_module] ?> <?php echo __("ড্যাশবোর্ড"); ?></h3>
    </div>
    <?php
    if(!empty($niderror)){
        echo "<div class='alert alert-danger'>{$niderror}</div>";
    }
    ?>
    <div class="row" id="sortable_portlets">
        <?php
        if (isset($dashboard_data) && !empty($dashboard_data)) {
            foreach ($dashboard_data as $dashboard) {
                $url = $this->request->webroot."dashboard/dashboard/";
                ?>
                <div class="col-md-6 sortable">
                    <div class="portlet box green-jungle ">
                        <div class="portlet-title">
                            <div class="caption">
                                <?php echo $dashboard['name']; ?>&nbsp;&nbsp;
                            </div>
                            <div class="actions">
                                <a class="bnt btn-sm btn-danger link" onclick="submitSectionSelectionForm('<?php echo $url; ?>', '<?php echo $dashboard['id'] ?>')"> <i class="fs1 a2i_gn_desk2"></i> </a>

                                </i>
                            </div>
                        </div>

                    </div>
                </div>
            <?php }
        }
        ?>
    </div>

</div>
<?php
if (isset($dashboard_data) && !empty($dashboard_data)) {
    foreach ($dashboard_data as $dashboard) {
        ?>
        <form id="dak_unit_selection_form_<?php echo $dashboard['id'] ?>" method="post" action="">
            <?php echo $this->Form->hidden('selected_office_id',
                ['value' => $dashboard['office_id']]) ?>
            <?php echo $this->Form->hidden('selected_office_unit_id',
                ['value' => $dashboard['office_unit_id']]) ?>
        <?php echo $this->Form->hidden('selected_office_unit_organogram_id',
            ['value' => $dashboard['id']]) ?>
        <?php echo $this->Form->hidden('selected_office_unit_organogram_label',
            ['value' => $dashboard['name']]) ?>
        <?php echo $this->Form->hidden('office_selection_form') ?>
        </form>
    <?php }
}
?>
<script type="text/javascript">
    function submitSectionSelectionForm(action, org_id) {
        $("#dak_unit_selection_form_" + org_id).attr("action", action);
        $("#dak_unit_selection_form_" + org_id).submit();
    }
</script>

<script src="<?php echo CDN_PATH; ?>assets/admin/pages/scripts/portlet-draggable.js"></script>
<script type="text/javascript">
    PortletDraggable.init();
</script>
<!-- END PAGE HEAD -->