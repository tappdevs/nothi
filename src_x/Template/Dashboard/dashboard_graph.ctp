<style>
    .portlet > .portlet-title > .tools > a{
        height:14px!important;
    }
</style>
<?php

?>

<?php
if ($canSee == 1) {
    echo $this->Cell('DakDashboard::officeSummary',[$owninfo['office_id']]);
}
?>  

<?php
echo $this->Cell('DakDashboard::ownSummary');
?>
<div class="clearfix"></div>
<div class="row" style="    
    margin-left: -15px;
    margin-right: -15px;">
    <div class="col-md-8 col-sm-12 pull-left">
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption">
                        কার্যক্রমের অগ্রগতি
                    </div>

                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <h4>ব্যক্তিগত </h4>
                            <?php echo $this->Cell('DakDashboard::agingOwnDesk'); ?>                
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <h4>অধীনস্থ শাখা</h4>
                            <?php echo $this->Cell('DakDashboard::agingOtherDesk') ?>               
                        </div>

                    </div>
                </div>
            </div>
        </div>
    <div class="col-md-4 col-sm-12 pull-right">
            <?php echo $this->Cell('DakDashboard::OfficeList',[$owninfo['office_id']]); ?>
    </div>
</div>
<div class="clearfix"></div>
<div class="portlet box purple">
    <div class="portlet-title">
        <div class="caption">
            গতিবিধি বিবরণ - গত ১ মাস
        </div>
    </div>
    <div class="portlet-body">

        <div class="row">
            <div class="col-md-6 col-sm-12">
                <?php echo $this->Cell('DakDashboard::dateWiseOwnDeskSummary'); ?>                
            </div>            

            <div class="col-md-6 col-sm-12">
                <?php echo $this->Cell('DakDashboard::dateWiseOwnDeskNothiSummary'); ?>                
            </div>
        </div>
    </div>
</div>


<script>

    $('body').addClass('page-quick-sidebar-over-content page-full-width');

</script>