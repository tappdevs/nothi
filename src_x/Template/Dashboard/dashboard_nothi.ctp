<?php
$session                 = $this->request->session();
$modules                 = $session->read('modules');
$selected_module         = $session->read('module_id');
$selected_office_section = $session->read('selected_office_section');
?>

<!-- BEGIN PAGE HEAD -->
<div class="page-head">
    <div class="row" style="padding-left: 15px;">
        <div class="portlet box">
            <div class="portlet-body">
                <div class="row" style="padding-left: 5px;">
                     <?php
                if (!empty($niderror)) {
                    ?>
                <div class="alert alert-danger text-center bold">
                    <?= $niderror ?>
                </div>
                <?php
                }
                ?>
<!--                    <div class="alert alert-danger text-center bold">
                       নথি অ্যাপ্লিকেশন সহজীকরনের কাজ চলছে। সাময়িক অসুবিধার জন্য আন্তরিক ভাবে দুঃখিত ।
                    </div>-->
                    <div class="col-md-6 col-sm-6 " >

                        <?php
                        if (isset($dashboard_data) && !empty($dashboard_data)) {
                            echo '<div class="form-group"><label><b> নিম্নোক্ত তালিকা থেকে পদবি নির্বাচন করুন </b></label><select class="form-control  input-md" id="selectDesignation">';

                            foreach ($dashboard_data as $dashboard) {

                                echo ' <option '.(($selected_office_section['office_unit_organogram_id']
                                == $dashboard['id']) ? 'selected=selected' : '').'  value="'.$dashboard['id'].'">';
                                echo h($dashboard['name']);
                                echo '</option>';
                            }
                            echo '</select></div>';
                        }
                        ?>

                    </div>
                    <div class="col-md-6 col-sm-6">
                        <br>
	                    <div class="input-group filter1 btn-group btn-group-round" style="width:100%">
		                    <input type="text" class="form-control input-md" placeholder="বিষয় দিয়ে  খুঁজুন" name="search" id="filter_input" style="width: calc(100% - 107px);height: 32px;border-radius: 5px 0 0 5px !important;">
		                    <button title="বর্ধিত করুন" type="button" class="btn btn-md purple-medium " onclick="showAdvance(this)"><i class="fa fa-chevron-down" aria-hidden="true"></i></button>
		                    <button type="button" title="খুঁজুন" class="btn btn-md green-haze " id="filter_submit_btn1"><i class="fs1 a2i_gn_search1" aria-hidden="true"></i></button>
		                    <button class="btn btn-md red filter-cancel" title="<?php echo __(RESET) ?>"><i class="fs1 a2i_gn_reset2"></i></button>
	                    </div>
                    </div>

                </div>

                <div class="advanceSearch "  style="padding-left:5px;display:none;">
                    <table class="table text-center">
                        <tr role="row" class="filter">
                            <td>
                                <input type="text" class="form-control form-filter input-sm" placeholder="নথি নম্বর" name="nothi_no" id="filter_input_4">
                            </td>
	                        <td style="width: 200px;">
		                        <?=$this->Form->input('nothi_type_id',
									array('label' => false, 'type' => 'select', 'class' => 'form-control input-md',
										'options' => [0 => ' -- নথি ধরন -- ' ] + $nothiTypes)); ?>
	                        </td>
                            <td class="text-center input-large">
	                            <center>
                                <div class="input-group date-picker input-daterange" data-date="<?php echo date('Y-m-d') ?>" data-date-format="yyyy-mm-dd">
                                    <input placeholder=" " type="text" class="form-control input-sm form-filter" name="from" id="search_date_from">
                                    <span class="input-group-addon input-sm"> হইতে </span>
                                    <input placeholder="" type="text" class="form-control input-sm form-filter" name="to" id="search_date_to">
                                </div>
                                </center>
                            </td>
                                    <?php
                                    if (!empty($ownChildInfo)) {
                                        echo '<td>';
                                        echo '<div class="form-group">';
                                        echo $this->Form->input('select_office_unit_id',
                                            array('label' => false, 'type' => 'select', 'class' => 'form-control input-md',
                                                'options' => [0 => ' -- শাখা নির্বাচন করুন -- ' ] + $ownChildInfo));
                                        echo '</div>';
                                        echo '</td>';

                                        echo '<td>';
                                        echo '<div class="form-group">';
                                        echo $this->Form->input('show_only_important',
                                            array('label' => false, 'type' => 'select', 'class' => 'form-control input-md',
                                                'options' => [0 => ' -- সকল নথি -- ' ] + [1 => ' শুধু জরুরি নথিসমূহ ']));
                                        echo '</div>';
                                        echo '</td>';
                                    }
                                    ?>
<!--                            <td  colspan="4">
                                <button class="btn btn-sm yellow filter-submit margin-bottom" id="filter_submit_btn"
                                        name="filter_submit_btn" data-title="<?php echo __(SEARCH) ?>" title="<?php echo __(SEARCH) ?>"><i class="fs1 a2i_gn_search1"></i> </button>
                                <button data-title="<?php echo __(RESET) ?>" title="<?php echo __(RESET) ?>" class="btn btn-sm red filter-cancel"><i class="fs1 a2i_gn_reset2"></i>
                                </button>
                            </td>-->

                        </tr>

                    </table>
                </div>

                <div class="content-dak" >
                     <img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"><span>&nbsp;&nbsp;লোড করা হচ্ছে...</span>
                </div>
            </div>
        </div>

        <div class="clearfix margin-bottom-20">
        </div>
    </div>
</div>

<?php
if (isset($dashboard_data) && !empty($dashboard_data)) {
    foreach ($dashboard_data as $dashboard) {
        ?>
        <form id="dak_unit_selection_form_<?php echo $dashboard['id'] ?>" method="post" action="">
            <?php
            echo $this->Form->hidden('selected_office_id', ['value' => $dashboard['office_id']])
            ?>
            <?php
            echo $this->Form->hidden('selected_office_unit_id',
                ['value' => $dashboard['office_unit_id']])
            ?>
            <?php
            echo $this->Form->hidden('selected_office_unit_organogram_id',
                ['value' => $dashboard['id']])
            ?>
            <?php
            echo $this->Form->hidden('selected_office_unit_organogram_label',
                ['value' => $dashboard['name']])
            ?>
            <?php echo $this->Form->hidden('office_selection_form') ?>
        </form>
        <?php
    }
}
?>
<script type="text/javascript">
    function submitSectionSelectionForm(action, org_id) {
        $("#dak_unit_selection_form_" + org_id).attr("action", action);
        $("#dak_unit_selection_form_" + org_id).submit();
    }
    jQuery.ajaxSetup({
        cache: true
    });
    $("#selectDesignation").on('change', function () {

        var url = '<?= $this->request->webroot ?>Dashboard/dashboard';

        submitSectionSelectionForm(url, $(this).val());

    });

    $(document).ready(function () {
<?php
$listtype = 'inbox';
if (!empty($session->read('refer_url'))) {
    $listtype = $session->read('refer_url');
}
else {
    $listtype = 'inbox';
}
if (empty($listtype)) {
    $listtype = 'inbox';
}
?>

        var listtype = '<?= $this->request->webroot ?>nothiMasters/index/<?= $listtype ?>';
     <?php
       if(!empty($selected_office_section['office_unit_organogram_id'])){
         ?>
                 PROJAPOTI.ajaxLoad(listtype, '.content-dak');
             <?php
       }else{
           ?>
                   $(".content-dak").html('');
                   $(".filter1").html('');
        <?php
       }
        ?>

            });
            function showAdvance(element) {
                if ($(".advanceSearch").css('display') == 'none') {
                    $(element).find('i').removeClass('fa-chevron-down');
                    $(element).find('i').addClass('fa-chevron-up');
                    $(element).attr('data-original-title', 'বন্ধ করুন');
                    $(".advanceSearch").toggle();
                } else {
                    $(element).find('i').removeClass('fa-chevron-up');
                    $(element).find('i').addClass('fa-chevron-down');
                    $(".advanceSearch").toggle();
                    $(element).attr('data-original-title', 'বর্ধিত করুন');
                }
            }
</script>
<!---CDN-->
<!--<script src="<?php echo CDN_PATH; ?>projapoti-nothi/js/nothi_master_movements.js"></script>-->
<script src="<?php echo CDN_PATH; ?>projapoti-nothi/js/tbl_nothi_masters.js"></script>
<script>
    $(function(){
//        $.ajax({
//            url: '<?//= $this->Url->build(['_name' => 'netSpeed']) ?>//',
//            dataType:'json',
//            cache: false,
//            success: function(res){
//                if(parseInt(res)>=20){
//                    toastr.options = {
//                        "closeButton": true,
//                        "positionClass": "toast-bottom-right",
//                        timeOut: 15000
//                    };
//                    toastr.warning("আপনার ইন্টারনেট কানেকশন কিছুটা ধীর। তাই নথি ব্যবহারে কিছুটা বিঘ্ন ঘটতে পারে। দয়া করে ইন্টারনেট কানেকশন আপডেট করুন। ধন্যবাদ।");
//                }
//            },
//            error: function(err,xstatus){
//
//            }
//        })
    })
</script>
<!---CDN-->
<!-- END PAGE HEAD -->
