<div class="page-head">
    <div class="row" style="padding-left: 15px;">
        <div class="portlet box">
            <div class="portlet-body">
                <?php
                if(!empty($dashboard_data['report'])){
                    $i = 1;
                    foreach($dashboard_data['report'] as $report){
                        ?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <?= $report['title']?>
                                </h4>
                            </div>
                            <div class="panel-body" style="font-size : 12pt!important;">
                                <div id="collapse_<?= $i++?>" class="panel-collapse collapse in">
                                    <?= $report['description']?>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                }else{
                  echo '<div class="well danger">এখন পর্যন্ত রিপোর্ট মডিউল এর জন্য কোন তথ্য অন্তর্ভুক্ত করা হয়নি।</div>' ;
                }
                ?>
            </div>
        </div>
        <div class="clearfix margin-bottom-20">
        </div>
    </div>
</div>
<div class="modal fade modal-purple" id="responsiveOnuccedModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">

                <button type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<script>
$(document).off('click', '.NoticePopup').on('click', '.NoticePopup', function (e) {
        e.preventDefault();
        var title = $(this).attr('title').length > 0 ? $(this).attr('title') : $(this).attr('data-original-title');
        getPopUp($(this).attr('href'), title);
    });
     function getPopUp(href, title) {
            $('#responsiveOnuccedModal').find('.modal-title').text('');
            $('#responsiveOnuccedModal').find('.modal-body').html('');
            $.ajax({
                type: 'POST',
                url: "<?php echo $this->request->webroot; ?>LoginPageSettings/NoticePopup/" + href,
                data: {},
                success: function (response) {
                    $('#responsiveOnuccedModal').modal('show');
                    $('#responsiveOnuccedModal').find('.modal-title').text(title);
                    $('#responsiveOnuccedModal').find('.modal-body').html(response);
                }
            });

        }
</script>