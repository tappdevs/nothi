<div class="portlet box purple">
    <div class="portlet-title">
        <div class="caption">
        মোবাইল অ্যাপ  ডাউনলোড লিঙ্ক
        </div>
    </div>
    <div class="portlet-body">
        <table class="table table-bordered table-hover">
            <thead>
                <tr class="heading">
                    <th> অ্যাপের ধরন </th>
                    <th><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                       Android
                    </td>
                    <td>
                         <a href='https://play.google.com/store/apps/details?id=com.tappware.nothipro'
                            target="__tab" class="btn btn-sm btn-primary">
              <?=  __('Download') ?></a>
                    </td>
                </tr>
                <tr>
                    <td>
                        IOS
                    </td>
                    <td>
                       <a href='https://itunes.apple.com/us/app/id1187955540'
                            target="__tab" class="btn btn-sm btn-primary">
              <?=  __('Download') ?></a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
