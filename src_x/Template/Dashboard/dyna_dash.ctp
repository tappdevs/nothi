<?php

$session = $this->request->session();
$modules = $session->read('modules');
$selected_module = $session->read('module_id');
?>
<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>assets/global/plugins/jquery-nestable/jquery.nestable.css"/>
<!--link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>assets/global/plugins/amcharts/amcharts/export/export.css" /-->

<script src="<?php echo CDN_PATH; ?>assets/global/plugins/jquery-nestable/jquery.nestable.js"></script>
<script src="<?php echo CDN_PATH; ?>assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
<script src="<?php echo CDN_PATH; ?>assets/global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
<script src="<?php echo CDN_PATH; ?>assets/global/plugins/amcharts/amcharts/radar.js" type="text/javascript"></script>
<script src="<?php echo CDN_PATH; ?>assets/global/plugins/amcharts/amstockcharts/amstock.js" type="text/javascript"></script>
<!--script src="<?php echo CDN_PATH; ?>assets/global/plugins/amcharts/amcharts/export/export.js" type="text/javascript"></script-->

<!-- BEGIN PAGE HEAD -->
<div class="page-head">
	<div class="portlet box">
		<div class="portlet-body">
			<div class="tabbable tabbable-tabdrop">
				<ul class="nav nav-tabs">
					<?php
						$count = 0;
						
						foreach($dashboard_data as $tab_data) {
							$isActive = '';
							if ($count == 0) {
								$isActive = 'active';
							}
							?>
								<li class="<?=$isActive ?>">
									<a href="#tab<?=($count+1)?>" data-toggle="tab" data-office_unit_organogram_id="<?=$tab_data['office_unit_organogram_id'] ?>"><?php echo $tab_data['designation'] . " (" . $tab_data['unit_name'] . ")" ; ?></a>
								</li>
							<?php
							$count++;
						}
					?>
				</ul>
				<div class="tab-content">
					<?php
						$count = 0;
						$chart_id = 0;
						
						foreach($dashboard_data as $tab_data) {
							$isActive = '';
							if ($count == 0) {
								$isActive = 'active';
							}
							?>
							<div class="tab-pane <?= $isActive ?>" id="tab<?=($count+1)?>">
								<div class="row">
									<div class="col-md-3">
										<div class="portlet box">
											<div class="portlet-body ">
												<div class="dd" id="nestable_list_<?=($count+1)?>" class="nestable_list">
													<ol class="dd-list">
														<li class="dd-item dd3-item" data-id="dak_<?=($count+1)?>">
															<div class="dd-handle dd3-handle">
															</div>
															<div class="dd3-content">
																 ডাক
															</div>
															<ol class="dd-list">
																<li class="dd-item dd3-item" data-id="dak_<?=($count+1)?>_0">
																	<div class="dd-handle dd3-handle">
																	</div>
																	<a href="javascript:;" class="chart-link" data-target-id="chart_<?=$chart_id?>">
																		<div class="dd3-content">
																			 আগত ডাক
																		</div>
																	</a>
																</li>
																<li class="dd-item dd3-item" data-id="dak_<?=($count+1)?>_1">
																	<div class="dd-handle dd3-handle">
																	</div>
																	<a href="javascript:;" class="chart-link" data-target-id="chart_<?=$chart_id+1?>">
																		<div class="dd3-content">
																			 বার্ধক্য
																		</div>
																	</a>
																</li>
															</ol>
														</li>
														<li class="dd-item dd3-item" data-id="nothi_<?=($count+1)?>">
															<div class="dd-handle dd3-handle">
															</div>
															<div class="dd3-content">
																 নথি
															</div>
															<ol class="dd-list">
																<li class="dd-item dd3-item" data-id="nothi_<?=($count+1)?>_0">
																	<div class="dd-handle dd3-handle">
																	</div>
																	<a href="javascript:;" class="chart-link" data-target-id="chart_<?=$chart_id+2?>">
																		<div class="dd3-content">
																			 আগত ও প্রেরিত নথি
																		</div>
																	</a>
																</li>
																<li class="dd-item dd3-item" data-id="nothi_<?=($count+1)?>_1">
																	<div class="dd-handle dd3-handle">
																	</div>
																	<a href="javascript:;" class="chart-link" data-target-id="chart_<?=$chart_id+3?>">
																		<div class="dd3-content">
																			 Polar Chart
																		</div>
																	</a>
																</li>
																<li class="dd-item dd3-item" data-id="nothi_<?=($count+1)?>_1">
																	<div class="dd-handle dd3-handle">
																	</div>
																	<a href="javascript:;" class="chart-link" data-target-id="chart_<?=$chart_id+4?>">
																		<div class="dd3-content">
																			 Stock Chart
																		</div>
																	</a>
																</li>
															</ol>
														</li>
													</ol>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-9">
										<div class="portlet box">
											<div class="portlet-body">
												<div class="portlet light bordered">
													<div class="portlet-body">
														<div class="chart_div" style="display:block;">
															<?php //echo $this->cell('DashboardChart', [$chart_id++]); ?>
															<?php echo $this->cell('DashboardChart::displayLineAreaChart', [$chart_id++, "1"]); ?>
														</div>
														<div class="chart_div" style="display:block;">
															<?php 
																echo $this->cell('DashboardChart::displayRadarChart', [$chart_id++]); 
															?>
														</div>
														
														<div class="chart_div" style="display:block;">
															<?php //echo $this->cell('DashboardChart::displayRadarChart', [$chart_id++]); ?>
															<?php 
																$cell = $this->cell('DashboardChart::displayLineAreaChart', [$chart_id++, "2"]); 
																$cell->template = "nothi_graph";
																echo $cell;
															?>
														</div>
														
														<div class="chart_div" style="display:block;">
															<?php echo $this->cell('DashboardChart::displayPolarChart', [$chart_id++]); ?>
														</div>
														
														<div class="chart_div" style="display:block;">
															<?php echo $this->cell('DashboardChart::displayStockChart', [$chart_id++]); ?>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<?php
							$count++;
						}
					?>
				</div>
			</div>
		</div>
	</div>
</div>
	<!-- END TAB PORTLET-->

<script type="text/javascript">
    $('.nestable_list').nestable();
	
	$(".chart-link").click(function(e){
		var chart_id = $(this).attr("data-target-id");
		$(".tab-pane.active .chart_div").hide();
		$("#" + chart_id).parent().show();
	});
	
	$(window).load(function(e){
		//$(".chart_div").hide();
		/* $(".chart_div").eq(0).show(); */
		//$("#chart_1").parent().show();
	});
</script>


<!-- END PAGE HEAD -->