<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-question-circle"></i> সাধারণ জিজ্ঞাসা
        </div>
    </div>
    <div class="portlet-body">
        <div class="panel-group accordion" id="accordion1">
            <?php
            if(!empty($data)){
                $len = count($data);
                for($i = 0; $i < $len; $i++){
//                    echo entobn($i+1)."। ". $data[$i]['question'].'<br>';
//                    echo "উত্তর: ". $data[$i]['ans'].'<br>';
//                }
//            }
            ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <?php
                            if($i == 0){
                        ?>
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapse_<?= $i?>">
                            <b><?=entobn($i+1)."। ". $data[$i]['question'] ?></b></a>
                        <?php
                            }
                        else {
                        ?>
                         <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#collapse_<?= $i?>">
                            <b><?=entobn($i+1)."। ".$data[$i]['question'] ?></b></a>
                        <?php } ?>
                    </h4>
                </div>
               <?php
                            if($i == 0){
                        ?>
                         <div id="collapse_<?= $i?>" class="panel-collapse collapse in">
                        <?php
                            }
                        else {
                        ?>
                         <div id="collapse_<?= $i?>" class="panel-collapse collapse">
                        <?php
                        }
                        ?>
                    <div class="panel-body" style="font-size : 12pt!important;">
                        <b>উত্তর:</b>&nbsp;<?=$data[$i]['ans'] ?>
                         <?php
                        if(!empty($data[$i]['img']) && file_exists(WWW_ROOT.'img/faq/'.$data[$i]['img'])){
                        ?>
                        <br>
                        <div class="text-center">
                            <img class="text-center" src="<?= $this->request->webroot.'img/faq/'.$data[$i]['img']?>">
                        </div>
                        <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
            <?php
                }
            }
            ?>
        </div>
    </div>
</div>
</div>