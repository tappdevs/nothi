<?php
$TotalUser      = $TotalToday     = $totalYesterday = 0;
?>
<div class="row">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <div class="panel-title text-center">
                <?php
                if (!empty($keyword)) {
                    echo __($keyword)." ভিত্তিক ড্যাশবোর্ড";
                }
                else {
                    echo __('Ministry')." ভিত্তিক ড্যাশবোর্ড";
                }
                ?>
            </div>
        </div>
        <div class="panel-body">
            <div class="col-md-6 col-sm-12">
                <div class="scrollable-area" id="floatIt-wrapper" >
                    <table class="table table-bordered table-hover" id="floatIt">
                        <thead >
                            <tr class="heading">
                                <th rowspan="2"  class="text-center "> ক্রম </th>
                                <th rowspan="2"  class="text-center "> অফিসের নাম </th>
                                <th colspan="3"  class="text-center">  ব্যবহারকারী </th>
                                <th rowspan="2"  class="text-center"> কার্যক্রম </th>
                            </tr>
                            <tr class="heading">
                                <th  class="text-center"> মোট </th>
                                <th  class="text-center"> আজকের </th>
                                <th  class="text-center"> গতকালের </th>
                            </tr>
                        </thead>
                        <tbody id="loadInfo">
                        </tbody>
                        <tfoot>
                            <tr class="heading" id="totalInfo" style="display: none;">
                                <th  class="text-center" colspan="2"> সর্বমোট  ব্যবহারকারী </th>
                                <th  class="text-center"> <span id="total-user" class="badge badge-primary" style="font-size: 14px!important;">  </span> </th>
                                <th  class="text-center"> <span id="total-today" class="badge badge-danger" style="font-size: 14px!important;">  </span </th>
                                <th  class="text-center"> <span id="total-yesterday" class="badge badge-success" style="font-size: 14px!important;">   </span> </th>
                                <th  class="text-center">  </th>
                            </tr>
                            <tr id="waitMessage">

                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <div class="col-md-6 col-sm-12" id="showlist" tabindex='1'>

            </div>
        </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/floatthead/2.0.3/jquery.floatThead.min.js"></script>
<script>
    var Offices = <?=json_encode(array_keys($Offices))?>;
    var OfficesNames = <?=json_encode(array_values($Offices))?>;
    var total_user = 0;
    var total_today = 0;
    var total_yesterday = 0;
    $(document).ready(function () {

        $('#floatIt').floatThead({
	            position: 'absolute',
                top: jQuery("div.navbar-fixed-top").height()
        });
        $('#floatIt').floatThead('reflow');
        setTimeout(loadOfficeInfo(0,Offices.length),5000);
    });
    function loadOfficeInfo(cnt,total){
        $('#waitMessage').html('');
        if(isEmpty(total)){
            return;
        }
        if(cnt >= total){
            printTotal();
            return;
        }
        if(isEmpty(Offices[cnt])){
            return;
        }
        if(cnt == 0){
            resetTotal();
        }
        $('#waitMessage').html('<td colspan="6"><img src="<?= CDN_PATH ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp; লোড হচ্ছে <b>'+OfficesNames[cnt]+'... </b></span></td>');
        PROJAPOTI.ajaxSubmitDataCallback(js_wb_root+'dashboard/getOfficeLoginCount',
            {office_id : Offices[cnt],office_name : OfficesNames[cnt],'start_date' : '<?=$today?>','end_date' : '<?=$today ?>','token': '<?= $token ?>'},
            'json',
            function(res){
                if(isEmpty(res)){
                    toastr.error('যান্ত্রিক ত্রুটি হয়েছে Code: ' + Offices[cnt]);
                }
                else if(res.status == 'error'){
                    toastr.error(res.message);
                }
                else{
                    countTotal(res);
                    var html = '<tr class="text-center">' +
                                    '<td>'+enTobn(cnt+1)+'</td>' +
                                    '<td>'+OfficesNames[cnt]+'</td>' +
                                    '<td>'+enTobn(res.TotalUser)+'</td>' +
                                    '<td>'+enTobn(res.TodayLogin)+'</td>' +
                                    '<td>'+enTobn(res.YesterdayLogin)+'</td>' +
                                    '<td>'+'<span class="btn btn-sm btn-primary" onclick="details('+Offices[cnt]+')"> বিস্তারিত </span>'+'</td>' +
                                '</tr>';
                    $("#loadInfo").append(html);
                    loadOfficeInfo(cnt+1,total);
                }
            }
        );
    }
    function resetTotal(){
        total_user = 0;
        total_today = 0;
        total_yesterday = 0;
    }
    function countTotal(res){
        if(isEmpty(res)){
            return;
        }
        if(!isEmpty(res.TotalUser)){
            total_user +=res.TotalUser;
        }
        if(!isEmpty(res.TodayLogin)){
            total_today +=res.TodayLogin;
        }
        if(!isEmpty(res.YesterdayLogin)){
            total_yesterday +=res.YesterdayLogin;
        }
    }
    function printTotal(){
        $("#totalInfo").show();
        $("#total-user").html(enTobn(total_user));
        $("#total-today").html(enTobn(total_today));
        $("#total-yesterday").html(enTobn(total_yesterday));
    }
    function details(key)
    {
        $('#showlist').html('<img src="<?= CDN_PATH ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp; লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');
        $.ajax({
            type: 'POST',
            url: "<?php echo $this->Url->build(['controller' => 'Dashboard', 'action' => 'loginDetails']) ?>",
            data: {"office_id": key},
            success: function (data) {
                $('#showlist').html('');
                $('#showlist').focus();
                $('#showlist').html(data);
            }
        });
    }
</script>