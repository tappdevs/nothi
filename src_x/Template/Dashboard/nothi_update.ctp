
<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-edit"></i>নথির নতুন আপডেট/রিলিজ নোট  <?= ($edit == 1 ? __('Edit') : 'যুক্ত') ?>  করুন
        </div>
        <div class="actions">
            <a href="<?php
            echo $this->Url->build(['controller' => 'dashboard',
                'action' => 'nothiUpdateList'])
            ?>" class="btn   green" ><i class="fa fa-book fa-2x"></i>&nbsp;নথির নতুন আপডেট/রিলিজ নোট তালিকা</a>
        </div>
    </div>
    <div class="portlet-body">
        <div class="row">
            <div class="form-group">
                <label class="col-md-2 col-sm-2 control-label text-right">  ভার্শন নাম্বারঃ </label>
                <div class="col-md-4 col-sm-4 ">
                    <input type="text" class="form-control" placeholder=" ১ বা ১.১" id="version" value="<?= !empty($data['version'])? entobn($data['version']) :'' ?>">
                    <span class="help-block">
                        সাধারণত পূর্ণসংখ্যা বা দশমিক সংখ্যা</span>
                </div>
                <label class="col-md-2 col-sm-2  control-label text-right">  রিলিজ ডেটঃ  </label>
                <div class="col-md-4 col-sm-4 ">
                    <input type="text" class="form-control release-date" value="<?=!empty($data['release_date'])?\Cake\I18n\Time::parse($data['release_date'])->format('Y-m-d') : date('Y-m-d') ?>" data-date-format="yyyy-mm-dd">
                    <span class="help-block">
                        আপডেট প্রকাশের তারিখ </span>
                </div>
            </div>
        </div>
        <div class="margin-top-20">
            <div class="form-group">
                <form method="post">

                    <textarea id="mytextarea"><?= (empty($data['body']) ? '' : $data['body']) ?></textarea>
                </form>
                <br>
                <button type="button" class="btn btn-primary" onclick="savethis()"><?= __('Submit') ?></button>
            </div>
        </div>



    </div>
</div>
<?= $this->element('froala_editor_js_css') ?>
<!--<script src = "--><?php //echo CDN_PATH; ?><!--js/tinymce/tinymce.min.js?v=--><?//=js_css_version?><!--" type = "text/javascript" ></script>-->
<script src = "<?php echo CDN_PATH; ?>js/tinymce/tinymce.min.js" type = "text/javascript" ></script>
<script>
    var buttons= [ 'bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', '|', 'fontFamily', 'fontSize', 'color', 'inlineStyle', 'paragraphStyle', '|', 'paragraphFormat', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote', 'insertLink', 'insertTable', '|', 'emoticons', 'insertHR', 'selectAll', 'clearFormatting', '|', 'print', 'undo', 'redo'];
    $(function() {
        $('#mytextarea').froalaEditor({
			key: 'xc1We1KYi1Ta1WId1CVd1F==',
			toolbarSticky: false,
			wordAllowedStyleProps: ['font-size', 'background', 'color', 'text-align', 'vertical-align', 'background-color', 'padding', 'margin', 'height', 'margin-top', 'margin-left', 'margin-right', 'margin-bottom', 'text-decoration', 'font-weight'],
			wordDeniedTags: ['a','form'],
			wordDeniedAttrs: ['width'],
			wordPasteModal: true,
            tableResizerOffset: 10,
            tableResizingLimit: 20,
            fontSize: ['10','11', '12','13', '14','15','16','17', '18','19','20','21','22','23','24','25','26','27','28','29', '30'],
            fontSizeDefaultSelection: '13',
            tableEditButtons:['tableHeader', 'tableRemove', '|', 'tableRows', 'tableColumns','tableCells', '-',  'tableCellBackground', 'tableCellVerticalAlign', 'tableCellHorizontalAlign', 'tableCellStyle'],
            toolbarButtons: buttons,
            toolbarButtonsMD: buttons,
            toolbarButtonsSM: buttons,
            toolbarButtonsXS: buttons,
            placeholderText: '',
            height:400
        })
    });

//                tinymce.init({
//                    selector: "#mytextarea",
//                    menubar: false,
//                    height: 400,
//                    theme: 'modern',
//                    statusbar: false,
//                    fontsize_formats: "8px 9px 10px 11px 12px 13px 14px 16px 18px 20px 24px 36px",
//                    plugins: [
//                        ' advlist autolink lists link image charmap print preview anchor',
//                        'searchreplace visualblocks code fullscreen',
//                        'insertdatetime media table contextmenu paste ',
//                        ' textcolor colorpicker  '
//                    ],
//                    toolbar: ' undo redo | styleselect | fontsizeselect  bold italic underline  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | table link forecolor backcolor fontselect | guardfilebutton',
//                    font_formats: "Nikosh=Nikosh;Arial=arial;Helvetica=helvetica,Sans-serif=sans-serif;Courier New=courier new;Courier=courier,Monospace=monospace;Comic Sans MS=comic sans ms;Times New Roman=times new roman,times;Kalpurush=kalpurush;Siyamrupali=Siyamrupali;SolaimanLipi=SolaimanLipi;",
//                    table_default_border: "1",
//                    table_default_attributes: {
//                        width: "30%",
//                        cellpadding: "2",
//                        cellspacing: "0",
//                        border: "1"
//                    },
//                    table_default_styles: {
//                        border: "1px solid #000;"
//                    },
//                    paste_retain_style_properties: "all",
//                });
                function savethis() {
                    Metronic.blockUI({target: '.portlet-body', boxed: true});
//                    var htmlbody = tinymce.activeEditor.getContent();
                    var htmlbody = $('#mytextarea').froalaEditor('html.get');
                    var version = $("#version").val();
                    var release = $(".release-date").val();
                    if (htmlbody == null || typeof (htmlbody) == undefined || htmlbody == '') {
                        toastr.error('No content Given. Please give some Data');
                        Metronic.unblockUI('.portlet-body');
                        return;
                    }
                    if (version == null || typeof (version) == undefined || version == '') {
                        toastr.error('No Version Given');
                         $("#version").focus();
                        Metronic.unblockUI('.portlet-body');
                        return;
                    }
                    if (release == null || typeof (release) == undefined || release == '') {
                        toastr.error('No Release Date Given');
                         $(".release-date").focus();
                        Metronic.unblockUI('.portlet-body');
                        return;
                    }
                    $.ajax({
                        type: 'POST',
                        url: "<?php echo $this->Url->build(['controller' => 'Dashboard', 'action' => 'nothiUpdate']) ?>",
                        data: {"body": htmlbody, "edit": <?= $edit ?>, "id": <?= $id ?>,'version' : version,'release' : release},
                        success: function (data) {
                            if (data.status == 'success') {
                                toastr.success(data.msg);
                                window.location.href = '<?= $this->Url->build(['controller' => 'Dashboard', 'action' => 'nothiUpdateList']) ?>';
                            } else {
                                toastr.error(data.msg);
                            }
                            Metronic.unblockUI('.portlet-body');
                        },
                        error: function (data) {
                            toastr.error(data);
                            Metronic.unblockUI('.portlet-body');
                        }
                    });

                }
jQuery(document).ready(function(){
    $('.release-date').datepicker({
        "format" : "yyyy-mm-dd",
    });
});


</script>
