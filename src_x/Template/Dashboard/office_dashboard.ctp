<style>
    .portlet > .portlet-title > .tools > a{
        height:14px!important;
    }
</style>
<?php
?>
<div class="alert alert-info font-lg text-center">
    <i class="fa fa-info-circle " aria-hidden="true"></i>
   প্রিয় ব্যবহারকারী, ড্যাশবোর্ডসমূহ এখন থেকে প্রতি ৩ ঘণ্টা পর পর আপডেট হবে।
</div>
<div class="row" >
    <div class="col-md-8 col-sm-8">
        <?php
        echo $this->Cell('DakDashboard::officeSummary', [$office_id]);
        ?>  
    </div>
    <div class="col-md-4 col-sm-4" id ="officeListCell">
        <?php // echo $this->Cell('DakDashboard::OfficeList', [$office_id]); ?>
    </div>
</div>

<div class="clearfix"></div>

<div class="row" style="">
    <div class="col-md-12 col-sm-12">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    কার্যক্রমের অগ্রগতি
                </div>
                <div class="actions">
                    <a  class="btn btn-primary" id ="loadownunit">লোড করুন </a>
                </div>
            </div>
            <div class="portlet-body" id="scrollhere">
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <h4>অধীনস্থ শাখা</h4>
                        <div id="msg"></div>
                        <div id="ownUnit">

                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 unit_dashboard_div" tabindex='1' id="unit_dashboard_div">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>



<script>

    $('body').addClass('page-quick-sidebar-over-content page-full-width');

    function autoload() {
        $('.unit_dashboard_details').click(function (ev) {
            ev.preventDefault();
            $('.unit_dashboard_div').html('<img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp; লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');
            $.ajax({
                url: $(this).attr('href'),
                type: 'post',
                cache: true,
                success: function (html) {
                    $("#unit_dashboard_div").focus();
                    $('.unit_dashboard_div').html(html);
                }
            });
        });
    }
    ;
    $(document).ready(function () {
    var unit_ids = [];
    var unit_names = [];
        $('#loadownunit').click(function (ev) {
            $('#loadownunit').hide();
             $('#ownUnit').html('');
            ev.preventDefault();
            $('#msg').html('<img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp; লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');
            $.ajax({
                url: "<?= $this->Url->build(['controller' => 'dashboard' , 'action' => 'ownUnitList']) ?>",
                type: 'post',
                cache: true,
                data: {"office_id": <?= $office_id ?>},
                success: function (response) {
                    if(response.status == 'success'){
                        if(response.data == ' দুঃখিত কোন তথ্য পাওয়া যায়নি। '
                                ){
                                     $('#ownUnit').html(response.data);
                                      $('#msg').html('');
                                     return;
                                }
                        $.each(response.data,function(i,v){
                            unit_ids.push(i);
                            unit_names.push(v);
                        });
                        loadUnits(0,unit_ids.length);
                    }else{
                         toastr.error(response.msg);
                    }
                },
                 error: function (response) {
                     toastr.error(response);
                 }
            });
        });
        $('#officeListCell').html('<img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp; লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');
            $.ajax({
                url: "<?= $this->Url->build(['controller' => 'dashboard' , 'action' => 'officeListCell']) ?>",
                type: 'post',
                cache: true,
                data: {"office_id": <?= $office_id ?>},
                success: function (html) {
                    $('#officeListCell').html(html);
                }
            });
function loadUnits(index,len){
                if(index >= len)
                {
                     $('#msg').html('');
                      autoload();
                     return;
                }
                 $('#msg').html('<img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp; লোড হচ্ছে<b> '+unit_names[index]+' </b>। একটু অপেক্ষা করুন... </span>');
                 $.ajax({
                    url: "<?= $this->Url->build(['controller' => 'dashboard' , 'action' => 'loadSingleUnit']) ?>/<?= $office_id ?>/"+unit_ids[index]+"/"+unit_names[index],
                    type: 'post',
                    cache: true,
                    success: function (html) {
                        $('#ownUnit').append(html);
                        if(index == 0){
                             autoload();
                        }
                        loadUnits(index+1,len);
                    },
                    error: function () {
                         loadUnits(index+1,len);
                      }
                });
            }
    });
</script>
