<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-info-circle fa-2x"></i>নথি অ্যাপ্লিকেশনের আপডেট/রিলিজ নোটসমূহ 
        </div>
        <a type="button" class="btn btn-info pull-right" href="<?= $this->request->webroot ?>login">লগইন পেইজ</a>
    </div>
    <div class="portlet-body">
        <div class="panel-group accordion" id="accordion1">
            <?php
            if(!empty($data)){
                $len = count($data);
                for($i = 0; $i < $len; $i++){
            ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <?php
                            if($i == 0){
                        ?>
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapse_<?= $i?>">
                            <div class="row">
                                <div class="col-md-6 col-sm-6">
                                            <b>ভার্শনঃ  <?=entobn($data[$i]['version']) ?></b>
                                </div>
                                <div class="col-md-6 col-sm-6 text-right">
                                            <b>রিলিজ ডেটঃ <?=entobn(Cake\I18n\Time::parse($data[$i]['release_date'])->format('d-M-y'))?></b>
                                </div>
                            </div>
                        </a>
                        <?php
                            }
                        else {
                        ?>
                         <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#collapse_<?= $i?>">
                            <div class="row">
                                <div class="col-md-6 col-sm-6">
                                            <b>ভার্শনঃ  <?=entobn($data[$i]['version']) ?></b>
                                </div>
                                <div class="col-md-6 col-sm-6 text-right">
                                            <b>রিলিজ ডেটঃ <?=entobn(Cake\I18n\Time::parse($data[$i]['release_date'])->format('d-M-y'))?></b>
                                </div>
                            </div>
                        <?php } ?>
                    </h4>
                </div>
               <?php
                            if($i == 0){
                        ?>
                         <div id="collapse_<?= $i?>" class="panel-collapse collapse in">
                        <?php
                            }
                        else {
                        ?>
                         <div id="collapse_<?= $i?>" class="panel-collapse collapse">
                        <?php
                        }
                        ?>
                    <div class="panel-body" style="font-size : 12pt!important;">
                        <?=$data[$i]['body'] ?>
                    </div>
                </div>
            </div>
            <?php
                }
            }else{
                echo " খুব শীঘ্রয় তথ্য অন্তর্ভুক্ত করা হবে। ";
            }
                ?>
        </div>
    </div>
</div>