<div class="portlet box purple">
    <div class="portlet-title">
        <div class="caption">
         <?= __('User Manual').' '.__('List') ?>
        </div>
    </div>
    <div class="portlet-body">
        <table class="table table-bordered table-hover">
            <thead>
                <tr class="heading">
                    <th> বিষয়বস্তু </th>
                    <th><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        লগইন প্রক্রিয়া ও প্রোফাইল ব্যবস্থাপনা
                    </td>
                    <td>
                         <a href='<?= $this->request->webroot ?>Dashboard/downloadUsermanual?path=Nothi-User-Manual&file=Nothi_User_Manual_Part1_V16&ext=pdf&name=ই-ফাইল ব্যবহার সহায়িকা-১(লগইন প্রক্রিয়া ও প্রোফাইল ব্যবস্থাপনা).'
                            target="__tab" class="btn btn-sm btn-primary">
              <?=  __('Download') ?></a>
                    </td>
                </tr>
                <tr>
                    <td>
                        ডাক
                    </td>
                    <td>
                         <a href='<?= $this->request->webroot ?>Dashboard/downloadUsermanual?path=Nothi-User-Manual&file=Nothi_User_Manual_Part2_V16&ext=pdf&name=ই-ফাইল ব্যবহার সহায়িকা-২(ডাক).'
                            target="__tab" class="btn btn-sm btn-primary">
              <?=  __('Download') ?></a>
                    </td>
                </tr>
                <tr>
                    <td>
                       নথি
                    </td>
                    <td>
                         <a href='<?= $this->request->webroot ?>Dashboard/downloadUsermanual?path=Nothi-User-Manual&file=Nothi_User_Manual_Part3_V16&ext=pdf&name=ই-ফাইল ব্যবহার সহায়িকা-৩(নথি).'
                            target="__tab" class="btn btn-sm btn-primary">
              <?=  __('Download') ?></a>
                    </td>
                </tr>
                <tr>
                    <td>
                       পত্রজারি
                    </td>
                    <td>
                         <a href='<?= $this->request->webroot ?>Dashboard/downloadUsermanual?path=Nothi-User-Manual&file=Nothi_User_Manual_Part4_V16&ext=pdf&name=ই-ফাইল ব্যবহার সহায়িকা-৪(পত্রজারি).'
                            target="__tab" class="btn btn-sm btn-primary">
              <?=  __('Download') ?></a>
                    </td>
                </tr>
                <tr>
                    <td>
                       সারসংক্ষেপ
                    </td>
                    <td>
                         <a href='<?= $this->request->webroot ?>Dashboard/downloadUsermanual?path=Nothi-User-Manual&file=Nothi_User_Manual_Part5_V16&ext=pdf&name=ই-ফাইল ব্যবহার সহায়িকা-৫(সারসংক্ষেপ).'
                            target="__tab" class="btn btn-sm btn-primary">
              <?=  __('Download') ?></a>
                    </td>
                </tr>
                <tr>
                    <td>
                       ড্যাশবোর্ড ও দপ্তর
                    </td>
                    <td>
                         <a href='<?= $this->request->webroot ?>Dashboard/downloadUsermanual?path=Nothi-User-Manual&file=Nothi_User_Manual_Part6_V16&ext=pdf&name=ই-ফাইল ব্যবহার সহায়িকা-৬( ড্যাশবোর্ড ও দপ্তর).'
                            target="__tab" class="btn btn-sm btn-primary">
              <?=  __('Download') ?></a>
                    </td>
                </tr>
                <tr>
                    <td>
                      সম্পূর্ণ ম্যানুয়াল (সকল বিষয় একসাথে)
                    </td>
                    <td>
                         <a href='<?= $this->request->webroot ?>Dashboard/downloadUsermanual?path=Nothi-User-Manual&file=Nothi_User_Manual_V15&ext=pdf&name=ই-ফাইল ব্যবহার সহায়িকা.'
                            target="__tab" class="btn btn-sm btn-primary">
              <?=  __('Download') ?></a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
