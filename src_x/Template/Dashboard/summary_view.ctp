<div class="table-scrollable">
    <table class="table table-bordered" id="summary">
        <thead>
        <tr class="info">
            <th rowspan="2"></th>
            <th rowspan="2">অফিস সংখ্যা  </th>
            <th colspan="2">  ব্যবহারকারী</th>
        </tr>
        <tr class="info">
            <th>মোট</th>
            <th> গতকালের  </th>
        </tr>
        </thead>
        <tbody>
        <?php
        if(!empty($data) && !empty($layerLevels)){
            foreach($layerLevels as $level){
                if($level == 'all'){
                    continue;
                }
                echo '<tr>';
                echo '<td class="text-center"><b><a target="_blank" class="" href="'.$this->request->webroot.'monitorLayers/'.$level.'">'. __($level). '</a></b></td>';
                echo '<td class="text-center"><b>'.(isset($data["totaloffices_".$level])?entobn($this->Number->format($data["totaloffices_".$level])):entobn(0)).'</b></td>';
                echo '<td class="text-center"><b>'.(isset($data["sum_".$level])?entobn($this->Number->format($data["sum_".$level])):entobn(0)) .'</b></td>';
                echo '<td class="text-center"><b>'.(isset($data["yesterday_login_".$level])?entobn($this->Number->format($data["yesterday_login_".$level])):entobn(0)) .'</b></td>';
                echo '</tr>';
            }
            echo '<tr class="info">
                                    <td class="text-center"><b> মোট   </b></td>
                                    <td class="text-center"><b><span class="badge badge-success" data-toggle="tooltip" title="সর্বমোট অফিস সংখ্যা ">'. (isset($data["totaloffices_all"])?entobn($this->Number->format($data["totaloffices_all"])):entobn(0)) .'</span></b></td>
                                    <td class="text-center"><b><span class="badge badge-primary" data-toggle="tooltip" title="সর্বমোট ব্যবহারকারী ">'.(isset($data["sum_all"])?entobn($this->Number->format($data["sum_all"])):entobn(0))  .'</span></b></td>
                                    <td class="text-center"><b><span class="badge badge-warning" data-toggle="tooltip" title="গতকালের লগইন ব্যক্তিবর্গ">'. (isset($data["yesterday_login_all"])?entobn($this->Number->format($data["yesterday_login_all"])):entobn(0)) .'</span></b></td>
                                    
                     </tr>';
        }
        ?>
        </tbody>
    </table>
</div>