<?= $this->element('Daak/DaakBox/daak_box_nav'); ?>

<div class="tab-content flex-basis pScroll overflow-hidden">
  <div class="tab-pane active" role="tabpanel" id="daak_box_agoto">
    <?= $this->element('Daak/DaakBox/daak_box_agoto', ['daak_box_content' => $daak_box_content, 'daak_box_type' => 'আগত ডাক']); ?>
  </div>
  <div class="tab-pane" role="tabpanel" id="daak_box_prerito">
    <?= $this->element('Daak/DaakBox/daak_box_prerito', ['daak_box_content' => $daak_box_content, 'daak_box_type' => 'প্রেরিত ডাক']); ?>
  </div>
  <div class="tab-pane" role="tabpanel" id="daak_box_khoshra">
    <?= $this->element('Daak/DaakBox/daak_box_khoshra', ['daak_box_content' => $daak_box_content, 'daak_box_type' => 'খসড়া ডাক']); ?>
  </div>
  <div class="tab-pane" role="tabpanel" id="daak_box_archive">
    <?= $this->element('Daak/DaakBox/daak_box_archive', ['daak_box_content' => $daak_box_content, 'daak_box_type' => 'আর্কাইভ ডাক']); ?>
  </div>
  <div class="tab-pane" role="tabpanel" id="daak_box_nothivukto">
    <?= $this->element('Daak/DaakBox/daak_box_nothivukto', ['daak_box_content' => $daak_box_content, 'daak_box_type' => 'নথিভুক্ত ডাক']); ?>
  </div>
</div>
<style>
  #dbl_click tr{cursor:pointer;}
</style>
<script>
  $(function (e) {
    $('#dbl_click tr').on('click', function (e) {
      console.log('daak_view_clicked');
      window.location = "<?= $this->request->webroot?>daak2/daak_view";
    })
  })
</script>