<h3><?= $daak_box_type ?></h3>
<table class="table table-striped table-bordered table-sm datatable">
  <thead>
  <tr role="row" class="heading">
    <th class="text-center">
      <input type="checkbox">
    </th>
    <th class="text-center">
      কার্যক্রম
    </th>
    <th class="text-center">
      উৎস
    </th>
    <th class="text-center">
      বিষয়
    </th>
    <th class="text-center">
      মূল প্রাপক
    </th>
    <th class="text-center">
      পূর্ববর্তী সিদ্ধান্ত
    </th>
    <th class="text-center">
      তারিখ
    </th>
    <th class="text-center">
      অগ্রাধিকার, গোপনীয়তা
    </th>
    <th class="text-center">
      ধরন
    </th>
    <th class="text-center">
      সংযুক্তি
    </th>
  </tr>
  </thead>
  <tbody id="dbl_click">
  <?php foreach($daak_box_content as $daak_content){?>
    <tr role="row" class="even">
      <td class="text-center"><input type="checkbox"></td>
      <td>
        <div class="btn-group btn-group-round">
          <button class="btn btn-sm btn-primary showDetailsDakInbox">
            <i class="a2i_gn_details2" aria-hidden="true"></i>
          </button>
          <button class="btn btn-sm forward-message">
            <i class="a2i_nt_cholomandak2" aria-hidden="true"></i>
          </button>
        </div>
      </td>
      <td><?= __($daak_content->daak_utsho) ?></td>
      <td><?= __($daak_content->daak_subject) ?></td>
      <td><?= __($daak_content->daak_main_receiver) ?></td>
      <td><?= __($daak_content->daak_next_decision) ?></td>
      <td><?= __($daak_content->daak_date) ?></td>
      <td><?= __($daak_content->daak_priority_secret) ?></td>
      <td class="text-center"><i title="দাপ্তরিক ডাক" class="fa fa-<?= __($daak_content->daak_type) ?> text-dark"></i></td>
      <td class="text-center"><?= __($daak_content->daak_attachment==1?'নাই':'আছে') ?></td>

    </tr>
  <?php }?>
  </tbody>
</table>
