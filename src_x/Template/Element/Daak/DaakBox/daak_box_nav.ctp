<div>
  <ul class="nav nav-tabs tabs-1 bg-white" role="tablist">
    <li class="nav-item">
      <a class="nav-link active d-flex align-items-center" data-toggle="tab" href="#daak_box_agoto" role="tab"
          aria-controls="home">
        <i class="fa fa-inbox fs-20"></i>
        <aside class="ml-3 line-height-1">
          <strong class="text-dark font-weight-normal">ডাক বক্স আগত</strong>
          <span class="badge">12</span>
          <small class="text-truncate nothi-from d-block text-muted" data-toggle="tooltip" title=""
              data-original-title="জাহাঙ্গীর, অফিস সহকারী, জেলা প্রশাসকের কার্যালয়, জামালপুর"> জাহাঙ্গীর, অফিস
            সহকারী, জেলা প্রশাসকের কার্যালয়, জামালপুর
          </small>
        </aside>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link  d-flex align-items-center" data-toggle="tab" href="#daak_box_prerito" role="tab"
          aria-controls="home">
        <i class="fa fa-paper-plane-o fs-20"></i>
        <aside class="ml-3 line-height-1">
          <strong class="text-dark font-weight-normal">ডাক বক্স প্রেরিত</strong>
        </aside>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link  d-flex align-items-center" data-toggle="tab" href="#daak_box_khoshra" role="tab"
          aria-controls="home">
        <i class="icon-layers fs-20"></i>
        <aside class="ml-3 line-height-1">
          <strong class="text-dark font-weight-normal">ডাক বক্স খসড়া</strong>
          <span class="badge">212</span>
          <small class="text-truncate nothi-from d-block text-muted" data-toggle="tooltip" title=""
              data-original-title="অফিস সহকারী, জামালপুর, জাহাঙ্গীর, জেলা প্রশাসকের কার্যালয়"> অফিস সহকারী,
            জামালপুর, জাহাঙ্গীর, জেলা প্রশাসকের কার্যালয়
          </small>
        </aside>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link  d-flex align-items-center" data-toggle="tab" href="#daak_box_archive" role="tab"
          aria-controls="home">
        <i class="icon-envelope-letter fs-20"></i>
        <aside class="ml-3 line-height-1">
          <strong class="text-dark font-weight-normal">ডাক বক্স আর্কাইভ</strong>
          <span class="badge">212</span>
          <small class="text-truncate nothi-from d-block text-muted" data-toggle="tooltip" title=""
              data-original-title="অফিস সহকারী, জামালপুর, জাহাঙ্গীর, জেলা প্রশাসকের কার্যালয়"> অফিস সহকারী,
            জামালপুর, জাহাঙ্গীর, জেলা প্রশাসকের কার্যালয়
          </small>
        </aside>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link  d-flex align-items-center" data-toggle="tab" href="#daak_box_nothivukto" role="tab"
          aria-controls="home">
        <i class="icon-layers fs-20"></i>
        <aside class="ml-3 line-height-1">
          <strong class="text-dark font-weight-normal">ডাক বক্স নথিভুক্ত</strong>
          <span class="badge">212</span>
        </aside>
      </a>
    </li>
  </ul>
</div>