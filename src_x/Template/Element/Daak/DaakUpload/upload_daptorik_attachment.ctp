<div class="card">
  <div class="card-header py-2 d-flex justify-content-between align-items-center">
    <span>মূল ডাক ও সংযুক্তিসমূহ</span>
    <button data-toggle="collapse" data-target="#daak_attachment"
        class="fa toogle-icon fa-angle-down btn btn-light btn-xs"></button>
  </div>
  <div class="card-body collapse show" id="daak_attachment">
    মূল প্রাপক
    <!--  <div class="alert alert-info">সংযুক্তিসমূহের মধ্যে থেকে মূল ডাকটি চিহ্নিত করুন</div>-->
    <div class="alert alert-info alert-dismissible fade show" role="alert">
      <strong>চিহ্নিত করন!</strong> সংযুক্তিসমূহের মধ্যে থেকে মূল ডাকটি চিহ্নিত করুন
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>


    <div class='daak_shongjukti'>
      <div class='input-group pb-3'>
        <div class='custom-file'>
          <input
              type='file' multiple
              class='custom-file-input file_uploader' id='mulpotro_upload'>
          <label class='custom-file-label' for='mulpotro_upload'>
            Choose file
          </label>
        </div>
        <div class='input-group-append'>
          <button class='btn btn-outline-secondary' type='button'>Upload</button>
        </div>
      </div>

      <div class='file_uploader_gallery upload_gallery'>
        <div class='upload_wrap flex-wrap row'></div>
      </div>
    </div>


  </div>
</div>
