<div class="card">
  <div class="card-header py-2 d-flex justify-content-between align-items-center">
    <span>ডাকের বিবরণ</span>
    <button data-toggle="collapse" data-target="#daak_detail"
      class="fa toogle-icon fa-angle-down btn btn-light btn-xs"></button>
  </div>
  <div class="card-body collapse show" id="daak_detail">
    <div class="row">
      <div class="col-sm-6">
        <div class="form-group">
          <label for="name">স্মারক নম্বর</label>
          <input class="form-control" id="name" type="text" placeholder="স্মারক নম্বর">
        </div>
      </div>
      <div class="col-sm-3">
        <div class="form-group">
          <label for="name">স্মারকের তারিখ</label>
          <input class="form-control" data-toggle="datepicker" type="text" placeholder="স্মারকের তারিখ">
        </div>
      </div>
      <div class="col-sm-3">
        <div class="form-group">
          <label for="ccmonth">প্রেরণের মাধ্যম </label>
          <select class="form-control" id="ccmonth">
            <option>ডাকযোগে</option>
            <option>সরাসরি</option>
            <option>ফ্যাক্স</option>
            <option>ই-মেইল</option>
          </select>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-8">
        <div class="form-group">
          <label for="name">বিষয়</label>
          <input class="form-control" id="name" type="text" placeholder="বিষয়">
        </div>
      </div>
      <div class="form-group col-sm-2">
        <label for="ccmonth">গোপনীয়তা</label>
        <select class="form-control" id="ccmonth">
          <option>গোপনীয়তা বাছাই করুন</option>
          <option>অতি গোপনীয়</option>
          <option>বিশেষ গোপনীয়</option>
          <option>গোপনীয়</option>
          <option>সীমিত</option>
        </select>
      </div>
      <div class="form-group col-sm-2">
        <label for="ccyear">অগ্রাধিকার</label>
        <select class="form-control" id="ccyear">
          <option>অগ্রাধিকার বাছাই করুন</option>
          <option>জরুরি</option>
          <option>অবিলম্বে</option>
          <option>সর্বোচ্চ অগ্রাধিকার</option>
          <option>তাগিদপত্র</option>
          <option>দৃষ্টি আকর্ষণ</option>
        </select>
      </div>
    </div>
    <!-- /.row-->
  </div>
</div>
