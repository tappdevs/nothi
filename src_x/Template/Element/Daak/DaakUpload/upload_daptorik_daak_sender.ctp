<div class="card">
  <div class="card-header py-2 d-flex justify-content-between align-items-center">
    <span>প্রেরক</span>
    <button data-toggle="collapse" data-target="#daak_prerok"
        class="fa toogle-icon fa-angle-down btn btn-light btn-xs"></button>
  </div>
  <div class="card-body collapse show" id="daak_prerok">
    <div class="row">
      <div class="col-sm-4">
        <div class="form-group">
          <label for="name">অফিস বাছাই করুন </label><i class="fa office_search fa-chevron-circle-down ml-2"></i>
          <input class="form-control" id="name" type="text" placeholder="অফিস বাছাই করুন">
        </div>
      </div>
      <div class="col-sm-4">
        <div class="form-group">
          <label for="name">অফিসার</label>
          <input class="form-control" id="name" type="text" placeholder="অফিসার">
        </div>
      </div>
    </div>
    <div class="search_specific">
      <div class="row">
        <div class="col-sm-4">
          <div class="form-group">
            <label for="name">কার্যালয়</label>
            <input class="form-control" id="name" type="text" placeholder="কার্যালয় ">
          </div>
        </div>
        <div class="col-sm-4">
          <div class="form-group">
            <label for="name">দপ্তর/শাখা</label>
            <input class="form-control" id="name" type="text" placeholder="দপ্তর/শাখা">
          </div>
        </div>
        <div class="col-sm-4">
          <div class="form-group">
            <label for="ccnumber">পদ </label>
            <input class="form-control" id="ccnumber" type="text" placeholder="পদ ">
          </div>
        </div>
      </div>
    </div><!--search_specific-->
    <div class="search_advanced">
      <div class="row">
        <div class="form-group col-sm-4">
          <label for="ccmonth">মন্ত্রণালয়</label>
          <select class="form-control" id="ccmonth">
            <option value="">--বাছাই করুন--</option>
            <option value="1">রাষ্ট্রপতির কার্যালয়</option>
            <option value="3">প্রধানমন্ত্রীর কার্যালয়</option>
            <option value="4">মন্ত্রিপরিষদ বিভাগ</option>
            <option value="5">জনপ্রশাসন মন্ত্রণালয়</option>
            <option value="6">সশস্র বাহিনী বিভাগ</option>
            <option value="7">অর্থ মন্ত্রণালয়</option>
            <option value="10">আইন, বিচার ও সংসদ বিষয়ক মন্ত্রণালয়</option>
            <option value="12">কৃষি মন্ত্রণালয়</option>
            <option value="13">খাদ্য মন্ত্রণালয়</option>
            <option value="14">ডাক, টেলিযোগাযোগ ও তথ্যপ্রযুক্তি মন্ত্রণালয়</option>
            <option value="15">তথ্য মন্ত্রণালয়</option>
            <option value="16">ধর্ম বিষয়ক মন্ত্রণালয়</option>
            <option value="17">নির্বাচন কমিশন সচিবালয়</option>
            <option value="18">নৌ-পরিবহন মন্ত্রণালয়</option>
            <option value="19">পররাষ্ট্র মন্ত্রণালয়</option>
            <option value="20">পরিকল্পনা মন্ত্রণালয়</option>
            <option value="22">পরিবেশ ও বন মন্ত্রণালয়</option>
            <option value="23">প্রতিরক্ষা মন্ত্রণালয়</option>
            <option value="24">বস্ত্র ও পাট মন্ত্রণালয়</option>
            <option value="25">গৃহায়ন ও গণপূর্ত মন্ত্রণালয়</option>
            <option value="26">বাণিজ্য মন্ত্রণালয়</option>
            <option value="27">বিদ্যুৎ জ্বালানি ও খনিজ সম্পদ মন্ত্রণালয়</option>
            <option value="29">পার্বত্য চট্টগ্রাম বিষয়ক মন্ত্রণালয়</option>
            <option value="30">বেসামরিক বিমান পরিবহন ও পর্যটন মন্ত্রণালয়</option>
            <option value="31">ভূমি মন্ত্রণালয়</option>
            <option value="32">মহিলা ও শিশু বিষয়ক মন্ত্রণালয়</option>
            <option value="33">মৎস্য ও প্রাণিসম্পদ মন্ত্রণালয়</option>
            <option value="34">যুব ও ক্রীড়া মন্ত্রণালয়</option>
            <option value="35">যোগাযোগ মন্ত্রণালয়</option>
            <option value="36">শিল্প মন্ত্রণালয়</option>
            <option value="37">শিক্ষা মন্ত্রণালয়</option>
            <option value="38">প্রাথমিক ও গণশিক্ষা মন্ত্রণালয়</option>
            <option value="39">বিজ্ঞান ও প্রযুক্তি মন্ত্রণালয়</option>
            <option value="40">শ্রম ও কর্মসংস্থান মন্ত্রণালয়</option>
            <option value="41">সমাজকল্যাণ মন্ত্রণালয়</option>
            <option value="42">পানি সম্পদ মন্ত্রণালয়</option>
            <option value="43">সংস্কৃতি বিষয়ক মন্ত্রণালয়</option>
            <option value="44">স্বরাষ্ট্র মন্ত্রণালয়</option>
            <option value="45">স্বাস্থ্য ও পরিবার কল্যাণ মন্ত্রণালয়</option>
            <option value="46">স্থানীয় সরকার, পল্লী উন্নয়ন ও সমবায় মন্ত্রণালয়</option>
            <option value="48">মুক্তিযুদ্ধ বিষয়ক মন্ত্রণালয়</option>
            <option value="49">প্রবাসী কল্যাণ ও বৈদেশিক কর্মসংস্থান মন্ত্রণালয়</option>
            <option value="51">দুর্যোগ ব্যবস্থাপনা ও ত্রাণ মন্ত্রণালয়</option>
            <option value="54">রেলপথ মন্ত্রণালয়</option>
            <option value="55">জন বিভাগ, রাষ্ট্রপতির কার্যালয়</option>
            <option value="56">সুপ্রিম কোর্ট</option>
            <option value="57">তথ্য ও যোগাযোগ প্রযুক্তি বিভাগ</option>
            <option value="58">সড়ক পরিবহন ও সেতু মন্ত্রণালয়</option>
            <option value="59">সেতু বিভাগ</option>
            <option value="60">সড়ক পরিবহন ও মহাসড়ক বিভাগ</option>
            <option value="61">Test ministry</option>
            <option value="62">ডেমো মন্ত্রণালয়</option>
          </select>
        </div>
        <div class="form-group col-sm-4">
          <label for="ccyear">মন্ত্রণালয়/বিভাগ</label>
          <select class="form-control" id="ccyear">
            <option value="0">--বাছাই করুন--</option>
            <option value="44">মন্ত্রণালয়</option>
            <option value="58">সংস্থা</option>
            <option value="59">প্রকল্প</option>
            <option value="67">ইউনিট</option>
            <option value="173">জোন অফিস (বেপজা)</option>
          </select>
        </div>
        <div class="form-group col-sm-4">
          <label for="ccyear">দপ্তর / অধিদপ্তরের ধরন</label>
          <select class="form-control" id="ccyear">
            <option value="0">--বাছাই করুন--</option>
            <option value="52">বাংলাদেশ অর্থনৈতিক অঞ্চল কর্তৃপক্ষ</option>
            <option value="56">পাবলিক প্রাইভেট পার্টনারশীপ কর্তৃপক্ষ</option>
            <option value="59">এনজিও বিষয়ক ব্যুরো</option>
          </select>
        </div>
        <div class="form-group col-sm-4">
          <label for="ccyear">কার্যালয়</label>
          <select class="form-control" id="ccyear">
            <option value="0">--বাছাই করুন--</option>
            <option value="52">বাংলাদেশ অর্থনৈতিক অঞ্চল কর্তৃপক্ষ</option>
            <option value="56">পাবলিক প্রাইভেট পার্টনারশীপ কর্তৃপক্ষ</option>
            <option value="59">এনজিও বিষয়ক ব্যুরো</option>
          </select>
        </div>
        <div class="form-group col-sm-4">
          <label for="ccyear">দপ্তর/শাখা</label>
          <select class="form-control" id="ccyear">
            <option value="0">--বাছাই করুন--</option>
            <option value="737">প্রধানমন্ত্রীর কার্যালয়</option>
            <option value="948">অর্থনৈতিক উপদেষ্টা</option>
            <option value="4477">মুখ্য সচিব এর দপ্তর</option>
            <option value="748">মহাপরিচালক ৩</option>
            <option value="952">ডাক গ্রহন ও বিতরণ</option>
            <option value="951">রাজনৈতিক উপদেষ্টা</option>
            <option value="811">এসআরসিসি</option>
            <option value="950">প্রটোকল অফিসার</option>
            <option value="743">মহাপরিচালক ২</option>
            <option value="949">তথ্য বিষয়ক উপদেষ্টা</option>
            <option value="945">প্রধানমন্ত্রীর একান্ত সচিব-১</option>
            <option value="946">প্রধানমন্ত্রীর একান্ত সচিব-২</option>
            <option value="947">এসাইনমেন্ট অফিসার</option>
            <option value="738">মহাপরিচালক ১</option>
            <option value="4478">সচিব এর দপ্তর</option>
            <option value="4479">জি আই ইউ</option>
            <option value="752">মহাপরিচালক ৪</option>
            <option value="739">পরিচালক - ১</option>
            <option value="740">পরিচালক - ২</option>
            <option value="741">পরিচালক - ৩</option>
            <option value="742">পরিচালক - ৪</option>
            <option value="746">পরিচালক - ৭</option>
            <option value="744">পরিচালক - ৫</option>
            <option value="745">পরিচালক - ৬</option>
            <option value="747">পরিচালক - ৮</option>
            <option value="751">পরিচালক - ১১</option>
            <option value="749">পরিচালক - ৯</option>
            <option value="750">পরিচালক - ১০</option>
            <option value="33">কম্পিউটার সেল</option>
            <option value="756">পরিচালক - ১৫</option>
            <option value="755">পরিচালক - ১৪</option>
            <option value="754">পরিচালক - ১৩</option>
            <option value="753">পরিচালক - ১২</option>
            <option value="34">হিসাব শাখা</option>
            <option value="805">আর এন্ড আই শাখা</option>
            <option value="809">সেবা শাখা</option>
            <option value="808">মাইক্রোগ্রাফি সেল</option>
            <option value="37">লাইব্রেরি</option>
            <option value="815">পরিচালক-২</option>
            <option value="814">পরিচালক-১</option>
            <option value="4480">পরিচালক- ইনোভেশন</option>
            <option value="4483">পরিচালক- গবেষণা ও সক্ষমতা বিকাশ</option>
            <option value="4481">উপপরিচালক - ইনোভেশন</option>
            <option value="4482">উপপরিচালক - আউটরিচ</option>
            <option value="4484">উপপরিচালক - গবেষণা</option>
            <option value="4485">উপপরিচালক - যোগাযোগ</option>
          </select>
        </div>
        <div class="form-group col-sm-4">
          <label for="ccyear">পদ</label>
          <select class="form-control" id="ccyear">
            <option value="0" title="--বাছাই করুন--">--বাছাই করুন--</option>
            <option title="শেখ হাসিনা" value="2131">প্রধানমন্ত্রী</option>
            <option title="ড. কামাল আবদুল নাসের চৌধুরী" value="2132">মুখ্য সচিব</option>
            <option title="সুরাইয়া বেগম,এনডিসি" value="2138">সচিব</option>
            <option title="(শুন্যপদ)" value="6087">প্রধানমন্ত্রীর একান্ত সচিব-১</option>
            <option title="(শুন্যপদ)" value="6088">প্রধানমন্ত্রীর একান্ত সচিব-২</option>
            <option title="(শুন্যপদ)" value="6089">প্রধানমন্ত্রীর সহকারী একান্ত সচিব-১</option>
            <option title="(শুন্যপদ)" value="6090">প্রধানমন্ত্রীর সহকারী একান্ত সচিব-২</option>
            <option title="(শুন্যপদ)" value="6091">প্রধানমন্ত্রীর প্রটোকল অফিসার - ১</option>
            <option title="(শুন্যপদ)" value="6092">প্রধানমন্ত্রীর প্রটোকল অফিসার -২</option>
          </select>
        </div>
      </div>
    </div><!--search_advanced-->
    <!-- /.row-->
  </div>
</div>