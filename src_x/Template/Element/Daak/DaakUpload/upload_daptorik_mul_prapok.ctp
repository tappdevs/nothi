<div class="card">
  <div class="card-header py-2 d-flex justify-content-between align-items-center">
    <span>মূল প্রাপক</span>
    <button data-toggle="collapse" data-target="#daak_receiver"
        class="fa toogle-icon fa-angle-down btn btn-light btn-xs"></button>
  </div>
  <div class="card-body collapse show" id="daak_receiver">


    <table class="table table-bordered table-sm">
      <thead>
      <tr>
        <th class="text-center">
          <button title="মুছে ফেলুন" class="fa fa-2x fa-plus-circle btn-clean" data-toggle="modal"
              data-target="#exampleModal"></button>
        </th>
        <th>পদ</th>
        <th>নাম</th>
        <th class="text-center">মূল প্রাপক</th>
        <th class="text-center">অনুলিপি প্রাপক</th>

      </tr>
      </thead>
      <tbody>
      <tr>
        <td class="text-center">
          <a href="#" class="btn btn-danger btn-sm" title="মুছে ফেলুন"><i class="fa fa-trash"></i></a>
        </td>
        <td>
                    <span title="" data-original-title="কোহিনূর বেগম
">
                    অফিস সহকারী, ব্যবসা-বাণিজ্য শাখা                    </span>
        </td>
        <td>
          কোহিনূর বেগম
        </td>
        <td class="text-center">
          <div class="radio" id="uniform-0"><span><input class="office_employee_to" type="radio" id="0"
                  name="office-employee_to" data-employee-office-id="25245" data-main-office-unit-id="773"
                  data-main-office-employee-id="52" data-main-office-employee-name="কোহিনূর বেগম "
                  data-main-office-unit-organogram-id="1833"></span></div>
        </td>
        <td class="text-center">
          <div class="checker" id="uniform-emp_1833"><span><input class="office_employee_checkbox text-center"
                  type="checkbox" id="emp_1833" name="office-employee"
                  data-label="অফিস সহকারী,ব্যবসা-বাণিজ্য শাখা"
                  data-employee-office-id="25245" data-office-employee-id="52"
                  data-office-employee-name="কোহিনূর বেগম "
                  data-office-unit-id="773" data-office-unit-organogram-id="1833"
                  style="display: inline-block;"></span>
          </div>
        </td>

      </tr>
      <tr>
        <td class="text-center">
          <a href="#" class="btn btn-danger btn-sm" title="মুছে ফেলুন"><i class="fa fa-trash"></i></a>
        </td>
        <td>
                    <span title="" data-original-title="মো: শাহাবুদ্দিন খান
">
                    জেলা প্রশাসক, জেলা প্রশাসকের কার্যালয়                    </span>
        </td>
        <td>
          মো: শাহাবুদ্দিন খান
        </td>
        <td class="text-center">
          <div class="radio" id="uniform-1"><span><input class="office_employee_to" type="radio" id="1"
                  name="office-employee_to" data-employee-office-id="25230" data-main-office-unit-id="333"
                  data-main-office-employee-id="76" data-main-office-employee-name="মো: শাহাবুদ্দিন খান "
                  data-main-office-unit-organogram-id="817"></span></div>
        </td>
        <td class="text-center">
          <div class="checker" id="uniform-emp_817"><span><input class="office_employee_checkbox text-center"
                  type="checkbox" id="emp_817" name="office-employee"
                  data-label="জেলা প্রশাসক,জেলা প্রশাসকের কার্যালয়"
                  data-employee-office-id="25230" data-office-employee-id="76"
                  data-office-employee-name="মো: শাহাবুদ্দিন খান " data-office-unit-id="333"
                  data-office-unit-organogram-id="817" style="display: inline-block;"></span></div>
        </td>

      </tr>
      <tr>
        <td class="text-center">
          <a href="#" class="btn btn-danger btn-sm" title="মুছে ফেলুন"><i class="fa fa-trash"></i></a>
        </td>
        <td>
                    <span title="" data-original-title="মো: এহসানুল হক
">
                    স্টেনোগ্রাফার (সি এ), জেলা প্রশাসকের কার্যালয়                    </span>
        </td>
        <td>
          মো: এহসানুল হক
        </td>
        <td class="text-center">
          <div class="radio" id="uniform-2"><span><input class="office_employee_to" type="radio" id="2"
                  name="office-employee_to" data-employee-office-id="10136" data-main-office-unit-id="333"
                  data-main-office-employee-id="83443" data-main-office-employee-name="মো: এহসানুল হক "
                  data-main-office-unit-organogram-id="818"></span></div>
        </td>
        <td class="text-center">
          <div class="checker" id="uniform-emp_818"><span><input class="office_employee_checkbox text-center"
                  type="checkbox" id="emp_818" name="office-employee"
                  data-label="স্টেনোগ্রাফার (সি এ),জেলা প্রশাসকের কার্যালয়" data-employee-office-id="10136"
                  data-office-employee-id="83443" data-office-employee-name="মো: এহসানুল হক "
                  data-office-unit-id="333"
                  data-office-unit-organogram-id="818" style="display: inline-block;"></span></div>
        </td>

      </tr>

      </tbody>
    </table>
  </div>
</div>