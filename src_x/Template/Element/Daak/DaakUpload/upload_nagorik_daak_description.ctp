<div class="card">
  <div class="card-header py-2 d-flex justify-content-between align-items-center">
    <span>ডাকের বিবরণ</span>
    <button data-toggle="collapse" data-target="#daak_description"
      class="fa toogle-icon fa-angle-down btn btn-light btn-xs"></button>
  </div>
  <div class="card-body collapse show" id="daak_description">

    <div class="row">
      <div class="col-sm-4">
        <div class="form-group">
          <label for="name">ন্যাশনাল আইডি</label>
          <input class="form-control" type="text" placeholder="ন্যাশনাল আইডি">
        </div>
      </div>
      <div class="col-sm-4">
        <div class="form-group">
          <label for="name">জন্ম সনদ</label>
          <input class="form-control" data-toggle="datepicker" type="text" placeholder="জন্ম সনদ">
        </div>
      </div>
      <div class="col-sm-4">
        <div class="form-group">
          <label for="name">পাসপোর্ট</label>
          <input class="form-control" type="text" placeholder="পাসপোর্ট">
        </div>
      </div>
    </div>
    <!-- /.row-->
    <div class="row">
      <div class="col-sm-6">
        <div class="form-group">
          <label for="name">পিতার/স্বামীর নাম (বাংলা / ইংরেজি)</label>
          <input class="form-control" type="text" placeholder="পিতা অথবা স্বামীর নাম লিখুন">
        </div>
      </div>
      <div class="col-sm-6">
        <div class="form-group">
          <label for="name">মাতার নাম (বাংলা / ইংরেজি)</label>
          <input class="form-control" data-toggle="datepicker" type="text" placeholder="মাতার নাম লিখুন">
        </div>
      </div>
    </div>
    <!-- /.row-->
    <div class="row">
      <div class="col-sm-6">
        <div class="form-group">
          <label for="name">বর্তমান ঠিকানা</label>
          <textarea class="form-control presentAddress" placeholder="বর্তমান ঠিকানা"></textarea>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="form-group">
          <div class="d-flex justify-content-between">
            <label for="name">স্থায়ী ঠিকানা</label>
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input sameAsPresent" id="customCheck1">
              <label class="custom-control-label" for="customCheck1">বর্তমান ঠিকানা</label>
            </div>
          </div>
          <textarea class="form-control permanentAddress" placeholder="স্থায়ী ঠিকানা"></textarea>
        </div>
      </div>
    </div>
    <!-- /.row-->
    <div class="row">
      <div class="col-sm-6">
        <div class="form-group">
          <label for="name">ই-মেইল</label>
          <input class="form-control" type="text" placeholder="ই-মেইল লিখুন">
        </div>
      </div>
      <div class="col-sm-6">
        <div class="form-group">
          <label for="name">মোবাইল নম্বর</label>
          <input class="form-control" data-toggle="datepicker" type="text" placeholder="মোবাইল নম্বর">
        </div>
      </div>
    </div>
    <!-- /.row-->
    <div class="row">
      <div class="col-sm-4">
        <div class="form-group">
          <label for="nationality">জাতীয়তা</label>
          <select name="nationality" id="nationality" class="form-control">
            <option value="bangladeshi">বাংলাদেশী</option>
            <option value="Other">অন্যান্য</option>
          </select>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="form-group">
          <label for="gender">লিঙ্গ</label>
          <select name="gender" class="form-control" id="gender">
            <option value="Male">পুরুষ</option>
            <option value="Female">মহিলা</option>
            <option value="Other">অন্যান্য</option>
          </select>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="form-group">
          <label for="religion">ধর্ম</label>
          <select name="religion" class="form-control" id="religion">
            <option value="islam">ইসলাম</option>
            <option value="sonaton">হিন্দু</option>
            <option value="christian">খ্রিষ্টান</option>
            <option value="buddhism">বৌদ্ধ</option>
            <option value="Other">অন্যান্য</option>
          </select>
        </div>
      </div>
    </div>
    <!-- /.row-->
    <div class="row">
      <div class="col-sm-8">
        <div class="form-group">
          <label for="name">বিষয়</label>
          <input class="form-control" type="text" placeholder="বিষয়">
        </div>
      </div>
      <div class="form-group col-sm-2">
        <label for="ccmonth">গোপনীয়তা</label>
        <select class="form-control" id="ccmonth">
          <option>গোপনীয়তা বাছাই করুন</option>
          <option>অতি গোপনীয়</option>
          <option>বিশেষ গোপনীয়</option>
          <option>গোপনীয়</option>
          <option>সীমিত</option>
        </select>
      </div>
      <div class="form-group col-sm-2">
        <label for="ccyear">অগ্রাধিকার</label>
        <select class="form-control" id="ccyear">
          <option>অগ্রাধিকার বাছাই করুন</option>
          <option>জরুরি</option>
          <option>অবিলম্বে</option>
          <option>সর্বোচ্চ অগ্রাধিকার</option>
          <option>তাগিদপত্র</option>
          <option>দৃষ্টি আকর্ষণ</option>
        </select>
      </div>
    </div>
    <!-- /.row-->
  </div>
</div>
