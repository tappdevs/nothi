<div class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog"
  aria-labelledby="myExtraLargeModalLabel" aria-hidden="true" id="exampleModal">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">

      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">প্রাপকের তালিকা তৈরি</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body d-flex flex-column">
        <div class="card h-100">
          <div class="card-header">
            <strong>পদের নাম:</strong> অফিস সহকারী, ব্যবসা-বাণিজ্য শাখা
          </div>
          <div class="card-body overflow-hidden">
            <div class="row flex-nowrap h-100 overflow-hidden justify-content-between">
              <div class="flex-fill col-6">
                <div class="office-seal d-flex flex-column h-100">
                  <h4>অফিস শাখা</h4>
                  <select id="office_unit_dropdown" class="form-control select2-offscreen">
                    <option value="all">সকল শাখা</option>
                    <option value="299">গোপনীয় শাখা</option>
                    <option value="300">রাজস্ব</option>
                    <option value="301">জেনারেল সার্টিফিকেট শাখা</option>
                    <option value="302">রাজস্ব মুন্সিখানা শাখা</option>
                    <option value="303">রেকর্ড রুম শাখা</option>
                    <option value="306">ভি পি শাখা</option>
                    <option value="308">ভূমি অধিগ্রহণ শাখা</option>
                  </select>

                  <div class="flex-fill overflow-hidden pt-3">
                    <div class="pScroll">
                      <div class="card mb-3" data-id="299">
                        <div class="collapsed card-header d-flex justify-content-between align-items-center py-1"
                          data-toggle="collapse" data-target="#office_goponio">
                          <span>গোপনীয় শাখা</span>
                          <i class="fa fa-plus-circle mr-0"></i>
                        </div>
                        <div id="office_goponio" class="collapse card-body py-2">

                          <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" title="মো : আবদুল্লাহ আলম"
                              id="cusomCheck1">
                            <label class="custom-control-label" for="cusomCheck1" title="মো : আবদুল্লাহ আলম"
                              data-toggle="tooltip">অতিঃ জেলা প্রশাসক</label>
                          </div>


                          <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" title="মো : আবদুল্লাহ আলম"
                              id="customChck2">
                            <label class="custom-control-label" title="মো : আবদুল্লাহ আলম" data-toggle="tooltip"
                              for="customChck2">স্টেনোগ্রাফার</label>
                          </div>

                        </div>
                      </div>

                      <div class="card mb-3" data-id="300">
                        <div class="collapsed card-header d-flex justify-content-between align-items-center py-1"
                          data-toggle="collapse" data-target="#office_rajosho">
                          <span>রাজস্ব</span>
                          <i class="fa fa-plus-circle mr-0"></i>
                        </div>
                        <div id="office_rajosho" class="collapse card-body py-2">

                          <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" title="মো : আবদুল্লাহ আলম"
                              id="customCheck1">
                            <label class="custom-control-label" for="customCheck1" title="মো : আবদুল্লাহ আলম"
                              data-toggle="tooltip">অতিঃ জেলা প্রশাসক</label>
                          </div>


                          <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" title="মো : আবদুল্লাহ আলম"
                              id="customCheck2">
                            <label class="custom-control-label" for="customCheck2" title="মো : আবদুল্লাহ আলম"
                              data-toggle="tooltip">স্টেনোগ্রাফার</label>
                          </div>

                        </div>
                      </div><!--card-->

                      <div class="card mb-3" data-id="301">
                        <div class="collapsed card-header d-flex justify-content-between align-items-center py-1"
                          data-toggle="collapse" data-target="#office_general_certificate">
                          <span>জেনারেল সার্টিফিকেট শাখা</span>
                          <i class="fa fa-plus-circle mr-0"></i>
                        </div>
                        <div id="office_general_certificate" class="collapse card-body py-2">

                          <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" title="মো : আবদুল্লাহ আলম"
                              id="customCheck1">
                            <label class="custom-control-label" for="customCheck1" title="মো : আবদুল্লাহ আলম"
                              data-toggle="tooltip">অতিঃ জেলা প্রশাসক</label>
                          </div>


                          <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" title="মো : আবদুল্লাহ আলম"
                              id="customCheck2">
                            <label class="custom-control-label" for="customCheck2" title="মো : আবদুল্লাহ আলম"
                              data-toggle="tooltip">স্টেনোগ্রাফার</label>
                          </div>

                        </div>
                      </div><!--card-->

                      <div class="card mb-3" data-id="302">
                        <div class="collapsed card-header d-flex justify-content-between align-items-center py-1"
                          data-toggle="collapse" data-target="#office_munshikhana">
                          <span>রাজস্ব মুন্সিখানা শাখা</span>
                          <i class="fa fa-plus-circle mr-0"></i>
                        </div>
                        <div id="office_munshikhana" class="collapse card-body py-2">

                          <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" title="মো : আবদুল্লাহ আলম"
                              id="tomCheck1">
                            <label class="custom-control-label" for="tomCheck1" title="মো : আবদুল্লাহ আলম"
                              data-toggle="tooltip">অফিস সহকারী কাম কম্পিউটার অপারেটর</label>
                          </div>

                        </div>
                      </div><!--card-->

                      <div class="card mb-3" data-id="303">
                        <div class="collapsed card-header d-flex justify-content-between align-items-center py-1"
                          data-toggle="collapse" data-target="#office_record_room">
                          <span>রেকর্ড রুম শাখা</span>
                          <i class="fa fa-plus-circle mr-0"></i>
                        </div>
                        <div id="office_record_room" class="collapse card-body py-2">

                          <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" title="মো : আবদুল্লাহ আলম"
                              id="cutomCheck1">
                            <label class="custom-control-label" for="cutomCheck1" title="মো : আবদুল্লাহ আলম"
                              data-toggle="tooltip">সহকারী কমিশনার</label>
                          </div>


                          <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" title="মো : আবদুল্লাহ আলম"
                              id="custCheck2">
                            <label class="custom-control-label" for="custCheck2" title="মো : আবদুল্লাহ আলম"
                              data-toggle="tooltip">অফিস সহকারী কাম কম্পিউটার
                              মুদ্রাক্ষরিক</label>
                          </div>

                        </div>
                      </div><!--card-->

                      <div class="card mb-3" data-id="306">
                        <div class="collapsed card-header d-flex justify-content-between align-items-center py-1"
                          data-toggle="collapse" data-target="#office_vp_section">
                          <span>ভি পি শাখা</span>
                          <i class="fa fa-plus-circle mr-0"></i>
                        </div>
                        <div id="office_vp_section" class="collapse card-body py-2">

                          <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" title="মো : আবদুল্লাহ আলম"
                              id="cutoeck1">
                            <label class="custom-control-label" for="cutoeck1" title="মো : আবদুল্লাহ আলম"
                              data-toggle="tooltip">সহকারী কমিশনার</label>
                          </div>

                        </div>
                      </div><!--card-->

                      <div class="card mb-3" data-id="308">
                        <div class="collapsed card-header d-flex justify-content-between align-items-center py-1"
                          data-toggle="collapse" data-target="#office_odhigrohon">
                          <span>ভূমি অধিগ্রহণ শাখা</span>
                          <i class="fa fa-plus-circle mr-0"></i>
                        </div>
                        <div id="office_odhigrohon" class="collapse card-body py-2">

                          <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" title="মো : আবদুল্লাহ আলম"
                              id="cutoeck1w">
                            <label class="custom-control-label" for="cutoeck1w" title="মো : আবদুল্লাহ আলম"
                              data-toggle="tooltip">সহকারী কমিশনার</label>
                          </div>

                          <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" title="মো : আবদুল্লাহ আলম"
                              id="cutoeck1q">
                            <label class="custom-control-label" for="cutoeck1q" title="মো : আবদুল্লাহ আলম"
                              data-toggle="tooltip">সারভেয়ার</label>
                          </div>

                          <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" title="মো : আবদুল্লাহ আলম"
                              id="cutoeck1a">
                            <label class="custom-control-label" for="cutoeck1a" title="মো : আবদুল্লাহ আলম"
                              data-toggle="tooltip">অফিস সহকারী কাম কম্পিউটার অপারেটর</label>
                          </div>

                          <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" title="মো : আবদুল্লাহ আলম"
                              id="cutoeck1s">
                            <label class="custom-control-label" for="cutoeck1s" title="মো : আবদুল্লাহ আলম"
                              data-toggle="tooltip">সারভেয়ার</label>
                          </div>

                        </div>
                      </div><!--card-->

                      <div class="card mb-3" data-id="300">
                        <div class="collapsed card-header d-flex justify-content-between align-items-center py-1"
                          data-toggle="collapse" data-target="#office_rajosho">
                          <span>রাজস্ব</span>
                          <i class="fa fa-plus-circle mr-0"></i>
                        </div>
                        <div id="office_rajosho" class="collapse card-body py-2">

                          <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" title="মো : আবদুল্লাহ আলম"
                              id="customCheck1">
                            <label class="custom-control-label" for="customCheck1" title="মো : আবদুল্লাহ আলম"
                              data-toggle="tooltip">অতিঃ জেলা প্রশাসক</label>
                          </div>


                          <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" title="মো : আবদুল্লাহ আলম"
                              id="customCheck2">
                            <label class="custom-control-label" for="customCheck2" title="মো : আবদুল্লাহ আলম"
                              data-toggle="tooltip">স্টেনোগ্রাফার</label>
                          </div>

                        </div>
                      </div><!--card-->

                      <div class="card mb-3" data-id="301">
                        <div class="collapsed card-header d-flex justify-content-between align-items-center py-1"
                          data-toggle="collapse" data-target="#office_general_certificate">
                          <span>জেনারেল সার্টিফিকেট শাখা</span>
                          <i class="fa fa-plus-circle mr-0"></i>
                        </div>
                        <div id="office_general_certificate" class="collapse card-body py-2">

                          <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" title="মো : আবদুল্লাহ আলম"
                              id="customCheck1">
                            <label class="custom-control-label" for="customCheck1" title="মো : আবদুল্লাহ আলম"
                              data-toggle="tooltip">অতিঃ জেলা প্রশাসক</label>
                          </div>


                          <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" title="মো : আবদুল্লাহ আলম"
                              id="customCheck2">
                            <label class="custom-control-label" for="customCheck2" title="মো : আবদুল্লাহ আলম"
                              data-toggle="tooltip">স্টেনোগ্রাফার</label>
                          </div>

                        </div>
                      </div><!--card-->

                      <div class="card mb-3" data-id="302">
                        <div class="collapsed card-header d-flex justify-content-between align-items-center py-1"
                          data-toggle="collapse" data-target="#office_munshikhana">
                          <span>রাজস্ব মুন্সিখানা শাখা</span>
                          <i class="fa fa-plus-circle mr-0"></i>
                        </div>
                        <div id="office_munshikhana" class="collapse card-body py-2">

                          <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" title="মো : আবদুল্লাহ আলম"
                              id="tomCheck1">
                            <label class="custom-control-label" for="tomCheck1" title="মো : আবদুল্লাহ আলম"
                              data-toggle="tooltip">অফিস সহকারী কাম কম্পিউটার অপারেটর</label>
                          </div>

                        </div>
                      </div><!--card-->

                      <div class="card mb-3" data-id="303">
                        <div class="collapsed card-header d-flex justify-content-between align-items-center py-1"
                          data-toggle="collapse" data-target="#office_record_room">
                          <span>রেকর্ড রুম শাখা</span>
                          <i class="fa fa-plus-circle mr-0"></i>
                        </div>
                        <div id="office_record_room" class="collapse card-body py-2">

                          <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" title="মো : আবদুল্লাহ আলম"
                              id="cutomCheck1">
                            <label class="custom-control-label" for="cutomCheck1" title="মো : আবদুল্লাহ আলম"
                              data-toggle="tooltip">সহকারী কমিশনার</label>
                          </div>


                          <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" title="মো : আবদুল্লাহ আলম"
                              id="custCheck2">
                            <label class="custom-control-label" for="custCheck2" title="মো : আবদুল্লাহ আলম"
                              data-toggle="tooltip">অফিস সহকারী কাম কম্পিউটার
                              মুদ্রাক্ষরিক</label>
                          </div>

                        </div>
                      </div><!--card-->

                      <div class="card mb-3" data-id="306">
                        <div class="collapsed card-header d-flex justify-content-between align-items-center py-1"
                          data-toggle="collapse" data-target="#office_vp_section">
                          <span>ভি পি শাখা</span>
                          <i class="fa fa-plus-circle mr-0"></i>
                        </div>
                        <div id="office_vp_section" class="collapse card-body py-2">

                          <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" title="মো : আবদুল্লাহ আলম"
                              id="cutoeck1">
                            <label class="custom-control-label" for="cutoeck1" title="মো : আবদুল্লাহ আলম"
                              data-toggle="tooltip">সহকারী কমিশনার</label>
                          </div>

                        </div>
                      </div><!--card-->

                      <div class="card mb-0" data-id="308">
                        <div class="collapsed card-header d-flex justify-content-between align-items-center py-1"
                          data-toggle="collapse" data-target="#office_odhigrohon">
                          <span>ভূমি অধিগ্রহণ শাখা</span>
                          <i class="fa fa-plus-circle mr-0"></i>
                        </div>
                        <div id="office_odhigrohon" class="collapse card-body py-2">

                          <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" title="মো : আবদুল্লাহ আলম"
                              id="cutoeck1w">
                            <label class="custom-control-label" for="cutoeck1w" title="মো : আবদুল্লাহ আলম"
                              data-toggle="tooltip">সহকারী কমিশনার</label>
                          </div>

                          <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" title="মো : আবদুল্লাহ আলম"
                              id="cutoeck1q">
                            <label class="custom-control-label" for="cutoeck1q" title="মো : আবদুল্লাহ আলম"
                              data-toggle="tooltip">সারভেয়ার</label>
                          </div>

                          <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" title="মো : আবদুল্লাহ আলম"
                              id="cutoeck1a">
                            <label class="custom-control-label" for="cutoeck1a" title="মো : আবদুল্লাহ আলম"
                              data-toggle="tooltip">অফিস সহকারী কাম কম্পিউটার অপারেটর</label>
                          </div>

                          <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" title="মো : আবদুল্লাহ আলম"
                              id="cutoeck1s">
                            <label class="custom-control-label" for="cutoeck1s" title="মো : আবদুল্লাহ আলম"
                              data-toggle="tooltip">সারভেয়ার</label>
                          </div>

                        </div>
                      </div><!--card-->
                    </div><!--card-->
                  </div><!--card-->
                </div>
              </div>
              <div class="flex-fill col-6">
                <div class="office-seal-list d-flex flex-column h-100">
                  <h4>প্রাপকের তালিকা</h4>
                  <div class="pScroll">
                    <div id="office_unit_seal_panel">
                      <table class="table table-bordered table-sm mb-0">
                        <tbody>
                        <tr>
                          <td><input type="hidden"
                              name="office-employee" data-office-id="11" data-office-employee-name="কোহিনূর বেগম "
                              data-office-employee-id="52" data-office-unit-id="773"
                              data-office-unit-organogram-id="1833"
                              data-designation-name="অফিস সহকারী" data-unit-name="ব্যবসা-বাণিজ্য শাখা"><span title=""
                              data-original-title="">কোহিনূর বেগম , অফিস সহকারী, ব্যবসা-বাণিজ্য শাখা</span>
                          </td>
                        </tr>
                        <tr>
                          <td><input type="hidden"
                              name="office-employee" data-office-id="11"
                              data-office-employee-name="মো: শাহাবুদ্দিন খান "
                              data-office-employee-id="76" data-office-unit-id="333"
                              data-office-unit-organogram-id="817"
                              data-designation-name="জেলা প্রশাসক" data-unit-name="জেলা প্রশাসকের কার্যালয়"><span
                              title=""
                              data-original-title="">মো: শাহাবুদ্দিন খান , জেলা প্রশাসক, জেলা প্রশাসকের কার্যালয়</span>
                          </td>
                        </tr>
                        <tr>
                          <td><input type="hidden"
                              name="office-employee" data-office-id="11"
                              data-office-employee-name="খন্দকার মোহাম্মদ আব্দুল্লাহ আল মাহমুদ"
                              data-office-employee-id="79" data-office-unit-id="300"
                              data-office-unit-organogram-id="722"
                              data-designation-name="অতিঃ জেলা প্রশাসক" data-unit-name="রাজস্ব ">
                            <span title="" data-original-title="">খন্দকার মোহাম্মদ আব্দুল্লাহ আল মাহমুদ, অতিঃ জেলা প্রশাসক, রাজস্ব </span>
                          </td>
                        </tr>
                        <tr>
                          <td><input type="hidden"
                              name="office-employee" data-office-id="11" data-office-employee-name="মো: এহসানুল হক "
                              data-office-employee-id="83443" data-office-unit-id="333"
                              data-office-unit-organogram-id="818" data-designation-name="স্টেনোগ্রাফার (সি এ)"
                              data-unit-name="জেলা প্রশাসকের কার্যালয়">
                            <span title=""
                              data-original-title="">মো: এহসানুল হক , স্টেনোগ্রাফার (সি এ), জেলা প্রশাসকের কার্যালয়</span>
                          </td>
                        </tr>
                        <tr>
                          <td><input type="hidden"
                              name="office-employee" data-office-id="11" data-office-employee-name="কোহিনূর বেগম "
                              data-office-employee-id="52" data-office-unit-id="773"
                              data-office-unit-organogram-id="1833"
                              data-designation-name="অফিস সহকারী" data-unit-name="ব্যবসা-বাণিজ্য শাখা"><span title=""
                              data-original-title="">কোহিনূর বেগম , অফিস সহকারী, ব্যবসা-বাণিজ্য শাখা</span>
                          </td>
                        </tr>
                        <tr>
                          <td><input type="hidden"
                              name="office-employee" data-office-id="11"
                              data-office-employee-name="মো: শাহাবুদ্দিন খান "
                              data-office-employee-id="76" data-office-unit-id="333"
                              data-office-unit-organogram-id="817"
                              data-designation-name="জেলা প্রশাসক" data-unit-name="জেলা প্রশাসকের কার্যালয়"><span
                              title=""
                              data-original-title="">মো: শাহাবুদ্দিন খান , জেলা প্রশাসক, জেলা প্রশাসকের কার্যালয়</span>
                          </td>
                        </tr>
                        <tr>
                          <td><input type="hidden"
                              name="office-employee" data-office-id="11"
                              data-office-employee-name="খন্দকার মোহাম্মদ আব্দুল্লাহ আল মাহমুদ"
                              data-office-employee-id="79" data-office-unit-id="300"
                              data-office-unit-organogram-id="722"
                              data-designation-name="অতিঃ জেলা প্রশাসক" data-unit-name="রাজস্ব ">
                            <span title="" data-original-title="">খন্দকার মোহাম্মদ আব্দুল্লাহ আল মাহমুদ, অতিঃ জেলা প্রশাসক, রাজস্ব </span>
                          </td>
                        </tr>
                        <tr>
                          <td><input type="hidden"
                              name="office-employee" data-office-id="11" data-office-employee-name="মো: এহসানুল হক "
                              data-office-employee-id="83443" data-office-unit-id="333"
                              data-office-unit-organogram-id="818" data-designation-name="স্টেনোগ্রাফার (সি এ)"
                              data-unit-name="জেলা প্রশাসকের কার্যালয়">
                            <span title=""
                              data-original-title="">মো: এহসানুল হক , স্টেনোগ্রাফার (সি এ), জেলা প্রশাসকের কার্যালয়</span>
                          </td>
                        </tr>
                        <tr>
                          <td><input type="hidden"
                              name="office-employee" data-office-id="11" data-office-employee-name="কোহিনূর বেগম "
                              data-office-employee-id="52" data-office-unit-id="773"
                              data-office-unit-organogram-id="1833"
                              data-designation-name="অফিস সহকারী" data-unit-name="ব্যবসা-বাণিজ্য শাখা"><span title=""
                              data-original-title="">কোহিনূর বেগম , অফিস সহকারী, ব্যবসা-বাণিজ্য শাখা</span>
                          </td>
                        </tr>
                        <tr>
                          <td><input type="hidden"
                              name="office-employee" data-office-id="11"
                              data-office-employee-name="মো: শাহাবুদ্দিন খান "
                              data-office-employee-id="76" data-office-unit-id="333"
                              data-office-unit-organogram-id="817"
                              data-designation-name="জেলা প্রশাসক" data-unit-name="জেলা প্রশাসকের কার্যালয়"><span
                              title=""
                              data-original-title="">মো: শাহাবুদ্দিন খান , জেলা প্রশাসক, জেলা প্রশাসকের কার্যালয়</span>
                          </td>
                        </tr>
                        <tr>
                          <td><input type="hidden"
                              name="office-employee" data-office-id="11"
                              data-office-employee-name="খন্দকার মোহাম্মদ আব্দুল্লাহ আল মাহমুদ"
                              data-office-employee-id="79" data-office-unit-id="300"
                              data-office-unit-organogram-id="722"
                              data-designation-name="অতিঃ জেলা প্রশাসক" data-unit-name="রাজস্ব ">
                            <span title="" data-original-title="">খন্দকার মোহাম্মদ আব্দুল্লাহ আল মাহমুদ, অতিঃ জেলা প্রশাসক, রাজস্ব </span>
                          </td>
                        </tr>
                        <tr>
                          <td><input type="hidden"
                              name="office-employee" data-office-id="11" data-office-employee-name="মো: এহসানুল হক "
                              data-office-employee-id="83443" data-office-unit-id="333"
                              data-office-unit-organogram-id="818" data-designation-name="স্টেনোগ্রাফার (সি এ)"
                              data-unit-name="জেলা প্রশাসকের কার্যালয়">
                            <span title=""
                              data-original-title="">মো: এহসানুল হক , স্টেনোগ্রাফার (সি এ), জেলা প্রশাসকের কার্যালয়</span>
                          </td>
                        </tr>
                        <tr>
                          <td><input type="hidden"
                              name="office-employee" data-office-id="11" data-office-employee-name="কোহিনূর বেগম "
                              data-office-employee-id="52" data-office-unit-id="773"
                              data-office-unit-organogram-id="1833"
                              data-designation-name="অফিস সহকারী" data-unit-name="ব্যবসা-বাণিজ্য শাখা"><span title=""
                              data-original-title="">কোহিনূর বেগম , অফিস সহকারী, ব্যবসা-বাণিজ্য শাখা</span>
                          </td>
                        </tr>
                        <tr>
                          <td><input type="hidden"
                              name="office-employee" data-office-id="11"
                              data-office-employee-name="মো: শাহাবুদ্দিন খান "
                              data-office-employee-id="76" data-office-unit-id="333"
                              data-office-unit-organogram-id="817"
                              data-designation-name="জেলা প্রশাসক" data-unit-name="জেলা প্রশাসকের কার্যালয়"><span
                              title=""
                              data-original-title="">মো: শাহাবুদ্দিন খান , জেলা প্রশাসক, জেলা প্রশাসকের কার্যালয়</span>
                          </td>
                        </tr>
                        <tr>
                          <td><input type="hidden"
                              name="office-employee" data-office-id="11"
                              data-office-employee-name="খন্দকার মোহাম্মদ আব্দুল্লাহ আল মাহমুদ"
                              data-office-employee-id="79" data-office-unit-id="300"
                              data-office-unit-organogram-id="722"
                              data-designation-name="অতিঃ জেলা প্রশাসক" data-unit-name="রাজস্ব ">
                            <span title="" data-original-title="">খন্দকার মোহাম্মদ আব্দুল্লাহ আল মাহমুদ, অতিঃ জেলা প্রশাসক, রাজস্ব </span>
                          </td>
                        </tr>
                        <tr>
                          <td><input type="hidden"
                              name="office-employee" data-office-id="11" data-office-employee-name="মো: এহসানুল হক "
                              data-office-employee-id="83443" data-office-unit-id="333"
                              data-office-unit-organogram-id="818" data-designation-name="স্টেনোগ্রাফার (সি এ)"
                              data-unit-name="জেলা প্রশাসকের কার্যালয়">
                            <span title=""
                              data-original-title="">মো: এহসানুল হক , স্টেনোগ্রাফার (সি এ), জেলা প্রশাসকের কার্যালয়</span>
                          </td>
                        </tr>
                        <tr>
                          <td><input type="hidden"
                              name="office-employee" data-office-id="11" data-office-employee-name="কোহিনূর বেগম "
                              data-office-employee-id="52" data-office-unit-id="773"
                              data-office-unit-organogram-id="1833"
                              data-designation-name="অফিস সহকারী" data-unit-name="ব্যবসা-বাণিজ্য শাখা"><span title=""
                              data-original-title="">কোহিনূর বেগম , অফিস সহকারী, ব্যবসা-বাণিজ্য শাখা</span>
                          </td>
                        </tr>
                        <tr>
                          <td><input type="hidden"
                              name="office-employee" data-office-id="11"
                              data-office-employee-name="মো: শাহাবুদ্দিন খান "
                              data-office-employee-id="76" data-office-unit-id="333"
                              data-office-unit-organogram-id="817"
                              data-designation-name="জেলা প্রশাসক" data-unit-name="জেলা প্রশাসকের কার্যালয়"><span
                              title=""
                              data-original-title="">মো: শাহাবুদ্দিন খান , জেলা প্রশাসক, জেলা প্রশাসকের কার্যালয়</span>
                          </td>
                        </tr>
                        <tr>
                          <td><input type="hidden"
                              name="office-employee" data-office-id="11"
                              data-office-employee-name="খন্দকার মোহাম্মদ আব্দুল্লাহ আল মাহমুদ"
                              data-office-employee-id="79" data-office-unit-id="300"
                              data-office-unit-organogram-id="722"
                              data-designation-name="অতিঃ জেলা প্রশাসক" data-unit-name="রাজস্ব ">
                            <span title="" data-original-title="">খন্দকার মোহাম্মদ আব্দুল্লাহ আল মাহমুদ, অতিঃ জেলা প্রশাসক, রাজস্ব </span>
                          </td>
                        </tr>
                        <tr>
                          <td><input type="hidden"
                              name="office-employee" data-office-id="11" data-office-employee-name="মো: এহসানুল হক "
                              data-office-employee-id="83443" data-office-unit-id="333"
                              data-office-unit-organogram-id="818" data-designation-name="স্টেনোগ্রাফার (সি এ)"
                              data-unit-name="জেলা প্রশাসকের কার্যালয়">
                            <span title=""
                              data-original-title="">মো: এহসানুল হক , স্টেনোগ্রাফার (সি এ), জেলা প্রশাসকের কার্যালয়</span>
                          </td>
                        </tr>
                        <tr>
                          <td><input type="hidden"
                              name="office-employee" data-office-id="11" data-office-employee-name="কোহিনূর বেগম "
                              data-office-employee-id="52" data-office-unit-id="773"
                              data-office-unit-organogram-id="1833"
                              data-designation-name="অফিস সহকারী" data-unit-name="ব্যবসা-বাণিজ্য শাখা"><span title=""
                              data-original-title="">কোহিনূর বেগম , অফিস সহকারী, ব্যবসা-বাণিজ্য শাখা</span>
                          </td>
                        </tr>
                        <tr>
                          <td><input type="hidden"
                              name="office-employee" data-office-id="11"
                              data-office-employee-name="মো: শাহাবুদ্দিন খান "
                              data-office-employee-id="76" data-office-unit-id="333"
                              data-office-unit-organogram-id="817"
                              data-designation-name="জেলা প্রশাসক" data-unit-name="জেলা প্রশাসকের কার্যালয়"><span
                              title=""
                              data-original-title="">মো: শাহাবুদ্দিন খান , জেলা প্রশাসক, জেলা প্রশাসকের কার্যালয়</span>
                          </td>
                        </tr>
                        <tr>
                          <td><input type="hidden"
                              name="office-employee" data-office-id="11"
                              data-office-employee-name="খন্দকার মোহাম্মদ আব্দুল্লাহ আল মাহমুদ"
                              data-office-employee-id="79" data-office-unit-id="300"
                              data-office-unit-organogram-id="722"
                              data-designation-name="অতিঃ জেলা প্রশাসক" data-unit-name="রাজস্ব ">
                            <span title="" data-original-title="">খন্দকার মোহাম্মদ আব্দুল্লাহ আল মাহমুদ, অতিঃ জেলা প্রশাসক, রাজস্ব </span>
                          </td>
                        </tr>
                        <tr>
                          <td><input type="hidden"
                              name="office-employee" data-office-id="11" data-office-employee-name="মো: এহসানুল হক "
                              data-office-employee-id="83443" data-office-unit-id="333"
                              data-office-unit-organogram-id="818" data-designation-name="স্টেনোগ্রাফার (সি এ)"
                              data-unit-name="জেলা প্রশাসকের কার্যালয়">
                            <span title=""
                              data-original-title="">মো: এহসানুল হক , স্টেনোগ্রাফার (সি এ), জেলা প্রশাসকের কার্যালয়</span>
                          </td>
                        </tr>
                        <tr>
                          <td><input type="hidden"
                              name="office-employee" data-office-id="11" data-office-employee-name="কোহিনূর বেগম "
                              data-office-employee-id="52" data-office-unit-id="773"
                              data-office-unit-organogram-id="1833"
                              data-designation-name="অফিস সহকারী" data-unit-name="ব্যবসা-বাণিজ্য শাখা"><span title=""
                              data-original-title="">কোহিনূর বেগম , অফিস সহকারী, ব্যবসা-বাণিজ্য শাখা</span>
                          </td>
                        </tr>
                        <tr>
                          <td><input type="hidden"
                              name="office-employee" data-office-id="11"
                              data-office-employee-name="মো: শাহাবুদ্দিন খান "
                              data-office-employee-id="76" data-office-unit-id="333"
                              data-office-unit-organogram-id="817"
                              data-designation-name="জেলা প্রশাসক" data-unit-name="জেলা প্রশাসকের কার্যালয়"><span
                              title=""
                              data-original-title="">মো: শাহাবুদ্দিন খান , জেলা প্রশাসক, জেলা প্রশাসকের কার্যালয়</span>
                          </td>
                        </tr>
                        <tr>
                          <td><input type="hidden"
                              name="office-employee" data-office-id="11"
                              data-office-employee-name="খন্দকার মোহাম্মদ আব্দুল্লাহ আল মাহমুদ"
                              data-office-employee-id="79" data-office-unit-id="300"
                              data-office-unit-organogram-id="722"
                              data-designation-name="অতিঃ জেলা প্রশাসক" data-unit-name="রাজস্ব ">
                            <span title="" data-original-title="">খন্দকার মোহাম্মদ আব্দুল্লাহ আল মাহমুদ, অতিঃ জেলা প্রশাসক, রাজস্ব </span>
                          </td>
                        </tr>
                        <tr>
                          <td><input type="hidden"
                              name="office-employee" data-office-id="11" data-office-employee-name="মো: এহসানুল হক "
                              data-office-employee-id="83443" data-office-unit-id="333"
                              data-office-unit-organogram-id="818" data-designation-name="স্টেনোগ্রাফার (সি এ)"
                              data-unit-name="জেলা প্রশাসকের কার্যালয়">
                            <span title=""
                              data-original-title="">মো: এহসানুল হক , স্টেনোগ্রাফার (সি এ), জেলা প্রশাসকের কার্যালয়</span>
                          </td>
                        </tr>
                        <tr>
                          <td><input type="hidden"
                              name="office-employee" data-office-id="11" data-office-employee-name="কোহিনূর বেগম "
                              data-office-employee-id="52" data-office-unit-id="773"
                              data-office-unit-organogram-id="1833"
                              data-designation-name="অফিস সহকারী" data-unit-name="ব্যবসা-বাণিজ্য শাখা"><span title=""
                              data-original-title="">কোহিনূর বেগম , অফিস সহকারী, ব্যবসা-বাণিজ্য শাখা</span>
                          </td>
                        </tr>
                        <tr>
                          <td><input type="hidden"
                              name="office-employee" data-office-id="11"
                              data-office-employee-name="মো: শাহাবুদ্দিন খান "
                              data-office-employee-id="76" data-office-unit-id="333"
                              data-office-unit-organogram-id="817"
                              data-designation-name="জেলা প্রশাসক" data-unit-name="জেলা প্রশাসকের কার্যালয়"><span
                              title=""
                              data-original-title="">মো: শাহাবুদ্দিন খান , জেলা প্রশাসক, জেলা প্রশাসকের কার্যালয়</span>
                          </td>
                        </tr>
                        <tr>
                          <td><input type="hidden"
                              name="office-employee" data-office-id="11"
                              data-office-employee-name="খন্দকার মোহাম্মদ আব্দুল্লাহ আল মাহমুদ"
                              data-office-employee-id="79" data-office-unit-id="300"
                              data-office-unit-organogram-id="722"
                              data-designation-name="অতিঃ জেলা প্রশাসক" data-unit-name="রাজস্ব ">
                            <span title="" data-original-title="">খন্দকার মোহাম্মদ আব্দুল্লাহ আল মাহমুদ, অতিঃ জেলা প্রশাসক, রাজস্ব </span>
                          </td>
                        </tr>
                        <tr>
                          <td><input type="hidden"
                              name="office-employee" data-office-id="11" data-office-employee-name="মো: এহসানুল হক "
                              data-office-employee-id="83443" data-office-unit-id="333"
                              data-office-unit-organogram-id="818" data-designation-name="স্টেনোগ্রাফার (সি এ)"
                              data-unit-name="জেলা প্রশাসকের কার্যালয়">
                            <span title=""
                              data-original-title="">মো: এহসানুল হক , স্টেনোগ্রাফার (সি এ), জেলা প্রশাসকের কার্যালয়</span>
                          </td>
                        </tr>
                        <tr>
                          <td><input type="hidden"
                              name="office-employee" data-office-id="11" data-office-employee-name="কোহিনূর বেগম "
                              data-office-employee-id="52" data-office-unit-id="773"
                              data-office-unit-organogram-id="1833"
                              data-designation-name="অফিস সহকারী" data-unit-name="ব্যবসা-বাণিজ্য শাখা"><span title=""
                              data-original-title="">কোহিনূর বেগম , অফিস সহকারী, ব্যবসা-বাণিজ্য শাখা</span>
                          </td>
                        </tr>
                        <tr>
                          <td><input type="hidden"
                              name="office-employee" data-office-id="11"
                              data-office-employee-name="মো: শাহাবুদ্দিন খান "
                              data-office-employee-id="76" data-office-unit-id="333"
                              data-office-unit-organogram-id="817"
                              data-designation-name="জেলা প্রশাসক" data-unit-name="জেলা প্রশাসকের কার্যালয়"><span
                              title=""
                              data-original-title="">মো: শাহাবুদ্দিন খান , জেলা প্রশাসক, জেলা প্রশাসকের কার্যালয়</span>
                          </td>
                        </tr>
                        <tr>
                          <td><input type="hidden"
                              name="office-employee" data-office-id="11"
                              data-office-employee-name="খন্দকার মোহাম্মদ আব্দুল্লাহ আল মাহমুদ"
                              data-office-employee-id="79" data-office-unit-id="300"
                              data-office-unit-organogram-id="722"
                              data-designation-name="অতিঃ জেলা প্রশাসক" data-unit-name="রাজস্ব ">
                            <span title="" data-original-title="">খন্দকার মোহাম্মদ আব্দুল্লাহ আল মাহমুদ, অতিঃ জেলা প্রশাসক, রাজস্ব </span>
                          </td>
                        </tr>
                        <tr>
                          <td><input type="hidden"
                              name="office-employee" data-office-id="11" data-office-employee-name="মো: এহসানুল হক "
                              data-office-employee-id="83443" data-office-unit-id="333"
                              data-office-unit-organogram-id="818" data-designation-name="স্টেনোগ্রাফার (সি এ)"
                              data-unit-name="জেলা প্রশাসকের কার্যালয়">
                            <span title=""
                              data-original-title="">মো: এহসানুল হক , স্টেনোগ্রাফার (সি এ), জেলা প্রশাসকের কার্যালয়</span>
                          </td>
                        </tr>
                        <tr>
                          <td><input type="hidden"
                              name="office-employee" data-office-id="11" data-office-employee-name="কোহিনূর বেগম "
                              data-office-employee-id="52" data-office-unit-id="773"
                              data-office-unit-organogram-id="1833"
                              data-designation-name="অফিস সহকারী" data-unit-name="ব্যবসা-বাণিজ্য শাখা"><span title=""
                              data-original-title="">কোহিনূর বেগম , অফিস সহকারী, ব্যবসা-বাণিজ্য শাখা</span>
                          </td>
                        </tr>
                        <tr>
                          <td><input type="hidden"
                              name="office-employee" data-office-id="11"
                              data-office-employee-name="মো: শাহাবুদ্দিন খান "
                              data-office-employee-id="76" data-office-unit-id="333"
                              data-office-unit-organogram-id="817"
                              data-designation-name="জেলা প্রশাসক" data-unit-name="জেলা প্রশাসকের কার্যালয়"><span
                              title=""
                              data-original-title="">মো: শাহাবুদ্দিন খান , জেলা প্রশাসক, জেলা প্রশাসকের কার্যালয়</span>
                          </td>
                        </tr>
                        <tr>
                          <td><input type="hidden"
                              name="office-employee" data-office-id="11"
                              data-office-employee-name="খন্দকার মোহাম্মদ আব্দুল্লাহ আল মাহমুদ"
                              data-office-employee-id="79" data-office-unit-id="300"
                              data-office-unit-organogram-id="722"
                              data-designation-name="অতিঃ জেলা প্রশাসক" data-unit-name="রাজস্ব ">
                            <span title="" data-original-title="">খন্দকার মোহাম্মদ আব্দুল্লাহ আল মাহমুদ, অতিঃ জেলা প্রশাসক, রাজস্ব </span>
                          </td>
                        </tr>
                        <tr>
                          <td><input type="hidden"
                              name="office-employee" data-office-id="11" data-office-employee-name="মো: এহসানুল হক "
                              data-office-employee-id="83443" data-office-unit-id="333"
                              data-office-unit-organogram-id="818" data-designation-name="স্টেনোগ্রাফার (সি এ)"
                              data-unit-name="জেলা প্রশাসকের কার্যালয়">
                            <span title=""
                              data-original-title="">মো: এহসানুল হক , স্টেনোগ্রাফার (সি এ), জেলা প্রশাসকের কার্যালয়</span>
                          </td>
                        </tr>
                        </tbody>
                      </table>
                    </div>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-success">সিল সংরক্ষণ করুন</button>
        <button class="btn btn-success">অস্থায়ী সিল সংরক্ষণ করুন</button>
        <button class="btn btn-danger" data-dismiss="modal">বন্ধ করুন</button>
      </div>
    </div>
  </div>
</div>
