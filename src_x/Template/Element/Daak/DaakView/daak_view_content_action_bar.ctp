<div class='title font-weight-normal'>
  সহকারী পরিচালক জাতীয় ভোক্তা অধিকার সংরক্ষণ। সবিনয় নিবেদন এই যে, আমি নিম্নস্বাক্ষরকারী
  উপর্যুক্ত বিষয়ে প্রয়োজনীয় তথ্যাদি আপনার সদয় বিবেচনার জন্য উপস্থাপন করলাম।

  <div class='decision pt-2 d-flex justify-content-between'>
    <div>
        <span
          class='badge badge-pill badge-off-white font-weight-normal py-1 px-2'>সদয় সিদ্ধান্তের জন্যে প্রেরণ করা হলো</span>
      <span class='badge badge-pill badge-light font-weight-normal py-1 px-2'>ডকেটিং নাম্বার: ১৫৭০</span>
      <span class='badge badge-pill badge-light font-weight-normal py-1 px-2'>স্বারক নাম্বার: ০৫.৪৫.৩৯০০.০১৫.১৭.০২১.১৮.৩৩১</span>
    </div>
    <div class='action-group'>
      <button
        class='btn btn-square btn-dark-o btn-sm daak_forward' data-toggle="modal" data-target="#exampleModal<?= $movement_no ?>"><i
          class='fa fa-mail-forward'></i>
      </button>
      <button
        class='btn btn-square btn-dark-o   btn-sm' data-toggle='tooltip'
        title='' data-original-title='নথিতে উপস্থাপিত'><i
          class='fa fa-files-o'></i></button>
      <a href="nothi-management.html"
        class='btn btn-square btn-dark-o btn-sm' data-toggle="modal" data-target=".nothi-jato-modal"><i
          data-toggle='tooltip' title='নথিজাত' class='fa fa-archive'></i>
      </a>
      <button
        class='btn btn-square btn-dark-o btn-sm' data-toggle='tooltip' title=''
        data-original-title='আর্কাইভ'><i class='icon-drawer'></i></button>
      <button
        class='btn btn-square btn-dark-o btn-sm' data-toggle='tooltip' title=''
        data-original-title='প্রিন্ট'><i class='icons cui-print'></i></button>
    </div>
  </div>
</div>
