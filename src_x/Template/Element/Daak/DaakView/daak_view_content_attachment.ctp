<div class="timeline-horizontal py-3">
  <div class="my-5 d-flex align-items-center">
    <h5 class="mr-3 mb-0">বর্তমান অবস্থা </h5>
    <span class="badge badge-light font-weight-normal fs-16 py-2 px-3">
                    পদবি স্থানান্তরিত করা হয়েছে পদবি স্থানান্তরিত করা হয়েছে
                  </span></div>


  <div class="pScroll horizontal p-3">
    <div class="timeline-items d-flex align-items-stretch">
      <div class="timeline-item">
        <div class="tl_sender">
          <div class="d-flex align-items-start">
            <img src="<?= $this->request->webroot ?>coreui/images/profils.jpg" alt="profile">
            <div class="mb-0">
              <p>মো: শাহাবুদ্দিন খান</p>
              <p class="mb-1">
                <small>সিনিয়র সফটওয়্যার ইঞ্জিনিয়ার, ই-সার্ভিস, এ্যাকসেস টু ইনফরমেশন (এটু্আই) প্রোগ্রাম
                </small>
              </p>
            </div>
          </div>
          <span class="badge badge-off-white font-weight-normal px-2 pt-1">পদবি স্থানান্তরিত করা হয়েছে পদবি স্থানান্তরিত করা হয়েছে</span>
        </div>
        <p class="tl_date"><span>২০/৬/১৮ ১১:২৭ পূর্বাহ্ণ</span></p>
        <div class="circle"></div>
        <div class="item-wrap">
          <table>
            <tr>
              <th class="text-right pr-2">মূল প্রাপক</th>
              <td class="pb-2">
                <p class="mb-0">মোঃ হাসানুজ্জামান</p>
                <small>সিনিয়র সফটওয়্যার ইঞ্জিনিয়ার (ভারপ্রাপ্ত)
                  ই-সার্ভিস, এ্যাকসেস টু ইনফরমেশন (এটু্আই)
                  প্রোগ্রাম
                </small>
              </td>
            </tr>
            <tr>
              <th class="text-right pr-2">অনুলীপি প্রাপক</th>
              <td>
                <ul class="list-inline mb-0 list-2">
                  <li>
                    <p class="mb-0">মোঃ হাসানুজ্জামান</p>
                    <small>সিনিয়র সফটওয়্যার ইঞ্জিনিয়ার (ভারপ্রাপ্ত)
                      ই-সার্ভিস, এ্যাকসেস টু ইনফরমেশন (এটু্আই)
                      প্রোগ্রাম
                    </small>
                  </li>
                  <li>
                    <p class="mb-0">মোঃ হাসানুজ্জামান</p>
                    <small>সিনিয়র সফটওয়্যার ইঞ্জিনিয়ার (ভারপ্রাপ্ত)
                      ই-সার্ভিস, এ্যাকসেস টু ইনফরমেশন (এটু্আই)
                      প্রোগ্রাম
                    </small>
                  </li>
                  <li>
                    <p class="mb-0">মোঃ হাসানুজ্জামান</p>
                    <small>সিনিয়র সফটওয়্যার ইঞ্জিনিয়ার (ভারপ্রাপ্ত)
                      ই-সার্ভিস, এ্যাকসেস টু ইনফরমেশন (এটু্আই)
                      প্রোগ্রাম
                    </small>
                  </li>
                </ul>
              </td>
            </tr>
          </table>
        </div>
      </div>
      <div class="timeline-item">
        <div class="tl_sender">
          <div class="d-flex align-items-start">
            <img src="<?= $this->request->webroot ?>coreui/images/profils.jpg" alt="profile">
            <div class="mb-0">
              <p>মো: শাহাবুদ্দিন খান</p>
              <p class="mb-1">
                <small>সিনিয়র সফটওয়্যার ইঞ্জিনিয়ার, ই-সার্ভিস, এ্যাকসেস টু ইনফরমেশন (এটু্আই) প্রোগ্রাম
                </small>
              </p>
            </div>
          </div>
          <span class="badge badge-off-white font-weight-normal px-2 py-1">পদবি স্থানান্তরিত করা হয়েছে পদবি স্থানান্তরিত করা হয়েছে</span>
        </div>
        <p class="tl_date"><span>২০/৬/১৮ ১১:২৭ পূর্বাহ্ণ</span></p>
        <div class="circle"></div>
        <div class="item-wrap">
          <table>
            <tr>
              <th class="text-right pr-2">মূল প্রাপক</th>
              <td class="pb-2">
                <p class="mb-0">মোঃ হাসানুজ্জামান</p>
                <small>সিনিয়র সফটওয়্যার ইঞ্জিনিয়ার (ভারপ্রাপ্ত)
                  ই-সার্ভিস, এ্যাকসেস টু ইনফরমেশন (এটু্আই)
                  প্রোগ্রাম
                </small>
              </td>
            </tr>
            <tr>
              <th class="text-right pr-2">অনুলীপি প্রাপক</th>
              <td>
                <ul class="list-inline mb-0 list-2">
                  <li>
                    <p class="mb-0">মোঃ হাসানুজ্জামান</p>
                    <small>সিনিয়র সফটওয়্যার ইঞ্জিনিয়ার (ভারপ্রাপ্ত)
                      ই-সার্ভিস, এ্যাকসেস টু ইনফরমেশন (এটু্আই)
                      প্রোগ্রাম
                    </small>
                  </li>
                </ul>
              </td>
            </tr>
          </table>
        </div>
      </div>
      <div class="timeline-item">
        <div class="tl_sender">
          <div class="d-flex align-items-start">
            <img src="<?= $this->request->webroot ?>coreui/images/profils.jpg" alt="profile">
            <div class="mb-0">
              <p>মো: শাহাবুদ্দিন খান</p>
              <p class="mb-1">
                <small>সিনিয়র সফটওয়্যার ইঞ্জিনিয়ার, ই-সার্ভিস, এ্যাকসেস টু ইনফরমেশন (এটু্আই) প্রোগ্রাম
                </small>
              </p>
            </div>
          </div>
          <span class="badge badge-off-white font-weight-normal px-2 py-1">পদবি স্থানান্তরিত করা হয়েছে পদবি স্থানান্তরিত করা হয়েছে</span>
        </div>
        <p class="tl_date"><span>২০/৬/১৮ ১১:২৭ পূর্বাহ্ণ</span></p>
        <div class="circle"></div>
        <div class="item-wrap">
          <table>
            <tr>
              <th class="text-right pr-2">মূল প্রাপক</th>
              <td class="pb-2">
                <p class="mb-0">মোঃ হাসানুজ্জামান</p>
                <small>সিনিয়র সফটওয়্যার ইঞ্জিনিয়ার (ভারপ্রাপ্ত)
                  ই-সার্ভিস, এ্যাকসেস টু ইনফরমেশন (এটু্আই)
                  প্রোগ্রাম
                </small>
              </td>
            </tr>
            <tr>
              <th class="text-right pr-2">অনুলীপি প্রাপক</th>
              <td>
                <ul class="list-inline mb-0 list-2">
                  <li>
                    <p class="mb-0">মোঃ হাসানুজ্জামান</p>
                    <small>সিনিয়র সফটওয়্যার ইঞ্জিনিয়ার (ভারপ্রাপ্ত)
                      ই-সার্ভিস, এ্যাকসেস টু ইনফরমেশন (এটু্আই)
                      প্রোগ্রাম
                    </small>
                  </li>
                  <li>
                    <p class="mb-0">মোঃ হাসানুজ্জামান</p>
                    <small>সিনিয়র সফটওয়্যার ইঞ্জিনিয়ার (ভারপ্রাপ্ত)
                      ই-সার্ভিস, এ্যাকসেস টু ইনফরমেশন (এটু্আই)
                      প্রোগ্রাম
                    </small>
                  </li>
                </ul>
              </td>
            </tr>
          </table>
        </div>
      </div>
      <div class="timeline-item">
        <div class="tl_sender">
          <div class="d-flex align-items-start">
            <img src="<?= $this->request->webroot ?>coreui/images/profils.jpg" alt="profile">
            <div class="mb-0">
              <p>মো: শাহাবুদ্দিন খান</p>
              <p class="mb-1">
                <small>সিনিয়র সফটওয়্যার ইঞ্জিনিয়ার, ই-সার্ভিস, এ্যাকসেস টু ইনফরমেশন (এটু্আই) প্রোগ্রাম
                </small>
              </p>
            </div>
          </div>
          <span class="badge badge-off-white font-weight-normal px-2 py-1">পদবি স্থানান্তরিত করা হয়েছে পদবি স্থানান্তরিত করা হয়েছে</span>
        </div>
        <p class="tl_date"><span>২০/৬/১৮ ১১:২৭ পূর্বাহ্ণ</span></p>
        <div class="circle"></div>
        <div class="item-wrap">
          <table>
            <tr>
              <th class="text-right pr-2">মূল প্রাপক</th>
              <td class="pb-2">
                <p class="mb-0">মোঃ হাসানুজ্জামান</p>
                <small>সিনিয়র সফটওয়্যার ইঞ্জিনিয়ার (ভারপ্রাপ্ত)
                  ই-সার্ভিস, এ্যাকসেস টু ইনফরমেশন (এটু্আই)
                  প্রোগ্রাম
                </small>
              </td>
            </tr>
            <tr>
              <th class="text-right pr-2">অনুলীপি প্রাপক</th>
              <td>
                <ul class="list-inline mb-0 list-2">
                  <li>
                    <p class="mb-0">মোঃ হাসানুজ্জামান</p>
                    <small>সিনিয়র সফটওয়্যার ইঞ্জিনিয়ার (ভারপ্রাপ্ত)
                      ই-সার্ভিস, এ্যাকসেস টু ইনফরমেশন (এটু্আই)
                      প্রোগ্রাম
                    </small>
                  </li>
                  <li>
                    <p class="mb-0">মোঃ হাসানুজ্জামান</p>
                    <small>সিনিয়র সফটওয়্যার ইঞ্জিনিয়ার (ভারপ্রাপ্ত)
                      ই-সার্ভিস, এ্যাকসেস টু ইনফরমেশন (এটু্আই)
                      প্রোগ্রাম
                    </small>
                  </li>
                  <li>
                    <p class="mb-0">মোঃ হাসানুজ্জামান</p>
                    <small>সিনিয়র সফটওয়্যার ইঞ্জিনিয়ার (ভারপ্রাপ্ত)
                      ই-সার্ভিস, এ্যাকসেস টু ইনফরমেশন (এটু্আই)
                      প্রোগ্রাম
                    </small>
                  </li>
                  <li>
                    <p class="mb-0">মোঃ হাসানুজ্জামান</p>
                    <small>সিনিয়র সফটওয়্যার ইঞ্জিনিয়ার (ভারপ্রাপ্ত)
                      ই-সার্ভিস, এ্যাকসেস টু ইনফরমেশন (এটু্আই)
                      প্রোগ্রাম
                    </small>
                  </li>
                  <li>
                    <p class="mb-0">মোঃ হাসানুজ্জামান</p>
                    <small>সিনিয়র সফটওয়্যার ইঞ্জিনিয়ার (ভারপ্রাপ্ত)
                      ই-সার্ভিস, এ্যাকসেস টু ইনফরমেশন (এটু্আই)
                      প্রোগ্রাম
                    </small>
                  </li>
                </ul>
              </td>
            </tr>
          </table>
        </div>
      </div>
      <div class="timeline-item">
        <div class="tl_sender">
          <div class="d-flex align-items-start">
            <img src="<?= $this->request->webroot ?>coreui/images/profils.jpg" alt="profile">
            <div class="mb-0">
              <p>মো: শাহাবুদ্দিন খান</p>
              <p class="mb-1">
                <small>সিনিয়র সফটওয়্যার ইঞ্জিনিয়ার, ই-সার্ভিস, এ্যাকসেস টু ইনফরমেশন (এটু্আই) প্রোগ্রাম
                </small>
              </p>
            </div>
          </div>
          <span class="badge badge-off-white font-weight-normal px-2 py-1">পদবি স্থানান্তরিত করা হয়েছে পদবি স্থানান্তরিত করা হয়েছে</span>
        </div>
        <p class="tl_date"><span>২০/৬/১৮ ১১:২৭ পূর্বাহ্ণ</span></p>
        <div class="circle"></div>
        <div class="item-wrap">
          <table>
            <tr>
              <th class="text-right pr-2">মূল প্রাপক</th>
              <td class="pb-2">
                <p class="mb-0">মোঃ হাসানুজ্জামান</p>
                <small>সিনিয়র সফটওয়্যার ইঞ্জিনিয়ার (ভারপ্রাপ্ত)
                  ই-সার্ভিস, এ্যাকসেস টু ইনফরমেশন (এটু্আই)
                  প্রোগ্রাম
                </small>
              </td>
            </tr>
            <tr>
              <th class="text-right pr-2">অনুলীপি প্রাপক</th>
              <td>
                <ul class="list-inline mb-0 list-2">
                  <li>
                    <p class="mb-0">মোঃ হাসানুজ্জামান</p>
                    <small>সিনিয়র সফটওয়্যার ইঞ্জিনিয়ার (ভারপ্রাপ্ত)
                      ই-সার্ভিস, এ্যাকসেস টু ইনফরমেশন (এটু্আই)
                      প্রোগ্রাম
                    </small>
                  </li>
                  <li>
                    <p class="mb-0">মোঃ হাসানুজ্জামান</p>
                    <small>সিনিয়র সফটওয়্যার ইঞ্জিনিয়ার (ভারপ্রাপ্ত)
                      ই-সার্ভিস, এ্যাকসেস টু ইনফরমেশন (এটু্আই)
                      প্রোগ্রাম
                    </small>
                  </li>
                  <li>
                    <p class="mb-0">মোঃ হাসানুজ্জামান</p>
                    <small>সিনিয়র সফটওয়্যার ইঞ্জিনিয়ার (ভারপ্রাপ্ত)
                      ই-সার্ভিস, এ্যাকসেস টু ইনফরমেশন (এটু্আই)
                      প্রোগ্রাম
                    </small>
                  </li>
                </ul>
              </td>
            </tr>
          </table>
        </div>
      </div>
      <div class="timeline-item">
        <div class="tl_sender">
          <div class="d-flex align-items-start">
            <img src="<?= $this->request->webroot ?>coreui/images/profils.jpg" alt="profile">
            <div class="mb-0">
              <p>মো: শাহাবুদ্দিন খান</p>
              <p class="mb-1">
                <small>সিনিয়র সফটওয়্যার ইঞ্জিনিয়ার, ই-সার্ভিস, এ্যাকসেস টু ইনফরমেশন (এটু্আই) প্রোগ্রাম
                </small>
              </p>
            </div>
          </div>
          <span class="badge badge-off-white font-weight-normal px-2 py-1">পদবি স্থানান্তরিত করা হয়েছে পদবি স্থানান্তরিত করা হয়েছে</span>
        </div>
        <p class="tl_date"><span>২০/৬/১৮ ১১:২৭ পূর্বাহ্ণ</span></p>
        <div class="circle"></div>
        <div class="item-wrap">
          <table>
            <tr>
              <th class="text-right pr-2">মূল প্রাপক</th>
              <td class="pb-2">
                <p class="mb-0">মোঃ হাসানুজ্জামান</p>
                <small>সিনিয়র সফটওয়্যার ইঞ্জিনিয়ার (ভারপ্রাপ্ত)
                  ই-সার্ভিস, এ্যাকসেস টু ইনফরমেশন (এটু্আই)
                  প্রোগ্রাম
                </small>
              </td>
            </tr>
            <tr>
              <th class="text-right pr-2">অনুলীপি প্রাপক</th>
              <td>
                <ul class="list-inline mb-0 list-2">
                  <li>
                    <p class="mb-0">মোঃ হাসানুজ্জামান</p>
                    <small>সিনিয়র সফটওয়্যার ইঞ্জিনিয়ার (ভারপ্রাপ্ত)
                      ই-সার্ভিস, এ্যাকসেস টু ইনফরমেশন (এটু্আই)
                      প্রোগ্রাম
                    </small>
                  </li>
                  <li>
                    <p class="mb-0">মোঃ হাসানুজ্জামান</p>
                    <small>সিনিয়র সফটওয়্যার ইঞ্জিনিয়ার (ভারপ্রাপ্ত)
                      ই-সার্ভিস, এ্যাকসেস টু ইনফরমেশন (এটু্আই)
                      প্রোগ্রাম
                    </small>
                  </li>
                  <li>
                    <p class="mb-0">মোঃ হাসানুজ্জামান</p>
                    <small>সিনিয়র সফটওয়্যার ইঞ্জিনিয়ার (ভারপ্রাপ্ত)
                      ই-সার্ভিস, এ্যাকসেস টু ইনফরমেশন (এটু্আই)
                      প্রোগ্রাম
                    </small>
                  </li>
                </ul>
              </td>
            </tr>
          </table>
        </div>
      </div>
      <div class="timeline-item">
        <div class="tl_sender">
          <div class="d-flex align-items-start">
            <img src="<?= $this->request->webroot ?>coreui/images/profils.jpg" alt="profile">
            <div class="mb-0">
              <p>মো: শাহাবুদ্দিন খান</p>
              <p class="mb-1">
                <small>সিনিয়র সফটওয়্যার ইঞ্জিনিয়ার, ই-সার্ভিস, এ্যাকসেস টু ইনফরমেশন (এটু্আই) প্রোগ্রাম
                </small>
              </p>
            </div>
          </div>
          <span class="badge badge-off-white font-weight-normal px-2 py-1">পদবি স্থানান্তরিত করা হয়েছে পদবি স্থানান্তরিত করা হয়েছে</span>
        </div>
        <p class="tl_date"><span>২০/৬/১৮ ১১:২৭ পূর্বাহ্ণ</span></p>
        <div class="circle"></div>
        <div class="item-wrap">
          <table>
            <tr>
              <th class="text-right pr-2">মূল প্রাপক</th>
              <td class="pb-2">
                <p class="mb-0">মোঃ হাসানুজ্জামান</p>
                <small>সিনিয়র সফটওয়্যার ইঞ্জিনিয়ার (ভারপ্রাপ্ত)
                  ই-সার্ভিস, এ্যাকসেস টু ইনফরমেশন (এটু্আই)
                  প্রোগ্রাম
                </small>
              </td>
            </tr>
            <tr>
              <th class="text-right pr-2">অনুলীপি প্রাপক</th>
              <td>
                <ul class="list-inline mb-0 list-2">
                  <li>
                    <p class="mb-0">মোঃ হাসানুজ্জামান</p>
                    <small>সিনিয়র সফটওয়্যার ইঞ্জিনিয়ার (ভারপ্রাপ্ত)
                      ই-সার্ভিস, এ্যাকসেস টু ইনফরমেশন (এটু্আই)
                      প্রোগ্রাম
                    </small>
                  </li>
                  <li>
                    <p class="mb-0">মোঃ হাসানুজ্জামান</p>
                    <small>সিনিয়র সফটওয়্যার ইঞ্জিনিয়ার (ভারপ্রাপ্ত)
                      ই-সার্ভিস, এ্যাকসেস টু ইনফরমেশন (এটু্আই)
                      প্রোগ্রাম
                    </small>
                  </li>
                  <li>
                    <p class="mb-0">মোঃ হাসানুজ্জামান</p>
                    <small>সিনিয়র সফটওয়্যার ইঞ্জিনিয়ার (ভারপ্রাপ্ত)
                      ই-সার্ভিস, এ্যাকসেস টু ইনফরমেশন (এটু্আই)
                      প্রোগ্রাম
                    </small>
                  </li>
                </ul>
              </td>
            </tr>
          </table>
        </div>
      </div>
      <div class="timeline-item">
        <div class="tl_sender">
          <div class="d-flex align-items-start">
            <img src="<?= $this->request->webroot ?>coreui/images/profils.jpg" alt="profile">
            <div class="mb-0">
              <p>মো: শাহাবুদ্দিন খান</p>
              <p class="mb-1">
                <small>সিনিয়র সফটওয়্যার ইঞ্জিনিয়ার, ই-সার্ভিস, এ্যাকসেস টু ইনফরমেশন (এটু্আই) প্রোগ্রাম
                </small>
              </p>
            </div>
          </div>
          <span class="badge badge-off-white font-weight-normal px-2 py-1">পদবি স্থানান্তরিত করা হয়েছে পদবি স্থানান্তরিত করা হয়েছে</span>
        </div>
        <p class="tl_date"><span>২০/৬/১৮ ১১:২৭ পূর্বাহ্ণ</span></p>
        <div class="circle"></div>
        <div class="item-wrap">
          <table>
            <tr>
              <th class="text-right pr-2">মূল প্রাপক</th>
              <td class="pb-2">
                <p class="mb-0">মোঃ হাসানুজ্জামান</p>
                <small>সিনিয়র সফটওয়্যার ইঞ্জিনিয়ার (ভারপ্রাপ্ত)
                  ই-সার্ভিস, এ্যাকসেস টু ইনফরমেশন (এটু্আই)
                  প্রোগ্রাম
                </small>
              </td>
            </tr>
            <tr>
              <th class="text-right pr-2">অনুলীপি প্রাপক</th>
              <td>
                <ul class="list-inline mb-0 list-2">
                  <li>
                    <p class="mb-0">মোঃ হাসানুজ্জামান</p>
                    <small>সিনিয়র সফটওয়্যার ইঞ্জিনিয়ার (ভারপ্রাপ্ত)
                      ই-সার্ভিস, এ্যাকসেস টু ইনফরমেশন (এটু্আই)
                      প্রোগ্রাম
                    </small>
                  </li>
                  <li>
                    <p class="mb-0">মোঃ হাসানুজ্জামান</p>
                    <small>সিনিয়র সফটওয়্যার ইঞ্জিনিয়ার (ভারপ্রাপ্ত)
                      ই-সার্ভিস, এ্যাকসেস টু ইনফরমেশন (এটু্আই)
                      প্রোগ্রাম
                    </small>
                  </li>
                  <li>
                    <p class="mb-0">মোঃ হাসানুজ্জামান</p>
                    <small>সিনিয়র সফটওয়্যার ইঞ্জিনিয়ার (ভারপ্রাপ্ত)
                      ই-সার্ভিস, এ্যাকসেস টু ইনফরমেশন (এটু্আই)
                      প্রোগ্রাম
                    </small>
                  </li>
                </ul>
              </td>
            </tr>
          </table>
        </div>
      </div>


    </div>
  </div>
</div>
