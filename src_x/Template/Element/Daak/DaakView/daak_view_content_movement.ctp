<div class="modal fade" id="exampleModal<?= $movement_no ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="daak_movement py-3">

          <div class="row">
            <div class='daptorik_sender col-lg-8'>
              <div class='d-flex justify-content-between align-items-center mb-3 pb-1'>
                <h5 class='mb-0'>প্রাপক <?= $movement_no ?></h5>
                <div class='add_receiver'>
                  <a href="daak-prapok-management.html" class='btn btn-light'><i class='fa fa-cogs mr-2'></i>
                    প্রাপক ব্যবস্থাপনা
                  </a>
                </div>
              </div>

              <div class='sender_selection'>
                <div class='advanced_search'>
                  <div class='sender_list pScroll' style="max-height: 366px;">
                    <ul class='list-group'>
                      <li class='list-group-item d-flex align-items-center justify-content-between'>
                        <div class='list_content d-flex align-items-center'>
                          <img
                            class='rounded-circle border'
                            src='<?= $this->request->webroot ?>coreui/images/profils.jpg'
                            width='48' height='48'>
                          <div class='pl-3'>
                            <p class='mb-0'>মুহাম্মদ ইউনুস আলী সরদার</p>
                            <small>টেকনোলজি এক্সপার্ট, ই-সার্ভিস</small>
                          </div>
                        </div>

                        <div class='list_button'>
                          <button class='btn btn-outline-success btn_main'>
                            মূল
                          </button>
                          <button class='btn btn-outline-info btn_copy'>
                            অনুলীপি
                          </button>
                          <span class='badge badge-success fs-14 py-1 px-2 label_main'>মূল প্রাপক</span>
                          <span
                            class='badge badge-info text-white fs-14 py-1 px-2 label_copy'>অনুলীপি প্রাপক</span>
                        </div>
                        <div class='custom-control custom-checkbox' hidden>
                          <input
                            type='checkbox' class='custom-control-input'>
                          <label class='custom-control-label'></label>
                        </div>
                      </li>
                      <li class='list-group-item d-flex align-items-center justify-content-between'>
                        <div class='list_content d-flex align-items-center'>
                          <img
                            class='rounded-circle border'
                            src='<?= $this->request->webroot ?>coreui/images/profils.jpg'
                            width='48' height='48'>
                          <div class='pl-3'>
                            <p class='mb-0'>এরশাদুল হামিদ পাভেল</p>
                            <small>কমিউনিকেশন এসোসিয়েট, কমিউনিকেশন ও পার্টনারশীপ</small>
                          </div>
                        </div>

                        <div class='list_button'>
                          <button class='btn btn-outline-success btn_main'>
                            মূল
                          </button>
                          <button class='btn btn-outline-info btn_copy'>
                            অনুলীপি
                          </button>
                          <span class='badge badge-success fs-14 py-1 px-2 label_main'>মূল প্রাপক</span>
                          <span
                            class='badge badge-info text-white fs-14 py-1 px-2 label_copy'>অনুলীপি প্রাপক</span>
                        </div>
                        <div class='custom-control custom-checkbox' hidden>
                          <input type='checkbox' class='custom-control-input'>
                          <label class='custom-control-label'></label>
                        </div>
                      </li>
                      <li class='list-group-item d-flex align-items-center justify-content-between'>
                        <div class='list_content d-flex align-items-center'>
                          <img
                            class='rounded-circle border'
                            src='<?= $this->request->webroot ?>coreui/images/kabir.jpg'
                            width='48' height='48'>
                          <div class='pl-3'>
                            <p class='mb-0'>কবির বিন আনোয়ার</p>
                            <small>প্রকল্প পরিচালক, প্রকল্প পরিচালকের দপ্তর</small>
                          </div>
                        </div>

                        <div class='list_button'>
                          <button class='btn btn-outline-success btn_main'>
                            মূল
                          </button>
                          <button class='btn btn-outline-info btn_copy'>
                            অনুলীপি
                          </button>
                          <span class='badge badge-success fs-14 py-1 px-2 label_main'>মূল প্রাপক</span>
                          <span
                            class='badge badge-info text-white fs-14 py-1 px-2 label_copy'>অনুলীপি প্রাপক</span>
                        </div>
                        <div class='custom-control custom-checkbox' hidden>
                          <input
                            type='checkbox' class='custom-control-input'>
                          <label class='custom-control-label'></label>
                        </div>
                      </li>
                      <li class='list-group-item d-flex align-items-center justify-content-between'>
                        <div class='list_content d-flex align-items-center'>
                          <img
                            class='rounded-circle border'
                            src='<?= $this->request->webroot ?>coreui/images/mukta.jpg'
                            width='48' height='48'>
                          <div class='pl-3'>
                            <p class='mb-0'>নাইমুজ্জামান মুক্তা</p>
                            <small>লোকাল ডেভেলপমেন্ট স্পেশালিস্ট, কমিউনিকেশন ও পার্টনারশীপ</small>
                          </div>
                        </div>

                        <div class='list_button'>
                          <button class='btn btn-outline-success btn_main'>
                            মূল
                          </button>
                          <button class='btn btn-outline-info btn_copy'>
                            অনুলীপি
                          </button>
                          <span class='badge badge-success fs-14 py-1 px-2 label_main'>মূল প্রাপক</span>
                          <span
                            class='badge badge-info text-white fs-14 py-1 px-2 label_copy'>অনুলীপি প্রাপক</span>
                        </div>
                        <div class='custom-control custom-checkbox' hidden>
                          <input
                            type='checkbox' class='custom-control-input'>
                          <label class='custom-control-label'></label>
                        </div>
                      </li>
                      <li class='list-group-item d-flex align-items-center justify-content-between'>
                        <div class='list_content d-flex align-items-center'>
                          <img
                            class='rounded-circle border'
                            src='<?= $this->request->webroot ?>coreui/images/profils.jpg'
                            width='48' height='48'>
                          <div class='pl-3'>
                            <p class='mb-0'>জালাল উদ্দিন আহমেদ </p>
                            <small>এডমিন স্পেশালিস্ট, এডমিন</small>
                          </div>
                        </div>

                        <div class='list_button'>
                          <button class='btn btn-outline-success btn_main'>
                            মূল
                          </button>
                          <button class='btn btn-outline-info btn_copy'>
                            অনুলীপি
                          </button>
                          <span class='badge badge-success fs-14 py-1 px-2 label_main'>মূল প্রাপক</span>
                          <span
                            class='badge badge-info text-white fs-14 py-1 px-2 label_copy'>অনুলীপি প্রাপক</span>
                        </div>
                        <div class='custom-control custom-checkbox' hidden>
                          <input
                            type='checkbox' class='custom-control-input'>
                          <label class='custom-control-label'></label>
                        </div>
                      </li>
                      <li class='list-group-item d-flex align-items-center justify-content-between'>
                        <div class='list_content d-flex align-items-center'>
                          <img
                            class='rounded-circle border'
                            src='<?= $this->request->webroot ?>coreui/images/profils.jpg'
                            width='48' height='48'>
                          <div class='pl-3'>
                            <p class='mb-0'>মুহাম্মদ ইউনুস আলী সরদার</p>
                            <small>টেকনোলজি এক্সপার্ট, ই-সার্ভিস</small>
                          </div>
                        </div>

                        <div class='list_button'>
                          <button class='btn btn-outline-success btn_main'>
                            মূল
                          </button>
                          <button class='btn btn-outline-info btn_copy'>
                            অনুলীপি
                          </button>
                          <span class='badge badge-success fs-14 py-1 px-2 label_main'>মূল প্রাপক</span>
                          <span
                            class='badge badge-info text-white fs-14 py-1 px-2 label_copy'>অনুলীপি প্রাপক</span>
                        </div>
                        <div class='custom-control custom-checkbox' hidden>
                          <input
                            type='checkbox' class='custom-control-input'>
                          <label class='custom-control-label'></label>
                        </div>
                      </li>
                      <li class='list-group-item d-flex align-items-center justify-content-between'>
                        <div class='list_content d-flex align-items-center'>
                          <img
                            class='rounded-circle border'
                            src='<?= $this->request->webroot ?>coreui/images/profils.jpg'
                            width='48' height='48'>
                          <div class='pl-3'>
                            <p class='mb-0'>এরশাদুল হামিদ পাভেল</p>
                            <small>কমিউনিকেশন এসোসিয়েট, কমিউনিকেশন ও পার্টনারশীপ</small>
                          </div>
                        </div>

                        <div class='list_button'>
                          <button class='btn btn-outline-success btn_main'>
                            মূল
                          </button>
                          <button class='btn btn-outline-info btn_copy'>
                            অনুলীপি
                          </button>
                          <span class='badge badge-success fs-14 py-1 px-2 label_main'>মূল প্রাপক</span>
                          <span
                            class='badge badge-info text-white fs-14 py-1 px-2 label_copy'>অনুলীপি প্রাপক</span>
                        </div>
                        <div class='custom-control custom-checkbox' hidden>
                          <input type='checkbox' class='custom-control-input'>
                          <label class='custom-control-label'></label>
                        </div>
                      </li>
                      <li class='list-group-item d-flex align-items-center justify-content-between'>
                        <div class='list_content d-flex align-items-center'>
                          <img
                            class='rounded-circle border'
                            src='<?= $this->request->webroot ?>coreui/images/kabir.jpg'
                            width='48' height='48'>
                          <div class='pl-3'>
                            <p class='mb-0'>কবির বিন আনোয়ার</p>
                            <small>প্রকল্প পরিচালক, প্রকল্প পরিচালকের দপ্তর</small>
                          </div>
                        </div>

                        <div class='list_button'>
                          <button class='btn btn-outline-success btn_main'>
                            মূল
                          </button>
                          <button class='btn btn-outline-info btn_copy'>
                            অনুলীপি
                          </button>
                          <span class='badge badge-success fs-14 py-1 px-2 label_main'>মূল প্রাপক</span>
                          <span
                            class='badge badge-info text-white fs-14 py-1 px-2 label_copy'>অনুলীপি প্রাপক</span>
                        </div>
                        <div class='custom-control custom-checkbox' hidden>
                          <input
                            type='checkbox' class='custom-control-input'>
                          <label class='custom-control-label'></label>
                        </div>
                      </li>
                      <li class='list-group-item d-flex align-items-center justify-content-between'>
                        <div class='list_content d-flex align-items-center'>
                          <img
                            class='rounded-circle border'
                            src='<?= $this->request->webroot ?>coreui/images/mukta.jpg'
                            width='48' height='48'>
                          <div class='pl-3'>
                            <p class='mb-0'>নাইমুজ্জামান মুক্তা</p>
                            <small>লোকাল ডেভেলপমেন্ট স্পেশালিস্ট, কমিউনিকেশন ও
                              পার্টনারশীপ
                            </small>
                          </div>
                        </div>

                        <div class='list_button'>
                          <button class='btn btn-outline-success btn_main'>
                            মূল
                          </button>
                          <button class='btn btn-outline-info btn_copy'>
                            অনুলীপি
                          </button>
                          <span class='badge badge-success fs-14 py-1 px-2 label_main'>মূল প্রাপক</span>
                          <span
                            class='badge badge-info text-white fs-14 py-1 px-2 label_copy'>অনুলীপি প্রাপক</span>
                        </div>
                        <div class='custom-control custom-checkbox' hidden>
                          <input
                            type='checkbox' class='custom-control-input'>
                          <label class='custom-control-label'></label>
                        </div>
                      </li>
                      <li class='list-group-item d-flex align-items-center justify-content-between'>
                        <div class='list_content d-flex align-items-center'>
                          <img
                            class='rounded-circle border'
                            src='<?= $this->request->webroot ?>coreui/images/profils.jpg'
                            width='48' height='48'>
                          <div class='pl-3'>
                            <p class='mb-0'>জালাল উদ্দিন আহমেদ </p>
                            <small>এডমিন স্পেশালিস্ট, এডমিন</small>
                          </div>
                        </div>

                        <div class='list_button'>
                          <button class='btn btn-outline-success btn_main'>
                            মূল
                          </button>
                          <button class='btn btn-outline-info btn_copy'>
                            অনুলীপি
                          </button>
                          <span class='badge badge-success fs-14 py-1 px-2 label_main'>মূল প্রাপক</span>
                          <span
                            class='badge badge-info text-white fs-14 py-1 px-2 label_copy'>অনুলীপি প্রাপক</span>
                        </div>
                        <div class='custom-control custom-checkbox' hidden>
                          <input
                            type='checkbox' class='custom-control-input'>
                          <label class='custom-control-label'></label>
                        </div>
                      </li>
                      <li class='list-group-item d-flex align-items-center justify-content-between'>
                        <div class='list_content d-flex align-items-center'>
                          <img
                            class='rounded-circle border'
                            src='<?= $this->request->webroot ?>coreui/images/profils.jpg'
                            width='48' height='48'>
                          <div class='pl-3'>
                            <p class='mb-0'>মুহাম্মদ ইউনুস আলী সরদার</p>
                            <small>টেকনোলজি এক্সপার্ট, ই-সার্ভিস</small>
                          </div>
                        </div>

                        <div class='list_button'>
                          <button class='btn btn-outline-success btn_main'>
                            মূল
                          </button>
                          <button class='btn btn-outline-info btn_copy'>
                            অনুলীপি
                          </button>
                          <span class='badge badge-success fs-14 py-1 px-2 label_main'>মূল প্রাপক</span>
                          <span
                            class='badge badge-info text-white fs-14 py-1 px-2 label_copy'>অনুলীপি প্রাপক</span>
                        </div>
                        <div class='custom-control custom-checkbox' hidden>
                          <input
                            type='checkbox' class='custom-control-input'>
                          <label class='custom-control-label'></label>
                        </div>
                      </li>
                      <li class='list-group-item d-flex align-items-center justify-content-between'>
                        <div class='list_content d-flex align-items-center'>
                          <img
                            class='rounded-circle border'
                            src='<?= $this->request->webroot ?>coreui/images/profils.jpg'
                            width='48' height='48'>
                          <div class='pl-3'>
                            <p class='mb-0'>এরশাদুল হামিদ পাভেল</p>
                            <small>কমিউনিকেশন এসোসিয়েট, কমিউনিকেশন ও পার্টনারশীপ</small>
                          </div>
                        </div>

                        <div class='list_button'>
                          <button class='btn btn-outline-success btn_main'>
                            মূল
                          </button>
                          <button class='btn btn-outline-info btn_copy'>
                            অনুলীপি
                          </button>
                          <span class='badge badge-success fs-14 py-1 px-2 label_main'>মূল প্রাপক</span>
                          <span
                            class='badge badge-info text-white fs-14 py-1 px-2 label_copy'>অনুলীপি প্রাপক</span>
                        </div>
                        <div class='custom-control custom-checkbox' hidden>
                          <input type='checkbox' class='custom-control-input'>
                          <label class='custom-control-label'></label>
                        </div>
                      </li>
                      <li class='list-group-item d-flex align-items-center justify-content-between'>
                        <div class='list_content d-flex align-items-center'>
                          <img
                            class='rounded-circle border'
                            src='<?= $this->request->webroot ?>coreui/images/kabir.jpg'
                            width='48' height='48'>
                          <div class='pl-3'>
                            <p class='mb-0'>কবির বিন আনোয়ার</p>
                            <small>প্রকল্প পরিচালক, প্রকল্প পরিচালকের দপ্তর</small>
                          </div>
                        </div>

                        <div class='list_button'>
                          <button class='btn btn-outline-success btn_main'>
                            মূল
                          </button>
                          <button class='btn btn-outline-info btn_copy'>
                            অনুলীপি
                          </button>
                          <span class='badge badge-success fs-14 py-1 px-2 label_main'>মূল প্রাপক</span>
                          <span
                            class='badge badge-info text-white fs-14 py-1 px-2 label_copy'>অনুলীপি প্রাপক</span>
                        </div>
                        <div class='custom-control custom-checkbox' hidden>
                          <input
                            type='checkbox' class='custom-control-input'>
                          <label class='custom-control-label'></label>
                        </div>
                      </li>
                      <li class='list-group-item d-flex align-items-center justify-content-between'>
                        <div class='list_content d-flex align-items-center'>
                          <img
                            class='rounded-circle border'
                            src='<?= $this->request->webroot ?>coreui/images/mukta.jpg'
                            width='48' height='48'>
                          <div class='pl-3'>
                            <p class='mb-0'>নাইমুজ্জামান মুক্তা</p>
                            <small>লোকাল ডেভেলপমেন্ট স্পেশালিস্ট, কমিউনিকেশন ও
                              পার্টনারশীপ
                            </small>
                          </div>
                        </div>

                        <div class='list_button'>
                          <button class='btn btn-outline-success btn_main'>
                            মূল
                          </button>
                          <button class='btn btn-outline-info btn_copy'>
                            অনুলীপি
                          </button>
                          <span class='badge badge-success fs-14 py-1 px-2 label_main'>মূল প্রাপক</span>
                          <span
                            class='badge badge-info text-white fs-14 py-1 px-2 label_copy'>অনুলীপি প্রাপক</span>
                        </div>
                        <div class='custom-control custom-checkbox' hidden>
                          <input
                            type='checkbox' class='custom-control-input'>
                          <label class='custom-control-label'></label>
                        </div>
                      </li>
                      <li class='list-group-item d-flex align-items-center justify-content-between'>
                        <div class='list_content d-flex align-items-center'>
                          <img
                            class='rounded-circle border'
                            src='<?= $this->request->webroot ?>coreui/images/profils.jpg'
                            width='48' height='48'>
                          <div class='pl-3'>
                            <p class='mb-0'>জালাল উদ্দিন আহমেদ </p>
                            <small>এডমিন স্পেশালিস্ট, এডমিন</small>
                          </div>
                        </div>

                        <div class='list_button'>
                          <button class='btn btn-outline-success btn_main'>
                            মূল
                          </button>
                          <button class='btn btn-outline-info btn_copy'>
                            অনুলীপি
                          </button>
                          <span class='badge badge-success fs-14 py-1 px-2 label_main'>মূল প্রাপক</span>
                          <span
                            class='badge badge-info text-white fs-14 py-1 px-2 label_copy'>অনুলীপি প্রাপক</span>
                        </div>
                        <div class='custom-control custom-checkbox' hidden>
                          <input
                            type='checkbox' class='custom-control-input'>
                          <label class='custom-control-label'></label>
                        </div>
                      </li>
                      <li class='list-group-item d-flex align-items-center justify-content-between'>
                        <div class='list_content d-flex align-items-center'>
                          <img
                            class='rounded-circle border'
                            src='<?= $this->request->webroot ?>coreui/images/profils.jpg'
                            width='48' height='48'>
                          <div class='pl-3'>
                            <p class='mb-0'>মুহাম্মদ ইউনুস আলী সরদার</p>
                            <small>টেকনোলজি এক্সপার্ট, ই-সার্ভিস</small>
                          </div>
                        </div>

                        <div class='list_button'>
                          <button class='btn btn-outline-success btn_main'>
                            মূল
                          </button>
                          <button class='btn btn-outline-info btn_copy'>
                            অনুলীপি
                          </button>
                          <span class='badge badge-success fs-14 py-1 px-2 label_main'>মূল প্রাপক</span>
                          <span
                            class='badge badge-info text-white fs-14 py-1 px-2 label_copy'>অনুলীপি প্রাপক</span>
                        </div>
                        <div class='custom-control custom-checkbox' hidden>
                          <input
                            type='checkbox' class='custom-control-input'>
                          <label class='custom-control-label'></label>
                        </div>
                      </li>
                      <li class='list-group-item d-flex align-items-center justify-content-between'>
                        <div class='list_content d-flex align-items-center'>
                          <img
                            class='rounded-circle border'
                            src='<?= $this->request->webroot ?>coreui/images/profils.jpg'
                            width='48' height='48'>
                          <div class='pl-3'>
                            <p class='mb-0'>এরশাদুল হামিদ পাভেল</p>
                            <small>কমিউনিকেশন এসোসিয়েট, কমিউনিকেশন ও পার্টনারশীপ</small>
                          </div>
                        </div>

                        <div class='list_button'>
                          <button class='btn btn-outline-success btn_main'>
                            মূল
                          </button>
                          <button class='btn btn-outline-info btn_copy'>
                            অনুলীপি
                          </button>
                          <span class='badge badge-success fs-14 py-1 px-2 label_main'>মূল প্রাপক</span>
                          <span
                            class='badge badge-info text-white fs-14 py-1 px-2 label_copy'>অনুলীপি প্রাপক</span>
                        </div>
                        <div class='custom-control custom-checkbox' hidden>
                          <input type='checkbox' class='custom-control-input'>
                          <label class='custom-control-label'></label>
                        </div>
                      </li>
                      <li class='list-group-item d-flex align-items-center justify-content-between'>
                        <div class='list_content d-flex align-items-center'>
                          <img
                            class='rounded-circle border'
                            src='<?= $this->request->webroot ?>coreui/images/kabir.jpg'
                            width='48' height='48'>
                          <div class='pl-3'>
                            <p class='mb-0'>কবির বিন আনোয়ার</p>
                            <small>প্রকল্প পরিচালক, প্রকল্প পরিচালকের দপ্তর</small>
                          </div>
                        </div>

                        <div class='list_button'>
                          <button class='btn btn-outline-success btn_main'>
                            মূল
                          </button>
                          <button class='btn btn-outline-info btn_copy'>
                            অনুলীপি
                          </button>
                          <span class='badge badge-success fs-14 py-1 px-2 label_main'>মূল প্রাপক</span>
                          <span
                            class='badge badge-info text-white fs-14 py-1 px-2 label_copy'>অনুলীপি প্রাপক</span>
                        </div>
                        <div class='custom-control custom-checkbox' hidden>
                          <input
                            type='checkbox' class='custom-control-input'>
                          <label class='custom-control-label'></label>
                        </div>
                      </li>
                      <li class='list-group-item d-flex align-items-center justify-content-between'>
                        <div class='list_content d-flex align-items-center'>
                          <img
                            class='rounded-circle border'
                            src='<?= $this->request->webroot ?>coreui/images/mukta.jpg'
                            width='48' height='48'>
                          <div class='pl-3'>
                            <p class='mb-0'>নাইমুজ্জামান মুক্তা</p>
                            <small>লোকাল ডেভেলপমেন্ট স্পেশালিস্ট, কমিউনিকেশন ও
                              পার্টনারশীপ
                            </small>
                          </div>
                        </div>

                        <div class='list_button'>
                          <button class='btn btn-outline-success btn_main'>
                            মূল
                          </button>
                          <button class='btn btn-outline-info btn_copy'>
                            অনুলীপি
                          </button>
                          <span class='badge badge-success fs-14 py-1 px-2 label_main'>মূল প্রাপক</span>
                          <span
                            class='badge badge-info text-white fs-14 py-1 px-2 label_copy'>অনুলীপি প্রাপক</span>
                        </div>
                        <div class='custom-control custom-checkbox' hidden>
                          <input
                            type='checkbox' class='custom-control-input'>
                          <label class='custom-control-label'></label>
                        </div>
                      </li>
                      <li class='list-group-item d-flex align-items-center justify-content-between'>
                        <div class='list_content d-flex align-items-center'>
                          <img
                            class='rounded-circle border'
                            src='<?= $this->request->webroot ?>coreui/images/profils.jpg'
                            width='48' height='48'>
                          <div class='pl-3'>
                            <p class='mb-0'>জালাল উদ্দিন আহমেদ </p>
                            <small>এডমিন স্পেশালিস্ট, এডমিন</small>
                          </div>
                        </div>

                        <div class='list_button'>
                          <button class='btn btn-outline-success btn_main'>
                            মূল
                          </button>
                          <button class='btn btn-outline-info btn_copy'>
                            অনুলীপি
                          </button>
                          <span class='badge badge-success fs-14 py-1 px-2 label_main'>মূল প্রাপক</span>
                          <span
                            class='badge badge-info text-white fs-14 py-1 px-2 label_copy'>অনুলীপি প্রাপক</span>
                        </div>
                        <div class='custom-control custom-checkbox' hidden>
                          <input
                            type='checkbox' class='custom-control-input'>
                          <label class='custom-control-label'></label>
                        </div>
                      </li>
                      <li class='list-group-item d-flex align-items-center justify-content-between'>
                        <div class='list_content d-flex align-items-center'>
                          <img
                            class='rounded-circle border'
                            src='<?= $this->request->webroot ?>coreui/images/profils.jpg'
                            width='48' height='48'>
                          <div class='pl-3'>
                            <p class='mb-0'>মুহাম্মদ ইউনুস আলী সরদার</p>
                            <small>টেকনোলজি এক্সপার্ট, ই-সার্ভিস</small>
                          </div>
                        </div>

                        <div class='list_button'>
                          <button class='btn btn-outline-success btn_main'>
                            মূল
                          </button>
                          <button class='btn btn-outline-info btn_copy'>
                            অনুলীপি
                          </button>
                          <span class='badge badge-success fs-14 py-1 px-2 label_main'>মূল প্রাপক</span>
                          <span
                            class='badge badge-info text-white fs-14 py-1 px-2 label_copy'>অনুলীপি প্রাপক</span>
                        </div>
                        <div class='custom-control custom-checkbox' hidden>
                          <input
                            type='checkbox' class='custom-control-input'>
                          <label class='custom-control-label'></label>
                        </div>
                      </li>
                      <li class='list-group-item d-flex align-items-center justify-content-between'>
                        <div class='list_content d-flex align-items-center'>
                          <img
                            class='rounded-circle border'
                            src='<?= $this->request->webroot ?>coreui/images/profils.jpg'
                            width='48' height='48'>
                          <div class='pl-3'>
                            <p class='mb-0'>এরশাদুল হামিদ পাভেল</p>
                            <small>কমিউনিকেশন এসোসিয়েট, কমিউনিকেশন ও পার্টনারশীপ</small>
                          </div>
                        </div>

                        <div class='list_button'>
                          <button class='btn btn-outline-success btn_main'>
                            মূল
                          </button>
                          <button class='btn btn-outline-info btn_copy'>
                            অনুলীপি
                          </button>
                          <span class='badge badge-success fs-14 py-1 px-2 label_main'>মূল প্রাপক</span>
                          <span
                            class='badge badge-info text-white fs-14 py-1 px-2 label_copy'>অনুলীপি প্রাপক</span>
                        </div>
                        <div class='custom-control custom-checkbox' hidden>
                          <input type='checkbox' class='custom-control-input'>
                          <label class='custom-control-label'></label>
                        </div>
                      </li>
                      <li class='list-group-item d-flex align-items-center justify-content-between'>
                        <div class='list_content d-flex align-items-center'>
                          <img
                            class='rounded-circle border'
                            src='<?= $this->request->webroot ?>coreui/images/kabir.jpg'
                            width='48' height='48'>
                          <div class='pl-3'>
                            <p class='mb-0'>কবির বিন আনোয়ার</p>
                            <small>প্রকল্প পরিচালক, প্রকল্প পরিচালকের দপ্তর</small>
                          </div>
                        </div>

                        <div class='list_button'>
                          <button class='btn btn-outline-success btn_main'>
                            মূল
                          </button>
                          <button class='btn btn-outline-info btn_copy'>
                            অনুলীপি
                          </button>
                          <span class='badge badge-success fs-14 py-1 px-2 label_main'>মূল প্রাপক</span>
                          <span
                            class='badge badge-info text-white fs-14 py-1 px-2 label_copy'>অনুলীপি প্রাপক</span>
                        </div>
                        <div class='custom-control custom-checkbox' hidden>
                          <input
                            type='checkbox' class='custom-control-input'>
                          <label class='custom-control-label'></label>
                        </div>
                      </li>
                      <li class='list-group-item d-flex align-items-center justify-content-between'>
                        <div class='list_content d-flex align-items-center'>
                          <img
                            class='rounded-circle border'
                            src='<?= $this->request->webroot ?>coreui/images/mukta.jpg'
                            width='48' height='48'>
                          <div class='pl-3'>
                            <p class='mb-0'>নাইমুজ্জামান মুক্তা</p>
                            <small>লোকাল ডেভেলপমেন্ট স্পেশালিস্ট, কমিউনিকেশন ও
                              পার্টনারশীপ
                            </small>
                          </div>
                        </div>

                        <div class='list_button'>
                          <button class='btn btn-outline-success btn_main'>
                            মূল
                          </button>
                          <button class='btn btn-outline-info btn_copy'>
                            অনুলীপি
                          </button>
                          <span class='badge badge-success fs-14 py-1 px-2 label_main'>মূল প্রাপক</span>
                          <span
                            class='badge badge-info text-white fs-14 py-1 px-2 label_copy'>অনুলীপি প্রাপক</span>
                        </div>
                        <div class='custom-control custom-checkbox' hidden>
                          <input
                            type='checkbox' class='custom-control-input'>
                          <label class='custom-control-label'></label>
                        </div>
                      </li>
                      <li class='list-group-item d-flex align-items-center justify-content-between'>
                        <div class='list_content d-flex align-items-center'>
                          <img
                            class='rounded-circle border'
                            src='<?= $this->request->webroot ?>coreui/images/profils.jpg'
                            width='48' height='48'>
                          <div class='pl-3'>
                            <p class='mb-0'>জালাল উদ্দিন আহমেদ </p>
                            <small>এডমিন স্পেশালিস্ট, এডমিন</small>
                          </div>
                        </div>

                        <div class='list_button'>
                          <button class='btn btn-outline-success btn_main'>
                            মূল
                          </button>
                          <button class='btn btn-outline-info btn_copy'>
                            অনুলীপি
                          </button>
                          <span class='badge badge-success fs-14 py-1 px-2 label_main'>মূল প্রাপক</span>
                          <span
                            class='badge badge-info text-white fs-14 py-1 px-2 label_copy'>অনুলীপি প্রাপক</span>
                        </div>
                        <div class='custom-control custom-checkbox' hidden>
                          <input
                            type='checkbox' class='custom-control-input'>
                          <label class='custom-control-label'></label>
                        </div>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div class="daptorik_decision col-lg-4 pt-2">

              <h5 class='mb-3'>অগ্রাধিকার</h5>

              <div class="custom-control custom-radio check-default">
                <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input">
                <label class="custom-control-label" for="customRadio1"> অগ্রাধিকার বাছাই করুন</label>
              </div>
              <div class="custom-control custom-radio check-default">
                <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input">
                <label class="custom-control-label" for="customRadio2"> জরুরি</label>
              </div>
              <div class="custom-control custom-radio check-default">
                <input type="radio" id="customRadio3" name="customRadio" class="custom-control-input">
                <label class="custom-control-label" for="customRadio3"> অবিলম্বে</label>
              </div>
              <div class="custom-control custom-radio check-default">
                <input type="radio" id="customRadio4" name="customRadio" class="custom-control-input">
                <label class="custom-control-label" for="customRadio4"> সর্বোচ্চ অগ্রাধিকার</label>
              </div>
              <div class="custom-control custom-radio check-default">
                <input type="radio" id="customRadio5" name="customRadio" class="custom-control-input">
                <label class="custom-control-label" for="customRadio5"> তাগিদপত্র</label>
              </div>
              <div class="custom-control custom-radio check-default">
                <input type="radio" id="customRadio6" name="customRadio" class="custom-control-input">
                <label class="custom-control-label" for="customRadio6"> দৃষ্টি আকর্ষণ</label>
              </div>
              <br>
              <div class="d-flex mb-3 justify-content-between">
                <h5>সিদ্ধান্ত</h5>
              </div>

              <div class="pScroll mh-120 decision_list">
                <div class="custom-control custom-radio check-default">
                  <input type="radio" id="decision_1" name="customRadio" class="custom-control-input">
                  <label class="custom-control-label" for="decision_1"> নথিতে উপস্থাপন করুন</label>
                </div>

                <div class="custom-control custom-radio check-default">
                  <input type="radio" id="decision_5" name="customRadio" class="custom-control-input">
                  <label class="custom-control-label" for="decision_5"> অদ্যই পেশ করুন</label>
                </div>
                <div class="custom-control custom-radio check-default">
                  <input type="radio" id="decision_1" name="customRadio" class="custom-control-input">
                  <label class="custom-control-label" for="decision_1"> নথিতে উপস্থাপন করুন</label>
                </div>
                <div class="custom-control custom-radio check-default">
                  <input type="radio" id="decision_2" name="customRadio" class="custom-control-input">
                  <label class="custom-control-label" for="decision_2"> নথিজাত করুন</label>
                </div>
                <div class="custom-control custom-radio check-default">
                  <input type="radio" id="decision_3" name="customRadio" class="custom-control-input">
                  <label class="custom-control-label" for="decision_3"> ডিফল্ট সিদ্ধান্তসমূহ</label>
                </div>

                <div class="custom-control custom-radio check-default">
                  <input type="radio" id="decision_4" name="customRadio" class="custom-control-input">
                  <label class="custom-control-label" for="decision_4"> তদন্ত পূর্বক প্রতিবেদন দিবেন</label>
                </div>
                <div class="custom-control custom-radio check-default">
                  <input type="radio" id="decision_2" name="customRadio" class="custom-control-input">
                  <label class="custom-control-label" for="decision_2"> নথিজাত করুন</label>
                </div>
                <div class="custom-control custom-radio check-default">
                  <input type="radio" id="decision_3" name="customRadio" class="custom-control-input">
                  <label class="custom-control-label" for="decision_3"> ডিফল্ট সিদ্ধান্তসমূহ</label>
                </div>
                <div class="custom-control custom-radio check-default">
                  <input type="radio" id="decision_4" name="customRadio" class="custom-control-input">
                  <label class="custom-control-label" for="decision_4"> তদন্ত পূর্বক প্রতিবেদন দিবেন</label>
                </div>
                <div class="custom-control custom-radio check-default">
                  <input type="radio" id="decision_5" name="customRadio" class="custom-control-input">
                  <label class="custom-control-label" for="decision_5"> অদ্যই পেশ করুন</label>
                </div>
              </div>
              <div class="custom-control custom-radio check-default">
                <input type="radio" id="decision_6" name="customRadio" class="custom-control-input">
                <label class="custom-control-label" for="decision_6">
                  <input type="text" class="">
                </label>
              </div>


            </div>
          </div>

        </div>
        <form method='post' action='' class="d-flex">
          <div class="daak_btns">
            <button class='btn btn-outline-dark daak_forward' data-toggle='tooltip' title='ফরোয়ার্ড'>
              <i class='fa fa-mail-forward mr-2'></i> ফরোয়ার্ড
            </button>
            <button class='btn btn-outline-dark' data-toggle='tooltip' title='নথিতে উপস্থাপিত'>
              <i class='fa fa-files-o mr-2'></i> নথিতে উপস্থাপিত
            </button>
            <a href="nothi-management.html" class='btn btn-outline-dark' data-toggle="modal"
              data-target=".nothi-jato-modal">
              <i data-toggle='tooltip' title='নথিজাত' class='fa fa-file-archive-o mr-2'></i> নথিজাত
            </a>
          </div>
          <div class="daak_btns hidden">
            <button class='btn btn-outline-dark daak_forward' data-toggle='tooltip' title='ডাক প্রেরণ করুন '>
              <i class='fa fa-mail-forward mr-2'></i> প্রেরণ করুন
            </button>
            <button class='btn btn-outline-dark daak_forward' data-toggle='tooltip' title='ডাক প্রেরণ করুন '>
              <i class='fa fa-close mr-2'></i> বাতিল
            </button>
          </div>
        </form>

      </div>
    </div>
  </div>
</div>
