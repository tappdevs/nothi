<?= __($tab_no.$doc_type) ?>
<?= $this->element('Daak/DaakView/daak_view_content_action_bar', ['movement_no' => $tab_no]); ?>

<?= $this->element('Daak/DaakView/daak_view_content_body', ['doc_type' => $doc_type]); ?>

<?= $this->element('Daak/DaakView/detail_attachment'); ?>

<?= $this->element('Daak/DaakView/daak_view_content_attachment'); ?>

<?= $this->element('Daak/DaakView/daak_view_content_movement', ['movement_no' => $tab_no]); ?>
