<div class='attachments py-4'>
  <h5 class='mb-4'>সংযুক্তি (<span>১০</span>)</h5>
  <div class='d-flex flex-wrap'>
    <div class='attach image'>
      <img src='<?= $this->request->webroot ?>coreui/images/img1.png' alt='img'>
      <div class='attach_download d-flex justify-content-between flex-column'>
        <div class='name_size d-flex align-items-start'>
          <span class='obj_icon'><i class='fa fa-image mr-1'></i></span>
          <div>
            <p>Image name here Image name hereImage name here Image name
              here.png</p>
            <span>54 kb</span>
          </div>
        </div>
        <div class='download_link text-center'>
          <a href='#' class='btn btn-secondary btn-sm'><i
              class='fa fa-download'></i></a>
          <a
            href='<?= $this->request->webroot ?>coreui/images/img1.png' data-fancybox='attachment'
            class='fencybox'></a>
        </div>
      </div>
    </div>
    <div class='attach image'>
      <img src='<?= $this->request->webroot ?>coreui/images/img2.png' alt='img'>
      <div class='attach_download d-flex justify-content-between flex-column'>
        <div class='name_size d-flex align-items-start'>
          <span class='obj_icon'><i class='fa fa-image mr-1'></i></span>
          <div>
            <p>Image name here Image name hereImage name here Image name
              here.png</p>
            <span>54 kb</span>
          </div>
        </div>
        <div class='download_link text-center'>
          <a href='#' class='btn btn-secondary btn-sm'><i
              class='fa fa-download'></i></a>
          <a
            href='<?= $this->request->webroot ?>coreui/images/img2.png' data-fancybox='attachment'
            class='fencybox'></a>
        </div>
      </div>
    </div>
    <div class='attach image'>
      <img src='<?= $this->request->webroot ?>coreui/images/img3.jpg' alt='img'>
      <div class='attach_download d-flex justify-content-between flex-column'>
        <div class='name_size d-flex align-items-start'>
          <span class='obj_icon'><i class='fa fa-image mr-1'></i></span>
          <div>
            <p>Image name here Image name hereImage name here Image name
              here.png</p>
            <span>54 kb</span>
          </div>
        </div>
        <div class='download_link text-center'>
          <a href='#' class='btn btn-secondary btn-sm'><i
              class='fa fa-download'></i></a>
          <a
            href='<?= $this->request->webroot ?>coreui/images/img3.jpg' data-fancybox='attachment'
            class='fencybox'></a>
        </div>
      </div>
    </div>
    <div class='attach d-flex flex-column justify-content-between align-items-stretch'>
      <div class='flex-grow-1 d-flex justify-content-center align-items-center'><i
          class='fa fa-file-pdf-o fa-4x'></i></div>
      <div class='title-caption text-truncate'>
        <i class='fa fa-image mr-1'></i> Image name here.png
      </div>
      <div class='attach_download d-flex justify-content-between flex-column'>
        <div class='name_size d-flex align-items-start'>
          <span class='obj_icon'><i class='fa fa-image mr-1'></i></span>
          <div>
            <p>Image name here Image name hereImage name here Image name
              here.png</p>
            <span>54 kb</span>
          </div>
        </div>
        <div class='download_link text-center'>
          <a href='#' class='btn btn-secondary btn-sm'><i
              class='fa fa-download'></i></a>
          <a
            href='<?= $this->request->webroot ?>coreui/images/img3.jpg' data-fancybox='attachment'
            class='fencybox'></a>
        </div>

      </div>
    </div>
    <div class='attach image'>
      <img src='<?= $this->request->webroot ?>coreui/images/img1.png' alt='img'>
      <div class='attach_download d-flex justify-content-between flex-column'>
        <div class='name_size d-flex align-items-start'>
          <span class='obj_icon'><i class='fa fa-image mr-1'></i></span>
          <div>
            <p>Image name here Image name hereImage name here Image name
              here.png</p>
            <span>54 kb</span>
          </div>
        </div>
        <div class='download_link text-center'>
          <a href='#' class='btn btn-secondary btn-sm'><i
              class='fa fa-download'></i></a>
          <a
            href='<?= $this->request->webroot ?>coreui/images/img1.png' data-fancybox='attachment'
            class='fencybox'></a>
        </div>
      </div>
    </div>
    <div class='attach image'>
      <img src='<?= $this->request->webroot ?>coreui/images/img2.png' alt='img'>
      <div class='attach_download d-flex justify-content-between flex-column'>
        <div class='name_size d-flex align-items-start'>
          <span class='obj_icon'><i class='fa fa-image mr-1'></i></span>
          <div>
            <p>Image name here Image name hereImage name here Image name
              here.png</p>
            <span>54 kb</span>
          </div>
        </div>
        <div class='download_link text-center'>
          <a href='#' class='btn btn-secondary btn-sm'><i
              class='fa fa-download'></i></a>
          <a
            href='<?= $this->request->webroot ?>coreui/images/img2.png' data-fancybox='attachment'
            class='fencybox'></a>
        </div>
      </div>
    </div>
    <div class='attach image'>
      <img src='<?= $this->request->webroot ?>coreui/images/img3.jpg' alt='img'>
      <div class='attach_download d-flex justify-content-between flex-column'>
        <div class='name_size d-flex align-items-start'>
          <span class='obj_icon'><i class='fa fa-image mr-1'></i></span>
          <div>
            <p>Image name here Image name hereImage name here Image name
              here.png</p>
            <span>54 kb</span>
          </div>
        </div>
        <div class='download_link text-center'>
          <a href='#' class='btn btn-secondary btn-sm'><i
              class='fa fa-download'></i></a>
          <a
            href='<?= $this->request->webroot ?>coreui/images/img3.jpg' data-fancybox='attachment'
            class='fencybox'></a>
        </div>
      </div>
    </div>
    <div class='attach d-flex flex-column justify-content-between align-items-stretch'>
      <div class='flex-grow-1 d-flex justify-content-center align-items-center'><i
          class='fa fa-file-pdf-o fa-4x'></i></div>
      <div class='title-caption text-truncate'>
        <i class='fa fa-image mr-1'></i> Image name here.png
      </div>
      <div class='attach_download d-flex justify-content-between flex-column'>
        <div class='name_size d-flex align-items-start'>
          <span class='obj_icon'><i class='fa fa-image mr-1'></i></span>
          <div>
            <p>Image name here Image name hereImage name here Image name
              here.png</p>
            <span>54 kb</span>
          </div>
        </div>
        <div class='download_link text-center'>
          <a href='#' class='btn btn-secondary btn-sm'><i
              class='fa fa-download'></i></a>
          <a
            href='<?= $this->request->webroot ?>coreui/images/img3.jpg' data-fancybox='attachment'
            class='fencybox'></a>
        </div>

      </div>
    </div>
    <div class='attach image'>
      <img src='<?= $this->request->webroot ?>coreui/images/img1.png' alt='img'>
      <div class='attach_download d-flex justify-content-between flex-column'>
        <div class='name_size d-flex align-items-start'>
          <span class='obj_icon'><i class='fa fa-image mr-1'></i></span>
          <div>
            <p>Image name here Image name hereImage name here Image name
              here.png</p>
            <span>54 kb</span>
          </div>
        </div>
        <div class='download_link text-center'>
          <a href='#' class='btn btn-secondary btn-sm'><i
              class='fa fa-download'></i></a>
          <a
            href='<?= $this->request->webroot ?>coreui/images/img1.png' data-fancybox='attachment'
            class='fencybox'></a>
        </div>
      </div>
    </div>
    <div class='attach image'>
      <img src='<?= $this->request->webroot ?>coreui/images/img2.png' alt='img'>
      <div class='attach_download d-flex justify-content-between flex-column'>
        <div class='name_size d-flex align-items-start'>
          <span class='obj_icon'><i class='fa fa-image mr-1'></i></span>
          <div>
            <p>Image name here Image name hereImage name here Image name
              here.png</p>
            <span>54 kb</span>
          </div>
        </div>
        <div class='download_link text-center'>
          <a href='#' class='btn btn-secondary btn-sm'><i
              class='fa fa-download'></i></a>
          <a
            href='<?= $this->request->webroot ?>coreui/images/img2.png' data-fancybox='attachment'
            class='fencybox'></a>
        </div>
      </div>
    </div>
    <div class='attach image'>
      <img src='<?= $this->request->webroot ?>coreui/images/img3.jpg' alt='img'>
      <div class='attach_download d-flex justify-content-between flex-column'>
        <div class='name_size d-flex align-items-start'>
          <span class='obj_icon'><i class='fa fa-image mr-1'></i></span>
          <div>
            <p>Image name here Image name hereImage name here Image name
              here.png</p>
            <span>54 kb</span>
          </div>
        </div>
        <div class='download_link text-center'>
          <a href='#' class='btn btn-secondary btn-sm'><i
              class='fa fa-download'></i></a>
          <a
            href='<?= $this->request->webroot ?>coreui/images/img3.jpg' data-fancybox='attachment'
            class='fencybox'></a>
        </div>
      </div>
    </div>
    <div class='attach d-flex flex-column justify-content-between align-items-stretch'>
      <div class='flex-grow-1 d-flex justify-content-center align-items-center'><i
          class='fa fa-file-pdf-o fa-4x'></i></div>
      <div class='title-caption text-truncate'>
        <i class='fa fa-image mr-1'></i> Image name here.png
      </div>
      <div class='attach_download d-flex justify-content-between flex-column'>
        <div class='name_size d-flex align-items-start'>
          <span class='obj_icon'><i class='fa fa-image mr-1'></i></span>
          <div>
            <p>Image name here Image name hereImage name here Image name
              here.png</p>
            <span>54 kb</span>
          </div>
        </div>
        <div class='download_link text-center'>
          <a href='#' class='btn btn-secondary btn-sm'><i
              class='fa fa-download'></i></a>
          <a
            href='<?= $this->request->webroot ?>coreui/images/img3.jpg' data-fancybox='attachment'
            class='fencybox'></a>
        </div>

      </div>
    </div>
  </div>
</div>
