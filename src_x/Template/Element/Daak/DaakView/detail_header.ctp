<div class='header justify-content-between mt-0'>
  <div class='d-flex'>
    <img class='avatar rounded-circle border' src='<?= $this->request->webroot ?>coreui/images/200000000010.png'>
    <div class='from'>
      <div class='fs-16 font-weight-normal'>কোহিনূর বেগম
        <small>(অফিস সহকারী)</small>
      </div>
      <div class='daak_sender d-flex align-items-center'>
        ব্যবসা-বাণিজ্য শাখা জেলা প্রশাসকের কার্যালয়, জামালপুর <span
          class='daak_sender_btn ml-2'><i class='fa fa-caret-down'></i></span>
        <div class='daak_sender_info'>
          <table cellpadding='0' class='table table-borderless table-sm mb-0 tbl-left vline-top'>
            <tbody>
            <tr>
              <td class='text-right' width='50'>
                <div class=' font-weight-bold'>মূল প্রাপক</div>
              </td>
              <td>
                <p class='mb-0'>মোঃ হাসানুজ্জামান</p>
                <small>সিনিয়র সফটওয়্যার ইঞ্জিনিয়ার (ভারপ্রাপ্ত) ই-সার্ভিস,
                  এ্যাকসেস টু ইনফরমেশন (এটু্আই) প্রোগ্রাম
                </small>
              </td>
            </tr>
            <tr>
              <td class='text-right' width='50'>
                <div class=' font-weight-bold'>অনুলিপি প্রাপক</div>
              </td>
              <td>
                <ul class='list-inline mb-0 list-2'>
                  <li class='pb-2'>
                    <p class='mb-0'>মোঃ হাসানুজ্জামান</p>
                    <small>সিনিয়র সফটওয়্যার ইঞ্জিনিয়ার (ভারপ্রাপ্ত)
                      ই-সার্ভিস, এ্যাকসেস টু ইনফরমেশন (এটু্আই)
                      প্রোগ্রাম
                    </small>
                  </li>
                  <li class='pb-2'>
                    <p class='mb-0'>মোঃ হাসানুজ্জামান</p>
                    <small>সিনিয়র সফটওয়্যার ইঞ্জিনিয়ার (ভারপ্রাপ্ত)
                      ই-সার্ভিস, এ্যাকসেস টু ইনফরমেশন (এটু্আই)
                      প্রোগ্রাম
                    </small>
                  </li>
                  <li>
                    <p class='mb-0'>মোঃ হাসানুজ্জামান</p>
                    <small>সিনিয়র সফটওয়্যার ইঞ্জিনিয়ার (ভারপ্রাপ্ত)
                      ই-সার্ভিস, এ্যাকসেস টু ইনফরমেশন (এটু্আই)
                      প্রোগ্রাম
                    </small>
                  </li>
                </ul>
              </td>
            </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <div class='text-right d-flex flex-column'>
    <div class='fs-12 d-flex justify-content-end'>
      <div class='mr-2'>
        <svg class='fill-violate align-middle mr-1' xmlns='http://www.w3.org/2000/svg'
          xmlns:xlink='http://www.w3.org/1999/xlink' version='1.1' id='Capa_1' x='0px' y='0px' width='14px'
          viewBox='0 0 484.5 484.5' style='enable-background:new 0 0 484.5 484.5;' xml:space='preserve'><path
            d='M372.3,84.15c-7.649-12.75-22.95-20.4-40.8-20.4H51c-28.05,0-51,22.95-51,51v255c0,28.05,22.95,51,51,51h280.5    c17.85,0,33.15-7.65,40.8-20.4l112.2-158.1L372.3,84.15z'/></svg>
        সর্বোচ্চ অগ্রাধিকার
      </div>
      <div>
        <svg class='fill-yellow align-middle mr-1' xmlns='http://www.w3.org/2000/svg'
          xmlns:xlink='http://www.w3.org/1999/xlink' version='1.1' id='Capa_1' x='0px' y='0px' width='14px'
          viewBox='0 0 484.5 484.5' style='enable-background:new 0 0 484.5 484.5;' xml:space='preserve'><path
            d='M372.3,84.15c-7.649-12.75-22.95-20.4-40.8-20.4H51c-28.05,0-51,22.95-51,51v255c0,28.05,22.95,51,51,51h280.5    c17.85,0,33.15-7.65,40.8-20.4l112.2-158.1L372.3,84.15z'/></svg>
        বিশেষ গোপনীয়
      </div>
    </div>
    <div class='d-flex'>
      <object
        data='<?= $this->request->webroot ?>coreui/images/attachment.svg'
        type='image/svg+xml' height='18'></object>
      <div class='date fs-12 ml-3'>সেপ্টেম্বর ১৮ ২০১৮, সকাল ৭:৩৪ ( ২৩ ঘণ্টা আগে)</div>
    </div>
  </div>

</div>