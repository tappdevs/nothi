<div class='toolbar d-flex justify-content-between border-bottom-0'>
  <div class='toolbar-left d-flex'>
    <button
      onclick="window.location.href = '<?= $this->Url->build(["controller" => "Dashboard", "action" => "daak2"]); ?>'"
      class='btn btn-light' type='button' data-toggle='tooltip' title='ডাক লিস্টে ফেরৎ যান'>
      <i class='fa fa-long-arrow-left mr-1'></i> ডাক লিস্টে ফেরৎ যান
    </button>

    <div class='btn-group'>

      <button class='btn btn-light' type='button' data-toggle='tooltip' title='বিশেষ'>
        <span class='fa fa-star'></span>
      </button>

    </div>
    <button class='btn btn-light' type='button' data-toggle='tooltip' title='রিফ্রেশ'>
      <span class='fa fa-refresh'></span>
    </button>

    <div class='btn-group ml-3'>
        <span
          class='badge badge-pill badge-light font-weight-light p-2 fs-14 mr-1 d-flex align-items-center'>আগত ডাক</span>
      <span
        class='badge badge-pill badge-light font-weight-light p-2 fs-14 d-flex align-items-center'>দাপ্তরিক ডাক</span>
    </div>
  </div>
  <div class='d-flex align-items-center'>
    <span class='mr-2'>১, সর্বমোট: ২৩,৪২৩</span>
    <div class='btn-group'>
      <button class='btn btn-light' type='button'>
        <span class='fa fa-chevron-left'></span>
      </button>
      &nbsp;
      <button class='btn btn-light' type='button'>
        <span class='fa fa-chevron-right'></span>
      </button>
    </div>
  </div>
</div>