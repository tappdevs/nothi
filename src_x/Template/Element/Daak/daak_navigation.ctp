<div class="sidebar">
  <nav class="sidebar-nav">

    <ul class="nav">
      <li class="nav-title"> ডাক</li>
      <li class="nav-item nav-dropdown">
        <a class="nav-link nav-dropdown-toggle" href="#">
          <i class="fs0 efile-email5 nav-icon"></i> ডাক</a>
        <ul class="nav-dropdown-items">
          <li class="nav-item">
            <a class="nav-link" href="<?= $this->Url->build(["controller" => "Dashboard", "action" => "daak2", "daak_upload_daptorik",]);?>">
              <i class="glyphicon glyphicon-home nav-icon" aria-hidden="true"></i> ডাক আপলোড দাপ্তরিক</a>

          </li>

          <li class="nav-item">
            <a class="nav-link" href="<?= $this->Url->build(["controller" => "Dashboard", "action" => "daak2", "daak_upload_nagorik",]);?>">
            <i class="glyphicon glyphicon-user nav-icon" aria-hidden="true"></i> ডাক আপলোড নাগরিক</a>

          </li>

          <li class="nav-item">
            <a class="nav-link" href="<?= $this->Url->build(["controller" => "Dashboard", "action" => "daak2", "daak_khoshra",]);?>">
              <i class="fs0 efile-dak_draft2 nav-icon" aria-hidden="true"></i> খসড়া ডাক</a>

          </li>

        </ul>
      </li>
      <li class="nav-item">
        <a href="<?= $this->Url->build(["controller" => "Dashboard", "action" => "daak2", "daak_tracking_all",]);?>" class="nav-link">
          <i class="fs0 efile-search3 nav-icon" aria-hidden="true"></i>
          <span class="title">সকল ডাক ট্র্যাকিং</span>
        </a>
      </li>
      <li class="nav-item nav-dropdown">
        <a class="nav-link nav-dropdown-toggle" href="#">
          <i class="fs0 a2i_gn_register2 nav-icon"></i> নিবন্ধন বহি</a>
        <ul class="nav-dropdown-items">
          <li class="nav-item">
            <a class="nav-link" href="#">
              <i class="fs0 efile-grohon_register1 nav-icon" aria-hidden="true"></i> ডাক গ্রহণ নিবন্ধন বহি</a>

          </li>

          <li class="nav-item">
            <a class="nav-link" href="#">
              <i class="fs0 efile-preron_register1 nav-icon" aria-hidden="true"></i> ডাক বিলি নিবন্ধন বহি</a>

          </li>

          <li class="nav-item">
            <a class="nav-link" href="#">
              <i class="fs0 efile-register1 nav-icon" aria-hidden="true"></i> শাখা ডায়েরি নিবন্ধন বহি</a>

          </li>

        </ul>
      </li>
      <li class="nav-item nav-dropdown">
        <a class="nav-link nav-dropdown-toggle" href="#">
          <i class="fs0 a2i_ld_protibedon3 nav-icon"></i> প্রতিবেদনসমূহ</a>
        <ul class="nav-dropdown-items">
          <li class="nav-item">
            <a class="nav-link" href="#">
              <i class="fs0 efile-choloman_dak5 nav-icon" aria-hidden="true"></i> অমীমাংসিত ডাক তালিকা</a>

          </li>

          <li class="nav-item">
            <a class="nav-link" href="#">
              <i class="fs0 efile-nishponno_dak5 nav-icon" aria-hidden="true"></i> মীমাংসিত ডাক তালিকা</a>

          </li>

          <li class="nav-item">
            <a class="nav-link" href="#">
              <i class="fs0 efile-nothi1 nav-icon" aria-hidden="true"></i> নথিতে উপস্থাপিত ডাক তালিকা</a>

          </li>

          <li class="nav-item">
            <a class="nav-link" href="#">
              <i class="fs0 efile-potrojari1 nav-icon" aria-hidden="true"></i> পত্রজারি ডাক তালিকা</a>

          </li>

          <li class="nav-item">
            <a class="nav-link" href="#">
              <i class="fa fa-bar-chart nav-icon" aria-hidden="true"></i> নথিজাত ডাক তালিকা</a>

          </li>

        </ul>
      </li>
      <li class="nav-item nav-dropdown">
        <a class="nav-link nav-dropdown-toggle" href="#">
          <i class="fs0 efile-settings2 nav-icon"></i> সেটিংস</a>
        <ul class="nav-dropdown-items">
          <li class="nav-item">
            <a class="nav-link" href="#">
              <i class="fa fa-edit nav-icon" aria-hidden="true"></i> ডাক সিদ্ধান্ত ব্যক্তিগত</a>

          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">
              <i class="fa fa-edit nav-icon" aria-hidden="true"></i> ডাক সিদ্ধান্ত সিস্টেম</a>

          </li>

        </ul>
      </li>
    </ul>
  </nav>
  <div class="alert-warning alert d-flex justify-content-between mb-0 update-notice align-items-center">
    <span class="notice-message">আপলোড হতে বাকি আছে</span>
    <span class="notice-count bg-warning px-1 rounded shadow-sm">১২১২</span>
  </div>
  <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div>