<div class="bg-white shadow-sm align-items-center py-2 d-flex">
    <div class="col-xl-4">
        <?= $this->Form->select('nothi_type', ['অফিস সহকারী, ব্যবসা-বাণিজ্য শাখা', 'সিনিয়র সফটওয়্যার ইঞ্জিনিয়ার 1 (অতিরিক্ত দায়িত্ব), ই-সার্ভিস টেস্ট', 'সাঁট লিপিকার, জেলা প্রশাসকের কার্যালয়'], ['class' => 'form-control', 'empty' => '-- পদবি নির্বাচন করুন --']); ?>
    </div>
    <div class="col d-flex">
        <div class="search-all flex-fill">
            <div class="dropdown">
                <input type="text" class="form-control" placeholder="&#xf002; বিষয় দিয়ে খুঁজুন...">
                <button id="advanced-search" class="btn btn-transparent text-info btn-toggler" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-caret-down"></i></button>
                <div class="dropdown-menu modules dropdown-menu-right shadow w-100" aria-labelledby="advanced-search">
                    <form class="px-4 py-3" action="" method="post">
                        <h4 class="font-bn-en">বিস্তারিত খুঁজুন</h4>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <?= $this->Form->control('nothi_no', ['placeholder' => 'নথি নাম্বার', 'class' => 'form-control']) ?>
                            </div>
                            <div class="col-md-6 mb-3">
                                <?= $this->Form->select('nothi_type', ['নিম্নোক্ত তালিকা', 'থেকে পদবি ', 'নির্বাচন করুন'], ['class' => 'form-control', 'empty' => '-- নথি ধরন --']); ?>
                            </div>
                            <div class="col-md-12 mb-3">
                                <div class="input-group">
                                    <input type="text" aria-label="First name" class="form-control datepicker" placeholder="তারিখ নির্বাচন">
                                    <span class="input-group-text">হইতে</span>
                                    <input type="text" aria-label="Last name" class="form-control datepicker" placeholder="তারিখ নির্বাচন">
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <?= $this->Form->select('select_section', ['গোপনীয় শাখা', 'আর এম শাখা', 'জেনারেল সার্টিফিকেট শাখা', 'রাজস্ব মুন্সিখানা শাখা', 'রেকর্ড রুম শাখা', 'ভূমি অধিগ্রহণ শাখা', 'ভি পি শাখা', 'প্রকাশনা শাখা', 'ফৌজদারী নকলখানা শাখা', 'রীট শাখা', 'বিচার শাখা', 'জুডিশিয়াল মুন্সিখানা শাখা', 'লাইসেন্স (প্রমোদ) শাখা', 'আগ্নেয়াস্ত্র শাখা', 'সীমান্ত শাখা', 'নির্বাহী ম্যাজিস্ট্রেট আদালত', 'গোপনীয় শাখা', 'সার্বিক', 'স্থানীয় সরকার', 'রাজস্ব', 'গোপনীয় শাখা', 'অতিরিক্ত জেলা ম্যাজিস্ট্রেটের দপ্তর', 'ভূমি অধিগ্রহণ', 'এনজিও বিষয়ক শাখা', 'মুক্তিযোদ্ধা কল্যাণ বিষয়ক শাখা', 'ফ্রন্ট ডেস্ক শাখা', 'শিক্ষা ও কল্যাণ শাখা', 'জেলা ই-সেবা কেন্দ্র'], ['class' => 'form-control', 'empty' => '-- শাখা নির্বাচন করুন --']); ?>
                            </div>
                            <div class="col-md-6 mb-3">
                                <?= $this->Form->select('all_nothi', ['গোপনীয় নথি সমূহ', 'শুধু জরুরী নথি সমূহ'], ['class' => 'form-control', 'empty' => '-- সকল নথি --']); ?>
                            </div>

                        </div>
                        <div class="btn-group">
                            <button type="submit" class="btn btn-success-n text-white"><i class="fa fa-search"></i> খুঁজুন
                            </button>
                            <button type="reset" class="btn btn-danger-n text-white"><i class="fa fa-refresh"></i> রিসেট
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="btn btn-success text-white"><i class="fa fa-search"></i></div>
        <div class="btn btn-danger"><i class="fa fa-refresh"></i></div>
        <div class="btn btn-primary aside-menu-toggler" data-toggle="aside-menu-lg-show"><i class="fa fa-bars"></i>
        </div>
    </div>
</div>  