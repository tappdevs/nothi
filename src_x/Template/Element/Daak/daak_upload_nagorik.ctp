<div class="p-3 flex-fill pScroll">
  <div class="card">
    <div class="card-body">
      <h3>নাগরিক ডাক আপলোড</h3>
      <?= $this->element('/Daak/DaakUpload/upload_nagorik_daak_description'); ?>

      <?= $this->element('/Daak/DaakUpload/upload_nagorik_mul_prapok'); ?>

      <?= $this->element('/Daak/DaakUpload/upload_nagorik_attachment'); ?>

      <div class="btn-group btn-group-round">
        <button class="btn btn-primary" type="submit">
          <i class="fs1 efile-save2 mr-2"></i>
          সংরক্ষণ
        </button>
        <button class="btn btn-success" type="submit">
          <i class="fs1 efile-send3 mr-2"></i>
          প্রেরণ
        </button>
      </div>


    </div>
  </div>
</div>
<?= $this->element('/Daak/DaakUpload/upload_nagorik_seal'); ?>

<?= $this->Html->css('/coreui/node_modules/@chenfengyuan/datepicker/dist/datepicker.min'); ?>
<?= $this->Html->script('/coreui/node_modules/@chenfengyuan/datepicker/dist/datepicker.min'); ?>
<?= $this->Html->script('/coreui/js/daak_upload_nagorik'); ?>