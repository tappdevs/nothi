<?= $this->Html->css('coreui/css/nothi-style-html'); ?>
<div class="daak_view_container pScroll p-3 flex-fill">

  <div class='daak_view_navbar'>
    <?= $this->element('/Daak/DaakView/detail_navigation'); ?>
  </div>
  <div class='daak_view_content'>
    <ul class="nav nav-tabs" id="myTab" role="tablist">
      <li class="nav-item">
        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home"
            aria-selected="true">সহকারী পরিচালক জাতীয় </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile"
            aria-selected="false">পরিচালক জাতীয় ভোক্তা অধিকার সংরক্ষণ</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact"
            aria-selected="false">জাতীয় ভোক্তা অধিকার সংরক্ষণ</a>
      </li>
    </ul>
    <div class="tab-content" id="myTabContent">
      <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
        <?= $this->element('Daak/DaakView/daak_view_content_tab', ['tab_no' => 1, 'doc_type' => 'pdf']); ?>
      </div>
      <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
        <?= $this->element('Daak/DaakView/daak_view_content_tab', ['tab_no' => 2, 'doc_type' => 'docx']); ?>
      </div>
      <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
        <?= $this->element('Daak/DaakView/daak_view_content_tab', ['tab_no' => 3, 'doc_type' => 'image']); ?>
      </div>
    </div>
  </div>
</div>