<div class="form-body">
    <h3 class="form-section">
        <i class="fa fa-hand-o-right"></i>&nbsp;প্রেরক
    </h3>
    <?php echo $officeCell = $this->cell('DesignationInfo'); ?>
    <hr/>
    <div class="form-horizontal">
        <div class="row">
            <div class=" col-md-6">
                <div class="">
                    <label class="control-label"> Sarok No </label>
                    <?php echo $this->Form->input('sender_sarok_no', array('label' => false, 'class' => 'form-control')); ?>

                </div>
            </div>
            <div class=" col-md-2">
                <div class="">
                    <label class="control-label"> Date </label>
                    <?php echo $this->Form->input('sending_date', array('type' => 'text', 'label' => false, 'class' => 'form-control', 'placeholder' => 'YYYY-mm-dd', 'value' => date('Y-m-d'))); ?>
                </div>
            </div>
            <div class=" col-md-4">
                <div class="">
                    <label class="control-label"> Media </label>
                    <?php echo $this->Form->input('dak_sending_media', array('type' => 'text', 'empty' => '--', 'label' => false, 'class' => 'form-control')); ?>
                </div>
            </div>
        </div>
    </div>

    <h3 class="form-section">
        <i class="fa fa-file-o"></i>&nbsp;Dak Biboroni
    </h3>

    <div class="form-horizontal">
        <div class="row">
            <div class="col-md-7">
                <div class="form-group form-horizontal">
                    <label class="control-label col-md-5">Docketing No</label>

                    <div class="col-md-6">
                        <?php echo $this->Form->input('docketing_no', array('label' => false, 'class' => 'form-control')); ?>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="form-group form-horizontal">
                    <label class="control-label col-md-4 pull-left">Receiving Date</label>

                    <div class="col-md-6">
                        <?php echo $this->Form->input('receiving_date', array('label' => false, 'type' => 'text', 'class' => 'form-control', 'placeholder' => 'YYYY-mm-dd')); ?>
                    </div>
                </div>
            </div>
        </div>
        <hr/>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group form-horizontal">
                    <label class="control-label col-md-2">Subject</label>

                    <div class="col-md-9">
                        <?php echo $this->Form->input('dak_subject', array('type' => 'text', 'label' => false, 'class' => 'form-control')); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label">Description </label>

            <div class="col-md-9">
                <?php echo $this->Form->input('dak_description', array('label' => false, 'type' => 'textarea', 'class' => 'form-control')); ?>
            </div>
        </div>
        <hr/>
        <div class="row">
            <div class="col-md-7">
                <div class="form-group form-horizontal">
                    <label class="control-label col-md-5">Secrecy</label>

                    <div class="col-md-6">
                        <?php echo $this->Form->input('dak_security_level', array('type' => 'select', 'empty' => '--', 'label' => false, 'class' => 'form-control')); ?>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="form-group form-horizontal">
                    <label class="control-label col-md-4 pull-left">Priority</label>

                    <div class="col-md-6">
                        <?php echo $this->Form->input('dak_priority_level', array('type' => 'select', 'empty' => '--', 'label' => false, 'class' => 'form-control')); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>

    <h3 class="form-section">
        <i class="fa fa-hand-o-left"></i>&nbsp;Receiver
    </h3>

    <?php echo $receiver_office_cell = $this->cell('OfficeUnitOrganogram'); ?>

    <h3 class="form-section">
        <i class="fa fa-upload"></i>&nbsp;<?php echo __(SHONGJUKTI) ?> s
    </h3>
    <?php if (isset($dak_attachments) && count($dak_attachments) > 0) { ?>
        <table class="table table-bordered att_tbl" id="att_tbl1">
            <tbody>
            <?php
            $i = 1;
            foreach ($dak_attachments as $single_data):
                ?>
                <tr>
                    <td width="80%">
                        <div id="<?php echo 'data_' . $single_data['id']; ?>"><a class=""
                                                                                 href="<?php echo $this->request->webroot . $single_data['file_name']; ?>"><?php echo $single_data['file_name']; ?></a>
                        </div>
                    </td>
                    <td width="20%" class="text-center">
		            	<span id="file_enable_div_<?php echo $single_data['id'];?>">
		               		<a target="_blank"
                               href="<?php echo $this->request->webroot . $single_data['file_name']; ?>"><i
                                    class="fs1 a2i_gn_view1"></i></a>
		                	<a href="javascript:;" class="trash_attachment"
                               data-attachment-id="<?php echo $single_data['id'];?>"><i class="fs1 a2i_gn_delete2"></i></a>
		                </span>
		                <span class="file_disable_div" id="file_disable_div_<?php echo $single_data['id'];?>">
		                	<a href="javascript:;" class="reload_trash_attachment"
                               data-attachment-id="<?php echo $single_data['id'];?>"><i class="fa fa-edit"></i></a>
		                </span>
                    </td>
                </tr>
                <?php
                $i++;
            endforeach;
            ?>
            </tbody>
        </table>
    <?php } ?>
    <div class="row">
        <div class="col-md-9">
            <?= $this->Form->button('', ['type' => 'button', 'class' => 'btn green fs1 a2i_gn_add1 projapoti_add_more_file']) ?>
        </div>
    </div>
    <hr/>
    <div id="projapoti_more_file_view"></div>

    <div id="file_view_1">
        <div class="form-group form-horizontal">
            <div class="form-group">
                <div class="col-md-1">
                    <span id="file_si_1" class="file_si badge badge-success">1.</span>
                </div>
                <div class="col-md-7">
                    <?php echo $this->Form->input('file_name.1', array('name' => 'attachment[1][file_name_file]', 'div' => false, 'label' => false, 'class' => 'btn default', 'type' => 'File')); ?>
                </div>
                <div class="col-md-2">
                    <a class="close fileinput-exists" data-file-index="1"
                       data-dismiss-view="file_view_1"
                       onclick="DakSetup.removeFileView(this)" href="javascript:"> </a>
                </div>
            </div>
        </div>
        <div class="form-group form-horizontal">
            <div class="form-group">
                <div class="col-md-10">
                    <?php echo $this->Form->input('file_description.1', array('name' => 'attachment[1][file_description]', 'label' => false, 'class' => 'form-control', 'placeholder' => 'File Description')); ?>
                </div>
            </div>
        </div>
        <?php echo $this->Form->hidden('file_dir.1', array('name' => 'attachment[1][file_dir]')); ?>
    </div>
</div>
<style>
    div.disabled {
        pointer-events: none;
        opacity: 0.2;
    }
</style>

<script>
    $(function () {
        $("#sender-office-designation-id").bind('blur', function () {
            DAK_FORM.generateSarokNo($(this).val());
        });
    });
</script>
<script type="text/javascript">
    var DAK_FORM = {
        generateSarokNo: function(office_id)
            {
                PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + '/dakDaptoriks/generateSarokNo',
                    {'office_id': office_id}, 'json',
                    function (response) {
                        $("#sender-sarok-no").val(response);
                    });
            }
    }
</script>

