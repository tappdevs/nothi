<div class="portlet-body form">

    <!--Start: Common Fields required only for create action -->
    <?php
    echo $this->Form->create($dak_daptoriks, ['type' => 'file', 'id' => 'uploadDakForm', 'method' => 'POST', 'action' => '#']);
    echo $this->Form->hidden('id', array('label' => false, 'class' => 'form-control'));
    echo $this->Form->hidden('dak_type', array('label' => false, 'class' => 'form-control', 'value' => DAK_DAPTORIK));
    echo $this->Form->hidden('dak_status', array('label' => false, 'class' => 'form-control', 'value' => 1));
    ?>
    <!--End: Common Fields required only for create action -->

    <!--Start: Remaining Form elements -->
    <div class="form-body">
        <h3 class="form-section">
        <?php echo __(PREROK) ?>
        </h3>
<?php echo $sender_cell = $this->cell('DakOffice', ['values' => $dak_daptoriks, 'prefix' => 'sender_']); ?>

        <h3 class="form-section">
<?php echo __(DAK_BIBORONI) ?>
        </h3>

        <div class="form-horizontal">
            <div class="row"> 
                <div class=" col-md-6">
                    <div class="">
                        <label class="control-label"> <?php echo __(SAROK_NO) ?> <span
			                        class="required">
                            * </span> </label>
<?php echo $this->Form->input('sender_sarok_no', array('label' => false, 'class' => 'form-control', 'required'=>true,'placeholder'=>'স্মারক নাম্বার লিখুন')); ?>
                    </div>
                </div>
                <div class=" col-md-2">
                    <div class="">
                        <label class="control-label">স্মারকের তারিখ</label>
<?php echo $this->Form->input('receiving_date', array('type' => 'text', 'label' => false, 'class' => 'form-control date-picker','placeholder'=>'yyyy-mm-dd' ,'default' => date('Y-m-d'), 'value'=> (!empty($dak_daptoriks->receiving_date)?($dak_daptoriks->receiving_date->format("Y-m-d")):date("Y-m-d")))); ?>
                    </div>
                </div>
                <div class=" col-md-6">
                    <div class="">
                        <?php
                        $dak_media = json_decode(DAK_MEDIA, true);
                        $dak_media = array_combine($dak_media, $dak_media);
                        ?>
                        <label
                            class="control-label"> <?php echo __(PRERONER_MADDHOM) ?> </label>
<?php echo $this->Form->input('dak_sending_media', array('label' => false, 'class' => 'form-control', 'options' => $dak_media)); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-horizontal">
            <div class="row">
                <div class="col-md-12">
                    <label class="control-label"> <?php echo __(BISHOY) ?> <span
                            class="required">
                            * </span> </label>
<?php echo $this->Form->input('dak_subject', array('type' => 'text', 'label' => false, 'class' => 'form-control', 'data-required' => 1,'placeholder'=>'ডাকের বিষয় লিখুন')); ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-5">
                    <label class="control-label"> <?php echo __(GOPONIOTA) ?> </label>
                    <?php
                    $dak_security_levels = json_decode(DAK_SECRECY_TYPE, true);
                    $dak_security_levels[0] = 'গোপনীয়তা বাছাই করুন';

                    echo $this->Form->input('dak_security_level', array('type' => 'select', 'label' => false, 'class' => 'form-control', 'options' => $dak_security_levels));
                    ?>
                </div>
                <div class="col-md-5">
                    <label class="control-label"> <?php echo __(OGRADHIKAR) ?> </label>
                    <?php
                    $dak_priority_levels = json_decode(DAK_PRIORITY_TYPE, true);
                    $dak_priority_levels[0]='অগ্রাধিকার বাছাই করুন';        
                    echo $this->Form->input('dak_priority_level', array('type' => 'select', 'label' => false, 'class' => 'form-control', 'options' => $dak_priority_levels));
                    ?>
                </div>
            </div>
        </div>
        <br>

        <div class="form-horizontal">
            <div class="row">
                <div class="col-md-12">
<?php echo $this->Form->hidden('docketing_no', array('label' => false, 'class' => 'form-control')); ?>
                </div>
            </div>
        </div>

        <h3 class="form-section">
<?php echo __(PRAPOK) ?>
        </h3>
        
        <div class="row">
            <div class="col-md-12 col-lg-12 sealDiv">
<?php echo $this->cell('DakSender', ['params' => $selected_office_section]); ?>
            </div>
        </div>

        <h3 class="form-section">
            <?php echo __('মূল ডাক ও সংযুক্তিসমূহ') ?> &nbsp;<span class="text-danger">*</span>
        </h3>
        <?php echo $this->Form->hidden('uploaded_attachments', ['id' => 'uploaded_attachments']) ?>
        <?php echo $this->Form->hidden('uploaded_attachments_names', ['id' => 'uploaded_attachments_names']) ?>
        <?php echo $this->Form->hidden('uploaded_attachments_is_main', ['id' => 'uploaded_attachments_is_main']) ?>
        <?php echo $this->Form->hidden('file_description', ['id' => 'file_description']) ?>
        <!--End: Form Buttons -->
<?php echo $this->Form->end(); ?>
        <!-- BEGIN PAGE CONTENT-->
       
       <form id="fileupload" action="<?php echo $this->Url->build(['_name'=>'tempUpload']) ?>" method="POST" enctype="multipart/form-data">
            <input type="hidden" name="module_type" value="Dak" />
           <input type="hidden" name="module" value="Dak" />
            <input type="hidden" name="office_id" value="<?php echo $selected_office_section['office_id'] ?>" />
            <input type="hidden" name="file_description_upload" id="file_description_upload" class="form-control" placeholder="সংযুক্তির বিবরণ">

            </br>
            <div class="row fileupload-buttonbar">
                <div class="col-lg-12">
	                <div class="alert alert-info font-lg text-center">সংযুক্তিসমূহের মধ্যে থেকে মূল ডাকটি চিহ্নিত করুন</div>
                    <!-- The fileinput-button span is used to style the file input field as button -->
                    <span class="btn green fileinput-button round-corner-5" title="সর্বোচ্চ ফাইল সাইজ ২০ এমবি হবে" data-toggle="tooltip">
                        <i class="fs1 a2i_gn_add1"></i>
                        <span>
                            ফাইল যুক্ত করুন </span>
                        <input type="file" name="files[]" multiple="" accept="image/*,.pdf">
                    </span>

                    <!-- The global file processing state -->
                    <span class="fileupload-process">
                    </span>
                </div>
                <!-- The global progress information -->
                <div class="col-lg-12 fileupload-progress fade">
                    <!-- The global progress bar -->
                    <div class="progress progress-striped active"
                         role="progressbar"
                         aria-valuemin="0" aria-valuemax="100">
                        <div class="progress-bar progress-bar-success"
                             style="width:0%;">
                        </div>
                    </div>
                    <!-- The extended global progress information -->
                    <div class="progress-extended">
                        &nbsp;
                    </div>
                </div>

            </div>
            <!-- The table listing the files available for upload/download -->
            <div role="presentation" class="table table-striped clearfix">
                <div class="files">
                    <?php
                    if (isset($dak_attachments) && count($dak_attachments) > 0) {
                        foreach ($dak_attachments as $single_data) {
                            $fileName = explode('/', $single_data['file_name']);
                            if($single_data['attachment_type']=='text')continue;
                            $attachmentHeaders = get_file_type($single_data['file_name']);

                            $value = array(
                                'user_file_name' => $single_data['user_file_name'],
                                'name' => urldecode($fileName[count($fileName) - 1]),
                                'thumbnailUrl' => (substr($attachmentHeaders,0,5) == 'image'?
                                    (FILE_FOLDER .$single_data['file_name'].'?token='.sGenerateToken(['file'=>$single_data['file_name']],['exp'=>time() + 60*300])) : null),
                                'size' => '',
                                'type' => $attachmentHeaders,
                                'url' => FILE_FOLDER . $single_data['file_name'].'?token='.sGenerateToken(['file'=>$single_data['file_name']],['exp'=>time() + 60*300]),
                                'deleteUrl' => $this->Url->build(['_name'=>'secureDelete','?'=>[
                                    'id'=>$single_data['id'],
                                    'token'=>$temp_token
                                ]]),
                                'deleteType' => "GET",
                            );
	                        ?>
                            <div class="col-md-2 template-download" style="border: silver solid 1px;padding: 10px;margin-left:5px; <?=($single_data['is_main']==1) ?'background:#eef5f2;' : ''?>">
						        <div class="pull-right">
						            <button class="btn delete btn-sm btn-link" title="ফাইলটি মুছে ফেলুন" style="color:red;" data-type="<?=$value['deleteType']?>" data-url="<?=$value['deleteUrl']?>">
									    <i class="fs1 a2i_gn_close2"></i>
								    </button>
						        </div>
						        <div>
						            <label>
						            <input title="মূল ডাক" class="main-dak radio" style="float:left" type="radio" name="main_dak" value="<?=$single_data['is_main']?>" onclick="mainDak(this)" <?=($single_data['is_main']==1)?'checked':''?>>&nbsp;মূল ডাক
						            </label>
						        </div>
						        <div style="padding: 10px 20px;width:151px;height:131px;">
							        <?php if(!empty($value["thumbnailUrl"])): ?>
								        <a hrefed="<?=$value['url']?>" downloaded="<?=$value['name']?>" onclick="doModal('imgViewer', 'ইমেজ ভিউয়ার', '<img src=<?=$value['url']?> style=\'height:auto;width:100% \' />')">
								            <img src="<?=$value["thumbnailUrl"]?>" style="width:100%">
							            </a>
								    <?php else: ?>
								        <?php //if($value['type'] == 'application/pdf'): ?>
								            <a hrefed="<?=$value['url']?>" onclick="doModal('pdfViewer', 'পিডিএভ ভিউয়ার', '<embed src=<?=$value['url']?> style=\'height:calc(100vh - 207px);width:100% \' type=<?=$value['type']?>></embed>')" downloaded="<?=$value['name']?>">
									            <i class="fa fa-file-pdf-o" style="font-size: 111px;margin-top: 50px;"></i>
								            </a>
								        <?php //endif; ?>
							        <?php endif; ?>
						        </div>
						        <div>
						            <span class="size"><?= (isset($value['size'])) ? $value['size'] : 0 ?></span>
						        </div>
						        <div>
						            <input type="text" <?=($single_data['is_main']==1)?'value="মূল ডাক" readonly="readonly"':'value="'.$value['user_file_name'].'"'?> class="form-control potro-attachment-input" image="<?=$value['url']?>" file-type="<?=$value['type']?>" placeholder="সংযুক্তির নাম">
						        </div>
						    </div>
                            <?php
                        }
                    }
                    ?>
                </div>
            </div>
        </form>
    </div>

    <!--End: Remaining Form elements -->
    <!--Start: Form Buttons -->
    <div class="form-actions">
        <div class="row">
            <div class="col-md-9">
	            <div class="btn-group btn-group-round">
	                <?= $this->Form->button('<i class="fs1 efile-save2"></i> '.__(SAVE), ['class' => 'btn btn-primary', 'onclick' => 'DAK_FORM.submitForm()']) ?>
	                <?= $this->Form->button('<i class="fs1 efile-send3"></i> '.__(SEND), ['class' => 'btn purple', 'onclick' => 'DAK_FORM.submitAndSendForm()']) ?>
	            </div>
            </div>
        </div>
    </div>
</div>

<link rel="stylesheet" type="text/css"
      href="<?php echo CDN_PATH; ?>assets/admin/pages/css/invoice.css"/>
<!-- END PAGE LEVEL STYLES -->

<script src="<?php echo CDN_PATH; ?>assets/global/scripts/printThis.js"></script>
<div class="modal fade modal-purple height-auto" id="receiptprint" role="basic" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span> &nbsp;&nbsp;লোড করা হচ্ছে... </span>
            </div>
        </div>
    </div>
</div>
<script>
    $(function(){
        $('.date-picker').datepicker({
            orientation: "left",
            autoclose: true,
            format: "yyyy-mm-dd"
        });

        $('[title]').tooltip('destroy');
        $('[title]').tooltip({container: 'body'});
    })
</script>