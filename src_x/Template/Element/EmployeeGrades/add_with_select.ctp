<?php
$employeeCadres = array();
foreach ($employee_cadres as $cadre):
    $employeeCadres[$cadre->id] = $cadre['cadre_name_bng'];
endforeach;

$employeeRanks = array();
foreach ($employee_ranks as $ranks):
    $employeeRanks[$ranks->id] = $ranks['rank_name_bng'];
endforeach;
?>

<div class="row">
    <div class="col-md-5">
        <label class="control-label col-md-4"><?php echo __('Cadre') ?> </label>
        <?php echo $this->Form->input('employee_cadre_id',
            array('label' => false, 'type' => 'select', 'class' => 'form-control input-medium',
            'options' => $employeeCadres)); ?>
        <?php
        if (!empty($errors['employee_cadre_id'])) {
            echo "<div class='font-red'>".$errors['employee_cadre_id']['_empty']."</div>";
        }
        ?>
    </div>
    <div class="col-md-5">
        <label class="control-label col-md-4"><?php echo __('Rank') ?></label>
        <?php echo $this->Form->input('employee_rank_id',
            array('label' => false, 'type' => 'select', 'class' => 'form-control  input-medium',
            'options' => $employeeRanks)); ?>
        <?php
        if (!empty($errors['employee_rank_id'])) {
            echo "<div class='font-red'>".$errors['employee_rank_id']['_empty']."</div>";
        }
        ?>
    </div>
</div><br>

<hr><br>


<div class="form-horizontal">
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __('Grader') ?> <?php echo __('Name Bangla') ?></label>

        <div class="col-sm-3">
            <?php echo $this->Form->input('grade_name_bng',
                array('label' => false, 'type' => 'text', 'class' => 'form-control',
                'placeholder' => ' গ্রেডের নাম (বাংলা) ')); ?>
<?php
if (!empty($errors['grade_name_bng'])) {
    echo "<div class='font-red'>".$errors['grade_name_bng']['_empty']."</div>";
}
?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __('Grader') ?> <?php echo __('Name English') ?></label>

        <div class="col-sm-3">
            <?php echo $this->Form->input('grade_name_eng',
                array('label' => false, 'type' => 'text', 'class' => 'form-control',
                'placeholder' => ' গ্রেডের নাম (ইংরেজি) ')); ?>
<?php
if (!empty($errors['grade_name_eng'])) {
    echo "<div class='font-red'>".$errors['grade_name_eng']['_empty']."</div>";
}
?>
        </div>
    </div>

</div>