<?php
$officeMinistries = array();
foreach ($office_ministries as $ministries):
    $officeMinistries[$ministries->id] = $ministries['name_bng'];
endforeach;
?>

<div class="form-horizontal">
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __('Ministry') ?></label>

        <div class="col-sm-3">
            <?php echo $this->Form->input('office_ministry_id', array('label' => false, 'type' => 'select', 'class' => 'form-control', 'options' => $officeMinistries)); ?>
        </div>
    </div>
</div>
<hr><br>

<div class="form-horizontal">
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __('Rankr') ?> <?php echo __('Name Bangla') ?></label>

        <div class="col-sm-3">
            <?php echo $this->Form->input('rank_name_bng', array('label' => false, 'class' => 'form-control')); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __('Rankr') ?> <?php echo __('Name English') ?></label>

        <div class="col-sm-3">
            <?php echo $this->Form->input('rank_name_eng', array('label' => false, 'class' => 'form-control')); ?>
        </div>
    </div>

</div>