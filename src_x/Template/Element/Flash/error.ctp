<link rel="stylesheet" type="text/css"
      href="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-toastr/toastr.min.css"/>
<style>
    .message{
        word-wrap: break-word;
    }
    .error-message{
        color: red;
    }
</style>
<div class="alert alert-danger message error">
    <button class="close" data-close="alert"></button>
    <?php echo (!is_array($message))?h($message):'' ?>
</div>


<script src="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-toastr/toastr.min.js"></script>
<script src="<?php echo CDN_PATH; ?>assets/admin/pages/scripts/ui-toastr.js"></script>
<script>


    $('.message').hide();
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "positionClass": "toast-bottom-right"
    };

    var msg = <?php if( is_array($message) ){
        echo json_encode($message);
    }else{
        echo json_encode(array($message));
    } ?>


    $.each(msg, function(i,v){
        $(document).find('[name=' + i +']').attr('style','border:1px solid red');
        toastr.error(v);
    })


</script>