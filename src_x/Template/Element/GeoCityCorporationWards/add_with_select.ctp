<?php
$geoDivisions = array();
foreach ($geo_divisions as $divisions):
    $geoDivisions[$divisions->id] = $divisions['division_name_bng'];
endforeach;

$geoDistricts = array();
foreach ($geo_districts as $districts):
    $geoDistricts[$districts->id] = $districts['district_name_bng'];
endforeach;

$geoCityCorporation = array();
foreach ($geo_city_corporations as $geo_city_corporation):
    $geoCityCorporation[$geo_city_corporation->id] = $geo_city_corporation['city_corporation_name_bng'];
endforeach;
?>



<div class="row">
    <div class="col-md-4">
        <label class="control-label col-md-4"><?php echo __("Geo Division") ?></label>
        <?php echo $this->Form->input('geo_division_id',
            array('label' => false, 'type' => 'select', 'class' => 'form-control input-small',
            'options' => $geoDivisions)); ?>
        <?php
        if (!empty($errors['geo_division_id'])) {
            echo "<div class='font-red'>".$errors['geo_division_id']['_empty']."</div>";
        }
        ?>
    </div>
    <div class="col-md-4">
        <label class="control-label col-md-4 pull-left"><?php echo __("District") ?></label>
        <?php echo $this->Form->input('geo_district_id',
            array('label' => false, 'type' => 'select', 'class' => 'form-control input-small',
            'options' => $geoDistricts)); ?>
        <?php
        if (!empty($errors['geo_district_id'])) {
            echo "<div class='font-red'>".$errors['geo_district_id']['_empty']."</div>";
        }
        ?>
    </div>
    <div class="col-md-4">
        <label class="control-label col-md-5 pull-left"><?php echo __("City Corporation") ?></label>
        <?php echo $this->Form->input('geo_city_corporation_id',
            array('label' => false, 'type' => 'select', 'class' => 'form-control input-small',
            'options' => $geoCityCorporation)); ?>
        <?php
        if (!empty($errors['geo_city_corporation_id'])) {
            echo "<div class='font-red'>".$errors['geo_city_corporation_id']['_empty']."</div>";
        }
        ?>
    </div>
</div>
<hr><br>

<div class="form-horizontal">
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __("Wardr") ?> <?php echo __("Name Bangla") ?></label>

        <div class="col-sm-3">
            <?php echo $this->Form->input('ward_name_bng',
                array('label' => false, 'class' => 'form-control')); ?>
            <?php
            if (!empty($errors['ward_name_bng'])) {
                echo "<div class='font-red'>".$errors['ward_name_bng']['_empty']."</div>";
            }
            ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __("Wardr") ?> <?php echo __("Name English") ?> </label>

        <div class="col-sm-3">
            <?php echo $this->Form->input('ward_name_eng',
                array('label' => false, 'class' => 'form-control')); ?>
            <?php
            if (!empty($errors['ward_name_eng'])) {
                echo "<div class='font-red'>".$errors['ward_name_eng']['_empty']."</div>";
            }
            ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __("Ward") ?> <?php echo __("Code") ?></label>

        <div class="col-sm-3">
            <?php echo $this->Form->input('bbs_code',
                array('label' => false, 'class' => 'form-control')); ?>
            <?php
            if (!empty($errors['bbs_code'])) {
                echo "<div class='font-red'>".$errors['bbs_code']['_empty']."</div>";
            }
            ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __("Status") ?></label>

        <div class="col-sm-3">
            <?php echo $this->Form->checkbox('status',
                array('label' => false, 'checked' => true)); ?>
            <?php
            if (!empty($errors['status'])) {
                echo "<div class='font-red'>".$errors['status']['_empty']."</div>";
            }
            ?>
        </div>
    </div>
</div>