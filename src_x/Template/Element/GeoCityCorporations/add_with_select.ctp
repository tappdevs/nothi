<?php
$geoDistricts = array();
foreach ($geo_districts as $districts):
    $geoDistricts[$districts->id] = $districts['district_name_bng'];
endforeach;

$geoDivisions = array();
foreach ($geo_divisions as $divisions):
    $geoDivisions[$divisions->id] = $divisions['division_name_bng'];
endforeach;
?>

<div class="row">
    <div class="col-md-5 col-sm-5">
        <div class="form-group form-horizontal">
            <label class="control-label col-md-5 col-sm-5 bold font-lg"><?php echo __("Geo Division") ?></label>

            <div class="col-md-7 col-sm-7">
                <?php echo $this->Form->input('geo_division_id',
                    array('label' => false, 'type' => 'select', 'class' => 'form-control',
                    'options' => $geoDivisions)); ?>
                <?php
                if (!empty($errors['geo_division_id'])) {
                    echo "<div class='font-red'>".$errors['geo_division_id']['_empty']."</div>";
                }
                ?>
            </div>
        </div>
    </div>
    <div class="col-md-5 col-sm-5">
        <div class="form-group form-horizontal">
            <label class="control-label col-md-5 col-sm-5 bold font-lg"><?php echo __("District") ?></label>

            <div class="col-md-7 col-sm-7">
                <?php echo $this->Form->input('geo_district_id',
                    array('label' => false, 'type' => 'select', 'class' => 'form-control',
                    'options' => $geoDistricts)); ?>
                <?php
                if (!empty($errors['geo_district_id'])) {
                    echo "<div class='font-red'>".$errors['geo_district_id']['_empty']."</div>";
                }
                ?>
            </div>
        </div>
    </div>
</div>
<hr><br>

<div class="form-horizontal">
    <div class="form-group">
        <label
            class="col-sm-4 control-label"><?php echo __("City Corporationr") ?> <?php echo __("Name Bangla") ?> </label>

        <div class="col-sm-3">
            <?php echo $this->Form->input('city_corporation_name_eng',
                array('label' => false, 'class' => 'form-control')); ?>
            <?php
            if (!empty($errors['city_corporation_name_eng'])) {
                echo "<div class='font-red'>".$errors['city_corporation_name_eng']['_empty']."</div>";
            }
            ?>
        </div>
    </div>
    <div class="form-group">
        <label
            class="col-sm-4 control-label"><?php echo __("City Corporationr") ?> <?php echo __("Name English") ?></label>

        <div class="col-sm-3">
            <?php echo $this->Form->input('city_corporation_name_bng',
                array('label' => false, 'class' => 'form-control')); ?>
            <?php
            if (!empty($errors['city_corporation_name_bng'])) {
                echo "<div class='font-red'>".$errors['city_corporation_name_bng']['_empty']."</div>";
            }
            ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __("City BBS Code") ?></label>

        <div class="col-sm-3">
            <?php echo $this->Form->input('bbs_code',
                array('label' => false, 'class' => 'form-control')); ?>
            <?php
            if (!empty($errors['bbs_code'])) {
                echo "<div class='font-red'>".$errors['bbs_code']['_empty']."</div>";
            }
            ?>
        </div>
    </div>
    <!--            <div class="form-group">-->
    <!--                <label class="col-sm-4 control-label">District BBS Code</label>-->
    <!--                <div class="col-sm-3">-->
    <!--                    --><?php //echo $this->Form->input('district_bbs_code', array('label' => false, 'class' =>'form-control')); ?>
    <!--                </div>-->
    <!--            </div>-->
    <!--            <div class="form-group">-->
    <!--                <label class="col-sm-4 control-label">Division BBS Code</label>-->
    <!--                <div class="col-sm-3">-->
    <!--                    --><?php //echo $this->Form->input('division_bbs_code', array('label' => false, 'class' =>'form-control')); ?>
    <!--                </div>-->
    <!--            </div>-->
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __("Status") ?></label>

        <div class="col-sm-3">
            <?php echo $this->Form->checkbox('status',
                array('label' => false, 'checked' => true)); ?>
            <?php
            if (!empty($errors['status'])) {
                echo "<div class='font-red'>".$errors['status']['_empty']."</div>";
            }
            ?>
        </div>
    </div>
</div>
