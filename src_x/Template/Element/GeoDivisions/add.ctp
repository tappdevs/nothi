<div class="form-horizontal">
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __('Division Name (Bangla)') ?></label>

        <div class="col-sm-3">
            <?php echo $this->Form->input('division_name_bng', array('label' => false, 'class' => 'form-control characters_only', 'id' => 'name_character')); ?>
             <?php
            if (!empty($errors['division_name_bng'])) {
                echo "<div class='font-red'>".$errors['division_name_bng']['_empty']."</div>";
            }
            ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __('Division Name (English)') ?></label>

        <div class="col-sm-3">
            <?php echo $this->Form->input('division_name_eng', array('label' => false, 'class' => 'form-control')); ?>
             <?php
            if (!empty($errors['division_name_eng'])) {
                echo "<div class='font-red'>".$errors['division_name_eng']['_empty']."</div>";
            }
            ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __('Division BBS Code') ?></label>

        <div class="col-sm-2">
            <?php echo $this->Form->input('bbs_code', array('label' => false, 'class' => 'form-control two-digits')); ?>
             <?php
            if (!empty($errors['bbs_code'])) {
                echo "<div class='font-red'>".$errors['bbs_code']['_empty']."</div>";
            }
            ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __("Status") ?></label>

        <div class="col-sm-3">
<?php echo $this->Form->checkbox('status',
    array('label' => false, 'checked' => true));
?>
            <?php
            if (!empty($errors['status'])) {
                echo "<div class='font-red'>".$errors['status']['_empty']."</div>";
            }
            ?>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(".two-digits").keydown(function (event) {

        //prevent using shift with numbers
        if (event.shiftKey == true) {
            event.preventDefault();
        }

        if (!((event.keyCode == 190) || (event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46)) {
            //not a number key or period so prevent
            event.preventDefault();
        }
    });

    $(".two-digits").keyup(function (event) {
        var number = parseFloat($(this).val());
        if (number > 99) {
            $(this).val("");
        }
    });

    $('.characters_only').keyup(function () {
        this.value = this.value.replace(/[0-9 \&\*\!\@\#\$\^\(\_\-\+\=\;\:\)\.]/g, '');
    });

</script>