<?php
$geoDivisions = array();
foreach ($geo_divisions as $divisions):
    $geoDivisions[$divisions->id] = $divisions['division_name_bng'];
endforeach;

$geoDistricts = array();
foreach ($geo_districts as $districts):
    $geoDistricts[$districts->id] = $districts['district_name_bng'];
endforeach;

$geoUpazilas = array();
foreach ($geo_upazilas as $geo_upazila):
    $geoUpazilas[$geo_upazila->id] = $geo_upazila['upazila_name_bng'];
endforeach;
?>

    <div class="form-horizontal">
        <div class="form-group" style="margin: 10px;">
            <div class="col-md-2 col-sm-3 text-center">
                <label class="control-label bold font-lg"><?php echo __("Geo Division") ?></label>
            </div>
            <div class="col-md-2 col-sm-3 text-center">
                <?php
                echo $this->Form->input('geo_division_id',
                    array('label' => false, 'type' => 'select', 'class' => 'form-control input-small',
                    'options' => $geoDivisions));
                ?>
                <?php
                if (!empty($errors['geo_division_id'])) {
                    echo "<div class='font-red'>".$errors['geo_division_id']['_empty']."</div>";
                }
                ?>
            </div>
            <div class="col-md-2 col-sm-3 text-center">
                <label class="control-label bold font-lg"><?php echo __("District") ?></label>
            </div>
            <div class="col-md-2 col-sm-3 text-center">
                <?php
                echo $this->Form->input('geo_district_id',
                    array('label' => false, 'type' => 'select', 'class' => 'form-control input-small',
                    'options' => $geoDistricts));
                ?>
                <?php
                if (!empty($errors['geo_district_id'])) {
                    echo "<div class='font-red'>".$errors['geo_district_id']['_empty']."</div>";
                }
                ?>
            </div>
            <div class="col-md-2 col-sm-3 text-center">
                <label class="control-label bold font-lg"><?php echo __("Upazila") ?></label>
            </div>
            <div class="col-md-2 col-sm-3 text-center">
                <?php
                echo $this->Form->input('geo_upazila_id',
                    array('label' => false, 'type' => 'select', 'class' => 'form-control input-small',
                    'options' => $geoUpazilas));
                ?>
                <?php
                if (!empty($errors['geo_upazila_id'])) {
                    echo "<div class='font-red'>".$errors['geo_upazila_id']['_empty']."</div>";
                }
                ?>
            </div>
        </div>
    </div>
<hr><br>


<div class="form-horizontal">
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __("Municipalityr") ?> <?= __("Name Bangla") ?> </label>

        <div class="col-sm-3">
            <?php
            echo $this->Form->input('municipality_name_bng',
                array('label' => false, 'class' => 'form-control', 'placeholder' => 'পৌরসভার নাম (বাংলা)'));
            ?>
            <?php
            if (!empty($errors['municipality_name_bng'])) {
                echo "<div class='font-red'>".$errors['municipality_name_bng']['_empty']."</div>";
            }
            ?>
        </div>
    </div>
    <div class="form-group">
        <label
            class="col-sm-4 control-label"><?php echo __("Municipalityr") ?> <?php echo __("Name English") ?> </label>

        <div class="col-sm-3">
            <?php
            echo $this->Form->input('municipality_name_eng',
                array('label' => false, 'class' => 'form-control', 'placeholder' => 'পৌরসভার নাম (ইংরেজি)'));
            ?>
            <?php
            if (!empty($errors['municipality_name_eng'])) {
                echo "<div class='font-red'>".$errors['municipality_name_eng']['_empty']."</div>";
            }
            ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __("Municipality BBS Code") ?></label>

        <div class="col-sm-3">
            <?php
            echo $this->Form->input('bbs_code',
                array('label' => false, 'class' => 'form-control', 'placeholder' => 'পৌরসভা কোড'));
            ?>
            <?php
            if (!empty($errors['bbs_code'])) {
                echo "<div class='font-red'>".$errors['bbs_code']['_empty']."</div>";
            }
            ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __("Status") ?></label>

        <div class="col-sm-3">
            <?php
            echo $this->Form->checkbox('status',
                array('label' => false, 'checked' => true));
            ?>
            <?php
            if (!empty($errors['status'])) {
                echo "<div class='font-red'>".$errors['status']['_empty']."</div>";
            }
            ?>
        </div>
    </div>
</div>