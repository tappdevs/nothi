<?php
$geoDivisions = array();
foreach ($geo_divisions as $divisions):
    $geoDivisions[$divisions->id] = $divisions['division_name_bng'];
endforeach;

$geoDistricts = array();
foreach ($geo_districts as $districts):
    $geoDistricts[$districts->id] = $districts['district_name_bng'];
endforeach;

$geoUpazilas = array();
foreach ($geo_upazilas as $geo_upazila):
    $geoUpazilas[$geo_upazila->id] = $geo_upazila['upazila_name_bng'];
endforeach;

?>

<div class="row">
    <div class="col-md-6">
        <label class="control-label col-md-4"><?php echo __('Geo Division') ?></label>
        <?php
        echo $this->Form->input('geo_division_id',
            array('label' => false, 'type' => 'select', 'class' => 'form-control input-medium',
            'options' => $geoDivisions));
        ?>
        <?php
        if (!empty($errors['geo_division_id'])) {
            echo "<div class='font-red'>".$errors['geo_division_id']['_empty']."</div>";
        }
        ?>
    </div>
    <div class="col-md-6">
        <label class="control-label col-md-4"><?php echo __('District') ?></label>
        <?php
        echo $this->Form->input('geo_district_id',
            array('label' => false, 'type' => 'select', 'class' => 'form-control  input-medium',
            'options' => $geoDistricts));
        ?>
        <?php
        if (!empty($errors['geo_district_id'])) {
            echo "<div class='font-red'>".$errors['geo_division_id']['_empty']."</div>";
        }
        ?>
    </div>
</div>
<br>
<div class="row">
    <div class="col-md-6">
        <label class="control-label col-md-4"><?php echo __('Upazila') ?></label>
        <?php
        echo $this->Form->input('geo_thana_id',
            array('label' => false, 'type' => 'select', 'class' => 'form-control input-medium',
            'options' => $geoUpazilas));
        ?>
<?php
if (!empty($errors['geo_upazila_id'])) {
    echo "<div class='font-red'>".$errors['geo_division_id']['_empty']."</div>";
}
?>
    </div>
    <div class="col-md-6">
        <label class="control-label col-md-4"><?php echo __('Thana') ?></label>
        <?php
        echo $this->Form->input('geo_thana_id',
            array('label' => false, 'type' => 'select', 'class' => 'form-control input-medium',
            'options' => $GeoThanas));
        ?>
<?php
if (!empty($errors['geo_thana_id'])) {
    echo "<div class='font-red'>".$errors['geo_thana_id']['_empty']."</div>";
}
?>
    </div>
</div>
<hr><br>


<div class="form-horizontal">
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __('Post Officer') ?> <?php echo __('Name Bangla') ?></label>

        <div class="col-sm-3">
            <?php
            echo $this->Form->input('postoffice_name_bng',
                array('label' => false, 'type' => 'text', 'class' => 'form-control',
                'placeholder' => 'Post Office Name in Bangla'));
            ?>
<?php
if (!empty($errors['postoffice_name_bng'])) {
    echo "<div class='font-red'>".$errors['postoffice_name_bng']['_empty']."</div>";
}
?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __('Post Officer') ?> <?php echo __('Name English') ?></label>

        <div class="col-sm-3">
<?php
echo $this->Form->input('postoffice_name_eng',
    array('label' => false, 'type' => 'text', 'class' => 'form-control',
    'placeholder' => 'Post Office Name in English'));
?>
            <?php
            if (!empty($errors['postoffice_name_eng'])) {
                echo "<div class='font-red'>".$errors['postoffice_name_eng']['_empty']."</div>";
            }
            ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __('Post Office') ?> <?php echo __('Code') ?></label>

        <div class="col-sm-3">
<?php echo $this->Form->input('bbs_code',
    array('label' => false, 'type' => 'text', 'class' => 'form-control'));
?>
            <?php
            if (!empty($errors['bbs_code'])) {
                echo "<div class='font-red'>".$errors['bbs_code']['_empty']."</div>";
            }
            ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __('Status') ?></label>

        <div class="col-sm-3">
<?php echo $this->Form->checkbox('status',
    array('label' => false, 'checked' => true));
?>
<?php
if (!empty($errors['status'])) {
    echo "<div class='font-red'>".$errors['status']['_empty']."</div>";
}
?>
        </div>
    </div>
</div>