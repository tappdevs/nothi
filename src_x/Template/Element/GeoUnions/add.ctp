<div class="form-horizontal">
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __('Unionr') ?> <?php echo __('Name Bangla') ?></label>

        <div class="col-sm-3">
            <?php echo $this->Form->input('union_name_bng', array('label' => false, 'type' => 'text', 'class' => 'form-control', 'placeholder' => 'Union Name in Bangla')); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __('Unionr') ?> <?php echo __('Name English') ?></label>

        <div class="col-sm-3">
            <?php echo $this->Form->input('union_name_eng', array('label' => false, 'type' => 'text', 'class' => 'form-control', 'placeholder' => 'Union Name in English')); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __('Upazila') ?> </label>

        <div class="col-sm-3">
            <?php echo $this->Form->input('geo_upazila_id', array('label' => false, 'type' => 'text', 'class' => 'form-control')); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __('District') ?> </label>

        <div class="col-sm-3">
            <?php echo $this->Form->input('geo_district_id', array('label' => false, 'type' => 'text', 'class' => 'form-control')); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __('Union Code') ?></label>

        <div class="col-sm-3">
            <?php echo $this->Form->input('bbs_code', array('label' => false, 'type' => 'text', 'class' => 'form-control')); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __('UBBS Code') ?></label>

        <div class="col-sm-3">
            <?php echo $this->Form->input('upazila_bbs_code', array('label' => false, 'type' => 'text', 'class' => 'form-control')); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __('BBS Code') ?></label>

        <div class="col-sm-3">
            <?php echo $this->Form->input('district_bbs_code', array('label' => false, 'type' => 'text', 'class' => 'form-control')); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __('Status') ?></label>

        <div class="col-sm-3">
            <?php echo $this->Form->input('status', array('label' => false, 'type' => 'checkbox', 'class' => 'form-control')); ?>
        </div>
    </div>
</div>