<?php
// for super admin
if ((isset($loggedUser) && $loggedUser['user_role_id'] <= 2)) {
    echo $this->element('LeftBar/doptor_left_bar_for_superadmin');
//for office admin
} else {
//please add new url to projapoti_static.php getOfficeAdminAccess function. Otherwise normal user can access it.
    ?>
    <li class=" ">
        <a href="javascript:;"> <i class="icon-briefcase"></i> <span class="title">কর্মকর্তা ব্যবস্থাপনা</span>
            <span class="arrow "></span></a>
        <ul class="sub-menu">
            <li>
                <a href="<?= $this->request->webroot; ?>officeEmployees/index"> <i class="icon-briefcase"></i> <span
                            class="title"> কর্মকর্তার তালিকা</span>
                </a>
            </li>
            <li>
                <a href="<?= $this->request->webroot; ?>employeeRecords/employeeManagement"> <i
                            class="icon-briefcase"></i>
                    <span class="title"> কর্মকর্তা ব্যবস্থাপনা</span>
                </a>
            </li>
            <li>
                <a href="<?= $this->request->webroot; ?>SummaryNothi/summaryNothiUser"> <i class="icon-briefcase"></i>
                    <span
                            class="title">সার সংক্ষেপ ব্যবস্থাপনা</span>
                </a>
            </li>
            <li>
                <a href="<?= $this->request->webroot; ?>employeeRecords/viewOfficeDashboard"> <i
                            class="fs0 efile-detail2"></i> <span
                            class="title"> অফিস ড্যাশবোর্ড দেখার অনুমতিপ্রাপ্ত কর্মকর্তা </span>
                </a>
            </li>
            <li>
                <a href="<?= $this->request->webroot; ?>EmployeeRecords/englishDesignationUpdateList"> <i
                            class="icon-briefcase"></i> <span
                            class="title">  কর্মকর্তাদের ইংরেজি পদবি সংশোধন</span>
                </a>
            </li>
            <li>
                <a href="<?= $this->Url->build(['controller' => 'Potrojari', 'action' => 'getDesignationListPotrojariHeader']) ?>">
                    <i class="fs0 a2i_gn_service3" aria-hidden="true"></i>
                    <span class="title">শাখার নাম দৃশ্যমান সেটিং </span>
                </a>
            </li>
            <li>
                <a href="<?= $this->Url->build(['controller' => 'OfficeEmployees', 'action' => 'employeeSignatureSetting']) ?>">
                    <i class="fa fa-pencil-square" aria-hidden="true"></i>
                    <span class="title">কর্মকর্তার স্বাক্ষর সেটিং</span>
                </a>
            </li>
	        <li>
		        <a href="<?= $this->Url->build(['controller' => 'CoreTransfer', 'action' => 'onlyContentTransfer']) ?>">
			        <i class="a2i_ld_orpitoshompottilease1" aria-hidden="true"></i>
			        <span class="title">পদবির কার্যক্রম স্থানান্তর</span>
		        </a>
	        </li>
        </ul>
    </li>
    <li class=" ">
        <a href="javascript:;"> <i class="icon-briefcase"></i> <span class="title">প্রতিকল্প</span>
            <span class="arrow "></span></a>
        <ul class="sub-menu">
            <li>
                <a href="<?= $this->request->webroot; ?>protikolpo/Setup"> <i class="icon-briefcase"></i> <span
                            class="title">প্রতিকল্প ব্যবস্থাপনা</span>
                </a>
            </li>
            <li>
                <a href="<?= $this->request->webroot; ?>protikolpo/Status"> <i class="icon-briefcase"></i> <span
                            class="title">প্রতিকল্প অবস্থা</span>
                </a>
            </li>
        </ul>
    </li>
    <li class=" ">
        <a href="javascript:;"><i class="fs0 a2i_gn_settings2" aria-hidden="true"></i> <span
                    class="title">অফিস সেটআপ</span>
            <span class="arrow "></span></a>
        <ul class="sub-menu">
            <li>
                <a href="<?= $this->request->webroot; ?>officeEmployeeMappings/officeHeadManager"><i
                            class="fs0 a2i_gn_settings2" aria-hidden="true"></i>
                    <span class="title">অফিস প্রধান</span>
                </a>
            </li>
            <li>
                <a href="<?= $this->request->webroot; ?>officeEmployeeMappings/officeFrontDeskManager"><i
                            class="fs0 a2i_gn_settings2" aria-hidden="true"></i>
                    <span class="title">অফিস ফ্রন্ট ডেস্ক</span>
                </a>
            </li>
            <li>
                <a href="<?= $this->request->webroot; ?>officeManagement/updateOwnOffice"><i
                            class="fs0 a2i_gn_settings2"
                            aria-hidden="true"></i> <span
                            class="title">অফিস তথ্য সংশোধন</span>
                </a>
            </li>
            <li>
                <a href="<?= $this->request->webroot; ?>officeManagement/officeUnitManagement"><i
                            class="fs0 a2i_gn_settings2"
                            aria-hidden="true"></i>
                    <span class="title"><?= ($loggedUser['user_role_id'] == 1 || $loggedUser['user_role_id']
                            == 2) ? "অফিস শাখা সংশোধন" : "অফিস শাখা" ?></span>
                </a>
            </li>
            <li>
                <a href="<?= $this->request->webroot; ?>officeManagement/officeUnitOrganogramManagement"><i
                            class="fs0 a2i_gn_settings2" aria-hidden="true"></i> <span
                            class="title"><?= ($loggedUser['user_role_id']
                            == 1 || $loggedUser['user_role_id'] == 2) ? "অফিস কাঠামো সংশোধন" : "অফিস কাঠামো" ?></span>
                </a>
            </li>
            <li>
                <a href="<?= $this->Url->build(['_name'=>'designationTransfer']) ?>">
                    <i class="fs0 a2i_gn_settings2" aria-hidden="true"></i> অফিস কাঠামো ট্র্যান্সফার</a>
            </li>
            <li>
                <a href="javascript:;">
                    <i class="fs0 a2i_gn_abedongrohon2" aria-hidden="true"></i>
                    <span class="title"> শাখা তথ্য সংশোধন</span>
                    <span class="arrow "></span>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a href="<?= $this->request->webroot; ?>officeEmployees/officeUnitNothiCodes">
                            <i class="fs0 a2i_gn_note1" aria-hidden="true"></i>
                            <span class="title">তালিকা ভিউ</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?= $this->request->webroot; ?>officeManagement/ownOfficeUnit">
                            <i class="fs0 a2i_gn_citizen2" aria-hidden="true"></i>
                            <span class="title">ট্রি ভিউ</span>
                        </a>
                    </li>
                </ul>
            </li>
            <?php
            $office = \Cake\ORM\TableRegistry::get('Offices')->get($session->read('selected_office_section')['office_id']);
            if ($office && ($office['unit_organogram_edit_option'] != null && $office['unit_organogram_edit_option'] != 0)) {
                ?>
                <li>
                    <a href="javascript:;">
                        <i class="fs0 a2i_gn_abedongrohon2" aria-hidden="true"></i>
                        <span class="title">নাম সংশোধন</span>
                        <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu">
                        <?php if ($office['unit_organogram_edit_option'] == 1 || $office['unit_organogram_edit_option'] == 2): ?>
                            <li>
                                <a href="<?= $this->request->webroot; ?>officeEmployees/editUnitsInfoOfficeWise">
                                    <i class="fa fa-edit" aria-hidden="true"></i>
                                    <span class="title">শাখার নাম সংশোধন</span>
                                </a>
                            </li>
                        <?php endif; ?>
                        <?php if ($office['unit_organogram_edit_option'] == 1 || $office['unit_organogram_edit_option'] == 3): ?>
                            <li>
                                <a href="<?= $this->request->webroot; ?>officeEmployees/designationNameUpdateOfficeWise">
                                    <i class="fa fa-edit" aria-hidden="true"></i>
                                    <span class="title">পদবির নাম সংশোধন</span>
                                </a>
                            </li>
                        <?php endif; ?>
                    </ul>
                </li>
                <?php
            }
            if (isset($loggedUser) && $loggedUser['user_role_id'] <= 2) {
                ?>
                <li>
                    <a href="<?= $this->request->webroot; ?>officeEmployees/editUnitsInfo"><i
                                class="fs0 a2i_gn_settings2"
                                aria-hidden="true"></i>
                        <span class="title">শাখার নাম সংশোধন</span>
                    </a>
                </li>
                <li>
                    <a href="<?= $this->request->webroot; ?>officeEmployees/designationNameUpdate"><i
                                class="fs0 a2i_gn_settings2"
                                aria-hidden="true"></i>
                        <span class="title">পদবির নাম সংশোধন</span>
                    </a>
                </li>
                <?php
            }
            ?>
        </ul>
    </li>
    <li class=" ">
        <a href="<?= $this->request->webroot; ?>officeEmployees/designationLebel">
            <i class="fs0 a2i_gn_user2" aria-hidden="true"></i> <span class="title">পদবির স্তর</span>

        </a>
    </li>
    <li class=" ">
        <a href="javascript:;">
            <i class="fs0 a2i_gn_hardcopy2" aria-hidden="true"></i>
            <span class="title"> রিপোর্ট</span>
            <span class="arrow "></span>
        </a>
        <ul class="sub-menu">
            <li>
                <a href="<?= $this->request->webroot; ?>users/loginAudit"> <i class="icon-briefcase"></i> <span
                            class="title">লগইন তথ্য</span>
                </a>
            </li>
            <li class=" ">
                <a href="javascript:;">
                    <i class="fs0 a2i_gn_hardcopy2" aria-hidden="true"></i>
                    <span class="title"> কার্যক্রম বিবরণী</span>
                    <span class="arrow "></span>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a href="<?= $this->request->webroot; ?>Performance/office">
                            <i class="fs0 a2i_gn_hardcopy2" aria-hidden="true"></i>
                            <span class="title">অফিস</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?= $this->request->webroot; ?>Performance/unit">
                            <i class="fs0 a2i_gn_hardcopy2" aria-hidden="true"></i>
                            <span class="title">শাখা</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?= $this->request->webroot; ?>Performance/designation">
                            <i class="fs0 a2i_gn_hardcopy2" aria-hidden="true"></i>
                            <span class="title">পদবি</span>
                        </a>
                    </li>
	                <li>
		                <a href="<?= $this->request->webroot; ?>Report/wingWiseReport">
			                <i class="fs0 a2i_gn_hardcopy2" aria-hidden="true"></i>
			                <span class="title">অনুবিভাগ ভিত্তিক ড্যাশবোর্ড</span>
		                </a>
	                </li>
                </ul>
            </li>
            <?php
            if (isset($loggedUser) && $loggedUser['user_role_id'] > 2) {
                ?>
                <li class=" ">
                    <a href="javascript:;">
                        <i class="fs0 a2i_gn_hardcopy2" aria-hidden="true"></i>
                        <span class="title"> অফিস ভিত্তিক নথি বিবরণী</span>
                        <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="<?= $this->request->webroot; ?>NothiReports/getMonthlyNothi">
                                <i class="fs0 a2i_gn_hardcopy2" aria-hidden="true"></i>
                                <span class="title">উপস্থাপিত নথি সংখ্যা</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?= $this->request->webroot; ?>NothiReports/getMonthlyNothiDetails">
                                <i class="fs0 a2i_gn_hardcopy2" aria-hidden="true"></i>
                                <span class="title">উপস্থাপিত নথি বিবরণী</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?= $this->Url->build(['controller' => 'NothiReports', 'action' => 'getNothiActivities']); ?>">
                                <i class="fs0 efile-note1" aria-hidden="true"></i>
                                <span class="title">তৈরিকৃত নথি তালিকা </span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class=" ">
                    <a href="javascript:;">
                        <i class="fs0 a2i_gn_hardcopy2" aria-hidden="true"></i>
                        <span class="title"> অনিষ্পন্ন কার্যক্রম বিবরণী</span>
                        <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="<?= $this->request->webroot; ?>Performance/unitPending">
                                <i class="fs0 a2i_gn_hardcopy2" aria-hidden="true"></i>
                                <span class="title">শাখা</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?= $this->request->webroot; ?>Performance/designationPending">
                                <i class="fs0 a2i_gn_hardcopy2" aria-hidden="true"></i>
                                <span class="title">পদবি</span>
                            </a>
                        </li>
                    </ul>
                </li>
				<?php if ($office['office_origin_id'] == 16): ?>
		            <li>
                        <a href="<?= $this->request->webroot; ?>Performance/districtWiseOfficeReport">
                            <i class="fs0 a2i_gn_hardcopy2" aria-hidden="true"></i>
                            <span class="title">জেলা অফিসসমূহের কার্যক্রম বিবরণী</span>
                        </a>
	                </li>
		            <li>
			            <a href="<?= $this->request->webroot; ?>Performance/customOfficeReport/17">
				            <i class="fs0 a2i_gn_hardcopy2" aria-hidden="true"></i>
				            <span class="title">উপজেলা নির্বাহী অফিসসমূহের কার্যক্রম বিবরণী</span>
			            </a>
		            </li>
		            <li>
			            <a href="<?= $this->request->webroot; ?>Performance/customOfficeReport/39">
				            <i class="fs0 a2i_gn_hardcopy2" aria-hidden="true"></i>
				            <span class="title">উপজেলা ভূমি অফিসসমূহের কার্যক্রম বিবরণী</span>
			            </a>
		            </li>
				<?php endif; ?>
				<?php
            }
            ?>
        </ul>
    </li>
    <?php
    if (isset($loggedUser) && $loggedUser['user_role_id'] > 2) {
        ?>
        <li class=" ">
            <a href="javascript:;">
                <i class="fs0 efile-nothi_management1" aria-hidden="true"></i>
                <span class="title"> নথি সেটিং</span>
                <span class="arrow "></span>
            </a>
            <ul class="sub-menu">
                <li>
                    <a href="<?= $this->request->webroot; ?>nothiNameUpdates">
                        <i class="fs0 a2i_gn_edit1" aria-hidden="true"></i>
                        <span class="title"> নথির তথ্য সংশোধন</span>
                    </a>
                </li>
                <li>
                    <a href="<?= $this->Url->build(['controller' => 'Potrojari', 'action' => 'setPotrojariHeader']) ?>">
                        <i class="fs0 a2i_gn_edit1" aria-hidden="true"></i>
                        <span class="title">পত্রজারি হেডিং সংশোধন</span>
                    </a>
                </li>
                <li class=" ">
                    <a href="javascript:;">
                        <i class="fs0 a2i_gn_shangrakkhan2" aria-hidden="true"></i>
                        <span class="title"> নথি আর্কাইভ</span>
                        <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="<?= $this->request->webroot; ?>Archive/archivable_list">
                                <i class="fs0 a2i_gn_template1" aria-hidden="true"></i>
                                <span class="title"> আর্কাইভের জন্য অপেক্ষমাণ নথিসমূহ</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?= $this->request->webroot; ?>Archive/pending_archivable_list">
                                <i class="fs0 a2i_gn_template1" aria-hidden="true"></i>
                                <span class="title">আর্কাইভের জন্য অপেক্ষমাণ অসম্পন্ন নথিসমূহ </span>
                            </a>
                        </li>
                        <li>
                            <a href="<?= $this->request->webroot; ?>Archive/archvied_status">
                                <i class="fs0 a2i_gn_template1" aria-hidden="true"></i>
                                <span class="title">আর্কাইভকৃত নথিসমূহের অবস্থা</span>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </li>

        <?php
    }
}
//please add new url to projapoti_static.php getOfficeAdminAccess function. Otherwise normal user can access it.
?>
