<?php
if (!empty($loggedUser['user_role_id']) && $loggedUser['user_role_id'] >= 2) {
    ?>
    <li class="">
        <a href="javascript:;"><i class="fs0 efile-nothi_management1" aria-hidden="true"></i> <span class="title">নথি ব্যবস্থাপনা</span>
            <span class="arrow"></span></a>
        <ul class="sub-menu" style="display: none;">
            <li>
                <a href="javascript:;">
                    <i class="fs0 a2i_gn_type2"></i> <span class="title">নথির ধরন</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a href="<?= $this->request->webroot ?>nothiTypes/nothiTypesAdd"><i class="fa fa-plus"></i>
                            <span class="title">ধরন তৈরি</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?= $this->request->webroot ?>nothiTypes/nothiTypes"><i class="icon-tag"></i>
                            <span class="title">ধরনসমূহ</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="<?= $this->request->webroot ?>nothiMasters/add"><i class="fs0 efile-add4"
                                                                            aria-hidden="true"></i>
                    <span class="title">নথি তৈরি</span>
                </a>
            </li>
            <li>
                <a href="<?= $this->request->webroot; ?>potrojariGroups/groupMail"> <i class="fs0 efile-user2"
                                                                                       aria-hidden="true"></i>
                    <span class="title"> পত্রজারি গ্রুপ  </span>
                </a>
            </li>
            <li>
                <a href="javascript:;">
                    <i class="fs0 efile-guard_file1"></i> <span class="title">গার্ড ফাইল</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a href="<?= $this->request->webroot ?>guardFiles/guardFileCategories"><i class="icon-tag"></i>
                            <span class="title">গার্ড ফাইলের ধরন</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?= $this->request->webroot ?>guardFiles/guardFiles"><i class="icon-tag"></i>
                            <span class="title">গার্ড ফাইল তালিকা</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?= $this->request->webroot ?>guardFiles/guardFileUploadAttachments"><i
                                    class="icon-tag"></i>
                            <span class="title">আপলোড গার্ড ফাইল</span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </li>
    <li class="">
        <a href="javascript:;"><i class="fs0 a2i_gn_register2" aria-hidden="true"></i> <span class="title">নিবন্ধন বহি</span>
            <span class="arrow"></span></a>
        <ul class="sub-menu" style="display: none;">
            <li>
                <a href="<?= $this->request->webroot ?>NothiMasters/masterFile"><i class="fs0 efile-user2"
                                                                                   aria-hidden="true"></i> <span
                            class="title">মাস্টার ফাইল</span>
                    <!--<span class="arrow"></span>-->
                </a>
            </li>
            <li>
                <a href="<?= $this->request->webroot ?>nothiRegisters/nothiPreronRegister"><i
                            class="fs0 efile-preron_register1" aria-hidden="true"></i> <span class="title">নথি প্রেরণ নিবন্ধন বহি</span>
                    <!--<span class="arrow"></span>-->
                </a>
            </li>
            <li>
                <a href="<?= $this->request->webroot ?>nothiRegisters/nothiGrohonRegister"><i
                            class="fs0 efile-grohon_register1" aria-hidden="true"></i> <span class="title">নথি গ্রহণ নিবন্ধন বহি</span>
                    <!--<span class="arrow"></span>-->
                </a>
            </li>
            <li>
                <a href="<?= $this->request->webroot ?>nothiRegisters/nothiDiaryRegister"><i class="fs0 efile-register1"
                                                                                             aria-hidden="true"></i>
                    <span
                            class="title">নথি নিবন্ধন বহি</span>
                </a>
            </li>
            <li>
                <a href="<?= $this->request->webroot ?>nothiRegisters/nothiPotrojariRegister"><i
                            class="fs0 efile-potrojari1" aria-hidden="true"></i> <span
                            class="title">পত্রজারি নিবন্ধন বহি</span>
                </a>
            </li>
	        <li>
		        <a href="<?= $this->request->webroot ?>nothiMasters/archivedNothiInbox"><i class="fs0 efile-save2" aria-hidden="true"></i>
			        <span class="title">আর্কাইভড নথিসমূহ</span>
		        </a>
	        </li>
        </ul>
    </li>
    <li class="">
        <a href="javascript:;"><i class="fs0 a2i_ld_protibedon3" aria-hidden="true"></i> <span
                    class="title">প্রতিবেদনসমূহ</span>
            <span class="arrow"></span></a>
        <ul class="sub-menu" style="display: none;">
            <li>
                <a href="<?= $this->request->webroot ?>nothiReports/sectionWiseNothiList"><i class="fs0 a2i_ld_protibedon3"
                                                                                             aria-hidden="true"></i>
                    <span
                            class="title">শাখাভিত্তিক নথিসমূহের তালিকা</span>
                </a>
            </li>
        </ul>
    </li>
    <li class="">
        <a href="<?= $this->Url->build(['_name'=>'pendingPotrojari']) ?>"><i class="fs0 efile-potrojari3" aria-hidden="true"></i> <span class="title">পত্রজারি পেন্ডিং  </span><?php echo ((isset($totalPotrojariPending) && !empty($totalPotrojariPending))?'<label class="badge badge-danger">'.enTobn($totalPotrojariPending).'</label>':''); ?>  </a>
    </li>
    <li class="">
        <a href="javascript:;"><i class="fs0 efile-settings2" aria-hidden="true"></i> <span class="title">সেটিংস </span>
            <span class="arrow"></span></a>
        <ul class="sub-menu" style="display: none;">
            <li>
                <a href="<?= $this->request->webroot ?>nothiDecisions/index"><i class="fs0 a2i_gn_abedongrohon2"
                                                                                aria-hidden="true"></i> <span
                            class="title">নথি সিদ্ধান্তসমূহ</span>
                </a>
            </li>
            <li>
                <a href="<?= $this->request->webroot ?>nothiRegisters/nothiPermissionAdd"><i class="fs0 a2i_gn_approval1"
                                                                                             aria-hidden="true"></i>
                    <span
                            class="title">নথিতে অনুমতি প্রদান</span>
                </a>
            </li>
            <li>
                <a href="<?= $this->request->webroot ?>nothiRegisters/nothiPermissionDelete"><i class="fs0 a2i_gn_close1"
                                                                                                aria-hidden="true"></i>
                    <span
                            class="title">নথি হতে অনুমতি প্রত্যাহার </span>
                </a>
            </li>
	        <li>
		        <a href="<?= $this->request->webroot; ?>Archive/archivable_list">
			        <i class="fs0 a2i_gn_template1" aria-hidden="true"></i>
			        <span class="title"> আর্কাইভের জন্য অপেক্ষমাণ নথিসমূহ</span>
		        </a>
	        </li>
        </ul>
    </li>
    <li class="">
        <a href="<?= $this->request->webroot ?>SummaryNothi/summaryNothiSarokTracking"><i class="fs0 a2i_nt_nishponnonothi1" aria-hidden="true"></i> <span class="title"> সার-সংক্ষেপ ট্রাকিং </span></a>
    </li>
	<?php if ($other_organogram_activities_permission['nothi'] == 1): ?>
		<li>
			<a href="<?= $this->request->webroot ?>permitted_nothi"><i class="efile-email4" aria-hidden="true"></i>
				<span class="title">শেয়ারকৃত নথিসমূহ</span>
			</a>
		</li>
	<?php endif; ?>

    <?php
}
//Menu for SuperAdmin
if (!empty($loggedUser['user_role_id']) && $loggedUser['user_role_id'] <= 2) {
    ?>
    <li class=" ">
        <a href="javascript:;">
            <i class="fs0 efile-settings2" aria-hidden="true"></i>
            <span class="title"><?= __('Setting') ?></span>
            <span class="arrow "></span>
        </a>
        <ul class="sub-menu">
            <li>
                <a href="<?= $this->request->webroot; ?>SuperAdminNothiDetailUpdates">
                    <i class="fa fa-pencil-square" aria-hidden="true"></i>
                    <span class="title"> নথির তথ্য সংশোধন </span>
                </a>
            </li>
            <li>
                <a href="<?= $this->request->webroot; ?>nothiTypes/superAdminOption">
                    <i class="fs0 a2i_gn_type2"></i>
                    <span class="title"><?= __('ধরনসমূহ ') ?></span>
                </a>
            </li>
            <li>
                <a href="<?= $this->request->webroot; ?>potrojariGroups/groupMail"> <i class="fs0 efile-user2"
                                                                                       aria-hidden="true"></i>
                    <span class="title"> পত্রজারি গ্রুপ  </span>
                </a>
            </li>
            <li>
                <a href="<?= $this->Url->build('/super-admin-bulk-nothi-number-update'); ?>">
                    <i class="fa fa-pencil-square" aria-hidden="true"></i>
                    <span class="title"> অফিস ভিত্তিক সকল নথি নাম্বার সংশোধন </span>
                </a>
            </li>
            <li>
                <a href="<?= $this->Url->build('/super-admin-office-potrojari-group-comparison'); ?>">
                    <i class="fs0 a2i_ld_namjarikarjakrom2" aria-hidden="true"></i>
                    <span class="title"> স্তর ভিত্তিক অফিস - পত্রজারি গ্রুপ তুলনা </span>
                </a>
            </li>
        </ul>
    </li>
    <?php
}
?>
