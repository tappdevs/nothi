<li class=" ">
    <a href="javascript:;"> <i class="fa fa-area-chart" aria-hidden="true"></i><span class="title"> রিপোর্টসমূহ </span>
        <span class="arrow "></span></a>
    <ul class="sub-menu">
        <li>
            <a href="<?= $this->request->webroot; ?>cron/layersReports">
                <i class="fa fa-line-chart" aria-hidden="true"></i>
                <span class="title">স্থর ভিত্তিক অফিস  কার্যক্রম বিবরণী  </span>
            </a>
        </li>
        <li>
            <a href="<?= $this->request->webroot; ?>cron/officesReports">
                <i class="fa fa-line-chart" aria-hidden="true"></i>
                <span class="title">অফিস  কার্যক্রম বিবরণী  </span>
            </a>
        </li>
    </ul>
</li>
