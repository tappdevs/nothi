<li>
    <a href="<?= $this->request->webroot; ?>monthlyReportUser">
        <i class="fa fa-file"></i>
        <span class="title">মাসিক প্রতিবেদন</span>
    </a>
</li>
<li>
    <a href="<?= $this->request->webroot; ?>efileRelatedReportUser">
        <i class="fa fa-file"></i>
        <span class="title">নির্দেশনাসমূহ</span>
    </a>
</li>
<?php
if ((isset($loggedUser) && $loggedUser['user_role_id'] <= 1)
) {
    //please add new url to projapoti_static.php getSuperAdminAccess function. Otherwise normal user can access it.

    ?>
    <li class=" ">
        <a href="javascript:;"> <i class="fs0 a2i_gn_settings2" aria-hidden="true"></i><span class="title"> রিপোর্ট সেটিং </span>
            <span class="arrow "></span></a>
        <ul class="sub-menu">
            <li class=" ">
                <a href="<?= $this->Url->build('/report-mark-distribution'); ?>"><i class="fa fa-tachometer"
                                                                                    aria-hidden="true"></i>
                    <span class="title">রিপোর্টের মার্ক বিন্যাস </span>
                </a>
            </li>
            <li class=" ">
                <a href="<?= $this->Url->build('/report-category-list'); ?>"><i class="fs0 a2i_ld_namjarikarjakrom2"
                                                                                aria-hidden="true"></i>
                    <span class="title">রিপোর্ট ক্যাটাগরি </span>
                </a>
            </li>
            <li class=" ">
                <a href="<?= $this->Url->build('/report-category-office-mapping'); ?>"><i
                            class="fs0 efile-head_of_wing2" aria-hidden="true"></i>
                    <span class="title"> ক্যাটাগরি - অফিস ম্যাপিং </span>
                </a>
            </li>
        </ul>
    </li>
    <li>
        <a href="<?= $this->Url->build('/category-wise-office-performance')?>">
            <i class="fs0 a2i_gn_dashboard1"></i>
            <span class="title">ক্যাটাগরি ভিত্তিক অফিস কার্যক্রম</span>
        </a>
    </li>
    <?php
}
//please add new url to projapoti_static.php getSuperAdminAccess function. Otherwise normal user can access it.
    ?>