<?php
if ((isset($loggedUser) && $loggedUser['user_role_id'] <= 1)
) {
    //please add new url to projapoti_static.php getSuperAdminAccess function. Otherwise normal user can access it.
    ?>
    <li class=" ">
        <a href="javascript:;">
            <i class="fs0 a2i_gn_home1"></i>
            <span class="title">মেন্যু ব্যবস্থাপনা</span>
            <span class="arrow "></span>
        </a>
        <ul class="sub-menu">
            <li>
                <a href="<?= $this->request->webroot; ?>ModuleMenus/index">
                    <i class="icon-plus"></i>
                    <span class="title">মেন্যু তালিকা</span>
                </a>
            </li>
            <li>
                <a href="<?= $this->request->webroot; ?>UserRoleActions/index">
                    <i class="icon-plus"></i>
                    <span class="title">ব্যবহারযোগ্যতা নিয়ন্ত্রণ</span>
                </a>
            </li>
        </ul>
    </li>

    <li class=" ">
        <a href="javascript:;">
            <i class="fs0 a2i_gn_home1"></i>
            <span class="title">লগইন পেইজ সেটিংস</span>
            <span class="arrow "></span>
        </a>
        <ul class="sub-menu">
            <li>
                <a href="<?= $this->request->webroot; ?>addLoginMenuNotice">
                    <i class="icon-plus"></i>
                    <span class="title">নোটিশ যুক্ত করুন</span>
                </a>
            </li>
            <li>
                <a href="<?= $this->Url->build('/monthlyReport'); ?>">
                    <i class="fs0 a2i_gn_another"></i>
                    <span class="title">মাসিক প্রতিবেদন যুক্ত করুন</span>
                </a>
            </li>
	        <li>
                <a href="<?= $this->Url->build('/efileRelatedReport'); ?>">
                    <i class="fs0 a2i_gn_another"></i>
                    <span class="title">নির্দেশনাসমূহ</span>
                </a>
            </li>
	        <li>
                <a href="<?= $this->Url->build('/addUserManual'); ?>">
                    <i class="fs0 a2i_gn_another"></i>
                    <span class="title">ব্যবহার সহায়িকা (ভার্সন ২.০)</span>
                </a>
            </li>
        </ul>
    </li>

    <li class=" ">
        <a href="javascript:">
            <i class="fs0 a2i_gn_home1"></i>
            <span class="title">মডুল তথ্য</span>
            <span class="arrow "></span>
        </a>
        <ul class="sub-menu">
            <li>
                <a href="<?= $this->request->webroot; ?>moduleInfos/add">
                    <i class="icon-plus"></i>
                    <span class="title">মডুল যোগ</span>
                </a>
            </li>
            <li>
                <a href="<?= $this->request->webroot; ?>moduleInfos/index">
                    <i class="icon-plus"></i>
                    <span class="title">মডুল তালিকা</span>
                </a>
            </li>
        </ul>
    </li>
    <li class=" ">
        <a href="javascript:;"> <i class="icon-briefcase"></i> <span class="title">ব্যবহারকারী ব্যবস্থাপনা</span>
            <span class="arrow "></span>
        </a>
        <ul class="sub-menu">
            <li>
                <a href="<?= $this->request->webroot; ?>userRoles/add"> <i class="icon-briefcase"></i> <span class="title">ব্যবহারকারীর নতুন রোল</span>
                </a>
            </li>
            <li>
                <a href="<?= $this->request->webroot; ?>userRoles/index"> <i class="icon-briefcase"></i> <span
                        class="title">ব্যবহারকারীদের রোলসমূহ</span>
                </a>
            </li>
            <li>
                <a href="<?= $this->request->webroot; ?>users/add"> <i class="icon-briefcase"></i> <span class="title">নতুন ব্যবহারকারী</span>
                </a>
            </li>
            <li>
                <a href="<?= $this->request->webroot; ?>users/index"> <i class="icon-briefcase"></i> <span class="title">ব্যবহারকারীর তালিকা</span>
                </a>
            </li>
        </ul>
    </li>
<?php } ?>
<li class=" ">
    <a href="javascript:;">
        <i class="fs0 a2i_gn_home1"></i>
        <span class="title">অফিস</span>
        <span class="arrow "></span>
    </a>
    <ul class="sub-menu">
        <?php if ($loggedUser['user_role_id'] == 1): ?>
            <li>
                <a href="<?= $this->request->webroot; ?>officeManagement/officeDomainSetup">
                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                    <span class="title">অফিস ডোমেইন ম্যাপিং</span>
                </a>
            </li>
        <?php endif; ?>
        <li>
            <a href="<?= $this->request->webroot; ?>OfficeManagement/OfficeAdmin">
                <i class="icon-plus"></i>
                <span class="title">অফিস অ্যাডমিন দায়িত্ব প্রদান</span>
            </a>
        </li>
        <li>
            <a href="<?= $this->request->webroot; ?>officeEmployeeMappings/officeHeadManager">
                <i class="fs0 efile-head_of_dept1"></i>
                <span class="title">দপ্তর প্রধান নির্বাচন</span>
            </a>
        </li>
        <li>
            <a href="javascript:;">
                <i class="fa fa-line-chart" aria-hidden="true"></i>
                <span class="title">কার্যক্রম বিবরণী</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                <li>
                    <a href="<?= $this->request->webroot; ?>cron/officesReports">
                        <i class="fa fa-line-chart" aria-hidden="true"></i>
                        <span class="title">অফিস  কার্যক্রম বিবরণী  </span>
                    </a>
                </li>
                <li>

                    <a href="<?= $this->request->webroot; ?>cron/layersReports">
                        <i class="fa fa-line-chart" aria-hidden="true"></i>
                        <span class="title">স্তর ভিত্তিক অফিস  কার্যক্রম বিবরণী  </span>
                    </a>
                </li>
            </ul>
        </li>
        <li>
            <a href="javascript:;">
                <i class="fa fa-wrench" aria-hidden="true"></i>
                <span class="title">স্তর ভিত্তিক সেটিং</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                <li class=" ">
                    <a href="<?= $this->request->webroot; ?>cron/layerOffices"><i class="fa fa-building-o" aria-hidden="true"></i>
                        <span class="title"> স্তর ভিত্তিক অফিস তালিকা </span>
                    </a>
                </li>
                <li>
                    <a href="javascript:;">
                        <i class="fa fa-wrench" aria-hidden="true"></i>
                        <span class="title">কাস্টম স্তর ভিত্তিক সেটিং</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class=" ">
                            <a href="<?= $this->Url->build(['controller' => 'CustomOfficeLayers', 'action' => 'index']); ?>"><i class="fa fa-building-o" aria-hidden="true"></i>
                                <span class="title"> কাস্টম স্তর তালিকা </span>
                            </a>
                        </li>
                        <li class=" ">
                            <a href="<?= $this->Url->build(['controller' => 'CustomOfficeLayers', 'action' => 'ministryWiseMap']); ?>"><i class="fa fa-wrench" aria-hidden="true"></i>
                                <span class="title"> মন্ত্রণালয় ভিত্তিক কাস্টম স্তর ম্যাপিং </span>
                            </a>
                        </li>
                        <li class=" ">
                            <a href="<?= $this->Url->build(['controller' => 'CustomOfficeLayers', 'action' => 'mapInfos']); ?>"><i class="fa fa-wrench" aria-hidden="true"></i>
                                <span class="title"> কাস্টম স্তর - অফিস ম্যাপিং </span>
                            </a>
                        </li>
                    </ul>
                </li>

            </ul>
        </li>

        <?php
        if ((isset($loggedUser) && $loggedUser['user_role_id'] <= 1) || ($loggedUser['is_admin'])
        ) {
            ?>
            <li>
                <a href="<?= $this->request->webroot; ?>NotificationOfficeSettings/supermanOfficeSettings">
                    <i class="fa fa-wrench" aria-hidden="true"></i>
                    <span class="title">অফিস নোটিফিকেশন  সেটিং </span>
                </a>
            </li>
            <li>
                <a href="<?= $this->request->webroot; ?>NotificationOfficeSettings/supermanOfficeSectionOrganogramSettings">
                    <i class="fa fa-wrench" aria-hidden="true"></i>
                    <span class="title">শাখা ও পদবি সংশোধন সেটিং </span>
                </a>
            </li>
        <?php } ?>
	    <li>
		    <a href="<?= $this->request->webroot; ?>DeleteOrganogram">
			    <i class="fa fa-trash" aria-hidden="true"></i>
			    <span class="title">অফিস কাঠামো মুছুন</span>
		    </a>
	    </li>
	    <li>
            <a href="<?= $this->request->webroot; ?>cron/officeListDistrictWise"><i class="fa fa-building-o" aria-hidden="true"></i>
                <span class="title"> জেলা ভিত্তিক অফিস তালিকা </span>
            </a>
	    </li>
    </ul>
</li>

<li class=" ">
    <a href="javascript:;"> <i class="icon-briefcase"></i> <span class="title">ভৌগলিক তথ্য ব্যবস্থাপনা</span>
        <span class="arrow "></span></a>
    <ul class="sub-menu">
        <li>
            <a href="javascript:;">
                <i class="icon-globe"></i>
                <span class="title">ভৌগলিক তথ্য কাঠামো</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                <li>
                    <a href="<?= $this->request->webroot; ?>treeViews/union_tree"><i class="icon-tag"></i>
                        <span class="title">ইউনিয়ন কাঠামো</span>
                    </a>
                </li>
                <li>
                    <a href="<?= $this->request->webroot; ?>treeViews/thana_tree"><i class="icon-tag"></i>
                        <span class="title">থানা কাঠামো</span>
                    </a>
                </li>
                <li>
                    <a href="<?= $this->request->webroot; ?>treeViews/postoffice_tree"><i class="icon-tag"></i>
                        <span class="title">পোস্ট অফিসের কাঠামো</span>
                    </a>
                </li>
                <li>
                    <a href="<?= $this->request->webroot; ?>treeViews/municipality_ward"><i class="icon-tag"></i>
                        <span class="title">পৌরসভা ওয়ার্ড</span>
                    </a>
                </li>
                <li>
                    <a href="<?= $this->request->webroot; ?>treeViews/city_ward"><i class="icon-tag"></i>
                        <span class="title">সিটি কর্পোরেশন কাঠামো</span>
                    </a>
                </li>
            </ul>
        </li>
        <li>
            <a href="<?= $this->request->webroot; ?>geoDivisions/"> <i class="icon-briefcase"></i> <span class="title">বিভাগ</span>
            </a>
        </li>
        <li>
            <a href="<?= $this->request->webroot; ?>geoDistricts/"> <i class="icon-briefcase"></i> <span class="title">জেলা</span>
            </a>
        </li>
        <li>
            <a href="<?= $this->request->webroot; ?>geoUpazilas/index"> <i class="icon-briefcase"></i> <span
                    class="title">উপজেলা</span>
            </a>
        </li>
        <li>
            <a href="<?= $this->request->webroot; ?>geoThanas/index"> <i class="icon-briefcase"></i> <span
                    class="title">থানা</span>
            </a>
        </li>
        <li>
            <a href="<?= $this->request->webroot; ?>geoPostOffices/index"> <i class="icon-briefcase"></i> <span
                    class="title">পোস্ট অফিস</span>
            </a>
        </li>
        <li>
            <a href="<?= $this->request->webroot; ?>geoCityCorporations/index"> <i class="icon-briefcase"></i> <span
                    class="title">সিটি কর্পোরেশন</span>
            </a>
        </li>
        <li>
            <a href="<?= $this->request->webroot; ?>geoCityCorporationWards/index"> <i class="icon-briefcase"></i> <span
                    class="title">সিটি কর্পোরেশন ওয়ার্ড</span>
            </a>
        </li>
        <li>
            <a href="<?= $this->request->webroot; ?>geoMunicipalities/index"> <i class="icon-briefcase"></i> <span
                    class="title">পৌরসভা</span>
            </a>
        </li>
        <li>
            <a href="<?= $this->request->webroot; ?>geoMunicipalityWards/index"> <i class="icon-briefcase"></i> <span
                    class="title">পৌরসভা ওয়ার্ড</span>
            </a>
        </li>
    </ul>
</li>

<li class=" ">
    <a href="javascript:;"> <i class="icon-briefcase"></i> <span class="title">দপ্তর ব্যবস্থাপনা</span>
        <span class="arrow "></span></a>
    <ul class="sub-menu">
        <li>
            <a href="<?= $this->request->webroot; ?>officeSettings/ministries"> <i class="icon-briefcase"></i> <span
                    class="title">মন্ত্রণালয় / বিভাগ</span>
            </a>
        </li>
        <li>
            <a href="<?= $this->request->webroot; ?>officeSettings/layerSetup"> <i class="icon-briefcase"></i> <span
                    class="title">অফিসের স্তর</span>
            </a>
        </li>
        <li>
            <a href="javascript:;">
                <i class="icon-globe"></i>
                <span class="title">মৌলিক অফিস ব্যবস্থাপনা</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                <li>
                    <a href="<?= $this->request->webroot; ?>officeSettings/officeOrigins"><i class="icon-tag"></i>
                        <span class="title">মৌলিক অফিস</span>
                    </a>
                </li>
                <li>
                    <a href="<?= $this->request->webroot; ?>officeOrigin/officeOriginUnits"><i class="icon-tag"></i>
                        <span class="title">মৌলিক অফিস শাখা</span>
                    </a>
                </li>
                <li>
                    <a href="<?= $this->request->webroot; ?>officeOrigin/originUnitOrganograms"><i class="icon-tag"></i>
                        <span class="title">মৌলিক অফিসের সাংগঠনিক কাঠামো</span>
                    </a>
                </li>
                <li>
                    <a href="<?= $this->request->webroot; ?>officeOrigin/officeUnitCategories"><i class="icon-tag"></i>
                        <span class="title">মৌলিক অফিস শাখার প্রকার</span>
                    </a>
                </li>
            </ul>
        </li>
        <li>
            <a href="javascript:;">
                <i class="icon-globe"></i>
                <span class="title">অফিসসমূহ</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                <li>
                    <a href="<?= $this->request->webroot; ?>officeManagement/officeManagement"><i class="icon-tag"></i>
                        <span class="title">অফিস</span>
                    </a>
                </li>
                <li>
                    <a href="<?= $this->request->webroot; ?>officeManagement/officeUnitManagement"><i
                            class="icon-tag"></i>
                        <span class="title">অফিস শাখা</span>
                    </a>
                </li>
                <li>
                    <a href="<?= $this->request->webroot; ?>officeManagement/officeUnitOrganogramManagement"><i
                            class="icon-tag"></i>
                        <span class="title">অফিস শাখার কাঠামো</span>
                    </a>
                </li>
            </ul>
        </li>
    </ul>
</li>
<li class=" ">
    <a href="javascript:;"> <i class="icon-briefcase"></i> <span class="title">কর্মকর্তা / কর্মচারী ব্যবস্থাপনা</span>
        <span class="arrow "></span></a>
    <ul class="sub-menu">
        <li>
            <a href="<?= $this->request->webroot; ?>employeeBatches/"> <i class="icon-briefcase"></i> <span
                    class="title">ব্যাচ</span>
            </a>
        </li>
        <li>
            <a href="<?= $this->request->webroot; ?>employeeCadres/"> <i class="icon-briefcase"></i> <span
                    class="title">ক্যাডার</span>
            </a>
        </li>
        <li>
            <a href="javascript:;">
                <i class="icon-globe"></i>
                <span class="title">কর্মকর্তা</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                <li>
                    <a href="<?= $this->request->webroot; ?>employeeRecords/employeeManagement"><i class="icon-tag"></i>
                        <span class="title">অফিস ও কর্মকর্তা ব্যবস্থাপনা</span>
                    </a>
                </li>
                <li>
                    <a href="<?= $this->request->webroot; ?>employeeRecords/employeeRecordList"><i class="icon-tag"></i>
                        <span class="title">কর্মকর্তাসমূহ</span>
                    </a>
                </li>
                <li>
                    <a href="<?= $this->request->webroot; ?>employeeRecords/employeeRoleHistories"><i
                            class="icon-tag"></i>
                        <span class="title">কর্মকর্তাবৃন্দের কর্ম ইতিহাস</span>
                    </a>
                </li>
            </ul>
        </li>
    </ul>
</li>
<li class=" ">
    <a href="javascript:;"> <i class="fa fa-bookmark"></i> <span class="title"> অন্যান্য </span>
        <span class="arrow "></span></a>
    <ul class="sub-menu">
        <li class=" ">
            <a href="<?= $this->request->webroot; ?>nothiUpdateList"><i class="fa fa-book" aria-hidden="true"></i>
                <span class="title"> নথির নতুন আপডেট/রিলিজ নোট তালিকা </span>
            </a>
        </li>
        <li class=" ">
            <a href="<?= $this->request->webroot; ?>cron/cronLogReport"><i class="fa fa-tachometer" aria-hidden="true"></i>
                <span class="title">ক্রন রিপোর্ট লগ </span>
            </a>
        </li>
        <li class=" ">
            <a href="<?= $this->Url->build(['_name' => 'permitted-report-module-user']) ?>"><i class="fa fa-lock" aria-hidden="true"></i>
                <span class="title"> রিপোর্ট মডুলে অনুমতিপ্রাপ্তদের তালিকা </span>
            </a>
        </li>
        <li class=" ">
            <a href="<?= $this->Url->build(['_name' => 'global-message']) ?>"><i class="fa fa-envelope" aria-hidden="true"></i>
                <span class="title"> সার্বজনীন বার্তা </span>
            </a>
        </li>
    </ul>
</li>
<li class=" ">
    <a href="javascript:;">  <span aria-hidden="true" class="icon-magnifier-add"></span><span class="title"> ট্র্যাকিং </span>
        <span class="arrow "></span></a>
    <ul class="sub-menu">
        <li class=" ">
            <a href="<?= $this->Url->build(['_name' => 'potrojari-audit']); ?>"><i class="fa fa-tachometer" aria-hidden="true"></i>
                <span class="title"> অফিসের পত্রজারি  ট্র্যাকিং </span>
            </a>
        </li>
        <li class=" ">
            <a href="<?= $this->Url->build(['_name' => 'unit-audit']); ?>"><i class="fa fa-clipboard" aria-hidden="true"></i>
                <span class="title"> শাখা পরিবর্তন ট্র্যাকিং </span>
            </a>
        </li>
        <li class=" ">
            <a href="<?= $this->Url->build(['_name' => 'designation-audit']); ?>"><i class="fa fa-tachometer" aria-hidden="true"></i>
                <span class="title"> পদবি পরিবর্তন ট্র্যাকিং </span>
            </a>
        </li>
        <li class=" ">
            <a href="<?= $this->Url->build(['_name' => 'redundant-nid-check']); ?>"><i class="fa fa-book" aria-hidden="true"></i>
                <span class="title"> প্রয়োজনাতিরিক্ত এন-আই-ডি ট্র্যাকিং </span>
            </a>
        </li>
        <li class=" ">
            <a href="<?= $this->Url->build(['_name' => 'officewise-potrojari-pending']); ?>">
               <span aria-hidden="true" class="icon-book-open"> </span>
               অফিসভিত্তিক পত্রজারি পেন্ডিং
            </a>
        </li>
        <li class=" ">
            <a href="<?= $this->Url->build(['_name' => 'office-id-wise-potrojari-pending']); ?>">
               <span aria-hidden="true" class="icon-book-open"> </span>
               অফিস আইডি দিয়ে পত্রজারি পেন্ডিং সার্চ
            </a>
        </li>
        <li class=" ">
            <a href="<?= $this->Url->build(['_name' => 'office-wise-login-track']); ?>">
                <i class="fa fa-laptop"></i>
               অফিসভিত্তিক  লগইন ট্র্যাক
            </a>
        </li>
        <li class=" ">
            <a href="<?= $this->request->webroot; ?>Cron/potrojariPending">
                <i class="fa fa-laptop"></i>
                অফিসভিত্তিক অপেক্ষমাণ পত্রজারি
            </a>
        </li>
    </ul>
</li>
<li class=" ">
    <a href="javascript:;"> <i class="fa fa-area-chart" aria-hidden="true"></i><span class="title"> রিপোর্টসমূহ </span>
        <span class="arrow "></span></a>
    <ul class="sub-menu">
        <li class=" ">
            <a href="<?= $this->request->webroot; ?>cron/reportTillNow"><i class="fa fa-tachometer" aria-hidden="true"></i>
                <span class="title">অদ্যাবধি অফিসের রিপোর্ট </span>
            </a>
        </li>
        <li class=" ">
            <a href="<?= $this->Url->build(['controller' => 'Cron','action' => 'nagorikAbedon'])?>"><i class="fa fa-cloud-download" aria-hidden="true"></i>
                <span class="title"><?= __('Nagorik Dak')?> এক্সপোর্ট  </span>
            </a>
        </li>
        <li class=" ">
            <a href="<?= $this->Url->build(['controller' => 'Report','action' => 'generateMonitoringDashboardMonthlyReport'])?>"><i class="fa fa-cloud-download" aria-hidden="true"></i>
                <span class="title"> মনিটরিং ড্যাশবোর্ড মাসিক রিপোর্ট
                </span>
            </a>
        </li>
        <li class=" ">
            <a href="<?= $this->request->webroot; ?>layerwise_archive_nothi_list">
                <i class="fa fa-laptop"></i>

                <span class="title"> স্তরভিত্তিক নিষ্পত্তিযোগ্য অনিষ্পন্ন নথি
                </span>
            </a>
        </li>
        <li class=" ">
            <a href="<?= $this->request->webroot; ?>layerwise_archive_done_nothi_list">
                <i class="fa fa-laptop"></i>

                <span class="title"> স্তরভিত্তিক নিষ্পত্তিযোগ্য নথি
                </span>
            </a>
        </li>
        <li class=" ">
            <a href="<?= $this->request->webroot; ?>Performance/districtWiseOfficeReportFromSuperman">
                <i class="fa fa-laptop"></i>

                <span class="title"> জেলা পর্যায়ের অফিসভিত্তিক প্রতিবেদন
                </span>
            </a>
        </li>
    </ul>
</li>

<?php
//please add new url to projapoti_static.php getSuperAdminAccess function. Otherwise normal user can access it.
?>
