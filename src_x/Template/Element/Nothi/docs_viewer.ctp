<!--<script type="text/javascript" src="<?/*= $HOME */?>coreui/kukudocs/docxjs/DocxJS.bundle.min.js"></script>
<script type="text/javascript" src="<?/*= $HOME */?>coreui/kukudocs/celljs/CellJS.bundle.min.js"></script>
<script type="text/javascript" src="<?/*= $HOME */?>coreui/kukudocs/slidejs/SlideJS.bundle.min.js"></script>
<script type="text/javascript" src="<?/*= $HOME */?>coreui/kukudocs/pdfjs/PdfJS.bundle.min.js"></script>-->


<script>
  $(window).on("load", function(e) {
    var $body = $("body");

    var $loading = $("#parser-loading");
    var $modal = $("#modal");
    var $docxjsWrapper = $("#docxjs-wrapper");

    var instance = null;

    var getInstanceOfFileType = function(file) {
      var fileExtension = null;

      if (file) {
        var fileName = file.name;
        fileExtension = fileName.split(".").pop();
      }

      return fileExtension;
    };

    var documentParser = function(file) {
      var fileType = getInstanceOfFileType(file);

      if (fileType) {
        if (fileType == "docx") {
          instance = window.docxJS = window.createDocxJS ?
            window.createDocxJS() :
            new window.DocxJS();
        } else if (fileType == "xlsx") {
          instance = window.cellJS = window.createCellJS ?
            window.createCellJS() :
            new window.CellJS();
        } else if (fileType == "pptx") {
          instance = window.slideJS = window.createSlideJS ?
            window.createSlideJS() :
            new window.SlideJS();
        } else if (fileType == "pdf") {
          instance = window.pdfJS = window.createPdfJS ?
            window.createPdfJS() :
            new window.PdfJS();
          instance.setCMapUrl("./cmaps/");
        }

        if (instance) {
          $loading.show();
          instance.parse(
            file,
            function() {
              $docxjsWrapper[0].filename = file.name;
              afterRender(file, fileType);
              $loading.hide();
            },
            function(e) {
              if (!$body.hasClass("is-docxjs-rendered")) {
                $docxjsWrapper.hide();
              }

              if (e.isError && e.msg) {
                alert(e.msg);
              }

              $loading.hide();
            },
            null
          );
        }
      }
    };

    var afterRender = function(file, fileType) {
      var element = $docxjsWrapper[0];
      $(element).css("height", "calc(100% - 65px)");

      var loadingNode = document.createElement("div");
      loadingNode.setAttribute("class", "docx-loading");
      element.parentNode.insertBefore(loadingNode, element);
      $modal.show();

      var endCallBackFn = function(result) {
        if (result.isError) {
          if (!$body.hasClass("is-docxjs-rendered")) {
            $docxjsWrapper.hide();
            $body.removeClass("is-docxjs-rendered");
            element.innerHTML = "";

            $modal.hide();
            $body.addClass("rendered");
          }
        } else {
          $body.addClass("is-docxjs-rendered");
          // console.log("Success Render");
        }

        loadingNode.parentNode.removeChild(loadingNode);
      };

      if (fileType === "docx") {
        window.docxAfterRender(element, endCallBackFn);
      } else if (fileType === "xlsx") {
        window.cellAfterRender(element, endCallBackFn);
      } else if (fileType === "pptx") {
        window.slideAfterRender(element, endCallBackFn, 0);
      } else if (fileType === "pdf") {
        window.pdfAfterRender(element, endCallBackFn, 0);
      }
    };

    var url = "http://hasan/html/kukudoc/file.docx";

    var xhr = new XMLHttpRequest();
    xhr.open("GET", url);
    xhr.responseType = "blob";
    xhr.addEventListener("load", function() {
      xhr.response.name = url;
      documentParser(xhr.response);
    });
    console.log(xhr);
    xhr.send();
  });
</script>
