<div class="nothi-content h-100">
  <div class="send-note" hidden>
    <div class="card h-100 mb-0">
      <div class="card-header">
        <div class="d-flex">
          <button class="btn btn-warning"><i class="fa fa-long-arrow-left"></i> আগত নথিতে ফেরৎ যান</button>
          <h3 class="ml-3 mb-0 font-weight-normal">পরবর্তী কার্যক্রমের জন্য প্রেরণ করুন</h3>
        </div>
      </div>
      <div class="card-body flex-fill">
        <div class="card h-100 mb-0">
          <div class="card-header">
            পরবর্তী কার্যক্রমের জন্য প্রেরণ করুন
          </div>
          <div class="card-body flex-fill">
            <h5 class="card-title">Card title</h5>
            <h6 class="card-subtitle mb-2 text-muted">Card subtitle</h6>
            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's
              content.</p>
            <a href="#" class="card-link">Card link</a>
            <a href="#" class="card-link">Another link</a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="d-flex mb-0 flex-column h-100">
    <div class="p-2 align-items-center">
      <?= $this->element('Nothi/nothi_note_title') ?>
    </div>
    <div class="flex-fill overflow-hidden">
      <div class="d-flex h-100">
        <div class="nothi-sidebar d-flex flex-column">
          <?= $this->element('Nothi/nothi_note_list') ?>
        </div>
        <style>
          .note-wrap{width:21cm;min-height:29.7cm;box-shadow:0 0 10px #000;margin:auto;padding:80px;}
          .note-margin{border:1px dashed #ccc;height:100%;padding:30px;}
          .note-margin p{margin:0;}
          .note-margin ol{padding-left:15px;}
        </style>
        <script>
          $(function (e) {
            $('.editor').each(function (e) {

              var editor = $(this);


              var fieldData = editor.find('.view_data p').map(function (e) {
                return '<div class="col-12 mb-3">\n' +
                  '<input type="text" class="form-control" name="field' + e + '" value="' + this.innerText + '" placeholder="First name">\n' +
                  '</div>';
              }).get();


              editor.find('.field_data').append(fieldData);

              editor.find('.view_data').click(function (e) {
                $('.find_data').hide();
                editor.find('.find_data').show();
                editor.toggleClass('editing');
                e.stopPropagation();
              })
              $(document).click(function (e) {
                editor.removeClass('editing');
                e.stopPropagation();
              })
              editor.find('.find_data').click(function (e) {
                e.stopPropagation();
              })

              editor.find('form').submit(function (e) {
                e.preventDefault();
                var formData = $(this).serializeArray();
                console.log(formData);


                var viewData = formData.map(function (v, e) {
                  return '<p>' + v.value + '</p>';
                });

                editor.find('.view_data').html(viewData);
                editor.find('.find_data').hide();
              })


            })
          })
        </script>
        <div class="flex-fill">
          <div class="note-template p-5 h-100 pScroll">
            <div class="note-wrap">
              <div class="note-margin">
                <div class="editor">
                  <div class="find_data">
                    <div class="card">
                      <div class="card-header py-0 px-2">
                        অনুমোদনকারী
                      </div>
                      <div class="card-content p-3">
                        <form method="post">
                          <div class="row field_data">

                          </div>
                          <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-save"></i> Success
                          </button>
                        </form>
                      </div>
                    </div>
                  </div>
                  <div class="view_data text-center">
                    <p>গণপ্রজাতন্ত্রী বাংলাদেশ সরকার</p>
                    <p>প্রকল্প পরিচালকের দপ্তর</p>
                    <p>মৎস্য ও প্রাণিসম্পদ খাতে ই-সেবা চালুকরণ শীর্ষক প্রকল্প</p>
                    <p>মৎস্য ও প্রাণিসম্পদ মন্ত্রণালয়</p>
                    <p><u>www.mofl.gov.bd</u></p>
                  </div>
                </div>
                <div class="d-flex justify-content-between">
                  <div class="editor">
                    <div class="find_data">
                      <div class="card">
                        <div class="card-header py-0 px-2">
                          অনুমোদনকারী
                        </div>
                        <div class="card-content p-3">
                          <form method="post">
                            <div class="row field_data">

                            </div>
                            <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-save"></i> Save
                            </button>
                          </form>
                        </div>
                      </div>
                    </div>
                    <div class="sharok-no view_data"><p>স্বারক নং-৩৩.৯০.৩০০.০০০.১৩৯.৪৪৩৪৩.১৯-৭২</p></div>
                  </div>
                  <div class="sharok-date">তারিখঃ ১০/০৬/২০১৯</div>
                </div>
                <div class="text-center">
                  <p><strong><u>সভার নোটিশ</u></strong></p>
                </div>
                <div class="note-content editor">
                  <div class="find_data">
                    <div class="card">
                      <div class="card-header py-0 px-2">
                        অনুমোদনকারী
                      </div>
                      <div class="card-content p-3">
                        <form method="post">
                          <div class="row field_data">

                          </div>
                          <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-save"></i> Success
                          </button>
                        </form>
                      </div>
                    </div>
                  </div>
                  <div class="view_data">
                    <p>সেবাপ্রার্থী কর্তৃক অনলাইনে সেবার আবেদন দাখিল, আবেদনের সর্বশেষ অবস্থা জানা এবং অনলাইনে সেবা
                      প্রাপ্তির উদ্দেশ্যে এই প্ল্যাটফরম পস্তুত করা হয়েছে। নাগরিক, ব্যবসায়ী, সরকারি ও বেসরকারি প্রতিষ্ঠান
                      এবং সরকারি কর্মকর্তা-কর্মচারিগণ এই প্ল্যাটফরমের সুবিধা গ্রহণ করতে পারবেন। সরকারি দপ্তরের অন্যান্য
                      সেবাসমূহ পর্যায়ক্রমে এই প্ল্যাটফরমে সংযোজন করা হবে।</p>
                    <p>সেবাপ্রার্থী কর্তৃক অনলাইনে সেবার আবেদন দাখিল, আবেদনের সর্বশেষ অবস্থা জানা এবং অনলাইনে সেবা
                      প্রাপ্তির উদ্দেশ্যে এই প্ল্যাটফরম পস্তুত করা হয়েছে। নাগরিক, ব্যবসায়ী, সরকারি ও বেসরকারি প্রতিষ্ঠান
                      এবং সরকারি কর্মকর্তা-কর্মচারিগণ এই প্ল্যাটফরমের সুবিধা গ্রহণ করতে পারবেন। সরকারি দপ্তরের অন্যান্য
                      সেবাসমূহ পর্যায়ক্রমে এই প্ল্যাটফরমে সংযোজন করা হবে।</p>
                  </div>
                </div>
                <div class="d-flex justify-content-end">
                  <div class="note-sender text-center">
                    <img src="<?= $this->request->webroot ?>coreui/img/sign.jpg" width="100">

                    <div class="editor">
                      <div class="find_data">
                        <div class="card">
                          <div class="card-header py-0 px-2">
                            অনুমোদনকারী
                          </div>
                          <div class="card-content p-3">
                            <form method="post">
                              <div class="row field_data">

                              </div>
                              <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-save"></i> Save
                              </button>
                            </form>
                          </div>
                        </div>
                      </div>
                      <div class="view_data">
                        <p>(পুলকেশ মন্ডল)</p>
                        <p>প্রকল্প পরিচালক</p>
                        <p>ও</p>
                        <p>সিনিয়র সহকারী প্রধান</p>
                        <p>ফোনঃ 02-9125454</p>
                        <p>ই-মেইলঃ emailserve@nothi.com</p>
                      </div>
                    </div>

                  </div>
                </div>
                <div class="default content">
                  <u>বিতরণ (কাযার্থে):</u>
                  <ol>
                    <li>সেবাপ্রার্থী কর্তৃক অনলাইনে সেবার আবেদন দাখিল</li>
                    <li> কর্তৃক অনলাইনে সেবার আবেদন দাখিল</li>
                    <li> অনলাইনে সেবার আবেদন দাখিল</li>
                    <li>সেবাসমূহ পর্যায়ক্রমে এই প্ল্যাটফরমে সংযোজন করা হবে</li>
                    <li> এই প্ল্যাটফরমে সংযোজন করা হবে</li>
                    <li> প্ল্যাটফরমে সংযোজন করা হবে</li>
                  </ol>
                </div>
                <div class="default content">
                  <u>বিতরণ (কাযার্থে):</u>
                  <ol>
                    <li>সেবাপ্রার্থী কর্তৃক অনলাইনে সেবার আবেদন দাখিল</li>
                    <li> কর্তৃক অনলাইনে সেবার আবেদন দাখিল</li>
                    <li> অনলাইনে সেবার আবেদন দাখিল</li>
                    <li>সেবাসমূহ পর্যায়ক্রমে এই প্ল্যাটফরমে সংযোজন করা হবে</li>
                    <li> এই প্ল্যাটফরমে সংযোজন করা হবে</li>
                    <li> প্ল্যাটফরমে সংযোজন করা হবে</li>
                  </ol>
                </div>
                <div class="note-content">
                  <p>সেবাপ্রার্থী কর্তৃক অনলাইনে সেবার আবেদন দাখিল, আবেদনের সর্বশেষ অবস্থা জানা এবং অনলাইনে সেবা
                    প্রাপ্তির উদ্দেশ্যে এই প্ল্যাটফরম পস্তুত করা হয়েছে। নাগরিক, ব্যবসায়ী, সরকারি ও বেসরকারি প্রতিষ্ঠান
                    এবং সরকারি কর্মকর্তা-কর্মচারিগণ এই প্ল্যাটফরমের সুবিধা গ্রহণ করতে পারবেন। সরকারি দপ্তরের অন্যান্য
                    সেবাসমূহ পর্যায়ক্রমে এই প্ল্যাটফরমে সংযোজন করা হবে।</p>
                  <p>সেবাপ্রার্থী কর্তৃক অনলাইনে সেবার আবেদন দাখিল, আবেদনের সর্বশেষ অবস্থা জানা এবং অনলাইনে সেবা
                    প্রাপ্তির উদ্দেশ্যে এই প্ল্যাটফরম পস্তুত করা হয়েছে। নাগরিক, ব্যবসায়ী, সরকারি ও বেসরকারি প্রতিষ্ঠান
                    এবং সরকারি কর্মকর্তা-কর্মচারিগণ এই প্ল্যাটফরমের সুবিধা গ্রহণ করতে পারবেন। সরকারি দপ্তরের অন্যান্য
                    সেবাসমূহ পর্যায়ক্রমে এই প্ল্যাটফরমে সংযোজন করা হবে।</p>
                </div>
                <div class="d-flex justify-content-end">
                  <div class="note-sender text-center">
                    <img src="<?= $this->request->webroot ?>coreui/img/sign.jpg" width="100">
                    <p>(পুলকেশ মন্ডল)</p>
                    <p>প্রকল্প পরিচালক</p>
                    <p>ও</p>
                    <p>সিনিয়র সহকারী প্রধান</p>
                    <p>ফোনঃ 02-9125454</p>
                    <p>ই-মেইলঃ emailserve@nothi.com</p>
                  </div>
                </div>
                <div class="default content">
                  <u>বিতরণ (কাযার্থে):</u>
                  <ol>
                    <li>সেবাপ্রার্থী কর্তৃক অনলাইনে সেবার আবেদন দাখিল</li>
                    <li> কর্তৃক অনলাইনে সেবার আবেদন দাখিল</li>
                    <li> অনলাইনে সেবার আবেদন দাখিল</li>
                    <li>সেবাসমূহ পর্যায়ক্রমে এই প্ল্যাটফরমে সংযোজন করা হবে</li>
                    <li> এই প্ল্যাটফরমে সংযোজন করা হবে</li>
                    <li> প্ল্যাটফরমে সংযোজন করা হবে</li>
                  </ol>
                </div>
                <div class="default content">
                  <u>বিতরণ (কাযার্থে):</u>
                  <ol>
                    <li>সেবাপ্রার্থী কর্তৃক অনলাইনে সেবার আবেদন দাখিল</li>
                    <li> কর্তৃক অনলাইনে সেবার আবেদন দাখিল</li>
                    <li> অনলাইনে সেবার আবেদন দাখিল</li>
                    <li>সেবাসমূহ পর্যায়ক্রমে এই প্ল্যাটফরমে সংযোজন করা হবে</li>
                    <li> এই প্ল্যাটফরমে সংযোজন করা হবে</li>
                    <li> প্ল্যাটফরমে সংযোজন করা হবে</li>
                  </ol>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script src="<?= $this->request->webroot ?>coreui/js/split.min.js"></script>
<script src='<?= $this->request->webroot ?>coreui/node_modules/froala-editor/js/froala_editor.min.js'></script>
<script src="<?= $this->request->webroot ?>coreui/js/onucched_attachment_uploader.js" crossorigin></script>
