<div class="card border-0 h-100 mb-0 notansho_note_view">

  <div class="card-header px-3 py-2 ch-style-2">
    <h4 class="mb-0 text-truncate note_title">
      <?= $this->request->data('tab_title'); ?>
    </h4>
    <h6 class="mb-0 note_datetime"><?= $this->request->data('tab_date'); ?>
    </h6>
  </div>

  <div class="card-body overflow-hidden flex-basis p-0 d-flex">
    <div class="onucched_number_list pScroll">
      <?= $this->element('Nothi/note/nothi_note_onucched_numbers', [
        'nothi_note_onucched_numbers' => json_encode($onucched_json)
      ]); ?>
    </div>
    <div class="onucched_content pScroll p-3 w-100">
      <?= $this->element('Nothi/note/nothi_note_onucched', [
        'nothi_note_onucched' => json_encode($nothi_note_onucched)
      ]); ?>
    </div>
  </div>

</div>
