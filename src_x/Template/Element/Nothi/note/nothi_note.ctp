<div class="h-100 d-flex flex-column notansho_note_view_tab" id="notansho_note">
    <nav>
      <ul class="nav nav-tabs" id="nav-tab" role="tablist">
        <li class="nav-item">
          <a class="nav-item nav-link show active" id="noteNavLink_firsts" data-toggle="tab" href="#noteNav_first">অনুচ্ছেদ
          <button class="ml-3 btn-sm btn btn-transparent text-dark"><i class="fa fa-external-link"></i></button>
          </a>
        </li>
      </ul>
    </nav>

    <div class="tab-content flex-basis overflow-hidden" id="nav-tabContent">
      <div class="tab-pane p-0 h-100 fade show active" id="noteNav_first" role="tabpanel">

        <?= $this->element('Nothi/note/note_sample'); ?>

      </div>
    </div>


</div>
<script src="<?= $this->request->webroot ?>coreui/js/onucched_list_tabview.js" crossorigin></script>
