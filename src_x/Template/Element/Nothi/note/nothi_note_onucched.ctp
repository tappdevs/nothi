<div class="onucched_list">

  <ul class="list-group">
    <?php
    $nothi_note_onucched = json_decode($nothi_note_onucched, true);
    foreach (json_decode($nothi_note_onucched) as $note_onucched) {
      if ($note_onucched->onucched_type == 'user') {
        echo $this->element('Nothi/note/nothi_note_onucched_user', [
          'note_onucched' => $note_onucched
        ]);
      } else {
        echo $this->element('Nothi/note/nothi_note_onucched_system', [
          'note_onucched' => $note_onucched
        ]);
      }
    }
    ?>
  </ul>
</div>