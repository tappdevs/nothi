<div class='mulpotro_upload_gallery upload_gallery'>
  <div class="pScroll">
    <div class='attach_list'></div>
  </div>
</div>
<div class='input-group pb-3'>
  <div class='custom-file'>
    <input
        type='file' multiple
        class='custom-file-input file_uploader' id='mulpotro_upload'
        aria-describedby='mulpotro_upload_file'>
    <label class='custom-file-label' for='mulpotro_upload'>

    </label>
  </div>
  <div class='input-group-append'>
    <button
        class='btn btn-outline-secondary' type='button'
        id='mulpotro_upload_file'>Upload
    </button>
  </div>
</div>

