<div class="accordion text-center" id="accordionExample">
  <?php
  $nothi_note_onucched_numbers = json_decode($nothi_note_onucched_numbers, true);
  // print_r($nothi_note_onucched_numbers);
  if (count($nothi_note_onucched_numbers) > 0) {
    foreach ($nothi_note_onucched_numbers as $key => $onucched_number) {
      ?>
      <div class="card mb-0">
        <button class="btn btn-light dropdown-toggle" type="button" data-toggle="collapse"
            data-target="#onuNo<?= __($key + 1) ?>"
            aria-expanded="true" aria-controls="collapseOne">
          <?= __($key + 1) ?>
        </button>

        <div id="onuNo<?= __($key + 1) ?>" class="collapse" aria-labelledby="headingOne"
            data-parent="#accordionExample">
          <div class="list-group">
            <?php foreach ($onucched_number as $number) { ?>
              <a href="#" class="list-group-item list-group-item-action p-2"><?= __($number) ?></a>
            <?php } ?>
          </div>
        </div>
      </div>
    <?php }
  } ?>
</div>