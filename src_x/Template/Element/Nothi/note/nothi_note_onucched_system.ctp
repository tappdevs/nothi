<li class="list-group-item list-group-item-warning text-danger">
  <div class="d-flex justify-content-between align-items-center border-bottom px-2 py-1">
    <h5 class="mb-0">অনুচ্ছেদ ১.০
      <small>(<?= __($note_onucched->onucched_date) ?>)</small>
    </h5>
    <div class="btn-group" role="group" aria-label="Basic example">
      <button type="button" class="btn btn-primary btn-sm">
        <i class="fs1 a2i_nt_dakdraft4"></i>
      </button>
      <button type="button" class="btn btn-success btn-sm">
        <i class="fs1 a2i_gn_edit2"></i>
      </button>
      <button type="button" class="btn btn-danger btn-sm">
        <i class="fs1 a2i_gn_delete2 "></i>
      </button>
    </div>
  </div>
  <div class="p-2 bg-white">
    <?= $note_onucched->onucched_body ?>
  </div>
</li>