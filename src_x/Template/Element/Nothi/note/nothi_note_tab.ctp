<div class="h-100 d-flex flex-column" id="notansho_note_tab">

  <div class="note_list_tab">

    <nav class="hidden">
      <ul class="nav nav-tabs" id="nav-tab" role="tablist">
      </ul>
    </nav>

    <div class="tab-content flex-basis overflow-hidden" id="nav-tabContent">
      <div class="tab-pane p-0 h-100 fade show active" id="nav-home" role="tabpanel">

      </div>
    </div>

    <?php $this->element('Nothi/note/nothi_note');?>
  </div>

</div>
<script src="<?= $this->request->webroot ?>coreui/js/onucched_list_tabview.js" crossorigin></script>
