<div class="card-header bg-violate-o text-white d-flex justify-content-between align-items-center p-1">
  <h4 class="mb-0 ml-1">পত্রাংশ</h4>
  <div class="btn-group">
    <button type="button" class="btn btn-primary"><i class="fa fa-print"></i></button>
    <button type="button" class="btn btn-success"><i class="fa fa-eye"></i></button>
  </div>
</div>