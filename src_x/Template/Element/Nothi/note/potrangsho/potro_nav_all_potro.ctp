<li class="nav-item">
  <a class="nav-link d-flex align-items-center" data-toggle="tab" href="#potro_tab_3"
    role="tab"
    aria-controls="messages">
    <i class="icon-envelope-letter fs-20"></i>
    <aside class="ml-3 line-height-1">
      <strong class="text-dark font-weight-normal">সকল পত্র</strong>
      <span class="badge">23</span>
    </aside>
  </a>
</li>