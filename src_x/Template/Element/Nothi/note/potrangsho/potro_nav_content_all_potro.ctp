<div class="tab-pane" id="<?= $potro_tab_id ?>" role="tabpanel">
  <?= $this->element('Nothi/note/potro_attachment_navigator',['potros_nav_id'=>'nav_all_porto','potros_group_nav'=>$potros_group_nav_all]); ?>

  <div class="potroview">
    <?= $this->element('Nothi/note/potrangsho/potro_potro_template', ['potro_layout' => 'image']); ?>
    <?= $this->element('Nothi/note/potrangsho/potro_potro_template', ['potro_layout' => 'pdf']); ?>
    <?= $this->element('Nothi/note/potrangsho/potro_potro_template', ['potro_layout' => 'text']); ?>
  </div>
</div>