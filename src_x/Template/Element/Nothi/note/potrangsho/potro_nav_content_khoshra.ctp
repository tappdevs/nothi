<div class="tab-pane active h-100 d-flex flex-column p-0" id="<?= $potro_tab_id ?>" role="tabpanel">
  <?= $this->element('Nothi/note/potro_attachment_navigator', ['potros_nav_id' => 'nav_khoshra_porto', 'potros_group_nav' => $potros_group_nav_all]);
  ?>
  <!--/potransho-->

  <div class="load_docs" hidden>
    <?php
    //    pr($potros_group_nav_all);
    foreach ($potros_group_nav_all as $key1 => $potros_group_nav) {
      foreach ($potros_group_nav->potros as $key2 => $potros) {
        echo '<input data-id="' . $key1 . 'a' . $key2 . '" data-url="' . $potros->file_name . '" type="hidden">';
      }
    }
    ?>
  </div>

  <div class="potroview tab-content flex-basis h-100 d-flex flex-column">
    <div class="navButtons d-flex justify-content-between">
      <i class="fa fa-chevron-circle-left fa-2x doc-previus" data-url=""></i>
      <i class="fa fa-chevron-circle-right fa-2x doc-next" data-url=""></i>
    </div>
    <div id="modal-containers" class="h-100">
      <div id="docxjs-wrapper" style="width:100%;height:100%;" data-url=""></div>
    </div>
  </div>
  <!--/potransho-->
</div>
<?php
// pr($potros_group_nav_all);
// echo $this->element('Nothi/docs_viewer');
?><!--
<script type="text/javascript" src="<?/*= $this->request->webroot */?>coreui/kukudocs/docxjs/DocxJS.bundle.min.js"></script>
<script type="text/javascript" src="<?/*= $this->request->webroot */?>coreui/kukudocs/celljs/CellJS.bundle.min.js"></script>
<script type="text/javascript" src="<?/*= $this->request->webroot */?>coreui/kukudocs/slidejs/SlideJS.bundle.min.js"></script>
<script type="text/javascript" src="<?/*= $this->request->webroot */?>coreui/kukudocs/pdfjs/PdfJS.bundle.min.js"></script>-->


<script>
  $(window).on("load", function (e) {
    var $body = $("body");

    var $loading = $("#parser-loading");
    var $modal = $("#modal");
    var $docxjsWrapper = $("#docxjs-wrapper");

    var instance = null;

    var getInstanceOfFileType = function (file) {
      var fileExtension = null;

      if (file) {
        var fileName = file.name;
        fileExtension = fileName.split(".").pop();
      }

      return fileExtension;
    };

    var documentParser = function (file) {
      var fileType = getInstanceOfFileType(file);

      if (fileType) {
        if (fileType == "docx") {
          instance = window.docxJS = window.createDocxJS ?
            window.createDocxJS() :
            new window.DocxJS();
        } else if (fileType == "xlsx") {
          instance = window.cellJS = window.createCellJS ?
            window.createCellJS() :
            new window.CellJS();
        } else if (fileType == "pptx") {
          instance = window.slideJS = window.createSlideJS ?
            window.createSlideJS() :
            new window.SlideJS();
        } else if (fileType == "pdf") {
          instance = window.pdfJS = window.createPdfJS ?
            window.createPdfJS() :
            new window.PdfJS();
          instance.setCMapUrl("./cmaps/");
        }

        if (instance) {
          $loading.show();
          instance.parse(
            file,
            function () {
              $docxjsWrapper[0].filename = file.name;
              afterRender(file, fileType);
              $loading.hide();
            },
            function (e) {
              if (!$body.hasClass("is-docxjs-rendered")) {
                $docxjsWrapper.hide();
              }

              if (e.isError && e.msg) {
                alert(e.msg);
              }

              $loading.hide();
            },
            null
          );
        }
      }
    };

    var afterRender = function (file, fileType) {
      var element = $docxjsWrapper[0];
      $(element).css("height", "calc(100% - 65px)");

      var loadingNode = document.createElement("div");
      loadingNode.setAttribute("class", "docx-loading");
      element.parentNode.insertBefore(loadingNode, element);
      $modal.show();

      var endCallBackFn = function (result) {
        if (result.isError) {
          if (!$body.hasClass("is-docxjs-rendered")) {
            $docxjsWrapper.hide();
            $body.removeClass("is-docxjs-rendered");
            element.innerHTML = "";

            $modal.hide();
            $body.addClass("rendered");
          }
        } else {
          $body.addClass("is-docxjs-rendered");
          // console.log("Success Render");
        }

        loadingNode.parentNode.removeChild(loadingNode);
      };

      if (fileType === "docx") {
        window.docxAfterRender(element, endCallBackFn);
      } else if (fileType === "xlsx") {
        window.cellAfterRender(element, endCallBackFn);
      } else if (fileType === "pptx") {
        window.slideAfterRender(element, endCallBackFn, 0);
      } else if (fileType === "pdf") {
        window.pdfAfterRender(element, endCallBackFn, 0);
      }
    };

    var loadFirst = $('.load_docs input:first-child');

    var loadURL = loadFirst.data('url');
    var loadID = loadFirst.data('id');
    var loadNextID = loadFirst.next().data('id');
    var loadNext = loadFirst.next().data('url');

    $('#docxjs-wrapper').attr('data-url', loadURL).attr('data-id', loadID);
    $('.doc-next').attr('data-url', loadNext).attr('data-id', loadNextID);

    var xhr = new XMLHttpRequest();
    xhr.open("GET", loadURL);
    xhr.responseType = "blob";
    xhr.addEventListener("load", function () {
      xhr.response.name = loadURL;
      documentParser(xhr.response);
    });
    console.log(xhr);
    xhr.send();
  });


  $(function (e) {

    $('.doc-next').on('click', function (e) {

      $('#modal-containers').html('<div id="docxjs-wrapper"></div>');

      var getInstanceOfFileType = function (file) {
        var fileExtension = null;

        if (file) {
          var fileName = file.name;
          fileExtension = fileName.split(".").pop();
        }

        return fileExtension;
      };

      var documentParser = function (file) {
        var fileType = getInstanceOfFileType(file);

        if (fileType) {
          if (fileType == "docx") {
            instance = window.docxJS = window.createDocxJS ?
              window.createDocxJS() :
              new window.DocxJS();
          } else if (fileType == "xlsx") {
            instance = window.cellJS = window.createCellJS ?
              window.createCellJS() :
              new window.CellJS();
          } else if (fileType == "pptx") {
            instance = window.slideJS = window.createSlideJS ?
              window.createSlideJS() :
              new window.SlideJS();
          } else if (fileType == "pdf") {
            instance = window.pdfJS = window.createPdfJS ?
              window.createPdfJS() :
              new window.PdfJS();
            instance.setCMapUrl("./cmaps/");
          }

          if (instance) {
            $loading.show();
            instance.parse(
              file,
              function () {
                $docxjsWrapper[0].filename = file.name;
                afterRender(file, fileType);
                $loading.hide();
              },
              function (e) {
                if (!$body.hasClass("is-docxjs-rendered")) {
                  $docxjsWrapper.hide();
                }

                if (e.isError && e.msg) {
                  alert(e.msg);
                }

                $loading.hide();
              },
              null
            );
          }
        }
      };

      var afterRender = function (file, fileType) {
        var element = $docxjsWrapper[0];
        $(element).css("height", "calc(100% - 65px)");

        var loadingNode = document.createElement("div");
        loadingNode.setAttribute("class", "docx-loading");
        element.parentNode.insertBefore(loadingNode, element);
        $modal.show();

        var endCallBackFn = function (result) {
          if (result.isError) {
            if (!$body.hasClass("is-docxjs-rendered")) {
              $docxjsWrapper.hide();
              $body.removeClass("is-docxjs-rendered");
              element.innerHTML = "";

              $modal.hide();
              $body.addClass("rendered");
            }
          } else {
            $body.addClass("is-docxjs-rendered");
            // console.log("Success Render");
          }

          loadingNode.parentNode.removeChild(loadingNode);
        };

        if (fileType === "docx") {
          window.docxAfterRender(element, endCallBackFn);
        } else if (fileType === "xlsx") {
          window.cellAfterRender(element, endCallBackFn);
        } else if (fileType === "pptx") {
          window.slideAfterRender(element, endCallBackFn, 0);
        } else if (fileType === "pdf") {
          window.pdfAfterRender(element, endCallBackFn, 0);
        }
      };


      var $body = $("body");

      var $loading = $("#parser-loading");
      var $modal = $("#modal");
      var $docxjsWrapper = $("#docxjs-wrapper");

      var instance = null;

      var loadID = $(this).data('id');

      var loadFirst = $('.load_docs').find('input[data-id="' + loadID + '"]');

      var loadURL = loadFirst.attr('data-url');

      var loadPreviusID = loadFirst.prev().data('id');
      var loadNextID = loadFirst.next().data('id');

      var loadPreviusUrl = loadFirst.prev().data('url');
      var loadNextUrl = loadFirst.next().data('url');

      console.log(loadID);

      // return false;


      $('#docxjs-wrapper').attr('data-url', loadURL);

      $('.doc-previus').attr('data-id', loadPreviusID).attr('data-url', loadPreviusUrl);
      $('.doc-next').attr('data-id', loadNextID).attr('data-url', loadNextUrl);

      var xhr = new XMLHttpRequest();
      xhr.open("GET", loadURL);
      xhr.responseType = "blob";
      xhr.addEventListener("load", function () {
        xhr.response.name = loadURL;
        documentParser(xhr.response);
      });
      console.log(xhr);
      xhr.send();


    })
  })
</script>
