<div class="tab-pane" id="potro_tab_2" role="tabpanel">
  <?= $this->element('Nothi/note/potro_attachment_navigator',['potros_nav_id'=>'nav_noter_porto','potros_group_nav'=>$potros_group_nav_noter]); ?>

  <div class="potroview">
    <?= $this->element('Nothi/note/potrangsho/potro_potro_pdf'); ?>
    <?= $this->element('Nothi/note/potrangsho/potro_potro_text'); ?>
    <?= $this->element('Nothi/note/potrangsho/potro_potro_image'); ?>
  </div>
</div>