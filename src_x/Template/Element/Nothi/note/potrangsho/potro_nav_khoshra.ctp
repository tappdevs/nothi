<li class="nav-item">
  <a class="nav-link active d-flex align-items-center" data-toggle="tab" href="#<?= __($potro_tab_id) ?>"
    role="tab"
    aria-controls="home">
    <i class="fa fa-inbox fs-20"></i>
    <aside class="ml-3 line-height-1">
      <strong class="text-dark font-weight-normal">খসড়া</strong>
      <span class="badge">12</span>
    </aside>
  </a>
</li>