<div id_ar="98" id_en="10" id_bn="১০" data-content-type="text"
  class="attachmentsallRecord active first">
  <div style="background-color: #a94442; padding: 5px; color: #fff;">
    <div class="pull-left" style="padding-top: 0px;    white-space: nowrap;">
      অফিস স্মারক টেম্পলেট
    </div>
    <div class="pull-right text-right">
      <button type="button" class="btn   btn-sm btn-warning btn-resizecss"
        onclick="createClonePotrojari(64,11)" data-toggle="tooltip"
        title="পত্রজারি ক্লোন করুন">
        <span class="glyphicon glyphicon-copy"></span></button>
      <a data-id="98" title="প্রিন্ট করুন" href="javascript:void(0)"
        class="btn    green btn-sm btn-printall"><i class="fs1 a2i_gn_print2"></i></a>
    </div>
    <div style="clear: both;"></div>
  </div>
  <div
    style="background-color: #fff; max-width:950px; min-height:815px;  margin:0 auto; page-break-inside: auto; ">
    <!--Office_sarok-->
    <div class="templateWrapper">
      <div class="row customFontSize" style="font-size:13pt">
        <div class="col-md-3 col-xs-3 col-sm-3  text-center"><span class="canedit"
            id="left_slogan" data-type="textarea">শিক্ষা নিয়ে গড়ব দেশ ,শেখ হাসিনার বাংলাদেশ</span>
        </div>
        <div class="col-md-6 col-xs-6 col-sm-6 text-center customFontSize"
          style="font-size:13pt"><span class="canedit" id="gov_name" data-type="text">গণপ্রজাতন্ত্রী বাংলাদেশ সরকার </span>
          <br> <span class="canedit" id="office_ministry" data-type="text">জনপ্রশাসন মন্ত্রণালয়</span>
          <br> <span class="canedit" id="offices" data-type="text">জেলা প্রশাসকের কার্যালয়, জামালপুর</span>
          <br><span class="canedit" id="web_url_or_office_address" data-type="text">http://www.jamalpur.gov.bd</span><br><span
            class="canedit" id="office_address" data-type="text">জেলা প্রশাসকের কার্যালয়জামালপুর, বাংলাদেশ</span>
        </div>
        <div class="col-md-3 col-xs-3 col-sm-3 customFontSize"
          style="font-size:13pt; text-align: center;"><span class="canedit"
            id="right_slogan" data-type="textarea">শিক্ষা নিয়ে গড়ব দেশ ,শেখ হাসিনার বাংলাদেশ</span><br>
          <span class="canedit" id="potro_priority" data-type="text"> </span> <br> <span
            class="canedit" id="potro_security" data-type="text"> </span></div>
      </div>
      <br>
      <div style="text-align:right;">
        <div class="row customFontSize" style="font-size:13pt">
          <div class="col-md-8 col-xs-8 col-sm-8 customFontSize text-left"
            style="font-size:13pt"> স্মারক নম্বর: <span class="canedit" id="sharok_no"
              data-type="text">০১.০১.০১০২.০০০.০১.০০৩.১৮.৩</span></div>
          <div class="col-md-4 col-xs-4 col-sm-4 customFontSize"
            style="font-size:13pt;">
            তারিখ: <span class=" bangladate"
              style="border-bottom:1px solid #000!important; margin-left: 3px; position: relative; top: -5px;">২৩ পৌষ ১৪২৫</span><br><span
              class="canedit" id="sending_date"
              data-type="date"> ০৬ জানুয়ারি ২০১৯</span></div>
        </div>
      </div>
      <div class="row customFontSize" style="font-size:13pt">
        <div class="col-md-1 col-xs-1 col-sm-1 customFontSize text-left"
          style="font-size:13pt"> বিষয়:
        </div>
        <div class="col-md-11 col-xs-11 col-sm-1 customFontSize text-justify"
          style="font-weight:bold; font-size:13pt"><span class="canedit" id="subject"
            data-type="text">অফিস স্মারক টেম্পলেট</span></div>
      </div>
      <div class="row customFontSize" style="font-size:13pt">
        <div class="col-md-1 col-xs-1 col-sm-1 customFontSize text-left"
          style="font-size:13pt"> সূত্র:
        </div>
        <div class="col-md-11 col-xs-11 col-sm-11 text-left  customFontSize"
          style="font-size:13pt"><span class="canedit" id="reference"
            data-type="textarea"> ১২৩৪৫</span></div>
      </div>
      <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12 text-left"><textarea name="" id="note"
            style="display: none;">য়াআআআআআআআআআআআআসদা&lt;br&gt;পত্রের বিষয়বস্তু -----------------------------------------------------------------&lt;br&gt;০২। নির্দেশনা/উদ্দেশ্য/বর্ণনা------------------------------------------------------------------------------------------------------------------------------------------------------------- নির্দেশক্রমে অনুরোধ করা হইল।</textarea>
          <div data-original-title="Enter notes" data-toggle="manual"
            data-type="wysihtml5" data-pk="1" id="noteView" class="editable"
            tabindex="-1" style="display: inline;">য়াআআআআআআআআআআআআসদা<br>পত্রের
            বিষয়বস্তু
            -----------------------------------------------------------------<br>০২।
            নির্দেশনা/উদ্দেশ্য/বর্ণনা-------------------------------------------------------------------------------------------------------------------------------------------------------------
            নির্দেশক্রমে অনুরোধ করা হইল।
          </div>
        </div>
      </div>
      <div style="text-align: center;">
        <div class="row customFontSize" style="font-size:13pt">
          <div
            class="col-md-5 col-xs-5 col-sm-5 col-md-offset-7 col-xs-offset-7 col-sm-offset-7 text-center">
                                          <span id="sender_signature" data-type="text"
                                            data-original-title="Enter signature here"><img
                                              src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJYAAAAyCAYAAAC+jCIaAAAfsklEQVR4Xu1cB3hUZdZ+p5dk0gMkgSQImCC9uwiIShHFxkpZAamri6iIWP5dFgEFVBZcmriKlABKFUF0hQVCbwEUBYQgJEAaJX2STKb/zzn33pk7k4kJgrv/v2T2CZuZubn3+873fue85z3nU+F2u92oe9VZ4DZbQFEHrNts0brbsQXqgFUHhN/EAnXA+k3MWnfTOmDVYeA3sUAdsH4Ts9bdtA5Y/1UYkBJ8RcBZBUr/A19560a57cAKOHj5h+JMAnx067P5L7/DLylDZFaPTX3Q8muh410h4Q43d59bB5bn+W5+dmBgyQZVB6xaw79WEqOPk6I3tA5k5ABA4GtlEBR/pY+lq/3XT8ErqoCC71n7128ILO+MFe46YNV+SbxXErBcLhfKysqg1Wqh1+truI1ocw8I/OLCLwDLc2PPsrk9XoJA9W8ClmzA/oOVj5DxVM3u+TWWvsP+xul0oqCgCOvXb4Beb0BS0t2ICI+AwWhESEgogoKCoNOpoFQCUv1EQfZmuweIHbUBVjU2/jcASxqw4CJ9JuQPeynseXYBTxtyF+81xP9P1PjM5SbDRU0zdjicuHL5CiZOnITcvDwk3X03YmJioVZrYTAEwxQUDFOoESFhwdDp9dCotdBp9TAYDNBoVFCrlexpNFottFodNBoNlEolFEoFXE4nVColNBotdDr6XgO1Wg2lwrumXnC6eZ3pXmq1RmBcNUTGmwiFAjr4X88/0u4A3C6AdpjNZofD4YDL5eaBGI16aDRq0YY+FNNLCWsaZU0r8B/8/rcEFtkzNzcXY0aPxbFjx9ChQwe0a9sOFZZKFBcVw1xqBrEMtVbFoNJo9DDog2AKCoJGq4JK4YLD7YBaY4DeYIROr4NKpWZgOe12KAEY9FqYTMEIDQ2B0WiEWq2CSqmCW+EWHB/xZjeg0WoQHV0PzZol3U5gSaASaZ7HpQKVlZXMAUrNZSgtKUNpqZnf2+0O3hEJCbFo0KA+goODodPpoVT6Ql0Y/M0Rw/8gjv6tjyZ+ReAZMWIk0s+l48WXx2PMmFEoKzPjypVL+PnCeeRm5yIrKwfm0grARShQwO1yweVywGatQEmZGXYHAKUKCvJWbGs3nHYb7FYbFHDCaNAjNCQYQUHBUGvUMBqMgNLFy6JQquBSKBEUHMrA7t//MRFYv7xmtfRYQrYhOSrCstPhgNXqwKkfT2PXrj3Yv/8AMjIyGGjktZxOB9xuO5onNcXDD/dFzwcfxD0tWvDuUKlUPmTwZuP3v3V1/4MPI29YUWHB+BdexA8nv8dzzz+HcS+Mq0qf/NfYTaHLDZed1sgKh9MBl9sFl6eRxQWXw45KSyUcdhscdivsdjsnCuSidHotyJ2RD1Ao1VCotNBoDTCZQmAymWqVJd4EsIT50LPN5jL889tt+Pyzz3Hy+xPQqjVo06Y12rVvj4aNYlEvOorBU1JchMyLl3Diu+9xJTsHCYmN8cK4cbivW1dZeBRid92rqgUIHEQrpk+fhm3btuGZZ4bi1VdfrR2wpNsRyAIaVwAffSk4DNlV4nJ49CsxJgrZoRAfa1qzmoFF5EnMNK5fz8e+/QfwxcYvcPxEGpo1uQv33dcVnTp1QmJiY5hCQpkIaojgKRTstWhXXLt2DTt27MTqVasRn5CIpUs/RWRkBBQU5GsxSMEuokbjYWZy5cUvrQ6g4fhrQoKBvJJIVf1GXA3PFz7Wpi3mHZaU+cr2R7X381lk+ZzE23kWWMimCVgzZszEt9/+E0OGDKkdsLxs2BtpyM4y2Scg3KqqQiLJkqbqQaHPuvnfiwFYYz+WONGMS5exadNmrF23FsWFBXjyycfRr18/NG3SFNHR0dAbdFASUjzexztKcrOHDx/G3DlzcPKHH/Hll5vRokVzzkRqApabgX2zXq3qHnXLtTQI/EFgpsI4ZbTRA13fre4PLPEZcuD9KmD5uhNpA5DZiW5czMjEBx98gP379+OppwbgpZdfRmhYGEcECl2U3dE4lSolf6ZmmiHMSFoKrzTlHeCvAZZwV69thRzf9zNprWoBLMBmt2Ht2vVYvHgx8vOv44knnsCYMaPRtGkzn5BWPQAU+PHHH7FgwXxs2fIVVq78DD163McZowAsdl0BXuSuJWBJ10jGET2G56/k4VQOLAkAKq8LUoheuLpSgc9I/AFVzVBv08fCPhaE0cKCQixauBCbv9qCK5ez0bx5C3Tr1gP169eHSq2B3WGDQwSWSqGASqWAingRcyMljMHBLDWolAQBwQMSZwoKNkGv10GnVUOr0XCE0eg00Op07CB0Wh2LsXSNAFRBKLPabCguLkZRYRFzP1qbkBATYmIaIDwi3NdqNXosAFevXsXMmbOwdetX6N3rIUx8dRKSkpJYE5GHE19gSW5eWJjz53/GkiWfYNmy5Vi4cDEee+wRBAcbfQTUqnHbCyy50Cpc90vAklMGSjVIhJGBl1dPlEoCE5Bq624eWLsEjkI/lP1KY/fV6ESv6L/TRVfiG54F/uLxWC4XCgoKMX/e33Ho4AGcPn0WkWHR6NixE2Li4tj2VofVqyW6neTi4BKJutPtglYfxPNwOu2wlFeiwmKFQqGGISgYBqMeWrUCboeDZSKlWgm9Uc/ZO2WJQUYjyw+S9kUmI1pDWMjLy0NZuQV2mxWN72qMPn16M2/2mXttgJWWloZZs2bh2tWrmPjqKxgw4GkRVIJUIMqePiFLcJlSTFYwz9q4cT2mTp3OP8OHD0NYWIiIcpEmBiTxLnHAwo6TvxQKOSp8vZ53kjQ6ApYIJnYI9InkCWkxFULGJJJZ0nnoxxMiPU9WwFJRgfz8fJ4PSSsEqMjISMTHN+KMiUDmFY3JPMK4qqPQ3vn4Aov+ggTSSxkZOHEsDYs/XIyw0EiMHftHNG+RLOiGbgd7Jnq5XE64KMNzCDqi1W6H1eZAeYUFFksFe5nikjI4nG4Q6Gh2bocNZaXFKC0phdVug0JFAqgaalFA5SyRbMGkXclUx2KxcuZP763WSjSKj8cj/R9F7969b95jbdz4BebN+zsiIyIwbdo0tG7TxhO/2XBSpiC7tWRIVmwBlJeXYceOHRg//kWMGf0cXp7wIqKiIjzAImOUl5fDbDbzMtLOoZKFVqP2oTqeRVOADVheXiHqZnYeEynJYWGhQl1NoWDpo7ysnNNu0neMQUa+t0ajZBG3stKGoqJCmMtIB3LD6XRBp9PwdfR8g5FUbA2DjsLAmdOncfDgQRw/foLVcKrhNYiJwfBhwziRCQ0N9excWhiLxQKbzSaGFj3zIPnOpt/pe7qWvBAvLF0jzprmW1pWipdfehk3rudj4sSJ6N2nlzesixtF2NVeD+mTOor7zzciiB86BY9FzyfAEa9zOmwoKSmBuVQAnMsNqMUQGmKiMKpnJd/uIIFMVPZ1Oh/HUjPHArBmzRrMn78AcXFxeOftd5DcPAkK5in08oYBuUeRfInXtTtx9GgaxowZi/vvfwBTp05h4VR6Xbt2Hdv/tQNfb/0KGo0OPXs+yDyscWIC1FqhFME2FJFFBsjKzsH+vQewO3U3u2cisDTGwYMHo0OHdlBrNDiXfhF7dx9EevrPqKwsR5d7O6FPnweRmNgIVquNAZKSshKnTp2GXq1lj2QKMSG5RXN05Yy3Ixo3TmDD03UpK1JgrbSgXft2aNGiJY9n4cIP+f2MGW+jdevWnjGSUEyk+9y5dLRp0wYtWrRAeHgYg4detKCU2KSnp6O0tJQFytjYWERFRXoASM91uBV44403cCztCEaOeBbjxo2Dm0KfZ8spOWq4xa4Gr6/1jRpesi2BUObF5V0PgegBuziZ56iO0kquojahcO3adZg/fz5nf1OnTUPbNq2hVHpTdWEn+IYqObCYEblcSEs7iqFDhyMmJg6rVq1AYmKih5scTTuG1157HcePHYVaLZQeGjdugqef/j0GDx6Ips3uEobsdqOkpAybNm3CqlWrcOF8OmJiolnuoLh/7NhxhEfUw9NPD4HD4cK/tn+DoqJraNqsGZPPn89nICGhCYYNHcYead26z3HxwmkMGTQY8QkJcDldyLuah1OnT+FsejoUChXu7/kArBYLzp79CX1798ITTz3J9yOAFBQUYNyfXoKl0oLZ789Ch44dPMDKyspiD5OamsrePjkpGd179EDPng8g2BSMffv2YeeOfzHFcLrcrA9GREaiV6/e6Nnzfly+nI3Lly5DpdFi584duHQpA0MGD8SMmW+zHeh/UluLwBnpnRw+kiQiEnd5UPbQCOE7oYAjrBp95Vk/6bMqwJJlh+6qyVetPBaFsNmz/8YZ4F+nTEGXzp1lwPL1WFXALnqY/PwCfLl5M/785psYPWo0XnvjNdSrFy0CS4HDhw5h4iuv4NKlTMycNYsnSyA5c+YnNGqUgBEjR+Khh+5HebkFM2a8i9SdOxBs0KBfv77o3bcvwiKjOOydOX0KHy1ewmk6hZS2bVpi2PAhSL5H8CTff3cCq1d/hszMbFgq7agXFYGxo59Fv0f7cdijF4Umc5kZGRcvYs+evUhZuYq5y6CBgzFq1Ei0at2KwwFtluzsXAx48mlk52She4/uiG8Uj5at7sFDDz0InU6HRQvn4XJGJnrc/wDOnk3H9ydPorikBDqDjklyz/u7o2P7TggLC0d2Tg5Sd+/BoUOH4HS5EBsTg2ZNmzB3OvPTWfx84SKaJTXHpEmT8Gj/hwXJAU4Q1ySfJYDDm/0qmEdKn7O2JEKrCl0VP/CXUGSp2S95LN7wPhSreh1LzgNogWfNmgmLpRKTJ09Gt25dZeU9ObB8OxeEi9ywWe1c8pn7wQfIyc7C4kUfokOnDlw6YOqvUOLkyZN4d+YMliW+3fYtgoNDkHf1KnZs38Gqs84QhFGjx+L61WuYP38uOnfsgMef6M/ibExsHHMrWuhScwn+PnceVn/2GZonN8dLL43H77rei5DQMJ458am0o4eRsmIltm3fiUaN4jH5L5Px5IDH+R5sIcrOXG5cyryELZu3YMGCBUySiUu98cbr6NXrIRj0BgbC56s/x/x589G4SWPO2Ih3FBcVISn5bowYMYKBfPzocTw78lmUmEuxMmU5vtq8BVqNDg1iYvHe+7PRuk1L7kiosFiwYcNGzH5/NopLijBq1Gg89eQTzDNzc3KwZs1a7Ny9F61atcIzQ4ey96JMjqIHAYvrMPwjrbQ3qgRs/PPFQsB38jvU4nLPJdV6LDmwiANMnz4dWVnZeP3119G//6OyYrKvvO9f7a+02PDjqVPcU7Rr107mP88/N5ZJLtlAaJtRIDMjAyuWL8emL77Atu3bERMbyzvy4oULbFBKIOo3aIQycxnUaifGvzAOffr2ZbGQcy63QOazs3Pw4cIP8cWmL5DULAkDBw1GXKM4rnM1bdoEkZHhXJzdnZqK5StW4syZc+jVqw/eemsyIiIjmKfRfqistOLQwcNY89kaBBkMiIiKwurPVmHYsKEYMXIEwsPCceTwEfxt9mz2vL369kGTJk2Rk5OHrVu3oqS0BHP+9jcG57o1G/DW9Mns1datXYO8rGzc3SwJe/YfxrMjRmLosMGccJRXVGBlykpMmzads7wJE17Byy+/iJCQEJSXleKTTz7FnLlzUVxcgi5d7sXMmTMZlKQHinm1H7DkUPDNqKsDye3q1qhVKLx+/Treeedt7Nu3H8OHj+DJcqYUoG2VvIbNasWly5dx4/oN5OXkYdfuPdi7bx+H0sl//gvad2jLfIYyKklhoIwrdVcqpkyZgk+WLEH79u2E0OQGTp86jUWLPsTyFSn8/s3/eR3PPjucBVpJz6Ks79y589iyeSu2bPkSF37+GQnxiWjXjjyjnltNOnbsgMcefwTx8Q05VU5N3cOLdf78ed44jz7aD6aQYA7P2Vm5WLNmHb7eshVz5rzPguSIkc8y9/nTn55Hw7iGOH7sODZ9+SWGDn0GLTk8GnDw4CEsW7ochUWFmDfvAxw5nIalnyzFR0s+xI7t27Fv7z60adsWnTp1xiuvTkLze5Kx+MOFiI2NYcCsWr0a7856lyUC8nhTp05FmLh5qKwzZ85c7N69hz+b+tZ0PDP0D4iOpuxa2uC1A1BtgMV8q4Y6rgRE/+tqBSzyBAsXLWBxs22bdpg75wNERUdwbPfNCoVMpyA/nwvUR44cwZVLl1FaZobT5eReocSERCQ3T8Yjjz6Clq1acrcDZXz0dz/99BOTe+Ix5Bmi60Xz/C0VFhw8cAATJkxARmYm3p76DgYNGYj4xAT+npz/jfx8/OOjT/Dxx58gLy+ba5b9+j6M3r37cE/T1m++YW+yYOF89O3bB+HhoSguLsVXX32Nt6ZMQcNGjTBv3hy0bCnwpx9+OIWVK1bixIkTeH/2uzh27Dt8suQfGPrMMxjyhyEIDQnF1dw85ORd5XGSJ7XZ7RzmiJdRJklhePu325G6OxXLVizDkUNHcfjQEYSEhSMkzIT1G9biscf74bVX32TphVqNjqUdQ0rKCvzw44/4/YCn8Kdx49lGBJzz589h+bJl+PjjJRw6J7w0ESNHj+Dn/5r24UDgqlpT9QWq//f+wJIAViOwpBLD9u3bMG/+PBQWlGDmO++i54PdoVYLcV2uOpPHImCtX7ceFy5eQHRkBBIbJ7J4eCO/AGlH07BjZyriYhti8pS/omvXe9lI5IoyMy9h1KgxvHtnzZqBJk2aCEKlm9T/PKSsWI5Z776Pnt0ewKuvT8LvunbhBjTiFzm5uZjxzkzs3bMP2dlXYNTrMOGVVzBu/HiQx9305WbMnTsHc+bMQf/+/REZEc5knrzcR4s/wldfb8HEiRMwaNAQxDRogMuXL2P9ug1Yv349EhITcC79PDp07IgXx7+AmNgGOJ52nL1TYWER1Bod90YVFhRBp9Ug1GTi/iaNWsVaF/U4zV+0CGqVBv/atg17dqdyqGvZuiXG/PE5nq/QDCm0I+Xl5XDS0rRpU+4IoY5RelVaKnDw0EGkLF/OUslrk94QPKVBf0ttSHJwBAKW/2dyQN4ysChbW7RoETZs2ISnBwzCOzOnwmDQcTruX86gh0n9PfKdRKArKS7BiuUrsWDhPDw98A8YOHAgazfksYqLizBv3gIcOnQAKSlL0blzZ+gMwgECp82Os2fPYNDgIbh8+QrGjn0Ozz//HFq0bM55D0kQ2779Bms+X4srWVmwVdjRvVt3dL3vPpy7kM7lqPj4eMycNRP33NPcoyVR2CaPtnvPHiQnJ7HHok3gcjiRl5eLkz+c5A0SG9cI3bvTZlIxD9q1cxeio+qhSZO7kHEpE0ePHEar1m0xfPhwdOrQnj10Xm4u62IuuNGydVuQuFhZYUZpSSFzwsh6sSIdEAvGXNMRWpMkwVTQn0VlinkklWfKYbfbEBpOepfaU52SFrym8OXvqaoLZ9J1UulKfl/pMylcejyVGDpr7bFoQtSV8N57sznlT0lZjrZtW3OhUso4vMj2SL1sFCLC5K2oETDj4gVs3rQZu3bvQkQ46U9NYTSaYLNZUWkpR2ZmJvILc7F0yad4uN/DiK4fLWSyLkH5XrZsGd577z32Zn/5y5+5IM7Ac7pQUVGOoqJ8WCoqcSH9Is6fS+falsNmRWx8LPo//jgSEu/iYqtQahEb4lxOLqoSbxSKtkLRlcBO2hcV4alQS56VRM9dO3fi6JGjnO2qVUroDGrc07wlOnbujLiGDQXVX+ylkprrqAfdc+iBS0rUkUDSAJVXxHDjUc7FsXFJzCtiSkGJP6J/pKKHmABJKf9vASx/MFZUVCIv7yonWBTGhXDtfdUILOFSASgXLlzAsqVL8Y+PP8Xo0aPx5ptvIDo6iiV/Rq5MViOD0WIfOpyGHTtScfLkd8jOusKLTyWWdm1ao1v3Hqz7aDVa2B1OmM2lLEISvyCt5t7f3YvwSLFqLraRkARB6jyVZ7p1785Kt1RgFnYRteW6+RmlJSWoqKjgcQWZTKhXv57oqeQLKYzcV4fxVva8iymo2IJnpfqawBtJliDQUHZKJ2fod+HlJxqKgqa0w72yjwAcX5lI9s5P7ZayP9/2Ff9lvzUCL78ba3pmM/ILC1FQVMwdFyX5xbjwcwY3cFqsFXjwoR7449jRXDOtPccSn0JDpbrXvr17uPEsv6AQCxcsxL33doExOEgElsd58oYiL/TpkqVYuXIVMjIvIioiAknJyeja9Xfo0rkL7mpyF0zBJibvtLNpEgX5Bbh2/RqSkpIRGRUphgqxo583qos5E+lFoSEhXPfjHlqfEYgKsr+9xfee3ix5xlNV2RVzXu9NCLjcklLdgVD2nG44nLSpqFzjRFl5OZxuSlwEHkSFZYedvnPwfO126jhwMUipBYZ+7DYnc6jKSgvcDjsULmrzdrONSPKkAjQ9Q+hWUHDotFZaeQORKFxRUcYdDQqujgjdF1wSEx2gkj4DoKHPlFQKUkJBMgsJruJpHN4afGLHjcL8AmTl5qKo1AxLuQUuqwNqhQpFJcVQapTo3rMbRo0cgaioKE8SUaPH8gpkwm+kE23auBGz3n0PjzzyKCZOmojmyck8eA4tjAGhW5HC54F9e3Hw4AGUV5SjceO7+JRHq1YtER4RKYYCCQSSF5H4hBccvJBcZhAMRT8+OzxAc6F0TVVsyXczLYUQDsm70qKTB6JFJzGYQjgp4LRIlRYLysvKhCKtBAKHi0HicjhgdzmY91gtVv4hjmlzunGjqBAOtxMhwSZQz5TD5oTT4RI7bMm7WuF0Cr1NdJ3d4YLd7uLzBDaLBWqFEzo1AUYNFdUY1bT4SjicgNVqZ6WdgEVVB5XCDa1KxTqdWgXoDVpo9Gqo6NiXWgUlpTkuBTR0JEwBBOsNgmNVq6Ex6KGlagJ3GAkJGVECg07PdjEToBScqkGv0fD5RqGHS4uYuFjcfffdTAEkTn1zwHKDDXblyhVMmzqV25THvfgiBg8axG0jSoV36RWiFuxy2lFWVsoLRw1mWp1Axr0t1r8MLGnnCAFZ4hy+7oWATDuYCC9947A7YGdvYGdwWCzl/BkBiA4WOGx29ghWWyWDiD4jHcxmc4LGTX9jLi1HpbWS22tcLjvMxWaUm0sFTkTyCBFpakGxO6AVtr9w7M3ugMvm4I2l1htQbrfBqXDDqNWC8j4K4SqFCnqDATqjDmqtErZKC3sX4n2kobNHddP/u2DQqfiHzwVqdVw3JA/D1yjowRJDU7CHoUY/auzT6zQIop4ryhg1dOSLYEWtLwqoFUr+CeIOEAKWiq+jEhQXgTghEzwWndih01VqrRYanVZ8hnA2kY6ICYvpG3pr1Zosl/Qlck4u/MypU1w0LjabufQwYMBTqF+/HhNUroqI5RypOCrMwJeMesKXfFxVQpK329k37aXQaedmOCLoJdx4ZuNbVpRZmAMR0S4uKsSN/GscVrRqNXsYApa5vIy9KAGeYgtlh7R4ROyF05ngHUgNcWpqsbG7oFNruPnNGGzkRSZuR+WWyHATTOGhvHuNOiOMfL7PCKMpGDpTMNudqnoKt4sdBHl3ulYfRKdfdHDYBXsRmafFp/sSyITPxAMMIuiqFOWkIrGMCgQwocdxs6n5Aj8eFuC/uyHGDqmBx6/RgG7iLXLThpSzkZvwWLI6IO9WBw7uP4Bp06ejtKwczwwbij8MGYJ61PJB6KJRSVmOvBmwyqy9h14F9AcIXqINPC04zHUUuHHjBr7e+jXWrd/AehJ5JUq/KZzxETQHeSM7HE4raz2xDWIQHhbGJJMAYgoNQYP69bjzgPrviSMEBRmEs3VGA0JMwdxtSTqVko5BiQsudBXIF0f0GvKpiHP3Ea49c5MtqudXb/bnbwHJjAI3lJoWfY0lOA+pw8HfkF7UeJ/s/S0QzqQxMFkQ0lBxQSUAyVogZH7Tk+DWpm1GeohcTqBnUej59pt/snCalZOLJ58agLFjxiIxoaFHJxI6MWXTCUCSpa/5Kz+3yuaqBliUnX134hh++P47mLh5TzhiTqkv1deMQUECv1Cp2PvUr1ePPQR3XYp8wXuIU3D9Qkes2BkbiKjz2nr9uNif6m058TgEvxAvkETZrhe+92JNBCt/QOOTvhEW1GsbsXPSwzdF+4qXi3TUd4eSvCGOmefoM1rxnQe9ntX26GPyk2E+rk8aCgPadzvU6LHkl1fRqQCUlJQi7cgRbNi4EcePf4e4RvEYNnw4HnigByLDw6FSq7w6jWzM3vt6d6BgWr8R+n0iF+uI01BNjSQFOhZOIKb2WQo15Lmk/04BhxTiFcQ1xI5XgY0JQA74VHlTksc7kQG9LcxSfU4OEU9SIU7Dxy94UOQ9KyB3Yj5hygdYYioibTo/0MkR6n8yh6foosMZQu+WkGSJ9/O4Uxnh8QxIWBe2jt+xMea6oik81rslYPnuMY+56Rh4+rlz2LlrF/YfOMiiGVX6Y+o34BMlcXGxiBV/hIKqD1w9b6Q97neBD9QClhxkfyDNT84N/e/nafuVb9ZADw0Ytt0+jXAecVP29wH+q01+Md4DP1nkD3inX7BNYCYV8C4ELPG0kxdYdGuvtTyh3QdY0uPl28P3ub6UwGuEm/JY/rb34kzQoKgu9tOZn5CTk4PS0jJkXMxAmdnMtbC27duhS5fOXDe83S/5VGsDrNv9/P/r95OXX6QW7996zLcMLN/+HeJdwukSItYnT/7Aaju1k1AbMvXKkwere/33W+D2AYv5mzx7kHyHLI+tSp/++y18h87wtgFLOtbtqxdIGUsVJnuHmvvOmfZtBpbIhn3sJ8sq6jzWHYOsWwaW96j7L6GmDlF3DKLEid5mYNUB6E4DUHXzrRFYvllfIOAESvbrzHunW6DWwKqut/lON2Dd/ANb4H8BzQIdN1p8oIUAAAAASUVORK5CYII="
                                              alt="signature" width="100"></span> <span id="sender_signature_date"
              style="            display: block;            font-size: 10px;            text-align: center;">৬-১-২০১৯</span>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6 col-xs-6 col-sm-6 text-left"><br><br>
            <div id="office_organogram_id"><span class="canedit"
                style="white-space:pre-wrap;" id="to_div_item" data-type="text">অফিস সহকারী
ব্যবসা-বাণিজ্য শাখা
জেলা প্রশাসকের কার্যালয়, জামালপুর</span></div>
          </div>
          <div
            class="col-md-5 col-xs-5 col-sm-5 col-md-offset-1 col-xs-offset-1 col-sm-offset-1 text-center">
            <div class="row customFontSize" style="font-size:13pt">
              <div class=""><span class="canedit" id="sender_name" data-type="text">মো: শাহাবুদ্দিন খান</span>
              </div>
            </div>
            <div class="row customFontSize" style="font-size:13pt">
              <div class=""><span class="canedit" id="sender_designation"
                  data-type="text">জেলা প্রশাসক</span></div>
            </div>
            <div class="row">
              <div class=""> ফোন: <span class="canedit" id="sender_phone"
                  data-type="text">০১৬৭৩৭১০৮০৯</span></div>
            </div>
            <div class="row">
              <div class=""> ফ্যাক্স: <span class="canedit" id="sender_fax"
                  data-type="text">১২৩৪৫৬</span></div>
            </div>
            <div class="row">
              <div class=""> ইমেইল: <span class="canedit" id="sender_email"
                  data-type="text">kaiser.tushar@yahoo.com</span></div>
            </div>
          </div>
        </div>
      </div>
      <br>
      <div class="row customFontSize" style="font-size: 13pt; display: none;">
        <div class="col-md-8 col-xs-8 col-sm-8 text-left"> স্মারক নম্বর:
          <span class="canedit" id="sharok_no2" data-type="text">০১.০১.০১০২.০০০.০১.০০৩.১৮.৩</span>
        </div>
        <div class="col-md-4 col-xs-4 col-sm-4" style="text-align:right;height:36px;">
          তারিখ: <span class=" bangladate"
            style="border-bottom:1px solid #000!important; margin-left: 3px; position: relative; top: -5px;">২৩ পৌষ ১৪২৫</span><br><span
            id="sending_date_2" style="position: relative; top: -8px;"
            readdate="Sun Jan 06 2019 12:03:34 GMT+0600 (Bangladesh Standard Time)"> ০৬ জানুয়ারি ২০১৯</span>
        </div>
      </div>
      <div class="row customFontSize" style="font-size: 13pt; display: none;">
        <div class="col-md-11 col-xs-11 col-sm-11 text-left"><span class="canedit"
            id="getarthe"
            data-type="text">সদয় অবগতি ও কার্যার্থে প্রেরণ করা হল:</span>
          <br>
          <div id="cc_list_div"></div>
        </div>
      </div>
      <div style="text-align:center;">
        <div class="row" style="display: none;">
          <div
            class="col-md-5 col-xs-5 col-sm-5 col-md-offset-7 col-xs-offset-7 col-sm-offset-7 text-center">
                                          <span id="sender_signature2" data-type="text"
                                            data-original-title="Enter sender signature here"><img
                                              src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJYAAAAyCAYAAAC+jCIaAAAfsklEQVR4Xu1cB3hUZdZ+p5dk0gMkgSQImCC9uwiIShHFxkpZAamri6iIWP5dFgEFVBZcmriKlABKFUF0hQVCbwEUBYQgJEAaJX2STKb/zzn33pk7k4kJgrv/v2T2CZuZubn3+873fue85z3nU+F2u92oe9VZ4DZbQFEHrNts0brbsQXqgFUHhN/EAnXA+k3MWnfTOmDVYeA3sUAdsH4Ts9bdtA5Y/1UYkBJ8RcBZBUr/A19560a57cAKOHj5h+JMAnx067P5L7/DLylDZFaPTX3Q8muh410h4Q43d59bB5bn+W5+dmBgyQZVB6xaw79WEqOPk6I3tA5k5ABA4GtlEBR/pY+lq/3XT8ErqoCC71n7128ILO+MFe46YNV+SbxXErBcLhfKysqg1Wqh1+truI1ocw8I/OLCLwDLc2PPsrk9XoJA9W8ClmzA/oOVj5DxVM3u+TWWvsP+xul0oqCgCOvXb4Beb0BS0t2ICI+AwWhESEgogoKCoNOpoFQCUv1EQfZmuweIHbUBVjU2/jcASxqw4CJ9JuQPeynseXYBTxtyF+81xP9P1PjM5SbDRU0zdjicuHL5CiZOnITcvDwk3X03YmJioVZrYTAEwxQUDFOoESFhwdDp9dCotdBp9TAYDNBoVFCrlexpNFottFodNBoNlEolFEoFXE4nVColNBotdDr6XgO1Wg2lwrumXnC6eZ3pXmq1RmBcNUTGmwiFAjr4X88/0u4A3C6AdpjNZofD4YDL5eaBGI16aDRq0YY+FNNLCWsaZU0r8B/8/rcEFtkzNzcXY0aPxbFjx9ChQwe0a9sOFZZKFBcVw1xqBrEMtVbFoNJo9DDog2AKCoJGq4JK4YLD7YBaY4DeYIROr4NKpWZgOe12KAEY9FqYTMEIDQ2B0WiEWq2CSqmCW+EWHB/xZjeg0WoQHV0PzZol3U5gSaASaZ7HpQKVlZXMAUrNZSgtKUNpqZnf2+0O3hEJCbFo0KA+goODodPpoVT6Ql0Y/M0Rw/8gjv6tjyZ+ReAZMWIk0s+l48WXx2PMmFEoKzPjypVL+PnCeeRm5yIrKwfm0grARShQwO1yweVywGatQEmZGXYHAKUKCvJWbGs3nHYb7FYbFHDCaNAjNCQYQUHBUGvUMBqMgNLFy6JQquBSKBEUHMrA7t//MRFYv7xmtfRYQrYhOSrCstPhgNXqwKkfT2PXrj3Yv/8AMjIyGGjktZxOB9xuO5onNcXDD/dFzwcfxD0tWvDuUKlUPmTwZuP3v3V1/4MPI29YUWHB+BdexA8nv8dzzz+HcS+Mq0qf/NfYTaHLDZed1sgKh9MBl9sFl6eRxQWXw45KSyUcdhscdivsdjsnCuSidHotyJ2RD1Ao1VCotNBoDTCZQmAymWqVJd4EsIT50LPN5jL889tt+Pyzz3Hy+xPQqjVo06Y12rVvj4aNYlEvOorBU1JchMyLl3Diu+9xJTsHCYmN8cK4cbivW1dZeBRid92rqgUIHEQrpk+fhm3btuGZZ4bi1VdfrR2wpNsRyAIaVwAffSk4DNlV4nJ49CsxJgrZoRAfa1qzmoFF5EnMNK5fz8e+/QfwxcYvcPxEGpo1uQv33dcVnTp1QmJiY5hCQpkIaojgKRTstWhXXLt2DTt27MTqVasRn5CIpUs/RWRkBBQU5GsxSMEuokbjYWZy5cUvrQ6g4fhrQoKBvJJIVf1GXA3PFz7Wpi3mHZaU+cr2R7X381lk+ZzE23kWWMimCVgzZszEt9/+E0OGDKkdsLxs2BtpyM4y2Scg3KqqQiLJkqbqQaHPuvnfiwFYYz+WONGMS5exadNmrF23FsWFBXjyycfRr18/NG3SFNHR0dAbdFASUjzexztKcrOHDx/G3DlzcPKHH/Hll5vRokVzzkRqApabgX2zXq3qHnXLtTQI/EFgpsI4ZbTRA13fre4PLPEZcuD9KmD5uhNpA5DZiW5czMjEBx98gP379+OppwbgpZdfRmhYGEcECl2U3dE4lSolf6ZmmiHMSFoKrzTlHeCvAZZwV69thRzf9zNprWoBLMBmt2Ht2vVYvHgx8vOv44knnsCYMaPRtGkzn5BWPQAU+PHHH7FgwXxs2fIVVq78DD163McZowAsdl0BXuSuJWBJ10jGET2G56/k4VQOLAkAKq8LUoheuLpSgc9I/AFVzVBv08fCPhaE0cKCQixauBCbv9qCK5ez0bx5C3Tr1gP169eHSq2B3WGDQwSWSqGASqWAingRcyMljMHBLDWolAQBwQMSZwoKNkGv10GnVUOr0XCE0eg00Op07CB0Wh2LsXSNAFRBKLPabCguLkZRYRFzP1qbkBATYmIaIDwi3NdqNXosAFevXsXMmbOwdetX6N3rIUx8dRKSkpJYE5GHE19gSW5eWJjz53/GkiWfYNmy5Vi4cDEee+wRBAcbfQTUqnHbCyy50Cpc90vAklMGSjVIhJGBl1dPlEoCE5Bq624eWLsEjkI/lP1KY/fV6ESv6L/TRVfiG54F/uLxWC4XCgoKMX/e33Ho4AGcPn0WkWHR6NixE2Li4tj2VofVqyW6neTi4BKJutPtglYfxPNwOu2wlFeiwmKFQqGGISgYBqMeWrUCboeDZSKlWgm9Uc/ZO2WJQUYjyw+S9kUmI1pDWMjLy0NZuQV2mxWN72qMPn16M2/2mXttgJWWloZZs2bh2tWrmPjqKxgw4GkRVIJUIMqePiFLcJlSTFYwz9q4cT2mTp3OP8OHD0NYWIiIcpEmBiTxLnHAwo6TvxQKOSp8vZ53kjQ6ApYIJnYI9InkCWkxFULGJJJZ0nnoxxMiPU9WwFJRgfz8fJ4PSSsEqMjISMTHN+KMiUDmFY3JPMK4qqPQ3vn4Aov+ggTSSxkZOHEsDYs/XIyw0EiMHftHNG+RLOiGbgd7Jnq5XE64KMNzCDqi1W6H1eZAeYUFFksFe5nikjI4nG4Q6Gh2bocNZaXFKC0phdVug0JFAqgaalFA5SyRbMGkXclUx2KxcuZP763WSjSKj8cj/R9F7969b95jbdz4BebN+zsiIyIwbdo0tG7TxhO/2XBSpiC7tWRIVmwBlJeXYceOHRg//kWMGf0cXp7wIqKiIjzAImOUl5fDbDbzMtLOoZKFVqP2oTqeRVOADVheXiHqZnYeEynJYWGhQl1NoWDpo7ysnNNu0neMQUa+t0ajZBG3stKGoqJCmMtIB3LD6XRBp9PwdfR8g5FUbA2DjsLAmdOncfDgQRw/foLVcKrhNYiJwfBhwziRCQ0N9excWhiLxQKbzSaGFj3zIPnOpt/pe7qWvBAvLF0jzprmW1pWipdfehk3rudj4sSJ6N2nlzesixtF2NVeD+mTOor7zzciiB86BY9FzyfAEa9zOmwoKSmBuVQAnMsNqMUQGmKiMKpnJd/uIIFMVPZ1Oh/HUjPHArBmzRrMn78AcXFxeOftd5DcPAkK5in08oYBuUeRfInXtTtx9GgaxowZi/vvfwBTp05h4VR6Xbt2Hdv/tQNfb/0KGo0OPXs+yDyscWIC1FqhFME2FJFFBsjKzsH+vQewO3U3u2cisDTGwYMHo0OHdlBrNDiXfhF7dx9EevrPqKwsR5d7O6FPnweRmNgIVquNAZKSshKnTp2GXq1lj2QKMSG5RXN05Yy3Ixo3TmDD03UpK1JgrbSgXft2aNGiJY9n4cIP+f2MGW+jdevWnjGSUEyk+9y5dLRp0wYtWrRAeHgYg4detKCU2KSnp6O0tJQFytjYWERFRXoASM91uBV44403cCztCEaOeBbjxo2Dm0KfZ8spOWq4xa4Gr6/1jRpesi2BUObF5V0PgegBuziZ56iO0kquojahcO3adZg/fz5nf1OnTUPbNq2hVHpTdWEn+IYqObCYEblcSEs7iqFDhyMmJg6rVq1AYmKih5scTTuG1157HcePHYVaLZQeGjdugqef/j0GDx6Ips3uEobsdqOkpAybNm3CqlWrcOF8OmJiolnuoLh/7NhxhEfUw9NPD4HD4cK/tn+DoqJraNqsGZPPn89nICGhCYYNHcYead26z3HxwmkMGTQY8QkJcDldyLuah1OnT+FsejoUChXu7/kArBYLzp79CX1798ITTz3J9yOAFBQUYNyfXoKl0oLZ789Ch44dPMDKyspiD5OamsrePjkpGd179EDPng8g2BSMffv2YeeOfzHFcLrcrA9GREaiV6/e6Nnzfly+nI3Lly5DpdFi584duHQpA0MGD8SMmW+zHeh/UluLwBnpnRw+kiQiEnd5UPbQCOE7oYAjrBp95Vk/6bMqwJJlh+6qyVetPBaFsNmz/8YZ4F+nTEGXzp1lwPL1WFXALnqY/PwCfLl5M/785psYPWo0XnvjNdSrFy0CS4HDhw5h4iuv4NKlTMycNYsnSyA5c+YnNGqUgBEjR+Khh+5HebkFM2a8i9SdOxBs0KBfv77o3bcvwiKjOOydOX0KHy1ewmk6hZS2bVpi2PAhSL5H8CTff3cCq1d/hszMbFgq7agXFYGxo59Fv0f7cdijF4Umc5kZGRcvYs+evUhZuYq5y6CBgzFq1Ei0at2KwwFtluzsXAx48mlk52She4/uiG8Uj5at7sFDDz0InU6HRQvn4XJGJnrc/wDOnk3H9ydPorikBDqDjklyz/u7o2P7TggLC0d2Tg5Sd+/BoUOH4HS5EBsTg2ZNmzB3OvPTWfx84SKaJTXHpEmT8Gj/hwXJAU4Q1ySfJYDDm/0qmEdKn7O2JEKrCl0VP/CXUGSp2S95LN7wPhSreh1LzgNogWfNmgmLpRKTJ09Gt25dZeU9ObB8OxeEi9ywWe1c8pn7wQfIyc7C4kUfokOnDlw6YOqvUOLkyZN4d+YMliW+3fYtgoNDkHf1KnZs38Gqs84QhFGjx+L61WuYP38uOnfsgMef6M/ibExsHHMrWuhScwn+PnceVn/2GZonN8dLL43H77rei5DQMJ458am0o4eRsmIltm3fiUaN4jH5L5Px5IDH+R5sIcrOXG5cyryELZu3YMGCBUySiUu98cbr6NXrIRj0BgbC56s/x/x589G4SWPO2Ih3FBcVISn5bowYMYKBfPzocTw78lmUmEuxMmU5vtq8BVqNDg1iYvHe+7PRuk1L7kiosFiwYcNGzH5/NopLijBq1Gg89eQTzDNzc3KwZs1a7Ny9F61atcIzQ4ey96JMjqIHAYvrMPwjrbQ3qgRs/PPFQsB38jvU4nLPJdV6LDmwiANMnz4dWVnZeP3119G//6OyYrKvvO9f7a+02PDjqVPcU7Rr107mP88/N5ZJLtlAaJtRIDMjAyuWL8emL77Atu3bERMbyzvy4oULbFBKIOo3aIQycxnUaifGvzAOffr2ZbGQcy63QOazs3Pw4cIP8cWmL5DULAkDBw1GXKM4rnM1bdoEkZHhXJzdnZqK5StW4syZc+jVqw/eemsyIiIjmKfRfqistOLQwcNY89kaBBkMiIiKwurPVmHYsKEYMXIEwsPCceTwEfxt9mz2vL369kGTJk2Rk5OHrVu3oqS0BHP+9jcG57o1G/DW9Mns1datXYO8rGzc3SwJe/YfxrMjRmLosMGccJRXVGBlykpMmzads7wJE17Byy+/iJCQEJSXleKTTz7FnLlzUVxcgi5d7sXMmTMZlKQHinm1H7DkUPDNqKsDye3q1qhVKLx+/Treeedt7Nu3H8OHj+DJcqYUoG2VvIbNasWly5dx4/oN5OXkYdfuPdi7bx+H0sl//gvad2jLfIYyKklhoIwrdVcqpkyZgk+WLEH79u2E0OQGTp86jUWLPsTyFSn8/s3/eR3PPjucBVpJz6Ks79y589iyeSu2bPkSF37+GQnxiWjXjjyjnltNOnbsgMcefwTx8Q05VU5N3cOLdf78ed44jz7aD6aQYA7P2Vm5WLNmHb7eshVz5rzPguSIkc8y9/nTn55Hw7iGOH7sODZ9+SWGDn0GLTk8GnDw4CEsW7ochUWFmDfvAxw5nIalnyzFR0s+xI7t27Fv7z60adsWnTp1xiuvTkLze5Kx+MOFiI2NYcCsWr0a7856lyUC8nhTp05FmLh5qKwzZ85c7N69hz+b+tZ0PDP0D4iOpuxa2uC1A1BtgMV8q4Y6rgRE/+tqBSzyBAsXLWBxs22bdpg75wNERUdwbPfNCoVMpyA/nwvUR44cwZVLl1FaZobT5eReocSERCQ3T8Yjjz6Clq1acrcDZXz0dz/99BOTe+Ix5Bmi60Xz/C0VFhw8cAATJkxARmYm3p76DgYNGYj4xAT+npz/jfx8/OOjT/Dxx58gLy+ba5b9+j6M3r37cE/T1m++YW+yYOF89O3bB+HhoSguLsVXX32Nt6ZMQcNGjTBv3hy0bCnwpx9+OIWVK1bixIkTeH/2uzh27Dt8suQfGPrMMxjyhyEIDQnF1dw85ORd5XGSJ7XZ7RzmiJdRJklhePu325G6OxXLVizDkUNHcfjQEYSEhSMkzIT1G9biscf74bVX32TphVqNjqUdQ0rKCvzw44/4/YCn8Kdx49lGBJzz589h+bJl+PjjJRw6J7w0ESNHj+Dn/5r24UDgqlpT9QWq//f+wJIAViOwpBLD9u3bMG/+PBQWlGDmO++i54PdoVYLcV2uOpPHImCtX7ceFy5eQHRkBBIbJ7J4eCO/AGlH07BjZyriYhti8pS/omvXe9lI5IoyMy9h1KgxvHtnzZqBJk2aCEKlm9T/PKSsWI5Z776Pnt0ewKuvT8LvunbhBjTiFzm5uZjxzkzs3bMP2dlXYNTrMOGVVzBu/HiQx9305WbMnTsHc+bMQf/+/REZEc5knrzcR4s/wldfb8HEiRMwaNAQxDRogMuXL2P9ug1Yv349EhITcC79PDp07IgXx7+AmNgGOJ52nL1TYWER1Bod90YVFhRBp9Ug1GTi/iaNWsVaF/U4zV+0CGqVBv/atg17dqdyqGvZuiXG/PE5nq/QDCm0I+Xl5XDS0rRpU+4IoY5RelVaKnDw0EGkLF/OUslrk94QPKVBf0ttSHJwBAKW/2dyQN4ysChbW7RoETZs2ISnBwzCOzOnwmDQcTruX86gh0n9PfKdRKArKS7BiuUrsWDhPDw98A8YOHAgazfksYqLizBv3gIcOnQAKSlL0blzZ+gMwgECp82Os2fPYNDgIbh8+QrGjn0Ozz//HFq0bM55D0kQ2779Bms+X4srWVmwVdjRvVt3dL3vPpy7kM7lqPj4eMycNRP33NPcoyVR2CaPtnvPHiQnJ7HHok3gcjiRl5eLkz+c5A0SG9cI3bvTZlIxD9q1cxeio+qhSZO7kHEpE0ePHEar1m0xfPhwdOrQnj10Xm4u62IuuNGydVuQuFhZYUZpSSFzwsh6sSIdEAvGXNMRWpMkwVTQn0VlinkklWfKYbfbEBpOepfaU52SFrym8OXvqaoLZ9J1UulKfl/pMylcejyVGDpr7bFoQtSV8N57sznlT0lZjrZtW3OhUso4vMj2SL1sFCLC5K2oETDj4gVs3rQZu3bvQkQ46U9NYTSaYLNZUWkpR2ZmJvILc7F0yad4uN/DiK4fLWSyLkH5XrZsGd577z32Zn/5y5+5IM7Ac7pQUVGOoqJ8WCoqcSH9Is6fS+falsNmRWx8LPo//jgSEu/iYqtQahEb4lxOLqoSbxSKtkLRlcBO2hcV4alQS56VRM9dO3fi6JGjnO2qVUroDGrc07wlOnbujLiGDQXVX+ylkprrqAfdc+iBS0rUkUDSAJVXxHDjUc7FsXFJzCtiSkGJP6J/pKKHmABJKf9vASx/MFZUVCIv7yonWBTGhXDtfdUILOFSASgXLlzAsqVL8Y+PP8Xo0aPx5ptvIDo6iiV/Rq5MViOD0WIfOpyGHTtScfLkd8jOusKLTyWWdm1ao1v3Hqz7aDVa2B1OmM2lLEISvyCt5t7f3YvwSLFqLraRkARB6jyVZ7p1785Kt1RgFnYRteW6+RmlJSWoqKjgcQWZTKhXv57oqeQLKYzcV4fxVva8iymo2IJnpfqawBtJliDQUHZKJ2fod+HlJxqKgqa0w72yjwAcX5lI9s5P7ZayP9/2Ff9lvzUCL78ba3pmM/ILC1FQVMwdFyX5xbjwcwY3cFqsFXjwoR7449jRXDOtPccSn0JDpbrXvr17uPEsv6AQCxcsxL33doExOEgElsd58oYiL/TpkqVYuXIVMjIvIioiAknJyeja9Xfo0rkL7mpyF0zBJibvtLNpEgX5Bbh2/RqSkpIRGRUphgqxo583qos5E+lFoSEhXPfjHlqfEYgKsr+9xfee3ix5xlNV2RVzXu9NCLjcklLdgVD2nG44nLSpqFzjRFl5OZxuSlwEHkSFZYedvnPwfO126jhwMUipBYZ+7DYnc6jKSgvcDjsULmrzdrONSPKkAjQ9Q+hWUHDotFZaeQORKFxRUcYdDQqujgjdF1wSEx2gkj4DoKHPlFQKUkJBMgsJruJpHN4afGLHjcL8AmTl5qKo1AxLuQUuqwNqhQpFJcVQapTo3rMbRo0cgaioKE8SUaPH8gpkwm+kE23auBGz3n0PjzzyKCZOmojmyck8eA4tjAGhW5HC54F9e3Hw4AGUV5SjceO7+JRHq1YtER4RKYYCCQSSF5H4hBccvJBcZhAMRT8+OzxAc6F0TVVsyXczLYUQDsm70qKTB6JFJzGYQjgp4LRIlRYLysvKhCKtBAKHi0HicjhgdzmY91gtVv4hjmlzunGjqBAOtxMhwSZQz5TD5oTT4RI7bMm7WuF0Cr1NdJ3d4YLd7uLzBDaLBWqFEzo1AUYNFdUY1bT4SjicgNVqZ6WdgEVVB5XCDa1KxTqdWgXoDVpo9Gqo6NiXWgUlpTkuBTR0JEwBBOsNgmNVq6Ex6KGlagJ3GAkJGVECg07PdjEToBScqkGv0fD5RqGHS4uYuFjcfffdTAEkTn1zwHKDDXblyhVMmzqV25THvfgiBg8axG0jSoV36RWiFuxy2lFWVsoLRw1mWp1Axr0t1r8MLGnnCAFZ4hy+7oWATDuYCC9947A7YGdvYGdwWCzl/BkBiA4WOGx29ghWWyWDiD4jHcxmc4LGTX9jLi1HpbWS22tcLjvMxWaUm0sFTkTyCBFpakGxO6AVtr9w7M3ugMvm4I2l1htQbrfBqXDDqNWC8j4K4SqFCnqDATqjDmqtErZKC3sX4n2kobNHddP/u2DQqfiHzwVqdVw3JA/D1yjowRJDU7CHoUY/auzT6zQIop4ryhg1dOSLYEWtLwqoFUr+CeIOEAKWiq+jEhQXgTghEzwWndih01VqrRYanVZ8hnA2kY6ICYvpG3pr1Zosl/Qlck4u/MypU1w0LjabufQwYMBTqF+/HhNUroqI5RypOCrMwJeMesKXfFxVQpK329k37aXQaedmOCLoJdx4ZuNbVpRZmAMR0S4uKsSN/GscVrRqNXsYApa5vIy9KAGeYgtlh7R4ROyF05ngHUgNcWpqsbG7oFNruPnNGGzkRSZuR+WWyHATTOGhvHuNOiOMfL7PCKMpGDpTMNudqnoKt4sdBHl3ulYfRKdfdHDYBXsRmafFp/sSyITPxAMMIuiqFOWkIrGMCgQwocdxs6n5Aj8eFuC/uyHGDqmBx6/RgG7iLXLThpSzkZvwWLI6IO9WBw7uP4Bp06ejtKwczwwbij8MGYJ61PJB6KJRSVmOvBmwyqy9h14F9AcIXqINPC04zHUUuHHjBr7e+jXWrd/AehJ5JUq/KZzxETQHeSM7HE4raz2xDWIQHhbGJJMAYgoNQYP69bjzgPrviSMEBRmEs3VGA0JMwdxtSTqVko5BiQsudBXIF0f0GvKpiHP3Ea49c5MtqudXb/bnbwHJjAI3lJoWfY0lOA+pw8HfkF7UeJ/s/S0QzqQxMFkQ0lBxQSUAyVogZH7Tk+DWpm1GeohcTqBnUej59pt/snCalZOLJ58agLFjxiIxoaFHJxI6MWXTCUCSpa/5Kz+3yuaqBliUnX134hh++P47mLh5TzhiTqkv1deMQUECv1Cp2PvUr1ePPQR3XYp8wXuIU3D9Qkes2BkbiKjz2nr9uNif6m058TgEvxAvkETZrhe+92JNBCt/QOOTvhEW1GsbsXPSwzdF+4qXi3TUd4eSvCGOmefoM1rxnQe9ntX26GPyk2E+rk8aCgPadzvU6LHkl1fRqQCUlJQi7cgRbNi4EcePf4e4RvEYNnw4HnigByLDw6FSq7w6jWzM3vt6d6BgWr8R+n0iF+uI01BNjSQFOhZOIKb2WQo15Lmk/04BhxTiFcQ1xI5XgY0JQA74VHlTksc7kQG9LcxSfU4OEU9SIU7Dxy94UOQ9KyB3Yj5hygdYYioibTo/0MkR6n8yh6foosMZQu+WkGSJ9/O4Uxnh8QxIWBe2jt+xMea6oik81rslYPnuMY+56Rh4+rlz2LlrF/YfOMiiGVX6Y+o34BMlcXGxiBV/hIKqD1w9b6Q97neBD9QClhxkfyDNT84N/e/nafuVb9ZADw0Ytt0+jXAecVP29wH+q01+Md4DP1nkD3inX7BNYCYV8C4ELPG0kxdYdGuvtTyh3QdY0uPl28P3ub6UwGuEm/JY/rb34kzQoKgu9tOZn5CTk4PS0jJkXMxAmdnMtbC27duhS5fOXDe83S/5VGsDrNv9/P/r95OXX6QW7996zLcMLN/+HeJdwukSItYnT/7Aaju1k1AbMvXKkwere/33W+D2AYv5mzx7kHyHLI+tSp/++y18h87wtgFLOtbtqxdIGUsVJnuHmvvOmfZtBpbIhn3sJ8sq6jzWHYOsWwaW96j7L6GmDlF3DKLEid5mYNUB6E4DUHXzrRFYvllfIOAESvbrzHunW6DWwKqut/lON2Dd/ANb4H8BzQIdN1p8oIUAAAAASUVORK5CYII="
                                              alt="signature" width="100"></span> <span id="sender_signature2_date"
              style="display: block;font-size: 10px;text-align:center;">৬-১-২০১৯</span>
            <span class="canedit" id="sender_name2"
              data-type="text">মো: শাহাবুদ্দিন খান</span> <br> <span class="canedit"
              id="sender_designation2" data-type="text">জেলা প্রশাসক</span>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
