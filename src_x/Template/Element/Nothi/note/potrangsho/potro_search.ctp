<div class="card-footer text-muted p-1">
  <div class="input-group">
    <div class="input-group-prepend">
      <button class="btn btn-secondary mr-1" type="button"><i class="fa fa-globe"></i>
      </button>
      <button class="btn btn-secondary mr-1" type="button"><i
          class="fa fa-flag"></i>
      </button>
    </div>
    <input type="text" class="form-control"
      placeholder="পত্রের বিষয় অথবা স্মারক নম্বর দিয়ে খুজুন"
      aria-label="Recipient's username" aria-describedby="button-addon2">
    <div class="input-group-append">
      <button class="btn btn-primary" type="button" id="button-addon2"><i
          class="fa fa-search"></i></button>
    </div>
  </div>
</div>