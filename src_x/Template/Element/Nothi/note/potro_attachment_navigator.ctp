<div class="note_navigation overflow-hidden">
  <div id="<?= $potros_nav_id ?>" class="slides d-flex justify-content-between">
    <button class="btn btn-info note_right"><i class="fa fa-angle-left"></i></button>
    <div class="overflow-hidden mx-1 flex-fill">
      <div class="btn-toolbar flex-nowrap nav" role="tablist">
        <?php
        foreach ($potros_group_nav as $group_key => $potros_groups) {
          ?>
          <div class="btn-group flex-nowrap mr-2 nav" role="group" aria-label="First group">
            <?php foreach ($potros_groups->potros as $potro_key => $potros_group) {
              $tooltip = 'page_bn: ' . $potros_group->nothi_potro_page_bn;
              $tooltip .= 'sarok_no: ' . $potros_group->sarok_no;
              $tooltip .= 'attachment_description: ' . $potros_group->attachment_description;
              $tooltip .= 'potro_no: ' . $potros_group->potro_no;
              $tooltip .= 'application_origin: ' . $potros_group->application_origin;
              $tooltip .= 'is_nothijato: ' . $potros_group->is_nothijato;
              $tooltip .= 'is_main: ' . $potros_group->is_main;
              $tooltip .= 'is_potrojari: ' . $potros_group->is_potrojari;
              $tooltip .= 'potrojari_status: ' . $potros_group->potrojari_status;
              $tooltip .= 'is_summary_nothi: ' . $potros_group->is_summary_nothi;
              $tooltip .= 'is_approved: ' . $potros_group->is_approved;
              $tooltip .= 'status: ' . $potros_group->status;
              $tooltip .= 'device_type: ' . $potros_group->device_type;
              $tooltip .= 'created: ' . $potros_group->created;
              $attachment_type = 'fa-flag';
              if ($potros_group->attachment_type == 'pdf') {
                $attachment_type = 'fa-file-pdf-o';
              }
              if ($potros_group->attachment_type == 'image') {
                $attachment_type = 'fa-image';
              }
              if ($potros_group->attachment_type == 'word') {
                $attachment_type = 'fa-file-word-o';
              }
              ?>
              <button data-toggle="tooltip" title="<?= $tooltip ?>" href="#tab<?= $group_key . $potro_key ?>"
                  data-toggle="tab" role="tab" class="btn btn-secondary"
                  type="button"><i class="fa <?= $attachment_type?>" style="position:absolute;top:3px;left:2px;"></i> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <span style="position:absolute;bottom:0; right:3px;"><?= $potros_group->nothi_potro_page_bn ?></span>
              </button>
            <?php } ?>
          </div>
        <?php } ?>


      </div>
    </div>
    <button class="btn btn-info note_left"><i class="fa fa-angle-right"></i></button>
  </div>
</div>