<div class="nothi-content h-100">
  <div class="send-note" hidden>
    <div class="card h-100 mb-0">
      <div class="card-header">
        <div class="d-flex">
          <button class="btn btn-warning"><i class="fa fa-long-arrow-left"></i> আগত নথিতে ফেরৎ যান</button>
          <h3 class="ml-3 mb-0 font-weight-normal">পরবর্তী কার্যক্রমের জন্য প্রেরণ করুন</h3>
        </div>
      </div>
      <div class="card-body flex-fill">
        <div class="card h-100 mb-0">
          <div class="card-header">
            পরবর্তী কার্যক্রমের জন্য প্রেরণ করুন
          </div>
          <div class="card-body flex-fill">
            <h5 class="card-title">Card title</h5>
            <h6 class="card-subtitle mb-2 text-muted">Card subtitle</h6>
            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's
              content.</p>
            <a href="#" class="card-link">Card link</a>
            <a href="#" class="card-link">Another link</a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="d-flex mb-0 flex-column h-100">
    <div class="p-2 align-items-center border-bottom">
      <?= $this->element('Nothi/nothi_note_title') ?>
    </div>
    <div class="flex-fill overflow-hidden">
      <?= $this->element('Nothi/splitted_notes') ?>
      <div class="d-flex h-100">
        <div class="row no-gutters flex-fill">
          <div id="note_left" class="h-100 d-flex flex-column">

            <div class="note_action mb-2 mr-2">
              <button class="side-pop-btn btn btn-primary-bar w-100 d-flex align-items-center ">
                <i class="icon-magnifier mr-2"></i><span>নোট খুঁজুন</span>
              </button>
            </div>
            <div class="note_action mb-2 mr-2">
              <button class="btn btn-danger-bar w-100 d-flex align-items-center ">
                <i class="icon-notebook mr-2"></i><span>সকল নোট</span>
              </button>
            </div>
            <div class="note_action mb-2 mr-2">
              <button class="btn btn-success-bar w-100 d-flex align-items-center ">
                <i class="icon-book-open mr-2"></i><span>নতুন নোট</span>
              </button>
            </div>



            <div class="note_action mb-2 mr-2">
              <button class="btn btn-violate-bar w-100 d-flex align-items-center justify-content-between">
                <span>নিজ ডেস্ক (৯৯৯)</span> <i class="fa fa-caret-right"></i>
              </button>
            </div>
            
            <div class="note_checklist flex-fill pScroll">
              <ul class="list-group">
                <li class="list-group-item list-item-bg">
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="customCheck1">
                    <label class="custom-control-label" for="customCheck1">অনুচ্ছেদ ১</label>
                  </div>
                </li>
                <li class="list-group-item list-item-bg activeBg">
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="customCheck2">
                    <label class="custom-control-label" for="customCheck2">অনুচ্ছেদ ২</label>
                  </div>
                </li>
                <li class="list-group-item list-item-bg">
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="customCheck3">
                    <label class="custom-control-label" for="customCheck3">অনুচ্ছেদ ৩</label>
                  </div>
                </li>
                <li class="list-group-item list-item-bg">
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="customCheck4">
                    <label class="custom-control-label" for="customCheck4">অনুচ্ছেদ ৪</label>
                  </div>
                </li>
                <li class="list-group-item list-item-bg">
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="customCheck5">
                    <label class="custom-control-label" for="customCheck5">অনুচ্ছেদ ৫</label>
                  </div>
                </li>
                <li class="list-group-item list-item-bg">
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="customCheck1">
                    <label class="custom-control-label" for="customCheck1">অনুচ্ছেদ ১</label>
                  </div>
                </li>
                <li class="list-group-item list-item-bg activeBg">
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="customCheck2">
                    <label class="custom-control-label" for="customCheck2">অনুচ্ছেদ ২</label>
                  </div>
                </li>
                <li class="list-group-item list-item-bg">
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="customCheck3">
                    <label class="custom-control-label" for="customCheck3">অনুচ্ছেদ ৩</label>
                  </div>
                </li>
                <li class="list-group-item list-item-bg">
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="customCheck4">
                    <label class="custom-control-label" for="customCheck4">অনুচ্ছেদ ৪</label>
                  </div>
                </li>
                <li class="list-group-item list-item-bg">
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="customCheck5">
                    <label class="custom-control-label" for="customCheck5">অনুচ্ছেদ ৫</label>
                  </div>
                </li>
                <li class="list-group-item list-item-bg">
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="customCheck1">
                    <label class="custom-control-label" for="customCheck1">অনুচ্ছেদ ১</label>
                  </div>
                </li>
                <li class="list-group-item list-item-bg activeBg">
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="customCheck2">
                    <label class="custom-control-label" for="customCheck2">অনুচ্ছেদ ২</label>
                  </div>
                </li>
                <li class="list-group-item list-item-bg">
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="customCheck3">
                    <label class="custom-control-label" for="customCheck3">অনুচ্ছেদ ৩</label>
                  </div>
                </li>
                <li class="list-group-item list-item-bg">
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="customCheck4">
                    <label class="custom-control-label" for="customCheck4">অনুচ্ছেদ ৪</label>
                  </div>
                </li>
                <li class="list-group-item list-item-bg">
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="customCheck5">
                    <label class="custom-control-label" for="customCheck5">অনুচ্ছেদ ৫</label>
                  </div>
                </li>
                <li class="list-group-item list-item-bg">
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="customCheck1">
                    <label class="custom-control-label" for="customCheck1">অনুচ্ছেদ ১</label>
                  </div>
                </li>
                <li class="list-group-item list-item-bg activeBg">
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="customCheck2">
                    <label class="custom-control-label" for="customCheck2">অনুচ্ছেদ ২</label>
                  </div>
                </li>
                <li class="list-group-item list-item-bg">
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="customCheck3">
                    <label class="custom-control-label" for="customCheck3">অনুচ্ছেদ ৩</label>
                  </div>
                </li>
                <li class="list-group-item list-item-bg">
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="customCheck4">
                    <label class="custom-control-label" for="customCheck4">অনুচ্ছেদ ৪</label>
                  </div>
                </li>
                <li class="list-group-item list-item-bg">
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="customCheck5">
                    <label class="custom-control-label" for="customCheck5">অনুচ্ছেদ ৫</label>
                  </div>
                </li>
                <li class="list-group-item list-item-bg">
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="customCheck1">
                    <label class="custom-control-label" for="customCheck1">অনুচ্ছেদ ১</label>
                  </div>
                </li>
                <li class="list-group-item list-item-bg activeBg">
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="customCheck2">
                    <label class="custom-control-label" for="customCheck2">অনুচ্ছেদ ২</label>
                  </div>
                </li>
                <li class="list-group-item list-item-bg">
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="customCheck3">
                    <label class="custom-control-label" for="customCheck3">অনুচ্ছেদ ৩</label>
                  </div>
                </li>
                <li class="list-group-item list-item-bg">
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="customCheck4">
                    <label class="custom-control-label" for="customCheck4">অনুচ্ছেদ ৪</label>
                  </div>
                </li>
                <li class="list-group-item list-item-bg">
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="customCheck5">
                    <label class="custom-control-label" for="customCheck5">অনুচ্ছেদ ৫</label>
                  </div>
                </li>
                <li class="list-group-item list-item-bg">
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="customCheck1">
                    <label class="custom-control-label" for="customCheck1">অনুচ্ছেদ ১</label>
                  </div>
                </li>
                <li class="list-group-item list-item-bg activeBg">
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="customCheck2">
                    <label class="custom-control-label" for="customCheck2">অনুচ্ছেদ ২</label>
                  </div>
                </li>
                <li class="list-group-item list-item-bg">
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="customCheck3">
                    <label class="custom-control-label" for="customCheck3">অনুচ্ছেদ ৩</label>
                  </div>
                </li>
                <li class="list-group-item list-item-bg">
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="customCheck4">
                    <label class="custom-control-label" for="customCheck4">অনুচ্ছেদ ৪</label>
                  </div>
                </li>
                <li class="list-group-item list-item-bg">
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="customCheck5">
                    <label class="custom-control-label" for="customCheck5">অনুচ্ছেদ ৫</label>
                  </div>
                </li>
              </ul>
            </div>




          </div>
          <div id="notansho_wrap" class="h-100">
            <div id="notansho_top">
              <div class="card border-0 h-100 mb-0">
                <?= $this->element('Nothi/nothi3_notansho') ?>
              </div>
            </div><!--note-top-->
            <div id="notansho_bottom">
              <div class="card h-100 add_onucched">
                <div class="card-header">
                  <div class="d-flex justify-content-between align-items-center">
                    <h4 class="mb-0">অনুচ্ছেদ লিখুন</h4>

                    <button class="btn btn-secondary compress">
                      <i class="fa fa-compress"></i>
                    </button>

                  </div>
                </div>
                <?= $this->element('Nothi/nothi_onucched_add') ?>
              </div>
            </div><!--note-bottom-->
          </div><!--notansho-->
          <div id="note_right" class="h-100">
            <?= $this->element('Nothi/nothi_potransho3') ?>
          </div><!--potransho-->
        </div>
      </div>
    </div>
  </div>
</div>

<script src="<?= $this->request->webroot ?>coreui/js/split.min.js"></script>
<script src='<?= $this->request->webroot ?>coreui/node_modules/froala-editor/js/froala_editor.min.js'></script>
<script src="<?= $this->request->webroot ?>coreui/js/onucched_attachment_uploader.js" crossorigin></script>
<script>
  (function (e) {

    $('.split_toggler').click(function (e) {
      $('.splitted_wrap').toggleClass('show');
    })


    function splitHorizontal(parentDiv) {
      $(parentDiv + ' > div').each(function (e) {
        $(this).attr('id', 'page' + (e + 1));
      })

      var splitIDs = $(parentDiv + " > div[id]").map(function () {
        return '#' + this.id;
      }).get();


      var noteTopBottom = Split(splitIDs, {
        direction: 'horizontal'
      });
    }

    splitHorizontal('.splitted_notes');

    /*var noteTopBottom = Split(['#note-1', '#note-2', '#note-3'], {
      direction: 'horizontal'
    });*/
    // noteTopBottom.collapse(1);


    if ($('#note_left, #notansho_wrap, #note_right').length) {
      var noteTopBottom = Split(['#note_left', '#notansho_wrap', '#note_right'], {
        sizes: [10, 45, 45],
        direction: 'horizontal'
      });
      // noteTopBottom.collapse(1);

    }
    if ($('#note_left, #notansho_wrap, #note_right').length) {
      var noteTopBottom = Split(['#notansho_top', '#notansho_bottom'], {
        sizes: [70, 30],
        direction: 'vertical'
      });
      // noteTopBottom.collapse(1);

    }
  })(jQuery)
</script>
