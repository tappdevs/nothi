<div class="card-header d-flex justify-content-between align-items-center p-1">
  <div class="d-flex align-items-center">
    <button type="button" class="btn btn-light border-0 onucched_toggle" title="সকল অনুচ্ছেদ" data-toggle="tooltip"><i
          class="cui-list align-middle"></i></button>
    <h4 class="mb-0 ml-1">নোটাংশ</h4>
  </div>
  <div class="btn-group">
    <button type="button" class="btn btn-light" title="ঊর্ধগামী" data-toggle="tooltip"><i
          class="fa fa-sort-amount-asc"></i></button>
    <button type="button" class="btn btn-light" title="নিম্নগামী" data-toggle="tooltip"><i
          class="fa fa-sort-amount-desc"></i></button>
    <button type="button" class="btn btn-light" title="পিডিএফ" data-toggle="tooltip"
        onClick="window.open('<?= $this->request->webroot ?>pdf');"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>
    </button>
    <button type="button" class="btn btn-light" title="প্রিন্ট" data-toggle="tooltip"
        onClick="window.open('<?= $this->request->webroot ?>print');"><i class="cui-print"></i></button>
    <button class="btn btn-light notansho_fullscreen" title="ফুলস্ক্রিন" data-toggle="tooltip">
      <i class="fa fa-expand"></i>
    </button>
  </div>
</div>

<div class="card-body overflow-hidden flex-basis p-0">
  <?= $this->element('Nothi/note/nothi3_note'); ?>
</div>
