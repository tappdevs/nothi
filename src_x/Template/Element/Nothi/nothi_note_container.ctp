<div class="nothi-content h-100">
  <div class="send-note" hidden>
    <div class="card h-100 mb-0">
      <div class="card-header">
        <div class="d-flex">
          <button class="btn btn-warning"><i class="fa fa-long-arrow-left"></i> আগত নথিতে ফেরৎ যান</button>
          <h3 class="ml-3 mb-0 font-weight-normal">পরবর্তী কার্যক্রমের জন্য প্রেরণ করুন</h3>
        </div>
      </div>
      <div class="card-body flex-fill">
        <div class="card h-100 mb-0">
          <div class="card-header">
            পরবর্তী কার্যক্রমের জন্য প্রেরণ করুন
          </div>
          <div class="card-body flex-fill">
            <h5 class="card-title">Card title</h5>
            <h6 class="card-subtitle mb-2 text-muted">Card subtitle</h6>
            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's
              content.</p>
            <a href="#" class="card-link">Card link</a>
            <a href="#" class="card-link">Another link</a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="d-flex mb-0 flex-column h-100">
    <div class="p-2 align-items-center">
      <?= $this->element('Nothi/nothi_note_title') ?>
    </div>
    <div class="flex-fill overflow-hidden">
      <div class="d-flex h-100">
        <div class="nothi-sidebar d-flex flex-column">
          <?= $this->element('Nothi/nothi_note_list') ?>
        </div>
        <div class="row no-gutters flex-fill">
          <div id="notansho" class="d-flex flex-column h-100">
            <div id="note-top" class="h-100">

              <div class="card border-0 h-100 mb-0">
                <?= $this->element('Nothi/nothi_notansho') ?>

                <button class="btn btn-primary write_onucched">
                  <i class="fa fa-file"></i> অনুচ্ছেদ লিখুন
                </button>

              </div>


            </div><!--note-top-->
            <div id="note-bottom">

              <div class="card h-100 add_onucched">
                <div class="card-header">
                  <div class="d-flex justify-content-between align-items-center">
                    <h4 class="mb-0">অনুচ্ছেদ লিখুন</h4>

                    <button class="btn btn-secondary compress">
                      <i class="fa fa-compress"></i>
                    </button>

                  </div>
                </div>
                <?= $this->element('Nothi/nothi_onucched_add') ?>
              </div>
            </div><!--note-bottom-->
          </div><!--notansho-->
          <div id="potransho" class="h-100">
            <div class="card border-0 mb-0 d-flex flex-column h-100">
              <?= $this->element('Nothi/nothi_potransho') ?>
            </div>
          </div><!--potransho-->
        </div>
      </div>
    </div>
  </div>
</div>

<script src="<?= $this->request->webroot ?>coreui/js/split.min.js"></script>
<script src='<?= $this->request->webroot ?>coreui/node_modules/froala-editor/js/froala_editor.min.js'></script>
<script src="<?= $this->request->webroot ?>coreui/js/onucched_attachment_uploader.js" crossorigin></script>
