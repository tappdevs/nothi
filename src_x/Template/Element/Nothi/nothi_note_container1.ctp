<div>
  <ul class="nav nav-tabs tabs-1 bg-white" role="tablist">
    <li class="nav-item">
      <a class="nav-link active d-flex align-items-center" data-toggle="tab" href="#home4" role="tab"
        aria-controls="home">
        <i class="fa fa-inbox fs-20"></i>
        <aside class="ml-3 line-height-1">
          <strong class="text-dark font-weight-normal">আগত নথি</strong>
          <span class="badge">12</span>
          <small class="text-truncate nothi-from d-block text-muted" data-toggle="tooltip"
            title="জাহাঙ্গীর, অফিস সহকারী (জেলা প্রশাসকের কার্যালয়, জামালপুর), a2i@gov.bd">জাহাঙ্গীর, অফিস
            সহকারী (জেলা প্রশাসকের কার্যালয়, জামালপুর), a2i@gov.bd
          </small>
        </aside>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link d-flex align-items-center" data-toggle="tab" href="#profile4" role="tab"
        aria-controls="profile">
        <i class="fa fa-paper-plane-o fs-20"></i>
        <aside class="ml-3 line-height-1">
          <strong class="text-dark font-weight-normal">প্রেরিত নথি</strong>
          <span class="badge">232</span>
          <small class="text-truncate nothi-from d-block text-muted" data-toggle="tooltip"
            title="জাহাঙ্গীর, অফিস সহকারী (জেলা প্রশাসকের কার্যালয়, জামালপুর), a2i@gov.bd">জাহাঙ্গীর, অফিস
            সহকারী (জেলা প্রশাসকের কার্যালয়, জামালপুর), a2i@gov.bd
          </small>
        </aside>
      </a>
    </li>

    <li class="nav-item dropdown">
      <a class="nav-link d-flex align-items-center dropdown-toggle" aria-controls="messages" id="dropdownMenuLink"
        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fa fa-envelope-open-o fs-20"></i>
        <aside class="ml-3 line-height-1">
          <strong class="text-dark font-weight-normal">অন্যান্য অফিসের নথি</strong>
          <span class="badge">343</span>
          <small class="text-truncate nothi-from d-block text-muted" data-toggle="tooltip"
            title="জাহাঙ্গীর, অফিস সহকারী (জেলা প্রশাসকের কার্যালয়, জামালপুর), a2i@gov.bd">জাহাঙ্গীর, অফিস
            সহকারী (জেলা প্রশাসকের কার্যালয়, জামালপুর), a2i@gov.bd
          </small>
        </aside>
      </a>
      <div class="dropdown-menu shadow" aria-labelledby="dropdownMenuLink">
        <a class="dropdown-item" href="#">অন্যান্য অফিস থেকে আগত নথি</a>
        <a class="dropdown-item" href="#">অন্যান্য অফিসে প্ররিত নথি</a>
      </div>
    </li>

    <li class="nav-item">
      <a class="nav-link d-flex align-items-center" data-toggle="tab" href="#messages4" role="tab"
        aria-controls="messages">
        <i class="icon-envelope-letter fs-20"></i>
        <aside class="ml-3 line-height-1">
          <strong class="text-dark font-weight-normal">সকল নথি</strong>
          <span class="badge">23</span>
          <small class="text-truncate nothi-from d-block text-muted" data-toggle="tooltip"
            title="জাহাঙ্গীর, অফিস সহকারী (জেলা প্রশাসকের কার্যালয়, জামালপুর), a2i@gov.bd">জাহাঙ্গীর, অফিস
            সহকারী (জেলা প্রশাসকের কার্যালয়, জামালপুর), a2i@gov.bd
          </small>
        </aside>
      </a>
    </li>

    <li class="nav-item">
      <a class="nav-link d-flex align-items-center" data-toggle="tab" href="#messages4" role="tab"
        aria-controls="messages">
        <i class="icon-layers fs-20"></i>
        <aside class="ml-3 line-height-1">
          <strong class="text-dark font-weight-normal">সকল পঠিত</strong>
          <span class="badge">23</span>
        </aside>
      </a>
    </li>
  </ul>
</div>

<div class="tab-content flex-basis pScroll overflow-hidden">
  <div class="tab-pane active" id="home4" role="tabpanel">

    <table class="table table-striped table-bordered datatable table-sm">
      <thead>

      <tr role="row" class="heading">
        <th style="width: 5%" class="text-center sorting_disabled" rowspan="1" colspan="1">
          ক্রম
        </th>
        <th class="text-center sorting_disabled" style="width: 20%" rowspan="1" colspan="1">
          নথি নম্বর
        </th>
        <th class="text-center sorting_disabled" style="width: 25%" rowspan="1" colspan="1">
          শিরোনাম
        </th>
        <th class="text-center sorting_disabled" style="width: 20%" rowspan="1" colspan="1">
          সর্বশেষ নোটের তারিখ
        </th>
        <th class="text-center sorting_disabled" style="width: 20%" rowspan="1" colspan="1">
          নথির শাখা
        </th>
        <th class="text-center sorting_disabled" style="width: 5%" rowspan="1" colspan="1">
          বিস্তারিত
        </th>
      </tr>
      </thead>
      <tbody id="dbl_click"></tbody>
    </table>


  </div>
  <div class="tab-pane" id="profile4" role="tabpanel">2. Lorem ipsum dolor sit amet, consectetur adipisicing
    elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
    nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
    Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
    Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est
    laborum.
  </div>
  <div class="tab-pane" id="messages4" role="tabpanel">3. Lorem ipsum dolor sit amet, consectetur adipisicing
    elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
    nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
    Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
    Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est
    laborum.
  </div>
</div>
