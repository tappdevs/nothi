<div class="note-actions text-center">

  <div class="mr-2 mb-2 side-pop-parent">
    <div class="style-changer" hidden>
      <button class="btn btn-primary w-100 d-flex align-items-center " data-style="style-1">
        <i class="icon-magnifier mr-2"></i><span>Style 1</span>
      </button>
      <button class="btn btn-primary w-100 d-flex align-items-center " data-style="style-2">
        <i class="icon-magnifier mr-2"></i><span>Style 2</span>
      </button>
      <button class="btn btn-primary w-100 d-flex align-items-center " data-style="style-3">
        <i class="icon-magnifier mr-2"></i><span>Style 3</span>
      </button>
    </div>
    <br>
    <button class="side-pop-btn btn btn-primary w-100 d-flex align-items-center ">
      <i class="icon-magnifier mr-2"></i><span>নোট খুঁজুন</span>
    </button>
    <div class="side-pop shadow-sm">
      <div class="card mb-0 text-left card-accent-primary">
        <div class="card-header d-flex justify-content-between align-items-center pl-2 pr-0 py-0">
          <h5 class="mb-0">নোট খুঁজুন</h5>
          <button type="button" class="btn btn-danger btn-sm btn-close">
            <i class="fa fa-close"></i>
          </button>
        </div>
        <div class="card-body">
          <div class="input-group d-flex">
            <div class="flex-fill overflow-hidden">

              <select name="onucchedSearch" class="custom-select">
                <option value="46">এখনই নির্ধারন করা সম্ভব হচ্ছেনা</option>
                <option value="49">আমাদের দেশে হবে সেই ছেলে</option>
                <option value="50">হ্যামিলটন টেস্টের প্রথম ইনিংসে দুর্দান্ত সেঞ্চুরি পান তামিমহ্যামিলটন টেস্টের প্রথম
                  ইনিংসে দুর্দান্ত সেঞ্চুরি পান তামিম ইকবাল
                </option>
                <option value="51"></option>
              </select>
            </div>
            <div class="input-group-append">
              <button class="btn btn-primary">
                <i class="fs1 efile-search3"></i> খুঁজুন
              </button>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
  <div class="mr-2 mb-2">
    <button class="btn btn-danger w-100 d-flex align-items-center ">
      <i class="icon-notebook mr-2"></i><span>সকল নোট</span>
    </button>
  </div>
  <div class="mr-2 mb-2">
    <button class="btn btn-success w-100 d-flex align-items-center ">
      <i class="icon-book-open mr-2"></i><span>নতুন নোট</span>
    </button>
  </div>


</div>

<div class="bg-violate p-2 m-0 text-center side-pop-parent">
  <div class="d-flex justify-content-between align-items-center">
    <h5 class="mb-0 text-white">নিজ ডেস্ক (৯৯৯)</h5>
    <button class="btn btn-success btn-sm side-pop-btn"><i class="fa fa-arrow-right"></i></button>
  </div>
  <div class="side-pop shadow-sm">
    <div class="card mb-0 text-left card-accent-primary text-dark">
      <div class="card-header d-flex justify-content-between align-items-center pl-2 pr-0 py-0">
        <h5 class="mb-0">নোট খুঁজুন</h5>
        <button type="button" class="btn btn-danger btn-sm btn-close">
          <i class="fa fa-close"></i>
        </button>
      </div>
      <div class="card-body">
        <ul>
          <li>note number</li>
          <li>note from</li>
          <li>আগত date</li>
          <li>total অনুচ্ছেদ</li>
          <li>path of start to end movement</li>
        </ul>

      </div>
    </div>
  </div>
</div>


<div class="list-group list-group-flush text-center pScroll note_list">
  <div class="list-group-item list-group-item-danger-on d-flex active">
    <div class="custom-control custom-checkbox">
      <input type="checkbox" data-title="বাংলাদেশ আমাদের জন্মভূমি" data-date="১৯-০২-২০১৯ ০৪:১৮:৪৯ " class="custom-control-input" id="chk1" value="1">
      <label class="custom-control-label" for="chk1">&nbsp;</label>
    </div>
    <div class="note_label">১ <span>(১.৪)</span></div>
  </div>
  <div class="list-group-item list-group-item-danger-on d-flex">
    <div class="custom-control custom-checkbox">
      <input type="checkbox" data-title="বাংলাদেশ আমাদের জন্মভূমি 2" data-date="2-০২-২০১৯ ০৪:১৮:৪৯ " class="custom-control-input" id="chk2" value="2">
      <label class="custom-control-label" for="chk2">&nbsp;</label>
    </div>
    <div class="note_label">২ <span>(১.৪)</span></div>
  </div>
  <div class="list-group-item list-group-item-danger-on d-flex">
    <div class="custom-control custom-checkbox">
      <input type="checkbox" data-title="বাংলাদেশ আমাদের জন্মভূমি 3" data-date="3-০২-২০১৯ ০৪:১৮:৪৯ " class="custom-control-input" id="chk3" value="3">
      <label class="custom-control-label" for="chk3">&nbsp;</label>
    </div>
    <div class="note_label">৩ <span>(১.৪)</span></div>
  </div>
  <div class="list-group-item list-group-item-danger-on d-flex">
    <div class="custom-control custom-checkbox">
      <input type="checkbox" data-title="বাংলাদেশ আমাদের জন্মভূমি 4" data-date="4-০২-২০১৯ ০৪:১৮:৪৯ " class="custom-control-input" id="chk4" value="4">
      <label class="custom-control-label" for="chk4">&nbsp;</label>
    </div>
    <div class="note_label">৪ <span>(৪.৩)</span></div>
  </div>
  <div class="list-group-item list-group-item-danger-on d-flex">
    <div class="custom-control custom-checkbox">
      <input type="checkbox" data-title="বাংলাদেশ আমাদের জন্মভূমি 5" data-date="5-০২-২০১৯ ০৪:১৮:৪৯ " class="custom-control-input" id="chk5" value="5">
      <label class="custom-control-label" for="chk5">&nbsp;</span></label>
    </div>
    <div class="note_label">৫ <span>(২.৯)</div>
  </div>
  <div class="list-group-item list-group-item-danger-on d-flex">
    <div class="custom-control custom-checkbox">
      <input type="checkbox" data-title="বাংলাদেশ আমাদের জন্মভূমি 6" data-date="6-০২-২০১৯ ০৪:১৮:৪৯ " class="custom-control-input" id="chk6" value="6">
      <label class="custom-control-label" for="chk6">&nbsp;</span></label>
    </div>
    <div class="note_label">৬ <span>(৬.৬)</div>
  </div>
  <div class="list-group-item list-group-item-danger-on d-flex">
    <div class="custom-control custom-checkbox">
      <input type="checkbox" data-title="বাংলাদেশ আমাদের জন্মভূম 7" data-date="7-০২-২০১৯ ০৪:১৮:৪৯ " class="custom-control-input" id="chk7" value="7">
      <label class="custom-control-label" for="chk7">&nbsp;</span></label>
    </div>
    <div class="note_label">৭ <span>(১.৩)</div>
  </div>

</div>
<input type="text" class="form-control" placeholder="নোট নম্বর" autofocus>
