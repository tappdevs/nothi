<div class="d-flex align-items-start">
  <a href="<?= $this->Url->build(["controller" => "Dashboard", "action" => "nothi2"]); ?>"
      class="btn btn-warning"><i class="fa fa-long-arrow-left"></i> আগত নথিতে ফেরৎ যান</a>
  <h3 class="ml-3 mb-0 font-weight-normal">শাখা: ব্যবসা-বাণিজ্য শাখা; নথি নম্বর: ০১.০১.০১০২.০১৮.০২.০০১.১৯;
    বিষয়: ফাইলের মাধ্যমে জিঞ্জাসিত বিষয়গুলো সঙ্গায়ীত</h3>

  <div class="btn-group ml-auto">
    <button title="" nothi_office="11" nothi_master_id="46" class=" btn btn-success" data-original-title=" প্রেরণ  করুন">
      <i class="fs1 a2i_gn_send2"></i> &nbsp; প্রেরণ করুন
    </button>
    <a title="" class="btn btn-warning" href="/tappware/nothi/" data-original-title=" নথি তালিকায় ফেরত যান">
      <i class="fs1 a2i_gn_details1"></i> নথিসমূহ
    </a>


  </div>

</div>