<div class="flex-fill overflow-hidden">
  <textarea class="form-control h-100 border-0 froala" resize="none" placeholder="Type your message"></textarea>
</div>
<div class="card-footer p-1">
  <div class="attachment_upload">
    <?= $this->element('Nothi/note/nothi_note_onucched_attachment_uploader'); ?>
  </div>

  <div class="d-flex justify-content-between">
    <div class="btn-group">
      <button class="btn btn-success d-flex align-items-center mr-1" title="সংরক্ষণ" data-toggle="tooltip">
        <i class="fa fa-save"></i> <span class="ml-1">সংরক্ষণ</span>
      </button>
      <button class="btn btn-secondary d-flex align-items-center mr-1 onucched_add_attachment" title="সংযুক্তি" data-toggle="tooltip">
        <i class="fa fa-paperclip"></i> <span class="ml-1">সংযুক্তি</span>
      </button>
      <button class="btn btn-secondary d-flex align-items-center mr-1" title="নতুন অনুচ্ছেদ" data-toggle="tooltip">
        <i class="fs1 a2i_gn_note2"></i> <span class="ml-1">নতুন অনুচ্ছেদ</span>
      </button>
      <button class="btn btn-secondary d-flex align-items-center mr-1" title="প্রেরণ" data-toggle="tooltip">
        <i class="fs1 a2i_gn_send2"></i> <span class="ml-1">প্রেরণ</span>
      </button>
      <button class="btn btn-danger d-flex align-items-center cancle_onucched" title="বাতিল করুন" data-toggle="tooltip">
        <i class="fs1 a2i_gn_close2"></i> <span class="ml-1">বাতিল করুন</span>
      </button>
    </div>


    <button class="btn btn-secondary expand" title="ফুলস্ক্রিন" data-toggle="tooltip">
      <i class="fa fa-expand"></i>
    </button>
  </div>
</div>
