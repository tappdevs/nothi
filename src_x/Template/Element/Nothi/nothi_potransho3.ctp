<?= $this->element('Nothi/note/potrangsho/potro_heading'); ?>
  <div class="card-body p-0 overflow-hidden flex-basis h-100 d-flex flex-column">
    <div class="h-100 overflow-hidden d-flex flex-column">

      <div class="potro_nav">
        <ul class="nav nav-tabs tabs-1 bg-white" role="tablist">
          <?= $this->element('Nothi/note/potrangsho/potro_nav_khoshra',['potro_tab_id'=>'potro_tab_1']); ?>
          <?= $this->element('Nothi/note/potrangsho/potro_nav_noter_potro',['potro_tab_id'=>'potro_tab_2']); ?>
          <?= $this->element('Nothi/note/potrangsho/potro_nav_all_potro',['potro_tab_id'=>'potro_tab_3']); ?>
          <?= $this->element('Nothi/note/potrangsho/potro_nav_nothijat_potro',['potro_tab_id'=>'potro_tab_4']); ?>
        </ul>
      </div><!--potro_nav-->

      <div class="tab-content flex-basis overflow-hidden potro_content">
        <?= $this->element('Nothi/note/potrangsho/potro_nav_content_khoshra',['potro_tab_id'=>'potro_tab_1']); ?>
        <?= $this->element('Nothi/note/potrangsho/potro_nav_content_noter_potro',['potro_tab_id'=>'potro_tab_2']); ?>
        <?= $this->element('Nothi/note/potrangsho/potro_nav_content_all_potro',['potro_tab_id'=>'potro_tab_3']); ?>
        <?= $this->element('Nothi/note/potrangsho/potro_nav_content_nothijat_potro',['potro_tab_id'=>'potro_tab_4']); ?>
      </div><!--potro_content-->

    </div>
  </div>
<?= $this->element('Nothi/note/potrangsho/potro_search'); ?>
<script src="<?= $this->request->webroot ?>coreui/js/potro_attachment_navigator.js" crossorigin></script>
