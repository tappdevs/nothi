<div class="splitted_wrap">
  <button class="btn split_toggler btn-danger rounded-circle btn-lg line-height-1 text-white shadow-sm"><i
      class="fa fa-arrows-alt fa-2x"></i></button>
  <div class="splitted_notes d-flex">
    <div>
      <div class="card h-100">
        <div class="card-header">
          Featured 1
        </div>
        <div class="card-body">
          <h5 class="card-title">Special title treatment</h5>
          <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
          <a href="#" class="btn btn-primary">Go somewhere</a>
        </div>
      </div>
    </div>
    <div>
      <div class="card h-100">
        <div class="card-header">
          Featured 1
        </div>
        <div class="card-body">
          <h5 class="card-title">Special title treatment</h5>
          <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
          <a href="#" class="btn btn-primary">Go somewhere</a>
        </div>
      </div>
    </div>
    <div>
      <div class="card h-100">
        <div class="card-header">
          Featured 2
        </div>
        <div class="card-body">
          <h5 class="card-title">Special title treatment</h5>
          <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
          <a href="#" class="btn btn-primary">Go somewhere</a>
        </div>
      </div>
    </div>
    <div>
      <div class="card h-100">
        <div class="card-header">
          Featured 3
        </div>
        <div class="card-body">
          <h5 class="card-title">Special title treatment</h5>
          <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
          <a href="#" class="btn btn-primary">Go somewhere</a>
        </div>
      </div>
    </div>
  </div>
</div>
