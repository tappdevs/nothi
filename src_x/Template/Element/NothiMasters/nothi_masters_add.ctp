
<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2.css" type="text/javascript">

<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2-bootstrap.css" type="text/javascript">

<style>
    .modal-open .modal {
        overflow-x: auto;
    }

    .error-message {
        color: red;
    }

    @media (min-width: 1010px) {
        .modal-dialog {
            width: 900px;
            margin: 30px auto;
        }
    }
    
    .checkbox input[type=checkbox], .checkbox-inline input[type=checkbox], .radio input[type=radio], .radio-inline input[type=radio]{
        margin-left: 0px;
    }

    #nothi_number_2nd_part::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
        color: #3a3a3a !important;
        opacity: 1; /* Firefox */
    }
    #nothi_number_2nd_part:focus{
        border-color: #d1d1d0;
    }

</style>
<div class="form">
    <?= $this->Form->create($nothiMastersRecords, ['id' => 'NothiCreateForm']); ?>

    <div class="form-body" >

        <div class="form-group row is_new_nothi">
            <label class="col-md-2 control-label">নথির ধরন <span class="text-danger">*</span></label>

            <div class="col-md-4">
                <?= $this->Form->input('nothi_types_id', ['label' => false, 'class' => 'form-control', 'type' => 'select', 'empty'=>'নথির ধরন নির্বাচন করুন', 'options' => $nothiTypes]); ?>
            </div>

            <label class="col-md-2 control-label">নথি নম্বর <span class="text-danger">*</span> </label>
            <input type="hidden" id="nothi-no" name="nothi_no" value="">
                <div class="col-md-4">
                    <div class="input-group" style="width: 100%;">
                        <?php if($nothi_no_1st_part_edit): ?>
                            <div class="input-group-addon" id="nothi_number_1st_part" style="border-right: none;background:  white;padding-right:  0px;    border-color: #d1d1d0;text-align: left;font-size: 13pt;color: #3a3a3a;">**.**.****.***.**.</div>
                            <input type="text" id="nothi_number_2nd_part" class="form-control text-left" maxlength="6" style="border-left: none;padding-left: 0;">
                        <?php else: ?>
                            <input type="text" id="nothi_number" class="form-control text-left" maxlength="24" placeholder="**.**.****.***.**.***.**">
                        <?php endif; ?>
                    </div>
                </div>
        </div>

        <div class="form-group row is_new_nothi">
            <label class="col-md-2 control-label">নথির নাম <span class="text-danger">*</span> </label>

            <div class="col-md-10">
                <?= $this->Form->input('subject', ['label' => false, 'class' => 'form-control', 'type' => 'text','default'=>(!empty($dak_subject)?$dak_subject:''),'placeholder'=>'নথির নাম']); ?>
	            <div id="subject_show"></div>
            </div>
        </div>

        <div class="form-group row is_new_nothi">
            <label class="col-md-2 control-label">নথির শ্রেণি </label>

            <div class="col-md-3" style="padding-right: 5px">
                <?php $nothiClass = json_decode(NOTHI_CLASS,true); echo $this->Form->input('nothi_class', ['label' => false, 'class' => 'form-control', 'type' => 'select', 'options' => ($nothiClass),'empty'=>'নথির শ্রেণি নির্বাচন করুন']); ?>
            </div><div class="col-md-1" style="padding-left: 0px; padding-top: 3px;"><a href="" style="text-decoration: none;" title="সচিবালয়ের নির্দেশমালা" data-target="#SecretariatGuidelinesModal" data-toggle="modal"><i class="efile-help4"></i></a></div>
            
            <label class="col-md-2 control-label">নথির শাখা <span class="text-danger">*</span></label>

            <div class="col-md-4">
                <?= $this->Form->input('office_units_id', ['label' => false, 'class' => 'form-control', 'type' => 'select', 'empty' => '--', 'options' => $unitList, 'readonly' => true, 'disabled' => 'disabled']); ?>
            </div>

        </div>

        <div class="form-group row is_new_nothi hidden">
            <label class="col-md-2 control-label">বিবরণ </label>

            <div class="col-md-10">
                <?= $this->Form->text('description', ['label' => false, 'class' => 'form-control ']); ?>
            </div>
        </div>

        <div class="form-group row is_new_nothi hidden">
            
            <label class="col-md-2 control-label">নথি তৈরির তারিখ </label>

            <div class="col-md-4">
                <div class="input-group">
                    <?= $this->Form->input('nothi_created_date', ['label' => false, 'class' => 'date-picker  form-control',
                        'type' => 'text', 'maxlength'=>'10', 'value' => date('Y-m-d')]); ?>
                    
                </div>
            </div>
        </div>
    </div>
    <?= $this->Form->end(); ?>
    <div class="form-actions">
        <div class="row">
            <div class="col-md-12 text-left">
                <input type="submit" class="btn btn-primary nothiCreateButton round-corner-5" value="<?php echo SAVE ?>"/>
                
            </div>
        </div>
    </div>

    <input type="button" class="btn btn-danger btn-sm btn-nothi-permission round-corner-5" value="নথিতে অনুমতি প্রদান করুন"/>
    <div id="permission-Div"></div><br/>
</div>
<!-- Modal -->
<div id="SecretariatGuidelinesModal" class="modal fade modal-purple" data-backdrop="static" role="dialog">
    <div class="modal-dialog" style="width: 99%;">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">সচিবালয়ের নির্দেশমালা</h4>
            </div>
            <div class="modal-body text-center">
                <img src="<?= $this->request->webroot ?>img/secretariat_guidelines.png" alt="Image">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default red round-corner-5" data-dismiss="modal">বন্ধ করুন</button>
            </div>
        </div>

    </div>
</div>
<!-- Modal End-->
<script src="<?php echo CDN_PATH; ?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js"
        type="text/javascript"></script>

<script src="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/jQuery-Mask/jquery.mask.min.js"></script>

<script type="text/javascript">

    jQuery(document).ready(function () {
        $('.nothiCreateButton').hide();
        $('#nothi-types-id').select2();
        $('.date-picker').datepicker({
             rtl: Metronic.isRTL(),
             orientation: "left",
             format:'yyyy-mm-dd',
             endDate: '<?php echo date("Y-m-d") ?>',
             autoclose: true
         });
        
    });
    
    $(document).on('keyup','input[name=subject]', function(){
        $("#subject_show").html($(this).val());
    });
    $(document).on('change','#nothi-master-id', function(){

        if($(this).val()>0){
            $('.nothiCreateButton').show();
            $('.is_new_nothi').hide();
        }else{
            $('.nothiCreateButton').hide();
           
            $('.is_new_nothi').show();
        }
    });

    <?php if($nothi_no_1st_part_edit): ?>
        $('#nothi_number_2nd_part').mask('AAA.AA',{
            translation:{
                A: {pattern: /[\u09E6-\u09EF-0-9]/},
            },
            placeholder: "***.**"
        });
    <?php else: ?>
        $('#nothi_number').mask('AA.AA.AAAA.AAA.AA.AAA.AA',{
            translation:{
                A: {pattern: /[\u09E6-\u09EF-0-9]/},
            },
            placeholder: "**.**.****.***.**.***.**"
        });
    <?php endif; ?>

    var bn_digits = new Array('০', '১', '২', '৩', '৪', '৫', '৬', '৭', '৮', '৯');

    $("#nothi-types-id").change(function (e) {
        $.ajax({
            url: js_wb_root + "nothiMasters/generateNothiNumber",
            data: {id: $(this).val()},
            type: "POST",
            dataType: 'JSON',
            success: function (data) {
                <?php if($nothi_no_1st_part_edit): ?>
                    var nothi_number_1st_part = data.substring(0, 18);
                    var nothi_number_2nd_part = data.substring(18, 25);
                    $("#nothi_number_1st_part").text(nothi_number_1st_part);
                    $("#nothi_number_2nd_part").val(nothi_number_2nd_part);
                <?php else: ?>
                    $("#nothi_number").val(data);
                <?php endif; ?>
            },
            error: function (status, xresponse) {

            }

        });
    });

    $(document).on('click',".btn-nothi-permission",function (e) {
        <?php if($nothi_no_1st_part_edit): ?>
            var nothi_no = ($("#nothi_number_1st_part").text())+($("#nothi_number_2nd_part").val());
        <?php else: ?>
            var nothi_no = $("#nothi_number").val();
        <?php endif; ?>

        if (!(nothi_no.replace(/[\.\*]/g, '').length==18)) {
            toastr.error('দুঃখিত! নথি নম্বর সঠিক নয়। নথি নম্বর ১৮ ডিজিটের হতে হবে ');
            return;
        }

        if (isEmpty($("#nothi-class").val())) {
            toastr.error('দুঃখিত! নথির শ্রেণি নির্বাচন করুন');
            return;
        }

        $("#permission-Div").html('<img src="' + js_wb_root + 'assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp;লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');
        $.ajax({
            url: js_wb_root + "nothiMasters/nothiMasterPermissionList/",
            success: function (response) {

                $('.btn-nothi-permission').hide();
                $('.nothiCreateButton').show();
                $("#permission-Div").html(response);
                $('select').select2();
               listown();listownother();
//                $('.dataTables_filter').css('float','left');
            },
            error: function (status, xresponse) {

            }

        });
    });
    

    

</script>