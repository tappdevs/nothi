<h3 class="form-section"><input type="checkbox" id="login_checkbox" value="1">লগইন তথ্য </h3>
<div id="login_div" style="display: none;">
    <div class="row">
        <div class="col-md-6 form-group form-horizontal">
            <label class="control-label">লগইন আইডি</label>
            <?= $this->Form->input('user_name', ['label' => false, 'class' => 'form-control', 'type' => 'text', 'placeholder' => 'পাসওয়ার্ড']); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 form-group form-horizontal">
            <label class="control-label">পাসওয়ার্ড</label>
            <?= $this->Form->input('fullname_eng', ['label' => false, 'class' => 'form-control', 'type' => 'password', 'placeholder' => 'পাসওয়ার্ড']); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 form-group form-horizontal">
            <label class="control-label">পাসওয়ার্ড নিশ্চিত করুন </label>
            <?= $this->Form->input('fullname_eng', ['label' => false, 'class' => 'form-control', 'type' => 'password', 'placeholder' => 'পাসওয়ার্ড নিশ্চিত করুন ']); ?>
        </div>
    </div>
</div>

<script type="text/javascript">
    $("#login_checkbox").click(function () {
        if ($(this).is(':checked')) {
            $("#login_div").show('slow');
        }
        else {
            $("#login_div").hide('slow');
        }
    });
</script>
