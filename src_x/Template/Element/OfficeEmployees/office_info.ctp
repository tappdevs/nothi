<h3 class="form-section">দাপ্তরিক তথ্য </h3>
<div class="row">
    <div class="col-md-6 form-group form-horizontal">
        <label class="control-label">মন্ত্রণালয়</label>
        <?php echo $this->Form->input('ministry_selection', array('label' => false, 'class' => 'form-control', 'placeholder' => 'মন্ত্রণালয়')); ?>
    </div>
    <div class="col-md-6 form-group form-horizontal">
        <label class="control-label">দপ্তর / অধিদপ্তর </label>
        <?php echo $this->Form->input('head_office_selection', array('label' => false, 'class' => 'form-control', 'placeholder' => 'দপ্তর / অধিদপ্তর ')); ?>
    </div>
</div>
<div class="row">
    <div class="col-md-6 form-group form-horizontal">
        <label class="control-label">অফিস</label>
        <?php echo $this->Form->input('office_selection', array('label' => false, 'class' => 'form-control', 'placeholder' => 'অফিস')); ?>
    </div>
    <div class="col-md-6 form-group form-horizontal">
        <label class="control-label"> সংশ্লিষ্ট পদে যোগদানের তারিখ </label>
        <?php echo $this->Form->input('joining_date', array('label' => false, 'class' => 'form-control', 'placeholder' => ' সংশ্লিষ্ট পদে যোগদানের তারিখ ')); ?>
    </div>
</div>
<div class="row">
    <div class="col-md-6 form-group form-horizontal">
        <label class="control-label">অত্যাবশ্যকীয় কার্যাবলি</label>
        <?php echo $this->Form->input('mandatory_role', array('empty' => '--Select--', 'type' => 'select', 'label' => false, 'class' => 'form-control', 'placeholder' => 'অত্যাবশ্যকীয় কার্যাবলি')); ?>
    </div>
    <div class="col-md-6 form-group form-horizontal">
        <label class="control-label">পদবি</label>
        <?php echo $this->Form->input('designation', array('label' => false, 'class' => 'form-control', 'placeholder' => 'পদবি')); ?>
    </div>
</div>



