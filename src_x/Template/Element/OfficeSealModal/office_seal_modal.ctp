<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>

<div class="modal fade addSeal modal-purple" class="modal fade" tabindex="-1" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <input type="hidden" value="0" id="sealtype" />
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
                <div class="modal-title">প্রাপকের তালিকা তৈরি</div>
            </div>
            <div class="modal-body">
                <div class="scroller">

                </div>
            </div>
            <div class="modal-footer">
                <button style="display: none;" type="button" id="save-seal" class="btn green" align="right">সিল সংরক্ষণ করুন
                </button>
                <button style="display: none;" type="button" data-dismiss="modal" id="save-temp-seal" class="btn green" align="right">অস্থায়ী সিল সংরক্ষণ করুন
                </button>
                <button type="button" data-dismiss="modal" class="btn  btn-danger btn-close-seal-modal" id="autoclose">
                    বন্ধ করুন
                </button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>

<script type="text/javascript">
   jQuery.ajaxSetup({
        cache: true
    });
    var OfficeSeal = {
        checked_node_ids: [],
        getUnitOrganogramByOfficeId: function (office_id) {
            $("#office_unit_tree_panel").html('<tr><td colspan="4" class="text-center"><img src="'+js_wb_root+'assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp; লোড হচ্ছে। একটু অপেক্ষা করুন... </span></td></tr>');
            PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + 'officeManagement/getOfficeDesignationsAndIdentificationNoByOfficeId',
                {'office_id': office_id}, 'json',
                function (response_data) {

                    var str_unit_org = '';
                    var dropdown_option='';
                    str_unit_org += '<div class="table-container">';
                    str_unit_org += '<div class="panel-group accordion">'
                   
                    $.each(response_data, function (i) {
                        response = response_data[i];
                        var unit = response.office_unit;
                        var orgs = response.designations;
                        
                        str_unit_org += '<div class="panel panel-default" id="div_collapse_'+unit.id+'"><div class="panel-heading"><h4 class="panel-title"><a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" id="collapse_link_' + unit.id + '" href="#collapse_' + unit.id + '">' + unit.unit_name_bng + '</a></h4></div><div id="collapse_' + unit.id + '" class="panel-collapse collapse"><div class="panel-body">';
                        dropdown_option += '<option value="'+ unit.id +'">'+ unit.unit_name_bng +'</option>';

                        $.each(orgs, function (i) {
                            var org = orgs[i];
                            if(typeof(org.employee_record_id)=='undefined' || org.employee_record_id == 'undefined'){
                                
                            }else{
                                if(typeof(org.designation_description)=='undefined' || org.designation_description == 'undefined' || typeof(org.designation_description)=='null' || org.designation_description == 'null' || org.designation_description == ''|| org.designation_description == null ){
                                    org.designation_description = org.office_employee_name;
                                }else{
                                     org.designation_description = org.office_employee_name + ",\n" +org.designation_description ;
                                }
                                str_unit_org += '<input type="checkbox" onclick="OfficeSeal.getOfficeSealByOfficeId();" class="org_checkbox" data-office-unit-id="' + unit.id + '" data-office-unit-organogram-id="' + org.id + '"  data-unit-name="' + unit.unit_name_bng + '" data-designation-name="' + org.designation_bng + '" employee-name="'+ org.office_employee_name +'" employee-id="'+org.employee_record_id+'" employee-office-id="'+org.employee_office_id+'"><span title="'+org.designation_description + '">' + org.designation_bng + "</span></br>";
                            }
                            
                        });
                        str_unit_org +='</div></div></div>';
                        
                    });
                    str_unit_org += '</div></div>';
                    var data = '<select id="office_unit_dropdown" class="form-control"><option value="all">সকল শাখা</option>'+dropdown_option+'</select>'+str_unit_org;

                    $("#office_unit_tree_panel").html(data);
                    show_unit();
                    $('#office_unit_dropdown').select2();

                    $(".selection_unit").hide();

                    var seal_designations = [];
                    $('input[name=office-employee]').each(function () {
                        seal_designations.push($(this).data('office-unit-organogram-id'));
                    });
                    $('.org_checkbox').each(function () {
                        var org_id = $(this).data('office-unit-organogram-id');
                        if ($.inArray(org_id, seal_designations) > -1) {
                            if ($('#sealtype').val()==1) {
                                $(this).attr('disabled', 'disabled');
                            }else{
                                $(this).attr('checked', 'checked');
                            }
                        }
                    });
                    //initTable1();
                    $('.dataTables_filter').css('float','left');

                    $('[data-toggle="tooltip"]').tooltip({'placement':'bottom'});
                    $('[title]').tooltip({'placement':'bottom'});
                }
            );
        },
        getOfficeSealByOfficeId: function () {
            $("#office_unit_seal_panel").html("");
            var str_seal_designation = '';

            str_seal_designation += '<div class="row">';
            str_seal_designation += '<table class="table table-bordered">';
            str_seal_designation += '<tbody>';
                
            var i = 0;
            $('#office_unit_tree_panel input:checked').each(function () {
                str_seal_designation += '<tr>';
                str_seal_designation += '<td  style="width: 30px; text-align: center;">&nbsp;&nbsp;';
                str_seal_designation += '<input type="hidden" name="office-employee" data-office-unit-id="' + $(this).attr('data-office-unit-id') + '" data-office-unit-organogram-id="' + $(this).attr('data-office-unit-organogram-id') + '"  data-designation-name="' + $(this).attr('data-designation-name') + '" data-unit-name="' + $(this).attr('data-unit-name') + '" employee-name="'+$(this).attr("employee-name")+'" employee-id="'+$(this).attr("employee-id")+'" employee-office-id="'+$(this).attr("employee-office-id")+'">';
                str_seal_designation += '</td>';
                str_seal_designation += '<td>&nbsp;&nbsp;';
                str_seal_designation += $(this).attr('data-designation-name') + ', ' + $(this).attr('data-unit-name');
                str_seal_designation += '</td>';
                str_seal_designation += '</tr>';
            });

            str_seal_designation += '</tbody>';
            str_seal_designation += '</table>';
            str_seal_designation += '</div>';

            $("#office_unit_seal_panel").append(str_seal_designation);
            if ($('#sealtype').val()==1) {
                $("#save-temp-seal").css("display", "inline");
            }else{
                $("#save-seal").css("display", "inline");
            }
        },
        checkIfSealExists: function () {
            var office_id = $("#office-unit-id").attr('office-id');
            var office_unit_id = $("#office-unit-id").val();
            $("#office_unit_seal_panel").html("");
            var data = {
                'office_id': office_id,
                'office_unit_id': office_unit_id
            };

            PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + 'officeManagement/loadOfficeSeal',
                data, 'html',
                function (response) {
                    OfficeSeal.getUnitOrganogramByOfficeId(office_id);
                    $("#office_unit_seal_panel").html(response);
                    $(document).find('[title]').tooltip({'placement':'bottom'});
                });
        },

        submitDesignationToSeal: function () {
            var office_id = $("#office-unit-id").attr('office-id');
            var seal_owner_unit_id = $("#office-unit-id").val();
            var designation_bng = [];
            var unit_bng = [];
            var office_unit_organogram_id = [];
            var office_unit_id = [];

            var i = 0;
            $('#office_unit_seal_panel input[name=office-employee]').each(function () {
                designation_bng[i] = $(this).attr("data-designation-name");
                unit_bng[i] = $(this).attr("data-unit-name");
                office_unit_organogram_id[i] = $(this).attr("data-office-unit-organogram-id");
                office_unit_id[i] = $(this).attr("data-office-unit-id");
                i++;
            });

            var data = {
                'office_id': office_id,
                'seal_owner_unit_id': seal_owner_unit_id,
                'office_unit_id': office_unit_id,
                'designation_bng': designation_bng,
                'unit_bng': unit_bng,
                'office_unit_organogram_id': office_unit_organogram_id
            };

            PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + 'officeManagement/submitDesignationInSeal',
                data, 'json',
                function (response) {
                    if (response == 1) {
                        OfficeSeal.getOfficeSealByOfficeId();
                        $(document).find('[title]').tooltip({'placement':'bottom'});
                        toastr.options = {
                            "closeButton": true,
                            "debug": false,
                            "positionClass": "toast-bottom-right"
                        };
                        toastr.success("সিল সংরক্ষণ করা হয়েছে");
                            $('#autoclose').click();
                        
                    }
                    else {
                        toastr.options = {
                            "closeButton": true,
                            "debug": false,
                            "positionClass": "toast-bottom-right"
                        };
                        toastr.error("দুঃখিত! সিল সংরক্ষণ করা সম্ভব হচ্ছে না");
                    }
                });

        }
        ,
        submitDesignationToTempSeal: function () {
//            var office_id = $("#office-unit-id option:selected").attr('office-id');
//            var seal_owner_unit_id = $("#office-unit-id").val();
            var designation_bng = [];
            var unit_bng = [];
            var office_unit_organogram_id = [];
            var office_unit_id = [];
            var employee_name = [];
            var employee_office_id = [];
            var employee_id = [];

            var i = $('.office_employee_to').length;

            var tempSealTable = "";

            $('#office_unit_seal_panel input[name=office-employee]').each(function () {
                designation_bng[i] = $(this).attr("data-designation-name");
                unit_bng[i] = $(this).attr("data-unit-name");
                office_unit_organogram_id[i] = $(this).attr("data-office-unit-organogram-id");
                office_unit_id[i] = $(this).attr("data-office-unit-id");
                employee_name[i] = $(this).attr("employee-name");
                employee_id[i] = $(this).attr("employee-id"); //employee record id
                employee_office_id[i] = $(this).attr("employee-office-id"); //employee office id


                tempSealTable += "<tr><td></td>";
                
                tempSealTable += "<td style='width:60%; background-color: rgb(255, 209, 209);'>" + designation_bng[i] + ', ' + unit_bng[i] + "</td>";
                tempSealTable += "<td style='width:60%; background-color: rgb(255, 209, 209);'>" + employee_name[i] + "</td>";
                
                tempSealTable += '<td style="width:15%; background-color: rgb(255, 209, 209); text-align: center;"><input class="office_employee_to" type="radio" id="'+i+'" name="office-employee_to" data-employee-office-id="'+employee_office_id[i]+'" data-main-office-unit-id="'+office_unit_id[i]+'" data-main-office-employee-id="'+employee_id[i]+'" data-main-office-employee-name="'+employee_name[i]+'" data-main-office-unit-organogram-id="'+office_unit_organogram_id[i]+'"></td>';

                tempSealTable += '<td style="width:15%; background-color: rgb(255, 209, 209); text-align: center; "><input class="office_employee_checkbox text-center" type="checkbox" id="emp_'+office_unit_organogram_id[i]+'" name="office-employee" data-label="'+ designation_bng[i] + ', ' + unit_bng[i] +'" data-employee-office-id="'+employee_office_id[i]+'" data-office-employee-id="'+employee_id[i]+'" data-office-employee-name="'+employee_name[i]+'" data-office-unit-id="'+office_unit_id[i]+'" data-office-unit-organogram-id="'+office_unit_organogram_id[i]+'"></td>';

                tempSealTable += "</tr>";

                i++;
            });

            $('.officeSealDiv').find('tbody').append(tempSealTable);
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "positionClass": "toast-bottom-right"
            };
            toastr.success("অস্থায়ী সিল  বানানো  হয়েছে");
        }
    };

    $(document).on('click', "#save-temp-seal", function () {
        OfficeSeal.submitDesignationToTempSeal();
    });
    $(document).on('click', "#save-seal", function () {
        OfficeSeal.submitDesignationToSeal();
    });
    $(document).on('change', "#office-unit-id", function () {
        OfficeSeal.checkIfSealExists();
    });

    var initTable1 = function () {
        var table = $('#organogramTable');

        var oTable = table.dataTable({

            loadingMessage: 'লোড করা হচ্ছে...',
            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "emptyTable": "কোনো তথ্য নেই",
                "info": "<span class='seperator'>|</span>মোট _TOTAL_ টি তথ্য পাওয়া গেছে",
                "infoEmpty": "কোনো তথ্য নেই",
                "infoFiltered": "<span class='seperator'>|</span>মোট _TOTAL_ টি তথ্য পাওয়া গেছে",
                "search": "খুঁজুন  ",
                "zeroRecords": "কোনো তথ্য নেই"
            },
            "order": [
                [1, 'asc']
            ],

            "serverSide": false,
            "paging":   false,
            "ordering": false,
            "info":     false,
            "columnDefs": [
                { "visible": false, "targets": 1 }
            ],
            "drawCallback": function ( settings ) {
                var api = this.api();
                var rows = api.rows( {page:'current'} ).nodes();
                var last=null;

                api.column(1, {page:'current'} ).data().each( function ( group, i ) {
                    if ( last !== group ) {
                        $(rows).eq( i ).before(
                            '<tr class="group"><td colspan="2" style="font-weight:bold;">'+group+'</td></tr>'
                        );

                        last = group;
                    }
                } );
            },
            "bStateSave": false,
            // set the initial value
            "pageLength": -1,
            "dom": "<'row'<'col-md-12 col-sm-12'>r><'table-scrollable table table-bordered table-stripped't>"
        });
    }

    function show_unit() {
        $("#office_unit_dropdown").change(function(){
            var div_collapse_val = $("#office_unit_dropdown").val();
            var prev_value = $("#prev_value").val();
            if(isEmpty(prev_value)){
                $("[id^=div_collapse]").hide();
            } else if(prev_value == 'all'){
                $(".accordion-toggle").each(function () {
                    if($(this).hasClass( "collapsed" ) == true){
                        $(this).click();
                    }
                });
                $("[id^=div_collapse]").hide();
            } else {
                $('#div_collapse_'+prev_value).hide();
                if($('#collapse_link_'+prev_value).hasClass( "collapsed" ) == false ){
                    $('#collapse_link_'+prev_value).click();
                }
            }

            if(div_collapse_val == 'all'){
               $(".accordion-toggle").each(function () {
                    if($(this).hasClass( "collapsed" ) == false){
                        $(this).click();
                    }
                });
               $("[id^=div_collapse]").show();
            } else {
                $('#div_collapse_'+div_collapse_val).show();
                if($('#collapse_link_'+div_collapse_val).hasClass( "collapsed" ) == true ){
                    $('#collapse_link_'+div_collapse_val).click();
                }
            }
            $("#prev_value").val(div_collapse_val);
        })
    }
</script>
