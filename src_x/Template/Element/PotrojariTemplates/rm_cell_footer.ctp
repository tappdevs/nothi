 <?php
                if($prefix == 'receiver_' || $prefix == 'onulipi_'){
            ?>
            <div class="row bg-grey-steel" <?= (isset($potro_type) && $potro_type== 27)?'hidden':'' ?>>
                <div class="col-md-3 col-sm-3 col-xs-3 text-right">
                    <label class="bold font-grey-gallery"><?= ($prefix == 'receiver_')?'প্রাপকের ধরনঃ':'অনুলিপির ধরনঃ' ?></label>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4">

                    <?php
                            echo $this->Form->radio(
                                $prefix.'internal_selection'.$refer,
                                [
                                    ['value' => 1, 'text' => 'অন্তঃস্থ ',(isset($potro_type) && $potro_type== 27)?'checked':'notchecked'],
                                    ['value' => 2, 'text' => 'বহিঃস্থ ',(isset($potro_type) && $potro_type== 21)?'checked':'notchecked'],
                                ]
                            );
                    ?>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4">
                    <?php
                          echo $this->Form->checkbox($prefix.'attention_selection'.$refer,['hiddenField' => false,'value' => 0]);
                    ?>
                    দৃষ্টি আকর্ষণ
                </div>
            </div>
            <br>
            <?php
                    }
            ?>
<div class="btn-group  saveButton">
                <a  class="btn green savethis margin-right-10" prefix="<?php echo $prefix ?>" refid="<?=$refer?>"><i class="a2i_gn_approval1"></i></a><a  class="btn red closethis" prefix="<?php echo $prefix ?>" refid="<?=$refer?>"><i class="a2i_gn_close2"></i></a>
</div>