<!-- BEGIN ACTIVATION FORM -->
<form class="activation-form" action="activation" method="post">
    <div class="form-group">
        <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
        <label class="control-label visible-ie8 visible-ie9">Activation Code</label>
        <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off"
               placeholder="Activation Code" name="activation_code" required="true"/>
    </div>
    <div class="form-actions">
        <button type="submit" class="btn green uppercase">Activate</button>
        <?= $this->Html->link('Back to login', ['action' => 'login'], ['class' => 'forget-password']) ?>
    </div>
</form>
<!-- END ACTIVATION FORM -->