<style>
    img{
        max-height: 1000px;
        max-width: 1000px;
    }
</style>

    <noscript>
        <div id="Script-disable-div" class="alert alert-danger">দুঃখিত, আপনার ব্রাউজার জাভাস্ক্রিপ্ট সমর্থন করে না! অনুগ্রহপুর্বক জাভাস্ক্রিপ্ট চালু করুন অথবা ব্রাউজার আপডেট করুন।</div>
    </noscript>

<div id="form-div">
    <div class="alert alert-danger" style="text-align: justify">
        নথি সিস্টেমের মাধ্যমে নিরাপদ কার্যক্রম পরিচালনার জন্য পরীক্ষামূলকভাবে SSL অন্তর্ভুক্ত করা হয়েছে। দয়া করে নির্বিঘ্নে কার্যক্রম পরিচালনার জন্য ব্রাউজার সাপেক্ষে নিরাপত্তা দিক নির্দেশনা জানতে
        <a data-toggle="modal" data-target="#ssl-instruction-modal"> এখানে ক্লিক করুন </a>।
    </div>
    <center><a type="button" href="<?= $this->Url->build(['controller' =>'Users','action' =>'login', '_ssl' => true]) ?>" class="btn blue btn-lg "> প্রবেশ করুন</a></center>
    <!-- SSl Instruction Modal -->
    <div id="ssl-instruction-modal" class="modal fade ">
        <div class="modal-dialog modal-lg" style="width: 90%">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <?php
                    $i = 1;
                    ?>
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-question-circle"></i>  ডিজিটাল সার্টিফিকেট  প্রযুক্তি ব্যবহার করতে আপনার করণীয়
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="panel-group accordion" id="accordion1">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#collapse_<?= $i ?>">
                                               <i class="fa fa-chrome" aria-hidden="true"></i> <b> Google Chrome এর ক্ষেত্রে করণীয় </b>
                                            </a>

                                        </h4>
                                    </div>
                                    <div id="collapse_<?= $i ?>" class="panel-collapse collapse">
                                        <div class="panel-body" style="font-size : 12pt!important;">
                                            <div class="row text-center">
                                                <br>
                                                <h4 class="text-center bold"><u> Google Chrome এর পুরোনো ভার্সনে </u></h4>
                                                <br>
                                                <img class="text-center" src="<?= $this->request->webroot.'img/ssl_images/image016.jpg' ?>">
                                                <br><br>
                                                ওয়েবসাইটে  প্রবেশ করতে <b>“proceed anyway”</b> লিখার উপর ক্লিক করুন  তাহলে আপনি ওয়েবসাইটে  প্রবেশ করবেন ।
                                                <br><br>
                                                <h4 class="text-center bold"><u> Google Chrome এর নতুন ভার্সনে </u></h4>
                                                <br>
                                                <span class="text-center bold"> ধাপ-১ </span>
                                                <br>
                                                <img class="text-center" src="<?= $this->request->webroot.'img/ssl_images/image031.jpg' ?>">
                                                <br><br>
                                                <b>“ADVANCED” লেখার উপর ক্লিক করুন</b>
                                                <br>
                                                <span class="text-center bold"> ধাপ-২ </span>
                                                <br>
                                                <img class="text-center" src="<?= $this->request->webroot.'img/ssl_images/image032.jpg' ?>">
                                                <br><br>
                                                ওয়েবসাইটে প্রবেশ করতে <b>“Proceed to www.nothi.gov.bd (unsafe)”</b> লিখার উপর ক্লিক করুন।
                                                <br><br>
                                                <span class="text-center bold"> ধাপ-৩ </span>
                                                <br>

                                                পরবর্তী কাজ সম্পাদন করুন
                                                <br>
                                                <img class="text-center" src="<?= $this->request->webroot.'img/ssl_images/image004.jpg' ?>">
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#collapse_<?= ++$i ?>">
                                               <i class="fa fa-firefox" aria-hidden="true"></i> <b> Mozilla Firefox এর ক্ষেত্রে করণীয় </b>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse_<?= $i ?>" class="panel-collapse collapse">
                                        <div class="panel-body" style="font-size : 12pt!important;">
                                            <br>
                                            <div class="text-center ">
                                                <br>
                                                www.nothi.gov.bd-তে ডিজিটাল সার্টিফিকেট  প্রযুক্তি ব্যবহার করার কারণে   <a href="https://www.mozilla.org/en-US/firefox/new/" style="decoration: none">Mozilla Firefox</a> -এ  নিচে প্রদর্শিত চিত্রের ন্যায় সতর্কবার্তা (warning message) পাবেন।
                                                <br>
                                                <h4 class="text-center bold"><u>  Mozilla Firefox এর পুরোনো ভার্সনে </u></h4>
                                                <br>
                                                <span class="text-center bold"> ধাপ-১ </span>
                                                <br>
                                                <img class="text-center" src="<?= $this->request->webroot.'img/ssl_images/image006.jpg' ?>">
                                                <br><br>
                                                ওয়েবসাইটে  প্রবেশ করতে<b>“I Understand the Risk”</b> লিখার উপর ক্লিক করুন ।তাহলে আপনি পরবর্তী চিত্রের ন্যায় আরেকটি অপশন পাবেন।
                                                <br>
                                                <span class="text-center bold"> ধাপ-২ </span>
                                                <br>
                                                <img class="text-center" src="<?= $this->request->webroot.'img/ssl_images/image032.jpg' ?>">
                                                <br><br>
                                                এখানে “Add Exception”বাটনে ক্লিক করতে হবে।
                                                <br><br>
                                                <span class="text-center bold"> ধাপ-৩ </span>
                                                <br>
                                                “Get Certificate ” বাটনে ক্লিক  করুণ ।
                                                <br>
                                                <img class="text-center" src="<?= $this->request->webroot.'img/ssl_images/image010.jpg' ?>">
                                                <br><br>
                                                <br><br>
                                                <span class="text-center bold"> ধাপ-৪ </span>
                                                <br>
                                                <b>“Confirm Security Exception”</b> বাটনে ক্লিক করে পরবর্তী চিত্রে প্রদর্শিত ওয়েব সাইটে প্রবেশ করুন ।
                                                <br>
                                                <img class="text-center" src="<?= $this->request->webroot.'img/ssl_images/image012.jpg' ?>">
                                                <br><br>
                                                <br><br>
                                                <span class="text-center bold"> ধাপ-৫ </span>
                                                <br>
                                                পরবর্তী কাজ সম্পাদন করুন ।
                                                <br>
                                                <img class="text-center" src="<?= $this->request->webroot.'img/ssl_images/image004.jpg' ?>">
                                                <br><br>
                                                <h4 class="text-center bold"><u> Mozilla Firefox এর নতুন ভার্সনে </u></h4>
                                                <br>
                                                <span class="text-center bold"> ধাপ-১ </span>
                                                <br>
                                                <img class="text-center" src="<?= $this->request->webroot.'img/ssl_images/image033.jpg' ?>">
                                                <br><br>
                                                পরবর্তী অপশন এ যেতে <b>“ADVANCED”</b> লেখার উপর ক্লিক করুন।
                                                <br>
                                                <span class="text-center bold"> ধাপ-২ </span>
                                                <br>
                                                <img class="text-center" src="<?= $this->request->webroot.'img/ssl_images/image034.jpg' ?>">
                                                <br><br>
                                                এখানে <b>“Add Exception”</b>বাটনে ক্লিক করুন।
                                                <br><br>
                                                <span class="text-center bold"> ধাপ-৩ </span>
                                                <br>

                                                <b>“Get Certificate ”</b> বাটনে ক্লিক  করুন ।
                                                <br>
                                                <img class="text-center" src="<?= $this->request->webroot.'img/ssl_images/image035.jpg' ?>">
                                                <span class="text-center bold"> ধাপ-৪ </span>
                                                <br>
                                                <b>“Confirm Security Exception” বাটনে ক্লিক করুন।
                                                <br>
                                                <img class="text-center" src="<?= $this->request->webroot.'img/ssl_images/image012.jpg' ?>">
                                                <br><br>
                                                <br><br>
                                                <span class="text-center bold"> ধাপ-৫ </span>
                                                <br>
                                                    পরবর্তী কাজ সম্পাদন করুন।
                                                <br>
                                                <img class="text-center" src="<?= $this->request->webroot.'img/ssl_images/image036.jpg' ?>">
                                                <br><br>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapse_<?= ++$i ?>">
                                                <i class="fa fa-opera" aria-hidden="true"></i><b> Opera এর ক্ষেত্রে করণীয় </b>
                                            </a>

                                        </h4>
                                    </div>
                                    <div id="collapse_<?= $i ?>" class="panel-collapse collapse">
                                        <div class="panel-body" style="font-size : 12pt!important;">
                                            <div class="row text-center">
                                                <br>
                                                www.nothi.gov.bd-তে ডিজিটাল সার্টিফিকেট  প্রযুক্তি ব্যবহার করার কারনে   <a href="http://www.opera.com/download" style="decoration: none">Opera</a>  -এ  নিচে প্রদর্শিত চিত্রের ন্যায় সতর্কবার্তা (warning message) পাবেন।
                                                <br>

                                                <span class="text-center bold"> ধাপ-১ </span>
                                                <br>
                                                ওয়েবসাইটে  প্রবেশ করতে <b>“Continue Anyway”</b> বাটনে ক্লিক করুন।
                                                <br>
                                                <img class="text-center" src="<?= $this->request->webroot.'img/ssl_images/image020.jpg' ?>">
                                                <br><br>
                                                <span class="text-center bold"> ধাপ-২ </span>
                                                <br>
                                                পরবর্তী কাজ সম্পাদন করুন।
                                                <br>
                                                <img class="text-center" src="<?= $this->request->webroot.'img/ssl_images/image004.jpg' ?>">
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapse_<?= ++$i ?>">
                                               <i class="fa fa-internet-explorer" aria-hidden="true"></i> <b> Internet Explorer এর ক্ষেত্রে করণীয় </b>
                                            </a>

                                        </h4>
                                    </div>
                                    <div id="collapse_<?= $i ?>" class="panel-collapse collapse">
                                        <div class="panel-body" style="font-size : 12pt!important;">
                                            <div class="row text-center">
                                                <br>
                                                www.nothi.gov.bd-তে ডিজিটাল সার্টিফিকেট  প্রযুক্তি ব্যবহার করার কারণে  Internet Explorer  -এ  নিচে প্রদর্শিত চিত্রের ন্যায় সতর্কবার্তা (warning message) পাবেন।
                                                <br>

                                                <span class="text-center bold"> ধাপ-১ </span>
                                                <br>
                                                ওয়েবসাইটে  প্রবেশ করতে <b>“Continue to this website (not recommended)”</b> বাটনে ক্লিক করুন।
                                                <br>
                                                <img class="text-center" src="<?= $this->request->webroot.'img/ssl_images/image002.jpg' ?>">
                                                <br><br>
                                                <span class="text-center bold"> ধাপ-২ </span>
                                                <br>
                                                প্রয়োজনীয় কাজ সম্পাদন করুন।
                                                <br>
                                                <img class="text-center" src="<?= $this->request->webroot.'img/ssl_images/image004.jpg' ?>">
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
            <!-- End Content -->

        </div>
    </div>
    <!-- End SSl Instruction Modal -->
</div>
<script src="<?php echo CDN_PATH; ?>js/client.min.js" type="text/javascript"></script>