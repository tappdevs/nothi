<!-- BEGIN SIDEBAR -->
<?php
$session         = $this->request->session();
$other_organogram_activities_permission['dak'] = $session->read('selected_office_section')['other_organogram_activities_permission_dak'];
$other_organogram_activities_permission['nothi'] = $session->read('selected_office_section')['other_organogram_activities_permission_nothi'];
$menu_items      = $session->read('menu_items');
$selected_module = $session->read('module_id');
?>
<div class="page-sidebar-wrapper" >
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar md-shadow-z-2-i  navbar-collapse collapse">
        <ul class="page-sidebar-menu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
            <li class="sidebar-toggler-wrapper">
                <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                <div class="sidebar-toggler">
                </div>
                <!-- END SIDEBAR TOGGLER BUTTON -->
            </li>

            <li class="start active ">
                <a href="<?= $this->request->webroot; ?>">
                    <?php
                    $icons           = 'fs0 a2i_gn_home1';
                    $iconTitle       = 'ড্যাশবোর্ড';
                    if ($selected_module == 5) {
                        $icons     = 'fs0 efile-email5';
                        $iconTitle = 'ডাক';
                    }
                    elseif ($selected_module == 4) {
                        $icons     = 'fs0 efile-nothi1';
                        $iconTitle = 'নথি';
                    }
                    elseif ($selected_module == 2) {
                        $icons     = 'fs0 efile-office1';
                        $iconTitle = 'দপ্তর';
                    }
                    elseif ($selected_module == 1) {
                        $icons     = 'fs0 efile-office1';
                        $iconTitle = 'সেটিংস';
                    }
                     elseif ($selected_module == 7) {
                        $icons     = 'fs0 efile-office1';
                        $iconTitle = 'প্রতিবেদন';
                    }
                    elseif ($selected_module == 8) {
                        $icons     = 'fs0 efile-note1';
                        $iconTitle = 'রিপোর্ট';
                    }
                    ?>
                    <i class="<?= $icons ?>"></i>
                    <span class="title"><?= __($iconTitle) ?></span>
                    <!--<span class="arrow "></span>-->
                </a>
            </li>
            <?php
            if ($selected_module == 5) {
                echo $this->element('LeftBar/dak_left_bar', ['other_organogram_activities_permission' => $other_organogram_activities_permission]);
            }
            elseif ($selected_module == 4) {
                echo $this->element('LeftBar/nothi_left_bar', ['other_organogram_activities_permission' => $other_organogram_activities_permission]);
            }
            elseif ($selected_module == 2) {
                echo $this->element('LeftBar/doptor_left_bar', ['session' => $session]);
               
            }
            elseif ($selected_module == 1) {
                if($loggedUser['user_role_id'] == 2){
                    echo $this->element('LeftBar/setting_left_bar_for_robin_layer');
                }else{
                    echo $this->element('LeftBar/setting_left_bar');
                }
                  
            }
            elseif ($selected_module == 7) {
                  echo $this->element('LeftBar/protibedon_left_bar');
            }
            elseif ($selected_module == 8) {
                  echo $this->element('LeftBar/report_left_bar');
            }
            ?>
        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
</div>
<!-- END SIDEBAR -->

<script>
    $(document).find('.page-sidebar-menu li').find('a').click(function () {
        var href = $(this).attr('href');
        var db = ProjapotiOffline.createDatabase('nothi_menu');
        var docid = 'nothi_menu_expand';

        if (href != 'javascript:;' && href != '') {
            db.get(docid).then(function (doc) {

                return db.put({
                    _id: docid,
                    _rev: doc._rev,
                    menu_url: href
                });
            }).then(function (response) {

            }).catch(function (err) {

                return db.put({
                    _id: docid,
                    menu_url: href
                });
            });
        }
    });


    $(function () {
        var db = ProjapotiOffline.createDatabase('nothi_menu');
        var docid = 'nothi_menu_expand';

        db.get(docid).then(function (doc) {

            activeMenu(doc.menu_url);
        }).then(function (response) {

        }).catch(function (err) {

        });

        function activeMenu(url) {
            $('.page-sidebar-menu').find('li').removeClass('active');

            $.each($('.page-sidebar-menu li'), function (i) {

                if ($(this).find('a').attr('href') == url) {
                    if ($(this).closest('ul').hasClass('sub-menu')) {
                        $(this).closest('li').addClass('active');
                        $(this).closest('ul.sub-menu').attr('style', 'display:block;');
                        $(this).closest('ul.sub-menu').parents('li').addClass('open').addClass('active');
                    } else {
                        $(this).closest('li').addClass('active');
                    }
                }
            });
        }
    })
</script>