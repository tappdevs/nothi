
<!--[if lt IE 9]>
      <script src="<?php echo CDN_PATH; ?>assets/global/plugins/respond.min.js"></script>
      <script src="<?php echo CDN_PATH; ?>assets/global/plugins/excanvas.min.js"></script>
      <![endif]-->

<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap/js/bootstrap.min.js"
type="text/javascript"></script>
<script
    src="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"
type="text/javascript"></script>
<script src="<?php echo CDN_PATH; ?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js"
type="text/javascript"></script>
<script src="<?php echo CDN_PATH; ?>assets/global/plugins/uniform/jquery.uniform.min.js"
type="text/javascript"></script>
<script src="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js"
type="text/javascript"></script>
<script type="text/javascript"
src="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>

<script type="text/javascript"
src="<?php echo CDN_PATH; ?>assets/global/plugins/fuelux/js/spinner.min.js"></script>
<script type="text/javascript"
src="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
<script type="text/javascript"
src="<?php echo CDN_PATH; ?>assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>
<script type="text/javascript"
src="<?php echo CDN_PATH; ?>assets/global/plugins/jquery.input-ip-address-control-1.0.min.js"></script>
<script src="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js"
type="text/javascript"></script>
<script src="<?php echo CDN_PATH; ?>assets/global/plugins/jquery-tags-input/jquery.tagsinput.min.js"
type="text/javascript"></script>
<script src="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js"
type="text/javascript"></script>
<script src="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-touchspin/bootstrap.touchspin.js"
type="text/javascript"></script>
<script src="<?php echo CDN_PATH; ?>assets/global/plugins/typeahead/handlebars.min.js"
type="text/javascript"></script>
<script src="<?php echo CDN_PATH; ?>assets/global/plugins/typeahead/typeahead.bundle.js"
type="text/javascript"></script>


<script src="<?php echo CDN_PATH; ?>assets/global/plugins/jquery.sparkline.min.js"
type="text/javascript"></script>


<script src="<?php echo CDN_PATH; ?>assets/global/plugins/jstree/dist/jstree.min.js"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script src="<?php echo CDN_PATH; ?>assets/admin/pages/scripts/ui-tree.js"></script>

<script src="<?php echo CDN_PATH; ?>assets/admin/layout4/scripts/layout.js"
type="text/javascript"></script>
<script src="<?php echo CDN_PATH; ?>assets/admin/layout4/scripts/demo.js" type="text/javascript"></script>
<script src="<?php echo CDN_PATH; ?>assets/admin/layout4/scripts/projapoti_ajax.js?v=<?= js_css_version ?>" type="text/javascript"></script>
<script src="<?php echo CDN_PATH; ?>assets/admin/pages/scripts/tasks.js" type="text/javascript"></script>
<script type="text/javascript"
src="<?php echo CDN_PATH; ?>assets/global/plugins/jquery-validation/js/jquery.validate.min.js"></script>
<script type="text/javascript"
src="<?php echo CDN_PATH; ?>assets/global/plugins/jquery-validation/js/additional-methods.min.js"></script>
<script src="<?php echo CDN_PATH; ?>assets/admin/pages/scripts/form-validation.js"></script>
<!-- END PAGE LEVEL SCRIPTS -->


<script src="<?php echo CDN_PATH; ?>assets/global/plugins/jquery-file-upload/js/vendor/jquery.ui.widget.js"></script>
<!-- The Templates plugin is included to render the upload/download listings -->
<script
src="<?php echo CDN_PATH; ?>assets/global/plugins/jquery-file-upload/js/vendor/tmpl.min.js"></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="<?php echo CDN_PATH; ?>assets/global/plugins/jquery-file-upload/js/vendor/load-image.min.js"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="<?php echo CDN_PATH; ?>assets/global/plugins/jquery-file-upload/js/vendor/canvas-to-blob.min.js"></script>
<!-- blueimp Gallery script -->
<script    src="<?php echo CDN_PATH; ?>assets/global/plugins/jquery-file-upload/blueimp-gallery/jquery.blueimp-gallery.min.js"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script
src="<?php echo CDN_PATH; ?>assets/global/plugins/jquery-file-upload/js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="<?php echo CDN_PATH; ?>assets/global/plugins/jquery-file-upload/js/jquery.fileupload.js"></script>
<!-- The File Upload processing plugin -->
<script src="<?php echo CDN_PATH; ?>assets/global/plugins/jquery-file-upload/js/jquery.fileupload-process.js"></script>
<!-- The File Upload image preview & resize plugin -->
<script src="<?php echo CDN_PATH; ?>assets/global/plugins/jquery-file-upload/js/jquery.fileupload-image.js"></script>
<!-- The File Upload audio preview plugin -->
<script
src="<?php echo CDN_PATH; ?>assets/global/plugins/jquery-file-upload/js/jquery.fileupload-audio.js"></script>
<!-- The File Upload video preview plugin -->
<script
src="<?php echo CDN_PATH; ?>assets/global/plugins/jquery-file-upload/js/jquery.fileupload-video.js"></script>
<!-- The File Upload validation plugin -->
<script src="<?php echo CDN_PATH; ?>assets/global/plugins/jquery-file-upload/js/jquery.fileupload-validate.js"></script>
<!-- The File Upload user interface plugin -->
<script src="<?php echo CDN_PATH; ?>assets/global/plugins/jquery-file-upload/js/jquery.fileupload-ui.js"></script>
<!-- The main application script -->
<!-- The XDomainRequest Transport is included for cross-domain file deletion for IE 8 and IE 9 -->
<!--[if (gte IE 8)&(lt IE 10)]>
<script
    src="<?php echo CDN_PATH; ?>assets/global/plugins/jquery-file-upload/js/cors/jquery.xdr-transport.js"></script>
<![endif]-->
<!-- END:File Upload Plugin JS files-->

<script type="text/javascript"
src="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript"
src="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
<script type="text/javascript"
src="<?php echo CDN_PATH; ?>assets/global/plugins/clockface/js/clockface.js"></script>


<script type="text/javascript"
src="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-daterangepicker/moment-with-locales.min.js"></script>


<script type="text/javascript"
src="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>

<script type="text/javascript"
src="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript"

<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2.min.js"></script>
    <script type="text/javascript"
    src="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
    <script type="text/javascript"
    src="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>


    <script type="text/javascript"
    src="<?php echo CDN_PATH; ?>assets/global/plugins/jquery.mockjax.js"></script>

    <script type="text/javascript"
    src="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js"></script>
    <script type="text/javascript"
    src="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-editable/inputs-ext/address/address.js"></script>
    <script type="text/javascript"
    src="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-editable/inputs-ext/wysihtml5/wysihtml5.js"></script>

    <script src="<?php echo CDN_PATH; ?>assets/admin/pages/scripts/form-editable.js"></script>
    <script src="<?php echo CDN_PATH; ?>assets/admin/pages/scripts/form-fileupload.js?v=<?= time() ?>"></script>

    <script src="<?php echo CDN_PATH; ?>assets/admin/pages/scripts/inbox.js" type="text/javascript"></script>
    <script src="<?php echo CDN_PATH; ?>assets/admin/layout4/scripts/office_setup.js" type="text/javascript"></script>
    <script src="<?php echo CDN_PATH; ?>assets/admin/layout4/scripts/projapoti_autocomplete.js" type="text/javascript"></script>

    <!-- Start Metronic File Upload -->
    <script src="<?php echo CDN_PATH; ?>assets/admin/pages/scripts/components-form-tools.js"></script>
    <script src="<?php echo CDN_PATH; ?>assets/admin/pages/scripts/components-pickers.js"></script>
    <!-- END -->
    <!-- Notification JS -->


    <script type="text/javascript">
        jQuery(document).ready(function () {
            Metronic.init();
            Layout.init(); // init layout
            Demo.init(); // init demo features
            UITree.init();
            FormValidation.init();
            ComponentsPickers.init();
        });
		$('select').not('[class*=protikolpo_]').select2();
		$('select[class*=protikolpo_]').select2({width: '350px'});
          /***Custom Bangla From English (Important)***/
          function enTobn(input){
             return BnFromEng(input);
          }
        function bnToen(input){
            return EngFromBn(input);
        }
        function BntoEng(input){
            return EngFromBn(input);
        }
            function BnFromEng(input) {
                var numbers = {
                    0: '০',
                    1: '১',
                    2: '২',
                    3: '৩',
                    4: '৪',
                    5: '৫',
                    6: '৬',
                    7: '৭',
                    8: '৮',
                    9: '৯'
                };
                var output = '';

                if (typeof (input) == 'number') {
                    input = input.toString();
                }
                for (var i = 0; i < input.length; ++i) {
                    if (numbers.hasOwnProperty(input[i])) {
                        output += numbers[input[i]];
                    } else {
                        output += input[i];
                    }
                }
                return output;
            }
            function EngFromBn(input) {
                var numbers = {
                     '০':0,
                     '১':1,
                     '২':2,
                     '৩':3,
                     '৪':4,
                     '৫':5,
                     '৬':6,
                     '৭':7,
                     '৮':8,
                     '৯':9
                };
                var output = '';

                if (typeof (input) == 'number') {
                    input = input.toString();
                }
                for (var i = 0; i < input.length; ++i) {
                    if (numbers.hasOwnProperty(input[i])) {
                        output += numbers[input[i]];
                    } else {
                        output += input[i];
                    }
                }
                return output;
            }
        var entityMap = {
            '&': '&amp;',
            '<': '&lt;',
            '>': '&gt;',
            '"': '&quot;',
            "'": '&#39;',
            '/': '&#x2F;',
            '`': '&#x60;',
            '=': '&#x3D;'
        };
        function escapeHtml (string) {
            return String(string).replace(/[&<>"'`=\/]/g, function (s) {
                return entityMap[s];
            });
        }
        function number_format(input){
            var output = '';

            if (typeof (input) == 'number') {
                input = input.toString();
            }
            var len = (input.indexOf('.') == -1)? input.length:input.indexOf('.');
            console.log(len,input.length,input.indexOf('.'));
            if(len < input.length){
                for (var i = input.indexOf('.'); i < input.length; i++) {
                 output += input[i];
                }
            }
            var thousandsCount = 0;
            for (var i = len; i > 0; i--) {
                var char_ = input.substr(i - 1, 1);
                console.log(char_);
                if(!isNaN(char_)){
                    thousandsCount++;
                }
                if (thousandsCount % 3 == 0){
                    thousandsCount = 0;
                    if(i != 1){
                        char_ = ',' + char_;
                    }
                }
                output = char_ + output;
            }
            return output;

        }
    </script>
    <!---CDN-->
    <!---CDN-->
    <!-- END JAVASCRIPTS -->
    <?php
    $this->fetch('script');
    ?>

<script src="<?php echo CDN_PATH; ?>assets/global/scripts/nothi-custom.js?v=<?= js_css_version ?>" type="text/javascript"></script>

<script>
    $(".modal.modal-purple").find('.modal-header').find('button[data-dismiss=modal]').on('click', function() {
        $(this).closest('.modal-purple').find('.btn.red').trigger('click');
    });
    $('a,button').on('click', function () {
        var tooltitpId = $(this).attr('aria-describedby');
        if(!isEmpty(tooltitpId)){
            $('.tooltip#'+tooltitpId).remove();
        }
    });
</script>