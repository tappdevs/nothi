<link rel="stylesheet" href="<?php echo $this->request->webroot; ?>a2i/demo-files/demo.css">
<link rel="stylesheet" href="<?php echo $this->request->webroot; ?>a2i/style.css">

<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="<?php echo $this->request->webroot; ?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo CDN_PATH; ?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css"
      rel="stylesheet" type="text/css"/>
<link href="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap/css/bootstrap.min.css"
      rel="stylesheet" type="text/css"/>
<link href="<?php echo CDN_PATH; ?>assets/global/plugins/uniform/css/uniform.default.css"
      rel="stylesheet" type="text/css"/>
<link href="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css"
      rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->

<!-- BEGIN PLUGINS USED BY X-EDITABLE -->
<link rel="stylesheet" type="text/css"
      href="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css"
      href="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css"/>
<link rel="stylesheet" type="text/css"
      href="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-datepicker/css/datepicker.css"/>
<link rel="stylesheet" type="text/css"
      href="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css"/>
<link rel="stylesheet" type="text/css"
      href="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"/>
<link rel="stylesheet" type="text/css"
      href="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-editable/bootstrap-editable/css/bootstrap-editable.css"/>
<link rel="stylesheet" type="text/css"
      href="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-editable/inputs-ext/address/address.css"/>
<!-- END PLUGINS USED BY X-EDITABLE -->

<!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
<link href="<?php echo CDN_PATH; ?>assets/global/plugins/jstree/dist/themes/default/style.min.css"/>
<link
        href="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"
        rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css"
      href="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"/>
<link href="<?php echo CDN_PATH; ?>assets/global/plugins/fullcalendar/fullcalendar.min.css"
      rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css"
      href="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<link rel="stylesheet" type="text/css"
      href="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css"/>
<link rel="stylesheet" type="text/css"
      href="<?php echo CDN_PATH; ?>assets/global/plugins/jquery-tags-input/jquery.tagsinput.css"/>
<link rel="stylesheet" type="text/css"
      href="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css">
<link rel="stylesheet" type="text/css"
      href="<?php echo CDN_PATH; ?>assets/global/plugins/typeahead/typeahead.css">

<!-- END PAGE LEVEL PLUGIN STYLES -->
<!-- BEGIN PAGE STYLES -->
<link href="<?php echo CDN_PATH; ?>assets/admin/pages/css/tasks.css" rel="stylesheet"
      type="text/css"/>
<link rel="stylesheet" type="text/css"
      href="<?php echo CDN_PATH; ?>assets/global/plugins/jstree/dist/themes/default/style.min.css"/>
<link href="<?php echo CDN_PATH; ?>assets/admin/pages/css/error.css" rel="stylesheet"
      type="text/css"/>
<!-- END PAGE STYLES -->
<link href="<?php echo CDN_PATH; ?>assets/global/css/components.css" id="style_components" rel="stylesheet"
      type="text/css"/>
<link href="<?php echo CDN_PATH; ?>assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo CDN_PATH; ?>assets/admin/layout4/css/layout.css" rel="stylesheet" type="text/css"/>
<!--Need To add custom.css for -->
<link href="<?php echo CDN_PATH; ?>assets/admin/layout4/css/custom.css" rel="stylesheet" type="text/css"/>
<!--Quick Sidebar-->
<link href="<?php echo CDN_PATH; ?>assets/admin/layout4/css/themes/light.css" rel="stylesheet" type="text/css"
      id="style_color"/>

<!--Need To add nothi-custom.css for -->
<link href="<?php echo CDN_PATH; ?>assets/global/css/nothi-custom.css" rel="stylesheet" type="text/css"/>

<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?php echo CDN_PATH; ?>assets/global/plugins/jquery-file-upload/blueimp-gallery/blueimp-gallery.min.css"
      rel="stylesheet"/>
<link href="<?php echo CDN_PATH; ?>assets/global/plugins/jquery-file-upload/css/jquery.fileupload.css"
      rel="stylesheet"/>
<link href="<?php echo CDN_PATH; ?>assets/global/plugins/jquery-file-upload/css/jquery.fileupload-ui.css"
      rel="stylesheet"/>
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css"
      rel="stylesheet" type="text/css"/>
<link href="<?php echo CDN_PATH; ?>assets/global/plugins/fancybox/source/jquery.fancybox.css"
      rel="stylesheet"/>
<!-- BEGIN:File Upload Plugin CSS files-->
<link href="<?php echo CDN_PATH; ?>assets/global/plugins/jquery-file-upload/blueimp-gallery/blueimp-gallery.min.css"
      rel="stylesheet"/>
<link href="<?php echo CDN_PATH; ?>assets/global/plugins/jquery-file-upload/css/jquery.fileupload.css"
      rel="stylesheet"/>
<link
        href="<?php echo CDN_PATH; ?>assets/global/plugins/jquery-file-upload/css/jquery.fileupload-ui.css"
        rel="stylesheet"/>
<!-- END:File Upload Plugin CSS files-->
<!-- END PAGE LEVEL STYLES -->
<link href="<?php echo CDN_PATH; ?>assets/admin/pages/css/profile.css" rel="stylesheet"
      type="text/css"/>
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?php echo CDN_PATH; ?>assets/admin/pages/css/inbox.css?v=<?= js_css_version ?>" rel="stylesheet"
      type="text/css"/>
<link rel="stylesheet" href="<?php echo CDN_PATH; ?>css/validationEngine.jquery.css" type="text/css"/>

<link rel="stylesheet" type="text/css"
      href="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css"/>
<link rel="stylesheet" type="text/css"
      href="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css"/>
<link rel="stylesheet" type="text/css"
      href="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-colorpicker/css/colorpicker.css"/>
<link rel="stylesheet" type="text/css"
      href="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<link rel="stylesheet" type="text/css"
      href="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"/>

<link rel="stylesheet" type="text/css"
      href="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-toastr/toastr.min.css"/>
<style>
    .class2 thead tr th, .class2 tbody tr td {
        border: 1px solid #0c0c0c;
    }
</style>

<link rel="shortcut icon" href="<?php echo CDN_PATH; ?>favicon.ico"/>
<script src="<?php echo CDN_PATH ?>assets/global/plugins/jquery.min.js"
        type="text/javascript"></script>

<script src="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-toastr/toastr.min.js"></script>
<script src="<?php echo CDN_PATH; ?>assets/admin/pages/scripts/ui-toastr.js"></script>

<script src="<?php echo CDN_PATH; ?>projapoti_offline/pouchdb.min.js"
        type="text/javascript"></script>
<script src="<?php echo CDN_PATH; ?>projapoti_offline/pouchdb.find.js"
        type="text/javascript"></script>
<script src="<?php echo CDN_PATH; ?>projapoti_offline/offline_plugin.js"
        type="text/javascript"></script>

<script src="<?php echo CDN_PATH; ?>assets/global/plugins/bootbox/bootbox.min.js" type="text/javascript"></script>

<script src="<?php echo CDN_PATH; ?>assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="<?php echo CDN_PATH; ?>assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="<?php echo CDN_PATH; ?>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?php echo CDN_PATH; ?>assets/global/scripts/metronic.js" type="text/javascript"></script>

<?php
if (Live == 1) {
    ?>
    <script id="fr-fek">try {
            (function (k) {
                localStorage.FEK = k;
                t = document.getElementById('fr-fek');
                t.parentNode.removeChild(t);
            })('xc1We1KYi1Ta1WId1CVd1F==')
        } catch (e) {
        }</script>
    <?php
}
?>
<script type="text/javascript">
    var js_wb_root = '<?php echo $this->request->webroot; ?>';
    var ds_url = '<?php echo (Live == 1)? 'http://signature.nothi.gov.bd/': $this->request->webroot?>';
//    var ds_url = 'http://nothi.tappware.com/';
    var js_paginate_type = '<?php
        if (defined("PAGINATE_TYPE")) echo PAGINATE_TYPE;
        else 1
        ?>';

    /***Custom Bangla From English (Important)***/
    function En2Bn(input) {
        var numbers = {
            0: '০',
            1: '১',
            2: '২',
            3: '৩',
            4: '৪',
            5: '৫',
            6: '৬',
            7: '৭',
            8: '৮',
            9: '৯'
        };
        var output = '';

        if (typeof (input) == 'number') {
            input = input.toString();
        }
        for (var i = 0; i < input.length; ++i) {
            if (numbers.hasOwnProperty(input[i])) {
                output += numbers[input[i]];
            } else {
                output += input[i];
            }
        }
        return output;
    }

    $(function () {
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-bottom-right"
        };
        //check for offlinedraft
        //            setInterval(function () {
        //                var db = ProjapotiOffline.createDatabase('offline_draft');
        //                db.allDocs({
        //                    include_docs: true,
        //                    attachments: true
        //                }).then(function (result) {
        //                    $.each(result.rows, function (i, v) {
        //                        $.ajax({
        //                            url: v.doc.url,
        //                            data: v.doc.data,
        //                            method: v.doc.method,
        //                            dataType: typeof(v.doc.datatype) != 'undefined' ? v.doc.datatype : 'JSON',
        //                            async: false,
        //                            success: function (response) {
        //                                if (typeof(v.doc.datatype) != 'undefined' && v.doc.datatype == 'JSON') {
        //                                    if (response.status == 'error') {
        ////                                        toastr.error(response.msg);
        //                                    } else {
        //                                        //toastr.success(response.msg);
        //                                    }
        //                                }
        //                                ProjapotiOffline.deleteData(db, v.doc._id, v.doc._rev);
        //                            },
        //                            error: function (xhr, status, errorThrown) {
        //
        //                            }
        //                        });
        //                    })
        //
        //                }).catch(function (err) {
        //                    console.log(err);
        //                });
        //            }, 3000);
    });
    function isEmpty(value){
        if(typeof(value) == 'undefined' || value == '' || value == null ||  value == 0){
            return true;
        }
        return false;
    }
</script>
<style>
    .mce-branding-powered-by {
        visibility: hidden !important;
    }

    * {
        font-size: 13pt;
    }

    .template-download {
        word-break: break-word;
    }

    .sub-menu .title {
        font-size: 11pt;
    }

    .tooltip, .tooltip-inner {
        font-size: 10pt;
    }
</style>
<?php //echo $this->element('notification/notification_related') ?>