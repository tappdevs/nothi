<div class="modal fade modal-purple" id="previewModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="bootbox-close-button" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <?= $this->Form->create(null, ['id' => 'marginform']) ?>
                    <div class="col-md-2 col-sm-3 col-xs-4 col-lg-2 form-group">
                        <?= $this->Form->input('meta', ['class' => 'form-control input-sm ', 'label' => 'ডিফল্ট', 'options' => [
                            0 => 'হ্যাঁ',
                            1 => 'না'
                        ], 'type' => 'select']) ?>
                    </div>
                    <div class="col-md-1 col-sm-2 col-xs-3 col-lg-1 form-group">
                        <?= $this->Form->input('margin_top', ['class' => 'form-control input-sm ', 'label' => 'উপর', 'default' => '1.0', 'type' => 'number', 'precision' => 2, 'step' => .25, 'min' => 0, 'disabled' => true]) ?>
                    </div>
                    <div class="col-md-1 col-sm-2 col-xs-3 col-lg-1 form-group">
                        <?= $this->Form->input('margin_bottom', ['class' => 'form-control input-sm', 'label' => 'নিচ', 'default' => '.75', 'type' => 'number', 'precision' => 2, 'step' => .25, 'min' => 0, 'disabled' => true]) ?>
                    </div>
                    <div class="col-md-1 col-sm-2 col-xs-3 col-lg-1 form-group">
                        <?= $this->Form->input('margin_left', ['class' => 'form-control input-sm', 'label' => 'বাম', 'default' => '0.75', 'type' => 'number', 'precision' => 2, 'step' => .25, 'min' => 0, 'disabled' => true]) ?>
                    </div>
                    <div class="col-md-1 col-sm-2 col-xs-3 col-lg-1 form-group">
                        <?= $this->Form->input('margin_right', ['class' => 'form-control input-sm', 'label' => 'ডান', 'default' => '0.75', 'type' => 'number', 'precision' => 2, 'step' => .25, 'min' => 0, 'disabled' => true]) ?>
                    </div>
                    <div class="col-md-2 col-sm-3 col-xs-4 col-lg-2 form-group">
                        <?= $this->Form->input('orientation', ['class' => 'form-control input-sm', 'label' => 'ধরন', 'type' => 'select', 'options' => ['portrait' => 'Portrait', 'landscape' => 'Landscape'],'disabled' => true]) ?>
                    </div>
                    <div class="col-md-2 col-sm-3 col-xs-4 col-lg-2 form-group">
                        <?= $this->Form->input('potro_header', ['class' => 'form-control input-sm', 'label' => __("Banner"), 'type' => 'select', 'options' => [0 => 'হ্যাঁ', 1 => 'না'],'disabled' => true]) ?>
                    </div>
                    <div class="col-md-2 col-sm-3 col-xs-4 col-lg-2 form-group">
                        <br/>
                        <div class="btn blue btn-sm btn-pdf-margin" type="button"><i class="fa fa-binoculars"></i>
                            প্রিভিউ
                        </div>
                        <a class="btn btn-info btn-sm btn-pdf-download hide btn-sm" target="_blank"  type="button"><i class="fa fa-download"></i>
                            ডাউনলোড
                        </a>
                    </div>
                    <?= $this->Form->end() ?>
                </div>

                <div class="loading text-center font-lg" style="display:none;"><i
                            class="fa fa-spinner fa-2x fa-spin"></i></div>
                <embed class="showPreview" src="" style="width:100%; height: calc(100vh - 250px);" type="application/pdf"></embed>
            </div>
            <div class="modal-footer">
                <button aria-hidden="true" class="btn red pull-right" type="button" onclick="$('#previewModal').modal('hide')" data-dismiss="modal"></i>
                    বন্ধ করুন
                </button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<?= $this->Html->script('assets/global/scripts/printThis.js') ?>
<script>
    $('#meta').change(function () {
        if ($(this).val() == 1) {
            $('#marginform').find('#margin-top').removeAttr('disabled');
            $('#marginform').find('#margin-right').removeAttr('disabled');
            $('#marginform').find('#margin-bottom').removeAttr('disabled');
            $('#marginform').find('#margin-left').removeAttr('disabled');
            $('#marginform').find('#orientation').removeAttr('disabled');
            $('#marginform').find('#potro-header').removeAttr('disabled');

            var metadata = $.cookie('meta_data_val');
            if(typeof metadata !='undefined') {
                metadata = JSON.parse($.cookie('meta_data_val'));
                $('#marginform').find('#margin-top').val(metadata.margin_top)
                $('#marginform').find('#margin-bottom').val(metadata.margin_bottom)
                $('#marginform').find('#margin-right').val(metadata.margin_right)
                $('#marginform').find('#margin-left').val(metadata.margin_left)
                $('#marginform').find('#orientation').val(metadata.orientation)
                $('#marginform').find('#potro-header').val(metadata.potro_header)
            }

        } else {
            $('#marginform').find('#margin-top').attr('disabled', 'disabled');
            $('#marginform').find('#margin-right').attr('disabled', 'disabled');
            $('#marginform').find('#margin-bottom').attr('disabled', 'disabled');
            $('#marginform').find('#margin-left').attr('disabled', 'disabled');
            $('#marginform').find('#orientation').attr('disabled', 'disabled');
            $('#marginform').find('#potro-header').attr('disabled', 'disabled');
        }
    });

    function showPdf(title, url) {
        $('#previewModal').find('.btn-pdf-download').removeAttr('href').addClass('hide');
        $('#previewModal').find('.showPreview').removeAttr('src');
        $('#previewModal').modal('show');
        $('#previewModal').find('.modal-title').text((typeof(title) != 'undefined' ? title : 'প্রিন্ট প্রিভিউ'));
        $(document).off('click', '.btn-pdf-margin').on('click', '.btn-pdf-margin', function () {
            $.ajax({
                async: true,
                type: "POST",
                url: url,
                dataType: 'json',
                data: {
                    margin_top: parseFloat($('#marginform').find('#margin-top').val()),
                    margin_right: parseFloat($('#marginform').find('#margin-right').val()),
                    margin_bottom: parseFloat($('#marginform').find('#margin-bottom').val()),
                    margin_left: parseFloat($('#marginform').find('#margin-left').val()),
                    orientation: $('#marginform').find('#orientation').val(),
                    potro_header: $('#marginform').find('#potro-header').val(),
                    meta: $('#marginform').find('#meta').val()
                },
                beforeSend: function (jqXHR) {
                    $('#previewModal').find('.loading').show();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $('#previewModal').find('.loading').hide();
                    toastr.error("Preview load error");
                },
                success: function (response, textStatus) {
                    $('#previewModal').find('.loading').hide();
                    if (response.status == 'success') {
                        $('#previewModal').find('.btn-pdf-download').attr('href',response.src).removeClass('hide');
                        $('#previewModal').find('.showPreview').attr('src', response.src);
                    } else {
                        $('#previewModal').find('.loading').hide();
                        $('#previewModal').find('.btn-pdf-download').removeAttr('href').addClass('hide');
                        toastr.error(response.msg);
                    }
                }
            });
        });
    }

	function showPrintView(node) {
		node.printThis({
			importCSS: true,
			debug: false,
			importStyle: true,
			printContainer: false,
			pageTitle: "",
			removeInline: false,
			header: null
		});
	}
</script>