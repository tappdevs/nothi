<?php if(SSO_LOGIN) : ?>
    <link rel="stylesheet" type="text/css" href="http://103.48.18.28/sso/lib/style.css"/>
    <script type="text/javascript" src="http://account.beta.doptor.gov.bd/sso/lib/script.2.min.js"> </script>
<?php endif; ?>
<?php 
    $session = $this->request->session(); 
    $modules = $session->read('modules');
    $employee = $session->read('selected_office_section');
    $logged_employee_record = $session->read('logged_employee_record');
    $current_module = "";
    $current_module_letter_counter = "";
?>
<!-- <script src="https://js.pusher.com/4.3/pusher.min.js"></script> -->
<!-- <script>
    // Enable pusher logging - don't include this in production
    Pusher.logToConsole = true;

    var pusher = new Pusher('8b8dc3ded365eb98880e', {
      cluster: 'ap1',
      forceTLS: true
    });
    var channel;

    function receivePusher($chanel=null,$event=null)
    {
        var $_chanel = 'channel-'+$chanel;
        channel = pusher.subscribe($_chanel);
    
        channel.bind($event, function(data) {
            var msg =  JSON.parse(data.message);
            var lbl = 'lbl_'+$event;
            //console.log(lbl.parentElement.innerHTML);
            var nothi_counter = Number(bnToen(!isEmpty(document.getElementById(lbl).innerHTML)?document.getElementById(lbl).innerHTML:0))+1;
            document.getElementById(lbl).innerHTML = enTobn(nothi_counter);
            //toastr.info(msg.message);
        });
    }
    
    receivePusher("<?//= $employee['officer_id'];?>","<?//='nothi';?>");
    receivePusher("<?//= $employee['officer_id'];?>","<?//='dak';?>");
 
</script>
<?php //echo $this->element('push_notification_js',['officer_id'=>$employee['officer_id']])?> -->
<style>
    .badge {
        display: inline;
    }

    .page-header.navbar .top-menu .navbar-nav > li.dropdown-user > .dropdown-toggle > .username {
        color: #fff !important;
    }

    .page-header.navbar .top-menu .navbar-nav > li.dropdown-user > .dropdown-toggle > i {
        color: #fff !important;
    }

    .monitorButton {
        background-color: #35aa47 !important;
    }

    .monitorButton:hover {
        background-color: #2d8f3c !important;
    }

    .page-header.navbar .top-menu .navbar-nav > li.dropdown-user .dropdown-toggle {
        padding: 5px;
    }
    .sso-logo-box{
        margin:10px;
    }
    .sso-logo{
        width:90px;
        height:90px;
    }
    .sso-logo-title{
        color:black;
        margin: 0 0 0 20px ;
        text-align:center;

    }

    .fa-mobile-phone
    {
        font-size:130%;
    }

    ul.sub-menu-user li a:hover
    {
        background:#ddd;
    }

    ul.sub-menu-user li a
    {
        padding-top:15px;
        padding-bottom:15px;
        border-bottom:1px solid #fff;
    }
    .page-top .nav .sub-menu-user li:nth-last-child
    {
        border-bottom:0px solid #ccc;
    }

    ul.xs-module-menu
    {
        margin-left:-20px;
        text-align:left;
        padding-bottom:15px;
        width:100%;
    }

    ul.xs-module-menu li
    {
        list-style:none;
    }

    ul.xs-module-menu .btn
    {
        display:block;
        width:100%;
        margin-bottom:2px;
    }

    div.username, div.username div 
    {
        overflow:hidden;
        text-overflow:ellipsis;
        white-space:nowrap;
        margin-bottom:5px;
    }
    div.username div.h5
    {
        margin-top:2px;
        margin-bottom:2px;
    }

    .margin-top-30
    {
        margin-top:30px;
    }

    .margin-top-40
    {
        margin-top:40px;
    }

  
    .right-collapse-bar
    {
        cursor:pointer;
        color:#fff;
        margin-right: 20px;
        display: inline-flex;
        width: 40px;
    }

    .menu-user
    {
        color:#fff;
        max-width:fit-content;
        display:flex;
        margin-left:auto;
        margin-right:10px;
    }

    .menu-user a.menu-link:hover
    {
        text-decoration:none;
    }
    .menu-user div.user-menu-designation
    {
        color:#fff;
        max-width:250px;
        overflow:hidden;
        text-overflow:ellipsis;
        white-space:nowrap;
        
    }

    .menu-user div.row
    {
       display:flex;
    }

    .menu-user .glyphicon 
    {
        color:#fff;
    }

    ul.sub-menu-user
    {
        background-color:#F2F3F4;
        width:250px !important;
        margin-top:0px;
        position:absolute;
        left:auto;
        right:7%;
    }

    .xs-menu-lbl
    {
        color:#fff;
        font-size:85%;
        margin-top:15px !important;
        max-width:70%;
        border-radius:5px !important;
        margin-left:10px;
        padding-right:20px;
        
    }

    .xs-menu-lbl:hover, .xs-menu-lbl:focus
    {
        color:#fff;
    }

    .xs-menu-nav
    {
        width:120px;
        top:4px;
        position:absolute;
        display:grid;
        min-height:1px;
        background:transparent;
        border:0px;   
    }

    #moduleNavbar .btn
    {
        border-radius:5px !important;
        margin-left:-10px;
    }

    #xs-menu-toggler
    {
        min-width:20%;
        padding:0px 0px;
    }

    .user-menu-img
    {
        display:inline-flex;
    }

    .user-menu-designation .h4, .user-menu-designation .h5
    {
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: nowrap;
        margin-bottom:5px;
    }

    .user-menu-designation  
    {
        max-width: 89%;
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: nowrap;
        padding-left: 10px;
        padding-right: 10px;
        cursor:pointer;
        
    }
    .user-menu-designation .h5
    {
        margin-top:3px !important;
        margin-bottom:3px !important;
    }

    #sso-widget{
        float: right;
        z-index: 1111111 !important;
        height: 30px;
        width: 30px;
        margin: 3.5% 0px 3.5% 4%;
    }

    
    @media screen and (min-width: 950px) {
    
    .page-sidebar-wrapper
    {
        margin-top:-10px;
    }

        
    }
    
    @media screen and (max-width: 450px) {
    
        .page-sidebar-wrapper
        {
            margin-top:-70px;
        }

        

        ul.sub-menu-user
        {
            
            position:absolute;
            left:auto;
            right:0%;
        }

        .user-menu-designation  
        {
            max-width:140px !important;
        }

        .xs-menu-nav
        {
            margin-left:-30px;
        }
    }

    
    @media screen and (min-width:451px and max-width: 949px) {
       
        
    }

      
    @media screen and (min-width:601px) {

        #mobile-sum-menu-notification-li
        {
            display:none;
        }
    }
      
    @media screen and ( max-width:600px) {

        ul.sub-menu-user
        {
            width: 200px !important;
        }

        .user-menu-designation  
        {
            max-width:165px !important;
        }

        #mobile-sum-menu-notification-li
        {
            display:block;
        }

    }

    @media screen and ( max-width:990px) and (min-width:670px){

         

    }


</style>

<!-- BEGIN HEADER  -->
<?php
$selected_module = $session->read('module_id');
$has_monitor = $session->read('has_monitor');
$canSeeOfficeDB = $session->read('canSeeOfficeDB');
$can_see_report_module = $session->read('can_see_report_module');

$switch_lang_code = "";

$lang_code = $session->read('lang_code');
if ($lang_code == "" || $lang_code == "bn") {
    $lang_code = "bn";
    $switch_lang_code = "en";
} else {
    $lang_code = "en";
    $switch_lang_code = "bn";
}
?>
<div class="page-header md-shadow-z-1-i navbar navbar-fixed-top">
    <!-- BEGIN HEADER INNER -->
    <div class="page-header-inner">
        <!-- BEGIN LOGO -->
        <div class="row" style='display:flex'>
            <div class="page-logo col-md-3 col-sm-3 col-xs-2">
                <a href="<?= $this->request->webroot; ?>">
                    <img src="<?= CDN_PATH; ?>img/logo-body.png" alt="logo" class="img-responsive hidden-xs logo-default"
                        style="max-width: 170px; vertical-align: middle;"/>
                </a>
                <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse"
                           data-target=".navbar-collapse" style='margin:25px;'>
                </a>
                <div class="menu-toggler sidebar-toggler" style='margin-top:-30px'>
                        <!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
                </div>
            </div><!-- end page-logo -->

            
            <div class='col-md-6 col-sm-6 menu-module desktop hidden-xs hidden-sm text-center' style='display:flex'>
                    <div class=" menu-item-module text-center  hidden-sm hidden-xs">
                            <div class="clearfix">
                                <div class="btn-toolbar " style='display:inline-flex'>
                                    <div class="btn-group btn-group-md btn-group-circle center-block"
                                        style="font-size: 11pt!important; display:inline-flex">
                                        <?php
                                        $ik = 0;
                                       
                                        foreach ($modules as $moduleId => $moduleName) {
                                            // setting Module
                                            $ik += 1;
                                            if($ik == 4 )
                                            {
                                                echo "<div class='btn  hidden hidden-caret' style='height:38.44px;background:#8bbb48'></div>";
                                            }   
                                               
                                            if ($selected_module == $moduleId) { 
                                                        $current_module = $moduleName;
                                                    }
                                            if ($selected_module == 4) { 
                                                        $current_module_letter_counter = $totalNothi;
                                                    }
                                            if ($selected_module == 5) { 
                                                        $current_module_letter_counter = $totalDak;
                                                    }

                                            if ($moduleId == 1) {
                                                if (isset($loggedUser) && $loggedUser['user_role_id'] <= 2) {
                                                    ?>
                                                    <?php if ($selected_module
                                                        == $moduleId
                                                    ) { 

                                                        ?>

                                                        <button onclick="TOB_BAR_MANAGER.loadModuleWiseMenu('<?= $moduleId ?>')"
                                                                class="btn red "  ><span
                                                                    class="md-click-circle md-click-animate"
                                                                    style="height: 82px; width: 82px; top: -19px; left: -1.46875px;"></span><?= $moduleName ?> <?php if (($moduleId
                                                                    == 4 && $totalNothi > 0) || ($moduleId == 5 && $totalDak
                                                                    > 0)
                                                            ): ?><label class="badge badge-danger top_bar_mamager_active"><?php echo($moduleId
                                                            == 4 ? $this->Number->format($totalNothi) : $this->Number->format($totalDak)); ?></label><?php endif; ?>
                                                        </button>
                                                    <?php } else { ?>
                                                        <button href="javascript:"
                                                                onclick="TOB_BAR_MANAGER.loadModuleWiseMenu('<?= $moduleId ?>')"
                                                                class=" btn green "  ><span
                                                                    class="md-click-circle md-click-animate"
                                                                    style="height: 82px; width: 82px; top: -19px; left: -1.46875px;"></span><?= $moduleName ?> <?php if (($moduleId
                                                                    == 4 && $totalNothi > 0) || ($moduleId == 5 && $totalDak
                                                                    > 0)
                                                            ): ?><label class="badge badge-danger"><?php echo($moduleId
                                                            == 4 ? $this->Number->format($totalNothi) : $this->Number->format($totalDak)); ?></label><?php endif; ?>
                                                        </button>
                                                    <?php } ?>
                                                    <?php
                                                }
                                                else {
                                                    continue;
                                                }
                                            } 
                                            //Doptor
                                            elseif ($moduleId == 2) {
                                                //dd($loggedUser);
                                                if ((isset($loggedUser) && $loggedUser['user_role_id'] <= 1) || ($employee['is_admin'])) {
                                                    ?>
                                                    <?php if ($selected_module
                                                        == $moduleId
                                                    ) { ?>

                                                        <button onclick="TOB_BAR_MANAGER.loadModuleWiseMenu('<?= $moduleId ?>')"
                                                                class="btn red "  ><span
                                                                    class="md-click-circle md-click-animate"
                                                                    style="height: 82px; width: 82px; top: -19px; left: -1.46875px;"></span><?= $moduleName ?> <?php if (($moduleId
                                                                    == 4 && $totalNothi > 0) || ($moduleId == 5 && $totalDak
                                                                    > 0)
                                                            ): ?><label class="badge badge-danger"><?php echo($moduleId
                                                            == 4 ? $this->Number->format($totalNothi) : $this->Number->format($totalDak)); ?>  </label><?php endif; ?>
                                                        </button>
                                                    <?php } else { ?>
                                                        <button href="javascript:"
                                                                onclick="TOB_BAR_MANAGER.loadModuleWiseMenu('<?= $moduleId ?>')"
                                                                class=" btn green "  ><span
                                                                    class="md-click-circle md-click-animate"
                                                                    style="height: 82px; width: 82px; top: -19px; left: -1.46875px;"></span><?= $moduleName ?> <?php if (($moduleId
                                                                    == 4 && $totalNothi > 0) || ($moduleId == 5 && $totalDak > 0)
                                                            ): ?><label class="badge badge-danger" id="lbl-nothi"><?php echo($moduleId
                                                            == 4 ? $this->Number->format($totalNothi) : $this->Number->format($totalDak)); ?>   </label><?php endif; ?>
                                                        </button>
                                                    <?php } ?>
                                                    <?php
                                                } else {
                                                    continue;
                                                }
                                            }
                                            // Other Module
                                            else {

                                                // Report Module
                                                if($moduleId == 7 && $can_see_report_module == 0){
                                                    continue;
                                                }
                                                if($moduleId == 6 && $loggedUser['user_role_id'] != 3){
                                                    continue;
                                                }
                                                if (isset($loggedUser) && $loggedUser['user_role_id'] != 2) {
                                                    if ($selected_module
                                                        == $moduleId
                                                    ) { ?>
                                                        <button href="javascript:"
                                                                onclick="TOB_BAR_MANAGER.loadModuleWiseMenu('<?= $moduleId ?>')"
                                                                class="btn red "
                                                                 ><span
                                                                    class="md-click-circle md-click-animate"
                                                                    style="height: 82px; width: 82px; top: -19px; left: -1.46875px;"></span><?= $moduleName ?> 
                                                                        <?php if (($moduleId == 4 && $totalNothi > 0) || ($moduleId == 5 && $totalDak > 0) ): ?>
                                                                            <label class="badge badge-info"  id="<?php echo($moduleId == 4 ? 'lbl_nothi' : 'lbl_dak');?>"> 
                                                                                <?php if($moduleId == 4){
                                                                                     
                                                                                    echo $this->Number->format($totalNothi); 
                                                                                }else{
                                                                                     
                                                                                    echo $this->Number->format($totalDak); 
                                                                                }     
                                                                                ?>
                                                                            </label>
                                                                        <?php endif; ?>
                                                              <!-- aaa -->      
                                                        </button>
                                                    <?php } else { ?>
                                                        <button href="javascript:"
                                                                onclick="TOB_BAR_MANAGER.loadModuleWiseMenu('<?= $moduleId ?>')"
                                                                class=" btn green " ><span
                                                                    class="md-click-circle md-click-animate"
                                                                    style="height: 82px; width: 82px; top: -19px; left: -1.46875px;"></span>
                                                                    <?= $moduleName ?> 
                                                                    <?php if (($moduleId
                                                                    == 4 && $totalNothi > 0) || ($moduleId == 5 && $totalDak
                                                                    > 0)): ?>
                                                                        <label class="badge badge-info" id="<?php echo($moduleId == 4 ? 'lbl_nothi':'lbl_dak');?>">
                                                                        <?php if($moduleId == 4){
                                                                            
                                                                            echo $this->Number->format($totalNothi);
                                                                        }else{
                                                                            
                                                                            echo $this->Number->format($totalDak);
                                                                        }  ?> 
                                                                        </label>
                                                                    <?php endif; ?>
                                                        <!-- bbb -->
                                                        </button>
                                                    <?php } ?>

                                                    <?php
                                                }
                                            }
                                        }
                                        if (!empty($has_monitor) && $has_monitor > 0) {
                                            ?>
                                            <a target="_blank" href="<?= $this->request->webroot ?>reporting"
                                            class="btn btn green monitorButton " style=" "><span
                                                        class="md-click-circle md-click-animate"
                                                        style="height: 82px; width: 82px; top: -19px; left: -1.46875px;"></span>মনিটরিং</a>

                                            <?php
                                        }
                                        if (!empty($canSeeOfficeDB) && $canSeeOfficeDB > 0) {
                                            ?>
                                            <a target="_blank" href="<?= \Cake\Routing\Router::url(['_name' => 'OwnOfficeDB']); ?>"
                                            class="btn btn green monitorButton " style=" "><span
                                                        class="md-click-circle md-click-animate"
                                                        style="height: 82px; width: 82px; top: -19px; left: -1.46875px;"></span>
                                                অফিস ড্যাশবোর্ড
                                             </a>

                                            <?php
                                        }
                                        ?>
                                        
                                    </div>
                                </div>
                            </div>
                        </div><!-- end module menu for md -->
                        <?php
                        $modules = jsonA(MODULES);
                        $selected_module_name = $modules[$selected_module];
                        if ($selected_module == 5) {
                            if ($totalDak > 0) {
                                $selected_module_name = $selected_module_name . " (" . enTobn($totalDak) . ") | অফিস ব্যবস্থাপনা";
                            } else {
                                $selected_module_name = $selected_module_name . " | অফিস ব্যবস্থাপনা";
                            }
                        } elseif ($selected_module == 4) {
                            if ($totalNothi > 0) {
                                $selected_module_name = $selected_module_name . " (" . enTobn($totalNothi) . ") | অফিস ব্যবস্থাপনা";
                            } else {
                                $selected_module_name = $selected_module_name . " | অফিস ব্যবস্থাপনা";
                            }
                        } else {
                            $selected_module_name = $selected_module_name . " | অফিস ব্যবস্থাপনা";
                        }
                        echo "<input type='hidden' id='page_title' value='$selected_module_name' />";
                        ?>
                        <?php
                         if(defined('SSO_LOGIN') && SSO_LOGIN) {
                        ?>
                            <div class="dropdown dropdown-language  " id="sso-widget" >
                            </div>
                        <?php
                         }
                        ?>
            </div><!-- end col-md-6 end module menu desktop for md margin-top-20 menu-item-module text-center  hidden-sm hidden-xs -->


            <div class='visible-xs visible-sm' id='xs-menu-toggler' ><!-- menu xs menu -->
                

                        <!-- <h3>module menu on small screen </h3> -->
                        <nav class="navbar navbar-default xs-menu-nav" style=''>
                        <a href="javascript:;" class=" btn red btn-circle navbar-toggle visible-xs visible-sm margin-top-10 xs-menu-lbl " data-toggle="collapse" data-target="#moduleNavbar" data-trigger="focus">
                                <?php echo $current_module;?>
                                <?php if($current_module_letter_counter>0){?>
                                    <label class="badge badge-danger top_bar_mamager_active">
                                        <?php echo enTobn($current_module_letter_counter);?>
                                    </label>
                                <?php } ?>
                                <i class='fa fa-chevron-down'></i>
                        </a>  
                  
                                   
                                    <ul class="collapse page-sidebar-menu xs-module-menu" id="moduleNavbar" >
                                              
                                            <?php
                                                foreach ($modules as $moduleId => $moduleName) {
                                                    // setting Module
                                                    if ($moduleId == 1) {
                                                        if (isset($loggedUser) && $loggedUser['user_role_id'] <= 2) {
                                                            ?>
                                                            <?php if ($selected_module
                                                                == $moduleId
                                                            ) { ?>
                                                                <li>
                                                                    <button onclick="TOB_BAR_MANAGER.loadModuleWiseMenu('<?= $moduleId ?>')"
                                                                            class="btn red" data-toggle="dropdown" data-hover="dropdown"
                                                                            data-close-others="true"><span
                                                                                class="md-click-circle md-click-animate"
                                                                                style="height: 82px; width: 82px; top: -19px; left: -1.46875px;"></span><?= $moduleName ?> <?php if (($moduleId
                                                                                == 4 && $totalNothi > 0) || ($moduleId == 5 && $totalDak
                                                                                > 0)
                                                                        ): ?><label class="badge badge-danger top_bar_mamager_active"><?php echo($moduleId
                                                                        == 4 ? $this->Number->format($totalNothi) : $this->Number->format($totalDak)); ?></label><?php endif; ?>
                                                                    </button>
                                                                    </li>
                                                            <?php } else { ?>
                                                                <li>
                                                                    <button href="javascript:"
                                                                            onclick="TOB_BAR_MANAGER.loadModuleWiseMenu('<?= $moduleId ?>')"
                                                                            class=" btn green" data-toggle="dropdown"
                                                                            data-hover="dropdown"
                                                                            data-close-others="true"><span
                                                                                class="md-click-circle md-click-animate"
                                                                                style="height: 82px; width: 82px; top: -19px; left: -1.46875px;"></span><?= $moduleName ?> <?php if (($moduleId
                                                                                == 4 && $totalNothi > 0) || ($moduleId == 5 && $totalDak
                                                                                > 0)
                                                                        ): ?><label class="badge badge-danger"><?php echo($moduleId
                                                                        == 4 ? $this->Number->format($totalNothi) : $this->Number->format($totalDak)); ?></label><?php endif; ?>
                                                                    </button>
                                                                </li>
                                                            <?php } ?>
                                                            <?php
                                                        }
                                                        else {
                                                            continue;
                                                        }
                                                    } 
                                                    //Doptor
                                                    elseif ($moduleId == 2) {
                                                        //dd($loggedUser);
                                                        if ((isset($loggedUser) && $loggedUser['user_role_id'] <= 1) || ($employee['is_admin'])) {
                                                            ?>
                                                            <?php if ($selected_module
                                                                == $moduleId
                                                            ) { ?>
                                                                <li>
                                                                    <button onclick="TOB_BAR_MANAGER.loadModuleWiseMenu('<?= $moduleId ?>')"
                                                                            class="btn red" data-toggle="dropdown" data-hover="dropdown"
                                                                            data-close-others="true"><span
                                                                                class="md-click-circle md-click-animate"
                                                                                style="height: 82px; width: 82px; top: -19px; left: -1.46875px;"></span><?= $moduleName ?> <?php if (($moduleId
                                                                                == 4 && $totalNothi > 0) || ($moduleId == 5 && $totalDak
                                                                                > 0)
                                                                        ): ?><label class="badge badge-danger"><?php echo($moduleId
                                                                        == 4 ? $this->Number->format($totalNothi) : $this->Number->format($totalDak)); ?>  </label><?php endif; ?>
                                                                    </button>
                                                                </li>
                                                            <?php } else { ?>
                                                                <li>
                                                                    <button href="javascript:"
                                                                            onclick="TOB_BAR_MANAGER.loadModuleWiseMenu('<?= $moduleId ?>')"
                                                                            class=" btn green" data-toggle="dropdown"
                                                                            data-hover="dropdown"
                                                                            data-close-others="true"><span
                                                                                class="md-click-circle md-click-animate"
                                                                                style="height: 82px; width: 82px; top: -19px; left: -1.46875px;"></span><?= $moduleName ?> <?php if (($moduleId
                                                                                == 4 && $totalNothi > 0) || ($moduleId == 5 && $totalDak > 0)
                                                                        ): ?><label class="badge badge-danger" id="lbl-nothi"><?php echo($moduleId
                                                                        == 4 ? $this->Number->format($totalNothi) : $this->Number->format($totalDak)); ?>   </label><?php endif; ?>
                                                                    </button>
                                                                </li>
                                                            <?php } ?>
                                                            <?php
                                                        } else {
                                                            continue;
                                                        }
                                                    }
                                                    // Other Module
                                                    else {
                                                        // Report Module
                                                        if($moduleId == 7 && $can_see_report_module == 0){
                                                            continue;
                                                        }
                                                        if($moduleId == 6 && $loggedUser['user_role_id'] != 3){
                                                            continue;
                                                        }
                                                        if (isset($loggedUser) && $loggedUser['user_role_id'] != 2) {
                                                            if ($selected_module
                                                                == $moduleId
                                                            ) { ?>
                                                            <li>
                                                                <button href="javascript:"
                                                                        onclick="TOB_BAR_MANAGER.loadModuleWiseMenu('<?= $moduleId ?>')"
                                                                        class="btn red" data-toggle="dropdown" data-hover="dropdown"
                                                                        data-close-others="true"><span
                                                                            class="md-click-circle md-click-animate"
                                                                            style="height: 82px; width: 82px; top: -19px; left: -1.46875px;"></span><?= $moduleName ?> 
                                                                            <label class="badge badge-info"  id="<?php// echo($moduleId == 4 ? 'lbl_nothi' : 'lbl_dak');?>"> 
                                                                                <?php if (($moduleId == 4 && $totalNothi > 0) || ($moduleId == 5 && $totalDak > 0) ): ?>
                                                                                        <?php echo($moduleId == 4 ? 
                                                                                            $this->Number->format($totalNothi) : 
                                                                                            $this->Number->format($totalDak)); 
                                                                                        ?>
                                                                                <?php else: ?>
                                                                                        0
                                                                                <?php endif; ?>
                                                                            </label>
                                                                </button>
                                                            </li>
                                                            <?php } else { ?>
                                                                <li>
                                                                    <button href="javascript:"
                                                                            onclick="TOB_BAR_MANAGER.loadModuleWiseMenu('<?= $moduleId ?>')"
                                                                            class=" btn green" data-toggle="dropdown"
                                                                            data-hover="dropdown"
                                                                            data-close-others="true"><span
                                                                                class="md-click-circle md-click-animate"
                                                                                style="height: 82px; width: 82px; top: -19px; left: -1.46875px;"></span><?= $moduleName ?> <?php if (($moduleId
                                                                                == 4 && $totalNothi > 0) || ($moduleId == 5 && $totalDak
                                                                                > 0)
                                                                        ): ?><label class="badge badge-info" id="<?php //echo($moduleId == 4 ? 'lbl_nothi':'lbl_dak');?>"><?php echo($moduleId
                                                                        == 4 ? $this->Number->format($totalNothi) : $this->Number->format($totalDak)); ?> </label><?php endif; ?>
                                                                    </button>
                                                                </li>
                                                            <?php } ?>


                                                            <?php
                                                        }
                                                    }
                                                }
                                                if (!empty($has_monitor) && $has_monitor > 0) {
                                                    ?>
                                                    <li>
                                                        <a target="_blank" href="<?= $this->request->webroot ?>reporting"
                                                        class="btn btn green monitorButton" style=" "><span
                                                                    class="md-click-circle md-click-animate"
                                                                    style="height: 82px; width: 82px; top: -19px; left: -1.46875px;"></span>মনিটরিং</a>
                                                    </li>
                                                    <?php
                                                }
                                                if (!empty($canSeeOfficeDB) && $canSeeOfficeDB > 0) {
                                                    ?>
                                                    <li>
                                                        <a target="_blank" href="<?= \Cake\Routing\Router::url(['_name' => 'OwnOfficeDB']); ?>"
                                                        class="btn btn green monitorButton" style=" "><span
                                                                    class="md-click-circle md-click-animate"
                                                                    style="height: 82px; width: 82px; top: -19px; left: -1.46875px;"></span>
                                                            অফিস ড্যাশবোর্ড
                                                        </a>
                                                    </li>

                                                    <?php
                                                }
                                                ?>

                                              
                                    </ul> 
                              
                            </nav>

            </div><!-- end col-xs-2 end module xs menu -->


            <div class=' menu-user'><!-- menu designation, name etc -->
                    
                    <!-- //name designatio menu -->
                        <?php if (isset($loggedUser)):
                                ?>
                                    
                                    
                                        <div class="user-menu-img margin-top-20">
                                            <?php
                                            $exist = FILE_FOLDER_DIR . 'Personal/profile/' .h($loggedUser['username']) . '.png';
                                            if (file_exists($exist)) {
                                                $file = $this->request->webroot . 'content/' . 'Personal/profile/' . h($loggedUser['username']) . '.png?token='.sGenerateToken(['file'=>'Personal/profile/' . h($loggedUser['username']) . '.png'],['exp'=>time() + 60*300]);
                                            } else {
                                                $file = $this->request->webroot . 'assets/admin/pages/media/profile/avatar.png';
                                            }
                                            ?>
                                            <?php
                                            if (!empty($employee['office_name'])) {
                                                ?>
                                                <img alt="" class="img-circle user" height="40px" width="40px" src="<?php echo $file ?>"
                                                    />
                                                <?php
                                            } else {
                                                ?>
                                                <img alt="" class="img-circle " class="img-circle " height="40px" width="40px"
                                                    src="<?php echo $file ?>"/>
                                                <?php
                                            }
                                            ?>
                                        </div>

                                        <!-- <a class="dropdown-toggle menu-link" data-toggle="dropdown" data-click="dropdown"
                                    data-close-others="true"  > -->

                                        <div class="user-menu-designation dropdown-toggle menu-link" data-toggle="dropdown" data-click="dropdown"
                                    data-close-others="true"  >
                                        <!-- name, designation, office branch-->
                                             
                                            
                                                    <?php
                                                    $f = 0;
                                                    if (!empty($employee['office_id'])) {
                                                        $f = 1;
                                                        ?><div class='h4'><b><?php
                                                            echo h($employee['officer_name']);
                                                        ?></b></div><?php   
                                                    }
                                                    ?><div class='h5'><?php
                                                        if (isset($employee['designation_label'])&&!empty($employee['office_id'])) {
                                                            $f = 1;
                                                            echo h($employee['designation_label']);
                                                            
                                                            if(isset($employee['incharge_label'])&&!empty($employee['incharge_label'])){
                                                                echo ' ('.h($employee['incharge_label']), ')';
                                                            }
                                                        }
                                                        if (!empty($employee['office_id'])) {
                                                            if(isset($employee['show_unit']) && $employee['show_unit'] == 1){
                                                                if ($f == 1)
                                                                echo ", ";
                                                                echo h($employee['office_unit_name']);
                                                            }
                                                            $f = 1;
                                                        }
                                                    ?></div><?php
                                                    ?> <div class='h5'><?php
                                                    if (!empty($employee)) {
                                                    
                                                        $f = 1;
                                                        echo h($employee['office_name']);
                                                    }
                                                
                                                        echo($f == 0 ? h($loggedUser['username'])  : '');
                                                    ?> </div><?php
                                                    ?>
                                       
                                            

                                        </div><!-- end username username-hide-on-mobile -->
                                        <i class="glyphicon glyphicon-chevron-down user-menu-caret pull-right margin-top-40 dropdown-toggle menu-link" data-toggle="dropdown" data-click="dropdown"
                                    data-close-others="true"  style="padding:0px;margin-right:10px">
                                        </i>
                                        
                                        <div class='clearfix'></div>
                                        <ul class="dropdown-menu dropdown-menu-default sub-menu-user">
                                                <?php //if ((isset($loggedUser) && $loggedUser['user_role_id']<= 1) || ($loggedUser['is_admin'])) { ?>
                                                <li>
                                                    <a href="<?php echo $this->Url->build(['controller' => 'Dashboard',
                                                        'action' => 'downloadApp']) ?>">
                                                        <i class="fa fa-mobile-phone" aria-hidden="true"></i> নথি মোবাইল অ্যাপ </a>
                                                </li>
                                                <!-- <li class="divider"> 
                                                </li>-->
                                                <?php //} ?>
                                                <li>
                                                    <a href="<?php echo $this->Url->build(['controller' => 'EmployeeRecords',
                                                        'action' => 'myProfile']) ?>">
                                                        <i class="fs1 a2i_gn_user1"></i> <?= __('My Profile') ?> </a>
                                                </li>

                                                <!-- <li class="divider"> 
                                                </li>-->

                                                <li id='mobile-sum-menu-notification-li' >
                                                    <a href="#" class="quick-sidebar-toggler">
                                                        <!-- <i class="icon-envelope-letter"></i> -->
                                                        <?php if($loggedUser['user_role_id'] > 2): ?>
                                                            <div title="বার্তা">
                                                                <i class="icon-envelope-letter"></i>
                                                                <span class="badge badge-danger" id="global-message-count"><?=enTobn(count($global_messages['unread']))?></span>
                                                                নটিফিকেশন
                                                            </div>
                                                        
                                                        <?php endif; ?>
                                                    </a>
                                                </li>

                                                <li>
                                                    <a class="logoutButton" href="<?php echo $this->request->webroot; ?>logout">
                                                        <i class="a2i_gn_logout2"></i> <?= __('Log Out') ?> </a>
                                                </li>

                                        </ul>

                                        <!-- </a> -->

                        
                            <?php endif; ?>
                    <!-- //end name designatio menu -->


                    <div class='right-collapse-bar  hidden-xs pull-right text-center margin-top-30'>
                         
                        <?php if($loggedUser['user_role_id'] > 2): ?>
                            <div class="dropdown dropdown-extended quick-sidebar-toggler menu-item-barta hidden-xs">
                                <div  id='right-bar-barta' class='right-bar-barta quick-sidebar-toggler' >
                                    <i class="icon-logout quick-sidebar-toggler-i" ></i>
                                    <span class="badge badge-danger" id="global-message-count">
                                        <?=enTobn(count($global_messages['unread']))?>
                                    </span>
                                </div>
                            </div>
                        <?php endif; ?>
                         
                    </div><!-- end col-md-1 col-sm-1 right-collapse-bar -->
            </div><!-- end name- designation menu menu-user col-md-4 col-sm-4 col-xs-8 -->

        </div><!-- end top bar row -->
    </div>
    <!-- END HEADER INNER -->
</div>
<!-- END HEADER -->
<script src="<?php echo CDN_PATH ?>assets/global/plugins/bootbox/bootbox.min.js" type="text/javascript"></script>
<script type="text/javascript">
	$(function() {
        $("body").click(function (e) {
            var clicked_element = $(e.target);
            if (clicked_element.closest('.page-header .quick-sidebar-toggler,.quick-sidebar-toggler-i, .page-quick-sidebar-toggler').length > 0) {
                $('body').toggleClass('page-quick-sidebar-open');
                $(".page-quick-sidebar-wrapper").find('ul>li').removeClass('active');
                $(".page-quick-sidebar-wrapper").find('ul>li').first().addClass('active');
            } else if ($("body").hasClass('page-quick-sidebar-open')) {
                if (clicked_element.closest('.page-quick-sidebar-wrapper').length == 0) {
                    $('body').toggleClass('page-quick-sidebar-open');
                }
            }

            if ($("body").hasClass('page-quick-sidebar-open')) {
                $('.dropdown.dropdown-extended.quick-sidebar-toggler').find('i').removeClass('icon-logout').addClass('icon-login');
            } else {
                $('.dropdown.dropdown-extended.quick-sidebar-toggler').find('i').removeClass('icon-login').addClass('icon-logout');
            }
        });

        $('.right-bar-barta').on('mouseover',function(e){
            var msg_count = $('#global-message-count').html();
            $(this).html("<i class='icon-login'> </i> <span class='badge badge-danger' id='global-message-count'>"+msg_count+"</span>");
        });
        $('.right-bar-barta').on('mouseout',function(e){
            var msg_count = $('#global-message-count').html();
            $(this).html("<i class='icon-logout'> </i> <span class='badge badge-danger' id='global-message-count'>"+msg_count+"</span>");
        });
	});
	function viewGlobalMessage(element, messageId) {
	    var title = $(element).find('.title').text();
	    var message = $(element).find('.message').html();
        doModal('viewGlobalMessage', title, message, '', '');

        $.ajax({
            type: 'POST',
            async: true,
            url: "<?php echo $this->Url->build(['_name' => 'view-message']) ?>",
            data: {"message_id": messageId},
            success: function (response) {
                if (response.status == 'new') {
                    $( element ).clone().appendTo( "#quick_sidebar_tab_2>ul" );
                    $( element ).remove();
                    $("#global-message-count").text(enTobn(bnToen($("#global-message-count").text()) - 1));
                }
            }
        });
	}
    window.onload = function(e) {
        document.getElementsByTagName("title")[0].innerHTML = document.getElementById("page_title").value;
    }
    var TOB_BAR_MANAGER =
        {
            loadModuleWiseMenu: function (module_id) {
                Metronic.blockUI({
                    target: '.page-container',
                    boxed: true,
                    message: 'অপেক্ষা করুন'
                });
                PROJAPOTI.ajaxSubmitDataCallback('<?php echo $this->request->webroot; ?>ModuleMenus/loadModuleWiseMenu', {"module_id": module_id}, 'json', function (response) {
                    window.location = '<?php echo $this->request->webroot ?>';
                    Metronic.unblockUI('.page-container');
                });
                
            },
            switchLanguage: function (lang_code) {
                PROJAPOTI.ajaxSubmitDataCallback('<?php echo $this->request->webroot; ?>ModuleMenus/switchLanguage', {"lang_code": lang_code}, 'json', function (response) {

                    window.location = '<?php echo $this->request->webroot ?>';
                });
            }
        };


    $('.logoutButton').click(function (ev) {
        return true;
        ev.preventDefault();
        var href = $(this).attr('href');
        Metronic.blockUI({
            target: '.page-container',
            boxed: true,
            message: 'অপেক্ষা করুন'
        });

        PROJAPOTI.ajaxSubmitDataCallbackError('<?php echo $this->Url->build(['controller' => 'dashboard',
            'action' => 'checkUndoneTask']) ?>', {}, 'json', function (msg) {

            if (msg.status == 'success') {
                bootbox.dialog({
                    message: "আপনি কি লগ আউট করতে ইচ্ছুক?",
                    title: "লগ আউট",
                    buttons: {
                        success: {
                            label: "হ্যাঁ",
                            className: "green",
                            callback: function () {
                                window.location.href = href;
                            }
                        },
                        danger: {
                            label: "না",
                            className: "red",
                            callback: function () {
                                Metronic.unblockUI('.page-container');
                            }
                        }
                    }
                });

            } else {
                bootbox.dialog({
                    message: (msg.data['draft'] != 0 ? ("আপনার " + msg.data['draft'] + "টি খসড়া পত্র  ") : "") + (msg.data['pendingdak'] != 0 ? ((msg.data['draft'] != 0 ? ' এবং ' : '') + msg.data['pendingdak'] + "টি ডাক ৩ দিনের অধিক ") : "") + "পেন্ডিং আছে। আপনি কি লগ আউট করতে ইচ্ছুক?",
                    title: "লগ আউট",
                    buttons: {
                        success: {
                            label: "হ্যাঁ",
                            className: "green",
                            callback: function () {
                                window.location.href = href;
                            }
                        },
                        danger: {
                            label: "না",
                            className: "red",
                            callback: function () {
                                Metronic.unblockUI('.page-container');
                            }
                        }
                    }
                });
            }
        }, function (err) {
            window.location.href = href;
            Metronic.unblockUI('.page-container');
        });

    });
    <?php
    if(defined('SSO_LOGIN') && SSO_LOGIN)
    {
    ?>
        $(document).ready(function () {
            setTimeout(function(){
                widget.init(
                    {
                        "widgetColor" : "light",
                        "widgetSize" : "20px",
                        "appPermissionURL": "<?php echo $this->Url->build(['controller' => 'SSO', 'action' => 'apps']); ?>"
                    });
            },2000);
        });
    <?php
    }
    ?>

    function resize_module() {
        // parent width
        var win_width = window.innerWidth;
        var btn_caret = $(".menu-item-module").find('.hidden-caret');
        
        if(win_width<1200)
        {
            btn_caret.removeClass('hidden');
            var dropdown_menu = $(".menu-item-module").find('.dropdown-menu');
            if(dropdown_menu.length>0)
            {
                //console.log(dropdown_menu);
            }else
            {
                btn_caret.append('<button type="button" class="btn blue dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="background:transparent;height:25px;"><span class="glyphicon glyphicon-chevron-down"></span></button><div class="dropdown-menu" style="margin-left:-75%"></div>');
                dropdown_menu = $(".menu-item-module").find('.dropdown-menu');
            }
            var parent_width = $(".menu-item-module").find('.btn-group:first-child').parent().width();
        
            $('.menu-item-module').find('.btn-group>.btn').each(function(k,v) {
                if(k<4)
                {
                
                }else{
                    $(v).addClass('btn-block dropdown-item');
                    dropdown_menu.append($(v));
                }

            });
        }else
        {
            btn_caret.addClass('hidden');
            var btnGroup = $('.menu-item-module').find('.btn-group');
            $('.menu-item-module').find('.dropdown-menu>.btn').each(function(k,v) {
                    $(v).removeClass('btn-block');
                    $(v).removeClass('dropdown-item');
                    btnGroup.append($(v));
            });
        }
         
    }
    window.addEventListener('load',resize_module );
    window.addEventListener('resize',resize_module );
</script>
