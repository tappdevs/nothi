<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<?php
  if (!defined("CDN_PATH") && defined("CDN") && CDN == 1) {
            define("CDN_PATH", 'http://cdn1.nothi.gov.bd/webroot/');
        }
        else if (!defined("CDN_PATH")) {
            define("CDN_PATH", $this->request->webroot);
        }

?>
<html lang="en" class="no-js">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title>নথি | অফিস ব্যবস্থাপনা</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <meta content="" name="description"/>
        <meta content="" name="author"/>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->

        <link href="<?php echo CDN_PATH; ?>assets/global/plugins/font-awesome/css/font-awesome.min.css"
              rel="stylesheet" type="text/css"/>
        <link href="<?php echo CDN_PATH; ?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css"
              rel="stylesheet" type="text/css"/>
        <link href="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap/css/bootstrap.min.css"
              rel="stylesheet" type="text/css"/>
        <link href="<?php echo CDN_PATH; ?>assets/global/plugins/uniform/css/uniform.default.css"
              rel="stylesheet" type="text/css"/>
        <link
            href="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css"
            rel="stylesheet" type="text/css"/>
        <!-- END GLOBAL MANDATORY STYLES -->
       
        <!-- BEGIN PAGE STYLES -->
        
        <link rel="stylesheet" type="text/css"
              href="<?php echo CDN_PATH; ?>assets/global/plugins/jstree/dist/themes/default/style.min.css"/>

        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN THEME STYLES -->
        <link href="<?php echo CDN_PATH; ?>webroot/assets/global/css/components.css"
              id="style_components" rel="stylesheet" type="text/css"/>
        <link href="<?php echo CDN_PATH; ?>webroot/assets/global/css/plugins.css" rel="stylesheet"
              type="text/css"/>
        <link href="<?php echo CDN_PATH; ?>webroot/assets/admin/layout4/css/layout.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo CDN_PATH; ?>webroot/assets/admin/layout4/css/custom.css" rel="stylesheet" type="text/css"/>
        <!-- END THEME STYLES -->

        <link href="<?php echo CDN_PATH; ?>assets/admin/pages/css/timeline.css" rel="stylesheet"
              type="text/css"/>
        <link rel="stylesheet" href="<?php echo CDN_PATH; ?>daptorik_preview/css/custom.css">
        
        <link rel="shortcut icon" href="favicon.ico"/>
    </head>
    <body class="page-header-fixed page-sidebar-closed-hide-logo page-sidebar-closed-hide-logo online-dak">
        <div class="clearfix"></div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container" style="margin-top: 0px;">
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content-online-dak">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="ajax-content">
                                <?php echo $this->fetch('content'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
<!--        <div class="page-footer">-->
<!--            <div class="page-footer-inner">-->
<!--                কপিরাইট --><?php //echo enTobn(date("Y")) ?><!--, এক্সেস টু ইনফরমেশন-->
<!--            </div>-->
<!--            <div class="scroll-to-top">-->
<!--                <i class="icon-arrow-up"></i>-->
<!--            </div>-->
<!--        </div>-->
        <div style="background-color: #e5e5e5; min-height: 30px;">
        <span style="float: left; font-size: 10px; margin-top: 5px;">

            <a href="http://www.bangladesh.gov.bd/" style="padding: 5px; vertical-align: middle;"><img class="img-responsive" style="border-radius: 50%" src="<?=$this->request->webroot?>img/bd.png" alt="BD_logo"></a>
                <a href="http://a2i.gov.bd/" style="padding-right: 3px; vertical-align: middle;"><img class="img-responsive" style="border-radius: 50%" src="<?=$this->request->webroot?>img/a2i_logo_new.png" alt="a2i_logo"></a>
                <a style="text-decoration: none;
            font-size: 10px;
            display: inline-table;
            vertical-align: middle;
            margin-bottom: 12px">তত্ত্বাবধানে: এক্সেস টু ইনফরমেশন</a>
            </span>

            <span style="float: right; font-size: 10px; margin-top: 5px;">
            <a style="text-decoration: none;
            font-size: 10px;
            display: inline-table;
            vertical-align: middle;
            margin-bottom: 12px">Powered By: Tappware Emailer App</a>
            <a href="http://www.tappware.com/" style="padding: 5px; vertical-align: middle;"><img style="border-radius: 50%" src="<?=$this->request->webroot?>img/tappware_logo.png" alt="tappware_logo"></a>
            </span>
        </div>
        <!-- END FOOTER -->
       
    </body>
    <!-- END BODY -->
</html>