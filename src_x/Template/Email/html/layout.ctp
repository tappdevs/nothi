<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<?php
  if (!defined("CDN_PATH") && defined("CDN") && CDN == 1) {
            define("CDN_PATH", 'http://cdn1.nothi.gov.bd/webroot/');
        }
        else if (!defined("CDN_PATH")) {
            define("CDN_PATH", $this->request->webroot);
        }

?>
<html lang="en" class="no-js">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title>নথি | অফিস ব্যবস্থাপনা</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <meta content="" name="description"/>
        <meta content="" name="author"/>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->

        <link href="<?php echo CDN_PATH; ?>assets/global/plugins/font-awesome/css/font-awesome.min.css"
              rel="stylesheet" type="text/css"/>
        <link href="<?php echo CDN_PATH; ?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css"
              rel="stylesheet" type="text/css"/>
        <link href="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap/css/bootstrap.min.css"
              rel="stylesheet" type="text/css"/>
        <link href="<?php echo CDN_PATH; ?>assets/global/plugins/uniform/css/uniform.default.css"
              rel="stylesheet" type="text/css"/>
        <link
            href="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css"
            rel="stylesheet" type="text/css"/>
        <!-- END GLOBAL MANDATORY STYLES -->
       
        <!-- BEGIN PAGE STYLES -->
        
        <link rel="stylesheet" type="text/css"
              href="<?php echo CDN_PATH; ?>assets/global/plugins/jstree/dist/themes/default/style.min.css"/>

        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN THEME STYLES -->
        <link href="<?php echo CDN_PATH; ?>webroot/assets/global/css/components.css"
              id="style_components" rel="stylesheet" type="text/css"/>
        <link href="<?php echo CDN_PATH; ?>webroot/assets/global/css/plugins.css" rel="stylesheet"
              type="text/css"/>
        <link href="<?php echo CDN_PATH; ?>webroot/assets/admin/layout4/css/layout.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo CDN_PATH; ?>webroot/assets/admin/layout4/css/custom.css" rel="stylesheet" type="text/css"/>
        <!-- END THEME STYLES -->

        <link href="<?php echo CDN_PATH; ?>assets/admin/pages/css/timeline.css" rel="stylesheet"
              type="text/css"/>
        <link rel="stylesheet" href="<?php echo CDN_PATH; ?>daptorik_preview/css/custom.css">
        
        <link rel="shortcut icon" href="favicon.ico"/>
    </head>
    <body class="page-header-fixed page-sidebar-closed-hide-logo page-sidebar-closed-hide-logo online-dak">
	    <div class="clearfix"></div>

	    <div class="page-container" style="margin-top:0px">

		    <div class="page-content-wrapper">
			    <div class="page-content-online-dak">
				    <div class="row">
					    <div class="col-md-12">
						    <div id="ajax-content">

							    <div class="panel" background="http://nothi.gov.bd/img/email-top.jpg" style="background-color:#ffffff;height:40px;background-image:url(http://nothi.gov.bd/img/email-top.jpg);background-repeat:no-repeat;background-size:100% 100%">

								    <div style="float:right;padding:0px 25px 10px 0px!important">
									    <p style="font-weight:bold;color:white;padding:6px;"><?= isset($title) ? $title : 'নথি ইমেইল' ?></p>
								    </div>

							    </div>
							    <div class="news-blocks" style="padding:10px;margin-bottom:0px;background:#ffffff;border-top:solid 2px #faf6ea">
								    <h3 style="border-bottom:1px solid #683091"><?= isset($subject) ? $subject : 'ইমেইলের বিষয় পাওয়া যায় নি' ?></h3>


								    <div class="news-block-tags">
                                        <?= isset($content) ? $content : 'ইমেইলের বিস্তারিত পাওয়া যায় নি' ?>
								    </div>



								    <br>
								    <br>
								    <a class="news-block-btn" href="http://nothi.gov.bd" title="লগইন করুন" target="_blank"> নথি কার্যক্রম করতে লগইন করুন <i class="m-icon-swapright m-icon-black"></i></a>
								    <br>
								    <br>

								    <br>
								    <div class="row">
									    <p style="color:grey;font-size:10px">*** এই ইমেইলটি সিস্টেম কর্তৃক স্বয়ংকৃতভাবে তৈরী হয়েছে। এই ইমেইলের কোনো প্রতিউত্তর(reply) দেয়ার প্রয়োজন নেই।</p>
								    </div>
							    </div>
						    </div>
					    </div>
				    </div>
			    </div>
		    </div>

	    </div>
    </body>
</html>