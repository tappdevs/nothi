<?php
  if (!defined("CDN_PATH") && defined("CDN") && CDN == 1) {
            define("CDN_PATH", 'http://cdn1.nothi.gov.bd/webroot/');
        }
        else if (!defined("CDN_PATH")) {
            define("CDN_PATH", $this->request->webroot);
        }
$url = str_replace('content/', '', FILE_FOLDER);
?>
<link href="<?= CDN_PATH; ?>assets/admin/pages/css/news.css" rel="stylesheet" type="text/css">
<div class="panel" background="<?=$url?>img/email-top.jpg" style="background-color: #ffffff; height: 40px; background-image:url(<?=$url?>img/email-top.jpg); background-repeat: no-repeat; background-size: 100% 100%">

    <div style="float:right;padding: 0px 25px 10px 0px!important;"><p style="font-weight: bold; color: white"><?php if(isset($subject) && mb_strlen($subject)>50){
                echo mb_substr($subject, 0, 50)."...";}
            else if(isset($subject))
                echo __($subject);
            ?></p></div>

</div>
<div class="news-blocks" style="padding: 10px;
    margin-bottom: 0px;
    background: #FFFFFF;
    border-top: solid 2px #faf6ea;">
    <?php if(!empty($body_subject)): ?>
    <h3 style="border-bottom: 1px solid #683091;">
        <?= __($body_subject) ?>
    </h3>
    <?php endif; ?>
    <div class="news-block-tags">
        <?php if(!empty($sender)): ?> <strong>প্রেরক: </strong><?= $sender .','?> <?php endif; ?><br/>
        <em ><b> সময়:  </b><?= $notificationTime ?></em>
    </div>
    <br/>
    <?= $notification ?>
    <br/><br/>
   <a class="news-block-btn" href="<?=$url?>" title="লগইন করুন" > নথি কার্যক্রম করতে লগইন করুন <i class="m-icon-swapright m-icon-black"></i></a>
    <br>
    <br>


    <br>
    <div class="row">
        <p style="color: grey; font-size: 10px;">*** এই ইমেইলটি সিস্টেম কর্তৃক স্বয়ংকৃতভাবে তৈরী হয়েছে। এই ইমেইলের কোনো প্রতিউত্তর(reply) দেয়ার প্রয়োজন নেই।</p>
    </div>

</div>