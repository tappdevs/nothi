<div class="row">
    <div class="col-md-6">
        <?= $OfficeUnitOrganogramCellFrom = $this->cell('SelectOrganogram', ['prefix' => 'from_']) ?>
    </div>
    <div class="col-md-6">
        <?= $OfficeUnitOrganogramCellTo = $this->cell('SelectOrganogram', ['prefix' => 'to_']) ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <a class="btn btn-xs green pull-right" id="btnSubmit" href="javascript:">
            <?= __("Submit") ?>
        </a>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <table class="table table-striped table-bordered table-hover" id="sample_1">
            <thead>
            <tr>
                <th class="">Employee Mapped From</th>
                <th class="">Employee Mapped To</th>
                <th class="">Actions</th>
            </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
</div>

<script type="text/javascript">
    var OfficeOrgSelectionCell = {
        loadMinistryWiseLayers: function (prefix) {

            PROJAPOTI.ajaxSubmitDataCallback('<?php echo $this->request->webroot ?>' + 'officeSettings/loadLayersByMinistry',
                {'office_ministry_id': $("#" + prefix + "office-ministry-id").val()}, 'json',
                function (response) {
                    PROJAPOTI.projapoti_dropdown_map("#" + prefix + "office-layer-id", response, "--বাছাই করুন--");
                });
        },
        loadMinistryAndLayerWiseOfficeOrigin: function (prefix) {
            PROJAPOTI.ajaxSubmitDataCallback('<?php echo $this->request->webroot ?>' + 'officeSettings/loadOfficesByMinistryAndLayer',
                {
                    'office_ministry_id': $("#" + prefix + "office-ministry-id").val(),
                    'office_layer_id': $("#" + prefix + "office-layer-id").val()
                }, 'json',
                function (response) {
                    PROJAPOTI.projapoti_dropdown_map("#" + prefix + "office-origin-id", response, "--বাছাই করুন--");
                });
        },
        loadOriginOffices: function (prefix) {
            PROJAPOTI.ajaxSubmitDataCallback('<?php echo $this->request->webroot ?>' + 'officeManagement/loadOriginOffices',
                {'office_origin_id': $("#" + prefix + "office-origin-id").val()}, 'json',
                function (response) {
                    PROJAPOTI.projapoti_dropdown_map("#" + prefix + "office-id", response, "--বাছাই করুন--");
                });
        },
        loadOfficeUnits: function (prefix) {
            PROJAPOTI.ajaxSubmitDataCallback('<?php echo $this->request->webroot ?>' + 'officeManagement/loadOfficeUnits',
                {'office_id': $("#" + prefix + "office-id").val()}, 'html',
                function (response) {
                    $("#" + prefix + "office-unit-id").html(response);
//                    PROJAPOTI.projapoti_dropdown_map("#" + prefix + "office-unit-id", response, "--বাছাই করুন--");
                });
        },
        loadOfficeUnitDesignations: function (prefix) {
            PROJAPOTI.ajaxSubmitDataCallback('<?php echo $this->request->webroot ?>' + 'officeManagement/loadOfficeUnitOrganogramsWithName',
                {'office_unit_id': $("#" + prefix + "office-unit-id").val()}, 'json',
                function (response) {
//                    PROJAPOTI.projapoti_dropdown_map("#" + prefix + "office-unit-organogram-id", response, "--বাছাই করুন--");
                    PROJAPOTI.projapoti_dropdown_map_with_title("#" + prefix + "office-unit-organogram-id", response, "--বাছাই করুন--",'',{'key' : 'id','value' : 'designation','title' : 'name'  });
                });

        },
        getOfficerInfo: function (prefix, designation_id) {
            PROJAPOTI.ajaxSubmitDataCallback('<?php echo $this->request->webroot ?>' + 'employeeRecords/getOfficerInfo',
                {'designation_id': designation_id}, 'json',
                function (response) {
                    $("input[name=" + prefix + "officer_id]").val(response.id);
                    $("#" + prefix + "officer-name").val(response.name_bng);

                });
        },
        getOfficerMappings: function (prefix, designation_id) {
            PROJAPOTI.ajaxSubmitDataCallback('<?php echo $this->request->webroot ?>' + 'employeeRecords/getOfficerMappings',
                {'designation_id': designation_id}, 'json',
                function (response) {

                    /* $("input[name="+prefix+"officer_id]").val(response.id);
                     $("#"+prefix+"officer-name").val(response.name_bng); */
                    var mappings = response;
                    var str_output = "";
                    $.each($(mappings), function (x) {
                        var mapping = $(mappings)[x];
                        str_output += '<tr>';
                        str_output += '<td class="">' + mapping.office_unit_organogram_name + '</td>';
                        str_output += '<td class="">' + mapping.mapped_office_unit_organogram_name + '</td>';
                        str_output += '<td class="">';

                        str_output += '</td>';
                        str_output += '</tr>';
                    });

                    $("#sample_1 > tbody").html(str_output);
                });
        }
    };

    $("#from-office-ministry-id").change(function () {
        OfficeOrgSelectionCell.loadMinistryWiseLayers('from-');
    });
    $("#from-office-layer-id").change(function () {
        OfficeOrgSelectionCell.loadMinistryAndLayerWiseOfficeOrigin('from-');
    });
    $("#from-office-origin-id").change(function () {
        OfficeOrgSelectionCell.loadOriginOffices('from-');
    });
    $("#from-office-id").change(function () {
        OfficeOrgSelectionCell.loadOfficeUnits('from-');
    });
    $("#from-office-unit-id").change(function () {
        OfficeOrgSelectionCell.loadOfficeUnitDesignations('from-');
    });
    $("#from-office-unit-organogram-id").change(function () {
        //OfficeOrgSelectionCell.getOfficerInfo($(this).val(), 'from-');
        OfficeOrgSelectionCell.getOfficerMappings('from-', $(this).val());
    });

    $("#to-office-ministry-id").change(function () {
        OfficeOrgSelectionCell.loadMinistryWiseLayers('to-');
    });
    $("#to-office-layer-id").change(function () {
        OfficeOrgSelectionCell.loadMinistryAndLayerWiseOfficeOrigin('to-');
    });
    $("#to-office-origin-id").change(function () {
        OfficeOrgSelectionCell.loadOriginOffices('to-');
    });
    $("#to-office-id").change(function () {
        OfficeOrgSelectionCell.loadOfficeUnits('to-');
    });
    $("#to-office-unit-id").change(function () {
        OfficeOrgSelectionCell.loadOfficeUnitDesignations('to-');
    });
    $("#to-office-unit-organogram-id").change(function () {
        //OfficeOrgSelectionCell.getOfficerInfo($(this).val(), 'to-');
    });

    $("#btnSubmit").click(function () {
        if (!$("#from-office-unit-organogram-id").val()) {
            alert("You must select a designation on the Map Employee From form");
        } else if (!$("#to-office-unit-organogram-id").val()) {
            alert("You must select a designation on the Map Employee To form");
        } else {
            PROJAPOTI.ajaxSubmitDataCallback('<?php echo $this->request->webroot ?>' + 'employeeAdditionalRoles/mapAdditionalRoles',
                {
                    'main_id': $("#from-office-unit-organogram-id").val(),
                    'secondary_id': $("#to-office-unit-organogram-id").val()
                }, 'json',
                function (response) {
                    if (response == "1") {
                        alert("Successful mapping");
                    } else {
                        alert("Mapping failed");
                    }
                });
        }
    });
</script>