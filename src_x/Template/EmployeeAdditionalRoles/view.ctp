<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption"><i class=""></i>View Additional Roles</div>
        <div class="tools">
            <a href="javascript:" class="collapse"></a>
            <a href="#portlet-config" data-toggle="modal" class="config"></a>
            <a href="javascript:" class="reload"></a>
            <a href="javascript:" class="remove"></a>
        </div>
    </div>
    <div class="portlet-body">
        <div class="panel-body">
            <table class="table table-bordered">
                <tr>
                    <td class="text-right">Employee</td>
                    <td><?= h($employee_additional_role->employee_id) ?></td>
                </tr>
                <tr>
                    <td class="text-right">Office Record</td>
                    <td><?= h($employee_additional_role->office_record_id) ?></td>
                </tr>
                <tr>
                    <td class="text-right">Organogram</td>
                    <td><?= h($employee_additional_role->office_organogram_id) ?></td>
                </tr>
                <tr>
                    <td class="text-right">Role</td>
                    <td><?= h($employee_additional_role->role_id) ?></td>
                </tr>
                <tr>
                    <td><?= $this->Html->link('Edit This Additional Roles', ['action' => 'edit', $employee_additional_role->id], array('class' => 'btn btn-primary pull-right')) ?></td>
                    <td><?= $this->Html->link('Back to Additional Roles List', ['action' => 'index'], array('class' => 'btn btn-primary pull-left')) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>