<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption"><i class=""></i>View Batch</div>
        <div class="tools">
            <a href="javascript:" class="collapse"></a>
            <a href="#portlet-config" data-toggle="modal" class="config"></a>
            <a href="javascript:" class="reload"></a>
            <a href="javascript:" class="remove"></a>
        </div>
    </div>
    <div class="portlet-body">

        <div class="panel-body">
            <table class="table table-bordered">

                <tr>
                    <td class="text-right">Batch Number</td>
                    <td><?= h($employee_batch->batch_no) ?></td>
                </tr>
                <tr>
                    <td class="text-right">Batch Year</td>
                    <td><?= h($employee_batch->batch_year) ?></td>
                </tr>

                <tr>
                    <td><?= $this->Html->link('Edit This Batch', ['action' => 'edit', $employee_batch->id], array('class' => 'btn btn-primary pull-right')) ?></td>
                    <td><?= $this->Html->link('Back to Batch List', ['action' => 'index'], array('class' => 'btn btn-primary pull-left')) ?></td>
                </tr>

            </table>
        </div>
    </div>
</div>
