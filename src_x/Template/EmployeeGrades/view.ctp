<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption"><i class=""></i>View Grade</div>
        <div class="tools">
            <a href="javascript:" class="collapse"></a>
            <a href="#portlet-config" data-toggle="modal" class="config"></a>
            <a href="javascript:" class="reload"></a>
            <a href="javascript:" class="remove"></a>
        </div>
    </div>
    <div class="portlet-body">
        <div class="panel-body">
            <table class="table table-bordered">
                <tr>
                    <td class="text-right">Grade Name in English</td>
                    <td><?= h($employee_grade->grade_name_eng) ?></td>
                </tr>
                <tr>
                    <td class="text-right">Grade Name in Bangla</td>
                    <td><?= h($employee_grade->grade_name_bng) ?></td>
                </tr>
                <tr>
                    <td class="text-right">Cadre ID</td>
                    <td><?= h($employee_grade->employee_cadre_id) ?></td>
                </tr>
                <tr>
                    <td class="text-right">Rank ID</td>
                    <td><?= h($employee_grade->employee_rank_id) ?></td>
                </tr>
                <tr>
                    <td><?= $this->Html->link('Edit This Grade', ['action' => 'edit', $employee_grade->id], array('class' => 'btn btn-primary pull-right')) ?></td>
                    <td><?= $this->Html->link('Back to Grade List', ['action' => 'index'], array('class' => 'btn btn-primary pull-left')) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>