<div class="portlet light">
    <div class="portlet-title">
        <div class="caption"><i class="fs1 a2i_gn_details1"></i><?php echo __('Rank') ?> <?php echo __('List') ?></div>
        <!-- <div class="tools">
             <a href="javascript:" class="collapse"></a>
             <a href="#portlet-config" data-toggle="modal" class="config"></a>
             <a href="javascript:" class="reload"></a>
             <a href="javascript:" class="remove"></a>
         </div>-->
    </div>
    <div class="portlet-body">
        <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                        <?= $this->Html->link(__('Add New'), ['action' => 'add'], ['class' => 'btn btn-default']) ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="btn-group pull-right">
                        <button class="btn dropdown-toggle" data-toggle="dropdown"><?php echo __('Tools') ?> <i
                                class="glyphicon glyphicon-chevron-down"></i></button>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="#"><?php echo __('Print') ?> </a></li>
                            <li><a href="#"><?php echo __('Save as PDF') ?> </a></li>
                            <li><a href="#">Export to Excel </a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th class="text-center"><?php echo __('Ministry') ?> <?php echo __('ID') ?></th>
                <th class="text-center"><?php echo __('Rankr') ?> <?php echo __('Name English') ?></th>
                <th class="text-center"><?php echo __('Rankr') ?> <?php echo __('Name Bangla') ?></th>
                <th class="text-center"><?php echo __('Actions') ?></th>
                <!--th class="text-center"><?= $this->Paginator->sort('Division') ?></th>
                <th class="text-center"><?= $this->Paginator->sort('District Name English') ?></th>
                <th class="text-center"><?= $this->Paginator->sort('District Name Bangla') ?></th>
                <th class="text-center"><?= $this->Paginator->sort('BBS Code') ?></th>
                <th class="actions text-center"><?= __('Actions') ?></th-->
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($query as $employeeRank): ?>
                <tr>
                    <td class="text-center"><?= h($employeeRank->office_ministry_id) ?></td>
                    <td class="text-center"><?= h($employeeRank->rank_name_eng) ?></td>
                    <td class="text-center"><?= h($employeeRank->rank_name_bng) ?></td>
                    <td class="actions text-center">
                        <!--<?= $this->Html->link(__('View'), ['action' => 'view', $employeeRank->id], ['class' => 'btn btn-primary']) ?>-->
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $employeeRank->id], ['class' => 'btn green']) ?>
                        <!--<?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $employeeRank->id], ['class' => 'btn btn-danger'], ['confirm' => __('Are you sure you want to delete # {0}?', $employeeRank->id)]) ?>-->
                    </td>
                </tr>

            <?php endforeach; ?>
            </tbody>
        </table>

        <div class="paginator text-center">
            <!--ul class="pagination">
                <?= $this->Paginator->prev('< ' . __('Previous')) ?>
                <?= $this->Paginator->numbers() ?>
                <?= $this->Paginator->next(__('Next') . ' >') ?>
            </ul>
            <p><?= $this->Paginator->counter() ?></p-->
        </div>
    </div>
</div>
