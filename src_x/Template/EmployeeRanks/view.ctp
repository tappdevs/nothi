<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption"><i class=""></i>View Rank</div>
        <div class="tools">
            <a href="javascript:" class="collapse"></a>
            <a href="#portlet-config" data-toggle="modal" class="config"></a>
            <a href="javascript:" class="reload"></a>
            <a href="javascript:" class="remove"></a>
        </div>
    </div>
    <div class="portlet-body">
        <div class="panel-body">
            <table class="table table-bordered">

                <tr>
                    <td class="text-right">District Name in Bangla</td>
                    <td><?= h($employee_rank->rank_name_bng) ?></td>
                </tr>
                <tr>
                    <td class="text-right">District Name in English</td>
                    <td><?= h($employee_rank->rank_name_eng) ?></td>
                </tr>
                <tr>
                    <td class="text-right">Ministry</td>
                    <td><?= h($employee_rank->office_ministry_id) ?></td>
                </tr>


                <tr>
                    <td><?= $this->Html->link('Edit This Rank', ['action' => 'edit', $employee_rank->id], array('class' => 'btn btn-primary pull-right')) ?></td>
                    <td><?= $this->Html->link('Back to Rank List', ['action' => 'index'], array('class' => 'btn btn-primary pull-left')) ?></td>
                </tr>

            </table>
        </div>
    </div>
</div>
