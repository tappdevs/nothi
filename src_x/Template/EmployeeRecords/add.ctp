<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption"><i class=""></i><?php echo __('Employeer') ?> <?php echo __('Information') ?>
        </div>
        <!-- <div class="tools">
             <a href="javascript:" class="collapse"></a>
             <a href="#portlet-config" data-toggle="modal" class="config"></a>
             <a href="javascript:;" class="reload"></a>
             <a href="javascript:;" class="remove"></a>
         </div>-->
    </div>
    <div class="portlet-body form">
        <?php echo $this->Form->create('EmployeeRecord', array('id' => 'EmployeeRecordForm')); ?>
        <div class="form-body">
            <?php echo $this->element('EmployeeRecords/personal_info'); ?>
            <?php echo $this->element('EmployeeRecords/professional_info'); ?>
       <!--     <?php /*echo $this->element('EmployeeRecords/office_info'); */?>
            --><?php echo $this->element('EmployeeRecords/login_info'); ?>
        </div>
        <?php echo $this->element('submit'); ?>
        <?php echo $this->Form->end(); ?>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        OfficeSetup.init();
        $('.date-picker').datepicker({
            rtl: Metronic.isRTL(),
            orientation: "left",
            autoclose: true,
            format:"yyyy-mm-dd"
        });
    });
    $( "#EmployeeRecordForm" ).submit(function( event ) {
        if($("#nid").val().length != 17 && $("#nid").val().length != 10){
              event.preventDefault();
              toastr.error(' জাতীয় পরিচয়পত্র নম্বর ১৭ সংখ্যার হতে হবে। ');
        }
          if ($("#date-of-birth").val().length == 0) {
            event.preventDefault();
            toastr.error(' জন্ম তারিখ দেওয়া হয়নি । ');
        }
          
});

    function check_10_digit_nid() {
        if ($("#nid").val().length == 10) {
            bootbox.dialog({
                message: "স্মার্ট জাতীয় পরিচয় পত্র দিয়ে ব্যাবহারকারীর অ্যাকাউন্ট করলে তা সচল হবে না। এ জন্য অবশ্যই অফিস এডমিন অথবা সাপোর্ট টিমের সাথে যোগাযোগ করতে হবে। আপনি কি পরবর্তী ধাপে যেতে চান?",
                title: "সতর্কীকরণ",
                buttons: {
                    success: {
                        label: "হ্যাঁ",
                        className: "green",
                        callback: function () {
                            //$("#EmployeeRecordForm").submit();
                        }
                    },
                    danger: {
                        label: "না",
                        className: "red",
                        callback: function () {
                            $("#nid").val("");
                            $("#nid").focus();
                            //return false;
                        }
                    }
                }
            });
        }
    }
</script>