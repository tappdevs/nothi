
<?php
echo $this->Html->css('assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css', ['block'=>true]);
echo $this->Html->css('assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css', ['block'=>true]);
echo $this->Html->css('assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css', ['block'=>true]);
?>

<ul class="page-breadcrumb breadcrumb"><!-- ADD CLASS hide for not showing breadcrumb -->
	<li><a href="<?php echo $this->request->webroot;?>">Home</a><i class="fa fa-circle"></i></li>
	<li><a href="<?php echo $this->request->webroot;?>users/">Users</a><i class="fa fa-circle"></i></li>
	<li class="active">Add Bulk User</li>
</ul>
<!-- END PAGE BREADCRUMB -->
<div class="portlet box blue">
	<div class="portlet-title">
		<div class="caption"><i class=""></i>Add Bulk User</div>
		<div class="tools">
			<a href="javascript:;" class="collapse"></a>
			<a href="#portlet-config" data-toggle="modal" class="config"></a>
			<a href="javascript:;" class="reload"></a>
			<a href="javascript:;" class="remove"></a>
		</div>
	</div>
	<div class="portlet-body form">
		<!-- BEGIN FORM-->
		 <?= $this->Form->create('', ['action' => 'addBulkReport','type'=>'post','class'=>'form-horizontal','type'=>'file']);?>
			<div class="form-body">
				<div class="form-group">
					<label class="col-md-3 control-label">Upload User File </label>
					<div class="col-md-4">
						<div class="input-group">
							<span class="input-group-addon input-circle-left">
							<i class="fa fa-file"></i>
							</span>
							<?= $this->Form->file('user',['label'=>false,'class'=>'form-control input-circle-right','required'=>true]);?>
						</div>
					</div>
				</div>
			</div>
			<div class="form-actions">
				<div class="row">
					<div class="col-md-offset-3 col-md-9">
						<button type="submit" class="btn   blue">Submit</button>
						<button type="reset" class="btn   default">Cancel</button>
					</div>
				</div>
			</div>
		<?= $this->Form->end();?>

	</div>

</div>

