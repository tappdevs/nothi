<div class="row">
    <div class="col-md-12">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fs1 a2i_gn_user1"></i><?php echo __('Employeer') ?> <?php echo __('Recordr') ?> <?php echo __('List') ?>
                </div>
                <div class="actions">
                    <?=
                    $this->Html->link( '<i class="fs1 a2i_gn_add1"></i> ' .__('Add New Employee Record'),
                        ['action' => 'add'],
                        ['class' => 'btn btn-info  ','escape'=>false])
                    ?>
                </div>
            </div>

            <div class="portlet-body">
                <?=
                $cell = $this->cell('OfficeSelection', ['entity' => ''])
                ?>
                <div class="row">
                    <div class="form-group">
                        <div class="col-sm-4">
                            <?php
                            echo $this->Form->input('office_id',
                                array(
                                'label' => __('Office'),
                                'class' => 'form-control',
                                'empty' => '----'
                            ));
                            ?>
                        </div>
                        <div class="col-sm-4">
                            <?php
                            echo $this->Form->input('name_bng',
                                array(
                                'label' => __('Name').' '.__('Bangla'),
                                'class' => 'form-control',
                                'empty' => '----'
                            ));
                            ?>
                        </div>
                        <div class="col-sm-4">
                            <?php
                            echo $this->Form->input('username',
                                array(
                                'label' => __('Login ID'),
                                'class' => 'form-control'
                            ));
                            ?>
                        </div>

                    </div>
                </div>
                <br/>
                <div class="row">
                    <div class="form-group">
                        <div class="col-sm-4">
                            <?php
                            echo $this->Form->input('identity_no',
                                array(
                                'label' => __('পরিচিতি নম্বর'),
                                'class' => 'form-control',
                                'empty' => '----'
                            ));
                            ?>
                        </div>
                        <div class="col-sm-4">
                            <?php
                            echo $this->Form->input('email',
                                array(
                                'label' => __('ইমেইল'),
                                'class' => 'form-control',
                                'empty' => '----'
                            ));
                            ?>
                        </div>
                        <div class="col-sm-4">
                            <?php
                            echo $this->Form->input('mobile',
                                array(
                                'label' => __('মোবাইল'),
                                'class' => 'form-control'
                            ));
                            ?>
                        </div>

                    </div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-sm-4 ">
                        <?php
                        echo $this->Form->button(__('Search'),
                            array(
                            'type' => 'button',
                            'class' => 'btn   btn-primary',
                            'id' => 'searchButton'
                        ));
                        ?>

                    </div>
                </div>

                <div class="table-list">

                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    var EmployeeAssignment = {
        checked_node_ids: [],
        loadMinistryWiseLayers: function () {
            OfficeSetup.loadLayers($("#office-ministry-id").val(), function (response) {

            });
        },
        loadMinistryAndLayerWiseOfficeOrigin: function () {
            OfficeSetup.loadOffices($("#office-ministry-id").val(), $("#office-layer-id").val(), function (response) {

            });
        },
        loadOriginOffices: function () {
            OfficeSetup.loadOriginOffices($("#office-origin-id").val(), function (response) {

            });
        },
        loadEmployees: function () {
            Metronic.blockUI({
                target: '#ajax-content',
                boxed: true
            });
            var loginid = $('#username').val();
            if(loginid.length != 12 && loginid.length > 0){
                var start = loginid.substr(0, 1);
                var restof = loginid.substr(1);
                loginid = start + pad(restof, 11);
                $('#username').val(loginid);
            }
            PROJAPOTI.ajaxSubmitAsyncDataCallback(js_wb_root + 'EmployeeRecords/employeeRecordListAjax',
                    {'office_id': $('#office-id').val(), name_bng: $('#name-bng').val(), username: $('#username').val(), identity_no: $('#identity-no').val(), email: $('#email').val(), mobile: $('#mobile').val()}, 'html',
                    function (response)
                    {
                        Metronic.unblockUI('#ajax-content');
                        $('.table-list').html(response);

                    }
            );
        }
    };

    $(function () {
        EmployeeAssignment.loadEmployees();

        $("#office-ministry-id").bind('change', function () {
            EmployeeAssignment.loadMinistryWiseLayers();
        });

        $("#office-layer-id").bind('change', function () {
            EmployeeAssignment.loadMinistryAndLayerWiseOfficeOrigin();
        });

        $("#office-origin-id").bind('change', function () {
            EmployeeAssignment.loadOriginOffices();
        });

        $("#searchButton").bind('click', function () {
            EmployeeAssignment.loadEmployees();
        });
    });
    function pad(str, max) {
        str = str.toString();
        return str.length < max ? pad("0" + str, max) : str;
    }
</script>