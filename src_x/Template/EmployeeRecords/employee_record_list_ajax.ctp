<div class=" table-scrollable">
    <table class="table table-striped table-advance table-bordered table-hover">
        <thead>
            <tr class="">
                <th class="text-center ajaxSort" style="width: 15%"><?php
                    echo $this->Paginator->sort("name_bng", __('Name').' '.__("Bangla"))
                    ?></th>
                <th class="text-center ajaxSort" style="width: 15%"><?php
                    echo $this->Paginator->sort("name_bng", __('Name').' (ইংরেজি)')
                    ?></th>
                <th class="text-center ajaxSort"><?php
                    echo $this->Paginator->sort("identity_no", __('Identity No'))
                    ?></th>
                <th class="text-center"><?php echo __('Email') ?></th>
                <th class="text-center"><?php echo __('Mobile') ?></th>
                <th class="text-center"> ক্যাডার </th>
                <th class="text-center" style="width: 15%"><?php echo __('Designations') ?> </th>
                <th class="text-center" style="width: 10%"><?php echo __('Unit') ?></th>
                <th class="text-center" style="width: 10%"><?php echo (__('Office')) ?></th>
                <th class="text-center" style="width: 5%"><?php echo __('Login ID') ?></th>
                <th class="text-center" colspan="2"><?php echo __('কার্যক্রম') ?></th>
            </tr>
        </thead>
        <tbody>

            <?php
            $i = 0;
            if (!empty($employee_data)) {
                foreach ($employee_data as $rows):
                    $i++;
                    ?>
                    <tr class="<?= ($rows->status == 0 ? 'danger' : '') ?>">
                        <td>
                            <?= h($rows->name_bng) ?>
                            <?php
                            if($rows['active'] == 0):
                                echo $this->Form->create('Post', ['action' => 'employeeActive', 'class' => 'form-horizontal']);
                                echo $this->Form->input('user_id', array('label' => false, 'type' => 'hidden', 'value' => $rows->user_id));
                                echo "<br/>".$this->Form->submit(__('সক্রিয় করুন'),array('class' => 'btn btn-md green'))."";
                                echo $this->Form->end();
                            endif;
                            ?>
                        </td>
                        <td><?= $rows->name_eng ?></td>
                        <td><?= $rows->identity_no ?></td>
                        <td><?= $rows->personal_email ?></td>
                        <td><?= $rows->personal_mobile ?></td>
                        <td class="text-center"><?=
            $rows->is_cadre == 1 || $rows->is_cadre == 5 ? __("হ্যাঁ") : __("না")
                    ?></td>
                        <td><?=
                            $rows->designation_bng.(!empty($rows->incharge_label) ? (" (".$rows->incharge_label.')')
                                    : '')
                            ?></td>
                        <td><?= $rows->unit_name_bng ?></td>
                        <td><?= $rows->office_name_bng ?></td>
                        <td id='<?= $rows["id"]?>'><?= $rows->username ?></td>
                        <td class="text-center">
        <?=
        $this->Html->link('', ['action' => 'edit', $rows['id']],
            ['class' => 'fa fa-edit text-primary', 'target' => '_tab'])
        ?>
                        </td>
                        <td class="text-center">
                            <?php
                            if ($auth['user_role_id'] == 1):
                            ?>
                                <a href="javascript:void(0)" class="psChange" data-toggle = 'tooltip' title = 'পাসওয়ার্ড পরিবর্তন'
                                   onclick = resetuserPass(<?=$rows["id"]?>)>
                                    <i class="fa fa-key" aria-hidden="true"></i>
                                </a>
                                <?php
                            endif;
                            ?>

                        </td>
                    </tr>
                            <?php
                        endforeach;
                    }
                    if ($i == 0) {
                        echo "<tr class='danger text-center'><td colspan=12>দুঃখিত কোন তথ্য পাওয়া যায় নি</td></tr>";
                    }
                    ?>

        </tbody>
    </table>
</div>
<div class="actions text-center">
    <ul class="pagination pagination-sm">
<?php
echo $this->Paginator->first(__('প্রথম', true), array('class' => 'number-first'));
echo $this->Paginator->prev('<<', array('tag' => 'li', 'escape' => false),
    '<a href="#" class="btn btn-sm blue ">&raquo;</a>',
    array('class' => 'next disabled', 'tag' => 'li', 'escape' => false));
echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li',
    'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a',
    'reverse' => FALSE, 'modulus' => 10));
echo $this->Paginator->next('>>', array('tag' => 'li', 'escape' => false),
    '<a href="#" class="btn btn-sm blue">&laquo;</a>',
    array('class' => 'prev disabled btn btn-sm blue', 'tag' => 'li',
    'escape' => false));

echo $this->Paginator->last(__('শেষ', true), array('class' => 'number-last'));
?>
    </ul>
</div>

<script>
    $('.pagination a').click(function (ev) {

        Metronic.blockUI({
            target: '#ajax-content',
            boxed: true
        });
        ev.preventDefault();
        PROJAPOTI.ajaxSubmitAsyncDataCallback($(this).attr('href'), {office_id: $('#office-id').val(), name_bng: $('#name-bng').val(), username: $('#username').val(), identity_no: $('#identity-no').val(), email: $('#email').val(), mobile: $('#mobile').val()}, 'html',
                function (response)
                {
                    Metronic.unblockUI('#ajax-content');
                    $('.table-list').html(response);

                }
        );
    });

    $('.ajaxSort a').click(function (ev) {

        Metronic.blockUI({
            target: '#ajax-content',
            boxed: true
        });
        ev.preventDefault();
        PROJAPOTI.ajaxSubmitAsyncDataCallback($(this).attr('href'), {office_id: $('#office-id').val(), name_bng: $('#name-bng').val(), username: $('#username').val(), identity_no: $('#identity-no').val(), email: $('#email').val(), mobile: $('#mobile').val()}, 'html',
                function (response)
                {
                    Metronic.unblockUI('#ajax-content');
                    $('.table-list').html(response);

                }
        );
    });
//    $('.psChange').click
    function resetuserPass(emp_rcrd){
        var user_name = $("#"+emp_rcrd).text();
        var showmsg = '';
        if(user_name == '' || user_name == undefined || user_name == null || typeof(user_name) == 'undefined'){
            showmsg = 'আপনি কি পাসওয়ার্ড পরিবর্তন করতে ইচ্ছুক? ';
        }else{
            showmsg = 'আপনি কি ' + user_name +' এর পাসওয়ার্ড পরিবর্তন করতে ইচ্ছুক? ';
        }
         bootbox.dialog({
                    message: showmsg ,
                    title: " পাসওয়ার্ড পরিবর্তন",
                    buttons: {
                        success: {
                            label: " পরিবর্তন করুন ",
                            className: "green",
                            callback: function () {
                                psChange(emp_rcrd);
                            }
                        },
                        danger: {
                            label: " বাতিল করুন ",
                            className: "red",
                            callback: function () {
                            }
                        },
                    }
                });
    }
    function psChange(emp_rcrd) {
        $.ajax({
            type: 'POST',
            url: "<?php echo $this->Url->build(['controller' => 'Users', 'action' => 'adminPasswordChange']) ?>/"+emp_rcrd,
            success: function (data) {
                if(data.status == 'error'){
                    toastr.error(data.msg);
                }else{
                     bootbox.dialog({
                    message: data.msg ,
                    title: " পাসওয়ার্ড পরিবর্তন সংক্রান্ত তথ্য ",
                    buttons: {
                        danger: {
                            label: " বন্ধ করুন ",
                            className: "red",
                            callback: function () {
                            }
                        },
                    }
                });
                }
            }
        });
    }
    $('[data-toggle="tooltip"]').tooltip({'placement':'bottom'});
</script>