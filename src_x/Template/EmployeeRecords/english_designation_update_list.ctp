<div class="row">
    <div class="col-md-12">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fs1 a2i_gn_edit2"></i>  অফিস কর্মকর্তাদের ইংরেজি পদবি সংশোধন
                </div>
                <!--<div class="tools">
                    <a href="javascript:" class="collapse"></a>
                    <a href="#portlet-config" data-toggle="modal" class="config"></a>
                    <a href="javascript:;" class="reload"></a>
                    <a href="javascript:;" class="remove"></a>
                </div>-->
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="sample_1">
                    <thead>
                    <tr>
                        <th class="text-center">নাম</th>
                        <th class="text-center">পদবি বাংলায়</th>
                        <th class="text-center">পদবি ইংরেজিতে</th>
                        <th class="text-center"><?php echo __('Actions') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr class="odd gradeX">
                        <?php
                        if(!empty($datas)){
                        foreach ($datas as $data):
                        ?>
                    <tr>
                        <td class="text-center"><?php echo h($data['name_bng']); ?></td>
                        <td class="text-center"><?php echo $data['designation_bng']; ?></td>
                        <td class="text-center designation_eng"><?php echo $data['designation_eng']; ?></td>
                        <td class="text-center">

                            <button class ="btn btn-primary office_unit_organogram_id" value="<?= $data->office_unit_organogram_id ?>">পদবি সংশোধন</button>

                        </td>
                        <?php
                        endforeach; } else { ?>
                    <tr><td class="text-center" colspan="4" style="color:red;"> দুঃখিত কোন তথ্য পাওয়া যায় নি।  </td></tr>
                    <?php }  ?>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="actions text-center">
    <ul class="pagination pagination-sm">
        <?php
        echo $this->Paginator->first(__('প্রথম', true), array('class' => 'number-first'));
        echo $this->Paginator->prev('<<', array('tag' => 'li', 'escape' => false), '<a href="#" class="btn btn-sm blue">&laquo;</a>', array('class' => 'prev disabled btn btn-sm blue', 'tag' => 'li', 'escape' => false));
        echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a', 'reverse' => FALSE));
        echo $this->Paginator->next('>>', array('tag' => 'li', 'escape' => false), '<a href="#" class="btn btn-sm blue ">&raquo;</a>', array('class' => 'next disabled', 'tag' => 'li', 'escape' => false));
        echo $this->Paginator->last(__('শেষ', true), array('class' => 'number-last'));
        ?>
    </ul>

</div>

<!--Update Modal-->
<div id="UpdateModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">পদবি পরিবর্তন করুন</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="<?php echo $this->request->webroot; ?>EmployeeRecords/englishDesignationUpdate">
                    <div class="form-group">
                        <label for="eng_designation">পদবি ইংরেজিতে</label>
                        <input type="text" class="form-control" id="eng_designation" name="eng_designation" value="">
                    </div>
                    <input type="hidden" value="" id="office_unit_organogram_for_modal" name="office_unit_organogram_id">

                    <button type="submit" class="btn btn-default"><?= __('Submit') ?></button>
                </form>
            </div>
            <div class="modal-footer">
            </div>
        </div>

    </div>
</div>
<script>
        $('.office_unit_organogram_id').on('click',function () {
          var office_unit_organogram_id =  $(this).val();
          var designation =  $(this).closest('tr').find('.designation_eng').text();
          $('#eng_designation').val(designation);
          $('#office_unit_organogram_for_modal').val(office_unit_organogram_id);
          $('#UpdateModal').modal('show');
        });
</script>