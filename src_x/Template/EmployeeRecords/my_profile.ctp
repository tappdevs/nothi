<style>
    .showContent a {
        font-size: 11pt !important;
    }
</style>

<div class="row margin-top-20">
    <div class="col-md-12">
        <!-- BEGIN PROFILE SIDEBAR -->
        <div class="profile-sidebar">
            <!-- PORTLET MAIN -->
            <div class="portlet light profile-sidebar-portlet">
                <!-- SIDEBAR USERPIC -->
                <div class="profile-userpic">
                    <img src="<?php echo $this->Url->build(['_name' => 'getPhoto', $user['username']]) ?>"
                         class="img-responsive" alt="ছবি পাওয়া যায়নি">
                </div>
                <!-- END SIDEBAR USERPIC -->
                <!-- SIDEBAR USER TITLE -->
                <div class="profile-usertitle">
                    <div class="profile-usertitle-name">
                        <?php echo h($employee_info['officer_name']); ?>
                    </div>
                    <div class="profile-usertitle-job">
                        <?php echo $employee_info['designation_label']; ?>
                    </div>
                </div>
                <!-- END SIDEBAR USER TITLE -->
                <!-- SIDEBAR MENU -->

                <!-- END MENU -->
                <hr/>
                <div class='portlet light'>
                    <?php if (!empty($employee_own['personal_info']['personal_mobile'])): ?>
                        <div class="margin-top-20 profile-desc-link">
                            <i class="fa fa-phone-square"></i>
                            <a href="javascript:void(0);"><?php echo h($employee_own['personal_info']['personal_mobile']) ?></a>
                        </div>
                    <?php endif ?>
                    <?php if (!empty($employee_own['personal_info']['personal_email'])): ?>
                        <div class="margin-top-20 profile-desc-link">
                            <i class="fs1 a2i_gn_email1"></i>
                            <a href="javascript:void(0);"><?php echo h($employee_own['personal_info']['personal_email']) ?></a>
                        </div>
                    <?php endif ?>
                    <?php if (!empty($employee_info['office_fax'])): ?>
                        <div class="margin-top-20 profile-desc-link">
                            <i class="fs1 a2i_gn_fax1"></i>
                            <a href="javascript:void(0);"><?php echo h($employee_info['office_fax']) ?></a>

                        </div>
                    <?php endif ?>
                    <?php if (!empty($employee_info['office_web'])): ?>
                        <div class="margin-top-20 profile-desc-link">
                            <i class="fa fa-globe"></i>
                            <a href="<?php echo h($employee_info['office_web']) ?>"
                               target="_tab"><?php echo h($employee_info['office_web']) ?></a>
                        </div>
                    <?php endif ?>
                </div>
            </div>
            <!-- END PORTLET MAIN -->
        </div>
        <!-- END BEGIN PROFILE SIDEBAR -->
        <div class="profile-content">
            <div class="tabbable-custom nav-justified">
                <ul class="nav nav-tabs nav-justified">
                    <?php if ($user['user_role_id'] > 2): ?>
                        <li class="profileli active showContent">
                            <a href="<?php echo $this->Url->build(['controller' => 'EmployeeRecords', 'action' => 'profileView']) ?>">
                                <i class="fs1 a2i_gn_user1"></i>
                                প্রোফাইল তথ্য </a>
                        </li>
                    <?php endif; ?>
                    <li class="profileppassword  showContent">
                        <a href="<?php echo $this->Url->build(['controller' => 'EmployeeRecords', 'action' => 'passwordChange']) ?>">
                            <i class="fs1 a2i_gn_secrecy3"></i>
                            পাসওয়ার্ড </a>
                    </li>

                    <li class="profilephoto showContent">
                        <a href="<?php echo $this->Url->build(['controller' => 'EmployeeRecords', 'action' => 'profileUpload']) ?>">
                            <i class="icon-picture"></i>
                           প্রোফাইল ছবি  </a>
                    </li>
                    <?php if ($user['user_role_id'] > 2): ?>
                        <li class="signature showContent">
                            <a href="<?php echo $this->Url->build(['controller' => 'EmployeeRecords', 'action' => 'signatureUpload']) ?>">
                                <i class="fs1 a2i_gn_onumodondelevery2"></i>
                                স্বাক্ষর </a>
                        </li>
                        <li class="protikolposettingsli showContent">
                            <a href="<?= $this->request->webroot; ?>getProtikolpoSettings"
                               target="_blank">
                                <i class="fs1 efile-user_manage3"></i>
                                প্রতিকল্প </a>
                        </li>
                        <li class="settingsli showContent">
                            <a href="<?php
                            echo $this->Url->build(['controller' => 'notificationMessages',
                                'action' => 'notificationSettings'])
                            ?>"
                               target="_blank">
                                <i class="fs1 a2i_gn_sms1"></i>
                                নোটিফিকেশন </a>
                        </li>
	                    <li class="viewPermissionToOther showContent">
		                    <a href="<?php echo $this->Url->build(['controller' => 'EmployeeRecords', 'action' => 'view_permission_settings']) ?>">
			                    <i class="fa fa-share-alt" aria-hidden="true"></i>
			                    শেয়ার  </a>
	                    </li>

                    <?php endif; ?>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active showContentDiv">

                    </div>

                </div>
            </div>

        </div>
    </div>
</div>

<script>

    $(document).on('click', '.showContent>a', function (e) {
        e.preventDefault();

        var href = $(this).attr('href');

        $('.showContentDiv').html('<img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"><span>&nbsp;&nbsp;লোড করা হচ্ছে...</span>');
        $('.showContent').removeClass('active');
        <?php if ($user['user_role_id'] > 2) {
        ?>
        if ($(this).closest('li').hasClass('profilephoto')) {
            $('.profilephoto').addClass('active');
        } else if ($(this).closest('li').hasClass('signature')) {
            $('.signature').addClass('active');
        } else if ($(this).closest('li').hasClass('profileppassword')) {
            $('.profileppassword').addClass('active');
        } else if ($(this).closest('li').hasClass('protikolposettingsli')) {
            $('.protikolposettingsli').addClass('active');
        } else if ($(this).closest('li').hasClass('settingsli')) {
            $('.settingsli').addClass('active');
        }else if ($(this).closest('li').hasClass('viewPermissionToOther')) {
            $('.viewPermissionToOther').addClass('active');
        } else {
            $('.profileli').addClass('active');
        }
        <?php
        }
        else {
        ?>
        if ($(this).closest('li').hasClass('profilephoto')) {
            $('.profilephoto').addClass('active');
        } else if ($(this).closest('li').hasClass('profileppassword')) {
            $('.profileppassword').addClass('active');
        }
        <?php
        }
        ?>
        $.ajax({
            url: href,
            cache: false,
            success: function (res) {
                $('.showContentDiv').html(res);
                $('[data-toggle="tooltip"]').tooltip({'placement':'bottom'});
            }
        });
    });

    $(function () {

        $('.showContentDiv').html('<img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"><span>&nbsp;&nbsp;লোড করা হচ্ছে...</span>');
        $(document).ready(function() {
            var searchParams = new URLSearchParams(window.location.search);
            if (searchParams.has('page')) {
                if (searchParams.get('page') == 'signature') {
                    $(".signature>a").trigger('click');
                }
                if (searchParams.get('page') == 'profileppassword') {
                    $(".profileppassword>a").trigger('click');
                }
                if (searchParams.get('page') == 'profilephoto') {
                    $(".profilephoto>a").trigger('click');
                }
                if (searchParams.get('page') == 'viewPermissionToOther') {
                    $(".viewPermissionToOther>a").trigger('click');
                }
            } else {
                <?php if ($user['user_role_id'] > 2) { ?>
                $.ajax({
                    url: '<?php echo $this->Url->build(['controller' => 'EmployeeRecords', 'action' => 'profileView']) ?>',
                    cache: false,
                    success: function (res) {
                        $('.showContentDiv').html(res);
                        $('.date-picker').datepicker({
                            rtl: Metronic.isRTL(),
                            autoclose: true
                        });
                        $('[data-toggle="tooltip"]').tooltip({'placement':'bottom'});

                        $(document).find('.make-switch').bootstrapSwitch();
                    }
                });
                <?php } else { ?>
                $.ajax({
                    url: '<?php echo $this->Url->build(['controller' => 'EmployeeRecords', 'action' => 'passwordChange']) ?>',
                    cache: false,
                    success: function (res) {
                        $('.showContentDiv').html(res);
                        $('.date-picker').datepicker({
                            rtl: Metronic.isRTL(),
                            autoclose: true
                        });
                        $('[data-toggle="tooltip"]').tooltip({'placement':'bottom'});

                        $(document).find('.make-switch').bootstrapSwitch();
                        $('.profileppassword').addClass('active');
                    }
                });
                <?php } ?>
            }
        }); 
    });
</script>
