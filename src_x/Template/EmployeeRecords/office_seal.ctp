<?php
$session = $this->request->session();
$logged_user = $session->read('logged_employee_record');
$office_records = $logged_user['office_records'];

$office_unit_options = array();
if (!empty($office_records)) {
    foreach ($office_records as $record) {
       
        $office_unit_options[] = [
          'text'=>$record['unit_name'],  
          'value'=>$record['office_unit_id'],  
          'office-id'=>$record['office_id'],  
        ];
    }
}
?>

<div class="portlet light">
    <div class="portlet-title">
        <div class="caption"><i class="fa fa-group"></i>Office Seal</div>
        <div class="tools">
            <a href="javascript:" class="collapse"></a>
            <a href="#portlet-config" data-toggle="modal" class="config"></a>
            <a href="javascript:;" class="reload"></a>
            <a href="javascript:;" class="remove"></a>
        </div>
    </div>
    <div class="portlet-body form">
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET -->
                <div class="">
                    <div class="portlet-title">
                        <div class="row">
                            <div class="col-md-5 form-group form-horizontal">
                                <?php
                                echo $this->Form->input('office_unit_id', array(
                                    'label' => false,
                                    'class' => 'form-control',
                                    'options' => $office_unit_options
                                ));
                                ?>
                            </div>
                            <div class="col-md-1"></div>
                            <div class="col-md-6 office-seal form-group form-horizontal">
                                <div class="col-md-10" id="office_unit_seal_panel"></div>
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row office-seal">
                            <div class="portlet light col-md-12">
                                <div class="portlet-title">
                                    <div class="caption" style="width:100%;">
                                        <span>
                                            <i class="fa fa-check-square-o"></i><?php echo __("অফিস শাখার অর্গানোগ্রাম") ?>
                                        </span>
                                    </div>
                                </div>
                                <div class="portlet-window-body">
                                    <div class="col-md-8" id="office_unit_tree_panel">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PORTLET -->
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var OfficeSeal = {
        checked_node_ids: [],
        getUnitOrganogramByOfficeId: function (office_id) {
            $("#office_unit_tree_panel").html("");
            PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + 'officeManagement/getOfficeDesignationsAndIdentificationNoByOfficeId',
                {'office_id': office_id}, 'json',
                function (response_data) {
                    $.each(response_data, function (i) {
                        response = response_data[i];
                        var unit = response.office_unit;
                        var orgs = response.designations;

                        var str_unit_org = '';
                        str_unit_org += '<div>';
                        str_unit_org += '<span class="caption-subject theme-font-color bold uppercase">' + unit.unit_name_bng + '</span>';
                        str_unit_org += '<hr/>';
                        $.each(orgs, function (i) {
                            var org = orgs[i];
                            str_unit_org += '<div class="row" id="checked_unit_' + unit.id + '_org_' + org.id + '">';
                            str_unit_org += '<div class="col-md-5 form-group form-horizontal">';
                            str_unit_org += '<div class="icheck-list">';
                            str_unit_org += '<label>';

                            str_unit_org += '<input type="checkbox" onclick="OfficeSeal.getOfficeSealByOfficeId();" class="org_checkbox" data-office-unit-id="' + unit.id + '" data-office-unit-organogram-id="' + org.id + '"  data-unit-name="' + unit.unit_name_bng + '" data-designation-name="' + org.designation_bng + '"> <span title="'+org.designation_description + '">' + org.designation_bng + "</span>";

                            str_unit_org += ' </label>';
                            str_unit_org += '</div>';
                            str_unit_org += '</div>';
                            str_unit_org += '</div>';
                        });

                        str_unit_org += '</div><br/>';
                        $("#office_unit_tree_panel").append(str_unit_org);
                        $(".selection_unit").hide();
                    });

                    var seal_designations = [];
                    $('input[name=office-employee]').each(function () {
                        seal_designations.push($(this).data('office-unit-organogram-id'));
                    });
                    $('.org_checkbox').each(function () {
                        var org_id = $(this).data('office-unit-organogram-id');
                        if ($.inArray(org_id, seal_designations) > -1) {
                            $(this).attr('checked', 'checked');
                        }
                    });

                }
            );
        },
        getOfficeSealByOfficeId: function () {
            $("#office_unit_seal_panel").html("");
            var str_seal_designation = '';

            str_seal_designation += '<div class="row">';
            str_seal_designation += '<table class="table table-bordered">';
            str_seal_designation += '<tbody>';

            $('#office_unit_tree_panel input:checked').each(function () {
                str_seal_designation += '<tr>';
                str_seal_designation += '<td>';
                str_seal_designation += $(this).attr('data-designation-name') + ', ' + $(this).attr('data-unit-name');
                str_seal_designation += '</td>';
                str_seal_designation += '<td>';
                str_seal_designation += '<input type="checkbox" name="office-employee" data-office-unit-id="' + $(this).attr('data-office-unit-id') + '" data-office-unit-organogram-id="' + $(this).attr('data-office-unit-organogram-id') + '"  checked="checked" data-designation-name="' + $(this).attr('data-designation-name') + '" data-unit-name="' + $(this).attr('data-unit-name') + '">';
                str_seal_designation += '</td>';
                str_seal_designation += '</tr>';
            });

            str_seal_designation += '</tbody>';
            str_seal_designation += '</table>';
            str_seal_designation += '</div>';

            $("#office_unit_seal_panel").append(str_seal_designation);
            $("#save-seal").css("display", "block");

        },
        checkIfSealExists: function () {
            var office_id = $("#office-unit-id option:selected").attr('office-id');
            var office_unit_id = $("#office-unit-id").val();
            $("#office_unit_seal_panel").html("");
            var data = {
                'office_id': office_id,
                'office_unit_id': office_unit_id
            };

            PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + 'officeManagement/loadOfficeSeal',
                data, 'html',
                function (response) {
                    OfficeSeal.getUnitOrganogramByOfficeId(office_id);
                    $("#office_unit_seal_panel").html(response);
                    $(document).find('[title]').tooltip({'placement':'bottom'});
                });
        },

        submitDesignationToSeal: function () {
            //alert('ddd');
            var office_id = $("#office-unit-id option:selected").attr('office-id');
            var seal_owner_unit_id = $("#office-unit-id").val();
            var designation_bng = [];
            var unit_bng = [];
            var office_unit_organogram_id = [];
            var office_unit_id = [];

            var i = 0;
            $('input[name=office-employee]').each(function () {
                designation_bng[i] = $(this).attr("data-designation-name");
                unit_bng[i] = $(this).attr("data-unit-name");
                office_unit_organogram_id[i] = $(this).attr("data-office-unit-organogram-id");
                office_unit_id[i] = $(this).attr("data-office-unit-id");
                i++;
            });

            var data = {
                'office_id': office_id,
                'seal_owner_unit_id': seal_owner_unit_id,
                'office_unit_id': office_unit_id,
                'designation_bng': designation_bng,
                'unit_bng': unit_bng,
                'office_unit_organogram_id': office_unit_organogram_id
            };

            PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + 'employeeRecords/submitDesignationInSeal',
                data, 'json',
                function (response) {
                    if (response == 1) {
                        toastr.options = {
                            "closeButton": true,
                            "debug": false,
                            "positionClass": "toast-bottom-right",
                        };
                        $(document).find('[title]').tooltip({'placement':'bottom'});
                        toastr.success("Seal has been prepared successfully", "Success!");
                    }
                    else {
                        toastr.options = {
                            "closeButton": true,
                            "debug": false,
                            "positionClass": "toast-bottom-right",
                        };
                        toastr.error("Failed to prepare seal", "Error!");
                    }
                });
        }

    };

    $(function () {
        $("#officeUnitTransfer").bind('click', function () {
            OfficeSeal.transferOrganogram();
        });
        $("#save-seal").bind('click', function () {
            OfficeSeal.submitDesignationToSeal();
        });
        $("#office-unit-id").bind('change', function () {
            OfficeSeal.checkIfSealExists();
        });
        $("#office-unit-id").trigger('change');
        $(".selection_unit").hide();
    });
</script>