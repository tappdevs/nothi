<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-user"></i><?php echo __('Employeer') ?> <?php echo __('Information') ?>
        </div>
        <div class="pull-right" style="padding:5px 0 0px 0"><a
                    href="<?= $this->request->webroot ?>officeEmployees/index" class="btn btn-sm btn-default"><i
                        class="fa fa-arrow-left"></i> অফিস কর্মকর্তার তালিকা</a></div>

    </div>
    <div class="portlet-body form">
        <?php echo $this->Form->create($entity, array('id' => 'EmployeeRecordForm')); ?>
        <?php echo $this->Form->hidden('id'); ?>
        <?php echo $this->Form->hidden('check_val', ['id' => 'check_val', 'type' => 'text', 'value' => 0]); ?>
        <div class="form-body">
            <?php echo $this->element('EmployeeRecords/personal_info'); ?>
            <?php echo $this->element('EmployeeRecords/professional_info'); ?>


            <div class="row">
                <div class="col-md-12">
                    <div class="row">

                        <div class="col-md-4 form-group form-horizontal">
                            <label class="control-label"> দপ্তর/শাখা </label>
                            <?php
                            echo $this->Form->input('office_unit_id',
                                array('empty' => '--বাছাই করুন--', 'type' => 'select', 'label' => false,
                                    'class' => 'form-control input-sm'));
                            ?>
                        </div>
                        <div class="col-md-4 form-group form-horizontal">
                            <label class="control-label"> পদ </label>
                            <?php echo $this->Form->input('office_unit_organogram_id',
                                array('empty' => '--বাছাই করুন--', 'type' => 'select', 'label' => false, 'class' => 'form-control input-sm'));
                            ?>

                        </div>
                        <div class="col-md-4 form-group form-horizontal">
                            <label class="control-label"> দায়িত্বের ধরণ </label>
                            <?php echo $this->Form->input('incharge_label',
                                array('empty' => '--বাছাই করুন--', 'type' => 'select', 'label' => false, 'class' => 'form-control input-sm','options' => $officeInchargeTypes));
                            ?>

                        </div>

                    </div>
                </div>
            </div>

            <?php echo $this->element('EmployeeRecords/login_info'); ?>
        </div>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-4 col-md-9">
                    <button type="button" class="btn btn-circle blue submitButtonClass" ><?php echo __("Submit"); ?></button>
                    <button type="reset" class="btn btn-circle default"><?php echo __("Reset"); ?></button>
                </div>
            </div>
        </div>
        <?php echo $this->Form->end(); ?>

        <div id="DoubleEntry" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"> কর্মকর্তার তথ্য নিশ্চিতকরণ </h4>
                    </div>
                    <div class="modal-body">
                        <div class="" id="dbl_entry">

                        </div>
                        <div class="well font-lg">
                            একই পরিচিতি নাম্বার দিয়ে একজন কর্মকর্তা অন্তর্ভুক্ত করা আছে। যদি একই পরিচিত নাম্বার দিয়ে
                            নতুন কর্মকর্তা সংরক্ষণ করতে চান তবে <b>"সংরক্ষণ "</b> বাটনে ক্লিক করুন। না হয় <b>"বাতিল
                                করুন"</b> বাটনে ক্লিক করে পুনরায় কার্যক্রমে ফিরে যান।
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn bg-green" onclick="saveData()"><?= __('Submit') ?></button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal"><?= __('Close') ?></button>
                    </div>
                </div>

            </div>
        </div>

    </div>
</div>
<script type="text/javascript">
    $(function () {
        OfficeSetup.init();
        $('.date-picker').datepicker({
            rtl: Metronic.isRTL(),
            orientation: "left",
            autoclose: true,
            format: "yyyy-mm-dd"
        });
    });
</script>
<script src="<?php echo CDN_PATH; ?>assets/admin/layout4/scripts/projapoti_ajax.js?v=<?= js_css_version ?>"
        type="text/javascript"></script>
<script>

    (function ($) {

        $("#office-unit-id").bind('change', function () {
            OfficeOrgSelectionCell.loadOfficeUnitDesignations();
        });


        PROJAPOTI.ajaxSubmitDataCallback('<?php echo $this->request->webroot ?>' + 'officeManagement/loadOfficeUnits',
            {'office_id': <?php echo $office_id ?>}, 'html',
            function (response) {
                $("#office-unit-id").html(response);
//                                PROJAPOTI.projapoti_dropdown_map("#office-unit-id", response, "--বাছাই করুন--");
            });

    }(jQuery));

</script>
<script type="text/javascript">
    var OfficeOrgSelectionCell = {
        loadOfficeUnits: function (prefix) {
            PROJAPOTI.ajaxSubmitDataCallback('<?php echo $this->request->webroot ?>' + 'officeManagement/loadOfficeUnits',
                {'office_id':<?php echo $office_id ?>}, 'html',
                function (response) {
                    $("#office-unit-id").html(response);
//                                    PROJAPOTI.projapoti_dropdown_map("#office-unit-id", response, "--বাছাই করুন--");
                });
        },
        loadOfficeUnitDesignations: function () {
            PROJAPOTI.ajaxSubmitDataCallback('<?php echo $this->request->webroot ?>' + 'officeManagement/loadUnUsedOfficeUnitOrganograms',
                {'office_unit_id': $("#office-unit-id").val()}, 'json',
                function (response) {
                    PROJAPOTI.projapoti_dropdown_map("#office-unit-organogram-id", response, "--বাছাই করুন--");
                });

        }
    };

    function check_10_digit_nid() {
        if ($("#nid").val().length == 10) {
            bootbox.dialog({
                message: "স্মার্ট জাতীয় পরিচয় পত্র দিয়ে ব্যাবহারকারীর অ্যাকাউন্ট করলে তা সচল হবে না। এ জন্য অবশ্যই অফিস এডমিন অথবা সাপোর্ট টিমের সাথে যোগাযোগ করতে হবে। আপনি কি পরবর্তী ধাপে যেতে চান?",
                title: "সতর্কীকরণ",
                buttons: {
                    success: {
                        label: "হ্যাঁ",
                        className: "green",
                        callback: function () {
                            //$("#EmployeeRecordForm").submit();
                        }
                    },
                    danger: {
                        label: "না",
                        className: "red",
                        callback: function () {
                            $("#nid").val("");
                            $("#nid").focus();
                            //return false;
                        }
                    }
                }
            });
        }
    }

    $(document).ready(function () {
        $("#check_val").val(0);
    });

    function saveData() {
        $("#check_val").val(1);
        $("#EmployeeRecordForm").submit();
    }

    $('.submitButtonClass').click(function(){
        if ($("#check_val").val() == 0) {
            var cdr_id = $("input[name='is_cadre']:checked").val();
            if (cdr_id != 1) {
                $("#check_val").val(1);
                $("#EmployeeRecordForm").submit();
            }else {
                if(isEmpty($("#identity-no").val()) && isEmpty($("#no-service-id").is(":checked"))){
                    //#2040
                    $("#check_val").val(1);
                    $("#EmployeeRecordForm").submit();
                    return;
                }
                PROJAPOTI.ajaxSubmitDataCallback('<?php echo $this->request->webroot ?>' + 'EmployeeRecords/checkDoubleEntry',
                    {'cadre': cdr_id, 'identity': $("#identity-no").val()}, 'json',
                    function (res) {
                        if(!isEmpty(res.status) && res.status=='success'){
                            var response = !isEmpty(res.data)?res.data:'';
                            if (isEmpty(response)) {
                                $("#check_val").val(1);
                                $("#EmployeeRecordForm").submit();
                            } else {
                                $("#dbl_entry").html("<div class='row'><div class='col-md-3 col-sm-3 bold font-lg'>ইউজার নেমঃ</div> <div class='col-md-9 col-sm-9 font-lg'>" + response.username + "</div> </div><div class='row'><div class='col-md-3 col-sm-3 bold font-lg'>পরিচিত নাম্বারঃ</div> <div class='col-md-9 col-sm-9 font-lg'>" + response.identity_no + "</div> </div> <div class='row'><div class='col-md-3 col-sm-3 bold font-lg'> নামঃ</div> <div class='col-md-9 col-sm-9 font-lg'>" + response.name_bng + "</div> </div> <div class='row'><div class='col-md-3 col-sm-3 bold font-lg'> পিতার নামঃ </div><div class='ccol-md-9 col-sm-9 font-lg'>" + response.father_name_bng + "</div> </div> <div class='row'><div class='col-md-3 col-sm-3 bold font-lg'> মাতার নামঃ </div><div class='col-md-9 col-sm-9 font-lg'>" + response.mother_name_bng + "</div> </div> <div class='row'><div class='col-md-3 col-sm-3 bold font-lg'> জন্ম তারিখঃ </div><div class=col-md-9 col-sm-9 font-lg'>" + ((response.date_of_birth == null || response.date_of_birth == '') ? ' ' : response.date_of_birth) + "</div></div> ");
                                $("#DoubleEntry").modal('toggle');
                            }
                        }else{
                            if(!isEmpty(res.message)){
                                toastr.error(res.message);
                            }else{
                                toastr.error('টেকনিক্যাল ত্রুটি হয়েছে।');
                            }
                        }

                    });
            }
        }
    });
</script>