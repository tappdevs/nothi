<?php
    if(empty($response) || $response['status'] == 'error'){
        echo '<div class="alert alert-danger" role="alert">
    <strong>দুঃখিতঃ ত্রুটি সংগঠিত হয়েছে।</strong> কারণঃ '.$response['msg'].'</div>';
        die;
    }else{
        $employee_record = $response['data'];
        $api_key = $response['api_key'];
    }
    $default_choose = 0;
    $options_of_signature_type = [0 => 'ইলেক্ট্রনিক'];
    if($employee_record['soft_signature']  == 1){
        $options_of_signature_type[1] = 'ডিজিটাল সফট টোকেন';
        if($employee_record['default_sign'] == 1){
            $default_choose = 1;
        }
    }
    if($employee_record['hard_signature']  == 1){
        $options_of_signature_type[2] = 'ডিজিটাল হার্ড  টোকেন';
        if($employee_record['default_sign'] == 2){
            $default_choose = 2;
        }
    }

?>

<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">স্বাক্ষর  সেটিংস </div>
    </div>
    <div class="portlet-body">
        <div class="row text-center font-lg">
            <div class="form-check form-check-inline">
                <label class="form-check-label  bold col-sm-3 col-md-3" for="inlineCheckbox0">স্বাক্ষর ধরনঃ</label>

                <input class="form-check-input" name="rcrd"  id="rcrd_id" type="hidden" value="<?= $employee_record['id']?>" >
                <input class="form-check-input" name="nid"  id="nid" type="hidden" value="<?= $employee_record['nid']?>" >
                <input class="form-check-input" name="api_key"  id="api_key" type="hidden" value="<?= $api_key?>" >
                <div class="col-sm-9 col-md-9">
                    <?=
                    $this->Form->input('default_sign',array('label' => false,'class' => 'form form-control default_sign','options' => $options_of_signature_type,'default' => $default_choose,'id' => 'default_sign_options'));
                    ?>
                </div>

            </div>
        </div>
        <hr>
        <div id="signatureSetting" class="row" hidden>
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">ডিজিটাল সিগনেচার সংযুক্তিকরণ </div>
                </div>
                <div class="portlet-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="control-label col-sm-3 col-md-3"> স্বাক্ষরের মাধ্যমঃ </label>
                            <div class="col-sm-9 col-md-9">
                                <select class="form form-control" name="ProofType"  id="ProofType" >
                                    <option value="0"> --বাছাই করুন--</option>
                                    <option value="1">জাতীয় পরিচয়পত্র </option>
                                    <option value="2">পাসপোর্ট </option>
                                    <option value="3">আয়কর সনাক্তকরণ </option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3 col-md-3"> নং </label>
                            <div class="col-sm-9 col-md-9">
                                <input class="form-check-input" name="ProofNo"  id="ProofNo" type="text" value="" >
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-3 col-md-3">
                            </div>
                            <div class="col-sm-9 col-md-9">
                                <button type="button" class="btn btn-primary btn-sm" onclick="digitalSignature.getUserSignature()"><i class="efile-approval2"></i> ডিজিটাল সিগনেচার তথ্য লোড করুন </button>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="" style="overflow: auto">
                        <table class="table table-bordered table-bordered" hidden id="showSignOptions">
                            <thead>
                                <tr class="header">
                                    <td colspan="4"> ডিজিটাল সিগনেচার সম্পর্কিত তথ্য (নিম্নোক্ত তথ্য হতে ডিফল্ট সিগনেচার নির্বাচন করুন)
                                    </td>
                                </tr>
                                <tr class="header">
                                    <td> কর্তৃপক্ষ </td>
                                    <td> সিরিয়াল নাম্বার  </td>
                                    <td> ধরন </td>
                                    <td> কার্যক্রম</td>
                                </tr>
                            </thead>
                            <tbody id="availableSignatureBody">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo $this->request->webroot; ?>projapoti-nothi/js/digital_sign.js?v=<?=js_css_version?>" type="text/javascript"></script>
<script>
    var api = 1;
</script>