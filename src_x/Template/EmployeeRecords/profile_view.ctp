<script>
    $('body').addClass('page-sidebar-closed');
    $('.page-sidebar-menu').addClass('page-sidebar-menu-closed');
</script>
<div class="portlet light">
    <div class="portlet-title">
        <div class="caption showContent">প্রোফাইল তথ্য 
            <a class="btn btn-default   btn-sm" data-toggle="tooltip" title="পরিবর্তন করুন" href="<?php echo $this->Url->build(['controller'=>'EmployeeRecords','action'=>'profileEdit']) ?>" ><i class="fa fa-edit" ></i></a>
        </div>
        <div class="actions">
            <a class="btn btn-sm green" href="<?php echo $this->Url->build(['controller' => 'Dashboard', 'action' => 'dashboard']) ?>"><i class="fa fa-home"></i> হোম </a>
        </div>
    </div>
    <div class="portlet-body">
        <div class="panel-body">
            <table class="table table-bordered table-responsive">
                <tr>
                    <th class="control-label col-md-2 text-right">নাম (বাংলা)</th>
                    <td class="control-label col-md-4"><?php echo h($employee_record->name_bng) ?></td>
                    <th class="control-label col-md-2 text-right">নাম (ইংরেজি)</th>
                    <td class="control-label col-md-4"><?php echo h($employee_record->name_eng) ?></td>
                </tr>
                <tr>
                    <th class="control-label col-md-2 text-right">পিতার নাম (বাংলা)</th>
                    <td class="control-label col-md-4"><?php echo h($employee_record->father_name_bng) ?></td>
                    <th class="control-label col-md-2 text-right">পিতার নাম (ইংরেজি)</th>
                    <td class="control-label col-md-4"><?php echo h($employee_record->father_name_eng) ?></td>
                </tr>
                <tr>
                    <th class="control-label col-md-2 text-right">মাতার নাম (বাংলা)</th>
                    <td class="control-label col-md-4"><?php echo h($employee_record->mother_name_bng) ?></td>
                    <th class="control-label col-md-2 text-right">মাতার নাম (ইংরেজি)</th>
                    <td class="control-label col-md-4"><?php echo h($employee_record->mother_name_eng) ?></td>
                </tr>
                <tr>
                    <th class="control-label col-md-2 text-right">জন্ম তারিখ</th>
                    <td class="control-label col-md-4"><?php 
                    if(!empty($employee_record->date_of_birth)){
                         $time = $employee_record->date_of_birth; 
                            echo $time->format("Y-m-d") ;
                    }
                        ?></td>
                    <th class="control-label col-md-2 text-right">পরিচয়পত্র নম্বর</th>
                    <td class="control-label col-md-4"><?php echo h($employee_record->nid) ?></td>
                </tr>
                <tr>
                    <th class="control-label col-md-2 text-right"> লিঙ্গ </th>
                    <td class="control-label col-md-4"><?php echo $employee_record->gender==1?'পুরুষ':($employee_record->gender==2?'নারী':'') ?></td>
                    <th class="control-label col-md-2 text-right"> ধর্ম </th>
                    <td class="control-label col-md-4"><?php echo h($employee_record->religion) ?></td>
                </tr>
                <tr>
                    <th class="control-label col-md-2 text-right">রক্তের গ্রুপ</th>
                    <td class="control-label col-md-4"><?php echo h($employee_record->blood_group) ?></td>
                    <th class="control-label col-md-2 text-right">বৈবাহিক অবস্থা</th>
                    <td class="control-label col-md-4"><?php echo h($employee_record->marital_status) ?></td>
                </tr>
                <tr>
                    <th class="control-label col-md-2 text-right">ব্যক্তিগত ই-মেইল</th>
                    <td class="control-label col-md-4"><?php echo h($employee_record->personal_email) ?></td>
                    <th class="control-label col-md-2 text-right">ব্যক্তিগত মোবাইল নম্বর</th>
                    <td class="control-label col-md-4"><?php echo h($employee_record->personal_mobile) ?></td>
                </tr>
	            <tr>
		            <th class="control-label col-md-2 text-right">বিকল্প মোবাইল নম্বর</th>
		            <td class="control-label col-md-4"><?php echo h($employee_record->alternative_mobile) ?></td>
		            <th class="control-label col-md-2 text-right">লগইন নেম</th>
		            <td class="control-label col-md-4"><?php echo h($employee_record->user_alias) ?></td>
	            </tr>
            </table>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
}); 
</script>