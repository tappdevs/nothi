<link rel="stylesheet" type="text/css"
      href="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css"
      href="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css"
      href="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>

<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">Bulk Price Data</div>
    </div>

    <div class="portlet-body">
        <?php
        if (!empty($row)) {
            ?>
            <div class="table-toolbar">
                <div class="row">
                    <div class="col-md-6">
                        <div class="btn-group">
                            <button id="importedData_new" class="btn green">
                                Add New <i class="fs1 a2i_gn_add1"></i>
                            </button>
                        </div>
                    </div>

                </div>
            </div>

            <table class="table table-striped table-bordered table-hover" id="importedData">
                <thead>
                    <tr>
                        <th>
                            নাম (বাংলা)
                        </th>
                        <th>
                            নাম (ইংরেজি)
                        </th>
                        <th>
                            জন্ম তারিখ (2015-11-20)
                        </th>
                        <th>
                            জাতীয় পরিচয়পত্র নম্বর
                        </th>
                        <th>
                            ক্যাডার? (1=হ্যাঁ, 0=না)
                        </th>
                        <th>
                            লগইন আইডি
                        </th>
                        <th>
                            পাসওয়ার্ড
                        </th>
                        <th>
                            কার্যক্রম
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (!empty($row[1])) {
                        for ($i = 1; $i < sizeof($row); $i++) {
                            echo "<tr>";
                            foreach ($row[$i] as $key => $value) {
                                echo "<td >" . $value . "</td>";
                            }
                            echo '<td><a class="edit" href="javascript:;">Edit </a> &nbsp; <a class="delete" href="javascript:;">Delete </a></td>';
                            echo "</tr>";
                        }
                    }
                    ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="<?php echo count($row[0]) + 1; ?>">
                            <input type="button" class="btn btn-primary " id="importedData_save" value="Save">
                        </td>
                    </tr>
                </tfoot>
            </table>
            <?php
        }
        ?>
    </div>
</div>

<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>

<script>
    var TableEditable = function () {

        var handleTable = function () {

            function restoreRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);

                for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                    oTable.fnUpdate(aData[i], nRow, i, false);
                }

                oTable.fnDraw();
            }

            function editRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
                var i = 0;
                for (i = 0; i < 7; i++) {
                    jqTds[i].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[i] + '">';
                }
                jqTds[7].innerHTML = '<a class="edit" href="">Save</a>&nbsp;<a class="cancel" href="">Cancel</a>';
            }

            function saveRow(oTable, nRow) {
                var jqInputs = $('input', nRow);
                var i = 0;
                for (i = 0; i < 7; i++) {
                    oTable.fnUpdate(jqInputs[i].value, nRow, i, false);
                }
                oTable.fnUpdate('<a class="edit" href="">Edit</a>&nbsp;<a class="delete" href="">Delete</a>', nRow, 7, false);
                oTable.fnDraw();
            }

            function cancelEditRow(oTable, nRow) {
                var jqInputs = $('input', nRow);
                var i = 0;
                for (i = 0; i < 7; i++) {
                    oTable.fnUpdate(jqInputs[i].value, nRow, i, false);
                }
                oTable.fnUpdate('<a class="edit" href="">Edit</a>', nRow, 7, false);
                oTable.fnDraw();
            }

            var table = $('#importedData');

            var oTable = table.dataTable({
                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js). 
                // So when dropdowns used the scrollable div should be removed. 
                //"dom": "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

                "lengthMenu": [
                    [5, 15, 20, -1],
                    [5, 15, 20, "All"] // change per page values here
                ],
                // Or you can use remote translation file
                //"language": {
                //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
                //},

                // set the initial value
                "pageLength": -1,
                "language": {
                    "lengthMenu": " _MENU_ records"
                },
                "columnDefs": [{// set default column settings
                        'orderable': false,
                        'targets': [0]
                    }, {
                        "searchable": true,
                        "targets": [0]
                    }],
                "order": [
                    [0, "asc"]
                ] // set first column as a default sort by asc
            });

            var tableWrapper = $("#sample_editable_1_wrapper");

            tableWrapper.find(".dataTables_length select").select2({
                showSearchInput: true //hide search box with special css class
            }); // initialize select2 dropdown

            var nEditing = null;
            var nNew = false;

            $('#importedData_new').click(function (e) {
                e.preventDefault();

                if (nNew && nEditing) {
                    if (confirm("Previose row not saved. Do you want to save it ?")) {
                        saveRow(oTable, nEditing); // save
                        $(nEditing).find("td:first").html("Untitled");
                        nEditing = null;
                        nNew = false;

                    } else {
                        oTable.fnDeleteRow(nEditing); // cancel
                        nEditing = null;
                        nNew = false;

                        return;
                    }
                }

                var aiNew = oTable.fnAddData(['', '', '', '', '', '','', '', '', '', '', '']);
                var nRow = oTable.fnGetNodes(aiNew[0]);
                editRow(oTable, nRow);
                nEditing = nRow;
                nNew = true;
            });

            table.on('click', '.delete', function (e) {
                e.preventDefault();

                if (confirm("Are you sure to delete this row ?") == false) {
                    return;
                }

                var nRow = $(this).parents('tr')[0];
                oTable.fnDeleteRow(nRow);
//                            alert("Deleted! Do not forget to do some ajax to sync with backend :)");
            });

            table.on('click', '.cancel', function (e) {
                e.preventDefault();
                if (nNew) {
                    oTable.fnDeleteRow(nEditing);
                    nEditing = null;
                    nNew = false;
                } else {
                    restoreRow(oTable, nEditing);
                    nEditing = null;
                }
            });

            table.on('click', '.edit', function (e) {
                e.preventDefault();

                /* Get the row as a parent of the link that was clicked on */
                var nRow = $(this).parents('tr')[0];

                if (nEditing !== null && nEditing != nRow) {
                    /* Currently editing - but not this row - restore the old before continuing to edit mode */
                    restoreRow(oTable, nEditing);
                    editRow(oTable, nRow);
                    nEditing = nRow;
                } else if (nEditing == nRow && this.innerHTML == "Save") {
                    /* Editing this row and want to save it */
                    saveRow(oTable, nEditing);
                    nEditing = null;
//                                alert("Updated! Do not forget to do some ajax to sync with backend :)");
                } else {
                    /* No edit in progress - let's start one */
                    editRow(oTable, nRow);
                    nEditing = nRow;
                }
            });
            
            
            $('#importedData_save').on('click', function(){
                            
                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "positionClass": "toast-bottom-right",
                    "onclick": null,
                    "showDuration": "1000",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                }

                if ( nEditing) {
                    alert("Please save data in table first!");
                    return;
                }
                $('#importedData_save').attr('disabled','disabled');

                var tr = $('#importedData').find('tbody').find('tr');
                var trlen = $('#importedData').find('tbody').find('tr').length;
                var thead = $('#importedData').find('thead').find('th');
                var loop = 0;
                $.each(tr, function(i,td){

                    var thistd = $(td).find('td:not(:last)');
                    var data = "";
                    var prevHtml = $(td).find('td:last').html();
                    var headerarray = [];
                    
                    headerarray[0] = 'name_bng';
                    headerarray[1] = 'name_eng';
                    headerarray[2] = 'date_of_birth';
                    headerarray[3] = 'nid';
                    headerarray[4] = 'is_cadre';
                    headerarray[5] = 'username';
                    headerarray[6] = 'password';
                                       

                    $.each(thistd, function(j, tddata){
                        data += "&" + headerarray[j] + "=" + $(this).text() ;
                    });

//                    $(td).find('td:last').html('<i class="fa fa-spin fa-spinner"></i>');
                    $.ajax({
                        url: js_wb_root + 'EmployeeRecords/addBulk',
                        data: data,
                        method: 'POST',
                        dataType: 'JSON',
                        success: function(response){
                            loop++;
                            if(loop == trlen){$('#importedData_save').removeAttr('disabled');}
                            if(response.status == 'success'){
                                $(td).removeClass('danger').addClass('success');
                                $(td).find('td').removeClass('danger')
//                                $(td).find('td:last').html('<i class="fa fa-check"></i>');

                            }else{
                                $(td).addClass('danger');
                                $(td).find('td').addClass('danger')
                                toastr.error(response.msg);
                                $(td).find('td:last').html(prevHtml);
                            }
                        },
                        error: function (status, xhrs){

                        }
                    })
                });
            });

        }

        return {
            //main function to initiate the module
            init: function () {
                handleTable();
            }

        };

    }();
    $(function () {
        TableEditable.init();
    });


</script>