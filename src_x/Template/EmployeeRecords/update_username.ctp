<div class="row">
    <div class="col-md-12">
        <div class="portlet box green">
            

            <div class="portlet-body">
                <?= $cell = $this->cell('OfficeSelection', ['entity' => '']) ?>
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo __('Office') ?> </label>

                        <div class="col-sm-6">
                            <?php
                            echo $this->Form->input('office_id', array(
                                'label' => false,
                                'class' => 'form-control',
                                'empty' => '----'
                            ));
                            ?>
                        </div>
                    </div>
                </div>
                <hr/>
                
                <table class="table table-striped table-bordered table-hover" id="sample_1">
                    <thead>
                    <tr>
                        <th class=""><?php echo __('Name') ?></th>
                        <th class=""><?php echo __('Current') ?> <?php echo __('Designations') ?> </th>
                        <th class=""><?php echo __('Unit') ?></th>
                        <th class=""><?php echo __('Login ID') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                   
                    </tbody>
                    <tfoot>
                        <tr ><td colspan="4" class="text-center"><input type="button" class="btn-update-username btn btn-success" value="<?php echo __("Update") ?>"/></td></tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    var EmployeeAssignment = {
        checked_node_ids: [],
        loadMinistryWiseLayers: function () {
            OfficeSetup.loadLayers($("#office-ministry-id").val(), function (response) {
                //
            });
        },
        loadMinistryAndLayerWiseOfficeOrigin: function () {
            OfficeSetup.loadOffices($("#office-ministry-id").val(), $("#office-layer-id").val(), function (response) {
                //
            });
        },
        loadOriginOffices: function () {
            OfficeSetup.loadOriginOffices($("#office-origin-id").val(), function (response) {

            });
        },
        loadEmployees: function(val,employeeid){
             
            PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + 'EmployeeRecords/emoloyeeByOffice',
            {'office_id': val,officer_id:employeeid}, 'json',
                function (response)
                {
                    $.each(response, function(i, data){
                        $('#sample_1').find('tbody').append("<tr><td>"+data.name_bng+"</td><td>"+data.designation+"</td><td>"+data.unit_name_bng+"</td><td>"+data.username+"</td></tr>");
                    })
                    
                    
                }
            );
        },
        updateEmployees: function(val){
            
            PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + 'EmployeeRecords/updatelogin',
            {'office_id': val}, 'json',
                function (response)
                {   
                    if(response>0){
                        EmployeeAssignment.loadEmployees(val);
                    }
                }
            );
        }
    };

    $(function () {
        $("#office-ministry-id").bind('change', function () {
            EmployeeAssignment.loadMinistryWiseLayers();
        });
        $("#office-layer-id").bind('change', function () {
            EmployeeAssignment.loadMinistryAndLayerWiseOfficeOrigin();
        });
        $("#office-origin-id").bind('change', function () {
            EmployeeAssignment.loadOriginOffices();
        });
        $("#office-id").bind('change', function () {
            $('#sample_1').find('tbody').html('');
            EmployeeAssignment.loadEmployees($(this).val(),0);
        });
        
        $(document).on('click','.btn-update-username', function(){
            $('#sample_1').find('tbody').html('');
            EmployeeAssignment.updateEmployees($("#office-id").val());
        })
    });
</script>