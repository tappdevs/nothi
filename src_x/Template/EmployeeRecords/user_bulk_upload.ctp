<!-- END PAGE BREADCRUMB -->
<div class="portlet box blue">
	<div class="portlet-title">
		<div class="caption">Add Bulk Employee</div>
	</div>
	<div class="portlet-body form">
		<!-- BEGIN FORM-->
		 <?= $this->Form->create('', ['action' => 'showBulkUpload','type'=>'post','class'=>'form-horizontal','type'=>'file']);?>
			<div class="form-body">
				<div class="form-group">
					<label class="col-md-3 control-label">Upload File </label>
					<div class="col-md-4">
						<div class="input-group">
							<span class="input-group-addon input-circle-left">
							<i class="fa fa-file"></i>
							</span>
							<?= $this->Form->file('user',['label'=>false,'class'=>'form-control input-circle-right','required'=>true]);?>
						</div>
					</div>
				</div>
			</div>
			<div class="form-actions">
				<div class="row">
					<div class="col-md-offset-3 col-md-9">
						<button type="submit" class="btn   blue">Submit</button>
					</div>
				</div>
			</div>
		<?= $this->Form->end();?>

	</div>

</div>

