<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption"><i class=""></i> Office Employee Details</div>
        <div class="tools">
            <a href="javascript:" class="collapse"></a>
            <a href="#portlet-config" data-toggle="modal" class="config"></a>
            <a href="javascript:" class="reload"></a>
            <a href="javascript:" class="remove"></a>
        </div>
    </div>
    <div class="portlet-body">
        <div class="panel-body">
            <table class="table table-bordered">
                <tr>
                    <td class="text-right">Full Name in Bangla</td>
                    <td><?= h($employee_record->name_bng) ?></td>
                </tr>
                <tr>
                    <td class="text-right">Full Name in English</td>
                    <td><?= h($employee_record->name_eng) ?></td>
                </tr>
                <!--tr><td class="text-right">System Code</td><td><?= h($employee_record->sys_code) ?></td></tr-->
                <tr>
                    <td class="text-right">Father Name in English</td>
                    <td><?= h($employee_record->father_name_eng) ?></td>
                </tr>
                <tr>
                    <td class="text-right">Father Name in Bangla</td>
                    <td><?= h($employee_record->father_name_bng) ?></td>
                </tr>
                <tr>
                    <td class="text-right">Mother Name in English</td>
                    <td><?= h($employee_record->mother_name_eng) ?></td>
                </tr>
                <tr>
                    <td class="text-right">Mother Name in Bangla</td>
                    <td><?= h($employee_record->mother_name_bng) ?></td>
                </tr>
                <!--tr><td class="text-right">Permanent Village</td><td><?= h($employee_record->parmanent_village) ?></td></tr>
                <tr><td class="text-right">Permanent Post Office</td><td><?= h($employee_record->parmanent_postoffice) ?></td></tr>
                <tr><td class="text-right">Permanent Thana</td><td><?= h($employee_record->parmanent_thana) ?></td></tr>
                <tr><td class="text-right">Permanent District</td><td><?= h($employee_record->parmanent_district) ?></td></tr>
                <tr><td class="text-right">Present Village</td><td><?= h($employee_record->present_village) ?></td></tr>
                <tr><td class="text-right">Present Post Office</td><td><?= h($employee_record->present_postoffice) ?></td></tr>
                <tr><td class="text-right">Present Thana</td><td><?= h($employee_record->present_thana) ?></td></tr>
                <tr><td class="text-right">Present District</td><td><?= h($employee_record->present_district) ?></td></tr-->
                <tr>
                    <td class="text-right">Birth Registration Number</td>
                    <td><?= h($employee_record->bcn) ?></td>
                </tr>
                <tr>
                    <td class="text-right">Birth Date</td>
                    <td><?= h($employee_record->date_of_birth) ?></td>
                </tr>
                <tr>
                    <td class="text-right">Blood Group</td>
                    <td><?= h($employee_record->blood_group) ?></td>
                </tr>
                <!--tr><td class="text-right">Nationality</td><td><?= h($employee_record->nationality) ?></td></tr-->
                <tr>
                    <td class="text-right">National ID</td>
                    <td><?= h($employee_record->nid) ?></td>
                </tr>
                <tr>
                    <td class="text-right">Image File Name</td>
                    <td><?= h($employee_record->photo) ?></td>
                </tr>
                <tr>
                    <td class="text-right">Signature File Name</td>
                    <td><?= h($employee_record->e_sign) ?></td>
                </tr>
                <!--tr><td class="text-right">Telephone Number</td><td><?= h($employee_record->personal_mobile) ?></td></tr-->
                <tr>
                    <td class="text-right">Mobile Number</td>
                    <td><?= h($employee_record->personal_mobile) ?></td>
                </tr>
                <tr>
                    <td class="text-right">Email</td>
                    <td><?= h($employee_record->personal_email) ?></td>
                </tr>
                <tr>
                    <td class="text-right">Gender</td>
                    <td><?= h($employee_record->gender) ?></td>
                </tr>
                <tr>
                    <td class="text-right">Marital Status</td>
                    <td><?= h($employee_record->marital_status) ?></td>
                </tr>
                <!--tr><td class="text-right">Spouse Name in English</td><td><?= h($employee_record->spouse_name_bng) ?></td></tr>
                <tr><td class="text-right">Spouse Name in Bangla</td><td><?= h($employee_record->spouse_name_eng) ?></td></tr-->
                <tr>
                    <td class="text-right">Religion</td>
                    <td><?= h($employee_record->religion) ?></td>
                </tr>
                <!--tr><td class="text-right">Max Educational Qualification</td><td><?= h($employee_record->max_educational_qualification) ?></td></tr-->
                <tr>
                    <td class="text-right">Cadre or Not</td>
                    <td><?= h($employee_record->iscadre) ?></td>
                </tr>
                <tr>
                    <td class="text-right">Batch</td>
                    <td><?= h($employee_record->employee_batch_id) ?></td>
                </tr>
                <tr>
                    <td class="text-right">Grade</td>
                    <td><?= h($employee_record->employee_cadre_id) ?></td>
                </tr>
                <tr>
                    <td class="text-right">Rank</td>
                    <td><?= h($employee_record->service_rank_id) ?></td>
                </tr>
                <tr>
                    <td class="text-right">Service Number</td>
                    <td><?= h($employee_record->identity_no) ?></td>
                </tr>
                <tr>
                    <td class="text-right">Service Joining Date</td>
                    <td><?= h($employee_record->joining_date) ?></td>
                </tr>
                <!--tr><td class="text-right">Current Posting Date</td><td><?= h($employee_record->present_posting_date) ?></td></tr-->
                <!--tr><td class="text-right">Organization ID</td><td><?= h($employee_record->office_organization_id) ?></td></tr-->
                <!--tr><td class="text-right">Status</td><td><?= h($employee_record->status) ?></td></tr-->
                <!--tr><td class="text-right">Deleted or Not</td><td><?= h($employee_record->isdeleted) ?></td></tr-->
                <tr>
                    <td><?= $this->Html->link('Edit This Employee', ['action' => 'edit', $employee_record->id], array('class' => 'btn btn-primary pull-right')) ?></td>
                    <td><?= $this->Html->link('Back to Employee List', ['action' => 'index'], array('class' => 'btn btn-primary pull-left')) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>