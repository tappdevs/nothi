<script>
    $('body').addClass('page-sidebar-closed');
    $('.page-sidebar-menu').addClass('page-sidebar-menu-closed');
</script>
<div class="UIblock">
	<div class="portlet light">
		<div class="portlet-title">
			<div class="caption">ডাক ও নথি শেয়ার</div>
			<div class="actions">
				<a class="btn btn-sm green" href="<?php echo $this->Url->build(['controller' => 'Dashboard', 'action' => 'dashboard']) ?>"><i class="fa fa-home"></i> হোম </a>
			</div>
		</div>
		<div class="portlet-body">
			<div class="form-horizontal">
				<div class="form-group">
					<?php
					if(empty($existing_permissions)){
						?>
						<div class="row">
							<?php echo $this->Form->create('', ['method' => 'post']); ?>
							<div class="form-group">
								<label class="control-label col-md-2">পদবি নির্বাচন </label>
								<div class="col-md-4">
									<select class="form-control dropdown_with_photo" name="assigned_organogram_id" required="required">
										<option value="">-- পদবি নির্বাচন করুন --</option>
										<?php foreach ($OfficeUnitOrganograms as $officeUnitOrganogram): ?>
											<?php $designation_info = designationInfo($officeUnitOrganogram['id']) ?>
											<option value="<?=$officeUnitOrganogram['id']?>" data-imageUrl="<?=$designation_info['user_photo']?>"><?= $designation_info['officer_name'] ?>~<?= $designation_info['officer_designation_label'] ?>~<?= $designation_info['office_unit_name'] ?></option>
										<?php endforeach; ?>
									</select>
								</div>
								<label class="control-label col-md-1">অনুমতি </label>
								<div class="col-md-2">
									<select class="form-control select2" name="permission_for">
										<option value="0">সকল</option>
										<!--<option value="1">ডাক</option>
										<option value="2">নথি</option>-->
									</select>
								</div>
								<div class="col-md-2">
									<button type="submit" class="btn btn-success btn-md">শেয়ার অনুমতি প্রদান</button>
								</div>
							</div>
							<?php echo $this->Form->end(); ?>
						</div>
						<?php
					} else {
						?>
						<div class="row">
							<div class="col-md-12">
								<div class="portlet-body">
									<table class="table table-striped table-bordered table-hover" id="sample_1">
										<tr class="heading">
											<th class="text-center">ক্রম</th>
											<th class="text-center">পদবি</th>
											<th class="text-center">অনুমতি</th>
											<th class="text-center">শেয়ার অনুমতি প্রদানের তারিখ</th>
											<th class="text-center">কার্যক্রম</th>
										</tr>
										<tbody>
										<?php $i = 0; ?>

										<?php foreach ($existing_permissions as $row => $permissions): ?>
											<tr class="assigned_organogram_id"
											    data-assigned-designation-id="<?= !empty($permissions['assigned_organogram_id']) ? $permissions['assigned_organogram_id'] : 0 ?>">
												<?php
												$designation_info = designationInfo($permissions['assigned_organogram_id']);
												?>
												<td class="text-center"><?php echo $this->Number->format(++$i) ?></td>
												<td>
													<div style="float: left;"><img src="<?=$designation_info['user_photo']?>" style="width:70px;height:70px;border:solid 1px silver;border-radius:5px!important;margin-right: 5px;"/></div>
													<?= $designation_info['officer_name'] ?><br/>
													<?= $designation_info['officer_designation_label'] ?><br/>
													<?= $designation_info['office_unit_name'] ?></td>
												<td>
													<?php if ($permissions['permission_for'] == 0) : ?>
														সকল
													<?php elseif ($permissions['permission_for'] == 1) : ?>
														ডাক
													<?php elseif ($permissions['permission_for'] == 2) : ?>
														নথি
													<?php endif; ?>
												</td>
												<td><?= $permissions['assigned_date'] ?></td>
												<td>
													<?php echo $this->Form->create('', ['method' => 'post']); ?>
													<input type="hidden" name="method" value="delete"/>
													<input type="hidden" name="permission_id"
													       value="<?= $permissions['id'] ?>"/>
													<button type="submit" class="btn btn-sm btn-danger"><i
																class="fa fa-trash"></i> বাতিল
													</button>
													<?php echo $this->Form->end(); ?>
												</td>
											</tr>
											<script>
                                                $("select[name=assigned_organogram_id] option[value=<?=$permissions['assigned_organogram_id']?>]").prop('disabled', true);
											</script>
										<?php endforeach; ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<?php
					}
					?>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
    $(function() {
        $("select[name=assigned_organogram_id] option[value=<?=$current_user['office_unit_organogram_id']?>]").remove();
        $('.select2').select2();

        dropdownWithPhoto($("select[name=assigned_organogram_id]"));
    })
</script>