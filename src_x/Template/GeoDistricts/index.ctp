<div class="portlet light">
    <div class="portlet-title">
        <div class="caption"><i class="fs1 a2i_gn_details1"></i><?php echo __("District List"); ?></div>
    </div>
    <div class="portlet-body">
        <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                        <?= $this->Html->link(__('Add New District'), ['action' => 'add'], ['class' => 'btn btn-default']) ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="btn-group pull-right">
                        <button class="btn dropdown-toggle" data-toggle="dropdown"><?php echo __("Tools"); ?>  <i
                                class="glyphicon glyphicon-chevron-down"></i></button>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="#"> </a></li>
                            <li><a href="#"><?php echo __("Save as PDF"); ?> </a></li>
                            <li><a href="#">Export to Excel </a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th class="text-center"><?= $this->Paginator->sort('Division') ?></th>
                <th class="text-center"><?= $this->Paginator->sort('District Name In English') ?></th>
                <th class="text-center"><?= $this->Paginator->sort('District Name In Bangla') ?></th>
                <th class="text-center"><?= $this->Paginator->sort('BBS Code') ?></th>
                <th class="actions text-center"><?= __('Actions') ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($query as $geoDistrict): ?>
                <tr>
                    <td class="text-center"><?= h($geoDistrict->geo_division->division_name_bng) ?></td>
                    <td class="text-center"><?= h($geoDistrict->district_name_eng) ?></td>
                    <td class="text-center"><?= h($geoDistrict->district_name_bng) ?></td>
                    <td class="text-center"><?= h($geoDistrict->bbs_code) ?></td>
                    <td class="actions text-center">
                        <!-- <?= $this->Html->link(__('View'), ['action' => 'view', $geoDistrict->id], ['class' => 'btn btn-primary']) ?>
					  -->
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $geoDistrict->id], ['class' => 'btn btn-success']) ?>
                        <!-- <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $geoDistrict->id], ['class' => 'btn btn-danger'], ['confirm' => __('Are you sure you want to delete # {0}?', $geoDistrict->id)]) ?>
					   -->
                    </td>
                </tr>

            <?php endforeach; ?>
            </tbody>
        </table>

        <div class="actions text-center">
            <ul class="pagination pagination-sm">
                <?php
                echo $this->Paginator->last(__('শেষ', true),
                    array('class' => 'number-last'));
                echo $this->Paginator->next('<<',
                    array('tag' => 'li', 'escape' => false),
                    '<a href="#" class="btn btn-sm blue ">&raquo;</a>',
                    array('class' => 'next disabled', 'tag' => 'li', 'escape' => false));
                echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li',
                    'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a',
                    'reverse' => true));
                echo $this->Paginator->prev('>>',
                    array('tag' => 'li', 'escape' => false),
                    '<a href="#" class="btn btn-sm blue">&laquo;</a>',
                    array('class' => 'prev disabled btn btn-sm blue', 'tag' => 'li',
                    'escape' => false));
                echo $this->Paginator->first(__('প্রথম', true),
                    array('class' => 'number-first'));
                ?>
            </ul>

        </div>
    </div>
</div>
