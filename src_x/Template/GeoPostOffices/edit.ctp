<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption"><i class=""></i><?php echo __('Post Office') ?> <?php echo __('Edit') ?></div>
        <div class="tools">
            <a  href="<?=
            $this->Url->build(['controller' => 'GeoPostOffices',
                'action' => 'index'])
            ?>"><button class="btn   blue margin-bottom-5"  style="margin-top: -5px;"> পোষ্ট অফিস   তালিকা  </button></a>
        </div>
        <!--<div class="tools">
            <a href="javascript:" class="collapse"></a>
            <a href="#portlet-config" data-toggle="modal" class="config"></a>
            <a href="javascript:" class="reload"></a>
            <a href="javascript:" class="remove"></a>
        </div>-->
    </div>
    <div class="portlet-body form"><br><br>
        <?php echo $this->Form->create($geo_post_office); ?>
        <?php echo $this->element('GeoPostOffices/add'); ?>
        <?php echo $this->element('update'); ?>
        <?php echo $this->Form->end(); ?>
    </div>
</div>
