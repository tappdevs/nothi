<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption"><i class="fs1 a2i_gn_edit2"></i><?php echo __("Thana") ?> <?php echo __("Create") ?></div>
        <div class="tools">
            <a  href="<?=
            $this->Url->build(['controller' => 'GeoThanas',
                    'action' => 'index'])
            ?>"><button class="btn   blue margin-bottom-10"  style="margin-top: -5px;"> থানা  তালিকা  </button></a>
        </div>
        <!--<div class="tools">  
            <a href="javascript:" class="collapse"></a>
            <a href="#portlet-config" data-toggle="modal" class="config"></a>
            <a href="javascript:" class="reload"></a>
            <a href="javascript:" class="remove"></a>
        </div>-->
    </div>
    <div class="portlet-body form"><br><br>
        <?php echo $this->Form->create(); ?>
        <?php echo $this->element('GeoThanas/add_with_select'); ?>
        <?php echo $this->element('submit'); ?>
        <?php echo $this->Form->end(); ?>
    </div>
</div>