<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption"><i class=""></i> থানা সম্পাদনা</div>
        <div class="tools">
            <a  href="<?=
            $this->Url->build(['controller' => 'GeoThanas',
                'action' => 'index'])
    ?>"><button class="btn   blue margin-bottom-10" style="margin-top: -5px;"> থানা  তালিকা  </button></a>
        </div>
    </div>
    <div class="portlet-body form"><br><br>
        <?php echo $this->Form->create($geo_thana); ?>
        <?php echo $this->element('GeoThanas/add'); ?>
        <?php echo $this->element('update'); ?>
        <?php echo $this->Form->end(); ?>
    </div>
</div>