<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption"><i class=""></i><?php echo __('Union') ?> <?php echo __('Edit') ?>  </div>

    </div>
    <div class="portlet-body form"><br><br>
        <?php echo $this->Form->create($geo_union); ?>
        <?php echo $this->element('GeoUnions/add'); ?>
        <?php echo $this->element('update'); ?>
        <?php echo $this->Form->end(); ?>
    </div>
</div>
