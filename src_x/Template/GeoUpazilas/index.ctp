<div class="portlet light">
    <div class="portlet-title">
        <div class="caption"><i class="fs1 a2i_gn_details1"></i><?php echo __('Upazilar') ?> <?php echo __('List') ?></div>

    </div>
    <div class="portlet-body">
        <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                        <?= $this->Html->link(__('Add New'),
                            ['action' => 'add'], ['class' => 'btn btn-default']) ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="btn-group pull-right">
                        <button class="btn dropdown-toggle" data-toggle="dropdown"><?php echo __('Tools') ?> <i
                                class="glyphicon glyphicon-chevron-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="#"><?php echo __('Print') ?></a></li>
                            <li><a href="#"><?php echo __('Save as PDF') ?> </a></li>
                            <li><a href="#">Export to Excel </a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <table class="table table-bordered">
            <thead>
                <tr>
                    <th class="text-center"><?php echo __('Division') ?></th>
                    <th class="text-center"><?php echo __('District') ?></th>
                    <th class="text-center"><?php echo __('UBBS Code') ?></th>
                    <th class="text-center"><?php echo __('BBS Code') ?></th>
                    <th class="text-center"><?php echo __('Upazila Name in Bangla') ?></th>
                    <th class="text-center"><?php echo __('Upazila Name in English') ?></th>
                    <th class="text-center"><?php echo __('Status') ?></th>
                    <th class="text-center"><?php echo __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>

                <?php
                foreach ($query as $rows):
                    ?>
                    <tr>
                        <td class="text-center"><?php echo $rows['geo_division']['division_name_bng']; ?></td>
                        <td class="text-center"><?php echo $rows['geo_district']['district_name_bng']; ?></td>
                        <td class="text-center"><?php echo $rows['bbs_code']; ?></td>
                        <td class="text-center"><?php echo $rows['district_bbs_code']; ?></td>
                        <td class="text-center"><?php echo $rows['upazila_name_bng']; ?></td>
                        <td class="text-center"><?php echo $rows['upazila_name_eng']; ?></td>
                        <td class="text-center"><?php echo $rows['status']; ?></td>
                        <td class="text-center">
                            <!-- <?=
                            $this->Form->postLink(
                                'Delete', ['action' => 'delete', $rows->id],
                                ['class' => 'btn btn-danger'],
                                ['confirm' => 'Are you sure to delete this item?'])
                            ?>-->
    <?= $this->Html->link(__('Edit'),
        ['action' => 'edit', $rows->id], ['class' => 'btn btn-primary']) ?>
                            <!-- <?= $this->Html->link('View',
        ['action' => 'view', $rows->id], ['class' => 'btn btn-success']) ?>-->

                        </td>
    <?php
endforeach;
?>
                </tr>
            </tbody>
        </table>
        <div class="actions text-center">
            <ul class="pagination pagination-sm">
                <?php
                echo $this->Paginator->last(__('শেষ', true),
                    array('class' => 'number-last'));
                echo $this->Paginator->next('<<',
                    array('tag' => 'li', 'escape' => false),
                    '<a href="#" class="btn btn-sm blue ">&raquo;</a>',
                    array('class' => 'next disabled', 'tag' => 'li', 'escape' => false));
                echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li',
                    'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a',
                    'reverse' => true));
                echo $this->Paginator->prev('>>',
                    array('tag' => 'li', 'escape' => false),
                    '<a href="#" class="btn btn-sm blue">&laquo;</a>',
                    array('class' => 'prev disabled btn btn-sm blue', 'tag' => 'li',
                    'escape' => false));
                echo $this->Paginator->first(__('প্রথম', true),
                    array('class' => 'number-first'));
                ?>
            </ul>

        </div>
    </div>
</div>
