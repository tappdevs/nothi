<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css"
      href="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css"
      href="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css"
      href="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>

<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            <i class="fs1 a2i_gn_nothi2"></i>গার্ড ফাইলের ধরন
        </div>
        <div class="actions">
            <a href="<?php echo $this->Url->build(['controller'=>'GuardFiles','action'=>'guardFileCategoryAdd']) ?>" class="btn round-corner-5  green btn-sm">
                <i class="fs1 a2i_gn_add1"></i> নতুন ধরন</a>
        </div>
    </div>
    <div class="portlet-body">
        <table class="table table-striped table-bordered table-hover" id="datatable-nothitype">
            <thead>

            <tr>
                <th style="width: 10%"  class='text-center'>ক্রমিক নং
                </th>
                <th class='text-center' style="width: 50%">
                    ধরন
                </th>
                
                <th class='text-center' style="width: 10%">
                    কার্যক্রম
                </th>
            </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
</div>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript"
        src="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript"
        src="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript"
        src="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script src="<?php echo CDN_PATH; ?>assets/global/scripts/datatable.js"></script>
<script src="<?php echo CDN_PATH; ?>projapoti-nothi/js/tbl_guard_category.js"></script>
<!-- END PAGE LEVEL PLUGINS -->

<script>
    $.extend(true, $.fn.dataTable.defaults, {
        "ordering": false
    });
    jQuery(document).ready(function () {
        TableAjax.init('GuardFiles/guardFileCategoriesList/');

    });
</script>