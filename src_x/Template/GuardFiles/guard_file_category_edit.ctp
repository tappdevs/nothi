<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class=""></i>গার্ড ফাইলের ধরন সংশোধন
        </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <?= $this->Form->create($guardFileCategories_records, ['type' => 'post', 'class' => 'form-horizontal']); ?>
        <div class="form-body">
            <div class="form-group">
                <label class="col-md-3 control-label">বিষয়</label>

                <div class="col-md-4">
                    <div class="input-group">

                        <?= $this->Form->hidden('id') ?>
                        <?= $this->Form->input('name_bng', ['label' => false, 'class' => 'form-control input-circle', 'type' => 'text', 'placeholder' => 'বিষয়']); ?>
                    </div>
                </div>
            </div>
            
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-3 col-md-9">
                        <button type="submit" class="btn   blue">সংরক্ষণ করুন</button>
                        <button type="button" class="btn   red"><a style="text-decoration: none; color: white;" href="<?= $this->URL->build(['action' => 'guardFileCategories']); ?>">বাতিল করুন</a></button>
                    </div>
                </div>
            </div>
        </div>
            <?= $this->Form->end(); ?>
    </div>
</div>
