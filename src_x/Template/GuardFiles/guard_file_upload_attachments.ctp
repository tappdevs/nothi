<style>

    .inbox .inbox-header h1 {
        margin-bottom: 0px;
    }

    .checkbox input[type=checkbox], .checkbox-inline input[type=checkbox], .radio input[type=radio], .radio-inline input[type=radio] {
        margin-left: -10px !important;
    }

    .inbox .inbox-header h1 {
        margin-bottom: 0px;
    }

    .checkbox input[type=checkbox], .checkbox-inline input[type=checkbox], .radio input[type=radio], .radio-inline input[type=radio] {
        margin-left: -10px !important;
    }

    .inbox .inbox-nav li.active b {
        display: none !important;
    }

    .tooltip-inner {
        white-space: pre-line;
    }

    h3 {
        margin-top: 0px;
        margin-bottom: 0px;
    }
</style>

<div class="portlet box purple">
    <div class="portlet-title">
        <div class="caption"> আপলোড গার্ড ফাইল</div>

    </div>
    <div class="portlet-body form">
        <!--Start: Common Fields required only for create action -->
        <?php
        echo $this->Form->create($guardFileEntity, ['type' => 'file', 'id' => 'uploadGuardForm', 'autocomplete' => 'off']);
        echo $this->Form->hidden('office_id', ['value' => $selected_office_section['office_id']]);
        ?>
        <!--Start: Remaining Form elements -->
        <div class="form-body">

            <div class="form-horizontal">

                <div class="row">
                    <div class="col-md-10">
                        <div class="form-group">
                            <label class="control-label col-md-2">শিরোনাম <span class="required"> * </span></label>

                            <div class="col-md-10">
                                <?php echo $this->Form->input('name_bng', array('label' => false, 'class' => 'form-control', 'data-required' => 1,'placeholder'=>'শিরোনাম লিখুন')); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10">
                        <div class="form-group">
                            <label class="control-label col-md-2">ধরন <span class="required"> * </span></label>

                            <div class="col-md-10">
                                <?php echo $this->Form->input('guard_file_category_id', array('label' => false, 'class' => 'form-control select', 'type' => 'select', 'data-required' => 1, 'options' => $categories,'empty'=>'ধরন নির্বাচন করুন')); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php echo $this->Form->hidden('uploaded_attachments', ['id' => 'uploaded_attachments']) ?>
            <!--End: Form Buttons -->
            <?php echo $this->Form->end(); ?>
            <!-- BEGIN PAGE CONTENT-->
            <div class="row">
                <div class="col-md-10">
                    <form id="fileupload" action="<?= $this->Url->build(['_name'=>'tempUpload']) ?>" method="POST" enctype="multipart/form-data">

                        <input type="hidden" name="module_type" value="Nothi" />
                        <input type="hidden" name="module" value="GuardFile" />
                        <input type="hidden" name="office_id" value="<?php echo $selected_office_section['office_id'] ?>" />

                        <div class="row fileupload-buttonbar">
                            <div class="col-lg-12">
                                <!-- The fileinput-button span is used to style the file input field as button -->
                                <span class="btn green fileinput-button round-corner-5">
                                    <i class="fs1 a2i_gn_add1"></i>
                                    <span>
                                        ফাইল যুক্ত করুন </span>
                                    <input type="file" name="files[]" >
                                </span>
                                <button type="button" class="btn red delete hidden round-corner-5">
                                    <i class="fs1 a2i_gn_delete2"></i>
                                    <span>
                                        সব মুছে ফেলুন </span>
                                </button>
                                <input type="checkbox" class="toggle hidden">
                                <!-- The global file processing state -->
                                <span class="fileupload-process">
                                </span>
                            </div>
                            <!-- The global progress information -->
                            <div class="col-lg-5 fileupload-progress fade">
                                <!-- The global progress bar -->
                                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                                    <div class="progress-bar progress-bar-success" style="width:0%;">
                                    </div>
                                </div>
                                <!-- The extended global progress information -->
                                <div class="progress-extended">
                                    &nbsp;
                                </div>
                            </div>

                        </div>
                        <!-- The table listing the files available for upload/download -->
                        <table role="presentation" class="table table-striped clearfix">
                            <tbody class="files">
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>


            <!--End: Remaining Form elements -->
            <!--Start: Form Buttons -->
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-9">
                        <?= $this->Form->button(__(SAVE), ['class' => 'btn purple submitbutton round-corner-5','type'=>'button','onclick'=>'GUARD_FORM.submit()']) ?>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<!-- Start: Common dak setup js -->
<script src="<?php echo CDN_PATH; ?>projapoti-nothi/js/guard_file_upload.js?v=<?= js_css_version ?>" type="text/javascript"></script>

<script>

    $(function () {
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-bottom-right"
        };
        GuardFileUpload.init();

    });
    
    var GUARD_FORM = {
        submit: function () {

            $("#uploadGuardForm").attr('action', '<?php echo $this->Url->build(['_name'=>'guardFile']) ?>');
            $('.template-download').each(function () {
                var tokne_href = $(this).find('.name > a').attr('href');
                var href = tokne_href.split('?token');
                $("#uploaded_attachments").val(href[0]);
            });
            
            toastr.options = {
            "closeButton": true,
                    "debug": false,
                    "positionClass": "toast-bottom-right"
            };
            if(isEmpty($("#name-bng").val())){
                toastr.error("দুঃখিত! শিরোনাম দেয়া হয়নি");
                return false;
            }
            if ($("#uploaded_attachments").val().length == 0) {
                toastr.error("দুঃখিত! গার্ড ফাইল দেয়া হয়নি");
                return false;
            }

            $(".submitbutton").attr('disabled', 'disabled');
                $("#uploadGuardForm").submit();
            }
        }
    
    
</script>

<!-- End: JavaScript -->
<!-- The blueimp Gallery widget -->
<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
    <div class="slides">
    </div>
    <h3 class="title"></h3>
    <a class="prev">
        ÔøΩ </a>
    <a class="next">
        ÔøΩ </a>
    <a class="close white">
    </a>
    <a class="play-pause">
    </a>
    <ol class="indicator">
    </ol>
</div>
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<script id="template-upload" type="text/x-tmpl">
    {% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
    
    <td>
    <p class="name">{%=file.name%}</p>
    <strong class="error label label-danger"></strong>
    </td>
    <td>
    <p class="size">প্রক্রিয়াকরন চলছে...</p>
    <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
    <div class="progress-bar progress-bar-success" style="width:0%;"></div>
    </div>
    </td>
    <td>
    {% if (!i && !o.options.autoUpload) { %}
    <button class="btn blue start" disabled>
    <i class="fa fa-upload"></i>
    <span>আপলোড করুন</span>
    </button>
    {% } %}
    {% if (!i) { %}
    <button class="btn red cancel">
    <i class="fa fa-ban"></i>
    <span>বাতিল করুন</span>
    </button>
    {% } %}
    </td>
    </tr>
    {% } %}


</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
    {% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade">
    
    <td>
    <p class="name">
    {% if (file.url) { %}
    <a href="<?= FILE_FOLDER ?>{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
    {% } else { %}
    <span>{%=file.name%}</span>
    {% } %}
    </p>
    {% if (file.error) { %}
    <div><span class="label label-danger"> ত্রুটি: </span> {%=file.error%}</div>
    {% } %}
    </td>
    <td>
    <span class="size">{%=o.formatFileSize(file.size)%}</span>
    </td>
    <td>
    {% if (file.deleteUrl) { %}
    <button class="btn red delete btn-sm" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
    <i class="fs1 a2i_gn_delete2"></i>
    <span>মুছে ফেলুন</span>
    </button>

    {% } else { %}
    <button class="btn yellow cancel btn-sm">
    <i class="fa fa-ban"></i>
    <span>বাতিল করুন</span>
    </button>
    {% } %}
    </td>
    </tr>
    {% } %}


</script>

