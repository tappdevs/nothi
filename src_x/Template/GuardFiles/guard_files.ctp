<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css"
      href="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css"
      href="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css"
      href="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>

<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            <i class="fs1 a2i_gn_nothi2"></i>গার্ড ফাইল তালিকা
        </div>
        <div class="actions">
            <!--            <a href="<?php // echo $this->Url->build(['controller'=>'GuardFiles','action'=>'guardFileCategoryAdd'])  ?>" class="btn   green btn-sm">
                <i class="fs1 a2i_gn_add1"></i> নতুন ধরন</a>-->
        </div>
    </div>
    <div class="portlet-body">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="row form-group">
                    <label class="col-md-2 control-label">ধরন</label>
                    <div class="col-md-6">
                        <?= $this->Form->input('guard_file_category_id',['label'=>false,'type'=>'select','class'=>'form-control', 'options'=>["0"=>"সকল"] + $guarfilesubjects]) ?>
                    </div>
                </div>
                <br/>

                <table class='table table-striped table-bordered table-hover' id="filelist">
                    <thead>
                    <tr>
                        <th class="text-center" style="width: 10%">ক্রম</th>
                        <th class="text-center" style="width: 30%">ধরন</th>
                        <th class="text-center" style="width: 40%">নাম</th>
                        <th class="text-center" style="width: 20%">কার্যক্রম</th>
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>

    </div>
</div>
<div class="modal fade modal-purple height-auto" id="responsiveOnuccedModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-full">

        <div class="modal-content">
            <div class="modal-header">

                <button type="button" data-dismiss="modal"
                        aria-hidden="true">×
                </button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript"
        src="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript"
        src="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script src="<?php echo CDN_PATH; ?>assets/global/scripts/datatable.js"></script>
<script src="<?php echo CDN_PATH; ?>projapoti-nothi/js/tbl_guard_files.js?v=<?= js_css_version ?>"></script>
<!-- END PAGE LEVEL PLUGINS -->

<script>
    function getPopUpPotro(href, title) {
        $('#responsiveOnuccedModal').find('.modal-title').text('');
        $('#responsiveOnuccedModal').find('.modal-body').html('');
        PROJAPOTI.ajaxSubmitDataCallback('<?php echo $this->request->webroot; ?>NothiMasters/showPopUp/' + href+'?nothi_part=0&token='+'<?= sGenerateToken(['file' => 0], ['exp' => time() + 60 * 300]) ?>', {'nothi_office':<?php echo $employee_office['office_id'] ?>}, 'html', function (response) {
            $('#responsiveOnuccedModal').modal('show');
            $('#responsiveOnuccedModal').find('.modal-title').text(title);

            $('#responsiveOnuccedModal').find('.modal-body').html(response);
        });

    }
    function deleteGuardFile(id) {
        bootbox.dialog({
            message: "আপনি কি গার্ড ফাইলটি মুছে দিতে ইচ্ছুক?",
            title: "গার্ড ফাইল মুছুন",
            buttons: {
                success: {
                    label: "হ্যাঁ",
                    className: "green",
                    callback: function () {
                        window.location.href='<?php echo $this->request->webroot; ?>GuardFiles/deleteGuardFile/'+id ;
                    }
                },
                danger: {
                    label: "না",
                    className: "red",
                    callback: function () {
                    }
                }
            }
        });

    }
    $.extend(true, $.fn.dataTable.defaults, {
        "ordering": false,
    });
    jQuery(document).ready(function () {
        TableAjax.init();
    });

</script>