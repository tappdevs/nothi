

<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            <i class="fs1 a2i_gn_guardfile1"></i>গার্ড ফাইল : <?php echo !empty($data['name_bng'])?(h($data['name_bng']).','):''; ?> <?php echo !empty($data['category_name'])?("বিষয়: ".$data['category_name']):''; ?>
        </div>
<!--        <div class="actions">
            <a href="<?php // echo $this->Url->build(['controller'=>'GuardFiles','action'=>'GuardFiles']) ?>" class="btn   green btn-sm">
                <i class="fs1 a2i_gn_previous1"></i> গার্ড ফাইল তালিকা</a>
        </div>-->
    </div>
    <div class="portlet-body">
       
        <table class="table table-striped table-bordered">
            
            <tr class="heading">
                <th class="text-center" style="width: 10%;">ক্রমিক নং</th>
                <th class="text-center" style="width: 70%">নাম</th>
                <th class="text-center" style="width: 20%">দেখুন</th>
            </tr>
        
       <?php if(!empty($attachments)): ?>
        <?php
        foreach($attachments as $key=>$value){
            
            ?> 
            <tr>
                <td class="text-center"><?php echo $this->Number->format($key+1); ?></td>
                <td class="text-left"><?php echo urldecode($value['name_bng']) ?></td>
                <td class="text-center">
                    <a href="<?= $value['id']?>/guard-attachment/<?php echo $data['id'] ?>" class="showforPopup" title="<?= urldecode($value['name_bng']) ?>"><i class="fs1 a2i_gn_details2"></i></a>
            </tr>
            <?php
        }
        
        ?>
       <?php endif; ?>
        </table>
    </div>
</div>

 <div class="modal fade modal-purple bs-modal-lg" id="responsiveModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">

            <div class="modal-content">
                <div class="modal-header">

                    <button type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">

                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

<script>


function getPopUpPotro(href, title) {
    $('#responsiveModal').find('.modal-title').text('');
    $('#responsiveModal').find('.modal-body').html('');
    $.ajax({
        url: '<?php echo $this->request->webroot; ?>NothiMasters/showPopUp/' + href+'?nothi_part=0&token='+'<?= sGenerateToken(['file' => 0], ['exp' => time() + 60 * 300]) ?>',
        dataType: 'html',
        data:{'nothi_office':<?php echo $data['office_id'] ?>},
        type:'post',
        success: function (response) {
            $('#responsiveModal').modal('show');
            $('#responsiveModal').find('.modal-title').text(title);

            $('#responsiveModal').find('.modal-body').html(response);
        }
    });
}

$(document).off('click', '.showforPopup').on('click', '.showforPopup', function (e) {
    e.preventDefault();
    var title = $(this).attr('title').length > 0 ? $(this).attr('title') : $(this).attr('data-original-title');
    getPopUpPotro($(this).attr('href'), title);
})
    
</script>