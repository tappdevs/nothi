<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.2
Version: 3.3.0
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title>দপ্তর | Activation Form</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta content="" name="description"/>
    <meta content="" name="author"/>

    <?php
$path = $this->request->webroot;
if(CDN == 1){
    $path = 'http://cdn1.nothi.gov.bd/webroot/';
}

?>
    <link href="<?php echo $path; ?>assets/global/plugins/font-awesome/css/font-awesome.min.css"
          rel="stylesheet" type="text/css"/>
    <link href="<?php echo $path; ?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css"
          rel="stylesheet" type="text/css"/>
    <link href="<?php echo $path; ?>assets/global/plugins/bootstrap/css/bootstrap.min.css"
          rel="stylesheet" type="text/css"/>
    <link href="<?php echo $path; ?>assets/global/plugins/uniform/css/uniform.default.css"
          rel="stylesheet" type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="<?php echo $path; ?>assets/global/plugins/select2/select2.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?php echo $path; ?>assets/admin/pages/css/login-soft.css" rel="stylesheet"
          type="text/css"/>
    <!-- END PAGE LEVEL SCRIPTS -->
    <!-- BEGIN THEME STYLES -->
    <link href="<?php echo $path; ?>assets/global/css/components-md.css" id="style_components"
          rel="stylesheet" type="text/css"/>
    <link href="<?php echo $path; ?>assets/global/css/plugins-md.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?php echo $path; ?>assets/admin/layout4/css/layout.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?php echo $path; ?>assets/admin/layout4/css/themes/default.css" rel="stylesheet"
          type="text/css" id="style_color"/>
    <link href="<?php echo $path; ?>assets/admin/layout4/css/custom.css" rel="stylesheet"
          type="text/css"/>
    <!-- END THEME STYLES -->
    <link rel="shortcut icon" href="favicon.ico"/>
    <?php
    if (Live == 1) {
        ?>
        <script id="fr-fek">try {
                (function (k) {
                    localStorage.FEK = k;
                    t = document.getElementById('fr-fek');
                    t.parentNode.removeChild(t);
                })('xc1We1KYi1Ta1WId1CVd1F==')
            } catch (e) {
            }</script>
        <?php
    }
    ?>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->

<body class="login">
<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
<div class="menu-toggler sidebar-toggler">
</div>
<!-- END SIDEBAR TOGGLER BUTTON -->
<!-- BEGIN LOGO -->
<div class="logo">
    <a href="index.html">
        <img src="<?php echo $path; ?>assets/admin/layout4/img/logo-big.png" alt=""/>
    </a>
</div>
<!-- END LOGO -->
<!-- BEGIN LOGIN -->
<div class="content">
    <?php echo $this->Flash->render(); ?>
    <?php echo $this->element('form_activation') ?>
</div>
<br>

<div class="content">
    <div class="create-help1">
        <p>
            <a href="javascript:" id="register-btn">
                ব্যবহার সহায়িকা </a>
        </p>
    </div>
    <div class="logobd" align="left">
        <a href="#">
            <img src="<?php echo $path; ?>assets/admin/layout4/img/bd.png" alt=""/>
        </a>
    </div>
    <div class="logoa2i" align="right">
        <a href="#">
            <img src="<?php echo $path; ?>assets/admin/layout4/img/a2i.png" alt=""/>
        </a>
    </div>
</div>
<!--div class="copyright">
	 2015 © A2i.
</div-->
<!-- END LOGIN -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="<?php echo $path; ?>assets/global/plugins/respond.min.js"></script>
<script src="<?php echo $path; ?>assets/global/plugins/excanvas.min.js"></script>
<![endif]-->
<script src="<?php echo $path; ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo $path; ?>assets/global/plugins/jquery-migrate.min.js"
        type="text/javascript"></script>
<script src="<?php echo $path; ?>assets/global/plugins/bootstrap/js/bootstrap.min.js"
        type="text/javascript"></script>
<script src="<?php echo $path; ?>assets/global/plugins/jquery.blockui.min.js"
        type="text/javascript"></script>
<script src="<?php echo $path; ?>assets/global/plugins/uniform/jquery.uniform.min.js"
        type="text/javascript"></script>
<script src="<?php echo $path; ?>assets/global/plugins/jquery.cokie.min.js"
        type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo $path; ?>assets/global/plugins/jquery-validation/js/jquery.validate.min.js"
        type="text/javascript"></script>
<script src="<?php echo $path; ?>assets/global/plugins/backstretch/jquery.backstretch.min.js"
        type="text/javascript"></script>
<script type="text/javascript"
        src="<?php echo $path; ?>assets/global/plugins/select2/select2.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo $path; ?>assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="<?php echo $path; ?>assets/admin/layout4/scripts/layout.js"
        type="text/javascript"></script>
<script src="<?php echo $path; ?>assets/admin/layout4/scripts/demo.js" type="text/javascript"></script>
<script src="<?php echo $path; ?>assets/admin/pages/scripts/login-soft.js"
        type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script type="text/javascript">
    jQuery(document).ready(function () {
        Metronic.init(); // init metronic core components
        Layout.init(); // init current layout
        Login.init();
        Demo.init();
// init background slide images
        $.backstretch([
            "<?php echo $path; ?>assets/admin/pages/media/bg/1.jpg"
        ], {
            fade: 1000,
            duration: 8000
        });
    });
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>