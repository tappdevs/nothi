<!DOCTYPE html>

<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->

<html lang="en" class="no-js">
<!--<![endif]-->

<head>
    <meta charset="utf-8"/>
    <title>নথি | অফিস ব্যবস্থাপনা</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    <?php
    $path = $this->request->webroot;
    if (CDN == 1) {
        $path = 'http://cdn1.nothi.gov.bd/webroot/';
    }

    ?>
    <link rel="stylesheet" href="<?php echo $path; ?>a2i/demo-files/demo.css">
    <link rel="stylesheet" href="<?php echo $path; ?>a2i/style.css">

    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="<?php echo $path; ?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo $path; ?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo $path; ?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo $path; ?>assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo $path; ?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>

    <link rel="stylesheet" type="text/css" href="<?php echo $path; ?>assets/global/plugins/select2/select2.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo $path; ?>assets/global/plugins/bootstrap-datepicker/css/datepicker.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo $path; ?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo $path; ?>assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css">
    <link href="<?php echo $path; ?>assets/admin/pages/css/error.css" rel="stylesheet" type="text/css"/>

    <link href="<?php echo $path; ?>assets/global/css/components.css" id="style_components" rel="stylesheet" type="text/css"/>
    <link href="<?php echo $path; ?>assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo $path; ?>assets/admin/layout4/css/layout.css" rel="stylesheet" type="text/css"/>
    <link rel="shortcut icon" href="<?php echo $path; ?>favicon.ico"/>
    <script src="<?php echo $path; ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>

    <?php
    if (Live == 1) {
        ?>
        <script id="fr-fek">try {
                (function (k) {
                    localStorage.FEK = k;
                    t = document.getElementById('fr-fek');
                    t.parentNode.removeChild(t);
                })('xc1We1KYi1Ta1WId1CVd1F==')
            } catch (e) {
            }</script>
        <?php
    }
    ?>
    <?php
    if(!defined('Live') || Live == 0 ){
        $background_image = '
            background-image: url("'.WWW_ROOT. 'img/test-background.png'.'");
            background-repeat: no-repeat;background-position:center;
        ';
    }else{
        $background_image = '';
    }
    ?>
    <style>
        .printOnucched img {
            width: 100%!important;
            height: 50px!important;
        }

        .printOnucched a[href]:after {
            content: "";
        }

        body{
            font-family: Nikosh, SolaimanLipi, 'Open Sans', sans-serif !important;
            font-size: 12pt!important;
        <?php
            if(!empty($query['margin-left'])){
            echo 'padding-left:'.$query['margin-left'].'in!important;';
            }
            if(!empty($query['margin-right'])){
            echo 'padding-right:'.$query['margin-right'].'in!important;';
            }
        ?>
            <?= $background_image ?>
        }
    </style>
</head>
<!-- END HEAD -->
<script type="text/javascript">
    var js_wb_root = '<?= $this->request->webroot ?>';
</script>
<body  class="page-header-fixed page-boxed page-full-width">
<div class="clearfix"></div>
<div class="page-container" style="margin-top: 0px;">
    <div class="page-content-wrapper">
        <div class="page-content" style="font-size:12pt;">
            <?php echo $this->fetch('content'); ?>
        </div>
    </div>
</div>

<!--[if lt IE 9]>
<script src="<?php echo $path; ?>assets/global/plugins/respond.min.js"></script>
<script src="<?php echo $path; ?>assets/global/plugins/excanvas.min.js"></script>
<![endif]-->
<script src="<?php echo $path; ?>assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="<?php echo $path; ?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo $path; ?>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?php echo $path; ?>assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="<?php echo $path; ?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo $path; ?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
<script src="<?php echo $path; ?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<script src="<?php echo $path; ?>assets/global/plugins/jquery-tags-input/jquery.tagsinput.min.js" type="text/javascript"></script>
<script src="<?php echo $path; ?>assets/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
<script src="<?php echo $path; ?>assets/admin/layout4/scripts/projapoti_ajax.js?v=<?= js_css_version ?>" type="text/javascript"></script>
<script src="<?php echo $path; ?>assets/global/scripts/metronic.js" type="text/javascript"></script>
<script type="text/javascript">
    jQuery(document).ready(function () {
        Metronic.init();
    });
</script>
</body>
</html>