<!DOCTYPE html>
<html lang="en">

<head>
  <base href="./">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
  <meta name="description" content="CoreUI - Open Source Bootstrap Admin Template">
  <meta name="author" content="Łukasz Holeczek">
  <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,jQuery,CSS,HTML,RWD,Dashboard">
  <title>নথি | অফিস ব্যবস্থাপনা</title>
  <!-- Icons-->
  <link href="<?= $this->request->webroot ?>coreui/css/font-icons.css" rel="stylesheet">
  <link href="<?= $this->request->webroot ?>coreui/node_modules/@coreui/icons/css/coreui-icons.min.css" rel="stylesheet">
  <link href="<?= $this->request->webroot ?>coreui/node_modules/flag-icon-css/css/flag-icon.min.css" rel="stylesheet">
  <link href="<?= $this->request->webroot ?>coreui/node_modules/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="<?= $this->request->webroot ?>coreui/node_modules/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">
  <link href="<?= $this->request->webroot ?>coreui/vendors/select2/dist/css/select2.min.css" rel="stylesheet">
  <link href="<?= $this->request->webroot ?>coreui/node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet">
  <link href="<?= $this->request->webroot ?>coreui/node_modules/datatables.net-bs4/css/dataTables.bootstrap4.css" rel="stylesheet">
  <!-- Include Editor style. -->
  <link href='<?= $this->request->webroot ?>coreui/node_modules/froala-editor/css/froala_editor.min.css' rel='stylesheet' type='text/css' />
  <link href='<?= $this->request->webroot ?>coreui/node_modules/froala-editor/css/froala_style.min.css' rel='stylesheet' type='text/css' />


  <!-- Main styles for this application-->
  <link href="<?= $this->request->webroot ?>coreui/css/style.genius.css" rel="stylesheet">
  <link href="<?= $this->request->webroot ?>coreui/vendors/pace-progress/css/pace.min.css" rel="stylesheet">
  <link href="<?= $this->request->webroot ?>coreui/css/nothi_style.css" rel="stylesheet">

  <!-- Bootstrap and necessary plugins-->
  <script src="<?= $this->request->webroot ?>coreui/node_modules/jquery/dist/jquery.min.js"></script>
  <script src="<?= $this->request->webroot ?>coreui/node_modules/popper.js/dist/umd/popper.min.js"></script>
  <script src="<?= $this->request->webroot ?>coreui/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>


  <script src="<?= $this->request->webroot ?>coreui/node_modules/pace-progress/pace.min.js"></script>
  <script src="<?= $this->request->webroot ?>coreui/node_modules/perfect-scrollbar/dist/perfect-scrollbar.min.js"></script>
  <script src="<?= $this->request->webroot ?>coreui/node_modules/@coreui/coreui-pro/dist/js/coreui.min.js"></script>
  <script src="<?= $this->request->webroot ?>coreui/vendors/select2/dist/js/select2.min.js"></script>
  <script src="<?= $this->request->webroot ?>coreui/node_modules/moment/min/moment.min.js"></script>
  <script src="<?= $this->request->webroot ?>coreui/node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>


  <script src="<?= $this->request->webroot ?>coreui/node_modules/datatables.net/js/jquery.dataTables.js"></script>
  <script src="<?= $this->request->webroot ?>coreui/node_modules/datatables.net-bs4/js/dataTables.bootstrap4.js"></script>
  <script src="<?= $this->request->webroot ?>coreui/js/datatables.js"></script>
  <!-- Include JS file. -->
  <script>
    var base_url = <?= $this->request->webroot; ?>
    // console.log(base_url)
  </script>
</head>

<body class="app header-fixed sidebar-fixed aside-menu-fixed <?= !isset($page) ? 'sidebar-lg-show' : '' ?> font-bn-en">
  <header class="app-header navbar">
    <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
      <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand py-1" href="#">
      <img class="navbar-brand-full h-100" src="<?= $this->request->webroot ?>coreui/img/logo.png" alt="CoreUI Logo">
      <img class="navbar-brand-minimized" src="<?= $this->request->webroot ?>coreui/img/logo.png" alt="CoreUI Logo">
    </a>
    <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
      <span class="navbar-toggler-icon"></span>
    </button>

    <ul class="nav navbar-nav">
      <li class="nav-item">
        <div class="btn-group">
          <button class="btn btn-success-n text-white fs-18 border-right-n" title="ডাক">
            <i class="fa fa-envelope-o"></i> ডাক
            <small class="badge badge-light text-success-n font-weight-normal">23</small>
          </button>
          <button class="btn btn-success-n text-white fs-18 border-left-n border-right-n active" title="নথি">
            <i class="fs0 efile-nothi1 mr-1"></i> নথি
            <small class="badge badge-light text-success-n font-weight-normal">23</small>
          </button>
          <button class="btn btn-success-n text-white fs-18 border-left-n border-right-n" title="দপ্তর">
            <i class="fa fa-folder-open mr-1"></i> দপ্তর
          </button>
          <button class="btn btn-success-n text-white fs-18 border-left-n" title="রিপোর্ট">
            <i class="fa fa-folder-open mr-1"></i>রিপোর্ট
            <small class="badge badge-light text-success-n font-weight-normal">23</small>
          </button>
        </div>
      </li>
    </ul>

    <ul class="nav navbar-nav ml-auto">

      <li class="nav-item">
        <div class="user_position font-bn-en dropdown line-height-1 pr-3 fs-16">
          <button class="dropdown-toggle text-light shadow-none btn-transparent text-left outline-none" id="user_position" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> কোহিনূর বেগম
            <small>(অফিস সহকারী, ব্যবসা-বাণিজ্য শাখা)</small>
            <br>
            জেলা প্রশাসকের কার্যালয়, জামালপুর
          </button>
          <div class="dropdown-menu dropdown-menu-right fs-16" aria-labelledby="user_position">
            <a class="dropdown-item" href="#"><i class="fa fa-mobile-phone"></i> নথি মোবাইল অ্যাপ </a>
            <a class="dropdown-item" href="#"><i class="fa fa-user"></i> প্রোফাইল ব্যবস্থাপনা </a>
            <a class="dropdown-item" href="#"><i class="fa fa-sign-out"></i> লগ আউট করুন </a>
          </div>
        </div>
      </li>

      <li class="nav-item dropdown">
        <a class="nav-link nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
          <img class="img-avatar" src="<?= $this->request->webroot ?>coreui/img/avatars/6.jpg" alt="admin@bootstrapmaster.com">
        </a>
        <div class="dropdown-menu dropdown-menu-right">
          <div class="dropdown-header text-center">
            <strong>Account</strong>
          </div>
          <a class="dropdown-item" href="#">
            <i class="fa fa-bell-o"></i> Updates
            <span class="badge badge-info">42</span>
          </a>
          <a class="dropdown-item" href="#">
            <i class="fa fa-envelope-o"></i> Messages
            <span class="badge badge-success">42</span>
          </a>
          <a class="dropdown-item" href="#">
            <i class="fa fa-tasks"></i> Tasks
            <span class="badge badge-danger">42</span>
          </a>
          <a class="dropdown-item" href="#">
            <i class="fa fa-comments"></i> Comments
            <span class="badge badge-warning">42</span>
          </a>
          <div class="dropdown-header text-center">
            <strong>Settings</strong>
          </div>
          <a class="dropdown-item" href="#">
            <i class="fa fa-user"></i> Profile</a>
          <a class="dropdown-item" href="#">
            <i class="fa fa-wrench"></i> Settings</a>
          <a class="dropdown-item" href="#">
            <i class="fa fa-usd"></i> Payments
            <span class="badge badge-dark">42</span>
          </a>
          <a class="dropdown-item" href="#">
            <i class="fa fa-file"></i> Projects
            <span class="badge badge-primary">42</span>
          </a>
          <div class="divider"></div>
          <a class="dropdown-item" href="#">
            <i class="fa fa-shield"></i> Lock Account</a>
          <a class="dropdown-item" href="#">
            <i class="fa fa-lock"></i> Logout</a>
        </div>
      </li>
    </ul>
    <!--
  <button class="navbar-toggler aside-menu-toggler d-md-down-none" type="button" data-toggle="aside-menu-lg-show">
    <span class="navbar-toggler-icon"></span>
  </button>
  <button class="navbar-toggler aside-menu-toggler d-lg-none" type="button" data-toggle="aside-menu-show">
    <span class="navbar-toggler-icon"></span>
  </button>
  -->
  </header>
  <div class="app-body d-flex flex-column h-100 overflow-hidden">
    <div class="sidebar">
      <nav class="sidebar-nav">
        <!--<ul class="nav">
        <li class="nav-item">
          <a class="nav-link" href="index.html">
            <i class="nav-icon icon-speedometer"></i> Dashboard
            <span class="badge badge-info">NEW</span>
          </a>
        </li>
      </ul>-->

        <!--
      <ul class="nav">
        <li class="nav-item">
          <a class="nav-link" href="index.html">
            <i class="nav-icon icon-speedometer"></i> Dashboard
            <span class="badge badge-info">NEW</span>
          </a>
        </li>

        <li class="nav-title">Components</li>
        <li class="nav-item nav-dropdown">
          <a class="nav-link nav-dropdown-toggle" href="#">
            <i class="nav-icon icon-puzzle"></i> Base</a>
          <ul class="nav-dropdown-items">
            <li class="nav-item">
              <a class="nav-link" href="base/breadcrumb.html">
                <i class="nav-icon icon-puzzle"></i> Breadcrumb</a>
            </li>
          </ul>
        </li>
      </ul>
      -->

        <ul class="nav">
          <li class="nav-title">নথি</li>
          <li class="nav-item nav-dropdown">
            <a class="nav-link nav-dropdown-toggle" href="#">
              <i class="fs0 efile-nothi_management1 nav-icon" aria-hidden="true"></i> নথি ব্যবস্থাপনা</a>
            <ul class="nav-dropdown-items">
              <li class="nav-item nav-dropdown">
                <a class="nav-link nav-dropdown-toggle" href="#">
                  <i class="fs0 efile-nothi_management1 nav-icon" aria-hidden="true"></i> নথির ধরন</a>
                <ul class="nav-dropdown-items">
                  <li class="nav-item">
                    <a class="nav-link" href="#">
                      <i class="fa fa-plus nav-icon"></i> ধরন তৈরি</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#">
                      <i class="icon-tag nav-icon"></i> ধরনসমূহ</a>
                  </li>
                </ul>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">
                  <i class="icon-tag nav-icon"></i> নথি তৈরি</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">
                  <i class="icon-tag nav-icon"></i> পত্রজারি গ্রুপ</a>
              </li>
              <li class="nav-item nav-dropdown">
                <a class="nav-link nav-dropdown-toggle" href="#">
                  <i class="fs0 efile-nothi_management1 nav-icon" aria-hidden="true"></i> গার্ড ফাইল</a>
                <ul class="nav-dropdown-items">
                  <li class="nav-item">
                    <a class="nav-link" href="#">
                      <i class="icon-tag nav-icon"></i> গার্ড ফাইলের ধরন</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="base/#.html">
                      <i class="icon-tag nav-icon"></i> গার্ড ফাইল তালিকা</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#">
                      <i class="icon-tag nav-icon"></i> আপলোড গার্ড ফাইল</a>
                  </li>
                </ul>
              </li>
            </ul>
          </li>
          <li class="nav-item nav-dropdown">
            <a class="nav-link nav-dropdown-toggle" href="#">
              <i class="fs0 a2i_gn_register2 nav-icon" aria-hidden="true"></i> নিবন্ধন বহি </a>
            <ul class="nav-dropdown-items">
              <li class="nav-item">
                <a class="nav-link" href="#"><i class="nav-icon fs0 efile-user2" aria-hidden="true"></i> মাস্টার ফাইল
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#"><i class="nav-icon fs0 efile-preron_register1" aria-hidden="true"></i> নথি প্রেরণ নিবন্ধন বহি
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#"><i class="nav-icon fs0 efile-grohon_register1" aria-hidden="true"></i> নথি গ্রহণ নিবন্ধন বহি
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#"><i class="nav-icon fs0 efile-register1" aria-hidden="true"></i>
                  নথি নিবন্ধন বহি
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#"><i class="nav-icon fs0 efile-potrojari1" aria-hidden="true"></i> পত্রজারি নিবন্ধন বহি
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#"><i class="nav-icon fs0 efile-save2" aria-hidden="true"></i>
                  আর্কাইভড নথিসমূহ
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item nav-dropdown">

            <a class="nav-link nav-dropdown-toggle" href="#">
              <i class="fs0 a2i_ld_protibedon3 nav-icon" aria-hidden="true"></i> <span class="title">প্রতিবেদনসমূহ</span>
              <span class="arrow"></span></a>
            <ul class="nav-dropdown-items">
              <li class="nav-item">
                <a class="nav-link" href="#"><i class="fs0 a2i_ld_protibedon3 nav-icon" aria-hidden="true"></i>
                  শাখাভিত্তিক নথিসমূহের তালিকা
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link"><i class="fs0 efile-potrojari3 nav-icon" aria-hidden="true"></i> পত্রজারি পেন্ডিং
            </a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link"><i class="fs0 efile-settings2 nav-icon" aria-hidden="true"></i> সেটিংস
              <ul class="nav-dropdown-items">
                <li class="nav-item">
                  <a href="#" class="nav-link"><i class="fs0 a2i_gn_abedongrohon2 nav-icon" aria-hidden="true"></i>
                    নথি সিদ্ধান্তসমূহ
                  </a>
                </li>
                <li class="nav-item">
                  <a href="#" class="nav-link"><i class="fs0 a2i_gn_approval1 nav-icon" aria-hidden="true"></i>
                    নথিতে অনুমতি প্রদান
                  </a>
                </li>
                <li class="nav-item">
                  <a href="#" class="nav-link"><i class="fs0 a2i_gn_close1 nav-icon" aria-hidden="true"></i>
                    নথি হতে অনুমতি প্রত্যাহার
                  </a>
                </li>
                <li class="nav-item">
                  <a href="#">
                    <i class="fs0 a2i_gn_template1 nav-icon" aria-hidden="true"></i>
                    আর্কাইভের জন্য অপেক্ষমাণ নথিসমূহ
                  </a>
                </li>
              </ul>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link"><i class="fs0 a2i_nt_nishponnonothi1 nav-icon" aria-hidden="true"></i> সার-সংক্ষেপ ট্রাকিং </a>
          </li>

        </ul>
        <!--
      <ul class="nav">
        <li class="nav-item">
          <a class="nav-link" href="index.html">
            <i class="nav-icon icon-speedometer"></i> Dashboard
            <span class="badge badge-info">NEW</span>
          </a>
        </li>

        <li class="nav-title">Components</li>
        <li class="nav-item nav-dropdown">
          <a class="nav-link nav-dropdown-toggle" href="#">
            <i class="nav-icon icon-puzzle"></i> Base</a>
          <ul class="nav-dropdown-items">
            <li class="nav-item">
              <a class="nav-link" href="base/breadcrumb.html">
                <i class="nav-icon icon-puzzle"></i> Breadcrumb</a>
            </li>
          </ul>
        </li>
      </ul>
      -->
      </nav>
      <button class="sidebar-minimizer brand-minimizer" type="button"></button>
    </div>
    <main class="main d-flex flex-column flex-fill position-relative h-100">
      <!-- Breadcrumb-->
      <?php
      if (!isset($page)) {
        ?>
        <div class="bg-white shadow-sm align-items-center py-2 d-flex">
          <div class="col-xl-4">
            <?= $this->Form->select('nothi_type', ['অফিস সহকারী, ব্যবসা-বাণিজ্য শাখা', 'সিনিয়র সফটওয়্যার ইঞ্জিনিয়ার 1 (অতিরিক্ত দায়িত্ব), ই-সার্ভিস টেস্ট', 'সাঁট লিপিকার, জেলা প্রশাসকের কার্যালয়'], ['class' => 'form-control', 'empty' => '-- পদবি নির্বাচন করুন --']); ?>
          </div>
          <div class="col d-flex">
            <div class="search-all flex-fill">
              <div class="dropdown">
                <input type="text" class="form-control" placeholder="&#xf002; বিষয় দিয়ে খুঁজুন...">
                <button id="advanced-search" class="btn btn-transparent text-info btn-toggler" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-caret-down"></i></button>
                <div class="dropdown-menu modules dropdown-menu-right shadow w-100" aria-labelledby="advanced-search">
                  <form class="px-4 py-3" action="" method="post">
                    <h4 class="font-bn-en">বিস্তারিত খুঁজুন</h4>
                    <div class="form-row">
                      <div class="col-md-6 mb-3">
                        <?= $this->Form->control('nothi_no', ['placeholder' => 'নথি নাম্বার', 'class' => 'form-control']) ?>
                      </div>
                      <div class="col-md-6 mb-3">
                        <?= $this->Form->select('nothi_type', ['নিম্নোক্ত তালিকা', 'থেকে পদবি ', 'নির্বাচন করুন'], ['class' => 'form-control', 'empty' => '-- নথি ধরন --']); ?>
                      </div>
                      <div class="col-md-12 mb-3">
                        <div class="input-group">
                          <input type="text" aria-label="First name" class="form-control datepicker" placeholder="তারিখ নির্বাচন">
                          <span class="input-group-text">হইতে</span>
                          <input type="text" aria-label="Last name" class="form-control datepicker" placeholder="তারিখ নির্বাচন">
                        </div>
                      </div>
                    </div>
                    <div class="form-row">
                      <div class="col-md-6 mb-3">
                        <?= $this->Form->select('select_section', ['গোপনীয় শাখা', 'আর এম শাখা', 'জেনারেল সার্টিফিকেট শাখা', 'রাজস্ব মুন্সিখানা শাখা', 'রেকর্ড রুম শাখা', 'ভূমি অধিগ্রহণ শাখা', 'ভি পি শাখা', 'প্রকাশনা শাখা', 'ফৌজদারী নকলখানা শাখা', 'রীট শাখা', 'বিচার শাখা', 'জুডিশিয়াল মুন্সিখানা শাখা', 'লাইসেন্স (প্রমোদ) শাখা', 'আগ্নেয়াস্ত্র শাখা', 'সীমান্ত শাখা', 'নির্বাহী ম্যাজিস্ট্রেট আদালত', 'গোপনীয় শাখা', 'সার্বিক', 'স্থানীয় সরকার', 'রাজস্ব', 'গোপনীয় শাখা', 'অতিরিক্ত জেলা ম্যাজিস্ট্রেটের দপ্তর', 'ভূমি অধিগ্রহণ', 'এনজিও বিষয়ক শাখা', 'মুক্তিযোদ্ধা কল্যাণ বিষয়ক শাখা', 'ফ্রন্ট ডেস্ক শাখা', 'শিক্ষা ও কল্যাণ শাখা', 'জেলা ই-সেবা কেন্দ্র'], ['class' => 'form-control', 'empty' => '-- শাখা নির্বাচন করুন --']); ?>
                      </div>
                      <div class="col-md-6 mb-3">
                        <?= $this->Form->select('all_nothi', ['গোপনীয় নথি সমূহ', 'শুধু জরুরী নথি সমূহ'], ['class' => 'form-control', 'empty' => '-- সকল নথি --']); ?>
                      </div>

                    </div>
                    <div class="btn-group">
                      <button type="submit" class="btn btn-success-n text-white"><i class="fa fa-search"></i> খুঁজুন
                      </button>
                      <button type="reset" class="btn btn-danger-n text-white"><i class="fa fa-refresh"></i> রিসেট
                      </button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
            <div class="btn btn-success text-white"><i class="fa fa-search"></i></div>
            <div class="btn btn-danger"><i class="fa fa-refresh"></i></div>
            <div class="btn btn-primary aside-menu-toggler" data-toggle="aside-menu-lg-show"><i class="fa fa-bars"></i>
            </div>
          </div>
        </div>
      <?php } ?>
      <div id="mainContent" class="animated mainContent fadeIn flex-basis border-top  bg-white overflow-hidden flex-column d-flex">

        <?php
        if (isset($page)) {
          echo $this->element('coreui/nothi/nothi_note_container');
        } else {
          ?>
          <div>
            <ul class="nav nav-tabs tabs-1 bg-white" role="tablist">
              <li class="nav-item">
                <a class="nav-link active d-flex align-items-center" data-toggle="tab" href="#home4" role="tab" aria-controls="home">
                  <i class="fa fa-inbox fs-20"></i>
                  <aside class="ml-3 line-height-1">
                    <strong class="text-dark font-weight-normal">আগত নথি</strong>
                    <span class="badge">12</span>
                    <small class="text-truncate nothi-from d-block text-muted" data-toggle="tooltip" title="জাহাঙ্গীর, অফিস সহকারী (জেলা প্রশাসকের কার্যালয়, জামালপুর), a2i@gov.bd">জাহাঙ্গীর, অফিস
                      সহকারী (জেলা প্রশাসকের কার্যালয়, জামালপুর), a2i@gov.bd
                    </small>
                  </aside>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link d-flex align-items-center" data-toggle="tab" href="#profile4" role="tab" aria-controls="profile">
                  <i class="fa fa-paper-plane-o fs-20"></i>
                  <aside class="ml-3 line-height-1">
                    <strong class="text-dark font-weight-normal">প্রেরিত নথি</strong>
                    <span class="badge">232</span>
                    <small class="text-truncate nothi-from d-block text-muted" data-toggle="tooltip" title="জাহাঙ্গীর, অফিস সহকারী (জেলা প্রশাসকের কার্যালয়, জামালপুর), a2i@gov.bd">জাহাঙ্গীর, অফিস
                      সহকারী (জেলা প্রশাসকের কার্যালয়, জামালপুর), a2i@gov.bd
                    </small>
                  </aside>
                </a>
              </li>

              <li class="nav-item dropdown">
                <a class="nav-link d-flex align-items-center dropdown-toggle" aria-controls="messages" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="fa fa-envelope-open-o fs-20"></i>
                  <aside class="ml-3 line-height-1">
                    <strong class="text-dark font-weight-normal">অন্যান্য অফিসের নথি</strong>
                    <span class="badge">343</span>
                    <small class="text-truncate nothi-from d-block text-muted" data-toggle="tooltip" title="জাহাঙ্গীর, অফিস সহকারী (জেলা প্রশাসকের কার্যালয়, জামালপুর), a2i@gov.bd">জাহাঙ্গীর, অফিস
                      সহকারী (জেলা প্রশাসকের কার্যালয়, জামালপুর), a2i@gov.bd
                    </small>
                  </aside>
                </a>
                <div class="dropdown-menu shadow" aria-labelledby="dropdownMenuLink">
                  <a class="dropdown-item" href="#">অন্যান্য অফিস থেকে আগত নথি</a>
                  <a class="dropdown-item" href="#">অন্যান্য অফিসে প্ররিত নথি</a>
                </div>
              </li>

              <li class="nav-item">
                <a class="nav-link d-flex align-items-center" data-toggle="tab" href="#messages4" role="tab" aria-controls="messages">
                  <i class="icon-envelope-letter fs-20"></i>
                  <aside class="ml-3 line-height-1">
                    <strong class="text-dark font-weight-normal">সকল নথি</strong>
                    <span class="badge">23</span>
                    <small class="text-truncate nothi-from d-block text-muted" data-toggle="tooltip" title="জাহাঙ্গীর, অফিস সহকারী (জেলা প্রশাসকের কার্যালয়, জামালপুর), a2i@gov.bd">জাহাঙ্গীর, অফিস
                      সহকারী (জেলা প্রশাসকের কার্যালয়, জামালপুর), a2i@gov.bd
                    </small>
                  </aside>
                </a>
              </li>

              <li class="nav-item">
                <a class="nav-link d-flex align-items-center" data-toggle="tab" href="#messages4" role="tab" aria-controls="messages">
                  <i class="icon-layers fs-20"></i>
                  <aside class="ml-3 line-height-1">
                    <strong class="text-dark font-weight-normal">সকল পঠিত</strong>
                    <span class="badge">23</span>
                  </aside>
                </a>
              </li>
            </ul>
          </div>

          <div class="tab-content flex-basis pScroll overflow-hidden">
            <div class="tab-pane active" id="home4" role="tabpanel">

              <table class="table table-striped table-bordered datatable table-sm">
                <thead>

                  <tr role="row" class="heading">
                    <th style="width: 5%" class="text-center sorting_disabled" rowspan="1" colspan="1">
                      ক্রম
                    </th>
                    <th class="text-center sorting_disabled" style="width: 20%" rowspan="1" colspan="1">
                      নথি নম্বর
                    </th>
                    <th class="text-center sorting_disabled" style="width: 25%" rowspan="1" colspan="1">
                      শিরোনাম
                    </th>
                    <th class="text-center sorting_disabled" style="width: 20%" rowspan="1" colspan="1">
                      সর্বশেষ নোটের তারিখ
                    </th>
                    <th class="text-center sorting_disabled" style="width: 20%" rowspan="1" colspan="1">
                      নথির শাখা
                    </th>
                    <th class="text-center sorting_disabled" style="width: 5%" rowspan="1" colspan="1">
                      বিস্তারিত
                    </th>
                  </tr>
                </thead>
                <tbody id="dbl_click"></tbody>
              </table>


            </div>
            <div class="tab-pane" id="profile4" role="tabpanel">2. Lorem ipsum dolor sit amet, consectetur adipisicing
              elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
              nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
              Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
              Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est
              laborum.
            </div>
            <div class="tab-pane" id="messages4" role="tabpanel">3. Lorem ipsum dolor sit amet, consectetur adipisicing
              elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
              nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
              Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
              Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est
              laborum.
            </div>
          </div>
        <?php } ?>
      </div>
      <?php
      if (!isset($page)) {
        ?>


        <div class="content-slide">

          <div class="card border-0 h-100 mb-0">
            <div class="card-header">
              <div class="align-items-center d-flex">
                <button type="button" class="btn btn-warning btn-show-hide">
                  <i class="fa fa-long-arrow-left"></i> আগত নথিতে ফেরৎ যান
                </button>
                <strong class="ml-3">ফাইলের মাধ্যমে জিঞ্জাসিত বিষয়গুলো সঙ্গায়ীত (ব্যবসা-বাণিজ্য শাখা
                  ) - <span>০১.০১.০১০২.০১৮.০২.০০১.১৯</span></strong>
              </div>

            </div>
            <div class="card-body overflow-hidden pScroll">
              <div class="alert alert-warning d-flex justify-content-between align-items-center" role="alert">
                <h3 class="mb-0">নিন্মোক্ত তালিকা হতে নোট সিলেক্ট করুন</h3>
                <div class="position-relative">
                  <i class="fa fa-angle-double-down animate" aria-hidden="true"></i>
                </div>
              </div>

              <table class="table table-striped table-bordered datatable table-sm">
                <thead>
                  <tr class="text-center">
                    <th class="text-center" style="width: 40%">বিষয়</th>
                    <th class="text-center" style="width: 20%">গ্রহণের সময়</th>
                    <th class="text-center" style="width: 30%">বর্তমান ডেস্ক</th>
                    <th class="text-center">বিস্তারিত</th>
                  </tr>
                </thead>
                <tbody id="table-nothi-notes-body"></tbody>
              </table>


            </div>
            <div class="card-footer text-right">
              <div class="btn-group btn-group-round">
                <button type="button" class="btn btn-warning">
                  <i class="fs1 a2i_gn_note2"></i> নতুন নোট
                </button>
                <a type="button" class="btn btn-primary btn-sokol" href="/tappware/nothi/nothiDetail/47/"><i class="fs1 a2i_gn_note1"></i> সকল নোট</a>
                <button type="button" class="btn btn-danger btn-show-hide" data-dismiss="modal"><i class="fs1 efile-close1"></i> বাতিল করুন
                </button>
              </div>
            </div>
          </div>

        </div>
      <?php } ?>
    </main>
    <aside class="aside-menu">
      <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item">
          <a class="nav-link active" data-toggle="tab" href="#timeline" role="tab">
            <i class="icon-list"></i>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-toggle="tab" href="#messages" role="tab">
            <i class="icon-speech"></i>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-toggle="tab" href="#settings" role="tab">
            <i class="icon-settings"></i>
          </a>
        </li>
      </ul>
      <!-- Tab panes-->
      <div class="tab-content">
        <div class="tab-pane active" id="timeline" role="tabpanel">
          <div class="list-group list-group-accent">
            <div class="list-group-item list-group-item-accent-secondary bg-light text-center font-weight-bold text-muted text-uppercase small">
              Today
            </div>
            <div class="list-group-item list-group-item-accent-warning list-group-item-divider">
              <div class="avatar float-right">
                <img class="img-avatar" src="<?= $this->request->webroot ?>coreui/img/avatars/7.jpg" alt="admin@bootstrapmaster.com">
              </div>
              <div>Meeting with
                <strong>Lucas</strong>
              </div>
              <small class="text-muted mr-3">
                <i class="icon-calendar"></i>  1 - 3pm
              </small>
              <small class="text-muted">
                <i class="icon-location-pin"></i>  Palo Alto, CA
              </small>
            </div>
            <div class="list-group-item list-group-item-accent-info">
              <div class="avatar float-right">
                <img class="img-avatar" src="<?= $this->request->webroot ?>coreui/img/avatars/4.jpg" alt="admin@bootstrapmaster.com">
              </div>
              <div>Skype with
                <strong>Megan</strong>
              </div>
              <small class="text-muted mr-3">
                <i class="icon-calendar"></i>  4 - 5pm
              </small>
              <small class="text-muted">
                <i class="icon-social-skype"></i>  On-line
              </small>
            </div>
            <div class="list-group-item list-group-item-accent-secondary bg-light text-center font-weight-bold text-muted text-uppercase small">
              Tomorrow
            </div>
            <div class="list-group-item list-group-item-accent-danger list-group-item-divider">
              <div>New UI Project -
                <strong>deadline</strong>
              </div>
              <small class="text-muted mr-3">
                <i class="icon-calendar"></i>  10 - 11pm
              </small>
              <small class="text-muted">
                <i class="icon-home"></i>  creativeLabs HQ
              </small>
              <div class="avatars-stack mt-2">
                <div class="avatar avatar-xs">
                  <img class="img-avatar" src="<?= $this->request->webroot ?>coreui/img/avatars/2.jpg" alt="admin@bootstrapmaster.com">
                </div>
                <div class="avatar avatar-xs">
                  <img class="img-avatar" src="<?= $this->request->webroot ?>coreui/img/avatars/3.jpg" alt="admin@bootstrapmaster.com">
                </div>
                <div class="avatar avatar-xs">
                  <img class="img-avatar" src="<?= $this->request->webroot ?>coreui/img/avatars/4.jpg" alt="admin@bootstrapmaster.com">
                </div>
                <div class="avatar avatar-xs">
                  <img class="img-avatar" src="<?= $this->request->webroot ?>coreui/img/avatars/5.jpg" alt="admin@bootstrapmaster.com">
                </div>
                <div class="avatar avatar-xs">
                  <img class="img-avatar" src="<?= $this->request->webroot ?>coreui/img/avatars/6.jpg" alt="admin@bootstrapmaster.com">
                </div>
              </div>
            </div>
            <div class="list-group-item list-group-item-accent-success list-group-item-divider">
              <div>
                <strong>#10 Startups.Garden</strong> Meetup
              </div>
              <small class="text-muted mr-3">
                <i class="icon-calendar"></i>  1 - 3pm
              </small>
              <small class="text-muted">
                <i class="icon-location-pin"></i>  Palo Alto, CA
              </small>
            </div>
            <div class="list-group-item list-group-item-accent-primary list-group-item-divider">
              <div>
                <strong>Team meeting</strong>
              </div>
              <small class="text-muted mr-3">
                <i class="icon-calendar"></i>  4 - 6pm
              </small>
              <small class="text-muted">
                <i class="icon-home"></i>  creativeLabs HQ
              </small>
              <div class="avatars-stack mt-2">
                <div class="avatar avatar-xs">
                  <img class="img-avatar" src="<?= $this->request->webroot ?>coreui/img/avatars/2.jpg" alt="admin@bootstrapmaster.com">
                </div>
                <div class="avatar avatar-xs">
                  <img class="img-avatar" src="<?= $this->request->webroot ?>coreui/img/avatars/3.jpg" alt="admin@bootstrapmaster.com">
                </div>
                <div class="avatar avatar-xs">
                  <img class="img-avatar" src="<?= $this->request->webroot ?>coreui/img/avatars/4.jpg" alt="admin@bootstrapmaster.com">
                </div>
                <div class="avatar avatar-xs">
                  <img class="img-avatar" src="<?= $this->request->webroot ?>coreui/img/avatars/5.jpg" alt="admin@bootstrapmaster.com">
                </div>
                <div class="avatar avatar-xs">
                  <img class="img-avatar" src="<?= $this->request->webroot ?>coreui/img/avatars/6.jpg" alt="admin@bootstrapmaster.com">
                </div>
                <div class="avatar avatar-xs">
                  <img class="img-avatar" src="<?= $this->request->webroot ?>coreui/img/avatars/7.jpg" alt="admin@bootstrapmaster.com">
                </div>
                <div class="avatar avatar-xs">
                  <img class="img-avatar" src="<?= $this->request->webroot ?>coreui/img/avatars/8.jpg" alt="admin@bootstrapmaster.com">
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="tab-pane p-3" id="messages" role="tabpanel">
          <div class="message">
            <div class="py-3 pb-5 mr-3 float-left">
              <div class="avatar">
                <img class="img-avatar" src="<?= $this->request->webroot ?>coreui/img/avatars/7.jpg" alt="admin@bootstrapmaster.com">
                <span class="avatar-status badge-success"></span>
              </div>
            </div>
            <div>
              <small class="text-muted">Lukasz Holeczek</small>
              <small class="text-muted float-right mt-1">1:52 PM</small>
            </div>
            <div class="text-truncate font-weight-bold">Lorem ipsum dolor sit amet</div>
            <small class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
              incididunt...
            </small>
          </div>
          <hr>
          <div class="message">
            <div class="py-3 pb-5 mr-3 float-left">
              <div class="avatar">
                <img class="img-avatar" src="<?= $this->request->webroot ?>coreui/img/avatars/7.jpg" alt="admin@bootstrapmaster.com">
                <span class="avatar-status badge-success"></span>
              </div>
            </div>
            <div>
              <small class="text-muted">Lukasz Holeczek</small>
              <small class="text-muted float-right mt-1">1:52 PM</small>
            </div>
            <div class="text-truncate font-weight-bold">Lorem ipsum dolor sit amet</div>
            <small class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
              incididunt...
            </small>
          </div>
          <hr>
          <div class="message">
            <div class="py-3 pb-5 mr-3 float-left">
              <div class="avatar">
                <img class="img-avatar" src="<?= $this->request->webroot ?>coreui/img/avatars/7.jpg" alt="admin@bootstrapmaster.com">
                <span class="avatar-status badge-success"></span>
              </div>
            </div>
            <div>
              <small class="text-muted">Lukasz Holeczek</small>
              <small class="text-muted float-right mt-1">1:52 PM</small>
            </div>
            <div class="text-truncate font-weight-bold">Lorem ipsum dolor sit amet</div>
            <small class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
              incididunt...
            </small>
          </div>
          <hr>
          <div class="message">
            <div class="py-3 pb-5 mr-3 float-left">
              <div class="avatar">
                <img class="img-avatar" src="<?= $this->request->webroot ?>coreui/img/avatars/7.jpg" alt="admin@bootstrapmaster.com">
                <span class="avatar-status badge-success"></span>
              </div>
            </div>
            <div>
              <small class="text-muted">Lukasz Holeczek</small>
              <small class="text-muted float-right mt-1">1:52 PM</small>
            </div>
            <div class="text-truncate font-weight-bold">Lorem ipsum dolor sit amet</div>
            <small class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
              incididunt...
            </small>
          </div>
          <hr>
          <div class="message">
            <div class="py-3 pb-5 mr-3 float-left">
              <div class="avatar">
                <img class="img-avatar" src="<?= $this->request->webroot ?>coreui/img/avatars/7.jpg" alt="admin@bootstrapmaster.com">
                <span class="avatar-status badge-success"></span>
              </div>
            </div>
            <div>
              <small class="text-muted">Lukasz Holeczek</small>
              <small class="text-muted float-right mt-1">1:52 PM</small>
            </div>
            <div class="text-truncate font-weight-bold">Lorem ipsum dolor sit amet</div>
            <small class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
              incididunt...
            </small>
          </div>
        </div>
        <div class="tab-pane p-3" id="settings" role="tabpanel">
          <h6>Settings</h6>
          <div class="aside-options">
            <div class="clearfix mt-4">
              <small>
                <b>Option 1</b>
              </small>
              <label class="switch switch-label switch-pill switch-success switch-sm float-right">
                <input class="switch-input" type="checkbox" checked="">
                <span class="switch-slider" data-checked="On" data-unchecked="Off"></span>
              </label>
            </div>
            <div>
              <small class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                incididunt ut labore et dolore magna aliqua.
              </small>
            </div>
          </div>
          <div class="aside-options">
            <div class="clearfix mt-3">
              <small>
                <b>Option 2</b>
              </small>
              <label class="switch switch-label switch-pill switch-success switch-sm float-right">
                <input class="switch-input" type="checkbox">
                <span class="switch-slider" data-checked="On" data-unchecked="Off"></span>
              </label>
            </div>
            <div>
              <small class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                incididunt ut labore et dolore magna aliqua.
              </small>
            </div>
          </div>
          <div class="aside-options">
            <div class="clearfix mt-3">
              <small>
                <b>Option 3</b>
              </small>
              <label class="switch switch-label switch-pill switch-success switch-sm float-right">
                <input class="switch-input" type="checkbox">
                <span class="switch-slider" data-checked="On" data-unchecked="Off"></span>
              </label>
            </div>
          </div>
          <div class="aside-options">
            <div class="clearfix mt-3">
              <small>
                <b>Option 4</b>
              </small>
              <label class="switch switch-label switch-pill switch-success switch-sm float-right">
                <input class="switch-input" type="checkbox" checked="">
                <span class="switch-slider" data-checked="On" data-unchecked="Off"></span>
              </label>
            </div>
          </div>
          <hr>
          <h6>System Utilization</h6>
          <div class="text-uppercase mb-1 mt-4">
            <small>
              <b>CPU Usage</b>
            </small>
          </div>
          <div class="progress progress-xs">
            <div class="progress-bar bg-info" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
          </div>
          <small class="text-muted">348 Processes. 1/4 Cores.</small>
          <div class="text-uppercase mb-1 mt-2">
            <small>
              <b>Memory Usage</b>
            </small>
          </div>
          <div class="progress progress-xs">
            <div class="progress-bar bg-warning" role="progressbar" style="width: 70%" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"></div>
          </div>
          <small class="text-muted">11444GB/16384MB</small>
          <div class="text-uppercase mb-1 mt-2">
            <small>
              <b>SSD 1 Usage</b>
            </small>
          </div>
          <div class="progress progress-xs">
            <div class="progress-bar bg-danger" role="progressbar" style="width: 95%" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100"></div>
          </div>
          <small class="text-muted">243GB/256GB</small>
          <div class="text-uppercase mb-1 mt-2">
            <small>
              <b>SSD 2 Usage</b>
            </small>
          </div>
          <div class="progress progress-xs">
            <div class="progress-bar bg-success" role="progressbar" style="width: 10%" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"></div>
          </div>
          <small class="text-muted">25GB/256GB</small>
        </div>
      </div>
    </aside>
  </div>
  <footer class="app-footer">
    <div>
      <span>কপিরাইট ২০১৯, একসেস টু ইনফরমেশন</span>
    </div>
    <div class="ml-auto d-flex">
      <span>পার্টনার: </span>
      <div class="partners d-flex">
        <a href="#" title="bangladehs"><img src="<?= $this->request->webroot ?>coreui/img/partners/ban-gov_logo.png" alt="bangladehs"></a>
        <a href="#" title="a2i"><img src="<?= $this->request->webroot ?>coreui/img/partners/a_a2i-logo.jpg" alt="a2i"></a>
        <a href="#" title="BCC"><img src="<?= $this->request->webroot ?>coreui/img/partners/bcc_logo.png" alt="BCC"></a>
        <a href="#" title="DOICT"><img src="<?= $this->request->webroot ?>coreui/img/partners/doict_logo.jpg" alt="DOICT"></a>
        <a href="#" title="TAPPWARE"><img src="<?= $this->request->webroot ?>coreui/img/partners/tappware_logo.png" alt="tappware"></a>
      </div>
    </div>
  </footer>


  <script src="<?= $this->request->webroot ?>coreui/js/react.production.min.js" crossorigin></script>
  <script src="<?= $this->request->webroot ?>coreui/js/react-dom.production.min.js" crossorigin></script>
  <script src="<?= $this->request->webroot ?>coreui/js/custom-script.js" crossorigin></script>


  <script>
    fetch('<?= $this->request->webroot?>NothiMasterMovements/getAllNothiV2').then(response => {
      return response.json();
    }).then(data => {
      if (data.status == "success") {
        let nothi_list = data.data;
        console.log("Nothi data : ", nothi_list);
        let table_body = $('#dbl_click');
        table_body.find('tr').remove();
        nothi_list.forEach(function(value) {
          let tr_tmp = "<tr role=row class=even><td style='font-family:nikosh'>" + value.id + "</td><td>" + value.NothiMasters.nothi_no +
            "</td><td>" + value.NothiMasters.subject + "</td><td>" + value.created +
            "</td><td>" + value.from_office_unit_name +
            "</td><td><div class='text-center'><a class='btn btn-success btn-sm btn-show-hide' data-nothi-master-id=" + value.nothi_master_id +
            " href='javascript;'><i class='fa fa-search-plus'></i></a></div></td></tr>";
          table_body.append(tr_tmp);
          // console.log(value.id);
        });

      }
    }).catch(err => {
      console.log('Error: ', err);
    });
  </script>

</body>

</html>
