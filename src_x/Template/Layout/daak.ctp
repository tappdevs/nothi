<!DOCTYPE html>
<html lang="en">

<head>
  <base href="./">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
  <meta name="description" content="CoreUI - Open Source Bootstrap Admin Template">
  <meta name="author" content="Łukasz Holeczek">
  <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,jQuery,CSS,HTML,RWD,Dashboard">
  <title>নথি | অফিস ব্যবস্থাপনা</title>
  <!-- Icons-->
  <link href="<?= $this->request->webroot ?>coreui/css/font-icons.css" rel="stylesheet">
  <link href="<?= $this->request->webroot ?>coreui/node_modules/@coreui/icons/css/coreui-icons.min.css" rel="stylesheet">
  <link href="<?= $this->request->webroot ?>coreui/node_modules/flag-icon-css/css/flag-icon.min.css" rel="stylesheet">
  <link href="<?= $this->request->webroot ?>coreui/node_modules/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="<?= $this->request->webroot ?>coreui/node_modules/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">
  <link href="<?= $this->request->webroot ?>coreui/vendors/select2/dist/css/select2.min.css" rel="stylesheet">
  <link href="<?= $this->request->webroot ?>coreui/node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css"
      rel="stylesheet">
  <link href="<?= $this->request->webroot ?>coreui/node_modules/datatables.net-bs4/css/dataTables.bootstrap4.css" rel="stylesheet">
  <!-- Include Editor style. -->
  <link href='<?= $this->request->webroot ?>coreui/node_modules/froala-editor/css/froala_editor.min.css' rel='stylesheet'
      type='text/css'/>
  <link href='<?= $this->request->webroot ?>coreui/node_modules/froala-editor/css/froala_style.min.css' rel='stylesheet' type='text/css'/>


  <!-- Main styles for this application-->
  <link href="<?= $this->request->webroot ?>coreui/css/style.genius.css" rel="stylesheet">
  <link href="<?= $this->request->webroot ?>coreui/vendors/pace-progress/css/pace.min.css" rel="stylesheet">
  <link href="<?= $this->request->webroot ?>coreui/css/nothi_style.css" rel="stylesheet">

  <!-- Bootstrap and necessary plugins-->
  <script src="<?= $this->request->webroot ?>coreui/node_modules/jquery/dist/jquery.min.js"></script>
  <script src="<?= $this->request->webroot ?>coreui/node_modules/popper.js/dist/umd/popper.min.js"></script>
  <script src="<?= $this->request->webroot ?>coreui/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>


  <script src="<?= $this->request->webroot ?>coreui/node_modules/pace-progress/pace.min.js"></script>
  <script src="<?= $this->request->webroot ?>coreui/node_modules/perfect-scrollbar/dist/perfect-scrollbar.min.js"></script>
  <script src="<?= $this->request->webroot ?>coreui/node_modules/@coreui/coreui-pro/dist/js/coreui.min.js"></script>
  <script src="<?= $this->request->webroot ?>coreui/vendors/select2/dist/js/select2.min.js"></script>
  <script src="<?= $this->request->webroot ?>coreui/node_modules/moment/min/moment.min.js"></script>
  <script src="<?= $this->request->webroot ?>coreui/node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>


  <script src="<?= $this->request->webroot ?>coreui/node_modules/datatables.net/js/jquery.dataTables.js"></script>
  <script src="<?= $this->request->webroot ?>coreui/node_modules/datatables.net-bs4/js/dataTables.bootstrap4.js"></script>
  <!-- Include JS file. -->

</head>

<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show font-bn-en">
<header class="app-header navbar">
  <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
    <span class="navbar-toggler-icon"></span>
  </button>
  <a class="navbar-brand py-1" href="#">
    <img class="navbar-brand-full h-100" src="<?= $this->request->webroot ?>coreui/img/logo.png" alt="CoreUI Logo">
    <img class="navbar-brand-minimized" src="<?= $this->request->webroot ?>coreui/img/logo.png" alt="CoreUI Logo">
  </a>
  <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
    <span class="navbar-toggler-icon"></span>
  </button>

  <ul class="nav navbar-nav">
    <li class="nav-item">
      <div class="btn-group">
        <button class="btn btn-success-n text-white fs-18 border-right-n" title="ডাক">
          <i class="fa fa-envelope-o"></i> ডাক
          <small class="badge badge-light text-success-n font-weight-normal">23</small>
        </button>
        <button class="btn btn-success-n text-white fs-18 border-left-n border-right-n active" title="নথি">
          <i class="fs0 efile-nothi1 mr-1"></i> নথি
          <small class="badge badge-light text-success-n font-weight-normal">23</small>
        </button>
        <button class="btn btn-success-n text-white fs-18 border-left-n border-right-n" title="দপ্তর">
          <i class="fa fa-folder-open mr-1"></i> দপ্তর
        </button>
        <button class="btn btn-success-n text-white fs-18 border-left-n" title="রিপোর্ট">
          <i class="fa fa-folder-open mr-1"></i>রিপোর্ট
          <small class="badge badge-light text-success-n font-weight-normal">23</small>
        </button>
      </div>
    </li>
  </ul>

  <ul class="nav navbar-nav ml-auto">

    <li class="nav-item">
      <div class="user_position font-bn-en dropdown line-height-1 pr-3 fs-16">
        <button class="dropdown-toggle text-light shadow-none btn-transparent text-left outline-none" id="user_position"
            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> কোহিনূর বেগম
          <small>(অফিস সহকারী, ব্যবসা-বাণিজ্য শাখা)</small>
          <br>
          জেলা প্রশাসকের কার্যালয়, জামালপুর
        </button>
        <div class="dropdown-menu dropdown-menu-right fs-16" aria-labelledby="user_position">
          <a class="dropdown-item" href="#"><i class="fa fa-mobile-phone"></i> নথি মোবাইল অ্যাপ </a>
          <a class="dropdown-item" href="#"><i class="fa fa-user"></i> প্রোফাইল ব্যবস্থাপনা </a>
          <a class="dropdown-item" href="#"><i class="fa fa-sign-out"></i> লগ আউট করুন </a>
        </div>
      </div>
    </li>

    <li class="nav-item dropdown">
      <a class="nav-link nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true"
          aria-expanded="false">
        <img class="img-avatar" src="<?= $this->request->webroot ?>coreui/img/avatars/6.jpg" alt="admin@bootstrapmaster.com">
      </a>
      <div class="dropdown-menu dropdown-menu-right">
        <div class="dropdown-header text-center">
          <strong>Account</strong>
        </div>
        <a class="dropdown-item" href="#">
          <i class="fa fa-bell-o"></i> Updates
          <span class="badge badge-info">42</span>
        </a>
        <a class="dropdown-item" href="#">
          <i class="fa fa-envelope-o"></i> Messages
          <span class="badge badge-success">42</span>
        </a>
        <a class="dropdown-item" href="#">
          <i class="fa fa-tasks"></i> Tasks
          <span class="badge badge-danger">42</span>
        </a>
        <a class="dropdown-item" href="#">
          <i class="fa fa-comments"></i> Comments
          <span class="badge badge-warning">42</span>
        </a>
        <div class="dropdown-header text-center">
          <strong>Settings</strong>
        </div>
        <a class="dropdown-item" href="#">
          <i class="fa fa-user"></i> Profile</a>
        <a class="dropdown-item" href="#">
          <i class="fa fa-wrench"></i> Settings</a>
        <a class="dropdown-item" href="#">
          <i class="fa fa-usd"></i> Payments
          <span class="badge badge-dark">42</span>
        </a>
        <a class="dropdown-item" href="#">
          <i class="fa fa-file"></i> Projects
          <span class="badge badge-primary">42</span>
        </a>
        <div class="divider"></div>
        <a class="dropdown-item" href="#">
          <i class="fa fa-shield"></i> Lock Account</a>
        <a class="dropdown-item" href="#">
          <i class="fa fa-lock"></i> Logout</a>
      </div>
    </li>
  </ul>
</header>
<div class="app-body d-flex flex-column h-100 overflow-hidden">
  <?= $this->element('Daak/daak_navigation'); ?>
  <main class="main d-flex flex-column flex-fill position-relative h-100">
    <!-- Breadcrumb-->
    <?php if (!isset($page)) { ?>
      <?= $this->element('Daak/daak_search'); ?>
    <?php } ?>
    <div id="mainContent"
        class="animated mainContent fadeIn flex-basis border-top  bg-white overflow-hidden flex-column d-flex">

      <?php
      if (isset($page) & $page == 'daak_view') {
        echo $this->element('Daak/daak_view');
      } elseif (isset($page) & $page == 'daak_upload_daptorik') {
        echo $this->element('Daak/daak_upload_daptorik');
      } elseif (isset($page) & $page == 'daak_upload_nagorik') {
        echo $this->element('Daak/daak_upload_nagorik');
      } elseif (isset($page) & $page == 'daak_khoshra') {
        echo $this->element('Daak/daak_khoshra');
      } else {
        echo $this->element('Daak/DaakBox/daak_box');
      }
      ?>


    </div>

  </main>
  <?= $this->element('Daak/right_sidebar'); ?>

</div>
<footer class="app-footer">
  <div>
    <span>কপিরাইট ২০১৯, একসেস টু ইনফরমেশন</span>
  </div>
  <div class="ml-auto d-flex">
    <span>পার্টনার: </span>
    <div class="partners d-flex">
      <a href="#" title="bangladehs"><img src="<?= $this->request->webroot ?>coreui/img/partners/ban-gov_logo.png" alt="bangladehs"></a>
      <a href="#" title="a2i"><img src="<?= $this->request->webroot ?>coreui/img/partners/a_a2i-logo.jpg" alt="a2i"></a>
      <a href="#" title="BCC"><img src="<?= $this->request->webroot ?>coreui/img/partners/bcc_logo.png" alt="BCC"></a>
      <a href="#" title="DOICT"><img src="<?= $this->request->webroot ?>coreui/img/partners/doict_logo.jpg" alt="DOICT"></a>
      <a href="#" title="TAPPWARE"><img src="<?= $this->request->webroot ?>coreui/img/partners/tappware_logo.png" alt="tappware"></a>
    </div>
  </div>
</footer>

<script src="<?= $this->request->webroot ?>coreui/js/datatables.js"></script>

<!--
<script src="<?= $this->request->webroot ?>coreui/js/react.production.min.js" crossorigin></script>
<script src="<?= $this->request->webroot ?>coreui/js/react-dom.production.min.js" crossorigin></script>e
-->
<script src="<?= $this->request->webroot ?>coreui/js/custom-script.js" crossorigin></script>

</body>

</html>
