<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.2
Version: 3.6.2
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->

<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title>নথি | অফিস ব্যবস্থাপনা</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    <meta content="#683091" name="theme-color"/>
    <meta content="#683091" name="msapplication-navbutton-color"/>
    <meta content="#683091" name="apple-mobile-web-app-status-bar-style"/>

    <?= $this->element('load_js_css_head') ?>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
<!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
<!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->
<!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->
<!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->
<!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->
<!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->

<body class="page-header-fixed-mobile page-footer-fixed-mobile page-header-fixed page-footer-fixed ">
<?= $this->element('top_bar') ?>

<div class="clearfix"></div>
<!-- BEGIN CONTAINER -->
<div class="page-container">

    <?= $this->element('left_bar') ?>
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="row" style="margin:0px !important;">
                <div class="col-md-12" style="padding:5px !important;">
                    <?= $this->Flash->render() ?>
                    <div id="ajax-content">
                        <?php echo $this->fetch('content'); ?>
                    </div>
                </div>
            </div>

	        <style>
		        .page-quick-sidebar-wrapper .page-quick-sidebar .list-items > li:hover {
			        background: #eaeaea;
			        color: black;
		        }
	        </style>
	        <a href="javascript:;" class="page-quick-sidebar-toggler hidden" style="background: transparent !important;"><i class="icon-login"></i></a>
	        <div class="page-quick-sidebar-wrapper" style="background: #ffffff;border: solid #bb71d0 1px;top:75px">
		        <div class="page-quick-sidebar" style="background: #ffffff;">
			        <div class="nav-justified">
				        <ul class="nav nav-tabs nav-justified">
					        <li class="active">
						        <a href="#quick_sidebar_tab_1" data-toggle="tab" style="padding-top: 15px;">
							        অপঠিত বার্তা
						        </a>
					        </li>
					        <li>
						        <a href="#quick_sidebar_tab_2" data-toggle="tab" style="padding-top: 15px;">
							        পঠিত বার্তা
						        </a>
					        </li>
				        </ul>
				        <div class="tab-content">
					        <div class="tab-pane active page-quick-sidebar-chat" id="quick_sidebar_tab_1" style="overflow: auto;height: calc(100vh - 173px);">
						        <ul class="feeds list-items">
							        <?php foreach($global_messages['unread'] as $key => $global_message): ?>
							        <li onclick="viewGlobalMessage(this, '<?=$global_message['id']?>')">
								        <div class="row">
									        <div class="col-md-12">
										        <h3 class="title" style="font-weight: bold;"><?=$global_message['title']?></h3>
										        <div class="message" style="display: -webkit-box;-webkit-line-clamp: 3;-webkit-box-orient: vertical;overflow: hidden;text-overflow: ellipsis;"><?=$global_message['message']?></div>
										        <div style="font-size: 14px;font-style: italic;color: #333a3e;"><?=$global_message['created']?></div>
									        </div>
								        </div>
							        </li>
							        <?php endforeach; ?>
						        </ul>
					        </div>
					        <div class="tab-pane page-quick-sidebar-alerts" id="quick_sidebar_tab_2" style="overflow: auto;height: calc(100vh - 173px);">
						        <ul class="feeds list-items">
									<?php foreach($global_messages['read'] as $key => $global_message): ?>
								        <li onclick="viewGlobalMessage(this, '<?=$global_message['id']?>')">
									        <div class="row">
										        <div class="col-md-12">
											        <h3 class="title" style="font-weight: bold;"><?=$global_message['title']?></h3>
											        <div class="message" style="display: -webkit-box;-webkit-line-clamp: 3;-webkit-box-orient: vertical;overflow: hidden;text-overflow: ellipsis;"><?=$global_message['message']?></div>
											        <div style="font-size: 14px;font-style: italic;color: #333a3e;"><?=$global_message['created']?></div>
										        </div>
									        </div>
								        </li>
									<?php endforeach; ?>
						        </ul>
					        </div>
				        </div>
			        </div>
		        </div>
	        </div>
        </div>
    </div>
    <!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<style>
    .page-footer *{
        font-size:11pt;
        vertical-align: middle;
    }
    .page-footer-fixed .page-footer
    {
        border: none;
        border-top: 2px solid #8dc641;
        border-bottom: 2px solid #8dc641;
        background: #fff;
    }
    .page-footer img{
        vertical-align: sub;
    }
     .col-12,.col-6,.col-3{
         padding-right:15px;
         padding-left:15px;
     }
    .col-12{
        flex: 0 0 100%;
        max-width: 100%;
    }
    .col-6{
        flex: 0 0 50%;
        max-width: 50%;
    }

    .col-3{
        flex: 0 0 25%;
        max-width: 25%;
    }

    /*image on select option*/
    .photo_icon_div {
	    height: 65px;
	    float:left;
    }
    .photo_icon {
	    width: 25px;
	    height: 25px;
	    margin-right:5px;
	    border: solid 1px silver;
	    border-radius: 5px !important;
    }
    .text_div {}
    /*image on select option END*/
</style>
<div class="page-footer">
    <div class="page-footer-inner text-center pull-left">
        <a href="http://www.bangladesh.gov.bd" target="_tab">
            <img src="<?= $this->request->webroot ?>img/ban-gov_logo.png" style="height: 24px;">
        </a>
        <a href="http://a2i.gov.bd" target="_tab">
            <img src="<?= $this->request->webroot ?>img/a_a2i-logo.jpg" style="height: 24px;">
        </a>
        <a href="http://www.bcc.gov.bd" target="_tab">
            <img src="<?= $this->request->webroot ?>img/bcc_logo.png" style="height: 24px;">
        </a>
        <a href="http://www.doict.gov.bd" target="_tab">
            <img src="<?= $this->request->webroot ?>img/doict_logo.jpg" style="height: 24px;">
        </a>
        <a href="http://tappware.com" target="_tab">
            <img src="<?= $this->request->webroot ?>img/tappware_logo.png" style="height: 24px;">
        </a>
    </div>
    <div class="page-footer-inner text-center pull-right">
        <img style="height: 24px;" src="<?php echo $this->request->webroot; ?>assets/admin/layout4/img/a2i.png" alt=""> কপিরাইট <?= $Year ?>, একসেস টু ইনফরমেশন
    </div>
</div>

<?= $this->element('load_js_css_body') ?>
<script>
	$(document).ready(function() {
        $('#dashboard-report-range').parent().prepend('<div style="float: left;border: solid 1px #eaeaea;padding: 6px 8px 4px;border-radius: 5px 0 0 5px !important;background: #eaeaea;">সময়সীমা</div>');
        $('#dashboard-report-range').css('cssText', 'border-radius: 0 5px 5px 0 !important;border: solid 1px #eaeaea;');
	});

    function bootboxCloseButton(element) {
        $(element).closest('.modal-content').find('.modal-footer').find('[data-bb-handler=danger]').trigger('click');
    };


    // image on select option
    function dropdownWithPhoto(selectElement) {
        $('style').append('.select2-results li {border: solid 1px silver !important;}');
        function format(state) {
            var html = $(state.element[0]).html();
            if (!state.id) return html; // optgroup

            var selectionText = html.replace(/~/g, '<br/>');
            var imageUrl = $(state.element[0]).attr('data-imageUrl');
            return "<div class='photo_icon_div'><img class='photo_icon' src='" + imageUrl + "'/></div><div class='text_div'>" + selectionText + "</div>";
        }

        $(selectElement).select2({
            formatResult: format,
            formatSelection: format,
            escapeMarkup: function (m) {
                return m;
            }
        });

        $(selectElement).on('change', function (e) {
            $(".select2-chosen").find('.text_div').html($(".select2-chosen").find('.text_div').html().replace(/<br>/g, ', '));
        });
    }
    // image on select option END
</script>
</body>
<!-- END BODY -->
</html>