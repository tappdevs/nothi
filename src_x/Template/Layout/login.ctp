<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.2
Version: 3.3.0
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title>নথি | অফিস ব্যবস্থাপনা</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta content="" name="description"/>
    <meta content="" name="author"/>

    <?php
    $path = $this->request->webroot;
    if (CDN == 1) {
        $path = 'http://cdn1.nothi.gov.bd/webroot/';
    }

    ?>


    <link rel="stylesheet" href="<?php echo $this->request->webroot; ?>a2i/demo-files/demo.css">
    <link rel="stylesheet" href="<?php echo $this->request->webroot; ?>a2i/style.css">

    <link href="<?php echo $path; ?>assets/global/plugins/bootstrap/css/bootstrap.min.css"
          rel="stylesheet" type="text/css"/>
    <link href="<?php echo $path; ?>assets/global/plugins/uniform/css/uniform.default.css"
          rel="stylesheet" type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="<?php echo $path; ?>assets/admin/pages/css/login-soft.css" rel="stylesheet"
          type="text/css"/>
    <!-- END PAGE LEVEL SCRIPTS -->
    <!-- BEGIN THEME STYLES -->
    <link href="<?php echo $path; ?>assets/global/css/components-rounded.css" id="style_components"
          rel="stylesheet" type="text/css"/>
    <link href="<?php echo CDN_PATH; ?>assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo $path; ?>assets/admin/layout4/css/layout.css" rel="stylesheet"
          type="text/css"/>
    <script src="<?php echo $path; ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>

    <script src="<?php echo $path; ?>assets/global/plugins/bootstrap-toastr/toastr.min.js"></script>

    <!-- END THEME STYLES -->
    <link rel="shortcut icon" href="<?php echo $path; ?>favicon.ico"/>

    <link rel="stylesheet" type="text/css"
          href="<?php echo $path; ?>assets/global/plugins/bootstrap-toastr/toastr.min.css"/>
    <style>
        .message {
            word-wrap: break-word;
        }

        .error-message {
            color: red;
        }

        .login {

            background: url(<?= $path; ?>img/bg_image.jpg) no-repeat center center #CDE7A8 fixed;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }

        [name=optionsRadios] {
            opacity: 1 !important;
            margin-left: 0 !important;
        }
        * {
            font-size: 13pt;
        }
    </style>

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->

<body class="login" style="font-family: Nikosh!important;font-size: 12pt!important;">
<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
<div class="menu-toggler sidebar-toggler">
</div>
<!-- END SIDEBAR TOGGLER BUTTON -->
<!-- BEGIN LOGO -->
<style>
    .modal-content {
        /*background-color: #8ab563 !important;*/
        background: url(/nothi_new/img/bg_image.jpg);
        background-size: 100% 100%;
    }

    .panel-primary {
        border-color: #596c47 !important;
    }

    .panel-primary > .panel-heading {
        background-color: #596c47 !important;
    }

    .panel-primary > .panel-heading + .panel-collapse .panel-body {
        border-top-color: #596c47 !important;
    }

    /* RIBBON */
    .ribbon {
        font-size: 16px !important;
        /* This ribbon is based on a 16px font side and a 24px vertical rhythm. I've used em's to position each element for scalability. If you want to use a different font size you may have to play with the position of the ribbon elements */

        /*width: 50%;*/

        position: relative;
        background: #804f7c;
        color: #fff;
        text-align: center;
        padding: 1em 2em; /* Adjust to suit */
        margin: 0em auto 3em; /* Based on 24px vertical rhythm. 48px bottom margin - normally 24 but the ribbon 'graphics' take up 24px themselves so we double it. */
    }
    .ribbon:before, .ribbon:after {
        content: "";
        position: absolute;
        display: block;
        bottom: -1em;
        border: 1.5em solid #986794;
        z-index: -1;
    }
    .ribbon:before {
        left: -2em;
        border-right-width: 1.5em;
        border-left-color: transparent;
    }
    .ribbon:after {
        right: -2em;
        border-left-width: 1.5em;
        border-right-color: transparent;
    }
    .ribbon .ribbon-content:before, .ribbon .ribbon-content:after {
        content: "";
        position: absolute;
        display: block;
        border-style: solid;
        border-color: #543752 transparent transparent transparent;
        bottom: -1em;
    }
    .ribbon .ribbon-content:before {
        left: 0;
        border-width: 1em 0 0 1em;
    }
    .ribbon .ribbon-content:after {
        right: 0;
        border-width: 1em 1em 0 0;
    }
    /* RIBBON */
</style>
<?= $this->element('ssl_message_and_mobile_detection'); ?>
<div class="logo" style="color:#000; ">
    <img style="width: 200px" src="<?php echo $path; ?>img/nothi_logo_login.png" alt=""/>
</div>
<div class="text-center font-lg" id="SSLMessageForAndroid" hidden>

    <a class="btn btn-primary" href="https://play.google.com/store/apps/details?id=com.tappware.nothi&hl=en">
        ডাউনলোড করুন নথির অ্যান্ড্রয়েড অ্যাপ
    </a>
</div>
<div class="text-center font-lg" id="SSLMessageForIOS" hidden>

    <a class="btn btn-primary" href="https://itunes.apple.com/us/app/id1187955540">
        ডাউনলোড করুন নথির আই ও এস অ্যাপ
    </a>
</div>
<!-- END LOGO -->
<!-- BEGIN LOGIN -->
<div class="content" style="width: 420px!important;">
    <!--    <p class="alert alert-info">নথির নতুন আপডেট এর কারনে অনুগ্রহ করে ব্রাউজারের কেশ পরিষ্কার করুন</p>-->
    <?php echo $this->Flash->render(); ?>
    <?php echo $this->element('form_login') ?>


    <br/><br/>
    <div class="create-help">

        <h4 class=" text-center"><a href="<?php
            echo $this->Url->build(['controller' => 'DakNagoriks',
                'action' => 'NagorikAbedon'])
            ?>" class="btn-link" style="text-decoration: none;"><i class="efile-citizen1"></i>&nbsp;নাগরিক কর্নার&nbsp;</a>
        </h4>
        <hr style="border-color:#D9EDF7">

        <div class="form-group font-blue-madison">
            <span class="caption-subject bold"><i class="efile-help4"></i>&nbsp;হেল্প ডেস্ক</span>
        </div>
        <!--<h4 class="text-center">হেল্প ডেস্ক </h4>-->
        <div class="row">
            <div class="col-sm-7 col-md-7 col-xs-7">
                <div class="form-group ">
                    <i class="glyphicon glyphicon-phone"></i> +৮৮ ০১৭১২-৪৮৫৭৪৭
                    <br/>
                    <i class="glyphicon glyphicon-phone"></i> +৮৮ ০১৭৫২-০২৬৫২১
                    <br/>
                    <span class="glyphicon glyphicon-envelope"></span> <a href="mailto:support@nothi.org.bd" style="
font-size: 14px">support@nothi.org.bd</a><br/>
                    <small style="font-size: .7em; vertical-align: bottom!important;">
                        <img src="<?= $this->request->webroot ?>img/img_tapp_logo.png" style="height: 18px;">
                        Maintenance by <a style="vertical-align: bottom!important;" href="http://tappware.com"
                                          target="_tab">Tappware</a>
                    </small>
                </div>
            </div>
            <div class="col-sm-5 col-md-5 col-xs-5">
                <!--            <a data-toggle="tooltip" title=" নথি অ্যান্ড্রয়েড অ্যাপ " href='<?= $this->request->webroot ?>downloadApp'>
                <img src="<?= CDN_PATH ?>img/nothi-android-dl.png" class="img-rounded img-responsive"
                     style="margin: 0 auto;"/> </a>-->
                <div class="form-group ">
                    <icon class="glyphicon glyphicon-bullhorn"></icon>
                    <a target="__tab" class="btn-link bold font-lg" style="text-decoration: none;"
                       href="<?= \Cake\Routing\Router::url(['_name' => 'release-note']); ?>">আপডেট </a><br/>
                    <icon class="glyphicon glyphicon-info-sign"></icon>
                    <a target="__tab" class="btn-link bold font-lg" style="text-decoration: none;"
                       href="<?= $this->request->webroot ?>FAQ">আপনার জিজ্ঞাসা</a><br/>
                    <icon class="glyphicon glyphicon-play-circle"></icon>
                    <a target="__tab" class="btn-link bold font-lg" style="text-decoration: none;"
                       href="https://www.youtube.com/watch?v=hmUUBt7G8Gs&amp;list=PLVCEN663EzMFwATrFC6ULiM64U1FRyeAu">ভিডিও
                        টিউটোরিয়াল</a><br/>
                    <img src="<?= $this->request->webroot ?>img/fb.png" style="margin-left:-2px;"/>
                    <a target="__tab" class="btn-link bold font-lg" style="text-decoration: none;"
                       href="https://fb.com/groups/nothi/">নথি (ফেসবুক গ্রুপ)</a>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>

    </div>
</div>
<br/>
<div class="content" style="padding-top: 20px!important;width: 420px!important;">
    <div class="create-help1">
        <p>
            <a target="__tab"
               href="<?= $this->Url->build(['_name' => 'showUserManualList']) ?>"> ব্যবহার
                সহায়িকা (ভার্সনঃ ১৫)</a>
        </p>
    </div>
    <div class="logobd" align="left">
        <a href="http://bangladesh.gov.bd" target="_blank">
            <img src="<?php echo $path ?>assets/admin/layout4/img/bd.png" alt="">
        </a>
    </div>
    <div class="logoa2i" align="right">
        <a href="http://www.a2i.gov.bd" target="_blank">
            <img src="<?php echo $path ?>assets/admin/layout4/img/a2i.png" alt="">
        </a>
    </div>
</div>
<br/>
<div class="page-footer">
    <div class="page-footer-inner text-center pull-left">
        <a href="http://www.bangladesh.gov.bd" target="_tab">
            <img src="<?= $this->request->webroot ?>img/ban-gov_logo.png" style="height: 24px;">
        </a>
        <a href="http://a2i.gov.bd" target="_tab">
            <img src="<?= $this->request->webroot ?>img/a_a2i-logo.jpg" style="height: 24px;">
        </a>
        <a href="http://www.bcc.gov.bd" target="_tab">
            <img src="<?= $this->request->webroot ?>img/bcc_logo.png" style="height: 24px;">
        </a>
        <a href="http://www.doict.gov.bd" target="_tab">
            <img src="<?= $this->request->webroot ?>img/doict_logo.jpg" style="height: 24px;">
        </a>
        <a href="http://tappware.com" target="_tab">
            <img src="<?= $this->request->webroot ?>img/tappware_logo.png" style="height: 24px;">
        </a>
    </div>
    <div class="page-footer-inner text-center pull-right">
        <img style="height: 24px;" src="<?php echo $this->request->webroot; ?>assets/admin/layout4/img/a2i.png" alt=""> কপিরাইট <?= $Year ?>, একসেস টু ইনফরমেশন
    </div>
</div>
<!-- END LOGIN -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="<?php echo $path; ?>assets/global/plugins/respond.min.js"></script>
<script src="<?php echo $path; ?>assets/global/plugins/excanvas.min.js"></script>
<![endif]-->

<script src="<?php echo $path; ?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo $path; ?>assets/global/plugins/jquery-validation/js/jquery.validate.min.js"
        type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script type="text/javascript">
    jQuery(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip({'placement':'bottom'});
        handleForgetPassword();
    });

    var handleForgetPassword = function () {
        $('.forget-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                username: {
                    required: true,
                }
            },
            messages: {
                username: {
                    required: "লগইন আইডি দেয়া হয়নি"
                }
            },
            invalidHandler: function (event, validator) { //display error alert on form submit

            },
            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },
            errorPlacement: function (error, element) {
                error.insertAfter(element.closest('.input-icon'));
            },
            submitHandler: function (form) {
                $.ajax({
                    url: '<?php
                        echo $this->Url->build(['controller' => 'Users',
                            'action' => 'forgetPassword'])
                        ?>',
                    data: $('.forget-form').serialize(),
                    method: 'post',
                    cache: false,
                    dataType: 'JSON',
                    success: function (response) {
                        $('.forget-form').find('.form-actions').show();
                        $('.message').hide();
                        toastr.options = {
                            "closeButton": true,
                            "debug": false,
                            "positionClass": "toast-bottom-right"
                        };
                        if (response.status == 'error') {
                            toastr.error(response.msg);
                        } else {
                            toastr.success(response.msg);
                        }
                    }
                })
            }
        });


    }

    $('.forget-form input').keypress(function (e) {
        if (e.which == 13) {
            if ($('.forget-form').validate().form()) {
                $('.forget-form').find('.form-actions').hide();
                $('.forget-form').submit();
            }
            return false;
        }
    });


    jQuery('#forget-password').click(function () {
        jQuery('.login-form').hide();
        jQuery('.forget-password').hide();
        jQuery('.forget-form').show();
    });

    jQuery('#back-btn').click(function () {
        jQuery('.login-form').show();
        jQuery('.forget-password').show();
        jQuery('.forget-form').hide();
    });


</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>