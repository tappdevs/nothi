<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.2
Version: 3.3.0
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title>নথি | অফিস ব্যবস্থাপনা</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    <?php
    $path = $this->request->webroot;
    if (CDN == 1) {
        $path = 'http://cdn1.nothi.gov.bd/webroot/';
    }
    ?>
    <link rel="stylesheet" href="<?= $path; ?>a2i/demo-files/demo.css">
    <link rel="stylesheet" href="<?= $path; ?>a2i/style.css">
    <link rel="stylesheet" href="<?= $path; ?>css/style.css">

    <link href="<?= $path ?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?= $path ?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet"
          type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="<?= $path ?>assets/global/css/components.css" rel="stylesheet" id="style_components"
          type="text/css"/>
    <link href="<?= $path ?>assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
    <link href="<?= $path; ?>css/login-5.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?= $path ?>assets/global/plugins/bootstrap-toastr/toastr.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?= $path ?>assets/admin/pages/css/blog.css" rel="stylesheet" type="text/css" />

    <script src="<?= $path ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <script type="text/javascript">
        var js_wb_root = '<?= $path; ?>';
    </script>
    <style>
        .login-copyright {
            position: absolute;
            bottom: 0px;
            padding: 5px;
            background: #fff;
            width: 100%;
            color: #000;
            font-size: 10pt !important;
        }

        .login-copyright img {
            vertical-align: sub;
        }
    </style>
</head>
<body class=" login">

<?php echo $this->fetch('content'); ?>


<!--[if lt IE 9]>
<script src="<?= $path ?>assets/global/plugins/respond.min.js"></script>
<script src="<?= $path ?>assets/global/plugins/excanvas.min.js"></script>
<script src="<?= $path ?>assets/global/plugins/ie8.fix.min.js"></script>
<![endif]-->
<script src="<?= $path ?>assets/global/plugins/backstretch/jquery.backstretch.js" type="text/javascript"></script>

<script src="<?= $path; ?>assets/global/plugins/jquery-validation/js/jquery.validate.min.js"
        type="text/javascript"></script>
<script src="<?= $path; ?>js/client.min.js" type="text/javascript"></script>
<script src="<?= $path ?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?= $path ?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js"
        type="text/javascript"></script>
<script src="<?= $path ?>assets/global/plugins/bootstrap-toastr/toastr.min.js" type="text/javascript"/>
<script src="<?= $path ?>assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>

<script>
    $(function () {
        handleForgetPassword();
    });


    var handleForgetPassword = function () {
        $('.forget-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                username: {
                    required: true,
                }
            },
            messages: {
                username: {
                    required: "লগইন আইডি দেয়া হয়নি"
                }
            },
            invalidHandler: function (event, validator) { //display error alert on form submit

            },
            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },
            errorPlacement: function (error, element) {
                error.insertAfter(element.closest('.input-icon'));
            },
            submitHandler: function (form) {
                $.ajax({
                    url: '<?php
                        echo $this->Url->build(['controller' => 'Users',
                            'action' => 'forgetPassword'])
                        ?>',
                    data: $('.forget-form').serialize(),
                    method: 'post',
                    cache: false,
                    dataType: 'JSON',
                    success: function (response) {
                        $('.forget-form').find('.form-actions').show();
                        $('.message').hide();
                        toastr.options = {
                            "closeButton": true,
                            "debug": false,
                            "positionClass": "toast-bottom-right"
                        };
                        if (response.status == 'error') {
                            toastr.error(response.msg);
                        } else {
                            toastr.success(response.msg);
                        }
                    }
                })
            }
        });


    }

    $('.forget-form input').keypress(function (e) {
        if (e.which == 13) {
            if ($('.forget-form').validate().form()) {
                $('.forget-form').find('.form-actions').hide();
                $('.forget-form').submit();
            }
            return false;
        }
    });

    $('.forget-form button').click(function (e) {
        if ($('.forget-form').validate().form()) {
            $('.forget-form').find('.form-actions').hide();
            $('.forget-form').submit();
        }
        return false;

    });
</script>

</body>
</html>