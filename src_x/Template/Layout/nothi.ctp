<!DOCTYPE html>
<html lang="en">
<head>
  <base href="./">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
  <meta name="description" content="CoreUI - Open Source Bootstrap Admin Template">
  <meta name="author" content="Łukasz Holeczek">
  <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,jQuery,CSS,HTML,RWD,Dashboard">
  <title>নথি | অফিস ব্যবস্থাপনা</title>
  <!-- Icons-->
  <link href="<?= $this->request->webroot ?>coreui/css/font-icons.css" rel="stylesheet">
  <link href="<?= $this->request->webroot ?>coreui/node_modules/@coreui/icons/css/coreui-icons.min.css" rel="stylesheet">
  <link href="<?= $this->request->webroot ?>coreui/node_modules/flag-icon-css/css/flag-icon.min.css" rel="stylesheet">
  <link href="<?= $this->request->webroot ?>coreui/node_modules/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="<?= $this->request->webroot ?>coreui/node_modules/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">
  <link href="<?= $this->request->webroot ?>coreui/vendors/select2/dist/css/select2.min.css" rel="stylesheet">
  <link href="<?= $this->request->webroot ?>coreui/node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css"
      rel="stylesheet">
  <link href="<?= $this->request->webroot ?>coreui/node_modules/datatables.net-bs4/css/dataTables.bootstrap4.css" rel="stylesheet">
  <!-- Include Editor style. -->
  <link href='<?= $this->request->webroot ?>coreui/node_modules/froala-editor/css/froala_editor.min.css' rel='stylesheet'
      type='text/css'/>
  <link href='<?= $this->request->webroot ?>coreui/node_modules/froala-editor/css/froala_style.min.css' rel='stylesheet' type='text/css'/>


  <!-- Main styles for this application-->
  <link href="<?= $this->request->webroot ?>coreui/css/style.genius.css" rel="stylesheet">
  <link href="<?= $this->request->webroot ?>coreui/vendors/pace-progress/css/pace.min.css" rel="stylesheet">
  <link href="<?= $this->request->webroot ?>coreui/css/nothi_style.css" rel="stylesheet">

  <!-- Bootstrap and necessary plugins-->
  <script src="<?= $this->request->webroot ?>coreui/node_modules/jquery/dist/jquery.min.js"></script>
  <script src="<?= $this->request->webroot ?>coreui/node_modules/popper.js/dist/umd/popper.min.js"></script>
  <script src="<?= $this->request->webroot ?>coreui/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>


  <script src="<?= $this->request->webroot ?>coreui/node_modules/pace-progress/pace.min.js"></script>
  <script src="<?= $this->request->webroot ?>coreui/node_modules/perfect-scrollbar/dist/perfect-scrollbar.min.js"></script>
  <script src="<?= $this->request->webroot ?>coreui/node_modules/@coreui/coreui-pro/dist/js/coreui.min.js"></script>
  <script src="<?= $this->request->webroot ?>coreui/vendors/select2/dist/js/select2.min.js"></script>
  <script src="<?= $this->request->webroot ?>coreui/node_modules/moment/min/moment.min.js"></script>
  <script src="<?= $this->request->webroot ?>coreui/node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>


  <script src="<?= $this->request->webroot ?>coreui/node_modules/datatables.net/js/jquery.dataTables.js"></script>
  <script src="<?= $this->request->webroot ?>coreui/node_modules/datatables.net-bs4/js/dataTables.bootstrap4.js"></script>
  <script src="<?= $this->request->webroot ?>coreui/js/datatables.js"></script>
  <!-- Include JS file. -->

</head>
<body
    class="app header-fixed sidebar-fixed aside-menu-fixed <?php echo !isset($page) ? 'sidebar-lg-show' : '' ?> font-bn-en">
<header class="app-header navbar">
  <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
    <span class="navbar-toggler-icon"></span>
  </button>
  <a class="navbar-brand py-1" href="#">
    <img class="navbar-brand-full h-100" src="<?= $this->request->webroot ?>coreui/img/logo.png" alt="CoreUI Logo">
    <img class="navbar-brand-minimized" src="<?= $this->request->webroot ?>coreui/img/logo.png" alt="CoreUI Logo">
  </a>
  <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
    <span class="navbar-toggler-icon"></span>
  </button>

  <ul class="nav navbar-nav">
    <li class="nav-item">
      <div class="btn-group">
        <button class="btn btn-success-n text-white fs-18 border-right-n" title="ডাক">
          <i class="fa fa-envelope-o"></i> ডাক
          <small class="badge badge-light text-success-n font-weight-normal">23</small>
        </button>
        <button class="btn btn-success-n text-white fs-18 border-left-n border-right-n active" title="নথি">
          <i class="fs0 efile-nothi1 mr-1"></i> নথি
          <small class="badge badge-light text-success-n font-weight-normal">23</small>
        </button>
        <button class="btn btn-success-n text-white fs-18 border-left-n border-right-n" title="দপ্তর">
          <i class="fa fa-folder-open mr-1"></i> দপ্তর
        </button>
        <button class="btn btn-success-n text-white fs-18 border-left-n" title="রিপোর্ট">
          <i class="fa fa-folder-open mr-1"></i>রিপোর্ট
          <small class="badge badge-light text-success-n font-weight-normal">23</small>
        </button>
      </div>
    </li>
  </ul>

  <ul class="nav navbar-nav ml-auto">

    <li class="nav-item">
      <div class="user_position font-bn-en dropdown line-height-1 pr-3 fs-16">
        <button class="dropdown-toggle text-light shadow-none btn-transparent text-left outline-none"
            id="user_position" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> কোহিনূর বেগম
          <small>(অফিস সহকারী, ব্যবসা-বাণিজ্য শাখা)</small>
          <br>
          জেলা প্রশাসকের কার্যালয়, জামালপুর
        </button>
        <div class="dropdown-menu dropdown-menu-right fs-16" aria-labelledby="user_position">
          <a class="dropdown-item" href="#"><i class="fa fa-mobile-phone"></i> নথি মোবাইল অ্যাপ </a>
          <a class="dropdown-item" href="#"><i class="fa fa-user"></i> প্রোফাইল ব্যবস্থাপনা </a>
          <a class="dropdown-item" href="#"><i class="fa fa-sign-out"></i> লগ আউট করুন </a>
        </div>
      </div>
    </li>

    <li class="nav-item dropdown">
      <a class="nav-link nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true"
          aria-expanded="false">
        <img class="img-avatar" src="<?= $this->request->webroot ?>coreui/img/avatars/6.jpg" alt="admin@bootstrapmaster.com">
      </a>
      <div class="dropdown-menu dropdown-menu-right">
        <div class="dropdown-header text-center">
          <strong>Account</strong>
        </div>
        <a class="dropdown-item" href="#">
          <i class="fa fa-bell-o"></i> Updates
          <span class="badge badge-info">42</span>
        </a>
        <a class="dropdown-item" href="#">
          <i class="fa fa-envelope-o"></i> Messages
          <span class="badge badge-success">42</span>
        </a>
        <a class="dropdown-item" href="#">
          <i class="fa fa-tasks"></i> Tasks
          <span class="badge badge-danger">42</span>
        </a>
        <a class="dropdown-item" href="#">
          <i class="fa fa-comments"></i> Comments
          <span class="badge badge-warning">42</span>
        </a>
        <div class="dropdown-header text-center">
          <strong>Settings</strong>
        </div>
        <a class="dropdown-item" href="#">
          <i class="fa fa-user"></i> Profile</a>
        <a class="dropdown-item" href="#">
          <i class="fa fa-wrench"></i> Settings</a>
        <a class="dropdown-item" href="#">
          <i class="fa fa-usd"></i> Payments
          <span class="badge badge-dark">42</span>
        </a>
        <a class="dropdown-item" href="#">
          <i class="fa fa-file"></i> Projects
          <span class="badge badge-primary">42</span>
        </a>
        <div class="divider"></div>
        <a class="dropdown-item" href="#">
          <i class="fa fa-shield"></i> Lock Account</a>
        <a class="dropdown-item" href="#">
          <i class="fa fa-lock"></i> Logout</a>
      </div>
    </li>
  </ul>
</header>
<div class="app-body d-flex flex-column h-100 overflow-hidden">
  <div class="sidebar">
    <nav class="sidebar-nav">
      <ul class="nav">
        <li class="nav-title">নথি</li>
        <li class="nav-item nav-dropdown">
          <a class="nav-link nav-dropdown-toggle" href="#">
            <i class="fs0 efile-nothi_management1 nav-icon" aria-hidden="true"></i> নথি ব্যবস্থাপনা</a>
          <ul class="nav-dropdown-items">
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="fs0 efile-nothi_management1 nav-icon" aria-hidden="true"></i> নথির ধরন</a>
              <ul class="nav-dropdown-items">
                <li class="nav-item">
                  <a class="nav-link" href="#">
                    <i class="fa fa-plus nav-icon"></i> ধরন তৈরি</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">
                    <i class="icon-tag nav-icon"></i> ধরনসমূহ</a>
                </li>
              </ul>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">
                <i class="icon-tag nav-icon"></i> নথি তৈরি</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">
                <i class="icon-tag nav-icon"></i> পত্রজারি গ্রুপ</a>
            </li>
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="fs0 efile-nothi_management1 nav-icon" aria-hidden="true"></i> গার্ড ফাইল</a>
              <ul class="nav-dropdown-items">
                <li class="nav-item">
                  <a class="nav-link" href="#">
                    <i class="icon-tag nav-icon"></i> গার্ড ফাইলের ধরন</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="base/#.html">
                    <i class="icon-tag nav-icon"></i> গার্ড ফাইল তালিকা</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">
                    <i class="icon-tag nav-icon"></i> আপলোড গার্ড ফাইল</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li class="nav-item nav-dropdown">
          <a class="nav-link nav-dropdown-toggle" href="#">
            <i class="fs0 a2i_gn_register2 nav-icon" aria-hidden="true"></i> নিবন্ধন বহি </a>
          <ul class="nav-dropdown-items">
            <li class="nav-item">
              <a class="nav-link" href="#"><i class="nav-icon fs0 efile-user2" aria-hidden="true"></i> মাস্টার ফাইল
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#"><i class="nav-icon fs0 efile-preron_register1"
                    aria-hidden="true"></i> নথি প্রেরণ নিবন্ধন বহি
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#"><i class="nav-icon fs0 efile-grohon_register1"
                    aria-hidden="true"></i> নথি গ্রহণ নিবন্ধন বহি
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#"><i class="nav-icon fs0 efile-register1"
                    aria-hidden="true"></i>
                নথি নিবন্ধন বহি
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#"><i class="nav-icon fs0 efile-potrojari1"
                    aria-hidden="true"></i> পত্রজারি নিবন্ধন বহি
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#"><i class="nav-icon fs0 efile-save2"
                    aria-hidden="true"></i>
                আর্কাইভড নথিসমূহ
              </a>
            </li>
          </ul>
        </li>

        <li class="nav-item nav-dropdown">

          <a class="nav-link nav-dropdown-toggle" href="#">
            <i class="fs0 a2i_ld_protibedon3 nav-icon" aria-hidden="true"></i> <span class="title">প্রতিবেদনসমূহ</span>
            <span class="arrow"></span></a>
          <ul class="nav-dropdown-items">
            <li class="nav-item">
              <a class="nav-link" href="#"><i class="fs0 a2i_ld_protibedon3 nav-icon"
                    aria-hidden="true"></i>
                শাখাভিত্তিক নথিসমূহের তালিকা
              </a>
            </li>
          </ul>
        </li>
        <li class="nav-item">
          <a href="#" class="nav-link"><i class="fs0 efile-potrojari3 nav-icon" aria-hidden="true"></i> পত্রজারি পেন্ডিং
          </a>
        </li>
        <li class="nav-item">
          <a href="#" class="nav-link"><i class="fs0 efile-settings2 nav-icon" aria-hidden="true"></i> সেটিংস
            <ul class="nav-dropdown-items">
              <li class="nav-item">
                <a href="#" class="nav-link"><i class="fs0 a2i_gn_abedongrohon2 nav-icon" aria-hidden="true"></i>
                  নথি সিদ্ধান্তসমূহ
                </a>
              </li>
              <li class="nav-item">
                <a href="#" class="nav-link"><i class="fs0 a2i_gn_approval1 nav-icon"
                      aria-hidden="true"></i>
                  নথিতে অনুমতি প্রদান
                </a>
              </li>
              <li class="nav-item">
                <a href="#" class="nav-link"><i class="fs0 a2i_gn_close1 nav-icon"
                      aria-hidden="true"></i>
                  নথি হতে অনুমতি প্রত্যাহার
                </a>
              </li>
              <li class="nav-item">
                <a href="#">
                  <i class="fs0 a2i_gn_template1 nav-icon" aria-hidden="true"></i>
                  আর্কাইভের জন্য অপেক্ষমাণ নথিসমূহ
                </a>
              </li>
            </ul>
        </li>
        <li class="nav-item">
          <a href="#" class="nav-link"><i class="fs0 a2i_nt_nishponnonothi1 nav-icon"
                aria-hidden="true"></i> সার-সংক্ষেপ ট্রাকিং </a>
        </li>

      </ul>
    </nav>
    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
  </div>
  <main class="main d-flex flex-column flex-fill position-relative h-100">
    <!-- Breadcrumb-->
    <?php
    if (!isset($page)) {

      ?>
      <div class="bg-white shadow-sm align-items-center py-2 d-flex">
        <div class="col-xl-4">
          <?= $this->Form->select('nothi_type', ['অফিস সহকারী, ব্যবসা-বাণিজ্য শাখা', 'সিনিয়র সফটওয়্যার ইঞ্জিনিয়ার 1 (অতিরিক্ত দায়িত্ব), ই-সার্ভিস টেস্ট', 'সাঁট লিপিকার, জেলা প্রশাসকের কার্যালয়'], ['class' => 'form-control', 'empty' => '-- পদবি নির্বাচন করুন --']); ?>
        </div>
        <div class="col d-flex">
          <div class="search-all flex-fill">
            <div class="dropdown">
              <input type="text" class="form-control" placeholder="&#xf002; বিষয় দিয়ে খুঁজুন...">
              <button id="advanced-search" class="btn btn-transparent text-info btn-toggler" data-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"><i class="fa fa-caret-down"></i></button>
              <div class="dropdown-menu modules dropdown-menu-right shadow w-100" aria-labelledby="advanced-search">
                <form class="px-4 py-3" action="" method="post">
                  <h4 class="font-bn-en">বিস্তারিত খুঁজুন</h4>
                  <div class="form-row">
                    <div class="col-md-6 mb-3">
                      <?= $this->Form->control('nothi_no', ['placeholder' => 'নথি নাম্বার', 'class' => 'form-control']) ?>
                    </div>
                    <div class="col-md-6 mb-3">
                      <?= $this->Form->select('nothi_type', ['নিম্নোক্ত তালিকা', 'থেকে পদবি ', 'নির্বাচন করুন'], ['class' => 'form-control', 'empty' => '-- নথি ধরন --']); ?>
                    </div>
                    <div class="col-md-12 mb-3">
                      <div class="input-group">
                        <input type="text" aria-label="First name" class="form-control datepicker"
                            placeholder="তারিখ নির্বাচন">
                        <span class="input-group-text">হইতে</span>
                        <input type="text" aria-label="Last name" class="form-control datepicker"
                            placeholder="তারিখ নির্বাচন">
                      </div>
                    </div>
                  </div>
                  <div class="form-row">
                    <div class="col-md-6 mb-3">
                      <?= $this->Form->select('select_section', ['গোপনীয় শাখা', 'আর এম শাখা', 'জেনারেল সার্টিফিকেট শাখা', 'রাজস্ব মুন্সিখানা শাখা', 'রেকর্ড রুম শাখা', 'ভূমি অধিগ্রহণ শাখা', 'ভি পি শাখা', 'প্রকাশনা শাখা', 'ফৌজদারী নকলখানা শাখা', 'রীট শাখা', 'বিচার শাখা', 'জুডিশিয়াল মুন্সিখানা শাখা', 'লাইসেন্স (প্রমোদ) শাখা', 'আগ্নেয়াস্ত্র শাখা', 'সীমান্ত শাখা', 'নির্বাহী ম্যাজিস্ট্রেট আদালত', 'গোপনীয় শাখা', 'সার্বিক', 'স্থানীয় সরকার', 'রাজস্ব', 'গোপনীয় শাখা', 'অতিরিক্ত জেলা ম্যাজিস্ট্রেটের দপ্তর', 'ভূমি অধিগ্রহণ', 'এনজিও বিষয়ক শাখা', 'মুক্তিযোদ্ধা কল্যাণ বিষয়ক শাখা', 'ফ্রন্ট ডেস্ক শাখা', 'শিক্ষা ও কল্যাণ শাখা', 'জেলা ই-সেবা কেন্দ্র'], ['class' => 'form-control', 'empty' => '-- শাখা নির্বাচন করুন --']); ?>
                    </div>
                    <div class="col-md-6 mb-3">
                      <?= $this->Form->select('all_nothi', ['গোপনীয় নথি সমূহ', 'শুধু জরুরী নথি সমূহ'], ['class' => 'form-control', 'empty' => '-- সকল নথি --']); ?>
                    </div>

                  </div>
                  <div class="btn-group">
                    <button type="submit" class="btn btn-success-n text-white"><i class="fa fa-search"></i> খুঁজুন
                    </button>
                    <button type="reset" class="btn btn-danger-n text-white"><i class="fa fa-refresh"></i> রিসেট
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <div class="btn btn-success text-white"><i class="fa fa-search"></i></div>
          <div class="btn btn-danger"><i class="fa fa-refresh"></i></div>
          <div class="btn btn-primary aside-menu-toggler" data-toggle="aside-menu-lg-show"><i class="fa fa-bars"></i>
          </div>
        </div>
      </div>
    <?php } ?>
    <div id="mainContent"
        class="animated mainContent fadeIn flex-basis border-top  bg-white overflow-hidden flex-column d-flex">

      <?php
      
      if (isset($page) && $page == 'khoshra') {
        echo $this->element('Nothi/note/khoshra/nothi_khoshra_container');
      }
      if (isset($page)) {
        echo $this->element('Nothi/nothi_note_container');
      } else {
        echo $this->element('Nothi/nothi_note_container1');
      }

      ?>

      <?php

      // if (isset($page) && $page == 'khoshra') {
      //   echo $this->element('Nothi/note/khoshra/nothi_khoshra_container');
      // } elseif (isset($page) && $page == 'nothi3') {
      //   echo $this->element('Nothi/nothi3_container');
      // } else {
      //   echo $this->element('Nothi/nothi_note_container');
      // }

      ?>


    </div>
    <?php
    if (!isset($page)) {
      ?>


      <div class="content-slide">

        <div class="card border-0 h-100 mb-0">
          <div class="card-header">
            <div class="align-items-center d-flex">
              <button type="button" class="btn btn-warning btn-show-hide">
                <i class="fa fa-long-arrow-left"></i> আগত নথিতে ফেরৎ যান
              </button>
              <strong class="ml-3">ফাইলের মাধ্যমে জিঞ্জাসিত বিষয়গুলো সঙ্গায়ীত (ব্যবসা-বাণিজ্য শাখা
                ) - <span>০১.০১.০১০২.০১৮.০২.০০১.১৯</span></strong>
            </div>

          </div>
          <div class="card-body overflow-hidden pScroll">
            <div class="alert alert-warning d-flex justify-content-between align-items-center" role="alert">
              <h3 class="mb-0">নিন্মোক্ত তালিকা হতে নোট সিলেক্ট করুন</h3>
              <div class="position-relative">
                <i class="fa fa-angle-double-down animate" aria-hidden="true"></i>
              </div>
            </div>

            <table class="table table-striped table-bordered datatable table-sm">
              <thead>
              <tr class="text-center">
                <th class="text-center" style="width: 40%">বিষয়</th>
                <th class="text-center" style="width: 20%">গ্রহণের সময়</th>
                <th class="text-center" style="width: 30%">বর্তমান ডেস্ক</th>
                <th class="text-center">বিস্তারিত</th>
              </tr>
              </thead>
              <tbody id="table-nothi-notes-body"></tbody>
            </table>


          </div>
          <div class="card-footer text-right">
            <div class="btn-group btn-group-round">
              <button type="button" class="btn btn-warning">
                <i class="fs1 a2i_gn_note2"></i> নতুন নোট
              </button>
              <a type="button" class="btn btn-primary btn-sokol" href="/tappware/nothi/nothiDetail/47/"><i
                    class="fs1 a2i_gn_note1"></i> সকল নোট</a>
              <button type="button" class="btn btn-danger btn-show-hide" data-dismiss="modal"><i
                    class="fs1 efile-close1"></i> বাতিল করুন
              </button>
            </div>
          </div>
        </div>

      </div>
    <?php } ?>
  </main>
  <?= $this->element('Nothi/nothi_aside_menu'); ?>
</div>
<footer class="app-footer">
  <div>
    <span>কপিরাইট ২০১৯, একসেস টু ইনফরমেশন</span>
  </div>
  <div class="ml-auto d-flex">
    <span>পার্টনার: </span>
    <div class="partners d-flex">
      <a href="#" title="bangladehs"><img src="<?= $this->request->webroot ?>coreui/img/partners/ban-gov_logo.png" alt="bangladehs"></a>
      <a href="#" title="a2i"><img src="<?= $this->request->webroot ?>coreui/img/partners/a_a2i-logo.jpg" alt="a2i"></a>
      <a href="#" title="BCC"><img src="<?= $this->request->webroot ?>coreui/img/partners/bcc_logo.png" alt="BCC"></a>
      <a href="#" title="DOICT"><img src="<?= $this->request->webroot ?>coreui/img/partners/doict_logo.jpg" alt="DOICT"></a>
      <a href="#" title="TAPPWARE"><img src="<?= $this->request->webroot ?>coreui/img/partners/tappware_logo.png" alt="tappware"></a>
    </div>
  </div>
</footer>


<script src="<?= $this->request->webroot ?>coreui/js/react.production.min.js" crossorigin></script>
<script src="<?= $this->request->webroot ?>coreui/js/react-dom.production.min.js" crossorigin></script>
<script src="<?= $this->request->webroot ?>coreui/js/custom-script.js" crossorigin></script>
<script>
  fetch('<?= $this->request->webroot?>NothiMasterMovements/getAllNothiV2').then(response => {
    return response.json();
  }).then(data => {
    if (data.status == "success") {
      let nothi_list = data.data;
      console.log("Nothi data : ", nothi_list);
      let table_body = $('#dbl_click');
      table_body.find('tr').remove();
      nothi_list.forEach(function (value) {
        let tr_tmp = "<tr role=row class=even><td style='font-family:nikosh'>" + value.id + "</td><td>" + value.NothiMasters.nothi_no +
          "</td><td>" + value.NothiMasters.subject + "</td><td>" + value.created +
          "</td><td>" + value.from_office_unit_name +
          "</td><td><div class='text-center'><a class='btn btn-success btn-sm btn-show-hide' data-nothi-master-id=" + value.nothi_master_id +
          " href='javascript;'><i class='fa fa-search-plus'></i></a></div></td></tr>";
        table_body.append(tr_tmp);
        // console.log(value.id);
      });
      
    }
  }).catch(err => {
    console.log('Error: ', err);
  });
</script>

</body>
</html>
