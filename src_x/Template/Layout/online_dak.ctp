<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->

<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title>নথি | অফিস ব্যবস্থাপনা</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="initial-scale=0" name="viewport"/>
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    <?php
    $path = $this->request->webroot;

    if (CDN == 1) {
        $path = 'http://cdn1.nothi.gov.bd/webroot/';
    }
    ?>
    <?php
    if (defined("Live") && Live == 1) {
        ?>
        <script id="fr-fek">try {
				(function (k) {
					localStorage.FEK = k;
					t = document.getElementById('fr-fek');
					t.parentNode.removeChild(t);
				})('xc1We1KYi1Ta1WId1CVd1F==')
			} catch (e) {
			}</script>
        <?php
    }
    ?>
    <?= $this->element('load_js_css_head') ?>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
<!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
<!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->
<!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->
<!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->
<!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->
<!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->
<script type="text/javascript">
    var js_wb_root = '<?= $this->request->webroot ?>';
</script>
<body class="page-header-fixed page-sidebar-closed-hide-logo page-sidebar-closed-hide-logo online-dak">
<div class="clearfix"></div>
<!-- BEGIN CONTAINER -->
<div class="page-container" style="margin-top: 0px;">
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content-online-dak">
            <div class="row">
                <div class="col-md-12">
                    <div id="ajax-content">
                        <?php echo $this->fetch('content'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<!--        <div class="page-footer">
            <div class="page-footer-inner">
                কপিরাইট <?= $Year ?>, একসেস টু ইনফরমেশন
            </div>
            <div class="scroll-to-top">
                <i class="icon-arrow-up"></i>
            </div>
        </div>-->
<!-- END FOOTER -->
<?= $this->element('load_js_css_body') ?>
<script>
    function bootboxCloseButton(element) {
        $(element).closest('.modal-content').find('.modal-footer').find('[data-bb-handler=danger]').trigger('click');
    };
</script>
<!-- END -->
</body>
<!-- END BODY -->
</html>