<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
<link href="<?= $this->request->webroot ?>potrojariPrintPreview/bootstrap.min.css" rel="stylesheet">
<link href="<?= $this->request->webroot ?>potrojariPrintPreview/style.css" rel="stylesheet">

</head>
<body>
<?php echo $this->fetch('content'); ?>
<script>
    const setAttributes = (el, attrs) => {
      for (var key in attrs) {
        el.setAttribute(key, attrs[key]);
      }
    }

    const replaceClass = (classToAdd, classToRemove) => {
      var element = document.querySelectorAll('.' + classToRemove);
      if (element) {
        for (i = 0; i < element.length; i++) {
          element[i].classList.add(classToAdd);
          element[i].classList.remove(classToRemove);
        }
      }
    }
    const removeClass = (classToRemove) => {
      var element = document.querySelectorAll('.' + classToRemove);
      if (element) {
        for (i = 0; i < element.length; i++) {
          element[i].classList.remove(classToRemove);
        }
      }
    }

    var left_slogan = document.querySelector('#left_slogan');
    var sharok_no2 = document.querySelector('#sharok_no2');
    var sharok_no = document.querySelector('#sharok_no');
    var getarthe = document.querySelector('#getarthe');
    var canedit = document.querySelectorAll('.canedit');


    var english = /^[a-zA-Z0-9$@$!%*?&#^-_. +]+$/;
    var isUrl = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;


    replaceClass('offset-md-7', 'col-md-offset-7');
    replaceClass('offset-sm-7', 'col-sm-offset-7');
    removeClass('col-xs-offset-7');


    if (getarthe) {
      getarthe.innerHTML = getarthe.innerHTML.replace('<br>', '');
    }
    if (sharok_no2) {
      setAttributes(sharok_no2.parentElement.parentElement, {
        "style": "margin-bottom:50px;",
        "class": "row customFontSize align-items-center"
      });
    }


    if (left_slogan) {
      left_slogan.parentElement.parentElement.classList.add('row');
    }

    if (canedit) {
      for (var i = 0; i < canedit.length; i++) {
        if (english.test(canedit[i].textContent) || isUrl.test(canedit[i].textContent)) {
          canedit[i].setAttribute('class', 'canedit en');
        }
      }
    }

</script>
</body>
</html>

