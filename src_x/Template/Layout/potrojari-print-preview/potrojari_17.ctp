<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<link href="<?= $this->request->webroot ?>potrojariPrintPreview/bootstrap-grid.min.css" rel="stylesheet">
<link href="<?= $this->request->webroot ?>potrojariPrintPreview/style.css" rel="stylesheet">
<style>

    .en {
      font-family: Arial, SolaimanLipi, sans-serif
    }

    .templateWrapper {
      margin: auto;
      padding: 80px;
    }

    /*
    .templateWrapper {
      box-shadow: 0 0 10px;
      margin: auto;
      width: 850px;
      padding: 80px;
      margin-top: 30px;
      max-width: 850px;
    }
    */

</style>

</head>
<body>
<div class="container">
<div class="">

<?php echo $this->fetch('content'); ?>
</div>
</div>

<script>
    const setAttributes = (el, attrs) => {
      for (var key in attrs) {
        el.setAttribute(key, attrs[key]);
      }
    }


    var sender_signature = document.querySelector('#sender_signature');
    var sender_signature2 = document.querySelector('#sender_signature2');
    var sender_email = document.querySelector('#sender_email');
    var sharok_no2 = document.querySelector('#sharok_no2');
    var sharok_no = document.querySelector('#sharok_no');
    var karjoSubject = document.querySelector('#subject');
    var showforPopup = document.querySelector('.showforPopup');
    var note = document.querySelector('#note');
    var sovapotiname = document.querySelector('#sovapotiname');
    var sovapotiname2 = document.querySelector('#sovapotiname2');
    var getarthe = document.querySelector('#getarthe');
    var left_slogan = document.querySelector('#left_slogan');
    // var subject = document.querySelector('#subject');


    var canedit = document.querySelectorAll('.canedit');
    var sender_block = document.querySelectorAll('#sender_name, #sender_designation,#sender_phone,#sender_fax,#sender_email');

    var english = /^[a-zA-Z0-9$@$!%*?&#^-_. +]+$/;
    var isUrl = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;


    if (left_slogan) {
      left_slogan.parentElement.parentElement.setAttribute('class', 'col-md-12 row col-xs-12 col-sm-12 text-center customFontSize');
    }
    if (note) {
      note.parentElement.setAttribute('class', 'col');
    }
    if (getarthe) {
      getarthe.parentElement.setAttribute('class', 'col-md-9 col-xs-9 col-sm-9 offset-md-1 offset-xs-1 col-sm-offset-1 text-left');
    }
    if (sovapotiname2) {
      sovapotiname2.parentElement.setAttribute('class', 'col-md-5 col-xs-5 col-sm-5 offset-md-7 col-sm-offset-7 text-center');
    }

    if (sovapotiname) {
      sovapotiname.closest('table').parentElement.setAttribute('class', 'col-md-10 col-xs-10 col-sm-10 text-left offset-md-1 offset-sm-1 offset-xs-1 ');
    }

    if (typeof (showforPopup) != 'undefined' && showforPopup != null) {
      showforPopup.setAttribute('class', 'showforPopup en');
    }
    if (karjoSubject) {
      setAttributes(karjoSubject.parentElement, {
        "style": "",
        "class": "text-left col"
      });
    }

    /*
    if (subject) {
      subject.parentElement.setAttribute('style', '');
    }*/


    if (canedit) {
      for (var i = 0; i < canedit.length; i++) {
        // console.log(canedit[i].textContent)

        if (english.test(canedit[i].textContent) || isUrl.test(canedit[i].textContent)) {
          canedit[i].setAttribute('class', 'canedit en');
        }
      }
    }

    if (sender_signature2) {
      sender_signature2.parentElement.setAttribute('class', 'col-md-3 offset-md-9 text-center');
    }
    if (sender_signature) {
      sender_signature.parentElement.setAttribute('class', 'col-md-3 offset-md-9 text-center');
      sender_signature.parentElement.parentElement.parentElement.setAttribute('id', 'senderBlock');
      sender_signature.parentElement.parentElement.parentElement.style.cssText = "margin-top:20px;";
    }
    if (sender_email) {
      sender_email.setAttribute('class', 'en');
    }

    if (sharok_no) {
      setAttributes(sharok_no.parentElement, {
        "style": "margin-bottom:50px;",
        "class": "col-md-5 col-xs-5 col-sm-5 col-md-offset-1 offset-md-1 offset-sm-1 text-left"
      });
    }
    if (sharok_no2) {
      setAttributes(sharok_no2.parentElement.parentElement, {
        "style": "margin-bottom:50px;",
        "class": "row customFontSize align-items-center"
      });
    }

    if (sender_block) {
      for (var i = 0; i < sender_block.length; i++) {
        sender_block[i].parentElement.parentElement.parentElement.setAttribute('class', 'col-md-3 offset-md-3 text-center');
        sender_block[i].parentElement.parentElement.setAttribute('class', 'text-center');
      }
    }

</script>
</body>
</html>