<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.2
Version: 3.3.0
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title>নথি | অফিস ব্যবস্থাপনা</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta content="" name="description"/>
    <meta content="" name="author"/>

    <?php
    $path = $this->request->webroot;
    if (CDN == 1) {
        $path = 'http://cdn1.nothi.gov.bd/webroot/';
    }

    ?>


    <link rel="stylesheet" href="<?php echo $this->request->webroot; ?>a2i/demo-files/demo.css">
    <link rel="stylesheet" href="<?php echo $this->request->webroot; ?>a2i/style.css">

    <link href="<?php echo $path; ?>assets/global/plugins/bootstrap/css/bootstrap.min.css"
          rel="stylesheet" type="text/css"/>
    <link href="<?php echo $path; ?>assets/global/plugins/uniform/css/uniform.default.css"
          rel="stylesheet" type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="<?php echo $path; ?>assets/admin/pages/css/login-soft.css" rel="stylesheet"
          type="text/css"/>
    <!-- END PAGE LEVEL SCRIPTS -->
    <!-- BEGIN THEME STYLES -->
    <link href="<?php echo $path; ?>assets/global/css/components-rounded.css" id="style_components"
          rel="stylesheet" type="text/css"/>

    <link href="<?php echo $path; ?>assets/admin/layout4/css/layout.css" rel="stylesheet"
          type="text/css"/>
    <script src="<?php echo $path; ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
    <!-- END THEME STYLES -->
    <link rel="shortcut icon" href="<?php echo $path; ?>favicon.ico"/>

    <style>
        .message {
            word-wrap: break-word;
        }

        .error-message {
            color: red;
        }

        .login {

            background: url(<?= $path; ?>img/bg_image.jpg) no-repeat center center #CDE7A8 fixed;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }
        *{
            font-size: 13pt!important;
        }
    </style>

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->

<body class="login" style="font-family: Nikosh!important;font-size: 13pt!important;">
<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
<div class="menu-toggler sidebar-toggler">
</div>
<!-- END SIDEBAR TOGGLER BUTTON -->
<!-- BEGIN LOGO -->
<?= $this->element('ssl_message_and_mobile_detection'); ?>
<div class="logo" style="color:#000; ">
    <img style="width: 200px" src="<?php echo $path; ?>img/nothi_logo_login.png" alt=""/>
</div>
<div class="text-center font-md" id="SSLMessageForAndroid" hidden>

    <a class="btn btn-primary" href="https://play.google.com/store/apps/details?id=com.tappware.nothi&hl=en">
        ডাউনলোড করুন নথির অ্যান্ড্রয়েড অ্যাপ
    </a>
</div>
<div class="text-center font-md" id="SSLMessageForIOS" hidden>

    <a class="btn btn-primary" href="https://itunes.apple.com/us/app/id1187955540">
        ডাউনলোড করুন নথির আই ও এস অ্যাপ
    </a>
</div>
<!-- END LOGO -->
<!-- BEGIN LOGIN -->
<div class="content" style="width: 480px!important;">
    <!--    <p class="alert alert-info">নথির নতুন আপডেট এর কারনে অনুগ্রহ করে ব্রাউজারের কেশ পরিষ্কার করুন</p>-->
    <?php echo $this->Flash->render(); ?>
    <?php echo $this->element('form_ssl_login') ?>
    <div class="row" style="border-bottom: 1px solid #b1b1b1;">
        <div class="col-md-6 col-sm-6 col-xs-6 text-left">
            <a href="#" onclick="showhelpdeskPanel()" class="btn btn-link"> হেল্প ডেস্ক <i class="glyphicon glyphicon-chevron-up icon-changer"></i></a>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-6 text-right">
            <a href="<?php
            echo $this->Url->build(['controller' => 'DakNagoriks',
                                    'action' => 'NagorikAbedon'])
            ?>" class="btn btn-link" style="text-decoration: none;"><i class="efile-citizen1"></i>&nbsp;নাগরিক কর্নার&nbsp;</a>
        </div>
    </div>
    <div class="create-help helpdeskPanel" style="
    display:block;
    position: relative;
    background: #fff;
    padding: 10px;
    z-index: 100;
    width: 480px;
    margin-left: -30px;
">
        <div class="row">
            <div class="col-sm-7 col-md-7 col-xs-7">
                <div class="form-group ">
                    <i class="glyphicon glyphicon-phone"></i> +৮৮ ০১৭৫২-০২৬৫২১
                    <br/>
                    <i class="glyphicon glyphicon-phone"></i> +৮৮ ০১৭১২-৪৮৫৭৪৭
                    <br/>
                    <span class="glyphicon glyphicon-envelope"></span> <a href="mailto:support@nothi.org.bd" style="
font-size: 14px">support@nothi.org.bd</a><br/>
                    <img src="<?= $this->request->webroot ?>img/fb.png" style="margin-left:-2px;"/>
                    <a target="__tab" class="btn-link bold font-md" style="text-decoration: none;"
                       href="https://fb.com/groups/nothi/">নথি (ফেসবুক গ্রুপ)</a><br/>
                    <small style="vertical-align: bottom!important;">
                        <img src="<?= $this->request->webroot ?>img/img_tapp_logo.png" style="height: 18px;">
                        কারিগরি সহায়তায় <a style="vertical-align: bottom!important;" href="http://tappware.com" target="_tab">ট্যাপওয়ার</a>
                    </small>
                </div>
            </div>
            <div class="col-sm-5 col-md-5 col-xs-5">
                <div class="form-group ">
                    <icon class="glyphicon glyphicon-info-sign"></icon>
                    <a target="__tab" class="btn-link bold font-md" style="text-decoration: none;"
                       href="<?= $this->request->webroot ?>FAQ">আপনার জিজ্ঞাসা</a><br/>
                    <icon class="glyphicon glyphicon-bullhorn"></icon>
                    <a target="__tab" class="btn-link bold font-md" style="text-decoration: none;"
                       href="<?= \Cake\Routing\Router::url(['_name' => 'release-note']); ?>">আপডেট </a><br/>
                    <icon class="glyphicon glyphicon-book"></icon>
                    <a target="__tab" class="btn-link bold font-md" href="<?= $this->Url->build(['_name' => 'showUserManualList']) ?>"> ব্যবহার সহায়িকা (ভার্সনঃ ১৬)</a><br/>
                    <icon class="glyphicon glyphicon-play-circle"></icon>
                    <a target="__tab" class="btn-link bold font-md" style="text-decoration: none;" href="https://youtu.be/hmUUBt7G8Gs">ভিডিও টিউটোরিয়াল</a><br/>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content" style="padding-top: 10px!important;width: 480px!important">
    <div class="logobd" align="left">
        <a href="http://bangladesh.gov.bd" target="_blank">
            <img src="<?php echo $path ?>assets/admin/layout4/img/bd.png" alt="">
        </a>
    </div>
    <div class="logoa2i" align="right">
        <a href="http://www.a2i.gov.bd" target="_blank">
            <img src="<?php echo $path ?>assets/admin/layout4/img/a2i.png" alt="">
        </a>
    </div>
    <div style="clear:both"></div>
</div>

<!-- END LOGIN -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="<?php echo $path; ?>assets/global/plugins/respond.min.js"></script>
<script src="<?php echo $path; ?>assets/global/plugins/excanvas.min.js"></script>
<![endif]-->

<script src="<?php echo $path; ?>assets/global/plugins/bootstrap/js/bootstrap.min.js"
        type="text/javascript"></script>
<script type="application/javascript">
    function showhelpdeskPanel() {
        $('.helpdeskPanel').toggle(500);
        $('.icon-changer').toggleClass("glyphicon-chevron-up glyphicon-chevron-down");
    }
</script>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>