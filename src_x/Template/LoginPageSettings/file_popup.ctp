<?php

if ($data['attachment_type'] == 'text') {

} elseif (!empty($data['file_name'])) {

    $url = urlencode(FILE_FOLDER . $data['file_name'] . "?token=" . sGenerateToken(['file' => $data['file_name']], ['exp' => time() + 60 * 300]));
    $down_url = ($this->request->webroot."content/" . $data['file_name'] . "?token=" . sGenerateToken(['file' => $data['file_name']], ['exp' => time() + 60 * 300]));

    if (substr($data['attachment_type'], 0, 5) != 'image') {
        if (substr($data['attachment_type'], 0, strlen('application/vnd')) == 'application/vnd' || substr($data['attachment_type'], 0, strlen('application/ms')) == 'application/ms') {
            echo "<h4 class='alert alert-danger'>ফাইলটি দেখতে কোন ধরনের সমস্যা হলে, দয়া করে ডাউনলোড করে দেখুন। <a class='btn btn-sm btn-success' href='" . $down_url . "' download><i class='fa fa-download'></i> ডাউনলোড</a></h4>";
//            echo "<h4 class='alert alert-danger'>ফাইলটি দেখতে কোন ধরনের সমস্যা হলে, দয়া করে ডাউনলোড করে দেখুন। <span class='btn btn-sm btn-success' onclick='getDownloadFile(\"" . $down_url . "\")' ><i class='fa fa-download'></i> ডাউনলোড</span></h4>";

            echo '<iframe style=" width:100%; height: 700px;" src="https://docs.google.com/gview?url=' . $url . '&embedded=true" style="width:600px; height:500px;" frameborder="0"></iframe>';
        } else{
//            echo "<h4 class='alert alert-danger'>ফাইলটি দেখতে কোন ধরনের সমস্যা হলে, দয়া করে ডাউনলোড করে দেখুন। <span class='btn btn-sm btn-success' onclick='getDownloadFile(\"" . $down_url . "\")' ><i class='fa fa-download'></i> ডাউনলোড</span></h4>";
            echo "<h4 class='alert alert-danger'>ফাইলটি দেখতে কোন ধরনের সমস্যা হলে, দয়া করে ডাউনলোড করে দেখুন। <a class='btn btn-sm btn-success' href='" . $down_url . "' download><i class='fa fa-download'></i> ডাউনলোড</a></h4>";
            echo '<embed src="' . $down_url . '" style=" width:100%; height: 700px;" type="' . $data['attachment_type'] . '"></embed>';
        }
    } else {
        echo '<div class="text-center"><a href="' . $this->request->webroot . 'content/' . $data['file_name'] . '" data-gallery="multiimages"  data-toggle="lightbox" data-footer="" ><img class="zoomimg img-responsive"  src="' . $this->request->webroot . 'content/' . $data['file_name'] . '?token=' . sGenerateToken(['file' => $data['file_name']], ['exp' => time() + 60 * 300]) . '" alt="ছবি পাওয়া যায়নি"></a></div>';
    }
} else {
    echo "<p class='text-danger text-center'>পাওয়া যায়নি</p>";
}
