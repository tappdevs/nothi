<div class="portlet light">
    <div class="portlet-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form">
                    <h3 class="text-center">
                        রিপোর্ট মডিউলের রিপোর্টসমূহ
                    </h3>
                    <br>

                    <?php echo $this->Form->create(null, ['type' => 'file', 'id' => 'uploadReportForm', 'autocomplete' => 'off']); ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">শিরোনাম</label>&nbsp;<span class="text-danger">*</span>
                                <div><?php echo $this->Form->input('title', array('label' => false, 'class' => 'form-control')); ?></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">বছর</label>&nbsp;<span class="text-danger">*</span>
                                <div><?php echo $this->Form->input('year', array('type'=>'select','label' => false, 'class' => 'form-control','options'=>$year)); ?></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">মাস</label>&nbsp;<span class="text-danger">*</span>
                                <div><?php echo $this->Form->input('month', array('type'=>'select','label' => false, 'class' => 'form-control','options'=>$month)); ?></div>
                            </div>
                        </div>
                    </div>
                    <div class="row hidden">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">অবস্থা</label>&nbsp;<span class="text-danger">*</span>
                                <div><?php echo $this->Form->input('status', array('type'=>'select','label' => false, 'class' => 'form-control','options'=>array('0' => 'নিস্ক্রিয়',1 => 'সক্রিয়'))); ?></div>
                            </div>
                        </div>
                    </div>

                    <h3 class="form-section">
                        <?php echo __(SHONGJUKTI) ?>
                    </h3>
                    <?php if (isset($dak_attachments) && count($dak_attachments) > 0) { ?>
                        <table class="table table-bordered att_tbl" id="att_tbl1">
                            <tbody>
                            <?php
                            $i = 1;
                            foreach ($dak_attachments as $single_data):
                                ?>
                                <tr>
                                    <td width="80%">
                                        <div id="<?php echo 'data_' . $single_data['id']; ?>">
                                            <a class="" href="<?php echo $this->request->webroot . 'content/' . $single_data['file_name'] . '?token=' . sGenerateToken(['file' => $single_data['file_name']], ['exp' => time() + 60 * 300]); ?>"><?php echo $single_data['file_name']; ?></a>
                                        </div>
                                    </td>
                                    <td width="20%" class="text-center">
                                        <span id="file_enable_div_<?php echo $single_data['id']; ?>">
                                            <a target="_blank" href="<?php echo $this->request->webroot . 'content/' . $single_data['file_name'] . '?token=' . sGenerateToken(['file' => $single_data['file_name']], ['exp' => time() + 60 * 300]); ?>"><i class="fs1 a2i_gn_view1"></i></a>
                                            <a href="javascript:;" class="trash_attachment" data-attachment-id="<?php echo $single_data['id']; ?>"><i class="fs1 a2i_gn_delete2"></i></a>
                                        </span>
                                        <span class="file_disable_div" id="file_disable_div_<?php echo $single_data['id']; ?>">
                                            <a href="javascript:;" class="reload_trash_attachment" data-attachment-id="<?php echo $single_data['id']; ?>"><i class="fa fa-edit"></i></a>
                                        </span>
                                    </td>
                                </tr>
                                <?php
                                $i++;
                            endforeach;
                            ?>
                            </tbody>
                        </table>
                    <?php } ?>
                    <?php echo $this->Form->hidden('attachments', ['id' => 'attachments']) ?>
                    <?php echo $this->Form->hidden('uploaded_attachments_names', ['id' => 'uploaded_attachments_names']) ?>
                    <?php echo $this->Form->hidden('file_description', ['id' => 'file_description']) ?>
                    <!--End: Form Buttons -->
                    <?php echo $this->Form->end(); ?>
                    <form id="fileupload" action="<?php echo $this->Url->build(['_name' => 'tempUpload']) ?>"
                          method="POST" enctype="multipart/form-data">

                        <input type="hidden" name="module_type" value="MonthlyReports"/>

                        <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
                        <div class="col-lg-6 form-group hidden">
                            <input type="text" name="file_description_upload" id="file_description_upload"
                                   class="form-control " placeholder="সংযুক্তির বিবরণ">
                        </div>

                        <div class="row fileupload-buttonbar">
                            <div class="col-lg-12">
                                <!-- The fileinput-button span is used to style the file input field as button -->
                                <span class="btn green fileinput-button" title="সর্বোচ্চ ফাইল সাইজ ২০ এমবি হবে"
                                      data-toggle="tooltip">
                                                <i class="fs1 a2i_gn_add1"></i>
                                                <span>
                                                    ফাইল যুক্ত করুন </span>
                                                <input type="file" name="files[]" multiple="">
                                            </span>
                                <button type="button" class="btn red delete hidden">
                                    <i class="fs1 a2i_gn_delete2"></i>
                                    <span>
                                                    সব মুছে ফেলুন </span>
                                </button>
                                <input type="checkbox" class="toggle hidden">
                                <!-- The global file processing state -->
                                <span class="fileupload-process">
                                            </span>
                            </div>
                            <!-- The global progress information -->
                            <div class="col-lg-5 fileupload-progress fade">
                                <!-- The global progress bar -->
                                <div class="progress progress-striped active" role="progressbar"
                                     aria-valuemin="0" aria-valuemax="100">
                                    <div class="progress-bar progress-bar-success" style="width:0%;">
                                    </div>
                                </div>
                                <!-- The extended global progress information -->
                                <div class="progress-extended">
                                    &nbsp;
                                </div>
                            </div>

                        </div>
                        <!-- The table listing the files available for upload/download -->
                        <table role="presentation" class="table table-striped clearfix">
                            <tbody class="files">
                            </tbody>
                        </table>
                    </form>

                </div>
                <hr/>
                <div class="row">
                    <div class="col-md-12 form-group">
                        <button class="btn blue saveNotice">সংরক্ষণ
                        </button>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption">
                            রিপোর্ট মডিউলের রিপোর্টসমূহ
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="well">নিম্নোক্ত লিস্টটি ড্রাগ এন্ড ড্রপের মাধ্যমে সাজানো যায়</div>
                        <div class="table-scrollable">
                            <table class="table table-hover table-bordered" id="datatable_table">
                                <thead>
                                <tr>
                                    <th style="width:10%;" class="text-center">
                                        নম্বর
                                    </th>
                                    <th style="width:60%;" class="text-center">
                                        শিরোনাম
                                    </th>
                                    <th class="text-center heading" style="width:10%;">
                                        বছর
                                    </th>
                                    <th style="width:10%;" class="text-center">
                                        মাস
                                    </th>
                                    <th style="width:10%;"  class="text-center">
                                        কার্যক্রম
                                    </th>
                                </tr>
                                </thead>
                                <tbody>

                                <?php
                                $i = 0;
                                if (!empty($list)) {
                                    foreach ($list as $key => $val) {
                                        $i++;
                                        ?>
                                        <tr data-id="<?= $val['id'] ?>">
                                            <td style="width:10%;" class="text-center" style="font-size: 14px!important;"><?= $this->Number->format($i); ?></td>
                                            <td style="width:60%;" id="update_<?= $val['id'] ?>" class="" style="font-size: 14px!important;">
	                                            <a href="<?= $this->request->webroot.'downloadContentFile?file='.$val['attachment_path'] ?>&name=<?= $val['title']; ?>" title="<?= $val['title']; ?>"><i class="fa fa-download"></i> <?= $val['title']; ?></a>
                                            </td>
                                            <td style="width:10%;" class="text-center" style="font-size: 14px!important;"><?= enTobn($val['year']); ?></td>
                                            <td style="width:10%;" class="text-center" style="font-size: 14px!important;"><?= enTobn($val['month']); ?></td>
                                            <td style="width:10%;" class="text-center" style="font-size: 14px!important;">
                                                <a data-toggle="modal" title="মুছে ফেলুন" data-target="#deleteModal"
                                                   onclick="del(<?= $val['id'] ?>);"><i class="fa fa-times"></i></a>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>

                                </tbody>
                            </table>

                        </div>


                        <div class="modal fade modal-purple height-auto" data-backdrop="static" id="deleteModal"
                             role="dialog">
                            <div class="modal-dialog">

                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title"> ডাক সিদ্ধান্ত বাতিল</h4>
                                    </div>
                                    <div class="modal-body">
                                        <form class="form-group">
                                            <label>আপনি কি সেটিংসটি মুছে ফেলতে চান?</label>
                                            <input type="hidden" class="form-control" id="delete_id">
                                            <input type="hidden" class="form-control" id="update_id">
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-success" onclick="delete_action();">মুছে
                                            ফেলুন
                                        </button>
                                        <button type="button" class="btn btn-danger" data-dismiss="modal">বন্ধ করুন
                                        </button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade modal-purple" id="responsiveOnuccedModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">

                <button type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- End: JavaScript -->
<!-- The blueimp Gallery widget -->
<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
    <div class="slides">
    </div>
    <h3 class="title"></h3>
    <a class="prev">
        ÔøΩ </a>
    <a class="next">
        ÔøΩ </a>
    <a class="close white">
    </a>
    <a class="play-pause">
    </a>
    <ol class="indicator">
    </ol>
</div>
<!-- The template to display files available for download -->
<script id="template-upload" type="text/x-tmpl">
                                                {% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">

    <td>
    <p class="name">{%=file.name%}</p>
    <strong class="error label label-danger"></strong>
    </td>
    <td>
    <p class="size">প্রক্রিয়াকরন চলছে...</p>
    <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
    <div class="progress-bar progress-bar-success" style="width:0%;"></div>
    </div>
    </td>
    <td>
    {% if (!i && !o.options.autoUpload) { %}
    <button class="btn blue start" disabled>
    <i class="fa fa-upload"></i>
    <span>আপলোড করুন</span>
    </button>
    {% } %}
    {% if (!i) { %}
    <button class="btn red cancel">
    <i class="fa fa-ban"></i>
    <span>বাতিল করুন</span>
    </button>
    {% } %}
    </td>
    </tr>
    {% } %}





</script>
<!--The template to display files available for download-->
<script id="template-download" type="text/x-tmpl">
                                                {% for (var i=0, file; file=o.files[i]; i++) { %}
                                                <tr class="template-download fade">
                                                {% if (file.error) { %}
                                                <td class="name" width="30%"><span>{%=file.name%}</span></td>
                                                <td class="size" width="20%"><span>{%=o.formatFileSize(file.size)%}</span></td>
                                                <td class="error" width="30%" colspan="2"><span class="label label-danger">ত্রুটি</span> {%=file.error%}</td>
                                                {% } else { %}
                                                <td class="name" width="40%">
                                                <a href="<?= FILE_FOLDER ?>{%=file.url%}" title="{%=file.name%}" data-gallery="{%=file.thumbnail_url&&'gallery'%}" download="{%=file.name%}">{%=file.name%}</a>
                                                </td>
                                                <td class="size" width="15%"><span>{%=o.formatFileSize(file.size)%}</span></td>
                                                {% } %}
                                                <td class="delete" width="5%" align="right">
                                                <button id="deletethis" onchange="check_title()"
                                                class="btn default btn-sm" data-type="{%=file.delete_type%}" data-url="{%=file.delete_url%}"{% if (file.delete_with_credentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                                                <i class="fa fa-times"></i>
                                                </button>
                                                </td>
                                                </tr>
                                                {% } %}





</script>


<script src="<?php echo CDN_PATH; ?>projapoti-nothi/js/guard_file_upload.js?v=<?= js_css_version ?>" type="text/javascript"></script>
<?= $this->element('froala_editor_js_css') ?>
<script>
    $(document).ready(function () {
        FormFileUpload.init();
        $.FroalaEditor.DefineIcon('attachment-ref', {NAME: 'paperclip'});
        $.FroalaEditor.RegisterCommand('attachment-ref', {
            title: 'সংযুক্ত-রেফ বাছাই করুন',
            type: 'dropdown',
            focus: false,
            undo: false,
            refreshAfterCallback: true,
            options: {
                '--': '--'
            },
            callback: function (cmd, val) {

                if (val.length > 0 && val != '--') {

                    var value_select = $('[id^=dropdown-menu-attachment-ref] ul li a[data-param1="' + val + '"]').text();
                    var file_type = $('[id^=dropdown-menu-attachment-ref] ul li a[data-param1="' + val + '"]').attr('ft');

                    $.ajax({
                        type: 'POST',
                        url: "<?php
                            echo $this->Url->build(['controller' => 'LoginPageSettings',
                                'action' => 'save_login_notice_attachment_refs'])
                            ?>",
                        data: {
                            "type": file_type,
                            "user_file_name": value_select,
                            "file_name": val
                        },
                        success: function (data) {
                            if (data.status == 'success') {
                                var id_nothi_attachment_ref = data.id;
                                $('#description').froalaEditor('html.insert', "<a  class='NoticePopup' value=" + id_nothi_attachment_ref + "  href='" + id_nothi_attachment_ref + "/notice-ref' title='" + value_select + "'>" + value_select + "</a>, &nbsp;");

                            } else {

                            }
                        }
                    });
                }
            }
        });
        var buttons = ['bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', '|', 'fontFamily', 'fontSize', 'color', 'inlineStyle', 'paragraphStyle', '|', 'paragraphFormat', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote', 'insertLink', 'insertTable', '|', 'insertHR', 'clearFormatting', '|', 'print', 'undo', 'redo', 'attachment-ref'];
        $('#description').froalaEditor({
            key: 'xc1We1KYi1Ta1WId1CVd1F==',
            toolbarSticky: false,
            wordPasteModal: true,
			wordAllowedStyleProps: ['font-size', 'background', 'color', 'text-align', 'vertical-align', 'background-color', 'padding', 'margin', 'height', 'margin-top', 'margin-left', 'margin-right', 'margin-bottom', 'text-decoration', 'font-weight'],
			wordDeniedTags: ['a','form'],
			wordDeniedAttrs: ['width'],
            tableResizerOffset: 10,
            tableResizingLimit: 20,
            toolbarButtons: buttons,
            toolbarButtonsMD: buttons,
            toolbarButtonsSM: buttons,
            toolbarButtonsXS: buttons,
            placeholderText: '',
            height: 300,
            enter: $.FroalaEditor.ENTER_BR,
            fontSize: ['10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30'],
            fontSizeDefaultSelection: '13',
            tableEditButtons: ['tableHeader', 'tableRemove', '|', 'tableRows', 'tableColumns', 'tableCells', 'tableCellBackground', 'tableCellVerticalAlign', 'tableCellHorizontalAlign', 'tableCellStyle'],
            fontFamily: {
                "Nikosh,SolaimanLipi,'Open Sans', sans-serif": "Nikosh",
                "Arial": "Arial",
                "Kalpurush": "Kalpurush",
                "SolaimanLipi": "SolaimanLipi",
                "'times new roman'": "Times New Roman",
                "'Open Sans Condensed',sans-serif": 'Open Sans Condensed'
            }
        });
    });

    function check_title() {
        $('[id^=dropdown-menu-attachment-ref] ul').html('');
        $('[id^=dropdown-menu-attachment-ref] ul').append($('<li role="presentation"><a class="fr-command" tabindex="-1" role="option" data-cmd="attachment-ref" data-param1="" title="" aria-selected="false">--</a></li>'));

        $('#fileupload .note-attachment-input').each(function () {
            var tx = $(this).val();
            var file_handle = $(this).attr('image');
            var file_type = $(this).attr('file-type');

            $('[id^=dropdown-menu-attachment-ref] ul').append('<li role="presentation"><a class="fr-command" tabindex="-1" role="option" data-cmd="attachment-ref" data-param1="' + file_handle + '" ft="' + file_type + '" title="" aria-selected="false">' + tx + '</a></li>');

//            $('#note-attachment-title').append($("<option value='" + file_handle + "' ft='" + file_type + "'></option>").text(tx));
        });
        if (parseInt($('#noteId').val()) > 0) {
            $.each($('#responsiveNoteEdit').next().next().find('.preview_attachment a'), function (i, v) {
                var tx = '';
                var d_text = 'No_user_input_file';
                if ($(this).attr('user_file_name') == d_text) {
                    tx = v.text;
                } else {
                    tx = $(this).attr('user_file_name');
                }
                var file_type = v.type;
                var file_handle = ' <?= FILE_FOLDER ?>' + v.download;

                $('[id^=dropdown-menu-attachment-ref] ul').append('<li role="presentation"><a class="fr-command" tabindex="-1" role="option" data-cmd="attachment-ref" data-param1="' + file_handle + '" ft="' + file_type + '" title="" aria-selected="false">' + tx + '</a></li>');

            });
        }
    }

    $('.saveNotice').click(function () {
        if (isEmpty($("#title").val())) {
            toastr.error('শিরোনাম লিখুন');
            return;
        }
        var attached_files = {};
        var i = 0;

        $('.template-download').each(function () {
            var link_td = $(this).find('td:first');
            var link_td_last = $(this).find('td:nth-child(3)');
            var tokne_href = $(link_td).find('a').attr('href');
            var href = tokne_href.split('?token');
            var attachment = href[0];
            var path = attachment.split('content/');
            attached_files[i] = path[1];
            i++;
        });

        $("#attachments").val(attached_files[0]);
        $("#uploadReportForm").submit();
    });

    function del(id) {
        $('#delete_id').val(id);
    }

    function update(id) {
        $('#update_id').val(id);
    }

    function delete_action() {
        var id = $('#delete_id').val();
//       console.log(id);
        $.ajax({
            type: 'POST',
            url: "<?=$this->Url->build(['controller' => 'LoginPageSettings', 'action' => 'monthlyReportDelete']) ?>",
            data: {"id": id},
            success: function (data) {
                window.location.reload();
            }
        });
    }

    function getPopUp(href, title) {
        $('#responsiveOnuccedModal').find('.modal-title').text('');
        $('#responsiveOnuccedModal').find('.modal-body').html('');
        $.ajax({
            type: 'POST',
            url: "<?php echo $this->request->webroot; ?>LoginPageSettings/NoticePopup/" + href,
            data: {},
            success: function (response) {
                $('#responsiveOnuccedModal').modal('show');
                $('#responsiveOnuccedModal').find('.modal-title').text(title);
                $('#responsiveOnuccedModal').find('.modal-body').html(response);
            }
        });

    }

    $(document).off('click', '.NoticePopup').on('click', '.NoticePopup', function (e) {
        e.preventDefault();
        var title = $(this).attr('title').length > 0 ? $(this).attr('title') : $(this).attr('data-original-title');
        getPopUp($(this).attr('href'), title);
    });

    $(document).ready(function() {
        $('#fileupload').fileupload({
            disableImageResize: false,
            autoUpload: true,
            maxFileSize: 20099999,
            maxNumberOfFiles:1,
            acceptFileTypes: /(\.|\/)(jpe?g|png|pdf|doc|docx)$/i,
            // Uncomment the following to send cross-domain cookies:
            //xhrFields: {withCredentials: true},
        });
    });

    $(document).ready(function () {
        $( "tbody" ).sortable({
            // Cancel the drag when selecting contenteditable items, buttons, or input boxes
            cancel: ":input,button,[contenteditable]",
            // Set it so rows can only be moved vertically
            axis: "y",
            // Triggered when the user has finished moving a row
            update: function (event, ui) {
                // sortable() - Creates an array of the elements based on the element's id.
                // The element id must be a word separated by a hyphen, underscore, or equal sign. For example, <tr id='item-1'>
                var data = $(this).sortable('serialize');

                var new_order = [];
                $.each($(this).children('tr'), function(i, j) {
                    new_order[i] = $(this).attr('data-id');
                });
                //console.log(new_order);
                //console.log(data); //<- Uncomment this to see what data will be sent to the server

                // AJAX POST to server
                $.ajax({
                    data: {'data':new_order},
                    type: 'POST',
                    url: js_wb_root+'monthlyReportReOrder',
                    success: function(response) {
                        // alert(response); <- Uncomment this to see the server's response
                    }
                });
            }
        });
    });
</script>