<div class="portlet light">
    <div class="portlet-body">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption">
                            রিপোর্ট মডিউলের রিপোর্টসমূহ
                        </div>
                    </div>
                    <div class="portlet-body">
                        <form action="" method="post" class="form-horizontal">
                            <div class="col-md-4">
                                <div class="form-group" style="width:100%;">
                                    <label class="control-label">বছর</label>&nbsp;<span class="text-danger">*</span>
                                    <div><?php echo $this->Form->input('year', array('type'=>'select','label' => false, 'class' => 'form-control','options'=>$yearList, 'value'=>$year)); ?></div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group" style="width:100%;">
                                    <label class="control-label">মাস</label>&nbsp;<span class="text-danger">*</span>
                                    <div><?php echo $this->Form->input('month', array('type'=>'select','label' => false, 'class' => 'form-control','options'=>array('01' => 'জানুয়ারী', '02' => 'ফেব্রুয়ারী', '03' => 'মার্চ', '04' => 'এপ্রিল', '05' => 'মে', '06' => 'জুন', '07' => 'জুলাই', '08' => 'আগস্ট', '09' => 'সেপ্টেম্বর', '10' => 'অক্টোবর', '11' => 'নভেম্বর', '12' => 'ডিসেম্বর'), 'value'=>$month)); ?></div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input type="submit" class="btn btn-success" style="margin-top: 28px;" name="btnSearch" value="খুঁজুন">
                                </div>
                            </div>
                        </form>
                        <div class="table-scrollable">
                            <table class="table table-hover table-bordered" id="datatable_table">
                                <thead>
                                <tr>
                                    <th style="width:10%;" class="text-center">
                                        নম্বর
                                    </th>
                                    <th style="width:60%;" class="text-center">
                                        শিরোনাম
                                    </th>
                                    <th class="text-center heading" style="width:10%;">
                                        বছর
                                    </th>
                                    <th style="width:10%;" class="text-center">
                                        মাস
                                    </th>
                                    <th style="width:10%;" class="text-center">
                                        ডাউনলোড
                                    </th>
                                </tr>
                                </thead>
                                <tbody>

                                <?php
                                $i = 0;
                                if (!empty($list)) {
                                    foreach ($list as $key => $val) {
                                        $i++;
                                        ?>
                                        <tr data-id="<?= $val['id'] ?>">
                                            <td style="width:10%;" class="text-center" style="font-size: 14px!important;"><?= $this->Number->format($i); ?></td>
                                            <td style="width:60%;" id="update_<?= $val['id'] ?>" class="" style="font-size: 14px!important;"><?= $val['title']; ?></td>
                                            <td style="width:10%;" class="text-center" style="font-size: 14px!important;"><?= enTobn($val['year']); ?></td>
                                            <td style="width:10%;" class="text-center" style="font-size: 14px!important;"><?= enTobn($val['month']); ?></td>
                                            <td style="width:60%;" id="update_<?= $val['id'] ?>" class="" style="font-size: 14px!important;">
                                                <a href="<?= $this->request->webroot.'downloadContentFile?file='.$val['attachment_path'] ?>&name=<?= $val['title']; ?>" title="<?= $val['title']; ?>"><i class="fa fa-download"></i> ডাউনলোড</a>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>

                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>