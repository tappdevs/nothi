<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption"><i class=""></i><?php echo __('Division') ?> <?php echo __('Edit') ?> </div>

    </div>
    <div class="portlet-body form"><br><br>
    <?php echo $this->Form->create($nothi_disposals, array('id' => 'nothi_disposals')); ?>
        <div class="form-horizontal" id ="nothi_disposals">
			<div class="form-group">
				<label class="col-sm-4 control-label">নথি শ্রেণি</label>

				<div class="col-sm-3">
					<?php echo $this->Form->input('disposal_name', array('label' => false, 'class' => 'form-control characters_only', 'placeholder' => 'নথি শ্রনি ' )); ?>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-4 control-label">নথি নিষ্পত্তি'র দিন</label>

				<div class="col-sm-3">
					<?php echo $this->Form->input('disposal_tenure', array('label' => false, 'class' => 'form-control', 'placeholder' => 'নথি নিষ্পত্তির দিন - (ইংরেজি সংখ্যায়) ')); ?>
				</div>
			</div>
		<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<div id="messages"></div>
				</div>
		</div>
		
        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-4 col-md-9">
                    <button class="btn   blue" type="submit"><?php echo __('Submit') ?></button>
                    <button class="btn   default" type="reset"><?php echo __('Reset') ?></button>
                </div>
            </div>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
    $('#nothi_disposals').bootstrapValidator({
        container: '#messages',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
             disposal_name: {
                validators: {
                    notEmpty: {
                        message: 'নথি নিষ্পত্তির শ্রনি লেখা হয়নি'
                    }
                }
            },
            disposal_tenure: {
                validators: {
                    notEmpty: {
                        message: 'নথি নিষ্পত্তির সর্বোচ্চ দিন - (ইংরেজি সংখ্যায়) লেখা হয়নি'
                    },
					greaterThan:{
						value: 1,
						message: 'অব্যশই ১ এর চেয়ে অধিক হবে'
					},
					integer: {
                        message: ' ইংরেজি সখ্যায় লিখুন '
                    }
					
				}
			
            }
            
        }
    });
});
</script>