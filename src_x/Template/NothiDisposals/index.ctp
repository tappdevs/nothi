<div class="portlet light">
    <div class="portlet-title">
        <div class="caption"><i class="fs1 a2i_gn_details1"></i>নথি শ্রেণি </div>

    </div>
    <div class="portlet-body">
        <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                        <?= $this->Html->link(('নতুন শ্রনি'), ['action' => 'add'], ['class' => 'btn btn-default']) ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="btn-group pull-right">
                        <button class="btn dropdown-toggle" data-toggle="dropdown"><?php echo __("Tools"); ?> <i
                                class="glyphicon glyphicon-chevron-down"></i></button>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="#"><?php echo __("Print"); ?></a></li>
                            <li><a href="#"><?php echo __("Save as PDF"); ?></a></li>
                            <li><a href="#">Export to Excel</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th class="text-center">ক্রমিক</th>
                <th class="text-center">নথি শ্রেণি</th>
                <th class="text-center">কাল</th>
                <th class="text-center"><?php echo __("Actions"); ?></th>
            </tr>
            </thead>
            <tbody>

            <?php
			$i=1;
			
            foreach ($query as $rows):
			$x = $this->convertNumberToBengali($i++);
			$tenure= $rows['disposal_tenure'];
            if($tenure==0)
				$tenure="স্থায়ী";
			else
				$tenure=$tenure." দিন";
			?>
            <tr>
                <td class="text-center"><?php echo $x++; ?></td>
                <td class="text-center"><?php echo $rows['disposal_name']; ?></td>
                <td class="text-center"><?php echo $tenure; ?></td>

                <td class="text-center">
                    <!--<?= $this->Form->postLink(
                        'Delete',
                        ['action' => 'delete', $rows->id],
                        ['class' => 'btn btn-danger'],
                        ['confirm' => 'Are you sure to delete this item?'])
                    ?>-->
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $rows->id], ['class' => 'btn btn-primary']) ?>
                    <!--<?= $this->Html->link('View', ['action' => 'view', $rows->id], ['class' => 'btn btn-success']) ?>-->

                </td>
                <?php
                endforeach;
                ?>
            </tr>
            </tbody>
        </table>
    </div>
</div>
