<style>
    .inbox .inbox-nav li.active b {
        display: none !important;
    }

    .tooltip-inner {
        white-space: pre-line;
    }

    .nav-tabs > li > a, .nav-pills > li > a {
        font-size: 11pt;
    }

    h3 {
        margin-top: 0px;
        margin-bottom: 0px;
    }

</style>
<div class="portlet light">
	<div class="portlet-title">
		<div class="caption">
			<i class="fs0 efile-add4"></i> নথি তৈরি
		</div>
	</div>
    <div class="portlet-body">
        <!--<h4 class="text-center"> নতুন নথি তৈরি</h4>-->

        <div class="row">
            <div class="col-md-12">
                <?php
                echo $this->element('NothiMasters/nothi_masters_add');
                ?>
            </div>
        </div>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-12 text-left">
                    <input type="submit" class="btn btn-primary nothiCreateButton round-corner-5" value="<?php echo SAVE ?>"/>
                    <input type="button" style="display: none;" data-nothijato="<?php echo isset($nothijato) ? $nothijato : 0 ?>" class="btn btn-danger bntNothiListShow round-corner-5" value="ফেরত যান"/>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

    $('.nothiCreateButton').on('click', function () {

        toastr.options = {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-bottom-right"
        };

        if ($('#nothi-master-id').val() > 0) {
        } else {
            var nothitype = $('select[name=nothi_types_id]').val();

            if (nothitype <= 0) {
                toastr.error("নথির ধরন দেয়া হয়নি");
                $("#nothi-types-id").select2('open');
                return false;
            }

            <?php if($nothi_no_1st_part_edit): ?>
                var nothi_no = ($("#nothi_number_1st_part").text())+($("#nothi_number_2nd_part").val());
            <?php else: ?>
                var nothi_no = $("#nothi_number").val();
            <?php endif; ?>

            if (!(nothi_no.replace(/[\.\*]/g, '').length==18)) {
                toastr.error('দুঃখিত! নথি নম্বর সঠিক নয়। নথি নম্বর ১৮ ডিজিটের হতে হবে ');
                $("#nothi_number_2nd_part").focus();
                $("#nothi_number").focus();
                return false;
            } else {
                $("#nothi-no").val(nothi_no);
            }
            if (isEmpty($("#nothi-class").val())) {
                toastr.error('দুঃখিত! নথির শ্রেণি নির্বাচন করুন');
                $("#nothi-class").select2('open');
                return;
            }

            if ($('.optionsRadios:checked').length == 0) {
                toastr.error("নথিতে অনুমতি দেয়া হয়নি");
                return false;
            }
        }

        if ($('.optionsRadios:checked').length == 0) {
            toastr.error("নথিতে অনুমতি দেয়া হয়নি");
            return false;
        }


        var selectedPriviliges = {};
        var j = 0;
        $.each($('#ownOfficeSelected').find('.own_office_permission'), function (i, v) {
            selectedPriviliges[j] = {};
            selectedPriviliges[j]['ofc_id'] = $(v).data('office-id');
            selectedPriviliges[j]['unit_id'] = $(v).data('office-unit-id');
            selectedPriviliges[j]['org_id'] = $(v).data('office-unit-organogram-id');
//            selectedPriviliges[j]['designation_level'] = $(v).data('designation-level');
            selectedPriviliges[j]['perm_value'] = 1;//parseInt($(v).val());
            j = j + 1;
        });
        $.each($('#ownOfficeSelected').find('tbody>tr'), function (i, v) {
            if ($(v).find('#designation_level').val() == '') {
                $(v).find('#designation_level').val(0);
            }
            selectedPriviliges[i]['designation_level'] = $(v).find('#designation_level').val();
        });
        $.each($('#otherOfficeSelected').find('tbody>tr'), function (i, v) {
            selectedPriviliges[i + j] = {};
            if ($(v).find('#designation_level').val() == '') {
                $(v).find('#designation_level').val(0);
            }
            selectedPriviliges[i + j]['designation_level'] = $(v).find('#designation_level').val();
        });
        $.each($('#otherOfficeSelected').find('.other_office_permission'), function (i, v) {
//            selectedPriviliges[j] = {};
            selectedPriviliges[j]['ofc_id'] = $(v).data('office-id');
            selectedPriviliges[j]['unit_id'] = $(v).data('office-unit-id');
            selectedPriviliges[j]['org_id'] = $(v).data('office-unit-organogram-id');
//            selectedPriviliges[j]['designation_level'] = $(v).data('designation-level');
            selectedPriviliges[j]['perm_value'] = 1;//parseInt($(v).val());
            j = j + 1;
        });


        // NEED to ADD PROMISE FOR AJAX
        $('.nothiCreateButton').attr('disabled', 'disabled');
        $.ajax({
            url: $('#NothiCreateForm').attr('action'),
            data: {formData: $('#NothiCreateForm').serializeArray(), priviliges: selectedPriviliges},
            type: "POST",
            dataType: 'JSON',
            success: function (response) {
                $('div.error-message').remove();
                if (response.status == 'error') {
                    $('.nothiCreateButton').removeAttr('disabled');
                    //toastr.error(response.msg);

                    if (typeof (response.data) != 'undefined') {
                        var errors = response.data;
                        $.each(errors, function (i, value) {
                            $.each(value, function (key, val) {
                                if (i != 'office_units_organogram_id') {
                                    if(i == 'nothi_no') {
                                        $('#nothi_number_2nd_part').focus();
                                        $('#nothi_number').focus();
                                    }
                                    $('[name=' + i + ']').focus();
                                    $('[name=' + i + ']').after('<div class="error-message">' + val + '</div>');
                                    toastr.error(val);
                                }
                            });
                        });
                    }
                } else if (response.status == 'success') {
                    toastr.success(response.msg);
                    $('#NothiCreateForm')[0].reset();
                    $('.nothiCreateButton').removeAttr('disabled');

                    if(!isEmpty(response.extra_message)){
                        bootbox.dialog({
                            message: response.extra_message,
                            title: "নথি তৈরির সময়কার সমস্যা",
                            buttons: {
                                success: {
                                    label: "সকল নথি তালিকা",
                                    className: "green",
                                    callback: function () {
                                        redirect('<?=$title?>');
                                    }
                                },
//                                danger: {
//                                    label: "না",
//                                    className: "red",
//                                    callback: function () {
//                                        Metronic.unblockUI('.page-container');
//                                    }
//                                }
                            }
                        });
                         return;
                    }

                    redirect('<?=$title?>');

                }
            },
            error: function (status, xresponse) {

            }

        });


        return false;
    });
    function redirect(title){
        if (title == 'edit') {
            window.location.href = '<?php echo $this->request->webroot ?>nothiMasters/index';
        }

        if ($('.bntNothiListShow').css('display') != 'none') {
            $('.bntNothiListShow').click();
        } else {
            window.location.href = '<?php echo $this->request->webroot ?>nothiMasters/index';
        }
    }
</script>

<script>
	$(document).ready(function () {
        listown();
        listownother();
    });
    function listown() {
        $('#ownOfficeSelected').find('tbody').html('');
        $.each($('.permissiondiv').find('.optionsRadios:checked'), function (i, v) {
            var ok = '';
            var id = escapeHtml($(v).data('employee-id'));
            var name = escapeHtml($(v).data('employee-name'));
            var of_id = escapeHtml($(v).data('employee-office-id'));
            var unit_id = escapeHtml($(v).data('office-unit-id'));
            var org_id = escapeHtml($(v).data('office-unit-organogram-id'));
            var designation = escapeHtml($(v).data('designation-name'));
            var unit_name = escapeHtml($(v).data('unit-name'));
            $.each($('#ownOfficeSelected').find('tbody>tr'), function (ind, val) {
                if ($(val).find('.own_office_permission').data('office-unit-organogram-id') == org_id) {
                    designation_level = escapeHtml($(val).find('#designation_level').val());
                }
            });
            var designation_level = escapeHtml($(v).data('designation-level'));

            var perm_value = parseInt($(v).val());
            ok += '<tr><td style="width: 30px;text-align: center;">&nbsp;&nbsp;';
            ok += '<input type="hidden" class="own_office_permission" name="office-employee" data-office-id="' + of_id + '"' +
                'data-office-employee-name="' + name + '" data-office-employee-id="' + id + '"' +
                ' data-office-unit-id="' + unit_id + '" data-office-unit-organogram-id="' + org_id + '" ' +
                'data-designation-name="' + designation + '" data-unit-name="' + unit_name + '" data-designation-level ="' + designation_level + '">' +
                '</td><td>' + name + ', ' + designation + ', ' + unit_name + '</td>' +
                '<td><input id="designation_level" type="text" value="' + designation_level + '" style="width:50px;"></td></tr>';

            $('#ownOfficeSelected').find('tbody').append(ok);
            $('#ownOfficeTotalSelected').html( enTobn($('#ownOfficeSelected').find('tbody>tr').length) );
        });
    }
    function listownother() {
        var ok = '';
        $.each($('.otherPermissionList').find('.optionsothers:checked'), function (i, v) {

            var id = escapeHtml($(v).data('employee-id'));
            var name = escapeHtml($(v).data('employee-name'));
            var of_id = escapeHtml($(v).data('employee-office-id'));
            var unit_id = escapeHtml($(v).data('office-unit-id'));
            var org_id = escapeHtml($(v).data('office-unit-organogram-id'));
            var designation = escapeHtml($(v).data('designation-name'));
            var unit_name = escapeHtml($(v).data('unit-name'));
            var designation_level = escapeHtml($(v).data('designation-level'));
//            var designation_level = 0;
            var perm_value = parseInt($(v).val());
            ok += '<tr><td style="width: 30px;text-align: center;">&nbsp;&nbsp;';
            ok += '<input type="hidden" class="other_office_permission" name="office-employee" data-office-id="' + of_id + '"' +
                'data-office-employee-name="' + name + '" data-office-employee-id="' + id + '"' +
                ' data-office-unit-id="' + unit_id + '" data-office-unit-organogram-id="' + org_id + '" ' +
                'data-designation-name="' + designation + '" data-unit-name="' + unit_name + '" data-designation-level ="' + designation_level + '">' +
                '</td><td>' + name + ', ' + designation + ', ' + unit_name + '</td><td><input id="designation_level" type="text" value="' + designation_level + '" style="width:50px;"></td></tr></tr>';

            $('#otherOfficeTotalSelected').html( enTobn(parseInt(bnToen($('#otherOfficeTotalSelected').html())) + 1) );
        });
        $('#otherOfficeSelected').find('tbody').html(ok);
    }
    function removeDuplicateRows($table) {
        function getVisibleRowText($row) {
            return $row.find('td:visible').text().toLowerCase();
        }

        $table.find('tr').each(function (index, row) {
            var $row = $(row);

            $row.nextAll('tr').each(function (index, next) {
                var $next = $(next);
                if (getVisibleRowText($next) == getVisibleRowText($row))
                    $next.remove();
            });
        });
    }
    function list_all(e_id) {
        $.each($('.otherPermissionList').find('.optionsothers'), function (i, v) {
            if ($(v).data('office-unit-organogram-id') == e_id) {
                if ($(v).is(':checked')) {
                    if ($('#otherOfficeSelected').find('tbody').find('.other_office_permission[data-office-unit-organogram-id='+e_id+']').length > 0) {
                        toastr.error('ইতোমধ্যে নির্বাচন করা হয়েছে');
                    } else {
                        var ok = '';
                        var id = escapeHtml($(v).data('employee-id'));
                        var name = escapeHtml($(v).data('employee-name'));
                        var of_id = escapeHtml($(v).data('employee-office-id'));
                        var unit_id = escapeHtml($(v).data('office-unit-id'));
                        var org_id = escapeHtml($(v).data('office-unit-organogram-id'));
                        var designation = escapeHtml($(v).data('designation-name'));
                        var unit_name = escapeHtml($(v).data('unit-name'));
                        var designation_level = escapeHtml($(v).data('designation-level'));
                        var perm_value = escapeHtml(parseInt($(v).val()));
                        ok += '<tr><td style="width: 30px;text-align: center;">&nbsp;&nbsp;';
                        ok += '<input type="hidden" class="other_office_permission" name="office-employee" data-office-id="' + of_id + '"' +
                            'data-office-employee-name="' + name + '" data-office-employee-id="' + id + '"' +
                            ' data-office-unit-id="' + unit_id + '" data-office-unit-organogram-id="' + org_id + '" ' +
                            'data-designation-name="' + designation + '" data-unit-name="' + unit_name + '" data-designation-level ="' + designation_level + '">' +
                            '</td><td>&nbsp;&nbsp;' + name + ', ' + designation + ', ' + unit_name + '</td><td><input id="designation_level" type="text" value="' + designation_level + '" style="width:50px;"></td></tr></tr>';
                        $('#otherOfficeSelected').find('tbody').append(ok);
                    }
                    $('#otherOfficeTotalSelected').html( enTobn($('#otherOfficeSelected').find('tbody>tr').length) );
                } else {
                    $('#otherOfficeSelected').find('tbody').html(ok);
                    $.each($('#otherOfficeSelected').find('tbody').find('.other_office_permission'), function () {
                        if ($(this).data('office-unit-organogram-id') == e_id) {
                            $(this).closest('tr').remove();
                            $('#otherOfficeTotalSelected').html( enTobn($('#otherOfficeSelected').find('tbody>tr').length) );
                        }
                    });
                }
            }
        });
    }
</script>