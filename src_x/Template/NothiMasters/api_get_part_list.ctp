<table class="table table-hover table-bordered ">
    <thead>
    <tr class="text-center">
        <th class="text-center" style="width: 40%">বিষয়</th>
        <th class="text-center" style="width: 20%">গ্রহণের সময়</th>
        <th class="text-center" style="width: 30%">বর্তমান ডেস্ক</th>
        <th class="text-center">বিস্তারিত</th>
    </tr>
    </thead>
    <tbody>
    <?php
    if(!empty($nothiLists)){
    foreach ($nothiLists as $nothiList){ ?>
        <tr class="<?=$nothiList['class'] ?>">
            <td data-search-term="<?= h($nothiList['NothiParts']['nothi_part_no_bn']).':'.h($nothiList['subject']) ?>"><?=h($nothiList['NothiParts']['nothi_part_no_bn']).') '.h($nothiList['subject']) ?></td>
            <td><?= h($nothiList['issue_date']) ?></td>
            <td><?=h($nothiList['current_desk'])?></td>
            <td class='text-center'>
                <?php
                if($nothiList['details'] == 1){
                    ?>
                    <a title='বিস্তারিত' class='btn btn-success btn-xs' data-title='বিস্তারিত' href="<?=$this->request->webroot; ?>noteDetail/<?= $nothiList['NothiParts']['id'] ?>"><i class='a2i_gn_details2'></i></a>
                    <?php
                }else{ ?>
                    আপনার ডেস্ক এ আসেনি
                <?php } ?>
            </td>
        </tr>
    <?php }} else { ?>
        <tr>
            <td class="text-center" colspan="4" style="color:red;">কোনো তথ্য পাওয়া যায়নি!
            </td>
        </tr>
   <?php } ?>
    </tbody>
</table>
<div class="actions text-center">
    <ul class="pagination pagination-sm">
        <?php
        echo $this->Paginator->first(__('প্রথম', true), array('class' => 'number-first'));
        echo $this->Paginator->prev('<<', array('tag' => 'li', 'escape' => false), '<a href="#" class="btn btn-sm blue">&laquo;</a>', array('class' => 'prev disabled btn btn-sm blue', 'tag' => 'li', 'escape' => false));
        echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a', 'reverse' => FALSE));
        echo $this->Paginator->next('>>', array('tag' => 'li', 'escape' => false), '<a href="#" class="btn btn-sm blue ">&raquo;</a>', array('class' => 'next disabled', 'tag' => 'li', 'escape' => false));
        echo $this->Paginator->last(__('শেষ', true), array('class' => 'number-last'));
        ?>
    </ul>

</div>

<script>
    jQuery(document).ready(function () {
        $(document).off('click','.pagination a').on('click','.pagination a',function(ev){
            ev.preventDefault();
            PROJAPOTI.ajaxLoad($(this).attr('href'),'.details-modal-inbox-content');
        });
    });
</script>

