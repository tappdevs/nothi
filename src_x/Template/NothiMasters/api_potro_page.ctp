<?= $this->Html->css('assets/global/css/nothi-custom') ?>
<?= $this->Html->script('assets/global/scripts/nothi-custom') ?>
<style>
    .portlet-body img {
        max-width: 100%;
        height: auto;
    }

    div.attachmentsRecord {
        display: none;
    }

    div.attachmentsallRecord {
        display: none;
    }

    div.attachmentsRecord.active {
        display: block;
    }

    div.attachmentsallRecord.active {
        display: block;
    }

    div.DraftattachmentsRecord {
        display: none;
    }

    div.DraftattachmentsRecord.active {
        display: block;
    }

    div.NothijatoattachmentsRecord {
        display: none;
    }

    div.NothijatoattachmentsRecord.active {
        display: block;
    }

    div.summaryDraftattachmentsRecord {
        display: none;
    }

    div.summaryDraftattachmentsRecord.active {
        display: block;
    }

    .pager li > a, .pager li > span {
        display: inline-block;
        padding: 5px 8px;
        background-color: #fff;
        border: 1px solid #ddd;
        border-radius: 0;
    }

    .potroview {
        margin: 0 auto;
    }

    div.potroAccordianPaginationBody {
        display: none;
    }

    div.potroAccordianPaginationBody.active {
        display: block;
    }

    a {
        cursor: pointer;
    }

    .editable {
        border: none;
        word-break: break-word;
        word-wrap: break-word
    }

    #potrodraft_list #sovapoti_signature, #potrodraft_list #sender_signature, #potrodraft_list #sender_signature2, #potrodraft_list #sender_signature3 {
        visibility: hidden;
    }

    #potrodraft_list #sovapoti_signature_date, #potrodraft_list #sender_signature_date, #potrodraft_list #sender_signature2_date, #potrodraft_list #sender_signature3_date {
        visibility: hidden;
    }

    .showimageforce #sovapoti_signature, .showimageforce #sender_signature, .showimageforce #sender_signature2, .showimageforce #sender_signature3,
    .showimageforce #sovapoti_signature_date, .showimageforce #sender_signature_date, .showimageforce #sender_signature2_date, .showimageforce #sender_signature3_date {
        visibility: visible!important;
    }

    #sarnothidraft_list img[alt=signature] {
        visibility: hidden;
    }

    .showImage img {
        visibility: visible!important;
    }

    #template-bodybody img[alt=signature] {
        visibility: hidden;
    }

    #note {
        margin-left: 0px !important;
        overflow-wrap: break-word;
        word-wrap: break-word;

        /* Adds a hyphen where the word breaks */
        -webkit-hyphens: auto;
        -ms-hyphens: auto;
        -moz-hyphens: auto;
        hyphens: auto;
    }

    .bangladate {
        border-bottom: 1px solid #000;
    }
</style>
<?php
$bookMarkLi = '';
$currentLi = array();
//if there is cs templates we need to add an extra js file
$hasCsTemplate = 0;
?>

<div class="portlet full-height-content ">

    <div class="portlet-body">
        <div class="row divcenter">

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 potroContentSide">
                <div class="potroDetailsPage full-height-content-body " data-always-visible="1"
                     data-rail-visible1="1">
                    <div class="tabbable-custom nav-justified">
                        <ul class="nav nav-tabs nav-justified potroview_tab">

                            <?php if (!empty($draftSummary) || !empty($draftPotro)):
                                ?>
                                <li class="dropdown active">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:;"
                                       aria-expanded="true">খসড়া&nbsp;<b class="caret"></b></a>
                                    <ul class="dropdown-menu" role="menu">

                                        <?php if (!empty($draftSummary)): ?>
                                            <li class="active">
                                                <a href="#sarnothidraft_list" data-toggle="tab">
                                                    খসড়া সার-সংক্ষেপ
                                                </a>
                                            </li>
                                        <?php endif; ?>
                                        <?php if (!empty($draftPotro)): ?>
                                            <li>
                                                <a href="#potrodraft_list" data-toggle="tab">
                                                    খসড়া পত্র
                                                </a>
                                            </li>
                                        <?php endif; ?>
                                    </ul>
                                </li>
                            <?php endif; ?>

                            <?php if (!empty($summaryAttachmentRecord)): ?>
                                <li class="<?php
                                echo(empty($draftSummary) && empty($draftPotro) ? 'active' : '')
                                ?>">
                                    <a href="#summaryfinal_list" data-toggle="tab">
                                        সার-সংক্ষেপ
                                    </a>
                                </li>
                            <?php endif; ?>
                            <li class=" <?php
                            echo(empty($draftSummary) && empty($summaryAttachmentRecord) ? (!empty($draftPotro)
                                ? '' : 'active') : '')
                            ?>">
                                <a href="#potroview_list" data-toggle="tab">
                                    নোটের পত্র
                                </a>
                            </li>

                        </ul>
                        <div class="tab-content">
                            <?php if (!empty($draftSummary)): ?>
                                <div class="tab-pane active" id="sarnothidraft_list">
                                    <div class="portlet light ">
                                        <div class="portlet-body" style="overflow: auto;">
                                            <div class="row divcenter">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="potroview" style="width: 7in;overflow-x: scroll;">

                                                        <div style="background-color: #a94442; padding: 5px; color: #fff;">
<!--                                                            <div class="pull-left"
                                                                 style="padding-top: 0px;    white-space: nowrap;">
                                                                স্মারক নম্বর:
                                                                &nbsp;&nbsp;<?php echo $draftSummary['sarok_no']; ?>
                                                            </div>-->
                                                            <div class="pull-left">
                                                                <?php
                                                                if ($privilige_type == 1 && $otherNothi
                                                                    == false
                                                                ):
                                                                    if (!empty($potrosummarydraftstatus) && $employee_office['office_unit_organogram_id']
                                                                        == $potrosummarydraftstatus[$draftSummary['id']]['designation_id']
                                                                    ) {
                                                                        ?>
                                                                        <input type="checkbox" class=" summary_draft_approve <?php
                                                                               echo ($potrosummarydraftstatus[$draftSummary['id']]['is_approve'])
                                                                                   ? 'checked' : ''
                                                                               ?>" <?php
                                                                        echo ($potrosummarydraftstatus[$draftSummary['id']]['is_approve'])
                                                                            ? 'checked=checked'
                                                                            : ''
                                                                        ?>
                                                                               potrojari_id="<?= $draftSummary['id'] ?>"/><?php echo __('অনুমোদন  ') ?>
                                                                        <?php
                                                                    }

                                                                    ?>
                                                                    <a data-title-orginal="সার-সংক্ষেপ জারি করুন"
                                                                       title="সার-সংক্ষেপ জারি করুন"
                                                                       class="btn btn-xs   green <?= ($is_approve ? 'btn-send-summary-draft' : (!empty($potrosummarydraftstatus[$draftSummary['id']]['is_approve']) ? 'btn-check-summary-draft' : 'hide')) ?>"
                                                                       potrojari="<?php echo $draftSummary['id'] ?>">
                                                                        <i class="fs1 a2i_gn_send1"></i> </a>

                                                                <?php endif; ?>
                                                            </div>
                                                            <div style="clear: both;"></div>
                                                        </div>
                                                        <div class="summaryContentBody">
                                                            <div class="tabbable-custom nav-justified">
                                                                <ul class="nav nav-tabs nav-justified">
                                                                    <li class="active">
                                                                        <a href="#tab_cover" data-toggle="tab">
                                                                            কভার পাতা</a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#tab_sar_body" data-toggle="tab">
                                                                            সার-সংক্ষেপ </a>
                                                                    </li>
                                                                </ul>
                                                                <div class="tab-content">
                                                                    <div class="tab-pane active" id="tab_cover">
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <div id="template-body"
                                                                                     style="background-color: #fff; max-width:950px; min-height:815px; height: auto; margin:0 auto;">
                                                                                    <?php echo $draftSummary->potro_cover; ?>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="tab-pane" id="tab_sar_body">
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <div id="template-body2"
                                                                                     style="background-color: #fff; max-width:950px; min-height:815px; height: auto; margin:0 auto;">
                                                                                    <?php echo $draftSummary->potro_description; ?>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <?php if (!empty($draftPotro)): ?>
                                <div class="tab-pane <?php
                                echo(!empty($draftSummary) ? '' : 'active')
                                ?>" id="potrodraft_list">
                                    <div class="portlet light  full-height-content full-height-content-scrollable">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                মোট: <b><?php echo enTobn($this->Number->format(count($draftPotro))); ?>টি</b>
                                            </div>
                                            <div class="actions">
                                                <nav>
                                                    <ul class="pager">
                                                        <li><a href="javascript:void(0);"
                                                               class="DraftimagePrev btn btn-sm default disabled"><</a>
                                                        </li>
                                                        <li><a href="javascript:void(0);"
                                                               class="DraftimageNext btn btn-sm default">></a>
                                                        </li>
                                                    </ul>
                                                </nav>
                                            </div>

                                        </div>
                                        <div class="portlet-body">
                                            <div class="row divcenter" style="overflow: auto;">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="potroview"  style="width: 8in;">
                                                        <?php
                                                        if (!empty($draftPotro)) {
                                                            $i = 0;
                                                            foreach ($draftPotro as $row) {
                                                                echo '<div id_en="' . $row['nothi_potro_page'] . '"  id_bn="' . $row['nothi_potro_page_bn'] . '" class="DraftattachmentsRecord ' . ($i == 0 ? 'active first' : ($i == (count($draftPotro) - 1) ? 'last' : '')) . '">';
                                                                ?>
                                                                <div style="background-color: #a94442; padding: 5px; color: #fff;">
<!--                                                                    <div class="pull-left"
                                                                         style="padding-top: 0px; float:left;   white-space: nowrap;">
                                                                        স্মারক নম্বর:
                                                                        &nbsp;&nbsp;<?php echo $row['sarok_no']; ?>
                                                                    </div>-->
                                                                    <!--<div class="pull-right text-right" style="float: right;">-->
                                                                    <div class="pull-left">
                                                                        <?php
                                                                        if ($privilige_type == 1 && $otherNothi == false):
                                                                            ?>
                                                                            <?php if(!in_array($row['potro_type'],[21,27,30])):?>

                                                                            <button data-title-orginal="খসড়া পত্র দেখুন"
                                                                                    title="খসড়া পত্র দেখুন"
                                                                                    onclick="viewEditDraft('<?php echo $this->request->webroot ?>postApiPotroDraft/<?php
                                                                                    echo $row['nothi_part_no'] . '/' . ($row['id']
                                                                                        == 0 ? ($row['id'] . '/note')
                                                                                            : ($row['id'] . '/potro')) . '/' . $nothi_office
                                                                                    ?>','<?=$employee_office['office_unit_organogram_id']?>')" class="btn    btn-xs green">
                                                                                <i class="fs1 a2i_nt_potrojari3"></i>
                                                                            </button>
                                                                        <?php endif; ?>

                                                                            <?php
                                                                            if ($row['can_potrojari']
                                                                                == 1 && $row['approval_officer_designation_id']
                                                                                == $employee_office['office_unit_organogram_id']
                                                                            ) {
                                                                                ?>
                                                                                <a data-title-orginal="পত্রজারি করুন"
                                                                                        title="পত্রজারি করুন"
                                                                                        class="btn    btn-xs green btn-send-draft-cron"
                                                                                        nothinotesid="<?php echo $row['nothi_notes_id'] ?>"
                                                                                        nothipotroid="<?php echo $row['nothi_potro_id'] ?>"
                                                                                        potrojari="<?php echo $row['id'] ?>">
                                                                                    পত্রজারি
                                                                                </a>
                                                                                <?php
                                                                            } elseif ($row['can_potrojari']
                                                                                == 1 && $row['approval_officer_designation_id']
                                                                                == $row['officer_designation_id']  && $row['potro_type'] != 17
                                                                            ) {
                                                                                ?>
                                                                                <a data-title-orginal="পত্রজারি করুন"
                                                                                        title="পত্রজারি করুন"
                                                                                        class="btn    btn-xs green btn-send-draft-cron"
                                                                                        nothinotesid="<?php echo $row['nothi_notes_id'] ?>"
                                                                                        nothipotroid="<?php echo $row['nothi_potro_id'] ?>"
                                                                                        potrojari="<?php echo $row['id'] ?>">
                                                                                    পত্রজারি
                                                                                </a>

                                                                                <?php
                                                                            }elseif ($row['potro_type']
                                                                                != 17 && $employee_office['office_unit_organogram_id']
                                                                                == $row['officer_designation_id']
                                                                            ) {
                                                                                ?>
                                                                                <input type="checkbox"
                                                                                       class=" approveDraftNothi <?php
                                                                                       echo ($row['can_potrojari'])
                                                                                           ? 'checked' : ''
                                                                                       ?>" <?php
                                                                                echo ($row['can_potrojari'])
                                                                                    ? 'checked=checked'
                                                                                    : ''
                                                                                ?>
                                                                                       potrojari_id="<?= $row['id'] ?>"/><?php echo __('অনুমোদন  ') ?>
                                                                                <?php
                                                                            } else if ($row['potro_type']
                                                                                == 17 && $employee_office['office_unit_organogram_id']
                                                                                == $row['sovapoti_officer_designation_id']
                                                                            ) {
                                                                                ?>
                                                                                <input type="checkbox"
                                                                                       class=" approveDraftNothi <?php
                                                                                       echo ($row['can_potrojari'])
                                                                                           ? 'checked' : ''
                                                                                       ?>" <?php
                                                                                echo ($row['can_potrojari'])
                                                                                    ? 'checked=checked'
                                                                                    : ''
                                                                                ?>
                                                                                       potrojari_id="<?= $row['id'] ?>"/><?php echo __('অনুমোদন  ') ?>
                                                                            <?php }
                                                                        endif;
                                                                        ?>
                                                                    </div>
                                                                    <div style="clear: both;"></div>
                                                                </div>
                                                                <?php
                                                                $header = jsonA($row->meta_data);
                                                                $cs_checker = (($privilige_type == 0)||(isset($header['has_signature']) && $header['has_signature'] == $employee_office['office_unit_organogram_id']))?'':
                                                                    '(<input type="checkbox" class ="checker cs-body-approve" data-cs="'.$row['id'].'" /> '.__('Approval').' )';
                                                                if(isset($row['potro_type']) && $row['potro_type'] == 30){
                                                                    $hasCsTemplate = 1;
                                                                    echo '<ul class="nav nav-tabs nav-justified">
                                                                            <li class="active">
                                                                                <a href="#csCover-'.$row['id'].'" data-toggle="tab" aria-expanded="true">ফরওয়ার্ডিং পত্র</a>
                                                                            </li>
                                                                            <li class="">
                                                                                <a href="#csBody-'.$row['id'].'" data-toggle="tab" aria-expanded="true">প্রতিবেদন '.$cs_checker .'</a>
                                                                            </li>
                                                                        </ul>
                                                                        <div class="tab-content">
                                                                            <div class="tab-pane active csCover" id="csCover-'.$row['id'].'"">'. (!empty($header['potro_header_banner'])?('<div class="potrojari_header_banner" style="text-align: '.($header['banner_position']).'!important;"><img src="'.$this->request->webroot . 'getContent?file='. base64_decode($header['potro_header_banner']).'&token=' . sGenerateToken(['file'=>base64_decode($header['potro_header_banner'])],['exp'=>time() + 60*300]) .'" style="width:'.($header['banner_width']).';height: 60px;max-width: 100%;"/></div>'):'')
                                                                        .'<div class="showimageforce">'. html_entity_decode($row['attached_potro']) . '</div><br/>' .$row['potro_cover'].'</div>';
                                                                    echo '<div class="tab-pane csBody" id="csBody-'.$row['id'].'"">';
                                                                    echo '<div id="template-body" potrojari_language="'.h($row['potrojari_language']).'" style="background-color: #fff; max-width:950px; min-height:815px; height: auto; margin:0 auto;" potro_type="'.$row['potro_type'].'">'
                                                                        . html_entity_decode($row['potro_description']) . "</div>";
                                                                    echo '</div></div>';
                                                                }else{
                                                                    echo '<div id="template-body" potrojari_language="'.h($row['potrojari_language']).'" style="background-color: #fff; max-width:950px; min-height:815px; height: auto; margin:0 auto;" potro_type="'.$row['potro_type'].'">' . (!empty($header['potro_header_banner'])?('<div class="potrojari_header_banner" style="text-align: '.($header['banner_position']).'!important;"><img src="'.$this->request->webroot . 'getContent?file='. base64_decode($header['potro_header_banner']).'&token=' . sGenerateToken(['file'=>base64_decode($header['potro_header_banner'])],['exp'=>time() + 60*300]) .'" style="width:'.($header['banner_width']).';height: 60px;max-width: 100%;"/></div>'):'') .'<div class="showimageforce">'. html_entity_decode($row['attached_potro']) . '</div><br/>' . html_entity_decode($row['potro_description']) . "</div>";
                                                                }
                                                                ?>
                                                                <div class="table">
                                                                    <table role="presentation"
                                                                           class="table table-bordered table-striped">
                                                                        <tbody class="files">
                                                                        <?php
                                                                        if (isset($draftPotroAttachments[$row['id']]) && count($draftPotroAttachments[$row['id']]) > 0) {
                                                                            echo '<tr class="text-center">
                                                                                    <td colspan="3">'.__('Attachments') . '</td>
                                                                                </tr>';
                                                                            foreach ($draftPotroAttachments[$row['id']] as $ke => $single_data) {
                                                                                if ($single_data['attachment_type'] == 'text' || $single_data['attachment_type'] == 'text/html') {
                                                                                    continue;
                                                                                }

                                                                                $fileName = explode('/', $single_data['file_name']);
                                                                                $attachmentHeaders = get_file_type($single_data['file_name']);
                                                                                $value = array(
                                                                                    'name' => urldecode($fileName[count($fileName) - 1]),
                                                                                    'thumbnailUrl' => $this->request->webroot . 'content/' . $single_data['file_name'],
                                                                                    'url' => $this->request->webroot . 'content/' . $single_data['file_name'],
                                                                                    'visibleName' => (!empty($single_data['user_file_name'])?$single_data['user_file_name']:urldecode($fileName[count($fileName) - 1])),
                                                                                );

                                                                                echo '<tr class="template-download fade in">

<td>
<p class="name">
'.(isset($value['name']) && (substr($single_data['attachment_type'],0,5) == 'image' || $single_data['attachment_type'] == 'application/pdf') ?'<a href="'. $single_data['id'] . '/showPotroAttachment/'. $row['nothi_part_no'] .'" title="' . $value['name']. '" class="showforPopup">' . urldecode($value['name']) . '</a>' : (isset($value['name'])?urldecode($value['name']):'')).'
</p>
</td>
<td>'.$value['visibleName'].'</td>
<td>
'. (($single_data['attachment_type'] != 'text' &&  $single_data['attachment_type'] != 'text/html')? $this->Html->link('<i class="fs1 fa fa-download"></i> &nbsp;',['controller' => 'Potrojari',
                                                                                        'action' => 'downloadPotro',
                                                                                        $nothi_office,
                                                                                        $single_data['id']],['escape'=>false]) : '').'
</td>
</tr>';
                                                                            }
                                                                        }
                                                                        ?>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                                <?php
                                                                echo "</div>";
                                                                $i++;

                                                            }
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <?php if (!empty($summaryAttachmentRecord)): ?>
                                <div class="tab-pane <?php echo((empty($draftSummary) && empty($draftPotro)) ? 'active' : '') ?>" id="summaryfinal_list">
                                    <div class="portlet light  full-height-content full-height-content-scrollable">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                মোট: <b><?php $totalSum = !empty($summaryAttachmentRecord)
                                                        ? count($summaryAttachmentRecord)
                                                        : 0;
                                                    echo enTobn($this->Number->format($totalSum)); ?>টি</b>
                                            </div>
                                            <div class="actions">
                                                <nav>
                                                    <ul class="pager">
                                                        <li><a href="javascript:void(0);"
                                                               class="summaryDraftimagePrev btn btn-sm default disabled"><</a>
                                                        </li>
                                                        <li><a href="javascript:void(0);"
                                                               class="summaryDraftimageNext btn btn-sm default">></a>
                                                        </li>
                                                    </ul>
                                                </nav>
                                            </div>

                                        </div>
                                        <div class="portlet-body">
                                            <div class="row divcenter" style="overflow: auto;">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="potroview"  style="width: 8in;">
                                                        <?php
                                                        if (!empty($summaryAttachmentRecord)) {
                                                            $i = 0;
                                                            foreach ($summaryAttachmentRecord as $row) {

                                                                echo '<div id_en="' . $row['nothi_potro_page'] . '"  id_bn="' . $row['nothi_potro_page_bn'] . '" class="summaryDraftattachmentsRecord ' . ($i == 0 ? 'active first' : ($i == (count($summaryAttachmentRecord) - 1) ? 'last' : '')) . '">';
                                                                ?>
                                                                <div style="background-color: #a94442; padding: 5px; color: #fff;">
                                                                    <div class="pull-left"
                                                                         style="padding-top: 0px; float:left;   white-space: nowrap;">
                                                                        স্মারক নম্বর:
                                                                        &nbsp;&nbsp;<?php echo $row['sarok_no']; ?>
                                                                    </div>
                                                                    <div style="clear: both;"></div>
                                                                </div>
                                                                <div style="background-color: #fff; max-width:950px; min-height:815px; margin:0 auto; page-break-inside: auto;">
                                                                    <div style="background-color: #fff; max-width:950px; min-height:815px; ">
                                                                        <div class="tabbable-custom nav-justified">
                                                                            <ul class="nav nav-tabs nav-justified">
                                                                                <li class="active">
                                                                                    <a href="#tab_cover<?= $row['nothi_potro_page'] ?>"
                                                                                       data-toggle="tab">
                                                                                        কভার পাতা</a>
                                                                                </li>
                                                                                <li>
                                                                                    <a href="#tab_sar_body<?= $row['nothi_potro_page'] ?>"
                                                                                       data-toggle="tab">
                                                                                        সার-সংক্ষেপ </a>
                                                                                </li>
                                                                            </ul>
                                                                            <div class="tab-content">
                                                                                <div class="tab-pane active" id="tab_cover<?= $row['nothi_potro_page'] ?>">
                                                                                    <div class="row">
                                                                                        <div class="col-md-12">
                                                                                            <div id="template-body"
                                                                                                 style="background-color: #fff; max-width:950px; min-height:815px; height: auto; margin:0 auto;">
                                                                                                <?php echo html_entity_decode($row['potro_cover']); ?>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="tab-pane" id="tab_sar_body<?= $row['nothi_potro_page'] ?>">
                                                                                    <div class="row">
                                                                                        <div class="col-md-12">
                                                                                            <div id="template-body2"
                                                                                                 style="background-color: #fff; max-width:950px; min-height:815px; height: auto; margin:0 auto;">
                                                                                                <?php echo html_entity_decode($row['content_body']); ?>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <?php
                                                                echo "</div>";
                                                                $i++;

                                                            }
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>

                            <div class="tab-pane <?php
                            echo(empty($draftSummary) && empty($summaryAttachmentRecord) ? (!empty($draftPotro)
                                ? '' : 'active') : '')
                            ?>" id="potroview_list">

                                <div class="portlet light  full-height-content full-height-content-scrollable">
                                    <div class="portlet-title">
                                        <div class="actions">
                                            <nav>
                                                <ul class="pager">
                                                    <li><a href="javascript:void(0);"
                                                           class="imagePrev btn btn-sm default disabled"><</a>
                                                    </li>
                                                    <li>
                                                        <input value="<?php echo enTobn($this->Number->format(!empty($potroAttachmentRecord) ? $potroAttachmentRecord[0]['nothi_potro_page'] : 0)) ?>"
                                                               type="text"
                                                               class="imageNo pagination-panel-input form-control input-mini input-inline input-sm potroNo"
                                                               maxlength="20" style="text-align:center; margin: 0 5px;">
                                                    </li>
                                                    <li><a href="javascript:void(0);"
                                                           class="imageNext btn btn-sm default">></a>
                                                    </li>
                                                </ul>
                                            </nav>
                                        </div>
                                        <div class="caption">
                                            মোট: <b><?php echo enTobn($this->Number->format($total)); ?>টি</b>
                                        </div>

                                    </div>
                                    <div class="portlet-body">
                                        <div class="divcenter" style="overflow: auto;">
                                            <div class="potroview" style="min-width: 8in;">
                                                    <?php
                                                    if (!empty($potroAttachmentRecord)) {
                                                        $i = 0;
                                                        foreach ($potroAttachmentRecord as $row) {
                                                            echo '<div id_ar="' . $row['id'] . '" id_en="' . $row['nothi_potro_page'] . '"  id_bn="' . $row['nothi_potro_page_bn'] . '" class="attachmentsRecord ' . ($i == 0 ? 'active first' : ($i == (count($potroAttachmentRecord) - 1) ? 'last' : '')) . '">';
                                                            ?>
                                                            <div style="background-color: #a94442; padding: 5px; color: #fff;">
                                                                স্মারক নম্বর:
                                                                &nbsp;&nbsp;<?php echo $row['sarok_no']; ?>
                                                            </div>
                                                            <?php

                                                            if ($row['attachment_type'] == 'text') {

                                                                if ($row['is_summary_nothi'] == 1) {
                                                                    echo '<div style="background-color: #fff; max-width:950px; min-height:815px;  margin:0 auto 20px; page-break-inside: auto; margin:0 auto 20px; padding-bottom: 20px;"><div style="background-color: #fff; max-width:950px; min-height:815px; max-height: 815px;">' . html_entity_decode($row['potro_cover']) . "</div>" . html_entity_decode($row['content_body']) . "</div>";
                                                                } else {
                                                                    echo '<div style="background-color: #fff; max-width:950px; min-height:815px;  margin:0 auto 20px; page-break-inside: auto; margin:0 auto 20px; padding-bottom: 20px;">' . html_entity_decode($row['content_body']) . "</div>";
                                                                }
                                                            } elseif (substr($row['attachment_type'], 0, 5) != 'image') {
                                                                if (substr($row['attachment_type'], 0, strlen('application/vnd')) == 'application/vnd' || substr($row['attachment_type'], 0, strlen('application/ms')) == 'application/ms') {
                                                                    $url = urlencode(FILE_FOLDER.$row['file_name'].'?token='.sGenerateToken(['file'=>$row['file_name']],['exp'=> time() + 60*300]));
                                                                    echo '<iframe style=" width:100%; height: 700px;" src="https://docs.google.com/gview?url=' . $url . '&embedded=true" style="width:600px; height:500px;" frameborder="0"></iframe>';
                                                                } else
//                echo '<embed src="' . $this->request->webroot . 'content/' . $row['file_name'] . '" style=" width:100%; height: 700px;" type="' . $row['attachment_type'] . '"></embed>';
                                                                    $url = urlencode(FILE_FOLDER.$row['file_name'].'?token='.sGenerateToken(['file'=>$row['file_name']],['exp'=> time() + 60*300]));
                                                                    echo '<iframe style=" width:100%; height: 700px;" src="https://docs.google.com/gview?url=' . $url . '&embedded=true" style="width:600px; height:500px;" frameborder="0"></iframe>';
                                                            } else {
                                                                echo '<div class="text-center"><a href="' . $this->request->webroot . 'content/' . $row['file_name'] . '" data-gallery="multiimages"  data-toggle="lightbox" data-title="পত্র নম্বর:  ' . $row['nothi_potro_page_bn'] . '" data-footer="" ><img class="zoomimg img-responsive"  src="' . $this->request->webroot . 'content/' . $row['file_name'] .'?token='.sGenerateToken(['file'=>$row['file_name']],['exp'=> time() + 60*300]). '" alt="ছবি পাওয়া যায়নি"></a></div>';
                                                            }
                                                            echo "</div>";
                                                            $i++;
                                                        }
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<div id="modalSummaryNothi" class="modal fade" tabindex="-1" aria-hidden="true" data-backdrop="static"
     data-keyboard="false" style="height: 100%!important;overflow: auto;">
    <div class="modal-dialog modal-full" style="width: 7in;">
        <div class="modal-content">
            <div class="modal-header">
                <!--<button type="button" data-dismiss="modal" aria-hidden="true">&times;</button>-->
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">সার-সংক্ষেপ অনুমোদন</h4>
            </div>
            <div class="modal-body">
                <div class="panel panel-primary showUsers">
                    <div class="panel-body">

                    </div>
                    <div class="panel-footer form-actions">
                        <input type="button" data-id="" class="btn btn-primary btn-sm btn-updatesummary" value="<?=__("Submit") ?>">
                    </div>
                </div>
                <div class="tabbable-custom nav-justified" >
                    <ul class="nav nav-tabs nav-justified">
                        <li class="active">
                            <a href="#tab_cover2" data-toggle="tab">
                                কভার পাতা</a>
                        </li>
                        <li>
                            <a href="#tab_sar_body2" data-toggle="tab">
                                সার-সংক্ষেপ </a>
                        </li>
                    </ul>
                    <div class="tab-content" style="height: 100%!important;width: 7in;">
                        <div class="tab-pane active" id="tab_cover2">
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="template-bodycover" style="background-color: #fff; max-width:950px; min-height:815px; height: auto; margin:0 auto;">

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_sar_body2">
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="template-bodybody" style="background-color: #fff; max-width:950px; min-height:815px; height: auto; margin:0 auto;">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade modal-purple height-auto" id="responsiveOnuccedModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-full">

        <div class="modal-content">
            <div class="modal-header">

                <button type="button" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<datalist id="selectReason">
    <option value="বিদেশ গমন করছেন">বিদেশ গমন করছেন</option>
    <option value="ছুটিতে আছেন">ছুটিতে আছেন</option>
</datalist>

<form method="get" action="<?= $this->Url->build(['controller'=>'NothiMasters','action'=>'apiPotroPage',$id,$nothi_office]) ?>" id="refresh_me">
    <input type="hidden" name="api_key" value="<?= $apikey ?>">
    <input type="hidden" name="data_ref" value="api">
    <input type="hidden" name="user_designation" value="<?= $user_designation ?>">
    <input type="hidden" name="height" id="height" value="<?= $height ?>">
    <input type="hidden" name="width" id="width" value="<?= $width ?>">
</form>
<form method="post" action="" id="custom_form">
    <input type="hidden" name="user_designation" value="<?= $user_designation ?>">
    <input type="hidden" name="api_key" value="<?= $apikey ?>">
    <input type="hidden" name="data_ref" value="api">
    <input type="hidden" name="height" id="height" value="<?= $height ?>">
    <input type="hidden" name="width" id="width" value="<?= $width ?>">
</form>
<script>
    var signature = '<?=!empty($employee_office['default_sign'])?$employee_office['default_sign']:0?>';
    var numberArray = {
        1: '১',
        2: '২',
        3: '৩',
        4: '৪',
        5: '৫',
        6: '৬',
        7: '৭',
        8: '৮',
        9: '৯',
        0: '০'
    };

    //summary nothi
    var summarydraftpotroPageCount = 1;
    $(document).off('click', '.summaryDraftimageNext');
    $(document).on('click', '.summaryDraftimageNext', function () {

        var firstndex = ($('.summaryDraftattachmentsRecord').first().index() == -1 ? 0 : $('.summaryDraftattachmentsRecord').first().index());
        var nowIndex = $('.summaryDraftattachmentsRecord.active').index();
        var lastIndex = $('.summaryDraftattachmentsRecord').last().index() == -1 ? 0 : $('.summaryDraftattachmentsRecord').last().index();

        if (nowIndex >= firstndex) {
            $('.summaryDraftimagePrev').removeClass('disabled');
        }

        if (lastIndex == 0 || nowIndex >= (<?php
                echo !empty($summaryAttachmentRecord) ? (count($summaryAttachmentRecord) - 1) : 0
                ?>)) {
            $(this).addClass('disabled');
            return;
        }

        if ($('.summaryDraftattachmentsRecord').eq(nowIndex).hasClass('last') == false) {
            nowIndex++;
            $('.summaryDraftattachmentsRecord').removeClass('active');
            $('.summaryDraftattachmentsRecord').eq(nowIndex).addClass('active');
        }
    });

    $(document).on('click', '.summaryDraftimagePrev', function () {
        var firstndex = ($('.summaryDraftattachmentsRecord').first().index() == -1 ? 0 : $('.summaryDraftattachmentsRecord').first().index());
        var nowIndex = $('.summaryDraftattachmentsRecord.active').index();
        var lastIndex = $('.summaryDraftattachmentsRecord').last().index() == -1 ? 0 : $('.summaryDraftattachmentsRecord').last().index();

        if (nowIndex < lastIndex) {
            $('.summaryDraftimageNext').removeClass('disabled');
        }

        if (nowIndex <= 0) {
            $('.summaryDraftimageNext').removeClass('disabled');
            $(this).addClass('disabled');
            return;
        }


        if ($('.summaryDraftattachmentsRecord').eq(nowIndex).hasClass('first') == false) {
            $('.summaryDraftattachmentsRecord').removeClass('active');
            $('.summaryDraftattachmentsRecord').eq(nowIndex - 1).addClass('active');
        } else {
            $('.summaryDraftimagePrev').addClass('disabled');
            return;
        }
    });

    //draft
    var draftpotroPageCount = 1;
    $(document).off('click', '.DraftimageNext');
    $(document).on('click', '.DraftimageNext', function () {

        var firstndex = ($('.DraftattachmentsRecord').first().index() == -1 ? 0 : $('.DraftattachmentsRecord').first().index());
        var nowIndex = $('.DraftattachmentsRecord.active').index();
        var lastIndex = $('.DraftattachmentsRecord').last().index() == -1 ? 0 : $('.DraftattachmentsRecord').last().index();

        if (nowIndex >= firstndex) {
            $('.DraftimagePrev').removeClass('disabled');
        }

        if (lastIndex == 0 || nowIndex >= (<?php echo count($draftPotro) - 1 ?>)) {
            $(this).addClass('disabled');
            return;
        }

        if ($('.DraftattachmentsRecord').eq(nowIndex).hasClass('last') == false) {
            nowIndex++;
            $('.DraftattachmentsRecord').removeClass('active');
            $('.DraftattachmentsRecord').eq(nowIndex).addClass('active');
        }
    });

    $(document).on('click', '.DraftimagePrev', function () {
        var firstndex = ($('.DraftattachmentsRecord').first().index() == -1 ? 0 : $('.DraftattachmentsRecord').first().index());
        var nowIndex = $('.DraftattachmentsRecord.active').index();
        var lastIndex = $('.DraftattachmentsRecord').last().index() == -1 ? 0 : $('.DraftattachmentsRecord').last().index();

        if (nowIndex < lastIndex) {
            $('.DraftimageNext').removeClass('disabled');
        }

        if (nowIndex <= 0) {
            $('.DraftimageNext').removeClass('disabled');
            $(this).addClass('disabled');
            return;
        }

        if ($('.DraftattachmentsRecord').eq(nowIndex).hasClass('first') == false) {
            $('.DraftattachmentsRecord').removeClass('active');
            $('.DraftattachmentsRecord').eq(nowIndex - 1).addClass('active');
        } else {
            $('.DraftimagePrev').addClass('disabled');
            return;
        }

    });

    //potro

    var potroPageCount = 1;
    $(document).off('click', '.imageNext');
    $(document).on('click', '.imageNext', function () {

        var firstndex = ($('.attachmentsRecord').first().index() == -1 ? 0 : $('.attachmentsRecord').first().index());
        var nowIndex = $('.attachmentsRecord.active').index();
        var lastIndex = $('.attachmentsRecord').last().index() == -1 ? 0 : $('.attachmentsRecord').last().index();

        if (nowIndex >= firstndex) {
            $('.imagePrev').removeClass('disabled');
        }

        if (lastIndex == 0 || nowIndex >= (<?php echo $total - 1 ?>)) {
            $(this).addClass('disabled');
            return;
        }

        if ($('.attachmentsRecord').eq(nowIndex).hasClass('last') == false) {
            nowIndex++;
            $('.attachmentsRecord').removeClass('active');
            $('.attachmentsRecord').eq(nowIndex).addClass('active');
            var potroPage = $('.attachmentsRecord').eq(nowIndex).attr('id_bn');
            getpotropage(potroPage);

        } else {
            $('.attachmentsRecord').removeClass('active');
            $('.attachmentsRecord').removeClass('last');
            potroPageCount++;
            $.ajax({
                url: '<?php echo $this->request->webroot ?>nothiMasters/apiNextPotroPage/<?php echo $id . '/'; ?>' + potroPageCount + '?data_ref=api',
                dataType: 'JSON',
                type: 'post',
                data: {nothi_office:<?php echo $nothi_office ?>, user_designation: <?= $user_designation ?>},
                success: function (response) {

                    if (response.html == '') {
                        $('.imageNext').addClass('disabled');
                    } else {
                        $('#potroview_list .potroview').append(response.html);
                        getpotropage(response.potrono);
                        $('.imageNext').removeClass('disabled');
                    }

                }

            });
        }
    });

    $(document).on('click', '.imagePrev', function () {
        var firstndex = ($('.attachmentsRecord').first().index() == -1 ? 0 : $('.attachmentsRecord').first().index());
        var nowIndex = $('.attachmentsRecord.active').index();
        var lastIndex = $('.attachmentsRecord').last().index() == -1 ? 0 : $('.attachmentsRecord').last().index();

        if (nowIndex < lastIndex) {
            $('.imageNext').removeClass('disabled');
        }

        if (nowIndex <= 0) {
            $('.imageNext').removeClass('disabled');
            $(this).addClass('disabled');
            return;
        }


        if ($('.attachmentsRecord').eq(nowIndex).hasClass('first') == false) {
            $('.attachmentsRecord').removeClass('active');
            $('.attachmentsRecord').eq(nowIndex - 1).addClass('active');
            var potroPage = $('.attachmentsRecord').eq(nowIndex - 1).attr('id_bn');
            getpotropage(potroPage);

        } else {
            $('.imagePrev').addClass('disabled');
            return;
        }

    });

    $(document).on('keypress', '.potroNo', function (key) {
        if (key.keyCode == 13) {
            goToPage($(this).val());
        }
    });


    function potroShow(href) {
        $('.potroview').html('<img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp;লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');
        $.ajax({
            url: href,
            success: function (response) {
                $('.potroview').html(response);
                Metronic.initSlimScroll('.scroller');

                if (typeof (pageno) != 'undefined') {
                    goToPage(pageno);
                    getpotropage(pageno);
                }

                $('[data-title-orginal]').tooltip({'placement':'bottom'});

                $('li').tooltip({'placement':'bottom'});

            }
        })

        $('[data-title-orginal]').tooltip({'placement':'bottom'});

        $('li').tooltip({'placement':'bottom'});

    }
    function getpotropage(potroPage) {
        $('.potroNo').val(potroPage);
    }

    function goToPage(pageNo) {
        var hasDivbn = $('.attachmentsRecord ').index($('[id_bn=' + pageNo + ']'));
        var hasDiv = $('.attachmentsRecord ').index($('[id_en=' + pageNo + ']'));

        if (hasDivbn != -1 || hasDiv != -1) {
            $('.attachmentsRecord').removeClass('active');
            if (hasDivbn != -1) {
                $('.attachmentsRecord').eq(hasDivbn).addClass('active');
            } else if (hasDiv != -1) {
                $('.attachmentsRecord').eq(hasDiv).addClass('active');
            }
        } else {
            $('.attachmentsRecord').removeClass('active');
            $('.attachmentsRecord').removeClass('last');
            $('.imagePrev').removeClass('disabled');
            $('.imageNext').removeClass('disabled');
            if (typeof (pageNo) == 'undefined')
                pageNo = 0;
            $.ajax({
                url: '<?php echo $this->request->webroot ?>nothiMasters/apiNextPotroPage/<?php echo $id . '/1/'; ?>' + pageNo + '?data_ref=api',
                dataType: 'JSON',
                type: 'post',
                data: {nothi_office:<?php  echo $nothi_office; ?>, user_designation: <?= $user_designation ?>},
                success: function (response) {

                    if (response.html == '') {

                    } else {
                        $('#potroview_list .potroview').append(response.html);
                        getpotropage(response.potrono);
                    }
                    $('.attachmentsRecord').eq(0).addClass('first');
                    $('[data-title-orginal]').tooltip({'placement':'bottom'});

                    $('li').tooltip({'placement':'bottom'});
                }
            });
        }
    }

    $(document).off('click', '.btn-send-draft-cron');
    $(document).on('click', '.btn-send-draft-cron', function () {
        var potrojari = $(this).attr('potrojari');
        var nothinote = $(this).attr('nothinotesid') == 0 ? 'potro' : 'note';
        var elem = $(this);
        bootbox.dialog({
            message: "আপনি কি খসড়া পত্রটি জারি করতে চান?",
            title: "খসড়া পত্র পত্রজারি",
            buttons: {
                success: {
                    label: "হ্যাঁ",
                    className: "green",
                    callback: function () {
                        if(signature > 0){
                            getSignatureToken('apiMakePotrojariRequest',potrojari,nothinote,elem);
                        }else{
                            apiMakePotrojariRequest(potrojari,nothinote,elem);
                        }

                    }
                },
                danger: {
                    label: "না",
                    className: "red",
                    callback: function () {
                    }
                }
            }
        });
    });
function apiMakePotrojariRequest(potrojari,nothinote,elem){
    if(isEmpty(potrojari)||isEmpty(nothinote)||isEmpty(elem)){
        toastr.error('দুঃখিত তথ্য সংরক্ষিত করা সম্ভব হচ্ছে না।')
        return;
    }
    Metronic.blockUI({
        target: '.potroDetailsPage',
        boxed: true,
        message: 'অপেক্ষা করুন'
    });
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "positionClass": "toast-top-center"
    };
                    var checkpromise = new Promise (function(resolve, reject){
                        if(elem.closest('.DraftattachmentsRecord.active').find('#template-body').find('#sender_signature img').attr('src') == '' || typeof(elem.closest('.DraftattachmentsRecord.active').find('#template-body').find('#sender_signature img').attr('src')) == 'undefined'){
                            reject("দুঃখিত! স্বাক্ষর লোড হয়নি। পুনরায় সংশোধন করে সংরক্ষণ করুন।")
                        }

                        else if(elem.closest('.DraftattachmentsRecord.active').find('#template-body').find('#sender_signature2').length > 0
                            && (elem.closest('.DraftattachmentsRecord.active').find('#template-body').find('#sender_signature2 img').attr('src') == '' ||
                                typeof(elem.closest('.DraftattachmentsRecord.active').find('#template-body').find('#sender_signature2 img').attr('src')) == 'undefined')){
                            reject("দুঃখিত! স্বাক্ষর লোড হয়নি। পুনরায় সংশোধন করে সংরক্ষণ করুন।")
                        }

                        else if(elem.closest('.DraftattachmentsRecord.active').find('#template-body').find('#sender_signature3').length > 0
                            && (elem.closest('.DraftattachmentsRecord.active').find('#template-body').find('#sender_signature3 img').attr('src') == '' ||
                                typeof(elem.closest('.DraftattachmentsRecord.active').find('#template-body').find('#sender_signature3 img').attr('src')) == 'undefined')){
                            reject("দুঃখিত! স্বাক্ষর লোড হয়নি। পুনরায় সংশোধন করে সংরক্ষণ করুন।")
                        }

                        else if(elem.closest('.DraftattachmentsRecord.active').find('#template-body').find('#sovapoti_signature').length > 0
                            && (elem.closest('.DraftattachmentsRecord.active').find('#template-body').find('#sovapoti_signature img').attr('src') == '' ||
                                typeof(elem.closest('.DraftattachmentsRecord.active').find('#template-body').find('#sovapoti_signature img').attr('src')) == 'undefined')){
                            reject("দুঃখিত! স্বাক্ষর লোড হয়নি। পুনরায় সংশোধন করে সংরক্ষণ করুন।")
                        }

                        else if(elem.closest('.DraftattachmentsRecord.active').find('#template-body').find('#sovapoti_signature2').length > 0
                            && (elem.closest('.DraftattachmentsRecord.active').find('#template-body').find('#sovapoti_signature2 img').attr('src') == '' ||
                                typeof(elem.closest('.DraftattachmentsRecord.active').find('#template-body').find('#sovapoti_signature2 img').attr('src')) == 'undefined')){
                            reject("দুঃখিত! স্বাক্ষর লোড হয়নি। পুনরায় সংশোধন করে সংরক্ষণ করুন।")
                        }else{
                            resolve();
                        }

                    }).then(function(){
                        var data2send = {nothi_office:<?php  echo $nothi_office; ?>, user_designation: <?= $user_designation ?>};
                        if(signature > 0){
                            data2send.soft_token = $("#soft_token").val();
                        }
                        $.ajax({
                            url: '<?php
                                echo $this->Url->build(['controller' => 'Potrojari',
                                                        'action' => 'apiSendDraftCronPotrojari', $id])
                                ?>/' + potrojari + '/' + nothinote,
                            method: 'post',
                            cache: false,
                            dataType: 'JSON',
                            data: data2send,
                            success: function (response) {
                                if (response.status == 'error') {
                                    toastr.error(response.msg);
                                    Metronic.unblockUI('.potroDetailsPage');
                                } else {
                                    toastr.success(response.msg);
                                    $("#refresh_me").submit();
                                }
                            },
                            error: function (xhr, status, errorThrown) {
                                toastr.options = {
                                    "closeButton": true,
                                    "debug": false,
                                    "positionClass": "toast-top-center"
                                };
                                toastr.error("দুঃখিত! কিছুক্ষন পর আবার চেষ্টা করুন। ধন্যবাদ");
                                Metronic.unblockUI('.potroDetailsPage');
                            }
                        });
                    }).catch(function(err){
                        toastr.error(err);
                        Metronic.unblockUI('.potroDetailsPage');
                    });
}

    $(document).off('change', '.approveDraftNothi').on('change', '.approveDraftNothi', function () {
        var thisObj = $(this);
        var potrojari = thisObj.attr('potrojari_id');
        bootbox.dialog({
            message: "আপনি কি খসড়া পত্রটি " + ($(".approveDraftNothi").is(':checked')==true?"অনুমোদন":"অনুমোদন বাতিল") +" করতে ইচ্ছুক?",
            title: "খসড়া পত্র অনুমোদন",
            buttons: {
                success: {
                    label: "হ্যাঁ",
                    className: "green",
                    callback: function () {
                        if($(".approveDraftNothi").is(':checked') == true && signature > 0){
                            getSignatureToken('apiPotroApporval',thisObj,potrojari);
                        }else{
                            apiPotroApporval(thisObj,potrojari);
                        }
                    }
                },
                danger: {
                    label: "না",
                    className: "red",
                    callback: function () {
                        if($(".approveDraftNothi").is(':checked')==false){
                            $(".approveDraftNothi").attr('checked','checked');
                            $(".approveDraftNothi").closest('span').addClass('checked');
                        }
                        else{
                            $(".approveDraftNothi").removeAttr('checked');
                            $(".approveDraftNothi").closest('span').removeClass('checked');
                        }
                    }
                }
            }
        });

    });
    function apiPotroApporval(thisObj,potrojari){
        if(isEmpty(thisObj)||isEmpty(potrojari)){
            toastr.error('দুঃখিত তথ্য সংরক্ষণ করা হয়নি');
            return;
        }
        Metronic.blockUI({
            target: '.potroDetailsPage',
            boxed: true,
            message: 'অপেক্ষা করুন'
        });
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-top-center"
        };
        var data2send = {
            potrojari_id: potrojari,user_designation: <?= $user_designation ?>,
            status: thisObj.is(':checked') ? 1 : 0
        };
        if(signature > 0){
            data2send.soft_token = $("#soft_token").val();
        }
        PROJAPOTI.ajaxSubmitDataCallback('<?=
            $this->Url->build(['controller' => 'Potrojari',
                               'action' => 'apiApprovePotrojari', $nothi_office])
            ?>', data2send, 'json', function (response) {
            if (response.status == 'success') {
                toastr.success(($(".approveDraftNothi").is(':checked')==true?"অনুমোদন":"অনুমোদন বাতিল")+' করা হয়েছে।');
                Metronic.unblockUI('.potroDetailsPage');
                $("#refresh_me").submit();
            }else{
                toastr.error('অনুরোধ সম্পন্ন করা সম্ভব হচ্চেহ না।');
                Metronic.unblockUI('.potroDetailsPage');
            }
        });
    }
    function viewEditDraft(link,designation){
        $("#custom_form").attr('action',link);
        // $("#editor_height").val(editor_height);
        // $("#editor_width").val(editor_width);
        $("#custom_form").submit();
    }

    $(document).off('click', '.songlapbutton').on('click', '.songlapbutton', function () {
        $('#responsivepotalaLog').modal('show');
        $('#responsivepotalaLog').find('.scroller').html('<img src="' + js_wb_root + 'assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp;লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');

        var id = $(this).attr('id');
        var url = $(this).data('url');
        var type = $(this).data('type');
        var officeid = parseInt($(this).data('nothi-office'));

        if(typeof(id)!='undefined'){
            $.ajax({
                url: '<?php echo $this->Url->build(['controller' => 'NothiMasters', 'action' => 'showSonglap']) ?>/' + id + '/' + officeid,
                method: 'post',
                dataType: 'json',
                cache: false,
                success: function (response) {

                    var attachmenttype = response.attachment_type;
                    if (response.attachment_type == 'text') {
                        $('#responsivepotalaLog').find('.scroller').html('<div style="background-color: #fff; max-width:950px; min-height:815px; margin:0 auto; page-break-inside: auto;">' + response.potro_cover + "<br/>" + response.content_body + "</div>");
                    } else if ((attachmenttype.substring(0, 15)) == 'application/vnd' || (attachmenttype.substring(0, 15)) == 'application/ms') {
                        $('#responsivepotalaLog').find('.scroller').html('<iframe style=" width:100%; height: 700px;" src="https://docs.google.com/gview?url=' + encodeURIComponent('<?php echo FILE_FOLDER ?>' + response.file_name) + '&embedded=true" style="width:600px; height:500px;" frameborder="0"></iframe>');

                    } else if ((attachmenttype.substring(0, 5)) != 'image') {
                        $('#responsivepotalaLog').find('.scroller').html('<embed src="' + '<?php echo $this->request->webroot . 'content/' ?>' + response.file_name + '" style=" width:100%; height: 700px;" type="' + response.attachment_type + '"></embed>');
                    } else {
                        $('#responsivepotalaLog').find('.scroller').html('<div class="text-center"><a href="' + '<?php echo $this->request->webroot . 'content/' ?>' + response.file_name + '" data-gallery="multiimages"  data-toggle="lightbox" data-title="পত্র নম্বর:  ' + response.nothi_potro_page_bn + '" data-footer="" ><img class="zoomimg img-responsive"  src="' + '<?php echo $this->request->webroot . 'content/' ?>' + response.file_name + '" alt="ছবি পাওয়া যায়নি"></a></div>');
                    }
                },
                error: function (err, status, rspn) {
                    $('#responsivepotalaLog').find('.scroller').html('');
                }
            })
        }
        else if(typeof(url)!='undefined'){
            if ((type.substring(0, 15)) == 'application/vnd' || (type.substring(0, 15)) == 'application/ms') {
                $('#responsivepotalaLog').find('.scroller').html('<iframe style=" width:100%; height: 700px;" src="https://docs.google.com/gview?url=' + url + '&embedded=true" style="width:600px; height:500px;" frameborder="0"></iframe>');

            } else if ((type.substring(0, 5)) != 'image') {
                $('#responsivepotalaLog').find('.scroller').html('<embed src="' + url + '" style=" width:100%; height: 700px;" type="' + type + '"></embed>');
            } else {
                $('#responsivepotalaLog').find('.scroller').html('<div class="text-center"><a href="' + url + '" data-gallery="multiimages"  data-toggle="lightbox" data-title="সংলাগ" data-footer="" ><img class="zoomimg img-responsive"  src="' + url + '" alt="সংলাগ পাওয়া যায়নি"></a></div>');
            }
        }
    });

    <?php if (!empty($draftSummary)): ?>

    $(document).on('click', '.btn-send-summary-draft', function () {

        var potrojari = $(this).attr('potrojari');
        bootbox.dialog({
            message: "আপনি কি সার-সংক্ষেপ জারি করতে চান?",
            title: "সার-সংক্ষেপ জারি",
            buttons: {
                success: {
                    label: "হ্যাঁ",
                    className: "green",
                    callback: function () {
                        Metronic.blockUI({
                            target: '.page-container',
                            boxed: true
                        });
                        $.ajax({
                            url: '<?php echo $this->Url->build(['_name' => 'apiSendSdraftPotrojari', $id,$nothi_office]) ?>/' + potrojari,
                            method: 'post',
                            cache: false,
                            dataType: 'JSON',
                            data: {user_designation:<?= intval($user_designation) ?>,api_key:'<?= $apikey ?>',data_ref:'api'},
                            success: function (response) {

                                if (response.status == 'error') {
                                    Metronic.unblockUI('.page-container');
                                    toastr.error(response.msg);
                                } else {
                                    toastr.success(response.msg);
                                    setTimeout(function () {
                                        $("#refresh_me").submit();
                                    }, 2000);
                                }
                            },
                            error: function (xhr, status, errorThrown) {
                                toastr.options = {
                                    "closeButton": true,
                                    "debug": false,
                                    "positionClass": "toast-top-center"
                                };
                                toastr.error("দুঃখিত! ইন্টারনেট সংযোগ বিঘ্নিত হয়েছে। কিছুক্ধষণ পর আবার চেষ্টা করুন। ধন্যবাদ");
                            }
                        });
                    }
                },
                danger: {
                    label: "না",
                    className: "red",
                    callback: function () {
                    }
                }
            }
        });

    });

    $(document).off('click', '.summary_draft_approve').on('click', '.summary_draft_approve', function () {
        Metronic.blockUI({
            target: '.page-container',
            boxed: true,
            message: 'অপেক্ষা করুন'
        });
        var potrojari = $(this).attr('potrojari_id');
        var prm = new Promise(function(res, rej){
            $.ajax({
                url: '<?= $this->Url->build(['_name' => 'apiGetPosition', $id,$nothi_office]) ?>/'+potrojari,
                method: "post",
                dataType: 'JSON',
                async: false,
                data: {user_designation:<?= intval($user_designation) ?>,api_key:'<?= $apikey ?>',data_ref:'api'},
                cache: false,
                success: function (response) {
                    if(response.status == 'error'){
                        rej(response.msg);
                    }else{
                        res(response);
                    }
                },
                error: function (xhr, status, errorThrown) {
                    Metronic.unblockUI('.page-container');
                }
            });
        }).then(function(data){
            Metronic.unblockUI('.page-container');
            var approves = ($('.summary_draft_approve').is(':checked') === true ? 1 : 0);
            if(approves==1){
                $('#sarnothidraft_list').find('#template-body2').find('#'+data.data.position_number).addClass('showImage');
            }else{
                $('#sarnothidraft_list').find('#template-body2').find('#'+data.data.position_number).removeClass('showImage');
            }

            var url = '<?php echo $this->Url->build(['_name' => 'apiDraftApproved', $id]) ?>/' + potrojari + '/' + approves + '/' +<?= $nothi_office ?>;

            $.ajax({
                url: url,
                method: "post",
                dataType: 'JSON',
                async: false,
                cache: false,
                data: {
                    description: $('#sarnothidraft_list').find('#template-body2').html(),
                    user_designation:<?= intval($user_designation) ?>,api_key:'<?= $apikey ?>',data_ref:'api'
                },
                success: function (response) {
                    if (response.status == 'error') {
                        $('.summary_draft_approve').closest('.checked').removeClass('checked');
                        $('.summary_draft_approve').removeAttr('checked');
                        toastr.error(response.msg);
                    } else {
                        $("#refresh_me").submit();
                    }
                },
                error: function (xhr, status, errorThrown) {
                    Metronic.unblockUI('.page-container');
                }
            });
        }).catch(function(err){
            taostr.error(err);
        });
    });
    $(document).off('click', '.btn-check-summary-draft').on('click', '.btn-check-summary-draft', function () {
        $('.btn-check-summary-draft').attr('disabled',true);
        var potrojari = $(this).attr('potrojari');
        $('.btn-updatesummary').attr('data-id',potrojari);
        var newprom = new Promise(function(resolve, reject){
            Metronic.blockUI({
                target: '.page-container',
                boxed: true,
                message: 'অপেক্ষা করুন'
            });

            $.ajax({
                url: '<?php echo $this->Url->build(['_name' => 'apiApprovalCheck', $id]) ?>/' + potrojari + '/' + <?= $nothi_office ?>,
                method: "post",
                dataType: 'JSON',
                data: {user_designation:<?= intval($user_designation) ?>,api_key:'<?= $apikey ?>',data_ref:'api'},
                success: function (response) {
                    Metronic.unblockUI('.page-container');
                     $('.btn-check-summary-draft').removeAttr('disabled');
                    if(response.status == 'success'){
                        if(parseInt(response.count) == 0){
                            $('.btn-check-summary-draft').addClass('btn-send-summary-draft').removeClass('btn-check-summary-draft');
                            $('.btn-send-summary-draft').trigger('click');
                        }else{
                            resolve(potrojari)
                        }
                    }else{
                        reject(response.msg);
                    }
                },
                error: function (xhr, status, errorThrown) {
                    reject(errorThrown)
                }
            });
        });

        newprom.then(function(potrojari){
         Metronic.blockUI({
                target: '.page-container',
                boxed: true,
                message: 'অপেক্ষা করুন'
            });
            $('.btn-check-summary-draft').attr('disabled',true);
            $.ajax({
                url: '<?php echo $this->Url->build(['_name' => 'apiApprovalUsers', $id]) ?>/' + potrojari + '/' + <?= $nothi_office ?>,
                method: "post",
                dataType: 'JSON',
                data: {user_designation:<?= intval($user_designation) ?>,api_key:'<?= $apikey ?>',data_ref:'api'},
                success: function (response) {
                    $('.btn-check-summary-draft').removeAttr('disabled');
                    Metronic.unblockUI('.page-container');
                    $('#modalSummaryNothi').modal('show');
                    if(response.status == 'success'){
                        $('#modalSummaryNothi').find('#template-bodycover').html(response.potrojari.potro_cover);
                        $('#modalSummaryNothi').find('#template-bodybody').html(response.potrojari.potro_description);
                        var userDiv = "<table class='table table-bordered'>";

                        $.each(response.data, function(i,v){
                            userDiv += "<tr class='approvaluserlist' id='"+ v.position_number +"'><td>" + v.employee_name + "</td><td>" + v.designation_name + "</td><td>" + '<input type="text" list="selectReason" class="form-control" placeholder="অনুমোদন না করার কারন লিখুন">' + "</td></tr>";
                        });
                        userDiv += "</table>";
                        $('#modalSummaryNothi').find('.panel-body').html(userDiv);
                    }else{

                    }
                },
                error: function (xhr, status, errorThrown) {
                $('.btn-check-summary-draft').removeAttr('disabled');
                    Metronic.unblockUI('.page-container');
                    newprom.reject(errorThrown)
                }
            });
        }).catch(function(err){
            $('.btn-check-summary-draft').removeAttr('disabled');
            toastr.error(err);
        })
    });

    $('.btn-updatesummary').click(function(){
        var userlist = $(this).closest('.panel').find('.approvaluserlist');

        var prm = new Promise(function(resolve, reject){
            var data = [];
            $.each(userlist, function(i,v){
                var onumodontext = 0;
                var checkonumodon = 0;
                if($.trim($(v).find('[type=text]').val()).length>0){
                    onumodontext = $.trim($(v).find('[type=text]').val());
                    checkonumodon = 1;
                }

                data.push({'position':$(v).attr('id'),'status':checkonumodon});
                if(checkonumodon != 0 && onumodontext != 0){
                    $('#template-bodybody').find('#'+$(v).attr('id')).text(onumodontext).css({'color':'red','font-style':'italic'});
                }
            });
            resolve(data);
        }).then(function(getdata){

            var potrojari = $('.btn-updatesummary').data('id');
            $.ajax({
                url: '<?php echo $this->Url->build(['_name' => 'apiApprovalStatus', $id]) ?>/' + potrojari + '/' + <?= $nothi_office ?>,
                method: "post",
                dataType: 'JSON',
                data: {
                    data: getdata, description: $('#template-bodybody').html(),
                    user_designation:<?= intval($user_designation) ?>,api_key:'<?= $apikey ?>',data_ref:'api'
                },
                success: function (response) {
                    if(response.status == 'success'){
							if(parseInt(response.canpotrojari) == 0) {
								setTimeout(function () {
									$.ajax({
										url: '<?php echo $this->Url->build(['_name' => 'apiSendSdraftPotrojari', $id, $nothi_office]) ?>/' + potrojari,
										method: 'post',
										cache: false,
										dataType: 'JSON',
										data: {
											user_designation:<?= intval($user_designation) ?>,
											api_key: '<?= $apikey ?>',
											data_ref: 'api'
										},
										success: function (response) {

											if (response.status == 'error') {
												Metronic.unblockUI('.page-container');
												toastr.error(response.msg);
											} else {
												toastr.success(response.msg);
												setTimeout(function () {
													$("#refresh_me").submit();
												}, 2000);
											}
										},
										error: function (xhr, status, errorThrown) {
											toastr.options = {
												"closeButton": true,
												"debug": false,
												"positionClass": "toast-top-center"
											};
											toastr.error("দুঃখিত! ইন্টারনেট সংযোগ বিঘ্নিত হয়েছে। কিছুক্ধষণ পর আবার চেষ্টা করুন। ধন্যবাদ");
										}
									});
								}, 2000);
							}else{
								setTimeout(function () {
									$("#refresh_me").submit();
								}, 1000);
                            }

                    }else{
                        toastr.error(response.msg);
                    }
                },
                error: function (xhr, status, errorThrown) {
                    toastr.error("দুঃখিত! ইন্টারনেট সংযোগ বিঘ্নিত হয়েছে। কিছুক্ধষণ পর আবার চেষ্টা করুন। ধন্যবাদ");
                }
            });
        });

    });
    <?php endif; ?>

	$(document).on('click', '.showforPopup', function (e) {
		e.preventDefault();
		var title = $(this).attr('title').length > 0 ? $(this).attr('title') : $(this).attr('data-original-title');
		getPopUpPotro($(this).attr('href'), title);
	})

	function getPopUpPotro(href, title) {
		$('#responsiveOnuccedModal').find('.modal-title').text('');
		$('#responsiveOnuccedModal').find('.modal-body').html('');
		PROJAPOTI.ajaxSubmitDataCallback('<?php echo $this->request->webroot; ?>NothiMasters/showPopUp/' + href+'?nothi_part='+'<?= $id ?>'+'&token='+'<?= sGenerateToken(['file' => $id, 'office_id'=>$employee_office['office_id'], 'office_unit_id'=>$employee_office['office_unit_id'], 'office_unit_organogram_id'=>$employee_office['office_unit_organogram_id']], ['exp' => time() + 60 * 300]) ?>', {'nothi_office':<?php echo $nothi_office ?>}, 'html', function (response) {
			$('#responsiveOnuccedModal').modal('show');
            if(title.length > 25){
                title = title.slice(0,21)+'...';
            }
			$('#responsiveOnuccedModal').find('.modal-title').text(title);

			$('#responsiveOnuccedModal').find('.modal-body').html(response);
		});
	}
    function getSignatureToken(functionName,el1,el2,el3){
        if(signature > 0)
        {
            $("#soft_token").focus();
            $("#soft_token").val(0);
            var promise = function () {
                return new Promise(function (resolve, reject) {
                    if (signature == 1) {
                        PROJAPOTI.ajaxSubmitDataCallback(js_wb_root+'employeeRecords/checkUserSoftToken',{'api_key' : '<?= $apikey ?>','employee_record_id' : '<?=$employee_office['officer_id'] ?>'},'json',function(result){
                            if(!isEmpty(result) && !isEmpty(result.status)){
                                if(result.status == 'success'){
                                    var str = '<div class="col-md-12 row"><div class="col-md-2 col-sm-2"><label class="control-label pull-right">সফট টোকেনঃ </label></div><div class="col-md-8 col-sm-8"><input type="password" class="form-control" id="soft_token" readonly></div></div><div class="col-md-12 row text-center font-green">'+(!isEmpty(result.message)?(result.message):'') +'</div>';
                                    setTimeout(function(){
                                        $("#soft_token").val((!isEmpty(result.sign_token)?result.sign_token:''));
                                    },1000);
                                    resolve(str);
                                }else{
                                    var str ='<div class="col-md-12 col-sm-12 col-xs-12"><label class="control-label">নিম্নে সফট টোকেনটি সরবরাহ করুন </label></div><div class="col-md-12 col-sm-12  col-xs-12"><input type="password" class="form-control" id="soft_token" ></div><br><br><hr><br><div class="col-md-12 row">যদি সফট টোকেন দিয়ে সাইন সম্ভব না হয় তবে মোবাইল অ্যাপ এ হোমপেজ থেকে  <b>স্বাক্ষর ধরন</b> পরিবর্তন করে নিন। </div>';
                                    resolve(str);
                                }
                            }else{
                                reject('Something went wrong.Code 1');
                            }
                        });

                    } else {
                        var str ='<div class="col-md-12 col-sm-12  col-xs-12"><b>হার্ড টোকেন</b> নির্বাচন করায় প্রতি স্বাক্ষরের সময় আপনাকে ডিজিটাল <b>হার্ড টোকেন</b> এর সহযোগিতা নিতে হবে। যদি হার্ড টোকেন দিয়ে সাইন সম্ভব না হয় তবে মোবাইল অ্যাপ এ হোমপেজ থেকে  <b>স্বাক্ষর ধরন</b>  পরিবর্তন করে নিন। নিরাপত্তা নিশ্চিতকরণের জন্য দুইবার টোকেন চাওয়া হতে পারে। <input type="hidden" class="form-control" id="soft_token">  </div>';
                        resolve(str);
                    }
                });
            };

            promise().then(function (str) {
                bootbox.dialog({
                    message: str,
                    title: 'ডিজিটাল সিগনেচার',
                    buttons: {
                        success: {
                            label: ((functionName == 'forwardNothi')?'প্রেরণ':(functionName == 'potroApproval')?'খসড়া অনুমোদন':(functionName == 'makePotrojariRequest')?'পত্রজারি':(functionName == 'goForDraftSave')?'খসড়া সংশোধন':(functionName == 'NoteNisponno')?' নোট নিষ্পন্ন':(functionName == 'apiMakePotrojariRequest')?'পত্রজারি':(functionName == 'apiPotroApporval')?'খসড়া অনুমোদন':''),
                            className: "green",
                            callback: function () {
                                if(functionName == 'forwardNothi'){
                                    NothiMasterMovement.forwardNothiFunction(el1);
                                }
                                else if(functionName == 'potroApproval'){
                                    potroApproval(el1,el2);
                                }
                                else if(functionName == 'makePotrojariRequest'){
                                    makePotrojariRequest(el1,el2);
                                }
                                else if(functionName == 'goForDraftSave'){
                                    DRAFT_FORM.goForDraftSave();
                                }
                                else if(functionName == 'NoteNisponno'){
                                    NoteNisponno(el1);
                                }
                                else if(functionName == 'apiMakePotrojariRequest'){
                                    apiMakePotrojariRequest(el1,el2,el3);
                                }
                                else if(functionName == 'apiPotroApporval'){
                                    apiPotroApporval(el1,el2);
                                }
                            }
                        },
                        proceedWithOutSignature: {
                            label: ((functionName == 'forwardNothi')?'প্রেরণ':(functionName == 'potroApproval')?'খসড়া অনুমোদন':(functionName == 'makePotrojariRequest')?'পত্রজারি':(functionName == 'goForDraftSave')?'খসড়া সংশোধন':(functionName == 'NoteNisponno')?' নোট নিষ্পন্ন':(functionName == 'apiMakePotrojariRequest')?'পত্রজারি':(functionName == 'apiPotroApporval')?'খসড়া অনুমোদন':''),
                            className: "blue",
                            callback: function () {
                                $("#soft_token").val('-1');
                                if(functionName == 'forwardNothi'){
                                    NothiMasterMovement.forwardNothiFunction(el1);
                                }
                                else if(functionName == 'potroApproval'){
                                    potroApproval(el1,el2);
                                }
                                else if(functionName == 'makePotrojariRequest'){
                                    makePotrojariRequest(el1,el2);
                                }
                                else if(functionName == 'goForDraftSave'){
                                    DRAFT_FORM.goForDraftSave();
                                }
                                else if(functionName == 'NoteNisponno'){
                                    NoteNisponno(el1);
                                }
                                else if(functionName == 'apiMakePotrojariRequest'){
                                    apiMakePotrojariRequest(el1,el2,el3);
                                }
                                else if(functionName == 'apiPotroApporval'){
                                    apiPotroApporval(el1,el2);
                                }
                            }
                        },
                        danger: {
                            label: "বন্ধ করুন",
                            className: "red",
                            callback: function () {
                                if(functionName == 'potroApproval'){
                                    if ($(".approveDraftNothi").is(':checked') == false) {
                                        $(".approveDraftNothi").attr('checked', 'checked');
                                        $(".approveDraftNothi").closest('span').addClass('checked');
                                    }
                                    else {
                                        $(".approveDraftNothi").removeAttr('checked');
                                        $(".approveDraftNothi").closest('span').removeClass('checked');
                                    }
                                }
                                Metronic.unblockUI('.page-container');
                                Metronic.unblockUI('#ajax-content');
                            }
                        }
                    }
                });
                return;
            }).catch (function (error) {
                console.log('Error: ', error);
            });

        }else{
            toastr.error('দুঃখিতঃ কোন ফাংশন নির্বাচন করা হয়নি।');
        }
    }
    var editor_height = 400;
    var editor_width = 300;
    function updateEditorHeightWidth(height,width) {
        editor_height = height;
        editor_width = width;
    }
</script>
<?php
if(isset($hasCsTemplate) && $hasCsTemplate == 1){
    echo $this->Html->script('/projapoti-nothi/js/cs_related.js');
    if(!isset($loggedUser['username'])){
        $loggedUser['username'] = $employee_office['username'];
    }
    $user_signature =  sGenerateToken(['file'=>$loggedUser['username']],['exp'=>time() + 60*300]);
    echo $this->Form->hidden('user_signature_token', ['id' => 'user_signature_token', 'value' => $user_signature]);
    echo $this->Form->hidden('username', ['id' => 'username', 'value' => $loggedUser['username']]);
    ?>
    <script>
        $(document).ready(function(){
            setTimeout(csSettings.init(<?=json_encode($employee_office)?>),2000);
        });
    </script>
    <?php
}
?>