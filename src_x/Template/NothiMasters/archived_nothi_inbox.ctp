<script src="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<script src="<?php echo CDN_PATH; ?>projapoti-nothi/js/nothi_master_movements.js?v=<?= js_css_version ?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/floatthead/2.0.3/jquery.floatThead.min.js"></script>
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-datepicker/css/datepicker.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>daptorik_preview/css/custom.css"/>

<!-- END PAGE LEVEL STYLES -->
<!-- END PAGE LEVEL STYLES -->
<style>
    tfoot {
        padding: 2px;
        background-color: rgba(241, 241, 241, 0.73);
    }

    .footer {
        display: none;
    }

    .dak_sender_cell_list {
        position: absolute;
        border: 2px solid #CECECE;
        background: #F5F5F5;
        padding-top: 15px;
        overflow: auto;
        z-index: 9000;
        color: #000;
        left: 15px;
        width: 100%;
        text-align: left;
    }

    .checkbox input[type=checkbox], .checkbox-inline input[type=checkbox], .radio input[type=radio], .radio-inline input[type=radio] {
        margin-left: -10px !important;
    }

    input[type=checkbox], input[type=radio] {
        margin-top: 2px;
    }

    div.radio, div.checker {
        margin-top: -2px;
    }

    .newInbox {
        background-color: #C7DAE0;
    }

    h3 {
        margin-top: 0px;
        margin-bottom: 0px;
    }
    .editable{
        border: none;
        word-break:break-word;
        word-wrap:break-word
    }
      .nav-tabs .dropdown-menu {
        font-size: 11pt!important;
    }
    .tab-content, .active{
        background-color:  #e9ffcc!important;
    }
</style>
<?php
$session = $this->request->session();
if ($session->check('refer_url')) {
    $listtype = $session->read('refer_url');
}
if (empty($listtype)) {
    $listtype = 'inbox';
}
?>
<div class="portlet light">
	<div class="portlet-title">
		<div class="caption">
			<i class="fs0 efile-save2"></i> আর্কাইভড নথিসমূহ
		</div>
	</div>
	<div class="portlet-body">
        <div class="table-container data-table-dak">
            <table class="table table-striped table-bordered table-hover" id="datatable-nothi-masters">
                <thead>
                    <tr role="row" class="heading">
                        <th style="width: 5%" class='text-center'>
                            ক্রম
                        </th>
                        <?php
                        if($listtype == 'sent' || $listtype == 'other_sent'){
                        ?>
                          <th class='text-center' style="width: 15%">
                            নথি নম্বর
                        </th>
                        <th class='text-center' style="width: 20%">
                            শিরোনাম
                        </th>
                        <th class='text-center' style="width: 10%">
                           প্রেরণের তারিখ
                        </th>
                        <?php }else{?>
                          <th class='text-center' style="width: 20%">
                            নথি নম্বর
                        </th>
                        <th class='text-center' style="width: 25%">
                            শিরোনাম
                        </th>
                        <?php } ?>
                        <?php
                            if($listtype !='all' && $listtype !='archiveNothi'):
                        ?>
                        <th class='text-center' style="width: 20%">
                            <?php
                            if ($listtype != 'inbox' && $listtype != 'other') {
                                ?>নথির অবস্থান <?php
                            } else {
                                ?>সর্বশেষ নোটের তারিখ<?php }
                            ?>
                        </th>
                        <?php endif; ?>

                        <th class='text-center' style="width: 20%">
                            নথির শাখা
                        </th>
                        <?php if ($listtype != 'office') : ?>
                            <th class='text-center' style="width: 5%">
                                বিস্তারিত
                            </th>
                        <?php endif; ?>
                        <?php if ($listtype == 'inbox' && 'archive'=='true') { ?>
                            <th style="width: 5%" class='text-center'>
                                আর্কাইভ
                            </th>
                        <?php } ?>
                    </tr>
                </thead>
                <tbody id="dbl_click">

                </tbody>
            </table>
        </div>
    </div>
</div>


<!-- Modal -->
<div id="archiveModal" class="modal fade modal-purple height-auto" data-backdrop="static" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">আর্কাইভ</h4>
      </div>
      <div class="modal-body">
          <p class="font-lg">
            নির্বাচিত নথির শুধু মাত্র  নিষ্পন্ন নোটগুলো আর্কাইভ হবে।  আপনি কি নথিটি  আর্কাইভ করতে চান?
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" id="goforArchive"><?=__('Archive'). " করুন" ?></button>
        <button type="button" class="btn btn-danger" data-dismiss="modal"><?=__('Close') ?></button>
      </div>
    </div>

  </div>
</div>

<div id="detailsModal" class="modal fade modal-purple" data-backdrop="static" role="dialog">
    <div class="modal-dialog  modal-full">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">বিস্তারিত</h4>
            </div>
            <div class="modal-body">
<!--                <div class="input-group ">-->
<!--                    <input type="text" class="form-control input-sm" placeholder="বিষয় দিয়ে  খুঁজুন" id="sub_search">-->
<!--                    <input type="hidden" id="data-nothimasterid" value="">-->
<!--                    <input type="hidden" id="data-type" value="">-->
<!--                    <span class="input-group-btn">-->
<!--                            <button type="button" class="btn btn-sm green-haze " id="filter_submit_btn1">-->
<!--                                &nbsp; <i class="fa fa-search" aria-hidden="true"></i>-->
<!--                            </button></span>-->
<!--                </div>-->
                <div class="details-modal-inbox-content">
                </div>

            </div>
            <div class="modal-footer">
	            <div class="btn-group btn-group-round">
	                <button type="button" class="btn btn-warning newPartCreate" nothi_parts_id=0 nothi_masters_id=0>নতুন নোট</button>
	                <a type="button" class="btn btn-primary btn-sokol" href="">সকল নোট</a>
	                <button type="button" class="btn btn-danger" data-dismiss="modal"><?=__('Close') ?></button>
	            </div>
            </div>
        </div>

    </div>
</div>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/media/js/jquery.dataTables.js"></script>
<script src="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script src="<?php echo CDN_PATH; ?>assets/global/scripts/datatable.js"></script>
<script src="<?php echo CDN_PATH; ?>assets/admin/pages/scripts/table-advanced.js"></script>
<script src="<?php echo CDN_PATH; ?>projapoti-nothi/js/tbl_nothi_masters.js?v=<?=js_css_version?>"></script>

<script type="text/javascript">
    $('#view_nothi_list').show();
    $('.inbox-loading').html('');
    $.extend(true, $.fn.dataTable.defaults, {
        "ordering": false
    });
	TableAjax.init('archiveNothi');

    $('[data-toggle="tooltip"]').tooltip({'placement':'bottom'});
    $('[title]').tooltip({'placement':'bottom'});
    $('[data-title]').tooltip({'placement':'bottom'});
    $('#dbl_click').on("dblclick", 'tr', function () {
      var si =  $(this).children('td').find('a.dbl_click').attr('href');
      if(typeof(si) == 'undefined' || si == '' || si == null)
          return;
     window.location.href = si;
    });
</script>
<script>
    jQuery(document).ready(function () {
        $('.table').floatThead({
            position: 'absolute',
            top: jQuery("div.navbar-fixed-top").height()
        });
        $('.table').floatThead('reflow');
    });
</script>