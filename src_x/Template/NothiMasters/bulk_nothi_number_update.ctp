<div class="portlet light ">
    <div class="portlet-title">
        <div class="caption"><i class="fs1 a2i_gn_details1"></i>  অফিস ভিত্তিক সকল নথি নাম্বার সংশোধন   </div>
        <div class="actions">
            <a target="_blank" href="<?php echo $this->Url->build(['controller' => 'OfficeManagement','action' => 'updateOwnOffice']) ?>" class="btn green btn-sm">
                <i class="fa fa-pencil-square"></i> অফিস তথ্য সংশোধন </a>
        </div>
    </div>
    <div class="portlet-body">
        <?= $officeSelectionCell = $this->cell('OfficeSelectionByMinistry', ['entity' => '', 'prefix' => '']) ?>
        <div class="row">
            <div class="form-group">
                <div class="col-md-4 form-group form-horizontal">
                    <?php
                    echo $this->Form->input('office_id',
                        array(
                            'label' => __('Office'),
                            'class' => 'form-control',
                            'empty' => '--বাছাই করুন--'
                        ));
                    ?>
                </div>
                <div class="col-md-4 form-group form-horizontal">
                    <?php
                    echo $this->Form->input('digital_nothi_code',
                        array(
                            'label' => '৮ ডিজিটের ডিজিটাল নথি নাম্বার',
                            'type' => 'text', 'class' => 'form-control','readonly'
                            ));
                    ?>
                </div>
                <div class="col-md-4 form-group form-horizontal margin-top-20">
                    <button class="btn btn-primary btn-md" id="changeNothiNumber" style="display: none;"> নথি নাম্বার পরিবর্তন করুন</button>
                </div>
            </div>
        </div>

        <?php echo $this->Form->end() ?>
        <br/>
        <div class="inbox-content">
            <div id="showlist"></div><br>
            <div class="table-container ">
                <table class="table table-bordered table-hover tableadvance">
                    <thead>
                    <tr class="heading">
                        <th class="text-center" > নথি নম্বর </th>
                        <th class="text-center" > শিরোনাম </th>
                        <th class="text-center"> শ্রেণি </th>
                        <th class="text-center"> নথির ধরন </th>
                        <th class="text-center"> তৈরির তারিখ </th>
                    </tr>
                    </thead>
                    <tbody id="addData">

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo CDN_PATH; ?>js/reports/bulk_nothi_number_update.js" type="text/javascript"></script>
<script>

</script>