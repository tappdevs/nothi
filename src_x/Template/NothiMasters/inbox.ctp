<link rel="stylesheet" type="text/css"
      href="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"/>
<style>
    .inbox .inbox-nav li.active b {
        display: none !important;
    }

    .tooltip-inner {
        white-space: pre-line;
    }
</style>
<div class="portlet light">
    <div class="portlet-body">
        <div class="row inbox">

            <div class="col-md-12">

                <div class="inbox-loading" style="display: none;"><img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp;লোড হচ্ছে। একটু অপেক্ষা করুন... </span></div>
                <div class="inbox-content">

                </div>
            </div>
        </div>
    </div>
</div>

<div id="responsiveNothiUsers" class="modal fade" tabindex="-1" aria-hidden="true" data-backdrop="static"
     data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">পরবর্তী কার্যক্রমের জন্য প্রেরণ করুন</h4>
            </div>
            <div class="modal-body">
                <div class="scroller" style="height:100%" data-always-visible="1" data-rail-visible1="1">
                    <input type="hidden" name="nothimasterid"/>

                    <div class="user_list">

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn green sendNothi">প্রেরণ করুন</button>
                <button type="button" data-dismiss="modal" class="btn  btn-danger">
                    বন্ধ করুন
                </button>
            </div>
        </div>
    </div>
</div>


<script src="<?php echo CDN_PATH; ?>projapoti-nothi/js/nothi_master_movements.js"></script>
