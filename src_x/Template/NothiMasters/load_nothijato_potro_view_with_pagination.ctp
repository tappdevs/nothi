<div class="actions text-center">
	<ul class="pagination pagination-sm nothijatoPagination">
        <?php
        echo $this->Paginator->first(__('প্রথম', true), array('class' => 'number-first'));
        echo $this->Paginator->prev('<<', array('tag' => 'li', 'escape' => false), '<a href="#" class="btn btn-sm blue">&laquo;</a>', array('class' => 'prev disabled btn btn-sm blue', 'tag' => 'li', 'escape' => false));
        echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a', 'reverse' => FALSE));
        echo $this->Paginator->next('>>', array('tag' => 'li', 'escape' => false), '<a href="#" class="btn btn-sm blue ">&raquo;</a>', array('class' => 'next disabled', 'tag' => 'li', 'escape' => false));
        echo $this->Paginator->last(__('শেষ', true), array('class' => 'number-last'));
        ?>
	</ul>
</div>
<?php
if (!empty($NothiPotroAttachmentsData)) {
    foreach($NothiPotroAttachmentsData as $key => $potro) {


		?><div style="background-color: #a94442; padding: 5px; color: #fff;"><?=$potro->sarok_no?></div><?php
        if ($potro->attachment_type == 'text') {
            echo '<div style="background-color: #fff; max-width:950px; min-height:815px; height: auto; margin:0 auto; page-break-inside: auto;">'.html_entity_decode($potro->content_body)."</div>";
        }
	    elseif (substr($potro->attachment_type, 0, 5) != 'image') {
            if (substr($potro->attachment_type, 0, strlen('application/vnd')) == 'application/vnd' || substr($potro->attachment_type, 0, strlen('application/ms')) == 'application/ms') {
                $url = urlencode(FILE_FOLDER.$potro->file_name.'?token='.sGenerateToken(['file'=>$potro->file_name],['exp'=>time() + 60*300]));
                echo '<iframe style=" width:100%; height: 700px;" src="https://docs.google.com/gview?url='. $url.'&embedded=true" style="width:600px; height:500px;" frameborder="0"></iframe>';
            } else {
                echo '<embed src="'. $this->request->webroot . 'getContent?file='. $potro->file_name.'&token='.sGenerateToken(['file'=>$potro->file_name],['exp'=>time() + 60*300]).'" style=" width:100%; height: 700px;" type="'.$potro->attachment_type.'"></embed>';
            }
        } else {
            echo '<div class="text-center"><img class=" img-responsive"  src="'.$this->request->webroot . 'getContent?file=' . $potro->file_name.'&token='.sGenerateToken(['file'=>$potro->file_name],['exp'=>time() + 60*300]).'" alt="ছবি পাওয়া যায়নি"></div>';
        }
    }
} else {
    ?>
    <tr><td colspan="5" class="danger text-center"> কোন তথ্য পাওয়া যায়নি। </td></tr>
    <?php
}
?>
<script>
//    $(document).ready(inialize);
//    function inialize(){
//        $(".showDetailsNothiJato").on('click',showNothijato);
//    }
	jQuery(document).ready(function () {
        $(document).off('click','.nothijatoPagination a').on('click','.nothijatoPagination a',function(ev){
            ev.preventDefault();
           if($(this).closest('li').hasClass('disabled')){
                return false;
              }
             Metronic.blockUI({
                    target: '.nothijatopotroview_div',
                    boxed: true,
                    message: 'অপেক্ষা করুন'
                });
            PROJAPOTI.ajaxLoadCallback($(this).attr('href'),function(response){
                $('.nothijatopotroview_div').html(response);
                Metronic.unblockUI('.nothijatopotroview_div');
            });
        });
    });
</script>